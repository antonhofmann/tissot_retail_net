<?php
$api_key = 'SRtotaraX67645tgZhud786599688Yxc80Ak69546aXy2';

$allowed_ips = array();
$allowed_ips[] = '185.19.31.89';
//$allowed_ips[] = '127.0.0.1';
//$allowed_ips[] = '84.74.156.237';

//sg internal
$b = '194.209.66.';
for($i=1;$i<=126;$i++) {
	$allowed_ips[] = $b . $i;
}

//get user's ip(s)
if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
	$ips = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
else
{
	$ips = $_SERVER['REMOTE_ADDR'];
}
$ips = str_replace(' ', '', $ips);
$ips = explode(',', $ips);

require_once("include/functions.php");


//check access
$allowed = false;
if(array_key_exists('apikey', $_GET) and $_GET['apikey'] = $api_key)
{
	$allowed = true;
}
else {
	die;
}

$allowed = false;
$ipfilter = New IP4Filter($allowed_ips);
foreach($ips as $key=>$ip) {
	if($ipfilter->check($ip) == 1) {
		$allowed = true;
	}
}

if($allowed == false) {
	die;
}


$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';

$translate = Translate::instance();
$settings = Settings::init();

Connector::instance();

$db_retailnet = new Model(Connector::DB_CORE);

//get all data from totara
$sql = "select * from totara_posaddresses where address_id > 0";
$result = $db_retailnet->query($sql)->fetchAll();

$companies = array();
foreach($result as $key=>$data) {
	$companies[$data['address_id']] = $data;
}

$sql = "select * from totara_posaddresses where posaddress_id > 0";
$result = $db_retailnet->query($sql)->fetchAll();

$poslocations = array();
foreach($result as $key=>$data) {
	
	//check if pos location is still operating
	$sql_p = "select count(posaddress_id) from posaddresses where posaddress_id = " . $data['posaddress_id'] . 
		     " and posaddress_client_id <> 13 and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')";

	$result_p = $db_retailnet->query($sql_p)->fetchAll();

	if(count($result_p) > 0) {
		$poslocations[$data['posaddress_id']] = $data;
	}
}

//truncate
$result = $db_retailnet->db->exec("truncate totara_posaddresses");


//get all pos locations
$result = $db_retailnet->query("
		SELECT
			if(address_totara_parent > 0, (select addresses2.address_id from addresses as addresses2 where addresses2.address_id = addresses.address_totara_parent), address_id) as address_id,
			
			if(address_totara_parent > 0, (select addresses2.address_company from addresses as addresses2 where addresses2.address_id = addresses.address_totara_parent), address_company) as address_company,
			
			if(address_totara_parent > 0, (select addresses2.address_legnr from addresses as addresses2 where addresses2.address_id = addresses.address_totara_parent), address_legnr) as address_legnr,
			posaddress_id,
			posaddress_eprepnr,
			posaddress_name,
			posaddress_name2,
			posaddress_address,
			posaddress_address2,
			posaddress_zip,
			posaddress_store_openingdate, 
			posaddress_store_closingdate, 
			posaddress_ownertype, 
			place_name,
			country_iso3166,
			posaddress_country, 
			posaddress_email,
			posaddress_phone,
			postype_name,
			posaddress_store_planned_closingdate,
			posaddress_store_closingdate
		FROM
			posaddresses
		LEFT JOIN postypes ON postype_id = posaddress_store_postype
		LEFT JOIN addresses ON address_id = posaddress_client_id
		left join places on place_id = posaddress_place_id
		left join countries on country_id = posaddress_country
		WHERE
			(posaddress_ownertype = 1 or (posaddress_client_id in (67, 16, 23, 264) and posaddress_store_postype in (1, 2, 3)))
			and posaddress_client_id <> 13 
			and posaddress_store_openingdate <> '0000-000-00'
			and posaddress_store_openingdate is not null 
			and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00'
			or posaddress_store_closingdate > (NOW() - INTERVAL 2 WEEK))
			and posaddress_store_subclass <> 20
			and posaddress_eprepnr is not null
			and posaddress_eprepnr <> ''
			and posaddress_distribution_channel <> 23
			order by address_company, address_legnr, country_name, place_name, posaddress_name
	")->fetchAll();
	
	// group items by company
	if ($result) { 
		
		
		$company_id = 0;
		$pos_parent_id = 0;
		$legal_numers_used = array();
		$prefix = 1;

		foreach($result as $key=>$pos_data)
		{
			$languages = array();

			$pos_data["address_legnr"] = trim($pos_data["address_legnr"]);

			//add company
			if($pos_data["address_id"] <> $company_id) {
			
				$company_id = $pos_data["address_id"];
				if(in_array($pos_data["address_legnr"], $legal_numers_used)) {
					$pos_data["address_legnr"] = $prefix . "_" . $pos_data["address_legnr"];
					$prefix++;
				}
				$legal_numers_used[] = $pos_data["address_legnr"];
				
				//echo '</br><pre>';
				//echo $pos_data["address_legnr"] . "<br />";
				//print_r($legal_numers_used);
			
				
				$timestamp = time();

				//check if there was a data change
				$olddata = $companies[$pos_data["address_id"]]['idnumber'] . $companies[$pos_data["address_id"]]['enitity_name'];

				$newdata = $pos_data["address_legnr"] . $pos_data["address_company"];
				if($olddata == $newdata)
				{
					$timestamp = $companies[$pos_data["address_id"]]['timemodified'];
				}
				
				
				$fields = array();
				$values = array();


				$fields[] = "id2";
				$values[] = dbquote('M' . $pos_data["address_id"]);

				$fields[] = "idnumber";
				$values[] = dbquote($pos_data["address_legnr"]);
				$pos_parent_id = 'M' . $pos_data["address_id"];

				$fields[] = "frameworkidnumber";
				$values[] = 1;

				$fields[] = "typeidnumber";
				$values[] = 1;

				$fields[] = "timemodified";
				$values[] = dbquote($timestamp);

				$fields[] = "enitity_name";
				$values[] = dbquote($pos_data["address_company"]);

				$fields[] = "address_id";
				$values[] = dbquote($pos_data["address_id"]);

				$sql_i = "insert into totara_posaddresses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

				$result = $db_retailnet->db->exec($sql_i);

			}


			//get languages
			$sql_l = "select language_iso639_1
				   from country_languages 
				   left join languages on language_id = country_language_language_id 
				   where country_language_country_id = " . dbquote($pos_data["posaddress_country"]);

			
		   $poslanguages = $db_retailnet->query($sql_l)->fetchAll();

	

			foreach($poslanguages as $key=>$language){
			
				$languages[] = $language["language_iso639_1"];
			}

			if(count($languages) > 0) {
				$pos_data["languages"] = implode('-', $languages);
			}
			else {
				$pos_data["languages"] = "";
			}



			$timestamp = time();

			$olddata = '';
			if(array_key_exists($pos_data["posaddress_id"], $poslocations)) {
				$olddata = $poslocations[$pos_data["posaddress_id"]]['idnumber'] . $poslocations[$pos_data["posaddress_id"]]['parentidnumber']  . $poslocations[$pos_data["posaddress_id"]]['enitity_name']  . $poslocations[$pos_data["posaddress_id"]]['enitity_name2']  . $poslocations[$pos_data["posaddress_id"]]['enitity_address']  . $poslocations[$pos_data["posaddress_id"]]['enitity_address2']  . $poslocations[$pos_data["posaddress_id"]]['enitity_zip']  . $poslocations[$pos_data["posaddress_id"]]['enitity_city']  . $poslocations[$pos_data["posaddress_id"]]['enitity_country_iso_code']  . $poslocations[$pos_data["posaddress_id"]]['enitity_email']  . $poslocations[$pos_data["posaddress_id"]]['enitity_phone']  . $poslocations[$pos_data["posaddress_id"]]['posaddress_type']  . $poslocations[$pos_data["posaddress_id"]]['posaddress_store_planned_closingdate'] .
				$poslocations[$pos_data["posaddress_id"]]['posaddress_store_closingdate'] .
				$poslocations[$pos_data["posaddress_id"]]['enitity_languages_iso_codes'];
			}

			$newdata = $pos_data["posaddress_eprepnr"] . $pos_parent_id . $pos_data["posaddress_name"]. $pos_data["posaddress_name2"]. $pos_data["posaddress_address"]. $pos_data["posaddress_address2"]. $pos_data["posaddress_zip"]. $pos_data["place_name"]. $pos_data["country_iso3166"]. $pos_data["posaddress_email"]. $pos_data["posaddress_phone"]. $pos_data["postype_name"] . $pos_data["posaddress_store_planned_closingdate"]. $pos_data["posaddress_store_closingdate"]. $pos_data["languages"];

			if($olddata == $newdata)
			{
				$timestamp = $poslocations[$pos_data["posaddress_id"]]['timemodified'];
			}
			

			$fields = array();
			$values = array();
			
			$fields[] = "id2";
			$values[] = dbquote($pos_data["posaddress_id"]);
			
			$fields[] = "idnumber";
			$values[] = dbquote($pos_data["posaddress_eprepnr"]);

			$fields[] = "frameworkidnumber";
			$values[] = 1;

			$fields[] = "parentidnumber";
			$values[] = dbquote($pos_parent_id);

			$fields[] = "typeidnumber";
			$values[] = 2;

			$fields[] = "timemodified";
			$values[] = dbquote($timestamp);
			

			$fields[] = "enitity_name";
			$values[] = dbquote($pos_data["posaddress_name"]);

			$fields[] = "enitity_name2";
			$values[] = dbquote($pos_data["posaddress_name2"]);

			$fields[] = "enitity_address";
			$values[] = dbquote($pos_data["posaddress_address"]);

			$fields[] = "enitity_address2";
			$values[] = dbquote($pos_data["posaddress_address2"]);

			$fields[] = "enitity_zip";
			$values[] = dbquote($pos_data["posaddress_zip"]);

			$fields[] = "enitity_city";
			$values[] = dbquote($pos_data["place_name"]);

			$fields[] = "enitity_country_iso_code";
			$values[] = dbquote($pos_data["country_iso3166"]);

			$fields[] = "enitity_email";
			$values[] = dbquote($pos_data["posaddress_email"]);

			$fields[] = "enitity_phone";
			$values[] = dbquote($pos_data["posaddress_phone"]);

			$fields[] = "posaddress_type";
			$values[] = dbquote($pos_data["postype_name"]);

			$fields[] = "enitity_languages_iso_codes";
			$values[] = dbquote($pos_data["languages"]);

			if($pos_data["posaddress_store_planned_closingdate"] <> ''
			and $pos_data["posaddress_store_planned_closingdate"] != '0000-00-00') {
				$fields[] = "planned_closing_date";
				$values[] = dbquote($pos_data["posaddress_store_planned_closingdate"]);
			}

			if($pos_data["posaddress_store_closingdate"] <> ''
			and $pos_data["posaddress_store_closingdate"] != '0000-00-00') {
				$fields[] = "closing_date";
				$values[] = dbquote($pos_data["posaddress_store_closingdate"]);
			}

			$fields[] = "posaddress_id";
			$values[] = dbquote($pos_data["posaddress_id"]);

			$sql_i = "insert into totara_posaddresses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

			
			$result = $db_retailnet->db->exec($sql_i);

		}
	}


//exports csv-files
$posdata = $db_retailnet->query("
		SELECT 
id,
id2,
frameworkidnumber,
parentidnumber,
typeidnumber,
timemodified,
enitity_name,
enitity_name2,
enitity_address,
enitity_address2,
enitity_zip,
enitity_city,
enitity_country_iso_code,
enitity_email,
enitity_phone,
posaddress_type,
enitity_languages_iso_codes,
planned_closing_date,
closing_date
 from totara_posaddresses")->fetchAll();

if ($posdata) { 
				
	$csv_data = '';

	$csv_data = "id;idnumber;frameworkidnumber;parentidnumber;typeidnumber;timemodified;enitity_name;enitity_name2;enitity_address;enitity_address2;enitity_zip;enitity_city;enitity_country_iso_code;enitity_email;enitity_phone;posaddress_type;enitity_languages_iso_codes;planned_closing_date;real_closing_date" . "\r\n";

	foreach($posdata as $key=>$row) {
		
		//unset($row["id"]);
		unset($row["address_id"]);
		unset($row["posaddress_id"]);
		unset($row["idnumber"]);
		foreach($row as $index=>$value) {
			//$row[$index] = trim(utf8_decode($value));
				$row[$index] = trim($value);
				$row[$index] = str_replace(';',',', $row[$index]);
		}
		
		$csv_data .= implode(';', $row) . "\r\n";
		
	}



	header('Content-Encoding: UTF-8 ');
	header('Content-type: text/csv; charset=ISO-8859-1 ');
	header("Content-Disposition: attachment; filename=org.csv");
	header("Pragma: no-cache");
	header("Expires: 0");
	//header('X-Debug: '.implode(';',$ips));

	echo $csv_data;
	exit;
}
exit;
/*
$file = fopen("/data/www/retailnet.swatch.com/htdocs/files/tmp/org.csv","w");

//dev mode
//$file = fopen("c:/tmp/org.csv","w");
fwrite($file,$csv_data);
fclose($file);


//Change the following directory path to your specification 
$local_directory = '/data/www/retailnet.swatch.com/htdocs/files/tmp/';
$remote_directory = '/';

//dev mode
//$local_directory = 'c:/tmp/';
//$remote_directory = '/home/codingro/public_html/tmp/';

$file = "org.csv";

$ftp_server = "ftp://www.swatchroute51.com";
//$ftp_server = "ftp://185.19.31.89";


$proxy_server = "proxy01.sharedit.ch:8082";


$user = 'swatch_hrimport';
$pass = 't0ni@sw4tch51';



//UPLOAD FILE
$fp = fopen($local_directory . $file, 'r');

$ch = curl_init(); 
curl_setopt($ch, CURLOPT_URL, $ftp_server . '/' . $file);
curl_setopt($ch, CURLOPT_USERPWD,$user.':'.$pass);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
curl_setopt($ch, CURLOPT_FTP_SSL, CURLFTPSSL_TRY);
curl_setopt($ch, CURLOPT_FTPSSLAUTH, CURLFTPAUTH_TLS);
curl_setopt($ch, CURLOPT_UPLOAD, 1);
curl_setopt($ch, CURLOPT_INFILE, $fp);


//curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
//curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);


$output = curl_exec($ch);
$error_no = curl_errno($ch);
var_dump(curl_error($ch));
curl_close($ch);

*/
?>