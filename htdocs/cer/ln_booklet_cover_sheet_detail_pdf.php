<?php
/********************************************************************

    ln_booklet_cover_sheet_pdf.php

    Print Cover Sheet for CER Booklet

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2015-01-08
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2015-01-08
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.
*********************************************************************/


$pos_info = $project["order_shop_address_country_name"];
$pos_info .= ", " . $project["order_shop_address_company"];


$poslegaltype = $project["project_costtype_text"];
$projectkind = str_replace("POS", "", $project["projectkind_name"]);
$projectkind = str_replace("  ", " ", $projectkind);
$projectkind_id = $project["project_projectkind"];

$relocated_pos = "";
if($project["project_projectkind"] == 6 and $project["project_relocated_posaddress_id"] > 0) // relocation
{
	$relocated_pos = get_relocated_pos_info($project["project_relocated_posaddress_id"]);

}
$postype = $project["postype_name"];

$pos_info = $projectkind . " " . $poslegaltype . " " . $postype . "\n" . $pos_info;

if($relocated_pos)
{
	$pos_info .= "\n\n" . "Relocation of " . $relocated_pos["posaddress_name"];
}

//page Header

$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
//arialn bold 15
$pdf->SetFont('arialn','B',12);
//Move to the right

$pdf->setXY(80,0);

//Title
$pdf->Cell(0,33,"LNR Booklet Project " . $project["order_number"],0,0,'R');


// Title first line

$y=40;
$pdf->SetFont("arialn", "B", 13);
$pdf->SetXY($x+80, $y);
$pdf->MultiCell(0,3.5, $pos_info, 1, "T");


$y=80;
$x=20;
$pdf->SetXY($x, $y);
$pdf->SetFont("arialn", "B", 20);
$pdf->Cell(120, 8, "1. LNR / Mall /Street Map / Pictures", 0);




$pdf->SetFont("arialn", "B", 20);
$pdf->setXY($x, $pdf->getY()+$y/2);
$pdf->Cell(160, 8, "2. Businessplan", 0);


$pdf->SetFont("arialn", "B", 20);
$pdf->setXY($x, $pdf->getY()+$y/2);
$pdf->Cell(160, 8, "3. Distribution / sell outs", 0);


$pdf->setXY($x, $pdf->getY()+$y/2);
$pdf->Cell(160, 8, "4. Google Map / Project Briefing", 0);

$pdf->setXY($x, $pdf->getY()+$y/2);
$pdf->Cell(160, 8, "5. Additional Info", 0);
$pdf->setXY($x+6, $pdf->getY()+10);
$pdf->Cell(160, 8, "Email Exchange", 0);



?>