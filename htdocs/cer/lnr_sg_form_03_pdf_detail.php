<?php
/********************************************************************

    lnr_sg_form_03_pdf_detail.php

    Print Detail Form LNR-03.

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2018-01-05
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2018-01-05
    Version:        1.0.0

    Copyright (c) 2018, Tissot, All Rights Reserved.
*********************************************************************/


$units_of_measurement = array();
$units_of_measurement[1] = "km";
$units_of_measurement[2] = "miles";


//get sellout data
$sellout_data = array();
$profitability_data = array();


$sql = "select * from ln_basicdata_inr03 " . 
	   "left join posaddresses on posaddress_id = ln_basicdata_lnr03_posaddress_id_da " .
	   " left join addresses on address_id = posaddress_franchisee_id " . 
	   "left join places on place_id = posaddress_place_id " .
	   "left join posowner_types on posowner_type_id = posaddress_ownertype " . 
	   "left join postypes on postype_id = posaddress_store_postype " . 
       "where ln_basicdata_lnr03_posaddress_id_da > 0 " .
	   "  and ln_basicdata_lnr03_cer_version = " . $cer_version . 
	   "  and ln_basicdata_lnr03_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"] . 
	   "  and ln_basicdata_lnr03_cer_version = " . $cer_version .
	   " order by ln_basicdata_lnr03_distance, posowner_type_name, postype_name";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$posaddress = $row["place_name"] . ", " . $row["posaddress_address"];
	$type = $row["posowner_type_name"] . " " . $row["postype_name"];
	
	$pct = 0;
	if($row["posaddress_store_planned_closingdate"] != NULL 
		and $row["posaddress_store_planned_closingdate"] != '0000-00-00') {
		$pct = 1;
	}


	$distance = $row["ln_basicdata_lnr03_distance"];
	if($row["ln_basicdata_lnr03_distance_unit"] > 0)
	{
		$distance .= " " . $units_of_measurement[$row["ln_basicdata_lnr03_distance_unit"]];
	}

	$sellout_data[] = array("posname"=>$row["address_company"] . "/" . $row["posaddress_name"],
					"address"=>$posaddress, 
					"type"=>$type,
					"distance"=>$distance, 
					"watches"=>$row["ln_basicdata_lnr03_watches_units"], 
					"bijoux"=>$row["ln_basicdata_lnr03_bijoux_units"], 
					"grossales"=>round($row["ln_basicdata_lnr03_grossales"]/1000, 0),
					"ptc"=>$pct);


}


//get profitability data

$profitability_data = array();

$sql = "select * from ln_basicdata_inr03 " . 
	   "left join posaddresses on posaddress_id = ln_basicdata_lnr03_posaddress_id_pr " .
	   " left join addresses on address_id = posaddress_franchisee_id " . 
	   "left join places on place_id = posaddress_place_id " .
	   "left join posowner_types on posowner_type_id = posaddress_ownertype " . 
	   "left join postypes on postype_id = posaddress_store_postype " . 
       "where ln_basicdata_lnr03_posaddress_id_pr > 0 " . 
	   "  and ln_basicdata_lnr03_cer_version = " . $cer_version . 
	   "  and ln_basicdata_lnr03_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"] . 
	   "  and ln_basicdata_lnr03_cer_version = " . $cer_version .
	   " order by ln_basicdata_lnr03_distance, posowner_type_name, postype_name";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	
	//get lease expiration date
	$lease_end_date = '';
	$sql_l = "select poslease_enddate from posleases where poslease_posaddress = " . dbquote($row["ln_basicdata_lnr03_posaddress_id_pr"]) . " order by poslease_enddate DESC";

	
	$res_l = mysql_query($sql_l) or dberror($sql_l);
	if ($row_l = mysql_fetch_assoc($res_l))
	{
		$lease_end_date = to_system_date($row_l["poslease_enddate"]);
	}
	
	$profitability_data[] = array("posname"=>$row["address_company"] . "/" . $row["posaddress_name"], "watches"=>$row["ln_basicdata_lnr03_watches_units"], "bijoux"=>$row["ln_basicdata_lnr03_bijoux_units"], 
	"netsales"=>round($row["ln_basicdata_lnr03_net_sales"]/1000, 0), "grossmargin"=>round($row["ln_basicdata_lnr03_grossmargin"]/1000, 0), "expenses"=>round($row["ln_basicdata_lnr03_operating_expenses"]/1000, 0), "income01"=>round($row["ln_basicdata_lnr03_operating_income_excl"]/1000, 0), "wsmargin"=>$row["ln_basicdata_lnr03_wholsale_margin"], "income02"=>round($row["ln_basicdata_lnr03_operating_income_incl"]/1000, 0), "openingdate"=>to_system_date($row["posaddress_store_openingdate"]),
	"lease_expiration"=>$lease_end_date);




}

//set pdf parameters
$margin_top = 12;
$margin_left = 12;
$y = $margin_top;
$x = $margin_left+1;
$standard_h = 6;

$pdf->AddPage("L");
$globalPageNumber++;

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(51, 8, "Swatch Group", 1);

$pdf->SetFont("arialn", "B", 14);
$pdf->Cell(178, 8, "LEASE NEGOTIATION - ADDITIONAL ANALYSIS", 1, "", "C");


$pdf->SetFont("arialn", "", 10);
$pdf->SetFillColor(248,251,167);
if($ln_version == 0 and $ln_basicdata["ln_basicdata_locked"] == 0)
{
	$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
}
else
{
	$pdf->Cell(20, 8, to_system_date(substr($ln_basicdata["versiondate"], 0, 10)), 1, "", "C", true);
}
$pdf->Cell(21, 8, "LNR-03", 1, "", "C");

	
	$y = $pdf->GetY()+10;
	$y_at_start = $y - 1;

	
	
	// 5. Top 10 distribution analysis of current stores in the area
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(268, 6, "5. Top 10 distribution analysis of current stores in the area (" . $project["order_shop_address_company"] . ")", 1, "", "L");

		
	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(68, $standard_h, "Retailer Name / Boutique Name", 1, "", "L");
	$pdf->Cell(70, $standard_h, "Location (mall name, street name)", 1, "", "L");
	$pdf->Cell(20, $standard_h, "Status", 1, "", "L");
	$pdf->Cell(34, $standard_h, "Distance from prop. POS", 1, "", "R");
	$pdf->Cell(34, $standard_h, "Type", 1, "", "L");
	$pdf->Cell(17, $standard_h, "Units sold", 1, "", "R");
	$pdf->Cell(25, $standard_h, "Value sold (k". $currency_symbol. ")", 1, "", "R");
	
	
	
	
	
	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 8);
	
	$num_of_lines = 0;
	$i=1;
	foreach($sellout_data as $key=>$data)
	{
		if($i<14)
		{
			$pdf->Cell(68, $standard_h, $data["posname"], 1, "", "L", true);
			$pdf->Cell(70, $standard_h, $data["address"], 1, "", "L", true);

			if($data["ptc"] == 1)
			{
				$pdf->Cell(20, $standard_h, "to be closed" , 1, "", "L", true);
			}
			else
			{
				$pdf->Cell(20, $standard_h, "keep" , 1, "", "L", true);
			}


			$pdf->Cell(34, $standard_h, $data["distance"], 1, "", "R", true);
			$pdf->Cell(34, $standard_h, $data["type"], 1, "", "L", true);
			$pdf->Cell(17, $standard_h, $data["watches"] + $data["bijoux"], 1, "", "R", true);
			$pdf->Cell(25, $standard_h, number_format($data["grossales"], 0, '.', "'"), 1, "", "R", true);

			

			$y = $pdf->GetY()+$standard_h;
			$pdf->SetXY($margin_left+1,$y);
			$i++;
			$num_of_lines++;
		}	
	}
	
	

	//6. Profitability analysis of Corporate stores in the country (to be completed for Corporate store application only) (4)
	if($project['project_cost_type'] == 1)
	{
		if(count($sellout_data) + count($profitability_data) > 24) {
			
			//draw outer box
			$y = $pdf->GetY()-$margin_top - 8;
			$pdf->SetXY($margin_left,$y_at_start);
			$pdf->Cell(270, $y, "", 1);


			$pdf->AddPage("L");
			$globalPageNumber++;

			// Title first line
			$pdf->SetXY($margin_left,$margin_top);
			$pdf->SetFont("arialn", "I", 10);
			$pdf->Cell(51, 8, "The Swatch Group", 1);

			$pdf->SetFont("arialn", "B", 14);
			$pdf->Cell(179, 8, "LEASE NEGOTIATION - ADDITIONAL ANALYSIS", 1, "", "C");


			$pdf->SetFont("arialn", "", 10);
			$pdf->SetFillColor(248,251,167);
			if($ln_version == 0 and $ln_basicdata["ln_basicdata_locked"] == 0)
			{
				$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
			}
			else
			{
				$pdf->Cell(20, 8, to_system_date(substr($ln_basicdata["versiondate"], 0, 10)), 1, "", "C", true);
			}
			$pdf->Cell(20, 8, "LNR-03", 1, "", "C");

			$y = $pdf->GetY()+10;
			$pdf->SetXY($margin_left+1,$y);
				
		}
		else {
			$y = $pdf->GetY()+2;
			$pdf->SetXY($margin_left+1,$y);
		}

		
		
		$pdf->SetFont("arialn", "B", 11);
		$pdf->Cell(268, 6, "6. Profitability analysis of Corporate stores in the country", 1, "", "L");

		$y = $pdf->GetY()+6;
		$pdf->SetXY($margin_left+1,$y);

		$pdf->SetFont("arialn", "B", 8);
		$pdf->MultiCell(68, 1.8*$standard_h, "Retailer Name / Boutique Name", 1, "L", false);
		$pdf->SetXY($margin_left+69,$y);
		$pdf->MultiCell(17, 1.8*$standard_h, "Units total", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 1*17,$y);
		$pdf->MultiCell(17, 1.8*$standard_h, "Net sales", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 2*17,$y);
		$pdf->MultiCell(17, 1.8*$standard_h, "Gross margin", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 3*17,$y);
		$pdf->MultiCell(25, 1.8*$standard_h, "Indirect operating expenses", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 1*25 + 3*17,$y);
		$pdf->MultiCell(25, 1.8*$standard_h, "Operating income", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 2*25+ 3*17,$y);
		$pdf->MultiCell(25, 1.8*$standard_h, "Wholesale margin", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 3*25+ 3*17,$y);
		$pdf->MultiCell(25, 1.8*$standard_h, "Operating income incl. whol. Margin", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 4*25+ 3*17,$y);
		$pdf->MultiCell(25, 1.8*$standard_h, "Opening date", 1, "L", false);
		$pdf->SetXY($margin_left+69 + 5*25+ 3*17,$y);
		$pdf->MultiCell(24, 1.8*$standard_h, "Lease Expiration", 1, "L", false);
		

		$y = $pdf->GetY();
		$pdf->SetXY($margin_left+1,$y);
		$pdf->SetFont("arialn", "", 8);
		

		$i=1;
		$k=11;
		$num_of_lines = 0;
		if(count($profitability_data) > 10) {
			$k=24;
		}
		foreach($profitability_data as $key=>$data)
		{
			if($i<$k)
			{
				$pdf->Cell(68, $standard_h, $data["posname"], 1, "", "L", true);
				$pdf->Cell(17, $standard_h, $data["watches"] + $data["bijoux"], 1, "", "R", true);
				$pdf->Cell(17, $standard_h, number_format($data["netsales"], 0, '.', "'"), 1, "", "R", true);
				$pdf->Cell(17, $standard_h, number_format($data["grossmargin"], 0, '.', "'"), 1, "", "R", true);
				$pdf->Cell(25, $standard_h, number_format($data["expenses"], 0, '.', "'"), 1, "", "R", true);
				$pdf->Cell(25, $standard_h, number_format($data["income01"], 0, '.', "'"), 1, "", "R", true);
				$pdf->Cell(25, $standard_h, number_format($data["wsmargin"], 0, '.', "'") ."%", 1, "", "R", true);
				$pdf->Cell(25, $standard_h, number_format($data["income02"], 0, '.', "'"), 1, "", "R", true);
				$pdf->Cell(25, $standard_h, $data["openingdate"], 1, "", "R", true);
				$pdf->Cell(24, $standard_h, $data["lease_expiration"], 1, "", "R", true);

				$y = $pdf->GetY()+$standard_h;
				$pdf->SetXY($margin_left+1,$y);
				$i++;
				$num_of_lines++;
			}
		
		}
	}
	
	if($num_of_lines > 22 and count($profitability_data) > 23) {
		
		//draw outer box
		$y = $pdf->GetY()-$margin_top - 8;
		$pdf->SetXY($margin_left,$y_at_start);
		$pdf->Cell(270, $y, "", 1);

		//set pdf parameters
		$margin_top = 12;
		$margin_left = 12;
		$y = $margin_top;
		$x = $margin_left+1;
		$standard_h = 6;

		$pdf->AddPage("L");
		$globalPageNumber++;

		// Title first line
		$pdf->SetXY($margin_left,$margin_top);
		$pdf->SetFont("arialn", "I", 10);
		$pdf->Cell(51, 8, "The Swatch Group", 1);

		$pdf->SetFont("arialn", "B", 14);
		$pdf->Cell(179, 8, "LEASE NEGOTIATION - ADDITIONAL ANALYSIS", 1, "", "C");


		$pdf->SetFont("arialn", "", 10);
		$pdf->SetFillColor(248,251,167);
		if($ln_version == 0 and $ln_basicdata["ln_basicdata_locked"] == 0)
		{
			$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
		}
		else
		{
			$pdf->Cell(20, 8, to_system_date($ln_basicdata["versiondate"]), 1, "", "C", true);
		}
		$pdf->Cell(20, 8, "LNR-03", 1, "", "C");

		$y = $pdf->GetY()+10;
		$pdf->SetXY($margin_left+1,$y);

		$pdf->SetFont("arialn", "B", 11);
		$pdf->Cell(268, 6, "6. Profitability analysis of Corporate stores in the country (to be completed for Corporate store application only) (4)", 1, "", "L");

		$y = $pdf->GetY()+6;
		$pdf->SetXY($margin_left+1,$y);

		$pdf->SetFont("arialn", "B", 8);
		$pdf->MultiCell(68, 1.8*$standard_h, "Retailer Name / Boutique Name", 1, "L", false);
		$pdf->SetXY($margin_left+69,$y);
		$pdf->MultiCell(17, 1.8*$standard_h, "Units total", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 1*17,$y);
		$pdf->MultiCell(17, 1.8*$standard_h, "Net sales", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 2*17,$y);
		$pdf->MultiCell(17, 1.8*$standard_h, "Gross margin", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 3*17,$y);
		$pdf->MultiCell(25, 1.8*$standard_h, "Indirect operating expenses", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 1*25 + 3*17,$y);
		$pdf->MultiCell(25, 1.8*$standard_h, "Operating income", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 2*25+ 3*17,$y);
		$pdf->MultiCell(25, 1.8*$standard_h, "Wholesale margin (%)", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 3*25+ 3*17,$y);
		$pdf->MultiCell(25, 1.8*$standard_h, "Operating income incl. whol. Margin", 1, "R", false);
		$pdf->SetXY($margin_left+69 + 4*25+ 3*17,$y);
		$pdf->MultiCell(25, 1.8*$standard_h, "Opening date", 1, "L", false);
		$pdf->SetXY($margin_left+69 + 5*25+ 3*17,$y);
		$pdf->MultiCell(24, 1.8*$standard_h, "Lease Expiration", 1, "L", false);
		

		$y = $pdf->GetY();
		$pdf->SetXY($margin_left+1,$y);
		$pdf->SetFont("arialn", "", 8);

		$i=1;
		$k=48;
		
		foreach($profitability_data as $key=>$data)
		{
			if($i>23 and $i<$k)
			{
				$pdf->Cell(68, $standard_h, $data["posname"], 1, "", "L", true);
				$pdf->Cell(17, $standard_h, $data["watches"] + $data["bijoux"], 1, "", "R", true);
				$pdf->Cell(17, $standard_h, number_format($data["netsales"], 0, '.', "'"), 1, "", "R", true);
				$pdf->Cell(17, $standard_h, number_format($data["grossmargin"], 0, '.', "'"), 1, "", "R", true);
				$pdf->Cell(25, $standard_h, number_format($data["expenses"], 0, '.', "'"), 1, "", "R", true);
				$pdf->Cell(25, $standard_h, number_format($data["income01"], 0, '.', "'"), 1, "", "R", true);
				$pdf->Cell(25, $standard_h, number_format($data["wsmargin"], 0, '.', "'") ."%", 1, "", "R", true);
				$pdf->Cell(25, $standard_h, number_format($data["income02"], 0, '.', "'"), 1, "", "R", true);
				$pdf->Cell(25, $standard_h, $data["openingdate"], 1, "", "R", true);
				$pdf->Cell(24, $standard_h, $data["lease_expiration"], 1, "", "R", true);

				$y = $pdf->GetY()+$standard_h;
				$pdf->SetXY($margin_left+1,$y);
				
			}
			$i++;
		
		}

		//draw outer box
		$y = $pdf->GetY()-$margin_top - 8;
		$pdf->SetXY($margin_left,$y_at_start);
		$pdf->Cell(270, $y, "", 1);

	}

	if($num_of_lines > 22) {
		$pdf->AddPage("L");
		$globalPageNumber++;
		$y = $pdf->GetY()+10;
		$y_at_start = $y - 1;
	}
	else {
		$y = $pdf->GetY()+2;
	}
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "B", 11);
	
	if($project['project_cost_type'] == 1)
	{
		//7. Conditions of other SG brands in same mall / street
		
		$pdf->Cell(268, 6, "7. Conditions of other SG brands in same mall / street (" . $project["order_shop_address_company"] . ")", 1, "", "L");
	}
	else {
		
		//6. Conditions of other SG brands in same mall / street		
		$pdf->Cell(268, 6, "6. Conditions of other SG brands in same mall / street (" . $project["order_shop_address_company"] . ")", 1, "", "L");
	}
	

			
	$y = $pdf->GetY()+6;
	$pdf->SetXY($x,$y);

	$pdf->SetFont("arialn", "B", 7);
	
	$pdf->MultiCell(34,8,"Brand",1, "L", false, 0);
	$pdf->MultiCell(40,8,"Type",1, "L", false, 0);
	$pdf->MultiCell(16,8,"Contract year signed",1, "L", false, 0);
	$pdf->MultiCell(16,8,"Gross surface (sqm)",1, "R", false, 0);
	$pdf->MultiCell(75,8,"Rental conditions",1, "L", false, 0);
	$pdf->MultiCell(11,8,"Sales year",1, "L", false, 0);
	$pdf->MultiCell(16,8,"Gross sales"  . "\n" . "in K" . $currency_symbol,1, "R", false, 0);
	$pdf->MultiCell(60,8,"Notes",1, "L", false, 1);
	
	$pdf->SetFont("arialn", "", 7);
	
	//get data
	$has_data = false;
	$sql = "select * from ln_basicdata_lnr03_brands " . 
		   " left join postypes on postype_id = ln_basicdata_lnr03_brand_postype_id " .
		   " left join posowner_types on posowner_type_id = ln_basicdata_lnr03_brand_legaltype_id " .
		   " left join sg_brands on sg_brand_id = ln_basicdata_lnr03_brand_sg_brand_id " . 
		   " where ln_basicdata_lnr03_brand_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"] . 
		   "  and ln_basicdata_lnr03_brand_cer_version = " . $ln_version .
		   " order by sg_brand_name";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$has_data = true;

		$y = $pdf->GetY();
		$pdf->SetXY($margin_left+1,$y);

		$amount = round($row["ln_basicdata_lnr03_brand_sales_amount"]/1000, 0);
		$pdf->MultiCell(34,0,$row["sg_brand_name"],1, "L", true, 0);
		$pdf->MultiCell(40,0,$row["posowner_type_name"] . " ". $row["postype_name"],1, "L", true, 0);
		$pdf->MultiCell(16,0,$row["ln_basicdata_lnr03_brand_contract_year"],1, "L", true, 0);
		$pdf->MultiCell(16,0,$row["ln_basicdata_lnr03_brand_gross_surface"],1, "R", true, 0);

		$x = $pdf->getX()+75;
		$pdf->MultiCell(75,0,$row["ln_basicdata_lnr03_brand_rental_conidtions"],1, "L", true, 1);
		$y1 = $pdf->GetY();
		


		$pdf->setXY($x, $y);

		$pdf->MultiCell(11,0,$row["ln_basicdata_lnr03_brand_sales_year"],1, "L", true, 0);
		$pdf->MultiCell(16,0,$amount,1, "R", true, 0);
		$pdf->MultiCell(60,0,$row["ln_basicdata_lnr03_brand_notes"],1, "L", true, 1);
		$y2 = $pdf->GetY();
		
		$y = $pdf->GetY();
		if($y1 > $y){$y = $y1;}
		if($y2 > $y){$y = $y2;}
			
		$pdf->SetXY($x,$y);

		
		
	}

	if($has_data == false)
	{	
		$y = $pdf->GetY()+1;
		$pdf->SetXY($x,$y);
		$pdf->MultiCell(190,0,"There are no other Swatch Group Brands in the same mall/street.",0, "L", false, 0);
		$y = $pdf->GetY()+3;
		$pdf->SetXY($x,$y);
	
	}



	//draw outer box
	$y = $pdf->GetY()-$margin_top - 8;
	$pdf->SetXY($margin_left,$y_at_start);
	$pdf->Cell(270, $y, "", 1);

		

	
?>