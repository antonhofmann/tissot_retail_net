<?php
/********************************************************************

    sg_brand.php

    Creation and mutation of swatch group brands.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-10-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-10-20
    Version:        1.0.0

    Copyright (c) 2016 Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

/********************************************************************
    build form 
*********************************************************************/
$form = new Form("sg_brands", "Swatch Group Brands");


$form->add_edit("sg_brand_name", "Type Name", NOTNULL);
$form->add_checkbox("sg_brand_active", "In use", "", 0, "Active");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

if($form->button(FORM_BUTTON_SAVE)) {
	redirect("sg_brands.php");
}

$page = new Page("sg_brands");

$page->header();
$page->title("Swatch Group Brand");
$form->render();
$page->footer();

?>
