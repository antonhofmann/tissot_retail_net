<?php
/********************************************************************

    cer_application_general.php

    Application Form: general information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/
if(param("pid") > 0)
{
	$result = check_cer_summary(param("pid"));
	$result = check_basic_data(param("pid"));
}

$result = check_expenses(param("pid"), $cer_basicdata["cer_basicdata_firstyear"], $cer_basicdata["cer_basicdata_lastyear"]);

$posdata = get_pos_data($project["project_order"]);


//neighbourhood
if($project["pipeline"] == 0)
{
	$pos_order_sql = "select * from posorders where posorder_order = " . $project["order_id"];
}
elseif($project["pipeline"] == 1)
{
	$pos_order_sql = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
}

$pos_type = 0;
$neighbourhoods = array();
$neighbourhoods_business_types = array();
$neighbourhoods_price_ranges = array();
$res = mysql_query($pos_order_sql) or dberror($pos_order_sql);
if ($row = mysql_fetch_assoc($res))
{
	$neighbourhoods["Shop on Left Side"] = $row["posorder_neighbour_left"];
	$neighbourhoods["Shop on Right Side"] = $row["posorder_neighbour_right"];
	$neighbourhoods["Shop Across Left Side"] = $row["posorder_neighbour_acrleft"];
	$neighbourhoods["Shop Across Right Side"] = $row["posorder_neighbour_acrright"];
	$neighbourhoods["Other Brands in Area"] = $row["posorder_neighbour_brands"];

	$neighbourhoods_business_types["Shop on Left Side"] = $row["posorder_neighbour_left_business_type"];
	$neighbourhoods_business_types["Shop on Right Side"] = $row["posorder_neighbour_right_business_type"];
	$neighbourhoods_business_types["Shop Across Left Side"] = $row["posorder_neighbour_acrleft_business_type"];
	$neighbourhoods_business_types["Shop Across Right Side"] = $row["posorder_neighbour_acrright_business_type"];

	$neighbourhoods_price_ranges["Shop on Left Side"] = $row["posorder_neighbour_left_price_range"];
	$neighbourhoods_price_ranges["Shop on Right Side"] = $row["posorder_neighbour_right_price_range"];
	$neighbourhoods_price_ranges["Shop Across Left Side"] = $row["posorder_neighbour_acrleft_price_range"];
	$neighbourhoods_price_ranges["Shop Across Right Side"] = $row["posorder_neighbour_acrright_price_range"];
	
	$pos_type = $row["posorder_postype"];
}	



param("id", get_id_of_cer_summary(param("pid")));

$currency = get_cer_currency(param("pid"));

$months = array();
for($i=1;$i<13;$i++)
{
	$months[$i] = $i;
}

$years = array();
$s = date("Y")-5;
$l = $s+32;
for($i=$s;$i<$l;$i++)
{
	$years[$i] = $i;
}

//get RRMA: HQ Project Leaders
$sql_rrmas = "select user_id, concat(user_name, ' ', user_firstname) as username " . 
             "from user_roles " .
	         "left join users on user_id = user_role_user " . 
	         "where user_role_role = 19 " .
			 "order by user_name, user_firstname";


//ckeck if the business plan period has been filled in
$business_plan_period_filled = 0;
if($cer_basicdata["cer_basicdata_firstyear"] 
   and $cer_basicdata["cer_basicdata_lastyear"] 
   and $cer_basicdata["cer_basicdata_firstmonth"]
   and $cer_basicdata["cer_basicdata_lastmonth"])
{
	$business_plan_period_filled = 1;
}

if(has_access("can_edit_catalog")) {
	//$business_plan_period_filled = 0;
}

//get business types and price ranges

$businesstypes = array();
$sql = "select businesstype_id, businesstype_text " . 
	   "from businesstypes " . 
	   " order by businesstype_text";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$businesstypes[$row["businesstype_id"]]	= $row["businesstype_text"];
}
$businesstypes["---"]	= "---------------------------------";
$businesstypes[999999]	= "Other not listed above";

$priceranges = array();
$sql = "select pricerange_id, pricerange_text " . 
	   "from priceranges " . 
	   " order by pricerange_text";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$priceranges[$row["pricerange_id"]]	= $row["pricerange_text"];
}

/********************************************************************
    copy from drafts
*********************************************************************/

//check if drafts from the country are available
$project = get_project(param("pid"));
$country = $project["order_shop_address_country"];

$posleases = get_pos_leasedata($posdata["posaddress_id"], $project["project_order"]);

if(has_access("has_access_to_drafts_of_his_country")) {

	$sql = "select count(cer_basicdata_id) as num_recs from cer_drafts ";
	$list_filter = 'cer_basicdata_user_id = ' . user_id();

	
	//get the team members
	$user_list = '';
	$sql = 'select address_id from addresses ' .
		   'left join users on user_address = address_id ' . 
		   'where user_id = ' . user_id();

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$sql = 'select user_id from users ' .
			   'where user_address = ' . $row["address_id"] . 
			   ' and user_active = 1';

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$user_list .= $row['user_id'] . ',';
		}

		$user_list = substr($user_list, 0, strlen($user_list)-1);

		$sql = "select count(cer_basicdata_id) as num_recs from cer_drafts ";
		$list_filter = ' where cer_basicdata_user_id IN (' . $user_list . ')';
	}
	
	
}
else
{
	$sql = "select count(cer_basicdata_id) as num_recs  from cer_drafts ";
	$list_filter = '';
}

if($list_filter) {
	$list_filter .= ' and cer_basicdata_country = ' . $country;
}
else
{
	$list_filter = ' where cer_basicdata_country = ' . $country;
}

$sql_darfts = $sql . $list_filter;
$res = mysql_query($sql_darfts) or dberror($sql_darfts);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) {
	$business_plan_period_filled = 1;
}


//$business_plan_period_filled = 0;
if(has_access("has_access_to_cer_drafts") and $business_plan_period_filled == 0)
{

	$copy_form = new Form("cer_summary", "cer_summary");

	$copy_form->add_comment("Since you have not yet set a businessplan period you may want to copy data from an existing business plan draft.");
	$copy_form->add_hidden("pid", param("pid"));
	$copy_form->add_button("copy_from_draft", '<span class="error">Click here</span> in case you like to copy data from a Business Plan Draft.');
}


//check if business plan can be edited
require_once("include/check_if_business_plan_is_editable.php");


/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_summary", "cer_summary");

include("include/project_head.php");

$form->add_hidden("pid", param("pid"));

$form->add_section(" ");

$form->add_section(" ");

if(($can_edit_business_plan == true 
	and $project["order_actual_order_state_code"] < '890' 
	and $cer_basicdata["cer_basicdata_cer_locked"] == 0 
	and has_access("has_access_to_his_cer") 
	and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") 
	and $project["project_state"] != 2))
{

	if($project["project_projectkind"] != 8)
	{
		$form->add_section("Business Plan Period");
	}
	else
	{
		$form->add_section("PopUp Period");
	}
	$form->add_list("basicdata_firstyear", "First Year*", $years, NOTNULL, $cer_basicdata["cer_basicdata_firstyear"]);
	$form->add_list("basicdata_firstmonth", "Month of First Year*", $months, NOTNULL, $cer_basicdata["cer_basicdata_firstmonth"]);
	$form->add_list("basicdata_lastyear", "Last Year*", $years, NOTNULL, $cer_basicdata["cer_basicdata_lastyear"]);
	$form->add_list("basicdata_lastmonth", "Month of Last Year*", $months, NOTNULL, $cer_basicdata["cer_basicdata_lastmonth"]);
	
	if(has_access("can_edit_catalog"))
	{
		$form->add_list("basicdata_firstyear", "First Year", $years, 0, $cer_basicdata["cer_basicdata_firstyear"]);
		$form->add_list("basicdata_firstmonth", "Month of First Year", $months, 0, $cer_basicdata["cer_basicdata_firstmonth"]);
		$form->add_list("basicdata_lastyear", "Last Year", $years, 0, $cer_basicdata["cer_basicdata_lastyear"]);
		$form->add_list("basicdata_lastmonth", "Month of Last Year", $months, 0, $cer_basicdata["cer_basicdata_lastmonth"]);
	}
	else
	{
		$form->add_list("basicdata_firstyear", "First Year*", $years, NOTNULL, $cer_basicdata["cer_basicdata_firstyear"]);
		$form->add_list("basicdata_firstmonth", "Month of First Year*", $months, NOTNULL, $cer_basicdata["cer_basicdata_firstmonth"]);
		$form->add_list("basicdata_lastyear", "Last Year*", $years, NOTNULL, $cer_basicdata["cer_basicdata_lastyear"]);
		$form->add_list("basicdata_lastmonth", "Month of Last Year*", $months, NOTNULL, $cer_basicdata["cer_basicdata_lastmonth"]);			
	}

	if($project["project_projectkind"] != 8)
	{
		$form->add_label("bp_duration", "Business Plan Duration", 0, $business_plan_period);
	}

	
	$form->add_label("currency", "Client's Currency", 0, $currency["symbol"]);
		
	if($project["project_projectkind"] != 8)
	{
	
		$form->add_section("Neighbourhood");		

		$form->add_edit("posorder_neighbour_left", "Shop on Left Side*", 0, $neighbourhoods["Shop on Left Side"]);

		$form->add_list("posorder_neighbour_left_business_type", "Business Type Shop on Left Side*", $businesstypes, 0, $neighbourhoods_business_types["Shop on Left Side"]);
		if(param("posorder_neighbour_left_business_type") == 999999)
		{
			$form->add_edit("posorder_neighbour_left_business_type_new", "--- Business Type*", 0, "", TYPE_CHAR, 50);
		}
		else
		{
			$form->add_hidden("posorder_neighbour_left_business_type_new");
		}
		
		$form->add_list("posorder_neighbour_left_price_range", "Price Range Shop on Left Side*", $priceranges, 0, $neighbourhoods_price_ranges["Shop on Left Side"]);

		$form->add_edit("posorder_neighbour_right", "Shop on Right Side*", 0, $neighbourhoods["Shop on Right Side"]);

		$form->add_list("posorder_neighbour_right_business_type", "Business Type Shop on Right Side*", $businesstypes, 0, $neighbourhoods_business_types["Shop on Right Side"]);

		if(param("posorder_neighbour_right_business_type") == 999999)
		{
			$form->add_edit("posorder_neighbour_right_business_type_new", "--- Business Type*", 0, "", TYPE_CHAR, 50);
		}
		else
		{
			$form->add_hidden("posorder_neighbour_right_business_type_new");
		}

		$form->add_list("posorder_neighbour_right_price_range", "Price Range Shop on Right Side*", $priceranges, 0, $neighbourhoods_price_ranges["Shop on Right Side"]);

		$form->add_edit("posorder_neighbour_acrleft", "Shop Across Left Side*", 0, $neighbourhoods["Shop Across Left Side"]);
		$form->add_list("posorder_neighbour_acrleft_business_type", "Business Type  Across Left Side*", $businesstypes, 0, $neighbourhoods_business_types["Shop Across Left Side"]);

		if(param("posorder_neighbour_acrleft_business_type") == 999999)
		{
			$form->add_edit("posorder_neighbour_acrleft_business_type_new", "--- Business Type*", 0, "", TYPE_CHAR, 50);
		}
		else
		{
			$form->add_hidden("posorder_neighbour_acrleft_business_type_new");
		}

		$form->add_list("posorder_neighbour_acrleft_price_range", "Price Range Shop  Across Left Side*", $priceranges, 0, $neighbourhoods_price_ranges["Shop Across Left Side"]);

		$form->add_edit("posorder_neighbour_acrright", "Shop Across Right Side*", 0, $neighbourhoods["Shop Across Right Side"]);

		$form->add_list("posorder_neighbour_acrright_business_type", "Business Type Shop Across Right Side*", $businesstypes, 0, $neighbourhoods_business_types["Shop Across Right Side"]);

		if(param("posorder_neighbour_acrright_business_type") == 999999)
		{
			$form->add_edit("posorder_neighbour_acrright_business_type_new", "--- Business Type*", 0, "", TYPE_CHAR, 50);
		}
		else
		{
			$form->add_hidden("posorder_neighbour_acrright_business_type_new");
		}

		$form->add_list("posorder_neighbour_acrright_price_range", "Price Range Shop Across Right Side*", $priceranges, 0, $neighbourhoods_price_ranges["Shop Across Right Side"]);
		
		$form->add_comment("Please indicate <strong>brand name, business type and price range</strong>.");
		$form->add_multiline("posorder_neighbour_brands", "Other Brands in Area", 4, 0, $neighbourhoods["Other Brands in Area"]);
	}

	$form->add_button(FORM_BUTTON_SAVE, "Save Data");
}
else
{

	if($project["project_projectkind"] != 8)
	{
		$form->add_section("PopUp Period");
	}
	else
	{
		$form->add_section("Business Plan Period");
	}

	$tmp = $cer_basicdata["cer_basicdata_firstyear"] . "-" . $cer_basicdata["cer_basicdata_firstmonth"] . " / " .
	$cer_basicdata["cer_basicdata_lastyear"] . "-" . $cer_basicdata["cer_basicdata_lastmonth"] . " / " . $business_plan_period;
	$form->add_label("business_plan_period", "From / To / Duration", 0, $tmp);

	if(count($posleases) > 0)
	{
		
		$rental_duration = "";
		if($posleases["poslease_startdate"] != NULL 
			and $posleases["poslease_startdate"] != '0000-00-00' 
			and $posleases["poslease_enddate"] != NULL
			and $posleases["poslease_enddate"] != '0000-00-00')
		{
			$diff = abs(strtotime($posleases["poslease_enddate"]) - strtotime($posleases["poslease_startdate"]));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

			if($months == 12)
			{
				$months = 0;
				$years++;
			}

			$rental_duration = $years . " years and " . $months . " months";


		}
		
	}


	
	if($project["project_projectkind"] != 8)
	{
		$form->add_section("Neighbourhood");
		$form->add_label("posorder_neighbour_left", "Shop on Left Side", 0, $neighbourhoods["Shop on Left Side"]);
		$form->add_lookup("neighbour_left_business_type", "Business Type Shop on Left Side", "businesstypes", "businesstype_text", 0, $neighbourhoods_business_types["Shop on Left Side"]);
		$form->add_lookup("neighbour_left_price_range", "Price Range Shop on Left Side", "priceranges", "pricerange_text", 0, $neighbourhoods_price_ranges["Shop on Left Side"]);


		
		$form->add_label("posorder_neighbour_right", "Shop on Right Side", 0, $neighbourhoods["Shop on Right Side"]);
		$form->add_lookup("neighbour_right_business_type", "Business Type Shop on Right Side", "businesstypes", "businesstype_text", 0, $neighbourhoods_business_types["Shop on Right Side"]);
		$form->add_lookup("neighbour_right_price_range", "Price Range Shop on Right Side", "priceranges", "pricerange_text", 0, $neighbourhoods_price_ranges["Shop on Right Side"]);


		$form->add_label("posorder_neighbour_acrleft", "Shop Across Left Side", 0, $neighbourhoods["Shop Across Left Side"]);
		$form->add_lookup("neighbour_acrleft_business_type", "Business Type Shop Across Left Side", "businesstypes", "businesstype_text", 0, $neighbourhoods_business_types["Shop Across Left Side"]);
		$form->add_lookup("neighbour_acrleft_price_range", "Price Range Shop Across Left Side", "priceranges", "pricerange_text", 0, $neighbourhoods_price_ranges["Shop Across Left Side"]);



		$form->add_label("posorder_neighbour_acrright", "Shop Across Right Side", 0, $neighbourhoods["Shop Across Right Side"]);
		$form->add_lookup("neighbour_acrright_business_type", "Business Type Shop Across Right Side", "businesstypes", "businesstype_text", 0, $neighbourhoods_business_types["Shop Across Right Side"]);
		$form->add_lookup("neighbour_acrright_price_range", "Price Range Shop Across Right Side", "priceranges", "pricerange_text", 0, $neighbourhoods_price_ranges["Shop Across Right Side"]);
		$form->add_label("posorder_neighbour_brands", "Other Brands in Area", 0, $neighbourhoods["Other Brands in Area"]);

	}

}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
if(has_access("has_access_to_cer_drafts") and $business_plan_period_filled == 0)
{
	$copy_form->populate();
	$copy_form->process();

	
	if($form->button("copy_from_draft"))
	{
		$link = 'cer_application_general_copy_draft.php?pid=' . param("pid") . '&p=' . $posorder_present . '&ft=' . $form_type;
		redirect($link);
	}
}

$form->populate();
$form->process();



if($form->button(FORM_BUTTON_SAVE))
{
	
	$form->add_validation("{basicdata_firstyear} <= {basicdata_lastyear}", "Hey, the last year must be equal or greater than the first year!");

	
	if($project["project_projectkind"] != 8)
	{
		if($form->value("posorder_neighbour_left_business_type") == 999999)
		{
			$form->add_validation("{posorder_neighbour_left_business_type_new} != ''", "Please indicate the business type for the shop on the left side.");
		}

		if($form->value("posorder_neighbour_right_business_type") == 999999)
		{
			$form->add_validation("{posorder_neighbour_right_business_type_new} != ''", "Please indicate the business type for the shop on the right side.");
		}

		if($form->value("posorder_neighbour_acrleft_business_type") == 999999)
		{
			$form->add_validation("{posorder_neighbour_acrleft_business_type_new} != ''", "Please indicate the business type for the shop across left.");
		}

		if($form->value("posorder_neighbour_acrright_business_type") == 999999)
		{
			$form->add_validation("{posorder_neighbour_acrright_business_type_new} != ''", "Please indicate the business type for the shop across right.");
		}
	}

	
	if($form->validate())
	{
		//update cer_basic_data
		$fields = array();
    
		$value = dbquote($form->value("basicdata_firstyear"));
		$fields[] = "cer_basicdata_firstyear = " . $value;

		$value = dbquote($form->value("basicdata_firstmonth"));
		$fields[] = "cer_basicdata_firstmonth = " . $value;

		$value = dbquote($form->value("basicdata_lastyear"));
		$fields[] = "cer_basicdata_lastyear = " . $value;

		$value = dbquote($form->value("basicdata_lastmonth"));
		$fields[] = "cer_basicdata_lastmonth = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_basicdata set " . join(", ", $fields) . 
			" where cer_basicdata_project = " . param("pid") . 
			" and cer_basicdata_version = 0 ";
		mysql_query($sql) or dberror($sql);
		
		
		//update posorders
		if($project["project_projectkind"] != 8)
		{
			$new_business_type_1 = $form->value("posorder_neighbour_left_business_type");
			$new_business_type_2 = $form->value("posorder_neighbour_right_business_type");
			$new_business_type_3 = $form->value("posorder_neighbour_acrleft_business_type");
			$new_business_type_4 = $form->value("posorder_neighbour_acrright_business_type");

			if($form->value("posorder_neighbour_left_business_type_new"))
			{

				//check if businesstype is already there
				$sql = "select businesstype_id " . 
					   " from businesstypes " . 
					   " where businesstype_text = " . dbquote(trim($form->value("posorder_neighbour_left_business_type_new")));
				

				$res = mysql_query($sql);
				
				if($row = mysql_fetch_assoc($res))
				{
					$new_business_type_1 = $row["businesstype_id"];
				}
				else
				{
					$fields = array();
					$values = array();

					$fields[] = "businesstype_text";
					$values[] = dbquote(trim($form->value("posorder_neighbour_left_business_type_new")));

					$fields[] = "date_created";
					$values[] = "current_timestamp";

					$fields[] = "user_created";
					$values[] = dbquote($_SESSION["user_login"]);

					$sql = "insert into businesstypes (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);

					$business_type_id =  mysql_insert_id();

					$new_business_type_1 = mysql_insert_id();
				}
			}

			if($form->value("posorder_neighbour_right_business_type_new"))
			{

				//check if businesstype is already there
				$sql = "select businesstype_id " . 
					   " from businesstypes " . 
					   " where businesstype_text = " . dbquote(trim($form->value("posorder_neighbour_right_business_type_new")));
				

				$res = mysql_query($sql);
				
				if($row = mysql_fetch_assoc($res))
				{
					$new_business_type_2 = $row["businesstype_id"];
				}
				else
				{
					$fields = array();
					$values = array();

					$fields[] = "businesstype_text";
					$values[] = dbquote(trim($form->value("posorder_neighbour_right_business_type_new")));

					$fields[] = "date_created";
					$values[] = "current_timestamp";

					$fields[] = "user_created";
					$values[] = dbquote($_SESSION["user_login"]);

					$sql = "insert into businesstypes (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);

					$business_type_id =  mysql_insert_id();

					$new_business_type_2 = mysql_insert_id();
				}
			}

			if($form->value("posorder_neighbour_acrleft_business_type_new"))
			{

				//check if businesstype is already there
				$sql = "select businesstype_id " . 
					   " from businesstypes " . 
					   " where businesstype_text = " . dbquote(trim($form->value("posorder_neighbour_acrleft_business_type_new")));
				

				$res = mysql_query($sql);
				
				if($row = mysql_fetch_assoc($res))
				{
					$new_business_type_3 = $row["businesstype_id"];
				}
				else
				{
					$fields = array();
					$values = array();

					$fields[] = "businesstype_text";
					$values[] = dbquote(trim($form->value("posorder_neighbour_acrleft_business_type_new")));

					$fields[] = "date_created";
					$values[] = "current_timestamp";

					$fields[] = "user_created";
					$values[] = dbquote($_SESSION["user_login"]);

					$sql = "insert into businesstypes (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);

					$business_type_id =  mysql_insert_id();

					$new_business_type_3 = mysql_insert_id();
				}
			}

			if($form->value("posorder_neighbour_acrright_business_type_new"))
			{

				//check if businesstype is already there
				$sql = "select businesstype_id " . 
					   " from businesstypes " . 
					   " where businesstype_text = " . dbquote(trim($form->value("posorder_neighbour_acrright_business_type_new")));
				

				$res = mysql_query($sql);
				
				if($row = mysql_fetch_assoc($res))
				{
					$new_business_type_4 = $row["businesstype_id"];
				}
				else
				{
					$fields = array();
					$values = array();

					$fields[] = "businesstype_text";
					$values[] = dbquote(trim($form->value("posorder_neighbour_acrright_business_type_new")));

					$fields[] = "date_created";
					$values[] = "current_timestamp";

					$fields[] = "user_created";
					$values[] = dbquote($_SESSION["user_login"]);

					$sql = "insert into businesstypes (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);

					$business_type_id =  mysql_insert_id();

					$new_business_type_4 = mysql_insert_id();
				}
			}
			
			$fields = array();
    
			$value = dbquote($form->value("posorder_neighbour_left"));
			$fields[] = "posorder_neighbour_left = " . $value;

			$value = dbquote($new_business_type_1);
			$fields[] = "posorder_neighbour_left_business_type = " . $value;

			$value = dbquote($form->value("posorder_neighbour_left_price_range"));
			$fields[] = "posorder_neighbour_left_price_range = " . $value;
	
			$value = dbquote($form->value("posorder_neighbour_right"));
			$fields[] = "posorder_neighbour_right = " . $value;

			$value = dbquote($new_business_type_2);
			$fields[] = "posorder_neighbour_right_business_type = " . $value;

			$value = dbquote($form->value("posorder_neighbour_right_price_range"));
			$fields[] = "posorder_neighbour_right_price_range = " . $value;
	
			$value = dbquote($form->value("posorder_neighbour_acrleft"));
			$fields[] = "posorder_neighbour_acrleft = " . $value;

			$value = dbquote($new_business_type_3);
			$fields[] = "posorder_neighbour_acrleft_business_type = " . $value;

			$value = dbquote($form->value("posorder_neighbour_acrleft_price_range"));
			$fields[] = "posorder_neighbour_acrleft_price_range = " . $value;
	
			$value = dbquote($form->value("posorder_neighbour_acrright"));
			$fields[] = "posorder_neighbour_acrright = " . $value;

			$value = dbquote($new_business_type_4);
			$fields[] = "posorder_neighbour_acrright_business_type = " . $value;

			$value = dbquote($form->value("posorder_neighbour_acrright_price_range"));
			$fields[] = "posorder_neighbour_acrright_price_range = " . $value;
	
			$value = dbquote($form->value("posorder_neighbour_brands"));
			$fields[] = "posorder_neighbour_brands = " . $value;
	
			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;
	
			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
			
			
			if($project["pipeline"] == 0)
			{
	   			$sql = "update posorders set " . join(", ", $fields) . " where posorder_order = " . $project["order_id"];
			}
			else
			{
				$sql = "update posorderspipeline set " . join(", ", $fields) . " where posorder_order = " . $project["order_id"];
			}
			mysql_query($sql) or dberror($sql);


		}

		//check if expense records exist
		$result = check_expenses(param("pid"), $form->value("basicdata_firstyear"), $form->value("basicdata_lastyear"));
		$result = check_paymentterms(param("pid"), $form->value("basicdata_firstyear"), $form->value("basicdata_lastyear"));

		//check if cer_revenue_records exist
		$result = check_revenues(param("pid"), $form->value("basicdata_firstyear"), $form->value("basicdata_lastyear"));
		$result = check_stocks(param("pid"), $form->value("basicdata_firstyear"), $form->value("basicdata_lastyear"));

		$link = "cer_application_general.php?pid=" . param("pid");
		redirect($link);
	}

}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();


if($project["project_projectkind"] == 8)
{
	
	if($form_type == "INR03")
	{
		$page->title("INR-03 - Retail Furniture in Third-party Store: General Information");
	}
	elseif($form_type == "AF")
	{
		$page->title("LNR for PopUps: General Information");
	}
	else
	{
		$page->title("LNR/CER for PopUps: General Information");
	}
}
else
{
	if($form_type == "INR03")
	{
		$page->title("INR-03 - Retail Furniture in Third-party Store: General Information");
	}
	else
	{
		$page->title("Business Plan: General Information");
	}
}
if($business_plan_period_filled == 1)
{
	require_once("include/tabs2016.php");
}
else
{
	
}

if(has_access("has_access_to_cer_drafts") and $business_plan_period_filled == 0)
{
	$copy_form->render();
	echo '<p>&nbsp;</p>';
}

$form->render();


require "include/footer_scripts.php";
$page->footer();

?>