<?PHP
/********************************************************************
    check if Busines Plan Data can be edited
*********************************************************************/

$can_edit_business_plan = true;
//cer is locked
if($cer_basicdata['cer_basicdata_cer_locked'] == 1)
{
	$can_edit_business_plan = false;
	//echo "1";
}

//if cer approval milestone has a date (cer/af was approved)
$milestone = get_project_milestone($cer_basicdata["cer_basicdata_project"], 12);
if(count($milestone) > 0 
   and ($milestone['project_milestone_date'] != NULL 
   and $milestone['project_milestone_date'] != '0000-00-00')
   )
{
	$can_edit_business_plan = false;
	//echo "2";
}

$milestone = get_project_milestone($cer_basicdata["cer_basicdata_project"], 21);
if(count($milestone) > 0 
   and ($milestone['project_milestone_date'] != NULL 
   and $milestone['project_milestone_date'] != '0000-00-00')
   )
{
	$can_edit_business_plan = false;
	//echo "3";
}


if($cer_basicdata['cer_basicdata_cer_locked'] == 0)
{
	//cer was submitted and not rejected
	if($cer_basicdata["cer_basicdata_submitted"] != NULL 
		and substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) != '0000'
		and ($cer_basicdata["cer_basicdata_rejected"] == NULL 
		     or substr($cer_basicdata["cer_basicdata_rejected"], 0, 4) == '0000')
		)
	{
		$can_edit_business_plan = false;
		//echo "4";
	}

	//cer was rejected and resubmitted after rejection
	if($cer_basicdata["cer_basicdata_rejected"] != NULL 
		and substr($cer_basicdata["cer_basicdata_rejected"], 0, 4) != '0000'
		and $cer_basicdata["cer_basicdata_resubmitted"] != NULL 
		and substr($cer_basicdata["cer_basicdata_resubmitted"], 0, 4) != '0000'
		and $cer_basicdata["cer_basicdata_resubmitted"] > $cer_basicdata["cer_basicdata_rejected"]
	  )
	{
		$can_edit_business_plan = false;
		//echo "5";
	}
}


//LN conditions for Corporate Projects only
if($project["project_cost_type"] == 1)
{
	//ln was submitted but not rejected
	if($ln_basicdata["ln_basicdata_locked"] == 0
	   and ($cer_basicdata["cer_basicdata_submitted"] == NULL 
			or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == '0000')
	   and $ln_basicdata["ln_basicdata_submitted"] != NULL 
	   and substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) != '0000'
	   and ($ln_basicdata["ln_basicdata_rejected"] == NULL 
			or substr($ln_basicdata["ln_basicdata_rejected"], 0, 4) == '0000')
	   )

	{
		//check if ln was approved in case of not being rejected
		//if not approved the editing is not possible
		$milestone = get_project_milestone($cer_basicdata["cer_basicdata_project"], 13);
		if(count($milestone) > 0 
			and ($milestone['project_milestone_date'] == NULL 
				 or $milestone['project_milestone_date'] == '0000-00-00'))
		{
			$can_edit_business_plan = false;
			//echo "6";
		}
	}

	//ln was submitted and rejected
	if($ln_basicdata["ln_basicdata_locked"] == 0
	   and ($cer_basicdata["cer_basicdata_submitted"] == NULL 
			or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == '0000')
	   and $ln_basicdata["ln_basicdata_submitted"] != NULL 
	   and substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) != '0000'
	   and $ln_basicdata["ln_basicdata_rejected"] != NULL 
	   and substr($ln_basicdata["ln_basicdata_rejected"], 0, 4) != '0000'
	   and $ln_basicdata["ln_basicdata_resubmitted"] > $ln_basicdata["ln_basicdata_rejected"]

	   )

	{
		//check if ln was approved in case of not being rejected
		//if not approved the editing is not possible
		$milestone = get_project_milestone($cer_basicdata["cer_basicdata_project"], 13);
		if(count($milestone) > 0 
			and ($milestone['project_milestone_date'] == NULL 
				 or $milestone['project_milestone_date'] == '0000-00-00'))
		{
			$can_edit_business_plan = false;
			//echo "7";
		}
	}
}


	
	