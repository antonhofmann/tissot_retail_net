<?php
$ids = array();

if(param("pid") > 0) //project
{
	$sql = "select * " .
		   "from cer_expenses " .
		   "left join cer_expense_types on cer_expense_type_id = cer_expense_type " . 
		   "where cer_expense_type = 2 " . 
		   " and cer_expense_project = " . param("pid") .
		   " and cer_expense_cer_version = 0 " . 
		   " order by cer_expense_year";
}
else //draft
{
	$sql = "select * " .
		   "from cer_draft_expenses " .
		   "left join cer_expense_types on cer_expense_type_id = cer_expense_type " . 
		   "where cer_expense_type = 2 " . 
		   " and cer_expense_draft_id = " . param("did") .
		   " order by cer_expense_year";

}

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$ids[$row["cer_expense_year"]] = $row["cer_expense_id"];
}

$amounts_per_month = array();
$increase_per_year = array();
$index_per_year = array();

foreach($price_records as $key=>$record)
{
	$fy = $record["fy"];
	$fm = $record["fm"];
	$fd = $record["fd"];

	$ty = $record["ty"];
	$tm = $record["tm"];
	$td = $record["td"];
	
	$unit = $record["unit"];
	$surface = $record["surface"];

	//$index_rate =  (float)(1 + ($record["indexrate"]/100));
	$index_rate =  0;
	$increase_rate = (float)(1 + ($record["increaserate"]/100));
	
	$monthcount = 0;
	$inflator = 0;
	$inflator2 = 0;


	$price = 0;

	if($unit == 1 or $unit == 2) //price per month and surface unit
	{
		$price = $record["amount"] * $record["surface"];
	}
	elseif($unit == 3 or $unit == 4) //price per year and surface unit
	{
		$price = ($record["amount"] * $record["surface"])/12;
	}
	elseif($unit == 5) //price per month and total surface
	{
		if($surface > 0)
		{
			$price = ($record["amount"] * $record["surface"])/$surface;
		}
	}
	elseif($unit == 6) //price per year and total surface
	{
		if($surface > 0)
		{
			$price = ($record["amount"] * $record["surface"])/12/$surface;
		}
	}

	for($year = $fy; $year <= $ty; $year++)
	{
		if($year == $fy and $fy == $ty)
		{
			for($month = $fm; $month <= $tm; $month++)
			{
				if($month < $tm)
				{
					$part_of_month = 1;
					$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
					if($month == $fm and $fd !=1)
					{
						$part_of_month = ($number_of_days_in_month-$fd+1)/$number_of_days_in_month;
					}

				}
				else
				{
					$part_of_month = 1;
					$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
					if($month == $tm and $td != $number_of_days_in_month)
					{
						$part_of_month = ($td)/$number_of_days_in_month;
					}
				}

				if(array_key_exists($year . " " . $month, $amounts_per_month))
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month] + $price*$part_of_month;
					

					//echo "n--" . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . "-->" . $price*$part_of_month . "<br />";
				}
				else
				{
					$amounts_per_month[$year . " " . $month] = $price*$part_of_month;
					
					//echo "n-" . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . "-->" . $price*$part_of_month . "<br />";
				}

				$monthcount++;
				if($monthcount == 12)
				{
					$monthcount = 0;
					$inflator++;
				}
				if($cer_basicdata["cer_basicdata_rental_increase_mode"] == 0)
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month]*pow($increase_rate, $inflator);
				}
				if($cer_basicdata["cer_basicdata_rental_index_mode"] == 0)
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month]*pow($index_rate, $inflator);
				}

				//echo $monthcount . ": " . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . " " . $increase_rate . " " . $inflator. " " . pow($increase_rate, $inflator) . "<br />";
				
			}

		}
		elseif($fy+1 == $ty and $year == $fy )
		{
			for($month = $fm; $month <= 12; $month++)
			{
				$part_of_month = 1;
				$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				if($month == $fm and $fd !=1)
				{
					$part_of_month = ($number_of_days_in_month-$fd+1)/$number_of_days_in_month;
				}

				

				if(array_key_exists($year . " " . $month, $amounts_per_month))
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month] + $price*$part_of_month;
					

					//echo "a--" . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . "-->" . $price*$part_of_month . "<br />";
				}
				else
				{
					$amounts_per_month[$year . " " . $month] = $price*$part_of_month;
					
					//echo "a-" . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . "-->" . $price*$part_of_month . "<br />";
				}

				
				if($monthcount == 12)
				{
					$monthcount = 0;
					$inflator++;
				}
				$monthcount++;
				if($cer_basicdata["cer_basicdata_rental_increase_mode"] == 0)
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month]*pow($increase_rate, $inflator);
				}
				if($cer_basicdata["cer_basicdata_rental_index_mode"] == 0)
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month]*pow($index_rate, $inflator);
				}
				//echo $monthcount . ": " . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . " " . $increase_rate . " " . $inflator. " " . pow($increase_rate, $inflator) . "<br />";
			}
			
		}
		elseif($fy+1 == $ty and $year == $ty )
		{
			for($month = 1; $month <= $tm; $month++)
			{
				$part_of_month = 1;
				$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				if($month == $tm and $td != $number_of_days_in_month)
				{
					$part_of_month = ($td)/$number_of_days_in_month;
				}
				
				if(array_key_exists($year . " " . $month, $amounts_per_month))
				{

					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month] + $price*$part_of_month;
					

					//echo "b--" . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . "-->" . $price*$part_of_month . "<br />";
				}
				else
				{
					$amounts_per_month[$year . " " . $month] = $price*$part_of_month;
					
					//echo "b-" . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . "-->" . $price*$part_of_month . "<br />";
				}
				if($monthcount == 12)
				{
					$monthcount = 0;
					$inflator++;
				}
				$monthcount++;
				if($cer_basicdata["cer_basicdata_rental_increase_mode"] == 0)
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month]*pow($increase_rate, $inflator);
				}
				if($cer_basicdata["cer_basicdata_rental_index_mode"] == 0)
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month]*pow($index_rate, $inflator);
				}
				//echo $monthcount . ": " . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . " " . $increase_rate . " " . $inflator. " " . pow($increase_rate, $inflator) . "<br />";
			}
		}
		elseif($year == $fy)
		{
			for($month = $fm; $month <= 12; $month++)
			{
				$part_of_month = 1;
				$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				if($month == $fm and $fd !=1)
				{
					$part_of_month = ($number_of_days_in_month-$fd+1)/$number_of_days_in_month;
				}

				

				if(array_key_exists($year . " " . $month, $amounts_per_month))
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month] + $price*$part_of_month;
					

					//echo "a--" . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . "-->" . $price*$part_of_month . "<br />";
				}
				else
				{
					$amounts_per_month[$year . " " . $month] = $price*$part_of_month;
					
					//echo "a-" . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . "-->" . $price*$part_of_month . "<br />";
				}
				if($monthcount == 12)
				{
					$monthcount = 0;
					$inflator++;
				}
				$monthcount++;
				if($cer_basicdata["cer_basicdata_rental_increase_mode"] == 0)
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month]*pow($increase_rate, $inflator);
				}
				if($cer_basicdata["cer_basicdata_rental_index_mode"] == 0)
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month]*pow($index_rate, $inflator);
				}
				//echo $monthcount . ": " . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . " " . $increase_rate . " " . $inflator. " " . pow($increase_rate, $inflator) . "<br />";
			}
		}
		elseif($year > $fy and $year < $ty)
		{
			for($month = 1; $month <= 12; $month++)
			{
				if(array_key_exists($year . " " . $month, $amounts_per_month))
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month]+$price;
					

					//echo "--" . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . "-->" . $price*$part_of_month . "<br />";
				}
				else
				{
					$amounts_per_month[$year . " " . $month] = $price;
					
					//echo $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . "-->" . $price*$part_of_month . "<br />";
				}
				if($monthcount == 12)
				{
					$monthcount = 0;
					$inflator++;
				}
				$monthcount++;
				if($cer_basicdata["cer_basicdata_rental_increase_mode"] == 0)
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month]*pow($increase_rate, $inflator);
				}
				if($cer_basicdata["cer_basicdata_rental_index_mode"] == 0)
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month]*pow($index_rate, $inflator);
				}
				//echo $monthcount . ": " . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . " " . $increase_rate . " " . $inflator. " " . pow($increase_rate, $inflator) . "<br />";
			}
		}
		else
		{
			for($month = 1; $month <= $tm; $month++)
			{
				$part_of_month = 1;
				$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				if($month == $tm and $td != $number_of_days_in_month)
				{
					$part_of_month = ($td)/$number_of_days_in_month;
				}
				
				if(array_key_exists($year . " " . $month, $amounts_per_month))
				{

					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month] + $price*$part_of_month;
					

					//echo "b--" . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . "-->" . $price*$part_of_month . "<br />";
				}
				else
				{
					$amounts_per_month[$year . " " . $month] = $price*$part_of_month;
					
					//echo "b-" . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . "-->" . $price*$part_of_month . "<br />";
				}
				if($monthcount == 12)
				{
					$monthcount = 0;
					$inflator++;
				}
				$monthcount++;
				if($cer_basicdata["cer_basicdata_rental_increase_mode"] == 0)
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month]*pow($increase_rate, $inflator);
				}
				if($cer_basicdata["cer_basicdata_rental_index_mode"] == 0)
				{
					$amounts_per_month[$year . " " . $month] = $amounts_per_month[$year . " " . $month]*pow($index_rate, $inflator);
				}
				//echo $monthcount . ": " . $year . " " . $month . " " . $amounts_per_month[$year . " " . $month] . " " . $increase_rate . " " . $inflator. " " . pow($increase_rate, $inflator) . "<br />";
			}
		}



		if($cer_basicdata["cer_basicdata_rental_increase_mode"] == 1 and $inflator2 > 0)
		{
			foreach($amounts_per_month as $key=>$amount)
			{
				$y = substr($key, 0, 4);

				if($y == $year)
				{
					$amounts_per_month[$key] = $amounts_per_month[$key]*pow($increase_rate, $inflator2);
				}
			}
		}

		if($cer_basicdata["cer_basicdata_rental_index_mode"] == 1 and $inflator2 > 0)
		{
			foreach($amounts_per_month as $key=>$amount)
			{
				$y = substr($key, 0, 4);
				if($y == $year)
				{
					$amounts_per_month[$key] = $amounts_per_month[$key]*pow($index_rate, $inflator2);
				}
			}
		}

		$inflator2++;
	}
}


//caclulate fixed rent per year
$rents_per_year = array();
foreach($ids as $year=>$expense_id)
{
	$rents_per_year[$year] = 0;
}

foreach($amounts_per_month as $key=>$amount)
{
	$year = substr($key, 0, 4);
	$month = substr($key, 5, strlen($key) - 1);
	$rents_per_year[$year] = $rents_per_year[$year] + $amount;
	//echo $year . " " . $month . " " . $amount . "<br />";
}

foreach($ids as $year=>$expense_id)
{
	$rents_per_year[$year] = round($rents_per_year[$year], 0);
}


foreach($ids as $year=>$id)
{
	$fields = array();

	
	if($rents_per_year[$year] > 0) // only update if there are values, prevent from overwriting existing values
	{
		$value = dbquote($rents_per_year[$year]);
		$fields[] = "cer_expense_amount = " . $value;

		
		$value = "current_timestamp";
		$fields[] = "date_modified = " . $value;

		if (isset($_SESSION["user_login"]))
		{
			$value = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value);
		}

		
		if(param("pid") > 0) //project
		{

			$sql = "update cer_expenses set " . join(", ", $fields) . 
					" where cer_expense_id = " . $id . 
					" and cer_expense_cer_version = 0 ";
		}
		else // draft
		{
			$sql = "update cer_draft_expenses set " . join(", ", $fields) . 
					" where cer_expense_id = " . $id;
		}
		mysql_query($sql) or dberror($sql);
	}
}


//$result = update_turnoverbased_rental_cost(param("pid"), $cer_basicdata["cer_basicdata_add_tob_rents"], $cer_basicdata["cer_basicdata_tob_from_net_sales"]);