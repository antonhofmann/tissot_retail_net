<?php


$tab2016 = '<div id="tab2016_container">';
$tab2016 .= '<ul id="tab2016">';

if($cer_basicdata["cer_basicdata_lastyear"] > 0)
{
	$class = '';
	$link = 'cer_application_general.php?pid=' . param("pid");
	if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
	$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">General</a></li>';


	if($project["project_projectkind"] != 8)
	{
		
		if($form_type == "CER" or $form_type == "AF")
		{
			$class = '';
			$link = 'cer_application_lease.php?pid=' . param("pid");
			if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
			$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Lease Conditions</a></li>';

			$class = '';
			$link = 'cer_application_rental.php?pid=' . param("pid");
			if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
			$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Rental Costs</a></li>';
		}

		if($project["project_projectkind"] != 5) //all projects except lease reneval
		{
			$class = '';
			$link = 'cer_application_investments.php?pid=' . param("pid");
			if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
			$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Key Money/Investment</a></li>';
		}
		else
		{
			$class = '';
			$link = 'cer_application_investments.php?pid=' . param("pid");
			if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
			$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Residual Value</a></li>';
		}

		$class = '';
		$link = 'cer_application_revenues.php?pid=' . param("pid");
		if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
		$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Revenues</a></li>';

		if($form_type == "CER" or $form_type == "AF")
		{
			$class = '';
			$link = 'cer_application_salaries.php?pid=' . param("pid");
			$link1 = 'cer_application_salary.php?pid=' . param("pid");
			if(strpos($_SERVER["REQUEST_URI"], $link) > 0 or strpos($_SERVER["REQUEST_URI"], $link1) > 0){$class = 'class="active"';}
			$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Human Resources</a></li>';

			$class = '';
			$link = 'cer_application_expenses.php?pid=' . param("pid");
			$link1 = 'cer_application_expense.php?pid=' . param("pid");
			if(strpos($_SERVER["REQUEST_URI"], $link) > 0 or strpos($_SERVER["REQUEST_URI"], $link1) > 0){$class = 'class="active"';}
			$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Expenses</a></li>';
		}

		$class = '';
		$link = 'cer_application_cashflow.php?pid=' . param("pid");
		if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
		$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Calculation Parameters</a></li>';
	}
	else
	{
		$class = '';
		$link = 'cer_application_investments.php?pid=' . param("pid");
		if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
		$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Investment</a></li>';

		$class = '';
		$link = 'cer_application_attachments.php?pid=' . param("pid");
		if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
		$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Attachments</a></li>';
	}
}
else
{
	$link = 'cer_application_general.php?pid=' . param("pid");
	if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
	$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">General</a></li>';

}

$tab2016 .= '</ul>';
$tab2016 .= '</div>';

echo $tab2016;

?>
