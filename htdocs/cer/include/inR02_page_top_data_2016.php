<?php

// draw page box
$pdf->SetXY($margin_left-2,$margin_top);

$pdf->Cell(277, 183, "", 1);

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 11);

if(param("pid") > 0 )
{
	if($epnr != '')
	{
		$pdf->Cell(220, 6, "Retail Business Plan Overview for Project " . $project_number . ": " . $project_name , 0, "", "L");
		$pdf->SetFont("arialn", "", 9);
		$pdf->Cell(15, 6, $epnr, 1, "", "C");
	}
	else
	{
		$pdf->Cell(235, 6, "Retail Business Plan Overview for Project " . $project_number . ": " . $project_name , 0, "", "L");
	}
}
else
{
	$pdf->Cell(235, 6, "Retail Business Draft: " . $project_name , 0, "", "L");
}

$pdf->SetFont("arialn", "", 9);



if($cer_version > 0)
{
	$pdf->Cell(20, 6, to_system_date(substr($cer_basicdata["version_date"], 0, 10)), 1, "", "C");
}
else
{
	$pdf->Cell(20, 6, date("d.m.Y"), 1, "", "C");
}

$pdf->Cell(20, 6, "IN-R02", 1, "", "C");

// draw box for general information


	$y = $margin_top + 6;
	$pdf->SetXY($margin_left,$y);
	$pdf->Cell(275, 9.5, "", 1);
	$pdf->SetXY($margin_left,$y);
	$y = $margin_top + 8;

	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 3.5, "Brand: Tissot", 0, "", "L");

	$pdf->SetXY($margin_left+106,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 3.5, "Legal Entity: " . $legal_entity, 0, "", "L");

	if($relocated_pos_name)
	{
		//row 2a
		$pdf->SetXY($margin_left+190,$y-3.5);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20, 3.5, "Project Classification: " . $pos_type_name . "/" . $project_kind . "/" . $legal_type_name, 0, "", "L");
		
		
		$y = $y;
		$pdf->SetXY($margin_left+190,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20, 3.5, "Relocated POS: " . $relocated_pos_name, 0, "", "L");
	
	}
	else
	{
		$pdf->SetXY($margin_left+190,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20, 3.5, "Project Classification: " . $pos_type_name . "/" . $project_kind . "/" . $legal_type_name, 0, "", "L");
	}

	//row 2b
	$y = $y + 3.5;
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 3.5, "Country/City: " . $pos_country . '/' . $pos_city, 0, "", "L");

	
	if($form_type == "AF" or $form_type == "INR03")
	{
		$pdf->SetXY($margin_left+106,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20, 3.5, "Address: " . $pos_address, 0, "", "L");

		/*
		$pdf->SetXY($margin_left+190,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20, 3.5, "City: " . $pos_city, 0, "", "L");
		*/

		$pdf->SetXY($margin_left+190,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20, 3.5, "Franchisee: " . $franchisee_company, 0, "", "L");
	}
	else
	{
		$pdf->SetXY($margin_left+106,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20, 3.5, "Address: " . $pos_address, 0, "", "L");


		$pdf->SetXY($margin_left+190,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20, 3.5, "Project Leader: " . $project_manager, 0, "", "L");
	}



// Investment and Organisation
$y = $y + 4;

$pdf->SetXY($margin_left,$y);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(63, 5, "2. Investment & Organisation", 1, "", "L");


	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 8);
	
	
	//box captions
	$pdf->Cell(63, 48.5, "", 1, "", "L");
		
	$pdf->SetXY($margin_left+65,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 5, "in T" . $currency_symbol, 1, "", "C");

	
	$pdf->SetXY($margin_left+85,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(10, 5, "Share", 1, "", "C");

	$pdf->SetXY($margin_left+95,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(10, 5, "Deprec.", 1, "", "C");

	
	$x1 = $margin_left+6;
	$x2 = $margin_left+65;
	$x3 = $margin_left+95;

	//column 1

	$y = $y + 6;
	$y2 = $y;   // for column2
	$y3 = $y;   // for column3


	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "B", 9);
	
	$pdf->Cell(20, 21, "", 1, "", "L");
	

	//box investment shares
	$pdf->SetXY($x3-10,$y);
	$pdf->SetFont("arialn", "B", 9);
		
	$pdf->Cell(10, 21, "", 1, "", "L");
	
	//box depr
	$pdf->SetXY($x3,$y);
	$pdf->SetFont("arialn", "B", 9);
	
	
	$pdf->Cell(10, 21, "", 1, "", "L");
		
	$pdf->SetXY($x2,$y+25);
	$pdf->SetFont("arialn", "B", 7);
	$pdf->Cell(20, 2, "", 1, "", "L");

	$pdf->SetXY($x3-10,$y+25);
	$pdf->SetFont("arialn", "B", 7);
	$pdf->Cell(10, 2, "", 1, "", "L");

	$pdf->SetXY($x3,$y+25);
	$pdf->SetFont("arialn", "B", 7);
	$pdf->Cell(10, 2, "", 1, "", "L");
	
	$pdf->SetXY($x2,$y+32);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(20, 10.5, "", 1, "", "L");

	$pdf->SetXY($x3-10,$y+32);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(20, 10.5, "", 1, "", "L");

	
	/*
	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(20, 3.5, "Project cost:", 0, "", "L");
	*/
	//new header 2016
	$y = $y - $standard_y;


	foreach($fixed_assets as $key=>$itype)
	{
		
		
		$deprperiod_string = "";
		if(array_key_exists($itype, $amounts))
		{
			if(!$amounts[$itype]){
				$amounts[$itype] = "-";
				
			}
			
			if($depryears[$itype] == 0 and $deprmonths[$itype] > 0)
			{
				$deprperiod_string  = $deprmonths[$itype] . "m";
				
			}
			elseif($depryears[$itype] > 0 and $deprmonths[$itype] >= 0)
			{
			    //$depryears[$itype]  = $depryears[$itype] . "y-" . $deprmonths[$itype] . "m";

				$deprperiod_string  = $depryears[$itype] . "y-" . $deprmonths[$itype] . "m";
			}
			else
			{
				$depryears[$itype] = "-";
				$deprperiod_string  = "-";
			}

			$y = $y + $standard_y;
			$pdf->SetXY($x1-3,$y);
			$pdf->SetFont("arialn", "", 8);

			$pdf->Cell(63, 3.5, $investment_names[$itype] , 0, "", "L");

			$pdf->SetXY($x2,$y);
			$pdf->SetFont("arialn", "", 8);
			
			if($amounts[$itype] != "-" and $amounts[$itype] != 0)
			{
				$pdf->Cell(20, 3.5, round(($exchange_rate*$amounts[$itype]/$exchange_rate_factor) / 1000,0), 0, "", "R");
			}
			else
			{
				$pdf->Cell(20, 3.5, "-", 0, "", "R");
			}



			//investmen shares
			if($investment_total > 0 and $amounts[$itype] > 0)
			{
				$ishare_percent = round(100*$amounts[$itype]/$investment_total) ."%";
			}

			$pdf->SetXY($x3-10,$y);
			$pdf->SetFont("arialn", "", 8);
			$pdf->Cell(10, 3.5, $ishare_percent , 0, "", "R");


			$pdf->SetXY($x3,$y);
			$pdf->SetFont("arialn", "", 8);
			$pdf->Cell(10, 3.5, $deprperiod_string , 0, "", "R");
		}
		else
		{
			$y = $y + $standard_y;
		}
	}


	$y = $y + 0.5;
	
	$y = $y + $standard_y;
	$pdf->SetXY($x1-3,$y);
	$pdf->SetFont("arialn", "B", 8);
	
	
	if($show_business_plan_version_2016 == 1)
	{
		$pdf->Cell(20, 3.5, "Gross total invest. fixed assets", 0, "", "L");
	}
	else
	{
		$pdf->Cell(20, 3.5, "Total investment fixed assets", 0, "", "L");
	}
	
	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "B", 8);
	
	$pdf->Cell(20, 3.5, round($exchange_rate*$investment_total/$exchange_rate_factor/1000,0), 0, "", "R");


	//landlord's contribution
	if($show_business_plan_version_2016 == 1)
	{
		$y = $y + $standard_y;
		$pdf->SetXY($x1-3,$y);
		$pdf->SetFont("arialn", "", 8);
		
		if(array_key_exists(19, $investment_names))
		{
			$pdf->Cell(20, 3.5, $investment_names[19], 0, "", "L");
		}
		else
		{
			$pdf->Cell(20, 3.5,"", 0, "", "L");
		}

		$pdf->SetXY($x2,$y);
		$pdf->SetFont("arialn", "", 8);
		
		
		if(array_key_exists(19, $amounts))
		{
			if($amounts[19] != 0)
			{
				$pdf->Cell(20, 3.5, round(-1*$exchange_rate*$amounts[19]/$exchange_rate_factor/1000,0), 0, "", "R");
			}
			else
			{
				$pdf->Cell(20, 3.5, "-", 0, "", "R");
			}
		}
		else
		{
			$pdf->Cell(20, 3.5, "", 0, "", "R");
		}


		//investmen shares
		if($investment_total > 0 and $amounts[19] > 0)
		{
			$ishare_percent = round(100*$amounts[19]/$investment_total) ."%";
		}
		else
		{
			$ishare_percent = "-";
		}

		$pdf->SetXY($x3-10,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(10, 3.5, $ishare_percent , 0, "", "R");
		
		
		
		$pdf->SetXY($x3,$y);
		$pdf->SetFont("arialn", "", 8);
		
		$pdf->Cell(10, 3.5,"-", 0, "", "R");
		


		
		$y = $y + $standard_y;
		$pdf->SetXY($x1-3,$y);
		$pdf->SetFont("arialn", "B", 8);

		$pdf->Cell(20, 3.5, "Net total invest. fixed assets", 0, "", "L");
		
		$pdf->SetXY($x2,$y);
		$pdf->SetFont("arialn", "B", 8);
	
		$pdf->Cell(20, 3.5, round($exchange_rate*($investment_total + $landloard_contribution)/$exchange_rate_factor/1000,0), 0, "", "R");

	}
	
	
	

	
	$y = $y + $standard_y;
	$pdf->SetXY($x1-3,$y);
	$pdf->SetFont("arialn", "", 8);

	$pdf->Cell(20, 3.5, $intagible_name, 0, "", "L");

	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if($intagible_amount != 0)
	{
		$pdf->Cell(20, 3.5, round($exchange_rate*$intagible_amount/$exchange_rate_factor/1000,0), 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 0, "", "R");
	}

	$pdf->SetXY($x3,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if($intagible_depryears != 0)
	{
		$pdf->Cell(10, 3.5, $intagible_depryears, 0, "", "R");
	}
	else
	{
		$pdf->Cell(10, 3.5, "-", 0, "", "R");
	}

	$y = $y + $standard_y;
	$pdf->SetXY($x1-3,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if(array_key_exists(9, $investment_names))
	{
		//$pdf->Cell(20, 3.5, $investment_names[9] . "/Recoverable Keymoney", 0, "", "L");
		$pdf->Cell(20, 3.5, $investment_names[9], 0, "", "L");
	}
	else
	{
		$pdf->Cell(20, 3.5,"", 0, "", "L");
	}

	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "", 8);
	
	
	if(array_key_exists(9, $amounts))
	{
		if($amounts[9] != 0)
		{
			$pdf->Cell(20, 3.5, round($exchange_rate*$amounts[9]/$exchange_rate_factor/1000,0), 0, "", "R");
		}
		else
		{
			$pdf->Cell(20, 3.5, "-", 0, "", "R");
		}
	}
	else
	{
		$pdf->Cell(20, 3.5, "", 0, "", "R");
	}
	

	$pdf->SetXY($x3,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if(array_key_exists(9, $depryears))
	{
		if($depryears[9] != 0)
		{
			$pdf->Cell(10, 3.5, $depryears[9], 0, "", "R");
		}
		else
		{
			$pdf->Cell(10, 3.5,"-", 0, "", "R");
		}
	}
	else
	{
		$pdf->Cell(10, 3.5,"", 0, "", "R");
	}



	$y = $y + $standard_y;
	$pdf->SetXY($x1-3,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if(array_key_exists(13, $investment_names))
	{
		$pdf->Cell(20, 3.5, $investment_names[13], 0, "", "L");
	}
	else
	{
		$pdf->Cell(20, 3.5, "", 0, "", "L");
	}

	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if(array_key_exists(13, $amounts))
	{
		if($amounts[13] != 0)
		{
			$pdf->Cell(20, 3.5, round($exchange_rate*$amounts[13]/$exchange_rate_factor/1000,0), 0, "", "R");
		}
		else
		{
			$pdf->Cell(20, 3.5, "-", 0, "", "R");
		}
	}
	else
	{
		$pdf->Cell(20, 3.5, "", 0, "", "R");
	}
	

	$pdf->SetXY($x3,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if(array_key_exists(13, $depryears))
	{
		if($depryears[13] != 0)
		{
			$pdf->Cell(10, 3.5, $depryears[13], 0, "", "R");
		}
		else
		{
			$pdf->Cell(10, 3.5, "-", 0, "", "R");
		}
	}
	else
	{
		$pdf->Cell(10, 3.5, "", 0, "", "R");
	}

	
	$y = $y+ $standard_y;
	$pdf->SetFont("arialn", "B", 9);
	$pdf->SetXY($margin_left,$y);
	$pdf->Cell(63, 3.5, "", 1, " ", "L");
	//$pdf->Line($margin_left, $y, $margin_left+63,$y);
	
	$pdf->SetXY($margin_left+3.2,$y);
	
	$pdf->Cell(50, 3.5, "Total Project costs (Requested amount)", 0, "", "L");

	$pdf->SetXY($x2,$y);
	//$pdf->Line($x2, $y, $x2+20,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(20, 3.5, round($exchange_rate*$cer_totals/$exchange_rate_factor/1000,0), 1, "", "R");
	$pdf->Cell(20, 3.5, " ", 1, "", "R");


	//column 2 for CER
	
	$x1 = $x1 + 101;
	$x2 = $x1 + 60;


	//new header 2016
	$y2 = $y2 - $standard_y;

	$pdf->SetXY($x1,$y2);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(82, 39, "", 1, "", "L");
	
	$pdf->SetXY($x1,$y2);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(20, 3.5, "Rental Contract:", 0, "", "L");

	$y2 = $y2 + $standard_y;
	$pdf->SetXY($x1+3,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "Rental Period (Duration in Years)", 0, "", "L");

	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, $rental_period , 0, "", "R");


	$y2 = $y2 + $standard_y;
	$pdf->SetXY($x1+3,$y2);
	$pdf->SetFont("arialn", "", 8);
	//$pdf->Cell(20, 3.5, "Gross / Total Surface in m2", 0, "", "L");
	$pdf->Cell(20, 3.5, "Total / Sales Surface in m2", 0, "", "L");

	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, $surfaces, 0, "", "R");

	$y2 = $y2 + $standard_y;
	$pdf->SetXY($x1+3,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "Sales Surface in m2", 0, "", "L");

	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, $sales_surface, 0, "", "R");

	$y2 = $y2 + $standard_y;
	$pdf->SetXY($x1+3,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "Annual Rent in T" . $currency_symbol, 0, "", "L");

	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, round($exchange_rate*$annual_rent/$exchange_rate_factor/1000,0), 0, "", "R");

	

	$y2 = $y2 + $standard_y;
	$pdf->SetXY($x1+3,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "Renewal Option (in years)", 0, "", "L");

	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, $renewal_option, 0, "", "R");

	$y2 = $y2 + $standard_y;
	$pdf->SetXY($x1+3,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "Exit Option", 0, "", "L");

	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, $exit_option, 0, "", "R");

	$y2 = $y2 + $standard_y;
	$pdf->SetXY($x1+3,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "Termination deadline", 0, "", "L");

	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, $termination_time, 0, "", "R");

	$y2 = $y2 + $standard_y;
	$pdf->SetXY($x1+3,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "Index clause in rental Contract", 0, "", "L");

	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, $index_clause, 0, "", "R");


	$y2 = $y2 + $standard_y;
	$pdf->SetXY($x1+3,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "Tacit Renewal Clause", 0, "", "L");

	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, $index_clause2, 0, "", "R");


	$y2 = $y2 + $standard_y;
	$pdf->SetXY($x1+3,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "Total Lease Commitment", 0, "", "L");

	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, round($exchange_rate*$total_lease_commitment/$exchange_rate_factor/1000,0), 0, "", "R");

	//column 3 for CER
	$x1 = $x1 + 84;
	$x2 = $x1 + 60;

	//new header 2016
	$y3 = $y3 - $standard_y;

	
	$pdf->SetXY($x1,$y3);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(82, 39, "", 1, "", "L");

	$pdf->SetXY($x1,$y3);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(20, 3.5, "Other Information:", 0, "", "L");

	$y3 = $y3 + $standard_y;
	$pdf->SetXY($x1+3,$y3);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "Business Plan Period", 0, "", "L");

	$pdf->SetXY($x2,$y3);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, $business_plan_period, 0, "", "R");


	$y3 = $y3 + $standard_y;
	$pdf->SetXY($x1+3,$y3);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "Headcounts/FTE", 0, "", "L");

	$pdf->SetXY($x2,$y3);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, $head_counts . "/" . $ftes, 0, "", "R");

	$y3 = $y3 + $standard_y;
	$pdf->SetXY($x1+3,$y3);
	$pdf->SetFont("arialn", "", 8);
	
	if($project_kind_id == 3)
	{
		$pdf->Cell(20, 3.5, "Planned takeover date/Planned opening date", 0, "", "L");

		$pdf->SetXY($x2,$y3);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(20, 3.5, $planned_takeover_date . "/" . $planned_opening_date, 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "Planned opening date", 0, "", "L");

		$pdf->SetXY($x2,$y3);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(20, 3.5, $planned_opening_date, 0, "", "R");
	}

	$y3 = $y3 + $standard_y;
	$pdf->SetXY($x1+3,$y3);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "Deadline for property", 0, "", "RZ");

	$pdf->SetXY($x2,$y3);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, $deadline_for_property, 0, "", "R");


	$y3 = $y3 + $standard_y;
	$pdf->SetXY($x1+3,$y3);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "Handover Date (Key)", 0, "", "RZ");

	$hand_over_key = ( !isset($hand_over_key) ) ? '': $hand_over_key;
	$pdf->SetXY($x2,$y3);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, $hand_over_key, 0, "", "R");


	$y3 = $y3 + $standard_y;
	$pdf->SetXY($x1+3,$y3);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "First Rent Payed as from", 0, "", "RZ");
	
	$first_rent_payed = ( !isset($first_rent_payed) ) ? '': $first_rent_payed;
	$pdf->SetXY($x2,$y3);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, $first_rent_payed, 0, "", "R");


	$y3 = $y3 + $standard_y;
	$pdf->SetXY($x1,$y3);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 3.5, "In case of Renovation:", 0, "", "L");

	$y3 = $y3 + $standard_y;
	$pdf->SetXY($x1+3,$y3);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "Date of store/boutique opening", 0, "", "L");

	$pdf->SetXY($x2,$y3);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, $pos_opening_date, 0, "", "R");

	$y3 = $y3 + $standard_y;
	$pdf->SetXY($x1+3,$y3);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, "Residual Value of Fixed Assets in T" . $currency_symbol, 0, "", "L");

	$pdf->SetXY($x2,$y3);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, round($exchange_rate*$residual_value_former_investment/$exchange_rate_factor/1000, 0), 0, "", "R");
	
		
	$y3 = $y3 + $standard_y;
	$pdf->SetXY($x1+3,$y3);
	$pdf->SetFont("arialn", "", 8);
	
	$pdf->Cell(20, 3.5, "Residual Value of Keymoney in T" . $currency_symbol, 0, "", "L");
	$pdf->SetXY($x2,$y3);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 3.5, round($exchange_rate*$residual_value_former_keymoney/$exchange_rate_factor/1000, 0), 0, "", "R");
		
	
?>