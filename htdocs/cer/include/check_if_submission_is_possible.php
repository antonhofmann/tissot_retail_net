<?php


//check if ln/AF/CER/inr03 can be submitted
$ln_submission_possible = 1;



if($ln_basicdata['ln_no_ln_submission_needed'] == 1) {
	$ln_submission_possible = 0;
}

//check ln_data
$ln_ok = true;
$ln_ok1 = true;
$ln_was_approved = 0;
//check if LN was approved, milestone id 13
$milestone = get_project_milestone($cer_basicdata["cer_basicdata_project"], 13);
if(count($milestone) > 0 
	and ($milestone['project_milestone_date'] != NULL 
		and $milestone['project_milestone_date'] != '0000-00-00'))
{
	$ln_submission_possible = 0;
	$ln_was_approved = 1;
}


if($project["project_projectkind"] == 8) //popups
{
	$ln_ok = true;
	$ln_ok1 = true;
}

$ok = "<img style='padding-top:4px;' src='../pictures/data_ok.png' />";
$ok ="<table><tr><td>" . $ok . "</td><td>&nbsp;</td></tr></table>";
$not_ok = "<img style='padding-top:4px;' src='../pictures/data_not_ok.png' />";


/****************************************
busines plan
*****************************************/
$business_plan_errors = array();
$bp_captions[0] = "Business Plan Period";
$bp_captions[1] = "Neighbourhood Information";
if($form_type != 'INR03') {
	$bp_captions[2] = "Lease Conditions";
	$bp_captions[3] = "Rental Costs";
}
$bp_captions[4] = "Key money/Investments";
$bp_captions[5] = "Revenues";
if($form_type != 'INR03') {
	$bp_captions[6] = "Other Sales Relevant Information";
	$bp_captions[7] = "Human Resources";
	$bp_captions[8] = "Expenses";
}
else{
	$bp_captions[6] = "Other Sales Relevant Information";
}

$bp_errorlines[0] = 'The business plan period is missing: <a href="cer_application_general.php?pid=' .  param("pid") .'">check it</a>';
$bp_errorlines[1] = 'Information about the neighbourhood is incomplete: <a href="cer_application_general.php?pid=' .  param("pid") .'">check it</a>';

$bp_errorlines[2] = 'Lease period is missing: <a href="cer_application_lease.php?pid=' .  param("pid") .'">check it</a>';
$bp_errorlines[3] = 'The date of first rent paid is missing: <a href="cer_application_lease.php?pid=' .  param("pid") .'">check it</a>';

$bp_errorlines[4] = 'Information about rental costs is missing: <a href="cer_application_rental.php?pid=' .  param("pid") .'">check it</a>';

$bp_errorlines[5] = 'Either a day, a month or a year is not indicated in the period of the fixed rental periods: <a href="cer_application_rental.php?pid=' .  param("pid") .'">check it</a>';

$bp_errorlines[6] = 'Information about fixed rental costs is missing: <a href="cer_application_rental.php?pid=' .  param("pid") .'">check it</a>';

$bp_errorlines[7] = 'Information about fixed rents is given in rental costs but you indicated that the contract does not contain fixed rents: <a href="cer_application_lease.php?pid=' .  param("pid") .'">check it</a>';

$bp_errorlines[8] = 'Information about key money or investments is missing: <a href="cer_application_investments.php?pid=' .  param("pid") .'">check it</a>';


$bp_errorlines[9] = 'Information about revenues or sales reduction is missing: <a href="cer_application_revenues.php?pid=' .  param("pid") .'">check it</a>';

$bp_errorlines[10] = 'Information about other total days open, total hours open per week or total working hours per week is missing: <a href="cer_application_revenues.php?pid=' .  param("pid") .'">check it</a>';

$bp_errorlines[11] = 'Information about staff and salaries is missing: <a href="cer_application_salaries.php?pid=' .  param("pid") .'">check it</a>';

$bp_errorlines[12] = 'Information about the costs of auxiliary material, energy, maintenance or material costs of products sold is missing: <a href="cer_application_expenses.php?pid=' .  param("pid") .'">check it</a>';


$bp_errorlines[13] = 'Index clause in lease details but no icrease rate indicated in rental costs: <a href="cer_application_rental.php?pid=' .  param("pid") .'">check it</a>';
$bp_errorlines[14] = 'Index rate indicated in rental costs but no index clause in lease details: <a href="cer_application_lease.php?pid=' .  param("pid") .'">check it</a>';


//business plan period
if($cer_basicdata["cer_basicdata_firstyear"] == NULL 
	or $cer_basicdata["cer_basicdata_firstyear"] == "0"
	or $cer_basicdata["cer_basicdata_lastyear"] == NULL 
	or $cer_basicdata["cer_basicdata_lastyear"] == "0"
	or$cer_basicdata["cer_basicdata_firstmonth"] == NULL 
	or $cer_basicdata["cer_basicdata_firstmonth"] == "0"
	or $cer_basicdata["cer_basicdata_lastmonth"] == NULL 
	or $cer_basicdata["cer_basicdata_lastmonth"] == "0")

{
	$business_plan_errors[] = array(0);
}

//check if neighbourhood is filled in
//neighbourhood
if($project["pipeline"] == 0)
{
	$pos_order_sql = "select * from posorders where posorder_order = " . $project["order_id"];
}
elseif($project["pipeline"] == 1)
{
	$pos_order_sql = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
}


$businesstypes_mandatory = array();
$sql = "select businesstype_id, businesstype_neighbour_needed " . 
	   "from businesstypes " . 
	   " order by businesstype_text";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$businesstypes_mandatory[$row["businesstype_id"]]	= $row["businesstype_neighbour_needed"];
}


$res = mysql_query($pos_order_sql) or dberror($pos_order_sql);
if ($row = mysql_fetch_assoc($res))
{
	if($row["posorder_neighbour_left_business_type"] > 0 and 
		$row["posorder_neighbour_left_business_type"] > 0 
		and $businesstypes_mandatory[$row["posorder_neighbour_left_business_type"]] == 0)
	{
		$neighbourhood = 1;
	}
	elseif(!$row["posorder_neighbour_left"]
	   or !$row["posorder_neighbour_left_business_type"]
       or !$row["posorder_neighbour_left_price_range"])
	{
		$business_plan_errors[1] = array(1);
	}


	if($row["posorder_neighbour_right_business_type"] > 0 and 
		$row["posorder_neighbour_right_business_type"] > 0 
		and $businesstypes_mandatory[$row["posorder_neighbour_right_business_type"]] == 0)
	{
		$neighbourhood = 1;
	}
	elseif(!$row["posorder_neighbour_right"]
	   or !$row["posorder_neighbour_right_business_type"]
       or !$row["posorder_neighbour_right_price_range"])
	{
		$business_plan_errors[1] = array(1);
	}


	if($row["posorder_neighbour_acrleft_business_type"] > 0 and 
		$row["posorder_neighbour_acrleft_business_type"] > 0 
		and $businesstypes_mandatory[$row["posorder_neighbour_acrleft_business_type"]] == 0)
	{
		$neighbourhood = 1;
	}
	elseif(!$row["posorder_neighbour_acrleft"]
	   or !$row["posorder_neighbour_acrleft_business_type"]
       or !$row["posorder_neighbour_acrleft_price_range"])
	{
		$business_plan_errors[1] = array(1);
	}

	if($row["posorder_neighbour_acrright_business_type"] > 0 and 
		$row["posorder_neighbour_acrright_business_type"] > 0 
		and $businesstypes_mandatory[$row["posorder_neighbour_acrright_business_type"]] == 0)
	{
		$neighbourhood = 1;
	}
	elseif(!$row["posorder_neighbour_acrright"]
	   or !$row["posorder_neighbour_acrright_business_type"]
       or !$row["posorder_neighbour_acrright_price_range"])
	{
		$business_plan_errors[1] = array(1);
	}
}

//lease conditions
if($form_type != 'INR03') {
	$tmp = array();
	if(!isset($posleases["poslease_startdate"]) 
		or $posleases["poslease_startdate"] == NULL 
		or $posleases["poslease_startdate"] == '0000-00-00'
		or !isset($posleases["poslease_enddate"]) 
		or $posleases["poslease_enddate"] == NULL 
		or $posleases["poslease_enddate"] == '0000-00-00') {

		$tmp[] = 2;
	}

	if(!isset($posleases["poslease_firstrentpayed"]) 
		or $posleases["poslease_firstrentpayed"] == NULL 
		or $posleases["poslease_firstrentpayed"] == '0000-00-00') {
		$tmp[] = 3;
	}

	if(count($tmp) > 0) {
		$business_plan_errors[2] =$tmp;
	}
}

if($form_type != 'INR03') {
	$salaries = 0;
	$turnoverbased_rents = 0;
	$fixed_rents = 0;
	$expenses = 0;
	$sum_of_index_rates = 0;
	if($form_type != 'INR03') {
		$sql = "select sum(cer_expense_amount) as total " .
			   "from cer_expenses " .
			   " where cer_expense_project = " . param("pid") .
			   " and cer_expense_cer_version = 0 ";

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$expenses = $row["total"];
		}


		//check if all necessary expenses are filled in
		//Material Cost, Salaries, Aux. Material and Energy 
		if($expenses > 0)
		{
			if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] <> '0000-00-00')
			{
				$sql = "select count(cer_expense_amount) as num_recs " .
					   "from cer_expenses " .
					   " where cer_expense_project = " . param("pid") .
					   " and cer_expense_cer_version = 0 " .
					   " and cer_expense_year >= " . substr($project["project_real_opening_date"], 0, 4) .
					   " and cer_expense_type IN(1, 4, 15) " . 
					   " and cer_expense_amount = 0";
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				if($row['num_recs'] > 1) {$expenses = 0;}
			}
			else
			{
				$sql = "select count(cer_expense_amount) as num_recs " .
					   "from cer_expenses " .
					   " where cer_expense_project = " . param("pid") .
					   " and cer_expense_cer_version = 0 " .
					   " and cer_expense_type IN(1, 4, 15) " . 
					   " and cer_expense_amount = 0";
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				if($row['num_recs'] > 1) {$expenses = 0;}
			}
		}


		//get fixed rents
		$sql = "select sum(cer_expense_amount) as amount " .
			   "from cer_expenses " .
			   " where cer_expense_project = " . param("pid") . 
			   " and cer_expense_cer_version = 0 " .
			   " and cer_expense_type IN(2) ";

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		$fixed_rents = $row['amount'];

		//get turn over based rents
		$sql = "select sum(cer_expense_amount) as tob " .
			   "from cer_expenses " .
			   " where cer_expense_project = " . param("pid") .
			   " and cer_expense_cer_version = 0 " .
			   " and cer_expense_type IN(16) ";
		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		if($row['tob'] > 1) {$turnoverbased_rents = $row['tob'];}


		if($expenses > 0)
		{
			$tmp1 = 1;
			$years_with_zero_values = array();
			$sql = "select cer_expense_year, cer_expense_amount " .
				   "from cer_expenses " .
				   " where cer_expense_project = " . param("pid") . 
				   " and cer_expense_cer_version = 0 " .
				   " and cer_expense_type = 2 " . 
				   " and cer_expense_year > " . $cer_basicdata["cer_basicdata_firstyear"] . 
				   " and cer_expense_amount = 0";

			$res = mysql_query($sql) or dberror($sql);
			while($row = mysql_fetch_assoc($res))
			{
				$tmp1 = 0;
				$years_with_zero_values[] = $row['cer_expense_year'];
			}

			if($tmp1 == 0)
			{

				$tmp2 = 1;
				foreach($years_with_zero_values as $key=>$year)
				{
					$sql = "select count(cer_expense_amount) as num_recs " .
						   "from cer_expenses " .
						   " where cer_expense_project = " . param("pid") . 
						   " and cer_expense_cer_version = 0 " .
						   " and cer_expense_type = 16 " . 
						   " and cer_expense_amount = 0 " .
						   " and cer_expense_year > " . $cer_basicdata["cer_basicdata_firstyear"] . 
						   " and cer_expense_year = " . $year;


					$res = mysql_query($sql) or dberror($sql);
					$row = mysql_fetch_assoc($res);
					if($row['num_recs'] > 0) {$tmp2 = 0;}

					if($tmp2 == 0) {$expenses = 0;}
				}
			}
		}


		//get index rates
		
		$sql = "select sum(cer_fixed_rent_increas_rate) as sumindex " .
			   "from cer_fixed_rents " .
			   " where cer_fixed_rent_project_id = " . param("pid") . 
			   " and cer_fixed_rent_cer_version = 0 ";
		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		$sum_of_index_rates = $row['sumindex'];



		$sql = "select sum(cer_salary_fixed_salary) as total " .
			   "from cer_salaries " .
			   " where cer_salary_project = " . param("pid") .
			   " and cer_salary_cer_version = 0 ";

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$salaries = $row["total"];
		}
	}
}

//rental costs
if($form_type != 'INR03') {
	$index_clause_in_lease_details = 0;
	if(count($posleases) > 0)
	{
		$index_clause_in_lease_details = $posleases["poslease_indexclause_in_contract"];
	}

	$tmp = array();
	if($fixed_rents == 0 
		and $turnoverbased_rents == 0)
	{
		$tmp[] = 4;
	}
	if(check_fixed_rental_data(param("pid")) == true){
		$tmp[] = 5;
	}

	if($fixed_rents == 0 
		and array_key_exists("poslease_hasfixrent", $posleases) 
		and $posleases["poslease_hasfixrent"] == 1)
	{
		$tmp[] = 6;
	}
	elseif($fixed_rents > 0 
		and array_key_exists("poslease_hasfixrent", $posleases) 
		and $posleases["poslease_hasfixrent"] == 0)
	{
		$tmp[] = 7;
	}

	if($index_clause_in_lease_details == 1 
		and  $sum_of_index_rates == 0)
	{	
		$tmp[] = 13;

	}
	elseif($index_clause_in_lease_details == 0 
		and  $sum_of_index_rates > 0)
	{
		$tmp[] = 14;
	}

	if(count($tmp) > 0) {
		$business_plan_errors[3] =$tmp;
	}
}

//keymoney investments
$construction = get_pos_intangibles(param("pid"), 1);
$fixturing = get_pos_intangibles(param("pid"), 3);
$keymoney = get_pos_intangibles(param("pid"), 15);
if((array_key_exists("cer_investment_amount_cer_loc", $construction) 
		and $construction["cer_investment_amount_cer_loc"] > 0) 
	or (array_key_exists("cer_investment_amount_cer_loc", $fixturing) 
		and $fixturing["cer_investment_amount_cer_loc"] > 0)  
	or (array_key_exists("cer_investment_amount_cer_loc", $keymoney) 
		and $keymoney["cer_investment_amount_cer_loc"] > 0))
	{
		
	}
	else
	{
		$business_plan_errors[4] = array(8);
	}


$sql = "select sum(cer_revenue_watches) as total " .
       "from cer_revenues " .
	   " where cer_revenue_project = " . param("pid") . 
	   " and cer_revenue_cer_version = 0 ";

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	if($row['total'] == 0) {
		$business_plan_errors[5] = array(9);
	}
}



//other sales relevant information

$sql = "select cer_revenue_id " .
       "from cer_revenues " .
	   " where cer_revenue_project = " . param("pid") . 
	   " and cer_revenue_cer_version = 0 ";

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
		$sql = "select count(cer_revenue_id) as num_recs " .
			   "from cer_revenues " .
			   " where cer_revenue_project = " . param("pid") . 
			   " and cer_revenue_cer_version = 0 " .
			   " and (cer_revenue_total_days_open_per_year = 0 " . 
			   " or cer_revenue_total_hours_open_per_week = 0 " . 
			   " or cer_revenue_total_workinghours_per_week = 0 " . 
			   " or cer_revenue_total_days_open_per_year is null " . 
			   " or cer_revenue_total_hours_open_per_week is null " . 
			   " or cer_revenue_total_workinghours_per_week is null)";


	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if($row['num_recs'] > 0) {
		$business_plan_errors[6] = array(10);
	}
}
else {
	$business_plan_errors[6] = array(10);
}



//salaries
if($form_type != 'INR03') {
	if($salaries == 0) {
		$business_plan_errors[7] = array(11);
	}
}

//expenses
if($form_type != 'INR03') {
	if($salaries == 0) {
		$business_plan_errors[8] = array(12);
	}
}



/****************************************
LN Form
*****************************************/
$ln_errors = array();
$ln_captions[0] = "General Information";
$ln_captions[1] = "Attachments";

if($project['project_projectkind'] != 5) // lease reneval
{
	$ln_captions[2] = "Project Information";
}

if($form_type != 'INR03'
 and $project['project_projectkind'] != 1) // new pos
{
	$ln_captions[3] = "Sellouts";
}
if($form_type != 'INR03') {
	$ln_captions[4] = "Distribution Analysis";
	$ln_captions[5] = "Other Swatch Group Brands";
}

if($project['project_cost_type'] == 1
    and ($project["project_projectkind"] == 2 
	or $project["project_projectkind"] == 3
	or $project["project_projectkind"] == 4
	or $project["project_projectkind"] == 5
	or $project["project_projectkind"] == 9)) // renovation or tekover/renovation or lease renewal
{
	$ln_captions[6] = "Passed Rental";
}
if($project['project_cost_type'] == 1
	and $project['project_projectkind'] == 6) // corporate relocation
{
	$ln_captions[7] = "Relocation Information";
}

if($form_type != 'CER') // franchisee, independent retailer
{
	$ln_captions[8] = "Relation Information";
}

$ln_errorlines[0] = 'General information is incomplete: <a href="ln_general.php?pid=' .  param("pid") .'">check it</a>';

$ln_errorlines[1] = 'On or more attachments/pictures are missing: <a href="ln_general.php?pid=' .  param("pid") .'">check it</a>';
$ln_errorlines[2] = 'Project information is incomplete: <a href="ln_project_information.php?pid=' .  param("pid") .'">check it</a>';

$ln_errorlines[3] = 'Information about the sellout data is missing: <a href="sellouts.php?pid=' .  param("pid") .'">check it</a>';

if($form_type == 'CER' 
	or $form_type == 'AF') {
	$ln_errorlines[4] = 'Distribution analysis is missing. Indicate the 10 closest POS locations: <a href="cer_application_distribution.php?pid=' .  param("pid") .'">check it</a>';

	$ln_errorlines[5] = 'Information of at least one selected POS is incomplete: <a href="cer_application_distribution.php?pid=' .  param("pid") .'">check it</a>';
}


if($form_type == 'CER') {
	$ln_errorlines[6] = 'Information about other SG brands is incomplete: <a href="cer_application_brands.php?pid=' .  param("pid") .'">check it</a>';
}
else {
	$ln_errorlines[6] = 'Information about other SG brands is incomplete: <a href="cer_application_brands.php?pid=' .  param("pid") .'&context=ln">check it</a>';
}

$ln_errorlines[7] = 'Information about passed rental conditions is missing: <a href="ln_general.php?pid=' .  param("pid") .'">check it</a>';

$ln_errorlines[8] = 'Document explaining past sellouts is missing: <a href="ln_general.php?pid=' .  param("pid") .'">check it</a>';

$ln_errorlines[9] = 'Information explaining closing costs is missing: <a href="ln_general.php?pid=' .  param("pid") .'">check it</a>';

$ln_errorlines[10] = 'Relation Information is incomplete: <a href="ln_application_relation.php?pid=' .  param("pid") .'">check it</a>';


//general information
if(!$ln_basicdata['ln_basicdata_remarks']
	or !$ln_basicdata['ln_basicdata_remarks']
	or !$ln_basicdata['ln_basicdata_remarks1']
	or !$ln_basicdata['ln_basicdata_remarks2']
	or !$ln_basicdata['ln_basicdata_remarks3']
	or !$ln_basicdata['ln_basicdata_remarks4']) {
	$ln_errors[0] = array(0);
}

//passed rent
if($form_type == 'CER'
	or $form_type == 'AF') {
	if(($project["project_projectkind"] == 2 
		or $project["project_projectkind"] == 3
		or $project["project_projectkind"] == 4
		or $project["project_projectkind"] == 5
		or $project["project_projectkind"] == 9)) // renovation or tekover/renovation or lease renewal
	{
		if(!$ln_basicdata['ln_basicdata_passedrent']) {
			$ln_errors[6] = array(7);
		}
	}
}

//attachments
if($form_type == 'AF' 
	or $form_type == 'INR03') {
	if(!$ln_basicdata['ln_basicdata_floorplan']
		or !$ln_basicdata['ln_basicdata_pos_section_image']
		or !$ln_basicdata['ln_basicdata_location_layout']
		or !$ln_basicdata['ln_basicdata_fassade_image']
		or !$ln_basicdata['ln_basicdata_pix1']
		or !$ln_basicdata['ln_basicdata_pix2']
		or !$ln_basicdata['ln_basicdata_pix3']
		or !$ln_basicdata['ln_basicdata_pix4']) {
		$ln_errors[1] = array(1);
	}
}
elseif($form_type == 'CER') {
	if(!$ln_basicdata['ln_basicdata_floorplan']
		or !$ln_basicdata['ln_basicdata_pos_section_image']
		or !$ln_basicdata['ln_basicdata_location_layout']
		or !$ln_basicdata['ln_basicdata_fassade_image']
		or !$ln_basicdata['ln_basicdata_draft_aggreement']
		or !$ln_basicdata['ln_basicdata_pix1']
		or !$ln_basicdata['ln_basicdata_pix2']
		or !$ln_basicdata['ln_basicdata_pix3']
		or !$ln_basicdata['ln_basicdata_pix4']) {
		$ln_errors[1] = array(1);
	}
}

//project information
if($project['project_projectkind'] != 5) // lease reneval
{
	if(!$ln_basicdata['ln_basicdata_area']
		or !$ln_basicdata['ln_basicdata_economics']
		or !$ln_basicdata['ln_basicdata_cityinfo']
		or !$ln_basicdata['ln_basicdata_basic_developer']
		or !$ln_basicdata['ln_basicdata_parking_space']
		or !$ln_basicdata['ln_basicdata_basic_info']
		or !$ln_basicdata['ln_basicdata_positioning']
		or !$ln_basicdata['ln_basicdata_target_customers']
		or !$ln_basicdata['ln_basicdata_total_floor_area']
		or !$ln_basicdata['ln_basicdata_project_specials']) {
		$ln_errors[2] = array(2);
	}
}

//closing costs in the context of a relocation
if($project['project_projectkind'] == 6) // relocation
{
	if(!$ln_basicdata['ln_basicdata_relocation_costs']
		or !$ln_basicdata['ln_basicdata_relocation_comment']
		or !$ln_basicdata['ln_basicdata_relocation_doc']) {
			$ln_errors[7] = array(9);
	}
}


//franchisee information in case of a franchisee
if($form_type == 'AF') // franchisee, independent retailer
{
	if(!$project['project_fagagreement_type']
		or !$project['project_fagrstart']
		or !$project['project_fagrend']
		or !$project['project_fag_comment']) {
		$ln_errors[8] = array(10);
	}
}

//sellout document
if($form_type != 'INR03')
{
	if(($project["project_projectkind"] == 2 
			or $project["project_projectkind"] == 3
			or $project["project_projectkind"] == 5)) // renovation or tekover/renovation or lease renewal
	{
		if(!$ln_basicdata['ln_basicdata_sales_doc']) {

			$ln_errors[1] = array(8);
		}
	}
}

//sellouts
$sellout_necessary = 0;
if($form_type != 'INR03') {

	
	$sellout_data_ok = true;
	$project_id = param("pid");
	$project_order = $project["project_order"];

	if(($project["project_projectkind"] == 2 
			or $project["project_projectkind"] == 3
			or $project["project_projectkind"] == 5)) // renovation or tekover/renovation or lease renewal
	{
		$sellout_necessary = 1;
	}
	elseif($project["project_projectkind"] == 6 and $project["project_relocated_posaddress_id"] > 0) // relocation
	{
		$latest_project = get_latest_pos_project($project["project_relocated_posaddress_id"]);
		
		if(count($latest_project) > 0)
		{
			$sellout_necessary = 1;
			$project_id = $latest_project["project_id"];
			$project_order = $latest_project["order_id"];
		}
	}

	if($sellout_necessary == 1)
	{
		$sellout_data_ok = check_sellout_data($project_order, $project["project_id"], $cer_basicdata["cer_basicdata_firstyear"]);
	}
}

if($sellout_necessary == 1 
	and $sellout_data_ok == false) {
	$ln_errors[3] = array(3);
}


//Distibution Analysis
$distribution_analysis = true;
$distribution_analysis2 = true;
$distribution_analysis3 = true;
if($form_type != 'INR03') {

	//get the number of operating POS locations
	$num_of_operating_pos_locations = 0;
	$sql = "select count(posaddress_id) as num_recs " . 
		   " from posaddresses " . 
		   " where posaddress_client_id = " . dbquote($client_address["id"]) . 
		   " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " . 
		   " and posaddress_country = " . dbquote($project["order_shop_address_country"]);

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$num_of_operating_pos_locations = $row['num_recs'];

	if($form_type == "CER" 
		and $ln_basicdata["ln_no_ln_submission_needed"] != 1 
		and ($ln_basicdata["ln_basicdata_submitted"] == NULL 
			or substr($ln_basicdata["ln_basicdata_submitted"],0,4) == '0000') )
	{
		//submission possible without distribution analysis

		$sql = "select count(ln_basicdata_lnr03_id) as num_recs " .
			   "from ln_basicdata_inr03 " .
			   " where ln_basicdata_lnr03_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"] .
			   " and ln_basicdata_lnr03_cer_version = 0 ";

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		
		if($row['num_recs'] == 0) {
			$distribution_analysis = false;
		}
		elseif($num_of_operating_pos_locations >= 10 
			and $row['num_recs'] < 10)
		{
			$distribution_analysis = false;
		}
		elseif($row['num_recs'] < 10 
			and $row['num_recs'] < $num_of_operating_pos_locations)
		{
			$distribution_analysis = false;
		}
	}
	elseif($form_type == "AF")
	{
		$sql = "select count(ln_basicdata_lnr03_id) as num_recs " .
			   "from ln_basicdata_inr03 " .
			   " where ln_basicdata_lnr03_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"] .
			   " and ln_basicdata_lnr03_cer_version = 0 ";

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		if($row['num_recs'] == 0) {
			$distribution_analysis = false;
		}
		elseif($num_of_operating_pos_locations >= 10 and $row['num_recs'] < 10)
		{
			$distribution_analysis = false;

			
		}
		elseif($row['num_recs'] < 10 and $row['num_recs'] < $num_of_operating_pos_locations)
		{
			$distribution_analysis = false;
		}
	}



	//check if data is missing in distribution analysis
	$distribution_analysis2 = true;
	$sql = "select count(ln_basicdata_lnr03_id) as num_recs " .
		   "from ln_basicdata_inr03 " .
		   " where ln_basicdata_lnr03_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"] .
		   " and ln_basicdata_lnr03_cer_version = 0 " . 
		   " and ln_basicdata_lnr03_posaddress_id_da > 0 " .
		   " and (ln_basicdata_lnr03_year = 0 or ln_basicdata_lnr03_year is null or ln_basicdata_lnr03_year = '' ".
		   " or ln_basicdata_lnr03_month = 0 or ln_basicdata_lnr03_month is null or ln_basicdata_lnr03_month = '' " . 
		   " or ln_basicdata_lnr03_distance_unit = 0 or ln_basicdata_lnr03_distance_unit is null or ln_basicdata_lnr03_distance_unit = '' " . 
		   " or ln_basicdata_lnr03_distance = 0 or ln_basicdata_lnr03_distance is null or ln_basicdata_lnr03_distance = '' " .
		   " or ln_basicdata_lnr03_watches_units = 0 or ln_basicdata_lnr03_watches_units is null or ln_basicdata_lnr03_watches_units = '' " . 
		   ")";
		   
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if($row['num_recs'] > 0) {
		$distribution_analysis2 = false;
	}

		
	$sql = "select count(ln_basicdata_lnr03_brand_id) as num_recs " . 
		   " from ln_basicdata_lnr03_brands " . 
			" where ln_basicdata_lnr03_brand_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"] .
			" and ln_basicdata_lnr03_brand_cer_version = 0 ";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($row['num_recs'] == 0 
		and $cer_basicdata["cer_basicdata_no_sg_brands"] == 0) {
		$distribution_analysis3 = false;
	}
}

$tmp = array();
if($distribution_analysis == false)
{
	$tmp[] = 4;
}

if($distribution_analysis2 == false)
{
	$tmp[] = 5;
}


if(count($tmp) > 0) {
	$ln_errors[4] = $tmp;
}

if($distribution_analysis3 == false) {
	$ln_errors[5] = array(6);
}



/****************************************
CER/INR03 Form
*****************************************/
$cer_errors = array();
$cer_captions[0] = "General Information";
$cer_captions[1] = "CER versus LNR";

$cer_errorlines[0] = 'CER general information is incomplete: <a href="cer_general.php?pid=' .  param("pid") .'">check it</a>';

$cer_errorlines[1] = 'The investment, revenue and expense data of your LNR and your CER does not match: <a href="cer_versus_ln.php?pid=' .  param("pid") .'">check it and add your comments</a>';

$cer_errorlines[2] = 'The investment and revenue data of your LNR and your CER does not match: <a href="cer_versus_ln.php?pid=' .  param("pid") .'">check it and add your comments</a>';

$cer_errorlines[3] = 'The investment and expense data of your LNR and your CER does not match: <a href="cer_versus_ln.php?pid=' .  param("pid") .'">check it and add your comments</a>';

$cer_errorlines[4] = 'The revenue and expense data of your LNR and your CER does not match: <a href="cer_versus_ln.php?pid=' .  param("pid") .'">check it and add your comments</a>';

$cer_errorlines[5] = 'The investment data of your LNR and your CER does not match: <a href="cer_versus_ln.php?pid=' .  param("pid") .'">check it and add your comments</a>';

$cer_errorlines[6] = 'The revenue data of your LNR and your CER does not match: <a href="cer_versus_ln.php?pid=' .  param("pid") .'">check it and add your comments</a>';

$cer_errorlines[7] = 'The expense data of your LNR and your CER does not match: <a href="cer_versus_ln.php?pid=' .  param("pid") .'">check it and add your comments</a>';

//general information
if($form_type == 'CER'
    and (!$cer_basicdata['cer_summary_in01_description']
	or !$cer_basicdata['cer_summary_in01_benefits'])) {
	$cer_errors[0] = array(0);
}

//ln versus cer
$investments_delta_limit = 100;
$revenues_delta_limit = 100;
$expenses_delta_limit = 100;
if($form_type == 'CER') {
	if($ln_basicdata["ln_no_ln_submission_needed"] == 0)
	{
		$state = is_there_a_difference_between_cer_and_ln(param("pid"));
		$difference_between_cer_and_ln = $state['state'];

		$delta_limit_in_percent = 0.05;
		$investments_delta = round($state['total_investments_cer'] - $state['total_investments_ln'],0);

	
		if($investments_delta == 0)
		{
			$investments_delta_limit = 0;
		}
		elseif($state['total_investments_cer'])
		{
			$investments_delta_limit = abs($state['total_investments_cer'] - $state['total_investments_ln']) / $state['total_investments_cer'];
			$investments_delta_limit = round(100*$investments_delta_limit, 0);

			if($investments_delta < 0 or abs($investments_delta_limit) < 100*$delta_limit_in_percent)
			{
				$investments_delta_limit = 0;
			}
		}



		$revenues_delta = round($state['total_revenues_cer'] - $state['total_revenues_ln'],0);


		if($revenues_delta == 0)
		{
			$revenues_delta_limit = 0;
		}
		elseif($state['total_revenues_cer'])
		{
			$revenues_delta_limit = abs($state['total_revenues_cer'] - $state['total_revenues_ln']) / $state['total_revenues_cer'];
			$revenues_delta_limit = round(100*$revenues_delta_limit, 0);

			if(abs($revenues_delta_limit) < 100*$delta_limit_in_percent)
			{
				$revenues_delta_limit = 0;
			}
		}

		$expenses_delta = round($state['total_expenses_cer'] - $state['total_expenses_ln'],0);

		
		if($expenses_delta == 0)
		{
			$expenses_delta_limit = 0;
		}
		elseif($state['total_expenses_cer'])
		{
			$expenses_delta_limit = abs($state['total_expenses_cer'] - $state['total_expenses_ln']) / $state['total_expenses_cer'];
			$expenses_delta_limit = round(100*$expenses_delta_limit, 0);

			if(abs($expenses_delta_limit) < 100*$delta_limit_in_percent)
			{
				$expenses_delta_limit = 0;
			}
		}
	}
	else
	{
		$investments_delta_limit = 0;
		$revenues_delta_limit = 0;
		$expenses_delta_limit = 0;
	}

}

if($ln_ok1 == true 
	and $project["project_cost_type"] == 1
	and $investments_delta_limit > 0
	and $revenues_delta_limit != 0
	and $expenses_delta_limit > 0
	and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment"]) < 20
	 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment2"]) < 20
	 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment3"]) < 20))
{
	$cer_errors[1] = array(1);
}
elseif($ln_ok1 == true 
	and $project["project_cost_type"] == 1
	and $investments_delta_limit > 0
	and $revenues_delta_limit != 0
	and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment4"]) < 20
	 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment5"]) < 20))
{
	$cer_errors[1] = array(2);
}
elseif($ln_ok1 == true and $project["project_cost_type"] == 1
	and $investments_delta_limit > 0
	and $expenses_delta_limit > 0
	and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment4"]) < 20
	 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment5"]) < 20))
{
	$cer_errors[1] = array(3);
}
elseif($ln_ok1 == true and $project["project_cost_type"] == 1
	and $revenues_delta_limit != 0
	and $expenses_delta_limit > 0
	and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment4"]) < 20
	 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment5"]) < 20))
{
	$cer_errors[1] = array(4);
}
elseif($ln_ok1 == true and $project["project_cost_type"] == 1
	and $investments_delta_limit > 0
	and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment4"]) < 20
	 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment5"]) < 20))
{
	$cer_errors[1] = array(5);
}
elseif($ln_ok1 == true and $project["project_cost_type"] == 1
	and $revenues_delta_limit != 0
	and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment4"]) < 20
	 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment5"]) < 20))
{
	$cer_errors[1] = array(6);
}
elseif($ln_ok1 == true and $project["project_cost_type"] == 1
	and $expenses_delta_limit > 0
	and (strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment4"]) < 20
	 or strlen($cer_basicdata["cer_basicdata_cer_versus_ln_comment5"]) < 20))
{
	$cer_errors[1] = array(7);
}


//check if budget was approved
$budget_was_approved = true;

if($form_type == 'CER'
	or $form_type == 'INR03') // corporate
{
	$sql_step = "select count(actual_order_state_id) as num_recs " . 
				" from actual_order_states " . 
				" where actual_order_state_order = " . $project["order_id"] . 
				" and actual_order_state_state in (34, 105)"; // budget approbval 620


	$res = mysql_query($sql_step) or dberror($sql_step);
	$row = mysql_fetch_assoc($res);
	if($row["num_recs"] == 0)
	{
		$budget_was_approved = false;
	}

	//budget was not approved or unfrezed
	if($project["order_budget_is_locked"] != 1) {
		$budget_was_approved = false;
	}
}


//check if LN was submitted
if($form_type == "CER" 
	and $ln_basicdata["ln_no_ln_submission_needed"] == 1 
	or ($ln_basicdata["ln_basicdata_submitted"] != NULL 
	and substr($ln_basicdata["ln_basicdata_submitted"],0,4) != '0000'
	and $ln_basicdata["ln_basicdata_rejected"] == NULL 
	and substr($ln_basicdata["ln_basicdata_rejected"],0,4) == '0000')
	)
{
	$cer_submission_possible = 0;
}


if(count($business_plan_errors) > 0 
	or count($ln_errors) > 0) {
	$ln_ok = false;
	$ln_submission_possible = 0;
	$cer_submission_possible = 0;
}


//check if cer submission is possible
$cer_submission_possible = 0;
if(($form_type == 'CER' or $form_type == 'AF')
   and $ln_was_approved == 1
	) {
	
	$cer_submission_possible = 1;
}

if(count($cer_errors) > 0) {
	$cer_submission_possible = 0;
}


?>