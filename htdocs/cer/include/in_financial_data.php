<?php

if( !function_exists ( "calculate_depreciation" ))
{
	include("calculate_depreciation.php");
}

//get data
if(param("pid") > 0 )
{
	$project = get_project(param("pid"));
}

//*change of calculation model in 2012
$calc_model = 2012;

if(param("pid") > 0 )
{
	
	$ln_version_data = get_latest_ln_version(param("pid") );
	$latest_ln_version = $ln_version_data["cer_basicdata_version"];
	$latest_ln_id = $ln_version_data["ln_basicdata_id"];
	$cer_basicdata_ln = get_cer_basicdata(param("pid"), $latest_ln_version);
	
	$project = get_project(param("pid"));
	//get pos data
	if($project["pipeline"] == 0)
	{
		$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
	}
	elseif($project["pipeline"] == 1)
	{
		$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
	}

	$total_surface = $project["project_cost_totalsqms"];
	$sales_surface = $project["project_cost_sqms"];
	//$other_surface = $project["project_cost_backofficesqms"] + $project["project_cost_othersqms"];
	$other_surface = $project["project_cost_backofficesqms"];
	$planned_opening_date = to_system_date($project["project_real_opening_date"]);

	$milestone_ln_approved = get_project_milestone($project["project_id"], 13);
	$ln_approved = to_system_date($milestone_ln_approved["project_milestone_date"]);
	
	$cost_of_production_values = update_cost_of_products_sold(param("pid"), $calc_model, $cer_version);
	if($project["order_date"] < '2012-01-24' and $project["order_actual_order_state_code"] >= 840)
	{
		$calc_model = 2008;
	}

	//get sellouts of existing pos locations
	$sellouts_watches = array();
	$sellouts_bjoux = array();
	$sellouts_months = array();

	$grosssales_watches = array();
	$grosssale_bjoux = array();
	$net_sales = array();

	$operating_income1 = array();
	$wsm = array();
	$operating_income1 = array();

	$sql = "select posorder_posaddress " . 
		   "from posorders " . 
		   "where posorder_order = " . $project["project_order"];

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$pos_id = $row["posorder_posaddress"];


	if(!$pos_id and $project["project_relocated_posaddress_id"] > 0)
	{
		$latest_project = get_latest_pos_project($project["project_relocated_posaddress_id"]);
		
		if(count($latest_project) > 0)
		{
			$old_project = get_project($latest_project["project_id"]);

			$sql = "select posorder_posaddress " . 
				   "from posorders " . 
				   "where posorder_order = " . $old_project["project_order"];

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			$pos_id = $row["posorder_posaddress"];
		}

	}

	$sql = "select * from cer_sellouts " . 
		   "where cer_sellout_version = " . dbquote($cer_version) . 
		   " and cer_sellout_project_id = " . dbquote($project["project_id"]) . 
		   " order by cer_sellout_year ASC";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$sellouts_watches[$row["cer_sellout_year"]] = $row["cer_sellout_watches_units"];
		$sellouts_bjoux[$row["cer_sellout_year"]] = $row["cer_sellout_bijoux_units"];
		$sellouts_months[$row["cer_sellout_year"]] = $row["cer_sellout_month"];


		$grosssales_watches[$row["cer_sellout_year"]] = $row["cer_sellout_watches_grossales"];
		$grosssale_bjoux[$row["cer_sellout_year"]] = $row["cer_sellout_bijoux_grossales"];
		$net_sales[$row["cer_sellout_year"]] = $row["cer_sellout_net_sales"];

		$operating_incomes1[$row["cer_sellout_year"]] = $row["cer_sellout_operating_income_excl"];
		$wsm[$row["cer_sellout_year"]] = $row["cer_sellout_wholsale_margin"];
		$operating_incomes2[$row["cer_sellout_year"]] = $row["cer_sellout_operating_income_incl"];
	}


}
else
{
	$cost_of_production_values = update_cost_of_products_sold(param("did"));

	$total_surface = $cer_basicdata["cer_basicdata_sqms"];
	$sales_surface = "";
	$other_surface = "";
}

if(param("pid") > 0 )
{

	$cer_basicdata = get_cer_basicdata(param("pid"), $cer_version);
	$_depreciation = calculate_depreciation($cer_basicdata, param("pid"), 0, $cer_version);
	$_table_revenues = "cer_revenues";
	$_table_expenses = "cer_expenses";
	$_table_salaries = "cer_salaries";
	$_table_investments = "cer_investments";
	$_table_paymentterms = "cer_paymentterms";
	$_table_stocks = "cer_stocks";
	$_id = param("pid");
	$_id_suffix = "project";
	$cer_version_filter = true;

	$_filter_revenues = "cer_revenue_cer_version = $cer_version and ";
	$_filter_expenses = "cer_expense_cer_version = $cer_version and ";
	$_filter_salaries = "cer_salary_cer_version = $cer_version and ";
	$_filter_investments = "cer_investment_cer_version = $cer_version and ";
	$_filter_paymentterms = "cer_paymentterm_cer_version = $cer_version and ";
	$_filter_stocks = "cer_stock_cer_version = $cer_version and ";

	$_filter_revenues_ln = "cer_revenue_cer_version = $latest_ln_version and ";
	$_filter_expenses_ln = "cer_expense_cer_version = $latest_ln_version and ";
	
}
else
{
	$cer_basicdata = get_draft_basicdata(param("did"));
	$_depreciation = calculate_depreciation($cer_basicdata, param("did"), 1);
	$_table_revenues = "cer_draft_revenues";
	$_table_expenses = "cer_draft_expenses";
	$_table_salaries = "cer_draft_salaries";
	$_table_investments = "cer_draft_investments";
	$_table_paymentterms = "cer_draft_paymentterms";
	$_table_stocks = "cer_draft_stocks";
	$_id = param("did");
	$_id_suffix = "draft_id";
	$cer_version_filter = false;

	$_filter_revenues = "";
	$_filter_expenses = "";
	$_filter_salaries = "";
	$_filter_investments = "";
	$_filter_paymentterms = "";
	$_filter_stocks = "";

	$_filter_expenses_ln = "";
}


$cer_basicdata_interest_rates = unserialize($cer_basicdata["cer_basic_data_interesrates"]);
$cer_basicdata_inflation_rates = unserialize($cer_basicdata["cer_basicdata_inflationrates"]);

$interest_rate_tmp = 0;
if(array_key_exists($cer_basicdata["cer_basicdata_firstyear"],$cer_basicdata_interest_rates))
{
	$interest_rate_tmp = $cer_basicdata_interest_rates[$cer_basicdata["cer_basicdata_firstyear"]];
}

$inflation_rate_tmp = 0;
if(!array_key_exists($cer_basicdata["cer_basicdata_firstyear"],$cer_basicdata_inflation_rates))
{
	foreach($cer_basicdata_inflation_rates as $year=>$rate)
	{
		$inflation_rate_tmp = $rate;
	}
}

//years and financial figures

$first_full_year = $cer_basicdata["cer_basicdata_firstyear"];
if($cer_basicdata["cer_basicdata_firstmonth"] > 1)
{
	$first_full_year = $cer_basicdata["cer_basicdata_firstyear"]+1;
}

$first_year = $cer_basicdata["cer_basicdata_firstyear"];
$first_month = $cer_basicdata["cer_basicdata_firstmonth"];
$last_year = $cer_basicdata["cer_basicdata_lastyear"];
$last_month = $cer_basicdata["cer_basicdata_lastmonth"];

if(!$last_year)
{
	$last_year = $first_year + 5;
	$last_month = 12;
}

$years = array();

for($y=$first_year;$y<=$last_year;$y++)
{
	$years[] = $y;

	if(!array_key_exists($y,$cer_basicdata_interest_rates))
	{
		$cer_basicdata_interest_rates[$y] = $interest_rate_tmp;
	}
	else
	{
		$interest_rate_tmp = $cer_basicdata_interest_rates[$y];
	}

	if(!array_key_exists($y,$cer_basicdata_inflation_rates))
	{
		$cer_basicdata_inflation_rates[$y] = $inflation_rate_tmp;
	}
	else
	{
		$inflation_rate_tmp = $cer_basicdata_inflation_rates[$y];
	}
}

$number_of_years = count($years);


//get intangibles
$intagible_amount = 0;
$intagible_name = "Key-/Premium Money/Goodwill";
$intagible_depryears = 0;
$cer_totals = 0;
$keymoney = 0;

$tmp_filter = "";
if($cer_version_filter == true)
{
	$tmp_filter = " and cer_investment_cer_version = " . $cer_version . " ";
}
$sql = "select * from " . $_table_investments .
	   " left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		"where posinvestment_type_intangible = 1 " . 
		$tmp_filter .
		"and cer_investment_" . $_id_suffix . " = " . $_id;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	
	$cer_totals = $cer_totals + $row["cer_investment_amount_cer_loc"];

	//keymoney
	if($row["cer_investment_type"] == 15)
	{
		$keymoney = $row["cer_investment_amount_cer_loc"];
	}
	elseif($row["cer_investment_type"] == 19) // landlord's contribution
	{
		$landloard_contribution = $landloard_contribution - $row["cer_investment_amount_cer_loc"];
	}

	if($cer_basicdata["cer_extend_depreciation_period"] == 0 and $row["cer_investment_depr_years"] > $number_of_years)
	{
		if((13-$first_month+$last_month) == 12)
		{
			$intagible_depryears = ($last_year-$first_year) . 'Y-0m';
		}
		else
		{
			$intagible_depryears = ($last_year-$first_year-1) . 'Y-' . (13-$first_month+$last_month) . 'm';
		}
	}
	elseif($cer_basicdata["cer_extend_depreciation_period"] == 1 and $row["cer_investment_depr_years"] > $number_of_years)
	{
		$intagible_depryears = $row["cer_investment_depr_years"] . 'y-' . $row["cer_investment_depr_months"]. 'm';
	}
	else
	{
		$intagible_depryears = $row["cer_investment_depr_years"] . 'y-' . $row["cer_investment_depr_months"]. 'm';
	}


	
}

$intagibles_total = $cer_totals;
$intagible_amount = $intagibles_total;


//get all investments
$fixed_assets = array(0=>1, 1=>3,2=>5,3=>7, 4=>11, 5=>18 );
$fixed_assets2 = array(0=>1, 1=>3,2=>5,3=>7, 4=>11, 5=>18, 6=>20 );

$amounts = array();
$investment_names = array();
$depryears = array();
$deprmonths = array();
$investment_total = 0;
$deposit = 0;
$other_noncapitalized_cost = 0;
$dismantling_costs = 0;
$landloard_contribution = 0;

$fixed_assets_total = 0;
$construction_total = 0;
$other_costs = 0;
$transportation_total = 0;
$equipment_costs = 0;

$tmp_filter = "";
if($cer_version_filter == true)
{
	$tmp_filter = " and cer_investment_cer_version = " . $cer_version . " ";
}

$sql = "select * from  " . $_table_investments . 
	   " left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
	   "where posinvestment_type_intangible = 0 " . 
	   $tmp_filter . 
	   " and cer_investment_" . $_id_suffix . " = " . $_id . 
	   " order by posinvestment_type_sortorder";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$amounts[$row["posinvestment_type_id"]] = $row["cer_investment_amount_cer_loc"];
	$investment_names[$row["posinvestment_type_id"]] = $row["posinvestment_type_name"];
	
	if($cer_basicdata["cer_extend_depreciation_period"] == 0 and $row["cer_investment_depr_years"] > $number_of_years)
	{

		if((13-$first_month+$last_month) == 12)
		{
			$depryears[$row["posinvestment_type_id"]] = $last_year-$first_year;
			$deprmonths[$row["posinvestment_type_id"]] = 0;
		}
		else
		{
			$depryears[$row["posinvestment_type_id"]] = $last_year-$first_year-1;

			$tmp = 13-$first_month+$last_month;
			if($tmp == 12)
			{
				$deprmonths[$row["posinvestment_type_id"]] = 0;
				$depryears[$row["posinvestment_type_id"]] = $depryears[$row["posinvestment_type_id"]] + 1;
			}
			elseif($tmp > 12)
			{
				$deprmonths[$row["posinvestment_type_id"]] = $tmp - 12;
				$depryears[$row["posinvestment_type_id"]] = $depryears[$row["posinvestment_type_id"]] + 1;
			}
			else
			{
				$deprmonths[$row["posinvestment_type_id"]] = $tmp;
			}

		    
		}
	}
	elseif($cer_basicdata["cer_extend_depreciation_period"] == 1 and $row["cer_investment_depr_years"] > $number_of_years)
	{
		$depryears[$row["posinvestment_type_id"]] = $row["cer_investment_depr_years"];
		$deprmonths[$row["posinvestment_type_id"]] = $row["cer_investment_depr_months"];
	}
	else
	{
		$depryears[$row["posinvestment_type_id"]] = $row["cer_investment_depr_years"];
		$deprmonths[$row["posinvestment_type_id"]] = $row["cer_investment_depr_months"];
	}

	
	if($row["posinvestment_type_id"] == 19)
	{
		$cer_totals = $cer_totals - $row["cer_investment_amount_cer_loc"];
	}
	else
	{
		$cer_totals = $cer_totals + $row["cer_investment_amount_cer_loc"];
	}
	

	if(in_array($row["posinvestment_type_id"], $fixed_assets))
	{
		$investment_total = $investment_total + $row["cer_investment_amount_cer_loc"];
	}

	if($row["posinvestment_type_id"] == 9) // deposit
	{
		$deposit = $row["cer_investment_amount_cer_loc"];
	}
	elseif($row["posinvestment_type_id"] == 13) // othe non capitalized cost
	{
		$other_noncapitalized_cost = $row["cer_investment_amount_cer_loc"];
	}
	elseif($row["posinvestment_type_id"] == 18) // dismantling
	{
		$dismantling_costs = $dismantling_costs + $row["cer_investment_amount_cer_loc"];
	}
	elseif($row["posinvestment_type_id"] == 19) // landlord's contribution
	{
		$landloard_contribution = $landloard_contribution - $row["cer_investment_amount_cer_loc"];
	}
	elseif($row["posinvestment_type_id"] == 20) // transportation
	{
		$transportation_total = $transportation_total + $row["cer_investment_amount_cer_loc"];
		$investment_total = $investment_total + $row["cer_investment_amount_cer_loc"];
	}

	if($row["posinvestment_type_id"] == 1
		or $row["posinvestment_type_id"] == 5
		or $row["posinvestment_type_id"] == 7) // construction
	{
		$construction_total = $construction_total + $row["cer_investment_amount_cer_loc"];
	}
	elseif($row["posinvestment_type_id"] == 3) // furniture
	{
		$fixed_assets_total = $fixed_assets_total + $row["cer_investment_amount_cer_loc"];
	}
	elseif($row["posinvestment_type_id"] == 11
		or $row["posinvestment_type_id"] == 12
		or $row["posinvestment_type_id"] == 23) // other
	{
		$other_costs = $other_costs + $row["cer_investment_amount_cer_loc"];
	}
	elseif($row["posinvestment_type_id"] == 7) // other
	{
		$equipment_costs = $equipment_costs + $row["cer_investment_amount_cer_loc"];
	}

}


// correct investments by adding merchandising and transportation
$correction_done = false;
$sql = "select * from  " . $_table_investments . 
	   " left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
	   "where posinvestment_type_intangible = 0 " . 
	   $tmp_filter . 
	   " and cer_investment_" . $_id_suffix . " = " . $_id . 
	   " order by posinvestment_type_sortorder";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if($row['cer_investment_child_amounts']) {
		
		$child_amounts = unserialize($row['cer_investment_child_amounts']);
		foreach($child_amounts as $key=>$value) {
			$amounts[$row['cer_investment_type']] = $amounts[$row['cer_investment_type']]  + $value;
			$correction_done = true;
		}
	}
}
if($correction_done == false) {
	$tmp_amount = 0;
	foreach($amounts as $itype=>$amount)
	{
		if($itype == 20) // other costs
		{
			$tmp_amount = $tmp_amount + $transportation_total;
		}
	}
	if(array_key_exists(3, $amounts)) {
		$amounts[3] = $amounts[3] + $tmp_amount;
	}
}

$investment_total = $investment_total;
$cer_totals = $cer_totals;
$renewal_option = "";
$epnr = "";

$number_of_months_first_year_lease_contract = '';
$contract_duration = "";
//get rental details
if(param("pid") > 0 )
{
	$posdata = get_pos_data($project["project_order"]);
	$epnr = $posdata["posaddress_eprepnr"];


	$posleases = get_pos_leasedata($posdata["posaddress_id"], $project["project_order"]);

	if(count($posleases) > 0)
	{
		$duration_in_years = $posleases["poslease_enddate"] - $posleases["poslease_startdate"];

		if(substr($posleases["poslease_startdate"], 5,2) == substr($posleases["poslease_enddate"], 5,2)) {
			$tmp1 = 12;
		}
		else
		{
			$tmp1 = 13 - substr($posleases["poslease_startdate"], 5,2) + substr($posleases["poslease_enddate"], 5,2);
		}

		$tmp2 = ($duration_in_years - 1)*12;
		$duration_in_years = round(($tmp1 + $tmp2) / 12, 1);

		$duration_in_years_and_months = floor(($tmp1 + $tmp2) / 12);
		$duration_in_years_and_months = $duration_in_years_and_months . "y-" . (($tmp1 + $tmp2) - ($duration_in_years_and_months*12)) . "m";
		

		$number_of_months_first_year_lease_contract = 13 - substr($posleases["poslease_startdate"], 5,2);

		$d1 = new DateTime($posleases["poslease_startdate"]);
		$d2 = new DateTime($posleases["poslease_enddate"]);
		$interval = date_diff($d2, $d1);

		$contract_duration = round($interval->days / 365, 1);

		$rental_period = substr($posleases["poslease_startdate"], 5,2) . '-' . substr($posleases["poslease_startdate"], 0,4) . '/' . substr($posleases["poslease_enddate"], 5,2) . '-' . substr($posleases["poslease_enddate"], 0,4) .  ' (' . $contract_duration . ')';
		

		$lease_duration = $duration_in_years_and_months;

		if($posleases["poslease_extensionoption"] != Null and $posleases["poslease_extensionoption"] != '0000-00-00')
		{
			$renewal_option = $posleases["poslease_extensionoption"] - $posleases["poslease_enddate"];
		}
		
		$exit_option = "-";
		if($posleases["poslease_exitoption"] != Null and $posleases["poslease_exitoption"] != '0000-00-00')
		{
			$exit_option = to_system_date($posleases["poslease_exitoption"]);
		}

		$hand_over_key = "-";
		if($posleases["poslease_handoverdate"] != Null and $posleases["poslease_handoverdate"] != '0000-00-00')
		{
			$hand_over_key = to_system_date($posleases["poslease_handoverdate"]);
		}

		$first_rent_payed = "-";
		if($posleases["poslease_firstrentpayed"] != Null and $posleases["poslease_firstrentpayed"] != '0000-00-00')
		{
			$first_rent_payed = to_system_date($posleases["poslease_firstrentpayed"]);
		}

		$m = substr($posleases["poslease_startdate"], 5, 2);
		$d = substr($posleases["poslease_startdate"], 8, 2);
		$y = substr($posleases["poslease_startdate"], 0, 4);
		$d1=mktime(0,0,0,$m,$d,$y);
		$m = substr($posleases["poslease_enddate"], 5, 2);
		$d = substr($posleases["poslease_enddate"], 8, 2);
		$y = substr($posleases["poslease_enddate"], 0, 4);
		$d2=mktime(0,0,0,$m,$d,$y);

		if($d2-$d1 > 0)
		{
			$termination_time = floor(($d2-$d1)/2628000);
		}


		$index_clause = "no";
		if($posleases["poslease_indexclause_in_contract"] == 1)
		{
			$index_clause = "yes";
		}

		$index_clause2 = "no";
		if($posleases["poslease_isindexed"] == 1)
		{
			$index_clause2 = "yes";
		}

		if($posleases["poslease_tacit_renewal_duration_years"] > 0)
		{
			$index_clause2 .= " / ".  $posleases["poslease_tacit_renewal_duration_years"] . "Years";

			if($posleases["poslease_tacit_renewal_duration_months"] > 0)
			{
				$index_clause2 .= ", " . $posleases["poslease_tacit_renewal_duration_months"] . "Months";
			}
		}
		elseif($posleases["poslease_tacit_renewal_duration_months"] > 0)
		{
			$index_clause2 .= " / ".  $posleases["poslease_tacit_renewal_duration_months"] . "Months";
		}

		

		//no calculation but entering in the rental tab
		$termination_time = $posleases["poslease_termination_time"];

		if($posleases["poslease_realestate_fee"] > 0)
		{
			$tmp = $keymoney*$posleases["poslease_realestate_fee"]/100;
			$intagibles_total = $intagibles_total + $tmp;
			$intagible_amount = $intagible_amount + $tmp;
			$cer_totals = $cer_totals + $tmp;
		}
	}
	else
	{
		$num_of_full_years = $cer_basicdata["cer_basicdata_lastyear"] - $cer_basicdata["cer_basicdata_firstyear"] - 1;
		$num_of_months = (13 - $cer_basicdata["cer_basicdata_firstmonth"]) + $cer_basicdata["cer_basicdata_lastmonth"] + 12 * $num_of_full_years;
		
		
		$duration_in_years = $num_of_months / 12;
		$renewal_option = "";
		$exit_option = "";
		$termination_time = "";
		$index_clause = "";
		$index_clause2 = "";

		$rental_period = '';
		$lease_duration = '';


	}
}
else
{
	if($cer_basicdata['cer_basicdata_lease_startdate'] != NULL and $cer_basicdata['cer_basicdata_lease_startdate'] != '0000-00-00'
	   and $cer_basicdata['cer_basicdata_lease_enddate'] != NULL and $cer_basicdata['cer_basicdata_lease_enddate'] != '0000-00-00')
	{
		$duration_in_years = $cer_basicdata['cer_basicdata_lease_enddate'] - $cer_basicdata['cer_basicdata_lease_startdate'];
		$tmp1 = 13 - substr($cer_basicdata['cer_basicdata_lease_startdate'], 5,2) + substr($cer_basicdata['cer_basicdata_lease_enddate'], 5,2);
		$tmp2 = ($duration_in_years - 1)*12;
		$duration_in_years = round(($tmp1 + $tmp2) / 12, 1);

		$duration_in_years_and_months = floor(($tmp1 + $tmp2) / 12);
		$duration_in_years_and_months = $duration_in_years_and_months . "y-" . (($tmp1 + $tmp2) - ($duration_in_years_and_months*12)) . "m";
		

		$d1 = new DateTime($cer_basicdata['cer_basicdata_lease_startdate']);
		$d2 = new DateTime($cer_basicdata['cer_basicdata_lease_enddate']);
		$interval = date_diff($d2, $d1);

		$contract_duration = round($interval->days / 365, 1);


		$rental_period = substr($cer_basicdata['cer_basicdata_lease_startdate'], 5,2) . '-' . substr($cer_basicdata['cer_basicdata_lease_startdate'], 0,4) . '/' . substr($cer_basicdata['cer_basicdata_lease_enddate'], 5,2) . '-' . substr($cer_basicdata['cer_basicdata_lease_enddate'], 0,4) .  ' (' . $contract_duration . ' years)';

		$renewal_option = "-";
		$exit_option = "-";
		

		$m = substr($cer_basicdata['cer_basicdata_lease_startdate'], 5, 2);
		$d = substr($cer_basicdata['cer_basicdata_lease_startdate'], 8, 2);
		$y = substr($cer_basicdata['cer_basicdata_lease_startdate'], 0, 4);
		$d1=mktime(0,0,0,$m,$d,$y);
		$m = substr($cer_basicdata['cer_basicdata_lease_enddate'], 5, 2);
		$d = substr($cer_basicdata['cer_basicdata_lease_enddate'], 8, 2);
		$y = substr($cer_basicdata['cer_basicdata_lease_enddate'], 0, 4);
		$d2=mktime(0,0,0,$m,$d,$y);

		if($d2-$d1 > 0)
		{
			$termination_time = floor(($d2-$d1)/2628000);
		}


		$index_clause = "-";
		
		$index_clause2 = "-";
		
		//no calculation but entering in the rental tab
		$termination_time = "-";
	}
	else
	{
		$num_of_full_years = $cer_basicdata["cer_basicdata_lastyear"] - $cer_basicdata["cer_basicdata_firstyear"] - 1;
		$num_of_months = (13 - $cer_basicdata["cer_basicdata_firstmonth"]) + $cer_basicdata["cer_basicdata_lastmonth"] + 12 * $num_of_full_years;
		
		
		$duration_in_years = $num_of_months / 12;
		$renewal_option = "";
		$exit_option = "";
		$termination_time = "";
		$index_clause = "";
		$index_clause2 = "";

		$rental_period = '';
	}
}



$contract_duration_ln = "";
if(param("pid") > 0 )
{
	$posleases_ln = get_ln_lease_data($project["project_id"], $latest_ln_id, $project["pipeline"], $latest_ln_version);

	if(count($posleases_ln) > 0)
	{
		$duration_in_years_ln = $posleases_ln["poslease_enddate"] - $posleases_ln["poslease_startdate"];

		if(substr($posleases_ln["poslease_startdate"], 5,2) == substr($posleases_ln["poslease_enddate"], 5,2)) {
			$tmp1 = 12;
		}
		else
		{
			$tmp1 = 13 - substr($posleases_ln["poslease_startdate"], 5,2) + substr($posleases_ln["poslease_enddate"], 5,2);
		}

		$tmp2 = ($duration_in_years_ln - 1)*12;
		$duration_in_years_ln = round(($tmp1 + $tmp2) / 12, 1);

		$duration_in_years_and_months_ln = floor(($tmp1 + $tmp2) / 12);
		$duration_in_years_and_months_ln = $duration_in_years_and_months_ln . "y-" . (($tmp1 + $tmp2) - ($duration_in_years_and_months_ln*12)) . "m";


		$d1 = new DateTime($posleases_ln["poslease_startdate"]);
		$d2 = new DateTime($posleases_ln["poslease_enddate"]);
		$interval = date_diff($d2, $d1);

		$contract_duration_ln = round($interval->days / 365, 1);


		$rental_period_ln = substr($posleases_ln["poslease_startdate"], 5,2) . '-' . substr($posleases_ln["poslease_startdate"], 0,4) . '/' . substr($posleases_ln["poslease_enddate"], 5,2) . '-' . substr($posleases_ln["poslease_enddate"], 0,4) .  ' (' . $contract_duration_ln . ' years)';
		
	

		$renewal_option_ln = "";
		if($posleases_ln["poslease_extensionoption"] != Null and $posleases_ln["poslease_extensionoption"] != '0000-00-00')
		{
			$renewal_option_ln = abs($posleases_ln["poslease_extensionoption"] - $posleases_ln["poslease_enddate"]);
			$renewal_option_date = $renewal_option_ln*12 . "(" . to_system_date($posleases["poslease_extensionoption"]) . ")";
		}
		
		$exit_option_ln = "";
		if($posleases_ln["poslease_exitoption"] != Null and $posleases_ln["poslease_exitoption"] != '0000-00-00')
		{
			$exit_option_ln = to_system_date($posleases_ln["poslease_exitoption"]);
		}

		$hand_over_key_ln = "-";
		if($posleases_ln["poslease_handoverdate"] != Null and $posleases_ln["poslease_handoverdate"] != '0000-00-00')
		{
			$hand_over_key_ln = to_system_date($posleases["poslease_handoverdate"]);
		}

		$first_rent_payed_ln = "-";
		if($posleases_ln["poslease_firstrentpayed"] != Null and $posleases_ln["poslease_firstrentpayed"] != '0000-00-00')
		{
			$first_rent_payed_ln = to_system_date($posleases_ln["poslease_firstrentpayed"]);
		}

		$m = substr($posleases_ln["poslease_startdate"], 5, 2);
		$d = substr($posleases_ln["poslease_startdate"], 8, 2);
		$y = substr($posleases_ln["poslease_startdate"], 0, 4);
		$d1=mktime(0,0,0,$m,$d,$y);
		$m = substr($posleases_ln["poslease_enddate"], 5, 2);
		$d = substr($posleases_ln["poslease_enddate"], 8, 2);
		$y = substr($posleases_ln["poslease_enddate"], 0, 4);
		$d2=mktime(0,0,0,$m,$d,$y);

		if($d2-$d1 > 0)
		{
			$termination_time_ln = floor(($d2-$d1)/2628000);
		}


		$index_clause_ln = "no";
		if($posleases_ln["poslease_indexclause_in_contract"] == 1)
		{
			$index_clause_ln = "yes";
		}

		$index_clause2_ln = "no";
		if($posleases_ln["poslease_isindexed"] == 1)
		{
			$index_clause2_ln = "yes";
		}

		//no calculation but entering in the rental tab
		$termination_time_ln = $posleases_ln["poslease_termination_time"];

	}
	else
	{
		$num_of_full_years_ln = $cer_basicdata_ln["cer_basicdata_lastyear"] - $cer_basicdata_ln["cer_basicdata_firstyear"] - 1;
		$num_of_months_ln = (13 - $cer_basicdata_ln["cer_basicdata_firstmonth"]) + $cer_basicdata_ln["cer_basicdata_lastmonth"] + 12 * $num_of_full_years_ln;
		
		
		$duration_in_years_ln = $num_of_months_ln / 12;
		$renewal_option_ln = "";
		$exit_option_ln = "";
		$termination_time_ln = "";
		$index_clause_ln = "";
		$index_clause2_ln = "";

		$rental_period_ln = '';
	}
}



//recalculate the duration in years from the business plan period not from posleases
$duration_in_years = $last_year - $first_year;
$tmp1 = 13 - $first_month + $last_month;
$tmp2 = ($duration_in_years - 1)*12;
$duration_in_years = round(($tmp1 + $tmp2) / 12, 1);

$duration_in_years_and_months = floor(($tmp1 + $tmp2) / 12);
$duration_in_years_and_months = $duration_in_years_and_months . "y-" . (($tmp1 + $tmp2) - ($duration_in_years_and_months*12)) . "m";

$business_plan_period = $first_month . '-' . $first_year . '/' . $last_month . '-' . $last_year .' (' . $duration_in_years_and_months . ')';



//additional rental cost and other fees
//get additional rental costs
if(param("pid") > 0 )
{
	$annual_charges = 0;
	$tax_on_rents = 0;
	$passenger_index = 0;
	$savings_on_rent = 0;
	$sql_e = "select * from cer_expenses " .
			 "where " . $_filter_expenses . " cer_expense_project = " . param("pid") . 
			 " and cer_expense_type in (3, 18, 19, 20) " . 
		     " and cer_expense_year = " . $first_full_year . 
			 " order by cer_expense_year";
	$res_e = mysql_query($sql_e) or dberror($sql_e);

	while($row_e = mysql_fetch_assoc($res_e))
	{
		if($row_e["cer_expense_type"] == 3)
		{
			$annual_charges = round($row_e["cer_expense_amount"]/1000, 0);
		}
		elseif($row_e["cer_expense_type"] == 18)
		{
			$tax_on_rents = round($row_e["cer_expense_amount"]/1000, 0);
		}
		elseif($row_e["cer_expense_type"] == 19)
		{
			$passenger_index = round($row_e["cer_expense_amount"]/1000, 0);
		}
		elseif($row_e["cer_expense_type"] == 20)
		{
			$savings_on_rent = round($row_e["cer_expense_amount"]/1000, 0);
		}
	}

	
	$annual_charges_ln = $annual_charges;
	$tax_on_rents_ln = $tax_on_rents;
	$passenger_index_ln = $passenger_index;
	$savings_on_rent_ln = $savings_on_rent;

	$sql_e = "select * from cer_expenses " .
			 "where " . $_filter_expenses_ln . " cer_expense_project = " . param("pid") . 
			 " and cer_expense_type in (3, 18, 19, 20) " . 
		     " and cer_expense_year = " . $first_full_year .
			 " order by cer_expense_year";

	$res_e = mysql_query($sql_e) or dberror($sql_e);

	while($row_e = mysql_fetch_assoc($res_e))
	{
		if($row_e["cer_expense_type"] == 3)
		{
			$annual_charges_ln = round($row_e["cer_expense_amount"]/1000, 0);
		}
		elseif($row_e["cer_expense_type"] == 18)
		{
			$tax_on_rents_ln = round($row_e["cer_expense_amount"]/1000, 0);
		}
		elseif($row_e["cer_expense_type"] == 19)
		{
			$passenger_index_ln = round($row_e["cer_expense_amount"]/1000, 0);
		}
		elseif($row_e["cer_expense_type"] == 20)
		{
			$savings_on_rent_ln = round($row_e["cer_expense_amount"]/1000, 0);
		}
	}

}

//franchisee contract duration

if(param("pid") > 0 )
{
	if($posdata["posaddress_fagrstart"] != '0000-00-00' and $posdata["posaddress_fagrstart"] != NULL)
	{
		$contract_duration_in_years = $posdata["posaddress_fagrend"] - $posdata["posaddress_fagrstart"];
		$tmp1 = 13 - substr($posdata["posaddress_fagrstart"], 5,2) + substr($posdata["posaddress_fagrend"], 5,2);
		$tmp2 = ($contract_duration_in_years - 1)*12;
		$contract_duration_in_years = round(($tmp1 + $tmp2) / 12, 1);
	}
	else
	{
		$contract_duration_in_years = 0;
	}

	$pos_opening_date = "-";
	if($posdata["posaddress_store_openingdate"] != NULL and $posdata["posaddress_store_openingdate"] != "0000-00-00")
	{
		$pos_opening_date = to_system_date($posdata["posaddress_store_openingdate"]);
	}

	$deadline_for_property = to_system_date($cer_basicdata["cer_basicdata_deadline_property"]);
	$residual_value_former_investment = $cer_basicdata["cer_basicdata_residual_value"];
	$residual_value_former_keymoney = $cer_basicdata["cer_basicdata_residualkeymoney_value"];

	$residual_value_former_investment_depricate_years = $cer_basicdata["cer_basicdata_residual_depryears"];
	$residual_value_former_investment_depricate_months = $cer_basicdata["cer_basicdata_residual_deprmonths"];
	
	$residual_value_former_keymoney_depricate_years = $cer_basicdata["cer_basicdata_residualkeymoney_depryears"];
	$residual_value_former_keymoney_depricate_months = $cer_basicdata["cer_basicdata_residualkeymoney_deprmonths"];

	$recoverable_keymoney = $cer_basicdata["cer_basicdata_recoverable_keymoney"];

	
	$first_year_depr = $first_year;
	$first_month_depr = $first_month;
	if($cer_basicdata["cer_basicdata_firstyear_depr"] > 0)
	{
		$first_year_depr = $cer_basicdata["cer_basicdata_firstyear_depr"];
		$first_month_depr = $cer_basicdata["cer_basicdata_firstmonth_depr"];
	}

	if($recoverable_keymoney != 1)
	{
		if(array_key_exists(9, $amounts))
		{
			$amounts[9] = $amounts[9] + $recoverable_keymoney;
		}
		else
		{
			$amounts[9] = $recoverable_keymoney;
		}
	}
	
	$cer_totals = $cer_totals + $recoverable_keymoney;

}
else
{
	$contract_duration_in_years = '-';
	$pos_opening_date = "-";
	$deadline_for_property = "-";
	$residual_value_former_investment = $cer_basicdata["cer_basicdata_residual_value"];
	$residual_value_former_keymoney = $cer_basicdata["cer_basicdata_residualkeymoney_value"];

	$residual_value_former_investment_depricate_years = $cer_basicdata["cer_basicdata_residual_depryears"];
	$residual_value_former_investment_depricate_months = $cer_basicdata["cer_basicdata_residual_deprmonths"];
	
	$residual_value_former_keymoney_depricate_years = $cer_basicdata["cer_basicdata_residualkeymoney_depryears"];
	$residual_value_former_keymoney_depricate_months = $cer_basicdata["cer_basicdata_residualkeymoney_deprmonths"];

	$first_year_depr = $first_year;
	$first_month_depr = $first_month;
	
	
	$recoverable_keymoney = $cer_basicdata["cer_basicdata_recoverable_keymoney"];
	if($recoverable_keymoney != 1)
	{
		if(array_key_exists(9, $amounts))
		{
			$amounts[9] = $amounts[9] + $recoverable_keymoney;
		}
		else
		{
			$amounts[9] = $recoverable_keymoney;
		}
	}
		
	$cer_totals = $cer_totals + $recoverable_keymoney;

}

if(param("pid") > 0 )
{
	$result = calculate_forcasted_salaries(param("pid"), $years, $project["order_shop_address_country"], $cer_version);
}
else
{
	$result = calculate_forcasted_salaries(param("did"), $years, $cer_basicdata["cer_basicdata_country"]);
}


//get payment_terms
$payment_terms = array();
$stock_data = array();
foreach($years as $key=>$year)
{
	$payment_terms[$year] = 0;
	$stock_data[$year] = 0;
}

$tmp_filter = "";
if($cer_version_filter == true)
{
	$tmp_filter = " and cer_paymentterm_cer_version = " . $cer_version . " ";
}
$sql = "select * from " . $_table_paymentterms . 
" where cer_paymentterm_" . $_id_suffix . " = " . $_id . 
$tmp_filter;

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$payment_terms[$row["cer_paymentterm_year"]] = $row["cer_paymentterm_in_months"];
}
//get stock data
$tmp_filter = "";
if($cer_version_filter == true)
{
	$tmp_filter = " and cer_stock_cer_version = " . $cer_version . " ";
}
$sql = "select * from " . $_table_stocks . 
" where cer_stock_" . $_id_suffix . " = " . $_id . 
$tmp_filter;

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$stock_data[$row["cer_stock_year"]] = $row["cer_stock_stock_in_months"];
}


//get expenses
$annual_rent = 0;
$material_of_products = array();
$marketing_expenses = array();
$income_taxes = array();
$interco_contribution = array();
$indirect_salaries = array();
$rents = array();
$fixedrents = array();
$turnoverbasedrents = array();
$additionalrents = array();
$other_expenses = array();
$expenses = array();
$expense_types = array();
$auxmat = array();
$sales_admin = array();
foreach($years as $key=>$year)
{
	$rents[$year] = 0;
	$marketing_expenses[$year] = 0;
	$interco_contribution[$year] = 0;
	$other_expenses[$year] = 0;
	$income_taxes[$year] = 0;
	$material_of_products[$year] = 0;
	$auxmat[$year] = 0;
	$sales_admin[$year] = 0;
	$indirect_salaries[$year] = 0;
}


$total_lease_commitment = 0;


$tmp_filter = "";
if($cer_version_filter == true)
{
	$tmp_filter = " and cer_expense_cer_version = " . $cer_version . " ";
}
$sql = "select * from  " . $_table_expenses .
	   " left join cer_expense_types on cer_expense_type_id = cer_expense_type " . 
	   "where cer_expense_" . $_id_suffix . " = " . $_id .
	   $tmp_filter . 
	   " order by cer_expense_type_sortorder, cer_expense_year";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	
	if($row["cer_expense_type"] == 1) //Salaries
	{
		$indirect_salaries[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	}
	elseif($row["cer_expense_type"] == 21) //Recruitement and training costs
	{
		$indirect_salaries[$row["cer_expense_year"]] = $indirect_salaries[$row["cer_expense_year"]] + $row["cer_expense_amount"];
	}
	elseif($row["cer_expense_type"] == 2 
		   or $row["cer_expense_type"] == 3 
		   or $row["cer_expense_type"] == 16
		   or $row["cer_expense_type"] == 18
		   or $row["cer_expense_type"] == 19
		   or $row["cer_expense_type"] == 20
		   or $row["cer_expense_type"] == 22) //rent
	{
		
		$total_lease_commitment = $total_lease_commitment + $row["cer_expense_amount"];
		$rents[$row["cer_expense_year"]] = $rents[$row["cer_expense_year"]] + $row["cer_expense_amount"];

		//echo $row["cer_expense_year"] . '->' . $row["cer_expense_amount"] . '->' . $rents[$row["cer_expense_year"]] . '<br />';
		
		//workaround for old projects
		//$additionalrents[$row["cer_expense_year"]] = 0;
		//end workaround

		if($row["cer_expense_type"] == 2) {
			$fixedrents[$row["cer_expense_year"]] = $row["cer_expense_amount"];
		}
		elseif($row["cer_expense_type"] == 16) {
			$turnoverbasedrents[$row["cer_expense_year"]] = $row["cer_expense_amount"];
		}
		elseif($row["cer_expense_type"] == 3
			   or $row["cer_expense_type"] == 18
			   or $row["cer_expense_type"] == 19
			   or $row["cer_expense_type"] == 20) {
			

			if(array_key_exists($row["cer_expense_year"], $additionalrents))
			{
				$additionalrents[$row["cer_expense_year"]] = $additionalrents[$row["cer_expense_year"]] + $row["cer_expense_amount"];
			}
			else
			{
				$additionalrents[$row["cer_expense_year"]] = $row["cer_expense_amount"];
			}
		}
	}
	elseif($row["cer_expense_type"] == 4) //Aux mat.
	{
		//$total_lease_commitment = $total_lease_commitment + $row["cer_expense_amount"];
		$auxmat[$row["cer_expense_year"]] = $row["cer_expense_amount"];

	}
	elseif($row["cer_expense_type"] == 5) //Sales/admin expenses
	{
		$sales_admin[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	}
	elseif($row["cer_expense_type"] == 7) //other non capitalized fees and income taxes
	{
		$income_taxes[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	}
	elseif($row["cer_expense_type"] == 13) //Marketing Expenses
	{
		$marketing_expenses[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	}
	elseif($row["cer_expense_type"] == 14) //Interco Contribution
	{
		$interco_contribution[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	}
	elseif($row["cer_expense_type"] == 15) //Cost of Products Sold
	{
		$material_of_products[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	}
	else
	{
		$other_expenses[$row["cer_expense_year"]] = $other_expenses[$row["cer_expense_year"]] + $row["cer_expense_amount"];
	}
	
	if($row["cer_expense_type"] != 13 and $row["cer_expense_type"] != 14 and $row["cer_expense_type"] != 9 and $row["cer_expense_type"] != 15)
	{
		$expenses[$row["cer_expense_type_name"]][$row["cer_expense_year"]] = $row["cer_expense_amount"];
		$expense_types[$row["cer_expense_year"]][$row["cer_expense_type_name"]] = $row["cer_expense_type"];
	}
}



if($duration_in_years > 0)
{
	$annual_rent = $total_lease_commitment / $duration_in_years;
}
else
{
	$annual_rent = "-";
}

//get investment
$initial_investment_total = 0;

$tmp_filter = "";
if($cer_version_filter == true)
{
	$tmp_filter = " and cer_investment_cer_version = " . $cer_version . " ";
}
$sql = "select * from  " . $_table_investments . 
	   " left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
	   "where cer_investment_" . $_id_suffix . " = " . $_id . 
	   $tmp_filter . 
	   " and posinvestment_type_intangible = 0";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$initial_investment_total = $initial_investment_total  + $row["cer_investment_amount_cer_loc"]; 
}

//calculate depreciation
//calculate depreciation
$depreciation = $_depreciation["investments"];
$prepayed_rents = $_depreciation["intangibles"];
$debug_depreciation = $_depreciation["investments_debug"];
$debug_depreciation_residual_fixed_assets = $_depreciation["depreciation_residual_fixed_assets"];

//residual values
$residual_value_investment = $_depreciation["investments_residual_value"];
$residual_value_intangbiles = $_depreciation["intangibles_residual_value"];

//cash flow initial investment
$initial_investment_total_for_cash_flow = $initial_investment_total + $intagibles_total;

//get other information
$head_counts = "-";  
$ftes = "-";
$tmp_filter = "";
if($cer_version_filter == true)
{
	$tmp_filter = " and cer_salary_cer_version = " . $cer_version . " ";
}
$sql = "select count(cer_salary_id) as head_counts, sum(cer_salary_headcount_percent) as ftes " .
	   "from " . $_table_salaries . 
	   " where cer_salary_" . $_id_suffix . " = " . $_id . 
	   $tmp_filter;


$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$head_counts = $row["head_counts"];
	$ftes = $row["ftes"]/100;
}


$payback_period_start = 0.5;
$pay_back_period_retail = 0;
$pay_back_period_retail_values = array();
$pay_back_cummunlated_retail_values = array();
$pay_back_period_wholesale = 0;
$pay_back_period_wholesale_values = array();
$pay_back_cummunlated_wholesale_values = array();
$discount_rate = $cer_basicdata["cer_basicdata_dicount_rate"] / 100;

//cash flow arrays
$cfs = array();
$cfs1 = array();
$cfs2 = array();
$cfs3 = array();

//init value arrays
$sales_units_watches_values = array();
$sales_units_watches_shares = array();
$sales_units_watches_growth = array();

$sales_units_jewellery_values = array();
$sales_units_jewellery_shares = array();

$average_price_watches_values = array();
$average_price_watches_shares = array();

$average_price_jewellery_values = array();
$average_price_jewellery_shares = array();

$sales_watches_values = array();
$sales_watches_shares = array();

$sales_jewellery_values = array();
$sales_jewellery_shares = array();

$sales_customer_service_values = array();
$sales_customer_service_shares = array();

$total_gross_sales_values = array();
$total_gross_sales_shares = array();

$sales_reduction_values = array();
$sales_reduction_shares = array();

$total_net_sales_values = array();
$total_net_sales_shares = array();

$material_of_products_values = array();
$material_of_products_shares = array();

$total_gross_margin_values = array();
$total_goss_margin_shares = array();

$marketing_expenses_values = array();
$marketing_expenses_shares = array();

$interco_contribution_values = array();
$interco_contribution_shares = array();

$indirect_salaries_values = array();
$indirect_salaries_shares = array();

$rents_values = array();
$rents_shares = array();

$fixedrents_values = array();
$turnoverbasedrents_values = array();
$additionalrents_values = array();


$rents_total_values  = array();

$income_taxes_values = array();
$income_taxes_shares = array();

$auxmat_values = array();
$auxmat_shares = array();

$sales_admin_values = array();
$sales_admin_shares = array();

$depreciation_values = array();
$depreciation_shares = array();

$prepayed_rent_values = array();
$prepayed_rent_shares = array();

$other_expenses_values = array();
$other_expenses_shares = array();

$total_indirect_expenses_values = array();
$total_indirect_expenses_shares = array();

$operating_income01_values = array();
$operating_income01_shares = array();

$operating_income02_values = array();
$operating_income02_shares = array();

$break_even_retail_margin = array();
$break_even_wholesale_margin = array();

$stat_wholesale_margin = array();

$total_cash_flow_values = array();
$total_cash_flow_fcf_values = array();
$cash_flow_values = array();

$cash_flow_whole_sale_values = array();

$pay_back_cummunlated_retail_values = array();
$pay_back_period_retail_values = array();

$pay_back_cummunlated_wholesale_values = array();
$pay_back_period_wholesale_values = array();


$cer_revenue_customer_frequency = array();
$cer_revenue_total_days_open_per_year = array();
$cer_revenue_total_hours_open_per_week = array();
$cer_revenue_total_workinghours_per_week = array();


$rents_80_percent_values = array();
$turnoverbasedrents_80_percent_values = array();
$total_gross_margin_80_percent_values = array();
$total_indirect_expenses_80_percent_values = array();
$operating_income01_80_percent_values = array();
$operating_income02_80_percent_values = array();
$taxes_80_percent_values = array();
$passenger_index_80_percent_values = array();


//fill value arrays with data
foreach($years as $key=>$year)
{
	$sales_units_watches_values[$year] = "??";
    $sales_units_watches_shares[$year] = "";
	$sales_units_watches_growth[$year] = "??";

    $sales_units_jewellery_values[$year] = "??";
    $sales_units_jewellery_shares[$year] = "";

    $average_price_watches_values[$year] = "??";
    $average_price_watches_shares[$year] = "";

    $average_price_jewellery_values[$year] = "??";
    $average_price_jewellery_shares[$year] = "";

    $sales_watches_values[$year] = "??";
    $sales_watches_shares[$year] = "??";

    $sales_jewellery_values[$year] = "??";
    $sales_jewellery_shares[$year] = "??";

	$sales_customer_service_values[$year] = "??";
    $sales_customer_service_shares[$year] = "";

    $total_gross_sales_values[$year] = "??";
    $total_gross_sales_shares[$year] = "??";

	$sales_reduction_values[$year] = "??";
    $sales_reduction_shares[$year] = "??";

	$total_net_sales_values[$year] = "??";
    $total_net_sales_shares[$year] = "??";

	$material_of_products_values[$year] = "??";
    $material_of_products_shares[$year] = "??";

	$total_gross_margin_values[$year] = "??";
    $total_goss_margin_shares[$year] = "??";

	$marketing_expenses_values[$year] = "??";
    $marketing_expenses_shares[$year] = "??";

	$interco_contribution_values[$year] = "??";
    $interco_contribution_shares[$year] = "??";

	$indirect_salaries_values[$year] = "??";
    $indirect_salaries_shares[$year] = "??";

    $rents_values[$year] = "??";
    $rents_shares[$year] = "??";

	$fixedrents_values = "??";
	$turnoverbasedrents_values = "??";
	$additionalrents_values = "??";


	$rents_total_values [$year] = "??";

	$income_taxes_values[$year] = "??";
    $income_taxes_shares[$year] = "??";

	$auxmat_values[$year] = "??";
    $auxmat_shares[$year] = "??";

	$sales_admin_values[$year] = "??";
	$sales_admin_shares[$year] = "??";

	$depreciation_values[$year] = "??";
    $depreciation_shares[$year] = "??";

	$prepayed_rent_values[$year] = "??";
    $prepayed_rent_shares[$year] = "??";

	$other_expenses_values[$year] = "??";
    $other_expenses_shares[$year] = "??";

	$total_indirect_expenses_values[$year] = "??";
    $total_indirect_expenses_shares[$year] = "??";

	$operating_income01_values[$year] = "??";
    $operating_income01_shares[$year] = "??";

	$operating_income02_values[$year] = "??";
    $operating_income02_shares[$year] = "??";

	$break_even_retail_margin[$year] = "??";
	$break_even_wholesale_margin[$year] = "??";

	$stat_wholesale_margin[$year] = "??";

	$total_cash_flow_values[$year] = "??";
	$total_cash_flow_fcf_values[$year] = 0;
	$cash_flow_values[$year] = "??";

	$cash_flow_whole_sale_values[$year] = "??";

	$pay_back_cummunlated_retail_values[$year] = 0;
	$pay_back_period_retail_values[$year] = 0;
	
	$pay_back_cummunlated_wholesale_values[$year] = 0;
	$pay_back_period_wholesale_values[$year] = 0;


	$cer_revenue_customer_frequency[$year] = "??";
	$cer_revenue_total_days_open_per_year[$year] = "??";
	$cer_revenue_total_hours_open_per_week[$year] = "??";
	$cer_revenue_total_workinghours_per_week[$year] = "??";


	$rents_80_percent_values[$year] = 0;
	$turnoverbasedrents_80_percent_values[$year] = 0;
	$total_gross_margin_80_percent_values[$year] = 0;
	$total_indirect_expenses_80_percent_values[$year] = 0;
	$operating_income01_80_percent_values[$year] = 0;
	$operating_income02_80_percent_values[$year] = 0;
	$taxes_80_percent_values[$year] = 0;
	$passenger_index_80_percent_values[$year] = 0;
}



//get sales planning
$tmp_filter = "";
if($cer_version_filter == true)
{
	$tmp_filter = " and cer_revenue_cer_version = " . $cer_version . " ";
}
$sql = "select * from  " . $_table_revenues .
	   " where cer_revenue_" . $_id_suffix . " = " . $_id . 
	   $tmp_filter;

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$year = $row["cer_revenue_year"];
	$sales_units_watches_values[$year] = $row["cer_revenue_quantity_watches"];
	$sales_units_jewellery_values[$year] = $row["cer_revenue_quantity_jewellery"];

	$average_price_watches_values[$year] =$row["cer_revenue_aveargeprice_watches"];
	$average_price_jewellery_values[$year] =  $row["cer_revenue_aveargeprice_jewellery"];

	$sales_watches_values[$year] = $row["cer_revenue_watches"];
	$sales_jewellery_values[$year] = $row["cer_revenue_jewellery"];
	$sales_customer_service_values[$year] = $row["cer_revenue_customer_service"];


	$total_gross_sales_values[$year] = $row["cer_revenue_watches"] + $row["cer_revenue_jewellery"] + $row["cer_revenue_customer_service"];


	$cer_revenue_customer_frequency[$year] = $row["cer_revenue_customer_frequency"];
	$cer_revenue_total_days_open_per_year[$year] = $row["cer_revenue_total_days_open_per_year"];
	$cer_revenue_total_hours_open_per_week[$year] = $row["cer_revenue_total_hours_open_per_week"];
	$cer_revenue_total_workinghours_per_week[$year] = $row["cer_revenue_total_workinghours_per_week"];

	$sales_units_watches_growth[$year] = "";

}

//calculate yearly_growth in percent of quantities of watches
$tmp_quantity = 0;
$tmp_first_year = $first_year;
$tmp_first_month = $first_month;

if(param('pid'))
{
	if(substr($project["project_real_opening_date"], 0, 4) > $tmp_first_year)
	{
		$tmp_first_year = (int)substr($project["project_real_opening_date"], 0, 4);
		$tmp_first_month = (int)substr($project["project_real_opening_date"], 5, 2);
	}
	elseif(substr($project["project_real_opening_date"], 5, 2) > $tmp_first_month)
	{
		$tmp_first_month = (int)substr($project["project_real_opening_date"], 5, 2);
	}
}


foreach($sales_units_watches_values as $year=>$quantity)
{
	if($year == $tmp_first_year)
	{
		$quantity = 12*$quantity/(13-$tmp_first_month);
	}

	if($year == $last_year)
	{
		$quantity = 12*$quantity/$last_month;
	}
	
	if($tmp_quantity > 0)
	{
		$tmp_percent = round(100*($quantity-$tmp_quantity)/$tmp_quantity, 1);
		
		if($tmp_percent > 0){$tmp_percent = "+" . $tmp_percent;}
		$sales_units_watches_growth[$year] = $tmp_percent . "%";
	}
	$tmp_quantity = $quantity;
}

//calculate sum of rents
$sum_of_rents_payed = 0;
$sum_of_fixedrents_payed = 0;
$sum_of_turnoverbasedrents_payed = 0;
$sum_of_additionalrents_payed = 0;
$rental_num_of_months = array();
$sum_of_rental_months = 0;

$fixedrents_values = array();
$turnoverbasedrents_values = array();
$additionalrents_values = array();

foreach($sales_units_watches_values as $year=>$value)
{
	$sum_of_rents_payed = $sum_of_rents_payed + $rents[$year];

	if(array_key_exists($year, $fixedrents)) {
		$sum_of_fixedrents_payed = $sum_of_fixedrents_payed + $fixedrents[$year];
	}
	if(array_key_exists($year, $turnoverbasedrents)) {
		$sum_of_turnoverbasedrents_payed = $sum_of_turnoverbasedrents_payed + $turnoverbasedrents[$year];
	}

	if(array_key_exists($year, $additionalrents)) {
		$sum_of_additionalrents_payed = $sum_of_additionalrents_payed + $additionalrents[$year];
	}

	if($cer_basicdata["cer_basicdata_firstyear"] == $year) {
		$rental_num_of_months[$year] = 13 - $cer_basicdata["cer_basicdata_firstmonth"];
	}
	elseif($cer_basicdata["cer_basicdata_lastyear"] == $year) {
		$rental_num_of_months[$year] = $cer_basicdata["cer_basicdata_lastmonth"];
	}
	else
	{
		$rental_num_of_months[$year] = 12;
	}

	$sum_of_rental_months = $sum_of_rental_months + $rental_num_of_months[$year];
}

//prepare ouput values
	
foreach($sales_units_watches_values as $year=>$value)
{
	//based on gross sales values, calculated forward to net sales valus
	//$tmp5 = $total_gross_sales_values[$year] * $cer_basicdata["cer_basicdata_sales_reduction"] / 100;
	//$sales_reduction_values[$year] = $tmp5;
	//$total_gross_sales_values[$year] = $total_gross_sales_values[$year];
	//$total_net_sales_values[$year] = $total_gross_sales_values[$year] - $sales_reduction_values[$year];

	//based on net sales values, calculated backwards up to gross sales
	$total_gross_sales_values[$year] = $total_gross_sales_values[$year];
	//$total_net_sales_values[$year] = ($total_gross_sales_values[$year] / (100+$cer_basicdata["cer_basicdata_sales_reduction"])) * 100;

	if($calc_model == 2008)
	{
		$total_net_sales_values[$year] = ($total_gross_sales_values[$year] / (100+$cer_basicdata["cer_basicdata_sales_reduction"])) * 100;
		$sales_reduction_values[$year] =  $total_net_sales_values[$year] * $cer_basicdata["cer_basicdata_sales_reduction"] / 100;
	}
	else
	{
		$sales_reduction_values[$year] =  $total_gross_sales_values[$year] * $cer_basicdata["cer_basicdata_sales_reduction"] / 100;
		$total_net_sales_values[$year] = $total_gross_sales_values[$year] - $sales_reduction_values[$year];
	}

	
	$material_of_products_values[$year] = $material_of_products[$year];


	$total_gross_margin_values[$year] = $total_gross_sales_values[$year] - $sales_reduction_values[$year] - $material_of_products_values[$year];

	$marketing_expenses_values[$year] = $marketing_expenses[$year];
	
	//2013 ther was a change from Marketing Contribution to into in Revenues
	//so we do not substract interco contribition from marketing expenses anymore
	//$interco_contribution_values[$year] = $interco_contribution[$year];
	$interco_contribution_values[$year] = 0;

	
	$indirect_salaries_values[$year] = $indirect_salaries[$year];

	
	//calcualte rents depending on the average parameter value
	if($cer_basicdata["cer_basicdata_calculate_rent_avg"] == 1) // average rent per year
	{
		if($sum_of_rental_months == 0)
		{
			$rents_values[$year] = 0;
			$fixedrents_values[$year] = 0;
			$turnoverbasedrents_values[$year] = 0;
			$additionalrents_values[$year] = 0;
		}
		else
		{
			$rents_values[$year] = ($rental_num_of_months[$year] * $sum_of_rents_payed) / $sum_of_rental_months;
			
			$fixedrents_values[$year] = (($rental_num_of_months[$year] * $sum_of_fixedrents_payed) / $sum_of_rental_months);
			
			$turnoverbasedrents_values[$year] = ($rental_num_of_months[$year] * $sum_of_turnoverbasedrents_payed) / $sum_of_rental_months;

			$additionalrents_values[$year] = ($rental_num_of_months[$year] * $sum_of_additionalrents_payed) / $sum_of_rental_months;

			//echo (($rental_num_of_months[$year] * $sum_of_fixedrents_payed) / $sum_of_rental_months) . '<br />';
			//echo $year . "->" . $rental_num_of_months[$year] . "->" .  $sum_of_fixedrents_payed . "->" . $sum_of_rental_months . "->" . $fixedrents_values[$year] . "<br />";

			//echo $additionalrents_values[$year] . " ";
		}
	}
	else
	{
		$rents_values[$year] = $rents[$year];
		$fixedrents_values[$year] = $fixedrents[$year];
		
		if(array_key_exists($year, $turnoverbasedrents)) {
			$turnoverbasedrents_values[$year] = $turnoverbasedrents[$year];
		}
		else
		{
			$turnoverbasedrents_values[$year] = 0;
		}
		
		if(array_key_exists($year, $additionalrents)) {
			$additionalrents_values[$year] = $additionalrents[$year];
		}
		else
		{
			$additionalrents_values[$year] = 0;
		}
	}
	
		
	$total_indirect_expenses_values[$year] = $indirect_salaries[$year] + $rents_values[$year] + $prepayed_rents[$year] + $auxmat[$year] + $depreciation[$year] + $sales_admin[$year] + $other_expenses[$year] + $income_taxes[$year];
	
	
	//$rents_total_values[$year] = $rents_values[$year] + $prepayed_rents[$year];
	$rents_total_values[$year] = $rents_values[$year];

	$income_taxes_values[$year] = $income_taxes[$year];

	$auxmat_values[$year] = $auxmat[$year];

	$depreciation_values[$year] = $depreciation[$year];

	$sales_admin_values[$year] = $sales_admin[$year];

	$other_expenses_values[$year] = $other_expenses[$year] + $income_taxes_values[$year];

	$tmp9 =  $total_gross_margin_values[$year] - $marketing_expenses_values[$year] + $interco_contribution_values[$year];
	$tmp10 = $tmp9 - $total_indirect_expenses_values[$year];
	$operating_income01_values[$year] = $tmp10;


	$tmp11 = $material_of_products_values[$year] * $cer_basicdata["cer_basicdata_wholesale_margin"] / 100; // Stat. Wholesale Margin
	$tmp12 = $total_gross_margin_values[$year] - $marketing_expenses_values[$year] + $interco_contribution_values[$year] - $total_indirect_expenses_values[$year]; // operating income
	
	$operating_income02_values[$year] = $tmp11 + $tmp12;

	$tmp13 = $total_indirect_expenses_values[$year] + $marketing_expenses_values[$year] - $interco_contribution_values[$year];
	$tmp14 = $sales_units_watches_values[$year] + $sales_units_jewellery_values[$year];
	
	if($tmp14 > 0)
	{
		$tmp15 = $total_gross_margin_values[$year] / $tmp14;
		$tmp16 = ($total_gross_margin_values[$year] + $tmp11) / $tmp14;

		if($tmp15 > 0 and $tmp16 > 0)
		{
			$break_even_retail_margin[$year] =  $tmp13/$tmp15;
			$break_even_wholesale_margin[$year]  =  $tmp13/$tmp16;
		}
	}

	$stat_wholesale_margin[$year] = $material_of_products_values[$year] * $cer_basicdata["cer_basicdata_wholesale_margin"] / 100;



	//cash flow
	if($year == $first_year)
	{
		//initial investment
		$_initial_investment = $initial_investment_total_for_cash_flow;
		$debug_initial_investment[$first_year] = $_initial_investment;

		//Variation Working Capital + Replacement Investment
		$_liabilities = $material_of_products_values[$first_year]/12 * $payment_terms[$first_year];
		$_stock = $material_of_products_values[$first_year]/12 * $stock_data[$first_year];
		$_receivables = 0; //not to take in consideration
		$_replacement_fixed_assets = 0; //not to take in consideration

		$_var_working_capital = $_liabilities + $_stock + $_receivables + $_replacement_fixed_assets;
		$debugvar_working_capital[$first_year] = $_var_working_capital;
		$debugvar_liabilities[$first_year] = $_liabilities;
		$debugvar_liabilities_change[$first_year] = $_liabilities;
		$debugvar_stock[$first_year] = $_stock;
		$debugvar_stock_change[$first_year] = $_stock;

		
		//total cash flow
		$_total_cash_flow = $operating_income01_values[$first_year] + $prepayed_rents[$first_year] + $depreciation_values[$first_year] - $_var_working_capital;
		$total_cash_flow_values[$first_year] = $_total_cash_flow;

		//income taxes
		$_income_taxes = $income_taxes_values[$first_year];

		//finacncial expenses
		$_interest_rate = $cer_basicdata_interest_rates[$first_year];
		$_finacial_expenses = $_initial_investment * $_interest_rate / 100;
		$total_cash_flow_fcf_values[$first_year] = -1*$_initial_investment + $total_cash_flow_values[$first_year] + $_finacial_expenses - $_income_taxes;

		$debugvar_finacial_expenses[$first_year] = $_finacial_expenses;

		//cash flow
		$cash_flow_values[$first_year] = $_total_cash_flow - $_initial_investment - $_finacial_expenses - $_income_taxes;
		$cash_flow_whole_sale_values[$first_year] = $_total_cash_flow  + $stat_wholesale_margin[$first_year];
		$cfs2[] = $cash_flow_whole_sale_values[$first_year];

		//pay back retail
		$tmp = (1+$discount_rate);
		$tmp = pow($tmp ,$payback_period_start);
		$tmp = $_total_cash_flow / $tmp;
		$tmp = -1*$_initial_investment + $tmp;
		$pay_back_cummunlated_retail_values[$first_year] = $tmp;

		if($pay_back_cummunlated_retail_values[$first_year] < 0)
		{
			$pay_back_period_retail_values[$first_year] = 0;
		}
		else
		{
			$pay_back_cummunlated_retail_values[$first_year] = $tmp;
			if($tmp != 0)
			{
				$pay_back_period_retail_values[$first_year] = 0 / ($tmp - 0) +  $payback_period_start;
			}
			else
			{
				$pay_back_period_retail_values[$first_year] = 0;
			}
		}

		if($pay_back_period_retail <= 0)
		{
			$pay_back_period_retail = $pay_back_period_retail_values[$first_year];
		}


		//pay back whole sale
		$tmp = (1+$discount_rate);
		$tmp = pow($tmp ,$payback_period_start);
		$tmp = $cash_flow_whole_sale_values[$year] / $tmp;
		$tmp = -1*$_initial_investment + $tmp;
		$pay_back_cummunlated_wholesale_values[$first_year] = $tmp;
		
		if($pay_back_cummunlated_wholesale_values[$first_year] < 0)
		{
			$pay_back_period_wholesale_values[$first_year] = 0;
		}
		else
		{
			if($tmp != 0)
			{
				$pay_back_period_wholesale_values[$first_year] = 0 / ($tmp - 0) +  $payback_period_start;
			}
			else
			{
				$pay_back_period_wholesale_values[$first_year] = 0;
			}
		}

		if($pay_back_period_wholesale <= 0)
		{
			$pay_back_period_wholesale = $pay_back_period_wholesale_values[$first_year];
		}
	}
	elseif(array_key_exists($year-1, $material_of_products_values))
	{
		//initial investment
		$_initial_investment = $initial_investment_total_for_cash_flow;
		

		//Variation Working Capital + Replacement Investment
		$_liabilities = $material_of_products_values[$year]/12 * $payment_terms[$year];
		$debugvar_liabilities[$year] = $_liabilities;
		$_liabilities = $_liabilities - ($material_of_products_values[$year-1]/12 * $payment_terms[$year-1]);
		$_stock = $material_of_products_values[$year]/12 * $stock_data[$year];
		$debugvar_stock[$year] = $_stock;
		$_stock = $_stock - ($material_of_products_values[$year-1]/12 * $stock_data[$year-1]);
		$_receivables = 0; //not to take in consideration
		$_replacement_fixed_assets = 0; //not to take in consideration
		
		$_var_working_capital = $_liabilities + $_stock + $_receivables + $_replacement_fixed_assets;
		$debugvar_working_capital[$year] = $_var_working_capital;
		$debugvar_liabilities_change[$year] = $_liabilities;
		
		$debugvar_stock_change[$year] = $_stock;

		//caculate liquidation revenue
		$liquidation_revenue = 0;

		if($year == $cer_basicdata["cer_basicdata_lastyear"])
		{
			
			$intagibles_total_revenue = $intagibles_total * $cer_basicdata["cer_bascidata_liquidation_keymoney"] / 100;
			$intagibles_total_revenue = $intagibles_total_revenue;
			$stock_revenue = ($material_of_products_values[$year]/12 * $stock_data[$year]) * $cer_basicdata["cer_bascidata_liquidation_stock"] / 100;
			
			$salary_cost = $indirect_salaries_values[$year] * $cer_basicdata["cer_bascicdate_liquidation_staff"] / 100;

			$deposit_revenue = $deposit * $cer_basicdata["cer_basicdata_liquidation_deposit"] / 100;
			$deposit_revenue = $deposit_revenue;

			$liquidation_revenue = $residual_value_investment + $intagibles_total_revenue + $stock_revenue - $_liabilities + $salary_cost + $_receivables + $deposit_revenue;
			
		}
		$debug_initial_investment[$year] = $liquidation_revenue;

		//total cash flow
		$_total_cash_flow = $operating_income01_values[$year] + $prepayed_rents[$year] + $depreciation_values[$year] - $_var_working_capital + $liquidation_revenue;
		$total_cash_flow_values[$year] = $_total_cash_flow;

		

		//income taxes
		$_income_taxes = $income_taxes_values[$year];
		
		//finacncial expenses
		
		$_interest_rate = $cer_basicdata_interest_rates[$year];
		$_finacial_expenses = $total_cash_flow_fcf_values[$year-1] * $_interest_rate / 100;

				
		if($year == $last_year)
		{
			
			$_finacial_expenses = $_finacial_expenses /12 * $last_month;
		}
		$debugvar_finacial_expenses[$year] = $_finacial_expenses;
		
		$total_cash_flow_fcf_values[$year] = $total_cash_flow_fcf_values[$year-1] + $total_cash_flow_values[$year] + $_finacial_expenses - $_income_taxes;

						
		//cash flow
	
		$cash_flow_values[$year] = $total_cash_flow_fcf_values[$year] - $total_cash_flow_fcf_values[$year-1];

		$cfs[] = $cash_flow_values[$year];
		$cash_flow_whole_sale_values[$year] = $_total_cash_flow + $stat_wholesale_margin[$year];
		$cfs2[] = $cash_flow_whole_sale_values[$year];


		//pay back retail
		if(($payback_period_start + 1) < ($last_year - $first_year + 1))
		{
			$payback_period_start = $payback_period_start + 1;
		}
		else
		{
			$payback_period_start = 0;
		}

		$tmp = (1+$discount_rate);
		$tmp = pow($tmp ,$payback_period_start);
		$tmp = $_total_cash_flow / $tmp;
		$tmp =  $pay_back_cummunlated_retail_values[$year-1] + $tmp;
		$pay_back_cummunlated_retail_values[$year] = $tmp;
		
		if($pay_back_cummunlated_retail_values[$year] < 0)
		{
			$pay_back_period_retail_values[$year] = 0;
		}
		elseif($pay_back_period_retail <=0)
		{
			$tmp = $pay_back_cummunlated_retail_values[$year] - $pay_back_cummunlated_retail_values[$year-1];
			if($tmp != 0)
			{
				$pay_back_period_retail_values[$year] = $pay_back_cummunlated_retail_values[$year-1]/$tmp + $payback_period_start;
			}
			else
			{
				$pay_back_period_retail_values[$year] = 0;
			}
		}
		else
		{
			$pay_back_period_retail_values[$year] = 0;
		}

		
		if($pay_back_period_retail <= 0)
		{
			$pay_back_period_retail = $pay_back_period_retail_values[$year];
		}
		
		//pay back wholesale
		$tmp = (1+$discount_rate);
		$tmp = pow($tmp ,$payback_period_start);
		$tmp = $cash_flow_whole_sale_values[$year] / $tmp;
		$tmp = $pay_back_cummunlated_wholesale_values[$year-1] + $tmp;
		$pay_back_cummunlated_wholesale_values[$year] = $tmp;
		$pay_back_period_wholesale_values[$year] = $tmp;


		if($pay_back_cummunlated_wholesale_values[$year] < 0)
		{
			$pay_back_period_wholesale_values[$year] = 0;
		}
		elseif($pay_back_period_wholesale <= 0)
		{
			$tmp = $pay_back_cummunlated_wholesale_values[$year] - $pay_back_cummunlated_wholesale_values[$year-1];
			
			if($tmp != 0)
			{
				$pay_back_period_wholesale_values[$year] = $pay_back_cummunlated_wholesale_values[$year-1]/$tmp + $payback_period_start;
			}
			else
			{
				$pay_back_period_wholesale_values[$year] = 0;
			}
		}
		else
		{
			$pay_back_period_wholesale_values[$year] = 0;
		}
		
		if($pay_back_period_wholesale_values[$first_year] == 0)
		{
			if($pay_back_period_wholesale <= 0)
			{
				$pay_back_period_wholesale = $pay_back_period_wholesale_values[$year];
			}
		}
		else
		{
			$pay_back_period_wholesale = 0;
		}
	}
}

//calculate percentage shares
foreach($years as $key=>$year)
{
    if($total_net_sales_values[$year] != 0 and $total_gross_sales_values[$year] != 0)
	{
		
		$sales_watches_shares[$year] = 100*$sales_watches_values[$year] / $total_net_sales_values[$year];
	    $sales_jewellery_shares[$year] = 100*$sales_jewellery_values[$year]  / $total_net_sales_values[$year];
		$sales_customer_service_shares[$year] = 100*$sales_customer_service_values[$year]  / $total_net_sales_values[$year];
		$total_gross_sales_shares[$year] = 100*$total_gross_sales_values[$year]  / $total_net_sales_values[$year];;
		
		
		//$sales_reduction_shares[$year] = 100*$sales_reduction_values[$year] / $total_gross_sales_values[$year];
		$sales_reduction_shares[$year] = 100*$sales_reduction_values[$year] / $total_net_sales_values[$year];
		
		//$total_net_sales_shares[$year] = 100*$total_gross_sales_values[$year] / $total_net_sales_values[$year];
		$total_net_sales_shares[$year] = 100;

		$material_of_products_shares[$year] = 100*$material_of_products_values[$year] / $total_net_sales_values[$year];


		$total_goss_margin_shares[$year] = 100*$total_gross_margin_values[$year] / $total_net_sales_values[$year];

		$tmp = $interco_contribution_values[$year] - $marketing_expenses_values[$year];
		$marketing_expenses_shares[$year] = 100*$tmp / $total_net_sales_values[$year];


		$indirect_salaries_shares[$year] = 100*$indirect_salaries_values[$year] / $total_net_sales_values[$year];
		$rents_shares[$year] = 100*$rents_total_values[$year] / $total_net_sales_values[$year];
		$income_taxes_shares[$year] = 100*$income_taxes_values[$year] / $total_net_sales_values[$year];
		$auxmat_shares[$year] = 100*$auxmat_values[$year] / $total_net_sales_values[$year];
		$sales_admin_shares[$year] = 100*$sales_admin_values[$year] / $total_net_sales_values[$year];
		$depreciation_shares[$year] = 100*$depreciation_values[$year] / $total_net_sales_values[$year];

		$prepayed_rent_values[$year] = $prepayed_rents[$year];
		$prepayed_rent_shares[$year] = 100*$prepayed_rent_values[$year] / $total_net_sales_values[$year];

		$other_expenses_shares[$year] = 100*$other_expenses_values[$year] / $total_net_sales_values[$year];
		$total_indirect_expenses_shares[$year] = 100*$total_indirect_expenses_values[$year] / $total_net_sales_values[$year];

		$operating_income01_shares[$year] = 100*$operating_income01_values[$year] / $total_net_sales_values[$year];;
		$operating_income02_shares[$year] = 100*$operating_income02_values[$year] / $total_net_sales_values[$year];;

	}
}

//add aux_mat_to_rents, Mail Sébastien 31.3.09, add aux mat to rents
/*
foreach($rents_total_values as $year=>$amount)
{
	if(array_key_exists($year, $auxmat_values))
	{
		$rents_total_values[$year] = $rents_total_values[$year] + $auxmat_values[$year];
		$rents_shares[$year] = 100*$rents_total_values[$year] / $total_net_sales_values[$year];

	}
}
*/

// net present value and internal rate of interest retail
$cfs = array();
foreach($total_cash_flow_values as $key=>$value)
{
	$cfs[] = $value;
}

$fin = new Financial;
$net_present_value_retail = $fin->NPV($discount_rate , $cfs);
$net_present_value_retail = $net_present_value_retail - $_initial_investment;

$cfs1 = array();
$cfs1[] = -1*$_initial_investment;

foreach($total_cash_flow_values as $key=>$value)
{
	$cfs1[] = $value;
}



$discounted_cash_flow_retail = 100*$fin->IRR($cfs1, -0.1);

if(!$discounted_cash_flow_retail)
{
	$discounted_cash_flow_retail = "0.00%";
}
else
{
	$discounted_cash_flow_retail = $discounted_cash_flow_retail . "%";
}



// net present value and internal rate of interest wholesale
$net_present_value_wholesale = $fin->NPV($discount_rate , $cfs2);
$net_present_value_wholesale = $net_present_value_wholesale -$_initial_investment;

$cfs3= array();
$cfs3[] = -$_initial_investment;

foreach($cfs2  as $key=>$value)
{
	$cfs3[] = $value;
}

$discounted_cash_flow_wholesale = 100*$fin->IRR($cfs3, -0.1);

if(!$discounted_cash_flow_wholesale)
{
	$discounted_cash_flow_wholesale = "0.00%";
}
else
{
	$discounted_cash_flow_wholesale = $discounted_cash_flow_wholesale . "%";
}



//sellouts
$sellout_ending_year = (int)substr($cer_basicdata["date_created"], 0, 4);

if($sellout_ending_year < (int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4))
{
	$sellout_ending_year =(int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4);
}

if($sellout_ending_year > date("Y"))
{
	$sellout_ending_year = date("Y");
}

if($sellout_ending_year == $cer_basicdata["cer_basicdata_firstyear"])
{
	$sellout_ending_year--;
}
$sellout_starting_year = $sellout_ending_year - 3;









//get number of months in first and last year
if(param('pid') 
   and $project["project_projectkind"] == 3
   and $project["project_planned_takeover_date"] != NULL 
   and $project["project_planned_takeover_date"] != '0000-00-00')
{

	if(substr($project["project_planned_takeover_date"], 5,2) > $cer_basicdata["cer_basicdata_firstmonth"])
	{
		$number_of_months_first_year = 13-(int)substr($project["project_planned_takeover_date"], 5,2);
	}
	else
	{
		//$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];

		if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
		{
			$number_of_months_first_year = $cer_basicdata["cer_basicdata_lastmonth"]-$cer_basicdata["cer_basicdata_firstmonth"] + 1;
		}
		else
		{
			//$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
			if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
			{
				$number_of_months_first_year = $cer_basicdata["cer_basicdata_lastmonth"]-$cer_basicdata["cer_basicdata_firstmonth"] + 1;
			}
			else
			{
				$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
			}
		}
	}
}
elseif(param('pid') 
	and $project["project_real_opening_date"] != NULL 
	and $project["project_real_opening_date"] != '0000-00-00')
{
	
	if($cer_version > 0) //former version of business plan
	{
		//get the last agreed opening date from project tracking
		if($cer_basicdata["cer_basicdata_version_context"] == 'ln')
		{
			$version_real_opening_date = get_version_agreed_opening_date($cer_basicdata["cer_basicdata_project"],  $project["project_real_opening_date"], $cer_version, 'ln');
		}
		else
		{
			$version_real_opening_date = get_version_agreed_opening_date($cer_basicdata["cer_basicdata_project"],  $project["project_real_opening_date"], $cer_version, 'cer');
		}
	}
	else
	{
		$version_real_opening_date = $project["project_real_opening_date"];
	}


	if(substr($version_real_opening_date, 5,2) > $cer_basicdata["cer_basicdata_firstmonth"])
	{
		$number_of_months_first_year = 13-(int)substr($version_real_opening_date, 5,2);
	}
	else
	{
		//$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];

		if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
		{
			$number_of_months_first_year = $cer_basicdata["cer_basicdata_lastmonth"]-$cer_basicdata["cer_basicdata_firstmonth"] + 1;
		}
		else
		{
			//$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
			if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
			{
				$number_of_months_first_year = $cer_basicdata["cer_basicdata_lastmonth"]-$cer_basicdata["cer_basicdata_firstmonth"] + 1;
			}
			else
			{
				$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
			}
		}
	}
}
else
{
	//$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
	if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
	{
		$number_of_months_first_year = $cer_basicdata["cer_basicdata_lastmonth"]-$cer_basicdata["cer_basicdata_firstmonth"] + 1;
	}
	else
	{
		$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
	}
}
$number_of_months_last_year = $cer_basicdata["cer_basicdata_lastmonth"];


if($number_of_months_first_year_lease_contract < (13 - $cer_basicdata["cer_basicdata_firstmonth"])) {
	$number_of_months_first_year = 13 - $cer_basicdata["cer_basicdata_firstmonth"];
}


//get rents

if(param("pid") > 0 )
{
	$first_full_year_ln = $cer_basicdata_ln["cer_basicdata_firstyear"];
	if($cer_basicdata_ln["cer_basicdata_firstmonth"] > 1)
	{
		$first_full_year_ln = $cer_basicdata_ln["cer_basicdata_firstyear"]+1;
	}
}


$last_full_year = $cer_basicdata["cer_basicdata_lastyear"];
if($cer_basicdata["cer_basicdata_lastmonth"] < 12)
{
	$last_full_year = $cer_basicdata["cer_basicdata_lastyear"]-1;
}

$fullrent_firstyear = 0;
$fixedrent_firstyear = 0;
$turnoverrent_firstyear = 0;
$turnoverrent_firstyear_full_amount = 0;


if($first_full_year > 0 and $cer_basicdata["cer_basicdata_firstyear"] < $cer_basicdata["cer_basicdata_lastyear"])
{
	$sql_e = "select sum(cer_expense_amount) as cer_expense_amount from  " .  $_table_expenses .
		   " where " . $_filter_expenses . " cer_expense_" . $_id_suffix . " = " . $_id . 
		   " and cer_expense_type in (2,20) " . 
		   " and cer_expense_year = " . $first_full_year;

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$fixedrent_firstyear = $row_e["cer_expense_amount"];
	}

	$sql_e = "select cer_expense_amount from  " . $_table_expenses . 
		   " where " . $_filter_expenses . " cer_expense_" . $_id_suffix . " = " . $_id . 
		   " and cer_expense_type = 16 " . 
		   " and cer_expense_year = " . $first_full_year;

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$turnoverrent_firstyear = $row_e["cer_expense_amount"];
		$turnoverrent_firstyear_full_amount = $row_e["cer_expense_amount"];
	}
}
elseif($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
{
	$sql_e = "select sum(cer_expense_amount) as cer_expense_amount from  " . $_table_expenses . 
		   " where " . $_filter_expenses . " cer_expense_" . $_id_suffix . " = " . $_id .
		   " and cer_expense_type in (2,20) " . 
		   " and cer_expense_year = " . $cer_basicdata["cer_basicdata_firstyear"];

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$fixedrent_firstyear = $row_e["cer_expense_amount"];
	}

	$sql_e = "select cer_expense_amount from  " . $_table_expenses . 
		   " where " . $_filter_expenses . " cer_expense_" . $_id_suffix . " = " . $_id . 
		   " and cer_expense_type = 16 " . 
		   " and cer_expense_year = " . $cer_basicdata["cer_basicdata_firstyear"];


	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$turnoverrent_firstyear = $row_e["cer_expense_amount"];
		$turnoverrent_firstyear_full_amount = $row_e["cer_expense_amount"];
	}
}

$fullrent_firstyear = $fixedrent_firstyear + $turnoverrent_firstyear;

//$fullrent_firstyear = $fullrent_firstyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
$fullrent_firstyear = round($fullrent_firstyear/1000, 0);

$fixedrent_firstyear = round($fixedrent_firstyear/1000, 0);


//$turnoverrent_firstyear = $turnoverrent_firstyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];

$turnoverrent_firstyear = round($turnoverrent_firstyear/1000, 0);



$fixedrent_firstyear_ln = $fixedrent_firstyear;
$turnoverrent_firstyear_ln = $turnoverrent_firstyear;
$turnoverrent_firstyear_full_amount_ln = $turnoverrent_firstyear_full_amount;

if(param("pid") > 0 )
{
	if($first_full_year > 0 and $cer_basicdata_ln["cer_basicdata_firstyear"] < $cer_basicdata_ln["cer_basicdata_lastyear"])
	{
		$sql_e = "select sum(cer_expense_amount) as cer_expense_amount from  " .  $_table_expenses .
			   " where " . $_filter_expenses_ln . " cer_expense_" . $_id_suffix . " = " . $_id . 
			   " and cer_expense_type in(2,20) " . 
			   " and cer_expense_year = " . $first_full_year;

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$fixedrent_firstyear_ln = $row_e["cer_expense_amount"];
		}

		$sql_e = "select cer_expense_amount from  " . $_table_expenses . 
			   " where " . $_filter_expenses_ln . " cer_expense_" . $_id_suffix . " = " . $_id . 
			   " and cer_expense_type = 16 " . 
			   " and cer_expense_year = " . $first_full_year;

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$turnoverrent_firstyear_ln = $row_e["cer_expense_amount"];
			$turnoverrent_firstyear_full_amount_ln = $row_e["cer_expense_amount"];
		}
	}
	elseif($cer_basicdata_ln["cer_basicdata_firstyear"] == $cer_basicdata_ln["cer_basicdata_lastyear"])
	{
		$sql_e = "select sum(cer_expense_amount) as cer_expense_amount from  " . $_table_expenses . 
			   " where " . $_filter_expenses_ln . " cer_expense_" . $_id_suffix . " = " . $_id .
			   " and cer_expense_type in (2,20) " . 
			   " and cer_expense_year = " . $cer_basicdata["cer_basicdata_firstyear"];

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$fixedrent_firstyear_ln = $row_e["cer_expense_amount"];
		}

		$sql_e = "select cer_expense_amount from  " . $_table_expenses . 
			   " where " . $_filter_expenses_ln . " cer_expense_" . $_id_suffix . " = " . $_id . 
			   " and cer_expense_type = 16 " . 
			   " and cer_expense_year = " . $cer_basicdata["cer_basicdata_firstyear"];


		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$turnoverrent_firstyear_ln = $row_e["cer_expense_amount"];
			$turnoverrent_firstyear_full_amount_ln = $row_e["cer_expense_amount"];
		}
	}


	$fullrent_firstyear_ln = $fixedrent_firstyear_ln + $turnoverrent_firstyear_ln;
	$fullrent_firstyear_ln = round($fullrent_firstyear_ln/1000, 0);

	$fixedrent_firstyear_ln = round($fixedrent_firstyear_ln/1000, 0);
	$turnoverrent_firstyear_ln = round($turnoverrent_firstyear_ln/1000, 0);
}


//scenario 80%
$apply_80_percent_scenario = 0;

if(param("pid") > 0)
{
	if(($ln_basicdata["ln_basicdata_submitted"] == NULL 
	   or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == '0000')
	   and ($cer_basicdata["cer_basicdata_submitted"] == NULL 
	   or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == '0000'))
	{
		$apply_80_percent_scenario = 1;
	}
	elseif($ln_basicdata["ln_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014
		and ($cer_basicdata["cer_basicdata_submitted"] == NULL 
		or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == '0000'))
	{
		$apply_80_percent_scenario = 1;
	}
	elseif(($ln_basicdata["ln_basicdata_submitted"] == NULL 
	   or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == '0000')
	   and $cer_basicdata["cer_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014)
	{
		$apply_80_percent_scenario = 1;
	}
	elseif($ln_basicdata["ln_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014
		   and $cer_basicdata["cer_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014)
	{
		$apply_80_percent_scenario = 1;
	}
	elseif($ln_basicdata["ln_basicdata_resubmitted"] > DATE_NEW_RENTAL_TOOL_2014
		   or $cer_basicdata["ln_basicdata_rejected"] > DATE_NEW_RENTAL_TOOL_2014)
	{
		$apply_80_percent_scenario = 1;
	}
}
else // draft
{
	$apply_80_percent_scenario = 1;
}


if($apply_80_percent_scenario == 1)
{

	if(param("pid") > 0)
	{
		
		$turnoverbased_80_percent_values = update_turnoverbased_rental_cost_version2014($project["project_id"], $cer_version, true);
	}
	else
	{
		$turnoverbased_80_percent_values = update_turnoverbased_rental_cost(param("did"),true);
	}

	

	$turnoverbasedrents_80_percent_values = $turnoverbased_80_percent_values ["tob_rents"];

	/*
	echo '<pre />';
	print_r($turnoverbasedrents_80_percent_values);
	abc();
	*/


	$taxes_80_percent_values = $turnoverbased_80_percent_values ["taxes"];
	$passenger_index_80_percent_values = $turnoverbased_80_percent_values ["passenger_index"];

	$total_gross_sales_80_percent_values = array();
	$sales_reduction_80_percent_values = array();
	$total_net_sales_80_percent_values = array();


	$operating_income02_80_percent_values = array();
	$operating_income01_80_percent_values = array();
	$operating_income02_80_percent_shares = array();
	$taxes_on_fixed_rents_80_percent_values = array();
	$passenger_index_on_fixed_rents_80_percent_values = array();

	foreach($years as $key=>$year)
	{
		$total_gross_sales_80_percent_values[$year] = 0.8*$total_gross_sales_values[$year];

		$sales_reduction_80_percent_values[$year] = $total_gross_sales_80_percent_values[$year] * $cer_basicdata["cer_basicdata_sales_reduction"] / 100;
		
		$total_net_sales_80_percent_values[$year] = $total_gross_sales_80_percent_values[$year] - $sales_reduction_80_percent_values[$year];


		if(array_key_exists($year, $turnoverbasedrents_80_percent_values))
		{
			$rents_80_percent_values[$year] = $fixedrents_values[$year] + $turnoverbasedrents_80_percent_values[$year] + $additionalrents_values[$year];

		}
		else
		{
			$rents_80_percent_values[$year] = $fixedrents_values[$year] + $additionalrents_values[$year];
		}

		
		$total_gross_margin_80_percent_values[$year] = $total_gross_sales_80_percent_values[$year] - $sales_reduction_80_percent_values[$year] - 0.8*$material_of_products_values[$year];

		
		$total_indirect_expenses_80_percent_values[$year] = $indirect_salaries[$year] + $rents_80_percent_values[$year] + $prepayed_rents[$year] + $auxmat[$year] + $depreciation[$year] + $sales_admin[$year] + $other_expenses[$year] + $income_taxes[$year];
		
		
		$tmp9_80_percent =  $total_gross_margin_80_percent_values[$year] - $marketing_expenses_values[$year] + $interco_contribution_values[$year];
		$tmp10_80_percent = $tmp9_80_percent - $total_indirect_expenses_80_percent_values[$year];
		$operating_income01_80_percent_values[$year] = $tmp10_80_percent;


		$tmp11_80_percent = 0.8*$material_of_products_values[$year] * $cer_basicdata["cer_basicdata_wholesale_margin"] / 100; // Stat. Wholesale Margin
		
		$tmp12_80_percent = $total_gross_margin_80_percent_values[$year] - $marketing_expenses_values[$year] + $interco_contribution_values[$year] - $total_indirect_expenses_80_percent_values[$year]; // operating income
	
		$operating_income02_80_percent_values[$year] = $tmp11_80_percent + $tmp12_80_percent;

		
		if($total_net_sales_values[$year] > 0)
		{
			$operating_income02_80_percent_shares[$year] = 100*$operating_income02_80_percent_values[$year] / ($total_net_sales_80_percent_values[$year]);
		}
		else
		{
			$operating_income02_80_percent_shares[$year] = "";
		}
	}
	
	
}


?>