<?php

if($cer_basicdata["cer_basicdata_lastyear"] > 0)
{
	$page->add_tab("genral", "General", "cer_application_general.php?pid=" . param("pid"), $target = "_self", $flags = 0);
	//$page->add_tab("location", "Location", "cer_application_location.php?pid=" . param("pid"), $target = "_self", $flags = 0);

	if($project["project_projectkind"] != 8)
	{	if($form_type == "INR03")
		{
			$page->add_tab("franchisee", "Relations and Uploads", "cer_application_franchisee.php?pid=" . param("pid"), $target = "_self", $flags = 0);
		}
		elseif($project["project_cost_type"] != 6 and $project["project_postype"] !=2)
		{
			$page->add_tab("franchisee", "Franchisee", "cer_application_franchisee.php?pid=" . param("pid"), $target = "_self", $flags = 0);
		}
		else
		{
			$page->add_tab("franchisee", "Owner Company", "cer_application_franchisee.php?pid=" . param("pid"), $target = "_self", $flags = 0);
		}
		if($form_type == "AF")
		{
			$page->add_tab("payment", "Agreement and Payment Terms", "af_paymentterms.php?pid=" . param("pid"), $target = "_self", $flags = 0);
		}
		if($form_type == "CER" or $form_type == "AF")
		{
			$page->add_tab("lease", "Lease Conditions", "cer_application_lease.php?pid=" . param("pid"), $target = "_self", $flags = 0);
			$page->add_tab("rental", "Rental Costs", "cer_application_rental.php?pid=" . param("pid"), $target = "_self", $flags = 0);
		}

	
		if($project["project_projectkind"] != 5) //all projects except lease reneval
		{
			$page->add_tab("investment", "Key Money/Investment", "cer_application_investments.php?pid=" . param("pid"), $target = "_self", $flags = 0);
		}
		else
		{
			$page->add_tab("investment", "Residual Value", "cer_application_investments.php?pid=" . param("pid"), $target = "_self", $flags = 0);
		}

		
		$page->add_tab("revenues", "Revenues", "cer_application_revenues.php?pid=" . param("pid"), $target = "_self", $flags = 0);
		$page->add_tab("salaries", "Human Resources", "cer_application_salaries.php?pid=" . param("pid"), $target = "_self", $flags = 0);
		$page->add_tab("expenses", "Expenses", "cer_application_expenses.php?pid=" . param("pid"), $target = "_self", $flags = 0);



		if(($project["project_projectkind"] == 2 
				or $project["project_projectkind"] == 3 
				or $project["project_projectkind"] == 4 
				or $project["project_projectkind"] == 5)) // renovation or takover/renovation or take over or lease renewal
		{
			$page->add_tab("sellout", "Sellouts", "sellouts.php?pid=" . param("pid"), $target = "_self", $flags = 0);
		}
		elseif($project["project_projectkind"] == 6 and $project["project_relocated_posaddress_id"] > 0) // relocation
		{
			$page->add_tab("sellout", "Sellouts", "sellouts.php?pid=" . param("pid"), $target = "_self", $flags = 0);

			/*
			$latest_project = get_latest_pos_project($project["project_relocated_posaddress_id"]);

			if(count($latest_project) > 0)
			{
				$page->add_tab("sellout", "Sellouts", "sellouts.php?pid=" . param("pid"), $target = "_self", $flags = 0);
			}
			*/
		}

		if($form_type == "AF" and ($project["project_projectkind"] == 1 
				or $project["project_projectkind"] == 2 
				or $project["project_projectkind"] == 3 
				or $project["project_projectkind"] == 6
				or $project["project_projectkind"] == 9)) // renovation or takover/renovation or take over or lease renewal
		{
			$page->add_tab("distribution", "Distribution Analysis", "cer_application_distribution.php?pid=" . param("pid"), $target = "_self", $flags = 0);
		}

		$page->add_tab("cash", "Calculation Parameters", "cer_application_cashflow.php?pid=" . param("pid"), $target = "_self", $flags = 0);
	}
	else
	{
		$page->add_tab("investment", "Investment", "cer_application_investments.php?pid=" . param("pid"), $target = "_self", $flags = 0);

		$page->add_tab("attachments", "Attachments", "cer_application_attachments.php?pid=" . param("pid"), $target = "_self", $flags = 0);
	}


	$page->tabs();
}


?>