<?php
$y = $y + $standard_y+0.8;
$x = $margin_left;
$x1 = $x+40;
$x2 = 77;
$x3 = 181;


	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 3);
	$pdf->Cell(40, 3.5, " ", 1, "", "L");
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(40, 3.5, "Exchange Rate " . $currency_symbol_page_bottom, 0, "", "L");

	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "B", 3);
	$pdf->Cell(23, 3.5, " ", 1, "", "L");
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(23, 3.5, ($exchange_rate_page_bottom), 0, "", "R");


	$y = $y + 3.5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 3);
	$pdf->Cell(40, 3.5, " ", 1, "", "L");
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(40, 3.5, "Factor ", 0, "", "L");

	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "B", 3);
	$pdf->Cell(23, 3.5, " ", 1, "", "L");
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(23, 3.5, ($exchange_rate_factor_page_bottom), 0, "", "R");


	// sellouts out in the past
	
	$y = $y - 3.5;
	
	//watches sold out in the past
	// renovation or tekover/renovation or lease renewal or new relocation project
	if(($project["project_projectkind"] == 2 
        or $project["project_projectkind"] == 3
		or $project["project_projectkind"] == 5)
		or ($project["project_projectkind"] == 6 and $project["project_relocated_posaddress_id"] > 0)
	   )

	{
		
		$x = $x2;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 3);
		$pdf->Cell(20,3.5,"",1, 0, 'L', 0);
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20,3.5,"Sellouts",0, 0, 'L', 0);

		$pdf->SetXY($x,$y +3.5);
		$pdf->SetFont("arialn", "B", 3);
		$pdf->Cell(20,3.5,"",1, 0, 'L', 0);
		$pdf->SetXY($x,$y +3.5);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20,3.5,"Watches",0, 0, 'L', 0);
		
		
			
		$x = $x2+20;
		$pdf->SetXY($x,$y);
		$sellout_ending_year = (int)substr($cer_basicdata["date_created"], 0, 4);

		if($sellout_ending_year < (int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4))
		{
			$sellout_ending_year =(int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4);
		}

		if($sellout_ending_year > date("Y"))
		{
			$sellout_ending_year = date("Y");
		}

		$sellout_starting_year =$sellout_ending_year - 3;

		
		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
		{
			$x = $pdf->getX();
			$pdf->SetFont("arialn", "B", 3);
			$pdf->Cell(20,3.5,"",1, 0, 'R', 0);
			$pdf->SetFont("arialn", "", 8);
			$pdf->SetXY($x,$y);
			if(array_key_exists($i, $sellouts_watches))
			{
				$pdf->Cell(20,3.5,$i . "(" . $sellouts_months[$i] . ")",0, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(20,3.5,$i ,0, 0, 'R', 0);
			}


			
		}
		
		$pdf->SetFont("arialn", "", 8);
		$x = $x2 + 20;
		$pdf->SetXY($x,$y+3.5);
		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
		{
			$x = $pdf->getX();
			$pdf->SetFont("arialn", "B", 3);
			$pdf->Cell(20,3.5,"",1, 0, 'R', 0);
			$pdf->SetFont("arialn", "", 8);
			$pdf->SetXY($x,$y+3.5);

			if(array_key_exists($i, $sellouts_watches))
			{
				$pdf->Cell(20,3.5,number_format($sellouts_watches[$i], 0, ".", "'"),0, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(20,3.5,"" ,0, 0, 'R', 0);
			}
			
		}
		
		$x = $x3;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 3);
		$pdf->Cell(20,3.5,"",1, 0, 'L', 0);
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20,3.5,"Sellouts",0, 0, 'L', 0);
	
		$pdf->SetXY($x,$y + 3.5);
		$pdf->SetFont("arialn", "B", 3);
		$pdf->Cell(20,3.5,"",1, 0, 'L', 0);
		$pdf->SetXY($x,$y + 3.5);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(20,3.5,"Watch Straps",0, 0, 'L', 0);
		
		
		
		$x = $x3+20;
		$pdf->SetXY($x,$y);
		$sellout_ending_year = (int)substr($cer_basicdata["date_created"], 0, 4);

		if($sellout_ending_year < (int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4))
		{
			$sellout_ending_year =(int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4);
		}

		if($sellout_ending_year > date("Y"))
		{
			$sellout_ending_year = date("Y");
		}

		$sellout_starting_year =$sellout_ending_year - 3;

		
		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
		{
			$x = $pdf->getX();
			$pdf->SetFont("arialn", "B", 3);
			$pdf->Cell(20,3.5,"",1, 0, 'R', 0);
			$pdf->SetFont("arialn", "", 8);
			$pdf->SetXY($x,$y);
			if(array_key_exists($i, $sellouts_watches))
			{
				$pdf->Cell(20,3.5,$i . "(" . $sellouts_months[$i] . ")",0, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(20,3.5,$i ,0, 0, 'R', 0);
			}


			
		}
		
		$pdf->SetFont("arialn", "", 8);
		$x = $x3 + 20;
		$pdf->SetXY($x,$y+3.5);
		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
		{
			$x = $pdf->getX();
			$pdf->SetFont("arialn", "B", 3);
			$pdf->Cell(20,3.5,"",1, 0, 'R', 0);
			$pdf->SetFont("arialn", "", 8);
			$pdf->SetXY($x,$y+3.5);

			
			if(array_key_exists($i, $sellouts_bjoux))
			{
				$pdf->Cell(20,3.5,number_format($sellouts_bjoux[$i], 0, ".", "'"),0, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(20,3.5,"" ,0, 0, 'R', 0);
			}
			
			
		}

	}

	
?>