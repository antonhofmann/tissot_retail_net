<?php

$tab2016 = '<div id="tab2016_container">';
$tab2016 .= '<ul id="tab2016">';

$class = '';
$link = 'ln_general.php?pid=' . param("pid");
if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">General Information</a></li>';

$class = '';
$link = 'ln_project_information.php?pid=' . param("pid");
if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Project Information</a></li>';



if($form_type == 'AF' 
   or $form_type == 'INR03'
	) {

	$class = '';
	$link = 'ln_application_relation.php?pid=' . param("pid");
	if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
	$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Relation Information</a></li>';
}

$class = '';
$link = 'cer_application_general.php?pid=' . param("pid");
if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Business Plan</a></li>';


if(($project["project_projectkind"] == 2 
		or $project["project_projectkind"] == 3 
		or $project["project_projectkind"] == 4 
		or $project["project_projectkind"] == 5)) // renovation or takover/renovation or take over or lease renewal
{
	
	$class = '';
	$link = 'sellouts.php?pid=' . param("pid") . '&context=ln';
	if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
	$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Sellouts</a></li>';
}
elseif($project["project_projectkind"] == 6 
	and $project["project_relocated_posaddress_id"] > 0) // relocation
{
	$class = '';
	$link = 'sellouts.php?pid=' . param("pid") . '&context=ln';
	if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
	$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Sellouts</a></li>';
}

if($form_type != 'INR03' and $project["project_projectkind"] != 8)
{
	$class = '';
	$link = 'cer_application_distribution.php?pid=' . param("pid"). "&context=ln";
	if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
	$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Distribution Analysis</a></li>';

	$class = '';
	$link = 'cer_application_brands.php?pid=' . param("pid"). "&context=ln";
	if(strpos($_SERVER["REQUEST_URI"], $link) > 0){$class = 'class="active"';}
	$tab2016 .= '<li ' .  $class . '><a href="'. $link .'">Other SG Brands</a></li>';
}

$tab2016 .= '</ul>';
$tab2016 .= '</div>';

echo $tab2016;
?>