<?php
//get data
if(param("pid") > 0 )
{
	$project = get_project(param("pid"));
}

//*change of calculation model in 2012
$calc_model = 2012;

if(param("pid") > 0 )
{
	$cost_of_production_values = update_cost_of_products_sold(param("pid"), $calc_model);
	if($project["order_date"] < '2012-01-24' and $project["order_actual_order_state_code"] >= 840)
	{
		$calc_model = 2008;
	}

	//get sellouts of existing pos locations
	$sellouts_watches = array();
	$sellouts_bjoux = array();
	$sellouts_months = array();

	$sql = "select posorder_posaddress " . 
		   "from posorders " . 
		   "where posorder_order = " . $project["project_order"];

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$pos_id = $row["posorder_posaddress"];


	if(!$pos_id and $project["project_relocated_posaddress_id"] > 0)
	{
		$latest_project = get_latest_pos_project($project["project_relocated_posaddress_id"]);
		
		if(count($latest_project) > 0)
		{
			$old_project = get_project($latest_project["project_id"]);

			$sql = "select posorder_posaddress " . 
				   "from posorders " . 
				   "where posorder_order = " . $old_project["project_order"];

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			$pos_id = $row["posorder_posaddress"];
		}

	}

	$sql = "select * from cer_sellouts " . 
		   "where cer_sellout_version = " . dbquote($cer_version) . 
		   " and cer_sellout_project_id = " . dbquote($project["project_id"]) . 
		   " order by cer_sellout_year ASC";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$sellouts_watches[$row["cer_sellout_year"]] = $row["cer_sellout_watches_units"];
		$sellouts_bjoux[$row["cer_sellout_year"]] = $row["cer_sellout_bijoux_units"];
		$sellouts_months[$row["cer_sellout_year"]] = $row["cer_sellout_month"];
	}

}
else
{
	$cost_of_production_values = update_cost_of_products_sold(param("did"));
}

if(param("pid") > 0 )
{
	$cer_basicdata = get_cer_basicdata(param("pid"), $cer_version);
}
else
{
	$cer_basicdata = get_draft_basicdata(param("did"));
}



$cer_basicdata_interest_rates = unserialize($cer_basicdata["cer_basic_data_interesrates"]);
$cer_basicdata_inflation_rates = unserialize($cer_basicdata["cer_basicdata_inflationrates"]);

$interest_rate_tmp = 0;
if(array_key_exists($cer_basicdata["cer_basicdata_firstyear"],$cer_basicdata_interest_rates))
{
	$interest_rate_tmp = $cer_basicdata_interest_rates[$cer_basicdata["cer_basicdata_firstyear"]];
}

$inflation_rate_tmp = 0;
if(!array_key_exists($cer_basicdata["cer_basicdata_firstyear"],$cer_basicdata_inflation_rates))
{
	foreach($cer_basicdata_inflation_rates as $year=>$rate)
	{
		$inflation_rate_tmp = $rate;
	}
}

//years and financial figures

$first_year = $cer_basicdata["cer_basicdata_firstyear"];
$first_month = $cer_basicdata["cer_basicdata_firstmonth"];
$last_year = $cer_basicdata["cer_basicdata_lastyear"];
$last_month = $cer_basicdata["cer_basicdata_lastmonth"];

if(!$last_year)
{
	$last_year = $first_year + 5;
	$last_month = 12;
}

$years = array();

for($y=$first_year;$y<=$last_year;$y++)
{
	$years[] = $y;

	if(!array_key_exists($y,$cer_basicdata_interest_rates))
	{
		$cer_basicdata_interest_rates[$y] = $interest_rate_tmp;
	}
	else
	{
		$interest_rate_tmp = $cer_basicdata_interest_rates[$y];
	}

	if(!array_key_exists($y,$cer_basicdata_inflation_rates))
	{
		$cer_basicdata_inflation_rates[$y] = $inflation_rate_tmp;
	}
	else
	{
		$inflation_rate_tmp = $cer_basicdata_inflation_rates[$y];
	}
}

$number_of_years = count($years);


//get intangibles
$intagible_amount = 0;
$intagible_name = "Key-/Premium Money/Goodwill";
$intagible_depryears = 0;
$cer_totals = 0;

if(param("pid") > 0 )
{
	$sql = "select * from cer_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
			"where posinvestment_type_intangible = 1 " . 
		    "and cer_investment_project = " . param("pid") . 
		    " and cer_investment_cer_version = " . $cer_version . " ";
}
else
{
	$sql = "select * from cer_draft_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
			"where posinvestment_type_intangible = 1 and cer_investment_draft_id = " . param("did");
}

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	
	$cer_totals = $cer_totals + $row["cer_investment_amount_cer_loc"];
	if($row["cer_investment_depr_years"] > $number_of_years)
	{
		$intagible_depryears = $number_of_years . 'Y-' . $row["cer_investment_depr_months"] . 'm';
	}
	else
	{
		$intagible_depryears = $row["cer_investment_depr_years"] . 'y-' . $row["cer_investment_depr_months"]. 'm';
	}
}

$intagibles_total = $cer_totals;
$intagible_amount = $intagibles_total;

//get all investments
$fixed_assets = array(0=>1, 1=>3,2=>5,3=>7,4=>11);
$amounts = array();
$investment_names = array();
$depryears = array();
$deprmonths = array();
$investment_total = 0;
$deposit = 0;
$other_noncapitalized_cost = 0;

if(param("pid") > 0 )
{
	$sql = "select * from cer_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where posinvestment_type_intangible = 0 and cer_investment_project = " . param("pid") . 
		   " and cer_investment_cer_version = " . $cer_version . 
		   " order by posinvestment_type_sortorder";
}
else
{
	$sql = "select * from cer_draft_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where posinvestment_type_intangible = 0 and cer_investment_draft_id = " . param("did") . 
		   " order by posinvestment_type_sortorder";
}

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$amounts[$row["posinvestment_type_id"]] = $row["cer_investment_amount_cer_loc"];
	$investment_names[$row["posinvestment_type_id"]] = $row["posinvestment_type_name"];
	
	if($row["cer_investment_depr_years"] > $number_of_years)
	{
		$depryears[$row["posinvestment_type_id"]] = $number_of_years;
		$deprmonths[$row["posinvestment_type_id"]] = 0;
	}
	else
	{
		$depryears[$row["posinvestment_type_id"]] = $row["cer_investment_depr_years"];
		$deprmonths[$row["posinvestment_type_id"]] = $row["cer_investment_depr_months"];
	}

	$cer_totals = $cer_totals + $row["cer_investment_amount_cer_loc"];

	if(in_array($row["posinvestment_type_id"], $fixed_assets))
	{
		$investment_total = $investment_total + $row["cer_investment_amount_cer_loc"];
	}

	if($row["posinvestment_type_id"] == 9) // deposit
	{
		$deposit = $row["cer_investment_amount_cer_loc"];
	}
	elseif($row["posinvestment_type_id"] == 13) // othe non capitalized cost
	{
		$other_noncapitalized_cost = $row["cer_investment_amount_cer_loc"];
	}
}
$investment_total = $investment_total;
$cer_totals = $cer_totals;
$renewal_option = "";
$epnr = "";

//get rental details
if(param("pid") > 0 )
{
	$posdata = get_pos_data($order_number);
	$epnr = $posdata["posaddress_eprepnr"];

	$posleases = get_pos_leasedata($posdata["posaddress_id"], $project["project_order"]);


	if(count($posleases) > 0)
	{
		$duration_in_years = $posleases["poslease_enddate"] - $posleases["poslease_startdate"];

		if(substr($posleases["poslease_startdate"], 5,2) == substr($posleases["poslease_enddate"], 5,2)) {
			$tmp1 = 12;
		}
		else
		{
			$tmp1 = 13 - substr($posleases["poslease_startdate"], 5,2) + substr($posleases["poslease_enddate"], 5,2);
		}

		$tmp2 = ($duration_in_years - 1)*12;
		$duration_in_years = round(($tmp1 + $tmp2) / 12, 1);

		$duration_in_years_and_months = floor(($tmp1 + $tmp2) / 12);
		$duration_in_years_and_months = $duration_in_years_and_months . "y-" . (($tmp1 + $tmp2) - ($duration_in_years_and_months*12)) . "m";
		



		$rental_period = substr($posleases["poslease_startdate"], 5,2) . '-' . substr($posleases["poslease_startdate"], 0,4) . '/' . substr($posleases["poslease_enddate"], 5,2) . '-' . substr($posleases["poslease_enddate"], 0,4) .  ' (' . $duration_in_years_and_months . ')';

		if($posleases["poslease_extensionoption"] != Null and $posleases["poslease_extensionoption"] != '0000-00-00')
		{
			$renewal_option = $posleases["poslease_extensionoption"] - $posleases["poslease_enddate"];
		}
		
		$exit_option = "-";
		if($posleases["poslease_exitoption"] != Null and $posleases["poslease_exitoption"] != '0000-00-00')
		{
			$exit_option = to_system_date($posleases["poslease_exitoption"]);
		}

		$hand_over_key = "-";
		if($posleases["poslease_handoverdate"] != Null and $posleases["poslease_handoverdate"] != '0000-00-00')
		{
			$hand_over_key = to_system_date($posleases["poslease_handoverdate"]);
		}

		$first_rent_payed = "-";
		if($posleases["poslease_firstrentpayed"] != Null and $posleases["poslease_firstrentpayed"] != '0000-00-00')
		{
			$first_rent_payed = to_system_date($posleases["poslease_firstrentpayed"]);
		}

		$m = substr($posleases["poslease_startdate"], 5, 2);
		$d = substr($posleases["poslease_startdate"], 8, 2);
		$y = substr($posleases["poslease_startdate"], 0, 4);
		$d1=mktime(0,0,0,$m,$d,$y);
		$m = substr($posleases["poslease_enddate"], 5, 2);
		$d = substr($posleases["poslease_enddate"], 8, 2);
		$y = substr($posleases["poslease_enddate"], 0, 4);
		$d2=mktime(0,0,0,$m,$d,$y);

		if($d2-$d1 > 0)
		{
			$termination_time = floor(($d2-$d1)/2628000);
		}


		$index_clause = "no";
		if($posleases["poslease_indexclause_in_contract"] == 1)
		{
			$index_clause = "yes";
		}

		$index_clause2 = "no";
		if($posleases["poslease_isindexed"] == 1)
		{
			$index_clause2 = "yes";
		}

		if($posleases["poslease_tacit_renewal_duration_years"] > 0)
		{
			$index_clause2 .= " / ".  $posleases["poslease_tacit_renewal_duration_years"] . "Years";

			if($posleases["poslease_tacit_renewal_duration_months"] > 0)
			{
				$index_clause2 .= ", " . $posleases["poslease_tacit_renewal_duration_months"] . "Months";
			}
		}
		elseif($posleases["poslease_tacit_renewal_duration_months"] > 0)
		{
			$index_clause2 .= " / ".  $posleases["poslease_tacit_renewal_duration_months"] . "Months";
		}

		//no calculation but entering in the rental tab
		$termination_time = $posleases["poslease_termination_time"];
	}
	else
	{
		$num_of_full_years = $cer_basicdata["cer_basicdata_lastyear"] - $cer_basicdata["cer_basicdata_firstyear"] - 1;
		$num_of_months = (13 - $cer_basicdata["cer_basicdata_firstmonth"]) + $cer_basicdata["cer_basicdata_lastmonth"] + 12 * $num_of_full_years;
		
		
		$duration_in_years = $num_of_months / 12;
		$renewal_option = "";
		$exit_option = "";
		$termination_time = "";
		$index_clause = "";
		$index_clause2 = "";

		$rental_period = '';
	}
}
else
{
	if($cer_basicdata['cer_basicdata_lease_startdate'] != NULL and $cer_basicdata['cer_basicdata_lease_startdate'] != '0000-00-00'
	   and $cer_basicdata['cer_basicdata_lease_enddate'] != NULL and $cer_basicdata['cer_basicdata_lease_enddate'] != '0000-00-00')
	{
		$duration_in_years = $cer_basicdata['cer_basicdata_lease_enddate'] - $cer_basicdata['cer_basicdata_lease_startdate'];
		$tmp1 = 13 - substr($cer_basicdata['cer_basicdata_lease_startdate'], 5,2) + substr($cer_basicdata['cer_basicdata_lease_enddate'], 5,2);
		$tmp2 = ($duration_in_years - 1)*12;
		$duration_in_years = round(($tmp1 + $tmp2) / 12, 1);

		$duration_in_years_and_months = floor(($tmp1 + $tmp2) / 12);
		$duration_in_years_and_months = $duration_in_years_and_months . "y-" . (($tmp1 + $tmp2) - ($duration_in_years_and_months*12)) . "m";
		



		$rental_period = substr($cer_basicdata['cer_basicdata_lease_startdate'], 5,2) . '-' . substr($cer_basicdata['cer_basicdata_lease_startdate'], 0,4) . '/' . substr($cer_basicdata['cer_basicdata_lease_enddate'], 5,2) . '-' . substr($cer_basicdata['cer_basicdata_lease_enddate'], 0,4) .  ' (' . $duration_in_years_and_months . ')';

		$renewal_option = "-";
		$exit_option = "-";
		

		$m = substr($cer_basicdata['cer_basicdata_lease_startdate'], 5, 2);
		$d = substr($cer_basicdata['cer_basicdata_lease_startdate'], 8, 2);
		$y = substr($cer_basicdata['cer_basicdata_lease_startdate'], 0, 4);
		$d1=mktime(0,0,0,$m,$d,$y);
		$m = substr($cer_basicdata['cer_basicdata_lease_enddate'], 5, 2);
		$d = substr($cer_basicdata['cer_basicdata_lease_enddate'], 8, 2);
		$y = substr($cer_basicdata['cer_basicdata_lease_enddate'], 0, 4);
		$d2=mktime(0,0,0,$m,$d,$y);

		if($d2-$d1 > 0)
		{
			$termination_time = floor(($d2-$d1)/2628000);
		}


		$index_clause = "-";
		
		$index_clause2 = "-";
		
		//no calculation but entering in the rental tab
		$termination_time = "-";
	}
	else
	{
		$num_of_full_years = $cer_basicdata["cer_basicdata_lastyear"] - $cer_basicdata["cer_basicdata_firstyear"] - 1;
		$num_of_months = (13 - $cer_basicdata["cer_basicdata_firstmonth"]) + $cer_basicdata["cer_basicdata_lastmonth"] + 12 * $num_of_full_years;
		
		
		$duration_in_years = $num_of_months / 12;
		$renewal_option = "";
		$exit_option = "";
		$termination_time = "";
		$index_clause = "";
		$index_clause2 = "";

		$rental_period = '';
	}
}




//recalculate the duration in years from the business plan period not from posleases
$duration_in_years = $last_year - $first_year;
$tmp1 = 13 - $first_month + $last_month;
$tmp2 = ($duration_in_years - 1)*12;
$duration_in_years = round(($tmp1 + $tmp2) / 12, 1);

$duration_in_years_and_months = floor(($tmp1 + $tmp2) / 12);
$duration_in_years_and_months = $duration_in_years_and_months . "y-" . (($tmp1 + $tmp2) - ($duration_in_years_and_months*12)) . "m";

$business_plan_period = $first_month . '-' . $first_year . '/' . $last_month . '-' . $last_year .' (' . $duration_in_years_and_months . ')';


//franchisee contract duration
if(param("pid") > 0 )
{
	if($posdata["posaddress_fagrstart"] != '0000-00-00' and $posdata["posaddress_fagrstart"] != NULL)
	{
		$contract_duration_in_years = $posdata["posaddress_fagrend"] - $posdata["posaddress_fagrstart"];
		$tmp1 = 13 - substr($posdata["posaddress_fagrstart"], 5,2) + substr($posdata["posaddress_fagrend"], 5,2);
		$tmp2 = ($contract_duration_in_years - 1)*12;
		$contract_duration_in_years = round(($tmp1 + $tmp2) / 12, 1);
	}
	else
	{
		$contract_duration_in_years = 0;
	}

	$pos_opening_date = "-";
	if($posdata["posaddress_store_openingdate"] != NULL and $posdata["posaddress_store_openingdate"] != "0000-00-00")
	{
		$pos_opening_date = to_system_date($posdata["posaddress_store_openingdate"]);
	}

	$deadline_for_property = to_system_date($cer_basicdata["cer_basicdata_deadline_property"]);
	$residual_value_former_investment = $cer_basicdata["cer_basicdata_residual_value"];
}
else
{
	$contract_duration_in_years = '-';
	$pos_opening_date = "-";
	$deadline_for_property = "-";
	$residual_value_former_investment = $cer_basicdata["cer_basicdata_residual_value"];
}

if(param("pid") > 0 )
{
	$result = calculate_forcasted_salaries(param("pid"), $years, $project["order_shop_address_country"]);
}
else
{
	$result = calculate_forcasted_salaries(param("did"), $years, $cer_basicdata["cer_basicdata_country"]);
}

//get payment_terms
$payment_terms = array();
$stock_data = array();
foreach($years as $key=>$year)
{
	$payment_terms[$year] = 0;
	$stock_data[$year] = 0;
}

if(param("pid") > 0 )
{
	$sql = "select * from cer_paymentterms " . 
		   "where cer_paymentterm_project = " . param("pid") .
		   " and cer_paymentterm_cer_version = " . $cer_version;
}
else
{
	$sql = "select * from cer_draft_paymentterms where cer_paymentterm_draft_id = " . param("did");
}

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$payment_terms[$row["cer_paymentterm_year"]] = $row["cer_paymentterm_in_months"];
}
//get stock data
if(param("pid") > 0 )
{
	$sql = "select * from cer_stocks " . 
		   "where cer_stock_project = " . param("pid") . 
		    " and cer_stock_cer_version = " . $cer_version;
}
else
{
	$sql = "select * from cer_draft_stocks where cer_stock_draft_id = " . param("did");
}
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$stock_data[$row["cer_stock_year"]] = $row["cer_stock_stock_in_months"];
}


//get expenses
$annual_rent = 0;
$material_of_products = array();
$marketing_expenses = array();
$income_taxes = array();
$interco_contribution = array();
$indirect_salaries = array();
$rents = array();
$fixedrents = array();
$turnoverbasedrents = array();
$additionalrents = array();
$other_expenses = array();
$expenses = array();
$expense_types = array();
$auxmat = array();
$sales_admin = array();
foreach($years as $key=>$year)
{
	$rents[$year] = 0;
	$marketing_expenses[$year] = 0;
	$interco_contribution[$year] = 0;
	$other_expenses[$year] = 0;
	$income_taxes[$year] = 0;
	$material_of_products[$year] = 0;
	$auxmat[$year] = 0;
	$sales_admin[$year] = 0;
	$indirect_salaries[$year] = 0;
}


$total_lease_commitment = 0;



if(param("pid") > 0 )
{
	$sql = "select * from cer_expenses " .
		   "left join cer_expense_types on cer_expense_type_id = cer_expense_type " . 
		   "where cer_expense_project = " . param("pid") .
		   " and cer_expense_cer_version = " . $cer_version .
		   " order by cer_expense_type_sortorder, cer_expense_year";
}
else
{
	$sql = "select * from cer_draft_expenses " .
		   "left join cer_expense_types on cer_expense_type_id = cer_expense_type " . 
		   "where cer_expense_draft_id = " . param("did") .
		   " order by cer_expense_type_sortorder, cer_expense_year";
}


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	
	if($row["cer_expense_type"] == 1) //Salaries
	{
		$indirect_salaries[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	}
	elseif($row["cer_expense_type"] == 2 
		   or $row["cer_expense_type"] == 3 
		   or $row["cer_expense_type"] == 16
		   or $row["cer_expense_type"] == 18
		   or $row["cer_expense_type"] == 19
		   or $row["cer_expense_type"] == 20) //rent
	{
		$total_lease_commitment = $total_lease_commitment + $row["cer_expense_amount"];
		$rents[$row["cer_expense_year"]] = $rents[$row["cer_expense_year"]] + $row["cer_expense_amount"];

		//echo $row["cer_expense_year"] . '->' . $row["cer_expense_amount"] . '->' . $rents[$row["cer_expense_year"]] . '<br />';
		
		//workaround for old projects
		$additionalrents[$row["cer_expense_year"]] = 0;
		//end workaround

		if($row["cer_expense_type"] == 2) {
			$fixedrents[$row["cer_expense_year"]] = $row["cer_expense_amount"];
		}
		elseif($row["cer_expense_type"] == 16) {
			$turnoverbasedrents[$row["cer_expense_year"]] = $row["cer_expense_amount"];
		}
		elseif($row["cer_expense_type"] == 3
			   or $row["cer_expense_type"] == 18
			   or $row["cer_expense_type"] == 19
			   or $row["cer_expense_type"] == 20) {
			$additionalrents[$row["cer_expense_year"]] = $additionalrents[$row["cer_expense_year"]] + $row["cer_expense_amount"];
		}
	}
	elseif($row["cer_expense_type"] == 4) //Aux mat.
	{
		//$total_lease_commitment = $total_lease_commitment + $row["cer_expense_amount"];
		$auxmat[$row["cer_expense_year"]] = $row["cer_expense_amount"];

	}
	elseif($row["cer_expense_type"] == 5) //Sales/admin expenses
	{
		$sales_admin[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	}
	elseif($row["cer_expense_type"] == 7) //other non capitalized fees and income taxes
	{
		$income_taxes[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	}
	elseif($row["cer_expense_type"] == 13) //Marketing Expenses
	{
		$marketing_expenses[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	}
	
	elseif($row["cer_expense_type"] == 14) //Interco Contribution
	{
		$interco_contribution[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	}
	elseif($row["cer_expense_type"] == 15) //Cost of Products Sold
	{
		$material_of_products[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	}
	else
	{
		$other_expenses[$row["cer_expense_year"]] = $other_expenses[$row["cer_expense_year"]] + $row["cer_expense_amount"];
	}
	
	if($row["cer_expense_type"] != 13 and $row["cer_expense_type"] != 14 and $row["cer_expense_type"] != 9 and $row["cer_expense_type"] != 15)
	{
		$expenses[$row["cer_expense_type_name"]][$row["cer_expense_year"]] = $row["cer_expense_amount"];
		$expense_types[$row["cer_expense_year"]][$row["cer_expense_type_name"]] = $row["cer_expense_type"];
	}
}

if($duration_in_years > 0)
{
	$annual_rent = $total_lease_commitment / $duration_in_years;
}
else
{
	$annual_rent = "-";
}

//get investment
$initial_investment_total = 0;

if(param("pid") > 0 )
{
	$sql = "select * from cer_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_project = " . param("pid") . 
		   " and cer_investment_cer_version = " . $cer_version .
		   " and posinvestment_type_intangible = 0";
}
else
{
	$sql = "select * from cer_draft_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_draft_id = " . param("did") . 
		   " and posinvestment_type_intangible = 0";
}

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$initial_investment_total = $initial_investment_total  + $row["cer_investment_amount_cer_loc"]; 
}

//calculate depreciation
$depreciation = array();
$total_depreciation = 0;
$total_depreciatable_investment = 0;
$residual_value = 0;
$depr_months_firstyear = 0;
$depr_months_lasttyear = 0;
$depr_lasttyear = 0;

foreach($years as $key=>$year)
{
	$depreciation[$year] = 0;
}


//get depreciable investment data
$debug_depreciation = array();
$total_depreciatable_investment_types = array();

if(param("pid") > 0 )
{
	$sql = "select * from cer_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_project = " . param("pid") .
		   " and cer_investment_cer_version = " . $cer_version .
		   " and posinvestment_type_intangible = 0 " . 
		   " and posinvestment_type_depreciatable = 1 " .
		   " and cer_investment_depr_years > 0";
}
else
{
	$sql = "select * from cer_draft_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_draft_id = " . param("did") . 
		   " and posinvestment_type_intangible = 0 " . 
		   " and posinvestment_type_depreciatable = 1 " .
		   " and cer_investment_depr_years > 0";
}
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$total_depreciatable_investment = $total_depreciatable_investment   + $row["cer_investment_amount_cer_loc"];
	$total_depreciatable_investment_types[$row["posinvestment_type_id"]] =  $row["cer_investment_amount_cer_loc"];
}

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{	
	if($row["cer_investment_depr_years"] > $number_of_years)
	{
		$months_count_of_all_years = $number_of_years * 12;
	}
	else
	{
		$months_count_of_all_years = ($row["cer_investment_depr_years"] * 12) + $row["cer_investment_depr_months"];
	}


	$depr_months_firstyear = 13 - $first_month;
	$depr_months_lasttyear = 12 - $depr_months_firstyear;

	if($first_year == $last_year)
	{
		$depr_months_firstyear = 12;
		$depr_months_lasttyear = 12;
	}

	

	//$number_of_full_years = ceil($months_count_of_all_years/12) - 2;
	$number_of_full_years = $row["cer_investment_depr_years"]-1;
	
	$number_of_full_years = floor($months_count_of_all_years/12);

	$depr_months_lasttyear = $months_count_of_all_years - $depr_months_firstyear - (12*$number_of_full_years);

	/*
	if($depr_months_lasttyear <= 0) {
		$number_of_full_years--;
		$depr_months_lasttyear = $months_count_of_all_years - $depr_months_firstyear - (12*$number_of_full_years);
	}
	*/


	

	//echo $number_of_full_years . " " . $months_count_of_all_years . " " . $depr_months_firstyear . " " . $depr_months_lasttyear . '<br />';
	//abc();

	
	$tmp_amount = $row["cer_investment_amount_cer_loc"];
	$debug_depreciation[$row["posinvestment_type_id"]] = array();

	if($months_count_of_all_years > 0)
	{
		$tmp = 0;
		
		//$tmp_depr_lastyear = $first_year + $number_of_years-1;

		if($number_of_full_years == 0 and $depr_months_lasttyear > 0) {
			$tmp_depr_lastyear = $first_year +  floor($row["cer_investment_depr_years"]) + 1;
		}
		else
		{
			$tmp_depr_lastyear = $first_year + $number_of_full_years;
			
			if($depr_months_lasttyear > 0) {
				$tmp_depr_lastyear = $tmp_depr_lastyear + 1;
			}
			elseif($depr_months_lasttyear <= 0) {
				$depr_months_lasttyear = 12 + $depr_months_lasttyear;
			}
		}


		for($i=$first_year;$i<=$tmp_depr_lastyear;$i++)
		{
			if($i == $first_year)
			{
				$tmp = $tmp_amount/$months_count_of_all_years * $depr_months_firstyear;
				
			}
			elseif($i == $tmp_depr_lastyear) // last year
			{

				$tmp = $depr_months_lasttyear * $tmp_amount/$months_count_of_all_years;

				//echo $number_of_full_years . " " . $months_count_of_all_years . " " . $depr_months_firstyear . " " . $depr_months_lasttyear . '<br />';
				//abc();

			}
			else
			{
				$tmp = $tmp_amount/$months_count_of_all_years * 12;
			}

			//echo $row["posinvestment_type_id"] . ' ' . $i . ' '. $tmp . ' ' . $tmp_depr_lastyear . '<br />';
			
			$debug_depreciation[$row["posinvestment_type_id"]][$i] = $tmp;
			$total_depreciatable_investment_types[$row["posinvestment_type_id"]] = $total_depreciatable_investment_types[$row["posinvestment_type_id"]] -  $tmp;
		
			$total_depreciation  = $total_depreciation + $tmp;

			//residual value
			if($i == $tmp_depr_lastyear and $debug_depreciation[$row["posinvestment_type_id"]][$i] > 0) // last year
			{
				$residual_value_investment_type = $total_depreciatable_investment_types[$row["posinvestment_type_id"]];
				$debug_depreciation[$row["posinvestment_type_id"]][$i] = $debug_depreciation[$row["posinvestment_type_id"]][$i] + $residual_value_investment_type;

				//echo $tmp_depr_lastyear . ' ' . $row["posinvestment_type_id"] . ' ' . $i . ' '. $residual_value_investment_type . '<br />';
				//abc();
			}
			

			
		}
		for($i=$tmp_depr_lastyear+1;$i<=$last_year+1;$i++)
		{
			$debug_depreciation[$row["posinvestment_type_id"]][$i] = 0;
		}
	}
}

//abc();

for($i=$first_year;$i<=$last_year;$i++)
{
	$depreciation[$i] = 0;
}

foreach($debug_depreciation as $invetsment_type=>$year_values)
{
	foreach($year_values as $key=>$amount)
	{
		if(array_key_exists($key, $depreciation))
		{
			$depreciation[$key] = $depreciation[$key] + $amount;
		}
	}
}



//residual value
$residual_value_investment = $total_depreciatable_investment - $total_depreciation;
//$depreciation[$tmp_depr_lastyear] = $residual_value_investment;


//prepayed rents (key money and goodwill)
$prepayed_rents = array();
$total_depreciatable_intangibles = 0;
$total_depreciation = 0;
$depr_months_firstyear = 0;
$depr_months_lasttyear = 0;
$depr_lasttyear = 0;

foreach($years as $key=>$year)
{
	$prepayed_rents[$year] = 0;
}

$total_depreciatable_investment_types = array();

//get depreciable intangible data
if(param("pid") > 0 )
{
	$sql = "select * from cer_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_project = " . param("pid") .
		   " and cer_investment_cer_version = " . $cer_version .
		   " and posinvestment_type_intangible = 1 " . 
		   " and posinvestment_type_depreciatable = 1 " .
		   " and cer_investment_depr_years > 0";
}
else
{
	$sql = "select * from cer_draft_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_draft_id = " . param("did") . 
		   " and posinvestment_type_intangible = 1 " . 
		   " and posinvestment_type_depreciatable = 1 " .
		   " and cer_investment_depr_years > 0";
}

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$total_depreciatable_intangibles = $total_depreciatable_intangibles   + $row["cer_investment_amount_cer_loc"];
	$total_depreciatable_investment_types[$row["posinvestment_type_id"]] =  $row["cer_investment_amount_cer_loc"];
}

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{	
	
	if($row["cer_investment_depr_years"] > $number_of_years)
	{
		$months_count_of_all_years = $number_of_years * 12;
	}
	else
	{
		$months_count_of_all_years = ($row["cer_investment_depr_years"] * 12) + $row["cer_investment_depr_months"];
	}

	$depr_months_firstyear = 13 - $first_month;
	$depr_months_lasttyear = 12 - $depr_months_firstyear;


	$number_of_full_years = $row["cer_investment_depr_years"]-1;
	
	$number_of_full_years = floor($months_count_of_all_years/12);

	$depr_months_lasttyear = $months_count_of_all_years - $depr_months_firstyear - (12*$number_of_full_years);


	
	
	$tmp_amount = $row["cer_investment_amount_cer_loc"];
	$debug_depreciation[$row["posinvestment_type_id"]] = array();

	

	if($months_count_of_all_years > 0)
	{
		$tmp = 0;
		
		//$tmp_depr_lastyear = $first_year + $number_of_years-1;

		if($number_of_full_years == 0 and $depr_months_lasttyear > 0) {
			$tmp_depr_lastyear = $first_year +  floor($row["cer_investment_depr_years"]) + 1;
		}
		else
		{
			$tmp_depr_lastyear = $first_year + $number_of_full_years;
			
			if($depr_months_lasttyear > 0) {
				$tmp_depr_lastyear = $tmp_depr_lastyear + 1;
			}
			elseif($depr_months_lasttyear <= 0) {
				$depr_months_lasttyear = 12 + $depr_months_lasttyear;
			}
		}


		for($i=$first_year;$i<=$tmp_depr_lastyear;$i++)
		{
			if($i == $first_year)
			{
				$tmp = $tmp_amount/$months_count_of_all_years * $depr_months_firstyear;
				
			}
			elseif($i == $tmp_depr_lastyear) // last year
			{

				$tmp = $depr_months_lasttyear * $tmp_amount/$months_count_of_all_years;

				//echo $number_of_full_years . " " . $months_count_of_all_years . " " . $depr_months_firstyear . " " . $depr_months_lasttyear . '<br />';
				//abc();

			}
			else
			{
				$tmp = $tmp_amount/$months_count_of_all_years * 12;
			}

			//echo $row["posinvestment_type_id"] . ' ' . $i . ' '. $tmp . ' ' . $tmp_depr_lastyear . '<br />';
			
			$debug_depreciation[$row["posinvestment_type_id"]][$i] = $tmp;
			$total_depreciatable_investment_types[$row["posinvestment_type_id"]] = $total_depreciatable_investment_types[$row["posinvestment_type_id"]] -  $tmp;
		
			$total_depreciation  = $total_depreciation + $tmp;

			//residual value
			if($i == $tmp_depr_lastyear and $debug_depreciation[$row["posinvestment_type_id"]][$i] > 0) // last year
			{
				$residual_value_investment_type = $total_depreciatable_investment_types[$row["posinvestment_type_id"]];
				$debug_depreciation[$row["posinvestment_type_id"]][$i] = $debug_depreciation[$row["posinvestment_type_id"]][$i] + $residual_value_investment_type;

				//echo $tmp_depr_lastyear . ' ' . $row["posinvestment_type_id"] . ' ' . $i . ' '. $residual_value_investment_type . '<br />';
				//abc();
			}
			

			
		}
		for($i=$tmp_depr_lastyear+1;$i<=$last_year+1;$i++)
		{
			$debug_depreciation[$row["posinvestment_type_id"]][$i] = 0;
		}
	}
	
	/*
	if($months_count_of_all_years > 0)
	{
		$tmp = 0;
		
		//$tmp_depr_lastyear = $first_year + $number_of_years-1;

		if($number_of_full_years == 0 and $depr_months_lasttyear > 0) {
			$tmp_depr_lastyear = $first_year +  floor($row["cer_investment_depr_years"]) + 1;
		}
		else
		{
			$tmp_depr_lastyear = $first_year + $number_of_full_years;
			
			if($depr_months_lasttyear > 0) {
				$tmp_depr_lastyear = $tmp_depr_lastyear + 1;
			}
			elseif($depr_months_lasttyear < 0) {
				$depr_months_lasttyear = 12 + $depr_months_lasttyear;
			}
		}


		for($i=$first_year;$i<=$tmp_depr_lastyear;$i++)
		{
			if($i == $first_year)
			{
				$tmp = $tmp_amount/$months_count_of_all_years * $depr_months_firstyear;
				
			}
			elseif($i == $tmp_depr_lastyear) // last year
			{

				$tmp = $depr_months_lasttyear * $tmp_amount/$months_count_of_all_years;

				//echo $number_of_full_years . " " . $months_count_of_all_years . " " . $depr_months_firstyear . " " . $depr_months_lasttyear . '<br />';
				//abc();

			}
			else
			{
				$tmp = $tmp_amount/$months_count_of_all_years * 12;
			}

			//echo $row["posinvestment_type_id"] . ' ' . $i . ' '. $tmp . ' ' . $tmp_depr_lastyear . '<br />';
			
			$debug_depreciation[$row["posinvestment_type_id"]][$i] = $tmp;
			$total_depreciatable_investment_types[$row["posinvestment_type_id"]] = $total_depreciatable_investment_types[$row["posinvestment_type_id"]] -  $tmp;
		
			$total_depreciation  = $total_depreciation + $tmp;

			//residual value
			if($i == $tmp_depr_lastyear and $debug_depreciation[$row["posinvestment_type_id"]][$i] > 0) // last year
			{
				$residual_value_investment_type = $total_depreciatable_investment_types[$row["posinvestment_type_id"]];
				$debug_depreciation[$row["posinvestment_type_id"]][$i] = $debug_depreciation[$row["posinvestment_type_id"]][$i] + $residual_value_investment_type;

				//echo $tmp_depr_lastyear . ' ' . $row["posinvestment_type_id"] . ' ' . $i . ' '. $residual_value_investment_type . '<br />';
			}
			

			
		}
		for($i=$tmp_depr_lastyear+1;$i<=$last_year+1;$i++)
		{
			$debug_depreciation[$row["posinvestment_type_id"]][$i] = 0;
		}
	}
	*/
}

for($i=$first_year;$i<=$last_year;$i++)
{
	$prepayed_rents[$i] = 0;
}

foreach($debug_depreciation as $invetsment_type=>$year_values)
{
	if($invetsment_type == 15 or $invetsment_type == 17) // key money and goodwill
	{
		foreach($year_values as $key=>$amount)
		{
			if(array_key_exists($key, $prepayed_rents))
			{
				$prepayed_rents[$key] = $prepayed_rents[$key] + $amount;
			}
		}
	}
}


//residual value
$residual_value_intangbiles = $total_depreciatable_intangibles - $total_depreciation;
//$prepayed_rents[$tmp_depr_lastyear] = $residual_value_intangbiles;

//cash flow initial investment
$initial_investment_total_for_cash_flow = $initial_investment_total + $intagibles_total;


//get other information
$head_counts = "-";  
$ftes = "-";
if(param("pid") > 0 )
{
	$sql = "select count(cer_salary_id) as head_counts, sum(cer_salary_headcount_percent) as ftes " .
		   "from cer_salaries " . 
		   "where cer_salary_project = " . param("pid") . 
		   " and cer_salary_cer_version = " . $cer_version;
}
else
{
	$sql = "select count(cer_salary_id) as head_counts, sum(cer_salary_headcount_percent) as ftes " .
		   "from cer_draft_salaries where cer_salary_draft_id = " . param("did");
}

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$head_counts = $row["head_counts"];
	$ftes = $row["ftes"]/100;
}


$payback_period_start = 0.5;
$pay_back_period_retail = 0;
$pay_back_period_retail_values = array();
$pay_back_cummunlated_retail_values = array();
$pay_back_period_wholesale = 0;
$pay_back_period_wholesale_values = array();
$pay_back_cummunlated_wholesale_values = array();
$discount_rate = $cer_basicdata["cer_basicdata_dicount_rate"] / 100;

foreach($years as $key=>$year)
{
	$sales_units_watches_values[$year] = "??";
    $sales_units_watches_shares[$year] = "";

    $sales_units_jewellery_values[$year] = "??";
    $sales_units_jewellery_shares[$year] = "";

    $average_price_watches_values[$year] = "??";
    $average_price_watches_shares[$year] = "";

    $average_price_jewellery_values[$year] = "??";
    $average_price_jewellery_shares[$year] = "";

    $sales_watches_values[$year] = "??";
    $sales_watches_shares[$year] = "??";

    $sales_jewellery_values[$year] = "??";
    $sales_jewellery_shares[$year] = "??";

	$sales_customer_service_values[$year] = "??";
    $sales_customer_service_shares[$year] = "";

    $total_gross_sales_values[$year] = "??";
    $total_gross_sales_shares[$year] = "??";

	$sales_reduction_values[$year] = "??";
    $sales_reduction_shares[$year] = "??";

	$total_net_sales_values[$year] = "??";
    $total_net_sales_shares[$year] = "??";

	$material_of_products_values[$year] = "??";
    $material_of_products_shares[$year] = "??";

	$total_gross_margin_values[$year] = "??";
    $total_goss_margin_shares[$year] = "??";

	$marketing_expenses_values[$year] = "??";
    $marketing_expenses_shares[$year] = "??";

	$interco_contribution_values[$year] = "??";
    $interco_contribution_shares[$year] = "??";

	$indirect_salaries_values[$year] = "??";
    $indirect_salaries_shares[$year] = "??";

    $rents_values[$year] = "??";
    $rents_shares[$year] = "??";

	$fixedrents_values = "??";
	$turnoverbasedrents_values = "??";
	$additionalrents_values = "??";


	$rents_total_values [$year] = "??";

	$income_taxes_values[$year] = "??";
    $income_taxes_shares[$year] = "??";

	$auxmat_values[$year] = "??";
    $auxmat_shares[$year] = "??";

	$sales_admin_values[$year] = "??";
	$sales_admin_shares[$year] = "??";

	$depreciation_values[$year] = "??";
    $depreciation_shares[$year] = "??";

	$prepayed_rent_values[$year] = "??";
    $prepayed_rent_shares[$year] = "??";

	$other_expenses_values[$year] = "??";
    $other_expenses_shares[$year] = "??";

	$total_indirect_expenses_values[$year] = "??";
    $total_indirect_expenses_shares[$year] = "??";

	$operating_income01_values[$year] = "??";
    $operating_income01_shares[$year] = "??";

	$operating_income02_values[$year] = "??";
    $operating_income02_shares[$year] = "??";

	$break_even_retail_margin[$year] = "??";
	$break_even_wholesale_margin[$year] = "??";

	$stat_wholesale_margin[$year] = "??";

	$total_cash_flow_values[$year] = "??";
	$total_cash_flow_fcf_values[$year] = 0;
	$cash_flow_values[$year] = "??";

	$cash_flow_whole_sale_values[$year] = "??";

	$pay_back_cummunlated_retail_values[$year] = 0;
	$pay_back_period_retail_values[$year] = 0;
	
	$pay_back_cummunlated_wholesale_values[$year] = 0;
	$pay_back_period_wholesale_values[$year] = 0;


	$cer_revenue_customer_frequency[$year] = "??";
	$cer_revenue_total_days_open_per_year[$year] = "??";
	$cer_revenue_total_hours_open_per_week[$year] = "??";
	$cer_revenue_total_workinghours_per_week[$year] = "??";
}



//get sales planning
if(param("pid") > 0 )
{
	$sql = "select * from cer_revenues " .
		   "where cer_revenue_project = " . param("pid") . 
		   " and cer_revenue_cer_version = " . $cer_version;
}
else
{
	$sql = "select * from cer_draft_revenues " .
		   "where cer_revenue_draft_id = " . param("did");
}

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$year = $row["cer_revenue_year"];
	$sales_units_watches_values[$year] = $row["cer_revenue_quantity_watches"];
	$sales_units_jewellery_values[$year] = $row["cer_revenue_quantity_jewellery"];

	$average_price_watches_values[$year] =$row["cer_revenue_aveargeprice_watches"];
	$average_price_jewellery_values[$year] =  $row["cer_revenue_aveargeprice_jewellery"];

	$sales_watches_values[$year] = $row["cer_revenue_watches"];
	$sales_jewellery_values[$year] = $row["cer_revenue_jewellery"];
	$sales_customer_service_values[$year] = $row["cer_revenue_customer_service"];


	$total_gross_sales_values[$year] = $row["cer_revenue_watches"] + $row["cer_revenue_jewellery"] + $row["cer_revenue_customer_service"];


	$cer_revenue_customer_frequency[$year] = $row["cer_revenue_customer_frequency"];
	$cer_revenue_total_days_open_per_year[$year] = $row["cer_revenue_total_days_open_per_year"];
	$cer_revenue_total_hours_open_per_week[$year] = $row["cer_revenue_total_hours_open_per_week"];
	$cer_revenue_total_workinghours_per_week[$year] = $row["cer_revenue_total_workinghours_per_week"];

}

//calculate sum of rents
$sum_of_rents_payed = 0;
$sum_of_fixedrents_payed = 0;
$sum_of_turnoverbasedrents_payed = 0;
$sum_of_additionalrents_payed = 0;
$rental_num_of_months = array();
$sum_of_rental_months = 0;

$fixedrents_values = array();
$turnoverbasedrents_values = array();
$additionalrents_values = array();

foreach($sales_units_watches_values as $year=>$value)
{
	$sum_of_rents_payed = $sum_of_rents_payed + $rents[$year];

	$sum_of_fixedrents_payed = $sum_of_fixedrents_payed + $fixedrents[$year];
	if(array_key_exists($year, $turnoverbasedrents)) {
		$sum_of_turnoverbasedrents_payed = $sum_of_turnoverbasedrents_payed + $turnoverbasedrents[$year];
	}

	if(array_key_exists($year, $additionalrents)) {
		$sum_of_additionalrents_payed = $sum_of_additionalrents_payed + $additionalrents[$year];
	}

	if($cer_basicdata["cer_basicdata_firstyear"] == $year) {
		$rental_num_of_months[$year] = 13 - $cer_basicdata["cer_basicdata_firstmonth"];
	}
	elseif($cer_basicdata["cer_basicdata_lastyear"] == $year) {
		$rental_num_of_months[$year] = $cer_basicdata["cer_basicdata_lastmonth"];
	}
	else
	{
		$rental_num_of_months[$year] = 12;
	}

	$sum_of_rental_months = $sum_of_rental_months + $rental_num_of_months[$year];
}

//prepare ouput values

foreach($sales_units_watches_values as $year=>$value)
{
	//based on gross sales values, calculated forward to net sales valus
	//$tmp5 = $total_gross_sales_values[$year] * $cer_basicdata["cer_basicdata_sales_reduction"] / 100;
	//$sales_reduction_values[$year] = $tmp5;
	//$total_gross_sales_values[$year] = $total_gross_sales_values[$year];
	//$total_net_sales_values[$year] = $total_gross_sales_values[$year] - $sales_reduction_values[$year];

	//based on net sales values, calculated backwards up to gross sales
	$total_gross_sales_values[$year] = $total_gross_sales_values[$year];
	//$total_net_sales_values[$year] = ($total_gross_sales_values[$year] / (100+$cer_basicdata["cer_basicdata_sales_reduction"])) * 100;

	if($calc_model == 2008)
	{

		$total_net_sales_values[$year] = ($total_gross_sales_values[$year] / (100+$cer_basicdata["cer_basicdata_sales_reduction"])) * 100;
		$sales_reduction_values[$year] =  $total_net_sales_values[$year] * $cer_basicdata["cer_basicdata_sales_reduction"] / 100;
	}
	else
	{
		$sales_reduction_values[$year] =  $total_gross_sales_values[$year] * $cer_basicdata["cer_basicdata_sales_reduction"] / 100;
		$total_net_sales_values[$year] = $total_gross_sales_values[$year] - $sales_reduction_values[$year];
	}

	
	$material_of_products_values[$year] = $material_of_products[$year];


	$total_gross_margin_values[$year] = $total_gross_sales_values[$year] - $sales_reduction_values[$year] - $material_of_products_values[$year];

	$marketing_expenses_values[$year] = $marketing_expenses[$year];
	$interco_contribution_values[$year] = $interco_contribution[$year];

	
	$indirect_salaries_values[$year] = $indirect_salaries[$year];

	
	//calcualte rents depending on the average parameter value
	if($cer_basicdata["cer_basicdata_calculate_rent_avg"] == 1) // average rent per year
	{
		if($sum_of_rental_months == 0)
		{
			$rents_values[$year] = 0;
			$fixedrents_values[$year] = 0;
			$turnoverbasedrents_values[$year] = 0;
			$additionalrents_values[$year] = 0;
		}
		else
		{
			$rents_values[$year] = ($rental_num_of_months[$year] * $sum_of_rents_payed) / $sum_of_rental_months;
			
			$fixedrents_values[$year] = (($rental_num_of_months[$year] * $sum_of_fixedrents_payed) / $sum_of_rental_months);
			
			$turnoverbasedrents_values[$year] = ($rental_num_of_months[$year] * $sum_of_turnoverbasedrents_payed) / $sum_of_rental_months;

			$additionalrents_values[$year] = ($rental_num_of_months[$year] * $sum_of_additionalrents_payed) / $sum_of_rental_months;

			//echo (($rental_num_of_months[$year] * $sum_of_fixedrents_payed) / $sum_of_rental_months) . '<br />';
			//echo $year . "->" . $rental_num_of_months[$year] . "->" .  $sum_of_fixedrents_payed . "->" . $sum_of_rental_months . "->" . $fixedrents_values[$year] . "<br />";
		}
	}
	else
	{
		$rents_values[$year] = $rents[$year];
		$fixedrents_values[$year] = $fixedrents[$year];
		
		if(array_key_exists($year, $turnoverbasedrents)) {
			$turnoverbasedrents_values[$year] = $turnoverbasedrents[$year];
		}
		else
		{
			$turnoverbasedrents_values[$year] = 0;
		}
		
		if(array_key_exists($year, $additionalrents)) {
			$additionalrents_values[$year] = $additionalrents[$year];
		}
		else
		{
			$additionalrents_values[$year] = 0;
		}
	}

		
	$total_indirect_expenses_values[$year] = $indirect_salaries[$year] + $rents_values[$year] + $prepayed_rents[$year] + $auxmat[$year] + $depreciation[$year] + $sales_admin[$year] + $other_expenses[$year] + $income_taxes[$year];
	
	
	//$rents_total_values[$year] = $rents_values[$year] + $prepayed_rents[$year];
	$rents_total_values[$year] = $rents_values[$year];

	$income_taxes_values[$year] = $income_taxes[$year];

	$auxmat_values[$year] = $auxmat[$year];

	$depreciation_values[$year] = $depreciation[$year];

	$sales_admin_values[$year] = $sales_admin[$year];

	$other_expenses_values[$year] = $other_expenses[$year] + $income_taxes_values[$year];

	$tmp9 =  $total_gross_margin_values[$year] - $marketing_expenses_values[$year] + $interco_contribution_values[$year];
	$tmp10 = $tmp9 - $total_indirect_expenses_values[$year];
	$operating_income01_values[$year] = $tmp10;


	$tmp11 = $material_of_products_values[$year] * $cer_basicdata["cer_basicdata_wholesale_margin"] / 100; // Stat. Wholesale Margin
	$tmp12 = $total_gross_margin_values[$year] - $marketing_expenses_values[$year] + $interco_contribution_values[$year] - $total_indirect_expenses_values[$year]; // operating income
	
	$operating_income02_values[$year] = $tmp11 + $tmp12;

	$tmp13 = $total_indirect_expenses_values[$year] + $marketing_expenses_values[$year] - $interco_contribution_values[$year];
	$tmp14 = $sales_units_watches_values[$year] + $sales_units_jewellery_values[$year];
	
	if($tmp14 > 0)
	{
		$tmp15 = $total_gross_margin_values[$year] / $tmp14;
		$tmp16 = ($total_gross_margin_values[$year] + $tmp11) / $tmp14;

		if($tmp15 > 0 and $tmp16 > 0)
		{
			$break_even_retail_margin[$year] =  $tmp13/$tmp15;
			$break_even_wholesale_margin[$year]  =  $tmp13/$tmp16;
		}
	}

	$stat_wholesale_margin[$year] = $material_of_products_values[$year] * $cer_basicdata["cer_basicdata_wholesale_margin"] / 100;
	

	//cash flow
	if($year == $first_year)
	{
		//initial investment
		$_initial_investment = $initial_investment_total_for_cash_flow;
		$debug_initial_investment[$first_year] = $_initial_investment;

		//Variation Working Capital + Replacement Investment
		$_liabilities = $material_of_products_values[$first_year]/12 * $payment_terms[$first_year];
		$_stock = $material_of_products_values[$first_year]/12 * $stock_data[$first_year];
		$_receivables = 0; //not to take in consideration
		$_replacement_fixed_assets = 0; //not to take in consideration

		$_var_working_capital = $_liabilities + $_stock + $_receivables + $_replacement_fixed_assets;
		$debugvar_working_capital[$first_year] = $_var_working_capital;
		$debugvar_liabilities[$first_year] = $_liabilities;
		$debugvar_liabilities_change[$first_year] = $_liabilities;
		$debugvar_stock[$first_year] = $_stock;
		$debugvar_stock_change[$first_year] = $_stock;

		
		//total cash flow
		$_total_cash_flow = $operating_income01_values[$first_year] + $prepayed_rents[$first_year] + $depreciation_values[$first_year] - $_var_working_capital;
		$total_cash_flow_values[$first_year] = $_total_cash_flow;

		//income taxes
		$_income_taxes = $income_taxes_values[$first_year];

		//finacncial expenses
		$_interest_rate = $cer_basicdata_interest_rates[$first_year];
		$_finacial_expenses = $_initial_investment * $_interest_rate / 100;
		$total_cash_flow_fcf_values[$first_year] = -1*$_initial_investment + $total_cash_flow_values[$first_year] + $_finacial_expenses - $_income_taxes;

		$debugvar_finacial_expenses[$first_year] = $_finacial_expenses;

		//cash flow
		$cash_flow_values[$first_year] = $_total_cash_flow - $_initial_investment - $_finacial_expenses - $_income_taxes;
		$cash_flow_whole_sale_values[$first_year] = $_total_cash_flow  + $stat_wholesale_margin[$first_year];
		$cfs2[] = $cash_flow_whole_sale_values[$first_year];

		//pay back retail
		$tmp = (1+$discount_rate);
		$tmp = pow($tmp ,$payback_period_start);
		$tmp = $_total_cash_flow / $tmp;
		$tmp = -1*$_initial_investment + $tmp;
		$pay_back_cummunlated_retail_values[$first_year] = $tmp;

		if($pay_back_cummunlated_retail_values[$first_year] < 0)
		{
			$pay_back_period_retail_values[$first_year] = 0;
		}
		else
		{
			$pay_back_cummunlated_retail_values[$first_year] = $tmp;
			if($tmp != 0)
			{
				$pay_back_period_retail_values[$first_year] = 0 / ($tmp - 0) +  $payback_period_start;
			}
			else
			{
				$pay_back_period_retail_values[$first_year] = 0;
			}
		}

		if($pay_back_period_retail <= 0)
		{
			$pay_back_period_retail = $pay_back_period_retail_values[$first_year];
		}


		//pay back whole sale
		$tmp = (1+$discount_rate);
		$tmp = pow($tmp ,$payback_period_start);
		$tmp = $cash_flow_whole_sale_values[$year] / $tmp;
		$tmp = -1*$_initial_investment + $tmp;
		$pay_back_cummunlated_wholesale_values[$first_year] = $tmp;
		
		if($pay_back_cummunlated_wholesale_values[$first_year] < 0)
		{
			$pay_back_period_wholesale_values[$first_year] = 0;
		}
		else
		{
			if($tmp != 0)
			{
				$pay_back_period_wholesale_values[$first_year] = 0 / ($tmp - 0) +  $payback_period_start;
			}
			else
			{
				$pay_back_period_wholesale_values[$first_year] = 0;
			}
		}

		if($pay_back_period_wholesale <= 0)
		{
			$pay_back_period_wholesale = $pay_back_period_wholesale_values[$first_year];
		}
	}
	elseif(array_key_exists($year-1, $material_of_products_values))
	{
		//initial investment
		$_initial_investment = $initial_investment_total_for_cash_flow;
		

		//Variation Working Capital + Replacement Investment
		$_liabilities = $material_of_products_values[$year]/12 * $payment_terms[$year];
		$debugvar_liabilities[$year] = $_liabilities;
		$_liabilities = $_liabilities - ($material_of_products_values[$year-1]/12 * $payment_terms[$year]);
		$_stock = $material_of_products_values[$year]/12 * $stock_data[$year];
		$debugvar_stock[$year] = $_stock;
		$_stock = $_stock - ($material_of_products_values[$year-1]/12 * $stock_data[$year-1]);
		$_receivables = 0; //not to take in consideration
		$_replacement_fixed_assets = 0; //not to take in consideration
		
		$_var_working_capital = $_liabilities + $_stock + $_receivables + $_replacement_fixed_assets;
		$debugvar_working_capital[$year] = $_var_working_capital;
		$debugvar_liabilities_change[$year] = $_liabilities;
		
		$debugvar_stock_change[$year] = $_stock;

		//caculate liquidation revenue
		$liquidation_revenue = 0;

		if($year == $cer_basicdata["cer_basicdata_lastyear"])
		{
			
			$intagibles_total_revenue = $intagibles_total * $cer_basicdata["cer_bascidata_liquidation_keymoney"] / 100;
			$intagibles_total_revenue = $intagibles_total_revenue;
			$stock_revenue = ($material_of_products_values[$year]/12 * $stock_data[$year]) * $cer_basicdata["cer_bascidata_liquidation_stock"] / 100;
			
			$salary_cost = $indirect_salaries_values[$year] * $cer_basicdata["cer_bascicdate_liquidation_staff"] / 100;

			$deposit_revenue = $deposit * $cer_basicdata["cer_basicdata_liquidation_deposit"] / 100;
			$deposit_revenue = $deposit_revenue;

			$liquidation_revenue = $residual_value_investment + $intagibles_total_revenue + $stock_revenue - $_liabilities + $salary_cost + $_receivables + $deposit_revenue;
			
		}
		$debug_initial_investment[$year] = $liquidation_revenue;

		//total cash flow
		$_total_cash_flow = $operating_income01_values[$year] + $prepayed_rents[$year] + $depreciation_values[$year] - $_var_working_capital + $liquidation_revenue;
		$total_cash_flow_values[$year] = $_total_cash_flow;

		

		//income taxes
		$_income_taxes = $income_taxes_values[$year];
		
		//finacncial expenses
		
		$_interest_rate = $cer_basicdata_interest_rates[$year];
		$_finacial_expenses = $total_cash_flow_fcf_values[$year-1] * $_interest_rate / 100;

				
		if($year == $last_year)
		{
			
			$_finacial_expenses = $_finacial_expenses /12 * $last_month;
		}
		$debugvar_finacial_expenses[$year] = $_finacial_expenses;
		
		$total_cash_flow_fcf_values[$year] = $total_cash_flow_fcf_values[$year-1] + $total_cash_flow_values[$year] + $_finacial_expenses - $_income_taxes;

						
		//cash flow
	
		$cash_flow_values[$year] = $total_cash_flow_fcf_values[$year] - $total_cash_flow_fcf_values[$year-1];

		$cfs[] = $cash_flow_values[$year];
		$cash_flow_whole_sale_values[$year] = $_total_cash_flow + $stat_wholesale_margin[$year];
		$cfs2[] = $cash_flow_whole_sale_values[$year];


		//pay back retail
		if(($payback_period_start + 1) < ($last_year - $first_year + 1))
		{
			$payback_period_start = $payback_period_start + 1;
		}
		else
		{
			$payback_period_start = 0;
		}

		$tmp = (1+$discount_rate);
		$tmp = pow($tmp ,$payback_period_start);
		$tmp = $_total_cash_flow / $tmp;
		$tmp =  $pay_back_cummunlated_retail_values[$year-1] + $tmp;
		$pay_back_cummunlated_retail_values[$year] = $tmp;
		
		if($pay_back_cummunlated_retail_values[$year] < 0)
		{
			$pay_back_period_retail_values[$year] = 0;
		}
		elseif($pay_back_period_retail <=0)
		{
			$tmp = $pay_back_cummunlated_retail_values[$year] - $pay_back_cummunlated_retail_values[$year-1];
			if($tmp != 0)
			{
				$pay_back_period_retail_values[$year] = $pay_back_cummunlated_retail_values[$year-1]/$tmp + $payback_period_start;
			}
			else
			{
				$pay_back_period_retail_values[$year] = 0;
			}
		}
		else
		{
			$pay_back_period_retail_values[$year] = 0;
		}

		
		if($pay_back_period_retail <= 0)
		{
			$pay_back_period_retail = $pay_back_period_retail_values[$year];
		}
		
		//pay back wholesale
		$tmp = (1+$discount_rate);
		$tmp = pow($tmp ,$payback_period_start);
		$tmp = $cash_flow_whole_sale_values[$year] / $tmp;
		$tmp = $pay_back_cummunlated_wholesale_values[$year-1] + $tmp;
		$pay_back_cummunlated_wholesale_values[$year] = $tmp;
		$pay_back_period_wholesale_values[$year] = $tmp;


		if($pay_back_cummunlated_wholesale_values[$year] < 0)
		{
			$pay_back_period_wholesale_values[$year] = 0;
		}
		elseif($pay_back_period_wholesale <= 0)
		{
			$tmp = $pay_back_cummunlated_wholesale_values[$year] - $pay_back_cummunlated_wholesale_values[$year-1];
			
			if($tmp != 0)
			{
				$pay_back_period_wholesale_values[$year] = $pay_back_cummunlated_wholesale_values[$year-1]/$tmp + $payback_period_start;
			}
			else
			{
				$pay_back_period_wholesale_values[$year] = 0;
			}
		}
		else
		{
			$pay_back_period_wholesale_values[$year] = 0;
		}
		
		if($pay_back_period_wholesale_values[$first_year] == 0)
		{
			if($pay_back_period_wholesale <= 0)
			{
				$pay_back_period_wholesale = $pay_back_period_wholesale_values[$year];
			}
		}
		else
		{
			$pay_back_period_wholesale = 0;
		}
	}
}

//calculate percentage shares
foreach($years as $key=>$year)
{
    if($total_net_sales_values[$year] != 0 and $total_gross_sales_values[$year] != 0)
	{
		/*
		$tmp_sales_total = $sales_watches_values[$year] + $sales_jewellery_values[$year] + $sales_customer_service_values[$year];
		$percent_base = 100 + $cer_basicdata["cer_basicdata_sales_reduction"];
		$base_unit = $tmp_sales_total / $percent_base;

		if($base_unit > 0)
		{
			$sales_watches_shares[$year] = $sales_watches_values[$year] / $base_unit;
		    $sales_jewellery_shares[$year] = $sales_jewellery_values[$year] / $base_unit;
			$sales_customer_service_shares[$year] = $sales_customer_service_values[$year] / $base_unit;
		}

		$total_gross_sales_shares[$year] = $sales_watches_shares[$year] + $sales_jewellery_shares[$year] + $sales_customer_service_shares[$year];
		*/

		$sales_watches_shares[$year] = 100*$sales_watches_values[$year] / $total_net_sales_values[$year];
	    $sales_jewellery_shares[$year] = 100*$sales_jewellery_values[$year]  / $total_net_sales_values[$year];
	    $sales_customer_service_shares[$year] = 100*$sales_customer_service_values[$year]  / $total_net_sales_values[$year];
		$total_gross_sales_shares[$year] = 100*$total_gross_sales_values[$year]  / $total_net_sales_values[$year];;
		
		
		//$sales_reduction_shares[$year] = 100*$sales_reduction_values[$year] / $total_gross_sales_values[$year];
		$sales_reduction_shares[$year] = 100*$sales_reduction_values[$year] / $total_net_sales_values[$year];
		
		//$total_net_sales_shares[$year] = 100*$total_gross_sales_values[$year] / $total_net_sales_values[$year];
		$total_net_sales_shares[$year] = 100;

		$material_of_products_shares[$year] = 100*$material_of_products_values[$year] / $total_net_sales_values[$year];


		$total_goss_margin_shares[$year] = 100*$total_gross_margin_values[$year] / $total_net_sales_values[$year];

		$tmp = $interco_contribution_values[$year] - $marketing_expenses_values[$year];
		$marketing_expenses_shares[$year] = 100*$tmp / $total_net_sales_values[$year];


		$indirect_salaries_shares[$year] = 100*$indirect_salaries_values[$year] / $total_net_sales_values[$year];
		$rents_shares[$year] = 100*$rents_total_values[$year] / $total_net_sales_values[$year];
		$income_taxes_shares[$year] = 100*$income_taxes_values[$year] / $total_net_sales_values[$year];
		$auxmat_shares[$year] = 100*$auxmat_values[$year] / $total_net_sales_values[$year];
		$sales_admin_shares[$year] = 100*$sales_admin_values[$year] / $total_net_sales_values[$year];
		$depreciation_shares[$year] = 100*$depreciation_values[$year] / $total_net_sales_values[$year];

		$prepayed_rent_values[$year] = $prepayed_rents[$year];
		$prepayed_rent_shares[$year] = 100*$prepayed_rent_values[$year] / $total_net_sales_values[$year];

		$other_expenses_shares[$year] = 100*$other_expenses_values[$year] / $total_net_sales_values[$year];
		$total_indirect_expenses_shares[$year] = 100*$total_indirect_expenses_values[$year] / $total_net_sales_values[$year];

		$operating_income01_shares[$year] = 100*$operating_income01_values[$year] / $total_net_sales_values[$year];;
		$operating_income02_shares[$year] = 100*$operating_income02_values[$year] / $total_net_sales_values[$year];;

	}
}

//add aux_mat_to_rents, Mail Sébastien 31.3.09, add aux mat to rents
/*
foreach($rents_total_values as $year=>$amount)
{
	if(array_key_exists($year, $auxmat_values))
	{
		$rents_total_values[$year] = $rents_total_values[$year] + $auxmat_values[$year];
		$rents_shares[$year] = 100*$rents_total_values[$year] / $total_net_sales_values[$year];

	}
}
*/

// net present value and internal rate of interest retail
$cfs = array();
foreach($total_cash_flow_values as $key=>$value)
{
	$cfs[] = $value;
}

$fin = new Financial;
$net_present_value_retail = $fin->NPV($discount_rate , $cfs);
$net_present_value_retail = $net_present_value_retail - $_initial_investment;

$cfs1 = array();
$cfs1[] = -1*$_initial_investment;

foreach($total_cash_flow_values as $key=>$value)
{
	$cfs1[] = $value;
}



$discounted_cash_flow_retail = 100*$fin->IRR($cfs1, -0.1);

if(!$discounted_cash_flow_retail)
{
	$discounted_cash_flow_retail = "0.00%";
}
else
{
	$discounted_cash_flow_retail = $discounted_cash_flow_retail . "%";
}



// net present value and internal rate of interest wholesale
$net_present_value_wholesale = $fin->NPV($discount_rate , $cfs2);
$net_present_value_wholesale = $net_present_value_wholesale -$_initial_investment;

$cfs3= array();
$cfs3[] = -$_initial_investment;

foreach($cfs2  as $key=>$value)
{
	$cfs3[] = $value;
}

$discounted_cash_flow_wholesale = 100*$fin->IRR($cfs3, -0.1);

if(!$discounted_cash_flow_wholesale)
{
	$discounted_cash_flow_wholesale = "0.00%";
}
else
{
	$discounted_cash_flow_wholesale = $discounted_cash_flow_wholesale . "%";
}



?>