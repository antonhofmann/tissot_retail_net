<?php
/********************************************************************

    project_page_actions.php

    Defines the possible page actions depending on access rights

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-25
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-27
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

$page->register_action('project', 'Overview/Submission', "cer_project.php?pid=" . param("pid"));

if($posorder_present == 1)
{
	//corporate, joint venture, cooperation 3rd party
	if($form_type == "CER")
	{
		
		
		if($project["project_projectkind"] == 8) // popup
		{
			$page->register_action('cer', 'CER for PopUps', "cer_application_general.php?pid=" . param("pid"));
		}
		else
		{
			$page->register_action('cer', 'Business Plan', "cer_application_general.php?pid=" . param("pid"));
		}
		
		if($ln_basicdata['ln_no_ln_submission_needed'] != 1
			and $project["project_projectkind"] !=8)
		{
			$page->register_action('ln', 'LNR', "ln_general.php?pid=" . param("pid"));
						
		}
		

		if($project["project_projectkind"] != 8) // popup
		{
			$page->register_action('cer', 'CER', "cer_general.php?pid=" . param("pid"));
		}
		
		if($project["project_projectkind"] !=8)
		{
			
			if($ln_basicdata["ln_no_ln_submission_needed"] == 0)
			{
				$page->register_action('ln_cer', 'CER versus LNR', "cer_versus_ln.php?pid=" . param("pid"));
			}
						
			$page->register_action('refund', 'Request for Additional Funding', "cer_additional_funding.php?pid=" . param("pid"));
		}
	}
	elseif($form_type == "INR03")
	{
		$page->register_action('cer', 'INR-03 Form', "ln_general.php?pid=" . param("pid"));
		$page->register_action('cer', 'Business Plan', "cer_application_general.php?pid=" . param("pid"));
		$page->register_action('refund', 'Request for Additional Funding', "cer_additional_funding.php?pid=" . param("pid"));
	}
	else
	{
		if($project["project_projectkind"] == 8) // popup
		{
			$page->register_action('cer', 'CER for PopUps', "cer_application_general.php?pid=" . param("pid"));
		}
		else
		{
			$page->register_action('cer', 'Business Plan', "cer_application_general.php?pid=" . param("pid"));
		}
		
		if($project["project_projectkind"] == 8) // popup
		{
			$page->register_action('cer', 'LNR for PopUps', "cer_application_general.php?pid=" . param("pid"));
		}
		else
		{
			$page->register_action('cer', 'LNR/INR03', "ln_general.php?pid=" . param("pid"));
		}

		if($project["project_postype"] == 1
			and $project["project_projectkind"] != 8) // popup
		{
			$page->register_action('cer', 'CER', "cer_general.php?pid=" . param("pid"));

			if($project["project_projectkind"] !=8)
			{
				
				if($ln_basicdata["ln_no_ln_submission_needed"] == 0)
				{
					$page->register_action('ln_cer', 'CER versus LNR', "cer_versus_ln.php?pid=" . param("pid"));
				}
							
				$page->register_action('refund', 'Request for Additional Funding', "cer_additional_funding.php?pid=" . param("pid"));
			}
		}
	}


	if(has_access("has_full_access_to_cer"))
	{
		$page->register_action('preapprove', 'Pre-Approval of Investments', "project_investment_pre_approval.php?pid=" . param("pid"));
		$page->register_action('edit_cer', 'Edit SG Approvals', "project_edit_approved_amounts_data.php?pid=" . param("pid"));
		$page->register_action('edit_names', 'Edit Approval Names', "cer_edit_approval_names.php?pid=" . param("pid"));
	}
	

	
	
	$page->register_action('cermail', 'Mailbox', "cer_mails.php?pid=" . param("pid") . "&id=" . param("pid"));
	
	$page->register_action('print', 'Print', "cer_print.php?pid=" . param("pid"));

		
	if($project["project_projectkind"] !=8)
	{
		//$page->register_action('dummy', ' ', "");
		//$page->register_action('fsummary', 'Financial Summary', "cer_financial_summary.php?pid=" . param("pid"));
	}
	
	

	

		
	$page->register_action('dummy0', ' ', "");
	$page->register_action('project', 'Access Project', "/user/project_task_center.php?pid=" . param("pid"), "_blank");
	if (has_access("can_edit_milestones") 
		and ($project["needs_cer"] == 1 
		or $project["needs_af"] == 1
		or $project["needs_inr03"] == 1))
	{
		
		$page->register_action('edit_milestones', 'Project Milestones', "/user/project_edit_milestones.php?pid=" . param("pid")); 
	}
	elseif (has_access("can_view_milestones") 
		and ($project["needs_cer"] == 1 
		or $project["needs_af"] == 1
		or $project["needs_inr03"] == 1))
	{   
		//$page->register_action('view_milestones', 'Project Milestones', "cer_view_milestones.php?pid=" . param("pid"));    
	}
	
	
	
	if (has_access("has_access_to_all_projects") or has_access("can_view_budget_in_projects"))
	{   
		//$page->register_action('view_project_budget', 'View Budget', "/user/project_costs_overview.php?pid=" . param("pid"), "_blank");
		
		
		if(($form_type == "CER" 
			or $form_type == "INR03")
			and $project["project_projectkind"] !=8)
		{
			$url = "/user/project_costs_bids.php?pid=" . param("pid");
			//$page->register_action('edit_lwoffers', 'Edit Quotes for Local Work', $url, "_blank");
		}
	}
		
}
?>