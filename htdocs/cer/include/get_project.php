<?php

$ln_version = 0;
$cer_version = 0;
if(array_key_exists('lnversion', $_GET))
{
	if($_GET['lnversion'] > 0 and $_GET['lnversion'] < 100)
	{
		$ln_version = $_GET['lnversion'];
	}
	else
	{
		$ln_version = 0;	
	}
}
if(array_key_exists('cerversion', $_GET))
{
	if($_GET['cerversion'] > 0 and $_GET['cerversion'] < 100)
	{
		$cer_version = $_GET['cerversion'];
	}
	else
	{
		$cer_version = 0;	
	}
}


$project = get_project(param("pid"));

//update depreciation period in cer_basicdata
$result = update_beginning_period_of_depreciation(param("pid"), $project["project_planned_opening_date"], $project["project_real_opening_date"]);

$cer_basicdata = get_cer_basicdata(param("pid"), $cer_version);


//check if we hav a posorder
$posorder_present = 0;
$sql = "select count(posorder_id) as num_recs " . 
       "from posorders " . 
	   "where posorder_order = " . $project["order_id"];

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	if($row["num_recs"] > 0)
	{
		$posorder_present = 1;
	}
}

if($posorder_present == 0)
{
	$sql = "select count(posorder_id) as num_recs " . 
		   "from posorderspipeline " . 
		   "where posorder_order = " . $project["order_id"];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		if($row["num_recs"] > 0)
		{
			$posorder_present = 1;
		}
	}
}

$form_type = "";
//corporate, joint venture, cooperation 3rd party
if($project["project_cost_type"] == 1 
	or $project["project_cost_type"] == 3 
	or $project["project_cost_type"] == 4)
{
	$form_type = "CER";
}
elseif($project["project_cost_type"] == 6)
{
	$form_type = "INR03";
}
else
{
	$form_type = "AF";
}


$business_plan_period = "";
$business_plan_period2 = "";
if($cer_basicdata["cer_basicdata_firstyear"] 
   and $cer_basicdata["cer_basicdata_lastyear"] 
   and $cer_basicdata["cer_basicdata_firstmonth"]
   and $cer_basicdata["cer_basicdata_lastmonth"])
{
	/*
	$date1 = $cer_basicdata["cer_basicdata_firstyear"] . "-" . $cer_basicdata["cer_basicdata_firstmonth"] . "-01";
	$date2 = $cer_basicdata["cer_basicdata_lastyear"] . "-" . $cer_basicdata["cer_basicdata_lastmonth"] . "-28";
	$diff = abs(strtotime($date2) - strtotime($date1));
	$num_years = floor($diff / (365*60*60*24));
	$num_months = floor(($diff - $num_years * 365*60*60*24) / (30*60*60*24));

	if($num_months == 12)
	{
		$num_months = 0;
		$num_years++;
	}

	$business_plan_period = $num_years . " years and " . $num_months . " months";
    */


	$duration_in_years = $cer_basicdata["cer_basicdata_lastyear"] - $cer_basicdata["cer_basicdata_firstyear"];
	$tmp1 = 13 - $cer_basicdata["cer_basicdata_firstmonth"] + $cer_basicdata["cer_basicdata_lastmonth"];
	$tmp2 = ($duration_in_years - 1)*12;
	$duration_in_years = round(($tmp1 + $tmp2) / 12, 1);

	$duration_in_years_and_months = floor(($tmp1 + $tmp2) / 12);
	$duration_in_years_and_months = $duration_in_years_and_months . " years and " . (($tmp1 + $tmp2) - ($duration_in_years_and_months*12)) . " months";

	$business_plan_period = $duration_in_years_and_months;
	$business_plan_period2 = $duration_in_years_and_months;
}


//check if there is is interco contribution
//new CER and AF Form 2013 not integrating it into Revenues
// so interco is not needed anymore
$use_old_ln_forms_before_2013 = false;
$use_old_cer_forms_before_2013 = check_interco_contribution(param("pid"));
//check if project is an old project
$ln_basicdata = get_ln_basicdata(param("pid"), $ln_version);


if($form_type == "CER" and ($ln_basicdata["ln_basicdata_submitted"] == NULL or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == '0000'))
{
	$use_old_ln_forms_before_2013 = false;
}
elseif($form_type == "CER" and $ln_basicdata["ln_basicdata_submitted"] <= DATE_NEW_LN_FORM_2013)
{
	$use_old_ln_forms_before_2013 = true;	
}

if($cer_basicdata["cer_basicdata_submitted"] == NULL or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == '0000')
{
	$use_old_cer_forms_before_2013 = false;	
}
elseif($cer_basicdata["cer_basicdata_resubmitted"] > DATE_NEW_CER_FORM_2013)
{
	$use_old_cer_forms_before_2013 = false;	
}
elseif($cer_basicdata["cer_basicdata_submitted"] <= DATE_NEW_CER_FORM_2013)
{
	$use_old_cer_forms_before_2013 = true;		
}
else
{
	$use_old_cer_forms_before_2013 = false;
}





if($ln_basicdata["ln_basicdata_rejected"] >= DATE_NEW_LN_FORM_2013 
	or $ln_basicdata["ln_basicdata_resubmitted"] >= DATE_NEW_LN_FORM_2013)
{
	$use_old_ln_forms_before_2013 = false;
}

if($cer_basicdata["cer_basicdata_rejected"] >= DATE_NEW_CER_FORM_2013 
   or $cer_basicdata["cer_basicdata_rejected"] >= DATE_NEW_CER_FORM_2013)
{
	$use_old_cer_forms_before_2013 = false;
}


//check if LN was approved
$ln_approval_date = "";
$sql = "select project_milestone_date from  project_milestones " . 
       " where project_milestone_milestone = 13 " . 
	   " and project_milestone_project = " . dbquote(param("pid"));

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	if($row["project_milestone_date"] != NULL and $row["project_milestone_date"] != '0000-00-00')
	{
		$ln_approval_date = $row["project_milestone_date"];
	}
}

//check if AF/CER was approved
$cer_approval_date = "";
$sql = "select project_milestone_date from  project_milestones " . 
       " where project_milestone_milestone in (12, 21) " . 
	   " and project_milestone_project = " . dbquote(param("pid"));

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if($row["project_milestone_date"] != NULL and $row["project_milestone_date"] != '0000-00-00')
	{
		$cer_approval_date = $row["project_milestone_date"];
	}
}

//location layout was introduced on september 25, 2014
$show_location_layout_field = true;

if($ln_approval_date != '' and $ln_approval_date <= '2014-09-24')
{
	$show_location_layout_field = false;
}
elseif($cer_approval_date != '' and $cer_approval_date <= '2014-09-24')
{
	$show_location_layout_field = false;
}




//new business plan 2016
$show_business_plan_version_2016 = 0;

if(($ln_basicdata["ln_basicdata_submitted"] == NULL 
   or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == '0000')
   and ($cer_basicdata["cer_basicdata_submitted"] == NULL 
   or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == '0000'))
{
	$show_business_plan_version_2016 = 1;

}
elseif($ln_basicdata["ln_basicdata_submitted"] >= DATE_NEW_BUSINESS_PLAN
	and ($cer_basicdata["cer_basicdata_submitted"] == NULL 
	or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == '0000'))
{
	$show_business_plan_version_2016 = 1;

}
elseif(($ln_basicdata["ln_basicdata_submitted"] == NULL 
   or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == '0000')
   and $cer_basicdata["cer_basicdata_submitted"] >= DATE_NEW_BUSINESS_PLAN)
{
	$show_business_plan_version_2016 = 1;
}
elseif($ln_basicdata["ln_basicdata_submitted"] >= DATE_NEW_BUSINESS_PLAN
	   or $cer_basicdata["cer_basicdata_submitted"] >= DATE_NEW_BUSINESS_PLAN)
{
	$show_business_plan_version_2016 = 1;
}
elseif($ln_basicdata["ln_basicdata_rejected"] >= DATE_NEW_BUSINESS_PLAN
	   or $cer_basicdata["cer_basicdata_rejected"] >= DATE_NEW_BUSINESS_PLAN)
{
	$show_business_plan_version_2016 = 1;
}
elseif($cer_basicdata["cer_basicdata_submitted"] == NULL 
   or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == '0000')
{
	$show_business_plan_version_2016 = 1;

}
elseif($cer_basicdata["cer_basicdata_lastyear"] >= substr(DATE_NEW_BUSINESS_PLAN, 0, 4))
{
	$show_business_plan_version_2016 = 1;
}


?>