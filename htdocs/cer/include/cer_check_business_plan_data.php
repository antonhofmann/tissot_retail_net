<?php
/*check if all business plan data is entered correctly*/

/*prepared for later use to show icons in the tabs 2016*/
/*work in progress*/

$tab_icon_classes = array();
$tab_icon_classes['general'] = 'data_ok';
$tab_icon_classes['franchisee'] = 'data_ok';
$tab_icon_classes['lease'] = 'data_ok';
$tab_icon_classes['rents'] = 'data_ok';
$tab_icon_classes['investments'] = 'data_ok';
$tab_icon_classes['revenues'] = 'data_ok';
$tab_icon_classes['salaries'] = 'data_ok';
$tab_icon_classes['expenses'] = 'data_ok';
$tab_icon_classes['cashflow'] = 'data_ok';
$tab_icon_classes['sellout'] = 'data_ok';
$tab_icon_classes['distribution'] = 'data_ok';

$tab_icon_classes['afpaymenttermns'] = 'data_ok';



if(!isset($posleases))
{
	$posdata = get_pos_data($project["project_order"]);
	$posleases = get_pos_leasedata($posdata["posaddress_id"], $project["project_order"]);
}

if($project["project_cost_type"] == 2 or $project["project_cost_type"] == 5) // franchisee, cooperation Tissot
{
	if($cer_basicdata["cer_basicdata_lastyear"] == NULL or $cer_basicdata["cer_basicdata_lastyear"] == "0")
	{
		$tab_icon_classes['general'] = 'data_not_ok';
	}
	elseif($project["project_projectkind"] != 8)
	{
		//franchisee attachments
		$franchisee_attachments = 1;
		if($project["project_projectkind"] != 8)
		{
			if($show_location_layout_field == true and $ln_basicdata["ln_no_ln_submission_needed"] == 0)
			{
				if($ln_basicdata["ln_basicdata_floorplan"] 
					and $ln_basicdata["ln_basicdata_location_layout"]
					and $ln_basicdata["ln_basicdata_pix1"]
					and $ln_basicdata["ln_basicdata_pix2"]
					and $ln_basicdata["ln_basicdata_pix3"]
					and $ln_basicdata["ln_basicdata_pix4"]
					and file_exists($_SERVER["DOCUMENT_ROOT"] . $ln_basicdata["ln_basicdata_floorplan"])
					and file_exists($_SERVER["DOCUMENT_ROOT"] . $ln_basicdata["ln_basicdata_location_layout"])
					and file_exists($_SERVER["DOCUMENT_ROOT"] . $ln_basicdata["ln_basicdata_pix1"])
					and file_exists($_SERVER["DOCUMENT_ROOT"] . $ln_basicdata["ln_basicdata_pix2"])
					and file_exists($_SERVER["DOCUMENT_ROOT"] . $ln_basicdata["ln_basicdata_pix3"])
					and file_exists($_SERVER["DOCUMENT_ROOT"] . $ln_basicdata["ln_basicdata_pix4"])
				
					)
				{
					//nop
				}
				else
				{
					$tab_icon_classes['franchisee'] = 'data_not_ok';
					$franchisee_attachments = 0;
				}
			}
			else
			{
				if($ln_basicdata["ln_basicdata_floorplan"] 
					and $ln_basicdata["ln_basicdata_pix1"]
					and $ln_basicdata["ln_basicdata_pix2"]
					and $ln_basicdata["ln_basicdata_pix3"]
					and $ln_basicdata["ln_basicdata_pix4"]
					and file_exists($_SERVER["DOCUMENT_ROOT"] . $ln_basicdata["ln_basicdata_floorplan"])
					and file_exists($_SERVER["DOCUMENT_ROOT"] . $ln_basicdata["ln_basicdata_pix1"])
					and file_exists($_SERVER["DOCUMENT_ROOT"] . $ln_basicdata["ln_basicdata_pix2"])
					and file_exists($_SERVER["DOCUMENT_ROOT"] . $ln_basicdata["ln_basicdata_pix3"])
					and file_exists($_SERVER["DOCUMENT_ROOT"] . $ln_basicdata["ln_basicdata_pix4"])
				
					)
				{
					//nop
				}
				else
				{
					$tab_icon_classes['franchisee'] = 'data_not_ok';
					$franchisee_attachments = 0;
				}
			}
		}

	
	}


}
else
{
}


if($project["project_projectkind"] != 8) // no PopUp
{
	//check if neighbourhood is filled in
	//neighbourhood
	if($project["pipeline"] == 0)
	{
		$pos_order_sql = "select * from posorders where posorder_order = " . $project["order_id"];
	}
	elseif($project["pipeline"] == 1)
	{
		$pos_order_sql = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
	}


	$businesstypes_mandatory = array();
	$sql = "select businesstype_id, businesstype_neighbour_needed " . 
		   "from businesstypes " . 
		   " order by businesstype_text";
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$businesstypes_mandatory[$row["businesstype_id"]]	= $row["businesstype_neighbour_needed"];
	}


	$pos_type = 1;
	$neighbourhood = 1;
	$res = mysql_query($pos_order_sql) or dberror($pos_order_sql);
	if ($row = mysql_fetch_assoc($res))
	{
		if($row["posorder_neighbour_left_business_type"] > 0 and 
			$row["posorder_neighbour_left_business_type"] > 0 
			and $businesstypes_mandatory[$row["posorder_neighbour_left_business_type"]] == 0)
		{
			$neighbourhood = 1;
		}
		elseif(!$row["posorder_neighbour_left"]
		   or !$row["posorder_neighbour_left_business_type"]
		   or !$row["posorder_neighbour_left_price_range"])
		{
			$neighbourhood = 0;
		}


		if($row["posorder_neighbour_right_business_type"] > 0 and 
			$row["posorder_neighbour_right_business_type"] > 0 
			and $businesstypes_mandatory[$row["posorder_neighbour_right_business_type"]] == 0)
		{
			$neighbourhood = 1;
		}
		elseif(!$row["posorder_neighbour_right"]
		   or !$row["posorder_neighbour_right_business_type"]
		   or !$row["posorder_neighbour_right_price_range"])
		{
			$neighbourhood = 0;
		}


		if($row["posorder_neighbour_acrleft_business_type"] > 0 and 
			$row["posorder_neighbour_acrleft_business_type"] > 0 
			and $businesstypes_mandatory[$row["posorder_neighbour_acrleft_business_type"]] == 0)
		{
			$neighbourhood = 1;
		}
		elseif(!$row["posorder_neighbour_acrleft"]
		   or !$row["posorder_neighbour_acrleft_business_type"]
		   or !$row["posorder_neighbour_acrleft_price_range"])
		{
			$neighbourhood = 0;
		}

		if($row["posorder_neighbour_acrright_business_type"] > 0 and 
			$row["posorder_neighbour_acrright_business_type"] > 0 
			and $businesstypes_mandatory[$row["posorder_neighbour_acrright_business_type"]] == 0)
		{
			$neighbourhood = 1;
		}
		elseif(!$row["posorder_neighbour_acrright"]
		   or !$row["posorder_neighbour_acrright_business_type"]
		   or !$row["posorder_neighbour_acrright_price_range"])
		{
			$neighbourhood = 0;
		}

		
		
		$pos_type = $row["posorder_postype"];
	}	


	if($neighbourhood == 0)
	{
		$tab_icon_classes['general'] = 'data_not_ok';
	}


	//check lease details
	$leaseperiod_is_defined = 1;
	$firstrentpayed_is_defined = 1;
	if($project["project_projectkind"] != 8) //popups
	{
		if(!isset($posleases["poslease_startdate"]) 
			or $posleases["poslease_startdate"] == NULL 
			or $posleases["poslease_startdate"] == '0000-00-00')
		{
			$tab_icon_classes['lease'] = 'data_not_ok';
			$leaseperiod_is_defined = 0;

		}
		elseif(!isset($posleases["poslease_firstrentpayed"]) 
			or $posleases["poslease_firstrentpayed"] == NULL 
			or $posleases["poslease_firstrentpayed"] == '0000-00-00')
		{
			$firstrentpayed_is_defined = 0;
			$tab_icon_classes['lease'] = 'data_not_ok';
		}
	}

	//Rental Costs
	$fixed_rents = 0;
	$turnoverbased_rents = 0;
	$period_for_fixed_tents_is_defined = 1;
	$sql = "select sum(cer_expense_amount) as amount " .
		   "from cer_expenses " .
		   " where cer_expense_project = " . dbquote(param("pid")) . 
		   " and cer_expense_cer_version = 0 " .
		   " and cer_expense_type IN(2) ";

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$fixed_rents = $row['amount'];



	$sql = "select sum(cer_expense_amount) as tob " .
		   "from cer_expenses " .
		   " where cer_expense_project = " . dbquote(param("pid")) .
		   " and cer_expense_cer_version = 0 " .
		   " and cer_expense_type IN(16) ";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if($row['tob'] > 1) {$turnoverbased_rents = $row['tob'];}

	if($fixed_rents == 0 and $turnoverbased_rents == 0)
	{
		$tab_icon_classes['rents'] = 'data_not_ok';
	}
	elseif(check_fixed_rental_data(param("pid")) == true)
	{
		$period_for_fixed_tents_is_defined = 0;
		$tab_icon_classes['rents'] = 'data_not_ok';

	}

	//Index on Rents if an Index Clause is present
	$index_clause_in_lease_details = 0;
	if($posorder_present == 1)
	{
		$posdata = get_pos_data($project["project_order"]);
		$posleases = get_pos_leasedata($posdata["posorder_posaddress"], $posdata["posorder_order"]);
		$construction = get_pos_intangibles(param("pid"), 1);
		$fixturing = get_pos_intangibles(param("pid"), 3);
		$keymoney = get_pos_intangibles(param("pid"), 15);

		if(count($posleases) > 0)
		{
			$index_clause_in_lease_details = $posleases["poslease_indexclause_in_contract"];
		}
	}

	$sum_of_index_rates = 0;
	$sql = "select sum(cer_fixed_rent_increas_rate) as sumindex " .
		   "from cer_fixed_rents " .
		   " where cer_fixed_rent_project_id = " . dbquote(param("pid")) . 
		   " and cer_fixed_rent_cer_version = 0 ";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sum_of_index_rates = $row['sumindex'];

	if($index_clause_in_lease_details == 1 and  $sum_of_index_rates == 0)
	{
		$tab_icon_classes['rents'] = 'data_not_ok';
	}
	elseif($index_clause_in_lease_details == 0 and  $sum_of_index_rates > 0)
	{
		$tab_icon_classes['rents'] = 'data_not_ok';
	}


	if($fixed_rents == 0 
		and array_key_exists("poslease_hasfixrent", $posleases) and $posleases["poslease_hasfixrent"] == 1)
	{
		$tab_icon_classes['rents'] = 'data_not_ok';
	}
	elseif($fixed_rents > 0 and array_key_exists("poslease_hasfixrent", $posleases) and $posleases["poslease_hasfixrent"] == 0)
	{
			$tab_icon_classes['lease'] = 'data_not_ok';
	}

	//Revenues
	$revenues = 0;
	$sql = "select sum(cer_revenue_watches) as total " .
		   "from cer_revenues " .
		   " where cer_revenue_project = " . dbquote(param("pid")) . 
		   " and cer_revenue_cer_version = 0 ";

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$revenues = $row["total"];
	}

	if($revenues == 0)
	{
		$tab_icon_classes['revenues'] = 'data_not_ok';
	}



	if($project["project_projectkind"] != 8)
	{
		if($revenues > 0 and $cer_basicdata["cer_basicdata_sales_reduction"] > 0)
		{
			//ok
		}
		else
		{
			$tab_icon_classes['revenues'] = 'data_not_ok';
		}
	}


	$other_sales_information = 1;

	if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] <> '0000-00-00')
	{
		$sql = "select count(cer_revenue_id) as num_recs " .
			   "from cer_revenues " .
			   " where cer_revenue_project = " . dbquote(param("pid")) . 
			   " and cer_revenue_cer_version = 0 " .
			   " and cer_revenue_year >= " . substr($project["project_real_opening_date"], 0, 4) . 
			   " and (cer_revenue_total_days_open_per_year = 0 " . 
			   " or cer_revenue_total_hours_open_per_week = 0 " . 
			   " or cer_revenue_total_workinghours_per_week = 0 " . 
			   " or cer_revenue_total_days_open_per_year is null " . 
			   " or cer_revenue_total_hours_open_per_week is null " . 
			   " or cer_revenue_total_workinghours_per_week is null)";
	}
	else
	{
		$sql = "select count(cer_revenue_id) as num_recs " .
			   "from cer_revenues " .
			   " where cer_revenue_project = " . dbquote(param("pid")) . 
			   " and cer_revenue_cer_version = 0 " .
			   " and (cer_revenue_total_days_open_per_year = 0 " . 
			   " or cer_revenue_total_hours_open_per_week = 0 " . 
			   " or cer_revenue_total_workinghours_per_week = 0 " . 
			   " or cer_revenue_total_days_open_per_year is null " . 
			   " or cer_revenue_total_hours_open_per_week is null " . 
			   " or cer_revenue_total_workinghours_per_week is null)";
	}

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if($row['num_recs'] > 0) {
		$other_sales_information = 0;
		$tab_icon_classes['revenues'] = 'data_not_ok';

	}


	//human resources
	$salaries = 0;
	$sql = "select sum(cer_salary_fixed_salary) as total " .
		   "from cer_salaries " .
		   " where cer_salary_project = " . dbquote(param("pid")) .
		   " and cer_salary_cer_version = 0 ";

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$salaries = $row["total"];
	}

	if($salaries == 0)
	{
		$tab_icon_classes['salaries'] = 'data_not_ok';
	}
	$expenses = $salaries;


	//check if all necessary expenses are filled in
	//Material Cost of Products SOLD, Aux. Material and Energy 
	if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] <> '0000-00-00')
	{
		$sql = "select count(cer_expense_amount) as num_recs " .
			   "from cer_expenses " .
			   " where cer_expense_project = " . dbquote(param("pid")) .
			   " and cer_expense_cer_version = 0 " .
			   " and cer_expense_year >= " . substr($project["project_real_opening_date"], 0, 4) .
			   " and cer_expense_type IN(4, 15) " . 
			   " and cer_expense_amount = 0";
		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		if($row['num_recs'] > 1) {$expenses = 0;}
	}
	else
	{
		$sql = "select count(cer_expense_amount) as num_recs " .
			   "from cer_expenses " .
			   " where cer_expense_project = " . dbquote(param("pid")) .
			   " and cer_expense_cer_version = 0 " .
			   " and cer_expense_type IN(4, 15) " . 
			   " and cer_expense_amount = 0";
		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		if($row['num_recs'] > 1) {$expenses = 0;}
	}


	if($expenses == 0)
	{
		$tab_icon_classes['expenses'] = 'data_not_ok';
	}

	if($project["project_projectkind"] != 8)
	{
		if($expenses > 0 and $cer_basicdata["cer_basicdata_cost_watches"] > 0)
		{
			//ok
		}
		else {
			$tab_icon_classes['expenses'] = 'data_not_ok';
		}
	}


	
	if($show_business_plan_version_2016 == 0)
	{
		//discount rate
		if($cer_basicdata["cer_basicdata_dicount_rate"] > 0)
		{
			//ok
		}
		else
		{
			$tab_icon_classes['cashflow'] = 'data_not_ok';
		}

		
		if($project["project_cost_type"] != 2 and
			$project["project_cost_type"] != 5) // franchisee, cooperation Tissot
		{
		
			//stock data
			$stock_data = 1;
			$sql = "select count(cer_stock_id) as num_recs " .
				   "from cer_stocks " .
				   " where cer_stock_project = " . dbquote(param("pid")) .
				   " and cer_stock_cer_version = 0 " .
				   " and cer_stock_stock_in_months = 0";

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			if($row['num_recs'] > 0 and $show_business_plan_version_2016 == 0) 
			{
				$stock_data = 0;
			}
			
			if($stock_data == 0)
			{
				$tab_icon_classes['cashflow'] = 'data_not_ok';
			}


			//payment terms
			$payment_terms = 1;
			$sql = "select count(cer_paymentterm_id) as num_recs " .
				   "from cer_paymentterms " .
				   " where cer_paymentterm_project = " . dbquote(param("pid")) .
				   " and cer_paymentterm_cer_version = 0 " .
				   " and cer_paymentterm_in_months = 0";

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			if($row['num_recs'] > 0) {$payment_terms = 0;}

			if($stock_data_present == false 
				and $show_business_plan_version_2016 == 1)
			{
				$payment_terms = 1;
			}
		}
	}


	//Sellouts
	$sellout_necessary = 0;
	$sellout_data_ok = true;
	$project_id = dbquote(param("pid"));
	$project_order = $project["project_order"];

	if(($project["project_projectkind"] == 2 
			or $project["project_projectkind"] == 3
			or $project["project_projectkind"] == 4
			or $project["project_projectkind"] == 5)) // renovation or tekover/renovation or lease renewal
	{
		$sellout_necessary = 1;
	}
	elseif($project["project_projectkind"] == 6 and $project["project_relocated_posaddress_id"] > 0) // relocation
	{
		$latest_project = get_latest_pos_project($project["project_relocated_posaddress_id"]);
		
		if(count($latest_project) > 0)
		{
			$sellout_necessary = 1;
			$project_id = $latest_project["project_id"];
			$project_order = $latest_project["order_id"];
		}
	}

	if($sellout_necessary == 1)
	{
		$sellout_data_ok = check_sellout_data($project_order, $project["project_id"], $cer_basicdata["cer_basicdata_firstyear"]);

		if($sellout_data_ok == false)
		{
			$tab_icon_classes['sellout'] = 'data_not_ok';
		}
	}








	//Distibution Analysis
	$distribution_analysis = true;

	//get the number of operating POS locations
	$num_of_operating_pos_locations = 0;
	$sql = "select count(posaddress_id) as num_recs " . 
		   " from posaddresses " . 
		   " where posaddress_client_id = " . dbquote($client_address["id"]) . 
		   " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " . 
		   " and posaddress_country = " . dbquote($project["order_shop_address_country"]);

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$num_of_operating_pos_locations = $row['num_recs'];

	if($form_type == "CER" 
		and $ln_basicdata["ln_no_ln_submission_needed"] != 1 
		and ($ln_basicdata["ln_basicdata_submitted"] == NULL or substr($ln_basicdata["ln_basicdata_submitted"],0,4) == '0000') )
	{
		//submission possible without distribution analysis

		$sql = "select count(ln_basicdata_lnr03_id) as num_recs " .
			   "from ln_basicdata_inr03 " .
			   " where ln_basicdata_lnr03_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"] .
			   " and ln_basicdata_lnr03_cer_version = 0 ";

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		
		if($row['num_recs'] == 0) {
			$distribution_analysis = false;
		}
		elseif($num_of_operating_pos_locations >= 10 and $row['num_recs'] < 10)
		{
			$distribution_analysis = false;
		}
		elseif($row['num_recs'] < 10 and $row['num_recs'] < $num_of_operating_pos_locations)
		{
			$distribution_analysis = false;
		}
	}
	elseif($form_type == "AF")
	{
		$sql = "select count(ln_basicdata_lnr03_id) as num_recs " .
			   "from ln_basicdata_inr03 " .
			   " where ln_basicdata_lnr03_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"] .
			   " and ln_basicdata_lnr03_cer_version = 0 ";

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		if($row['num_recs'] == 0) {
			$distribution_analysis = false;
		}
		elseif($num_of_operating_pos_locations >= 10 and $row['num_recs'] < 10)
		{
			$distribution_analysis = false;
		}
		elseif($row['num_recs'] < 10 and $row['num_recs'] < $num_of_operating_pos_locations)
		{
			$distribution_analysis = false;
		}
	}


	if($distribution_analysis == false)
	{
		$tab_icon_classes['distribution'] = 'data_not_ok';
	}

	//check if data is missing in distribution analysis
	$distribution_analysis2 = true;
	$sql = "select count(ln_basicdata_lnr03_id) as num_recs " .
		   "from ln_basicdata_inr03 " .
		   " where ln_basicdata_lnr03_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"] .
		   " and ln_basicdata_lnr03_cer_version = 0 " . 
		   " and (ln_basicdata_lnr03_year = 0 or ln_basicdata_lnr03_year is null or ln_basicdata_lnr03_year = '' ".
		   " or ln_basicdata_lnr03_month = 0 or ln_basicdata_lnr03_month is null or ln_basicdata_lnr03_month = '' " . 
		   " or ln_basicdata_lnr03_distance_unit = 0 or ln_basicdata_lnr03_distance_unit is null or ln_basicdata_lnr03_distance_unit = '' " . 
		   " or ln_basicdata_lnr03_distance = 0 or ln_basicdata_lnr03_distance is null or ln_basicdata_lnr03_distance = '' " .
		   " or ln_basicdata_lnr03_watches_units = 0 or ln_basicdata_lnr03_watches_units is null or ln_basicdata_lnr03_watches_units = '' " . 
		   ")";
		   


	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if($row['num_recs'] > 0) {
		$distribution_analysis2 = false;
	}


	if($distribution_analysis2 == false)
	{
		$tab_icon_classes['distribution'] = 'data_not_ok';
	}



	//ln versus cer
	if($ln_basicdata["ln_no_ln_submission_needed"] == 0)
	{
		$state = is_there_a_difference_between_cer_and_ln(param("pid"));
		$difference_between_cer_and_ln = $state['state'];

		$delta_limit_in_percent = 0.05;
		$investments_delta = round($state['total_investments_cer'] - $state['total_investments_ln'],0);

		$investments_delta_limit = 100;
		
		if($investments_delta == 0)
		{
			$investments_delta_limit = 0;
		}
		elseif($state['total_investments_cer'])
		{
			$investments_delta_limit = abs($state['total_investments_cer'] - $state['total_investments_ln']) / $state['total_investments_cer'];
			$investments_delta_limit = round(100*$investments_delta_limit, 0);

			if($investments_delta < 0 or abs($investments_delta_limit) < 100*$delta_limit_in_percent)
			{
				$investments_delta_limit = 0;
			}
		}



		$revenues_delta = round($state['total_revenues_cer'] - $state['total_revenues_ln'],0);

		$revenues_delta_limit = 100;
		if($revenues_delta == 0)
		{
			$revenues_delta_limit = 0;
		}
		elseif($state['total_revenues_cer'])
		{
			$revenues_delta_limit = abs($state['total_revenues_cer'] - $state['total_revenues_ln']) / $state['total_revenues_cer'];
			$revenues_delta_limit = round(100*$revenues_delta_limit, 0);

			if(abs($revenues_delta_limit) < 100*$delta_limit_in_percent)
			{
				$revenues_delta_limit = 0;
			}
		}

		$expenses_delta = round($state['total_expenses_cer'] - $state['total_expenses_ln'],0);

		$expenses_delta_limit = 100;
		if($expenses_delta == 0)
		{
			$expenses_delta_limit = 0;
		}
		elseif($state['total_expenses_cer'])
		{
			$expenses_delta_limit = abs($state['total_expenses_cer'] - $state['total_expenses_ln']) / $state['total_expenses_cer'];
			$expenses_delta_limit = round(100*$expenses_delta_limit, 0);

			if(abs($expenses_delta_limit) < 100*$delta_limit_in_percent)
			{
				$expenses_delta_limit = 0;
			}
		}
	}
	else
	{
		$investments_delta_limit = 0;
		$revenues_delta_limit = 0;
		$expenses_delta_limit = 0;
	}

}


//investments
if($project["project_projectkind"] != 5) //all projects except lease reneval
{
	if((array_key_exists("cer_investment_amount_cer_loc", $construction) and $construction["cer_investment_amount_cer_loc"] > 0) 
	or (array_key_exists("cer_investment_amount_cer_loc", $fixturing) and $fixturing["cer_investment_amount_cer_loc"] > 0)  
	or (array_key_exists("cer_investment_amount_cer_loc", $keymoney) and $keymoney["cer_investment_amount_cer_loc"] > 0))
	{
		$investments_are_defined =1;
	}
	else
	{
		$investments_are_defined = 0;
		$tab_icon_classes['investments'] = 'data_not_ok';
	}
}