<?php

//settings
$standard_line_height = 3.5;

// Financial Figures
$y = $y + 5;
$pdf->SetXY($margin_left,$y);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(63, 5, "3. Financial Figures", 1, "", "L");
$pdf->SetXY($margin_left,$y+5);
$pdf->Cell(63,97.6, "", 1, "", "L");

	$x1 = $margin_left+65;
	$x2 = $x1+42;
	$x3 = $x2+42;
	$x4 = $x3+42;
	$x5 = $x4+42;

	//column 1 to 5 Years
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;
		//column 1
		$pdf->SetXY($$xi,$y);
		$pdf->SetFont("arialn", "B", 9);
		
		if($year == $cer_basicdata["cer_basicdata_firstyear"] 
			and $number_of_months_first_year < 12)
		{
			$pdf->Cell(40, 5, $year . " (" . $number_of_months_first_year . ")", 1, "", "C");
		}
		elseif($year == $cer_basicdata["cer_basicdata_lastyear"] 
			and $number_of_months_first_year < 12)
		{
			$pdf->Cell(40, 5, $year . " (" . $number_of_months_last_year . ")", 1, "", "C");
		}
		else
		{
			$pdf->Cell(40, 5, $year, 1, "", "C");
		}
		$i++;

	}

	$y = $y + 5;
	
	//column 1 to 5 Currency
	for($i=1;$i<6;$i++)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(20, $standard_line_height, "in T" . $currency_symbol, 1, "", "C");

		$pdf->SetXY($$xi+20,$y);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(20, $standard_line_height, "in %", 1, "", "C");
	}

	//column 1 to 5 Boxes
	for($i=1;$i<6;$i++)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(20, 97.8, "", 1, "", "C");
		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, 97.8, "", 1, "", "C");
	}
	
	//Sales

	$y = $y + 2;
	$x0 = $margin_left+6;

	//$pdf->SetXY($margin_left+3.2,$y);
	//$pdf->SetFont("arialn", "B", 8);
	//$pdf->Cell(50, $standard_line_height, "Sales in Units", 0, "", "L");

	$y = $y + $standard_y;

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(22, $standard_line_height, "Sales in Units", 0, "", "L");

	//$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(25, $standard_line_height, "Tissot Watches", 0, "", "L");

	//print values for columns 1 to 5
	$i = 1;

	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, $sales_units_watches_values[$year], 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, $sales_units_watches_growth[$year], 0, "", "R");
		$i++;
	}	


	$y = $y + $standard_y;
	
	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(22, $standard_line_height, "", 0, "", "L");

	//$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(25, $standard_line_height, "Watch Straps", 0, "", "L");

	//print values for columns 1 to 5
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, $sales_units_jewellery_values[$year], 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		//$pdf->Cell(20, $standard_line_height, round($sales_units_jewellery_shares[$year], 1), 0, "", "R");
		$i++;
	}
	
	$y = $y + $standard_y;
	
	
	$pdf->Line($margin_left, $y, $margin_left+63,$y);
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;
		$pdf->Line($$xi, $y, $$xi+20,$y);
		$pdf->Line($$xi+20, $y, $$xi+40,$y);
		$i++;
	}


	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(22, $standard_line_height, "Average Price", 0, "", "L");

	//$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(25, $standard_line_height, "Tissot Watches", 0, "", "L");

	//print values for columns 1 to 5
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, round($exchange_rate*$average_price_watches_values[$year]/$exchange_rate_factor,2), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, $average_price_watches_shares[$year], 0, "", "R");
		$i++;
	}	

	$y = $y + $standard_y;
	
	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(22, $standard_line_height, "", 0, "", "L");
	
	//$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(25, $standard_line_height, "Watch Straps", 0, "", "L");

	//print values for columns 1 to 5
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, round($exchange_rate*$average_price_jewellery_values[$year]/$exchange_rate_factor,2), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, $average_price_jewellery_shares[$year], 0, "", "R");
		$i++;
	}
	
	$y = $y + $standard_y;
	

	$pdf->Line($margin_left, $y, $margin_left+63,$y);
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;
		$pdf->Line($$xi, $y, $$xi+20,$y);
		$pdf->Line($$xi+20, $y, $$xi+40,$y);
		$i++;
	}


	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(22, $standard_line_height, "Sales Value", 0, "", "L");

	//$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(25, $standard_line_height, "Tissot Watches", 0, "", "L");

	//print values for columns 1 to 5
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, round($exchange_rate*$sales_watches_values[$year]/$exchange_rate_factor/1000,0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, round($sales_watches_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	


	$y = $y + $standard_y;
	
	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(22, $standard_line_height, "", 0, "", "L");
	
	//$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(25, $standard_line_height, "Watch Straps", 0, "", "L");

	//print values for columns 1 to 5
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, round($exchange_rate*$sales_jewellery_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, round($sales_jewellery_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}

	$y = $y + $standard_y;
	
	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(22, $standard_line_height, "", 0, "", "L");
	
	//$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(25, $standard_line_height, "Customer Services/Accessories", 0, "", "L");


	//print values for columns 1 to 5
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, round($exchange_rate*$sales_customer_service_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, round($sales_customer_service_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}

	$y = $y + $standard_y;

	$pdf->SetXY($margin_left,$y);
	$pdf->Cell(63, $standard_line_height, "", 1, "", "L");

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	
	$pdf->Cell(60, $standard_line_height, "Total Gross Sales", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, round($exchange_rate*$total_gross_sales_values[$year]/$exchange_rate_factor/1000, 0), 1, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, round($total_gross_sales_shares[$year], 1). "%", 1, "", "R");
		$i++;
	}	

	$y = $y + $standard_line_height;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, $standard_line_height, "Sales Reduction", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, -round($exchange_rate*$sales_reduction_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, -round($sales_reduction_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	
	
	$y = $y +  $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, $standard_line_height, "Total Net Sales", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, round($exchange_rate*$total_net_sales_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, round($total_net_sales_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	

	$y = $y +  $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, $standard_line_height, "Material of Products Sold", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, -round($exchange_rate*$material_of_products_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, -round($material_of_products_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	

	$y = $y + $standard_y;
	
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 3);
	$pdf->Cell(63, $standard_line_height, "", 1, "", "L");

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, $standard_line_height, "Total Gross Margin", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, round($exchange_rate*$total_gross_margin_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, round($total_goss_margin_shares[$year], 1) . "%", 0, "", "R");
		

		$pdf->SetXY($$xi,$y);
		$pdf->SetFont("arialn", "B", 3);
		$pdf->Cell(20, $standard_line_height, "", 1, "", "L");
		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, "", 1, "", "L");
		
		
		$pdf->SetFont("arialn", "B", 8);
		$i++;
	}	

	$y = $y + $standard_y;
	
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 3);
	$pdf->Cell(63, $standard_line_height, "", 1, "", "L");

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, $standard_line_height, "Marketing Expenses", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$tmp =  $interco_contribution_values[$year] - $marketing_expenses_values[$year];
		if($tmp < 0){$tmp = -1*$tmp;}

		if($interco_contribution_values[$year] > $marketing_expenses_values[$year])
		{
			$pdf->Cell(20, $standard_line_height, round($exchange_rate*$tmp/$exchange_rate_factor/1000, 0), 1, "", "R");
		}
		else
		{
			$pdf->Cell(20, $standard_line_height, -1 * round($exchange_rate*$tmp/$exchange_rate_factor/1000, 0), 1, "", "R");
		}

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, round($marketing_expenses_shares[$year], 1) . "%", 1, "", "R");
		$i++;
	}	

	$y = $y + 4;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, $standard_line_height, "Indirect Salaries", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, -round($exchange_rate*$indirect_salaries_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, -round($indirect_salaries_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	

	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, $standard_line_height, "Rents", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, -round($exchange_rate*$rents_total_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, -round($rents_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	

	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, $standard_line_height, "Aux. Mat., energy, maintenance", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, -round($exchange_rate*$auxmat_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, -round($auxmat_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}
	
	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, $standard_line_height, "Sales & administration expenses", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, -round($exchange_rate*$sales_admin_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, -round($sales_admin_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	

	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	
	$pdf->Cell(50, $standard_line_height, "Depreciation on fixed assets (incl. dismantling costs)", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, -round($exchange_rate*$depreciation_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, -round($depreciation_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}
	
	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, $standard_line_height, "Depreciation on Goodwill/Keymoney", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, -round($exchange_rate*$prepayed_rent_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, -round($prepayed_rent_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	

	$y = $y + $standard_y;
	$pdf->SetXY($x0,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, $standard_line_height, "Other indirect Expenses", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, -round($exchange_rate*$other_expenses_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, -round($other_expenses_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	

	$y = $y + $standard_y;
	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, $standard_line_height, "Total Indirect Expenses", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, -round($exchange_rate*$total_indirect_expenses_values[$year]/$exchange_rate_factor/1000, 0), 0, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, -round($total_indirect_expenses_shares[$year], 1) . "%", 0, "", "R");
		$i++;
	}	
	
	$y = $y + $standard_line_height;
	
	$pdf->SetFont("arialn", "B", 8);
	$pdf->SetXY($margin_left,$y);
	$pdf->Cell(63, $standard_line_height, "", 1, "", "L");

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, $standard_line_height, "Operating Income w/o Wholesale Margin", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, round($exchange_rate*$operating_income01_values[$year]/$exchange_rate_factor/1000, 0), 1, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, round($operating_income01_shares[$year], 1) . "%", 1, "", "R");
		$i++;
	}	

	
	$y = $y +3.8;

	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(63, $standard_line_height, "", 1, "", "L");

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, $standard_line_height, "Operating Income incl. Wholesale Margin (" . $cer_basicdata["cer_basicdata_wholesale_margin"] . "%)", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, round($exchange_rate*$operating_income02_values[$year]/$exchange_rate_factor/1000, 0), 1, "", "R");

		$pdf->SetXY($$xi+20,$y);
		$pdf->Cell(20, $standard_line_height, round($operating_income02_shares[$year], 1) . "%", 1, "", "R");
		$i++;
	}


	if($apply_80_percent_scenario == 1)
	{
		$y = $y +3.8;

		$pdf->SetXY($margin_left,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(63, $standard_line_height, "", 1, "", "L");

		$pdf->SetXY($margin_left+3.2,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(50, $standard_line_height, "Operating Income incl. WSM (scenario 80%)", 0, "", "L");
		
		$i = 1;
		foreach($years_page1 as $key=>$year)
		{
			$xi = "x" . $i;

			$pdf->SetXY($$xi,$y);
			$pdf->Cell(20, $standard_line_height, round($exchange_rate*$operating_income02_80_percent_values[$year]/$exchange_rate_factor/1000, 0), 1, "", "R");

			$pdf->SetXY($$xi+20,$y);
			$pdf->Cell(20, $standard_line_height, round($operating_income02_80_percent_shares[$year], 1) . "%", 1, "", "R");
			$i++;
		}
	}
	
	//summary at bottom of page

	$y = $y + $standard_line_height;

	

	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, $standard_line_height, "Break Even Retail Margin ", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, round($break_even_retail_margin[$year], 0), 0, "", "R");
		$i++;
	}

	
	$y = $y + $standard_line_height;
    $pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "", 3);
	$pdf->Cell(63, $standard_line_height, "", 1, "", "L");


	$pdf->SetXY($margin_left+3.2,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, $standard_line_height, "Break Even Wholesale Margin", 0, "", "L");
	
	$i = 1;
	foreach($years_page1 as $key=>$year)
	{
		$xi = "x" . $i;

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, "", 1, "", "R");

		$pdf->SetXY($$xi,$y);
		$pdf->Cell(20, $standard_line_height, round($break_even_wholesale_margin[$year], 0), 0, "", "R");
		$i++;
	}

	$y = $y + 1;

?>