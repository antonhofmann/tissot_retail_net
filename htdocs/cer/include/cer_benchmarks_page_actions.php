<?php
/********************************************************************

    cer_benchmarks_page_actions.php

    Defines the possible page actions depending on access rights

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-03-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-03-27
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

$page->register_action('cer_benchmarks', 'Benchmarks', "cer_benchmarks.php");
$page->register_action('ln_versus_cer', 'LNR versus CER', "cer_ln_versus_cers.php");



?>