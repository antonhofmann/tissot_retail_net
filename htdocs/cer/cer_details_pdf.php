<?php
/********************************************************************

    cer_details_pdf.php

    Print Report of all CER/AF Data entered

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-09-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-09-12
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();


$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";
require "include/get_functions.php";
require "include/financial_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');

// Create and setup PDF document

$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 10);
$pdf->setPrintHeader(false);

$pdf->AddFont('helvetica');
$pdf->AddFont('helvetica', 'B');
$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');

$pdf->Open();

include("cer_details_pdf_detail.php");

$pdf->Output();
?>