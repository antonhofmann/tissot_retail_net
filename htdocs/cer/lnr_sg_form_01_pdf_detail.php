<?php
/********************************************************************

    lnr_sg_form_01_pdf_detail.php

    Print Detail Form LNR-01.

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2018-01-05
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2018-01-05
    Version:        1.0.0

    Copyright (c) 2018, Tissot, All Rights Reserved.
*********************************************************************/

//set pdf parameters
$margin_top = 10;
$margin_left = 12;
$y = $margin_top;
$x = $margin_left+1;
$standard_h = 6;

$pdf->AddPage('L');
$globalPageNumber++;

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(51, 8, "Swatch Group", 1);

$pdf->SetFont("arialn", "B", 14);
$pdf->Cell(178, 8, "LNR FORM", 1, "", "C");

$pdf->SetFont("arialn", "", 10);
$pdf->SetFillColor(248,251,167);


if($ln_version == 0 and $ln_basicdata["ln_basicdata_locked"] == 0)
{
	$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
}
else
{
	$pdf->Cell(20, 8, to_system_date(substr($ln_basicdata["versiondate"], 0, 10)), 1, "", "C", true);
}



$pdf->Cell(21, 8, "LNR-01", 1, "", "C");

	
	$y = $pdf->GetY()+10;
	$y_at_start = $y - 1;

	
	
	// 1. General information
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(228, 6, "1. General information (" . $project["order_shop_address_company"] . ', Project: ' . $project["project_number"]. ")", 1, "", "L");

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(20, 6, "Currency:", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(20, 6, $currency_symbol, 1, "", "L", true);
	
	$y = $pdf->GetY()+6;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, $standard_h, "Proposed Brand(s)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, BRAND, 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, $standard_h, "Country", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $project["country_name"], 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, $standard_h, "City + State / Province", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $pos_data["place_name"] . " / " . $pos_data["province_canton"], 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Landlord Name", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $landlord, 1, "", "L", true);


	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);


	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(50, 1.8*$standard_h, "Street address and/or name of mall / " . "\n" .  "dept. Store", 1, "T");
	$pdf->SetXY($margin_left+ 51,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(84,1.8*$standard_h, $pos_data["posaddress_address"], 1, "T", true);

	
	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);

	$h = $y;
	$pdf->SetXY($margin_left+ 51,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(84,$standard_h, $keypoints, 1, "T", true);
	
	$h = $pdf->GetY() - $h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(50, $h, "Type / Key points", 1, "L", false);
	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);

	
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Gross Sales Surface (sqm)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $total_surface, 1, "", "L", true);
	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);


	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Net Sales Surface (sqm)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $sale_surface, 1, "", "L", true);
	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	/*
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Back Office / Other Surface (sqm)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $bo_surface . ' / ' . $other_surface, 1, "", "L", true);
	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);
	*/

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Contract period begin", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $contract_starting_date, 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Contract period end", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $contract_ending_date, 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Renewal Option (months)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $renewal_option_date , 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Planned opening", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, to_system_date($project["project_real_opening_date"]), 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Rent free period (months)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $free_months, 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, $standard_h, "Annual fix rent (K" . $currency_symbol .")", 1, "", "L");
	$pdf->SetFont("arialn", "B", 9);
	
	if($savings_on_rent !=0)
	{
		$tmp = $fixedrent_firstyear . " (Savings on rental costs " . $savings_on_rent . ")";
		$pdf->Cell(84, $standard_h, $tmp, 1, "", "L", true);
	}
	else
	{
		$pdf->Cell(84, $standard_h, $fixedrent_firstyear, 1, "", "L", true);
	}

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, $standard_h, "Annual variable rent (% turnover)", 1, "", "L");
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(84, $standard_h, $sales_percent_first_year, 1, "", "L", true);
	

	
	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$h = $y;
	$pdf->SetXY($margin_left + 51,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(84,1.8*$standard_h, $negotiated_rental_conditions, 1, "T", true);
	
	$h = $pdf->GetY() - $h;
	$pdf->SetXY($margin_left+1,$y);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(50, $h, "Conditions for the application/ " . "\n" .  "of variable rent", 1, "L", false);
	$y = $pdf->GetY();
	$pdf->SetXY($margin_left+1,$y);

	

	
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Average annual increase (%)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	
	if($average_yearly_increase) {
		$pdf->Cell(84, $standard_h, $average_yearly_increase, 1, "", "L", true);
	}
	else {
		$pdf->Cell(84, $standard_h, "no increase", 1, "", "L", true);
	}

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Annual charges/other fees (K" . $currency_symbol .")", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	
	
	
	$tmp = $annual_charges + $tax_on_rents + $passenger_index;
	
	
	if($tmp == 0)
	{
		$pdf->Cell(84, $standard_h, "", 1, "", "L", true);
	}
	else
	{
		$tmp = "";
		if($tax_on_rents != 0)
		{
			$tmp .= "  Tax: " . $tax_on_rents;
		}
		if($tax_on_rents != 0)
		{
			$tmp .= "  Passenger Index: " . $passenger_index;
		}

		if($tmp != "")
		{
			$tmp = "Charges: " . $annual_charges . $tmp;
			$pdf->Cell(84, $standard_h, $tmp, 1, "", "L", true);
		}
		else
		{
			$pdf->Cell(84, $standard_h, $annual_charges, 1, "", "L", true);
		}
	}

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, $standard_h, "Key money (K" . $currency_symbol . ")", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $keymoney, 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Deposit (value/months of rent)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, round($deposit/1000, 0) . '/' . $deposit_value_in_months, 1, "", "L", true);

	$y = $pdf->GetY()+ $standard_h;
	$pdf->SetXY($margin_left+1,$y);

	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(50, $standard_h, "Real estate fees (%)", 1, "", "L");
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(84, $standard_h, $real_estate_fee, 1, "", "L", true);


	//draw outer box
	$y = $pdf->GetY()-$margin_top - 2;
	$pix_h = $pdf->GetY()-$margin_top - 12;
	$pdf->SetXY($margin_left,$y_at_start);
	$pdf->Cell(270, $y, "", 1);



	//picture 1

	$y = $margin_top + 18;
	$x = $margin_left+136;
	$picture_printed = false;
	if($pix1 != ".." and file_exists($pix1))
	{
		if(substr($pix1, strlen($pix1)-3, 3) == "jpg" or substr($pix1, strlen($pix1)-3, 3) == "JPG")
		{
			$imagesize = getimagesize($pix1);
			$w = $imagesize[0];
			$h = $imagesize[1];
			
			if($w >= $h)
			{
				$pdf->Image($pix1,$x,$y, 132);
			}
			else
			{
				$pdf->Image($pix1,$x,$y, 132, $pix_h, '', '', '', true, 300, '', false, false, 0, true);

			}
			
		}
	}
	
?>