<?php
/********************************************************************

	cer_draft_expenses.php

    Application Form: expenses
    
	Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-02-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-02-17
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_draft_functions.php";

check_access("has_access_to_cer_drafts");
set_referer("cer_draft_expense.php");


/********************************************************************
    prepare all data needed
*********************************************************************/
//get user's country
$sql = 'select address_country from users ' . 
       'left join addresses on address_id = user_address ' .
	   'where user_id = ' . user_id();

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$country = $row['address_country'];
}

$basicdata = get_draft_basicdata(param("did"));
$currency = get_draft_currency(param("did"));

$result = update_cost_of_products_sold(param("did"));

// get all years
$years = array();
$year_amounts = array();
$sql  = "select DISTINCT cer_expense_year " .
		"from cer_draft_expenses " . 
		"where cer_expense_draft_id = " . param("did") . 
		" order by cer_expense_year";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$year_amounts[$row["cer_expense_year"]] = array();
	$years[] = $row["cer_expense_year"];
}



// get all expenses for for each year
$sql  = "select cer_expense_type, cer_expense_year, cer_expense_amount " .
		"from cer_draft_expenses " .
		"where cer_expense_type != 9 and cer_expense_draft_id = " . param("did");

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$year_amounts[$row["cer_expense_year"]][$row["cer_expense_type"]] = $row["cer_expense_amount"];
}

//update salaries
$result = calculate_forcasted_salaries(param("did"), $years, $country);


//prepare sql for list
$sql_list1 = "select DISTINCT cer_expense_type_id, cer_expense_type_group_name, cer_expense_type_name " .
             "from cer_draft_expenses " . 
			 "left join cer_expense_types on cer_expense_type_id = cer_expense_type ";

if($basicdata['cer_basicdata_legal_type'] == 1) {
	$list1_filter = "cer_expense_type <> 9 and cer_expense_draft_id = " . param("did") . " and cer_expense_type <> 14";
}
else {
	$list1_filter = "cer_expense_type <> 9 " .
					"and cer_expense_draft_id = " . param("did") . 
					" and cer_expense_type <> 14 " . 
					" and cer_expense_type <> 11 " . 
					" and cer_expense_type <> 12 ";
}

//get list_totals
$list_totals = array();
foreach($years as $key=>$year)
{
	$list_totals[$year] = 0;
}

$sql = "select cer_expense_year, cer_expense_amount " .
       "from cer_draft_expenses " . 
	   " where " . $list1_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$list_totals[$row["cer_expense_year"]] = $list_totals[$row["cer_expense_year"]] + $row["cer_expense_amount"];
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_drafts", "cer_drafts");

$form->add_hidden("did", param("did"));
$form->add_section("Cost of Products Sold in Percent of Gross Sales");
$form->add_comment("Cost of watches/straps sold are: Purchase Price to Swatch Group or Agents - Retail Purchase Price.");

$form->add_edit("cer_basicdata_cost_watches", "Cost of Watches Sold in % of gross sales", 0, $basicdata["cer_basicdata_cost_watches"], TYPE_DECIMAL, 12,2);
$form->add_edit("cer_basicdata_cost_jewellery", "Cost of Watch Straps Sold in % gross sales", 0, $basicdata["cer_basicdata_cost_jewellery"], TYPE_DECIMAL, 12,2);

$form->add_edit("cer_basicdata_cost_service", "Cost of Customer Service/Accessories Sold in % gross sales", 0, $basicdata["cer_basicdata_cost_service"], TYPE_DECIMAL, 12,2);


$form->add_button("form_save", "Save Data");

/********************************************************************
    build list of standard expenses
*********************************************************************/
$list1 = new ListView($sql_list1, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("Expenses in " . $currency["symbol"]);
$list1->set_entity("cer_draft_expenses");
$list1->set_filter($list1_filter);
$list1->set_group("cer_expense_type_group_name");
$list1->set_order("cer_expense_type_sortorder");

$list1->add_hidden("did", param("did"));

$link = "cer_draft_expense.php?did=" . param("did");
$list1->add_column("cer_expense_type_name", "Type", $link, "", "", COLUMN_NO_WRAP);

foreach($years as $key=>$year)
{
	$list1->add_text_column("y" . $year, "$year", COLUMN_ALIGN_RIGHT, $year_amounts[$year]);
	$list1->set_footer("y" . $year, number_format($list_totals[$year] ,0, ".", "'"));

}
$list1->set_footer("cer_expense_type_name", "Total");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

$list1->populate();
$list1->process();

if($form->button("form_save"))
{
	if($form->validate())
	{
		
		$fields = array();
    
		$value = dbquote($form->value("cer_basicdata_cost_watches"));
		$fields[] = "cer_basicdata_cost_watches = " . $value;

		$value = dbquote($form->value("cer_basicdata_cost_jewellery"));
		$fields[] = "cer_basicdata_cost_jewellery = " . $value;

		$value = dbquote($form->value("cer_basicdata_cost_service"));
		$fields[] = "cer_basicdata_cost_service = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_drafts set " . join(", ", $fields) . " where cer_basicdata_id = " . param("did");
		mysql_query($sql) or dberror($sql);

		//recalculate cost of productions
		$result = update_cost_of_products_sold(param("did"));

		$link = "cer_draft_expenses.php?did=" . param("did");
		redirect($link);
		
	}
	
}

  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_drafts");


require "include/draft_page_actions.php";
$page->header();
$page->title($basicdata['cer_basicdata_title'] . ": Expenses");
require_once("include/tabs_draft2016.php");
$form->render();

$list1->render();
$page->footer();

?>