<?php
/********************************************************************

    af_INR03_2013_pdf.php

    Print PDF for AF Form INR03.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-06-29
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-29
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require "include/financial_functions.php";


check_access("has_access_to_cer");

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');


// Create and setup PDF document
class MYPDF extends TCPDF
{
	
	//Page footer
	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-10);
		//arialn italic 8
		$this->SetFont('arialn','I',8);
		//Page number
		$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
	}

}


$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 10, 0);
$pdf->setPrintHeader(false);
$pdf->SetAutoPageBreak(false, 0);

$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');
$pdf->Open();

include("af_INR03_2013_pdf_detail.php");


//page 3: Distribution Analysis
include("lnr_03_pdf_detail_2016.php");


$pdf->Output('INR-03_' . $project_name . "_" . $project_number . '.pdf');


?>