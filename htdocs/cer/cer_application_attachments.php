<?php
/********************************************************************

    cer_application_attachments.php

    Application Form: Attachments to the application
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-06-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-06-18
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/
$posdata = get_pos_data($project["project_order"]);


$business_plan_period_filled = 0;
if($cer_basicdata["cer_basicdata_firstyear"] 
   and $cer_basicdata["cer_basicdata_lastyear"] 
   and $cer_basicdata["cer_basicdata_firstmonth"]
   and $cer_basicdata["cer_basicdata_lastmonth"])
{
	$business_plan_period_filled = 1;
}



$sql_attachment = "select distinct order_file_id, order_file_visited, ".
                  "    order_file_title, order_file_description, ".
                  "    order_file_path, file_type_name, ".
                  "    order_files.date_created, ".
                  "    order_file_category_name, order_file_category_priority, ".
                  "    concat(user_name, ' ', user_firstname) as owner_fullname ".
                  "from order_files  " .
                  "left join order_file_categories on order_file_category_id = order_file_category ".
                  "left join users on user_id = order_file_owner ".
                  "left join file_types on order_file_type = file_type_id ";

$category = 18;
$list1_filter = "order_file_category_id = " . $category . " and order_file_order = " . $project["project_order"];


$urlQuery = http_build_query(array(
	'action' => '/cer/ajx_upload_supporting_documents.php',
	'title' => 'Upload Files',
	'subtitle' => 'Attachments',
	'infotext' => 'Allowed file types are: pdf, jpg, jpeg',
	'fields' => array(
		'id' => param("pid"),
		'category' => $category,
		'extensions' => 'pdf,jpg,jpeg',
		'startload' => true
	)
));

$iframeAttributes = array(
	"data-fancybox-type" => "iframe",
	"data-fancybox-width" => "800",
	"data-fancybox-height" => "620",
	"href" => "/public/themes/default/multifile.uploader.php?$urlQuery"
);


/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_summary", "cer_summary");

include("include/project_head.php");

$form->add_hidden("pid", param("pid"));

$form->add_button("add_attachment", "Add Attachments", $iframeAttributes, IS_UPLOAD_BUTTON);

$form->populate();

/********************************************************************
    List of Attachments
*********************************************************************/ 

$list1 = new ListView($sql_attachment);

if($project["project_cost_type"] == 1)
{
	$list1->set_title("CER Supporting Documents");
}
else
{
	$list1->set_title("LNR Supporting Documents");
}

$list1->set_entity("order_files");
$list1->set_filter($list1_filter);
$list1->set_order("order_files.date_created DESC");



$link = "http://" . $_SERVER["HTTP_HOST"] . "/include/openfile.php?id={order_file_id}";
$list1->add_column("order_file_title", "Title", $link, "", "", COLUMN_NO_WRAP);
$list1->add_column("file_type_name", "Type", "", "", "", COLUMN_NO_WRAP);



/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();


if($project["project_projectkind"] == 8)
{
	
	if($form_type == "INR03")
	{
		$page->title("INR-03 - Retail Furniture in Third-party Store: Attachments");
	}
	elseif($form_type == "AF")
	{
		$page->title("LNR for PopUps: Attachments");
	}
	else
	{
		$page->title("LNR/CER for PopUps: Attachments");
	}
}
else
{
	if($form_type == "INR03")
	{
		$page->title("INR-03 - Retail Furniture in Third-party Store: Attachments");
	}
	elseif($form_type == "AF")
	{
		$page->title("LNR/INR03: Attachments");
	}
	else
	{
		$page->title("LNR/CER: Attachments");
	}
}
if($business_plan_period_filled == 1)
{
	require_once("include/tabs2016.php");
}



$form->render();
$list1->render();

require "include/footer_scripts.php";
$page->footer();

?>