<?php
/********************************************************************

    ln_sg_booklet_pdf.php

    Print Lease Negotiation Booklet

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-11-06
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/
if(isset($write_tmp_file) and $write_tmp_file == true) {

}
else {
	require "../include/frame.php";
}

check_access("has_access_to_cer");

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../vendor/setasign/fpdi/fpdi.php');
require_once('../include/SetaPDF/Autoload.php');

require_once('lnr_sg_form_data.php');
require_once "include/in_financial_data.php";	



// write pdf
if($form_type == 'CER') {
	$file_name = 'LN_Booklet_' . str_replace(" ", "_", $pos_data['posaddress_name']) . "_" . $project["project_number"] . '.pdf';
}
else {
	$file_name = 'AF_Booklet_' . str_replace(" ", "_", $pos_data['posaddress_name']) . "_" . $project["project_number"] . '.pdf';
}

/********************************************************************
    prepare all data needed
*********************************************************************/
$PDFmerger_was_used = false;

class MYFPDI extends FPDI {

    var $_tplIdx;
    
    protected $footerCaption;

    protected $customStartPageNumber;

    protected $showHeader;

	public function setHeaderVisibility($visible=true) {
        $this->showHeader = $visible;
    }

	public function setFooterVisibility($visible=true) {
        $this->showFooter = $visible;
    }

	public function setFooterCaption($caption) {
        $this->footerCaption = $caption;
    }

	public function getStartPageNumber() {
        return $this->customStartPageNumber;
    }

	public function setStartPageNumber($pageNumber) {
        $this->customStartPageNumber = $pageNumber;
    }


	public function Header() {
		
		
	}
    
    
    function Footer() {
        
        if ($this->showFooter) {
			$this->SetY(-10);
			$this->SetX(12);
			$this->SetFont('arialn','I',8);
			// page number
			$pageNumber = $this->PageNo() + $this->customStartPageNumber - 1;
			$this->Cell(130, 8, $this->footerCaption . " / Page " . $pageNumber , 0, 1, 'L');
		}
    }
}

// Create and setup PDF document
class MYPDF extends TCPDF
{

	protected $customStartPageNumber = 0;
	
	public function setStartPageNumber($pageNumber) {
        $this->customStartPageNumber = $pageNumber;
    }
	
	//Page footer
	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-10);
		$this->SetX(12);
		$this->SetFont('arialn','I',8);
		if(($this->PageNo()+$this->customStartPageNumber) > 1) {
			$this->Cell(0,10, $this->filename . " - " . $this->versiondate . ' / Page '. ($this->PageNo() + $this->customStartPageNumber),0,0,'L');
		}
	}

}

$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);
$pdf->SetMargins(10, 23, 10);

$pdf->Open();
$pdf->SetAutoPageBreak(false);

$pdf->SetFillColor(220, 220, 220); 


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');



$pdf->versiondate = $version_date;
$pdf->filename = BRAND . " " . $file_name;
$footer_caption = BRAND . " " . $file_name . " - " . $version_date;


//add cover sheet
$pdf->AddPage('L');
$pdf->setJPEGQuality(100);
$pdf->Image('pictures/coversheet_booklets.jpg',0,0, 294);


$pdf->SetXY(170, 80);
$pdf->SetFont("arialn", "B", 14);

if($project["project_cost_type"] == 1) {
	$pdf->Cell(100, 8, "LNR BOOKLET", 0, "", "L");
}
else {
	$pdf->Cell(100, 8, "LNR BOOKLET", 0, "", "L");
}


$pdf->SetXY(170, 105);
$pdf->Cell(100, 8, $project["country_name"] . " - " . $pos_data["place_name"], 0, "", "L");
$pdf->SetXY(170, 115);
$pdf->Cell(100, 8, $pos_data["posaddress_name"], 0, "", "L");

$pdf->SetFont("arialn", "B", 12);
$pdf->SetXY(170, 125);
$pdf->Cell(100, 8, "Project " . $project["project_number"], 0, "", "L");
$pdf->SetXY(170, 130);
$pdf->Cell(100, 8, $postype, 0, "", "L");

include("lnr_sg_form_01_pdf_detail.php");

if($project["project_cost_type"] == 1 and $project["project_projectkind"] == 5) //corporate lease renewal
{
	include("lnr_sg_form_02Abis_pdf_detail.php");
}
elseif($project["project_cost_type"] == 1 or $project["project_cost_type"] == 9) //corporate
{
	include("lnr_sg_form_02A_pdf_detail.php");
}
elseif($project["project_cost_type"] == 2) //franchisee
{
	include("lnr_sg_form_02B_pdf_detail.php");
}
include("lnr_sg_form_03_pdf_detail.php");

//City Overview, Google Maps

//include("lnr_sg_google_maps.php");

if($project["project_projectkind"] !=5) {
	include("lnr_sg_city_information.php");
	include("lnr_sg_project_introduction.php");
}
include("lnr_sg_key_points.php");

$merger = new SetaPDF_Merger();
$pdfString = $pdf->output('', 'S');
$tmp = SetaPDF_Core_Document::loadByString($pdfString);
$merger->addDocument($tmp);
$pdf = null;

if($project["project_projectkind"] !=5) {
	// Floor Plan, Mall Map, Street Map
	$source_file = $floor_plan;
	if(file_exists($source_file))
	{
		//PDF
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			
			$globalPageNumber++;
			$fpdi = new MYFPDI();
			$fpdi->setHeaderVisibility(true);
			$fpdi->setFooterVisibility(true);
			$fpdi->SetMargins(0, 0, 0);
			$fpdi->SetAutoPageBreak(true, 40);
			$fpdi->setFooterCaption($footer_caption);
			$globalPageNumber++;

			// get source pdf
			$pageCount = $fpdi->setSourceFile($source_file);
			
			for ($i=1; $i <= $pageCount; $i++) { 
				$fpdi->setStartPageNumber($globalPageNumber);
				$tpl = $fpdi->ImportPage($i);
				$size = $fpdi->getTemplateSize($tpl);
				if($size['h'] > 220) {
					$fpdi->AddPage('P');
					$fpdi->useTemplate($tpl, 0, 0, 210, 0, true);
				}
				else {
					$fpdi->AddPage('L');
					$fpdi->useTemplate($tpl, 0, 0, 290, 0, true);
				}
			}
			
			$pdfString = $fpdi->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
			$merger->addDocument($tmp);
			$globalPageNumber = $globalPageNumber + $pageCount;
		}
	}


	//Zoom POS Section

	$source_file = $pos_section_image;

	if(file_exists($source_file))
	{
		//PDF
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			$fpdi = new MYFPDI();
			$fpdi->setStartPageNumber($globalPageNumber);
			$fpdi->setHeaderVisibility(true);
			$fpdi->setFooterVisibility(true);
			$fpdi->SetMargins(0, 0, 0);
			$fpdi->SetAutoPageBreak(true, 40);
			$fpdi->setFooterCaption($footer_caption);

			// get source pdf
			$pageCount = $fpdi->setSourceFile($source_file);

			for ($i=1; $i <= $pageCount; $i++) { 
				$fpdi->setStartPageNumber($globalPageNumber);
				$tpl = $fpdi->ImportPage($i);
				$size = $fpdi->getTemplateSize($tpl);
				if($size['h'] > 220) {
					$fpdi->AddPage('P');
					$fpdi->useTemplate($tpl, 0, 0, 210, 0, true);
				}
				else {
					$fpdi->AddPage('L');
					$fpdi->useTemplate($tpl, 0, 0, 290, 0, true);
				}
			}

			$pdfString = $fpdi->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
			$merger->addDocument($tmp);
			$globalPageNumber = $globalPageNumber + $pageCount;
		}
	}

	if($pix1 != ".." and file_exists($pix1))
	{
			
		$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(true);
		$pdf->SetMargins(10, 23, 10);

		$pdf->Open();
		$pdf->SetAutoPageBreak(false);

		$pdf->SetFillColor(220, 220, 220); 

		$pdf->AddFont('arialn','');
		$pdf->AddFont('arialn','B');
		$pdf->AddFont('arialn','I');
		$pdf->AddFont('arialn','BI');

		$pdf->versiondate = $version_date;
		$pdf->filename = $file_name;
		$pdf->setStartPageNumber($globalPageNumber-1);

		//Picture 1
		$x_offset = 145;
		if($pix1 != ".." and file_exists($pix1))
		{
			if(substr($pix1, strlen($pix1)-3, 3) == "jpg" 
				or substr($pix1, strlen($pix1)-3, 3) == "JPG"
				 or substr($pix1, strlen($pix1)-3, 3) == "jpeg"
				or substr($pix1, strlen($pix1)-3, 3) == "JPEG")
			{
				$pdf->AddPage("L");
				$globalPageNumber++;
				
				
				// Title first line
				$pdf->SetXY($margin_left,$margin_top);
				$pdf->SetFont("arialn", "B", 11);
				$pdf->Cell(270, 8, "  Site Pictures: " . $project["country_name"] . " - " . $pos_data["place_name"] . " - " . $pos_data["posaddress_name"] . " - Project " . $project['project_number'], 1, "", "L");

				$pdf->setXY($margin_left, 25);
				$x = $pdf->GetX();
				$y = $pdf->GetY();
				$pdf->Image($pix1,$x,$y, 0, 80, "", "", "", true, 300, "", false, false, 0, false, false,true);

				$image_dimenstions = getimagesize ( $pix1);
				$w = $image_dimenstions[0];
				$h = $image_dimenstions[1];
				$ratio = 80/$h;
				$x_offset = ceil($w*$ratio) + 5;
			}
		}


		//Picture 2
		if($pix2 != ".." and file_exists($pix2))
		{
			if(substr($pix2, strlen($pix2)-3, 3) == "jpg" 
				or substr($pix2, strlen($pix2)-3, 3) == "JPG"
				 or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
				or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
			{
				$pdf->setXY($margin_left+$x_offset, 25);
				$x = $pdf->GetX();
				$y = $pdf->GetY();
				$pdf->Image($pix2,$x,$y, 0, 80, "", "", "", true, 300, "", false, false, 0, false, false,true);
			}
		}


		//Picture 3
		if($pix3 != ".." and file_exists($pix3))
		{
			if(substr($pix3, strlen($pix3)-3, 3) == "jpg" 
				or substr($pix3, strlen($pix3)-3, 3) == "JPG"
				 or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
				or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
			{
				$pdf->setXY($margin_left, 110);
				$x = $pdf->GetX();
				$y = $pdf->GetY();
				$pdf->Image($pix3,$x,$y, 0, 80, "", "", "", true, 300, "", false, false, 0, false, false,true);

				$image_dimenstions = getimagesize ( $pix3);
				$w = $image_dimenstions[0];
				$h = $image_dimenstions[1];
				$ratio = 80/$h;
				$x_offset = ceil($w*$ratio) + 5;
			}
		}


		//Picture 4
		if($pix4 != ".." and file_exists($pix4))
		{
			if(substr($pix4, strlen($pix4)-3, 3) == "jpg" 
				or substr($pix4, strlen($pix4)-3, 3) == "JPG"
				 or substr($pix4, strlen($pix4)-3, 3) == "jpeg"
				or substr($pix4, strlen($pix4)-3, 3) == "JPEG")
			{
				$pdf->setXY($margin_left+$x_offset, 110);
				$x = $pdf->GetX();
				$y = $pdf->GetY();
				$pdf->Image($pix4,$x,$y, 0, 80, "", "", "", true, 300, "", false, false, 0, false, false,true);
			}
		}

		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		$pdf = null;
	}


	//Page 4 Location layout

	$source_file = $location_layout;

	if(file_exists($source_file))
	{
		//PDF
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			$fpdi = new MYFPDI();
			$fpdi->setStartPageNumber($globalPageNumber);
			$fpdi->setHeaderVisibility(true);
			$fpdi->setFooterVisibility(true);
			$fpdi->SetMargins(0, 0, 0);
			$fpdi->SetAutoPageBreak(true, 40);
			$fpdi->setFooterCaption($footer_caption);

			// get source pdf
			$pageCount = $fpdi->setSourceFile($source_file);

			for ($i=1; $i <= $pageCount; $i++) { 
				$fpdi->setStartPageNumber($globalPageNumber);
				$tpl = $fpdi->ImportPage($i);
				$size = $fpdi->getTemplateSize($tpl);
				if($size['h'] > 220) {
					$fpdi->AddPage('P');
					$fpdi->useTemplate($tpl, 0, 0, 210, 0, true);
				}
				else {
					$fpdi->AddPage('L');
					$fpdi->useTemplate($tpl, 0, 0, 290, 0, true);
				}
			}

			$pdfString = $fpdi->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
			$merger->addDocument($tmp);
			$globalPageNumber = $globalPageNumber + $pageCount;
		}
	}


	// Document explaining relocation costs 
	if($project["project_projectkind"] == 6) {

		$source_file = $relocation_doc;

		if(file_exists($source_file))
		{
			//PDF
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
			
				$fpdi = new MYFPDI();
				$fpdi->setStartPageNumber($globalPageNumber);
				$fpdi->setHeaderVisibility(true);
				$fpdi->setFooterVisibility(true);
				$fpdi->SetMargins(0, 0, 0);
				$fpdi->SetAutoPageBreak(true, 40);
				$fpdi->setFooterCaption($footer_caption);

				// get source pdf
				$pageCount = $fpdi->setSourceFile($source_file);

				for ($i=1; $i <= $pageCount; $i++) { 
					$fpdi->setStartPageNumber($globalPageNumber);
					$tpl = $fpdi->ImportPage($i);
					$size = $fpdi->getTemplateSize($tpl);
					if($size['h'] > 220) {
						$fpdi->AddPage('P');
						$fpdi->useTemplate($tpl, 0, 0, 210, 0, true);
					}
					else {
						$fpdi->AddPage('L');
						$fpdi->useTemplate($tpl, 0, 0, 290, 0, true);
					}
				}
				$pdfString = $fpdi->output('', 'S');
				$tmp = SetaPDF_Core_Document::loadByString($pdfString);
				$merger->addDocument($tmp);
				$globalPageNumber = $globalPageNumber + $pageCount;

			}
		}
	}

	// Document explaining sales figures of the last years
	if($project["project_projectkind"] == 2
		or $project["project_projectkind"] == 3
		or $project["project_projectkind"] == 6) {

		$source_file = $sales_doc;

		if(file_exists($source_file))
		{
			//PDF
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
			
				$fpdi = new MYFPDI();
				$fpdi->setStartPageNumber($globalPageNumber);
				$fpdi->setHeaderVisibility(true);
				$fpdi->setFooterVisibility(true);
				$fpdi->SetMargins(0, 0, 0);
				$fpdi->SetAutoPageBreak(true, 40);
				$fpdi->setFooterCaption($footer_caption);

				// get source pdf
				$pageCount = $fpdi->setSourceFile($source_file);

				for ($i=1; $i <= $pageCount; $i++) { 
					$fpdi->setStartPageNumber($globalPageNumber);
					$tpl = $fpdi->ImportPage($i);
					$size = $fpdi->getTemplateSize($tpl);
					if($size['h'] > 220) {
						$fpdi->AddPage('P');
						$fpdi->useTemplate($tpl, 0, 0, 210, 0, true);
					}
					else {
						$fpdi->AddPage('L');
						$fpdi->useTemplate($tpl, 0, 0, 290, 0, true);
					}
				}
				$pdfString = $fpdi->output('', 'S');
				$tmp = SetaPDF_Core_Document::loadByString($pdfString);
				$merger->addDocument($tmp);
				$globalPageNumber = $globalPageNumber + $pageCount;

			}
		}
	}
}

// Lease Agreement 
$source_file = $lease_agreement;
if(file_exists($source_file))
{
	//PDF
	if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
	{
		$fpdi = new MYFPDI();
		$fpdi->setStartPageNumber($globalPageNumber);
		$fpdi->setHeaderVisibility(true);
		$fpdi->setFooterVisibility(true);
		$fpdi->SetMargins(0, 0, 0);
		$fpdi->SetAutoPageBreak(true, 40);
		$fpdi->setFooterCaption($footer_caption);

		// get source pdf
		$pageCount = $fpdi->setSourceFile($source_file);

		for ($i=1; $i <= $pageCount; $i++) { 
			$fpdi->setStartPageNumber($globalPageNumber);
			$tpl = $fpdi->ImportPage($i);
			$size = $fpdi->getTemplateSize($tpl);
			if($size['h'] > 220) {
				$fpdi->AddPage('P');
				$fpdi->useTemplate($tpl, 0, 0, 210, 0, true);
			}
			else {
				$fpdi->AddPage('L');
				$fpdi->useTemplate($tpl, 0, 0, 290, 0, true);
			}
		}

		

		$pdfString = $fpdi->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		$globalPageNumber = $globalPageNumber + $pageCount;
	}
}

	
//LN supporting documents
$output_done = true;
$has_images = false;
$image_pages_created = false;
$number_of_images = 1;
if(count($ln_supporting_documents_images) > 0 
	or count($ln_supporting_documents_pdfs) > 0)
{
	foreach($ln_supporting_documents_images as $key=>$source_file)
	{
		if(file_exists($source_file))
		{
			if(substr($source_file, strlen($source_file)-3, 3) == "jpg" 
				or substr($source_file, strlen($source_file)-3, 3) == "JPG"
				 or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
				or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
			{
				$has_images = true;
				
				if($image_pages_created == false) {
					$image_pages_created = true;
					$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
					$pdf->setPrintHeader(false);
					$pdf->setPrintFooter(true);
					$pdf->SetMargins(10, 23, 10);

					$pdf->Open();
					$pdf->SetAutoPageBreak(false);

					$pdf->SetFillColor(220, 220, 220); 

					$pdf->AddFont('arialn','');
					$pdf->AddFont('arialn','B');
					$pdf->AddFont('arialn','I');
					$pdf->AddFont('arialn','BI');

					$pdf->versiondate = $version_date;
					$pdf->filename = $file_name;
					$pdf->setStartPageNumber($globalPageNumber-1);


				}

				if($number_of_images == 1) {				
					
					$pdf->AddPage("L");
					$globalPageNumber++;

					$pdf->SetXY($margin_left,$margin_top);
					$pdf->SetFont("arialn", "B", 11);
					$pdf->Cell(270, 8, "Additional Pictures: " . $project["country_name"] . " - " . $pos_data["place_name"] . " - " . $pos_data["posaddress_name"] . " - Project " . $project['project_number'], 1, "", "L");
					
					$pdf->setXY($margin_left, 25);
					$x = $pdf->GetX();
					$y = $pdf->GetY();
					$pdf->Image($source_file,$x,$y, 0, 80, "", "", "", true, 300, "", false, false, 0, false, false,true);

					$image_dimenstions = getimagesize ( $source_file);
					$w = $image_dimenstions[0];
					$h = $image_dimenstions[1];
					$ratio = 80/$h;
					$x_offset = ceil($w*$ratio) + 5;
					$number_of_images++;
				}
				elseif($number_of_images == 2) {
					$pdf->setXY($margin_left+$x_offset, 25);
					$x = $pdf->GetX();
					$y = $pdf->GetY();
					$pdf->Image($source_file,$x,$y, 0, 80, "", "", "", true, 300, "", false, false, 0, false, false,true);
					$number_of_images++;
				}
				elseif($number_of_images == 3) {
					$pdf->setXY($margin_left, 110);
					$x = $pdf->GetX();
					$y = $pdf->GetY();
					$pdf->Image($source_file,$x,$y, 0, 80, "", "", "", true, 300, "", false, false, 0, false, false,true);

					$image_dimenstions = getimagesize ( $source_file);
					$w = $image_dimenstions[0];
					$ratio = 80/$h;
					$x_offset = ceil($w*$ratio) + 5;
					$number_of_images++;
				}
				elseif($number_of_images == 4) {
					$pdf->setXY($margin_left+$x_offset, 110);
					$x = $pdf->GetX();
					$y = $pdf->GetY();
					$pdf->Image($source_file,$x,$y, 0, 80, "", "", "", true, 300, "", false, false, 0, false, false,true);
					$number_of_images = 1;
				}

			}
		}
	}
	
	if($has_images == true) {
		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		$pdf = null;
	}

	foreach($ln_supporting_documents_pdfs as $key=>$source_file)
	{
		if(file_exists($source_file))
		{
			//PDF
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				$fpdi = new MYFPDI();
				$fpdi->setStartPageNumber($globalPageNumber);
				$fpdi->setHeaderVisibility(true);
				$fpdi->setFooterVisibility(true);
				$fpdi->SetMargins(0, 0, 0);
				$fpdi->SetAutoPageBreak(true, 40);
				$fpdi->setFooterCaption($footer_caption);

				// get source pdf
				$pageCount = $fpdi->setSourceFile($source_file);

				for ($i=1; $i <= $pageCount; $i++) { 
					$fpdi->setStartPageNumber($globalPageNumber);
					$tpl = $fpdi->ImportPage($i);
					$size = $fpdi->getTemplateSize($tpl);
					if($size['h'] > 220) {
						$fpdi->AddPage('P');
						$fpdi->useTemplate($tpl, 0, 0, 210, 0, true);
					}
					else {
						$fpdi->AddPage('L');
						$fpdi->useTemplate($tpl, 0, 0, 290, 0, true);
					}
				}

				$pdfString = $fpdi->output('', 'S');
				$tmp = SetaPDF_Core_Document::loadByString($pdfString);
				$merger->addDocument($tmp);
				$globalPageNumber = $globalPageNumber + $pageCount;
			}
		}
	}
}

	
$merger->merge();
$document = $merger->getDocument();


//write file to disk
if(isset($write_tmp_file) and $write_tmp_file == true) {

	$file_name = TMP_FILE_DIR_ABSOLUTE . $file_name;
	$document->setWriter(new SetaPDF_Core_Writer_File($file_name, true));
	$document->save()->finish();
}
else {
	$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
	$document->save()->finish();
}

?>