<?php
/********************************************************************

    ln_form_pdf_detail.php

    Print Lease Negotiation Form

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-11-06
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/
	
		
	if($exchange_rate_factor > 0)
	{
		$e_rate = $exchange_rate/$exchange_rate_factor;
	}
	else
	{
		$e_rate = $exchange_rate;
	}
	
	
	if($project["project_projectkind"] == 3
	   and $project["project_planned_takeover_date"] != NULL and $project["project_planned_takeover_date"] != '0000-00-00')
	{
		if(substr($project["project_planned_takeover_date"], 5,2) > $cer_basicdata["cer_basicdata_firstmonth"])
		{
			$number_of_months_first_year = 13-(int)substr($project["project_planned_takeover_date"], 5,2);
		}
		else
		{
			if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
			{
				$number_of_months_first_year = $cer_basicdata["cer_basicdata_lastmonth"]-$cer_basicdata["cer_basicdata_firstmonth"] + 1;
			}
			else
			{
				if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
				{
					$number_of_months_first_year = $cer_basicdata["cer_basicdata_lastmonth"]-$cer_basicdata["cer_basicdata_firstmonth"] + 1;
				}
				else
				{
					$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
				}
			}
		}
	}
	elseif($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] != '0000-00-00')
	{
		if(substr($project["project_real_opening_date"], 0, 4) == $cer_basicdata["cer_basicdata_firstyear"]
			and substr($project["project_real_opening_date"], 5,2) > $cer_basicdata["cer_basicdata_firstmonth"])
		{
			$number_of_months_first_year = 13-(int)substr($project["project_real_opening_date"], 5,2);
		}
		else
		{
			if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
			{
				$number_of_months_first_year = $cer_basicdata["cer_basicdata_lastmonth"]-$cer_basicdata["cer_basicdata_firstmonth"] + 1;
			}
			else
			{
				if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
				{
					$number_of_months_first_year = $cer_basicdata["cer_basicdata_lastmonth"]-$cer_basicdata["cer_basicdata_firstmonth"] + 1;
				}
				else
				{
					$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
				}
			}
		}
	}
	else
	{
		$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
	}
	
	
	
	$number_of_months_last_year = $cer_basicdata["cer_basicdata_lastmonth"];

	$add_total_sales = false;
	if($number_of_months_first_year == 12)
	{
		$i = 0;
	}
	elseif($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
	{
		$add_total_sales = true;
	}
	elseif($cer_basicdata["cer_basicdata_firstyear"]+1 == $cer_basicdata["cer_basicdata_lastyear"] and $number_of_months_last_year < 12)
	{
		$add_total_sales = true;
	}
	else
	{
		$i = 1;
	}

	$sd01 = "";
	$sd02 = "";
	$sd03 = "";
	$sd04 = "";

	if($add_total_sales == true and array_key_exists(0, $years) and array_key_exists(1, $years))
	{
	
		$sd01 = number_format($sales_units_watches_values[$years[0]] + $sales_units_watches_values[$years[1]], 0, ".", "'");
		$sd02 = number_format(round($e_rate*($total_gross_sales_values[$years[0]]+$total_gross_sales_values[$years[1]])/1000,0), 0, ".", "'");
		$sd03 = number_format(round($e_rate*($operating_income01_values[$years[0]]+$operating_income01_values[$years[1]])/1000,0), 0, ".", "'");
		$sd04 = number_format(round($e_rate*($operating_income02_values[$years[0]]+$operating_income02_values[$years[1]])/1000,0), 0, ".", "'");
	}
	elseif($add_total_sales == true and array_key_exists(0, $years))
	{
	
		$sd01 = number_format($sales_units_watches_values[$years[0]], 0, ".", "'");
		$sd02 = number_format(round($e_rate*$total_gross_sales_values[$years[0]]/1000,0), 0, ".", "'");
		$sd03 = number_format(round($e_rate*$operating_income01_values[$years[0]]/1000,0), 0, ".", "'");
		$sd04 = number_format(round($e_rate*$operating_income02_values[$years[0]]/1000,0), 0, ".", "'");
	}
	elseif(array_key_exists($i, $years))
	{
		$sd01 = number_format($sales_units_watches_values[$years[$i]], 0, ".", "'");
		$sd02 = number_format(round($e_rate*$total_gross_sales_values[$years[$i]]/1000,0), 0, ".", "'");
		$sd03 = number_format(round($e_rate*$operating_income01_values[$years[$i]]/1000,0), 0, ".", "'");
		$sd04 = number_format(round($e_rate*$operating_income02_values[$years[$i]]/1000,0), 0, ".", "'");
			
	}

	//if business plan period is shorter than 12 months
	$first_full_year_exists = true;
	if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
	{
		if(array_key_exists($cer_basicdata["cer_basicdata_firstyear"], $sales_units_watches_values))
		{
			$i = 0;
			$sd01 = number_format($sales_units_watches_values[$years[$i]], 0, ".", "'");
			$sd02 = number_format(round($e_rate*$total_gross_sales_values[$years[$i]]/1000,0), 0, ".", "'");
			$sd03 = number_format(round($e_rate*$operating_income01_values[$years[$i]]/1000,0), 0, ".", "'");
			$sd04 = number_format(round($e_rate*$operating_income02_values[$years[$i]]/1000,0), 0, ".", "'");
			$first_full_year_exists = false;
		}
	}
	

	//Logo
	$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
	//arialn bold 15
	
	if(isset($version_date) and $version_date)
	{
		$pdf->SetFont('arialn','B',12);
		//Move to the right
		$pdf->Cell(40);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title . $version_date,0,0,'R');
		//Line break
		$pdf->SetY(23);
	}
	else
	{
		$pdf->SetFont('arialn','B',18);
		//Move to the right
		$pdf->Cell(80);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title,0,0,'R');
		//Line break
		$pdf->SetY(23);
	}




	$pdf->setXY(10, $pdf->getY());
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(97,4,"Address Data",1, 0, 'L', 1);
	$pdf->Cell(90,4,"Location Data",1, 0, 'L', 1);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(30,4,"POS Name:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$posname,1, 0, 'L', 0);
	//$pdf->Cell(40,4,"Environment:",1, 0, 'L', 0);
	$pdf->Cell(90,4,$posareas,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"Country:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$country_name,1, 0, 'L', 0);
	$pdf->Cell(25,4,"Placement:",1, 0, 'L', 0);
	$pdf->Cell(65,4,$placement,1, 0, 'L', 0);
	$pdf->Ln();

	//$tmp = "Gross/Total/Sales: " . $gross_surface . " / " .$total_surface . " / " . $sales_surface;
	$tmp = "Total/Sales: " .$total_surface . " / " . $sales_surface;
	$pdf->Cell(30,4,"City:",1, 0, 'L', 0);	
	$pdf->Cell(67,4,$city_name,1, 0, 'L', 0);
	$pdf->Cell(25,4,"Surfaces in sqms: ",1, 0, 'L', 0);
	$pdf->Cell(65,4,$tmp,1, 0, 'L', 0);
	$pdf->Ln();

	
	

	if($surface_of_relocated_pos  != "")
	{
		$pdf->Cell(30,4,"Pos Type:",1, 0, 'L', 0);	
		$pdf->Cell(67,4,$postype,1, 0, 'L', 0);
		
		$pdf->Cell(25,4," -> Relocated: ",1, 0, 'L', 0);
		$pdf->Cell(65,4,"Gross/Total/Sales: " .  $surface_of_relocated_pos,1, 0, 'L', 0);
		$pdf->Ln();

		$pdf->Cell(30,4,"",1, 0, 'L', 0);	
		$pdf->Cell(67,4,"",1, 0, 'L', 0);
		$pdf->Cell(25,4,"Project Type:",1, 0, 'L', 0);
		$pdf->Cell(65,4,$project_kind,1, 0, 'L', 0);
		$pdf->Ln();
	}
	else
	{
		$pdf->Cell(30,4,"Pos Type:",1, 0, 'L', 0);	
		$pdf->Cell(67,4,$postype,1, 0, 'L', 0);
		$pdf->Cell(25,4,"Project Type:",1, 0, 'L', 0);
		$pdf->Cell(65,4,$project_kind,1, 0, 'L', 0);
		$pdf->Ln();
	}

	


	$x = $pdf->GetX();
	$y = $pdf->GetY() + 2;


	//picture 1
	$picture_printed = false;
	if($pix1 != ".." and file_exists($pix1))
	{
		if(substr($pix1, strlen($pix1)-3, 3) == "jpg" 
			or substr($pix1, strlen($pix1)-3, 3) == "JPG"
		     or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
				or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
		{
			
			$imagesize = getimagesize($pix1);
			$w = $imagesize[0];
			$h = $imagesize[1];

			$imgratio=$w/$h;

			if ($imgratio>1)
			{
				if($w >= 95)
				{
					$scale_factor = 95/$w;
					$h = $h*$scale_factor;
					$w = $w*$scale_factor;
					$pdf->Image($pix1,$x,$y, $w, $h);
					$y1 = $h;
				}
				else
				{
					$pdf->Image($pix1,$x,$y);
					$y1 = ($h / 72) * 25.4;
				}
				
			}
			else
			{
				if($w >= 71)
				{
					$scale_factor = 71/$h;
					$h = $h*$scale_factor;
					$w = $w*$scale_factor;
					$pdf->Image($pix1,$x,$y, $w, $h);
					$y1 = $h;
				}
				else
				{
					$pdf->Image($pix1,$x,$y);
					$y1 = ($h / 72) * 25.4;
				}
				
			}

			$pdf->SetY($y1 + $pdf->GetY() + 5);
			
			$picture_printed = true;
		}
	}

	


	$tmp_x = $pdf->getX();
	$tmp_y = $pdf->getY();


	$pdf->setXY(107, $y);
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(90,4,"Sales Data",1, 0, 'L', 1);
	$pdf->Ln();
	
	$pdf->SetFont('arialn','',9);
	$pdf->setXY(107, $pdf->getY());
	if($first_full_year_exists == false)
	{
		$pdf->Cell(70,4,"Total watch units in first year:",1, 0, 'L', 0);	
	}
	else
	{
		$pdf->Cell(70,4,"Total watch units in first full year:",1, 0, 'L', 0);	
	}
	$pdf->Cell(20,4,$sd01,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	if($first_full_year_exists == false)
	{
		$pdf->Cell(70,4,"Gross Sales 1st year in KCHF:",1, 0, 'L', 0);	
	}
	else
	{
		$pdf->Cell(70,4,"Gross Sales 1st full year in KCHF:",1, 0, 'L', 0);	
	}
	$pdf->Cell(20,4,$sd02,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	if($first_full_year_exists == false)
	{
		$pdf->Cell(70,4,"Operating Income 1st year Retail:",1, 0, 'L', 0);	
	}
	else
	{
		$pdf->Cell(70,4,"Operating Income 1st full year Retail:",1, 0, 'L', 0);	
	}
	$pdf->Cell(20,4,$sd03,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	if($first_full_year_exists == false)
	{
		$pdf->Cell(70,4,"Operating Income 1st year WS: ",1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(70,4,"Operating Income 1st full year WS: ",1, 0, 'L', 0);	
	}
	$pdf->Cell(20,4,$sd04,1, 0, 'R', 0);
	$pdf->Ln();
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(90,4,"KeyData",1, 0, 'L', 1);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Deadline for property:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$deadline_for_property,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	
	
	if($project["project_projectkind"] == 3) // Take Over and Renovation
	{
		$pdf->Cell(70,4,"Planned take over date:",1, 0, 'L', 0);
		$pdf->Cell(20,4,$planned_takeover_date,1, 0, 'R', 0);
		$pdf->Ln();
		$pdf->setXY(107, $pdf->getY());
	}
	
	
	if($project["project_projectkind"] == 4) // Take Over
	{
		$pdf->Cell(70,4,"Planned take over date:",1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(70,4,"Planned opening date:",1, 0, 'L', 0);	
	}
	$pdf->Cell(20,4,$planned_opening_date,1, 0, 'R', 0);
	
	
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Keymoney in KCHF:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$keymoney_loc,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Goodwill in KCHF:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$goodwill_loc,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	//$pdf->Cell(70,4,"Deposit/Recoverable Keymoney in KCHF:",1, 0, 'L', 0);	
	$pdf->Cell(70,4,"Deposit in KCHF:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$depositposted,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Rent Free Period in Weeks:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$free_weeks,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Total rent 1st year in KCHF/K" . $currency_symbol . ":",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$fullrent_firstyear . "/" . $fullrent_firstyear_loc,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->setXY(107, $pdf->getY());
	
	if($project["project_projectkind"] == 2 
		or $project["project_projectkind"] == 3
		or $project["project_projectkind"] == 4
		or $project["project_projectkind"] == 5 )
	{
		$pdf->Cell(70,4,"Total rent last full year in KCHF/K" . $currency_symbol . ":",1, 0, 'L', 0);	
		$pdf->Cell(20,4,$fullrent_lastyear . "/" . $fullrent_lastyear_loc,1, 0, 'R', 0);
		$pdf->Ln();
	}
	elseif($project["project_projectkind"] == 6)
	{
		$pdf->Cell(70,4,"Total rent last full year in KCHF/K" . $currency_symbol . ":",1, 0, 'L', 0);	
		$pdf->Cell(20,4,$fullrent_lastyear . "/" . $fullrent_lastyear_loc,1, 0, 'R', 0);
		$pdf->Ln();
	}
	else
	{
		//$pdf->Cell(70,4,"",0, 0, 'L', 0);	
		//$pdf->Cell(20,4,"",0, 0, 'R', 0);
	}
	
	$pdf->Ln();


	$pdf->setXY(107, $pdf->getY());
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(90,4,"Contract Period:",1, 0, 'L', 1);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Contract Duration:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$lease_duration,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Contract Starting Date:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$contract_starting_date,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->setXY(107, $pdf->getY());
	$pdf->Cell(70,4,"Contract Ending Date:",1, 0, 'L', 0);	
	$pdf->Cell(20,4,$contract_ending_date,1, 0, 'R', 0);
	$pdf->Ln();



	$pdf->setX(10);



	if($picture_printed == false)
	{
		$pdf->setY($tmp_y+90);
	}
	else
	{
		if($tmp_y < $pdf->getY())
		{
			$pdf->setY($pdf->getY()+5);
		}
		else
		{		
			$pdf->setY($tmp_y+5);
		}
	}

	
	

	$pdf->setXY(10, $pdf->getY());
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(187,4,"Neighbourhood",1, 0, 'L', 1);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(30,4,"Left side:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$neighbourhoods["Shop on left side"],1, 0, 'L', 0);
	$pdf->Cell(90,4,"Other brands in the area:",1, 0, 'L', 0);
	$pdf->Ln();

	$tmp_y = $pdf->getY();

	$pdf->Cell(30,4,"Right side:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$neighbourhoods["Shop on right side"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->Cell(30,4,"Opposite Left:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$neighbourhoods["Shop across left side"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->Cell(30,4,"Opposite right:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$neighbourhoods["Shop across right side"],1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	$y = $pdf->getY();
	$pdf->setXY(107,$tmp_y);

	
	$pdf->MultiCell(90,12.5, $neighbourhoods["Other brands in area"], 1, "T", false, 1);
	$pdf->setXY(10, $pdf->getY()+2);

	

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(187,7,"Key points",1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->MultiCell(187,3.5, $ln_basicdata["ln_basicdata_remarks"], 1, "T");




	$y = $pdf->GetY()+5;
	$pdf->SetY($y);

	$x1 = $pdf->GetX();
	
	
	
	$pdf->SetFont('arialn','',9);
	$pdf->MultiCell(55,9,"Brand Manager" . "\r\n" . $brand_manager,1, 'T');
	$pdf->setXY($x+55, $pdf->getY()-9);
	$pdf->Cell(35,9,"" ,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->MultiCell(55,9,"Head of Controlling" . "\r\n" . $head_controlling,1, 'T');
	$pdf->setXY($x+55, $pdf->getY()-9);
	$pdf->Cell(35,9,"" ,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','',9);
	$pdf->MultiCell(55,9,"Country Manager" . "\r\n" . $country_manager,1, 'T');
	$pdf->setXY($x+55, $pdf->getY()-9);
	$pdf->Cell(35,9,"" ,1, 0, 'L', 0);
	$pdf->Ln();
	
	$x = 107;
	$pdf->setY($y);
	$pdf->setX($x);
	
	

	$pdf->SetFont('freesans','',7);

	$tmp_y = $pdf->getY($y);
	$tmp_x = $pdf->getX($x);

	
	$pdf->Cell(90,9,"" ,1, 'L');
	$pdf->setY($tmp_y);
	$pdf->setX($tmp_x);
	
	$pdf->MultiCell(55,9,"CEO" . "\r\n" . $ceo,1, 'T');

	$pdf->setX($x);
	$pdf->SetFont('freesans','',7);
	$pdf->MultiCell(55,9,"CFO" . "\r\n" . $cfo,1, 'T');
	
	$pdf->setXY($x+55, $pdf->getY()-9);
	$pdf->Cell(35,9,"" ,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->setX($x);
	$pdf->SetFont('freesans','',7);
	$pdf->MultiCell(55,9,"VP Sales" . "\r\n" . $vp_sales,1, 'T');
	$pdf->setXY($x+55, $pdf->getY()-9);
	$pdf->Cell(35,9,"" ,1, 0, 'L', 0);
	$pdf->Ln();
		
		
	$pdf->setX($x);
	$pdf->SetFont('freesans','',7);
	$pdf->MultiCell(55,9,"International Retail Manager" . "\r\n" . $retail_head,1, 'T');
	
	$pdf->setXY($x+55, $pdf->getY()-9);
	$pdf->Cell(35,9,"" ,1, 0, 'L', 0);
	$pdf->Ln();
	


	//page 2
	$pdf->AddPage("P", "A4");


	//Logo
	$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
	//arialn bold 15
	if(isset($version_date) and $version_date)
	{
		$pdf->SetFont('arialn','B',12);
		//Move to the right
		$pdf->Cell(40);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title . $version_date,0,0,'R');
		//Line break
		$pdf->SetY(23);
	}
	else
	{
		$pdf->SetFont('arialn','B',18);
		//Move to the right
		$pdf->Cell(80);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title,0,0,'R');
		//Line break
		$pdf->SetY(23);
	}

	$pdf->setXY(10, $pdf->getY());
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(97,4,"Address Data",1, 0, 'L', 1);
	$pdf->Cell(90,4,"Location Data",1, 0, 'L', 1);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(30,4,"POS Name:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$posname,1, 0, 'L', 0);
	//$pdf->Cell(40,4,"Environment:",1, 0, 'L', 0);
	$pdf->Cell(90,4,$posareas,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"Country:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$country_name,1, 0, 'L', 0);
	$pdf->Cell(25,4,"Placement:",1, 0, 'L', 0);
	$pdf->Cell(65,4,$placement,1, 0, 'L', 0);
	$pdf->Ln();

	//$tmp = "Gross/Total/Sales: " . $gross_surface . " / " .$total_surface . " / " . $sales_surface;
	$tmp = "Total/Sales: "  .$total_surface . " / " . $sales_surface;
	$pdf->Cell(30,4,"City:",1, 0, 'L', 0);	
	$pdf->Cell(67,4,$city_name,1, 0, 'L', 0);
	$pdf->Cell(25,4,"Surfaces in sqms: ",1, 0, 'L', 0);
	$pdf->Cell(65,4,$tmp,1, 0, 'L', 0);
	$pdf->Ln();

	
	

	if($surface_of_relocated_pos  != "")
	{
		$pdf->Cell(30,4,"Pos Type:",1, 0, 'L', 0);	
		$pdf->Cell(67,4,$postype,1, 0, 'L', 0);
		
		$pdf->Cell(25,4," -> Relocated: ",1, 0, 'L', 0);
		$pdf->Cell(65,4,"Gross/Total/Sales: " .  $surface_of_relocated_pos,1, 0, 'L', 0);
		$pdf->Ln();

		$pdf->Cell(30,4,"",1, 0, 'L', 0);	
		$pdf->Cell(67,4,"",1, 0, 'L', 0);
		$pdf->Cell(25,4,"Project Type:",1, 0, 'L', 0);
		$pdf->Cell(65,4,$project_kind,1, 0, 'L', 0);
		$pdf->Ln();
	}
	else
	{
		$pdf->Cell(30,4,"Pos Type:",1, 0, 'L', 0);	
		$pdf->Cell(67,4,$postype,1, 0, 'L', 0);
		$pdf->Cell(25,4,"Project Type:",1, 0, 'L', 0);
		$pdf->Cell(65,4,$project_kind,1, 0, 'L', 0);
		$pdf->Ln();
	}

	


	$x = $pdf->GetX();
	$y = $pdf->GetY() + 5;

	$pdf->setXY($x,$y);


	
	//Sales data
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(97,5,"Sales Data",1, 0, 'L', 1);
	for($i=0;$i<4;$i++)
	{
		if(array_key_exists($i, $years))
		{
			if($i == 0)
			{
				$pdf->Cell(22.5,5,$years[$i] . "(" . $number_of_months_first_year . ")",1, 0, 'R', 0);
			}
			elseif($i == 3)
			{
				
				if(count($years) == 4)
				{
					$pdf->Cell(22.5,5,$years[$i] . "(" . $number_of_months_last_year . ")",1, 0, 'R', 0);
				}
				else
				{
					$pdf->Cell(22.5,5,$years[$i],1, 0, 'R', 0);
				}
			}
			else
			{
				$pdf->Cell(22.5,5,$years[$i],1, 0, 'R', 0);
			}
		}
		else
		{
			$pdf->Cell(22.5,5," ",1, 0, 'R', 0);
		}
	}
	
	$pdf->Ln();

	
	$x = $pdf->GetX();
	$pdf->Cell(187,25,"",1, 0, 'L', 0);
	$pdf->SetX($x);

	$pdf->Cell(97,10," ",0, 0, 'R', 0);
	for($i=0;$i<4;$i++)
	{
		$pdf->Cell(22.5,10," ",1, 0, 'R', 0);
	}
	

	$pdf->SetX($x);

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(97,5,"Total Gross Sales in KCHF" ,0, 0, 'L', 0);

	for($i=0;$i<4;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $total_gross_sales_values))
		{
			$pdf->Cell(22.5,5,number_format(round($e_rate*$total_gross_sales_values[$years[$i]]/1000,0), 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(22.5,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	
	$pdf->Cell(97,5,"Total Gross Margin in KCHF",0, 0, 'L', 0);
	for($i=0;$i<4;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $total_gross_margin_values))
		{
			$pdf->Cell(22.5,5,number_format(round($e_rate*$total_gross_margin_values[$years[$i]]/1000,0), 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(22.5,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	$pdf->Cell(97,5,"Total Indirect Expenses in KCHF",1, 0, 'L', 0);
	for($i=0;$i<4;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $total_indirect_expenses_values))
		{
			$pdf->Cell(22.5,5,number_format(round(-1*$e_rate*$total_indirect_expenses_values[$years[$i]]/1000,0), 0, ".", "'"),1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(22.5,5," ",1, 0, 'R', 0);
		}
	}
	$pdf->Ln();


	
	
	$pdf->Cell(97,5," ",0, 0, 'R', 0);
	for($i=0;$i<4;$i++)
	{
		$pdf->Cell(22.5,10," ",1, 0, 'R', 0);
	}

	$pdf->SetX($x);

	$pdf->Cell(97,5,"Operating Income without WS Margin in KCHF",0, 0, 'L', 0);
	for($i=0;$i<4;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $operating_income01_values))
		{
			$pdf->Cell(22.5,5,number_format(round($e_rate*$operating_income01_values[$years[$i]]/1000,0), 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(22.5,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();


	
	

	$pdf->Cell(97,5,"Operating Income incl. WS Margin (" . $cer_basicdata["cer_basicdata_wholesale_margin"] . "%) in KCHF",0, 0, 'L', 0);
	for($i=0;$i<4;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $operating_income02_values))
		{
			$pdf->Cell(22.5,5,number_format(round($e_rate*$operating_income02_values[$years[$i]]/1000,0), 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(22.5,5," ",0, 0, 'R', 0);
		}
	}


	$pdf->Ln();


	
	if($apply_80_percent_scenario == 1)
	{
		$pdf->Cell(97,5," ",1, 0, 'R', 0);
		for($i=0;$i<4;$i++)
		{
			$pdf->Cell(22.5,5," ",1, 0, 'R', 0);
		}

		$pdf->SetX($x);

		

		$pdf->Cell(97,5,"Operating Income incl. WS Margin in KCHF (scenario 80%)",0, 0, 'L', 0);
		for($i=0;$i<4;$i++)
		{
			if(array_key_exists($i, $years) and array_key_exists($years[$i], $operating_income02_80_percent_values))
			{
				$pdf->Cell(22.5,5,number_format(round($e_rate*$operating_income02_80_percent_values[$years[$i]]/1000,0), 0, ".", "'"),0, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(22.5,5," ",0, 0, 'R', 0);
			}
		}

		$pdf->Ln();
	}

	$pdf->Ln();


	//estimates sellouts

	//check if there are sellouts for the first year
	$first_year_has_sellouts = false;
	if(array_key_exists(0, $years) and array_key_exists($years[0], $sales_units_watches_values) and $sales_units_watches_values[$years[0]] > 0)
	{
		$first_year_has_sellouts = true;
	}

	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(97,5,"Estimated sell out (planned units)",1, 0, 'L', 1);
	
	$i_year = 0;
	if($first_year_has_sellouts == false)
	{
		$i_year = 1;
	}
	for($i=$i_year;$i<($i_year+4);$i++)
	{
		if(array_key_exists($i, $years))
		{
			if($i == 0)
			{
				$pdf->Cell(22.5,5,$years[$i] . "(" . $number_of_months_first_year . ")",1, 0, 'R', 0);
			}
			
			elseif($i == 3 and count($years) == ($i+1))
			{
				if(count($years) == 4)
				{
					$pdf->Cell(22.5,5,$years[$i] . "(" . $number_of_months_last_year . ")",1, 0, 'R', 0);
				}
				else
				{
					$pdf->Cell(22.5,5,$years[$i],1, 0, 'R', 0);
				}
				
			}
			else
			{
				$pdf->Cell(22.5,5,$years[$i],1, 0, 'R', 0);
			}
		}
		else
		{
			$pdf->Cell(22.5,5," ",1, 0, 'R', 0);
		}
	}
	
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$x = $pdf->GetX();
	$pdf->Cell(187,10,"",1, 0, 'L', 0);
	$pdf->SetX($x);

	$pdf->Cell(97,10," ",1, 0, 'R', 0);
	for($i=$i_year;$i<($i_year+4);$i++)
	{
		$pdf->Cell(22.5,10," ",1, 0, 'R', 0);
	}

	$pdf->SetX($x);

	$pdf->Cell(97,5,"Watches",1, 0, 'L', 0);
	
	for($i=$i_year;$i<($i_year+4);$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $sales_units_watches_values) and $sales_units_watches_values[$years[$i]] > 0)
		{
			$pdf->Cell(22.5,5,number_format($sales_units_watches_values[$years[$i]], 0, ".", "'"),1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(22.5,5," ",1, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	$pdf->Cell(97,5,"Bjoux",1, 0, 'L', 0);
	for($i=$i_year;$i<($i_year+4);$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $sales_units_jewellery_values) and $sales_units_jewellery_values[$years[$i]] > 0)
		{
			$pdf->Cell(22.5,5,number_format($sales_units_jewellery_values[$years[$i]], 0, ".", "'"),1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(22.5,5," ",1, 0, 'R', 0);
		}
	}
	$pdf->Ln();
	$pdf->Ln();


	$x = $pdf->GetX();
	$pdf->Cell(97,17,"",1, 0, 'L', 0);
	$pdf->SetX($x);
	
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(97,7,"Total Investment in fixed assets in KCHF",1, 0, 'L', 1);
	$pdf->SetFont('arialn','',9);
	$pdf->Cell(22.5,7,number_format(round($e_rate*$investment_total/1000,0), 0, ".", "'"),1, 0, 'R', 0);

	$pdf->Ln();
    
	
	foreach($fixed_assets as $key=>$itype)
	{
		if($itype == 1 and array_key_exists($itype, $investment_names))
		{
			$pdf->Cell(97,5," of which - " . $investment_names[$itype] . " in KCHF",0, 0, 'L', 0);
			$pdf->Cell(22.5,5,number_format(round($e_rate*$amounts[$itype] / 1000,0), 0, ".", "'"),1, 0, 'R', 0);
			$pdf->Ln();
		}
		if($itype == 3 and array_key_exists($itype, $investment_names))
		{
			$pdf->Cell(97,5," of which - " . $investment_names[$itype] . " in KCHF",0, 0, 'L', 0);
			$pdf->Cell(22.5,5,number_format(round($e_rate*$amounts[$itype] / 1000,0), 0, ".", "'"),1, 0, 'R', 0);
			$pdf->Ln();
		}
		
	    
	}

	$pdf->setXY(10, $pdf->getY()+4);


	//watches sold out in the past
	// renovation or tekover/renovation or lease renewal or new relocation project
	if(($project["project_projectkind"] == 2 
        or $project["project_projectkind"] == 3
		or $project["project_projectkind"] == 4
		or $project["project_projectkind"] == 5)
		or ($project["project_projectkind"] == 6 and $project["project_relocated_posaddress_id"] > 0)
	   )

	{
	
		$sellout_ending_year = (int)substr($cer_basicdata["date_created"], 0, 4);
		if($sellout_ending_year < (int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4))
		{
			$sellout_ending_year =(int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4);
		}


		if($sellout_ending_year > date("Y"))
		{
			$sellout_ending_year = date("Y");
		}

		$sellout_starting_year = $sellout_ending_year - 3;

		$pdf->SetFont('arialn','B',9);
		$pdf->Cell(97,5,"Historical sell out (sold units)",1, 0, 'L', 1);

		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
		{
			if(array_key_exists($i, $sellouts_watches))
			{
				$pdf->Cell(22.5,5,$i . "(" . $sellouts_months[$i] . ")",1, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(22.5,5,$i,1, 0, 'R', 0);
			}
		}
		
		
		$pdf->Ln();

		$pdf->SetFont('arialn','',9);
		$x = $pdf->GetX();
		$pdf->Cell(187,10,"",1, 0, 'L', 0);
		$pdf->SetX($x);

		$pdf->Cell(97,10," ",1, 0, 'R', 0);
		for($i=0;$i<2;$i++)
		{
			$pdf->Cell(22.5,10," ",1, 0, 'R', 0);
		}

		$pdf->SetX($x);

		$pdf->Cell(97,5,"Watches",1, 0, 'L', 0);
		
		$pdf->SetFont('arialn','',9);


		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
		{
			if(array_key_exists($i, $sellouts_watches))
			{
				$pdf->Cell(22.5,5,number_format($sellouts_watches[$i], 0, ".", "'"),1, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(22.5,5," ",1, 0, 'R', 0);
			}
		}
		$pdf->Ln();

		$pdf->SetFont('arialn','B',9);
		$pdf->Cell(97,5,"Bjoux",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',9);
		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
			{
			if(array_key_exists($i, $sellouts_bjoux))
			{
				$pdf->Cell(22.5,5,number_format($sellouts_bjoux[$i], 0, ".", "'"),1, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(22.5,5," ",1, 0, 'R', 0);
			}
		}
		$pdf->Ln();
		$pdf->Ln();
	}
	

	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(187,4,"Rental conditions",1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',9);
	$pdf->MultiCell(187,3.5, $negotiated_rental_conditions, 1, "T");

	$pdf->setXY(10, $pdf->getY()+4);
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(97,4,"Rental Contract",1, 0, 'L', 1);
	$pdf->Cell(90,4,"Additional rental costs",1, 0, 'L', 1);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(50,4,"Renewal Option in  years:",1, 0, 'L', 0);
	$pdf->Cell(47,4,$renewal_option,1, 0, 'L', 0);
	$pdf->Cell(45,4,"Index rate in %:",1, 0, 'L', 0);
	$pdf->Cell(45,4,$indexrate,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(50,4,"Exit option date:",1, 0, 'L', 0);
	$pdf->Cell(47,4,$exit_option,1, 0, 'L', 0);
	$pdf->Cell(45,4,"Yearly increase in %:",1, 0, 'L', 0);
	$pdf->Cell(45,4,$average_yearly_increase,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(50,4,"Notice Period in months:",1, 0, 'L', 0);
	$pdf->Cell(47,4,$termination_time,1, 0, 'L', 0);
	$pdf->Cell(45,4,"Real estate fees in % of KM:",1, 0, 'L', 0);
	$pdf->Cell(45,4,$real_estate_fee,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(50,4,"Index clause:",1, 0, 'L', 0);
	$pdf->Cell(47,4,$index_clause,1, 0, 'L', 0);
	$pdf->Cell(45,4,"Annual charges in KCHF:",1, 0, 'L', 0);
	$pdf->Cell(45,4,$annual_charges,1, 0, 'R', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(50,4,"Tacit renewal clause / duration:",1, 0, 'L', 0);
	$pdf->Cell(47,4,$index_clause2,1, 0, 'L', 0);
	$pdf->Cell(45,4,"Other fees:",1, 0, 'L', 0);
	$pdf->Cell(45,4,$other_fees,1, 0, 'R', 0);
	$pdf->Ln();

	if($project["project_projectkind"] == 6) // relocation
	{
		$pdf->setXY(10, $pdf->getY()+4);
		$pdf->SetFont('arialn','B',9);
		$pdf->Cell(187,4,"Relocation Information",1, 0, 'L', 1);
		$pdf->Ln();
		$pdf->SetFont('arialn','',9);
		$pdf->MultiCell(187,3.5, $relocation_info, 1, "T");
	}

	if(param("hr") == 1)
	{
		$pdf->setXY(10, $pdf->getY()+4);
		$pdf->SetFont('arialn','B',9);
		$pdf->Cell(187,4,"Human Resources (CHF)",1, 0, 'L', 1);
		$pdf->Ln();

		//table headers
		$pdf->SetFont('arialn','B',9);
		$pdf->Cell(25,4,"Function",1, 0, 'L', 0);
		$pdf->Cell(15,4,"from Year",1, 0, 'R', 0);
		$pdf->Cell(15,4,"Month",1, 0, 'R', 0);
		$pdf->Cell(12,4,"FTE%",1, 0, 'R', 0);
		$pdf->Cell(20,4,"Fixed Salary",1, 0, 'R', 0);
		$pdf->Cell(20,4,"Bonus",1, 0, 'R', 0);
		$pdf->Cell(20,4,"Other Salary",1, 0, 'R', 0);
		$pdf->Cell(20,4,"Social Charges",1, 0, 'R', 0);
		$pdf->Cell(20,4,"Total Cost",1, 0, 'R', 0);
		$pdf->Cell(20,4,"Part Time Cost",1, 0, 'R', 0);
		$pdf->Ln();
		
		$t_fte = 0;
		$t_fixed_salary = 0;
		$t_salary_bonus = 0;
		$t_salary_other = 0;
		$t_salary_social_charges = 0;
		$t_total = 0;
		$t_part_time = 0;

		foreach($human_resources as $key=>$human_resource)
		{
			$total = $human_resource["cer_salary_fixed_salary"] + $human_resource["cer_salary_bonus"] + $human_resource["cer_salary_other"] + $human_resource["cer_salary_social_charges"];
			$part_time = round($total * $human_resource["cer_salary_headcount_percent"] / 100, 2);
			
			$pdf->SetFont('arialn','',9);
			$pdf->Cell(25,4,$human_resource["cer_staff_type_name"],1, 0, 'L', 0);
			$pdf->Cell(15,4,$human_resource["cer_salary_year_starting"],1, 0, 'R', 0);
			$pdf->Cell(15,4,$human_resource["cer_salary_month_starting"],1, 0, 'R', 0);
			$pdf->Cell(12,4,$human_resource["cer_salary_headcount_percent"],1, 0, 'R', 0);
			$pdf->Cell(20,4,$human_resource["cer_salary_fixed_salary"],1, 0, 'R', 0);
			$pdf->Cell(20,4,$human_resource["cer_salary_bonus"],1, 0, 'R', 0);
			$pdf->Cell(20,4,$human_resource["cer_salary_other"],1, 0, 'R', 0);
			$pdf->Cell(20,4,$human_resource["cer_salary_social_charges"],1, 0, 'R', 0);
			$pdf->Cell(20,4,$total,1, 0, 'R', 0);
			$pdf->Cell(20,4,$part_time,1, 0, 'R', 0);
			$pdf->Ln();


			$t_fte = $t_fte + $human_resource["cer_salary_headcount_percent"];
			$t_fixed_salary = $t_fixed_salary + $human_resource["cer_salary_fixed_salary"];
			$t_salary_bonus = $t_salary_bonus + $human_resource["cer_salary_bonus"];
			$t_salary_other = $t_salary_other + $human_resource["cer_salary_other"];
			$t_salary_social_charges = $t_salary_social_charges + $human_resource["cer_salary_social_charges"];
			$t_total = $t_total + $total;
			$t_part_time = $t_part_time + $part_time;
		}


		if($t_fte > 0)
		{
		
			$pdf->SetFont('arialn','B',9);
			$pdf->Cell(25,4,"Totals",1, 0, 'L', 0);
			$pdf->Cell(15,4,"",1, 0, 'R', 0);
			$pdf->Cell(15,4,"",1, 0, 'R', 0);
			$pdf->Cell(12,4,number_format($t_fte, 2, ".", "'"),1, 0, 'R', 0);
			$pdf->Cell(20,4,$t_fixed_salary,1, 0, 'R', 0);
			$pdf->Cell(20,4,$t_salary_bonus,1, 0, 'R', 0);
			$pdf->Cell(20,4,$t_salary_other,1, 0, 'R', 0);
			$pdf->Cell(20,4,$t_salary_social_charges,1, 0, 'R', 0);
			$pdf->Cell(20,4,$t_total,1, 0, 'R', 0);
			$pdf->Cell(20,4,$t_part_time,1, 0, 'R', 0);
			$pdf->Ln();
		
		}
	}


?>