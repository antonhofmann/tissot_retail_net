<?php

if($posdata["posaddress_google_lat"] and $posdata["posaddress_google_long"])
{ 
	$latitude = $posdata["posaddress_google_lat"];
	$longitude = $posdata["posaddress_google_long"];

	//insert google map
	$pdf->AddPage("L");
	
	// Title first line
	$pdf->SetXY($margin_left,$margin_top);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(270, 8, "  City Map: " . $project["country_name"] . " - " . $pos_data["place_name"] . " - " . $pos_data["posaddress_name"] . " - Project " . $project['project_number'], 1, "", "L");

	$pdf->setY(25);

	
	
	$url = STATIC_MAPS_HOST;
	$url .= '?center=' . $latitude . ',' . $longitude;
	$url .= '&zoom=16';
	$url .= '&size=640x640';
	$url .= '&maptype=roadmap' . "\\";
	$url .= '&markers=color:red%7Clabel:P%7C' . $latitude . ',' . $longitude;
	$url .= '&format=jpg';
	$url .= '&key=' . GOOGLE_API_KEY;
	$url .= '&sensor=false';

	
	$tmpfilename1 = "map1" . time() . ".jpg";

	$mapImage1 = '';
	if(!$context) {
		
		$mapImage1 = file_get_contents($url) or $mapImage1 = '';
	}
	else
	{
		$mapImage1 = file_get_contents($url, false, $context) or $mapImage1 = '';
	}
	
	if($mapImage1) {
		$fh = fopen(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1, 'w') or $mapImage1 = '';
		fwrite($fh, $mapImage1);
		fclose($fh);
		
		if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1)) {
			$pdf->Image(TMP_FILE_DIR_RELATIVE . $tmpfilename1, $margin_left, $pdf->getY(), 130, 130);
		}
	}

	
	$url = STATIC_MAPS_HOST;
	$url .= '?center=' . $latitude . ',' . $longitude;
	$url .= '&zoom=6';
	$url .= '&size=640x640';
	$url .= '&maptype=roadmap' . "\\";
	$url .= '&markers=color:red%7Clabel:P%7C' . $latitude . ',' . $longitude;
	$url .= '&format=jpg';
	$url .= '&key=' . GOOGLE_API_KEY;
	$url .= '&sensor=false';

	$tmpfilename2 = "map2" . time() . ".jpg";
	
	$mapImage2 = '';
	if(!$context) {
		
		$mapImage2 = file_get_contents($url) or $mapImage2 = '';
	}
	else
	{
		$mapImage2 = file_get_contents($url, false, $context) or $mapImage2 = '';
	}

	if($mapImage2) {

		$fh = fopen(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2, 'w') $mapImage2 = '';
		fwrite($fh, $mapImage2);
		fclose($fh);
		
		if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2)) {
			$pdf->Image(TMP_FILE_DIR_RELATIVE . $tmpfilename2, 152, $pdf->getY(), 130, 130);
		}
	}

	if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1))
	{
		unlink(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1);
	}

	if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2))
	{
		unlink(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2);
	}
}
?>