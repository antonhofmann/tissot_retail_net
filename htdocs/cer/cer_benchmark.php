<?php
/********************************************************************

    cer_benchmark

    Enter filter criteria for cer benchmark

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-13
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2007-12-07
    Version:        1.1.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";


check_access("has_access_to_cer_benchmarks");

/********************************************************************
    prepare all data needed
*********************************************************************/

$icon = "../pictures/add_item.gif";

$user = get_user(user_id());

// create sql for filter criteria
$sql_salesregion = "select salesregion_id, salesregion_name ".
               "from salesregions order by salesregion_name";

$sql_country = "select DISTINCT country_id, country_name " .
               "from posaddresses " .
			   "left join countries on country_id = posaddress_country " . 
			   "order by country_name";

$sql_cities = "select DISTINCT posaddress_place ".
              "from posaddresses order by posaddress_place";

$sql_areas = "select posareatype_id, posareatype_name ".
              "from posareatypes order by posareatype_name";

$sql_product_lines = "select product_line_id, product_line_name ".
                     "from product_lines " . 
                     "where product_line_mis = 1 " . 
                     "   order by product_line_name";

$sql_product_line_subclasses = "select productline_subclass_id, productline_subclass_name ".
						 "from productline_subclasses " . 
						 "   order by productline_subclass_name";
					

$sql_pos_types = "select DISTINCT postype_name ".
                     "from postypes " .
                     "order by postype_name";

$sql_possubclasses = "select possubclass_id, possubclass_name ".
                     "from possubclasses " .
                     "order by possubclass_name";

$sql_project_cost_types = "select project_costtype_id, project_costtype_text ".
						  "from project_costtypes " .
                          "order by project_costtype_text";

$sql_years = "select DISTINCT YEAR(order_date) as year1, YEAR(order_date) as year2 " .
             "from orders " . 
			 "where order_type = 1 " . 
			 "order by year1";

$sql_order_states = "select order_state_code, concat(order_state_code, ' ', order_state_name) as name " .
                    "from order_states ". 
					"left join order_state_groups on order_state_group_id = order_state_group " . 
					"where order_state_group_order_type = 1 and order_state_code < '910' " . 
					"order by order_state_code";



$sqm_ranges = array();
$sqm_ranges[1] = "1 - 15 sqms";
$sqm_ranges[2] = "16 - 30 sqms";
$sqm_ranges[3] = "31 - 45 sqms";
$sqm_ranges[4] = "46 - 60 sqms";
$sqm_ranges[5] = "61 - 75 sqms";
$sqm_ranges[6] = "76 - 100 sqms";
$sqm_ranges[7] = "100 and more sqms";


//prepare data for access
$access_roles = array();
$sql_access_roles = "select role_id from roles where role_include_in_queryselection_cer = 1";
$res = mysql_query($sql_access_roles) or dberror($sql_access_roles);
while ($row = mysql_fetch_assoc($res))
{
	$access_roles[] = $row["role_id"];
}

if(count($access_roles) > 0)
{
	$sql_persons_hq = "select distinct user_id, concat(user_name, ' ' , user_firstname, '(', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " . 
		   "where user_id = 2 or (address_id = 13 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . ")) " . 
		   "order by username";

	$sql_persons_sub = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, '(', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   " left join countries on country_id = address_country " . 
		   "where address_id <> 13 and address_client_type in (2, 3) and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . ") " . 
		   "order by country_name, username";

	$sql_persons_ag = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, '(', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		    " left join countries on country_id = address_country " .  
		   "where address_id <> 13 and address_client_type = 1 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . ") " . 
		   "order by country_name, username";
}
else
{
	$sql_persons_hq = "select distinct user_id, concat(user_name, ' ' , user_firstname, ' (', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   "where user_id = 2 or (address_id = 13  and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and user_address = " . $user["address"] . ") " . 
		   "order by username";

	$sql_persons_sub = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, ' (', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   " left join countries on country_id = address_country " . 
		   "where address_id <> 13 and address_client_type in (2, 3) and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and user_address = " . $user["address"] . " " . 
		   "order by country_name, username";

	$sql_persons_ag = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, ' (', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   " left join countries on country_id = address_country " . 
		   "where address_id <> 13 and address_client_type = 1 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and user_address = " . $user["address"] . " " . 
		   "order by country_name, username";
}



$sql_project_kinds = "select projectkind_id, projectkind_name ".
                     "from projectkinds " . 
                     "   order by projectkind_name";

$base_currency = array();
$base_currency["local"] = "Local Currency";
$base_currency["chf"] = "Swiss Francs";

//get benchmark parameters
$salesregions = array();
$countries = array();
$cities = array();
$areas = array();
$product_lines = array();
$product_line_subclasses = array();
$pos_types = array();
$subclasses = array();
$project_kinds = array();
$selected_sqms = "";

if(id() > 0)
{
	//check if filter is present
	$sql = "select * from cer_benchmarks " .
		   "where cer_benchmark_id = " . id();

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		if(!$row["cer_benchmark_filter"])
		{
			$filter = array();
			$filter["re"] =  "";
			$filter["co"] =  "";
			$filter["ci"] =  "";
			$filter["ar"] =  "";
			$filter["pct"] =  "";
			$filter["pl"] =  "";
			$filter["pls"] =  "";
			$filter["pt"] =  "";
			$filter["sc"] =  "";
			$filter["fs"] =  "";
			$filter["ts"] =  "";
			$filter["pkinds"] =  "";

			$sql = "update cer_benchmarks " . 
				   "set cer_benchmark_filter = " . dbquote(serialize($filter)) . 
				   " where cer_benchmark_id = " . param("id");

			$result = mysql_query($sql) or dberror($sql);
		}
	}
	
	$sql = "select * from cer_benchmarks " .
		   "where cer_benchmark_id = " . id();

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$filter = array();
		$filter = unserialize($row["cer_benchmark_filter"]);

		$salesregions = explode("-", $filter["re"]);
		$countries = explode("-", $filter["co"]);
		$cities = explode("-", $filter["ci"]);
		$areas = explode("-", $filter["ar"]);
		$project_cost_types = explode("-", $filter["pct"]);
		$product_lines = explode("-", $filter["pl"]);
		
		if(array_key_exists("pls", $filter)) {
			$product_line_subclasses = explode("-", $filter["pls"]);
		}

		$pos_types = explode("-", $filter["pt"]);
		$subclasses = explode("-", $filter["sc"]);

		if(array_key_exists("pkinds", $filter)) {
			$project_kinds = explode("-", $filter["pkinds"]);
		}
	
		$selected_sqms = $row["cer_benchmark_selected_sqms"];
	}
}






$qgroups = array();
$query_groups = "select DISTINCT cer_benchmark_id, cer_benchmark_group " . 
			   "from cer_benchmarks " . 
			   " where cer_benchmark_group<> '' and cer_benchmark_user_id = " . user_id() . 
			   " order by cer_benchmark_group";
$res = mysql_query($query_groups) or dberror($query_groups);
while ($row = mysql_fetch_assoc($res))
{
	if(!in_array($row["cer_benchmark_group"], $qgroups))
	{
		$qgroups[$row["cer_benchmark_id"]] = $row["cer_benchmark_group"];
	}
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("cer_benchmarks", "cer_benchmarks");

$form->add_section("Benchmark");
//$form->add_edit("cer_benchmark_shortcut", "Shortcut*", NOTNULL);
$form->add_edit("cer_benchmark_title", "Title*", NOTNULL);
$form->add_list("cer_benchmark_basic_currency", "Basic Currency*", $base_currency, 0);


$form->add_section("Group of Benchmarks");
$form->add_comment("Please select to which group the benchmark belongs or enter a new group.");
$form->add_list("existing_groups", "Existing Groups", $qgroups, SUBMIT);
$form->add_edit("cer_benchmark_group", "Group");


$link = "javascript:open_selector('')";
if(id() > 0)
{
	$form->add_Section("Selected Filter Criteria");
	$selected_salesregions = "";
	$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["salesregion_id"], $salesregions))
		{
			$selected_salesregions .= $row["salesregion_name"] . ", ";
		}
	}
	$selected_salesregions = substr($selected_salesregions, 0, strlen($selected_salesregions) - 2);
	$form->add_label_selector("salesregions", "Regions", 0, $selected_salesregions, $icon, $link);


	$selected_countries = "";
	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["country_id"], $countries))
		{
			$selected_countries .= $row["country_name"] . ", ";
		}
	}
	$selected_countries = substr($selected_countries, 0, strlen($selected_countries) - 2);
	$form->add_label_selector("countries", "Countries", 0, $selected_countries, $icon, $link);

	$selected_cities = "";
	$res = mysql_query($sql_cities) or dberror($sql_cities);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array(trim($row["posaddress_place"]), $cities))
		{
			$selected_cities .= $row["posaddress_place"] . ", ";
		}
	}
	$selected_cities = substr($selected_cities, 0, strlen($selected_cities) - 2);
	$form->add_label_selector("cities", "Cities", 0, $selected_cities, $icon, $link);


	$selected_areas = "";
	$res = mysql_query($sql_areas) or dberror($sql_areas);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["posareatype_id"], $areas))
		{
			$selected_areas .= $row["posareatype_name"] . ", ";
		}
	}
	$selected_areas = substr($selected_areas, 0, strlen($selected_areas) - 2);
	$form->add_label_selector("areas", "Areas", 0, $selected_areas, $icon, $link);

	
	$selected_pcts = "";
	$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["project_costtype_id"], $project_cost_types))
		{
			$selected_pcts .= $row["project_costtype_text"] . ", ";
		}
	}
	$selected_pcts = substr($selected_pcts, 0, strlen($selected_pcts) - 2);
	$form->add_label_selector("pcts", "Legal Types", 0, $selected_pcts, $icon, $link);

	
	$selected_pkinds = "";
	$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["projectkind_id"], $project_kinds))
		{
			$selected_pkinds .= $row["projectkind_name"] . ", ";
		}
	}
	$selected_pkinds = substr($selected_pkinds, 0, strlen($selected_pkinds) - 2);
	$form->add_label_selector("pkinds", "Project Types", 0, $selected_pkinds, $icon, $link);


	$selected_pls = "";
	$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["product_line_id"], $product_lines))
		{
			$selected_pls .= $row["product_line_name"] . ", ";
		}
	}
	$selected_pls = substr($selected_pls, 0, strlen($selected_pls) - 2);
	$form->add_label_selector("pls", "Product Lines", 0, $selected_pls, $icon, $link);



	$selected_plss = "";
	$res = mysql_query($sql_product_line_subclasses) or dberror($sql_product_line_subclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["productline_subclass_id"], $product_line_subclasses))
		{
			$selected_plss .= $row["productline_subclass_name"] . ", ";
		}
	}
	$selected_plss = substr($selected_plss, 0, strlen($selected_plss) - 2);
	$form->add_label_selector("plss", "Product Line Subclasses", 0, $selected_plss, $icon, $link);


	$selected_pts = "";
	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["postype_name"], $pos_types))
		{
			$selected_pts .= $row["postype_name"] . ", ";
		}
	}
	$selected_pts = substr($selected_pts, 0, strlen($selected_pts) - 2);
	$form->add_label_selector("pts", "POS Types", 0, $selected_pts, $icon, $link);

	$selected_subclasses = "";
	$res = mysql_query($sql_possubclasses) or dberror($sql_possubclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["possubclass_id"], $subclasses))
		{
			$selected_subclasses .= $row["possubclass_name"] . ", ";
		}
	}
	$selected_subclasses = substr($selected_subclasses, 0, strlen($selected_subclasses) - 2);
	$form->add_label_selector("subclasses", "POS Type Subclass", 0, $selected_subclasses, $icon, $link);


	$form->add_Section("Selected Projects");
	$form->add_list("cer_benchmark_from_state", "From State", $sql_order_states, 0);
	$form->add_list("cer_benchmark_to_state", "To State", $sql_order_states, 0);
	
	$form->add_comment("Enter project numbers separated by a semicolon in case you want to have only certain projects in your benchmark");
	$form->add_multiline("cer_benchmark_project_filter", "Project Numbers", 4, 0, "", 1, "project_info");
	
	
	$form->add_comment("Enter a project number in case you like to compare the selected projects with a specific project.");
	$form->add_edit("cer_benchmark_compare_with_project", "Compare average with project");
	
	$form->add_Section("Other Criteria");
	$form->add_checkbox("cer_benchmark_investments", "show only investments as indicated in the LNR", "", "", "Investments");
	$form->add_Section(" ");

	$form->add_checkbox("cer_benchmark_last_project", "include only the latest project of each POS", "", "", "Projects");
	$form->add_Section(" ");
	$form->add_list("cer_benchmark_project_state", "Project State", "select project_state_id, project_state_text from project_states", 0);
	$form->add_Section(" ");
	$form->add_list("cer_benchmark_from_year", "From Year", $sql_years, 0);
	$form->add_list("cer_benchmark_to_year", "To Year", $sql_years, 0);

	$form->add_list("cer_benchmark_selected_sqms", "Total Area in sqms", $sqm_ranges, 0, $selected_sqms);

	$form->add_Section(" ");

	$form->add_Section("Project Pipeline");
	$form->add_comment("The pipeline contains all the projects still going on in retail net (not having an actual shop opening date).");
	$form->add_checkbox("cer_benchmark_include_pipeline", "include projects in the pipeline", "", "", "Pipeline");
}

$form->add_hidden("id", id());

if(id() == 0) {
	$form->add_hidden("cer_benchmark_user_id", user_id(), 0);
}


$form->add_section("Access Rights");
$form->add_comment("The following persons can have access to my queries");
$form->add_checklist("Persons0", "Persons HQ", "cer_benchmarkpermissions", $sql_persons_hq, 0, "", true );
$form->add_checklist("Persons1", "Subs and Affiliates", "cer_benchmarkpermissions", $sql_persons_sub, 0, "", true );
$form->add_checklist("Persons2", "Agents", "cer_benchmarkpermissions", $sql_persons_ag, 0, "", true );


$form->add_button("save", "Save Benchmark");
$form->add_button(FORM_BUTTON_DELETE, "Delete Benchmark");
$form->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

if($form->button("existing_groups"))
{
	$form->value("cer_benchmark_group", $qgroups[$form->value("existing_groups")]);
	$form->value("existing_groups", "");
}
elseif($form->button("save"))
{
	//validate project numbers

	$form->add_validation("{cer_benchmark_basic_currency} != ''", "Basic currency must be indicated!");
	
	if($form->validate()) {
		$error = 0;
		$error_string = "";

		if(isset($form->items["cer_benchmark_project_filter"]))
		{
			$project_numbers = trim($form->value("cer_benchmark_project_filter"));
			$projects = explode(";",$project_numbers);
			foreach($projects as $key=>$value)
			{
				if($value)
				{
					$sql = "select count(order_id) as num_recs from orders " . 
						   "where order_number = ". dbquote($value);

					$res = mysql_query($sql) or dberror($sql);
					$row = mysql_fetch_assoc($res);
					if($row["num_recs"] == 0)
					{
						$error = 1;
						$error_string = $error_string . " " . $value;
					}
				}
			}
		}

		if($error == 0)
		{
			$form->save();
			$form->message("Your data has been saved.");

			if(id() > 0)
			{
				$link = "cer_benchmark.php?id=" . id();
				redirect($link);
			}
		}
		else
		{
			$form->error("Check your entry for the project numbers. The following entires are invalid:" . $error_string);
		}
	}

	
}

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("cer_benchmarks");

require "include/benchmark_page_actions.php";

$page->header();
$page->title(id() ? "Edit Benchmark" : "Add Benchmark");


$form->render();

?>


<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#salesregions_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/cer/cer_benchmark_selector.php?id=<?php echo id();?>&s=re'
    });
    return false;
  });
  $('#countries_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/cer/cer_benchmark_selector.php?id=<?php echo id();?>&s=co'
    });
    return false;
  });
  $('#cities_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/cer/cer_benchmark_selector.php?id=<?php echo id();?>&s=ci'
    });
    return false;
  });
  $('#areas_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/cer/cer_benchmark_selector.php?id=<?php echo id();?>&s=ar'
    });
    return false;
  });
  $('#pcts_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/cer/cer_benchmark_selector.php?id=<?php echo id();?>&s=pct'
    });
    return false;
  });
  $('#pkinds_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/cer/cer_benchmark_selector.php?id=<?php echo id();?>&s=pkinds'
    });
    return false;
  });
  $('#pls_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/cer/cer_benchmark_selector.php?id=<?php echo id();?>&s=pl'
    });
    return false;
  });
  $('#plss_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/cer/cer_benchmark_selector.php?id=<?php echo id();?>&s=pls'
    });
    return false;
  });
  $('#pts_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/cer/cer_benchmark_selector.php?id=<?php echo id();?>&s=pt'
    });
    return false;
  });
  $('#subclasses_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/cer/cer_benchmark_selector.php?id=<?php echo id();?>&s=sc'
    });
    return false;
  });
});
</script>


<div id="project_info" style="display:none;">
    Please indicate the projects you like to have in the benchmark separated by a semicolon as follows:<br />
	28.852.204;28.852.205;28.852.206
	
</div> 

<?php
$page->footer();
?>