<?php
/********************************************************************

    cer_sg_form_inr02c_pdf.php

    Print CER versus LN Figures (Form INR-02C).

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-01-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-01-15
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";


check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}

require_once('cer_sg_form_data.php');

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');


// Create and setup PDF document

$pdf = new TCPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 10, 10);
$pdf->setPrintHeader(false);

$pdf->SetDisplayMode(100); 		

$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');
$pdf->Open();

$pdf->SetTitle("Retail Capital Expenditure Request (CER) Form INR-02C");
$pdf->SetAuthor("Swatch Retailnet");
$pdf->SetDisplayMode(100);
$pdf->SetAutoPageBreak(false, 0);


//show data in client's currency
if(array_key_exists('cid', $_GET) and $_GET['cid'] == 'c1')
{
	$exchange_rate = 1;
	$exchange_rate_factor = 1;

	$ln_exchange_rate = 1;
	$ln_exchange_rate_factor = 1;
}

include("cer_sg_form_inr02C_detail_pdf.php");

$pdf->Output('INR-02C_' . $project_name . "_" . $project_number . '.pdf');

?>