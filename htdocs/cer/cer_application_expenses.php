<?php
/********************************************************************

	cer_application_expenses.php

    Application Form: expenses
    
	Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_cer");
set_referer("cer_application_expense.php");


/********************************************************************
    prepare all data needed
*********************************************************************/

$currency = get_cer_currency(param("pid"));

//*change of calculation model in 2012
$calc_model = 2012;
if($project["order_date"] < '2012-01-24' and $project["order_actual_order_state_code"] >= 840)
{
	$calc_model = 2008;
}

$result = update_cost_of_products_sold(param("pid"), $calc_model);

// get all years
$years = array();
$year_amounts = array();
$sql  = "select DISTINCT cer_expense_year  " .
		"from cer_expenses " .
		"where cer_expense_project = " . param("pid") . 
		" and cer_expense_cer_version = 0 " .
		" order by cer_expense_year";



$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$year_amounts[$row["cer_expense_year"]] = array();
	$years[] = $row["cer_expense_year"];
}



// get all expenses for for each year
$sql  = "select cer_expense_type, cer_expense_year, cer_expense_amount " .
		"from cer_expenses " .
		"where cer_expense_type != 9 " . 
		" and cer_expense_project = " . param("pid") . 
		" and cer_expense_cer_version = 0 ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$year_amounts[$row["cer_expense_year"]][$row["cer_expense_type"]] = $row["cer_expense_amount"];
}

//update salaries
$result = calculate_forcasted_salaries(param("pid"), $years, $project["order_shop_address_country"]);


//prepare sql for list
$sql_list1 = "select DISTINCT cer_expense_type_id, cer_expense_type_group_name, " . 
             "IF(cer_expense_type_id in (1, 2, 4, 15, 16), concat(cer_expense_type_name, '*'), cer_expense_type_name) as cer_expense_type_name " .
             "from cer_expenses " . 
			 "left join cer_expense_types on cer_expense_type_id = cer_expense_type ";

if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
{
	$list1_filter = "cer_expense_type <> 9 " . 
		            " and cer_expense_project = " . param("pid") . 
		            " and cer_expense_cer_version = 0 " .
		            " and cer_expense_type <> 14";
}
else
{
	$list1_filter = "cer_expense_type <> 9 " .
		            "and cer_expense_project = " . param("pid") . 
		            " and cer_expense_cer_version = 0 " .
		            " and cer_expense_type <> 14 " . 
		            " and cer_expense_type <> 11 " . 
		            " and cer_expense_type <> 12 ";
}


$is_airport_project = false;
$posdata = get_pos_data($project["project_order"]);
if(is_airport_project($project["pipeline"], $posdata["posaddress_id"]))
{
	$is_airport_project = true;
}
if($is_airport_project == false)
{
	$list1_filter .= " and cer_expense_type <> 19 ";
}

//$list1_filter = "cer_expense_type <> 9 and cer_expense_project = " . param("pid") . " and cer_expense_type <> 14";

//get list_totals
$list_totals = array();
$total_rents = array();
foreach($years as $key=>$year)
{
	$list_totals[$year] = 0;
	$total_rents[$year] = 0;
}

$sql = "select cer_expense_year, cer_expense_amount, cer_expense_type_group_name " .
       "from cer_expenses " . 
	   "left join cer_expense_types on cer_expense_type_id = cer_expense_type " .
	   " where " . $list1_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$list_totals[$row["cer_expense_year"]] = $list_totals[$row["cer_expense_year"]] + $row["cer_expense_amount"];

	if($row["cer_expense_type_group_name"] == '2 Rents')
	{
		$total_rents[$row["cer_expense_year"]] = $total_rents[$row["cer_expense_year"]] + $row["cer_expense_amount"];
	}

}


//check if business plan can be edited
require_once("include/check_if_business_plan_is_editable.php");

/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_basicdata", "cer_basicdata");
include("include/project_head.php");

$form->add_hidden("pid", param("pid"));


$form->add_section("Cost of Products Sold in Percent of Gross Sales");
$form->add_comment("Cost of watches/straps sold are: Purchase Price to Swatch Group or Agents - Retail Purchase Price.");


if(($can_edit_business_plan == true and $project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer") and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") and $project["project_state"] != 2))
{
	$form->add_edit("cer_basicdata_cost_watches", "Cost of Watches Sold in % of gross sales", 0, $cer_basicdata["cer_basicdata_cost_watches"], TYPE_DECIMAL, 12,2);
	$form->add_edit("cer_basicdata_cost_jewellery", "Cost of Watch Straps Sold in % gross sales", 0, $cer_basicdata["cer_basicdata_cost_jewellery"], TYPE_DECIMAL, 12,2);
	
	$form->add_edit("cer_basicdata_cost_service", "Cost of Customer Service/Accessories Sold in % gross sales", 0, $cer_basicdata["cer_basicdata_cost_service"], TYPE_DECIMAL, 12,2);


	$form->add_button("form_save", "Save Data");
}
else
{
	$form->add_label("cer_basicdata_cost_watches", "Cost of Watches Sold in %", 0, $cer_basicdata["cer_basicdata_cost_watches"]);
	$form->add_label("cer_basicdata_cost_jewellery", "Cost of Watch Straps Sold in %", 0, $cer_basicdata["cer_basicdata_cost_jewellery"]);
	
	
	$form->add_label("cer_basicdata_cost_service", "Cost of Customer Service/Accessories Sold in %", 0, $cer_basicdata["cer_basicdata_cost_service"]);
}

/********************************************************************
    build list of standard expenses
*********************************************************************/
$list1 = new ListView($sql_list1, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("Expenses in " . $currency["symbol"]);
$list1->set_entity("cer_expenses");
$list1->set_filter($list1_filter);
$list1->set_group("cer_expense_type_group_name");
$list1->set_order("cer_expense_type_sortorder");

$list1->add_hidden("pid", param("pid"));

$link = "cer_application_expense.php?pid=" . param("pid");
if(($can_edit_business_plan == true and $project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer") and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") and $project["project_state"] != 2))
{
	$list1->add_column("cer_expense_type_name", "Type", $link, "", "", COLUMN_NO_WRAP);
}
else
{
	$list1->add_column("cer_expense_type_name", "Type", "", "", "", COLUMN_NO_WRAP);
}


foreach($years as $key=>$year)
{
	$list1->add_text_column("y" . $year, "$year", COLUMN_ALIGN_RIGHT, $year_amounts[$year]);
	$list1->set_footer("y" . $year, number_format($list_totals[$year] ,0, ".", "'"));

	$list1->set_group_footer("y" . $year, '2 Rents' ,  number_format($total_rents[$year] ,0, ".", "'"));

}
$list1->set_footer("cer_expense_type_name", "Total");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

$list1->populate();
$list1->process();

if($form->button("form_save"))
{
	if($form->validate())
	{
		
		$fields = array();
    
		$value = dbquote($form->value("cer_basicdata_cost_watches"));
		$fields[] = "cer_basicdata_cost_watches = " . $value;

		$value = dbquote($form->value("cer_basicdata_cost_jewellery"));
		$fields[] = "cer_basicdata_cost_jewellery = " . $value;

		$value = dbquote($form->value("cer_basicdata_cost_service"));
		$fields[] = "cer_basicdata_cost_service = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_basicdata set " . join(", ", $fields) . 
			   " where cer_basicdata_project = " . param("pid") . 
			   " and cer_basicdata_version = 0 ";
		mysql_query($sql) or dberror($sql);

		//recalculate cost of productions
		$result = update_cost_of_products_sold(param("pid"), $calc_model);

		$link = "cer_application_expenses.php?pid=" . param("pid");
		redirect($link);
		
	}
	
}

  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();
if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Expenses");
}
else
{
	$page->title("Business Plan: Expenses");
}
require_once("include/tabs2016.php");
$form->render();

$list1->render();

require "include/footer_scripts.php";
$page->footer();

?>