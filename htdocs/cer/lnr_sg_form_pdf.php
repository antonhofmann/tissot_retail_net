<?php
/********************************************************************

    lnr_sg_form_pdf.php

    Print Lease Negotiation Application Form.

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2018-01-05
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2018-01-05
    Version:        1.0.0

    Copyright (c) 2018, Tissot, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 


require "../include/frame.php";
check_access("has_access_to_cer");


$cer_version = 0;
$ln_version = 0;
$ln_signatures = "";
if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}
require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('lnr_sg_form_data.php');

// Create and setup PDF document

$pdf = new TCPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 10, 10);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->SetTitle("Retail Lease Negotiation");
$pdf->SetAuthor( BRAND . " Retailnet");
$pdf->SetDisplayMode(100);
$pdf->SetAutoPageBreak(false, 0);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');
$pdf->Open();



include("lnr_sg_form_01_pdf_detail.php");

if($project["project_cost_type"] == 1 and $project["project_projectkind"] == 5) //corporate lease renewal
{
	include("lnr_sg_form_02Abis_pdf_detail.php");
}
elseif($project["project_cost_type"] == 1 or $project["project_cost_type"] == 9) //corporate
{
	include("lnr_sg_form_02A_pdf_detail.php");
}
elseif($project["project_cost_type"] == 2) //franchisee
{
	include("lnr_sg_form_02B_pdf_detail.php");
}
include("lnr_sg_form_03_pdf_detail.php");


$file_name = "LNR_" . $project["project_number"] . ".pdf";

$pdf->Output($file_name);

?>