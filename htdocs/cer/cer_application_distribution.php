<?php
/********************************************************************

    cer_application_distribution.php

    Application Form: general information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-10-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-10-20
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/

$units_of_measurement = array();
$units_of_measurement[1] = "km";
$units_of_measurement[2] = "miles";

$posdata = get_pos_data($project["project_order"]);

$currency_symbol = $posdata["currency_symbol"];


$province_id = 0;
$sql = "select place_province from places where place_id = " . dbquote($posdata["posaddress_place_id"]);
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$province_id = $row["place_province"];
}

if(param("province") === 0 or param("province") === 'all')
{
	$province_id = 'all';
}
elseif(param("province") and param("province") != 0)
{
	$province_id = param("province");
}


$province_filter = array();
$province_filter["all"] = "All";
$sql_provinces = "select DISTINCT province_id, province_canton " . 
	   "from posaddresses " .
	   "left join places on place_id = posaddress_place_id " . 
	   "left join provinces on province_id = place_province " . 
	   " where province_country = " . $posdata["posaddress_country"] . " order by province_canton";
$res = mysql_query($sql_provinces) or dberror($sql_provinces);
while($row = mysql_fetch_assoc($res))
{
	$province_filter[$row["province_id"]] = $row["province_canton"];
}



//create links and get values for the distribution analysis

//get all locations already included
$included_pos = array();
$sql_p = "select ln_basicdata_lnr03_posaddress_id_da, ln_basicdata_lnr03_posaddress_id_pr from ln_basicdata_inr03 " . 
			   "where ln_basicdata_lnr03_cer_version = 0  " . 
			   "and ln_basicdata_lnr03_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"];
$res_p = mysql_query($sql_p) or dberror($sql_p);
while ($row_p = mysql_fetch_assoc($res_p))
{
	if($row_p["ln_basicdata_lnr03_posaddress_id_da"] > 0) {
		$included_pos[] = $row_p["ln_basicdata_lnr03_posaddress_id_da"];
	}
	elseif($row_p["ln_basicdata_lnr03_posaddress_id_pr"] > 0) {
		$included_pos[] = $row_p["ln_basicdata_lnr03_posaddress_id_pr"];
	
	}
}


//sql for the list
//compose list
$sql_list = "select posaddress_id, posaddress_client_id, if(posaddress_name <> '', posaddress_name, 'n.a.') as posname, " .
       "posaddress_address, posaddress_address2, posaddress_zip, posaddress_store_openingdate, " .
       "    posaddress_place, country_name, project_costtype_text, postype_name, posaddress_ownertype, " .
	   "posaddress_google_precision, posaddress_store_closingdate, province_canton, posaddress_country, " .
	   "posaddress_store_planned_closingdate, posaddress_store_totalsurface " . 
       "from posaddresses " .
	   "left join places on place_id = posaddress_place_id " .
	   "left join provinces on province_id = place_province " .
	   "left join countries on posaddress_country = country_id " . 
	   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
	   "left join postypes on postype_id = posaddress_store_postype ";


if(count($included_pos) > 0 and $province_id == 'all')
{
	$list_filter = "(province_id > 0 " . 
		           " or posaddress_id in (" . implode(',', $included_pos) . ")) " .
                   " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') ";
}
elseif(count($included_pos) > 0 and $province_id > 0)
{
	$list_filter = "(province_id = " . dbquote($province_id) . 
		           " or posaddress_id in (" . implode(',', $included_pos) . ")) " .
                   " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') ";
}
elseif($province_id == 'all')
{
	$list_filter = "province_id > 0 " . 
                   " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') ";
}
else
{
	$list_filter = "province_id = " . dbquote($province_id) . 
                   " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') ";
}

$list_filter = "(" . $list_filter . ") and posaddress_country = " . $posdata["posaddress_country"];

$list_filter2 = "(" . $list_filter . ") and posaddress_ownertype = 1 and posaddress_country = " . $posdata["posaddress_country"];


//get list values
$preceeding_year = date("Y") - 1;
$include_sellouts_in_ln = array();
$distances = array();
$distance_units = array();
$sellout_ids = array();
$years = array();
$months = array();
$selling_months = array();
$units_watches = array();
$units_bijoux = array();
$gross_sales = array();
$planned_closing_dates = array();
$opening_dates = array();
$corporate_pos = array();

$links = array();

for($i=1;$i<13;$i++)
{
	$months[$i] = $i;
}


$sql_tmp = $sql_list . " where " . $list_filter;


$res = mysql_query($sql_tmp) or dberror($sql_tmp);
while($row = mysql_fetch_assoc($res))
{
	$links[$row["posaddress_id"]] = '<a href="/pos/pos_profitability_pos.php?country=' . $row["posaddress_country"] . '&province=' . $province_id. '&ltf=all&ostate=1&pos_id=' . $row["posaddress_id"] . '" target="_blank">' . $row["posname"] . '</a>';

	
	$included_pos_filter = " and possellout_posaddress_id in (" . $row["posaddress_id"] . ") ";
	if(count($included_pos) > 0)
	{
		$included_pos[$row["posaddress_id"]] = $row["posaddress_id"];
		$included_pos_filter = " and possellout_posaddress_id in (" . implode(',', $included_pos) . ") ";
	}
	
	$sql_p = "select possellout_id, possellout_posaddress_id, possellout_year, possellout_month, " . 
				 "possellout_watches_units, possellout_bijoux_units, " .
				 "possellout_grossales " .
				 "from possellouts " .
				 " where possellout_year = " . $preceeding_year . 
				 $included_pos_filter . 
				 " order by possellout_year DESC";
	$res_p = mysql_query($sql_p) or dberror($sql_p);
	while ($row_p = mysql_fetch_assoc($res_p))
	{
		$years[$row_p["possellout_posaddress_id"]] = $row_p["possellout_year"];
		$selling_months[$row_p["possellout_posaddress_id"]] = $row_p["possellout_month"];

		$units_watches[$row_p["possellout_posaddress_id"]] = $row_p["possellout_watches_units"];
		$units_bijoux[$row_p["possellout_posaddress_id"]] = $row_p["possellout_bijoux_units"];
		$gross_sales[$row_p["possellout_posaddress_id"]] = $row_p["possellout_grossales"];

	}

	$planned_closing_dates[$row["posaddress_id"]] = to_system_date($row["posaddress_store_planned_closingdate"]);

	if($row["posaddress_store_openingdate"] == NULL
		or $row["posaddress_store_openingdate"] == '0000-00-00'
	    or $row["posaddress_store_openingdate"] < '1971-01-01')
	{
			$opening_dates[$row["posaddress_id"]] = "n/a";
	}
	else
	{
		$opening_dates[$row["posaddress_id"]] =to_system_date($row["posaddress_store_openingdate"]);
	}

	if($row["posaddress_ownertype"] == 1) {
		$corporate_pos[] = $row["posaddress_id"];
	}

}


if(array_key_exists("action", $_POST) and $_POST["action"] = 'save')
{
	
}
else
{
	//get saved values for this LNR/CER
	$sql_p = "select * from ln_basicdata_inr03 " . 
			 "where ln_basicdata_lnr03_cer_version = 0 " . 
		     " and ln_basicdata_lnr03_posaddress_id_da > 0 " . 
			 "and ln_basicdata_lnr03_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"];

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	while ($row_p = mysql_fetch_assoc($res_p))
	{
		$include_sellouts_in_ln[$row_p["ln_basicdata_lnr03_posaddress_id_da"]] = 1;
		$years[$row_p["ln_basicdata_lnr03_posaddress_id_da"]] = $row_p["ln_basicdata_lnr03_year"];
		$selling_months[$row_p["ln_basicdata_lnr03_posaddress_id_da"]] = $row_p["ln_basicdata_lnr03_month"];

		$units_watches[$row_p["ln_basicdata_lnr03_posaddress_id_da"]] = $row_p["ln_basicdata_lnr03_watches_units"];		
		$units_bijoux[$row_p["ln_basicdata_lnr03_posaddress_id_da"]] = $row_p["ln_basicdata_lnr03_bijoux_units"];
		$gross_sales[$row_p["ln_basicdata_lnr03_posaddress_id_da"]] = $row_p["ln_basicdata_lnr03_grossales"];
		
		$distances [$row_p["ln_basicdata_lnr03_posaddress_id_da"]] = $row_p["ln_basicdata_lnr03_distance"];
		$distance_units [$row_p["ln_basicdata_lnr03_posaddress_id_da"]] = $row_p["ln_basicdata_lnr03_distance_unit"];

		$planned_closing_dates[$row["ln_basicdata_lnr03_posaddress_id_da"]] = to_system_date($row["ln_basicdata_lnr03_planned_closingdate"]);
	}
}




//profitability
$sql = $sql_list . " where " . $list_filter;
//get sellouts values
$years2 = array();
$units_watches2 = array();
$units_bijoux2 = array();
$net_sales = array();
$wholesalemargin_values = array();
$grossmargin_values = array();
$expenses_values = array();
$operating_income01_values = array();
$operating_income02_values = array();

$include_profitability_in_ln = array();
$years2 = array();
$selling_months2 = array();

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	
	$sql_p = "select * " . 
		     "from possellouts " . 
		     " where possellout_month = 12 " . 
		     " and possellout_posaddress_id = " . $row["posaddress_id"] . 
		     " order by possellout_year DESC";


	$sql_p = "select * " . 
		     "from possellouts " . 
		     " where possellout_posaddress_id = " . $row["posaddress_id"] . 
		     " order by possellout_year DESC";
	
	
	$res_p = mysql_query($sql_p) or dberror($sql_p);
	if ($row_p = mysql_fetch_assoc($res_p))
	{
		$years2[$row["posaddress_id"]] = $row_p["possellout_year"];
		$selling_months2[$row_p["possellout_posaddress_id"]] = $row_p["possellout_month"];
		$units_watches2[$row_p["possellout_posaddress_id"]] = $row_p["possellout_watches_units"];
		$units_bijoux2[$row_p["possellout_posaddress_id"]] = $row_p["possellout_bijoux_units"];
		$net_sales[$row["posaddress_id"]] = $row_p["possellout_net_sales"];
		$grossmargin_values[$row["posaddress_id"]] = $row_p["possellout_grossmargin"];

		$wholesalemargin_values[$row["posaddress_id"]] = $row_p["possellout_wholsale_margin"];
		
		$expenses_values[$row["posaddress_id"]] = $row_p["possellout_operating_expenses"];
		$operating_income01_values[$row["posaddress_id"]] = $row_p["possellout_operating_income_excl"];
		$operating_income02_values[$row["posaddress_id"]] = $row_p["possellout_operating_income_incl"];
		$profitability_ids[$row["posaddress_id"]] = $row_p["possellout_id"];
		
	}
}


if(array_key_exists("action", $_POST) and $_POST["action"] = 'save')
{
	
}
else
{
	//get saved values for this LNR/CER
	$sql_p = "select * from ln_basicdata_inr03 " . 
			 "where ln_basicdata_lnr03_cer_version = 0 " . 
		     " and ln_basicdata_lnr03_posaddress_id_pr > 0 " . 
			 "and ln_basicdata_lnr03_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"];

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	while ($row_p = mysql_fetch_assoc($res_p))
	{
		$include_profitability_in_ln[$row_p["ln_basicdata_lnr03_posaddress_id_pr"]] = 1;
		$years2[$row_p["ln_basicdata_lnr03_posaddress_id_pr"]] = $row_p["ln_basicdata_lnr03_year"];
		$selling_months2[$row_p["ln_basicdata_lnr03_posaddress_id_pr"]] = $row_p["ln_basicdata_lnr03_month"];

		$units_watches2[$row_p["ln_basicdata_lnr03_posaddress_id_da"]] = $row_p["ln_basicdata_lnr03_watches_units"];		
		$units_bijoux2[$row_p["ln_basicdata_lnr03_posaddress_id_da"]] = $row_p["ln_basicdata_lnr03_bijoux_units"];

		$net_sales[$row_p["ln_basicdata_lnr03_posaddress_id_pr"]] = $row_p["ln_basicdata_lnr03_net_sales"];		
		
		$wholesalemargin_values[$row_p["ln_basicdata_lnr03_posaddress_id_pr"]] = $row_p["ln_basicdata_lnr03_wholsale_margin"];		
		
		$grossmargin_values[$row_p["ln_basicdata_lnr03_posaddress_id_pr"]] = $row_p["ln_basicdata_lnr03_grossmargin"];
		$expenses_values[$row_p["ln_basicdata_lnr03_posaddress_id_pr"]] = $row_p["ln_basicdata_lnr03_operating_expenses"];
		
		$operating_income01_values [$row_p["ln_basicdata_lnr03_posaddress_id_pr"]] = $row_p["ln_basicdata_lnr03_operating_income_excl"];

		$operating_income01_values [$row_p["ln_basicdata_lnr03_posaddress_id_pr"]] = $row_p["ln_basicdata_lnr03_operating_income_excl"];

		$operating_income02_values [$row_p["ln_basicdata_lnr03_posaddress_id_pr"]] = $row_p["ln_basicdata_lnr03_operating_income_incl"];
	}
}


/********************************************************************
	build form
*********************************************************************/
$form = new Form("cer_expenses", "CER Expenses");

include("include/project_head.php");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("province", $province_id);


if(!param("context"))
{
	$form->add_hidden("context", "cer");
}
else
{
	$form->add_hidden("context", "ln");
}

$form->add_comment('<span class="error">Please fill in the sell outs as follows:</span><br />
POS open during full last year: sell out for full last year<br />
POS opened during last year: sell out for relevant number of months last year<br />
POS opened in current year: sell out for rlevant number of months current year<br /><br />
Note that a <strong>maximum of 10 POS locations</strong> will be printed on the LNR-03 form.<br />
Please indicate information for the <span class="error">10 closest POS locations</span> to the proposed location.
');


//check if business plan can be edited
require_once("include/check_if_business_plan_is_editable.php");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();

/********************************************************************
	build list of pos locations
*********************************************************************/
$list = new ListView($sql_list);
$list->set_title("POS Locations for LNR-03 Distribution Analysis");
$list->set_entity("posaddresses");
$list->set_filter($list_filter);
$list->set_order("country_name, posaddress_place, posaddress_name");
$list->set_group("province_canton");

$list->add_listfilters("province", "Province", 'select', $province_filter, $province_id);


$list->add_checkbox_column("include_in_ln_da", "", "", $include_sellouts_in_ln);

if(has_access("has_access_to_pos_profitability") 
	or has_access("can_edit_pos_profitability_of_his_pos")
	or has_access("can_edit_pos_profitability_of_all_pos"))
{
	$list->add_text_column("posname", "POS Name", COLUMN_UNDERSTAND_HTML, $links);
}
else
{
	$list->add_column("posname", "POS Name", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
}
//$list->add_column("province_canton", "Province", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("postype_name", "POS Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("project_costtype_text", "Legal Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);

$list->add_column("posaddress_store_totalsurface", "Total Surface<br />in sqm", "", LIST_FILTER_LIST ,'', COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP);


$list->add_text_column("opening", "Opening Date", "", $opening_dates);


//$list->add_column("posaddress_store_openingdate", "Opening Date", "", LIST_FILTER_LIST ,'', COLUMN_IS_DATE);

$list->add_edit_column("distances", "Distance from<br />proposed POS", 8, COLUMN_UNDERSTAND_HTML, $distances);

$list->add_list_column("unit_of_measurement", "Unit", $units_of_measurement, 0, $distance_units);
$list->add_edit_column("years", "Year", 4, "", $years);
$list->add_list_column("months", "Months<br />Selling", $months, COLUMN_UNDERSTAND_HTML, $selling_months);
$list->add_edit_column("units_watches", "Units sold<br />Watches", 10, COLUMN_UNDERSTAND_HTML, $units_watches);
$list->add_edit_column("units_bijoux", "Units sold<br />Watch Straps", 10, COLUMN_UNDERSTAND_HTML, $units_bijoux);

$list->add_edit_column("gross_sales", "Gross Sales<br />in " . $currency_symbol, 10, COLUMN_UNDERSTAND_HTML, $gross_sales);

$list->add_edit_column("posaddress_store_planned_closingdate", "Planned Closing<br />Date", 10, COLUMN_UNDERSTAND_HTML, $planned_closing_dates);

if(($can_edit_business_plan == true and $project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer") and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") and $project["project_state"] != 2))
{
	$list->add_button("save", "Save List");
}


//$list->add_button("refresh", "Refresh List");
$list->populate();
$list->process();



/********************************************************************
    build list of standard expenses
*********************************************************************/
$list2 = new ListView($sql_list, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list2->set_title("POS Locations for LNR-03 Profitability Analysis (for corporate POS only)");
$list2->set_entity("posaddresses");
$list2->set_filter($list_filter2);
$list2->set_order("country_name, posaddress_ownertype, posaddress_place, posaddress_name");
$list2->set_group("province_canton");

$list2->add_checkbox_column("include_in_ln_pr", "", "", $include_profitability_in_ln);

if(has_access("has_access_to_pos_profitability") 
	or has_access("can_edit_pos_profitability_of_his_pos")
	or has_access("can_edit_pos_profitability_of_all_pos"))
{
	$list2->add_text_column("posname", "POS Name", COLUMN_UNDERSTAND_HTML, $links);
}
else
{
	$list2->add_column("posname", "POS Name", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
}
$list2->add_column("posaddress_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list2->add_column("postype_name", "POS Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list2->add_column("project_costtype_text", "Legal Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);

$list2->add_column("posaddress_store_totalsurface", "Total Surface<br />in sqm", "", LIST_FILTER_LIST ,'', COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP);


$list2->add_text_column("opening", "Opening Date", "", $opening_dates);

$list2->add_edit_column("years2", "Year", 4, "", $years2);
$list2->add_list_column("months2", "Months<br />Selling", $months, COLUMN_UNDERSTAND_HTML, $selling_months2);

$list2->add_edit_column("units_watches2", "Units sold<br />Watches", 10, COLUMN_UNDERSTAND_HTML, $units_watches2);
$list2->add_edit_column("units_bijoux2", "Units sold<br />Watch Straps", 10, COLUMN_UNDERSTAND_HTML, $units_bijoux2);


$list2->add_edit_column("net_sales", "Net Sales<br />in " . $currency_symbol, 10, COLUMN_UNDERSTAND_HTML, $net_sales);
$list2->add_edit_column("gross_margin", "Gross Margin<br />in " . $currency_symbol, 10, COLUMN_UNDERSTAND_HTML, $grossmargin_values);

$list2->add_edit_column("expenses", "Indirect operating<br />expenses in " . $currency_symbol, 10, COLUMN_UNDERSTAND_HTML, $expenses_values);

$list2->add_edit_column("operating_income", "Operating Income<br />in " . $currency_symbol, 10, COLUMN_UNDERSTAND_HTML, $operating_income01_values);

$list2->add_edit_column("wholesale_margin", "Wholesale maring in %", 10, COLUMN_UNDERSTAND_HTML, $wholesalemargin_values);

$list2->add_edit_column("operating_income2", "Operating Income<br />incl. Wholesale Margin in " . $currency_symbol, 10, COLUMN_UNDERSTAND_HTML, $operating_income02_values);



if(($can_edit_business_plan == true and $project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer") and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") and $project["project_state"] != 2))
{
	
	//$link = 'javascript:select_all_corporate_pos();';
	//$list2->add_button("s1", "Select all corporate POS", $link);
	
	$list2->add_button("save", "Save List");
}


//$list2->add_button("refresh", "Refresh List");
$list2->populate();
$list2->process();



/********************************************************************
save list
*********************************************************************/
if($list->button("save") or $list2->button("save"))
{
	//process distribution
	
	//sellouts
	$include_sellouts_in_ln = array();
	$save_in_pos_sellouts = array();
	$distances = array();
	$distance_units = array();
	$sellout_ids = array();
	$years = array();
	$months = array();
	$units_watches = array();
	$units_bijoux = array();
	$gross_sales = array();
	$planned_closing_dates = array();
	
	//profitability
	$include_profitability_in_ln = array();
	$years2 = array();
	$months2 = array();
	$units_watches2 = array();
	$units_bijoux2 = array();
	$net_sales = array();
	$gross_margin = array();
	$expenses = array();
	$operating_income = array();
	$wholesale_margin = array();
	$operating_income2 = array();

	foreach($list->columns as $key=>$column)
	{
		
		
		if($column["name"] == "include_in_ln_da")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$include_sellouts_in_ln[$posaddress_id] = $value;
				$save_in_pos_sellouts[$posaddress_id] = $value;
			}
		}
		if($column["name"] == "distances")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$distances[$posaddress_id] = $_POST['__posaddresses_distances_' . $posaddress_id];
			}
		}

		if($column["name"] == "unit_of_measurement")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$distance_units[$posaddress_id] = $_POST['__posaddresses_unit_of_measurement_' . $posaddress_id];
			}
		}

		if($column["name"] == "years")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$years[$posaddress_id] = $_POST['__posaddresses_years_' . $posaddress_id];
				
				if($years[$posaddress_id])
				{
					$save_in_pos_sellouts[$posaddress_id] = $years[$posaddress_id];
				}
			}
		}

		if($column["name"] == "months")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$months[$posaddress_id] = $_POST['__posaddresses_months_' . $posaddress_id];
			}
		}

		if($column["name"] == "units_watches")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$units_watches[$posaddress_id] = $_POST['__posaddresses_units_watches_' . $posaddress_id];
			}
		}

		if($column["name"] == "units_bijoux")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$units_bijoux[$posaddress_id] = $_POST['__posaddresses_units_bijoux_' . $posaddress_id];
			}
		}

		
		if($column["name"] == "gross_sales")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$gross_sales[$posaddress_id] = $_POST['__posaddresses_gross_sales_' . $posaddress_id];
			}
		}

		if($column["name"] == "posaddress_store_planned_closingdate")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$planned_closingdates[$posaddress_id] = $_POST['__posaddresses_posaddress_store_planned_closingdate_' . $posaddress_id];
			}
		}
	}


	foreach($list2->columns as $key=>$column)
	{
		if($column["name"] == "include_in_ln_pr")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$include_profitability_in_ln[$posaddress_id] = $value;
				$save_in_pos_sellouts[$posaddress_id] = $value;
			}
		}
		if($column["name"] == "years2")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$years2[$posaddress_id] = $_POST['__posaddresses_years2_' . $posaddress_id];
				
				if($years2[$posaddress_id])
				{
					$save_in_pos_sellouts[$posaddress_id] = $years2[$posaddress_id];
				}
			}
		}

		if($column["name"] == "months2")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$months2[$posaddress_id] = $_POST['__posaddresses_months2_' . $posaddress_id];
			}
		}

		if($column["name"] == "units_watches2")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$units_watches2[$posaddress_id] = $_POST['__posaddresses_units_watches2_' . $posaddress_id];
			}
		}

		if($column["name"] == "units_bijoux2")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$units_bijoux2[$posaddress_id] = $_POST['__posaddresses_units_bijoux2_' . $posaddress_id];
			}
		}
		
		if($column["name"] == "net_sales")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$net_sales[$posaddress_id] = $_POST['__posaddresses_net_sales_' . $posaddress_id];
			}
		}

		if($column["name"] == "gross_margin")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$gross_margin[$posaddress_id] = $_POST['__posaddresses_gross_margin_' . $posaddress_id];
			}
		}

		if($column["name"] == "expenses")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$expenses[$posaddress_id] = $_POST['__posaddresses_expenses_' . $posaddress_id];
			}
		}

		if($column["name"] == "operating_income")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$operating_income[$posaddress_id] = $_POST['__posaddresses_operating_income_' . $posaddress_id];
			}
		}

		if($column["name"] == "wholesale_margin")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$wholesale_margin[$posaddress_id] = $_POST['__posaddresses_wholesale_margin_' . $posaddress_id];
			}
		}

		if($column["name"] == "operating_income2")
		{
			foreach($column["values"] as $posaddress_id=>$value)
			{
				$operating_income2[$posaddress_id] = $_POST['__posaddresses_operating_income2_' . $posaddress_id];
			}
		}

		
	}



	//validate data
	$error = 0;
	$error_occured = 0;
	$errors[1] = "Distribution: You have indicated an invalid year.";
	$errors[2] = "Distribution: You have indicated an invalid month.";
	$errors[3] = "Distribution: Please indicate the unit of measurement.";
	$errors[4] = "Distribution: The distance of a selected POS must be a number.";
	$errors[5] = "Distribution: Please indicate only integer values for units sold and gross sales.";
	$errors[6] = "Distribution: The planned closing date muts be a date in the form of dd.mm.yyyy.";
	$errors[7] = "Distribution: Please add units and gross sales at least for the watches.";
	$errors[8] = "Distribution: The planned closing date must be a future date.<br />In caes the POS concerned is already closed then please add a closing date to the POS in MPS.";
	$errors[9] = "Profitability: You have indicated an invalid year.";
	$errors[10] = "Profitability: You have indicated an invalid month.";
	$errors[11] = "Profitability: Please indicate only integer values for net sales.";
	$errors[12] = "Profitability: Please indicate only integer values for gross margins.";
	$errors[13] = "Profitability: Please indicate only integer values for expenses.";
	$errors[14] = "Profitability: Please indicate only integer values for operating incomes.";
	$errors[15] = "Profitability: Please indicate only decimal values for wholesale margins.";
	$errors[16] = "Profitability: Please indicate only integer values for operating incomes.";
	$errors[17] = "Profitability: Please add values least for the watches.";



	//save in POS sellouts
	foreach($save_in_pos_sellouts as $posaddress_id=>$value)
	{

		$error = 0;
		if(array_key_exists($posaddress_id, $include_sellouts_in_ln)) {
			if(!is_int_value($years[$posaddress_id], 5) 
				or $years[$posaddress_id] == 0)
			{
				$error = 1;
			}
			if(array_key_exists($posaddress_id, $include_sellouts_in_ln)
				and !$distance_units[$posaddress_id])
			{
				$error = 3;
			}

			if(!$months[$posaddress_id])
			{
				$error = 2;
			}

			if($units_watches[$posaddress_id] and !is_int_value($units_watches[$posaddress_id], 12))
			{
				$error = 5;
			}
			if($units_bijoux[$posaddress_id] and !is_int_value($units_bijoux[$posaddress_id], 12))
			{
				$error = 5;
			}
			
			if(!is_date_value ( $planned_closingdates[$posaddress_id]))
			{
				$error = 6;
			}

			if(!$units_watches[$posaddress_id])
			{
				$error = 7;
			}

			if(!$gross_sales[$posaddress_id])
			{
				$error = 7;
			}

			if($planned_closingdates[$posaddress_id] and from_system_date($planned_closingdates[$posaddress_id]) <= date("Y-m-d"))
			{
				$error = 8;

			}
		}

	

		if(array_key_exists($posaddress_id, $include_profitability_in_ln)) {
			
			if(!is_int_value($years2[$posaddress_id], 5) 
				or $years2[$posaddress_id] == 0)
			{
				$error = 9;
			}
			if(!$months2[$posaddress_id])
			{
				$error = 10;
			}

					
			if(!$units_watches2[$posaddress_id] or !is_int_value($units_watches2[$posaddress_id], 12))
			{
				$error = 17;
			}

			if(!$net_sales[$posaddress_id] or !is_int_value($net_sales[$posaddress_id], 12))
			{
				$error = 11;
			}

			if(!$gross_margin[$posaddress_id] or !is_int_value($gross_margin[$posaddress_id], 12))
			{
				$error = 11;
			}

			if(!$expenses[$posaddress_id] or !is_int_value($expenses[$posaddress_id], 12))
			{
				$error = 13;
			}

			
			if(!$operating_income[$posaddress_id] or !is_int_value($operating_income[$posaddress_id], 12))
			{
				$error = 14;
			}

			if(!$wholesale_margin[$posaddress_id] or !is_decimal_value($wholesale_margin[$posaddress_id], 5, 2))
			{
				$error = 15;
			}

			if(!$operating_income2[$posaddress_id] or !is_int_value($operating_income2[$posaddress_id], 12))
			{			
				$error = 16;
			}
		}



		
		//update posaddress with planned closing date
		$sql = "update posaddresses SET " . 
			   "posaddress_store_planned_closingdate  = " . dbquote(from_system_date($planned_closingdates[$posaddress_id])) . 
			   " where posaddress_id = " . $posaddress_id;

		$result = mysql_query($sql) or dberror($sql);

		//update latest project of the POS with planned closing date
		// do not consider closed or cancceled projects
		
		$sql = "select project_id " . 
			   " from posorders " . 
			   " left join projects on project_order = posorder_order " . 
			   " where posorder_type = 1 " . 
			   " and posorder_posaddress = " . $posaddress_id . 
			   " and project_projectkind <> 8 " .
			   " and project_state < 5 " . 
			   " order by project_actual_opening_date DESC";
		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$sql = "update projects SET " . 
				   "project_planned_closing_date  = " . dbquote(from_system_date($planned_closingdates[$posaddress_id])) . 
				   " where project_id = " . $row["project_id"];
			$result = mysql_query($sql) or dberror($sql);
		}


		//update POS Sellout data
		foreach($save_in_pos_sellouts as $posaddress_id=>$value)
		{
			
			//check if sellout is present
			$sql = "select * " . 
				   " from possellouts ". 
				   " where possellout_posaddress_id = " . $posaddress_id. 
				   " and possellout_year = " .  dbquote($years[$posaddress_id]);

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res) 
				and array_key_exists($posaddress_id, $years2))
			{
				$sql = "UPDATE possellouts SET " .
					   " possellout_month = " . dbquote($months[$posaddress_id]) . ", " .
					   " possellout_watches_units = " . dbquote($units_watches[$posaddress_id]) . ", " .
					   " possellout_bijoux_units = " . dbquote($units_bijoux[$posaddress_id]) . ", " .
					   " possellout_grossales = " . dbquote($gross_sales[$posaddress_id]) . ", " .
					   " possellout_net_sales = " . dbquote($net_sales[$posaddress_id]) . ", " .
					   " possellout_grossmargin = " . dbquote($gross_margin[$posaddress_id]) . ", " .
					   " possellout_operating_expenses = " . dbquote($expenses[$posaddress_id]) . ", " .
					   " possellout_operating_income_excl = " . dbquote($operating_income[$posaddress_id]) . ", " .
					   " possellout_wholsale_margin = " . dbquote($wholesale_margin[$posaddress_id]) . ", " .
					   " possellout_operating_income_incl = " . dbquote($operating_income2[$posaddress_id]) . 
					   " where possellout_posaddress_id = " . $posaddress_id . 
					   " and possellout_year = " . dbquote($years2[$posaddress_id]);
				$result = mysql_query($sql) or dberror($sql);
			}
			elseif(array_key_exists($posaddress_id, $years2)
				and $years[$posaddress_id] > 0)
			{
				$sql = "insert into possellouts (" .
					   "possellout_posaddress_id, " . 
					   "possellout_year, " . 
					   "possellout_month, " . 
					   "possellout_watches_units, " .
					   "possellout_bijoux_units, " .
					   "possellout_grossales, " .
					   "possellout_net_sales, " .
					   "possellout_grossmargin, " .
					   "possellout_operating_expenses, " .
					   "possellout_operating_income_excl, " .
					   "possellout_wholsale_margin, " .
					   "possellout_operating_income_incl, " .
					   "user_created, date_created) " .
					   " values (" .
					   $posaddress_id . ", " .
					   dbquote($years[$posaddress_id]) . ", " .
					   dbquote($months[$posaddress_id]) . ", " .
					   dbquote($units_watches[$posaddress_id]) . ", " .
					   dbquote($units_bijoux[$posaddress_id]) . ", " .
					   dbquote($gross_sales[$posaddress_id]) . ", " .
					   dbquote($net_sales[$posaddress_id]) . ", " .
					   dbquote($gross_margin[$posaddress_id]) . ", " .
					   dbquote($expenses[$posaddress_id]) . ", " .
					   dbquote($operating_income[$posaddress_id]) . ", " .
					   dbquote($wholesale_margin[$posaddress_id]) . ", " .
					   dbquote($operating_income2[$posaddress_id]) . ", " .
						   "'" . user_login() . "', " .
					   "'" . date("Y-m-d H:i:s") . "')";

				$result = mysql_query($sql) or dberror($sql);
			}
			elseif($row = mysql_fetch_assoc($res))
			{
				$sql = "UPDATE possellouts SET " .
					   " possellout_month = " . dbquote($months[$posaddress_id]) . ", " .
					   " possellout_watches_units = " . dbquote($units_watches[$posaddress_id]) . ", " .
					   " possellout_bijoux_units = " . dbquote($units_bijoux[$posaddress_id]) . ", " .
					   " possellout_grossales = " . dbquote($gross_sales[$posaddress_id]) . 
					   " where possellout_posaddress_id = " . $posaddress_id . 
					   " and possellout_year = " . dbquote($years[$posaddress_id]);

				$result = mysql_query($sql) or dberror($sql);
			}
			elseif($years[$posaddress_id] > 0) {
				$sql = "insert into possellouts (" .
					   "possellout_posaddress_id, " . 
					   "possellout_year, " . 
					   "possellout_month, " . 
					   "possellout_watches_units, " . 
					   "possellout_bijoux_units, " . 
					   "possellout_grossales, " . 
					   "user_created, date_created) " .
					   " values (" .
					   $posaddress_id . ", " .
					   dbquote($years[$posaddress_id]) . ", " .
					   dbquote($months[$posaddress_id]) . ", " .
					   dbquote($units_watches[$posaddress_id]) . ", " .
					   dbquote($units_bijoux[$posaddress_id]) . ", " .
					   dbquote($gross_sales[$posaddress_id]) . ", " .
					   "'" . user_login() . "', " .
					   "'" . date("Y-m-d H:i:s") . "')";

				$result = mysql_query($sql) or dberror($sql);
			}


			//check if sellout is present
			$sql = "select * " . 
				   " from possellouts ". 
				   " where possellout_posaddress_id = " . $posaddress_id. 
				   " and possellout_year = " .  dbquote($years2[$posaddress_id]);

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res) 
				and array_key_exists($posaddress_id, $years))
			{
				$sql = "UPDATE possellouts SET " .
					   " possellout_month = " . dbquote($months2[$posaddress_id]) . ", " .
					   " possellout_watches_units = " . dbquote($units_watches2[$posaddress_id]) . ", " .
					   " possellout_bijoux_units = " . dbquote($units_bijoux2[$posaddress_id]) . ", " .
					   " possellout_grossales = " . dbquote($gross_sales[$posaddress_id]) . ", " .
					   " possellout_net_sales = " . dbquote($net_sales[$posaddress_id]) . ", " .
					   " possellout_grossmargin = " . dbquote($gross_margin[$posaddress_id]) . ", " .
					   " possellout_operating_expenses = " . dbquote($expenses[$posaddress_id]) . ", " .
					   " possellout_operating_income_excl = " . dbquote($operating_income[$posaddress_id]) . ", " .
					   " possellout_wholsale_margin = " . dbquote($wholesale_margin[$posaddress_id]) . ", " .
					   " possellout_operating_income_incl = " . dbquote($operating_income2[$posaddress_id]) . 
					   " where possellout_posaddress_id = " . $posaddress_id . 
					   " and possellout_year = " . dbquote($years2[$posaddress_id]);
				
				$result = mysql_query($sql) or dberror($sql);
			}
			elseif(array_key_exists($posaddress_id, $years)
				and $years2[$posaddress_id] > 0)
			{
				$sql = "insert into possellouts (" .
					   "possellout_posaddress_id, " . 
					   "possellout_year, " . 
					   "possellout_month, " . 
					   "possellout_watches_units, " .
					   "possellout_bijoux_units, " .
					   "possellout_grossales, " .
					   "possellout_net_sales, " .
					   "possellout_grossmargin, " .
					   "possellout_operating_expenses, " .
					   "possellout_operating_income_excl, " .
					   "possellout_wholsale_margin, " .
					   "possellout_operating_income_incl, " .
					   "user_created, date_created) " .
					   " values (" .
					   $posaddress_id . ", " .
					   dbquote($years2[$posaddress_id]) . ", " .
					   dbquote($months2[$posaddress_id]) . ", " .
					   dbquote($units_watches2[$posaddress_id]) . ", " .
					   dbquote($units_bijoux2[$posaddress_id]) . ", " .
					   dbquote($gross_sales[$posaddress_id]) . ", " .
					   dbquote($net_sales[$posaddress_id]) . ", " .
					   dbquote($gross_margin[$posaddress_id]) . ", " .
					   dbquote($expenses[$posaddress_id]) . ", " .
					   dbquote($operating_income[$posaddress_id]) . ", " .
					   dbquote($wholesale_margin[$posaddress_id]) . ", " .
					   dbquote($operating_income2[$posaddress_id]) . ", " .
						   "'" . user_login() . "', " .
					   "'" . date("Y-m-d H:i:s") . "')";

				$result = mysql_query($sql) or dberror($sql);
			}
			elseif($row = mysql_fetch_assoc($res))
			{
				$sql = "UPDATE possellouts SET " .
					   " possellout_month = " . dbquote($months2[$posaddress_id]) . ", " .
					   " possellout_watches_units = " . dbquote($units_watches2[$posaddress_id]) . ", " .
					   " possellout_bijoux_units = " . dbquote($units_bijoux2[$posaddress_id]) . ", " .
					   " possellout_net_sales = " . dbquote($net_sales[$posaddress_id]) . ", " .
					   " possellout_grossmargin = " . dbquote($gross_margin[$posaddress_id]) . ", " .
					   " possellout_operating_expenses = " . dbquote($expenses[$posaddress_id]) . ", " .
					   " possellout_operating_income_excl = " . dbquote($operating_income[$posaddress_id]) . ", " .
					   " possellout_wholsale_margin = " . dbquote($wholesale_margin[$posaddress_id]) . ", " .
					   " possellout_operating_income_incl = " . dbquote($operating_income2[$posaddress_id]) . 
					   " where possellout_posaddress_id = " . $posaddress_id . 
					   " and possellout_year = " . dbquote($years2[$posaddress_id]);
				$result = mysql_query($sql) or dberror($sql);
			}
			elseif($years2[$posaddress_id] > 0) {
				$sql = "insert into possellouts (" .
					   "possellout_posaddress_id, " . 
					   "possellout_year, " . 
					   "possellout_month, " . 
					   "possellout_watches_units, " .
					   "possellout_bijoux_units, " .
					   "possellout_net_sales, " .
					   "possellout_grossmargin, " .
					   "possellout_operating_expenses, " .
					   "possellout_operating_income_excl, " .
					   "possellout_wholsale_margin, " .
					   "possellout_operating_income_incl, " .
					   "user_created, date_created) " .
					   " values (" .
					   $posaddress_id . ", " .
					   dbquote($years2[$posaddress_id]) . ", " .
					   dbquote($months2[$posaddress_id]) . ", " .
					   dbquote($units_watches2[$posaddress_id]) . ", " .
					   dbquote($units_bijoux2[$posaddress_id]) . ", " .
					   dbquote($net_sales[$posaddress_id]) . ", " .
					   dbquote($gross_margin[$posaddress_id]) . ", " .
					   dbquote($expenses[$posaddress_id]) . ", " .
					   dbquote($operating_income[$posaddress_id]) . ", " .
					   dbquote($wholesale_margin[$posaddress_id]) . ", " .
					   dbquote($operating_income2[$posaddress_id]) . ", " .
						   "'" . user_login() . "', " .
					   "'" . date("Y-m-d H:i:s") . "')";

				$result = mysql_query($sql) or dberror($sql);
			}
			





		}
		if($error > 0)
		{
			$form->error($errors[$error]);
			$error_occured = 1;
		}
		
	}
	
	//Save distribution
	if($error_occured == 0)
	{
		$sql = 	"delete from ln_basicdata_inr03 " . 
				" where ln_basicdata_lnr03_cer_version = 0 " . 
				" and ln_basicdata_lnr03_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"];

		$result = mysql_query($sql) or dberror($sql);

		foreach($include_sellouts_in_ln as $posaddress_id=>$value)
		{
			if(array_key_exists($posaddress_id, $include_profitability_in_ln)
				and $years[$posaddress_id] == $years2[$posaddress_id]) {
				$sql = "insert into ln_basicdata_inr03 (" .
					   "ln_basicdata_lnr03_cerbasicdata_id, " . 
					   "ln_basicdata_lnr03_cer_version, " . 
					   "ln_basicdata_lnr03_posaddress_id_da, " .
					   "ln_basicdata_lnr03_posaddress_id_pr, " .
					   "ln_basicdata_lnr03_year, " . 
					   "ln_basicdata_lnr03_month, " . 
					   "ln_basicdata_lnr03_watches_units, " . 
					   "ln_basicdata_lnr03_bijoux_units, " . 
					   "ln_basicdata_lnr03_grossales, " .
					   "ln_basicdata_lnr03_net_sales, " .
					   "ln_basicdata_lnr03_grossmargin, " .
					   "ln_basicdata_lnr03_operating_expenses, " .
					   "ln_basicdata_lnr03_operating_income_excl, " .
					   "ln_basicdata_lnr03_wholsale_margin, " .
					   "ln_basicdata_lnr03_operating_income_incl, " .
					   "ln_basicdata_lnr03_distance, " .
					   "ln_basicdata_lnr03_distance_unit, " .
					   "ln_basicdata_lnr03_planned_closingdate, " .
					   "user_created, date_created) " .
					   " values (" .
					   $cer_basicdata["cer_basicdata_id"] . ", " .
					   "0, " . 
					   $posaddress_id . ", " .
					   $posaddress_id . ", " .
					   dbquote($years[$posaddress_id]) . ", " .
					   dbquote($months[$posaddress_id]) . ", " .
					   dbquote($units_watches[$posaddress_id]) . ", " .
					   dbquote($units_bijoux[$posaddress_id]) . ", " .
					   dbquote($gross_sales[$posaddress_id]) . ", " .
					   dbquote($net_sales[$posaddress_id]) . ", " .
					   dbquote($gross_margin[$posaddress_id]) . ", " .
					   dbquote($expenses[$posaddress_id]) . ", " .
					   dbquote($operating_income[$posaddress_id]) . ", " .
					   dbquote($wholesale_margin[$posaddress_id]) . ", " .
					   dbquote($operating_income2[$posaddress_id]) . ", " .
					   dbquote($distances[$posaddress_id]) . ", " .
					   dbquote($distance_units[$posaddress_id]) . ", " .
					   dbquote(from_system_date($planned_closingdates[$posaddress_id])) . ", " .
					   "'" . user_login() . "', " .
					   "'" . date("Y-m-d H:i:s") . "')";

				

				unset($include_profitability_in_ln[$posaddress_id]);

			}
			else {
			
				$sql = "insert into ln_basicdata_inr03 (" .
					   "ln_basicdata_lnr03_cerbasicdata_id, " . 
					   "ln_basicdata_lnr03_cer_version, " . 
					   "ln_basicdata_lnr03_posaddress_id_da, " . 
					   "ln_basicdata_lnr03_year, " . 
					   "ln_basicdata_lnr03_month, " . 
					   "ln_basicdata_lnr03_watches_units, " . 
					   "ln_basicdata_lnr03_bijoux_units, " . 
					   "ln_basicdata_lnr03_grossales, " .
					   "ln_basicdata_lnr03_distance, " .
					   "ln_basicdata_lnr03_distance_unit, " .
					   "ln_basicdata_lnr03_planned_closingdate, " .
					   "user_created, date_created) " .
					   " values (" .
					   $cer_basicdata["cer_basicdata_id"] . ", " .
					   "0, " . 
					   $posaddress_id . ", " .
					   dbquote($years[$posaddress_id]) . ", " .
					   dbquote($months[$posaddress_id]) . ", " .
					   dbquote($units_watches[$posaddress_id]) . ", " .
					   dbquote($units_bijoux[$posaddress_id]) . ", " .
					   dbquote($gross_sales[$posaddress_id]) . ", " .
					   dbquote($distances[$posaddress_id]) . ", " .
					   dbquote($distance_units[$posaddress_id]) . ", " .
					   dbquote(from_system_date($planned_closingdates[$posaddress_id])) . ", " .
					   "'" . user_login() . "', " .
					   "'" . date("Y-m-d H:i:s") . "')";

			}

			$result = mysql_query($sql) or dberror($sql);

		}

		foreach($include_profitability_in_ln as $posaddress_id=>$value)
		{
			$sql = "insert into ln_basicdata_inr03 (" .
					   "ln_basicdata_lnr03_cerbasicdata_id, " . 
					   "ln_basicdata_lnr03_cer_version, " . 
					   "ln_basicdata_lnr03_posaddress_id_pr, " .
					   "ln_basicdata_lnr03_year, " . 
					   "ln_basicdata_lnr03_month, " .
				       "ln_basicdata_lnr03_watches_units, " .
				       "ln_basicdata_lnr03_bijoux_units, " .
					   "ln_basicdata_lnr03_net_sales, " .
					   "ln_basicdata_lnr03_grossmargin, " .
					   "ln_basicdata_lnr03_operating_expenses, " .
					   "ln_basicdata_lnr03_operating_income_excl, " .
					   "ln_basicdata_lnr03_wholsale_margin, " .
					   "ln_basicdata_lnr03_operating_income_incl, " .
					   "user_created, date_created) " .
					   " values (" .
					   $cer_basicdata["cer_basicdata_id"] . ", " .
					   "0, " . 
					   $posaddress_id . ", " .
					   dbquote($years2[$posaddress_id]) . ", " .
					   dbquote($months2[$posaddress_id]) . ", " .
				       dbquote($units_watches2[$posaddress_id]) . ", " .
				       dbquote($units_bijoux2[$posaddress_id]) . ", " .
					   dbquote($net_sales[$posaddress_id]) . ", " .
					   dbquote($gross_margin[$posaddress_id]) . ", " .
					   dbquote($expenses[$posaddress_id]) . ", " .
					   dbquote($operating_income[$posaddress_id]) . ", " .
					   dbquote($wholesale_margin[$posaddress_id]) . ", " .
					   dbquote($operating_income2[$posaddress_id]) . ", " .
					   "'" . user_login() . "', " .
					   "'" . date("Y-m-d H:i:s") . "')";

			$result = mysql_query($sql) or dberror($sql);

			
		}

		$link = "cer_application_distribution.php?pid=" . param("pid") . "&context=" . param("context") . "&province=" . param("province");
			redirect($link);
	}

	
}




/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();


if($project["project_projectkind"] == 8)
{
	if($form_type == "INR03")
	{
		$page->title("INR-03 - Retail Furniture in Third-party Store: Distribution analysis of 10 closest POS by proximity in the area");
	}
	elseif($form_type == "AF")
	{
		$page->title("LNR for PopUps: Distribution analysis of 10 closest POS by proximity in the area");
	}
	else
	{
		$page->title("LNR/CER for PopUps: Distribution analysis of 10 closest POS by proximity in the area");
	}
}
else
{
	if($form_type == "INR03")
	{
		$page->title("INR-03 - Retail Furniture in Third-party Store: Distribution analysis of 10 closest POS by proximity in the area");
	}
	elseif($form_type == "AF")
	{
		$page->title("LNR/INR03: Distribution analysis of 10 closest POS by proximity in the area");
	}
	else
	{
		$page->title("LNR/CER: Distribution analysis of 10 closest POS by proximity in the area");
	}
}


require_once("include/tabs_ln2016.php");

$form->render();
$list->render();
echo '<p>&nbsp;</p>';
$list2->render();

?>
<script language="javascript">


<?php
	foreach($corporate_pos as $key=>$posaddress_id)
	{
		$field = '#__posaddresses_include_in_ln_pr_' . $posaddress_id;

		echo '$("' . $field . '").val("1");';
		echo '$("' . $field . '").prop( "checked", true );';
	}
?>

function select_all_corporate_pos() {
	<?php
		foreach($corporate_pos as $key=>$posaddress_id)
		{
			$field = '#__posaddresses_include_in_ln_pr_' . $posaddress_id;

			echo '$("' . $field . '").val("1");';
			echo '$("' . $field . '").prop( "checked", true );';
		}
	?>
}
<?php
foreach($include_sellouts_in_ln as $posaddress_id=>$value)
{
?>
	$( "#__posaddresses_include_in_ln_da_<?php echo $posaddress_id;?>" ).click(function() {
		if($("#__posaddresses_include_in_ln_da_<?php echo $posaddress_id;?>").is(":checked") == false)
		{
			$("#__posaddresses_distances_<?php echo $posaddress_id;?>").val('');
		}
	});

<?php
}
?>
</script>
<?php
require "include/footer_scripts.php";
$page->footer();

?>