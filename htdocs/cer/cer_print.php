<?php
/********************************************************************

    cer_print.php

    Select object to be printed

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-06-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-14
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require "include/get_project.php";


check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/

$result = build_missing_cer_investment_records(param("pid"));

$currencies = array();
$currency_id = 1;
if($cer_basicdata['cer_basicdata_currency']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c1"] = $row['currency_symbol'];
		$currency_id = $row['currency_id'];
	}
}

if($cer_basicdata['cer_basicdata_currency2']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency2'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c2"] = $row['currency_symbol'];
		$currency_id = $row['currency_id'];
	}
}

//get ln versions
$ln_versions = array();
$latest_version = 0;
$latest_submitted_ln_version = 0;
$latest_submitted_cer_version = 0;
$latest_version_date = "";
$latest_ln_version_date = "";
$link_lnr_booklet = "";
$sql = "select ln_basicdata_version, ln_basicdata_version_cer_version, 
        ln_basicdata.date_created as date1, ln_basicdata_resubmitted, ln_basicdata_submitted, ln_basicdata_rejected " . 
       "from ln_basicdata " .
	   "left join users on user_id = ln_basicdata_version_user_id " . 
	   "where ln_basicdata_project = " . param("pid") .
	   " order by ln_basicdata_version asc";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
		if($row["ln_basicdata_version"] == 0 and $ln_basicdata["ln_basicdata_locked"] == 0)
		{
			$latest_version = $row["ln_basicdata_version"];
			$latest_version_date = to_system_date(substr($row["date1"], 0, 10)) . " " . substr($row["date1"], 10, 9);

			$url = "lnr_sg_booklet_pdf.php?pid=" . param("pid") . "&lnversion=" . $row["ln_basicdata_version"] . "&cerversion=" . $row["ln_basicdata_version_cer_version"];	
			
			$link_lnr_booklet = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";
			

			$url = "/cer/cer_inr02_pdf.php?pid=" . param("pid") . "&cid=s&cerversion=" . $row["ln_basicdata_version_cer_version"];
			$link_bp01v = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


			$url = "/cer/cer_inr02_pdf.php?pid=" . param("pid") . "&cid=c1&cerversion=" . $row["ln_basicdata_version_cer_version"];
			$link_bp02v = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

			$url = "/cer/cer_inr02_pdf.php?pid=" . param("pid") . "&cid=c2&cerversion=" . $row["ln_basicdata_version_cer_version"];
			$link_bp02v2 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

			$url = "/cer/cer_inr02_extension_pdf.php?pid=" . param("pid") . "&cerversion=" . $row["ln_basicdata_version_cer_version"];
			$link_bp03v = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";
		}
		else
		{
			if($row["ln_basicdata_resubmitted"] != NULL 
				and substr($row["ln_basicdata_resubmitted"],0,4) != '0000'
				and $row["ln_basicdata_rejected"] <= $row["ln_basicdata_resubmitted"])
			{
				$latest_submitted_ln_version = $row["ln_basicdata_version"];
				$latest_submitted_cer_version = $row["ln_basicdata_version_cer_version"];
				$latest_ln_version_date = to_system_date(substr($row["date1"], 0, 10)) . " " . substr($row["date1"], 10, 9);
			}
			elseif($row["ln_basicdata_submitted"] != NULL 
				and substr($row["ln_basicdata_submitted"], 0, 4) != '0000'
				and $row["ln_basicdata_rejected"] <= $row["ln_basicdata_submitted"])
			{
				$latest_submitted_ln_version = $row["ln_basicdata_version"];
				$latest_submitted_cer_version = $row["ln_basicdata_version_cer_version"];
				$latest_ln_version_date = to_system_date(substr($row["date1"], 0, 10)) . " " . substr($row["date1"], 10, 9);
			}
			else {
				$latest_submitted_ln_version = $row["ln_basicdata_version"];
				$latest_submitted_cer_version = $row["ln_basicdata_version_cer_version"];
				$latest_ln_version_date = to_system_date(substr($row["date1"], 0, 10)) . " " . substr($row["date1"], 10, 9);
			}

			$url = "lnr_sg_booklet_pdf.php?pid=" . param("pid") . "&lnversion=" . $row["ln_basicdata_version"] . "&cerversion=" . $row["ln_basicdata_version_cer_version"];
			$link_lnr_bookletv = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

			
			if($row["ln_basicdata_version"] > 0) {
				$tmp = to_system_date(substr($row["date1"], 0, 10)) . " " . substr($row["date1"], 10, 9);
				$ln_versions[$row["ln_basicdata_version"]] = array('date'=>$tmp, 'link0'=>$link_lnr_bookletv);
			}
		}
}


//get cer versions
// only show the latest version
$cer_versions = array();
$inr_versions = array();
$sql = "select cer_basicdata_version, cer_basicdata_version_context, cer_basicdata.date_created as date1 " . 
       "from cer_basicdata " .
	   "left join users on user_id = cer_basicdata_version_user_id " . 
	   "where cer_basicdata_version > 0 " .
	   " and cer_basicdata_project = " . param("pid") .
	   " and cer_basicdata_version_context = 'cer' " . 
	   " order by cer_basicdata_version";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
		$url2 = '';
		if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
		{
			$url = "cer_sg_booklet_pdf.php?pid=" . param("pid") . "&cerversion=" . $row["cer_basicdata_version"];
		}
		elseif($form_type == "INR03")
		{
			$url = "cer_sg_form_inr03_pdf.php?pid=" . param("pid") . "&cerversion=" . $row["cer_basicdata_version"] . "&booklet=true";
		}
		else
		{
			$url = "lnr_sg_booklet_pdf.php?pid=" . param("pid") . "&cerversion=" . $row["cer_basicdata_version"];

			$url2 = "cer_sg_form_inr03_pdf.php?pid=" . param("pid") . "&cerversion=" . $row["cer_basicdata_version"] . "&booklet=true";
		}
		
		$link_cer_bookletv = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

		$tmp = to_system_date(substr($row["date1"], 0, 10)) . " " . substr($row["date1"], 10, 9);
		$cer_versions[$row["cer_basicdata_version"]] = array('date'=>$tmp, 'link0'=>$link_cer_bookletv);


		if($url2) {
			$link_cer_bookletinr = "<a href=\"javascript:popup('" . $url2 . "', 1024, 768);\">Print</a>";

			$tmp = to_system_date(substr($row["date1"], 0, 10)) . " " . substr($row["date1"], 10, 9);
			$inr_versions[$row["cer_basicdata_version"]] = array('date'=>$tmp, 'link0'=>$link_cer_bookletinr);
		}
}

$url = "cer_mails_pdf.php?pid=" . $project["project_id"];
$link_malbox = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";



$url = "cer_mails_pdf.php?pid=" . param("pid");
$link_malbox = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "cer_rental_cost_versions_pdf.php?pid=" . $project["project_id"];
$link_rental_cost_overview = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "/user/project_costs_budget_pdf.php?pid=" . param("pid") . "&sc=0";
$link_budget = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "/user/project_costs_bid_comparison_pdf.php?pid=" . param("pid") . "&lwoid=0";
$link_lw = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";



if($use_old_cer_forms_before_2013 == true)
{
	$url = "af_booklet_pdf.php?pid=" . param("pid");
}
else
{
	$url = "af_booklet_2013_pdf.php?pid=" . param("pid");
}
$link_af_booklet = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "cer_booklet_pdf.php?pid=" . param("pid") . "&v=0";
$link_cer_booklet_before_2013 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";





$url = "cer_in01_pdf.php?pid=" . param("pid"). "&v=0";
$link_summary_in01_before_2013 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "/cer/cer_inr02_pdf.php?pid=" . param("pid") . "&cid=s";
$link_bp01 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "/cer/cer_inr02_pdf.php?pid=" . param("pid") . "&cid=c1";
$link_bp02 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "/cer/cer_inr02_pdf.php?pid=" . param("pid") . "&cid=c2";
$link_bp022 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "/cer/cer_inr02_extension_pdf.php?pid=" . param("pid");
$link_bp03 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "cer_inr02_pdf_debug.php?pid=" . param("pid");
$link_bp04 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "af_inr03_pdf.php?pid=" . param("pid");
$link_iaf_af = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "ln_lease_details_pdf.php?pid=" . param("pid");
$link_lease = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "cer_listofvars_pdf.php?pid=" . param("pid");
$link_lvars = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "/user/project_print_project_data.php?pid=" . param("pid");
$link_project_data = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "cer_booklet_popup_inr01_pdf.php?pid=" . param("pid");
$cer_popup = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "af_booklet_popup_inr01_pdf.php?pid=" . param("pid");
$af_popup = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";



//links for corporate projects
//LNR
if($latest_submitted_ln_version > 0) {
	
	$url = "lnr_sg_booklet_pdf.php?pid=" . param("pid") . "&lnversion=" . $latest_submitted_ln_version . "&cerversion=" . $latest_submitted_cer_version;
	$link_lnr_booklet = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";
	
	
	$url = "lnr_sg_form_pdf.php?pid=" . param("pid") . "&cid=s&lnversion=" . $latest_submitted_ln_version . "&cerversion=" . $latest_submitted_cer_version;
	$link_lnr = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


	$url = "/cer/cer_inr02_pdf.php?pid=" . param("pid") . "&cid=s&cerversion=" . $latest_submitted_cer_version;
	$link_bp01v = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


	$url = "/cer/cer_inr02_pdf.php?pid=" . param("pid") . "&cid=c1&cerversion=" . $latest_submitted_cer_version;
	$link_bp02v = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

	$url = "/cer/cer_inr02_pdf.php?pid=" . param("pid") . "&cid=c2&cerversion=" . $row["ln_basicdata_version_cer_version"];
	$link_bp02v2 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

	$url = "/cer/cer_inr02_extension_pdf.php?pid=" . param("pid") . "&cerversion=" . $latest_submitted_cer_version;
	$link_bp03v = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";
}
else {
	$url = "lnr_sg_form_pdf.php?pid=" . param("pid") . "&cid=s";
	$link_lnr = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";
}
//CER

$url = "/cer/cer_sg_form_pdf.php?pid=" . param("pid") . "&cid=c1";
$link_cer = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "/cer/cer_sg_form_inr03_pdf.php?pid=" . param("pid") . "&cid=c1";
$link_inr03 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "cer_sg_booklet_pdf.php?pid=" . param("pid");
$link_cer_booklet = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";


$url = "cer_sg_form_inr02c_pdf.php?pid=" . param("pid") . "&cid=s";
$link_inr02c_1 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "cer_sg_form_inr02c_pdf.php?pid=" . param("pid") . "&cid=c1";
$link_inr02c_2 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

//links for non corporate projects
// AF
if($latest_submitted_ln_version > 0) {
	$url = "lnr_sg_form_pdf.php?pid=" . param("pid") . "&cid=s&lnversion=" . $latest_submitted_ln_version . "&cerversion=" . $latest_submitted_cer_version;
	$link_af_form = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";
}
else {
	$url = "lnr_sg_form_pdf.php?pid=" . param("pid") . "&cid=s";
	$link_af_form = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";
}

//Furniture
$url = "cer_sg_form_inr03_pdf.php?pid=" . param("pid");
$link_inr03 = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

$url = "cer_sg_form_inr03_pdf.php?pid=" . param("pid") . '&booklet=true';
$link_inr03_booklet = "<a href=\"javascript:popup('" . $url . "', 1024, 768);\">Print</a>";

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("cer_basicdata", "cer_basicdata");
$form->add_hidden("pid", param("pid"));

include("include/project_head.php");


if($project["project_projectkind"] == 8)  //popUp
{
	if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
	{
		$form->add_section("INR01");
		$form->add_label("cer", "INR01 Booklet ", RENDER_HTML, $cer_popup);
	}
	else
	{
		$form->add_section("LNR");
		$form->add_label("af", "LNR Booklet", RENDER_HTML, $af_popup);
	}
}
else
{
	if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
	{
		
		$form->add_section("LNR");
		
		
		$form->add_label("lnr", "LNR Booklet " . $latest_ln_version_date, RENDER_HTML, $link_lnr_booklet);
		

		$form->add_label("lnr01", "LNR " . $latest_ln_version_date, RENDER_HTML, $link_lnr);
		
		$form->add_label("bp01v", "Business Plan in CHF " . $latest_ln_version_date, RENDER_HTML, $link_bp01v);
		if($currencies["c1"] != 'CHF') {
			$form->add_label("bp02v", "Business Plan in " . $currencies["c1"]  . " " . $latest_ln_version_date, RENDER_HTML, $link_bp02v);
		}

		if(array_key_exists("c2", $currencies) and $currencies["c2"] and $currencies["c1"] != $currencies["c2"])
		{
			$form->add_label("bp02v2", "Business Plan in " . $currencies["c2"] . "  " . $latest_ln_version_date, RENDER_HTML, $link_bp02v2);
		}

		$form->add_label("bp03v", "Business Plan Extension " . $latest_ln_version_date, RENDER_HTML, $link_bp03v);

		


		$form->add_section("CER");
		$form->add_label("cer_booklet", "CER Booklet", RENDER_HTML, $link_cer_booklet);
		$form->add_label("cer_forms", "CER Forms", RENDER_HTML, $link_cer);

		
		

		$form->add_label("bp01", "Business Plan in CHF ", RENDER_HTML, $link_bp01);
		
		if($currencies["c1"] != 'CHF') {
			$form->add_label("bp02", "Business Plan in " . $currencies["c1"], RENDER_HTML, $link_bp02);
		}

		if(array_key_exists("c2", $currencies) and $currencies["c2"] and $currencies["c1"] != $currencies["c2"])
		{
			$form->add_label("bp022", "Business Plan in " . $currencies["c2"], RENDER_HTML, $link_bp022);
		}


		$form->add_label("inr02c_1", "CER versus LNR Figures (INR-02C) in CHF", RENDER_HTML, $link_inr02c_1);
		$form->add_label("inr02c_2", "CER versus LNR Figures (INR-02C) in " . $currencies["c1"], RENDER_HTML, $link_inr02c_2);


		$form->add_label("bp03", "Business Plan Extension ", RENDER_HTML, $link_bp03);

		
		if(has_access("can_edit_catalog"))
		{
			$form->add_label("bp04", "Business Plan Debugging ", RENDER_HTML, $link_bp04);
		}

		if (has_access("has_access_to_all_projects") or has_access("can_view_budget_in_projects"))
		{
			$form->add_section("Project");
			$form->add_label("project_data", "Project Data", RENDER_HTML, $link_project_data);
			$form->add_label("budget", "Project's Budget", RENDER_HTML, $link_budget);
			$form->add_label("local_work", "Suppliter's Quotes", RENDER_HTML, $link_lw);
		}


		$form->add_section("Other");
		$form->add_label("lvars", "List of Variables", RENDER_HTML, $link_lvars);
		$form->add_label("lease", "Lease Details", RENDER_HTML, $link_lease);
		$form->add_label("cer_mailbox", "Mailbox", RENDER_HTML, $link_malbox);

		if(count($ln_versions) > 0)
		{
			$form->add_label("cer_rental_cost_overviewl", "Versions - Rental Cost Overview", RENDER_HTML, $link_rental_cost_overview);
		}


		
		
		if(count($ln_versions) > 0)
		{
			$form->add_section("Older LNR Versions");
			foreach($ln_versions as $version=>$data)
			{
				$form->add_label("lnr" . $version, "LNR Booklet " . $data['date'], RENDER_HTML, $data['link0']);
				
			}
		}


		if(count($cer_versions) > 0)
		{
			$form->add_section("Older CER Versions");
			foreach($cer_versions as $version=>$data)
			{
				$form->add_label("cer" . $version, "CER Booklet " . $data['date'], RENDER_HTML, $data['link0']);
				//$form->add_label("bp01v" . $version, "Business Plan in CHF ", RENDER_HTML, $data['link1']);
				//$form->add_label("bp02v" . $version, "Business Plan in " . $currencies["c1"], RENDER_HTML, $data['link2']);
				//$form->add_label("bp03v", "Business Plan Extension ", RENDER_HTML, $data['link3']);
			}
		}
		


		/*
		$form->add_section("Booklets befor June 2013");
		$form->add_label("ln_booklet_before_2013", "LNR Booklet", RENDER_HTML, $link_ln_booklet_before_2013);
		$form->add_label("cer_booklet_before_2013", "CER Booklet", RENDER_HTML, $link_cer_booklet_before_2013);

		$form->add_section("Single Forms before June 2013");
		$form->add_label("summary_inr01_before_2013", "Summary INR-01", RENDER_HTML, $link_summary_in01_before_2013);
		$form->add_label("ln_form_before_2013_before_2013", "LNR Form", RENDER_HTML, $link_ln_form_before_2013);
		$form->add_label("investment_approval", "Investment Approval Form", RENDER_HTML, $link_iaf);
		*/

		$page_title = "Select Object to be Printed";

	}
	elseif($form_type == "AF")
	{
		$form->add_section("LNR");
		
		$form->add_label("cer_booklet", "LNR Booklet " . $latest_ln_version_date, RENDER_HTML, $link_lnr_booklet);
		$form->add_label("investment_investment_form", "LNR" . $latest_ln_version_date, RENDER_HTML, $link_af_form);

		$form->add_section("Retail Furniture");
		$form->add_label("inr03booklet", "INR-03 Booklet", RENDER_HTML, $link_inr03_booklet);
		$form->add_label("inr03", "Request - Retail Furniture Third Party INR-03", RENDER_HTML, $link_inr03);
		
		

		
		$form->add_section("Business Plan");
		$form->add_label("bp01", "Business Plan in CHF", RENDER_HTML, $link_bp01);
		$form->add_label("bp02", "Business Plan in " . $currencies["c1"], RENDER_HTML, $link_bp02);
		if(array_key_exists('c2', $currencies)
			and $currencies["c2"] and $currencies["c1"] != $currencies["c2"])
		{
			$form->add_label("bp02", "Business Plan in " . $currencies["c2"], RENDER_HTML, $link_bp022);
		}
		
		$form->add_label("bp03", "Business Plan Extension ", RENDER_HTML, $link_bp03);
		
		
		if(has_access("can_edit_catalog"))
		{
			$form->add_label("bp04", "Business Plan Debugging ", RENDER_HTML, $link_bp04);
		}

		if (has_access("has_access_to_all_projects") or has_access("can_view_budget_in_projects"))
		{
			$form->add_section("Project");
			$form->add_label("project_data", "Project Data", RENDER_HTML, $link_project_data);
			$form->add_label("budget", "Project's Budget", RENDER_HTML, $link_budget);
			$form->add_label("local_work", "Suppliter's Quotes", RENDER_HTML, $link_lw);
		}


		$form->add_section("Other");
		$form->add_label("lvars", "List of Variables", RENDER_HTML, $link_lvars);
		$form->add_label("lease", "Lease Details", RENDER_HTML, $link_lease);
		$form->add_label("cer_mailbox", "Mailbox", RENDER_HTML, $link_malbox);



		if(count($cer_versions) > 0)
		{
			$form->add_section("Older LNR Versions");
			foreach($cer_versions as $version=>$data)
			{
				$form->add_label("cer" . $version, "LNR Booklet " . $data['date'], RENDER_HTML, $data['link0']);
				//$form->add_label("bp01v" . $version, "Business Plan in CHF ", RENDER_HTML, $data['link1']);
				//$form->add_label("bp02v" . $version, "Business Plan in " . $currencies["c1"], RENDER_HTML, $data['link2']);
				//$form->add_label("bp03v", "Business Plan Extension ", RENDER_HTML, $data['link3']);
			}
		}


		if(count($inr_versions) > 0)
		{
			$form->add_section("Older INR03 Versions");
			foreach($inr_versions as $version=>$data)
			{
				$form->add_label("cer2" . $version, "INR03 Booklet " . $data['date'], RENDER_HTML, $data['link0']);
				//$form->add_label("bp01v" . $version, "Business Plan in CHF ", RENDER_HTML, $data['link1']);
				//$form->add_label("bp02v" . $version, "Business Plan in " . $currencies["c1"], RENDER_HTML, $data['link2']);
				//$form->add_label("bp03v", "Business Plan Extension ", RENDER_HTML, $data['link3']);
			}
		}

		$page_title = "Select Object to be Printed";
	}
	elseif($form_type == "INR03")
	{
		$form->add_section("INR03");
		$form->add_label("inr03booklet", "INR-03 Booklet", RENDER_HTML, $link_inr03_booklet);
		$form->add_label("inr03", "Request - Retail Furniture Third Party INR-03", RENDER_HTML, $link_inr03);
		
		$form->add_section("Business Plan");
		$form->add_label("bp01", "Business Plan in CHF", RENDER_HTML, $link_bp01);
		$form->add_label("bp02", "Business Plan in " . $currencies["c1"], RENDER_HTML, $link_bp02);
		if(array_key_exists('c2', $currencies) and $currencies["c1"] != $currencies["c2"])
		{
			$form->add_label("bp02", "Business Plan in " . $currencies["c1"], RENDER_HTML, $link_bp022);
		}
		
		$form->add_label("bp03", "Business Plan Extension ", RENDER_HTML, $link_bp03);
		
		
		if(has_access("can_edit_catalog"))
		{
			$form->add_label("bp04", "Business Plan Debugging ", RENDER_HTML, $link_bp04);
		}

		if (has_access("has_access_to_all_projects") or has_access("can_view_budget_in_projects"))
		{
			$form->add_section("Project");
			$form->add_label("project_data", "Project Data", RENDER_HTML, $link_project_data);
			$form->add_label("budget", "Project's Budget", RENDER_HTML, $link_budget);
			$form->add_label("local_work", "Suppliter's Quotes", RENDER_HTML, $link_lw);
		}


		$form->add_section("Other");
		$form->add_label("lvars", "List of Variables", RENDER_HTML, $link_lvars);
		$form->add_label("cer_mailbox", "Mailbox", RENDER_HTML, $link_malbox);



		if(count($cer_versions) > 0)
		{
			$form->add_section("Older INR03 Versions");
			foreach($cer_versions as $version=>$data)
			{
				$form->add_label("cer" . $version, "INR03 Booklet " . $data['date'], RENDER_HTML, $data['link0']);
				//$form->add_label("bp01v" . $version, "Business Plan in CHF ", RENDER_HTML, $data['link1']);
				//$form->add_label("bp02v" . $version, "Business Plan in " . $currencies["c1"], RENDER_HTML, $data['link2']);
				//$form->add_label("bp03v", "Business Plan Extension ", RENDER_HTML, $data['link3']);
			}
		}

		$page_title = "Select Object to be Printed";
	}
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();




/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("cer_projects");

require "include/project_page_actions.php";

$page->header();
$page->title($page_title);


$form->render();


$page->footer();
?>