<?php
/********************************************************************

    ln_project_information.php

    Application Form: lease negiotiation form
    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2018-02-22
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2018-02-22
    Version:        1.0.0

    Copyright (c) 2018, Tissot, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_cer");




$deletable = "";
if(has_access("can_delete_uploaded_files"))
{
	$deletable = DELETABLE;
}


$project = get_project(param("pid"));

$client_address = get_address($project["order_client_address"]);

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_address"] . ", " .
		$project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];

$project_detail = $project["project_number"] . ", " . $project["project_costtype_text"] . " " . $project["postype_name"] . ", " . $project["product_line_name"] . ", " . $project["projectkind_name"];




/********************************************************************
    build form
*********************************************************************/
$form = new Form("ln_basicdata", "ln_basicdata");

$form->add_hidden("pid", param("pid"));

include("include/project_head.php");


if($project['project_projectkind'] == 2
	or $project['project_projectkind'] == 5 ) {
	
	$form->add_section("Latest Renovation");
	$form->add_comment("Pleae indicate the date of the latest renovation of this POS.");
	$form->add_edit("ln_basicdata_latest_renovation", "Date", 0 , to_system_date($ln_basicdata['ln_basicdata_latest_renovation']), TYPE_DATE);
}
else {
	$form->add_hidden("ln_basicdata_latest_renovation");
}


$form->add_section("City Overview");
$form->add_comment("Please provide a description.");
$form->add_multiline("ln_basicdata_area", "Area/Population*", 4, NOTNULL, $ln_basicdata["ln_basicdata_area"]);
$form->add_multiline("ln_basicdata_economics", "Economic Statistics*", 4, NOTNULL, $ln_basicdata["ln_basicdata_economics"]);
$form->add_multiline("ln_basicdata_cityinfo", "General Information*", 8, NOTNULL, $ln_basicdata["ln_basicdata_cityinfo"]);


$form->add_section("Project Introduction");

$form->add_edit("ln_basicdata_basic_developer", "Developer*", NOTNULL , $ln_basicdata['ln_basicdata_basic_developer']);

$form->add_edit("ln_basicdata_numfloors", "Number of floors*", NOTNULL , $ln_basicdata['ln_basicdata_numfloors']);
$form->add_edit("ln_basicdata_parking_space", "Parking Space*", NOTNULL , $ln_basicdata['ln_basicdata_parking_space']);
$form->add_multiline("ln_basicdata_basic_info", "Basic Information*", 8, NOTNULL, $ln_basicdata["ln_basicdata_basic_info"]);
$form->add_multiline("ln_basicdata_positioning", "Positioning*", 8, NOTNULL, $ln_basicdata["ln_basicdata_positioning"]);
$form->add_edit("ln_basicdata_target_customers", "Target Customers*", NOTNULL , $ln_basicdata['ln_basicdata_target_customers']);
$form->add_comment("Total Construction Floor Area and Sourroundings: Please indicate <strong>overall</strong> and <strong>retail</strong> area in square meters.");
$form->add_multiline("ln_basicdata_total_floor_area", "Total Construction Floor Area and Sourroundings*", 8, NOTNULL, $ln_basicdata["ln_basicdata_total_floor_area"]);
$form->add_multiline("ln_basicdata_project_specials", "Speciality of this Project*", 8, NOTNULL, $ln_basicdata["ln_basicdata_project_specials"]);


if(($project["order_actual_order_state_code"] < '890' 
	and $ln_basicdata["ln_basicdata_locked"] == 0 
	and has_access("has_access_to_his_cer") 
	and $project["project_state"] != 2) 
	or (has_access("has_full_access_to_cer") and $project["project_state"] != 2))
{

	$form->add_button("save", "Save Data");

}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();





/********************************************************************
    Process Form
*********************************************************************/

if($form->button("save"))
{	
	if($form->validate()) 
	{
		//update ln_basic_data
		$fields = array();

		
		$value = dbquote($form->value("ln_basicdata_area"));
		$fields[] = "ln_basicdata_area = " . $value;

		$value = dbquote($form->value("ln_basicdata_economics"));
		$fields[] = "ln_basicdata_economics = " . $value;

		$value = dbquote($form->value("ln_basicdata_cityinfo"));
		$fields[] = "ln_basicdata_cityinfo = " . $value;

		$value = dbquote($form->value("ln_basicdata_basic_developer"));
		$fields[] = "ln_basicdata_basic_developer = " . $value;

		$value = dbquote($form->value("ln_basicdata_numfloors"));
		$fields[] = "ln_basicdata_numfloors = " . $value;

		$value = dbquote($form->value("ln_basicdata_parking_space"));
		$fields[] = "ln_basicdata_parking_space = " . $value;

		$value = dbquote($form->value("ln_basicdata_basic_info"));
		$fields[] = "ln_basicdata_basic_info = " . $value;

		$value = dbquote($form->value("ln_basicdata_positioning"));
		$fields[] = "ln_basicdata_positioning = " . $value;

		$value = dbquote($form->value("ln_basicdata_target_customers"));
		$fields[] = "ln_basicdata_target_customers = " . $value;

		$value = dbquote($form->value("ln_basicdata_total_floor_area"));
		$fields[] = "ln_basicdata_total_floor_area = " . $value;

		$value = dbquote($form->value("ln_basicdata_project_specials"));
		$fields[] = "ln_basicdata_project_specials = " . $value;
		
		if($form->value("ln_basicdata_latest_renovation")) {
			$value = dbquote(from_system_date($form->value("ln_basicdata_latest_renovation")));
			$fields[] = "ln_basicdata_latest_renovation = " . $value;
		}
		


		$value1 = "current_timestamp";
		$project_fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$project_fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update ln_basicdata set " . join(", ", $fields) . 
			" where ln_basicdata_project = " . param("pid") .
			" and ln_basicdata_version = 0";
		
		mysql_query($sql) or dberror($sql);

		
		$link = "ln_project_information.php?pid=" . param("pid");
		redirect($link);
	}
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();


if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1)) {
	$page->title("LNR/CER: Project Information");
}
elseif($form_type == "AF") {
	$page->title("LNR/INR03: Project Information");
}
else {
	$page->title("INR-03 - Retail Furniture in Third-party Store: Project Information");
}

require_once("include/tabs_ln2016.php");


$form->render();

require "include/footer_scripts.php";


$page->footer();

?>