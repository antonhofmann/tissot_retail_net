<?php
/********************************************************************

    cer_application_expense.php

    Add/edit New expense position

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_cer");

if(!has_access("has_full_access_to_cer") and !has_access("has_access_to_his_cer"))
{
	redirect("noaccess.php");
}


$apply_new_version_of_rental_tool = 0;

if(($ln_basicdata["ln_basicdata_submitted"] == NULL 
   or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == '0000')
   and ($cer_basicdata["cer_basicdata_submitted"] == NULL 
   or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == '0000'))
{
	$apply_new_version_of_rental_tool = 1;
}
elseif($ln_basicdata["ln_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014
	and ($cer_basicdata["cer_basicdata_submitted"] == NULL 
	or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == '0000'))
{
	$apply_new_version_of_rental_tool = 1;
}
elseif(($ln_basicdata["ln_basicdata_submitted"] == NULL 
   or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == '0000')
   and $cer_basicdata["cer_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014)
{
	$apply_new_version_of_rental_tool = 1;
}
elseif($ln_basicdata["ln_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014
       and $cer_basicdata["cer_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014)
{
	$apply_new_version_of_rental_tool = 1;
}


/********************************************************************
    prepare all data needed
*********************************************************************/
$posdata = get_pos_data($project["project_order"]);
$currency = get_cer_currency(param("pid"));


if(id() == 16) // turn over based rents, update data
{
	$result = check_expenses(param("pid"), $cer_basicdata["cer_basicdata_firstyear"], $cer_basicdata["cer_basicdata_lastyear"]);
}


$expense_type_name = "";
$amounts = array();
$sales_percents = array();
$ids = array();
$sales_percent_ids = array();
$edidables = array();
$inflated = array();
$total_sales_percents = array();
$sql = "select * " .
	   "from cer_expenses " .
	   "left join cer_expense_types on cer_expense_type_id = cer_expense_type " . 
	   "where cer_expense_type = " . id() .
	   " and cer_expense_project = " . param("pid") .
	   " and cer_expense_cer_version = 0 " . 
	   " order by cer_expense_year";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$expense_type_name = $row["cer_expense_type_name"];
	$amounts[$row["cer_expense_year"]] = $row["cer_expense_amount"];
	$ids[$row["cer_expense_year"]] = $row["cer_expense_id"];
	$edidables[$row["cer_expense_year"]] = $row["cer_expense_type_editable"];
	$comments[$row["cer_expense_year"]] = $row["cer_expense_comment"];
}


$sql = "select * " .
	   "from cer_rent_percent_from_sales " .
	   "where cer_rent_percent_from_sale_project = " . param("pid") .
	   " and cer_rent_percent_from_sale_cer_version = 0 " . 
	   " order by cer_rent_percent_from_sale_year";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$sales_percent_ids[$row["cer_rent_percent_from_sale_year"]] = $row["cer_rent_percent_from_sale_id"];
	$sales_percents[$row["cer_rent_percent_from_sale_year"]] = $row["cer_rent_percent_from_sale_percent"];
}

$total_rents = get_total_rents(param("pid"));
$total_sales = get_gross_sale_values(param("pid"));

foreach($total_sales as $year=>$value)
{
	$total_sales_percents[$year] = "";
	if($value > 0)
	{
		$total_sales_percents[$year] = round(100*$total_rents[$year]/$value, 2);
	}
}

//check if CER was approved
$can_edit_fixed_rent = true;
$milestone = get_project_milestone(param("pid"), 12);
$milestone2 = get_project_milestone(param("pid"), 21);
if(count($milestone) > 0 
   and ($milestone['project_milestone_date'] == NULL or $milestone['project_milestone_date'] == '0000-00-00')
   and count($milestone2) > 0 and ($milestone2['project_milestone_date'] == NULL or $milestone2['project_milestone_date'] == '0000-00-00'))
{
	$can_edit_fixed_rent = false;
}
elseif(count($milestone) > 0 
   and ($milestone['project_milestone_date'] == NULL or $milestone['project_milestone_date'] == '0000-00-00'))
{
	$can_edit_fixed_rent = false;
}
elseif(count($milestone2) > 0 
   and ($milestone2['project_milestone_date'] == NULL or $milestone2['project_milestone_date'] == '0000-00-00'))
{
	$can_edit_fixed_rent = false;
}


/********************************************************************
	build form
*********************************************************************/
$form = new Form("cer_expenses", "CER Expenses");

include("include/project_head.php");

$form->add_hidden("pid", param("pid"));



$form->add_section($expense_type_name . " in " . $currency["symbol"]);

if(id() == 2 and $apply_new_version_of_rental_tool = 0) //fixed rental cost
{
	$form->add_comment("Please indicate <strong>ONLY FIXED</strong> rental cost. <strong>DO NOT</strong> add turnover based rental cost here!");
}
elseif(id() == 20) // reduction of rental expenses
{
	$form->add_comment('Please enter the reduction as <span class="error">minus values</span> (negative sign)!');
}

if(id() == 16 and $apply_new_version_of_rental_tool == 0 and user_id() != 2094) // turn over based rents (special access for Janine)
{
	if($cer_basicdata["cer_basicdata_tob_from_net_sales"] == 1)
	{
		$form->add_section("Total Annual Rent in % of Net Sales Values in " . $currency["symbol"]);
		$form->add_comment("Please indicate the total annual rent as a percentage from net sales.");
	}
	else
	{
		$form->add_section("Total Annual Rent in % of Gross Sales Values in " . $currency["symbol"]);
		$form->add_comment("Please indicate the total annual rent as a percentage from gross sales.");
	}
	
	foreach($sales_percents as $year=>$sales_percent)
	{
		$form->add_edit("p" . $sales_percent_ids[$year], "Percent " . $year, 0, $sales_percent, TYPE_DECIMAL, 12,9);
	}

	foreach($amounts as $year=>$amount)
	{
		$form->add_label("r" . $ids[$year], "Amount " . $year . " in " . $currency["symbol"], 0, $amount);
	}

	
	$form->add_button("save_form", "Save");
	$form->add_button("back", "Back");
	

	$form->populate();
	$form->process();

	if($form->button("save_form"))
	{
		if($form->validate())
		{
			foreach($sales_percent_ids as $year=>$id)
			{
				$fields = array();
		
				$value = dbquote($form->value("p" . $sales_percent_ids[$year]));
				$fields[] = "cer_rent_percent_from_sale_percent = " . $value;

				$value = "current_timestamp";
				$fields[] = "date_modified = " . $value;

				if (isset($_SESSION["user_login"]))
				{
					$value = $_SESSION["user_login"];
					$fields[] = "user_modified = " . dbquote($value);
				}
		   
				$sql = "update cer_rent_percent_from_sales set " . join(", ", $fields) . 
					" where cer_rent_percent_from_sale_id = " . $id . 
					" and cer_rent_percent_from_sale_cer_version = 0 ";
				mysql_query($sql) or dberror($sql);

		
			}

			
			if($apply_new_version_of_rental_tool == 0)
			{
				$result = update_turnoverbased_rental_cost_version2013(param("pid"), $cer_basicdata["cer_basicdata_add_tob_rents"], $cer_basicdata["cer_basicdata_tob_from_net_sales"]);
			}
			else
			{
				$result = update_turnoverbased_rental_cost_version2014(param("pid"));
			}

			$link = "cer_application_expenses.php?pid=" . param("pid");
			redirect($link);
		}
	}
	elseif($form->button("back"))
	{
		$link = "cer_application_expenses.php?pid=" . param("pid");
		redirect($link);
	}



}
elseif(id() == 16 and $apply_new_version_of_rental_tool == 1 and user_id() != 2094) // turn over based rents (special access for Janine)
{
	if($cer_basicdata["cer_basicdata_tob_from_net_sales"] == 1)
	{
		$form->add_section("Total Annual Rent in % of Net Sales Values in " . $currency["symbol"]);
	}
	else
	{
		$form->add_section("Total Annual Rent in % of Gross Sales Values in " . $currency["symbol"]);
	}
	
	foreach($amounts as $year=>$amount)
	{
		$form->add_label("r" . $ids[$year], "Amount " . $year . " in " . $currency["symbol"], 0, $amount);
	}

	$form->add_button("back", "Back");
	

	$form->populate();
	$form->process();

	if($form->button("back"))
	{
		$link = "cer_application_expenses.php?pid=" . param("pid");
		redirect($link);
	}



}
elseif(id() == 2 
	and $can_edit_fixed_rent == false 
	and $apply_new_version_of_rental_tool == 0
	 and user_id() != 2094)
{
	foreach($amounts as $year=>$amount)
	{
			$form->add_label("r" . $ids[$year], "Amount " . $year . " in " . $currency["symbol"], 0, $amount);
			$form_is_editable = 0;
	}
}
elseif(id() == 2 
	and $apply_new_version_of_rental_tool == 1
	 and user_id() != 2094
	) // fixed rents after introduction of the rent calculator tool
{
	
	foreach($amounts as $year=>$amount)
	{
			$form->add_label("r" . $ids[$year], "Amount " . $year . " in " . $currency["symbol"], 0, $amount);
			$form_is_editable = 0;
	}
}
else // all other expenses
{

	
	$form_is_editable = 1;
	foreach($amounts as $year=>$amount)
	{
		if($edidables[$year] == 1 or (id() == 3 and $apply_new_version_of_rental_tool == 0))
		{
			$form->add_edit("r" . $ids[$year], "Amount " . $year . " in " . $currency["symbol"], 0, $amount, TYPE_INT, 12);
			
			if(id() == 7 or id() == 10) // other expenses
			{
				$form->add_edit("c" . $ids[$year], "Comment ", 0, $comments[$year]);
			}
			else
			{
				$form->add_hidden("c" . $ids[$year]);
			}
		}
		else
		{
			$form->add_label("r" . $ids[$year], "Amount " . $year . " in " . $currency["symbol"], 0, $amount);
			$form_is_editable = 0;
		}
	}

	if($form_is_editable ==1)
	{
		$form->add_button("save_form", "Save");
	}
	$form->add_button("back", "Back");

	$form->populate();
	$form->process();

	if($form->button("save_form"))
	{
		if($form->validate())
		{
			foreach($ids as $year=>$id)
			{
				$fields = array();
		
				$value = dbquote($form->value("r" . $ids[$year]));
				$fields[] = "cer_expense_amount = " . $value;

				$value = dbquote($form->value("c" . $ids[$year]));
				$fields[] = "cer_expense_comment = " . $value;

				$value = "current_timestamp";
				$fields[] = "date_modified = " . $value;

				if (isset($_SESSION["user_login"]))
				{
					$value = $_SESSION["user_login"];
					$fields[] = "user_modified = " . dbquote($value);
				}
		   
				$sql = "update cer_expenses set " . join(", ", $fields) . 
					" where cer_expense_id = " . $id . 
					" and cer_expense_cer_version = 0 ";
				mysql_query($sql) or dberror($sql);

		
			}
			
			/*
			if($expense_type != 3 and $expense_type != 6 and $expense_type != 9 and $expense_type != 11) //Annual fixed rents, Marketing Expenses, Contribution
			{
				$result = calculate_forcasted_expenses(id(), $years, $project["project_id"]);
			}
			*/

			/*
			if(id() == 2) // fixed rental cos
			{
				if($apply_new_version_of_rental_tool == 0)
				{
					$result = update_turnoverbased_rental_cost_version2013(param("pid"), $cer_basicdata["cer_basicdata_add_tob_rents"], $cer_basicdata["cer_basicdata_tob_from_net_sales"]);
				}
				else
				{
					$result = update_turnoverbased_rental_cost_version2014(param("pid"));
				}
			}
			*/

			$form->message("The data has been saved.");
			$link = "cer_application_expenses.php?pid=" . param("pid");
			redirect($link);
		}
	}
	elseif($form->button("back"))
	{
		$link = "cer_application_expenses.php?pid=" . param("pid");
		redirect($link);
	}
}


$page = new Page("cer_projects");
require "include/project_page_actions.php";
$page->header();

if($form_type == "INR03")
{
	$page->title(id() ? "INR-03 - Retail Furniture in Third-party Store: Edit " . $expense_type_name : "INR-03 - Retail Furniture in Third-party Store: Add Expense Position");
}
elseif($form_type == "AF")
{
	$page->title(id() ? "LNR/INR03: Edit " . $expense_type_name : "LNR: Add Expense Position");
}
else
{
	$page->title(id() ? "LNR/CER: Edit " . $expense_type_name : "LNR/CER: Add Expense Position");
}
require_once("include/tabs2016.php");
$form->render();

require "include/footer_scripts.php";
$page->footer();
?>