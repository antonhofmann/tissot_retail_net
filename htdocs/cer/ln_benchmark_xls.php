<?php
/********************************************************************

    ln_benchmarks_xls.php

    LN versus CEr Statistics

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-03-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-03-27
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_access_to_cer_benchmarks");

require_once "../libraries/phpexcel/Classes/PHPExcel.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";


include("ln_benchmark_filter.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

if(!param("query_id"))
{
	redirect("cer_ln_versus_cers.php");
}


//get pos investment types
$posinvestment_types = array();
$sql = "select posinvestment_type_id, posinvestment_type_name " . 
       " from posinvestment_types " . 
	   " order by posinvestment_type_sortorder ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$posinvestment_types[$row["posinvestment_type_id"]] = $row["posinvestment_type_name"];
}


$captions['A'] = "Nr";
$captions['B'] = "Country";
$captions['C'] = "POS Location";
$captions['D'] = "Project Number";
$captions['E'] = "";
$captions['F'] = "Currency";

$letter = 'F';
foreach($posinvestment_types as $id=>$name)
{
	$captions[++$letter] = $name;
}
$captions[++$letter] = "Total Investment";



$colwidth = array();
$colwidth['A'] = 5;
$colwidth['B'] = 10;
$colwidth['C'] = 5;
$colwidth['D'] = 15;
$colwidth['E'] = 15;
$colwidth['F'] = 10;

$letter = 'F';
foreach($posinvestment_types as $id=>$name)
{
	if(2+strlen($name) < 15)
	{
		$colwidth[++$letter] = 15;
	}
	else
	{
		$colwidth[++$letter] = 2+strlen($name);
	}
}

$colwidth[++$letter] = 2+strlen("Total Investment");



/********************************************************************
    Start output
*********************************************************************/
$objPHPExcel = new PHPExcel();

$logo = new PHPExcel_Worksheet_Drawing();
$logo->setName('Logo');
$logo->setDescription('Logo');
$logo->setPath('../pictures/brand_logo.jpg');
$logo->setHeight(36);
$logo->setWidth(113);

$sheet = $objPHPExcel->getActiveSheet();
$objPHPExcel->getActiveSheet()->setTitle('LNR versus CER');

$logo->setWorksheet($objPHPExcel->getActiveSheet());

//output formats
$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>'1006A3')
);

$style_normal_border = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
);


$style_normal_border_right = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
);


$style_title = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
    'font' => array(
        'bold' => true,
		'size' => 16
    )
);

$style_header = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'font' => array(
        'bold' => true,
    )
);

$style_header_right = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
    'font' => array(
        'bold' => true,
    )
);


$red = new PHPExcel_Style_Color();
$red->setRGB('FF0000');  

//default styles
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8); 


// HEADRES ROW 1
$sheet->setCellValue('D1', 'LNR versus CER - ' . $benchmark_title . ' (' . date("d.m.Y H:i:s") . ')');
$sheet->getStyle('D1')->applyFromArray( $style_title );
$sheet->getRowDimension('1')->setRowHeight(36);


$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);


// HEADRES 
foreach($captions as $col=>$caption){
    $sheet->setCellValue($col . '3', $caption);
	$sheet->getStyle($col . '3')->applyFromArray( $style_header );
	//$sheet->getStyle($col . '3')->getAlignment()->setTextRotation(90);
}

//$sheet->getRowDimension('3')->setRowHeight(150);


//OUTPUT DATA


$zebra_counter = 0;
$row_index = 5;
$i = 1;
$cell_index = 0;

$total_investments = array();
$total_investments2 = array();
$total_investments3 = array();
$number_of_projects = 0;

$country = '';
$number_of_projects_in_country = 0;
$total_country_investments = array();
$total_country_investments2 = array();
$total_country_investments3 = array();

$number_of_countries = 0;

foreach($posorders as $key=>$posorder)
{
	if($posorder["pipeline"] == 1)
	{
		$sql = "select * " . 
			   "from posaddressespipeline " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " . 
			   "left join cer_basicdata on cer_basicdata_project = project_id " .
			   "left join project_milestones on project_milestone_project = project_id " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"] . 
			   " and cer_basicdata_submitted is not NULL and cer_basicdata_submitted <> '0000-00-00 00:00:00' " .
			   " and project_milestone_milestone = 13 " . 
			   " and project_milestone_date is not null and project_milestone_date <> '0000-00-00' " .
			   " order by cer_basicdata_version DESC " . 
			   " limit 0,1";
	}
	elseif($posorder["pipeline"] == 2) // renovation project
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "left join cer_basicdata on cer_basicdata_project = project_id " .
			   "left join project_milestones on project_milestone_project = project_id " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"] . 
			   " and cer_basicdata_submitted is not NULL and cer_basicdata_submitted <> '0000-00-00 00:00:00' " .
			   " and project_milestone_milestone = 13 " . 
			   " and project_milestone_date is not null and project_milestone_date <> '0000-00-00' " .
			   " order by cer_basicdata_version DESC " . 
			   " limit 0,1";

	}
	else
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorders on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "left join cer_basicdata on cer_basicdata_project = project_id " .
			   "left join project_milestones on project_milestone_project = project_id " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"] . 
			   " and cer_basicdata_submitted is not NULL and cer_basicdata_submitted <> '0000-00-00 00:00:00' " .
			   " and project_milestone_milestone = 13 " . 
			   " and project_milestone_date is not null and project_milestone_date <> '0000-00-00' " .
			   " order by cer_basicdata_version DESC " . 
			   " limit 0,1";
		
	}


	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
	
		
		if($build_country_group_totals == 1 and $country == '')
		{
			$country = $row["country_name"];
		}
		elseif($build_country_group_totals == 1 and $country != $row["country_name"])
		{
			
			//print country totals
			if($number_of_projects_in_country > 1)
			{
				
				//**********************************
				//Country TOTALS
				//**********************************
				// Country Totals LN
				$cell_index = 2;
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $country);
				$cell_index++;
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Totals");
				$cell_index++;
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "LNR");
				$cell_index++;
				$cell_index++;

				$total_investment = 0;
				$letter = "F";
				foreach($posinvestment_types as $id=>$name)
				{
					if(array_key_exists($id, $total_country_investments))
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments[$id]);
						$total_investment = $total_investment + $total_country_investments[$id];
					}
					else
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
					}
					$cell = ++$letter . ($row_index);
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

					$cell_index++;
				}


				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment);
				$cell = ++$letter . ($row_index+1);
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
				$cell_index++;

				// Country Totals CER
				$row_index++;
				$cell_index = 4;

				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "CER");
				$cell_index++;
				$cell_index++;

				$total_investment2 = 0;
				$letter = "F";
				foreach($posinvestment_types as $id=>$name)
				{
					if(array_key_exists($id, $total_country_investments2))
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments2[$id]);
						$total_investment2 = $total_investment2 + $total_country_investments2[$id];
					}
					else
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
					}
					$cell = ++$letter . ($row_index);
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

					$cell_index++;
				}


				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment2);
				$cell = ++$letter . ($row_index+1);
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
				$cell_index++;



				// country Totals IN-03
				if(count($total_investments3) > 0)
				{
					$row_index++;
					$cell_index = 4;

					$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "IN-03");
					$cell_index++;
					$cell_index++;

					$total_investment3 = 0;
					$letter = "F";
					foreach($posinvestment_types as $id=>$name)
					{
						if(array_key_exists($id, $total_country_investments3))
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments3[$id]);
							$total_investment3 = $total_investment3 + $total_country_investments3[$id];
						}
						else
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
						}
						$cell = ++$letter . ($row_index);
						$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

						$cell_index++;
					}


					$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment3);
					$cell = ++$letter . ($row_index+1);
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
					$cell_index++;
				}


				//Country DELTA AMOUNT
				$row_index++;
				$cell_index = 4;

				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Delta amount");
				$cell_index++;
				$cell_index++;
				
				$letter = "F";		
				foreach($posinvestment_types as $id=>$name)
				{
					if(array_key_exists($id, $total_country_investments2) and array_key_exists($id, $total_country_investments3))
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id]);
					}
					elseif(array_key_exists($id, $total_country_investments2))
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments2[$id] - $total_country_investments[$id]);
					}
					else
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
					}
					$cell = ++$letter . ($row_index);
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

					if(array_key_exists($id, $total_country_investments2) and array_key_exists($id, $total_country_investments3))
					{
						if(($total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id]) > 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
						}
					}
					else
					{
						if(($total_country_investments2[$id] - $total_country_investments[$id]) > 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
						}
					}


					$cell_index++;
				}

				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment3 + $total_investment2 - $total_investment);
				$cell = ++$letter . ($row_index);
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

				if(($total_investment3 + $total_investment2 - $total_investment) > 0)
				{
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
				}



				//Country DELTA Percent
				$row_index++;
				$cell_index = 4;

				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Delta percent");
				$cell_index++;
				$cell_index++;
				
				$letter = "F";		
				foreach($posinvestment_types as $id=>$name)
				{
					if(array_key_exists($id, $total_country_investments2) and array_key_exists($id, $total_country_investments3))
					{
						$delta = $total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id];
						$percent = "";
						if($total_country_investments[$id] > 0)
						{
							$percent = round(100*$delta/$total_country_investments[$id],2) . "%";
						}
						
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
					}
					elseif(array_key_exists($id, $total_country_investments2))
					{
						$delta = $total_country_investments2[$id] - $total_country_investments[$id];
						$percent = "";
						if($total_country_investments[$id] > 0)
						{
							$percent = round(100*$delta/$total_country_investments[$id],2) . "%";
						}

						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
					}
					else
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
					}
					$cell = ++$letter . ($row_index);
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

					if(array_key_exists($id, $total_investments2) and array_key_exists($id, $total_investments3))
					{
						if(($total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id]) > 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
						}
					}
					else
					{
						if(($total_country_investments2[$id] - $total_country_investments[$id]) > 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
						}
					}


					$cell_index++;
				}

				$delta = $total_investment3 + $total_investment2 - $total_investment;
				$percent = "";
				if($total_investment > 0)
				{
					$percent = round(100*$delta/$total_investment,2) . "%";
				}
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
				$cell = ++$letter . ($row_index);
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				if(($total_investment3 + $total_investment2 - $total_investment) > 0)
				{
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
				}




				$row_index = $row_index + 2;
	
				//**********************************
				// country AVERAGE
				//**********************************


				foreach($total_country_investments as $key=>$value)
				{
					$total_country_investments[$key] = round($total_country_investments[$key]/$number_of_projects_in_country,2);
				}
				foreach($total_country_investments2 as $key=>$value)
				{
					$total_country_investments2[$key] = round($total_country_investments2[$key]/$number_of_projects_in_country,2);
				}
				foreach($total_country_investments3 as $key=>$value)
				{
					$total_country_investments3[$key] = round($total_country_investments3[$key]/$number_of_projects_in_country,2);
				}


				// Totals LN
				$cell_index = 3;

				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Average");
				$cell_index++;
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "LNR");
				$cell_index++;
				$cell_index++;

				$total_investment = 0;
				$letter = "F";
				foreach($posinvestment_types as $id=>$name)
				{
					if(array_key_exists($id, $total_country_investments))
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments[$id]);
						$total_investment = $total_investment + $total_country_investments[$id];
					}
					else
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
					}
					$cell = ++$letter . ($row_index);
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

					$cell_index++;
				}


				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment);
				$cell = ++$letter . ($row_index+1);
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
				$cell_index++;


				// Totals CER
				$row_index++;
				$cell_index = 4;

				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "CER");
				$cell_index++;
				$cell_index++;

				$total_investment2 = 0;
				$letter = "F";
				foreach($posinvestment_types as $id=>$name)
				{
					if(array_key_exists($id, $total_country_investments2))
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments2[$id]);
						$total_investment2 = $total_investment2 + $total_country_investments2[$id];
					}
					else
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
					}
					$cell = ++$letter . ($row_index);
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

					$cell_index++;
				}


				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment2);
				$cell = ++$letter . ($row_index+1);
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
				$cell_index++;



				// Totals IN-03
				if(count($total_investments3) > 0)
				{
					$row_index++;
					$cell_index = 4;

					$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "IN-03");
					$cell_index++;
					$cell_index++;

					$total_investment3 = 0;
					$letter = "F";
					foreach($posinvestment_types as $id=>$name)
					{
						if(array_key_exists($id, $total_country_investments3))
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments3[$id]);
							$total_investment3 = $total_investment3 + $total_country_investments3[$id];
						}
						else
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
						}
						$cell = ++$letter . ($row_index);
						$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

						$cell_index++;
					}


					$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment3);
					$cell = ++$letter . ($row_index+1);
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
					$cell_index++;
				}


				//DELTA AMOUNT
				$row_index++;
				$cell_index = 4;

				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Delta amount");
				$cell_index++;
				$cell_index++;
				
				$letter = "F";		
				foreach($posinvestment_types as $id=>$name)
				{
					if(array_key_exists($id, $total_country_investments2) and array_key_exists($id, $total_country_investments3))
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id]);
					}
					elseif(array_key_exists($id, $total_country_investments2))
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments2[$id] - $total_country_investments[$id]);
					}
					else
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
					}
					$cell = ++$letter . ($row_index);
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

					if(array_key_exists($id, $total_country_investments2) and array_key_exists($id, $total_country_investments3))
					{
						if(($total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id]) > 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
						}
					}
					else
					{
						if(($total_country_investments2[$id] - $total_country_investments[$id]) > 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
						}
					}


					$cell_index++;
				}

				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment3 + $total_investment2 - $total_investment);
				$cell = ++$letter . ($row_index);
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

				if(($total_investment3 + $total_investment2 - $total_investment) > 0)
				{
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
				}



				//DELTA Percent
				$row_index++;
				$cell_index = 4;

				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Delta percent");
				$cell_index++;
				$cell_index++;
				
				$letter = "F";		
				foreach($posinvestment_types as $id=>$name)
				{
					if(array_key_exists($id, $total_country_investments2) and array_key_exists($id, $total_country_investments3))
					{
						$delta = $total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id];
						$percent = "";
						if($total_country_investments[$id] > 0)
						{
							$percent = round(100*$delta/$total_country_investments[$id],2) . "%";
						}
						
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
					}
					elseif(array_key_exists($id, $total_country_investments2))
					{
						$delta = $total_country_investments2[$id] - $total_country_investments[$id];
						$percent = "";
						if($total_country_investments[$id] > 0)
						{
							$percent = round(100*$delta/$total_country_investments[$id],2) . "%";
						}

						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
					}
					else
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
					}
					$cell = ++$letter . ($row_index);
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

					if(array_key_exists($id, $total_country_investments2) and array_key_exists($id, $total_country_investments3))
					{
						if(($total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id]) > 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
						}
					}
					else
					{
						if(($total_country_investments2[$id] - $total_country_investments[$id]) > 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
						}
					}


					$cell_index++;
				}

				$delta = $total_investment3 + $total_investment2 - $total_investment;
				$percent = "";
				if($total_investment > 0)
				{
					$percent = round(100*$delta/$total_investment,2) . "%";
				}
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
				$cell = ++$letter . ($row_index);
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				if(($total_investment3 + $total_investment2 - $total_investment) > 0)
				{
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
				}

				$row_index = $row_index + 1;
			}
			
			$country = $row["country_name"];
			$number_of_countries++;
			$number_of_projects_in_country = 0;
			$total_country_investments = array();
			$total_country_investments2 = array();
			$total_country_investments3 = array();
			$row_index++;
			$row_index++;
		}
		
		
		//output data for a single project
		$cell_index = 0;
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $i);
		if($colwidth['A'] < strlen($i)){$colwidth['A'] = strlen($i);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["country_name"]);
		if($colwidth['B'] < strlen($row["country_name"])){$colwidth['B'] = 2+strlen($row["country_name"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["posaddress_name"]);
		if($colwidth['C'] < strlen($row["posaddress_name"])){$colwidth['C'] = 2+strlen($row["posaddress_name"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["project_number"]);
		if($colwidth['D'] < strlen($row["project_number"])){$colwidth['D'] = 2+strlen($row["project_number"]);}
		$cell_index++;

		

		//get latest ln version
		$version = 0;
		$exchange_rate = 1;
		$factor = 1;


		$sql = "select cer_basicdata_version, cer_basicdata_exchangerate, cer_basicdata_factor, " .
			   " currency_symbol " .
			   "from cer_basicdata " .
			   " left join currencies on currency_id = cer_basicdata_currency " .
			   " where cer_basicdata_project = " . $row["project_id"] . 
			   " and cer_basicdata_version_context = 'ln' " . 
			   " order by cer_basicdata_version ASC";


		$res_p = mysql_query($sql) or dberror($sql);
		if($row_p = mysql_fetch_assoc($res_p))
		{
			$version = $row_p["cer_basicdata_version"];

			$currency_symbol = $row_p["currency_symbol"];
			if($base_currency == "chf")
			{
				$currency_symbol = "CHF";
				$exchange_rate = $row_p["cer_basicdata_exchangerate"];
				$factor = $row_p["cer_basicdata_factor"];
			}
		}
		else
		{
			$sql = "select cer_basicdata_version, cer_basicdata_exchangerate, cer_basicdata_factor, " .
				   " currency_symbol " .
				   "from cer_basicdata " .
				   " left join currencies on currency_id = cer_basicdata_currency " .
				   " where cer_basicdata_project = " . $row["project_id"] . 
				   " order by cer_basicdata_version ASC";


			$res_p = mysql_query($sql) or dberror($sql);
			if($row_p = mysql_fetch_assoc($res_p))
			{
				$version = $row_p["cer_basicdata_version"];

				$currency_symbol = $row_p["currency_symbol"];
				if($base_currency == "chf")
				{
					$currency_symbol = "CHF";
					$exchange_rate = $row_p["cer_basicdata_exchangerate"];
					$factor = $row_p["cer_basicdata_factor"];
				}
			}
		}

		
		/*
		$sql = "select ln_basicdata_version, ln_basicdata_exchangerate, ln_basicdata_factor, " . 
				   " currency_symbol " . 
				   " from ln_basicdata " .
				   " left join currencies on currency_id = ln_basicdata_currency " . 
				   " where ln_basicdata_project = " . $row["project_id"] . 
				   " order by ln_basicdata_version ASC";
		$res_p = mysql_query($sql) or dberror($sql);
		if($row_p = mysql_fetch_assoc($res_p))
		{
			$version = $row_p["ln_basicdata_version"];

			$currency_symbol = $row_p["currency_symbol"];
			if($base_currency == "chf")
			{
				$currency_symbol = "CHF";
				$exchange_rate = $row_p["ln_basicdata_exchangerate"];
				$factor = $row_p["ln_basicdata_factor"];
			}
		}
		*/
	

		//get LN Investments
		$investments = array();
		$total_investment = 0;
		$sql = "select cer_investment_type, cer_investment_amount_cer_loc, cer_investment_amount_additional_cer_loc " . 
			   " from cer_investments " . 
			   " where cer_investment_project = " . $row["project_id"] . 
			   " and cer_investment_cer_version = " . $version;
		
		$res_p = mysql_query($sql) or dberror($sql);
		while($row_p = mysql_fetch_assoc($res_p))
		{
			$investments[$row_p["cer_investment_type"]] = round(($row_p["cer_investment_amount_cer_loc"])* $exchange_rate / $factor, 2, 0);

			$total_investment = $total_investment + $investments[$row_p["cer_investment_type"]];

			if(array_key_exists($row_p["cer_investment_type"], $total_investments))
			{
				$total_investments[$row_p["cer_investment_type"]] = $total_investments[$row_p["cer_investment_type"]] + round(($row_p["cer_investment_amount_cer_loc"])* $exchange_rate / $factor, 2, 0);
			}
			else
			{
				$total_investments[$row_p["cer_investment_type"]] = round(($row_p["cer_investment_amount_cer_loc"])* $exchange_rate / $factor, 2, 0);
			}

			if(array_key_exists($row_p["cer_investment_type"], $total_country_investments))
			{
				$total_country_investments[$row_p["cer_investment_type"]] = $total_country_investments[$row_p["cer_investment_type"]] + round(($row_p["cer_investment_amount_cer_loc"])* $exchange_rate / $factor, 2, 0);
			}
			else
			{
				$total_country_investments[$row_p["cer_investment_type"]] = round(($row_p["cer_investment_amount_cer_loc"])* $exchange_rate / $factor, 2, 0);
			}
		}

		
		if($version == 0)
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "LNR");
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "LNR Version " . $version);
		}

		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $currency_symbol);
		$cell_index++;

		$letter = "F";
		foreach($posinvestment_types as $id=>$name)
		{
			
			if(array_key_exists($id, $investments))
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$investments[$id]);

			}
			else
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, 0);
			}
			
			$cell = ++$letter . ($row_index);
			$sheet->getStyle("$cell")->getNumberFormat()->setFormatCode('0.00');
			$cell_index++;

			
		}

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment);
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
		$cell_index++;
		
		$row_index++;
	    $cell_index = 4;

				
		//get latest cer version
		$version = 0;
		$exchange_rate = 1;
		$factor = 1;
		$sql = "select cer_basicdata_version, cer_basicdata_exchangerate, cer_basicdata_factor, " . 
			   " currency_symbol " . 
			   " from cer_basicdata " .
			   " left join currencies on currency_id = cer_basicdata_currency " . 
			   " where cer_basicdata_project = " . $row["project_id"] . 
			   " and (cer_basicdata_version_context <> 'ln'  or cer_basicdata_version_context is null )" .
			   " order by cer_basicdata_version ASC";

		$res_p = mysql_query($sql) or dberror($sql);
		if($row_p = mysql_fetch_assoc($res_p))
		{
			$version = $row_p["cer_basicdata_version"];

			$currency_symbol = $row_p["currency_symbol"];
			if($base_currency == "chf")
			{
				$currency_symbol = "CHF";
				$exchange_rate = $row_p["cer_basicdata_exchangerate"];
				$factor = $row_p["cer_basicdata_factor"];
			}
		}



		
		//get CER Investments
		$investments2 = array();
		$total_investment2 = 0;
		$sql = "select cer_investment_type, cer_investment_amount_cer_loc, cer_investment_amount_additional_cer_loc " . 
			   " from cer_investments " . 
			   " where cer_investment_project = " . $row["project_id"] . 
			   " and cer_investment_cer_version = " . $version;
		
		$res_p = mysql_query($sql) or dberror($sql);
		while($row_p = mysql_fetch_assoc($res_p))
		{
			$investments2[$row_p["cer_investment_type"]] = round(($row_p["cer_investment_amount_cer_loc"])* $exchange_rate / $factor, 2, 0);

			$total_investment2 = $total_investment2 + $investments2[$row_p["cer_investment_type"]];

			if(array_key_exists($row_p["cer_investment_type"], $total_investments2))
			{
				$total_investments2[$row_p["cer_investment_type"]] = $total_investments2[$row_p["cer_investment_type"]] + round(($row_p["cer_investment_amount_cer_loc"])* $exchange_rate / $factor, 2, 0);
			}
			else
			{
				$total_investments2[$row_p["cer_investment_type"]] = round(($row_p["cer_investment_amount_cer_loc"])* $exchange_rate / $factor, 2, 0);
			}

			if(array_key_exists($row_p["cer_investment_type"], $total_country_investments2))
			{
				$total_country_investments2[$row_p["cer_investment_type"]] = $total_country_investments2[$row_p["cer_investment_type"]] + round(($row_p["cer_investment_amount_cer_loc"])* $exchange_rate / $factor, 2, 0);
			}
			else
			{
				$total_country_investments2[$row_p["cer_investment_type"]] = round(($row_p["cer_investment_amount_cer_loc"])* $exchange_rate / $factor, 2, 0);
			}
		}

		if($version == 0)
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "CER");
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "CER Version " . $version);
		}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $currency_symbol);
		$cell_index++;

		$letter = "F";
		foreach($posinvestment_types as $id=>$name)
		{
			if(array_key_exists($id, $investments2))
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $investments2[$id]);
			}
			else
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
			}
			$cell = ++$letter . ($row_index);
			$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

			$cell_index++;
		}


		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment2);
		$cell = ++$letter . ($row_index+1);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
		$cell_index++;


		//additional funding
		
		

		$investments3 = array();
		$total_investment3 = 0;
		$sql = "select cer_investment_type, cer_investment_amount_cer_loc, cer_investment_amount_additional_cer_loc " . 
			   " from cer_investments " . 
			   " where cer_investment_project = " . $row["project_id"] . 
			   " and cer_investment_cer_version = " . $version;
		
		$res_p = mysql_query($sql) or dberror($sql);
		while($row_p = mysql_fetch_assoc($res_p))
		{
			$investments3[$row_p["cer_investment_type"]] = round(($row_p["cer_investment_amount_additional_cer_loc"])* $exchange_rate / $factor, 2, 0);

			$total_investment3 = $total_investment3 + $investments3[$row_p["cer_investment_type"]];

			if($row_p["cer_investment_amount_additional_cer_loc"] > 0)
			{
				if(array_key_exists($row_p["cer_investment_type"], $total_investments3))
				{
					$total_investments3[$row_p["cer_investment_type"]] = $total_investments3[$row_p["cer_investment_type"]] + round(($row_p["cer_investment_amount_additional_cer_loc"])* $exchange_rate / $factor, 2, 0);
				}
				else
				{
					$total_investments3[$row_p["cer_investment_type"]] = round(($row_p["cer_investment_amount_additional_cer_loc"])* $exchange_rate / $factor, 2, 0);
				}

				if(array_key_exists($row_p["cer_investment_type"], $total_country_investments3))
				{
					$total_country_investments3[$row_p["cer_investment_type"]] = $total_country_investments3[$row_p["cer_investment_type"]] + round(($row_p["cer_investment_amount_additional_cer_loc"])* $exchange_rate / $factor, 2, 0);
				}
				else
				{
					$total_country_investments3[$row_p["cer_investment_type"]] = round(($row_p["cer_investment_amount_additional_cer_loc"])* $exchange_rate / $factor, 2, 0);
				}
			}
		}

		if($total_investment3 > 0)
		{
			$row_index++;
			$cell_index = 4;

			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "IN-03 Version " . $version);
			$cell_index++;

			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $currency_symbol);
			$cell_index++;

			$letter = "F";
			foreach($posinvestment_types as $id=>$name)
			{
				if(array_key_exists($id, $investments2))
				{
					$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $investments3[$id]);
				}
				else
				{
					$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
				}
				$cell = ++$letter . ($row_index);
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

				$cell_index++;
			}


			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment3);
			$cell = ++$letter . ($row_index+1);
			$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
			$cell_index++;
		}

		
		//DELTA AMOUNT
		$row_index++;
	    $cell_index = 4;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Delta amount");
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $currency_symbol);
		$cell_index++;
		
		$letter = "F";		
		foreach($posinvestment_types as $id=>$name)
		{
			if(array_key_exists($id, $investments2) and array_key_exists($id, $investments3))
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $investments3[$id] + $investments2[$id] - $investments[$id]);
			}
			elseif(array_key_exists($id, $investments2))
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $investments2[$id] - $investments[$id]);
			}
			else
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
			}
			$cell = ++$letter . ($row_index);
			$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

			if(array_key_exists($id, $investments2) and array_key_exists($id, $investments3))
			{
				if(($investments3[$id] + $investments2[$id] - $investments[$id]) > 0)
				{
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
				}
			}
			else
			{
				if(($investments2[$id] - $investments[$id]) > 0)
				{
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
				}
			}


			$cell_index++;
		}

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment3 + $total_investment2 - $total_investment);
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

		if(($total_investment3 + $total_investment2 - $total_investment) > 0)
		{
			$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
		}



		//DELTA Percent
		$row_index++;
	    $cell_index = 4;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Delta percent");
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $currency_symbol);
		$cell_index++;
		
		$letter = "F";		
		foreach($posinvestment_types as $id=>$name)
		{
			if(array_key_exists($id, $investments2) and array_key_exists($id, $investments3))
			{
				$delta = $investments3[$id] + $investments2[$id] - $investments[$id];
				$percent = "";
				if($investments[$id] > 0)
				{
					$percent = round(100*$delta/$investments[$id],2) . "%";
				}
				
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
			}
			elseif(array_key_exists($id, $investments2))
			{
				$delta = $investments2[$id] - $investments[$id];
				$percent = "";
				if($investments[$id] > 0)
				{
					$percent = round(100*$delta/$investments[$id],2) . "%";
				}

				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
			}
			else
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
			}
			$cell = ++$letter . ($row_index);
			$objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

			if(array_key_exists($id, $investments2) and array_key_exists($id, $investments3))
			{
				if(($investments3[$id] + $investments2[$id] - $investments[$id]) > 0)
				{
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
				}
			}
			else
			{
				if(($investments2[$id] - $investments[$id]) > 0)
				{
					$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
				}
			}


			$cell_index++;
		}

		
		
		$delta = $total_investment3 + $total_investment2 - $total_investment;
		$percent = "";
		if($total_investment > 0)
		{
			$percent = round(100*$delta/$total_investment,2) . "%";
		}
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

		if(($total_investment3 + $total_investment2 - $total_investment) > 0)
		{
			$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
		}
		

		$i++;
		$row_index++;
		$row_index++;
		

		$number_of_projects++;
		$number_of_projects_in_country++;
	}

}









	
//print country totals for the last country
if($build_country_group_totals == 1 and $number_of_countries > 1 and $number_of_projects_in_country > 1)
{
	
	//**********************************
	//Country TOTALS
	//**********************************
	// Country Totals LN
	$cell_index = 2;
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $country);
	$cell_index++;
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Totals");
	$cell_index++;
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "LNR");
	$cell_index++;
	$cell_index++;

	$total_investment = 0;
	$letter = "F";
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_country_investments))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments[$id]);
			$total_investment = $total_investment + $total_country_investments[$id];
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

		$cell_index++;
	}


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment);
	$cell = ++$letter . ($row_index+1);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
	$cell_index++;

	// Country Totals CER
	$row_index++;
	$cell_index = 4;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "CER");
	$cell_index++;
	$cell_index++;

	$total_investment2 = 0;
	$letter = "F";
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_country_investments2))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments2[$id]);
			$total_investment2 = $total_investment2 + $total_country_investments2[$id];
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

		$cell_index++;
	}


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment2);
	$cell = ++$letter . ($row_index+1);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
	$cell_index++;



	// country Totals IN-03
	if(count($total_investments3) > 0)
	{
		$row_index++;
		$cell_index = 4;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "IN-03");
		$cell_index++;
		$cell_index++;

		$total_investment3 = 0;
		$letter = "F";
		foreach($posinvestment_types as $id=>$name)
		{
			if(array_key_exists($id, $total_country_investments3))
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments3[$id]);
				$total_investment3 = $total_investment3 + $total_country_investments3[$id];
			}
			else
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
			}
			$cell = ++$letter . ($row_index);
			$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

			$cell_index++;
		}


		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment3);
		$cell = ++$letter . ($row_index+1);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
		$cell_index++;
	}


	//Country DELTA AMOUNT
	$row_index++;
	$cell_index = 4;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Delta amount");
	$cell_index++;
	$cell_index++;
	
	$letter = "F";		
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_country_investments2) and array_key_exists($id, $total_country_investments3))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id]);
		}
		elseif(array_key_exists($id, $total_country_investments2))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments2[$id] - $total_country_investments[$id]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

		if(array_key_exists($id, $total_country_investments2) and array_key_exists($id, $total_country_investments3))
		{
			if(($total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}
		else
		{
			if(($total_country_investments2[$id] - $total_country_investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}


		$cell_index++;
	}

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment3 + $total_investment2 - $total_investment);
	$cell = ++$letter . ($row_index);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

	if(($total_investment3 + $total_investment2 - $total_investment) > 0)
	{
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
	}



	//Country DELTA Percent
	$row_index++;
	$cell_index = 4;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Delta percent");
	$cell_index++;
	$cell_index++;
	
	$letter = "F";		
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_country_investments2) and array_key_exists($id, $total_country_investments3))
		{
			$delta = $total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id];
			$percent = "";
			if($total_country_investments[$id] > 0)
			{
				$percent = round(100*$delta/$total_country_investments[$id],2) . "%";
			}
			
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
		}
		elseif(array_key_exists($id, $total_country_investments2))
		{
			$delta = $total_country_investments2[$id] - $total_country_investments[$id];
			$percent = "";
			if($total_country_investments[$id] > 0)
			{
				$percent = round(100*$delta/$total_country_investments[$id],2) . "%";
			}

			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

		if(array_key_exists($id, $total_investments2) and array_key_exists($id, $total_investments3))
		{
			if(($total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}
		else
		{
			if(($total_country_investments2[$id] - $total_country_investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}


		$cell_index++;
	}

	$delta = $total_investment3 + $total_investment2 - $total_investment;
	$percent = "";
	if($total_investment > 0)
	{
		$percent = round(100*$delta/$total_investment,2) . "%";
	}
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
	$cell = ++$letter . ($row_index);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	if(($total_investment3 + $total_investment2 - $total_investment) > 0)
	{
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
	}




	$row_index = $row_index + 2;

	//**********************************
	// country AVERAGE
	//**********************************


	foreach($total_country_investments as $key=>$value)
	{
		$total_country_investments[$key] = round($total_country_investments[$key]/$number_of_projects_in_country,2);
	}
	foreach($total_country_investments2 as $key=>$value)
	{
		$total_country_investments2[$key] = round($total_country_investments2[$key]/$number_of_projects_in_country,2);
	}
	foreach($total_country_investments3 as $key=>$value)
	{
		$total_country_investments3[$key] = round($total_country_investments3[$key]/$number_of_projects_in_country,2);
	}


	// Totals LN
	$cell_index = 3;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Average");
	$cell_index++;
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "LNR");
	$cell_index++;
	$cell_index++;

	$total_investment = 0;
	$letter = "F";
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_country_investments))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments[$id]);
			$total_investment = $total_investment + $total_country_investments[$id];
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

		$cell_index++;
	}


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment);
	$cell = ++$letter . ($row_index+1);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
	$cell_index++;


	// Totals CER
	$row_index++;
	$cell_index = 4;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "CER");
	$cell_index++;
	$cell_index++;

	$total_investment2 = 0;
	$letter = "F";
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_country_investments2))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments2[$id]);
			$total_investment2 = $total_investment2 + $total_country_investments2[$id];
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

		$cell_index++;
	}


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment2);
	$cell = ++$letter . ($row_index+1);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
	$cell_index++;



	// Totals IN-03
	if(count($total_investments3) > 0)
	{
		$row_index++;
		$cell_index = 4;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "IN-03");
		$cell_index++;
		$cell_index++;

		$total_investment3 = 0;
		$letter = "F";
		foreach($posinvestment_types as $id=>$name)
		{
			if(array_key_exists($id, $total_country_investments3))
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments3[$id]);
				$total_investment3 = $total_investment3 + $total_country_investments3[$id];
			}
			else
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
			}
			$cell = ++$letter . ($row_index);
			$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

			$cell_index++;
		}


		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment3);
		$cell = ++$letter . ($row_index+1);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
		$cell_index++;
	}


	//DELTA AMOUNT
	$row_index++;
	$cell_index = 4;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Delta amount");
	$cell_index++;
	$cell_index++;
	
	$letter = "F";		
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_country_investments2) and array_key_exists($id, $total_country_investments3))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id]);
		}
		elseif(array_key_exists($id, $total_country_investments2))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_country_investments2[$id] - $total_country_investments[$id]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

		if(array_key_exists($id, $total_country_investments2) and array_key_exists($id, $total_country_investments3))
		{
			if(($total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}
		else
		{
			if(($total_country_investments2[$id] - $total_country_investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}


		$cell_index++;
	}

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment3 + $total_investment2 - $total_investment);
	$cell = ++$letter . ($row_index);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

	if(($total_investment3 + $total_investment2 - $total_investment) > 0)
	{
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
	}



	//DELTA Percent
	$row_index++;
	$cell_index = 4;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Delta percent");
	$cell_index++;
	$cell_index++;
	
	$letter = "F";		
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_country_investments2) and array_key_exists($id, $total_country_investments3))
		{
			$delta = $total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id];
			$percent = "";
			if($total_country_investments[$id] > 0)
			{
				$percent = round(100*$delta/$total_country_investments[$id],2) . "%";
			}
			
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
		}
		elseif(array_key_exists($id, $total_country_investments2))
		{
			$delta = $total_country_investments2[$id] - $total_country_investments[$id];
			$percent = "";
			if($total_country_investments[$id] > 0)
			{
				$percent = round(100*$delta/$total_country_investments[$id],2) . "%";
			}

			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

		if(array_key_exists($id, $total_country_investments2) and array_key_exists($id, $total_country_investments3))
		{
			if(($total_country_investments3[$id] + $total_country_investments2[$id] - $total_country_investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}
		else
		{
			if(($total_country_investments2[$id] - $total_country_investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}


		$cell_index++;
	}

	$delta = $total_investment3 + $total_investment2 - $total_investment;
	$percent = "";
	if($total_investment > 0)
	{
		$percent = round(100*$delta/$total_investment,2) . "%";
	}
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
	$cell = ++$letter . ($row_index);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	if(($total_investment3 + $total_investment2 - $total_investment) > 0)
	{
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
	}

	$row_index = $row_index + 1;
	$row_index++;
	$row_index++;
}

if($number_of_projects > 1)
{
	$row_index = $row_index + 2;
	
	//**********************************
	//TOTALS
	//**********************************
	// Totals LN
	$cell_index = 3;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Totals");
	$cell_index++;
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "LNR");
	$cell_index++;
	$cell_index++;

	$total_investment = 0;
	$letter = "F";
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_investments))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investments[$id]);
			$total_investment = $total_investment + $total_investments[$id];
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

		$cell_index++;
	}


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment);
	$cell = ++$letter . ($row_index+1);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
	$cell_index++;


	// Totals CER
	$row_index++;
	$cell_index = 4;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "CER");
	$cell_index++;
	$cell_index++;

	$total_investment2 = 0;
	$letter = "F";
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_investments2))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investments2[$id]);
			$total_investment2 = $total_investment2 + $total_investments2[$id];
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

		$cell_index++;
	}


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment2);
	$cell = ++$letter . ($row_index+1);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
	$cell_index++;



	// Totals IN-03
	if(count($total_investments3) > 0)
	{
		$row_index++;
		$cell_index = 4;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "IN-03");
		$cell_index++;
		$cell_index++;

		$total_investment3 = 0;
		$letter = "F";
		foreach($posinvestment_types as $id=>$name)
		{
			if(array_key_exists($id, $total_investments3))
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investments3[$id]);
				$total_investment3 = $total_investment3 + $total_investments3[$id];
			}
			else
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
			}
			$cell = ++$letter . ($row_index);
			$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

			$cell_index++;
		}


		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment3);
		$cell = ++$letter . ($row_index+1);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
		$cell_index++;
	}


	//DELTA AMOUNT
	$row_index++;
	$cell_index = 4;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Delta amount");
	$cell_index++;
	$cell_index++;
	
	$letter = "F";		
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_investments2) and array_key_exists($id, $total_investments3))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investments3[$id] + $total_investments2[$id] - $total_investments[$id]);
		}
		elseif(array_key_exists($id, $investments2))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investments2[$id] - $total_investments[$id]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

		if(array_key_exists($id, $investments2) and array_key_exists($id, $investments3))
		{
			if(($total_investments3[$id] + $total_investments2[$id] - $total_investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}
		else
		{
			if(($investments2[$id] - $investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}


		$cell_index++;
	}

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment3 + $total_investment2 - $total_investment);
	$cell = ++$letter . ($row_index);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

	if(($total_investment3 + $total_investment2 - $total_investment) > 0)
	{
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
	}



	//DELTA Percent
	$row_index++;
	$cell_index = 4;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Delta percent");
	$cell_index++;
	$cell_index++;
	
	$letter = "F";		
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_investments2) and array_key_exists($id, $total_investments3))
		{
			$delta = $total_investments3[$id] + $total_investments2[$id] - $total_investments[$id];
			$percent = "";
			if($total_investments[$id] > 0)
			{
				$percent = round(100*$delta/$total_investments[$id],2) . "%";
			}
			
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
		}
		elseif(array_key_exists($id, $total_investments2))
		{
			$delta = $total_investments2[$id] - $total_investments[$id];
			$percent = "";
			if($total_investments[$id] > 0)
			{
				$percent = round(100*$delta/$total_investments[$id],2) . "%";
			}

			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

		if(array_key_exists($id, $total_investments2) and array_key_exists($id, $total_investments3))
		{
			if(($total_investments3[$id] + $total_investments2[$id] - $total_investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}
		else
		{
			if(($total_investments2[$id] - $total_investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}


		$cell_index++;
	}

	$delta = $total_investment3 + $total_investment2 - $total_investment;
	$percent = "";
	if($total_investment > 0)
	{
		$percent = round(100*$delta/$total_investment,2) . "%";
	}
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
	$cell = ++$letter . ($row_index);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	if(($total_investment3 + $total_investment2 - $total_investment) > 0)
	{
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
	}




	$row_index = $row_index + 2;
	
	//**********************************
	//AVERAGE
	//**********************************


	foreach($total_investments as $key=>$value)
	{
		$total_investments[$key] = round($total_investments[$key]/$number_of_projects,2);
	}
	foreach($total_investments2 as $key=>$value)
	{
		$total_investments2[$key] = round($total_investments2[$key]/$number_of_projects,2);
	}
	foreach($total_investments3 as $key=>$value)
	{
		$total_investments3[$key] = round($total_investments3[$key]/$number_of_projects,2);
	}


	// Totals LN
	$cell_index = 3;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Average");
	$cell_index++;
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "LNR");
	$cell_index++;
	$cell_index++;

	$total_investment = 0;
	$letter = "F";
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_investments))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investments[$id]);
			$total_investment = $total_investment + $total_investments[$id];
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

		$cell_index++;
	}


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment);
	$cell = ++$letter . ($row_index+1);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
	$cell_index++;


	// Totals CER
	$row_index++;
	$cell_index = 4;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "CER");
	$cell_index++;
	$cell_index++;

	$total_investment2 = 0;
	$letter = "F";
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_investments2))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investments2[$id]);
			$total_investment2 = $total_investment2 + $total_investments2[$id];
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

		$cell_index++;
	}


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment2);
	$cell = ++$letter . ($row_index+1);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
	$cell_index++;



	// Totals IN-03
	if(count($total_investments3) > 0)
	{
		$row_index++;
		$cell_index = 4;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "IN-03");
		$cell_index++;
		$cell_index++;

		$total_investment3 = 0;
		$letter = "F";
		foreach($posinvestment_types as $id=>$name)
		{
			if(array_key_exists($id, $total_investments3))
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investments3[$id]);
				$total_investment3 = $total_investment3 + $total_investments3[$id];
			}
			else
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
			}
			$cell = ++$letter . ($row_index);
			$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

			$cell_index++;
		}


		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment3);
		$cell = ++$letter . ($row_index+1);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');
		$cell_index++;
	}


	//DELTA AMOUNT
	$row_index++;
	$cell_index = 4;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Delta amount");
	$cell_index++;
	$cell_index++;
	
	$letter = "F";		
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_investments2) and array_key_exists($id, $total_investments3))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investments3[$id] + $total_investments2[$id] - $total_investments[$id]);
		}
		elseif(array_key_exists($id, $investments2))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investments2[$id] - $total_investments[$id]);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

		if(array_key_exists($id, $investments2) and array_key_exists($id, $investments3))
		{
			if(($total_investments3[$id] + $total_investments2[$id] - $total_investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}
		else
		{
			if(($investments2[$id] - $investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}


		$cell_index++;
	}

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_investment3 + $total_investment2 - $total_investment);
	$cell = ++$letter . ($row_index);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getNumberFormat()->setFormatCode('0.00');

	if(($total_investment3 + $total_investment2 - $total_investment) > 0)
	{
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
	}



	//DELTA Percent
	$row_index++;
	$cell_index = 4;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Delta percent");
	$cell_index++;
	$cell_index++;
	
	$letter = "F";		
	foreach($posinvestment_types as $id=>$name)
	{
		if(array_key_exists($id, $total_investments2) and array_key_exists($id, $total_investments3))
		{
			$delta = $total_investments3[$id] + $total_investments2[$id] - $total_investments[$id];
			$percent = "";
			if($total_investments[$id] > 0)
			{
				$percent = round(100*$delta/$total_investments[$id],2) . "%";
			}
			
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
		}
		elseif(array_key_exists($id, $total_investments2))
		{
			$delta = $total_investments2[$id] - $total_investments[$id];
			$percent = "";
			if($total_investments[$id] > 0)
			{
				$percent = round(100*$delta/$total_investments[$id],2) . "%";
			}

			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
		}
		$cell = ++$letter . ($row_index);
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

		if(array_key_exists($id, $total_investments2) and array_key_exists($id, $total_investments3))
		{
			if(($total_investments3[$id] + $total_investments2[$id] - $total_investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}
		else
		{
			if(($total_investments2[$id] - $total_investments[$id]) > 0)
			{
				$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
			}
		}


		$cell_index++;
	}

	$delta = $total_investment3 + $total_investment2 - $total_investment;
	$percent = "";
	if($total_investment > 0)
	{
		$percent = round(100*$delta/$total_investment,2) . "%";
	}
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $percent);
	$cell = ++$letter . ($row_index);
	$objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	if(($total_investment3 + $total_investment2 - $total_investment) > 0)
	{
		$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setColor( $red );
	}


	
}



//Format column heights and witdhs
foreach($colwidth as $col=>$width) {
	$sheet->getColumnDimension($col)->setWidth($width);
}

/********************************************************************
    Activate Sheet 1
*********************************************************************/

$objPHPExcel->setActiveSheetIndex(0);
$sheet->setCellValue('A1', "");
$sheet->getStyle('A1')->applyFromArray($style_title);
$objPHPExcel->getActiveSheet()->insertNewRowBefore(1, 1);
$sheet->setCellValue('A1', "");
$sheet->getStyle('A1')->applyFromArray($style_title);


/********************************************************************
    Start output
*********************************************************************/
$filename = 'Statistics_ln_versus_cer_' . date('Ymd H:i:s') . '.xls';

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

/*
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
header('Pragma: ');

//send data
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
*/

?>