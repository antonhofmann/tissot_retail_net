<?php
/********************************************************************

    cer_details_pdf_detail.php

    Print Report of all CER/AF Data entered

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-09-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-09-12

    Copyright (c) 2009, Swatch AG, All Rights Reserved.
*********************************************************************/

/********************************************************************
    prepare all data needed
*********************************************************************/
$posdata = get_pos_data($project["project_order"]);
$franchisee_address = get_address($posdata["posaddress_franchisee_id"]);
$client_address = get_address($project["order_client_address"]);

$basicdata = get_cer_basicdata(param("pid"));

$currency = get_cer_currency(param("pid"));
$posleases = get_pos_leasedata($posdata["posaddress_id"], $project["project_order"]);


if($form_type == "CER" and count($posleases) > 0)
{
	$lease_type_name = "";
	$sql = "select poslease_type_name from poslease_types where poslease_type_id = " . dbquote($posleases["poslease_lease_type"]);
	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$lease_type_name = $row["poslease_type_name"];
	}

	//similar pos
	$similar_pos = "";
	$sql = "select concat(product_line_name, ': ', posaddress_place, ', ', posaddress_name) as posname " . 
			   "from posaddresses " .
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posaddress_store_furniture " . 
			   "where posaddress_id = " . dbquote($posdata["posaddress_best_benchmark_pos"]);

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$similar_pos = $row["posname"];
	}
}
//get quantities of watches
$units = array();
$units[0] = "";
$units[1] = "";
$units[2] = "";

$i = 0;
$sql = "select * from cer_revenues where cer_revenue_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$units[$i] = "Units in " . $row["cer_revenue_year"] . ": " . $row["cer_revenue_quantity_watches"];
	$i++;
}


$project_name = $project["order_shop_address_company"]. ", " . $project["order_shop_address_place"] . ", " . $project["country_name"];

$subclass_name = "";

$sql = "select possubclass_name from possubclasses where possubclass_id = " . dbquote($project["project_pos_subclass"]);
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$subclass_name = $row["possubclass_name"];	
}


$hq_project_manager = "";

$sql = "select concat(user_name, ' ', user_firstname) as username from users where user_id = " . dbquote($project["project_hq_project_manager"]);
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$hq_project_manager = $row["username"];	
}

$local_retail_coordinator = "";

$sql = "select concat(user_name, ' ', user_firstname) as username from users where user_id = " . dbquote($project["project_local_retail_coordinator"]);
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$local_retail_coordinator = $row["username"];	
}

//get cer summary data
$description = "";
$strategy = "";
$investment = "";
$benefits = "";
$alternative = "";
$risks = "";

$sql = "select * from cer_summary where cer_summary_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{

	$description = $row["cer_summary_in01_description"];
	$strategy = $row["cer_summary_in01_strategy"];
	$investment = $row["cer_summary_in01_investment"];
	$benefits = $row["cer_summary_in01_benefits"];
	$alternative = $row["cer_summary_in01_alternative"];
	$risks = $row["cer_summary_in01_risks"];

	$name1 = $row["cer_summary_in01_sig01"];
	$name2 = $row["cer_summary_in01_sig02"];
	$name3 = $row["cer_summary_in01_sig03"];
	$post_compl_review = substr($row["cer_summary_in01_review_date"], 5,2) . "/" . substr($row["cer_summary_in01_review_date"], 0,4);
	
}


$keymoney = get_pos_intangibles(param("pid"), 15);
$goodwill = get_pos_intangibles(param("pid"), 17);
$construction = get_pos_intangibles(param("pid"), 1);
$fixturing = get_pos_intangibles(param("pid"), 3);
$architectural = get_pos_intangibles(param("pid"), 5);
$equipment = get_pos_intangibles(param("pid"), 7);
$deposit = get_pos_intangibles(param("pid"), 9);
$other1 = get_pos_intangibles(param("pid"), 11);
$other2 = get_pos_intangibles(param("pid"), 13);


//get revenues
$years = array();

$sql = "select * " .
       "from cer_expenses " . 
	   "where cer_expense_type = 1 and cer_expense_project = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$years[] = $row["cer_expense_year"];
}

$quantity_watches = array();
$aveargeprice_watches = array();
$quantity_jewellery = array();
$aveargeprice_jewellery = array();
$customer_service = array();

$sql = "select * from cer_revenues where cer_revenue_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$quantity_watches[$row["cer_revenue_year"]] = $row["cer_revenue_quantity_watches"];
	$aveargeprice_watches[$row["cer_revenue_year"]] = $row["cer_revenue_aveargeprice_watches"];
	$quantity_jewellery[$row["cer_revenue_year"]] = $row["cer_revenue_quantity_jewellery"];
	$aveargeprice_jewellery[$row["cer_revenue_year"]] = $row["cer_revenue_aveargeprice_jewellery"];
	$customer_service[$row["cer_revenue_year"]] = $row["cer_revenue_customer_service"];

	
}


// marketing contribution
$marketing_contributions = array();

$sql  = "select cer_expense_type, cer_expense_year, cer_expense_amount " .
		"from cer_expenses " .
		"where cer_expense_type = 14 and cer_expense_project = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$marketing_contributions[$row["cer_expense_year"]] = $row["cer_expense_amount"];
}


//salaries
$hr_functions = array();
$hr_from = array();
$hr_fulltime = array();
$hr_fixed = array();
$hr_bonus = array();
$hr_other = array();
$hr_other = array();
$hr_social = array();
$hr_total = array();
$hr_parttime = array();

$sql = "select cer_salary_id, cer_salary_year_starting, " .
		 "cer_salary_month_starting, cer_salary_fixed_salary, cer_salary_bonus, " . 
		 "cer_salary_other, cer_salary_social_charges, cer_salary_total, cer_salary_headcount_percent,  " .
		 "TRUNCATE(cer_salary_total*cer_salary_headcount_percent/100,2 ) as cer_salary_part, " .
		 "cer_staff_type_name " . 
		 "from cer_salaries " . 
		 "left join cer_staff_types on cer_staff_type_id = cer_salary_staff_type " . 
		 "where cer_salary_project = " . dbquote(param("pid"));

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$hr_functions[] = $row["cer_staff_type_name"];
	$hr_from[] = $row["cer_salary_month_starting"] . "/" . $row["cer_salary_year_starting"];
	$hr_fixed[] = $row["cer_salary_fixed_salary"];
	$hr_bonus[] = $row["cer_salary_bonus"];
	$hr_other[] = $row["cer_salary_other"];
	$hr_social[] = $row["cer_salary_social_charges"];
	$hr_total[] = $row["cer_salary_total"];
	$hr_percent[] = $row["cer_salary_headcount_percent"];
	$hr_parttime[] = $row["cer_salary_part"];
}


//expenses
//prepare sql for list
$sql = "select cer_expense_type_id, cer_expense_type_group_name, cer_expense_type_name, " .
       "cer_expense_year, cer_expense_amount " .
       "from cer_expenses " . 
	   "left join cer_expense_types on cer_expense_type_id = cer_expense_type ";

if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
{
	$sql .= " where cer_expense_type <> 9 and cer_expense_project = " . param("pid") . " and cer_expense_type <> 14";
}
else
{
	$sql .= " where cer_expense_type <> 9 " .
		            "and cer_expense_project = " . param("pid") . 
		            " and cer_expense_type <> 14 " . 
		            " and cer_expense_type <> 11 " . 
		            " and cer_expense_type <> 12 ";
}

$sql .= " order by cer_expense_type_group_name, cer_expense_type_name, cer_expense_year ";

$expenses = array();

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$expenses[$row["cer_expense_type_group_name"]][$row["cer_expense_type_name"]][$row["cer_expense_year"]] = $row["cer_expense_amount"];
}


$inflation_rates = unserialize($basicdata["cer_basicdata_inflationrates"]);
$interest_rates = unserialize($basicdata["cer_basic_data_interesrates"]);

$inflation_rates_per_year = "";
foreach($inflation_rates as $key=>$value)
{
	$inflation_rates_per_year .= $key . ": " . $value . "%, ";
}
$interest_rates_per_year = "";
foreach($interest_rates as $key=>$value)
{
	$interest_rates_per_year .= $key . ": " . $value . "%, ";
}


//get stock_data
$stock_data = array();
$sql = "select * from cer_stocks " .
       "where cer_stock_project = " . param("pid") . 
	   " order by cer_stock_year";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$stock_data[$row["cer_stock_year"]] = $row["cer_stock_stock_in_months"];
}

//get paymentterm_data
$paymentterm_data = array();
$sql = "select * from cer_paymentterms " .
       "where cer_paymentterm_project = " . param("pid") . 
	   " order by cer_paymentterm_year";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$paymentterm_data[$row["cer_paymentterm_year"]] = $row["cer_paymentterm_in_months"];
}

/********************************************************************
    print PDF
*********************************************************************/
//set pdf parameters

$margin_top = 16;
$margin_left = 12;
$y = $margin_top;
$y2 = $margin_top;

$pdf->SetTitle("cer_af_details");
$pdf->SetAuthor("Tissot Retailnet");
$pdf->SetDisplayMode(100);
$pdf->AddPage("P");
$pdf->SetAutoPageBreak(false, 0);

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "I", 9);
$pdf->Cell(45, 10, "", 1);

$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);

$pdf->SetFont("arialn", "B", 11);
if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
{
	$pdf->Cell(122, 10, "LNR/CER Details: " . $project_name, 1, "", "L");
}
else
{
	$pdf->Cell(122, 10, "LNR Details: " . $project_name, 1, "", "L");
}
$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(20, 10, date("d.m.Y") . "/1", 1, "", "C");


// draw first box
$pdf->SetXY($margin_left,$y+8);

	// print project and investment infos
	$y = $y+11;
	$x = $margin_left;

	$y2 = $y2+11;
	$x2 = $margin_left + 97;

	//franchisee
	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(90, 5, "1. Franchisee", 0, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Company Name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	if($franchisee_address["company2"])
	{
		$pdf->Cell(60, 5, $franchisee_address["company"] .  ", " . $franchisee_address["company2"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(60, 5, $franchisee_address["company"], 1, "", "L");
	}

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Legal Entity Name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $franchisee_address["legal_entity_name"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Address:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);

	if($franchisee_address["address2"])
	{
		$pdf->Cell(60, 5, $franchisee_address["address"] .  ", " . $franchisee_address["address2"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(60, 5, $franchisee_address["address"], 1, "", "L");
	}

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Zip and City:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $franchisee_address["zip"] . " " . $franchisee_address["place"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Country:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $franchisee_address["country_name"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Phone and Mobile:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	
	if($franchisee_address["mobile_phone"])
	{
		$pdf->Cell(60, 5, $franchisee_address["phone"] .  ", " . $franchisee_address["mobile_phone"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(60, 5, $franchisee_address["phone"], 1, "", "L");
	}

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Email:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $franchisee_address["email"], 1, "", "L");

	
	
	
	//POS Location
	$y2 = $y2+7;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(90, 5, "2. POS Location", 0, "", "L");

	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "POS Name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $posdata["posaddress_name"], 1, "", "L");


	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Address:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	if(strlen($posdata["posaddress_address"]) > 50)
	{
		$pdf->Cell(60, 5, substr($posdata["posaddress_address"], 0, 50) . "...", 1, "", "L");
	}
	else
	{
		$pdf->Cell(60, 5,$posdata["posaddress_address"], 1, "", "L");
	}

	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Zip and City:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $posdata["posaddress_zip"] . " " . $posdata["posaddress_place"], 1, "", "L");

	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Country:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $posdata["country_name"], 1, "", "L");

	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Phone and Mobile:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	
	if($posdata["posaddress_mobile_phone"])
	{
		$pdf->Cell(60, 5, $posdata["posaddress_phone"] .  ", " . $posdata["posaddress_mobile_phone"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(60, 5, $posdata["posaddress_phone"], 1, "", "L");
	}

	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Contact:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $posdata["posaddress_contact_name"], 1, "", "L");

	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Email:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $posdata["posaddress_email"], 1, "", "L");
	

	//Project Details
	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "3. Project Details", 0, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Project Number:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $project["project_number"], 1, "", "L");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Product Line:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	if($project["productline_subclass_name"])
	{
		$pdf->Cell(60, 5, $project["product_line_name"] . " / " . $project["productline_subclass_name"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(60, 5, $project["product_line_name"], 1, "", "L");
	}

	
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "POS Type:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $project["postype_name"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "POS Type Subclass:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $subclass_name, 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Project Legal Type:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $project["project_costtype_text"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Project Type:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $project["projectkind_name"], 1, "", "L");
	
	//column 2

	$y2 = $y2+12;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Project Starting Date:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, to_system_date($project["order_date"]), 1, "", "L");

	
	if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
	{
		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 5, "", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(60, 5, "", 1, "", "L");
	}
	else
	{
		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 5, "Planned Opening Date:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(60, 5, to_system_date($project["project_planned_opening_date"]), 1, "", "L");
	}

	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, $project["projectkind_milestone_shortname_01"] . ":", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, to_system_date($project["project_real_opening_date"]), 1, "", "L");

	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, $project["projectkind_milestone_shortname_02"] . ":", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, to_system_date($project["project_actual_opening_date"]), 1, "", "L");
	

	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Total Surface in sqms:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $project["project_cost_totalsqms"], 1, "", "L");

	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Sales Surface in sqms:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $project["project_cost_sqms"], 1, "", "L");
    
	
	//General Details
	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "4. General Details", 0, "", "L");

	
	$business_plan_period = "from " . $basicdata["cer_basicdata_firstmonth"] . "/" . $basicdata["cer_basicdata_firstyear"] . " up to " . $basicdata["cer_basicdata_lastmonth"] . "/" . $basicdata["cer_basicdata_lastyear"];
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Business Plan Period:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $business_plan_period, 1, "", "L");

	/*
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "HQ Project Leader:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $hq_project_manager, 1, "", "L");
	*/

	$y = $y+10;

	//column 2
	$y2 = $y2+12;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Local Project Leader:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $local_retail_coordinator, 1, "", "L");

	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Local Architect:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $basicdata["cer_basicdata_local_architect"], 1, "", "L");

	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "Description of Capital Expenditure Project:", 1, "", "L");
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(187,3.5, $description, 1, "T");

	$y = $pdf->GetY()+2;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "Strategy:", 1, "", "L");
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(187,3.5, $strategy, 1, "T");

	$y = $pdf->GetY()+2;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "Investment (overview):", 1, "", "L");
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(187,3.5, $investment, 1, "T");

	$y = $pdf->GetY()+2;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "Benefits/Profitability:", 1, "", "L");
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(187,3.5, $benefits, 1, "T");

	$y = $pdf->GetY()+2;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "Alternative taken into consideration:", 1, "", "L");
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(187,3.5, $alternative, 1, "T");

	$y = $pdf->GetY()+2;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "Risks:", 1, "", "L");
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(187,3.5, $risks, 1, "T");


	$y = $pdf->GetY()+2;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "Signature of requesting entity (PC, Country):", 1, "", "L");

	$y = $pdf->GetY()+5;
	$y2 = $y;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Country Manager:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $name1, 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Head of Controlling:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $name2, 1, "", "L");
	
	//column 2
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Brand Manager:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $name3, 1, "", "L");

	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Review Date:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, $post_compl_review, 1, "", "L");

	
	//PAGE 2
	$pdf->AddPage("P", "A4");
	// Title first line
	$y = $margin_top;
	$y2 = $margin_top;
	$pdf->SetXY($margin_left,$margin_top);
	$pdf->SetFont("arialn", "I", 9);
	$pdf->Cell(45, 10, "", 1);

	$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);

	$pdf->SetFont("arialn", "B", 11);
	if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
	{
		$pdf->Cell(122, 10, "LNR/CER Details: " . $project_name, 1, "", "L");
	}
	else
	{
		$pdf->Cell(122, 10, "LNR Details: " . $project_name, 1, "", "L");
	}
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 10, date("d.m.Y") . "/2", 1, "", "C");

	$y = $y+11;
	$x = $margin_left;

	$y2 = $y2+11;
	$x2 = $margin_left + 97;


	//Territory
	
	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(120, 5, "5. Territory of Exclusivity", 0, "", "L");
	$pdf->Cell(7, 5, "", 0, "", "L");
	$pdf->Cell(60, 5, "6. Minimum Quantity", 0, "", "L");
	
	$y = $y+5;

	$y_old = $pdf->GetY() + 5;
	
	$pdf->SetFont("arialn", "", 8);
	$pdf->SetXY($x+127,$y);
	$pdf->Cell(60, 5, $units[0], 1, "", "L");
	$pdf->SetXY($x+127,$y+5);
	$pdf->Cell(60, 5, $units[1], 1, "", "L");
	$pdf->SetXY($x+127,$y+10);
	$pdf->Cell(60, 5, $units[2], 1, "", "L");

	
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->SetXY($x,$y+0.5);
	$pdf->MultiCell(120, 3.5, $posdata["posaddress_fag_territory"]);

	$new_y = $pdf->GetY();
	
	$box_height = $new_y - $y_old;

	$y = $pdf->SetXY($margin_left, $y_old);
	
	if($box_height <=15)
	{
		$new_box_height = $box_height + (15 - $box_height);
		$pdf->Cell(120, $new_box_height, "", 1, "", "L");
	}
	else
	{
		$pdf->Cell(127, $box_height, "", 1, "", "L");
	}

	if($form_type == "CER"  and count($posleases) > 0)
	{
		//Rental Details
		$y = $new_y+14;
		$y2 = $new_y+7;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(187, 5, "7. Rental Details", 0, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 5, "Similar POS:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(60, 5, $similar_pos, 1, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 5, "Lease Type:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(60, 5, $lease_type_name, 1, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 5, "Start Date:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(60, 5, to_system_date($posleases["poslease_startdate"]), 1, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 5, "Expiry Date:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(60, 5, to_system_date($posleases["poslease_enddate"]), 1, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 5, "Extension Option to Date:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(60, 5, to_system_date($posleases["poslease_extensionoption"]), 1, "", "L");

		//column2
		$y2 = $y2+12;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(35, 5, "Exit Option to Date:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(55, 5, to_system_date($posleases["poslease_exitoption"]), 1, "", "L");

		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(35, 5, "Termination deadline:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(55, 5, $posleases["poslease_termination_time"], 1, "", "L");

		$index_clause = 'no';
		if($posleases["poslease_indexclause_in_contract"] == 1) {$index_clause = 'yes';}
		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(35, 5, "Index Clause in Contract:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(55, 5, $index_clause, 1, "", "L");

		$index_clause2 = "no";
		if($posleases["poslease_isindexed"] == 1)
		{
			$index_clause2 = "yes";
		}

		if($posleases["poslease_tacit_renewal_duration_years"] > 0)
		{
			$index_clause2 .= " / ".  $posleases["poslease_tacit_renewal_duration_years"] . "Years";

			if($posleases["poslease_tacit_renewal_duration_months"] > 0)
			{
				$index_clause2 .= ", " . $posleases["poslease_tacit_renewal_duration_months"] . "Months";
			}
		}
		elseif($posleases["poslease_tacit_renewal_duration_months"] > 0)
		{
			$index_clause2 .= " / ".  $posleases["poslease_tacit_renewal_duration_months"] . "Months";
		}
		


		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(35, 5, "Tacit Renewal Clause / duration:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(55, 5, $index_clause2, 1, "", "L");

		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(35, 5, "Deadline for property:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(55, 5, to_system_date($basicdata["cer_basicdata_deadline_property"]), 1, "", "L");

		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(35, 5, "Negotiator's Name:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(55, 5, $posleases["poslease_negotiator"], 1, "", "L");

		$land_lord = str_replace("\n", "", $posleases["poslease_landlord_name"]);
		$land_lord = str_replace("\r", " ", $land_lord);

		$y = $y+12;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(30, 5, "Landlord", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(157, 5, $land_lord, 1, "", "L");

		$y = $pdf->GetY()+7;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(187, 5, "Rental conditions:", 1, "", "L");
		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->MultiCell(187,3.5, $posleases["poslease_negotiated_conditions"], 1, "T");


		$y = $pdf->GetY()+2;
		$y2 = $pdf->GetY()+2;
	}
	else
	{
		$y = $new_y+10;
		$y2 = $new_y+10;
		$pdf->SetXY($x,$y);
	}


	//Key Money/Investment
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "8. Key Money/Investment", 0, "", "L");

	if(count($keymoney) > 0)
	{
		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Key Money in " . $currency["symbol"] . ":", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, number_format($keymoney["cer_investment_amount_cer_loc"], 2, '.', "'"), 1, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Goodwill in " . $currency["symbol"] . ":", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, number_format($goodwill["cer_investment_amount_cer_loc"], 2, '.', "'"), 1, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Local construction in " . $currency["symbol"] . ":", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, number_format($construction["cer_investment_amount_cer_loc"], 2, '.', "'"), 1, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Store fixturing / Furniture in " . $currency["symbol"] . ":", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, number_format($fixturing["cer_investment_amount_cer_loc"], 2, '.', "'"), 1, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Architectural services in " . $currency["symbol"] . ":", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, number_format($architectural["cer_investment_amount_cer_loc"], 2, '.', "'"), 1, "", "L");


		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Equipment in " . $currency["symbol"] . ":", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, number_format($equipment["cer_investment_amount_cer_loc"], 2, '.', "'"), 1, "", "L");


		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Deposit in " . $currency["symbol"] . ":", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, number_format($deposit["cer_investment_amount_cer_loc"], 2, '.', "'"), 1, "", "L");

		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Other in " . $currency["symbol"] . ":", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, number_format($other1["cer_investment_amount_cer_loc"], 2, '.', "'"), 1, "", "L");

		$y = $y+7;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Other non-capitalized costs in " . $currency["symbol"] . ":", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, number_format($other2["cer_investment_amount_cer_loc"], 2, '.', "'"), 1, "", "L");


		//column 2
		$depr_years = " ";
		if($keymoney["cer_investment_depr_years"] > 0) {$depr_years = number_format($keymoney["cer_investment_depr_years"], 0);}
		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Depreciation Period in Years:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, $depr_years, 1, "", "L");

		$depr_years = " ";
		if($goodwill["cer_investment_depr_years"] > 0) {$depr_years = number_format($goodwill["cer_investment_depr_years"], 0);}
		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Depreciation Period in Years:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, $depr_years, 1, "", "L");

		$depr_years = " ";
		if($construction["cer_investment_depr_years"] > 0) {$depr_years = number_format($construction["cer_investment_depr_years"], 0);}
		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Depreciation Period in Years:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, $depr_years, 1, "", "L");

		$depr_years = " ";
		if($fixturing["cer_investment_depr_years"] > 0) {$depr_years = number_format($fixturing["cer_investment_depr_years"], 0);}
		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Depreciation Period in Years:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, $depr_years, 1, "", "L");

		$depr_years = " ";
		if($architectural["cer_investment_depr_years"] > 0) {$depr_years = number_format($architectural["cer_investment_depr_years"], 0);}
		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Depreciation Period in Years:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, $depr_years, 1, "", "L");

		$depr_years = " ";
		if($equipment["cer_investment_depr_years"] > 0) {$depr_years = number_format($equipment["cer_investment_depr_years"], 0);}
		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Depreciation Period in Years:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, $depr_years, 1, "", "L");

		$depr_years = " ";
		if($deposit["cer_investment_depr_years"] > 0) {$depr_years = number_format($deposit["cer_investment_depr_years"], 0);}
		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Depreciation Period in Years:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, $depr_years, 1, "", "L");

		$depr_years = " ";
		if($other1["cer_investment_depr_years"] > 0) {$depr_years = number_format($other1["cer_investment_depr_years"], 0);}
		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Depreciation Period in Years:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, $depr_years, 1, "", "L");


		$y2 = $y2+7;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, "Residual Value in " . $currency["symbol"] . ":", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, number_format($basicdata["cer_basicdata_residual_value"], 2, '.', "'"), 1, "", "L");

	}

	//Revenues
	$y = $pdf->GetY()+7;
	$y2 = $pdf->GetY()+7;
	
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "9. Revenues", 0, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, "Customer Service/Accessories in % of net sales:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, number_format($basicdata["cer_basicdata_customer_service"], 2) . "%", 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, "Sales Reduction in % of net sales:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, number_format($basicdata["cer_basicdata_sales_reduction"], 2) . "%", 1, "", "L");

	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, "Wholesale Margin in %:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, number_format($basicdata["cer_basicdata_wholesale_margin"], 2) . "%", 1, "", "L");
	

	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "Sales Revenues in " . $currency["symbol"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(10, 5, "Year", 1, "", "L");
	$pdf->Cell(21, 5, "Quantity Watches", 1, "", "R");
	$pdf->Cell(20, 5, "Average Price", 1, "", "R");
	$pdf->Cell(20, 5, "Gross Sales", 1, "", "R");
	$pdf->Cell(22, 5, "Quantity Watch Straps", 1, "", "R");
	$pdf->Cell(20, 5, "Average Price", 1, "", "R");
	$pdf->Cell(20, 5, "Gross Sales", 1, "", "R");
	$pdf->Cell(25, 5, "Services/Accessories", 1, "", "R");
	$pdf->Cell(29, 5, "Marketing Contribution", 1, "", "R");
	$pdf->SetFont("arialn", "", 8);

	foreach($years as $key=>$year)
	{
		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->Cell(10, 5, $year, 1, "", "L");
		$pdf->Cell(21, 5, number_format($quantity_watches[$year], 0), 1, "", "R");
		$pdf->Cell(20, 5, number_format($aveargeprice_watches[$year], 0), 1, "", "R");
		
		$gross_sales = $quantity_watches[$year]*$aveargeprice_watches[$year];
		$pdf->Cell(20, 5, number_format($gross_sales, 2, '.', "'"), 1, "", "R");


		$pdf->Cell(22, 5, number_format($quantity_jewellery[$year], 0), 1, "", "R");
		$pdf->Cell(20, 5, number_format($aveargeprice_jewellery[$year], 0), 1, "", "R");

		$gross_sales = $quantity_jewellery[$year]*$aveargeprice_jewellery[$year];
		$pdf->Cell(20, 5, number_format($gross_sales, 2, '.', "'"), 1, "", "R");

		$gross_sales = $quantity_jewellery[$year]*$aveargeprice_jewellery[$year];
		$pdf->Cell(25, 5, number_format($customer_service[$year], 2, '.', "'"), 1, "", "R");

		$pdf->Cell(29, 5, number_format($marketing_contributions[$year], 2, '.', "'"), 1, "", "R");

	}


	//PAGE 3
	$pdf->AddPage("P", "A4");
	// Title first line
	$y = $margin_top;
	$y2 = $margin_top;
	$pdf->SetXY($margin_left,$margin_top);
	$pdf->SetFont("arialn", "I", 9);
	$pdf->Cell(45, 10, "", 1);

	$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);

	$pdf->SetFont("arialn", "B", 11);
	if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
	{
		$pdf->Cell(122, 10, "LNR/CER Details: " . $project_name, 1, "", "L");
	}
	else
	{
		$pdf->Cell(122, 10, "LNR Details: " . $project_name, 1, "", "L");
	}
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 10, date("d.m.Y") . "/3", 1, "", "C");

	$y = $y+18;
	$x = $margin_left;

	$y2 = $y2+18;
	$x2 = $margin_left + 97;

	//Human Resources
	
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "10. Human Resources and Salaries in " . $currency["symbol"], 0, "", "L");

	
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(30, 5, "Function", 1, "", "L");
	$pdf->Cell(22, 5, "From Month/Year", 1, "", "R");
	$pdf->Cell(20, 5, "Fixed", 1, "", "R");
	$pdf->Cell(20, 5, "Bonus", 1, "", "R");
	$pdf->Cell(19, 5, "Other", 1, "", "R");
	$pdf->Cell(19, 5, "Social Charges", 1, "", "R");
	$pdf->Cell(19, 5, "Total", 1, "", "R");
	$pdf->Cell(19, 5, "Time Share", 1, "", "R");
	$pdf->Cell(19, 5, "Part Time", 1, "", "R");
	$pdf->SetFont("arialn", "", 8);
	
	foreach($hr_functions as $key=>$amount)
	{
		
		$y = $y+5;
		$pdf->SetXY($x,$y);
		
		$pdf->Cell(30, 5,$hr_functions[$key], 1, "", "L");
		$pdf->Cell(22, 5,$hr_from[$key], 1, "", "L");

		$pdf->Cell(20, 5, number_format($hr_fixed[$key], 0), 1, "", "R");
		$pdf->Cell(20, 5, number_format($hr_bonus[$key], 0), 1, "", "R");
		$pdf->Cell(19, 5, number_format($hr_other[$key], 0), 1, "", "R");
		$pdf->Cell(19, 5, number_format($hr_social[$key], 0), 1, "", "R");
		$pdf->Cell(19, 5, number_format($hr_total[$key], 0), 1, "", "R");
		$pdf->Cell(19, 5, number_format($hr_percent[$key], 2) . "%", 1, "", "R");
		$pdf->Cell(19, 5, number_format($hr_parttime[$key], 0), 1, "", "R");

	}


	//Expenses
	$y = $y+7;
	$y2 = $y;
	
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "11. Expenses in " . $currency["symbol"], 0, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(70, 5, "Cost of Watches Sold in % of net sales:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 5, number_format($basicdata["cer_basicdata_cost_watches"], 2) . "%", 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(70, 5, "Cost of Watch Straps Sold in % net sales:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 5, number_format($basicdata["cer_basicdata_cost_jewellery"], 2) . "%", 1, "", "L");

	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(70, 5, "Cost of Customer Service/Accessories Sold in % net sales:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 5, number_format($basicdata["cer_basicdata_cost_service"], 2) . "%", 1, "", "L");

	$y = $y+2;
	$y2 = $y+2;
	$x2 = $x + 44;
	$first_line = 1;

	//$expenses[$row["cer_expense_type_group_name"]][$row["cer_expense_type_name"]][$row["cer_expense_year"]] = $row["cer_expense_amount"];

	foreach($expenses as $group=>$expense_types)
	{
		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		
		if($first_line == 1)
		{
			$pdf->Cell(44, 5, $group, 1, "", "L");
			$x3 = $x2;
			$y2 = $y;
			foreach($years as $key=>$year)
			{
				$pdf->SetXY($x3,$y2);
				$pdf->SetFont("arialn", "B", 8);
				$pdf->Cell(11, 5, $year, 1, "", "R");
				$x3 = $x3 + 11;
			}
			$first_line = 0;

		}
		else
		{
			$pdf->Cell(44, 5,$group, 1, "", "L");
		}
		
		
		
		foreach($expense_types as $expense_type=>$years)
		{
			$y = $y+5;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("arialn", "", 7);
			$pdf->Cell(44, 5, " " . $expense_type, 1, "", "L");

			$x3 = $x2;
			$y2 = $y;
			foreach($years as $year=>$amount)
			{
				$pdf->SetXY($x3,$y2);
				$pdf->SetFont("arialn", "", 7);
				$pdf->Cell(11, 5, number_format($amount, 0, '.', "'"), 1, "", "R");
				$x3 = $x3 + 11;
			}
		}
	}


	//Cash Flow Calculation
	$y = $y+7;
	$y2 = $y;
	$x2 = $margin_left + 97;
	
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "12. Cash Flow Calculation", 0, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 5, "Currency:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(40, 5, $currency["symbol"], 1, "", "L");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 5, "Discounted Cash Flow Rate in %:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(40, 5, number_format($basicdata["cer_basicdata_dicount_rate"], 2) . '%', 1, "", "L");


	$y2 = $y2+5;
	$pdf->SetXY($x2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Exchange Rate:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(60, 5, number_format($currency["exchange_rate"], 6), 1, "", "L");
	

	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Inflation Rates:", 1, "", "L");
	$pdf->Cell(157, 5, $inflation_rates_per_year, 1, "", "L");



	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 5, "Interest Rates:", 1, "", "L");
	$pdf->Cell(157, 5, $interest_rates_per_year, 1, "", "L");

	
	if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
	{
		$y = $y+7;
		$y2 = $y;

		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, "Liquidation Revenue on Keymoney in %:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, number_format($basicdata["cer_bascidata_liquidation_keymoney"], 2) . '%', 1, "", "L");


		$y = $y+5;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, "Liquidation Revenue on Deposit in %:",  1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, number_format($basicdata["cer_basicdata_liquidation_deposit"], 2) . '%', 1, "", "L");


		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, "Liquidation Revenue on Stock in %:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, number_format($basicdata["cer_bascidata_liquidation_stock"], 2) . '%', 1, "", "L");

		$y2 = $y2+5;
		$pdf->SetXY($x2,$y2);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(50, 5, "Liquidation Revenue on Staff in %:", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(40, 5, number_format($basicdata["cer_bascicdate_liquidation_staff"], 2) . '%', 1, "", "L");
		
	}
	

	$y = $y+10;
	$x3 = $x+44;
	$y2 = $y;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(44, 5, "Years",  1, "", "L");
	foreach($stock_data as $year=>$stock)
	{
		$pdf->SetXY($x3,$y2);
		$pdf->Cell(11, 5, $year, 1, "", "R");
		$x3 = $x3 + 11;
	}
	
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 7);
	$pdf->Cell(44, 5, "Stock in Months",  1, "", "L");
	$pdf->SetFont("arialn", "", 7);


	$x3 = $x+44;
	$y2 = $y;

	foreach($stock_data as $year=>$stock)
	{
		$pdf->SetXY($x3,$y2);
		$pdf->SetFont("arialn", "", 7);
		$pdf->Cell(11, 5, number_format($stock, 0, '.', "'"), 1, "", "R");
		$x3 = $x3 + 11;
	}

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 7);
	$pdf->Cell(44, 5, "Payment Terms in Months for Liabilities",  1, "", "L");
	$pdf->SetFont("arialn", "", 7);


	$x3 = $x+44;
	$y2 = $y;

	foreach($paymentterm_data as $year=>$paymentterm)
		{
		$pdf->SetXY($x3,$y2);
		$pdf->SetFont("arialn", "", 7);
		$pdf->Cell(11, 5, number_format($paymentterm, 0, '.', "'"), 1, "", "R");
		$x3 = $x3 + 11;
	}


?>