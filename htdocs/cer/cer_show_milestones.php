<?php
/********************************************************************

    cer_show_milestones.php

    Show MileStones of the Projecs

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-10-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-16
    Version:        1.0.1

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("can_edit_milestones");

register_param("pid");

/********************************************************************
    prepare all data needed
*********************************************************************/
$client_address = get_address($project["order_client_address"]);

$client = $client_address["company"] . ", " .
		  $client_address["zip"] . " " .
          $client_address["place"] . ", " .
          $client_address["country_name"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_address"] . ", " .
		$project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];



/********************************************************************
    Get Cost Monitoring Sheet Budget Data
*********************************************************************/ 
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];


$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$project_cost_sqms = $row["project_cost_sqms"];

$project_cost_CER = $row["project_cost_CER"];
$project_cost_milestone_remarks = $row["project_cost_milestone_remarks"];
$project_cost_milestone_remarks2 = $row["project_cost_milestone_remarks2"];
$project_cost_milestone_turnaround = $row["project_cost_milestone_turnaround"];

/********************************************************************
    Caclucalte dates and diffeneces in dates
*********************************************************************/
$dates = array();
$comments = array();
$datecomments = array();
$daysconsumed = array();
$daysaccumulated = array();
$lastdate = "";
$milestone_turnaround = "";


//get trunaround
$date_min = "0000-00-00";
$sql_m = "select min(project_milestone_date) as date_min " .
         "from project_milestones " .
         " where project_milestone_date <> '0000-00-00' " . 
		 " and project_milestone_date is not NULL " . 
		 " and project_milestone_project = " . $project["project_id"];
$res = mysql_query($sql_m) or dberror($sql_m);
if($row = mysql_fetch_assoc($res))
{
	$date_min = $row["date_min"];
}

$date_max = "0000-00-00";
$sql_m = "select max(project_milestone_date) as date_max " . 
         "from project_milestones " .
         " where project_milestone_date <> '0000-00-00' " . 
		 " and project_milestone_date is not NULL " . 
		 " and project_milestone_project = " . $project["project_id"];
$res = mysql_query($sql_m) or dberror($sql_m);
if($row = mysql_fetch_assoc($res))
{
	$date_max = $row["date_max"];
}

$milestone_turnaround = floor((strtotime($date_max) - strtotime($date_min)) / 86400);


$sql = "select project_milestone_id, project_milestone_date, project_milestone_date_comment, " .
       "project_milestone_comment, milestone_code, milestone_text " . 
       "from project_milestones " .
       " left join milestones on milestone_id = project_milestone_milestone ";

$list_filter = "project_milestone_visible = 1 and project_milestone_project = " . $project["project_id"];


$sql_m = $sql . " where " . $list_filter . " order by milestone_code ";

$res = mysql_query($sql_m) or dberror($sql_m);

while($row = mysql_fetch_assoc($res))
{
	$dates[$row["project_milestone_id"]] = to_system_date($row["project_milestone_date"]);
	
	if($lastdate != "0000-00-00" and $lastdate != NULL and $row["project_milestone_date"] != "0000-00-00" and $row["project_milestone_date"] != NULL)
	{
		$thisdate = $row["project_milestone_date"];
		$difference_in_days = floor((strtotime($thisdate) - strtotime($date_min)) / 86400);
		
		
		
		$daysconsumed[$row["project_milestone_id"]] = $difference_in_days;
		
		
		$milestone_turnaround = $milestone_turnaround + $difference_in_days;
		$daysaccumulated[$row["project_milestone_id"]] = $milestone_turnaround;
	}

	if($row["project_milestone_date"] != "0000-00-00" and $row["project_milestone_date"] != NULL)
	{
		$lastdate = $row["project_milestone_date"];
	}
}



/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

include("include/project_head.php");

$form->add_section(" ");
$form->add_label("project_cost_milestone_turnaround", "Turn Around ", $milestone_turnaround);
$form->add_label("project_cost_milestone_remarks", "Remarks", 0, $project_cost_milestone_remarks);
$form->add_label("project_cost_milestone_remarks2", "Remarks2", 0, $project_cost_milestone_remarks2);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();

/********************************************************************
    Create List for Milestones
*********************************************************************/ 
$list = new ListView($sql);

$list->set_title("Milestones");
$list->set_entity("project_milestones");
$list->set_filter($list_filter);
$list->set_order("milestone_code");

$list->add_column("milestone_code", "Code", "", "", "", COLUMN_NO_WRAP);   
$list->add_column("milestone_text", "Description", "", "", "", COLUMN_NO_WRAP);
//$list->add_column("project_milestone_date", "Date", "", "", "", COLUMN_NO_WRAP);
$list->add_text_column("dates", "Sum", COLUMN_ALIGN_RIGHT, $dates);
$list->add_text_column("daysconsumed", "Days", COLUMN_ALIGN_RIGHT, $daysconsumed);
$list->add_text_column("daysaccumulated", "Sum", COLUMN_ALIGN_RIGHT, $daysaccumulated);



$list->populate();
$list->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page_Modal("cer_projects");

require "include/project_page_actions.php";

$page->header();

if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Project Milestones");
}
elseif($form_type == "AF")
{
	$page->title("LNR/INR03: Project Milestones");
}
else
{
	$page->title("LNR/CER: Project Milestones");
}

$form->render();

echo "<br>";
$list->render();

$page->footer();

?>