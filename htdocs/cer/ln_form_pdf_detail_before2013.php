<?php
/********************************************************************

    ln_form_pdf_detail_before2013.php

    Print Lease Negotiation Form before April 2013

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-11-06
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/
	
		
	if($exchange_rate_factor > 0)
	{
		$e_rate = $exchange_rate/$exchange_rate_factor;
	}
	else
	{
		$e_rate = $exchange_rate;
	}


	//Logo
	$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
	if(isset($version_date) and $version_date)
	{
		$pdf->SetFont('arialn','B',12);
		//Move to the right
		$pdf->Cell(40);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title . $version_date,0,0,'R');
		//Line break
		$pdf->SetY(23);
	}
	else
	{
		$pdf->SetFont('arialn','B',18);
		//Move to the right
		$pdf->Cell(80);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title,0,0,'R');
		//Line break
		$pdf->SetY(23);
	}


	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,$posname,1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->Cell(190,7,$posaddress,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->Cell(190,7,$postype,1, 0, 'L', 0);
	$pdf->Ln();

	$x = $pdf->GetX();
	$y = $pdf->GetY() + 2;

	
	//picture 1
	if($pix1 != ".." and file_exists($pix1))
	{
		if(substr($pix1, strlen($pix1)-3, 3) == "jpg" 
			or substr($pix1, strlen($pix1)-3, 3) == "JPG"
		     or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
		    or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
		{
			$imagesize = getimagesize($pix1);
			$w = $imagesize[0];
			$h = $imagesize[1];

			$imgratio=$w/$h;

			if ($imgratio>1)
			{
				if($w >= 190)
				{
					$scale_factor = 190/$w;
					$h = $h*$scale_factor;
					$w = $w*$scale_factor;
					$pdf->Image($pix1,$x,$y, $w, $h);
					$y1 = $h;
				}
				else
				{
					$pdf->Image($pix1,$x,$y);
					$y1 = ($h / 72) * 25.4;
				}
				
			}
			else
			{
				if($w >= 142)
				{
					$scale_factor = 142/$h;
					$h = $h*$scale_factor;
					$w = $w*$scale_factor;
					$pdf->Image($pix1,$x,$y, $w, $h);
					$y1 = $h;
				}
				else
				{
					$pdf->Image($pix1,$x,$y);
					$y1 = ($h / 72) * 25.4;
				}
				
			}

			$pdf->SetY($y1 + $pdf->GetY() + 5);
		}
	}


	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,"Key points",1, 0, 'L', 1);
	$pdf->Ln();

	
	
	//Area/Lcation
	$pdf->SetFont('arialn','',9);
	$text = "Area/Location:\r\n";
	$text .= $ln_area;

	$pdf->MultiCell(190,3.5, $text, 1, "T");

	//Size
	$pdf->SetFont('arialn','',9);
	//$text = "Gross / Total /Sales Surface in sqms: " . $gross_surface . " / " . $total_surface . " / " . $sales_surface;
	$text = "Total /Sales Surface in sqms: " . $total_surface . " / " . $sales_surface;
	//$text .= "\r\n" . "Head Counts/FTE: " . $ftes;
	
	if($ln_size_remarks)
	{
		$text .= "\r\n\r\n" . $ln_size_remarks;
	}
	$pdf->MultiCell(190,3.5, $text, 1, "T");

	//Rent Details
	$tmp = "Average annual Rent in KCHF: {rent} (over BP period {period} years):";
	$text = "";
	if($ln_basicdata['ln_basicdata_passedrental'] > 0 or $ln_basicdata['ln_basicdata_passedrent']) // lease renewal
	{
		
		if($ln_basicdata['ln_basicdata_passedrental'] > 0) {
			$text = "Rent of last full year in KCHF: " . number_format(round($e_rate*$ln_basicdata['ln_basicdata_passedrental']/1000,0), 0, ".", "'") . "\r\n";
		}
		
		if($ln_basicdata['ln_basicdata_passedrent'])
		{
			$text .= $ln_basicdata['ln_basicdata_passedrent'] .  "\r\n\r\n";
		}
		
		
		$tmp = "Average new annual Rent in KCHF: {rent} (over BP period {period}):";
		
	}
	
	$tmp = str_replace('{rent}', number_format(round($e_rate*$annual_rent/1000,0), 0, ".", "'"), $tmp);
	$tmp = str_replace('{period}', $lease_duration, $tmp);
	
	$text .= $tmp;
	$text .= "\r\n" . "Keymoney in KCHF: " . number_format(round($e_rate*$intagible_amount/1000,0), 0, ".", "'");
	if($ln_rent)
	{
		$text .= "\r\n" . $ln_rent;
	}

	
	/*
	if(strlen($text) > 300) {
		$pdf->AddPage();
		//Logo
		$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
		//arialn bold 15
		$pdf->SetFont('arialn','B',18);
		//Move to the right
		$pdf->Cell(80);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title,0,0,'R');
		//Line break
		$pdf->SetY(23);
	}
	*/
	

	$pdf->SetFont('arialn','',9);
	$pdf->MultiCell(190,3.5, $text, 1, "T");


	$text = "Rental conditions:\r\n";
	if(count($posleases) > 0) {
		$text .= $posleases["poslease_negotiated_conditions"];
	}

	$pdf->MultiCell(190,3.5, $text, 1, "T");
	

	//Availability, Contract Period
	$text = "Availability/Contract Period: ";
	
	if($ln_availability)
	{
		$text .= "\r\n" . $ln_availability;
	}
	
	/*
	if($planned_opening_date)
	{
		$text .= $duration_in_years . " years/from " . $planned_opening_date;
	}
	else
	{
		$text .= $duration_in_years . " years";
	}
	*/
	
	$pdf->SetFont('arialn','',9);
	$pdf->MultiCell(190,3.5, $text, 1, "T");


	//page 2
	$pdf->AddPage("P", "A4");

	//Logo
	$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
	if(isset($version_date) and $version_date)
	{
		$pdf->SetFont('arialn','B',12);
		//Move to the right
		$pdf->Cell(40);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title . $version_date,0,0,'R');
		//Line break
		$pdf->SetY(23);
	}
	else
	{
		$pdf->SetFont('arialn','B',18);
		//Move to the right
		$pdf->Cell(80);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title,0,0,'R');
		//Line break
		$pdf->SetY(23);
	}


	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,$posname,1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->Cell(190,7,$posaddress,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->Cell(190,7,$postype,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();




	//rental Details
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(75,5,"",1, 0, 'L', 0);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years))
		{
			$pdf->Cell(23,5,$years[$i],1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",1, 0, 'R', 0);
		}
	}
	
	$pdf->Ln();

	
	$pdf->SetFont('arialn','B',9);
	$x = $pdf->GetX();
	$pdf->Cell(190,20,"",1, 0, 'L', 0);
	$pdf->SetX($x);

	$pdf->Cell(75,20," ",0, 0, 'R', 0);
	for($i=0;$i<5;$i++)
	{
		$pdf->Cell(23,20," ",1, 0, 'R', 0);
	}
	
	$pdf->SetX($x);

	$pdf->SetFont('arialn','',9);

	$pdf->Cell(75,5,"Annual fix rental costs in KCHF",0, 0, 'L', 0);
	
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $fixedrents))
		{
			$pdf->Cell(23,5,number_format($e_rate*$fixedrents[$years[$i]]/1000, 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	$pdf->Cell(75,5,"Turnover based rental costs per year in KCHF",0, 0, 'L', 0);
	
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $turnoverbasedrents))
		{
			$pdf->Cell(23,5,number_format($e_rate*$turnoverbasedrents[$years[$i]]/1000, 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();


	$pdf->Cell(75,5,"Additional rental costs per year in KCHF",0, 0, 'L', 0);
	
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $additionalrents))
		{
			$pdf->Cell(23,5,number_format($e_rate*$additionalrents[$years[$i]]/1000, 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();


	$pdf->SetFont('arialn','B',9);

	$pdf->Cell(75,5,"Totals",1, 0, 'L', 0);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $sales_units_watches_values))
		{
			$pdf->Cell(23,5,number_format($e_rate*$rents[$years[$i]]/1000, 0, ".", "'"),1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",1, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);

	$pdf->Ln();



	//watches sold out in the past
	// renovation or tekover/renovation or lease renewal or new relocation project
	if(($project["project_projectkind"] == 2 
        or $project["project_projectkind"] == 3
		or $project["project_projectkind"] == 4
		or $project["project_projectkind"] == 5)
		or ($project["project_projectkind"] == 6 and $project["project_relocated_posaddress_id"] > 0)
	   )

	{
	
		$sellout_ending_year = (int)substr($cer_basicdata["date_created"], 0, 4);
		if($sellout_ending_year < (int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4))
		{
			$sellout_ending_year =(int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4);
		}


		if($sellout_ending_year > date("Y"))
		{
			$sellout_ending_year = date("Y");
		}

		$sellout_starting_year =$sellout_ending_year - 3;

		$pdf->SetFont('arialn','B',9);
		$pdf->Cell(75,5,"",0, 0, 'L', 0);

		for($i=$sellout_starting_year;$i<=($sellout_ending_year);$i++)
		{
			if(array_key_exists($i, $sellouts_watches))
			{
				$pdf->Cell(23,5,$i . "(" . $sellouts_months[$i] . ")",1, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(23,5,$i,1, 0, 'R', 0);
			}
		}
		$pdf->Cell(23,5," ",1, 0, 'R', 0);
		
		$pdf->Ln();

		$pdf->SetFont('arialn','B',9);
		$x = $pdf->GetX();
		$pdf->Cell(190,10,"",1, 0, 'L', 0);
		$pdf->SetX($x);

		$pdf->Cell(75,10," ",0, 0, 'R', 0);
		for($i=0;$i<5;$i++)
		{
			$pdf->Cell(23,10," ",1, 0, 'R', 0);
		}

		$pdf->SetX($x);

		$pdf->Cell(75,5,"Watches units (sold)",0, 0, 'L', 0);
		
		$pdf->SetFont('arialn','',9);
		for($i=$sellout_starting_year;$i<=($sellout_ending_year+1);$i++)
		{
			if(array_key_exists($i, $sellouts_watches))
			{
				$pdf->Cell(23,5,number_format($sellouts_watches[$i], 0, ".", "'"),0, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(23,5," ",0, 0, 'R', 0);
			}
		}
		$pdf->Ln();

		$pdf->SetFont('arialn','B',9);
		$pdf->Cell(75,5,"Bjoux units (sold)",0, 0, 'L', 0);
		$pdf->SetFont('arialn','',9);
		for($i=$sellout_starting_year;$i<=($sellout_ending_year+1);$i++)
			{
			if(array_key_exists($i, $sellouts_bjoux))
			{
				$pdf->Cell(23,5,number_format($sellouts_bjoux[$i], 0, ".", "'"),0, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(23,5," ",0, 0, 'R', 0);
			}
		}
		$pdf->Ln();
		$pdf->Ln();
	}
	


	//plannded sellouts
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(75,5,"",0, 0, 'L', 0);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years))
		{
			$pdf->Cell(23,5,$years[$i],1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",1, 0, 'R', 0);
		}
	}
	
	$pdf->Ln();

	$pdf->SetFont('arialn','B',9);
	$x = $pdf->GetX();
	$pdf->Cell(190,10,"",1, 0, 'L', 0);
	$pdf->SetX($x);

	$pdf->Cell(75,10," ",0, 0, 'R', 0);
	for($i=0;$i<5;$i++)
	{
		$pdf->Cell(23,10," ",1, 0, 'R', 0);
	}

	$pdf->SetX($x);

	$pdf->Cell(75,5,"Watches units (planned)",0, 0, 'L', 0);
	
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $sales_units_watches_values) and $sales_units_watches_values[$years[$i]] > 0)
		{
			$pdf->Cell(23,5,number_format($sales_units_watches_values[$years[$i]], 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(75,5,"Bjoux units (planned)",0, 0, 'L', 0);
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $sales_units_jewellery_values) and $sales_units_jewellery_values[$years[$i]] > 0)
		{
			$pdf->Cell(23,5,number_format($sales_units_jewellery_values[$years[$i]], 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();
	$pdf->Ln();

	$x = $pdf->GetX();
	$pdf->Cell(190,25,"",1, 0, 'L', 0);
	$pdf->SetX($x);

	$pdf->Cell(75,10," ",0, 0, 'R', 0);
	for($i=0;$i<5;$i++)
	{
		$pdf->Cell(23,10," ",1, 0, 'R', 0);
	}

	$pdf->SetX($x);

	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(75,5,"Total Gross Sales in KCHF" ,0, 0, 'L', 0);

	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $total_gross_sales_values))
		{
			$pdf->Cell(23,5,number_format(round($e_rate*$total_gross_sales_values[$years[$i]]/1000,0), 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	
	
	
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(75,5,"Total Gross Margin in KCHF",0, 0, 'L', 0);
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $total_gross_margin_values))
		{
			$pdf->Cell(23,5,number_format(round($e_rate*$total_gross_margin_values[$years[$i]]/1000,0), 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(75,5,"Total Indirect Expenses in KCHF",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $total_indirect_expenses_values))
		{
			$pdf->Cell(23,5,number_format(round(-1*$e_rate*$total_indirect_expenses_values[$years[$i]]/1000,0), 0, ".", "'"),1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",1, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	$pdf->SetFont('arialn','B',9);

	$pdf->Cell(75,10," ",0, 0, 'R', 0);
	for($i=0;$i<5;$i++)
	{
		$pdf->Cell(23,10," ",1, 0, 'R', 0);
	}

	$pdf->SetX($x);

	$pdf->Cell(75,5,"Operating Income without WS Margin in KCHF",0, 0, 'L', 0);
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $operating_income01_values))
		{
			$pdf->Cell(23,5,number_format(round($e_rate*$operating_income01_values[$years[$i]]/1000,0), 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(75,5,"Operating Income incl. WS Margin in KCHF",0, 0, 'L', 0);
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $operating_income02_values))
		{
			$pdf->Cell(23,5,number_format(round($e_rate*$operating_income02_values[$years[$i]]/1000,0), 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();
	$pdf->Ln();

	


	$x = $pdf->GetX();
	$pdf->Cell(94,17,"",1, 0, 'L', 0);
	$pdf->SetX($x);
	
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(70,7,"Total Investment in Fixed Assets in KCHF",0, 0, 'L', 0);
	$pdf->SetFont('arialn','',9);
	$pdf->Cell(24,7,number_format(round($e_rate*$investment_total/1000,0), 0, ".", "'"),0, 0, 'R', 0);
	$pdf->Cell(2,7," ",0, 0, 'R', 0);
	$pdf->Ln();
    
	
	foreach($fixed_assets as $key=>$itype)
	{
		if($itype == 1 and array_key_exists($itype, $investment_names))
		{
			$pdf->Cell(70,5," Of which - " . $investment_names[$itype] . " in KCHF",0, 0, 'L', 0);
			$pdf->Cell(24,5,number_format(round($e_rate*$amounts[$itype] / 1000,0), 0, ".", "'"),0, 0, 'R', 0);
			$pdf->Cell(2,5," ",0, 0, 'R', 0);
			$pdf->Ln();
		}
		if($itype == 3 and array_key_exists($itype, $investment_names))
		{
			$pdf->Cell(70,5," Of which - " . $investment_names[$itype] . " in KCHF",0, 0, 'L', 0);
			$pdf->Cell(24,5,number_format(round($e_rate*$amounts[$itype] / 1000,0), 0, ".", "'"),0, 0, 'R', 0);
			$pdf->Cell(2,5," ",0, 0, 'R', 0);
			$pdf->Ln();
		}
		
	    
	}

	$pdf->Ln();

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,"Direct Neighbours",0, 0, 'L', 0);
	$pdf->Ln();
	$pdf->SetFont('arialn','',9);
	$pdf->MultiCell(190,3.5, $ln_brands, 1, "T");	
	//$pdf->Ln();
	
	$y = $pdf->GetY()+5;
	$pdf->SetY($y);
	
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,"Remarks",0, 0, 'L', 0);
	$pdf->Ln();
	$pdf->SetFont('arialn','',9);
	$pdf->MultiCell(190,3.5, $ln_remarks, 1, "T");	
	
	//$pdf->Ln();
	
	
	$y = $pdf->GetY()+5;
	$pdf->SetY($y);

	$x1 = $pdf->GetX();
	

	
	$pdf->SetFont('arialn','',9);
	$pdf->Cell(55,9,"Brand Manager, " . $brand_manager,1, 0, 'L', 0);
	$pdf->Cell(35,9,"" ,1, 0, 'L', 0);
	$pdf->Ln();
	
	$x = 110;
	$pdf->setY($y);
	$pdf->setX($x);
	
	

	$pdf->SetFont('freesans','',7);

	$tmp_y = $pdf->getY($y);
	$tmp_x = $pdf->getX($x);

	$pdf->Cell(90,9,"" ,1, 'L');
	$pdf->setY($tmp_y);
	$pdf->setX($tmp_x);
	$pdf->MultiCell(55,9,"Regional Sales Manager, " . $sales_manager,1, 'T');
	
	//$pdf->Ln();

	$pdf->setX($x);
	$pdf->SetFont('freesans','',7);
	
	if($retail_head)
	{
		$pdf->Cell(55,9,"International Retail Manager, " . $retail_head,1, 0, 'L', 0);
	}
	elseif($vp_sales)
	{
		$pdf->Cell(55,9,"VP Sales, " . $vp_sales,1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(55,9,"",1, 0, 'L', 0);
	}
	$pdf->Cell(35,9,"" ,1, 0, 'L', 0);
	$pdf->Ln();
	
	$pdf->setX($x);
	$pdf->SetFont('freesans','',7);
	if($retail_head and $vp_sales)
	{
		$pdf->Cell(55,9,"VP Sales, " . $vp_sales,1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(55,9,"",1, 0, 'L', 0);
	}

	$pdf->Cell(35,9,"" ,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->setX($x);
	$pdf->SetFont('freesans','',7);
	//$pdf->Cell(55,9,"Tissot CEO, " . $president,1, 0, 'L', 0);
	$pdf->Cell(55,9,"",1, 0, 'L', 0);
	$pdf->Cell(35,9,"" ,1, 0, 'L', 0);


?>