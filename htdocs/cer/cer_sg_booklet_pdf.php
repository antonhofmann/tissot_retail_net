<?php
/********************************************************************

    cer_sg_booklet_pdf.php

    Print PDF CER all Forms.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-02-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-02-20
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.
*********************************************************************/
if(isset($write_tmp_file) and $write_tmp_file == true) {
	
}
else {
	session_name("retailnet");
	session_start();
	require "../include/frame.php";
}


$SUPPRESS_HEADERS = true;
$PDFmerger_was_used = false;
$pdf_print_output = true;


define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

check_access("has_access_to_cer");

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../vendor/setasign/fpdi/fpdi.php');
require_once('../include/SetaPDF/Autoload.php');
require_once('cer_sg_form_data.php');


// write pdf
$file_name = 'CER_Booklet_' . str_replace(" ", "_", $pos_data['posaddress_name']) . "_" . $project["project_number"] . '.pdf';


/********************************************************************
    prepare all data needed
*********************************************************************/
$PDFmerger_was_used = false;

class MYFPDI extends FPDI {

    var $_tplIdx;
    
    protected $footerCaption;

    protected $customStartPageNumber;

    protected $showHeader;

	public function setHeaderVisibility($visible=true) {
        $this->showHeader = $visible;
    }

	public function setFooterVisibility($visible=true) {
        $this->showFooter = $visible;
    }

	public function setFooterCaption($caption) {
        $this->footerCaption = $caption;
    }

	public function getStartPageNumber() {
        return $this->customStartPageNumber;
    }

	public function setStartPageNumber($pageNumber) {
        $this->customStartPageNumber = $pageNumber;
    }


	public function Header() {
		
		
	}
    
    
    function Footer() {
        
        if ($this->showFooter) {
			$this->SetY(-10);
			$this->SetX(12);
			$this->SetFont('arialn','I',8);
			// page number
			$pageNumber = $this->PageNo() + $this->customStartPageNumber - 1;
			$this->Cell(130, 8, $this->footerCaption . " / Page " . $pageNumber , 0, 1, 'L');
		}
    }
}

// Create and setup PDF document
class MYPDF extends TCPDF
{

	protected $customStartPageNumber = 0;
	
	public function setStartPageNumber($pageNumber) {
        $this->customStartPageNumber = $pageNumber;
    }
	
	//Page footer
	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-10);
		$this->SetX(12);
		$this->SetFont('arialn','I',8);
		if(($this->PageNo()+$this->customStartPageNumber) > 1) {
			$this->Cell(0,10, $this->filename . " - " . $this->versiondate . ' / Page '. ($this->PageNo() + $this->customStartPageNumber),0,0,'L');
		}
	}

}

$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);
$pdf->SetMargins(10, 23, 10);

$pdf->Open();
$pdf->SetAutoPageBreak(false);

$pdf->SetFillColor(220, 220, 220); 


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');



$pdf->versiondate = $version_date;
$pdf->filename = BRAND . " " . $file_name;
$footer_caption = BRAND . " " . $file_name . " - " . $version_date;


//add cover sheet
$pdf->AddPage('L');
$pdf->setJPEGQuality(100);
$pdf->Image('pictures/coversheet_booklets.jpg',0,0, 294);


$pdf->SetXY(170, 80);
$pdf->SetFont("arialn", "B", 14);

$pdf->Cell(100, 8, "CER BOOKLET", 0, "", "L");


$pdf->SetXY(170, 105);
$pdf->Cell(100, 8, $project["country_name"] . " - " . $pos_data["place_name"], 0, "", "L");
$pdf->SetXY(170, 115);
$pdf->Cell(100, 8, $pos_data["posaddress_name"], 0, "", "L");

$pdf->SetFont("arialn", "B", 12);
$pdf->SetXY(170, 125);
$pdf->Cell(100, 8, "Project " . $project["project_number"], 0, "", "L");
$pdf->SetXY(170, 130);
$pdf->Cell(100, 8, $postype, 0, "", "L");


include("cer_sg_form_inr01_pdf_detail.php");

include("cer_sg_form_inr02A_detail_pdf.php");

include("cer_sg_form_inr02B_detail_pdf.php");

include("cer_sg_form_inr02C_detail_pdf.php");


$cer_version = $tmp_cer_version;
$ln_version = $tmp_ln_version;



$merger = new SetaPDF_Merger();
$pdfString = $pdf->output('', 'S');
$tmp = SetaPDF_Core_Document::loadByString($pdfString);
$merger->addDocument($tmp);
$pdf = null;


//project budget in local curency
if($cer_version > 0)
{
	$source_file = $_SERVER['DOCUMENT_ROOT'] . 'files/ln/' . $project["project_number"] . '/cer_budget_' . param('pid') . '_version_' . $cer_version . '.pdf';
	
	if(file_exists($source_file))
	{

		$globalPageNumber++;
		$fpdi = new MYFPDI();
		$fpdi->setHeaderVisibility(true);
		$fpdi->setFooterVisibility(true);
		$fpdi->SetMargins(0, 0, 0);
		$fpdi->SetAutoPageBreak(true, 40);
		$fpdi->setFooterCaption($footer_caption);
		$globalPageNumber++;

		// get source pdf
		$pageCount = $fpdi->setSourceFile($source_file);
		
		for ($i=1; $i <= $pageCount; $i++) { 
			$fpdi->setStartPageNumber($globalPageNumber);
			$tpl = $fpdi->ImportPage($i);
			$size = $fpdi->getTemplateSize($tpl);
			if($size['h'] > 220) {
				$fpdi->AddPage('P');
				$fpdi->useTemplate($tpl, 0, 0, 210, 0, true);
			}
			else {
				$fpdi->AddPage('L');
				$fpdi->useTemplate($tpl, 0, 0, 290, 0, true);
			}
		}
		
		$pdfString = $fpdi->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		$globalPageNumber = $globalPageNumber + $pageCount;
	
	}
	elseif($budget_present == true)
	{
		$globalPageNumber++;
		$include_from_cer_booklet = true;
		$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
		$pdf->SetMargins(10, 23, 10);
		$pdf->setPrintHeader(false);

		$pdf->AddFont('arialn');
		$pdf->AddFont('arialn', 'B');

		$pdf->versiondate = $version_date;
		$pdf->filename = $file_name;
		$pdf->setStartPageNumber($globalPageNumber);

		$pdf->Open();
		
		include("../user/project_costs_budget_detail_pdf.php");

		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		$pdf = null;
	}
}
elseif($budget_present == true)
{
	$globalPageNumber++;
	$include_from_cer_booklet = true;
	$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
	$pdf->SetMargins(10, 23, 10);
	$pdf->setPrintHeader(false);

	$pdf->AddFont('arialn');
	$pdf->AddFont('arialn', 'B');

	$pdf->versiondate = $version_date;
	$pdf->filename = $file_name;
	$pdf->setStartPageNumber($globalPageNumber);

	$pdf->Open();
	
	
	include("../user/project_costs_budget_detail_pdf.php");

	$pdfString = $pdf->output('', 'S');
	$tmp = SetaPDF_Core_Document::loadByString($pdfString);
	$merger->addDocument($tmp);
	$pdf = null;
}


//quotes for local works

if($cer_version > 0)
{
	$source_file = $_SERVER['DOCUMENT_ROOT'] . 'files/ln/' . $project["project_number"] . '/cer_offer_comparison_' . param('pid') . '_version_' . $cer_version . '.pdf';
	
	if(file_exists($source_file))
	{
		$globalPageNumber++;
		$fpdi = new MYFPDI();
		$fpdi->setHeaderVisibility(true);
		$fpdi->setFooterVisibility(true);
		$fpdi->SetMargins(0, 0, 0);
		$fpdi->SetAutoPageBreak(true, 40);
		$fpdi->setFooterCaption($footer_caption);
		$globalPageNumber++;

		// get source pdf
		$pageCount = $fpdi->setSourceFile($source_file);
		
		for ($i=1; $i <= $pageCount; $i++) { 
			$fpdi->setStartPageNumber($globalPageNumber);
			$tpl = $fpdi->ImportPage($i);
			$size = $fpdi->getTemplateSize($tpl);
			if($size['h'] > 220) {
				$fpdi->AddPage('P');
				$fpdi->useTemplate($tpl, 0, 0, 210, 0, true);
			}
			else {
				$fpdi->AddPage('L');
				$fpdi->useTemplate($tpl, 0, 0, 290, 0, true);
			}
		}
		
		$pdfString = $fpdi->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		$globalPageNumber = $globalPageNumber + $pageCount;
	}
	elseif($bids_present == true)
	{
		
		$globalPageNumber++;
		$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
		$pdf->SetMargins(10, 23, 10);
		$pdf->setPrintHeader(false);

		$pdf->AddFont('arialn');
		$pdf->AddFont('arialn', 'B');

		$pdf->versiondate = $version_date;
		$pdf->filename = $file_name;
		$pdf->setStartPageNumber($globalPageNumber-1);

		$pdf->Open();
		
		
		include("../user/project_costs_bid_comparison_pdf_main.php");

		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		$pdf = null;
		
		
		
	}
}
elseif($bids_present == true)
{
	$globalPageNumber++;
	$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
	$pdf->SetMargins(10, 23, 10);
	$pdf->setPrintHeader(false);

	$pdf->AddFont('arialn');
	$pdf->AddFont('arialn', 'B');

	$pdf->versiondate = $version_date;
	$pdf->filename = $file_name;
	$pdf->setStartPageNumber($globalPageNumber-1);

	$pdf->Open();
	
	
	include("../user/project_costs_bid_comparison_pdf_main.php");

	$pdfString = $pdf->output('', 'S');
	$tmp = SetaPDF_Core_Document::loadByString($pdfString);
	$merger->addDocument($tmp);
	$pdf = null;
}

//add project attachments
$output_done = true;
$has_images = false;
$image_pages_created = false;
$number_of_images = 1;
if(count($attached_documents_images) > 0 
	or count($attached_documents_pdfs) > 0)
{
	foreach($attached_documents_images as $key=>$source_file)
	{
		if(file_exists($source_file))
		{
			if(substr($source_file, strlen($source_file)-3, 3) == "jpg" 
				or substr($source_file, strlen($source_file)-3, 3) == "JPG"
				 or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
				or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
			{
				$has_images = true;
				
				if($image_pages_created == false) {
					$image_pages_created = true;
					$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
					$pdf->setPrintHeader(false);
					$pdf->setPrintFooter(true);
					$pdf->SetMargins(10, 23, 10);

					$pdf->Open();
					$pdf->SetAutoPageBreak(false);

					$pdf->SetFillColor(220, 220, 220); 

					$pdf->AddFont('arialn','');
					$pdf->AddFont('arialn','B');
					$pdf->AddFont('arialn','I');
					$pdf->AddFont('arialn','BI');

					$pdf->versiondate = $version_date;
					$pdf->filename = $file_name;
					$pdf->setStartPageNumber($globalPageNumber-1);


				}

				if($number_of_images == 1) {				
					
					$pdf->AddPage("L");
					$globalPageNumber++;

					$pdf->SetXY($margin_left,$margin_top);
					$pdf->SetFont("arialn", "B", 11);
					$pdf->Cell(270, 8, "Additional Pictures: " . $project["country_name"] . " - " . $pos_data["place_name"] . " - " . $pos_data["posaddress_name"] . " - Project " . $project['project_number'], 1, "", "L");
					
					$pdf->setXY($margin_left, 25);
					$x = $pdf->GetX();
					$y = $pdf->GetY();
					$pdf->Image($source_file,$x,$y, 0, 80, "", "", "", true, 300, "", false, false, 0, false, false,true);

					$image_dimenstions = getimagesize ( $source_file);
					$w = $image_dimenstions[0];
					$h = $image_dimenstions[1];
					$ratio = 80/$h;
					$x_offset = ceil($w*$ratio) + 5;
					$number_of_images++;
				}
				elseif($number_of_images == 2) {
					$pdf->setXY($margin_left+$x_offset, 25);
					$x = $pdf->GetX();
					$y = $pdf->GetY();
					$pdf->Image($source_file,$x,$y, 0, 80, "", "", "", true, 300, "", false, false, 0, false, false,true);
					$number_of_images++;
				}
				elseif($number_of_images == 3) {
					$pdf->setXY($margin_left, 110);
					$x = $pdf->GetX();
					$y = $pdf->GetY();
					$pdf->Image($source_file,$x,$y, 0, 80, "", "", "", true, 300, "", false, false, 0, false, false,true);

					$image_dimenstions = getimagesize ( $source_file);
					$w = $image_dimenstions[0];
					$ratio = 80/$h;
					$x_offset = ceil($w*$ratio) + 5;
					$number_of_images++;
				}
				elseif($number_of_images == 4) {
					$pdf->setXY($margin_left+$x_offset, 110);
					$x = $pdf->GetX();
					$y = $pdf->GetY();
					$pdf->Image($source_file,$x,$y, 0, 80, "", "", "", true, 300, "", false, false, 0, false, false,true);
					$number_of_images = 1;
				}

			}
		}
	}
	
	if($has_images == true) {
		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		$pdf = null;
	}

	foreach($attached_documents_pdfs as $key=>$source_file)
	{
		if(file_exists($source_file))
		{
			//PDF
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				$fpdi = new MYFPDI();
				$fpdi->setStartPageNumber($globalPageNumber);
				$fpdi->setHeaderVisibility(true);
				$fpdi->setFooterVisibility(true);
				$fpdi->SetMargins(0, 0, 0);
				$fpdi->SetAutoPageBreak(true, 40);
				$fpdi->setFooterCaption($footer_caption);

				// get source pdf
				$pageCount = $fpdi->setSourceFile($source_file);

				for ($i=1; $i <= $pageCount; $i++) { 
					$fpdi->setStartPageNumber($globalPageNumber);
					$tpl = $fpdi->ImportPage($i);
					$size = $fpdi->getTemplateSize($tpl);
					if($size['h'] > 220) {
						$fpdi->AddPage('P');
						$fpdi->useTemplate($tpl, 0, 0, 210, 0, true);
					}
					else {
						$fpdi->AddPage('L');
						$fpdi->useTemplate($tpl, 0, 0, 290, 0, true);
					}
				}

				$pdfString = $fpdi->output('', 'S');
				$tmp = SetaPDF_Core_Document::loadByString($pdfString);
				$merger->addDocument($tmp);
				$globalPageNumber = $globalPageNumber + $pageCount;
			}
		}
	}
}



$merger->merge();
$document = $merger->getDocument();

//write file to disk
if(isset($write_tmp_file) and $write_tmp_file == true) {

	$file_name = TMP_FILE_DIR_ABSOLUTE . $file_name;
	$document->setWriter(new SetaPDF_Core_Writer_File($file_name, true));
	$document->save()->finish();
}
else {
	$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
	$document->save()->finish();
}
?>