<?php
/********************************************************************

    lnr_sg_form_data.php

    Get all data needed in forms

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2018-01-05
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2018-01-05
    Version:        1.0.0

    Copyright (c) 2018, Tissot, All Rights Reserved.
*********************************************************************/
check_access("has_access_to_cer");

if(isset($write_tmp_file) and $write_tmp_file == true) {

}
else {
	require "include/get_functions.php";
	require "../shared/func_posindex.php";
}
require "include/get_project.php";

require "include/financial_functions.php";

$globalPageNumber = 0;
// data needed
$project = get_project(param("pid"));
$order_number  = $project["project_order"];


//get pos data
if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
}


$relocated_pos_info = '';
if($project["project_projectkind"] == 6 
	and $project["project_relocated_posaddress_id"] > 0) {

	$relocated_pos = get_relocated_pos_info($project["project_relocated_posaddress_id"]);

	if (count($relocated_pos) > 0)
	{
		
		$relocated_pos_info = $relocated_pos["posaddress_name"];
		$relocated_pos_info .= ", " .
				$relocated_pos["posaddress_zip"] . " " .
				$relocated_pos["place_name"] . ", " .
				$relocated_pos["country_name"];
	
	}

}


$client_currency = get_cer_currency(param("pid"), $cer_version);
$currency_symbol = $client_currency["symbol"];

//$gross_surface = $project["project_cost_grosssqms"];
$gross_surface = $project["project_cost_totalsqms"];
$total_surface = $project["project_cost_totalsqms"];
$sale_surface = $project["project_cost_sqms"];
$bo_surface = $project["project_cost_backofficesqms"];
//$other_surface = $project["project_cost_othersqms"];


if($project["possubclass_name"]) {
	$postype = $project["projectkind_name"] . ", " . $project["project_costtype_text"] . ", " . $project["postype_name"] . ", " . $project["possubclass_name"];
}
else {
	$postype = $project["projectkind_name"] . ", " . $project["project_costtype_text"] . ", " . $project["postype_name"];
}
$keypoints =  $postype . "\n" . $ln_basicdata["ln_basicdata_remarks"];

$contract_starting_date = "";
$contract_ending_date = "";


//get keymoney and deposit
$tmp = get_pos_intangibles(param("pid"), 15, $cer_version);
if(count($tmp) > 0)
{
	$keymoney_full = $tmp ["cer_investment_amount_cer_loc"];
	$keymoney = round($keymoney_full/1000, 0);
	
	$tmp = get_pos_intangibles(param("pid"), 9, $cer_version);
	$deposit = $tmp ["cer_investment_amount_cer_loc"];
}
else
{
	$keymoney = "";
	$deposit = "";
}

//get rents
$first_full_year = $cer_basicdata["cer_basicdata_firstyear"];
if($cer_basicdata["cer_basicdata_firstmonth"] > 1 and $cer_basicdata["cer_basicdata_lastyear"] > ($first_full_year+1))
{
	$first_full_year = $cer_basicdata["cer_basicdata_firstyear"]+1;
}


$last_full_year = $cer_basicdata["cer_basicdata_lastyear"];
if($cer_basicdata["cer_basicdata_lastmonth"] < 12)
{
	$last_full_year = $cer_basicdata["cer_basicdata_lastyear"]-1;
}

$fullrent_firstyear = 0;
$fixedrent_firstyear = 0;
$turnoverrent_firstyear = 0;
$additional_rental_cost_firstyear = 0;
$fullrent_firstyear_loc = 0;


if($first_full_year > 0 and $cer_basicdata["cer_basicdata_firstyear"] < $cer_basicdata["cer_basicdata_lastyear"])
{
	$sql_e = "select cer_expense_amount from cer_expenses " . 
		   "where cer_expense_project = " . param("pid") .
		   " and cer_expense_cer_version =  " . $cer_version . 
		   " and cer_expense_type = 2 " . 
		   " and cer_expense_year = " . $first_full_year;

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$fixedrent_firstyear = $row_e["cer_expense_amount"];
	}

	$sql_e = "select cer_expense_amount from cer_expenses " . 
		   "where cer_expense_project = " . param("pid") .
		   " and cer_expense_cer_version =  " . $cer_version . 
		   " and cer_expense_type = 16 " . 
		   " and cer_expense_year = " . $first_full_year;

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$turnoverrent_firstyear = $row_e["cer_expense_amount"];
	}

	$sql_e = "select cer_expense_amount from cer_expenses " . 
		   "where cer_expense_project = " . param("pid") . 
		   " and cer_expense_cer_version =  " . $cer_version .  
		   " and cer_expense_type IN (3, 18, 19, 20, 22) " . 
		   " and cer_expense_year = " . $first_full_year;

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	while ($row_e = mysql_fetch_assoc($res_e))
	{
		$additional_rental_cost_firstyear = $additional_rental_cost_firstyear + $row_e["cer_expense_amount"];
	}
}
elseif($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
{
	$sql_e = "select cer_expense_amount from cer_expenses " . 
		   "where cer_expense_project = " . param("pid") . 
		   " and cer_expense_cer_version =  " . $cer_version . 
		   " and cer_expense_type = 2 " . 
		   " and cer_expense_year = " . $cer_basicdata["cer_basicdata_firstyear"];

	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$fixedrent_firstyear = $row_e["cer_expense_amount"];
	}

	$sql_e = "select cer_expense_amount from cer_expenses " . 
		   "where cer_expense_project = " . param("pid") . 
		   " and cer_expense_cer_version =  " . $cer_version . 
		   " and cer_expense_type = 16 " . 
		   " and cer_expense_year = " . $cer_basicdata["cer_basicdata_firstyear"];


	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$turnoverrent_firstyear = $row_e["cer_expense_amount"];
	}

	$sql_e = "select cer_expense_amount from cer_expenses " . 
		   "where cer_expense_project = " . param("pid") . 
		   " and cer_expense_cer_version =  " . $cer_version .  
		   " and cer_expense_type IN (3, 18, 19, 20, 22) " . 
		   " and cer_expense_year = " . $cer_basicdata["cer_basicdata_firstyear"];


	$res_e = mysql_query($sql_e) or dberror($sql_e);
	while ($row_e = mysql_fetch_assoc($res_e))
	{
		$additional_rental_cost_firstyear = $row_e["cer_expense_amount"];
	}
}

$fullrent_firstyear = $fixedrent_firstyear + $turnoverrent_firstyear;

$fullrent_firstyear = $fullrent_firstyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
$fullrent_firstyear = round($fullrent_firstyear/1000, 0);

$turnoverrent_firstyear = $turnoverrent_firstyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
$turnoverrent_firstyear = round($turnoverrent_firstyear/1000, 0);

$fixedrent_firstyear = round($fixedrent_firstyear/1000, 0);


//Lease Consitions
$poslease = get_ln_lease_data($project["project_id"], $ln_basicdata["ln_basicdata_id"], $project["pipeline"], $ln_version);

if(count($poslease) > 0)
{
	$lease_type = $poslease["poslease_type_name"];
	$landlord = $poslease["poslease_landlord_name"];
	$negotiated_rental_conditions = $poslease["poslease_negotiated_conditions"];
	$indexrate = $poslease["poslease_indexrate"] . "%";
	$real_estate_fee = $poslease["poslease_realestate_fee"] . "%";

	$free_weeks = $poslease["poslease_freeweeks"];
	$free_months = round($free_weeks/4, 1);
	$termination_deadline = $poslease["poslease_termination_time"];
	
	$renewal_option_date = "";

	if($poslease["poslease_extensionoption"] != Null and $poslease["poslease_extensionoption"] != '0000-00-00')
	{
		$d1 = new DateTime($poslease["poslease_enddate"]);
		$d2 = new DateTime($poslease["poslease_extensionoption"]);
		$interval = date_diff($d2, $d1);

		$tmp = round($interval->d/30, 1) + $interval->m + ($interval->y * 12);
		$renewal_option_date = $tmp . " months (" . to_system_date($poslease["poslease_extensionoption"]) . ")";
	}

	$average_annual_rent = 0;
	$duration_in_years = 0;
	if($poslease["poslease_startdate"] != NULL 
		and $poslease["poslease_startdate"] != '0000-00-00' 
		and $poslease["poslease_enddate"] != NULL
		and $poslease["poslease_enddate"] != '0000-00-00')
		{
			$diff = abs(strtotime($poslease["poslease_enddate"]) - strtotime($poslease["poslease_startdate"]));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

			if($months == 12)
			{
				$months = 0;
				$years++;
			}

			$rental_duration = $years . " years and " . $months . " months";

			$duration_in_years = $years + ($months/12);

			$contract_starting_date = to_system_date($poslease["poslease_startdate"]);
			$contract_ending_date = to_system_date($poslease["poslease_enddate"]);

			/*
			$date1 = date(strtotime($poslease["poslease_startdate"]));
			$date2 = date(strtotime($poslease["poslease_enddate"]));
			
			$difference = $date2 - $date1;
			$rental_duration_in_months = round($difference / 86400 / 30, 1);
            */

			$date1 = new DateTime($poslease["poslease_startdate"]);
			$date2 = new DateTime($poslease["poslease_enddate"]);
			$interval = date_diff($date1, $date2);
			$rental_duration_in_months = round($interval->d/30, 1) + $interval->m + ($interval->y * 12) . ' months';

			$sql_r = "select cer_fixed_rent_total_surface, cer_fixed_rent_amount, cer_fixed_rent_unit " . 
				   " from cer_fixed_rents " . 
				   " where cer_fixed_rent_cer_version = " . $cer_version . 
				   " and cer_fixed_rent_project_id = " . param("pid") . 
				   " order by cer_fixed_rent_from_year ASC ";
			
			$res_r = mysql_query($sql_r) or dberror($sql_r);
			if ($row_r = mysql_fetch_assoc($res_r))
			{
				$tmp_p = 0;
				if($row_r["cer_fixed_rent_unit"] == 1 or $row_r["cer_fixed_rent_unit"] == 2)
				{
					$tmp_p = $row_r["cer_fixed_rent_total_surface"] * $row_r["cer_fixed_rent_amount"];
				}
				elseif($row_r["cer_fixed_rent_unit"] == 3 or $row_r["cer_fixed_rent_unit"] == 3)
				{
					$tmp_p = ($row_r["cer_fixed_rent_total_surface"] * $row_r["cer_fixed_rent_amount"])/12;
				}
				elseif($row_r["cer_fixed_rent_unit"] == 5)
				{
					$tmp_p = $row_r["cer_fixed_rent_amount"];
				}
				elseif($row_r["cer_fixed_rent_unit"] == 6)
				{
					$tmp_p = $row_r["cer_fixed_rent_amount"]/12;
				}
				if($tmp_p > 0)
				{
					$tmp = get_pos_intangibles(param("pid"), 9, $cer_version);
					
					$deposit_value_in_months = 	 round($tmp["cer_investment_amount_cer_loc"] / $tmp_p, 2);
				}
			}
	}
		
	if($duration_in_years > 0)
	{
		$total_lease_commitment = 0;
		$sql_cer = "select * from cer_expenses " .
					  "where cer_expense_cer_version = " . $cer_version . "  and cer_expense_project = " . param("pid") .
					  " and cer_expense_type IN(2, 3, 16, 18, 19, 20)";


		$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
		while ($row_cer = mysql_fetch_assoc($res_cer))
		{
			$total_lease_commitment = $total_lease_commitment + 1*$row_cer["cer_expense_amount"];
		}

		if($duration_in_years > 0)
		{
			$average_annual_rent = round(($total_lease_commitment / $duration_in_years)/1000, 0);
		}
	}
}
else
{
	$landlord = "";
	$negotiated_rental_conditions = "";
	$indexrate = "";
	$real_estate_fee = "";
	$average_annual_rent = "";
	$duration_in_years = "";
	$total_lease_commitment = "";
	$contract_starting_date = "";
	$contract_ending_date = "";
	$free_weeks = "";
	$free_months = "";
	$termination_deadline = "";
	$rental_duration_in_months = "";
	$renewal_option_date = "";
}

//get averag increas in fixed rents
$average_yearly_increase = "";
$num_of_years1 = 0;
$num_of_years2 = 0;
$num_of_years3 = 0;
$index_rate_total = 0;
$increase_rate_total = 0;
$inflation_rate_total = 0;
$sql_e = "select cer_fixed_rent_index_rate, cer_fixed_rent_increas_rate, cer_fixed_rent_inflation_rate " . 
		 "from cer_fixed_rents " . 
		 "where cer_fixed_rent_cer_version = " . $cer_version . " and cer_fixed_rent_project_id = " . param("pid");

$res = mysql_query($sql_e) or dberror($sql_e);

while($row = mysql_fetch_assoc($res))
{
	if($row["cer_fixed_rent_index_rate"] > 0)
	{
		$num_of_years1++;
		$index_rate_total = $index_rate_total + $row["cer_fixed_rent_index_rate"];
	}
	if($row["cer_fixed_rent_increas_rate"] > 0)
	{
		$num_of_years2++;
		$increase_rate_total = $increase_rate_total + $row["cer_fixed_rent_increas_rate"];
	}
	if($row["cer_fixed_rent_inflation_rate"] > 0)
	{
		$num_of_years3++;
		$inflation_rate_total = $inflation_rate_total + $row["cer_fixed_rent_inflation_rate"];
	}
}

$tmp1 = "";
$tmp2 = "";
$tmp3 = "";
if($num_of_years1 > 0)
{
	$tmp1 = "Index Rate: " . number_format($index_rate_total/$num_of_years1, 2) . "% ";
}
if($num_of_years2 > 0)
{
	
	$tmp2 = "Increase Rate: " . number_format($increase_rate_total/$num_of_years2, 2) . "% ";
}
if($num_of_years3 > 0)
{
	$tmp3 = "Inflation Rate: " . number_format($inflation_rate_total/$num_of_years3, 2) . "% ";
}
$average_yearly_increase = $tmp1 . $tmp2 . $tmp3;

//additional rental cost and other fees
//get additional rental costs
$annual_charges = 0;
$tax_on_rents = 0;
$passenger_index = 0;
$savings_on_rent = 0;
$sql_e = "select * from cer_expenses " .
         "where cer_expense_cer_version = " . $cer_version . "  and cer_expense_project = " . param("pid") . 
		 " and cer_expense_type in (3,18,19, 20) " . 
		 " and cer_expense_year = " . $first_full_year . 
		 " order by cer_expense_year";
$res_e = mysql_query($sql_e) or dberror($sql_e);

while($row_e = mysql_fetch_assoc($res_e))
{
	if($row_e["cer_expense_type"] == 3)
	{
		$annual_charges = round($row_e["cer_expense_amount"]/1000, 0);
	}
	elseif($row_e["cer_expense_type"] == 18)
	{
		$tax_on_rents = round($row_e["cer_expense_amount"]/1000, 0);
	}
	elseif($row_e["cer_expense_type"] == 19)
	{
		$passenger_index = round($row_e["cer_expense_amount"]/1000, 0);
	}
	elseif($row_e["cer_expense_type"] == 20)
	{
		$savings_on_rent = round($row_e["cer_expense_amount"]/1000, 0);
	}
}


//turnover percent for rents, only first record
$sales_percent_first_year = "";
$sql_e = "select * " .
	   "from cer_rent_percent_from_sales " .
	   "where cer_rent_percent_from_sale_cer_version = " . $cer_version . "  and cer_rent_percent_from_sale_project = " . param("pid") .
	   " order by cer_rent_percent_from_sale_year";

$res_e = mysql_query($sql_e) or dberror($sql_e);
if($row_e = mysql_fetch_assoc($res_e))
{
	$sales_percent_first_year = round($row_e["cer_rent_percent_from_sale_percent"], 2) . "%";
}

//sellouts
$sellout_ending_year = (int)substr($cer_basicdata["date_created"], 0, 4);

if($sellout_ending_year < (int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4))
{
	$sellout_ending_year =(int)substr($cer_basicdata["cer_basicdata_firstyear"], 0, 4);
}

if($sellout_ending_year > date("Y"))
{
	$sellout_ending_year = date("Y");
}

$sellout_starting_year = $sellout_ending_year - 3;


$currency_symbol = get_currencys_symbol($ln_basicdata["ln_basicdata_currency"]);


//get average turnoverbased rent
$total_sales = 0;
$average_salespercent = 0;
$sql =  "select * from cer_revenues " .
		"where cer_revenue_project = " . param("pid") .
		" and cer_revenue_cer_version = " . $cer_version;

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$sales_watches_values = $row["cer_revenue_watches"];
	$sales_jewellery_values = $row["cer_revenue_jewellery"];
	$sales_customer_service_values = $row["cer_revenue_customer_service"];
	$total_sales = $total_sales + 1*$sales_watches_values + 1*$sales_jewellery_values + 1*$sales_customer_service_values;

}

if($total_sales > 0)
{ 
	$average_salespercent = 100 * $total_lease_commitment / $total_sales/100;
}



//get names of roles
$brand_manager = "";
$ceo = "";
$cfo = "";
$vp_sales = "";
$president = "";
$head_controlling = "";
$country_manager = "";

$sql_u = "select cer_summary_in01_sig01, cer_summary_in01_sig02, cer_summary_in01_sig03 " . 
		 "from cer_summary " . 
		 "where cer_summary_project = " . param("pid") . 
		 " and cer_summary_cer_version = 0";

$res_u = mysql_query($sql_u) or dberror($sql_u);
if($row_u = mysql_fetch_assoc($res_u))
{
	$brand_manager = $row_u["cer_summary_in01_sig03"];
	$country_manager = $row_u["cer_summary_in01_sig01"];
	$head_controlling = $row_u["cer_summary_in01_sig02"];
}

$approval_name1 = $cer_basicdata["cer_basicdata_approvalname1"];
$president = $cer_basicdata["cer_basicdata_approvalname2"];
$ceo = $cer_basicdata["cer_basicdata_approvalname2"];
$cfo = $cer_basicdata["cer_basicdata_approvalname7"];
$vp_sales = $cer_basicdata["cer_basicdata_approvalname3"];
$retail_head = $cer_basicdata["cer_basicdata_approvalname4"];

$pix1 = ".." . $ln_basicdata["ln_basicdata_pix1"];
$pix2 = ".." . $ln_basicdata["ln_basicdata_pix2"];
$pix3 = ".." . $ln_basicdata["ln_basicdata_pix3"];
$pix4 = ".." . $ln_basicdata["ln_basicdata_pix4"];

$floor_plan = ".." . $ln_basicdata["ln_basicdata_floorplan"];
$location_layout = ".." . $ln_basicdata["ln_basicdata_location_layout"];
$lease_agreement = ".." . $ln_basicdata["ln_basicdata_draft_aggreement"];
$relocation_doc = ".." . $ln_basicdata["ln_basicdata_relocation_doc"];
$sales_doc = ".." . $ln_basicdata["ln_basicdata_sales_doc"];
$facade_image = ".." . $ln_basicdata["ln_basicdata_fassade_image"];
$pos_section_image = ".." . $ln_basicdata["ln_basicdata_pos_section_image"];

$page_title = "Lease Negotiation Booklet: " . $row["project_number"];



//LN supporting documents
$ln_supporting_documents_images = array();
$ln_supporting_documents_pdfs = array();
$sql_d = "select order_file_path, order_file_type " . 
		 " from order_files " . 
		 " where order_file_order = " . dbquote($order_number) . 
		 " and order_file_category = 17 " . 
		 " and order_file_type IN (2,13) " . 
		 " order by order_file_type";

$res_d = mysql_query($sql_d) or dberror($sql_d);
while ($row_d = mysql_fetch_assoc($res_d))
{
	

	if($row_d['order_file_type'] == 2) {
		$ln_supporting_documents_images[] = $_SERVER["DOCUMENT_ROOT"] . $row_d["order_file_path"];
	}
	else {
		$ln_supporting_documents_pdfs[] = $_SERVER["DOCUMENT_ROOT"] . $row_d["order_file_path"];
	}
}


if($ln_version == 0)
{
	$version_date = date("d.m.Y H:i:s");
}
else
{
	$version_date = to_system_date($cer_basicdata['date_created']);
}


//get areas and neighbourhood information
$posareas = "";
if($project["pipeline"] == 0)
{
	$sql_i = "select * from posareas " . 
			 "left join posareatypes on posareatype_id = posarea_area " .
			 "where posarea_posaddress = " . $project["posaddress_id"];
	
}
elseif($project["pipeline"] == 1)
{
	
	$sql_p = "select posorder_posaddress from posorderspipeline where posorder_order = " . $project["order_id"];
	$res_p = mysql_query($sql_p) or dberror($sql_p);

	if ($row_p = mysql_fetch_assoc($res_p))
	{
		$sql_i = "select * from posareaspipeline " . 
				 "left join posareatypes on posareatype_id = posarea_area " .
				 "where posarea_posaddress = " . $row_p["posorder_posaddress"];
	}
}
$res_i = mysql_query($sql_i) or dberror($sql_i);
while ($row_i = mysql_fetch_assoc($res_i))
{
	$posareas .= $row_i["posareatype_name"] . ", ";
}
$posareas = substr($posareas,0,strlen($posareas)-2);


//neighbourhood
$neighbourhoods = array();
if($project["pipeline"] == 0)
{
	$sql_e = "select * from posorders where posorder_order = " . $project["order_id"];
}
elseif($project["pipeline"] == 1)
{
	$sql_e = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
}

$res_e = mysql_query($sql_e) or dberror($sql_e);
if ($row_e = mysql_fetch_assoc($res_e))
{
	$tmp = "";
	$bt = get_business_type($row_e["posorder_neighbour_left_business_type"]);
	$pr = get_price_range($row_e["posorder_neighbour_left_price_range"]);
	if($bt or $pr)
	{
		$tmp = " (". $bt . " " . $pr . ")";
	}
	$neighbourhoods["Shop on left side"] = $row_e["posorder_neighbour_left"] . $tmp;

	$tmp = "";
	$bt = get_business_type($row_e["posorder_neighbour_right_business_type"]);
	$pr = get_price_range($row_e["posorder_neighbour_right_price_range"]);
	if($bt or $pr)
	{
		$tmp = " (". $bt . " " . $pr . ")";
	}
	$neighbourhoods["Shop on right side"] = $row_e["posorder_neighbour_right"] . $tmp;

	$tmp = "";
	$bt = get_business_type($row_e["posorder_neighbour_acrleft_business_type"]);
	$pr = get_price_range($row_e["posorder_neighbour_acrleft_price_range"]);
	if($bt or $pr)
	{
		$tmp = " (". $bt . " " . $pr . ")";
	}
	$neighbourhoods["Shop across left side"] = $row_e["posorder_neighbour_acrleft"] . $tmp;

	$tmp = "";
	$bt = get_business_type($row_e["posorder_neighbour_acrright_business_type"]);
	$pr = get_price_range($row_e["posorder_neighbour_acrright_price_range"]);
	if($bt or $pr)
	{
		$tmp = " (". $bt . " " . $pr . ")";
	}
	$neighbourhoods["Shop across right side"] = $row_e["posorder_neighbour_acrright"] . $tmp;
	
	$neighbourhoods["Other brands in area"] = $row_e["posorder_neighbour_brands"];
	$neighbourhoods["Other brands in area"] = str_replace("\r\n", "", $neighbourhoods["Other brands in area"]);
	$neighbourhoods["Other brands in area"] = str_replace("\n", "", $neighbourhoods["Other brands in area"]);
	$neighbourhoods["Other brands in area"] = str_replace("\r", "", $neighbourhoods["Other brands in area"]);
	$neighbourhoods["Other brands in area"] = trim($neighbourhoods["Other brands in area"]);
}		



$ln_brands = $row["ln_basicdata_brands"];

$latest_renovation_date = to_system_date($ln_basicdata['ln_basicdata_latest_renovation']);

?>