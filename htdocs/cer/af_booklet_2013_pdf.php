<?php
/********************************************************************

    af_booklet_2013_pdf.php

    Print PDF for AF Booklet INR03.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-06-29
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-29
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
$pdf_print_output = true;


define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require "include/financial_functions.php";
require "../shared/func_posindex.php";

check_access("has_access_to_cer");

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/SetaPDF/Autoload.php');


$PDFmerger_was_used = false;

$currencies = array();
$sql = 'select currency_id, currency_symbol from currencies where currency_system = 1';

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$currencies["s"] = $row['currency_symbol'];
}


if($cer_basicdata['cer_basicdata_currency']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c1"] = $row['currency_symbol'];
	}
}

if($cer_basicdata['cer_basicdata_currency2']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency2'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c2"] = $row['currency_symbol'];
	}
}

$version_date = ""; 
if($cer_version > 0)
{
	$version_date = " Older Version " . to_system_date(substr($cer_basicdata["version_date"], 0,10)) . " " . substr($cer_basicdata["version_date"], 10,9);
}




//LNR supporting documents
$ln_supporting_documents = array();
$sql_d = "select order_file_path " . 
		 " from order_files " . 
		 " where order_file_order = " . dbquote($project["project_order"]) . 
		 " and order_file_category = 17 " . 
		 " and order_file_type IN (2,13)";

$res_d = mysql_query($sql_d) or dberror($sql_d);
while ($row_d = mysql_fetch_assoc($res_d))
{
	$ln_supporting_documents[] = $_SERVER["DOCUMENT_ROOT"] . $row_d["order_file_path"];
}


// Create and setup PDF document
class MYPDF extends TCPDF
{
	
	//Page footer
	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		$this->SetFont('arialn','I',8);
		$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
	}

}

$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 10);
$pdf->setPrintHeader(false);
$pdf->SetAutoPageBreak(false, 0);

$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');
$pdf->Open();

include("af_INR03_2013_pdf_detail.php");


//page 3: Distribution Analysis
include("lnr_03_pdf_detail_2016.php");


//Picture 2
if($pix2 != ".." and file_exists($pix2))
{
	//PDF
	if(substr($pix2, strlen($pix2)-3, 3) == "jpg" 
		or substr($pix2, strlen($pix2)-3, 3) == "JPG"
	    or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
		or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
	{
		$pdf->AddPage("P", "A4");
		$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
		
		if(isset($version_date) and $version_date)
		{
			$pdf->SetFont('arialn','B',12);
			//Move to the right
			$pdf->Cell(40);
			//Title
			$pdf->SetY(0);
			$pdf->Cell(0,33,$page_title . $version_date,0,0,'R');
			//Line break
			$pdf->SetY(23);
		}
		else
		{
			//arialn bold 15
			$pdf->SetFont('arialn','B',12);
			//Move to the right
			$pdf->Cell(80);
			//Title
			$pdf->SetY(0);
			$pdf->Cell(0,33,$page_title,0,0,'R');
			//Line break
			$pdf->SetY(23);
		}

		$x = $pdf->GetX();
		$y = $pdf->GetY() + 2;
		$pdf->Image($pix2,$x,$y, 0, 110, "", "", "", true, 300, "", false, false, 0, false, false,true);
	}
}


//Picture 3
if($pix3 != ".." and file_exists($pix3))
{
	//PDF
	if(substr($pix3, strlen($pix3)-3, 3) == "jpg" 
		or substr($pix3, strlen($pix3)-3, 3) == "JPG"
	    or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
		or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
	{
		//$pdf->AddPage("P", "A4");
		$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
		if(isset($version_date) and $version_date)
		{
			$pdf->SetFont('arialn','B',12);
			//Move to the right
			$pdf->Cell(40);
			//Title
			$pdf->SetY(0);
			$pdf->Cell(0,33,$page_title . $version_date,0,0,'R');
			//Line break
		}
		else
		{
			//arialn bold 15
			$pdf->SetFont('arialn','B',12);
			//Move to the right
			$pdf->Cell(80);
			//Title
			$pdf->SetY(0);
			$pdf->Cell(0,33,$page_title,0,0,'R');
		}
		
		$y = 145;
		$pdf->Image($pix3,$x,$y, 0, 110, "", "", "", true, 300, "", false, false, 0, false, false,true);
	}
}


if(file_exists($floor_plan))
{
	//PDF
	if(substr($floor_plan, strlen($floor_plan)-3, 3) == "pdf" or substr($floor_plan, strlen($floor_plan)-3, 3) == "PDF")
	{
		
		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		
		$merger = new SetaPDF_Merger();
		$merger->addDocument($tmp);

		$merger->addFile(array(
			'filename' => $floor_plan,
			'copyLayers' => true
		));

		$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
		$pdf->SetMargins(10, 23, 10);
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		$pdf->AddFont('arialn','');
		$pdf->AddFont('arialn','B');
		$pdf->AddFont('arialn','I');
		$pdf->AddFont('arialn','BI');
		$pdf->Open();

		$PDFmerger_was_used = true;
		
		
	}
	elseif(substr($floor_plan, strlen($floor_plan)-3, 3) == "jpg" 
		or substr($floor_plan, strlen($floor_plan)-3, 3) == "JPG"
	     or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
		or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
	{
		$pdf->setPrintFooter(true);
		$pdf->AddPage("P", "A4");
		
		$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
		$pdf->SetFont('arialn','B',12);
		$pdf->Cell(80);
		$pdf->SetY(0);
		$pdf->Cell(0,33,"Mall Map/Street Map",0,0,'R');

		$pdf->SetY(23);
		$pdf->Image($floor_plan,10,30,180,0,  "", "", "", true, 300, "", false, false, 0, false, false,true);

		
	}
}



if(file_exists($location_layout))
{
	//PDF
	if(substr($location_layout, strlen($location_layout)-3, 3) == "pdf" 
		or substr($location_layout, strlen($location_layout)-3, 3) == "PDF")
	{
		
		if($PDFmerger_was_used == false)
		{
			$pdfString = $pdf->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
			
			$merger = new SetaPDF_Merger();
			$merger->addDocument($tmp);
		}
		$merger->addFile(array(
			'filename' => $location_layout,
			'copyLayers' => true
		));

		$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
		$pdf->SetMargins(10, 23, 10);
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		$pdf->AddFont('arialn','');
		$pdf->AddFont('arialn','B');
		$pdf->AddFont('arialn','I');
		$pdf->AddFont('arialn','BI');
		$pdf->Open();

		$PDFmerger_was_used = true;
		
		
	}
}


//business plan
include("cer_inr02_pdf_detail.php");


//business plan in system'currency
if(array_key_exists("s", $currencies) and array_key_exists("c1", $currencies) and $currencies["s"] != $currencies["c1"])
{
	$cid = 's';
	include("cer_inr02_pdf_detail.php");
}

//business plan in pos'currency
if(array_key_exists("c2", $currencies) and array_key_exists("c1", $currencies) and $currencies["c2"] != $currencies["c1"])
{
	$cid = 'c2';
	include("cer_inr02_pdf_detail.php");
}


//list of variables
include("cer_listofvars_pdf_detail.php");

$output_done = true;

//AF supporting documents
if(count($ln_supporting_documents) > 0)
{
	foreach($ln_supporting_documents as $key=>$source_file)
	{
	
		if(file_exists($source_file))
		{
			//PDF
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				
				if($output_done == true)
				{
					$pdfString = $pdf->output('', 'S');
					$tmp = SetaPDF_Core_Document::loadByString($pdfString);
					
					
					if(!isset($merger)) {
						$merger = new SetaPDF_Merger();
					}
					$merger->addDocument($tmp);
					$output_done = false;
				}

				$merger->addFile(array(
					'filename' => $source_file,
					'copyLayers' => true
				));


				$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetMargins(10, 23, 10);
				$pdf->Open();
				

				$PDFmerger_was_used = true;
				$output_done = false;

			}
			elseif(substr($source_file, strlen($source_file)-3, 3) == "jpg" 
				or substr($source_file, strlen($source_file)-3, 3) == "JPG"
			     or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
				or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
			{
				$pdf->AddPage("P");
				$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
				//arialn bold 15
				if(isset($version_date) and $version_date)
				{
					$pdf->SetFont('arialn','B',12);
					//Move to the right
					$pdf->Cell(40);
					//Title
					$pdf->SetY(0);
					$pdf->Cell(0,33,$page_title . $version_date,0,0,'R');
					//Line break
					$pdf->SetY(23);
				}
				else
				{
					$pdf->SetFont('arialn','B',18);
					//Move to the right
					$pdf->Cell(80);
					//Title
					$pdf->SetY(0);
					$pdf->Cell(0,33,$page_title,0,0,'R');
					//Line break
					$pdf->SetY(23);
				}
				$pdf->Image($source_file,10,30, 180, 0, "", "", "", true, 300, "", false, false, 0, false, false,true);
				$output_done = true;
				
			}
		}
	}

}



//check if project has jpg attachments to be added to the booklet

$sql = "select order_file_path " . 
	   "from projects " . 
	   "left join order_files on order_file_order = project_order " . 
	   "where order_file_attach_to_cer_booklet = 1 and order_file_type= 2 and project_id = " .param("pid") . 
	   " order by order_files.date_created DESC";

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$source_file = ".." . $row["order_file_path"];
	if(file_exists($source_file))
	{
		$pdf->AddPage("L");
		$margin_top = 16;
		$margin_left = 12;
		$pdf->SetXY($margin_left,$margin_top);

		$pdf->SetFont("arialn", "I", 9);
		$pdf->Cell(45, 10, "", 1);

		$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);

		$pdf->SetFont("arialn", "B", 11);
		$pdf->Cell(172, 10, "Layout: " . $project_name, 1, "", "L");
		$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");

		$pdf->Image($source_file,20,40, 200);
		$output_done = true;
	}
}


//check if project has pdf attachments to be added to the booklet

$sql = "select order_file_path " . 
       "from projects " . 
	   "left join order_files on order_file_order = project_order " . 
       "where order_file_attach_to_cer_booklet = 1 and order_file_type = 13 and project_id = " .param("pid") . 
	   " order by order_files.date_created DESC";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$source_file = ".." . $row["order_file_path"];
	if(file_exists($source_file))
	{
		if($output_done == true)
		{
			$pdfString = $pdf->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
			
			
			if(!isset($merger)) {
				$merger = new SetaPDF_Merger();
			}
			$merger->addDocument($tmp);
		}
		
		if(!isset($merger)) {
			$merger = new SetaPDF_Merger();
		}
		
		$merger->addFile(array(
			'filename' => $source_file,
			'copyLayers' => true
		));


		$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(true);

		$pdf->Open();

		$output_done = false;
		$PDFmerger_was_used = true;

	}
}


if($use_old_ln_forms_before_2013 != true)
{
	include("distribution_summary_pdf_detail.php");
	if($distribution_summary_available == true)
	{
		$output_done = true;
	}
}

$file_name = "LN_Booklet_" . str_replace(" ", "_", $posname) . "_" . date("Y-m-d") . ".pdf";



if($PDFmerger_was_used == true)
{

	if($output_done == false)
	{
		$merger->merge();
		$document = $merger->getDocument();

		$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
		$document->save()->finish();
	}
	else
	{
	
		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		
		$merger->merge();
		$document = $merger->getDocument();
		$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
		$document->save()->finish();
	}
}
else
{
	$pdf->Output($file_name, 'I');
}


?>