<?php


//key points
$pdf->AddPage("L");
$globalPageNumber++;

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(270, 8, "  Key Points: " . $project["country_name"] . " - " . $pos_data["place_name"] . " - " . $pos_data["posaddress_name"] . " - Project " . $project['project_number'], 1, "", "L");

$pdf->setY(25);



$x= $margin_left;
$pdf->SetFont("arialn", "B", 10);
$pdf->SetX($x);
$pdf->Cell(100, 8, "Project", 0, "", "L");
$pdf->Ln();

$pdf->SetX($x);
$pdf->SetFont("arialn", "", 9);
$pdf->MultiCell(130,0, $postype, 0, "T", 0, 1);


if(isset($relocated_pos_info) 
	and $relocated_pos_info) //relocation
{
	$x= $margin_left;
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 8, "Relocated POS", 0, "", "L");
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(130,0, $relocated_pos_info, 0, "T", 0, 1);
}

if($latest_renovation_date 
	and $latest_renovation_date != '0000-00-00') {
	$x= $margin_left;
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 8, "Latest Renovation", 0, "", "L");
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(130,0, $latest_renovation_date, 0, "T", 0, 1);


}


$x= $margin_left;
$pdf->SetFont("arialn", "B", 10);
$pdf->SetX($x);
$pdf->Cell(100, 8, "Key Points", 0, "", "L");
$pdf->Ln();

$pdf->SetX($x);
$pdf->SetFont("arialn", "", 9);
$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_remarks'], 0, "T", 0, 1);


$pdf->SetFont("arialn", "B", 10);
$pdf->SetX($x);
$pdf->Cell(100, 8, "Advantages, Disadvantages", 0, "", "L");
$pdf->Ln();

$pdf->SetX($x);
$pdf->SetFont("arialn", "", 9);
$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_remarks1'], 0, "T", 0, 1);




if($project["project_projectkind"] !=5) {
	
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 8, "POS Strategy - Brand Image", 0, "", "L");
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_remarks2'], 0, "T", 0, 1);
	
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 8, "POS Strategy - Sales Development", 0, "", "L");
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_remarks3'], 0, "T", 0, 1);


	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 8, "POS Strategy - Market Situation", 0, "", "L");
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_remarks4'], 0, "T", 0, 1);
}
else {
	
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 8, "Justification", 0, "", "L");
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_remarks2'], 0, "T", 0, 1);
	
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 8, "Commitment by the Brand Manager", 0, "", "L");
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_remarks3'], 0, "T", 0, 1);


	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 8, "Commitment by the Country Manager", 0, "", "L");
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_remarks4'], 0, "T", 0, 1);
}


$pdf->Ln();
$pdf->SetX($x);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(30,8,"Area/Neighbourhood:",0, 0, 'L', 0);
$pdf->Ln();

$x=$x+1;
$pdf->SetFont("arialn", "", 9);
$pdf->SetX($x);
$pdf->Cell(30,5,"Area",1, 0, 'L', 0);
$pdf->Cell(100,5,$posareas,1, 0, 'L', 0);
$pdf->Ln();

if(count($neighbourhoods) > 0) {
	
	$pdf->SetX($x);
	$pdf->Cell(30,5,"Left side:",1, 0, 'L', 0);
	$pdf->Cell(100,5,$neighbourhoods["Shop on left side"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->Cell(30,5,"Right side:",1, 0, 'L', 0);
	$pdf->Cell(100,5,$neighbourhoods["Shop on right side"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->Cell(30,5,"Opposite Left:",1, 0, 'L', 0);
	$pdf->Cell(100,5,$neighbourhoods["Shop across left side"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->Cell(30,5,"Opposite right:",1, 0, 'L', 0);
	$pdf->Cell(100,5,$neighbourhoods["Shop across right side"],1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->MultiCell(130,0, "Other brands in the area: " . "\n" . $neighbourhoods["Other brands in area"], 1, "T");
}


//columns 2

if(isset($poslease) and
	count($poslease) > 0 and
   ($project["project_projectkind"] == 1
   or $project["project_projectkind"] == 4
   or $project["project_projectkind"] == 5
   or $project["project_projectkind"] == 9)) {

    $pdf->setY(25);
	$x = 150;
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 8, "Rental Details", 0, "", "L");
	$pdf->Ln();
	
	$x = 151;
	$pdf->SetFont('arialn','',9);
	
	$pdf->SetX($x);
	$pdf->Cell(27,6,"Lease Type:",1, 0, 'L', 0);
	if(count($poslease) > 0) {
		$pdf->Cell(18,6,$poslease["poslease_type_name"],1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(18,6,"",1, 0, 'L', 0);
	}
	$pdf->Cell(25,6,"Start Date:",1, 0, 'L', 0);
	if(count($poslease) > 0) {
		$pdf->Cell(18,6, to_system_date($poslease["poslease_startdate"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(18,6, "",1, 0, 'L', 0);
	}
	$pdf->Cell(25,6,"End Date:",1, 0, 'L', 0);
	if(count($poslease) > 0) {
		$pdf->Cell(18,6,to_system_date($poslease["poslease_enddate"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(18,6,"",1, 0, 'L', 0);
	}
	$pdf->Ln();


	$pdf->SetX($x);
	$pdf->Cell(27,6,"Extension Option:",1, 0, 'L', 0);
	if(count($poslease) > 0) {
		$pdf->Cell(18,6,to_system_date($poslease["poslease_extensionoption"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(18,6,"",1, 0, 'L', 0);
	}
	$pdf->Cell(25,6,"Exit Option to Date:",1, 0, 'L', 0);
	if(count($poslease) > 0) {
		$pdf->Cell(18,6,to_system_date($poslease["poslease_exitoption"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(18,6,"",1, 0, 'L', 0);
	}
	$pdf->Cell(25,6,"Term.Deadline:",1, 0, 'L', 0);
	if(count($poslease) > 0) {
		$pdf->Cell(18,6,$poslease["poslease_termination_time"],1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(18,6,"",1, 0, 'L', 0);
	}
	$pdf->Ln();

	
		
	
	$pdf->SetX($x);
	

	$pdf->Cell(27,6,"Av. rent p.a. in " . $currency_symbol . ":",1, 0, 'L', 0);
	$pdf->Cell(18,6, number_format($average_annual_rent, 0, '.', "'"),1, 0, 'L', 0);
	$pdf->Cell(25,6,"Av. rent % of sales:",1, 0, 'L', 0);
	$pdf->Cell(18,6,round(100*$average_salespercent,2) . "%",1, 0, 'L', 0);
	
	
	$text = "";
	if(count($poslease) > 0) {
		if($poslease["poslease_indexclause_in_contract"] == 1) {
			$text .= "Index Clause";
		}
		if($poslease["poslease_indexclause_in_contract"] == 1) {
			if($text) {
			$text .= "/";	
			}
			$text .= "Tacit Renewal Clause";
		}
	}
	
	$pdf->Cell(43,6,$text,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->Cell(27,6,"Deadline for property:",1, 0, 'L', 0);
	$pdf->Cell(18,6,to_system_date($cer_basicdata["cer_basicdata_deadline_property"]),1, 0, 'L', 0);
	$pdf->Cell(25,6,"Handover Date:",1, 0, 'L', 0);
	if(count($poslease) > 0) {
		$pdf->Cell(18,6,to_system_date($poslease["poslease_handoverdate"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(18,6,"",1, 0, 'L', 0);
	}
	$pdf->Cell(25,6,"First Rent Payed:",1, 0, 'L', 0);
	if(count($poslease) > 0) {
		$pdf->Cell(18,6,to_system_date($poslease["poslease_firstrentpayed"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(18,6,"",1, 0, 'L', 0);
	}
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->Cell(27,6,"Total surface sqm:",1, 0, 'L', 0);
	$pdf->Cell(18,6,$total_surface,1, 0, 'L', 0);
	$pdf->Cell(25,6,"Sales surface sqm",1, 0, 'L', 0);
	$pdf->Cell(18,6,$sale_surface,1, 0, 'L', 0);
	$pdf->Cell(25,6,"Rent Free Weeks:",1, 0, 'L', 0);
	if(count($poslease) > 0) {
		$pdf->Cell(18,6,$poslease["poslease_freeweeks"],1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(18,6,"",1, 0, 'L', 0);
	}
	$pdf->Ln();

	
	$pdf->SetX($x);
	$pdf->Cell(27,6,"Negotiator:",1, 0, 'L', 0);
	if(count($poslease) > 0) {
		$pdf->Cell(104,6,$poslease["poslease_negotiator"],1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(104,6,"",1, 0, 'L', 0);
	}
	$pdf->Ln();

	$pdf->SetX($x);
	$text = "Landlord (Negotiation Partner):\r\n";
	if(count($poslease) > 0) {
		$text .= $poslease["poslease_landlord_name"];
	}

	$pdf->MultiCell(131,3.5, $text, 1, "T");
	$pdf->SetX($x);
	$text = "Conditions for the application of variable rent:\r\n";
	if(count($poslease) > 0) {
		$text .= $poslease["poslease_negotiated_conditions"];
	}

	$pdf->MultiCell(131,3.5, $text, 1, "T");


	$x = 150;
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 8, "Rental Payments in " . $currency_symbol, 0, "", "L");
	$pdf->Ln();

	$x = 151;


	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 9);
	$pdf->Cell(35,5,"",1, 0, 'L', 0);
	

	for($i=0;$i<6;$i++)
	{
		if(array_key_exists($i, $years))
		{
			$pdf->Cell(16,5,$years[$i],1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(16,5," ",1, 0, 'R', 0);
		}
	}
	
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->Cell(35,5,"Fix rental costs",1, 0, 'L', 0);

	for($i=0;$i<6;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $fixedrents))
		{
			$pdf->Cell(16,5,number_format($fixedrents[$years[$i]], 0, ".", "'"),1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(16,5," ",1, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->Cell(35,5,"Turnover based rental costs",1, 0, 'L', 0);
	for($i=0;$i<6;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $turnoverbasedrents))
		{
			$pdf->Cell(16,5,number_format($turnoverbasedrents[$years[$i]], 0, ".", "'"),1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(16,5," ",1, 0, 'R', 0);
		}
	}
	$pdf->Ln();


	$pdf->SetX($x);
	$pdf->Cell(35,5,"Additional rental costs",1, 0, 'L', 0);
	for($i=0;$i<6;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $additionalrents))
		{
			$pdf->Cell(16,5,number_format($additionalrents[$years[$i]], 0, ".", "'"),1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(16,5," ",1, 0, 'R', 0);
		}
	}
	$pdf->Ln();


	$pdf->SetX($x);
	$pdf->Cell(35,5,"Total rental costs",1, 0, 'L', 0);
	for($i=0;$i<6;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $sales_units_watches_values))
		{
			$pdf->Cell(16,5,number_format($rents[$years[$i]], 0, ".", "'"),1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(16,5," ",1, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	if(count($years) > 5) {
		$pdf->SetX($x);
		$pdf->Cell(35,5,"",1, 0, 'L', 0);
		for($i=6;$i<12;$i++)
		{
			if(array_key_exists($i, $years))
			{
				$pdf->Cell(16,5,$years[$i],1, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(16,5," ",1, 0, 'R', 0);
			}
		}
		
		$pdf->Ln();

		$pdf->SetX($x);
		$pdf->Cell(35,5,"Fix rental costs",1, 0, 'L', 0);

		for($i=6;$i<12;$i++)
		{
			if(array_key_exists($i, $years) and array_key_exists($years[$i], $fixedrents))
			{
				$pdf->Cell(16,5,number_format($fixedrents[$years[$i]], 0, ".", "'"),1, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(16,5," ",1, 0, 'R', 0);
			}
		}
		$pdf->Ln();

		$pdf->SetX($x);
		$pdf->Cell(35,5,"Turnover based rental costs",1, 0, 'L', 0);
		for($i=6;$i<12;$i++)
		{
			if(array_key_exists($i, $years) and array_key_exists($years[$i], $turnoverbasedrents))
			{
				$pdf->Cell(16,5,number_format($turnoverbasedrents[$years[$i]], 0, ".", "'"),1, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(16,5," ",1, 0, 'R', 0);
			}
		}
		$pdf->Ln();


		$pdf->SetX($x);
		$pdf->Cell(35,5,"Additional rental costs",1, 0, 'L', 0);
		for($i=6;$i<12;$i++)
		{
			if(array_key_exists($i, $years) and array_key_exists($years[$i], $additionalrents))
			{
				$pdf->Cell(16,5,number_format($additionalrents[$years[$i]], 0, ".", "'"),1, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(16,5," ",1, 0, 'R', 0);
			}
		}
		$pdf->Ln();


		$pdf->SetX($x);
		$pdf->Cell(35,5,"Total rental costs",1, 0, 'L', 0);
		for($i=6;$i<12;$i++)
		{
			if(array_key_exists($i, $years) and array_key_exists($years[$i], $sales_units_watches_values))
			{
				$pdf->Cell(16,5,number_format($rents[$years[$i]], 0, ".", "'"),1, 0, 'R', 0);
			}
			else
			{
				$pdf->Cell(16,5," ",1, 0, 'R', 0);
			}
		}
		$pdf->Ln();
	}
	
}
elseif($project["project_projectkind"] == 2
   or $project["project_projectkind"] == 3
   or $project["project_projectkind"] == 6 ) {

	$pdf->setY(25);
	$x= 150;
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 8, "Previous Rental Conditions", 0, "", "L");
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_passedrent'], 0, "T", 0, 1);

	$pdf->Cell(100, 0, "", 0, "", "L");
	$pdf->Ln();

	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 0, "Previous Sellouts in " . $currency_symbol, 0, "", "L");
	$pdf->Ln();

	$pdf->SetFont("arialn", "B", 9);
	$pdf->SetX($x+1);
	$pdf->Cell(29, 0, "Year (Months)", 1, "", "L");

	$pdf->SetX($x+30);
	foreach($sellouts_watches as $year=>$amount) {
		$pdf->Cell(25,0, $year . " (" . $sellouts_months[$year] . ")", 1, "", "R");
	}

	$pdf->SetFont("arialn", "", 9);
	$pdf->Ln();
	$pdf->SetX($x+1);
	$pdf->Cell(29, 0, "Watches units", 1, "", "L");
	$pdf->SetX($x+30);
	foreach($sellouts_watches as $year=>$amount) {
		$pdf->Cell(25,0, number_format($amount, 0, '.', "'"), 1, "", "R");
	}
	$pdf->Ln();
	$pdf->SetX($x+1);
	$pdf->Cell(29, 0, "Watche straps units", 1, "", "L");
	foreach($sellouts_bjoux as $year=>$amount) {
		$pdf->Cell(25,0, number_format($amount, 0, '.', "'"), 1, "", "R");
	}
	$pdf->Ln();
	$pdf->SetX($x+1);
	$pdf->Cell(29, 0, "Gross Sales", 1, "", "L");
	foreach($grosssales_watches as $year=>$amount) {
		$pdf->Cell(25,0, number_format($amount + $grosssale_bjoux[$year], 0, '.', "'"), 1, "", "R");
	}
	$pdf->Ln();
	$pdf->SetX($x+1);
	$pdf->Cell(29, 0, "Net Sales", 1, "", "L");
	foreach($net_sales as $year=>$amount) {
		$pdf->Cell(25,0, number_format($amount, 0, '.', "'"), 1, "", "R");
	}
	$pdf->Ln();
	$pdf->SetX($x+1);
	$pdf->Cell(29, 0, "Op. Income excl. WSM", 1, "", "L");
	foreach($operating_incomes1 as $year=>$amount) {
		$pdf->Cell(25,0, number_format($amount, 0, '.', "'"), 1, "", "R");
	}
	$pdf->Ln();
	$pdf->SetX($x+1);
	$pdf->Cell(29, 0, "WSM", 1, "", "L");
	foreach($wsm as $year=>$amount) {
		$pdf->Cell(25,0, number_format($amount, 2, '.', "'") . '%', 1, "", "R");
	}
	$pdf->Ln();
	$pdf->SetX($x+1);
	$pdf->Cell(29, 0, "Op. Income incl. WSM", 1, "", "L");
	foreach($operating_incomes2 as $year=>$amount) {
		$pdf->Cell(25,0, number_format($amount, 0, '.', "'"), 1, "", "R");
	}
}


if(isset($relocated_pos_info)
	and $relocated_pos_info) {
	
	$pdf->Ln();
	$pdf->Ln();
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 8, "Estimated Closing Costs in " . $currency_symbol, 0, "", "L");
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(130,0, number_format($ln_basicdata['ln_basicdata_relocation_costs'], 0, '.', "'"), 0, "T", 0, 1);


	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 8, "Description of estimated closing costs", 0, "", "L");
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_relocation_comment'], 0, "T", 0, 1);
	
}


?>