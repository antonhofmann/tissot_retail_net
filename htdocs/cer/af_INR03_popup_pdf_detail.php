<?php
/********************************************************************

    af_INR03_popup_pdf_detail.php

    Print PDF for AF Form INR03 Detail Data for PopUps.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-06-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-06-16
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.
*********************************************************************/

$approval_name1 = $cer_basicdata["cer_basicdata_approvalname1"];
$approval_name2 = $cer_basicdata["cer_basicdata_approvalname2"];
$approval_name3 = $cer_basicdata["cer_basicdata_approvalname3"];
$approval_name4 = $cer_basicdata["cer_basicdata_approvalname4"];
$approval_name5 = $cer_basicdata["cer_basicdata_approvalname5"];
$approval_name6 = $cer_basicdata["cer_basicdata_approvalname6"];
$approval_name7 = $cer_basicdata["cer_basicdata_approvalname7"];
$approval_name8 = $cer_basicdata["cer_basicdata_approvalname8"];
$approval_name9 = str_replace(' / ', "\r\n", $cer_basicdata["cer_basicdata_approvalname9"]);
$approval_name9 = str_replace('/', "\r\n", $approval_name9);
$approval_name10 = $cer_basicdata["cer_basicdata_approvalname10"];
$approval_name12 = $cer_basicdata["cer_summary_in01_sig02"];
$key_points = $cer_basicdata["cer_summary_in01_description"];


// get all data needed from project
$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
	   "left join currencies on currency_id = country_currency " . 
       "left join postypes on postype_id = project_postype ".
	   "left join possubclasses on possubclass_id = project_pos_subclass ".
	   "left join ln_basicdata on ln_basicdata_project = project_id ".
	   "left join floors on floor_id = project_floor " . 
	    "left join users on user_id = project_retail_coordinator " . 
	   "where project_id = " . param("pid") . 
	   " and ln_basicdata_version = " . $ln_version;

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	
	$legal_entity_name = $row["address_company"];
		
	$project_name = $row["order_shop_address_company"]. ", " . $row["order_shop_address_place"] . ", " . $row["country_name"];


	$project_manager = $row["user_firstname"] . " " . $row["user_name"];
	
	$project_kind = $row["projectkind_name"];


	
	$factor = $cer_basicdata['cer_basicdata_factor'];
	if(!$cer_basicdata['cer_basicdata_factor'])
	{
		$factor = 1;
	}

	$exchange_rate = 1;
	if($cer_basicdata['cer_basicdata_exchangerate'] > 0)
	{
		$exchange_rate = $cer_basicdata['cer_basicdata_exchangerate'];
	}
	
	
	$budget_amount = $row["project_approximate_budget"]/$cer_basicdata['cer_basicdata_exchangerate']*$factor;

	$budget_amount = number_format($budget_amount, 0, "", "'");
	$project_start = substr($row["order_date"], 5,2) . "/" . substr($row["order_date"], 0,4);

	$tmp = date('Y-m-d H:i:s', strtotime($row["project_real_opening_date"] . ' + 90 day'));

	
	$project_end = substr($tmp, 5,2) . "/" . substr($tmp, 0,4);
	$order_number = $row["project_order"];
	$address_country = $row["address_country"];

	$opening_date = to_system_date($row["project_real_opening_date"]);

	if($row["project_projectkind"] == 1
		or $project["project_projectkind"] == 9)  {
		$project_kind_new = "X";
	}
	elseif($project["project_projectkind"] == 6)  {
		$project_kind_relocation = "X";
	}
	elseif($row["project_projectkind"] == 2 
		or $row["project_projectkind"] == 3)  {
		$project_kind_renovation = "X";
	}


	// franchisee
	$franchisee_name = "";
	$franchisee_company = "";
	$sql_a = "select * from addresses where address_id = " . dbquote($row["order_franchisee_address_id"]);
	$res_a = mysql_query($sql_a) or dberror($sql_a);
	if($row_a = mysql_fetch_assoc($res_a))
	{
		$franchisee_name = $row_a["address_company"];
		$franchisee_company = $row_a["address_company"];
		if($row_a["address_contact_name"])
		{
			$franchisee_name .= "\r\n" . $row_a["address_contact_name"];
			
		}
	}


	//relocated POS-Name
	$relocated_pos_name = "";
	if($row["project_relocated_posaddress_id"] > 0)
	{
		$relocated_pos_data = get_relocated_pos_info($row["project_relocated_posaddress_id"]);

			$relocated_pos_name = $relocated_pos_data["posaddress_name"] .", " .
			$relocated_pos_data["posaddress_zip"] . " " .
			$relocated_pos_data["place_name"];
	}


	//get data for detail Pages
	$order_number = $row["project_order"];
	//POS Basic Data
	$posname = $row["order_shop_address_company"];

	if($row["order_shop_address_company2"])
    {
		$posname .= ", " . $row["order_shop_address_company2"];
	}

	
	$posaddress = $row["country_name"];
	$country_name = $row["country_name"];

	if($row["order_shop_address_zip"])
    {
		$posaddress .= ", " . $row["order_shop_address_zip"] . " " . $row["order_shop_address_place"];
	}
	else
	{
		$posaddress .= ", " . $row["order_shop_address_place"];
	}

	$city_name = $row["order_shop_address_place"];

	$posaddress .= ", " . $row["order_shop_address_address"];
	
	if($row["order_shop_address_address2"])
    {
		$posaddress .= ", " . $row["order_shop_address_address2"];
	}
	
	$planned_takeover_date = to_system_date($row["project_planned_takeover_date"]);
	$planned_opening_date = to_system_date($row["project_real_opening_date"]);
	$planned_closing_date = to_system_date($row["project_planned_closing_date"]);
	//$planned_opening_date = to_system_date($row["project_planned_opening_date"]);
	$sales_surface = $row["project_cost_sqms"];
	$total_surface = $row["project_cost_totalsqms"];
	//$gross_surface = $row["project_cost_grosssqms"];
	$gross_surface = $row["project_cost_totalsqms"];

	if($cer_version > 0 and $cer_basicdata["cer_basicdata_version_sqms"] > 0)
	{
		$sales_surface = $cer_basicdata["cer_basicdata_version_sqms"];
		$total_surface = $cer_basicdata["cer_basicdata_version_totalsqms"];
		//$gross_surface = $cer_basicdata["cer_basicdata_version_grosssqms"];
		$gross_surface = $cer_basicdata["cer_basicdata_version_totalsqms"];
	}

	$page_title = "Approval Form Franchisee Location";

	
	if($row["productline_subclass_name"])
	{
		$postype = $row["projectkind_name"] . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"] . " / " . $row["productline_subclass_name"];
	
	}
	else {
		$postype = $row["projectkind_name"] . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"];
	}

	$postype = $row["project_costtype_text"] . " " . $row["postype_name"] . " " . $row["possubclass_name"];

	$project_kind = $row["projectkind_name"];

	if(($project["project_projectkind"] == 6 or $project["project_projectkind"] == 9) 
		and $project["project_relocated_posaddress_id"] > 0) // relocation
	{
		$relocated_pos = get_relocated_pos_info($project["project_relocated_posaddress_id"]);
		$project_kind .= " of " . $relocated_pos["posaddress_name"];
	
	}

	$placement = $row["floor_name"];

	$deadline_for_property = to_system_date($cer_basicdata["cer_basicdata_deadline_property"]);

	//get keymoney and deposit
	$tmp = get_pos_intangibles(param("pid"), 15);
	if(count($tmp) > 0)
	{
		$keymoney = $tmp ["cer_investment_amount_cer_loc"];
		$keymoney = $keymoney*$cer_basicdata["cer_basicdata_exchangerate"]/$cer_basicdata["cer_basicdata_factor"];
		$keymoney = round($keymoney/1000, 0);
		

		$tmp = get_pos_intangibles(param("pid"), 9);
		$depositposted = $tmp ["cer_investment_amount_cer_loc"];
		$depositposted = $depositposted*$cer_basicdata["cer_basicdata_exchangerate"]/$cer_basicdata["cer_basicdata_factor"];
		$depositposted = round($depositposted/1000, 0);
	}
	else
	{
		$keymoney = "";
		$depositposted = "";
	}

	//get rents
	$first_full_year = $cer_basicdata["cer_basicdata_firstyear"];
	if($cer_basicdata["cer_basicdata_firstmonth"] > 1)
	{
		$first_full_year = $cer_basicdata["cer_basicdata_firstyear"]+1;
	}

	$last_full_year = $cer_basicdata["cer_basicdata_lastyear"];
	if($cer_basicdata["cer_basicdata_lastmonth"] < 12)
	{
		$last_full_year = $cer_basicdata["cer_basicdata_lastyear"]-1;
	}

	$fullrent_firstyear = 0;
	$fixedrent_firstyear = 0;
	$turnoverrent_firstyear = 0;
	$additional_rental_cost_firstyear = 0;
	$fullrent_firstyear_loc = 0;

	if($first_full_year > 0 and $cer_basicdata["cer_basicdata_firstyear"] < $cer_basicdata["cer_basicdata_lastyear"])
	{
		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_project = " . param("pid") . 
			   " and cer_expense_cer_version =  " . $cer_version .  
			   " and cer_expense_type = 2 " . 
			   " and cer_expense_year = " . $first_full_year;

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$fixedrent_firstyear = $row_e["cer_expense_amount"];
		}

		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_project = " . param("pid") . 
			   " and cer_expense_cer_version =  " . $cer_version .  
			   " and cer_expense_type = 16 " . 
			   " and cer_expense_year = " . $first_full_year;

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$turnoverrent_firstyear = $row_e["cer_expense_amount"];
		}


		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_project = " . param("pid") . 
			   " and cer_expense_cer_version =  " . $cer_version .  
			   " and cer_expense_type IN (3, 18, 19, 20, 22) " . 
			   " and cer_expense_year = " . $first_full_year;

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		while ($row_e = mysql_fetch_assoc($res_e))
		{
			$additional_rental_cost_firstyear = $additional_rental_cost_firstyear + $row_e["cer_expense_amount"];
		}

			}
	elseif($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
	{
		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_project = " . param("pid") .
			   " and cer_expense_cer_version =  " . $cer_version .  
			   " and cer_expense_type = 2 " . 
			   " and cer_expense_year = " . $cer_basicdata["cer_basicdata_firstyear"];

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$fixedrent_firstyear = $row_e["cer_expense_amount"];
		}

		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_project = " . param("pid") . 
			   " and cer_expense_cer_version =  " . $cer_version .  
			   " and cer_expense_type = 16 " . 
			   " and cer_expense_year = " . $cer_basicdata["cer_basicdata_firstyear"];


		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$turnoverrent_firstyear = $row_e["cer_expense_amount"];
		}


		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_project = " . param("pid") . 
			   " and cer_expense_cer_version =  " . $cer_version .  
			   " and cer_expense_type IN (3, 18, 19, 20, 22) " . 
			   " and cer_expense_year = " . $cer_basicdata["cer_basicdata_firstyear"];


		$res_e = mysql_query($sql_e) or dberror($sql_e);
		while ($row_e = mysql_fetch_assoc($res_e))
		{
			$additional_rental_cost_firstyear = $additional_rental_cost_firstyear + $row_e["cer_expense_amount"];
		}
	}

	$fullrent_firstyear_loc = $fixedrent_firstyear + $turnoverrent_firstyear + $additional_rental_cost_firstyear;

	$fullrent_firstyear = $fullrent_firstyear_loc*$cer_basicdata["cer_basicdata_exchangerate"]/$cer_basicdata["cer_basicdata_factor"];
	$fullrent_firstyear = round($fullrent_firstyear/1000, 0);
	$fullrent_firstyear_loc = round($fullrent_firstyear_loc/1000, 0);

	$turnoverrent_firstyear = $turnoverrent_firstyear*$cer_basicdata["cer_basicdata_exchangerate"]/$cer_basicdata["cer_basicdata_factor"];
	$turnoverrent_firstyear = round($turnoverrent_firstyear/1000, 0);


	

	$fullrent_lastyear_loc = $ln_basicdata["ln_basicdata_passedrental"];

	$fullrent_lastyear = $fullrent_lastyear_loc*$cer_basicdata["cer_basicdata_exchangerate"]/$cer_basicdata["cer_basicdata_factor"];
	$fullrent_lastyear = round($fullrent_lastyear/1000, 0);
	$fullrent_lastyear_loc = round($fullrent_lastyear_loc/1000, 0);

	
	
	//Additional Rental Costs
	$poslease = array();
	if($project["pipeline"] == 0)
	{
		$sql_i = "select * from posleases " . 
				 "where poslease_order = " . $project["project_order"] . 
			     " order by poslease_lease_type DESC, poslease_id DESC";
		
	}
	elseif($project["pipeline"] == 1)
	{
		$sql_i = "select * from posleasespipeline " . 
				 "where poslease_order = " . $project["project_order"] . 
			     " order by poslease_lease_type DESC, poslease_id DESC";
	}
	$res_i = mysql_query($sql_i) or dberror($sql_i);
	if ($row_i = mysql_fetch_assoc($res_i))
	{
		$poslease = $row_i;
	}

	if(count($poslease) > 0)
	{
		$negotiated_rental_conditions = $poslease["poslease_negotiated_conditions"];
		$indexrate = $poslease["poslease_indexrate"] . "%";
		$average_yearly_increase = $poslease["poslease_average_increase"] . "%";
		$real_estate_fee = $poslease["poslease_realestate_fee"] . "%";

		$annual_charges = $poslease["poslease_annual_charges"];
		$annual_charges = $annual_charges*$cer_basicdata["cer_basicdata_exchangerate"]/$cer_basicdata["cer_basicdata_factor"];
		


		$other_fees = $poslease["poslease_other_fees"];
		$other_fees = $other_fees*$cer_basicdata["cer_basicdata_exchangerate"]/$cer_basicdata["cer_basicdata_factor"];
		

		$free_weeks = $poslease["poslease_freeweeks"];


		$average_annual_rent = 0;
		$duration_in_years = 0;
		if($poslease["poslease_startdate"] != NULL 
			and $poslease["poslease_startdate"] != '0000-00-00' 
			and $poslease["poslease_enddate"] != NULL
			and $poslease["poslease_enddate"] != '0000-00-00')
			{
				$diff = abs(strtotime($poslease["poslease_enddate"]) - strtotime($poslease["poslease_startdate"]));
				$years = floor($diff / (365*60*60*24));
				$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

				if($months == 12)
				{
					$months = 0;
					$years++;
				}

				$rental_duration = $years . "y-" . $months . "m";

				$duration_in_years = $years + ($months/12);

				$contract_starting_date = to_system_date($poslease["poslease_startdate"]);
				$contract_ending_date = to_system_date($poslease["poslease_enddate"]);
		}
			
		if($duration_in_years > 0)
		{
			$total_lease_commitment = 0;
			$sql_cer = "select * from cer_expenses " .
						  "where cer_expense_project = " . param("pid") .
				          " and cer_expense_cer_version =  " . $cer_version .  
						  " and cer_expense_type IN(2, 3, 16, 18, 19, 20)";


			$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
			while ($row_cer = mysql_fetch_assoc($res_cer))
			{
				$total_lease_commitment = $total_lease_commitment + 1*$row_cer["cer_expense_amount"];
			}

			if($duration_in_years > 0)
			{
				$average_annual_rent = round(($total_lease_commitment / $duration_in_years)/1000, 0);
			}
		}
	}
	else
	{
		$negotiated_rental_conditions = "";
		$indexrate = "";
		$average_yearly_increase = "";
		$real_estate_fee = "";
		$annual_charges = "";
		$other_fees = "";
		$average_annual_rent = "";
		$duration_in_years = "";
		$total_lease_commitment = "";
		$contract_starting_date = "";
		$contract_ending_date = "";
		$free_weeks = "";
	}


	//annual charges and other fees -> new concept for projects starting on April 22, 2014
	if(!$annual_charges)
	{
		//additional rental cost and other fees
		//get additional rental costs
		$annual_charges = 0;

		//first full year
		$sql_e = "select * from cer_expenses " .
				 "where cer_expense_cer_version = " . $cer_version . 
			     "  and cer_expense_project = " . param("pid") . 
				 " and cer_expense_type in (3) " . 
			     " and cer_expense_year = " . $first_full_year;
				 " order by cer_expense_year";

		$res_e = mysql_query($sql_e) or dberror($sql_e);

		while($row_e = mysql_fetch_assoc($res_e))
		{
			$annual_charges = $annual_charges + $row_e["cer_expense_amount"];
		}
	}

	if(!$other_fees)
	{
		//additional rental cost and other fees
		//get other fees
		$other_fees = 0;

		//first full year
		$sql_e = "select * from cer_expenses " .
				 "where cer_expense_cer_version = " . $cer_version . 
			     "  and cer_expense_project = " . param("pid") . 
				 " and cer_expense_type in (18,19) " . 
			     " and cer_expense_year = " . $first_full_year;
				 " order by cer_expense_year";

		$res_e = mysql_query($sql_e) or dberror($sql_e);

		while($row_e = mysql_fetch_assoc($res_e))
		{
			$other_fees = $other_fees + $row_e["cer_expense_amount"];
		}
	}

	$annual_charges = round($annual_charges/1000, 0);
	$other_fees = round($other_fees/1000, 0);

	//get areas and neighbourhood information
	$posareas = "";
	if($project["pipeline"] == 0)
	{
		$sql_i = "select * from posareas " . 
				 "left join posareatypes on posareatype_id = posarea_area " .
				 "where posarea_posaddress = " . $project["posaddress_id"];
		
	}
	elseif($project["pipeline"] == 1)
	{
		
		$sql_p = "select posorder_posaddress from posorderspipeline where posorder_order = " . $project["order_id"];
		$res_p = mysql_query($sql_p) or dberror($sql_p);

		if ($row_p = mysql_fetch_assoc($res_p))
		{
			$sql_i = "select * from posareaspipeline " . 
					 "left join posareatypes on posareatype_id = posarea_area " .
					 "where posarea_posaddress = " . $row_p["posorder_posaddress"];
		}
	}
	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{
		$posareas .= $row_i["posareatype_name"] . ", ";
	}
	$posareas = substr($posareas,0,strlen($posareas)-2);


	//neighbourhood
	$neighbourhoods = array();
	if($project["pipeline"] == 0)
	{
		$sql_e = "select * from posorders where posorder_order = " . $project["order_id"];
	}
	elseif($project["pipeline"] == 1)
	{
		$sql_e = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
	}
	
	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$tmp = "";
		$bt = get_business_type($row_e["posorder_neighbour_left_business_type"]);
		$pr = get_price_range($row_e["posorder_neighbour_left_price_range"]);
		if($bt or $pr)
		{
			$tmp = " (". $bt . " " . $pr . ")";
		}
		$neighbourhoods["Shop on left side"] = $row_e["posorder_neighbour_left"] . $tmp;

		$tmp = "";
		$bt = get_business_type($row_e["posorder_neighbour_right_business_type"]);
		$pr = get_price_range($row_e["posorder_neighbour_right_price_range"]);
		if($bt or $pr)
		{
			$tmp = " (". $bt . " " . $pr . ")";
		}
		$neighbourhoods["Shop on right side"] = $row_e["posorder_neighbour_right"] . $tmp;

		$tmp = "";
		$bt = get_business_type($row_e["posorder_neighbour_acrleft_business_type"]);
		$pr = get_price_range($row_e["posorder_neighbour_acrleft_price_range"]);
		if($bt or $pr)
		{
			$tmp = " (". $bt . " " . $pr . ")";
		}
		$neighbourhoods["Shop across left side"] = $row_e["posorder_neighbour_acrleft"] . $tmp;

		$tmp = "";
		$bt = get_business_type($row_e["posorder_neighbour_acrright_business_type"]);
		$pr = get_price_range($row_e["posorder_neighbour_acrright_price_range"]);
		if($bt or $pr)
		{
			$tmp = " (". $bt . " " . $pr . ")";
		}
		$neighbourhoods["Shop across right side"] = $row_e["posorder_neighbour_acrright"] . $tmp;
		
		$neighbourhoods["Other brands in area"] = $row_e["posorder_neighbour_brands"];
	}		



	$ln_brands = $row["ln_basicdata_brands"];
	$ln_size_remarks = $row["ln_basicdata_size_remarks"];
	$ln_area = $row["ln_basicdata_area"];
	$ln_rent = $row["ln_basicdata_rent"];
	$ln_availability = $row["ln_basicdata_availability"];

	

	$pix1 = ".." . $row["ln_basicdata_pix1"];
	$pix2 = ".." . $row["ln_basicdata_pix2"];
	$pix3 = ".." . $row["ln_basicdata_pix3"];

	$floor_plan = ".." . $row["ln_basicdata_floorplan"];
	$location_layout = ".." . $row["ln_basicdata_location_layout"];
	$lease_agreement = ".." . $row["ln_basicdata_draft_aggreement"];


		
	$exchange_rate_factor = $cer_basicdata["cer_basicdata_factor"];
	if(!$cer_basicdata["cer_basicdata_factor"])
	{
		$exchange_rate_factor = 1;
	}

	$exchange_rate = 1;
	if($cer_basicdata["cer_basicdata_exchangerate"] > 0)
	{
		$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
	}



	$surface_of_relocated_pos = "";
	if(($project["project_projectkind"] == 6 or $project["project_projectkind"] == 9)
		and $project["project_relocated_posaddress_id"] > 0) // relocation
	{
		$relocated_pos = get_relocated_pos_info($project["project_relocated_posaddress_id"]);
		//$project_kind .= " of " . $relocated_pos["posaddress_name"];

		//$surface_of_relocated_pos = $relocated_pos["posaddress_store_grosssurface"] . " / " . $relocated_pos["posaddress_store_totalsurface"] . " / " . $relocated_pos["posaddress_store_retailarea"];

		$surface_of_relocated_pos = $relocated_pos["posaddress_store_totalsurface"] . " / " . $relocated_pos["posaddress_store_retailarea"];

	}

	
	
}

$client_currency = get_cer_currency(param("pid"));
$currency_symbol = $client_currency["symbol"];

//get milestones
$date_af_request = "";

$sql = "select project_milestone_milestone, project_milestone_date from project_milestones " . 
       "where project_milestone_milestone in(1) " . 
	   " and project_milestone_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($row["project_milestone_milestone"] == 1)
	{
		if($row["project_milestone_date"] == NULL or $row["project_milestone_date"] == '0000-00-00')
		{
			$date_af_request = 'n.a.';
		}
		else
		{
			$date_af_request = to_system_date($row["project_milestone_date"]);
		}
	}
}

//get financial data
if($use_old_cer_forms_before_2013 == true)
{
	require_once("include/in_financial_data_before2013.php");
}
else
{
	require_once("include/in_financial_data.php");	
}

$requested_amount = round(($investment_total + $intagibles_total + $deposit + $other_noncapitalized_cost) / 1000, 0);
$investment_amount = round($investment_total/1000,0);
$key_money = round($intagibles_total/1000, 0);
$deposit = round($deposit/1000, 0);
$other_cost = round($other_noncapitalized_cost/1000, 0);

$net_present_value = round($net_present_value_retail/1000, 0);
$internal_reate_of_return = "";
if($net_present_value) 
{
	$internal_reate_of_return = round($discounted_cash_flow_retail,2) . "%";
}

if($pay_back_period_retail)
{
	$pay_back_period = round($pay_back_period_retail,2);
}
else
{
	$pay_back_period = "Invest. Period";
}

if($exchange_rate_factor > 0)
{
	$e_rate = $exchange_rate/$exchange_rate_factor;
}
else
{
	$e_rate = $exchange_rate;
}

//get number of months in first and last year
if($project["project_projectkind"] == 3
   and $project["project_planned_takeover_date"] != NULL and $project["project_planned_takeover_date"] != '0000-00-00')
{
	if(substr($project["project_planned_takeover_date"], 5,2) > $cer_basicdata["cer_basicdata_firstmonth"])
	{
		$number_of_months_first_year = 13-(int)substr($project["project_planned_takeover_date"], 5,2);
	}
	else
	{
		//$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];

		if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
		{
			$number_of_months_first_year = $cer_basicdata["cer_basicdata_lastmonth"]-$cer_basicdata["cer_basicdata_firstmonth"] + 1;
		}
		else
		{
			//$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
			if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
			{
				$number_of_months_first_year = $cer_basicdata["cer_basicdata_lastmonth"]-$cer_basicdata["cer_basicdata_firstmonth"] + 1;
			}
			else
			{
				$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
			}
		}
	}
}
elseif($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] != '0000-00-00')
{
	if(substr($project["project_real_opening_date"], 5,2) > $cer_basicdata["cer_basicdata_firstmonth"])
	{
		$number_of_months_first_year = 13-(int)substr($project["project_real_opening_date"], 5,2);
	}
	else
	{
		//$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];

		if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
		{
			$number_of_months_first_year = $cer_basicdata["cer_basicdata_lastmonth"]-$cer_basicdata["cer_basicdata_firstmonth"] + 1;
		}
		else
		{
			//$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
			if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
			{
				$number_of_months_first_year = $cer_basicdata["cer_basicdata_lastmonth"]-$cer_basicdata["cer_basicdata_firstmonth"] + 1;
			}
			else
			{
				$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
			}
		}
	}
}
else
{
	//$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
	if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
	{
		$number_of_months_first_year = $cer_basicdata["cer_basicdata_lastmonth"]-$cer_basicdata["cer_basicdata_firstmonth"] + 1;
	}
	else
	{
		$number_of_months_first_year = 13-$cer_basicdata["cer_basicdata_firstmonth"];
	}
}
$number_of_months_last_year = $cer_basicdata["cer_basicdata_lastmonth"];

if($number_of_months_first_year == 12)
{
	$i = 0;
}
else
{
	$i = 1;
}

$sd01 = "";
$sd02 = "";
$sd03 = "";
$sd04 = "";
if(array_key_exists($i, $years))
{
		$sd01 = number_format(1*$sales_units_watches_values[$years[$i]], 0, ".", "'");
		$sd02 = number_format(round($e_rate*$total_gross_sales_values[$years[$i]]/1000,0), 0, ".", "'");
		$sd03 = number_format(round($e_rate*$operating_income01_values[$years[$i]]/1000,0), 0, ".", "'");
		$sd04 = number_format(round($e_rate*$operating_income02_values[$years[$i]]/1000,0), 0, ".", "'");
		
}

//if business plan period is shorter than 12 months
$first_full_year_exists = true;
if($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
{
	if(array_key_exists($cer_basicdata["cer_basicdata_firstyear"], $sales_units_watches_values))
	{
		$i = 0;
		$sd01 = number_format($sales_units_watches_values[$years[$i]], 0, ".", "'");
		$sd02 = number_format(round($e_rate*$total_gross_sales_values[$years[$i]]/1000,0), 0, ".", "'");
		$sd03 = number_format(round($e_rate*$operating_income01_values[$years[$i]]/1000,0), 0, ".", "'");
		$sd04 = number_format(round($e_rate*$operating_income02_values[$years[$i]]/1000,0), 0, ".", "'");
		$first_full_year_exists = false;
	}
}
	


	//START PDF OUTPUT of DETAIL DATA
	$pdf->AddPage("P", "A4");

	
	$pdf->SetFillColor(224,224,224);

	//Logo
	$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
	if(isset($version_date) and $version_date)
	{
		$pdf->SetFont('arialn','B',12);
		//Move to the right
		$pdf->Cell(40);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title . $version_date,0,0,'R');
		//Line break
		$pdf->SetY(23);
	}
	else
	{
		//arialn bold 15
		$pdf->SetFont('arialn','B',18);
		//Move to the right
		$pdf->Cell(80);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title,0,0,'R');
		//Line break
		$pdf->SetY(23);
	}




	$pdf->setXY(10, $pdf->getY());
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(97,4,"Address Data",1, 0, 'L', 1);
	$pdf->Cell(90,4,"Location Data",1, 0, 'L', 1);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(30,4,"POS Name:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$posname,1, 0, 'L', 0);
	//$pdf->Cell(40,4,"Environment:",1, 0, 'L', 0);
	
	if($placement)
	{
		$pdf->Cell(90,4,$posareas . ' - ' . $placement,1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(90,4,$posareas,1, 0, 'L', 0);
	}
	$pdf->Ln();


	$pdf->Cell(30,4,"Country/City:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$country_name . "/" . $city_name,1, 0, 'L', 0);
	$pdf->Cell(25,4,"Surfaces in sqms:",1, 0, 'L', 0);
	//$pdf->Cell(65,4, "Gross/Total/Sales: " . $gross_surface . " / " . $total_surface . " / " . $sales_surface,1, 0, 'L', 0);
	$pdf->Cell(65,4, "Total/Sales: " . $total_surface . " / " . $sales_surface,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"Franchisee:",1, 0, 'L', 0);	
	$pdf->Cell(67,4,$franchisee_company,1, 0, 'L', 0);
	
	if($surface_of_relocated_pos  != "")
	{
		$pdf->Cell(25,4," -> Relocated:",1, 0, 'L', 0);
		//$pdf->Cell(65,4, "Gross/Total/Sales: " . $surface_of_relocated_pos,1, 0, 'L', 0);
		$pdf->Cell(65,4, "Total/Sales: " . $surface_of_relocated_pos,1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(25,4,"",1, 0, 'L', 0);
		$pdf->Cell(65,4, "",1, 0, 'L', 0);
	}

	$pdf->Ln();


	$pdf->Cell(30,4,"Pos Type:",1, 0, 'L', 0);	
	$pdf->Cell(67,4,$postype,1, 0, 'L', 0);
	$pdf->Cell(25,4,"Project Type:",1, 0, 'L', 0);
	$pdf->Cell(65,4,$project_kind,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"Planned opening:",1, 0, 'L', 0);	
	$pdf->Cell(67,4,$planned_opening_date,1, 0, 'L', 0);
	$pdf->Cell(25,4,"Planned closing:",1, 0, 'L', 0);
	$pdf->Cell(65,4,$planned_closing_date,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();


	
	$x = $pdf->GetX();
	$pdf->Cell(97,17,"",1, 0, 'L', 0);
	$pdf->SetX($x);
	
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(97,7,"Total Investment in fixed assets in KCHF",1, 0, 'L', 1);
	$pdf->SetFont('arialn','',9);
	$pdf->Cell(22.5,7,number_format(round($e_rate*$investment_total/1000,0), 0, ".", "'"),1, 0, 'R', 0);

	$pdf->Ln();
    
	
	foreach($fixed_assets as $key=>$itype)
	{
		if($itype == 1 and array_key_exists($itype, $investment_names))
		{
			$pdf->Cell(97,5," of which - " . $investment_names[$itype] . " in KCHF",0, 0, 'L', 0);
			$pdf->Cell(22.5,5,number_format(round($e_rate*$amounts[$itype] / 1000,0), 0, ".", "'"),1, 0, 'R', 0);
			$pdf->Ln();
		}
		if($itype == 3 and array_key_exists($itype, $investment_names))
		{
			$pdf->Cell(97,5," of which - " . $investment_names[$itype] . " in KCHF",0, 0, 'L', 0);
			$pdf->Cell(22.5,5,number_format(round($e_rate*$amounts[$itype] / 1000,0), 0, ".", "'"),1, 0, 'R', 0);
			$pdf->Ln();
		}
		
	    
	}


	
	

	$pdf->Ln();

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(187,4,"Key points",1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',9);

	$key_points = str_replace("\r\n"," ", $key_points);
	$key_points = str_replace("\n"," ", $key_points);
	$pdf->MultiCell(187,20, $key_points, 1, "T", false, 1, '', '', false, 0, false, false, 20, 'T', false);
	
	
	//signature block

	//title bar 1
	
	$pdf->setXY(10, $pdf->getY()+2);
	$y = $pdf->getY();
	
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetFillColor(224,224,224);
	$pdf->Cell(187,5, "Swatch Group HQ", 1, "", "L", true);

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(50, 12, "Retail Committee". "\r\n" . $approval_name10, 1);
	$pdf->SetXY($x+50,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(65, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(52, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+14;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetFillColor(224,224,224);
	$pdf->Cell(187,5, "Tissot HQ", 1, "", "L", true);

	
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(50, 12, "CFO". "\r\n" . $approval_name7, 1);
	$pdf->SetXY($x+50,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(65, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(52, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	if($approval_name3)
	{
		$y = $y+12;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->MultiCell(50, 12, "VP Sales". "\r\n" . $approval_name3, 1);
		$pdf->SetXY($x+50,$y);
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(65, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(20, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(52, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');
	}


	if($approval_name4)
	{
		$y = $y+12;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->MultiCell(50, 12, "International Retail Manager". "\r\n" . $approval_name4, 1);
		$pdf->SetXY($x+50,$y);
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(65, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(20, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(52, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');
	}

	if($approval_name9)
	{
		$y = $y+12;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "", 8);
		$pdf->MultiCell(50, 12, "Retail Controlling". "\r\n" . $approval_name9, 1);
		$pdf->SetXY($x+50,$y);
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(65, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(20, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
		$pdf->SetFont("freesans", "", 7);
		$pdf->Cell(52, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');
	}


	$y = $y+14;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetFillColor(224,224,224);
	$pdf->Cell(187,5, "Country", 1, "", "L", true);

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(50, 12, "Service CenterManager,\r\nBrand Manager". "\r\n" . $approval_name12, 1);
	$pdf->SetXY($x+50,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(65, 12, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 12, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(52, 12, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+12;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(50, 14, "Franchisee " . "\r\n" . $franchisee_name, 1);
	$pdf->SetXY($x+50,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(65, 14, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(20, 14, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(52, 14, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');
	

?>