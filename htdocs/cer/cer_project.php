<?php
/********************************************************************

    cer_project.php

    Project Data

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";


check_access("has_access_to_cer");


//check if basic data exists for this project
if(param("pid") > 0)
{
	$result = check_cer_summary(param("pid"));
	$result = check_basic_data(param("pid"));
}

require "include/get_project.php";
require_once "../shared/func_posindex.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
$new_cer_version = 0;
$client_address = get_address($project["order_client_address"]);


$client = $client_address["company"] . ", " .
		  $client_address["zip"] . " " .
          $client_address["place"] . ", " .
          $client_address["country_name"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_address"] . ", " .
		$project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];

$ln_basicdata = get_ln_basicdata(param("pid"));



$ln_submitted_by = get_user($ln_basicdata["ln_basicdata_submitted_by"]);
$ln_resubmitted_by = get_user($ln_basicdata["ln_basicdata_resubmitted_by"]);
$ln_rejected_by = get_user($ln_basicdata["ln_basicdata_rejected_by"]);

$cer_submitted_by = get_user($cer_basicdata["cer_basicdata_submitted_by"]);
$cer_resubmitted_by = get_user($cer_basicdata["cer_basicdata_resubmitted_by"]);
$cer_rejected_by = get_user($cer_basicdata["cer_basicdata_rejected_by"]);




//add approval names
if($project["order_actual_order_state_code"] < '890' 
	and ($project["order_archive_date"] == NULL or $project["order_archive_date"] == '0000-00-00'))
{
	if($cer_basicdata["cer_basicdata_approvalname1"] == '' or $cer_basicdata["cer_basicdata_approvalname10"] == '' or $cer_basicdata["cer_basicdata_approvalname3"] == '')
	{
		
		$sql = "select * from cer_approvalnames where cer_approvalname_id = 1";
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$fields = array();

			$value = dbquote($row["cer_approvalname_name1"]);
			$fields[] = "cer_basicdata_approvalname1 = " . $value;

			$value = dbquote($row["cer_approvalname_name2"]);
			$fields[] = "cer_basicdata_approvalname2 = " . $value;

			$value = dbquote($row["cer_approvalname_name3"]);
			$fields[] = "cer_basicdata_approvalname3 = " . $value;

			$value = dbquote($row["cer_approvalname_name4"]);
			$fields[] = "cer_basicdata_approvalname4 = " . $value;

			$value = dbquote($row["cer_approvalname_name5"]);
			$fields[] = "cer_basicdata_approvalname5 = " . $value;

			$value = dbquote($row["cer_approvalname_name6"]);
			$fields[] = "cer_basicdata_approvalname6 = " . $value;

			$value = dbquote($row["cer_approvalname_name7"]);
			$fields[] = "cer_basicdata_approvalname7 = " . $value;

			$value = dbquote($row["cer_approvalname_name8"]);
			$fields[] = "cer_basicdata_approvalname8 = " . $value;

			$value = dbquote($row["cer_approvalname_name9"]);
			$fields[] = "cer_basicdata_approvalname9 = " . $value;

			$value = dbquote($row["cer_approvalname_name10"]);
			$fields[] = "cer_basicdata_approvalname10 = " . $value;

			$value = dbquote($row["cer_approvalname_name11"]);
			$fields[] = "cer_basicdata_approvalname11 = " . $value;

			$sql = "update cer_basicdata set " . join(", ", $fields) . 
				" where cer_basicdata_project = " . param("pid") . 
				" and cer_basicdata_version = 0 ";
			mysql_query($sql) or dberror($sql);
		}
		
	}

	if($cer_basicdata["cer_basicdata_approvalname11"] == '')
	{
		
		$sql = "select * from cer_approvalnames where cer_approvalname_id = 1";
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$fields = array();

			
			$value = dbquote($row["cer_approvalname_name11"]);
			$fields[] = "cer_basicdata_approvalname11 = " . $value;

			$sql = "update cer_basicdata set " . join(", ", $fields) . 
				" where cer_basicdata_project = " . param("pid") . 
				" and cer_basicdata_version = 0 ";
			mysql_query($sql) or dberror($sql);
		}
		
	}
}

$posdata = get_pos_data($project["project_order"]);
$posleases = get_pos_leasedata($posdata["posorder_posaddress"], $posdata["posorder_order"]);


include("include/check_if_submission_is_possible.php");


/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_basicdata", "cer_basicdata");

$form->add_hidden("pid", param("pid"));
include("include/project_head.php");


if(has_access("has_access_to_his_cer") 
	or has_access("cer_has_full_access_to_his_projects")
	or has_access("has_full_access_to_cer"))
{
	if($form_type == 'CER') //corporate
	{
		if($ln_basicdata['ln_basicdata_locked'] != 1
		and $ln_basicdata['ln_no_ln_submission_needed'] != 1
		and $cer_submission_possible == 0) {
		
			
			//ln submission
			if($ln_ok == false and $ln_basicdata['ln_no_ln_submission_needed'] != 1) {
				$form->add_section("LNR Submission");
				$form->add_comment('<span class="error">LNR submission is not possible due to incomplete data!</span>');
			}
			elseif($ln_submission_possible == 1)
			{
				
				$can_be_submitted = true;
				if($ln_basicdata["ln_basicdata_submitted"] != NULL 
					and substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) != "0000")
				{
					$button_Text= "Resubmit LNR";
					
					
					if(!$ln_basicdata["ln_basicdata_rejected"] 
					or $ln_basicdata["ln_basicdata_rejected"] == NULL 
					or substr($ln_basicdata["ln_basicdata_rejected"], 0, 4) == '0000')
					{
						$can_be_submitted = false;
					}
					elseif($ln_basicdata["ln_basicdata_resubmitted"] > $ln_basicdata["ln_basicdata_rejected"])
					{
						$can_be_submitted = false;
					}
					
				}
				else
				{
					$button_Text = "Submit LNR";
				}
				
				
				$ln_can_be_submitted = true;
				if($ln_basicdata["ln_basicdata_submitted"] != NULL 
					and substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) != "0000")
				{
					$button_Text = "Resubmit LNR";

					if(!$ln_basicdata["ln_basicdata_rejected"] 
					or $ln_basicdata["ln_basicdata_rejected"] == NULL 
					or substr($ln_basicdata["ln_basicdata_rejected"], 0, 4) == '0000')
					{
						$ln_can_be_submitted = false;
					}
					elseif($ln_basicdata["ln_basicdata_resubmitted"] > $ln_basicdata["ln_basicdata_rejected"])
					{
						$ln_can_be_submitted = false;
					}
					
				}
				else
				{
					
					$button_Text = "Submit LNR";
				}

				
				if(array_key_exists('s', $_GET) and $_GET['s'] == 1)
				{
					$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" />Your request was submitted successfully.";
				}
				else
				{
					
					$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"#\" onClick=\"button('submit_ln');$(this).hide();\">" .  $button_Text . "</a>";
				}
				
				if($ln_can_be_submitted == true 
					and $project["order_actual_order_state_code"] < '890' 
					and $ln_basicdata["ln_basicdata_locked"] == 0)
				{
					$form->add_section("LNR Submission");
					$form->add_label("submit_ln", "Submission", RENDER_HTML, $link);
				}
			}

			//ln rejection
			if($ln_submission_possible == 1
			   and $ln_basicdata["ln_basicdata_submitted"] != NULL 
			   and substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) != '0000'
			   and $ln_basicdata['ln_basicdata_locked'] != 1
			   and $ln_basicdata['ln_no_ln_submission_needed'] != 1)
			{

				if(has_access("can_reject_submissions"))
				{

					if($ln_basicdata["ln_basicdata_resubmitted"] 
						and $ln_basicdata["ln_basicdata_rejected"] > $ln_basicdata["ln_basicdata_resubmitted"])
					{

					}
					elseif(!$ln_basicdata["ln_basicdata_resubmitted"] 
						and $ln_basicdata["ln_basicdata_rejected"] > $ln_basicdata["ln_basicdata_submitted"])
					{
					
					}
					else
					{
						$form->add_section("Rejection");
						$link = "createWindowWithRemotingUrl2('Reject LNR', 'reject_submission.php?pid=" . param("pid") . "&type=ln');";

						$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"javascript:void(0);\" onClick=\"" . $link . ";\">Reject LNR</a>";

						$form->add_label("reject_ln", "Submission", RENDER_HTML, $link);
					}
				}
			}
		}
		elseif($cer_basicdata['cer_basicdata_cer_locked'] != 1
		and $cer_basicdata['cer_basicdata_no_cer_submission_needed'] != 1) {
			
			if($ln_was_approved == 0 ) {
				$form->add_section("CER Submission");
				$form->add_comment('<span class="error">CER submission is not possible since the LNR has not been approved!</span>');
			}
			elseif($budget_was_approved == false ) {
				$form->add_section("CER Submission");
				$form->add_comment('<span class="error">CER submission is not possible since the budget has not been approved!</span>');
			}
			elseif($cer_submission_possible == 0) {
				$form->add_section("CER Submission");
				$form->add_comment('<span class="error">CER submission is not possible due to missing information!</span>');
			}
			elseif($cer_basicdata['cer_basicdata_cer_locked'] != 1
				and $cer_basicdata['cer_basicdata_no_cer_submission_needed'] != 1) {

				
				$can_be_submitted = true;
				if($cer_basicdata["cer_basicdata_submitted"] != NULL 
					and substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) != "0000")
				{
					$button_Text= "Resubmit Capital Expenditure Request";
										
					if(!$cer_basicdata["cer_basicdata_rejected"] 
					or $cer_basicdata["cer_basicdata_rejected"] == NULL 
					or substr($cer_basicdata["cer_basicdata_rejected"], 0, 4) == '0000')
					{
						$can_be_submitted = false;
					}
					elseif($cer_basicdata["cer_basicdata_resubmitted"] > $cer_basicdata["cer_basicdata_rejected"])
					{
						$can_be_submitted = false;
					}
				}
				elseif($ln_ok == true and $ln_ok1 == true)
				{
					$button_Text = "Submit Capital Expenditure Request";
				}


				if($can_be_submitted == true 
					and $cer_basicdata["cer_basicdata_no_cer_submission_needed"] != 1) //submission is necessary
				{
					
					if($project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0) {
					
						if($ln_ok == true and $ln_ok1 == true)
						{
							$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"#\" onClick=\"button('submit');$(this).hide();\">" . $button_Text ."</a>";
						}
						elseif($ln_ok1 == false)
						{
							$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><span class=\"error\">" . $button_Text . "</span>";
						}
						else
						{
							$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><span class=\"error\">" . $button_Text . "</span>";
						}
						
						if($sellout_necessary == 1 and $sellout_data_ok == true)
						{
							$form->add_section("CER Submission");
							$form->add_label("submit", "Submission", RENDER_HTML, $link);
						}
						elseif($sellout_necessary == 0)
						{
							$form->add_section("CER Submission");
							$form->add_label("submit", "Submission", RENDER_HTML, $link);
						}
					}
				}
			}



			//cer rejection
			if( $cer_basicdata["cer_basicdata_submitted"] != NULL 
				and substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) != '0000'
				and $cer_basicdata["cer_basicdata_cer_locked"] != 1
				and $cer_basicdata['cer_basicdata_no_cer_submission_needed'] != 1)
				
			{
				
				if(has_access("can_reject_submissions"))
				{

					if($cer_basicdata["cer_basicdata_resubmitted"] 
						and $cer_basicdata["cer_basicdata_rejected"] > $cer_basicdata["cer_basicdata_resubmitted"]) {

					}
					elseif(!$cer_basicdata["cer_basicdata_resubmitted"] 
						and $cer_basicdata["cer_basicdata_rejected"] > $cer_basicdata["cer_basicdata_submitted"]) {
					}
					else
					{
						$form->add_section("Rejection");
						$link = "createWindowWithRemotingUrl2('Reject CER', 'reject_submission.php?pid=" . param("pid") . "&type=cer');";

						$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"javascript:void(0);\" onClick=\"" . $link . ";\">Reject CER</a>";

						$form->add_label("reject_ln", "Submission", RENDER_HTML, $link);
					}
				}
			}
		}
	}
	elseif($form_type == 'AF') {
		
		if($ln_basicdata['ln_basicdata_locked'] != 1
		and $ln_basicdata['ln_no_ln_submission_needed'] != 1
		and $cer_submission_possible == 0) {
		
			
			//ln submission
			if($ln_ok == false and $ln_basicdata['ln_no_ln_submission_needed'] != 1) {
				$form->add_section("LNR Submission");
				$form->add_comment('<span class="error">LNR submission is not possible due to incomplete data!</span>');
			}
			elseif($ln_submission_possible == 1)
			{
				
				$can_be_submitted = true;
				if($ln_basicdata["ln_basicdata_submitted"] != NULL 
					and substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) != "0000")
				{
					$button_Text= "Resubmit LNR";
					
					
					if(!$ln_basicdata["ln_basicdata_rejected"] 
					or $ln_basicdata["ln_basicdata_rejected"] == NULL 
					or substr($ln_basicdata["ln_basicdata_rejected"], 0, 4) == '0000')
					{
						$can_be_submitted = false;
					}
					elseif($ln_basicdata["ln_basicdata_resubmitted"] > $ln_basicdata["ln_basicdata_rejected"])
					{
						$can_be_submitted = false;
					}
					
				}
				else
				{
					$button_Text = "Submit LNR";
				}
				
				
				$ln_can_be_submitted = true;
				if($ln_basicdata["ln_basicdata_submitted"] != NULL 
					and substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) != "0000")
				{
					$button_Text = "Resubmit LNR";

					if(!$ln_basicdata["ln_basicdata_rejected"] 
					or $ln_basicdata["ln_basicdata_rejected"] == NULL 
					or substr($ln_basicdata["ln_basicdata_rejected"], 0, 4) == '0000')
					{
						$ln_can_be_submitted = false;
					}
					elseif($ln_basicdata["ln_basicdata_resubmitted"] > $ln_basicdata["ln_basicdata_rejected"])
					{
						$ln_can_be_submitted = false;
					}
					
				}
				else
				{
					
					$button_Text = "Submit LNR";
				}

				
				if(array_key_exists('s', $_GET) and $_GET['s'] == 1)
				{
					$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" />Your request was submitted successfully.";
				}
				else
				{
					
					$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"#\" onClick=\"button('submit_ln');$(this).hide();\">" .  $button_Text . "</a>";
				}
				
				if($ln_can_be_submitted == true 
					and $project["order_actual_order_state_code"] < '890' 
					and $ln_basicdata["ln_basicdata_locked"] == 0)
				{
					$form->add_section("LNR Submission");
					$form->add_label("submit_ln", "Submission", RENDER_HTML, $link);
				}
			}

			//ln rejection
			if($ln_submission_possible == 1
			   and $ln_basicdata["ln_basicdata_submitted"] != NULL 
			   and substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) != '0000'
			   and $ln_basicdata['ln_basicdata_locked'] != 1
			   and $ln_basicdata['ln_no_ln_submission_needed'] != 1)
			{

				if(has_access("can_reject_submissions"))
				{

					if($ln_basicdata["ln_basicdata_resubmitted"] 
						and $ln_basicdata["ln_basicdata_rejected"] > $ln_basicdata["ln_basicdata_resubmitted"])
					{

					}
					elseif(!$ln_basicdata["ln_basicdata_resubmitted"] 
						and $ln_basicdata["ln_basicdata_rejected"] > $ln_basicdata["ln_basicdata_submitted"])
					{
					
					}
					else
					{
						$form->add_section("Rejection");
						$link = "createWindowWithRemotingUrl2('Reject LNR', 'reject_submission.php?pid=" . param("pid") . "&type=ln');";

						$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"javascript:void(0);\" onClick=\"" . $link . ";\">Reject LNR</a>";

						$form->add_label("reject_ln", "Submission", RENDER_HTML, $link);
					}
				}
			}
		}
		elseif($cer_basicdata['cer_basicdata_cer_locked'] != 1
		and $cer_basicdata['cer_basicdata_no_cer_submission_needed'] != 1) {
			
			
			if($project["project_postype"] == 1) {
				if($ln_was_approved == 0 ) {
					$form->add_section("CER Submission");
					$form->add_comment('<span class="error">CER submission is not possible since the LNR has not been approved!</span>');
				}
				elseif($budget_was_approved == false ) {
					$form->add_section("CER Submission");
					$form->add_comment('<span class="error">CER submission is not possible since the budget has not been approved!</span>');
				}
				elseif($cer_submission_possible == 0) {
					$form->add_section("CER Submission");
					$form->add_comment('<span class="error">CER submission is not possible due to missing information!</span>');
				}
				elseif($cer_basicdata['cer_basicdata_cer_locked'] != 1
					and $cer_basicdata['cer_basicdata_no_cer_submission_needed'] != 1) {

					
					$can_be_submitted = true;
					if($cer_basicdata["cer_basicdata_submitted"] != NULL 
						and substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) != "0000")
					{
						$button_Text= "Resubmit Capital Expenditure Request";
											
						if(!$cer_basicdata["cer_basicdata_rejected"] 
						or $cer_basicdata["cer_basicdata_rejected"] == NULL 
						or substr($cer_basicdata["cer_basicdata_rejected"], 0, 4) == '0000')
						{
							$can_be_submitted = false;
						}
						elseif($cer_basicdata["cer_basicdata_resubmitted"] > $cer_basicdata["cer_basicdata_rejected"])
						{
							$can_be_submitted = false;
						}
					}
					elseif($ln_ok == true and $ln_ok1 == true)
					{
						$button_Text = "Submit Capital Expenditure Request";
					}


					if($can_be_submitted == true 
						and $cer_basicdata["cer_basicdata_no_cer_submission_needed"] != 1) //submission is necessary
					{
						
						if($project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0) {
						
							if($ln_ok == true and $ln_ok1 == true)
							{
								$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"#\" onClick=\"button('submit');$(this).hide();\">" . $button_Text ."</a>";
							}
							elseif($ln_ok1 == false)
							{
								$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><span class=\"error\">" . $button_Text . "</span>";
							}
							else
							{
								$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><span class=\"error\">" . $button_Text . "</span>";
							}
							
							if($sellout_necessary == 1 and $sellout_data_ok == true)
							{
								$form->add_section("CER Submission");
								$form->add_label("submit", "Submission", RENDER_HTML, $link);
							}
							elseif($sellout_necessary == 0)
							{
								$form->add_section("CER Submission");
								$form->add_label("submit", "Submission", RENDER_HTML, $link);
							}
						}
					}
				}

				//cer rejection
				if( $cer_basicdata["cer_basicdata_submitted"] != NULL 
					and substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) != '0000'
					and $cer_basicdata["cer_basicdata_cer_locked"] != 1
					and $cer_basicdata['cer_basicdata_no_cer_submission_needed'] != 1)
					
				{
					
					if(has_access("can_reject_submissions"))
					{

						if($cer_basicdata["cer_basicdata_resubmitted"] 
							and $cer_basicdata["cer_basicdata_rejected"] > $cer_basicdata["cer_basicdata_resubmitted"]) {

						}
						elseif(!$cer_basicdata["cer_basicdata_resubmitted"] 
							and $cer_basicdata["cer_basicdata_rejected"] > $cer_basicdata["cer_basicdata_submitted"]) {
						}
						else
						{
							$form->add_section("Rejection");
							$link = "createWindowWithRemotingUrl2('Reject CER', 'reject_submission.php?pid=" . param("pid") . "&type=cer');";

							$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"javascript:void(0);\" onClick=\"" . $link . ";\">Reject CER</a>";

							$form->add_label("reject_ln", "Submission", RENDER_HTML, $link);
						}
					}
				}
			}
			else {
			
			
				if($ln_was_approved == 0 ) {
					$form->add_section("INR03 Submission");
					$form->add_comment('<span class="error">INR03 submission is not possible since the LNR has not been approved!</span>');
				}
				elseif($budget_was_approved == false ) {
					$form->add_section("INR03 Submission");
					$form->add_comment('<span class="error">INR03 submission is not possible since the budget has not been approved!</span>');
				}
				elseif($cer_submission_possible == 0) {
					$form->add_section("INR03 Submission");
					$form->add_comment('<span class="error">INR03 submission is not possible due to missing information!</span>');
				}
				elseif($cer_basicdata['cer_basicdata_cer_locked'] != 1
					and $cer_basicdata['cer_basicdata_no_cer_submission_needed'] != 1) {

					
					$can_be_submitted = true;
					if($cer_basicdata["cer_basicdata_submitted"] != NULL 
						and substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) != "0000")
					{
						$button_Text= "Resubmit INR03";
											
						if(!$cer_basicdata["cer_basicdata_rejected"] 
						or $cer_basicdata["cer_basicdata_rejected"] == NULL 
						or substr($cer_basicdata["cer_basicdata_rejected"], 0, 4) == '0000')
						{
							$can_be_submitted = false;
						}
						elseif($cer_basicdata["cer_basicdata_resubmitted"] > $cer_basicdata["cer_basicdata_rejected"])
						{
							$can_be_submitted = false;
						}
					}
					elseif($ln_ok == true and $ln_ok1 == true)
					{
						$button_Text = "Submit INR03";
					}


					if($can_be_submitted == true 
						and $cer_basicdata["cer_basicdata_no_cer_submission_needed"] != 1) //submission is necessary
					{
						
						if($project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0) {
						
							if($ln_ok == true and $ln_ok1 == true)
							{
								$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"#\" onClick=\"button('submit');$(this).hide();\">" . $button_Text ."</a>";
							}
							elseif($ln_ok1 == false)
							{
								$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><span class=\"error\">" . $button_Text . "</span>";
							}
							else
							{
								$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><span class=\"error\">" . $button_Text . "</span>";
							}
							
							if($sellout_necessary == 1 and $sellout_data_ok == true)
							{
								$form->add_section("INR03 Submission");
								$form->add_label("submit", "Submission", RENDER_HTML, $link);
							}
							elseif($sellout_necessary == 0)
							{
								$form->add_section("INR03 Submission");
								$form->add_label("submit", "Submission", RENDER_HTML, $link);
							}
						}
					}
				}

				//INR03 rejection
				if( $cer_basicdata["cer_basicdata_submitted"] != NULL 
					and substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) != '0000'
					and $cer_basicdata["cer_basicdata_cer_locked"] != 1
					and $cer_basicdata['cer_basicdata_no_cer_submission_needed'] != 1)
					
				{
					
					if(has_access("can_reject_submissions"))
					{

						if($cer_basicdata["cer_basicdata_resubmitted"] 
							and $cer_basicdata["cer_basicdata_rejected"] > $cer_basicdata["cer_basicdata_resubmitted"]) {

						}
						elseif(!$cer_basicdata["cer_basicdata_resubmitted"] 
							and $cer_basicdata["cer_basicdata_rejected"] > $cer_basicdata["cer_basicdata_submitted"]) {
						}
						else
						{
							$form->add_section("Rejection");
							$link = "createWindowWithRemotingUrl2('Reject INR03', 'reject_submission.php?pid=" . param("pid") . "&type=inr03');";

							$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"javascript:void(0);\" onClick=\"" . $link . ";\">Reject INR03</a>";

							$form->add_label("reject_ln", "Submission", RENDER_HTML, $link);
						}
					}
				}
			}
		}
	}
	elseif($form_type == 'INR03'
		and $cer_basicdata['cer_basicdata_cer_locked'] != 1
		and $cer_basicdata['cer_basicdata_no_cer_submission_needed'] != 1) {
			
		
		//inr03 submission
		if($ln_ok == false ) {
			$form->add_section("Submission");
			$form->add_comment('<span class="error">INR03 submission is not possible due to incomplete data!</span>');
		}
		elseif($budget_was_approved == false ) {
			$form->add_section("Submission");
			$form->add_comment('<span class="error">INR03 submission is not possible since the budget has not been approved!</span>');
		}
		else
		{
			$can_be_submitted = true;
			if($cer_basicdata["cer_basicdata_submitted"] != NULL 
				and substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) != "0000")
			{
				
				$button_Text= "Resubmit INR03 form";
				
				
				if(!$cer_basicdata["cer_basicdata_rejected"] 
				or $cer_basicdata["cer_basicdata_rejected"] == NULL 
				or substr($cer_basicdata["cer_basicdata_rejected"], 0, 4) == '0000')
				{
					$can_be_submitted = false;
				}
				elseif($cer_basicdata["cer_basicdata_resubmitted"] > $cer_basicdata["cer_basicdata_rejected"])
				{
					$can_be_submitted = false;
				}
			}
			else
			{
				$button_Text = "Submit INR03 Form";
			}
			

			if($can_be_submitted == true 
				and $cer_basicdata["cer_basicdata_no_cer_submission_needed"] != 1) //submission is necessary
			{
				
				$form->add_section("Submission");
				if($project["order_actual_order_state_code"] < '890' 
					and $cer_basicdata["cer_basicdata_cer_locked"] == 0) {
				
					if($ln_ok == true and $ln_ok1 == true)
					{
						$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"#\" onClick=\"button('submit');$(this).hide();\">" . $button_Text ."</a>";
					}
					elseif($ln_ok1 == false)
					{
						$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><span class=\"error\">" . $button_Text . "</span>";
					}
					else
					{
						$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><span class=\"error\">" . $button_Text . "</span>";
					}
					
					if($sellout_necessary == 1 and $sellout_data_ok == true)
					{
						$form->add_label("submit", "Submission", RENDER_HTML, $link);
					}
					elseif($sellout_necessary == 0)
					{
						$form->add_label("submit", "Submission", RENDER_HTML, $link);
					}
				}
			}

			//INR03 rejection
			if($can_be_submitted == false 
				and $cer_basicdata["cer_basicdata_submitted"] != NULL 
				and substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) != '0000'
			    and $cer_basicdata['cer_basicdata_cer_locked'] != 1)
			{
				if(has_access("can_reject_submissions"))
				{

					if($cer_basicdata["cer_basicdata_resubmitted"] 
						and $cer_basicdata["cer_basicdata_rejected"] > $cer_basicdata["cer_basicdata_resubmitted"])
					{

					}
					elseif(!$cer_basicdata["cer_basicdata_resubmitted"] 
						and $cer_basicdata["cer_basicdata_rejected"] > $cer_basicdata["cer_basicdata_submitted"])
					{
						
					}
					else
					{
						$form->add_section("Rejection");
						$link = "createWindowWithRemotingUrl2('Reject INR03 Form', 'reject_submission.php?pid=" . param("pid") . "&type=inr03');";

						$link = "<img style=\"vertical-align:-10%;padding-right:4px;\" src=\"../pictures/wf_right_blue.gif\" /><a href=\"javascript:void(0);\" onClick=\"" . $link . ";\">Reject INR03</a>";

						$form->add_label("reject_ln", "Submission", RENDER_HTML, $link);
						
					}
				}
			}
			
		}
	}
}

//status report
$form->add_section("Business Plan");
foreach($bp_captions as $key=>$caption) {
	if(array_key_exists($key, $business_plan_errors)) {
		
		$tmp = '';
		foreach($business_plan_errors[$key] as $key2=>$info_id) {
			$tmp .= "<table style=\"white-space: normal;\" width=\"400\"><tr><td width=\"20\" valign=\"top\">" . $not_ok . "</td><td valign=\"top\">" . $bp_errorlines[$info_id] . "</td></tr></table>";
		}
		$form->add_label("bpe" . $key, $caption, RENDER_HTML, $tmp);
	}
	else {
		$form->add_label("bpe" . $key, $caption, RENDER_HTML, $ok);
	}
}


if($form_type == 'CER' 
	and $ln_basicdata['ln_no_ln_submission_needed'] != 1) {
	$form->add_section("LNR");

	if(has_access("can_edit_ln_workflow_data")) {
		$form->add_checkbox("ln_no_ln_submission_needed", "No LNR submission needed", $ln_basicdata["ln_no_ln_submission_needed"], 0, "LNR Submission");
	}
	else
	{
		$form->add_hidden("ln_no_ln_submission_needed", $ln_basicdata["ln_no_ln_submission_needed"]);
	}

	if($ln_basicdata['ln_no_ln_submission_needed'] != 1) {
		$by = "";
		if($ln_submitted_by["name"])
		{
			$by = " by " . $ln_submitted_by["name"] . " " . $ln_submitted_by["firstname"];
		}

		$form->add_label("date",  "Submission Date", 0,  to_system_date($ln_basicdata["ln_basicdata_submitted"]) . $by);

		$by = "";
		if($ln_resubmitted_by["name"])
		{
			$by = " by " . $ln_resubmitted_by["name"] . " " . $ln_resubmitted_by["firstname"];
		}

		$form->add_label("date1",  "Date of latest Resubmission", 0,  to_system_date($ln_basicdata["ln_basicdata_resubmitted"]). $by);

		$by = "";
		if($ln_rejected_by["name"])
		{
			$by = " by " . $ln_rejected_by["name"] . " " . $ln_rejected_by["firstname"];
		}

		$form->add_label("date2",  "Date of Rejection", 0,  to_system_date($ln_basicdata["ln_basicdata_rejected"]). $by);

	}

}
elseif($form_type == 'CER' 
	and $ln_basicdata['ln_no_ln_submission_needed'] == 1
	and has_access("can_edit_ln_workflow_data")) {
	$form->add_section("LNR");
	$form->add_checkbox("ln_no_ln_submission_needed", "No LNR submission needed", $ln_basicdata["ln_no_ln_submission_needed"], 0, "LNR Submission");

}
elseif($form_type == 'AF') {
	$form->add_section("LNR");
	
	if(has_access("can_edit_ln_workflow_data")) {
		$form->add_checkbox("ln_no_ln_submission_needed", "No LNR submission needed", $ln_basicdata["ln_no_ln_submission_needed"], 0, "LNR Submission");
	}
	else
	{
		$form->add_hidden("ln_no_ln_submission_needed", $ln_basicdata["ln_no_ln_submission_needed"]);
	}

	
	$by = "";
	if($ln_submitted_by["name"])
	{
		$by = " by " . $ln_submitted_by["name"] . " " . $ln_submitted_by["firstname"];
	}

	$form->add_label("date",  "Submission Date", 0,  to_system_date($ln_basicdata["ln_basicdata_submitted"]) . $by);

	$by = "";
	if($ln_resubmitted_by["name"])
	{
		$by = " by " . $ln_resubmitted_by["name"] . " " . $ln_resubmitted_by["firstname"];
	}

	$form->add_label("date1",  "Date of latest Resubmission", 0,  to_system_date($ln_basicdata["ln_basicdata_resubmitted"]). $by);

	$by = "";
	if($ln_rejected_by["name"])
	{
		$by = " by " . $ln_rejected_by["name"] . " " . $ln_rejected_by["firstname"];
	}

	$form->add_label("date2",  "Date of Rejection", 0,  to_system_date($ln_basicdata["ln_basicdata_rejected"]). $by);
}
elseif($form_type == 'INR03') {
	$form->add_section("INR03 Form");
	$form->add_hidden("ln_no_ln_submission_needed");
	
	
	$by = "";
	if($cer_submitted_by["name"])
	{
		$by = " by " . $cer_submitted_by["name"] . " " . $cer_submitted_by["firstname"];
	}

	if(has_access("can_edit_cer_workflow_data")) {
			$form->add_checkbox("cer_basicdata_no_cer_submission_needed", "No INR03 submission needed", $cer_basicdata["cer_basicdata_no_cer_submission_needed"], 0, "INR03 Submission");
	}
	else
	{
		$form->add_hidden("cer_basicdata_no_cer_submission_needed");
	}

	$form->add_label("date1", "Date of Submission", 0, to_system_date($cer_basicdata["cer_basicdata_submitted"]) . $by);

	$by = "";
	if($cer_resubmitted_by["name"])
	{
		$by = " by " . $cer_resubmitted_by["name"] . " " . $cer_resubmitted_by["firstname"];
	}
	$form->add_label("date2", "Date of latest Resubmission", 0, to_system_date($cer_basicdata["cer_basicdata_resubmitted"]) . $by);

	$by = "";
	if($cer_rejected_by["name"])
	{
		$by = " by " . $cer_rejected_by["name"] . " " . $cer_rejected_by["firstname"];
	}

	$form->add_label("date3", "Date of Rejection", 0, to_system_date($cer_basicdata["cer_basicdata_rejected"]) . $by);


}

//status report LN/AF/INR03
if($form_type == 'CER'
	and $ln_basicdata['ln_no_ln_submission_needed'] != 1) {
	foreach($ln_captions as $key=>$caption) {
		if(array_key_exists($key, $ln_errors)) {
			
			$tmp = '';
			foreach($ln_errors[$key] as $key2=>$info_id) {
				$tmp .= "<table style=\"white-space: normal;\" width=\"400\"><tr><td width=\"20\" valign=\"top\">" . $not_ok . "</td><td valign=\"top\">" . $ln_errorlines[$info_id] . "</td></tr></table>";
			}
			$form->add_label("le" . $key, $caption, RENDER_HTML, $tmp);
		}
		else {
			$form->add_label("le" . $key, $caption, RENDER_HTML, $ok);
		}
	}
}
elseif($form_type == 'AF'
	and $ln_basicdata['ln_no_ln_submission_needed'] != 1) {
	foreach($ln_captions as $key=>$caption) {
		if(array_key_exists($key, $ln_errors)) {
			
			$tmp = '';
			foreach($ln_errors[$key] as $key2=>$info_id) {
				$tmp .= "<table style=\"white-space: normal;\" width=\"400\"><tr><td width=\"20\" valign=\"top\">" . $not_ok . "</td><td valign=\"top\">" . $ln_errorlines[$info_id] . "</td></tr></table>";
			}
			$form->add_label("le" . $key, $caption, RENDER_HTML, $tmp);
		}
		else {
			$form->add_label("le" . $key, $caption, RENDER_HTML, $ok);
		}
	}
}
elseif($form_type == 'INR03'
	and $cer_basicdata['cer_basicdata_no_cer_submission_needed'] != 1) {
	foreach($ln_captions as $key=>$caption) {
		if(array_key_exists($key, $ln_errors)) {
			
			$tmp = '';
			foreach($ln_errors[$key] as $key2=>$info_id) {
				$tmp .= "<table style=\"white-space: normal;\" width=\"400\"><tr><td width=\"20\" valign=\"top\">" . $not_ok . "</td><td valign=\"top\">" . $ln_errorlines[$info_id] . "</td></tr></table>";
			}
			$form->add_label("le" . $key, $caption, RENDER_HTML, $tmp);
		}
		else {
			$form->add_label("le" . $key, $caption, RENDER_HTML, $ok);
		}
	}
}

if($form_type == 'CER'
	and ($ln_was_approved == true or $ln_basicdata['ln_no_ln_submission_needed'] == 1)
	) {
	$form->add_section("CER");

	$by = "";
	if($cer_submitted_by["name"])
	{
		$by = " by " . $cer_submitted_by["name"] . " " . $cer_submitted_by["firstname"];
	}

	if(has_access("can_edit_cer_workflow_data")) {
			$form->add_checkbox("cer_basicdata_no_cer_submission_needed", "No CER submission needed", $cer_basicdata["cer_basicdata_no_cer_submission_needed"], 0, "CER Submission");
	}
	else
	{
		$form->add_hidden("cer_basicdata_no_cer_submission_needed");
	}

	$form->add_label("date1c", "Date of Submission", 0, to_system_date($cer_basicdata["cer_basicdata_submitted"]) . $by);

	$by = "";
	if($cer_resubmitted_by["name"])
	{
		$by = " by " . $cer_resubmitted_by["name"] . " " . $cer_resubmitted_by["firstname"];
	}
	$form->add_label("date2c", "Date of latest Resubmission", 0, to_system_date($cer_basicdata["cer_basicdata_resubmitted"]) . $by);

	$by = "";
	if($cer_rejected_by["name"])
	{
		$by = " by " . $cer_rejected_by["name"] . " " . $cer_rejected_by["firstname"];
	}

	$form->add_label("date3c", "Date of Rejection", 0, to_system_date($cer_basicdata["cer_basicdata_rejected"]) . $by);


	foreach($cer_captions as $key=>$caption) {
		if(array_key_exists($key, $cer_errors)) {
			
			$tmp = '';
			foreach($cer_errors[$key] as $key2=>$info_id) {
				$tmp .= "<table style=\"white-space: normal;\" width=\"400\"><tr><td width=\"20\" valign=\"top\">" . $not_ok . "</td><td valign=\"top\">" . $cer_errorlines[$info_id] . "</td></tr></table>";
			}
			$form->add_label("ce" . $key, $caption, RENDER_HTML, $tmp);
		}
		else {
			$form->add_label("ce" . $key, $caption, RENDER_HTML, $ok);
		}
	}
}
elseif($form_type == 'CER') {
	$form->add_hidden("cer_basicdata_no_cer_submission_needed");
}
elseif($form_type == 'AF'
	and ($ln_was_approved == true or $ln_basicdata['ln_no_ln_submission_needed'] == 1)
	) {
	$form->add_section("INR03 Form");

	$by = "";
	if($cer_submitted_by["name"])
	{
		$by = " by " . $cer_submitted_by["name"] . " " . $cer_submitted_by["firstname"];
	}

	if(has_access("can_edit_cer_workflow_data")) {
			$form->add_checkbox("cer_basicdata_no_cer_submission_needed", "No INR03 submission needed", $cer_basicdata["cer_basicdata_no_cer_submission_needed"], 0, "INR03 Submission");
	}
	else
	{
		$form->add_hidden("cer_basicdata_no_cer_submission_needed");
	}

	$form->add_label("date1c", "Date of Submission", 0, to_system_date($cer_basicdata["cer_basicdata_submitted"]) . $by);

	$by = "";
	if($cer_resubmitted_by["name"])
	{
		$by = " by " . $cer_resubmitted_by["name"] . " " . $cer_resubmitted_by["firstname"];
	}
	$form->add_label("date2c", "Date of latest Resubmission", 0, to_system_date($cer_basicdata["cer_basicdata_resubmitted"]) . $by);

	$by = "";
	if($cer_rejected_by["name"])
	{
		$by = " by " . $cer_rejected_by["name"] . " " . $cer_rejected_by["firstname"];
	}

	$form->add_label("date3c", "Date of Rejection", 0, to_system_date($cer_basicdata["cer_basicdata_rejected"]) . $by);

	/*
	foreach($ln_captions as $key=>$caption) {
		if(array_key_exists($key, $ln_errors)) {
			
			$tmp = '';
			foreach($ln_errors[$key] as $key2=>$info_id) {
				$tmp .= "<table style=\"white-space: normal;\" width=\"400\"><tr><td width=\"20\" valign=\"top\">" . $not_ok . "</td><td valign=\"top\">" . $ln_errorlines[$info_id] . "</td></tr></table>";
			}
			$form->add_label("le2" . $key, $caption, RENDER_HTML, $tmp);
		}
		else {
			$form->add_label("le2" . $key, $caption, RENDER_HTML, $ok);
		}
	}
	*/
}
elseif($form_type == 'AF') {
	$form->add_hidden("cer_basicdata_no_cer_submission_needed");
}


if(has_access("has_full_access_to_cer") 
	and $form_type == "CER")
{
	$form->add_section("Locking");
	$form->add_checkbox("ln_basicdata_locked", "LNR is locked", $ln_basicdata["ln_basicdata_locked"], 0, "LNR");
	$form->add_checkbox("cer_basicdata_cer_locked", "CER is locked", $cer_basicdata["cer_basicdata_cer_locked"], 0, "CER");
	
}
elseif(has_access("has_full_access_to_cer") 
	and $form_type == "INR03")
{
	$form->add_section("Locking");
	$form->add_hidden("ln_basicdata_locked");
	$form->add_checkbox("cer_basicdata_cer_locked", "INR03 is locked", $cer_basicdata["cer_basicdata_cer_locked"], 0, "INR03");
}
elseif(has_access("has_full_access_to_cer") 
	and $form_type == "AF")
{

	$form->add_section("Locking");
	$form->add_checkbox("ln_basicdata_locked", "LNR is locked", $ln_basicdata["ln_basicdata_locked"], 0, "LNR");
	$form->add_checkbox("cer_basicdata_cer_locked", "INR03 is locked", $cer_basicdata["cer_basicdata_cer_locked"], 0, "INR03");
	
}
else
{
	if($form_type == "INR03" 
		and $cer_basicdata["cer_basicdata_cer_locked"] == 1) {
		$form->add_label("l1", "INR03", 0, "is locked");
	}
	elseif($form_type == "AF" 
		and $ln_basicdata["ln_basicdata_locked"] == 1) {
		$form->add_label("l1", "LNR", 0, "is locked");
	}
	else
	{
		if($ln_basicdata["ln_basicdata_locked"] == 1) {
			$form->add_label("l1", "LNR", 0, "is locked");
		}
		if($cer_basicdata["cer_basicdata_cer_locked"] == 1) {
			$form->add_label("l2", "CER", 0, "is locked");
		}
	}

	$form->add_hidden("cer_basicdata_cer_locked");
	$form->add_hidden("ln_basicdata_locked");
}

if(has_access("has_full_access_to_cer"))
{
	$form->add_button("save", "Save Data");
}

if($posorder_present == 0)
{
	$form->error("There is no entry in the POS Index for this project. Please add the project to the POS Index frist.");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if(param("s") > 0)
{
	$form->error("Thank you! Your request was submitted successfully.");
}
if($form->button("save"))
{
	
	//update cer basicdata
	$fields = array();

	$value = dbquote($form->value("ln_no_ln_submission_needed"));
	$fields[] = "ln_no_ln_submission_needed = " . $value;

	$value1 = "current_timestamp";
	$fields[] = "date_modified = " . $value1;

	if (isset($_SESSION["user_login"]))
	{
		$value1 = $_SESSION["user_login"];
		$fields[] = "user_modified = " . dbquote($value1);
	}

	$sql = "update ln_basicdata set " . join(", ", $fields) . 
		" where ln_basicdata_project = " . param("pid") . 
		" and ln_basicdata_version = 0 ";
	mysql_query($sql) or dberror($sql);


	
	
	//update cer basicdata
	$fields = array();


	$value = dbquote($form->value("cer_basicdata_cer_locked"));
	$fields[] = "cer_basicdata_cer_locked = " . $value;

	$value = dbquote($form->value("cer_basicdata_no_cer_submission_needed"));
	$fields[] = "cer_basicdata_no_cer_submission_needed = " . $value;

	$value1 = "current_timestamp";
	$fields[] = "date_modified = " . $value1;

	if (isset($_SESSION["user_login"]))
	{
		$value1 = $_SESSION["user_login"];
		$fields[] = "user_modified = " . dbquote($value1);
	}

	$sql = "update cer_basicdata set " . join(", ", $fields) . 
		" where cer_basicdata_project = " . param("pid") . 
		" and cer_basicdata_version = 0 ";
	mysql_query($sql) or dberror($sql);

	


	//crete a business plan version in case ln was locked
	/*
	if($form->value("ln_basicdata_locked") == 1 and $ln_basicdata["ln_basicdata_locked"] == 0)
	{
		$new_cer_version = create_cer_version(param("pid"), $context = "ln", $project["pipeline"]);
	}

	if($form->value("cer_basicdata_cer_locked") == 1 and $cer_basicdata["cer_basicdata_cer_locked"] == 0)
	{
		$new_cer_version = create_cer_version(param("pid"), $context = "cer", $project["pipeline"]);
	}
	*/
	

	//update ln basicdata
	$fields = array();

	$value = dbquote($form->value("ln_basicdata_locked"));
	$fields[] = "ln_basicdata_locked = " . $value;

	$value1 = "current_timestamp";
	$fields[] = "date_modified = " . $value1;

	if (isset($_SESSION["user_login"]))
	{
		$value1 = $_SESSION["user_login"];
		$fields[] = "user_modified = " . dbquote($value1);
	}

	$sql = "update ln_basicdata set " . join(", ", $fields) . 
		   " where ln_basicdata_project = " . param("pid") . 
		   " and ln_basicdata_version = 0";

	mysql_query($sql) or dberror($sql);


	$link = "cer_project.php?pid=" . param("pid");
	redirect($link);
}
elseif($form->button("submit_ln"))
{
	//prepare attachment
	$write_tmp_file = true;
	include('lnr_sg_booklet_pdf.php');
	
	$project_info =  "Project " . $project["order_number"] . ", " . $project["country_name"] . ", " . $project["order_shop_address_place"] . ", " . $project["order_shop_address_company"];
	
	//update ln_basic_data
	$fields = array();

	$value = date("Y-m-d H:i:s");
	
	if($ln_basicdata["ln_basicdata_submitted"] == NULL or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == "0000")
	{
		$fields[] = "ln_basicdata_submitted = " . dbquote($value);
		$fields[] = "ln_basicdata_submitted_by = " . dbquote(user_id());
	}
	else
	{
		$fields[] = "ln_basicdata_resubmitted = " . dbquote($value);
		$fields[] = "ln_basicdata_resubmitted_by = " . dbquote(user_id());
	}
	
	$sql = "update ln_basicdata set " . join(", ", $fields) . 
		   " where ln_basicdata_project = " . param("pid") . 
		   " and ln_basicdata_version = 0";
	mysql_query($sql) or dberror($sql);


	//update project milestones
	$fields = array();


	$value = date("Y-m-d");
	$fields[] = "project_milestone_date = " . dbquote($value);

	$value = user_login();
	$fields[] = "user_created = " . dbquote($value);

	$value = date("Y-m-d H:i:s");
	$fields[] = "date_created = " . dbquote($value);
	
	$value = user_login();
	$fields[] = "user_modified = " . dbquote($value);

	$value = date("Y-m-d H:i:s");
	$fields[] = "date_modified = " . dbquote($value);
	
	
	if($ln_basicdata["ln_basicdata_submitted"] == NULL 
		or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == "0000")
	{
		$sql = "update project_milestones set " . join(", ", $fields) . 
			   " where project_milestone_project = " . param("pid") . 
			   " and project_milestone_milestone = 16";

		$sql_mail_milestones = "select * from milestones " . 
							   "where milestone_id = 16 and milestone_on_cer_submission = 1";
	}
	else
	{
		$sql = "select count(project_milestone_id) as num_recs from project_milestones " . 
				" where project_milestone_project = " . param("pid") . 
				" and project_milestone_milestone = 18";

		$sql_mail_milestones = "select * from milestones " . 
							   "where milestone_id = 18 and milestone_on_cer_submission = 1";

		 $res = mysql_query($sql) or dberror($sql);
		 if ($row = mysql_fetch_assoc($res))
		 {
			 if($row["num_recs"] == 1) 
			 {
				 $sql = "update project_milestones set " . join(", ", $fields) . 
						" where project_milestone_project = " . param("pid") . 
						" and project_milestone_milestone = 18";
			 }
			 else
			 {
				 $sql = "insert into project_milestones (project_milestone_project, " . 
						"project_milestone_milestone, project_milestone_date, " . 
						"project_milestone_date_comment, user_created, date_created) Values (" . 
						dbquote(param("pid")) . ", " .
						"18, " .
						dbquote(date("Y-m-d")) . ", " .
						"'LNR Resubmission', " .
						dbquote(user_login()) . ", " .
						dbquote(date("Y-m-d H:i:s")) . ")";

			 }
		 }
		 
		
		
	}

	mysql_query($sql) or dberror($sql);

	//create ln-version
	$new_cer_version = create_cer_version(param("pid"), 'ln', $project["pipeline"], $ln_basicdata['ln_basicdata_id']);


	//delete task for INR03-Submission
	$sql = "delete from tasks where task_order_state = 4 and task_order = " . dbquote($project['project_order']);
	$result = mysql_query($sql) or dberror($sql);


	

	// 03 send email notificaions
	//get sender
	$user_data = get_user(user_id());
	$sender_name = $user_data['firstname'] . " " . $user_data['name'];
	$sender_firstname = $user_data['firstname'];
	$sender_email = $user_data['email'];
	$sender_id = user_id();
	
	//get recipient
	$user_data = get_user($project['project_retail_coordinator']);
	$recipient_name = $user_data['firstname'] . " " . $user_data['name'];
	$recipient_firtsname = $user_data['firstname'];
	$recipient_email = $user_data['email'];
	$recipient_id = $project['project_retail_coordinator'];


	if($user_data["cc"])
	{
		$cc_recipients[$user_data["cc"]] = $user_data["cc"];

		$sql = 'select user_id from users ' . 
			   'where user_email = ' . dbquote($user_data["cc"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			$cc_recipient_ids[$user_data["cc"]] = $row_u["user_id"];
		}
	}

	if($user_data["deputy"])
	{
		$cc_recipients[$user_data["deputy"]] = $user_data["deputy"];

		$sql = 'select user_id from users ' . 
			   'where user_email = ' . dbquote($user_data["deputy"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			$cc_recipient_ids[$user_data["cc"]] = $row_u["user_id"];
		}
	}


	
	if($ln_basicdata["ln_basicdata_submitted"] == NULL 
		or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == "0000")
	{
		$subject = "LNR submission alert - " . $project_info;

		$sql = 'select * from projecttype_newproject_notifications ' . 
			   'where projecttype_newproject_notification_on_lnsubmission = 1 ' . 
			   ' and projecttype_newproject_notification_country = ' . $project["order_shop_address_country"] . 
			   ' and projecttype_newproject_notification_postype = ' . $project['project_postype'] . 
				   ' and projecttype_newproject_notification_legal_type = 1';
	}
	else
	{
		$subject = "LNR resubmission alert - " . $project_info;

		$sql = 'select * from projecttype_newproject_notifications ' . 
			   'where projecttype_newproject_notification_on_lnresubmission = 1 ' . 
			   ' and projecttype_newproject_notification_country = ' . $project["order_shop_address_country"] . 
			   ' and projecttype_newproject_notification_postype = ' . $project['project_postype'] . 
				   ' and projecttype_newproject_notification_legal_type = 1';
	}

	$sql_mail = "select * from mail_alert_types " . 
				"where mail_alert_type_id = 48";


	
	
	//get notifivcation recipients
	$cc_fields = array();
	$cc_fields[] = 'projecttype_newproject_notification_email';
	$cc_fields[] = 'projecttype_newproject_notification_emailcc1';
	$cc_fields[] = 'projecttype_newproject_notification_emailcc2';
	$cc_fields[] = 'projecttype_newproject_notification_emailcc3';
	$cc_fields[] = 'projecttype_newproject_notification_emailcc4';
	$cc_fields[] = 'projecttype_newproject_notification_emailcc5';
	$cc_fields[] = 'projecttype_newproject_notification_emailcc6';
	$cc_fields[] = 'projecttype_newproject_notification_emailcc7';
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		foreach($cc_fields as $key=>$field_name) {
			if($row[$field_name])
			{
				$cc_recipients[$row[$field_name]] = $row[$field_name];

				$sql = 'select user_id from users ' . 
					   'where user_email = ' . dbquote($row[$field_name]);

				$res_u = mysql_query($sql) or dberror($sql);
				if ($row_u = mysql_fetch_assoc($res_u))
				{
					$cc_recipient_ids[$row[$field_name]] = $row_u["user_id"];
				}
			}
		}
	}
	
	//users for regional responsabilities
	$regional_responsable_reciepients = get_regional_responsible($project['project_cost_type'], $project['order_client_address']);

	//local financers
	$local_financers = get_local_financers($project['project_cost_type'], $project['order_client_address']);

	if(count($regional_responsable_reciepients) > 0)
	{
		foreach($regional_responsable_reciepients as $user_id=>$email)
		{							
			if($email and !in_array($email, $cc_recipients)) {
				$cc_recipients[$email] = $email;
				$cc_recipient_ids[$email] = $user_id;
			}
		}
	}

	if(count($local_financers) > 0)
	{
		foreach($local_financers as $user_id=>$email)
		{							
			if($email and !in_array($email, $cc_recipients)) {
				$cc_recipients[$email] = $email;
				$cc_recipient_ids[$email] = $user_id;
			}
		}
	}


	//milesonte allerts
	if($sql_mail_milestones) {
		$cc_fields = array();
		$cc_fields[] = 'milestone_email_' . $project['project_postype'];
		$cc_fields[] = 'milestone_ccmail_' . $project['project_postype'];
		$cc_fields[] = 'milestone_ccmail_' . $project['project_postype']. '_2';
		$cc_fields[] = 'milestone_ccmail_' . $project['project_postype']. '_3';
		$cc_fields[] = 'milestone_ccmail_' . $project['project_postype']. '_4';

		$res = mysql_query($sql_mail_milestones) or dberror($sql_mail_milestones);
		if ($row = mysql_fetch_assoc($res))
		{
			foreach($cc_fields as $key=>$field_name) {
				
				if($row[$field_name] 
					and $recipient_email != $row[$field_name]
					and !in_array($row[$field_name], $cc_recipients)) {
					$cc_recipients[$row[$field_name]] = $row[$field_name];

					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row[$field_name]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$cc_recipient_ids[$row[$field_name]] = $row_u["user_id"];
					}
				}
			}
		}
	}

	//send mail		
	$reciepient_ccs = '';
	$res = mysql_query($sql_mail) or dberror($sql_mail);
	if ($row = mysql_fetch_assoc($res))
	{
		$link = APPLICATION_URL ."/cer/lnr_sg_booklet_pdf.php?pid=" . param("pid") . "&booklet=true";
		$link2 = APPLICATION_URL ."/cer/cer_project.php?pid=" . param("pid");
		
		$mail_text = $row["mail_alert_mail_text"];

		$mail_text = str_replace('{name}', $recipient_firtsname, $mail_text);
		$mail_text = str_replace('{sender}', $sender_firstname, $mail_text);
		$mail_text = str_replace('{link}', $link, $mail_text);
		$mail_text = str_replace('{link2}', $link2, $mail_text);

		
		$bodytext = $mail_text;

		$mail = new Mail();
		$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
		$mail->set_sender($sender_email, $sender_name);
		
		$mail->add_recipient($recipient_email);



		foreach($cc_recipients as $key=>$cc_email)
		{
			if($recipient_email != $cc_email)
			{
				$mail->add_cc($cc_email);
				$reciepient_ccs .= "\n" . $cc_email;
			}
		}

		$mail->add_text($bodytext);

		if(isset($file_name) and file_exists($file_name)) {
			if(filesize($file_name) < '8500000') {
				$file_data = fread(fopen($file_name,"r"),filesize($file_name));
				$mail->add_attachment('LNR booklet', $file_data);
			}
		}
		if($senmail_activated == true)
		{
			$result = $mail->send();
		}
		else
		{
			$result = 1;
		}

		$rctps = $recipient_email . "\n" . "and CC-Mail to:" . $reciepient_ccs;
		

		if(isset($file_name) and file_exists($file_name)) {
			unlink($file_name);
		}

		if($result == 1)
		{
			$fields = array();
			$values = array();

			$fields[] = "cer_mail_project";
			$values[] = dbquote(param("pid"));

			$fields[] = "cer_mail_group";
			$values[] = dbquote($subject);

			$fields[] = "cer_mail_text";
			$values[] = dbquote($bodytext);

			$fields[] = "cer_mail_sender";
			$values[] = dbquote($sender_name);

			$fields[] = "cer_mail_sender_email";
			$values[] = dbquote($sender_email);

			$fields[] = "cer_mail_reciepient";
			$values[] = dbquote($rctps);

			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "date_modified";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "user_modified";
			$values[] = dbquote(user_login());

			$sql = "insert into cer_mails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			
			mysql_query($sql) or dberror($sql);
		}
	}


	redirect("cer_project.php?pid=" . param("pid") . "&s=" . $new_cer_version);

}
elseif($form->button("submit"))
{
	$project_info =  "Project " . $project["order_number"] . ", " . $project["country_name"] . ", " . $project["order_shop_address_place"] . ", " . $project["order_shop_address_company"];


	$legal_type = $project["project_cost_type"];
	if($legal_type != 1)
	{
		$legal_type = 0;
	}

	$cc_recipients = array();
	$cc_recipient_ids = array();


	//get sender
	$user_data = get_user(user_id());
	$sender_name = $user_data['firstname'] . " " . $user_data['name'];
	$sender_email = $user_data['email'];
	$sender_id = user_id();
	
	//get recipient
	$user_data = get_user($project['project_retail_coordinator']);
	$recipient_name = $user_data['firstname'] . " " . $user_data['name'];
	$recipient_firtsname = $user_data['firstname'];
	$recipient_email = $user_data['email'];
	$recipient_id = $project['project_retail_coordinator'];


	if($user_data["cc"])
	{
		$cc_recipients[$user_data["cc"]] = $user_data["cc"];

		$sql = 'select user_id from users ' . 
			   'where user_email = ' . dbquote($user_data["cc"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			$cc_recipient_ids[$user_data["cc"]] = $row_u["user_id"];
		}
	}

	if($user_data["deputy"])
	{
		$cc_recipients[$user_data["deputy"]] = $user_data["deputy"];

		$sql = 'select user_id from users ' . 
			   'where user_email = ' . dbquote($user_data["deputy"]);

		$res_u = mysql_query($sql) or dberror($sql);
		if ($row_u = mysql_fetch_assoc($res_u))
		{
			$cc_recipient_ids[$user_data["cc"]] = $row_u["user_id"];
		}
	}
	
	

	$subject = '';
	if($form_type == 'INR03' 
		or ($form_type == 'AF' and $ln_basicdata['ln_basicdata_locked'] == 1)) {
		
		//prepare attachment
		$write_tmp_file = true;
		include('cer_sg_form_inr03_pdf.php');

		// 01 update cer_basicdata
		$fields = array();
			
		if($cer_basicdata["cer_basicdata_submitted"] == NULL 
			or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == "0000") {
			$value1 = date("Y-m-d H:i:s");
			$fields[] = "cer_basicdata_submitted = " . dbquote($value1);
			$fields[] = "cer_basicdata_submitted_by = " . dbquote(user_id());

			$sql_mail_milestones = "select * from milestones " . 
							   "where milestone_id = 35 and milestone_on_cer_submission = 1";
		}
		else {
			$value1 = date("Y-m-d H:i:s");
			$fields[] = "cer_basicdata_resubmitted = " . dbquote($value1);
			$fields[] = "cer_basicdata_resubmitted_by = " . dbquote(user_id());

			$sql_mail_milestones = "select * from milestones " . 
							   "where milestone_id = 36 and milestone_on_cer_submission = 1";
		}

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"])) {
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update cer_basicdata set " . join(", ", $fields) . 
			" where cer_basicdata_project = " . param("pid") .
			" and cer_basicdata_version = 0 ";
		mysql_query($sql) or dberror($sql);
		
		
		//02 update milestones
		if($cer_basicdata["cer_basicdata_submitted"] == NULL or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == "0000")  {
			$sql = "select * from milestones " . 
				   "where milestone_id = 1 and milestone_on_cer_submission = 1";
		}
		else {
			$sql = "select * from milestones " . 
				   "where milestone_id = 15 and milestone_on_cer_submission = 1";
		}
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res)) {
			$sql = "update project_milestones SET " . 
				   "project_milestone_date = " . dbquote(date("Y-m-d")) . ", " . 
				   "project_milestone_date_comment = " . dbquote($row['milestone_text_state']) . ", " .
				   "user_created = " . dbquote(user_login()) . ", " .
				   "date_created = " . dbquote(date("Y-m-d H:i:s")) . 
				   " where project_milestone_project = " . dbquote(param("pid")) .
				   " and project_milestone_milestone = " . dbquote($row["milestone_id"]);
			$result = mysql_query($sql) or dberror($sql);

			//crete a business plan version in case ln was locked
			$new_cer_version = create_cer_version(param("pid"), $context = "cer", $project["pipeline"]);
			
			//delete task for INR03-Submission
			$sql = "delete from tasks where task_order_state = 4 and task_order = " . dbquote($project['project_order']);
			$result = mysql_query($sql) or dberror($sql);
		}


		// 03 send email notificaions
		if($cer_basicdata["cer_basicdata_submitted"] == NULL 
			or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == "0000")
		{
			$subject = "INR03 submission alert - " . $project_info;

			$sql = 'select * from projecttype_newproject_notifications ' . 
				   'where projecttype_newproject_notification_oninr03submission = 1 ' . 
				   ' and projecttype_newproject_notification_country = ' . $project["order_shop_address_country"] . 
				   ' and projecttype_newproject_notification_postype = ' . $project['project_postype'] . 
					   ' and projecttype_newproject_notification_legal_type = ' . $legal_type;
		}
		else
		{
			$subject = "INR03 resubmission alert - " . $project_info;

			$sql = 'select * from projecttype_newproject_notifications ' . 
				   'where projecttype_newproject_notification_oninr03resubmission = 1 ' . 
				   ' and projecttype_newproject_notification_country = ' . $project["order_shop_address_country"] . 
				   ' and projecttype_newproject_notification_postype = ' . $project['project_postype'] . 
					   ' and projecttype_newproject_notification_legal_type = ' . $legal_type;
		}

		if($cer_basicdata["cer_basicdata_submitted"] == NULL 
			or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == "0000") {
				$sql_mail = "select * from mail_alert_types " . 
							"where mail_alert_type_id = 43";
		}
		else {
			$sql_mail = "select * from mail_alert_types " . 
							"where mail_alert_type_id = 52";
		}


		$link = APPLICATION_URL ."/cer/cer_sg_form_inr03_pdf.php?pid=" . param("pid") . "&booklet=true";
		$link2 =  APPLICATION_URL ."/cer/cer_project.php?pid=" . param("pid");
	}
	elseif($form_type == 'CER') {
		
		//prepare attachment
		$write_tmp_file = true;
		include('cer_sg_booklet_pdf.php');

		// 01 update cer_basicdata
		$fields = array();
			
		if($cer_basicdata["cer_basicdata_submitted"] == NULL 
			or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == "0000") {
			$value1 = date("Y-m-d H:i:s");
			$fields[] = "cer_basicdata_submitted = " . dbquote($value1);
			$fields[] = "cer_basicdata_submitted_by = " . dbquote(user_id());

			$sql_mail_milestones = "select * from milestones " . 
							   "where milestone_id = 35 and milestone_on_cer_submission = 1";
		}
		else {
			$value1 = date("Y-m-d H:i:s");
			$fields[] = "cer_basicdata_resubmitted = " . dbquote($value1);
			$fields[] = "cer_basicdata_resubmitted_by = " . dbquote(user_id());

			$sql_mail_milestones = "select * from milestones " . 
							   "where milestone_id = 36 and milestone_on_cer_submission = 1";
		}

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"])) {
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update cer_basicdata set " . join(", ", $fields) . 
			" where cer_basicdata_project = " . param("pid") .
			" and cer_basicdata_version = 0 ";
		mysql_query($sql) or dberror($sql);
		
		
		//02 update milestones
		if($cer_basicdata["cer_basicdata_submitted"] == NULL or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == "0000")  {
			$sql = "select * from milestones " . 
				   "where milestone_id = 1 and milestone_on_cer_submission = 1";
		}
		else {
			$sql = "select * from milestones " . 
				   "where milestone_id = 15 and milestone_on_cer_submission = 1";
		}
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res)) {
			$sql = "update project_milestones SET " . 
				   "project_milestone_date = " . dbquote(date("Y-m-d")) . ", " . 
				   "project_milestone_date_comment = " . dbquote($row['milestone_text_state']) . ", " .
				   "user_created = " . dbquote(user_login()) . ", " .
				   "date_created = " . dbquote(date("Y-m-d H:i:s")) . 
				   " where project_milestone_project = " . dbquote(param("pid")) .
				   " and project_milestone_milestone = " . dbquote($row["milestone_id"]);
			$result = mysql_query($sql) or dberror($sql);

			//crete a business plan version in case ln was locked
			$new_cer_version = create_cer_version(param("pid"), $context = "cer", $project["pipeline"]);
			
			//delete task for INR03-Submission
			$sql = "delete from tasks where task_order_state = 4 and task_order = " . dbquote($project['project_order']);
			$result = mysql_query($sql) or dberror($sql);
		}


		// 03 send email notificaions
		if($cer_basicdata["cer_basicdata_submitted"] == NULL 
			or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == "0000")
		{
			$subject = "CER submission alert - " . $project_info;

			$sql = 'select * from projecttype_newproject_notifications ' . 
				   'where projecttype_newproject_notification_oncerafsubmission = 1 ' . 
				   ' and projecttype_newproject_notification_country = ' . $project["order_shop_address_country"] . 
				   ' and projecttype_newproject_notification_postype = ' . $project['project_postype'] . 
					   ' and projecttype_newproject_notification_legal_type = ' . $legal_type;
		}
		else
		{
			$subject = "CER resubmission alert - " . $project_info;

			$sql = 'select * from projecttype_newproject_notifications ' . 
				   'where projecttype_newproject_notification_oncerafresubmission = 1 ' . 
				   ' and projecttype_newproject_notification_country = ' . $project["order_shop_address_country"] . 
				   ' and projecttype_newproject_notification_postype = ' . $project['project_postype'] . 
					   ' and projecttype_newproject_notification_legal_type = ' . $legal_type;
		}

		if($cer_basicdata["cer_basicdata_submitted"] == NULL 
			or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == "0000") {
				$sql_mail = "select * from mail_alert_types " . 
							"where mail_alert_type_id = 4";
		}
		else {
			$sql_mail = "select * from mail_alert_types " . 
							"where mail_alert_type_id = 50";
		}

		$link = APPLICATION_URL ."/cer/cer_sg_booklet_pdf.php?pid=" . param("pid");
		$link2 = APPLICATION_URL ."/cer/cer_project.php?pid=" . param("pid");
	}
	elseif($form_type == 'AF') {
		
		//prepare attachment
		$write_tmp_file = true;
		include('lnr_sg_booklet_pdf.php');
		
		// 01 update ln_basicdata
		$fields = array();
			
		if($ln_basicdata["ln_basicdata_submitted"] == NULL 
			or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == "0000") {
			$value1 = date("Y-m-d H:i:s");
			$fields[] = "ln_basicdata_submitted = " . dbquote($value1);
			$fields[] = "ln_basicdata_submitted_by = " . dbquote(user_id());

			$sql_mail_milestones = "select * from milestones " . 
							   "where milestone_id = 16 and milestone_on_cer_submission = 1";
		}
		else {
			$value1 = date("Y-m-d H:i:s");
			$fields[] = "ln_basicdata_resubmitted = " . dbquote($value1);
			$fields[] = "ln_basicdata_resubmitted_by = " . dbquote(user_id());

			$sql_mail_milestones = "select * from milestones " . 
							   "where milestone_id = 18 and milestone_on_cer_submission = 1";
		}

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"])) {
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update ln_basicdata set " . join(", ", $fields) . 
			" where ln_basicdata_project = " . param("pid") .
			" and ln_basicdata_version = 0 ";
		mysql_query($sql) or dberror($sql);
		
		
		//02 update milestones
		if($ln_basicdata["ln_basicdata_submitted"] == NULL or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == "0000")  {
			$sql = "select * from milestones " . 
				   "where milestone_id = 16 and milestone_on_cer_submission = 1";
		}
		else {
			$sql = "select * from milestones " . 
				   "where milestone_id = 18 and milestone_on_cer_submission = 1";
		}
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res)) {
			$sql = "update project_milestones SET " . 
				   "project_milestone_date = " . dbquote(date("Y-m-d")) . ", " . 
				   "project_milestone_date_comment = " . dbquote($row['milestone_text_state']) . ", " .
				   "user_created = " . dbquote(user_login()) . ", " .
				   "date_created = " . dbquote(date("Y-m-d H:i:s")) . 
				   " where project_milestone_project = " . dbquote(param("pid")) .
				   " and project_milestone_milestone = " . dbquote($row["milestone_id"]);
			$result = mysql_query($sql) or dberror($sql);

			//crete a business plan version in case ln was locked
			$new_cer_version = create_cer_version(param("pid"), $context = "ln", $project["pipeline"]);
			
			//delete task for AF-Submission
			$sql = "delete from tasks where task_order_state = 4 and task_order = " . dbquote($project['project_order']);
			$result = mysql_query($sql) or dberror($sql);
		}


		// 03 send email notificaions
		if($ln_basicdata["ln_basicdata_submitted"] == NULL 
			or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == "0000")
		{
			$subject = "LNR submission alert - " . $project_info;

			$sql = 'select * from projecttype_newproject_notifications ' . 
				   'where projecttype_newproject_notification_on_afsubmission = 1 ' . 
				   ' and projecttype_newproject_notification_country = ' . $project["order_shop_address_country"] . 
				   ' and projecttype_newproject_notification_postype = ' . $project['project_postype'] . 
					   ' and projecttype_newproject_notification_legal_type = ' . $legal_type;
		}
		else
		{
			$subject = "LNR resubmission alert - " . $project_info;

			$sql = 'select * from projecttype_newproject_notifications ' . 
				   'where projecttype_newproject_notification_on_afresubmission = 1 ' . 
				   ' and projecttype_newproject_notification_country = ' . $project["order_shop_address_country"] . 
				   ' and projecttype_newproject_notification_postype = ' . $project['project_postype'] . 
					   ' and projecttype_newproject_notification_legal_type = ' . $legal_type;
		}

		if($cer_basicdata["cer_basicdata_submitted"] == NULL 
			or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == "0000") {
				$sql_mail = "select * from mail_alert_types " . 
							"where mail_alert_type_id = 20";
		}
		else {
			$sql_mail = "select * from mail_alert_types " . 
							"where mail_alert_type_id = 51";
		}

		$link = APPLICATION_URL ."/cer/lnr_sg_booklet_pdf.php?pid=" . param("pid") . "&booklet=true";
		$link2 = APPLICATION_URL ."/cer/cer_project.php?pid=" . param("pid");
	}
	
	//get notifivcation recipients
	$cc_fields = array();
	$cc_fields[] = 'projecttype_newproject_notification_email';
	$cc_fields[] = 'projecttype_newproject_notification_emailcc1';
	$cc_fields[] = 'projecttype_newproject_notification_emailcc2';
	$cc_fields[] = 'projecttype_newproject_notification_emailcc3';
	$cc_fields[] = 'projecttype_newproject_notification_emailcc4';
	$cc_fields[] = 'projecttype_newproject_notification_emailcc5';
	$cc_fields[] = 'projecttype_newproject_notification_emailcc6';
	$cc_fields[] = 'projecttype_newproject_notification_emailcc7';
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		foreach($cc_fields as $key=>$field_name) {
			if($row[$field_name])
			{
				$cc_recipients[$row[$field_name]] = $row[$field_name];

				$sql = 'select user_id from users ' . 
					   'where user_email = ' . dbquote($row[$field_name]);

				$res_u = mysql_query($sql) or dberror($sql);
				if ($row_u = mysql_fetch_assoc($res_u))
				{
					$cc_recipient_ids[$row[$field_name]] = $row_u["user_id"];
				}
			}
		}
	}
	
	//users for regional responsabilities
	$regional_responsable_reciepients = get_regional_responsible($project['project_cost_type'], $project['order_client_address']);

	//local financers
	$local_financers = get_local_financers($project['project_cost_type'], $project['order_client_address']);

	if(count($regional_responsable_reciepients) > 0)
	{
		foreach($regional_responsable_reciepients as $user_id=>$email)
		{							
			if($email and !in_array($email, $cc_recipients)) {
				$cc_recipients[$email] = $email;
				$cc_recipient_ids[$email] = $user_id;
			}
		}
	}

	if(count($local_financers) > 0)
	{
		foreach($local_financers as $user_id=>$email)
		{							
			if($email and !in_array($email, $cc_recipients)) {
				$cc_recipients[$email] = $email;
				$cc_recipient_ids[$email] = $user_id;
			}
		}
	}


	//milesonte allerts
	if($sql_mail_milestones) {
		$cc_fields = array();
		$cc_fields[] = 'milestone_email_' . $project['project_postype'];
		$cc_fields[] = 'milestone_ccmail_' . $project['project_postype'];
		$cc_fields[] = 'milestone_ccmail_' . $project['project_postype']. '_2';
		$cc_fields[] = 'milestone_ccmail_' . $project['project_postype']. '_3';
		$cc_fields[] = 'milestone_ccmail_' . $project['project_postype']. '_4';

		$res = mysql_query($sql_mail_milestones) or dberror($sql_mail_milestones);
		if ($row = mysql_fetch_assoc($res))
		{
			foreach($cc_fields as $key=>$field_name) {
				
				if($row[$field_name] 
					and $recipient_email != $row[$field_name]
					and !in_array($row[$field_name], $cc_recipients)) {
					$cc_recipients[$row[$field_name]] = $row[$field_name];

					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row[$field_name]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$cc_recipient_ids[$row[$field_name]] = $row_u["user_id"];
					}
				}
			}
		}
	}

	//send mail		
	$reciepient_ccs = '';
	$res = mysql_query($sql_mail) or dberror($sql_mail);
	if ($row = mysql_fetch_assoc($res))
	{
		
		$mail_text = $row["mail_alert_mail_text"];

		$mail_text = str_replace('{name}', $recipient_firtsname, $mail_text);
		$mail_text = str_replace('{sender}', $sender_firstname, $mail_text);
		$mail_text = str_replace('{link}', $link, $mail_text);
		$mail_text = str_replace('{link2}', $link2, $mail_text);
		
		$bodytext = $mail_text;
		

		$mail = new Mail();
		$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
		$mail->set_sender($sender_email, $sender_name);
		
		$mail->add_recipient($recipient_email);

		foreach($cc_recipients as $key=>$cc_email)
		{
			if($recipient_email != $cc_email)
			{
				$mail->add_cc($cc_email);
				$reciepient_ccs .= "\n" . $cc_email;
			}
		}

		$mail->add_text($bodytext);

		if(isset($file_name) and file_exists($file_name)) {
			if(filesize($file_name) < '8500000') {
				$file_data = fread(fopen($file_name,"r"),filesize($file_name));
				
				if($form_type == 'AF') {
					$mail->add_attachment('LNR booklet', $file_data);
				}
				elseif($form_type == 'CER') {
					$mail->add_attachment('CER booklet', $file_data);
				}
				elseif($form_type == 'INR03') {
					$mail->add_attachment('INR03 booklet', $file_data);
				}
			}
		}

		if($senmail_activated == true)
		{
			$result = $mail->send();
		}
		else
		{
			$result = 1;
		}

		$rctps = $recipient_email . "\n" . "and CC-Mail to:" . $reciepient_ccs;
		

		if(isset($file_name) and file_exists($file_name)) {
			unlink($file_name);
		}

		if($result == 1)
		{
			$fields = array();
			$values = array();

			$fields[] = "cer_mail_project";
			$values[] = dbquote(param("pid"));

			$fields[] = "cer_mail_group";
			$values[] = dbquote($subject);

			$fields[] = "cer_mail_text";
			$values[] = dbquote($bodytext);

			$fields[] = "cer_mail_sender";
			$values[] = dbquote($sender_name);

			$fields[] = "cer_mail_sender_email";
			$values[] = dbquote($sender_email);

			$fields[] = "cer_mail_reciepient";
			$values[] = dbquote($rctps);

			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "date_modified";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "user_modified";
			$values[] = dbquote(user_login());

			$sql = "insert into cer_mails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			
			mysql_query($sql) or dberror($sql);



			//delete task for INR03-Submission
			$sql = "delete from tasks where task_order_state = 87 and task_order = " . dbquote($project['project_order']);
			$result = mysql_query($sql) or dberror($sql);
		}
	}

	redirect("cer_project.php?pid=" . param("pid") . "&s=" . $new_cer_version);

	/*
	if($ln_error == 1 or $ln_error == 2 or $budget_was_approved == false) {
		
		
		
		$fields = array();

		//$value = dbquote($form->value("cer_basicdata_comment"));
		//$fields[] = "cer_basicdata_comment = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update cer_basicdata set " . join(", ", $fields) . 
			" where cer_basicdata_project = " . param("pid") . 
			" and cer_basicdata_version = 0 ";
		mysql_query($sql) or dberror($sql);
		
		if($form_type == "CER" and $ln_error == 1)
		{
			$form->error("CER Submission is not possible since LNR Data is incomplete!");
		}
		elseif($form_type == "CER" and $ln_error == 2)
		{
			$form->error("CER Submission is not possible since the description of capital expenditure project is not complete! <br />Click 'Capital Expenditure Request Form' and complete the following information: <br />Description, Strategy, Investment (Overview), Benefits/Profitability, Alternative taken into consideration and Risks.");
		}
		elseif($budget_was_approved == false)
		{
			$form->error("Submission is not possible, the project's full budget has not been approved yet!");
		}
		else
		{
			if($form_type == 'INR03') {
				$form->error("INR-03 Submission is not possible since Franchisee Data is incomplete!");
			}
			else {
				$form->error("LNR Submission is not possible since Franchisee Data is incomplete!");
			}
		}
	}
	else
	{

		
		
		
		
			

			
			
			//send mail notification

			$sql = "select order_number, order_shop_address_place, country_name, order_shop_address_company, " .
				   "project_postype, project_projectkind, project_cost_type, project_retail_coordinator, user_email " . 
				   "from projects " .
				   "left join orders on order_id = project_order " .
				   "left join countries on country_id = order_shop_address_country " .
				   "left join project_costs on project_cost_order = order_id " .
				   "left join users on user_id = project_retail_coordinator " .
				   "where project_id = " .  dbquote(param("pid"));

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$rtc_id = $row["project_retail_coordinator"];
				$rtc_email = $row["user_email"];
				
				$user_data = get_user(user_id());
				
				
				if($cer_basicdata["cer_basicdata_submitted"] == NULL or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == "0000")
				{
					if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
					{
						$subject = "CER submission alert - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];
					}
					else
					{
						$subject = "LNR submission alert -  Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", ". $row["order_shop_address_company"];
					}
				}
				else
				{
					if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
					{
						$subject = "CER resubmission alert - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];
					}
					else
					{
						$subject = "LNR resubmission alert - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];
					}
				}

				$subject_rmas = $subject;
			
			
				if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
				{
					$sql = "select * from mail_alert_types " . 
						   "where mail_alert_type_id = 4";
				}
				else
				{
					$sql = "select * from mail_alert_types " . 
						   "where mail_alert_type_id = 20";
				}
							

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$sender_email = $user_data["email"];
					$sender_name = $user_data["firstname"] . " " . $user_data["name"];
					$mail_text = $row["mail_alert_mail_text"] . " " . $user_data["firstname"] . " " . $user_data["name"] . ".";

					$bodytext0 = "Dear " . $row["mail_alert_type_sender_name"] . "\n\n" . $mail_text;
					//$link ="cer_project.php?pid=" . param("pid") . "&id=" . param("pid");
					
					if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
					{
						$link ="cer_booklet_pdf.php?pid=" . param("pid");
					}
					else
					{
						if($use_old_cer_forms_before_2013 == true)
						{
							$link ="af_booklet_pdf.php?pid=" . param("pid");
						}
						else
						{
							$link ="af_booklet_2013_pdf.php?pid=" . param("pid");
						}
					}
					$bodytext = $bodytext0 . "\nClick below to download the booklet:\n";
					$bodytext = $bodytext .  APPLICATION_URL ."/cer/" . $link . "\n";
					
					$link ="cer_application_general.php?pid=" . param("pid");
					$bodytext = $bodytext . "\nClick below to access the general data:\n";
					$bodytext = $bodytext .  APPLICATION_URL ."/cer/" . $link . "\n\n"; 
					
					
					$bodytext .= "Thank you very much.\nKind regards, " . $sender_name;

					$mail = new Mail();
					$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
					$mail->set_sender($sender_email, $sender_name);
					
					$mail->add_recipient($reciepient_email);
					$mail->add_cc($reciepient_cc);

					$email1 = $reciepient_email;
					$email2 = $reciepient_cc;


					foreach($cc_recipients as $key=>$cc_email)
					{
						if($email1 != $cc_email and $email2 != $cc_email)
						{
							$mail->add_cc($cc_email);
							$reciepient_cc .= "\n" . $cc_email;
						}
					}

					if($form_type == "AF" or $form_type == "INR03") {
						if($rtc_email and $recipient_email != $rtc_email)
						{
							$mail->add_cc($rtc_email);
							$reciepient_cc .= "\n" . $rtc_email;

						}
					}

					$mail->add_text($bodytext);

					if($senmail_activated == true)
					{
						$result = $mail->send();
					}
					else
					{
						$result = 1;
					}

					$rctps = $reciepient_email . "\n" . "and CC-Mail to:" . "\n" . $reciepient_cc;

					
					//add submission recipeint alerts
					foreach($recipient_user_ids as $key=>$user_id)
					{
						if($key != $reciepient_email and $key!= $reciepient_cc)
						{
							$mail->add_cc($key);
							$rctps .= "\n" . $key;
						}
					}

					if($result == 1)
					{
						$fields = array();
						$values = array();

						$fields[] = "cer_mail_project";
						$values[] = dbquote(param("pid"));

						$fields[] = "cer_mail_group";
						$values[] = dbquote($subject);

						$fields[] = "cer_mail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "cer_mail_sender";
						$values[] = dbquote($sender_name);

						$fields[] = "cer_mail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "cer_mail_reciepient";
						$values[] = dbquote($rctps);

						$fields[] = "date_created";
						$values[] = "now()";

						$fields[] = "date_modified";
						$values[] = "now()";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into cer_mails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						
						mysql_query($sql) or dberror($sql);
					}
				}
			}

			//check if date of lease contract signature was entered only for corporate projects
			if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
			{
				if(!isset($posleases["poslease_signature_date"]) 
					or $posleases["poslease_signature_date"] == NULL 
					or $posleases["poslease_signature_date"] == '0000-00-00')
				{
					$mail = new ActionMail('cer.posted.lease.signing.date.missing');
					$mail->setParam('projectID', param("pid"));
					$mail->setParam('link', URLBuilder::cerApplicationLease(param("pid")));
					
					
					if($senmail_activated == true)
					{
						$mail->send();
						$result = $mail->isSuccess();
					}
					else
					{
						$result = 1;
					}
				}
			}


			//remove task
			$sql = "delete from tasks where task_order_state = 87 and task_order = " . dbquote($project["project_order"]);
			$res = mysql_query($sql) or dberror($sql);

			redirect("cer_project.php?pid=" . param("pid") . "&s=1");
			
		}
	}
	*/

}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");

require "include/project_page_actions.php";


$page->header();
if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Project");
}
elseif($form_type == "AF")
{
	$page->title("LNR/INR03: Overview and Submission");
}
else
{
	$page->title("LNR/CER: Overview and Submission");
}

$form->render();

require "include/footer_scripts.php";


if(param("s") > 0)
{
	$new_cer_version = param("s");
	$link = '/user/project_costs_budget_pdf.php?pid=' . param("pid") . '&cerversion=' . $new_cer_version;
	$link_loc = '/user/project_costs_budget_pdf.php.php?pid=' . param("pid") . '&cerversion=' . $new_cer_version ."&loc=1";

	$link2 = '/user/project_costs_bids_pdf.php?pid=' . param("pid") . '&cerversion=' . $new_cer_version;

	include("../user/project_costs_bid_comparison_pdf.php");
	
	?>
	<script language="javascript">

		$.ajax({
			type: "GET",
			data: '',
			url: "<?php echo $link;?>",
			success: function(msg){
			}
		});

		$.ajax({
			type: "GET",
			data: '',
			url: "<?php echo $link_loc;?>",
			success: function(msg){
			}
		});

		$.ajax({
			type: "GET",
			data: '',
			url: "<?php echo $link2;?>",
			success: function(msg){
			}
		});
	
	</script>
	<?php

}
$page->footer();

?>