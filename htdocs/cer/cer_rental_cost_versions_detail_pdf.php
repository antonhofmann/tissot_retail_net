<?php
/********************************************************************

    cer_rental_cost_versions_detail_pdf.php

    Print Rental Cost Overview for Versions

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-05-31
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-05-31
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.
*********************************************************************/

/********************************************************************
    prepare all data needed
*********************************************************************/
$posdata = get_pos_data($project["project_order"]);


$project_name = $project["order_shop_address_company"]. ", " . $project["order_shop_address_place"] . ", " . $project["country_name"];
$project_number = $project["project_number"];


$units = array();
$units[0] = "Please select";
$units[1] = "square foot and month";
$units[2] = "square meter and month";
$units[3] = "square foot and year";
$units[4] = "square meter and year";
$units[5] = "total surface per month";
$units[6] = "total surface per year";

/********************************************************************
    print PDF
*********************************************************************/
//set pdf parameters

$margin_top = 16;
$margin_left = 12;
$y = $margin_top;

$pdf->SetTitle("LNR Rental Cost");
$pdf->SetAuthor("Tissot Retailnet");
$pdf->SetDisplayMode(100);
$pdf->AddPage("P");
$pdf->SetAutoPageBreak(true, 0);

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "I", 9);
$pdf->Cell(45, 10, "", 1);

$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);


$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(122, 10,  $project_name, 1, "", "L");
$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");

$y = $y+11;
$x = $margin_left;
$pdf->SetXY($x,$y);

$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(187, 5, "Versions - Rental Cost Overview: " . $project_number, 1, "", "L");

$group = "";
$sql = "select * from cer_basicdata " .
       "where cer_basicdata_project = " . dbquote(param("pid")) .
	   " order by cer_basicdata_version";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($row["cer_basicdata_version"] == 0 or $row["cer_basicdata_version"] == null)
	{
		$version = 0;
	}
	else
	{
		$version = $row["cer_basicdata_version"];
	}

	//get sqms
	$total_sqms = "n.a.";
	$sql_f = "select * " . 
		     " from cer_fixed_rents " . 
		     " where cer_fixed_rent_project_id = " . dbquote(param("pid")) .
		     " and cer_fixed_rent_cer_version = " .  $version . 
		     " order by cer_fixed_rent_from_year, cer_fixed_rent_from_month, cer_fixed_rent_from_day";

	$res_f = mysql_query($sql_f) or dberror($sql_f);
	if($row_f = mysql_fetch_assoc($res_f))
	{
		$total_sqms = $row_f["cer_fixed_rent_total_surface"];
	}

	
	$y = $pdf->GetY();
	$y = $y+7;
	$pdf->SetXY($x,$y);

	$pdf->SetFont("arialn", "B", 8);

	if($version == 0)
	{
		$version_header = "Latest Version ";
	}
	elseif($row["cer_basicdata_version_context"] != NULL)
	{
		$version_header = "Version " . strtoupper($row["cer_basicdata_version_context"]) . ": " .  to_system_date($row["date_created"]);
	}

	$version_header .= " / Total Rented Surface: " . $total_sqms;
	
	$pdf->Cell(187, 5, $version_header, 1, "", "L");
	$pdf->ln();
	
	
	//fixed rents
	$header_printed = 0;
	
	$res_f = mysql_query($sql_f) or dberror($sql_f);
	while($row_f = mysql_fetch_assoc($res_f))
	{
		if($header_printed == 0)
		{
			$header_printed = 1;
			$pdf->SetFont("arialn", "B", 8);
			$pdf->Cell(15, 5, "Fixed Rent", 0, "", "L");
			$pdf->Cell(30, 5, "Paid per", 0, "", "L");
			$pdf->Cell(15, 5, "From year", 0, "", "L");
			$pdf->Cell(15, 5, "From month", 0, "", "L");
			$pdf->Cell(15, 5, "From day", 0, "", "L");
			$pdf->Cell(15, 5, "To year", 0, "", "L");
			$pdf->Cell(15, 5, "To month", 0, "", "L");
			$pdf->Cell(15, 5, "To day", 0, "", "L");
			$pdf->Cell(15, 5, "Months", 0, "", "L");
			//$pdf->Cell(15, 5, "Tax Rate", 0, "", "L");
			$pdf->ln();
			
			$pdf->SetFont("arialn", "", 8);
		}
		
		$pdf->Cell(15, 5, $row_f["cer_fixed_rent_amount"], 0, "", "L");
		$pdf->Cell(30, 5, $units[$row_f["cer_fixed_rent_unit"]], 0, "", "L");
		$pdf->Cell(15, 5, $row_f["cer_fixed_rent_from_year"], 0, "", "L");
		$pdf->Cell(15, 5, $row_f["cer_fixed_rent_from_month"], 0, "", "L");
		$pdf->Cell(15, 5, $row_f["cer_fixed_rent_from_day"], 0, "", "L");
		$pdf->Cell(15, 5, $row_f["cer_fixed_rent_to_year"], 0, "", "L");
		$pdf->Cell(15, 5, $row_f["cer_fixed_rent_to_month"], 0, "", "L");
		$pdf->Cell(15, 5, $row_f["cer_fixed_rent_to_day"], 0, "", "L");
		$pdf->Cell(15, 5, $row_f["cer_fixed_rent_number_of_months"], 0, "", "L");
		//$pdf->Cell(15, 5, $row_f["cer_fixed_rent_tax_rate"], 0, "", "L");

		$pdf->ln();
	}

	
	// annual index on fixed rents
	$header_printed = 0;
	
	$res_f = mysql_query($sql_f) or dberror($sql_f);
	while($row_f = mysql_fetch_assoc($res_f))
	{
		if($row_f["cer_fixed_rent_index_rate"] > 0)
		{
			if($header_printed == 0)
			{
				$header_printed = 1;
				$pdf->SetFont("arialn", "B", 8);
				$pdf->Cell(187, 5, "Annual Index on Fixed Rents", 0, "", "L");
				
				$pdf->ln();

				$pdf->Cell(15, 5, "Fixed Rent", 0, "", "L");
				$pdf->Cell(15, 5, "From", 0, "", "L");
				$pdf->Cell(15, 5, "To", 0, "", "L");
				$pdf->Cell(15, 5, "Index %", 0, "", "L");
				$pdf->Cell(15, 5, "Starting on", 0, "", "L");
				$pdf->ln();
				
				$pdf->SetFont("arialn", "", 8);
			}
			
			$from = $row_f["cer_fixed_rent_from_day"] . "." . $row_f["cer_fixed_rent_from_month"] . "." . $row_f["cer_fixed_rent_from_year"];
			$to = $row_f["cer_fixed_rent_to_day"] . "." . $row_f["cer_fixed_rent_to_month"] . "." . $row_f["cer_fixed_rent_to_year"];
			$pdf->Cell(15, 5, $row_f["cer_fixed_rent_amount"], 0, "", "L");
			$pdf->Cell(15, 5, $from, 0, "", "L");
			$pdf->Cell(15, 5, $to, 0, "", "L");
			$pdf->Cell(15, 5, $row_f["cer_fixed_rent_index_rate"], 0, "", "L");
			$pdf->Cell(15, 5, to_system_date($row_f["cer_fixed_rent_index_startdate"]), 0, "", "L");

			$pdf->ln();
		}
	}


	// annual increas on fixed rents
	$header_printed = 0;
	
	$res_f = mysql_query($sql_f) or dberror($sql_f);
	while($row_f = mysql_fetch_assoc($res_f))
	{
		if($row_f["cer_fixed_rent_increas_rate"] > 0)
		{
			if($header_printed == 0)
			{
				$header_printed = 1;
				$pdf->SetFont("arialn", "B", 8);
				$pdf->Cell(187, 5, "Annual Increase on Fixed Rents", 0, "", "L");
				
				$pdf->ln();

				$pdf->Cell(15, 5, "Fixed Rent", 0, "", "L");
				$pdf->Cell(15, 5, "From", 0, "", "L");
				$pdf->Cell(15, 5, "To", 0, "", "L");
				$pdf->Cell(15, 5, "Increase %", 0, "", "L");
				$pdf->Cell(15, 5, "Starting on", 0, "", "L");
				$pdf->ln();
				
				$pdf->SetFont("arialn", "", 8);
			}
			
			$from = $row_f["cer_fixed_rent_from_day"] . "." . $row_f["cer_fixed_rent_from_month"] . "." . $row_f["cer_fixed_rent_from_year"];
			$to = $row_f["cer_fixed_rent_to_day"] . "." . $row_f["cer_fixed_rent_to_month"] . "." . $row_f["cer_fixed_rent_to_year"];
			$pdf->Cell(15, 5, $row_f["cer_fixed_rent_amount"], 0, "", "L");
			$pdf->Cell(15, 5, $from, 0, "", "L");
			$pdf->Cell(15, 5, $to, 0, "", "L");
			$pdf->Cell(15, 5, $row_f["cer_fixed_rent_increas_rate"], 0, "", "L");
			$pdf->Cell(15, 5, to_system_date($row_f["cer_fixed_rent_increas_startdate"]), 0, "", "L");

			$pdf->ln();
		}
	}


	// Turnover based rents
	$header_printed = 0;
	$sql_f = "select * from cer_rent_percent_from_sales " . 
		     " where cer_rent_percent_from_sale_cer_version = " . $version . 
		     " and cer_rent_percent_from_sale_project = " . dbquote(param("pid")) .
		     " order by cer_rent_percent_from_sale_from_year, cer_rent_percent_from_sale_from_month, cer_rent_percent_from_sale_from_day";
	
	
	$res_f = mysql_query($sql_f) or dberror($sql_f);
	while($row_f = mysql_fetch_assoc($res_f))
	{
		if($row_f["cer_rent_percent_from_sale_percent"] > 0)
		{
			if($header_printed == 0)
			{
				$header_printed = 1;
				$pdf->SetFont("arialn", "B", 8);
				$pdf->Cell(187, 5, "Total Annual Rent in % of Sales Values and Breakpoints", 0, "", "L");
				
				$pdf->ln();

				$pdf->Cell(15, 5, "Percent", 0, "", "L");
				$pdf->Cell(15, 5, "Breakpoint", 0, "", "L");
				$pdf->Cell(15, 5, "From", 0, "", "L");
				$pdf->Cell(15, 5, "To", 0, "", "L");
				$pdf->Cell(15, 5, "Months", 0, "", "L");
				//$pdf->Cell(15, 5, "Tax Rate", 0, "", "L");
				$pdf->Cell(15, 5, "API %", 0, "", "L");

				
				$pdf->ln();
				
				$pdf->SetFont("arialn", "", 8);
			}
			
			$from = $row_f["cer_rent_percent_from_sale_from_day"] . "." . $row_f["cer_rent_percent_from_sale_from_month"] . "." . $row_f["cer_rent_percent_from_sale_from_year"];
			$to = $row_f["cer_rent_percent_from_sale_to_day"] . "." . $row_f["cer_rent_percent_from_sale_to_month"] . "." . $row_f["cer_rent_percent_from_sale_to_year"];
			
			$pdf->Cell(15, 5, $row_f["cer_rent_percent_from_sale_amount"], 0, "", "L");
			$pdf->Cell(15, 5, $row_f["cer_rent_percent_from_sale_percent"], 0, "", "L");
			$pdf->Cell(15, 5, $from, 0, "", "L");
			$pdf->Cell(15, 5, $to, 0, "", "L");
			$pdf->Cell(15, 5, $row_f["cer_rent_percent_from_sale_number_of_months"], 0, "", "L");
			//$pdf->Cell(15, 5, to_system_date($row_f["cer_rent_percent_from_sale_tax_rate"]), 0, "", "L");
			$pdf->Cell(15, 5, to_system_date($row_f["cer_rent_percent_from_sale_passenger_rate"]), 0, "", "L");

			$pdf->ln();
		}
	}



	// Additional Rental Costs
	$header_printed = 0;
	$years = array();
	$cost_types = array();
	$amounts = array();
	
	

	$sql_f = "select * from cer_additional_rental_cost_amounts " . 
		     " left join additional_rental_cost_types on additional_rental_cost_type_id = cer_additional_rental_cost_amount_costtype_id " . 
		     " where cer_additional_rental_cost_amount_version	 = " . $version . 
		     " and cer_additional_rental_cost_amount_project = " . dbquote(param("pid")) .
		     " order by additional_rental_cost_type_name, cer_additional_rental_cost_amount_year";
		

	$res_f = mysql_query($sql_f) or dberror($sql_f);
	while($row_f = mysql_fetch_assoc($res_f))
	{
		
		if($row_f["cer_additional_rental_cost_amount_amount"] > 0)
		{
			
			$amounts[$row_f["additional_rental_cost_type_name"]][$row_f["cer_additional_rental_cost_amount_year"]] = $row_f["cer_additional_rental_cost_amount_amount"];

			if(!in_array($row_f["cer_additional_rental_cost_amount_year"], $years))
			{
				$years[] = $row_f["cer_additional_rental_cost_amount_year"];
			}

			if(!in_array($row_f["additional_rental_cost_type_name"], $cost_types))
			{
				$cost_types[] = $row_f["additional_rental_cost_type_name"];
			}




		}
	}


	$number_of_data_sets = ceil(count($years)/10);

	
	

	$counter = 1;
	$counter2 = 1;
	
	$data_set = 1;
	for($i=1;$i<=$number_of_data_sets;$i++)
	{
		
		if($header_printed == 0)
		{
			$header_printed = 1;
			$pdf->SetFont("arialn", "B", 8);
			$pdf->Cell(187, 5, "Amounts for Additional Rental Costs", 0, "", "L");
			$pdf->ln();
		}

		//print header
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(35, 5, "Cost type", 0, "", "L");
		for($j=$counter;$j<=($data_set*10);$j++)
		{
			if(array_key_exists($j-1, $years))
			{
				$pdf->Cell(15, 5, $years[$j-1], 0, "", "L");
			}
			$counter++;
		}
		
		$pdf->ln();

		//print data
		$pdf->SetFont("arialn", "", 8);
		
		foreach($cost_types as $key=>$cost_type_name)
		{
			$counter3 = $counter2;
			$pdf->Cell(35, 5, $cost_type_name, 0, "", "L");

			for($j=$counter3;$j<=($data_set*10);$j++)
			{
				if(array_key_exists($j-1, $years)
					and array_key_exists($cost_type_name, $amounts) 
					and array_key_exists($years[$j-1], $amounts[$cost_type_name]))
				{
					$pdf->Cell(15, 5, $amounts[$cost_type_name][$years[$j-1]], 0, "", "L");
				}
				$counter3++;
			}
			$pdf->ln();
		}

		$counter2 = $counter2+10;
		$data_set++;

	}



	// other relevant parameters
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "Other Relevant Parameters", 0, "", "L");
	$pdf->ln();

	$pdf->SetFont("arialn", "", 8);
	
	if($row["cer_basicdata_calculate_rent_avg"] == 1)
	{
		$parameter_info = '- Calculate average rent per year';
		$pdf->Cell(187, 5, $parameter_info, 0, "", "L");			
		$pdf->ln();
	}
	if($row["cer_basicdata_tob_from_net_sales"] == 1)
	{
		$parameter_info = '- Turnover based rents are to be calculated on the base of NET SALES VALUES';
		$pdf->Cell(187, 5, $parameter_info, 0, "", "L");			
		$pdf->ln();
	}
	else
	{
		$parameter_info = '- Turnover based rents are to be calculated on the base of GROSS SALES VALUES';
		$pdf->Cell(187, 5, $parameter_info, 0, "", "L");			
		$pdf->ln();
	}
	if($row["cer_basicdata_add_tob_rents"] == 1)
	{
		$parameter_info = '- Turnover based rents are to be paid in addition to fixed rents';
		$pdf->Cell(187, 5, $parameter_info, 0, "", "L");			
		$pdf->ln();
	}
	else
	{
		$parameter_info = "- Turnover based rents are  are calculated on a 'whatever is higher base'";
		$pdf->Cell(187, 5, $parameter_info, 0, "", "L");			
		$pdf->ln();
	}
	if($row["cer_basicdata_tob_from_breakpoint_difference"] == 1)
	{
		$parameter_info = '- Turnover based rents are to be calculated on the base of the DIFFERENCE between SALES VALUES and BREAKPOINTS';
		$pdf->Cell(187, 5, $parameter_info, 0, "", "L");			
		$pdf->ln();
	}
	else
	{
		$parameter_info = "- Turnover based rents are  are calculated on the base of SALES VALUES";
		$pdf->Cell(187, 5, $parameter_info, 0, "", "L");			
		$pdf->ln();
	}

	$pdf->ln();
	
}


?>