<?php
/********************************************************************

    cer_draft.php

    Add/Edit Business Plan Draft

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-02-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-02-16
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require "include/get_draft_functions.php";

check_access("has_access_to_cer_drafts");


/********************************************************************
    prepare all data needed
*********************************************************************/

$currencies = array();
$currencies_exchangerates = array();
$currencies_factors = array();
$user_country = 0;
$currency = 0;
$exchangerate = 0;
$factor = 0;
$title = "";
$basicdata = array();

if(param('did')) {
	param('id', param('did'));

	$currency = get_draft_currency(param("did"));
	$basicdata = get_draft_basicdata(param("did"));
	$title = $basicdata["cer_basicdata_title"];
	$user_country = $basicdata['cer_basicdata_country'];


	//interco is not needed anymore since we introduced into the revenues in 2013
	$result = set_interco_to_zero(param('did'));

	check_expenses(param("did"), $basicdata["cer_basicdata_firstyear"], $basicdata["cer_basicdata_lastyear"]);

	//build missing investment records
	$result = build_missing_draft_investment_records(param('did'));
}



//get user's currencies
if(param('cer_basicdata_country')) {
	$sql = 'select country_id, currency_id,currency_symbol, currency_exchange_rate, currency_factor from countries ' . 
		   'left join currencies on currency_id = country_currency ' .
		   'where currency_id = 1 or country_id = ' . param('cer_basicdata_country');

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$currencies[$row['currency_id']] = $row['currency_symbol'];
		$currencies_exchangerates[$row['currency_id']] = $row['currency_exchange_rate'];
		$currencies_factors[$row['currency_id']] = $row['currency_factor'];
	
	}
}
elseif(param('did'))
{
	$basicdata = get_draft_basicdata(param("did"));
	$sql = 'select cer_basicdata_title, cer_basicdata_legal_type, cer_basicdata_country, cer_basicdata_currency, ' . 
		   'cer_basicdata_exchangerate, cer_basicdata_factor, ' .
		   'currency_id, currency_symbol ' .
		   'from cer_drafts ' . 
		   'left join currencies on currency_id = cer_basicdata_currency ' . 
		   'where cer_basicdata_id = ' . param("did");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$currencies[$row['currency_id']] = $row['currency_symbol'];
		$currencies_exchangerates[$row['currency_id']] = $row['cer_basicdata_exchangerate'];
		$currencies_factors[$row['currency_id']] = $row['cer_basicdata_factor'];
	}
}
elseif(id() == 0)
{
	$sql = 'select country_id, currency_id,currency_symbol, currency_exchange_rate, currency_factor, address_country from users ' . 
		   'left join addresses on address_id = user_address ' .
		   'left join countries on country_id = address_country ' . 
		   'left join currencies on currency_id = country_currency ' .
		   'where user_id = ' . user_id();

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$currencies[$row['currency_id']] = $row['currency_symbol'];
		$currencies_exchangerates[$row['currency_id']] = $row['currency_exchange_rate'];
		$currencies_factors[$row['currency_id']] = $row['currency_factor'];
		$user_country = $row['address_country'];

		if($row['country_id'] == $row['address_country']) {
			$currency = $row['currency_id'];
			$exchangerate = $row['currency_exchange_rate'];
			$factor = $row['currency_factor'];
		}
	}

	$currencies[1] = 'CHF';
	$currencies_exchangerates[1] = 1;
	$currencies_factors[1] = 1;
}



$months = array();
for($i=1;$i<13;$i++)
{
	$months[$i] = $i;
}

$years = array();

if(id() > 0) {
	$s = date("Y")-10;
}
else
{
	$s = date("Y")-1;
}
$l = $s+31;
for($i=$s;$i<$l;$i++)
{
	$years[$i] = $i;
}

$sql_countires = 'select country_id, country_name from countries order by country_name';
$sql_cost_types = "select * from project_costtypes where project_costtype_id < 3";
$sql_pos_types = "select * from postypes where postype_id < 4";



$units = array();
$units[1] = "Square Feet (sqf)";
$units[2] = "Square Meters (sqm)";


$form = new Form("cer_drafts", "Business Plan Draft");

$form->add_section("Draft");
$form->add_hidden("did", param("did"));
$form->add_hidden("id", param("id"));
$form->add_hidden("cer_basicdata_user_id", user_id());
$form->add_edit("cer_basicdata_title", "Title of the Draft*", NOTNULL);
$form->add_list("cer_basicdata_legal_type", "Legal Type*", $sql_cost_types, NOTNULL);
$form->add_list("cer_basicdata_pos_type", "POS Type", $sql_pos_types, 0);

$form->add_list("cer_basicdata_country", "Country*", $sql_countires, NOTNULL|SUBMIT, $user_country);

$form->add_section("Business Plan Period");
$form->add_list("cer_basicdata_firstyear", "First Year*", $years, NOTNULL);
$form->add_list("cer_basicdata_firstmonth", "Month of First Year*", $months, NOTNULL);
$form->add_list("cer_basicdata_lastyear", "Last Year*", $years, NOTNULL);
$form->add_list("cer_basicdata_lastmonth", "Month of Last Year*", $months, NOTNULL);


$form->add_section("Currency and Exchange Rate");
$form->add_list("cer_basicdata_currency", "Currency*", $currencies, NOTNULL | SUBMIT, $currency);
$form->add_edit("cer_basicdata_exchangerate", "Exchange Rate*", NOTNULL, $exchangerate, TYPE_DECIMAL, 10,6);
$form->add_hidden("cer_basicdata_factor", $factor);

$form->add_section("Inhouse Surfaces");
$form->add_comment("Please indicate the inhouse surface information. <strong>First read the INFOBUTTONS</strong>!");
$form->add_list("unit_of_measurement", "Unit of measurement", $units, NOTNULL, 2);

//$form->add_edit("cer_basicdata_grosssqms", "Gross Surface in <span id='um00'>sqms</span>*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 3, "grosssqm");
$form->add_hidden("cer_basicdata_grosssqms");

$form->add_edit("cer_basicdata_totalsqms", "Total Surface in <span id='um01'>sqms</span>*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 3, "totalsqm");
$form->add_edit("cer_basicdata_sqms", "Sales Surface in <span id='um02'>sqms</span>*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 3, "retailsqm");
$form->add_edit("cer_basicdata_backofficesqms", "Other Surface in <span id='um03'>sqms</span>", 0, "", TYPE_DECIMAL, 8, 2, 3, "backofficesqm");


$form->add_section("Rental Information");
$form->add_list("cer_basicdata_lease_type", "Lease Type",
				"select poslease_type_id, poslease_type_name from poslease_types order by poslease_type_id", 0);
$form->add_edit("cer_basicdata_lease_startdate", "Start Date", NOTNULL, "", TYPE_DATE);
$form->add_edit("cer_basicdata_lease_enddate", "Expiry Date", NOTNULL, "", TYPE_DATE);



$form->add_button("save", "Save");


//if(count($basicdata)> 0 and (user_id() == $basicdata['cer_basicdata_user_id'])) {

if(id()) {
	$form->add_button("delete_draft", "Delete Draft");
}
$form->add_button("back", "Back");
//}


$form->populate();
$form->process();


if($form->button("save")) {
	
	if($form->validate())
	{
		if(id() > 0) {
			param('did', id());
		}


		if($form->value("unit_of_measurement") == 1)
		{
			//$form->value("cer_basicdata_grosssqms", 0.09290304*$form->value("cer_basicdata_grosssqms"));
			$form->value("cer_basicdata_totalsqms", 0.09290304*$form->value("cer_basicdata_totalsqms"));
			$form->value("cer_basicdata_sqms", 0.09290304*$form->value("cer_basicdata_sqms"));
			$form->value("cer_basicdata_backofficesqms", 0.09290304*$form->value("cer_basicdata_backofficesqms"));
		}



		$form->save();

		if(param('did') == 0)
		{
			$draft_id = mysql_insert_id();
		}
		else
		{
			$draft_id = param('did');
		}

				
		//get standardparameters
		$sparams = array();
		$sql = "select * from cer_standardparameters";
		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$sparams[$row["cer_standardparameter_id"]] = $row["cer_standardparameter_value"];
		}

		//get inflationrates
		$inflationrates = array();
		$sql = "select * from cer_inflationrates " . 
			   "where inflationrate_country = " . $form->value('cer_basicdata_country') . 
			   " order by inflationrate_year";

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$inflationrates[$row["inflationrate_year"]] = $row["inflationrate_rate"];
		}

		$inflationrates = serialize($inflationrates);
		
		//get interestrates
		$interestrates = array();

		$sql = "select * " .
			   "from cer_interestrates where interestrate_country = " . $form->value('cer_basicdata_country')  . 
			   " order by interestrate_year ASC";

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$interestrates[$row["interestrate_year"]] = $row["interestrate_rate"];
		}

		$interestrates = serialize($interestrates);

		$fields = array();


		$fields[] = "cer_basicdata_dicount_rate  = " . dbquote($sparams[1]);
		$fields[] = "cer_bascidata_liquidation_keymoney = " . dbquote($sparams[2]);
		$fields[] = "cer_basicdata_liquidation_deposit = " . dbquote($sparams[3]);
		$fields[] = "cer_bascidata_liquidation_stock = " . dbquote($sparams[4]);
		$fields[] = "cer_bascicdate_liquidation_staff = " . dbquote($sparams[5]);
		$fields[] = "cer_basicdata_inflationrates = " . dbquote($inflationrates);
		$fields[] = "cer_basic_data_interesrates = " . dbquote($interestrates);


		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update cer_drafts set " . join(", ", $fields) . " where cer_basicdata_id = " . $draft_id;
		mysql_query($sql) or dberror($sql);

		
		//build missing investment records
		$result = build_missing_draft_investment_records($draft_id);

		//build missing revenue_records
		if($form->value('cer_basicdata_firstyear') > 0 and $form->value('cer_basicdata_lastyear') > 0) {
			
			check_revenues($draft_id, $form->value('cer_basicdata_firstyear'), $form->value('cer_basicdata_lastyear'));
			check_expenses($draft_id, $form->value('cer_basicdata_firstyear'), $form->value('cer_basicdata_lastyear'));
			check_stocks($draft_id, $form->value('cer_basicdata_firstyear'), $form->value('cer_basicdata_lastyear'));
			check_paymentterms($draft_id, $form->value('cer_basicdata_firstyear'), $form->value('cer_basicdata_lastyear'));

		}


		//update additional rental costs in case poslease dates have changed
		
		$result = calculate_additional_rental_costs($draft_id, from_system_date($form->value("cer_basicdata_lease_startdate")),  from_system_date($form->value("cer_basicdata_lease_enddate")));
		
		$form->message("Your data has bee saved.");

		$link = "cer_draft.php?did=" . $draft_id;
		redirect($link);
	}
}
elseif($form->button('cer_basicdata_currency')) {

	if($form->value('cer_basicdata_currency')) {
		$form->value('cer_basicdata_exchangerate', $currencies_exchangerates[$form->value('cer_basicdata_currency')]);
		$form->value('cer_basicdata_factor', $currencies_factors[$form->value('cer_basicdata_currency')]);
	}
	else
	{
		$form->value('cer_basicdata_exchangerate', '');
		$form->value('cer_basicdata_factor', '');
	}
}
elseif($form->button('cer_basicdata_country')) {
	$sql = 'select * from countries ' . 
		   'left join currencies on currency_id = country_currency ' . 
		   'where country_id = ' . $form->value('cer_basicdata_country');
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->value('cer_basicdata_currency', $row['currency_id']);
		$form->value('cer_basicdata_exchangerate', $row['currency_exchange_rate']);
		$form->value('cer_basicdata_factor', $row['currency_factor']);
	}
}
elseif($form->button('delete_draft')) {
	
	//delete files
	$sql = "select * from cer_draft_files where cer_draft_file_draft = " . param('did');
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		unlink($_SERVER["DOCUMENT_ROOT"] . $row["cer_draft_file_path"]);
	}
	
	
	$sql = 'Delete from cer_drafts where cer_basicdata_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$sql = 'Delete from cer_draft_expenses where cer_expense_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	//new additional rental costs
	$sql = 'Delete from cer_draft_additional_rental_costs where cer_additional_rental_cost_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	
	$sql = 'Delete from cer_draft_additional_rental_cost_amounts where cer_additional_rental_cost_amount_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	//end new additional rental costs

	$sql = 'Delete from cer_draft_investments where cer_investment_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$sql = 'Delete from cer_draft_paymentterms where cer_paymentterm_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$sql = 'Delete from cer_draft_revenues where cer_revenue_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$sql = 'Delete from cer_draft_salaries where cer_salary_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$sql = 'Delete from cer_draft_stocks where cer_stock_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$sql = 'Delete from cer_draft_rent_percent_from_sales where cer_rent_percent_from_sale_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$sql = 'Delete from cer_draft_fixed_rents where cer_fixed_rent_draft_id =' . param('did');
	$res = mysql_query($sql) or dberror($sql);

	$link = '/cer/cer_drafts.php';
	redirect($link);
	

}

$page = new Page("cer_drafts");
require "include/draft_page_actions.php";
$page->header();



$page->title(id() ? $title . ": General Information" : "Add Business Plan Draft: General Information");

if(id() > 0) {
	require_once("include/tabs_draft2016.php");
}

$form->render();

?>
<!--
<div id="grosssqm" style="display:none;">
    Please indicate the Gross Surface in units of measurement <strong>rented by Tissot</strong><br />incl. corridors and other public areas, back office, toilets etc.
</div> 
-->
<div id="totalsqm" style="display:none;">
    Please indicate the total surface in units of measurement <strong>occupied by Tissot</strong><br />incl. back office, toilets etc.
</div> 
<div id="retailsqm" style="display:none;">
    Please indicate the surface in units of measurement <strong>occupied by Tissot</strong><br />used for sales purposes (without back office, toiletsetc.).
</div> 
<div id="backofficesqm" style="display:none;">
    Please indicate the total other surface in units of measurement <strong>occupied by Tissot</strong><br />like back office, toilets, stock areas, etc.
</div> 

<script language="Javascript">

	function round_decimal(num,decimals){
		return Math.round(num*Math.pow(10,decimals))/Math.pow(10,decimals);
	}
	
	$(document).ready(function(){
		  $("#unit_of_measurement" ).change(function() {
				
				if($("#unit_of_measurement" ).val() == 1)
				{
					//$("#um00").html("<strong> sqf</strong>");
					$("#um01").html("<strong> sqf</strong>");
					$("#um02").html("<strong> sqf</strong>");
					$("#um03").html("<strong> sqf</strong>");
					
					//$("#cer_basicdata_grosssqms").val(round_decimal(10.7639104*$("#cer_basicdata_grosssqms").val(), 2));
					$("#cer_basicdata_totalsqms").val(round_decimal(10.7639104*$("#cer_basicdata_totalsqms").val(), 2));
					$("#cer_basicdata_sqms").val(round_decimal(10.7639104*$("#cer_basicdata_sqms").val(), 2));
					$("#cer_basicdata_backofficesqms").val(round_decimal(10.7639104*$("#cer_basicdata_backofficesqms").val(), 2));

				}
				else
			    {
					//$("#um00").html("<strong> sqms</strong>");
					$("#um01").html("<strong> sqms</strong>");
					$("#um02").html("<strong> sqms</strong>");
					$("#um03").html("<strong> sqms</strong>");
					

					//$("#cer_basicdata_grosssqms").val(round_decimal(0.09290304*$("#cer_basicdata_grosssqms").val(), 2));
					$("#cer_basicdata_totalsqms").val(round_decimal(0.09290304*$("#cer_basicdata_totalsqms").val(), 2));
					$("#cer_basicdata_sqms").val(round_decimal(0.09290304*$("#cer_basicdata_sqms").val(), 2));
					$("#cer_basicdata_backofficesqms").val(round_decimal(0.09290304*$("#cer_basicdata_backofficesqms").val(), 2));
				}
		  });

	});

</script>

<?php

$page->footer();

?>