<?php
/********************************************************************

    project_investment_pre_approval.php

    Application Form: investment pre approval
    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2018-10-09
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2018-10-09
    Version:        1.0.0

    Copyright (c) 2018, Tissot, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";
require_once "../shared/project_cost_functions.php";

check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/


$currency = get_cer_currency(param("pid"));

//build missing investment records
$result = build_missing_cer_investment_records(param("pid"));

/********************************************************************
    Create Project Milestones
*********************************************************************/ 
$new_cer_version = 0;

$sql = "select * from milestones " .
	   "where milestone_active = 1 " .
	   "order by milestone_code ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$sql_m = "select count(project_milestone_id) as num_recs from project_milestones " .
		     "where project_milestone_project = " . $project["project_id"] . 
	         "   and project_milestone_milestone = " . $row["milestone_id"];
	
	
	$res_m = mysql_query($sql_m) or dberror($sql_m);
	$row_m = mysql_fetch_assoc($res_m);

	if($row_m["num_recs"] == 0)
	{
		$fields = array();
		$values = array();

		$fields[] = "project_milestone_project";
		$values[] = $project["project_id"];

		$fields[] = "project_milestone_milestone";
		$values[] = $row["milestone_id"];
		
		$fields[] = "date_created";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "date_modified";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		if($row["milestone_is_inr03_milestone"] == 1 and $project["project_cost_type"] == 1)
		{
			$sql = "insert into project_milestones (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);
		}
		elseif($row["milestone_is_inr03_milestone"] == 0)
		{
			$sql = "insert into project_milestones (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);
		}
	}
}

$pre_approval = get_project_milestone(param("pid"), 40);

//get investments

$keymoney = get_pos_intangibles(param("pid"), 15);
$goodwill = get_pos_intangibles(param("pid"), 17);
$construction = get_pos_intangibles(param("pid"), 1);
$fixturing = get_pos_intangibles(param("pid"), 3);
$architectural = get_pos_intangibles(param("pid"), 5);
$equipment = get_pos_intangibles(param("pid"), 7);
$deposit = get_pos_intangibles(param("pid"), 9);
$other1 = get_pos_intangibles(param("pid"), 11);
$other2 = get_pos_intangibles(param("pid"), 13);
$landlord_contribution = get_pos_intangibles(param("pid"), 19);
$dismantling = get_pos_intangibles(param("pid"), 18);
$transportation = get_pos_intangibles(param("pid"), 20);



$total_landlord_contribution = 0;
$sql = "select * from cer_investments " .
       "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
	   "where posinvestment_type_id = 19 " .
	   " and cer_investment_project = " . param("pid") . 
	   " and cer_investment_cer_version = 0 ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$total_landlord_contribution = $total_landlord_contribution + $row["cer_investment_amount_cer_loc"];
}


$cer_totals = 0;

$sql = "select cer_investment_amount_cer_loc from cer_investments " .
       "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
	   "where posinvestment_type_intangible = 0 and posinvestment_type_id not in (9,19) " .
	   " and cer_investment_project = " . param("pid") . 
	   " and cer_investment_cer_version = 0 ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$cer_totals = $cer_totals + $row["cer_investment_amount_cer_loc"];
}


$keymoney_sqm = "";
$goodwill_sqm = "";
$construction_sqm = "";
$fixturing_sqm = "";
$architectural_sqm = "";
$equipment_sqm = "";
$deposit_sqm = "";
$other1_sqm = "";
$other2_sqm = "";
$landlord_contribution_sqm = "";
$dismantling_sqm = "";
$transportation_sqm = "";
$cer_totals_sqm = "";



if($project["project_cost_totalsqms"] > 0) {
	$keymoney_sqm = " / " . round($keymoney["cer_investment_amount_cer_loc"]/$project["project_cost_totalsqms"], 2);
	$goodwill_sqm = " / " . round($goodwill["cer_investment_amount_cer_loc"]/$project["project_cost_totalsqms"], 2);
	$construction_sqm = " / " . round($construction["cer_investment_amount_cer_loc"]/$project["project_cost_totalsqms"], 2);
	$fixturing_sqm = " / " . round($fixturing["cer_investment_amount_cer_loc"]/$project["project_cost_totalsqms"], 2);
	$architectural_sqm = " / " . round($architectural["cer_investment_amount_cer_loc"]/$project["project_cost_totalsqms"], 2);
	$equipment_sqm = " / " . round($equipment["cer_investment_amount_cer_loc"]/$project["project_cost_totalsqms"], 2);
	$deposit_sqm = " / " . round($deposit["cer_investment_amount_cer_loc"]/$project["project_cost_totalsqms"], 2);
	$other1_sqm = " / " . round($other1["cer_investment_amount_cer_loc"]/$project["project_cost_totalsqms"], 2);
	$other2_sqm = " / " . round($other2["cer_investment_amount_cer_loc"]/$project["project_cost_totalsqms"], 2);
	$landlord_contribution_sqm = " / " . round($landlord_contribution["cer_investment_amount_cer_loc"]/$project["project_cost_totalsqms"], 2);
	$dismantling_sqm = " / " . round($dismantling["cer_investment_amount_cer_loc"]/$project["project_cost_totalsqms"], 2);
	$transportation_sqm = " / " . round($transportation["cer_investment_amount_cer_loc"]/$project["project_cost_totalsqms"], 2);
	$cer_totals_sqm = " / " . round($cer_totals/$project["project_cost_totalsqms"], 2);
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

include("include/project_head.php");
$form->add_hidden("pid", param("pid"));

$form->add_section("Total Investments");
$form->add_label("total_investment", "Investments in " . $currency["symbol"], 0, number_format($cer_totals,0, ".", "'") . $cer_totals_sqm);

$form->add_section("Key Money Goodwill and Deposit");
$form->add_label("keymoney", $keymoney["posinvestment_type_name"] ." in " . $currency["symbol"], 0,  number_format($keymoney["cer_investment_amount_cer_loc"],0, ".", "'") . $keymoney_sqm);

$form->add_label("goodwill", $goodwill["posinvestment_type_name"] ." in " . $currency["symbol"], 0,  number_format($goodwill["cer_investment_amount_cer_loc"],0, ".", "'") . $goodwill_sqm);

$form->add_label("deposit", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0,  number_format($deposit["cer_investment_amount_cer_loc"],0, ".", "'") . $deposit_sqm);


$form->add_section("Investments");
$form->add_label("construction", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0,  number_format($construction["cer_investment_amount_cer_loc"],0, ".", "'") . $construction_sqm);

$form->add_label("fixturing", $fixturing["posinvestment_type_name"] . " in " . $currency["symbol"], 0,  number_format($fixturing["cer_investment_amount_cer_loc"],0, ".", "'") . $fixturing_sqm);

$form->add_label("architectural", $architectural["posinvestment_type_name"] . " in " . $currency["symbol"], 0,  number_format($architectural["cer_investment_amount_cer_loc"],0, ".", "'") . $architectural_sqm);

$form->add_label("equipment", $equipment["posinvestment_type_name"] ." in " . $currency["symbol"], 0,  number_format($equipment["cer_investment_amount_cer_loc"],0, ".", "'") . $equipment_sqm);

$form->add_label("other1", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0,  number_format($other1["cer_investment_amount_cer_loc"],0, ".", "'") . $other1_sqm);

$form->add_label("transportation", $transportation["posinvestment_type_name"] ." in " . $currency["symbol"], 0,  number_format($transportation["cer_investment_amount_cer_loc"],0, ".", "'") . $transportation_sqm);

$form->add_label("other2", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0,  number_format($other2["cer_investment_amount_cer_loc"],0, ".", "'") . $other2_sqm);

$form->add_section("Dismantling Costs");
$form->add_label("dismantling", $dismantling["posinvestment_type_name"] ." in " . $currency["symbol"], 0,  number_format($dismantling["cer_investment_amount_cer_loc"],0, ".", "'") . $dismantling_sqm);

$form->add_section("Contributions");
$form->add_label("cer_basicdata_franchsiee_investment_share", "Business Partner's Contribution in % is", 0, $cer_basicdata["cer_basicdata_franchsiee_investment_share"]);

$form->add_label("total_landlord_contribution", $landlord_contribution["posinvestment_type_name"] ." in " . $currency["symbol"], 0,  number_format($landlord_contribution["cer_investment_amount_cer_loc"],0, ".", "'") . $landlord_contribution_sqm);

/*
$form->add_section("Residual Value of Fixed Assets in " . $currency["symbol"] . " (only in case of Renovation or Take Over/Renovation Projects)");
$form->add_label("cer_basicdata_residual_value", "Residual Value in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_residual_value"]);

$form->add_section("Residual Value of Keymoney in " . $currency["symbol"] . " (only in case of Lease Renewal or Renovation or Take Over/Renovation Projects)");

$form->add_label("cer_basicdata_residualkeymoney_value", "Residual Value in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_residualkeymoney_value"]);

*/

$form->add_section("Investment Pre-Approval");
$form->add_comment("Please approve the above investments. The client will be informed by email accordingly.");

if($pre_approval['project_milestone_date'] == NULL
    or $pre_approval['project_milestone_date'] == '0000-00-00') {
	$form->add_checkbox("investments_approved", "Yes, I approve the above investments", "", 0, "Approval");
	$form->add_button("approve", "Approve Investments");
}
else {
	$form->add_label("approved", "Pre-Approval", 0, "Approved on " . to_system_date($pre_approval['project_milestone_date']) . " by " . $pre_approval['user_firstname'] . " " . $pre_approval['user_name']);
}

$form->populate();
$form->process();

if($form->button("approve")) {

	if(param("investments_approved") == 1) {
		
		$fields = array();
		$value = date("Y-m-d");
		$fields[] = "project_milestone_date = " . dbquote($value);

		$value = user_id();
		$fields[] = "project_milestone_date_enterd_by_user= " . dbquote($value);

		$value = user_login();
		$fields[] = "user_created = " . dbquote($value);

		$value = date("Y-m-d H:i:s");
		$fields[] = "date_created = " . dbquote($value);
		
		$value = user_login();
		$fields[] = "user_modified = " . dbquote($value);

		$value = date("Y-m-d H:i:s");
		$fields[] = "date_modified = " . dbquote($value);
		
		$sql_m = "update project_milestones set " . join(", ", $fields) . 
			   " where project_milestone_project = " . param("pid") . 
			   " and project_milestone_milestone = 40";

		mysql_query($sql_m) or dberror($sql_m);


		//send mail to client
		$link = APPLICATION_URL . "/user/project_task_center.php?pid=" . param("pid");
		$actionmail = new ActionMail(156);
		$actionmail->setParam('projectID', param("pid"));
		$actionmail->setParam('link', $link);
		$actionmail->send();


		//send mail to design supervisor
		$link = APPLICATION_URL . "/user/project_task_center.php?pid=" . param("pid");
		$actionmail = new ActionMail(157);
		$actionmail->setParam('projectID', param("pid"));
		$actionmail->setParam('link', $link);
		$actionmail->send();
					
		$link = "project_investment_pre_approval.php?pid=" . param("pid");
		redirect($link);
	}
	else {
		$form->error("Please check the approval checkbox!");
	}
}

/********************************************************************
	render page
*********************************************************************/
$page = new Page("cer_projects");
require "include/project_page_actions.php";
$page->header();

$page->title("Pre-Approval of Investments");
$form->render();

$page->footer();

?>