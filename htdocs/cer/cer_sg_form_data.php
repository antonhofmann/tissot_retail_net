<?php
/********************************************************************

    cer_sg_form_data.php

    Get all data needed in forms

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2018-01-05
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2018-01-05
    Version:        1.0.0

    Copyright (c) 2018, Tissot, All Rights Reserved.
*********************************************************************/

if(isset($write_tmp_file) and $write_tmp_file == true) {
	
}
else {
	require "include/get_functions.php";
	require "../shared/func_posindex.php";
}

check_access("has_access_to_cer");


require "include/get_project.php";
require_once "../shared/project_cost_functions.php";
require "include/financial_functions.php";


$globalPageNumber = 0;
// data needed
$project = get_project(param("pid"));
$order_number  = $project["project_order"];

$client_address = get_address($project["order_client_address"]);



if($project["possubclass_name"]) {
	$postype = $project["projectkind_name"] . ", " . $project["project_costtype_text"] . ", " . $project["postype_name"] . ", " . $project["possubclass_name"];
}
else {
	$postype = $project["projectkind_name"] . ", " . $project["project_costtype_text"] . ", " . $project["postype_name"];
}


$cer_version = 0;
$ln_version = 0;
$ln_signatures = "";
if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}



if($cer_version == 0)
{
	$version_date = date("d.m.Y H:i:s");
}
else
{
	$version_date = to_system_date($cer_basicdata['date_created']);
}

$tmp_cer_version = $cer_version;
$tmp_ln_version = $ln_version;

//get date of latest ln version
$sql = "select ln_basicdata_version_cer_version from ln_basicdata where ln_basicdata_project = " . dbquote(param("pid")) . 
" and ln_basicdata_submitted is not null and ln_basicdata_submitted <> '0000-00-00' " .
" order by ln_basicdata_version desc";

//for testing only
//$sql = "select ln_basicdata_version_cer_version from ln_basicdata where ln_basicdata_project = " . dbquote(param("pid")) . 
//" order by ln_basicdata_version desc";


$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$cer_version = $row["ln_basicdata_version_cer_version"];

	include("include/in_financial_data.php");
	$ln_amounts = $amounts;
	$ln_exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
	$ln_exchange_rate_factor = $cer_basicdata["cer_basicdata_factor"];
	$ln_investment_total = $investment_total;
	$ln_intagible_amount = $intagible_amount;
	$ln_totals = $cer_totals;

	$ln_net_investment_total = $investment_total +$landloard_contribution + $intagible_amount + $amounts[9]  + $amounts[13];

	$ln_landloard_contribution = $landloard_contribution;
	
	$ln_sales_units_watches_values = $sales_units_watches_values;
	$ln_sales_units_jewellery_values = $sales_units_jewellery_values;
	$ln_total_net_sales_values = $total_net_sales_values;
	$ln_operating_income02_values = $operating_income02_values;
	$ln_operating_income02_80_percent_values = $operating_income02_80_percent_values;

	$ln_number_of_months_first_year = $number_of_months_first_year;
	$ln_number_of_months_last_year = $number_of_months_last_year;
	$ln_wholesale_margin = $cer_basicdata["cer_basicdata_wholesale_margin"];

	$total_gross_sales_values_ln = $total_gross_sales_values;

}
else
{
	$ln_amounts = array();
	$ln_exchange_rate = 0;
	$ln_exchange_rate_factor = 0;
	$ln_investment_total = 0;
	$ln_net_investment_total = 0;
	$ln_landloard_contribution = 0;
	$ln_intagible_amount = 0;
	$ln_totals =0;
	$ln_sales_units_watches_values = array();
	$ln_sales_units_jewellery_values = array();
	$ln_total_net_sales_values = array();
	$ln_operating_income02_values = array();
	$ln_operating_income02_80_percent_values = array();

	$ln_number_of_months_first_year = 0;
	$ln_number_of_months_last_year = 0;
	$ln_wholesale_margin = "";

	$total_gross_sales_values_ln = 0;
}


//actual cer
$cer_version = $tmp_cer_version;
$ln_version = $tmp_ln_version;

$cer_basicdata = get_cer_basicdata(param("pid"), $cer_version);
$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
$exchange_rate_factor = $cer_basicdata["cer_basicdata_factor"];
$cer_wholesale_margin = $cer_basicdata["cer_basicdata_wholesale_margin"];
include("include/in_financial_data.php");	

//set defaul values
$legal_entity_name = "";
$project_name = "";
$project_manager = "";
$requested_amount = "";
$budget_amount = "";
$currency_symbol = "";
$investment_amount = "";
$landlord_amount = "";
$net_investment_amount = "";
$project_start = "";
$net_present_value = "";
$key_money = "";
$project_end = "";
$pay_back_period = "";
$deposit = "";
$post_compl_review = "";
$internal_reate_of_return ="";
$other_cost = "";
$project_kind = "";


$description = "";
$strategy = "";
$investment = "";
$benefits = "";
$alternative = "";
$risks = "";

$name1 = "";
$name2 = "";
$name3 = "";
$name4 = "";
$name5 = "";
$name6 = "";
$name7 = "";
$name8 = "";
$name9 = "";

$date1 = "";
$date2 = "";
$date3 = "";
$date4 = "";
$date5 = "";
$date6 = "";
$date7 = "";
$date8 = "";
$date9 = "";


$protocol_number1 = "";
$protocol_number1 = "";
$cer_number = "";

$opening_date = "";
$takeover_date = "";
$takeover = false;

$project_kind_new = "";
$project_kind_renovation = "";
$project_kind_relocation = "";


$approval_name1 = $cer_basicdata["cer_basicdata_approvalname1"];
$approval_name2 = $cer_basicdata["cer_basicdata_approvalname2"];
$approval_name3 = $cer_basicdata["cer_basicdata_approvalname3"];
$approval_name4 = $cer_basicdata["cer_basicdata_approvalname4"];
$approval_name5 = $cer_basicdata["cer_basicdata_approvalname5"];
$approval_name6 = $cer_basicdata["cer_basicdata_approvalname6"];
$approval_name7 = $cer_basicdata["cer_basicdata_approvalname7"];
$approval_name8 = $cer_basicdata["cer_basicdata_approvalname8"];
$approval_name9 = $cer_basicdata["cer_basicdata_approvalname9"];
$approval_name10 = $cer_basicdata["cer_basicdata_approvalname10"];

$approval_name11 = $cer_basicdata["cer_summary_in01_sig01"];
$approval_name12 = $cer_basicdata["cer_summary_in01_sig02"];
$approval_name13 = $cer_basicdata["cer_summary_in01_sig03"];

// get all data needed from cer_basic_data

$sql = "select * from cer_summary " . 
       "where cer_summary_project = " . param("pid") . 
	   " and cer_summary_cer_version = " . $cer_version;
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{

	$description = $row["cer_summary_in01_description"];
	$strategy = $row["cer_summary_in01_strategy"];
	$investment = $row["cer_summary_in01_investment"];
	$benefits = $row["cer_summary_in01_benefits"];
	$alternative = $row["cer_summary_in01_alternative"];
	$risks = $row["cer_summary_in01_risks"];

	$name1 = $row["cer_summary_in01_sig01"];
	$name2 = $row["cer_summary_in01_sig02"];
	$name3 = $row["cer_summary_in01_sig03"];
	$name4 = $row["cer_summary_in01_sig04"];
	$name5 = $row["cer_summary_in01_sig05"];
	$name6 = $row["cer_summary_in01_sig06"];
	$name7 = $row["cer_summary_in01_sig07"];
	$name8 = $row["cer_summary_in01_sig08"];
	$name9 = $row["cer_summary_in01_sig09"];

	$name7 = $cer_basicdata["cer_basicdata_approvalname11"];

	$date1 = to_system_date($row["cer_summary_in01_date01"]);
	$date2 = to_system_date($row["cer_summary_in01_date02"]);
	$date3 = to_system_date($row["cer_summary_in01_date03"]);
	$date4 = to_system_date($row["cer_summary_in01_date04"]);
	$date5 = to_system_date($row["cer_summary_in01_date05"]);
	$date6 = to_system_date($row["cer_summary_in01_date06"]);
	$date7 = to_system_date($row["cer_summary_in01_date07"]);
	$date8 = to_system_date($row["cer_summary_in01_date08"]);
	$date9 = to_system_date($row["cer_summary_in01_date09"]);


	$protocol_number1 = $row["cer_summary_in01_prot01"];
	$protocol_number2 = $row["cer_summary_in01_prot02"];
	$cer_number = $row["cer_summary_in01_cernr"];

	
	if($row["cer_summary_in01_review_date"] and $row["cer_summary_in01_review_date"] != '0000-00-00')
	{
		$post_compl_review = substr($row["cer_summary_in01_review_date"], 5,2) . "/" . substr($row["cer_summary_in01_review_date"], 0,4);
	}
	
}

// get all data needed from project
$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
       "left join users on user_id = project_retail_coordinator " . 
	   "where project_id = " . param("pid");


$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$legal_entity_name = $row["address_company"];
	
	$project_name = $row["order_shop_address_company"]. ", " . $row["order_shop_address_place"] . ", " . $row["country_name"];


	$project_manager = $row["user_firstname"] . " " . $row["user_name"];
	
	$project_kind = $row["projectkind_name"];

	if($row["project_projectkind"] == 4)
	{
		$takeover = true;
	}


	
	$factor = $cer_basicdata['cer_basicdata_factor'];
	if(!$cer_basicdata['cer_basicdata_factor'])
	{
		$factor = 1;
	}

	$exchange_rate = 1;
	if($cer_basicdata['cer_basicdata_exchangerate'] > 0)
	{
		$exchange_rate = $cer_basicdata['cer_basicdata_exchangerate'];
	}
	
	
	$budget_amount = $row["project_approximate_budget"]/$cer_basicdata['cer_basicdata_exchangerate']*$factor;

	$budget_amount = number_format($budget_amount, 0, "", "'");
	$project_start = substr($row["order_date"], 5,2) . "/" . substr($row["order_date"], 0,4);

	$tmp = date('Y-m-d H:i:s', strtotime($row["project_real_opening_date"] . ' + 90 day'));

	
	$project_end = substr($tmp, 5,2) . "/" . substr($tmp, 0,4);
	$order_number = $row["project_order"];
	$address_country = $row["address_country"];

	
	$opening_date = to_system_date($row["project_real_opening_date"]);

	if($row["project_planned_takeover_date"] != NULL and $row["project_planned_takeover_date"] != '0000-00-00')
	{
		$takeover_date = to_system_date($row["project_planned_takeover_date"]);
	}

	if($row["project_projectkind"] == 1 or $row["project_projectkind"] == 9)  {
		$project_kind_new = "X";
	}
	elseif($project["project_projectkind"] == 6)  {
		$project_kind_relocation = "X";
	}
	elseif($row["project_projectkind"] == 2 or $row["project_projectkind"] == 3)  {
		$project_kind_renovation = "X";
	}


	//relocated POS-Name
	$relocated_pos_name = "";
	if($row["project_relocated_posaddress_id"] > 0)
	{
		$relocated_pos_data = get_relocated_pos_info($row["project_relocated_posaddress_id"]);

			$relocated_pos_name = $relocated_pos_data["posaddress_name"] .", " .
			$relocated_pos_data["posaddress_zip"] . " " .
			$relocated_pos_data["place_name"];
	}
	
	
}

$client_currency = get_cer_currency(param("pid"));
$currency_symbol = $client_currency["symbol"];

//get milestones
$date_ln_approval = "";
$date_cer_request = "";

$sql = "select project_milestone_milestone, project_milestone_date from project_milestones " . 
       "where project_milestone_milestone in(1, 13, 15) " . 
	   " and project_milestone_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($row["project_milestone_milestone"] == 1)
	{
		if($row["project_milestone_date"] == NULL or $row["project_milestone_date"] == '0000-00-00')
		{
			$date_cer_request = 'n.a.';
		}
		else
		{
			$date_cer_request = to_system_date($row["project_milestone_date"]);
		}
	}
	elseif($row["project_milestone_milestone"] == 15)
	{
		if($row["project_milestone_date"] != NULL and $row["project_milestone_date"] != '0000-00-00')
		{
			$date_cer_request = to_system_date($row["project_milestone_date"]);
		}
	}
	elseif($row["project_milestone_milestone"] == 13)
	{
		if($row["project_milestone_date"] == NULL or $row["project_milestone_date"] == '0000-00-00')
		{
			$date_ln_approval = 'n.a.';
		}
		else
		{
			$date_ln_approval = to_system_date($row["project_milestone_date"]);
		}
	}
}

$fixed_assets_amount = round($fixed_assets_total/1000,0);
$requested_amount = round(($investment_total + $intagibles_total + $deposit + $other_noncapitalized_cost + $landloard_contribution) / 1000, 0);
$investment_amount = round($investment_total/1000,0);
$key_money = round($intagibles_total/1000, 0);
$deposit = round($deposit/1000, 0);
$other_cost = round($other_noncapitalized_cost/1000, 0);

$landlord_amount = round($landloard_contribution/1000,0);

$net_investment_total = $investment_total+$landloard_contribution;
$net_investment_amount = round(($investment_total+$landloard_contribution)/1000,0);

$net_present_value = round($net_present_value_retail/1000, 0);
$internal_reate_of_return = "";
if($net_present_value) 
{
	$internal_reate_of_return = round($discounted_cash_flow_retail,2) . "%";
}

if($pay_back_period_retail)
{
	$pay_back_period = round($pay_back_period_retail,2);
}
else
{
	$pay_back_period = "Invest. Period";
}


//compose dataset for sales data
$cer_ln_years = array();
$cer_ln_years_first_year = 99999999;
$cer_first_year = "";
$ln_first_year = "";
$cer_last_year = "";
$ln_last_year = "";

if(count($sales_units_watches_values) > 0)
{
	end($sales_units_watches_values);         
	$cer_last_year = key($sales_units_watches_values); 
	
	reset($sales_units_watches_values);
	$cer_first_year = key($sales_units_watches_values);
}

if(count($ln_sales_units_watches_values) > 0)
{
	end($ln_sales_units_watches_values);         
	$ln_last_year = key($ln_sales_units_watches_values); 
	

	reset($ln_sales_units_watches_values);
	$ln_first_year = key($ln_sales_units_watches_values);
}


foreach($sales_units_watches_values as $year=>$value)
{
	$cer_ln_years[$year] = $year;
	if($year < $cer_ln_years_first_year)
	{
		$cer_ln_years_first_year = $year;
	}
}
foreach($ln_sales_units_watches_values as $year=>$value)
{
	$cer_ln_years[$year] = $year;
	if($year < $cer_ln_years_first_year)
	{
		$cer_ln_years_first_year = $year;
	}
}


//check if we have budget data
$budget_present = false;
$sql = "select count(costsheet_id) as num_recs from costsheets
        where costsheet_version = 0 and costsheet_project_id = " . dbquote($project['project_id']);


$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
if($row['num_recs'] > 0) {
	$budget_present = true;
}

//check if we have bids
$bids_present = false;
$sql = "select count(costsheet_bid_position_id) as num_recs from costsheet_bid_positions
        where costsheet_bid_position_project_id = " . dbquote($project['project_id']);


$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
if($row['num_recs'] > 0) {
	$bids_present = true;
}


//check if e have attachmenst form the project
//LN supporting documents
$attached_documents_images = array();
$attached_documents_pdfs = array();
$sql_d = "select order_file_path, order_file_type " . 
		 " from order_files " . 
		 " where order_file_order = " . dbquote($order_number) . 
		 " and order_file_attach_to_cer_booklet = 1 " . 
		 " and order_file_type IN (2,13) " . 
		 " order by order_file_type";

$res_d = mysql_query($sql_d) or dberror($sql_d);
while ($row_d = mysql_fetch_assoc($res_d))
{
	

	if($row_d['order_file_type'] == 2) {
		$attached_documents_images[] = $_SERVER["DOCUMENT_ROOT"] . $row_d["order_file_path"];
	}
	else {
		$attached_documents_pdfs[] = $_SERVER["DOCUMENT_ROOT"] . $row_d["order_file_path"];
	}
}

?>