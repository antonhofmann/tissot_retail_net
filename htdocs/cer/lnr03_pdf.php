<?php
/********************************************************************

    lnr03_pdf.php

    Print LNR-03 single form

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-11-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-11-20
    Version:        1.0.0

    Copyright (c) 201,4 Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
$pdf_print_output = true;


define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require "include/financial_functions.php";
include("include/in_project_data.php");


check_access("has_access_to_cer");

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/SetaPDF/Autoload.php');


$PDFmerger_was_used = false;

$currencies = array();
$sql = 'select currency_id, currency_symbol from currencies where currency_system = 1';

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$currencies["s"] = $row['currency_symbol'];
}


if($cer_basicdata['cer_basicdata_currency']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c1"] = $row['currency_symbol'];
	}
}

if($cer_basicdata['cer_basicdata_currency2']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency2'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c2"] = $row['currency_symbol'];
	}
}

$version_date = ""; 
if($cer_version > 0)
{
	$version_date = " Older Version " . to_system_date(substr($cer_basicdata["version_date"], 0,10)) . " " . substr($cer_basicdata["version_date"], 10,9);
}


// Create and setup PDF document
class MYPDF extends TCPDF
{
	
	//Page footer
	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		$this->SetFont('arialn','I',8);
		$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
	}

}

$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 10);
$pdf->setPrintHeader(false);

$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');

$pdf->SetFillColor(224,224,224);

$pdf->Open();

//Distribution Analysis
include("lnr_03_pdf_detail_2016.php");

$pdf->Output('LNR-03_' . $project_name . "_" . $project_number . '.pdf');


?>