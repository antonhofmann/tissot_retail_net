<?php
/********************************************************************

    projects_archive_projects.php

    Entry page for the cer section in archive.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-07-24
    Version:        1.1.1

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/access_filters.php";

check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/

// create sql

$sub_select_filter = " (select count(posproject_type_id) " . 
              "from posproject_types " . 
			  "where posproject_type_postype = postype_id " . 
			  " and posproject_type_projectcosttype = project_costtype_id " . 
			  " and posproject_type_projectkind = project_projectkind " . 
			  " and (posproject_type_needs_cer = 1 or posproject_type_needs_af = 1 or posproject_type_needs_inr03 = 1)) > 0 ";

$sql = "select distinct project_id, project_number, project_order, " .
	   "left(projects.date_created, 10), ".
	   "product_line_name, postype_name, " . 
	   "concat(order_shop_address_place,', ', order_shop_address_company), country_name, ".
	   "    concat(user_name,' ',user_firstname), ".
	   "    order_id, order_actual_order_state_code, project_costtype_text, projectkind_code, ".
	   " YEAR(projects.date_created) as year " . 
	   "from projects ".
	   "left join orders on project_order = order_id ".
	   "left join project_costs on project_cost_order = order_id " .
	   "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join product_lines on project_product_line = product_line_id ".
	   "left join postypes on postype_id = project_postype ".
	   "left join countries on order_shop_address_country = countries.country_id ".
	   "left join projectkinds on projectkind_id = project_projectkind ".
	   "left join users on project_retail_coordinator = users.user_id ";

$list_filter = $sub_select_filter . " and (order_actual_order_state_code = '900' or order_archive_date <> '0000-00-00' or order_archive_date is not null)";

if(param("search_term"))
{
	$s = urldecode(param("search_term"));
	$list_filter .= " and (project_number like '%" . $s . "%'
					or postype_name like '%" . $s . "%' 
					or country_name like '%" . $s . "%' 
					or product_line_name like '%" . $s . "%' 
					or order_shop_address_company like '%" . $s . "%' 
					or order_shop_address_place like '%" . $s . "%' 
					or project_costtype_text like '%" . $s . "%' 
					or order_actual_order_state_code like '%" . $s . "%'
					or order_actual_order_state_code like '%" . $s . "%'
					)";
}


if(param("y1") > 0)
{
	$list_filter .= "    and left(order_date,4) >= " . param("y1");
	register_param("y1");
}
if(param("y2") > 0)
{
	$list_filter .= "    and left(order_date,4) <= " . param("y2");
	register_param("y2");
}

if(param("p") > 0)
{
	$list_filter .= "    and product_line_id = " . param("p");
	register_param("p");
}



if(param("c") > 0)
{
	$list_filter .= "    and country_id = " . param("c");
	register_param("c");
}



if(param("pk") > 0)
{
	$list_filter .= "    and project_projectkind = " . param("pk");
	register_param("pk");
}

if(param("pt") > 0)
{
	$list_filter .= "    and project_postype = " . param("pt");
	register_param("pt");
}

if(param("lt") > 0)
{
	$list_filter .= "    and project_cost_type = " . param("lt");
	register_param("lt");
}

if(param("sqm") > 0)
{
	$sqm_ranges = array();
	$sqm_ranges[1] = "16";
	$sqm_ranges[2] = "31";
	$sqm_ranges[3] = "45";
	$sqm_ranges[4] = "61";
	$sqm_ranges[5] = "76";
	$sqm_ranges[6] = "101";
	$sqm_ranges[7] = "10000";

	if(param("sqm") == 1)
	{
		$list_filter .= "    and project_cost_totalsqms < 16";
	}
	elseif(param("sqm") < 8)
	{
		
		$list_filter .= "    and project_cost_totalsqms < " . $sqm_ranges[param("sqm")];
		$list_filter .= "    and project_cost_totalsqms > " . $sqm_ranges[param("sqm")-1];
	}
	
	register_param("sqm");
}


$year_filter = array();

if(!has_access("has_full_access_to_cer") 
	and (has_access("has_access_to_his_cer") or has_access("can_view_his_cer_data")))
{

	$user_data = get_user(user_id());
		
	$country_filter = "";
	$tmp = array();
	$sql_c = "select * from country_access " .
		     "where country_access_user = " . user_id();


	$res_c = mysql_query($sql_c) or dberror($sql_c);

	while ($row_c = mysql_fetch_assoc($res_c))
	{            
		$tmp[] = $row_c["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
	}


	$company_access_filter = get_users_regional_access_to_projects(user_id());

	/*
	if($country_filter) 
	{
		$list_filter .= " and ( " . $country_filter;
		$list_filter .= " or project_retail_coordinator = " . user_id() . ")";
	}
	else
	{
		$list_filter .= "    and order_client_address = " . $user_data["address"];
	}
	*/


	if($country_filter) 
	{
		if($company_access_filter)
		{
			$list_filter .= " and ( (" . $country_filter . " or " . $company_access_filter . ") ";
		}
		else
		{
			$list_filter .= " and ( " . $country_filter;
		}
		
		if(has_access("has_access_to_all_projects_of_his_country"))
		{
			//$list_filter .= " or project_retail_coordinator = " . user_id() . " or order_client_address = " . dbquote($user_data["address"]) . ")";

			$list_filter .= " or order_client_address = " . dbquote($user_data["address"]) . ")";
		}
		else
		{
			//$list_filter .= " or project_retail_coordinator = " . user_id() . ")";
		}
	}
	else
	{
		if($company_access_filter)
		{
			//$list_filter .= " and (project_retail_coordinator = " . user_id() . " or " . $company_access_filter . ") ";
			$list_filter .= " and (" . $company_access_filter . ") ";
		}
		else
		{
			//$list_filter .= " and project_retail_coordinator = " . user_id();
		}
	}

	if(has_access("has_access_to_all_travalling_retail_data"))
	{
		$user_roles = get_user_roles(user_id());
		$order_list = get_user_specific_order_list(user_id(), $user_roles, true);
		if($order_list != "()")
		{
			$list_filter .= " and (project_order IN " . $order_list . ") ";
		}
	}
	elseif(!$company_access_filter and !$country_filter) {
		
		$list_filter .= "    and order_client_address = " . $user_data["address"];
	}

	
	$first_year = 0;
	$sql_years = "select DISTINCT YEAR(projects.date_created) as year " . 
				   "from projects ".
				   "left join orders on project_order = order_id ".
				   "left join project_costs on project_cost_order = order_id " .
				   "left join project_costtypes on project_costtype_id = project_cost_type " .
				   "left join product_lines on project_product_line = product_line_id ".
				   "left join postypes on postype_id = project_postype ".
				   "left join countries on order_shop_address_country = countries.country_id ".
				   "left join projectkinds on projectkind_id = project_projectkind ".
				   "left join users on project_retail_coordinator = users.user_id " . 
				   " where " . $list_filter . 
		           " order by year DESC";

	$res_y = mysql_query($sql_years) or dberror($sql_years);
	while($row_y = mysql_fetch_assoc($res_y))
	{
		if($first_year == 0) {
			$first_year = $row_y['year'];
		}
		$year_filter[$row_y['year']] = $row_y['year'];
	}


	if($list_filter) {
		if(param('year_filter')) {
			$list_filter .= ' and projects.date_created like "%' . param('year_filter') . '%"';
		}
		else {
			$list_filter .= ' and projects.date_created like "%' . $first_year . '%"';
		}
		
	}
	else {
		
		if(param('year_filter')) {
			$list_filter .= ' where projects.date_created like "%' .  param('year_filter') . '%"';
		}
		else {
			$list_filter .= ' where projects.date_created like "%' . $first_year . '%"';
		}
		
	}
}


/*
//image column
if(!param("show_project"))
{
	$submission_states = array();
	$sql_s = $sql . " where " . $list_filter;
	$res = mysql_query($sql_s) or dberror($sql_s);
	while($row = mysql_fetch_assoc($res))
	{
		$ok = true;
		
		$posdata = get_pos_data($row["project_order"]);
		$construction = get_pos_intangibles($row["project_id"], 1);
		$fixturing = get_pos_intangibles($row["project_id"], 3);

	
		if(array_key_exists("posaddress_fag_territory", $posdata) and !$posdata["posaddress_fag_territory"])
		{
			$ok = false;
		}
		if(!array_key_exists("cer_investment_amount_cer_loc", $construction) and !array_key_exists("cer_investment_amount_cer_loc", $fixturing))
		{
		//if($construction == 0 or $fixturing == 0)
		//{
			$ok = false;
		}

		$revenues = 0;
		$sql_r = "select sum(cer_revenue_watches) as total " .
			     "from cer_revenues " .
			     " where cer_revenue_project = " . $row["project_id"] . 
			     " and cer_revenue_cer_version = 0 ";

		$res_r = mysql_query($sql_r) or dberror($sql_r);
		if($row_r = mysql_fetch_assoc($res_r))
		{
			$revenues = $row_r["total"];
		}

		$expenses = 0;
		$sql_r = "select sum(cer_expense_amount) as total " .
			   "from cer_expenses " .
			   " where cer_expense_project = " . $row["project_id"] . 
			   " and cer_expense_cer_version = 0 ";

		$res_r = mysql_query($sql_r) or dberror($sql_r);
		if($row_r = mysql_fetch_assoc($res_r))
		{
			$expenses = $row_r["total"];
		}


		$salaries = 0;
		$sql_r = "select sum(cer_salary_fixed_salary) as total " .
			   "from cer_salaries " .
			   " where cer_salary_project = " . $row["project_id"] . 
			   " and cer_salary_cer_version = 0 ";

		$res_r = mysql_query($sql_r) or dberror($sql_r);
		if($row_r = mysql_fetch_assoc($res_r))
		{
			$salaries = $row_r["total"];
		}

		if($revenues == 0 or $expenses == 0 or $salaries == 0)
		{
			$ok = false;
		}
		
		if($ok == true)
		{
			$submission_states[$row["project_id"]] = "/pictures/ok.gif";
		}
		else
		{
			$submission_states[$row["project_id"]] = "/pictures/not_ok.gif";
		}
	}
}

*/

arsort($year_filter);

//count projects
 $sql_count = "select count(project_id) as num_recs ".
	   "from projects ".
	   "left join orders on project_order = order_id ".
	   "left join project_costs on project_cost_order = order_id " .
	   "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join product_lines on project_product_line = product_line_id ".
	   "left join postypes on postype_id = project_postype ".
	   "left join countries on order_shop_address_country = countries.country_id ".
	   "left join users on project_retail_coordinator = users.user_id ";


$sql_count = $sql_count . " where " . $list_filter;
$res = mysql_query($sql_count);
$row = mysql_fetch_assoc($res);



if($row["num_recs"] == 1) {
	
	
	$sql_count = "select project_id ".
	   "from projects ".
	   "left join orders on project_order = order_id ".
	   "left join project_costs on project_cost_order = order_id " .
	   "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join product_lines on project_product_line = product_line_id ".
	   "left join postypes on postype_id = project_postype ".
	   "left join countries on order_shop_address_country = countries.country_id ".
	   "left join users on project_retail_coordinator = users.user_id ";

	$sql_count = $sql_count . " where " . $list_filter;
	$res = mysql_query($sql_count);
	$row = mysql_fetch_assoc($res);


	$link = "cer_project.php?pid=" . $row["project_id"] . "&id=" . $row["project_id"];
	redirect($link);
}

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);
$list->add_hidden("showall", param("showall"));


if(!has_access("has_full_access_to_cer") 
   and (has_access("has_access_to_his_cer") or has_access("can_view_his_cer_data")))
{
	$list->add_listfilters("year_filter", "Year", 'select', $year_filter, param("year_filter"));
}

$list->set_entity("projects");
$list->set_order("order_actual_order_state_code ASC, left(projects.date_created, 10) desc");
$list->set_filter($list_filter);   


$list->add_hidden("y1", param("y1"));
$list->add_hidden("y2", param("y2"));
$list->add_hidden("p", param("p"));
$list->add_hidden("c", param("c"));
$list->add_hidden("pn", param("pn"));


$list->add_column("project_number", "Project No.", "cer_project.php?pid={project_id}", "", "", COLUMN_NO_WRAP);

if(!param("show_project"))
{
	//$list->add_image_column("", "", 0, $submission_states);
}

$list->add_column("project_costtype_text", "Legal Type", "", 0, "", COLUMN_NO_WRAP);
$list->add_column("projectkind_code", "Kind", "", 0, "", COLUMN_NO_WRAP);
$list->add_column("product_line_name", "Product Line", "", 0, "", COLUMN_NO_WRAP);
$list->add_column("postype_name", "POS Type", "", 0, "", COLUMN_NO_WRAP);

$list->add_column("order_actual_order_state_code", "Project\nStatus", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
//$list->add_column("left(projects.date_created, 10)", "Submitted", "", "", "", COLUMN_NO_WRAP);
$list->add_column("country_name", "Country", "", 0, "");
$list->add_column("concat(order_shop_address_place,', ', order_shop_address_company)", "POS Address");
$list->add_column("concat(user_name,' ',user_firstname)", "Project Leader", "", 0, "");

/********************************************************************
    Populate list and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

/********************************************************************
    render page
*********************************************************************/ 
$page = new Page("cer_archive");

$page->header();
$page->title("LNR/CER Archive: Projects");
$list->render();
$page->footer();
?>