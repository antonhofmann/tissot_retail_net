<?php
/********************************************************************

    cer_additional_rental_cost_types.php

    Lists Additional rental Cost types

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2015-04-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2015-04-15
    Version:        1.0.0

    Copyright (c) 2015 Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_full_access_to_cer");
set_referer("cer_additional_rental_cost_type.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
//get all data from interest rates


$sql = "select additional_rental_cost_type_id, additional_rental_cost_type_name, " .
       "if(additional_rental_cost_type_active = 1, 'x', '') as inuse " . 
       "from additional_rental_cost_types ";


/********************************************************************
    build list 
*********************************************************************/

$list = new ListView($sql);

$list->set_entity("additional_rental_cost_types");
$list->set_order("additional_rental_cost_type_name");

$list->add_column("additional_rental_cost_type_name", "Type", "cer_additional_rental_cost_type.php");
$list->add_column("inuse", "in use");

$list->add_button(LIST_BUTTON_NEW, "New", "cer_additional_rental_cost_type.php");

$list->populate();
$list->process();

$page = new Page("cer_additional_rental_cost_types");

$page->header();
$page->title("Additional Rental Cost Types");
$list->render();
$page->footer();
?>
