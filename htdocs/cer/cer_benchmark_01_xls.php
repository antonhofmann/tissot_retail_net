<?php
/********************************************************************

    cer_benchmark_01_xls.php

    Generate Excel File: CER Benchmark Overview

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-10-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-10-15
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_access_to_cer_benchmarks");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

include("include/calculate_depreciation.php");

/********************************************************************
    prepare Data Needed
*********************************************************************/
include("cer_benchmark_filter.php");


/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "benchmark_" . $benchmark_shortcut . "_horizonal_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet = $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$f_header =& $xls->addFormat();
$f_header->setSize(12);
$f_header->setAlign('left');
$f_header->setBold();

$f_title =& $xls->addFormat();
$f_title->setSize(10);
$f_title->setAlign('left');
$f_title->setBold();

$f_group_title =& $xls->addFormat();
$f_group_title->setSize(10);
$f_group_title->setAlign('left');
$f_group_title->setBold();

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_normal_bold =& $xls->addFormat();
$f_normal_bold->setSize(8);
$f_normal_bold->setAlign('left');
$f_normal_bold->setBorder(1);
$f_normal_bold->setBold();

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);
$f_number->setNumFormat("0.00");

$f_number_bold =& $xls->addFormat();
$f_number_bold->setSize(8);
$f_number_bold->setAlign('right');
$f_number_bold->setBorder(1);
$f_number_bold->setBold();
$f_number_bold->setNumFormat("0.00");

$f_percent =& $xls->addFormat();
$f_percent->setSize(8);
$f_percent->setAlign('right');
$f_percent->setBorder(1);
$f_percent->setNumFormat("0.00%");

$f_percent_bold =& $xls->addFormat();
$f_percent_bold->setSize(8);
$f_percent_bold->setAlign('right');
$f_percent_bold->setBorder(1);
$f_percent_bold->setBold();
$f_percent_bold->setNumFormat("0.00%");

$f_unused =& $xls->addFormat();
$f_unused->setSize(8);
$f_unused->setBorder(1);
$f_unused->setPattern(2);
$f_unused->setBgColor('silver');

$f_black_on_grey =& $xls->addFormat();
$f_black_on_grey->setSize(8);
$f_black_on_grey->setBorder(1);
$f_black_on_grey->setPattern(1);
$f_black_on_grey->setFgColor('silver');
$f_black_on_grey->setColor('black');
$f_black_on_grey->setBold();


$f_white_on_green =& $xls->addFormat();
$f_white_on_green->setSize(8);
$f_white_on_green->setBorder(1);
$f_white_on_green->setPattern(1);
$f_white_on_green->setFgColor('green');
$f_white_on_green->setColor('white');
$f_white_on_green->setBold();


$f_white_on_green_border_left =& $xls->addFormat();
$f_white_on_green_border_left->setSize(8);
$f_white_on_green_border_left->setPattern(1);
$f_white_on_green_border_left->setFgColor('green');
$f_white_on_green_border_left->setColor('white');
$f_white_on_green_border_left->setBold();
$f_white_on_green_border_left->setTop(1);
$f_white_on_green_border_left->setBottom(1);
$f_white_on_green_border_left->setLeft(1);

$f_white_on_green_border_middle =& $xls->addFormat();
$f_white_on_green_border_middle->setSize(8);
$f_white_on_green_border_middle->setPattern(1);
$f_white_on_green_border_middle->setFgColor('green');
$f_white_on_green_border_middle->setColor('white');
$f_white_on_green_border_middle->setBold();
$f_white_on_green_border_middle->setTop(1);

$f_white_on_green_border_right =& $xls->addFormat();
$f_white_on_green_border_right->setSize(8);
$f_white_on_green_border_right->setPattern(1);
$f_white_on_green_border_right->setFgColor('green');
$f_white_on_green_border_right->setColor('white');
$f_white_on_green_border_right->setBold();
$f_white_on_green_border_right->setTop(1);
$f_white_on_green_border_right->setBottom(1);
$f_white_on_green_border_right->setRight(1);

$f_border_left=& $xls->addFormat();
$f_border_left->setTop(1);
$f_border_left->setBottom(1);
$f_border_left->setLeft(1);
$f_border_left->setSize(8);
$f_border_left->setAlign('left');
$f_border_left->setBold();

$f_border_right=& $xls->addFormat();
$f_border_right->setTop(1);
$f_border_right->setBottom(1);
$f_border_right->setRight(1);


$f_border_middle=& $xls->addFormat();
$f_border_middle->setTop(1);
$f_border_middle->setBottom(1);

$f_comment =& $xls->addFormat();
$f_comment->setSize(8);
$f_comment->setAlign('left');
$f_comment->setBorder(0);

//benchmark rows
$cer_captions = array();

$cer_captions[0] = "Currency";
$cer_captions[1] = "Sales surface in m2";

if($investments_only == 0)
{
	$cer_captions[2] = "Full time equivalent";
	$cer_captions[3] = "Lease term in years";
	$cer_captions[4] = "Tissot Watches - Units";
	$cer_captions[5] = "Tissot Jewelery - Units";
	$cer_captions[8] = "Tissot Watches - Average Price";
	$cer_captions[9] = "Tissot Jewelery - Average Price";
	$cer_captions[12] = "Sales Value Tissot Watches"; 
	$cer_captions[13] = "Sales Value Tissot Jewelery";

	$cer_captions[16] = "Sales Value Accessories / Customer Serivces";
	$cer_captions[17] = "Total Gross Sales";
	$cer_captions[18] = "Sales Reduction";
	$cer_captions[19] = "Total Net Sales";

	$cer_captions[20] = "Material of Products Sold";
	$cer_captions[21] = "Total Gross Margin";
	$cer_captions[22] = "Marketing Expenses";

	$cer_captions[23] = "Indirect Salaries";
	$cer_captions[24] = "Rents";
	$cer_captions[25] = "Aux. Mat., energy, maintenance";
	$cer_captions[26] = "Sales & administration expenses";
	$cer_captions[27] = "Depreciation on fixed assets";
	$cer_captions[28] = "Depreciation on Key Money";
	$cer_captions[29] = "Other indirect Expenses";
	$cer_captions[30] = "Total Indirect Expenses";
	$cer_captions[31] = "Operating Income w/o Wholesale Margin";
}

$cer_captions[32] = "Investments";
$cer_captions[33] = "Local construction";
$cer_captions[34] = "Store fixturing / Furniture";
$cer_captions[35] = "Architectural services";
$cer_captions[36] = "Equipment";
$cer_captions[37] = "Other";
$cer_captions[38] = "Total Investment in Fixed Assets";

if($investments_only == 0)
{
	$cer_captions[39] = "Key-/Premium Money/Goodwill";
	//$cer_captions[37] = "Deposit/Recoverable Keymoney";
	$cer_captions[40] = "Deposit";
	$cer_captions[41] = "Other non-capitalized costs";
	$cer_captions[42] = "Total Project costs (Requested amount)";

	$cer_captions[43] = "KL Approved Investments";
	$cer_captions[44] = "Local construction";
	$cer_captions[45] = "Store fixturing / Furniture";
	$cer_captions[46] = "Architectural services";
	$cer_captions[47] = "Equipment";
	$cer_captions[48] = "Other";
	$cer_captions[49] = "Total KL approved Investment for Fixed Assets";
	$cer_captions[50] = "Key-/Premium Money/Goodwill";
	//$cer_captions[48] = "Deposit/Recoverable Keymoney";
	$cer_captions[51] = "Deposit";
	$cer_captions[52] = "Other non-capitalized costs";
	$cer_captions[53] = "Total KL approved Investments for Project";




	$cms_captions = array();

	$cms_captions[1] = "Local construction";
	$cms_captions[2] = "Store fixturing / Furniture";
	$cms_captions[3] = "Architectural services";
	$cms_captions[4] = "Equipment";
	$cms_captions[5] = "Other";
	$cms_captions[6] = "Total Real Costs of Fixed Assets";
	$cms_captions[7] = "Key-/Premium Money/Goodwill";
	//$cms_captions[8] = "Deposit/Recoverable Keymoney";
	$cms_captions[8] = "Deposit";
	$cms_captions[9] = "Other non-capitalized costs";
	$cms_captions[10] = "Total Real Costs";
}

/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $benchmark_title, $f_header);
$sheet->write(1, 0, "Benchmark horizontal (" . date("d.m.Y, H:m:s") . ")", $f_title);


//$sheet->setRow(4, 165);
//$sheet->writeRow(4, 0, $captions, $f_caption);

/********************************************************************
    write all data
*********************************************************************/
$list_totals = array();
$num_of_projects = 0;


$c01 = 0;
if($investments_only == 0)
{
	$row_index = 11;
}
else {
		$row_index = 9;
}
$sheet->setColumn($c01, $c01, 38);
$sheet->write($row_index, $c01, "LNR/CER Estimations (Business Plan)", $f_title);
$row_index++;


foreach($cer_captions as $key=>$caption)
{
	if($key == 17 or $key == 19 or $key == 21 or $key == 22 
		or $key == 30 or $key == 31 or $key == 38 or $key == 42
		or $key == 49 or $key == 53)
	{
		$sheet->write($row_index, $c01, $caption, $f_normal_bold);
	}
	elseif($key == 32)
	{
		if($investments_only == 1)
		{
			$row_index++;
		}
		
		$sheet->write($row_index, $c01, $caption, $f_title);
	}
	elseif($key == 43)
	{
		$row_index++;
		$sheet->write($row_index, $c01, $caption, $f_title);
	}
	else
	{
		$sheet->write($row_index, $c01, $caption, $f_normal);
	}
	
	
	
	if($key == 31)
	{
		$row_index++;
		//$row_index++;
		//$row_index++;
	}
	$row_index++;
}

if($investments_only == 0)
{

	$row_index++;
	//$row_index++;

	$sheet->write($row_index, $c01, "CMS (real cost)", $f_title);
	$row_index++;
	foreach($cms_captions as $key=>$caption)
	{
		
		if($key == 6 or $key == 10)
		{
			$sheet->write($row_index, $c01, $caption, $f_normal_bold);
		}
		else
		{
			$sheet->write($row_index, $c01, $caption, $f_normal);
		}
		$row_index++;
	}
}

$sheet->setColumn(1, 1, 2);
$col_index01 = 2;
$col_index02 = 2;
$list_avgs = array();


foreach($posorders as $key=>$posorder)
{
	if($posorder["pipeline"] == 1)
	{
		$sql = "select * " . 
			   "from posaddressespipeline " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " . 
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];

	}
	elseif($posorder["pipeline"] == 2) // renovation project
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];

	}
	else
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorders on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and posorder_id = " . $posorder["id"];

		
	}


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		
		//$result = update_posdata_from_posorders($row["posaddress_id"]);
		//$result = build_missing_investment_records($row["posorder_id"]);
		$result = update_investment_records_from_system_currency($row["posorder_id"]);
		$result = update_investment_records_from_local_currency($row["posorder_id"]);
		$num_of_projects++;

		//get benchmark first full years
		$benchmark_year = 0;
		$exchange_rate = 1;
		$factor = 1;

		$cer_basicdata = get_cer_basicdata($row["project_id"]);

		if(count($cer_basicdata) > 0)
		{
			$first_year = $cer_basicdata["cer_basicdata_firstyear"];
			$first_month = $cer_basicdata["cer_basicdata_firstyear"];

			if($first_month == 1)
			{
				$benchmark_year = $cer_basicdata["cer_basicdata_firstyear"];
			}
			else
			{
				$benchmark_year = $cer_basicdata["cer_basicdata_firstyear"] + 1;
			}
			$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
			$factor = $cer_basicdata["cer_basicdata_factor"];

			if(!$factor) {$factor = 1;}
		}
		
		$row_index = 3;

		$sheet->setColumn($col_index01, $col_index01, 9);
		$sheet->setColumn($col_index01+1, $col_index01+1, 9);
		$sheet->setColumn($col_index01+2, $col_index01+2, 9);
		$sheet->setColumn($col_index01+3, $col_index01+3, 12);
		$sheet->setColumn($col_index01+4, $col_index01+4, 2);
		
		$sheet->write($row_index, $col_index01, $row["posaddress_name"], $f_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_border_right);
		$row_index++;
		$sheet->write($row_index, $col_index01, $row["country_name"] . ", " . $row["posaddress_place"], $f_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_border_right);
		$row_index++;

		$product_line_name = $row["product_line_name"];
		if($row["productline_subclass_name"]) {
			$product_line_name .= " / " . $row["productline_subclass_name"];
		}
		
		$sheet->write($row_index, $col_index01, $row["postype_name"] . ": " . $product_line_name, $f_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_border_right);
		$row_index++;
		
		$tmp = $row["project_costtype_text"] . " - " . $row["order_number"] . ' / Kind: ' . $row["projectkind_code"] . ' / Satus: ' . $row["order_actual_order_state_code"];
		$sheet->write($row_index, $col_index01, $tmp, $f_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_border_right);
		$row_index++;

		$sheet->write($row_index, $col_index01, "Benchmark Full Year : " . $benchmark_year, $f_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_border_right);
		$row_index++;
		$row_index++;

		if($investments_only == 0)
		{		
			$sheet->write($row_index, $col_index01, "Amount", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+1,"per sqm", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+2,"per fte", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+3,"% of net sales", $f_black_on_grey);
			$row_index = 12;
			
		}
		else
		{
			$row_index = 10;
		}



		//curency
		$currency_symbol = "";
		$sales_reduction_percent = 0;
		$sql_s = "select currency_symbol, cer_basicdata_sales_reduction " . 
			     "from cer_basicdata " . 
			     "left join currencies on currency_id = cer_basicdata_currency " . 
			     "where cer_basicdata_project = " . dbquote($row["project_id"]) . 
			     " and cer_basicdata_version = 0 ";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$currency_symbol = $row_s["currency_symbol"];
			$sales_reduction_percent = $row_s["cer_basicdata_sales_reduction"] / 100;
		}

		if($base_currency == "chf")
		{
			$currency_symbol = "CHF";
		}
		else
		{
			$exchange_rate = 1;
			$factor = 1;
		}

		$sheet->write($row_index, $col_index01, $currency_symbol, $f_number);
		$row_index++;

		//sales surface

		
		$retail_area = $row["project_cost_sqms"];
		$sheet->write($row_index, $col_index01, $retail_area, $f_number);
		
		if($investments_only == 0)
		{
			$row_index++;
		}
		else
		{
			$row_index++;
			$row_index++;
		}


		if(array_key_exists("project_cost_sqms", $list_avgs))
		{
			$list_avgs["project_cost_sqms"] = $list_avgs["project_cost_sqms"] + $retail_area;
		}
		else
		{
			$list_avgs["project_cost_sqms"] = $retail_area;
		}

		//ful time equivalent
		// get total ftes
		
		if($investments_only == 0)
		{
			$ftes = 0;
			$sql_s = "select sum(cer_salary_headcount_percent) as fte " . 
					 "from cer_salaries " .
					 "left join projects on project_id = cer_salary_project " . 
					 "left join orders on order_id = project_order " . 
					 "where order_id = '" . $row["posorder_order"] . "'" . 
					 " and cer_salary_cer_version = 0 " .
					 " and cer_salary_year_starting <= " . $benchmark_year;
			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			while($row_s = mysql_fetch_assoc($res_s))
			{
				$ftes = $ftes + $row_s["fte"];
			}
			
			$ftes = round($ftes/100,2);
			$sheet->write($row_index, $col_index01, $ftes, $f_number);
			if(array_key_exists("ftes", $list_avgs))
			{
				$list_avgs["ftes"] = $list_avgs["ftes"] + $ftes;
			}
			else
			{
				$list_avgs["ftes"] = $ftes;
			}


			$ftes_per_sqm = 0;
			if($ftes > 0)
			{
				$ftes_per_sqm = round($retail_area/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+1, $ftes_per_sqm, $f_number);
			$row_index++;

			//lease terms
			//get the lease duration in years
			$starting_date = 0;
			$ending_date = 0;
			$extension_option = 0;
			$exit_option = 0;

			if($posorder["pipeline"] == 1)
			{
				$sql_s = "select poslease_startdate, poslease_enddate, poslease_extensionoption, poslease_exitoption " . 
						 "from posleases " . 
						 "where poslease_posaddress = " . $row["posaddress_id"];
			}
			else
			{
				$sql_s = "select poslease_startdate, poslease_enddate, poslease_extensionoption, poslease_exitoption " . 
						 "from posleasespipeline " . 
						 "where poslease_posaddress = " . $row["posaddress_id"];
			}
			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			while($row_s = mysql_fetch_assoc($res_s))
			{
				if($starting_date == 0)
				{
					$starting_date = $row_s["poslease_startdate"];
				}
				elseif($row_s["poslease_startdate"] < $starting_date)
				{
					$starting_date = $row_s["poslease_startdate"];
				}

				if($ending_date == 0)
				{
					$ending_date = $row_s["poslease_enddate"];
				}
				elseif($row_s["poslease_enddate"] > $ending_date)
				{
					$ending_date = $row_s["poslease_enddate"];
				}

				if($extension_option == 0)
				{
					$extension_option = $row_s["poslease_extensionoption"];
				}
				elseif($row_s["poslease_extensionoption"] > $extension_option)
				{
					$extension_option = $row_s["poslease_extensionoption"];
				}

				if($exit_option == 0)
				{
					$exit_option = $row_s["poslease_exitoption"];
				}
				elseif($row_s["poslease_exitoption"] > $exit_option)
				{
					$exit_option = $row_s["poslease_exitoption"];
				}
			}
			if($starting_date == 0 and $ending_date == 0)
			{
				$lease_period = 0;
			}
			else
			{
				$start = strtotime($starting_date) ;
				$end = strtotime($ending_date) ;

				$lease_period = $end - $start;
				$lease_period = $lease_period / 86400;
				$lease_period = round($lease_period/365, 2);

			}

			$sheet->write($row_index, $col_index01, $lease_period, $f_number);
			$row_index++;
			if(array_key_exists("lease_period", $list_avgs))
			{
				$list_avgs["lease_period"] = $list_avgs["lease_period"] + $lease_period;
			}
			else
			{
				$list_avgs["lease_period"] = $lease_period;
			}

			//get revenues
			$watches = 0;
			$jewellery = 0;
			$watches_revenue = 0;
			$jewellery_revenue = 0;
			$services_revenue = 0;

			$sql_s = "select sum(cer_revenue_quantity_watches) as watches, " .
					 "sum(cer_revenue_quantity_jewellery) as jewellery,  " .
					 "sum(cer_revenue_watches) as watches_revenue,  " .
					 "sum(cer_revenue_jewellery) as jewellery_revenue,  " .
					 "sum(cer_revenue_customer_service) as services_revenue  " .
					 "from cer_revenues " .
					 "left join projects on project_id = cer_revenue_project " . 
					 "left join orders on order_id = project_order " . 
					 "where cer_revenue_project = " . dbquote($row["project_id"]) . 
					 " and cer_revenue_cer_version = 0 " .
					 " and cer_revenue_year = " . $benchmark_year;

			$res_s = mysql_query($sql_s) or dberror($sql_s);
			while($row_s = mysql_fetch_assoc($res_s))
			{
				$watches = $watches + $row_s["watches"];
				$jewellery = $jewellery + $row_s["jewellery"];

				$watches_revenue = $watches_revenue + $row_s["watches_revenue"];
				$jewellery_revenue = $jewellery_revenue + $row_s["jewellery_revenue"];
				$services_revenue = $services_revenue + $row_s["services_revenue"];
			}

			$watches_revenue = $watches_revenue * $exchange_rate / $factor;
			$jewellery_revenue = $jewellery_revenue * $exchange_rate / $factor;
			$services_revenue = $services_revenue * $exchange_rate / $factor;
			$total_revenue = $watches_revenue + $jewellery_revenue + $services_revenue;
			$percent_base = 100 + 100*$sales_reduction_percent;
			$base_unit = $total_revenue / $percent_base;

			
			if(array_key_exists("sales_reduction_percent", $list_avgs))
			{
				$list_avgs["sales_reduction_percent"] = $list_avgs["sales_reduction_percent"] + 100*$sales_reduction_percent;
			}
			else
			{
				$list_avgs["sales_reduction_percent"] = 100*$sales_reduction_percent;
			}

					

			//calculate average prices
			$watches_price = 0;
			$jewellery_price = 0;

			if($watches > 0)
			{
				$watches_price = round($watches_revenue/$watches, 2);
			}

			if($jewellery > 0)
			{
				$jewellery_price = round($jewellery_revenue/$jewellery, 2);
			}

			

			
			$sheet->write($row_index, $col_index01, $watches, $f_number);
			if(array_key_exists("watches", $list_avgs))
			{
				$list_avgs["watches"] = $list_avgs["watches"] + $watches;
			}
			else
			{
				$list_avgs["watches"] = $watches;
			}
			$watches_per_sqm = 0;
			if($retail_area > 0)
			{
				$watches_per_sqm = round($watches/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $watches_per_sqm, $f_number);
			$watches_per_fte = 0;
			if($ftes > 0)
			{
				$watches_per_fte = round($watches/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $watches_per_fte, $f_number);
			$row_index++;

			$sheet->write($row_index, $col_index01, $jewellery, $f_number);
			if(array_key_exists("jewellery", $list_avgs))
			{
				$list_avgs["jewellery"] = $list_avgs["jewellery"] + $jewellery;
			}
			else
			{
				$list_avgs["jewellery"] = $jewellery;
			}
			$jewellery_per_sqm = 0;
			if($retail_area > 0)
			{
				$jewellery_per_sqm = round($jewellery/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $jewellery_per_sqm, $f_number);
			$jewellery_per_fte = 0;
			if($ftes > 0)
			{
				$jewellery_per_fte = round($jewellery/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $jewellery_per_fte, $f_number);
			$row_index++;

			$sheet->write($row_index, $col_index01, $watches_price, $f_number);
			$row_index++;
			if(array_key_exists("watches_price", $list_avgs))
			{
				$list_avgs["watches_price"] = $list_avgs["watches_price"] + $watches_price;
			}
			else
			{
				$list_avgs["watches_price"] = $watches_price;
			}

			$sheet->write($row_index, $col_index01, $jewellery_price, $f_number);
			$row_index++;
			if(array_key_exists("jewellery_price", $list_avgs))
			{
				$list_avgs["jewellery_price"] = $list_avgs["jewellery_price"] + $jewellery_price;
			}
			else
			{
				$list_avgs["jewellery_price"] = $jewellery_price;
			}
			
			
			//revenues
			$gross_sales = $watches_revenue + $jewellery_revenue + $services_revenue;
			$sales_reduction = $gross_sales * $sales_reduction_percent;
			$net_sales = $gross_sales - $sales_reduction;


			$sheet->write($row_index, $col_index01, $watches_revenue, $f_number);
			if(array_key_exists("watches_revenue", $list_avgs))
			{
				$list_avgs["watches_revenue"] = $list_avgs["watches_revenue"] + $watches_revenue;
			}
			else
			{
				$list_avgs["watches_revenue"] = $watches_revenue;
			}

			$watches_revenue_per_sqm = 0;
			if($retail_area > 0)
			{
				$watches_revenue_per_sqm = round($watches_revenue/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $watches_revenue_per_sqm, $f_number);

			$watches_revenue_per_fte = 0;
			if($ftes > 0)
			{
				$watches_revenue_per_fte = round($watches_revenue/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $watches_revenue_per_fte, $f_number);

			$percent_of_watches = "0.00";
			$percent_of_net_sales = 0;
			if($net_sales > 0)
			{
				$percent_of_watches = $watches_revenue/$net_sales;
				$percent_of_net_sales = $watches_revenue/$net_sales;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_watches, $f_percent);

			$row_index++;


			$sheet->write($row_index, $col_index01, $jewellery_revenue, $f_number);
			if(array_key_exists("jewellery_revenue", $list_avgs))
			{
				$list_avgs["jewellery_revenue"] = $list_avgs["jewellery_revenue"] + $jewellery_revenue;
			}
			else
			{
				$list_avgs["jewellery_revenue"] = $jewellery_revenue;
			}

			$jewellery_revenue_per_sqm = 0;
			if($retail_area > 0)
			{
				$jewellery_revenue_per_sqm = round($jewellery_revenue/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $jewellery_revenue_per_sqm, $f_number);

			$jewellery_revenue_per_fte = 0;
			if($ftes > 0)
			{
				$jewellery_revenue_per_fte = round($jewellery_revenue/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $jewellery_revenue_per_fte, $f_number);

			$percent_of_jewellery = "0.00";
			if($net_sales > 0)
			{
				$percent_of_jewellery = $jewellery_revenue/$net_sales;
				$percent_of_net_sales = $percent_of_net_sales + ($jewellery_revenue/$net_sales);
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_jewellery, $f_percent);

			$row_index++;
			
			
			$sheet->write($row_index, $col_index01, $services_revenue, $f_number);
			if(array_key_exists("services_revenue", $list_avgs))
			{
				$list_avgs["services_revenue"] = $list_avgs["services_revenue"] + $services_revenue;
			}
			else
			{
				$list_avgs["services_revenue"] = $services_revenue;
			}

			$services_revenue_per_sqm = 0;
			if($retail_area > 0)
			{
				$services_revenue_per_sqm = round($services_revenue/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $services_revenue_per_sqm, $f_number);

			$services_revenue_per_fte = 0;
			if($ftes > 0)
			{
				$services_revenue_per_fte = round($services_revenue/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $services_revenue_per_fte, $f_number);

			$percent_of_service = "0.00";
			if($net_sales > 0)
			{
				$percent_of_service = $services_revenue/$net_sales;
				$percent_of_net_sales = $percent_of_net_sales + ($services_revenue/$net_sales);
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_service, $f_percent);

			$row_index++;
			
			
			
			//gross sales
			
			$sheet->write($row_index, $col_index01, $gross_sales, $f_number_bold);
			if(array_key_exists("gross_sales", $list_avgs))
			{
				$list_avgs["gross_sales"] = $list_avgs["gross_sales"] + $gross_sales;
			}
			else
			{
				$list_avgs["gross_sales"] = $gross_sales;
			}
			$gross_sales_per_sqm = 0;
			if($retail_area > 0)
			{
				$gross_sales_per_sqm = round($gross_sales/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $gross_sales_per_sqm, $f_number_bold);
			$gross_sales_per_fte = 0;
			if($ftes > 0)
			{
				$gross_sales_per_fte = round($gross_sales/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $gross_sales_per_fte, $f_number_bold);

			$percent_of_net_sales = $percent_of_net_sales;
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent_bold);


			$row_index++;



			$sheet->write($row_index, $col_index01, $sales_reduction, $f_number);
			if(array_key_exists("sales_reduction", $list_avgs))
			{
				$list_avgs["sales_reduction"] = $list_avgs["sales_reduction"] + $sales_reduction;
			}
			else
			{
				$list_avgs["sales_reduction"] = $sales_reduction;
			}

			$sales_reduction_per_sqm = 0;
			if($retail_area > 0)
			{
				$sales_reduction_per_sqm = round($sales_reduction/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $sales_reduction_per_sqm, $f_number);
			$sales_reduction_per_fte = 0;
			if($ftes > 0)
			{
				$sales_reduction_per_fte = round($sales_reduction/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $sales_reduction_per_fte, $f_number);

			
			if($net_sales > 0)
			{
				$sales_reduction_percent = $sales_reduction / $net_sales;
			}
			else
			{
				$sales_reduction_percent = 0;
			}

			$sales_reduction_percent = -1*$sales_reduction_percent;
			$sheet->write($row_index, $col_index01+3, $sales_reduction_percent, $f_percent_bold);

			$row_index++;

			
			//net sales
			$net_sales = $gross_sales - $sales_reduction;

			$sheet->write($row_index, $col_index01, $net_sales, $f_number_bold);
			if(array_key_exists("net_sales", $list_avgs))
			{
				$list_avgs["net_sales"] = $list_avgs["net_sales"] + $net_sales;
			}
			else
			{
				$list_avgs["net_sales"] = $net_sales;
			}
			$net_sales_per_sqm = 0;
			if($retail_area > 0)
			{
				$net_sales_per_sqm = round($net_sales/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $net_sales_per_sqm, $f_number_bold);
			$net_sales_per_fte = 0;
			if($ftes > 0)
			{
				$net_sales_per_fte = round($net_sales/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $net_sales_per_fte, $f_number_bold);
			$sheet->write($row_index, $col_index01+3, "1.00", $f_percent_bold);

			$row_index++;
			

			//get expenses
			$total_cost = 0;
			$expenses = array();
			$sql_s = "select cer_expense_type, sum(cer_expense_amount) as total " . 
					 "from cer_expenses " . 
					 "where cer_expense_project = " . dbquote($row["project_id"]) . 
					 " and cer_expense_cer_version = 0 " . 
					 " and cer_expense_year = " . $benchmark_year . 
					 " group by cer_expense_type";
			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			while($row_s = mysql_fetch_assoc($res_s))
			{
				$expenses[$row_s["cer_expense_type"]] = $row_s["total"];
			}

			foreach($expenses as $key=>$value)
			{
				if($key == 6 or $key == 8 or $key == 9 or $key == 10 or $key == 11 or  $key == 12 or  $key == 23)
				{
					$expenses[10] = $expenses[10] + $value;
				}
			}


			//Gross sales (net sales minus Material of products sold)
			$amount = 0;
			if(array_key_exists(15, $expenses))
			{
				$amount =  $expenses[15] * $exchange_rate / $factor;
				$sheet->write($row_index, $col_index01, $amount, $f_number);
				if(array_key_exists("e15", $list_avgs))
				{
					$list_avgs["e15"] = $list_avgs["e15"] + $amount;
				}
				else
				{
					$list_avgs["e15"] = $amount;
				}
				$expenses_per_sqm = 0;
				if($retail_area > 0)
				{
					$expenses_per_sqm = round($amount/$retail_area,2);
				}
				$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number);
				$expenses_per_fte = 0;
				if($ftes > 0)
				{
					$expenses_per_fte = round($amount/$ftes,2);
				}
				$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number);
				$percent_of_net_sales = "0.00";
				if($net_sales > 0)
				{
					//$percent_of_net_sales =  round($amount/$net_sales,2);
					$percent_of_net_sales = -1*$amount/$net_sales;
				}
				$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
			}
			else
			{
				$sheet->write($row_index, $col_index01, "0", $f_number);
				$sheet->write($row_index, $col_index01+1, "0", $f_number);
				$sheet->write($row_index, $col_index01+2, "0", $f_number);
				$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
				if(array_key_exists("e15", $list_avgs))
				{
					$list_avgs["e15"] = $list_avgs["e15"] + 0;
				}
				else
				{
					$list_avgs["e15"] = 0;
				}
			}


			$row_index++;

			//gross margin

			$gross_margin = $net_sales - $amount;
			$sheet->write($row_index, $col_index01, $gross_margin, $f_number_bold);
			if(array_key_exists("gross_margin", $list_avgs))
			{
				$list_avgs["gross_margin"] = $list_avgs["gross_margin"] + $gross_margin;
			}
			else
			{
				$list_avgs["gross_margin"] = $gross_margin;
			}
			$gross_margin_per_sqm = 0;
			if($retail_area > 0)
			{
				$gross_margin_per_sqm = round($gross_margin/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $gross_margin_per_sqm, $f_number_bold);
			$gross_margin_per_fte = 0;
			if($ftes > 0)
			{
				$gross_margin_per_fte = round($gross_margin/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $gross_margin_per_fte, $f_number_bold);

			$percent_of_net_sales = "0.00";
			if($net_sales > 0)
			{
				$percent_of_net_sales = $gross_margin/$net_sales;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent_bold);

			$row_index++;


			//marketing expenses
			$amount = 0;
			if(array_key_exists(14, $expenses))
			{
				$amount = ($expenses[14] * $exchange_rate / $factor);
			}
			if(array_key_exists(13, $expenses))
			{
				if($amount > 0) 
				{
					$amount =  $amount - ($expenses[13] * $exchange_rate / $factor);
				}
				else
				{
					$amount =  $expenses[13] * $exchange_rate / $factor;
				}

				$sheet->write($row_index, $col_index01, $amount, $f_number_bold);
				if(array_key_exists("e13", $list_avgs))
				{
					$list_avgs["e13"] = $list_avgs["e13"] + $amount;
				}
				else
				{
					$list_avgs["e13"] = $amount;
				}
				$expenses_per_sqm = 0;
				if($retail_area > 0)
				{
					$expenses_per_sqm = round($amount/$retail_area,2);
				}
				$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number_bold);
				$expenses_per_fte = 0;
				if($ftes > 0)
				{
					$expenses_per_fte = round($amount/$ftes,2);
				}
				$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number_bold);
				$percent_of_net_sales = "0.00";
				if($net_sales > 0)
				{
					//$percent_of_net_sales = round($amount/$net_sales,2);
					$percent_of_net_sales = -1*$amount/$net_sales;
				}
				$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent_bold);
			}
			else
			{
				$sheet->write($row_index, $col_index01, "0", $f_number);
				$sheet->write($row_index, $col_index01+1, "0", $f_number);
				$sheet->write($row_index, $col_index01+2, "0", $f_number);
				$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
				if(array_key_exists("e13", $list_avgs))
				{
					$list_avgs["e13"] = $list_avgs["e13"] + 0;
				}
				else
				{
					$list_avgs["e13"] = 0;
				}
			}
			$marketing_expenses = $amount;

			$row_index++;
			
			

			//indirect salaries
			if(array_key_exists(1, $expenses))
			{
				$amount =  $expenses[1] * $exchange_rate / $factor;
				$sheet->write($row_index, $col_index01, $amount, $f_number);
				if(array_key_exists("e1", $list_avgs))
				{
					$list_avgs["e1"] = $list_avgs["e1"] + $amount;
				}
				else
				{
					$list_avgs["e1"] = $amount;
				}
				$expenses_per_sqm = 0;
				if($retail_area > 0)
				{
					$expenses_per_sqm = round($amount/$retail_area,2);
				}
				$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number);
				$expenses_per_fte = 0;
				if($ftes > 0)
				{
					$expenses_per_fte = round($amount/$ftes,2);
				}
				$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number);
				$percent_of_net_sales = "0.00";
				if($net_sales > 0)
				{
					$percent_of_net_sales = -1*$amount/$net_sales;
				}
				$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
				$total_cost = $total_cost +$amount;
			}
			else
			{
				$sheet->write($row_index, $col_index01, "0", $f_number);
				$sheet->write($row_index, $col_index01+1, "0", $f_number);
				$sheet->write($row_index, $col_index01+2, "0", $f_number);
				$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
				if(array_key_exists("e1", $list_avgs))
				{
					$list_avgs["e1"] = $list_avgs["e1"] + 0;
				}
				else
				{
					$list_avgs["e1"] = 0;
				}
			}

			$row_index++;

			
			//rents
			$rents = 0;
			if(array_key_exists(2, $expenses))
			{
				$rents = $expenses[2];
			}
			if(array_key_exists(3, $expenses))
			{
				$rents = $rents + $expenses[3];
			}
			if(array_key_exists(16, $expenses))
			{
				$rents = $rents + $expenses[16];
			}
			$rents =  $rents * $exchange_rate / $factor;

			$sheet->write($row_index, $col_index01, $rents, $f_number);
			if(array_key_exists("rents", $list_avgs))
			{
				$list_avgs["rents"] = $list_avgs["rents"] + $rents;
			}
			else
			{
				$list_avgs["rents"] = $rents;
			}
			$rents_per_sqm = 0;
			if($retail_area > 0)
			{
				$rents_per_sqm = round($rents/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $rents_per_sqm, $f_number);
			$rents_per_fte = 0;
			if($ftes > 0)
			{
				$rents_per_fte = round($rents/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $rents_per_fte, $f_number);
			$percent_of_net_sales = "0.00";
			if($net_sales > 0)
			{
				$percent_of_net_sales = -1*$rents/$net_sales;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
			$row_index++;
			$total_cost = $total_cost + $rents;

			//Aux Mat.
			if(array_key_exists(4, $expenses))
			{
				$amount =  $expenses[4] * $exchange_rate / $factor;
				$sheet->write($row_index, $col_index01, $amount, $f_number);
				if(array_key_exists("e4", $list_avgs))
				{
					$list_avgs["e4"] = $list_avgs["e4"] + $amount;
				}
				else
				{
					$list_avgs["e4"] = $amount;
				}

				$expenses_per_sqm = 0;
				if($retail_area > 0)
				{
					$expenses_per_sqm = round($amount/$retail_area,2);
				}
				$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number);
				$expenses_per_fte = 0;
				if($ftes > 0)
				{
					$expenses_per_fte = round($amount/$ftes,2);
				}
				$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number);
				$percent_of_net_sales = "0.00";
				if($net_sales > 0)
				{
					$percent_of_net_sales = $amount/$net_sales;
				}
				$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
				$total_cost = $total_cost + $amount;
			}
			else
			{
				$sheet->write($row_index, $col_index01, "0", $f_number);
				$sheet->write($row_index, $col_index01+1, "0", $f_number);
				$sheet->write($row_index, $col_index01+2, "0", $f_number);
				$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
				if(array_key_exists("e4", $list_avgs))
				{
					$list_avgs["e4"] = $list_avgs["e4"] + 0;
				}
				else
				{
					$list_avgs["e4"] = 0;
				}
			}
			$row_index++;

			//Sales Admin
			if(array_key_exists(5, $expenses))
			{
				$amount =  $expenses[5] * $exchange_rate / $factor;
				$sheet->write($row_index, $col_index01, $amount, $f_number);
				if(array_key_exists("e5", $list_avgs))
				{
					$list_avgs["e5"] = $list_avgs["e5"] + $amount;
				}
				else
				{
					$list_avgs["e5"] = $amount;
				}
				$expenses_per_sqm = 0;
				if($retail_area > 0)
				{
					$expenses_per_sqm = round($amount/$retail_area,2);
				}
				$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number);
				$expenses_per_fte = 0;
				if($ftes > 0)
				{
					$expenses_per_fte = round($amount/$ftes,2);
				}
				$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number);
				$percent_of_net_sales = "0.00";
				if($net_sales > 0)
				{
					$percent_of_net_sales = -1*$amount/$net_sales;
				}
				$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
				$total_cost = $total_cost + $amount;
			}
			else
			{
				$sheet->write($row_index, $col_index01, "0", $f_number);
				$sheet->write($row_index, $col_index01+1, "0", $f_number);
				$sheet->write($row_index, $col_index01+2, "0", $f_number);
				$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
				if(array_key_exists("e5", $list_avgs))
				{
					$list_avgs["e5"] = $list_avgs["e5"] + 0;
				}
				else
				{
					$list_avgs["e5"] = 0;
				}
			}
			$row_index++;

			
			//Depreciation on Investment

			$d = calculate_depreciation($cer_basicdata, $row["project_id"], 0);
			$depreciation = $d["investments"];
				
			if(array_key_exists($benchmark_year, $depreciation))
			{
				$depreciation_benchmark_year = $depreciation[$benchmark_year];
			}
			else
			{
				$depreciation_benchmark_year = 0;
			}

			$depreciation_benchmark_year = $depreciation_benchmark_year * $exchange_rate / $factor;

			$sheet->write($row_index, $col_index01, round($depreciation_benchmark_year, 2), $f_number);
			if(array_key_exists("depreciation_benchmark_year", $list_avgs))
			{
				$list_avgs["depreciation_benchmark_year"] = $list_avgs["depreciation_benchmark_year"] + $depreciation_benchmark_year;
			}
			else
			{
				$list_avgs["depreciation_benchmark_year"] = $depreciation_benchmark_year;
			}
			$depreciation_benchmark_year_per_sqm = 0;
			if($retail_area > 0)
			{
				$depreciation_benchmark_year_per_sqm = round($depreciation_benchmark_year/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $depreciation_benchmark_year_per_sqm, $f_number);
			$depreciation_benchmark_year_per_fte = 0;
			if($ftes > 0)
			{
				$depreciation_benchmark_year_per_fte = round($depreciation_benchmark_year/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $depreciation_benchmark_year_per_fte, $f_number);
			$percent_of_net_sales = "0.00";
			if($net_sales > 0)
			{
				$percent_of_net_sales = -1*$depreciation_benchmark_year/$net_sales;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
			$row_index++;

			$total_cost = $total_cost + $depreciation_benchmark_year;

			//Depreciation on Key Money
			$depreciation_benchmark_year = 0;
			$depreciation_benchmark_year = 0;
			$prepayed_rents = $d["intangibles"];
			if(array_key_exists($benchmark_year, $depreciation))
			{
				$depreciation_benchmark_year = $prepayed_rents[$benchmark_year];
			}
			else
			{
				$depreciation_benchmark_year = 0;
			}
			
			
			
			$depreciation_benchmark_year = $depreciation_benchmark_year * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, round($depreciation_benchmark_year, 2), $f_number);
			if(array_key_exists("depreciation_benchmark_year_km", $list_avgs))
			{
				$list_avgs["depreciation_benchmark_year_km"] = $list_avgs["depreciation_benchmark_year_km"] + $depreciation_benchmark_year;
			}
			else
			{
				$list_avgs["depreciation_benchmark_year_km"] = $depreciation_benchmark_year;
			}
			$depreciation_benchmark_year_per_sqm = 0;
			if($retail_area > 0)
			{
				$depreciation_benchmark_year_per_sqm = round($depreciation_benchmark_year/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $depreciation_benchmark_year_per_sqm, $f_number);
			$depreciation_benchmark_year_per_fte = 0;
			if($ftes > 0)
			{
				$depreciation_benchmark_year_per_fte = round($depreciation_benchmark_year/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $depreciation_benchmark_year_per_fte, $f_number);
			$percent_of_net_sales = "0.00";
			if($net_sales > 0)
			{
				$percent_of_net_sales = -1*$depreciation_benchmark_year/$net_sales;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
			$row_index++;

			$total_cost = $total_cost + $depreciation_benchmark_year;

			//Other indirect expenses
			if(array_key_exists(10, $expenses))
			{
				$amount = $expenses[10] * $exchange_rate / $factor;
				$sheet->write($row_index, $col_index01, $amount, $f_number);
				if(array_key_exists("e10", $list_avgs))
				{
					$list_avgs["e10"] = $list_avgs["e10"] + $amount;
				}
				else
				{
					$list_avgs["e10"] = $amount;
				}
				$expenses_per_sqm = 0;
				if($retail_area > 0)
				{
					$expenses_per_sqm = round($amount/$retail_area,2);
				}
				$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number);
				$expenses_per_fte = 0;
				if($ftes > 0)
				{
					$expenses_per_fte = round($amount/$ftes,2);
				}
				$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number);
				$percent_of_net_sales = "0.00";
				if($net_sales > 0)
				{
					$percent_of_net_sales = -1*$amount/$net_sales;
				}
				$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
				$total_cost = $total_cost + $amount;
			}
			else
			{
				$sheet->write($row_index, $col_index01, "0", $f_number);
				$sheet->write($row_index, $col_index01+1, "0", $f_number);
				$sheet->write($row_index, $col_index01+2, "0", $f_number);
				$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
				if(array_key_exists("e10", $list_avgs))
				{
					$list_avgs["e10"] = $list_avgs["e10"] + 0;
				}
				else
				{
					$list_avgs["e10"] =0;
				}
			}
			$row_index++;

			
			// total cost
			
			$sheet->write($row_index, $col_index01, $total_cost, $f_number_bold);
			if(array_key_exists("total_cost", $list_avgs))
			{
				$list_avgs["total_cost"] = $list_avgs["total_cost"] + $total_cost;
			}
			else
			{
				$list_avgs["total_cost"] = $total_cost;
			}
			$total_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$total_cost_per_sqm = round($total_cost/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $total_cost_per_sqm, $f_number_bold);
			$total_cost_per_fte = 0;
			if($ftes > 0)
			{
				$total_cost_per_fte = round($total_cost/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $total_cost_per_fte, $f_number_bold);
			$percent_of_net_sales = "0.00";
			if($net_sales > 0)
			{
				$percent_of_net_sales = -1*$total_cost/$net_sales;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent_bold);
			$row_index++;


			//operational income
			$operational_income = $gross_margin + $marketing_expenses - $total_cost;
			$sheet->write($row_index, $col_index01, $operational_income, $f_number_bold);
			if(array_key_exists("operational_income", $list_avgs))
			{
				$list_avgs["operational_income"] = $list_avgs["operational_income"] + $operational_income;
			}
			else
			{
				$list_avgs["operational_income"] = $operational_income;
			}
			$operational_income_per_sqm = 0;
			if($retail_area > 0)
			{
				$operational_income_per_sqm = round($operational_income/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $operational_income_per_sqm, $f_number_bold);
			$operational_income_per_fte = 0;
			if($ftes > 0)
			{
				$operational_income_per_fte = round($operational_income/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $operational_income_per_fte, $f_number_bold);

			$percent_of_net_sales = "0.00";
			if($net_sales > 0)
			{
				$percent_of_net_sales = $operational_income/$net_sales;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent_bold);

			$row_index++;
			$row_index++;
		}

		

		//CMS DATA Real COST for investment section
		$total_real_asset_cost = 0;
		$real_cost_shown = false;
		if($investments_only == 1 and $row["project_cost_cms_approved"] == 1)
		{
			$grouptotals_real = array();
			$grouptotals_real[2] = 0;
			$grouptotals_real[6] = 0;
			$grouptotals_real[7] = 0;
			$grouptotals_real[8] = 0;
			$grouptotals_real[9] = 0;
			$grouptotals_real[10] = 0;
			$grouptotals_real[11] = 0;

			$total_real_asset_cost = 0;
			$total_cost_cms = 0;


			$sql_i = "select order_item_cost_group, order_item_type, ".
				   "order_item_system_price, order_item_real_system_price, order_item_quantity,  ".
				   "order_item_system_price_freezed, order_item_quantity_freezed, project_id " . 
				   "from order_items ".
				   " left join orders on order_id = order_item_order " . 
				   " left join projects on project_order = order_id " . 
				   "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
				   "   and order_item_order = " . dbquote($row["project_order"]) . 
				   "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
				   "   or order_item_type = " . ITEM_TYPE_SERVICES .
				   "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
				   " ) " .
				   " and order_item_cost_group > 0 " . 
				   " order by order_item_cost_group";

			
			$res_i = mysql_query($sql_i) or dberror($sql_i);
			while($row_i = mysql_fetch_assoc($res_i))
			{
				$tmp = ($row_i["order_item_real_system_price"] * $row_i["order_item_quantity"]);
				
				if($base_currency != "chf")
				{
					$cer_basicdata = get_cer_basicdata($row_i["project_id"]);
					$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
					$factor = $cer_basicdata["cer_basicdata_factor"];
					
					$tmp = $factor*$tmp /$exchange_rate;
				}
				$grouptotals_real[$row_i["order_item_cost_group"]] =  $grouptotals_real[$row_i["order_item_cost_group"]] + $tmp;
			}



			


			/********************************************************************
				project items coming from catalogue orders
			*********************************************************************/ 

			$oip_group_totals = array();
			$sql_oip = "select order_items_in_project_id, order_items_in_project_costgroup_id, " . 
					   "order_items_in_project_order_item_id, " . 
					   " IF(address_shortcut <> '', address_shortcut, order_item_supplier_freetext) as address_shortcut, " .
					   "order_item_text,  " .
					   "DATE_FORMAT(order_item_ordered, '%d.%m.%y') as date1, " .
						"DATE_FORMAT(order_item_pickup, '%d.%m.%y') as date2, " .
						"DATE_FORMAT(order_item_arrival, '%d.%m.%y') as date3, " .
						"TRUNCATE(sum(order_item_quantity * order_items_in_project_real_price), 2) as investment, ".
					   " order_item_po_number, project_cost_groupname_name " . 
					   " from order_items_in_projects " .
					   " left join order_items on order_item_id = order_items_in_project_order_item_id " .
					   " left join addresses on order_item_supplier_address = address_id ".
					   " left join project_cost_groupnames on project_cost_groupname_id =  order_items_in_project_costgroup_id " .
					   " where order_items_in_project_project_order_id = " . dbquote($row["project_order"]) . 
					   " group by order_items_in_project_id";

			$grouptotals_oip = 0;
			$res_oip = mysql_query($sql_oip) or dberror($sql_oip);
			while($row_oip = mysql_fetch_assoc($res_oip))
			{
				$tmp = $row_oip["investment"];
				if($base_currency != "chf")
				{
					$tmp = $factor*$tmp /$exchange_rate;
				}
				
				
				$grouptotals_oip = $grouptotals_oip  + $tmp;

				if(array_key_exists($row_oip["order_items_in_project_costgroup_id"], $oip_group_totals))
				{
					$oip_group_totals[$row_oip["order_items_in_project_costgroup_id"]] = 		$oip_group_totals[$row_oip["order_items_in_project_costgroup_id"]] + $tmp;
				}
				else
				{
					$oip_group_totals[$row_oip["order_items_in_project_costgroup_id"]] = $tmp;
				}
			}


			foreach($grouptotals_real as $key=>$value)
			{
			
				if(array_key_exists($key, $oip_group_totals))
				{
					$grouptotals_real[$key] = $grouptotals_real[$key] + $oip_group_totals[$key];
				}
			}

			$total_real_asset_cost = $grouptotals_real[7] +  $grouptotals_real[2] +  $grouptotals_real[6] + $grouptotals_real[10] +  $grouptotals_real[11] +  $grouptotals_real[9] +  $grouptotals_real[8];

			$real_cost_shown = true;

		}
		
		
		
		
		/********************************************************************
		investments
		*********************************************************************/
		
		if($real_cost_shown == true) {
			$sheet->write($row_index, $col_index01, "Real Costs", $f_black_on_grey);
		}
		else {
			$sheet->write($row_index, $col_index01, "Amount", $f_black_on_grey);
		}
		$sheet->write($row_index, $col_index01+1,"per sqm", $f_black_on_grey);
		$sheet->write($row_index, $col_index01+2,"per fte", $f_black_on_grey);
		$sheet->write($row_index, $col_index01+3,"% of invest.", $f_black_on_grey);
		
		$row_index++;

		//get total investment in fixed assets
		$total_investment = 0;
		$total_investment_in_fixed_assets = 0;

		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " . 
			     " and (cer_investment_type = 1 " . 
			     " or cer_investment_type = 3 " .
			     " or cer_investment_type = 5 " .
			     " or cer_investment_type = 7 " .
			     " or cer_investment_type = 11 " .
				 " or cer_investment_type = 9 " . 
			     " or cer_investment_type = 13 " .
			     " or cer_investment_type = 15 " .
			     " or cer_investment_type = 17) ";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$total_investment = $row_s["total"] * $exchange_rate / $factor;
		}

		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " .
			     " and (cer_investment_type = 1 " . 
			     " or cer_investment_type = 3 " .
			     " or cer_investment_type = 5 " .
			     " or cer_investment_type = 7 " .
			     " or cer_investment_type = 11) ";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$total_investment_in_fixed_assets = $row_s["total"] * $exchange_rate / $factor;
		}

		if($investments_only == 1 and $total_real_asset_cost > 0) {
			$total_investment = $total_real_asset_cost;
			$total_investment_in_fixed_assets = $total_real_asset_cost;

		}


		//get construction cost
		$construction = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " .
			     " and cer_investment_type = 1";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$construction = $row_s["total"];
		}
		$construction = $construction * $exchange_rate / $factor;

		if($investments_only == 1 and $total_real_asset_cost > 0) {
			$construction = $grouptotals_real[7];
		}

		$sheet->write($row_index, $col_index01, $construction, $f_number);
		if(array_key_exists("construction", $list_avgs))
		{
			$list_avgs["construction"] = $list_avgs["construction"] + $construction;
		}
		else
		{
			$list_avgs["construction"] = $construction;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($construction/$retail_area,2);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($construction/$ftes,2);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00";
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = $construction/$total_investment;
			
		}
		*/
		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = $construction/$total_investment_in_fixed_assets;
			
		}

		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
		$row_index++;



		//get fixturing cost
		$fixturing = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " .
			     " and cer_investment_type = 3";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$fixturing = $row_s["total"];
		}
		$fixturing = $fixturing * $exchange_rate / $factor;

		if($investments_only == 1 and $total_real_asset_cost > 0) {
			$fixturing = $grouptotals_real[2] + $grouptotals_real[6] + $grouptotals_real[10];
		}
		
		$sheet->write($row_index, $col_index01, $fixturing, $f_number);
		if(array_key_exists("fixturing", $list_avgs))
		{
			$list_avgs["fixturing"] = $list_avgs["fixturing"] + $fixturing;
		}
		else
		{
			$list_avgs["fixturing"] = $fixturing;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($fixturing/$retail_area,2);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($fixturing/$ftes,2);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00";
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = $fixturing/$total_investment;
		}
		*/

		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = $fixturing/$total_investment_in_fixed_assets;
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
		$row_index++;



		//get architectural cost
		$architectural = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " .
			     " and cer_investment_type = 5";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$architectural = $row_s["total"];
		}
		$architectural = $architectural * $exchange_rate / $factor;

		if($investments_only == 1 and $total_real_asset_cost > 0) {
			$architectural = $grouptotals_real[9];
		}
		
		$sheet->write($row_index, $col_index01, $architectural, $f_number);
		if(array_key_exists("architectural", $list_avgs))
		{
			$list_avgs["architectural"] = $list_avgs["architectural"] + $architectural;
		}
		else
		{
			$list_avgs["architectural"] = $architectural;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($architectural/$retail_area,2);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($architectural/$ftes,2);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00";
		
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = $architectural/$total_investment;
		}
		*/

		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = $architectural/$total_investment_in_fixed_assets;
		}

		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
		$row_index++;

		
		//get equipment cost
		$equipment = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " .
			     " and cer_investment_type = 7";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$equipment = $row_s["total"];
		}
		$equipment = $equipment * $exchange_rate / $factor;


		if($investments_only == 1 and $total_real_asset_cost > 0) {
		
			$equipment = $grouptotals_real[11];
		}

		$sheet->write($row_index, $col_index01, $equipment, $f_number);
		if(array_key_exists("equipment", $list_avgs))
		{
			$list_avgs["equipment"] = $list_avgs["equipment"] + $equipment;
		}
		else
		{
			$list_avgs["equipment"] = $equipment;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($equipment/$retail_area,2);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($equipment/$ftes,2);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00";
		
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = $equipment/$total_investment;
		}
		*/

		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = $equipment/$total_investment_in_fixed_assets;
		}

		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
		$row_index++;

		
		//get other cost
		$other = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " .
			     " and cer_investment_type = 11";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$other = $row_s["total"];
		}
		$other = $other * $exchange_rate / $factor;


		if($investments_only == 1 and $total_real_asset_cost > 0) {
			$other = $grouptotals_real[8];
		}
		
		$sheet->write($row_index, $col_index01, $other, $f_number);
		if(array_key_exists("other", $list_avgs))
		{
			$list_avgs["other"] = $list_avgs["other"] + $other;
		}
		else
		{
			$list_avgs["other"] = $other;
		}
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($other/$retail_area,2);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($other/$ftes,2);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00";
		
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = $other/$total_investment;
		}
		*/

		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = $other/$total_investment_in_fixed_assets;
		}

		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
		$row_index++;



		//totel investment in fixed assets

		$total_cost = 0;

		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " .
			     " and (cer_investment_type = 1 " . 
			     " or cer_investment_type = 3 " .
			     " or cer_investment_type = 5 " .
			     " or cer_investment_type = 7 " .
			     " or cer_investment_type = 11) ";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$total_cost = $row_s["total"] * $exchange_rate / $factor;
		}

		if($investments_only == 1 and $total_real_asset_cost > 0) {
			$total_cost = $total_real_asset_cost;
		}

		$sheet->write($row_index, $col_index01, $total_cost, $f_number_bold);
		
		$total_cost_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$total_cost_per_sqm = round($total_cost/$retail_area,2);
		}
		$sheet->write($row_index, $col_index01+1, $total_cost_per_sqm, $f_number_bold);
		$total_cost_cost_per_fte = 0;
		if($ftes > 0)
		{
			$total_cost_per_fte = round($total_cost/$ftes,2);
		}
		//$sheet->write($row_index, $col_index01+2, $total_cost_per_fte, $f_number_bold);
		$percent_of_total_cost = "0.00";
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = $total_cost/$total_investment;
		}
		*/

		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = $total_cost/$total_investment_in_fixed_assets;
		}


		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent_bold);


		if(array_key_exists("total_cost2", $list_avgs))
		{
			$list_avgs["total_cost2"] = $list_avgs["total_cost2"] + $total_cost;
		}
		else
		{
			$list_avgs["total_cost2"] = $total_cost;
		}
		$row_index++;


		//get intangibles
		if($investments_only == 0)
		{
			$intangibles = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and (cer_investment_type = 15 or cer_investment_type = 17)";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$intangibles = $row_s["total"];
			}
			$intangibles = $intangibles * $exchange_rate / $factor;


			$sheet->write($row_index, $col_index01, $intangibles, $f_number);
			if(array_key_exists("intangibles", $list_avgs))
			{
				$list_avgs["intangibles"] = $list_avgs["intangibles"] + $intangibles;
			}
			else
			{
				$list_avgs["intangibles"] = $intangibles;
			}
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($intangibles/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($intangibles/$ftes,2);
			}
			$percent_of_total_cost = "0.00";
			if($total_investment > 0)
			{
				$percent_of_total_cost = $intangibles/$total_investment;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
			$sheet->write($row_index, $col_index01+3, "", $f_number);

			
			
			$row_index++;

		
			//get deposit cost
			$deposit = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 9";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				if(count($cer_basicdata) > 0)
				{
					$deposit = $row_s["total"] + $cer_basicdata["cer_basicdata_recoverable_keymoney"];
				}
				else
				{
					$deposit = $row_s["total"];
				}

			}
			$deposit = $deposit * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $deposit, $f_number);
			if(array_key_exists("deposit", $list_avgs))
			{
				$list_avgs["deposit"] = $list_avgs["deposit"] + $deposit;
			}
			else
			{
				$list_avgs["deposit"] = $deposit;
			}




			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($deposit/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($deposit/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost = "0.00";
			if($total_investment > 0)
			{
				$percent_of_total_cost = $deposit/$total_investment;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
			$row_index++;

			//get noncapitalized cost
			$noncapitalized = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 13";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$noncapitalized = $row_s["total"];
			}
			$noncapitalized = $noncapitalized * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $noncapitalized, $f_number);
			if(array_key_exists("noncapitalized", $list_avgs))
			{
				$list_avgs["noncapitalized"] = $list_avgs["noncapitalized"] + $noncapitalized;
			}
			else
			{
				$list_avgs["noncapitalized"] = $noncapitalized;
			}
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($noncapitalized/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($noncapitalized/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost = "0.00";
			if($total_investment > 0)
			{
				$percent_of_total_cost = $noncapitalized/$total_investment;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
			$row_index++;

			
			
			//Total Project costs (Requested amount)
			
			if($total_investment_in_fixed_assets > 0)
			{
				$total_investment = $total_cost_cms;
			}
			
			$sheet->write($row_index, $col_index01, $total_investment, $f_number_bold);
			
			$total_cost_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$total_cost_per_sqm = round($total_investment/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $total_cost_per_sqm, $f_number_bold);
			$total_cost_cost_per_fte = 0;
			if($ftes > 0)
			{
				$total_cost_per_fte = round($total_investment/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $total_cost_per_fte, $f_number_bold);
			$percent_of_total_cost = "0.00";
			if($total_investment > 0)
			{
				$percent_of_total_cost = $total_investment/$total_investment;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number_bold);
			$sheet->write($row_index, $col_index01+3, "", $f_number_bold);


			if(array_key_exists("total_cost3", $list_avgs))
			{
				$list_avgs["total_cost3"] = $list_avgs["total_cost3"] + $total_investment;
			}
			else
			{
				$list_avgs["total_cost3"] = $total_investment;
			}

			$row_index++;
			$row_index++;
		}
		
		
		//KL Approved investments
		if($investments_only == 0)
		{
			$sheet->write($row_index, $col_index01, "Amount", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+1,"per sqm", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+2,"per fte", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+3,"% of invest.", $f_black_on_grey);
			
			$row_index++;
			//get total investment in fixed assets
			$total_investment_approved_approved = 0;
			
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and (cer_investment_type = 1 " . 
					 " or cer_investment_type = 3 " .
					 " or cer_investment_type = 5 " .
					 " or cer_investment_type = 7 " .
					 " or cer_investment_type = 11 " .
					 " or cer_investment_type = 9 " . 
					 " or cer_investment_type = 13 " .
					 " or cer_investment_type = 15 " .
					 " or cer_investment_type = 17) ";


			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$total_investment_approved = $row_s["total"] * $exchange_rate / $factor;
			}


			$total_investment_in_fixed_assets_approved = 0;
			
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and (cer_investment_type = 1 " . 
					 " or cer_investment_type = 3 " .
					 " or cer_investment_type = 5 " .
					 " or cer_investment_type = 7 " .
					 " or cer_investment_type = 11 " .
					 " or cer_investment_type = 9 " . 
					 " or cer_investment_type = 13 " .
					 " or cer_investment_type = 15 " .
					 " or cer_investment_type = 17) ";


			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$total_investment_approved = $row_s["total"] * $exchange_rate / $factor;
			}



			//totel investment in fixed assets

			$total_cost_approved = 0;

			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and (cer_investment_type = 1 " . 
					 " or cer_investment_type = 3 " .
					 " or cer_investment_type = 5 " .
					 " or cer_investment_type = 7 " .
					 " or cer_investment_type = 11) ";

			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$total_cost_approved = $row_s["total"] * $exchange_rate / $factor;
			}


			//get construction cost
			$construction = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 1";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$construction = $row_s["total"];
			}
			$construction = $construction * $exchange_rate / $factor;

			$sheet->write($row_index, $col_index01, $construction, $f_number);
			if(array_key_exists("construction_approved", $list_avgs))
			{
				$list_avgs["construction_approved"] = $list_avgs["construction_approved"] + $construction;
			}
			else
			{
				$list_avgs["construction_approved"] = $construction;
			}
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($construction/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($construction/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost = $construction/$total_cost_approved;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
			$row_index++;



			//get fixturing cost
			$fixturing = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 3";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$fixturing = $row_s["total"];
			}
			$fixturing = $fixturing * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $fixturing, $f_number);
			if(array_key_exists("fixturing_approved", $list_avgs))
			{
				$list_avgs["fixturing_approved"] = $list_avgs["fixturing_approved"] + $fixturing;
			}
			else
			{
				$list_avgs["fixturing_approved"] = $fixturing;
			}
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($fixturing/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($fixturing/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost = $fixturing/$total_cost_approved;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
			$row_index++;



			//get architectural cost
			$architectural = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 5";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$architectural = $row_s["total"];
			}
			$architectural = $architectural * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $architectural, $f_number);
			if(array_key_exists("architectural_approved", $list_avgs))
			{
				$list_avgs["architectural_approved"] = $list_avgs["architectural_approved"] + $architectural;
			}
			else
			{
				$list_avgs["architectural_approved"] = $architectural;
			}
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($architectural/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($architectural/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost = $architectural/$total_cost_approved;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
			$row_index++;

			
			//get equipment cost
			$equipment = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 7";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$equipment = $row_s["total"];
			}
			$equipment = $equipment * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $equipment, $f_number);
			if(array_key_exists("equipment_approved", $list_avgs))
			{
				$list_avgs["equipment_approved"] = $list_avgs["equipment_approved"] + $equipment;
			}
			else
			{
				$list_avgs["equipment_approved"] = $equipment;
			}
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($equipment/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($equipment/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost = $equipment/$total_cost_approved;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
			$row_index++;

			
			//get other cost
			$other = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 11";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$other = $row_s["total"];
			}
			$other = $other * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $other, $f_number);
			if(array_key_exists("other_approved", $list_avgs))
			{
				$list_avgs["other_approved"] = $list_avgs["other_approved"] + $other;
			}
			else
			{
				$list_avgs["other_approved"] = $other;
			}
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($other/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($other/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost = $other/$total_cost_approved;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
			$row_index++;


			//total fixturing
			$sheet->write($row_index, $col_index01, $total_cost_approved, $f_number_bold);
			
			
			$total_cost_approved_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$total_cost_approved_per_sqm = round($total_cost_approved/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $total_cost_approved_per_sqm, $f_number_bold);
			$total_cost_approved_cost_per_fte = 0;
			if($ftes > 0)
			{
				$total_cost_approved_per_fte = round($total_cost_approved/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $total_cost_approved_per_fte, $f_number_bold);
			$percent_of_total_cost_approved = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost_approved = $total_cost_approved/$total_cost_approved;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_total_cost_approved, $f_percent_bold);


			if(array_key_exists("total_cost_approved2", $list_avgs))
			{
				$list_avgs["total_cost_approved2"] = $list_avgs["total_cost_approved2"] + $total_cost_approved;
			}
			else
			{
				$list_avgs["total_cost_approved2"] = $total_cost_approved;
			}
			$row_index++;


			//get intangibles

			$intangibles = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) .
					 " and cer_investment_cer_version = 0 " .
					 " and (cer_investment_type = 15 or cer_investment_type = 17)";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$intangibles = $row_s["total"];
			}
			$intangibles = $intangibles * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $intangibles, $f_number);
			if(array_key_exists("intangibles_approved", $list_avgs))
			{
				$list_avgs["intangibles_approved"] = $list_avgs["intangibles_approved"] + $intangibles;
			}
			else
			{
				$list_avgs["intangibles_approved"] = $intangibles;
			}
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($intangibles/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($intangibles/$ftes,2);
			}
			$percent_of_total_cost_approved = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost_approved = $intangibles/$total_investment_approved;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost_approved, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_number);

			
			
			$row_index++;

		
			//get deposit cost
			$deposit = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 9";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$deposit = $row_s["total"];
			}
			$deposit = $deposit * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $deposit, $f_number);
			if(array_key_exists("deposit_approved", $list_avgs))
			{
				$list_avgs["deposit_approved"] = $list_avgs["deposit_approved"] + $deposit;
			}
			else
			{
				$list_avgs["deposit_approved"] = $deposit;
			}
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($deposit/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($deposit/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost_approved = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost_approved = $deposit/$total_investment_approved;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost_approved, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
			$row_index++;

			//get noncapitalized cost
			$noncapitalized = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) .
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 13";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$noncapitalized = $row_s["total"];
			}
			$noncapitalized = $noncapitalized * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $noncapitalized, $f_number);
			if(array_key_exists("noncapitalized_approved", $list_avgs))
			{
				$list_avgs["noncapitalized_approved"] = $list_avgs["noncapitalized_approved"] + $noncapitalized;
			}
			else
			{
				$list_avgs["noncapitalized_approved"] = $noncapitalized;
			}
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($noncapitalized/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($noncapitalized/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost_approved = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost_approved = $noncapitalized/$total_investment_approved;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost_approved, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
			$row_index++;

			
			
			//Total Project costs (Requested amount)
			$sheet->write($row_index, $col_index01, $total_investment_approved, $f_number_bold);
			
			$total_cost_approved_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$total_cost_approved_per_sqm = round($total_investment_approved/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $total_cost_approved_per_sqm, $f_number_bold);
			$total_cost_approved_cost_per_fte = 0;
			if($ftes > 0)
			{
				$total_cost_approved_per_fte = round($total_investment_approved/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $total_cost_approved_per_fte, $f_number_bold);
			$percent_of_total_cost_approved = "0.00";
			if($total_cost_approved > 0)
			{
				$percent_of_total_cost_approved = $total_investment_approved/$total_investment_approved;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost_approved, $f_percent_bold);
			$sheet->write($row_index, $col_index01+3, "", $f_number);


			if(array_key_exists("total_cost_approved3", $list_avgs))
			{
				$list_avgs["total_cost_approved3"] = $list_avgs["total_cost_approved3"] + $total_investment_approved;
			}
			else
			{
				$list_avgs["total_cost_approved3"] = $total_investment_approved;
			}

			$row_index++;
			$row_index++;
		}
	

	
		//CMS DATA Real COST
		if($investments_only == 0)
		{
			$grouptotals_real = array();
			$grouptotals_real[2] = 0;
			$grouptotals_real[6] = 0;
			$grouptotals_real[7] = 0;
			$grouptotals_real[8] = 0;
			$grouptotals_real[9] = 0;
			$grouptotals_real[10] = 0;
			$grouptotals_real[11] = 0;

			$total_real_asset_cost = 0;
			$total_cost_cms = 0;


			$sql_i = "select order_item_cost_group, order_item_type, ".
				   "order_item_system_price, order_item_real_system_price, order_item_quantity,  ".
				   "order_item_system_price_freezed, order_item_quantity_freezed, project_id " . 
				   "from order_items ".
				   " left join orders on order_id = order_item_order " . 
				   " left join projects on project_order = order_id " . 
				   "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
				   "   and order_item_order = " . dbquote($row["project_order"]) . 
				   "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
				   "   or order_item_type = " . ITEM_TYPE_SERVICES .
				   "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
				   " ) " .
				   " and order_item_cost_group > 0 " . 
				   " order by order_item_cost_group";

			
			$res_i = mysql_query($sql_i) or dberror($sql_i);
			while($row_i = mysql_fetch_assoc($res_i))
			{
				$tmp = ($row_i["order_item_real_system_price"] * $row_i["order_item_quantity"]);
				
				if($base_currency != "chf")
				{
					$cer_basicdata = get_cer_basicdata($row_i["project_id"]);
					$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
					$factor = $cer_basicdata["cer_basicdata_factor"];
					
					$tmp = $factor*$tmp /$exchange_rate;
				}
				$grouptotals_real[$row_i["order_item_cost_group"]] =  $grouptotals_real[$row_i["order_item_cost_group"]] + $tmp;
			}






			/********************************************************************
				project items coming from catalogue orders
			*********************************************************************/ 

			$oip_group_totals = array();
			$sql_oip = "select order_items_in_project_id, order_items_in_project_costgroup_id, " . 
					   "order_items_in_project_order_item_id, " . 
					   " IF(address_shortcut <> '', address_shortcut, order_item_supplier_freetext) as address_shortcut, " .
					   "order_item_text,  " .
					   "DATE_FORMAT(order_item_ordered, '%d.%m.%y') as date1, " .
						"DATE_FORMAT(order_item_pickup, '%d.%m.%y') as date2, " .
						"DATE_FORMAT(order_item_arrival, '%d.%m.%y') as date3, " .
						"TRUNCATE(sum(order_item_quantity * order_items_in_project_real_price), 2) as investment, ".
					   " order_item_po_number, project_cost_groupname_name " . 
					   " from order_items_in_projects " .
					   " left join order_items on order_item_id = order_items_in_project_order_item_id " .
					   " left join addresses on order_item_supplier_address = address_id ".
					   " left join project_cost_groupnames on project_cost_groupname_id =  order_items_in_project_costgroup_id " .
					   " where order_items_in_project_project_order_id = " . dbquote($row["project_order"]) . 
					   " group by order_items_in_project_id";

			$grouptotals_oip = 0;
			$res_oip = mysql_query($sql_oip) or dberror($sql_oip);
			while($row_oip = mysql_fetch_assoc($res_oip))
			{
				$tmp = $row_oip["investment"];
				if($base_currency != "chf")
				{
					$tmp = $factor*$tmp /$exchange_rate;
				}
				
				
				$grouptotals_oip = $grouptotals_oip  + $tmp;

				if(array_key_exists($row_oip["order_items_in_project_costgroup_id"], $oip_group_totals))
				{
					$oip_group_totals[$row_oip["order_items_in_project_costgroup_id"]] = 		$oip_group_totals[$row_oip["order_items_in_project_costgroup_id"]] + $tmp;
				}
				else
				{
					$oip_group_totals[$row_oip["order_items_in_project_costgroup_id"]] = $tmp;
				}
			}


			foreach($grouptotals_real as $key=>$value)
			{
			
				if(array_key_exists($key, $oip_group_totals))
				{
					$grouptotals_real[$key] = $grouptotals_real[$key] + $oip_group_totals[$key];
				}
			}

			$total_real_asset_cost = $grouptotals_real[7] +  $grouptotals_real[2] +  $grouptotals_real[6] +  $grouptotals_real[10] +  $grouptotals_real[11] +  $grouptotals_real[9] +  $grouptotals_real[8];



			$sheet->write($row_index, $col_index01, "Amount", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+1,"per sqm", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+2,"per fte", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+3,"% of Cost", $f_black_on_grey);
			
			$row_index++;

			//construction
			$amount = 0;
			
			$amount = $grouptotals_real[7];

			$sheet->write($row_index, $col_index01, $amount, $f_number);
			if(array_key_exists("cms2", $list_avgs))
			{
				$list_avgs["cms2"] = $list_avgs["cms2"] + $amount;
			}
			else
			{
				$list_avgs["cms2"] = $amount;
			}
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($total_real_asset_cost > 0)
			{
				//$percent_of_cer = round($amount/$construction,2);
				$percent_of_cer = round($amount/$total_real_asset_cost,2);
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$row_index++;

			$total_cost_cms = $total_cost_cms + $amount;

			//fixturing
			$amount = 0;
			$amount = $grouptotals_real[2] + $grouptotals_real[6] + $grouptotals_real[10];

			$sheet->write($row_index, $col_index01, $amount, $f_number);
			if(array_key_exists("cms3", $list_avgs))
			{
				$list_avgs["cms3"] = $list_avgs["cms3"] + $amount;
			}
			else
			{
				$list_avgs["cms3"] = $amount;
			}
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($total_real_asset_cost > 0)
			{
				//$percent_of_cer = round($amount/$fixturing,2);
				$percent_of_cer = round($amount/$total_real_asset_cost,2);
				
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$row_index++;

			$total_cost_cms = $total_cost_cms + $amount;

			//architectural
			$amount = 0;
			$amount = $grouptotals_real[9];

			$sheet->write($row_index, $col_index01, $amount, $f_number);
			if(array_key_exists("cms4", $list_avgs))
			{
				$list_avgs["cms4"] = $list_avgs["cms4"] + $amount;
			}
			else
			{
				$list_avgs["cms4"] = $amount;
			}
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($total_real_asset_cost > 0)
			{
				//$percent_of_cer = round($amount/$architectural,2);
				$percent_of_cer = round($amount/$total_real_asset_cost,2);
				
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$row_index++;

			$total_cost_cms = $total_cost_cms + $amount;

			//equipment
			$amount = 0;
			$amount = $grouptotals_real[11];

			$sheet->write($row_index, $col_index01, $amount, $f_number);
			if(array_key_exists("cms5", $list_avgs))
			{
				$list_avgs["cms5"] = $list_avgs["cms5"] + $amount;
			}
			else
			{
				$list_avgs["cms5"] = $amount;
			}
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($total_real_asset_cost > 0)
			{
				//$percent_of_cer = round($amount/$equipment,2);
				$percent_of_cer = round($amount/$total_real_asset_cost,2);
				
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$row_index++;

			$total_cost_cms = $total_cost_cms + $amount;

			//other
			$amount = 0;
			$amount = $grouptotals_real[8];

			$sheet->write($row_index, $col_index01, $amount, $f_number);
			if(array_key_exists("cms6", $list_avgs))
			{
				$list_avgs["cms6"] = $list_avgs["cms6"] + $amount;
			}
			else
			{
				$list_avgs["cms6"] = $amount;
			}
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($total_real_asset_cost > 0)
			{
				//$percent_of_cer = round($amount/$other,2);
				$percent_of_cer = round($amount/$total_real_asset_cost,2);
				
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$row_index++;

			$total_cost_cms = $total_cost_cms + $amount;
			
			


			//total real cost fixed assets
			$sheet->write($row_index, $col_index01, $total_real_asset_cost, $f_number_bold);
			if(array_key_exists("cms_asste_total", $list_avgs))
			{
				$list_avgs["cms_asste_total"] = $list_avgs["cms_asste_total"] + $total_real_asset_cost;
			}
			else
			{
				$list_avgs["cms_asste_total"] = $total_real_asset_cost;
			}
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($total_real_asset_cost/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number_bold);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($total_real_asset_cost/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number_bold);
			$percent_of_cer = "0.00";
			if($total_real_asset_cost > 0)
			{
				$percent_of_cer = round($total_real_asset_cost/$total_real_asset_cost,2);
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent_bold);
			$row_index++;


		
			//get kemoney cost
			$amount = 0;
			
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) .
					 " and cer_investment_cer_version = 0 " .
					 " and (cer_investment_type = 15 or cer_investment_type = 17)";


			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$amount = $row_s["total"];
			}

			$amount = $amount * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $amount, $f_number);
			if(array_key_exists("cms1", $list_avgs))
			{
				$list_avgs["cms1"] = $list_avgs["cms1"] + $amount;
			}
			else
			{
				$list_avgs["cms1"] = $amount;
			}
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($intangibles > 0)
			{
				$percent_of_cer = round($amount/$intangibles,2);
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_percent);
			$total_cost_cms = $total_cost_cms + $amount;

			$row_index++;
		
			
			//deposit
			$amount = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 9";


			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$amount = $row_s["total"];
			}
			$amount = $amount * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $amount, $f_number);
			if(array_key_exists("cms7", $list_avgs))
			{
				$list_avgs["cms7"] = $list_avgs["cms7"] + $amount;
			}
			else
			{
				$list_avgs["cms7"] = $amount;
			}
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($deposit > 0)
			{
				$percent_of_cer = round($amount/$deposit,2);
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_percent);
			$total_cost_cms = $total_cost_cms + $amount;

			$row_index++;
			

			//noncapitalized
			$amount = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 13";
			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$amount = $row_s["total"];
			}
			$amount = $amount * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $amount, $f_number);
			if(array_key_exists("cms8", $list_avgs))
			{
				$list_avgs["cms8"] = $list_avgs["cms8"] + $amount;
			}
			else
			{
				$list_avgs["cms8"] = $amount;
			}
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($noncapitalized > 0)
			{
				$percent_of_cer = round($amount/$noncapitalized,2);
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_percent);
			$total_cost_cms = $total_cost_cms + $amount;
			$row_index++;
			

			//total cost
			$sheet->write($row_index, $col_index01, $total_cost_cms, $f_number_bold);
			if(array_key_exists("cms_total", $list_avgs))
			{
				$list_avgs["cms_total"] = $list_avgs["cms_total"] + $total_cost_cms;
			}
			else
			{
				$list_avgs["cms_total"] = $total_cost_cms;
			}
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($total_cost_cms/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number_bold);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($total_cost_cms/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number_bold);
			$percent_of_cer = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_cer = round($total_cost_cms/$total_investment_approved,2);
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent_bold);
			$sheet->write($row_index, $col_index01+3, "", $f_percent_bold);
			$row_index++;
			$row_index++;
		}
		
		$col_index01 = $col_index01 + 5;
		$col_index02 = $col_index02 + 5;
	}
}



//print average column
if($num_of_projects > 0)
{
	$row_index = 3;

	$sheet->setColumn($col_index01, $col_index01, 9);
	$sheet->setColumn($col_index01+1, $col_index01+1, 9);
	$sheet->setColumn($col_index01+2, $col_index01+2, 9);
	$sheet->setColumn($col_index01+3, $col_index01+3, 12);
	$sheet->setColumn($col_index01+4, $col_index01+4, 2);
	
	
	if(1==2 and $investments_only == 1)
	{
		$sheet->write($row_index, $col_index01, "Average", $f_white_on_green_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_white_on_green_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_white_on_green_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_white_on_green_border_right);
	}
	else
	{
		$sheet->write($row_index, $col_index01, "Average", $f_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_border_right);
	}
	
	
	
	
	$row_index++;
	$sheet->write($row_index, $col_index01, "Project count: " . $num_of_projects, $f_border_left);
	$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+3,"", $f_border_right);
	$row_index++;
	$sheet->write($row_index, $col_index01, "", $f_border_left);
	$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+3,"", $f_border_right);
	$row_index++;
	$sheet->write($row_index, $col_index01, "", $f_border_left);
	$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+3,"", $f_border_right);
	$row_index++;
	$sheet->write($row_index, $col_index01, "", $f_border_left);
	$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
	$sheet->write($row_index, $col_index01+3,"", $f_border_right);
	$row_index++;
	$row_index++;

	if($investments_only == 0)
	{
		$sheet->write($row_index, $col_index01, "Amount", $f_black_on_grey);
		$sheet->write($row_index, $col_index01+1,"per sqm", $f_black_on_grey);
		$sheet->write($row_index, $col_index01+2,"per fte", $f_black_on_grey);
		$sheet->write($row_index, $col_index01+3,"% of net sales", $f_black_on_grey);
		
		$row_index = 12;
	}
	else {
		$row_index = 10;
	}

	if($base_currency == "chf")
	{
		$sheet->write($row_index, $col_index01, "CHF", $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01, "", $f_number);
	}
	
	$row_index++;

	$avg = round($list_avgs["project_cost_sqms"]/$num_of_projects, 2);
	$sheet->write($row_index, $col_index01, $avg, $f_number);
	
	if($investments_only == 0)
	{
		$row_index++;
	}
	else
	{
		$row_index++;
		$row_index++;
	}

	
	if($investments_only == 0)
	{
		$avg = round($list_avgs["ftes"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);

		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["ftes"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}

		$row_index++;

		$avg = round($list_avgs["lease_period"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		$row_index++;

		$avg = round($list_avgs["watches"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);

		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["watches"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["watches"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}

		$row_index++;

		$avg = round($list_avgs["jewellery"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["jewellery"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["jewellery"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		$row_index++;


			

		$avg = round($list_avgs["watches_price"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		$row_index++;

		$avg = round($list_avgs["jewellery_price"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		$row_index++;

		

		//revenues
		$percent_of_net_sales = 0;
		if($num_of_projects > 0) 
		{
			$total_avg_revenue = $list_avgs["gross_sales"]/$num_of_projects;
			$percent_base = 100 + ($list_avgs["sales_reduction_percent"]/$num_of_projects);
			$base_unit = $total_avg_revenue / $percent_base;
		}
		else
		{
			$total_avg_revenue = 0;
			$percent_base = 100;
			$base_unit = $total_avg_revenue / $percent_base;
		}


		$avg = round($list_avgs["watches_revenue"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["watches_revenue"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["watches_revenue"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}

		$percent_of_watches = "0.00";
		$percent_of_net_sales = 0;
		if($list_avgs["net_sales"] > 0 and $num_of_projects > 0) 
		{
			$avg = $list_avgs["watches_revenue"]/$num_of_projects;
			$percent_of_watches = $avg/$list_avgs["net_sales"];
			$percent_of_net_sales = $avg/$list_avgs["net_sales"];
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_watches, $f_percent);

		$row_index++;

		
		$avg = round($list_avgs["jewellery_revenue"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["jewellery_revenue"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["jewellery_revenue"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}

		$percent_of_jewellery = "0.00";
		if($list_avgs["net_sales"] > 0 and $num_of_projects > 0) 
		{
			$avg = $list_avgs["jewellery_revenue"]/$num_of_projects;
			$percent_of_jewellery = $avg/$list_avgs["net_sales"];
			$percent_of_net_sales = $percent_of_net_sales + $avg/$list_avgs["net_sales"];
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_jewellery, $f_percent);

		$row_index++;
		
		$avg = round($list_avgs["services_revenue"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["services_revenue"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["services_revenue"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}

		$percent_of_service = "0.00";
		if($list_avgs["net_sales"] > 0 and $num_of_projects > 0) 
		{
			$avg = $list_avgs["services_revenue"]/$num_of_projects;
			$percent_of_service = $avg/$list_avgs["net_sales"];
			$percent_of_net_sales = $percent_of_net_sales + $avg/$list_avgs["net_sales"];
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_service, $f_percent);

		$row_index++;

		$avg = round($list_avgs["gross_sales"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number_bold);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["gross_sales"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["gross_sales"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}

		$sheet->write($row_index, $col_index01+3, $percent_of_net_sales , $f_percent_bold);

		$row_index++;

		$avg = round($list_avgs["sales_reduction"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["sales_reduction"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["sales_reduction"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}

		if($list_avgs["net_sales"] > 0)
		{
			$sales_reduction_percent = $list_avgs["sales_reduction"] / $list_avgs["net_sales"];
		}
		else
		{
			$sales_reduction_percent = 0;
		}
		$sales_reduction_percent = -1*$sales_reduction_percent;
		$sheet->write($row_index, $col_index01+3, $sales_reduction_percent, $f_percent_bold);
		
		$row_index++;

		
		
		//net Sales
		$avg = round($list_avgs["net_sales"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number_bold);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["net_sales"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["net_sales"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}

		$sheet->write($row_index, $col_index01+3, "1.00", $f_percent_bold);
		$row_index++;


		//material of products sold
		$avg = round($list_avgs["e15"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["e15"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["e15"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["net_sales"] > 0)
		{
			$avg = round($list_avgs["e15"]/$list_avgs["net_sales"], 2);
			$sheet->write($row_index, $col_index01+3, -1*$avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;


		//gross margin
		$avg = round($list_avgs["gross_margin"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number_bold);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["gross_margin"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number_bold);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["gross_margin"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number_bold);
		}
		if($list_avgs["net_sales"] > 0)
		{
			$avg = round($list_avgs["gross_margin"]/$list_avgs["net_sales"], 2);
			$sheet->write($row_index, $col_index01+3, $avg , $f_percent_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number_bold);
		}

		$row_index++;


		//marketing expenses
		$avg = round($list_avgs["e13"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number_bold);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["e13"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["e13"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["net_sales"] > 0)
		{
			$avg = round($list_avgs["e13"]/$list_avgs["net_sales"], 2);
			$sheet->write($row_index, $col_index01+3, $avg , $f_percent_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number_bold);
		}
		$row_index++;



		$avg = round($list_avgs["e1"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["e1"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["e1"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["net_sales"] > 0)
		{
			$avg = round($list_avgs["e1"]/$list_avgs["net_sales"], 2);
			$sheet->write($row_index, $col_index01+3, -1*$avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;

		$avg = round($list_avgs["rents"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["rents"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["rents"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["net_sales"] > 0)
		{
			$avg = round($list_avgs["rents"]/$list_avgs["net_sales"], 2);
			$sheet->write($row_index, $col_index01+3, -1*$avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;

		$avg = round($list_avgs["e4"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["e4"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["e4"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["net_sales"] > 0)
		{
			$avg = round($list_avgs["e4"]/$list_avgs["net_sales"], 2);
			$sheet->write($row_index, $col_index01+3, -1*$avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;

		$avg = round($list_avgs["e5"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["e5"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["e5"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["net_sales"] > 0)
		{
			$avg = round($list_avgs["e5"]/$list_avgs["net_sales"], 2);
			$sheet->write($row_index, $col_index01+3, -1*$avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;




		//depreciation

		$avg = round($list_avgs["depreciation_benchmark_year"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);

		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["depreciation_benchmark_year"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["depreciation_benchmark_year"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["net_sales"] > 0)
		{
			$avg = round($list_avgs["depreciation_benchmark_year"]/$list_avgs["net_sales"], 2);
			$sheet->write($row_index, $col_index01+3, -1*$avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		
		$row_index++;




		$avg = round($list_avgs["depreciation_benchmark_year_km"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["depreciation_benchmark_year_km"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["depreciation_benchmark_year_km"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["net_sales"] > 0)
		{
			$avg = round($list_avgs["depreciation_benchmark_year_km"]/$list_avgs["net_sales"], 2);
			$sheet->write($row_index, $col_index01+3, -1*$avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;

		$avg = round($list_avgs["e10"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["e10"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["e10"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["net_sales"] > 0)
		{
			$avg = round($list_avgs["e10"]/$list_avgs["net_sales"], 2);
			$sheet->write($row_index, $col_index01+3, -1*$avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;

		
		//total cost
		$avg = round($list_avgs["total_cost"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number_bold);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["total_cost"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["total_cost"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["net_sales"] > 0)
		{
			$avg = round($list_avgs["total_cost"]/$list_avgs["net_sales"], 2);
			$sheet->write($row_index, $col_index01+3, -1*$avg , $f_percent_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;


		//operational income
		$avg = round($list_avgs["operational_income"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number_bold);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["operational_income"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["operational_income"]/$list_avgs["ftes"], 2);
			$sheet->write($row_index, $col_index01+2, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["net_sales"] > 0)
		{
			$avg = round($list_avgs["operational_income"]/$list_avgs["net_sales"], 2);
			$sheet->write($row_index, $col_index01+3, $avg , $f_percent_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;
		$row_index++;
	}

	
	
	//investments
	$sheet->write($row_index, $col_index01, "Amount", $f_black_on_grey);
	$sheet->write($row_index, $col_index01+1,"per sqm", $f_black_on_grey);
	$sheet->write($row_index, $col_index01+2,"per fte", $f_black_on_grey);
	$sheet->write($row_index, $col_index01+3,"% of invest.", $f_black_on_grey);
	
	$row_index++;

	
	$avg = round($list_avgs["construction"]/$num_of_projects, 2);
	$sheet->write($row_index, $col_index01, $avg, $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["construction"]/$list_avgs["project_cost_sqms"], 2);
		$sheet->write($row_index, $col_index01+1, $avg, $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["construction"]/$list_avgs["ftes"], 2);
		//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round($list_avgs["construction"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["fixturing"]/$num_of_projects, 2);
	$sheet->write($row_index, $col_index01, $avg, $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["fixturing"]/$list_avgs["project_cost_sqms"], 2);
		$sheet->write($row_index, $col_index01+1, $avg, $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["fixturing"]/$list_avgs["ftes"], 2);
		//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round($list_avgs["fixturing"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["architectural"]/$num_of_projects, 2);
	$sheet->write($row_index, $col_index01, $avg, $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["architectural"]/$list_avgs["project_cost_sqms"], 2);
		$sheet->write($row_index, $col_index01+1, $avg, $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["architectural"]/$list_avgs["ftes"], 2);
		//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round($list_avgs["architectural"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
	}
	$row_index++;

	$avg = round($list_avgs["equipment"]/$num_of_projects, 2);
	$sheet->write($row_index, $col_index01, $avg, $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["equipment"]/$list_avgs["project_cost_sqms"], 2);
		$sheet->write($row_index, $col_index01+1, $avg, $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["equipment"]/$list_avgs["ftes"], 2);
		//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round($list_avgs["equipment"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
	}
	$row_index++;
	
	$avg = round($list_avgs["other"]/$num_of_projects, 2);
	$sheet->write($row_index, $col_index01, $avg, $f_number);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["other"]/$list_avgs["project_cost_sqms"], 2);
		$sheet->write($row_index, $col_index01+1, $avg, $f_number);
	}
	else
	{
		$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["other"]/$list_avgs["ftes"], 2);
		//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
	}
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round($list_avgs["other"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
	}
	$row_index++;


	
	$avg = round($list_avgs["total_cost2"]/$num_of_projects, 2);
	$sheet->write($row_index, $col_index01, $avg, $f_number_bold);
	if($list_avgs["project_cost_sqms"] > 0)
	{
		$avg = round($list_avgs["total_cost2"]/$list_avgs["project_cost_sqms"], 2);
		
		if(1==2 and $investments_only == 1)
		{
			$sheet->write($row_index, $col_index01+1, $avg, $f_white_on_green);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, $avg, $f_number_bold);
		}
	}
	else
	{
		if(1==2 and $investments_only == 1)
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_white_on_green);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number_bold);
		}
	}
	if($list_avgs["ftes"] > 0)
	{
		$avg = round($list_avgs["total_cost2"]/$list_avgs["ftes"], 2);
		//$sheet->write($row_index, $col_index01+2, $avg, $f_number_bold);
	}
	else
	{
		//$sheet->write($row_index, $col_index01+2, "0.00", $f_number_bold);
	}
	
	
	if($list_avgs["total_cost2"] > 0)
	{
		$avg = round($list_avgs["total_cost2"]/$list_avgs["total_cost2"], 2);
		$sheet->write($row_index, $col_index01+3, $avg , $f_percent_bold);
	}
	else
	{
		$sheet->write($row_index, $col_index01+3, "0.00", $f_number_bold);
	}

	$row_index++;


	if($investments_only == 0)
	{
		$avg = round($list_avgs["intangibles"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["intangibles"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["intangibles"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		/*
		if($list_avgs["total_cost2"] > 0)
		{
			$avg = round($list_avgs["intangibles"]/$list_avgs["total_cost2"], 2);
			$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		*/
		$sheet->write($row_index, $col_index01+3, "", $f_number);
		$row_index++;
		
		

		
		$avg = round($list_avgs["deposit"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["deposit"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["deposit"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		/*
		if($list_avgs["total_cost2"] > 0)
		{
			$avg = round($list_avgs["deposit"]/$list_avgs["total_cost2"], 2);
			$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		*/
		$sheet->write($row_index, $col_index01+3, "", $f_number);
		$row_index++;
		
		$avg = round($list_avgs["noncapitalized"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["noncapitalized"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["noncapitalized"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		
		/*
		if($list_avgs["total_cost2"] > 0)
		{
			$avg = round($list_avgs["noncapitalized"]/$list_avgs["total_cost2"], 2);
			$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		*/
		$sheet->write($row_index, $col_index01+3, "", $f_number);

		$row_index++;


		$avg = round($list_avgs["total_cost3"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number_bold);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["total_cost3"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number_bold);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["total_cost3"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number_bold);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number_bold);
		}
		
		/*
		if($list_avgs["total_cost3"] > 0)
		{
			$avg = round($list_avgs["total_cost3"]/$list_avgs["total_cost3"], 2);
			$sheet->write($row_index, $col_index01+3, $avg , $f_percent_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number_bold);
		}
		*/
		$sheet->write($row_index, $col_index01+3, "", $f_number_bold);
		
		$row_index++;
		$row_index++;
	}


	
	//KL approved investment
	if($investments_only == 0)
	{
		$sheet->write($row_index, $col_index01, "Amount", $f_black_on_grey);
		$sheet->write($row_index, $col_index01+1,"per sqm", $f_black_on_grey);
		$sheet->write($row_index, $col_index01+2,"per fte", $f_black_on_grey);
		$sheet->write($row_index, $col_index01+3,"% of invest.", $f_black_on_grey);

		$row_index++;

		$avg = round($list_avgs["construction_approved"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["construction_approved"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["construction_approved"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["total_cost_approved2"] > 0)
		{
			$avg = round($list_avgs["construction_approved"]/$list_avgs["total_cost_approved2"], 2);
			$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;

		$avg = round($list_avgs["fixturing_approved"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["fixturing_approved"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["fixturing_approved"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["total_cost_approved2"] > 0)
		{
			$avg = round($list_avgs["fixturing_approved"]/$list_avgs["total_cost_approved2"], 2);
			$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;

		$avg = round($list_avgs["architectural_approved"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["architectural_approved"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["architectural_approved"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["total_cost_approved2"] > 0)
		{
			$avg = round($list_avgs["architectural_approved"]/$list_avgs["total_cost_approved2"], 2);
			$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;

		$avg = round($list_avgs["equipment_approved"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["equipment_approved"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["equipment_approved"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["total_cost_approved2"] > 0)
		{
			$avg = round($list_avgs["equipment_approved"]/$list_avgs["total_cost_approved2"], 2);
			$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;
		
		$avg = round($list_avgs["other_approved"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["other_approved"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["other_approved"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["total_cost_approved2"] > 0)
		{
			$avg = round($list_avgs["other_approved"]/$list_avgs["total_cost_approved2"], 2);
			$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;


		
		$avg = round($list_avgs["total_cost_approved2"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number_bold);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["total_cost_approved2"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number_bold);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["total_cost_approved2"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number_bold);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number_bold);
		}
		if($list_avgs["total_cost_approved2"] > 0)
		{
			$avg = round($list_avgs["total_cost_approved2"]/$list_avgs["total_cost_approved2"], 2);
			$sheet->write($row_index, $col_index01+3, $avg , $f_percent_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number_bold);
		}

		$row_index++;


		
		
		
		$avg = round($list_avgs["intangibles_approved"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["intangibles_approved"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["intangibles_approved"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["total_cost_approved2"] > 0)
		{
			$avg = round($list_avgs["intangibles_approved"]/$list_avgs["total_cost_approved2"], 2);
			//$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "", $f_number);
		}
		$row_index++;

		
		$avg = round($list_avgs["deposit_approved"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["deposit_approved"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["deposit_approved"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["total_cost_approved2"] > 0)
		{
			$avg = round($list_avgs["deposit_approved"]/$list_avgs["total_cost_approved2"], 2);
			//$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
		}
		$row_index++;
		
		$avg = round($list_avgs["noncapitalized_approved"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["noncapitalized_approved"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["noncapitalized_approved"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["total_cost_approved2"] > 0)
		{
			$avg = round($list_avgs["noncapitalized_approved"]/$list_avgs["total_cost_approved2"], 2);
			//$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
		}
		$row_index++;


		$avg = round($list_avgs["total_cost_approved3"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number_bold);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["total_cost_approved3"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number_bold);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["total_cost_approved3"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number_bold);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number_bold);
		}
		if($list_avgs["total_cost_approved3"] > 0)
		{
			$avg = round($list_avgs["total_cost_approved3"]/$list_avgs["total_cost_approved3"], 2);
			//$sheet->write($row_index, $col_index01+3, $avg , $f_percent_bold);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+3, "0.00", $f_number_bold);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
		}
		
		
		$row_index++;
		$row_index++;
	}


	
	//CMS
	if($investments_only == 0)
	{
		$sheet->write($row_index, $col_index01, "Amount", $f_black_on_grey);
		$sheet->write($row_index, $col_index01+1,"per sqm", $f_black_on_grey);
		$sheet->write($row_index, $col_index01+2,"per fte", $f_black_on_grey);
		$sheet->write($row_index, $col_index01+3,"% of Cost", $f_black_on_grey);
		
		$row_index++;
		

		$avg = round($list_avgs["cms2"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["cms2"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["cms2"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["cms_asste_total"] > 0)
		{
			//$avg = round($list_avgs["cms2"]/$list_avgs["construction"], 2);
			$avg = round($list_avgs["cms2"]/$list_avgs["cms_asste_total"], 2);
			$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;

		$avg = round($list_avgs["cms3"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["cms3"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["cms3"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["cms_asste_total"] > 0)
		{
			//$avg = round($list_avgs["cms3"]/$list_avgs["fixturing"], 2);
			$avg = round($list_avgs["cms3"]/$list_avgs["cms_asste_total"], 2);
			
			$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;

		$avg = round($list_avgs["cms4"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["cms4"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["cms4"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["cms_asste_total"] > 0)
		{
			//$avg = round($list_avgs["cms4"]/$list_avgs["architectural"], 2);
			$avg = round($list_avgs["cms4"]/$list_avgs["cms_asste_total"], 2);

			
			$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;

		$avg = round($list_avgs["cms5"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["cms5"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["cms5"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["cms_asste_total"] > 0)
		{
			//$avg = round($list_avgs["cms5"]/$list_avgs["equipment"], 2);
			$avg = round($list_avgs["cms5"]/$list_avgs["cms_asste_total"], 2);
			
			$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;

		$avg = round($list_avgs["cms6"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["cms6"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["cms6"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["cms_asste_total"] > 0)
		{
			//$avg = round($list_avgs["cms6"]/$list_avgs["other"], 2);
			$avg = round($list_avgs["cms6"]/$list_avgs["cms_asste_total"], 2);
			
			$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;

		

		//total real cost of fixed assets
		$avg = round($list_avgs["cms_asste_total"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number_bold);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["cms_asste_total"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number_bold);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["cms_asste_total"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number_bold);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number_bold);
		}
		if($list_avgs["cms_asste_total"] > 0)
		{
			//$avg = round($list_avgs["cms_asste_total"]/$list_avgs["total_cost2"], 2);
			$avg = round($list_avgs["cms_asste_total"]/$list_avgs["cms_asste_total"], 2);
			
			$sheet->write($row_index, $col_index01+3, $avg , $f_percent_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
		}
		$row_index++;

		

		
		$avg = round($list_avgs["cms1"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["cms1"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["cms1"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["intangibles"] > 0)
		{
			$avg = round($list_avgs["cms1"]/$list_avgs["intangibles"], 2);
			//$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_percent);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
		}
		
		$row_index++;

		
		
		
		$avg = round($list_avgs["cms7"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["cms7"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["cms7"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["deposit"] > 0)
		{
			$avg = round($list_avgs["cms7"]/$list_avgs["deposit"], 2);
			//$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_percent);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
		}
		$row_index++;

		
		
		$avg = round($list_avgs["cms8"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["cms8"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["cms8"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number);
		}
		if($list_avgs["noncapitalized"] > 0)
		{
			$avg = round($list_avgs["cms8"]/$list_avgs["noncapitalized"], 2);
			//$sheet->write($row_index, $col_index01+3, $avg, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_percent);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
		}
		$row_index++;

		
		
		$avg = round($list_avgs["cms_total"]/$num_of_projects, 2);
		$sheet->write($row_index, $col_index01, $avg, $f_number_bold);
		if($list_avgs["project_cost_sqms"] > 0)
		{
			$avg = round($list_avgs["cms_total"]/$list_avgs["project_cost_sqms"], 2);
			$sheet->write($row_index, $col_index01+1, $avg, $f_number_bold);
		}
		else
		{
			$sheet->write($row_index, $col_index01+1, "0.00", $f_number_bold);
		}
		if($list_avgs["ftes"] > 0)
		{
			$avg = round($list_avgs["cms_total"]/$list_avgs["ftes"], 2);
			//$sheet->write($row_index, $col_index01+2, $avg, $f_number_bold);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+2, "0.00", $f_number_bold);
		}
		if($list_avgs["total_cost2"] > 0)
		{
			$avg = round($list_avgs["cms_total"]/$list_avgs["total_cost2"], 2);
			//$sheet->write($row_index, $col_index01+3, $avg , $f_percent_bold);
			$sheet->write($row_index, $col_index01+3, "" , $f_percent_bold);
		}
		else
		{
			//$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
		}
		$row_index++;
	}

}



//print project to which average will be compared
if($num_of_projects > 0 and $compare_with_project != '' and $compare_with_project != NULL)
{
	$row_index = 3;
	$col_index01 = $col_index01 + 5;


	//check if project is in pipeline
	$pipeline = 0;

	$sql = "select project_id " . 
		   "from posaddressespipeline " . 
		   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
		   "left join projects on project_order = posorder_order " .
		   "left join orders on order_id = project_order " . 
		   "left join countries on country_id = posaddress_country " .
		   "left join product_lines on product_line_id = posorder_product_line " .
		   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
		   "left join projectkinds on projectkind_id = posorder_project_kind " .
		   "left join postypes on postype_id = posorder_postype " .
		   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
		   "left join project_costs on project_cost_order = posorder_order " . 
		   "where posorder_type = 1 and project_number = " . dbquote($compare_with_project);
	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$pipeline = 1;
	}
	else {
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and project_number = " . dbquote($compare_with_project);
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$pipeline = 2;
		}
	}

	if($pipeline == 1)
	{
		$sql = "select * " . 
			   "from posaddressespipeline " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " . 
			   "where posorder_type = 1 and project_number = " . dbquote($compare_with_project);

	}
	elseif($pipeline == 2) // renovation project
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorderspipeline on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and project_number = " . dbquote($compare_with_project);

	}
	else
	{
		$sql = "select * " . 
			   "from posaddresses " . 
			   "left join posorders on posorder_posaddress = posaddress_id " .
			   "left join projects on project_order = posorder_order " .
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = posaddress_country " .
			   "left join product_lines on product_line_id = posorder_product_line " .
			   "left join productline_subclasses on productline_subclass_id = posorder_product_line_subclass " .
			   "left join projectkinds on projectkind_id = posorder_project_kind " .
			   "left join postypes on postype_id = posorder_postype " .
			   "left join project_costtypes on project_costtype_id = posorder_legal_type " .
			   "left join project_costs on project_cost_order = posorder_order " .
			   "where posorder_type = 1 and project_number = " . dbquote($compare_with_project);

		
	}


	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		
		//$result = update_posdata_from_posorders($row["posaddress_id"]);
		//$result = build_missing_investment_records($row["posorder_id"]);
		$result = update_investment_records_from_system_currency($row["posorder_id"]);
		$result = update_investment_records_from_local_currency($row["posorder_id"]);
		$num_of_projects++;

		//get benchmark first full years
		$benchmark_year = 0;
		$exchange_rate = 1;
		$factor = 1;

		$cer_basicdata = get_cer_basicdata($row["project_id"]);

		if(count($cer_basicdata) > 0)
		{
			$first_year = $cer_basicdata["cer_basicdata_firstyear"];
			$first_month = $cer_basicdata["cer_basicdata_firstyear"];

			if($first_month == 1)
			{
				$benchmark_year = $cer_basicdata["cer_basicdata_firstyear"];
			}
			else
			{
				$benchmark_year = $cer_basicdata["cer_basicdata_firstyear"] + 1;
			}
			$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
			$factor = $cer_basicdata["cer_basicdata_factor"];

			if(!$factor) {$factor = 1;}
		}
		
		$row_index = 3;

		$sheet->setColumn($col_index01, $col_index01, 9);
		$sheet->setColumn($col_index01+1, $col_index01+1, 9);
		$sheet->setColumn($col_index01+2, $col_index01+2, 9);
		$sheet->setColumn($col_index01+3, $col_index01+3, 12);
		$sheet->setColumn($col_index01+4, $col_index01+4, 2);
		
		
		
		if(1==2 and $investments_only == 1)
		{
			$sheet->write($row_index, $col_index01, $row["posaddress_name"], $f_white_on_green_border_left);
			$sheet->write($row_index, $col_index01+1,"", $f_white_on_green_border_middle);
			$sheet->write($row_index, $col_index01+2,"", $f_white_on_green_border_middle);
			$sheet->write($row_index, $col_index01+3,"", $f_white_on_green_border_right);
		}
		else
		{
			$sheet->write($row_index, $col_index01, $row["posaddress_name"], $f_border_left);
			$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
			$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
			$sheet->write($row_index, $col_index01+3,"", $f_border_right);
		}
		



		$row_index++;
		$sheet->write($row_index, $col_index01, $row["country_name"] . ", " . $row["posaddress_place"], $f_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_border_right);
		$row_index++;

		$product_line_name = $row["product_line_name"];
		if($row["productline_subclass_name"]) {
			$product_line_name .= " / " . $row["productline_subclass_name"];
		}
		
		$sheet->write($row_index, $col_index01, $row["postype_name"] . ": " . $product_line_name, $f_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_border_right);
		$row_index++;
		
		$tmp = $row["project_costtype_text"] . " - " . $row["order_number"] . ' / Kind: ' . $row["projectkind_code"] . ' / Satus: ' . $row["order_actual_order_state_code"];
		$sheet->write($row_index, $col_index01, $tmp, $f_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_border_right);
		$row_index++;

		$sheet->write($row_index, $col_index01, "Benchmark Full Year : " . $benchmark_year, $f_border_left);
		$sheet->write($row_index, $col_index01+1,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+2,"", $f_border_middle);
		$sheet->write($row_index, $col_index01+3,"", $f_border_right);
		$row_index++;
		$row_index++;

		if($investments_only == 0)
		{		
			$sheet->write($row_index, $col_index01, "Amount", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+1,"per sqm", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+2,"per fte", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+3,"% of net sales", $f_black_on_grey);
			$row_index = 12;
			
		}
		else
		{
			$row_index = 10;
		}



		//curency
		$currency_symbol = "";
		$sales_reduction_percent = 0;
		$sql_s = "select currency_symbol, cer_basicdata_sales_reduction " . 
			     "from cer_basicdata " . 
			     "left join currencies on currency_id = cer_basicdata_currency " . 
			     "where cer_basicdata_project = " . dbquote($row["project_id"]) . 
			     " and cer_basicdata_version = 0 ";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$currency_symbol = $row_s["currency_symbol"];
			$sales_reduction_percent = $row_s["cer_basicdata_sales_reduction"] / 100;
		}

		if($base_currency == "chf")
		{
			$currency_symbol = "CHF";
		}
		else
		{
			$exchange_rate = 1;
			$factor = 1;
		}

		$sheet->write($row_index, $col_index01, $currency_symbol, $f_number);
		$row_index++;

		//sales surface

		
		$retail_area = $row["project_cost_sqms"];
		$sheet->write($row_index, $col_index01, $retail_area, $f_number);
		
		if($investments_only == 0)
		{
			$row_index++;
		}
		else
		{
			$row_index++;
			$row_index++;
		}


		if(array_key_exists("project_cost_sqms", $list_avgs))
		{
			$list_avgs["project_cost_sqms"] = $list_avgs["project_cost_sqms"] + $retail_area;
		}
		else
		{
			$list_avgs["project_cost_sqms"] = $retail_area;
		}

		//ful time equivalent
		// get total ftes
		
		if($investments_only == 0)
		{
			$ftes = 0;
			$sql_s = "select sum(cer_salary_headcount_percent) as fte " . 
					 "from cer_salaries " .
					 "left join projects on project_id = cer_salary_project " . 
					 "left join orders on order_id = project_order " . 
					 "where order_id = '" . $row["posorder_order"] . "'" . 
					 " and cer_salary_cer_version = 0 " .
					 " and cer_salary_year_starting <= " . $benchmark_year;
			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			while($row_s = mysql_fetch_assoc($res_s))
			{
				$ftes = $ftes + $row_s["fte"];
			}
			
			$ftes = round($ftes/100,2);
			$sheet->write($row_index, $col_index01, $ftes, $f_number);
			

			$ftes_per_sqm = 0;
			if($ftes > 0)
			{
				$ftes_per_sqm = round($retail_area/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+1, $ftes_per_sqm, $f_number);
			$row_index++;

			//lease terms
			//get the lease duration in years
			$starting_date = 0;
			$ending_date = 0;
			$extension_option = 0;
			$exit_option = 0;

			if($posorder["pipeline"] == 1)
			{
				$sql_s = "select poslease_startdate, poslease_enddate, poslease_extensionoption, poslease_exitoption " . 
						 "from posleases " . 
						 "where poslease_posaddress = " . $row["posaddress_id"];
			}
			else
			{
				$sql_s = "select poslease_startdate, poslease_enddate, poslease_extensionoption, poslease_exitoption " . 
						 "from posleasespipeline " . 
						 "where poslease_posaddress = " . $row["posaddress_id"];
			}
			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			while($row_s = mysql_fetch_assoc($res_s))
			{
				if($starting_date == 0)
				{
					$starting_date = $row_s["poslease_startdate"];
				}
				elseif($row_s["poslease_startdate"] < $starting_date)
				{
					$starting_date = $row_s["poslease_startdate"];
				}

				if($ending_date == 0)
				{
					$ending_date = $row_s["poslease_enddate"];
				}
				elseif($row_s["poslease_enddate"] > $ending_date)
				{
					$ending_date = $row_s["poslease_enddate"];
				}

				if($extension_option == 0)
				{
					$extension_option = $row_s["poslease_extensionoption"];
				}
				elseif($row_s["poslease_extensionoption"] > $extension_option)
				{
					$extension_option = $row_s["poslease_extensionoption"];
				}

				if($exit_option == 0)
				{
					$exit_option = $row_s["poslease_exitoption"];
				}
				elseif($row_s["poslease_exitoption"] > $exit_option)
				{
					$exit_option = $row_s["poslease_exitoption"];
				}
			}
			if($starting_date == 0 and $ending_date == 0)
			{
				$lease_period = 0;
			}
			else
			{
				$start = strtotime($starting_date) ;
				$end = strtotime($ending_date) ;

				$lease_period = $end - $start;
				$lease_period = $lease_period / 86400;
				$lease_period = round($lease_period/365, 2);

			}

			$sheet->write($row_index, $col_index01, $lease_period, $f_number);
			$row_index++;
			
			//get revenues
			$watches = 0;
			$jewellery = 0;
			$watches_revenue = 0;
			$jewellery_revenue = 0;
			$services_revenue = 0;

			$sql_s = "select sum(cer_revenue_quantity_watches) as watches, " .
					 "sum(cer_revenue_quantity_jewellery) as jewellery,  " .
					 "sum(cer_revenue_watches) as watches_revenue,  " .
					 "sum(cer_revenue_jewellery) as jewellery_revenue,  " .
					 "sum(cer_revenue_customer_service) as services_revenue  " .
					 "from cer_revenues " .
					 "left join projects on project_id = cer_revenue_project " . 
					 "left join orders on order_id = project_order " . 
					 "where cer_revenue_project = " . dbquote($row["project_id"]) . 
					 " and cer_revenue_cer_version = 0 " .
					 " and cer_revenue_year = " . $benchmark_year;

			$res_s = mysql_query($sql_s) or dberror($sql_s);
			while($row_s = mysql_fetch_assoc($res_s))
			{
				$watches = $watches + $row_s["watches"];
				$jewellery = $jewellery + $row_s["jewellery"];

				$watches_revenue = $watches_revenue + $row_s["watches_revenue"];
				$jewellery_revenue = $jewellery_revenue + $row_s["jewellery_revenue"];
				$services_revenue = $services_revenue + $row_s["services_revenue"];
			}

			$watches_revenue = $watches_revenue * $exchange_rate / $factor;
			$jewellery_revenue = $jewellery_revenue * $exchange_rate / $factor;
			$services_revenue = $services_revenue * $exchange_rate / $factor;
			$total_revenue = $watches_revenue + $jewellery_revenue + $services_revenue;
			$percent_base = 100 + 100*$sales_reduction_percent;
			$base_unit = $total_revenue / $percent_base;

			//calculate average prices
			$watches_price = 0;
			$jewellery_price = 0;

			if($watches > 0)
			{
				$watches_price = round($watches_revenue/$watches, 2);
			}

			if($jewellery > 0)
			{
				$jewellery_price = round($jewellery_revenue/$jewellery, 2);
			}

			
			$sheet->write($row_index, $col_index01, $watches, $f_number);
			
			$watches_per_sqm = 0;
			if($retail_area > 0)
			{
				$watches_per_sqm = round($watches/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $watches_per_sqm, $f_number);
			$watches_per_fte = 0;
			if($ftes > 0)
			{
				$watches_per_fte = round($watches/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $watches_per_fte, $f_number);
			$row_index++;

			$sheet->write($row_index, $col_index01, $jewellery, $f_number);
			
			$jewellery_per_sqm = 0;
			if($retail_area > 0)
			{
				$jewellery_per_sqm = round($jewellery/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $jewellery_per_sqm, $f_number);
			$jewellery_per_fte = 0;
			if($ftes > 0)
			{
				$jewellery_per_fte = round($jewellery/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $jewellery_per_fte, $f_number);
			$row_index++;



			


			$sheet->write($row_index, $col_index01, $watches_price, $f_number);
			$row_index++;
			

			$sheet->write($row_index, $col_index01, $jewellery_price, $f_number);
			$row_index++;
			


						
			
			//revenues
			$gross_sales = $watches_revenue + $jewellery_revenue + $services_revenue;
			$sales_reduction = $gross_sales * $sales_reduction_percent;
			$net_sales = $gross_sales - $sales_reduction;


			$sheet->write($row_index, $col_index01, $watches_revenue, $f_number);
			

			$watches_revenue_per_sqm = 0;
			if($retail_area > 0)
			{
				$watches_revenue_per_sqm = round($watches_revenue/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $watches_revenue_per_sqm, $f_number);

			$watches_revenue_per_fte = 0;
			if($ftes > 0)
			{
				$watches_revenue_per_fte = round($watches_revenue/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $watches_revenue_per_fte, $f_number);

			$percent_of_watches = "0.00";
			$percent_of_net_sales = 0;
			if($net_sales > 0)
			{
				$percent_of_watches = $watches_revenue/$net_sales;
				$percent_of_net_sales = $watches_revenue/$net_sales;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_watches, $f_percent);

			$row_index++;


			$sheet->write($row_index, $col_index01, $jewellery_revenue, $f_number);
			

			$jewellery_revenue_per_sqm = 0;
			if($retail_area > 0)
			{
				$jewellery_revenue_per_sqm = round($jewellery_revenue/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $jewellery_revenue_per_sqm, $f_number);

			$jewellery_revenue_per_fte = 0;
			if($ftes > 0)
			{
				$jewellery_revenue_per_fte = round($jewellery_revenue/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $jewellery_revenue_per_fte, $f_number);

			$percent_of_jewellery = "0.00";
			if($net_sales > 0)
			{
				$percent_of_jewellery = $jewellery_revenue/$net_sales;
				$percent_of_net_sales = $percent_of_net_sales + ($jewellery_revenue/$net_sales);
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_jewellery, $f_percent);

			$row_index++;


			
			
			$sheet->write($row_index, $col_index01, $services_revenue, $f_number);
			

			$services_revenue_per_sqm = 0;
			if($retail_area > 0)
			{
				$services_revenue_per_sqm = round($services_revenue/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $services_revenue_per_sqm, $f_number);

			$services_revenue_per_fte = 0;
			if($ftes > 0)
			{
				$services_revenue_per_fte = round($services_revenue/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $services_revenue_per_fte, $f_number);

			$percent_of_service = "0.00";
			if($net_sales > 0)
			{
				$percent_of_service = $services_revenue/$net_sales;
				$percent_of_net_sales = $percent_of_net_sales + ($services_revenue/$net_sales);
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_service, $f_percent);

			$row_index++;
			
			
			
			//gross sales
			
			$sheet->write($row_index, $col_index01, $gross_sales, $f_number_bold);
			
			$gross_sales_per_sqm = 0;
			if($retail_area > 0)
			{
				$gross_sales_per_sqm = round($gross_sales/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $gross_sales_per_sqm, $f_number_bold);
			$gross_sales_per_fte = 0;
			if($ftes > 0)
			{
				$gross_sales_per_fte = round($gross_sales/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $gross_sales_per_fte, $f_number_bold);

			$percent_of_net_sales = $percent_of_net_sales;
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent_bold);


			$row_index++;



			$sheet->write($row_index, $col_index01, $sales_reduction, $f_number);
			

			$sales_reduction_per_sqm = 0;
			if($retail_area > 0)
			{
				$sales_reduction_per_sqm = round($sales_reduction/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $sales_reduction_per_sqm, $f_number);
			$sales_reduction_per_fte = 0;
			if($ftes > 0)
			{
				$sales_reduction_per_fte = round($sales_reduction/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $sales_reduction_per_fte, $f_number);

			
			if($net_sales > 0)
			{
				$sales_reduction_percent = $sales_reduction / $net_sales;
			}
			else
			{
				$sales_reduction_percent = 0;
			}

			$sales_reduction_percent = -1*$sales_reduction_percent;
			$sheet->write($row_index, $col_index01+3, $sales_reduction_percent, $f_percent_bold);

			$row_index++;

			
			//net sales
			$net_sales = $gross_sales - $sales_reduction;

			$sheet->write($row_index, $col_index01, $net_sales, $f_number_bold);
			
			$net_sales_per_sqm = 0;
			if($retail_area > 0)
			{
				$net_sales_per_sqm = round($net_sales/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $net_sales_per_sqm, $f_number_bold);
			$net_sales_per_fte = 0;
			if($ftes > 0)
			{
				$net_sales_per_fte = round($net_sales/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $net_sales_per_fte, $f_number_bold);
			$sheet->write($row_index, $col_index01+3, "1.00", $f_percent_bold);

			$row_index++;
			

			//get expenses
			$total_cost = 0;
			$expenses = array();
			$sql_s = "select cer_expense_type, sum(cer_expense_amount) as total " . 
					 "from cer_expenses " . 
					 "where cer_expense_project = " . dbquote($row["project_id"]) . 
					 " and cer_expense_cer_version = 0 " . 
					 " and cer_expense_year = " . $benchmark_year . 
					 " group by cer_expense_type";
			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			while($row_s = mysql_fetch_assoc($res_s))
			{
				$expenses[$row_s["cer_expense_type"]] = $row_s["total"];
			}

			foreach($expenses as $key=>$value)
			{
				if($key == 6 or $key == 8 or $key == 9 or $key == 10 or $key == 11 or  $key == 12 )
				{
					$expenses[10] = $expenses[10] + $value;
				}
			}


			//Gross sales (net sales minus Material of products sold)
			$amount = 0;
			if(array_key_exists(15, $expenses))
			{
				$amount =  $expenses[15] * $exchange_rate / $factor;
				$sheet->write($row_index, $col_index01, $amount, $f_number);
				
				$expenses_per_sqm = 0;
				if($retail_area > 0)
				{
					$expenses_per_sqm = round($amount/$retail_area,2);
				}
				$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number);
				$expenses_per_fte = 0;
				if($ftes > 0)
				{
					$expenses_per_fte = round($amount/$ftes,2);
				}
				$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number);
				$percent_of_net_sales = "0.00";
				if($net_sales > 0)
				{
					//$percent_of_net_sales =  round($amount/$net_sales,2);
					$percent_of_net_sales = -1*$amount/$net_sales;
				}
				$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
			}
			else
			{
				$sheet->write($row_index, $col_index01, "0", $f_number);
				$sheet->write($row_index, $col_index01+1, "0", $f_number);
				$sheet->write($row_index, $col_index01+2, "0", $f_number);
				$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
			}


			$row_index++;

			//gross margin

			$gross_margin = $net_sales - $amount;
			$sheet->write($row_index, $col_index01, $gross_margin, $f_number_bold);
			
			$gross_margin_per_sqm = 0;
			if($retail_area > 0)
			{
				$gross_margin_per_sqm = round($gross_margin/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $gross_margin_per_sqm, $f_number_bold);
			$gross_margin_per_fte = 0;
			if($ftes > 0)
			{
				$gross_margin_per_fte = round($gross_margin/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $gross_margin_per_fte, $f_number_bold);

			$percent_of_net_sales = "0.00";
			if($net_sales > 0)
			{
				$percent_of_net_sales = $gross_margin/$net_sales;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent_bold);

			$row_index++;


			//marketing expenses
			$amount = 0;
			if(array_key_exists(14, $expenses))
			{
				$amount = ($expenses[14] * $exchange_rate / $factor);
			}
			if(array_key_exists(13, $expenses))
			{
				if($amount > 0) 
				{
					$amount =  $amount - ($expenses[13] * $exchange_rate / $factor);
				}
				else
				{
					$amount =  $expenses[13] * $exchange_rate / $factor;
				}

				$sheet->write($row_index, $col_index01, $amount, $f_number_bold);
				
				$expenses_per_sqm = 0;
				if($retail_area > 0)
				{
					$expenses_per_sqm = round($amount/$retail_area,2);
				}
				$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number_bold);
				$expenses_per_fte = 0;
				if($ftes > 0)
				{
					$expenses_per_fte = round($amount/$ftes,2);
				}
				$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number_bold);
				$percent_of_net_sales = "0.00";
				if($net_sales > 0)
				{
					//$percent_of_net_sales = round($amount/$net_sales,2);
					$percent_of_net_sales = -1*$amount/$net_sales;
				}
				$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent_bold);
			}
			else
			{
				$sheet->write($row_index, $col_index01, "0", $f_number);
				$sheet->write($row_index, $col_index01+1, "0", $f_number);
				$sheet->write($row_index, $col_index01+2, "0", $f_number);
				$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
			}
			$marketing_expenses = $amount;

			$row_index++;
			
			

			//indirect salaries
			if(array_key_exists(1, $expenses))
			{
				$amount =  $expenses[1] * $exchange_rate / $factor;
				$sheet->write($row_index, $col_index01, $amount, $f_number);
				$expenses_per_sqm = 0;
				if($retail_area > 0)
				{
					$expenses_per_sqm = round($amount/$retail_area,2);
				}
				$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number);
				$expenses_per_fte = 0;
				if($ftes > 0)
				{
					$expenses_per_fte = round($amount/$ftes,2);
				}
				$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number);
				$percent_of_net_sales = "0.00";
				if($net_sales > 0)
				{
					$percent_of_net_sales = -1*$amount/$net_sales;
				}
				$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
				$total_cost = $total_cost +$amount;
			}
			else
			{
				$sheet->write($row_index, $col_index01, "0", $f_number);
				$sheet->write($row_index, $col_index01+1, "0", $f_number);
				$sheet->write($row_index, $col_index01+2, "0", $f_number);
				$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
			}

			$row_index++;

			
			//rents
			$rents = 0;
			if(array_key_exists(2, $expenses))
			{
				$rents = $expenses[2];
			}
			if(array_key_exists(3, $expenses))
			{
				$rents = $rents + $expenses[3];
			}
			if(array_key_exists(16, $expenses))
			{
				$rents = $rents + $expenses[16];
			}
			$rents =  $rents * $exchange_rate / $factor;

			$sheet->write($row_index, $col_index01, $rents, $f_number);
			
			$rents_per_sqm = 0;
			if($retail_area > 0)
			{
				$rents_per_sqm = round($rents/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $rents_per_sqm, $f_number);
			$rents_per_fte = 0;
			if($ftes > 0)
			{
				$rents_per_fte = round($rents/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $rents_per_fte, $f_number);
			$percent_of_net_sales = "0.00";
			if($net_sales > 0)
			{
				$percent_of_net_sales = -1*$rents/$net_sales;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
			$row_index++;
			$total_cost = $total_cost + $rents;

			//Aux Mat.
			if(array_key_exists(4, $expenses))
			{
				$amount =  $expenses[4] * $exchange_rate / $factor;
				$sheet->write($row_index, $col_index01, $amount, $f_number);
				

				$expenses_per_sqm = 0;
				if($retail_area > 0)
				{
					$expenses_per_sqm = round($amount/$retail_area,2);
				}
				$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number);
				$expenses_per_fte = 0;
				if($ftes > 0)
				{
					$expenses_per_fte = round($amount/$ftes,2);
				}
				$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number);
				$percent_of_net_sales = "0.00";
				if($net_sales > 0)
				{
					$percent_of_net_sales = $amount/$net_sales;
				}
				$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
				$total_cost = $total_cost + $amount;
			}
			else
			{
				$sheet->write($row_index, $col_index01, "0", $f_number);
				$sheet->write($row_index, $col_index01+1, "0", $f_number);
				$sheet->write($row_index, $col_index01+2, "0", $f_number);
				$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
			}
			$row_index++;

			//Sales Admin
			if(array_key_exists(5, $expenses))
			{
				$amount =  $expenses[5] * $exchange_rate / $factor;
				$sheet->write($row_index, $col_index01, $amount, $f_number);
				
				$expenses_per_sqm = 0;
				if($retail_area > 0)
				{
					$expenses_per_sqm = round($amount/$retail_area,2);
				}
				$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number);
				$expenses_per_fte = 0;
				if($ftes > 0)
				{
					$expenses_per_fte = round($amount/$ftes,2);
				}
				$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number);
				$percent_of_net_sales = "0.00";
				if($net_sales > 0)
				{
					$percent_of_net_sales = -1*$amount/$net_sales;
				}
				$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
				$total_cost = $total_cost + $amount;
			}
			else
			{
				$sheet->write($row_index, $col_index01, "0", $f_number);
				$sheet->write($row_index, $col_index01+1, "0", $f_number);
				$sheet->write($row_index, $col_index01+2, "0", $f_number);
				$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
			}
			$row_index++;

			
			//Depreciation on Investment

			$d = calculate_depreciation($cer_basicdata, $row["project_id"], 0);
			$depreciation = $d["investments"];
				
			if(array_key_exists($benchmark_year, $depreciation))
			{
				$depreciation_benchmark_year = $depreciation[$benchmark_year];
			}
			else
			{
				$depreciation_benchmark_year = 0;
			}

			$depreciation_benchmark_year = $depreciation_benchmark_year * $exchange_rate / $factor;

			$sheet->write($row_index, $col_index01, round($depreciation_benchmark_year, 2), $f_number);
			
			$depreciation_benchmark_year_per_sqm = 0;
			if($retail_area > 0)
			{
				$depreciation_benchmark_year_per_sqm = round($depreciation_benchmark_year/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $depreciation_benchmark_year_per_sqm, $f_number);
			$depreciation_benchmark_year_per_fte = 0;
			if($ftes > 0)
			{
				$depreciation_benchmark_year_per_fte = round($depreciation_benchmark_year/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $depreciation_benchmark_year_per_fte, $f_number);
			$percent_of_net_sales = "0.00";
			if($net_sales > 0)
			{
				$percent_of_net_sales = -1*$depreciation_benchmark_year/$net_sales;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
			$row_index++;

			$total_cost = $total_cost + $depreciation_benchmark_year;

			//Depreciation on Key Money
			$depreciation_benchmark_year = 0;
			$depreciation_benchmark_year = 0;
			$prepayed_rents = $d["intangibles"];
			if(array_key_exists($benchmark_year, $depreciation))
			{
				$depreciation_benchmark_year = $prepayed_rents[$benchmark_year];
			}
			else
			{
				$depreciation_benchmark_year = 0;
			}
			
			
			
			$depreciation_benchmark_year = $depreciation_benchmark_year * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, round($depreciation_benchmark_year, 2), $f_number);
			
			$depreciation_benchmark_year_per_sqm = 0;
			if($retail_area > 0)
			{
				$depreciation_benchmark_year_per_sqm = round($depreciation_benchmark_year/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $depreciation_benchmark_year_per_sqm, $f_number);
			$depreciation_benchmark_year_per_fte = 0;
			if($ftes > 0)
			{
				$depreciation_benchmark_year_per_fte = round($depreciation_benchmark_year/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $depreciation_benchmark_year_per_fte, $f_number);
			$percent_of_net_sales = "0.00";
			if($net_sales > 0)
			{
				$percent_of_net_sales = -1*$depreciation_benchmark_year/$net_sales;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
			$row_index++;

			$total_cost = $total_cost + $depreciation_benchmark_year;

			//Other indirect expenses
			if(array_key_exists(10, $expenses))
			{
				$amount = $expenses[10] * $exchange_rate / $factor;
				$sheet->write($row_index, $col_index01, $amount, $f_number);
				
				$expenses_per_sqm = 0;
				if($retail_area > 0)
				{
					$expenses_per_sqm = round($amount/$retail_area,2);
				}
				$sheet->write($row_index, $col_index01+1, $expenses_per_sqm, $f_number);
				$expenses_per_fte = 0;
				if($ftes > 0)
				{
					$expenses_per_fte = round($amount/$ftes,2);
				}
				$sheet->write($row_index, $col_index01+2, $expenses_per_fte, $f_number);
				$percent_of_net_sales = "0.00";
				if($net_sales > 0)
				{
					$percent_of_net_sales = -1*$amount/$net_sales;
				}
				$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent);
				$total_cost = $total_cost + $amount;
			}
			else
			{
				$sheet->write($row_index, $col_index01, "0", $f_number);
				$sheet->write($row_index, $col_index01+1, "0", $f_number);
				$sheet->write($row_index, $col_index01+2, "0", $f_number);
				$sheet->write($row_index, $col_index01+3, "0.00", $f_number);
				
			}
			$row_index++;

			
			// total cost
			
			$sheet->write($row_index, $col_index01, $total_cost, $f_number_bold);
			
			$total_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$total_cost_per_sqm = round($total_cost/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $total_cost_per_sqm, $f_number_bold);
			$total_cost_per_fte = 0;
			if($ftes > 0)
			{
				$total_cost_per_fte = round($total_cost/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $total_cost_per_fte, $f_number_bold);
			$percent_of_net_sales = "0.00";
			if($net_sales > 0)
			{
				$percent_of_net_sales = -1*$total_cost/$net_sales;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent_bold);
			$row_index++;


			//operational income
			$operational_income = $gross_margin + $marketing_expenses - $total_cost;
			$sheet->write($row_index, $col_index01, $operational_income, $f_number_bold);
			
			$operational_income_per_sqm = 0;
			if($retail_area > 0)
			{
				$operational_income_per_sqm = round($operational_income/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $operational_income_per_sqm, $f_number_bold);
			$operational_income_per_fte = 0;
			if($ftes > 0)
			{
				$operational_income_per_fte = round($operational_income/$ftes,2);
			}
			$sheet->write($row_index, $col_index01+2, $operational_income_per_fte, $f_number_bold);

			$percent_of_net_sales = "0.00";
			if($net_sales > 0)
			{
				$percent_of_net_sales = $operational_income/$net_sales;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_net_sales, $f_percent_bold);

			$row_index++;
			$row_index++;
		}

		
		
		
		
		
		
		
		
		
		/********************************************************************
		investments
		*********************************************************************/
		$sheet->write($row_index, $col_index01, "Amount", $f_black_on_grey);
		$sheet->write($row_index, $col_index01+1,"per sqm", $f_black_on_grey);
		$sheet->write($row_index, $col_index01+2,"per fte", $f_black_on_grey);
		$sheet->write($row_index, $col_index01+3,"% of invest.", $f_black_on_grey);
		
		$row_index++;

		//get total investment in fixed assets
		$total_investment = 0;
		$total_investment_in_fixed_assets = 0;

		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " . 
			     " and (cer_investment_type = 1 " . 
			     " or cer_investment_type = 3 " .
			     " or cer_investment_type = 5 " .
			     " or cer_investment_type = 7 " .
			     " or cer_investment_type = 11 " .
				 " or cer_investment_type = 9 " . 
			     " or cer_investment_type = 13 " .
			     " or cer_investment_type = 15 " .
			     " or cer_investment_type = 17) ";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$total_investment = $row_s["total"] * $exchange_rate / $factor;
		}

		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " .
			     " and (cer_investment_type = 1 " . 
			     " or cer_investment_type = 3 " .
			     " or cer_investment_type = 5 " .
			     " or cer_investment_type = 7 " .
			     " or cer_investment_type = 11) ";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$total_investment_in_fixed_assets = $row_s["total"] * $exchange_rate / $factor;
		}


		//get construction cost
		$construction = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " .
			     " and cer_investment_type = 1";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$construction = $row_s["total"];
		}
		$construction = $construction * $exchange_rate / $factor;

		$sheet->write($row_index, $col_index01, $construction, $f_number);
		
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($construction/$retail_area,2);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($construction/$ftes,2);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00";
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = $construction/$total_investment;
			
		}
		*/
		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = $construction/$total_investment_in_fixed_assets;
			
		}

		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
		$row_index++;



		//get fixturing cost
		$fixturing = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " .
			     " and cer_investment_type = 3";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$fixturing = $row_s["total"];
		}
		$fixturing = $fixturing * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, $fixturing, $f_number);
		
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($fixturing/$retail_area,2);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($fixturing/$ftes,2);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00";
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = $fixturing/$total_investment;
		}
		*/

		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = $fixturing/$total_investment_in_fixed_assets;
		}
		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
		$row_index++;



		//get architectural cost
		$architectural = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " .
			     " and cer_investment_type = 5";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$architectural = $row_s["total"];
		}
		$architectural = $architectural * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, $architectural, $f_number);
		
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($architectural/$retail_area,2);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($architectural/$ftes,2);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00";
		
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = $architectural/$total_investment;
		}
		*/

		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = $architectural/$total_investment_in_fixed_assets;
		}

		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
		$row_index++;

		
		//get equipment cost
		$equipment = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " .
			     " and cer_investment_type = 7";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$equipment = $row_s["total"];
		}
		$equipment = $equipment * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, $equipment, $f_number);
		
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($equipment/$retail_area,2);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($equipment/$ftes,2);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00";
		
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = $equipment/$total_investment;
		}
		*/

		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = $equipment/$total_investment_in_fixed_assets;
		}

		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
		$row_index++;

		
		//get other cost
		$other = 0;
		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " .
			     " and cer_investment_type = 11";

		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$other = $row_s["total"];
		}
		$other = $other * $exchange_rate / $factor;
		$sheet->write($row_index, $col_index01, $other, $f_number);
		
		$investment_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$investment_cost_per_sqm = round($other/$retail_area,2);
		}
		$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
		$investment_cost_per_fte = 0;
		if($ftes > 0)
		{
			$investment_cost_per_fte = round($other/$ftes,2);
		}
		//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
		$percent_of_total_cost = "0.00";
		
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = $other/$total_investment;
		}
		*/

		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = $other/$total_investment_in_fixed_assets;
		}

		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
		$row_index++;



		//totel investment in fixed assets

		$total_cost = 0;

		$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
				 "from cer_investments " . 
				 "where cer_investment_project = " . dbquote($row["project_id"]) . 
			     " and cer_investment_cer_version = 0 " .
			     " and (cer_investment_type = 1 " . 
			     " or cer_investment_type = 3 " .
			     " or cer_investment_type = 5 " .
			     " or cer_investment_type = 7 " .
			     " or cer_investment_type = 11) ";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$total_cost = $row_s["total"] * $exchange_rate / $factor;
		}

		$sheet->write($row_index, $col_index01, $total_cost, $f_number_bold);
		
		$total_cost_cost_per_sqm = 0;
		if($retail_area > 0)
		{
			$total_cost_per_sqm = round($total_cost/$retail_area,2);
		}
		
		
		//$sheet->write($row_index, $col_index01+1, $total_cost_per_sqm, $f_white_on_green);
		$sheet->write($row_index, $col_index01+1, $total_cost_per_sqm, $f_number_bold);
		$total_cost_cost_per_fte = 0;
		if($ftes > 0)
		{
			$total_cost_per_fte = round($total_cost/$ftes,2);
		}
		//$sheet->write($row_index, $col_index01+2, $total_cost_per_fte, $f_number_bold);
		$percent_of_total_cost = "0.00";
		/*
		if($total_investment > 0)
		{
			$percent_of_total_cost = $total_cost/$total_investment;
		}
		*/

		if($total_investment_in_fixed_assets > 0)
		{
			$percent_of_total_cost = $total_cost/$total_investment_in_fixed_assets;
		}


		$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent_bold);


		
		$row_index++;


		//get intangibles
		if($investments_only == 0)
		{
			$intangibles = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and (cer_investment_type = 15 or cer_investment_type = 17)";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$intangibles = $row_s["total"];
			}
			$intangibles = $intangibles * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $intangibles, $f_number);
			
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($intangibles/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($intangibles/$ftes,2);
			}
			$percent_of_total_cost = "0.00";
			if($total_investment > 0)
			{
				$percent_of_total_cost = $intangibles/$total_investment;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
			$sheet->write($row_index, $col_index01+3, "", $f_number);

			
			
			$row_index++;

		
			//get deposit cost
			$deposit = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 9";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				if(count($cer_basicdata) > 0)
				{
					$deposit = $row_s["total"] + $cer_basicdata["cer_basicdata_recoverable_keymoney"];
				}
				else
				{
					$deposit = $row_s["total"];
				}

			}
			$deposit = $deposit * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $deposit, $f_number);
			

			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($deposit/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($deposit/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost = "0.00";
			if($total_investment > 0)
			{
				$percent_of_total_cost = $deposit/$total_investment;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
			$row_index++;

			//get noncapitalized cost
			$noncapitalized = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 13";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$noncapitalized = $row_s["total"];
			}
			$noncapitalized = $noncapitalized * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $noncapitalized, $f_number);
			
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($noncapitalized/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($noncapitalized/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost = "0.00";
			if($total_investment > 0)
			{
				$percent_of_total_cost = $noncapitalized/$total_investment;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
			$row_index++;

			
			
			//Total Project costs (Requested amount)
			$sheet->write($row_index, $col_index01, $total_investment, $f_number_bold);
			
			$total_cost_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$total_cost_per_sqm = round($total_investment/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $total_cost_per_sqm, $f_number_bold);
			$total_cost_cost_per_fte = 0;
			if($ftes > 0)
			{
				$total_cost_per_fte = round($total_investment/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $total_cost_per_fte, $f_number_bold);
			$percent_of_total_cost = "0.00";
			if($total_cost > 0)
			{
				$percent_of_total_cost = $total_investment/$total_investment;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_number_bold);
			$sheet->write($row_index, $col_index01+3, "", $f_number_bold);
			
			$row_index++;
			$row_index++;
		}
		
		
		//KL Approved investments
		if($investments_only == 0)
		{
			$sheet->write($row_index, $col_index01, "Amount", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+1,"per sqm", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+2,"per fte", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+3,"% of invest.", $f_black_on_grey);
			
			$row_index++;
			//get total investment in fixed assets
			$total_investment_approved_approved = 0;
			
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and (cer_investment_type = 1 " . 
					 " or cer_investment_type = 3 " .
					 " or cer_investment_type = 5 " .
					 " or cer_investment_type = 7 " .
					 " or cer_investment_type = 11 " .
					 " or cer_investment_type = 9 " . 
					 " or cer_investment_type = 13 " .
					 " or cer_investment_type = 15 " .
					 " or cer_investment_type = 17) ";


			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$total_investment_approved = $row_s["total"] * $exchange_rate / $factor;
			}


			$total_investment_in_fixed_assets_approved = 0;
			
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and (cer_investment_type = 1 " . 
					 " or cer_investment_type = 3 " .
					 " or cer_investment_type = 5 " .
					 " or cer_investment_type = 7 " .
					 " or cer_investment_type = 11 " .
					 " or cer_investment_type = 9 " . 
					 " or cer_investment_type = 13 " .
					 " or cer_investment_type = 15 " .
					 " or cer_investment_type = 17) ";


			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$total_investment_approved = $row_s["total"] * $exchange_rate / $factor;
			}



			//totel investment in fixed assets

			$total_cost_approved = 0;

			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and (cer_investment_type = 1 " . 
					 " or cer_investment_type = 3 " .
					 " or cer_investment_type = 5 " .
					 " or cer_investment_type = 7 " .
					 " or cer_investment_type = 11) ";

			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$total_cost_approved = $row_s["total"] * $exchange_rate / $factor;
			}


			//get construction cost
			$construction = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 1";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$construction = $row_s["total"];
			}
			$construction = $construction * $exchange_rate / $factor;

			$sheet->write($row_index, $col_index01, $construction, $f_number);
			
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($construction/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($construction/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost = $construction/$total_cost_approved;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
			$row_index++;



			//get fixturing cost
			$fixturing = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 3";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$fixturing = $row_s["total"];
			}
			$fixturing = $fixturing * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $fixturing, $f_number);
			
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($fixturing/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($fixturing/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost = $fixturing/$total_cost_approved;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
			$row_index++;



			//get architectural cost
			$architectural = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 5";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$architectural = $row_s["total"];
			}
			$architectural = $architectural * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $architectural, $f_number);
			
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($architectural/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($architectural/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost = $architectural/$total_cost_approved;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
			$row_index++;

			
			//get equipment cost
			$equipment = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 7";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$equipment = $row_s["total"];
			}
			$equipment = $equipment * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $equipment, $f_number);
			
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($equipment/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($equipment/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost = $equipment/$total_cost_approved;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
			$row_index++;

			
			//get other cost
			$other = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 11";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$other = $row_s["total"];
			}
			$other = $other * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $other, $f_number);
			
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($other/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($other/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost = $other/$total_cost_approved;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_total_cost, $f_percent);
			$row_index++;


			//total fixturing
			$sheet->write($row_index, $col_index01, $total_cost_approved, $f_number_bold);
			
			
			$total_cost_approved_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$total_cost_approved_per_sqm = round($total_cost_approved/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $total_cost_approved_per_sqm, $f_number_bold);
			$total_cost_approved_cost_per_fte = 0;
			if($ftes > 0)
			{
				$total_cost_approved_per_fte = round($total_cost_approved/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $total_cost_approved_per_fte, $f_number_bold);
			$percent_of_total_cost_approved = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost_approved = $total_cost_approved/$total_cost_approved;
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_total_cost_approved, $f_percent_bold);


			
			$row_index++;


			//get intangibles

			$intangibles = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) .
					 " and cer_investment_cer_version = 0 " .
					 " and (cer_investment_type = 15 or cer_investment_type = 17)";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$intangibles = $row_s["total"];
			}
			$intangibles = $intangibles * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $intangibles, $f_number);
			
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($intangibles/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($intangibles/$ftes,2);
			}
			$percent_of_total_cost_approved = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost_approved = $intangibles/$total_investment_approved;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost_approved, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_number);

			
			
			$row_index++;

		
			//get deposit cost
			$deposit = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 9";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$deposit = $row_s["total"];
			}
			$deposit = $deposit * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $deposit, $f_number);
			
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($deposit/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($deposit/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost_approved = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost_approved = $deposit/$total_investment_approved;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost_approved, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
			$row_index++;

			//get noncapitalized cost
			$noncapitalized = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc_approved) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) .
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 13";

			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$noncapitalized = $row_s["total"];
			}
			$noncapitalized = $noncapitalized * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $noncapitalized, $f_number);
			
			$investment_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$investment_cost_per_sqm = round($noncapitalized/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $investment_cost_per_sqm, $f_number);
			$investment_cost_per_fte = 0;
			if($ftes > 0)
			{
				$investment_cost_per_fte = round($noncapitalized/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $investment_cost_per_fte, $f_number);
			$percent_of_total_cost_approved = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_total_cost_approved = $noncapitalized/$total_investment_approved;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost_approved, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_number);
			$row_index++;

			
			
			//Total Project costs (Requested amount)
			$sheet->write($row_index, $col_index01, $total_investment_approved, $f_number_bold);
			
			$total_cost_approved_cost_per_sqm = 0;
			if($retail_area > 0)
			{
				$total_cost_approved_per_sqm = round($total_investment_approved/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $total_cost_approved_per_sqm, $f_number_bold);
			$total_cost_approved_cost_per_fte = 0;
			if($ftes > 0)
			{
				$total_cost_approved_per_fte = round($total_investment_approved/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $total_cost_approved_per_fte, $f_number_bold);
			$percent_of_total_cost_approved = "0.00";
			if($total_cost_approved > 0)
			{
				$percent_of_total_cost_approved = $total_investment_approved/$total_investment_approved;
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_total_cost_approved, $f_percent_bold);
			$sheet->write($row_index, $col_index01+3, "", $f_number);

			$row_index++;
			$row_index++;
		}
	

	
		//CMS DATA Real COST
		if($investments_only == 0)
		{
			$grouptotals_real = array();
			$grouptotals_real[2] = 0;
			$grouptotals_real[6] = 0;
			$grouptotals_real[7] = 0;
			$grouptotals_real[8] = 0;
			$grouptotals_real[9] = 0;
			$grouptotals_real[10] = 0;
			$grouptotals_real[11] = 0;

			$total_real_asset_cost = 0;
			$total_cost_cms = 0;


			$sql_i = "select order_item_cost_group, order_item_type, ".
				   "order_item_system_price, order_item_real_system_price, order_item_quantity,  ".
				   "order_item_system_price_freezed, order_item_quantity_freezed, project_id " . 
				   "from order_items ".
				   " left join orders on order_id = order_item_order " . 
				   " left join projects on project_order = order_id " . 
				   "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
				   "   and order_item_order = " . dbquote($row["project_order"]) . 
				   "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
				   "   or order_item_type = " . ITEM_TYPE_SERVICES .
				   "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
				   " ) " .
				   " and order_item_cost_group > 0 " . 
				   " order by order_item_cost_group";

			
			$res_i = mysql_query($sql_i) or dberror($sql_i);
			while($row_i = mysql_fetch_assoc($res_i))
			{
				$tmp = ($row_i["order_item_real_system_price"] * $row_i["order_item_quantity"]);
				
				if($base_currency != "chf")
				{
					$cer_basicdata = get_cer_basicdata($row_i["project_id"]);
					$exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
					$factor = $cer_basicdata["cer_basicdata_factor"];
					
					$tmp = $factor*$tmp /$exchange_rate;
				}
				$grouptotals_real[$row_i["order_item_cost_group"]] =  $grouptotals_real[$row_i["order_item_cost_group"]] + $tmp;
			}






			/********************************************************************
				project items coming from catalogue orders
			*********************************************************************/ 

			$oip_group_totals = array();
			$sql_oip = "select order_items_in_project_id, order_items_in_project_costgroup_id, " . 
					   "order_items_in_project_order_item_id, " . 
					   " IF(address_shortcut <> '', address_shortcut, order_item_supplier_freetext) as address_shortcut, " .
					   "order_item_text,  " .
					   "DATE_FORMAT(order_item_ordered, '%d.%m.%y') as date1, " .
						"DATE_FORMAT(order_item_pickup, '%d.%m.%y') as date2, " .
						"DATE_FORMAT(order_item_arrival, '%d.%m.%y') as date3, " .
						"TRUNCATE(sum(order_item_quantity * order_items_in_project_real_price), 2) as investment, ".
					   " order_item_po_number, project_cost_groupname_name " . 
					   " from order_items_in_projects " .
					   " left join order_items on order_item_id = order_items_in_project_order_item_id " .
					   " left join addresses on order_item_supplier_address = address_id ".
					   " left join project_cost_groupnames on project_cost_groupname_id =  order_items_in_project_costgroup_id " .
					   " where order_items_in_project_project_order_id = " . dbquote($row["project_order"]) . 
					   " group by order_items_in_project_id";

			$grouptotals_oip = 0;
			$res_oip = mysql_query($sql_oip) or dberror($sql_oip);
			while($row_oip = mysql_fetch_assoc($res_oip))
			{
				$tmp = $row_oip["investment"];
				if($base_currency != "chf")
				{
					$tmp = $factor*$tmp /$exchange_rate;
				}
				
				
				$grouptotals_oip = $grouptotals_oip  + $tmp;

				if(array_key_exists($row_oip["order_items_in_project_costgroup_id"], $oip_group_totals))
				{
					$oip_group_totals[$row_oip["order_items_in_project_costgroup_id"]] = 		$oip_group_totals[$row_oip["order_items_in_project_costgroup_id"]] + $tmp;
				}
				else
				{
					$oip_group_totals[$row_oip["order_items_in_project_costgroup_id"]] = $tmp;
				}
			}


			foreach($grouptotals_real as $key=>$value)
			{
			
				if(array_key_exists($key, $oip_group_totals))
				{
					$grouptotals_real[$key] = $grouptotals_real[$key] + $oip_group_totals[$key];
				}
			}

			$total_real_asset_cost = $grouptotals_real[7] +  $grouptotals_real[2] +  $grouptotals_real[6] +  $grouptotals_real[10] +  $grouptotals_real[11] +  $grouptotals_real[9] +  $grouptotals_real[8];



			$sheet->write($row_index, $col_index01, "Amount", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+1,"per sqm", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+2,"per fte", $f_black_on_grey);
			$sheet->write($row_index, $col_index01+3,"% of Cost", $f_black_on_grey);
			
			$row_index++;

			//construction
			$amount = 0;
			
			$amount = $grouptotals_real[7];

			$sheet->write($row_index, $col_index01, $amount, $f_number);
			
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($total_real_asset_cost > 0)
			{
				//$percent_of_cer = round($amount/$construction,2);
				$percent_of_cer = round($amount/$total_real_asset_cost,2);
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$row_index++;

			$total_cost_cms = $total_cost_cms + $amount;

			//fixturing
			$amount = 0;
			$amount = $grouptotals_real[2] + $grouptotals_real[6] + $grouptotals_real[10];

			$sheet->write($row_index, $col_index01, $amount, $f_number);
			
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($total_real_asset_cost > 0)
			{
				//$percent_of_cer = round($amount/$fixturing,2);
				$percent_of_cer = round($amount/$total_real_asset_cost,2);
				
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$row_index++;

			$total_cost_cms = $total_cost_cms + $amount;

			//architectural
			$amount = 0;
			$amount = $grouptotals_real[9];

			$sheet->write($row_index, $col_index01, $amount, $f_number);
			
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($total_real_asset_cost > 0)
			{
				//$percent_of_cer = round($amount/$architectural,2);
				$percent_of_cer = round($amount/$total_real_asset_cost,2);
				
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$row_index++;

			$total_cost_cms = $total_cost_cms + $amount;

			//equipment
			$amount = 0;
			$amount = $grouptotals_real[11];

			$sheet->write($row_index, $col_index01, $amount, $f_number);
			
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($total_real_asset_cost > 0)
			{
				//$percent_of_cer = round($amount/$equipment,2);
				$percent_of_cer = round($amount/$total_real_asset_cost,2);
				
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$row_index++;

			$total_cost_cms = $total_cost_cms + $amount;

			//other
			$amount = 0;
			$amount = $grouptotals_real[8];

			$sheet->write($row_index, $col_index01, $amount, $f_number);
			
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($total_real_asset_cost > 0)
			{
				//$percent_of_cer = round($amount/$other,2);
				$percent_of_cer = round($amount/$total_real_asset_cost,2);
				
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$row_index++;

			$total_cost_cms = $total_cost_cms + $amount;
			
			


			//total real cost fixed assets
			$sheet->write($row_index, $col_index01, $total_real_asset_cost, $f_number_bold);
			
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($total_real_asset_cost/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number_bold);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($total_real_asset_cost/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number_bold);
			$percent_of_cer = "0.00";
			if($total_real_asset_cost > 0)
			{
				$percent_of_cer = round($total_real_asset_cost/$total_real_asset_cost,2);
			}
			$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent_bold);
			$row_index++;


		
			//get kemoney cost
			$amount = 0;
			
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) .
					 " and cer_investment_cer_version = 0 " .
					 " and (cer_investment_type = 15 or cer_investment_type = 17)";


			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$amount = $row_s["total"];
			}

			$amount = $amount * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $amount, $f_number);
			
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($intangibles > 0)
			{
				$percent_of_cer = round($amount/$intangibles,2);
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_percent);
			$total_cost_cms = $total_cost_cms + $amount;

			$row_index++;
		
			
			//deposit
			$amount = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 9";


			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$amount = $row_s["total"];
			}
			$amount = $amount * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $amount, $f_number);
			
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($deposit > 0)
			{
				$percent_of_cer = round($amount/$deposit,2);
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_percent);
			$total_cost_cms = $total_cost_cms + $amount;

			$row_index++;
			

			//noncapitalized
			$amount = 0;
			$sql_s = "select sum(cer_investment_amount_cer_loc) as total " . 
					 "from cer_investments " . 
					 "where cer_investment_project = " . dbquote($row["project_id"]) . 
					 " and cer_investment_cer_version = 0 " .
					 " and cer_investment_type = 13";
			
			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$amount = $row_s["total"];
			}
			$amount = $amount * $exchange_rate / $factor;
			$sheet->write($row_index, $col_index01, $amount, $f_number);
			
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($amount/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($amount/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number);
			$percent_of_cer = "0.00";
			if($noncapitalized > 0)
			{
				$percent_of_cer = round($amount/$noncapitalized,2);
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent);
			$sheet->write($row_index, $col_index01+3, "", $f_percent);
			$total_cost_cms = $total_cost_cms + $amount;
			$row_index++;
			

			//total cost
			$sheet->write($row_index, $col_index01, $total_cost_cms, $f_number_bold);
			
			$amount_per_sqm = 0;
			if($retail_area > 0)
			{
				$amount_per_sqm = round($total_cost_cms/$retail_area,2);
			}
			$sheet->write($row_index, $col_index01+1, $amount_per_sqm, $f_number_bold);
			$amount_per_fte = 0;
			if($ftes > 0)
			{
				$amount_per_fte = round($total_cost_cms/$ftes,2);
			}
			//$sheet->write($row_index, $col_index01+2, $amount_per_fte, $f_number_bold);
			$percent_of_cer = "0.00";
			if($total_investment_approved > 0)
			{
				$percent_of_cer = round($total_cost_cms/$total_investment_approved,2);
			}
			//$sheet->write($row_index, $col_index01+3, $percent_of_cer, $f_percent_bold);
			$sheet->write($row_index, $col_index01+3, "", $f_percent_bold);
			$row_index++;
			$row_index++;
		}
		
		$col_index01 = $col_index01 + 5;
		$col_index02 = $col_index02 + 5;
	}

	
	

}

$xls->close(); 

?>