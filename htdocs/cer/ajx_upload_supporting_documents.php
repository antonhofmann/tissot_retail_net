<?php 
	
/********************************************************************

ajx_upload_supporting_documents.php

Upload AF/LN supporting documents

Created by:     Anton Hofmann (aho@mediaparx.ch)
Date created:   2013-12-21
Modified by:    Anton Hofmann (aho@mediaparx.ch)
Date modified:  2013-12-21
Version:        1.0.0

Copyright (c) 2013, Swatch AG, All Rights Reserved.
*********************************************************************/
	
	session_name("retailnet");
	session_start();

	
	require "../include/frame.php";
	require "include/get_functions.php";


	switch ($_REQUEST['section']) {
		
		// save files
		case 'save':
			$action = 0;
	
			if( has_access("has_access_to_cer") && $_REQUEST["id"] && $_REQUEST["file"]) {
				
				$model = new Model(Connector::DB_CORE);
				
				$project = get_project($_REQUEST["id"]);
				$order_id = $project["project_order"];
				$orderNumber = $project["order_number"];

				$category = $_REQUEST["category"];
		
				foreach($_REQUEST["file"] as $key => $file) {
					
					// insert new file
					if (strpos($file, '/tmp/') !== false) {
						
						$dir = "/files/orders/$orderNumber";
						$filename = basename($file);
						$extension = File::extension($filename);
						
						// move temporary file to target destination
						$copy = Upload::moveTemporaryFile($file, $dir);
						
						if ($copy) { 
							
							$type = $model->query("
								SELECT file_type_id 
								FROM file_types 
								WHERE file_type_extension LIKE '%$extension%'
							")->fetch();
							
							// save file in db
							$sth = $model->db->prepare("
								INSERT INTO order_files (
									order_file_order,
									order_file_title,
									order_file_path,
									order_file_type,
									order_file_category,
									order_file_owner,
									date_created,
									user_created
								) VALUES (
									:order, :title, :file, :type, :category, :owner, CURRENT_TIMESTAMP, :user
								)
							");
								
							$action = $sth->execute(array(
								'order' => $order_id,
								'title' => $_REQUEST['title'][$key],
								'file' => "$dir/$filename",
								'type' => $type['file_type_id'],
								'category' => $category,
								'owner' => User::instance()->id,
								'user' => User::instance()->login
							));
						}
					} 
					// update file data
					elseif (isset($_REQUEST['title'][$key])) {
						
						// order file exist
						$result = $model->query("
							SELECT order_file_id 
							FROM order_files 
							WHERE order_file_id = $key
						")->fetch();
						
						// update file title
						if ($result['order_file_id']) { 
							
							$sth = $model->db->prepare("
								UPDATE order_files SET
									order_file_title = :title
								WHERE order_file_id = :id
							");
							
							$action = $sth->execute(array(
								'title' => $_REQUEST['title'][$key],
								'id' => $key
							));
						}
					}
				}
				
				$response = array(
					'response' => $action,
					'reload' => true
				);
			}
			
		break;
		
		
		// delete file
		case 'delete':
			
			if ($_REQUEST['id']) {
					
				$id = $_REQUEST['id'];
				$model = new Model(Connector::DB_CORE);
					
				$file = $model->query("
					SELECT *
					FROM order_files
					WHERE order_file_id = $id
				")->fetch();
					
				if (file_exists($_SERVER['DOCUMENT_ROOT'].$file['order_file_path'])) {
			
					$filename = basename($file['order_file_path']);
					$dirpath = file::dirpath($file['order_file_path']);
			
					file::remove($file['order_file_path']);
					file::remove("$dirpath/thumbnail/$filename");
			
					$delete = $model->db->exec("
						DELETE FROM order_files
						WHERE order_file_id = $id
					");
			
					$response = array($filename => $delete);
				}
			}
		
		break;
		
		// load files
		default:
			
			$id = $_REQUEST['id'];
			$category = $_REQUEST['category'];
			
			if ($id) {
				
				$project = get_project($_REQUEST['id']);
				$order = $project["project_order"];
				
				$model = new Model(Connector::DB_CORE);
					
				$result = $model->query("
					SELECT DISTINCT
						order_file_id AS id,
						order_file_title AS title,
						order_file_description AS description,
						order_file_path AS file
					FROM order_files
					LEFT JOIN order_file_categories ON order_file_category_id = order_file_category
	                LEFT JOIN users ON user_id = order_file_owner
	               	LEFT JOIN file_types ON order_file_type = file_type_id
					WHERE order_file_category_id = $category AND order_file_order = $order AND order_file_id > 0
				")->fetchAll();
				
				$response = Upload::get($result);
			}
			
		break;
	}

	header('Content-Type: text/json');
	echo json_encode($response);
?>	