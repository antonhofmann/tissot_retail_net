<?php
/********************************************************************

    ln_lease_details_pdf.php

    Print Lease Details

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-09-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-09-26
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require "include/financial_functions.php";
require "../shared/func_posindex.php";
require "include/get_project.php";
require_once "include/in_financial_data.php";

check_access("has_access_to_cer");


/********************************************************************
    prepare all data needed
*********************************************************************/



// get all data needed from project
$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
	   "left join currencies on currency_id = country_currency " . 
       "left join postypes on postype_id = project_postype ".
	   "left join ln_basicdata on ln_basicdata_project = project_id ".
	   "left join floors on floor_id = project_floor " .
	   "where project_id = " . param("pid") . 
	   " and ln_basicdata_version = " . $ln_version;


$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$posdata = get_pos_data($row["project_order"]);
	$posleases = get_pos_leasedata($posdata["posaddress_id"], $row["project_order"]);
	$currency = get_cer_currency(param("pid"));


	
	$order_number = $row["project_order"];
	//POS Basic Data
	$posname = $row["order_shop_address_company"];

	if($row["order_shop_address_company2"])
    {
		$posname .= ", " . $row["order_shop_address_company2"];
	}

	
	$posaddress = $row["country_name"];

	if($row["order_shop_address_zip"])
    {
		$posaddress .= ", " . $row["order_shop_address_zip"] . " " . $row["order_shop_address_place"];
	}
	else
	{
		$posaddress .= ", " . $row["order_shop_address_place"];
	}

	$posaddress .= ", " . $row["order_shop_address_address"];
	
	if($row["order_shop_address_address2"])
    {
		$posaddress .= ", " . $row["order_shop_address_address2"];
	}

	$city_name = $row["order_shop_address_place"];
	
	$planned_opening_date = to_system_date($row["project_real_opening_date"]);
	$sales_surface = $row["project_cost_sqms"];
	$total_surface = $row["project_cost_totalsqms"];
	//$gross_surface = $row["project_cost_grosssqms"];
	$gross_surface = $row["project_cost_totalsqms"];

	$page_title = "Lease Details: " . $row["project_number"];
	$page_title = "Lease Details ";

	$project_number = $row["project_number"];
	$country_name = $row["country_name"];
	$placement = $row["floor_name"];
	$postype2 = $row["project_costtype_text"] . " " . $row["postype_name"];
	
	if($row["productline_subclass_name"])
	{
		$postype = $row["projectkind_name"] . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"] . " / " . $row["productline_subclass_name"];
	}
	else {
		$postype = $row["projectkind_name"] . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"];
	}

	$ln_remarks = $row["ln_basicdata_remarks"];
	$ln_brands = $row["ln_basicdata_brands"];
	$ln_size_remarks = $row["ln_basicdata_size_remarks"];
	$ln_area = $row["ln_basicdata_area"];
	$ln_rent = $row["ln_basicdata_rent"];
	$ln_availability = $row["ln_basicdata_availability"];


	$exchange_rate_factor = $ln_basicdata['ln_basicdata_factor'];
	if(!$ln_basicdata['ln_basicdata_factor'])
	{
		$exchange_rate_factor = 1;
	}

	$exchange_rate = 1;
	if($ln_basicdata['ln_basicdata_exchangerate'] > 0)
	{
		$exchange_rate = $ln_basicdata['ln_basicdata_exchangerate'];
	}



	
	//get annual rent
	$annual_rent = "";
	$salespercent = "";

	$sql = "select * from posleases " .
		   "where poslease_lease_type = 1 and poslease_posaddress = " . $posdata["posaddress_id"] .
		   " order by poslease_startdate DESC ";
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		
		$duration_in_years = $row["poslease_enddate"] - $row["poslease_startdate"];
		$tmp1 = 13 - substr($row["poslease_startdate"], 5,2) + substr($row["poslease_enddate"], 5,2);
		$tmp2 = ($duration_in_years - 1)*12;
		$duration_in_years = round(($tmp1 + $tmp2) / 12, 1);

		$total_lease_commitment = 0;
		$sql_cer = "select * from cer_expenses " .
					  "where cer_expense_project = " . param("pid") .
			          " and cer_expense_cer_version = 0 " .
					  " and (cer_expense_type = 2 or cer_expense_type = 3 or cer_expense_type = 16 or cer_expense_type = 18 or cer_expense_type = 19 or cer_expense_type = 20)";

		$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
		while ($row_cer = mysql_fetch_assoc($res_cer))
		{
			$total_lease_commitment = $total_lease_commitment + 1*$row_cer["cer_expense_amount"];
			
		}

		$total_sales = 0;
		$sql =  "select * from cer_revenues " .
				"where cer_revenue_project = " . param("pid") .
			    " and cer_revenue_cer_version = 0 ";

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$sales_watches_values = $row["cer_revenue_watches"];
			$sales_jewellery_values = $row["cer_revenue_jewellery"];
			$sales_customer_service_values = $row["cer_revenue_customer_service"];
			$total_sales = $total_sales + 1*$sales_watches_values + 1*$sales_jewellery_values + 1*$sales_customer_service_values;

		}

		
		if($duration_in_years > 0)
		{
			$annual_rent = $total_lease_commitment / $duration_in_years;
		}
		if($total_sales > 0)
		{ 
			$salespercent = 100 * $total_lease_commitment / $total_sales/100;
		}

		$annual_rent = round($total_lease_commitment / $duration_in_years, 0);

		$salespercent = round(100*$salespercent, 2) . "%";
		
	}


	//get areas and neighbourhood information
	$posareas = "";
	if($project["pipeline"] == 0)
	{
		$sql_i = "select * from posareas " . 
				 "left join posareatypes on posareatype_id = posarea_area " .
				 "where posarea_posaddress = " . $project["posaddress_id"];
		
	}
	elseif($project["pipeline"] == 1)
	{
		
		$sql_p = "select posorder_posaddress from posorderspipeline where posorder_order = " . $project["order_id"];
		$res_p = mysql_query($sql_p) or dberror($sql_p);

		if ($row_p = mysql_fetch_assoc($res_p))
		{
			$sql_i = "select * from posareaspipeline " . 
					 "left join posareatypes on posareatype_id = posarea_area " .
					 "where posarea_posaddress = " . $row_p["posorder_posaddress"];
		}
	}
	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{
		$posareas .= $row_i["posareatype_name"] . ", ";
	}
	$posareas = substr($posareas,0,strlen($posareas)-2);


	

	

	
	/********************************************************************
		prepare pdf
	*********************************************************************/

	
	require_once('../include/tcpdf/config/lang/eng.php');
	require_once('../include/tcpdf/tcpdf.php');


    //Instanciation of inherited class
	$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
	$pdf->SetMargins(10, 23, 10);
	$pdf->setPrintHeader(false);
	$pdf->SetAutoPageBreak(false);

	$pdf->Open();

	$pdf->SetFillColor(220, 220, 220); 

	$pdf->AddFont('arialn','');
	$pdf->AddFont('arialn','B');
	$pdf->AddFont('arialn','I');
	$pdf->AddFont('arialn','BI');

	$pdf->AddPage();
	$new_page = 0;

	//PRINT DATA

	if($exchange_rate_factor > 0)
	{
		$e_rate = $exchange_rate/$exchange_rate_factor;
	}
	else
	{
		$e_rate = $exchange_rate;
	}


	//Logo
	$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
	//arialn bold 15
	$pdf->SetFont('arialn','B',18);
	//Move to the right
	$pdf->Cell(80);
	//Title
	$pdf->SetY(0);
	$pdf->Cell(0,33,$page_title,0,0,'R');
	//Line break
	$pdf->SetY(23);


	
	
	$pdf->setXY(10, $pdf->getY());
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(97,4,"Address Data",1, 0, 'L', 1);
	$pdf->Cell(90,4,"Location Data",1, 0, 'L', 1);
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);
	$pdf->Cell(30,4,"POS Name:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$posname,1, 0, 'L', 0);
	$pdf->Cell(40,4,"Environment:",1, 0, 'L', 0);
	$pdf->Cell(50,4,$posareas,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"Country:",1, 0, 'L', 0);
	$pdf->Cell(67,4,$country_name,1, 0, 'L', 0);
	$pdf->Cell(40,4,"Placement:",1, 0, 'L', 0);
	$pdf->Cell(50,4,$placement,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"City:",1, 0, 'L', 0);	
	$pdf->Cell(67,4,$city_name,1, 0, 'L', 0);
	//$pdf->Cell(40,4,"Gross / Total / Sales surface:",1, 0, 'L', 0);
	//$pdf->Cell(50,4,$gross_surface . " m2 / " .$total_surface . " m2 / " . $sales_surface . " m2",1, 0, 'L', 0);
	$pdf->Cell(40,4,"Total / Sales surface:",1, 0, 'L', 0);
	$pdf->Cell(50,4,$total_surface . " m2 / " . $sales_surface . " m2",1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->Cell(30,4,"Type:",1, 0, 'L', 0);	
	$pdf->Cell(67,4,$postype2,1, 0, 'L', 0);
	$pdf->Cell(40,4,"",1, 0, 'L', 0);
	$pdf->Cell(50,4,"",1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();


	
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(187,7,"Key points",1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->MultiCell(187,3.5, $ln_remarks, 1, "T");
	$pdf->Ln();


		
	//rental Details
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(72,5,"",1, 0, 'L', 0);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years))
		{
			$pdf->Cell(23,5,$years[$i],1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",1, 0, 'R', 0);
		}
	}
	
	$pdf->Ln();

	
	$pdf->SetFont('arialn','B',9);
	$x = $pdf->GetX();
	$pdf->Cell(187,20,"",1, 0, 'L', 0);
	$pdf->SetX($x);

	$pdf->Cell(72,20," ",0, 0, 'R', 0);
	for($i=0;$i<5;$i++)
	{
		$pdf->Cell(23,20," ",1, 0, 'R', 0);
	}
	

	$pdf->SetX($x);

	$pdf->SetFont('arialn','',9);

	$pdf->Cell(72,5,"Annual fix rental costs in KCHF",0, 0, 'L', 0);
	
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $fixedrents))
		{
			$pdf->Cell(23,5,number_format($e_rate*$fixedrents[$years[$i]]/1000, 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();

	$pdf->Cell(72,5,"Turnover based rental costs per year in KCHF",0, 0, 'L', 0);
	
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $turnoverbasedrents))
		{
			$pdf->Cell(23,5,number_format($e_rate*$turnoverbasedrents[$years[$i]]/1000, 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();


	$pdf->Cell(72,5,"Additional rental costs per year in KCHF",0, 0, 'L', 0);
	
	$pdf->SetFont('arialn','',9);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $additionalrents))
		{
			$pdf->Cell(23,5,number_format($e_rate*$additionalrents[$years[$i]]/1000, 0, ".", "'"),0, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",0, 0, 'R', 0);
		}
	}
	$pdf->Ln();


	$pdf->SetFont('arialn','B',9);

	$pdf->Cell(72,5,"Totals",1, 0, 'L', 0);
	for($i=0;$i<5;$i++)
	{
		if(array_key_exists($i, $years) and array_key_exists($years[$i], $sales_units_watches_values))
		{
			$pdf->Cell(23,5,number_format($e_rate*$rents[$years[$i]]/1000, 0, ".", "'"),1, 0, 'R', 0);
		}
		else
		{
			$pdf->Cell(23,5," ",1, 0, 'R', 0);
		}
	}
	$pdf->Ln();
	$pdf->Ln();

	$pdf->SetFont('arialn','',9);

	

	//lease details (TAB Rental from CER)
	
	$pdf->SetFont('arialn','',9);
	$pdf->Cell(35,6,"Lease Type:",1, 0, 'L', 0);
	if(count($posleases) > 0) {
		$pdf->Cell(31,6,$posleases["poslease_type_name"],1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(31,6,"",1, 0, 'L', 0);
	}
	$pdf->Cell(31,6,"Start Date:",1, 0, 'L', 0);
	if(count($posleases) > 0) {
		$pdf->Cell(31,6, to_system_date($posleases["poslease_startdate"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(31,6, "",1, 0, 'L', 0);
	}
	$pdf->Cell(31,6,"End Date:",1, 0, 'L', 0);
	if(count($posleases) > 0) {
		$pdf->Cell(28,6,to_system_date($posleases["poslease_enddate"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(28,6,"",1, 0, 'L', 0);
	}
	$pdf->Ln();



	$pdf->Cell(35,6,"Extension Option:",1, 0, 'L', 0);
	if(count($posleases) > 0) {
		$pdf->Cell(31,6,to_system_date($posleases["poslease_extensionoption"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(31,6,"",1, 0, 'L', 0);
	}
	$pdf->Cell(31,6,"Exit Option to Date:",1, 0, 'L', 0);
	if(count($posleases) > 0) {
		$pdf->Cell(31,6,to_system_date($posleases["poslease_exitoption"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(31,6,"",1, 0, 'L', 0);
	}
	$pdf->Cell(31,6,"Term.Deadline:",1, 0, 'L', 0);
	if(count($posleases) > 0) {
		$pdf->Cell(28,6,$posleases["poslease_termination_time"],1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(28,6,"",1, 0, 'L', 0);
	}
	$pdf->Ln();

	
		
	

	$pdf->Cell(35,6,"Rent p.a. in CHF:",1, 0, 'L', 0);
	$pdf->Cell(31,6, number_format(round($e_rate*$annual_rent,0)),1, 0, 'L', 0);
	$pdf->Cell(31,6,"Rent as % of Sales:",1, 0, 'L', 0);
	$pdf->Cell(31,6,$salespercent,1, 0, 'L', 0);
	
	
	$text = "";
	if(count($posleases) > 0) {
		if($posleases["poslease_indexclause_in_contract"] == 1) {
			$text .= "Index Clause";
		}
		if($posleases["poslease_indexclause_in_contract"] == 1) {
			if($text) {
			$text .= " / ";	
			}
			$text .= "Tacit Renewal Clause";
		}
	}
	
	$pdf->Cell(59,6,$text,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->Cell(35,6,"Deadline for property:",1, 0, 'L', 0);
	$pdf->Cell(31,6,to_system_date($cer_basicdata["cer_basicdata_deadline_property"]),1, 0, 'L', 0);
	$pdf->Cell(31,6,"Handover Date:",1, 0, 'L', 0);
	if(count($posleases) > 0) {
		$pdf->Cell(31,6,to_system_date($posleases["poslease_handoverdate"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(31,6,"",1, 0, 'L', 0);
	}
	$pdf->Cell(31,6,"First Rent Payed as from:",1, 0, 'L', 0);
	if(count($posleases) > 0) {
		$pdf->Cell(28,6,to_system_date($posleases["poslease_firstrentpayed"]),1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(28,6,"",1, 0, 'L', 0);
	}
	$pdf->Ln();


	$pdf->Cell(66,6,"Rent Free Period in Weeks:",1, 0, 'L', 0);
	if(count($posleases) > 0) {
		$pdf->Cell(31,6,$posleases["poslease_freeweeks"],1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(31,6,"",1, 0, 'L', 0);
	}
	$pdf->Cell(31,6,"",1, 0, 'L', 0);

	$pdf->Cell(31,6,"",1, 0, 'L', 0);
	$pdf->Cell(28,6,"",1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->Cell(35,6,"Negotiator:",1, 0, 'L', 0);
	if(count($posleases) > 0) {
		$pdf->Cell(152,6,$posleases["poslease_negotiator"],1, 0, 'L', 0);
	}
	else
	{
		$pdf->Cell(152,6,"",1, 0, 'L', 0);
	}
	$pdf->Ln();

	$text = "Landlord (Negotiation Partner):\r\n";
	if(count($posleases) > 0) {
		$text .= $posleases["poslease_landlord_name"];
	}

	$pdf->MultiCell(187,3.5, $text, 1, "T");

	$text = "Conditions for the application of variable rent:\r\n";
	if(count($posleases) > 0) {
		$text .= $posleases["poslease_negotiated_conditions"];
	}

	$pdf->MultiCell(187,3.5, $text, 1, "T");

	


	//Position at 1.5 cm from bottom
	$pdf->SetY(282);
	$pdf->SetFont('arialn','I',8);
	$pdf->Cell(0,10, to_system_date(date("d.m.y")),0,0,'R');




	

	// write pdf
	$pdf->Output('lease_details_' . $posname . '_' . $project_number . '.pdf');

}

?>