<?php
/********************************************************************

    inflationrate_rate.php

    Creation and mutation of inflation rate records records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_full_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/
$sql_countries = "select country_id, country_name ". 
                 "from countries " . 
				 "order by country_name";

/********************************************************************
    build form 
*********************************************************************/
$form = new Form("cer_inflationrates", "inflation rate");

$form->add_section();
$form->add_list("inflationrate_country", "Country*", $sql_countries, NOTNULL);

$form->add_section();
$form->add_edit("inflationrate_year", "Year*", NOTNULL, "", TYPE_INT, 4);
$form->add_edit("inflationrate_rate", "Rate in %*",NOTNULL, "", TYPE_DECIMAL, 12, 2);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("inflationrates");

$page->header();
$page->title(id() ? "Edit Inflation Rate" : "Add Inflation Rate");
$form->render();
$page->footer();

?>
