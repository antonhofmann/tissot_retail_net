<?php
/********************************************************************

    cer_sg_form_inr03_data.php

    Get all data needed in forms

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2018-01-05
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2018-01-05
    Version:        1.0.0

    Copyright (c) 2018, Tissot, All Rights Reserved.
*********************************************************************/
check_access("has_access_to_cer");

if(isset($write_tmp_file) and $write_tmp_file == true) {
	
}
else {
	require "include/get_functions.php";
	require "../shared/func_posindex.php";
}


require "include/get_project.php";
require "include/financial_functions.php";
require_once "../shared/project_cost_functions.php";
include("include/in_financial_data.php");

$cer_version = 0;
$ln_version = 0;
$version_date = date("d.m.Y H:i:s");
if(param("cerversion"))
{
	$cer_version = param("cerversion");
	$version_date = to_system_date($cer_basicdata['date_created']);

}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
	$version_date = to_system_date($ln_basicdata['date_created']);
}


if($project["possubclass_name"]) {
	$postype = $project["projectkind_name"] . ", " . $project["project_costtype_text"] . ", " . $project["postype_name"] . ", " . $project["possubclass_name"];
}
else {
	$postype = $project["projectkind_name"] . ", " . $project["project_costtype_text"] . ", " . $project["postype_name"];
}


$description = $ln_basicdata['ln_basicdata_remarks'];

//set defaul values
$client_is_hq_agent = 0;
$legal_entity_name = "";
$project_name = "";
$project_manager = "";
$requested_amount = "";
$budget_amount = "";
$currency_symbol = "";
$investment_amount = "";
$project_start = "";
$net_present_value = "";
$key_money = "";
$project_end = "";
$pay_back_period = "";
$deposit = "";
$post_compl_review = "";
$internal_reate_of_return ="";
$other_cost = "";
$project_kind = "";


$suppliers = "";

$name1 = "";
$name2 = "";
$name3 = "";
$name4 = "";
$name5 = "";
$name6 = "";
$name7 = "";
$name8 = "";
$name9 = "";

$date1 = "";
$date2 = "";
$date3 = "";
$date4 = "";
$date5 = "";
$date6 = "";
$date7 = "";
$date8 = "";
$date9 = "";


$protocol_number1 = "";
$protocol_number1 = "";
$cer_number = "";

$opening_date = "";

$project_kind_new = "";
$project_kind_renovation = "";
$project_kind_relocation = "";



// get basic data
$cer_basicdata = get_cer_basicdata(param("pid"), $cer_version);


$approval_name1 = $cer_basicdata["cer_basicdata_approvalname1"];
$approval_name2 = $cer_basicdata["cer_basicdata_approvalname2"];
$approval_name3 = $cer_basicdata["cer_basicdata_approvalname3"];
$approval_name4 = $cer_basicdata["cer_basicdata_approvalname4"];
$approval_name5 = $cer_basicdata["cer_basicdata_approvalname5"];
$approval_name6 = $cer_basicdata["cer_basicdata_approvalname6"];
$approval_name7 = $cer_basicdata["cer_basicdata_approvalname7"];
$approval_name8 = $cer_basicdata["cer_basicdata_approvalname8"];
$approval_name9 = $cer_basicdata["cer_basicdata_approvalname9"];
$approval_name10 = $cer_basicdata["cer_basicdata_approvalname10"];

$approval_name11 = $cer_basicdata["cer_summary_in01_sig01"];
$approval_name12 = $cer_basicdata["cer_summary_in01_sig02"];
$approval_name13 = $cer_basicdata["cer_summary_in01_sig03"];
$approval_name14 = $cer_basicdata["cer_summary_in01_sig04"];
$approval_name15 = $cer_basicdata["cer_summary_in01_sig05"];
$approval_name16 = $cer_basicdata["cer_summary_in01_sig06"];

$approval_name18 = $cer_basicdata["cer_summary_in01_sig07"];

// get all data needed from cer_basic_data

$sql = "select * from cer_summary where cer_summary_cer_version = " . $cer_version . "  and cer_summary_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{

	$name1 = $row["cer_summary_in01_sig01"];
	$name2 = $row["cer_summary_in01_sig02"];
	$name3 = $row["cer_summary_in01_sig03"];
	$name4 = $row["cer_summary_in01_sig04"];
	$name5 = $row["cer_summary_in01_sig05"];
	$name6 = $row["cer_summary_in01_sig06"];
	$name7 = $row["cer_summary_in01_sig07"];
	$name8 = $row["cer_summary_in01_sig08"];
	$name9 = $row["cer_summary_in01_sig09"];

	$name7 = $cer_basicdata["cer_basicdata_approvalname11"];

	$date1 = to_system_date($row["cer_summary_in01_date01"]);
	$date2 = to_system_date($row["cer_summary_in01_date02"]);
	$date3 = to_system_date($row["cer_summary_in01_date03"]);
	$date4 = to_system_date($row["cer_summary_in01_date04"]);
	$date5 = to_system_date($row["cer_summary_in01_date05"]);
	$date6 = to_system_date($row["cer_summary_in01_date06"]);
	$date7 = to_system_date($row["cer_summary_in01_date07"]);
	$date8 = to_system_date($row["cer_summary_in01_date08"]);
	$date9 = to_system_date($row["cer_summary_in01_date09"]);


	$protocol_number1 = $row["cer_summary_in01_prot01"];
	$protocol_number2 = $row["cer_summary_in01_prot02"];
	$cer_number = $row["cer_summary_in01_cernr"];

	
	if($row["cer_summary_in01_review_date"] and $row["cer_summary_in01_review_date"] != '0000-00-00')
	{
		$post_compl_review = substr($row["cer_summary_in01_review_date"], 5,2) . "/" . substr($row["cer_summary_in01_review_date"], 0,4);
	}
	
}

// get all data needed from project
$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
       "left join users on user_id = project_retail_coordinator " . 
	   "where project_id = " . dbquote(param("pid"));


$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	
	if($row["address_legal_entity_name"])
	{
		$legal_entity_name = $row["address_legal_entity_name"];
	}
	else
	{
		$legal_entity_name = $row["address_company"];
	}
	
	$project_name = $row["order_shop_address_company"]. ", " . $row["order_shop_address_place"] . ", " . $row["country_name"];


	$project_manager = $row["user_firstname"] . " " . $row["user_name"];
	
	$project_kind = $row["projectkind_name"];


	
	$factor = $cer_basicdata['cer_basicdata_factor'];
	if(!$cer_basicdata['cer_basicdata_factor'])
	{
		$factor = 1;
	}

	$exchange_rate = 1;
	if($cer_basicdata['cer_basicdata_exchangerate'] > 0)
	{
		$exchange_rate = $cer_basicdata['cer_basicdata_exchangerate'];
	}
	
	
	$budget_amount = $row["project_approximate_budget"]/$cer_basicdata['cer_basicdata_exchangerate']*$factor;

	$budget_amount = number_format($budget_amount, 0, "", "'");
	$project_start = substr($row["order_date"], 5,2) . "/" . substr($row["order_date"], 0,4);
	
	//$project_start = substr($cer_basicdata['cer_basicdata_project_start'], 5,2) . "/" . substr($cer_basicdata['cer_basicdata_project_start'], 0,4);
	
	//$tmp = date('Y-m-d H:i:s', strtotime($row["project_real_opening_date"] . ' + 90 day'));
	$tmp = $row["project_real_opening_date"];
	$project_end = substr($tmp, 5,2) . "/" . substr($tmp, 0,4);

	//$project_end = substr($cer_basicdata['cer_basicdata_project_end'], 5,2) . "/" . substr($cer_basicdata['cer_basicdata_project_end'], 0,4);

	$order_number = $row["project_order"];
	$address_country = $row["address_country"];

	$opening_date = to_system_date($row["project_real_opening_date"]);

	if($row["project_projectkind"] == 1 or $row["project_projectkind"] == 9)  {
		$project_kind_new = "X";
	}
	elseif($project["project_projectkind"] == 6)  {
		$project_kind_relocation = "X";
	}
	elseif($row["project_projectkind"] == 2 or $row["project_projectkind"] == 3)  {
		$project_kind_renovation = "X";
	}
}


//get predecessor project
$predecessor_opening_date = '';
$sql = "select posorder_posaddress from posorders " . 
       " where posorder_order = " . dbquote($project["project_order"]);

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$predecessor_project = get_predecessor_project($row["posorder_posaddress"], $project["project_order"]);
	
	if(count($predecessor_project) > 0) {
		$predecessor_opening_date = to_system_date($predecessor_project["posorder_opening_date"]);
	}
}



//get suppliers
$tmp = array();
$sql = "select DISTINCT costsheet_company from costsheets " . 
       "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " .
	   " where costsheet_project_id = " . dbquote(param("pid")) . 
	   " and costsheet_is_in_budget = 1 " . 
	   " and pcost_group_posinvestment_type_id = 3 " . 
	   " and costsheet_company <> '' and costsheet_company is not null";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$tmp[] = $row["costsheet_company"];
}
if(count($tmp) > 0)
{
	$suppliers = implode(', ', $tmp);
}

$franchisee_address = get_address($project["order_franchisee_address_id"]);

$owner_company = $franchisee_address['company'];
if($franchisee_address['address']) {
	$owner_company .= ", " . $franchisee_address['address'];
}
$owner_company .= ", " . $franchisee_address['place'];
$owner_company .= ", " . $franchisee_address['country_name'];


$client_address = get_address($project["order_client_address"]);

$client_currency = get_cer_currency(param("pid"), $cer_version);
$currency_symbol = $client_currency["symbol"];

//get milestones
$date_sis_approval = "";

$sql = "select project_milestone_milestone, project_milestone_date from project_milestones " . 
       "where project_milestone_milestone in(23) " . 
	   " and project_milestone_project = " . dbquote(param("pid"));
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($row["project_milestone_milestone"] == 23)
	{
		if($row["project_milestone_date"] == NULL or $row["project_milestone_date"] == '0000-00-00')
		{
			$date_sis_approval = '';
		}
		else
		{
			$date_sis_approval = to_system_date($row["project_milestone_date"]);
		}
	}
}


//get financial data
include("include/in_financial_data.php");	



//CHF
$exr = $cer_basicdata["cer_basicdata_exchangerate"];
$exrf = $cer_basicdata["cer_basicdata_factor"];

$investment_amount_chf = round($exr*($investment_total+$landloard_contribution)/$exrf/1000,0);
$key_money_chf = round($exr*$intagibles_total/$exrf/1000, 0);
$deposit_chf = round($exr*$deposit/$exrf/1000, 0);
$other_cost_chf = round($exr*$other_noncapitalized_cost/$exrf/1000, 0);

$construction_total = $construction_total + $other_costs + $equipment_costs;

$construction_total_chf  = round($exr*$construction_total/$exrf/1000,0);
$fixed_assets_total_chf  = round($exr*$fixed_assets_total/$exrf/1000,0);
$transportation_total_chf  = round($exr*$transportation_total/$exrf/1000,0);


//$merchandising_total_chf =  round($exr*$merchandising_total/$exrf/1000,0);
//$transportation_total_chf =  round($exr*$transportation_total/$exrf/1000,0);

$investment_business_partner_chf = round($exr*($investment_total+$landloard_contribution)/$exrf*$cer_basicdata["cer_basicdata_franchsiee_investment_share"]/100/1000, 0);

$requested_amount_chf = $exr*($investment_total+$landloard_contribution)/$exrf - ($exr*($investment_total+$landloard_contribution)/$exrf*$cer_basicdata["cer_basicdata_franchsiee_investment_share"]/100);
$requested_amount_chf = round($requested_amount_chf/1000,0);


//local currency
$investment_amount = round(($investment_total+$landloard_contribution)/1000,0);
$key_money = round($intagibles_total/1000, 0);
$deposit = round($deposit/1000, 0);
$other_cost = round($other_noncapitalized_cost/1000, 0);

$construction_total  = round($construction_total/1000,0);
$fixed_assets_total  = round($fixed_assets_total/1000,0);
$transportation_total = round($transportation_total/1000,0);

//$merchandising_total =  round($merchandising_total/1000,0);
//$transportation_total =  round($transportation_total/1000,0);

$investment_business_partner = round(($investment_total+$landloard_contribution)*$cer_basicdata["cer_basicdata_franchsiee_investment_share"]/100/1000, 0);

$requested_amount1 = ($investment_total+$landloard_contribution) - (($investment_total+$landloard_contribution)*$cer_basicdata["cer_basicdata_franchsiee_investment_share"]/100);
$requested_amount = round($requested_amount1/1000,0);

//calculate depreciation on requested amounts
$depreciation_on_requested_amount_in_percent = array();
$share = 1;
if($cer_basicdata["cer_basicdata_franchsiee_investment_share"] > 0)
{
	$share = (100-$cer_basicdata["cer_basicdata_franchsiee_investment_share"])/100;
}

$k = 1;
for($i=$first_year;$i<=$last_year;$i++)
{
	if($k < 11)
	{
		if(array_key_exists($i, $depreciation_values) and $depreciation_values[$i] > 0) {
			$depreciation_on_requested_amount_in_percent[$i] = round($share*100*$depreciation_values[$i]/$total_gross_sales_values[$i], 2) . "%";
		}
		else
		{
			$depreciation_on_requested_amount_in_percent[$i] = '';
		}
		$k++;
	}
}

//depreciation period
$dyears = '';
$dmonths = '';
$depr_period = '';
foreach($depryears as $key=>$value)
{
	if($value > $dyears) {
		$dyears = $value;
	}
	if($deprmonths[$key] > $dmonths) {
		$dmonths = $deprmonths[$key];
	}
}

if($dyears > 0 and $dmonths > 0)
{
	$depr_period = $dyears . "y-" . $dmonths . "m";
}
elseif($dyears > 0)
{
	$depr_period = $dyears . "y-0m";
}
elseif($dmonths > 0)
{
	$depr_period = "0y-" . $dmonths . "m";
}




$pix1 = ".." . $ln_basicdata["ln_basicdata_pix1"];
$pix2 = ".." . $ln_basicdata["ln_basicdata_pix2"];
$pix3 = ".." . $ln_basicdata["ln_basicdata_pix3"];
$pix4 = ".." . $ln_basicdata["ln_basicdata_pix4"];

$floor_plan = ".." . $ln_basicdata["ln_basicdata_floorplan"];
$location_layout = ".." . $ln_basicdata["ln_basicdata_location_layout"];
$lease_agreement = ".." . $ln_basicdata["ln_basicdata_draft_aggreement"];
$relocation_doc = ".." . $ln_basicdata["ln_basicdata_relocation_doc"];
$sales_doc = ".." . $ln_basicdata["ln_basicdata_sales_doc"];
$facade_image = ".." . $ln_basicdata["ln_basicdata_fassade_image"];
$pos_section_image = ".." . $ln_basicdata["ln_basicdata_pos_section_image"];


$page_title = "LNR Booklet: " . $row["project_number"];



//LN supporting documents
$ln_supporting_documents = array();
$sql_d = "select order_file_path " . 
		 " from order_files " . 
		 " where order_file_order = " . dbquote($order_number) . 
		 " and order_file_category = 17 " . 
		 " and order_file_type IN (2,13)";

$res_d = mysql_query($sql_d) or dberror($sql_d);
while ($row_d = mysql_fetch_assoc($res_d))
{
	$ln_supporting_documents[] = $_SERVER["DOCUMENT_ROOT"] . $row_d["order_file_path"];
}


//get areas and neighbourhood information
$posareas = "";
if($project["pipeline"] == 0)
{
	$sql_i = "select * from posareas " . 
			 "left join posareatypes on posareatype_id = posarea_area " .
			 "where posarea_posaddress = " . $project["posaddress_id"];
	
}
elseif($project["pipeline"] == 1)
{
	
	$sql_p = "select posorder_posaddress from posorderspipeline where posorder_order = " . $project["order_id"];
	$res_p = mysql_query($sql_p) or dberror($sql_p);

	if ($row_p = mysql_fetch_assoc($res_p))
	{
		$sql_i = "select * from posareaspipeline " . 
				 "left join posareatypes on posareatype_id = posarea_area " .
				 "where posarea_posaddress = " . $row_p["posorder_posaddress"];
	}
}
$res_i = mysql_query($sql_i) or dberror($sql_i);
while ($row_i = mysql_fetch_assoc($res_i))
{
	$posareas .= $row_i["posareatype_name"] . ", ";
}
$posareas = substr($posareas,0,strlen($posareas)-2);


//neighbourhood
$neighbourhoods = array();
if($project["pipeline"] == 0)
{
	$sql_e = "select * from posorders where posorder_order = " . $project["order_id"];
}
elseif($project["pipeline"] == 1)
{
	$sql_e = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
}

$res_e = mysql_query($sql_e) or dberror($sql_e);
if ($row_e = mysql_fetch_assoc($res_e))
{
	$tmp = "";
	$bt = get_business_type($row_e["posorder_neighbour_left_business_type"]);
	$pr = get_price_range($row_e["posorder_neighbour_left_price_range"]);
	if($bt or $pr)
	{
		$tmp = " (". $bt . " " . $pr . ")";
	}
	$neighbourhoods["Shop on left side"] = $row_e["posorder_neighbour_left"] . $tmp;

	$tmp = "";
	$bt = get_business_type($row_e["posorder_neighbour_right_business_type"]);
	$pr = get_price_range($row_e["posorder_neighbour_right_price_range"]);
	if($bt or $pr)
	{
		$tmp = " (". $bt . " " . $pr . ")";
	}
	$neighbourhoods["Shop on right side"] = $row_e["posorder_neighbour_right"] . $tmp;

	$tmp = "";
	$bt = get_business_type($row_e["posorder_neighbour_acrleft_business_type"]);
	$pr = get_price_range($row_e["posorder_neighbour_acrleft_price_range"]);
	if($bt or $pr)
	{
		$tmp = " (". $bt . " " . $pr . ")";
	}
	$neighbourhoods["Shop across left side"] = $row_e["posorder_neighbour_acrleft"] . $tmp;

	$tmp = "";
	$bt = get_business_type($row_e["posorder_neighbour_acrright_business_type"]);
	$pr = get_price_range($row_e["posorder_neighbour_acrright_price_range"]);
	if($bt or $pr)
	{
		$tmp = " (". $bt . " " . $pr . ")";
	}
	$neighbourhoods["Shop across right side"] = $row_e["posorder_neighbour_acrright"] . $tmp;
	
	$neighbourhoods["Other brands in area"] = $row_e["posorder_neighbour_brands"];
	$neighbourhoods["Other brands in area"] = str_replace("\r\n", "", $neighbourhoods["Other brands in area"]);
	$neighbourhoods["Other brands in area"] = str_replace("\n", "", $neighbourhoods["Other brands in area"]);
	$neighbourhoods["Other brands in area"] = str_replace("\r", "", $neighbourhoods["Other brands in area"]);
	$neighbourhoods["Other brands in area"] = trim($neighbourhoods["Other brands in area"]);
}		



$ln_brands = $row["ln_basicdata_brands"];
$latest_renovation_date = to_system_date($ln_basicdata['ln_basicdata_latest_renovation']);

?>