<?php
/********************************************************************

    af_booklet_popup_inr01_pdf.php

    Print PDF for AF Booklet INR03 for Pupup Projects.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-06-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-06-16
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
$pdf_print_output = true;


define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require "include/financial_functions.php";


check_access("has_access_to_cer");

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/SetaPDF/Autoload.php');


$PDFmerger_was_used = false;

$currencies = array();
$sql = 'select currency_id, currency_symbol from currencies where currency_system = 1';

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$currencies["s"] = $row['currency_symbol'];
}


if($cer_basicdata['cer_basicdata_currency']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c1"] = $row['currency_symbol'];
	}
}

if($cer_basicdata['cer_basicdata_currency2']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency2'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c2"] = $row['currency_symbol'];
	}
}

$version_date = ""; 
if($cer_version > 0)
{
	$version_date = " Older Version " . to_system_date(substr($cer_basicdata["version_date"], 0,10)) . " " . substr($cer_basicdata["version_date"], 10,9);
}



// supporting documents
$supporting_documents = array();
$sql_d = "select order_file_path " . 
		 " from order_files " . 
		 " where order_file_order = " . dbquote($project["project_order"]) . 
		 " and order_file_category = 18 " . 
		 " and order_file_type IN (2,13)";

$res_d = mysql_query($sql_d) or dberror($sql_d);
while ($row_d = mysql_fetch_assoc($res_d))
{
	$supporting_documents[] = $_SERVER["DOCUMENT_ROOT"] . $row_d["order_file_path"];
}


// Create and setup PDF document
class MYPDF extends TCPDF
{
	
	//Page footer
	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		$this->SetFont('arialn','I',8);
		$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
	}

}

$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 10);
$pdf->setPrintHeader(false);

$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');
$pdf->Open();

include("af_INR03_popup_pdf_detail.php");

//Picture 2
if($pix2 != ".." and file_exists($pix2))
{
	//PDF
	if(substr($pix2, strlen($pix2)-3, 3) == "jpg" 
		or substr($pix2, strlen($pix2)-3, 3) == "JPG"
	    or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
		or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
	{
		$pdf->AddPage("P", "A4");
		$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
		
		if(isset($version_date) and $version_date)
		{
			$pdf->SetFont('arialn','B',12);
			//Move to the right
			$pdf->Cell(40);
			//Title
			$pdf->SetY(0);
			$pdf->Cell(0,33,$page_title . $version_date,0,0,'R');
			//Line break
			$pdf->SetY(23);
		}
		else
		{
			//arialn bold 15
			$pdf->SetFont('arialn','B',12);
			//Move to the right
			$pdf->Cell(80);
			//Title
			$pdf->SetY(0);
			$pdf->Cell(0,33,$page_title,0,0,'R');
			//Line break
			$pdf->SetY(23);
		}

		$x = $pdf->GetX();
		$y = $pdf->GetY() + 2;
		$pdf->Image($pix2,$x,$y, 0, 110, "", "", "", true, 300, "", false, false, 0, false, false,true);
	}
}


//Picture 3
if($pix3 != ".." and file_exists($pix3))
{
	//PDF
	if(substr($pix3, strlen($pix3)-3, 3) == "jpg" 
		or substr($pix3, strlen($pix3)-3, 3) == "JPG"
	    or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
		or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
	{
		//$pdf->AddPage("P", "A4");
		$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
		if(isset($version_date) and $version_date)
		{
			$pdf->SetFont('arialn','B',12);
			//Move to the right
			$pdf->Cell(40);
			//Title
			$pdf->SetY(0);
			$pdf->Cell(0,33,$page_title . $version_date,0,0,'R');
			//Line break
		}
		else
		{
			//arialn bold 15
			$pdf->SetFont('arialn','B',12);
			//Move to the right
			$pdf->Cell(80);
			//Title
			$pdf->SetY(0);
			$pdf->Cell(0,33,$page_title,0,0,'R');
		}
		
		$y = 145;
		$pdf->Image($pix3,$x,$y, 0, 110, "", "", "", true, 300, "", false, false, 0, false, false,true);
	}
}



// supporting documents
$output_done = false;
if(count($supporting_documents) > 0)
{
	foreach($supporting_documents as $key=>$source_file)
	{
		if(file_exists($source_file))
		{
			//PDF
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				
				if($output_done == false)
				{
					$pdfString = $pdf->output('', 'S');
					$tmp = SetaPDF_Core_Document::loadByString($pdfString);
					
					if(!isset($merger)) {
						$merger = new SetaPDF_Merger();
					}
					$merger->addDocument($tmp);
					$output_done = true;
				}
				
				
				$merger->addFile(array(
					'filename' => $source_file,
					'copyLayers' => true
				));


				$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetMargins(10, 23, 10);
				$pdf->Open();

				$PDFmerger_was_used = true;

			}
			elseif(substr($source_file, strlen($source_file)-3, 3) == "jpg" 
				or substr($source_file, strlen($source_file)-3, 3) == "JPG"
				 or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
				or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
			{
				
				$pdf->AddPage("P");
				$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
				//arialn bold 15
				if(isset($version_date) and $version_date)
				{
					$pdf->SetFont('arialn','B',12);
					//Move to the right
					$pdf->Cell(40);
					//Title
					$pdf->SetY(0);
					$pdf->Cell(0,33,$page_title . $version_date,0,0,'R');
					//Line break
					$pdf->SetY(23);
				}
				else
				{
					$pdf->SetFont('arialn','B',18);
					//Move to the right
					$pdf->Cell(80);
					//Title
					$pdf->SetY(0);
					$pdf->Cell(0,33,$page_title,0,0,'R');
					//Line break
					$pdf->SetY(23);
				}
				$pdf->Image($source_file,10,30, 180, 0, "", "", "", true, 300, "", false, false, 0, false, false,true);
				$output_done = false;
			}
		}
	}

}



//check if project has pdf attachments to be added to the booklet

$sql = "select order_file_path " . 
       "from projects " . 
	   "left join order_files on order_file_order = project_order " . 
       "where order_file_attach_to_cer_booklet = 1 and order_file_type = 13 and project_id = " .param("pid") . 
	   " order by order_files.date_created DESC";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$source_file = ".." . $row["order_file_path"];
	if(file_exists($source_file))
	{
		
		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		
		if($PDFmerger_was_used == false)
		{
			$merger = new SetaPDF_Merger();
		}

		$merger->addDocument($tmp);

		$merger->addFile(array(
			'filename' => $source_file,
			'copyLayers' => true
		));


		$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(true);

		$pdf->AddFont('arialn','');
		$pdf->AddFont('arialn','B');
		$pdf->AddFont('arialn','I');
		$pdf->AddFont('arialn','BI');

		$pdf->Open();

		$PDFmerger_was_used = true;
		$pdf_print_output = false;

	}
}


$file_name = "LN_PopUp_Booklet_" . str_replace(" ", "_", $posname) . "_" . date("Y-m-d") . ".pdf";



if($PDFmerger_was_used == true)
{

	if($output_done == false)
	{
		$merger->merge();
		$document = $merger->getDocument();

		$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
		$document->save()->finish();
	}
	else
	{
	
		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		
		$merger->merge();
		$document = $merger->getDocument();
		$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
		$document->save()->finish();
	}
}
else
{
	$pdf->Output($file_name, 'I');
}


?>