<?php
/********************************************************************

    sellouts.php

    Enter Sellout Data for the POS Location

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2012-08-16
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2012-08-16
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
require "include/get_project.php";

check_access("has_access_to_cer");

$sellout_project = get_project(param("pid"));


if($sellout_project["project_projectkind"] == 6 
   and $sellout_project["project_relocated_posaddress_id"] > 0)
{
	//get the original project from the pos location to be relocated
	
	$latest_project = get_latest_pos_project($sellout_project["project_relocated_posaddress_id"]);

	
	if(count($latest_project) > 0)
	{
		

		
		$pos_id = $sellout_project["project_relocated_posaddress_id"];
		$sellout_project = get_project($latest_project["project_id"]);

		//set temporary relocation project to true because latest project has no relocation info
		$sellout_project["project_projectkind"] = 6;
		
	}
	else
	{

		$pos_data = get_poslocation($sellout_project["project_relocated_posaddress_id"], "posaddresses");

		$pos_id = $sellout_project["project_relocated_posaddress_id"];

	}
}
else
{   
	$shop = $sellout_project["order_shop_address_company"] . ", " .
			$sellout_project["order_shop_address_address"] . ", " .
			$sellout_project["order_shop_address_zip"] . " " .
			$sellout_project["order_shop_address_place"] . ", " .
			$sellout_project["order_shop_address_country_name"];



	//get pos_id
	$sql = "select posorder_posaddress " . 
		   "from posorders " . 
		   "where posorder_order = " . dbquote($sellout_project["project_order"]);

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$pos_id = $row["posorder_posaddress"];

	
}


$pos_data = get_poslocation($pos_id, "posaddresses");
$has_project = update_posdata_from_posorders($pos_id);

$opening_date = $pos_data["posaddress_store_openingdate"];
$closing_date = $pos_data["posaddress_store_closingdate"];


$opening_year = substr($opening_date, 0, 4);
$closing_year = substr($closing_date, 0, 4);
$actual_year = date("Y");

//check if sellout records exist else create
$bp_starting_date = $cer_basicdata["cer_basicdata_firstyear"];

if($bp_starting_date > date("Y"))
{
	$bp_starting_date = date("Y");
}

if($opening_year + 3 > $bp_starting_date )
{
	$starting_year = $opening_year;
}
else
{
	$starting_year = $bp_starting_date - 3;

}

if($closing_year > 0)
{
	$ending_year = $closing_year;
}
else
{
	$ending_year = $actual_year;
}


if($ending_year > $bp_starting_date)
{
	$ending_year = $bp_starting_date;
}

if(($project["project_cost_type"] == 1 and $ln_basicdata["ln_basicdata_locked"] == 0)
 or ($project["project_cost_type"] == 1 and $ln_basicdata["ln_basicdata_locked"] == 1 and $cer_basicdata["cer_basicdata_cer_locked"] == 0)
 or ($project["project_cost_type"] != 1 and $cer_basicdata["cer_basicdata_cer_locked"] == 0))
{
	for($year=$starting_year;$year<=$ending_year;$year++)
	{
		$sql = "select count(cer_sellout_id) as num_recs " . 
			   "from cer_sellouts " . 
			   "where cer_sellout_version = 0 " . 
			   " and cer_sellout_project_id = " . dbquote(param("pid")) . 
			   " and cer_sellout_year = " . dbquote($year);

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		
		if($row["num_recs"] == 0)
		{
			$sql_i = "INSERT INTO cer_sellouts (" . 
					 "cer_sellout_project_id, cer_sellout_version, cer_sellout_year, date_created, user_created" . 
					 ") VALUES (" . 
					 dbquote(param("pid")) . ", " .
					 "0, " . 
					 $year . ", " . 
					 dbquote(date("Y-m-d H:i:s")) . ", " . 
					 dbquote(user_login()) . 
					 ")";
			
			$result = mysql_query($sql_i) or dberror($sql_i);

		}
	}
}

$watches = array();
$bijoux = array();

$watches_grossales = array();
$bijoux_grossales = array();
$grossmargins = array();
$netsales = array();
$expenses = array();
$operating_incomes1 = array();
$whsm = array();
$operating_incomes2 = array();
$months = array();
$delete_record = array();

$sql = "select * from cer_sellouts where cer_sellout_version = 0 and cer_sellout_project_id = " . dbquote(param("pid"));
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$watches[$row["cer_sellout_id"]] = $row["cer_sellout_watches_units"];
	$bijoux[$row["cer_sellout_id"]] = $row["cer_sellout_bijoux_units"];
	
	if($row["cer_sellout_month"] > 0)
	{
		$months[$row["cer_sellout_id"]] = $row["cer_sellout_month"];
	}
	elseif($row["cer_sellout_year"] < date("Y"))
	{
		$months[$row["cer_sellout_id"]] = 12;
	}
	else
	{
		$months[$row["cer_sellout_id"]] = (int)date("m");
	}

	$watches_grossales[$row["cer_sellout_id"]] = $row["cer_sellout_watches_grossales"];
	$bijoux_grossales[$row["cer_sellout_id"]] = $row["cer_sellout_bijoux_grossales"];
	$netsales[$row["cer_sellout_id"]] = $row["cer_sellout_net_sales"];
	$grossmargins[$row["cer_sellout_id"]] = $row["cer_sellout_grossmargin"];
	$expenses[$row["cer_sellout_id"]] = $row["cer_sellout_operating_expenses"];
	$operating_incomes1[$row["cer_sellout_id"]] = $row["cer_sellout_operating_income_excl"];
	$whsm[$row["cer_sellout_id"]] = $row["cer_sellout_wholsale_margin"];
	$operating_incomes2[$row["cer_sellout_id"]] = $row["cer_sellout_operating_income_incl"];



	
	$link = "sellouts.php?pid=" . param("pid") . "&did=" . $row["cer_sellout_id"] . "&context=" . param("context");

	$delete_record[$row["cer_sellout_id"]] = '<a href="' . $link . '"><img src="/pictures/closed.gif" border="0"/></a>';
}


    

/********************************************************************
    Create Form
*********************************************************************/

$form = new Form("posaddresses", "posaddress");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("pos_id", $pos_id);
$form->add_hidden("context", param("context"));


include("include/project_head.php");




$form->populate();
$form->process();

for($year=$starting_year;$year<=$ending_year;$year++)
{
	
	if($form->button("y" . $year))
	{

		$sql_i = "INSERT INTO cer_sellouts (" . 
				 "cer_sellout_project_id, cer_sellout_version, cer_sellout_year, date_created, user_created" . 
				 ") VALUES (" . 
				 dbquote(param("pid")) . ", " .
			     "0, " . 
				 $year . ", " . 
				 dbquote(date("Y-m-d H:i:s")) . ", " . 
				 dbquote(user_login()) . 
				 ")";
		
		$result = mysql_query($sql_i) or dberror($sql_i);
		redirect("sellouts.php?pid=" . param("pid") . "&context=" . param("context"));
	}

}


/********************************************************************
    Create List
*********************************************************************/
$list = new ListView("select * from cer_sellouts");

$list->set_title("Sellouts in Units");
$list->set_entity("cer_sellouts");
$list->set_filter("cer_sellout_version = 0 and cer_sellout_project_id = " . param("pid"));
$list->set_order("cer_sellout_year DESC, cer_sellout_month DESC");

if(has_access("has_access_to_his_cer") or has_access("has_full_access_to_cer"))
{
	$list->add_column("cer_sellout_year", "Year", "", 0 ,'', COLUMN_NO_WRAP);
	$list->add_number_edit_column("cer_sellout_month", "Months\nSelling", "2", COLUMN_BREAK, $months);

	$list->add_number_edit_column("cer_sellout_watches_units", "Watches", "8", COLUMN_BREAK, $watches);
	$list->add_number_edit_column("cer_sellout_bijoux_units", "Watch\nStraps", "8", COLUMN_BREAK, $bijoux);

	$list->add_number_edit_column("cer_sellout_watches_grossales", "Gross Sales\nWatches", "8", COLUMN_BREAK, $watches_grossales);
	$list->add_number_edit_column("cer_sellout_bijoux_grossales", "Gross Sales\nWatch Straps", "10", COLUMN_BREAK, $bijoux_grossales);
	$list->add_number_edit_column("cer_sellout_net_sales", "Net Sales", "10", COLUMN_BREAK, $netsales);
	$list->add_number_edit_column("cer_sellout_grossmargin", "Gross Margin", "10", COLUMN_BREAK, $grossmargins);
	$list->add_number_edit_column("cer_sellout_operating_expenses", "Expenses", "10", COLUMN_BREAK, $expenses);
	$list->add_number_edit_column("cer_sellout_operating_income_excl", "Operating\nIncome\nexcl. WSM", "10", COLUMN_BREAK, $operating_incomes1);
	$list->add_number_edit_column("cer_sellout_wholsale_margin", "WSM in %", "10", 0, $whsm);
	$list->add_number_edit_column("cer_sellout_operating_income_incl", "Operating\nIncome\nincl. WSM", "10", COLUMN_BREAK, $operating_incomes2);

	//$list->add_text_column("delete", "", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML , $delete_record);
	$list->add_button("save", "Save");
}
else
{
	$list->add_column("cer_sellout_year", "Year", "", 0 ,'', COLUMN_NO_WRAP);
	$list->add_column("cer_sellout_month", "Num of Months", "", 0,'', COLUMN_NO_WRAP);
	$list->add_column("cer_sellout_watches_units", "Watches", "",  "", "",COLUMN_ALIGN_RIGHT);
	$list->add_column("cer_sellout_bijoux_units", "Watch Straps", "",  "", "",COLUMN_ALIGN_RIGHT);

	$list->add_column("cer_sellout_watches_grossales", "Gross Sales\nWatches", "", "",  "",COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
	$list->add_column("cer_sellout_bijoux_grossales", "Gross Sales\nWatch Straps", "", "",  "",COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
	$list->add_column("cer_sellout_net_sales", "Net Sales", "", "",  "",COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
	$list->add_column("cer_sellout_grossmargin", "Gross Margin", "", "",  "",COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
	$list->add_column("cer_sellout_operating_expenses", "Expenses", "", "",  "",COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
	$list->add_column("cer_sellout_operating_income_excl", "Operating\nIncome\nexcl. WSM", "", "",  "",COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
	$list->add_column("cer_sellout_wholsale_margin", "WSM in %", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
	$list->add_column("cer_sellout_operating_income_incl", "Operating\nIncome\nincl. WSM", "", "",  "",COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
}



$list->populate();
$list->process();



if($list->button("save"))
{
	$error = "";
	$error_text = "";
	foreach($list->columns as $key=>$column)
	{
	
		if($column["name"] == "cer_sellout_month")
		{
			foreach($column["values"] as $cer_sellout_id=>$month)
			{
				if($month) 
				{
					if($month >= 1 and $month <= 12)
					{
					
						$fields = array();
						$fields[] = "cer_sellout_month = " . dbquote($month);
						$sql = "update cer_sellouts set " . join(", ", $fields) . " where cer_sellout_id = " . $cer_sellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The number of months mus be a number from 1 to 12!";
					}

				}

				$fields = array();
				$fields[] = "date_modified = " . dbquote(date("Y-m-d H:i:s"));
				$fields[] = "user_modified = " . dbquote(user_login());
				$sql = "update cer_sellouts set " . join(", ", $fields) . " where cer_sellout_id = " . $cer_sellout_id;
				mysql_query($sql) or dberror($sql);
			}
		}
		elseif($column["name"] == "cer_sellout_watches_units")
		{
			foreach($column["values"] as $cer_sellout_id=>$value)
			{
				if($value) 
				{
					if($value >= 1 and $value <= 9999999999)
					{
						$fields = array();
						$fields[] = "cer_sellout_watches_units = " . dbquote($value);
						$sql = "update cer_sellouts set " . join(", ", $fields) . " where cer_sellout_id = " . $cer_sellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The amount of watches must be a number!";
					}
				}
				else
				{
					$error_text = "The amount of watches must be indicated!";
				}
			}
		}
		elseif($column["name"] == "cer_sellout_bijoux_units")
		{
			foreach($column["values"] as $cer_sellout_id=>$value)
			{
				if($value) 
				{
					if($value >= 1 and $value <= 9999999999)
					{
						$fields = array();
						$fields[] = "cer_sellout_bijoux_units = " . dbquote($value);
						$sql = "update cer_sellouts set " . join(", ", $fields) . " where cer_sellout_id = " . $cer_sellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The amount of watch straps must be a number!";
					}

				}
			}
		}
		elseif($column["name"] == "cer_sellout_watches_grossales")
		{
			foreach($column["values"] as $cer_sellout_id=>$value)
			{
				if($value) 
				{
					if($value >= 1 and $value <= 9999999999)
					{
						$fields = array();
						$fields[] = "cer_sellout_watches_grossales = " . dbquote($value);
						$sql = "update cer_sellouts set " . join(", ", $fields) . " where cer_sellout_id = " . $cer_sellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The gross sales watches must be a number!";
					}
				}
				else
				{
					$error_text = "The gross sales watches must be indicated!";
				}
			}
		}
		elseif($column["name"] == "cer_sellout_bijoux_grossales")
		{
			foreach($column["values"] as $cer_sellout_id=>$value)
			{
				if($value) 
				{
					if($value >= 1 and $value <= 9999999999)
					{
						$fields = array();
						$fields[] = "cer_sellout_bijoux_grossales = " . dbquote($value);
						$sql = "update cer_sellouts set " . join(", ", $fields) . " where cer_sellout_id = " . $cer_sellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The gross sales watch straps must be a number!";
					}

				}
			}
		}
		elseif($column["name"] == "cer_sellout_net_sales")
		{
			foreach($column["values"] as $cer_sellout_id=>$value)
			{
				if($value) 
				{
					if($value >= 1 and $value <= 9999999999)
					{
						$fields = array();
						$fields[] = "cer_sellout_net_sales = " . dbquote($value);
						$sql = "update cer_sellouts set " . join(", ", $fields) . " where cer_sellout_id = " . $cer_sellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The net sales must be a number!";
					}

				}
				else
				{
					$error_text = "The net sales must be indicated!";
				}
			}
		}
		elseif($column["name"] == "cer_sellout_grossmargin")
		{
			foreach($column["values"] as $cer_sellout_id=>$value)
			{
				if($value) 
				{
					if($value >= 1 and $value <= 9999999999)
					{
						$fields = array();
						$fields[] = "cer_sellout_grossmargin = " . dbquote($value);
						$sql = "update cer_sellouts set " . join(", ", $fields) . " where cer_sellout_id = " . $cer_sellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The gross margin must be a number!";
					}

				}
				else
				{
					$error_text = "The gross margin must be indicated!";
				}
			}
		}
		elseif($column["name"] == "cer_sellout_operating_expenses")
		{
			foreach($column["values"] as $cer_sellout_id=>$value)
			{
				if($value) 
				{
					if($value >= 1 and $value <= 9999999999)
					{
						$fields = array();
						$fields[] = "cer_sellout_operating_expenses = " . dbquote($value);
						$sql = "update cer_sellouts set " . join(", ", $fields) . " where cer_sellout_id = " . $cer_sellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The expenses must be a number!";
					}

				}
				else
				{
					$error_text = "The expenses must be indicated!";
				}
			}
		}
		elseif($column["name"] == "cer_sellout_operating_income_excl")
		{
			foreach($column["values"] as $cer_sellout_id=>$value)
			{
				if($value) 
				{
					if($value >= 1 and $value <= 9999999999)
					{
						$fields = array();
						$fields[] = "cer_sellout_operating_income_excl = " . dbquote($value);
						$sql = "update cer_sellouts set " . join(", ", $fields) . " where cer_sellout_id = " . $cer_sellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The operating income must be a number!";
					}

				}
				else
				{
					$error_text = "The operating incomes be indicated!";
				}
			}
		}
		elseif($column["name"] == "cer_sellout_operating_income_incl")
		{
			foreach($column["values"] as $cer_sellout_id=>$value)
			{
				if($value) 
				{
					if($value >= 1 and $value <= 9999999999)
					{
						$fields = array();
						$fields[] = "cer_sellout_operating_income_incl = " . dbquote($value);
						$sql = "update cer_sellouts set " . join(", ", $fields) . " where cer_sellout_id = " . $cer_sellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The operating income must be a number!";
					}

				}
				else
				{
					$error_text = "The operating incomes be indicated!";
				}
			}
		}
		elseif($column["name"] == "cer_sellout_wholsale_margin")
		{
			foreach($column["values"] as $cer_sellout_id=>$value)
			{
				if($value) 
				{
					if($value >= 1 and $value <= 99)
					{
						$fields = array();
						$fields[] = "cer_sellout_wholsale_margin = " . dbquote($value);
						$sql = "update cer_sellouts set " . join(", ", $fields) . " where cer_sellout_id = " . $cer_sellout_id;
						mysql_query($sql) or dberror($sql);
					}
					else
					{
						$error_text = "The whole sale margin must be a number!";
					}

				}
				else
				{
					$error_text = "The whole sale margin must be indicated!";
				}
			}
		}

	}

	if($error_text)
	{
		$form->error($error_text);
	}
	else
	{
		//$form->message("Your sellout data was saved.");
	}

}



$page = new Page("cer_projects");
require "include/project_page_actions.php";
$page->header();

if(param("context") and param("context") == 'ln')
{
	$page->title("LNR: Sellouts");
	require_once("include/tabs_ln2016.php");
}
else
{
	if($form_type == "INR03")
	{
		$page->title("INR-03 - Retail Furniture in Third-party Store: Sellouts");
	}
	elseif($form_type == "AF")
	{
		$page->title("LNR/INR03: Sellouts");
	}
	else
	{
		$page->title("LNR/CER: Sellouts");
	}
	require_once("include/tabs2016.php");
}


$form->render();
$list->render();


require "include/footer_scripts.php";
$page->footer();

?>
