<?php
/********************************************************************

    ln_booklet_pdf.php

    Print Lease Negotiation Booklet

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-11-06
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require "include/financial_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/

$PDFmerger_was_used = false;

if($ln_basicdata["ln_basicdata_factor"] == 0) {$ln_basicdata["ln_basicdata_factor"] = 1;}
$currency_symbol = get_currencys_symbol($ln_basicdata["ln_basicdata_currency"]);

// get all data needed from project
$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
	   "left join currencies on currency_id = country_currency " . 
       "left join postypes on postype_id = project_postype ".
	   "left join possubclasses on possubclass_id = project_pos_subclass ".
	   "left join ln_basicdata on ln_basicdata_project = project_id ".
	   "left join floors on floor_id = project_floor " . 
	   "where project_id = " . param("pid") . 
	   " and ln_basicdata_version = " . $ln_version;


$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$order_number = $row["project_order"];
	//POS Basic Data
	$posname = $row["order_shop_address_company"];
	$posname2 = $row["order_shop_address_company"];

	if($row["order_shop_address_company2"])
    {
		$posname .= ", " . $row["order_shop_address_company2"];
		$posname2 .= ", " . $row["order_shop_address_company2"];
	}

	
	$posaddress = $row["country_name"];
	$country_name = $row["country_name"];

	if($row["order_shop_address_zip"])
    {
		$posaddress .= ", " . $row["order_shop_address_zip"] . " " . $row["order_shop_address_place"];
	}
	else
	{
		$posaddress .= ", " . $row["order_shop_address_place"];
	}

	$city_name = $row["order_shop_address_place"];

	$posaddress .= ", " . $row["order_shop_address_address"];
	
	if($row["order_shop_address_address2"])
    {
		$posaddress .= ", " . $row["order_shop_address_address2"];
	}
	
	$planned_takeover_date = to_system_date($row["project_planned_takeover_date"]);
	$planned_opening_date = to_system_date($row["project_real_opening_date"]);
	//$planned_opening_date = to_system_date($row["project_planned_opening_date"]);
	$sales_surface = $row["project_cost_sqms"];
	$total_surface = $row["project_cost_totalsqms"];
	//$gross_surface = $row["project_cost_grosssqms"];
	$gross_surface = $row["project_cost_totalsqms"];

	if($ln_version > 0 and $ln_basicdata["ln_basicdata_version_sqms"] > 0)
	{
		$sales_surface = $ln_basicdata["ln_basicdata_version_sqms"];
		$total_surface = $ln_basicdata["ln_basicdata_version_totalsqms"];
		//$gross_surface = $ln_basicdata["ln_basicdata_version_grosssqms"];
		$gross_surface = $ln_basicdata["ln_basicdata_version_totalsqms"];
	}
		

	
	$page_title = "LNR Booklet: " . $row["project_number"];
	$page_title = "LNR ";
	$version_date = ""; 
	if($ln_version > 0)
	{
		$version_date = " Older Version " . to_system_date(substr($ln_basicdata["versiondate"], 0,10)) . " " . substr($ln_basicdata["versiondate"], 10,9);
	}

	
	if($row["productline_subclass_name"])
	{
		$postype = $row["projectkind_name"] . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"] . " / " . $row["productline_subclass_name"];
	}
	else {
		$postype = $row["projectkind_name"] . " - " . $row["project_costtype_text"] . " " . $row["postype_name"] . " - " . $row["product_line_name"];
	}

	$postype = $row["project_costtype_text"] . " " . $row["postype_name"] . " " . $row["possubclass_name"];

	$project_kind = $row["projectkind_name"];

	$surface_of_relocated_pos = "";
	if($project["project_projectkind"] == 6 and $project["project_relocated_posaddress_id"] > 0) // relocation
	{
		$relocated_pos = get_relocated_pos_info($project["project_relocated_posaddress_id"]);
		$project_kind .= " of " . $relocated_pos["posaddress_name"];

		//$surface_of_relocated_pos = $relocated_pos["posaddress_store_grosssurface"] . " / " . $relocated_pos["posaddress_store_totalsurface"] . " / " . $relocated_pos["posaddress_store_retailarea"];
		$surface_of_relocated_pos =  $relocated_pos["posaddress_store_totalsurface"] . " / " . $relocated_pos["posaddress_store_retailarea"];
		
        
	
	}
	
	$placement = $row["floor_name"];

	
	$deadline_for_property = to_system_date($cer_basicdata["cer_basicdata_deadline_property"]);


	//get keymoney and deposit
	$tmp = get_pos_intangibles(param("pid"), 15, $cer_version);
	if(count($tmp) > 0)
	{
		$keymoney = $tmp ["cer_investment_amount_cer_loc"];
		$keymoney = $keymoney*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
		$keymoney_loc = round($keymoney/1000, 0);
		

		$tmp = get_pos_intangibles(param("pid"), 9, $cer_version);
		$depositposted = $tmp ["cer_investment_amount_cer_loc"];
		$depositposted = $depositposted*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
		$depositposted = round($depositposted/1000, 0);
	}
	else
	{
		$keymoney_loc = "";
		$depositposted = "";
	}

	$tmp = get_pos_intangibles(param("pid"), 17);
	if(count($tmp) > 0)
	{
		$goodwill_loc = $tmp ["cer_investment_amount_cer_loc"];
		$goodwill_loc = $goodwill_loc*$cer_basicdata["cer_basicdata_exchangerate"]/$cer_basicdata["cer_basicdata_factor"];
		$goodwill_loc = round($goodwill_loc/1000, 0);
	}
	else
	{
		$goodwill_loc = "";
	}



	//get rents
	$first_full_year = $cer_basicdata["cer_basicdata_firstyear"];
	if($cer_basicdata["cer_basicdata_firstmonth"] > 1 and $cer_basicdata["cer_basicdata_lastyear"] > ($first_full_year+1))
	{
		$first_full_year = $cer_basicdata["cer_basicdata_firstyear"]+1;
	}


	$last_full_year = $cer_basicdata["cer_basicdata_lastyear"];
	if($cer_basicdata["cer_basicdata_lastmonth"] < 12)
	{
		$last_full_year = $cer_basicdata["cer_basicdata_lastyear"]-1;
	}

	$fullrent_firstyear = 0;
	$fixedrent_firstyear = 0;
	$turnoverrent_firstyear = 0;
	$additional_rental_cost_firstyear = 0;
	$fullrent_firstyear_loc = 0;
	

	if($first_full_year > 0 and $cer_basicdata["cer_basicdata_firstyear"] < $cer_basicdata["cer_basicdata_lastyear"])
	{
		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_project = " . param("pid") .
			   " and cer_expense_cer_version =  " . $cer_version . 
			   " and cer_expense_type = 2 " . 
			   " and cer_expense_year = " . $first_full_year;

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$fixedrent_firstyear = $row_e["cer_expense_amount"];
		}

		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_project = " . param("pid") .
			   " and cer_expense_cer_version =  " . $cer_version . 
			   " and cer_expense_type = 16 " . 
			   " and cer_expense_year = " . $first_full_year;

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$turnoverrent_firstyear = $row_e["cer_expense_amount"];
		}

		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_project = " . param("pid") . 
			   " and cer_expense_cer_version =  " . $cer_version .  
			   " and cer_expense_type IN (3, 18, 19, 20, 22) " . 
			   " and cer_expense_year = " . $first_full_year;

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		while ($row_e = mysql_fetch_assoc($res_e))
		{
			$additional_rental_cost_firstyear = $additional_rental_cost_firstyear + $row_e["cer_expense_amount"];
		}
	}
	elseif($cer_basicdata["cer_basicdata_firstyear"] == $cer_basicdata["cer_basicdata_lastyear"])
	{
		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_project = " . param("pid") . 
			   " and cer_expense_cer_version =  " . $cer_version . 
			   " and cer_expense_type = 2 " . 
			   " and cer_expense_year = " . $cer_basicdata["cer_basicdata_firstyear"];

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$fixedrent_firstyear = $row_e["cer_expense_amount"];
		}

		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_project = " . param("pid") . 
			   " and cer_expense_cer_version =  " . $cer_version . 
			   " and cer_expense_type = 16 " . 
			   " and cer_expense_year = " . $cer_basicdata["cer_basicdata_firstyear"];


		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$turnoverrent_firstyear = $row_e["cer_expense_amount"];
		}

		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_project = " . param("pid") . 
			   " and cer_expense_cer_version =  " . $cer_version .  
			   " and cer_expense_type IN (3, 18, 19, 20, 22) " . 
			   " and cer_expense_year = " . $cer_basicdata["cer_basicdata_firstyear"];


		$res_e = mysql_query($sql_e) or dberror($sql_e);
		while ($row_e = mysql_fetch_assoc($res_e))
		{
			$additional_rental_cost_firstyear = $row_e["cer_expense_amount"];
		}
	}

	$fullrent_firstyear_loc = $fixedrent_firstyear + $turnoverrent_firstyear + $additional_rental_cost_firstyear;

	$fullrent_firstyear = $fullrent_firstyear_loc*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
	$fullrent_firstyear = round($fullrent_firstyear/1000, 0);
	$fullrent_firstyear_loc = round($fullrent_firstyear_loc/1000, 0);

	$turnoverrent_firstyear = $turnoverrent_firstyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
	$turnoverrent_firstyear = round($turnoverrent_firstyear/1000, 0);



	/*
	$fullrent_lastyear = 0;
	$fixedrent_lastyear = 0;
	$turnoverrent_lastyear = 0;

	if($last_full_year > 0)
	{
		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_project = " . param("pid") .
			   " and cer_expense_cer_version  =  " . $cer_version .
			   " and cer_expense_type = 2 " . 
			   " and cer_expense_year = " . $last_full_year;

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$fixedrent_lastyear = $row_e["cer_expense_amount"];
		}

		$sql_e = "select cer_expense_amount from cer_expenses " . 
			   "where cer_expense_project = " . param("pid") .
			   " and cer_expense_cer_version  =  " . $cer_version .
			   " and cer_expense_type = 16 " . 
			   " and cer_expense_year = " . $last_full_year;

		$res_e = mysql_query($sql_e) or dberror($sql_e);
		if ($row_e = mysql_fetch_assoc($res_e))
		{
			$turnoverrent_lastyear = $row_e["cer_expense_amount"];
		}
	}
	
	$fullrent_lastyear = $fixedrent_lastyear + $turnoverrent_lastyear;
	*/
	
	$fullrent_lastyear_loc = $ln_basicdata["ln_basicdata_passedrental"];
	

	$fullrent_lastyear = $fullrent_lastyear_loc*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
	$fullrent_lastyear = round($fullrent_lastyear/1000, 0);
	$fullrent_lastyear_loc = round($fullrent_lastyear_loc/1000, 0);

	//$turnoverrent_lastyear = $turnoverrent_lastyear*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
	//$turnoverrent_lastyear = round($turnoverrent_lastyear/1000, 0);


	
	//Additional Rental Costs
	$poslease = array();
	if($project["pipeline"] == 1)
	{
		$sql_i = "select * from posleasespipeline " . 
				 "where poslease_order = " . $project["project_order"] . 
			     " order by poslease_lease_type DESC, poslease_id DESC";
		
		
	}
	elseif($project["pipeline"] == 0)
	{
		$sql_i = "select * from posleases " . 
				 "where poslease_order = " . $project["project_order"] . 
			     " order by poslease_lease_type DESC, poslease_id DESC";
	}
	$res_i = mysql_query($sql_i) or dberror($sql_i);
	if ($row_i = mysql_fetch_assoc($res_i))
	{
		$poslease = $row_i;
	}

	if(count($poslease) > 0)
	{

		$negotiated_rental_conditions = $poslease["poslease_negotiated_conditions"];
		$indexrate = $poslease["poslease_indexrate"] . "%";
		$average_yearly_increase = $poslease["poslease_average_increase"] . "%";
		$real_estate_fee = $poslease["poslease_realestate_fee"] . "%";

		$annual_charges = $poslease["poslease_annual_charges"];
		$annual_charges = $annual_charges*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
		


		$other_fees = $poslease["poslease_other_fees"];
		$other_fees = $other_fees*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
		

		$free_weeks = $poslease["poslease_freeweeks"];


		$average_annual_rent = 0;
		$duration_in_years = 0;
		if($poslease["poslease_startdate"] != NULL 
			and $poslease["poslease_startdate"] != '0000-00-00' 
			and $poslease["poslease_enddate"] != NULL
			and $poslease["poslease_enddate"] != '0000-00-00')
			{
				$diff = abs(strtotime($poslease["poslease_enddate"]) - strtotime($poslease["poslease_startdate"]));
				$years = floor($diff / (365*60*60*24));
				$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

				if($months == 12)
				{
					$months = 0;
					$years++;
				}

				$rental_duration = $years . " years and " . $months . " months";

				$duration_in_years = $years + ($months/12);

				$contract_starting_date = to_system_date($poslease["poslease_startdate"]);
				$contract_ending_date = to_system_date($poslease["poslease_enddate"]);
		}
			
		if($duration_in_years > 0)
		{
			$total_lease_commitment = 0;
			$sql_cer = "select * from cer_expenses " .
						  "where cer_expense_project = " . param("pid") .
				          " and cer_expense_cer_version  =  " . $cer_version .
						  " and cer_expense_type IN(2, 3, 16, 18, 19, 20)";


			$res_cer = mysql_query($sql_cer) or dberror($sql_cer);
			while ($row_cer = mysql_fetch_assoc($res_cer))
			{
				$total_lease_commitment = $total_lease_commitment + 1*$row_cer["cer_expense_amount"];
			}

			if($duration_in_years > 0)
			{
				$average_annual_rent = round(($total_lease_commitment / $duration_in_years)/1000, 0);
			}
		}
	}
	else
	{
		$negotiated_rental_conditions = "";
		$indexrate = "";
		$indexrate = "";
		$average_yearly_increase = "";
		$real_estate_fee = "";
		$annual_charges = "";
		$other_fees = "";
		$average_annual_rent = "";
		$duration_in_years = "";
		$total_lease_commitment = "";
		$contract_starting_date = "";
		$contract_ending_date = "";
		$free_weeks = "";
	}
	

	//annual charges and other fees -> new concept for projects starting on April 22, 2014
	if(!$annual_charges)
	{
		//additional rental cost and other fees
		//get additional rental costs
		$annual_charges = 0;

		//first full year
		$sql_e = "select * from cer_expenses " .
				 "where cer_expense_cer_version = " . $cer_version . 
			     "  and cer_expense_project = " . param("pid") . 
				 " and cer_expense_type in (3) " . 
			     " and cer_expense_year = " . $first_full_year;
				 " order by cer_expense_year";

		$res_e = mysql_query($sql_e) or dberror($sql_e);

		while($row_e = mysql_fetch_assoc($res_e))
		{
			$annual_charges = $annual_charges + $row_e["cer_expense_amount"];
		}

		$annual_charges = $annual_charges*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];

	}

	if(!$other_fees)
	{
		//additional rental cost and other fees
		//get other fees
		$other_fees = 0;
		//first full year
		$sql_e = "select * from cer_expenses " .
				 "where cer_expense_cer_version = " . $cer_version . 
			     "  and cer_expense_project = " . param("pid") . 
				 " and cer_expense_type in (18,19) " . 
			     " and cer_expense_year = " . $first_full_year;
				 " order by cer_expense_year";

		$res_e = mysql_query($sql_e) or dberror($sql_e);

		while($row_e = mysql_fetch_assoc($res_e))
		{
			$other_fees = $other_fees + $row_e["cer_expense_amount"];
		}
		$other_fees = $other_fees*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];

	}
	$annual_charges = round($annual_charges/1000, 0);
	$other_fees = round($other_fees/1000, 0);
	
	//get areas and neighbourhood information
	$posareas = "";
	if($project["pipeline"] == 0)
	{
		$sql_i = "select * from posareas " . 
				 "left join posareatypes on posareatype_id = posarea_area " .
				 "where posarea_posaddress = " . $project["posaddress_id"];
		
	}
	elseif($project["pipeline"] == 1)
	{
		
		$sql_p = "select posorder_posaddress from posorderspipeline where posorder_order = " . $project["order_id"];
		$res_p = mysql_query($sql_p) or dberror($sql_p);

		if ($row_p = mysql_fetch_assoc($res_p))
		{
			$sql_i = "select * from posareaspipeline " . 
					 "left join posareatypes on posareatype_id = posarea_area " .
					 "where posarea_posaddress = " . $row_p["posorder_posaddress"];
		}
	}
	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{
		$posareas .= $row_i["posareatype_name"] . ", ";
	}
	$posareas = substr($posareas,0,strlen($posareas)-2);


	//neighbourhood
	$neighbourhoods = array();

	if($project["pipeline"] == 0)
	{
		$sql_e = "select * from posorders where posorder_order = " . $project["order_id"];
	}
	elseif($project["pipeline"] == 1)
	{
		$sql_e = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
	}
	
	$res_e = mysql_query($sql_e) or dberror($sql_e);
	if ($row_e = mysql_fetch_assoc($res_e))
	{
		$tmp = "";
		$bt = get_business_type($row_e["posorder_neighbour_left_business_type"]);
		$pr = get_price_range($row_e["posorder_neighbour_left_price_range"]);
		if($bt or $pr)
		{
			$tmp = " (". $bt . " " . $pr . ")";
		}
		$neighbourhoods["Shop on left side"] = $row_e["posorder_neighbour_left"] . $tmp;

		$tmp = "";
		$bt = get_business_type($row_e["posorder_neighbour_right_business_type"]);
		$pr = get_price_range($row_e["posorder_neighbour_right_price_range"]);
		if($bt or $pr)
		{
			$tmp = " (". $bt . " " . $pr . ")";
		}
		$neighbourhoods["Shop on right side"] = $row_e["posorder_neighbour_right"] . $tmp;

		$tmp = "";
		$bt = get_business_type($row_e["posorder_neighbour_acrleft_business_type"]);
		$pr = get_price_range($row_e["posorder_neighbour_acrleft_price_range"]);
		if($bt or $pr)
		{
			$tmp = " (". $bt . " " . $pr . ")";
		}
		$neighbourhoods["Shop across left side"] = $row_e["posorder_neighbour_acrleft"] . $tmp;

		$tmp = "";
		$bt = get_business_type($row_e["posorder_neighbour_acrright_business_type"]);
		$pr = get_price_range($row_e["posorder_neighbour_acrright_price_range"]);
		if($bt or $pr)
		{
			$tmp = " (". $bt . " " . $pr . ")";
		}
		$neighbourhoods["Shop across right side"] = $row_e["posorder_neighbour_acrright"] . $tmp;
		
		$neighbourhoods["Other brands in area"] = $row_e["posorder_neighbour_brands"];
	}



	$ln_remarks = $row["ln_basicdata_remarks"];
	$ln_brands = $row["ln_basicdata_brands"];
	$ln_size_remarks = $row["ln_basicdata_size_remarks"];
	$ln_area = $row["ln_basicdata_area"];
	$ln_rent = $row["ln_basicdata_rent"];
	$ln_availability = $row["ln_basicdata_availability"];


	$approved = "";
		
	$pix1 = ".." . $row["ln_basicdata_pix1"];
	$pix2 = ".." . $row["ln_basicdata_pix2"];
	$pix3 = ".." . $row["ln_basicdata_pix3"];

	$floor_plan = ".." . $row["ln_basicdata_floorplan"];
	$location_layout = ".." . $row["ln_basicdata_location_layout"];
	$lease_agreement = ".." . $row["ln_basicdata_draft_aggreement"];

	$relocation_doc = ".." . $row["ln_basicdata_relocation_doc"];


	//get names of roles
	$brand_manager = "";
	$ceo = "";
	$cfo = "";
	$vp_sales = "";
	$president = "";
	$head_controlling = "";
	$country_manager = "";


	$sql_u = "select cer_summary_in01_sig01, cer_summary_in01_sig02, cer_summary_in01_sig03 " . 
		     "from cer_summary " . 
		     "where cer_summary_project = " . param("pid") . 
		     " and cer_summary_cer_version = " . $cer_version;
	
	$res_u = mysql_query($sql_u) or dberror($sql_u);
	if($row_u = mysql_fetch_assoc($res_u))
	{
		$brand_manager = $row_u["cer_summary_in01_sig03"];
		$country_manager = $row_u["cer_summary_in01_sig01"];
		$head_controlling = $row_u["cer_summary_in01_sig02"];
	}

		
	$approval_name1 = $cer_basicdata["cer_basicdata_approvalname1"];
	$president = $cer_basicdata["cer_basicdata_approvalname2"];
	$ceo = $cer_basicdata["cer_basicdata_approvalname2"];
	$cfo = $cer_basicdata["cer_basicdata_approvalname7"];
	$vp_sales = $cer_basicdata["cer_basicdata_approvalname3"];
	$retail_head = $cer_basicdata["cer_basicdata_approvalname4"];

	
	

	$exchange_rate_factor = $ln_basicdata['ln_basicdata_factor'];
	if(!$ln_basicdata['ln_basicdata_factor'])
	{
		$exchange_rate_factor = 1;
	}

	$exchange_rate = 1;
	if($ln_basicdata['ln_basicdata_exchangerate'] > 0)
	{
		$exchange_rate = $ln_basicdata['ln_basicdata_exchangerate'];
	}


	//LN supporting documents
	$ln_supporting_documents = array();
	$sql_d = "select order_file_path " . 
		     " from order_files " . 
		     " where order_file_order = " . dbquote($order_number) . 
		     " and order_file_category = 17 " . 
		     " and order_file_type IN (2,13)";

	$res_d = mysql_query($sql_d) or dberror($sql_d);
	while ($row_d = mysql_fetch_assoc($res_d))
	{
		$ln_supporting_documents[] = $_SERVER["DOCUMENT_ROOT"] . $row_d["order_file_path"];
	}


	$relocation_costs = $ln_basicdata['ln_basicdata_relocation_costs'];
	$relocation_costs = $relocation_costs*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];

	$relocation_info = "Estimated closing costs in CHF: " . number_format( $relocation_costs,2, ".", "'");
	$relocation_info .= "\n" . $ln_basicdata['ln_basicdata_relocation_comment'];



	//Human resources
	$human_resources = array();
	if(param("hr") == 1)
	{
	
		$sql_d = "select * from cer_salaries " .
			     " left join cer_staff_types on cer_staff_type_id = cer_salary_staff_type " . 
			     " where cer_salary_cer_version = " . param("cerversion") . 
			     " and cer_salary_project = " . param("pid");


		$res_d = mysql_query($sql_d) or dberror($sql_d);
		while ($row_d = mysql_fetch_assoc($res_d))
		{
			$human_resources[] = $row_d;
		}

		foreach($human_resources as $key=>$human_resource)
		{
			$human_resources[$key]["cer_salary_fixed_salary"] = round($human_resource["cer_salary_fixed_salary"]*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"], 0);
			$human_resources[$key]["cer_salary_bonus"] = round($human_resource["cer_salary_bonus"]*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"], 0);
			$human_resources[$key]["cer_salary_other"] = round($human_resource["cer_salary_other"]*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"], 0);
			$human_resources[$key]["cer_salary_social_charges"] = round($human_resource["cer_salary_social_charges"]*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"], 0);
		}
	}

    
	require_once "include/in_financial_data.php";	

	/********************************************************************
		prepare pdf
	*********************************************************************/

	require_once('../include/tcpdf/config/lang/eng.php');
	require_once('../include/tcpdf/tcpdf.php');
	require_once('../include/SetaPDF/Autoload.php');

	
	//Instanciation of inherited class

	// Create and setup PDF document
	class MYPDF extends TCPDF
	{
		
		//Page footer
		function Footer()
		{
			//Position at 1.5 cm from bottom
			$this->SetY(-15);
			$this->SetFont('arialn','I',8);
			$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
		}

	}

	$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(true);
	$pdf->SetMargins(10, 23, 10);

	$pdf->Open();
	$pdf->SetAutoPageBreak(false);

	$pdf->SetFillColor(220, 220, 220); 


	$pdf->AddFont('arialn','');
	$pdf->AddFont('arialn','B');
	$pdf->AddFont('arialn','I');
	$pdf->AddFont('arialn','BI');

	
	$new_page = 0;


		
	if($use_old_ln_forms_before_2013 == true)
	{
		$pdf->AddPage();
		include("ln_form_pdf_detail_before2013.php");
	}
	else
	{
		
		//cover sheet
		$pdf->AddPage();
		include("ln_booklet_cover_sheet_detail_pdf.php");
		$pdf->AddPage();
		
		include("ln_form_pdf_detail.php");

	}


	//page 3: Distribution Analysis
	include("lnr_03_pdf_detail_2016.php");

	

	//Page 3 Floor Plan
	$source_file = $floor_plan;
	if(file_exists($source_file))
	{
		
		//PDF
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			$pdfString = $pdf->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
			
			$merger = new SetaPDF_Merger();
			$merger->addDocument($tmp);

			$merger->addFile(array(
				'filename' => $source_file,
				'copyLayers' => true
			));


			$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			$pdf->SetMargins(10, 23, 10);
			$pdf->Open();

			$PDFmerger_was_used = true;
			$floor_plan_printed = true;

		}
		elseif(substr($source_file, strlen($source_file)-3, 3) == "jpg" 
			or substr($source_file, strlen($source_file)-3, 3) == "JPG"
		     or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
			or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
		{
			$pdf->AddPage("P");
			$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
			//arialn bold 15
			if(isset($version_date) and $version_date)
			{
				$pdf->SetFont('arialn','B',12);
				//Move to the right
				$pdf->Cell(40);
				//Title
				$pdf->SetY(0);
				$pdf->Cell(0,33,$page_title . $version_date,0,0,'R');
				//Line break
				$pdf->SetY(23);
			}
			else
			{
				$pdf->SetFont('arialn','B',18);
				//Move to the right
				$pdf->Cell(80);
				//Title
				$pdf->SetY(0);
				$pdf->Cell(0,33,$page_title,0,0,'R');
				//Line break
				$pdf->SetY(23);
			}
			$pdf->Image($source_file,10,30, 180, 0, "", "", "", true, 300, "", false, false, 0, false, false,true);
		}
	}

	//Page 4 Location layout

	$source_file = $location_layout;

	
	if(file_exists($source_file))
	{
		
		//PDF
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			if($PDFmerger_was_used == false)
			{
				$pdfString = $pdf->output('', 'S');
				$tmp = SetaPDF_Core_Document::loadByString($pdfString);

				$merger = new SetaPDF_Merger();
				$merger->addDocument($tmp);

				

			}

			$merger->addFile(array(
				'filename' => $source_file,
				'copyLayers' => true
			));

			if($PDFmerger_was_used == false)
			{
				    $pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
					$pdf->setPrintHeader(false);
					$pdf->setPrintFooter(false);
					$pdf->SetMargins(10, 23, 10);
					$pdf->Open();
			}

			$PDFmerger_was_used = true;

		}
	}

	
	
	//Picture 2
	if($pix2 != ".." and file_exists($pix2))
	{
		if(substr($pix2, strlen($pix2)-3, 3) == "jpg" 
			or substr($pix2, strlen($pix2)-3, 3) == "JPG"
		     or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
			or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
		{
			$pdf->AddPage("P", "A4");
			$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
			//arialn bold 15
			$pdf->SetFont('arialn','B',12);
			//Move to the right
			$pdf->Cell(80);
			//Title
			$pdf->SetY(0);
			$pdf->Cell(0,33,$page_title,0,0,'R');
			//Line break
			$pdf->SetY(23);

			$x = $pdf->GetX();
			$y = $pdf->GetY() + 2;
			$pdf->Image($pix2,$x,$y, 0, 110, "", "", "", true, 300, "", false, false, 0, false, false,true);
		}
	}


	//Picture 3
	if($pix3 != ".." and file_exists($pix3))
	{
		if(substr($pix3, strlen($pix3)-3, 3) == "jpg" 
			or substr($pix3, strlen($pix3)-3, 3) == "JPG"
		     or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
		    or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
		{
			$y = 145;
			$pdf->Image($pix3,$x,$y, 0, 110, "", "", "", true, 300, "", false, false, 0, false, false,true);
		}
	}

	
	// Lease Agreement 
	$source_file = $lease_agreement;
	if(file_exists($source_file))
	{
		//PDF
		if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
		{
			$pdfString = $pdf->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
			
			if(!isset($merger)) {
				$merger = new SetaPDF_Merger();
			}
			$merger->addDocument($tmp);

			$merger->addFile(array(
				'filename' => $source_file,
				'copyLayers' => true
			));


			
			$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
			$pdf->setPrintHeader(false);
			$pdf->SetMargins(10, 23, 10);

			$pdf->Open();
			$pdf->SetAutoPageBreak(false);
			$pdf->SetFillColor(220, 220, 220); 
			$pdf->AddFont('arialn','');
			$pdf->AddFont('arialn','B');
			$pdf->AddFont('arialn','I');
			$pdf->AddFont('arialn','BI');
			$pdf->Open();
			

			$PDFmerger_was_used = true;
			
		}
	}


	if($project["project_projectkind"] == 6) // relocation
	{
		
		// Document explaining relocation costs 
		$source_file = $relocation_doc;
		if(file_exists($source_file))
		{
			//PDF
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				
				if($PDFmerger_was_used == false) {
					$pdfString = $pdf->output('', 'S');
					$tmp = SetaPDF_Core_Document::loadByString($pdfString);
				
				
					if(!isset($merger)) {
						$merger = new SetaPDF_Merger();
					}
					$merger->addDocument($tmp);
				}

				$merger->addFile(array(
					'filename' => $source_file,
					'copyLayers' => true
				));

			
				$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
				$pdf->setPrintHeader(false);
				$pdf->SetMargins(10, 23, 10);

				$pdf->Open();
				$pdf->SetAutoPageBreak(false);
				$pdf->SetFillColor(220, 220, 220); 
				$pdf->AddFont('arialn','');
				$pdf->AddFont('arialn','B');
				$pdf->AddFont('arialn','I');
				$pdf->AddFont('arialn','BI');
				$pdf->Open();
				

				$PDFmerger_was_used = true;
				
			}
		}
	}


	//business plan
	$currencies = array();
	$sql = 'select currency_id, currency_symbol from currencies where currency_system = 1';

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$currencies["s"] = $row['currency_symbol'];
	}


	if($cer_basicdata['cer_basicdata_currency']) {
		$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency'];

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
		{
			$currencies["c1"] = $row['currency_symbol'];
		}
	}

	if($cer_basicdata['cer_basicdata_currency2']) {
		$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency2'];

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
		{
			$currencies["c2"] = $row['currency_symbol'];
		}
	}

	//business plan in client's currency
	$currency_source = "LN";


	if($use_old_ln_forms_before_2013 == true)
	{
		include("cer_inr02_pdf_detail_before2013.php");
	}
	else
	{
		include("cer_inr02_pdf_detail.php");
	}


	//business plan in system'currency
	if(array_key_exists("s", $currencies) and array_key_exists("c1", $currencies) and $currencies["s"] != $currencies["c1"])
	{
		$cid = 's';
		if($use_old_ln_forms_before_2013 == true)
		{
			include("cer_inr02_pdf_detail_before2013.php");
		}
		else
		{
			include("cer_inr02_pdf_detail.php");
		}
	}

	//business plan in pos'currency
	if(array_key_exists("c2", $currencies) and array_key_exists("c1", $currencies) and $currencies["c2"] != $currencies["c1"])
	{
		$cid = 'c2';
		if($use_old_ln_forms_before_2013 == true)
		{
			include("cer_inr02_pdf_detail_before2013.php");
		}
		else
		{
			include("cer_inr02_pdf_detail.php");
		}
	}


		
	//LN supporting documents
	$output_done = false;
	if(count($ln_supporting_documents) > 0)
	{
		foreach($ln_supporting_documents as $key=>$source_file)
		{
			if(file_exists($source_file))
			{
				//PDF
				if(substr($source_file, strlen($source_file)-3, 3) == "pdf" or substr($source_file, strlen($source_file)-3, 3) == "PDF")
				{
					
					if($output_done == false)
					{
						$pdfString = $pdf->output('', 'S');
						$tmp = SetaPDF_Core_Document::loadByString($pdfString);
						
						if(!isset($merger)) {
							$merger = new SetaPDF_Merger();
						}
						$merger->addDocument($tmp);
						$output_done = true;
					}
					
					
					$merger->addFile(array(
						'filename' => $source_file,
						'copyLayers' => true
					));


					$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
					$pdf->setPrintHeader(false);
					$pdf->setPrintFooter(false);
					$pdf->SetMargins(10, 23, 10);
					$pdf->Open();

					$PDFmerger_was_used = true;

				}
				elseif(substr($source_file, strlen($source_file)-3, 3) == "jpg" 
					or substr($source_file, strlen($source_file)-3, 3) == "JPG"
				     or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
					or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
				{
					
					$pdf->AddPage("P");
					$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
					//arialn bold 15
					if(isset($version_date) and $version_date)
					{
						$pdf->SetFont('arialn','B',12);
						//Move to the right
						$pdf->Cell(40);
						//Title
						$pdf->SetY(0);
						$pdf->Cell(0,33,$page_title . $version_date,0,0,'R');
						//Line break
						$pdf->SetY(23);
					}
					else
					{
						$pdf->SetFont('arialn','B',18);
						//Move to the right
						$pdf->Cell(80);
						//Title
						$pdf->SetY(0);
						$pdf->Cell(0,33,$page_title,0,0,'R');
						//Line break
						$pdf->SetY(23);
					}
					$pdf->Image($source_file,10,30, 180, 0, "", "", "", true, 300, "", false, false, 0, false, false,true);
					$output_done = false;
				}
			}
		}
	
	}

	if($use_old_ln_forms_before_2013 != true)
	{
		include("distribution_summary_pdf_detail.php");
		if($distribution_summary_available == true)
		{
			$output_done = false;
		}
	}

	
	
	// write pdf
	$file_name = 'LN_Booklet_' . str_replace(" ", "_", $posname2) . "_" . $project["project_number"] . '.pdf';
	if($PDFmerger_was_used == true)
	{
		if(count($ln_supporting_documents) > 0 and $output_done == true)
		{
		}
		else
		{
			$pdfString = $pdf->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
			$merger->addDocument($tmp);
		}
		
		$merger->merge();
		$document = $merger->getDocument();

		$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
		$document->save()->finish();
	}
	else
	{
		$pdf->Output($file_name, 'I');
	}
	
}

?>