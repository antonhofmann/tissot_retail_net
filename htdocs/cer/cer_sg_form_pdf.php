<?php
/********************************************************************

    cer_sg_form_pdf.php

    Print CER Forms.

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2018-01-05
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2018-01-05
    Version:        1.0.0

    Copyright (c) 2018, Tissot, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 


require "../include/frame.php";
check_access("has_access_to_cer");


$cer_version = 0;
$ln_version = 0;
$ln_signatures = "";
if(param("cerversion"))
{
	$cer_version = param("cerversion");
}
if(param("lnversion"))
{
	$ln_version = param("lnversion");
}

check_access("has_access_to_cer");
if(has_access("has_access_only_to_human_resources"))
{
	redirect("noaccess.php");
}
require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('cer_sg_form_data.php');

// Create and setup PDF document

$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 10, 10);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->SetTitle("Retail Lease Negotiation");
$pdf->SetAuthor( BRAND . " Retailnet");
$pdf->SetDisplayMode(100);
$pdf->SetAutoPageBreak(false, 0);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');
$pdf->Open();


include("cer_sg_form_inr01_pdf_detail.php");

include("cer_sg_form_inr02A_detail_pdf.php");

include("cer_sg_form_inr02B_detail_pdf.php");

include("cer_sg_form_inr02C_detail_pdf.php");

$file_name = "INR_" . $project["project_number"] . ".pdf";

$pdf->Output($file_name);

?>