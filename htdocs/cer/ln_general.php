<?php
/********************************************************************

    ln_general.php

    Application Form: lease negiotiation form
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-11-06
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_cer");


$deletable = "";
if(has_access("can_delete_uploaded_files"))
{
	$deletable = DELETABLE;
}

/********************************************************************
    prepare all data needed
*********************************************************************/
$project = get_project(param("pid"));

$client_address = get_address($project["order_client_address"]);
$client_currency = get_cer_currency(param("pid"));
$currency_symbol = $client_currency["symbol"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_address"] . ", " .
		$project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];

$project_detail = $project["project_number"] . ", " . $project["project_costtype_text"] . " " . $project["postype_name"] . ", " . $project["product_line_name"] . ", " . $project["projectkind_name"];


$choices = array();

$choices[0] = "No";
$choices[1] = "Yes";




//AF/LN Supporting Documents
$sql_attachment = "select distinct order_file_id, order_file_visited, ".
                  "    order_file_title, order_file_description, ".
                  "    order_file_path, file_type_name, ".
                  "    order_files.date_created, ".
                  "    order_file_category_name, order_file_category_priority, ".
                  "    concat(user_name, ' ', user_firstname) as owner_fullname ".
                  "from order_files  " .
                  "left join order_file_categories on order_file_category_id = order_file_category ".
                  "left join users on user_id = order_file_owner ".
                  "left join file_types on order_file_type = file_type_id ";

$list1_filter = "order_file_category_id = 17 and order_file_order = " . $project["project_order"];

/********************************************************************
    build form
*********************************************************************/
$form = new Form("ln_basicdata", "ln_basicdata");

$form->add_hidden("pid", param("pid"));
include("include/project_head.php");

	
$form->add_section("POS of the same Brand in the area");
$form->add_edit("ln_basicdata_numofpos_in_area", "Number of POS (for the same Brand) in the same city area", "" , $ln_basicdata['ln_basicdata_numofpos_in_area'], TYPE_INT, 8);
$form->add_edit("ln_basicdata_numofcpos_in_area", " - of which " . BRAND . " Corporate Stores ", "" , $ln_basicdata['ln_basicdata_numofcpos_in_area'], TYPE_INT, 8);
$form->add_edit("ln_basicdata_numoffpos_in_area", " - of which " . BRAND . " Franchise Stores ", "" , $ln_basicdata['ln_basicdata_numoffpos_in_area'], TYPE_INT, 8);


if($project['project_projectkind'] == 5
	or $project['project_projectkind'] == 4) // lease reneval, take over
{
	$form->add_hidden("ln_basicdata_average_buildout_costs");
}
else {
	$form->add_section('Average Buildout Costs');
	$form->add_comment("Please indicate the average buildout costs (" . $currency_symbol . "/m2) of corporate POS for " . BRAND . " <strong>in the same area</strong>.<br />Take similar corporate POS (e.g. compare airport stores to airport stores, SIS to SIS etc.)<br />If not enough other POS in the same city, please provide the information for the same market or same country.<br />Click to <a id='calculateabc' href='javascript:void(0);'>calculate the average buildout costs</a>.");
			
	$form->add_edit("ln_basicdata_average_buildout_costs", "Average Buildout Costs in " . $currency_symbol , 0, $ln_basicdata["ln_basicdata_average_buildout_costs"], TYPE_DECIMAL, 10,2);
}

if($project['project_projectkind'] != 5) // lease renewal
{
	$form->add_section("Key Points");
	$form->add_multiline("ln_basicdata_remarks", "Key Points*", 10, NOTNULL, $ln_basicdata["ln_basicdata_remarks"]);

	$form->add_multiline("ln_basicdata_remarks1", "Advantages, Disadvantages*", 10, NOTNULL, $ln_basicdata["ln_basicdata_remarks1"]);

	$form->add_section("POS Stragtegy");
	$form->add_multiline("ln_basicdata_remarks2", "Brand Image*", 4, NOTNULL, $ln_basicdata["ln_basicdata_remarks2"]);
	$form->add_multiline("ln_basicdata_remarks3", "Sales Development*", 4, NOTNULL, $ln_basicdata["ln_basicdata_remarks3"]);
	$form->add_multiline("ln_basicdata_remarks4", "Market Situation*", 4, NOTNULL, $ln_basicdata["ln_basicdata_remarks4"]);
}
else {

	$form->add_section("Key Points");
	$form->add_multiline("ln_basicdata_remarks", "Key Points, Advantages, Disadvantages*", 10, NOTNULL, $ln_basicdata["ln_basicdata_remarks"]);

	$form->add_section("Reasons/Commitment");
	$form->add_comment("Indicate the reasons of the lease renewal request (location, performance, ...)");
	$form->add_multiline("ln_basicdata_remarks1", "Reasons*", 10, NOTNULL, $ln_basicdata["ln_basicdata_remarks1"]);

	
	$form->add_comment("Justify projected sales in view of pas performance (make also reference to other lease renewals)");
	$form->add_multiline("ln_basicdata_remarks2", "Justification*", 10, NOTNULL, $ln_basicdata["ln_basicdata_remarks2"]);
	
	$form->add_comment("Commitment that Sales targets are very likely to be achieved)");
	$form->add_multiline("ln_basicdata_remarks3", "Commitment by the Brand Manager*", 10, NOTNULL, $ln_basicdata["ln_basicdata_remarks3"]);
	$form->add_multiline("ln_basicdata_remarks4", "Commitment by the Country Manager*", 10, NOTNULL, $ln_basicdata["ln_basicdata_remarks4"]);
}


if($project['project_cost_type'] == 1
    and ($project["project_projectkind"] == 2 
	or $project["project_projectkind"] == 3
	or $project["project_projectkind"] == 4 
	or $project["project_projectkind"] == 5 
	or $project["project_projectkind"] == 6
	or $project["project_projectkind"] == 9))
{
	$form->add_section("Previous Rental Conditions");

	$form->add_multiline("ln_basicdata_passedrent", "Description of the previous rental conditions*", 6, NOTNULL, $ln_basicdata["ln_basicdata_passedrent"]);

	$form->add_comment("Upload a document with figures of the last years as to clearly show the trend of the POS.");
	$form->add_upload("ln_basicdata_sales_doc", "Document explaning figures of last years (PDF only)*", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_sales_doc"]);
}
else
{
	$form->add_hidden("ln_basicdata_passedrent");
	$form->add_hidden("ln_basicdata_sales_doc");
}

if($project["project_cost_type"] == 1
	and $project["project_projectkind"] == 6) // corporate relocation
{
	$form->add_section("Relocation Information");
	$form->add_edit("ln_basicdata_relocation_costs", "Total estimated closing costs in " . $cer_basicdata['currency_symbol']."*", NOTNULL , $ln_basicdata["ln_basicdata_relocation_costs"], TYPE_DECIMAL, 12, 0);

	$form->add_multiline("ln_basicdata_relocation_comment", "Description of estimated closing costs*", 4, NOTNULL, $ln_basicdata["ln_basicdata_relocation_comment"]);

	$form->add_upload("ln_basicdata_relocation_doc", "Document explaning estimated closing costs (PDF only)*", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_relocation_doc"]);

}
else {
	
	$form->add_hidden("ln_basicdata_relocation_costs");

	$form->add_hidden("ln_basicdata_relocation_comment");

	$form->add_hidden("ln_basicdata_relocation_doc");
}

if($form_type == 'AF'
	or $form_type == 'INR03') {

	$form->add_section("Attachments");
	$form->add_comment("For best results in printing we recommend to upload files having the format A4 landscape.");

	$form->add_upload("ln_basicdata_floorplan", "Mall, Floor Map/Street Map (PDF only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_floorplan"], 1, "floorplan");

	$form->add_upload("ln_basicdata_pos_section_image", "Zoom POS Section Details (PDF only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pos_section_image"]);

	$form->add_upload("ln_basicdata_location_layout", "Technicals layout with dimensions (pdf only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_location_layout"]);

	$form->add_upload("ln_basicdata_fassade_image", "Façade Image or Rendering (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_fassade_image"]);

	$form->add_hidden("ln_basicdata_draft_aggreement");

	$form->add_comment("Insert Pictures of the Location proposed from different direction large view and close ones.");
	$form->add_upload("ln_basicdata_pix1", "Photo 1 Left View (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pix1"]);
	$form->add_upload("ln_basicdata_pix2", "Photo 2 Right View (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pix2"]);
	$form->add_upload("ln_basicdata_pix3", "Photo 3 Large one (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pix3"]);
	$form->add_upload("ln_basicdata_pix4", "Photo 4 Close one (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pix4"]);
}
elseif($project['project_projectkind'] == 5) // lease reneval
{
	$form->add_section("Attachments");
	$form->add_comment("For best results in printing we recommend to upload files having the format A4 landscape.");

	$form->add_hidden("ln_basicdata_floorplan");
	$form->add_hidden("ln_basicdata_pos_section_image");
	$form->add_hidden("ln_basicdata_location_layout");
	$form->add_hidden("ln_basicdata_fassade_image");
	
	$form->add_upload("ln_basicdata_draft_aggreement", "Draft Lease Agreement (PDF only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_draft_aggreement"]);


	$form->add_hidden("ln_basicdata_pix1");
	$form->add_hidden("ln_basicdata_pix2");
	$form->add_hidden("ln_basicdata_pix3");
	$form->add_hidden("ln_basicdata_pix4");
}
else {

	$form->add_section("Attachments");
	$form->add_comment("For best results in printing we recommend to upload files having the format A4 landscape.");

	$form->add_upload("ln_basicdata_floorplan", "Mall, Floor Map/Street Map (PDF only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_floorplan"], 1, "floorplan");

	$form->add_upload("ln_basicdata_pos_section_image", "Zoom POS Section Details (PDF only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pos_section_image"]);

	$form->add_upload("ln_basicdata_location_layout", "Technicals layout with dimensions (pdf only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_location_layout"]);

	$form->add_upload("ln_basicdata_fassade_image", "Façade Image or Rendering (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_fassade_image"]);

	$form->add_upload("ln_basicdata_draft_aggreement", "Draft Lease Agreement (PDF only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_draft_aggreement"]);

	$form->add_comment("Insert Pictures of the Location proposed from different direction large view and close ones.");
	$form->add_upload("ln_basicdata_pix1", "Photo 1 Left View (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pix1"]);
	$form->add_upload("ln_basicdata_pix2", "Photo 2 Right View (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pix2"]);
	$form->add_upload("ln_basicdata_pix3", "Photo 3 Large one (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pix3"]);
	$form->add_upload("ln_basicdata_pix4", "Photo 4 Close one (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pix4"]);
}


if(($project["order_actual_order_state_code"] < '890' 
	and $ln_basicdata["ln_basicdata_locked"] == 0 
	and has_access("has_access_to_his_cer") 
	and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") 
	and $project["project_state"] != 2))
{

	
	if (has_access("can_add_attachments_in_projects"))
	{
		
		$urlQuery = http_build_query(array(
			'action' => '/cer/ajx_upload_supporting_documents.php',
			'title' => 'Upload Files',
			'subtitle' => 'Supporting Documents',
			'infotext' => 'Allowed file types are: pdf, jpg, jpeg',
			'fields' => array(
				'id' => param("pid"),
			    'category' => 17,
				'extensions' => 'pdf,jpg,jpeg',
				'startload' => true
			)
		));
		
		$iframeAttributes = array(
			"data-fancybox-type" => "iframe",
			"data-fancybox-width" => "800",
			"data-fancybox-height" => "620",
			"href" => "/public/themes/default/multifile.uploader.php?$urlQuery"
		);
		
		$form->add_button("add_attachment", "Add Supporting Documents", $iframeAttributes, IS_UPLOAD_BUTTON);
	}

	$form->add_button("save", "Save Data");

}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();




/********************************************************************
    List of AF/LN supporting documents
*********************************************************************/
$list1 = new ListView($sql_attachment);

if($project["project_cost_type"] == 1)
{
	$list1->set_title("LNR Supporting Documents");
}
else
{
	$list1->set_title("LNR Supporting Documents");
}

$list1->set_entity("order_files");
$list1->set_filter($list1_filter);
$list1->set_order("order_files.date_created DESC");



$link = "http://" . $_SERVER["HTTP_HOST"] . "/include/openfile.php?id={order_file_id}";
$list1->add_column("order_file_title", "Title", $link, "", "", COLUMN_NO_WRAP);
$list1->add_column("file_type_name", "Type", "", "", "", COLUMN_NO_WRAP);


/********************************************************************
    Process Form
*********************************************************************/

if($form->button("save"))
{

	//$valid = $form->validate();
	$valid = true;


	if($form->value("ln_basicdata_floorplan") 
		and strpos(strtolower($form->value("ln_basicdata_floorplan")), 'pdf') == 0)
	{
		$form->error("The Mall Map/Street Map must be a PDF-File.");
	}
	elseif($form->value("ln_basicdata_pos_section_image") and strpos(strtolower($form->value("ln_basicdata_pos_section_image")), 'pdf') == 0)
	{
		$form->error("The Zoom POS Section Details must be a PDF-File.");
	}
	elseif($form->value("ln_basicdata_location_layout") and strpos(strtolower($form->value("ln_basicdata_location_layout")), 'pdf') == 0)
	{
		$form->error("The Location Layout must be a PDF-File.");
	}
	elseif($form->value("ln_basicdata_fassade_image") 
		and strpos(strtolower($form->value("ln_basicdata_fassade_image")), 'jpg') == 0
		and strpos(strtolower($form->value("ln_basicdata_fassade_image")), 'jpeg') == 0)
	{
		$form->error("The façade image must be a JPG file.");
	}
	elseif($form->value("ln_basicdata_pix1") 
		and strpos(strtolower($form->value("ln_basicdata_pix1")), 'jpg') == 0)
	{
		$form->error("Pictures mus be JPG-Files.");
	}
	elseif($form->value("ln_basicdata_pix2") 
		and strpos(strtolower($form->value("ln_basicdata_pix2")), 'jpg') == 0)
	{
		$form->error("Pictures mus be JPG-Files.");
	}
	elseif($form->value("ln_basicdata_pix3") 
		and strpos(strtolower($form->value("ln_basicdata_pix3")), 'jpg') == 0)
	{
		$form->error("Pictures mus be JPG-Files.");
	}
	elseif($form->value("ln_basicdata_pix4") 
		and strpos(strtolower($form->value("ln_basicdata_pix4")), 'jpg') == 0)
	{
		$form->error("Pictures mus be JPG-Files.");
	}
	elseif($form->value("ln_basicdata_draft_aggreement") 
		and strpos(strtolower($form->value("ln_basicdata_draft_aggreement")), 'pdf') == 0)
	{
		$form->error("The Draft Lease Agreement must be a PDF-File.");
	}
	elseif(strlen($form->value("ln_basicdata_remarks")) > 600)
	{
		$form->error("Description of key points is to long. The maximum number of characters is 600 characters. Plese shorten the description.");
	}
	elseif($project["project_projectkind"] == 6
		and $form->value("ln_basicdata_relocation_doc") and strpos(strtolower($form->value("ln_basicdata_relocation_doc")), 'pdf') == 0)
	{
		$form->error("The document explaning estimated closing costs must be a PDF-File.");
	}
	elseif(($project["project_projectkind"] == 2 
			or $project["project_projectkind"] == 3
			or $project["project_projectkind"] == 4 
			or $project["project_projectkind"] == 5 
			or $project["project_projectkind"] == 6) 
		    and strpos(strtolower($form->value("ln_basicdata_sales_doc")), 'pdf') == 0)
	{
		$form->error("The document explaning last years figures must be a PDF-File.");
	}
	elseif($form->validate())
	{
		//update ln_basic_data
		$fields = array();


		$value = dbquote($form->value("ln_basicdata_numofpos_in_area"));
		$fields[] = "ln_basicdata_numofpos_in_area = " . $value;

		$value = dbquote($form->value("ln_basicdata_numofcpos_in_area"));
		$fields[] = "ln_basicdata_numofcpos_in_area = " . $value;

		$value = dbquote($form->value("ln_basicdata_numoffpos_in_area"));
		$fields[] = "ln_basicdata_numoffpos_in_area = " . $value;
		
		$value = dbquote($form->value("ln_basicdata_passedrent"));
		$fields[] = "ln_basicdata_passedrent = " . $value;

		$value = dbquote($form->value("ln_basicdata_remarks"));
		$fields[] = "ln_basicdata_remarks = " . $value;

		$value = dbquote($form->value("ln_basicdata_remarks1"));
		$fields[] = "ln_basicdata_remarks1 = " . $value;

		$value = dbquote($form->value("ln_basicdata_remarks2"));
		$fields[] = "ln_basicdata_remarks2 = " . $value;

		$value = dbquote($form->value("ln_basicdata_remarks3"));
		$fields[] = "ln_basicdata_remarks3 = " . $value;

		$value = dbquote($form->value("ln_basicdata_remarks4"));
		$fields[] = "ln_basicdata_remarks4 = " . $value;

		$value = dbquote($form->value("ln_basicdata_average_buildout_costs"));
		$fields[] = "ln_basicdata_average_buildout_costs = " . $value;

		$value = dbquote($form->value("ln_basicdata_floorplan"));
		$fields[] = "ln_basicdata_floorplan = " . $value;

		$value = dbquote($form->value("ln_basicdata_pos_section_image"));
		$fields[] = "ln_basicdata_pos_section_image = " . $value;

		$value = dbquote($form->value("ln_basicdata_fassade_image"));
		$fields[] = "ln_basicdata_fassade_image = " . $value;
		

		$value = dbquote($form->value("ln_basicdata_location_layout"));
		$fields[] = "ln_basicdata_location_layout = " . $value;

		$value = dbquote($form->value("ln_basicdata_pix1"));
		$fields[] = "ln_basicdata_pix1 = " . $value;

		$value = dbquote($form->value("ln_basicdata_pix2"));
		$fields[] = "ln_basicdata_pix2 = " . $value;

		$value = dbquote($form->value("ln_basicdata_pix3"));
		$fields[] = "ln_basicdata_pix3 = " . $value;

		$value = dbquote($form->value("ln_basicdata_pix4"));
		$fields[] = "ln_basicdata_pix4 = " . $value;

		$value = dbquote($form->value("ln_basicdata_draft_aggreement"));
		$fields[] = "ln_basicdata_draft_aggreement = " . $value;


		if($project["project_projectkind"] == 6) // relocation
		{
			$value = dbquote($form->value("ln_basicdata_relocation_costs"));
			$fields[] = "ln_basicdata_relocation_costs = " . $value;

			$value = dbquote($form->value("ln_basicdata_relocation_comment"));
			$fields[] = "ln_basicdata_relocation_comment = " . $value;

			$value = dbquote($form->value("ln_basicdata_relocation_doc"));
			$fields[] = "ln_basicdata_relocation_doc = " . $value;

			$value = dbquote($form->value("ln_basicdata_sales_doc"));
			$fields[] = "ln_basicdata_sales_doc = " . $value;
		}
		
		
		if($project["project_projectkind"] == 2 
			or $project["project_projectkind"] == 3
			or $project["project_projectkind"] == 4 
			or $project["project_projectkind"] == 5)
		{
			$value = dbquote($form->value("ln_basicdata_sales_doc"));
			$fields[] = "ln_basicdata_sales_doc = " . $value;
		}
		


		$value1 = "current_timestamp";
		$project_fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$project_fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update ln_basicdata set " . join(", ", $fields) . 
			" where ln_basicdata_project = " . param("pid") .
			" and ln_basicdata_version = 0";
		
		mysql_query($sql) or dberror($sql);


		if($form->button("save"))
		{
			//$link = "ln_general.php?pid=" . param("pid");
			//redirect($link);
			//$form->message("Your data has been saved.");
		}
		
	}
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();
if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1)) {
	$page->title("LNR/CER: General Information");
}
elseif($form_type == "AF") {
	$page->title("LNR/INR03: General Information");
}
else {
	$page->title("INR-03 - Retail Furniture in Third-party Store: General Information");
}


require_once("include/tabs_ln2016.php");

/*
if(($project["project_projectkind"] == 2 
        or $project["project_projectkind"] == 3
		or $project["project_projectkind"] == 4
		or $project["project_projectkind"] == 5)) // renovation or tekover/renovation or lease renewal
{
	require_once("include/tabs_ln.php");
}
elseif($project["project_projectkind"] == 6 and $project["project_relocated_posaddress_id"] > 0) // relocation
{
	require_once("include/tabs_ln.php");
	
	$latest_project = get_latest_pos_project($project["project_relocated_posaddress_id"]);
	
	if(count($latest_project) > 0)
	{
		require_once("include/tabs_ln.php");
	}
	
}
*/

$form->render();
echo "<p>&nbsp;</p>";
$list1->render();

?>

<div id="floorplan" style="display:none;">
    Please make sure that the mall/street Map plan contains:
	<ul>
	<li>a clear indication of future Tissot Location</li>
	<li>a clear indication of who our direct neighbours are</li>
	<li>indication of major brands an brand mix of the shopping mall</li>
	</ul>
</div>




<script type="text/javascript">
  jQuery(document).ready(function($) {
  
	$('#calculateabc').click(function(e) {
		e.preventDefault();
		$.nyroModalManual({
		  url: 'include/calculate_average_bouildout_costs.php?cid=<?php echo $cer_basicdata["cer_basicdata_id"];?>&pid=<?php echo param("pid");?>'
		});
		return false;
	  });
  
});
</script>


<?php

require "include/footer_scripts.php";

/*
// translations
$translate = Translate::instance();

$modalContent = Template::load('file.uploader', array(
	'action' => '/cer/ajx_upload_supporting_documents.php',
	'class' => 'modal-750',
	'title' => $translate->upload_file,
	'subtitle' => 'Supporting Documents',
	'infotext' => 'Allowed file types are: pdf, jpg, jpeg',
	'fields' => array(
		'id' => param("pid"),
		'extensions' => 'pdf,jpg,jpeg',
		'startload' => true
	), 
	'attributes' => array(
		'data-url' => '/applications/helpers/file.uploader.php',
		'data-upload-template-id' => 'retailnet-template-upload',
		'data-download-template-id' => 'retailnet-template-download',
		'data-auto-upload' => true,
		'data-sequential-uploads' => true
	)
));
*/

$page->footer();

?>