<?php
/********************************************************************

    standardparameters.php

    Lists CER Standardparameters

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_full_access_to_cer");
set_referer("standardparameter.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
//get all data from interest rates

$year_months = array();
$rates = array();

$sql = "select cer_standardparameter_id, cer_standardparameter_text, cer_standardparameter_value " . 
       "from cer_standardparameters ";


/********************************************************************
    build list 
*********************************************************************/

$list = new ListView($sql);

$list->set_entity("cer_standardparameters");
$list->set_order("cer_standardparameter_text");

$list->add_column("cer_standardparameter_text", "Parameter", "standardparameter.php");
$list->add_column("cer_standardparameter_value", "Value", "", "", "", COLUMN_ALIGN_RIGHT);

$list->populate();
$list->process();

$page = new Page("standardparameters");

$page->header();
$page->title("Standard Parameters");
$list->render();
$page->footer();
?>
