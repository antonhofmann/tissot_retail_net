<?php
/********************************************************************

    cer_application_franchisee.php

    Application Form: franchisee information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/
$client_address = get_address($project["order_client_address"]);
$posdata = get_pos_data($project["project_order"]);

//get quantities of watches
//get revenue values
$quantity_watches = array();

$sql = "select * from cer_revenues " . 
       "where cer_revenue_project = " . param("pid") . 
	   " and cer_revenue_cer_version = 0 ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$quantity_watches[$row["cer_revenue_year"]] = $row["cer_revenue_quantity_watches"];
}

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";

/********************************************************************
    build form
*********************************************************************/
$form = new Form("posaddresses", "posaddress");

include("include/project_head.php");


$form->add_hidden("pid", param("pid"));

$form->add_section("Franchisee");

$form->add_label("franchisee", $franchisee_address["company"]);

$form->add_checkbox("franchisee_address_swatch_retailer", "Company is a present Tissot Retailer", "", "", "Relation");

$form->add_section("Territory Exclusivity");
$form->add_comment("Please give a description clearly delimitating the exclusive franchisee territory covered by the franchisee contract and provide a detailed city pasted under annex 1. ");
$form->add_multiline("posaddress_fag_territory", "Agreement Content*", 4, NOTNULL, $posdata["posaddress_fag_territory"]);

$form->add_upload("posaddress_fag_city_pasted", "City Pasted", "/files/cer/project_". param("pid"), NOTNULL | $deletable, $posdata["posaddress_fag_city_pasted"]);

$form->add_section("Minimum Quantities (only Watches as per Franchisee Contract)");
foreach($quantity_watches as $year=>$quantity)
{
	$form->add_label("units_" . $year, "Units in " . $year ,0 ,$quantity);
}


$form->add_section("Franchisor Address");


$form->add_button("form_save", "Save Data");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if($form->button("form_save"))
{
	if($form->validate())
	{
		
		


		//update posaddress data

		$fields = array();
		
		


		$value = dbquote($form->value("posaddress_fag_territory"));
		$fields[] = "posaddress_fag_territory = " . $value;

		$value = dbquote($form->value("posaddress_fag_quantity1"));
		$fields[] = "posaddress_fag_quantity1 = " . $value;

		$value = dbquote($form->value("posaddress_fag_quantity2"));
		$fields[] = "posaddress_fag_quantity2 = " . $value;

		$value = dbquote($form->value("posaddress_fag_quantity3"));
		$fields[] = "posaddress_fag_quantity3 = " . $value;

		$value = dbquote($form->value("posaddress_fag_quantity4"));
		$fields[] = "posaddress_fag_quantity4 = " . $value;

		$value = dbquote($form->value("posaddress_fag_quantity5"));
		$fields[] = "posaddress_fag_quantity5 = " . $value;

		$value = dbquote($form->value("posaddress_fag_city_pasted"));
		$fields[] = "posaddress_fag_city_pasted = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update " . $posdata["table"] . " set " . join(", ", $fields) . " where posaddress_id = " . $posdata["posaddress_id"];
		mysql_query($sql) or dberror($sql);

		$form->message("Your data has bee saved.");

	
	}

}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();
if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Franchisee Information");
}
elseif($form_type == "AF")
{
	$page->title("LNR/INR03: Franchisee Information");
}
else
{
	$page->title("LNR/CER: Franchisee Information");
}
require_once("include/tabs2016.php");
$form->render();

require "include/footer_scripts.php";
$page->footer();

?>