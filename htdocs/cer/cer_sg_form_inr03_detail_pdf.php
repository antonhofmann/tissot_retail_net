<?php
/********************************************************************

    cer_sg_form_inr03_detail_pdf.php

    Print PDF for Request Retail Furniture for Third-Party POS (Form INR-03).

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-08-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-12
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.
*********************************************************************/


//set pdf parameters
$margin_top = 16;
$margin_left = 12;
$y = $margin_top;

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 9);
$pdf->Cell(40, 8, "Swatch Group", 1);

$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(82, 8, "Request - Retail Furniture in Third-party-Store", 1, "", "C");

$pdf->SetFont("arialn", "", 9);
$pdf->SetFillColor(248,251,167);
$pdf->Cell(25, 8, "SUMMARY", 1, "", "C", false);

$pdf->SetFont("arialn", "", 9);

if($cer_version == 0)
{
	$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C");
}
else
{
	$pdf->Cell(20, 8,to_system_date(substr($cer_basicdata["version_date"], 0, 10)), 1, "", "C");
}
$pdf->Cell(20, 8, "INR-03", 1, "", "C");


// draw outer box
$pdf->SetXY($margin_left,$y+8);
$pdf->Cell(187, 223, "", 1);



	// print project and investment infos
	
	$y = $y+9;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Business Partner Information:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(107, 5, substr($owner_company, 0, 130), 1, "", "L", true);


	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 5, "Brand:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(19, 5, BRAND, 1, "", "L", true);


	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->Cell(39, 5, "Store location", 1, "", "L");
	$pdf->Cell(146, 5, $project_name, 1, "", "L", true);

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Project classification: (X)", 1, "", "L");
	$pdf->Cell(20, 5, "New", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(11, 5, $project_kind_new, 1, "", "L", true);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 5, "Replacement", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(11, 5, $project_kind_renovation, 1, "", "L", true);
	$pdf->Cell(28, 5, "Last SIS Installation:", 1, "", "L");
	$pdf->Cell(56, 5, $predecessor_opening_date, 1, "", "L", true);


	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Existing Business Partner:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	if($cer_basicdata["cer_basicdata_franchsiee_already_partner"] == 1)
	{
		if($franchisee_address["address_company_is_partner_since"] != NULL
			and $franchisee_address["address_company_is_partner_since"] != '0000-00-00') {

			$pdf->Cell(146, 5, "YES, since " . to_system_date($franchisee_address["address_company_is_partner_since"]), 1, "", "L", true);
		}
		else {
			$pdf->Cell(146, 5, "YES", 1, "", "L", true);
		}
	}
	else
	{
		$pdf->Cell(146, 5, "NO", 1, "", "L", true);
	}



	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Furniture supplier name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(72, 5, $suppliers, 1, "", "L", true);
	
	$pdf->Cell(34, 5, "Store Concept:", 1, "", "L");
	$pdf->Cell(40, 5, substr($project["product_line_name"], 0, 30), 1, "", "L", true);

	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Cost of Furniture:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(72, 5, $fixed_assets_total . " / " . $fixed_assets_total_chf, 1, "", "L", true);
	
	$pdf->Cell(34, 5, "Surface (in sqm):", 1, "", "L");
	$pdf->Cell(40, 5, $project["project_cost_totalsqms"], 1, "", "L", true);


	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Cost of installation/Transport:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(72, 5, $transportation_total . " / " . $transportation_total_chf, 1, "", "L", true);



	$pdf->Cell(34, 5, "Currency", 1, "", "L");
	$pdf->Cell(40, 5, $currency_symbol . " / CHF", 1, "", "L", true);

	
	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Cost of Construction:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(72, 5, $construction_total . " / " . $construction_total_chf, 1, "", "L", true);

	$pdf->Cell(34, 5, "Contract start:", 1, "", "L");
	$pdf->Cell(40, 5, to_system_date($project["project_fagrstart"]), 1, "", "L", true);


	
	

	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Total Cost:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(72, 5, $investment_amount . " / " . $investment_amount_chf, 1, "", "L", true);

	$pdf->Cell(34, 5, "Contract end:", 1, "", "L");
	$pdf->Cell(40, 5, to_system_date($project["project_fagrend"]), 1, "", "L", true);

	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Business Partner Contribution:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(37, 5, $investment_business_partner . " / " . $investment_business_partner_chf, 1, "", "L", true);
	$pdf->Cell(8, 5, "in %", 1, "", "L");
	$pdf->Cell(27, 5, $cer_basicdata["cer_basicdata_franchsiee_investment_share"] . "%", 1, "", "L", true);

	$pdf->Cell(34, 5, "Years of depreciation", 1, "", "L");
	$pdf->Cell(40, 5, $depr_period, 1, "", "L", true);


	$y = $y+5;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Requested Amount:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(37, 5, $requested_amount . " / " . $requested_amount_chf, 1, "", "L", true);
	$pdf->Cell(109, 5, "", 1, "", "L");

	if(($project["project_projectkind"] == 2 
				or $project["project_projectkind"] == 3 
				or $project["project_projectkind"] == 5)
		and count($sellouts_watches) > 0) // renovation or takover/renovation or lease renewal
	{
		//reduce to the last 2 years
		$sellouts_watches = array_slice($sellouts_watches,count($sellouts_watches)-2, 2, true);
		$sellouts_watches_gross_sales = array_slice($sellouts_watches_gross_sales,count($sellouts_watches_gross_sales)-2,2, true);


		$y = $y+7;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(21, 5, "1. Sales*", 1, "", "L");
		$pdf->SetFont("arialn", "B", 8);


		//data from the past
		$k = 1;
		foreach($sellouts_watches as $year=>$value)
		{
			if($k < 11)
			{
				$pdf->Cell(12, 5, $year . " (" . $sellouts_months[$year] . ")", 1, "", "R");
				$k++;
			}
		}

		//expected data
		$pdf->Cell(30, 5, "", 1, "", "L");
		$k = 1;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 9)
			{
				$pdf->Cell(12, 5, $i, 1, "", "R");
				$k++;
			}
		}


		$y = $y+5;
		$x = $margin_left+1;

		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(21, 5, "Watches sold:", 1, "", "L");

		//data from the past
		$pdf->SetFont("arialn", "n", 8);
		foreach($sellouts_watches as $year=>$value)
		{
			$pdf->Cell(12, 5, $value, 1, "", "R", true);
		}


		//expected data
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(30, 5, "Expected watches sold:", 1, "", "L");
		$pdf->SetFont("arialn", "n", 8);
		$k = 1;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 9)
			{
				if(array_key_exists($i, $sales_units_watches_values)) {
					$pdf->Cell(12, 5, $sales_units_watches_values[$i], 1, "", "R", true);
				}
				else
				{
					$pdf->Cell(12, 5, "", 1, "", "R", true);
				}
				$k++;
			}
		}





		$y = $y+5;
		$x = $margin_left+1;

		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(21, 5, "Gross sales:", 1, "", "L");

		$pdf->SetFont("arialn", "n", 8);
		
		//data from the past
		foreach($sellouts_watches_gross_sales as $year=>$value)
		{
			$pdf->Cell(12, 5, round($value/1000, 0), 1, "", "R", true);
		}


		//expected data
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(30, 5, "Expected gross sales:", 1, "", "L");
		$pdf->SetFont("arialn", "n", 8);
		$k = 1;
		$total_expected_gross_sales = 0;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 9)
			{
				if(array_key_exists($i, $total_gross_sales_values)) {
					$pdf->Cell(12, 5, round($total_gross_sales_values[$i]/1000, 0), 1, "", "R", true);
				}
				else
				{
					$pdf->Cell(12, 5, "", 1, "", "R", true);
				}
				$k++;
			}

			if(array_key_exists($i, $total_gross_sales_values)) {
				$total_expected_gross_sales = $total_expected_gross_sales + $total_gross_sales_values[$i];
			}
		}
		
		if($total_expected_gross_sales > 0)
		{
			$pdf->setX($pdf->getX() + 10);
			$pdf->Cell(12, 5, round($total_expected_gross_sales/1000, 0), 1, "", "R", true);
		}

		//investment in percent of sales
		$y = $y+5;
		$x = $margin_left+1;
		$pdf->SetXY($x,$y);
		
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(45, 5, "", 1, "", "L");
		$pdf->Cell(30, 5, "Investment in % of sales:", 1, "", "L");

		$pdf->SetFont("arialn", "n", 8);
		$k = 1;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 11)
			{
				if(array_key_exists($i, $depreciation_on_requested_amount_in_percent))
				{
					$pdf->Cell(12, 5, $depreciation_on_requested_amount_in_percent[$i], 1, "", "R", true);
				}
				else
				{
					$pdf->Cell(12, 5, "", 1, "", "R", true);
				}
				$k++;
			}
		}

		
		if($total_expected_gross_sales > 0)
		{
			$pdf->setX($pdf->getX() + 10);
			$pdf->Cell(12, 5, round(100*$requested_amount1/$total_expected_gross_sales, 2) .'%', 1, "", "R", true);
		}

		$y = $y+5;
		$pdf->SetXY($margin_left+1,$y);
		
		$pdf->Cell(80, 5, "*Units are sell through with Wholesale average price ", 0, "", "L");

		
	}
	else
	{
		$y = $y+7;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 9);
		$pdf->Cell(39, 5, "1. Sales*", 1, "", "L");
		

		$pdf->SetFont("arialn", "B", 8);
		$k = 1;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 11)
			{
				$pdf->Cell(12, 5, $i, 1, "", "R");
				$k++;
			}
		}

		$y = $y+5;
		$x = $margin_left+1;

		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Expected watches sold:", 1, "", "L");


		$pdf->SetFont("arialn", "n", 8);
		$k = 1;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 11)
			{
				if(array_key_exists($i, $sales_units_watches_values)) {
					$pdf->Cell(12, 5, $sales_units_watches_values[$i], 1, "", "R", true);
				}
				else
				{
					$pdf->Cell(12, 5, "", 1, "", "R", true);
				}
				$k++;
			}
		}



		$y = $y+5;
		$x = $margin_left+1;

		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Expected gross sales:", 1, "", "L");

		$pdf->SetFont("arialn", "n", 8);
		$k = 1;
		$total_expected_gross_sales = 0;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 11)
			{
				if(array_key_exists($i, $total_gross_sales_values)) {
					$pdf->Cell(12, 5, round($total_gross_sales_values[$i]/1000, 0), 1, "", "R", true);
				}
				else
				{
					$pdf->Cell(12, 5, "", 1, "", "R", true);
				}
				$k++;
			}

			if(array_key_exists($i, $total_gross_sales_values)) {
				$total_expected_gross_sales = $total_expected_gross_sales + $total_gross_sales_values[$i];
			}
		}
		
		if($total_expected_gross_sales > 0)
		{
			$pdf->setX($pdf->getX() + 10);
			$pdf->Cell(12, 5, round($total_expected_gross_sales/1000, 0), 1, "", "R", true);
		}


		$y = $y+5;
		$x = $margin_left+1;

		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(39, 5, "Investment in % of sales:", 1, "", "L");

		$pdf->SetFont("arialn", "n", 8);
		$k = 1;
		for($i=$first_year;$i<=$last_year;$i++)
		{
			if($k < 11)
			{
				if(array_key_exists($i, $depreciation_on_requested_amount_in_percent))
				{
					$pdf->Cell(12, 5, $depreciation_on_requested_amount_in_percent[$i], 1, "", "R", true);
				}
				else
				{
					$pdf->Cell(12, 5, "", 1, "", "R", true);
				}
				$k++;
			}
		}

		if($total_expected_gross_sales > 0)
		{
			$pdf->setX($pdf->getX() + 10);
			$pdf->Cell(12, 5, round(100*$requested_amount1/$total_expected_gross_sales, 2) .'%', 1, "", "R", true);
		}

		$y = $y+5;
		$pdf->SetXY($margin_left+1,$y);
		
		$pdf->Cell(80, 5, "*Units are sell through with Wholesale average price ", 0, "", "L");
	}

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(102, 8, "2. Additional Comments / Short Description", 0, "", "L");

	$content = $description;
	$content = str_replace("\r\n\r\n", "", $content);
	
	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFillColor(248,251,167);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(185, 40, "", 1, "", "L", false);
	$pdf->SetXY($x+1,$y+1);
	$pdf->MultiCell(183,0, $content, 0, 'L', false, 1, '', '', true, 0, false, true, '', 'T');
	

	

	$y = $y+40;
	$pdf->SetXY($x-1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(102, 8, "3. Signatures:", 0, "", "L");

	

	//title bar 1
	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetFillColor(224,224,224);
	$pdf->Cell(185,6, "Brand", 1, "", "L", true);

	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 10, "CEO " . "\r\n" . $approval_name2, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 10, "CFO" . "\r\n" . $approval_name7, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');

	
	$y = $y+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 10, "VP Sales " . "\r\n" . $approval_name3, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 10, "Int. Retail Manager" . "\r\n" . $approval_name4, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');



	$y = $y+15;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetFillColor(224,224,224);
	$pdf->Cell(185,6, "Country", 1, "", "L", true);

	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 10, "Brand Manager " . "\r\n" . $approval_name13, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');

	$y = $y+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 10, "Head of Controlling " . "\r\n" . $approval_name12, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');



	$y = $y+10;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 10, "Country Manager " . "\r\n" . $approval_name11, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 10, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 10, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 10, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');
	
?>