<?php
/********************************************************************

    distribution_summary_pdf.php

    Print Distribution Summary

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-09-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();


$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/SetaPDF/Autoload.php');

// Create and setup PDF document
$PDFmerger_was_used = false;

class MYPDF extends TCPDF
{
	
	

}

$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);

$pdf->SetMargins(10, 23, 10);
$pdf->setPrintHeader(false);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');


if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Distribution Summary");
}
elseif($form_type == "AF")
{
	$pdf->SetTitle("Distribution Summary for LNR");
}
else
{
	$pdf->SetTitle("Distribution Summary for LNR");
}
$pdf->SetAuthor("Tissot Retailnet");
$pdf->SetDisplayMode(100);

$pdf->Open();

include("distribution_summary_pdf_detail.php");



$file_name = "distribution_summary_" . str_replace(" ", "_", $project["project_number"]) . "_" . date("Y-m-d") . ".pdf";

$pdf->Output($file_name, 'I');


?>