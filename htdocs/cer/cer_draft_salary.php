<?php
/********************************************************************

    cer_draft_salary.php

    Add/edit New salary position

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-02-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-02-16
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require "include/get_draft_functions.php";

check_access("has_access_to_cer_drafts");

/********************************************************************
    prepare all data needed
*********************************************************************/
$currency = get_draft_currency(param("did"));
$basicdata = get_draft_basicdata(param("did"));

//get user's country
$sql = 'select address_country from users ' . 
       'left join addresses on address_id = user_address ' .
	   'where user_id = ' . user_id();

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$country = $row['address_country'];
}


// create list values for the years
$years = array();
$first_year = $basicdata["cer_basicdata_firstyear"];
$first_month = $basicdata["cer_basicdata_firstmonth"];
$last_year = $basicdata["cer_basicdata_lastyear"];

for($i = $first_year;$i <= $last_year;$i++)
{
	$years[$i] = $i;
}

// create list values for the years
$months = array();

for($i=1;$i<13;$i++)
{
	$months[$i] = $i;
}


for($i=1;$i<13;$i++)
{
	$numbers[$i] = $i .  ' times';
}



/********************************************************************
    build form
*********************************************************************/

$form = new Form("cer_draft_salaries", "CER Salaries");

$form->add_hidden("did", param("did"));
$form->add_hidden("cer_salary_draft_id", param("did"));

$form->add_section("Salaries in " . $currency["symbol"]);


$form->add_list("cer_salary_staff_type", "Function*", "select cer_staff_type_id, cer_staff_type_name from cer_staff_types", NOTNULL);
$form->add_list("cer_salary_year_starting", "Starting Year*", $years, NOTNULL);
$form->add_list("cer_salary_month_starting", "Starting Month*", $months, NOTNULL);
$form->add_edit("cer_salary_headcount_percent", "Headcounts Fulltime Equivalent in %*", NOTNULL, "", TYPE_DECIMAL, 7, 2);

$form->add_comment("Please enter all figures based on a 100% occupation and a full year.");
$form->add_edit("cer_salary_fixed_salary", "Fixed Salary in " . $currency["symbol"] ."*", NOTNULL, "", TYPE_INT, 12);
//$form->add_edit("cer_salary_bonus", "Bonus in " . $currency["symbol"], "", "", TYPE_INT, 12);
$form->add_edit("cer_salary_other", "Other Salary in " . $currency["symbol"], "", "", TYPE_INT, 12);

$form->add_edit("cer_salary_social_charges_percent", "Social Charges in %", "", "", TYPE_DECIMAL, 5, 2);
//$form->add_edit("cer_salary_social_charges", "Social Charges", "", "", TYPE_DECIMAL, 12, 2);
//$form->add_label("cer_salary_social_charges", "Social Charges in " . $currency["symbol"]);
//$form->add_label("cer_salary_total", "Total 100% per year in " . $currency["symbol"]);

$form->add_comment("Please indicate if a bonus will be payed.");
$form->add_checkbox("cer_salary_bonus_percent_on_net_sales", "on net sales", 0, 0, "Base (standard is gross sales)");
$form->add_edit("cer_salary_bonus_percent", "Bonus in % for a FTE of 100%", "", "", TYPE_DECIMAL, 6,2);

$form->add_section("Bulk Operation");
$form->add_list("number", "Duplicate the above human resource", $numbers);

$form->add_button("save_form", "Save");
$form->add_button("back", "Back");
$form->add_button("delete", "Delete", "", OPTIONAL);

$form->populate();
$form->process();

if($form->button("save_form"))
{
	if($form->validate())
	{
		//$total = $form->value("cer_salary_fixed_salary") + $form->value("cer_salary_bonus") + $form->value("cer_salary_other");

		//$total = $form->value("cer_salary_fixed_salary") + $form->value("cer_salary_other");

		//$social_charges = $form->value("cer_salary_social_charges_percent") * $total/100;
		//$form->value("cer_salary_social_charges", $social_charges);

		//$total = $form->value("cer_salary_fixed_salary") + $form->value("cer_salary_bonus") + $form->value("cer_salary_other") + $social_charges;

		//$total = $form->value("cer_salary_fixed_salary") + $form->value("cer_salary_other") + $social_charges;

		//$form->value("cer_salary_total", $total);

		$form->save();


		if($form->value('number') > 0) {
		 
			for($i=1;$i<=$form->value('number');$i++) {
				param('id', 0);
				$form->save();
			}
		
		}

		calculate_forcasted_salaries(param("did"), $years, $country);

		$form->message("The data has been saved.");

		$link = "cer_draft_salaries.php?did=" . param("did");
		redirect($link);
	}
}
elseif($form->button("back"))
{
	$link = "cer_draft_salaries.php?did=" . param("did");
	redirect($link);
}
elseif($form->button("delete"))
{
	$sql = "delete from cer_draft_salaries where cer_salary_id = " . id();
	$result = mysql_query($sql) or dberror($sql);
	$link = "cer_draft_salaries.php?did=" . param("did");
	redirect($link);
}

$page = new Page("cer_drafts");
require "include/draft_page_actions.php";
$page->header();
$page->title(id() ? $basicdata['cer_basicdata_title'] . ": Edit Human Resource" : $basicdata['cer_basicdata_title'] . ": Add Human Resource");

require_once("include/tabs_draft2016.php");
$form->render();
$page->footer();

?>