<?php
/********************************************************************

    cer_inr02_pdf_debug.php

    Print Debuggung Information PDF for CER Business Plan (Form IN-R02)

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";

require "../shared/func_posindex.php";


require "include/get_functions.php";
require "include/get_project.php";
require "include/financial_functions.php";

check_access("has_access_to_cer");

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');


// get all data needed from project
$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
       "left join users on user_id = project_retail_coordinator " . 
	   "where project_id = " . param("pid");


$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	$legal_entity_name = $row["address_company"];
	
	$address_country = $row["address_country"];
	
	$project_name = $row["order_shop_address_company"]. ", " . $row["order_shop_address_place"] . ", " . $row["country_name"];

	$pos_country = $row["country_name"];
	$pos_address = $row["order_shop_address_address"];
	$pos_city = $row["order_shop_address_place"];

	$project_manager = $row["user_name"] . " " . $row["user_firstname"];
	
	$project_kind = $row["projectkind_name"];

	$legal_entity = $row["address_company"];
	

	$date_request = to_system_date($row["order_date"]);

	$order_number = $row["project_order"];

	$sales_surface = round($row["project_cost_sqms"], 0);

	$planned_opening_date = to_system_date($row["project_real_opening_date"]);
	

}

$client_currency = get_cer_currency(param("pid"));
$currency_symbol = $client_currency["symbol"];


if($use_old_cer_forms_before_2013 == true)
{
	require_once("include/in_financial_data_before2013.php");
}
else
{
	require_once("include/in_financial_data.php");
}


$stock_data_present = false;
foreach($years as $key=>$year)
{
	if($debugvar_stock[$year] > 0)
	{
		$stock_data_present = true;

	}
}


//set pdf parameters

$margin_top = 8;
$margin_left = 12;
$y = $margin_top;
$standard_y = 3.5;

// Create and setup PDF document

$pdf = new TCPDF("L", "mm", "A3", true, 'UTF-8', false);
$pdf->SetMargins(12, 8);
$pdf->setPrintHeader(false);

$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');

$pdf->Open();
$pdf->SetTitle("Retail Busines Plan Overview Form IN-R02");
$pdf->SetAuthor("Tissot Retailnet");
$pdf->SetDisplayMode(100);
$pdf->AddPage();
$pdf->SetAutoPageBreak(false, 0);

//page data

// draw page box
$pdf->SetXY($margin_left-2,$margin_top);
$pdf->Cell(406, 280, "", 1);
// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(255, 6, "Debug Output for the Retail Business Plan Overview for Project " . $project["project_number"] . ": " . $project_name . ", Page 1", 0, "", "L");


	//investment
	
	$x1 = $margin_left;
	$x2 = $margin_left+50;
	$x3 = $margin_left+70;
	

	$y = $y + 14;
	$y2 = $y + $standard_y;   // for column2
	$y3 = $y + $standard_y;   // for column3
	$y4 = $y + $standard_y;   // for column4
	$y5 = $y + $standard_y;   // for column5
	$y6 = $y + $standard_y;   // for column6

	foreach($fixed_assets as $key=>$itype)
	{
		if(array_key_exists($itype, $amounts))
		{
			if(!$amounts[$itype]){$amounts[$itype] = "-";}
			if(!$depryears[$itype]){$depryears[$itype] = "-";}

			$y = $y + $standard_y;
			$pdf->SetXY($x1,$y);
			$pdf->SetFont("arialn", "", 8);
			$pdf->Cell(63, 4, $investment_names[$itype], 0, "", "L");

			$pdf->SetXY($x2,$y);
			$pdf->SetFont("arialn", "", 8);
			
			if($amounts[$itype] != 0)
			{
				if($itype == 11) {
					$amounts[$itype] = $amounts[$itype] - $merchandising_total -$transportation_total;
				}
				$pdf->Cell(20, 4, number_format($amounts[$itype],2, ".", "'"), 0, "", "R");
				$pdf->Cell(5, 4, $depryears[$itype], 0, "", "R");
			}
			else
			{
				$pdf->Cell(20, 4, "-", 0, "", "R");
			}
		}
		else
		{
			$y = $y + $standard_y;
		}
	}


	$y = $y + $standard_y;
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "B", 8);
	
	$pdf->Cell(20, 4, "Gross Total Fixed Assets", 0, "", "L");
	
	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "B", 8);
	
	$pdf->Cell(20, 4, number_format(($investment_total),2, ".", "'"), 0, "", "R");
	
	
	
	$y = $y + $standard_y;
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if(array_key_exists(19, $investment_names))
	{
		$pdf->Cell(20, 3.5, $investment_names[19], 0, "", "L");
	}
	else
	{
		$pdf->Cell(20, 4, "", 0, "", "L");
	}

	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if(array_key_exists(19, $amounts))
	{
		if($amounts[19] > 0)
		{
			$pdf->Cell(20, 4, number_format(-1*$amounts[19],2, ".", "'"), 0, "", "R");
			$pdf->Cell(5, 4, $depryears[19], 0, "", "R");
		}
		else
		{
			$pdf->Cell(20, 4, "-", 0, "", "R");
		}
	}
	else
	{
		$pdf->Cell(20, 4, "", 0, "", "R");
	}


	$y = $y + $standard_y;
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "B", 8);

	$pdf->Cell(20, 4, "Net Total Fixed Assets", 0, "", "L");
	
	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "B", 8);
	
	$pdf->Cell(20, 4, number_format(($investment_total + $landloard_contribution),2, ".", "'"), 0, "", "R");
	
	
	$y = $y + $standard_y;
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "", 8);

	$pdf->Cell(20, 4, $intagible_name, 0, "", "L");

	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if($intagible_amount != 0)
	{
		$pdf->Cell(20, 4, number_format($intagible_amount,0, ".", "'"), 0, "", "R");
		if(array_key_exists(15, $depryears))
		{
			$pdf->Cell(5, 4, $depryears[15], 0, "", "R");
		}
		else
		{
			$pdf->Cell(5, 4, "0", 0, "", "R");
		}
	}
	else
	{
		$pdf->Cell(20, 4, "-", 0, "", "R");
	}

	
	$y = $y + $standard_y;
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if(array_key_exists(9, $investment_names))
	{
		$pdf->Cell(20, 3.5, $investment_names[9], 0, "", "L");
	}
	else
	{
		$pdf->Cell(20, 4, "", 0, "", "L");
	}

	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if(array_key_exists(9, $amounts))
	{
		if($amounts[9] > 0)
		{
			$pdf->Cell(20, 4, number_format($amounts[9],2, ".", "'"), 0, "", "R");
			$pdf->Cell(5, 4, $depryears[9], 0, "", "R");
		}
		else
		{
			$pdf->Cell(20, 4, "-", 0, "", "R");
		}
	}
	else
	{
		$pdf->Cell(20, 4, "", 0, "", "R");
	}

	
	$y = $y + $standard_y;
	$pdf->SetXY($x1,$y);
	$pdf->SetFont("arialn", "", 8);
	
	if(array_key_exists(13, $investment_names))
	{
		$pdf->Cell(20, 4, $investment_names[13], 0, "", "L");
	}
	else
	{
		$pdf->Cell(20, 4, "", 0, "", "L");
	}

	
	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "", 8);
	
	
	if(array_key_exists(13, $amounts))
	{
		if($amounts[13] > 0)
		{
			$pdf->Cell(20, 4, number_format($amounts[13],2, ".", "'"), 0, "", "R");
			$pdf->Cell(5, 4, $depryears[13], 0, "", "R");
		}
		else
		{
			$pdf->Cell(20, 4, "-", 0, "", "R");
		}
	}
	else
	{
		$pdf->Cell(20, 4, "", 0, "", "R");
	}

	$y = $y+ $standard_y;
	$pdf->SetXY($margin_left,$y);
	
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(50, 4, "Total Project costs", 0, "", "L");

	$pdf->SetXY($x2,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(20, 4, number_format($cer_totals,2, ".", "'"), 0, "", "R");


	//parameter
	$x1c2 = 93;
	$x2c2 = 108;
	
	
	$pdf->SetXY($x1c2,$y2);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 4, "Customer Service", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_customer_service"], 2, ".", "'") . "%", 0, "", "R");
	
	$y2 = $y2+ $standard_y;

	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Sales Reduction", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_sales_reduction"],2, ".", "'") . "%", 0, "", "R");

	$y2 = $y2+ $standard_y;

	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Cost of Watches", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_cost_watches"],2, ".", "'") . "%", 0, "", "R");

	$y2 = $y2+ $standard_y;

	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Cost of Watch Straps", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_cost_jewellery"],2, ".", "'") . "%", 0, "", "R");

	$y2 = $y2+ $standard_y;


	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Cost of Services", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_cost_service"],2, ".", "'") . "%", 0, "", "R");

	$y2 = $y2+ $standard_y;

	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Wholsale Margin", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_wholesale_margin"],2, ".", "'") . "%", 0, "", "R");

	$y2 = $y2+ $standard_y;

	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Discount Rate", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format(100*$discount_rate,2, ".", "'") . "%", 0, "", "R");


	$y2 = $y2+ $standard_y;

	$pdf->SetXY($x1c2,$y2);
	$pdf->Cell(50, 4, "Fixed Salary Growth", 0, "", "L");

	$pdf->SetXY($x2c2,$y2);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_salary_growth"],2, ".", "'") . "%", 0, "", "R");


	
	//liquidation parameters
	$x1c4 = 130;
	$x2c4 = 160;

	$pdf->SetXY($x1c4,$y4);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 4, "Liquidation Revenue on Keymoney", 0, "", "L");

	$pdf->SetXY($x2c4,$y4);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_bascidata_liquidation_keymoney"],2, ".", "'") . "%", 0, "", "R");

	$y4 = $y4+ $standard_y;

	$pdf->SetXY($x1c4,$y4);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 4, "Liquidation Revenue on Deposit", 0, "", "L");

	$pdf->SetXY($x2c4,$y4);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_liquidation_deposit"],2, ".", "'") . "%", 0, "", "R");

	$y4 = $y4+ $standard_y;

	$pdf->SetXY($x1c4,$y4);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 4, "Liquidation Revenue on Stock", 0, "", "L");

	$pdf->SetXY($x2c4,$y4);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_bascidata_liquidation_stock"],2, ".", "'") . "%", 0, "", "R");

	$y4 = $y4+ $standard_y;

	$pdf->SetXY($x1c4,$y4);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 4, "Liquidation Revenue on Staff", 0, "", "L");

	$pdf->SetXY($x2c4,$y4);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_bascicdate_liquidation_staff"],2, ".", "'") . "%", 0, "", "R");

	



	//residual values
	$y4 = $y4+ $standard_y;
	$y4 = $y4+ $standard_y;
	
	$pdf->SetXY($x1c4,$y4);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(50, 4, "Residual Value of Fixed Assets", 0, "", "L");

	$pdf->SetXY($x2c4,$y4);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_residual_value"],2, ".", "'"), 0, "", "R");

	$y4 = $y4+ $standard_y;

	$pdf->SetXY($x1c4,$y4);
	$pdf->SetFont("arialn", "", 8);
	
	$pdf->Cell(50, 4, "Residual Value of Keymoney", 0, "", "L");
	$pdf->SetXY($x2c4,$y4);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_residualkeymoney_value"],2, ".", "'"), 0, "", "R");

	$y4 = $y4+ $standard_y;

	/*
	$pdf->SetXY($x1c4,$y4);
	$pdf->Cell(50, 4, "Recoverable Keymoney", 0, "", "L");
	$pdf->SetXY($x2c4,$y4);
	$pdf->Cell(20, 4, number_format($cer_basicdata["cer_basicdata_recoverable_keymoney"],2, ".", "'"), 0, "", "R");
	*/


	//sales values
	$captions = array();
	
	$captions[] = "Watches Units";
	$captions[] = "Watch Straps Units";
	$captions[] = "Watches av. Price";
	$captions[] = "Watch Straps Av. Price";
	$captions[] = "Sales Value Watches";
	$captions[] = "Sales Value Watch Straps";
	$captions[] = "Sales Value Services";
	$captions[] = "Total Gross Sales";
	$captions[] = "Sales Reduction";
	$captions[] = "Total Net Sales";
	$captions[] = "";
	$captions[] = "Cost of Watches Sold";
	$captions[] = "Cost of Watch Straps Sold";
	$captions[] = "Cost of Services Sold";
	$captions[] = "Cost of Products Sold";
	$captions[] = "Total Gross Margin";
	$captions[] = "";
	$captions[] = "Marketing Expenses";
	$captions[] = "Marketing Contribution";
	$captions[] = "";
	foreach($expenses as $expense_name=>$value)
	{
		$captions[] = $expense_name;
	}
	
	$captions[] = "Depreciation on Investment (incl. dismantling)";
	
	foreach($fixed_assets2 as $key=>$itype)
	{
		if(isset($debug_depreciation[$itype]))
		{
			$captions[] = "  " . $investment_names[$itype];
		}
	}
	
	
	$captions[] = "Total Depreciation on Investment";
	$captions[] = "- of which  on residual value of fixed assets";
	$captions[] = "";
	$captions[] = "Depreciation on Goodwill/Keymoney";

	$captions[] = "";

	$captions[] = "Total Expenses";

	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	$y_start = $y;
	$standard_xoffset = 17;

	$pdf->SetFont("arialn", "", 8);
	foreach($captions as $key=>$caption)
	{
		$pdf->SetXY($x1,$y);
		$pdf->Cell(50, 4, $caption, 0, "", "L");
		$y = $y+ $standard_y;
	}
	$pdf->SetFont("arialn", "B", 8);
	$xi = $standard_xoffset;
	$y = $y_start - $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, $year, 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$pdf->SetFont("arialn", "", 8);
	$xi = $standard_xoffset;
	$y = $y_start;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  $sales_units_watches_values[$year], 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  $sales_units_jewellery_values[$year], 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  $average_price_watches_values[$year], 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  $average_price_jewellery_values[$year], 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($sales_watches_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($sales_jewellery_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($sales_customer_service_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_gross_sales_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($sales_reduction_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_net_sales_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	// costs of products sold
	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  "", 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		if(array_key_exists($year, $cost_of_production_values['cost_watches_values']))
		{
			$pdf->Cell(12, 4,  number_format($cost_of_production_values['cost_watches_values'][$year],2, ".", "'"), 0, "", "R");
		}
		else
		{
			$pdf->Cell(12, 4,  '0', 0, "", "R");
		}
			
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		if(array_key_exists($year, $cost_of_production_values['cost_sales_jewellery_values']))
		{
			$pdf->Cell(12, 4,  number_format($cost_of_production_values['cost_sales_jewellery_values'][$year],2, ".", "'"), 0, "", "R");
		}
		else
		{
			$pdf->Cell(12, 4,  '0', 0, "", "R");
		}
		$xi = $xi + $standard_xoffset;
	}
	
	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		if(array_key_exists($year, $cost_of_production_values['cost_sales_customer_service_values']))
		{
			$pdf->Cell(12, 4,  number_format($cost_of_production_values['cost_sales_customer_service_values'][$year],2, ".", "'"), 0, "", "R");
		}
		else
		{
			$pdf->Cell(12, 4,  '0' , 0, "", "R");
		}
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$cost_of_products_sold = 0;
		if(array_key_exists($year, $cost_of_production_values['cost_watches_values']))
		{
			$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_values['cost_watches_values'][$year];
		}
		if(array_key_exists($year, $cost_of_production_values['cost_sales_jewellery_values']))
		{
			$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_values['cost_sales_jewellery_values'][$year];
		}
		
		if(array_key_exists($year, $cost_of_production_values['cost_sales_customer_service_values']))
		{
			$cost_of_products_sold = $cost_of_products_sold + $cost_of_production_values['cost_sales_customer_service_values'][$year];
		}

		
		$pdf->SetXY($x2+$xi,$y);
		//$pdf->Cell(12, 4,  number_format($material_of_products_values[$year],2, ".", "'"), 0, "", "R");
		$pdf->Cell(12, 4,  number_format($cost_of_products_sold,2, ".", "'"), 0, "", "R");

		
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_gross_margin_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	// marketing expenses
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, "", 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($marketing_expenses_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($interco_contribution_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	//expenses
	$y = $y+ $standard_y;
	foreach($expenses as $name=>$values)
	{
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($values as $year=>$value)
		{
			$pdf->SetXY($x2+$xi,$y);
			
			
			if($cer_basicdata["cer_basicdata_calculate_rent_avg"] == 1 and 
			   ($expense_types[$year][$name] == 2 
				or $expense_types[$year][$name] == 3 
				or $expense_types[$year][$name] == 16
				or $expense_types[$year][$name] == 18
				or $expense_types[$year][$name] == 19
				or $expense_types[$year][$name] == 20)) // rental cost
			{
				if($expense_types[$year][$name] == 2) // fixed rent
				{
					$pdf->Cell(12, 4,  number_format($fixedrents_values[$year],2, ".", "'"), 0, "", "R");
				}
				elseif($expense_types[$year][$name] == 16) // turnoverbased
				{
					$pdf->Cell(12, 4,  number_format($turnoverbasedrents_values[$year],2, ".", "'"), 0, "", "R");
				}
				elseif($expense_types[$year][$name] == 3) // aditional renal cost
				{
					$pdf->Cell(12, 4,  number_format($additionalrents_values[$year],2, ".", "'"), 0, "", "R");
				}
				else
				{
					$pdf->Cell(12, 4,  number_format($value,2, ".", "'"), 0, "", "R");
				}
			}
			else
			{
				$pdf->Cell(12, 4,  number_format($value,2, ".", "'"), 0, "", "R");
			}
			$xi = $xi + $standard_xoffset;
		}
	}

	//Depreciation
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($fixed_assets2 as $key=>$itype)
	{
		if(isset($debug_depreciation[$itype]))
		{
			if(!$amounts[$itype]){$amounts[$itype] = "-";}
			if(!$depryears[$itype]){$depryears[$itype] = "-";}

			$xi = $standard_xoffset;
			foreach($years as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  number_format($debug_depreciation[$itype][$year],2, ".", "'"), 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}
			$y = $y+ $standard_y;
		}
	}

	
	
	
	
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($depreciation[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($debug_depreciation_residual_fixed_assets[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		if(array_key_exists(15, $debug_depreciation) and array_key_exists(17, $debug_depreciation))
		{
			$tmp = $debug_depreciation[15][$year] + $debug_depreciation[17][$year];
		}
		elseif(array_key_exists(15, $debug_depreciation))
		{
			$tmp = $debug_depreciation[15][$year];
		}
		elseif(array_key_exists(17, $debug_depreciation))
		{
			$tmp = $debug_depreciation[17][$year];
		}
		else
	    {
			$tmp = "0.00";
		}

		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($tmp,2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_indirect_expenses_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	

//page 2
$pdf->AddPage();
// draw page box
$pdf->SetXY($margin_left-2,$margin_top);
$pdf->Cell(406, 270, "", 1);
// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(255, 6, "Debug Output for the Retail Business Plan Overview for: " . $project_name . ", Page 2", 0, "", "L");

	$captions = array();
	$captions[] = "Gross Margin";
	$captions[] = "Marketing Investment";
	$captions[] = "Marketing Contribution";
	$captions[] = "Contribution II";
	$captions[] = "Expenses";
	$captions[] = "Operating Income without Wholsale Margin";
	$captions[] = "";
	$captions[] = "Material Costs of Products Sold";
	$captions[] = "Operating Income incl. Wholsale Margin";
	$captions[] = "";

	if($stock_data_present == true)
	{
		$captions[] = "Liabilities";
		$captions[] = "Liabilities Change";
		$captions[] = "Stock";
		$captions[] = "Stock Change";
		$captions[] = "";
	}

	
	$captions[] = "Operating Income without Wholsale Margin";
	$captions[] = "Depreciation on Goodwill/Keymoney";
	$captions[] = "Depreciation on Investment";
	$captions[] = "Variation Working Capital";
	$captions[] = "Initial Investment/Disinvestment";
	$captions[] = "Total Cash Flow";
	$captions[] = "";
	$captions[] = "Financial Expenses";
	$captions[] = "Income Taxes";
	$captions[] = "Total FCF";
	$captions[] = "Cash Flow";
	$captions[] = "";

	$captions[] = "Total Cash Flow Wholesale";
	$captions[] = "";

	$captions[] = "Net Present Value Retail";
	$captions[] = "Discounted Cash Flow ROI in %";
	$captions[] = "Pay Back Period";
	
	$captions[] = "";
	$captions[] = "Net Present Value Wholesale";
	$captions[] = "Discounted Cash Flow ROI in %";
	$captions[] = "Pay Back Period";
	
	$captions[] = "";
	$captions[] = "";
	$captions[] = "Pay Back Cummulated Retail Values";
	$captions[] = "Pay Back Period Retail Values";
	$captions[] = "";
	$captions[] = "Pay Back Cummulated Whole Sale Values";
	$captions[] = "Pay Back Period Whole Sale Values";

	$captions[] = "";
	$captions[] = "Interest Rate";
	$captions[] = "Inflation Rate";
	$captions[] = "Stock in Months";
	$captions[] = "Payment Terms in Months";


	$x1 = $margin_left;
	$x2 = $margin_left+32;
	$x3 = $margin_left+52;

	
	$y = $margin_top + 6;
	$y2 = $y + $standard_y;   // for column2
	
	$pdf->SetFont("arialn", "B", 8);
	$xi = $standard_xoffset;

	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, $year, 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	
	$y = $y + $standard_y;
	$y_start = $y;
	$pdf->SetFont("arialn", "", 8);
	foreach($captions as $key=>$caption)
	{
		$pdf->SetXY($x1,$y);
		$pdf->Cell(50, 4, $caption, 0, "", "L");
		$y = $y+ $standard_y;
	}
	
	$y = $y_start - $standard_y;
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_gross_margin_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	// marketing expenses
	$xi = $standard_xoffset;
	//$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, "", 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($marketing_expenses_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($interco_contribution_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	//contribution II
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$tmp9 =  $total_gross_margin_values[$year] - $marketing_expenses_values[$year] + $interco_contribution_values[$year];
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($tmp9,2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_indirect_expenses_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($operating_income01_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$tmp11 = $material_of_products_values[$year]; // Stat. Wholesale Margin
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($tmp11,2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($operating_income02_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	if($stock_data_present == true) 
	{
		$y = $y+ $standard_y;
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($debugvar_liabilities[$year],2, ".", "'"), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}

		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($debugvar_liabilities_change[$year],2, ".", "'"), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}

		
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($debugvar_stock[$year],2, ".", "'"), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}
		
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($years as $key=>$year)
		{
			$pdf->SetXY($x2+$xi,$y);
			$pdf->Cell(12, 4,  number_format($debugvar_stock_change[$year],2, ".", "'"), 0, "", "R");
			$xi = $xi + $standard_xoffset;
		}

		
	}

	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($operating_income01_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		if(array_key_exists(15, $debug_depreciation) and array_key_exists(17, $debug_depreciation))
		{
			$tmp = $debug_depreciation[15][$year] + $debug_depreciation[17][$year];
		}
		elseif(array_key_exists(15, $debug_depreciation))
		{
			$tmp = $debug_depreciation[15][$year];
		}
		elseif(array_key_exists(17, $debug_depreciation))
		{
			$tmp = $debug_depreciation[17][$year];
		}
		else
		{
			$tmp = "0.00";
		}
		

		$pdf->SetXY($x2+$xi,$y);
		//$pdf->Cell(12, 4,  number_format($tmp,2), 0, "", "R");
		$pdf->Cell(12, 4,  number_format($prepayed_rents[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($depreciation[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($debugvar_working_capital[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($debug_initial_investment[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_cash_flow_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($debugvar_finacial_expenses[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($income_taxes_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_cash_flow_fcf_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($cash_flow_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($cash_flow_whole_sale_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4,  number_format($net_present_value_retail,2, ".", "'"), 0, "", "R");

	$formula = "NPV(" . $discount_rate . ", [";
	foreach($cfs as $key=>$value)
	{
		$formula .= number_format($value,2, ".", "'") . ", ";
	}
	$formula = substr($formula, 0, strlen($formula) - 2) . "]) - " . number_format($_initial_investment, 2, ".", "'");

	$xi = $xi + 14;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4, $formula, 0, "", "L");

	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$pdf->SetXY($x2+$xi,$y);
	if(abs($discounted_cash_flow_retail) < 10000)
	{
		$pdf->Cell(12, 4,  $discounted_cash_flow_retail, 0, "", "R");
	}


	$formula = "IRR([";
	foreach($cfs1 as $key=>$value)
	{
		$formula .= number_format($value, 2, ".", "'") . ", ";
	}
	$formula = substr($formula, 0, strlen($formula) - 2) . "], -0.1)";

	$xi = $xi + 14;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4, $formula, 0, "", "L");


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4,  number_format($pay_back_period_retail,2, ".", "'"), 0, "", "R");

	/*
	$formula = "NPV(" . $discount_rate . ", [";
	foreach($cfs2 as $key=>$value)
	{
		$formula .= number_format($value, 2, ".", "'") . ", ";
	}
	$formula = substr($formula, 0, strlen($formula) - 2) . "]) - " . number_format($_initial_investment, 2, ".", "'");

	$xi = $xi + 14;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4, $formula, 0, "", "L");
	*/
	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4,  number_format($net_present_value_wholesale,2, ".", "'"), 0, "", "R");

	$formula = "NPV(" . $discount_rate . ", [";
	foreach($cfs2 as $key=>$value)
	{
		$formula .= number_format($value, 2, ".", "'") . ", ";
	}
	$formula = substr($formula, 0, strlen($formula) - 2) . "]) - " . number_format($_initial_investment, 2, ".", "'");

	$xi = $xi + 14;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4, $formula, 0, "", "L");


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$pdf->SetXY($x2+$xi,$y);
	if(abs($discounted_cash_flow_wholesale) < 10000)
	{
		$pdf->Cell(12, 4, $discounted_cash_flow_wholesale, 0, "", "R");
	}

	$formula = "IRR([";
	foreach($cfs3 as $key=>$value)
	{
		$formula .= number_format($value, 2, ".", "'") . ", ";
	}
	$formula = substr($formula, 0, strlen($formula) - 2) . "], -0.1)";

	$xi = $xi + 14;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4, $formula, 0, "", "L");


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4,  number_format($pay_back_period_wholesale,2, ".", "'"), 0, "", "R");

	/*
	$formula = "NPV(" . $discount_rate . ", [";
	foreach($cfs2 as $key=>$value)
	{
		$formula .= number_format($value, 2, ".", "'") . ", ";
	}
	$formula = substr($formula, 0, strlen($formula) - 2) . "]) - " . number_format($_initial_investment, 2, ".", "'");

	$xi = $xi + 14;
	$pdf->SetXY($x2+$xi,$y);
	$pdf->Cell(12, 4, $formula, 0, "", "L");
    */
	


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($pay_back_cummunlated_retail_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($pay_back_period_retail_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($pay_back_cummunlated_wholesale_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($pay_back_period_wholesale_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, number_format($cer_basicdata_interest_rates[$year],2, ".", "'") . "%", 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, number_format($cer_basicdata_inflation_rates[$year],2, ".", "'") . "%", 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, number_format($stock_data[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, number_format($payment_terms[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}



if($apply_80_percent_scenario == 1)
{
	$standard_xoffset = $standard_xoffset + 2;
	//page 3
	$pdf->AddPage();
	// draw page box
	$pdf->SetXY($margin_left-2,$margin_top);
	$pdf->Cell(406, 270, "", 1);
	// Title first line
	$pdf->SetXY($margin_left,$margin_top);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(255, 6, "Debug Output for the Retail Business Plan Overview for: " . $project_name . ", Page 2", 0, "", "L");

	$captions = array();
	$captions[] = "Total Gross Sales 80%";
	$captions[] = "Sales Reduction 80%";
	$captions[] = "Total Net Sales 80%";
	$captions[] = "Material Costs of Products Sold";
	$captions[] = "Gross Margin 80%";
	$captions[] = "";
	$captions[] = "Marketing Investment";
	$captions[] = "Marketing Contribution";
	$captions[] = "Contribution II";
	$captions[] = "";

	foreach($expenses as $expense_name=>$value)
	{
		$captions[] = $expense_name;
	}
	$captions[] = "Depreciation on Investment";

	foreach($fixed_assets as $key=>$itype)
	{
		if(isset($debug_depreciation[$itype]))
		{
			$captions[] = "  " . $investment_names[$itype];
		}
	}
	
	
	$captions[] = "Total Depreciation on Investment";
	$captions[] = "- of which  on residual value of fixed assets";
	$captions[] = "";
	$captions[] = "Depreciation on Goodwill/Keymoney";

	$captions[] = "";

	$captions[] = "Total Expenses";

	$captions[] = "";


	$captions[] = "Operating Income without Wholsale Margin";
	$captions[] = "Operating Income incl. WSM (scenario 80%)";


	

	$x1 = $margin_left;
	$x2 = $margin_left+32;
	$x3 = $margin_left+52;

	
	$y = $margin_top + 6;
	$y2 = $y + $standard_y;   // for column2
	
	$pdf->SetFont("arialn", "B", 8);
	$xi = $standard_xoffset;

	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, $year, 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	
	$y = $y + $standard_y;
	$y_start = $y;
	$pdf->SetFont("arialn", "", 8);
	foreach($captions as $key=>$caption)
	{
		$pdf->SetXY($x1,$y);
		$pdf->Cell(50, 4, $caption, 0, "", "L");
		$y = $y+ $standard_y;
	}
	
	
	$y = $y_start - $standard_y;
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_gross_sales_80_percent_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($sales_reduction_80_percent_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_net_sales_80_percent_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}



	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format(0.8*$material_of_products_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_gross_margin_80_percent_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;


	// marketing expenses
	$xi = $standard_xoffset;
	//$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4, "", 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($marketing_expenses_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($interco_contribution_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	//contribution II
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$tmp9 =  $total_gross_margin_80_percent_values[$year] - $marketing_expenses_values[$year] + $interco_contribution_values[$year];
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($tmp9,2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	//expenses
	$y = $y+ $standard_y;
	foreach($expenses as $name=>$values)
	{
		$xi = $standard_xoffset;
		$y = $y+ $standard_y;
		foreach($values as $year=>$value)
		{
			$pdf->SetXY($x2+$xi,$y);
			
			if($cer_basicdata["cer_basicdata_calculate_rent_avg"] == 1 and 
			   ($expense_types[$year][$name] == 2 
				or $expense_types[$year][$name] == 3 
				or $expense_types[$year][$name] == 16
				or $expense_types[$year][$name] == 18
				or $expense_types[$year][$name] == 19
				or $expense_types[$year][$name] == 20)) // rental cost
			{
				if($expense_types[$year][$name] == 2) // fixed rent
				{
					$pdf->Cell(12, 4,  number_format($fixedrents_values[$year],2, ".", "'"), 0, "", "R");
				}
				elseif($expense_types[$year][$name] == 16) // turnoverbased
				{
					$pdf->Cell(12, 4,  number_format($turnoverbasedrents_80_percent_values[$year],2, ".", "'"), 0, "", "R");
				}
				elseif($expense_types[$year][$name] == 18) // taxes
				{
					$pdf->Cell(12, 4,  number_format($taxes_80_percent_values[$year],2, ".", "'"), 0, "", "R");
				}
				elseif($expense_types[$year][$name] == 19) // passenger index
				{
					$pdf->Cell(12, 4,  number_format($passenger_index_80_percent_values[$year],2, ".", "'"), 0, "", "R");
				}
				elseif($expense_types[$year][$name] == 3) // aditional renal cost
				{
					$pdf->Cell(12, 4,  number_format($additionalrents_values[$year],2, ".", "'"), 0, "", "R");
				}
				else
				{
					$pdf->Cell(12, 4,  number_format($value,2, ".", "'"), 0, "", "R");
				}
			}
			else
			{
				if($expense_types[$year][$name] == 2 
					or $expense_types[$year][$name] == 3 
					or $expense_types[$year][$name] == 16
					or $expense_types[$year][$name] == 18
					or $expense_types[$year][$name] == 19
					or $expense_types[$year][$name] == 20) // rental cost
				{
					if($expense_types[$year][$name] == 2) // fixed rent
					{
						$pdf->Cell(12, 4,  number_format($fixedrents_values[$year],2, ".", "'"), 0, "", "R");
					}
					elseif($expense_types[$year][$name] == 16) // turnoverbased
					{
						$pdf->Cell(12, 4,  number_format($turnoverbasedrents_80_percent_values[$year],2, ".", "'"), 0, "", "R");
					}
					elseif($expense_types[$year][$name] == 18) // taxes
					{
						$pdf->Cell(12, 4,  number_format($taxes_80_percent_values[$year],2, ".", "'"), 0, "", "R");
					}
					elseif($expense_types[$year][$name] == 19) // passenger index
					{
						$pdf->Cell(12, 4,  number_format($passenger_index_80_percent_values[$year],2, ".", "'"), 0, "", "R");
					}
					elseif($expense_types[$year][$name] == 3) // aditional renal cost
					{
						$pdf->Cell(12, 4,  number_format($additionalrents_values[$year],2, ".", "'"), 0, "", "R");
					}
					else
					{
						$pdf->Cell(12, 4,  number_format($value,2, ".", "'"), 0, "", "R");
					}
				}
				else
				{
					$pdf->Cell(12, 4,  number_format($value,2, ".", "'"), 0, "", "R");
				}
			}
			$xi = $xi + $standard_xoffset;
		}
	}

	//Depreciation
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($fixed_assets as $key=>$itype)
	{
		if(isset($debug_depreciation[$itype]))
		{
			if(!$amounts[$itype]){$amounts[$itype] = "-";}
			if(!$depryears[$itype]){$depryears[$itype] = "-";}

			$xi = $standard_xoffset;
			foreach($years as $key=>$year)
			{
				$pdf->SetXY($x2+$xi,$y);
				$pdf->Cell(12, 4,  number_format($debug_depreciation[$itype][$year],2, ".", "'"), 0, "", "R");
				$xi = $xi + $standard_xoffset;
			}
			$y = $y+ $standard_y;
		}
	}

	
	
	
	
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($depreciation[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	$y = $y+ $standard_y;
	$xi = $standard_xoffset;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($debug_depreciation_residual_fixed_assets[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		if(array_key_exists(15, $debug_depreciation) and array_key_exists(17, $debug_depreciation))
		{
			$tmp = $debug_depreciation[15][$year] + $debug_depreciation[17][$year];
		}
		elseif(array_key_exists(15, $debug_depreciation))
		{
			$tmp = $debug_depreciation[15][$year];
		}
		elseif(array_key_exists(17, $debug_depreciation))
		{
			$tmp = $debug_depreciation[17][$year];
		}
		else
	    {
			$tmp = "0.00";
		}

		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($tmp,2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	
	

	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
				
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($total_indirect_expenses_80_percent_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}


	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($operating_income01_80_percent_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}

	
	$xi = $standard_xoffset;
	$y = $y+ $standard_y;
	foreach($years as $key=>$year)
	{
		$pdf->SetXY($x2+$xi,$y);
		$pdf->Cell(12, 4,  number_format($operating_income02_80_percent_values[$year],2, ".", "'"), 0, "", "R");
		$xi = $xi + $standard_xoffset;
	}
	

}
$filename = "debug_busnessplan_" . $project["project_number"] . "_" . date("Y-m-d")  . '.pdf';

$pdf->Output($filename);

?>