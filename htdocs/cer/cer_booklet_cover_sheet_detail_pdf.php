<?php
/********************************************************************

    cer_booklet_cover_sheet_pdf.php

    Print Cover Sheet for CER Booklet

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-02-01
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-02-01
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.
*********************************************************************/


$pos_info = $project["order_shop_address_country_name"];
$pos_info .= ", " . $project["order_shop_address_company"];


$poslegaltype = $project["project_costtype_text"];
$projectkind = str_replace("POS", "", $project["projectkind_name"]);
$projectkind = str_replace("  ", " ", $projectkind);
$projectkind_id = $project["project_projectkind"];

$relocated_pos = "";
if($project["project_projectkind"] == 6 and $project["project_relocated_posaddress_id"] > 0) // relocation
{
	$relocated_pos = get_relocated_pos_info($project["project_relocated_posaddress_id"]);

}
$postype = $project["postype_name"];

$pos_info = $projectkind . " " . $poslegaltype . " " . $postype . "\n" . $pos_info;

if($relocated_pos)
{
	$pos_info .= "\n\n" . "Relocation of " . $relocated_pos["posaddress_name"];
}

$pdf->AddPage();
$pdf->SetAutoPageBreak(false, 0);


//page Header

$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
//arialn bold 15
$pdf->SetFont('arialn','B',12);
//Move to the right

$pdf->setXY(80,0);

//Title
$pdf->Cell(0,33,"CER Booklet Project " . $project["order_number"],0,0,'R');


// Title first line
$y=40;
$x=20;
$pdf->SetXY($x, $y);
$pdf->SetFont("arialn", "B", 20);
$pdf->Cell(120, 8, "1. CER / Businessplan", 0);

$pdf->SetFont("arialn", "B", 13);
$pdf->SetXY($x+80, $y);
$pdf->MultiCell(0,3.5, $pos_info, 1, "T");

$pdf->SetFont("arialn", "B", 20);
$pdf->setXY($x, $pdf->getY()+$y);
$pdf->Cell(160, 8, "2. Budget / Layout", 0);

$pdf->setXY($x, $pdf->getY()+$y);
$pdf->Cell(160, 8, "3. Construction Offers/Comparison", 0);

$pdf->setXY($x, $pdf->getY()+$y);
$pdf->Cell(160, 8, "4. Analysis / Benchmark", 0);

$pdf->setXY($x, $pdf->getY()+$y);
$pdf->Cell(160, 8, "5. LNR / Email Exchange", 0);




?>