<?php
/********************************************************************

    inflationrates.php

    Lists countries for editing the inflation rates.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_full_access_to_cer");
set_referer("inflationrate.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
//get all data from inflation rates

$year_months = array();
$rates = array();

$sql = "select inflationrate_id, concat(inflationrate_rate, '%') as rate, " . 
       "inflationrate_year, cer_inflationrates.date_modified as last_change, country_name " .
       "from cer_inflationrates " .
	   "left join countries on country_id = inflationrate_country ";


/********************************************************************
    build list 
*********************************************************************/

$list = new ListView($sql);

$list->set_entity("cer_inflationrates");
$list->set_order("country_name, inflationrate_year DESC");
$list->set_group("country_name");

$list->add_column("inflationrate_year", "Year", "inflationrate.php");
$list->add_column("rate", "Rate", "", "", "", COLUMN_ALIGN_RIGHT);
$list->add_column("last_change", "Modified");

$list->add_button(LIST_BUTTON_NEW, "New", "inflationrate.php");
$list->add_button("new_bulk", "Bulk Operation");

$list->process();

if($list->button("new_bulk")) {
	redirect("inflationrate_bulk.php");
}

$page = new Page("inflationrates");

$page->header();
$page->title("Countries' Inflation Rates");
$list->render();
$page->footer();
?>
