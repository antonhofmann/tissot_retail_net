<?php
/********************************************************************

    cer_inr01_pdf.php

    Print PDF for CER Summary (Form IN-01).

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require "include/financial_functions.php";

check_access("has_access_to_cer");

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');


//get date of milestone 17 has a date
$milestone_17_date = NULL;
$milestone17 = get_project_milestone(param("pid"), 17);
if(count($milestone17) > 1)
{
	$milestone_17_date = $milestone17["project_milestone_date"];
}

// Create and setup PDF document

$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 10);
$pdf->setPrintHeader(false);

$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');
$pdf->Open();

if($milestone_17_date == NULL or $milestone_17_date == '0000-00-00' or $milestone_17_date > "2013-06-01") {
	include("cer_inr01_pdf_detail.php");
}
else
{
	include("cer_in01_pdf_detail_before_2013.php");
}

$file_name = "INR-01_" . $project["project_number"] . ".pdf";
$pdf->Output($file_name);

?>