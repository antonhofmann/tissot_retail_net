<?php
/********************************************************************

    cer_inr03_pdf_detail.php

    Print PDF for Investment Approval Form (Form IN-R03).

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-09-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*********************************************************************/

//get approval names

$approval_name1 = $cer_basicdata["cer_basicdata_approvalname1"];
$approval_name2 = $cer_basicdata["cer_basicdata_approvalname2"];
$approval_name3 = $cer_basicdata["cer_basicdata_approvalname3"];
$approval_name4 = $cer_basicdata["cer_basicdata_approvalname4"];
$approval_name5 = $cer_basicdata["cer_basicdata_approvalname5"];
$approval_name6 = $cer_basicdata["cer_basicdata_approvalname6"];
$approval_name7 = $cer_basicdata["cer_basicdata_approvalname7"];
$approval_name8 = $cer_basicdata["cer_basicdata_approvalname8"];
$approval_name9 = $cer_basicdata["cer_basicdata_approvalname9"];
$approval_name10 = $cer_basicdata["cer_basicdata_approvalname10"];

$approval_name11 = $cer_basicdata["cer_summary_in01_sig01"];
$approval_name12 = $cer_basicdata["cer_summary_in01_sig02"];
$approval_name13 = $cer_basicdata["cer_summary_in01_sig03"];

// get all data needed from project
$sql = "select * " . 
       "from projects " . 
	   "left join orders on order_id = project_order " .
	   "left join countries on country_id = order_shop_address_country " .
	   "left join product_lines on product_line_id = project_product_line " .
	   "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join addresses on address_id = order_client_address " .
       "left join users on user_id = project_retail_coordinator " .
	    "left join postypes on postype_id = project_postype ".
	   "where project_id = " . param("pid");


$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	
	$legal_entity_name = $row["address_company"];
	
	
	$project_name = $row["order_shop_address_company"]. ", " . $row["order_shop_address_place"] . ", " . $row["country_name"];
	
	$project_name .= " - " . str_replace('POS', $row["postype_name"], $row["postype_name"]) . "/" . $row["projectkind_name"] . "/" . $row["project_costtype_text"];

	$project_manager = $row["user_name"] . " " . $row["user_firstname"];
	
	$project_kind = $row["projectkind_name"];
	$budget_amount = number_format($row["project_approximate_budget"], 0, "", "'");
	$project_start = substr($row["order_date"], 5,2) . "/" . substr($row["order_date"], 0,4);
	
	$project_end = substr($row["project_real_opening_date"], 5,2) . "/" . substr($row["project_real_opening_date"], 0,4);
	$order_number = $row["project_order"];
	$address_country = $row["address_country"];
	
}

$client_currency = get_cer_currency(param("pid"));
$currency_symbol = $client_currency["symbol"];

//set pdf parameters

$margin_top = 16;
$margin_left = 12;
$y = $margin_top;


$pdf->SetTitle("Retail Investment Approval Form (CER) Form IN-R03");
$pdf->SetAuthor("Tissot Retailnet");
$pdf->SetDisplayMode(100);
$pdf->AddPage();
$pdf->SetAutoPageBreak(false, 0);

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "I", 9);
$pdf->Cell(40, 8, "Swatch Group", 1);

$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(107, 8, "Retail Investment Approval Form", 1, "", "C");

$pdf->SetFont("arialn", "", 9);
$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C");
$pdf->Cell(20, 8, "IN-R03", 1, "", "C");


// draw first box
$pdf->SetXY($margin_left,$y+8);
$pdf->Cell(187, 17, "", 1);


	// print project and investment infos
	$y = $y+9;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Brand:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(146, 5, "Tissot", 1, "", "L");
	
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Legal Entity Name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(146, 5, $legal_entity_name, 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Project Name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(146, 5, $project_name, 1, "", "L");

	$y = $y+9;
	$pdf->SetXY($x-2,$y);
	$pdf->SetFont("arialn", "B", 12);
	$pdf->Cell(39, 5, "Signatures for Approval:", 0, "", "L");

	//box 1
	$y = $y+7;
	$pdf->SetXY($margin_left,$y);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(100, 17, "", 1);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(37, 17, "", 1);

	$pdf->SetFont("freesans", "B", 7);
	$pdf->SetXY($margin_left,$y);
	$pdf->MultiCell(25, 4, "Retail Investment Comitee" . "\r\n" . $approval_name1, 0);

	$pdf->SetFont("arialn", "", 8);
	$pdf->SetXY($margin_left+25,$y);
	$pdf->Cell(25, 4, "Remarks:", 0);

	$pdf->SetXY($margin_left+125,$y);
	$pdf->Cell(25, 4, "Date:", 0);

	$pdf->SetXY($margin_left+150,$y);
	$pdf->Cell(25, 4, "Signature:", 0);


	//title bar 1
	$y = $y+18;
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFillColor(224,224,224);
	$pdf->Cell(187,6, "Swatch Group HQ", 1, "", "L", true);

	//box 2
	
	$y = $y+6;
	$pdf->SetXY($margin_left,$y);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(100, 17, "", 1);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(37, 17, "", 1);

	$pdf->SetFont("freesans", "B", 7);
	$pdf->SetXY($margin_left,$y);
	$pdf->MultiCell(25, 4, "Retail Development" . "\r\n" . $approval_name5, 0);

	$pdf->SetFont("arialn", "", 8);
	$pdf->SetXY($margin_left+25,$y);
	$pdf->Cell(25, 4, "Remarks:", 0);

	$pdf->SetXY($margin_left+125,$y);
	$pdf->Cell(25, 4, "Date:", 0);

	$pdf->SetXY($margin_left+150,$y);
	$pdf->Cell(25, 4, "Signature:", 0);
	

	//box 3
	$y = $y+17;
	$pdf->SetXY($margin_left,$y);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(100, 17, "", 1);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(37, 17, "", 1);

	$pdf->SetFont("freesans", "B", 7);
	$pdf->SetXY($margin_left,$y);
	$pdf->MultiCell(25, 4, "Retail Controlling" . "\r\n" . $approval_name6, 0);

	$pdf->SetFont("arialn", "", 8);
	$pdf->SetXY($margin_left+25,$y);
	$pdf->Cell(25, 4, "Remarks:", 0);

	$pdf->SetXY($margin_left+125,$y);
	$pdf->Cell(25, 4, "Date:", 0);

	$pdf->SetXY($margin_left+150,$y);
	$pdf->Cell(25, 4, "Signature:", 0);

	
	//title bar 2
	$y = $y+18;
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFillColor(224,224,224);
	$pdf->Cell(187,6, "Brand", 1, "", "L", true);

	//box 4
	/*
	$y = $y+6;
	$pdf->SetXY($margin_left,$y);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(100, 17, "", 1);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(37, 17, "", 1);

	
	$pdf->SetFont("freesans", "B", 7);
	$pdf->SetXY($margin_left,$y);
	$pdf->MultiCell(25, 4, "Tissot CEO" . "\r\n" . $approval_name2, 0);

	$pdf->SetFont("arialn", "", 8);
	$pdf->SetXY($margin_left+25,$y);
	$pdf->Cell(25, 4, "Remarks:", 0);

	$pdf->SetXY($margin_left+125,$y);
	$pdf->Cell(25, 4, "Date:", 0);

	$pdf->SetXY($margin_left+150,$y);
	$pdf->Cell(25, 4, "Signature:", 0);
	*/

	
	if($approval_name7)
	{
		//box 6
		$y = $y+6;
		$pdf->SetXY($margin_left,$y);
		$pdf->Cell(25, 17, "", 1);
		$pdf->Cell(100, 17, "", 1);
		$pdf->Cell(25, 17, "", 1);
		$pdf->Cell(37, 17, "", 1);

		$pdf->SetFont("freesans", "B", 7);
		$pdf->SetXY($margin_left,$y);
		$pdf->MultiCell(25, 4, "CFO" . "\r\n" . $approval_name7, 0);

		$pdf->SetFont("arialn", "", 8);
		$pdf->SetXY($margin_left+25,$y);
		$pdf->Cell(25, 4, "Remarks:", 0);

		$pdf->SetXY($margin_left+125,$y);
		$pdf->Cell(25, 4, "Date:", 0);

		$pdf->SetXY($margin_left+150,$y);
		$pdf->Cell(25, 4, "Signature:", 0);
	}


	if($approval_name3)
	{
		//box 7
		$y = $y+17;
		$pdf->SetXY($margin_left,$y);
		$pdf->Cell(25, 17, "", 1);
		$pdf->Cell(100, 17, "", 1);
		$pdf->Cell(25, 17, "", 1);
		$pdf->Cell(37, 17, "", 1);

		$pdf->SetFont("freesans", "B", 7);
		$pdf->SetXY($margin_left,$y);
		$pdf->MultiCell(25, 4, "VP Sales" . "\r\n" . $approval_name3, 0);

		$pdf->SetFont("arialn", "", 8);
		$pdf->SetXY($margin_left+25,$y);
		$pdf->Cell(25, 4, "Remarks:", 0);

		$pdf->SetXY($margin_left+125,$y);
		$pdf->Cell(25, 4, "Date:", 0);

		$pdf->SetXY($margin_left+150,$y);
		$pdf->Cell(25, 4, "Signature:", 0);
	}

	if($approval_name4)
	{
		//box 8
		$y = $y+17;
		$pdf->SetXY($margin_left,$y);
		$pdf->Cell(25, 17, "", 1);
		$pdf->Cell(100, 17, "", 1);
		$pdf->Cell(25, 17, "", 1);
		$pdf->Cell(37, 17, "", 1);

		$pdf->SetFont("freesans", "B", 7);
		$pdf->SetXY($margin_left,$y);
		$pdf->MultiCell(25, 4, "International Retail Manager" . "\r\n" . $approval_name4, 0);

		$pdf->SetFont("arialn", "", 8);
		$pdf->SetXY($margin_left+25,$y);
		$pdf->Cell(25, 4, "Remarks:", 0);

		$pdf->SetXY($margin_left+125,$y);
		$pdf->Cell(25, 4, "Date:", 0);

		$pdf->SetXY($margin_left+150,$y);
		$pdf->Cell(25, 4, "Signature:", 0);
	}

	//box 9
	/*
	$y = $y+17;
	$pdf->SetXY($margin_left,$y);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(100, 17, "", 1);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(37, 17, "", 1);

	$pdf->SetFont("freesans", "B", 7);
	$pdf->SetXY($margin_left,$y);
	$pdf->MultiCell(25, 4, "Retail Development" . "\r\n" . $approval_name8, 0);

	$pdf->SetFont("arialn", "", 8);
	$pdf->SetXY($margin_left+25,$y);
	$pdf->Cell(25, 4, "Remarks:", 0);

	$pdf->SetXY($margin_left+125,$y);
	$pdf->Cell(25, 4, "Date:", 0);

	$pdf->SetXY($margin_left+150,$y);
	$pdf->Cell(25, 4, "Signature:", 0);
	*/

	if($approval_name9)
	{
		//box 10
		$y = $y+17;
		$pdf->SetXY($margin_left,$y);
		$pdf->Cell(25, 17, "", 1);
		$pdf->Cell(100, 17, "", 1);
		$pdf->Cell(25, 17, "", 1);
		$pdf->Cell(37, 17, "", 1);

		$pdf->SetFont("freesans", "B", 7);
		$pdf->SetXY($margin_left,$y);
		$pdf->MultiCell(25, 4, "Retail Controlling" . "\r\n" . $approval_name9, 0);

		$pdf->SetFont("arialn", "", 8);
		$pdf->SetXY($margin_left+25,$y);
		$pdf->Cell(25, 4, "Remarks:", 0);

		$pdf->SetXY($margin_left+125,$y);
		$pdf->Cell(25, 4, "Date:", 0);

		$pdf->SetXY($margin_left+150,$y);
		$pdf->Cell(25, 4, "Signature:", 0);

	}
	
	//title bar 3
	$y = $y+18;
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetXY($margin_left,$y);
	$pdf->SetFillColor(224,224,224);
	$pdf->Cell(187,6, "Country", 1, "", "L", true);

	//box 10
	$y = $y+6;
	$pdf->SetXY($margin_left,$y);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(100, 17, "", 1);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(37, 17, "", 1);

	$pdf->SetFont("freesans", "B", 7);
	$pdf->SetXY($margin_left,$y);
	$pdf->MultiCell(25, 4, "Country Manager" . "\r\n" . $approval_name11, 0);

	$pdf->SetFont("arialn", "", 8);
	$pdf->SetXY($margin_left+25,$y);
	$pdf->Cell(25, 4, "Remarks:", 0);

	$pdf->SetXY($margin_left+125,$y);
	$pdf->Cell(25, 4, "Date:", 0);

	$pdf->SetXY($margin_left+150,$y);
	$pdf->Cell(25, 4, "Signature:", 0);

	//box 11
	$y = $y+17;
	$pdf->SetXY($margin_left,$y);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(100, 17, "", 1);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(37, 17, "", 1);

	$pdf->SetFont("freesans", "B", 7);
	$pdf->SetXY($margin_left,$y);
	$pdf->MultiCell(25, 4, "Finance/Service Manager" . "\r\n" . $approval_name12, 0);

	$pdf->SetFont("arialn", "", 8);
	$pdf->SetXY($margin_left+25,$y);
	$pdf->Cell(25, 4, "Remarks:", 0);

	$pdf->SetXY($margin_left+125,$y);
	$pdf->Cell(25, 4, "Date:", 0);

	$pdf->SetXY($margin_left+150,$y);
	$pdf->Cell(25, 4, "Signature:", 0);

	//box 12
	$y = $y+17;
	$pdf->SetXY($margin_left,$y);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(100, 17, "", 1);
	$pdf->Cell(25, 17, "", 1);
	$pdf->Cell(37, 17, "", 1);

	$pdf->SetFont("freesans", "B", 7);
	$pdf->SetXY($margin_left,$y);
	$pdf->MultiCell(25, 4, "Brand Manager"  . "\r\n" . $approval_name13, 0);

	$pdf->SetFont("arialn", "", 8);
	$pdf->SetXY($margin_left+25,$y);
	$pdf->Cell(25, 4, "Remarks:", 0);

	$pdf->SetXY($margin_left+125,$y);
	$pdf->Cell(25, 4, "Date:", 0);

	$pdf->SetXY($margin_left+150,$y);
	$pdf->Cell(25, 4, "Signature:", 0);

?>