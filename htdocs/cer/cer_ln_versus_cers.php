<?php
/********************************************************************

    cer_ln_versus_cers.php

    List of LN versus CER Statistics - LN Benchmarks

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-03-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-03-27
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_cer_benchmarks");
set_referer("cer_ln_versus_cer.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data


//create a copy of an existing query
if(param("copy_id"))
{
	$sql = "select * from ln_benchmarks where ln_benchmark_id = " . param("copy_id");
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$sql_i = "Insert into ln_benchmarks (" . 
		         "ln_benchmark_user_id, " .
				 "ln_benchmark_group, " .
			     "ln_benchmark_shortcut, " . 
			     "ln_benchmark_title, " . 
			     "ln_benchmark_filter, " . 
			     "ln_benchmark_project_filter, " . 
			     "ln_benchmark_basic_currency, " . 
			     "ln_benchmark_include_pipeline, " . 
			     "ln_benchmark_last_project, " . 
			     "ln_benchmark_project_state, " . 
			     "ln_benchmark_cer_state, " . 
			     "ln_benchmark_af_state, " . 
			     "ln_benchmark_from_year, " . 
			     "ln_benchmark_to_year, " .
			     "ln_benchmark_from_state, " .
			     "ln_benchmark_to_state, " .
			     "user_created, " . 
			     "date_created) VALUES (" . 
			     user_id() . ", " .
				 dbquote($row["ln_benchmark_group"]) . ", " .
				 dbquote($row["ln_benchmark_shortcut"] .  " - copy") . ", " .
			     dbquote($row["ln_benchmark_title"].  " - copy") . ", " . 
			     dbquote($row["ln_benchmark_filter"]) . ", " . 
			     dbquote($row["ln_benchmark_project_filter"]) . ", " . 
			     dbquote($row["ln_benchmark_basic_currency"]) . ", " . 
			     dbquote($row["ln_benchmark_include_pipeline"]) . ", " . 
			     dbquote($row["ln_benchmark_last_project"]) . ", " . 
			     dbquote($row["ln_benchmark_project_state"]) . ", " . 
			     dbquote($row["ln_benchmark_cer_state"]) . ", " . 
			     dbquote($row["ln_benchmark_af_state"]) . ", " . 
			     dbquote($row["ln_benchmark_from_year"]) . ", " . 
			     dbquote($row["ln_benchmark_to_year"]) . ", " . 
			     dbquote($row["ln_benchmark_from_state"]) . ", " . 
			     dbquote($row["ln_benchmark_to_state"]) . ", " . 
			     dbquote(user_login()) . ", " .
			     "CURRENT_TIMESTAMP)";
		$result = mysql_query($sql_i);
	}
}

elseif(param("remove_id"))
{
	$sql_d = "delete from ln_benchmarkpermissions " . 
		   " where ln_benchmarkpermission_benchmark = " . param("remove_id") . 
		   " and ln_benchmarkpermission_user = " . user_id();
	$result = mysql_query($sql_d);

}

// create sql
$sql = "select ln_benchmark_id, ln_benchmark_group, ln_benchmark_shortcut, ln_benchmark_title,  ln_benchmark_user_id, " . 
       "ln_benchmarks.date_created as cdate, ln_benchmarks.date_modified as mdate, " .
	   "concat(user_name, ' ', user_firstname) as uname " . 
       "from ln_benchmarks " .
	   "left join users on user_id = ln_benchmark_user_id";


$benchmark_links = array();
$copyk_links = array();
$benchmark_permissions = array();
$delete_links = array();
$permitted_users = array();

//$sql_a = $sql . " where ln_benchmark_user_id = " . user_id();
$res = mysql_query($sql) or dberror($sql);

//$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	if(user_id() == $row["ln_benchmark_user_id"]) {
		
		$benchmark_links[$row["ln_benchmark_id"]] = "<a href='cer_ln_versus_cer.php?id=" .  $row["ln_benchmark_id"] . "'>" . $row["ln_benchmark_shortcut"] . "</a>";
	}
	else
	{
		$benchmark_links[$row["ln_benchmark_id"]] = $row["ln_benchmark_shortcut"];	
		$delete_links[$row["ln_benchmark_id"]] = '<a title="Remove query from my list" href="cer_ln_versus_cers.php?remove_id=' .  $row["ln_benchmark_id"] . '"><img src="/pictures/remove.gif" border="0" style="margin-top:3px;" alet="Remove"/></a>';
	}

	$copyk_links[$row["ln_benchmark_id"]] = "<a href='cer_ln_versus_cers.php?copy_id=" .  $row["ln_benchmark_id"] . "'>create copy</a>";

	$url = "ln_benchmark_xls.php?query_id=" . $row["ln_benchmark_id"];
	$link = "javascript:popup('" . $url . "', 1024, 768)";
	$link = '<a title="Statistics" href="' . $link . '"><img src="/pictures/ico_xls.gif" border="0"/></a>';

	$queries[$row["ln_benchmark_id"]] = $link;

	
	//get permissions
	$tmp_users = array();
	$sql_p = "select * from ln_benchmarkpermissions " .
			 "left join ln_benchmarks on ln_benchmark_id = ln_benchmarkpermission_benchmark " .
			  " left join users on user_id = ln_benchmarkpermission_user " .
			 "where  ln_benchmarkpermission_benchmark = " . $row["ln_benchmark_id"];

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	while ($row_p = mysql_fetch_assoc($res_p))
	{
		if($row_p["ln_benchmarkpermission_user"] == user_id())
		{
			$benchmark_permissions[$row_p["ln_benchmarkpermission_benchmark"]] = $row_p["ln_benchmarkpermission_benchmark"];
		}
		else {
			$tmp_users[] = $row_p["user_name"] . " " . $row_p["user_firstname"];
		}
	}

	if(count($tmp_users) > 0) {
		$permitted_users[$row["ln_benchmark_id"]] = implode(', ', $tmp_users);
	}
}


$list_filter = "ln_benchmark_user_id = " . user_id();
if(count($benchmark_permissions) > 0)
{
	$permission_filter =  implode(',', $benchmark_permissions);
	$list_filter .= " or ln_benchmark_id in (" . $permission_filter . ") ";
}


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("ln_benchmarks");
$list->set_order("ln_benchmark_shortcut");
$list->set_filter($list_filter);
$list->set_group("ln_benchmark_group");

$list->add_text_column("q1", "", COLUMN_NO_WRAP | COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $queries);
$list->add_text_column("benchmarklinks", "Shortcut", COLUMN_NO_WRAP | COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $benchmark_links);
$list->add_column("ln_benchmark_title", "Title", "", "", "", COLUMN_NO_WRAP);
$list->add_column("uname", "Owner", "", "", "", COLUMN_NO_WRAP);
$list->add_column("cdate", "Created", "", "", "", COLUMN_NO_WRAP);
$list->add_column("mdate", "Modified", "", "", "", COLUMN_NO_WRAP);



$list->add_text_column("benchmarklinks", "", COLUMN_NO_WRAP | COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $copyk_links);
$list->add_text_column("removelinks", "", COLUMN_NO_WRAP | COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $delete_links);
$list->add_text_column("permitted_users", "Granted Access", COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $permitted_users);

$list->add_button(LIST_BUTTON_NEW, "Add New Query", "cer_ln_versus_cer.php");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->process();

$page = new Page("cer_benchmarks");
require "include/cer_benchmarks_page_actions.php";
$page->header();


$page->title("Statistics - LNR versus CER");
$list->render();
$page->footer();
?>