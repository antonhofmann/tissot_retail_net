<?php
/********************************************************************

	cer_draft_revenues.php

    Application Form: revenues
    
	Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-02-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-02-16
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_draft_functions.php";

check_access("has_access_to_cer_drafts");
set_referer("cer_draft_revenue.php");


/********************************************************************
    prepare all data needed
*********************************************************************/

$currency = get_draft_currency(param("did"));
$basicdata = get_draft_basicdata(param("did"));

$years = array();

$sql = "select * " .
       "from cer_draft_expenses " . 
	   "where cer_expense_type = 1 and cer_expense_draft_id = " . param("did");

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$years[] = $row["cer_expense_year"];
}

//get revenue values
$quantity_watches = array();
$aveargeprice_watches = array();
$quantity_jewellery = array();
$aveargeprice_jewellery = array();

$customer_frequencies = array();
$days_open_per_year = array();
$working_hours_per_week = array();
$opening_hours_per_week = array();

$total_gross_sales = array();

$number_of_months = array();

$sql = "select * from cer_draft_revenues where cer_revenue_draft_id = " . param("did");
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$quantity_watches[$row["cer_revenue_id"]] = $row["cer_revenue_quantity_watches"];
	$aveargeprice_watches[$row["cer_revenue_id"]] = $row["cer_revenue_aveargeprice_watches"];
	$quantity_jewellery[$row["cer_revenue_id"]] = $row["cer_revenue_quantity_jewellery"];
	$aveargeprice_jewellery[$row["cer_revenue_id"]] = $row["cer_revenue_aveargeprice_jewellery"];

	$customer_frequencies[$row["cer_revenue_id"]] = $row["cer_revenue_customer_frequency"];

	$days_open_per_year[$row["cer_revenue_id"]] = $row["cer_revenue_total_days_open_per_year"];
	$opening_hours_per_week[$row["cer_revenue_id"]] = $row["cer_revenue_total_hours_open_per_week"];
	$working_hours_per_week[$row["cer_revenue_id"]] = $row["cer_revenue_total_workinghours_per_week"];

	$total_gross_sales[$row["cer_revenue_id"]] = $row["cer_revenue_watches"] + $row["cer_revenue_jewellery"] + $row["cer_revenue_customer_service"];


	if($row["cer_revenue_year"] == $basicdata["cer_basicdata_firstyear"])
	{
		if($basicdata["cer_basicdata_firstmonth"] < 12)
		{
			$number_of_months[$row["cer_revenue_id"]] = date("F", mktime(0, 0, 0, $basicdata["cer_basicdata_firstmonth"], 10)) . " to December (" . (13-$basicdata["cer_basicdata_firstmonth"]) . ")";
		}
		else
		{
			$number_of_months[$row["cer_revenue_id"]] = "December (1)";
		}
	}
	elseif($row["cer_revenue_year"] == $basicdata["cer_basicdata_lastyear"])
	{
		if($basicdata["cer_basicdata_lastmonth"] > 1)
		{
			$number_of_months[$row["cer_revenue_id"]] = "January to " . date("F", mktime(0, 0, 0, $basicdata["cer_basicdata_lastmonth"], 10)) . "(" . $basicdata["cer_basicdata_lastmonth"] . ")";
		}
		else
		{
			$number_of_months[$row["cer_revenue_id"]] = "January (1)";
		}
	}
	else
	{
		$number_of_months[$row["cer_revenue_id"]] = "January to December (12)";
	}
}


$sql_list1 = "select cer_revenue_id, cer_revenue_year, " .
             "cer_revenue_quantity_watches, cer_revenue_aveargeprice_watches, " . 
			 "cer_revenue_quantity_jewellery, cer_revenue_aveargeprice_jewellery, " .
             "cer_revenue_watches, cer_revenue_jewellery, " . 
			 " cer_revenue_customer_service " .
             "from cer_draft_revenues ";

$list1_filter = "cer_revenue_draft_id = " . param("did");


//get list_totals
$list_totals = array();
$list_totals[1] = 0;
$list_totals[2] = 0;
$list_totals[3] = 0;
$list_totals[4] = 0;
$list_totals[5] = 0;
$list_totals[6] = 0;
$list_totals[7] = 0;
$list_totals[8] = 0;
$list_totals[9] = 0;
$list_totals[10] = 0;

$sql = $sql_list1 . " where " . $list1_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$list_totals[1] = $list_totals[1] + $row["cer_revenue_quantity_watches"];
	$list_totals[2] = $list_totals[2] + $row["cer_revenue_watches"];
	$list_totals[3] = $list_totals[3] + $row["cer_revenue_quantity_jewellery"];
	$list_totals[4] = $list_totals[4] + $row["cer_revenue_jewellery"];
	$list_totals[7] = $list_totals[7] + $row["cer_revenue_customer_service"];
	$list_totals[8] = $list_totals[8] + $row["cer_revenue_watches"] + $row["cer_revenue_jewellery"] + $row["cer_revenue_customer_service"];
}


//prepare sql for list 2
$sql_list2 = "select cer_expense_year, cer_expense_amount, cer_expense_type_name " .
             "from cer_draft_expenses " . 
			 "left join cer_expense_types on cer_expense_type_id = cer_expense_type ";

$list2_filter = "cer_expense_draft_id = " . param("did") . " and cer_expense_type = 14";

// get all expenses for for each year
$years = array();
$year_amounts = array();

$sql  = "select cer_expense_type, cer_expense_year, cer_expense_amount " .
		"from cer_draft_expenses " .
		"where cer_expense_type = 14 and cer_expense_draft_id = " . param("did");

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$years[$row["cer_expense_year"]] = $row["cer_expense_year"];
	$year_amounts[$row["cer_expense_year"]] = $row["cer_expense_amount"];
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_drafts", "cer_drafts");

$form->add_hidden("did", param("did"));
$form->add_section("Revenue Parameters");

$form->add_edit("cer_basicdata_customer_service", "Customer Service/Accessories in % of gross sales", 0, $basicdata["cer_basicdata_customer_service"], TYPE_DECIMAL, 12,2, 1, "customer_service");
$form->add_edit("cer_basicdata_sales_reduction", "Sales Reduction in % of gross sales", 0, $basicdata["cer_basicdata_sales_reduction"], TYPE_DECIMAL, 12,2,1, "sales_reduction");

if($basicdata['cer_basicdata_legal_type'] == 1) {
	$form->add_edit("cer_basicdata_wholesale_margin", "Wholesale Margin in %", 0, $basicdata["cer_basicdata_wholesale_margin"], TYPE_DECIMAL, 12,2, 1, "whole_sale_margin");
}
else
{
	$form->add_edit("cer_basicdata_wholesale_margin", "Additional wholesale Margin in %", 0, $basicdata["cer_basicdata_wholesale_margin"], TYPE_DECIMAL, 12,2, 1, "whole_sale_margin");
}

$form->add_button("form_save", "Save Data");


/********************************************************************
    build list of standard revenues
*********************************************************************/
$list1 = new ListView($sql_list1, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("Sales Revenues in " . $currency["symbol"]);
$list1->set_entity("cer_draft_revenues");
$list1->set_filter($list1_filter);
$list1->set_order("cer_revenue_year");

$list1->add_column("cer_revenue_year", "Year");
$list1->add_text_column("months", "Period",COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_LEFT, $number_of_months);

$list1->add_number_edit_column("cer_revenue_quantity_watches", "Quantity\nWatches", "4",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $quantity_watches);
$list1->add_number_edit_column("cer_revenue_aveargeprice_watches", "Average Price\nWatches in " . $currency["symbol"], "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $aveargeprice_watches);

$list1->add_column("cer_revenue_watches", "Gross Sales\nWatches", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_RIGHT);

$list1->add_number_edit_column("cer_revenue_quantity_jewellery", "Quantity\nWatch Straps", "4",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $quantity_jewellery);
$list1->add_number_edit_column("cer_revenue_aveargeprice_jewellery", "Average Price\nWatch Straps in " . $currency["symbol"], "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $aveargeprice_jewellery);
$list1->add_column("cer_revenue_jewellery", "Gross Sales\nWatch Straps", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_RIGHT);

$list1->add_column("cer_revenue_customer_service", "Gross Sales\nCustomer Service\nAccessories", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_RIGHT);

$list1->add_text_column("total_gross_sales", "Total Gross\nSales Values",COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $total_gross_sales);

$list1->set_footer("total_gross_sales", number_format($list_totals[8] ,0, ".", "'"));


$list1->set_footer("cer_revenue_year", "Total");
$list1->set_footer("cer_revenue_quantity_watches", number_format($list_totals[1] ,0, ".", "'"));
$list1->set_footer("cer_revenue_watches", number_format($list_totals[2] ,0, ".", "'"));
$list1->set_footer("cer_revenue_quantity_jewellery", number_format($list_totals[3] ,0, ".", "'"));
$list1->set_footer("cer_revenue_jewellery", number_format($list_totals[4] ,0, ".", "'"));

$list1->set_footer("cer_revenue_customer_service", number_format($list_totals[7] ,0, ".", "'"));

$list1->add_button("list1_save", "Save Values");

/********************************************************************
    build list of revenues Marketing contribution
*********************************************************************/
$list2 = new ListView($sql_list2, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list2->set_title("Marketing Contribution in " . $currency["symbol"]);
$list2->set_entity("cer_draft_expenses");
$list2->set_filter($list2_filter);
$list2->set_order('cer_expense_year');


$list2->add_text_column("cer_expense_year", "Year", 0, $years);
$list2->add_text_column("months2", "Period",COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_LEFT, $number_of_months);
$list2->add_column("cer_expense_type_name", "Revenue Type");

$list2->add_number_edit_column("cer_expense_amount", "Amount in " . $currency["symbol"], "12",  COLUMN_ALIGN_RIGHT, $year_amounts);

$list2->add_button("list2_save", "Save Values");


/********************************************************************
    build list of standard revenues
*********************************************************************/
$list3 = new ListView($sql_list1, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list3->set_title("Other Sales Relevant Information");
$list3->set_entity("cer_revenues");
$list3->set_filter($list1_filter);
$list3->set_order("cer_revenue_year");

$list3->add_column("cer_revenue_year", "Year");
$list3->add_text_column("months3", "Period",COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_LEFT, $number_of_months);


$list3->add_number_edit_column("cer_revenue_total_days_open_per_year", "Total Days\n Open\nper Year", "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $days_open_per_year);

$list3->add_number_edit_column("cer_revenue_total_hours_open_per_week", "Total Hours\n Open\nper Week", "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $opening_hours_per_week);

$list3->add_number_edit_column("cer_revenue_total_workinghours_per_week", "Total Working\nHours per Week \nfor 1 Employee\n (working 100%)", "20",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $working_hours_per_week);


$list3->add_number_edit_column("cer_revenue_customer_frequency", "Customer\nFrequency\nper Day", "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $customer_frequencies);

$list3->add_button("list3_save", "Save Values");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

$list1->populate();
$list1->process();

$list2->populate();
$list2->process();

$list3->populate();
$list3->process();


if($form->button("form_save"))
{
	if($form->validate())
	{
		
		$fields = array();
    
		$value = dbquote($form->value("cer_basicdata_customer_service"));
		$fields[] = "cer_basicdata_customer_service = " . $value;

		$value = dbquote($form->value("cer_basicdata_sales_reduction"));
		$fields[] = "cer_basicdata_sales_reduction = " . $value;

		$value = dbquote($form->value("cer_basicdata_wholesale_margin"));
		$fields[] = "cer_basicdata_wholesale_margin = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_drafts set " . join(", ", $fields) . " where cer_basicdata_id = " . param("did");
		mysql_query($sql) or dberror($sql);

		$form->message("Your data has bee saved.");
	}

	//update revenues
	$sql = "select * from cer_draft_revenues " . 
		   "where cer_revenue_draft_id = " . param("did");
	
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$fields = array();
		$value = $row["cer_revenue_quantity_watches"]*$row["cer_revenue_aveargeprice_watches"] + $row["cer_revenue_quantity_jewellery"]*$row["cer_revenue_aveargeprice_jewellery"];

		$value = $value * $form->value("cer_basicdata_customer_service") / 100;
		
		$value = dbquote($value);
		$fields[] = "cer_revenue_customer_service = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_draft_revenues set " . join(", ", $fields) . " where cer_revenue_id = " . $row["cer_revenue_id"];
		mysql_query($sql) or dberror($sql);
	}
	
}
elseif($list1->button("list1_save"))
{

	$v1 = $list1->values("cer_revenue_quantity_watches");
	$v2 = $list1->values("cer_revenue_aveargeprice_watches");
	$v3 = $list1->values("cer_revenue_quantity_jewellery");
	$v4 = $list1->values("cer_revenue_aveargeprice_jewellery");

	foreach ($v1 as $key=>$value)
    {
        
            $fields = array();
    
            $value = dbquote($v1[$key]);
			$fields[] = "cer_revenue_quantity_watches = " . $value;

			$value = dbquote($v2[$key]);
			$fields[] = "cer_revenue_aveargeprice_watches = " . $value;

			$value = dbquote($v3[$key]);
			$fields[] = "cer_revenue_quantity_jewellery = " . $value;

			$value = dbquote($v4[$key]);
			$fields[] = "cer_revenue_aveargeprice_jewellery = " . $value;

			$value = dbquote($v1[$key]*$v2[$key]);
			$fields[] = "cer_revenue_watches = " . $value;

			$value = dbquote($v3[$key]*$v4[$key]);
			$fields[] = "cer_revenue_jewellery = " . $value;

			$value = $v1[$key]*$v2[$key] + $v3[$key]*$v4[$key];
			$value = $value * $form->value("cer_basicdata_customer_service") / 100;
			
			$value = dbquote($value);
			$fields[] = "cer_revenue_customer_service = " . $value;

            $value1 = "current_timestamp";
            $fields[] = "date_modified = " . $value1;
    
            if (isset($_SESSION["user_login"]))
            {
                $value1 = $_SESSION["user_login"];
                $fields[] = "user_modified = " . dbquote($value1);
            }
       
            $sql = "update cer_draft_revenues set " . join(", ", $fields) . " where cer_revenue_id = " . $key;
            mysql_query($sql) or dberror($sql);
	}

	$reuslt = update_cost_of_products_sold(param("did"));

	$form->message("Your data has bee saved.");
	//$link = "cer_draft_revenues.php"did"=" . param("did");
	//redirect($link);
}
elseif($list2->button("list2_save"))
{
	$v1 = $list2->values("cer_expense_year");
	$v2 = $list2->values("cer_expense_amount");

	foreach ($v1 as $key=>$value)
    {
        
            $fields = array();
    
			$value = dbquote($v2[$key]);
			$fields[] = "cer_expense_amount = " . $v2[$key];

	        $value1 = "current_timestamp";
            $fields[] = "date_modified = " . $value1;
    
            if (isset($_SESSION["user_login"]))
            {
                $value1 = $_SESSION["user_login"];
                $fields[] = "user_modified = " . dbquote($value1);
            }
       
            $sql = "update cer_draft_expenses set " . join(", ", $fields) . " where cer_expense_type = 14 and cer_expense_draft_id = " . param("did") . " and cer_expense_year = " . $v1[$key];
            mysql_query($sql) or dberror($sql);
	}

	$reuslt = update_cost_of_products_sold(param("did"));

	$form->message("Your data has bee saved.");
	//$link = "cer_draft_revenues.php"did"=" . param("did");
	//redirect($link);

}

elseif($list3->button("list3_save"))
{

	$v1 = $list3->values("cer_revenue_customer_frequency");
	$v2= $list3->values("cer_revenue_total_days_open_per_year");
	$v3= $list3->values("cer_revenue_total_workinghours_per_week");
	$v4= $list3->values("cer_revenue_total_hours_open_per_week");

	foreach ($v1 as $key=>$value)
    {
        
            $fields = array();
    
            
			$value = dbquote($v1[$key]);
			$fields[] = "cer_revenue_customer_frequency = " . $value;

			$value = dbquote($v2[$key]);
			$fields[] = "cer_revenue_total_days_open_per_year = " . $value;

			$value = dbquote($v3[$key]);
			$fields[] = "cer_revenue_total_workinghours_per_week = " . $value;

			$value = dbquote($v4[$key]);
			$fields[] = "cer_revenue_total_hours_open_per_week = " . $value;

            $value1 = "current_timestamp";
            $fields[] = "date_modified = " . $value1;
    
            if (isset($_SESSION["user_login"]))
            {
                $value1 = $_SESSION["user_login"];
                $fields[] = "user_modified = " . dbquote($value1);
            }
       
            $sql = "update cer_draft_revenues set " . join(", ", $fields) . " where cer_revenue_id = " . $key;
            mysql_query($sql) or dberror($sql);
	}

	$form->message("Your data has bee saved.");
	
}

  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_drafts");

require "include/draft_page_actions.php";
$page->header();
$page->title($basicdata['cer_basicdata_title'] . ": Revenues");

require_once("include/tabs_draft2016.php");
$form->render();
echo "<br />";

$list1->render();

//interco is not needed anymore since we introduced it into the revenues in 2013
//echo "<br />";
//echo "<br />";
//$list2->render();

echo "<br />";
echo "<br />";
$list3->render();

?>

<div id="customer_service" style="display:none;">
    Customer service in percent of gross sales
</div>

<div id="sales_reduction" style="display:none;">
    Sales reduction in percent of gross sales
</div>

<div id="whole_sale_margin" style="display:none;">
    Wholesale margin in percent on costs of products sold
</div> 

<div id="additional_whole_sale_margin" style="display:none;">
    Additional margin should only be introduced if the POS is an agent POS because a third party won't earn any wholesale margin.
</div> 

<?php
$page->footer();

?>