<?php
/********************************************************************

    cer_booklet_pdf.php

    Print PDF CER all Forms.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-02-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-02-20
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();


$SUPPRESS_HEADERS = true;
$PDFmerger_was_used = false;
$pdf_print_output = true;


define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";

require "../shared/func_posindex.php";

check_access("has_access_to_cer");
require "include/get_functions.php";
require "include/get_project.php";
require "include/financial_functions.php";
require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/SetaPDF/Autoload.php');



if($use_old_cer_forms_before_2013 != true)
{
	//save actual cer version temporarily
	$tmp_cer_version = $cer_version;

	//get data of latest ln version
	if($tmp_cer_version > 0)
	{
		$sql = "select cer_basicdata_version from cer_basicdata where cer_basicdata_project = " . dbquote(param("pid")) . " and cer_basicdata_version < " . $tmp_cer_version . " and cer_basicdata_version_context = 'ln' order by cer_basicdata_version desc";
	}
	else
	{
		$sql = "select cer_basicdata_version from cer_basicdata where cer_basicdata_project = " . dbquote(param("pid")) . " and cer_basicdata_version_context = 'ln' order by cer_basicdata_version desc";
	}

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$cer_version = $row["cer_basicdata_version"];

		include("include/in_financial_data.php");
		$ln_amounts = $amounts;
		$ln_exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
		$ln_exchange_rate_factor = $cer_basicdata["cer_basicdata_factor"];
		$ln_investment_total = $investment_total;
		$ln_intagible_amount = $intagible_amount;
		$ln_totals = $cer_totals;
		$ln_sales_units_watches_values = $sales_units_watches_values;
		$ln_sales_units_jewellery_values = $sales_units_jewellery_values;
		$ln_total_net_sales_values = $total_net_sales_values;
		$ln_operating_income02_values = $operating_income02_values;

		$ln_number_of_months_first_year = $number_of_months_first_year;
		$ln_number_of_months_last_year = $number_of_months_last_year;
		$ln_wholesale_margin = $cer_basicdata["cer_basicdata_wholesale_margin"];

	}
	else
	{
		$ln_amounts = array();
		$ln_exchange_rate = $cer_basicdata["cer_basicdata_exchangerate"];
		$ln_exchange_rate_factor = $cer_basicdata["cer_basicdata_factor"];
		$ln_investment_total = 0;
		$ln_intagible_amount = 0;
		$ln_totals =0;
		$ln_sales_units_watches_values = array();
		$ln_sales_units_jewellery_values = array();
		$ln_total_net_sales_values = array();
		$ln_operating_income02_values = array();

		$ln_number_of_months_first_year = $number_of_months_first_year;
		$ln_number_of_months_last_year = $number_of_months_last_year;
		$ln_wholesale_margin = "";
	}


	//restore cer_version
	$cer_version = $tmp_cer_version;
}


//get currencies
$currencies = array();
$sql = 'select currency_id, currency_symbol from currencies where currency_system = 1';

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$currencies["s"] = $row['currency_symbol'];
}


if($cer_basicdata['cer_basicdata_currency']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c1"] = $row['currency_symbol'];
	}
}

if($cer_basicdata['cer_basicdata_currency2']) {
	$sql = 'select currency_id, currency_symbol from currencies where currency_id = ' . $cer_basicdata['cer_basicdata_currency2'];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res) and !in_array($row['currency_symbol'], $currencies))
	{
		$currencies["c2"] = $row['currency_symbol'];
	}
}


//get date of milestone 17 has a date
$milestone_17_date = NULL;
$milestone17 = get_project_milestone(param("pid"), 17);
if(count($milestone17) > 1)
{
	$milestone_17_date = $milestone17["project_milestone_date"];
}

// Create and setup PDF document
// Create and setup PDF document
class MYPDF extends TCPDF
{
	
	//Page footer
	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		$this->SetFont('arialn','I',8);
		$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
	}

}


$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');

$pdf->Open();


//cover sheet
include("cer_booklet_cover_sheet_detail_pdf.php");

//investmen approval form
//include("cer_inr03_pdf_detail.php");


//financial summary
if($milestone_17_date == NULL or $milestone_17_date == '0000-00-00' or $milestone_17_date > "2013-06-01") {
	include("cer_inr01_pdf_detail.php");
}
else
{
	include("cer_in01_pdf_detail_before_2013.php");
}

//business plan in client's currency
$tmp_ln_exchange_rate = $ln_exchange_rate;
$tmp_ln_exchange_rate_factor = $ln_exchange_rate_factor;
if($use_old_cer_forms_before_2013 == true)
{
	include("cer_inr02_pdf_detail_before2013.php");
}
else
{
	include("cer_inr02_pdf_detail.php");

	$ln_exchange_rate = 1;
	$ln_exchange_rate_factor = 1;
	include("cer_inr02c_pdf_detail.php");

}


//business plan in system'currency
$ln_exchange_rate = $tmp_ln_exchange_rate;
$ln_exchange_rate_factor = $tmp_ln_exchange_rate_factor;

if(array_key_exists("s", $currencies) and array_key_exists("c1", $currencies) and $currencies["s"] != $currencies["c1"])
{
	$cid = 's';
	if($use_old_cer_forms_before_2013 == true)
	{
		include("cer_inr02_pdf_detail_before2013.php");
	}
	else
	{
		include("cer_inr02_pdf_detail.php");
		include("cer_inr02c_pdf_detail.php");
	}
}



//business plan in pos'currency
if(array_key_exists("c2", $currencies) and array_key_exists("c1", $currencies) and $currencies["c2"] != $currencies["c1"])
{
	$cid = 'c2';
	if($use_old_cer_forms_before_2013 == true)
	{
		include("cer_inr02_pdf_detail_before2013.php");
	}
	else
	{
		include("cer_inr02_pdf_detail.php");
	}
}




//project budget
if($cer_version > 0)
{
	$source_file = $_SERVER['DOCUMENT_ROOT'] . 'files/ln/' . $project["project_number"] . '/cer_budget_' . param('pid') . '_version_' . $cer_version . '.pdf';
	
	if(file_exists($source_file))
	{

		$pdfString = $pdf->output('', 'S');
		
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		
		$merger = new SetaPDF_Merger();
		$merger->addDocument($tmp);

		$merger->addFile(array(
			'filename' => $source_file,
			'copyLayers' => true
		));


		$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(true);

		$pdf->AddFont('arialn','');
		$pdf->AddFont('arialn','B');
		$pdf->AddFont('arialn','I');
		$pdf->AddFont('arialn','BI');

		$pdf->Open();

		$PDFmerger_was_used = true;
	
	}
	else
	{
		include("../user/project_view_project_budget_pdf_detail.php");
	}
}
else
{
	include("../user/project_view_project_budget_pdf_detail.php");
}



//project layout attachment pdf
$layout_found = false;
$layout_file = "";

/*
//desactiveted on July 22 2014

$sql = "select order_file_path " . 
       "from projects " . 
	   "left join order_files on order_file_order = project_order " . 
       "where order_file_is_final_layout = 1 " .
	   " and order_file_category = 8 and order_file_type= 13 and project_id = " .param("pid") . 
	   " order by order_files.date_created DESC";

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	
	$source_file = ".." . $row["order_file_path"];
	if(file_exists($source_file) and strtolower(pathinfo($row["order_file_path"], PATHINFO_EXTENSION)) == 'pdf')
	{
		$layout_file = $row["order_file_path"];
		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		
		if($PDFmerger_was_used == false)
		{
			$merger = new SetaPDF_Merger();
		}

		$merger->addDocument($tmp);

		$merger->addFile(array(
			'filename' => $source_file,
			'copyLayers' => true
		));


		$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(true);

		$pdf->AddFont('arialn','');
		$pdf->AddFont('arialn','B');
		$pdf->AddFont('arialn','I');
		$pdf->AddFont('arialn','BI');

		$pdf->Open();

		$PDFmerger_was_used = true;
		$layout_found = true;

	}
}
else // layout as JPEG
{

	$sql = "select order_file_path " . 
		   "from projects " . 
		   "left join order_files on order_file_order = project_order " . 
		   "where order_file_is_final_layout = 1 " .
		   " and order_file_category = 8 and order_file_type= 2 and project_id = " .param("pid") . 
		   " order by order_files.date_created DESC";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$source_file = ".." . $row["order_file_path"];
		if(file_exists($source_file) 
			and  (strtolower(pathinfo($row["order_file_path"], PATHINFO_EXTENSION)) == 'jpg'
		     or strtolower(pathinfo($row["order_file_path"], PATHINFO_EXTENSION)) == 'jpeg')
			)
		{
			$layout_file = $row["order_file_path"];
			$pdf->AddPage("L");
			$margin_top = 16;
			$margin_left = 12;
			$pdf->SetXY($margin_left,$margin_top);

			$pdf->SetFont("arialn", "I", 9);
			$pdf->Cell(45, 10, "", 1);

			$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);

			$pdf->SetFont("arialn", "B", 11);
			$pdf->Cell(172, 10, "Layout: " . $project_name, 1, "", "L");
			$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");

			$pdf->Image($source_file,20,40, 200);
			$layout_found = true;
		}
	}
}
*/

//project layout attachment in case there is no special tagged layout
/*
if($layout_found == false)
{
	$sql = "select order_file_path " . 
		   "from projects " . 
		   "left join order_files on order_file_order = project_order " . 
		   "where order_file_category = 8 and order_file_type= 13 and project_id = " .param("pid") . 
		   " order by order_files.date_created DESC";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$source_file = ".." . $row["order_file_path"];
		if(file_exists($source_file))
		{
			
			$pdfString = $pdf->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
			
			$merger = new SetaPDF_Merger();
			$merger->addDocument($tmp);

			$merger->addFile(array(
				'filename' => $source_file,
				'copyLayers' => true
			));


			$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8');
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(true);

			$pdf->AddFont('arialn','');
			$pdf->AddFont('arialn','B');
			$pdf->AddFont('arialn','I');
			$pdf->AddFont('arialn','BI');

			$pdf->Open();

			$PDFmerger_was_used = true;
			$layout_found = true;

		}
	}
	else // layout as JPEG
	{

		$sql = "select order_file_path " . 
			   "from projects " . 
			   "left join order_files on order_file_order = project_order " . 
			   "where order_file_category = 8 and order_file_type= 2 and project_id = " .param("pid") . 
			   " order by order_files.date_created DESC";

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$source_file = ".." . $row["order_file_path"];
			if(file_exists($source_file))
			{
				$pdf->AddPage("L");
				$margin_top = 16;
				$margin_left = 12;
				$pdf->SetXY($margin_left,$margin_top);

				$pdf->SetFont("arialn", "I", 9);
				$pdf->Cell(45, 10, "", 1);

				$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);

				$pdf->SetFont("arialn", "B", 11);
				$pdf->Cell(172, 10, "Layout: " . $project_name, 1, "", "L");
				$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");

				$pdf->Image($source_file,20,40, 200);
				$layout_found = true;
			}
		}
	}
}
*/


//quotes for local works

if($cer_version > 0)
{
	$source_file = $_SERVER['DOCUMENT_ROOT'] . 'files/ln/' . $project["project_number"] . '/cer_offer_comparison_' . param('pid') . '_version_' . $cer_version . '.pdf';
	
	if(file_exists($source_file))
	{
		if($PDFmerger_was_used == false)
		{
		
			$pdfString = $pdf->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
	
			$merger = new SetaPDF_Merger();
			$merger->addDocument($tmp);
		}
		

		$merger->addFile(array(
			'filename' => $source_file,
			'copyLayers' => true
		));

		
		/*
		$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(true);

		$pdf->AddFont('arialn','');
		$pdf->AddFont('arialn','B');
		$pdf->AddFont('arialn','I');
		$pdf->AddFont('arialn','BI');

		$pdf->Open();
		*/

		$PDFmerger_was_used = true;
	
	}
	else
	{
		include("../user/project_offer_comparison_pdf_main.php");
	}
}
else
{
	include("../user/project_offer_comparison_pdf_main.php");
}

//list of variables
include("cer_listofvars_pdf_detail.php");


//check if project has jpg attachments to be added to the booklet
$sql = "select order_file_path " . 
	   "from projects " . 
	   "left join order_files on order_file_order = project_order " . 
	   "where order_file_attach_to_cer_booklet = 1 and order_file_type= 2 and project_id = " .param("pid") . 
	   " order by order_files.date_created DESC";

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	if($layout_file != $row["order_file_path"])
	{
	
		$source_file = ".." . $row["order_file_path"];
		if(file_exists($source_file))
		{
			$pdf->AddPage("L");
			$margin_top = 16;
			$margin_left = 12;
			$pdf->SetXY($margin_left,$margin_top);

			$pdf->SetFont("arialn", "I", 9);
			$pdf->Cell(45, 10, "", 1);

			$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);

			$pdf->SetFont("arialn", "B", 11);
			$pdf->Cell(172, 10, "Layout: " . $project_name, 1, "", "L");
			$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");

			$pdf->Image($source_file,20,40, 200);
		}
	}
}


//check if project has pdf attachments to be added to the booklet

$pdf_print_output = true;

$sql = "select order_file_path " . 
       "from projects " . 
	   "left join order_files on order_file_order = project_order " . 
       "where order_file_attach_to_cer_booklet = 1 and order_file_type = 13 and project_id = " .param("pid") . 
	   " order by order_files.date_created DESC";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if($layout_file != $row["order_file_path"] and strtolower(pathinfo($row["order_file_path"], PATHINFO_EXTENSION)) == 'pdf')
	{
		$source_file = ".." . $row["order_file_path"];
		if(file_exists($source_file))
		{
			
			if($pdf_print_output == true)
			{
				$pdfString = $pdf->output('', 'S');
				$tmp = SetaPDF_Core_Document::loadByString($pdfString);
				
				if($PDFmerger_was_used == false)
				{
					$merger = new SetaPDF_Merger();
				}

				$merger->addDocument($tmp);
				$pdf_print_output = false;
			}

			$merger->addFile(array(
				'filename' => $source_file,
				'copyLayers' => true
			));


			
			$PDFmerger_was_used = true;
			$pdf_print_output = false;

		}
	}
}


$file_name = "CER_Booklet_" . str_replace(" ", "_", $project_name) . "_" . date("Y-m-d") . ".pdf";
if($PDFmerger_was_used == true)
{

	if($pdf_print_output == false)
	{
		$merger->merge();
		$document = $merger->getDocument();

		$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
		$document->save()->finish();
	}
	else
	{
	
		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		
		$merger->merge();
		$document = $merger->getDocument();
		$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
		$document->save()->finish();
	}
}
else
{
	$pdf->Output($file_name, 'I');
}
?>