<?php
/********************************************************************

    cer_listofvars_pdf.php

    Print PDF for List of Variables

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-09-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();


$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";
require "include/get_functions.php";
require "include/financial_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/SetaPDF/Autoload.php');

// Create and setup PDF document
$PDFmerger_was_used = false;

class MYPDF extends TCPDF
{
	
	

}

$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);

$pdf->SetMargins(10, 23, 10);
$pdf->setPrintHeader(false);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');

$pdf->Open();

include("cer_listofvars_pdf_detail.php");



$file_name = "List_of_Variables_" . str_replace(" ", "_", $project["project_number"]) . "_" . date("Y-m-d") . ".pdf";

if($PDFmerger_was_used == true)
{

	//$pdfString = $pdf->output('', 'S');
	//$tmp = SetaPDF_Core_Document::loadByString($pdfString);
	//$merger->addDocument($tmp);
	
	$merger->merge();
	$document = $merger->getDocument();

	$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
	$document->save()->finish();
}
else
{
	$pdf->Output($file_name, 'I');
}

?>