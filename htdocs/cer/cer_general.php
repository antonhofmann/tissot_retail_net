<?php
/********************************************************************

    cer_general.php

    Application Form: general information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/
if(param("pid") > 0)
{
	$result = check_cer_summary(param("pid"));
	$result = check_basic_data(param("pid"));
}


$posdata = get_pos_data($project["project_order"]);


//neighbourhood
if($project["pipeline"] == 0)
{
	$pos_order_sql = "select * from posorders where posorder_order = " . $project["order_id"];
}
elseif($project["pipeline"] == 1)
{
	$pos_order_sql = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
}

param("id", get_id_of_cer_summary(param("pid")));

$currency = get_cer_currency(param("pid"));

//check if business plan can be edited
require_once("include/check_if_business_plan_is_editable.php");


/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_summary", "cer_summary");

include("include/project_head.php");

$form->add_hidden("pid", param("pid"));

$form->add_section(" ");

$form->add_section(" ");

if(($can_edit_business_plan == true 
	and $project["order_actual_order_state_code"] < '890' 
	and $cer_basicdata["cer_basicdata_cer_locked"] == 0 
	and has_access("has_access_to_his_cer") 
	and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") 
	and $project["project_state"] != 2))
{

	$form->add_section("Other Information");

	$line = "concat(user_name, ' ', user_firstname)";
	if ($project["project_local_retail_coordinator"])
	{
		$form->add_lookup("local_retail_coordinator", "Local Project Leader", "users", $line, 0, $project["project_local_retail_coordinator"]);
	}
	else
	{
		$form->add_label("local_retail_coordinator", "Local Project Leader");
	}

	
	$form->add_label("currency", "Client's Currency", 0, $currency["symbol"]);

	$form->add_section("Project Description and Strategy");
	$form->add_multiline("cer_summary_in01_description", "Text", 8);


	$form->add_section("Benefit, Risks, Alternatives");
	$form->add_multiline("cer_summary_in01_benefits", "Text", 8);
		
	
	if(has_access("has_full_access_to_cer"))
	{
		$form->add_section("Other Information");
		$form->add_edit("cer_summary_in01_review_date", "Post Completion Review Date", 0, "", TYPE_DATE);
		$form->add_edit("cer_summary_in01_cernr", "CER Number");
		$form->add_edit("cer_summary_in01_prot01", "Protocol Number 1");
		$form->add_edit("cer_summary_in01_prot02", "Protocol Number 2");

		$form->add_edit("cer_summary_in01_approval_date", "Approval Date", 0, "", TYPE_DATE);
		$form->add_edit("cer_summary_in01_approval_by", "Approved by");
	}
	else
	{
		$form->add_hidden("cer_summary_in01_review_date");
		$form->add_hidden("cer_summary_in01_cernr");
		$form->add_hidden("cer_summary_in01_prot01");
		$form->add_hidden("cer_summary_in01_prot02");

		$form->add_hidden("cer_summary_in01_approval_date");
		$form->add_hidden("cer_summary_in01_approval_by");
	}   
	$form->add_button(FORM_BUTTON_SAVE, "Save Data");

}
else
{

	if($project["project_projectkind"] != 8)
	{
		$form->add_section("PopUp Period");
	}
	else
	{
		$form->add_section("Business Plan Period");
	}

	$tmp = $cer_basicdata["cer_basicdata_firstyear"] . "-" . $cer_basicdata["cer_basicdata_firstmonth"] . " / " .
	$cer_basicdata["cer_basicdata_lastyear"] . "-" . $cer_basicdata["cer_basicdata_lastmonth"] . " / " . $business_plan_period;
	$form->add_label("business_plan_period", "From / To / Duration", 0, $tmp);

	if(count($posleases) > 0)
	{
		
		$rental_duration = "";
		if($posleases["poslease_startdate"] != NULL 
			and $posleases["poslease_startdate"] != '0000-00-00' 
			and $posleases["poslease_enddate"] != NULL
			and $posleases["poslease_enddate"] != '0000-00-00')
		{
			$diff = abs(strtotime($posleases["poslease_enddate"]) - strtotime($posleases["poslease_startdate"]));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

			if($months == 12)
			{
				$months = 0;
				$years++;
			}

			$rental_duration = $years . " years and " . $months . " months";


		}
		
	}

	$form->add_section("Other Information");
	$line = "concat(user_name, ' ', user_firstname)";
	$form->add_lookup("local_retail_coordinator", "Local Project Leader", "users", $line, 0, dbquote($project["project_local_retail_coordinator"]));

	
		
	$form->add_label("currency", "Client's Currency", 0, $currency["symbol"]);

		
		
	$form->add_section("Project Description and Strategy");
	$form->add_label("cer_summary_in01_description", "Text");
	$form->add_section("Benefit, Risks, Alternatives");
	$form->add_label("cer_summary_in01_benefits", "Text");

		
	$form->add_section("Other Information");
	$form->add_label("cer_summary_in01_review_date", "Post Completion Review Date");
	$form->add_label("cer_summary_in01_cernr", "CER Number");
	$form->add_label("cer_summary_in01_prot01", "Protocol Number 1");
	$form->add_label("cer_summary_in01_prot02", "Protocol Number 2");

	$form->add_label("cer_summary_in01_approval_date", "Approval Date");
	$form->add_label("cer_summary_in01_approval_by", "Approved by");
}

$form->populate();
$form->process();



if($form->button(FORM_BUTTON_SAVE))
{
	if(strlen($form->value("cer_summary_in01_description")) > 1000)
	{
		$form->error("The maximum amount of characters allowed in the description is 1000.");
		$error = 1;
	}
	if(strlen($form->value("cer_summary_in01_benefits")) > 1000)
	{
		$form->error("The maximum amount of characters allowed in the description of benefits is 1000.");
		$error = 1;
	}
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();


if($project["project_projectkind"] == 8)
{
	
	if($form_type == "INR03")
	{
		$page->title("INR-03 - Retail Furniture in Third-party Store: General Information");
	}
	elseif($form_type == "AF")
	{
		$page->title("LNR for PopUps: General Information");
	}
	else
	{
		$page->title("LNR/CER for PopUps: General Information");
	}
}
else
{
	$page->title("CER: General Information");
}


$form->render();


require "include/footer_scripts.php";
$page->footer();

?>