<?php
/********************************************************************

    cer_application_investment.php

    Application Form: investment information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";
require_once "../shared/project_cost_functions.php";

check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/

$months = array();
for($i=1;$i<12;$i++) {
	$months[$i] = $i;
}

$currency = get_cer_currency(param("pid"));

//build missing investment records
$result = build_missing_cer_investment_records(param("pid"));


$months = array();
for($i=1;$i<13;$i++)
{
	$months[$i] = $i;
}

$years = array();

for($i=$cer_basicdata["cer_basicdata_firstyear"];$i<=$cer_basicdata["cer_basicdata_lastyear"];$i++)
{
	$years[$i] = $i;
}

if(!in_array(substr($project["project_real_opening_date"], 0, 4), $years))
{
	$years[substr($project["project_real_opening_date"], 0, 4)] = substr($project["project_real_opening_date"], 0, 4);
	asort($years);
}

//get investments

$keymoney = get_pos_intangibles(param("pid"), 15);
$goodwill = get_pos_intangibles(param("pid"), 17);
$construction = get_pos_intangibles(param("pid"), 1);
$fixturing = get_pos_intangibles(param("pid"), 3);
$architectural = get_pos_intangibles(param("pid"), 5);
$equipment = get_pos_intangibles(param("pid"), 7);
$deposit = get_pos_intangibles(param("pid"), 9);
$other1 = get_pos_intangibles(param("pid"), 11);
$other2 = get_pos_intangibles(param("pid"), 13);
$landlord_contribution = get_pos_intangibles(param("pid"), 19);

$dismantling = get_pos_intangibles(param("pid"), 18);
$transportation = get_pos_intangibles(param("pid"), 20);

$amounts = array();
$comments = array();
$depryears = array();
$deprmonths = array();
$cer_totals = 0;

$sql = "select * from cer_investments " .
       "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
	   "where posinvestment_type_intangible = 0 and posinvestment_type_id <> 9 " .
	   " and cer_investment_project = " . param("pid") . 
	   " and cer_investment_cer_version = 0 ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if($row["cer_investment_type"] == 1) //local construction work
	{
		$local_construction_works_for_franchisee_projects = $row["cer_investment_amount_cer_loc"];
	}
	$amounts[$row["cer_investment_id"]] = $row["cer_investment_amount_cer_loc"];
	$comments[$row["cer_investment_id"]] = $row["cer_investment_comment"];
	$depryears[$row["cer_investment_id"]] = $row["cer_investment_depr_years"];
	$deprmonths[$row["cer_investment_id"]] = $row["cer_investment_depr_months"];
	
	if($row["cer_investment_type"] != 19) // landlord's contribution
	{
		$cer_totals = $cer_totals + $row["cer_investment_amount_cer_loc"];
	}
}

//get intangibles
$total_intagibles = 0;
$sql_insc = "select * from cer_investments " . 
			"left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
			"where posinvestment_type_intangible =1 " . 
			" and cer_investment_project = " . param("pid") . 
			" and cer_investment_cer_version = 0 ";

$res = mysql_query($sql_insc) or dberror($sql_insc);
while ($row = mysql_fetch_assoc($res))
{
	$total_intagibles = $total_intagibles + $row["cer_investment_amount_cer_loc"];
}


//get intangibles
$total_deposit = 0;
$sql = "select * from cer_investments " .
       "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
	   "where posinvestment_type_id = 9 " .
	   " and cer_investment_project = " . param("pid") . 
	   " and cer_investment_cer_version = 0 ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$total_deposit = $total_deposit + $row["cer_investment_amount_cer_loc"];
}

$total_landlord_contribution = 0;
$sql = "select * from cer_investments " .
       "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
	   "where posinvestment_type_id = 19 " .
	   " and cer_investment_project = " . param("pid") . 
	   " and cer_investment_cer_version = 0 ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$total_landlord_contribution = $total_landlord_contribution + $row["cer_investment_amount_cer_loc"];
}



$sql_list = "select * from cer_investments " .
            "left join posinvestment_types on posinvestment_type_id = cer_investment_type "; 
$list_filter = "cer_investment_project = " . param("pid") . 
               " and posinvestment_type_intangible = 0 " . 
			   " and cer_investment_cer_version = 0 ";


//check if business plan can be edited
require_once("include/check_if_business_plan_is_editable.php");


/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

include("include/project_head.php");

$form->add_hidden("pid", param("pid"));


if($project["project_projectkind"] != 8) // popup
{

	$form->add_section("Total Investments in " . $currency["symbol"]);
	$form->add_label("total_intangible", "Key Money and Goodwill in " . $currency["symbol"], 0, number_format($total_intagibles,2, ".", "'"));
	$form->add_label("total_investment", "Investments in " . $currency["symbol"], 0, number_format($cer_totals,2, ".", "'"));
	$form->add_label("total", "Total in " . $currency["symbol"], 0, number_format($cer_totals + $total_intagibles,2, ".", "'"));

	$form->add_label("total_deposit", "Deposit in " . $currency["symbol"], 0, number_format($total_deposit,2, ".", "'"));
	
	if($show_business_plan_version_2016 == 1)
	{
		$form->add_label("total_landlord_contribution", "Landlord's Contribution in " . $currency["symbol"], 0, number_format($total_landlord_contribution,2, ".", "'"));

		$form->add_label("total_net", "Total net in " . $currency["symbol"], 0, number_format($cer_totals + $total_intagibles - $total_landlord_contribution,2, ".", "'"));
	}


	if(($can_edit_business_plan == true and $project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer") and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") and $project["project_state"] != 2))
	{
		if($use_old_cer_forms_before_2013 == false)
		{
			
			if($project["project_projectkind"] != 5) //all projects except lease reneval
			{
				$form->add_section("Beginning of the Depreciation Period");
				$form->add_comment("Please use the month and the year of the agreed opening date.");
				$form->add_list("cer_basicdata_firstyear_depr", "First Year of Depreciation Period*", $years, NOTNULL, $cer_basicdata["cer_basicdata_firstyear_depr"]);
				$form->add_list("cer_basicdata_firstmonth_depr", "First Month of Depreciation Period*", $months, NOTNULL, $cer_basicdata["cer_basicdata_firstmonth_depr"]);
			}
			else
			{
				//echo $cer_basicdata["cer_basicdata_firstyear_depr"] . " " . $cer_basicdata["cer_basicdata_firstmonth_depr"];
				$form->add_hidden("cer_basicdata_firstyear_depr", substr($posleases["poslease_startdate"], 0, 4));
				$form->add_hidden("cer_basicdata_firstmonth_depr", substr($posleases["poslease_startdate"], 5, 2));

				$form->add_section("Beginning of the Depreciation Period");
				$form->add_label("firstyear_depr", "First Year of Depreciation Period*", 0, substr($posleases["poslease_startdate"], 0, 4));
				$form->add_label("firstmonth_depr", "First Month of Depreciation Period*", 0,substr($posleases["poslease_startdate"], 5, 2));
			}
		}
			
		
		$form->add_section("Key Money and Goodwill in " . $currency["symbol"]);
		
		$form->add_edit("keymoney", $keymoney["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $keymoney["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
		
		$form->add_edit("goodwill", $goodwill["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $goodwill["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
		
		
		$form->add_edit("keymoney_depr_years", "Depreciation Period in full Years", 0, $keymoney["cer_investment_depr_years"], TYPE_INT, 4);
		$form->add_list("keymoney_depr_months", "Depreciation Period, additional Months", $months, 0, $keymoney["cer_investment_depr_months"]);

		if($use_old_cer_forms_before_2013 == false)
		{
			//$form->add_edit("cer_basicdata_recoverable_keymoney", "Recoverable Keymoney in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_recoverable_keymoney"], TYPE_DECIMAL, 12, 0);
			$form->add_hidden("cer_basicdata_recoverable_keymoney");
		}

		if($project["project_projectkind"] != 5) //all projects except lease reneval
		{
			$form->add_section("Construction, Store fixturing / Furniture and Architectural Services in " . $currency["symbol"]);
			
			if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
			{
				$form->add_edit("construction", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $construction["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
			}
			else
			{
				$form->add_edit("construction", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $local_construction_works_for_franchisee_projects, TYPE_DECIMAL, 12, 2);
			}

			$form->add_edit("construction_depr_years", "Depreciation Period in full Years", 0, $construction["cer_investment_depr_years"], TYPE_INT, 4);
			$form->add_list("construction_depr_months", "Depreciation Period, additional Months", $months, 0, $construction["cer_investment_depr_months"]);
			
			$form->add_edit("fixturing", $fixturing["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $fixturing["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
			

			$form->add_edit("fixturing_depr_years", "Depreciation Period in full Years", 0, $fixturing["cer_investment_depr_years"], TYPE_INT, 4);
			$form->add_list("fixturing_depr_months", "Depreciation Period, additional Months", $months, 0, $fixturing["cer_investment_depr_months"]);

			$form->add_edit("architectural", $architectural["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $architectural["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
			
			$form->add_edit("architectural_depr_years", "Depreciation Period in full Years", 0, $architectural["cer_investment_depr_years"], TYPE_INT, 4);
			$form->add_list("architectural_depr_months", "Depreciation Period, additional Months", $months, 0, $architectural["cer_investment_depr_months"]);

			$form->add_section("Other in " . $currency["symbol"]);
			$form->add_edit("equipment", $equipment["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $equipment["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
			
			$form->add_edit("equipment_depr_years", "Depreciation Period in full Years", 0, $equipment["cer_investment_depr_years"], TYPE_INT, 4);
			$form->add_list("equipment_depr_months", "Depreciation Period, additional Months", $months, 0, $equipment["cer_investment_depr_months"]);

			$form->add_comment("");
		}
		else
		{
			$form->add_hidden("construction");
			$form->add_hidden("construction_depr_years");
			$form->add_hidden("construction_depr_months");
			$form->add_hidden("fixturing");
			$form->add_hidden("fixturing_depr_years");
			$form->add_hidden("fixturing_depr_months");
			$form->add_hidden("architectural");
			$form->add_hidden("architectural_depr_years");
			$form->add_hidden("architectural_depr_months");
			$form->add_hidden("equipment");
			$form->add_hidden("equipment_depr_years");
			$form->add_hidden("equipment_depr_months");
		}
		
		$form->add_edit("deposit", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $deposit["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);

		if($show_business_plan_version_2016 == 1)
		{
			$form->add_edit("landlord_contribution", $landlord_contribution["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $landlord_contribution["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
		}
		else
		{
			$form->add_hidden("landlord_contribution");
		}
		
		if($project["project_projectkind"] != 5) //all projects except lease reneval
		{
			$form->add_comment("");
			$form->add_edit("other1", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other1["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
			$form->add_edit("other1_depr_years", "Depreciation Period in full Years", 0, $other1["cer_investment_depr_years"], TYPE_INT, 4);
			$form->add_list("other1_depr_months", "Depreciation Period, additional Months", $months, 0, $other1["cer_investment_depr_months"]);


			$form->add_comment("");
			$form->add_edit("transportation", $transportation["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $transportation["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);

			$form->add_hidden("old_transportation", $transportation["posinvestment_type_name"]);


			$form->add_edit("transportation_depr_years", "Depreciation Period in full Years", 0, $transportation["cer_investment_depr_years"], TYPE_INT, 4);
			$form->add_list("transportation_depr_months", "Depreciation Period, additional Months", $months, 0, $transportation["cer_investment_depr_months"]);

			$form->add_comment("");
			$form->add_edit("other2", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
		}
		else
		{
			$form->add_hidden("transportation");
			$form->add_hidden("old_transportation");
			$form->add_hidden("transportation_depr_years");
			$form->add_hidden("transportation_depr_months");

			$form->add_hidden("other1");
			$form->add_hidden("other1_depr_years");
			$form->add_hidden("other1_depr_months");


			$form->add_hidden("other2");
		
		}

		$form->add_section("Dismantling Costs " . $currency["symbol"]);
		$form->add_edit("dismantling", $dismantling["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $dismantling["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
		$form->add_edit("dismantling_depr_years", "Depreciation Period in full Years", 0, $dismantling["cer_investment_depr_years"], TYPE_INT, 4);
		$form->add_list("dismantling_depr_months", "Depreciation Period, additional Months", $months, 0, $dismantling["cer_investment_depr_months"]);
		
		
		$form->add_section("Business Partner's Contribution");
		$form->add_edit("cer_basicdata_franchsiee_investment_share", "Business Partner's Contribution in % is", 0, $cer_basicdata["cer_basicdata_franchsiee_investment_share"], TYPE_DECIMAL, 6, 2);

		
		if($project["project_projectkind"] != 5) //all projects except lease reneval
		{
			$form->add_section("Residual Value of Fixed Assets in " . $currency["symbol"] . " (only in case of Renovation or Take Over/Renovation Projects)");
		}
		else
		{
			$form->add_section("Residual Value of Fixed Assets in " . $currency["symbol"]);
		}

		$form->add_edit("keymoney_depr_years", "Depreciation Period in full Years", 0, $keymoney["cer_investment_depr_years"], TYPE_INT, 4);
		
		$form->add_edit("cer_basicdata_residual_value", "Residual Value in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_residual_value"], TYPE_INT, 12);
		
		if($use_old_cer_forms_before_2013 == false)
		{
			
			$form->add_edit("cer_basicdata_residual_depryears", "Depreciation Period in full Years", 0, $cer_basicdata["cer_basicdata_residual_depryears"], TYPE_INT, 4);
			$form->add_list("cer_basicdata_residual_deprmonths", "Depreciation Period, additional Months", $months, 0, $cer_basicdata["cer_basicdata_residual_deprmonths"]);


			if($project["project_projectkind"] != 5 and $form_type == "CER" and has_access("has_full_access_to_cer"))
			{
				$form->add_section("Residual Value of Keymoney in " . $currency["symbol"] . " (only in case of Renovation or Take Over/Renovation Projects)");
			}
			else
			{
				$form->add_section("Residual Value of Keymoney in " . $currency["symbol"]);
			}


			
			$form->add_edit("cer_basicdata_residualkeymoney_value", "Residual Value in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_residualkeymoney_value"], TYPE_INT, 12);

			$form->add_edit("cer_basicdata_residualkeymoney_depryears", "Depreciation Period in full Years", 0, $cer_basicdata["cer_basicdata_residualkeymoney_depryears"], TYPE_INT, 4);
			$form->add_list("cer_basicdata_residualkeymoney_deprmonths", "Depreciation Period, additional Months", $months, 0, $cer_basicdata["cer_basicdata_residualkeymoney_deprmonths"]);
			
		}



		if($project["project_projectkind"] != 5 and $form_type == "CER" and has_access("has_full_access_to_cer"))
		{
			$total_klapproved = 0;
			$form->add_section("KL approved Investments in " . $currency["symbol"]);
			$form->add_label("construction_approved", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $construction["cer_investment_amount_cer_loc_approved"]);
			$total_klapproved = $construction["cer_investment_amount_cer_loc_approved"];

			$form->add_label("fixturing_approved", $fixturing["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $fixturing["cer_investment_amount_cer_loc_approved"]);
			
			$form->add_label("architectural_approved", $architectural["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $architectural["cer_investment_amount_cer_loc_approved"]);
			
			$form->add_label("equipment_approved", $equipment["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $equipment["cer_investment_amount_cer_loc_approved"]);

			$form->add_label("other1_approved", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other1["cer_investment_amount_cer_loc_approved"]);

			$form->add_label("transportation_approved", $transportation["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $transportation["cer_investment_amount_cer_loc_approved"]);

			$form->add_section("Business Partner's Contribution");
			$form->add_label("cer_basicdata_franchsiee_investment_share", "Business Partner Contribution in %", 0, $cer_basicdata["cer_basicdata_franchsiee_investment_share"]);
			
			$form->add_label("dismantling_approved", $dismantling["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $dismantling["cer_investment_amount_cer_loc_approved"]);
						
			$total_klapproved = $total_klapproved + $fixturing["cer_investment_amount_cer_loc_approved"] + $architectural["cer_investment_amount_cer_loc_approved"] + $equipment["cer_investment_amount_cer_loc_approved"] + $other1["cer_investment_amount_cer_loc_approved"]+$dismantling["cer_investment_amount_cer_loc_approved"]+$transportation["cer_investment_amount_cer_loc_approved"];

			
			
			$form->add_label("approved_total", "Total KL approved Investments in Fixed Assets in " . $currency["symbol"], 0, number_format($total_klapproved, 2, ".", "'"));
			
			$form->add_label("keymoney_approved", $keymoney["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $keymoney["cer_investment_amount_cer_loc_approved"]);

			$form->add_label("deposit_approved", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $deposit["cer_investment_amount_cer_loc_approved"]);

			$form->add_label("other2_approved", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_cer_loc_approved"]);

			
			//$form->add_label("deposite", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $deposit["cer_investment_amount_cer_loc"]);
			//$form->add_label("noncapitalized", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_cer_loc"]);
			
			$total_klapproved2 = $total_klapproved + $keymoney["cer_investment_amount_cer_loc_approved"] + $deposit["cer_investment_amount_cer_loc_approved"] + $other2["cer_investment_amount_cer_loc_approved"];

			$form->add_label("approved_total2", "Total Project Costs KL approved amount in " . $currency["symbol"], 0, number_format($total_klapproved2, 2, ".", "'"));
				
		}
		
		
		
		if(($can_edit_business_plan == true and $project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer") and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") and $project["project_state"] != 2))
		{
			$form->add_button("recalculate", "Recalculate Investments from Project's Budget");
		}

		$form->add_button("form_save", "Save Data");
	}
	else
	{
		
		if($use_old_cer_forms_before_2013 == false)
		{
			$form->add_section("Beginning of the Depreciation Period");
			$form->add_label("cer_basicdata_firstyear_depr", "First Year of Depreciation Period", 0,$cer_basicdata["cer_basicdata_firstyear_depr"]);
			$form->add_label("cer_basicdata_firstmonth_depr", "First Month of Depreciation Period",0,$cer_basicdata["cer_basicdata_firstmonth_depr"]);
		}
		
		$form->add_section("Key Money and Goodwill in " . $currency["symbol"]);
		$form->add_label("keymoney", $keymoney["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $keymoney["cer_investment_amount_cer_loc"]);
		$form->add_label("goodwill", $goodwill["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $goodwill["cer_investment_amount_cer_loc"]);
		$form->add_label("keymoney_depr_years", "Depreciation Period in full Years", 0, $keymoney["cer_investment_depr_years"]);
		$form->add_label("keymoney_depr_months", "Depreciation Period, additional Months", 0, $keymoney["cer_investment_depr_months"]);

		if($use_old_cer_forms_before_2013 == false)
		{
			//$form->add_label("cer_basicdata_recoverable_keymoney", "Recoverable Keymoney in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_recoverable_keymoney"]);
			$form->add_hidden("cer_basicdata_recoverable_keymoney");
		}

		
		if($project["project_projectkind"] != 5) //all projects except lease reneval
		{
			$form->add_section("Construction, Store fixturing / Furniture and Architectural Services in " . $currency["symbol"]);
			
			$form->add_label("construction", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $construction["cer_investment_amount_cer_loc"]);
			
			$form->add_label("construction_depr_years", "Depreciation Period in full Years", 0, $construction["cer_investment_depr_years"]);
			$form->add_label("construction_depr_months", "Depreciation Period, additional Months", 0, $construction["cer_investment_depr_months"]);

			$form->add_label("fixturing", $fixturing["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $fixturing["cer_investment_amount_cer_loc"]);
			$form->add_label("fixturing_depr_years", "Depreciation Period in full Years", 0, $fixturing["cer_investment_depr_years"]);
			$form->add_label("fixturing_depr_months", "Depreciation Period, additional Months", 0, $fixturing["cer_investment_depr_months"]);

			$form->add_label("architectural", $architectural["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $architectural["cer_investment_amount_cer_loc"]);
			$form->add_label("architectural_depr_years", "Depreciation Period in full Years", 0, $architectural["cer_investment_depr_years"]);
			$form->add_label("architectural_depr_months", "Depreciation Period, additional Months", 0, $architectural["cer_investment_depr_months"]);

			$form->add_section("Other in " . $currency["symbol"]);
			$form->add_label("equipment", $equipment["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $equipment["cer_investment_amount_cer_loc"]);
			$form->add_label("equipment_depr_years", "Depreciation Period in full Years", 0, $equipment["cer_investment_depr_years"]);
			$form->add_label("equipment_depr_months", "Depreciation Period, additional Months", 0, $equipment["cer_investment_depr_months"]);

			$form->add_comment("");
		

			$form->add_label("deposit", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $deposit["cer_investment_amount_cer_loc"]);
			
			if($show_business_plan_version_2016 == 1)
			{
				$form->add_label("total_landlord_contribution", $landlord_contribution["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $total_landlord_contribution["cer_investment_amount_cer_loc"]);
			}

			$form->add_comment("");
			
			
			$form->add_label("other1", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other1["cer_investment_amount_cer_loc"]);
			$form->add_label("other1_depr_years", "Depreciation Period in full Years", 0, $other1["cer_investment_depr_years"]);
			$form->add_label("other1_depr_years", "Depreciation Period, additional Months", 0, $other1["cer_investment_depr_months"]);


			$form->add_comment("");
			$form->add_label("transportation", $transportation["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $transportation["cer_investment_amount_cer_loc"]);
			$form->add_hidden("old_transportation", $transportation["posinvestment_type_name"]);
			$form->add_label("transportation_depr_years", "Depreciation Period in full Years", 0, $transportation["cer_investment_depr_years"]);
			$form->add_label("transportation_depr_years", "Depreciation Period, additional Months", 0, $transportation["cer_investment_depr_months"]);

			$form->add_comment("");
			$form->add_label("other2", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_cer_loc"]);

			$form->add_section("Dismantling Costs " . $currency["symbol"]);
			$form->add_edit("dismantling", $dismantling["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $dismantling["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
			$form->add_edit("dismantling_depr_years", "Depreciation Period in full Years", 0, $dismantling["cer_investment_depr_years"], TYPE_INT, 4);
			$form->add_list("dismantling_depr_months", "Depreciation Period, additional Months", $months, 0, $dismantling["cer_investment_depr_months"]);

			$form->add_section("Business Partner's Contribution");
			$form->add_edit("cer_basicdata_franchsiee_investment_share", "Business Partner's Contribution in % is", 0, $cer_basicdata["cer_basicdata_franchsiee_investment_share"], TYPE_DECIMAL, 6, 2);
			
			$form->add_section("Residual Value of Fixed Assets in " . $currency["symbol"] . " (only in case of Renovation or Take Over/Renovation Projects)");
			$form->add_label("cer_basicdata_residual_value", "Residual Value in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_residual_value"]);
			

			if($use_old_cer_forms_before_2013 == false)
			{
				
				$form->add_label("cer_basicdata_residual_depryears", "Depreciation Period in full Years", 0, $cer_basicdata["cer_basicdata_residual_depryears"]);
				$form->add_label("cer_basicdata_residual_deprmonths", "Depreciation Period, additional Months", 0, $cer_basicdata["cer_basicdata_residual_deprmonths"]);
				

				$form->add_section("Residual Value of Keymoney in " . $currency["symbol"] . " (only in case of Lease Renewal or Renovation or Take Over/Renovation Projects)");

				$form->add_label("cer_basicdata_residualkeymoney_value", "Residual Value in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_residualkeymoney_value"]);

				$form->add_label("cer_basicdata_residualkeymoney_depryears", "Depreciation Period in full Years", 0, $cer_basicdata["cer_basicdata_residualkeymoney_depryears"]);
				$form->add_label("cer_basicdata_residualkeymoney_deprmonths", "Depreciation Period, additional Months", 0, $cer_basicdata["cer_basicdata_residualkeymoney_deprmonths"]);
			}
		}
		else
		{
				$form->add_label("deposit", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $deposit["cer_investment_amount_cer_loc"]);

				if($show_business_plan_version_2016 == 1)
				{
					$form->add_label("total_landlord_contribution", $landlord_contribution["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $total_landlord_contribution["cer_investment_amount_cer_loc"]);
				}

				$form->add_comment("");

				
				$form->add_section("Residual Value of Fixed Assets in " . $currency["symbol"]);
				$form->add_label("cer_basicdata_residual_value", "Residual Value in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_residual_value"]);
				

				if($use_old_cer_forms_before_2013 == false)
				{
					
					$form->add_label("cer_basicdata_residual_depryears", "Depreciation Period in full Years", 0, $cer_basicdata["cer_basicdata_residual_depryears"]);
					$form->add_label("cer_basicdata_residual_deprmonths", "Depreciation Period, additional Months", 0, $cer_basicdata["cer_basicdata_residual_deprmonths"]);
				
					
					$form->add_section("Residual Value of Keymoney in " . $currency["symbol"]);

					$form->add_label("cer_basicdata_residualkeymoney_value", "Residual Value in " . $currency["symbol"], 0, $cer_basicdata["cer_basicdata_residualkeymoney_value"]);

					$form->add_label("cer_basicdata_residualkeymoney_depryears", "Depreciation Period in full Years", 0, $cer_basicdata["cer_basicdata_residualkeymoney_depryears"]);
					$form->add_label("cer_basicdata_residualkeymoney_deprmonths", "Depreciation Period, additional Months", 0, $cer_basicdata["cer_basicdata_residualkeymoney_deprmonths"]);
				}



		}
	}


	/********************************************************************
		Populate form and process button clicks
	*********************************************************************/ 
	$form->populate();
	$form->process();

	if($form->button("recalculate"))
	{
		$result = update_exchange_rates(param("pid"));
		$order_currency = get_orders_currency($project["project_order"]);
		$budget = get_project_budget_totals(param("pid"), $order_currency);
		$result = update_investments_from_project_cost_sheet(param("pid"), $budget, 0);
	
		$link = "cer_application_investments.php?pid=" . param("pid");
		redirect($link);
	}
	elseif($form->button("form_save"))
	{
		//check if depreciation period in years and months exceeds business plan period
		$error = 0;
		if($cer_basicdata["cer_extend_depreciation_period"] != 1)
		{
			//get number of months for depreciation
			if($form->value("cer_basicdata_firstyear_depr") > $cer_basicdata["cer_basicdata_firstyear"])
			{
				$max_number_of_months = 13 - $form->value("cer_basicdata_firstmonth_depr");
				for($i = $form->value("cer_basicdata_firstyear_depr")+1;$i<$cer_basicdata["cer_basicdata_lastyear"];$i++)
				{
					$max_number_of_months = $max_number_of_months + 12;
				}
				$max_number_of_months = $max_number_of_months + $cer_basicdata["cer_basicdata_lastmonth"];
			}
			elseif($form->value("cer_basicdata_firstmonth_depr") > $cer_basicdata["cer_basicdata_firstmonth"])
			{
				$max_number_of_months = 13 - $form->value("cer_basicdata_firstmonth_depr");
				for($i = $cer_basicdata["cer_basicdata_firstyear"]+1;$i<$cer_basicdata["cer_basicdata_lastyear"];$i++)
				{
					$max_number_of_months = $max_number_of_months + 12;
				}
				$max_number_of_months = $max_number_of_months + $cer_basicdata["cer_basicdata_lastmonth"];
			}
			else
			{
				$max_number_of_months = 13 - $cer_basicdata["cer_basicdata_firstmonth"];
				for($i = $cer_basicdata["cer_basicdata_firstyear"]+1;$i<$cer_basicdata["cer_basicdata_lastyear"];$i++)
				{
					$max_number_of_months = $max_number_of_months + 12;
				}
				$max_number_of_months = $max_number_of_months + $cer_basicdata["cer_basicdata_lastmonth"];
			}


			

			
			if(($form->value("keymoney_depr_years")*12 + $form->value("keymoney_depr_months")) > $max_number_of_months)
			{
				$error = 1;
				$form->error("Keymoney/Goodwill: The depreciation period must not extend the business plan period.");
			}
			elseif(($form->value("construction_depr_years")*12 + $form->value("construction_depr_months")) > $max_number_of_months)
			{
				$error = 1;
				$form->error("Local construction: The depreciation period must not extend the business plan period.");
			}
			elseif(($form->value("fixturing_depr_years")*12 + $form->value("fixturing_depr_months")) > $max_number_of_months)
			{
				$error = 1;
				$form->error("Store fixturing / Furniture: The depreciation period must not extend the business plan period.");
			}
			elseif(($form->value("architectural_depr_years")*12 + $form->value("architectural_depr_months")) > $max_number_of_months)
			{
				$error = 1;
				$form->error("Architectural services: The depreciation period must not extend the business plan period.");
			}
			elseif(($form->value("equipment_depr_years")*12 + $form->value("equipment_depr_months")) > $max_number_of_months)
			{
				$error = 1;
				$form->error("Equipment: The depreciation period must not extend the business plan period.");
			}
			elseif(($form->value("other1_depr_years")*12 + $form->value("other1_depr_months")) > $max_number_of_months)
			{
				$error = 1;
				$form->error("Other: The depreciation period must not extend the business plan period.");
			}
			elseif(($form->value("transportation_depr_years")*12 + $form->value("other1_depr_months")) > $max_number_of_months)
			{
				$error = 1;
				$form->error("Installation/Transportation: The depreciation period must not extend the business plan period.");
			}
			elseif(($form->value("cer_basicdata_residual_depryears")*12 + $form->value("cer_basicdata_residual_deprmonths")) > $max_number_of_months)
			{
				$error = 1;
				$form->error("Residual value of fixed assets: The depreciation period must not extend the business plan period.");
			}
			elseif(($form->value("cer_basicdata_residualkeymoney_depryears")*12 + $form->value("cer_basicdata_residualkeymoney_deprmonths")) > $max_number_of_months)
			{
				$error = 1;
				$form->error("Residual value of keymoney: The depreciation must not extend the business plan period.");
			}

		}


		if($form->value("construction") > 0 
			and (!$form->value("construction_depr_years") and !$form->value("construction_depr_months")))
		{
			$error = 1;
			$form->error("Please indicate a depreciation period for the local construction.");
		}
		elseif($form->value("fixturing") > 0 
			and (!$form->value("fixturing_depr_years") and !$form->value("fixturing_depr_months")))
		{
			$error = 1;
			$form->error("Please indicate a depreciation period for the Store fixturing / furniture.");
		}
		elseif($form->value("architectural") > 0 
			and (!$form->value("architectural_depr_years") and !$form->value("architectural_depr_months")))
		{
			$error = 1;
			$form->error("Please indicate a depreciation period for the architectural services.");
		}
		elseif($form->value("equipment") > 0 
			and (!$form->value("equipment_depr_years") and !$form->value("equipment_depr_months")))
		{
			$error = 1;
			$form->error("Please indicate a depreciation period for the costs of equipment .");
		}
		elseif($form->value("other1") > 0 
			and (!$form->value("other1_depr_years") and !$form->value("other1_depr_months")))
		{
			$error = 1;
			$form->error("Please indicate a depreciation period for the other.");
		}
		elseif($form->value("transportation") > 0 
			and (!$form->value("transportation_depr_years") and !$form->value("transportation_depr_months")))
		{
			$error = 1;
			$form->error("Please indicate a depreciation period for the installation/transportation.");
		}
		
		
		
		
		if($error == 0 and $form->validate())
		{
			//save intangibles type keymoney
			$fields = array();
		
			$value = dbquote($form->value("keymoney"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			$value = dbquote($form->value("keymoney_depr_years"));
			$fields[] = "cer_investment_depr_years = " . $value;

			$value = dbquote($form->value("keymoney_depr_months"));
			$fields[] = "cer_investment_depr_months = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($keymoney["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);

			//save intangibles type goodwill
			$fields = array();
		
			$value = dbquote($form->value("goodwill"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			$value = dbquote($form->value("keymoney_depr_years"));
			$fields[] = "cer_investment_depr_years = " . $value;

			$value = dbquote($form->value("keymoney_depr_months"));
			$fields[] = "cer_investment_depr_months = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($goodwill["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);

			//save investment type construction
			$fields = array();
		
			$value = dbquote($form->value("construction"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			$value = dbquote($form->value("construction_depr_years"));
			$fields[] = "cer_investment_depr_years = " . $value;

			$value = dbquote($form->value("construction_depr_months"));
			$fields[] = "cer_investment_depr_months = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($construction["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);

			//save investment type fixturing
			$fields = array();
		
			$value = dbquote($form->value("fixturing"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			
			$value = dbquote($form->value("fixturing_depr_years"));
			$fields[] = "cer_investment_depr_years = " . $value;

			$value = dbquote($form->value("fixturing_depr_months"));
			$fields[] = "cer_investment_depr_months = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($fixturing["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);


			//save investment type architectural
			$fields = array();
		
			$value = dbquote($form->value("architectural"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			
			$value = dbquote($form->value("architectural_depr_years"));
			$fields[] = "cer_investment_depr_years = " . $value;

			$value = dbquote($form->value("architectural_depr_months"));
			$fields[] = "cer_investment_depr_months = " . $value;

			
			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($architectural["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);

			//save investment type equipment
			$fields = array();
		
			$value = dbquote($form->value("equipment"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			
			$value = dbquote($form->value("equipment_depr_years"));
			$fields[] = "cer_investment_depr_years = " . $value;

			$value = dbquote($form->value("equipment_depr_months"));
			$fields[] = "cer_investment_depr_months = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($equipment["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);

			//save investment type deposit
			$fields = array();
		
			$value = dbquote($form->value("deposit"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " .$value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($deposit["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);


			//save investment type landlord's contribution
			$fields = array();
		
			$value = dbquote(abs($form->value("landlord_contribution")));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;
		
			$value1 = "current_timestamp";
			$fields[] = "date_modified = " .$value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($landlord_contribution["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);

			
			
			//save investment type other costs
			$fields = array();
		
			$value = dbquote($form->value("other1"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			
			$value = dbquote($form->value("other1_depr_years"));
			$fields[] = "cer_investment_depr_years = " . $value;

			$value = dbquote($form->value("other1_depr_months"));
			$fields[] = "cer_investment_depr_months = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($other1["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);

			//save investment type transportation costs
			$fields = array();
		
			$value = dbquote($form->value("transportation"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			
			$value = dbquote($form->value("transportation_depr_years"));
			$fields[] = "cer_investment_depr_years = " . $value;

			$value = dbquote($form->value("transportation_depr_months"));
			$fields[] = "cer_investment_depr_months = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $transportation["cer_investment_id"];
			mysql_query($sql) or dberror($sql);


			if($form->value("transportation") != $form->value("old_transportation"))
			{
				$sql = "update cer_investments set cer_investment_child_amounts = '' where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote(param('pid'));
				mysql_query($sql) or dberror($sql);
			}

			//save investment type other non captalized costs
			$fields = array();
		
			$value = dbquote($form->value("other2"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;


			
			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($other2["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);


			//save dismantling costs
			$fields = array();

			$value = dbquote($form->value("dismantling"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			
			$value = dbquote($form->value("dismantling_depr_years"));
			$fields[] = "cer_investment_depr_years = " . $value;

			$value = dbquote($form->value("dismantling_depr_months"));
			$fields[] = "cer_investment_depr_months = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($dismantling["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);

			//update cer_basic_data
			$fields = array();
		
			$value = dbquote($form->value("cer_basicdata_residual_value"));
			$fields[] = "cer_basicdata_residual_value = " . $value;

			

			if($use_old_cer_forms_before_2013 == false)
			{
				
				$value = dbquote($form->value("cer_basicdata_firstyear_depr"));
				$fields[] = "cer_basicdata_firstyear_depr = " . $value;

				$value = dbquote($form->value("cer_basicdata_firstmonth_depr"));
				$fields[] = "cer_basicdata_firstmonth_depr = " . $value;
				
				$value = dbquote($form->value("cer_basicdata_residual_depryears"));
				$fields[] = "cer_basicdata_residual_depryears = " . $value;

				$value = dbquote($form->value("cer_basicdata_residual_deprmonths"));
				$fields[] = "cer_basicdata_residual_deprmonths = " . $value;

				$value = dbquote($form->value("cer_basicdata_residualkeymoney_value"));
				$fields[] = "cer_basicdata_residualkeymoney_value = " . $value;

				$value = dbquote($form->value("cer_basicdata_residualkeymoney_depryears"));
				$fields[] = "cer_basicdata_residualkeymoney_depryears = " . $value;

				$value = dbquote($form->value("cer_basicdata_residualkeymoney_deprmonths"));
				$fields[] = "cer_basicdata_residualkeymoney_deprmonths = " . $value;

				$value = dbquote($form->value("cer_basicdata_recoverable_keymoney"));
				$fields[] = "cer_basicdata_recoverable_keymoney = " . $value;

				$value = dbquote($form->value("cer_basicdata_franchsiee_investment_share"));
				$fields[] = "cer_basicdata_franchsiee_investment_share = " . $value;
			}

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " .$value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_basicdata set " . join(", ", $fields) . 
				" where cer_basicdata_project = " . param("pid") . 
				" and cer_basicdata_version = 0 ";

			mysql_query($sql) or dberror($sql);

			//update expenses by savings from landloard's contribution
			$result = update_expenses_by_landlord_contribution(param("pid"), abs($form->value("landlord_contribution")), $cer_basicdata["cer_basicdata_firstyear"], $cer_basicdata["cer_basicdata_lastyear"], $cer_basicdata["cer_basicdata_firstmonth"], $cer_basicdata["cer_basicdata_lastmonth"]);

			$link = "cer_application_investments.php?pid=" . param("pid");
			redirect($link);
		}
	}

	/********************************************************************
		render page
	*********************************************************************/
	$page = new Page("cer_projects");


	require "include/project_page_actions.php";


	$page->header();
	
	if($form_type == "INR03")
	{
		$page->title("INR-03 - Retail Furniture in Third-party Store: Investment Information");
	}
	else
	{
		$page->title("Business Plan: Investment Information");
	}
	require_once("include/tabs2016.php");
	$form->render();

	require "include/footer_scripts.php";
	$page->footer();
}
else
{
	$form->add_section("Total Investments in " . $currency["symbol"]);
	$form->add_label("total", "Total Investments in " . $currency["symbol"], 0, number_format($cer_totals + $total_intagibles,2, ".", "'"));
	$form->add_label("total_deposit", "Deposit in " . $currency["symbol"], 0, number_format($total_deposit,2, ".", "'"));
	
	if(($can_edit_business_plan == true and $project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer") and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") and $project["project_state"] != 2))
	{
		$form->add_section("Construction, Store fixturing / Furniture and Architectural Services in " . $currency["symbol"]);
		
		if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
		{
			$form->add_edit("construction", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $construction["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
		}
		else
		{
			$form->add_edit("construction", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $local_construction_works_for_franchisee_projects, TYPE_DECIMAL, 12, 2);
		}

		$form->add_edit("fixturing", $fixturing["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $fixturing["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);

		$form->add_edit("architectural", $architectural["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $architectural["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
		
		$form->add_edit("equipment", $equipment["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $equipment["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
		
		$form->add_edit("other1", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other1["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);

		$form->add_edit("other2", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_cer_loc"], TYPE_DECIMAL, 12, 2);
		

		



		if($form_type == "CER" and has_access("has_full_access_to_cer"))
		{
			$total_klapproved = 0;
			$form->add_section("KL approved Investments in " . $currency["symbol"]);
			$form->add_label("construction_approved", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $construction["cer_investment_amount_cer_loc_approved"]);
			$total_klapproved = $construction["cer_investment_amount_cer_loc_approved"];

			$form->add_label("fixturing_approved", $fixturing["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $fixturing["cer_investment_amount_cer_loc_approved"]);
			
			$form->add_label("architectural_approved", $architectural["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $architectural["cer_investment_amount_cer_loc_approved"]);
			
			$form->add_label("equipment_approved", $equipment["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $equipment["cer_investment_amount_cer_loc_approved"]);

			$form->add_label("other1_approved", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other1["cer_investment_amount_cer_loc_approved"]);

			$form->add_label("dismantling_approved", $dismantling["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $dismantling["cer_investment_amount_cer_loc_approved"]);
			
			$total_klapproved = $total_klapproved + $fixturing["cer_investment_amount_cer_loc_approved"] + $architectural["cer_investment_amount_cer_loc_approved"] + $equipment["cer_investment_amount_cer_loc_approved"] + $other1["cer_investment_amount_cer_loc_approved"]+$dismantling["cer_investment_amount_cer_loc_approved"];

			$form->add_label("approved_total", "Total KL approved Investments in Fixed Assets in " . $currency["symbol"], 0, number_format($total_klapproved, 2, ".", "'"));
			
			
			$form->add_label("other2_approved", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_cer_loc_approved"]);

			//$form->add_label("deposite", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $deposit["cer_investment_amount_cer_loc"]);
			//$form->add_label("noncapitalized", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_cer_loc"]);
			
			$total_klapproved2 = $total_klapproved + $keymoney["cer_investment_amount_cer_loc_approved"] + $deposit["cer_investment_amount_cer_loc_approved"] + $other2["cer_investment_amount_cer_loc_approved"];

			$form->add_label("approved_total2", "Total Project Costs KL approved amount in " . $currency["symbol"], 0, number_format($total_klapproved2, 2, ".", "'"));
				
		}
		
		
		
		if($can_edit_business_plan == true) {
			$form->add_button("recalculate", "Recalculate Investments from Project's Budget");
		}
		$form->add_button("form_save", "Save Data");
	}
	else
	{
		$form->add_section("Construction, Store fixturing / Furniture and Architectural Services in " . $currency["symbol"]);
		
		$form->add_label("construction", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $construction["cer_investment_amount_cer_loc"]);
		
		
		$form->add_label("fixturing", $fixturing["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $fixturing["cer_investment_amount_cer_loc"]);
		

		$form->add_label("architectural", $architectural["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $architectural["cer_investment_amount_cer_loc"]);
		

		$form->add_label("equipment", $equipment["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $equipment["cer_investment_amount_cer_loc"]);
		
				
		$form->add_label("other1", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other1["cer_investment_amount_cer_loc"]);


		$form->add_label("other2", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_cer_loc"]);

	}


	/********************************************************************
		Populate form and process button clicks
	*********************************************************************/ 
	$form->populate();
	$form->process();

	if($form->button("recalculate"))
	{
		$result = update_exchange_rates(param("pid"));
		$order_currency = get_orders_currency($project["project_order"]);
		$budget = get_project_budget_totals(param("pid"), $order_currency);
		$result = update_investments_from_project_cost_sheet(param("pid"), $budget, 0);
	
		$link = "cer_application_investments.php?pid=" . param("pid");
		redirect($link);
	}
	elseif($form->button("form_save"))
	{
		
		//check if depreciation period in years and months exceeds business plan period
		$error = 0;
		if($cer_basicdata["cer_extend_depreciation_period"] != 1)
		{
			//maximum allowed years
			if($form->value("cer_basicdata_firstyear_depr") > $cer_basicdata["cer_basicdata_firstyear"])
			{
				$max_years = $cer_basicdata["cer_basicdata_lastyear"] - $cer_basicdata["cer_basicdata_firstyear"] + 1;
			}
			else
			{
				$max_years = $cer_basicdata["cer_basicdata_lastyear"] - $form->value("cer_basicdata_firstyear_depr") + 1;
			}

			if($form->value("keymoney_depr_years") > $max_years)
			{
				$error = 1;
				$form->error("Keymoney/Goodwill: The depreciation period in full years must not extend the business plan period.");
			}
			if($form->value("construction_depr_years") > $max_years)
			{
				$error = 1;
				$form->error("Local construction: The depreciation period in full years must not extend the business plan period.");
			}
			if($form->value("fixturing_depr_years") > $max_years)
			{
				$error = 1;
				$form->error("Store fixturing / Furniture: The depreciation period in full years must not extend the business plan period.");
			}
			if($form->value("architectural_depr_years") > $max_years)
			{
				$error = 1;
				$form->error("Architectural services: The depreciation period in full years must not extend the business plan period.");
			}
			if($form->value("equipment_depr_years") > $max_years)
			{
				$error = 1;
				$form->error("Equipment: The depreciation period in full years must not extend the business plan period.");
			}
			if($form->value("other1_depr_years") > $max_years)
			{
				$error = 1;
				$form->error("Other: The depreciation period in full years must not extend the business plan period.");
			}
			if($form->value("cer_basicdata_residual_depryears") > $max_years)
			{
				$error = 1;
				$form->error("Residual value of fixed assets: The depreciation period in full years must not extend the business plan period.");
			}
			if($form->value("cer_basicdata_residualkeymoney_depryears") > $max_years)
			{
				$error = 1;
				$form->error("Residual value of keymoney: The depreciation period in full years must not extend the business plan period.");
			}

		}


		if($form->value("construction") > 0 
			and (!$form->value("construction_depr_years") and !$form->value("construction_depr_months")))
		{
			$error = 1;
			$form->error("Please indicate a depreciation period for the local construction.");
		}
		elseif($form->value("fixturing") > 0 
			and (!$form->value("fixturing_depr_years") and !$form->value("fixturing_depr_months")))
		{
			$error = 1;
			$form->error("Please indicate a depreciation period for the Store fixturing / Furniture.");
		}
		elseif($form->value("architectural") > 0 
			and (!$form->value("architectural_depr_years") and !$form->value("architectural_depr_months")))
		{
			$error = 1;
			$form->error("Please indicate a depreciation period for the architectural services.");
		}
		elseif($form->value("equipment") > 0 
			and (!$form->value("equipment_depr_years") and !$form->value("equipment_depr_months")))
		{
			$error = 1;
			$form->error("Please indicate a depreciation period for the costs of equipment .");
		}
		elseif($form->value("other1") > 0 
			and (!$form->value("other1_depr_years") and !$form->value("other1_depr_months")))
		{
			$error = 1;
			$form->error("Please indicate a depreciation period for the other costs.");
		}
		
		
		
		if($error == 0 and $form->validate())
		{
			//save intangibles type keymoney
			$fields = array();
		
			//save investment type construction
			$fields = array();
		
			$value = dbquote($form->value("construction"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($construction["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);

			//save investment type fixturing
			$fields = array();
		
			$value = dbquote($form->value("fixturing"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			
			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($fixturing["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);


			//save investment type architectural
			$fields = array();
		
			$value = dbquote($form->value("architectural"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			
			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($architectural["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);

			//save investment type equipment
			$fields = array();
		
			$value = dbquote($form->value("equipment"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			
			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($equipment["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);

			

			//save investment type other costs
			$fields = array();
		
			$value = dbquote($form->value("other1"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			
			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($other1["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);

			//save investment type other non captalized costs
			$fields = array();
		
			$value = dbquote($form->value("other2"));
			$fields[] = "cer_investment_amount_cer_loc = " . $value;


			
			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				" where cer_investment_id = " . dbquote($other2["cer_investment_id"]) . 
				" and cer_investment_cer_version = 0 ";
			mysql_query($sql) or dberror($sql);

		
			//$form->message("Your data has bee saved.");

			$link = "cer_application_investments.php?pid=" . param("pid");
			redirect($link);
		}
	}

	/********************************************************************
		render page
	*********************************************************************/
	$page = new Page("cer_projects");


	require "include/project_page_actions.php";


	$page->header();
	
	if($form_type == "INR03")
	{
		$page->title("INR-03 - Retail Furniture in Third-party Store: Investment Information");
	}
	elseif($form_type == "AF")
	{
		$page->title("LNR for PopUps: Investment Information");
	}
	else
	{
		$page->title("LNR/CER for PopUps: Investment Information");
	}
	require_once("include/tabs2016.php");
	$form->render();

	require "include/footer_scripts.php";
	$page->footer();
}

?>