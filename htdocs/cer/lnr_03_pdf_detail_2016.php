<?php
/********************************************************************

    lnr_03_pdf_detail.php

    Print Detail Form LNR-03.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-06-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-17
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.
*********************************************************************/
$currency_symbol = get_currencys_symbol($ln_basicdata["ln_basicdata_currency"]);


$units_of_measurement = array();
$units_of_measurement[1] = "km";
$units_of_measurement[2] = "miles";


//get sellout data
$sellout_data = array();

$sql = "select * from ln_basicdata_inr03 " . 
	   "left join posaddresses on posaddress_id = ln_basicdata_lnr03_posaddress_id_da " .
	   " left join addresses on address_id = posaddress_franchisee_id " . 
	   "left join places on place_id = posaddress_place_id " .
	   "left join posowner_types on posowner_type_id = posaddress_ownertype " . 
	   "left join postypes on postype_id = posaddress_store_postype " . 
       "where ln_basicdata_lnr03_cer_version = " . $ln_version . 
	   "  and ln_basicdata_lnr03_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"] . 
	   "  and ln_basicdata_lnr03_cer_version = " . $ln_version .
	   " order by ln_basicdata_lnr03_distance, posowner_type_name, postype_name";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$posname = $row["posaddress_name"];
	$type = $row["posowner_type_name"] . " / " . $row["postype_name"];

	$distance = $row["ln_basicdata_lnr03_distance"];
	if($row["ln_basicdata_lnr03_distance_unit"] > 0)
	{
		$distance .= " " . $units_of_measurement[$row["ln_basicdata_lnr03_distance_unit"]];
	}

	$total_surface = $row["posaddress_store_totalsurface"];
	
	$sellout_data[] = array("owner"=>substr($row["address_company"], 0, 48), 
							"posname"=>$posname, 
							"type"=>$type,
		                    "total_surface"=>$total_surface,
							"distance"=>$distance, 
							"watches"=>$row["ln_basicdata_lnr03_watches_units"], 
							"bijoux"=>$row["ln_basicdata_lnr03_bijoux_units"], 
							"grossales"=>$row["ln_basicdata_lnr03_grossales"], 
							"year"=>$row["ln_basicdata_lnr03_year"],
		                    "month"=>$row["ln_basicdata_lnr03_month"],
							"take_over_date"=>$row["posaddress_takeover_date"], 
							"opening_date"=>$row["posaddress_store_openingdate"], 
							"planned_closing"=>to_system_date($row["posaddress_store_planned_closingdate"])
		);
}

if(count($sellout_data) > 0)
{

	//page 2
	$pdf->AddPage("L", "A4");

	//Logo
	$pdf->Image('../pictures/brand_logo.jpg',8,8,33);
	
	if(!isset($page_title)) {$page_title = '';}
	
	if(isset($version_date) and $version_date)
	{
		$pdf->SetFont('arialn','B',12);
		//Move to the right
		$pdf->Cell(40);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title . $version_date,0,0,'R');
		//Line break
		$pdf->SetY(23);
	}
	else
	{
		//arialn bold 15
		$pdf->SetFont('arialn','B',18);
		//Move to the right
		$pdf->Cell(80);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title,0,0,'R');
		//Line break
		$pdf->SetY(23);
	}




	$x = 8;
	$pdf->setXY(8, $pdf->getY());
	$pdf->SetFont('arialn','B',10);
	
	if($project["project_cost_type"] == 1)
	{
		$pdf->Cell(280,4,"LEASE NEGOTIATION - ADDITIONAL ANALYSIS",1, 0, 'L', 1);
	}
	else
	{
		$pdf->Cell(280,4,"ADDITIONAL ANALYSIS",1, 0, 'L', 1);
	}
	$pdf->Ln();

	$y = $pdf->GetY()+10;
	$y_at_start = $y - 1;

		
		
	// 5. Top 10 distribution analysis of current stores in the area
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(280, 6, "Top 10 distribution analysis of current stores in the area (" . $project["order_shop_address_company"] . ")", 1, "", "L");
	

			
	$y = $pdf->GetY()+6;
	$pdf->SetXY($x,$y);

	$pdf->SetFont("arialn", "B", 7);
	
	$pdf->MultiCell(58,8,"Owner company name",1, "L", false, 0);
	$pdf->MultiCell(73,8,"Location/POS name, city",1, "L", false, 0);
	$pdf->MultiCell(12,8,"Opening Date",1, "R", false, 0);
	$pdf->MultiCell(19,8,"Distance from proposed store",1, "L", false, 0);
	$pdf->MultiCell(20,8,"Legal Type / POS Type",1, "L", false, 0);
	$pdf->MultiCell(17,8,"Total surface in sqm",1, "L", false, 0);
	$pdf->MultiCell(11,8,"Year Months",1, "L", false, 0);
	$pdf->MultiCell(13,8,"Units sold Watches",1, "R", false, 0);
	$pdf->MultiCell(13,8,"Units sold Watch Straps",1, "R", false, 0);
	$pdf->MultiCell(16,8,"Total Gross Sales K" . $currency_symbol,1, "R", false, 0);
	$pdf->MultiCell(16,8,"Total Gross Sales KCHF*",1, "R", false, 0);
	$pdf->MultiCell(12,8,"Planned Closing",1, "R", false, 1);
	



	
	$y = $pdf->GetY()+2;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 7);
	
	$i=1;
	foreach($sellout_data as $key=>$data)
	{
		if($i<11)
		{
			
			$gross_sales = $data["grossales"];
			$kloc = round($gross_sales / 1000, 0);
			
			if($project["project_cost_type"] == 1 and $ln_basicdata["ln_no_ln_submission_needed"] == 0) //corporate
			{
				$kchf = $gross_sales*$ln_basicdata["ln_basicdata_exchangerate"]/$ln_basicdata["ln_basicdata_factor"];
			}
			else
			{
				$kchf = $gross_sales*$cer_basicdata["cer_basicdata_exchangerate"]/$cer_basicdata["cer_basicdata_factor"];
			}
			$kchf = round($kchf / 1000, 0);
			
			$pdf->Cell(58, 4, $data["owner"], 1, "", "L", false);
			$pdf->Cell(73, 4, $data["posname"], 1, "", "L", false);
			
			if($data["take_over_date"] != NULL and $data["take_over_date"] != '0000-00-00')
			{
				$opening = to_system_date($data["take_over_date"]);
			}
			else
			{
				if($data["opening_date"] == NULL
					or $data["opening_date"] == '0000-00-00'
					or $data["opening_date"] < '1971-01-01')
				{
					$opening = "n/a";
				}
				else
				{
					$opening = to_system_date($data["opening_date"]);
				}
			}
			
			$pdf->Cell(12, 4, $opening, 1, "", "R", false);
			$pdf->Cell(19, 4, $data["distance"], 1, "", "R", false);
			
			$pdf->Cell(20, 4, substr($data["type"], 0, 20), 1, "", "L", false);
			$pdf->Cell(17, 4, $data["total_surface"], 1, "", "R", false);
			$pdf->Cell(11, 4, $data["year"] . "/" . $data["month"], 1, "", "L", false);
			$pdf->Cell(13, 4, $data["watches"], 1, "", "R", false);
			$pdf->Cell(13, 4, $data["bijoux"], 1, "", "R", false);
			$pdf->Cell(16, 4, $kloc, 1, "", "R", false);
			$pdf->Cell(16, 4, $kchf, 1, "", "R", false);
			$pdf->Cell(12, 4, $data["planned_closing"], 1, "", "R", false);

			$y = $pdf->GetY()+4;
			$pdf->SetXY($x,$y);
			$i++;
		}	
	}
	for($i=($i-1);$i<11;$i++)
	{
		$pdf->Cell(58, 4, "", 1, "", "L", false);
		$pdf->Cell(73, 4, "", 1, "", "L", false);
		$pdf->Cell(12, 4, "", 1, "", "R", false);
		$pdf->Cell(19, 4, "", 1, "", "R", false);
		$pdf->Cell(20, 4, "", 1, "", "L", false);
		$pdf->Cell(17, 4, "", 1, "", "R", false);
		$pdf->Cell(11, 4, "", 1, "", "R", false);
		$pdf->Cell(13, 4, "", 1, "", "R", false);
		$pdf->Cell(13, 4, "", 1, "", "R", false);
		$pdf->Cell(16, 4, "", 1, "", "R", false);
		$pdf->Cell(16, 4, "", 1, "", "R", false);
		$pdf->Cell(12, 4, "", 1, "", "R", false);

		$y = $pdf->GetY()+4;
		$pdf->SetXY($x,$y);
	}
	
	
	$pdf->SetXY($x,$y+0.5);
	
	if($project["project_cost_type"] == 1 and $ln_basicdata["ln_no_ln_submission_needed"] == 0) //corporate
	{
		$pdf->Cell(50, 4, "*Exchange Rate per " . date("d.m.Y") . ": " . $cer_basicdata["cer_basicdata_exchangerate"], 0, "" , "L", false);
	}
	else
	{
		$pdf->Cell(50, 4, "*Exchange Rate per " . date("d.m.Y") . ": " . $ln_basicdata["ln_basicdata_exchangerate"], 0, "" , "L", false);
	}

	$pdf->Ln();
	$pdf->SetX($x);
	




	//7. Conditions of other SG brands in same mall / street

	$y = $pdf->getY() + 2;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(280, 6, "Conditions of other SG brands in same mall / street (" . $project["order_shop_address_company"] . ")", 1, "", "L");
	

			
	$y = $pdf->GetY()+6;
	$pdf->SetXY($x,$y);

	$pdf->SetFont("arialn", "B", 7);
	
	$pdf->MultiCell(35,8,"Brand",1, "L", false, 0);
	$pdf->MultiCell(15,8,"Legal Type",1, "L", false, 0);
	$pdf->MultiCell(25,8,"POS Type",1, "L", false, 0);
	$pdf->MultiCell(19,8,"Contract year signed",1, "L", false, 0);
	//$pdf->MultiCell(20,8,"Gross surface (sqm)",1, "R", false, 0);
	$pdf->MultiCell(20,8,"Total surface (sqm)",1, "R", false, 0);
	$pdf->MultiCell(75,8,"Rental conditions",1, "L", false, 0);
	$pdf->MultiCell(11,8,"Sales year",1, "L", false, 0);
	$pdf->MultiCell(18,8,"Gross sales"  . "\n" . "in K" . $currency_symbol,1, "R", false, 0);
	$pdf->MultiCell(62,8,"Notes",1, "L", false, 1);
	
	$y = $pdf->GetY()+2;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 7);
	
	

	

	
	//get data
	$has_data = false;
	$sql = "select * from ln_basicdata_lnr03_brands " . 
		   " left join postypes on postype_id = ln_basicdata_lnr03_brand_postype_id " .
		   " left join posowner_types on posowner_type_id = ln_basicdata_lnr03_brand_legaltype_id " .
		   " left join sg_brands on sg_brand_id = ln_basicdata_lnr03_brand_sg_brand_id " . 
		   " where ln_basicdata_lnr03_brand_cerbasicdata_id = " . $cer_basicdata["cer_basicdata_id"] . 
		   "  and ln_basicdata_lnr03_brand_cer_version = " . $ln_version .
		   " order by sg_brand_name";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$has_data = true;

		$y = $pdf->GetY();
		$pdf->setX(8);

		$amount = round($row["ln_basicdata_lnr03_brand_sales_amount"]/1000, 0);
		$pdf->MultiCell(35,0,$row["sg_brand_name"],1, "L", false, 0);
		$pdf->MultiCell(15,0,$row["posowner_type_name"],1, "L", false, 0);
		$pdf->MultiCell(25,0,$row["postype_name"],1, "L", false, 0);
		$pdf->MultiCell(19,0,$row["ln_basicdata_lnr03_brand_contract_year"],1, "L", false, 0);
		$pdf->MultiCell(20,0,$row["ln_basicdata_lnr03_brand_gross_surface"],1, "R", false, 0);
		
		$x = $pdf->getX()+75;
		$pdf->MultiCell(75,0,$row["ln_basicdata_lnr03_brand_rental_conidtions"],1, "L", false, 1);
		$y1 = $pdf->GetY();
		


		$pdf->setXY($x, $y);

		$pdf->MultiCell(11,0,$row["ln_basicdata_lnr03_brand_sales_year"],1, "L", false, 0);
		$pdf->MultiCell(18,0,$amount,1, "R", false, 0);
		$pdf->MultiCell(62,0,$row["ln_basicdata_lnr03_brand_notes"],1, "L", false, 1);
		$y2 = $pdf->GetY();
		
		$y = $pdf->GetY();
		if($y1 > $y){$y = $y1;}
		if($y2 > $y){$y = $y2;}
			
		$pdf->SetXY($x,$y);

		
		
	}

	if($has_data == false)
	{
		$pdf->MultiCell(190,0,"There are no other Swatch Group Brands in the same mall/street.",0, "L", false, 0);
	}




	//print map data
	/*
	$coordinates = array();
	$coordinates[1] = array("lat"=>40.702147, "long"=>-74.015794, "color"=>"red");
	$coordinates[2] = array("lat"=>40.711614, "long"=>-74.012318, "color"=>"yellow");
	$coordinates[3] = array("lat"=>40.718217, "long"=>-73.998284, "color"=>"yellow");

	$marker_string = "";
	foreach($coordinates as $key=>$cordinate)
	{
		$marker_string .= "&markers=color:". $cordinate["color"] . "|label:" . $key . "|" . $cordinate["lat"] . "," . $cordinate["long"];
	}



	$url = "http://maps.google.com/maps/api/staticmap?size=640x640&maptype=roadmap&key=AIzaSyD2wMUs74upe6ZvEOflrrMDRCoPYABu1Qs";
	$url .= $marker_string;
	$url .= "&sensor=false";

	$tmpfilename1 = "map1" . time() . ".jpg";


	if(!$context) {
		
		$mapImage1 = file_get_contents($url) or die("url not loading");
	}
	else
	{
		$mapImage1 = file_get_contents($url, false, $context) or die("url not loading");
	}

	$fh = fopen(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1, 'w') or die("can't open file");
	fwrite($fh, $mapImage1);
	fclose($fh);

	
	$tmpfilename1 = "map1" . time() . ".jpg";

	
	if(!$context) {
		
		$mapImage1 = file_get_contents($url) or die("url not loading");
	}
	else
	{
		$mapImage1 = file_get_contents($url, false, $context) or die("url not loading");
	}
	
	$fh = fopen(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1, 'w') or die("can't open file");
	fwrite($fh, $mapImage1);
	fclose($fh);
	
	if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1)) {
		$pdf->Image(TMP_FILE_DIR_RELATIVE . $tmpfilename1, $x, $pdf->getY(), 90, 90);
	}



	$pdf->AddPage("P", "A4");

	//Logo
	$pdf->Image('../pictures/brand_logo.jpg',10,8,33);
	
	if(isset($version_date) and $version_date)
	{
		$pdf->SetFont('arialn','B',12);
		//Move to the right
		$pdf->Cell(40);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title . $version_date,0,0,'R');
		//Line break
		$pdf->SetY(23);
	}
	else
	{
		//arialn bold 15
		$pdf->SetFont('arialn','B',18);
		//Move to the right
		$pdf->Cell(80);
		//Title
		$pdf->SetY(0);
		$pdf->Cell(0,33,$page_title,0,0,'R');
		//Line break
		$pdf->SetY(23);
	}




	
	$pdf->setXY(10, $pdf->getY());
	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,4,"LEASE NEGOTIATION - ADDITIONAL ANALYSIS",1, 0, 'L', 1);
	$pdf->Ln();

	$y = $pdf->GetY()+10;
	$y_at_start = $y - 1;

		
		
	// 5. Top 10 distribution analysis of current stores in the area
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(190, 6, "Top 10 distribution analysis of current stores in the area (" . $project["order_shop_address_company"] . ")", 1, "", "L");
	

			
	$y = $pdf->GetY()+10;
	$pdf->SetXY($x,$y);



	if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1)) {
		$pdf->Image(TMP_FILE_DIR_RELATIVE . $tmpfilename1, $x, $pdf->getY(), 190, 190);
	}
	*/




}		

	
?>