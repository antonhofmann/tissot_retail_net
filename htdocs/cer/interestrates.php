<?php
/********************************************************************

    interestrates.php

    Lists countries for editing the interest rates.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_full_access_to_cer");
set_referer("interestrate.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
//get all data from interest rates

$year_months = array();
$rates = array();

$sql = "select interestrate_id, concat(interestrate_rate, '%') as rate, " . 
       "interestrate_year, cer_interestrates.date_modified as last_change, country_name " .
       "from cer_interestrates " .
	   "left join countries on country_id = interestrate_country ";


/********************************************************************
    build list 
*********************************************************************/

$list = new ListView($sql);

$list->set_entity("cer_interestrates");
$list->set_order("country_name, interestrate_year DESC");
$list->set_group("country_name");

$list->add_column("interestrate_year", "Year", "interestrate.php");
$list->add_column("rate", "Rate", "", "", "", COLUMN_ALIGN_RIGHT);
$list->add_column("last_change", "Modified");

$list->add_button(LIST_BUTTON_NEW, "New", "interestrate.php");
$list->add_button("new_bulk", "Bulk Operation");

$list->process();

if($list->button("new_bulk")) {
	redirect("interestrate_bulk.php");
}
$page = new Page("interestrates");

$page->header();
$page->title("Countries' Interest Rates");
$list->render();
$page->footer();
?>
