<?php
/********************************************************************

   ln_application_relation.php

    Application Form: franchisee information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/
$client_address = get_address($project["order_client_address"]);
$posdata = get_pos_data($project["project_order"]);
$franchisee_address = get_address($posdata["posaddress_franchisee_id"]);
$franchisor_address = get_address($posdata["posaddress_franchisor_id"]);

if(!array_key_exists("company", $franchisee_address))
{
	$franchisee_address["company"] = "";
	$franchisee_address["zip"] = "";
	$franchisee_address["place"] = "";
	$franchisee_address["country_name"] = "";

}

//get quantities of watches
$quantity_watches = array();

$sql = "select * from cer_revenues " . 
       "where cer_revenue_project = " . param("pid") . 
	   " and cer_revenue_cer_version = 0 ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$quantity_watches[$row["cer_revenue_year"]] = $row["cer_revenue_quantity_watches"];
}
ksort($quantity_watches);

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";


/********************************************************************
    build form
*********************************************************************/
$form = new Form("posaddresses", "posaddress");

$donotshowfranchisee = true;
include("include/project_head.php");


$form->add_hidden("pid", param("pid"));


if($project["project_cost_type"] != 6 and $project["project_postype"] !=2)
{
	$form->add_section("Franchisee");
}
else
{
	$form->add_section("Owner Company");
}

$form->add_label("franchisee_company", "Company", 0, $franchisee_address["company"]);
$form->add_label("franchisee_address", "City", 0, $franchisee_address["zip"] . " " . $franchisee_address["place"]);
$form->add_label("franchisee_country", "Country", 0, $franchisee_address["country_name"]);


if($project["project_actual_opening_date"] == NULL 
	or $project["project_actual_opening_date"] == '0000-00-00')
{
	if (has_access("can_edit_franchisee_agreement_data"))
	{

		if(count($posdata) > 0 and array_key_exists("posaddress_fagagreement_type", $posdata) and  $posdata["posaddress_fagagreement_type"] > 0 and $project["project_projectkind"] ==  2) // renovation projects
		{
			$type = $project["project_fagagreement_type"];
			$sent = $project["project_fagrsent"];
			$signed = $project["project_fagrsigned"];
			$start = $project["project_fagrstart"];
			$end = $project["project_fagrend"];
			$comment = $project["project_fag_comment"];
		}
		else
		{
			$type = $project["project_fagagreement_type"];
			$sent = $project["project_fagrsent"];
			$signed = $project["project_fagrsigned"];
			$start = $project["project_fagrstart"];
			$end = $project["project_fagrend"];
			$comment = $project["project_fag_comment"];

		}
		
		$form->add_section("Agreement");
		$form->add_list("project_fagagreement_type", "Agreement Type*", "select agreement_type_id, agreement_type_name from agreement_types", 0, $type);
		$form->add_checkbox("project_fagrsent", "Sent", $sent, 0, "Agreement");
		$form->add_checkbox("project_fagrsigned", "Signed", $signed, 0, "Agreement");
		$form->add_label("dummy", "");
		$form->add_edit("project_fagrstart", "Agreement Starting Date", 0, to_system_date($start), TYPE_DATE);
		$form->add_edit("project_fagrend", "Agreement Ending Date", 0, to_system_date($end), TYPE_DATE);
		$form->add_multiline("project_fag_comment", "Comment", 4, 0, $comment);
	}
	else
	{	
		if(count($posdata) > 0 and array_key_exists("posaddress_fagagreement_type", $posdata) and  $posdata["posaddress_fagagreement_type"] > 0)
		{
			$type = $posdata["posaddress_fagagreement_type"];
			$sent = $posdata["posaddress_fagrsent"];
			$signed = $posdata["posaddress_fagrsigned"];
			$start = $posdata["posaddress_fagrstart"];
			$end = $posdata["posaddress_fagrend"];
			$comment = $posdata["posaddress_fag_comment"];
		}
		else
		{
			$type = $project["project_fagagreement_type"];
			$sent = $project["project_fagrsent"];
			$signed = $project["project_fagrsigned"];
			$start = $project["project_fagrstart"];
			$end = $project["project_fagrend"];
			$comment = $project["project_fag_comment"];
		}
		
		$form->add_hidden("project_fagagreement_type", $type);
		$form->add_hidden("project_fagrsent", $sent);
		$form->add_hidden("project_fagrsigned", $signed);
		$form->add_hidden("project_fagrstart", to_system_date($start));
		$form->add_hidden("project_fagrend", to_system_date($end));
		$form->add_hidden("project_fag_comment",$comment);


	}
}
elseif (has_access("can_edit_franchisee_agreement_data"))
{
	if(count($posdata) >0  and array_key_exists("posaddress_fagagreement_type", $posdata) and  $posdata["posaddress_fagagreement_type"] > 0)
	{
		$type = $posdata["posaddress_fagagreement_type"];
		$sent = $posdata["posaddress_fagrsent"];
		$signed = $posdata["posaddress_fagrsigned"];
		$start = $posdata["posaddress_fagrstart"];
		$end = $posdata["posaddress_fagrend"];
		$comment = $posdata["posaddress_fag_comment"];
	}
	else
	{
		$type = $project["project_fagagreement_type"];
		$sent = $project["project_fagrsent"];
		$signed = $project["project_fagrsigned"];
		$start = $project["project_fagrstart"];
		$end = $project["project_fagrend"];
		$comment = $project["project_fag_comment"];
	}
	
	$form->add_section("Agreement");
	$form->add_list("project_fagagreement_type", "Agreement Type*", "select agreement_type_id, agreement_type_name from agreement_types", 0, $type);
	$form->add_checkbox("project_fagrsent", "Sent", $sent, 0, "Agreement");
	$form->add_checkbox("project_fagrsigned", "Signed", $signed, 0, "Agreement");
	$form->add_label("dummy", "");
	$form->add_edit("project_fagrstart", "Agreement Starting Date", 0, to_system_date($start), TYPE_DATE);
	$form->add_edit("project_fagrend", "Agreement Ending Date", 0, to_system_date($end), TYPE_DATE);
	$form->add_multiline("project_fag_comment", "Comment", 4, 0, $comment);
}


$form->add_section("Relation");
$form->add_checkbox("address_is_independent_retailer", "Company is an independent retailer", $franchisee_address["address_is_independent_retailer"], "", "Independent Retailer");
$form->add_checkbox("franchisee_address_swatch_retailer", "Company is a present ". BRAND ." Retailer", $franchisee_address["address_swatch_retailer"], "", "Relation with " . BRAND);



$form->add_checkbox("cer_basicdata_franchsiee_already_partner", "Company is already a business partner of " . BRAND, $cer_basicdata["cer_basicdata_franchsiee_already_partner"], "", "Third Party Business Partner");

$form->add_edit("address_company_is_partner_since", "Company is partner since", 0, to_system_date($franchisee_address["address_company_is_partner_since"]), TYPE_DATE);

$form->add_edit("cer_basicdata_franchsiee_number_of_pos", "&nbsp;&nbsp;- with Numer of Stores", 0, $cer_basicdata["cer_basicdata_franchsiee_number_of_pos"], TYPE_INT, 8);

$form->add_edit("cer_basicdata_franchsiee_brands", "&nbsp;&nbsp;- of the following Brands", 0, $cer_basicdata["cer_basicdata_franchsiee_brands"]);



$form->add_section("Territory Exclusivity");
$form->add_comment("Please give a description clearly delimitating the exclusive territory of this POS location, covered by the Franchisee contract.");



if(has_access("has_access_to_his_cer") 
	or has_access("has_full_access_to_cer"))
{
	$form->add_multiline("posaddress_fag_territory", "Description*", 4, NOTNULL, $posdata["posaddress_fag_territory"]);
	
	
	if($posdata["posaddress_fag_city_pasted"])
	{
		$form->add_upload("posaddress_fag_city_pasted", "City Map (PDF or JPG only)*", "/files/cer/". $project["order_number"],  $deletable, $posdata["posaddress_fag_city_pasted"]);
	}
	else
	{
		$form->add_hidden("posaddress_fag_city_pasted");
	}
}
else
{
	$form->add_label("posaddress_fag_territory", "Agreement Content");
	
	if($posdata["posaddress_fag_city_pasted"])
	{
		$link = "<a href=\"#\" onClick=\"javascript:popup('" . $posdata["posaddress_fag_city_pasted"] . "', 640, 480);\">Show City Pasted</a>";
		$form->add_label("posaddress_fag_city_pasted", "City Pasted", RENDER_HTML, $link);
	}

}

//check if business plan can be edited
require_once("include/check_if_business_plan_is_editable.php");



if(($can_edit_business_plan == true 
	and $project["order_actual_order_state_code"] < '890' 
	and $cer_basicdata["cer_basicdata_cer_locked"] == 0 
	and has_access("has_access_to_his_cer") 
	and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") 
	and $project["project_state"] != 2))
{	
	$form->add_button("save", "Save Data");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();



if($form->button("save"))
{
	
	if($form->validate())
	{

		$sql = "update addresses set
		address_swatch_retailer = " . dbquote($form->value("franchisee_address_swatch_retailer")) . ", " . 
		"address_is_independent_retailer = " . dbquote($form->value("address_is_independent_retailer")) . ", " . 
		"address_company_is_partner_since = " . dbquote(from_system_date($form->value("address_company_is_partner_since"))) . 
		" where address_id = " . $posdata["posaddress_franchisee_id"];
		
		mysql_query($sql) or dberror($sql);
		
		//update posaddress data

		$fields = array();
		
		$value = dbquote($form->value("posaddress_fag_territory"));
		$fields[] = "posaddress_fag_territory = " . $value;

		$value = dbquote($form->value("posaddress_fag_city_pasted"));
		$fields[] = "posaddress_fag_city_pasted = " . $value;

		$value1 = "current_timestamp";
		$project_fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$project_fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update " . $posdata["table"] . " set " . join(", ", $fields) . " where posaddress_id = " . $posdata["posaddress_id"];
		mysql_query($sql) or dberror($sql);



		//update cer basic data

		$fields = array();
		
		$value = dbquote($form->value("cer_basicdata_franchsiee_already_partner"));
		$fields[] = "cer_basicdata_franchsiee_already_partner = " . $value;

		$value = dbquote($form->value("cer_basicdata_franchsiee_number_of_pos"));
		$fields[] = "cer_basicdata_franchsiee_number_of_pos = " . $value;

		$value = dbquote($form->value("cer_basicdata_franchsiee_brands"));
		$fields[] = "cer_basicdata_franchsiee_brands = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_id = " . $cer_basicdata["cer_basicdata_id"];
		mysql_query($sql) or dberror($sql);



		//update agreement data
		$project_fields = array();

		$value = trim($form->value("project_fagagreement_type")) == "" ? "0" : dbquote($form->value("project_fagagreement_type"));
		$project_fields[] = "project_fagagreement_type = " . $value;

		$value = trim($form->value("project_fagrsent")) == "" ? "0" : dbquote($form->value("project_fagrsent"));
		$project_fields[] = "project_fagrsent = " . $value;

		$value = trim($form->value("project_fagrsigned")) == "" ? "0" : dbquote($form->value("project_fagrsigned"));
		$project_fields[] = "project_fagrsigned = " . $value;

		$value = trim($form->value("project_fagrstart")) == "" ? "NULL" : dbquote(from_system_date($form->value("project_fagrstart")));
		$project_fields[] = "project_fagrstart = " . $value;

		$value = trim($form->value("project_fagrend")) == "" ? "NULL" : dbquote(from_system_date($form->value("project_fagrend")));
		$project_fields[] = "project_fagrend = " . $value;

		$value = trim($form->value("project_fag_comment")) == "" ? "null" : dbquote($form->value("project_fag_comment"));
		$project_fields[] = "project_fag_comment = " . $value;

		$sql = "update projects set " . join(", ", $project_fields) . " where project_id = " . dbquote(param("pid"));
		mysql_query($sql) or dberror($sql);

		$form->message("Your data has bee saved.");

		$link = "ln_application_relation.php?pid=" . param("pid");
		redirect($link);
	}
}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();
if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Franchisee Information");
}
elseif($form_type == "AF")
{
	$page->title("LNR/INR03: Franchisee Information");
}

require_once("include/tabs_ln2016.php");
$form->render();


?>

<?php

require "include/footer_scripts.php";

$page->footer();

?>