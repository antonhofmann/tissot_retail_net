<?php
/********************************************************************

    cer_sg_form_inr01_pdf_detail.php

    Print PDF for CER Summary (Form INR-01).

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-04
    Version:        2.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*********************************************************************/

//set pdf parameters
$margin_top = 16;
$margin_left = 12;
$y = $margin_top;

$pdf->SetTitle("Retail Capital Expenditure Request (CER) Form INR-01");
$pdf->SetAuthor("Tissot Retailnet");
$pdf->SetDisplayMode(100);
$pdf->AddPage("P");
$globalPageNumber++;
$pdf->SetAutoPageBreak(false, 0);

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 9);
$pdf->Cell(40, 8, "The Swatch Group", 1);

$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(82, 8, "Retail Capital Expenditure Request (CER)", 1, "", "C");

$pdf->SetFont("arialn", "", 9);
$pdf->SetFillColor(248,251,167);
$pdf->Cell(25, 8, "SUMMARY", 1, "", "C", false);

$pdf->SetFont("arialn", "", 9);

if($cer_version == 0)
{
	$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
}
else
{
	$pdf->Cell(20, 8, substr(to_system_date($cer_basicdata["version_date"]), 0, 10), 1, "", "C", true);
}
$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(20, 8, "INR-01", 1, "", "C");


// draw first box
$pdf->SetXY($margin_left,$y+8);
$pdf->Cell(187, 32, "", 1);



	// print project and investment infos
	$y = $y+9;
	$x = $margin_left+1;

	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Legal Entity Name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(102, 5, $legal_entity_name, 1, "", "L", true);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(26, 5, "Currency:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(18, 5, $currency_symbol, 1, "", "L", true);


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Project Name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(70, 5, $project_name, 1, "", "L", true);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(32, 5, "Project Leader:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(44, 5, $project_manager, 1, "", "L", true);


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Requested Amount:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(31, 5, $requested_amount, 1, "", "R", true);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Project begin (month/year):", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(32, 5, $project_start, 1, "", "L", true);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(26, 5, "Date LNR approval:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(18, 5, $date_ln_approval, 1, "", "L", true);
	

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 7);
	$pdf->Cell(39, 5, "- Investments in fixed Assets:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(31, 5, $fixed_assets_amount, 1, "", "R", true);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Project end (month/year):", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(32, 5, $project_end, 1, "", "L", true);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(26, 5, "Date CER request:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(18, 5, $date_cer_request, 1, "", "L", true);


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 7);
	$pdf->Cell(39, 5, "- Key/Prem. Money/Goodwill:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(31, 5, $key_money, 1, "", "R", true);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Post Compl. Review (month/year):", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(32, 5, $post_compl_review, 1, "", "L", true);
	$pdf->SetFont("arialn", "B", 8);
	if($takeover == true)
	{
		$pdf->Cell(26, 5, "Takeover Date:", 1, "", "L");
	}
	else
	{
		$pdf->Cell(26, 5, "Opening Date:", 1, "", "L");
	}
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(18, 5, $opening_date, 1, "", "L", true);


	

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(39, 5, "Project classification: (X)", 1, "", "L");
	
	
	
	if($relocated_pos_name)
	{
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(102, 5, "Relocation of " . $relocated_pos_name , 1, "", "L");
		$pdf->SetFillColor(224,224,224);
		$pdf->Cell(44, 5, "", 1, "", "C", true);
	}
	elseif($project["project_projectkind"] == 8) // popup
	{
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(102, 5, "PopUp in " . $project_name , 1, "", "L");
		$pdf->SetFillColor(224,224,224);
		$pdf->Cell(44, 5, "", 1, "", "C", true);
	}
	else
	{
		$pdf->Cell(20, 5, "New", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(11, 5, $project_kind_new, 1, "", "L", true);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(28, 5, "Renovation", 1, "", "L");
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(11, 5, $project_kind_renovation, 1, "", "L", true);
		$pdf->SetFont("arialn", "B", 8);
		
		if($takeover == true)
		{
			$pdf->Cell(21, 5, "Takeover", 1, "", "L");
			$pdf->SetFont("arialn", "", 8);
			$pdf->Cell(11, 5, "X", 1, "", "L", true);
			$pdf->SetFont("arialn", "", 8);
		}
		else
		{
			$pdf->Cell(21, 5, "Relocation", 1, "", "L");
			$pdf->SetFont("arialn", "", 8);
			$pdf->Cell(11, 5, $project_kind_relocation, 1, "", "L", true);
			$pdf->SetFont("arialn", "", 8);
		}
		
		if( $takeover_date)
		{
			$pdf->SetFont("arialn", "B", 8);
			$pdf->Cell(26, 5, "Takeover Date:", 1, "", "L");
			$pdf->SetFont("arialn", "", 8);
			$pdf->Cell(18, 5, $takeover_date, 1, "", "L", true);
		}
		else
		{
			$pdf->Cell(44, 5, "", 1, "", "C");
		}
	}

// end of first box

// draw second box
$pdf->SetXY($margin_left,$y+6);

if($approval_name8)
{
	$pdf->Cell(187, 226, "", 1);
}
else
{
	$pdf->Cell(187, 212, "", 1);
}

	$y = $y+5;
	$pdf->SetXY($x-1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(102, 8, "1. Project desription and strategy:", 0, "", "L");
	
	if($strategy or $investment)
	{
		$content = $description . "\r\n" . $strategy . "\r\n" . $investment;
	}
	else
	{
		$content = $description;
	}
	$content = str_replace("\r\n\r\n", "\r\n", $content);
	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFillColor(248,251,167);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(185, 40, "", 1, "", "L", true);
	$pdf->SetXY($x,$y+1);
	$pdf->MultiCell(185,3.5, $content, 0, "T");

	

	$y = $y+40;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(102, 8, "2. Benefits, risks, alternatives:", 0, "", "L");

	if($risks or $alternative)
	{
		$content = $benefits . "\r\n" . $risks . "\r\n" . $alternative;
	}
	else
	{
		$content = $benefits;
	}
	$content = str_replace("\r\n\r\n", "\r\n", $content);
	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFillColor(248,251,167);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(185, 40, "", 1, "", "L", true);
	$pdf->SetXY($x+1,$y+1);
	$pdf->MultiCell(183,3.5, $content, 0, "T");

	

	$y = $y+40;
	$pdf->SetXY($x-1,$y);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(102, 8, "3. Signatures:", 0, "", "L");

	

	//title bar 1
	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetFillColor(224,224,224);
	$pdf->Cell(185,6, "Brand", 1, "", "L", true);
	

	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 14, "CEO" . "\r\n" . $approval_name2, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 14, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 14, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 14, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');

	$y = $y+14;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 14, "CFO" . "\r\n" . $approval_name7, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 14, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 14, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 14, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');
	
	$y = $y+14;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 14, "VP Sales " . "\r\n" . $approval_name3, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 14, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 14, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 14, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+14;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 14, "International Retail Manager" . "\r\n" . $approval_name4, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 14, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 14, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 14, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+16;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetFillColor(224,224,224);
	$pdf->Cell(185,6, "Country", 1, "", "L", true);

	$y = $y+6;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 14, "County Manager " . "\r\n" . $approval_name11, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 14, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 14, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 14, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');

	$y = $y+14;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 14, "Head of Controlling " . "\r\n" . $approval_name12, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 14, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 14, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 14, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');


	$y = $y+14;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->MultiCell(30, 14, "Brand Manager " . "\r\n" . $approval_name13, 1);
	$pdf->SetXY($x+30,$y);
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(63, 14, "Remarks: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(30, 14, "Date: ", 1, "", "L", false, '', 0, false, 'T', 'T');
	$pdf->SetFont("freesans", "", 7);
	$pdf->Cell(62, 14, "Signature:", 1, "", "L", false, '', 0, false, 'T', 'T');

	
	

	

//end of second box
?>