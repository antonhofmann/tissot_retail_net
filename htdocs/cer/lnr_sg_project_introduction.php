<?php


//project introduction
$pdf->AddPage("L");
$globalPageNumber++;

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(270, 8, "  Project Introduction: " . $project["country_name"] . " - " . $pos_data["place_name"] . " - " . $pos_data["posaddress_name"] . " - Project " . $project['project_number'], 1, "", "L");

$pdf->setY(25);



//Fa�ade image
$x_offset = 145;
if($facade_image != ".." and file_exists($facade_image))
{
	if(substr($facade_image, strlen($facade_image)-3, 3) == "jpg" 
		or substr($facade_image, strlen($facade_image)-3, 3) == "JPG"
		 or substr($facade_image, strlen($facade_image)-3, 3) == "jpeg"
		or substr($facade_image, strlen($facade_image)-3, 3) == "JPEG")
	{

		$pdf->setXY($margin_left, 25);
		$x = $pdf->GetX();
		$y = $pdf->GetY();
		$pdf->Image($facade_image,$x,$y, 0, 80, "", "", "", true, 300, "", false, false, 0, false, false,true);

	}
}

$x= 150;
$pdf->SetFont("arialn", "B", 10);
$pdf->SetXY($x, 24);
$pdf->Cell(100, 4, "Estimated/Agreed Opening Date", 0, "", "L");
$pdf->Ln();

$pdf->SetX($x);
$pdf->SetFont("arialn", "", 9);
$pdf->Cell(100, 6, to_system_date($project['project_planned_opening_date']) . "/" . to_system_date($project['project_real_opening_date']), 0, "", "L");
$pdf->Ln();

$pdf->SetFont("arialn", "B", 10);
$pdf->SetX($x);
$pdf->Cell(100, 8, "Developer", 0, "", "L");
$pdf->Ln();

$pdf->SetX($x);
$pdf->SetFont("arialn", "", 9);
$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_basic_developer'], 0, "T", 0, 1);


$pdf->SetFont("arialn", "B", 10);
$pdf->SetX($x);
$pdf->Cell(100, 8, "Number of Floors", 0, "", "L");
$pdf->Ln();

$pdf->SetX($x);
$pdf->SetFont("arialn", "", 9);
$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_numfloors'], 0, "T", 0, 1);

$pdf->SetFont("arialn", "B", 10);
$pdf->SetX($x);
$pdf->Cell(100, 8, "Parking Space", 0, "", "L");
$pdf->Ln();

$pdf->SetX($x);
$pdf->SetFont("arialn", "", 9);
$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_parking_space'], 0, "T", 0, 1);

$pdf->SetFont("arialn", "B", 10);
$pdf->SetX($x);
$pdf->Cell(100, 8, "Basic Information", 0, "", "L");
$pdf->Ln();

$pdf->SetX($x);
$pdf->SetFont("arialn", "", 9);
$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_basic_info'], 0, "T", 0, 1);


$pdf->SetFont("arialn", "B", 10);
$pdf->SetX($x);
$pdf->Cell(100, 8, "Positioning", 0, "", "L");
$pdf->Ln();

$pdf->SetX($x);
$pdf->SetFont("arialn", "", 9);
$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_positioning'], 0, "T", 0, 1);

$pdf->SetFont("arialn", "B", 10);
$pdf->SetX($x);
$pdf->Cell(100, 8, "Target Customer", 0, "", "L");
$pdf->Ln();

$pdf->SetX($x);
$pdf->SetFont("arialn", "", 9);
$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_target_customers'], 0, "T", 0, 1);

$pdf->SetFont("arialn", "B", 10);
$pdf->SetX($x);
$pdf->Cell(100, 8, "Total Construction Flooer Area and Surroundings", 0, "", "L");
$pdf->Ln();

$pdf->SetX($x);
$pdf->SetFont("arialn", "", 9);
$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_total_floor_area'], 0, "T", 0, 1);


$pdf->SetFont("arialn", "B", 10);
$pdf->SetX($x);
$pdf->Cell(100, 8, "Speciality of this Project", 0, "", "L");
$pdf->Ln();

$pdf->SetX($x);
$pdf->SetFont("arialn", "", 9);
$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_project_specials'], 0, "T", 0, 1);
?>