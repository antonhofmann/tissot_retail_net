<?php
/********************************************************************

    cer_rental_cost_versions_pdf.php

    Print Rental Cost Overview for Versions

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-05-31
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-05-31
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();


$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');

// Create and setup PDF document

$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(12, 16);
$pdf->setPrintHeader(false);


$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');

$pdf->Open();


include("cer_rental_cost_versions_detail_pdf.php");

$pdf->Output('versions_rental_cost_overview_' . $project_name . "_" . $project_number . '.pdf');

?>