<?php

if($posdata["posaddress_google_lat"] and $posdata["posaddress_google_long"])
{ 
	$latitude = $posdata["posaddress_google_lat"];
	$longitude = $posdata["posaddress_google_long"];

	//insert google map
	$pdf->AddPage("L");
	$globalPageNumber++;
	
	// Title first line
	$pdf->SetXY($margin_left,$margin_top);
	$pdf->SetFont("arialn", "B", 11);
	$pdf->Cell(270, 8, "  City Oveview: " . $project["country_name"] . " - " . $pos_data["place_name"] . " - " . $pos_data["posaddress_name"] . " - Project " . $project['project_number'], 1, "", "L");

	$pdf->setY(25);

	
	
	$url = STATIC_MAPS_HOST;
	$url .= '?center=' . $latitude . ',' . $longitude;
	$url .= '&zoom=11';
	$url .= '&size=640x640';
	$url .= '&maptype=roadmap' . "\\";
	$url .= '&markers=color:red%7Clabel:P%7C' . $latitude . ',' . $longitude;
	$url .= '&format=jpg';
	$url .= '&key=' . GOOGLE_API_KEY;
	$url .= '&sensor=false';

	
	$tmpfilename1 = "map1" . time() . ".jpg";

	$mapImage1 = '';
	if(!$context) {
		
		$mapImage1 = file_get_contents($url) or $mapImage1 = '';
	}
	else
	{
		$mapImage1 = file_get_contents($url, false, $context) or $mapImage1 = '';
	}
	
	if($mapImage1) {
		$fh = fopen(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1, 'w') or $mapImage1 = '';
		fwrite($fh, $mapImage1);
		fclose($fh);
		
		if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1)) {
			$pdf->Image(TMP_FILE_DIR_RELATIVE . $tmpfilename1, $margin_left, $pdf->getY(), 130, 130);
		}
	}
	if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1))
	{
		unlink(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1);
	}

	$x= 150;
	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetXY($x, 24);
	$pdf->Cell(100, 8, "Area/Population", 0, "", "L");
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_area'], 0, "T", 0, 1);


	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 8, "Economic Statistics", 0, "", "L");
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_economics'], 0, "T", 0, 1);


	$pdf->SetFont("arialn", "B", 10);
	$pdf->SetX($x);
	$pdf->Cell(100, 8, "General Information", 0, "", "L");
	$pdf->Ln();

	$pdf->SetX($x);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(130,0, $ln_basicdata['ln_basicdata_cityinfo'], 0, "T", 0, 1);


}
?>