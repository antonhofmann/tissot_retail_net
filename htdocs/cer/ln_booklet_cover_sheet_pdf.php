<?php
/********************************************************************

    ln_booklet_cover_sheet_pdf.php

    Print Cover Sheet for Booklet

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2015-01-08
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2015-01-08
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.
*********************************************************************/
session_name("retailnet");
session_start();


$SUPPRESS_HEADERS = true;
$PDFmerger_was_used = false;


define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 

require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";

check_access("has_access_to_cer");

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');



// Create and setup PDF document
// Create and setup PDF document
class MYPDF extends TCPDF
{
	
	//Page footer
	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		$this->SetFont('arialn','I',8);
		$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
	}

}


$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

$pdf->AddFont('arialn','');
$pdf->AddFont('arialn','B');
$pdf->AddFont('arialn','I');
$pdf->AddFont('arialn','BI');

$pdf->Open();

$pdf->AddPage();
$pdf->SetAutoPageBreak(false, 0);


//cover sheet
include("ln_booklet_cover_sheet_detail_pdf.php");

$pdf->Output($file_name, 'I');

?>