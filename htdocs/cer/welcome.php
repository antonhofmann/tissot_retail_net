<?php
/********************************************************************

    welcome.php

    Entry page for the cer section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-26
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/access_filters.php";

check_access("has_access_to_cer");

$select_country = false;

$country_filter = "";

if(has_access("has_full_access_to_cer") or has_access("can_view_all_cer_date"))
{
	//no restictions
}
else
{
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
		$select_country = true;
	}

	$country_access_filter = get_users_regional_access_to_countries(1, user_id());
	if($country_access_filter)
	{
		if($country_filter)
		{
			$country_filter = "(" . $country_filter .  " or " . $country_access_filter . ")";
		}
		else
		{
			$country_filter = $country_access_filter;
		}
	}
	
}

if(has_access("has_full_access_to_cer") or has_access("can_view_all_cer_date") or $select_country == true)
{

	set_referer("cer_projects.php");

	/********************************************************************
	prepare all data needed
	*********************************************************************/
	//create sqls for the project selection

	
	if($country_filter)
	{
		$sql_years = "select distinct left(order_date, 4) as year " .
					  "from orders ".
			          "left join countries on order_shop_address_country = countries.country_id ".
					  "where order_actual_order_state_code <= '900' and (order_archive_date = '0000-00-00' or order_archive_date is null) " . 
			          " and " . $country_filter . 
					  " order by year DESC";



		$sql_product_lines = "select distinct product_line_id, product_line_name ".
							 "from projects ".
							 "left join product_lines on  project_product_line = product_line_id ".
							 "left join orders on project_order = order_id ".
			                 "left join project_costs on project_cost_order = order_id " .
			                 "left join countries on order_shop_address_country = countries.country_id ".
							 "where order_actual_order_state_code <= '900' " .
			                 "and (order_archive_date = '0000-00-00' or order_archive_date is null) " . 
			                 " and " . $country_filter .
							 " order by product_line_name";

		$sql_countries = "select distinct country_id, country_name ".
						 "from projects ".
						 "left join orders on project_order = order_id ".
			             "left join project_costs on project_cost_order = order_id " .
						 "left join countries on order_shop_address_country = countries.country_id ".
						 "where order_actual_order_state_code <= '900' " . 
			             " and (order_archive_date = '0000-00-00' or order_archive_date is null) " . 
			             " and " . $country_filter .
						 " order by country_name";
	}
	else
	{
		$sql_years = "select distinct left(order_date, 4) as year " .
					  "from orders ".
					  "where order_actual_order_state_code <= '900' and (order_archive_date = '0000-00-00' or order_archive_date is null) " . 
					  " order by year DESC";



		$sql_product_lines = "select distinct product_line_id, product_line_name ".
							 "from projects ".
							 "left join product_lines on  project_product_line = product_line_id ".
							 "left join orders on project_order = order_id ".
							 "where order_actual_order_state_code <= '900' and (order_archive_date = '0000-00-00' or order_archive_date is null) " . 
							 " order by product_line_name";

		$sql_countries = "select distinct country_id, country_name ".
						 "from projects ".
						 "left join orders on project_order = order_id ".
						 "left join countries on order_shop_address_country = countries.country_id ".
						 "where order_actual_order_state_code <= '900' and (order_archive_date = '0000-00-00' or order_archive_date is null) " . 
						 " order by country_name";
	}


	$sql_project_kinds = "select distinct projectkind_id, projectkind_name ".
						 "from projects ".
		                 "left join projectkinds on  projectkind_id = project_projectkind " . 
						 "left join orders on project_order = order_id ".
						 "left join countries on order_shop_address_country = countries.country_id ".
						 "where order_actual_order_state_code <= '900' " . 
		                 " and (order_archive_date = '0000-00-00' or order_archive_date is null) " . 
		                 "    and projectkind_id > 0 " .
						 " order by projectkind_name";

	$sql_pos_types = "select postype_id, postype_name " . 
					 "from postypes " . 
		             " where postype_id < 4 " . 
		             " order by postype_name";

	$sql_legal_types = "select project_costtype_id, project_costtype_text " . 
					 "from project_costtypes " . 
		             " where project_costtype_id < 3 " . 
		             " order by project_costtype_text";


	$sqm_ranges = array();
	$sqm_ranges[1] = "1 - 15 sqms";
	$sqm_ranges[2] = "16 - 30 sqms";
	$sqm_ranges[3] = "31 - 45 sqms";
	$sqm_ranges[4] = "46 - 60 sqms";
	$sqm_ranges[5] = "61 - 75 sqms";
	$sqm_ranges[6] = "76 - 100 sqms";
	$sqm_ranges[7] = "100 and more sqms";



	/********************************************************************
	build form
	*********************************************************************/
	$form = new Form("orders", "orders");

	$form->add_edit("search_term", "Search Term/Project Number");
	$form->add_list("country", "Country", $sql_countries, 0);
	$form->add_list("legal_type", "Legal Type", $sql_legal_types, 0);
	$form->add_list("porject_kind", "Project Type", $sql_project_kinds, 0);
	$form->add_list("pos_type", "POS Type", $sql_pos_types, 0);
	$form->add_list("sqms", "Total Area in sqms", $sqm_ranges, 0);


	$form->add_section(" ");
	$form->add_list("from_year", "From Year", $sql_years, 0);
	$form->add_list("to_year", "To Year", $sql_years, 0);
	$form->add_section(" ");
	$form->add_list("productline", "Product Line", $sql_product_lines, 0);
	

	$form->add_button("proceed", "Show Projects");


	/********************************************************************
		Populate form and process button clicks
	*********************************************************************/ 
	$form->populate();
	$form->process();

	if ($form->button("proceed"))
	{
		$search_term = $form->value("search_term");
		$y1 = $form->value("from_year");
		$y2 = $form->value("to_year");
		$p = $form->value("productline");
		$c = $form->value("country");
		$pk = $form->value("porject_kind");
		$pt = $form->value("pos_type");
		$lt = $form->value("legal_type");
		$sqm = $form->value("sqms");

		//$link = "cer_projects.php?search_term=" . urlencode($search_term) ."&y1=". $y1 . "&y2=" . $y2 . "&p=" . $p . "&c=" . $c . "&pk=" . $pk . "&pt=" . $pt . "&lt=" . $lt . "&sqm=" . $sqm;

		$link = "cer_projects.php?search_term=" .$search_term ."&y1=". $y1 . "&y2=" . $y2 . "&p=" . $p . "&c=" . $c . "&pk=" . $pk . "&pt=" . $pt . "&lt=" . $lt . "&sqm=" . $sqm;


		redirect($link);
	}

	/********************************************************************
		render page
	*********************************************************************/ 
	$page = new Page("cer_projects");

	$page->header();
	$page->title("LNR/CER: Project Selection");
	$form->render();

	?>

	<script type="text/javascript">
		
		
		var selectedInput = null;
		$(document).ready(function(){
		  $("#projectnumber").focus();

		  $('input').focus(function() {
					selectedInput = this;
				});

		});

		document.onkeydown = process_key;
		
		function process_key(e)
		{
		  if( !e ) 
		  {
			if( window.event ) 
			{
			  e = window.event;
			} 
			else 
			{
			  return;
			}
		  }

		  if(e.keyCode==13)
		  {
			  button('proceed');
		  }
		}
	</script>

	<?php

	$page->footer();
}
elseif(has_access("has_access_to_his_cer") or has_access("can_view_his_cer_data") or has_access("cer_has_full_access_to_his_projects"))
{
	$link = "cer_projects.php";
	redirect($link);
}
else
{
	redirect("noaccess.php");	
}
?>