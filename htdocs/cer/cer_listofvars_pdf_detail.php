<?php
/********************************************************************

    cer_listofvars_pdf_detail.php

    Print PDF for List of Variables

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-09-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*********************************************************************/

/********************************************************************
    prepare all data needed
*********************************************************************/
$posdata = get_pos_data($project["project_order"]);

if($project['project_projectkind'] == 3 or $project['project_projectkind'] == 4) {
	$franchisee_address = get_address($project["order_franchisee_address_id"]);
}
else
{
	//$franchisee_address = get_address($posdata["posaddress_franchisee_id"]);
	$franchisee_address = get_address($project["order_franchisee_address_id"]);
}


$client_address = get_address($project["order_client_address"]);

//get quantities of watches
$units = array();
$units[0] = "";
$units[1] = "";
$units[2] = "";

$i = 0;
$sql = "select * from cer_revenues " . 
       "where cer_revenue_project = " . param("pid") . 
	   " and cer_revenue_cer_version = 0 " .
       " order by cer_revenue_year ASC";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project["project_fagrstart"] != NULL 
		   and $project["project_fagrstart"] != '0000-00-00')
	{
		if($row["cer_revenue_year"] >=  substr($project["project_fagrstart"], 0, 4))
		{
			$units[$i] = "Units in " . $row["cer_revenue_year"] . ": " . $row["cer_revenue_quantity_watches"];
			$i++;
		}
	}
	else
	{
	    $units[$i] = "Units in " . $row["cer_revenue_year"] . ": " . $row["cer_revenue_quantity_watches"];
		$i++;
	}
	
}


$project_name = $project["order_shop_address_company"]. ", " . $project["order_shop_address_place"] . ", " . $project["country_name"];

/********************************************************************
    print PDF
*********************************************************************/
//set pdf parameters

$margin_top = 16;
$margin_left = 12;
$y = $margin_top;

$pdf->SetTitle("List of Variables");
$pdf->SetAuthor("Tissot Retailnet");
$pdf->SetDisplayMode(100);
$pdf->AddPage("P", "A4");
$pdf->setPrintFooter(false);
$pdf->SetAutoPageBreak(false, 0);

// Title first line
$pdf->SetXY($margin_left,$margin_top);
$pdf->SetFont("arialn", "I", 9);
$pdf->Cell(45, 10, "", 1);

$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);

$pdf->SetFont("arialn", "B", 11);
$pdf->Cell(122, 10, "List of Variables: " . $project_name, 1, "", "L");
$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");


// draw first box
$pdf->SetXY($margin_left,$y+8);

	// print project and investment infos
	$y = $y+11;
	$x = $margin_left;

	//franchisee
	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "1. Franchisee", 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Company Name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	if($franchisee_address["company2"])
	{
		$pdf->Cell(142, 5, $franchisee_address["company"] .  ", " . $franchisee_address["company2"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(142, 5, $franchisee_address["company"], 1, "", "L");
	}

	/*
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Legal Entity Name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $franchisee_address["legal_entity_name"], 1, "", "L");
	*/

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Address:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);

	if($franchisee_address["address2"])
	{
		$pdf->Cell(142, 5, $franchisee_address["address"] .  ", " . $franchisee_address["address2"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(142, 5, $franchisee_address["address"], 1, "", "L");
	}

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Zip and City:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $franchisee_address["zip"] . " " . $franchisee_address["place"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Country:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $franchisee_address["country_name"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Phone and Mobile:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	
	if($franchisee_address["mobile_phone"])
	{
		$pdf->Cell(142, 5, $franchisee_address["phone"] .  ", " . $franchisee_address["mobile_phone"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(142, 5, $franchisee_address["phone"], 1, "", "L");
	}

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Email:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $franchisee_address["email"], 1, "", "L");

	//Business Premises
	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "2. Business premises", 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "POS Name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $posdata["posaddress_name"], 1, "", "L");


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Address:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);

	if($posdata["posaddress_address2"])
	{
		$pdf->Cell(142, 5, $posdata["posaddress_address"] .  ", " . $posdata["posaddress_address2"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(142, 5, $posdata["posaddress_address"], 1, "", "L");
	}

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Zip and City:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $posdata["posaddress_zip"] . " " . $posdata["posaddress_place"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Country:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $posdata["country_name"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Phone and Mobile:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	
	if($posdata["posaddress_mobile_phone"])
	{
		$pdf->Cell(142, 5, $posdata["posaddress_phone"] .  ", " . $posdata["posaddress_mobile_phone"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(142, 5, $posdata["posaddress_phone"], 1, "", "L");
	}

	/*
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Contact:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $posdata["posaddress_contact_name"], 1, "", "L");
    */


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Email:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $posdata["posaddress_email"], 1, "", "L");

	/*
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Gross Area in sqms:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $project["project_cost_grosssqms"], 1, "", "L");
	*/

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Total Surface in sqms:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $project["project_cost_totalsqms"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Sales Surface upon project entry in sqms:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $project["project_cost_original_sqms"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Sales Surface according layout in sqms:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $project["project_cost_sqms"], 1, "", "L");


	//Territory
	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(127, 5, "3. Territory of Exclusivity", 1, "", "L");
	$pdf->Cell(60, 5, "4. Minimum Quantity", 1, "", "L");
	
	$y = $y+5;

	$y_old = $pdf->GetY() + 5;
	
	
	$pdf->SetXY($x+127,$y);

	/*
	foreach($units as $key=>$unit) {

		$pdf->Cell(60, 5, $units[$key], 1, "", "L");
		$pdf->SetXY($x+127,$y+(($key+1)*5));

	}
	*/

	$pdf->Cell(60, 5, $units[0], 1, "", "L");
	$pdf->SetXY($x+127,$y+5);
	$pdf->Cell(60, 5, $units[1], 1, "", "L");
	$pdf->SetXY($x+127,$y+10);
	$pdf->Cell(60, 5, $units[2], 1, "", "L");
	$pdf->SetXY($x+127,$y+15);
	

	
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->SetXY($x,$y+0.5);
	$pdf->MultiCell(126, 3.5, $posdata["posaddress_fag_territory"], 0, "L");

	$new_y = $pdf->GetY() + $key*5 + 1;
	$new_y = $pdf->GetY() + 5;
	
	$box_height = $new_y - $y_old;

	

	$y = $pdf->SetXY($margin_left, $y_old);
	
	if($box_height <=15)
	{
		$new_box_height = $box_height + (15 - $box_height);
		$pdf->Cell(127, $new_box_height, "", 1, "", "L");
	}
	else
	{
		$pdf->Cell(127, $box_height, "", 1, "", "L");
	}

	//Duration of the Agreement
	
	$y = $new_y+5;
	
	if($box_height <=15)
	{
		$y = $y + (15 - $box_height);
	}

	if($y > 220)
	{
		$pdf->AddPage();

		// Title first line
		$pdf->SetXY($margin_left,$margin_top);
		$pdf->SetFont("arialn", "I", 9);
		$pdf->Cell(45, 10, "", 1);

		$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);

		$pdf->SetFont("arialn", "B", 11);
		$pdf->Cell(122, 10, "List of Variables: " . $project_name, 1, "", "L");
		$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");

		$y = $margin_top;

		// draw first box
		$pdf->SetXY($margin_left,$y+8);

		// print project and investment infos
		$y = $y+11;
		$x = $margin_left;

		//franchisee
		$y = $y+7;
	}
	
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "5. Duration of the Agreement", 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Effective date and term of Agreement:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	$date = "";
	
	

	/*
	if($project["project_fagrstart"] != NULL and $project["project_fagrstart"] != '0000-00-00')
	{
		$date = to_system_date($project["project_fagrstart"]);
		$date = "01." . substr($date, 3, strlen($date)-1);
	}
	*/
	
	if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] != '0000-00-00')
	{
		$date = to_system_date($project["project_real_opening_date"]);
		$date = "01." . substr($date, 3, strlen($date)-1);
	}

	$pdf->Cell(142, 5, $date, 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Date of opening of the business premises:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	if($project["project_real_opening_date"] != Null and $project["project_real_opening_date"] != "0000-00-00")
	{
		$pdf->Cell(142, 5, to_system_date($project["project_real_opening_date"]), 1, "", "L");
	}
	else
	{
		//$pdf->Cell(142, 5, to_system_date($project["project_planned_opening_date"]), 1, "", "L");
		$pdf->Cell(142, 5, "", 1, "", "L");
	}

	//calculate $duration
	$duration = "";

	if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] != '0000-00-00')
	{
		$months = 13 - substr($project["project_real_opening_date"], 5,2);
		$duration = "3 years and " . $months . " months";
	}


	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Term of contractual period:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $duration, 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Expiring date of the contractual period:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	/*
	$date = "";
	if($posdata["posaddress_fagrstart"] != NULL and $posdata["posaddress_fagrstart"] != '0000-00-00' and $project["project_projectkind"] < 3)
	{
		$date = to_system_date($posdata["posaddress_fagrstart"]);
		if(substr($date, 6, strlen($date)-1) < 10) {
			$date = "31.12.0" . (substr($date, 6, strlen($date)-1) + 3);
		}
		else
		{
			$date = "31.12." . (substr($date, 6, strlen($date)-1) + 3);
		}
		$pdf->Cell(142, 5, $date, 1, "", "L");
	}
	else
	{
		$pdf->Cell(142, 5, " ", 1, "", "L");
	}
	*/


	$date = "";
	if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] != '0000-00-00' and $project["project_projectkind"] < 3)
	{
		$date = to_system_date($project["project_real_opening_date"]);
		if(substr($date, 6, strlen($date)-1) < 10) {
			$date = "31.12.0" . (substr($date, 6, strlen($date)-1) + 3);
		}
		else
		{
			$date = "31.12." . (substr($date, 6, strlen($date)-1) + 3);
		}
		$pdf->Cell(142, 5, $date, 1, "", "L");
	}
	else
	{
		$pdf->Cell(142, 5, " ", 1, "", "L");
	}
	


	//client
	$y = $y+7;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "B", 8);
	$pdf->Cell(187, 5, "6. Tissot Subsidiary or Agent", 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Company Name:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	if($client_address["company2"])
	{
		$pdf->Cell(142, 5, $client_address["company"] .  ", " . $client_address["company2"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(142, 5, $client_address["company"], 1, "", "L");
	}

	/*
	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Legal Form of Company:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $client_address["legal_entity_name"], 1, "", "L");
	*/

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Address:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);

	if($client_address["address2"])
	{
		$pdf->Cell(142, 5, $client_address["address"] .  ", " . $client_address["address2"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(142, 5, $client_address["address"], 1, "", "L");
	}

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Zip and City:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $client_address["zip"] . " " . $client_address["place"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Country:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $client_address["country_name"], 1, "", "L");

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Phone and Mobile:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	
	
	if($client_address["mobile_phone"])
	{
		$pdf->Cell(142, 5, $client_address["phone"] .  ", " . $client_address["mobile_phone"], 1, "", "L");
	}
	else
	{
		$pdf->Cell(142, 5, $client_address["phone"], 1, "", "L");
	}

	$y = $y+5;
	$pdf->SetXY($x,$y);
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(45, 5, "Email:", 1, "", "L");
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(142, 5, $client_address["email"], 1, "", "L");

	
	$pdf->Ln();
	
	
	if($posdata['posaddress_fag_city_pasted'])
	{ 
		$filetype = strtolower(substr($posdata['posaddress_fag_city_pasted'], strlen($posdata['posaddress_fag_city_pasted'])-3, strlen($posdata['posaddress_fag_city_pasted'])));

		$source_file = ".." . $posdata['posaddress_fag_city_pasted'];
		
		
		
		if($filetype == 'pdf')
		{
			if(file_exists($source_file))
			{
				
				$pdfString = $pdf->output('', 'S');
				$tmp = SetaPDF_Core_Document::loadByString($pdfString);
				
				if(!isset($merger)) {
					$merger = new SetaPDF_Merger();
				}
				
				$merger->addDocument($tmp);

				$merger->addFile(array(
					'filename' => $source_file,
					'copyLayers' => true
				));
				
				

				$pdf = new MYPDF("P", "mm", "A4");
				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(true);
				$pdf->SetMargins(10, 23, 10);

				$pdf->AddFont('arialn','');
				$pdf->AddFont('arialn','B');
				$pdf->AddFont('arialn','I');
				$pdf->AddFont('arialn','BI');

				$pdf->Open();
				
				$PDFmerger_was_used = true;
				$pdf_print_output = false;

				
			}
		}
		elseif($filetype == 'jpg')
		{

			if(file_exists($source_file))
			{
				$pdf->AddPage("L");
				$margin_top = 16;
				$margin_left = 12;
				$pdf->SetXY($margin_left,$margin_top);

				$pdf->SetFont("arialn", "I", 9);
				$pdf->Cell(45, 10, "", 1);

				$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);

				$pdf->SetFont("arialn", "B", 11);
				$pdf->Cell(172, 10, "City Map: " . $project_name, 1, "", "L");
				$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");

				$pdf->Image($source_file,20,40, 200);
			}
		}
	}
	elseif (!preg_match("/retailnet.mpx02/", $_SERVER["HTTP_HOST"]))
	{ 
		
		if($posdata["posaddress_google_lat"] and $posdata["posaddress_google_long"])
		{ 
			$latitude = $posdata["posaddress_google_lat"];
			$longitude = $posdata["posaddress_google_long"];

			//insert google map
			$pdf->AddPage("P");
			
			// Title first line
			$pdf->SetXY($margin_left,$margin_top);
			$pdf->SetFont("arialn", "I", 9);
			$pdf->Cell(45, 10, "", 1);

			$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);

			$pdf->SetFont("arialn", "B", 11);
			$pdf->Cell(122, 10, "City Map: " . $project_name, 1, "", "L");
			$pdf->Cell(20, 10, date("d.m.Y"), 1, "", "C");


			$pdf->setY(40);

			
			
			$url = STATIC_MAPS_HOST;
			$url .= '?center=' . $latitude . ',' . $longitude;
			$url .= '&zoom=16';
			$url .= '&size=640x640';
			$url .= '&maptype=roadmap' . "\\";
			$url .= '&markers=color:red%7Clabel:P%7C' . $latitude . ',' . $longitude;
			$url .= '&format=jpg';
			$url .= '&key=' . GOOGLE_API_KEY;
			$url .= '&sensor=false';

			
			$tmpfilename1 = "map1" . time() . ".jpg";

			$mapImage1 = '';
			if(!$context) {
				
				$mapImage1 = file_get_contents($url) or $mapImage1 = '';
			}
			else
			{
				$mapImage1 = file_get_contents($url, false, $context) or $mapImage1 = '';
			}
			
			if($mapImage1) {
				$fh = fopen(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1, 'w') or die("can't open file");
				fwrite($fh, $mapImage1);
				fclose($fh);
				
				if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1)) {
					$pdf->Image(TMP_FILE_DIR_RELATIVE . $tmpfilename1, $margin_left, $pdf->getY(), 90, 90);
				}
			}

			
			$url = STATIC_MAPS_HOST;
			$url .= '?center=' . $latitude . ',' . $longitude;
			$url .= '&zoom=14';
			$url .= '&size=640x640';
			$url .= '&maptype=roadmap' . "\\";
			$url .= '&markers=color:red%7Clabel:P%7C' . $latitude . ',' . $longitude;
			$url .= '&format=jpg';
			$url .= '&key=' . GOOGLE_API_KEY;
			$url .= '&sensor=false';

			$tmpfilename2 = "map2" . time() . ".jpg";
			
			$mapImage2 = '';
			if(!$context) {
				
				$mapImage2 = file_get_contents($url) or $mapImage2 = '';
			}
			else
			{
				$mapImage2 = file_get_contents($url, false, $context) or $mapImage2 = '';
			}

			if($mapImage2) {
				$fh = fopen(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2, 'w') or die("can't open file");
				fwrite($fh, $mapImage2);
				fclose($fh);
			}

			if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2)) {
				$pdf->Image(TMP_FILE_DIR_RELATIVE . $tmpfilename2, 108, $pdf->getY(), 90, 90);
			}

			if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1))
			{
				unlink(TMP_FILE_DIR_ABSOLUTE . $tmpfilename1);
			}

			if(file_exists(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2))
			{
				unlink(TMP_FILE_DIR_ABSOLUTE . $tmpfilename2);
			}
		}
	}
?>