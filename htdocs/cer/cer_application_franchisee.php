<?php
/********************************************************************

    cer_application_franchisee.php

    Application Form: franchisee information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/
$client_address = get_address($project["order_client_address"]);
$posdata = get_pos_data($project["project_order"]);
$franchisee_address = get_address($posdata["posaddress_franchisee_id"]);
$franchisor_address = get_address($posdata["posaddress_franchisor_id"]);

if(!array_key_exists("company", $franchisee_address))
{
	$franchisee_address["company"] = "";
	$franchisee_address["zip"] = "";
	$franchisee_address["place"] = "";
	$franchisee_address["country_name"] = "";

}

//get quantities of watches
$quantity_watches = array();

$sql = "select * from cer_revenues " . 
       "where cer_revenue_project = " . param("pid") . 
	   " and cer_revenue_cer_version = 0 ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$quantity_watches[$row["cer_revenue_year"]] = $row["cer_revenue_quantity_watches"];
}
ksort($quantity_watches);

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";

/*
//agreemant data

//calculate franchisee agreement duration
if($posdata["posaddress_fagrstart"] != NULL and $posdata["posaddress_fagrstart"] != '0000-00-00')
{
	$starting_date = $posdata["posaddress_fagrstart"];
	$expiry_date = $posdata["posaddress_fagrend"];
}
else
{
	$fag_data = get_fag_data($project["project_planned_opening_date"]);
	$starting_date = $fag_data["starting_date"];
	$expiry_date = $fag_data["ending_date"];

	$sql = "update " . $posdata["table"] . " set " .
		   "posaddress_fagrstart = " . dbquote($starting_date) . ", " .
		   "posaddress_fagrend = " . dbquote($expiry_date) .
		   "where posaddress_id = " . $posdata["posaddress_id"];
	
	$result = mysql_query($sql) or dberror($sql);
}

$duration = "";
$sql = "select IF(TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend) >0, TIMESTAMPDIFF(MONTH,posaddress_fagrstart,posaddress_fagrend), 'n.a.') as months from ". $posdata["table"] . " where posaddress_id = " . $posdata["posaddress_id"];

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$duration = $row["months"];
	$duration = floor($duration/12);

	$months = 1 + $row["months"] - $duration*12;


	if($months == 12)
	{
		$duration = $duration + 1 . " years";
	}
	
	elseif($months > 0)
	{
		$duration = $duration . " years and " . (1 + $row["months"] - $duration*12) . " months";
	}
}
*/


//AF/LN Supporting Documents
if($form_type == "AF" or $form_type == "INR03")
{
	$sql_attachment = "select distinct order_file_id, order_file_visited, ".
					  "    order_file_title, order_file_description, ".
					  "    order_file_path, file_type_name, ".
					  "    order_files.date_created, ".
					  "    order_file_category_name, order_file_category_priority, ".
					  "    concat(user_name, ' ', user_firstname) as owner_fullname ".
					  "from order_files  " .
					  "left join order_file_categories on order_file_category_id = order_file_category ".
					  "left join users on user_id = order_file_owner ".
					  "left join file_types on order_file_type = file_type_id ";

	$list1_filter = "order_file_category_id = 17 and order_file_order = " . $project["project_order"];
}


/********************************************************************
    build form
*********************************************************************/
$form = new Form("posaddresses", "posaddress");

$donotshowfranchisee = true;
include("include/project_head.php");


$form->add_hidden("pid", param("pid"));


if($project["project_cost_type"] != 6 and $project["project_postype"] !=2)
{
	$form->add_section("Franchisee");
}
else
{
	$form->add_section("Owner Company");
}

$form->add_label("franchisee_company", "Company", 0, $franchisee_address["company"]);
$form->add_label("franchisee_address", "City", 0, $franchisee_address["zip"] . " " . $franchisee_address["place"]);
$form->add_label("franchisee_country", "Country", 0, $franchisee_address["country_name"]);


if($project["project_actual_opening_date"] == NULL or $project["project_actual_opening_date"] == '0000-00-00')
{
	if (has_access("can_edit_franchisee_agreement_data"))
	{

		if(count($posdata) > 0 and array_key_exists("posaddress_fagagreement_type", $posdata) and  $posdata["posaddress_fagagreement_type"] > 0 and $project["project_projectkind"] ==  2) // renovation projects
		{

			/*
			$type = $posdata["posaddress_fagagreement_type"];
			$sent = $posdata["posaddress_fagrsent"];
			$signed = $posdata["posaddress_fagrsigned"];
			$start = $posdata["posaddress_fagrstart"];
			$end = $posdata["posaddress_fagrend"];
			$comment = $posdata["posaddress_fag_comment"];
			

			
			$type = "";
			$sent = "";
			$signed = "";
			$start = "";
			$end = "";
			$comment = "";;
			*/

			$type = $project["project_fagagreement_type"];
			$sent = $project["project_fagrsent"];
			$signed = $project["project_fagrsigned"];
			$start = $project["project_fagrstart"];
			$end = $project["project_fagrend"];
			$comment = $project["project_fag_comment"];
		}
		else
		{
			$type = $project["project_fagagreement_type"];
			$sent = $project["project_fagrsent"];
			$signed = $project["project_fagrsigned"];
			$start = $project["project_fagrstart"];
			$end = $project["project_fagrend"];
			$comment = $project["project_fag_comment"];

		}
		
		$form->add_section("Agreement");
		$form->add_list("project_fagagreement_type", "Agreement Type*", "select agreement_type_id, agreement_type_name from agreement_types", 0, $type);
		$form->add_checkbox("project_fagrsent", "Sent", $sent, 0, "Agreement");
		$form->add_checkbox("project_fagrsigned", "Signed", $signed, 0, "Agreement");
		$form->add_label("dummy", "");
		$form->add_edit("project_fagrstart", "Agreement Starting Date", 0, to_system_date($start), TYPE_DATE);
		$form->add_edit("project_fagrend", "Agreement Ending Date", 0, to_system_date($end), TYPE_DATE);
		$form->add_multiline("project_fag_comment", "Comment", 4, 0, $comment);
	}
	else
	{	
		if(count($posdata) > 0 and array_key_exists("posaddress_fagagreement_type", $posdata) and  $posdata["posaddress_fagagreement_type"] > 0)
		{
			$type = $posdata["posaddress_fagagreement_type"];
			$sent = $posdata["posaddress_fagrsent"];
			$signed = $posdata["posaddress_fagrsigned"];
			$start = $posdata["posaddress_fagrstart"];
			$end = $posdata["posaddress_fagrend"];
			$comment = $posdata["posaddress_fag_comment"];
		}
		else
		{
			$type = $project["project_fagagreement_type"];
			$sent = $project["project_fagrsent"];
			$signed = $project["project_fagrsigned"];
			$start = $project["project_fagrstart"];
			$end = $project["project_fagrend"];
			$comment = $project["project_fag_comment"];
		}
		
		$form->add_hidden("project_fagagreement_type", $type);
		$form->add_hidden("project_fagrsent", $sent);
		$form->add_hidden("project_fagrsigned", $signed);
		$form->add_hidden("project_fagrstart", to_system_date($start));
		$form->add_hidden("project_fagrend", to_system_date($end));
		$form->add_hidden("project_fag_comment",$comment);


	}
}
elseif (has_access("can_edit_franchisee_agreement_data"))
{
	if(count($posdata) >0  and array_key_exists("posaddress_fagagreement_type", $posdata) and  $posdata["posaddress_fagagreement_type"] > 0)
	{
		$type = $posdata["posaddress_fagagreement_type"];
		$sent = $posdata["posaddress_fagrsent"];
		$signed = $posdata["posaddress_fagrsigned"];
		$start = $posdata["posaddress_fagrstart"];
		$end = $posdata["posaddress_fagrend"];
		$comment = $posdata["posaddress_fag_comment"];
	}
	else
	{
		$type = $project["project_fagagreement_type"];
		$sent = $project["project_fagrsent"];
		$signed = $project["project_fagrsigned"];
		$start = $project["project_fagrstart"];
		$end = $project["project_fagrend"];
		$comment = $project["project_fag_comment"];
	}
	
	$form->add_section("Agreement");
	$form->add_list("project_fagagreement_type", "Agreement Type*", "select agreement_type_id, agreement_type_name from agreement_types", 0, $type);
	$form->add_checkbox("project_fagrsent", "Sent", $sent, 0, "Agreement");
	$form->add_checkbox("project_fagrsigned", "Signed", $signed, 0, "Agreement");
	$form->add_label("dummy", "");
	$form->add_edit("project_fagrstart", "Agreement Starting Date", 0, to_system_date($start), TYPE_DATE);
	$form->add_edit("project_fagrend", "Agreement Ending Date", 0, to_system_date($end), TYPE_DATE);
	$form->add_multiline("project_fag_comment", "Comment", 4, 0, $comment);
}


if($form_type == "AF" or $form_type == "INR03")
{
	$form->add_section("Relation");
	$form->add_checkbox("address_is_independent_retailer", "Company is an independent retailer", $franchisee_address["address_is_independent_retailer"], "", "Independent Retailer");
	$form->add_checkbox("franchisee_address_swatch_retailer", "Company is a present ". BRAND ." Retailer", $franchisee_address["address_swatch_retailer"], "", "Relation with " . BRAND);
	
	
	
	$form->add_checkbox("cer_basicdata_franchsiee_already_partner", "Company is already a business partner of " . BRAND, $cer_basicdata["cer_basicdata_franchsiee_already_partner"], "", "Third Party Business Partner");

	$form->add_edit("address_company_is_partner_since", "Company is partner since", 0, to_system_date($franchisee_address["address_company_is_partner_since"]), TYPE_DATE);

	$form->add_edit("cer_basicdata_franchsiee_number_of_pos", "&nbsp;&nbsp;- with Numer of Stores", 0, $cer_basicdata["cer_basicdata_franchsiee_number_of_pos"], TYPE_INT, 8);

	$form->add_edit("cer_basicdata_franchsiee_brands", "&nbsp;&nbsp;- of the following Brands", 0, $cer_basicdata["cer_basicdata_franchsiee_brands"]);
}
else
{
	$form->add_section("Relation");
	$form->add_checkbox("franchisee_address_swatch_retailer", "Company is a present Tissot Retailer", $franchisee_address["address_swatch_retailer"], "", "Relation");


	$form->add_hidden("address_is_independent_retailer");
	$form->add_hidden("address_company_is_partner_since");

	$form->add_hidden("cer_basicdata_franchsiee_already_partner");
	$form->add_hidden("cer_basicdata_franchsiee_number_of_pos");
	$form->add_hidden("cer_basicdata_franchsiee_brands");
}


$form->add_section("Territory Exclusivity");
$form->add_comment("Please give a description clearly delimitating the exclusive territory of this POS location, covered by the Franchisee contract.");



if(has_access("has_access_to_his_cer") or has_access("has_full_access_to_cer"))
{
	$form->add_multiline("posaddress_fag_territory", "Description*", 4, NOTNULL, $posdata["posaddress_fag_territory"]);
	
	
	if($posdata["posaddress_fag_city_pasted"])
	{
		$form->add_upload("posaddress_fag_city_pasted", "City Map (PDF or JPG only)*", "/files/cer/". $project["order_number"],  $deletable, $posdata["posaddress_fag_city_pasted"]);
	}
	else
	{
		$form->add_hidden("posaddress_fag_city_pasted");
	}
}
else
{
	$form->add_label("posaddress_fag_territory", "Agreement Content");
	
	if($posdata["posaddress_fag_city_pasted"])
	{
		$link = "<a href=\"#\" onClick=\"javascript:popup('" . $posdata["posaddress_fag_city_pasted"] . "', 640, 480);\">Show City Pasted</a>";
		$form->add_label("posaddress_fag_city_pasted", "City Pasted", RENDER_HTML, $link);
	}

}

$deletable = "";
if(has_access("can_delete_uploaded_files"))
{
	$deletable = DELETABLE;
}



if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
{
	$form->add_section("Mall Map/Street Map (PDF only)*:	");
	$form->add_upload("ln_basicdata_floorplan", "Mall Map/Street Map (PDF or JPG only)*", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_floorplan"], 1, "floorplan");

	
	if($show_location_layout_field == true)
	{
		$form->add_upload("ln_basicdata_location_layout", "Location layout with dimensions (pdf only)*", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_location_layout"]);
	}
	else
	{
		$form->add_hidden("ln_basicdata_location_layout");
	}

}
elseif($form_type == "AF" or $form_type == "INR03")
{

	$form->add_section("Attachments");
	$form->add_comment("For best results in printing we recommend to upload files having the format A4 landscape.");


	$form->add_upload("ln_basicdata_floorplan", "Mall Map/Street Map (PDF or JPG only)*", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_floorplan"], 1, "floorplan");

	
	if($show_location_layout_field == true)
	{
		$form->add_upload("ln_basicdata_location_layout", "Location layout with dimensions (pdf only)*", "/files/ln/". $ln_basicdata["project_number"], $deletable, $ln_basicdata["ln_basicdata_location_layout"]);
	}
	else
	{
		$form->add_hidden("ln_basicdata_location_layout");
	}

	$form->add_comment("Insert Pictures of the Location proposed from different direction large view and close ones.");
	$form->add_upload("ln_basicdata_pix1", "Photo 1 Left View (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pix1"]);
	$form->add_upload("ln_basicdata_pix2", "Photo 2 Right View (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pix2"]);
	$form->add_upload("ln_basicdata_pix3", "Photo 3 Large one (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pix3"]);
	$form->add_upload("ln_basicdata_pix4", "Photo 4 Close one (JPG only)*", "/files/ln/". $ln_basicdata["project_number"], NOTNULL | $deletable, $ln_basicdata["ln_basicdata_pix4"]);
}




//check if business plan can be edited
require_once("include/check_if_business_plan_is_editable.php");



if(($can_edit_business_plan == true and $project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer") and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") and $project["project_state"] != 2))
{
	if (has_access("can_add_attachments_in_projects") and ($form_type == "AF" or $form_type == "INR03"))
	{
		
		$urlQuery = http_build_query(array(
			'action' => '/cer/ajx_upload_supporting_documents.php',
			'title' => 'Upload Files',
			'subtitle' => 'Supporting Documents',
			'infotext' => 'Allowed file types are: pdf, jpg, jpeg',
			'fields' => array(
					'id' => param("pid"),
			        'category' => 17,
					'extensions' => 'pdf,jpg,jpeg',
					'startload' => true
			)
		));
		
		$iframeAttributes = array(
			"data-fancybox-type" => "iframe",
			"data-fancybox-width" => "800",
			"data-fancybox-height" => "620",
			"href" => "/public/themes/default/multifile.uploader.php?$urlQuery"
		);
		
		$form->add_button("add_attachment", "Add Supporting Documents", $iframeAttributes, IS_UPLOAD_BUTTON);
	}
	
	$form->add_button("save", "Save Data");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();




/********************************************************************
    List of AF/LN supporting documents
*********************************************************************/
if($form_type == "AF" or $form_type == "INR03")
{
	$list1 = new ListView($sql_attachment);

	if($project["project_cost_type"] == 1)
	{
		$list1->set_title("LNR Supporting Documents");
	}
	else
	{
		$list1->set_title("LNR Supporting Documents");
	}

	$list1->set_entity("order_files");
	$list1->set_filter($list1_filter);
	$list1->set_order("order_files.date_created DESC");



	$link = "http://" . $_SERVER["HTTP_HOST"] . "/include/openfile.php?id={order_file_id}";
	$list1->add_column("order_file_title", "Title", $link, "", "", COLUMN_NO_WRAP);
	$list1->add_column("file_type_name", "Type", "", "", "", COLUMN_NO_WRAP);
}



if($form->button("save"))
{
		
	
	$files_ok = true;
	
	$floor_plan_ext = substr(strtolower($form->value('ln_basicdata_floorplan')), -3, 3);
	
	$layout_ext = substr(strtolower($form->value('ln_basicdata_location_layout')), -3, 3);

	if($form_type == "AF" or $form_type == "INR03")
	{
		
		$city_pasted_ext = substr(strtolower($form->value('posaddress_fag_city_pasted')), -3, 3);
	}
	
	
	
	if($floor_plan_ext and $floor_plan_ext != 'pdf' and $floor_plan_ext != 'jpg') 
	{
		$form->error('Only PFD or JPG Files are allowed for uploads.');	
		$files_ok = false;
	}
	elseif($layout_ext and $layout_ext != 'pdf' and $layout_ext != 'jpg') 
	{
		$form->error('Only PFD or JPG Files are allowed for uploads.');	
		$files_ok = false;
	}
	elseif($form_type == "AF" and $city_pasted_ext and $city_pasted_ext != 'pdf' and $city_pasted_ext != 'jpg') 
	{
		$form->error('Only PFD or JPG Files are allowed for uploads.');
		$files_ok = false;
	}
	
	if($form_type == "AF" or $form_type == "INR03")
	{
		if($form->value("ln_basicdata_pix1") and strpos(strtolower($form->value("ln_basicdata_pix1")), 'jpg') == 0)
		{
			$form->error("Pictures mus be JPG-Files.");
			$files_ok = false;
		}
		elseif($form->value("ln_basicdata_pix2") and strpos(strtolower($form->value("ln_basicdata_pix2")), 'jpg') == 0)
		{
			$form->error("Pictures mus be JPG-Files.");
			$files_ok = false;
		}
		elseif($form->value("ln_basicdata_pix3") and strpos(strtolower($form->value("ln_basicdata_pix3")), 'jpg') == 0)
		{
			$form->error("Pictures mus be JPG-Files.");
			$files_ok = false;
		}
		elseif($form->value("ln_basicdata_pix4") and strpos(strtolower($form->value("ln_basicdata_pix4")), 'jpg') == 0)
		{
			$form->error("Pictures mus be JPG-Files.");
			$files_ok = false;
		}
	}
	
	if($files_ok == true and $form->validate())
	{

		$sql = "update addresses set
		address_swatch_retailer = " . dbquote($form->value("franchisee_address_swatch_retailer")) . ", " . 
		"address_is_independent_retailer = " . dbquote($form->value("address_is_independent_retailer")) . ", " . 
		"address_company_is_partner_since = " . dbquote(from_system_date($form->value("address_company_is_partner_since"))) . 
		" where address_id = " . $posdata["posaddress_franchisee_id"];
		
		mysql_query($sql) or dberror($sql);
			
			
		//update ln_basic_data
		$fields = array();

		$value = dbquote($form->value("ln_basicdata_floorplan"));
		$fields[] = "ln_basicdata_floorplan = " . $value;

		$value = dbquote($form->value("ln_basicdata_location_layout"));
		$fields[] = "ln_basicdata_location_layout = " . $value;


		if($form_type == "AF" or $form_type == "INR03")
		{
			

			$value = dbquote($form->value("ln_basicdata_pix1"));
			$fields[] = "ln_basicdata_pix1 = " . $value;

			$value = dbquote($form->value("ln_basicdata_pix2"));
			$fields[] = "ln_basicdata_pix2 = " . $value;

			$value = dbquote($form->value("ln_basicdata_pix3"));
			$fields[] = "ln_basicdata_pix3 = " . $value;

			$value = dbquote($form->value("ln_basicdata_pix4"));
			$fields[] = "ln_basicdata_pix4 = " . $value;
		}

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$project_fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update ln_basicdata set " . join(", ", $fields) . 
			   " where ln_basicdata_project = " . param("pid") . 
			   " and ln_basicdata_version = 0";
		mysql_query($sql) or dberror($sql);
		
		//update posaddress data

		$fields = array();
		
		$value = dbquote($form->value("posaddress_fag_territory"));
		$fields[] = "posaddress_fag_territory = " . $value;

		$value = dbquote($form->value("posaddress_fag_city_pasted"));
		$fields[] = "posaddress_fag_city_pasted = " . $value;

		$value1 = "current_timestamp";
		$project_fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$project_fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update " . $posdata["table"] . " set " . join(", ", $fields) . " where posaddress_id = " . $posdata["posaddress_id"];
		mysql_query($sql) or dberror($sql);



		//update cer basic data

		$fields = array();
		
		$value = dbquote($form->value("cer_basicdata_franchsiee_already_partner"));
		$fields[] = "cer_basicdata_franchsiee_already_partner = " . $value;

		$value = dbquote($form->value("cer_basicdata_franchsiee_number_of_pos"));
		$fields[] = "cer_basicdata_franchsiee_number_of_pos = " . $value;

		$value = dbquote($form->value("cer_basicdata_franchsiee_brands"));
		$fields[] = "cer_basicdata_franchsiee_brands = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_version = 0 and cer_basicdata_id = " . $cer_basicdata["cer_basicdata_id"];
		mysql_query($sql) or dberror($sql);



		//update agreement data
		$project_fields = array();

		$value = trim($form->value("project_fagagreement_type")) == "" ? "0" : dbquote($form->value("project_fagagreement_type"));
		$project_fields[] = "project_fagagreement_type = " . $value;

		$value = trim($form->value("project_fagrsent")) == "" ? "0" : dbquote($form->value("project_fagrsent"));
		$project_fields[] = "project_fagrsent = " . $value;

		$value = trim($form->value("project_fagrsigned")) == "" ? "0" : dbquote($form->value("project_fagrsigned"));
		$project_fields[] = "project_fagrsigned = " . $value;

		$value = trim($form->value("project_fagrstart")) == "" ? "NULL" : dbquote(from_system_date($form->value("project_fagrstart")));
		$project_fields[] = "project_fagrstart = " . $value;

		$value = trim($form->value("project_fagrend")) == "" ? "NULL" : dbquote(from_system_date($form->value("project_fagrend")));
		$project_fields[] = "project_fagrend = " . $value;

		$value = trim($form->value("project_fag_comment")) == "" ? "null" : dbquote($form->value("project_fag_comment"));
		$project_fields[] = "project_fag_comment = " . $value;

		$sql = "update projects set " . join(", ", $project_fields) . " where project_id = " . dbquote(param("pid"));
		mysql_query($sql) or dberror($sql);

		$form->message("Your data has bee saved.");

		$link = "cer_application_franchisee.php?pid=" . param("pid");
		redirect($link);
	}
}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();
if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Franchisee Information");
}
elseif($form_type == "AF")
{
	$page->title("LNR/INR03: Franchisee Information");
}
else
{
	$page->title("LNR/CER: Franchisee Information");
}

require_once("include/tabs2016.php");
$form->render();

if($form_type == "AF" or $form_type == "INR03")
{
	echo "<p>&nbsp;</p>";
	$list1->render();
}

?>


<div id="floorplan" style="display:none;">
    Please make sure that the mall/street map contains:
	<ul>
	<li>a clear indication of future Tissot Location</li>
	<li>a clear indication of who our direct neighbours are</li>
	<li>indication of major brands an brand mix of the shopping mall</li>
	</ul>
</div>


<?php

require "include/footer_scripts.php";



if($form_type == "AF" or $form_type == "INR03") {
/*
	// translations
	$translate = Translate::instance();

	$modalContent = Template::load('file.uploader', array(
		'action' => '/cer/ajx_upload_supporting_documents.php',
		'class' => 'modal-750',
		'title' => $translate->upload_file,
		'subtitle' => 'Supporting Documents',
		'infotext' => 'Allowed file types are: pdf, jpg, jpeg',
		'fields' => array(
				'id' => param("pid"),
				'extensions' => 'pdf,jpg,jpeg',
				'startload' => true
		),
		'attributes' => array(
				'data-url' => '/applications/helpers/file.uploader.php',
				'data-upload-template-id' => 'retailnet-template-upload',
				'data-download-template-id' => 'retailnet-template-download',
				'data-auto-upload' => true,
				'data-sequential-uploads' => true
		)
	));
*/
}

$page->footer();

?>