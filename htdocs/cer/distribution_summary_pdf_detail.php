<?php
/********************************************************************

    distribution_summary_pdf_detail.php

    Print Distribution Summary

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-09-27
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*********************************************************************/

/********************************************************************
    prepare all data needed
*********************************************************************/
$posdata = get_pos_data($project["project_order"]);


$client_address = get_address($project["order_client_address"]);


$project_name = $project["order_shop_address_company"]. ", " . $project["order_shop_address_place"] . ", " . $project["country_name"];


$date_limit = date("Y-m-d");
$relevant_year = date("Y");
if($ln_version == 0)
{
	$submission_date = date("d.m.Y");
}
else
{
	$submission_date = to_system_date($cer_basicdata['date_created']);
	$date_limit = from_system_date(to_system_date($cer_basicdata['date_created']));
	$relevant_year = substr($cer_basicdata['date_created'], 0, 4);
}


//get all operating POS locations in the country

$show_corporate_data_in_af = false;
$show_wholesale_data_in_cer = false;

$pos_per_country = array();
$pos_per_country[1] = ""; // store;
$pos_per_country[2] = ""; // sis;
$pos_per_country[3] = ""; // kiosk;
$pos_per_country[4] = ""; // independent retailer;

$pos_per_city = array();
$pos_per_city[1] = ""; // store;
$pos_per_city[2] = ""; // sis;
$pos_per_city[3] = ""; // kiosk;
$pos_per_city[4] = ""; // independent retailer;

$pos_per_country2 = array();
$pos_per_country2[1] = ""; // store;
$pos_per_country2[2] = ""; // sis;
$pos_per_country2[3] = ""; // kiosk;
$pos_per_country2[4] = ""; // independent retailer;

$pos_per_city2 = array();
$pos_per_city2[1] = ""; // store;
$pos_per_city2[2] = ""; // sis;
$pos_per_city2[3] = ""; // kiosk;
$pos_per_city2[4] = ""; // independent retailer;


$distribution_summary_available = true;
if($ln_version == 0)
{
	if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
	{
		$sql_p = "select posaddress_store_postype, count(posaddress_id) as cnt from posaddresses " . 
				 " where posaddress_country = " . dbquote($project["order_shop_address_country"]) . 
				 " and (posaddress_store_closingdate is null  or posaddress_store_closingdate = '0000-00-00') " .
			     " and posaddress_ownertype = 1 " .
				 " group by posaddress_store_postype";

	}
	else
	{
		$sql_p = "select posaddress_store_postype, count(posaddress_id) as cnt from posaddresses " . 
				 " where posaddress_country = " . dbquote($project["order_shop_address_country"]) . 
				 " and (posaddress_store_closingdate is null  or posaddress_store_closingdate = '0000-00-00') " .
				 " and posaddress_ownertype != 1 " .
				 " group by posaddress_store_postype";

	}


	$res_p = mysql_query($sql_p) or dberror($sql_p);

	while ($row_p = mysql_fetch_assoc($res_p))
	{
		$pos_per_country[$row_p["posaddress_store_postype"]] = $row_p["cnt"];
	}


	if($form_type == "AF" or $form_type == "INR03")
	{
		$sql_p = "select posaddress_store_postype, count(posaddress_id) as cnt from posaddresses " . 
				 " where posaddress_country = " . dbquote($project["order_shop_address_country"]) . 
				 " and (posaddress_store_closingdate is null  or posaddress_store_closingdate = '0000-00-00') " .
			     " and posaddress_ownertype = 1 " .
				 " group by posaddress_store_postype";


		$res_p = mysql_query($sql_p) or dberror($sql_p);

		while ($row_p = mysql_fetch_assoc($res_p))
		{
			$pos_per_country2[$row_p["posaddress_store_postype"]] = $row_p["cnt"];

			if($row_p["cnt"] > 0)
			{
				$show_corporate_data_in_af = true;
			}
		}
	}
	else
	{
		$sql_p = "select posaddress_store_postype, count(posaddress_id) as cnt from posaddresses " . 
				 " where posaddress_country = " . dbquote($project["order_shop_address_country"]) . 
				 " and (posaddress_store_closingdate is null  or posaddress_store_closingdate = '0000-00-00') " .
			     " and posaddress_ownertype != 1 " .
				 " group by posaddress_store_postype";


		$res_p = mysql_query($sql_p) or dberror($sql_p);

		while ($row_p = mysql_fetch_assoc($res_p))
		{
			$pos_per_country2[$row_p["posaddress_store_postype"]] = $row_p["cnt"];

			if($row_p["cnt"] > 0)
			{
				$show_wholesale_data_in_cer = true;
			}
		}
	}
	


	//get all operating POS locations in the city

	//check if we have a posorder
	$tmp_place_id = 0;
	$sql = "select posorder_parent_table, posorder_posaddress " . 
			   "from posorderspipeline " . 
			   "where posorder_order = " . $project["order_id"];

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		
		if($row["posorder_parent_table"] == 'posaddresses')
		{
			$sql = "select posaddress_place_id " . 
				   " from posaddresses " . 
				   " where posaddress_id = " . dbquote($row["posorder_posaddress"]);

			
		}
		else
		{
			$sql = "select posaddress_place_id " . 
				   " from posaddressespipeline " . 
				   " where posaddress_id = " . dbquote($row["posorder_posaddress"]);
		}

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$tmp_place_id = $row["posaddress_place_id"];
		}
		
	}
	else
	{
		$sql = "select posorder_posaddress " . 
			   "from posorders " . 
			   "where posorder_order = " . $project["order_id"];

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$sql = "select posaddress_place_id " . 
				   " from posaddresses " . 
				   " where posaddress_id = " . $row["posorder_posaddress"];

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$tmp_place_id = $row["posaddress_place_id"];
			}
		}
	}

	
	if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
	{
		$sql_p = "select posaddress_store_postype, count(posaddress_id) as cnt from posaddresses " . 
				 " where posaddress_place_id = " . dbquote($tmp_place_id) . 
				 " and (posaddress_store_closingdate is null  or posaddress_store_closingdate = '0000-00-00') " . 
			     " and posaddress_ownertype = 1 " .
				 " group by posaddress_store_postype";
	}
	else
	{
		$sql_p = "select posaddress_store_postype, count(posaddress_id) as cnt from posaddresses " . 
				 " where posaddress_place_id = " . dbquote($tmp_place_id) . 
				 " and (posaddress_store_closingdate is null  or posaddress_store_closingdate = '0000-00-00') " . 
				 " and posaddress_ownertype != 1 " .
				 " group by posaddress_store_postype";

	}


	$res_p = mysql_query($sql_p) or dberror($sql_p);

	while ($row_p = mysql_fetch_assoc($res_p))
	{
		$pos_per_city[$row_p["posaddress_store_postype"]] = $row_p["cnt"];
	}


	if($form_type == "AF" or $form_type == "INR03")
	{
		$sql_p = "select posaddress_store_postype, count(posaddress_id) as cnt from posaddresses " . 
				 " where posaddress_place_id = " . dbquote($tmp_place_id) . 
				 " and (posaddress_store_closingdate is null  or posaddress_store_closingdate = '0000-00-00') " . 
			     " and posaddress_ownertype = 1 " .
				 " group by posaddress_store_postype";

		$res_p = mysql_query($sql_p) or dberror($sql_p);

		while ($row_p = mysql_fetch_assoc($res_p))
		{
			$pos_per_city2[$row_p["posaddress_store_postype"]] = $row_p["cnt"];
		}
	}
	else
	{
		$sql_p = "select posaddress_store_postype, count(posaddress_id) as cnt from posaddresses " . 
				 " where posaddress_place_id = " . dbquote($tmp_place_id) . 
				 " and (posaddress_store_closingdate is null  or posaddress_store_closingdate = '0000-00-00') " . 
			     " and posaddress_ownertype != 1 " .
				 " group by posaddress_store_postype";

		$res_p = mysql_query($sql_p) or dberror($sql_p);

		while ($row_p = mysql_fetch_assoc($res_p))
		{
			$pos_per_city2[$row_p["posaddress_store_postype"]] = $row_p["cnt"];
		}
	}




	//get all relevant ongoing projects with approved LN
	$relevant_projects = array();
	$relevant_projects2 = array();
	
	if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
	{
	
		$sql_p = "select order_shop_address_company, order_shop_address_place, " . 
				 "project_id, project_real_opening_date, projectkind_name, project_costtype_text, " .
			     "project_state, project_state_text, concat(postype_name, ' ', IFNULL(possubclass_name, '')) as postype " . 
				 "from orders " . 
				 "left join projects on project_order = order_id  " . 
				 "left join projectkinds on projectkind_id = project_projectkind  " .
				 "left join project_costs on project_cost_order = order_id  " .
			     "left join project_costtypes on project_costtype_id = project_cost_type  " .
			     "left join project_states on project_state_id = project_state " .
			     "left join postypes on postype_id = project_postype " .
			     "left join possubclasses on possubclass_id = project_pos_subclass " . 
				 "where order_type = 1  " . 
				 "and order_actual_order_state_code < 890  " .
				 " and project_projectkind in (1, 3, 4, 6, 9) " . 
				 " and  order_shop_address_country = " . $project["order_shop_address_country"] . 
				 " and (order_archive_date is NULL or order_archive_date = '0000-00-00') " . 
				 " and (project_actual_opening_date is NULL or project_actual_opening_date = '0000-00-00') " . 
				 " and project_cost_type in (1, 3, 5) " . 
				 " order by order_shop_address_place, order_shop_address_company ";

	}
	else
	{
		$sql_p = "select order_shop_address_company, order_shop_address_place, " . 
				 "project_id, project_real_opening_date, projectkind_name, project_costtype_text, " .
			     "project_state, project_state_text, concat(postype_name, ' ', IFNULL(possubclass_name, '')) as postype " .  
				 "from orders " . 
				 "left join projects on project_order = order_id  " . 
				 "left join projectkinds on projectkind_id = project_projectkind  " .
				 "left join project_costs on project_cost_order = order_id  " .
			     "left join project_costtypes on project_costtype_id = project_cost_type  " .
			     "left join project_states on project_state_id = project_state " . 
			     "left join postypes on postype_id = project_postype " .
			     "left join possubclasses on possubclass_id = project_pos_subclass " . 
				 "where order_type = 1  " . 
				 "and order_actual_order_state_code < 890  " .
				 " and project_projectkind in (1, 3, 4, 6, 9) " . 
				 " and  order_shop_address_country = " . $project["order_shop_address_country"] . 
				 " and (order_archive_date is NULL or order_archive_date = '0000-00-00') " . 
				 " and (project_actual_opening_date is NULL or project_actual_opening_date = '0000-00-00') " . 
				 " and project_cost_type in (2) " . 
				 " order by order_shop_address_place, order_shop_address_company ";

		
	}


	$res_p = mysql_query($sql_p) or dberror($sql_p);

	while ($row_p = mysql_fetch_assoc($res_p))
	{
		
		//check if ln was approved, if not check if cer was approved
		if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
		{
			$sql_m = "select project_milestone_project, project_milestone_date from project_milestones " . 
					 " where project_milestone_project = " . $row_p["project_id"] . 
					 " and project_milestone_milestone = 13 " . 
						 " and project_milestone_date is not null " . 
						 " and project_milestone_date <> '0000-00-00'";

			//echo $sql_m . "<br />";
			$res_m = mysql_query($sql_m) or dberror($sql_m);

			if ($row_m = mysql_fetch_assoc($res_m))
			{
				
				$row_p["project_milestone_date"] = $row_m["project_milestone_date"];
				$relevant_projects[$row_m["project_milestone_project"]] = $row_p;
				//echo "->added" . "<br />";
			}
			else
			{
				$sql_m = "select project_milestone_project, project_milestone_date from project_milestones " . 
						 " where project_milestone_project = " . $row_p["project_id"] . 
						 " and project_milestone_milestone in (12, 21) " . 
						 " and project_milestone_date is not null " . 
						 " and project_milestone_date <> '0000-00-00'";

				$res_m = mysql_query($sql_m) or dberror($sql_m);

				if ($row_m = mysql_fetch_assoc($res_m))
				{
					$row_p["project_milestone_date"] = $row_m["project_milestone_date"];
					$relevant_projects[$row_m["project_milestone_project"]] = $row_p;

					
					
				}
			}
		}
		else
		{
			$sql_m = "select project_milestone_project, project_milestone_date from project_milestones " . 
						 " where project_milestone_project = " . $row_p["project_id"] . 
						 " and project_milestone_milestone in (12, 10, 21) " . 
						 " and project_milestone_date is not null " . 
						 " and project_milestone_date <> '0000-00-00'";

			$res_m = mysql_query($sql_m) or dberror($sql_m);

			if ($row_m = mysql_fetch_assoc($res_m))
			{
				$row_p["project_milestone_date"] = $row_m["project_milestone_date"];
				$relevant_projects[$row_m["project_milestone_project"]] = $row_p;

				
				
			}
		}
	}


	if($form_type == "AF")
	{
		$sql_p = "select order_shop_address_company, order_shop_address_place, " . 
				 "project_id, project_real_opening_date, projectkind_name, project_costtype_text, " .
			     "project_state, project_state_text, concat(postype_name, ' ', IFNULL(possubclass_name, '')) as postype " .  
				 "from orders " . 
				 "left join projects on project_order = order_id  " . 
				 "left join projectkinds on projectkind_id = project_projectkind  " .
				 "left join project_costs on project_cost_order = order_id  " .
			     "left join project_costtypes on project_costtype_id = project_cost_type  " .
			      "left join project_states on project_state_id = project_state " .
			     "left join postypes on postype_id = project_postype " .
			     "left join possubclasses on possubclass_id = project_pos_subclass " .
				 "where order_type = 1  " . 
				 "and order_actual_order_state_code < 890  " .
				 " and project_projectkind in (1, 3, 4, 6, 9) " . 
				 " and  order_shop_address_country = " . $project["order_shop_address_country"] . 
				 " and (order_archive_date is NULL or order_archive_date = '0000-00-00') " . 
				 " and (project_actual_opening_date is NULL or project_actual_opening_date = '0000-00-00') " . 
				 " and project_cost_type in (1, 3, 5) " . 
				 " order by order_shop_address_place, order_shop_address_company ";

		$res_p = mysql_query($sql_p) or dberror($sql_p);

		while ($row_p = mysql_fetch_assoc($res_p))
		{
			
			//check if ln was approved, if not check if cer was approved
			$sql_m = "select project_milestone_project, project_milestone_date from project_milestones " . 
					 " where project_milestone_project = " . $row_p["project_id"] . 
					 " and project_milestone_milestone = 13 " . 
						 " and project_milestone_date is not null " . 
						 " and project_milestone_date <> '0000-00-00'";

			//echo $sql_m . "<br />";
			$res_m = mysql_query($sql_m) or dberror($sql_m);

			if ($row_m = mysql_fetch_assoc($res_m))
			{
				
				$row_p["project_milestone_date"] = $row_m["project_milestone_date"];
				$relevant_projects2[$row_m["project_milestone_project"]] = $row_p;
			}
			else
			{
				$sql_m = "select project_milestone_project, project_milestone_date from project_milestones " . 
						 " where project_milestone_project = " . $row_p["project_id"] . 
						 " and project_milestone_milestone in (12, 21) " . 
						 " and project_milestone_date is not null " . 
						 " and project_milestone_date <> '0000-00-00'";

				$res_m = mysql_query($sql_m) or dberror($sql_m);

				if ($row_m = mysql_fetch_assoc($res_m))
				{
					$row_p["project_milestone_date"] = $row_m["project_milestone_date"];
					$relevant_projects2[$row_m["project_milestone_project"]] = $row_p;

					
					
				}
			}
		}
	}
	else
	{
		$sql_p = "select order_shop_address_company, order_shop_address_place, " . 
				 "project_id, project_real_opening_date, projectkind_name, project_costtype_text, " .
			     "project_state, project_state_text, concat(postype_name, ' ', IFNULL(possubclass_name, '')) as postype " .  
				 "from orders " . 
				 "left join projects on project_order = order_id  " . 
				 "left join projectkinds on projectkind_id = project_projectkind  " .
				 "left join project_costs on project_cost_order = order_id  " .
			     "left join project_costtypes on project_costtype_id = project_cost_type  " .
			     "left join postypes on postype_id = project_postype " .
			     "left join possubclasses on possubclass_id = project_pos_subclass " .
			      "left join project_states on project_state_id = project_state " . 
				 "where order_type = 1  " . 
				 "and order_actual_order_state_code < 890  " .
				 " and project_projectkind in (1, 3, 4, 6, 9) " . 
				 " and  order_shop_address_country = " . $project["order_shop_address_country"] . 
				 " and (order_archive_date is NULL or order_archive_date = '0000-00-00') " . 
				 " and (project_actual_opening_date is NULL or project_actual_opening_date = '0000-00-00') " . 
				 " and project_cost_type in (2) " . 
				 " order by order_shop_address_place, order_shop_address_company ";


		$res_p = mysql_query($sql_p) or dberror($sql_p);

		while ($row_p = mysql_fetch_assoc($res_p))
		{
			
		$sql_m = "select project_milestone_project, project_milestone_date from project_milestones " . 
					 " where project_milestone_project = " . $row_p["project_id"] . 
					 " and project_milestone_milestone in (12, 21) " . 
					 " and project_milestone_date is not null " . 
					 " and project_milestone_date <> '0000-00-00'";

			$res_m = mysql_query($sql_m) or dberror($sql_m);

			if ($row_m = mysql_fetch_assoc($res_m))
			{
				$row_p["project_milestone_date"] = $row_m["project_milestone_date"];
				$relevant_projects2[$row_m["project_milestone_project"]] = $row_p;
			}
		}
	}


	ksort($relevant_projects);
	ksort($relevant_projects2);
	$distribution_summary = array();
	$distribution_summary['pos_per_country'] = $pos_per_country;
	$distribution_summary['pos_per_city'] = $pos_per_city;
	$distribution_summary['pos_per_country2'] = $pos_per_country2;
	$distribution_summary['pos_per_city2'] = $pos_per_city2;
	$distribution_summary['relevant_projects'] = $relevant_projects;
	$distribution_summary['relevant_projects2'] = $relevant_projects2;

	$tmp = serialize($distribution_summary);

	$sql_u = "update cer_basicdata set cer_basicdata_version_distribution_summary = " . dbquote($tmp) . 
		     " where cer_basicdata_project = " . $project["project_id"] . 
		     " and cer_basicdata_version = 0";

	$result = mysql_query($sql_u) or dberror($sql_u);
}
else
{
	if($cer_basicdata["cer_basicdata_version_distribution_summary"])
	{
		$distribution_summary = unserialize($cer_basicdata["cer_basicdata_version_distribution_summary"]);
		$pos_per_country = $distribution_summary['pos_per_country'];
		$pos_per_city = $distribution_summary['pos_per_city'];
		$relevant_projects = $distribution_summary['relevant_projects'];
		if(array_key_exists('relevant_projects2', $distribution_summary))
		{
			$relevant_projects2 = $distribution_summary['relevant_projects2'];
		}

		if(array_key_exists('pos_per_country2', $distribution_summary))
		{
			$pos_per_country2 = $distribution_summary['pos_per_country2'];
		}

		if(array_key_exists('pos_per_city2', $distribution_summary))
		{
			$pos_per_city2 = $distribution_summary['pos_per_city2'];
		}
	}
	else
	{
		$distribution_summary_available = false;
	}


}

if($distribution_summary_available == true)
{

	/********************************************************************
		print PDF
	*********************************************************************/
	//set pdf parameters

	$margin_top = 16;
	$margin_left = 12;
	$y = $margin_top;


	$pdf->AddPage("P", "A4");
	$pdf->setPrintFooter(false);
	$pdf->SetAutoPageBreak(false, 0);

	// Title first line
	$pdf->SetXY($margin_left,$margin_top);
	$pdf->SetFont("arialn", "I", 9);
	$pdf->Cell(45, 10, "", 1);

	$pdf->Image('../pictures/brand_logo.jpg',13,$margin_top + 1,22);

	$pdf->SetFont("arialn", "B", 11);
	if($form_type == "AF")
	{
		$pdf->Cell(122, 10, "Distribution Summary for INR-03", 1, "", "C");
	}
	elseif($form_type == "AF")
	{
		$pdf->Cell(122, 10, "Distribution Summary for LNR", 1, "", "C");
	}
	else
	{
		$pdf->Cell(122, 10, "Distribution Summary for LNR", 1, "", "C");
	}
	$pdf->Cell(20, 10, $submission_date, 1, "", "C");

	$pdf->SetXY($margin_left,$y+12);

	// print project and investment infos
	$y = $y+11;
	$x = $margin_left;

	$pdf->Cell(142, 10, $project_name, 0, "", "L");
		
		
		// draw first box
		$pdf->SetXY($margin_left,$y+4);

		// print project and investment infos
		$y = $y+11;
		$x = $margin_left;

		//POS locations
		$y = $y+7;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		
		
		if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
		{
			$y_tmp = $pdf->getY();
			$pdf->SetFillColor(90,149,54);
			$pdf->Cell(65, 5, "1. Operating Corporate POS Locations", 1, "", "L", 1);
			$pdf->SetFillColor(224,224,224);

			$y = $y+5;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("arialn", "B", 8);
			$pdf->Cell(20, 5, "POS Type", 1, "", "L");
			$pdf->Cell(25, 5, $project["country_name"], 1, "", "L");
			$pdf->Cell(20, 5, $project["order_shop_address_place"], 1, "", "L");


			$y = $y+5;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("arialn", "", 8);
			$pdf->Cell(20, 5, "Stores", 1, "", "L");
			$pdf->Cell(25, 5, $pos_per_country[1], 1, "", "L");
			$pdf->Cell(20, 5, $pos_per_city[1], 1, "", "L");


			$y = $y+5;
			$pdf->SetXY($x,$y);
			$pdf->Cell(20, 5, "Kiosks", 1, "", "L");
			$pdf->Cell(25, 5, $pos_per_country[3], 1, "", "L");
			$pdf->Cell(20, 5, $pos_per_city[3], 1, "", "L");

			$y = $y+5;
			$pdf->SetXY($x,$y);
			$pdf->Cell(20, 5, "SIS", 1, "", "L");
			$pdf->Cell(25, 5, $pos_per_country[2], 1, "", "L");
			$pdf->Cell(20, 5, $pos_per_city[2], 1, "", "L");

			
			$y = $y+5;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("arialn", "B", 8);
			$pdf->Cell(20, 5, "Total", 1, "", "L");
			$pdf->Cell(25, 5, $pos_per_country[1]+$pos_per_country[2]+$pos_per_country[3], 1, "", "L");
			$pdf->Cell(20, 5, $pos_per_city[1]+$pos_per_city[2]+$pos_per_city[3], 1, "", "L");


			if($show_wholesale_data_in_cer == true)
			{
				$y = $y_tmp;
				$pdf->SetXY($x + 70,$y);
				$pdf->SetFillColor(235,202,62);
				$pdf->Cell(75, 5, "2. Operating Wholesale POS Locations", 1, "", "L", 1);
				$pdf->SetFillColor(224,224,224);
				$y = $y+5;
				$pdf->SetXY($x + 70,$y);
				$pdf->SetFont("arialn", "B", 8);
				$pdf->Cell(30, 5, "POS Type", 1, "", "L");
				$pdf->Cell(25, 5, $project["country_name"], 1, "", "L");
				$pdf->Cell(20, 5, $project["order_shop_address_place"], 1, "", "L");


				$y = $y+5;
				$pdf->SetXY($x + 70,$y);
				$pdf->SetFont("arialn", "", 8);
				$pdf->Cell(30, 5, "Stores", 1, "", "L");
				$pdf->Cell(25, 5, $pos_per_country2[1], 1, "", "L");
				$pdf->Cell(20, 5, $pos_per_city2[1], 1, "", "L");


				$y = $y+5;
				$pdf->SetXY($x + 70,$y);
				$pdf->Cell(30, 5, "Kiosks", 1, "", "L");
				$pdf->Cell(25, 5, $pos_per_country2[3], 1, "", "L");
				$pdf->Cell(20, 5, $pos_per_city2[3], 1, "", "L");

				$y = $y+5;
				$pdf->SetXY($x + 70,$y);
				$pdf->Cell(30, 5, "SIS", 1, "", "L");
				$pdf->Cell(25, 5, $pos_per_country2[2], 1, "", "L");
				$pdf->Cell(20, 5, $pos_per_city2[2], 1, "", "L");

				$y = $y+5;
				$pdf->SetXY($x + 70,$y);
				$pdf->Cell(30, 5, "Independent Retailer", 1, "", "L");
				$pdf->Cell(25, 5, $pos_per_country2[4], 1, "", "L");
				$pdf->Cell(20, 5, $pos_per_city2[4], 1, "", "L");

				
				$y = $y+5;
				$pdf->SetXY($x + 70,$y);
				$pdf->SetFont("arialn", "B", 8);
				$pdf->Cell(30, 5, "Total", 1, "", "L");
				$pdf->Cell(25, 5, $pos_per_country2[1]+$pos_per_country2[2]+$pos_per_country2[3]+$pos_per_country2[4], 1, "", "L");
				$pdf->Cell(20, 5, $pos_per_city2[1]+$pos_per_city2[2]+$pos_per_city2[3]+$pos_per_city2[4], 1, "", "L");
			}
		}
		else
		{
			$y_tmp = $pdf->getY();
			$pdf->SetFillColor(235,202,62);
			$pdf->Cell(75, 5, "1. Operating Wholesale POS Locations", 1, "", "L", 1);
			$pdf->SetFillColor(224,224,224);
			$y = $y+5;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("arialn", "B", 8);
			$pdf->Cell(30, 5, "POS Type", 1, "", "L");
			$pdf->Cell(25, 5, $project["country_name"], 1, "", "L");
			$pdf->Cell(20, 5, $project["order_shop_address_place"], 1, "", "L");


			$y = $y+5;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("arialn", "", 8);
			$pdf->Cell(30, 5, "Stores", 1, "", "L");
			$pdf->Cell(25, 5, $pos_per_country[1], 1, "", "L");
			$pdf->Cell(20, 5, $pos_per_city[1], 1, "", "L");


			$y = $y+5;
			$pdf->SetXY($x,$y);
			$pdf->Cell(30, 5, "Kiosks", 1, "", "L");
			$pdf->Cell(25, 5, $pos_per_country[3], 1, "", "L");
			$pdf->Cell(20, 5, $pos_per_city[3], 1, "", "L");

			$y = $y+5;
			$pdf->SetXY($x,$y);
			$pdf->Cell(30, 5, "SIS", 1, "", "L");
			$pdf->Cell(25, 5, $pos_per_country[2], 1, "", "L");
			$pdf->Cell(20, 5, $pos_per_city[2], 1, "", "L");

			$y = $y+5;
			$pdf->SetXY($x,$y);
			$pdf->Cell(30, 5, "Independent Retailer", 1, "", "L");
			$pdf->Cell(25, 5, $pos_per_country[4], 1, "", "L");
			$pdf->Cell(20, 5, $pos_per_city[4], 1, "", "L");

			
			$y = $y+5;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("arialn", "B", 8);
			$pdf->Cell(30, 5, "Total", 1, "", "L");
			$pdf->Cell(25, 5, $pos_per_country[1]+$pos_per_country[2]+$pos_per_country[3]+$pos_per_country[4], 1, "", "L");
			$pdf->Cell(20, 5, $pos_per_city[1]+$pos_per_city[2]+$pos_per_city[3]+$pos_per_city[4], 1, "", "L");

			
			if($show_corporate_data_in_af == true)
			{
				$y = $y_tmp;
				$pdf->SetXY($x + 80,$y);
				$pdf->SetFillColor(90,149,54);
				$pdf->Cell(65, 5, "2. Operating Corporate POS Locations", 1, "", "L", 1);
				$pdf->SetFillColor(224,224,224);
				$y = $y+5;
				$pdf->SetXY($x + 80,$y);
				$pdf->SetFont("arialn", "B", 8);
				$pdf->Cell(20, 5, "POS Type", 1, "", "L");
				$pdf->Cell(25, 5, $project["country_name"], 1, "", "L");
				$pdf->Cell(20, 5, $project["order_shop_address_place"], 1, "", "L");


				$y = $y+5;
				$pdf->SetXY($x + 80,$y);
				$pdf->SetFont("arialn", "", 8);
				$pdf->Cell(20, 5, "Stores", 1, "", "L");
				$pdf->Cell(25, 5, $pos_per_country2[1], 1, "", "L");
				$pdf->Cell(20, 5, $pos_per_city2[1], 1, "", "L");


				$y = $y+5;
				$pdf->SetXY($x + 80,$y);
				$pdf->Cell(20, 5, "Kiosks", 1, "", "L");
				$pdf->Cell(25, 5, $pos_per_country2[3], 1, "", "L");
				$pdf->Cell(20, 5, $pos_per_city2[3], 1, "", "L");

				$y = $y+5;
				$pdf->SetXY($x + 80,$y);
				$pdf->Cell(20, 5, "SIS", 1, "", "L");
				$pdf->Cell(25, 5, $pos_per_country2[2], 1, "", "L");
				$pdf->Cell(20, 5, $pos_per_city2[2], 1, "", "L");

				
				$y = $y+5;
				$pdf->SetXY($x + 80,$y);
				$pdf->SetFont("arialn", "B", 8);
				$pdf->Cell(20, 5, "Total", 1, "", "L");
				$pdf->Cell(25, 5, $pos_per_country2[1]+$pos_per_country2[2]+$pos_per_country2[3], 1, "", "L");
				$pdf->Cell(20, 5, $pos_per_city2[1]+$pos_per_city2[2]+$pos_per_city2[3], 1, "", "L");

				$y = $y+5;
			}
		}

		



		//POS locations
		$y = $y+9;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 8);
		
		if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
		{
			$pdf->SetFillColor(90,149,54);

			if($show_wholesale_data_in_cer == true)
			{
				$pdf->SetFillColor(90,149,54);
				$pdf->Cell(187, 5, "3. Approved Corporate Projects as per " . $submission_date, 1, "", "L", 1);
				$pdf->SetFillColor(224,224,224);
			}
			else
			{
				$pdf->SetFillColor(90,149,54);
				$pdf->Cell(187, 5, "2. Approved Corporate Projects as per " . $submission_date, 1, "", "L", 1);
			}
			$pdf->SetFillColor(224,224,224);

		}
		else
		{
			$pdf->SetFillColor(235,202,62);
			if($show_corporate_data_in_af == true)
			{
				$pdf->Cell(187, 5, "3. Approved Wholesale Projects as per " . $submission_date, 1, "", "L", 1);
				
			}
			else
			{
				$pdf->Cell(187, 5, "2. Approved Wholesale Projects as per " . $submission_date, 1, "", "L", 1);
			}
			$pdf->SetFillColor(224,224,224);
		}


		
		$y = $y+5;
		$pdf->SetXY($x,$y);
		
		$pdf->Cell(62, 5, "POS Name", 1, "", "L");
		$pdf->Cell(35, 5, "City", 1, "", "L");
		$pdf->Cell(25, 5, "Project Type", 1, "", "L");
		$pdf->Cell(25, 5, "POS Type", 1, "", "L");
		$pdf->Cell(20, 5, "Agreed Opening", 1, "", "L");
		

		if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
		{
			$pdf->Cell(20, 5, "LNR Approval", 1, "", "L");
		}
		else
		{
			$pdf->Cell(20, 5, "LNR Approval", 1, "", "L");
		}


		$pdf->SetFont("arialn", "", 8);
		
		$tmp_year = 0;
		$tmp_first_group = true;
		foreach($relevant_projects as $key=>$relevant_project)
		{
			
			if($tmp_year != substr($relevant_project["project_milestone_date"], 0, 4))
			{
				if($tmp_first_group == false)
				{
					$y = $y+5;
				}
			}

			$y = $y+5;
			$pdf->SetXY($x,$y);
			
			$pdf->Cell(62, 5, $relevant_project["order_shop_address_company"], 1, "", "L");
			$pdf->Cell(35, 5, $relevant_project["order_shop_address_place"], 1, "", "L");
			$pdf->Cell(25, 5, $relevant_project["projectkind_name"], 1, "", "L");
			$pdf->Cell(25, 5, $relevant_project["postype"], 1, "", "L");
			
			if($relevant_project["project_state"] == 2)
			{
				$pdf->SetTextColor(255, 0, 0);
				$pdf->Cell(20, 5, $relevant_project["project_state_text"], 1, "", "L");
				$pdf->SetTextColor(0, 0, 0);
			}
			else
			{
				$pdf->Cell(20, 5, to_system_date($relevant_project["project_real_opening_date"]), 1, "", "L");
			}
			$pdf->Cell(20, 5, to_system_date($relevant_project["project_milestone_date"]), 1, "", "L");
			
				
			$tmp_year = substr($relevant_project["project_milestone_date"], 0, 4);
			$tmp_first_group = false;

		}



		//POS locations
		if($form_type == "AF" and count($relevant_projects2) > 0)
		{
			$y = $y+9;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("arialn", "B", 8);
			
			$pdf->SetFillColor(90,149,54);
			if($show_corporate_data_in_af == true)
			{
				$pdf->Cell(187, 5, "4. Approved Corporate Projects as per " . $submission_date, 1, "", "L", 1);
			}
			else
			{
				$pdf->Cell(187, 5, "4. Approved Corporate Projects as per " . $submission_date, 1, "", "L", 1);
			}
			$pdf->SetFillColor(224,224,224);
			
			$y = $y+5;
			$pdf->SetXY($x,$y);
			
			$pdf->Cell(62, 5, "POS Name", 1, "", "L");
			$pdf->Cell(35, 5, "City", 1, "", "L");
			$pdf->Cell(25, 5, "Project Type", 1, "", "L");
			$pdf->Cell(25, 5, "POS Type", 1, "", "L");
			$pdf->Cell(20, 5, "Agreed Opening", 1, "", "L");
			$pdf->Cell(20, 5, "LNR Approval", 1, "", "L");
			
			$pdf->SetFont("arialn", "", 8);
			
			$tmp_year = 0;
			$tmp_first_group = true;
			foreach($relevant_projects2 as $key=>$relevant_project)
			{
				
				if($tmp_year != substr($relevant_project["project_milestone_date"], 0, 4))
				{
					if($tmp_first_group == false)
					{
						$y = $y+5;
					}
				}

				$y = $y+5;
				$pdf->SetXY($x,$y);
				
				$pdf->Cell(62, 5, $relevant_project["order_shop_address_company"], 1, "", "L");
				$pdf->Cell(35, 5, $relevant_project["order_shop_address_place"], 1, "", "L");
				$pdf->Cell(25, 5, $relevant_project["projectkind_name"], 1, "", "L");
				$pdf->Cell(25, 5, $relevant_project["postype"], 1, "", "L");
				if($relevant_project["project_state"] == 2)
				{
					$pdf->SetTextColor(255, 0, 0);
					$pdf->Cell(20, 5, $relevant_project["project_state_text"], 1, "", "L");
					$pdf->SetTextColor(0, 0, 0);
				}
				else
				{
					$pdf->Cell(20, 5, to_system_date($relevant_project["project_real_opening_date"]), 1, "", "L");
				}
				$pdf->Cell(20, 5, to_system_date($relevant_project["project_milestone_date"]), 1, "", "L");
				
					
				$tmp_year = substr($relevant_project["project_milestone_date"], 0, 4);
				$tmp_first_group = false;

			}
		}
		elseif(count($relevant_projects2) > 0)
		{
			$y = $y+9;
			$pdf->SetXY($x,$y);
			$pdf->SetFont("arialn", "B", 8);
			
			$pdf->SetFillColor(235,202,62);
			if($show_corporate_data_in_af == true)
			{
				$pdf->Cell(187, 5, "4. Approved Wholesale Projects as per " . $submission_date, 1, "", "L", 1);
			}
			else
			{
				$pdf->Cell(187, 5, "3. Approved Wholesale Projects as per " . $submission_date, 1, "", "L", 1);
			}
			$pdf->SetFillColor(224,224,224);
			
			$y = $y+5;
			$pdf->SetXY($x,$y);
			
			$pdf->Cell(62, 5, "POS Name", 1, "", "L");
			$pdf->Cell(35, 5, "City", 1, "", "L");
			$pdf->Cell(25, 5, "Project Type", 1, "", "L");
			$pdf->Cell(25, 5, "POS Type", 1, "", "L");
			$pdf->Cell(20, 5, "Agreed Opening", 1, "", "L");
			$pdf->Cell(20, 5, "LNR Approval", 1, "", "L");
			
			$pdf->SetFont("arialn", "", 8);
			
			$tmp_year = 0;
			$tmp_first_group = true;
			foreach($relevant_projects2 as $key=>$relevant_project)
			{
				
				if($tmp_year != substr($relevant_project["project_milestone_date"], 0, 4))
				{
					if($tmp_first_group == false)
					{
						$y = $y+5;
					}
				}

				$y = $y+5;
				$pdf->SetXY($x,$y);
				
				$pdf->Cell(62, 5, $relevant_project["order_shop_address_company"], 1, "", "L");
				$pdf->Cell(35, 5, $relevant_project["order_shop_address_place"], 1, "", "L");
				$pdf->Cell(25, 5, $relevant_project["projectkind_name"], 1, "", "L");
				$pdf->Cell(25, 5, $relevant_project["postype"], 1, "", "L");
				if($relevant_project["project_state"] == 2)
				{
					$pdf->SetTextColor(255, 0, 0);
					$pdf->Cell(20, 5, $relevant_project["project_state_text"], 1, "", "L");
					$pdf->SetTextColor(0, 0, 0);
				}
				else
				{
					$pdf->Cell(20, 5, to_system_date($relevant_project["project_real_opening_date"]), 1, "", "L");
				}
				$pdf->Cell(20, 5, to_system_date($relevant_project["project_milestone_date"]), 1, "", "L");
				
					
				$tmp_year = substr($relevant_project["project_milestone_date"], 0, 4);
				$tmp_first_group = false;

			}
		}
}

?>