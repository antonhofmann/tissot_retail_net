<?php
/********************************************************************

    cer_application_lease.php

    Application Form: lease information
    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-08-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-08-14
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require "include/get_project.php";
require_once "../shared/func_posindex.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/
$client_address = get_address($project["order_client_address"]);
$posdata = get_pos_data($project["project_order"]);

$currency = get_cer_currency(param("pid"));
$posleases = get_pos_leasedata($posdata["posaddress_id"], $project["project_order"]);


//build missing investment records
$result = build_missing_cer_investment_records(param("pid"));


$apply_new_version_of_rental_tool = 0;

if(($ln_basicdata["ln_basicdata_submitted"] == NULL 
   or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == '0000')
   and ($cer_basicdata["cer_basicdata_submitted"] == NULL 
   or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == '0000'))
{
	$apply_new_version_of_rental_tool = 1;
}
elseif($ln_basicdata["ln_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014
	and ($cer_basicdata["cer_basicdata_submitted"] == NULL 
	or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == '0000'))
{
	$apply_new_version_of_rental_tool = 1;
}
elseif(($ln_basicdata["ln_basicdata_submitted"] == NULL 
   or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == '0000')
   and $cer_basicdata["cer_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014)
{
	$apply_new_version_of_rental_tool = 1;
}
elseif($ln_basicdata["ln_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014
       and $cer_basicdata["cer_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014)
{
	$apply_new_version_of_rental_tool = 1;
}


$yes_no = array();
$yes_no[0] = "No";
$yes_no[1] = "Yes";


//calculate annual rent

$average_annual_rent = "";
$rental_duration = "";

$old_lease_start_date = '';
$old_lease_end_date = '';

if(count($posleases) > 0)
{
	if($posleases["poslease_startdate"] != NULL 
		and $posleases["poslease_startdate"] != '0000-00-00' 
		and $posleases["poslease_enddate"] != NULL
		and $posleases["poslease_enddate"] != '0000-00-00')
	{
		$diff = abs(strtotime($posleases["poslease_enddate"]) - strtotime($posleases["poslease_startdate"]));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

		if($months == 12)
		{
			$months = 0;
			$years++;
		}

		$rental_duration = $years . " years and " . $months . " months";

		$duration_in_years = $years + ($months/12);
	}

	$old_lease_start_date = $posleases["poslease_startdate"];
	$old_lease_end_date = $posleases["poslease_enddate"];
	
}

$sql_pos = "select posaddress_id, " . 
           " concat(posaddress_place, ', ', posaddress_name, ' - ', product_line_name) as posname " . 
           "from posaddresses " .
		   "left join countries on country_id = posaddress_country " .
		   "left join product_lines on product_line_id = posaddress_store_furniture " . 
		   "where posaddress_country = " . $client_address["country"] . 
		   " and posaddress_store_postype = " . $project["project_postype"] .
		   " order by posaddress_place, posaddress_name";

//check if business plan can be edited
require_once("include/check_if_business_plan_is_editable.php");



// get date of LN Approval
$ln_was_approved = false;
$sql = "select project_milestone_date from project_milestones " . 
       "where project_milestone_milestone = 13 " . 
	   " and project_milestone_project = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res))
{
	
	if($row["project_milestone_date"] != NULL and $row["project_milestone_date"] != '0000-00-00')
	{
		$ln_was_approved = true;
	}
}





/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

include("include/project_head.php");

$form->add_hidden("pid", param("pid"));



if(($can_edit_business_plan == true and $project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer") and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") and $project["project_state"] != 2))
{
	
	$form->add_section("Benchmark Info");
	$form->add_comment("You might enter a POS which presents similarities in term of investments, revenues, costs,and/or location, etc. <br />
You can also compare the business plan of your project with the one of another POS.
This will help you selecting the POS.");
	$form->add_list("posaddress_best_benchmark_pos", "Similar POS Location", $sql_pos, 0, $posdata["posaddress_best_benchmark_pos"]);
	


	$form->add_section("Lease Details");

	if(count($posleases) > 0)
	{
		$form->add_comment("There is a contract in the system for this POS location as follows:");
		$form->add_list("poslease_lease_type", "Lease Type*",
				"select poslease_type_id, poslease_type_name from poslease_types order by poslease_type_id", NOTNULL, $posleases["poslease_lease_type"]);
		$form->add_edit("poslease_startdate", "Start Date*", NOTNULL, to_system_date($posleases["poslease_startdate"]), TYPE_DATE);
		$form->add_edit("poslease_enddate", "Expiry Date*", NOTNULL, to_system_date($posleases["poslease_enddate"]), TYPE_DATE);
		
		
		
		$form->add_edit("poslease_signature_date", "Lease Contract Signed on", 0, to_system_date($posleases["poslease_signature_date"]), TYPE_DATE, "", "", 1, "poslease_signature_info");
		
		
		
		$form->add_label("rental_duration", "Rental Duration", 0, $rental_duration);
		$form->add_list("poslease_hasfixrent", "Contract contains fixed rent*", $yes_no, 0, $posleases["poslease_hasfixrent"]);
		$form->add_checkbox("poslease_isindexed", "contains tacit renewal clause", $posleases["poslease_isindexed"], "", "Contract");
		
		$form->add_edit("poslease_tacit_renewal_duration_years", "Tacit renewal duration years", 0, $posleases["poslease_tacit_renewal_duration_years"], TYPE_INT, 4);

		$form->add_edit("poslease_tacit_renewal_duration_months", "Tacit renewal duration months", 0, $posleases["poslease_tacit_renewal_duration_months"], TYPE_INT, 4);



		$form->add_checkbox("poslease_indexclause_in_contract", "Index Clause in Contract", $posleases["poslease_indexclause_in_contract"], "", "Index");

		if($apply_new_version_of_rental_tool == 1)
		{
			$form->add_label("poslease_indexrate", "Average Index Rate in %", 0, $posleases["poslease_indexrate"]);
			$form->add_label("poslease_average_increase", "Average yearly Increase in %", 0, $posleases["poslease_average_increase"]);
		}
		else
		{
			$form->add_edit("poslease_indexrate", "Average Index Rate in %", 0, $posleases["poslease_indexrate"], TYPE_DECIMAL, 6, 2);
			$form->add_edit("poslease_average_increase", "Average yearly Increase in %", 0, $posleases["poslease_average_increase"], TYPE_DECIMAL, 12, 2);
		}

		


		$form->add_section("Lease Options");
		
		$form->add_edit("poslease_extensionoption", "Extension Option to Date", 0, to_system_date($posleases["poslease_extensionoption"]), TYPE_DATE, "", "", 1, "extenstion_option_info");
		$form->add_edit("poslease_exitoption", "Exit Option to Date", 0, to_system_date($posleases["poslease_exitoption"]), TYPE_DATE, "", "", 1, "exit_option_info");
		$form->add_edit("poslease_termination_time", "Termination deadline*", NOTNULL, $posleases["poslease_termination_time"], TYPE_INT, 8, 0, 1, "termination");
		
		if($project["project_projectkind"] != 2 and $project["project_projectkind"] !=7)
		{
			$form->add_edit("cer_basicdata_deadline_property", "Deadline for property*", NOTNULL, to_system_date($cer_basicdata["cer_basicdata_deadline_property"]), TYPE_DATE, "", "", 1, "deadline_for_property_info");
		}
		else
		{
			$form->add_edit("cer_basicdata_deadline_property", "Deadline for property", 0, to_system_date($cer_basicdata["cer_basicdata_deadline_property"]), TYPE_DATE, "", "", 1, "deadline_for_property_info");
		}

		$form->add_edit("poslease_handoverdate", "Handover Date (Key)", 0, to_system_date($posleases["poslease_handoverdate"]), TYPE_DATE);
		
		if($project["project_projectkind"] == 1) // new project
		{
			$form->add_edit("poslease_firstrentpayed", "First Rent Paid as from*", NOTNULL, to_system_date($posleases["poslease_firstrentpayed"]), TYPE_DATE);
		}
		else
		{
			$form->add_edit("poslease_firstrentpayed", "First Rent Paid as from", 0, to_system_date($posleases["poslease_firstrentpayed"]), TYPE_DATE);
		}
		

		$form->add_edit("poslease_freeweeks", "Rent Free Period in Weeks*", 0, $posleases["poslease_freeweeks"], TYPE_INT, 6);
		

		/*
		$form->add_section("Deposit");
		$form->add_comment("In case you have a deposit then please indicate the number of months your deposit will be blocked.");
		
		$form->add_edit("poslease_deposit_duration", "Duration in months", 0, $posleases["poslease_deposit_duration"], TYPE_INT, 5);
		*/

		$form->add_hidden("poslease_deposit_duration");

		$form->add_section("Negotiation Details");
		$form->add_edit("poslease_negotiator", "Negotiator of Key Money and Lease*", NOTNULL, $posleases["poslease_negotiator"]);
		$form->add_multiline("poslease_landlord_name", "Landlord (Negotiation Partner)*", 4, NOTNULL, $posleases["poslease_landlord_name"]);
		
		$form->add_comment("Please indicate detailed conditions as stipulated in your contract for fixed rent - per measurement unit (m2/sqft) <br />per month or year or, if not specified, total amount.<br />
As well mention turnover percentages, additional rental cost, index rate, yearly increases.
");
		$form->add_multiline("poslease_negotiated_conditions", "Conditions for application of variable rent*", 8, NOTNULL, $posleases["poslease_negotiated_conditions"], 1, "condition_info");
	}
	else
	{
		$form->add_list("poslease_lease_type", "Lease Type*",
				"select poslease_type_id, poslease_type_name from poslease_types order by poslease_type_id", NOTNULL);
		
		$form->add_edit("poslease_startdate", "Start Date*", NOTNULL, "", TYPE_DATE);
		$form->add_edit("poslease_enddate", "Expiry Date*", NOTNULL, "", TYPE_DATE);
		
		$form->add_edit("poslease_signature_date", "Lease Contract Signed on", 0, "", TYPE_DATE, "", "", 1, "poslease_signature_info");
	
		
		
		$form->add_list("poslease_hasfixrent", "Contract contains fixed rent*", $yes_no, 0, 1);
		$form->add_checkbox("poslease_isindexed", "contains tacit renewal clause", "", "", "Contract");

		$form->add_edit("poslease_tacit_renewal_duration_years", "Tacit renewal duration years", 0, "", TYPE_INT, 4);

		$form->add_edit("poslease_tacit_renewal_duration_months", "Tacit renewal duration months", 0, "", TYPE_INT, 4);


		$form->add_checkbox("poslease_indexclause_in_contract", "Index Clause in Contract", "", "", "Index");
		
		if($apply_new_version_of_rental_tool == 1)
		{
			$form->add_label("poslease_indexrate", "Average Index Rate in %");
			$form->add_label("poslease_average_increase", "Average yearly Increase in %");
		}
		else
		{
			$form->add_edit("poslease_indexrate", "Average Index Rate in %", 0, "", TYPE_DECIMAL, 6, 2);
			$form->add_edit("poslease_average_increase", "Average yearly Increase in %", 0, "", TYPE_DECIMAL, 12, 2);
		}
		

		
		$form->add_section("Lease Options");
		$form->add_edit("poslease_extensionoption", "Extension Option to Date", 0, "", TYPE_DATE, "", "", 1, "extenstion_option_info");
		$form->add_edit("poslease_exitoption", "Exit Option to Date", 0, "", TYPE_DATE, "", "", 1, "exit_option_info");
		$form->add_edit("poslease_termination_time", "Termination deadline*", NOTNULL, "", TYPE_INT, 10, 0, 1, "termination");

		
		if($project["project_projectkind"] != 2 and $project["project_projectkind"] !=7)
		{
			$form->add_edit("cer_basicdata_deadline_property", "Deadline for property*", NOTNULL, "", TYPE_DATE, "", "", 1, "deadline_for_property_info");
		}
		else
		{
			$form->add_edit("cer_basicdata_deadline_property", "Deadline for property", 0, "", TYPE_DATE, "", "", 1, "deadline_for_property_info");
		}

		$form->add_edit("poslease_handoverdate", "Handover Date (Key)", 0, "", TYPE_DATE);
		

		if($project["project_projectkind"] == 1
			or $project["project_projectkind"] == 6
			or $project["project_projectkind"] == 9) // new project
		{
			$form->add_edit("poslease_firstrentpayed", "First Rent Paid as from*", NOTNULL, "", TYPE_DATE);
		}
		else
		{
			$form->add_edit("poslease_firstrentpayed", "First Rent Paid as from", 0, "", TYPE_DATE);
		}
		$form->add_edit("poslease_freeweeks", "Rent Free Period in Weeks*", 0, "", TYPE_INT, 6);

		/*
		$form->add_section("Deposit");
		$form->add_comment("In case you have a deposit then please indicate the number of months your deposit will be blocked.");
		
		$form->add_edit("poslease_deposit_duration", "Duration in months", 0, $posleases["poslease_deposit_duration"], TYPE_INT, 5);
		*/
		$form->add_hidden("poslease_deposit_duration");
				
		$form->add_section("Negotiation Details");
		$form->add_edit("poslease_negotiator", "Negotiator of Key Money and Lease*", NOTNULL, "");
		$form->add_multiline("poslease_landlord_name", "Landlord (Negotiation Partner)*", 4, NOTNULL, "");

		$form->add_comment("Please indicate detailed conditions as stipulated in your contract for fixed rent - per measurement unit (m2/sqft) <br />per month or year or, if not specified, total amount.<br />
As well mention turnover percentages, additional rental cost, index rate, yearly increases.
");
		$form->add_multiline("poslease_negotiated_conditions", "Conditions for the application of variable rent*", 8, NOTNULL, "", 1, "condition_info");

		

	}

	$form->add_button("form_save", "Save Data");
}
else
{
	$form->add_section("Benchmark Info");

	$pos_name = "concat(posaddress_name, ', ', posaddress_place)";
	
	if($posdata["posaddress_best_benchmark_pos"] > 0)
	{
		$form->add_lookup("similar_pos", "Similar POS Location", "posaddresses", $pos_name, 0, $posdata["posaddress_best_benchmark_pos"]);
	}
	else
	{
		$form->add_label("similar_pos", "Similar POS Location");
	}


	$form->add_section("Lease Details");


	if(count($posleases) > 0)
	{
		
		$rental_duration = "";
		if($posleases["poslease_startdate"] != NULL 
			and $posleases["poslease_startdate"] != '0000-00-00' 
			and $posleases["poslease_enddate"] != NULL
			and $posleases["poslease_enddate"] != '0000-00-00')
		{
			$diff = abs(strtotime($posleases["poslease_enddate"]) - strtotime($posleases["poslease_startdate"]));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

			if($months == 12)
			{
				$months = 0;
				$years++;
			}

			$rental_duration = $years . " years and " . $months . " months";


		}
		
		

		$form->add_comment("There is a contract in the system for this POS location as follows:");
		$form->add_lookup("poslease_lease_type", "Lease Type", "poslease_types", "poslease_type_name", 0, $posleases["poslease_lease_type"]);
		
		$form->add_label("poslease_startdate", "Start Date", 0, to_system_date($posleases["poslease_startdate"]));
		$form->add_label("poslease_enddate", "Expiry Date", 0, to_system_date($posleases["poslease_enddate"]));
		
		
		$form->add_edit("poslease_signature_date", "Lease Contract Signed on", 0, to_system_date($posleases["poslease_signature_date"]), TYPE_DATE, "", "", 1, "poslease_signature_info");

		//$form->add_label("poslease_signature_date", "Lease Contract Signed on", 0, to_system_date($posleases["poslease_signature_date"]));
		
		
		$form->add_label("rental_duration", "Rental Duration", 0, $rental_duration);
		
		$form->add_label("poslease_hasfixrent", "Contract contains fixed rent", $yes_no[$posleases["poslease_hasfixrent"]]);

		$form->add_checkbox("poslease_isindexed", "contains tacit renewal clause", $posleases["poslease_isindexed"], "", "Contract");

		$form->add_edit("poslease_tacit_renewal_duration_years", "Tacit renewal duration years", 0, $posleases["poslease_tacit_renewal_duration_years"], TYPE_INT, 4);

		$form->add_edit("poslease_tacit_renewal_duration_months", "Tacit renewal duration months", 0, $posleases["poslease_tacit_renewal_duration_months"], TYPE_INT, 4);

		$form->add_checkbox("poslease_indexclause_in_contract", "Index Clause in Contract", $posleases["poslease_indexclause_in_contract"], "", "Index");

		$form->add_label("poslease_indexrate", "Average Index Rate in %", 0, $posleases["poslease_indexrate"]);
		$form->add_label("poslease_average_increase", "Average yearly Increase in %", 0, $posleases["poslease_average_increase"]);
		
		
		
		
		$form->add_section("Lease Options");
		
		$form->add_label("poslease_extensionoption", "Extension Option to Date", 0, to_system_date($posleases["poslease_extensionoption"]));
		$form->add_label("poslease_exitoption", "Exit Option to Date", 0, to_system_date($posleases["poslease_exitoption"]));
		$form->add_label("poslease_termination_time", "Terminationdeadline", 0, $posleases["poslease_termination_time"]);

		
		$form->add_label("cer_basicdata_deadline_property", "Deadline for property", 0, to_system_date($cer_basicdata["cer_basicdata_deadline_property"]), TYPE_DATE);

		$form->add_label("poslease_handoverdate", "Handover Date (Key)", 0, to_system_date($posleases["poslease_handoverdate"]));
		$form->add_label("poslease_firstrentpayed", "First Rent Paid as from", 0, to_system_date($posleases["poslease_firstrentpayed"]));

		$form->add_label("poslease_freeweeks", "Rent Free Period in Weeks", 0, $posleases["poslease_freeweeks"]);
		
				
		$form->add_section("Negotiation Details");
		$form->add_label("poslease_negotiator", "Negotiator of Key Money and Lease", 0, $posleases["poslease_negotiator"]);
		$form->add_label("poslease_landlord_name", "Landlord (Negotiation Partner)", 0, $posleases["poslease_landlord_name"]);
		$form->add_label("poslease_negotiated_conditions", "Conditions for the application of variable rent", 0, $posleases["poslease_negotiated_conditions"], 1, "condition_info");


		/*
		$form->add_section("Deposit");
		$form->add_label("poslease_deposit_duration", "Duration in months", 0, $posleases["poslease_deposit_duration"]);
		*/
		$form->add_hidden("poslease_deposit_duration");
		
		$form->add_button("save_signed_date", "Save");
		
	}
	else
	{
				
		$form->add_label("poslease_lease_type", "Lease Type");
		$form->add_label("poslease_startdate", "Start Date");
		$form->add_label("poslease_enddate", "Expiry Date");
		$form->add_label("poslease_signature_date", "Lease Contract Signed on");
		
		$form->add_label("poslease_hasfixrent", "Contract contains fixed rent", "");
		$form->add_checkbox("poslease_isindexed", "contains tacit renewal clause", "", "", "Contract");

		$form->add_label("poslease_tacit_renewal_duration_years", "Tacit renewal duration years");
		$form->add_label("poslease_tacit_renewal_duration_months", "Tacit renewal duration months");

		$form->add_checkbox("poslease_indexclause_in_contract", "Index Clause in Contract", "", "", "Index");
		$form->add_label("poslease_indexrate", "Average Index Rate in %", 0, "");
		$form->add_label("poslease_average_increase", "Average yearly Increase in %", 0);
		
		
		
		$form->add_section("Lease Options");
		$form->add_label("poslease_extensionoption", "Extension Option to Date");
		$form->add_label("poslease_exitoption", "Exit Option to Date");
		$form->add_label("poslease_termination_time", "Termination deadline");

		$form->add_label("cer_basicdata_deadline_property", "Deadline for property");

		$form->add_label("poslease_handoverdate", "Handover Date (Key)", 0, "");
		$form->add_label("poslease_firstrentpayed", "First Rent Paid as from", 0, "");

		$form->add_label("poslease_freeweeks", "Rent Free Period in Weeks", 0, "");
		

		/*
		$form->add_section("Deposit");
		$form->add_label("poslease_deposit_duration", "Duration in months", 0, $posleases["poslease_deposit_duration"]);
		*/
		$form->add_hidden("poslease_deposit_duration");


		$form->add_section("Negotiation Details");
		$form->add_label("poslease_negotiator", "Negotiator of Key Money and Lease");
		$form->add_label("poslease_landlord_name", "Landlord (Negotiation Partner)");
		$form->add_label("poslease_negotiated_conditions", "Conditions for the application of variable rent");
	}
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if($form->button("save_signed_date"))
{
	$fields = array();
		
	

	$value = dbquote(from_system_date($form->value("poslease_signature_date")));
	$fields[] = "poslease_signature_date = " . $value;

		
	$value1 = "current_timestamp";
	$fields[] = "date_modified = " . $value1;

	if (isset($_SESSION["user_login"]))
	{
		$value1 = $_SESSION["user_login"];
		$fields[] = "user_modified = " . dbquote($value1);
	}

	$sql = "update " . $posleases["table"] . " set " . join(", ", $fields) . " where poslease_id = " . $posleases["poslease_id"];
	mysql_query($sql) or dberror($sql);
}
elseif($form->button("form_save"))
{
	
	/*
	if($project["project_cost_type"] == 1)
	{
		$form->add_validation("from_system_date({poslease_signature_date}) <=  " . dbquote(date("Y-m-d")), "The dat of contract's signature must not be a future date!");
	}
	*/

	$form->add_validation("from_system_date({poslease_signature_date}) <=  " . dbquote(date("Y-m-d")), "The date of contract's signature must not be a future date!");
	

	$diff = strtotime(from_system_date($form->value("poslease_firstrentpayed")), 0) - strtotime(from_system_date($form->value("poslease_handoverdate")), 0);
	$number_of_rent_free_weeks =  floor($diff / 604800);



	if($form->value("poslease_hasfixrent") == '')
	{
		$form->error("Please indicate if the contract includes fixed rents.");
	}
	elseif($form->value("poslease_extensionoption") 
		  and $form->value("poslease_enddate") 
		  and from_system_date($form->value("poslease_extensionoption")) <= from_system_date($form->value("poslease_enddate"))
	)
	{
		$form->error("Extension option to date must be a date in the future of the expiry date.");
	}
	elseif($form->value("poslease_exitoption") 
		  and $form->value("poslease_enddate") 
		  and from_system_date($form->value("poslease_exitoption")) > from_system_date($form->value("poslease_enddate"))
	)
	{
		$form->error("Exit option to date must be a date before of the expiry date.");
	}
	elseif($form->value("poslease_handoverdate") and $form->value("poslease_firstrentpayed") and from_system_date($form->value("poslease_handoverdate")) < from_system_date($form->value("poslease_firstrentpayed"))
		   and $form->value("poslease_freeweeks") == '' and $form->value("poslease_freeweeks") != 0)
	{
		$form->error("Please indicate rent free period in weeks!");
	}
	elseif($form->value("poslease_freeweeks") and !$form->value("poslease_handoverdate"))
	{
		$form->error("Please indicate the handover date of the key!");
	}
	elseif($form->value("poslease_freeweeks") and !$form->value("poslease_firstrentpayed"))
	{
		$form->error("Please indicate the date of the first rental pyment!");
	}
	elseif($form->value("poslease_firstrentpayed") and $form->value("poslease_handoverdate") and $number_of_rent_free_weeks != $form->value("poslease_freeweeks"))
	{
		$form->error("The rent free period in weeks is not correct. We propose " . $number_of_rent_free_weeks . " weeks.");
	}
	elseif(strlen($form->value("poslease_negotiated_conditions")) < 10)
	{
		$form->error("It seems you have not properly entered your conditions for application of variable rent, please read explanations & infobutton and revise accordingly.");
	}
	elseif($form->value("poslease_isindexed") 
		and !$form->value("poslease_tacit_renewal_duration_years")
		and !$form->value("poslease_tacit_renewal_duration_months"))
	{
		$form->error("Tacit renewal: Please indicate the tacit renewal duration in years and months!");
	}
	elseif(!$form->value("poslease_isindexed") 
		and ($form->value("poslease_tacit_renewal_duration_years")
		or $form->value("poslease_tacit_renewal_duration_months")))
	{
		$form->error("Tacit renewal: Please check the checkbox 'contains tacit renewal clause' or remove the duration information!");
	}
	elseif($form->validate())
	{

		// save pos data
		$fields = array();
    
		$value = dbquote($form->value("posaddress_best_benchmark_pos"));
		$fields[] = "posaddress_best_benchmark_pos = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update " . $posdata["table"] . " set " . join(", ", $fields) . " where posaddress_id = " . $posdata["posaddress_id"];
		mysql_query($sql) or dberror($sql);

		//save data to posleases
		
		if(count($posleases) > 0) // update existing record
		{
			
			$fields = array();
		
			$value = dbquote($form->value("poslease_lease_type"));
			$fields[] = "poslease_lease_type = " . $value;

			$value = dbquote($form->value("poslease_isindexed"));
			$fields[] = "poslease_isindexed = " . $value;

			$value = dbquote($form->value("poslease_tacit_renewal_duration_years"));
			$fields[] = "poslease_tacit_renewal_duration_years = " . $value;

			$value = dbquote($form->value("poslease_tacit_renewal_duration_months"));
			$fields[] = "poslease_tacit_renewal_duration_months = " . $value;

			$value = dbquote($form->value("poslease_indexclause_in_contract"));
			$fields[] = "poslease_indexclause_in_contract = " . $value;

			$value = dbquote($form->value("poslease_indexrate"));
			$fields[] = "poslease_indexrate = " . $value;

			$value = dbquote($form->value("poslease_average_increase"));
			$fields[] = "poslease_average_increase = " . $value;

					
			$value = dbquote(from_system_date($form->value("poslease_startdate")));
			$fields[] = "poslease_startdate = " . $value;

			$value = dbquote(from_system_date($form->value("poslease_enddate")));
			$fields[] = "poslease_enddate = " . $value;

			$value = dbquote(from_system_date($form->value("poslease_signature_date")));
			$fields[] = "poslease_signature_date = " . $value;

			$value = dbquote(from_system_date($form->value("poslease_extensionoption")));
			$fields[] = "poslease_extensionoption = " . $value;

			$value = dbquote(from_system_date($form->value("poslease_exitoption")));
			$fields[] = "poslease_exitoption = " . $value;


			$value = dbquote(from_system_date($form->value("poslease_handoverdate")));
			$fields[] = "poslease_handoverdate = " . $value;

			$value = dbquote(from_system_date($form->value("poslease_firstrentpayed")));
			$fields[] = "poslease_firstrentpayed = " . $value;

			$value = dbquote($form->value("poslease_freeweeks"));
			$fields[] = "poslease_freeweeks = " . $value;

			$value = dbquote($form->value("poslease_hasfixrent"));
			$fields[] = "poslease_hasfixrent = " . $value;

			
			$value = dbquote($form->value("poslease_negotiator"));
			$fields[] = "poslease_negotiator = " . $value;

			$value = dbquote($form->value("poslease_landlord_name"));
			$fields[] = "poslease_landlord_name = " . $value;

			$value = dbquote($form->value("poslease_negotiated_conditions"));
			$fields[] = "poslease_negotiated_conditions = " . $value;

			$value = dbquote($form->value("poslease_termination_time"));
			$fields[] = "poslease_termination_time = " . $value;

			$value = dbquote($form->value("poslease_deposit_duration"));
			$fields[] = "poslease_deposit_duration = " . $value;

			
			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update " . $posleases["table"] . " set " . join(", ", $fields) . " where poslease_id = " . $posleases["poslease_id"];
			mysql_query($sql) or dberror($sql);

		}
		else //insert new record
		{

			$fields = array();
			$values = array();

			$fields[] = "poslease_posaddress";
			$values[] = dbquote($posdata["posaddress_id"]);

			$fields[] = "poslease_order";
			$values[] = dbquote($project["project_order"]);

			$fields[] = "poslease_lease_type";
			$values[] = dbquote($form->value("poslease_lease_type"));
			
			$fields[] = "poslease_isindexed";
			$values[] = dbquote($form->value("poslease_isindexed"));

			$fields[] = "poslease_tacit_renewal_duration_years";
			$values[] = dbquote($form->value("poslease_tacit_renewal_duration_years"));

			$fields[] = "poslease_tacit_renewal_duration_months";
			$values[] = dbquote($form->value("poslease_tacit_renewal_duration_months"));

			$fields[] = "poslease_indexclause_in_contract";
			$values[] = dbquote($form->value("poslease_indexclause_in_contract"));

			$fields[] = "poslease_indexrate";
			$values[] = dbquote($form->value("poslease_indexrate"));

			$fields[] = "poslease_average_increase";
			$values[] = dbquote($form->value("poslease_average_increase"));

			$fields[] = "poslease_startdate";
			$values[] = dbquote(from_system_date($form->value("poslease_startdate")));

			$fields[] = "poslease_enddate";
			$values[] = dbquote(from_system_date($form->value("poslease_enddate")));

			$fields[] = "poslease_signature_date";
			$values[] = dbquote(from_system_date($form->value("poslease_signature_date")));

			$fields[] = "poslease_extensionoption";
			$values[] = dbquote(from_system_date($form->value("poslease_extensionoption")));

			$fields[] = "poslease_exitoption";
			$values[] = dbquote(from_system_date($form->value("poslease_exitoption")));

			$fields[] = "poslease_handoverdate";
			$values[] = dbquote(from_system_date($form->value("poslease_handoverdate")));

			$fields[] = "poslease_firstrentpayed";
			$values[] = dbquote(from_system_date($form->value("poslease_firstrentpayed")));

			$fields[] = "poslease_freeweeks";
			$values[] = dbquote($form->value("poslease_freeweeks"));

			$fields[] = "poslease_hasfixrent";
			$values[] = dbquote($form->value("poslease_hasfixrent"));

						
			$fields[] = "poslease_negotiator";
			$values[] = dbquote($form->value("poslease_negotiator"));

			$fields[] = "poslease_landlord_name";
			$values[] = dbquote($form->value("poslease_landlord_name"));

			$fields[] = "poslease_negotiated_conditions";
			$values[] = dbquote($form->value("poslease_negotiated_conditions"));

			$fields[] = "poslease_termination_time";
			$values[] = dbquote($form->value("poslease_termination_time"));

			$fields[] = "poslease_deposit_duration";
			$values[] = dbquote($form->value("poslease_deposit_duration"));

			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "date_modified";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "user_modified";
			$values[] = dbquote(user_login());

			if($posdata["table_leases"] == "posleasepipeline")
			{
				$sql = "insert into posleasespipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			}
			else
			{
				$sql = "insert into posleases (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			}
			
			mysql_query($sql) or dberror($sql);
		
		}

		
		//update cer_basic_data
		$fields = array();
    
		$value = dbquote(from_system_date($form->value("cer_basicdata_deadline_property")));
		$fields[] = "cer_basicdata_deadline_property = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;
		$fields[] = "user_modified = " . dbquote(user_login());
   
		$sql = "update cer_basicdata set " . join(", ", $fields) . 
			" where cer_basicdata_project = " . param("pid") . 
			" and cer_basicdata_version = 0 ";
		mysql_query($sql) or dberror($sql);


		//update milestones

		//create missing milestones
		$sql = "select * from milestones " .
			   "where milestone_active = 1 " .
			   "order by milestone_code ";

		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$sql_m = "select count(project_milestone_id) as num_recs from project_milestones " .
					 "where project_milestone_project = " . $project["project_id"] . 
					 "   and project_milestone_milestone = " . $row["milestone_id"];
			
			
			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($row_m["num_recs"] == 0)
			{
				$fields = array();
				$values = array();

				$fields[] = "project_milestone_project";
				$values[] = $project["project_id"];

				$fields[] = "project_milestone_milestone";
				$values[] = $row["milestone_id"];
				
				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());

				
				if($row["milestone_is_inr03_milestone"] == 1 and $project["project_cost_type"] == 1)
				{
					$sql = "insert into project_milestones (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
				elseif($row["milestone_is_inr03_milestone"] == 0)
				{
					$sql = "insert into project_milestones (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
			}
		}

		if($form->value('poslease_signature_date'))
		{
			$sql_u = "update project_milestones SET " . 
				     "project_milestone_date = " . dbquote(from_system_date($form->value("poslease_signature_date"))) . 
				     " where project_milestone_project = " . param("pid") . 
				     " and project_milestone_milestone = 33";

			$result = mysql_query($sql_u) or dberror($sql_u);
		}



		//update additional rental costs in case poslease dates have changed
		if(count($posleases) > 0)
		{
			/*
			if($old_lease_start_date != '' and $old_lease_start_date <> from_system_date($form->value("poslease_startdate")))
			{
				$result = calculate_additional_rental_costs(param('pid'), from_system_date($form->value("poslease_startdate")),  from_system_date($form->value("poslease_enddate")), $project["project_cost_totalsqms"]);
			}
			elseif($old_lease_end_date != '' and $old_lease_end_date <> from_system_date($form->value("poslease_enddate")))
			{
				$result = calculate_additional_rental_costs(param('pid'), from_system_date($form->value("poslease_startdate")), from_system_date($form->value("poslease_enddate")), $project["project_cost_totalsqms"]);
			}
			*/

			if($form->value("poslease_firstrentpayed") 
				and $form->value("poslease_startdate")
				and from_system_date($form->value("poslease_firstrentpayed")) > from_system_date($form->value("poslease_startdate"))) {
					$result = calculate_additional_rental_costs(param('pid'), from_system_date($form->value("poslease_firstrentpayed")), from_system_date($form->value("poslease_enddate")), $project["project_cost_totalsqms"]);
			}
			elseif($form->value("poslease_startdate")
				and $form->value("poslease_enddate")) {
				
				$result = calculate_additional_rental_costs(param('pid'), from_system_date($form->value("poslease_startdate")), from_system_date($form->value("poslease_enddate")), $project["project_cost_totalsqms"]);
			}
		}
	}
}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");
require "include/project_page_actions.php";

$page->header();
if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Lease Conditions");
}
else
{
	$page->title("Business Plan: Lease Conditions");
}
require_once("include/tabs2016.php");
$form->render();
?>

<div id="condition_info" style="display:none;">
	Please indicate the conditions you have negotiated for the variable rent<br />
	Example : Whichever is higher <br />
	In addition to the fix rent, etc
</div> 

<div id="termination" style="display:none;">
    Please provide the number of months prior to termination date that notice must be given to landlord of tenant's intention to extend or terminate.
</div> 

<div id="extenstion_option_info" style="display:none;">
    This date shall indicate until when a contract can be extended after its expiry.
</div> 

<div id="exit_option_info" style="display:none;">
    This date shall indicate the date you can terminate the contract before its expiry.
</div> 

<div id="deadline_for_property_info" style="display:none;">
    This date shall indicate until when you have to give final answer to your landlord.
</div> 

<div id="poslease_signature_info" style="display:none;">
    This date is to be put after lease contract has been signed<br />as you are only allowed to sign the lease contract after LNR approval.<br />
You will receive a notice when time has come to add this date.<br />
In case lease contract has anyway already been signed, please key in the signing date.

</div> 

<?php

require "include/footer_scripts.php";
$page->footer();

?>