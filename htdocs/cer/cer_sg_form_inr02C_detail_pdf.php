<?php
/********************************************************************

    cer_sg_form_inr02c_pdf_detail.php

    Print CER versus LN Figures Detail Page (Form INR-02C).

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-01-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-01-15
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.
*********************************************************************/

//set pdf parameters
$margin_top = 10;
$margin_left = 11;
$y = $margin_top;

$pdf->AddPage("L");
$globalPageNumber++;
$pdf->SetFillColor(248,251,167);


//general information
if(!function_exists('print_inr_02c_general_information'))
{
	function print_inr_02c_general_information($pdf, $locked = 0, $version = 0, $version_date = '')
	{
		$margin_top = 10;
		$margin_left = 10;
		$y = $margin_top;
		$x = $margin_left+1;
		
		// Title first line
		
	
		$pdf->SetXY($margin_left,$margin_top);
		$pdf->SetFont("arialn", "B", 10);
		$pdf->Cell(51, 8, "The Swatch Group", 1);

		$pdf->SetFont("arialn", "B", 14);
		$pdf->Cell(166, 8, "RETAIL BUSINESS PLAN OVERVIEW", 1, "", "C");


		$pdf->SetFont("arialn", "", 10);

		$pdf->Cell(20, 8, "DETAILS", 1, "", "C", false);
		if($version == 0)
		{
			$pdf->Cell(20, 8, date("d.m.Y"), 1, "", "C", true);
		}
		else
		{
			$pdf->Cell(20, 8, to_system_date(substr($version_date, 0, 10)), 1, "", "C", true);
		}
		$pdf->SetFont("arialn", "B", 10);
		$pdf->Cell(20, 8, "INR-02C", 1, "", "C");
	
		

		// 5. General Information
		$y = $pdf->GetY()+10;
		$pdf->SetXY($x,$y);
		$pdf->SetFont("arialn", "B", 11);
		$pdf->Cell(275, 6, "5. CER figures versus LNR figures", 1, "", "L");

		$y = $pdf->GetY()+12;

		return $y;
	}

}
$y = print_inr_02c_general_information($pdf, $cer_basicdata["cer_basicdata_cer_locked"], $cer_basicdata["cer_basicdata_version"], $cer_basicdata["version_date"]);

// SALES DATA

$standard_y = 3.5;


$is_fist_page = 1;
if(count($cer_ln_years) > 0 and $cer_ln_years_first_year > 2000)
{
	$number_of_years_per_data_set = 4;
	$number_of_data_sets = ceil(count($cer_ln_years) / $number_of_years_per_data_set);

	$tmp_first_year = $cer_ln_years_first_year-$number_of_years_per_data_set;


	for($i=1;$i<=$number_of_data_sets;$i++)
	{
		$tmp_first_year = $tmp_first_year+$number_of_years_per_data_set;
	
		
		if($i==5 and $is_fist_page == 1)
		{
			$pdf->AddPage("L");
			$globalPageNumber++;
			$y = print_inr_02c_general_information($pdf, $project_number, $project_name, $legal_entity, $pos_type_name, $project_kind , $legal_type_name, $pos_country, $pos_city, $pos_address, $project_manager, $relocated_pos_name);

			$y = $y+2;

			$is_fist_page = 0;
		}
		else
		{
			$y = $pdf->GetY() + 6;
		}

		$pdf->SetXY($margin_left,$y);
		$pdf->SetFont("arialn", "B", 10);
		$pdf->Cell(75, 6, "Sales Data: CER Figures versus LNR Figures in T" . $currency_symbol, 1, "", "L");

		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{
			$year = $tmp_first_year+$k;
			if(array_key_exists($year, $cer_ln_years))
			{
				
				if($year == $cer_first_year and $year == $ln_first_year)
				{
					$tmp = $year . " - CER=" . $number_of_months_first_year . "m, LNR=" . $ln_number_of_months_first_year . "m";
					$pdf->Cell(50, 6, $tmp, 1, "", "C");
				}
				elseif($year == $cer_first_year)
				{
					$tmp = $year . " - CER=" . $number_of_months_first_year. "m";
					$pdf->Cell(50, 6, $tmp, 1, "", "C");
				}
				elseif($year == $cer_first_year)
				{
					$tmp = $year . " - LNR=" . $ln_number_of_months_first_year. "m";
					$pdf->Cell(50, 6, $tmp, 1, "", "C");
				}
				elseif($year == $cer_last_year and $year == $ln_last_year)
				{
					$tmp = $year . " - CER=" . $number_of_months_last_year . "m, LNR=" . $ln_number_of_months_last_year . "m";
					$pdf->Cell(50, 6, $tmp, 1, "", "C");
				}
				elseif($year == $cer_last_year)
				{
					$tmp = $year . " - CER=" . $number_of_months_last_year. "m";
					$pdf->Cell(50, 6, $tmp, 1, "", "C");
				}
				elseif($year == $ln_last_year)
				{
					$tmp = $tmp_first_year+$k . " - LNR=" . $ln_number_of_months_last_year. "m";
					$pdf->Cell(50, 6, $tmp, 1, "", "C");
				}
				else
				{
					$pdf->Cell(50, 6, $year, 1, "", "C");
				}
			}
		}
		
		$y = $y + 6;

		$pdf->SetXY($margin_left,$y);
		$pdf->SetFont("arialn", "B", 8);
		$pdf->Cell(75, 3.5, "", 1, "", "R");

		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{
			$year = $tmp_first_year+$k;
			if(array_key_exists($year, $cer_ln_years))
			{
				$pdf->Cell(20, 3.5, "CER (INR-02B)", 1, "", "R");
				$pdf->Cell(19, 3.5, "LNR (LNR-02A)", 1, "", "R");
				$pdf->Cell(11, 3.5, "Delta", 1, "", "R");
			}
		}

		$pdf->Ln();
		$pdf->SetX($margin_left);
		$pdf->SetFont("arialn", "", 8);
		$pdf->Cell(75, 3.5, "Watches Units", 1, "", "L");

		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{	
			$year = $tmp_first_year+$k;
			$delta = "";
			if(array_key_exists($year, $cer_ln_years))
			{
				if(array_key_exists($year, $sales_units_watches_values))
				{
					$pdf->Cell(20, 3.5, $sales_units_watches_values[$year], 1, "", "R");
				}
				else
				{
					$pdf->Cell(20, 3.5, "-", 1, "", "R");
				}


				if(array_key_exists($year, $ln_sales_units_watches_values))
				{
					$pdf->Cell(19, 3.5, $ln_sales_units_watches_values[$year], 1, "", "R");

					if(array_key_exists($year, $sales_units_watches_values) and $ln_sales_units_watches_values[$year] > 0)
					{
						$delta = number_format(round((100*$sales_units_watches_values[$year] / $ln_sales_units_watches_values[$year]) - 100, 1), 1) . "%";
					}
				}
				else
				{
					$pdf->Cell(19, 3.5, "-", 1, "", "R");
				}

				

				$pdf->Cell(11, 3.5, $delta, 1, "", "R");
			}
		}
		$pdf->Ln();
		$pdf->SetX($margin_left);
		$pdf->Cell(75, 3.5, "Watch Straps Units", 1, "", "L");

		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{	
			$year = $tmp_first_year+$k;
			$delta = "";
			if(array_key_exists($year, $cer_ln_years))
			{
				if(array_key_exists($year, $sales_units_jewellery_values))
				{
					$pdf->Cell(20, 3.5, $sales_units_jewellery_values[$year], 1, "", "R");

				}
				else
				{
					$pdf->Cell(20, 3.5, "-", 1, "", "R");
				}


				if(array_key_exists($year, $ln_sales_units_jewellery_values))
				{
					$pdf->Cell(19, 3.5, $ln_sales_units_jewellery_values[$year], 1, "", "R");

					if(array_key_exists($year, $sales_units_jewellery_values) and $ln_sales_units_jewellery_values[$year] > 0)
					{
						$delta = number_format(round((100*$sales_units_jewellery_values[$year] / $ln_sales_units_jewellery_values[$year])- 100, 1), 1) . "%";
					}
				}
				else
				{
					$pdf->Cell(19, 3.5, "-", 1, "", "R");
				}

				$pdf->Cell(11, 3.5, $delta, 1, "", "R");
			}
		}

		$pdf->Ln();
		$pdf->SetX($margin_left);
	

		$pdf->SetFont("arialn", "B", 8);
		$pdf->Ln();
		$pdf->SetX($margin_left);
		$pdf->SetXY($pdf->GetX(), $pdf->GetY()+1);
		$pdf->Cell(75, 3.5, "Net Sales", 1, "", "L");

		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{	
			$year = $tmp_first_year+$k;
			$delta = "";
			if(array_key_exists($year, $cer_ln_years))
			{
				if(array_key_exists($year, $total_net_sales_values))
				{
					$pdf->Cell(20, 3.5, round(($exchange_rate*$total_net_sales_values[$year]/$exchange_rate_factor) / 1000,0), 1, "", "R");
				}
				else
				{
					$pdf->Cell(20, 3.5, "-", 1, "", "R");
				}


				if(array_key_exists($year, $total_net_sales_values) and array_key_exists($year, $ln_total_net_sales_values))
				{
					$pdf->Cell(19, 3.5, round(($ln_exchange_rate*$ln_total_net_sales_values[$year]/$ln_exchange_rate_factor) / 1000,0), 1, "", "R");

					if($ln_total_net_sales_values[$year] > 0)
					{
						$delta = number_format(round((100*($exchange_rate*$total_net_sales_values[$year]/$exchange_rate_factor) / ($ln_exchange_rate*$ln_total_net_sales_values[$year]/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";
					}
				}
				else
				{
					$pdf->Cell(19, 3.5, "-", 1, "", "R");
				}
				
				$pdf->SetFont("arialn", "", 8);
				$pdf->Cell(11, 3.5, $delta, 1, "", "R");
				$pdf->SetFont("arialn", "B", 8);
			}
		}

		$pdf->Ln();
		$pdf->SetX($margin_left);
		$pdf->Cell(75, 3.5, "Operating Income (incl. WSM: CER = " . $cer_basicdata["cer_basicdata_wholesale_margin"] . "%, LNR=" . $ln_wholesale_margin . "%)", 1, "", "L");

		
		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{	
			$year = $tmp_first_year+$k;
			$delta = "";
			if(array_key_exists($year, $cer_ln_years))
			{
				if(array_key_exists($year, $operating_income02_values))
				{
					$pdf->Cell(20, 3.5, round(($exchange_rate*$operating_income02_values[$year]/$exchange_rate_factor) / 1000,0), 1, "", "R");
				}
				else
				{
					$pdf->Cell(20, 3.5, "-", 1, "", "R");
				}


				if(array_key_exists($year, $operating_income02_values) and array_key_exists($year, $ln_operating_income02_values))
				{
					$pdf->Cell(19, 3.5, round(($ln_exchange_rate*$ln_operating_income02_values[$year]/$ln_exchange_rate_factor) / 1000,0), 1, "", "R");

					if($ln_operating_income02_values[$year] != 0)
					{
						
						if($operating_income02_values[$year] < 0 
							and $ln_operating_income02_values[$year] < 0)
						{
							$delta = round
								(100*
									(
										(
											($ln_exchange_rate*$ln_operating_income02_values[$year]/$ln_exchange_rate_factor)-
											($exchange_rate*$operating_income02_values[$year]/$exchange_rate_factor)
										)
										/ 
										($ln_exchange_rate*$ln_operating_income02_values[$year]/$ln_exchange_rate_factor)
									)
								, 1
								) . "%";
						}
						else
						{
							$delta = number_format(round((100*($exchange_rate*$operating_income02_values[$year]/$exchange_rate_factor) / ($ln_exchange_rate*$ln_operating_income02_values[$year]/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";
						}
					}
				}
				else
				{
					$pdf->Cell(19, 3.5, "-", 1, "", "R");
				}

				$pdf->SetFont("arialn", "", 8);
				$pdf->Cell(11, 3.5, $delta, 1, "", "R");
				$pdf->SetFont("arialn", "B", 8);
			}
		}



		$pdf->Ln();
		$pdf->SetX($margin_left);
		$pdf->Cell(75, 3.5, "Operating income incl. WHS margin in %", 1, "", "L");

		
		for($k=0;$k<$number_of_years_per_data_set;$k++)
		{	
			$year = $tmp_first_year+$k;
			$delta = "";
			if(array_key_exists($year, $cer_ln_years))
			{
				if(array_key_exists($year, $operating_income02_values) and $total_net_sales_values[$year] > 0)
				{
					$pdf->Cell(20, 3.5, round(100*($exchange_rate*$operating_income02_values[$year]/$exchange_rate_factor)/($ln_exchange_rate*$total_net_sales_values[$year]/$ln_exchange_rate_factor),1) . "%", 1, "", "R");
				}
				else
				{
					$pdf->Cell(20, 3.5, "-", 1, "", "R");
				}


				if(array_key_exists($year, $ln_operating_income02_values) and $ln_total_net_sales_values[$year] > 0)
				{
					$pdf->Cell(19, 3.5, round(100*($ln_exchange_rate*$ln_operating_income02_values[$year]/$ln_exchange_rate_factor)/($ln_exchange_rate*$ln_total_net_sales_values[$year]/$ln_exchange_rate_factor),1) . "%", 1, "", "R");
				}
				else
				{
					$pdf->Cell(19, 3.5, "-", 1, "", "R");
				}

				$pdf->SetFont("arialn", "", 8);
				$pdf->Cell(11, 3.5, $delta, 1, "", "R");
				$pdf->SetFont("arialn", "B", 8);
			}
		}
	}
}




//INVESTMENTS

if($pdf->GetY() > 150)
{
	$pdf->AddPage("L");
	$globalPageNumber++;

	$y = print_inr_02c_general_information($pdf, $project_number, $project_name, $legal_entity, $pos_type_name, $project_kind , $legal_type_name, $pos_country, $pos_city, $pos_address, $project_manager, $relocated_pos_name);
}

$y = $pdf->GetY() + 6;

$pdf->SetXY($margin_left,$y);
$pdf->SetFont("arialn", "B", 10);
$pdf->Cell(75, 6, "Investment: CER Figures versus LNR Figures in T" . $currency_symbol, 1, "", "L");

if(array_key_exists("cid", $_GET) and $_GET["cid"] ==  "s")
{
	$pdf->SetFont("arialn", "", 8);
	$pdf->Cell(90, 6, "Exchange Rate CER = " . $exchange_rate . ", Exchange Rate LNR = " . $ln_exchange_rate, 1, "" , "L");
}
else
{
	$pdf->Cell(90, 6, "", 1, "" , "L");
}
$y = $y + 6;


//draw boxes for investments
$pdf->SetXY($margin_left,$y);
$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(75, 50.7, "", 1, "", "L");


$pdf->SetXY($margin_left+75,$y);
$pdf->Cell(20, 50.7, "", 1, "", "L");

$pdf->SetXY($margin_left+95,$y);
$pdf->Cell(15, 50.7, "", 1, "", "L");

$pdf->SetXY($margin_left+110,$y);
$pdf->Cell(20, 50.7, "", 1, "", "L");


$pdf->SetXY($margin_left+130,$y);
$pdf->Cell(15, 50.7, "", 1, "", "L");


$pdf->SetXY($margin_left+145,$y);
$pdf->Cell(20, 50.7, "", 1, "", "L");

$x1 = $margin_left+1;
$x2 = $margin_left+75;

$y = $y + 1;
$y2 = $y;   // for column2


$pdf->SetXY($margin_left,$y);
$pdf->SetFont("arialn", "B", 9);
$pdf->Cell(20, 3.5, "Project cost:", 0, "", "L");


$pdf->SetXY($x2,$y);
$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(20, 3.5, "CER (INR-02B)", 1, "", "R");
$pdf->Cell(15, 3.5, "Share", 1, "", "R");
$pdf->Cell(20, 3.5, "LNR (LNR-02A)", 1, "", "R");
$pdf->Cell(15, 3.5, "Share", 1, "", "R");
$pdf->Cell(20, 3.5, "Delta CER/LNR", 1, "", "R");

foreach($fixed_assets as $key=>$itype)
{

	if(array_key_exists($itype, $amounts))
	{
		if(!$amounts[$itype]){
			$amounts[$itype] = "-";
			
		}

		$y = $y + $standard_y;
		$pdf->SetXY($x1,$y);
		$pdf->SetFont("arialn", "", 8);

		$pdf->Cell(63, 3.5, $investment_names[$itype] , 0, "", "L");

		$pdf->SetXY($x2,$y);
		$pdf->SetFont("arialn", "", 8);
		
		
		if($amounts[$itype] != "-" and $amounts[$itype] != 0)
		{
			$pdf->Cell(20, 3.5, round(($exchange_rate*$amounts[$itype]/$exchange_rate_factor) / 1000,0), 0, "", "R");

			if($investment_total > 0)
			{
				
				$pdf->Cell(15, 3.5, round(($exchange_rate*$amounts[$itype]/$exchange_rate_factor)/($exchange_rate*$investment_total/$exchange_rate_factor), 2)*100 . "%", 0, "", "R");
			}
			else
			{
				$pdf->Cell(15, 3.5, "-", 0, "", "R");
			}
		}
		else
		{
			$pdf->Cell(20, 3.5, "-", 0, "", "R");
			$pdf->Cell(15, 3.5, "-", 0, "", "R");
		}

		if(array_key_exists($itype, $ln_amounts) and $ln_amounts[$itype] != "-" and $ln_amounts[$itype] != 0)
		{
			$pdf->Cell(20, 3.5, round(($ln_exchange_rate*$ln_amounts[$itype]/$ln_exchange_rate_factor) / 1000,0), 0, "", "R");

			if($ln_investment_total > 0)
			{
				$pdf->Cell(15, 3.5, round(($ln_exchange_rate*$ln_amounts[$itype]/$ln_exchange_rate_factor)/($ln_exchange_rate*$ln_investment_total/$ln_exchange_rate_factor), 2)*100 . "%", 0, "", "R");
			}
			else
			{
				$pdf->Cell(15, 3.5, "-", 0, "-", "R");
			}
		}
		else
		{
			$pdf->Cell(20, 3.5, "-", 0, "", "R");
			$pdf->Cell(15, 3.5, "-", 0, "", "R");
		}


		if(array_key_exists($itype, $ln_amounts) and $ln_amounts[$itype] > 0)
		{
			$delta = number_format(round((100*($exchange_rate*$amounts[$itype]/$exchange_rate_factor) / ($ln_exchange_rate*$ln_amounts[$itype]/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";
			$pdf->Cell(20, 3.5, $delta, 0, "-", "R");
		}
		else
		{
			$pdf->Cell(20, 3.5, "-", 0, "-", "R");
		}
	}
	else
	{
		$y = $y + $standard_y;
	}
}



$y = $y + $standard_y;
$pdf->SetXY($x1,$y);
$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(20, 3.5, "Gross total invest. fixed assets", 0, "", "L");

$pdf->SetXY($x2,$y);
$pdf->SetFont("arialn", "B", 8);

$pdf->Cell(20, 3.5, round(($exchange_rate*$investment_total/$exchange_rate_factor) / 1000,0), 1, "", "R");
$pdf->Cell(15, 3.5, "", 1, "", "R");
$pdf->Cell(20, 3.5, round(($ln_exchange_rate*$ln_investment_total/$ln_exchange_rate_factor) / 1000,0), 1, "", "R");
$pdf->Cell(15, 3.5, "", 1, "", "R");

if($ln_investment_total > 0)
{
	$delta = number_format(round((100*($exchange_rate*$investment_total/$exchange_rate_factor) / ($ln_exchange_rate*$ln_investment_total/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";
	$pdf->Cell(20, 3.5, $delta, 1, "-", "R");
}
else
{
	$pdf->Cell(20, 3.5, "", 1, "", "R");
}


//landlorad's contribution

$y = $y + $standard_y;
$pdf->SetXY($x1,$y);
$pdf->SetFont("arialn", "", 8);

$pdf->Cell(63, 3.5, "Landlord's Contribution" , 0, "", "L");

$pdf->SetXY($x2,$y);
$pdf->SetFont("arialn", "", 8);


if($landloard_contribution != 0 or $ln_landloard_contribution != 0)
{
	$pdf->Cell(20, 3.5, round(($exchange_rate*$landloard_contribution/$exchange_rate_factor) / 1000,0), 0, "", "R");

	if($investment_total > 0)
	{
		
		$pdf->Cell(15, 3.5, round(($exchange_rate*$landloard_contribution/$exchange_rate_factor)/($exchange_rate*$investment_total/$exchange_rate_factor), 2)*100 . "%", 0, "", "R");
	}
	else
	{
		$pdf->Cell(15, 3.5, "-", 0, "", "R");
	}

	$pdf->Cell(20, 3.5,	round(($ln_exchange_rate*$ln_landloard_contribution/$ln_exchange_rate_factor) / 1000,0), 0, "", "R");

	if($ln_investment_total > 0)
	{
		$pdf->Cell(15, 3.5, round(($ln_exchange_rate*$ln_landloard_contribution/$ln_exchange_rate_factor)/($ln_exchange_rate*$ln_investment_total/$ln_exchange_rate_factor), 2)*100  . "%", 0, "", "R");
	}
	else
	{
		$pdf->Cell(15, 3.5, "-", 0, "-", "R");
	}

	$delta = number_format(round((100*($exchange_rate*$landloard_contribution/$exchange_rate_factor) / ($ln_exchange_rate*ln_landloard_contribution/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";

	$pdf->Cell(20, 3.5, $delta, 0, "-", "R");
}
else
{
	$pdf->Cell(20, 3.5, "-", 0, "", "R");
	$pdf->Cell(15, 3.5, "-", 0, "", "R");
	$pdf->Cell(20, 3.5, "-", 0, "", "R");
	$pdf->Cell(15, 3.5, "-", 0, "", "R");
	$pdf->Cell(20, 3.5, "-", 0, "", "R");
}


$y = $y + $standard_y;
$pdf->SetXY($x1,$y);
$pdf->SetFont("arialn", "B", 8);
$pdf->Cell(20, 3.5, "Net total invest. fixed assets", 0, "", "L");

$pdf->SetXY($x2,$y);
$pdf->SetFont("arialn", "B", 8);

$pdf->Cell(20, 3.5, round(($exchange_rate*($investment_total+ $landloard_contribution)/$exchange_rate_factor) / 1000,0), 1, "", "R");
$pdf->Cell(15, 3.5, "", 1, "", "R");
$pdf->Cell(20, 3.5, round(($ln_exchange_rate*($ln_investment_total+$ln_landloard_contribution)/$ln_exchange_rate_factor) / 1000,0), 1, "", "R");
$pdf->Cell(15, 3.5, "", 1, "", "R");

if($ln_investment_total > 0)
{
	$delta = number_format(round((100*($exchange_rate*($investment_total + $landloard_contribution)/$exchange_rate_factor) / ($ln_exchange_rate*($ln_investment_total+ $ln_landloard_contribution)/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";
	$pdf->Cell(20, 3.5, $delta, 1, "-", "R");
}
else
{
	$pdf->Cell(20, 3.5, "", 1, "", "R");
}

/*INTANGIBLES*/

$y = $y + $standard_y;
$pdf->SetXY($x1,$y);
$pdf->SetFont("arialn", "", 8);

$pdf->Cell(20, 3.5, $intagible_name, 0, "", "L");

$pdf->SetXY($x2,$y);
$pdf->SetFont("arialn", "", 8);

if($intagible_amount != 0)
{
	$pdf->Cell(20, 3.5, round(($exchange_rate*$intagible_amount/$exchange_rate_factor) / 1000,0), 0, "", "R");
}
else
{
	$pdf->Cell(20, 3.5, "-", 0, "", "R");
}
$pdf->Cell(15, 3.5, "-", 0, "", "R");

if($ln_intagible_amount != 0)
{
	$pdf->Cell(20, 3.5, round(($ln_exchange_rate*$ln_intagible_amount/$ln_exchange_rate_factor) / 1000,0), 0, "", "R");
}
else
{
	$pdf->Cell(20, 3.5, "-", 0, "", "R");
}

$pdf->Cell(15, 3.5, "-", 0, "", "R");

if($ln_intagible_amount > 0)
{
	$delta = number_format(round((100*($exchange_rate*$intagible_amount/$exchange_rate_factor) / ($ln_exchange_rate*$ln_intagible_amount/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";
	$pdf->Cell(20, 3.5, $delta, 0, "-", "R");
}
else
{
	if($intagible_amount > 0)
	{
		$pdf->Cell(20, 3.5, "100.0%", 0, "-", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 0, "-", "R");
	}
}

//$pdf->SetXY($x3,$y);
//$pdf->SetFont("arialn", "", 8);


$y = $y + $standard_y;
$pdf->SetXY($x1,$y);
$pdf->SetFont("arialn", "", 8);

if(array_key_exists(9, $investment_names))
{
	$pdf->Cell(20, 3.5, $investment_names[9], 0, "", "L");
}
else
{
	$pdf->Cell(20, 3.5,"", 0, "", "L");
}

$pdf->SetXY($x2,$y);
$pdf->SetFont("arialn", "", 8);


if(array_key_exists(9, $amounts))
{
	if($amounts[9] != 0)
	{
		$pdf->Cell(20, 3.5, round(($exchange_rate*$amounts[9]/$exchange_rate_factor) / 1000,0), 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 0, "", "R");
	}
}
else
{
	$pdf->Cell(20, 3.5, "", 0, "", "R");
}
$pdf->Cell(15, 3.5, "-", 0, "", "R");

if(array_key_exists(9, $ln_amounts))
{
	if($ln_amounts[9] != 0)
	{
		$pdf->Cell(20, 3.5, round(($ln_exchange_rate*$ln_amounts[9]/$ln_exchange_rate_factor) / 1000,0), 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 0, "", "R");
	}
}
else
{
	$pdf->Cell(20, 3.5, "", 0, "", "R");
}
$pdf->Cell(15, 3.5, "-", 0, "", "R");

if(array_key_exists(9, $ln_amounts) and $ln_amounts[9] > 0)
{
	$delta = number_format(round((100*($exchange_rate*$amounts[9]/$exchange_rate_factor) / ($ln_exchange_rate*$ln_amounts[9]/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";
	$pdf->Cell(20, 3.5, $delta, 0, "-", "R");
}
else
{
	if($amounts[9] > 0)
	{
		$pdf->Cell(20, 3.5, "100.0%", 0, "-", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 0, "-", "R");
	}
}

$y = $y + $standard_y;
$pdf->SetXY($x1,$y);
$pdf->SetFont("arialn", "", 8);

if(array_key_exists(13, $ln_amounts) and array_key_exists(13, $investment_names))
{
	$pdf->Cell(20, 3.5, $investment_names[13], 0, "", "L");


}
else
{
	$pdf->Cell(20, 3.5, "", 0, "", "L");
}


$pdf->SetXY($x2,$y);
$pdf->SetFont("arialn", "", 8);

if(array_key_exists(13, $amounts))
{
	if(array_key_exists(13, $ln_amounts) and $amounts[13] != 0)
	{
		$pdf->Cell(20, 3.5, round(($exchange_rate*$amounts[13]/$exchange_rate_factor) / 1000,0), 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 0, "", "R");
	}
}
else
{
	$pdf->Cell(20, 3.5, "", 0, "", "R");
}
$pdf->Cell(15, 3.5, "-", 0, "", "R");

if(array_key_exists(13, $ln_amounts))
{
	if($ln_amounts[13] != 0)
	{
		$pdf->Cell(20, 3.5, round(($ln_exchange_rate*$ln_amounts[13]/$ln_exchange_rate_factor) / 1000,0), 0, "", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 0, "", "R");
	}
}
else
{
	$pdf->Cell(20, 3.5, "", 0, "", "R");
}
$pdf->Cell(15, 3.5, "-", 0, "", "R");

if(array_key_exists(13, $ln_amounts) and $ln_amounts[13] > 0)
{
	$delta = number_format(round((100*($exchange_rate*$amounts[13]/$exchange_rate_factor) / ($ln_exchange_rate*$ln_amounts[13]/$ln_exchange_rate_factor)) - 100, 1), 1) . "%";
	$pdf->Cell(20, 3.5, $delta, 0, "-", "R");
}
else
{
	if(array_key_exists(13, $ln_amounts) and $amounts[13] > 0)
	{
		$pdf->Cell(20, 3.5, "100.0%", 0, "-", "R");
	}
	else
	{
		$pdf->Cell(20, 3.5, "-", 0, "-", "R");
	}
}


$y = $y+ $standard_y;



$pdf->SetXY($margin_left,$y);
$pdf->SetFont("arialn", "B", 9);
$pdf->Cell(50, 3.5, "Total Project costs (Requested amount)", 0, "", "L");

$pdf->SetXY($x2,$y);
$pdf->SetFont("arialn", "B", 9);
$pdf->Cell(20, 3.5, round(($exchange_rate*$cer_totals/$exchange_rate_factor) / 1000,1), 1, "", "R");
$pdf->Cell(15, 3.5, "", 1, "", "R");
$pdf->Cell(20, 3.5, round(($ln_exchange_rate*$ln_totals/$ln_exchange_rate_factor) / 1000,1), 1, "", "R");
$pdf->Cell(15, 3.5, "", 1, "", "R");


if($ln_totals > 0)
{
	$delta = number_format(round((100*($exchange_rate*$cer_totals/$exchange_rate_factor) / ($ln_exchange_rate*$ln_totals/$ln_exchange_rate_factor)) - 100 , 1), 1) . "%";
	$pdf->Cell(20, 3.5, $delta, 1, "-", "R");
}
else
{
	$pdf->Cell(20, 3.5, "-", 1, "-", "R");
}

$outerbox_drawn = false;
$predecessor_printed = false;

$pdf->SetAutoPageBreak(true, 10);
if($cer_basicdata["cer_basicdata_cer_versus_ln_comment4"])
{
	if($pdf->getY() > 170)
	{
		//draw outer box
		$pdf->SetXY($margin_left-1,18);
		$pdf->Cell(277, 175, "", 1);
		$outerbox_drawn == true;

		$pdf->AddPage("L");
		$globalPageNumber++;
		$y = print_inr_02c_general_information($pdf, $cer_basicdata["cer_basicdata_cer_locked"], $cer_basicdata["cer_basicdata_version"], $cer_basicdata["version_date"]);
		$pdf->setY($y-5);
	}
	else {
		$pdf->ln();
		$pdf->ln();
	}

	$pdf->SetX($margin_left);
	
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, 3.5, "Comment on deviation CER (INR02B) versus LNR (LNR-02A) - Country Comment", 0, "", "L");
	$pdf->ln();
	$pdf->SetX($margin_left);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(275,0, $cer_basicdata["cer_basicdata_cer_versus_ln_comment4"], 1, "T");
	$predecessor_printed = true;
	$y_tmp = $pdf->GetY();
}


if($cer_basicdata["cer_basicdata_cer_versus_ln_comment5"])
{
	$pdf->setY($pdf->getY()+3);
	if($pdf->getY() > 170)
	{
		//draw outer box
		$pdf->SetXY($margin_left-1,18);
		$pdf->Cell(277, 175, "", 1);
		$outerbox_drawn == true;

		$pdf->AddPage("L");
		$globalPageNumber++;
		$y = print_inr_02c_general_information($pdf, $cer_basicdata["cer_basicdata_cer_locked"], $cer_basicdata["cer_basicdata_version"], $cer_basicdata["version_date"]);
		$pdf->setY($y-5);
	}
	if($predecessor_printed == false)
	{
		$pdf->ln();
		$pdf->ln();
	}

	$pdf->SetX($margin_left);
	$pdf->SetFont("arialn", "B", 9);
	$pdf->Cell(50, 3.5, "Comment on deviation CER (INR02B) versus LNR (LNR-02A) - Brand HQ Comment", 0, "", "L");
	$pdf->ln();
	$pdf->SetX($margin_left);
	$pdf->SetFont("arialn", "", 9);
	$pdf->MultiCell(275,0, $cer_basicdata["cer_basicdata_cer_versus_ln_comment5"], 1, "T");
	$predecessor_printed = true;
	
}

if($outerbox_drawn == false) {
	$y_tmp = $pdf->GetY();
	//draw outer box
	$pdf->SetXY($margin_left-1,18);
	$pdf->Cell(277, 175, "", 1);
}
?>