<?php
/********************************************************************

	cer_application_revenues.php

    Application Form: revenues
    
	Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-04-14
    Version:        2.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";
require_once "../shared/func_posindex.php";


check_access("has_access_to_cer");
set_referer("cer_application_revenue.php");


/********************************************************************
    prepare all data needed
*********************************************************************/

$currency = get_cer_currency(param("pid"));
$cer_basicdata = get_cer_basicdata(param("pid"));
require "include/get_project.php";

$project = get_project($cer_basicdata["cer_basicdata_project"]);
$order_number = $project["project_order"];

$posdata = get_pos_data($project["project_order"]);
$posleases = get_pos_leasedata($posdata["posaddress_id"], $project["project_order"]);



$apply_new_version_of_rental_tool = 0;

if(($ln_basicdata["ln_basicdata_submitted"] == NULL 
   or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == '0000')
   and ($cer_basicdata["cer_basicdata_submitted"] == NULL 
   or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == '0000'))
{
	$apply_new_version_of_rental_tool = 1;
}
elseif($ln_basicdata["ln_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014
	and ($cer_basicdata["cer_basicdata_submitted"] == NULL 
	or substr($cer_basicdata["cer_basicdata_submitted"], 0, 4) == '0000'))
{
	$apply_new_version_of_rental_tool = 1;
}
elseif(($ln_basicdata["ln_basicdata_submitted"] == NULL 
   or substr($ln_basicdata["ln_basicdata_submitted"], 0, 4) == '0000')
   and $cer_basicdata["cer_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014)
{
	$apply_new_version_of_rental_tool = 1;
}
elseif($ln_basicdata["ln_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014
       and $cer_basicdata["cer_basicdata_submitted"] > DATE_NEW_RENTAL_TOOL_2014)
{
	$apply_new_version_of_rental_tool = 1;
}


require "include/financial_functions.php";
require "include/in_financial_data.php";

$years = array();

$sql = "select * " .
       "from cer_expenses " . 
	   "where cer_expense_type = 1 " . 
	   "and cer_expense_project = " . param("pid") . 
	   " and cer_expense_cer_version = 0 ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$years[] = $row["cer_expense_year"];
}

//get revenue values
$quantity_watches = array();
$aveargeprice_watches = array();
$quantity_jewellery = array();
$aveargeprice_jewellery = array();
$customer_frequencies = array();
$days_open_per_year = array();
$working_hours_per_week = array();
$opening_hours_per_week = array();

$total_gross_sales = array();
$total_net_sales = array();

$number_of_months = array();

$tmp_fy = $cer_basicdata["cer_basicdata_firstyear"];
$tmp_fy2 = 0;
$tmp_fm = $cer_basicdata["cer_basicdata_firstmonth"];
$tmp_fm2 = 0;
$tmp_ly = $cer_basicdata["cer_basicdata_lastyear"];
$tmp_lm = $cer_basicdata["cer_basicdata_lastmonth"];


if(($project["project_projectkind"] == 3 or $project["project_projectkind"] == 9)
   and $project["project_planned_takeover_date"] != NULL and $project["project_planned_takeover_date"] != '0000-00-00')
{
	if(substr($project["project_planned_takeover_date"], 5,2) > $cer_basicdata["cer_basicdata_firstmonth"])
	{
		$tmp_fy2 = substr($project["project_planned_takeover_date"], 0, 4);
		$tmp_fm2 = substr($project["project_planned_takeover_date"], 5, 2);
	}
}
elseif(substr($project["project_real_opening_date"], 0, 4) > $tmp_fy)
{
	$tmp_fy2 = substr($project["project_real_opening_date"], 0, 4);
	$tmp_fm2 = substr($project["project_real_opening_date"], 5, 2);
}
elseif(substr($project["project_real_opening_date"], 0, 4) == $tmp_fy
	and substr($project["project_real_opening_date"], 5, 2) > $tmp_fm)
{
	$tmp_fm = substr($project["project_real_opening_date"], 5, 2);
}



$exclude_some_years = false;

$sql = "select * from cer_revenues " . 
       "where cer_revenue_project = " . param("pid") . 
	   " and cer_revenue_cer_version = 0 " . 
	   " order by cer_revenue_year";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$quantity_watches[$row["cer_revenue_id"]] = $row["cer_revenue_quantity_watches"];
	$aveargeprice_watches[$row["cer_revenue_id"]] = $row["cer_revenue_aveargeprice_watches"];
	$quantity_jewellery[$row["cer_revenue_id"]] = $row["cer_revenue_quantity_jewellery"];
	$aveargeprice_jewellery[$row["cer_revenue_id"]] = $row["cer_revenue_aveargeprice_jewellery"];


	if(array_key_exists($row["cer_revenue_year"], $total_gross_sales_values))
	{
		$total_gross_sales[$row["cer_revenue_id"]] = $total_gross_sales_values[$row["cer_revenue_year"]];
	}
	

	if(array_key_exists($row["cer_revenue_year"], $total_net_sales_values))
	{
		$total_net_sales[$row["cer_revenue_id"]] = $total_net_sales_values[$row["cer_revenue_year"]];
	}

	$customer_frequencies[$row["cer_revenue_id"]] = $row["cer_revenue_customer_frequency"];

	$days_open_per_year[$row["cer_revenue_id"]] = $row["cer_revenue_total_days_open_per_year"];
	$opening_hours_per_week[$row["cer_revenue_id"]] = $row["cer_revenue_total_hours_open_per_week"];
	$working_hours_per_week[$row["cer_revenue_id"]] = $row["cer_revenue_total_workinghours_per_week"];

	
	if($tmp_fy2 > 0 and $row["cer_revenue_year"] == $tmp_fy2)
	{
		if($tmp_fm2 < 12)
		{
			$number_of_months[$row["cer_revenue_id"]] = date("F", mktime(0, 0, 0, $tmp_fm2, 10)) . " to December (" . (13-$tmp_fm2) . ")";
		}
		else
		{
			$number_of_months[$row["cer_revenue_id"]] = "December (1)";
		}
	}
	elseif($row["cer_revenue_year"] == $tmp_fy and $row["cer_revenue_year"] == $tmp_ly)
	{
		$number_of_months[$row["cer_revenue_id"]] = date("F", mktime(0, 0, 0, $tmp_fm, 10)) . " to " .  date("F", mktime(0, 0, 0, $tmp_lm, 10)) . " (" . ($tmp_lm - $tmp_fm + 1) . ")";
		
	}
	elseif($row["cer_revenue_year"] == $tmp_fy)
	{
		
		if($tmp_fm < 12)
		{
			$number_of_months[$row["cer_revenue_id"]] = date("F", mktime(0, 0, 0, $tmp_fm, 10)) . " to December (" . (13-$tmp_fm) . ")";
		}
		else
		{
			$number_of_months[$row["cer_revenue_id"]] = "December (1)";
		}
	}
	elseif($row["cer_revenue_year"] == $tmp_ly)
	{
		if($tmp_lm > 1)
		{
			$number_of_months[$row["cer_revenue_id"]] = "January to " . date("F", mktime(0, 0, 0, $tmp_lm, 10)) . "(" . $tmp_lm . ")";
		}
		else
		{
			$number_of_months[$row["cer_revenue_id"]] = "January (1)";
		}
	}
	else
	{
		$number_of_months[$row["cer_revenue_id"]] = "January to December (12)";
	}


	if($tmp_fy2 > 0 
		and $row["cer_revenue_year"] < $tmp_fy2
		and $row["cer_revenue_quantity_watches"] == 0 
		and $row["cer_revenue_total_days_open_per_year"] == 0)
	{
		$exclude_some_years = true;
	}
}


$sql_list1 = "select cer_revenue_id, cer_revenue_year, " .
             "cer_revenue_quantity_watches, cer_revenue_aveargeprice_watches, " . 
			 "cer_revenue_quantity_jewellery, cer_revenue_aveargeprice_jewellery, " .
             "cer_revenue_watches, cer_revenue_jewellery, " . 
			 " cer_revenue_customer_service " .
             "from cer_revenues ";

$list1_filter = "cer_revenue_project = " . param("pid") . " and cer_revenue_cer_version = 0 ";



if($tmp_fy2 > 0 and $exclude_some_years == true)
{
	$list1_filter .=  " and cer_revenue_year >= " . $tmp_fy2;
}


//get list_totals
$list_totals = array();
$list_totals[1] = 0;
$list_totals[2] = 0;
$list_totals[3] = 0;
$list_totals[4] = 0;
$list_totals[5] = 0;
$list_totals[6] = 0;
$list_totals[7] = 0;
$list_totals[8] = 0;
$list_totals[9] = 0;
$list_totals[10] = 0;
$list_totals[11] = 0;

$sql = $sql_list1 . " where " . $list1_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$list_totals[1] = $list_totals[1] + $row["cer_revenue_quantity_watches"];
	$list_totals[2] = $list_totals[2] + $row["cer_revenue_watches"];
	$list_totals[3] = $list_totals[3] + $row["cer_revenue_quantity_jewellery"];
	$list_totals[4] = $list_totals[4] + $row["cer_revenue_jewellery"];


	$list_totals[7] = $list_totals[7] + $row["cer_revenue_customer_service"];

}

$list_totals[8] = array_sum($total_gross_sales_values);
$list_totals[9] = array_sum($total_net_sales_values);

//prepare sql for list 2
$sql_list2 = "select cer_expense_year, cer_expense_amount, cer_expense_type_name " .
             "from cer_expenses " . 
			 "left join cer_expense_types on cer_expense_type_id = cer_expense_type ";

$list2_filter = "cer_expense_project = " . param("pid") . 
                " and cer_expense_cer_version = 0 " . 
				" and cer_expense_type = 14";


// get all expenses for for each year
$years = array();
$year_amounts = array();

$sql  = "select cer_expense_type, cer_expense_year, cer_expense_amount " .
		"from cer_expenses " .
		"where cer_expense_type = 14 " . 
		"and cer_expense_project = " . param("pid") . 
		" and cer_expense_cer_version = 0 ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$years[$row["cer_expense_year"]] = $row["cer_expense_year"];
	$year_amounts[$row["cer_expense_year"]] = $row["cer_expense_amount"];
}

$has_interco = check_interco_contribution(param("pid"));



//check if business plan can be edited
require_once("include/check_if_business_plan_is_editable.php");


/********************************************************************
    build form
*********************************************************************/
$form = new Form("cer_basicdata", "cer_basicdata");
include("include/project_head.php");

$form->add_hidden("pid", param("pid"));



$form->add_section("Revenue Parameters");

if(($can_edit_business_plan == true and $project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer") and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") and $project["project_state"] != 2))
{
	$form->add_edit("cer_basicdata_customer_service", "Customer Service/Accessories in % of gross sales", 0, $cer_basicdata["cer_basicdata_customer_service"], TYPE_DECIMAL, 12,2, 1, "customer_service");
	$form->add_edit("cer_basicdata_sales_reduction", "Sales Reduction in % of gross sales", 0, $cer_basicdata["cer_basicdata_sales_reduction"], TYPE_DECIMAL, 12,2,1, "sales_reduction");

	if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
	{
		$form->add_edit("cer_basicdata_wholesale_margin", "Wholesale Margin in %", 0, $cer_basicdata["cer_basicdata_wholesale_margin"], TYPE_DECIMAL, 12,2, 1, "whole_sale_margin");
	}
	else
	{
		$form->add_edit("cer_basicdata_wholesale_margin", "Additional wholesale Margin in %", 0, $cer_basicdata["cer_basicdata_wholesale_margin"], TYPE_DECIMAL, 12,2, 1, "additional_whole_sale_margin");
	}

	$form->add_button("form_save", "Save Data");
}
else
{
	$form->add_label("cer_basicdata_customer_service", "Customer Service/Accessories in %", 0, $cer_basicdata["cer_basicdata_customer_service"]);
	$form->add_label("cer_basicdata_sales_reduction", "Sales Reduction in %", 0, $cer_basicdata["cer_basicdata_sales_reduction"]);

	if($form_type == "CER" or ($form_type == "AF" and $project["project_postype"] == 1))
	{
		$form->add_label("cer_basicdata_wholesale_margin", "Wholesale Margin in %", 0, $cer_basicdata["cer_basicdata_wholesale_margin"]);
	}
	else
	{
		$form->add_label("cer_basicdata_wholesale_margin", "Additional wholesale Margin in %", 0, $cer_basicdata["cer_basicdata_wholesale_margin"]);
	}
}

/********************************************************************
    build list of standard revenues
*********************************************************************/
$list1 = new ListView($sql_list1, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("Sales Revenues in " . $currency["symbol"]);
$list1->set_entity("cer_revenues");
$list1->set_filter($list1_filter);
$list1->set_order("cer_revenue_year");

$list1->add_column("cer_revenue_year", "Year");
$list1->add_text_column("months", "Period",COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_LEFT, $number_of_months);


$list1->add_number_edit_column("cer_revenue_quantity_watches", "Quantity\nWatches", "4",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $quantity_watches);
$list1->add_number_edit_column("cer_revenue_aveargeprice_watches", "Average Price\nWatches in " . $currency["symbol"], "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $aveargeprice_watches);

$list1->add_column("cer_revenue_watches", "Gross Sales\nWatches", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_RIGHT);

$list1->add_number_edit_column("cer_revenue_quantity_jewellery", "Quantity\nWatch Straps", "4",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $quantity_jewellery);
$list1->add_number_edit_column("cer_revenue_aveargeprice_jewellery", "Average Price\nWatch Straps in " . $currency["symbol"], "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $aveargeprice_jewellery);

$list1->add_column("cer_revenue_jewellery", "Gross Sales\nWatch Straps", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_RIGHT);

$list1->add_column("cer_revenue_customer_service", "Gross Sales\nCustomer Service\nAccessories", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list1->add_text_column("total_gross_sales", "Total Gross\nSales Values",COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $total_gross_sales);


$list1->add_text_column("total_net_sales", "Total Net\nSales Values",COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $total_net_sales);

$list1->set_footer("cer_revenue_year", "Total");
$list1->set_footer("cer_revenue_quantity_watches", number_format($list_totals[1] ,0, ".", "'"));
$list1->set_footer("cer_revenue_watches", number_format($list_totals[2] ,0, ".", "'"));
$list1->set_footer("cer_revenue_quantity_jewellery", number_format($list_totals[3] ,0, ".", "'"));
$list1->set_footer("cer_revenue_jewellery", number_format($list_totals[4] ,0, ".", "'"));

$list1->set_footer("cer_revenue_customer_service", number_format($list_totals[7] ,0, ".", "'"));
$list1->set_footer("total_gross_sales", number_format($list_totals[8] ,0, ".", "'"));
$list1->set_footer("total_net_sales", number_format($list_totals[9] ,0, ".", "'"));

if(($can_edit_business_plan == true and $project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer") and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") and $project["project_state"] != 2))
{
	$list1->add_button("list1_save", "Save Values");
}


/********************************************************************
    build list of revenues Marketing contribution
*********************************************************************/
$list2 = new ListView($sql_list2, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list2->set_title("Marketing Contribution in " . $currency["symbol"]);
$list2->set_entity("cer_expenses");
$list2->set_filter($list2_filter);
$list2->set_order('cer_expense_year');


$list2->add_text_column("cer_expense_year", "Year", 0, $years);
$list2->add_text_column("months2", "Period",COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_LEFT, $number_of_months);

$list2->add_column("cer_expense_type_name", "Revenue Type");

if($use_old_cer_forms_before_2013 == true)
{
	$list2->add_number_edit_column("cer_expense_amount", "Amount in " . $currency["symbol"], "12",  COLUMN_ALIGN_RIGHT, $year_amounts);
}
else
{
	$list2->add_text_column("cer_expense_amount", "Amount in " . $currency["symbol"], COLUMN_ALIGN_RIGHT, $year_amounts);
}

if(($can_edit_business_plan == true and $project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer") and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") and $project["project_state"] != 2))
{
	
	if($use_old_cer_forms_before_2013 == true)
	{
		$list2->add_button("list2_save", "Save Values");
	}
}

/********************************************************************
    build list of standard revenues
*********************************************************************/
$list3 = new ListView($sql_list1, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list3->set_title("Other Sales Relevant Information");
$list3->set_entity("cer_revenues");
$list3->set_filter($list1_filter);
$list3->set_order("cer_revenue_year");

$list3->add_column("cer_revenue_year", "Year");
$list3->add_text_column("months3", "Period",COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_ALIGN_LEFT, $number_of_months);


$list3->add_number_edit_column("cer_revenue_total_days_open_per_year", "Total Days\n Open\nper Year", "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $days_open_per_year);

$list3->add_number_edit_column("cer_revenue_total_hours_open_per_week", "Total Hours\n Open\nper Week", "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $opening_hours_per_week);

$list3->add_number_edit_column("cer_revenue_total_workinghours_per_week", "Total Working\nHours per Week \nfor 1 Employee\n (working 100%)", "20",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $working_hours_per_week);


$list3->add_number_edit_column("cer_revenue_customer_frequency", "Customer\nFrequency\nper Day", "12",  COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $customer_frequencies);




if(($can_edit_business_plan == true and $project["order_actual_order_state_code"] < '890' and $cer_basicdata["cer_basicdata_cer_locked"] == 0 and has_access("has_access_to_his_cer") and $project["project_state"] != 2) or (has_access("has_full_access_to_cer") and $project["project_state"] != 2))
{
	$list3->add_button("list3_save", "Save Values");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

$list1->populate();
$list1->process();


$list2->populate();
$list2->process();



$list3->populate();
$list3->process();

if($form->button("form_save") or $list1->button("list1_save") or $list2->button("list2_save") or $list3->button("list3_save"))
{
	$form_saved = 0;
	if($form->validate())
	{
		
		$fields = array();
    
		$value = dbquote($form->value("cer_basicdata_customer_service"));
		$fields[] = "cer_basicdata_customer_service = " . $value;

		$value = dbquote($form->value("cer_basicdata_sales_reduction"));
		$fields[] = "cer_basicdata_sales_reduction = " . $value;

		$value = dbquote($form->value("cer_basicdata_wholesale_margin"));
		$fields[] = "cer_basicdata_wholesale_margin = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_basicdata set " . join(", ", $fields) . 
			   " where cer_basicdata_project = " . param("pid") . 
			   " and cer_basicdata_version = 0 ";
		mysql_query($sql) or dberror($sql);

		$form_saved = 1;
		
	}

	//update revenues
	$sql = "select * from cer_revenues " . 
		   "where cer_revenue_project = " . param("pid") .
		   " and cer_revenue_cer_version = 0 ";
	
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$fields = array();
					
		
		$value = $row["cer_revenue_quantity_watches"]*$row["cer_revenue_aveargeprice_watches"] + $row["cer_revenue_quantity_jewellery"]*$row["cer_revenue_aveargeprice_jewellery"];
		$value = $value * $form->value("cer_basicdata_customer_service") / 100;
				
		$value = dbquote($value);
		$fields[] = "cer_revenue_customer_service = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_revenues set " . join(", ", $fields) . 
			" where cer_revenue_id = " . $row["cer_revenue_id"] . 
			" and cer_revenue_cer_version = 0 ";
		mysql_query($sql) or dberror($sql);
	}

	
	$v1 = $list1->values("cer_revenue_quantity_watches");
	$v2 = $list1->values("cer_revenue_aveargeprice_watches");
	$v3 = $list1->values("cer_revenue_quantity_jewellery");
	$v4 = $list1->values("cer_revenue_aveargeprice_jewellery");
	

	foreach ($v1 as $key=>$value)
    {
        
            $fields = array();
    
            $value = dbquote($v1[$key]);
			$fields[] = "cer_revenue_quantity_watches = " . $value;

			$value = dbquote($v2[$key]);
			$fields[] = "cer_revenue_aveargeprice_watches = " . $value;

			$value = dbquote($v3[$key]);
			$fields[] = "cer_revenue_quantity_jewellery = " . $value;

			$value = dbquote($v4[$key]);
			$fields[] = "cer_revenue_aveargeprice_jewellery = " . $value;


			$value = dbquote($v1[$key]*$v2[$key]);
			$fields[] = "cer_revenue_watches = " . $value;

			$value = dbquote($v3[$key]*$v4[$key]);
			$fields[] = "cer_revenue_jewellery = " . $value;

			
			$value = $v1[$key]*$v2[$key] + $v3[$key]*$v4[$key];
			$value = $value * $form->value("cer_basicdata_customer_service") / 100;
			
				
			$value = dbquote($value);
			$fields[] = "cer_revenue_customer_service = " . $value;

            $value1 = "current_timestamp";
            $fields[] = "date_modified = " . $value1;
    
            if (isset($_SESSION["user_login"]))
            {
                $value1 = $_SESSION["user_login"];
                $fields[] = "user_modified = " . dbquote($value1);
            }
       
            $sql = "update cer_revenues set " . join(", ", $fields) . 
				" where cer_revenue_id = " . $key . 
				" and cer_revenue_cer_version = 0 ";
            mysql_query($sql) or dberror($sql);
	}

	//*change of calculation model in 2012
	$calc_model = 2012;
	if($project["order_date"] < '2012-01-24' and $project["order_actual_order_state_code"] >= 840)
	{
		$calc_model = 2008;
	}

	$result = update_cost_of_products_sold(param("pid"), $calc_model);
	
	if($apply_new_version_of_rental_tool == 0)
	{
		$result = update_turnoverbased_rental_cost_version2013(param("pid"), $cer_basicdata["cer_basicdata_add_tob_rents"], $cer_basicdata["cer_basicdata_tob_from_net_sales"]);
	}
	else
	{
		$result = update_turnoverbased_rental_cost_version2014(param("pid"));
	}
	

	if($use_old_cer_forms_before_2013 == true)
	{
		
		$v1 = $list2->values("cer_expense_year");
		$v2 = $list2->values("cer_expense_amount");

		foreach ($v1 as $key=>$value)
		{
			
				$fields = array();
		
				$value = dbquote($v2[$key]);
				$fields[] = "cer_expense_amount = " .$value;

				$value1 = "current_timestamp";
				$fields[] = "date_modified = " . $value1;
		
				if (isset($_SESSION["user_login"]))
				{
					$value1 = $_SESSION["user_login"];
					$fields[] = "user_modified = " . dbquote($value1);
				}
		   
				$sql = "update cer_expenses set " . join(", ", $fields) . 
					" where cer_expense_type = 14  " . 
					" and cer_expense_project = " . param("pid") .
					" and cer_expense_cer_version = 0 " . 
					" and cer_expense_year = " . dbquote($v1[$key]);
				mysql_query($sql) or dberror($sql);
		}


		//*change of calculation model in 2012
		$calc_model = 2012;
		if($project["order_date"] < '2012-01-24' and $project["order_actual_order_state_code"] >= 840)
		{
			$calc_model = 2008;
		}

		$result = update_cost_of_products_sold(param("pid"), $calc_model);
		
		if($apply_new_version_of_rental_tool == 0)
		{
			$result = update_turnoverbased_rental_cost_version2013(param("pid"), $cer_basicdata["cer_basicdata_add_tob_rents"], $cer_basicdata["cer_basicdata_tob_from_net_sales"]);
		}
		else
		{
			$result = update_turnoverbased_rental_cost_version2014(param("pid"));
		}
	}


	$v1 = $list3->values("cer_revenue_customer_frequency");
	$v2= $list3->values("cer_revenue_total_days_open_per_year");
	$v3= $list3->values("cer_revenue_total_workinghours_per_week");
	$v4= $list3->values("cer_revenue_total_hours_open_per_week");


	foreach ($v1 as $key=>$value)
    {
        
            $fields = array();
    
            
			$value = dbquote($v1[$key]);
			$fields[] = "cer_revenue_customer_frequency = " . $value;

			$value = dbquote($v2[$key]);
			$fields[] = "cer_revenue_total_days_open_per_year = " . $value;

			$value = dbquote($v3[$key]);
			$fields[] = "cer_revenue_total_workinghours_per_week = " . $value;

			$value = dbquote($v4[$key]);
			$fields[] = "cer_revenue_total_hours_open_per_week = " . $value;

            $value1 = "current_timestamp";
            $fields[] = "date_modified = " . $value1;
    
            if (isset($_SESSION["user_login"]))
            {
                $value1 = $_SESSION["user_login"];
                $fields[] = "user_modified = " . dbquote($value1);
            }
       
            $sql = "update cer_revenues set " . join(", ", $fields) . 
				" where cer_revenue_id = " . $key . 
				" and cer_revenue_cer_version = 0 ";
            mysql_query($sql) or dberror($sql);
	}
	if($apply_new_version_of_rental_tool == 0)
	{
		$result = update_turnoverbased_rental_cost_version2013(param("pid"), $cer_basicdata["cer_basicdata_add_tob_rents"], $cer_basicdata["cer_basicdata_tob_from_net_sales"]);
	}
	else
	{
		$result = update_turnoverbased_rental_cost_version2014(param("pid"));
	}

	
	if($form_saved == 1)
	{
		if($apply_new_version_of_rental_tool == 0)
		{
			$result = update_turnoverbased_rental_cost_version2013(param("pid"), $cer_basicdata["cer_basicdata_add_tob_rents"], $cer_basicdata["cer_basicdata_tob_from_net_sales"]);
		}
		else
		{
			$result = update_turnoverbased_rental_cost_version2014(param("pid"));
		}
		$link = "cer_application_revenues.php?pid=" . param("pid");
		redirect($link);
	}

}

  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("cer_projects");


require "include/project_page_actions.php";


$page->header();
if($form_type == "INR03")
{
	$page->title("INR-03 - Retail Furniture in Third-party Store: Revenues");
}
else
{
	$page->title("Business Plan: Revenues");
}
require_once("include/tabs2016.php");
$form->render();

$list1->render();

//new CER integration in 2013
//no need for interco anymore
//if($has_interco)
//{
//	echo "<br />";
//	echo "<br />";
//	$list2->render();
//}


echo "<br />";
echo "<br />";
$list3->render();

?>

<div id="customer_service" style="display:none;">
    Customer service in percent of gross sales
</div>

<div id="sales_reduction" style="display:none;">
    Sales reduction in percent of gross sales
</div>

<div id="whole_sale_margin" style="display:none;">
    Wholesale margin in percent on costs of products sold
</div> 

<div id="additional_whole_sale_margin" style="display:none;">
    Additional margin should only be introduced if the POS is an agent POS because a third party won't earn any wholesale margin.
</div> 

<?php

require "include/footer_scripts.php";
$page->footer();

?>