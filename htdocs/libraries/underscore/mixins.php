<?php
/**
 * This file contains our custom mixins for underscore php
 */

/**
 * Pick a key or array of keys out of an array
 * 
 * Examples:
 * __::pick(array('id' => 4, 'firstname' => 'Bob'), 'firstname')
 * // 'Bob'
 *
 * __::pick(array('id' => 4, 'firstname' => 'Bob', 'lastname' => 'Builder'), array('firstname', 'lastname'))
 * // array('firstname' => 'Bob', 'lastname' => 'Builder')
 */
__::mixin(array(
  'pick' => function($arr, $key) {
    if (is_array($key)) {
      return array_intersect_key((array) $arr, array_flip($key));
    }
    if (is_array($arr) && array_key_exists($key, $arr)) {
      return $arr[$key];
    }
    else {
      return null;
    }
  }
  ));

/**
 * A recursive version of __::map() that preserves array keys
 */
__::mixin(array(
  'mapRecursive' => function($arr, $func) {
      return filter_var($arr, FILTER_CALLBACK, array('options' => $func));
    }
  ));