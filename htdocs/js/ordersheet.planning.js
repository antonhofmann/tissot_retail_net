
function sum(list) {
		
	var sum = 0;
	for (var i = 0; i < list.length; i++) {
		sum += list[i];
	}
	
	return sum;
}

$(document).ready(function() {

	var spreadsheet,
		application = $('#application').val(),
		controller = $('#controller').val(),
		action = $('#action').val(),
		ordersheet = $('#ordersheet').val(),
		formFilters = $('#formFilters'),
		readonly = $('#readonly').val() || 'false',
		checkApprovedQuantities = $('#check_approved_quantities').val(),
		fixedNotSelectable = ($('#fixedNotSelectable').val()) ? false : true,
		dataindex = [];
	
	// load
	retailnet.loader.init();
	retailnet.loader.show();
	
	// load data
	var response = retailnet.ajax.request('/applications/helpers/ordersheet.planning.data.php', $('form.request, #filters').serialize());
	
	
	if (response) {
		
		var grid = retailnet.json.normalizer(response.data) || {};
		
		var spreadsheet = new Spreadsheet('#spreadsheet', grid, {
			readonly: false,
			autoHeight: false,
			fixedPartsNotSelectable: true,
			hiddenAsNull: true,
			fixTop: response.top,
			fixLeft: response.left,
			merge: response.merge ? eval(response.merge) : {},
			onModify: function(data) {
				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
				updateSpreadsheet(data);
			}
		});
		
		// add focus
		spreadsheet.focus();

		// hide grid
		retailnet.loader.hide();
	}


	// Data history indexing
	$(".spreadsheet .main .cel").each(function() {
		var id = $(this).attr('id');
		var index = id.substr(12);
		var value = $(this).text();
		dataindex[index] = (value) ? Number(value) : 0;
	});
	
	// pos/material tooltip
	$('.infotip').tipsy({
		html: true,
		gravity: 'sw',
		live: true,
		title: function() {
			
			var id = $(this).attr('data');
			var section = $(this).attr('rel');
			var selector = section+'-'+id;
			var elem = $('#'+selector);

			if (elem.text().length) {
				return elem.html();
			} else {
				$('.image-loader').trigger('load-data', [this]);
				return $('.image-loader').html();
			}
		}
	});

	// load tooltip content from ajax request
	$('.image-loader').on('load-data', function(event, tooltip) {

		var id = $(tooltip).attr('data');
		var section = $(tooltip).attr('rel');
		var selector = section+'-'+id;

		$.ajax({
			url: '/applications/helpers/ajax.tooltip.php',
			type: 'GET',
			data: { 
				application: application,
				section: section,
				id: id 
			},
			async: true,
			cache: false,
			dataType: 'json',
			success: function(response) {
				if(response && response.content) {
					
					$('.tooltips-container').append('<div id="'+selector+'" >'+response.content+'</div>');
					
					if ($(tooltip).is(':hover')) {
						$(tooltip).tipsy("show");
					}
				}
			}
		});
	});


	// show/hide postype group
	//$('.row-caption.postype').click(function() {
	$(document).delegate('.row-caption.postype', 'click', function() {

		if ($('.row-caption.postype').length > 2) {

			var self = $(this);
			var type = $('span', self).attr('type');
			var row = $('span', self).attr('row');
			var rows = $('.type_'+type);
			
			if (rows.length > 2) {
				
				//self.toggleClass('close');
				$('.spreadsheet-'+row+' td').toggleClass('close');
				$('.arrow_'+type).toggleClass('arrow-down').toggleClass('arrow-right');

				var data = [];

				$.each(rows, function(i,elem) {
					data.push($(elem).attr('row'));
				});

				data.splice(0,2);
				
				if (self.hasClass('close')) {
					spreadsheet.hideRows(data);
				} else {
					spreadsheet.showRows(data);
				}
			}
		}
		
	});


	// submit spreadsheet
	$('.apply').click(function(event) {

		event.preventDefault();

		var self = $(this);
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

		$.ajax({
			type: 'post',
			url: $(this).attr('href'),
			dataType: 'json',
			data: $('form.request').serialize(),
			beforeSend: function() {
				retailnet.loader.show();
			},
			async: true,
			cache: false,
			success: function(xhr) {
				
				retailnet.loader.hide();

				// reload page on success
				if (xhr && xhr.response) {

					if (self.hasClass('popup_close')) {
						
						if (window.opener) {
							window.opener.location.reload();
						}
						
						window.close();
						
					} else {
						window.location.reload();
					}
				}

				// show request message
				if (xhr && xhr.message) {
					$.jGrowl(xhr.message, { life: 5000, theme: 'error' });
				}
			}
		});

		return false;
	});


	// dialog buttons
	//$('.dialog').click(function(event) {
	$(document).delegate('.dialog', 'click', function(event) {
		
		event.preventDefault();
		var elem = '#'+$(this).attr('id');
		var href = $(this).attr('href');
	
		// assign button link and show modal box
		$(elem+'_apply').attr('href', href);
		modalBox(elem+'_dialog');
	});

	// close modal screen
	$('.cancel').click(function(event) {
		event.preventDefault();
		$.fancybox.close();
		return false;
	});	


	//remove warehouse dialog
	$('.remove-warehouse').click(function(event) {
	
		event.preventDefault();
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
	
		var warehouse = $(this).attr('data');
	
		if (warehouse) {
			
			$.ajax({
				url: '/applications/helpers/ordersheet.warehouse.php',
				dataType: 'json',
				async: true,
				cache: false,
				data: {
					application: application,
					ordersheet: ordersheet,
					warehouse: warehouse,
					section: 'check'
				},
				success: function(xhr) {
					if (xhr && xhr.response) {
						$('#remove_warehouse_apply').attr('data', warehouse);
						modalBox('#remove_warehouse_dialog');
					}
					else if (xhr && xhr.message) {
						$.jGrowl(xhr.message, { life:5000, theme:'warning' });
					}
				}
			});
		}
	});

	// add new warehouse
	$(document).delegate('#add_warehouse_apply', 'click', function(event) {
	
		event.preventDefault();
		
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
	
		if ($(this).hasClass('disabled')) {
			return false;
		} else {
			if (!$('#new_warehouse').val() && !$('#address_warehouse_id').val()) {
				$.jGrowl('Please select one company warehouse or add new one.', { sticky:true, theme:'error'});
			} else {
				$('#warehouseForm').submit();
			}
		}
	});
	
	// cancel add new warehouse
	$('#add_warehouse_cancel').click(function(event) {
		event.preventDefault();
		$('#warehouse_name').val($('#warehouse_name').attr('title')).addClass('silver');
		$('#add_warehouse_apply').addClass('disabled').removeAttr('href');
		$.fancybox.close();
		return false;
	});

	// remove warehouse
	$('#remove_warehouse_apply').click(function(event) {

		event.preventDefault();
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

		$.ajax({
			url: '/applications/helpers/ordersheet.warehouse.php',
			dataType: 'json',
			async: true,
			cache: false,
			data: {
				application: application,
				ordersheet: ordersheet,
				warehouse: $(this).attr('data'),
				section: 'delete'
			},
			success: function(xhr) {
				if (xhr.response) {
					window.location.reload();
				} else if (xhr.message) {
					$.jGrowl(xhr.message, { life:5000, theme: 'error' });
				}
			}
		});
	
		return false;
	});

	// new warehouse name
	$('#warehouse_name').bind({
		click: function() {
			$(this).val('').removeClass('silver');
			$('#add_warehouse_apply').addClass('disabled');
			$('#address_warehouse_id').val('');
		},
		keypress: function() {
			$('#add_warehouse_apply').removeClass('disabled');
		},
		blur: function() {
			$(this).trigger('change');
		},
		change: function() {

			var value = $(this).val();
			var title = $(this).attr('title');
			
			if (value && value!=title) {
				$('#address_warehouse_id').val('');
				$('#add_warehouse_apply').removeClass('disabled');
				$('#new_warehouse').val(value);
			} else {
				$(this).val(title).addClass('silver');
				$('#new_warehouse').val('');
				$('#add_warehouse_apply').addClass('disabled');
			}
		}
	})
	
	//dropdown company warehouses
	$('#address_warehouse_id').change(function() {
		if ($(this).val()) {
			$('#warehouse_name').val('').trigger('change');
			$('#add_warehouse_apply').removeClass('disabled');
		} else {
			$('#add_warehouse_apply').addClass('disabled');
		}
	});
	
	// submit warehouse form
	$('#warehouseForm').submit(function(event) {

		event.preventDefault();
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

		var data = $('#warehouseForm, form.request').serialize();

		$.ajax({
			url: $('#warehouseForm').attr('action'),
			dataType: 'json',
			data: $('#warehouseForm, form.request').serialize(),
			success: function(xhr) {
				
				// reload page on success
				if (xhr && xhr.response) window.location.reload();

				// show request message
				if (xhr && xhr.message) $.jGrowl(xhr.message, { life: 5000, theme: 'error' });
			} 
		});

		return false;		
	});

	//build dropdown UI skins
	$('select', $('#filters')).dropdown();


	// confirmation versions
	$('#version').change(function() {
		var value = $(this).val();
		var url = $('#url').val();
		var link = (value) ? url+'/'+value : url;
		window.location = link;
	});

	// reload page when filters are changed
	$('select', $('#filters')).change(function() {
		$('#filters').submit();
	});

	var popFilter = $('#pop_filter');
	
	popFilter.pop().click(function() {
		$('.icon',this).toggleClass('direction-up');
	});

	popFilter.pop('hide');


	// show modal box
	var modalBox = function(elem) {
		$.fancybox({ 
			'href': elem,
			'autoDimensions': true,
			'autoScale': true,
			'centerOnScroll': true,
			'transitionIn': 'elastic',
			'transitionOut': 'elastic',
			'showCloseButton': false,
			'hideOnOverlayClick': false,
			'scrolling' : false,
			'titleShow' : false,
			'padding' : 0,
			'margin' : 0
		});
	}
	
	// update spreadsheet
	var updateSpreadsheet = function(data) {
		
		if (data) {
				
			$.each(data, function(i,cell) {
				
				if (cell && cell.tag != null) {

					var error = false;
					var message = '';
					var value = Number(cell.value);
					
					var tag = cell.tag.split(';');
					var sectionName = tag[0];
					var itemID = tag[1];
					var refererID = tag[2];
					var materialID = tag[3];
					var celTotal = tag[4];
					var celPlanned = tag[5];
					var celReserve = tag[6];

					
					// check calculated quantity
					if (celTotal && celPlanned && $('#mode_distribution').val()) {
						
						var totalQuantities = tag[5] ? Number(spreadsheet.getValue(celTotal)) : 0;
						var totalPlanned = tag[6] ? Number(spreadsheet.getValue(celPlanned)) : 0;
						
						// check negativ value
						error = (totalQuantities-totalPlanned >= 0) ? false : true;
						
						message = ($('#spreadsheet').hasClass('planning'))
							? 'Total quantity in planning exeeds aproved quantity.'
							: 'Total quantity in distribution exeeds shipped quantity.';
					}
					
					// reduce stock reserve
					if (!error && celReserve) { 

						var totalReserve = Number(spreadsheet.getValue(celReserve)); 
						
						if (value) {
							var reserve = totalReserve-value;
						} else {
							var reserve = totalReserve+dataindex[cell.name];
						}
						
						if (reserve >= 0) {
							
							// set stock reserve
							spreadsheet.setValue(celReserve, reserve);
							
							// store quantitie in dataindex
							dataindex[celReserve] = reserve;
							
							// save reserve stock on db
							retailnet.ajax.json('/applications/helpers/ordersheet.item.planning.save.php', {
								application: application,
								controller: controller,
								ordersheet: ordersheet,
								quantity : reserve,
								section: 'stock-reserve',
								material: materialID
							});
							
						} else if (reserve < 0) { 
							error = true;
							message = 'Total quantity in planning exeeds stock reserve.';
						}
					}
					
					if (error) {
	
						spreadsheet.setValue(cell.name, dataindex[cell.name] )
							
						$.jGrowl(message, { life: 5000, theme: 'error'});
						
					} else if (sectionName) { 

						$.ajax({
							url: '/applications/helpers/ordersheet.item.planning.save.php',
							dataType: 'json',
							async: true,
							cache: false,
							data: { 
								application: application,
								controller: controller,
								ordersheet: ordersheet,
								quantity : value,
								section: sectionName, 
								id: itemID,
								referer: refererID,
								material: materialID
							},
							success: function(xhr) {
								if (xhr && xhr.id) { 
	
									// store quantitie in dataindex
									dataindex[cell.name] = value;
									
									// store cell tag in spreadsheet
									// new tag content new inserted ID
									spreadsheet.setValue(cell.name, {
										tag: tag[0]+';'+xhr.id+';'+tag[2]+';'+tag[3]+';'+tag[4]+';'+tag[5]+';'+tag[6]
									});
								}
							}
						});
					}
				}
			});
		}
	}
	
});