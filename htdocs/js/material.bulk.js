$(document).ready(function() {

	var bulkform = $("#bulkform"),
		mailform = $("#mailform"),
		buttonSubmit = $('#update'),
		buttonSendMail = $('#sendmail'),
		actions = $('#action'),
		mastersheets = $('#mps_mastersheet_id'),
		ordersheets = $('#ordersheets'),
		items = $('#item_id'),
		materiial = $('#mps_material_id'),
		data, value, action, content, request;

	// loader insance
	retailnet.loader.init();

	
	// dropdown: actions
	actions.change( function() {

		// remove all error classes
		$('div', bulkform).children().removeClass('error');

		// on page load: hide ordersheets block
		mastersheets.show().val('').attr('disabled', true);
		items.empty();

		buttonSubmit.hide();
		$('.ordersheets, .select_all, .proposed_quantity, .item_id, .send_mail_clients').hide();
		$('.noresult').remove();
		
		if ($(this).val()) {

			mastersheets.attr('disabled', false);

			var data = bulkform.serializeArray();
			data.push({name: 'section', value: 'mastersheets'});

			retailnet.ajax.json('/applications/helpers/material.ajax.php', data).done(function(xhr) {
				
				if (xhr) {
					if (xhr.response) {
						mastersheets.html(xhr.content);
						buttonSubmit.show();
					} else {
						mastersheets.hide();
						$('.mps_mastersheet_id .-element').append(xhr.content);
					}
				}
			});
		}
		
	}).trigger('change');

	
	// dropdown: master sheets
	mastersheets.change( function() {

		var value = $(this).val();
		var action = actions.val();

		// hide dynamiv placeholders
		$('.ordersheets, .select_all, .proposed_quantity, .item_id, .send_mail_clients').slideUp();
		$('.noresult').remove();

		// empty blocks
		ordersheets.empty()
		items.show().empty();

		if (value) {

			var data = bulkform.serializeArray();
			var section = (action==3) ? 'items' : 'ordersheets'
			data.push({name: 'section',value: section});

			// show wait screen
			retailnet.loader.show();

			retailnet.ajax.json('/applications/helpers/material.ajax.php', data).done(function(xhr) {
				
				if (xhr) {

					if (action==3) {
	
						if (xhr.response) items.html(xhr.content);
						else {
							items.hide();
							$('.item_id .-element').append(json.content);
						}
	
						$('.item_id').slideDown('slow');
					} 
					else {
	
						ordersheets.html(xhr.content);
						$('.ordersheets').slideDown('slow');
	
						if (xhr.response) {
							$('.select_all, .send_mail_clients').slideDown('slow');
						}
	
						if (action==1 && xhr.response) {
							$('.proposed_quantity').slideDown();
						}
					}	
				}
			}).complete(function() {
				
				// hide wait screen
				retailnet.loader.hide();
			});	
			
		} else {
			
			$('.ordersheets, .select_all, .proposed_quantity, .item_id, .send_mail_clients').slideUp();
		}
	});

	// dropdown: items
	items.change( function() {

		var value = $(this).val();

		// reset ordersheets block
		$('.ordersheets, .select_all, .send_mail_clients').slideUp();

		ordersheets.empty();

		if (value) {

			var data = bulkform.serializeArray();
			data.push({name: 'section',value: 'ordersheets'});

			// loader
			retailnet.loader.show();

			retailnet.ajax.json('/applications/helpers/material.ajax.php', data).done(function(xhr) {

				if (xhr) {

					if (xhr.content) {
						ordersheets.html(xhr.content);
						$('.ordersheets').slideDown('slow');
					}

					if (xhr.response) {
						$('.select_all, .send_mail_clients').slideDown('slow');
					} else {
						$('.select_all, .send_mail_clients').slideUp();
					}
				}
			}).complete(function() {
				
				// hide wait screen
				retailnet.loader.hide();
			});	
		}			
	});

	// checkbox: select all
	$(document).delegate("#select_all", "change", function() {
		var caption = $(this).next('.-caption');
		var label = caption.attr('data');
		var checked = ($(this).is(':checked')) ? true : false;
		$('input.ordersheet').attr('checked', checked);
		caption.attr('data', caption.text()).text(label);
	});

	$(document).delegate(".select_all .-caption", "click", function() {
		$('#select_all').trigger('click');
	});
	
	// checkbox: is all selected
	$(document).delegate("input.ordersheet", "click", function() {
		var checkboxes = $('input.ordersheet').length;
		var checked = $('input.ordersheet:checked').length;
		var checked = (checked==checkboxes) ? true : false;
		$('#select_all').attr('checked', checked);
	});

	// form: tooltips
	$('.-tooltip[title]').qtip({
		position: {corner: {target:'topRight', tooltip:'leftTop'}},
		style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
	});

	// button: close modal screen
	$('#cancel').click(function() {
		$.fancybox.close();
		return false;
	});

	// action button: update
	$('#update').click( function(e) {

		e.preventDefault();

		var action = actions.val();
		var error = false;
		
		// validata bulk form
		var validata = bulkform.validationEngine('validate');
		
		// send mail checkbox
		var sendmail = $("#send_mail_clients").is(':checked') ? true : false;

		// remove all notofocations
		retailnet.notification.hide();

		// remove error clases
		items.removeClass('error');

		// for item replacement, replaced item is required
		if (validata && !items.val() && action==3) {

			error = true;

			// show notification
			retailnet.notification.error('Please, select item for replace.');

			items.addClass('error');
		}
		
		// sen mails to order sheets, min one order sheet must be selected
		if (sendmail && $('input.ordersheet:checked').length==0) {

			error = true;

			// show notification
			retailnet.notification.error('Please, select at least one order sheet.');
		}

		if (!error && validata) {

			// send mail to order sheets owners
			if (sendmail) {
				
				if ($('#mail_template_view_modal').val() == 1) {

					// show mail modal screen
					retailnet.modal.show('#modalbox'); 
					
				} else {

					// submit mail form
					mailform.submit();
				}
			} else { 
				
				// show splash screen
				retailnet.loader.show();
				
				// submit bulk form
				bulkform.submit();
			}
		}

		return false;
	});

	
	// action button: sendmail
	buttonSendMail.click(function(e) {

		e.preventDefault();
		
		// remove all notofocations
		retailnet.notification.hide();

		// sendmail url
		var url = mailform.attr('action');
		var data = $('#bulkform, #mailform').serialize()
		var error = false;
		var required = $('.required', mailform);
		
		
		if (required.length > 0) {
			
			var failures = $('.required[value=]', mailform);
    		
    		// remove marks
			$('.required', mailform).removeClass('error');
    		
			if (failures.length > 0) {
				
				error = true
				
				// mark required failures
				$('.required[value=]', mailform).addClass('error');
	    		
	    		// show notification
	    		retailnet.notification.error('Red marked fields are required.');
			}	
    	}
		
		if (!error) {
			
			// close modal screen
			retailnet.modal.hide();

			// submit mail form
			mailform.submit();
		}

		return false;
	});
	
	
	// mail form submit
	mailform.submit(function(e) {
		
		e.preventDefault();
		
		var self = $(this);
		var url = self.attr('action');
		var data = $('#mailform, #bulkform').serialize();
		
		// show wait screen
		retailnet.loader.show();
		
		retailnet.ajax.json(url,data).done(function(xhr) {
			
			if (xhr.response) {
				
				// reload page
				if (xhr && xhr.reload) {
					window.location.reload();
	        	}
				
				// show notification
				if (xhr && xhr.notification) {
					retailnet.notification.show(xhr.notification.content, eval(xhr.notification.properties));
				}

				// show wait screen
				retailnet.loader.show();
				
				// submit bulk form
				bulkform.submit();

			} else {
				
				// show notification
				retailnet.notification.error('ERROR: Check form data.');
			}

		}).complete(function() {
			
			// hide splash screen
			retailnet.loader.hide();
		});
		
		return false;
	});

	
	// submit bulk form
	bulkform.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) {

			// hide splash screen
			retailnet.loader.hide();
			
			// remove all notofocations
			retailnet.notification.hide();
			
			if (json && json.redirect) {
				window.location=json.redirect;
			}

			if (json && json.message) {
				
				// show notification
				retailnet.notification.success(json.message);
			}
		}
	});
});	