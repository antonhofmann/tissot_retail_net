$(document).ready(function() {

	var UploadForm = $('#fileupload'),
		Upload_Dir = $('.upload_dir', UploadForm).val(),
		extensions = $('#allowed_extensions', $('#uplForm')).val();
	
	// loader initialise
	retailnet.loader.init();
	
	// Initialize the jQuery File Upload widget:
	UploadForm.fileupload({
        url		: '/applications/helpers/file.uploader.php',
        formData: {
        	upload_dir: Upload_Dir
        }
    });

    // Enable iframe cross-domain access via redirect option:
	UploadForm.fileupload('option', 'redirect', window.location.href.replace(/\/[^\/]*$/, '/fileuploader/cors/result.html?%s'));

    // Load existing files:
	UploadForm.addClass('fileupload-processing');
   
    $.ajax({
        url: UploadForm.fileupload('option', 'url'),
        dataType: 'json',
        context: $('#fileupload')[0],
        data: {
        	upload_dir: Upload_Dir
        }
    }).always(function () {
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
        $(this).fileupload('option', 'done').call(this, $.Event('done'), {result: result});
    });
	

	// show modal screen
	$('.upload-modal').click(function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();

		var self = $(this);
		var target = self.attr('href');
		var options = {};
		
		if (self.attr('modal-width') && self.attr('modal-height')) {
			options.autoDimensions = false;
			options.width = Number(self.attr('modal-width'));
			options.height = Number(self.attr('modal-height'));
		}
		
		if ($('tbody.files tr.template-download', $(target)).length == 0 ) {
			$('button.delete', $(target)).hide();
		}
		
		retailnet.modal.show(target, options);

		return false;

	});
	
	// close modal screen
	$('.modal-close', UploadForm).click(function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();
		
		retailnet.modal.hide();
		
		UploadForm[0].reset();
		
		return false;
	});
	
	// close modal screen
	$('button.start', UploadForm).click(function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();
		
		UploadForm.submit();
		
		return false;
	});
/*	
	// submit modal
	$('.modal-submit').click(function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();
		
		retailnet.notification.hide();
		
		var modalbox = $(this).closest('.modalbox');
		var totalRows = $('.file-row', modalbox).length;
		var row = $('.file-row.cloned:last', modalbox);
		
		// last cloned element
		// set as not mandatory
		if (totalRows > 1 && !$('input.file', row).val()) {
			$('.title', row).removeClass('required').removeClass('validate[required]')
		}

		var validata = UploadForm.validationEngine('validate');
		
		if (validata) {
			UploadForm.submit();
			retailnet.modal.hide();
			retailnet.loader.show();
		} else {
			retailnet.notification.show('ERROR: check red selected fields.', {
				sticky: true,
				theme: 'error'
			});
		}
		
	});

	$('.modalbox').delegate(".required", "change", function() {
		$(this).removeClass('error');
		$(this).toggleClass('error', $(this).val() ? false : true)
	});


	// upload file
	$('.modalbox').delegate('.button.upload-file', 'click', function(e) { 

		e.preventDefault();
		e.stopImmediatePropagation();

		retailnet.notification.hide();
		
		var row = $(this).closest('.file-row'); 

		if (!$('.title.required',row).val()) {

			retailnet.notification.show('Please indicate a file title and select the file again.', {
				theme: 'error'
			});
			
		} else { 

			AjaxFile.open();
		}
		
		return false;
	});

	// remove uploaded file
	$('.modalbox').delegate('.button.remove-file', 'click', function(e) { 

		e.preventDefault();
		e.stopImmediatePropagation();
		
		var row = $(this).closest('.file-row'),
			path = $('.show-file', row).attr('href');

		
		retailnet.ajax.json('/applications/helpers/ajax.file.upload.php', {
			section: 'remove',
			file: path
		}).done(function(xhr) {

			if (xhr) {

				if (xhr.response) {
					row.remove();
				}

				if (xhr.message) {
					retailnet.notification.show(xhr.message);
				}
			}
		});
		
		return false;
	});
*/

});