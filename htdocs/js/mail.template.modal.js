$(document).ready(function() {
	
	$('.modal-mail-open').click(function(event) {
		event.preventDefault();
		$.fancybox.showActivity();
		modal($(this).attr('data-target'));
		$.fancybox.hideActivity();
		return false;
	});
	
	$('.modal-mail-close').click(function(event) {
		event.preventDefault();
		$.fancybox.close();
		return false;
	});
	
	$('.modal-mail-submit').click(function(event) { 
		event.preventDefault();
		$('form', $(this).closest('.modalbox')).submit();
		return false;
	});
	
	// event: focus
	$('input,select,textarea', $('.mail-template-modal')).click(function() {		
		$(this).addClass('onFocus').removeClass('onBlur');
	});
	
	// event: blur
	$('input,select,textarea', $('.mail-template-modal')).blur(function() {		
		$(this).removeClass('onFocus').addClass('onBlur');
	});
	
	// event: change
	$('input,select,textarea', $('.mail-template-modal')).change(function() {		
		$(this).toggleClass('error', ($(this).hasClass('required') && !$(this).val()) ? true : false );
		$(this).removeClass('onFocus').addClass('onBlur');
	});

	$('form.modal-mail-form-submit').submit(function() {
		
		var self = $(this);
		var errors = $('.error', self).length ? true : false;
		
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
		
		if (errors) {
			
			$.jGrowl('Red marked Fields are required.', { 
				sticky: true, 
				theme:'error'
			});
			
		} else {
			
			$.loader();
			
			var request = xhr(self.attr('action'), self.serialize());

			if (request) {

				$.loader('close');
				$.fancybox.close();

				if (request.response && request.redirect) {
					window.location=request.redirect;
				} 

				if (request.message) {
					$.jGrowl(request.message, {
						life: 5000, 
						sticky: (request.response) ? false : true,
						theme: (request.response) ? 'message' : 'error'
					});
				}
			}
		}
		
		return false;
	});

	// load mail form data
	$('form.modal-mail-form-preloader').change(function(event) {
		
		event.preventDefault();
		event.stopPropagation();
		
		var self = $(this);
		var link = $('#populate', self).val();
		var modalbox = self.closest('.modalbox');

		// form data pupullate
		if (link) {

			var data = xhr(link, self.serializeArray());

			if (data) {
				$.each(data, function(field, value) {
					var field = $('.'+field, self);
					if (field.is('textarea')) field.text(value);
					else field.val(value);
				});
			}

			if ($('.mail-modal', self).val()) modal('#'+modalbox.attr('id')); 
			else self.submit();

		} else {
			$.jGrowl('Error: Form data pupulation not posible. ', { 
				sticky: false, 
				theme: 'error'
			});
		}
		
		$.fancybox.hideActivity();
		
		return false;
	});

});