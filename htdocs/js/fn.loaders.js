var xhr = function(url, data) {
	var response;
	$.ajax({
		url: url,
		type: 'post',
		dataType: 'json',
		data: data,
		async: false,
		cache: false,
        success: function(json) { 
        	response = json;           
        }
    });
	return response;
}

var modal = function(href, callback) {
	$.fancybox({ 
		'href': href,
		'autoDimensions':true,
		'autoScale': true,
		'centerOnScroll': true,
		'transitionIn': 'elastic',
		'transitionOut': 'elastic',
		'showCloseButton': false,
		'hideOnOverlayClick': false,
		'scrolling' : false,
		'titleShow' : false,
		'padding' : 0,
		'margin' : 0,
		'onComplete': function() {
			$.fancybox.hideActivity();
		}
	});
}