$(document).ready(function() {

	var dashboardsContainer = $('#dashboardsContainer');
	var dashboardURL = '/applications/helpers/dashboard.ajax.php';
	var widgetURL = '/applications/helpers/widget.ajax.php';
	var Dashboard = [];
	var Gridster = [];
	var Widget = [];
	var Dialog = [];


/******************************************************** widget actions ********************************************************/
	
	var WidgetAction = {
			
		// add new widget
		add: function(widget) {
			
			if (widget) {
				
				var dashboard = $('.gridster.active').attr('data-id');
				
				var xhr = retailnet.ajax.request(widgetURL, {
					section: 'add',
					dashboard: dashboard,
					widget: widget
					
				});

				if (xhr && xhr.redirect) {
					window.location.href = xhr.redirect;
				}

				if (xhr && xhr.reload) {
					window.location.reload();
				}

				if (xhr && xhr.message) {
					BootstrapDialog.alert(xhr.message, {
						title: 'Success',
						live: 2000
					});
				}

				if (xhr && xhr.response) {
					
					// create gridster widget
					if (Gridster[dashboard]) {
						
						Gridster[dashboard].add_widget(
							xhr.content,
							xhr.data.width,
							xhr.data.height, 
							xhr.data.col, 
							xhr.data.row
						);
					}
					
					// load widget content
					if (xhr.data.widget) {
						WidgetAction.load(xhr.data.widget);
					}
					
					return true;
				};
			}
		},

		// save widgets properties
		save: function() {

			var dashboard = $('ul.gridster-items', $('.gridster.active'));
			var key = dashboard.attr('data-id');

			if (Gridster[key]) {

				var dataWidgets = Gridster[key].serialize();
	
				if (dataWidgets instanceof Object == true) {
					
					var elem = $('li.gridster-widget', dashboard); 
					var widgets = [];
					
					$.each(dataWidgets, function(i, obj) {
						var id = $(elem[i]).attr('data-id'); 
						widgets[id] = JSON.stringify(obj);
					});
	
					return retailnet.ajax.json(widgetURL, {
						dashboard: key,
						section: 'save',
						widgets: widgets
					});
				}
			}
		},
		
		// load widget content
		load: function(widget) {
			
			var self = $('#widget-'+widget);
			
			return Widget[widget] = $('.panel', self).ajaxContent({
				url: widgetURL,
				refresh: self.attr('data-refresh'),
				namespace: 'panel-body',
				data: {
					section: 'load',
					widget: widget,
					limit: self.attr('data-limit')
				}
			});
		},

		// remove widget
		remove: function(widget) {

			BootstrapDialog.confirm('Are you sure to delete this Widget?', function(result) {

	            if(result && widget) {

	            	var xhr = retailnet.ajax.request(widgetURL, {
						section: 'delete',
						widget: widget
					});
	            	
	            	if (xhr && xhr.response) {

						// find widget dashboard
	            		var elem = $('#widget-'+widget);
						var dashboard = elem.closest('.gridster')
						var key = dashboard.attr('data-id');

						// remove widget from dashboard
						if (Gridster[key]) {
							Gridster[key].remove_widget(elem);
						}
						
						return true;
					}
	            }
	        });
		},

		// save widget theme
		theme: function(elem, theme) {

			retailnet.ajax.json(widgetURL, {
				section: 'theme',
				themename: theme,
				widget: elem.attr('data-id')
			});
		}, 

		// widget expander
		expand: function(elem) {

			var height = elem.attr('data-sizey');

			if (height > 1) {
				elem.attr('data-cols', height);
				height = 1;
			} else {
				height = elem.attr('data-cols');
			}

			var dashboard = $('ul', $('.gridster.active'));
			var key = dashboard.attr('data-id');
			
			Gridster[key].resize_widget(elem, 0, height);

			// toggle caret arrow
			$('.widget-toggler', elem).toggleClass('fa-angle-up').toggleClass('fa-angle-down');
		}
	}
	

/******************************************************** dashboard loader ********************************************************/
	
	$(function() {

		$('.gridster').each(function(i,el) {

			var self = $(this);
			var key = self.attr('data-id');
		
			// create dashboard instane
			Dashboard[key] = $(el).ajaxContent({
				url: dashboardURL,
				data: {
					section: 'load',
					dashboard: key
				},
				overlay: false,
				after: function(response) {

					var dashboard = $("ul", self);
					var namespace = '#' + self.attr('id');

					// create gridster instance
					Gridster[key] = dashboard.gridster({
						namespace: namespace,
						widget_base_dimensions: [50, 50],
						widget_margins: [10, 10],
						min_rows: 20,
						min_cols: 20,
						resize: {
							enabled: true,
							stop: function(e, ui, $widget) {
								WidgetAction.save();
							}
						},
						draggable: {
							handle: '.panel-heading',
							stop: function(e, ui, $widget) {
								WidgetAction.save();
							}
						}
					}).data('gridster');

					// create widgets instance
					$('li', dashboard).each(function(i,el) {
						var widget = $(el).attr('data-id');	
						WidgetAction.load(widget);
					});
				}
			});
		});
	});
	
	
/******************************************************** Dialogs ********************************************************/	
	
	
    Dialog['dashboard'] = new BootstrapDialog({
        title: 'Dashboard',
        message: $('#fromDashboard'),
        autodestroy: false,
        buttons: [
	        {
	            label: 'Close',
	            action: function(dialogRef) {
	                dialogRef.close();
	            }
	        },
	        {
	            id: 'delete',
	            label: 'Delete',
	            icon: 'fa fa-check',
	            cssClass: 'btn-danger',
	            autospin: true,
	            action: function(dialogRef) {
	
	            	var totalWidgets = $('li', $('.gridster.active')).length;
	
	            	var dashboard = $('.gridster.active').attr('data-id');
	
	            	if (dashboard && totalWidgets==0) {
	
	            		dialogRef.enableButtons(false);
	                    dialogRef.setClosable(false);
	
	                    retailnet.ajax.json(dashboardURL, {
	    					section: 'remove',
	    					dashboard: dashboard
	    				}).done(function(xhr) {
	
	    					dialogRef.close();
	
	    					if (xhr && xhr.redirect) {
	    						window.location.href = xhr.redirect;
	        				}
	
	    					if (xhr && xhr.reload) {
	    						window.location.reload();
	        				}
	
	    					if (xhr && xhr.message) {
	    						BootstrapDialog.alert(xhr.message, {
									title: 'Success',
									live: 2000
	        					});
	    					}
	
							// remove tab / tab content
	    					$('#dashtab-'+dashboard).remove();
	    					$('.gridster.active').remove();
	
	    					// activate last existing tabs
	    					$('#dashtabs li:last').addClass('active');
	    					$('#dashtab-contents .gridster:last').addClass('active');
	        			});
	                }
	
	                return false;
	            }
	        },
	        {
	            id: 'save',
	            icon: 'fa fa-check',
	            label: 'Save',
	            cssClass: 'btn-primary',
	            action: function(dialogRef){
	
	                var dashboard = $('#dashboard').val();
	    			var title = $('#dashboardTitle').val();
	
	    			if (!title) {
	    				BootstrapDialog.alert('Please enter Dashboard title.', {
							title: 'Error',
							type: BootstrapDialog.TYPE_DANGER
	        			});
	    			} else {
	
	    				 dialogRef.enableButtons(false);
	                     dialogRef.setClosable(false);
	                     
	    				retailnet.ajax.json(dashboardURL, {
	    					section: 'save',
	    					dashboard: dashboard,
	    					title: title
	    				}).done(function(xhr) {
	
	    					dialogRef.close();
	    					
	    					if (xhr && xhr.redirect) {
	    						window.location.href = xhr.redirect;
	        				}
	
	    					if (xhr && xhr.reload) {
	    						window.location.reload();
	        				}
	
	    					if (xhr && xhr.message) {
	    						BootstrapDialog.alert(xhr.message, {
									title: 'Success',
									live: 2000
	        					});
	    					}
	
	    					if (xhr && xhr.response) {
	    						$('li.active b', $('#dashtabs')).text(title);
	        				}
	    					
	    				}).fail(function() {
	
	    					dialogRef.close();
	    					
	    					BootstrapDialog.alert('Error: Check ajxx URL.', {
								type: BootstrapDialog.TYPE_DANGER
	            			});
	    				});
	    			}
	
	    			return false;
	            }
	        }
        ],
        onshow: function(dialogRef){
        	
        },
        onhide: function(dialogRef){
        	dialogRef.enableButtons(true);
            dialogRef.setClosable(true);
        	$('#dashboard').val('');
			$('#dashboardTitle').val('');
        }
    });
	
	
	Dialog['widget'] = new BootstrapDialog({
        title: 'Widget',
        message: $('#fromWidget'),
        autodestroy: false,
        closable: true,
        onshow: function(dialogRef) {
			dialogRef.getModalDialog().addClass('widgets-modal');
        },
        onhide: function(dialogRef){
        	dialogRef.getModalDialog().removeClass('widgets-modal');
        }
    })
	
	
/*********************************************************** actions ************************************************************/
	
	
	// widget settigns
	dashboardsContainer.on( "mouseover", '.gridster .panel', function(e) {
		$('.panel-actions', $(this)).stop().show();
	}).on( "mouseout", '.gridster .panel', function(e) {
		$('.panel-actions', $(this)).stop().hide();
	});

	
	// show themes box
	dashboardsContainer.on( "click", '.widget-theme', function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();
		
		var self = $(this);
		var picker = $('.theme-box:not(.cloned)').clone(true,true).addClass('cloned');

		$('.theme-box.cloned').remove();
		
		self.parent().toggleClass('active');
		
		picker.prependTo(self.parent()).toggle(500);
	});

	
	// change widget theme
	dashboardsContainer.on('click', '.theme-box span', function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();
		
		var panel = $(this).closest('.panel');
		var theme = $(this).attr('data-theme');
		
		panel.removeClass(function (index, css) {
		    return (css.match (/\panel\-\S+/g) || []).join(' ');
		});
		
		panel.addClass(theme);

		// save theme in db
		var elem = $(this).closest('li');

		// save theme in db
		WidgetAction.theme(elem, theme);
		
		// remove all cloned elements
		$('.theme-box.cloned').remove();
	});

	
	// widget refresh
	dashboardsContainer.on('click', '.widget-refresh', function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();
		
		var elem = $(this).closest('li');
		var widget = elem.attr('data-id');

		if (Widget[widget]) {
			Widget[widget].refresh({
				limit: elem.attr('data-limit')
			});
		}
	});
	
	
	// widget: add
	$(document).on('click', '.add-widget', function(e) {

		e.preventDefault();
		e.stopImmediatePropagation();
		
		var widget = $(this).attr('data-id');
		
		if (widget) {
			WidgetAction.add(widget)
			$('#widget-button-'+widget).removeClass('add-widget btn-success').addClass('widget-remover btn-danger').text('Remove');
		}

		return false;
	});
	
	
	// widget remove
	$(document).on( "click", '.widget-remover', function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();
		
		var widget = $(this).attr('data-id');

		if (widget) {
			WidgetAction.remove(widget);
			$('#widget-button-'+widget).removeClass('widget-remover btn-danger').addClass('add-widget btn-success').text('Add');
		}
	});


	// toggle (expand/collapse) widget
	dashboardsContainer.on('click', '.widget-toggler', function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();
		
		var elem = $(this).closest('li');
		
		WidgetAction.expand(elem);
	});
	
	
	// dashboard title change
	$('#dashtabs').on('click', '.dashboard-settings', function() { 

		var dashboard = $(this).closest('li'),
			key = dashboard.attr('data-id'),
			title = $('b', dashboard).text(),
			show, button, name, dataWidgets;

		// assign dashboard data
		$('#dashboard').val(dashboard);
		$('#dashboardTitle').val(title);

		// get delete button
		Dialog['dashboard'].realize();
		btnDelete = Dialog['dashboard'].getButton('delete');
		
		if (btnDelete) {

			btnDelete.hide();

			if (Gridster[key]) {
				
				dataWidgets = Gridster[key].serialize();  
				
				if (dataWidgets.length==0) {
					btnDelete.show();
				}
				
			} else {
				btnDelete.show();
			}
		}
		
		Dialog['dashboard'].open();
	});
	
	
	// change adnew dropdown
	$('#addnew li').click(function(e) {

		e.preventDefault();
		e.stopImmediatePropagation();

		var self = $(this);
		var m = $(this).attr('data-target');
		
		if (Dialog[m]) {
			
			// enable or disable add buttons
			$('.gridster-widget').each(function(i,elem) {
				
				var widget = $(this).attr('data-id')
				
				//$('#addwidget-'+widget).addClass('disabled');
				
			});

			Dialog[m].realize();
			
			var btnDelete = Dialog[m].getButton('delete');
			
			if (btnDelete) btnDelete.hide();

			Dialog[m].open();
			
			self.closest('.dropdown-btn').removeClass('open');
		}

		return false;
	});
	
	
	// appender "more"
	dashboardsContainer.on('click', '.appender', function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();
		
		var widget = $(this).closest('.gridster-widget');
		var key = widget.attr('data-id');

		if (Widget[key]) {
			
			var limit = parseInt(widget.attr('data-limit'));
			limit = limit + 1;
			
			widget.attr('data-limit', limit)
			
			Widget[key].settings({
				appender: 'tbody.appender-content'
			});
			
			Widget[key].append({
				limit: limit
			});
		}
		
		return false;
	});
});