function sum(list) {
	var sum = 0;
	for (var i = 0; i < list.length; i++) { sum += list[i]; }
	return sum;
}

$(document).ready(function() {
	
	var spreadsheet;
	var mailReminder = $('#reminder-form form');
	var formFilters = $('form.filters');
	
	var options = {
		readonly: true,
		autoHeight: false,
		fixedPartsNotSelectable: true,
		hiddenAsNull: true,
		fixTop: 3,
		fixLeft: 1
	}
	
	// loader instance
	retailnet.loader.init();
	
	// english named months
	Date.prototype.monthNames = [ "January", "February", "March","April","May", "June","July", "August", "September","October", "November", "December"];

	// get month name
	Date.prototype.getMonthName = function() {
		var i = this.getMonth();
		return this.monthNames[i];
	};
	
	// dropdown
	$('.selectbox').dropdown();
	
	// window on resize
	$(window).resize(function() {
		var elem = $('#spreadsheet');
		var elemOffset = elem.offset(); 
		var winHeight = $(window).height();
		var height = winHeight-elemOffset.top;
		elem.height(height);
	}).trigger('resize');

	
	// submit form filters
	formFilters.submit(function(e) {
		
		e.preventDefault();
		
		var state = $('#states').val();
		var data = $('form.request, form.filters').serialize();
		
		// hide remind button for comleted states
		$('#remind').toggleClass('hidden', state==1 ? true : false);
		
		// show wait screen
		retailnet.loader.show();
		
		// load spreadsheet data
		retailnet.ajax.json('/applications/helpers/pos.verification.data.php', data).done(function(xhr) {
			
			var grid = (xhr && xhr.data) ? retailnet.json.normalizer(xhr.data) : {};
			
			if (xhr && xhr.top) {
				options.fixTop = xhr.top;
			}
			
			if (xhr && xhr.left) {
				options.fixLeft = xhr.left;
			}
			
			if (xhr && xhr.merge) {
				options.merge = eval(xhr.merge);
			}
			
			spreadsheet = new Spreadsheet('#spreadsheet', grid, options);
			spreadsheet.focus();
			
		}).complete(function() {
			
			// close wait screen
			retailnet.loader.hide();
		});

		
		return false;
	
	}).trigger('submit');
	

	
	// dropdown on change
	$('select.submit').change(function(event) {
		
		var self = $(this).closest('.dropdown-placeholder');
		var caption = $('option:selected', $(this)).text();
		
		$('span.label', self).text(caption);
		formFilters.submit();
	});

	
	// cancel modal screen
	$('.cancel').click(function(event) {
		event.preventDefault();
		retailnet.modal.hide();
		return false;
	});

	
	// button reset verifications
	$('#reset_verification').click(function(e) {
		
		e.preventDefault();
		
		if ($(this).hasClass('dialog')) {
			
			// show dialog
			retailnet.modal.show('#reset_verification_dialog');
			
		} else {
			
			// submit reset verifications
			$('#reset_verification_apply').trigger('click');
		}
		
		return false;
	});

	
	// reset verification
	$('#reset_verification_apply').click(function(e) {
		
		e.preventDefault();
		
		var url = $('#reset_verification').attr('href');
		var data = $('form.request, form.filters').serialize();
		
		// show wait screen
		retailnet.loader.show();
		
		retailnet.ajax.json(url, data).done(function(xhr) {
			
			if (xhr && xhr.reload) {
				window.location.reload();
        	}
			
			if (xhr && xhr.notification) {
				retailnet.notification.show(xhr.notification.content, eval(xhr.notification.properties));
			}
			
			// reload spredsheet
			formFilters.submit();
			
		}).complete(function() {
			
			// close wait screen
        	retailnet.loader.hide();
		});
		
		return false;
	});
	
	
	
	// button remind
	// load mail content
	$('#remind').click(function(e) {
		
		e.preventDefault();
		
		var self = $(this);
		var target = self.attr('data-target');
		var template =self.attr('data-template');
		
		// show waite screen
		retailnet.loader.show();
		
		// get current user data
		var data = retailnet.ajax.request('/applications/helpers/ajax.user.php');
		
		if (data) {
			
			data.sender_firstname = data.user_firstname;
			data.sender_name = data.user_name;
			data.id = template;
			data.render = true;
			
			// parse periode name
			var d = new Date($('#periodes').val()*1000)
			data.periode = d.getMonthName() + ' ' + d.getFullYear();
			
			// load mail template
			retailnet.ajax.json('/applications/helpers/ajax.mailtemplate.php', data).done(function(xhr) {
				
				if (xhr) {
					
					$('.mail_template_id', target).val(xhr.mail_template_id);
					$('.mail_template_view_modal', target).text(xhr.mail_template_view_modal);
					$('.mail_template_subject', target).val(xhr.mail_template_subject);
					$('.mail_template_text', target).text(xhr.mail_template_text);

					if (xhr.mail_template_view_modal == 1) {
						
						// show mail form modal
						retailnet.modal.show(target); 
						
					} else {
						
						// submit mail form
						mailReminder.submit();
					}
				}
				
			}).complete(function() {
				
				// clos wait screen
				retailnet.loader.hide();
			});
		}
		
		return false;
	});
	
	
	
	// button sendmail modal
	$('.button.apply', $('#reminder-form')).click(function(e) {
		
		e.preventDefault();
		
		// close all notofications
		retailnet.notification.hide();
		
		var self = $(this);
		var error = false;
		var required = $('.required', mailReminder)

		if (required.length > 0) {
			
			var failures = $('.required[value=]', mailReminder);
			
			// remove error marks
			$('.required', mailReminder).removeClass('error');
			
			if (failures.length > 0) {
				
				error = true;
				
				// mark required failures
				$('.required[value=]', mailReminder).addClass('error');
				
				// show notification
				retailnet.notification.error('Red marked fields are required.');
			}
		}
		
		if (!error) {
			
			// submit mail form
			mailReminder.submit(); 
		}
		
		return false;
	});
	
	
	// submit modal mail
	mailReminder.submit(function(e) {
		
		e.preventDefault();
		
		var self = $(this);
		var data = $('form.request, form.filters, #reminder-form form').serialize();

		// close modal screen
		retailnet.modal.hide();
		
		// show wait screen
		retailnet.loader.show();

    	retailnet.ajax.json(self.attr('action'), data).done(function(xhr) {

    		if (xhr && xhr.reload) {
				window.location.reload();
        	}
    		
			if (xhr && xhr.notification) {
				retailnet.notification.show(xhr.notification.content, eval(xhr.notification.properties));
			}
			
        }).complete(function() {
        	
        	// close wait screen
        	retailnet.loader.hide();
        });
		
		
		return false;
	});

});