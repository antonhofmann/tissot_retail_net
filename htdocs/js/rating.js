function star_click(star, starnumber, value, numofstars, text)
{
	for(i=1;i<=starnumber;i++)
	{
		starObject = document.getElementById(star  + "_" + i);
		starObject.src = "../pictures/rating_star.gif";
	}
	for(i=starnumber+1;i<=numofstars;i++)
	{
		starObject = document.getElementById(star + "_" + i);
		starObject.src = "../pictures/rating_star_off.gif";
	}
	
	document.getElementById(star + "_rating_text").innerHTML = text;
	document.getElementById(star).value = value;
	
	return true;

}