$(document).ready(function() {
	
	var mailTemplateForm = $('#mailTemplateForm'),
		mailTemplateSubmit = $('#mailTemplateSubmit'),
		mailTemplateCancel = $('#mailTemplateCancel');
	
	// send mail template form
	mailTemplateForm.submit(function(event) {
		
		event.preventDefault();
		$.fancybox.hideActivity();
		
		var state = newState.val();

		$.ajax({
			url: mailForm.attr('action'),
			dataType: 'json',
			data: $('#mailForm, #ordersheetsForm').serialize(),
			beforeSend: function() {
				$.loader();
				$.fancybox.close();
			},
			success: function(json) { 

				$.loader('close');

				if (json && json.response && state) {
					orderSheetsForm.submit();
				} 

				if (json && json.message) {
					$.jGrowl(json.message, {
						life: 5000, 
						sticky: (json.response) ? false : true,
						theme: (json.response) ? 'message' : 'error'
					});
				}
			}
		});

		return false;
	});
	
	// change mail template form
	mailTemplateForm.change(function() {
		
	});
	
	// submit mail template form
	mailTemplateSubmit.click(function() {
		
	});
	
	// cancel mail template form
	mailTemplateCancel.click(function() {
		
	});
	
	$('#cancel').bind('click', function(event) {
		event.preventDefault();
		$.fancybox.close();
		return false;
	});	

});