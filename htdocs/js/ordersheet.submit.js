$(document).ready(function() {

	var orderSheetsForm = $('#ordersheetsForm'),
		mailForm = $('#mailForm'),
		workflowState = $('#workflowStates'),
		orderSheets = $('#ordersheets');
	
	// loader instance
	retailnet.loader.init();

	// table loader
	$('#list').tableLoader({
		url: '/applications/helpers/ordersheet.submit.list.php',
		filters: orderSheetsForm,
		after: function(self) {

			// dropdown builders
			$('select',self).dropdown();

			// reset ordersheets
			$('select',self).change(function() {
				$('#ordersheets').val('')
			});

			// order sheet box
			$('.ordersheetbox').click(function() {
				setOrdersheets();
			});

			// check all order sheets
			$('.checkall').click(function() {
				var checkbox = $('.ordersheetbox');
				var checked = ($(this).is(':checked')) ? 'checked' : false;
				checkbox.attr('checked', checked);
				setOrdersheets();
			});
		}
	});

	// store seleted ordersheets
	var setOrdersheets = function() {

		var checkbox = $('.ordersheetbox'),
			data = orderSheets.val().split(',') || [],
			value, index, checked; 
		
		$.each(checkbox, function() {

			value = $(this).attr('value');
			index = $.inArray(value, data);
			checked = $(this).is(':checked');

			if (checked) data.push(value);
			else if (index>-1) data.splice(index, 1);
		});
		
		data = $.grep(data, function(n) { 
			return (n); 
		});
		
		data = $.grep(data, function(v,k){
		    return $.inArray(v,data) === k;
		});
		
		orderSheets.val('');
		
		if (data.length > 0) {
			orderSheets.val(data.join(','));
		}

		checked = (checkbox.length == $('.ordersheetbox:checked').length) ? 'checked' : false;
		
		$('.checkall').attr('checked', checked);
	}
 
	
	// submit button
	$('.submit').click(function(e) {

		e.preventDefault();

		var self = $(this);
		
		if ($('#ordersheets').val()) { 
			
			// send mail trigger
			$('#sendmail').val(self.hasClass('sendmail') ? 1 : 0);
			
			// submit order sheets
			if ($('#action').val()=='submit') { 
				orderSheetsForm.submit();
			}
			
			// send mail to order sheets
			if (self.hasClass('sendmail')) {
				
				// edit mail content befor send
				if ($('#mail_template_view_modal').val() == 1) {
					retailnet.modal.show('#modalbox'); 
				} 
				// submit mail form
				else {
					mailForm.submit();
				}				
			}
			
		} else {		

			// show notification
			retailnet.notification.show('Please, select at least one order sheet.', {
				sticky: false, 
				life: 5000,
				theme: 'error'
			});
		}

		return false;
	});	

	$('.cancel').bind('click', function(e) {
		e.preventDefault();
		retailnet.modal.hide();
		return false;
	});	

	// 
	$('.submit-modal').click(function(e) {

		e.preventDefault();
		
		var self = $(this),
			required = $('.required', mailForm),
			failures = $('.required[value=]', mailForm);

    	if (required.length > 0 && failures.length > 0) {
    		
    		// mark required failures
    		$('.required', mailForm).removeClass('error');
    		$('.required[value=]', mailForm).addClass('error');
    		
    		// show notification
    		retailnet.notification.show('Red marked fields are required.', {
    			sticky: false,
    			life: 5000,
    			theme: 'error'
    		});
    		
    	} else {

    		// hide modal screen
			retailnet.modal.hide();
			
			// submit mail form
			mailForm.submit(); 
    	}

		return false;
	});
	
	
	// submit order sheet form
	orderSheetsForm.submit(function(e) {
		
		e.preventDefault();
		
		var self = $(this);
		
		if ($('#ordersheets').val()) {
			
			// show splash screen
			retailnet.loader.show();
			
			retailnet.ajax.json(self.attr('action'), self.serialize()).done(function(xhr) {
				
				// show notification
				if (xhr && xhr.notification) {
					retailnet.notification.show(xhr.notification.content, eval(xhr.notification.properties));
				}
				
				// reload page
				if (xhr && xhr.reload) {
					window.location.reload();
	        	}
				
				// reset list inputs
				if (xhr && xhr.response && $('#sendmail').val()!=1) {
					$('table input').attr('checked', false);
					$('#ordersheets').val('');
				}
				
			}).complete(function() {
				
				// hide splash screen
				retailnet.loader.hide();
			});	
			
		} else {
			
			// show notification
			retailnet.notification.show('Please, select at least one order sheet.', {
				sticky: false, 
				life: 5000,
				theme: 'error'
			});
		}
		
		return false;
	});
	
	
	// submit mail form
	mailForm.submit(function(e) { 
	
		e.preventDefault();
		
		var self = $(this);
		var url = self.attr('action');
		var data = $('#mailForm, #ordersheetsForm').serialize();
		
		if ($('#ordersheets').val()) {
			
			// show splash screen
			retailnet.loader.show();
			
			retailnet.ajax.json(url,data).done(function(xhr) {
				
				if (xhr.response) {
					
					// reload page
					if (xhr && (xhr.reload || $('#action').val()=='submit') ) {
						window.location.reload();
		        	}
					
					// reset data
					if (xhr && xhr.response) {
						$('table input').attr('checked', false);
						$('#ordersheets').val('');
					}
					
					// show notification
					if (xhr && xhr.notification) {
						retailnet.notification.show(xhr.notification.content, eval(xhr.notification.properties));
					}

				} else {
					
					// show notification
					retailnet.notification.show('Selected order sheets can not be subbmited.', {
						sticky: false, 
						life: 5000,
						theme: 'error'
					});
				}

			}).complete(function() {
				
				// hide splash screen
				retailnet.loader.hide();
			});
			
		} else {
				
			// show notification
			retailnet.notification.show('Please, select at least one order sheet.', {
				sticky: false, 
				life: 5000,
				theme: 'error'
			});
		}
	});
});