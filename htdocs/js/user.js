$(document).ready(function() {

	var userForm = $("#userform"),
		accountMailContainer = $('#accountMailContainer'),
		accountMailForm = $('form', accountMailContainer),
		loginMailContainer = $('#loginMailContainer'),
		loginMailForm = $('form', loginMailContainer);
	
	
	// loader instance
	retailnet.loader.init();

	
	// tooltips
	$('.-tooltip[title]').qtip({
		position: {corner: {target:'topRight', tooltip:'leftTop'}},
		style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
	});
	
	// event: focus
	$('input:visible, select:visible, textarea:visible', accountMailForm).click(function() {		
		$(this).addClass('onFocus').removeClass('onBlur');
	});
	
	// event: blur
	$('input:visible,select:visible,textarea:visible', accountMailForm).blur(function() {		
		$(this).removeClass('onFocus').addClass('onBlur');
	});
	
	// event: change
	$('.required:visible', accountMailForm).change(function() {		
		$(this).removeClass('onFocus').addClass('onBlur');
	});
	
	
	// dialog buttons
	$('.dialog').click(function(e) {
		
		e.preventDefault();
		e.stopPropagation();
	
		var button = $(this);
		
		// open dialog
		retailnet.modal.show('#'+button.attr('id')+'_dialog', {
			'onComplete':	function() {
				$('#apply').attr('href', button.attr('href'))
			}
		}); 
	});
	
	
	// cancel modal screen
	$('.cancel').click(function(e) {
		
		e.preventDefault();
		e.stopPropagation();
		
		// mycompany add new user
		// on cancel sendmail, go to user list
		if ($('#redirect').val() && $('#user').val()) {
			window.location = $('#redirect').val();
		}
		
		retailnet.modal.hide();
		
		return false;
	});	
	

	
/**************************************************** USER FORM ***********************************************************/
	
	
	// check user login name as uniques
	$('#user_login').change(function() {
	
		var self = $(this);
		var value = self.val();
	
		// close all notofications
		retailnet.notification.hide();
		
		// remove error marks
		self.removeClass('error');
		
		if (value) {
			
			// user name exist?
			retailnet.ajax.json('/applications/helpers/user.validator.php', {
				section: 'double_login_name',
				value: value
			}).done(function(xhr) {
				
				// login name exist, add error class
				if (xhr && xhr.error) {
					self.addClass('error');
				}
				
				// notification
				if (xhr && xhr.message) {
					retailnet.notification.show(xhr.message);
				}
			});
		}
	});
	
	
	// button submit user form
	$("#user_save").click(function(e) {
	
		e.preventDefault();
		e.stopPropagation();
		
		// close all notofications
		retailnet.notification.hide();
		
		// remove all error marks
		$('.error', userForm).removeClass('error');
	
		// mycompany roles is required
		if (userForm.hasClass('mycompany') && $('input.roles:checked').length == 0) {
			$('.roles label').addClass('error');
		}
		
		var errors = userForm.find('.error').length;
		var valid = userForm.validationEngine('validate');
		
		if (errors || !valid) {
			
			// shor error message
			retailnet.notification.error('ERROR: check red marked fields');
			
		} else {
			
			// submit user form
			userForm.submit();
		}
		
		return false;
	});
	
	
	// submit user form
	userForm.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) {
	
			if (json) {
				
				var sendmail = $("#user_save").hasClass('sendmail');
	
				// send mail notifocation to role responsibile user
				if (json.response && json.id && sendmail) {
				
					// modal subtitle
					var subtitle = $('#user_firstname').val() + ' ' + $('#user_name').val();
					$('.subtitle', accountMailContainer).text(subtitle);
				
					
					// account form triggering
					$('#user').val(json.id).trigger('change');
				
				} else {
	
					if (json.redirect) {
						window.location=json.redirect;
					}
	
					if (json.message) {
						retailnet.notification.show(json.message);
					}
				}
			}
		}
	});

	
/**************************************************** ACCOUNT FORM ***********************************************************/
	
	
	// account mail change user id
	// load user data and parse date to load mail template content
	$('#user', accountMailForm).change(function(e) {
		
		e.preventDefault();
		e.stopPropagation();
		
		var self = $(this);
		var template = $('#mail_template_id', accountMailForm).val();
		
		var data = {
			render: true,
			id: template
		}
		
		// user dataloader
		var user = retailnet.ajax.request('/applications/helpers/ajax.user.php', {
			id: self.val()
		});
		
		// sender dataloader
		var sender = retailnet.ajax.request('/applications/helpers/ajax.user.php', {
			section: 'sender'
		});
		
		// company dataloader
		var company = retailnet.ajax.request('/applications/helpers/ajax.company.php', {
			id: user.user_address
		});

		// recipient dataloader
		var recipient = retailnet.ajax.request('/applications/helpers/ajax.user.php', {
			id: self.val(), 
			section: 'role-responsibile'
		});
		
		// dataloader
		data = $.extend(data, user, sender, company, recipient);
		
		// mail temlate
		var mail = retailnet.ajax.request('/applications/helpers/ajax.mailtemplate.php', data);
		
		if (mail) {

			// modal screen title
			$('.subtitle', accountMailContainer).html(user.user_firstname + ' ' + user.user_name);
			
			// mail subject
			$('#mail_template_subject', accountMailForm).val(mail.mail_template_subject);
			
			// mail template content
			$('#mail_template_text', accountMailForm).text(mail.mail_template_text);
			
			// mail template content
			$('#mail_template_view_modal', accountMailForm).text(mail.mail_template_view_modal);

			// edit mail befor send
			if (mail.mail_template_view_modal) {
				
				// show modal mail form
				var target = '#'+accountMailContainer.attr('id');
				retailnet.modal.show(target);
				
			} else {
				
				// submit mail form
				accountMailForm.submit();
			}
			
		} else {
			
			// shor error message
			retailnet.notification.error('Error: E-Mail template not found');
		}
		
		return false;
	});
	
	
	// apply account mail form
	$('.apply',accountMailContainer).click(function(e) {
		
		e.preventDefault();
		e.stopPropagation();
		
		var self = $(this);
		var error = false;
		var requiredFields = $('.required:visible', accountMailForm);
		
		// close all notofications
		retailnet.notification.hide();
		
		// remove all error marks
		$('.error', accountMailForm).removeClass('error');
		
		// check required fields
		if (requiredFields.length > 0) {
			requiredFields.each(function(i,e) {
				if (!$(e).val()) {
					$(e).addClass('error');
					error = true;
				}
			});			
		}
	
		if (error) {
			
			// shor error mesage
			retailnet.notification.error('Red marked Fields are required');
			
		} else {
			
			// submit account mail
			accountMailForm.submit(); 
		}
		
		return false;
	});
	
	
	// submit account form
	accountMailForm.submit(function(e) {
		
		e.preventDefault();
		e.stopPropagation();
		
		var self = $(this);
		var data = self.serializeArray();
		
		// close modal screen
		retailnet.modal.hide();
		
		// show wait screen
		retailnet.loader.show();
		
		// controller
		data.push({
			name: 'controller', 
			value: $('#controller').val()
		});
		
		retailnet.ajax.json(self.attr('action'), data).done(function(xhr) {
			
			if (xhr && xhr.reload) {
				window.location.reload();
        	}
			
			if (xhr && xhr.redirect) {
				window.location = xhr.redirect;
			} 
    		
			if (xhr && xhr.notification) {
				retailnet.notification.show(xhr.notification.content, eval(xhr.notification.properties));
			}
			
		}).complete(function() {
			
			// hide splash screen
			retailnet.loader.hide();
		});
		
		return false;
	});
	
	
/**************************************************** LOGIN FORM ***********************************************************/
	
	
	// login form submit button
	$('#send_login_data').click(function(e) {
		
		e.preventDefault();
		e.stopPropagation();
		
		if ($('#mail_template_view_modal').val() == 1) {
			
			// edit mail form befor send
			var target = '#'+loginMailContainer.attr('id');
			retailnet.modal.show(target);
			
		} else {
			
			// submit login form
			loginMailForm.submit();
		}
		
		return false;
	});
	
	
	// button send login data
	$('#login_data_apply').click(function(e) {
		
		e.preventDefault();
		e.stopPropagation();
		
		var self = $(this);
		var error = false;
		var requiredFields = $('.required:visible', loginMailForm);
		
		// close all notofications
		retailnet.notification.hide();
		
		// remove all error marks
		$('.error', loginMailForm).removeClass('error');
		
		// check required fields
		if (requiredFields.length > 0) {
			requiredFields.each(function(i,e) {
				if (!$(e).val()) {
					$(e).addClass('error');
					error = true;
				}
			});			
		}
	
		if (error) {
			
			// shor error mesage
			retailnet.notification.error('Red marked Fields are required');
			
		} else {
			
			// submit account mail
			loginMailForm.submit(); 
		}

		return false;
	});
	
	
	// submit login from
	loginMailForm.submit(function(e) {
		
		e.preventDefault();
		e.stopPropagation();
		
		var self = $(this);
		var data = self.serializeArray();
		
		// close modal screen
		retailnet.modal.hide();
		
		// show wait screen
		retailnet.loader.show();
		
		// controller
		data.push({
			name: 'controller', 
			value: $('#controller').val()
		});
		
		retailnet.ajax.json(self.attr('action'), data).done(function(xhr) {
			
			if (xhr && xhr.reload) {
				window.location.reload();
        	}
			
			if (xhr.response && xhr.redirect) {
				window.location = xhr.redirect;
			} 
    		
			if (xhr && xhr.notification) {
				retailnet.notification.show(xhr.notification.content, eval(xhr.notification.properties));
			}
			
		}).complete(function() {
			
			// hide splash screen
			retailnet.loader.hide();
		});
		
		return false;
		
	});
	
/**************************************************** OLD RETAILNET ***********************************************************/	

	// old retailnet
	$('.users-list td a').click(function(e) {
		
		e.preventDefault();
		e.stopPropagation();

		var id = $(this).attr('data');
		
		if (id) {
			
			// add close trigger
			// and add template id for close account
			$('.title', accountMailContainer).text('Request to Close Account');
			$('#close', accountMailForm).val(1);
			$('#mail_template_id', accountMailForm).val(11);

			// assign user id to form
			// and trigger form for submitting
			$('#user', $('#accountMailContainer') ).val(id).trigger('change');
				
		} else {

			// error message
			retailnet.notification.error('Error: User not found.');
		}

		return false;
	});

	$('.modal-userform').click(function(e) {
		
		e.preventDefault();
		e.stopPropagation();
		
		// remove close trigger
		// and add template id for new open account
		$('.title', accountMailContainer).text('Request to Open New Account');
		$('#close', accountMailForm).val('');
		$('#mail_template_id', accountMailForm).val(12);
		
		retailnet.modal.show('#user_modal_form');
		
		return false;
	});
});
