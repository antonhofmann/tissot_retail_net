
$(document).ready(function() {
	
	// user list
	$('#send_login_data').click(function(event) {
		
		event.preventDefault();
		
		var request = xhr('/applications/helpers/ajax.user.php', {
			section: 'user-roles',
			id: $('#mail_template_user').val()
		});
		
		if (request && request.roles) {
			if ($('#mail_template_mode').val()) {
				modal('#modal_login_data');
			} else {
				$('#form_login_data').submit();
			}
		} else {
			$.jGrowl('Please assign at least one role to the user.', {
				life: 5000, 
				sticky: false,
				theme: 'error'
			});
		}

		return false;
	});
	
	$('.cancel').click(function(event) {
		event.preventDefault();
		$.fancybox.close();
		return false;
	});	

	$('#login_data_apply').click(function(event) {
		event.preventDefault();
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
		$('#form_login_data').submit();
		return false;
	});
		
	$('#form_login_data').submit(function(event) {

		event.preventDefault();

		var errors = ($('input.required[value=""]', $('#form_login_data')).length) ? true : false;

		$('input', $('#form_login_data')).removeClass('error');
		$('input.required[value=""]', $('#form_login_data')).addClass('error');

		if (errors) {
			$.jGrowl('Red marked Fields are required.', { 
				sticky: true, 
				theme:'error'
			});
		} else {
			
			$.loader();
			
			var action = $('#form_login_data').attr('action');
			var data = $('#form_login_data').serialize();
			
			var request = xhr(action, data);

			if (request) {

				$.loader('close');

				if (request.response && request.redirect) {
					window.location=request.redirect;
				} 

				if (request.message) {
					$.jGrowl(request.message, {
						life: 5000, 
						sticky: (request.response) ? false : true,
						theme: (request.response) ? 'message' : 'error'
					});
				}
			}
		}

		$.fancybox.close();
		
		return false;
	});
	
});