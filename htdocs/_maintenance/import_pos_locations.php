<?php


// Numbers of days between January 1, 1900 and 1970 (including 19 leap years)
define("MIN_DATES_DIFF", 25569);
  
// Numbers of second in a day:
define("SEC_IN_DAY", 86400);   
 
function excel2date($excelDate)
{
   if ($excelDate <= MIN_DATES_DIFF) {
      return 0;
   }
   $tmp = ($excelDate - MIN_DATES_DIFF) * SEC_IN_DAY;
   return date('Y-m-d', $tmp);
}



$ratings = array();
$ratings["Bad"] = 5;
$ratings["Poor"] = 4;
$ratings["Good"] = 3;
$ratings["Very good"] = 2;
$ratings["Excellent"] = 1;


$debug_info = array();
require_once "../include/frame.php";

check_access("can_edit_catalog");

//import POS locations
$sql = "select * from _import_data_hongkong_20180126 
		where pos_id = 0 or pos_id is null 
		order by OWNER_COMPANY_NAME";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res)) {

	//remove leading and ending blanks
	foreach($row as $key=>$value) {
	$row[$key] = trim($value);
		$row[$key] = str_replace("’", "'", $row[$key]);
		$row[$key] = trim($row[$key]);

	}

	/* 01 OWNER COMPANY */

	// get owner_companie's country
	$country_id = 0;
	$streetnumber_before_street_name = false;
	$currency_id = 0;
	$sql2 = "select country_id, country_nr_before_streename, country_currency
		  from countries 
		  where LOWER(country_name) = " . dbquote(strtolower($row['COUNTRY']));



	$res2 = mysql_query($sql2) or dberror($sql2);
	if($row2 = mysql_fetch_assoc($res2)) {
		$country_id = $row2['country_id'];
		if($row2['country_nr_before_streename'] == 1){
		$streetnumber_before_street_name = true;
		}

		$currency_id = $row2['country_nr_before_streename'];
	}
	else {
		$debug_info[] = "Owner company's country " . $row['COUNTRY'] . " does not exist.";
	}

	// get owner_companie's city
	$place_id = 0;
	$sql2 = "select place_id from places 
		  where LOWER(place_name) = " . dbquote(strtolower($row['OWNER_CITY']));



	$res2 = mysql_query($sql2) or dberror($sql2);
	if($row2 = mysql_fetch_assoc($res2)) {
		$place_id = $row2['place_id'];
	}
	else {
		$debug_info[] = "Owner company's city " . $row['OWNER_CITY'] . " does not exist.";
	}


	//get parent
	

	$parent_id = 0;
	$sql2 = "select address_id from addresses 
			where LOWER(address_company) = " . dbquote(strtolower($row['Parent_']));


	 $res2 = mysql_query($sql2) or dberror($sql2);
	 if($row2 = mysql_fetch_assoc($res2)) {
		 $parent_id = $row2['address_id'];
	 }
	 else {
		$debug_info[] = "Parent company " . $row['Parent_'] . " does not exist.";
	 }

	 if($parent_id > 0 and $country_id > 0 and $place_id > 0) {

		//check if company is already present
		$company_id = 0;
		$sql2 = "select address_id from addresses 
				where LOWER(address_company) = " . dbquote(strtolower($row['OWNER_COMPANY_NAME'])) . 
				" and LOWER(address_street) = " . dbquote(strtolower($row['OWNER_ADDRESS_STREET'])) . 
				" and address_place_id = " . $place_id .
				" and address_country = " . $country_id;


		 $res2 = mysql_query($sql2) or dberror($sql2);
		 if($row2 = mysql_fetch_assoc($res2)) {
			 $company_id = $row2['address_id'];
		 }
		 else {
			
			//add company
			$error = 0;

			$fields = array();
			$values = array();

			//imported fields

			$fields[] = "address_sapnr";
			$values[] = dbquote($row["CLIENT_CODE_SAP_Number_Owner_Company"]);

			$fields[] = "address_company";
			$values[] = dbquote($row["OWNER_COMPANY_NAME"]);

			$fields[] = "address_company2";
			$values[] = dbquote($row["OWNER_COMPANY_NAME_2"]);

			$fields[] = "address_street";
			$values[] = dbquote($row["OWNER_ADDRESS_STREET"]);

			$fields[] = "address_streetnumber";
			$values[] = dbquote($row["OWNER_ADDRESS_STREET_NUMBER"]);

			//other fields
			if($streetnumber_before_street_name == true) {
				if($row["OWNER_ADDRESS_STREET_NUMBER"]) {
					$fields[] = "address_address";
					$values[] = dbquote($row["OWNER_ADDRESS_STREET_NUMBER"] . ", " . $row["OWNER_ADDRESS_STREET"]);
				}
				else {
					$fields[] = "address_address";
					$values[] = dbquote($row["OWNER_ADDRESS_STREET"]);
				}
			}
			else {
				if($row["OWNER_ADDRESS_STREET_NUMBER"]) {
					$fields[] = "address_address";
					$values[] = dbquote($row["OWNER_ADDRESS_STREET"] . " " . $row["OWNER_ADDRESS_STREET_NUMBER"]);
				}
				else {
					$fields[] = "address_address";
					$values[] = dbquote($row["OWNER_ADDRESS_STREET"]);
				}
			}

			$fields[] = "address_address2";
			$values[] = dbquote($row["OWNER_ADDRESS_2"]);

			$fields[] = "address_zip";
			$values[] = dbquote($row["OWNER_ZIP"]);

			$fields[] = "address_place";
			$values[] = dbquote($row["OWNER_CITY"]);

			$fields[] = "address_place_id";
			$values[] = dbquote($place_id);

			$fields[] = "address_country";
			$values[] = dbquote($country_id);

			$fields[] = "address_phone_country";
			$values[] = dbquote($row["Owner_Phone_Number_COUNTRY_CODE"]);

			$fields[] = "address_phone_area";
			$values[] = dbquote($row["Owner_Phone_Number_AREA_CODE"]);

			$fields[] = "address_phone_number";
			$values[] = dbquote($row["Owner_Phone_Number_NUMBER"]);

			$fields[] = "address_phone";
			if($row["Owner_Phone_Number_AREA_CODE"]) {
				$values[] = dbquote(trim($row["Owner_Phone_Number_COUNTRY_CODE"] . " " . $row["Owner_Phone_Number_AREA_CODE"] . " " . $row["Owner_Phone_Number_NUMBER"]));
			}
			else {
				$values[] = dbquote(trim($row["Owner_Phone_Number_COUNTRY_CODE"] . " " . $row["Owner_Phone_Number_NUMBER"]));
			}


			$fields[] = "address_contact_name";
			$values[] = dbquote($row["Owner_contact_person"]);

			$fields[] = "address_contact_email";
			$values[] = dbquote($row["Owner_email"]);

			$fields[] = "address_website";
			$values[] = dbquote($row["Owner_web_site"]);

			
			$fields[] = "address_currency";
			$values[] = dbquote($currency_id);
			
			
			if($row["Address_Type"] == 'Third Party') {
				$fields[] = "address_type";
				$values[] = 7;

				$fields[] = "address_swatch_retailer";
				$values[] = 1;

				$fields[] = "address_is_independent_retailer";
				$values[] = 1;

				$fields[] = "address_can_own_independent_retailers";
				$values[] = 1;
			}
			elseif($row["Address_Type"] == 'Subsidiary') {
				$fields[] = "address_type";
				$values[] = 1;

				$fields[] = "address_client_type";
				$values[] = 2;
			}
			elseif($row["Address_Type"] == 'Franchisee') {
				$fields[] = "address_type";
				$values[] = 1;

				$fields[] = "address_client_type";
				$values[] = 1;

				$fields[] = "address_can_own_independent_retailers";
				$values[] = 1;
			}
			else {
				$error = "Address type is unknown";
			}

			$fields[] = "address_parent";
			$values[] = dbquote($parent_id);

			$fields[] = "address_active";
			$values[] = 1;

			$fields[] = "address_canbefranchisee";
			$values[] = 1;

			$fields[] = "address_showinposindex";
			$values[] = 1;

			$fields[] = "address_added_by";
			$values[] = 2;

			$fields[] = "date_created";
			$values[] = dbquote(date("Y-m-d H:i:s"));

			$fields[] = "user_created";
			$values[] = dbquote(user_login());


			$sql = "insert into addresses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";


			if($error === 0) {
				mysql_query($sql) or dberror($sql);
				$company_id = mysql_insert_id();
				$debug_info[] = "Owner company " . $row['OWNER_COMPANY_NAME'] . " was inserted.";
			}
			else {
				$debug_info[] = "Owner company " . $row['OWNER_COMPANY_NAME'] . " error: " . $error;
			}
		 }


		/* 02 POS LOCATION */
		if($company_id > 0) 
		{
			// get POS location's country
			$country_id = 0;
			$streetnumber_before_street_name = false;
			$currency_id = 0;
			$sql2 = "select country_id, country_nr_before_streename, country_currency
				  from countries 
				  where LOWER(country_name) = " . dbquote(strtolower($row['POS_Country']));



			$res2 = mysql_query($sql2) or dberror($sql2);
			if($row2 = mysql_fetch_assoc($res2)) {
				$country_id = $row2['country_id'];
				if($row2['country_nr_before_streename'] == 1){
				$streetnumber_before_street_name = true;
				}

				$currency_id = $row2['country_nr_before_streename'];
			}
			else {
				$debug_info[] = "Pos Location's country " . $row['POS_Country'] . " does not exist.";
			}

			// get Pos location's city
			$place_id = 0;
			$sql2 = "select place_id from places 
				  where LOWER(place_name) = " . dbquote(strtolower($row['POS_CITY']));

			$res2 = mysql_query($sql2) or dberror($sql2);
			if($row2 = mysql_fetch_assoc($res2)) {
				$place_id = $row2['place_id'];
			}
			else {
				
				//create new city
				$province_id = 0;
				$sql2 = "select province_id from provinces 
					  where province_country = " . dbquote($country_id) .
					  " and LOWER(province_canton) = " . dbquote(strtolower($row['POS_PROVINCE']));

				$res2 = mysql_query($sql2) or dberror($sql2);
				if($row2 = mysql_fetch_assoc($res2)) {
					$province_id = $row2['place_id'];

					$fields = array();
					$values = array();

					$fields[] = "place_country";
					$values[] = dbquote($country_id);

					$fields[] = "place_province";
					$values[] = dbquote($province_id);

					$fields[] = "place_name";
					$values[] = dbquote($row['POS_CITY']);

					$fields[] = "date_created";
					$values[] = dbquote(date("Y-m-d H:i:s"));

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$sql = "insert into places (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
					$place_id = mysql_insert_id();
					$debug_info[] = "City " . $row['POS_CITY'] . " was inserted.";
				}
				else {
					$debug_info[] = "Pos Location's province " . $row['POS_PROVINCE'] . " does not exist.";
				}
			}

			//get POS Type
			$postype_id = 0;
			$sql2 = "select postype_id
				  from postypes 
				  where LOWER(postype_name) = " . dbquote(strtolower($row['POS_TYPE']));



			$res2 = mysql_query($sql2) or dberror($sql2);
			if($row2 = mysql_fetch_assoc($res2)) {
				$postype_id = $row2['postype_id'];
			}
			else {
				$debug_info[] = "Pos Location's type " . $row['POS_TYPE'] . " does not exist.";
			}

			//get product line
			$productline_id = 0;
			$sql2 = "select product_line_id
				  from product_lines 
				  where LOWER(product_line_name) = " . dbquote(strtolower($row['Furniture_Type']));



			$res2 = mysql_query($sql2) or dberror($sql2);
			if($row2 = mysql_fetch_assoc($res2)) {
				$productline_id = $row2['product_line_id'];
			}
			else {
				$debug_info[] = "Pos Location's furniture type " . $row['Furniture_Type'] . " does not exist.";
			}

			//get distribution channel
			$distribution_channel_id = 0;
			$sql2 = "select mps_distchannel_id
				  from mps_distchannels 
				  where LOWER(mps_distchannel_code) = " . dbquote(strtolower($row['DISTRIBUTION_CHANNEL_CODE']));



			$res2 = mysql_query($sql2) or dberror($sql2);
			if($row2 = mysql_fetch_assoc($res2)) {
				$distribution_channel_id = $row2['mps_distchannel_id'];
			}
			else {
				$debug_info[] = "Pos Location's distribution channel " . $row['DISTRIBUTION_CHANNEL_CODE'] . " does not exist.";
			}

			$pos_id = 0;
			if($country_id > 0 
				and $place_id > 0 
				and $postype_id > 0
				and $productline_id > 0
				and $distribution_channel_id > 0) 
			{
				//check if pos location is already present
				$sql2 = "select posaddress_id from posaddresses 
						where LOWER(posaddress_name) = " . dbquote(strtolower($row['POS_NAME'])) . 
						" and LOWER(posaddress_street) = " . dbquote(strtolower($row['POS_ADDRESS_STREET'])) . 
						" and posaddress_place_id = " . $place_id .
						" and posaddress_country = " . $country_id;


				 $res2 = mysql_query($sql2) or dberror($sql2);
				 if($row2 = mysql_fetch_assoc($res2)) {
					 $pos_id = $row2['posaddress_id'];
				 }
				 else {
					
					//add posaddress
					$error = 0;

					$fields = array();
					$values = array();

					//imported fields

					$fields[] = "posaddress_client_id";
					$values[] = dbquote($parent_id);

					if($row["LEGAL_TYPE"] == 'Corporate') {
						$fields[] = "posaddress_ownertype";
						$values[] = 1;
					}
					elseif($row["LEGAL_TYPE"] == 'Franchisee') {
						$fields[] = "posaddress_ownertype";
						$values[] = 2;
					}
					elseif($row["LEGAL_TYPE"] == 'Other') {
						$fields[] = "posaddress_ownertype";
						$values[] = 6;
					}
					else {
						$error = "Legal type is invalid " . $row["LEGAL_TYPE"];
					}

					$fields[] = "posaddress_eprepnr";
					$values[] = dbquote($row["Enterprise_Reporting_Number_Swatch_Group_Corporate"]);

					$fields[] = "posaddress_sapnumber";
					$values[] = dbquote($row["SAP_Number_POS_Location"]);

					$fields[] = "posaddress_franchisor_id";
					$values[] = 13;

					$fields[] = "posaddress_franchisee_id";
					$values[] = dbquote($company_id);

					$fields[] = "posaddress_name";
					$values[] = dbquote($row["POS_NAME"]);

					$fields[] = "posaddress_name2";
					$values[] = dbquote($row["POS_NAME_2"]);

					$fields[] = "posaddress_street";
					$values[] = dbquote($row["POS_ADDRESS_STREET"]);

					$fields[] = "posaddress_street_number";
					$values[] = dbquote($row["POS_ADDRESS_STREET_NUMBER"]);

					if($streetnumber_before_street_name == true) {
						
						if($row["POS_ADDRESS_STREET_NUMBER"]) {
							$fields[] = "posaddress_address";
							$values[] = dbquote($row["POS_ADDRESS_STREET_NUMBER"] . ", " . $row["POS_ADDRESS_STREET"]);
						}
						else {
							$fields[] = "posaddress_address";
							$values[] = dbquote($row["POS_ADDRESS_STREET_NUMBER"]);
						}
					}
					else {
						if($row["POS_ADDRESS_STREET_NUMBER"]) {
							$fields[] = "posaddress_address";
							$values[] = dbquote($row["POS_ADDRESS_STREET"] . " " . $row["POS_ADDRESS_STREET_NUMBER"]);
						}
						else {
							$fields[] = "posaddress_address";
							$values[] = dbquote($row["POS_ADDRESS_STREET"]);
						}
					}

					$fields[] = "posaddress_address2";
					$values[] = dbquote($row['POSADDRESS_2']);

					$fields[] = "posaddress_zip";
					$values[] = dbquote($row['POS_ZIP']);

					$fields[] = "posaddress_place_id";
					$values[] = dbquote($place_id);

					$fields[] = "posaddress_place";
					$values[] = dbquote($row['POS_CITY']);

					$fields[] = "posaddress_country";
					$values[] = dbquote($country_id);
					
					$fields[] = "posaddress_phone_country";
					$values[] = dbquote($row["POS_Phone_Number_COUNTRY_CODE"]);

					$fields[] = "posaddress_phone_area";
					$values[] = dbquote($row["POS_Phone_Number_AREA_CODE"]);

   				    $fields[] = "posaddress_phone_number";
					$values[] = dbquote($row["POS_Phone_Number_Number"]);

					
					if($row["POS_Phone_Number_AREA_CODE"]) {
						$fields[] = "posaddress_phone";
						$values[] = dbquote($row["POS_Phone_Number_COUNTRY_CODE"]. " " . $row["POS_Phone_Number_AREA_CODE"]. " " . $row["posaddress_phone_number"]); 
					}
					else {
						$fields[] = "posaddress_phone";
						$values[] = dbquote($row["POS_Phone_Number_COUNTRY_CODE"]. " " . $row["posaddress_phone_number"]); 
					}

					$fields[] = "posaddress_email";
					$values[] = dbquote($row["POS_email"]);

					$fields[] = "posaddress_store_postype";
					$values[] = dbquote($postype_id);

					$fields[] = "posaddress_store_furniture";
					$values[] = dbquote($productline_id);


					$fields[] = "posaddress_store_grosssurface";
					$values[] = dbquote(str_replace('m', '', $row['Gross_Surface']));

					$fields[] = "posaddress_store_totalsurface";
					$values[] = dbquote(str_replace('m', '', $row['Total_Surface']));

					$fields[] = "posaddress_store_retailarea";
					$values[] = dbquote(str_replace('m', '', $row['Retail_Area']));

					$fields[] = "posaddress_store_backoffice";
					$values[] = dbquote(str_replace('m', '', $row['Surface_Back_Office']));

					$fields[] = "posaddress_store_numfloors";
					$values[] = dbquote($row['Number_of_Floors']);

					$fields[] = "posaddress_store_floorsurface1";
					$values[] = dbquote($row['Surface_Floor_1']);

					$fields[] = "posaddress_store_floorsurface2";
					$values[] = dbquote($row['Surface_Floor_2']);

					$fields[] = "posaddress_store_floorsurface3";
					$values[] = dbquote($row['Surface_Floor_3']);

					$fields[] = "posaddress_store_openingdate";
					//$values[] = dbquote(excel2date($row['POS_OPENING_DATE']));
					$values[] = dbquote($row['POS_OPENING_DATE']);

					if(array_key_exists($row['Class/Image_Area'], $ratings)) {
						$fields[] = "posaddress_perc_class";
						$values[] = dbquote($ratings[$row['Class/Image_Area']]);
					}

					if(array_key_exists($row['Tourist/Historical_Area'], $ratings)) {
						$fields[] = "posaddress_perc_tourist";
						$values[] = dbquote($ratings[$row['Tourist/Historical_Area']]);
					}

					if(array_key_exists($row['Public_Transportation'], $ratings)) {
						$fields[] = "posaddress_perc_transport";
						$values[] = dbquote($ratings[$row['Public_Transportation']]);
					}

					if(array_key_exists($row['People_Traffic_Area'], $ratings)) {
						$fields[] = "posaddress_perc_people";
						$values[] = dbquote($ratings[$row['People_Traffic_Area']]);
					}

					if(array_key_exists($row['Parking_Possibilities'], $ratings)) {
						$fields[] = "posaddress_perc_parking";
						$values[] = dbquote($ratings[$row['Parking_Possibilities']]);
					}

					if(array_key_exists($row['Visibility_from_Pavement'], $ratings)) {
						$fields[] = "posaddress_perc_visibility1";
						$values[] = dbquote($ratings[$row['Visibility_from_Pavement']]);
					}

					if(array_key_exists($row['Visibility_from_accross_the_Street'], $ratings)) {
						$fields[] = "posaddress_perc_visibility2";
						$values[] = dbquote($row['Visibility_from_accross_the_Street']);
					}

					$fields[] = "posaddress_distribution_channel";
					$values[] = dbquote($distribution_channel_id);

					//othe fields
					$fields[] = "date_created";
					$values[] = dbquote(date("Y-m-d H:i:s"));

					$fields[] = "user_created";
					$values[] = dbquote(user_login());


					$sql = "insert into posaddresses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";


					if($error === 0) {
						mysql_query($sql) or dberror($sql);
						$pos_id = mysql_insert_id();
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " was inserted.";
					}
					else {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " error: " . $error;
					}
				}
			}

			if($pos_id > 0) {
				
				//AREAS

				$sql_d = "delete from posareas where posarea_posaddress = " . dbquote($pos_id);
				mysql_query($sql_d) or dberror($sql_d);

				if($row['Town/City_Centre'] == 'x') {
				
					$sql_i = "insert into posareas (posarea_posaddress, posarea_area, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "1, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";

				   if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " Area Town/City_Centre was inserted";
					}
				}

				
				if($row['Shopping_Mall'] == 'x') {
				
					$sql_i = "insert into posareas (posarea_posaddress, posarea_area, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "2, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";

					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " Shopping_Mall was inserted";
					}
				}

				


				if($row['Suburban_Area'] == 'x') {
				
					$sql_i = "insert into posareas (posarea_posaddress, posarea_area, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "3, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";
					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . "Suburban_Area was inserted";
					}
				}

				

				if($row['Airport_Landside'] == 'x') {
				
					$sql_i = "insert into posareas (posarea_posaddress, posarea_area, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "4, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";
					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . "Airport_Landside was inserted";
					}
				}

				
				
				if($row['Airport_Airside'] == 'x') {
				
					$sql_i = "insert into posareas (posarea_posaddress, posarea_area, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "5, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";
					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . "Airport_Airside was inserted";
					}
				}

				

				if($row['Train_Station'] == 'x') {
				
					$sql_i = "insert into posareas (posarea_posaddress, posarea_area, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "6, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";

					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . "Train_Station was inserted";
					}
				}

				

				if($row['Street_Level'] == 'x') {
				
					$sql_i = "insert into posareas (posarea_posaddress, posarea_area, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "7, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";
					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . "Street_Level";
					}
				}

				


				if($row['Other_Location']) {
				
					$sql_i = "insert into posareas (posarea_posaddress, posarea_area, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "8, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";
					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . "Other_Location";
					}
				}

				

				if($row['Department_Store'] == 'x') {
				
					$sql_i = "insert into posareas (posarea_posaddress, posarea_area, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "9, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";
					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . "Department_Store";
					}
				}

				//floor
				
				if($row['5th_floor'] == 'x') {
				
					$sql_u = "update posaddresses set posaddress_store_floor = 1 where posaddress_id = " . $pos_id;

					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " 5th_floor";
					}
	
				}

				if($row['4th_floor'] == 'x') {
				
					$sql_u = "update posaddresses set posaddress_store_floor = 2 where posaddress_id = " . $pos_id;

					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " 5th_floor";
					}
	
				}

				if($row['3rd_floor'] == 'x') {
				
					$sql_u = "update posaddresses set posaddress_store_floor = 3 where posaddress_id = " . $pos_id;

					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " 5th_floor";
					}
	
				}
				if($row['2nd_floor'] == 'x') {
				
					$sql_u = "update posaddresses set posaddress_store_floor = 4 where posaddress_id = " . $pos_id;

					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " 5th_floor";
					}
	
				}
				if($row['1st_floor'] == 'x') {
				
					$sql_u = "update posaddresses set posaddress_store_floor = 5 where posaddress_id = " . $pos_id;

					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " 5th_floor";
					}
	
				}
				if($row['Ground_floor_Street_Level'] == 'x') {
				
					$sql_u = "update posaddresses set posaddress_store_floor = 6 where posaddress_id = " . $pos_id;

					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " 5th_floor";
					}
	
				}

				if($row['Basement_1'] == 'x') {
				
					$sql_u = "update posaddresses set posaddress_store_floor = 7 where posaddress_id = " . $pos_id;

					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " 5th_floor";
					}
	
				}

				if($row['Basement_2'] == 'x') {
				
					$sql_u = "update posaddresses set posaddress_store_floor = 8 where posaddress_id = " . $pos_id;

					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " 5th_floor";
					}
	
				}

				if($row['Basement_3'] == 'x') {
				
					$sql_u = "update posaddresses set posaddress_store_floor = 9 where posaddress_id = " . $pos_id;

					if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " 5th_floor";
					}
	
				}

				
			

				//Customer Services

				$sql_d = "delete from posaddress_customerservices where posaddress_customerservice_posaddress_id = " . dbquote($pos_id);
				mysql_query($sql_d) or dberror($sql_d);

				if($row['Battery_Change'] == 'x') {
				
					$sql_i = "insert into posaddress_customerservices (posaddress_customerservice_posaddress_id, posaddress_customerservice_customerservice_id, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "1, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";

				   if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " Battery_Change was inserted";
					}
				}

				if($row['Polish_Glass'] == 'x') {
				
					$sql_i = "insert into posaddress_customerservices (posaddress_customerservice_posaddress_id, posaddress_customerservice_customerservice_id, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "2, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";

				   if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " Polish_Glass was inserted";
					}
				}

				if($row['Change_Strap'] == 'x') {
				
					$sql_i = "insert into posaddress_customerservices (posaddress_customerservice_posaddress_id, posaddress_customerservice_customerservice_id, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "3, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";

				   if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " Change_Strap was inserted";
					}
				}

				if($row['Strap_Adjustment'] == 'x') {
				
					$sql_i = "insert into posaddress_customerservices (posaddress_customerservice_posaddress_id, posaddress_customerservice_customerservice_id, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "4, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";

				   if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " Strap_Adjustment was inserted";
					}
				}

				if($row['Battery_Power_Check'] == 'x') {
				
					$sql_i = "insert into posaddress_customerservices (posaddress_customerservice_posaddress_id, posaddress_customerservice_customerservice_id, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "5, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";

				   if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " Battery_Power_Check was inserted";
					}
				}

				if($row['Buckle_Change'] == 'x') {
				
					$sql_i = "insert into posaddress_customerservices (posaddress_customerservice_posaddress_id, posaddress_customerservice_customerservice_id, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "6, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";

				   if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " Buckle_Change was inserted";
					}
				}

				if($row['Loop_Change'] == 'x') {
				
					$sql_i = "insert into posaddress_customerservices (posaddress_customerservice_posaddress_id, posaddress_customerservice_customerservice_id, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "7, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";

				   if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " Buckle_Change was inserted";
					}
				}


				if($row['Manual Glass Polish'] == 'x') {
				
					$sql_i = "insert into posaddress_customerservices (posaddress_customerservice_posaddress_id, posaddress_customerservice_customerservice_id, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "2, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";

				   if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " Manual Glass Polish was inserted";
					}
				}

				if($row['SWAP @ Customer Service'] == 'x') {
				
					$sql_i = "insert into posaddress_customerservices (posaddress_customerservice_posaddress_id, posaddress_customerservice_customerservice_id, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "8, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";

				   if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " SWAP @ Customer Service was inserted";
					}
				}

				if($row['Machine Glass Polish'] == 'x') {
				
					$sql_i = "insert into posaddress_customerservices (posaddress_customerservice_posaddress_id, posaddress_customerservice_customerservice_id, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "9, " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";

				   if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " Machine Glass Polish was inserted";
					}
				}



				//Leases

				$sql_d = "delete from posleases where poslease_posaddress = " . dbquote($pos_id);
				mysql_query($sql_d) or dberror($sql_d);

				if($row['Type'] == 'Initial') {
				
					$sql_i = "insert into posleases (poslease_posaddress, poslease_lease_type, poslease_startdate, poslease_enddate, poslease_signature_date,  poslease_isindexed, poslease_tacit_renewal_duration_years, poslease_tacit_renewal_duration_months, poslease_extensionoption, poslease_exitoption, poslease_termination_time, poslease_handoverdate, poslease_firstrentpayed, poslease_freeweeks, poslease_anual_rent, poslease_addcharges, poslease_salespercent, poslease_currency, user_created, date_created)
					         VALUES ( " . 
							 $pos_id . ", " .
							 "1, " .
							 dbquote($row['Start_Date'] ? excel2date($row['Start_Date']):'') . ", " .
							 dbquote($row['Expiry_Date'] ? excel2date($row['Expiry_Date']):'') . ", " .
							 dbquote($row['Lease_Contract_Signed_on'] ? excel2date($row['Lease_Contract_Signed_on']): '') . ", " .
							 dbquote($row['contains_tacit_renewal_clause']? 1:0) . ", " .
							 dbquote($row['Tacit_renewal_duration_years']) . ", " .
							 dbquote($row['Tacit_renewal_duration_months']) . ", " .
							 dbquote($row['Extension_Option_to_Date'] ? excel2date($row['Extension_Option_to_Date']) :'') . ", " .
							 dbquote($row['Exit_Option_to_Date'] ? excel2date($row['Exit_Option_to_Date']):'') . ", " .
							 dbquote($row['Termination_deadline_in_months']) . ", " .
							 dbquote($row['Handover_Date_Key'] ? excel2date($row['Handover_Date_Key']):'') . ", " .
							 dbquote($row['First_Rent_Paid_on'] ? excel2date($row['First_Rent_Paid_on']):'') . ", " .
							 dbquote($row['Rent_Free_Period_in_Weeks']) . ", " .
							 dbquote($row['Average_of_annual_base_rent_paid_during_rent_period']) . ", " .
							 dbquote($row['Additional_charges']) . ", " .
							 dbquote($row['Percentage_if_part_of_the_rent_is_turnover_based']) . ", " .
							 dbquote($row['Currency']) . ", " .
							 "'anton.hofmann', " .
							 dbquote(date('Y-m-d H:i:s')) . "); ";

				   if(mysql_query($sql_i) or dberror($sql_i)) {
						$debug_info[] = "POS Location " . $row['POS_NAME'] . " Pos Lease was inserted";
					}
				}
				elseif($row['Type'] == 'Extension') {
				
					
				}
			}
		}
	 }
}

echo '<pre>';
print_r($debug_info);
?>