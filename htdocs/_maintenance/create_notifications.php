<?php



require_once "../include/frame.php";

check_access("can_edit_catalog");

//import POS locations
$sql = "select * from projecttype_newproject_notifications";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res)) {

	$sql_c = "select country_id from countries where country_id <> 118";
	$res_c = mysql_query($sql_c ) or dberror($sql_c );
	while($row_c  = mysql_fetch_assoc($res_c )) {
		
		$fields = array();
		$values = array();

		//imported fields

		$fields[] = "projecttype_newproject_notification_country";
		$values[] = dbquote($row_c["country_id"]);

		$fields[] = "projecttype_newproject_notification_legal_type";
		$values[] = dbquote($row["projecttype_newproject_notification_legal_type"]);

		$fields[] = "projecttype_newproject_notification_postype";
		$values[] = dbquote($row["projecttype_newproject_notification_postype"]);

		$fields[] = "projecttype_newproject_notification_email";
		$values[] = dbquote($row["projecttype_newproject_notification_email"]);

		$fields[] = "projecttype_newproject_notification_emailcc1";
		$values[] = dbquote($row["projecttype_newproject_notification_emailcc1"]);

		$fields[] = "projecttype_newproject_notification_emailcc2";
		$values[] = dbquote($row["projecttype_newproject_notification_emailcc2"]);

		$fields[] = "projecttype_newproject_notification_emailcc3";
		$values[] = dbquote($row["projecttype_newproject_notification_emailcc3"]);


		$fields[] = "projecttype_newproject_notification_emailcc4";
		$values[] = dbquote($row["projecttype_newproject_notification_emailcc4"]);

		$fields[] = "projecttype_newproject_notification_emailcc5";
		$values[] = dbquote($row["projecttype_newproject_notification_emailcc5"]);

		$fields[] = "projecttype_newproject_notification_emailcc6";
		$values[] = dbquote($row["projecttype_newproject_notification_emailcc6"]);

		$fields[] = "projecttype_newproject_notification_emailcc7";
		$values[] = dbquote($row["projecttype_newproject_notification_emailcc7"]);

		
		
		$fields[] = "projecttype_newproject_notification_on_new_project";
		$values[] = dbquote($row["projecttype_newproject_notification_on_new_project"]);
		
		$fields[] = "projecttype_newproject_notification_on_lnsubmission";
		$values[] = dbquote($row["projecttype_newproject_notification_on_lnsubmission"]);

		$fields[] = "projecttype_newproject_notification_on_lnresubmission";
		$values[] = dbquote($row["projecttype_newproject_notification_on_lnresubmission"]);

		$fields[] = "projecttype_newproject_notification_on_afsubmission";
		$values[] = dbquote($row["projecttype_newproject_notification_on_afsubmission"]);

		$fields[] = "projecttype_newproject_notification_oncerafsubmission";
		$values[] = dbquote($row["projecttype_newproject_notification_oncerafsubmission"]);

		$fields[] = "projecttype_newproject_notification_on_afresubmission";
		$values[] = dbquote($row["projecttype_newproject_notification_on_afresubmission"]);

		$fields[] = "projecttype_newproject_notification_oncerafresubmission";
		$values[] = dbquote($row["projecttype_newproject_notification_oncerafresubmission"]);

		$fields[] = "projecttype_newproject_notification_oninr03submission";
		$values[] = dbquote($row["projecttype_newproject_notification_oninr03submission"]);

		$fields[] = "projecttype_newproject_notification_oninr03resubmission";
		$values[] = dbquote($row["projecttype_newproject_notification_oninr03resubmission"]);


		$fields[] = "projecttype_newproject_notification_on_rfafsubmission";
		$values[] = dbquote($row["projecttype_newproject_notification_on_rfafsubmission"]);

		$fields[] = "projecttype_newproject_notification_on_rfafresubmission";
		$values[] = dbquote($row["projecttype_newproject_notification_on_rfafresubmission"]);


		$fields[] = "date_created";
		$values[] = dbquote(date("Y-m-d H:i:s"));

		$fields[] = "user_created";
		$values[] = dbquote(user_login());


		$sql_i = "insert into projecttype_newproject_notifications (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		mysql_query($sql_i) or dberror($sql_i);
	}
}
		

echo 'done';

?>