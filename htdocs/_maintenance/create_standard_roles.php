<?php



require_once "../include/frame.php";

check_access("can_edit_catalog");

//import POS locations
$sql = "select * from standardroles";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res)) {

	$sql_c = "select country_id from countries where country_id <> 118";
	$res_c = mysql_query($sql_c ) or dberror($sql_c );
	while($row_c  = mysql_fetch_assoc($res_c )) {
		
		$fields = array();
		$values = array();

		//imported fields

		$fields[] = "standardrole_country";
		$values[] = dbquote($row_c["country_id"]);

		$fields[] = "standardrole_postype";
		$values[] = dbquote($row["standardrole_postype"]);

		$fields[] = "standardrole_legaltype";
		$values[] = dbquote($row["standardrole_legaltype"]);

		$fields[] = "standardrole_rtco_user";
		$values[] = dbquote($row["standardrole_rtco_user"]);

		$fields[] = "standardrole_dsup_user";
		$values[] = dbquote($row["standardrole_dsup_user"]);

		$fields[] = "standardrole_rtop_user";
		$values[] = dbquote($row["standardrole_rtop_user"]);

		$fields[] = "standardrole_cms_approver_user";
		$values[] = dbquote($row["standardrole_cms_approver_user"]);

		$fields[] = "date_created";
		$values[] = dbquote(date("Y-m-d H:i:s"));

		$fields[] = "user_created";
		$values[] = dbquote(user_login());


		$sql_i = "insert into standardroles (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		mysql_query($sql_i) or dberror($sql_i);
	}
}
		

echo 'done';

?>