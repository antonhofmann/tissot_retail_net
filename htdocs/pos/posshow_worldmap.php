<?php
/********************************************************************

    posshow_worldmap.php

    Shows the world map

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2010-11-20
    Version:        2.0.0

    Copyright (c) 2008-2010, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
check_access("can_use_posindex");

$link = "";
//prepare data
$sql_salesregions = "select salesregion_id, salesregion_name " . 
                    "from salesregions " . 
					"order by salesregion_name";


if(param("salesregion"))
{
	$sql_countries = "select country_id, country_name " . 
				   "from countries " . 
				   "where country_salesregion = " . param("salesregion") .  
				   " order by country_name";
}
else
{
	$sql_countries = "select country_id, country_name " . 
				   "from countries " . 
				   " order by country_name";
}


//get pos types
$pos_types = array();
$sql_postypes = "select postype_id, postype_name " . 
			    "from postypes " . 
			    " order by postype_name";

$res = mysql_query($sql_postypes) or dberror($postype_name);

while ($row = mysql_fetch_assoc($res))
{
    $pos_types[$row["postype_id"]] = $row["postype_name"];
}

//get legal types
$legal_types = array();
$sql_legaltypes = "select project_costtype_id, project_costtype_text " . 
					"from project_costtypes " . 
					"where project_costtype_id IN(1,2,6) " . 
					" order by project_costtype_text";


$res = mysql_query($sql_legaltypes) or dberror($sql_legaltypes);

while ($row = mysql_fetch_assoc($res))
{
    $legal_types[$row["project_costtype_id"]] = $row["project_costtype_text"];
}


//get distribution channels
$sql_provinces = "";
$tmp_filter = '';
if(param("country"))
{
	$dc = array();
	$sql = "select distinct posaddress_distribution_channel " .
		   "from posaddresses " . 
		   " where posaddress_distribution_channel > 0 and posaddress_country = " . param("country");

	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{
		$dc[] = $row['posaddress_distribution_channel'];
	}

	if(count($dc) > 0)
	{
		$tmp_filter = " and mps_distchannel_id IN (" . implode(', ', $dc) . ") ";
	}
	else
	{
		$tmp_filter = " and mps_distchannel_id = 9999999999 ";
	}

	$sql_provinces = "select province_id, province_canton " . 
		             " from provinces " . 
		             " where province_country = " . param("country") . 
					 " order by province_canton ";


}

$distribution_channels = array();
$sql_distchannels = "select mps_distchannel_id, concat(mps_distchannel_code, ' ', mps_distchannel_name) as dname " . 
					"from mps_distchannels" . 
					" where mps_distchannel_active = 1 " .
					$tmp_filter . 
					" order by mps_distchannel_code";

$res = mysql_query($sql_distchannels) or dberror($sql_distchannels);

while ($row = mysql_fetch_assoc($res))
{
    $distribution_channels[$row["mps_distchannel_id"]] = $row["dname"];
}


//get turn over classes watches

$tmp_filter = '';
if(param("country"))
{
	$tcw = array();
	$sql = "select distinct posaddress_turnoverclass_watches " .
		   "from posaddresses " . 
		   " where posaddress_turnoverclass_watches > 0 and posaddress_country = " . param("country");

	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{
		$tcw[] = $row['posaddress_turnoverclass_watches'];
	}

	if(count($tcw) > 0)
	{
		$tmp_filter = " and mps_turnoverclass_id IN (" . implode(', ', $tcw) . ") ";
	}
	else
	{
		$tmp_filter = " and mps_turnoverclass_id = 9999999999 ";
	}

}

$turnover_classes = array();
$sql_turnoverclasses = "select mps_turnoverclass_id, mps_turnoverclass_code " . 
					"from mps_turnoverclasses" . 
					" where mps_turnoverclass_group_id = 1 " . 
					$tmp_filter . 
					" order by mps_turnoverclass_code";


$res = mysql_query($sql_turnoverclasses) or dberror($sql_turnoverclasses);

while ($row = mysql_fetch_assoc($res))
{
    $turnover_classes[$row["mps_turnoverclass_id"]] = $row["mps_turnoverclass_code"];
}


/********************************************************************
Render Form
*********************************************************************/

$form = new Form("posaddresses", "World Map");

$form->add_section("Selection Criteria");


$form->add_list("salesregion", "Sales Region",$sql_salesregions, SUBMIT);
$form->add_list("country", "Country",$sql_countries, SUBMIT);

if($sql_provinces)
{
	$form->add_list("province", "Province", $sql_provinces);
}
else
{
	$form->add_hidden("province");
}


$form->add_section("Project Pipeline");
$form->add_checkbox("pip", "Add projects in progress from pipeline", false, 0, "Pipeline");
$form->add_checkbox("pip2", "Add projects on hold from pipeline", false, 0, "");
$form->add_checkbox("opip", "Show only projects from pipeline", false, 0, "");

$form->add_checkbox("lnaf1", "Show projects from pipeline where LN/AF is submitted but not approved", false, 0, "LN/AF");
$form->add_checkbox("lnaf2", "Show projects from pipeline where LN/AF is approved", false, 0, "");


$form->add_section("Legal Types");
$l_check_box_names = array();
foreach ($legal_types as $id=>$name)
{
	$form->add_checkbox("L" . $id, $name, 1, 0, "");
	$l_check_box_names["L" . $id] = "L" . $id;
}

$form->add_section("Pos Types");
$p_check_box_names = array();
foreach ($pos_types as $id=>$name)
{
	$form->add_checkbox("P" . $id, $name, 1, 0, "");
	$p_check_box_names["P" . $id] = "P" . $id;
}


$form->add_section("Distribution Channels");
$d_check_box_names = array();
$form->add_label("select0", "Distribution Channels", RENDER_HTML, "<div id='select0'><a href='javascript:select_all_dchannels();'>select all distribution channels</a></div>");
foreach ($distribution_channels as $id=>$name)
{
	$form->add_checkbox("D" . $id, $name, 0, 0, "");
	$d_check_box_names["D" . $id] = "D" . $id;
}


$form->add_section("Turnover Class Watches");
$t_check_box_names = array();
$form->add_label("select1", "Turnover Classes", RENDER_HTML, "<div id='select1'><a href='javascript:select_all_tclasses();'>select all classes</a></div>");
foreach ($turnover_classes as $id=>$name)
{
	$form->add_checkbox("T" . $id, $name, 0, 0, "");
	$t_check_box_names["T" . $id] = "T" . $id;
}

$form->add_section("Map Options");
$form->add_checkbox("closed", "Only show operating POS locations", false, 0, "POS Locations");

$form->add_checkbox("lb1", "Add labels for POS names", false, 0, "POS Name");
$form->add_checkbox("lb2", "Add labels for distribution channels", false, 0, "Distribution Channel");
$form->add_checkbox("lb3", "Add labels for turn over classes", false, 0, "Turnover Class");

$form->add_button("show_map", "Show Map");

$form->populate();
$form->process();


if($form->validate() and $form->button("show_map") or $form->button("show_map1"))
{
	
	$getparams = '';

	if(param("salesregion"))
	{
		$getparams = "?r=" . param("salesregion");
	}


	if($getparams and param("country"))
	{
		$getparams .= "&c=" . param("country");
	}
	elseif(param("country"))
	{
		$getparams = "?c=" . param("country");
	}
	
	
	if(param("province"))
	{
		$getparams .= "&p=" . param("province");
	}

	
	
	$s_ptypes = array();
	foreach($p_check_box_names as $key=>$name)
	{
		if($form->value($name) == 1)
		{
			$s_ptypes[] = substr($name, 1, strlen($name));
		}
	}
	
	if($getparams)
	{
		$getparams .= "&t=" . implode("-", $s_ptypes);
	}
	else
	{
		$getparams = "?t=" . implode("-", $s_ptypes);
	}


	$s_ltypes = array();
	foreach($l_check_box_names as $key=>$name)
	{
		if($form->value($name) == 1)
		{
			$s_ltypes[] = substr($name, 1, strlen($name));
		}
	}

	if($getparams)
	{
		$getparams .= "&l=" . implode("-", $s_ltypes);
	}
	else
	{
		$getparams = "?l=" . implode("-", $s_ltypes);
	}


	$s_distributionchannles = array();
	foreach($d_check_box_names as $key=>$name)
	{
		if($form->value($name) == 1)
		{
			$s_distributionchannles[] = substr($name, 1, strlen($name));
		}
	}

	if($getparams)
	{
		$getparams .= "&d=" . implode("-", $s_distributionchannles);
	}
	else
	{
		$getparams = "?d=" . implode("-", $s_distributionchannles);
	}


	$s_turnoverclasses = array();
	foreach($t_check_box_names as $key=>$name)
	{
		if($form->value($name) == 1)
		{
			$s_turnoverclasses[] = substr($name, 1, strlen($name));
		}
	}

	if($getparams)
	{
		$getparams .= "&tc=" . implode("-", $s_turnoverclasses);
	}
	else
	{
		$getparams = "?tc=" . implode("-", $s_turnoverclasses);
	}


	
	//Map Options
	
	
	if($getparams and param("closed"))
	{
		$getparams .= "&o=" . param("closed");
	}
	elseif(param("closed"))
	{
		$getparams = "?o=" . param("closed");
	}


	if($getparams and param("lb1"))
	{
		$getparams .= "&lb1=1";
	}
	elseif(param("lb1"))
	{
		$getparams = "&lb1=1";
	}

	if($getparams and param("lb2"))
	{
		$getparams .= "&lb2=1";
	}
	elseif(param("lb2"))
	{
		$getparams = "&lb2=1";
	}

	if($getparams and param("lb3"))
	{
		$getparams .= "&lb3=1";
	}
	elseif(param("lb3"))
	{
		$getparams = "&lb3=1";
	}


	
	if($getparams and param("pip"))
	{
		$getparams .= "&pip=" . param("pip");
	}
	elseif(param("pip"))
	{
		$getparams = "?pip=" . param("pip");
	}

	if($getparams and param("pip2"))
	{
		$getparams .= "&pip2=" . param("pip2");
	}
	elseif(param("pip2"))
	{
		$getparams = "?pip2=" . param("pip2");
	}


	if($getparams and param("opip"))
	{
		$getparams .= "&opip=" . param("opip");
	}
	elseif(param("opip"))
	{
		$getparams = "?opip=" . param("opip");
	}

	if($getparams and param("lnaf1"))
	{
		$getparams .= "&lnaf1=" . param("lnaf1");
	}
	elseif(param("lnaf1"))
	{
		$getparams = "?lnaf1=" . param("lnaf1");
	}

	if($getparams and param("lnaf2"))
	{
		$getparams .= "&lnaf2=" . param("lnaf2");
	}
	elseif(param("lnaf2"))
	{
		$getparams = "?lnaf2=" . param("lnaf2");
	}

	



	if(count($s_ltypes) == 0 or $s_ptypes == 0)
	{
		$form->error("Please select at least one Legal Type and one POS Type");
	}
	else
	{
		$link = "posworldmap.php" . $getparams;
		//redirect($link);
	}
}


// Render page

$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: World Map");
$form->render();



?>
<script language='javascript'>

	function select_all_dchannels()
	{
		<?php
		foreach($distribution_channels as $id=>$name)
		{
			echo "$('input[name=D" . $id . "]').attr('checked', true);";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:deselect_all_dchannels();'>deselect all distirbution channels</a>";
		
		
	}


	function deselect_all_dchannels()
	{
		<?php
		foreach($distribution_channels as $id=>$name)
		{
			echo "$('input[name=D" . $id . "]').attr('checked', false);";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:select_all_dchannels();'>select all distirbution channels</a>";
	}

	function select_all_tclasses()
	{
		<?php
		foreach($turnover_classes as $id=>$name)
		{
			echo "$('input[name=T" . $id . "]').attr('checked', true);";
		}
		?>

		var div = document.getElementById('select1');
		div.innerHTML = "<a href='javascript:deselect_all_tclasses();'>deselect all classes</a>";
		
		
	}


	function deselect_all_tclasses()
	{
		<?php
		foreach($turnover_classes as $id=>$name)
		{
			echo "$('input[name=T" . $id . "]').attr('checked', false);";
		}
		?>

		var div = document.getElementById('select1');
		div.innerHTML = "<a href='javascript:select_all_tclasses();'>select all classes</a>";
	}
</script>

<?php
if($link) 
{
?>
<script>
	window.open('<?php echo $link;?>');
</script>
<?php 
}

$page->footer();

?>