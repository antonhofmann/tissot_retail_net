<?php
/********************************************************************

    customer_services.php

    Lists customer servicesfor editing.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-08-08
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-08-08
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("customer_service.php");

$list = new ListView("select customerservice_id, customerservice_text from customerservices");

$list->set_entity("customerservices");
$list->set_order("customerservice_text");

$list->add_column("customerservice_text", "Service", "customer_service.php", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_NEW, "New", "customer_service.php");

$list->process();



$sql = "SELECT posaddress_client_id, country_name, address_company,
count(posaddress_customerservices.posaddress_customerservice_id) as num_recs,
count(DISTINCT posaddress_customerservices.posaddress_customerservice_posaddress_id) as num_recs2
FROM
posaddresses
LEFT JOIN posaddress_customerservices ON posaddresses.posaddress_id = posaddress_customerservices.posaddress_customerservice_posaddress_id
INNER JOIN addresses ON posaddresses.posaddress_client_id = addresses.address_id
INNER JOIN countries on country_id = address_country
where (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null)  
group by country_name, addresses.address_company, posaddress_client_id";



//get number of operating POS locations
$pos_locations  = array();
$sql_i = "select posaddress_client_id, count(posaddress_id) as num_recs " . 
         "from posaddresses " . 
		 "where (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null) " . 
		 " group by posaddress_client_id";

$res = mysql_query($sql_i) or dberror($sql_i);
while ($row = mysql_fetch_assoc($res))
{
	$pos_locations[$row["posaddress_client_id"]] = $row["num_recs"];
}



$list1 = new ListView($sql);

$list1->set_entity("posaddresses");

$list1->add_column("country_name", "Country", "");
$list1->add_column("address_company", "Company", "");
$list1->add_text_column("num_of_pos", "#POS", COLUMN_ALIGN_RIGHT, $pos_locations);
$list1->add_column("num_recs2", "#POS with CS", "", "", "", COLUMN_ALIGN_RIGHT);
$list1->add_column("num_recs", "#CS", "", "", "", COLUMN_ALIGN_RIGHT);


$list1->add_button("print", "Print List");


$list->process();

if($list1->button("print"))
{
	$link = "customer_services_xls.php";
	redirect($link);
}

$page = new Page("customerservices");

$page->header();
$page->title("Customer Services");
$list->render();
echo "<p>&nbsp;</p>";
$list1->render();
$page->footer();

?>
