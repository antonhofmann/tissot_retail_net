<?php
/********************************************************************

    posindex_preselect.php

    Preselection of POS List

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/access_filters.php";

check_access("can_use_posindex");
set_referer("posindex.php");

$postype_filter = array();
$postype_filter["all"] = "All";
$sql = "select * from postypes order by postype_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$postype_filter[$row["postype_id"]] = $row["postype_name"];
}

$possubclass_filter["all"] = "All";
$sql_sc = "select * from possubclasses order by possubclass_name";
$res_sc = mysql_query($sql_sc) or dberror($sql_sc);
while($row_sc = mysql_fetch_assoc($res_sc))
{
	$possubclass_filter[$row_sc["possubclass_id"]] = $row_sc["possubclass_name"];
}



$legaltype_filter = array();
$legaltype_filter["all"] = "All";
$sql = "select * from project_costtypes where project_costtype_id in (1, 2, 6) order by project_costtype_text";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$legaltype_filter[$row["project_costtype_id"]] = $row["project_costtype_text"];
}


$states = array();
$states[1] = "Operating POS locations only";
$states[2] = "Closed POS locations only";

//compose list
if(has_access("can_view_posindex") or has_access("can_edit_posindex"))
{
	$sql = "select DISTINCT country_id, country_name " .
		   "from posaddresses " . 
		   "left join countries on posaddress_country = country_id " . 
		   "where country_name is not null " . 
		   "order by country_name";
}
else
{

	$user = get_user(user_id());

	
	$country_filter = "";
	$country_access_filter = "";
	$country_travelling_retail_filter = "";
	
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
	}



	$country_access_filter =  get_users_regional_access_to_poslocations(user_id());


	if(has_access("has_access_to_all_travalling_retail_data"))
	{
		$tmp = get_country_access_for_travelling_retail_pos();

		if(count($tmp) > 0) {
			$country_travelling_retail_filter = " country_id IN (" . implode(",", $tmp) . ") ";
		}
	}


	if($country_access_filter and $country_travelling_retail_filter)
	{
		if($country_filter)
		{
			$country_filter = "(" . $country_filter .  " or " . $country_access_filter .  " or " . $country_travelling_retail_filter . ")";
		}
		else
		{
			$country_filter = "(" . $country_access_filter .  " or " . $country_travelling_retail_filter . ")";
		}
	}
	elseif($country_access_filter)
	{
		if($country_filter)
		{
			$country_filter = "(" . $country_filter .  " or " . $country_access_filter . ")";
		}
		else
		{
			$country_filter = $country_access_filter;
		}
	}
	elseif($country_travelling_retail_filter)
	{
		if($country_filter)
		{
			$country_filter = "(" . $country_filter .  " or " . $country_travelling_retail_filter . ")";
		}
		else
		{
			$country_filter = $country_travelling_retail_filter;
		}
	}

	if($country_filter == "")
	{

		$sql = "select DISTINCT country_id, country_name " .
			   "from posaddresses " . 
			   "left join countries on posaddress_country = country_id " .
			   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
			   "left join postypes on postype_id = posaddress_store_postype " .
			   "where country_name is not null " .
			   " and posaddress_client_id = " . $user["address"] . 
			   //" and (postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) " . 
			   " order by country_name";
	}
	elseif($country_travelling_retail_filter)
	{
		$sql = "select DISTINCT country_id, country_name " .
			   "from posaddresses " . 
			   "left join countries on posaddress_country = country_id " .
			   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
			   "left join postypes on postype_id = posaddress_store_postype " .
			   "where country_name is not null " .
			   " and (posaddress_client_id = " . $user["address"] .
			   " or " . $country_filter . ") " . 
			   //" and (postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) " . 
			   " order by country_name";
	}
	else
	{
		$sql = "select DISTINCT country_id, country_name " .
			   "from posaddresses " . 
			   "left join countries on posaddress_country = country_id " .
			   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
			   "left join postypes on postype_id = posaddress_store_postype " .
			   "where country_name is not null " .
			   " and " . $country_filter . 
			   //" and (postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) " . 
			   " order by country_name";
	}

	
}

if(param("country"))
{
	$sql_p = "select province_id, province_canton from provinces  " . 
		     "where province_country = " . dbquote(param("country")) .
		     " order by province_canton";
}

$form = new Form("posaddresses", "posaddress");

$form->add_section("Filter Selection");

$form->add_list("country", "Country Selection",$sql, SUBMIT);
if(param("country"))
{
	$form->add_list("province", "Province",$sql_p);
}
else
{
	$form->add_hidden("province");
}

$selected_l = "all";
if(param('legaltype') > 0)
{
	$selected_l = param('legaltype');
}
$form->add_list("legaltype", "Legal Type",$legaltype_filter, 0, $selected_l);


$selected_p = "all";
if(param('ltf') > 0)
{
	$selected_p = param('ltf');
}

$selected_psc = "all";
if(param('psc') > 0)
{
	$selected_psc = param('psc');
}


$form->add_list("ltf", "POS Type",$postype_filter, 0, $selected_p);

$form->add_list("psc", "POS Subclass",$possubclass_filter, 0, $selected_psc);

$form->add_list("states", "Operating/Closed", $states, 0, 1);


$form->add_button("show_pos", "Show List");



$form->populate();
$form->process();




if($form->button("show_pos"))
{
	redirect("posindex.php?country=" . $form->value("country") . "&province=" . $form->value("province"). "&ostate=" . $form->value("states") . "&ltf=" . $form->value("ltf") . "&psc=" . $form->value("psc") . '&province=' . param("province") . '&let=' . param("legaltype"));
}

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index");
$form->render();
?>

	<script type="text/javascript">
		
		document.onkeydown = process_key;
		
		function process_key(e)
		{
		  if( !e ) 
		  {
			if( window.event ) 
			{
			  e = window.event;
			} 
			else 
			{
			  return;
			}
		  }

		  if(e.keyCode==13)
		  {
			  button('show_pos');
		  }
		}
	</script>

	<?php

$page->footer();

?>
