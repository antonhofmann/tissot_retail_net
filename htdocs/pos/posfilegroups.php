<?php
/********************************************************************

    posfilegroups.php

    Lists file groups for editing.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("posfilegroup.php");

$list = new ListView("select posfilegroup_id, posfilegroup_name from posfilegroups");

$list->set_entity("filegroups");
$list->set_order("posfilegroup_name");

$list->add_column("posfilegroup_name", "Name", "posfilegroup.php", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_NEW, "New", "posfilegroup.php");

$list->process();

$page = new Page("posfilegroups");

$page->header();
$page->title("File groups");
$list->render();
$page->footer();

?>
