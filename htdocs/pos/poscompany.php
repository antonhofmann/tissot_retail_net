<?php
/********************************************************************

    poscompany.php

    Creation and mutation of address records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_use_posindex");

set_referer("poscompany_file.php");



$country = param("country");


if(!$country and array_key_exists("country", $_SESSION) and $_SESSION["country"] > 0)
{
	$country = $_SESSION["country"];
}
else
{
	$_SESSION["country"] = $country;
}


$client_type = 0;

$sql = "select * from addresses where address_id = " . id();
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$client_type = $row["address_client_type"];
}
elseif(param("address_client_type") > 0)
{
	$client_type = param("address_client_type");
}


//check if user can edit this company
$user = get_user(user_id());
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex") and $user["country"] == param("country"))
{
	$can_edit = true;
}


if(param("address_country"))
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = " . dbquote(param("address_country")) . " order by place_name";
}
else
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = {address_country} order by place_name";
}





/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("addresses", "address");

$form->add_hidden("active", param("active"));
$form->add_hidden("country", $country);
$form->add_hidden("address_showinposindex", 1);

$form->add_hidden("address_filter", param("address_filter"));

$form->add_section("Name and Address");

if(has_access("can_edit_posindex"))
{
	//$form->add_edit("address_shortcut", "Shortcut*", NOTNULL);
	
	if($client_type > 0)
	{
		$form->add_edit("address_number", "Tissot SAP Customer Code");
	}
	
	if($client_type == 2 or $client_type == 3)
	{
		$form->add_edit("address_legnr", "SG Legal Number");
	}
	else
	{
		$form->add_hidden("address_legnr");
	}
	
	$form->add_label("address_sapnr", "SAP Customer Code");
	
	//$form->add_edit("address_mps_customernumber", "Customer Number");

	
	$form->add_edit("address_company", "Company*", NOTNULL, "", TYPE_CHAR, 0, 0, 2, "address_company");
	$form->add_edit("address_company2", "", 0, "", TYPE_CHAR, 0, 0, 2, "address_company2");

	$form->add_hidden("address_address");
	$form->add_multi_edit("street", array("address_street", "address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6), array('',''), 2, "address_address", '', array(40,5));

	$form->add_edit("address_address2", "Additional Address Info", 0, "", TYPE_CHAR, 0, 0, 2, "address_address2");
	
	
	$form->add_list("address_country", "Country*",
    "select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);

	$form->add_edit("address_zip", "Zip");
	$form->add_list("address_place_id", "City*", $sql_places, NOTNULL | SUBMIT);
	$form->add_edit("address_place", "", DISABLED);
	

	$form->add_section("Communication");

	$form->add_hidden("address_phone");
	$form->add_multi_edit("phone_number", array("address_phone_country", "address_phone_area", "address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_hidden("address_mobile_phone");
	$form->add_multi_edit("mobile_phone_number", array("address_mobile_phone_country", "address_mobile_phone_area", "address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_edit("address_email", "Company Email");
	$form->add_edit("address_website", "Website");
	$form->add_edit("address_contact_name", "Contact Name");
	$form->add_edit("address_contact_email", "Contact Email");

	
	$form->add_section("Miscellanous");
	$form->add_list("address_type", "Type",
		"select address_type_id, address_type_name from address_types  where (address_type_id = 7 or address_type_id = 1) order by address_type_name", NOTNULL);

	$form->add_section("");
	$sql_addresses = "select address_id, concat(country_name, ': ', address_company) as company from addresses " .
                 "left join countries on country_id = address_country " .
				 "where address_type = 1  order by country_name, address_company";

	$form->add_list("address_parent", "Parent*",$sql_addresses);

	$form->add_checkbox("address_active", "Address in Use", true);
	//$form->add_checkbox("address_canbefranchisor", "Can Be Franchisor", false);
	$form->add_checkbox("address_canbefranchisee", "Can Be Franchisee", false);
	$form->add_checkbox("address_canbefranchisee_worldwide", "Franchisee can have worldwide projects");
	$form->add_checkbox("address_canbejointventure", "Is a Joint Venture Partner");
	$form->add_checkbox("address_canbecooperation", "Is a Cooperation Partner");
	$form->add_checkbox("address_swatch_retailer", "Company is a present Tissot retailer");
	$form->add_checkbox("address_is_independent_retailer", "Company is an independent retailer");
	$form->add_checkbox("address_can_own_independent_retailers", "Company can own independent retailers");
	
	$form->add_button("save", "Save");
	$form->add_button(FORM_BUTTON_DELETE, "Delete");
}
elseif($can_edit == true and has_access("can_edit_his_posindex"))
{
	$form->add_lookup("address_type", "Type", "address_types", "address_type_name");
	//$form->add_label("address_shortcut", "Shortcut");
	$form->add_label("address_number", "Tissot SAP Customer Code");
	$form->add_label("address_legnr", "SG Legal Number");
	$form->add_label("address_sapnr", "SAP Customer Code");
	$form->add_edit("address_company", "Company*", NOTNULL);
	$form->add_edit("address_company2", "");
	$form->add_hidden("address_address");
	$form->add_multi_edit("street", array("address_street", "address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6), array('',''), '', '', '', array(40,5));


	$form->add_edit("address_address2", "Additional Address Info");
	
	$form->add_list("address_country", "Country*",
    "select country_id, country_name from countries order by country_name", SUBMIT);

	$form->add_edit("address_zip", "Zip");
	$form->add_list("address_place_id", "City*", $sql_places, NOTNULL | SUBMIT);
	$form->add_edit("address_place", "", DISABLED);

	$form->add_section("Communication");
	$form->add_hidden("address_phone");
	$form->add_multi_edit("phone_number", array("address_phone_country", "address_phone_area", "address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_hidden("address_mobile_phone");
	$form->add_multi_edit("mobile_phone_number", array("address_mobile_phone_country", "address_mobile_phone_area", "address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_edit("address_email", "Company Email");
	$form->add_edit("address_website", "Website");
	$form->add_edit("address_contact_name", "Contact Name");
	$form->add_edit("address_contact_email", "Contact Email");

	$form->add_section(" ");
	
	$form->add_button("save", "Save");
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$form->add_lookup("address_type", "Type", "address_types", "address_type_name");
	//$form->add_label("address_shortcut", "Shortcut");
	$form->add_label("address_number", "Tissot SAP Customer Code");
	$form->add_label("address_legnr", "SG Legal Number");
	$form->add_label("address_sapnr", "SAP Customer Code");
	$form->add_label("address_company", "Company");
	$form->add_label("address_company2", "");
	$form->add_label("address_address", "Street");
	$form->add_label("address_address2", "Additional Address Info");
	$form->add_label("address_zip", "Zip");
	$form->add_label("address_place", "City");
	$form->add_lookup("address_country", "Country", "countries", "country_name");

	$form->add_section("Communication");
	$form->add_label("address_phone", "Phone");
	$form->add_label("address_mobile_phone", "Mobile");
	$form->add_label("address_email", "Company Email");
	$form->add_label("address_website", "Website");
	$form->add_label("address_contact_name", "Contact Name");
	$form->add_label("address_contact_email", "Contact Email");
}

$form->add_button("back", "Back to Company List");

//$form->add_validation("is_email_address({address_email})", "The company's email address is invalid.");
//$form->add_validation("is_web_address({address_website})", "The website url is invalid.");
//$form->add_validation("is_email_address({address_contact_email})", "The contact's email address is invalid.");



/********************************************************************
    Create List of Ownership of operating POS Locations
*********************************************************************/
$sql_pa = "select posaddress_id, posaddress_name, posaddress_zip, " . 
		   "country_name, posaddress_store_closingdate, postype_name, project_costtype_text, " . 
		   " DATE_FORMAT(posaddress_store_openingdate, '%d.%m.%Y') as openingdate " . 
		   "from posaddresses " .
		   "left join countries on country_id = posaddress_country " .
		   "left join postypes on postype_id = posaddress_store_postype " .
		   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
		   "left join places on place_id = posaddress_place_id";

$list_filter_pa = "posaddress_franchisee_id = " . dbquote(id()) .
                  " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')";

$list_pa = new ListView($sql_pa);
$list_pa->set_title("Operating POS Locations");
$list_pa->set_entity("posorders");
$list_pa->set_filter($list_filter_pa);
$list_pa->set_order("project_costtype_text, posaddress_store_openingdate");

$link = APPLICATION_URL . "/pos/posindex_pos.php?id={posaddress_id}";

$list_pa->add_column("country_name", "Country", "", "", "", COLUMN_NO_WRAP);
$list_pa->add_column("posaddress_name", "POS Name", $link, "", "", COLUMN_NO_WRAP);
$list_pa->add_column("project_costtype_text", "Legal Type", "", "", "", COLUMN_NO_WRAP);
$list_pa->add_column("postype_name", "Legal Type", "", "", "", COLUMN_NO_WRAP);
$list_pa->add_column("openingdate", "Opened", "", "", "", COLUMN_NO_WRAP);


/********************************************************************
    Create List of Ownership of closed POS Locations
*********************************************************************/
$sql_pac = "select posaddress_id, posaddress_name, posaddress_zip, " . 
		   "country_name, postype_name, project_costtype_text, " . 
		   " DATE_FORMAT(posaddress_store_openingdate, '%d.%m.%Y') as openingdate, " . 
		   " DATE_FORMAT(posaddress_store_closingdate, '%d.%m.%Y') as closingdate " .
		   "from posaddresses " .
		   "left join countries on country_id = posaddress_country " .
		   "left join postypes on postype_id = posaddress_store_postype " .
		   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
		   "left join places on place_id = posaddress_place_id";

$list_filter_pac = "posaddress_franchisee_id = " . dbquote(id()) .
                  " and (posaddress_store_closingdate is not null and posaddress_store_closingdate <> '0000-00-00')";

$list_pac = new ListView($sql_pac);
$list_pac->set_title("Closed POS Locations");
$list_pac->set_entity("posorders");
$list_pac->set_filter($list_filter_pac);
$list_pac->set_order("project_costtype_text, posaddress_store_openingdate");

$link = APPLICATION_URL . "/pos/posindex_pos.php?id={posaddress_id}";

$list_pac->add_column("country_name", "Country", "", "", "", COLUMN_NO_WRAP);
$list_pac->add_column("posaddress_name", "POS Name", $link, "", "", COLUMN_NO_WRAP);
$list_pac->add_column("project_costtype_text", "Legal Type", "", "", "", COLUMN_NO_WRAP);
$list_pac->add_column("postype_name", "Legal Type", "", "", "", COLUMN_NO_WRAP);
$list_pac->add_column("openingdate", "Opened", "", "", "", COLUMN_NO_WRAP);
$list_pac->add_column("closingdate", "Closed", "", "", "", COLUMN_NO_WRAP);

/********************************************************************
    Create List of Projects in pipeline
*********************************************************************/ 
$sql_p = "select project_id, project_number, order_shop_address_company, order_actual_order_state_code " . 
         " from projects " . 
		 " left join orders on order_id = project_order ";;

$list_filter_p = "order_actual_order_state_code < '890' " . 
                 "and (order_client_address = " . dbquote(id()) . 
				 " or order_franchisee_address_id = " . dbquote(id()) . ")";

$list_p = new ListView($sql_p);
$list_p->set_title("Projects in Pipeline");
$list_p->set_entity("posorders");
$list_p->set_filter($list_filter_p);
$list_p->set_order("project_number");

$link = APPLICATION_URL . "/user/project_task_center.php?pid={project_id}";

$list_p->add_column("project_number", "Project Number", $link, "", "", COLUMN_NO_WRAP);
$list_p->add_column("order_actual_order_state_code", "State", "", "", "", COLUMN_NO_WRAP);
$list_p->add_column("order_shop_address_company", "Project Name", "", "", "", COLUMN_NO_WRAP);



/********************************************************************
    Create List of cancelled projects
*********************************************************************/ 
$sql_c = "select project_id, project_number, order_shop_address_company, order_actual_order_state_code " . 
         " from posorderspipeline " . 
		 " left join orders on order_id = posorder_order " . 
		 " left join projects on project_order = order_id ";

$list_filter_c = "order_actual_order_state_code = 900 " . 
                 "and (order_client_address = " . dbquote(id()) . 
				 " or order_franchisee_address_id = " . dbquote(id()) . ")";
$list_c = new ListView($sql_c);
$list_c->set_title("Cancelled projects");
$list_c->set_entity("posorders");
$list_c->set_filter($list_filter_c);
$list_c->set_order("project_number");

$link = APPLICATION_URL . "/user/project_task_center.php?pid={project_id}";

$list_c->add_column("project_number", "Project Number", $link, "", "", COLUMN_NO_WRAP);
$list_c->add_column("order_actual_order_state_code", "State", "", "", "", COLUMN_NO_WRAP);
$list_c->add_column("order_shop_address_company", "Project Name", "", "", "", COLUMN_NO_WRAP);


/********************************************************************
    Create List of Files
*********************************************************************/ 

$sql_files = "select addressfile_id, addressfile_title, addressfile_path " . 
			 "from addressfiles ";
$list_filter = " addressfile_address = " . id();

//get all files
$files = array();
$sql = $sql_files . " where " . $list_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$link = "http://" . $_SERVER["HTTP_HOST"] . "/" . $row["addressfile_path"];
	$link = "<a href=\"javascript:popup('" . $link . "',800,600);\"><img src=\"/pictures/view.gif\" border='0'/></a>";
		
		$files[$row["addressfile_id"]] = $link;
}


$list = new ListView($sql_files);
$list->set_title("Files");
$list->set_entity("addressfiles");
$list->set_filter($list_filter);
$list->set_order("addressfile_title");

$list->add_text_column("files", "", COLUMN_UNDERSTAND_HTML, $files);

$link = "poscompany_file.php?address=" . id();

if(has_access("can_edit_his_posindex") or has_access("can_edit_posindex"))
{
	$list->add_column("addressfile_title", "Title", $link, "", "", COLUMN_NO_WRAP);
}
else
{
	$list->add_column("addressfile_title", "Title", "", "", "", COLUMN_NO_WRAP);
}

if(has_access("can_edit_posindex"))
{
	$list->add_button("newfile", "Add File", "");
}
elseif($can_edit == true and has_access("can_edit_his_posindex"))
{
	$list->add_button("newfile", "Add File", "");
}


// Populate form and process button clicks

$form->populate();
$form->process();

$list->populate();
$list->process();

if($form->button("back"))
{
	redirect("poscompanies.php?country=" . $form->value("country") . "&address_filter=" . $form->value("address_filter"));
}
elseif($form->button("save"))
{
	$form->value("address_phone", $form->unify_multi_edit_field($form->items["phone_number"]));
	$form->value("address_mobile_phone", $form->unify_multi_edit_field($form->items["mobile_phone_number"]));

	$form->add_validation("{address_phone} != '' or {address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");
	
	if($form->validate())
	{
		
		$form->value("address_address", $form->unify_multi_edit_field($form->items["street"], get_country_street_number_rule($form->value("address_country"))));
		
		$form->save();
		redirect("poscompanies.php?country=" . $form->value("country") . "&address_filter=" . $form->value("address_filter"));
		//$form->message("Your data has been saved.");
	}
}
elseif($form->button("address_place_id"))
{
	$sql = "select place_name from places where place_id = " . dbquote($form->value("address_place_id"));
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value("address_place", $row["place_name"]);
	}

}
elseif($list->button("newfile"))
{
	$link = "poscompany_file.php?address=" . id() . "&country=" . param("country");
	redirect($link);
}

// Render page

$page = new Page("poscompanies");
require "include/pos_page_actions.php";
$page->header();

if(has_access("can_edit_posindex") or has_access("can_edit_his_posindex"))
{
	$page->title(id() ? "Edit Company Address" : "Add Company Address");
}
else
{
	$page->title("Company");
}

$form->render();

$list_pa->render();
$list_pac->render();
$list_p->render();
$list_c->render();
$list->render();


?>

<script language="javascript">
	$("#h_address_company").click(function() {
	   $('#address_company').val($('#address_company').val().toLowerCase());
	   var txt = $('#address_company').val();

	   $('#address_company').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_address_company2").click(function() {
	   $('#address_company2').val($('#address_company2').val().toLowerCase());
	   var txt = $('#address_company2').val();

	   $('#address_company2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	
	$("#h_address_address2").click(function() {
	   $('#address_address2').val($('#address_address2').val().toLowerCase());
	   var txt = $('#address_address2').val();

	   $('#address_address2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_address_address").click(function() {
	   $('#address_street').val($('#address_street').val().toLowerCase());
	   var txt = $('#address_street').val();

	   $('#address_street').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	
</script>
<?php
$page->footer();

?>