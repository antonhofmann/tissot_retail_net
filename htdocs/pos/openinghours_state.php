<?php
/********************************************************************

    openinghours_state.php

    Lists opening hours state of data entry.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-10-17
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-10-17
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");


$sql = "SELECT posaddress_client_id, country_name, address_company,
		count(posaddress_id) as num_recs
		FROM
		posaddresses
		INNER JOIN addresses ON posaddresses.posaddress_client_id = addresses.address_id
		INNER JOIN countries on country_id = address_country
		where (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null)
		group by country_name, addresses.address_company, posaddress_client_id";



//get number of operating POS locations
$pos_openinghrs  = array();


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	
	$sql_i = "select COUNT(DISTINCT posopeninghr_posaddress_id) as num_recs " . 
			 "from posaddresses " .
		     "INNER JOIN posopeninghrs on posopeninghr_posaddress_id = posaddress_id " . 
		     " where posaddress_client_id = " . $row["posaddress_client_id"]  . 
		     " and (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null) " . 
		     " group by posaddress_client_id";
	
	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{
		$pos_openinghrs[$row["posaddress_client_id"]] = $row_i["num_recs"];
	}
}



$list1 = new ListView($sql);

$list1->set_entity("posaddresses");

$list1->add_column("country_name", "Country", "");
$list1->add_column("address_company", "Company", "");
$list1->add_column("num_recs", "#POS", "");
//$list1->add_text_column("num_recs", "#POS", COLUMN_ALIGN_RIGHT, $pos_locations);
$list1->add_text_column("num_recs2", "#POS with OH", COLUMN_ALIGN_RIGHT, $pos_openinghrs);
$list1->add_button("print", "Print List");


$list1->process();

if($list1->button("print"))
{
	$link = "openinghours_state_xls.php";
	redirect($link);
}

$page = new Page("openinghours");

$page->header();
$page->title("Status of Opening Hours");

$list1->render();
$page->footer();

?>
