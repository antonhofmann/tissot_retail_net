<?php
/********************************************************************

    correct_address_data_check_address.php

    Check Corrections Address Data and transfer to Addresses

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    get POS Location Data
*********************************************************************/
function get_addressdata($address_id, $table)
{

	$address = array();

    $sql = "select * " .
           "from $table ".
		   "left join countries on country_id = address_country " .
           "where address_id  = " . dbquote($address_id);


    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $address = $row;
    }

	return $address;
}


/********************************************************************
    compare POS Data and corrected POS Data
*********************************************************************/
function compare($array1, $array2)
{ 
	if(count($array1)!= count($array2)) return false; 
	
	foreach($array1 as $name => $value)
	{ 
		if($array1[$name] != $array2[$name]) 
		{	
			$output[$name] = 1;
		}
		else
		{
			$output[$name] = 0; 
		}
	}
	
	return $output; 
}


require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_accept_pos_data_corrections");

if(param("address_id"))
{
	register_param("address_id");
	param("id", param("address_id"));
}
else
{
	register_param("address_id");
	param("address_id", id());
}

if(param("address_id") > 0)
{
	$address = get_addressdata(param("address_id"), "addresses");
	$newaddress = get_addressdata(param("address_id"), "_addresses");

}


$difference_in_address = compare($address, $newaddress);

if(param("address_country"))
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = " . param("address_country") . " order by place_name";
}
else
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = {address_country} order by place_name";
}

// Build form
$form = new Form("_addresses", "address");

$form->add_section("Address Data");
if($difference_in_address["address_company"] == 1)
{
	$form->add_edit("address_company", "<span class='error'>Company*</span>", NOTNULL);
}
else
{
	$form->add_edit("address_company", "Company*", NOTNULL);
}

if($difference_in_address["address_company2"] == 1)
{
	$form->add_edit("address_company2", "<span class='error'></span>");
}
else
{
	$form->add_edit("address_company2", "");
}


if($difference_in_address["address_street"] == 1
	or $difference_in_address["address_streetnumber"] == 1)
{
	
	$form->add_hidden("address_address");
	$form->add_multi_edit("street", array("address_street", "address_streetnumber"), "<span class='error'>Street/Street number</span>", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6), array(), 0, '', '', array(40, 5));
}
else
{
	$form->add_hidden("address_address");
	$form->add_multi_edit("street", array("address_street", "address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6), array(), 0, '', '', array(40, 5));

}

if($difference_in_address["address_address2"] == 1)
{
	$form->add_edit("address_address2", "<span class='error'>Additional Address Info</span>");
}
else
{
	$form->add_edit("address_address2", "Additional Address Info");
}



if($difference_in_address["address_country"] == 1)
{
	$form->add_list("address_country", "<span class='error'>Country*</span>",
	"select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);
}
else
{
	$form->add_list("address_country", "Country*",
	"select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);
}


if($difference_in_address["address_zip"] == 1)
{
	$form->add_edit("address_zip", "<span class='error'>Zip*</span>", NOTNULL);
}
else
{
	$form->add_edit("address_zip", "Zip", 0);
}



if($difference_in_address["address_place"] == 1)
{
	$form->add_list("address_place_id", "<span class='error'>City*</span>", $sql_places, NOTNULL | SUBMIT);
	$form->add_edit("address_place", "", DISABLED);
}
else
{
	$form->add_list("address_place_id", "City*", $sql_places, NOTNULL | SUBMIT);
	$form->add_edit("address_place", "", DISABLED);
}


$form->add_section("Communication");

if($difference_in_address["address_phone_country"] == 1
	or $difference_in_address["address_phone_area"] == 1
	or $difference_in_address["address_phone_number"] == 1)
{
	$form->add_hidden("address_phone");
	$form->add_multi_edit("phone_number", array("address_phone_country", "address_phone_area", "address_phone_number"), "<span class='error'>Phone: Country/Area/Number</span>", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));
}
else
{
	$form->add_hidden("address_phone");
	$form->add_multi_edit("phone_number", array("address_phone_country", "address_phone_area", "address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));
}

if($difference_in_address["address_mobile_phone_country"] == 1
	or $difference_in_address["address_mobile_phone_area"] == 1
	or $difference_in_address["address_mobile_phone_number"] == 1)
{
	
	$form->add_hidden("address_mobile_phone");
	$form->add_multi_edit("mobile_phone_number", array("address_mobile_phone_country", "address_mobile_phone_area", "address_mobile_phone_number"), "<span class='error'>Mobile Phone: Country/Area/Number</span>", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));
}
else
{
	$form->add_hidden("address_mobile_phone");
	$form->add_multi_edit("mobile_phone_number", array("address_mobile_phone_country", "address_mobile_phone_area", "address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

}

if($difference_in_address["address_email"] == 1)
{
	$form->add_edit("address_email", "<span class='error'>Email*</span>", 0);
}
else
{
	$form->add_edit("address_email", "Email*", 0);
}


if($difference_in_address["address_contact_name"] == 1)
{
	$form->add_edit("address_contact_name", "<span class='error'>Contact Name*</span>", NOTNULL);
}
else
{
	$form->add_edit("address_contact_name", "Contact Name*", NOTNULL);
}

if($difference_in_address["address_website"] == 1)
{
	$form->add_edit("address_website", "<span class='error'>Website</span>");
}
else
{
	$form->add_edit("address_website", "Website");
}

$form->add_hidden("country", param("country"));

$form->add_button("remove", "Remove Record");
$form->add_button("save", "Accepts Corrections");
$form->add_button("back", "Back");

// Populate form and process button clicks

$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("correct_pos_data.php?country=" . param("country"));
}
elseif($form->button("remove"))
{
	//delete working space
	$sql = "delete from _addresses where address_id = " . dbquote(param("address_id"));
	mysql_query($sql) or dberror($sql);
	
	
	redirect("correct_address_data_check.php?country=" . param("country"));
}
elseif($form->button("address_place_id"))
{
	$sql = "select place_name from places where place_id = " . dbquote($form->value("address_place_id"));
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value("address_place", $row["place_name"]);
	}

}
elseif($form->button("address_country"))
{
	$form->value("address_place_id", "");
	$form->value("address_place", "");
}
elseif($form->button("save"))
{
	$form->value("address_phone", $form->unify_multi_edit_field($form->items["phone_number"]));
	$form->value("address_mobile_phone", $form->unify_multi_edit_field($form->items["mobile_phone_number"]));

	$form->add_validation("{address_phone} != '' or {address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");

	if($form->validate())
	{
		
		$form->value("address_address", $form->unify_multi_edit_field($form->items["street"], get_country_street_number_rule($form->value("address_country"))));
		
		$form->save();

		//update address
		$fields = array();
					
		$value = trim($form->value("address_company")) == "" ? "null" : dbquote($form->value("address_company"));
		$fields[] = "address_company = " . $value;

		$value = trim($form->value("address_company2")) == "" ? "null" : dbquote($form->value("address_company2"));
		$fields[] = "address_company2 = " . $value;

		$value = trim($form->value("address_address")) == "" ? "null" : dbquote($form->value("address_address"));
		$fields[] = "address_address = " . $value;

		if(array_key_exists('street', $form->items))
		{
			foreach($form->items['street']['fields'] as $key=>$fieldname)
			{
				$value = dbquote($form->items['street']['values'][$key]);
				$fields[] = $fieldname . " = " . $value;

			}
		}

		$value = trim($form->value("address_address2")) == "" ? "null" : dbquote($form->value("address_address2"));
		$fields[] = "address_address2 = " . $value;

		$value = trim($form->value("address_zip")) == "" ? "null" : dbquote($form->value("address_zip"));
		$fields[] = "address_zip = " . $value;

		$value = trim($form->value("address_place")) == "" ? "null" : dbquote($form->value("address_place"));
		$fields[] = "address_place = " . $value;

		$value = trim($form->value("address_place_id")) == "" ? "null" : dbquote($form->value("address_place_id"));
		$fields[] = "address_place_id = " . $value;

		$value = trim($form->value("address_country")) == "" ? "null" : dbquote($form->value("address_country"));
		$fields[] = "address_country = " . $value;
		
		$value = trim($form->value("address_phone")) == "" ? "null" : dbquote($form->value("address_phone"));
		$fields[] = "address_phone = " . $value;

		if(array_key_exists('phone_number', $form->items))
		{
			foreach($form->items['phone_number']['fields'] as $key=>$fieldname)
			{
				$value = dbquote($form->items['phone_number']['values'][$key]);
				$fields[] = $fieldname . " = " . $value;

			}
		}
		
		$value = trim($form->value("address_mobile_phone")) == "" ? "null" : dbquote($form->value("address_mobile_phone"));
		$fields[] = "address_mobile_phone = " . $value;

		if(array_key_exists('mobile_phone_number', $form->items))
		{
			foreach($form->items['mobile_phone_number']['fields'] as $key=>$fieldname)
			{
				$value = dbquote($form->items['mobile_phone_number']['values'][$key]);
				$fields[] = $fieldname . " = " . $value;

			}
		}
		
		$value = trim($form->value("address_email")) == "" ? "null" : dbquote($form->value("address_email"));
		$fields[] = "address_email = " . $value;

		$value = trim($form->value("address_contact_name")) == "" ? "null" : dbquote($form->value("address_contact_name"));
		$fields[] = "address_contact_name = " . $value;

		$value = trim($form->value("address_website")) == "" ? "null" : dbquote($form->value("address_website"));
		$fields[] = "address_website = " . $value;

		$fields[] = "address_checkdate = " . dbquote(date("Y-m-d"));

		
		$value = "current_timestamp";
		$fields[] = "date_modified = " . $value;

		if (isset($_SESSION["user_login"]))
		{
			$value = dbquote($_SESSION["user_login"]);
			$fields[] = "user_modified = " . $value;
		}

		
		$sql = "update addresses set " . join(", ", $fields) . " where address_id = " . dbquote(param("address_id"));
		
		mysql_query($sql) or dberror($sql);
		
		//delete working space
		$sql = "delete from _addresses where address_id = " . dbquote(param("address_id"));
		mysql_query($sql) or dberror($sql);
		
		
		redirect("correct_address_data_check.php?country=" . param("country"));
	}
}

// Render page
$page = new Page("posaddresses", "Companies: Check Correction");
require "include/pos_page_actions.php";
$page->header();

$page->title("Companies: Check Correction");

$form->render();

$page->footer();

?>