<?php
/********************************************************************

    delete_query.php

    Delet a query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2017-03-08
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  delete_query
    Version:        1.0.0

    Copyright (c) 2017, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}


/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("projects");

$page->header();

$qt = param("qt");

$html = '<h2 style="margin-left:20px;">Delete Query</h2>';
$html .= '<p style="margin-left:20px;">Are you sure to delete this query?</p>';

$html .= '<p>&nbsp;</p>';

if($_GET["context"] == 'pos') {
	$html .= '<p style="margin-left:20px;"><a href ="/pos/query_generator.php?delete_id=' . $_GET["id"]  .  '">Yes</a>&nbsp;&nbsp;<a href="">No</a></p>';
}
echo $html;


?>
<script type="text/javascript">
	$(function() {
		$.nyroModalSettings({height: 300, width: 400});
	});
	</script>
<?php

$page->footer();
