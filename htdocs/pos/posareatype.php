<?php
/********************************************************************

    posareatype.php

    Creation and mutation of area type records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");

$form = new Form("posareatypes", "area type");

$form->add_section("POS Area Type Name");
$form->add_edit("posareatype_name", "Name*", NOTNULL | UNIQUE);

$form->add_section("Send an Email Notification for New Projects");
$form->add_edit("posareatype_email1", "Email Recipient 1");
$form->add_edit("posareatype_email2", "Email Recipient 2");
$form->add_edit("posareatype_email3", "Email Recipient 3");
$form->add_edit("posareatype_email4", "Email Recipient 4");
$form->add_edit("posareatype_email5", "Email Recipient 5");
$form->add_edit("posareatype_email6", "Email Recipient 6");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

if($form->button(FORM_BUTTON_SAVE)) {
	redirect('posareatypes.php');
}

$page = new Page("posareatypes");
$page->header();
$page->title(id() ? "Edit Area Type" : "Add Area Type");
$form->render();
$page->footer();

?>