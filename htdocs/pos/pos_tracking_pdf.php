<?php
/********************************************************************

    pos_tracking_pdf.php

    Print Hitrory of POS trackings

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-07-25
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-07-25
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
check_access("can_edit_posindex");


/********************************************************************
    prepare all data needed
*********************************************************************/

if (param("id"))
{
	$pos = get_poslocation(param("id"));

	global $page_title;
	$page_title = "POS Tracking";

	
	//POS Basic Data
	$posname = $pos["posaddress_name"];

	if($pos["posaddress_name2"])
    {
		$posname .= ", " . $pos["posaddress_name2"];
	}

	$posaddress = $pos["posaddress_address"];
	
	if($pos["posaddress_address2"])
    {
		$posaddress .= ", " . $pos["posaddress_address2"];
	}
	if($pos["posaddress_zip"])
    {
		$posaddress .= ", " . $pos["posaddress_zip"] . " " . $pos["place_name"];
	}
	else
	{
		$posaddress .= ", " . $pos["place_name"];
	}
	if($pos["country_name"])
    {
		$posaddress .= ", " . $pos["country_name"];
	}


	$country = $pos["country_name"];
	

	$poslegaltype = $pos["posowner_type_name"];
	$postype = $pos["postype_name"];


	
	//history of closing dates

	$tracking_info = array();
	$sql = "select postracking_oldvalue, postracking_newvalue, postracking_comment, postracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " . 
		   "from postracking " . 
		   "left join users on user_id = postracking_user_id " . 
		   "where postracking_pos_id = " . dbquote(param("id")) . 
		   " and postracking_field = 'posaddress_store_planned_closingdate' " . 
		   " order by postracking_time";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info[] = array("postracking_oldvalue"=>$row["postracking_oldvalue"],
			"postracking_newvalue"=>$row["postracking_newvalue"],
			"postracking_comment"=>$row["postracking_comment"],
			"postracking_time"=>$row["postracking_time"],
			"user_name"=>$row["user_name"]
			);
	}


	$tracking_info1 = array();
	$sql = "select postracking_oldvalue, postracking_newvalue, postracking_comment, postracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " . 
		   "from postracking " . 
		   "left join users on user_id = postracking_user_id " . 
		   "where postracking_pos_id = " . dbquote(param("id")) . 
		   " and postracking_field = 'posaddress_store_closingdate' " . 
		   " order by postracking_time";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info1[] = array("postracking_oldvalue"=>$row["postracking_oldvalue"],
			"postracking_newvalue"=>$row["postracking_newvalue"],
			"postracking_comment"=>$row["postracking_comment"],
			"postracking_time"=>$row["postracking_time"],
			"user_name"=>$row["user_name"]
			);
	}


	

	/********************************************************************
		prepare pdf
	*********************************************************************/

	require_once('../include/tcpdf/config/lang/eng.php');
	require_once('../include/tcpdf/tcpdf.php');


	class MYPDF extends TCPDF
	{
		//Page header
		function Header()
		{
			global $page_title;
			//Logo
			$this->Image('../pictures/brand_logo.jpg',10,8,33);
			//arialn bold 15
			$this->SetFont('arialn','B',12);
			//Move to the right
			$this->Cell(80);
			//Title
			$this->Cell(0,33,$page_title,0,0,'R');
			//Line break
			$this->Ln(20);

		}

		//Page footer
		function Footer()
		{
			//Position at 1.5 cm from bottom
			$this->SetY(-15);
			//arialn italic 8
			$this->SetFont('arialn','I',8);
			//Page number
			$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
		}

	}

	//Instanciation of inherited class
	$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
	$pdf->SetMargins(10, 23, 10);

	$pdf->Open();

	$pdf->SetFillColor(220, 220, 220); 
	$pdf->AddFont('arialn');
	$pdf->AddFont('arialn', 'B');

	$pdf->AddPage();
	$new_page = 0;

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,$posname,1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->Cell(190,7,$posaddress,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	
		
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Legal Type",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$poslegaltype,1, 0, 'L', 0);
	
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"POS Type",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$postype,1, 0, 'L', 0);
	
		
	if(count($tracking_info) > 0)
	{
		$pdf->Ln();
		$pdf->Ln();


		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Planned Closing Dates",1, 0, 'L', 0);

		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(20, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(20, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(96, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		
		
		
		foreach($tracking_info as $key=>$values)
		{
			
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['postracking_time'],1, 0, 'L', 0);
			$pdf->Cell(20, 5,str_replace(' 00:00:00', '', $values['postracking_oldvalue']),1, 0, 'L', 0);
			$pdf->Cell(20, 5,str_replace(' 00:00:00', '', $values['postracking_newvalue']),1, 0, 'L', 0);
			//$pdf->Cell(106, 5,$values['postracking_comment'],1, 0, 'L', 0);
			$pdf->MultiCell(96,5, $values['postracking_comment'], 1, "T");
			

		}
	}


	if(count($tracking_info1) > 0)
	{
		$pdf->Ln();
		$pdf->Ln();


		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Closing Dates",1, 0, 'L', 0);

		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(20, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(20, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(96, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		
		
		
		foreach($tracking_info1 as $key=>$values)
		{
			
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['postracking_time'],1, 0, 'L', 0);
			$pdf->Cell(20, 5,str_replace(' 00:00:00', '', $values['postracking_oldvalue']),1, 0, 'L', 0);
			$pdf->Cell(20, 5,str_replace(' 00:00:00', '', $values['postracking_newvalue']),1, 0, 'L', 0);
			//$pdf->Cell(106, 5,$values['postracking_comment'],1, 0, 'L', 0);
			$pdf->MultiCell(96,5, $values['postracking_comment'], 1, "T");
			

		}
	}



	//output all POS Mails
	$first_record_processed = false;
	


	$sql_m = 'select * from mail_trackings where mail_tracking_mail_template_id in (72,73,90,91) and mail_tracking_content like "%' . $posname . ' ' . $country . '%" order by date_created';

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	while($row_m = mysql_fetch_assoc($res_m))
	{
		
		
		
		if($first_record_processed == false)
		{
			$first_record_processed = true;
			$pdf->Ln();
			$pdf->Ln();
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(190, 5,"Lease Alerts",1, 0, 'L', 0);

			$pdf->Ln();
			
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(47, 5,"Recipient",1, 0, 'L', 0);
			$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
			$pdf->Cell(116, 5,'Alert',1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Ln();
		}
		
		
		$pdf->Cell(47, 8,$row_m["mail_tracking_recipient_email"],1, 0, 'L', 0);
		$pdf->Cell(27, 8,$row_m["date_created"],1, 0, 'L', 0);
		$mailtext = $row_m["mail_tracking_subject"];

		$pdf->MultiCell(116,8, $mailtext, 1, "T");
	}

	//if($first_record_processed == false)
	//{
		$lease_trackings = array();
		
		$sql_m = "select * from posleases where poslease_mailalert is not null and poslease_mailalert <> '' and poslease_posaddress = " . dbquote(param("id"));

		$res_m = mysql_query($sql_m) or dberror($sql_m);
		while($row_m = mysql_fetch_assoc($res_m))
		{
			$tmp = explode('Mail sent ', $row_m["poslease_mailalert"]);

			if(count($tmp) == 2)
			{
				
				$tmp1 = explode('To: ', $tmp[1]);
				

				if(count($tmp1) == 2)
				{
					$subject = "Tissot Retail Net: Lease contract alert - POS Location " . $posname . " " . $country;
					$date = substr($tmp1[0], 0, 8);
					$lease_trackings[] = array('date'=>$date, 'recipient'=>$tmp1[1], 'subject'=>$subject);


				}
			}
		}


		$sql_m = "select * from posleases where poslease_mailalert2 is not null and poslease_mailalert2 <> '' and poslease_posaddress = " . dbquote(param("id"));

		$res_m = mysql_query($sql_m) or dberror($sql_m);
		while($row_m = mysql_fetch_assoc($res_m))
		{
			$tmp = explode('Mail sent ', $row_m["poslease_mailalert2"]);

			if(count($tmp) == 2)
			{
				
				$tmp1 = explode('To: ', $tmp[1]);
				

				if(count($tmp1) == 2)
				{
					$subject = "Tissot Retail Net: Lease contract alert - POS Location " . $posname . " " . $country;
					$date = substr($tmp1[0], 0, 8);
					$lease_trackings[] = array('date'=>$date, 'recipient'=>$tmp1[1], 'subject'=>$subject);
				}
			}
		}


		if(count($lease_trackings) > 0)
		{
			foreach($lease_trackings as $key=>$values)
			{
				if($first_record_processed == false)
				{
					$first_record_processed = true;
					$pdf->Ln();
					$pdf->Ln();
					$pdf->SetFont('arialn','B',8);
					$pdf->Cell(190, 5,"Lease Alerts",1, 0, 'L', 0);

					$pdf->Ln();
					
					$pdf->SetFont('arialn','B',8);
					$pdf->Cell(47, 5,"Recipient",1, 0, 'L', 0);
					$pdf->Cell(27, 5,"Date",1, 0, 'L', 0);
					$pdf->Cell(116, 5,'Alert',1, 0, 'L', 0);
					$pdf->SetFont('arialn','',8);
					$pdf->Ln();
				}
				
				
				$pdf->Cell(47, 0,$values["recipient"],1, 0, 'L', 0);
				$pdf->Cell(27, 0,$values["date"],1, 0, 'L', 0);
				$mailtext = 

				$pdf->MultiCell(116,0, $values["subject"], 1, "T");
			}
		}
	//}


	
	

	// write pdf
	$pdf->Output("pos_tracking_" . $posname . "_" . $country . ".pdf");

}

?>