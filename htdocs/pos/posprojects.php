<?php
/********************************************************************

    posprojects.php

    Entry page for the projects section.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_use_posindex");

/********************************************************************
    prepare all data needed
*********************************************************************/
register_param("pos_id");
set_referer("posporject.php");

//update posorders
$infolinks = update_posorders_from_projects(param("pos_id"));

$pos = array();
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}

/********************************************************************
    Header
*********************************************************************/
$form = new Form("posorders", "posorders");
require_once("include/poslocation_head.php");

$form->add_hidden("country", param("country"));
$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("psc", param("psc"));
$form->add_hidden("let", param("let"));
$form->add_hidden("province",  param("province"));
$form->add_hidden("ostate", param("ostate"));

$form->populate();
$form->process();

$sql = "select posorder_id, posorder_reporting_number, " . 
       "if(posorder_product_line = 0, 'n.a.', product_line_name) as product_line_name, " .
	   "if(posorder_popup_name <> '', concat(projectkind_name, ', ', posorder_popup_name), projectkind_name) as projectkind_name, " . 
	   "project_costtype_text, order_actual_order_state_code, postype_name, " . 
	   "if(posorder_opening_date = '0000-00-00', 'n.a.', DATE_FORMAT(posorder_opening_date,'%d.%m.%Y')) as opened, " .
       "if(posorder_closing_date = '0000-00-00', '', DATE_FORMAT(posorder_closing_date,'%d.%m.%Y')) as closed, " .
	   "if(posorder_popup_closingdate = '0000-00-00', '', DATE_FORMAT(posorder_popup_closingdate,'%d.%m.%Y')) as popup_closed, " .
	   "if(posorder_ordernumber is Null, '00.000.000', posorder_ordernumber) as order_number, " . 
	   "order_actual_order_state_code " . 
	   "from posorders " . 
	   "left join orders on order_id = posorder_order " . 
	   "left join product_lines on product_line_id = posorder_product_line ".
		 "left join postypes on postype_id = posorder_postype ".
		 "left join projectkinds on projectkind_id = posorder_project_kind ".
		 "left join project_costtypes on project_costtype_id = posorder_legal_type ";

$list_filter = "posorder_type = 1 and posorder_posaddress = " . param("pos_id");


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);
$list->set_entity("posorders");
$list->set_filter($list_filter);
$list->set_order("posorder_year, posorder_opening_date, posorder_order");
$link = "posproject.php?pos_id=" . param("pos_id") . "&country=" . param("country"). "&ltf=" . param("ltf") . "&psc=" . param("psc"). "&ostate=" . param("ostate"). '&province=' . param("province") . '&let=' . param("let");

$list->add_hidden("pos_id", param("pos_id"));
//$list->add_column("project_number", "Project No.", "project_task_center.php?pid={project_id}", "", "", COLUMN_NO_WRAP);
$list->add_column("order_number", "Project No.", $link, "", "", COLUMN_NO_WRAP);

if(has_access("can_view_projects"))
{
	$list->add_text_column("info", "", COLUMN_UNDERSTAND_HTML, $infolinks);
}
$list->add_column("product_line_name", "Product Line", "", LIST_FILTER_LIST, "", COLUMN_NO_WRAP);
$list->add_column("postype_name", "POS Type", "", LIST_FILTER_LIST, "", COLUMN_NO_WRAP);
$list->add_column("projectkind_name", "Project Type", "", LIST_FILTER_LIST, "");
$list->add_column("project_costtype_text", "Legal Type", "", LIST_FILTER_LIST);
$list->add_column("order_actual_order_state_code", "Status");
$list->add_column("opened", "Opening Date", "", LIST_FILTER_LIST);
$list->add_column("closed", "Closing Date", "", LIST_FILTER_LIST);
$list->add_column("popup_closed", "PopUp Closing Date", "", LIST_FILTER_LIST);
//$list->add_column("posorder_reporting_number", "Reporting Nr.", "", LIST_FILTER_LIST);


//$list->add_column("posorder_system_currency", "", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
//$list->add_column("posorder_budget_approved_sc", "Approved", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
//$list->add_column("posorder_real_cost_sc", "Real", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
//$list->add_column("posorder_client_currency", "", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
//$list->add_column("posorder_budget_approved_cc", "Approved", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
//$list->add_column("posorder_real_cost_cc", "Real", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

if(has_access("can_administrate_posindex"))
{
	$list->add_button("new", "New", $link);
}
$list->add_button("back", "Back to POS List");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

if($list->button("back"))
{
	redirect("posindex.php?country=" . param("country") . "&ltf=" . param("ltf") . "&psc=" . param("psc"). '&ostate=' . param("ostate"). '&province=' . param("province") . '&let=' . param("let"));
}
elseif($list->button("new"))
{
	redirect($link);
}


/********************************************************************
    create page
*********************************************************************/
$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();

$page->title("Projects: " . $poslocation["posaddress_name"]);

require_once("include/tabs.php");

$form->render();

$list->render();
$page->footer();
?>