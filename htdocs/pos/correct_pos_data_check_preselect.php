<?php
/********************************************************************

    correct_pos_data_check_preselect.php

    Preselection of POS List for data corrections

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_accept_pos_data_corrections");
set_referer("posindex.php");


//compose list
if(has_access("can_view_posindex") or has_access("can_edit_posindex"))
{
	$sql = "select DISTINCT country_id, country_name " .
		   "from _posaddresses " . 
		   "left join countries on posaddress_country = country_id " . 
		   "where country_name is not null and posaddress_checked = 1 " . 
		   "order by country_name";
}

$form = new Form("posaddresses", "posaddress");

$form->add_section("Country Selection");

$form->add_list("country", "Country Selection",$sql, SUBMIT);


$form->populate();
$form->process();



if(param("country"))
{
	redirect("correct_pos_data_check.php?country=" . param("country"));
}

$page = new Page("posaddresses", "POS Index: Check Data Corrections");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: Check Data Corrections");
$form->render();


$page->footer();

?>
