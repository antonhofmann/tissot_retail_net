<?php
/********************************************************************

    posequipmenttypes.php

    Lists store equipment types for editing.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("posequipmenttype.php");

$list = new ListView("select posequipmenttype_id, posequipmenttype_name " .
                     "from posequipmenttypes");

$list->set_entity("posequipmenttypes");
$list->set_order("posequipmenttype_name");

$list->add_column("posequipmenttype_name", "Name", "posequipmenttype.php");

$list->add_button(LIST_BUTTON_NEW, "New", "posequipmenttype.php");

$list->process();

$page = new Page("posequipmenttypes");

$page->header();
$page->title("Equipment");
$list->render();
$page->footer();

?>
