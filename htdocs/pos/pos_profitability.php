<?php
/********************************************************************

    pos_profitability.php

    Lists of addresses (POS)

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-10-20
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-10-20
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
require_once "../shared/access_filters.php";

check_access("has_access_to_pos_profitability");
set_referer("pos_profitability_pos.php");


if(!has_access("can_view_his_posindex") 
       and !has_access("can_edit_his_posindex") 
	   and !has_access("can_view_posindex") 
	   and !has_access("can_edit_posindex")
	   and !has_access("has_access_to_pos_profitability"))
{
	redirect("noaccess.php");
}

$user = get_user(user_id());


$preselect_filter = "";
if(param("country"))
{
	$preselect_filter = "posaddress_country = " . param("country");
	register_param("country", param("country"));
}
else
{
	redirect("posindex_preselect.php");
}

if(param("province") and $preselect_filter)
{
	if(param('province') == "all") {
	}
	else
	{
		$preselect_filter .= " and place_province = " . param("province");
	}
	register_param("province", param("province"));
}
elseif(param("province"))
{
	if(param('province') == "all") {
	}
	else
	{
		$preselect_filter = "place_province = " . param("province");
	}
	register_param("province", param("province"));
}


$state_filter = "";
if(param("ostate") and $preselect_filter)
{
	if(param("ostate") == 1) // only operating POS locations
	{
		$preselect_filter .= " and (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null) ";
		$state_filter = "  (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null) ";
	}
	elseif(param("ostate") == 2) // only closed POS locations
	{
		$preselect_filter .= " and posaddress_store_closingdate <> '0000-00-00' and posaddress_store_closingdate is not null";
		$state_filter = "  (posaddress_store_closingdate <> '0000-00-00' and posaddress_store_closingdate is not null)";
	}

	

	register_param("ostate", param("ostate"));
}
elseif(param("ostate"))
{
	if(param("ostate") == 1) // only operating POS locations
	{
		$preselect_filter = " (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null) ";
		$state_filter = " (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null)";
	}
	elseif(param("ostate") == 2) // only closed POS locations
	{
		$preselect_filter = " posaddress_store_closingdate <> '0000-00-00' and posaddress_store_closingdate is not null";
		$state_filter = " (posaddress_store_closingdate <> '0000-00-00' and posaddress_store_closingdate is not null)";
	}


	register_param("ostate", param("ostate"));
}


if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{

}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	if(!param("country"))
	{
		redirect("posindex_preselect.php");
	}

	
	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}
	
	$clients_access_filter = get_users_regional_access_to_poslocations(user_id());

	
	if($clients_access_filter)
	{
		if($preselect_filter)
		{
			$preselect_filter .= " and (posaddress_client_id = " . $user["address"] . " or " . $clients_access_filter . ") ";
		}
		else
		{
			$preselect_filter = "  (posaddress_client_id = " . $user["address"] . " or " . $clients_access_filter . ") ";;
		}

		if(count($tmp) > 0)
		{
			if($preselect_filter)
			{
				$preselect_filter .= " or posaddress_country in (" . implode(',', $tmp) . ") ";
			}
			else
			{
				$preselect_filter = "  posaddress_country in (" . implode(',', $tmp) . ") ";
			}
		}
	}
	elseif(count($tmp) > 0 and !param("country"))
	{
		if($preselect_filter)
		{
			$preselect_filter .= " or posaddress_country in (" . implode(',', $tmp) . ") ";
		}
		else
		{
			$preselect_filter = "  posaddress_country in (" . implode(',', $tmp) . ") ";
		}
	}
	elseif(param("country"))
	{
		if($preselect_filter)
		{
			$preselect_filter .= " or posaddress_country = " . param("country");
		}
		else
		{
			$preselect_filter = " posaddress_country = " . param("country");
		}
	}
	else
	{
		if($preselect_filter)
		{
			$preselect_filter .= " and posaddress_client_id = " . $user["address"];
		}
		else
		{
			$preselect_filter = "  posaddress_client_id = " . $user["address"];
		}
	}
	

	if($preselect_filter)
	{
		if($state_filter)
		{
			$preselect_filter .= " and (" . $state_filter . ") ";
		}
	}
	elseif($state_filter)
	{
		$preselect_filter = " (" . $state_filter . ") ";
	}

	
}


//compose list
$sql = "select posaddress_id, posaddress_client_id, if(posaddress_name <> '', posaddress_name, 'n.a.') as posname, " .
       "posaddress_address, posaddress_address2, posaddress_zip, " .
       "    posaddress_place, country_name, project_costtype_text, postype_name, " .
	   "posaddress_google_precision, posaddress_store_closingdate, province_canton, posaddress_country " . 
       "from posaddresses " .
	   "left join places on place_id = posaddress_place_id " .
	   "left join provinces on province_id = place_province " .
	   "left join countries on posaddress_country = country_id " . 
	   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
	   "left join postypes on postype_id = posaddress_store_postype ";

if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{
}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	
	//$preselect_filter = "(postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) and " . $preselect_filter;
}


if(param('ltf')) 
{
	if($preselect_filter) {
		
		if(param('ltf') == "all") {
		}
		else
		{
			$preselect_filter .= " and posaddress_store_postype = " . param('ltf');
		}
	}
	else
	{
		if(param('ltf') == "all") {
		}
		else
		{
			$preselect_filter = "posaddress_store_postype = " . param('ltf');
		}
	}
}
else
{
	if($preselect_filter) {
		$preselect_filter .= " and posaddress_store_postype <> 4";
	}
	else
	{
		$preselect_filter = "posaddress_store_postype <> 4";
	}
}


if(param('ltype')) 
{
	if($preselect_filter) {
		$preselect_filter .= " and posaddress_ownertype = " . param('ltype');
	}
	else
	{
		$preselect_filter = "posaddress_ownertype = " . param('ltype');
	}
}


//get image columns
$closed = array();

$sql_u = "select posaddress_id, posaddress_country, posaddress_google_precision, posaddress_store_closingdate,  " .
		 "posfile_id, posfile_filegroup, posaddress_export_to_web, place_name, posaddress_address, posaddress_address2, posaddress_zip, posaddress_google_lat, posaddress_google_long, " . 
		 "posclosingassessment_id, posclosingassessment_filesigned " .
         "from posaddresses " . 
		 "left join places on place_id = posaddress_place_id " .
		 "left join provinces on province_id = place_province " .
		 "left join posfiles on posfile_posaddress = posaddress_id " . 
		 "left join posclosingassessments on posclosingassessment_posaddress_id = posaddress_id ";

//$sql_u = $sql_u . " where " . $preselect_filter . " and (posfile_filegroup = 1 or posfile_filegroup is null) ";
if($preselect_filter)
{
	$sql_u = $sql_u . " where " . $preselect_filter;
}

$res = mysql_query($sql_u) or dberror($sql_u);
while($row = mysql_fetch_assoc($res))
{
	//$result = update_posdata_from_posorders($row["posaddress_id"]);

	
	if($row["posaddress_store_closingdate"] != NULL and $row["posaddress_store_closingdate"] != "0000-00-00")
	{
		
		$closed[$row["posaddress_id"]] = "<img src=\"/pictures/closed.gif\" />";
	}
}



//list filter arrays
$postype_filter = array();
$postype_filter["all"] = "All";
$sql_postypes = "select * from postypes order by postype_name";
$res = mysql_query($sql_postypes) or dberror($sql_postypes);
while($row = mysql_fetch_assoc($res))
{
	$postype_filter[$row["postype_id"]] = $row["postype_name"];
}

$province_filter = array();
$province_filter["all"] = "All";
$sql_provinces = "select province_id, province_canton " . 
	   "from provinces where province_country = " . param('country') . " order by province_canton";
$res = mysql_query($sql_provinces) or dberror($sql_provinces);
while($row = mysql_fetch_assoc($res))
{
	$province_filter[$row["province_id"]] = $row["province_canton"];
}


$states = array();
$states[1] = "Operating POS locations only";
$states[2] = "Closed POS locations only";


$legal_types = array();
$sql_legaltypes = "select * from posowner_types order by posowner_type_name";
$res = mysql_query($sql_legaltypes) or dberror($sql_legaltypes);
while($row = mysql_fetch_assoc($res))
{
	$legal_types[$row["posowner_type_id"]] = $row["posowner_type_name"];
}

/********************************************************************
    Create List
*********************************************************************/
$list = new ListView($sql);

$list->set_entity("posaddresses");
$list->set_filter($preselect_filter);
$list->set_order("country_name, posaddress_place, posaddress_name");

$list->add_listfilters("ltf", "POS Type", 'select', $postype_filter, param("ltf"));
$list->add_listfilters("province", "Province", 'select', $province_filter, param("province"));
$list->add_listfilters("ostate", "State", 'select', $states, param("ostate"));
$list->add_listfilters("ltype", "Legal Type", 'select', $legal_types, param("ltype"));

$list->add_hidden("country", param("country"));
$list->add_hidden("province", param("province"));
$list->add_hidden("ostate", param("ostate"));

$list->add_column("country_name", "Country", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("province_canton", "Province", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);

$list->add_column("posname", "POS Name", "pos_profitability_pos.php?pos_id={posaddress_id}&country=" . param("country") . '&ltf=' . param("ltf") . '&ostate=' . param("ostate"). '&province=' . param("province") . '&ltype=' .param('ltype'), LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_address", "Street", "", LIST_FILTER_FREE);
$list->add_column("postype_name", "POS Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("project_costtype_text", "Legal Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_text_column("closed", "Closed", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML , $closed);


$list->populate();
$list->process();


$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: POS yearly sellout");
$list->render();


echo '<script type="text/javascript">';
echo 'jQuery(document).ready(function($) {';

foreach($pictures as $posaddress_id=>$value)
{
  echo '$("#p_' . $posaddress_id . '").click(function(e) {';
  echo 'e.preventDefault();';
  echo '$.nyroModalManual({';
  echo 'width: "860",';
  echo 'height: "700",';
  echo 'url: "/pos/pos_pictures.php?posaddress_id=' . $posaddress_id . '"';
  echo '});';
  echo 'return false;';
  echo '});';
}


echo '});';
echo '</script>';


$page->footer();

?>
