<?php
/********************************************************************

    dataanalysis_ownerorphans_xls.php

    Generate Excel-File of Owner Orphans

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2015-01-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2015-01-15
    Version:        1.1.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
require_once "../shared/access_filters.php";

$user = get_user(user_id());

/********************************************************************
    prepare Data Needed
*********************************************************************/

//get all owner companies
$posowner_ids = array();
$sql = "select DISTINCT posaddress_franchisee_id " . 
       " from posaddresses " .
	   " left join addresses on address_id = posaddress_franchisee_id " .
	   " where address_active = 1 and posaddress_store_closingdate is not null and posaddress_store_closingdate <> '0000-00-00'";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$posowner_ids[] = $row["posaddress_franchisee_id"];
}


$posowner_ids2 = array();
$list_filter = "address_id = 0";
if(count($posowner_ids) > 0)
{
	$filter = " posaddress_franchisee_id in (" . implode(',', $posowner_ids) . ") ";

	$posowner_ids2 = array();
	$sql = "select DISTINCT posaddress_franchisee_id " . 
		   " from posaddresses " . 
		   " where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " . 
		   " and " . $filter;


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$posowner_ids2[] = $row["posaddress_franchisee_id"];
	}


	if(count($posowner_ids2) > 0)
	{
		$fullDiff = array_merge(array_diff($posowner_ids, $posowner_ids2), array_diff($posowner_ids2, $posowner_ids));
		$list_filter = " address_id in (" . implode(',', $fullDiff) . ") ";
	}
}



$sql = "select address_id, address_company, address_zip, " .
	 "    address_place, country_name, address_type_name, province_canton, " .
	  "IF(address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1, 'yes', '') as franchisee, " .
	   "IF(address_is_independent_retailer = 1, 'yes', '') as retailer " .
	 "from addresses " . 
	 " left join address_types on address_type_id = address_type " .
	 "left join countries on address_country = country_id " . 
	 "left join places on place_id = address_place_id " .
	 "left join provinces on province_id = place_province";



$open_poslocations = array();
$closed_poslocations = array();
$projects_in_pipeline = array();

$res = mysql_query($sql . " where " . $list_filter) or dberror($sql . " where " . $list_filter);
while ($row = mysql_fetch_assoc($res))
{
	$sql_p = "select count(posaddress_id) as num_recs from posaddresses " .
		     " where posaddress_store_openingdate is not null and posaddress_store_openingdate <> '0000-00-00' " . 
	         " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " . 
		     " and posaddress_franchisee_id = " . $row["address_id"];

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	$row_p = mysql_fetch_assoc($res_p);
	$open_poslocations[$row["address_id"]] = $row_p["num_recs"];

	$sql_p = "select count(posaddress_id) as num_recs from posaddresses " .
		     " where posaddress_store_closingdate is not null and posaddress_store_closingdate <> '0000-00-00' " . 
		     " and posaddress_franchisee_id = " . $row["address_id"];

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	$row_p = mysql_fetch_assoc($res_p);
	$closed_poslocations[$row["address_id"]] = $row_p["num_recs"];

	$sql_p = "select count(posaddress_id) as num_recs from posaddressespipeline " .
		     " where posaddress_franchisee_id = " .  $row["address_id"];

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	$row_p = mysql_fetch_assoc($res_p);
	$projects_in_pipeline[$row["address_id"]] = $row_p["num_recs"];

}


$sql_d = $sql . " where " . $list_filter . " order by country_name, address_company";


/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "owner_orphans" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename);

$xls->setVersion(8);

$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_normal_bold =& $xls->addFormat();
$f_normal_bold->setSize(8);
$f_normal_bold->setAlign('left');
$f_normal_bold->setBorder(1);
$f_normal_bold->setBold();


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('yellow');



//captions
$captions = array();
//$captions[] = "Nr";
//$captions[] = "Geographical Region";
$captions[] = "Country";
$captions[] = "Type";
$captions[] = "Company";
$captions[] = "City";
$captions[] = "Franchisee";
$captions[] = "Retailer";
$captions[] = "Operating";
$captions[] = "Closed";
$captions[] = "Pipeline";

/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);
$sheet->writeRow(1, 0, $captions, $f_normal_bold);


$row_index = 2;
$cell_index = 0;
$counter = 0;
$col_widths = array();
for($i=0;$i<count($captions);$i++)
{
	$col_widths[$i] = strlen($captions[$i]);
}


$res = mysql_query($sql_d) or dberror($sql_d);
while ($row = mysql_fetch_assoc($res))
{
    	
	
	$sheet->write($row_index, $cell_index, $row["country_name"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["country_name"]))
	{
		$col_widths[$cell_index] = strlen($row["country_name"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_type_name"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_type_name"]))
	{
		$col_widths[$cell_index] = strlen($row["address_type_name"]);
	}
	$cell_index++;


	$sheet->write($row_index, $cell_index, $row["address_company"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_company"]))
	{
		$col_widths[$cell_index] = strlen($row["address_company"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_place"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_place"]))
	{
		$col_widths[$cell_index] = strlen($row["address_place"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["franchisee"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["franchisee"]))
	{
		$col_widths[$cell_index] = strlen($row["franchisee"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["retailer"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["retailer"]))
	{
		$col_widths[$cell_index] = strlen($row["retailer"]);
	}
	$cell_index++;


	$sheet->write($row_index, $cell_index, $open_poslocations[$row["address_id"]], $f_number);
	if($col_widths[$cell_index] < strlen($open_poslocations[$row["address_id"]]))
	{
		$col_widths[$cell_index] = strlen($open_poslocations[$row["address_id"]]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $closed_poslocations[$row["address_id"]], $f_number);
	if($col_widths[$cell_index] < strlen($closed_poslocations[$row["address_id"]]))
	{
		$col_widths[$cell_index] = strlen($closed_poslocations[$row["address_id"]]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $projects_in_pipeline[$row["address_id"]], $f_number);
	if($col_widths[$cell_index] < strlen($projects_in_pipeline[$row["address_id"]]))
	{
		$col_widths[$cell_index] = strlen($projects_in_pipeline[$row["address_id"]]);
	}
	
	
	$cell_index = 0;
	$row_index++;
}

for($i=0;$i<count($captions);$i++)
{
	$sheet->setColumn($i, $i, $col_widths[$i]);
}


$xls->close(); 

?>