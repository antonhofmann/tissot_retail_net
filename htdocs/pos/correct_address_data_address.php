<?php
/********************************************************************

    correct_address_data_address.php

    Correct Address Data

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_correct_pos_data");

if(param("address_id"))
{
	register_param("address_id");
	param("id", param("address_id"));
}
else
{
	register_param("address_id");
	param("address_id", id());
}

$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = {address_country} order by place_name";


$address_checked = 0;
if(param("address_checked") != 1)
{
	$sql = "select address_checked from _addresses where address_id = " . id();
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$address_checked = $row["address_checked"];
	}
}


// Build form
$form = new Form("_addresses", "address");
$form->add_hidden("country", param("country"));

/*
$form->add_section("Address Numbers");
$form->add_edit("address_number", "Tissot SAP Customer Code");
$form->add_edit("address_legnr", "SG Legal Number");
$form->add_edit("address_sapnr", "SAP Customer Code");
*/

$form->add_section("Address Data");
$form->add_edit("address_company", "Company*", NOTNULL);
$form->add_edit("address_company2", "");


$form->add_hidden("address_address");
$form->add_multi_edit("street", array("address_street", "address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6), array(), 0, '', '', array(40, 5));

$form->add_edit("address_address2", "Additional Address Info");

$form->add_list("address_country", "Country*",
    "select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);


$form->add_list("address_place_id", "City*", $sql_places, NOTNULL | SUBMIT);
$form->add_edit("address_place", "", DISABLED);

$form->add_edit("address_zip", "Zip");

$form->add_section("Communication");

$form->add_hidden("address_phone");
$form->add_multi_edit("phone_number", array("address_phone_country", "address_phone_area", "address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


$form->add_hidden("address_mobile_phone");
$form->add_multi_edit("mobile_phone_number", array("address_mobile_phone_country", "address_mobile_phone_area", "address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


$form->add_edit("address_email", "Email*", NOTNULL);
$form->add_edit("address_contact_name", "Contact Name*", NOTNULL);
$form->add_edit("address_website", "Website");

$form->add_section("Address Check");
$form->add_checkbox("address_checked", "Adress is checked and correct", "", 0, "Address check*");


$form->add_button("save", "Save my Corrections");
$form->add_button("back", "Back");

// Populate form and process button clicks

$form->populate();
$form->process();


if($address_checked == 2)
{
	$form->value("address_checked", 0);
}


if($form->button("back"))
{
	redirect("correct_address_data.php?country=" . param("country"));
}
elseif($form->button("save"))
{
	
	//$form->add_validation("is_email_address({address_email})", "The email address is invalid.");
	//$form->add_validation("is_web_address({address_website})", "The website url is invalid Use the form 'http://www...'.");
	

	$form->value("address_phone", $form->unify_multi_edit_field($form->items["phone_number"]));
	$form->value("address_mobile_phone", $form->unify_multi_edit_field($form->items["mobile_phone_number"]));

	$form->add_validation("{address_phone} != '' or {address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");
	
	if(!$form->value("address_checked"))
	{
		$form->value("address_address", $form->unify_multi_edit_field($form->items["street"], get_country_street_number_rule($form->value("address_country"))));


		
		$form->save();
		$sql = "update _addresses set address_checked = 2 where address_id = " . id();
		mysql_query($sql) or dberror($sql);
	}
	elseif($form->validate())
	{
		
		$form->value("address_address", $form->unify_multi_edit_field($form->items["street"], get_country_street_number_rule($form->value("address_country"))));

		$form->save();
		redirect("correct_address_data.php?country=" . param("country"));
	}
}
elseif($form->button("address_place_id"))
{
	$sql = "select place_name from places where place_id = " . dbquote($form->value("address_place_id"));
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value("address_place", $row["place_name"]);
	}

}
elseif($form->button("address_country"))
{
	$form->value("address_place_id", "");
	$form->value("address_place", "");
}

// Render page
$page = new Page("posaddresses", "Companies: Data Correction");
require "include/pos_page_actions.php";
$page->header();

$page->title("Companies: Data Correction");

$form->render();

$page->footer();

?>