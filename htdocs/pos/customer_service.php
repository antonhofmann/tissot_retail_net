<?php
/********************************************************************

    customer_service.php

    Creation and mutation of customer service records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-08-08
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-08-08
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");

$form = new Form("customerservices", "Customer Service");

$form->add_section();
$form->add_edit("customerservice_text", "Service*", NOTNULL | UNIQUE);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();


if($form->button(FORM_BUTTON_SAVE))
{
	$db2 = mysql_connect(STORE_LOCATOR_SERVER, STORE_LOCATOR_USER, STORE_LOCATOR_PASSWORD);
	$dbname2 = STORE_LOCATOR_DB;
	mysql_query( "SET NAMES 'utf8'");
	mysql_select_db($dbname2, $db2);


	//check if service is there
	$sql = "select count(customerservice_id) as num_recs " . 
		   "from customerservices where customerservice_id = " . id();
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($row["num_recs"] == 0)
	{
		$sql = 'INSERT into customerservices (customerservice_id, customerservice_text) VALUES (' . 
		       id() . ', ' . dbquote($form->value("customerservice_text")) . ')';
		$res = mysql_query($sql) or dberror($sql);
	}
	else
	{
		$sql = 'UPDATE customerservices set customerservice_text = ' . dbquote($form->value("customerservice_text")) . 
			   ' where customerservice_id = ' . id();
		$res = mysql_query($sql) or dberror($sql);
	}

	
	
	$db = mysql_pconnect(RETAILNET_SERVER, RETAILNET_USER, RETAILNET_PASSWORD);
	$dbname = RETAILNET_DB;
	mysql_query( "SET NAMES 'utf8'");
	mysql_select_db($dbname, $db);
}

$page = new Page("customerservices");
$page->header();
$page->title(id() ? "Edit Customer Service" : "Add Customer Service");
$form->render();
$page->footer();

?>