<?php
/********************************************************************

    poslease.php

    Creation and mutation of lease records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_use_posindex");


$pos = array();
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}

//check if user can edit this company
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	$can_edit = get_user_edit_permission(user_id(), param("pos_id"));
}

$projects = array();
$sql = "select project_order, project_number from posorders " . 
                "inner join projects on project_order = posorder_order " . 
				"where posorder_posaddress = " . param("pos_id") . 
				" order by project_number";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	$projects[$row["project_order"]] = $row["project_number"];
}

// Build form

$form = new Form("posleases", "poslease");

$form->add_section("Name and address");

require_once("include/poslocation_head.php");

$form->add_hidden("country", param("country"));
$form->add_hidden("pos_id", param("pos_id"));
$form->add_hidden("poslease_posaddress", param("pos_id"));
$form->add_hidden("ostate", param("ostate"));
$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("psc", param("psc"));
$form->add_hidden("let", param("let"));
$form->add_hidden("province",  param("province"));

/*
$form->add_section("Landlord");
$form->add_edit("poslease_landlord_name", "Landlord's Name*", NOTNULL);
$form->add_multiline("poslease_landlord_address", "Address", 4);

$form->add_section("Real Estate Agent");
$form->add_edit("poslease_estate_agent", "Name");
$form->add_multiline("poslease_estate_address", "Address", 4);
*/

if(has_access("can_edit_posindex") or has_access("can_edit_lease_data_of_his_country") 
    or ($can_edit == true and has_access("can_edit_his_posindex")))
{
	$form->add_section("Lease Details");
	$form->add_list("poslease_order", "Project Context", $projects);
	$form->add_list("poslease_lease_type", "Lease Type*",
		"select poslease_type_id, poslease_type_name from poslease_types order by poslease_type_id", NOTNULL);
	$form->add_edit("poslease_startdate", "Start Date*", NOTNULL, "", TYPE_DATE);
	$form->add_edit("poslease_enddate", "Expiry Date*", NOTNULL, "", TYPE_DATE);
	$form->add_edit("poslease_signature_date", "Lease Contract Signed on", 0, "", TYPE_DATE);

	$form->add_checkbox("poslease_isindexed", "contains tacit renewal clause:", "", "", "Contract");
	$form->add_edit("poslease_tacit_renewal_duration_years", "Tacit renewal duration years", 0, $posleases["poslease_tacit_renewal_duration_years"], TYPE_INT, 4);

	$form->add_edit("poslease_tacit_renewal_duration_months", "Tacit renewal duration months", 0, $posleases["poslease_tacit_renewal_duration_months"], TYPE_INT, 4);

	$form->add_section("Lease Options");
	$form->add_edit("poslease_extensionoption", "Extension Option to Date", 0, "", TYPE_DATE);
	$form->add_edit("poslease_exitoption", "Exit Option to Date", 0, "", TYPE_DATE);
	$form->add_edit("poslease_termination_time", "Termination deadline*", NOTNULL, "", TYPE_INT, 8, 0, 1, "termination");

	$form->add_edit("poslease_handoverdate", "Handover Date (Key)", 0, "", TYPE_DATE);
	$form->add_edit("poslease_firstrentpayed", "First Rent Paid on", 0, "", TYPE_DATE);
	$form->add_edit("poslease_freeweeks", "Rent Free Period in Weeks", 0, "", TYPE_INT);

	

	//$form->add_section("Lease Termination");
	//$form->add_edit("poslease_nexttermin_date", "Next Termination Date", 0, "", TYPE_DATE);
	//$form->add_multiline("poslease_termin_penalty", "Termination Penalty", 3);

	$form->add_section("Rent Details");
	$form->add_edit("poslease_anual_rent", "Average of annual base rent paid during rent period", 0, "", TYPE_DECIMAL, 12, 2);
	$form->add_edit("poslease_addcharges", "Additional charges", 0, "", TYPE_DECIMAL, 10, 2);
	$form->add_edit("poslease_salespercent", "Percentage if part of the rent is turnover based", 0, "", TYPE_DECIMAL, 10, 2);
	$form->add_list("poslease_currency", "Currency", "select currency_id, currency_symbol from currencies order by currency_symbol");
	


	$form->add_button("save", "Save");
	$form->add_button('delete', "Delete");
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$form->add_section("Lease Details");
	$form->add_lookup("poslease_order", "Project Context", "orders", "order_number");
	$form->add_lookup("poslease_lease_type", "Lease Type", "poslease_types", "poslease_type_name");
	$form->add_label("poslease_startdate", "Start Date");
	$form->add_label("poslease_enddate", "Expiry Date");
	$form->add_label("poslease_signature_date", "Lease Contract Signed on");

	$form->add_checkbox("poslease_isindexed", "contains tacit renewal clause:", "", "", "Contract");
	$form->add_label("poslease_tacit_renewal_duration_years", "Tacit renewal duration years", 0);

	$form->add_label("poslease_tacit_renewal_duration_months", "Tacit renewal duration months", 0);


	$form->add_section("Lease Options");
	$form->add_label("poslease_extensionoption", "Extension Option to Date");
	$form->add_label("poslease_exitoption", "Exit Option to Date");
	$form->add_label("poslease_termination_time", "Termination deadline");

	$form->add_label("poslease_handoverdate", "Handover Date (Key)");
	$form->add_label("poslease_firstrentpayed", "First Rent Paid on");
	$form->add_label("poslease_freeweeks", "Rent Free Period in Weeks");

	//$form->add_section("Lease Termination");
	//$form->add_label("poslease_nexttermin_date", "Next Termination Date");
	//$form->add_label("poslease_termin_penalty", "Termination Penalty");

	$form->add_section("Rent Details");
	$form->add_label("poslease_anual_rent", "Annual Rent");
	$form->add_lookup("poslease_currency", "Currency", "currencies", "currency_symbol");
	$form->add_label("poslease_salespercent", "Annual Rent in % of Sales");
}

$form->add_button("back", "Back");

// Populate form and process button clicks

$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "posleases.php?pos_id=" . param("pos_id")  . "&country=" . param("country") . "&ltf=" . param("ltf") . "&psc=" . param("psc") . "&ostate=" . param("ostate"). '&province=' . param("province") . '&let=' . param("let");
	redirect($link);
}
elseif($form->button('delete'))
{
	$sql = 'delete from posleases where poslease_id = ' . id();
	$res = mysql_query($sql) or dberror($sql);
	
	$link = "posleases.php?pos_id=" . param("pos_id")  . "&country=" . param("country") . "&ltf=" . param("ltf") . "&psc=" . param("psc") . "&ostate=" . param("ostate"). '&province=' . param("province") . '&let=' . param("let");
	redirect($link);
}
elseif($form->button("save"))
{
	
	if($form->value("poslease_isindexed") 
		and !$form->value("poslease_tacit_renewal_duration_years")
		and !$form->value("poslease_tacit_renewal_duration_months"))
	{
		$form->error("Tacit renewal: Please indicate the tacit renewal duration in years and months!");
	}
	elseif(!$form->value("poslease_isindexed") 
		and ($form->value("poslease_tacit_renewal_duration_years")
		or $form->value("poslease_tacit_renewal_duration_months")))
	{
		$form->error("Tacit renewal: Please check the checkbox 'contains tacit renewal clause' or remove the duration information!");
	}
	else
	{
		$form->save();
	}

	//update milestones

	if($form->value('poslease_order'))
	{
		
		//get project id
		$project_id = 0;

		$sql = "select project_id from projects " . 
			   " where project_order = " . $form->value('poslease_order');

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
		
			$project_id = $row["project_id"];

			//create missing milestones
			$sql = "select * from milestones " .
				   "where milestone_active = 1 " .
				   "order by milestone_code ";

			$res = mysql_query($sql) or dberror($sql);
			while($row = mysql_fetch_assoc($res))
			{
				$sql_m = "select count(project_milestone_id) as num_recs from project_milestones " .
						 "where project_milestone_project = " . $project_id . 
						 "   and project_milestone_milestone = " . $row["milestone_id"];
				
				
				$res_m = mysql_query($sql_m) or dberror($sql_m);
				$row_m = mysql_fetch_assoc($res_m);

				if($row_m["num_recs"] == 0)
				{
					$fields = array();
					$values = array();

					$fields[] = "project_milestone_project";
					$values[] = $project_id;

					$fields[] = "project_milestone_milestone";
					$values[] = $row["milestone_id"];
					
					$fields[] = "date_created";
					$values[] = dbquote(date("Y-m-d"));

					$fields[] = "date_modified";
					$values[] = dbquote(date("Y-m-d"));

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					
					if($row["milestone_is_inr03_milestone"] == 1 and $project["project_cost_type"] == 1)
					{
						$sql = "insert into project_milestones (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
					elseif($row["milestone_is_inr03_milestone"] == 0)
					{
						$sql = "insert into project_milestones (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
				}
			}

			if($form->value('poslease_signature_date'))
			{
				$sql_u = "update project_milestones SET " . 
						 "project_milestone_date = " . dbquote(from_system_date($form->value("poslease_signature_date"))) . 
						 " where project_milestone_project = " . $project_id . 
						 " and project_milestone_milestone = 33";


				$result = mysql_query($sql_u) or dberror($sql_u);
			}
		}
	}
}

// Render page

$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();

$page->title(id() ? "Edit Lease Information" : "Add Lease Information");

require_once("include/tabs.php");

$form->render();

?>

<div id="termination" style="display:none;">
    Number of month until when latest you have to give notice to your landlord whether you want to extend or terminate current lease contract
</div> 

<?php
$page->footer();

?>