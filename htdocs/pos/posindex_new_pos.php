<?php
/********************************************************************

    posindex_new_pos.php

    Creation of a new POS.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-06-16
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-06-16
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

if(has_access("can_edit_posindex") or has_access("can_edit_his_posindex"))
{
}
else
{
	redirect("noaccess.php");
}

set_referer("posprojects.php");

if(param("pos_id"))
{
	register_param("pos_id");
	param("id", param("pos_id"));
}
else
{
	register_param("pos_id");
	param("pos_id", id());
}

$pos = array();
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}

$has_project = update_posdata_from_posorders(param("pos_id"));

$ratings = array();
$ratings[5] = "bad";
$ratings[4] = "poor";
$ratings[3] = "good";
$ratings[2] = "very good";
$ratings[1] = "excellent";
//$ratings[0] = "n.a.";


//posareas
$posareas = array();
$sql = "select posareatype_id, posareatype_name " . 
	   "from posareatypes " . 
	   " order by posareatype_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$posareas[$row["posareatype_id"]]	= $row["posareatype_name"];
}

//check if user can edit this company
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	$can_edit = get_user_edit_permission(user_id(), param("pos_id"));
}


// Build form
$form = new Form("posaddresses", "posaddress");

$form->add_section("Roles");

if(has_access("can_edit_posindex"))
{
	$sql_clients = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where (address_active = 1 and (address_type = 1 or address_type = 4))  order by country_name, address_company";

	$sql_franchisees = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1) order by country_name, address_company";

	$sql_franchisors = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisor = 1 order by country_name, address_company";
}
else
{
	$user = get_user(user_id());

	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " or country_id IN (" . implode(",", $tmp) . ") ";
	}

	if($country_filter == "")
	{
		$sql_clients = "select address_id, " . 
			"concat(country_name, ': ', address_company, ', ', address_place) as company " . 
			" from addresses " . 
			" left join countries on country_id = address_country " . 
			" where address_active = 1 and (address_type = 1 or address_type = 4)  " .
			" and address_id =  " . $user["address"] . 
		    " order by country_name, address_company";


		$sql_franchisees = "select address_id, " .
			               " concat(country_name, ': ', address_company, ', ', address_place) as company " .
			               " from addresses " .
			               " left join countries on country_id = address_country " . 
			               " where address_active = 1  " . 
			               " and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1) " .
			               " and (address_parent =  " . $user["address"] . 
			               "  or address_id = " . $user["address"] . ") " . 
			               " order by country_name, address_company";

		$sql_franchisors = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisor = 1 order by country_name, address_company";
	}
	else
	{

		$sql_clients = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company " . 
			" from addresses " . 
			"left join countries on country_id = address_country " . 
			" where address_active = 1 and (address_type = 1 or address_type = 4)  " .
			" and (address_id =  " . $user["address"] . " " . $country_filter . " ) " .
		    " order by country_name, address_company";


		$sql_franchisees = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company " .
			               " from addresses " . 
			               " left join countries on country_id = address_country " . 
			               " where address_showinposindex = 1 " . 
			               " and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1) " .
			               " and (address_id =  " . $user["address"] . " " . $country_filter . " ) " . 
			               " order by country_name, address_company";

		$sql_franchisors = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company " .
			"from addresses left join countries on country_id = address_country " . 
			"where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisor = 1 order by country_name, address_company";
	}
}


$form->add_list("posaddress_client_id", "Subsidiary/Agent*", $sql_clients	, NOTNULL);



$form->add_list("posaddress_ownertype", "Legal Type*",
	"select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id IN (1, 2, 6)", SUBMIT | NOTNULL);


$form->add_edit("posaddress_eprepnr", "Enterprise Reporting Number");
$form->add_edit("posaddress_sapnumber", "SAP Number");


$form->add_list("posaddress_franchisor_id", "Franchisor", $sql_franchisors);

if(param("posaddress_ownertype") != 6)
{
	$tmp = "Owner Company*";
}
else
{
	$tmp = "Owner Company*";
}


$form->add_list("posaddress_franchisee_id", $tmp, $sql_franchisees, NOTNULL);


$form->add_list("posaddress_store_furniture", "Product Line*",
		"select product_line_id, product_line_name from product_lines where product_line_posindex = 1 order by product_line_name", NOTNULL);

$form->add_list("posaddress_store_postype", "POS Type*",
	"select postype_id, postype_name from postypes order by postype_name", NOTNULL);

$form->add_list("posaddress_store_subclass", "POS Type Subclass",
		"select possubclass_id, possubclass_name from possubclasses order by possubclass_name");



$form->add_section("Address Data");
//$form->add_edit("posaddress_name", "POS Name*", NOTNULL);
$form->add_edit("posaddress_name", "POS Name*", NOTNULL, "", TYPE_CHAR, 0, 0, 2, "posaddress_name");

//$form->add_edit("posaddress_name2", "", 0);
$form->add_edit("posaddress_name2", "", 0, "", TYPE_CHAR, 0, 0, 2, "posaddress_name2");

$form->add_hidden("posaddress_address");
$form->add_multi_edit("street", array("posaddress_street", "posaddress_street_number"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6), array('',''), 2, "posaddress_address", '', array(40,5));

//$form->add_edit("posaddress_address2", "Address 2");
$form->add_edit("posaddress_address2", "Address 2", 0, "", TYPE_CHAR, 0, 0, 2, "posaddress_address2");




$form->add_list("posaddress_country", "Country*",
	"select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT, param("country"));



if(param("posaddress_country"))
{
	$form->add_list("posaddress_place_id", "City Selection*",
		"select place_id, concat(place_name, ' (', province_canton, ')') from places left join provinces on province_id = place_province where place_country = " . dbquote(param("posaddress_country")) . " order by place_name", SUBMIT | NOTNULL);
}
else
{
	$form->add_list("posaddress_place_id", "City Selection*",
		"select place_id, concat(place_name, ' (', province_canton, ')') from places left join provinces on province_id = place_province where place_country = " . dbquote(param("country")) . " order by place_name", SUBMIT | NOTNULL);
}

$form->add_edit("posaddress_place", "City", NOTNULL | DISABLED);


$form->add_edit("posaddress_zip", "Zip*", NOTNULL);


$form->add_section("Communication");
$form->add_hidden("posaddress_phone");
$form->add_multi_edit("phone_number", array("posaddress_phone_country", "posaddress_phone_area", "posaddress_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

$form->add_hidden("posaddress_mobile_phone");
$form->add_multi_edit("mobile_phone_number", array("posaddress_mobile_phone_country", "posaddress_mobile_phone_area", "posaddress_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));
$form->add_edit("posaddress_email", "Email");


$form->add_edit("posaddress_contact_name", "Contact Name");
$form->add_edit("posaddress_website", "Website");


$form->add_section("Dates");
$form->add_edit("posaddress_store_openingdate", "Opening Date");
$form->add_edit("posaddress_store_closingdate", "Closing Date");


$form->add_section("Environment");

foreach($posareas as $key=>$name)
{
	$form->add_checkbox("area". $key, $name, "", 0, "");
}


$form->add_comment("Please indicate on which floor the POS will be situated (Ground floor, Street Level, 1st Floor etc.).");

$form->add_edit("posaddress_store_floor", "Floor*", NOTNULL,$pos["posaddress_store_floor"], TYPE_CHAR, 30);

$form->add_section("Area Perception");
$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");

$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from accross the Street*", $ratings, 0, NOTNULL);



$form->add_section("Website " . BRAND_WEBSITE);
$form->add_checkbox("posaddress_export_to_web", "Show this POS in the Store Locator", "", "", BRAND_WEBSITE);
$form->add_checkbox("posaddress_email_on_web", "Show Email Address in the Store Locator", "", "", BRAND_WEBSITE);

$form->add_section("Address Check");
$form->add_checkbox("posaddress_checked", "Address check", "", "", "Adress is ok");

$form->add_button('save_data', "Save");



$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("ostate", param("ostate"));


// Populate form and process button clicks

$form->populate();
$form->process();



if($form->button("save_data"))
{
	$error = 0;
		

	if($form->validate())
	{
		
		$form->value("posaddress_phone", $form->unify_multi_edit_field($form->items["phone_number"]));
		$form->value("posaddress_mobile_phone", $form->unify_multi_edit_field($form->items["mobile_phone_number"]));

		$form->value("posaddress_address", $form->unify_multi_edit_field($form->items["street"], get_country_street_number_rule($form->value("posaddress_country"))));
		

		$form->save();

		$pos_id = mysql_insert_id();

		//update posareas
		$sql = "delete from posareas where posarea_posaddress =" . dbquote($pos_id);
		$result = $res = mysql_query($sql) or dberror($sql);
		foreach($posareas as $key=>$name)
		{
			if($form->value("area" . $key) == 1)
			{
				$fields = array();
				$values = array();

				$fields[] = "posarea_posaddress";
				$values[] = dbquote($pos_id);

				$fields[] = "posarea_area";
				$values[] = dbquote($key);

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());

				$sql = "insert into posareas (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
			}
		}


		//check if google map is created
		$sql = "select posaddress_google_precision from posaddresses where posaddress_id = " . $pos_id;
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if(!$row["posaddress_google_precision"])
			{
				$result = google_maps_geo_encode_pos($pos_id, $context);
			}
		}

		if($pos_id)
		{
			update_store_locator($pos_id);
			mysql_select_db(RETAILNET_DB, $db);
		}
		redirect("posindex_pos.php?country=" . $form->value("posaddress_country") . "&id=" . $pos_id);
	}
}
elseif($form->button("posaddress_place_id"))
{
	$sql = "select place_name " .
		   "from places " . 
		   "where place_id = " . dbquote($form->value("posaddress_place_id"));
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->value("posaddress_place", $row["place_name"]);
	}
}
elseif($form->button("posaddress_country"))
{
	$form->value("posaddress_place",  "");
	$form->value("posaddress_place_id", 0);
}


// Render page
$poslocation = get_poslocation(id(), "posaddresses");

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title(id() ? "Basic Data: " . $poslocation["posaddress_name"] : "Add POS Location");

if(id())
{
	require_once("include/tabs.php");
}

$form->render();

echo "<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>";

?>
<script language="javascript">
	$("#h_posaddress_name").click(function() {
	   $('#posaddress_name').val($('#posaddress_name').val().toLowerCase());
	   var txt = $('#posaddress_name').val();

	   $('#posaddress_name').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_posaddress_name2").click(function() {
	   $('#posaddress_name2').val($('#posaddress_name2').val().toLowerCase());
	   var txt = $('#posaddress_name2').val();

	   $('#posaddress_name2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_posaddress_address").click(function() {
	   $('#posaddress_address').val($('#posaddress_address').val().toLowerCase());
	   var txt = $('#posaddress_address').val();

	   $('#posaddress_address').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_posaddress_address2").click(function() {
	   $('#posaddress_address2').val($('#posaddress_address2').val().toLowerCase());
	   var txt = $('#posaddress_address2').val();

	   $('#posaddress_address2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});



	
</script>
<?php
$page->footer();

?>