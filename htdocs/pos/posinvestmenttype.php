<?php
/********************************************************************

    posinvestmenttype.php

    Creation and mutation of investment type records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");



$form = new Form("posinvestment_types", "investment type");

$form->add_section("Investment Type");
$form->add_hidden("posinvestment_type_sortorder");



$form->add_edit("posinvestment_type_group", "Group*", NOTNULL);
$form->add_edit("posinvestment_type_name", "Name*", NOTNULL);
$form->add_checkbox("posinvestment_type_active", "Active");
$form->add_checkbox("posinvestment_type_intangible", "Itangible");

$form->add_section("Cost Monitoring Parameters");

$form->add_edit("posinvestment_type_cms_sumcostgroups", "Sum of the following Cost Groups");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();

if (!$form->value("posinvestment_type_sortorder"))
{
    $sql = "select max(posinvestment_type_sortorder) from posinvestment_types";
    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);
    $form->value("posinvestment_type_sortorder", $row[0] + 1);
}

$form->process();

$page = new Page("posinvestmenttypes");
$page->header();
$page->title(id() ? "Edit Investment Type" : "Add Investment Type");
$form->render();
$page->footer();

?>