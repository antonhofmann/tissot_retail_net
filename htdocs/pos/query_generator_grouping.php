<?php
/********************************************************************

    query_generator_grouping.php

    Set Grouping for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2009-01-11
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2009-01-11
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_query_posindex");

require_once "include/query_get_functions.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("query_generator.php");
}

$query_id = param("query_id");

$posquery = get_query_name($query_id);
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


//get Query Fields
$fields = array();

$sql = "select posquery_fields, posquery_grouping01, posquery_grouping02 from posqueries " .
	   "where posquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$fields = unserialize($row["posquery_fields"]);
	if($fields)
	{
		$fields = array_merge($fields["pl"], $fields["cl"], $fields["fr"], $fields["po"], $fields["in"]);
	}

	$grouping01 = $row["posquery_grouping01"];
	$grouping02 = $row["posquery_grouping02"];
}

$groups = query_group_fields();


$choices = array();
foreach($groups as $key=>$field)
{
	if(array_key_exists($field, $fields))
	{
		$choices[$key] = $fields[$field];
	}
}


/********************************************************************
    create form
*********************************************************************/

$form = new Form("posqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_hidden("form_save", 1);

$form->add_section($posquery["name"]);

$form->add_section(" ");
$form->add_comment("Please specify the grouping criteria of your query.");

if(count($choices) > 0)
{
	$form->add_list("posquery_grouping01", "Group 1", $choices, 0, $grouping01);
}

if(count($choices) > 1)
{
	$form->add_list("posquery_grouping02", "Group 2", $choices, 0, $grouping02);
}
else
{
	$form->add_hidden("posquery_grouping02", 0);
}

if(count($choices) > 0)
{
	$form->add_button("submit", "Save Grouping", 0);
}
if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}
$form->add_button("back", "Back to the List of Queries");


/********************************************************************
    process form
*********************************************************************/
$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("query_generator.php");
}
elseif($form->button("submit"))
{
	
	if(!$form->value("posquery_grouping01") and $form->value("posquery_grouping02") > 0)
	{
		$form->error("Group 2 can not be indicated without indicating Group 1.");
	}
	else
	{
		$sql = "Update posqueries SET " . 
			   "posquery_grouping01 = " . dbquote($form->value("posquery_grouping01")) . ", " . 
			   "posquery_grouping02 = " . dbquote($form->value("posquery_grouping02"))  . ", " . 
			   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
		       "user_modified = " . dbquote(user_login()) . 
			   " where posquery_id = " . param("query_id");

		$result = mysql_query($sql) or dberror($sql);

		redirect("query_generator_grouping.php?query_id=" . param("query_id"));
	}
}
elseif($form->button("execute"))
{
	redirect("query_generator_xls.php?query_id=" . param("query_id"));
}


/********************************************************************
    render
*********************************************************************/

$page = new Page("query_generator");
$page->header();
$page->title("Edit POS Query - Grouping");

require_once("include/query_tabs.php");

$form->render();

$page->footer();

?>


