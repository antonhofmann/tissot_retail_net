<?php
/********************************************************************

    posfiletypes.php

    Lists file types for editing.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("posfiletype.php");

$list = new ListView("select posfiletype_id, posfiletype_name, posfiletype_mime_type, posfiletype_extension " .
                     "from posfiletypes");

$list->set_entity("file types");
$list->set_order("posfiletype_name");

$list->add_column("posfiletype_name", "Name", "posfiletype.php", LIST_FILTER_FREE);
$list->add_column("posfiletype_mime_type", "MIME Type", "", LIST_FILTER_FREE);
$list->add_column("posfiletype_extension", "Extension", "", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_NEW, "New", "posfiletype.php");
$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");

$list->process();

$page = new Page("posfiletypes");

$page->header();
$page->title("File Types");
$list->render();
$page->footer();

?>
