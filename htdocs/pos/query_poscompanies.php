<?php
/********************************************************************

    query_poscompanies.php

    Generate Excel-File of POS Companies

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-06-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-06-09
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_posindex");

/********************************************************************
    prepare all data needed
*********************************************************************/
$user = get_user(user_id());
if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{

	$sql_salesregion = "select salesregion_id, salesregion_name ".
				   "from salesregions order by salesregion_name";


	$sql_country = "select DISTINCT country_id, country_name ".
				   "from posaddresses " . 
				   "inner join countries on country_id = posaddress_country " . 
				   "order by country_name";

}
else
{

	$link = "query_poscompanies_xls.php" .
		    "?co=" . $user["country"] .
            "&re=";
	
	redirect($link);
}

if(isset($_POST["show_benchmark"]) and $_POST["show_benchmark"] == 1)
{
    //salesregions
    $RE = "";

    if($_POST["RE_0"])
    {
        $RE = "";
    }
    else
    {
        $res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
        while($row = mysql_fetch_assoc($res))
        {
            if($_POST["RE_" . $row["salesregion_id"]])
            {
                $RE .=$row["salesregion_id"] . "-";
            }
        }
    }


    //Countries
    $CO = "";

    if($_POST["CO_0"])
    {
        $CO = "";
    }
    else
    {
        $res = mysql_query($sql_country) or dberror($sql_country);
        while($row = mysql_fetch_assoc($res))
        {
            if($_POST["CO_" . $row["country_id"]] == 1)
            {
				$CO .=$row["country_id"] . "-";
            }
        }
    }


    $link = "query_poscompanies_xls.php" .
		    "?co=" . $CO .
            "&re=" . $RE;
	
	redirect($link);
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("projects", "projects");

	$form->add_comment("");
	$form->add_label("L4", "Geographical Region", "", "Select the Geographical Regions to be included in the Report");
	$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
	$form->add_checkbox("RE_0", "Every Geographical Region", false);
	while($row = mysql_fetch_assoc($res))
	{
		$form->add_checkbox("RE_" . $row["salesregion_id"], $row["salesregion_name"], false);
	}

	$form->add_comment("");
	$form->add_label("L5", "Country", "", "Select the Countries to be included in the Report");
	$form->add_checkbox("CO_0", "Every Country", false);
	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res))
	{
		$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], false);
	}
$form->add_hidden("show_benchmark", 1, 0);
$form->add_button("benchmark_xls", "Generate List", "");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("queries");


require_once "../include/xls/Writer.php";

$page->header();
$page->title("POS Companies");

$form->render();

$page->footer();
