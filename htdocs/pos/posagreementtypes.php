<?php
/********************************************************************

    posagreementtypes.php

    Lists agreementtypes for editing.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-08-20
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-20
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("posagreementtype.php");

$list = new ListView("select agreement_type_id, agreement_type_name " .
                     "from agreement_types");

$list->set_entity("agreement_types");
$list->set_order("agreement_type_name");

$list->add_column("agreement_type_name", "Name", "posagreementtype.php");

$list->add_button(LIST_BUTTON_NEW, "New", "posagreementtype.php");

$list->process();

$page = new Page("posagreementtypes");

$page->header();
$page->title("Agreement Types");
$list->render();
$page->footer();

?>
