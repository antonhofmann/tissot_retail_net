<?php
/********************************************************************

    poslocation_head.php

    Entry page for the projects section.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
$poslocation = get_poslocation(param("pos_id"), "posaddresses");

$tmp = "";
if($poslocation["address_company"])
{
	$tmp = $poslocation["address_company"] . ", " . $poslocation["address_place"];
}

$form->add_label("posclient", "Client", 0, $tmp);
$form->add_label("posname", "POS Name", 0, $poslocation["posaddress_name"]);

$tmp = $poslocation["posaddress_zip"] . " " . $poslocation["posaddress_place"] . ", " . $poslocation["country_name"];
$form->add_label("posplace", "City", 0, $tmp);

?>