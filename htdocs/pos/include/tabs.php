<?php
$page->add_tab("basicdata", "Basic Data", "posindex_pos.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);

$page->add_tab("store", "POS", "posstore.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);

if($pos["posaddress_store_postype"] != 4) //independent retailer
{
	
	$page->add_tab("ownership", "Ownership", "posownership.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);
	$page->add_tab("leases", "Leases", "posleases.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);
	
	//$page->add_tab("investment", "Intangible Assets", "posinvestments.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);
	
	//$page->add_tab("closings", "Closures", "posclosings.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);
	$page->add_tab("projects", "Projects", "posprojects.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);
	//$page->add_tab("orders", "Orders", "posorders.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);
	$page->add_tab("files", "Files", "posfiles.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);
}
else
{
	//$page->add_tab("closings", "Closures", "posclosings.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);
	$page->add_tab("projects", "Projects", "posprojects.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);
	//$page->add_tab("orders", "Orders", "posorders.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);
	$page->add_tab("files", "Files", "posfiles.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);
}

$page->add_tab("googlemap", "Google Map", "posindex_map.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);

if(has_access("can_edit_pos_opening_hours") or has_access("can_view_pos_opening_hours")) 
{
	//$page->add_tab("openinghrs", "Opening Hours", "posindex_openinghr.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);
}

if(has_access("can_edit_customerservices") or has_access("can_view_customerservices"))
{

	$page->add_tab("customerservice", "Customer Services", "posindex_customerservice.php?pos_id=" . param('pos_id') . "&country=" . param("country"). '&ltf=' . param('ltf') . '&psc=' . param('psc') . '&ostate=' . param('ostate'). '&province=' . param("province") . '&let=' . param("let"), $target = "_self", $flags = 0);

}


if(has_access("can_edit_posindex"))
{
	$link = "javascript:popup('pos_tracking_pdf.php?id=" .  param('pos_id') . "', 640,480)";
	$page->add_tab("tracking", "Tracking", $link);
}

$page->tabs();


?>