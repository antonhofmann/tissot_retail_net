<?php
/********************************************************************

    correct_address_data_preselect.php

    Preselection of Address List for data corrections

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/access_filters.php";

check_access("can_correct_pos_data");
set_referer("posindex.php");

//compose list
if(has_access("can_view_posindex") or has_access("can_edit_posindex"))
{
	$sql = "select DISTINCT country_id, country_name " .
		   "from _addresses " . 
		   "left join countries on address_country = country_id " . 
		   "where country_name is not null " . 
		   "order by country_name";
	
}
else
{

	$country_access_filter = get_users_regional_access_to_companies(user_id());
	foreach($country_access_filter as $key=>$country_id)
	{
		$tmp[] = $country_id;
	}

	if(count($tmp) > 0) {
		$country_filter = " address_country IN (" . implode(",", $tmp) . ") ";
	}
	else
	{
		$user = get_user(user_id());

		$sql = "select DISTINCT address_country " .
			   "from _addresses " . 
			   "where address_country = " . $user["country"];


		$num_of_countries = 0;
		$res = mysql_query($sql) or dberror($sql);
		$num_rows = mysql_num_rows($res);
		if($num_rows < 2)
		{
			if($row = mysql_fetch_assoc($res))
			{
				redirect("correct_address_data.php?country=" . $row["address_country"]);
			}
		}
	}

	
	if($country_filter == "")
	{
		$sql = "select DISTINCT country_id, country_name " .
			   "from _addresses " . 
			   "left join countries on address_country = country_id " . 
			   "where country_name is not null " . 
			   " and address_country = " . $user["country"];
			   "order by country_name";
	}
	else
	{
		$sql = "select DISTINCT country_id, country_name " .
			   "from _addresses " . 
			   "left join countries on address_country = country_id " . 
			   "where country_name is not null " . 
			   " and " . $country_filter;
			   "order by country_name";
	}
}


$form = new Form("posaddresses", "posaddress");

$form->add_section("Country Selection");

$form->add_list("country", "Country Selection",$sql, SUBMIT);


$form->populate();
$form->process();


if(param("country"))
{
	redirect("correct_address_data.php?country=" .param("country"));
}

$page = new Page("posaddresses", "Companies: Data Corrections");
require "include/pos_page_actions.php";
$page->header();

$page->title("Companies: Data Corrections");
$form->render();


$page->footer();

?>
