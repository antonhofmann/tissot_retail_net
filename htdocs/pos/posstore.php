<?php
/********************************************************************

    posstore.php

    Creation and mutation of posaddress records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";


check_access("can_use_posindex");

if(param("pos_id"))
{
	register_param("pos_id");
	param("id", param("pos_id"));
}
else
{
	register_param("pos_id");
	param("pos_id", id());
}

$old_store_planned_closingdate = "";
$old_store_closingdate = "";
$pos = array();
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
	$old_store_planned_closingdate = $pos["posaddress_store_planned_closingdate"];
	$old_store_closingdate = $pos["posaddress_store_closingdate"];

	if($old_store_planned_closingdate == '0000-00-00')
	{
		$old_store_planned_closingdate = "";
	}
	if($old_store_closingdate == '0000-00-00')
	{
		$old_store_closingdate = "";
	}
}

$has_project = update_posdata_from_posorders(param("pos_id"));

if(has_access("can_edit_posindex"))
{
	$sql_distribution_channels = "SELECT DISTINCT mps_distchannels.mps_distchannel_id, " .
	" concat(mps_distchannel_group_name, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) as distributionchannel " . 
	"from mps_distchannels " . 
	"left join mps_distchannel_groups on mps_distchannel_groups.mps_distchannel_group_id = mps_distchannels.mps_distchannel_group_id " .
	"where mps_distchannel_active = 1 or mps_distchannel_active = {posaddress_distribution_channel} " . 
	" order by mps_distchannel_group_name, mps_distchannels.mps_distchannel_name, mps_distchannels.mps_distchannel_code";
}
else
{

	$sql_distribution_channels = "SELECT DISTINCT mps_distchannels.mps_distchannel_id, " .
	" concat(mps_distchannel_group_name, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) as distributionchannel " . 
	"from mps_distchannels " . 
	"left join mps_distchannel_groups on mps_distchannel_groups.mps_distchannel_group_id = mps_distchannels.mps_distchannel_group_id " . 
	" left JOIN posdistributionchannels ON mps_distchannels.mps_distchannel_id = posdistributionchannels.posdistributionchannel_dchannel_id " . 
	" where (posdistributionchannels.posdistributionchannel_postype_id = " .  dbquote($pos['posaddress_store_postype']) . 
	" and (posdistributionchannels.posdistributionchannel_possubclass_id = " . dbquote($pos['posaddress_store_subclass']) . 
	" or posdistributionchannels.posdistributionchannel_possubclass_id = 0) " . 
	" and posdistributionchannels.posdistributionchannel_legaltype_id = ". dbquote($pos['posaddress_ownertype']) . ") " . 
	" or (mps_distchannels.mps_distchannel_id = " . dbquote($pos['posaddress_distribution_channel']) . ") " .
	" or (posdistributionchannels.posdistributionchannel_legaltype_id = ". dbquote($pos['posaddress_ownertype']) .
	"    and posdistributionchannels.posdistributionchannel_postype_id = " .  dbquote($pos['posaddress_store_postype']) . 
	"	 and posdistributionchannel_assignable_by_user = 1) " . 
	" order by mps_distchannel_group_name, mps_distchannels.mps_distchannel_name, mps_distchannels.mps_distchannel_code";
}

//check if user can edit this company
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	$can_edit = get_user_edit_permission(user_id(), param("pos_id"));
}


// Build form

$form = new Form("posaddresses", "posaddress");

$form->add_section("Classification");


$form->add_hidden("old_store_planned_closingdate", to_system_date($old_store_planned_closingdate));

if(has_access("can_edit_posindex") and (id() > 0 and $pos["posaddress_store_postype"] != 4) or id() == 0)
{
	if($has_project == false)
	{
		$form->add_list("posaddress_ownertype", "Legal Type*",
			"select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id IN (1, 2, 6)", NOTNULL);

		$form->add_list("posaddress_store_furniture", "Product Line*",
			"select product_line_id, product_line_name from product_lines where product_line_posindex = 1 order by product_line_name", NOTNULL);

		$form->add_list("posaddress_store_furniture_subclass", "Product Line Subclass",
			"select productline_subclass_id, productline_subclass_name from productline_subclasses where productline_subclass_productline = {posaddress_store_furniture} order by productline_subclass_name", 0);

		$form->add_list("posaddress_store_postype", "POS Type*",
			"select postype_id, postype_name from postypes order by postype_name", NOTNULL);

		$form->add_list("posaddress_store_subclass", "POS Type Subclass",
			"select possubclass_id, possubclass_name from possubclasses order by possubclass_name");
	}
	else
	{
		
		//$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text");
		
		$form->add_list("posaddress_ownertype", "Legal Type*",
			"select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id IN (1, 2, 6)", NOTNULL);

		$form->add_lookup("posaddress_store_furniture", "Product Line", "product_lines", "product_line_name");
		$form->add_lookup("posaddress_store_furniture_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name");
		
		
		$form->add_lookup("posaddress_store_postype", "POS Type", "postypes", "postype_name");
		$form->add_lookup("posaddress_store_subclass", "POS Type Subclass", "possubclasses", "possubclass_name");
	}

	$form->add_checkbox("posaddress_is_flagship", "the POS is a flag ship POS", 0, "", "Flag Ship Option");

	
	$form->add_checkbox("posaddress_local_production", "project is locally realized (local production)", 0, DISABLED, "Local Production");
	
	$form->add_lookup("posaddress_project_type_subclass_id", "Project Type Subclass", "project_type_subclasses", "project_type_subclass_name");

	
	$form->add_checkbox("posaddress_uses_icedunes_visuals", "the POS uses Visuals", 0, 0, "Visuals");
	
	$form->add_section("Distribution Channel");
	

	$form->add_list("posaddress_distribution_channel", "Distribution Channel",$sql_distribution_channels);

	$form->add_section("Dates");
	$form->add_edit("posaddress_store_openingdate", "Opening Date", 0, "", TYPE_DATE);
	
	
	
	
	$form->add_edit("posaddress_store_planned_closingdate", "Planned Closing Date", 0, "", TYPE_DATE);
	if($pos["posaddress_store_planned_closingdate"] != NULL and $pos["posaddress_store_planned_closingdate"] != '0000-00-00')
	{
		$form->add_edit("planned_closing_change_comment", "Reason for changing planned closing date*", 0);
	}
	else
	{
		$form->add_hidden("planned_closing_change_comment");
	}
	
	
	
	$form->add_edit("posaddress_store_closingdate", "Closing Date", 0, "", TYPE_DATE);

	$form->add_section("Surfaces");
	//$form->add_edit("posaddress_store_grosssurface", "Gross Surface in sqms", 0, "", TYPE_DECIMAL, 8,2, 3, "grosssqm");
	$form->add_hidden("posaddress_store_grosssurface");
	$form->add_edit("posaddress_store_totalsurface", "Total Surface in sqms", 0, "", TYPE_DECIMAL, 8,2, 3, "totalsqm");
	$form->add_edit("posaddress_store_retailarea", "Sales Surface in sqms", 0, "", TYPE_DECIMAL, 8,2, 3, "retailsqm");
	$form->add_edit("posaddress_store_backoffice", "Other Surface in sqms", 0, "", TYPE_DECIMAL, 8,2, 3, "backofficesqm");
	$form->add_edit("posaddress_store_numfloors", "Number of Floors", 0, "", TYPE_INT);
	$form->add_edit("posaddress_store_floorsurface1", "Floor 1: Surface  in sqms", 0, "", TYPE_DECIMAL, 8);
	$form->add_edit("posaddress_store_floorsurface2", "Floor 2: Surface  in sqms", 0, "", TYPE_DECIMAL, 8);
	$form->add_edit("posaddress_store_floorsurface3", "Floor 3: Surface  in sqms", 0, "", TYPE_DECIMAL, 8);

	$form->add_section("Staff");
	
	if($has_project)
	{
		$form->add_label("posaddress_store_headcounts", "Headcounts");
		$form->add_label("posaddress_store_fulltimeeqs", "Full Time Equivalents");
	}
	else
	{
		$form->add_edit("posaddress_store_headcounts", "Headcounts", 0, "", TYPE_INT);
		$form->add_edit("posaddress_store_fulltimeeqs", "Full Time Equivalents", 0, "", TYPE_DECIMAL, 8);
	}

	/*
	$form->add_section("Equipment");

		$sql = "select posequipmenttype_id, posequipmenttype_name " . 
			   "from posequipmenttypes " . 
			   " order by  posequipmenttype_name";

		$form->add_checklist("equipment", "Equipment", "posequipments",$sql, RENDER_HTML);

	*/

	$form->add_hidden("country", param("country"));
	$form->add_button(FORM_BUTTON_SAVE, "Save");
}
elseif($can_edit == true and has_access("can_edit_his_posindex") and (id() > 0 and $pos["posaddress_store_postype"] != 4))
{
	if($has_project == false)
	{
		$form->add_list("posaddress_ownertype", "Legal Type*",
			"select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id IN (1, 2, 6)", NOTNULL);

		$form->add_list("posaddress_store_furniture", "Product Line*",
			"select product_line_id, product_line_name from product_lines where product_line_posindex = 1 order by product_line_name", NOTNULL);

		$form->add_list("posaddress_store_furniture_subclass", "Product Line Subclass",
			"select productline_subclass_id, productline_subclass_name from productline_subclasses where productline_subclass_productline = {posaddress_store_furniture} order by productline_subclass_name", 0);


		$form->add_list("posaddress_store_postype", "POS Type*",
			"select postype_id, postype_name from postypes order by postype_name", NOTNULL);

		$form->add_list("posaddress_store_subclass", "POS Type Subclass",
			"select possubclass_id, possubclass_name from possubclasses where possubclass_selectable_in_mps = 1 order by possubclass_name");
	}
	else
	{
		$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text");
		$form->add_lookup("posaddress_store_furniture", "Product Line", "product_lines", "product_line_name");
		$form->add_lookup("posaddress_store_furniture_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name");
		$form->add_lookup("posaddress_store_postype", "POS Type", "postypes", "postype_name");

		$form->add_lookup("posaddress_store_subclass", "POS Type Subclass", "possubclasses", "possubclass_name");
	}

	$form->add_checkbox("posaddress_is_flagship", "the POS is a flag ship POS", 0, DISABLED, "Flag Ship Option");

	
	$form->add_checkbox("posaddress_local_production", "project is locally realized (local production)", 0, DISABLED, "Local Production");
	
	
	$form->add_lookup("posaddress_project_type_subclass_id", "Project Type Subclass", "project_type_subclasses", "project_type_subclass_name");
	$form->add_checkbox("posaddress_uses_icedunes_visuals", "the POS uses Visuals", 0, DISABLED, "Visuals");
	
		
	$form->add_section("Distribution Channel");

	
	$form->add_list("posaddress_distribution_channel", "Distribution Channel",$sql_distribution_channels);

	$form->add_section("Dates");
	$form->add_label("posaddress_store_openingdate", "Opening Date");
	$form->add_label("posaddress_store_planned_closingdate", "Planned Closing Date");
	$form->add_hidden("planned_closing_change_comment");
	
	$form->add_label("posaddress_store_closingdate", "Closing Date");

	$form->add_section("Surfaces");
	//$form->add_edit("posaddress_store_grosssurface", "Gross Surface in sqms", 0, "", TYPE_DECIMAL, 8,2, 3, "grosssqm");
	$form->add_hidden("posaddress_store_grosssurface");
	$form->add_edit("posaddress_store_totalsurface", "Total Surface in sqms", 0, "", TYPE_DECIMAL, 8,2, 3, "totalsqm");
	$form->add_edit("posaddress_store_retailarea", "Sales Surface in sqms", 0, "", TYPE_DECIMAL, 8,2, 3, "retailsqm");
	$form->add_edit("posaddress_store_backoffice", "Other Surface in sqms", 0, "", TYPE_DECIMAL, 8,2, 3, "backofficesqm");

	$form->add_edit("posaddress_store_numfloors", "Number of Floors", 0, "", TYPE_INT);
	$form->add_edit("posaddress_store_floorsurface1", "Floor 1: Surface  in sqms", 0, "", TYPE_DECIMAL, 8);
	$form->add_edit("posaddress_store_floorsurface2", "Floor 2: Surface  in sqms", 0, "", TYPE_DECIMAL, 8);
	$form->add_edit("posaddress_store_floorsurface3", "Floor 3: Surface  in sqms", 0, "", TYPE_DECIMAL, 8);

	$form->add_section("Staff");
	$form->add_edit("posaddress_store_headcounts", "Headcounts", 0, "", TYPE_INT);
	$form->add_edit("posaddress_store_fulltimeeqs", "Full Time Equivalents", 0, "", TYPE_DECIMAL, 8);

	/*
	$form->add_section("Equipment");

		$sql = "select posequipmenttype_id, posequipmenttype_name " . 
			   "from posequipmenttypes " . 
			   " order by  posequipmenttype_name";

		$form->add_checklist("equipment", "Equipment", "posequipments",$sql, RENDER_HTML);

	*/

	$form->add_hidden("country", param("country"));
	$form->add_button(FORM_BUTTON_SAVE, "Save");

}
elseif(has_access("can_edit_catalog"))
{
	$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text");
	$form->add_lookup("posaddress_store_postype", "POS Type", "postypes", "postype_name");

	$form->add_list("posaddress_store_subclass", "POS Type Subclass",
			"select possubclass_id, possubclass_name from possubclasses where possubclass_selectable_in_mps = 1 order by possubclass_name");
	
	$form->add_checkbox("posaddress_is_flagship", "the POS is a flag ship POS", 0, "", "Flag Ship Option");

	$form->add_section("Distribution Channel");
	$form->add_list("posaddress_distribution_channel", "Distribution Channel",$sql_distribution_channels);

	$form->add_section("Dates");
	$form->add_edit("posaddress_store_openingdate", "Opening Date", 0, "", TYPE_DATE);
	
	
	

	$form->add_edit("posaddress_store_planned_closingdate", "Planned Closing Date", 0, "", TYPE_DATE);

	if($pos["posaddress_store_planned_closingdate"] != NULL and $pos["posaddress_store_planned_closingdate"] != '0000-00-00')
	{
		$form->add_edit("planned_closing_change_comment", "Reason for changing planned closing date*", 0);
	}
	else
	{
		$form->add_hidden("planned_closing_change_comment");
	}
	
	$form->add_edit("posaddress_store_closingdate", "Closing Date", 0, "", TYPE_DATE);

	$form->add_section("Surfaces");
	$form->add_label("posaddress_overall_sqms", "Retailers overall sales surface");
	$form->add_label("posaddress_dedicated_sqms_wall", "Sales wall surface dedicated to Tissot");
	$form->add_label("posaddress_dedicated_sqms_free", "Sales freestanding surface dedicated to Tissot");

	
	$form->add_hidden("country", param("country"));
	$form->add_button(FORM_BUTTON_SAVE, "Save");
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	if($pos["posaddress_store_postype"] != 4)
	{
		$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text");
		$form->add_lookup("posaddress_store_furniture", "Product Line", "product_lines", "product_line_name");
		$form->add_lookup("posaddress_store_furniture_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name");
		$form->add_lookup("posaddress_store_postype", "POS Type", "postypes", "postype_name");
		$form->add_lookup("posaddress_store_subclass", "POS Type Subclass", "possubclasses", "possubclass_name");

		$form->add_checkbox("posaddress_is_flagship", "the POS is a flag ship POS", 0, DISABLED, "Flag Ship Option");
		$form->add_checkbox("posaddress_local_production", "project is locally realized (local production)", 0, DISABLED, "Local Production");
		
		$form->add_lookup("posaddress_project_type_subclass_id", "Project Type Subclass", "project_type_subclasses", "project_type_subclass_name");
		$form->add_checkbox("posaddress_uses_icedunes_visuals", "the POS uses Visuals", 0, DISABLED, "Visuals");
		


		$form->add_section("Distribution Channel");

		$sql = "select concat(mps_distchannel_group_name, ' - ', mps_distchannel_name, ' - ',mps_distchannel_code) as dchannel " .
		       "from mps_distchannels ". 
			   "left join mps_distchannel_groups on mps_distchannel_groups.mps_distchannel_group_id = mps_distchannels.mps_distchannel_group_id " .
			   " where mps_distchannel_id = " . dbquote($pos["posaddress_distribution_channel"]);

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			
			$form->add_label("dchannel", "Distribution Channel", 0, $row["dchannel"]);
		}
		else
		{
			$form->add_label("dchannel", "Distribution Channel", 0, "");
		}
		
		
		$form->add_section("Dates");
		$form->add_label("posaddress_store_openingdate", "Opening Date");
		$form->add_label("posaddress_store_planned_closingdate", "Planned Closing Date");
		$form->add_hidden("planned_closing_change_comment");
		
		

		$form->add_section("Surfaces");
		//$form->add_label("posaddress_store_grosssurface", "Gross Surface in sqms");
		$form->add_label("posaddress_store_totalsurface", "Total Surface in sqms");
		$form->add_label("posaddress_store_retailarea", "Sales Surface in sqms");
		$form->add_label("posaddress_store_backoffice", "Other Surface in sqms");
		$form->add_label("posaddress_store_numfloors", "Number of Floors");
		$form->add_label("posaddress_store_floorsurface1", "Floor 1: Surface  in sqms");
		$form->add_label("posaddress_store_floorsurface2", "Floor 2: Surface  in sqms");
		$form->add_label("posaddress_store_floorsurface3", "Floor 3: Surface  in sqms");

		$form->add_section("Staff");
		$form->add_label("posaddress_store_headcounts", "Headcounts");
		$form->add_label("posaddress_store_fulltimeeqs", "Full Time Equivalents");

	}
	else
	{
		$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text");
		$form->add_lookup("posaddress_store_postype", "POS Type", "postypes", "postype_name");
		
		
		$form->add_section("Distribution Channel");

		$sql = "select concat(mps_distchannel_group_name, ' - ', mps_distchannel_name, ' - ',mps_distchannel_code) as dchannel " .
		       "from mps_distchannels ". 
			   "left join mps_distchannel_groups on mps_distchannel_groups.mps_distchannel_group_id = mps_distchannels.mps_distchannel_group_id " .
			   " where mps_distchannel_id = " . dbquote($pos["posaddress_distribution_channel"]);

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$form->add_label("dchannel", "Distribution Channel", 0, $row["dchannel"]);
		}
		else
		{
			$form->add_label("dchannel", "Distribution Channel", 0, "");
		}

		$form->add_section("Distribution Channel");

		$sql = "select concat(mps_distchannel_group_name, ' - ', mps_distchannel_name, ' - ',mps_distchannel_code) as dchannel " .
		       "from mps_distchannels ". 
			   "left join mps_distchannel_groups on mps_distchannel_groups.mps_distchannel_group_id = mps_distchannels.mps_distchannel_group_id " .
			   " where mps_distchannel_id = " . dbquote($pos["posaddress_distribution_channel"]);

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$form->add_label("dchannel", "Distribution Channel", 0, $row["dchannel"]);
		}
		else
		{
			$form->add_label("dchannel", "Distribution Channel", 0, "");
		}

		$form->add_section("Dates");
		$form->add_label("posaddress_store_openingdate", "Opening Date");
		$form->add_label("posaddress_store_planned_closingdate", "Planned Closing Date");
		$form->add_hidden("planned_closing_change_comment");
		$form->add_label("posaddress_store_closingdate", "Closing Date");

		$form->add_section("Surfaces");
		$form->add_label("posaddress_overall_sqms", "Retailers overall sales surface");
		$form->add_label("posaddress_dedicated_sqms_wall", "Sales wall surface dedicated to Tissot");
		$form->add_label("posaddress_dedicated_sqms_free", "Sales freestanding surface dedicated to Tissot");


	}

	$form->add_hidden("country", param("country"));
}

$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("psc", param("ltf"));
$form->add_hidden("let", param("let"));
$form->add_hidden("ostate", param("ostate"));
$form->add_hidden("province",  param("province"));
$form->add_button("back", "Back to POS List");


// Populate form and process button clicks

$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("posindex.php?country=" . param("country"). '&ltf=' . param("ltf") . '&psc=' . param("psc") . '&ostate=' . param("ostate"). '&province=' . param("province") . '&let=' . param("let"));
}
elseif($form->button(FORM_BUTTON_SAVE))
{
	
	//check if POS Closing date is valid (max 2 days in the future)
	$error = 0;

	if($form->value("posaddress_store_closingdate"))
	{
		$closing_date = from_system_date($form->value("posaddress_store_closingdate"));
		$opening_date = from_system_date($form->value("posaddress_store_openingdate"));

		
		$aftertomorrow = date('Y-m-d', strtotime(date("Y-m-d"). ' + 2 days'));

		if($closing_date > $aftertomorrow)
		{
			$error = 1;
		}
		
		if($closing_date < $opening_date)
		{
			$error = 2;
		}
	}

	if($form->value("posaddress_store_planned_closingdate"))
	{
		$planned_closing_date = from_system_date($form->value("posaddress_store_planned_closingdate"));
		$opening_date = from_system_date($form->value("posaddress_store_openingdate"));
		if($planned_closing_date < $opening_date)
		{
			$error = 3;
		}


		
		if($error == 0 
			and $form->value("old_store_planned_closingdate") 
			and $form->value("old_store_planned_closingdate") != $form->value("posaddress_store_planned_closingdate")
			and !$form->value("planned_closing_change_comment"))
		{

			$error = 4;
		}
	}


	
	
	if($error == 0 and $form->validate())
	{
		//$form->save();

		//update latest project with closing date and sqms
		//if($form->value("posaddress_store_closingdate"))
		//{
			$sql =	"select posorder_order, posorder_id " .  
					"from posorders " .
					"left join orders on order_id = posorder_order " . 
					"where (order_actual_order_state_code <> '900' or order_actual_order_state_code is null) " . 
					" and posorder_type = 1 and posorder_posaddress = " . $pos["posaddress_id"] .
					" and posorder_opening_date is not null and posorder_opening_date <> '0000-00-00' " . 
				    " and posorder_project_kind <> 8 " . 
					" order by posorder_year DESC, posorder_opening_date DESC";

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{

				if($row["posorder_order"] > 0)
				{
					
					if($form->value("posaddress_store_closingdate"))
					{
						$sql = "update projects " . 
							   "set project_state = 5, " . 
							   " project_shop_closingdate = " . dbquote(from_system_date($form->value("posaddress_store_closingdate"))) . 
							   ", project_planned_closing_date = " . dbquote(from_system_date($form->value("posaddress_store_planned_closingdate"))) .
							   " where project_order = " . $row["posorder_order"];
					}
					else
					{
						
						$sql = "update projects " . 
							   "set project_state = 4, project_shop_closingdate = " . dbquote(from_system_date($form->value("posaddress_store_closingdate"))) . 
							   ", project_planned_closing_date = " . dbquote(from_system_date($form->value("posaddress_store_planned_closingdate"))) .
							   " where project_order = " . $row["posorder_order"];
					}					
					
					$result = mysql_query($sql) or dberror($sql);

					/*
					$sql = "update project_costs set " . 
						   "project_cost_grosssqms = " . dbquote($form->value("posaddress_store_grosssurface")) . ", " .
						   "project_cost_totalsqms = " . dbquote($form->value("posaddress_store_totalsurface")) . ", " .
						   "project_cost_sqms = " . dbquote($form->value("posaddress_store_retailarea")) . ", " .
						   "project_cost_backofficesqms = " . dbquote($form->value("posaddress_store_backoffice")) . ", " .
						   "project_cost_floorsurface1 = " . dbquote($form->value("posaddress_store_floorsurface1")) . ", " .
						   "project_cost_floorsurface2 = " . dbquote($form->value("posaddress_store_floorsurface2")) . ", " .
						   "project_cost_floorsurface3 = " . dbquote($form->value("posaddress_store_floorsurface3")) . ", " .
						   "project_cost_numfloors = " . dbquote($form->value("posaddress_store_numfloors")) . " " .
						   " where project_cost_order = " . $row["posorder_order"];

					$result = mysql_query($sql) or dberror($sql);
				    */

					$sql = "update project_costs set " . 
						   "project_cost_totalsqms = " . dbquote($form->value("posaddress_store_totalsurface")) . ", " .
						   "project_cost_sqms = " . dbquote($form->value("posaddress_store_retailarea")) . ", " .
						   "project_cost_backofficesqms = " . dbquote($form->value("posaddress_store_backoffice")) . ", " .
						   "project_cost_floorsurface1 = " . dbquote($form->value("posaddress_store_floorsurface1")) . ", " .
						   "project_cost_floorsurface2 = " . dbquote($form->value("posaddress_store_floorsurface2")) . ", " .
						   "project_cost_floorsurface3 = " . dbquote($form->value("posaddress_store_floorsurface3")) . ", " .
						   "project_cost_numfloors = " . dbquote($form->value("posaddress_store_numfloors")) . " " .
						   " where project_cost_order = " . $row["posorder_order"];

					$result = mysql_query($sql) or dberror($sql);


				}
				else // fake project
				{
					$sql = "update posorders  " . 
						   "set posorder_closing_date  = " . dbquote(from_system_date($form->value("posaddress_store_closingdate"))) . 
						   " where posorder_id = " . $row["posorder_id"];
					$result = mysql_query($sql) or dberror($sql);
				}
			}
			
		//}
	
		
		if(param("pos_id"))
		{
			update_store_locator(param("pos_id"));
			mysql_select_db(RETAILNET_DB, $db);

			set_owner_company_state(param("pos_id"), $form->value("posaddress_store_closingdate"));
		}


		//remove mail_alert in case store_planned_closingdate is greater than old_store_planned_closingdate
		// and old_store_planned_closingdate was not empty

		if($old_store_planned_closingdate and $old_store_planned_closingdate < from_system_date($form->value("posaddress_store_planned_closingdate")))
		{
			
			//get latest order of type temporyry POS
			$sql = "select posorder_order from posorders " . 
				   " where posorder_type = 1 " . 
				   " and posorder_posaddress = " . dbquote($pos["posaddress_id"]) . 
				   " and posorder_subclass = 27 " . 
				   " and (posorder_closing_date is null or posorder_closing_date = '0000-00-00')";

			$res = mysql_query($sql) or dberror($sql);

			if ($row = mysql_fetch_assoc($res))
			{
				   
				$sql = "delete from mail_alerts " . 
					   " where mail_alert_type = 25 " . 
					   " and mail_alert_order = " . dbquote($row["posorder_order"]);
				
				$res = mysql_query($sql) or dberror($sql);
			}
		}



		//update postracking
		if($old_store_closingdate != from_system_date($form->value("posaddress_store_closingdate")))
		{
		
			//project tracking
			$field = "posaddress_store_closingdate";
			$sql = "Insert into postracking (" . 
				   "postracking_user_id, postracking_pos_id, postracking_field, postracking_oldvalue, postracking_newvalue, postracking_comment, postracking_time) VALUES (" . 
				   user_id() . ", " . 
				   dbquote($pos["posaddress_id"]) . ", " . 
				   dbquote($field) . ", " . 
				   dbquote($old_store_closingdate ) . ", " . 
				   dbquote(from_system_date($form->value('posaddress_store_closingdate'))) . ", " . 
				   dbquote('changed by user') . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);

			//send mail alerts

			$mail_template_id = 19;
			if($old_store_closingdate == NULL 
				or $old_store_closingdate == '0000-00-00'
				or $old_store_closingdate == '')
			{
				$mail_template_id = 21;
			}
			$actionMail = new ActionMail($mail_template_id);
			
			$actionMail->setParam('id', $pos["posaddress_id"]);
			$actionMail->setParam('section', 'posaddress_store_planned_closingdate');
			
			$model = new Model(Connector::DB_CORE);
			$dataloader = $model->query("
				SELECT *
				FROM posaddresses 
				LEFT JOIN countries ON country_id = posaddress_country
				LEFT JOIN posowner_types ON posowner_type_id = posaddress_ownertype
				LEFT JOIN postypes ON postype_id = posaddress_store_postype
				WHERE posaddress_id = " . $pos["posaddress_id"] . "
			")->fetch();
			
			// link
			$protocol = Settings::init()->http_protocol.'://';
			$server = $_SERVER['SERVER_NAME'];
			$dataloader['link'] = $protocol.$server."/pos/posindex_pos.php?id=" . $pos["posaddress_id"];
			$dataloader['closing_date'] = $form->value("posaddress_store_closingdate");
			

			$actionMail->setDataloader($dataloader);
			
			// send mails
			

			if($senmail_activated == true)
			{
				$actionMail->send();
			}
		}
		
		if($old_store_planned_closingdate != from_system_date($form->value("posaddress_store_planned_closingdate")))
		{
		
			
			//project tracking
			$field = "posaddress_store_planned_closingdate";
			$sql = "Insert into postracking (" . 
				   "postracking_user_id, postracking_pos_id, postracking_field, postracking_oldvalue, postracking_newvalue, postracking_comment, postracking_time) VALUES (" . 
				   user_id() . ", " . 
				   dbquote($pos["posaddress_id"]) . ", " . 
				   dbquote($field) . ", " . 
				   dbquote($old_store_planned_closingdate ) . ", " . 
				   dbquote(from_system_date($form->value('posaddress_store_planned_closingdate'))) . ", " . 
				   dbquote($form->value("planned_closing_change_comment")) . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
			$result = mysql_query($sql) or dberror($sql);

			
			//send mail alerts
			$mail_template_id = 20;
			if($old_store_planned_closingdate == NULL 
				or $old_store_planned_closingdate == '0000-00-00'
				or $old_store_planned_closingdate == '')
			{
				$mail_template_id = 22;
			}


			$actionMail = new ActionMail($mail_template_id);
			
			$actionMail->setParam('id', $pos["posaddress_id"]);
			$actionMail->setParam('section', 'posaddress_store_planned_closingdate');

			$model = new Model(Connector::DB_CORE);
			$dataloader = $model->query("
				SELECT *
				FROM posaddresses 
				LEFT JOIN countries ON country_id = posaddress_country
				LEFT JOIN posowner_types ON posowner_type_id = posaddress_ownertype
				LEFT JOIN postypes ON postype_id = posaddress_store_postype
				WHERE posaddress_id = " . $pos["posaddress_id"] . "
			")->fetch();
			
			// link
			$protocol = Settings::init()->http_protocol.'://';
			$server = $_SERVER['SERVER_NAME'];
			$dataloader['link'] = $protocol.$server."/pos/posindex_pos.php?id=" . $pos["posaddress_id"];
			$dataloader['planned_closing_date'] = $form->value("posaddress_store_planned_closingdate");
			$dataloader['reason'] = $form->value("planned_closing_change_comment");
			

			$actionMail->setDataloader($dataloader);
			
			// send mails
			if($senmail_activated == true)
			{
				$actionMail->send();
			}
		}



		//send mail alerts for clsing of a Corporate POS
		if(param("posaddress_store_closingdate") 
			and $old_store_closingdate == ''
			and $pos["posaddress_ownertype"] == 1)
		{
				$dchannel = 'not available';
				$sql = "select concat(mps_distchannel_name, ' - ', mps_distchannel_code) as dchannel 
					   from mps_distchannels 
					   where mps_distchannel_id = " . dbquote($pos['posaddress_distribution_channel']);


				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$dchannel = $row["dchannel"];
				}
				
				$Mail = new ActionMail('info.new.closings.corporate.pos');

				$data = array(
					'country' => $pos['country_name'],
					'pos_name' => $pos['posaddress_name'],
					'closing_date' => param("posaddress_store_closingdate"),
					'eprnr' => $pos['posaddress_eprepnr'] ? $pos['posaddress_eprepnr'] : "not available",
					'dchannel' => $dchannel,
					'sap_nr' => $pos['posaddress_sapnumber'] ? $pos['posaddress_sapnumber'] : 'not available',
					'sap_shipto_nr' => $pos_data['posaddress_sap_shipto_number'] ? $pos['posaddress_sap_shipto_number'] : 'not available'
				);
				
				
				$Mail->setDataloader($data);
				
				if($senmail_activated == true)
				{
					$Mail->send(true);

					$recipients = $Mail->getRecipients();
					$ccrecipients = $Mail->getCCRecipients();


				
					$rcpts = array();
					$mail_text = '';
					$subject = '';
					foreach($recipients as $user_id=>$recipient_data)
					{
						$sql = "select user_email from users where user_id = " . dbquote($user_id);
						$res = mysql_query($sql) or dberror($sql);
						if($row = mysql_fetch_assoc($res)) {
							$rcpts[] = $row["user_email"];
						}
						$mail_text = $recipient_data->getBody(true);
						$subject = $recipient_data->getSubject(true);
					}

					foreach($ccrecipients as $key=>$email)
					{
						$rcpts[] = $email;
					}

					//update mail history
					if($mail_text and count($rcpts) > 0) {
						$pos_mail_fields = array();
						$pos_mail_values = array();

						$pos_mail_fields[] = "posmail_posaddress_id";
						$pos_mail_values[] = dbquote($pos["posaddress_id"]);;

						$pos_mail_fields[] = "posmail_mail_template_id";
						$pos_mail_values[] = 153;;

						$pos_mail_fields[] = "posmail_sender_email";
						$pos_mail_values[] = dbquote($Mail->getSender()->email);;


						$pos_mail_fields[] = "posmail_recipeint_email";
						$pos_mail_values[] = dbquote(implode(';', $rcpts));

						$pos_mail_fields[] = "posmail_subject";
						$pos_mail_values[] = dbquote($subject);

						$pos_mail_fields[] = "posmail_text";
						$pos_mail_values[] = dbquote($mail_text);

						$pos_mail_fields[] = "date_created";
						$pos_mail_values[] = "current_timestamp";

						$pos_mail_fields[] = "user_created";
						$pos_mail_values[] = dbquote($_SESSION["user_login"]);


						$sql = "insert into posmails (" . join(", ", $pos_mail_fields) . ") values (" . join(", ", $pos_mail_values) . ")";
						mysql_query($sql) or dberror($sql);

					}
	
				}
		}


	//set owner company to inacitve or active

		//redirect("posindex.php?country=" . param("country"). '&ltf=' . param("ltf") . '&ostate=' . param("ostate"). '&province=' . param("province") . '&let=' . param("let"));
	}
	elseif($error == 1)
	{
		$form->error("The POS closing date must be a date in the past or can only be at maximum two days in the future!");
	}
	elseif($error == 2)
	{
		$form->error("The POS closing date can not be a date in the past of the POS opening date!");
	}
	elseif($error == 3)
	{
		$form->error("The planned POS closing date can not be a date in the past of the POS opening date!");
	}
	elseif($error == 4)
	{
		$form->error("Please indicate the reason for changing the planned closing date!");
	}
}


// Render page
$poslocation = get_poslocation(id(), "posaddresses");

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title(id() ? "POS Details: " . $poslocation["posaddress_name"] : "Add POS Location");

require_once("include/tabs.php");

$form->render();

?>
<!--
<div id="grosssqm" style="display:none;">
    Please indicate the Gross Surface in units of measurement <strong>rented by Tissot</strong><br />incl. corridors and other public areas, back office, toilets etc.
</div>
-->
<div id="totalsqm" style="display:none;">
    Please indicate the total surface in units of measurement <strong>occupied by Tissot</strong><br />incl. back office, toilets etc.
</div> 
<div id="retailsqm" style="display:none;">
    Please indicate the surface in units of measurement <strong>occupied by Tissot</strong><br />used for sales purposes (without back office, toiletsetc.).
</div> 
<div id="backofficesqm" style="display:none;">
    Please indicate the total other surface in units of measurement <strong>occupied by Tissot</strong><br />like back office, toilets, stock areas, etc.
</div> 

<?php


$page->footer();

?>