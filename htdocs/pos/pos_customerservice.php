<?php
/********************************************************************

    pos_customerservice

    Lists of addresses (POS)

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-08-09
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-08-09
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
require_once "../shared/access_filters.php";


if(!has_access("can_edit_pos_opening_hours") and !has_access("can_view_pos_opening_hours"))
{
	redirect("/pos");
}

$user = get_user(user_id());

$postype_filter = array();
$postype_filter["all"] = "All";
$sql = "select * from postypes order by postype_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$postype_filter[$row["postype_id"]] = $row["postype_name"];
}

$legalype_filter = array();
$legalype_filter["all"] = "All";
$sql = "select * from project_costtypes where project_costtype_id IN (1, 2, 6) order by project_costtype_text ";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$legalype_filter[$row["project_costtype_id"]] = $row["project_costtype_text"];
}


$preselect_filter = "";
if(param("country"))
{
	$preselect_filter = "posaddress_country = " . param("country");
	register_param("country", param("country"));
}
else
{
	redirect("welcome.php");
}

if(param("province") and $preselect_filter)
{
	$preselect_filter .= " and place_province = " . param("province");
	register_param("province", param("province"));
}
elseif(param("province"))
{
	$preselect_filter = "place_province = " . param("province");
	register_param("province", param("province"));
}


$preselect_filter .= " and (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null) ";



//get customer services
$customer_services = array();
$checkboxlist = "";
$sql_o = "select customerservice_id, customerservice_text " . 
		 "from customerservices " . 
		 "order by customerservice_text";

$res_o = mysql_query($sql_o) or dberror($sql_o);

while($row_o = mysql_fetch_assoc($res_o))
{
	$customer_services[$row_o["customerservice_id"]] = $row_o["customerservice_text"];
	$checkboxlist .= "<input type=\"checkbox\" name=\"cs@@_" . $row_o["customerservice_id"] . "\" id=\"cs@@_" . $row_o["customerservice_id"] . "\" value=\"1\"" . " /> " . $row_o["customerservice_text"] . "<br />";

	
}
$checkboxlist .= '<br />';

if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{

}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	if(!param("country"))
	{
		redirect("welcome.php");
	}

	$country_filter = "";
	$tmp_country = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp_country[] = $row["country_access_country"];
	}

	
	$clients_access_filter = get_users_regional_access_to_poslocations(user_id());

	
	if($clients_access_filter)
	{
		if($preselect_filter)
		{
			$preselect_filter .= " and (posaddress_client_id = " . $user["address"] . " or " . $clients_access_filter . ") ";
		}
		else
		{
			$preselect_filter = "  (posaddress_client_id = " . $user["address"] . " or " . $clients_access_filter . ") ";;
		}

		if(count($tmp_country) > 0)
		{
			if($preselect_filter)
			{
				$preselect_filter .= " or posaddress_country in (" . implode(',', $tmp_country) . ") ";
			}
			else
			{
				$preselect_filter = "  posaddress_country in (" . implode(',', $tmp_country) . ") ";
			}
		}
	}
	elseif(count($tmp_country) > 0 and !param("country"))
	{
		if($preselect_filter)
		{
			$preselect_filter .= " or posaddress_country in (" . implode(',', $tmp_country) . ") ";
		}
		else
		{
			$preselect_filter = "  posaddress_country in (" . implode(',', $tmp) . ") ";
		}
	}
	elseif(param("country"))
	{
		if($preselect_filter)
		{
			$preselect_filter .= " and posaddress_country = " . param("country");
		}
		else
		{
			$preselect_filter = " posaddress_country = " . param("country");
		}
	}
	else
	{
		if($preselect_filter)
		{
			$preselect_filter .= " and posaddress_client_id = " . $user["address"];
		}
		else
		{
			$preselect_filter = "  posaddress_client_id = " . $user["address"];
		}
	}


	if(has_access("has_access_to_all_travalling_retail_data"))
	{

		if(count($tmp_country) > 0 and in_array(param("country"), $tmp_country))
		{

		}
		else
		{
			$tmp = get_pos_access_for_travelling_retail_pos($preselect_filter);
			if(count($tmp) > 0)
			{
				if($preselect_filter)
				{
					$preselect_filter = "(" . $preselect_filter . ") and (posaddress_id in (" . implode(',', $tmp) . ") or posaddress_client_id = " . $user["address"] . ") ";
				}
				else
				{
					$preselect_filter = " posaddress_id in (" . implode(',', $tmp) . ") or posaddress_client_id = " . $user["address"];
				}
			}
			else
			{
				if($preselect_filter)
				{
					$preselect_filter = "(" . $preselect_filter . ") and (posaddress_client_id = " . $user["address"] . ") ";
				}
				else
				{
					$preselect_filter = " posaddress_client_id = " . $user["address"];
				}
			}
		}
	}
	else
	{
		if($preselect_filter)
		{
			$preselect_filter .= " and posaddress_client_id = " . $user["address"];
		}
		else
		{
			$preselect_filter = " posaddress_client_id = " . $user["address"];
		}
	}
}

if(!has_access("can_view_his_posindex") 
       and !has_access("can_edit_his_posindex") 
	   and !has_access("can_view_posindex") 
	   and !has_access("can_edit_posindex"))
{
	redirect("welcome.php");
}



//compose list

$sql = "select posaddress_id, if(posaddress_name <> '', posaddress_name, 'n.a.') as posname, " .
       "posaddress_address, posaddress_address2, posaddress_zip, " .
       "    posaddress_place, country_name, project_costtype_text, postype_name, " .
	   "posaddress_google_precision, posaddress_store_closingdate, province_canton, posaddress_country " . 
       "from posaddresses " .
	   "left join places on place_id = posaddress_place_id " .
	   "left join provinces on province_id = place_province " .
	   "left join countries on posaddress_country = country_id " . 
	   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
	   "left join postypes on postype_id = posaddress_store_postype ";

if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{
}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	
	//$preselect_filter = "(postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) and " . $preselect_filter;
}






if(param('ptf')) 
{
	if($preselect_filter) {
		
		if(param('ptf') == "all") {
		}
		else
		{
			$preselect_filter .= " and posaddress_store_postype = " . param('ptf');
		}
	}
	else
	{
		if(param('ptf') == "all") {
		}
		else
		{
			$preselect_filter = "posaddress_store_postype = " . param('ptf');
		}
	}
}
else
{
	param('ptf', 'all');
}


if(param('ltf')) 
{
	if($preselect_filter) {
		
		if(param('ltf') == "all") {
		}
		else
		{
			$preselect_filter .= " and posaddress_ownertype = " . param('ltf');
		}
	}
	else
	{
		if(param('ltf') == "all") {
		}
		else
		{
			$preselect_filter = "posaddress_ownertype = " . param('ltf');
		}
	}
}
else
{
	param('ltf', 'all');
}




//get image columns
$onweb = array();
$opening_hours_indicated = array();
$selected_pos_locations = array();

$sql_u = "select DISTINCT posaddress_id, posaddress_country, posaddress_google_precision, posaddress_store_closingdate,  " .
		 "posaddress_export_to_web, place_name, posaddress_address, posaddress_address2, posaddress_zip, posaddress_google_lat, posaddress_google_long,  " . 
		 "posclosingassessment_id, posclosingassessment_filesigned " .
         "from posaddresses " . 
		 "left join places on place_id = posaddress_place_id " .
		 "left join provinces on province_id = place_province " .
		 "left join posclosingassessments on posclosingassessment_posaddress_id = posaddress_id ";

if($preselect_filter)
{
	$sql_u = $sql_u . " where " . $preselect_filter;
}

$res = mysql_query($sql_u) or dberror($sql_u);
while($row = mysql_fetch_assoc($res))
{
	//$result = update_posdata_from_posorders($row["posaddress_id"]);
	
	$selected_pos_locations[] = $row["posaddress_id"];
	
	if($row["posaddress_export_to_web"] == 1 and ($row["posaddress_store_closingdate"] == NULL or $row["posaddress_store_closingdate"] == "0000-00-00"))
	{
		$onweb[$row["posaddress_id"]] = "<img src=\"/pictures/bullet_ball_glass_green.gif\" border='0'/>";
	}

	$cb = str_replace("@@", $row["posaddress_id"], $checkboxlist);
	//get customer services of the POS Location
	$sql_c = "select posaddress_customerservice_customerservice_id " . 
		     "from posaddress_customerservices " . 
		     "where posaddress_customerservice_posaddress_id = " . $row["posaddress_id"];
	
	$res_c = mysql_query($sql_c) or dberror($sql_c);
	while($row_c = mysql_fetch_assoc($res_c))
	{
		$id = 'id="cs' . $row["posaddress_id"] . '_' . $row_c["posaddress_customerservice_customerservice_id"] . '"';
		$tmp = 'id="cs' . $row["posaddress_id"] . '_' . $row_c["posaddress_customerservice_customerservice_id"] . '" checked="checked" ';
		
		$cb = str_replace($id, $tmp , $cb);

		
	}
	
	$services[$row["posaddress_id"]] = $cb;
}

/********************************************************************
    Create Form
*********************************************************************/

$form = new Form("posaddresses", "posaddress");

$form->add_section("Bulk Selection");

$res_o = mysql_query($sql_o) or dberror($sql_o);
$i = 0;
while($row_o = mysql_fetch_assoc($res_o))
{
	$i = $row_o["customerservice_id"];
	$tmp = '<span id="se' . $i . '"><a href="javascript:void(0);" onclick="javascript:select_all(1,' . $i . ', ' . $i . ');">Select all POS locations</a></span>';
	
	$form->add_label("s" . $row_o["customerservice_id"], $row_o["customerservice_text"], RENDER_HTML, $tmp);
	
}
$i++;

$tmp = '<span id="se0"><a href="javascript:void(0);" onclick="javascript:select_all(1,0,0);">Select all Services for all POS locations</a></span>';
$form->add_label("all", "All Services", RENDER_HTML, $tmp);

$form->add_section(" ");
$form->add_section("List Filter Selection");





$list = new ListView($sql);

$list->set_entity("posaddresses");
$list->set_filter($preselect_filter);
$list->set_order("country_name, posaddress_place, posaddress_name");

$list->add_listfilters("ltf", "Legal Type", 'select', $legalype_filter, param('ltf'));
$list->add_listfilters("ptf", "POS Type", 'select', $postype_filter, param('ptf'));

$list->add_hidden("country", param("country"));
$list->add_hidden("province", param("province"));


$list->add_column("country_name", "Country", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
$list->add_column("province_canton", "Province", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_place", "City", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posname", "POS Name", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("posaddress_address", "Street", "", LIST_FILTER_FREE,'', COLUMN_NO_WRAP);
$list->add_column("postype_name", "POS Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);
//$list->add_column("project_costtype_text", "Legal Type", "", LIST_FILTER_LIST ,'', COLUMN_NO_WRAP);

$list->add_text_column("web", "Web", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $onweb);
$list->add_text_column("customer_services", "Services", COLUMN_UNDERSTAND_HTML, $services);


$list->add_button("save", "Save");
$list->add_button(FORM_BUTTON_BACK, "Back");
$list->populate();
$list->process();

if($list->button("save"))
{
	
	$sql = "delete from posaddress_customerservices where posaddress_customerservice_posaddress_id IN (" . implode(', ', $selected_pos_locations) . ")";

	$result = mysql_query($sql) or dberror($sql);


	$res = mysql_query($sql_u) or dberror($sql_u);
	while($row = mysql_fetch_assoc($res))
	{
		$pos_customer_services = array();
		
		foreach($customer_services as $key=>$name)
		{
			$filed_name = 	'cs' . $row["posaddress_id"]. '_' . $key;

			if(array_key_exists($filed_name, $_POST))
			{
				
				$sql = "Insert into posaddress_customerservices (" . 
					   "posaddress_customerservice_posaddress_id, " . 
					   "posaddress_customerservice_customerservice_id , " . 
					   "user_created, date_created) VALUES (" . 
					   $row["posaddress_id"] . ", " . 
					   $key . ", " . 
					   dbquote(user_login()) . ", " . 
					   dbquote(date("Y-m-d H:i:s")) . ")";

				$result = mysql_query($sql) or dberror($sql);

				$pos_customer_services[] = $key;
			}
		}

		update_store_locator($row["posaddress_id"]);
	}

	$link = "pos_customerservice.php?country=" . param("country") . "&province=" . param("province"). "&ptf=" . param("ptf"). "&ltf=" . param("ltf");
	redirect($link);
	

}



$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: Customer Services");
$form->render();
$list->render();

?>

<script type="text/javascript">
  function select_all(status, id, range) {
	
	$("#main input").each( function(i,e) {
		
		
		if($(e).is(':checkbox'))
		{
			if(id == 0)
			{
				$(this).attr("checked",status);
			}
			else
			{
				var tmp = $(this).attr('id');
				if(tmp.substring(0,2) == 'cs')
				{
					strs = tmp.split("_");
					sid = strs[1];
					
					if(sid == id)
					{
						if(status == 1)
						{
							$(this).attr("checked",true);
						}
						else
						{
							$(this).attr("checked",false);
						}
					}
				}
			
			}
		}
	})

	if(range == 0 && status == 1)
	{
		tmp = '<span id="se' + id + '"><a href="javascript:void(0);" onclick="javascript:select_all(0,0, 0);">Deselect all Services for all POS locations</a></span>';
	}
	else if(range == 0 && status == 0)
	{
		tmp = '<span id="se' + id + '"><a href="javascript:void(0);" onclick="javascript:select_all(1,0, 0);">Select all Services for all POS locations</a></span>';
	}
	else if(range > 0 && status == 1)
	{
		tmp = '<span id="se' + id + '"><a href="javascript:void(0);" onclick="javascript:select_all(0,' + id + ', ' + id + ');">Deselect all POS locations</a></span>';
	}
	else if(range > 0 && status == 0)
	{
		tmp = '<span id="se' + id + '"><a href="javascript:void(0);" onclick="javascript:select_all(1,' + id + ', ' + id + ');">Select all POS locations</a></span>';
	}
	$("#se" + id).html(tmp);

  }
</script>

<?php
$page->footer();

?>
