<?php
/********************************************************************

    pos_profitability_preselect.php

    Preselection of POS List to enter profitability data

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-10-20
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-10-20
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/access_filters.php";

check_access("has_access_to_pos_profitability");
set_referer("pos_profitability.php");

$postype_filter = array();
$postype_filter["all"] = "All";
$sql = "select * from postypes order by postype_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$postype_filter[$row["postype_id"]] = $row["postype_name"];
}


$states = array();
$states[1] = "Operating POS locations only";
$states[2] = "Closed POS locations only";

//compose list
if(has_access("can_view_posindex") or has_access("can_edit_posindex"))
{
	$sql = "select DISTINCT country_id, country_name " .
		   "from posaddresses " . 
		   "left join countries on posaddress_country = country_id " . 
		   "where country_name is not null " . 
		   "order by country_name";
}
else
{

	$user = get_user(user_id());

	
	$country_filter = "";
	$tmp = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}

	if(count($tmp) > 0) {
		$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
	}

	$country_access_filter =  get_users_regional_access_to_poslocations(user_id());
	if($country_access_filter)
	{
		if($country_filter)
		{
			$country_filter = "(" . $country_filter .  " or " . $country_access_filter . ")";
		}
		else
		{
			$country_filter = $country_access_filter;
		}
	}



	if($country_filter == "")
	{
		$sql = "select DISTINCT country_id, country_name " .
			   "from posaddresses " . 
			   "left join countries on posaddress_country = country_id " .
			   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
			   "left join postypes on postype_id = posaddress_store_postype " .
			   "where country_name is not null " .
			   " and posaddress_client_id = " . $user["address"] . 
			   //" and (postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) " . 
			   " order by country_name";
	}
	else
	{
		$sql = "select DISTINCT country_id, country_name " .
			   "from posaddresses " . 
			   "left join countries on posaddress_country = country_id " .
			   "left join project_costtypes on project_costtype_id = posaddress_ownertype " . 
			   "left join postypes on postype_id = posaddress_store_postype " .
			   "where country_name is not null " .
			   " and " . $country_filter . 
			   //" and (postype_id <> 2 or (postype_id = 2 and project_costtype_id = 1)) " . 
			   " order by country_name";
	}
	


	
}

if(param("country"))
{
	$sql_p = "select province_id, province_canton from provinces  " . 
		     "where province_country = " . dbquote(param("country")) .
		     " order by province_canton";
}

$form = new Form("posaddresses", "posaddress");

$form->add_section("Country Selection");

$form->add_list("country", "Country Selection",$sql, SUBMIT);
if(param("country"))
{
	$form->add_list("province", "Province",$sql_p);
}
else
{
	$form->add_hidden("province");
}

$selected = 'all';
if(param('ltf'))
{
	$selected = param('ltf');
}

$form->add_list("ltf", "POS Type Filter",$postype_filter, 0, $selected);

$form->add_list("states", "Filter", $states, 0, 1);


$form->add_button("show_pos", "Show List");



$form->populate();
$form->process();




if($form->button("show_pos"))
{
	redirect("pos_profitability.php?country=" . $form->value("country") . "&province=" . $form->value("province"). "&ostate=" . $form->value("states") . "&ltf=" . $form->value("ltf"). '&province=' . param("province"));
}

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: POS yearly sellout");
$form->render();
?>

	<script type="text/javascript">
		
		document.onkeydown = process_key;
		
		function process_key(e)
		{
		  if( !e ) 
		  {
			if( window.event ) 
			{
			  e = window.event;
			} 
			else 
			{
			  return;
			}
		  }

		  if(e.keyCode==13)
		  {
			  button('show_pos');
		  }
		}
	</script>

	<?php

$page->footer();

?>
