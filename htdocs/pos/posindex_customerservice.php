<?php
/********************************************************************

    posindex_customerservice.php

    Edit Customer Services.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-08-10
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-08-10
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";


if(!has_access("can_edit_customerservices") and !has_access("can_view_customerservices"))
{
	redirect("welcome.php");
}

$pos = array();
$pos = get_poslocation(param("pos_id"), "posaddresses");


//get customer services
$customer_services = array();
$sql_o = "select customerservice_id, customerservice_text " . 
		 "from customerservices " . 
		 "order by customerservice_text";

$res_o = mysql_query($sql_o) or dberror($sql_o);

while($row_o = mysql_fetch_assoc($res_o))
{
	$customer_services[$row_o["customerservice_id"]] = $row_o["customerservice_text"];
}


//get POS customer services
$pos_services = array();
$sql_o = "select posaddress_customerservice_customerservice_id " . 
		 "from posaddress_customerservices " .
		 "where posaddress_customerservice_posaddress_id = " . param("pos_id");

$res_o = mysql_query($sql_o) or dberror($sql_o);

while($row_o = mysql_fetch_assoc($res_o))
{
	$pos_services[] = $row_o["posaddress_customerservice_customerservice_id"];
}

// Build form

$form = new Form("posclosings", "posclosure");

$form->add_section("Name and address");

require_once("include/poslocation_head.php");

$form->add_hidden("country", param("country"));
$form->add_hidden("pos_id", param("pos_id"));
$form->add_hidden("posclosing_posaddress", param("pos_id"));
$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("psc", param("psc"));
$form->add_hidden("let", param("let"));
$form->add_hidden("province",  param("province"));
$form->add_hidden("ostate", param("ostate"));

$form->add_section("Customer Services");

foreach($customer_services as $key=>$name)
{
	if(in_array($key, $pos_services))
	{
		$form->add_checkbox("cs_" . $key, $name, true);
	}
	else
	{
		$form->add_checkbox("cs_" . $key, $name, false);
	}
}



if(has_access("can_edit_customerservices"))
{
	$form->add_button('save', "Save");
}


$form->add_button("back", "Back to POS List");

// Populate form and process button clicks

$form->populate();
$form->process();

if($form->button("save"))
{

	$sql = "delete from posaddress_customerservices where posaddress_customerservice_posaddress_id = " . param("pos_id");
	$result = mysql_query($sql) or dberror($sql);

	foreach($customer_services as $key=>$name)
	{
		$filed_name = 	'cs_' . $key;

		if($form->value($filed_name))
		{
			$sql = "Insert into posaddress_customerservices (" . 
				   "posaddress_customerservice_posaddress_id, " . 
				   "posaddress_customerservice_customerservice_id , " . 
				   "user_created, date_created) VALUES (" . 
				   param("pos_id") . ", " . 
				   $key . ", " . 
				   dbquote(user_login()) . ", " . 
				   dbquote(date("Y-m-d H:i:s")) . ")";

			$result = mysql_query($sql) or dberror($sql);
		}
	}	

	//update store locator
	update_store_locator(param("pos_id"));
	$form->message("Your data has been saved!");
}
elseif($form->button("back"))
{
	redirect("posindex.php?country=" . param("country") . "&ltf=" . param("ltf") . "&psc=" . param("psc"). '&ostate=' . param("ostate"). '&province=' . param("province") . '&let=' . param("let"));
	redirect($link);
}


// Render page

$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();

$page->title("Edit Customer Services");

require_once("include/tabs.php");

$form->render();
$page->footer();

?>