<?php
/********************************************************************

    posindex_print_xls.php

    Generate Excel-File of POS Addresses

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-06-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-06-09
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_use_posindex");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
require_once "../shared/access_filters.php";

$user = get_user(user_id());


/********************************************************************
    prepare Data Needed
*********************************************************************/

$filter = "";
if(param("country"))
{
	$filter = "posaddress_country = " . param("country");
	register_param("country", param("country"));
}
else
{
	redirect("posindex_preselect.php");
}

if(param("province") and $filter)
{
	if(param('province') == "all") {
	}
	else
	{
		$filter .= " and place_province = " . param("province");
	}
	register_param("province", param("province"));
}
elseif(param("province"))
{
	if(param('province') == "all") {
	}
	else
	{
		$filter = "place_province = " . param("province");
	}
	register_param("province", param("province"));
}


//$state_filter = "";
if(param("ostate") and $filter)
{
	if(param("ostate") == 1) // only operating POS locations
	{
		$filter .= " and (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null) ";
		//$state_filter = "  (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null) ";
	}
	elseif(param("ostate") == 2) // only closed POS locations
	{
		$filter .= " and posaddress_store_closingdate <> '0000-00-00' and posaddress_store_closingdate is not null";
		//$state_filter = "  (posaddress_store_closingdate <> '0000-00-00' and posaddress_store_closingdate is not null)";
	}

	

	register_param("ostate", param("ostate"));
}
elseif(param("ostate"))
{
	if(param("ostate") == 1) // only operating POS locations
	{
		$filter = " (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null) ";
		//$state_filter = " (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null)";
	}
	elseif(param("ostate") == 2) // only closed POS locations
	{
		$filter = " posaddress_store_closingdate <> '0000-00-00' and posaddress_store_closingdate is not null";
		//$state_filter = " (posaddress_store_closingdate <> '0000-00-00' and posaddress_store_closingdate is not null)";
	}


	register_param("ostate", param("ostate"));
}


if(param('ltf')) 
{
	if($filter) {
		
		if(param('ltf') == "all") {
		}
		else
		{
			$filter .= " and posaddress_store_postype = " . param('ltf');
		}
	}
	else
	{
		if(param('ltf') == "all") {
		}
		else
		{
			$filter = "posaddress_store_postype = " . param('ltf');
		}
	}
}
else
{
	if($filter) {
		$filter .= " and posaddress_store_postype <> 4";
	}
	else
	{
		$filter = "posaddress_store_postype <> 4";
	}
}

if(param('let')) 
{
	if($filter) {
		
		if(param('let') == "all") {
		}
		else
		{
			$filter .= " and posaddress_ownertype = " . param('let');
		}
	}
	else
	{
		if(param('let') == "all") {
		}
		else
		{
			$filter = "posaddress_ownertype = " . param('let');
		}
	}
}

$filter1 = $filter;

if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{

}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	if(!param("country"))
	{
		redirect("posindex_preselect.php");
	}

	
	$country_filter = "";
	$tmp_country = array();
	$sql = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp_country[] = $row["country_access_country"];
	}

	
	$clients_access_filter = get_users_regional_access_to_poslocations(user_id());

	
	if($clients_access_filter)
	{
		if($filter)
		{
			$filter .= " and (posaddress_client_id = " . $user["address"] . " or " . $clients_access_filter . ") ";
		}
		else
		{
			$filter = "  (posaddress_client_id = " . $user["address"] . " or " . $clients_access_filter . ") ";;
		}

		if(count($tmp_country) > 0)
		{
			if(param("country") and in_array(param("country"), $tmp_country))
			{
			}
			elseif(param("country"))
			{
				if($filter)
				{
					$filter .= " or (" . $filter1 . " and posaddress_country in (" . param("country") . ")) ";

				}
				else
				{
					$filter = $filter1 . " and posaddress_country in (" . param("country") . ") ";
				}
			}
			else
			{
				if($filter)
				{
					$filter .= " or posaddress_country in (" . implode(',', $tmp_country) . ") ";

				}
				else
				{
					$filter = "  posaddress_country in (" . implode(',', $tmp_country) . ") ";
				}
			}
		}
	}
	elseif(count($tmp_country) > 0 and !param("country"))
	{
		if($filter)
		{
			$filter .= " or posaddress_country in (" . implode(',', $tmp_country) . ") ";
		}
		else
		{
			$filter = "  posaddress_country in (" . implode(',', $tmp) . ") ";
		}
	}
	elseif(param("country"))
	{
		if($filter)
		{
			$filter .= " and posaddress_country = " . param("country");
		}
		else
		{
			$filter = " posaddress_country = " . param("country");
		}
	}
	else
	{
		if($filter)
		{
			$filter .= " and posaddress_client_id = " . $user["address"];
		}
		else
		{
			$filter = "  posaddress_client_id = " . $user["address"];
		}
	}
	
	/*
	if($filter)
	{
		if($state_filter)
		{
			$filter .= " and (" . $state_filter . ") ";
		}
	}
	elseif($state_filter)
	{
		$filter = " (" . $state_filter . ") ";
	}
	*/

}





if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{

}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{
	if(has_access("has_access_to_all_travalling_retail_data"))
	{

		if(count($tmp_country) > 0 and in_array(param("country"), $tmp_country))
		{

		}
		else
		{
			$tmp = get_pos_access_for_travelling_retail_pos($filter);
			if(count($tmp) > 0)
			{
				if($filter)
				{
					$filter = "(" . $filter . ") and (posaddress_id in (" . implode(',', $tmp) . ") or posaddress_client_id = " . $user["address"] . ") ";
				}
				else
				{
					$filter = " posaddress_id in (" . implode(',', $tmp) . ") or posaddress_client_id = " . $user["address"];
				}
			}
			else
			{
				if($filter)
				{
					$filter = "(" . $filter . ") and (posaddress_client_id = " . $user["address"] . ") ";
				}
				else
				{
					$filter = " posaddress_client_id = " . $user["address"];
				}
			}
		}
	}
}






if($filter)
{
	$filter = " where " . $filter;
}
$sql_d = "select * " . 
       "from posaddresses " . 
	   "left join countries on country_id = posaddress_country " . 
	   "left join salesregions on salesregion_id = country_salesregion " .
	   "left join product_lines on product_line_id = posaddress_store_furniture " .
	   "left join postypes on postype_id = posaddress_store_postype " .
	   "left join possubclasses on possubclass_id = posaddress_store_subclass " . 
       "left join project_costtypes on project_costtype_id = posaddress_ownertype " .
	   "left join addresses on address_id = posaddress_franchisee_id " .
	   "left join places on place_id = posaddress_place_id " .
	   "left join provinces on province_id = place_province " .
	   $filter .
	   " order by salesregion_name, country_name, posaddress_place";

/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "pos_addresses_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename);

$xls->setVersion(8);

$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_normal_bold =& $xls->addFormat();
$f_normal_bold->setSize(8);
$f_normal_bold->setAlign('left');
$f_normal_bold->setBorder(1);
$f_normal_bold->setBold();


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('yellow');



//captions
$captions = array();
//$captions[] = "Nr";
//$captions[] = "Geographical Region";
$captions[] = "Country";
$captions[] = "POS Name";
$captions[] = "Province";
$captions[] = "City";
$captions[] = "Zip";
$captions[] = "Street";
$captions[] = "Additional Address";
$captions[] = "Phone";
$captions[] = "Mobile";
$captions[] = "Email";
$captions[] = "POS Type";
$captions[] = "Furniture Type";
$captions[] = "Legal Type";
$captions[] = "Owner Company";
//$captions[] = "POS Type Subclass";
//$captions[] = "Project Type";
//$captions[] = "Project";
$captions[] = "Opening Date";
$captions[] = "Closing Date";

if(param("list") == 2)
{
	//$captions[] = "Gross Surface sqms";
	$captions[] = "Total Surface sqms";
	$captions[] = "Sales Surface sqms";
	$captions[] = "Other Surface sqms";
	$captions[] = "Floors";
	$captions[] = "Floor 1 sqms";
	$captions[] = "Floor 2 sqms";
	$captions[] = "Floor 3 sqms";
	$captions[] = "Headcounts";
	$captions[] = "Fulltime EQs";
}

/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);
$sheet->writeRow(1, 0, $captions, $f_normal_bold);


$row_index = 2;
$cell_index = 0;
$counter = 0;
$col_widths = array();
for($i=0;$i<count($captions);$i++)
{
	$col_widths[$i] = strlen($captions[$i]);
}


$res = mysql_query($sql_d) or dberror($sql_d);
while ($row = mysql_fetch_assoc($res))
{
    //$result = update_posorders_year($row["posaddress_id"]);
	$result = update_posorders_from_projects($row["posaddress_id"]);
	
	//project Type
	$show_project = 1;
	if($filter_pk)
	{
		$sql_p =	"select posorder_project_kind " . 
					"from posorders " .
					"where posorder_type = 1 and posorder_posaddress = " . $row["posaddress_id"] .
					$filter_pk . 
		            " order by posorder_year DESC, posorder_opening_date DESC";

		$res_p = mysql_query($sql_p) or dberror($sql_p);
		if ($row_p = mysql_fetch_assoc($res_p))
		{
			$show_project = 1;
		}
		else
		{
			$show_project = 0;
		}
	}

	if($show_project == 1)
	{
	
		$counter++;
		
		/*
		$sheet->write($row_index, $cell_index, $counter, $f_normal);
		if($col_widths[$cell_index] < strlen($counter))
		{
			$col_widths[$cell_index] = strlen($counter);
		}
		$cell_index++;

		
		$sheet->write($row_index, $cell_index, $row["salesregion_name"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["salesregion_name"]))
		{
			$col_widths[$cell_index] = strlen($row["salesregion_name"]);
		}
		$cell_index++;
		*/

		
		
		$sheet->write($row_index, $cell_index, $row["country_name"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["country_name"]))
		{
			$col_widths[$cell_index] = strlen($row["country_name"]);
		}
		$cell_index++;

		$sheet->write($row_index, $cell_index, $row["posaddress_name"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["posaddress_name"]))
		{
			$col_widths[$cell_index] = strlen($row["posaddress_name"]);
		}
		$cell_index++;

		$sheet->write($row_index, $cell_index, $row["province_canton"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["province_canton"]))
		{
			$col_widths[$cell_index] = strlen($row["province_canton"]);
		}
		$cell_index++;

		$sheet->write($row_index, $cell_index, $row["posaddress_place"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["posaddress_place"]))
		{
			$col_widths[$cell_index] = strlen($row["posaddress_place"]);
		}
		$cell_index++;

		$sheet->write($row_index, $cell_index, $row["posaddress_zip"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["posaddress_zip"]))
		{
			$col_widths[$cell_index] = strlen($row["posaddress_zip"]);
		}
		$cell_index++;

		
		$sheet->write($row_index, $cell_index, $row["posaddress_address"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["posaddress_address"]))
		{
			$col_widths[$cell_index] = strlen($row["posaddress_address"]);
		}
		$cell_index++;

		
		$sheet->write($row_index, $cell_index, $row["posaddress_address2"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["posaddress_address2"]))
		{
			$col_widths[$cell_index] = strlen($row["posaddress_address2"]);
		}
		$cell_index++;

		$sheet->write($row_index, $cell_index, $row["posaddress_phone"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["posaddress_phone"]))
		{
			$col_widths[$cell_index] = strlen($row["posaddress_phone"]);
		}
		$cell_index++;

		$sheet->write($row_index, $cell_index, $row["posaddress_mobile_phone"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["posaddress_mobile_phone"]))
		{
			$col_widths[$cell_index] = strlen($row["posaddress_mobile_phone"]);
		}
		$cell_index++;


		$sheet->write($row_index, $cell_index, $row["posaddress_email"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["posaddress_email"]))
		{
			$col_widths[$cell_index] = strlen($row["posaddress_email"]);
		}
		$cell_index++;


		//Store details

		$sheet->write($row_index, $cell_index, $row["postype_name"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["postype_name"]))
		{
			$col_widths[$cell_index] = strlen($row["postype_name"]);
		}
		$cell_index++;

		$sheet->write($row_index, $cell_index, $row["product_line_name"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["product_line_name"]))
		{
			$col_widths[$cell_index] = strlen($row["product_line_name"]);
		}
		$cell_index++;

		$sheet->write($row_index, $cell_index, $row["project_costtype_text"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["project_costtype_text"]))
		{
			$col_widths[$cell_index] = strlen($row["project_costtype_text"]);
		}
		$cell_index++;


		/*
		

		$sheet->write($row_index, $cell_index, $row["possubclass_name"], $f_normal);
		if($col_widths[$cell_index] < strlen($row["possubclass_name"]))
		{
			$col_widths[$cell_index] = strlen($row["possubclass_name"]);
		}
		$cell_index++;

		


		//project Type and project number
		if($filter_pk)
		{
			$sql_p =	"select posorder_year, posorder_ordernumber, projectkind_name " . 
						"from posorders " .
						"left join projectkinds on projectkind_id = posorder_project_kind ".			
						"where posorder_type = 1 and posorder_posaddress = " . $row["posaddress_id"] .
				         $filter_pk . 
						" order by posorder_year DESC, posorder_opening_date DESC";
		}
		else
		{
			$sql_p =	"select posorder_year, posorder_ordernumber, projectkind_name " . 
						"from posorders " .
						"left join projectkinds on projectkind_id = posorder_project_kind ".			
						"where posorder_type = 1 and posorder_posaddress = " . $row["posaddress_id"] .
						" order by posorder_year DESC, posorder_opening_date DESC";
		}

		$res_p = mysql_query($sql_p) or dberror($sql_p);
		if ($row_p = mysql_fetch_assoc($res_p))
		{
			$sheet->write($row_index, $cell_index, $row_p["projectkind_name"], $f_normal);
			if($col_widths[$cell_index] < strlen($row_p["projectkind_name"]))
			{
				$col_widths[$cell_index] = strlen($row_p["projectkind_name"]);
			}
			$cell_index++;

			$sheet->write($row_index, $cell_index, $row_p["posorder_ordernumber"], $f_normal);
			if($col_widths[$cell_index] < strlen($row_p["posorder_ordernumber"]))
			{
				$col_widths[$cell_index] = strlen($row_p["posorder_ordernumber"]);
			}
			$cell_index++;
		
		}
		else
		{
			$sheet->write($row_index, $cell_index, "", $f_normal);
			$cell_index++;
			$sheet->write($row_index, $cell_index, "", $f_normal);
			$cell_index++;

		}

		*/

		//franchisee
		$franchisee_address = $row["address_company"] . ", " . $row["address_zip"] . " " . $row["address_place"];
		
		$sheet->write($row_index, $cell_index, $franchisee_address, $f_normal);
		if($col_widths[$cell_index] < strlen($franchisee_address))
		{
			$col_widths[$cell_index] = strlen($franchisee_address);
		}
		$cell_index++;


		$sheet->write($row_index, $cell_index, to_system_date($row["posaddress_store_openingdate"]), $f_normal);
		if($col_widths[$cell_index] < strlen($row["posaddress_store_openingdate"]))
		{
			$col_widths[$cell_index] = strlen($row["posaddress_store_openingdate"]);
		}
		$cell_index++;

		$sheet->write($row_index, $cell_index, to_system_date($row["posaddress_store_closingdate"]), $f_normal);
		if($col_widths[$cell_index] < strlen($row["posaddress_store_closingdate"]))
		{
			$col_widths[$cell_index] = strlen($row["posaddress_store_closingdate"]);
		}
		$cell_index++;


		if(param("list") == 2)
		{
			//$sheet->write($row_index, $cell_index, $row["posaddress_store_grosssurface"], $f_number);
			//$cell_index++;
			$sheet->write($row_index, $cell_index, $row["posaddress_store_totalsurface"], $f_number);
			$cell_index++;
			$sheet->write($row_index, $cell_index, $row["posaddress_store_retailarea"], $f_number);
			$cell_index++;
			$sheet->write($row_index, $cell_index, $row["posaddress_store_backoffice"], $f_number);
			$cell_index++;
			$sheet->write($row_index, $cell_index, $row["posaddress_store_numfloors"], $f_number);
			$cell_index++;
			$sheet->write($row_index, $cell_index, $row["posaddress_store_floorsurface1"], $f_number);
			$cell_index++;
			$sheet->write($row_index, $cell_index, $row["posaddress_store_floorsurface2"], $f_number);
			$cell_index++;
			$sheet->write($row_index, $cell_index, $row["posaddress_store_floorsurface3"], $f_number);
			$cell_index++;
			$sheet->write($row_index, $cell_index, $row["posaddress_store_headcounts"], $f_number);
			$cell_index++;
			$sheet->write($row_index, $cell_index, $row["posaddress_store_fulltimeeqs"], $f_number);
			$cell_index++;
		}
		
		

		
		$cell_index = 0;
		$row_index++;
	}
}

for($i=0;$i<count($captions);$i++)
{
	$sheet->setColumn($i, $i, $col_widths[$i]);
}


$xls->close(); 

?>