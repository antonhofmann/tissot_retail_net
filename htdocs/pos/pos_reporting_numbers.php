<?php
/********************************************************************

    pos_reporting_numbers.php

    Lists used reporting numbers.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-07-22
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-07-22
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");

$sql = "select country_name, max(posaddress_eprepnr) as repnr " . 
       "from posaddresses " . 
	   "left join countries on country_id = posaddress_country ";
$list_filter = "posaddress_ownertype = 2";

$sql .= " where " .$list_filter . " group by country_name order by country_name";

$list = new ListView($sql);

$list->set_entity("posaddresses");

$list->add_column("country_name", "Country");
$list->add_column("repnr", "Latest Number");


$list->process();

$page = new Page("reporting_numbers");

$page->header();
$page->title("Latest Reporting Numbers");
$list->render();
$page->footer();

?>
