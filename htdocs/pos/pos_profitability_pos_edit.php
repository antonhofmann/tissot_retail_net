<?php
/********************************************************************

    pos_profitability_pos_edit

    Edit Profitability data of POS Location

    created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-10-20
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-10-20
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

if(!has_access("has_access_to_pos_profitability") 
	and !has_access("can_edit_pos_profitability_of_his_pos")
	and !has_access("can_edit_pos_profitability_of_all_pos"))
{
	redirect("/pos");
}

$pos_data = get_poslocation(param("pos_id"), "posaddresses");
$currency_symbol = "";
$sql = "select currency_symbol " .
       " from posaddresses " . 
	   " left join countries on country_id = posaddress_country " . 
	   " left join currencies on currency_id = country_currency " . 
	   " where posaddress_id= " . dbquote($pos_data["posaddress_id"]);

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$currency_symbol = $row["currency_symbol"];
}

$pos_link = '<a href="posindex_pos.php?country=' . $pos_data["posaddress_country"] .'&ltf=all&ostate=1&province=0&lt=0&id=' . $pos_data["posaddress_id"] . '" target="_blank">' . $pos_data["posaddress_name"] . '</a>';


/********************************************************************
    Create Form
*********************************************************************/

$form = new Form("possellouts", "possellouts");

$form->add_hidden("country", param("country"));
$form->add_hidden("province", param("province"));
$form->add_hidden("ostate", param("ostate"));
$form->add_hidden("pos_id", param("pos_id"));
$form->add_hidden("ltype", param("ltype"));

$form->add_label("posaddress_name", "POS Name", RENDER_HTML, $pos_link);
$form->add_label("posaddress_address", "Street", 0, $pos_data["posaddress_address"]);
$form->add_label("posaddress_place", "City", 0, $pos_data["place_name"]);
$form->add_label("posaddress_place", "City", 0, $pos_data["country_name"]);
$form->add_label("posaddress_store_openingdate", "Opening Date", 0, to_system_date($pos_data["posaddress_store_openingdate"]));
$form->add_label("posaddress_store_closingdate", "Closing Date", 0, to_system_date($pos_data["posaddress_store_closingdate"]));

$form->add_section("Sellout Data");
$form->add_comment("Please indicate real business date from passed years.");
$form->add_hidden("possellout_posaddress_id", param("pos_id"));


if(param("id"))
{
	$form->add_label("possellout_year", "Year*");
}
else
{
	$form->add_edit("possellout_year", "Year*", NOTNULL, "", TYPE_INT, 4);
}

$form->add_edit("possellout_month", "Number of months selling", 0, "", TYPE_INT, 2);

$form->add_edit("possellout_watches_units", "Watches Units sold", 0, "", TYPE_INT, 12);
$form->add_edit("possellout_bijoux_units", "Watch Straps Units sold", 0, "", TYPE_INT, 12);
$form->add_edit("possellout_grossales", "Gross Sales", 0, "", TYPE_INT, 12);
$form->add_edit("possellout_net_sales", "Net Sales", 0, "", TYPE_INT, 12);
$form->add_edit("possellout_grossmargin", "Gross Margin", 0, "", TYPE_INT, 12);
$form->add_edit("possellout_operating_expenses", "Indirect operating expenses", 0, "", TYPE_INT, 12);
$form->add_edit("possellout_operating_income_excl", "Operating Income", 0, "", TYPE_INT, 12);
$form->add_edit("possellout_wholsale_margin", "Wholesale margin (%)", 0, "", TYPE_DECIMAL, 6, 2);
$form->add_edit("possellout_operating_income_incl", "Operating income incl. Whole Sale Margin", 0, "", TYPE_INT, 12);



$form->add_button("delete", "Delete");
$form->add_button("back", "Back");

if(has_access("can_edit_pos_profitability_of_his_pos")
	or has_access("can_edit_pos_profitability_of_all_pos"))
{
	$form->add_button(FORM_BUTTON_SAVE, "Save");
}

$form->populate();
$form->process();


$link = "pos_profitability_pos.php?country=" . param("country") . '&province=' . param("province") . '&ltf=' . param("ltf"). '&ostate=' . param("ostate") . '&pos_id=' . param("pos_id"). '&ltype=' . param("ltype");

if($form->button(FORM_BUTTON_SAVE))
{
	if($form->validate())
	{
		$form->save;
		redirect($link);
	}
}
elseif($form->button("back"))
{
	redirect($link);
}
elseif($form->button("delete"))
{
	$sql = "delete from possellouts " .
	       "where possellout_id = " . id();

    $result = mysql_query($sql) or dberror($sql);
	redirect($link);
}



$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: POS yearly sellout");

$form->render();

$page->footer();

?>
