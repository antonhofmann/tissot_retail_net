<?php
/********************************************************************

    posfiles.php

    Entry page for the files section.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_use_posindex");

set_referer("posfile.php");

//check if user can edit this company
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	$user = get_user(user_id());

	$sql = "select posaddress_client_id " .
		   "from posaddresses " .
		   "where posaddress_id = " . param("pos_id") . 
		   "   and posaddress_client_id = " . $user["address"];

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($user["address"] == $row["posaddress_client_id"])
	{
		$can_edit = true;
	}
}

$pos = array();
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}

/********************************************************************
    prepare all data needed
*********************************************************************/
register_param("pos_id");

$posfiles = array();
$sql_files = "select posfile_id, posfile_path from posfiles where posfile_posaddress = " . param("pos_id");
$res = mysql_query($sql_files) or dberror($sql_files);

while ($row = mysql_fetch_assoc($res))
{
	
	$link = "http://" . $_SERVER["HTTP_HOST"] . "/" . $row["posfile_path"];

	
	$link = "<a href=\"" . $link. "\" target=\"_blank\"><img style=\"padding-top:3px;\"src=\"/pictures/view.gif\" border='0'/></a>";
	$posfiles[$row["posfile_id"]] = $link;
}

/********************************************************************
    Header
*********************************************************************/
$form = new Form("posfiles", "posfiles");
require_once("include/poslocation_head.php");

$form->add_hidden("country", param("country"));
$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("psc", param("psc"));
$form->add_hidden("let", param("let"));
$form->add_hidden("province",  param("province"));
$form->add_hidden("ostate", param("ostate"));

$form->populate();
$form->process();



/********************************************************************
    list
*********************************************************************/
// create sql
$sql = "select posfile_id, posfile_title,  posfilegroup_name, posfiletype_name, " . 
       "posfile_path, posfile_description, posfiles.date_modified as datemodified " .
       "from posfiles " . 
	   "left join posfilegroups on posfilegroup_id = posfile_filegroup " . 
	   "left join posfiletypes on posfiletype_id = posfile_filetype ";

$list_filter = "(posfile_posorder = 0 or posfile_posorder is NULL) and posfile_posaddress = " . param("pos_id");

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);
$list->set_title("POS Files");
$list->set_entity("posfiles");
$list->set_order("posfile_title");
$list->set_filter($list_filter);   
$list->set_group("posfilegroup_name");

$link = "posfile.php?pos_id=" . param("pos_id") . "&country=" . param("country"). "&ltf=" . param("ltf") . "&psc=" . param("psc"). "&ostate=" . param("ostate"). '&province=' . param("province") . '&let=' . param("let");

$list->add_hidden("pos_id", param("pos_id"));
$list->add_text_column("file", "", COLUMN_UNDERSTAND_HTML, $posfiles);

if(has_access("can_edit_posindex") or ($can_edit == true and has_access("can_edit_his_posindex")))
{
	$list->add_column("posfile_title", "Title", $link, "", "", COLUMN_NO_WRAP);
}
else
{
	$list->add_column("posfile_title", "Title", "", "", "", COLUMN_NO_WRAP);
}

$list->add_column("posfiletype_name", "File Type", "", "", "", COLUMN_NO_WRAP);

$list->add_column("datemodified", "Date", "", "", "", COLUMN_NO_WRAP);

$list->add_column("posfile_description", "Description");
if(has_access("can_edit_posindex") or ($can_edit == true and has_access("can_edit_his_posindex")) or has_access("can_add_new_pos_files"))
{
	$list->add_button("new", "Add New File to POS", $link);
}

$list->add_button("back", "Back to POS List");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

if($list->button("back"))
{
	redirect("posindex.php?country=" . param("country") . "&ltf=" . param("ltf") . "&psc=" . param("psc"). '&ostate=' . param("ostate"). '&province=' . param("province") . '&let=' . param("let"));
}
elseif($list->button("new"))
{
	redirect($link);
}


/********************************************************************
    get project files for projects not in retail net
*********************************************************************/
$project_file_lists = array();
$project_file_forms = array();
$sql = "select posorder_id, posorder_ordernumber " . 
       "from posorders " . 
	   "where posorder_order is NULL " .
	   " and posorder_type = 1 " . 
	   " and posorder_posaddress = " . param("pos_id") . 
	   " order by posorder_order";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$project_file_lists[] = "list_o" . $row["posorder_id"];
	
	// build sql for attachment entries
	$sql_attachment = "select posfile_id, posfile_title, posfiles.date_modified as datemodified, " .
		              "posfilegroup_name, posfiletype_name, posfile_path, posfile_description " .
					  "from posfiles " . 
					  "left join posfilegroups on posfilegroup_id = posfile_filegroup " . 
					  "left join posfiletypes on posfiletype_id = posfile_filetype ";

	$list1_filter = "posfile_posorder > 0 and posfile_posorder = " . $row["posorder_id"] . 
	                " and posfile_posaddress = " . param("pos_id");
					  


	$posfiles = array();
	$sql_files = $sql_attachment . " where " . $list1_filter;
	$res_a = mysql_query($sql_files) or dberror($sql_files);

	
	while ($row_a = mysql_fetch_assoc($res_a))
	{
		$link = "http://" . $_SERVER["HTTP_HOST"] . "/" . $row_a["posfile_path"];
		$link = "<a href=\"" . $link. "\" target=\"_blank\"><img style=\"padding-top:3px;\"src=\"/pictures/view.gif\" border='0'/></a>";
		$posfiles[$row_a["posfile_id"]] = $link;
	}

	/*
	if (!has_access("has_access_to_all_attachments_in_posindex"))
	{
		$list1_filter = $list1_filter . " and (order_file_address_address = " . $user_data["address"] . " or order_file_owner = " . user_id() . ")";
	}
	*/
	//create list
	$listname = "list_o" . $row["posorder_id"];

	$$listname = new ListView($sql_attachment, LIST_HAS_HEADER | LIST_HAS_FOOTER);
	$$listname->set_title("Files of Project " . $row["posorder_ordernumber"]);
	$$listname->set_entity("posfiles");
	$$listname->set_filter($list1_filter);
	$$listname->set_order("posfiles.date_created DESC");
	$$listname->set_group("posfilegroup_name");



	$$listname->add_text_column("file", "", COLUMN_UNDERSTAND_HTML, $posfiles);


	$link = "posfile.php?pos_id=" . param("pos_id") . "&country=" . param("country");

	$list->add_hidden("pos_id", param("pos_id"));
	$list->add_text_column("file", "", COLUMN_UNDERSTAND_HTML, $posfiles);

	if(has_access("can_edit_posindex") or ($can_edit == true and has_access("can_edit_his_posindex")))
	{
		$$listname->add_column("posfile_title", "Title", $link, "", "", COLUMN_NO_WRAP);
	}
	else
	{
		$$listname->add_column("posfile_title", "Title", "", "", "", COLUMN_NO_WRAP);
	}

	//$$listname->add_column("posfile_title", "Title", "", "", "", COLUMN_NO_WRAP);
	$$listname->add_column("posfiletype_name", "Type", "", "", "", COLUMN_NO_WRAP);
	$$listname->add_column("datemodified", "Date", "", "", "", COLUMN_NO_WRAP);
	$$listname->add_column("posfile_description", "Description");

	$$listname->populate();
	$$listname->process();

}


/********************************************************************
    get project files
*********************************************************************/

$sql = "select posorder_order, posorder_ordernumber, project_id " . 
       "from posorders " . 
	   " left join projects on project_order = posorder_order " . 
	   "where posorder_order > 0 and posorder_type = 1 and posorder_posaddress = " . param("pos_id") . 
	   " order by posorder_order";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$project_file_lists[] = "list" . $row["posorder_order"];
	
	// build sql for attachment entries
	$sql_attachment = "select distinct order_file_id, order_file_visited, order_files.date_modified as datemodified, ".
					  "    order_file_title, order_file_description, ".
					  "    order_file_path, file_type_name, ".
					  "    order_files.date_created, ".
					  "    order_file_category_name, order_file_category_priority ".
					  "from order_files ".
					  "left join order_file_categories on order_file_category_id = order_file_category ".
					  "left join file_types on order_file_type = file_type_id ";
					  

	// build filter for the list of attachments
	$list1_filter = "order_file_order = " . $row["posorder_order"];


	$posfiles = array();
	$sql_files = $sql_attachment . " where " . $list1_filter;
	$res_a = mysql_query($sql_files) or dberror($sql_files);

	while ($row_a = mysql_fetch_assoc($res_a))
	{
		
		$link = "http://" . $_SERVER["HTTP_HOST"] . "/" . $row_a["order_file_path"];
		$link = "<a href=\"" . $link. "\" target=\"_blank\"><img style=\"padding-top:3px;\"src=\"/pictures/view.gif\" border='0'/></a>";
		$posfiles[$row_a["order_file_id"]] = $link;
	}

	/*
	if (!has_access("has_access_to_all_attachments_in_posindex"))
	{
		$list1_filter = $list1_filter . " and (order_file_address_address = " . $user_data["address"] . " or order_file_owner = " . user_id() . ")";
	}
	*/

	//create list
	$listname = "list" . $row["posorder_order"];

	$$listname = new ListView($sql_attachment, LIST_HAS_HEADER | LIST_HAS_FOOTER);
	$$listname->set_title("Files of Project " . $row["posorder_ordernumber"]);
	$$listname->set_entity("order_files");
	$$listname->set_filter($list1_filter);
	$$listname->set_order("order_files.date_created DESC");
	$$listname->set_group("order_file_category_priority", "order_file_category_name");



	$$listname->add_text_column("file", "", COLUMN_UNDERSTAND_HTML, $posfiles);
	$$listname->add_column("order_file_title", "Title", "", "", "", COLUMN_NO_WRAP);
	$$listname->add_column("file_type_name", "Type", "", "", "", COLUMN_NO_WRAP);
	
	$$listname->add_column("datemodified", "Date", "", "", "", COLUMN_NO_WRAP);
	
	$$listname->add_column("order_file_description", "Description");

	$$listname->populate();
	$$listname->process();


	//get LN supporting documents
	$formname = 'lndocs_' . $row["posorder_order"];
	
	$ln_basicdata = array();


	$sql_l = "select *,ln_basicdata.date_created as versiondate  from ln_basicdata " . 
		   "left join projects on project_id = ln_basicdata_project " . 
		   "where ln_basicdata_project = " . dbquote($row["project_id"]) . 
		   " and ln_basicdata_version = 0";
	

	$res_l = mysql_query($sql_l) or dberror($sql_l);
	if ($row_l = mysql_fetch_assoc($res_l))
	{
		$ln_basicdata = $row_l;
	}


	if(count($ln_basicdata) > 0 and
	($ln_basicdata["ln_basicdata_floorplan"] 
	or $ln_basicdata["ln_basicdata_pix1"]
	or $ln_basicdata["ln_basicdata_pix2"]
	or $ln_basicdata["ln_basicdata_pix3"]
	or $ln_basicdata["ln_basicdata_draft_aggreement"])
	)
	{
		$project_file_forms[$listname] = 'lndocs_' . $row["posorder_order"];
		$show_ln_documents = true;
		$floorplan = '<a href="' . $ln_basicdata["ln_basicdata_floorplan"] . '" target="_blank">Download File</a>';
		$location_layout = '<a href="' . $ln_basicdata["ln_basicdata_location_layout"] . '" target="_blank">Download File</a>';
		$pix1 = '<a href="' . $ln_basicdata["ln_basicdata_pix1"] . '" target="_blank">Download File</a>';
		$pix2 = '<a href="' . $ln_basicdata["ln_basicdata_pix2"] . '" target="_blank">Download File</a>';
		$pix3 = '<a href="' . $ln_basicdata["ln_basicdata_pix3"] . '" target="_blank">Download File</a>';
		$lease_agreement = '<a href="' . $ln_basicdata["ln_basicdata_draft_aggreement"] . '" target="_blank">Download File</a>';
		
		$$formname = new Form("ln_basicdata", "ln_basicdata");

		
		//$camera = '<i class="fa fa-camera af_ln_docs"></i>';
		$camera = '';
		
		$$formname->add_section("LN/AF Documents " . $camera);
		
		if($ln_basicdata["ln_basicdata_floorplan"])
		{
			$$formname->add_label("f1", "Mall Map/Street Map", RENDER_HTML, $floorplan);
		}
		if($ln_basicdata["ln_basicdata_location_layout"])
		{
			$$formname->add_label("f16", "Location layout with dimensions", RENDER_HTML, $location_layout);
		}
		if($ln_basicdata["ln_basicdata_pix1"])
		{
			$$formname->add_label("f2", "Photo 1", RENDER_HTML, $pix1);
		}
		if($ln_basicdata["ln_basicdata_pix2"])
		{
			$$formname->add_label("f3", "Photo 2", RENDER_HTML, $pix2);
		}
		if($ln_basicdata["ln_basicdata_pix2"])
		{
			$$formname->add_label("f4", "Photo 3", RENDER_HTML, $pix3);
		}
		if(has_access("has_access_to_all_attachments_in_projects") and $ln_basicdata["ln_basicdata_draft_aggreement"])
		{
			$$formname->add_label("f5", "Draft Lease Agreement", RENDER_HTML, $lease_agreement);
		}

		$$formname->populate();
		$$formname->process();

	}


	//list of cer mail attachments
	$sql_cer_mail_attachments = "select * from cer_mails where cer_mail_attachment1_path <> '' and cer_mail_project = " . $row["project_id"];
	$res_l = mysql_query($sql_l) or dberror($sql_l);
	if ($row_l = mysql_fetch_assoc($res_l))
	{

		$listname = "cermailattachments" . $row["posorder_order"];
		$project_file_lists[] = "cermailattachments" . $row["posorder_order"];
		$sql_cer_mail_attachments = "select * from cer_mails";

		$$listname = new ListView($sql_cer_mail_attachments);
		$$listname->set_title("CER/AF Mail Attachments");
		$$listname->set_entity("order_files");
		$$listname->set_filter("cer_mail_attachment1_path <> '' and cer_mail_project = " . $row["project_id"]);
		$$listname->set_order("cer_mails.date_created DESC");

		$$listname->add_column("date_created", "Date/Time", "", "", "", COLUMN_NO_WRAP);
		$$listname->add_column("cer_mail_sender", "Created by", "", "", "", COLUMN_NO_WRAP);
		$link = "http://" . $_SERVER["HTTP_HOST"] . "{cer_mail_attachment1_path}";
		$$listname->add_column("cer_mail_attachment1_title", "Title", $link, "", "", COLUMN_NO_WRAP);

		$$listname->populate();
		$$listname->process();
	}
	

}

/********************************************************************
    create page
*********************************************************************/
$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();

$page->title("Files: " . $poslocation["posaddress_name"]);

require_once("include/tabs.php");

$form->render();

$list->render();

echo "--------------------------------------------------------------------------------------------------------------------------------------------------";

foreach($project_file_lists as $key=>$listname)
{
	
	echo "<p>&nbsp;</p>";
	$$listname->render();

	if(array_key_exists($listname, $project_file_forms))
	{
		$formname= $project_file_forms[$listname];
		echo "<p>&nbsp;</p>";
		$$formname->render();
	}
	
}



$page->footer();
?>