<?php
/********************************************************************

    openinghours_state_xls.php

    Generate Excel-File of states of opening hours

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-10-17
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-10-17
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

$user = get_user(user_id());

/********************************************************************
    prepare Data Needed
*********************************************************************/


$header = "";
$header = "State of Opening Hours: (" . date("d.m.Y G:i") . ")";

/********************************************************************
    prepare Data
*********************************************************************/
$user = get_user(user_id());

$sql = "SELECT posaddress_client_id, country_name, address_company,
		count(posaddress_id) as num_recs
		FROM
		posaddresses
		INNER JOIN addresses ON posaddresses.posaddress_client_id = addresses.address_id
		INNER JOIN countries on country_id = address_country
		where (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null)
		group by country_name, addresses.address_company, posaddress_client_id";


//get number of operating POS locations
$pos_openinghrs  = array();


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	
	$sql_i = "select COUNT(DISTINCT posopeninghr_posaddress_id) as num_recs " . 
			 "from posaddresses " .
		     "INNER JOIN posopeninghrs on posopeninghr_posaddress_id = posaddress_id " . 
		     " where posaddress_client_id = " . $row["posaddress_client_id"]  .
		      " and (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null) " . 
		     " group by posaddress_client_id";
	
	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{
		$pos_openinghrs[$row["posaddress_client_id"]] = $row_i["num_recs"];
	}
}


/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "state_of_openinghours_per_client_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename);

$xls->setVersion(8);

$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_normal_bold =& $xls->addFormat();
$f_normal_bold->setSize(8);
$f_normal_bold->setAlign('left');
$f_normal_bold->setBorder(1);
$f_normal_bold->setBold();


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('yellow');



//captions
$captions = array();
//$captions[] = "Nr";
//$captions[] = "Geographical Region";
$captions[] = "Country";
$captions[] = "Company";
$captions[] = "#POS";
$captions[] = "#POS with OH";


/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);


$sheet->writeRow(2, 0, $captions, $f_normal_bold);


$row_index = 3;
$cell_index = 0;
$counter = 0;
$col_widths = array();
for($i=0;$i<count($captions);$i++)
{
	$col_widths[$i] = strlen($captions[$i]);
}



$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{

	$sheet->write($row_index, $cell_index, $row["country_name"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["country_name"]))
	{
		$col_widths[$cell_index] = strlen($row["country_name"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_company"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_company"]))
	{
		$col_widths[$cell_index] = strlen($row["address_company"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["num_recs"], $f_number);
	$cell_index++;

	if(array_key_exists($row["posaddress_client_id"], $pos_openinghrs))
	{
		$sheet->write($row_index, $cell_index, $pos_openinghrs[$row["posaddress_client_id"]], $f_number);
	}
	else
	{
		$sheet->write($row_index, $cell_index, "", $f_number);
	}
	
	$cell_index = 0;
	$row_index++;
	
}

for($i=0;$i<count($captions);$i++)
{
	$sheet->setColumn($i, $i, $col_widths[$i]);
}


$xls->close(); 

?>