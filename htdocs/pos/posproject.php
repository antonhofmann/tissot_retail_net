<?php
/********************************************************************

    posproject.php

    Creation and mutation of posorder records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_use_posindex");

$pos = array();
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");
}

$system_currency = get_system_currency_fields();

//check if user can edit this company
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	$can_edit = get_user_edit_permission(user_id(), param("pos_id"));
}

$is_a_new_project = false;
if(!id()) {
	$is_a_new_project = true;
}

/********************************************************************
    Check if a projects exists in projects
*********************************************************************/
$project_exists = 0;
$sql = "select posorder_id, posorder_product_line, posorder_product_line_subclass, " . 
       "posorder_postype, posorder_order, order_actual_order_state_code, " . 
	   "posorder_product_line, posorder_project_kind " . 
       "from posorders " .
	   "left join orders on order_id = posorder_order " . 
	   "where posorder_id = " . id() . " and posorder_order > 0";

$order_number = 0;
$order_state_code = "890";
$product_line = 0;
$postype = 0;
$product_line_subclass = 0;
$project_kind = 0;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$project_exists = 1;
	$order_number = $row["posorder_order"];
	$order_state_code = $row["order_actual_order_state_code"];
	$product_line = $row["posorder_product_line"];
	$product_line_subclass = $row["posorder_product_line_subclass"];
	$postype  = $row["posorder_postype"];
	$project_kind = $row["posorder_project_kind"];

	$project_type_subclass = $row["project_type_subclass_id"];
}
else
{
	$sql = "select posorder_id, posorder_product_line, posorder_product_line_subclass, " . 
		   "posorder_postype, posorder_order, order_actual_order_state_code, " . 
		   "posorder_product_line, posorder_project_kind " . 
		   "from posorders " .
		   "left join orders on order_id = posorder_order " . 
		   "where posorder_id = " . id() . " and posorder_order is null";
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$project_kind = $row["posorder_project_kind"];
	}
}

//product lines
$product_lines = array();
$product_lines[0] = "n.a.";
$sql_product_lines =  "select product_line_id, product_line_name from product_lines where product_line_posindex= 1 order by product_line_name";

$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
while ($row = mysql_fetch_assoc($res))
{
	$product_lines[$row["product_line_id"]] = $row["product_line_name"];
}


$sql_pos_types = "select postype_id, postype_name ".
                     "from postypes ".
		             "where postype_id in (1, 2, 3, 4) " . 
                     "order by postype_name";



$sql_project_type_subclasses = "select project_type_subclass_id, project_type_subclass_name " . 
                               " from project_type_subclasses order by project_type_subclass_name";

//project Types
$project_kinds = array();
$sql_project_kinds =  "select projectkind_id, projectkind_name " . 
                      " from projectkinds " . 
					  " where projectkind_id > 0 " . 
					  " order by projectkind_name";

$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
while ($row = mysql_fetch_assoc($res))
{
	$project_kinds[$row["projectkind_id"]] = $row["projectkind_name"];
}

//create sql for product line subclasses
$num_or_product_line_subclasses = 0;
if(param("posorder_product_line"))
{
	$sql_product_line_sub_classes = "select productline_subclass_id, productline_subclass_name " . 
									"from productline_subclass_productlines " . 
		                            " left join productline_subclasses on productline_subclass_id = productline_subclass_productline_class_id " . 
									"where productline_subclass_productline_line_id = " . dbquote(param("posorder_product_line"));

	//count subclasses
	$sql_product_line_sub_classes_count = "select count(productline_subclass_id) as num_recs " . 
									"from productline_subclass_productlines " . 
									 " left join productline_subclasses on productline_subclass_id = productline_subclass_productline_class_id " . 
									"where productline_subclass_productline_line_id = " . dbquote(param("posorder_product_line"));

	$res = mysql_query($sql_product_line_sub_classes_count) or dberror($sql_product_line_sub_classes_count);
    $row = mysql_fetch_assoc($res);
	$num_or_product_line_subclasses = $row["num_recs"];
}
elseif($product_line_subclass > 0) 
{
	$sql_product_line_sub_classes = "select productline_subclass_id, productline_subclass_name " . 
									"from productline_subclasses " . 
									"where productline_subclass_productline = " . $product_line_subclass;

	//count subclasses
	$sql_product_line_sub_classes_count = "select count(productline_subclass_id) as num_recs " . 
									"from productline_subclasses " . 
									"where productline_subclass_productline = " . $product_line_subclass;

	$res = mysql_query($sql_product_line_sub_classes_count) or dberror($sql_product_line_sub_classes_count);
    $row = mysql_fetch_assoc($res);
	$num_or_product_line_subclasses = $row["num_recs"];
}
else
{
	$sql_product_line_sub_classes = "select productline_subclass_id, productline_subclass_name " . 
									"from productline_subclasses " . 
									"where productline_subclass_productline = {posorder_product_line}";
}


//neighbourhood
$pos_order_sql = "select * from posorders where posorder_id = " . id();

$neighbourhoods = array();
$neighbourhoods_business_types = array();
$neighbourhoods_price_ranges = array();
$res = mysql_query($pos_order_sql) or dberror($pos_order_sql);
if ($row = mysql_fetch_assoc($res))
{
	$neighbourhoods["Shop on Left Side"] = $row["posorder_neighbour_left"];
	$neighbourhoods["Shop on Right Side"] = $row["posorder_neighbour_right"];
	$neighbourhoods["Shop Across Left Side"] = $row["posorder_neighbour_acrleft"];
	$neighbourhoods["Shop Across Right Side"] = $row["posorder_neighbour_acrright"];
	$neighbourhoods["Other Brands in Area"] = $row["posorder_neighbour_brands"];

	$neighbourhoods_business_types["Shop on Left Side"] = $row["posorder_neighbour_left_business_type"];
	$neighbourhoods_business_types["Shop on Right Side"] = $row["posorder_neighbour_right_business_type"];
	$neighbourhoods_business_types["Shop Across Left Side"] = $row["posorder_neighbour_acrleft_business_type"];
	$neighbourhoods_business_types["Shop Across Right Side"] = $row["posorder_neighbour_acrright_business_type"];

	$neighbourhoods_price_ranges["Shop on Left Side"] = $row["posorder_neighbour_left_price_range"];
	$neighbourhoods_price_ranges["Shop on Right Side"] = $row["posorder_neighbour_right_price_range"];
	$neighbourhoods_price_ranges["Shop Across Left Side"] = $row["posorder_neighbour_acrleft_price_range"];
	$neighbourhoods_price_ranges["Shop Across Right Side"] = $row["posorder_neighbour_acrright_price_range"];
}


//get business types and price ranges

$businesstypes = array();
$sql = "select businesstype_id, businesstype_text " . 
	   "from businesstypes " . 
	   " order by businesstype_text";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$businesstypes[$row["businesstype_id"]]	= $row["businesstype_text"];
}
//$businesstypes["---"]	= "---------------------------------";
//$businesstypes[999999]	= "Other not listed above";

$priceranges = array();
$sql = "select pricerange_id, pricerange_text " . 
	   "from priceranges " . 
	   " order by pricerange_text";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$priceranges[$row["pricerange_id"]]	= $row["pricerange_text"];
}

/********************************************************************
    Build Form
*********************************************************************/

$form = new Form("posorders", "POS Project");

$form->add_section("Name and address");

require_once("include/poslocation_head.php");

$form->add_hidden("pos_id", param("pos_id"));
$form->add_hidden("posorder_posaddress", param("pos_id"));
$form->add_hidden("posorder_type", 1);

$form->add_hidden("posorder_order");
$form->add_hidden("country", param("country"));
$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("psc", param("psc"));
$form->add_hidden("let", param("let"));
$form->add_hidden("province",  param("province"));
$form->add_hidden("ostate", param("ostate"));

$form->add_section("Project Details");

if(has_access("can_edit_posindex"))
{
	if($project_exists == 1)
	{
		$form->add_label("posorder_ordernumber", "Project Number");
		$form->add_lookup("posorder_product_line", "Product Line", "product_lines", "product_line_name");
		$form->add_lookup("posorder_product_line_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name");
		$form->add_lookup("posorder_postype", "POS Type", "postypes", "postype_name");
		$form->add_lookup("posorder_subclass", "POS Type Subclass", "possubclasses", "possubclass_name");
		$form->add_lookup("posorder_project_kind", "Project Type", "projectkinds", "projectkind_name");
		$form->add_lookup("posorder_legal_type", "Legal Type", "project_costtypes", "project_costtype_text");
		
		
		if($project_kind == 8 or param("posorder_project_kind") == 8) // PopUp
		{
			$form->add_label("posorder_popup_name", "PopUp Description");
			$form->add_label("posorder_opening_date", "PopUp Opening Date");
			$form->add_label("posorder_popup_closingdate", "PopUp Closing Date");

		}
		else
		{
			$form->add_label("posorder_opening_date", "Opening Date");
			$form->add_label("posorder_closing_date", "Closing Date");
		}
	
	}
	else
	{
		
		$form->add_edit("posorder_ordernumber", "Project Number*", NOTNULL);
		$form->add_list("posorder_product_line", "Product Line*", $product_lines , SUBMIT | NOTNULL | ALLOW_NULL_VALUE, $product_line);

		$form->add_list("posorder_product_line_subclass", "Product Line Subclass", $sql_product_line_sub_classes,0);
		
		$form->add_list("posorder_postype", "POS Type*",
		$sql_pos_types, NOTNULL);

		$form->add_list("posorder_subclass", "POS Type Subclass",
		"select possubclass_id, possubclass_name from possubclasses order by possubclass_name");
		
		$form->add_list("posorder_project_kind", "Project Type*",$project_kinds, SUBMIT | NOTNULL | ALLOW_NULL_VALUE, $project_kind);

		$form->add_list("posorder_legal_type", "Legal Type*",
		"select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id IN(1,2,6) order by project_costtype_text", NOTNULL);
		
		
		if($project_kind == 8 or param("posorder_project_kind") == 8) // PopUp
		{
			$form->add_hidden("posorder_closing_date");
			$form->add_edit("posorder_popup_name", "PopUp Description*", NOTNULL);
			$form->add_edit("posorder_opening_date", "PopUp Opening Date", 0, "", TYPE_DATE);
			$form->add_edit("posorder_popup_closingdate", "PopUp Closing Date", 0, "", TYPE_DATE);
		}
		else
		{
			$form->add_edit("posorder_opening_date", "POS Opening Date", 0, "", TYPE_DATE);
			$form->add_edit("posorder_closing_date", "POS Closing Date", 0, "", TYPE_DATE);
			$form->add_hidden("posorder_popup_closingdate");
		}
	
	}

	$form->add_section("General Remarks");
	$form->add_checkbox("posorder_project_locally_produced", "project is locally realized (local production)", 0, "", "Local Production");
	
	$form->add_list("posorder_project_type_subclass_id", "Project Type Subclass", $sql_project_type_subclasses, 0, $project_type_subclass);
	

	
	$form->add_checkbox("posorder_uses_icedunes_visuals", "The project uses Visuals", "", 0, "Visuals");
	
	$form->add_checkbox("posorder_furniture_type_store", "SIS project realized in STORE furniture", 0, "", "Furniture");
	$form->add_checkbox("posorder_furniture_type_sis", "STORE project realized in SIS furniture", 0, "", "Furniture");

	$form->add_multiline("posorder_remark", "Remarks", 4);

	$form->add_section("Neighbourhood");
	$form->add_edit("posorder_neighbour_left", "Shop on Left Side", 0, $neighbourhoods["Shop on Left Side"]);

	$form->add_list("posorder_neighbour_left_business_type", "Business Type Shop on Left Side", $businesstypes, 0, $neighbourhoods_business_types["Shop on Left Side"]);
	if(param("posorder_neighbour_left_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_left_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_left_business_type_new");
	}
	
	$form->add_list("posorder_neighbour_left_price_range", "Price Range Shop on Left Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop on Left Side"]);

	$form->add_edit("posorder_neighbour_right", "Shop on Right Side", 0, $neighbourhoods["Shop on Right Side"]);

	$form->add_list("posorder_neighbour_right_business_type", "Business Type Shop on Right Side", $businesstypes, 0, $neighbourhoods_business_types["Shop on Right Side"]);

	if(param("posorder_neighbour_right_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_right_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_right_business_type_new");
	}

	$form->add_list("posorder_neighbour_right_price_range", "Price Range Shop on Right Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop on Right Side"]);

	$form->add_edit("posorder_neighbour_acrleft", "Shop Across Left Side", 0, $neighbourhoods["Shop Across Left Side"]);
	$form->add_list("posorder_neighbour_acrleft_business_type", "Business Type  Across Left Side", $businesstypes, 0, $neighbourhoods_business_types["Shop Across Left Side"]);

	if(param("posorder_neighbour_acrleft_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_acrleft_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_acrleft_business_type_new");
	}

	$form->add_list("posorder_neighbour_acrleft_price_range", "Price Range Shop  Across Left Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop Across Left Side"]);

	$form->add_edit("posorder_neighbour_acrright", "Shop Across Right Side", 0, $neighbourhoods["Shop Across Right Side"]);

	$form->add_list("posorder_neighbour_acrright_business_type", "Business Type Shop Across Right Side", $businesstypes, 0, $neighbourhoods_business_types["Shop Across Right Side"]);

	if(param("posorder_neighbour_acrright_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_acrright_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_acrright_business_type_new");
	}

	$form->add_list("posorder_neighbour_acrright_price_range", "Price Range Shop Across Right Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop Across Right Side"]);

	$form->add_multiline("posorder_neighbour_brands", "Other Brands in Area", 4, 0, $neighbourhoods["Other Brands in Area"]);

	/*
	if($project_exists == 0)
	{
		$form->add_section("Local Currency before EURO");
		$form->add_edit("posorder_currency_symbol", "Currency Symbol");
		$form->add_edit("posorder_exchangerate", "Exchange Rate", 0, "", TYPE_DECIMAL, 12, 6);
	}
	else
	{
		$form->add_hidden("posorder_currency_symbol");
		$form->add_hidden("posorder_exchangerate");
	}
	*/

	$form->add_hidden("posorder_currency_symbol");
	$form->add_hidden("posorder_exchangerate");

	$form->add_button("save_form", "Save");
	$form->add_button("back", "Back");
	$form->add_button("delete_project", "Delete Project");
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$form->add_label("posorder_ordernumber", "Project Number");
	$form->add_lookup("posorder_product_line", "Product Line", "product_lines", "product_line_name");
	$form->add_lookup("posorder_product_line_subclass", "Product Line Subclass", "productline_subclasses", "productline_subclass_name");
	$form->add_lookup("posorder_postype", "POS Type", "postypes", "postype_name");
	$form->add_lookup("posorder_subclass", "POS Type Subclass", "possubclasses", "possubclass_name");
	$form->add_lookup("posorder_project_kind", "Project Type", "projectkinds", "projectkind_name");
	$form->add_lookup("posorder_legal_type", "Legal Type", "project_costtypes", "project_costtype_text");
	
	
	if($project_kind == 8 or param("posorder_project_kind") == 8) // popup
	{
		$form->add_label("posorder_opening_date", "PopUp Opening Date");
		$form->add_label("posorder_closing_date", "PopUp Closing Date");
	}
	else
	{
		$form->add_label("posorder_opening_date", "POS Opening Date");
		$form->add_label("posorder_popup_closingdate", "POS Closing Date");
	}

	$form->add_section("General Remarks");
	$form->add_checkbox("posorder_project_locally_produced", "project is locally realized (local production)", 0, "", "Local Production");
	
	
	$form->add_list("posorder_project_type_subclass_id", "Project Type Subclass", $sql_project_type_subclasses, 0, $project_type_subclass);

	
	$form->add_checkbox("posorder_furniture_type_store", "SIS project realized in STORE furniture", 0, "", "Furniture");
	$form->add_checkbox("posorder_furniture_type_sis", "STORE project realized in SIS furniture", 0, "", "Furniture");
	$form->add_checkbox("posorder_uses_icedunes_visuals", "The project uses Visuals", "", 0, "Visuals");
	$form->add_label("posorder_remark", "Remarks");

	$form->add_section("Neighbourhood");
	$form->add_edit("posorder_neighbour_left", "Shop on Left Side", 0, $neighbourhoods["Shop on Left Side"]);

	$form->add_list("posorder_neighbour_left_business_type", "Business Type Shop on Left Side", $businesstypes, 0, $neighbourhoods_business_types["Shop on Left Side"]);
	if(param("posorder_neighbour_left_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_left_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_left_business_type_new");
	}
	
	$form->add_list("posorder_neighbour_left_price_range", "Price Range Shop on Left Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop on Left Side"]);

	$form->add_edit("posorder_neighbour_right", "Shop on Right Side", 0, $neighbourhoods["Shop on Right Side"]);

	$form->add_list("posorder_neighbour_right_business_type", "Business Type Shop on Right Side", $businesstypes, 0, $neighbourhoods_business_types["Shop on Right Side"]);

	if(param("posorder_neighbour_right_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_right_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_right_business_type_new");
	}

	$form->add_list("posorder_neighbour_right_price_range", "Price Range Shop on Right Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop on Right Side"]);

	$form->add_edit("posorder_neighbour_acrleft", "Shop Across Left Side", 0, $neighbourhoods["Shop Across Left Side"]);
	$form->add_list("posorder_neighbour_acrleft_business_type", "Business Type  Across Left Side", $businesstypes, 0, $neighbourhoods_business_types["Shop Across Left Side"]);

	if(param("posorder_neighbour_acrleft_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_acrleft_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_acrleft_business_type_new");
	}

	$form->add_list("posorder_neighbour_acrleft_price_range", "Price Range Shop  Across Left Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop Across Left Side"]);

	$form->add_edit("posorder_neighbour_acrright", "Shop Across Right Side", 0, $neighbourhoods["Shop Across Right Side"]);

	$form->add_list("posorder_neighbour_acrright_business_type", "Business Type Shop Across Right Side", $businesstypes, 0, $neighbourhoods_business_types["Shop Across Right Side"]);

	if(param("posorder_neighbour_acrright_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_acrright_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_acrright_business_type_new");
	}

	$form->add_list("posorder_neighbour_acrright_price_range", "Price Range Shop Across Right Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop Across Right Side"]);

	$form->add_multiline("posorder_neighbour_brands", "Other Brands in Area", 4, 0, $neighbourhoods["Other Brands in Area"]);

	if($project_exists == 0)
	{
		$form->add_section("Local Currency before EURO");
		$form->add_edit("posorder_currency_symbol", "Currency Symbol");
		$form->add_edit("posorder_exchangerate", "Exchange Rate", 0, "", TYPE_DECIMAL, 12, 6);
	}
	else
	{
		$form->add_hidden("posorder_currency_symbol");
		$form->add_hidden("posorder_exchangerate");
	}

	$form->add_button("back", "Back");
}



// Populate form and process button clicks

$form->populate();
$form->process();


if($form->button("save_form"))
{
	if($form->validate())
	{
		$form->save();
		//update projects
		$sql = "update projects set " . 
			   "project_is_local_production = " . dbquote($form->value("posorder_project_locally_produced")) . ", " . 
			   "project_type_subclass_id = " . dbquote($form->value("posorder_project_type_subclass_id")) . ", " . 
			   "project_furniture_type_store = " . dbquote($form->value("posorder_furniture_type_store")) . ", " . 
			   "project_furniture_type_sis = " . dbquote($form->value("posorder_furniture_type_sis"))  . ", " . 
			   "project_uses_icedunes_visuals = " . dbquote($form->value("posorder_uses_icedunes_visuals")) .
			   " where project_order = " . $order_number;
		$result = mysql_query($sql) or dberror($sql);
		
		
		if($form->value("posorder_closing_date"))
		{
			$sql = "update posaddresses set posaddress_store_closingdate = " .  dbquote(from_system_date($form->value("posorder_closing_date"))) . 
				   " where posaddress_id = " . dbquote(param("pos_id"));
			$result = mysql_query($sql) or dberror($sql);
		}

		$result = update_posdata_from_posorders(param("pos_id"));
		
		$form->message("The data has been saved.");

		$result = update_store_locator($form->value("pos_id"));


		//send mail for new corporate take over projects
		if(param("posorder_opening_date") 
			and $is_a_new_project == true
		    and param("posorder_legal_type") == 1
			and param("posorder_project_kind") == 4
			and count($pos) > 0)
		{
				
				
				
				$dchannel = 'not available';
								
				$Mail = new ActionMail('info.takeover.corporate.pos');
								
				
				$data = array(
					'country' => $pos['country_name'],
					'pos_name' => $pos['posaddress_name'],
					'opening_date' => to_system_date($pos['posaddress_store_openingdate']),
					'takeover_date' => param("posorder_opening_date"),
					'eprnr' => $pos['posaddress_eprepnr'] ? $pos['posaddress_eprepnr'] : "not available",
					'dchannel' => $dchannel,
					'sap_nr' => $pos['posaddress_sapnumber'] ? $pos['posaddress_sapnumber'] : 'not available',
					'sap_shipto_nr' => $pos['posaddress_sap_shipto_number'] ? $pos['posaddress_sap_shipto_number'] : 'not available'
				);

				

				$Mail->setDataloader($data);
				
				if($senmail_activated == true)
				{
					$Mail->send(true);

					$recipients = $Mail->getRecipients();

					$ccrecipients = $Mail->getCCRecipients();

					
					$rcpts = array();
					$mail_text = '';
					$subject = '';
					foreach($recipients as $user_id=>$recipient_data)
					{
						$sql = "select user_email from users where user_id = " . dbquote($user_id);
						$res = mysql_query($sql) or dberror($sql);
						if($row = mysql_fetch_assoc($res)) {
							$rcpts[] = $row["user_email"];
						}
						$mail_text = $recipient_data->getBody(true);
						$subject = $recipient_data->getSubject(true);
					}

					foreach($ccrecipients as $key=>$email)
					{
						$rcpts[] = $email;
					}

					//update mail history
					if($mail_text and count($rcpts) > 0) {
						$pos_mail_fields = array();
						$pos_mail_values = array();

						$pos_mail_fields[] = "posmail_posaddress_id";
						$pos_mail_values[] = dbquote($pos["posaddress_id"]);;

						$pos_mail_fields[] = "posmail_mail_template_id";
						$pos_mail_values[] = 154;;

						$pos_mail_fields[] = "posmail_sender_email";
						$pos_mail_values[] = dbquote($Mail->getSender()->email);;


						$pos_mail_fields[] = "posmail_recipeint_email";
						$pos_mail_values[] = dbquote(implode(';', $rcpts));

						$pos_mail_fields[] = "posmail_subject";
						$pos_mail_values[] = dbquote($subject);

						$pos_mail_fields[] = "posmail_text";
						$pos_mail_values[] = dbquote($mail_text);

						$pos_mail_fields[] = "date_created";
						$pos_mail_values[] = "current_timestamp";

						$pos_mail_fields[] = "user_created";
						$pos_mail_values[] = dbquote($_SESSION["user_login"]);


						$sql = "insert into posmails (" . join(", ", $pos_mail_fields) . ") values (" . join(", ", $pos_mail_values) . ")";
						mysql_query($sql) or dberror($sql);

					}
				}
		}

	}
}
elseif($form->button("back"))
{
	$link = "posprojects.php?pos_id=" . param("pos_id")  . "&country=" . param("country"). "&ltf=" . param("ltf") . "&psc=" . param("psc"). "&ostate=" . param("ostate"). '&province=' . param("province") . '&let=' . param("let");
	redirect($link);
}
elseif($form->button("delete_project"))
{
	$sql = "delete from posorders where posorder_id = " . id();
	$res = mysql_query($sql) or dberror($sql);

	$sql = "delete from posorderinvestments where posorderinvestment_posorder = " . id();
	$res = mysql_query($sql) or dberror($sql);
	
	$result = update_posdata_from_posorders(param("pos_id"));

	$link = "posprojects.php?pos_id=" . param("pos_id")  . "&country=" . param("country"). "&ltf=" . param("ltf"). "&ostate=" . param("ostate"). '&province=' . param("province") . '&let=' . param("let");
	redirect($link);
}


/********************************************************************
	check an build missing investment records
*********************************************************************/
if(id() > 0)
{
	$result = build_missing_investment_records(id());
	$result = update_investment_records_from_system_currency(id());
}

/********************************************************************
    list of investment in system currency
*********************************************************************/
/*
// create sql
$sql_sc = "select * from posorderinvestments " . 
          "left join posinvestment_types on posinvestment_type_id = posorderinvestment_investment_type"; 

$list_filter_sc = "posinvestment_type_intangible <> 1 and posorderinvestment_posorder = " . id();

$cers = array();
$cmss = array();
$diff = array();
$diffp = array();

$cer_group_totals = array();
$cms_group_totals = array();
$diff_group_totals = array();

$cer_totals = 0;
$cms_totals = 0;
$diff_totals = 0;


$sql = $sql_sc . " where " . $list_filter_sc;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$cers[$row["posorderinvestment_id"]] = $row["posorderinvestment_amount_cer"];
	$cmss[$row["posorderinvestment_id"]] = $row["posorderinvestment_amount_cms"];

	//build group totals
	if(isset($cer_group_totals[$row["posinvestment_type_group"]]))
	{
		$cer_group_totals[$row["posinvestment_type_group"]] = $cer_group_totals[$row["posinvestment_type_group"]] + $row["posorderinvestment_amount_cer"];
	}
	else
	{
		$cer_group_totals[$row["posinvestment_type_group"]] = $row["posorderinvestment_amount_cer"];
	}

	if(isset($cms_group_totals[$row["posinvestment_type_group"]]))
	{
		$cms_group_totals[$row["posinvestment_type_group"]] = $cms_group_totals[$row["posinvestment_type_group"]] + $row["posorderinvestment_amount_cms"];
	}
	else
	{
		$cms_group_totals[$row["posinvestment_type_group"]] = $row["posorderinvestment_amount_cms"];
	}

	//build list totals
	$cer_totals = $cer_totals + $row["posorderinvestment_amount_cer"];
	$cms_totals = $cms_totals + $row["posorderinvestment_amount_cms"];

	if($row["posorderinvestment_amount_cer"] - $row["posorderinvestment_amount_cms"] != 0)
	{
		$diff[$row["posorderinvestment_id"]] = number_format($row["posorderinvestment_amount_cms"] - $row["posorderinvestment_amount_cer"],2,".","'");
		
		if($row["posorderinvestment_amount_cer"] != 0)
		{
			$diffp[$row["posorderinvestment_id"]] = ($row["posorderinvestment_amount_cms"] - $row["posorderinvestment_amount_cer"]) / $row["posorderinvestment_amount_cer"];
			$diffp[$row["posorderinvestment_id"]] = number_format($diffp[$row["posorderinvestment_id"]]*100,2,".","") ."%";
		}
	}
}
*/

/********************************************************************
    Create List in System Currency
*********************************************************************/ 
/*
$list_sc = new ListView($sql_sc, LIST_HAS_HEADER | LIST_HAS_FOOTER);
$list_sc->set_title("Investment in ". $system_currency["symbol"]);
$list_sc->set_entity("posorderinvestments");
$list_sc->set_order("posinvestment_type_sortorder");
$list_sc->set_filter($list_filter_sc);
$list_sc->set_group("posinvestment_type_group");

//$link = "posinvestment.php?pos_id=" . param("pos_id");

$list_sc->add_hidden("pos_id", param("pos_id"));
$list_sc->add_column("posinvestment_type_name", "Type", "", "", "", COLUMN_NO_WRAP);

if(has_access("can_edit_posindex"))
{
	if($project_exists == 0)
	{
		$list_sc->add_number_edit_column("cer", "Amount CER", "12", COLUMN_ALIGN_RIGHT, $cers);
		$list_sc->add_number_edit_column("cms", "Real Cost", "12", COLUMN_ALIGN_RIGHT, $cmss);
	}
	else
	{
		$list_sc->add_text_column("cer", "Amount CER", COLUMN_ALIGN_RIGHT, $cers);
		$list_sc->add_text_column("cms", "Real Cost", COLUMN_ALIGN_RIGHT, $cmss);
	}
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$list_sc->add_text_column("cer", "Amount CER", COLUMN_ALIGN_RIGHT, $cers);
	$list_sc->add_text_column("cms", "Real Cost", COLUMN_ALIGN_RIGHT, $cmss);
}

if(count($diff) > 0)
{
	$list_sc->add_text_column("diff", "Difference", COLUMN_ALIGN_RIGHT, $diff);
	$list_sc->add_text_column("diffp", "in %", COLUMN_ALIGN_RIGHT, $diffp);
}

// set group totals
foreach ($cer_group_totals as $key=>$value)
{
    if($value)
	{
		$list_sc->set_group_footer("cer", $key , number_format($value,2, ".", "'"));
	}
	else
	{
		$list_sc->set_group_footer("cer", $key , "0.00");
	}
}
foreach ($cms_group_totals as $key=>$value)
{
    if($value)
	{
		$list_sc->set_group_footer("cms", $key , number_format($value,2, ".", "'"));
	}
	else
	{
		$list_sc->set_group_footer("cms", $key , "0.00");
	}
}
foreach ($cms_group_totals as $key=>$value)
{
    $group_diff = $value - $cer_group_totals[$key];
	if($group_diff)
	{
		$list_sc->set_group_footer("diff", $key , number_format($group_diff,2, ".", "'"));
	}
}

foreach ($cer_group_totals as $key=>$value)
{
	if($value != 0)
	{
		$group_diffp = ($cms_group_totals[$key] - $value) / $value;
		$group_diffp = number_format($group_diffp*100,2,".","");
		$list_sc->set_group_footer("diffp", $key , $group_diffp ."%");
	}
}


//list total
$list_sc->set_footer("posinvestment_type_name", "Total");
$list_sc->set_footer("cer", number_format($cer_totals,2, ".", "'"));
$list_sc->set_footer("cms", number_format($cms_totals,2, ".", "'"));

if(count($diff) > 0)
{
	$diff_totals = $cms_totals - $cer_totals;
	$list_sc->set_footer("diff", number_format($diff_totals,2, ".", "'"));
}


if($cer_totals != 0)
{
	$difft = ($cms_totals - $cer_totals) / $cer_totals;
	$difft = number_format($difft*100,2,".","");
	$list_sc->set_footer("diffp", number_format($difft,2, ".", "") ."%");
}

if($project_exists == 0)
{
	if(has_access("can_edit_posindex"))
	{
		$list_sc->add_button("save_sc", "Save List Values");
	}
	elseif($can_edit == true and has_access("can_edit_his_posindex"))
	{
		$list_sc->add_button("save_sc", "Save List Values");
	}
}
*/
/********************************************************************
    list of investment in local currency
*********************************************************************/
/*
// create sql
$sql_lc = "select * from posorderinvestments " . 
          "left join posinvestment_types on posinvestment_type_id = posorderinvestment_investment_type"; 

$list_filter_lc = "posinvestment_type_intangible <> 1 and posorderinvestment_posorder = " .  id();

$cers_lc = array();
$cmss_lc = array();
$diff_lc = array();
$diffp_lc = array();

$cer_group_totals_lc = array();
$cms_group_totals_lc = array();
$diff_group_totals_lc = array();

$cer_totals_lc = 0;
$cms_totals_lc = 0;
$diff_totals_lc = 0;


$sql = $sql_lc . " where " . $list_filter_lc;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$cers_lc[$row["posorderinvestment_id"]] = $row["posorderinvestment_amount_cer_loc"];
	$cmss_lc[$row["posorderinvestment_id"]] = $row["posorderinvestment_amount_cms_loc"];

	//build group totals
	if(isset($cer_group_totals_lc[$row["posinvestment_type_group"]]))
	{
		$cer_group_totals_lc[$row["posinvestment_type_group"]] = $cer_group_totals_lc[$row["posinvestment_type_group"]] + $row["posorderinvestment_amount_cer_loc"];
	}
	else
	{
		$cer_group_totals_lc[$row["posinvestment_type_group"]] = $row["posorderinvestment_amount_cer_loc"];
	}

	if(isset($cms_group_totals_lc[$row["posinvestment_type_group"]]))
	{
		$cms_group_totals_lc[$row["posinvestment_type_group"]] = $cms_group_totals_lc[$row["posinvestment_type_group"]] + $row["posorderinvestment_amount_cms_loc"];
	}
	else
	{
		$cms_group_totals_lc[$row["posinvestment_type_group"]] = $row["posorderinvestment_amount_cms_loc"];
	}

	//build list totals
	$cer_totals_lc = $cer_totals_lc + $row["posorderinvestment_amount_cer_loc"];
	$cms_totals_lc = $cms_totals_lc + $row["posorderinvestment_amount_cms_loc"];

	if($row["posorderinvestment_amount_cer_loc"] - $row["posorderinvestment_amount_cms_loc"] != 0)
	{
		$diff_lc[$row["posorderinvestment_id"]] = number_format($row["posorderinvestment_amount_cms_loc"] - $row["posorderinvestment_amount_cer_loc"],2,".","'");
		
		if($row["posorderinvestment_amount_cer_loc"] != 0)
		{
			$diffp_lc[$row["posorderinvestment_id"]] = ($row["posorderinvestment_amount_cms_loc"] - $row["posorderinvestment_amount_cer_loc"]) / $row["posorderinvestment_amount_cer_loc"];
			$diffp_lc[$row["posorderinvestment_id"]] = number_format($diffp_lc[$row["posorderinvestment_id"]]*100,2,".","") ."%";
		}
	}
}
*/

/********************************************************************
    Create List
*********************************************************************/ 
/*
$list_lc = new ListView($sql_lc, LIST_HAS_HEADER | LIST_HAS_FOOTER);
$list_lc->set_title("Investment in Local Currency");
$list_lc->set_entity("posorderinvestments");
$list_lc->set_order("posinvestment_type_sortorder");
$list_lc->set_filter($list_filter_lc);   
$list_lc->set_group("posinvestment_type_group");


//$link = "posinvestment.php?pos_id=" . param("pos_id");

$list_lc->add_hidden("pos_id", param("pos_id"));
$list_lc->add_column("posinvestment_type_name", "Type", "", "", "", COLUMN_NO_WRAP);

if(has_access("can_edit_posindex"))
{
	if($project_exists == 0)
	{
		$list_lc->add_number_edit_column("cer_lc", "Amount CER", "12", COLUMN_ALIGN_RIGHT, $cers_lc);
		$list_lc->add_number_edit_column("cms_lc", "Real Cost", "12", COLUMN_ALIGN_RIGHT, $cmss_lc);
	}
	else
	{
		$list_lc->add_text_column("cer_lc", "Amount CER", COLUMN_ALIGN_RIGHT, $cers_lc);
		$list_lc->add_text_column("cms_lc", "Real Cost", COLUMN_ALIGN_RIGHT, $cmss_lc);
	}
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$list_lc->add_text_column("cer_lc", "Amount CER", COLUMN_ALIGN_RIGHT, $cers_lc);
	$list_lc->add_text_column("cms_lc", "Real Cost", COLUMN_ALIGN_RIGHT, $cmss_lc);
}

if(count($diff_lc) > 0)
{
	$list_lc->add_text_column("diff_lc", "Difference", COLUMN_ALIGN_RIGHT, $diff_lc);
	$list_lc->add_text_column("diffp_lc", "in %", COLUMN_ALIGN_RIGHT, $diffp_lc);
}




// set group totals
foreach ($cer_group_totals_lc as $key=>$value)
{
    if($value)
	{
		$list_lc->set_group_footer("cer_lc", $key , number_format($value,2, ".", "'"));
	}
	else
	{
		$list_lc->set_group_footer("cer_lc", $key , "0.00");
	}
}
foreach ($cms_group_totals_lc as $key=>$value)
{
    if($value)
	{
		$list_lc->set_group_footer("cms_lc", $key , number_format($value,2, ".", "'"));
	}
	else
	{
		$list_lc->set_group_footer("cms_lc", $key , "0.00");
	}
}
foreach ($cms_group_totals_lc as $key=>$value)
{
    $group_diff_lc = $value - $cer_group_totals_lc[$key];
	if($group_diff_lc)
	{
		$list_lc->set_group_footer("diff_lc", $key , number_format($group_diff_lc,2, ".", "'"));
	}
}

foreach ($cer_group_totals_lc as $key=>$value)
{
	if($value != 0)
	{
		$group_diffp_lc = ($cms_group_totals_lc[$key] - $value) / $value;
		$group_diffp_lc = number_format($group_diffp_lc*100,2,".","");
		$list_lc->set_group_footer("diffp_lc", $key , $group_diffp_lc ."%");
	}
}


//list total

$list_lc->set_footer("posinvestment_type_name", "Total");
$list_lc->set_footer("cer_lc", number_format($cer_totals_lc,2, ".", "'"));
$list_lc->set_footer("cms_lc", number_format($cms_totals_lc,2, ".", "'"));

if(count($diff_lc) > 0)
{
	$diff_totals_lc = $cms_totals_lc - $cer_totals_lc;
	$list_lc->set_footer("diff_lc", number_format($diff_totals_lc,2, ".", "'"));
}


if($cer_totals_lc != 0)
{
	$difft_lc = ($cms_totals_lc - $cer_totals_lc) / $cer_totals_lc;
	$difft_lc = number_format($difft_lc*100,2,".","");
	$list_lc->set_footer("diffp_lc", number_format($difft_lc,2, ".", "") ."%");
}

if($project_exists == 0)
{
	if(has_access("can_edit_posindex") or has_access("can_edit_his_posindex"))
	{
		$list_lc->add_button("save_lc", "Save List Values");
	}
}
*/
/********************************************************************
    Populate lists and process button clicks
*********************************************************************/ 
/*
if($form->button("save_form"))
{

	$list_sc->process();
	$list_lc->process();
	
	$reuslt = update_store_locator($form->value("pos_id"));
}
else
{
	$list_sc->populate();
	$list_sc->process();

	$list_lc->populate();
	$list_lc->process();
}


//system currency
if ($list_sc->button("save_sc"))
{
   foreach ($list_sc->values("cer") as $key=>$value)
   {
		$fields = array();
		if(is_int_value($value, 12) or is_decimal_value($value, 12, 2))
		{
			$fields[] = "posorderinvestment_amount_cer = " . dbquote($value);
		}
		else
		{
			$fields[] = "posorderinvestment_amount_cer = NULL";
		}
		$fields[] = "user_modified = " . dbquote(user_login());
		$fields[] = "date_modified = current_timestamp";

		$sql = "update posorderinvestments set " . join(", ", $fields) . " where posorderinvestment_id = " . $key;
		mysql_query($sql) or dberror($sql);
   }

   foreach ($list_sc->values("cms") as $key=>$value)
   {
		$fields = array();
		if(is_int_value($value, 12) or is_decimal_value($value, 12, 2))
		{
			$fields[] = "posorderinvestment_amount_cms = " . dbquote($value);
		}
		else
		{
			$fields[] = "posorderinvestment_amount_cms = NULL";
		}
		$fields[] = "user_modified = " . dbquote(user_login());
		$fields[] = "date_modified = current_timestamp";

		$sql = "update posorderinvestments set " . join(", ", $fields) . " where posorderinvestment_id = " . $key;
		mysql_query($sql) or dberror($sql);

   }

   $result = update_investment_records_from_system_currency(id());

   $link = "posproject.php?id=". id() . "&pos_id=" . param("pos_id");
   redirect($link);
}

//local currency
if ($list_lc->button("save_lc"))
{
   foreach ($list_lc->values("cer_lc") as $key=>$value)
   {
		$fields = array();
		if(is_int_value($value, 12) or is_decimal_value($value, 12, 2))
		{
			$fields[] = "posorderinvestment_amount_cer_loc = " . dbquote($value);
		}
		else
		{
			$fields[] = "posorderinvestment_amount_cer_loc = NULL";
		}
		$fields[] = "user_modified = " . dbquote(user_login());
		$fields[] = "date_modified = current_timestamp";

		$sql = "update posorderinvestments set " . join(", ", $fields) . " where posorderinvestment_id = " . $key;
		mysql_query($sql) or dberror($sql);

   }

   foreach ($list_lc->values("cms_lc") as $key=>$value)
   {
		$fields = array();
		if(is_int_value($value, 12) or is_decimal_value($value, 12, 2))
		{
			$fields[] = "posorderinvestment_amount_cms_loc = " . dbquote($value);
		}
		else
		{
			$fields[] = "posorderinvestment_amount_cms_loc = NULL";
		}
		$fields[] = "user_modified = " . dbquote(user_login());
		$fields[] = "date_modified = current_timestamp";

		$sql = "update posorderinvestments set " . join(", ", $fields) . " where posorderinvestment_id = " . $key;
		mysql_query($sql) or dberror($sql);
		

   }

   $result = update_investment_records_from_local_currency(id());
   

   $link = "posproject.php?id=". id() . "&pos_id=" . param("pos_id");
   redirect($link);
}
*/


/********************************************************************
    Render page
*********************************************************************/ 

$page = new Page("posindex");
require "include/pos_page_actions.php";
$page->header();

$page->title(id() ? "Edit Project Information" : "Add Project Information");

require_once("include/tabs.php");

$form->render();

/*
if(id() > 0 and $order_state_code >= "890")
{
	echo "<p>&nbsp;</p>";
	$list_sc->render();
	echo "<p>&nbsp;</p>";
	$list_lc->render();
}
*/
$page->footer();

?>