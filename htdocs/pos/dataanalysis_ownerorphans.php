<?php
/********************************************************************

    dataanalysis_ownerorphans.php

    Lists of addresses not having running POS Locations

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   201-06-21
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-06-21
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/access_filters.php";

check_access("can_administrate_posindex");

set_referer("poscompany.php");

//get all owner companies
$posowner_ids = array();
$sql = "select DISTINCT posaddress_franchisee_id " . 
       " from posaddresses " .
	   " left join addresses on address_id = posaddress_franchisee_id " .
	   " where address_active = 1 and posaddress_store_closingdate is not null and posaddress_store_closingdate <> '0000-00-00'";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$posowner_ids[] = $row["posaddress_franchisee_id"];
}


$posowner_ids2 = array();
$list_filter = "address_id = 0";
if(count($posowner_ids) > 0)
{
	$filter = " posaddress_franchisee_id in (" . implode(',', $posowner_ids) . ") ";

	$posowner_ids2 = array();
	$sql = "select DISTINCT posaddress_franchisee_id " . 
		   " from posaddresses " . 
		   " where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " . 
		   " and " . $filter;


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$posowner_ids2[] = $row["posaddress_franchisee_id"];
	}


	if(count($posowner_ids2) > 0)
	{
		$fullDiff = array_merge(array_diff($posowner_ids, $posowner_ids2), array_diff($posowner_ids2, $posowner_ids));
		$list_filter = " address_id in (" . implode(',', $fullDiff) . ") ";
	}
}



$sql = "select address_id, address_company, address_zip, " .
	 "    address_place, country_name, address_type_name, province_canton, " .
	  "IF(address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1, 'yes', '') as franchisee, " .
	   "IF(address_is_independent_retailer = 1, 'yes', '') as retailer " .
	 "from addresses " . 
	 " left join address_types on address_type_id = address_type " .
	 "left join countries on address_country = country_id " . 
	 "left join places on place_id = address_place_id " .
	 "left join provinces on province_id = place_province";



$open_poslocations = array();
$closed_poslocations = array();
$projects_in_pipeline = array();

$res = mysql_query($sql . " where " . $list_filter) or dberror($sql . " where " . $list_filter);
while ($row = mysql_fetch_assoc($res))
{
	$sql_p = "select count(posaddress_id) as num_recs from posaddresses " .
		     " where posaddress_store_openingdate is not null and posaddress_store_openingdate <> '0000-00-00' " . 
	         " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " . 
		     " and posaddress_franchisee_id = " . $row["address_id"];

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	$row_p = mysql_fetch_assoc($res_p);
	$open_poslocations[$row["address_id"]] = $row_p["num_recs"];

	$sql_p = "select count(posaddress_id) as num_recs from posaddresses " .
		     " where posaddress_store_closingdate is not null and posaddress_store_closingdate <> '0000-00-00' " . 
		     " and posaddress_franchisee_id = " . $row["address_id"];

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	$row_p = mysql_fetch_assoc($res_p);
	$closed_poslocations[$row["address_id"]] = $row_p["num_recs"];

	$sql_p = "select count(posaddress_id) as num_recs from posaddressespipeline " .
		     " where posaddress_franchisee_id = " .  $row["address_id"];

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	$row_p = mysql_fetch_assoc($res_p);
	$projects_in_pipeline[$row["address_id"]] = $row_p["num_recs"];

}


$list = new ListView($sql);

$list->set_entity("addresses");
$list->set_order("country_name, address_company");
$list->set_filter($list_filter);

$list->add_column("country_name", "Country", "",
    "select country_name from countries order by country_name");
$list->add_column("address_type_name", "Type", "", "select address_type_name from address_types where and address_type_id <> 7 order by address_type_name");
$list->add_column("address_company", "Company", "popup1:poscompany.php?id={address_id}");
$list->add_column("address_place", "City", "", LIST_FILTER_FREE);
$list->add_column("franchisee", "Franchisee");
$list->add_column("retailer", "Retailer");
$list->add_text_column("open", "Operating", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $open_poslocations);
$list->add_text_column("closed", "Closed", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $closed_poslocations);
$list->add_text_column("pipeline", "Pipeline", COLUMN_ALIGN_CENTER | COLUMN_UNDERSTAND_HTML, $projects_in_pipeline);



$link = "dataanalysis_ownerorphans_xls.php";
$link = "javascript:popup('" . $link . "');";

$list->add_button("print", "Print List", $link);

$list->populate();
$list->process();

/********************************************************************
    Populate and process button clicks
*********************************************************************/ 

$page = new Page("poscompanies");
require "include/pos_page_actions.php";
$page->header();

$page->title("Owner Companies without POS Locations");
$list->render();


$page->footer();

?>
