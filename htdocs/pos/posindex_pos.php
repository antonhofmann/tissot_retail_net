<?php
/********************************************************************

    posindex_pos.php

    Creation and mutation of address records.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_use_posindex");

set_referer("posprojects.php");

if(param("pos_id"))
{
	register_param("pos_id");
	param("id", param("pos_id"));
}
else
{
	register_param("pos_id");
	param("pos_id", id());
}


$pos = array();
$ern_caption = "Reporting Number";
if(param("pos_id") > 0)
{
	$pos = get_poslocation(param("pos_id"), "posaddresses");

	if($pos["posaddress_ownertype"] == 1)
	{
		$ern_caption = "Enterprise Reporting Number";
	}
}

//get info for relocated POS
$former_relocated_pos = get_relocated_pos_info2(param("pos_id"));
$relocated_pos = get_relocated_pos_info3(param("pos_id"));


//update latest project with closing date
if(array_key_exists("posaddress_id", $pos))
{
	$sql =	"select posorder_id, posorder_order " .  
			"from posorders " .
			"left join orders on order_id = posorder_order " . 
			"where (order_actual_order_state_code <> '900' or order_actual_order_state_code is null) " . 
			" and posorder_type = 1 and posorder_posaddress = " . dbquote($pos["posaddress_id"]) .
			" and posorder_opening_date is not null and posorder_opening_date <> '0000-00-00' " .  
			" order by posorder_year DESC, posorder_opening_date DESC";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{

		if($row["posorder_order"] > 0)
		{
			$sql = "update projects " . 
				   "set project_shop_closingdate = " . dbquote($pos["posaddress_store_closingdate"]) . 
				   " where project_order = " . $row["posorder_order"];
			$result = mysql_query($sql) or dberror($sql);
		}
		else // fake project
		{
			$sql = "update posorders  " . 
				   "set posorder_closing_date  = " . dbquote($pos["posaddress_store_closingdate"]) . 
				   " where posorder_id = " . $row["posorder_id"];
			$result = mysql_query($sql) or dberror($sql);
		}
	}
}

$has_project = update_posdata_from_posorders(param("pos_id"));

$ratings = array();
$ratings[5] = "bad";
$ratings[4] = "poor";
$ratings[3] = "good";
$ratings[2] = "very good";
$ratings[1] = "excellent";
//$ratings[0] = "n.a.";


//posareas
$posareas = array();
$sql = "select posareatype_id, posareatype_name " . 
	   "from posareatypes " . 
	   " order by posareatype_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$posareas[$row["posareatype_id"]]	= $row["posareatype_name"];
}

$pos_posareas = array();


$sql = "select posarea_area " . 
	   "from posareas " . 
	   "where posarea_posaddress = " . dbquote($pos["posaddress_id"]) . 
	   " order by posarea_area";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$pos_posareas[$row["posarea_area"]]	= $row["posarea_area"];
}



//check if user can edit this company
$can_edit = false;
if(has_access("can_edit_posindex"))
{
	$can_edit = true;
}
elseif(has_access("can_edit_his_posindex"))
{
	$can_edit = get_user_edit_permission(user_id(), param("pos_id"));
}


// Build form
$form = new Form("posaddresses", "posaddress");

if(has_access("can_view_mps_posindex")
	or has_access("can_view_mps_his_posindex")
	or has_access("can_edit_mps_posindex")
	or has_access("can_edit_only_his_mps_posindex"))
{
	$form->add_comment('<a href="/mps/pos/address/' . param("pos_id") . '" target="_blank">Access this POS in MPS</a>');
}


//if(has_access("can_edit_posindex") and (id() > 0 and $pos["posaddress_store_postype"] != 4) or id() == 0)
if(has_access("can_edit_posindex"))
{
	$form->add_section("Roles");
	
	if(has_access("can_administrate_posindex"))
	{
		$form->add_list("posaddress_client_id", "Client*",
			"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where (address_active = 1 and (address_type = 1 or address_type = 4)) or address_id = {posaddress_client_id} order by country_name, address_company", NOTNULL);

		if($has_project == false)
		{
			$form->add_list("posaddress_ownertype", "Legal Type*",
				"select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id IN (1, 2, 6)", SUBMIT | NOTNULL);
		}
		else
		{
			$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text", 0, $pos["posaddress_ownertype"]);
		}

		$form->add_edit("posaddress_eprepnr", $ern_caption);
		$form->add_label("posaddress_sapnumber", "SAP Customer Code");

		if(array_key_exists("posaddress_store_postype", $pos) 
			and $pos["posaddress_store_postype"] != 4)
		{
			$form->add_list("posaddress_franchisor_id", "Franchisor",
				"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisor = 1 order by country_name, address_company");
		}
		elseif(id() == 0 and (param("posaddress_ownertype") == 1 
			or param("posaddress_ownertype") == 2))
		{
			$form->add_list("posaddress_franchisor_id", "Franchisor",
				"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisor = 1 order by country_name, address_company");
		}
		else
		{
			$form->add_hidden('posaddress_franchisor_id');
		}

		if(array_key_exists("posaddress_ownertype", $pos) and $pos["posaddress_ownertype"] != 6 and $pos['posaddress_store_postype'] != 2)
		{
			$tmp = "Owner Company*";
		}
		else
		{
			$tmp = "Owner Company*";
		}

		
		
		
			
		if(array_key_exists("posaddress_country", $pos))
		{
			$f_sql = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company " . 
					 "from addresses " . 
					 "left join countries on country_id = address_country " .
					 "where address_showinposindex = 1 " . 
					 " and country_name <> '' and address_company <> '' and address_place <> '' " . 
					 " and ((address_canbefranchisee = 1  and address_country = " . $pos["posaddress_country"] . " ) or address_canbefranchisee_worldwide = 1 or (address_is_independent_retailer = 1  and address_country = " . $pos["posaddress_country"] . " ) or address_id = " . $pos["posaddress_franchisee_id"] . ") " .
					 " order by country_name, address_company";
		}
		else
		{
			$f_sql = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company " . 
					 "from addresses " . 
					 "left join countries on country_id = address_country " .
					 "where address_showinposindex = 1 " . 
					 " and country_name <> '' and address_company <> '' and address_place <> '' " . 
					 " and ((address_canbefranchisee = 1  and address_country = " . dbquote(param("country")) . " ) or address_canbefranchisee_worldwide = 1 or (address_is_independent_retailer = 1  and address_country = " . param("country") . " )) " .
					 " order by country_name, address_company";
		}


		$form->add_list("posaddress_franchisee_id", $tmp, $f_sql, NOTNULL);
		
		
		
	}
	elseif(param("pos_id") > 0 and $pos["posaddress_store_closingdate"] != '' and $pos["posaddress_store_closingdate"] != '0000-00-00' and $pos["posaddress_client_id"] > 0)
	{
		$form->add_lookup("posaddress_client_id", "Client", "addresses", "concat(address_company, ', ', address_place) as company", 0, $pos["posaddress_client_id"]);

		$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text", 0, $pos["posaddress_ownertype"]);

		$form->add_edit("posaddress_eprepnr", $ern_caption);
		$form->add_label("posaddress_sapnumber", "SAP Customer Code");

		$form->add_lookup("posaddress_franchisor_id", "Franchisor", "addresses", "concat(address_company, ', ', address_place) as company", 0, $pos["posaddress_franchisor_id"]);

		if($pos["posaddress_ownertype"] != 6 and $pos['posaddress_store_postype'] != 2)
		{
			$tmp = "Owner Company";
		}
		else
		{
			$tmp = "Owner Company";
		}
		$form->add_lookup("posaddress_franchisee_id", $tmp, "addresses", "concat(address_company, ', ', address_place) as company", 0, $pos["posaddress_franchisee_id"]);

	}
	else
	{
		$form->add_list("posaddress_client_id", "Client*",
			"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_active = 1 and (address_type = 1 or address_type = 4) order by country_name, address_company", NOTNULL);

		if($has_project == false)
		{
			$form->add_list("posaddress_ownertype", "Legal Type*",
				"select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id IN(1,2,6)", NOTNULL);
		}
		else
		{
			$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text", 0, $pos["posaddress_ownertype"]);
		}

		$form->add_edit("posaddress_eprepnr", $ern_caption);
		$form->add_label("posaddress_sapnumber", "SAP Customer Code");

		if(array_key_exists("posaddress_store_postype", $pos) 
			and $pos["posaddress_store_postype"] != 4)
		{
			$form->add_list("posaddress_franchisor_id", "Franchisor",
				"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisor = 1 order by country_name, address_company");
		}
		elseif(id() == 0 and (param("posaddress_ownertype") == 1 
			or param("posaddress_ownertype") == 2))
		{
			$form->add_list("posaddress_franchisor_id", "Franchisor",
				"select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company from addresses left join countries on country_id = address_country where address_showinposindex = 1 and country_name <> '' and address_company <> '' and address_place <> '' and address_canbefranchisor = 1 order by country_name, address_company");
		}
		else
		{
			$form->add_hidden('posaddress_franchisor_id');
		}

		if($pos["posaddress_ownertype"] != 6 and $pos['posaddress_store_postype'] != 2)
		{
			$tmp = "Owner Company*";
		}
		else
		{
			$tmp = "Owner Company*";
		}



		$f_sql = "select address_id, concat(country_name, ': ', address_company, ', ', address_place) as company " . 
				 "from addresses " . 
				 "left join countries on country_id = address_country " .
				 "where address_showinposindex = 1 " . 
				 " and country_name <> '' and address_company <> '' and address_place <> '' " . 
				 " and ((address_canbefranchisee = 1  and address_country = " . $pos["posaddress_country"] . " ) or address_canbefranchisee_worldwide = 1 or (address_is_independent_retailer = 1  and address_country = " . $pos["posaddress_country"] . " )) " .
				 " order by country_name, address_company";


		$form->add_list("posaddress_franchisee_id", $tmp, $f_sql, NOTNULL);

		
	}


	$form->add_section("Address Data");
	//$form->add_edit("posaddress_name", "POS Name*", NOTNULL);
	$form->add_edit("posaddress_name", "POS Name*", NOTNULL, "", TYPE_CHAR, 0, 0, 2, "posaddress_name");

	//$form->add_edit("posaddress_name2", "", 0);
	$form->add_edit("posaddress_name2", "", 0, "", TYPE_CHAR, 0, 0, 2, "posaddress_name2");

	$form->add_hidden("posaddress_address");
	$form->add_multi_edit("street", array("posaddress_street", "posaddress_street_number"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6), array('',''), 2, "posaddress_address", '', array(40,5));

	
	//$form->add_edit("posaddress_address2", "Address 2");
	$form->add_edit("posaddress_address2", "Additional Address Info", 0, "", TYPE_CHAR, 0, 0, 2, "posaddress_address2");
	

	
	if(param("id"))
	{
		$form->add_list("posaddress_country", "Country*",
			"select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);
	}
	else
	{
		$form->add_list("posaddress_country", "Country*",
			"select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT, param("country"));
	}

	
	if(param("posaddress_country"))
	{
		$form->add_list("posaddress_place_id", "City Selection*",
			"select place_id, concat(place_name, ' (', province_canton, ')') from places left join provinces on province_id = place_province where place_country = " . dbquote(param("posaddress_country")) . " order by place_name", SUBMIT | NOTNULL);
	}
	elseif(param("id"))
	{
		$form->add_list("posaddress_place_id", "City Selection*",
			"select place_id, concat(place_name, ' (', province_canton, ')') from places left join provinces on province_id = place_province where place_country = " . dbquote($pos["posaddress_country"]) . " order by place_name", SUBMIT | NOTNULL);
	}
	else
	{
		$form->add_list("posaddress_place_id", "City Selection*",
			"select place_id, concat(place_name, ' (', province_canton, ')') from places left join provinces on province_id = place_province where place_country = " . dbquote(param("country")) . " order by place_name", SUBMIT | NOTNULL);
	}
	
	$form->add_edit("posaddress_place", "City", NOTNULL | DISABLED);

	
	$form->add_edit("posaddress_zip", "Zip", 0);

	if(count($former_relocated_pos) > 0)
	{
		$link = '<a href="/pos/posindex_pos.php?pos_id=' . $former_relocated_pos["posaddress_id"] . '&country=' . param("country") .'&ltf=' . param("ltf") .'&psc=' . param("psc") .'&ostate=' . param("ostate") .'&province=' . param("province") .'&let=' . param("let") .'" target="_blank">' . $former_relocated_pos["posaddress_name"] . ', ' . $former_relocated_pos["place_name"] . '</a>';
		
		$form->add_section("Relocation Info");
		$form->add_comment("This POS is a relocation of the following POS:");
		$form->add_label("former_relocated_pos", "Relocated POS", RENDER_HTML, $link);
	}
	elseif(count($relocated_pos) > 0)
	{
		$link = '<a href="/pos/posindex_pos.php?pos_id=' . $relocated_pos["posaddress_id"] . '&country=' . param("country") .'&ltf=' . param("ltf") .'&psc=' . param("psc").'&ostate=' . param("ostate") .'&province=' . param("province") .'&let=' . param("let") .'" target="_blank">' . $relocated_pos["posaddress_name"] . ', ' . $relocated_pos["place_name"] . '</a>';
		
		$form->add_section("Relocation Info");
		$form->add_comment("This POS was relocated to the following POS:");
		$form->add_label("former_relocated_pos", "Relocated POS", RENDER_HTML, $link);
	}

	$form->add_section("Communication");
	$form->add_hidden("posaddress_phone");
	$form->add_multi_edit("phone_number", array("posaddress_phone_country", "posaddress_phone_area", "posaddress_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_hidden("posaddress_mobile_phone");
	$form->add_multi_edit("mobile_phone_number", array("posaddress_mobile_phone_country", "posaddress_mobile_phone_area", "posaddress_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_edit("posaddress_email", "Email");
	
	/*
	$form->add_edit("posaddress_contact_name", "Contact Name");
	*/
	$form->add_edit("posaddress_website", "Website");
    

	$form->add_section("Dates");
	$form->add_label("store_openingdate", "Opening Date",0, to_system_date($pos["posaddress_store_openingdate"]));
	$form->add_label("planned_store_closingdate", "Planned Closing Date", 0, to_system_date($pos["posaddress_store_planned_closingdate"]));
	$form->add_label("store_closingdate", "Closing Date", 0, to_system_date($pos["posaddress_store_closingdate"]));
	


	$form->add_section("Environment");
	foreach($posareas as $key=>$name)
	{
		if(array_key_exists($key, $pos_posareas))
		{
			$form->add_checkbox("area". $key, $name, 1, 0, "");
		}
		else
		{
			$form->add_checkbox("area". $key, $name, "", 0, "");
		}

	}

	$form->add_comment("Please indicate on which floor the POS will be situated (Ground floor, Street Level, 1st Floor etc.).");

	$form->add_edit("posaddress_store_floor", "Floor*", NOTNULL,$pos["posaddress_store_floor"], TYPE_CHAR, 30);

	$form->add_section("Area Perception");
	$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");
	if(count($pos) > 0 and array_key_exists($pos["posaddress_perc_class"], $ratings))
	{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, $ratings[$pos["posaddress_perc_class"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, $ratings[$pos["posaddress_perc_tourist"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings,  $ratings[$pos["posaddress_perc_transport"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings,  $ratings[$pos["posaddress_perc_people"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings,  $ratings[$pos["posaddress_perc_parking"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings,  $ratings[$pos["posaddress_perc_visibility1"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from across the Street*", $ratings,  $ratings[$pos["posaddress_perc_visibility2"]], NOTNULL);
	}
	else
	{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from across the Street*", $ratings, 0, NOTNULL);
	}

	//$form->add_section("----------------------------------------------------------------------");
	//$form->add_checkbox("checked2", "Address", "", 0, "revised");

	$form->add_hidden("country", param("country"));

	$form->add_section("Website www.tissot.ch");
	
	
	$form->add_checkbox("posaddress_export_to_web", "Show this POS in the Store Locator", "", "", "");
	$form->add_checkbox("posaddress_email_on_web", "Show Email Address in the Store Locator", "", "", "");
	
	$form->add_checkbox("posaddress_offers_free_battery", "POS offers free battery change", "", "", "");

	if(has_access("can_set_collector_card_checkbox"))
	{
		$form->add_checkbox("posaddress_show_in_club_jublee_stores", "Show this POS in the Tissot Club Collector Card Store Locator", "", "", "");
	}
	else
	{
		$form->add_hidden("posaddress_show_in_club_jublee_stores");
		$form->add_checkbox("show_in_club_jublee_stores", "Show this POS in the Tissot Club Collector Card Store Locator", $pos["posaddress_show_in_club_jublee_stores"], DISABLED, "");


	}

	if(has_access("can_set_birthday_vaucher_checkbox"))
	{
		//$form->add_checkbox("posaddress_birthday_vaucher_on_web", "Show this POS in the Tissot Club Birthday Voucher Store Locator", "", "", "");
		$form->add_hidden("posaddress_birthday_vaucher_on_web");
	}
	else
	{
		$form->add_hidden("posaddress_birthday_vaucher_on_web");
		//$form->add_checkbox("birthday_vaucher_on_web", "Show this POS in the Tissot Club Birthday Voucher Store Locator", $pos["posaddress_birthday_vaucher_on_web"], DISABLED, "");
	}
	
	if(has_access("can_set_vip_service_checkbox"))
	{
		$form->add_checkbox("posaddress_vipservice_on_web", "Show 'POS offers VIP Service for Tissot Club Members' in the Store Locator", "", "", "");
	}
	else
	{
		$form->add_hidden("posaddress_vipservice_on_web");
		$form->add_checkbox("vipservice_on_web", "Show 'POS offers VIP Service for Tissot Club Members' in the Store Locator", $pos["posaddress_vipservice_on_web"],  DISABLED, "");
	}
	

	$form->add_section("Address Check");
	$form->add_checkbox("posaddress_checked", "Address check", "", "", "Adress is ok");

	$form->add_button("save_data", "Save");
}
elseif($can_edit == true and has_access("can_edit_his_posindex") and (id() > 0 and $pos["posaddress_store_postype"] != 4))
{
	
	$form->add_section("Roles");
	
	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place) as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_client_id " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	
	$form->add_label("company", "Client", 0, $company);
	
	$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text");
	$form->add_label("posaddress_eprepnr", $ern_caption);
	$form->add_label("posaddress_sapnumber", "SAP Customer Code");

	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place) as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_franchisor_id " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	$form->add_label("franchisor", "Franchisor", 0, $company);


	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place) as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_franchisee_id " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	
	
	if($pos["posaddress_ownertype"] != 6 and $pos['posaddress_store_postype'] != 2)
	{
		$tmp = "Owner Company";
	}
	else
	{
		$tmp = "Owner Company";
	}
	$form->add_label("franchisee", $tmp , 0, $company);


	$form->add_section("Address Data");
	$form->add_edit("posaddress_name", "POS Name*", NOTNULL);
	$form->add_edit("posaddress_name2", "", 0);

	$form->add_hidden("posaddress_address");
	$form->add_multi_edit("street", array("posaddress_street", "posaddress_street_number"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6), array('',''), '', '', '', array(40,5));

	$form->add_edit("posaddress_address2", "Additional Address Info");
	$form->add_edit("posaddress_zip", "Zip", 0);
	
	
	$form->add_list("posaddress_place_id", "City Selection*",
		"select place_id, place_name from places where place_country = " . $pos["posaddress_country"] . " order by place_name", NOTNULL | SUBMIT);

	$form->add_edit("posaddress_place", "City", NOTNULL | DISABLED);
	
	$form->add_lookup("posaddress_country", "Legal Country", "countries", "country_name");

	
	if(count($former_relocated_pos) > 0)
	{
		$link = '<a href="/pos/posindex_pos.php?pos_id=' . $former_relocated_pos["posaddress_id"] . '&country=' . param("country") .'&ltf=' . param("ltf") .'&psc=' . param("psc") .'&ostate=' . param("ostate") .'&province=' . param("province") .'&let=' . param("let") .'" target="_blank">' . $former_relocated_pos["posaddress_name"] . ', ' . $former_relocated_pos["place_name"] . '</a>';
		
		$form->add_section("Relocation Info");
		$form->add_comment("This POS is a relocation of the following POS:");
		$form->add_label("former_relocated_pos", "Relocated POS", RENDER_HTML, $link);
	}
	elseif(count($relocated_pos) > 0)
	{
		$link = '<a href="/pos/posindex_pos.php?pos_id=' . $relocated_pos["posaddress_id"] . '&country=' . param("country") .'&ltf=' . param("ltf") .'&psc=' . param("psc") .'&ostate=' . param("ostate") .'&province=' . param("province") .'&let=' . param("let") .'" target="_blank">' . $relocated_pos["posaddress_name"] . ', ' . $relocated_pos["place_name"] . '</a>';
		
		$form->add_section("Relocation Info");
		$form->add_comment("This POS was relocated to the following POS:");
		$form->add_label("former_relocated_pos", "Relocated POS", RENDER_HTML, $link);
	}
	
	
	$form->add_section("Communication");
	$form->add_hidden("posaddress_phone");
	$form->add_multi_edit("phone_number", array("posaddress_phone_country", "posaddress_phone_area", "posaddress_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


	$form->add_hidden("posaddress_mobile_phone");
	$form->add_multi_edit("mobile_phone_number", array("posaddress_mobile_phone_country", "posaddress_mobile_phone_area", "posaddress_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


	$form->add_edit("posaddress_email", "Email");
	
	/*
	$form->add_edit("posaddress_contact_name", "Contact Name");
	*/
	$form->add_edit("posaddress_website", "Website");
    

	$form->add_section("Dates");
	$form->add_label("store_openingdate", "Opening Date",0, to_system_date($pos["posaddress_store_openingdate"]));
	$form->add_label("planned_store_closingdate", "Planned Closing Date", 0, to_system_date($pos["posaddress_store_planned_closingdate"]));
	$form->add_label("store_closingdate", "Closing Date", 0, to_system_date($pos["posaddress_store_closingdate"]));


	$form->add_section("Environment");
	foreach($posareas as $key=>$name)
	{
		if(array_key_exists($key, $pos_posareas))
		{
			$form->add_checkbox("area". $key, $name, 1, 0, "");
		}
		else
		{
			$form->add_checkbox("area". $key, $name, "", 0, "");
		}

	}

	$form->add_comment("Please indicate on which floor the POS will be situated (Ground floor, Street Level, 1st Floor etc.).");

	$form->add_edit("posaddress_store_floor", "Floor*", NOTNULL,$pos["posaddress_store_floor"], TYPE_CHAR, 30);
	

	$form->add_section("Area Perception");
	$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");
	if(array_key_exists($pos["posaddress_perc_class"], $ratings))
	{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, $ratings[$pos["posaddress_perc_class"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, $ratings[$pos["posaddress_perc_tourist"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings,  $ratings[$pos["posaddress_perc_transport"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings,  $ratings[$pos["posaddress_perc_people"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings,  $ratings[$pos["posaddress_perc_parking"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings,  $ratings[$pos["posaddress_perc_visibility1"]], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from across the Street*", $ratings,  $ratings[$pos["posaddress_perc_visibility2"]], NOTNULL);
	}
	else
	{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from across the Street*", $ratings, 0, NOTNULL);
	}

	//$form->add_section("----------------------------------------------------------------------");
	//$form->add_checkbox("checked2", "Address", "", 0, "revised");

	$form->add_hidden("country", param("country"));

	$form->add_section("Website www.tissot.ch");
	$form->add_checkbox("posaddress_export_to_web", "Show this POS in the Store Locator", "", "", "");
	$form->add_checkbox("posaddress_email_on_web", "Show Email Address in the Store Locator", "", "", "");
	
	$form->add_checkbox("posaddress_offers_free_battery", "POS offers free battery change", "", "", "");

	if(has_access("can_set_collector_card_checkbox"))
	{
		$form->add_checkbox("posaddress_show_in_club_jublee_stores", "Show this POS in the Tissot Club Collector Card Store Locator", "", "", "");
	}
	else
	{
		$form->add_hidden("posaddress_show_in_club_jublee_stores");
		$form->add_checkbox("show_in_club_jublee_stores", "Show this POS in the Tissot Club Collector Card Store Locator", $pos["posaddress_show_in_club_jublee_stores"], DISABLED, "");


	}

	if(has_access("can_set_birthday_vaucher_checkbox"))
	{
		$form->add_hidden("posaddress_birthday_vaucher_on_web");
		//$form->add_checkbox("posaddress_birthday_vaucher_on_web", "Show this POS in the Tissot Club Birthday Voucher Store Locator", "", "", "");
	}
	else
	{
		$form->add_hidden("posaddress_birthday_vaucher_on_web");
		//$form->add_checkbox("birthday_vaucher_on_web", "Show this POS in the Tissot Club Birthday Voucher Store Locator", $pos["posaddress_birthday_vaucher_on_web"], DISABLED, "");
	}
	
	if(has_access("can_set_vip_service_checkbox"))
	{
		$form->add_checkbox("posaddress_vipservice_on_web", "Show 'POS offers VIP Service for Tissot Club Members' in the Store Locator", "", "", "");
	}
	else
	{
		$form->add_hidden("posaddress_vipservice_on_web");
		$form->add_checkbox("vipservice_on_web", "Show 'POS offers VIP Service for Tissot Club Members' in the Store Locator", $pos["posaddress_vipservice_on_web"],  DISABLED, "");
	}


	$form->add_button("save_data", "Save");
}
elseif(has_access("can_view_posindex") or has_access("can_view_his_posindex"))
{
	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place) as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_client_id " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	
	$form->add_label("company", "Client", 0, $company);
	
	$form->add_lookup("posaddress_ownertype", "Legal Type", "project_costtypes", "project_costtype_text");
	$form->add_label("posaddress_eprepnr", $ern_caption);
	$form->add_label("posaddress_sapnumber", "SAP Customer Code");

	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place) as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_franchisor_id " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	$form->add_label("franchisor", "Franchisor", 0, $company);


	$company = "";
	$sql = "select concat(country_name, ': ', address_company, ', ', address_place) as company " . 
		   "from posaddresses " .
		   "left join addresses on address_id = posaddress_franchisee_id " . 
		   "left join countries on country_id = address_country " . 
		   "where posaddress_id = " . param("pos_id");

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$company = $row["company"];
	}
	
	if($pos["posaddress_ownertype"] != 6 and $pos['posaddress_store_postype'] != 2)
	{
		$tmp = "Owner Company";
	}
	else
	{
		$tmp = "Owner Company";
	}
	$form->add_label("franchisee", $tmp, 0, $company);

	$form->add_section("Address Data");
	$form->add_label("posaddress_name", "POS Name");
	$form->add_label("posaddress_name2", "");
	$form->add_label("posaddress_address", "Street");
	$form->add_label("posaddress_address2", "Additional Address Info");
	$form->add_label("posaddress_zip", "Zip");
	$form->add_label("posaddress_place", "City");
	$form->add_lookup("posaddress_country", "Legal Country", "countries", "country_name");

	if(count($former_relocated_pos) > 0)
	{
		$link = '<a href="/pos/posindex_pos.php?pos_id=' . $former_relocated_pos["posaddress_id"] . '&country=' . param("country") .'&ltf=' . param("ltf") .'&psc=' . param("psc") .'&ostate=' . param("ostate") .'&province=' . param("province") .'&let=' . param("let") .'" target="_blank">' . $former_relocated_pos["posaddress_name"] . ', ' . $former_relocated_pos["place_name"] . '</a>';
		
		$form->add_section("Relocation Info");
		$form->add_comment("This POS is a relocation of the following POS:");
		$form->add_label("former_relocated_pos", "Relocated POS", RENDER_HTML, $link);
	}
	elseif(count($relocated_pos) > 0)
	{
		$link = '<a href="/pos/posindex_pos.php?pos_id=' . $relocated_pos["posaddress_id"] . '&country=' . param("country") .'&ltf=' . param("ltf") .'&psc=' . param("psc") .'&ostate=' . param("ostate") .'&province=' . param("province") .'&let=' . param("let") .'" target="_blank">' . $relocated_pos["posaddress_name"] . ', ' . $relocated_pos["place_name"] . '</a>';
		
		$form->add_section("Relocation Info");
		$form->add_comment("This POS was relocated to the following POS:");
		$form->add_label("former_relocated_pos", "Relocated POS", RENDER_HTML, $link);
	}

	$form->add_section("Communication");
	$form->add_label("posaddress_phone", "Phone");
	$form->add_label("posaddress_mobile_phone", "Mobile");
	$form->add_label("posaddress_email", "Email");
	
	
	/*
	$form->add_label("posaddress_contact_name", "Contact Name");
	*/
	$form->add_label("posaddress_website", "Website");
    

	$form->add_section("Dates");
	$form->add_label("store_openingdate", "Opening Date",0, to_system_date($pos["posaddress_store_openingdate"]));
	$form->add_label("planned_store_closingdate", "Planned Closing Date", 0, to_system_date($pos["posaddress_store_planned_closingdate"]));
	$form->add_label("store_closingdate", "Closing Date", 0, to_system_date($pos["posaddress_store_closingdate"]));


	$form->add_section("Environment");
	foreach($posareas as $key=>$name)
	{
		if(array_key_exists($key, $pos_posareas))
		{
			$form->add_checkbox("area". $key, $name, 1, 0, "");
		}
		else
		{
			$form->add_checkbox("area". $key, $name, "", 0, "");
		}

	}

	$form->add_label("posaddress_store_floor", "Floor");
	$form->add_hidden("country", param("country"));

	$form->add_section("Area Perception");
	
	$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from across the Street*", $ratings, 0, NOTNULL);


	$form->add_section("Website www.tissot.ch");
	
	$form->add_hidden("posaddress_export_to_web");
		$form->add_checkbox("export_to_web", "Show this POS in the Store Locator", $pos["posaddress_export_to_web"], DISABLED, "");

	$form->add_hidden("posaddress_email_on_web");
		$form->add_checkbox("email_on_web", "Show Email Address in the Store Locator", $pos["posaddress_email_on_web"], DISABLED, "");

	$form->add_hidden("posaddress_offers_free_battery");
		$form->add_checkbox("offers_free_battery", "POS offers free battery change", $pos["posaddress_show_in_club_jublee_stores"], DISABLED, "");

	if(has_access("can_set_collector_card_checkbox"))
	{
		$form->add_checkbox("posaddress_show_in_club_jublee_stores", "Show this POS in the Tissot Club Collector Card Store Locator", "", "", "");
	}
	else
	{
		$form->add_hidden("posaddress_show_in_club_jublee_stores");
		$form->add_checkbox("show_in_club_jublee_stores", "Show this POS in the Tissot Club Collector Card Store Locator", $pos["posaddress_show_in_club_jublee_stores"], DISABLED, "");


	}

	if(has_access("can_set_birthday_vaucher_checkbox"))
	{
		$form->add_hidden("posaddress_birthday_vaucher_on_web");
		//$form->add_checkbox("posaddress_birthday_vaucher_on_web", "Show this POS in the Tissot Club Birthday Voucher Store Locator", "", "", "");
	}
	else
	{
		$form->add_hidden("posaddress_birthday_vaucher_on_web");
		//$form->add_checkbox("birthday_vaucher_on_web", "Show this POS in the Tissot Club Birthday Voucher Store Locator", $pos["posaddress_birthday_vaucher_on_web"], DISABLED, "");
	}
	
	if(has_access("can_set_vip_service_checkbox"))
	{
		$form->add_checkbox("posaddress_vipservice_on_web", "Show 'POS offers VIP Service for Tissot Club Members' in the Store Locator", "", "", "");
	}
	else
	{
		$form->add_hidden("posaddress_vipservice_on_web");
		$form->add_checkbox("vipservice_on_web", "Show 'POS offers VIP Service for Tissot Club Members' in the Store Locator", $pos["posaddress_vipservice_on_web"],  DISABLED, "");
	}

	if(has_access("can_set_birthday_vaucher_checkbox")
		or has_access("can_set_birthday_vaucher_checkbox")
		or has_access("can_set_birthday_vaucher_checkbox")
		)
	{
		$form->add_button("save_check_boxes", "Save");
	}

}
$form->add_hidden("let", param("let"));
$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("psc", param("psc"));
$form->add_hidden("province",  param("province"));
$form->add_hidden("ostate", param("ostate"));
$form->add_hidden("province",  param("province"));
$form->add_button("back", "Back to POS List");

if(has_access("can_edit_catalog"))
{
	$form->add_button("delete", "Delete POS from POS List");
}

// Populate form and process button clicks

$form->populate();
$form->process();


if($form->button("save_check_boxes"))
{
	$sql_u = "update posaddresses set " . 
		     "posaddress_birthday_vaucher_on_web =  " . dbquote($form->value("posaddress_birthday_vaucher_on_web")) . ", " . 
	         "posaddress_vipservice_on_web =  " . dbquote($form->value("posaddress_vipservice_on_web")) . ", " . 
			 "posaddress_show_in_club_jublee_stores =  " . dbquote($form->value("posaddress_show_in_club_jublee_stores")) . 
		     " where posaddress_id =  " . dbquote(id()); 
	
	$result = mysql_query($sql_u) or dberror($sql_u);
	if(id())
	{
		update_store_locator(id());
		mysql_select_db(RETAILNET_DB, $db);
	}
}
elseif($form->button("save_data"))
{
	
	
	
	
	
	$error = 0;
	
	$form->value("posaddress_phone", $form->unify_multi_edit_field($form->items["phone_number"]));
	$form->value("posaddress_mobile_phone", $form->unify_multi_edit_field($form->items["mobile_phone_number"]));

	//$form->add_validation("{posaddress_phone} != '' or {posaddress_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");
		

	if($form->validate())
	{
		
		$form->value("posaddress_address", $form->unify_multi_edit_field($form->items["street"], get_country_street_number_rule($form->value("posaddress_country"))));
		
		$form->save();
		
		//update posareas
		$sql = "delete from posareas where posarea_posaddress =" . dbquote($pos["posaddress_id"]);
		$result = $res = mysql_query($sql) or dberror($sql);
		foreach($posareas as $key=>$name)
		{
			if($form->value("area" . $key) == 1)
			{
				$fields = array();
				$values = array();

				$fields[] = "posarea_posaddress";
				$values[] = dbquote($pos["posaddress_id"]);

				$fields[] = "posarea_area";
				$values[] = dbquote($key);

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());

				$sql = "insert into posareas (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
			}
		}

		//update projects connected to the pos
		$sql = "select posorder_order from posorders " . 
			   "where posorder_order > 0 and posorder_type = 1 " . 
			   " and posorder_posaddress = " . id();
		
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$sql_u = "update projects " . 
				     "set project_floor = " . dbquote($form->value('posaddress_store_floor')) . 
				     " where project_order = " . $row["posorder_order"];

			$result = mysql_query($sql_u) or dberror($sql_u);
		}

		
		//check if google map is created
		$sql = "select posaddress_google_precision from posaddresses where posaddress_id = " . id();
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if(!$row["posaddress_google_precision"])
			{
				$result = google_maps_geo_encode_pos(id(), $context);
			}
		}

		if(id())
		{
			update_store_locator(id());
			mysql_select_db(RETAILNET_DB, $db);
		}

		$result = update_pos_reporting_number(id());
		
		
		param('pos_id', id());
		$pos = get_poslocation( id(), "posaddresses");

	}
}
elseif($form->button("back"))
{
	redirect("posindex.php?country=" . param("country") . '&ltf=' . param("ltf") . '&psc=' . param("psc") . '&ostate=' . param("ostate"). '&province=' . param("province"). '&let=' . param("let"));
}
elseif($form->button("delete"))
{
	redirect("posindex_pos_delete.php?pos_id=" . id() . "&country=" . param("country"));	
}
elseif($form->button("posaddress_place_id"))
{
	$sql = "select place_name " .
		   "from places " . 
		   "where place_id = " . dbquote($form->value("posaddress_place_id"));
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->value("posaddress_place", $row["place_name"]);
	}
}
elseif($form->button("posaddress_country"))
{
	$form->value("posaddress_place",  "");
	$form->value("posaddress_place_id", 0);
}


// Render page
$poslocation = get_poslocation(id(), "posaddresses");

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title(id() ? "Basic Data: " . $poslocation["posaddress_name"] : "Add POS Location");

if(id())
{
	require_once("include/tabs.php");
}

$form->render();


echo "<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>";


?>


<script language="javascript">
	$("#h_posaddress_name").click(function() {
	   $('#posaddress_name').val($('#posaddress_name').val().toLowerCase());
	   var txt = $('#posaddress_name').val();

	   $('#posaddress_name').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_posaddress_name2").click(function() {
	   $('#posaddress_name2').val($('#posaddress_name2').val().toLowerCase());
	   var txt = $('#posaddress_name2').val();

	   $('#posaddress_name2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_posaddress_address").click(function() {
	   $('#posaddress_street').val($('#posaddress_street').val().toLowerCase());
	   var txt = $('#posaddress_street').val();
	   $('#posaddress_street').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_posaddress_address2").click(function() {
	   $('#posaddress_address2').val($('#posaddress_address2').val().toLowerCase());
	   var txt = $('#posaddress_address2').val();

	   $('#posaddress_address2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});



	
</script>
<?php
$page->footer();

?>