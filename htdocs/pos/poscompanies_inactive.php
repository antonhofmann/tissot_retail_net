<?php
/********************************************************************

    addresses_inactive.php

    Lists of addresses (company addresses)

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_use_posindex");
check_access("can_edit_posindex");

set_referer("poscompany.php");


$preselect_filter = "";
if( param("country") > 0)
{
	$country =  param("country");
	$preselect_filter = " and address_country = " . $country;
}
elseif(array_key_exists("country", $_SESSION) and $_SESSION["country"] > 0)
{
	$country = $_SESSION["country"];
	unset($_SESSION["country"]);
	$preselect_filter = " and address_country = " . $country;
}




/********************************************************************
    Create Form
*********************************************************************/
$address_filter = array();
$address_filter["a"] = "All";
$address_filter["f"] = "Franchisees";
$address_filter["o"] = "Independent retailers";

$form = new Form("addresses", "address");

$form->add_section("List Filter Selection");
$form->add_hidden("country", $country);

if(param("address_filter"))
{
	$form->add_list("address_filter", "Address Filter", $address_filter, SUBMIT | NOTNULL, param("address_filter"));
	$_SESSION["address_filter"] = param("address_filter");
}
elseif(array_key_exists("address_filter", $_SESSION) and $_SESSION["address_filter"] != '')
{
	$form->add_list("address_filter", "Address Filter", $address_filter, SUBMIT | NOTNULL, $_SESSION["address_filter"]);
}
else
{
	$form->add_list("address_filter", "Address Filter", $address_filter, SUBMIT | NOTNULL, "f");
	$_SESSION["address_filter"] = 'f';
}

/********************************************************************
    Create List
*********************************************************************/
if(param('address_filter')) {
	
	if(param('address_filter') == 'f') {
		$list_filter = "address_showinposindex = 1 and address_active <> 1 and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1)" . $preselect_filter;
	}
	elseif(param('address_filter') == 'o') {
		$list_filter = "address_showinposindex = 1 and address_active <> 1 and address_is_independent_retailer = 1 " . $preselect_filter;
	}
	else {
		$list_filter = "address_showinposindex = 1 and address_active <> 1 " . $preselect_filter;
	}
}
elseif(array_key_exists("address_filter", $_SESSION) and $_SESSION["address_filter"] != '')
{
	
	if($_SESSION["address_filter"] == 'f') {
		$list_filter = "address_showinposindex = 1 and address_active <> 1 and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1)" . $preselect_filter;
	}
	elseif($_SESSION["address_filter"] == 'o') {
		$list_filter = "address_showinposindex = 1 and address_active <> 1 and address_is_independent_retailer = 1 " . $preselect_filter;
	}
	else {
		$list_filter = "address_showinposindex = 1 and address_active <> 1 " . $preselect_filter;
	}
}
else
{
	$list_filter = "address_showinposindex = 1 and address_active <> 1)" . $preselect_filter;
}



//compose list
$list = new ListView("select address_id, address_company, address_zip, " .
                     "    address_place, country_name, address_type_name, province_canton " .
                     "from addresses " . 
					 " left join address_types on address_type_id = address_type " .
					 "left join countries on address_country = country_id " . 
					 "left join places on place_id = address_place_id " .
					 "left join provinces on province_id = place_province");

$list->set_entity("addresses");
$list->set_order("country_name, address_company");
$list->set_filter($list_filter);

$list->add_column("country_name", "Country", "", LIST_FILTER_LIST,
    "select country_name from countries order by country_name");
$list->add_column("address_type_name", "Type", "", LIST_FILTER_LIST, "select address_type_name from address_types where (address_type_id = 7 or address_type_id = 1) order by address_type_name");
$list->add_column("address_company", "Company", "poscompany.php", LIST_FILTER_FREE);
$list->add_column("province_canton", "Province", "", LIST_FILTER_FREE);
$list->add_column("address_zip", "Zip", "", LIST_FILTER_FREE);
$list->add_column("address_place", "City", "", LIST_FILTER_FREE);


$list->process();

$page = new Page("poscompanies_inactive");
require "include/pos_page_actions.php";
$page->header();

$page->title("Companies Inactive");
$form->render();
$list->render();


$page->footer();

?>
