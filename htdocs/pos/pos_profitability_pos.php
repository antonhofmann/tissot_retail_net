<?php
/********************************************************************

    pos_profitability_pos.php

    List Profitability data of POS Location

    created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-10-20
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-10-20
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("has_access_to_pos_profitability");


if(!has_access("has_access_to_pos_profitability") 
	and !has_access("can_edit_pos_profitability_of_his_pos")
	and !has_access("can_edit_pos_profitability_of_all_pos")
	and !has_access("can_view_pos_profitability_of_his_pos")
	and !has_access("can_view_pos_profitability_of_all_pos"))
{
	redirect("/pos");
}

$pos_data = get_poslocation(param("pos_id"), "posaddresses");


$pos_link = '<a href="posindex_pos.php?country=' . $pos_data["posaddress_country"] .'&ltf=all&ostate=1&province=0&let=0&id=' . $pos_data["posaddress_id"] . '" target="_blank">' . $pos_data["posaddress_name"] . '</a>';


/********************************************************************
    Create Form
*********************************************************************/

$form = new Form("possellouts", "possellouts");

$form->add_hidden("country", param("country"));
$form->add_hidden("province", param("province"));
$form->add_hidden("ostate", param("ostate"));
$form->add_hidden("pos_id", param("pos_id"));
$form->add_hidden("ltype", param("ltype"));

$form->add_label("posaddress_name", "POS Name", RENDER_HTML, $pos_link);
$form->add_label("posaddress_address", "Street", 0, $pos_data["posaddress_address"]);
$form->add_label("posaddress_place", "City", 0, $pos_data["place_name"]);
$form->add_label("posaddress_place", "City", 0, $pos_data["country_name"]);
$form->add_label("posaddress_store_openingdate", "Opening Date", 0, to_system_date($pos_data["posaddress_store_openingdate"]));
$form->add_label("posaddress_store_closingdate", "Closing Date", 0, to_system_date($pos_data["posaddress_store_closingdate"]));


$form->populate();

/********************************************************************
    Create List
*********************************************************************/
$sql = "select * from possellouts ";
$list_filter = "possellout_posaddress_id = " . param("pos_id");

$link = "pos_profitability_pos_edit.php?country=" . param("country") . '&province=' . param("province") . '&ltf=' . param("ltf"). '&ostate=' . param("ostate") . '&pos_id=' . param("pos_id");


$list = new ListView($sql);

$list->set_title("");
$list->set_entity("possellouts");
$list->set_filter($list_filter);
$list->set_order("possellout_year DESC");

$list->add_hidden("country", param("country"));
$list->add_hidden("province", param("province"));
$list->add_hidden("ostate", param("ostate"));
$list->add_hidden("ltf", param("ltf"));
$list->add_hidden("pos_id", param("pos_id"));

$list->add_column("possellout_year", "Year", $link, 0 ,'','', COLUMN_NO_WRAP);
$list->add_column("possellout_month", "Months", "", 0,'', COLUMN_NO_WRAP);
$list->add_column("possellout_watches_units", "Watches", "", "", "",COLUMN_BREAK |  COLUMN_ALIGN_RIGHT);
$list->add_column("possellout_bijoux_units", "Watch Straps", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("possellout_grossales", "Gross Sales", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("possellout_net_sales", "Net Sales", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("possellout_grossmargin", "Gross Margin", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("possellout_operating_expenses", "Operating\nExpenses", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("possellout_operating_income_excl", "Operating Income\nexcl. WS", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("possellout_wholsale_margin", "Whole Sale\nMargin %", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("possellout_operating_income_incl", "Operating Income\nincl. WS", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);


if(has_access("can_edit_pos_profitability_of_his_pos")
	or has_access("can_edit_pos_profitability_of_all_pos"))
{
	$list->add_button("add","Add New Year");
}
$list->add_button("back","Back");

$list->populate();
$list->process();


if($list->button("back"))
{
	$link = "pos_profitability.php?country=" . param("country") . '&province=' . param("province") . '&ltf=' . param("ltf"). '&ostate=' . param("ostate"). '&ltype=' . param("ltype");
	redirect($link);
}
elseif($list->button("add"))
{
		$link = "pos_profitability_pos_edit.php?country=" . param("country") . '&province=' . param("province") . '&ltf=' . param("ltf"). '&ostate=' . param("ostate") . '&pos_id=' . param("pos_id"). '&ltype=' . param("ltype");
		redirect($link);

}

$page = new Page("posaddresses");
require "include/pos_page_actions.php";
$page->header();

$page->title("POS Index: POS yearly sellout");

$form->render();
$list->render();

$page->footer();

?>
