<?php
/********************************************************************

    posopeninghr.php

    Data Entry for POS Opening Hours.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-01-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-01-03
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/


	require_once "../include/frame.php";
	require_once "../shared/func_posindex.php";
	require_once "include/get_functions.php";
	
		
	error_reporting(E_ERROR | E_WARNING);
	
	if(!has_access("can_edit_pos_opening_hours") and !has_access("can_view_pos_opening_hours")) {
		redirect("/pos");
	}
	
	$pos_data = get_poslocation(id());
	$posname = $pos_data["country_name"] . ", " . $pos_data["posaddress_name"];

	// pos header
	$form = new Form("posaddresses", "posaddresses");
	$form->add_label("posname", "POS Location", 0, $posname);
	$form->populate();

	$__needs_form = false;
	
	
	
	$pos = id();
	
	// get auto increment value
	$result = mysql_fetch_assoc(mysql_query("
		SHOW TABLE STATUS LIKE 'posopeninghrs'
	"));
	
	// set new datagrid increment 
	$increment = $result['Auto_increment'];
	
	// show timeformat 24 hours
	$timeformat = $pos_data["country_timeformat"];
	
	// set grid as readonly
	$readonly = (has_access("can_edit_pos_opening_hours")) ? false : true;
	
	// general closed days
	$result = mysql_fetch_assoc(mysql_query("
		SELECT posclosinghr_text 
		FROM posclosinghrs 
		WHERE posclosinghr_posaddress_id = $pos
	"));
	
	$posclosinghr_text = $result['posclosinghr_text'];
	
	$form1 = new Form("posclosinghrs", "posclosinghrs");

	$form1->add_hidden("country", param("country"));
	$form1->add_hidden("province", param("province"));
	$form1->add_hidden("ltf", param("ltf"));
	$form1->add_hidden("psc", param("psc"));



	$form1->add_section("Days Closed in General");
	$form1->add_comment("Please indicate the days on which the POS is closed in general like e.g. Sundays, New Year, 24th of December to 3rd of January or similar information.");
	$form1->add_multiline("days", "Days Closed", 8, 0, $posclosinghr_text);
	
	if(has_access("can_edit_pos_opening_hours")) {
		
		$form1->add_button("save", "Save", "http://");
	}
	
	if(array_key_exists("", $_GET))
	{
		$link = "posindex_openinghr.php?pos_id=" . $_GET["id"] . "&country=" . $_GET["country"] . "&ltf=" . $_GET["ltf"] . "&ostate=" . $_GET["ostate"]. '&province=' . $_GET["province"] . '&psc=' . $_GET["psc"];
		$form1->add_button("back", "Back", $link);
	}
	else
	{
		$form1->add_button("back", "Back", "http://");
	}
	$form1->populate();
	$form1->process();


	if($form->button("save"))
	{
		$sql = "update posclosinghrs set posclosinghr_text = " . dbquote($form1->value("days")) . 
		       "where posclosinghr_posaddress_id = " . id();
		
		$res = mysql_query($sql) or dberror($sql);
	
	}


	// page builder
	$page = new Page("posaddresses");
	require "include/pos_page_actions.php";
	$page->header();
	
	$page->title("Edit Opening Hours");
	$form->render();
	

?>
<link rel='stylesheet' type='text/css' href='/public/scripts/jquery/jquery.ui.blue.css' />
<script type='text/javascript' src="/public/scripts/jquery/jquery.ui.js"></script>

<link rel='stylesheet' type='text/css' href='/public/scripts/weekcalendar/weekcalendar.css' />
<script type='text/javascript' src='/public/scripts/weekcalendar/weekcalendar.js'></script>

<link rel="stylesheet" type="text/css" href="/public/scripts/jgrowl/jgrowl.css" />
<script type="text/javascript" src="/public/scripts/jgrowl/jgrowl.js"  ></script>

<link rel="stylesheet" type="text/css" href="/public/scripts/popover/popover.css" />
<script type="text/javascript" src="/public/scripts/popover/popover.js"  ></script>

<link rel="stylesheet" type="text/css" href="/public/scripts/fancybox/fancybox.css" />
<script type="text/javascript" src="/public/scripts/fancybox/fancybox.js"  ></script>

<script type='text/javascript'>
	$(document).ready(function() {

		var $calendar = $('#calendar'),										// calendar instance
			increment = $('#increment'),									// current opening hours auto inrement value
			pos = $('#id').val(),											// pos id
			use24Hour = ( $('#timeformat').val() > 12 ) ? true : false,		// 24 hour time format
			timeFormat = (use24Hour) ? 'H:i' : 'h:i a',						// time format pattern
			readonly = ($('#readonly').val()) ? true : false,				// grid readonly
			copyfrom = $('#copyfrom'),										// popover box
			loaderTrigger = $('#loaderTrigger');							// dropdown loader trigger

		// calendar builder
		$calendar.weekCalendar({
			date : new Date(2012,0,2), // fix calendar on first monday in year 2012
			timeslotsPerHour : 2,
			timeslotHeight : 14,
			businessHours : {
				start : 0,
				end : 24,
				limitDisplay : false
			},
			daysToShow : 7,
			readonly: readonly,
			use24Hour : use24Hour,
			timeFormat: timeFormat,
			dateFormat: false, 
			allowCalEventOverlap: true,
			showHeader: false,
			firstDayOfWeek: 1,
			textSize: 11,
			newEventText: 'New Opening Time',
			height : function($calendar) {
				return 720;
			},
			draggable : function(calEvent, $event) {
				return calEvent.readOnly != true;
			},
			resizable : function(calEvent, $event) { 
				
				return calEvent.readOnly != true;
			},
			eventNew : function(calEvent, $event) {
	
				var $dialogContent = $("#event_edit_container");
	
				resetForm($dialogContent);
	
				var startField = $dialogContent.find("select[name='start']").val(calEvent.start);
				var endField = $dialogContent.find("select[name='end']").val(calEvent.end);
				
				checkboxes(calEvent);
				
				$dialogContent.dialog({
					modal : true,
					width: 320,
					resizable: false,
					title : "New Opening Time",
					close : function() {
						$dialogContent.dialog("destroy");
						$dialogContent.hide();
						$('#calendar').weekCalendar("removeUnsavedEvents");
					},
					buttons : {
						save : function() {
							
							calEvent.id = increment.val();
							calEvent.start = new Date(startField.val()); 
							calEvent.end = new Date(endField.val());

							var id = calEvent.id+1;
							increment.val(id);
							
							//post to data.php
							setData('save',calEvent);
	
							$calendar.weekCalendar("removeUnsavedEvents");
							$calendar.weekCalendar("updateEvent",calEvent);
							$dialogContent.dialog("close");
						},
						cancel : function() {
							$dialogContent.dialog("close");
						}
					}
				}).show();
	
				$dialogContent.find(".date_holder").text($calendar.weekCalendar("formatDate",calEvent.start));
				setupStartAndEndTimeFields(startField,endField, calEvent, $calendar.weekCalendar("getTimeslotTimes",calEvent.start));
	
			},
			eventDrop : function(calEvent, $event) { 
				$('input[name^=day]').attr('checked',false).attr('disabled',true);
				setData('save', calEvent);
			},
			eventResize : function(calEvent, $event) { 
				$('input[name^=day]').attr('checked',false).attr('disabled',true);
				setData('save', calEvent);
			},
			eventClick : function(calEvent, $event) { 
	
				if (readonly) { 
					return;
				}
	
				var $dialogContent = $("#event_edit_container");
				
				resetForm($dialogContent);
				
				var startField = $dialogContent.find("select[name='start']").val(calEvent.start);
				var endField = $dialogContent.find("select[name='end']").val(calEvent.end);

				//$('.dayboxes').hide();
				//$('input[name^=day]').attr('checked',false).attr('disabled',true);
				
				checkboxes(calEvent, 'edit');
	
				$dialogContent.dialog({
					modal : true,
					width: 320,
					resizable: false,
					title : "Edit Opening Time",
					close : function() {
						$dialogContent.dialog("destroy");
						$dialogContent.hide();
						$('#calendar').weekCalendar("removeUnsavedEvents");
					},
					buttons : {
						save : function() {
	
							calEvent.start = new Date(startField.val());
							calEvent.end = new Date(endField.val());
	
							setData('save', calEvent);
							
							$calendar.weekCalendar("updateEvent",calEvent);
							$dialogContent.dialog("close");
						},
						"delete" : function() {
							setData('delete', calEvent);
							$calendar.weekCalendar("removeEvent",calEvent.id);
							$dialogContent.dialog("close");
						},
						cancel : function() {
							$dialogContent.dialog("close");
						}
					}
				}).show();
	
				var startField = $dialogContent.find("select[name='start']").val(calEvent.start);
				var endField = $dialogContent.find("select[name='end']").val(calEvent.end);
				
				$dialogContent.find(".date_holder").text($calendar.weekCalendar("formatDate",calEvent.start));
				setupStartAndEndTimeFields(startField,endField, calEvent, $calendar.weekCalendar("getTimeslotTimes",calEvent.start));
				$(window).resize().resize();
	
			},
			eventMouseover : function(calEvent, $event) {
			},
			eventMouseout : function(calEvent, $event) {
			},
			noEvents : function() {
			}, 
			data : function(start, end, callback) {
				$.getJSON('/applications/helpers/pos.openinghours.php', {action:'load', pos:pos }, function(json) { 
					callback(buildEvents(json));
				});
		    }
		});

		function buildEvents(json) {

			var events = [];
			
			$(json).each(function( i, data ) {

				var startDate = new Date(data.start);
				var endDate = new Date(data.end);
				var obj = {};

				obj.id = data.id;
				obj.start = data.start + startDate.getTimezoneOffset()*60*1000;
				obj.end = data.end + endDate.getTimezoneOffset()*60*1000;
				events.push(obj);
			});

			return events;
		}
	
		function resetForm($dialogContent) {
			$dialogContent.find("input").val("");
			$dialogContent.find("textarea").val("");
			$dialogContent.find("select[name='start']").empty();
			$dialogContent.find("select[name='end']").empty();
		}
	
		function setupStartAndEndTimeFields($startTimeField, $endTimeField, calEvent, timeslotTimes) {
	
			for ( var i = 0; i < timeslotTimes.length; i++) {
				
				if (timeslotTimes[i]) { 

					var startTime = timeslotTimes[i].start;
					var endTime = timeslotTimes[i].end;
					var startSelected = "";

					if (startTime.getTime() === calEvent.start.getTime()) {
						startSelected = "selected=\"selected\"";
					}
					var endSelected = "";
	
					if (endTime.getTime() === calEvent.end.getTime()) {
						endSelected = "selected=\"selected\"";
					}

					$startTimeField.append("<option value=\""
						+ startTime + "\" " + startSelected + ">"
						+ timeslotTimes[i].startFormatted
						+ "</option>");
	
					$endTimeField.append("<option value=\"" + endTime
						+ "\" " + endSelected + ">"
						+ timeslotTimes[i].endFormatted
						+ "</option>");
				}
			}
			
			$endTimeOptions = $endTimeField.find("option");
			$startTimeField.trigger("change");
		}
		
		function setData(action, calEvent) {
		
			var data = $('input[name^=day]').serializeArray();
			data.push({name: 'action', value: action });
			data.push({name: 'pos', value: pos });

			if (calEvent) {	
				var start = calEvent.start.getTime()/1000 - calEvent.start.getTimezoneOffset()*60;
				var end = calEvent.end.getTime()/1000 - calEvent.end.getTimezoneOffset()*60;
				data.push({name: 'id', value: calEvent.id });
				data.push({name: 'start', value: start  });
				data.push({name: 'end', value: end });
			}
			
			$.ajax({
				dataType: "json",
				url: "/applications/helpers/pos.openinghours.php",
				data: data,
				success: function(json) {
					if (json) {

						if (json.events) { 

							var events = buildEvents(json.events);
							
							$(events).each(function( i, data ) { console.log(data);
								$calendar.weekCalendar("updateEvent", {
									id: data.id,
									start: data.start,
									end: data.end
								});
							});

							$calendar.weekCalendar('refresh');
						}
						
						if (json.last_id) {
							increment.val(json.last_id);
						}
						
						if (json.message) {
							$.jGrowl(json.message, { 
								sticky: false, 
								theme: 'message'
							});
						}
						
						if (json.closing_days) {
							$('#days_closed').val(json.closing_days);
						}
					}
				}
			});
		}
		
		function checkboxes(calEvent, mod) { 
			
			var data = {
				action: 'getdays',
				pos: pos,
				mod: mod,
				start: calEvent.start.getTime()/1000 - calEvent.start.getTimezoneOffset()*60,
				end: calEvent.end.getTime()/1000 - calEvent.end.getTimezoneOffset()*60
			}

			$('.dayboxes').show();
			$('input[name^=day]').attr('checked',false).attr('disabled',false);
			
			$.getJSON('/applications/helpers/pos.openinghours.php', data, function(json) { 
				
				if (json) {
					for (var day in json) {
						$(json[day]).each(function( i, obj ) {
							$('input[name="day['+day+']"]')
								.attr('checked', obj.checked)
								.attr('disabled', obj.disabled);
						});
					}
				}
			});
		}
	
		var $endTimeField = $("select[name='end']");
		var $endTimeOptions = $endTimeField.find("option");
	
		// reduces the end time options to be only after the start
		// time options.
		$("select[name='start']").change(function() {
		
			var startTime = $(this).find(":selected").val();
			var currentEndTime = $endTimeField.find("option:selected").val();

			var start = new Date(startTime);
			var end = new Date(currentEndTime);
			
			$endTimeField.html($endTimeOptions.filter(function() {
				var newTime = new Date($(this).val());
				//console.log(start.toTimeString(), end.toTimeString(), newTime.toTimeString());
				//return startTime < $(this).val();
				return start.getTime() <= newTime.getTime();
			}));
	
			var endTimeSelected = false;
			//console.log('Start Selected: ', startTime, ' End Selected: ', currentEndTime);
			$endTimeField.find("option").each(function() { 

				var newTime = new Date($(this).val());

				if (newTime.getTime() === end.getTime()) {
					$(this).attr("selected","selected");
					endTimeSelected = true;
					return false;
				}
			});
	
			if (!endTimeSelected) {
				// automatically select an end date
				// 2 slots away.
				$endTimeField.find("option:eq(1)").attr("selected","selected");
			}
	
		});
		
		$('#save').click(function(e) {

			e.preventDefault();
			e.stopImmediatePropagation();
			
			var days = $('#days').val();

			$.getJSON('/applications/helpers/pos.closed.days.php', {
				days: days,
				pos: pos
			}, function(json) {
				if (json) { 
					$.jGrowl(json.message, { 
						sticky: (json.response) ? false : true, 
						theme: (json.response) ? 'message' : 'error'
					});
				}
			});
			
			return false;
		});

		
		$('#back').click(function(e) { 
			e.preventDefault();
			var country = $('#country').val();
			var province = $('#province').val();
			var ltf = $('#ltf').val();
			var psc = $('#psc').val();
			window.location = 'posopeninghrs.php?country='+country+'&province='+province+'&ltf='+ltf+'&psc='+psc;
			return false;
		});
		
		
		var filter = $("#filter");
		var content = $('#filter-form').html(); 
		
		$('#filter-form').remove();
		
		filter.popover({
			title: "POS",
			content: content
		});
			
		filter.click(function(event) {
			event.preventDefault();
			event.stopPropagation();
			filter.popover('show');
		});
		
		$('.filters select').change(function() {
			
			var id = $(this).attr('id');
			
			if (id=='pos') {
				
				if ($(this).val()) {
					
					$calendar.weekCalendar("clear");
					$('#days').val('');
					
					$.ajax({
						dataType: "json",
						url: "/applications/helpers/pos.openinghours.php",
						data: {
							action: 'copy',
							pos: $('#id').val(),
							id: $(this).val()
						},
						success: function(json) { 
							if (json) {
								
								if (json.events) {
									$(json.events).each(function( i, data ) {
										$calendar.weekCalendar("updateEvent", {
											id: data.id,
											start: data.start,
											end: data.end
										});
									});
								}
								
								if (json.last_id) {
									increment.val(json.last_id);
								}
								
								if (json.closing_days) {
									$('#days').val(json.closing_days);
								}
								
								if (json.message) {
									$.jGrowl(json.message, { 
										sticky: false, 
										theme: 'message'
									});
								}
										
								$("#filter").popover('hide');
								$('.filters select').val('');
								$('#elem').val('').trigger('change');
							}
						}
					});
				}
				
			} else {
				$('#elem').val(id).trigger('change');
			}
		});
		
		$('#elem').change(function(even) { 
		
			var self = $(this); 
			
			$('select.filter').each(function(i, elem) { 
				
				var id = $(elem).attr('id');
				var selected = $(elem).val();  
				
				if (id != self.val()) {
					
					var data = $('#advanced').serializeArray()
					data.push({ name: "section", value: id });
					
					$.ajax({
						url: '/applications/helpers/ajax.opening.hours.php',
						dataType: 'json',
						data: data,
						success: function(json) {
							
							$(elem).empty();
							
							if (json) {
								for (i = 0; i < json.length; i++) {  
									for ( key in json[i] ) {	
										$(elem).append("<option value="+key+">"+json[i][key]+"</option>");
									}
								}
							}
							
							if (selected) {
								$(elem).val(selected);
							}
						}
					});
				}
			});
			
		}).trigger('change');
		
		$('.dialog').click(function(event) {
			var button = $(this);
			var dialog = '#delete_dialog';
			event.preventDefault();
			$.fancybox({ 
				'href': dialog,
				'autoDimensions':false,
				'width': $(dialog).width(),
				'height': $(dialog).height(),
				'autoScale':false,
				'transitionIn': 'elastic',
				'transitionOut': 'elastic',
				'showCloseButton': false,
				'hideOnOverlayClick': false,
				'scrolling' : false,
				'titleShow' : false,
				'padding' : 0,
				'onComplete':	function() {
					$(dialog).show();
				},
				'onClosed': function() {
					$(dialog).hide();
				}
			});
		});

		$('.-cancel').bind('click', function(event) {
			event.preventDefault();
			$.fancybox.close();
			return false;
		});	

		$('.-apply').bind('click', function(event) {
			event.preventDefault();
			
			$.ajax({
				dataType: "json",
				url: "/applications/helpers/pos.openinghours.php",
				data: {
					action: 'deleteall',
					pos: $('#id').val()
				},
				success: function(json) { 
					if (json) {
						if (json.message) {
							$.jGrowl(json.message, { 
								sticky: false, 
								theme: 'message'
							});
						}
						$('#days').val('');
					}
				}
			});
			
			$calendar.weekCalendar("clear");
			$.fancybox.close();
		})
	});
	
	// table rollover
	$(document).delegate("tbody tr", "mouseover", function() {
        $(this).removeClass("-over");
    });
        
</script>
<style type="text/css">

	.oh-wrapper {
		width: 800px;
		margin: 20px 0;
	}

	#calendar {
		width: 800px;
		height: 700px;
		overflow: hidden;
	}
	.wc-time-header-cell,
	.wc-day-column-header {
		font-size: 12px;
	}
	
	.wc-scrollbar-shim {
		display: none;
	}
	
	.util {
		display: none;
	}
	
	#calendar table {
		border-style: solid;
		border-color: silver;
		border-width: 1px 1px 0;
	}
	
	#event_edit_container {
		display: none;
	}
	
	#event_edit_container label {
		display: block;
		margin-top: 1em;
		margin-bottom: 0.5em;
		font-size: 12px;
	}
	
	#event_edit_container ul {
		padding: 0.3em;
		list-style: none;
	}
	
	#event_edit_container select, 
	#event_edit_container input[type='text'] {
		width: 250px;
		padding: 3px;
		font-size: 12px;
		border: 1px solid silver;
	}
	
	#event_edit_container input[type='text'] {
		width: 245px;
		font-size: 12px;
	}
	
	.ui-dialog-title,
	.ui-button-text {
		font-size: 14px;	
	}
	
	#closing_days {
		display: block;
		margin: 20px 0;
	}
	
	#closing_days p {
		padding: 20px;
		font-size: 12px;
	}
	
	#days_closed {
		width: 520px;
		height: 60px;
	}
	
	.daybox {
		display: inline-block;
		*display: inline;
		zoom: 1;
		width: 20px;
		font-size: 12px;
		line-height: 18px;
		padding: 5px;
		margin: 0;
		background-color: #f2f2f2;
	}
	
	.table-toolbox {
		text-align: right;
	}
	
	.popover {
		width: 440px;
	}
	
	form.filters {
		width: 360px;
	}
	
	form.filters .row {
		display: block;
		margin-bottom: 20px;
	}
	
	form#form_closing_days {
		width: 800px !important;
	}
	
	.dialogbox {
		width: 400px;
		background-color: white;
		display: none;
	}

	.dialogbox .-title {
		display: block;
		width: 100%;
		height: 32px;
		line-height: 32px;
		color: white;
		font-variant: small-caps;
		font-weight: bold;
		font-size: 14px;
		background: #7091a9;
		background: -moz-linear-gradient(top, #7091a9 0%, #07243d 100%);
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#7091a9), color-stop(100%,#07243d));
		background: -webkit-linear-gradient(top, #7091a9 0%,#07243d 100%);
		background: -o-linear-gradient(top, #7091a9 0%,#07243d 100%);
		background: -ms-linear-gradient(top, #7091a9 0%,#07243d 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#7091a9', endColorstr='#07243d',GradientType=0 );
		background: linear-gradient(top, #7091a9 0%,#07243d 100%);
	}

	.dialogbox .-title b {
		display: block;
		line-height: 32px;
		padding: 0 10px;
	}

	.dialogbox .-content {
		display: block;
		width: 360px;
		padding: 20px;
		font-size: 12px;
	}

	.dialogbox .-actions {
		width: 360px;
		margin: 0 20px;
		display: block;
		padding-bottom: 20px;
		text-align: right;
	}
	
	.section {
		padding-top: 40px;	
	}
	
</style>
<div class="oh-wrapper">
	<?php if (!$readonly) : ?>
	<!--
	<div style="text-align: right; margin-bottom: 10px;">
		<span id="delete" class="-button -delete dialog"><b>Delete All</b></span>
		<span id="filter" class="-button -search"><b>Copy From</b></span>
	</div>
	-->
	<div style="text-align: right; margin-bottom: 10px;" class="bootstrap">
		<span id="delete" class="btn btn-danger btn-xs dialog"><b>Delete All</b></span>
		<span id="filter" class="btn btn-primary btn-xs -search"><b>Copy From</b></span>
	</div>
	<?php  endif; ?>
	<div id='calendar'></div>
	<form class="request" id="form_closing_days" action="/applications/helpers/pos.closed.days.php" method="post" >
		<?php $form1->render(); ?>
	</form>
</div>
<div id="event_edit_container">
	<form id="timeform">
		<input type="hidden" />
		<ul>
			<li class="util">
				<span>Date: </span><span class="date_holder"></span> 
			</li>
			<li  class="util">
				<label for="title">Title: </label><input type="text" name="title" />
			</li>
			<li>
				<label for="start">Start Time: </label><select name="start"><option value="">Select Start Time</option></select>
			</li>
			<li>
				<label for="end">End Time: </label><select name="end"><option value="">Select End Time</option></select>
			</li>
			<li class="dayboxes">
				<label for="start">Apply to: </label>
				<span class=daybox >Mo <input type=checkbox name=day[1] value=1 ></span>
				<span class=daybox >Tu <input type=checkbox name=day[2] value=2 ></span>
				<span class=daybox >We <input type=checkbox name=day[3] value=3 ></span>
				<span class=daybox >Th <input type=checkbox name=day[4] value=4 ></span>
				<span class=daybox >Fr <input type=checkbox name=day[5] value=5 ></span>
				<span class=daybox >Sa <input type=checkbox name=day[6] value=6 ></span>
				<span class=daybox >Su <input type=checkbox name=day[7] value=7 ></span>
			</li>
		</ul>
	</form>
</div>	
<!--
<div id="delete_dialog" class="dialogbox">
	<div class="-title"><b>Delete</b></div>
	<div class="-content ">Are you sure to delete all Opening Hours?</div>
	<div class="-actions">
		<a class="-button -cancel "><b>Cancel</b></a>
		<a class="-button -apply "><b>Yes</b></a>
	</div>
</div>
-->
<div id="delete_dialog" class="dialogbox bootstrap">
	<div class="-title"><b>Delete</b></div>
	<div class="-content "><p>Are you sure to delete all Opening Hours?</p></div>
	<div class="-actions">
		<a class="btn btn-default btn-xs pull-left -cancel "><b>Cancel</b></a>
		<a class="btn btn-primary btn-xs pull-right -apply "><b>Yes</b></a>
		<br />
	</div>
</div>
<?php

    echo "</td>";
    echo "</tr>";
    echo "</table>";

    echo "</td>";
    echo "<td width=\"20\">";
    echo "<img src=\"../pictures/spacer.gif\" width=\"20\" height=\"1\" alt=\"\" />";
    echo "</td>";

    echo "</tr>";
	echo "<tr>";
    echo "<td colspan=\"3\" height=\"20\">";
    echo "</td>";
    echo "</tr>";
    echo "</table>";
   	echo "</form>";
?>
<div id="filter-form">
<form id="advanced" class="filters default">
	<input type="hidden" name="elem" id="elem" />
	<input type="hidden" name="application" id="application" value="mps" />
	<input type="hidden" name="country" id="country" value="<?php echo $_REQUEST['country'] ?>" />
	<input type="hidden" name="province" id="province" value="<?php echo $_REQUEST['province'] ?>" />
	<input type="hidden" name="ltf" id="ltf" value="<?php echo $_REQUEST['ltf'] ?>" />
	<input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
	<input type="hidden" name="readonly" id="readonly" value="<?php echo $readonly ?>" />
	<input type="hidden" name="increment" id="increment" value="<?php echo $increment ?>" />
	<input type="hidden" name="timeformat" id="timeformat" value="<?php echo $timeformat ?>" />
	<div class="row">
		<select name="place" id="place" class="filter"></select>
	</div>
	<div class="row">
		<select name="ownertype" id="ownertype" class="filter"></select>
	</div>
	<div class="row">
		<select name="pos" id="pos" class="filter"></select>
	</div>
</form>
</div>
</body>
</html>