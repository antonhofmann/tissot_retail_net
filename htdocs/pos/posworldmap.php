<?php
/********************************************************************

    posworldmap.php

    Lists of addresses (POS)

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2010-11-20
    Version:        2.0.0

    Copyright (c) 2008-2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
check_access("can_use_posindex");

//$api_key = GOOGLE_API_KEY;
if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
{
	$link_to_pos_detail = APPLICATION_URL . "/pos/posindex_pos.php?id=";
	$link_to_project_detail = APPLICATION_URL . "/user/project_task_center.php?pid=";
}
else
{
	$link_to_pos_detail = "http://retailnet/pos/posindex_pos.php?id=";
	$link_to_project_detail = "http://retailnet/user/project_task_center.php?pid=";
}

//get parameters
$filter = "";
$project_filter = "";
$project_pos_filter = "";
$only_regiion = true;

if(array_key_exists("r", $_GET) and $_GET["r"] > 0)
{
	$filter = "country_salesregion = " . $_GET["r"]; 

	$project_filter = "country_salesregion = " . $_GET["r"]; 

}

if(array_key_exists("c", $_GET) and $_GET["c"] > 0)
{
	if($filter)
	{
		$filter .= " and posaddress_country = " . $_GET["c"];
		$project_filter .= " and order_shop_address_country = " . $_GET["c"]; 
	}
	else
	{
		$filter = "posaddress_country = " . $_GET["c"]; 
		$project_filter = "order_shop_address_country = " . $_GET["c"]; 
	}
	$only_regiion = false;
}

if(array_key_exists("p", $_GET) and $_GET["p"] > 0)
{
	if($filter)
	{
		$filter .= " and place_province = " . $_GET["p"];
		
	}
	else
	{
		$filter = "place_province = " . $_GET["p"]; 
	}

	$project_pos_filter .= " and place_province = " . $_GET["p"];
}


$f_ptypes = array();
if(array_key_exists("t", $_GET) and strlen($_GET["t"]) > 0)
{
	$tmp = explode("-", $_GET["t"]);
	$f_ptypes = $tmp;
	$tmp = " posaddress_store_postype IN (" . implode(",", $tmp) . ") ";
	
	$tmp2 = explode("-", $_GET["t"]);
	$tmp2 = " project_postype IN (" . implode(",", $tmp2) . ") ";
	

	
	if($filter)
	{
		
		$filter .= " and " . $tmp;
		

	}
	else
	{
		$filter = $tmp;
		
	}

	if($project_filter)
	{
		$project_filter .= " and " . $tmp2;
	}
	else
	{
		$project_filter = $tmp2;
	}
}

$f_ltypes = array();
if(array_key_exists("l", $_GET) and strlen($_GET["l"]) > 0)
{
	$tmp = explode("-", $_GET["l"]);
	$f_ltypes = $tmp;
	$tmp = " posaddress_ownertype IN (" . implode(",", $tmp) . ") ";

	$tmp2 = explode("-", $_GET["l"]);
	$tmp2 = " project_cost_type IN (" . implode(",", $tmp2) . ") ";
	

	
	if($filter)
	{
		
		$filter .= " and " . $tmp;
	}
	else
	{
		$filter = $tmp; 
	}

	if($project_filter)
	{
		
		$project_filter .= " and " . $tmp2;
	}
	else
	{
		$project_filter = $tmp2; 
	}
}


$f_distributionchannels = array();
if(array_key_exists("d", $_GET) and strlen($_GET["d"]) > 0)
{
	$tmp = explode("-", $_GET["d"]);
	$f_distributionchannels = $tmp;
	$tmp = " posaddress_distribution_channel IN (" . implode(",", $tmp) . ") ";
	

	
	if($filter)
	{
		
		$filter .= " and " . $tmp;

	}
	else
	{
		$filter = $tmp; 
	}

	$project_pos_filter .= " and " . $tmp;
}


$f_turnoverclasses = array();
if(array_key_exists("tc", $_GET) and strlen($_GET["tc"]) > 0)
{
	$tmp = explode("-", $_GET["tc"]);
	$f_turnoverclasses = $tmp;
	$tmp = " posaddress_turnoverclass_watches IN (" . implode(",", $tmp) . ") ";
	

	
	if($filter)
	{
		
		$filter .= " and " . $tmp;
	}
	else
	{
		$filter = $tmp;
	}

	$project_pos_filter .= " and " . $tmp;
}

if(array_key_exists("o", $_GET) and $_GET["o"] > 0)
{
	if($filter)
	{
		$filter .= " and (posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null)";
	}
	else
	{
		$filter = "posaddress_store_closingdate = '0000-00-00' or posaddress_store_closingdate is null"; 
	}
}



if($filter) 
{
	$filter = "where " . $filter;
}

//marker icons
$marker_icons = array();
$marker_icon_legends = array();
$marker_icons[1] = 'pictures/marker_store_corporate.png';
$marker_icon_legends[1] = 'Corporate Boutique';
$marker_icons[2] = 'pictures/marker_sis_corporate.png';
$marker_icon_legends[2] = 'Corporate SIS';
$marker_icons[3] = 'pictures/marker_kiosk_corporate.png';
$marker_icon_legends[3] = 'Corporate Kiosk';
$marker_icons[4] = 'pictures/marker_store_franchisee.png';
$marker_icon_legends[4] = 'Franchisee Boutique';
$marker_icons[5] = 'pictures/marker_sis_franchisee2.png';
$marker_icon_legends[5] = 'Franchisee/Retailer SIS';
$marker_icons[6] = 'pictures/marker_kiosk_franchisee.png';
$marker_icon_legends[6] = 'Franchisee Kiosk';
$marker_icons[7] = 'pictures/marker_store_closed.png';
$marker_icon_legends[7] = 'Closed Boutique';
$marker_icons[8] = 'pictures/marker_sis_closed.png';
$marker_icon_legends[8] = 'Closed SIS';
$marker_icons[9] = 'pictures/marker_kiosk_closed.png';
$marker_icon_legends[9] = 'Closed Kiosk';


$marker_icons[10] = 'pictures/marker_independent.png';
$marker_icon_legends[10] = 'Independent Retailer';


$marker_icons[11] = 'pictures/marker_independent_closed.png';
$marker_icon_legends[11] = 'Closed Independent Retailer';

$marker_icons[12] = 'pictures/pin_red.png';
$marker_icon_legends[12] = 'Check legal type and POS type';




$marker_icons_n = array();
$marker_icons_n[1] = 'pictures/marker_store_corporate_n.png';
$marker_icons_n[2] = 'pictures/marker_sis_corporate_n.png';
$marker_icons_n[3] = 'pictures/marker_kiosk_corporate_n.png';
$marker_icons_n[4] = 'pictures/marker_store_franchisee_n.png';
$marker_icons_n[5] = 'pictures/marker_sis_franchisee2_n.png';
$marker_icons_n[6] = 'pictures/marker_kiosk_franchisee_n.png';
$marker_icons_n[10] = 'pictures/marker_independent_n.png';


$marker_icon_legends_a = array();
$marker_icons_a[1] = 'pictures/marker_store_corporate_a.png';
$marker_icons_a[2] = 'pictures/marker_sis_corporate_a.png';
$marker_icons_a[3] = 'pictures/marker_kiosk_corporate_a.png';
$marker_icons_a[4] = 'pictures/marker_store_franchisee_a.png';
$marker_icons_a[5] = 'pictures/marker_sis_franchisee2_a.png';
$marker_icons_a[6] = 'pictures/marker_kiosk_franchisee_a.png';
$marker_icons_a[10] = 'pictures/marker_independent_a.png';



$labelstyle_0 = "border: 1px solid black; font-size:25px; background: #FFFFFF; padding: 3px; white-space:nowrap;";

$labelstyle_n = "border: 1px solid black; background: #FFFFFF; padding: 3px; white-space:nowrap;    -moz-border-radius: 20px; -webkit-border-radius: 20px; -khtml-border-radius: 20px;    border-radius: 20px;";

$labelstyle_1 = "font-weight:bold;color: #FFFFFF;border: 1px solid black; background: #539e31; padding: 3px; white-space:nowrap;   -moz-border-radius: 20px; -webkit-border-radius: 20px; -khtml-border-radius: 20px;    border-radius: 20px;";

$labelstyle_2 = "font-weight:bold;color: #000000;border: 1px solid black; background: #f9ed2a; padding: 3px; white-space:nowrap;   -moz-border-radius: 20px; -webkit-border-radius: 20px; -khtml-border-radius: 20px;    border-radius: 20px;";

$labelstyle_6 = "font-weight:bold;color: #FFFFFF;border: 1px solid black; background: #1c5ff2; padding: 3px; white-space:nowrap;   -moz-border-radius: 20px; -webkit-border-radius: 20px; -khtml-border-radius: 20px;    border-radius: 20px;";

$labelstyle_c = "font-weight:bold;color: #FFFFFF;border: 1px solid black; background: #d04c20; padding: 3px; white-space:nowrap;   -moz-border-radius: 20px; -webkit-border-radius: 20px; -khtml-border-radius: 20px;    border-radius: 20px;";



//get markes of POS locations
$markers = array();
$marker_icon_types = array();
$posdata = array();
$posnames = array();
$labeltext = array();
$labelstyle = array();

if(!array_key_exists("opip", $_GET))
{
	$sql = "select posaddress_id, posaddress_name, posaddress_name2, posaddress_address, posaddress_address2, " .
		   "posaddress_zip, posaddress_place, " . 
		   "posaddress_store_postype, posaddress_ownertype, posaddress_store_closingdate, " . 
		   "posaddress_google_lat, posaddress_google_long, mps_turnoverclass_code, mps_distchannel_code " .
		   "from posaddresses " . 
		   "left join countries on country_id = posaddress_country " .
		   "left join places on place_id = posaddress_place_id " .
		   "left join mps_distchannels on mps_distchannel_id = posaddress_distribution_channel " .
		   "left join mps_turnoverclasses on mps_turnoverclass_id = posaddress_turnoverclass_watches " .
		   $filter . 
		   " order by posaddress_ownertype DESC, posaddress_name";


	$res = mysql_query($sql);
	$i = 0;
	while ($row = mysql_fetch_assoc($res)) 
	{
		if($row["posaddress_google_lat"] != 0 and $row["posaddress_google_long"] != 0)
		{
			$posnames[$i] = str_replace('"', '', $row["posaddress_name"]);

			$labeltext[$i] = '';
			$labelstyle[$i] = $labelstyle_n;


			if($row["posaddress_store_postype"] == 1 
				or $row["posaddress_store_postype"] == 2
				 or $row["posaddress_store_postype"] == 3 
				or $row["posaddress_store_postype"] == 4)
			{
				$tmp = 'labelstyle_' . $row["posaddress_ownertype"];
			}
			else
			{
				$tmp = 'labelstyle_2';
			}
			$labelstyle[$i] = $$tmp;
			

			if(array_key_exists("lb1", $_GET) and $_GET["lb1"] == 1)
			{
				$labeltext[$i] .= $posnames[$i];
			}

			if(array_key_exists("lb2", $_GET) and $_GET["lb2"] == 1)
			{
				if($labeltext[$i])
				{
					$labeltext[$i] .= " - " . $row["mps_distchannel_code"];
				}
				else
				{
					$labeltext[$i] = $row["mps_distchannel_code"];
				}
				
			}

			if(array_key_exists("lb3", $_GET) and $_GET["lb3"] == 1)
			{
				
				if($labeltext[$i] and $row["mps_turnoverclass_code"])
				{
					$labeltext[$i] .= " - " . $row["mps_turnoverclass_code"];
				}
				elseif($row["mps_turnoverclass_code"])
				{
					$labeltext[$i] = $row["mps_turnoverclass_code"];
				}
			}
			


			$tmp = "<div class='infowindow'><strong>" . $row["posaddress_name"] . "</strong><br />";
			if($row["posaddress_name2"])
			{
				$tmp .= $row["posaddress_name2"] . "<br />";
			}

			$tmp .= $row["posaddress_address"] . "<br />";
			if($row["posaddress_address2"])
			{
				$tmp .= $row["posaddress_address2"] . "<br />";
			}
			$tmp .= $row["posaddress_zip"] . " " . $row["posaddress_place"] . "<br />";



			if(has_access("can_edit_posindex") or has_access("can_view_posindex") or has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
			{
			   $tmp .= "<br /><a href=\"" . $link_to_pos_detail . $row["posaddress_id"] .  "\" target=\"_blank\">POS Details<\/a>";
			}
			
			$tmp = str_replace("\"", "\'", $tmp);

			$tmp .= "</div>";
			$posdata[$i] = $tmp;

			$markers[$i] = array("lat"=>$row["posaddress_google_lat"], "long"=>$row["posaddress_google_long"]);
			if($row["posaddress_store_closingdate"] == NULL or $row["posaddress_store_closingdate"] == '0000-00-00')
			{
				
				
				if($row["posaddress_ownertype"] == 1 and $row["posaddress_store_postype"] == 1)
				{
					$marker_icon_types[$i] = $marker_icons[1];
				}
				elseif($row["posaddress_ownertype"] == 1 and $row["posaddress_store_postype"] == 2)
				{
					$marker_icon_types[$i] = $marker_icons[2];
				}
				elseif($row["posaddress_ownertype"] == 1 and $row["posaddress_store_postype"] == 3)
				{
					$marker_icon_types[$i] = $marker_icons[3];
				}
				elseif($row["posaddress_ownertype"] == 2 and $row["posaddress_store_postype"] == 1)
				{
					$marker_icon_types[$i] = $marker_icons[4];
				}
				elseif($row["posaddress_ownertype"] == 6 and $row["posaddress_store_postype"] == 2)
				{
					$marker_icon_types[$i] = $marker_icons[5];
				}
				elseif($row["posaddress_ownertype"] == 2 and $row["posaddress_store_postype"] == 3)
				{
					$marker_icon_types[$i] = $marker_icons[6];
				}
				elseif($row["posaddress_ownertype"] == 6 and $row["posaddress_store_postype"] == 4)
				{
					$marker_icon_types[$i] = $marker_icons[10];
				}
				elseif($row["posaddress_ownertype"] == 2 and $row["posaddress_store_postype"] == 2)
				{
					$marker_icon_types[$i] = $marker_icons[5];
				}
				else
				{
					$marker_icon_types[$i] = $marker_icons[12];
				}
			}
			else
			{
				if($row["posaddress_store_postype"] == 1)
				{
					$marker_icon_types[$i] = $marker_icons[7];
				}
				elseif($row["posaddress_store_postype"] == 2)
				{
					$marker_icon_types[$i] = $marker_icons[8];
				}
				elseif($row["posaddress_store_postype"] == 3)
				{
					$marker_icon_types[$i] = $marker_icons[9];
				}
				elseif($row["posaddress_store_postype"] == 4)
				{
					$marker_icon_types[$i] = $marker_icons[11];
				}
				else
				{
					$marker_icon_types[$i] = $marker_icons[12];
				}
				$labelstyle[$i] = $labelstyle_c;
			}
		}
		$i++;
	}
}


//echo '<pre />';
//print_r($markers);

if($project_filter and array_key_exists("opip", $_GET) and $_GET["opip"] == 1
 or (array_key_exists("pip", $_GET) and $_GET["pip"] == 1
 or array_key_exists("pip2", $_GET) and $_GET["pip2"] == 1))
{

	$pos_projects = array();
	
	
	if(array_key_exists("pip", $_GET) and $_GET["pip"] == 1
		and array_key_exists("pip2", $_GET) and $_GET["pip2"] == 1)
	{
		$project_filter .= " and project_state in (1, 2)";
	}
	elseif(array_key_exists("pip", $_GET) and $_GET["pip"] == 1)
	{
		$project_filter .= " and project_state in (1)";
	}
	elseif(array_key_exists("pip2", $_GET) and $_GET["pip2"] == 1)
	{
		$project_filter .= " and project_state in (2)";
	}

	
	if(array_key_exists("pip", $_GET) and $_GET["pip"] == 1
		and array_key_exists("pip2", $_GET) and $_GET["pip2"] == 1)
	{
		$sql = "select project_id, project_order, project_state, project_postype, project_cost_type " . 
			   " from projects " . 
			   " left join orders on order_id = project_order " .
			   " left join project_costs on project_cost_order = project_order " .
			   "left join countries on country_id = order_shop_address_country " .
			   " where (order_archive_date is null or order_archive_date = '0000-00-00' ) " . 
			   " and project_state in (1, 2) " . 
			   " and " . $project_filter;
	}
	elseif(array_key_exists("pip", $_GET) and $_GET["pip"] == 1)
	{
		$sql = "select project_id, project_order, project_state, project_postype, project_cost_type " . 
			   " from projects " . 
			   " left join orders on order_id = project_order " .
			   " left join project_costs on project_cost_order = project_order " .
			   "left join countries on country_id = order_shop_address_country " .
			   " where (order_archive_date is null or order_archive_date = '0000-00-00' ) " . 
			   " and project_state in (1) " . 
			   " and " . $project_filter;
	}
	elseif(array_key_exists("pip2", $_GET) and $_GET["pip2"] == 1)
	{
		$sql = "select project_id, project_order, project_state, project_postype, project_cost_type " . 
			   " from projects " . 
			   " left join orders on order_id = project_order " .
			   " left join project_costs on project_cost_order = project_order " .
			   "left join countries on country_id = order_shop_address_country " .
			   " where (order_archive_date is null or order_archive_date = '0000-00-00' ) " . 
			   " and project_state in (2) " . 
			   " and " . $project_filter;
	}

	//$sql .= " order by project_costtype DESC, order_shop_address_company";

	$res = mysql_query($sql);

	while ($row = mysql_fetch_assoc($res)) 
	{
		
		//check if ln/af filter
		$submitted = false;
		$approved = false;
		$include_project = true;
		
		
		$milestone_filter = array();
		
		// AF/LN submitted but not approved
		$milestone_filter[] = 1;
		$milestone_filter[] = 16;

		$milestone_filter[] = 10;
		$milestone_filter[] = 13;
		
		
		
		if(count($milestone_filter) > 0)
		{
			$sql_m = "select project_milestone_milestone, project_milestone_date " . 
						 " from project_milestones " . 
						 " where project_milestone_project = " . $row["project_id"] . 
						 " and project_milestone_milestone in (" . implode(',', $milestone_filter) . ") ";
			$res_m = mysql_query($sql_m);

			while ($row_m = mysql_fetch_assoc($res_m)) 
			{
				
				if($row_m["project_milestone_date"] != NULL and $row_m["project_milestone_date"] != '0000-00-00')
				{
					
					if($row_m["project_milestone_milestone"] == 1 or 
						$row_m["project_milestone_milestone"] == 16)
					{
						$submitted = true;
					}
					if($row_m["project_milestone_milestone"] == 10 or 
						$row_m["project_milestone_milestone"] == 13)
					{
						$approved = true;
					}
					
				}

				//echo $row_m["project_milestone_milestone"] . "->" . $row_m["project_milestone_date"] . "<br />";
			}

			if(array_key_exists("lnaf1", $_GET) and $_GET["lnaf1"] > 0
				and array_key_exists("lnaf2", $_GET) and $_GET["lnaf2"] > 0)
			{
				if($submitted == false and $approved == false)
				{
					$include_project = false;
				}
			}
			elseif(array_key_exists("lnaf1", $_GET) and $_GET["lnaf1"] > 0)
			{
				if($submitted == false or $approved == true)
				{
					$include_project = false;
				}
			}
			elseif(array_key_exists("lnaf2", $_GET) and $_GET["lnaf2"] > 0)
			{
				if($approved == false)
				{
					$include_project = false;
				}
			}
		}

		
		if($include_project == true)
		{
			//look in pipeline
			$sql_po = "select posorder_parent_table, posorder_posaddress " . 
						"from posorderspipeline " . 
						" where posorder_order = " . $row["project_order"];

			$res_po = mysql_query($sql_po);

			if ($row_po = mysql_fetch_assoc($res_po)) 
			{
				if($row_po["posorder_parent_table"] == 'posaddressespipeline')
				{
					$sql_p = "select posaddress_id, posaddress_name, posaddress_name2, " . 
							   "posaddress_address, posaddress_address2, " .
							   "posaddress_zip, posaddress_place, " . 
							   "posaddress_google_lat, posaddress_google_long, mps_turnoverclass_code, mps_distchannel_code " .
							   "from posaddressespipeline " . 
							   "left join countries on country_id = posaddress_country " .
							   "left join places on place_id = posaddress_place_id " .
							   "left join mps_distchannels on mps_distchannel_id = posaddress_distribution_channel " .
							   "left join mps_turnoverclasses on mps_turnoverclass_id = posaddress_turnoverclass_watches " .
							   " where posaddress_id = " . $row_po["posorder_posaddress"] . 
							   $project_pos_filter;
					           //" order by posaddress_ownertype DESC, posaddress_name";


					


					$res_p = mysql_query($sql_p);

					if ($row_p = mysql_fetch_assoc($res_p)) 
					{
						
						$row_p["project_id"] = $row["project_id"];
						$row_p["posaddress_store_postype"] = $row["project_postype"];
						$row_p["posaddress_ownertype"] = $row["project_cost_type"];
						$row_p["submitted"] = $submitted;
						$row_p["approved"] = $approved;
						
						$pos_projects[$row["project_id"]] =  $row_p;

						
					}

					
				
				}
				else
				{
					$sql_p = "select posaddress_id, posaddress_name, posaddress_name2, " . 
							   "posaddress_address, posaddress_address2, " .
							   "posaddress_zip, posaddress_place, " . 
							   "posaddress_google_lat, posaddress_google_long, mps_turnoverclass_code, mps_distchannel_code " .
							   "from posaddresses " . 
							   "left join countries on country_id = posaddress_country " .
							   "left join places on place_id = posaddress_place_id " .
							   "left join mps_distchannels on mps_distchannel_id = posaddress_distribution_channel " .
							   "left join mps_turnoverclasses on mps_turnoverclass_id = posaddress_turnoverclass_watches " .
							   " where posaddress_id = " . $row_po["posorder_posaddress"] . 
							   $project_pos_filter;
						       //" order by posaddress_ownertype DESC, posaddress_name";

					

					$res_p = mysql_query($sql_p);

					if ($row_p = mysql_fetch_assoc($res_p)) 
					{
						
						$row_p["project_id"] = $row["project_id"];
						$row_p["posaddress_store_postype"] = $row["project_postype"];
						$row_p["posaddress_ownertype"] = $row["project_cost_type"];
						$row_p["submitted"] = $submitted;
						$row_p["approved"] = $approved;

						$pos_projects[$row["project_id"]] =  $row_p;
					}
				}
			}
			else
			{
				
				$sql_po = "select posorder_posaddress " . 
						 "from posorders " . 
						 " where posorder_order = " . $row["project_order"];

				$res_po = mysql_query($sql_po);

				if ($row_po = mysql_fetch_assoc($res_po)) 
				{
				
					$sql_p = "select posaddress_id, posaddress_name, posaddress_name2, " . 
							   "posaddress_address, posaddress_address2, " .
							   "posaddress_zip, posaddress_place, " . 
							   "posaddress_google_lat, posaddress_google_long, mps_turnoverclass_code, mps_distchannel_code " .
							   "from posaddresses " . 
							   "left join countries on country_id = posaddress_country " .
							   "left join places on place_id = posaddress_place_id " .
							   "left join mps_distchannels on mps_distchannel_id = posaddress_distribution_channel " .
							   "left join mps_turnoverclasses on mps_turnoverclass_id = posaddress_turnoverclass_watches " .
							   " where posaddress_id = " . $row_po["posorder_posaddress"] . 
							   $project_pos_filter;
						       //" order by posaddress_ownertype DESC, posaddress_name";

					

					$res_p = mysql_query($sql_p);

					if ($row_p = mysql_fetch_assoc($res_p)) 
					{
						
						$row_p["project_id"] = $row["project_id"];
						$row_p["posaddress_store_postype"] = $row["project_postype"];
						$row_p["posaddress_ownertype"] = $row["project_cost_type"];
						$row_p["submitted"] = $submitted;
						$row_p["approved"] = $approved;

						$pos_projects[$row["project_id"]] =  $row_p;

						
					}
				}
			}
		}
	
	}


	foreach($pos_projects as $project_id=>$row)
	{
		$i = count($posnames);
		if($row["posaddress_google_lat"] != 0 and $row["posaddress_google_long"] != 0)
		{
			$tmp = str_replace('"', '', $row["posaddress_name"]);
			if(in_array($tmp, $posnames)) {
				
				$i = array_search ($tmp, $posnames);
			}
			
			$posnames[$i] = $tmp;

			$labeltext[$i] = '';
			$labelstyle[$i] = $labelstyle_n;


			if($row["posaddress_store_postype"] == 1 
				or $row["posaddress_store_postype"] == 2
				 or $row["posaddress_store_postype"] == 3 
				or $row["posaddress_store_postype"] == 4)
			{
				$tmp = 'labelstyle_' . $row["posaddress_ownertype"];
			}
			else
			{
				$tmp = 'labelstyle_2';
			}
			$labelstyle[$i] = $$tmp;
			

			if(array_key_exists("lb1", $_GET) and $_GET["lb1"] == 1)
			{
				$labeltext[$i] .= $posnames[$i];
			}

			if(array_key_exists("lb2", $_GET) and $_GET["lb2"] == 1)
			{
				if($labeltext[$i])
				{
					$labeltext[$i] .= " - " . $row["mps_distchannel_code"];
				}
				else
				{
					$labeltext[$i] = $row["mps_distchannel_code"];
				}
				
			}

			if(array_key_exists("lb3", $_GET) and $_GET["lb3"] == 1)
			{
				
				if($labeltext[$i] and $row["mps_turnoverclass_code"])
				{
					$labeltext[$i] .= " - " . $row["mps_turnoverclass_code"];
				}
				elseif($row["mps_turnoverclass_code"])
				{
					$labeltext[$i] = $row["mps_turnoverclass_code"];
				}
			}
			


			$tmp = "<div class='infowindow'><strong>" . $row["posaddress_name"] . "</strong><br />";
			if($row["posaddress_name2"])
			{
				$tmp .= $row["posaddress_name2"] . "<br />";
			}

			$tmp .= $row["posaddress_address"] . "<br />";
			if($row["posaddress_address2"])
			{
				$tmp .= $row["posaddress_address2"] . "<br />";
			}
			$tmp .= $row["posaddress_zip"] . " " . $row["posaddress_place"] . "<br />";



			if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
			{
			   $tmp .= "<br /><a href=\"" . $link_to_project_detail . $row["project_id"] .  "\" target=\"_blank\">Project Details<\/a>";
			}
			
			$tmp = str_replace("\"", "\'", $tmp);

			$tmp .= "</div>";
			$posdata[$i] = $tmp;

			$markers[$i] = array("lat"=>$row["posaddress_google_lat"], "long"=>$row["posaddress_google_long"]);
				
			
			if($row["approved"] == true)
			{
				if($row["posaddress_ownertype"] == 1 and $row["posaddress_store_postype"] == 1)
				{
					$marker_icon_types[$i] = $marker_icons_a[1];
				}
				elseif($row["posaddress_ownertype"] == 1 and $row["posaddress_store_postype"] == 2)
				{
					$marker_icon_types[$i] = $marker_icons_a[2];
				}
				elseif($row["posaddress_ownertype"] == 1 and $row["posaddress_store_postype"] == 3)
				{
					$marker_icon_types[$i] = $marker_icons_a[3];
				}
				elseif($row["posaddress_ownertype"] == 2 and $row["posaddress_store_postype"] == 1)
				{
					$marker_icon_types[$i] = $marker_icons_a[4];
				}
				elseif($row["posaddress_ownertype"] == 6 and $row["posaddress_store_postype"] == 2)
				{
					$marker_icon_types[$i] = $marker_icons_a[5];
				}
				elseif($row["posaddress_ownertype"] == 2 and $row["posaddress_store_postype"] == 3)
				{
					$marker_icon_types[$i] = $marker_icons_a[6];
				}
				elseif($row["posaddress_ownertype"] == 6 and $row["posaddress_store_postype"] == 4)
				{
					$marker_icon_types[$i] = $marker_icons_a[10];
				}
				else
				{
					$marker_icon_types[$i] = $marker_icons[12];
				}
			}
			
			if(($row["submitted"] == true and $row["approved"] == false)
				or ($row["submitted"] == false and $row["approved"] == false))
			{
				if($row["posaddress_ownertype"] == 1 and $row["posaddress_store_postype"] == 1)
				{
					$marker_icon_types[$i] = $marker_icons_n[1];
				}
				elseif($row["posaddress_ownertype"] == 1 and $row["posaddress_store_postype"] == 2)
				{
					$marker_icon_types[$i] = $marker_icons_n[2];
				}
				elseif($row["posaddress_ownertype"] == 1 and $row["posaddress_store_postype"] == 3)
				{
					$marker_icon_types[$i] = $marker_icons_n[3];
				}
				elseif($row["posaddress_ownertype"] == 2 and $row["posaddress_store_postype"] == 1)
				{
					$marker_icon_types[$i] = $marker_icons_n[4];
				}
				elseif($row["posaddress_ownertype"] == 6 and $row["posaddress_store_postype"] == 2)
				{
					$marker_icon_types[$i] = $marker_icons_n[5];
				}
				elseif($row["posaddress_ownertype"] == 2 and $row["posaddress_store_postype"] == 3)
				{
					$marker_icon_types[$i] = $marker_icons_n[6];
				}
				elseif($row["posaddress_ownertype"] == 6 and $row["posaddress_store_postype"] == 4)
				{
					$marker_icon_types[$i] = $marker_icons_n[10];
				}
				else
				{
					$marker_icon_types[$i] = $marker_icons[12];
				}
			}


			
		$i++;

		//echo $i . "<br />";
		}
		
	}
}

//print_r($markers);
//echo '<pre>';
//var_dump($markers);
//var_dump($marker_icon_types);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>POS Index - World Map</title>


	<script type="text/javascript"src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=<?php echo GOOGLE_API_KEY;?>"></script>
	
	<script language="javascript" src="../js/infobox.js" type="text/javascript"></script>
	<script language="javascript" src="../js/jquery-1.2.6.js" type="text/javascript"></script>
	<script type="text/javascript">
	  
	  
		  
		  
		  $(window).resize(function() {
			  
			 $('#map').width($(window).width() - 20 );
			 $('#map').height($(window).height() - 80 );
		  });


			
		  
		  var map;
		  function initialize() {
			var latlng = new google.maps.LatLng(0, 0);
			var myOptions = {
			  zoom: 2,
			  center: latlng,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			var map = new google.maps.Map(document.getElementById("map"), myOptions);

			var bounds = new google.maps.LatLngBounds();


			

			
			<?php
			foreach($markers as $key=>$cords)
			{
			?>
				var MarkerIcon = new google.maps.MarkerImage("<?php echo $marker_icon_types[$key];?>");

				var latlng = new google.maps.LatLng(<?php echo $cords["lat"];?>, <?php echo $cords["long"];?>);
				bounds.extend(latlng)

				var marker<?php echo $key;?> = new google.maps.Marker({
				  position: latlng, 
				  map: map,
				  zIndex: <?php echo $key;?>,
				  icon: MarkerIcon,
				  title:"<?php echo $posnames[$key];?>"
				});

				var infowindow<?php echo $key;?> = new google.maps.InfoWindow(
				  { content: "<?php echo $posdata[$key];?>",
					position: latlng
				  });
			  
				google.maps.event.addListener(marker<?php echo $key;?>, 'click', function() {
					infowindow<?php echo $key;?>.open(map,marker<?php echo $key;?>);
				});


				var boxText = document.createElement("div");

				boxText.style.cssText = "<?php echo $labelstyle[$key];?>";
				boxText.innerHTML = "<?php echo $labeltext[$key];?>";
						
				
				<?php
				if($labeltext[$key] and
					(array_key_exists("lb1", $_GET) and $_GET["lb1"] == 1
					or array_key_exists("lb2", $_GET) and $_GET["lb2"] == 1	
					or array_key_exists("lb3", $_GET) and $_GET["lb3"] == 1)
				)
				{
				?>
				
					var myOptions = {
							 content: boxText
							,disableAutoPan: false
							,maxWidth: 0
							,pixelOffset: new google.maps.Size(-20, 0)
							,zIndex: <?php echo $key;?>
							,boxStyle: { 
							  opacity: 0.8,
							  width: "auto"}
							,closeBoxMargin: "1px 1px 1px 1px"
							,closeBoxURL: ""
							,infoBoxClearance: new google.maps.Size(1, 1)
							,isHidden: false
							,pane: "floatPane"
							,enableEventPropagation: false
					};


					
					var ib<?php echo $key;?> = new InfoBox(myOptions);
					ib<?php echo $key;?>.open(map, marker<?php echo $key;?>);

			<?php
				}
			}
			
			if($filter and $only_regiion == false and count($posnames) > 0)
			{
			?>
				map.fitBounds(bounds);

			<?php
			}
			if(count($posnames) == 0)
			{
			?>
			

				
				var latlng = new google.maps.LatLng(0,0);

				var marker = new google.maps.Marker({
				  position: latlng, 
				  map: map,
				  title:"No POS"
				});


				var boxText = document.createElement("div");

				boxText.style.cssText = "<?php echo $labelstyle_0;?>";
				boxText.innerHTML = "There are no POS locations matching the criteria you entered";

				var myOptions = {
							 content: boxText
							,disableAutoPan: false
							,maxWidth: 0
							,pixelOffset: new google.maps.Size(-120, -35)
							,zIndex: null
							,boxStyle: { 
							  opacity: 1,
							  width: "auto"}
							,closeBoxMargin: "1px 1px 1px 1px"
							,closeBoxURL: ""
							,infoBoxClearance: new google.maps.Size(1, 1)
							,isHidden: false
							,pane: "floatPane"
							,enableEventPropagation: false
					};


					
					var ib = new InfoBox(myOptions);
					ib.open(map, marker);


			<?php
			}
			?>
		  }

	

	  

	  

	</script>


	
  <style type="text/css">
  		
		body{font-family: Verdana, Geneva, sans-serif;font-size: 12px;color: #000000;}
		a {color: #006699;text-decoration: none;font-weight: bold;}
		a:hover {color: #FF0000;text-decoration: none;}
	
		#map{font-family:Arial, Helvetica, sans-serif; }
		.clear{ float:none; clear:both; height:0px; line-height:0px; font-size:0px; }
		.infowindow{width:250px;height:120px;}
		.legend{float:left; margin-bottom:4px; margin-right:10px;}
		
		.infoBoxContent{background-color:#00ff00;}
		
  
  </style>

  </head>

  <body>
	<div id="map" style="width: 100%px; height: 700px;"></div>
	<div><br /><br />
	<?php
		
		//compoase legend
		
		if(in_array(1, $f_ltypes) and in_array(1, $f_ptypes))
		{
			echo '<div class="legend"><img src="' . str_replace(".png", "_small.png", $marker_icons[1]) . '" />&nbsp;' . $marker_icon_legends[1] . "</div>";
		}
		if(in_array(1, $f_ltypes) and in_array(2, $f_ptypes))
		{
			echo '<div class="legend"><img src="' . str_replace(".png", "_small.png", $marker_icons[2]) . '" />&nbsp;' . $marker_icon_legends[2] . "</div>";
		}
		if(in_array(1, $f_ltypes) and in_array(3, $f_ptypes))
		{
			echo '<div class="legend"><img src="' . str_replace(".png", "_small.png", $marker_icons[3]) . '" />&nbsp;' . $marker_icon_legends[3] . "</div>";
		}
		if(in_array(2, $f_ltypes) and in_array(1, $f_ptypes))
		{
			echo '<div class="legend"><img src="' . str_replace(".png", "_small.png", $marker_icons[4]) . '" />&nbsp;' . $marker_icon_legends[4] . "</div>";
		}
		if(in_array(6, $f_ltypes) and in_array(2, $f_ptypes))
		{	
			echo '<div class="legend"><img src="' . str_replace(".png", "_small.png", $marker_icons[5]) . '" />&nbsp;' . $marker_icon_legends[5] . "</div>";
		}
		if(in_array(2, $f_ltypes) and in_array(3, $f_ptypes))
		{
			echo '<div class="legend"><img src="' . str_replace(".png", "_small.png", $marker_icons[6]) . '" />&nbsp;' . $marker_icon_legends[6] . "</div>";
		}

		if(!array_key_exists("o", $_GET) and in_array(1, $f_ptypes))
		{
			echo '<div class="legend"><img src="' . str_replace(".png", "_small.png", $marker_icons[7]) . '" />&nbsp;' . $marker_icon_legends[7] . "</div>";
		}

		if(!array_key_exists("o", $_GET) and in_array(2, $f_ptypes))
		{
			echo '<div class="legend"><img src="' . str_replace(".png", "_small.png", $marker_icons[8]) . '" />&nbsp;' . $marker_icon_legends[8] . "</div>";
		}

		if(!array_key_exists("o", $_GET) and in_array(3, $f_ptypes))
		{
			echo '<div class="legend"><img src="' . str_replace(".png", "_small.png", $marker_icons[9]) . '" />&nbsp;' . $marker_icon_legends[9] . "</div>";
		}

		if(in_array(4, $f_ptypes))
		{
			echo '<div class="legend"><img src="' . str_replace(".png", "_small.png", $marker_icons[10]) . '" />&nbsp;' . $marker_icon_legends[10] . "</div>";
		}

		if(!array_key_exists("o", $_GET) and in_array(4, $f_ptypes))
		{
			echo '<div class="legend"><img src="' . str_replace(".png", "_small.png", $marker_icons[11]) . '" />&nbsp;' . $marker_icon_legends[11] . "</div>";
		}

		if(array_key_exists("opip", $_GET) and $_GET["opip"] == 1
		 or array_key_exists("pip", $_GET) and $_GET["pip"] == 1
		 or array_key_exists("pip2", $_GET) and $_GET["pip2"] == 1)
		{
			if(array_key_exists("lnaf1", $_GET) and $_GET["lnaf1"] == 1
				and array_key_exists("lnaf2", $_GET) and $_GET["lnaf2"] == 1)
			{
				echo '<div class="legend">a = AF/LN approved&nbsp;n = AF/LN not approved</div>';
			}
			elseif(array_key_exists("lnaf1", $_GET) and $_GET["lnaf1"] == 1)
			{
				echo '<div class="legend">n = AF/LN not approved</div>';
			}
			elseif(array_key_exists("lnaf2", $_GET) and $_GET["lnaf2"] == 1)
			{
				echo '<div class="legend">a = AF/LN approved&nbsp;</div>';
			}
			else
			{
				echo '<div class="legend">a = AF/LN approved&nbsp;n = AF/LN not approved</div>';
			}
			
		}
			
			
		
	?>
	</div>
	
	<script type="text/javascript">
		$(document).ready(function() {
			initialize();
		});
	</script>
  </body>
</html>

