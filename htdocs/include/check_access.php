<?php
//check if user has access to a project
if(param("pid") and user_id())
{
	if(has_access("has_access_to_all_projects") or has_access("has_full_access_to_cer"))
	{
	
	}
	else
	{
		//get all projects where the user has access
		$user_id = user_id();
		$user_address = 0;
		$user_country = 0;
		$user_email = "nomail";

		$project_ids = array();

		// get user's address
		$sql = "select user_address, address_country, user_can_only_see_his_projects, user_email " . 
			   "from users ".
			   "lfte join addresses on address_id = user_address " . 
			   "where user_id = " . $user_id;

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{            
			$user_address = $row["user_address"];
			$user_country = $row["address_country"];
			$user_email = $row["user_email"];
			
			$project_access_filter = "";
			if($row["user_can_only_see_his_projects"] == 1)
			{
				$project_access_filter = " and order_user = " . user_id() . " ";
			}
		}


		//regional access
		$regional_access_filter = "";
		$tmp = array();
		$sql = "select project_id " . 
			   " from user_company_responsibles " . 
			   " inner join orders on order_client_address = user_company_responsible_address_id " .
			   " inner join projects on project_order = order_id " .
			   " where user_company_responsible_user_id = " . dbquote(user_id());


		$res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
		{ 
			$project_ids[$row["project_id"]] = $row["project_id"];
		}



		//check if user has a task for the project
		$sql = "select DISTINCT project_id from tasks " . 
			   " left join orders on order_id = task_order " . 
			   " left join projects on project_order = order_id " . 
			   " where task_user = " . dbquote(user_id()) . 
			   " and project_id = " . dbquote(param("pid"));


		$res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
		{ 
			$project_ids[$row["project_id"]] = $row["project_id"];
		}


		//check if user was involved into the project
		$sql = "select DISTINCT project_id from order_mails " . 
			   " left join orders on order_id = order_mail_order " . 
			   " left join projects on project_order = order_id " . 
			   " where order_mail_user = " . dbquote(user_id()) . 
			   " and project_id = " . dbquote(param("pid"));


		$res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
		{ 
			$project_ids[$row["project_id"]] = $row["project_id"];
		}

		//get user's roles
		$user_roles = array();
    
		$sql = "select user_role_role ".
			   "from user_roles ".
			   "left join roles on user_role_role = role_id ".
			   "where user_role_user = " . $user_id;

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$user_roles[] = $row["user_role_role"];
		}

		// check client
		$sql = "select project_id " .
			   "from projects ".
			   "left join orders on order_id = project_order " . 
			   "where order_user = " . $user_id . 
			   "   and order_client_address = " . $user_address;

		$res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
		{            
			$project_ids[$row["project_id"]] = $row["project_id"];
		}


		//check company access
		if(has_access("has_access_to_all_projects_of_his_company"))
		{
			$sql = "select project_id " .
				   "from projects ".
				   "left join orders on order_id = project_order " . 
				   "where order_client_address = " . $user_address;

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{            
				$project_ids[] = $row["project_id"];
			}

		}


		// local agent
		if (has_access("has_access_to_all_projects_of_his_country"))
		{

			$country_filter = "";
			$sql = "select * from country_access " .
				   "where country_access_user = " . user_id();


			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{            
				$country_filter.= " or address_country = " . $row["country_access_country"];
			}

			if($country_filter)
			{
				$country_filter = "address_country  = " . $user_country . $country_filter;
			}
			else
			{
				$country_filter = "address_country  = " . $user_country;
			}
			
			if($country_filter)
			{
				$sql = "select project_id " .
					   "from projects ".
					   "left join orders on order_id = project_order " .
					   "left join addresses on address_id = order_client_address " .
					   "where (" . $country_filter . ") ";
			}
			else
			{
				$sql = "select project_id " .
					   "from projects ".
					   "left join orders on order_id = project_order " .
					   "left join addresses on address_id = order_client_address " .
					   "where (" . $country_filter . ") " . 
					   $project_access_filter;
			}

			

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{            
				$project_ids[$row["project_id"]] = $row["project_id"];
			}
		}
		
		// retail coordinator, design contractor, design supervisor
		$sql = "select project_id " .
			   "from projects ".
			   "left join orders on order_id = project_order " .
			   "where order_actual_order_state_code >= 220 " . 
			   "   and (project_retail_coordinator = " . $user_id . 
			   "   or project_design_contractor = " . $user_id .
			   "   or project_design_supervisor = " . $user_id . 
			   ")";

		$res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
		{            
			$project_ids[$row["project_id"]] = $row["project_id"];
		}

	   
		// check supplier or forwarder
		$sql = "select distinct project_id, order_item_order " .
			   "from projects ".
		       "left join order_items on order_item_order = project_order " .
			   "where order_item_supplier_address = " .$user_address . 
			   "   or order_item_forwarder_address = " . $user_address;
		
		$res = mysql_query($sql) or dberror($sql);
	
		while ($row = mysql_fetch_assoc($res))
		{            
			$project_ids[$row["project_id"]] = $row["project_id"];
		}

		
		if(in_array(5, $user_roles)) // supplier
		{
			//check if supplier has a request for offer without having items in the list of materials
			$sql = "select project_id from tasks " .
				   " left join projects on project_order = task_order " . 
				   " where task_user = " . dbquote($user_id) . 
				   " and task_order_state = 26 ";

			$res = mysql_query($sql) or dberror($sql);
		
			while ($row = mysql_fetch_assoc($res))
			{            
				$project_ids[$row["project_id"]] = $row["project_id"];
			}

		}


		//get projects from attachments
		$sql_order_addresses = 'select DISTINCT project_id from order_files ' . 
						   'left join order_file_addresses on order_file_address_file = order_file_id ' .
			               'left join projects on project_order = order_file_order ' . 
						   'where order_file_address_address = ' . $user_address;

		$res = mysql_query($sql_order_addresses) or dberror($sql_order_addresses);
		while ($row = mysql_fetch_assoc($res))
		{
			 $project_ids[$row["project_id"]] = $row["project_id"];
		}


		//check cer access
		if(has_access("cer_has_full_access_to_his_projects"))
		{
			$sql = "select distinct project_id " .
				   "from projects ".
				   "left join orders on order_id = project_order " .
				   "where order_shop_address_country = " . $user_country;
			
			$res = mysql_query($sql) or dberror($sql);
		
			while ($row = mysql_fetch_assoc($res))
			{            
				$project_ids[$row["project_id"]] = $row["project_id"];
			}
		}


		//check travelling projects

		if(has_access("has_access_to_all_travalling_retail_data"))
		{
			$sql = "select distinct project_id " .
				   "from posorderspipeline ".
				   "left join orders on order_id = posorder_order " .
				   "left join projects on project_order = order_id " .
				   "left join posareaspipeline on posarea_posaddress = posorder_posaddress " .
				   "where project_id =  " . dbquote(param("pid")) . 
				   " and(posarea_area IN (4,5) or posorder_subclass IN(17)) and posorder_order > 0";


			$res = mysql_query($sql) or dberror($sql);
		
			if ($row = mysql_fetch_assoc($res))
			{            
				$project_ids[$row["project_id"]] = $row["project_id"];
			}
			
			$sql = "select distinct project_id " .
				   "from posorders ".
				   "left join orders on order_id = posorder_order " .
				   "left join projects on project_order = order_id " .
				   "left join posareas on posarea_posaddress = posorder_posaddress " .
				   " where project_id =  " . dbquote(param("pid")) . 
				   " and (posarea_area IN (4,5) or posorder_subclass IN(17)) and posorder_order > 0";

			$res = mysql_query($sql) or dberror($sql);
		
			while ($row = mysql_fetch_assoc($res))
			{            
				$project_ids[$row["project_id"]] = $row["project_id"];
			}
		}
		
		
		//check country access for region reponsables persons
		$sql = "select project_id " .
			   "from projects ".
			   "left join orders on order_id = project_order " .
			   "left join user_company_responsibles on user_company_responsible_address_id = order_client_address " . 
			   "where project_id = " . dbquote(param("pid")) . 
			   " and user_company_responsible_user_id = " . dbquote(user_id());

		$res = mysql_query($sql) or dberror($sql);
	
		if ($row = mysql_fetch_assoc($res))
		{            
			$project_ids[$row["project_id"]] = $row["project_id"];
		}
		
		
		if(!in_array( param("pid"), $project_ids))
		{
			redirect("noaccess.php");
		}

	}
}
?>