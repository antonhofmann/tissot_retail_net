<?php
/********************************************************************

    page_module_navigation.php

    Renders Modul navigation at the top right of the page

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-11-29
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-8--26
    Version:        1.0.0

    Copyright (c) 2000, Swatch AG, All Rights Reserved.

*********************************************************************/

if(isset($_SESSION["user_login"]) and strlen($_SESSION["user_login"]) > 0)
{
	echo "<div style=\"float:right;padding-top:20px;\">";
	
	echo "|&nbsp;";
	
	
	if(has_access("can_edit_catalog") or has_access("can_read_news"))
	{
		//echo "<a href=\"/gazette\" >Gazette</a>&nbsp;|&nbsp;";
	}
	if(has_access("can_publish_news_article") or has_access("can_edit_newsletter"))
	{
		//echo "<a href=\"/gazette/admin\" >Admin Gazette</a>&nbsp;|&nbsp;";
	}
	if(has_access("can_view_projects"))
	{
		echo "<a href=\"/user/projects.php\" >Projects</a>&nbsp;|&nbsp;";
	}
	
	if(has_access("has_access_to_archive"))
	{
		echo "<a href=\"/archive/projects_archive.php\" >Project Archive</a>&nbsp;|&nbsp;";
	}
	if(has_access("can_use_posindex"))
	{
		echo "<a href=\"/pos/index.php\" >POS Index</a>&nbsp;|&nbsp;";
	}
	if(has_access("can_perform_queries"))
	{
		echo "<a href=\"/mis/index.php\" >MIS</a>&nbsp;|&nbsp;";
	}
	if(has_access("has_access_to_cer"))
	{
		echo "<a href=\"/cer/index.php\" >CER/AF</a>&nbsp;|&nbsp;";
	}

	
	if(has_access("can_edit_catalog") or has_access("can_browse_catalog_in_admin"))
	{
		echo "<a href=\"/catalog/items\" >Supply Chain</a>&nbsp;|&nbsp;";
	}

	if(has_access("has_access_to_mps") or has_access("has_access_to_mps_posindex"))
	{
		echo "<a href=\"/mps\" >MPS</a>&nbsp;|&nbsp;";
	}

	if(has_access("has_access_to_lps"))
	{
		//echo "<a href=\"/lps\" >LPS</a>&nbsp;|&nbsp;";
	}

	if(has_access("has_access_to_scpps"))
	{
		//echo "<a href=\"/scpps\" >SCPPS</a>&nbsp;|&nbsp;";
	}
	
	if(has_access("can_edit_socialmedias"))
	{
		//echo "<a href=\"/uberalldata\" >UBERALL</a>&nbsp;|&nbsp;";
	}
	
	if(has_access("can_view_all_red_projects") or has_access("can_view_only_his_red_projects"))
	{
		//echo "<a href=\"/red\" >RED</a>&nbsp;|&nbsp;";
	}
	
	
	if(has_access("can_edit_catalog")
		or has_access("can_browse_catalog_in_admin")
		or has_access("can_edit_logistics_system_data")
		or has_access("can_unlock_ips"))
	{
		echo "<a href=\"/admin/index.php\" >SysAdmin</a>&nbsp;|&nbsp;";
	}
	
	echo "<a href=\"/mycompany\" >My Company</a>&nbsp;|&nbsp;";

	if(has_access("can_create_new_ideas"))
	{
		//echo '<a href="#ideabox" id="btnIdea" ><i class="fa fa-comment"></i> Idea</a>';
		/*
		echo '
			<a href="/public/themes/default/idea.html" data-fancybox-type="iframe" id="btnIdea" class="fancy-frame" data-fancybox-width="800" data-fancybox-height="620" >
				<i class="fa fa-comment"></i> Idea
			</a>
		';
		*/
	}

	echo "&nbsp;&nbsp;&nbsp;";

	echo "</div>";
}

?>