
	"snagging_templates" => array(
        "key" => "snagging_template_id",
        "references" => array(
            array("table" => "product_lines",
                  "key" => "snagging_template_product_line"))),
    "snagging_template_elements" => array(
        "key" => "snagging_template_element_id",
        "references" => array(
            array("table" => "snagging_templates",
                  "key" => "snagging_template_element_template",
                  "cascade" => true),
            array("table" => "snagging_element_types",
                  "key" => "snagging_template_element_type"))),
    "snagging_element_types" => array(
        "key" => "snagging_element_type_id",
        "references" => array()),
    "snagging_lists" => array(
        "key" => "snagging_list_id",
        "references" => array(
            array("table" => "snagging_templates",
                  "key" => "snagging_list_template",
                  "cascade" => true),
            array("table" => "projects",
                  "key" => "snagging_list_project"))
    ),
    "snagging_element_data_types" => array(
        "key" => "snagging_element_data_type_id",
        "references" => array(
            array("table" => "snagging_element_types",
                  "key" => "snagging_element_data_type_type"))
    ),
    "snagging_list_elements" => array(
        "key" => "snagging_list_element_id", 
        "references" => array(
            array("table" => "snagging_lists",
                  "key" => "snagging_list_element_list",
                  "cascade" => true),
            array("table" => "snagging_element_types",
                  "key" => "snagging_list_element_type"))
    ),
    "snagging_list_values" => array(
        "key" => "snagging_list_value_id",
        "references" => array(
            array("table" => "snagging_list_elements",
                  "key" => "snagging_list_value_element")
            )),
	