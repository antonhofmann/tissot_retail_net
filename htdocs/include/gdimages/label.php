<?php

	error_reporting(E_ERROR | E_WARNING | E_PARSE);
	
	// request vars
	$width = ($_GET['width']) ? $_GET['width'] : 120;
	$height = ($_GET['height']) ? $_GET['height'] : 32;
	$bg = ($_GET['bg']) ? $_GET['bg'] : "fff";
	$content = $_GET['content'];

	
	function html2rgb($color) {
	
		if ($color[0] == '#')  $color = substr($color, 1);
	
	    if (strlen($color) == 6) list($r, $g, $b) = array($color[0].$color[1],$color[2].$color[3],$color[4].$color[5]);
	    elseif (strlen($color) == 3) list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
	    else return list($r, $g, $b) = array(0, 0, 0);
	
	    $r = hexdec($r); $g = hexdec($g); $b = hexdec($b);
	
	    return array($r, $g, $b);
	}
	
	
	// set image
	$image = imagecreatetruecolor($width, $height);
	
	// image background
	$rgb = html2rgb($bg);
	$bg = imagecolorallocate($image, $rgb[0], $rgb[1], $rgb[2]);
	imagefill($image, 0, 0, $bg);
	
	
	if (is_array($content)) {
		
		foreach ($content as $row) {
			
			$text 	= ($row['text'])? $row['text'] 	: "";
			$size 	= ($row['size']) ? $row['size'] : 10;
			$left	= ($row['left']) ? $row['left'] : 15;
			$top 	= ($row['top']) ? $row['top'] : 10;
			$angle 	= ($row['angle']) ? $row['angle'] : 0;
			$color 	= ($row['color']) ? $row['color'] : '000';
			$align 	= ($row['align']) ? $row['align'] : null;
			$font 	= ($row['font']) ? $row['font'] : '../fonts/arial.ttf';
			
			// bounding box
			$bbox = imageftbbox($size, $angle, $font, $text);
			
			// vertival images
			if ($angle == 90) {
				
				switch ($align) {
					
					case 'bottom':
						$top = $height - $top;
					break;
					case 'center':
						$top = $bbox[1] + (imagesy($image) / 2) - ($bbox[5] / 2);
					break;
				}
			}
			else {
				
				switch ($align) {
					
					case 'right':
						$left = $width - $left;
					break;
					case 'center':
						$left = $bbox[0] + (imagesx($image) / 2) - ($bbox[4] / 2) - 5;
					break;
				}
			}
			
			$rgb = html2rgb($color);
			$textcolor = imagecolorallocate($image, $rgb[0], $rgb[1], $rgb[2]);
			imagettftext($image, $size, $angle, $left, $top, $textcolor, $font, $text);
		}
	}

	header('Content-type: image/png');
	imagepng($image);
	imagedestroy($image);
