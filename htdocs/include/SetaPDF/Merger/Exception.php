<?php
/**
 * This file is part of the SetaPDF-Merger Component
 * 
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Merger
 * @license    http://www.setasign.de/ Commercial
 * @version    $Id: Exception.php 542 2013-10-14 08:41:40Z maximilian.kresse $
 */

/**
 * Merger Exception
 * 
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Merger
 * @license    http://www.setasign.de/ Commercial
 */
class SetaPDF_Merger_Exception extends SetaPDF_Exception
{}