<?php
/**
 * This file is part of the SetaPDF package
 *
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF
 * @license    http://www.setasign.de/ Commercial
 * @version    $Id: Exception.php 542 2013-10-14 08:41:40Z maximilian.kresse $
 */

/**
 * Main exception of the SetaPDF package
 *
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF
 * @license    http://www.setasign.de/ Commercial
 */
class SetaPDF_Exception extends Exception
{
    /**
     * All Exception class constants have their own prefix byte:
     *
     *  SetaPDF_Exception                                   = 0x00
     *  SetaPDF_Core_Exception                              = 0x01
     *  SetaPDF_Core_Reader_Exception                       = 0x02
     *  SetaPDF_Core_Filter_Exception                       = 0x03
     *  SetaPDF_Core_Parser_Exception                       = 0x04
     *  SetaPDF_Core_Parser_CrossReferenceTable_Exception   = 0x05
     *  SetaPDF_Core_SecHandler_Exception                   = 0x06
     *  SetaPDF_Core_Type_Exception                         = 0x07
     *  SetaPDF_Core_Type_IndirectReference_Exception       = 0x08
     *  SetaPDF_Core_Writer_Exception                       = 0x09
     *  SetaPDF_Core_Parser_Pdf_InvalidTokenException       = 0x0a
     *  SetaPDF_Core_Font_Exception                         = 0x0b
     *  = 0x0c
     *  = 0x0d
     *  = 0x0e
     *  = 0x0f
     *  = 0x10
     *  = 0x11
     *  = 0x12
     *  = 0x13
     *  = 0x14
     *  = 0x15
     *  = 0x16
     *  = 0x17
     *  = 0x18
     *  = 0x19
     *  = 0x1a
     *  = 0x1b
     *  = 0x1c
     *  = 0x1d
     */
}