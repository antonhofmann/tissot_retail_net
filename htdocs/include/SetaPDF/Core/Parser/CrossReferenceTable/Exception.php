<?php
/**
 * This file is part of the SetaPDF-Core Component
 *
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @subpackage Parser
 * @license    http://www.setasign.de/ Commercial
 * @version    $Id: Exception.php 542 2013-10-14 08:41:40Z maximilian.kresse $
 */

/**
 * Cross reference table exception
 *
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @subpackage Parser
 * @license    http://www.setasign.de/ Commercial
 */
class SetaPDF_Core_Parser_CrossReferenceTable_Exception
    extends SetaPDF_Core_Parser_Exception
{
  /** Constants prefix: 0x05 **/
}