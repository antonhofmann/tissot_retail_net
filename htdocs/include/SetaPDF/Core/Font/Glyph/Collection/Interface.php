<?php
/**
 * This file is part of the SetaPDF-Core Component
 *
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @subpackage Font
 * @license    http://www.setasign.de/ Commercial
 * @version    $Id: Interface.php 542 2013-10-14 08:41:40Z maximilian.kresse $
 */

/**
 * An interface for glyph collections
 *
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @subpackage Font
 * @license    http://www.setasign.de/ Commercial
 */
interface SetaPDF_Core_Font_Glyph_Collection_Interface
{
    /**
     * Get the glyph width of a single character
     * @param string $char
     * @param string $encoding
     */
    public function getGlyphWidth($char, $encoding = 'UTF-16BE');

    /**
     * Get the glyphs width of a string
     *
     * @param string $chars
     * @param string $encoding
     */
    public function getGlyphsWidth($chars, $encoding = 'UTF-16BE');
}