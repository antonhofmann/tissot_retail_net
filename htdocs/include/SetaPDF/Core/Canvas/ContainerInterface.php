<?php
/**
 * This file is part of the SetaPDF-Core Component
 *
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @license    http://www.setasign.de/ Commercial
 * @version    $Id: ContainerInterface.php 542 2013-10-14 08:41:40Z maximilian.kresse $
 */

/**
 * An interface for objects which contains a canvas object.
 *
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @subpackage Canvas
 * @license    http://www.setasign.de/ Commercial
 */
interface SetaPDF_Core_Canvas_ContainerInterface
{
    /**
     * Get the indirect object of the container
     *
     * This could be an object holding a dictionary or a stream
     *
     * @return SetaPDF_Core_Type_IndirectObject
     */
    public function getObject();

    /**
     * Get the stream proxy object
     *
     * @return SetaPDF_Core_Canvas_StreamProxyInterface
     */
    public function getStreamProxy();

    /**
     * Get the width for the canvas
     *
     * @return float
     */
    public function getWidth();

    /**
     * Get the height for the canvas
     *
     * @return float
     */
    public function getHeight();
}