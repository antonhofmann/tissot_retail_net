<?php
/**
 * This file is part of the SetaPDF-Core Component
 * 
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @subpackage SecHandler
 * @license    http://www.setasign.de/ Commercial
 * @version    $Id: Interface.php 542 2013-10-14 08:41:40Z maximilian.kresse $
 */

/**
 * Security handler interface
 * 
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @subpackage SecHandler
 * @license    http://www.setasign.de/ Commercial
 */
interface SetaPDF_Core_SecHandler_Interface
{
    /**
     * Returns the encryption dictionary
     * 
     * @return SetaPDF_Core_Type_Dictionary
     */
    public function getEncryptionDictionary();
    
    /**
     * Encrypts stream data through the desired security handler
     * 
     * @param string $data
     * @param mixed $param 
     */
    public function encryptStream($data, $param = null);
    
    /**
     * Encrypts string data through the desired security handler
     * 
     * @param string $data
     * @param mixed $param 
     */
    public function encryptString($data, $param = null);
    
    /**
     * Decrypts stream data through the desired security handler
     * 
     * @param string $data
     * @param null|array|SetaPDF_Core_Type_IndirectObject $param An array of possible arguments
     */
    public function decryptStream($data, $param = null);
    
    /**
     * Decrypts string data through the desired security handler
     * 
     * @param string $data
     * @param null|array|SetaPDF_Core_Type_IndirectObject $param An array of possible arguments
     */
    public function decryptString($data, $param = null);
    
    /**
     * Authenticate to the document with given credentials 
     * 
     * @param mixed $data Credentials data
     * @return boolean Authentification was successfull or not
     */
    public function auth($data = null);
    
    /**
     * Returns the status if the handler is authenticated and ready
     * to en- and decrypt strings or streams
     * 
     * @return boolean
     */
    public function isAuth();
    
    /**
     * Queries if a permission is granted
     * 
     * @param integer $permission
     */
    public function getPermission($permission);
    
    /**
     * Returns all rights
     * 
     * @return integer
     */
    public function getPermissions();
    
    /**
     * Returns the needed PDF version for this security handler
     * 
     * @return string
     */
    public function getPdfVersion();
    
    /**
     * Returns true if the metadata are/will be encrypted
     * 
     * @return boolean
     */
    public function getEncryptMetadata();
}