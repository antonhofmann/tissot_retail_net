<?php
/**
 * This file is part of the SetaPDF-Core Component
 *
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @subpackage Writer
 * @license    http://www.setasign.de/ Commercial
 * @version    $Id: Interface.php 542 2013-10-14 08:41:40Z maximilian.kresse $
 */

/**
 * The writer interface
 *
 * @copyright  Copyright (c) 2013 Setasign - Jan Slabon (http://www.setasign.de)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @subpackage Writer
 * @license    http://www.setasign.de/ Commercial
 */
interface SetaPDF_Core_Writer_Interface
    extends SetaPDF_Core_WriteInterface
{
    /**
     * Method called when the writing process starts
     *
     * This method could send for example headers
     */
    public function start();

    /**
     * This method is called when the writing process is finished
     *
     * It could close a file handle for example or send headers and
     * flushs a buffer
     */
    public function finish();

    /**
     * Get the current writer status
     *
     * @see SetaPDF_Core_Writer
     * @return integer
     */
    public function getStatus();

    /**
     * Gets the current position/offset
     *
     * @return integer
     */
    public function getPos();

    /**
     * Method called if a documents cleanUp-method is called
     */
    public function cleanUp();
}