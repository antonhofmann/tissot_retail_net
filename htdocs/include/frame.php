<?php
/********************************************************************

    frame.php

    Performs global initialization and includes other modules.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-28
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-11-25
    Version:        1.1.4
    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
date_default_timezone_set('Europe/Zurich');


require_once("check_url_parameters.php");

define("BRAND", "Tissot");
define("APPLICATION_NAME", "Tissot Retail Net");
define("MAIL_SUBJECT_PREFIX", "Tissot Retail Net");
define("APPLICATION_URL", "https://retailnet.tissot.ch");
define("APPLICATION_URLPART", "retailnet.tissot");
define("MAPS_HOST", "maps.google.com");
define("STATIC_MAPS_HOST", "https://maps.googleapis.com/maps/api/staticmap");
define("BRAND_WEBSITE", "tissot.ch");


/********************************************************************
    Developper Vars
*********************************************************************/


global $senmail_activated; // true to tell the system to send mails
if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
{
	$senmail_activated = true;
}
else
{
	$senmail_activated = false;
}
ini_set("include_path", "./");

/********************************************************************
    Pre-Initialization
*********************************************************************/
global $start_time;
$start_time = time();

error_reporting(E_ALL);
//ini_set("magic_quotes_gpc", 0);
//ini_set("magic_quotes_runtime", 0);
//set_magic_quotes_runtime(false);

if (!isset($_SERVER))
{
    $_SERVER = $_SERVER;
}

if (!isset($_REQUEST))
{
    $_REQUEST = array_merge($HTTP_GET_VARS, $HTTP_POST_VARS, $HTTP_POST_FILES);
}
else
{
    $_REQUEST = array_merge($_REQUEST, $_FILES);
}

ini_set("include_path", ini_get("include_path") . ":" . $_SERVER["DOCUMENT_ROOT"] . "/include");

/********************************************************************
    Includes
*********************************************************************/


require_once "util.php";
require_once "db.php";
require_once "page.php";
require_once "popup_page.php";
if(!isset($exclude_form))
{
	require_once "form.php";
}
require_once "list.php";
require_once "mail.php";
//require_once "phpmailer/class.phpmailer.php";
//set_error_handler ('error_handler');

// mvc autoloader
require_once $_SERVER['DOCUMENT_ROOT'].'/public/autorun.php';
require_once $_SERVER['DOCUMENT_ROOT']. '/vendor/phpmailer/phpmailer/class.phpmailer.php';

// attach compiler files

// attach compiler files
// attach compiler files
Compiler::attach(array(
	"/public/css/gui.css",
	"/public/themes/default/css/form.css",
	"/public/themes/default/css/modal.css",
	"/public/scripts/fancybox/fancybox.css",
	"/public/scripts/fancybox/fancybox.js",
	"/public/scripts/jgrowl/jgrowl.css",
	"/public/scripts/jgrowl/jgrowl.js",
	"/public/scripts/spin/spin.min.js",
	"/public/scripts/adomodal/adomodal.css",
	"/public/scripts/adomodal/adomodal.js",
	"/public/scripts/jquery.actual.min.js",
	"/public/css/spinners.css",
	"/public/scripts/message.js",
	"/public/scripts/retailnet.js"
));



/********************************************************************
    Initialization
*********************************************************************/

// Determine browser capabilities
/*
$bad_browser = preg_match("/^Mozilla\/1/i", $_SERVER["HTTP_USER_AGENT"])
    || preg_match("/^Mozilla\/2/i", $_SERVER["HTTP_USER_AGENT"])
    || preg_match("/^Mozilla\/3/i", $_SERVER["HTTP_USER_AGENT"])
    || preg_match("/^Mozilla\/4/i", $_SERVER["HTTP_USER_AGENT"]);

$msie_browser = preg_match("/MSIE (\d+\.\d+)/i", $_SERVER["HTTP_USER_AGENT"], $matches)
    && $matches[1] >= 5.0
    && !preg_match("/Opera/i", $_SERVER["HTTP_USER_AGENT"]);

*/


//used because 2013 LN and CER were made completely new
//old forms and print outs must still appear in the old form
define("DATE_NEW_LN_FORM_2013", "2013-04-30");
define("DATE_NEW_CER_FORM_2013", "2013-04-30");
define("DATE_NEW_RENTAL_TOOL_2014", "2014-05-31");
define("DATE_NEW_BUSINESS_PLAN", "2016-10-01");
define("DATE_NEW_APPROVAL_NAMES", "2016-09-01");

// Open database

$context = "";

ini_set("default_charset", "utf-8");
if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
{

    $db = mysql_connect("localhost", "retailnet", "gkgNNeXrq5iz7xaX");
    $result = mysql_select_db("db_retailnet", $db);

	if(!$result) {
		//redirect("/service_not_available.php");
	}
	
	define("RETAILNET_SERVER", "localhost");
	define("RETAILNET_DB", "db_retailnet");
	define("RETAILNET_USER", "retailnet");
	define("RETAILNET_PASSWORD", "gkgNNeXrq5iz7xaX");

	define("STORE_LOCATOR_SERVER", "localhost");
	define("STORE_LOCATOR_DB", "db_storelocator");
	define("STORE_LOCATOR_USER", "retailnet");
	define("STORE_LOCATOR_PASSWORD", "gkgNNeXrq5iz7xaX");

		
	//proxy server Swisscom
	define("PROXY_SERVER", "proxy01.sharedit.ch:8082");
	$opts	= array(
		'http'	=> array(
			'proxy'				=> 'proxy01.sharedit.ch:8082',
			'request_fulluri'	=> true
		)
	);

	$context = stream_context_create($opts);

}
else
{
	$db = mysql_connect("localhost", "root", "root");
    $result = mysql_select_db("tissot_db_retailnet", $db);
	//$result = mysql_select_db("db_retailnet", $db);

	if(!$result) {
		redirect("/service_not_available.php");
	}

	define("RETAILNET_SERVER", "localhost");
	define("RETAILNET_DB", "tissot_db_retailnet");
	//define("RETAILNET_DB", "db_retailnet");
	define("RETAILNET_USER", "root");
	define("RETAILNET_PASSWORD", "root");

	define("STORE_LOCATOR_SERVER", "localhost");
	define("STORE_LOCATOR_DB", "db_storelocator");
	define("STORE_LOCATOR_USER", "root");
	define("STORE_LOCATOR_PASSWORD", "root");

	
	define("PROXY_SERVER", "");
}

mysql_query( "SET NAMES 'utf8'");
mysql_query( "set sql_mode = ''");

date_default_timezone_set ( "Europe/Zurich" );


if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
{
	define("GOOGLE_API_KEY", "AIzaSyD2wMUs74upe6ZvEOflrrMDRCoPYABu1Qs");
	define("GOOGLE_API_KEY_GEO", "AIzaSyCT0qo4PQAkgZuT3A2niAE9Gvqp6fir0G0");

	define("TMP_FILE_DIR_ABSOLUTE", "/data/www/retailnet.tissot.ch/files/tmp/");
	define("TMP_FILE_DIR_RELATIVE", "../files/tmp/");

		
}
else
{
	define("GOOGLE_API_KEY", "ABQIAAAANMWovGK6w72eKOOPCsyWJBTIE1u1rkD0AV69z6wMUCuqr_LikRRrmkhJgaCxFROqt4Pkuys8jceQ4g");
	define("TMP_FILE_DIR_ABSOLUTE", "c:/webs/tissot/htdocs/tmp/");
	define("TMP_FILE_DIR_RELATIVE", "../tmp/");
}

// Prepare REQUEST variable

if (isset($_COOKIE))
{
    foreach (array_keys($_COOKIE) as $cookie)
    {
        unset($_REQUEST[$cookie]);
    }
}

// Start session
session_name("retailnet");
if (!isset($SUPPRESS_HEADERS))
{
	session_start();
/*
    if (!isset($_SESSION))
    {
        $_SESSION = &$HTTP_SESSION_VARS;
    }*/
}



if(isset($_GET)) {
	foreach($_GET as $key => $value) {
		$myvar = $key;
		$$myvar = empty($value) ? "" : trim($value);
	}
}
/*
if(isset($_POST)) {
	foreach($_POST as $key => $value) {
		$myvar = $key;
		$$myvar = empty($value) ? "" : trim($value);
	}
}
*/

// Register commonly used parameters

register_param("id");
register_param("order");
register_param("filter");

require_once("check_access.php");


?>