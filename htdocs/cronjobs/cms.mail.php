<?php

// cronjob is being called directly: include header file
if (!defined('PATH_APPLICATIONS')) {
	$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';
}

require_once PATH_CONTROLLERS . '/cron_cmsmailcontroller.php';
$Controller = new Cron_CMSMailController();
$Controller->run();