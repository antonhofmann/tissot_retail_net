<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';


$_DIR_RAMCO = $_SERVER['DOCUMENT_ROOT']."/ramco_exchange";
dir::make($_DIR_RAMCO);

// insert: null
// update: product group, product subbroup, collection, collection category, reference
$content  = 'PR';
$content .= '08';
$content .= 'S11';
$content .= str_pad('Gent', 100);
$content .= 'S11B';
$content .= str_pad('Gent Standard *', 100);
$content .= str_pad('GZ270', 16);
$content .= str_pad('Swatch Roland-Garros Blanche *', 40);
$content .= str_pad('SS2014', 16);
$content .= str_pad('1107 Life Style', 40);
$content .= '01';
$content .= str_pad('120.00', 8);
$content .= 'CHF';
$content .= "\n\r";

// insert: product group, product subbroup
// update: collection, collection category, reference
$content .= 'PR';
$content .= '08';
$content .= 'AA1';
$content .= str_pad('New Product Group', 100);
$content .= 'AAAA';
$content .= str_pad('New Product Subgroup *', 100);
$content .= str_pad('ASE500', 16);
$content .= str_pad('Ase500 description *', 40);
$content .= str_pad('SS2013', 16);
$content .= str_pad('1120 Lady Pink', 40);
$content .= '01';
$content .= str_pad('120.00', 8);
$content .= 'CHF';
$content .= "\n\r";


// insert: product group, product subbroup, collection, collection category, reference
// update: null
$content .= 'PR';
$content .= '08';
$content .= 'BB2';
$content .= str_pad('New Product Group 2', 100);
$content .= 'BBBB';
$content .= str_pad('New Product Subgroup *', 100);
$content .= str_pad('ASE1000', 16);
$content .= str_pad('ASE1000 description *', 40);
$content .= str_pad('SS2015', 16);
$content .= str_pad('1200 Medias *', 40);
$content .= '01';
$content .= str_pad('200', 8);
$content .= 'CHF';
$content .= "\n\r";


$timestamp = date('YmdHis').'.'.str_pad(substr((float)microtime(), 2), 4, '0', STR_PAD_LEFT);
$filename = "$_DIR_RAMCO/lps.references.$timestamp.txt";
$handle = fopen($filename, "x+");
fwrite($handle, $content);
fclose($handle);
chmod($filename, 0755);