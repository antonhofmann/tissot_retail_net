<?php
/********************************************************************

	transfer_data_to_trevision.php

    transfer data to trevision

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-12-27
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-12-27
    Version:        1.0.0

	Contact:		Alexander Palnik 
					syreta gmbh e-business solutions
					maria-theresia-str. 51 | a-4600 Wels
					t   +43|7242-93 490
					a.palnik@syreta.com


    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    strtodate
*********************************************************************/

function strtodate($str)
{
    $parts = preg_split("/[.-]+/", $str);

    if (count($parts) != 3)
    {
        return false;
    }

    list($day, $month, $year) = $parts;

    if ($day > 31 && $year <= 31)
    {
        $temp = $day;
        $day = $year;
        $year = $temp;
    }

    if ($year == 0)
    {
        $year = 2000;
    }

    if (!checkdate((int)$month, (int)$day, (int)$year))
    {
        return false;
    }
	$day = substr($day,0,2);

	//echo "->" . $day;
    return mktime(0, 0, 0, $month, $day, $year);
}

/********************************************************************
    to_system_date
*********************************************************************/

function to_system_date($date)
{
	if ($date == "0000-00-00" or $date == NULL)
    {
        
		return "";
		
    }

    if ($date)
    {
        return date("d.m.y", strtodate($date));
    }
    else
    {
        return "";
    }
}


$db = mysql_connect("10.96.12.41", "retailnet", "uDUjbayVe2FeCTsZ");
$result = mysql_select_db("db_retailnet-utf8", $db);

//ftp_data
$ftp_server = "trevisiondfc.syreta.com";
$ftp_user_name = "swatch";
$ftp_user_pass = "sw!atch98";
$remote_file = "retailnet_projects.csv";
//$local_file = $_SERVER["DOCUMENT_ROOT"] . "/cronjobs/retailnet_projects.csv";

$local_file = "/data/www/retailnet.tissot.ch/htdocs/cronjobs/retailnet_projects.csv";

$file_content = "";

$sql = "select project_number,  project_real_opening_date " . 
		"from projects " . 
		"left join orders on project_order = order_id " . 
		"where (order_archive_date is null or order_archive_date = '0000-00-00') " .
		"and order_actual_order_state_code < '820' " . 
		"order by project_number";

$res = mysql_query($sql);
while ($row = mysql_fetch_assoc($res))
{
	$file_content .= $row["project_number"] . ';' . to_system_date($row["project_real_opening_date"]) . "\r\n";
}



$file = fopen($local_file, "w");
fwrite($file, $file_content);
fclose($file);

//transfer data
$conn_id = ftp_connect($ftp_server);

// login with username and password
$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

if (ftp_put($conn_id, $remote_file, $local_file, FTP_ASCII)) {
 
} 
else {
 
}

// close the connection
ftp_close($conn_id);

?>