<?php
$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';

//ini_set('memory_limit', '4096M');

$_TEMPLATE = 30;

// has sap pos locations new imports :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$model = new Model();

$sth = $model->db->prepare("
	SELECT COUNT(sap_imported_posaddress_id) AS total
	FROM sap_imported_posaddresses
	WHERE sap_imported_posaddress_notified IS NULL OR sap_imported_posaddress_notified != 1
");

$sth->execute();
$result = $sth->fetch();



// send mail alert :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($result['total'] > 0) {

	$actionMail = new ActionMail($_TEMPLATE);
	
	// link to sap imports
	$protocol = Settings::init()->http_protocol.'://';
	$server = $_SERVER['SERVER_NAME'];
	$link = '/mps/sapimport';

	$actionMail->setDataloader(array('link'=>$protocol.$server.$link));

	$actionMail->send();
	// update system as notified
	$model->db->exec("UPDATE sap_imported_posaddresses SET sap_imported_posaddress_notified = 1");
}