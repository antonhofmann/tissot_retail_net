<?php 

$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

ini_set('memory_limit', '4096M');
ini_set('max_execution_time', 7200);
set_time_limit(7200);

Connector::instance();

$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);
$model = new Model(Connector::DB_RETAILNET_MPS);

$result = $model->query("
	SELECT GROUP_CONCAT(DISTINCT mps_ordersheet_item_purchase_order_number) AS pons
	FROM mps_ordersheets 
	INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_ordersheet_id = mps_ordersheet_id
	WHERE mps_ordersheet_item_quantity_approved > 0
	AND (
		(
			mps_ordersheet_workflowstate_id = 9 
			AND mps_ordersheet_item_quantity_confirmed > 0
		) 
		OR (
			mps_ordersheet_workflowstate_id IN (9,10)
			AND mps_ordersheet_item_quantity_shipped > 0
		)
	)
")->fetch();

$pons = $result['pons'];
$pons = $pons ? explode(',', $pons) : array();

if (!$pons) die;


$pons_filter = "'".join("', '", $pons)."'";

$result = $model->query("
	SELECT mps_ordersheet_item_id, mps_ordersheet_item_ordersheet_id
	FROM mps_ordersheet_items
	WHERE mps_ordersheet_item_purchase_order_number IN ($pons_filter)
")->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$ordersheets[] = $row['mps_ordersheet_item_ordersheet_id'];
		$items[] = $row['mps_ordersheet_item_id'];
	}
}

if (!$ordersheets || !$items) die;


$ordersheets = array_unique($ordersheets);
$items = array_unique($items);

foreach ($ordersheets as $id) {
	
	$model->db->exec("
		UPDATE mps_ordersheets SET
			mps_ordersheet_workflowstate_id = 9
		WHERE mps_ordersheet_id = $id
	");
	
	$model->db->exec("
		DELETE FROM user_trackings
		WHERE user_tracking_entity = 'order sheet' 
		AND user_tracking_action IN ('order confirmed', 'partially shipped', 'shipped')  
		AND user_tracking_entity_id $id
	");
}

foreach ($items as $id) {
	
	$model->db->exec("
		UPDATE mps_ordersheet_items SET
			mps_ordersheet_item_quantity_confirmed = NULL,
		    mps_ordersheet_item_quantity_shipped = NULL
		WHERE mps_ordersheet_item_id = $id
	");
}

foreach ($pons as $pon) {
	
	$model->db->exec("
		DELETE FROM mps_ramco_orders
		WHERE mps_ramco_order_po_number = '$pon'
	");

	$model->db->exec("
		DELETE FROM mps_ordersheet_item_confirmed
		WHERE mps_ordersheet_item_confirmed_po_number = '$pon'
	");

	$model->db->exec("
		DELETE FROM mps_ordersheet_item_shipped
		WHERE mps_ordersheet_item_shipped_po_number = '$pon'
	");

	$exchange->db->exec("
		UPDATE db_retailnet_ramco_exchange.ramco_orders SET 
			mps_order_completely_imported = NULL
		WHERE TRIM(mps_salesorder_ponumber) = '$pon'
	");

	$exchange->db->exec("
		UPDATE db_retailnet_ramco_exchange.ramco_confirmed_items SET 
			mps_item_completely_imported = NULL
		WHERE TRIM(mps_salesorder_ponumber) = '$pon'
	");

	$exchange->db->exec("
		UPDATE db_retailnet_ramco_exchange.ramco_shipped_items SET
			mps_item_completely_imported = NULL
		WHERE TRIM(mps_salesorderitem_ponumber) = '$pon'
	");
}
