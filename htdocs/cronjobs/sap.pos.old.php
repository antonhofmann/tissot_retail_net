<?php

	$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';

	// shell scripting
	Connector::instance();

	$console = array();
	
	// exchange model
	$exchangeModel = new Model(Connector::DB_RETAILNET_EXCHANGES);
	
	// db retailnet
	$model = new Model(Connector::DB_CORE);
	
	$result = $exchangeModel->query("
		SELECT 
			sap_poslocation_id AS id,
			TRIM(sap_poslocation_sapnr) AS sap_imported_posaddress_sapnr,
			TRIM(sap_poslocation_name1) AS sap_imported_posaddress_name1,
			TRIM(sap_poslocation_name2) AS sap_imported_posaddress_name2,
			TRIM(sap_poslocation_search_item) AS sap_imported_posaddress_search_item,
			TRIM(sap_poslocation_address) AS sap_imported_posaddress_address,
			TRIM(sap_poslocation_zip) AS sap_imported_posaddress_zip,
			TRIM(sap_poslocation_city) AS sap_imported_posaddress_city,
			TRIM(sap_poslocation_country) AS sap_imported_posaddress_country,
			TRIM(sap_poslocation_phone) AS sap_imported_posaddress_phone,
			TRIM(sap_poslocation_fax) AS sap_imported_posaddress_fax,
			TRIM(sap_poslocation_sales_organization) AS sap_imported_posaddress_sales_organization,
			TRIM(sap_poslocation_distribution_channel) AS sap_imported_posaddress_distribution_channel,
			TRIM(sap_poslocation_division) AS sap_imported_posaddress_division,
			TRIM(sap_poslocation_order_block) AS sap_imported_posaddress_order_block,
			TRIM(sap_poslocation_order_block_sales_area) AS sap_imported_posaddress_order_block_sales_area,
			TRIM(sap_poslocation_deletion_indicator) AS sap_imported_posaddress_deletion_indicator,
			TRIM(sap_poslocation_deletion_sales_area_indicator) AS sap_imported_posaddress_deletion_sales_area_indicator
		FROM sap_poslocations
		WHERE 
			TRIM(sap_poslocation_imported_into_mps) IS NULL 
			OR TRIM(sap_poslocation_imported_into_mps) = ''
			OR TRIM(sap_poslocation_imported_into_mps) = 0 
	")->fetchAll();

	if ($result) {
		
		if ($application) {
			$console[] = "DATA: found ".count($result)." records.";
		}
		
		// insert statement
		$sth = $model->db->prepare("
			INSERT INTO sap_imported_posaddresses (
				sap_imported_posaddress_sapnr,
				sap_imported_posaddress_name1,
				sap_imported_posaddress_name2,
				sap_imported_posaddress_search_item,
				sap_imported_posaddress_address,
				sap_imported_posaddress_zip,
				sap_imported_posaddress_city,
				sap_imported_posaddress_country,
				sap_imported_posaddress_phone,
				sap_imported_posaddress_fax,
				sap_imported_posaddress_sales_organization,
				sap_imported_posaddress_distribution_channel,
				sap_imported_posaddress_division,
				sap_imported_posaddress_order_block,
				sap_imported_posaddress_deletion_indicator,
				sap_imported_posaddress_deletion_sales_area_indicator,
				sap_imported_posaddress_order_block_sales_area,
				sap_imported_posaddress_date_updated,
				date_created,
				user_created
			) VALUES (
				:sap_imported_posaddress_sapnr,
				:sap_imported_posaddress_name1,
				:sap_imported_posaddress_name2,
				:sap_imported_posaddress_search_item,
				:sap_imported_posaddress_address,
				:sap_imported_posaddress_zip,
				:sap_imported_posaddress_city,
				:sap_imported_posaddress_country,
				:sap_imported_posaddress_phone,
				:sap_imported_posaddress_fax,
				:sap_imported_posaddress_sales_organization,
				:sap_imported_posaddress_distribution_channel,
				:sap_imported_posaddress_division,
				:sap_imported_posaddress_order_block,
				:sap_imported_posaddress_order_block_sales_area,
				:sap_imported_posaddress_deletion_indicator,
				:sap_imported_posaddress_deletion_sales_area_indicator,
				CURDATE(),
				NOW(),
				:user_created
			)	
		");
		
		foreach ($result as $data) {
			
			// exchange id
			$key = array_shift($data);
			
			$data['user_created'] =  'ramco';
			
			$response = $sth->execute($data);
			
			if ($response) {

				if ($application) {
					$inserted = $model->db->lastInsertId();
					$console[] = "Import record $inserted";
				}
				
				$response = $exchangeModel->db->exec("
					UPDATE sap_poslocations SET
						sap_poslocation_imported_into_mps = 1
					WHERE TRIM(sap_poslocation_id) = $key
				");
				
				if ($application) {
					if ($response) {
						$console[] = "Set exchange record $key as imported";
					} else {
						$console[] = "Error on set exchange record $key as imported";
					}
				}
				
			} elseif ($application) {
				$console[] = "Error on import record $key";
			}
		}
	}

	// for app call
	if ($application && $console) {
		echo json_encode($console);
	}
	
	