<?php 

$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';

ini_set('memory_limit', '6096M');
ini_set('max_execution_time', 7200);
set_time_limit(7200);

define(DEBUGGING_MODE, $_REQUEST['debug'] ?: false);

$application = $_REQUEST['application'];
$requestPon = $_REQUEST['pon'];

$_ERRORS = array();
$_FAILURES = array();
$_CONSOLE = array();
$_NOTIFICATIONS = array();
$_DATA = array();
$_PONS = array();

// start import
$_CONSOLE[] = "START: ".date('d.m.Y H:i:s');

if (DEBUGGING_MODE) {
	$_CONSOLE[] = "DEBUGGING MODE active";
}

// shell scripting
if (!$application) {
	Connector::instance();
	$_CONSOLE[] = "Cronjob Mode, set connector for live server: swatch.com";
}

// exchange server
$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);

// connector identificators
$connectors = array(
	'08' => Connector::DB_RETAILNET_MPS
);

// db models
$models = array(
	'08' => new Model(Connector::DB_RETAILNET_MPS)
);

/**
 * CLEANUP EXCHANGE ORDERS
 * Cleanup unused/double orders
 **************************************************************************************************/

$exchange->db->exec("
	DELETE FROM db_retailnet_ramco_exchange.ramco_orders
	WHERE ramco_order_id NOT IN (
		SELECT * FROM (
			SELECT MAX(ramco_order_id) AS ramco_order_id
			FROM db_retailnet_ramco_exchange.ramco_orders 
			GROUP BY REPLACE(LOWER(mps_salesorder_ponumber), ' ', '')
		) as maxids
	)
");

$exchange->db->exec("
	DELETE FROM db_retailnet_ramco_exchange.ramco_confirmed_items
	WHERE ramco_confirmed_id NOT IN (
		SELECT ramco_confirmed_id FROM (
			SELECT MAX(ramco_confirmed_id) AS ramco_confirmed_id
			FROM db_retailnet_ramco_exchange.ramco_confirmed_items 
			GROUP BY 
				REPLACE(LOWER(mps_salesorderitem_material_code), ' ', ''),
				REPLACE(LOWER(mps_salesorder_ponumber), ' ', '')
		) as maxids
	)
");

$exchange->db->exec("
	DELETE FROM db_retailnet_ramco_exchange.ramco_shipped_items
	WHERE ramco_shipped_id NOT IN (
		SELECT ramco_shipped_id FROM (
			SELECT MAX(ramco_shipped_id) AS ramco_shipped_id
			FROM db_retailnet_ramco_exchange.ramco_shipped_items 
			GROUP BY 
				REPLACE(LOWER(mps_salesorderitem_material_code), ' ', ''),
				REPLACE(LOWER(mps_salesorderitem_ponumber), ' ', '')
		) as maxids
	)
");


//clean data befor import
$exchange->db->exec("
	UPDATE db_retailnet_ramco_exchange.ramco_confirmed_items
	SET mps_salesorder_ponumber = REPLACE(mps_salesorder_ponumber,'\'', '')
	
");

$exchange->db->exec("
	UPDATE db_retailnet_ramco_exchange.ramco_shipped_items
	SET mps_salesorderitem_ponumber = REPLACE(mps_salesorderitem_ponumber,'\'', '')
	
");


/**
 * ORDERS
 * Get all not imported orders or selectd PO order
 **************************************************************************************************/
		
$_ORDERS = array();
$_ORDERS_DIVISION = array();

$filterRamcoOrders = $requestPon 
	? "REPLACE(LOWER(mps_salesorder_ponumber), ' ', '') = '$requestPon' "
	: "(
		mps_order_completely_imported IS NULL 
		OR mps_order_completely_imported = '' 
		OR mps_order_completely_imported = 0
	)";

$result = $exchange->query("
	SELECT 
		TRIM(ramco_order_type) AS ramco_order_type,
		TRIM(mps_salesorder_customernumber) AS mps_salesorder_customernumber,
		TRIM(mps_salesorder_ponumber) AS mps_salesorder_ponumber,
		TRIM(ramco_sold_to_sapnr) AS ramco_sold_to_sapnr,
		TRIM(ramco_sales_order_number) AS ramco_sales_order_number,
		TRIM(ramco_shipto_number) AS ramco_shipto_number,
		TRIM(ramco_billto_number) AS ramco_billto_number,
		TRIM(ramco_order_confirmation_date) AS ramco_order_confirmation_date,
		TRIM(ramco_payment_condition) AS ramco_payment_condition,
		TRIM(ramco_shipment_mode) AS ramco_shipment_mode,
		TRIM(ramco_inco_terms) AS ramco_inco_terms,
		TRIM(ramco_delivered_by) AS ramco_delivered_by,
		TRIM(ramco_employee) AS ramco_employee,
		TRIM(ramco_distribution_code) AS ramco_distribution_code,
		TRIM(ramco_sales_organisation) AS ramco_sales_organisation,
		TRIM(ramco_division) AS ramco_division,
		TRIM(ramco_order_reason) AS ramco_order_reason,
		TRIM(ramco_order_deleted) AS ramco_order_deleted,
		TRIM(mps_order_completely_imported) AS mps_order_completely_imported
	FROM ramco_orders 
	WHERE $filterRamcoOrders
")->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$pon = $row['mps_salesorder_ponumber'];
		$division = $row['ramco_division'];

		if ($pon && $division) {
			$_ORDERS_DIVISION[$pon] = $division;
			$_ORDERS[$pon] = $row;
		}
		else $_FAILURES['ramco.orders'][] = "Missing data: PON($pon) or division($division)";
	}
} 
else {
	$_CONSOLE[] = "Ramco orders not found";
	goto BLOCK_RESPONSE;
}


/**
 * CONFIRMED ORDERS
 * Get all not imported confirmed orders or selectd PO order
 **************************************************************************************************/

$_IMPORT_CONFIRMED_ITEMS = array();
$_IMPORT_CONFIRMED_ITEMS_FAILURES = array();

$filterConfirmedItems = $requestPon
	? "REPLACE(LOWER(mps_salesorder_ponumber), ' ', '') = '$requestPon' "
	: "(
		mps_item_completely_imported IS NULL 
		OR mps_item_completely_imported = '' 
		OR mps_item_completely_imported = 0
	) AND date_created IS NOT NULL AND date_created >= DATE_SUB(NOW(), INTERVAL 360 DAY)";

$result = $exchange->query("
	SELECT 
		TRIM(mps_salesorder_ponumber) AS mps_salesorder_ponumber,
		TRIM(mps_salesorderitem_linenumber) AS mps_salesorderitem_linenumber,
		TRIM(mps_salesorderitem_material_code) AS mps_salesorderitem_material_code,
		TRIM(ramco_eannumber) AS ramco_eannumber,
		TRIM(ramco_confirmed_quantity) AS ramco_confirmed_quantity,
		TRIM(ramco_confirmed_price) AS ramco_confirmed_price,
		TRIM(ramco_estimated_departure_date) AS ramco_estimated_departure_date,
		TRIM(ramco_requested_delivery_date) AS ramco_requested_delivery_date,
		TRIM(ramco_price_discount_code) AS ramco_price_discount_code,
		TRIM(ramco_price_discount_percent) AS ramco_price_discount_percent,
		TRIM(ramco_order_state) AS ramco_order_state,
		TRIM(ramco_confirmation_state) AS ramco_confirmation_state,
		TRIM(mps_item_completely_imported) AS mps_item_completely_imported,
		LOWER(REPLACE(mps_salesorderitem_material_code, ' ', '')) AS code
	FROM ramco_confirmed_items
	WHERE $filterConfirmedItems
")->fetchAll();
	
if ($result) {

	foreach ($result as $row) {
		
		$code = $row['code'];
		$pon = $row['mps_salesorder_ponumber'];
		$division = $_ORDERS_DIVISION[$pon];

		if (!$code || !$pon || !$division) {
			$_FAILURES['order.confirmed'][] = "Missing data: material ($code) or PON($pon) or division($division)";
			continue;
		}
		
		// confirmed items
		$_IMPORT_CONFIRMED_ITEMS[$division][$pon][$code] = $row;

		// involved pons
		$_PONS[$pon] = $pon;
	}

} 
else $_CONSOLE[] = "Confirmed orders not found";


/**
 * SHIPPED ORDERS
 * Get all not imported shipped orders or selectd PO order
 **************************************************************************************************/

$_IMPORT_SHIPPED_ITEMS = array();

$filterShippedOrders = $requestPon 
	? " AND REPLACE(LOWER(mps_salesorderitem_ponumber), ' ', '') = '$requestPon' " 
	: " AND (
		mps_item_completely_imported IS NULL 
		OR mps_item_completely_imported = '' 
		OR mps_item_completely_imported != 1
	) AND date_created IS NOT NULL AND date_created >= DATE_SUB(NOW(), INTERVAL 360 DAY)";

$result = $exchange->query("
	SELECT 
		TRIM(mps_salesorderitem_ponumber) AS mps_salesorderitem_ponumber,
		TRIM(mps_salesorderitem_linenumber) AS mps_salesorderitem_linenumber,
		TRIM(mps_salesorderitem_material_code) AS mps_salesorderitem_material_code,
		TRIM(ramco_shipping_date) AS ramco_shipping_date,
		TRIM(ramco_shipped_quantity) AS ramco_shipped_quantity,
		TRIM(ramco_order_state) AS ramco_order_state,
		TRIM(ramco_order_shortclosed) AS ramco_order_shortclosed,
		TRIM(mps_salesorderitem_ean_number) AS mps_salesorderitem_ean_number,
		LOWER(REPLACE(mps_salesorderitem_material_code, ' ', '')) AS code
	FROM ramco_shipped_items
	WHERE 
		REPLACE(ramco_order_state, ' ', '') = '03'
		$filterShippedOrders
")->fetchAll();
		
if ($result) {

	foreach ($result as $row) {
		
		$code = $row['code'];
		$pon = $row['mps_salesorderitem_ponumber'];
		$division = $_ORDERS_DIVISION[$pon];

		if (!$code || !$pon || !$division) {
			$_FAILURES['order.shipped'][] = "Missing data: material ($code) or PON($pon) or division($division)";
			continue;
		}

		// shipped items
		$_IMPORT_SHIPPED_ITEMS[$division][$pon][$code] = $row;
		
		// involved pons
		$_PONS[$pon] = $pon;
	}

} 
else $_CONSOLE[] = "Shipped orders not found";


/**
 * MAIN FILTER
 * Purchase Order NUmber
 **************************************************************************************************/

if (!$_IMPORT_CONFIRMED_ITEMS && !$_IMPORT_SHIPPED_ITEMS) {
	$_NOTIFICATIONS[] = "No import data found";
	goto BLOCK_RESPONSE;
}


if (!$_PONS) {
	$_NOTIFICATIONS[] = "WARNING: no PO-numbers found. Import not possible.";
	goto BLOCK_RESPONSE;
}

$_FILTER_PONS = "'".join("' , '", array_filter(array_values($_PONS)))."'";


/**
 * IMPORT OBSERVER
 * Import only orders with confirmed/shipped items
 **************************************************************************************************/
	
$_IMPORT_ORDERS = array();
$_IMPORT_ORDERS_FAILURE = array();

if ($_ORDERS) {

	foreach ($_ORDERS as $pon => $row) {
		
		if ($_PONS[$pon]) {
			$division = $row['ramco_division'];
			$_IMPORT_ORDERS[$division][$pon] = $row;
		} 
		else $_IMPORT_ORDERS_FAILURE[] = $row;
	}
}


/**
 * ORDER REFERENCE
 * Get all involved sales order references
 **************************************************************************************************/
		
$_SALESORDER_ITEMS = array();

$result = $exchange->query("
	SELECT *, LOWER(REPLACE(mps_salesorderitem_material_code, ' ', '')) AS code
	FROM mps_salesorderitems
	WHERE mps_salesorderitem_ponumber IN ($_FILTER_PONS)	
")->fetchAll();

if (!$result) {
	$_ERRORS[] = "Sales order items not found";
	goto BLOCK_RESPONSE;
}

foreach ($result as $row) {
	$pon = $row['mps_salesorderitem_ponumber'];
	$code = $row['code'];
	$_SALESORDER_ITEMS[$pon][$code] = $row;
}


/**
 * MPS REFERENCES
 * Get all MPS references (materials, order sheet items, workflow states, confirmed items)
 **************************************************************************************************/

$_MATERIALS = array();
$_ORDERSHEET_ITEMS_ID = array();
$_ITEM_ORDERSHEETS = array();
$_PON_ORDERSHEETS = array();
$_ITEM_STATE_CONFIRMED = array();
$_WORKFLOW_STATES = array();
$_ITEM_AUTO_DISTRIBUTION = array();

foreach ($models as $division => $model) {
	
	// get division materials
	$result = $model->query("
		SELECT
			LOWER(REPLACE(mps_material_code, ' ', '')) AS code,
			mps_material_id
		FROM mps_ordersheet_items
		INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
		WHERE mps_ordersheet_item_purchase_order_number IN ($_FILTER_PONS)
	")->fetchAll();

	$_MATERIALS[$division] = _array::extract($result);

	// get division order sheet items
	$result = $model->query("
		SELECT
			mps_ordersheet_item_id,
			mps_ordersheet_item_ordersheet_id,
			mps_ordersheet_item_material_id,
			mps_ordersheet_item_purchase_order_number
		FROM mps_ordersheet_items
		WHERE mps_ordersheet_item_purchase_order_number IN ($_FILTER_PONS)
	")->fetchAll();
	
	if ($result) {
		foreach ($result as $row) {
			$id = $row['mps_ordersheet_item_id'];
			$ma = $row['mps_ordersheet_item_material_id'];
			$po = $row['mps_ordersheet_item_purchase_order_number'];
			$_ORDERSHEET_ITEMS_ID[$division][$po][$ma] = $id;
			$_ITEM_ORDERSHEETS[$division][$id] = $row['mps_ordersheet_item_ordersheet_id'];
			$_PON_ORDERSHEETS[$division][$po] = $row['mps_ordersheet_item_ordersheet_id'];
		}
	}

	// get order sheet items confirmed state
	$result = $model->query("
		SELECT
			mps_ordersheet_item_confirmed_ordersheet_item_id as item,
			mps_ordersheet_item_confirmed_order_state AS state
		FROM mps_ordersheet_item_confirmed
		WHERE mps_ordersheet_item_confirmed_po_number IN ($_FILTER_PONS)
		ORDER BY mps_ordersheet_item_confirmed_id DESC
	")->fetchAll();
		
	if ($result) {
		
		foreach ($result as $row) {
			
			$id = $row['item'];

			// get only last item state
			if (!$_ITEM_STATE_CONFIRMED[$division][$id]) {
				$_ITEM_STATE_CONFIRMED[$division][$id] = $row['state'];
			}
		}
	}	

	// get workflow states
	$result = $model->query("
		SELECT mps_workflow_state_id, mps_workflow_state_name
		FROM mps_workflow_states
	")->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$state = $row['mps_workflow_state_id'];
			$_WORKFLOW_STATES[$division][$state] = $row['mps_workflow_state_name'];
		}
	}

	// dummy items
	$result = $model->query("
		SELECT mps_ordersheet_item_id, mps_material_planning_type_id
		FROM mps_ordersheet_items
		INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
		INNER JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id
		WHERE mps_material_planning_is_dummy = 1 
		AND mps_material_planning_ordersheet_distrubution_automatically 
		AND mps_ordersheet_item_purchase_order_number IN ($_FILTER_PONS)
	")->fetchAll();

	$_ITEM_AUTO_DISTRIBUTION[$division] = _array::extract($result);
}


/**
 * IMPORT SYNCHRONIZATION
 * Check data balance between confirmation and shipment and update missing confirmations
 **************************************************************************************************/

if ($_IMPORT_SHIPPED_ITEMS) {

	$confirmedOrders = array();

	// get all involved confirmations
	$result = $exchange->query("
		SELECT 
			TRIM(mps_salesorder_ponumber) AS pon,
			LOWER(REPLACE(mps_salesorderitem_material_code, ' ', '')) AS code
		FROM ramco_confirmed_items
		WHERE TRIM(mps_salesorder_ponumber) IN ($_FILTER_PONS)
	")->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$pon = $row['pon'];
			$code = $row['code'];
			$confirmedOrders[$pon][$code] = true; 
		}
	}

	foreach ($_IMPORT_SHIPPED_ITEMS as $division => $pons) {

		foreach ($pons as $pon => $items) {
		
			foreach ($items as $code => $item) {
				
				if (!$confirmedOrders[$pon][$code]) {

					$_CONSOLE[] = "Item $code ($pon) whithout confirmation, add manually confirmation";

					// add order to confirmed orders
					$_IMPORT_CONFIRMED_ITEMS[$division][$pon][$code] = array(
						'mps_salesorder_ponumber' => $pon,
						'mps_salesorderitem_linenumber' => $item['mps_salesorderitem_linenumber'],
						'mps_salesorderitem_material_code' => $item['mps_salesorderitem_material_code'],
						'ramco_eannumber' => NULL,
						'ramco_confirmed_quantity' => $_SALESORDER_ITEMS[$pon][$code]['mps_salesorderitem_quantity'],
						'ramco_confirmed_price' => NULL,
						'ramco_estimated_departure_date' => $item['ramco_shipping_date'],
						'ramco_requested_delivery_date' => $item['ramco_shipping_date'],
						'ramco_price_discount_code' => NULL,
						'ramco_price_discount_percent' => NULL,
						'ramco_order_state' => '00',
						'code' => $code
					);
				}
			}	
		}
	}
}


/**
 * IMPORT ORDERS
 * Import orders to MPS (only new orders)
 **************************************************************************************************/

if ($_IMPORT_ORDERS) {

	$_CONSOLE[] = "START IMPORT ORDERS";

	foreach ($_IMPORT_ORDERS as $division => $pons) {
		
		$model = $models[$division];
		
		$addOrder = $model->db->prepare("
			INSERT INTO mps_ramco_orders (
				mps_ramco_order_type,
				mps_ramco_order_customer_number,
				mps_ramco_order_po_number,
				mps_ramco_order_sold_to_sapnr,
				mps_ramco_order_sales_order_number,
				mps_ramco_order_shipto_number,
				mps_ramco_order_billto_number,
				mps_ramco_order_confirmation_date,
				mps_ramco_order_payment_condition,
				mps_ramco_order_shipment_mode,
				mps_ramco_order_inco_terms,
				mps_ramco_order_delivered_by,
				mps_ramco_order_employee,
				mps_ramco_order_distribution_code,
				mps_ramco_order_sales_organisation,
				mps_ramco_order_division,
				mps_ramco_order_order_reason,
				mps_ramco_order_order_deleted
			)
			VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
		");

		foreach ($pons as $pon => $order) {

			$data = array(
				$order['ramco_order_type'],
				$order['mps_salesorder_customernumber'],
				$order['mps_salesorder_ponumber'],
				$order['ramco_sold_to_sapnr'],
				$order['ramco_sales_order_number'],
				$order['ramco_shipto_number'],
				$order['ramco_billto_number'],
				$order['ramco_order_confirmation_date'],
				$order['ramco_payment_condition'],
				$order['ramco_shipment_mode'],
				$order['ramco_inco_terms'],
				$order['ramco_delivered_by'],
				$order['ramco_employee'],
				$order['ramco_distribution_code'],
				$order['ramco_sales_organisation'],
				$order['ramco_division'],
				$order['ramco_order_reason'],
				$order['ramco_order_deleted']
			);

			$response = !DEBUGGING_MODE ? $addOrder->execute($data) : true;

			if ($response) "Add Order $pon in MPS";
			else $_FAILURES['orders'][] = $pon;
		}
	}

	$_CONSOLE[] = "END IMPORT ORDERS";
}



/**
 * First IMPORT ITEM SHIPMENT to be later able to see if a ordered item was deleted in SAP
 * Import all shipped quantities
 **************************************************************************************************/

$_IMPORTED_SHIPPED_ITEMS = array();
$_CANCELLED_SHIPPED_ITEMS = array();

if ($_IMPORT_SHIPPED_ITEMS) {

	$_CONSOLE[] = "START IMPORT SHIPPED ITEMS";

	$updateExchangeItem = $exchange->db->prepare("
		UPDATE ramco_shipped_items SET
			mps_item_completely_imported = 1
		WHERE TRIM(mps_salesorderitem_ponumber) = ? 
		AND LOWER(REPLACE(mps_salesorderitem_material_code, ' ', '')) = ?
	");

	foreach ($_IMPORT_SHIPPED_ITEMS as $division => $pons) {
		
		$model = $models[$division];

		// add shipped quantity
		$addItem = $model->db->prepare("
			INSERT INTO mps_ordersheet_item_shipped (
				mps_ordersheet_item_shipped_ordersheet_item_id,
				mps_ordersheet_item_shipped_material_code,
				mps_ordersheet_item_shipped_po_number,
				mps_ordersheet_item_shipped_line_number,
				mps_ordersheet_item_shipped_date,
				mps_ordersheet_item_shipped_quantity,
				mps_ordersheet_item_shipped_order_state,
				mps_ordersheet_item_shortclosed,
				user_created,
				date_created
			) VALUES (?,?,?,?,?,?,?,?,'SAP',NOW())
		");

		// add ramco shipped raport
		$addShippedRaport = $model->db->prepare("
			INSERT INTO ramco_shipped_reports (ramco_shipped_report_item_id) 
			VALUES (?)
		");

		foreach ($pons as $pon => $items) {

			$_CONSOLE[] = "START shippment for order: $pon";
			
			foreach ($items as $code => $item) {

				$material = $_MATERIALS[$division][$code]; 
				
				$item['mps_material_id'] = $material;
				
				// order sheet item ID
				$id = $_ORDERSHEET_ITEMS_ID[$division][$pon][$material];

				if (!$id) {

					// item not found, set item as imported on exchange server
					$updated = DEBUGGING_MODE ? true : $updateExchangeItem->execute(array($pon, $code));
					
					$_FAILURES['import.shipped.items'][] = "Item shipped $code ($pon) NOT found in MPS ($division)";
					$_CONSOLE[] = $updated ? "Set exchange shipped item $code ($pon) as imported" : "Item shipped $code ($pon) cannot be set as imported on exchange server";

					continue;
				}

				$_CONSOLE[] = "Order sheet item $code ($id) found";

				$data = array(
					$id,
					$item['mps_salesorderitem_material_code'],
					$pon,
					$item['mps_salesorderitem_linenumber'],
					$item['ramco_shipping_date'],
					$item['ramco_shipped_quantity'],
					$item['ramco_order_state'],
					$item['ramco_order_shortclosed']
				);
				
				$inserted = DEBUGGING_MODE ? true : $addItem->execute($data);

				if (!$inserted) {
					$_FAILURES['import.shipped.items'][] = "Item shipped $code ($id) cannot be imported";
					continue;
				}
				
				$_CONSOLE[] = "Item shipped $code ($id) is imported";

				// set item as shipped
				if ($_ITEM_STATE_CONFIRMED[$division][$id] != '02') {

					$_IMPORTED_SHIPPED_ITEMS[$division][$id] = $item;

					if (!DEBUGGING_MODE) {
						$inserted = $model->db->lastInsertId();
						$addShippedRaport->execute(array($inserted));
						$_CONSOLE[] = "Item shipped raport for item $inserted";
					}

				} 
				else {
					$_CANCELLED_SHIPPED_ITEMS[$division][$id] = true;
				}

				// set shipped item as imprted on exchange server
				$updated = DEBUGGING_MODE ? true : $updateExchangeItem->execute(array($pon, $code));
				$_CONSOLE[] = $updated ? "Set exchange shipped item $code ($pon) as imported" : "Item shipped $code ($pon) cannot be set as imported on exchange server";

			} // end foreach item

			$_CONSOLE[] = "End Shippment order: $pon";

		} // end foreach order
	} // end foreach division

	$_CONSOLE[] = ":: END IMPORT RAMCO SHIPPED ITEMS ::";
}


/**
 * IMPORT ITEM CONFIRMATION
 * Import all confirmed quantities
 **************************************************************************************************/
$_IMPORTED_CONFIRMED_ITEMS_WITH_STATUS_1 = array();
$_IMPORTED_CONFIRMED_ITEMS = array();

if ($_IMPORT_CONFIRMED_ITEMS) {

	$_CONSOLE[] = "START IMPORT CONFIRMED ITEMS";

	// set confirmed items as imported on exchange server
	$updateExchangeItem = $exchange->db->prepare("
		UPDATE ramco_confirmed_items SET
		mps_item_completely_imported = 1
		WHERE TRIM(mps_salesorder_ponumber) = ? AND LOWER(REPLACE(mps_salesorderitem_material_code, ' ', '')) = ?
	");


	foreach ($_IMPORT_CONFIRMED_ITEMS as $division => $pons) {
		
		$model = $models[$division];

		$addItem = $model->db->prepare("
			INSERT INTO mps_ordersheet_item_confirmed (
				mps_ordersheet_item_confirmed_ordersheet_item_id,
				mps_ordersheet_item_confirmed_material_code,
				mps_ordersheet_item_confirmed_po_number,
				mps_ordersheet_item_confirmed_line_number,
				mps_ordersheet_item_confirmed_ean_number,
				mps_ordersheet_item_confirmed_quantity,
				mps_ordersheet_item_confirmed_price,
				mps_ordersheet_item_confirmed_departure_date,
				mps_ordersheet_item_confirmed_delivery_date,
				mps_ordersheet_item_confirmed_discount_code,
				mps_ordersheet_item_confirmed_discount_percent,
				mps_ordersheet_item_confirmed_order_state,
				mps_ordersheet_item_confirmed_confirmation_state,
				user_created,
				date_created
			) 
			VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,'SAP', NOW())
		");

		foreach ($pons as $pon => $items) {

			$_CONSOLE[] = "Start confirmation for order: $pon";
			
			foreach ($items as $code => $item) {

				// material
				$material = $_MATERIALS[$division][$code]; 

				// order sheet item ID
				$id = $_ORDERSHEET_ITEMS_ID[$division][$pon][$material];

				if (!$id) {

					// item not found in mps, set item as imported on exchange server
					$updated = DEBUGGING_MODE ? true : $updateExchangeItem->execute(array($pon, $code));
					
					$_FAILURES['import.confirmed.items'] = "Item confirmed $pon ($code) NOT found in MPS ($division)";
					
					$_CONSOLE[] = $updated 
						? "Set item confirmed $code ($pon) as imported on exchange server" 
						: "Item confirmed $code ($pon) cannot be set as imported on exchange server";

					continue;
				}

				$_CONSOLE[] = "Order sheet item $code ($id) found";


				if($item['ramco_confirmation_state'] == 1) {
					
					//check if there are alreday shipped items
					$result = $model->query("
											SELECT mps_ordersheet_item_quantity_shipped
											FROM mps_ordersheet_items
											WHERE mps_ordersheet_item_quantity_shipped> 0
											and mps_ordersheet_item_id = " . $id)->fetchAll();
					
					
					if(!$result) // item was deleted in SAP, no shipments were made
					{			
						$item['ramco_confirmed_quantity'] = 0;
					}
				}

				$data = array(
					$id,
					$item['mps_salesorderitem_material_code'],
					$pon,
					$item['mps_salesorderitem_linenumber'],
					$item['ramco_eannumber'],
					$item['ramco_confirmed_quantity'],
					$item['ramco_confirmed_price'],
					$item['ramco_estimated_departure_date'],
					$item['ramco_requested_delivery_date'],
					$item['ramco_price_discount_code'],
					$item['ramco_price_discount_percent'],
					$item['ramco_order_state'],
					$item['ramco_confirmation_state']
				);

				$inserted = DEBUGGING_MODE ? true : $addItem->execute($data);

				if (!$inserted) {
					$_FAILURES['import.confirmed.items'][] = "Item confirmed $code ($pon) can not be imported";
					continue;
				}

				$_CONSOLE[] = "Item confirmed $code ($id) is imported";
				
				$_IMPORTED_CONFIRMED_ITEMS[$division][$id] = $item;
				$_ITEM_STATE_CONFIRMED[$division][$id] = $item['ramco_order_state'];

				// set item as imported on exchange server
				$updated = DEBUGGING_MODE ? true : $updateExchangeItem->execute(array($pon, $code));
				
				$_CONSOLE[] = $updated 
					? "Set item confirmed $code ($pon) as imported on exchange server" 
					: "Item confirmed $code ($pon) cannot be set as imported on exchange server";
					
			} // end foreach item

			$_CONSOLE[] = "End confirmation for order: $pon";

		} // end foreach order
	} // end foreach division

	$_CONSOLE[] = "END IMPORT CONFIRMATION";
}






/**
 * CLEANUP MPS 
 * Remove all unused orders, only last order is considered as active
 **************************************************************************************************/

foreach ($models as $div => $model) {
	
	$model->db->exec("
		DELETE FROM mps_ramco_orders
		WHERE mps_ramco_order_id NOT IN (
			SELECT mps_ramco_order_id FROM (
				SELECT MAX(mps_ramco_order_id) AS mps_ramco_order_id
				FROM mps_ramco_orders
				GROUP BY REPLACE(LOWER(mps_ramco_order_po_number), ' ', '')
			) as maxids
		)
	");

	$model->db->exec("
		DELETE FROM mps_ordersheet_item_confirmed
		WHERE mps_ordersheet_item_confirmed_id NOT IN (
			SELECT * FROM (
				SELECT MAX(mps_ordersheet_item_confirmed_id) AS mps_ordersheet_item_confirmed_id
				FROM mps_ordersheet_item_confirmed 
				GROUP BY 
					REPLACE(LOWER(mps_ordersheet_item_confirmed_material_code), ' ', ''), 
					REPLACE(LOWER(mps_ordersheet_item_confirmed_po_number), ' ', '')
			) as maxids
		)
	");

	$model->db->exec("
		DELETE FROM mps_ordersheet_item_shipped
		WHERE mps_ordersheet_item_shipped_id NOT IN (
			SELECT * FROM (
				SELECT MAX(mps_ordersheet_item_shipped_id) AS mps_ordersheet_item_shipped_id
				FROM mps_ordersheet_item_shipped 
				GROUP BY 
					REPLACE(LOWER(mps_ordersheet_item_shipped_material_code), ' ', ''), 
					REPLACE(LOWER(mps_ordersheet_item_shipped_po_number), ' ', '')
			) as maxids
		)
	");
}


/**
 * UPDATE MPS CONFIRMATIONS
 * Update order sheet confirmed items
 **************************************************************************************************/

$_INVOLVED_ORDERSHEETS = array();

if (!$_ERRORS && $_IMPORTED_CONFIRMED_ITEMS) {

	$_CONSOLE[] = "START UPDATE ORDER SHEET ITEMS CONFIRMED QUANTITIES";

	foreach ($_IMPORTED_CONFIRMED_ITEMS as $division => $items) {
		
		$model = $models[$division];

		$updateItem = $model->db->prepare("
			UPDATE mps_ordersheet_items SET
				mps_ordersheet_item_quantity_confirmed = ?,
				user_modified = 'SAP'
			WHERE mps_ordersheet_item_id = ?
		");

		foreach ($items as $id => $item) {

			$ordersheet = $_ITEM_ORDERSHEETS[$division][$id];

			if (!$ordersheet) {
				$_CONSOLE[] = "Item $id order sheet not found";
				continue;
			}
				
			$quantity = $_ITEM_STATE_CONFIRMED[$division][$id]=='02' ? 0 : $item['ramco_confirmed_quantity'];

			$updated = DEBUGGING_MODE ? true : $updateItem->execute(array($quantity, $id));
				
			// involved ordersheets
			$_INVOLVED_ORDERSHEETS[$division][] = $ordersheet;
			
			$_CONSOLE[] = "Update ordersheet item $id whith confirmed quantity $quantity";
		} 
	} 

	$_CONSOLE[] = "END UPDATE ORDER SHEET CONFIRMED QUANTITIES";
}


/**
 * UPDATE MPS SHIPMENT
 * Update order sheet shipped items
 **************************************************************************************************/

if (!$_ERRORS && $_IMPORTED_SHIPPED_ITEMS) {

	$_CONSOLE[] = "START UPDATE ORDER SHEET SHIPPED QUANTITIES";

	foreach ($_IMPORTED_SHIPPED_ITEMS as $division => $items) {
		
		$model = $models[$division];

		// update ordersheet item
		$updateItem = $model->db->prepare("
			UPDATE mps_ordersheet_items SET
				mps_ordersheet_item_quantity_shipped = ?,
				user_modified = 'SAP'
			WHERE mps_ordersheet_item_id = ?
		");

		// update material 
		$updateMaterial = $model->db->prepare("
			UPDATE mps_materials SET
				mps_material_ean_number = ?,
				user_modified = 'SAP',
				date_modified = NOW()
			WHERE mps_material_id = ?
		");

		// update ordersheet item
		$autoDistribution= $model->db->prepare("
			UPDATE mps_ordersheet_items SET
				mps_ordersheet_item_quantity_distributed = ?,
				user_modified = 'SAP',
				date_modified = NOW()
			WHERE mps_ordersheet_item_id = ?
		");

		foreach ($items as $id => $item) {

			$ordersheet = $_ITEM_ORDERSHEETS[$division][$id];

			if (!$ordersheet) {
				$_ERRORS[] = "Ordersheet not found for item {$item[code]} ($id)";
				continue;
			}

			$quantity = $_ITEM_STATE_CONFIRMED[$division][$id]=='02' ? 0 : $item['ramco_shipped_quantity'];

			$updated = DEBUGGING_MODE ? true : $updateItem->execute(array($quantity, $id));

			if ($updated && $_ITEM_AUTO_DISTRIBUTION[$division][$id]) {
				$auto = DEBUGGING_MODE ? true : $autoDistribution->execute(array($quantity, $id));
				if ($auto) $_CONSOLE[] = "Auto distristribution for dummy item $id";
			}

			// involved order sheets
			$_INVOLVED_ORDERSHEETS[$division][] = $ordersheet;
			
			$_CONSOLE[] = "Update ordersheet item $id whith shipped quantity $quantity";
			
			$ean = $item['mps_salesorderitem_ean_number'];

			// update ean number
			if ($ean && $item['mps_material_id']) {
				
				if (!DEBUGGING_MODE) {
					$updateMaterial->execute(array($ean, $item['mps_material_id']));
				}
				
				$_CONSOLE[] = "Update material EAN number $ean";
			}

		} // end foreach item
	} // end foreach division

	$_CONSOLE[] = "END UPDATE ORDER SHEET SHIPPED QUANTITIES ::";
}


/**
 * CHECK MPS ORDERS
 **************************************************************************************************/

if (!$_INVOLVED_ORDERSHEETS) {
	$_CONSOLE[] = "Involved order sheets not found";
	goto BLOCK_RESPONSE;
}


/**
 * MPS DATA
 * Get involved order sheets data (order sheets, items, confirmations, shipment)
 **************************************************************************************************/

$_ORDERSHEET_STATE = array();
$_ITEM_CONFIRMED_STATE = array();
$_ITEM_SHIPPED_STATE = array();
$_TOTAL_CANCELLED_ITEMS = array();
$_TOTAL_CONFIRMED_ITEMS = array();
$_TOTAL_SHIPPED_ITEMS = array();
$_TOTAL_DISTRIBUTED_ITEMS = array();
$_TOTAL_PARTIALLY_SHIPPED_ITEMS = array();
$_TOTAL_ITEMS = array();
$_TOTAL_COMPLETED_ITEMS = array();

foreach ($_INVOLVED_ORDERSHEETS as $division => $ordersheets) {
	
	$model = $models[$division];

	$ordersheets = array_filter(array_unique($ordersheets));
	$involdevOrdersheets = join(',', $ordersheets); 

	// get order sheet current states
	$result = $model->query("
		SELECT mps_ordersheet_id, mps_ordersheet_workflowstate_id
		FROM mps_ordersheets
		WHERE mps_ordersheet_id IN ($involdevOrdersheets)
	")->fetchAll();

	$_ORDERSHEET_STATE[$division] = _array::extract($result);

	// quantity confirmed states
	$result = $model->query("
		SELECT mps_ordersheet_item_confirmed_ordersheet_item_id, mps_ordersheet_item_confirmed_order_state
		FROM mps_ordersheet_item_confirmed
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_confirmed_ordersheet_item_id
		WHERE mps_ordersheet_item_ordersheet_id IN ($involdevOrdersheets) AND mps_ordersheet_item_quantity_approved > 0
		GROUP BY mps_ordersheet_item_confirmed_ordersheet_item_id
		ORDER BY mps_ordersheet_item_confirmed_id DESC
	")->fetchAll();

	$_ITEM_CONFIRMED_STATE[$division] = _array::extract($result);

	// quantity shipped states
	/*
	$result = $model->query("
		SELECT mps_ordersheet_item_shipped_ordersheet_item_id, mps_ordersheet_item_shortclosed
		FROM mps_ordersheet_item_shipped
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_shipped_ordersheet_item_id
		WHERE mps_ordersheet_item_ordersheet_id IN ($involdevOrdersheets) AND mps_ordersheet_item_quantity_approved > 0
		GROUP BY mps_ordersheet_item_shipped_ordersheet_item_id
		ORDER BY mps_ordersheet_item_shipped_id DESC
	")->fetchAll();

	$_ITEM_SHIPPED_STATE[$division] = _array::extract($result);
	*/


	$result = $model->query("
		SELECT mps_ordersheet_item_confirmed_ordersheet_item_id, mps_ordersheet_item_confirmed_confirmation_state
		FROM mps_ordersheet_item_confirmed
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_confirmed_ordersheet_item_id
		WHERE mps_ordersheet_item_ordersheet_id IN ($involdevOrdersheets) AND mps_ordersheet_item_quantity_approved > 0
		GROUP BY mps_ordersheet_item_confirmed_ordersheet_item_id
		ORDER BY mps_ordersheet_item_confirmed_id DESC
	")->fetchAll();

	$_ITEM_SHIPPED_STATE[$division] = _array::extract($result);

	// order sheet quantities
	$result = $model->query("
		SELECT DISTINCT
			mps_ordersheet_item_id,
			mps_ordersheet_item_ordersheet_id,
			mps_ordersheet_item_quantity_approved,
			mps_ordersheet_item_quantity_confirmed,
			mps_ordersheet_item_quantity_shipped,
			mps_ordersheet_item_quantity_distributed
		FROM mps_ordersheet_items
		INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
		WHERE mps_ordersheet_item_ordersheet_id IN ($involdevOrdersheets)
		AND mps_material_locally_provided != 1 
		AND mps_ordersheet_item_quantity_approved > 0
	")->fetchAll();

	if (!$result) {
		$_CONSOLE[] = "Order sheet items not found for division $division";
		continue;
	}

	foreach ($result as $row) {

		$item = $row['mps_ordersheet_item_id'];
		$ordersheet = $row['mps_ordersheet_item_ordersheet_id'];
		$qtyApproved = $row['mps_ordersheet_item_quantity_approved'];
		$qtyConfirmed = $row['mps_ordersheet_item_quantity_confirmed'];
		$qtyShipped = $row['mps_ordersheet_item_quantity_shipped'];
		$qtyDistributed = $row['mps_ordersheet_item_quantity_distributed'];
		
		$confirmedState = $_ITEM_CONFIRMED_STATE[$division][$item];
		$shippedState = $_ITEM_SHIPPED_STATE[$division][$item];

		// count order sheet items
		$_TOTAL_ITEMS[$division][$ordersheet] += 1;

		// item is confirmed
		if (!$confirmedState) {
			$_CONSOLE[] = "Ignore item $item whithout confirmation state";
			continue;
		}

		// cancelled item
		
		if ($confirmedState=='02' || $confirmedState==2) {
			$_TOTAL_CANCELLED_ITEMS[$division][$ordersheet] += 1;
			$_TOTAL_COMPLETED_ITEMS[$division][$ordersheet] += 1;
			$_CONSOLE[] = "Item $item is cancelled";
			continue;
		} 
		
		// confirmed quantities
		if ($qtyConfirmed > 0) {
			$_TOTAL_CONFIRMED_ITEMS[$division][$ordersheet] += 1;
			$_CONSOLE[] = "Item $item is confirmed";
		}

		/*
		if (isset($shippedState) && $qtyShipped==0 && $shippedState==0) {
			$_TOTAL_CANCELLED_ITEMS[$division][$ordersheet] += 1;
			$_TOTAL_COMPLETED_ITEMS[$division][$ordersheet] += 1;
			$_CONSOLE[] = "Item $item shippment is cancelled";
			continue;
		}
		*/

		if ($shippedState==1) {
			$_TOTAL_CANCELLED_ITEMS[$division][$ordersheet] += 1;
			$_TOTAL_COMPLETED_ITEMS[$division][$ordersheet] += 1;
			$_CONSOLE[] = "Item $item shipment is cancelled";
			continue;
		}

		// shippe quantities
		if (!$qtyShipped) {
			$_CONSOLE[] = "Item $item hasn't shipped quantities";
			continue;
		}

		// regulary shipped
		if ($qtyShipped >= $qtyConfirmed) {
			$_TOTAL_SHIPPED_ITEMS[$division][$ordersheet] += 1;
			$_CONSOLE[] = "Item $item is shipped";
		}
		
		// partially shipped
		if($qtyShipped > 0 and $qtyShipped < $qtyConfirmed) {
			if ($shippedState==1) {
				$_TOTAL_SHIPPED_ITEMS[$division][$ordersheet] += 1;
				$_CONSOLE[] = "Item $item is shipped";
			} else {
				$_TOTAL_PARTIALLY_SHIPPED_ITEMS[$division][$ordersheet] += 1;
				$_CONSOLE[] = "Item $item is partially shipped";
			}
		}

		if ($qtyShipped==$qtyDistributed) {
			$_TOTAL_DISTRIBUTED_ITEMS[$division][$ordersheet] += 1;
			$_TOTAL_COMPLETED_ITEMS[$division][$ordersheet] += 1;
			$_CONSOLE[] = "Item $item is distributed";
		}
	}
}


/**
 * ORDER SHEET STATES
 * Process ordersheet state
 **************************************************************************************************/

$_UPGARDE_TO_CONFIRMED = array();
$_UPGRADE_TO_SHIPPED = array();
$_UPGRATE_TO_PARTIALLY_SHIPPED = array();
$_UPGRADE_TO_DISTRIBUTED = array();
$_UPGRADE_TO_CANCELLED = array();
$_COMPLETED_ORDERSHEETS = array();

if ($_TOTAL_ITEMS) {

	$_CONSOLE[] = "START BUILDING ORDER SHEET STATE";

	$stateSubmitted 		= Workflow_State::STATE_SALES_ORDER_SUBITTED;
	$stateConfirmed 		= Workflow_State::STATE_ORDER_CONFIRMED;
	$stateShipped 			= Workflow_State::STATE_SHIPPED;
	$statePartiallyShipped 	= Workflow_State::STATE_PARTIALLY_SHIPPED;
	$stateDistributed 		= Workflow_State::STATE_DISTRIBUTED;
	$stateArchived 			= Workflow_State::STATE_ARCHIVED;


	foreach ($_TOTAL_ITEMS as $division => $ordersheets) { 

		foreach ($ordersheets as $ordersheet => $totalItems) {

			$totalCancelled = $_TOTAL_CANCELLED_ITEMS[$division][$ordersheet];
			$totalConfirmed = $_TOTAL_CONFIRMED_ITEMS[$division][$ordersheet];
			$totalShipped = $_TOTAL_SHIPPED_ITEMS[$division][$ordersheet];
			$totalPartially = $_TOTAL_PARTIALLY_SHIPPED_ITEMS[$division][$ordersheet];
			$totalDistributed = $_TOTAL_DISTRIBUTED_ITEMS[$division][$ordersheet];
			$totalCompleted = $_TOTAL_COMPLETED_ITEMS[$division][$ordersheet];

			$currentState = $_ORDERSHEET_STATE[$division][$ordersheet];
			
			// order sheet completed
			if ($totalItems==$totalCompleted) {
				$_COMPLETED_ORDERSHEETS[$division][$ordersheet] = $ordersheet;
			}

			// confirmed
			if ($currentState==$stateSubmitted && $totalConfirmed > 0) {
				$currentState = $stateConfirmed;
				$_UPGARDE_TO_CONFIRMED[$division][$ordersheet] = true;
				$_CONSOLE[] = "Order sheet is confirmed.";
			}

						
			// partially shipped
			/*
			if ($currentState==$stateConfirmed 
				&& $totalPartially > 0 
				&& $totalItems >= $totalPartially) {
				$currentState = $statePartiallyShipped;
				$_UPGRATE_TO_PARTIALLY_SHIPPED[$division][$ordersheet] = true;
				$_CONSOLE[] = "Order sheet is partially shipped.";
				continue;
			}


			// shipped
			if ($currentState==$stateConfirmed 
				&& $totalShipped > 0 
				&& $totalItems==$totalShipped) {
				$currentState = $stateShipped;
				$_UPGRADE_TO_SHIPPED[$division][$ordersheet] = true;
				$_CONSOLE[] = "Order sheet is shipped.";
			}
			*/

						
			//aho
			
			//echo $totalItems . "->" . $totalConfirmed . "->" . $totalPartially  . "->" . $totalCancelled;
			
			
			if ($currentState==$statePartiallyShipped
				and $totalShipped > 0
				and ($totalConfirmed-$totalCancelled) ==  $totalShipped) {
				$currentState = $stateShipped;
				$_UPGRADE_TO_SHIPPED[$division][$ordersheet] = true;
				$_CONSOLE[] = "Order sheet is shipped.";
			}
			elseif ($currentState==$stateConfirmed 
				&& $totalPartially > 0 
				&& $totalItems >= $totalPartially) {
				$currentState = $statePartiallyShipped;
				$_UPGRATE_TO_PARTIALLY_SHIPPED[$division][$ordersheet] = true;
				$_CONSOLE[] = "Order sheet is partially shipped.";
				continue;
			}
			elseif ($currentState==$stateConfirmed 
				and $totalShipped > 0
				and ($totalConfirmed-$totalCancelled) >  $totalShipped) {
				$currentState = $statePartiallyShipped;
				$_UPGRATE_TO_PARTIALLY_SHIPPED[$division][$ordersheet] = true;
				$_CONSOLE[] = "Order sheet is partially shipped.";
				continue;
			}
			elseif ($currentState==$stateConfirmed 
				and $totalShipped > 0
				and ($totalConfirmed >  $totalShipped)) {
				$currentState = $statePartiallyShipped;
				$_UPGRATE_TO_PARTIALLY_SHIPPED[$division][$ordersheet] = true;
				$_CONSOLE[] = "Order sheet is partially shipped.";
				continue;
			}
			elseif ($currentState==$stateConfirmed
				and $totalShipped > 0
				and ($totalConfirmed-$totalCancelled) ==  $totalShipped) {
				$currentState = $stateShipped;
				$_UPGRADE_TO_SHIPPED[$division][$ordersheet] = true;
				$_CONSOLE[] = "Order sheet is shipped.";
			}
			elseif ($currentState==$stateConfirmed
				and $totalShipped > 0
				and ($totalConfirmed) ==  $totalShipped) {
				$currentState = $stateShipped;
				$_UPGRADE_TO_SHIPPED[$division][$ordersheet] = true;
				$_CONSOLE[] = "Order sheet is shipped.";
			}
			


			

			

			

			// distributed
			if ($currentState==$stateShipped && $totalDistributed > 0 && $totalItems==$totalDistributed) {
				$currentState = $stateDistributed;
				$_UPGRADE_TO_DISTRIBUTED[$division][$ordersheet] = true;
				$_CONSOLE[] = "Order sheet is shipped.";
				continue;
			}

			
			// cancelled order sheets
			/*
			if ($currentState==$stateSubmitted &&  $totalCancelled > 0 && $totalItems==$totalCancelled) {
				$currentState = $stateArchived;
				$_UPGRADE_TO_CANCELLED[$division][$ordersheet] = true;
				$_CONSOLE[] = "Order sheet is cancelled.";
			}
			*/

			if ($totalShipped == 0 
				and $totalPartially == 0
				and $totalCancelled > 0 && $totalItems==$totalCancelled) {
				$currentState = $stateArchived;
				$_UPGRADE_TO_CANCELLED[$division][$ordersheet] = true;
				$_CONSOLE[] = "Order sheet is cancelled.";
			}
		}
	}

	

	$_CONSOLE[] = "END BUILDING ORDER SHEET STATE";
}


/**
 * ORDER SHEET UPGARDES
 * Change order sheet workflow state
 **************************************************************************************************/


$_CONSOLE[] = "START UPGRADE ORDER SHEETS STATE";

foreach ($_INVOLVED_ORDERSHEETS as $division => $ordersheets) {
	
	$model = $models[$division];

	$ordersheets = array_filter(array_unique($ordersheets));

	// update order sheet state
	$updateOrdersheetState = $model->db->prepare("
		UPDATE mps_ordersheets SET
			mps_ordersheet_workflowstate_id = ?,
			user_modified = 'SAP',
			date_modified = NOW()
		WHERE mps_ordersheet_id = ?
	");

	// statement: track order sheet
	$tracker = $model->db->prepare("
		INSERT INTO user_trackings (
			user_tracking_entity,
			user_tracking_entity_id,
			user_tracking_user_id,
			user_tracking_action,
			user_created,
			date_created
		) VALUES ('order sheet', ?, ?, ?, ?, NOW())
	");

	foreach ($ordersheets as $ordersheet) {

		// Cuurent state
		$currentState = $_ORDERSHEET_STATE[$division][$ordersheet];
		$stateName = $_WORKFLOW_STATES[$division][$currentState];

		$_CONSOLE[] = "START states processing for order sheet $ordersheet";
		$_CONSOLE[] = "Order Sheet befor state upgrade: $ordersheet is $stateName";

		// order sheet in distribution, distributted, partially distributed, manually archived or archived
		if (in_array($currentState, array(6,11,13,15,16))) {
			$_CONSOLE[] = "Order Sheet $ordersheet is $stateName (cancel upgrade)";
			$_CONSOLE[] = "END states processing for order sheet $ordersheet";
			continue;
		}

		// set order sheet as archived
		if ($_UPGRADE_TO_CANCELLED[$division][$ordersheet] && $currentState<>Workflow_State::STATE_ARCHIVED) {

			$state = Workflow_State::STATE_ARCHIVED;
			$update = DEBUGGING_MODE ? true : $updateOrdersheetState->execute(array($state, $ordersheet)); 

			if (!$update) {
				$_ERRORS[] = "The order sheet $ordersheet can not be upgrade to state $stateName";
				continue;
			}

			$currentState = $state;
			$stateName = $_WORKFLOW_STATES[$division][$state];
			$_CONSOLE[] = "Order Sheet $ordersheet is $stateName (cancel upgrade)";
			$_CONSOLE[] = "END states processing for order sheet $ordersheet";

			$track = DEBUGGING_MODE ? true : $tracker->execute(array($ordersheet, $user->id, $stateName, 'SAP'));

			continue;
		}

		// set order sheet as confirmed
		if ($_UPGARDE_TO_CONFIRMED[$division][$ordersheet] && $currentState==Workflow_State::STATE_SALES_ORDER_SUBITTED) {

			$state = Workflow_State::STATE_ORDER_CONFIRMED;
			$update = DEBUGGING_MODE ? true : $updateOrdersheetState->execute(array($state, $ordersheet)); 

			if (!$update) {
				$_ERRORS[] = "The order sheet $ordersheet can not be upgrade to state $stateName";
				continue;
			}

			$currentState = $state;
			$stateName = $_WORKFLOW_STATES[$division][$state];
			$_CONSOLE[] = "Order sheet $ordersheet is $stateName";
			$track = DEBUGGING_MODE ? true : $tracker->execute(array($ordersheet, $user->id, $stateName, 'SAP'));	
		}

		// set order sheet as partially shipped
		if ($_UPGRATE_TO_PARTIALLY_SHIPPED[$division][$ordersheet] && !$_UPGRADE_TO_SHIPPED[$division][$ordersheet] && $currentState==Workflow_State::STATE_ORDER_CONFIRMED) {

			$state = Workflow_State::STATE_PARTIALLY_SHIPPED;
			$update = DEBUGGING_MODE ? true : $updateOrdersheetState->execute(array($state, $ordersheet));

			if (!$update) {
				$_ERRORS[] = "The order sheet $ordersheet can not be upgrade to state $stateName";
				continue;
			}

			$currentState = $state;
			$stateName = $_WORKFLOW_STATES[$division][$state];
			$_CONSOLE[] = "Order sheet $ordersheet is $stateName";
			$track = DEBUGGING_MODE ? true : $tracker->execute(array($ordersheet, $user->id, $stateName, 'SAP'));

		}
		
			

		// set order sheet as shipped shipped
		if ($_UPGRADE_TO_SHIPPED[$division][$ordersheet] && in_array($currentState, array(9,10,14))) {

			$state = Workflow_State::STATE_SHIPPED;
			$update = DEBUGGING_MODE ? true : $updateOrdersheetState->execute(array($state, $ordersheet)); 

			if (!$update) {
				$_ERRORS[] = "The order sheet $ordersheet can not be upgrade to state $stateName";
				continue;
			}

			$currentState = $state;
			$stateName = $_WORKFLOW_STATES[$division][$state];
			$_CONSOLE[] = "Order sheet $ordersheet is $stateName";
			$track = DEBUGGING_MODE ? true : $tracker->execute(array($ordersheet, $user->id, $stateName, 'SAP'));
		} 
			

		// set order sheet as distributed
		if ($_UPGRADE_TO_DISTRIBUTED[$division][$ordersheet] && $currentState==Workflow_State::STATE_SHIPPED) {

			$state = Workflow_State::STATE_DISTRIBUTED;
			$update = DEBUGGING_MODE ? true : $updateOrdersheetState->execute(array($state, $ordersheet)); 

			if (!$update) {
				$_ERRORS[] = "The order sheet $ordersheet can not be upgrade to state $stateName";
				continue;
			}

			$currentState = $state;
			$stateName = $_WORKFLOW_STATES[$division][$state];
			$_CONSOLE[] = "Order sheet $ordersheet is $stateName";
			$track = DEBUGGING_MODE ? true : $tracker->execute(array($ordersheet, $user->id, $stateName, 'SAP'));
		} 

		$_CONSOLE[] = "END states processing for order sheet $ordersheet";

	} // end foreeach order sheet
} // end foreach division

$_CONSOLE[] = "END ORDER SHEET UPGRADES";


/**
 * MASTER SHEET PROCESSNG
 * Up/Downgrade master sheet archived state
 **************************************************************************************************/

$_CONSOLE[] = "START UPDATE MASTER SHEETS STATE";


foreach ($_INVOLVED_ORDERSHEETS as $division => $ordersheets) {

	$_TOTAL_ORDERSHEETS = array();
	$_TOTAL_ARCHIVED_ORDERSHEETS = array();

	$model = $models[$division];

	$ordersheets = array_filter(array_unique($ordersheets));
	$involdevOrdersheets = join(',', $ordersheets);

	// reset mastersheet
	$resetMastersheet = $model->db->prepare("
		UPDATE mps_mastersheets SET 
			mps_mastersheet_archived = ?,
			user_modified = 'SAP',
			date_modifeid = NOW()
		WHERE mps_mastersheet_id = ?
	");

	$result = $model->query("
		SELECT DISTINCT 
			mps_ordersheet_id, 
			mps_ordersheet_mastersheet_id,
			mps_ordersheet_workflowstate_id
		FROM mps_ordersheets
		WHERE mps_ordersheet_id IN ($involdevOrdersheets)
	")->fetchAll();

	foreach ($result as $row) {
		
		$ordersheet = $row['mps_ordersheet_id'];
		$mastersheet = $row['mps_ordersheet_mastersheet_id'];
		$state = $row['mps_ordersheet_workflowstate_id'];

		$_TOTAL_ORDERSHEETS[$mastersheet] = $_TOTAL_ORDERSHEETS[$mastersheet]+1;

		if (in_array($state, array(6,15,16))) {
			$_TOTAL_ARCHIVED_ORDERSHEETS[$mastersheet] = $_TOTAL_ARCHIVED_ORDERSHEETS[$mastersheet]+1;
		}
	}

	foreach ($_TOTAL_ORDERSHEETS as $mastersheet => $total) {
		
		$archived = $total==$_TOTAL_ARCHIVED_ORDERSHEETS[$mastersheet] ? 1 : 0;

		$update = DEBUGGING_MODE ? true : $resetMastersheet->execute(array($archived, $mastersheet)); 
		
		if ($update) {
			$caption = $archived ? 'archived' : 'unarchived';
			$_CONSOLE[] = "Updating master sheet $mastersheet as $caption";
		}
	}
}

$_CONSOLE[] = "END UPDATE MASTER SHEETS STATE";


/**
 * UPGRADE ORDERS
 * Set ramco orders as complited
 **************************************************************************************************/

if ($_IMPORT_ORDERS) {

	$_CONSOLE[] = "START UPDATE EXCHANGE ORDERS";

	$sth = $exchange->db->prepare("
		UPDATE ramco_orders SET
			mps_order_completely_imported = 1
		WHERE TRIM(mps_salesorder_ponumber) = ?
	");
	
	foreach ($_IMPORT_ORDERS as $division => $orders) {
		
		foreach ($orders as $pon => $order) {

			$ordersheet = $_PON_ORDERSHEETS[$division][$pon];
			
			if ($ordersheet) {
				
				if ($_COMPLETED_ORDERSHEETS[$division][$ordersheet]) {

					$update = DEBUGGING_MODE ? true : $update = $sth->execute(array($pon)); 

					if ($updated) $_CONSOLE[] = "Ramco order $pon for order sheet $ordersheet is completed";
					else $_FAILURES['upgrade.ramco.orders'][] = "Order $pon cannot be set as completed";
				}

			} 
			else $_FAILURES['upgrade.ramco.orders'][] = "The order sheet for purchase order number $pon not found.";

		} // end for each orders
	} // end for each devision

	$_CONSOLE[] = "END UPDATE EXCHANGE ORDERS";
}

	
/**
 * RESPONSE
 **************************************************************************************************/

BLOCK_RESPONSE:

$_CONSOLE[] = "END import on ".date('d.m.Y H:i:s');

if ($application) {
	
	header('Content-Type: text/json');
	
	echo json_encode(array(
		'response' => true,
		'errors' => $_ERRORS,
		'failures' => $_FAILURES,
		'console' => $_CONSOLE,
		'notifications' => $_NOTIFICATIONS,
		'message' => $_ORDERS ? "All orders are imported." :  "All orders are imported. No action was affected."
	));
}

