<?php

	ini_set('memory_limit', '8096M');
	
	$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';
	
	$translate = Translate::instance();
	$settings = Settings::init();
	
	// for http request
	if ($_REQUEST['application']) {
		$update_ordersheets = ($_REQUEST['update_existing_order_sheets']) ? true : false;
	} 
	// cronjob shell request
	else {
		Connector::instance();
		$update_ordersheets = true;
	}
	
	
	// exchange model
	$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);
	
	$data = array();
	$datagrid = array();
	$dataloader = array();
	$messages = array();
	$errors = array();
	$ordersheets = array();
	
	// conectors
	$connectors = array(
		'tissot' => Connector::DB_RETAILNET_MPS
	);
	
	// get materials
	$result = $exchange->query("
		SELECT 
			item_id,
			TRIM(company) AS company,
			TRIM(item_no) AS item_no,
			TRIM(item_desc) AS item_desc,
			TRIM(coll_code) AS coll_code,
			TRIM(coll_cat) AS coll_cat,
			TRIM(mps_cat) AS mps_cat,
			TRIM(mps_purpose) AS mps_purpose,
			TRIM(mps_plan_type) AS mps_plan_type,
			TRIM(ret_price_chf) AS ret_price_chf,
			TRIM(curr_code) AS curr_code,
			TRIM(mps_setof) AS mps_setof,
			TRIM(mps_width) AS mps_width,
			TRIM(mps_height) AS mps_height,
			TRIM(mps_length) AS mps_length,
			TRIM(mps_radius) AS mps_radius,
			TRIM(mps_longterm) AS mps_longterm,
			TRIM(item_status) AS item_status,
			TRIM(mps_hsc) AS mps_hsc,
			TRIM(dummy) AS dummy,
			TRIM(mps_eancode) as mps_eancode
		FROM mps_item_mst
		WHERE item_status <> 1
	")->fetchAll();
	
	// group items by company
	if ($result) { 
		
		$all_companies = array();
		
		foreach ($result as $row) {
			
			$company = strtolower($row['company']);
			$code = strtolower($row['item_no']);
			
			if ($company=='all') $all_companies[$code] = $row;
			else $datagrid[$company][$code] = $row;
		}
		
		// push all-copmany materials to each company
		if ($all_companies) {
			foreach ($datagrid as $company => $data) {
				$data = array_merge($data, $all_companies);
				$datagrid[$company] = $data;
			}
		}
	}

	
	if ($datagrid) { 
		
		foreach ($datagrid as $company => $data) {
			
			$dataloader = array();
			
			// db model
			$conn = ($connectors[$company]) ? $connectors[$company] : Connector::DB_CORE;
			$model = new Model($conn);
			
			// dataloader: material planning types
			$result = $model->query("
				SELECT
					LOWER(REPLACE(mps_material_code, ' ', '')) AS code,
					mps_material_id
				FROM mps_materials
			")->fetchAll();
			
			$dataloader['materials'] = _array::extract($result);
			
			
			// dataloader: material planning types
			$result = $model->query("
				SELECT
					LOWER(REPLACE(mps_material_planning_type_name, ' ', '')) AS code,
					mps_material_planning_type_id
				FROM mps_material_planning_types
			")->fetchAll();
			
			$dataloader['planning_types'] = _array::extract($result);
			
			
			// dataloader: material purposes
			$result = $model->query("
				SELECT 
					LOWER(REPLACE(mps_material_purpose_name, ' ', '')) AS code,
					mps_material_purpose_id
				FROM mps_material_purposes
			")->fetchAll();
			
			$dataloader['purposes'] = _array::extract($result);
			
			
			// dataloader: material categories
			$result = $model->query("
				SELECT 
					LOWER(REPLACE(mps_material_category_name, ' ', '')) AS code,
					mps_material_category_id
				FROM mps_material_categories
			")->fetchAll();
			
			$dataloader['categories'] = _array::extract($result);
			
			
			// material collections
			$result = $model->query("
				SELECT 
					LOWER(REPLACE(mps_material_collection_code, ' ', '')) AS code, 
					mps_material_collection_id
				FROM mps_material_collections
			")->fetchAll();
			
			$dataloader['collections'] = _array::extract($result);
			
			
			// dataloader: material collection categories
			$result = $model->query("
				SELECT 
					LOWER(REPLACE(mps_material_collection_category_code, ' ', '')) AS code, 
					mps_material_collection_category_id
				FROM mps_material_collection_categories
			")->fetchAll();
			
			$dataloader['collection_categories'] = _array::extract($result);
			
			// modul builders
			$material = new Material($conn);
			$material_purpose = new Material_Purpose($conn);
			$material_category = new Material_Category($conn);
			$material_planning_type = new Material_Planning_Type($conn);
			$material_collection = new Material_Collection($conn);
			$material_collection_category = new Material_Collection_Category($conn);
			$mastersheet = new Mastersheet($conn);
			$ordersheet = new Ordersheet($conn);
			$ordersheet_item = new Ordersheet_Item($conn);
			$Company = new Company();
			

			// import materials
			foreach ($data as $row) {
				
				// captions
				$material_code = $row['item_no'];
				$material_planning_type_name = $row['mps_plan_type'];
				$material_purpose_name = $row['mps_purpose'];
				$material_category_name = $row['mps_cat'];
				$material_collection_name = $row['coll_code'];
				$material_collection_category_name = $row['coll_cat'];
				
				// identificators
				$_material = strtolower(str_replace(' ', '', $row['item_no']));
				$_planning = strtolower(str_replace(' ', '', $row['mps_plan_type']));
				$_collection = strtolower(str_replace(' ', '', $row['coll_code']));
				$_collection_category = strtolower(str_replace(' ', '', $row['coll_cat']));
				$_category = strtolower(str_replace(' ', '', $row['mps_cat']));
				$_purpose = strtolower(str_replace(' ', '', $row['mps_purpose']));
				
				
				if ($material_planning_type_name && $material_code) {
					
					// add new material planning type
					if (!$dataloader['planning_types'][$_planning]) { 
						
						// long term material can not be of another planning type
						$row['mps_longterm'] = 0;
						
						$inserted = $material_planning_type->create(array(
							'mps_material_planning_type_name' => $material_planning_type_name,
							'mps_material_planning_is_dummy' => $row['dummy'],
							'user_created' => 'SAP',
							'date_created' => date("Y-m-d H:i:s")
						));
		
						if ($inserted) {
							$dataloader['planning_types'][$_planning] = $inserted;
							$messages[] = "Missing planning type <b>$material_planning_type_name</b> for item <b>$material_code</b> was inserted.";
						} else {
							$messages[] = "Missing planning type <b>$material_planning_type_name</b> for item <b>$material_code</b> was not inserted.";
						}
					} 
					elseif ($dataloader['planning_types'][$_planning] && $row['dummy']==1) {

						$material_planning_type->getFromName($material_planning_type_name);

						$update = $material_planning_type->update(array(
							'mps_material_planning_type_name' => $material_planning_type_name,
							'mps_material_planning_is_dummy' => 1,
							'user_modified' => 'SAP',
							'date_modified' => date("Y-m-d H:i:s")
						));

						if ($update) {
							$messages[] = "Update planning type <b>$material_planning_type_name</b> for item <b>$material_code</b> as dummy.";
						} else {
							$messages[] = "Update planning type <b>$material_planning_type_name</b> for item <b>$material_code</b> as dummy.";
						}
					}
				}
	
				
				// add new material purposes
				if (!$dataloader['purposes'][$_purpose] && $material_purpose_name && $row['mps_longterm']==1 && $material_code) {

					$inserted = $material_purpose->create(array(
						'mps_material_purpose_name' => $material_purpose_name,
						'mps_material_purpose_active' => 1,
						'user_created' => 'SAP',
						'date_created' => date("Y-m-d H:i:s")
					));

					if ($inserted) {
						$dataloader['purposes'][$_purpose] = $inserted;
						$messages[] = "Missing purpose <b>$material_purpose_name</b> for item <b>$material_code</b> was inserted.";
					} else {
						$errors[] = "Missing purpose <b>$material_purpose_name</b> for item <b>$material_code</b> was not inserted.";
					}
				}
	
				
				// add new material category
				if (!$dataloader['categories'][$_category] && $material_category_name && $row["mps_longterm"]==1 && $material_code) {
	
					$inserted = $material_category->create(array(
						'mps_material_category_name' => $material_category_name,
						'mps_material_category_active' => 1,
						'user_created' => 'SAP',
						'date_created' => date("Y-m-d H:i:s")
					));
					
					if ($inserted) {
						$dataloader['categories'][$_category] = $inserted;
						$messages[]  = "Missing categrory <b>$material_category_name</b> for item <b>$material_code</b> was inserted.";
					} else {
						$errors[]  = "Missing categrory <b>$material_category_name</b> for item <b>$material_code</b> was inserted.";
					}
				}
	
				
				// add new material collection category
				if (!$dataloader['collection_categories'][$_collection_category] && $material_collection_category_name && $material_code) {
					
					$inserted = $material_collection_category->create(array(
						'mps_material_collection_category_code' => $material_collection_category_name,
						'mps_material_collection_category_active' => 1,
						'user_created' => 'SAP',
						'date_created' => date("Y-m-d H:i:s")
					));
					
					if ($inserted) {
						$dataloader['collection_categories'][$_collection_category] = $inserted;
						$messages[] = "Missing collection categrory <b>$material_collection_category_name</b> for item <b>$material_code</b> was inserted.";
					} else {
						$errors[] = "Missing collection categrory <b>$material_collection_category_name</b> for item <b>$material_code</b> was not inserted.";
					}
				}
	
				
				// add new material collections
				if (!$dataloader['collections'][$_collection] && $material_collection_name && $material_code) {
					
					$inserted = $material_collection->create(array(
						'mps_material_collection_code' => $material_collection_name,
						'mps_material_collection_active' => 1,
						'user_created' => 'import_from_ramco',
						'date_created' => date("Y-m-d H:i:s")
					));
					
					if ($inserted) {
						$dataloader['collections'][$_collection] = $inserted;
						$messages[] = "Missing collection <b>$material_collection_name</b> for item <b>$material_code</b> was inserted.";
					} else {
						$errors[] = "Missing collection <b>$material_collection_name</b> for item <b>$material_code</b> was not inserted.";
					}
				}
	
				
				// update material	
				if ($dataloader['materials'][$_material] && $material_code)  {
					
					$material->read($dataloader['materials'][$_material]);
					
					if($row['ret_price_chf'] > 0) {
						$material->update(array(
							'mps_material_currency_id' => 1,
							'mps_material_name' => $row['item_desc'],
							'mps_material_hsc' => $row['mps_hsc'],
							'mps_material_ean_number' => $row['mps_eancode'],
							'mps_material_price' => $row['ret_price_chf'],
							'mps_material_setof' => $row['mps_setof'],
							'mps_material_islongterm' => $row['mps_longterm'],
							'mps_material_active' => 1,
							'mps_material_width' => ($row['mps_width']) ? $row['mps_width'] : null,
							'mps_material_height' => ($row['mps_height']) ? $row['mps_height'] : null,
							'mps_material_length' => ($row['mps_length']) ? $row['mps_length']  : null,
							'mps_material_radius' => ($row['mps_radius']) ? $row['mps_radius'] : null,
							'user_modified' => 'SAP',
							'date_modified' => date("Y-m-d H:i:s")
						));
					}
					else {
						$material->update(array(
							'mps_material_currency_id' => 1,
							'mps_material_name' => $row['item_desc'],
							'mps_material_hsc' => $row['mps_hsc'],
							'mps_material_ean_number' => $row['mps_eancode'],
							'mps_material_setof' => $row['mps_setof'],
							'mps_material_islongterm' => $row['mps_longterm'],
							'mps_material_active' => 1,
							'mps_material_width' => ($row['mps_width']) ? $row['mps_width'] : null,
							'mps_material_height' => ($row['mps_height']) ? $row['mps_height'] : null,
							'mps_material_length' => ($row['mps_length']) ? $row['mps_length']  : null,
							'mps_material_radius' => ($row['mps_radius']) ? $row['mps_radius'] : null,
							'user_modified' => 'SAP',
							'date_modified' => date("Y-m-d H:i:s")
						));
					}
					
					$messages[] = "Item <b>$material_code</b> was successfully updated.";
				}
				else {
					
					$material->create(array(
						'mps_material_material_collection_id' => $dataloader['collections'][$_collection],
						'mps_material_material_collection_category_id' => $dataloader['collection_categories'][$_collection_category],
						'mps_material_material_category_id' => $dataloader['categories'][$_category],
						'mps_material_material_purpose_id' => $dataloader['purposes'][$_purpose],
						'mps_material_material_planning_type_id' => $dataloader['planning_types'][$_planning],
						'mps_material_currency_id' => 1,
						'mps_material_code' => $material_code,
						'mps_material_name' => $row['item_desc'],
						'mps_material_hsc' => $row['mps_hsc'],
						'mps_material_ean_number' => $row['mps_eancode'],
						'mps_material_price' => $row['ret_price_chf'],
						'mps_material_setof' => $row['mps_setof'],
						'mps_material_islongterm' => $row['mps_longterm'],
						'mps_material_active' => 1,
						'mps_material_width' => ($row['mps_width']) ? $row['mps_width'] : null,
						'mps_material_height' => ($row['mps_height']) ? $row['mps_height'] : null,
						'mps_material_length' => ($row['mps_length']) ? $row['mps_length']  : null,
						'mps_material_radius' => ($row['mps_radius']) ? $row['mps_radius'] : null,
						'user_modified' => 'SAP',
						'date_modified' => date("Y-m-d H:i:s")
					));
					
					$messages[] = "Item <b>$material_code</b> was successfully inserted.";
				}
	
				// update item price for existing order sheet
				if ($material->id && $update_ordersheets) {
				
					$items = $model->query("
						SELECT DISTINCT 
							mps_ordersheet_item_id
						FROM mps_ordersheet_items
					")
					->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
					->filter('material', "mps_ordersheet_item_material_id = ".$material->id)
					->filter('workflow_state', "mps_ordersheet_workflowstate_id IN (1,2,3,4,5,7,8) ")
					->filter('not_ordered', "(mps_ordersheet_item_purchase_order_number IS NULL OR mps_ordersheet_item_purchase_order_number='') ")
					->fetchAll();
					
					if ($items) {
						
						foreach ($items as $item) {
							
							$ordersheet_item->read($item['mps_ordersheet_item_id']);
							$ordersheet_item->update(array(
								'mps_ordersheet_item_price' => $material->price
							));
							
							if (!$ordersheets[$ordersheet_item->ordersheet_id]) {
								$ordersheet->read($ordersheet_item->ordersheet_id);
								$mastersheet->read($ordersheet->mastersheet_id);
								$Company->read($ordersheet->address_id);
								$ordersheets[$ordersheet_item->ordersheet_id] = $mastersheet->name.', '.$Company->company;
							}
						}
					}
				}

			}
		}
				
	} else {
		$messages[] = $translate->missing_import_request;
	}

	//remove all imported items
	/*
	foreach ($data as $row) {
		
		
		$result = $exchange->db->exec("
			DELETE 
			FROM mps_item_mst
			WHERE item_id = " . $row["item_id"]);	
	}
	*/
	
	// for browser request
	if ($_REQUEST['application']) {
		
		if ($messages) {
			$messages = '<p>'.join('</p><p>', $messages).'</p>';
		} else {
			$messages = "<p>No inserted or updated materials data.</p>";
		}
		
		if ($errors) {
			$errors = '<p>'.join('</p><p>', $errors).'</p>';
		} else {
			$errors = "<p>No errors occurred.</p>";
		}
		
		if ($ordersheets) {
			$ordersheets = '<p>'.join('</p><p>', $ordersheets).'</p>';
		} else {
			$ordersheets = "<p>No order sheets affected from import routine</p>";
		}
		
		header('Content-Type: text/json');
		
		echo json_encode(array(
			'response' => true,
			'success' => $messages,
			'errors' => $errors,
			'ordersheets' => $ordersheets
		));
	}
	