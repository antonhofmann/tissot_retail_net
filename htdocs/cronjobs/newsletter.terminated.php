<?php 

session_name('retailnet');
session_start();

$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';

define(DEBUGGING_MODE, false);

$_TERMINATED_ERRORS = array();

$model = Connector::get('db_retailnet_news');

$user = User::instance();


$_FILEPATH_SEND_NEWSLETTER = $_SERVER['DOCUMENT_ROOT']."/applications/modules/news/newsletter/send.php";
$_FILEPATH_PUBLISH_NEWSLETTER = $_SERVER['DOCUMENT_ROOT']."/applications/modules/news/newsletter/publish.php";

if (!file_exists($_FILEPATH_SEND_NEWSLETTER)) {
	$_TERMINATED_ERRORS[] = "Newsletter sendmail $_FILEPATH_SEND_NEWSLETTER not found.";
	goto BLOCK_RESPONDING;
}


// get terminated newsletters ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_RESPONSE = array();

$sth = $model->prepare("
	SELECT 
		newsletter_id AS id,
		newsletter_publish_state_id AS state
	FROM newsletters
	WHERE newsletter_send_date = CURDATE() AND newsletter_publish_state_id IN (2,3)
	ORDER BY newsletter_send_date
");

$sth->execute();
$_TERMINATED_NEWSLETTERS = $sth->fetchAll();

if (!$_TERMINATED_NEWSLETTERS) {
	$_TERMINATED_ERRORS[] = "Newsletter(s) not found.";
	goto BLOCK_RESPONDING;
}

$_RESPONSE_PUBLISHING = array();
$_IS_PUBLISHED = array();

// publish newsletter
foreach ($_TERMINATED_NEWSLETTERS as $newsletter) {

	$_id = $newsletter['id'];

	if ($newsletter['state'] < 3) {

		ob_start();
		$_REQUEST['id'] = $_id;
		$_REQUEST['state'] = 3;
		$_REQUEST['terminated'] = true;
		$_REQUEST['debuge'] = DEBUGGING_MODE;

		require $_FILEPATH_PUBLISH_NEWSLETTER;
		$response = ob_get_contents();
		$response = json_decode($response, true);
		$_RESPONSE_PUBLISHING[$_id] = $response;

		ob_end_clean();

		if ($response['success']) {
			$_IS_PUBLISHED[$_id] = true;
		}

	} else {
		$_IS_PUBLISHED[$_id] = true;
	}
}

// send newsletter
foreach ($_TERMINATED_NEWSLETTERS as $newsletter) {

	$_id = $newsletter['id'];

	if ($_IS_PUBLISHED[$_id]) {

		ob_start();
		$_REQUEST['id'] = $_id;
		$_REQUEST['terminated'] = true;
		$_REQUEST['debuge'] = DEBUGGING_MODE;
		
		require $_FILEPATH_SEND_NEWSLETTER;
		$response = ob_get_contents();
		ob_end_clean();

		$_RESPONSE_NEWSLETTERS[$_id] = json_decode($response);
	}
}

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONDING:

ob_start();

if (DEBUGGING_MODE && $_SERVER['SERVER_NAME']) {
	
	/*
	header('Content-Type: text/json');
	
	echo json_encode(array(
		'publshing' => $_RESPONSE_PUBLISHING,
		'newsletters' => $_RESPONSE_NEWSLETTERS,
		'errors' => $_TERMINATED_ERRORS
	));
	*/
}
