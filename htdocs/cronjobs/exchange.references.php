<?php 

if (!$_SERVER['DOCUMENT_ROOT']) {
	$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));
}

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';

ini_set('memory_limit', '8096M');

$application = $_REQUEST['application'];

$IMPORT_KEY = date('U');

$_ERRORS = 0;

define('MODE_DEBUGGING', false);

$_ERRORS = array();
$_SUCCESSES = array();
$_NOTIFICATIONS = array();
$_SOURCES = array();

// php comand lien, cronjob runtimes
if (!$application) {
	Connector::instance();
	$_SOURCES['message'][] = "Cronjob Mode, set connector for live server: swatch.com";
}

// for browser request
$user = User::instance();

// connector identificators
$connectors = array(
	'08' => new Model('lps')
);

$_ORDER_LINE_TAG_CONFIRM_ITEM = 'PR';

$_DIR_RAMCO = $_SERVER['DOCUMENT_ROOT']."/ramco_exchange";
$_DIR_PROCCESSD = "$_DIR_RAMCO/processed";
$_DIR_FAILURES = "$_DIR_RAMCO/failures";



// get references ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$productGroups = array();
$productSubGroups = array();
$collections = array();
$collectionCategories = array();
$references = array();
$_BIND_SUBGROUPS = array();

//$files = glob("$_DIR_RAMCO/lps.references.*.{txt}", GLOB_BRACE);
$files = glob("$_DIR_RAMCO/lps.reference.*.{txt}", GLOB_BRACE);

if ($files) {

	foreach ($files as $file) {
		
		$lines = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

		if ($lines) {

			foreach ($lines as $i => $line) {
				
				$data = explode("\t", $line);

				if (count($data) == 13 and $data[0] == $_ORDER_LINE_TAG_CONFIRM_ITEM) {

					$division = trim(substr($data[1], 1, 2));

					// product groups
					$productGroup = trim($data[2]);
					
					if ($productGroup) {
						$productGroups[$division][$productGroup] = trim($data[3]);
					}

					// product subgroup
					$productSubgroup = trim($data[4]);
					
					if ($productSubgroup) {
						$productSubGroups[$division][$productSubgroup] = trim($data[5]);
						$_BIND_SUBGROUPS[$division][$productSubgroup] = $productGroup;
					}

					// collection
					$collection = trim($data[8]);

					if ($collection) {
						$key = strtolower(str_replace(' ', '', $collection));
						$collections[$division][$key] = $collection;
					}

					// collection categories
					$colectionCategory = trim($data[9]);

					if ($colectionCategory) {
						$key = strtolower(str_replace(' ', '', $colectionCategory));
						$collectionCategories[$division][$key] = $colectionCategory;
					}

					// product type
					$key = trim($data[10]);
					$type = $key=='01' ? 1 : null;
					$type = $key=='15' ? 2 : $type;

					// references
					$reference = trim($data[6]);
						
					if ($reference) {

						$key = strtolower(str_replace(' ', '', $reference));
						$references[$division][$key] = array(
							'product_group' => $productGroup,
							'product_subgroup' => $productSubgroup,
							'collection' => $collection,
							'colection_category' => $colectionCategory,
							'type' => $type,
							'code' => $reference,
							'name' => trim($data[7]),
							'price' => trim($data[11]),
							'currency' => strtolower(trim($data[12])),
						);
					}
				}
			}
		}
	}

	$_IMPORTS['product.groups'] = $productGroups;
	$_IMPORTS['product.subgroups'] = $productSubGroups;
	$_IMPORTS['collections'] = $collections;
	$_IMPORTS['collection.categories'] = $collectionCategories;
	$_IMPORTS['references'] = $references;

}


// import product groups :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$totalProductGroups = 0;
$_PRODUCT_GROUPS = array();

if (!$_ERRORS && $productGroups) {

	$queryInsert = "
		INSERT INTO lps_product_groups (
			lps_product_group_code,
			lps_product_group_name,
			lps_product_group_active,
			user_created
		) VALUES (?,?,1,'sap')
	";

	$queryUpdate = "
		UPDATE lps_product_groups SET 
			lps_product_group_code = ?,
			lps_product_group_name = ?,
			user_modified = 'sap',
			date_modified = NOW()
		WHERE lps_product_group_id = ?
	";

	foreach ($productGroups as $division => $groups) {
		
		$model = $connectors[$division];

		$insert = $model->db->prepare($queryInsert);
		$update = $model->db->prepare($queryUpdate);

		// get product groups
		$result = $model->query("
			SELECT 
				lps_product_group_id AS id,
				LOWER(lps_product_group_code) AS code
			FROM lps_product_groups
		")->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$code = $row['code'];
				$_PRODUCT_GROUPS[$division][$code] = $row['id'];
			}
		}

		// import product groups
		foreach ($groups as $code => $name) {
			
			$id = null;
			$_code = strtolower($code);
			$id = $_PRODUCT_GROUPS[$division][$_code];

			// update product group
			if ($id) {

				$response = MODE_DEBUGGING ? true : $update->execute(array($code, $name, $id));

				if ($response) $_SUCCESSES[] = "<p>Product group <b>$code, $name ($id)</b> was updated.</p>";
				else $_ERRORS[] = "<p>Product group <b>$code, $name ($id)</b> was NOT updated.</p>";
			} 
			// insert product group
			else {

				$response = MODE_DEBUGGING ? true : $insert->execute(array($code, $name));

				if ($response) {
					$id = MODE_DEBUGGING ? rand(5,15) :  $model->db->lastInsertId();
					$_PRODUCT_GROUPS[$division][$_code] = $id;
					$_SUCCESSES[] = "<p>Missing product group <b>$code, $name ($id)</b> was inserted.</p>";
				}
				else $_ERRORS[] = "<p>Missing product group <b>$code, $name ($id)</b> was NOT inserted.</p>";
			}

			$totalProductGroups = $totalProductGroups + $response;
		}
	}
	
	$_SOURCES['product.groups.ids'] = $_PRODUCT_GROUPS;
	$_NOTIFICATIONS[] = "<p>Total imported Product Groups: $totalProductGroups</p>";

} else {
	$_NOTIFICATIONS[] = "<p>No product groups were imported.</p>";
}

// import product subgroups ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$totalProductSubroups = 0;
$_PRODUCT_SUBGROUPS = array();

if (!$_ERRORS && $productSubGroups) {

	$queryInsert = "
		INSERT INTO lps_product_subgroups (
			lps_product_subgroup_group_id,
			lps_product_subgroup_code,
			lps_product_subgroup_name,
			lps_product_subgroup_active,
			user_created
		) VALUES (?,?,?,1,'sap')
	";

	$queryUpdate = "
		UPDATE lps_product_subgroups SET 
			lps_product_subgroup_group_id = ?,
			lps_product_subgroup_code = ?,
			lps_product_subgroup_name = ?,
			user_modified = 'sap',
			date_modified = NOW()
		WHERE lps_product_subgroup_id = ?
	";

	foreach ($productSubGroups as $division => $subgroups) {
		
		$model = $connectors[$division];

		$insert = $model->db->prepare($queryInsert);
		$update = $model->db->prepare($queryUpdate);

		// get product groups
		$result = $model->query("
			SELECT 
				lps_product_subgroup_id AS id,
				lps_product_subgroup_group_id AS group_id,
				LOWER(lps_product_subgroup_code) AS code
			FROM lps_product_subgroups
		")->fetchAll();

		if ($result) {
			
			foreach ($result as $row) {
				
				$code = $row['code'];
				
				$_PRODUCT_SUBGROUPS[$division][$code] = array(
					'id' => $row['id'],
					'group' => $row['group_id']
				);
			}
		}

		// import product groups
		foreach ($subgroups as $code => $name) {
			
			$_code = strtolower($code);
			$id = $_PRODUCT_SUBGROUPS[$division][$_code]['id'];

			// update product subgroup
			if ($id) {

				$group = $_PRODUCT_SUBGROUPS[$division][$_code]['group'];
				$response = MODE_DEBUGGING ? true : $update->execute(array($group, $code, $name, $id));

				if ($response) $_SUCCESSES[] = "<p>Product subgroup <b>$code, $name ($id)</b> was updated.</p>";
				else $_ERRORS[] = "<p>Product subgroup <b>$code, $name ($id)</b> was NOT updated.</p>";
			} 
			// insert product group
			else {

				$group = strtolower($_BIND_SUBGROUPS[$division][$code]);

				if ($_PRODUCT_GROUPS[$division][$group]) {

					$group = $_PRODUCT_GROUPS[$division][$group];
					//$_SUCCESSES[] = "PRODUCT SUBGROUP: group $group for $code found";

					$response = MODE_DEBUGGING ? true : $insert->execute(array($group, $code, $name));

					if ($response) {
						$id = MODE_DEBUGGING ? rand(5,15) : $model->db->lastInsertId();
						$_PRODUCT_SUBGROUPS[$division][$_code] = $id;
						$_SUCCESSES[] = "<p>Missing product subgroup <b>$code, $name ($id)</b> was inserted.</p>";
					}
					else $_ERRORS[] = "<p>Missing product subgroup <b>$code, $name ($id)</b> was NOT inserted.</p>";

				} else {
					$_ERRORS[] = "<p>PRODUCT SUBGROUPS: product group for $code, $name not found.</p>";
				}
			}

			$totalProductSubroups = $totalProductSubroups + $response;
		}
	}
	
	$_SOURCES['product.subgroups'] = $_PRODUCT_SUBGROUPS;
	$_SOURCES['product.subgroups.binds'] = $_BIND_SUBGROUPS;
	$_NOTIFICATIONS[] = "<p>Total imported Product Subgroups: $totalProductSubroups</p>";

} else {
	$_NOTIFICATIONS[] = "<p>No product subgroups were imported.</p>";
}



// import collections ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$totalCollections = 0;
$_COLLECTIONS = array();

if (!$_ERRORS && $collections) {

	$queryInsert = "
		INSERT INTO lps_collections (
			lps_collection_code,
			lps_collection_active,
			user_created
		) VALUES (?,1,'sap')
	";

	$queryUpdate = "
		UPDATE lps_collections SET 
			lps_collection_code = ?,
			user_modified = 'sap',
			date_modified = NOW()
		WHERE lps_collection_id = ?
	";

	foreach ($collections as $division => $_collections) {
		
		$model = $connectors[$division];

		$insert = $model->db->prepare($queryInsert);
		$update = $model->db->prepare($queryUpdate);

		// get current collections
		$result = $model->query("
			SELECT 
				lps_collection_id AS id,
				REPLACE(LOWER(lps_collection_code), ' ', '') AS code
			FROM lps_collections
		")->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$code = $row['code'];
				$_COLLECTIONS[$division][$code] = $row['id'];
			}
		}

		// import product groups
		foreach ($_collections as $key => $collection) {
			
			$id = $_COLLECTIONS[$division][$key];

			// update product group
			if ($id) {

				$response = MODE_DEBUGGING ? true : $update->execute(array($collection, $id));

				if ($response) $_SUCCESSES[] = "<p>Collection <b>$collection ($id)</b> was updated.</p>";
				else $_ERRORS[] = "<p>Collection <b>$collection ($id)</b> was NOT updated.</p>";
			} 
			// insert product group
			else {

				$response = MODE_DEBUGGING ? true : $insert->execute(array($collection));

				if ($response) {
					$id = MODE_DEBUGGING ? rand(5,15) : $model->db->lastInsertId();
					$_COLLECTIONS[$division][$key] = $id;
					$_SUCCESSES[] = "<p>Missing collection <b>$collection ($id)</b> was inserted.</p>";
				}
				else $_ERRORS[] = "<p>Missing collection <b>$collection</b> was NOT inserted.</p>";
			}

			$totalCollections = $totalCollections + $response;
		}
	}
	
	$_SOURCES['collections'] = $_COLLECTIONS;
	$_NOTIFICATIONS[] = "<p>Total imported collections: $totalCollections</p>";

} else {
	$_NOTIFICATIONS[] = "<p>No collections were imported.</p>";
}



// import collection categories ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$totalCollectionCategories = 0;
$_COLLECTION_CATEGORIES = array();

if (!$_ERRORS && $collectionCategories) {

	$queryInsert = "
		INSERT INTO lps_collection_categories (
			lps_collection_category_code,
			lps_collection_category_active,
			user_created
		) VALUES (?,1,'sap')
	";

	$queryUpdate = "
		UPDATE lps_collection_categories SET 
			lps_collection_category_code = ?,
			user_modified = 'sap',
			date_modified = NOW()
		WHERE lps_collection_category_id = ?
	";

	foreach ($collectionCategories as $division => $_categories) {
		
		$model = $connectors[$division];

		$insert = $model->db->prepare($queryInsert);
		$update = $model->db->prepare($queryUpdate);

		// get current collections
		$result = $model->query("
			SELECT 
				lps_collection_category_id AS id,
				REPLACE(LOWER(lps_collection_category_code), ' ', '') AS code
			FROM lps_collection_categories
		")->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$code = $row['code'];
				$_COLLECTION_CATEGORIES[$division][$code] = $row['id'];
			}
		}

		

		// import product groups
		foreach ($_categories as $key => $category) {
			
			$id = $_COLLECTION_CATEGORIES[$division][$key];

			// update product group
			if ($id) {

				$response = MODE_DEBUGGING ? true : $update->execute(array($category, $id));

				if ($response) $_SUCCESSES[] = "<p>Collection Category <b>$category ($id)</b> was updated.</p>";
				else $_ERRORS[] = "<p>Collection Category <b>$category ($id)</b> was updated.</p>";
			} 
			// insert product group
			else {
				$response = MODE_DEBUGGING ? true : $insert->execute(array($category));
				if ($response) {
					$id = MODE_DEBUGGING ? rand(5,15) : $model->db->lastInsertId();
					$_COLLECTION_CATEGORIES[$division][$key] = $id;
					$_SUCCESSES[] = "<p>Missing collection category <b>$category ($id)</b> was inserted.</p>";
				}
				else $_ERRORS[] = "<p>Missing collection category <b>$category</b> was NOT inserted.</p>";
			}

			$totalCollectionCategories = $totalCollectionCategories + $response;
		}
	}
	
	$_SOURCES['collections'] = $_COLLECTION_CATEGORIES;
	$_NOTIFICATIONS[] = "<p>Total imported collection categories: $totalCollections</p>";

} else {
	$_NOTIFICATIONS[] = "<p>No collection categories were imported.</p>";
}

// import references ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$totalReferences = 0;
$_REFERENCES = array();
$_CURRENCIES = array();

if (!$_ERRORS && $references) {

	$queryInsert = "
		INSERT INTO lps_references (
			lps_reference_product_group_id,
			lps_reference_product_subgroup_id,
			lps_reference_collection_id,
			lps_reference_collection_category_id,
			lps_reference_product_type_id,
			lps_reference_code,
			lps_reference_name,
			lps_reference_price_point,
			lps_reference_currency_id,
			lps_reference_active,
			user_created
		) VALUES (
			:product_group,
			:product_subgroup,
			:collection,
			:colection_category,
			:type,
			:code,
			:name,
			:price,
			:currency,
			1,
			'sap'
		)
	";

	$queryUpdate = "
		UPDATE lps_references SET 
			lps_reference_product_group_id = :product_group,
			lps_reference_product_subgroup_id = :product_subgroup,
			lps_reference_collection_id = :collection,
			lps_reference_collection_category_id = :colection_category,
			lps_reference_product_type_id = :type,
			lps_reference_code = :code,
			lps_reference_name = :name,
			lps_reference_price_point = :price,
			lps_reference_currency_id = :currency,
			user_modified = 'sap',
			date_modified = NOW()
		WHERE lps_reference_id = :id
	";

	// get currencies
	$model = $connectors['08'];

	$result = $model->query("
		SELECT 
			currency_id AS id,
			REPLACE(LOWER(currency_symbol), ' ', '') AS currency
		FROM db_retailnet.currencies
	")->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$currency = $row['currency'];
			$_CURRENCIES[$currency] = $row['id'];
		}
	}

	foreach ($references as $division => $_references) {
		
		$model = $connectors[$division];

		$insert = $model->db->prepare($queryInsert);
		$update = $model->db->prepare($queryUpdate);

		// get current references
		$result = $model->query("
			SELECT 
				lps_reference_id,
				lps_reference_product_group_id,
				lps_reference_product_subgroup_id,
				lps_reference_collection_id,
				lps_reference_collection_category_id,
				lps_reference_product_type_id,
				lps_reference_currency_id,
				FORMAT(lps_reference_price_point, 2) AS lps_reference_price_point,
				lps_reference_name,
				REPLACE(LOWER(lps_reference_code), ' ', '') AS code
			FROM lps_references
		")->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				if ($row['code']) {
					$code = $row['code'];
					$_REFERENCES[$division][$code] = array(
						'id' => $row['lps_reference_id'],
						'product_group' => $row['lps_reference_product_group_id'],
						'product_subgroup' => $row['lps_reference_product_subgroup_id'],
						'collection' => $row['lps_reference_collection_id'],
						'colection_category' => $row['lps_reference_collection_category_id'],
						'type' => $row['lps_reference_product_type_id'],
						'currency' => $row['lps_reference_currency_id'],
						'name' => $row['lps_reference_name'],
						'price' => $row['lps_reference_price_point']
					);
				}
			}
		}

		

		// import product groups
		foreach ($_references as $code => $reference) {
			
			$id = $_REFERENCES[$division][$code]['id'];
			$product_group = str_replace(' ', '', strtolower($reference['product_group']));
			$product_subgroup = str_replace(' ', '', strtolower($reference['product_subgroup']));
			$collection = str_replace(' ', '', strtolower($reference['collection']));
			$colection_category = str_replace(' ', '', strtolower($reference['colection_category']));
			$currency = str_replace(' ', '', strtolower($reference['currency']));
			$type = $reference['type'];
			


			$data = array(
				'product_group' => $_PRODUCT_GROUPS[$division][$product_group] ?: $_REFERENCES[$division][$code]['product_group'],
				'product_subgroup' => $_PRODUCT_SUBGROUPS[$division][$product_subgroup]['id'] ?: $_REFERENCES[$division][$code]['product_subgroup'],
				'collection' => $_COLLECTIONS[$division][$collection] ?: $_REFERENCES[$division][$code]['collection'],
				'colection_category' => $_COLLECTION_CATEGORIES[$division][$colection_category] ?: $_REFERENCES[$division][$code]['colection_category'],
				'type' => $type ?: $_REFERENCES[$division][$code]['type'],
				'code' => $reference['code'],
				'name' => $reference['name'] ?:  $_REFERENCES[$division][$code]['name'],
				'price' => $reference['price'] ?:  $_REFERENCES[$division][$code]['price'],
				'currency' => $_CURRENCIES[$currency] ?:  $_REFERENCES[$division][$code]['currency']
			);


			// update product group
			if ($id) {

				$data['id'] = $id;
				$response = MODE_DEBUGGING ? true : $update->execute($data);

				if ($response) $_SUCCESSES[] = "<p>Item <b>{$reference[code]}, $name ($id)</b> was updated.</p>";
				else $_ERRORS[] = "<p>Item <b>{$reference[code]}, $name ($id)</b> was NOT updated.</p>";

				$_UPDATED['references'][] = $data;
			} 
			// insert product group
			else {
				
				$response = MODE_DEBUGGING ? true : $insert->execute($data);

				if ($response) {
					$id = MODE_DEBUGGING ? rand(5,15) : $model->db->lastInsertId();
					$_SUCCESSES[] = "<p>Missing item <b>{$reference[code]}, $name ($id)</b> was inserted.</p>";
				}
				else $_ERRORS[] = "<p>Missing item <b>{$reference[code]}, $name</b> was NOT inserted.</p>";

				$_INSERTED['references'][] = $data;
			}

			$totalReferences = $totalReferences + $response;
		}
	}
	
	$_SOURCES['references'] = $_REFERENCES;
	$_SOURCES['currencies'] = $_CURRENCIES;
	$_NOTIFICATIONS[] = "<p>Total imported references: $totalReferences</p>";

} else {
	$_NOTIFICATIONS[] = "<p>No references were imported.</p>";
}



// update launch plans :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::





// cleanup reference files :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


/*
echo "<pre>";
echo "NOTIFICATIONS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n";
print_r($_NOTIFICATIONS);
echo "\n\nSUCCESSES :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n";
print_r($_SUCCESSES);
echo "\n\nERRORS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n";
print_r($_ERRORS);
echo "\n\nIMPORTS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n";
print_r($_IMPORTS);
echo "\n\nSOURCES ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n";
print_r($_SOURCES);
echo "\n\nINSERTED ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n";
print_r($_INSERTED);
echo "\n\nUPDATED :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n";
print_r($_UPDATED);
echo "</pre>";
die();
*/

if ($files && !MODE_DEBUGGING) {

	@chmod($_DIR_RAMCO, 0777);
	@chmod($_DIR_PROCCESSD, 0777);
	@chmod($_DIR_FAILURES, 0777);

	foreach ($files as $file) {

		$filename = basename($file);
		$target = $_DIR_PROCCESSD."/$filename";

		if (!MODE_DEBUGGING) {

			@chmod($file, 0666);

			$response = copy($file, $target);

			if ($response) {
				@unlink($file);
			}
		}
	}
}

//print_r($_ERRORS);die;
// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
if ($application) {
	
	header('Content-Type: text/json');
	
	echo json_encode(array(
		'response' => true,
		'message' => is_array($_NOTIFICATIONS) ? join($_NOTIFICATIONS) : null,
		'success' => is_array($_SUCCESSES) ? join($_SUCCESSES) : null,
		'errors' => $_ERRORS ? join($_ERRORS) : null
	));
}
