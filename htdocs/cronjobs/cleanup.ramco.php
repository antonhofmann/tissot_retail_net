<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

ini_set('memory_limit', '4096M');
ini_set('max_execution_time', 3600);
set_time_limit(3600);

Connector::instance();

$_ERRORS = array();
$_FAILURES = array();
$_CONSOLE = array();
$_NOTIFICATIONS = array();
$_DATA = array();
$_PONS = array();

// start import
$_CONSOLE[] = "START: ".date('d.m.Y H:i:s');

// exchange server
$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);

// connector identificators
$connectors = array(
	'08' => Connector::DB_RETAILNET_MPS
);

// db models
$models = array(
	'08' => new Model(Connector::DB_RETAILNET_MPS)
);


/**
 * CLEANUP 
 * Cleanup unused/double orders
 **************************************************************************************************/

$exchange->db->exec("
	DELETE FROM db_retailnet_ramco_exchange.ramco_orders
	WHERE ramco_order_id NOT IN (
		SELECT * FROM (
			SELECT MAX(ramco_order_id) AS ramco_order_id
			FROM db_retailnet_ramco_exchange.ramco_orders 
			GROUP BY REPLACE(LOWER(mps_salesorder_ponumber), ' ', '')
		) as maxids
	)
");

$exchange->db->exec("
	DELETE FROM db_retailnet_ramco_exchange.ramco_confirmed_items
	WHERE ramco_confirmed_id NOT IN (
		SELECT ramco_confirmed_id FROM (
			SELECT MAX(ramco_confirmed_id) AS ramco_confirmed_id
			FROM db_retailnet_ramco_exchange.ramco_confirmed_items 
			GROUP BY 
				REPLACE(LOWER(mps_salesorderitem_material_code), ' ', ''),
				REPLACE(LOWER(mps_salesorder_ponumber), ' ', '')
		) as maxids
	)
");

$exchange->db->exec("
	DELETE FROM db_retailnet_ramco_exchange.ramco_shipped_items
	WHERE ramco_shipped_id NOT IN (
		SELECT ramco_shipped_id FROM (
			SELECT MAX(ramco_shipped_id) AS ramco_shipped_id
			FROM db_retailnet_ramco_exchange.ramco_shipped_items 
			GROUP BY 
				REPLACE(LOWER(mps_salesorderitem_material_code), ' ', ''),
				REPLACE(LOWER(mps_salesorderitem_ponumber), ' ', '')
		) as maxids
	)
");

foreach ($models as $div => $model) {
	
	$model->db->exec("
		DELETE FROM mps_ramco_orders
		WHERE mps_ramco_order_id NOT IN (
			SELECT mps_ramco_order_id FROM (
				SELECT MAX(mps_ramco_order_id) AS mps_ramco_order_id
				FROM mps_ramco_orders
				GROUP BY REPLACE(LOWER(mps_ramco_order_po_number), ' ', '')
			) as maxids
		)
	");

	$model->db->exec("
		DELETE FROM mps_ordersheet_item_confirmed
		WHERE mps_ordersheet_item_confirmed_id NOT IN (
			SELECT * FROM (
				SELECT MAX(mps_ordersheet_item_confirmed_id) AS mps_ordersheet_item_confirmed_id
				FROM mps_ordersheet_item_confirmed 
				GROUP BY 
					REPLACE(LOWER(mps_ordersheet_item_confirmed_material_code), ' ', ''), 
					REPLACE(LOWER(mps_ordersheet_item_confirmed_po_number), ' ', '')
			) as maxids
		)
	");

	$model->db->exec("
		DELETE FROM mps_ordersheet_item_shipped
		WHERE mps_ordersheet_item_shipped_id NOT IN (
			SELECT * FROM (
				SELECT MAX(mps_ordersheet_item_shipped_id) AS mps_ordersheet_item_shipped_id
				FROM mps_ordersheet_item_shipped 
				GROUP BY 
					REPLACE(LOWER(mps_ordersheet_item_shipped_material_code), ' ', ''), 
					REPLACE(LOWER(mps_ordersheet_item_shipped_po_number), ' ', '')
			) as maxids
		)
	");
}


/**
 * GET FAILURES
 * get all ordersheet with workflow state submitted and confirmed or shipped items great then 0
 **************************************************************************************************/

$_ORDERSHEETS = array();

foreach ($models as $division => $model) {
	
	$sth = $model->db->prepare("
		SELECT 
			mps_ordersheet_id,
			GROUP_CONCAT(DISTINCT REPLACE(LOWER(mps_ordersheet_item_purchase_order_number), ' ', '')) AS pon
		FROM mps_ordersheets 
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_ordersheet_id = mps_ordersheet_id
		INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
		WHERE (
			mps_ordersheet_workflowstate_id = 9 
			AND mps_ordersheet_item_quantity_confirmed > 0
		) 
		OR (
			mps_ordersheet_workflowstate_id IN (9,10)
			AND mps_ordersheet_item_quantity_shipped > 0
		)
		GROUP BY mps_ordersheet_id
	");

	$sth->execute();
	$result = $sth->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$ordersheet = $row['mps_ordersheet_id'];
			$pon = $row['pon'];
			$_ORDERSHEETS[$division][$ordersheet] = $pon;
		}
	}
}

if (!$_ORDERSHEETS) {
	$_CONSOLE[] = "No failures";
	goto BLOCK_RESPONSE;
} 


/**
 * RESETING
 * reset all failure order sheets and ramco orders
 **************************************************************************************************/

foreach ($_ORDERSHEETS as $division => $ordersheets) {
	
	$model = $models[$division];

	$updateOrder = $exchange->db->prepare("
		UPDATE ramco_orders SET mps_order_completely_imported = 0
		WHERE REPLACE(LOWER(mps_salesorder_ponumber), ' ', '') = ?
	");

	$updateConfirmation = $exchange->db->prepare("
		UPDATE ramco_confirmed_items SET mps_item_completely_imported = 0
		WHERE REPLACE(LOWER(mps_salesorder_ponumber), ' ', '') = ?
	");

	$updateShipment = $exchange->db->prepare("
		UPDATE ramco_shipped_items SET mps_item_completely_imported = 0
		WHERE REPLACE(LOWER(mps_salesorderitem_ponumber), ' ', '') = ?
	");

	$updateOrderSheetItem = $model->db->prepare("
		UPDATE mps_ordersheet_items SET 
			mps_ordersheet_item_quantity_confirmed = NULL,
    		mps_ordersheet_item_quantity_shipped = NULL
    	WHERE mps_ordersheet_item_ordersheet_id = ?
	");

	$updateOrderSheet = $model->db->prepare("
		UPDATE mps_ordersheets SET 
			mps_ordersheet_workflowstate_id = 9
    	WHERE mps_ordersheet_id = ?
	");

	$removeTracks = $model->db->prepare("
		DELETE FROM user_trackings 
    	WHERE user_tracking_entity = 'order sheet'
    	AND user_tracking_entity_id = ?
    	AND (user_tracking_action = 'order confirmed' OR user_tracking_action = 'shipped')
	");

	$removeOrder = $model->db->prepare("
		DELETE FROM mps_ramco_orders 
    	WHERE REPLACE(LOWER(mps_ramco_order_po_number), ' ', '') = ?
	");

	$removeConfirmations = $model->db->prepare("
		DELETE FROM mps_ordersheet_item_confirmed 
    	WHERE REPLACE(LOWER(mps_ordersheet_item_confirmed_po_number), ' ', '') = ?
	");

	$removeShipment = $model->db->prepare("
		DELETE FROM mps_ordersheet_item_shipped 
    	WHERE REPLACE(LOWER(mps_ordersheet_item_shipped_po_number), ' ', '') = ?
	");

	foreach ($ordersheets as $id => $pon) {
		
		$response = $updateOrderSheetItem->execute(array($id));

		if (!$response) {
			$_ERRORS[] = "order sheet items: $id";
			continue;
		}

		$response = $updateOrderSheet->execute(array($id));

		if (!$response) {
			$_ERRORS[] = "order sheet: $id";
			continue;
		}

		$removeTracks->execute(array($id));

		$pons = explode(',', $pon);

		foreach ($pons as $p) {
			$removeOrder->execute(array($p));
			$removeConfirmations->execute(array($p));
			$removeShipment->execute(array($p));
			$updateOrder->execute(array($p));
			$updateConfirmation->execute(array($p));
			$updateShipment->execute(array($p));
		}

		$_SUCCESS[] = "Order sheet $id successfully reset. ($pon)";
	}
}


/**
 * RESPONSE
 **************************************************************************************************/

BLOCK_RESPONSE:

$_CONSOLE[] = "END import on ".date('d.m.Y H:i:s');

header('Content-Type: text/json');

echo json_encode(array(
	'response' => true,
	'errors' => $_ERRORS,
	'success' => $_SUCCESS,
	'console' => $_CONSOLE
));

