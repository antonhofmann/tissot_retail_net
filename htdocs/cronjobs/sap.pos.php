<?php

$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';

define(DEBUGGING_MODE, false);

Connector::instance();

$_CONSOLE = array();
$_MESSAGES = array();
$_ERRORS = array();
$_SOURCES = array();

// db modela
$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);
$model = new Model(Connector::DB_CORE);


// get not imported data :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	

$_NOT_IMPORTED_POS = $exchange->query("
	SELECT 
		sap_poslocation_id AS id,
		TRIM(sap_poslocation_sapnr) AS sap_imported_posaddress_sapnr,
		TRIM(sap_poslocation_name1) AS sap_imported_posaddress_name1,
		TRIM(sap_poslocation_name2) AS sap_imported_posaddress_name2,
		TRIM(sap_poslocation_search_item) AS sap_imported_posaddress_search_item,
		TRIM(sap_poslocation_address) AS sap_imported_posaddress_address,
		TRIM(sap_poslocation_zip) AS sap_imported_posaddress_zip,
		TRIM(sap_poslocation_city) AS sap_imported_posaddress_city,
		TRIM(sap_poslocation_country) AS sap_imported_posaddress_country,
		TRIM(sap_poslocation_phone) AS sap_imported_posaddress_phone,
		TRIM(sap_poslocation_fax) AS sap_imported_posaddress_fax,
		TRIM(sap_poslocation_sales_organization) AS sap_imported_posaddress_sales_organization,
		TRIM(sap_poslocation_distribution_channel) AS sap_imported_posaddress_distribution_channel,
		TRIM(sap_poslocation_division) AS sap_imported_posaddress_division,
		TRIM(sap_poslocation_order_block) AS sap_imported_posaddress_order_block,
		TRIM(sap_poslocation_order_block_sales_area) AS sap_imported_posaddress_order_block_sales_area,
		TRIM(sap_poslocation_deletion_indicator) AS sap_imported_posaddress_deletion_indicator,
		TRIM(sap_poslocation_deletion_sales_area_indicator) AS sap_imported_posaddress_deletion_sales_area_indicator
	FROM sap_poslocations
	WHERE 
		TRIM(sap_poslocation_imported_into_mps) IS NULL 
		OR TRIM(sap_poslocation_imported_into_mps) = ''
		OR TRIM(sap_poslocation_imported_into_mps) = 0 
")->fetchAll();

if (!$_NOT_IMPORTED_POS) {
	$_MESSAGES[] = "Import data not found.";
	goto BLOCK_RESPONDING;
}


// get checked data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_CHECKED_POS = array();

$result = $exchange->query("
	SELECT GROUP_CONCAT(TRIM(sap_poslocation_sapnr)) AS pons
	FROM sap_poslocations
	WHERE TRIM(sap_poslocation_imported_into_mps) IS NULL 
	OR TRIM(sap_poslocation_imported_into_mps) = ''
	OR TRIM(sap_poslocation_imported_into_mps) = 0
")->fetch();

$pons = $result['pons'];

$result = $model->query("
	SELECT * 
	FROM sap_imported_posaddresses
	WHERE sap_imported_posaddress_date_checked IS NOT NULL
	AND sap_imported_posaddress_date_checked != ''
	AND sap_imported_posaddress_date_checked != '0000-00-00'
	AND sap_imported_posaddress_sapnr IN ($pons)
	ORDER BY sap_imported_posaddress_id DESC
")->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$sap = $row['sap_imported_posaddress_sapnr'];
		$zip = $row['sap_imported_posaddress_zip'];
		
		if ($sap && $zip && !$_CHECKED_POS[$sap] && !$_CHECKED_POS[$sap][$zip]) {
			$_CHECKED_POS[$sap][$zip] = $row;
		}
	}
}

$_SOURCES['pons'] = $pons;
$_SOURCES['checked'] = $_CHECKED_POS;

// check data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_IGNORED_POS = array();

foreach ($_NOT_IMPORTED_POS as $i => $row) {

	$id = $row['id'];
	$sap = $row['sap_imported_posaddress_sapnr'];
	$zip = $row['sap_imported_posaddress_zip'];
	$division = $row['sap_imported_posaddress_division'];
	
	$lastImportData = $_CHECKED_POS[$sap][$zip];

	$checked = false;

	// ignore imports if is not mps ore mpsflikflak
	if ($division && !in_array($division, array('08'))) {
		$_IGNORED_POS[$id] = $id; 
		$_CONSOLE[] = "Division $division not matched in (08). ($id)";
		continue;
	}

	if (!$division) {
		
		if ($lastImportData['sap_imported_posaddress_sales_organization']) {
			$row['sap_imported_posaddress_sales_organization'] = $lastImportData['sap_imported_posaddress_sales_organization'];
			$_NOT_IMPORTED_POS[$i]['sap_imported_posaddress_sales_organization'] = $lastImportData['sap_imported_posaddress_sales_organization'];
		} else {
			$_IGNORED_POS[$id] = $id; 
			$_CONSOLE[] = "Missing sales organization. ($id)";
			continue;
		}
		
		if ($lastImportData['sap_imported_posaddress_distribution_channel']) {
			$row['sap_imported_posaddress_distribution_channel'] = $lastImportData['sap_imported_posaddress_distribution_channel'];
			$_NOT_IMPORTED_POS[$i]['sap_imported_posaddress_distribution_channel'] = $lastImportData['sap_imported_posaddress_distribution_channel'];
		} else {
			$_IGNORED_POS[$id] = $id;
			$_CONSOLE[] = "Missing distribution channel. ($id)";
			continue;
		}
		
		if ($lastImportData['sap_imported_posaddress_division']) {
			$row['sap_imported_posaddress_division'] = $lastImportData['sap_imported_posaddress_division'];
			$_NOT_IMPORTED_POS[$i]['sap_imported_posaddress_division'] = $lastImportData['sap_imported_posaddress_division'];
		} else {
			$_IGNORED_POS[$id] = $id; 
			$_CONSOLE[] = "Missing division. ($id)";
			continue;
		}
	}

	if (!$row['sap_imported_posaddress_division']) {
		$_IGNORED_POS[$id] = $id; 
		$_CONSOLE[] = "Missing division. ($id)";
		continue;
	}

	if (!$row['sap_imported_posaddress_distribution_channel']) {
		$_IGNORED_POS[$id] = $id; 
		$_CONSOLE[] = "Missing distribution channel. ($id)";
		continue;
	}

	if (!$row['sap_imported_posaddress_sales_organization']) {
		$_IGNORED_POS[$id] = $id; 
		$_CONSOLE[] = "Missing sales organization. ($id)";
		continue;
	}

	// sap and zip number
	if (!$lastImportData) {
		$_NOT_IMPORTED_POS[$i]['sap_imported_posaddress_date_checked'] = null;
		continue;
	}

	if ($row['sap_imported_posaddress_name1']) {
		$checked = $row['sap_imported_posaddress_name1']==$lastImportData['sap_imported_posaddress_name1'] ? true : false;
		$_SOURCES['compare'][$i]['sap_imported_posaddress_name1']['sap'] = $row['sap_imported_posaddress_name1'];
		$_SOURCES['compare'][$i]['sap_imported_posaddress_name1']['mps'] = $lastImportData['sap_imported_posaddress_name1'];
	}

	if ($checked && $row['sap_imported_posaddress_name2']) {
		$checked = $row['sap_imported_posaddress_name2']==$lastImportData['sap_imported_posaddress_name2'] ? true : false;
		$_SOURCES['compare'][$i]['sap_imported_posaddress_name2']['sap'] = $row['sap_imported_posaddress_name2'];
		$_SOURCES['compare'][$i]['sap_imported_posaddress_name2']['mps'] = $lastImportData['sap_imported_posaddress_name2'];
	}

	if ($checked && $row['sap_imported_posaddress_search_item']) {
		$checked = $row['sap_imported_posaddress_search_item']==$lastImportData['sap_imported_posaddress_search_item'] ? true : false;
		$_SOURCES['compare'][$i]['sap_imported_posaddress_search_item']['sap'] = $row['sap_imported_posaddress_search_item'];
		$_SOURCES['compare'][$i]['sap_imported_posaddress_search_item']['mps'] = $lastImportData['sap_imported_posaddress_search_item'];
	}

	if ($checked && $row['sap_imported_posaddress_address']) {
		$checked = $row['sap_imported_posaddress_address']==$lastImportData['sap_imported_posaddress_address'] ? true : false;
		$_SOURCES['compare'][$i]['sap_imported_posaddress_address']['sap'] = $row['sap_imported_posaddress_address'];
		$_SOURCES['compare'][$i]['sap_imported_posaddress_address']['mps'] = $lastImportData['sap_imported_posaddress_address'];
	}

	if ($checked && $row['sap_imported_posaddress_city']) {
		$checked = $row['sap_imported_posaddress_city']==$lastImportData['sap_imported_posaddress_city'] ? true : false;
		$_SOURCES['compare'][$i]['sap_imported_posaddress_city']['sap'] = $row['sap_imported_posaddress_city'];
		$_SOURCES['compare'][$i]['sap_imported_posaddress_city']['mps'] = $lastImportData['sap_imported_posaddress_city'];
	}

	if ($checked && $row['sap_imported_posaddress_country']) {
		$checked = $row['sap_imported_posaddress_country']==$lastImportData['sap_imported_posaddress_country'] ? true : false;
		$_SOURCES['compare'][$i]['sap_imported_posaddress_country']['sap'] = $row['sap_imported_posaddress_country'];
		$_SOURCES['compare'][$i]['sap_imported_posaddress_country']['mps'] = $lastImportData['sap_imported_posaddress_country'];
	}

	if ($checked && $row['sap_imported_posaddress_phone']) {
		$checked = $row['sap_imported_posaddress_phone']==$lastImportData['sap_imported_posaddress_phone'] ? true : false;
		$_SOURCES['compare'][$i]['sap_imported_posaddress_phone']['sap'] = $row['sap_imported_posaddress_phone'];
		$_SOURCES['compare'][$i]['sap_imported_posaddress_phone']['mps'] = $lastImportData['sap_imported_posaddress_phone'];
	}

	if ($checked && $row['sap_imported_posaddress_fax']) {
		$checked = $row['sap_imported_posaddress_fax']==$lastImportData['sap_imported_posaddress_fax'] ? true : false;
	}

	if ($checked && $row['sap_imported_posaddress_sales_organization']) {
		$checked = $row['sap_imported_posaddress_sales_organization']==$lastImportData['sap_imported_posaddress_sales_organization'] ? true : false;
		$_SOURCES['compare'][$i]['sap_imported_posaddress_sales_organization']['sap'] = $row['sap_imported_posaddress_sales_organization'];
		$_SOURCES['compare'][$i]['sap_imported_posaddress_sales_organization']['mps'] = $lastImportData['sap_imported_posaddress_sales_organization'];
	}

	if ($checked && $row['sap_imported_posaddress_distribution_channel']) {
		$checked = $row['sap_imported_posaddress_distribution_channel']==$lastImportData['sap_imported_posaddress_distribution_channel'] ? true : false;
		$_SOURCES['compare'][$i]['sap_imported_posaddress_distribution_channel']['sap'] = $row['sap_imported_posaddress_distribution_channel'];
		$_SOURCES['compare'][$i]['sap_imported_posaddress_distribution_channel']['mps'] = $lastImportData['sap_imported_posaddress_distribution_channel'];
	}

	if ($checked && $row['sap_imported_posaddress_division']) {
		$checked = $row['sap_imported_posaddress_division']==$lastImportData['sap_imported_posaddress_division'] ? true : false;
		$_SOURCES['compare'][$i]['sap_imported_posaddress_division']['sap'] = $row['sap_imported_posaddress_division'];
		$_SOURCES['compare'][$i]['sap_imported_posaddress_division']['mps'] = $lastImportData['sap_imported_posaddress_division'];
	}

	if ($checked && $row['sap_imported_posaddress_order_block']) {
		$checked = $row['sap_imported_posaddress_order_block']==$lastImportData['sap_imported_posaddress_order_block'] ? true : false;
		$_SOURCES['compare'][$i]['sap_imported_posaddress_order_block']['sap'] = $row['sap_imported_posaddress_order_block'];
		$_SOURCES['compare'][$i]['sap_imported_posaddress_order_block']['mps'] = $lastImportData['sap_imported_posaddress_order_block'];
	}

	if ($checked && $row['sap_imported_posaddress_deletion_indicator']) {
		$checked = $row['sap_imported_posaddress_deletion_indicator']==$lastImportData['sap_imported_posaddress_deletion_indicator'] ? true : false;
		$_SOURCES['compare'][$i]['sap_imported_posaddress_deletion_indicator']['sap'] = $row['sap_imported_posaddress_deletion_indicator'];
		$_SOURCES['compare'][$i]['sap_imported_posaddress_deletion_indicator']['mps'] = $lastImportData['sap_imported_posaddress_deletion_indicator'];
	}

	if ($checked && $row['sap_imported_posaddress_deletion_sales_area_indicator']) {
		$checked = $row['sap_imported_posaddress_deletion_sales_area_indicator']==$lastImportData['sap_imported_posaddress_deletion_sales_area_indicator'] ? true : false;
		$_SOURCES['compare'][$i]['sap_imported_posaddress_deletion_sales_area_indicator']['sap'] = $row['sap_imported_posaddress_deletion_sales_area_indicator'];
		$_SOURCES['compare'][$i]['sap_imported_posaddress_deletion_sales_area_indicator']['mps'] = $lastImportData['sap_imported_posaddress_deletion_sales_area_indicator'];
	}

	if ($checked && $row['sap_imported_posaddress_order_block_sales_area']) {
		$checked = $row['sap_imported_posaddress_order_block_sales_area']==$lastImportData['sap_imported_posaddress_order_block_sales_area'] ? true : false;
		$_SOURCES['compare'][$i]['sap_imported_posaddress_order_block_sales_area']['sap'] = $row['sap_imported_posaddress_order_block_sales_area'];
		$_SOURCES['compare'][$i]['sap_imported_posaddress_order_block_sales_area']['mps'] = $lastImportData['sap_imported_posaddress_order_block_sales_area'];
	}

	$_NOT_IMPORTED_POS[$i]['sap_imported_posaddress_date_checked'] = $checked ? date('Y-m-d') : '1';
}

$_SOURCES['not.imported'] = $_NOT_IMPORTED_POS;

// import data :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	INSERT INTO sap_imported_posaddresses (
		sap_imported_posaddress_sapnr,
		sap_imported_posaddress_name1,
		sap_imported_posaddress_name2,
		sap_imported_posaddress_search_item,
		sap_imported_posaddress_address,
		sap_imported_posaddress_zip,
		sap_imported_posaddress_city,
		sap_imported_posaddress_country,
		sap_imported_posaddress_phone,
		sap_imported_posaddress_fax,
		sap_imported_posaddress_sales_organization,
		sap_imported_posaddress_distribution_channel,
		sap_imported_posaddress_division,
		sap_imported_posaddress_order_block,
		sap_imported_posaddress_order_block_sales_area,
		sap_imported_posaddress_deletion_indicator,
		sap_imported_posaddress_deletion_sales_area_indicator,
		sap_imported_posaddress_date_checked,
		sap_imported_posaddress_date_updated,
		date_created,
		user_created
	) VALUES (
		:sap_imported_posaddress_sapnr,
		:sap_imported_posaddress_name1,
		:sap_imported_posaddress_name2,
		:sap_imported_posaddress_search_item,
		:sap_imported_posaddress_address,
		:sap_imported_posaddress_zip,
		:sap_imported_posaddress_city,
		:sap_imported_posaddress_country,
		:sap_imported_posaddress_phone,
		:sap_imported_posaddress_fax,
		:sap_imported_posaddress_sales_organization,
		:sap_imported_posaddress_distribution_channel,
		:sap_imported_posaddress_division,
		:sap_imported_posaddress_order_block,
		:sap_imported_posaddress_order_block_sales_area,
		:sap_imported_posaddress_deletion_indicator,
		:sap_imported_posaddress_deletion_sales_area_indicator,
		:sap_imported_posaddress_date_checked,
		CURDATE(),
		NOW(),
		:user_created
	)	
");

		
foreach ($_NOT_IMPORTED_POS as $row) {
	
	// exchange id
	$key = array_shift($row);
	$response = false;
	
	if ($_IGNORED_POS[$key]) {
		$_CONSOLE[] = "SAP record $key is ignored.";
		$response = true;
	} else {
		$row['user_created'] =  'ramco';
		$response = DEBUGGING_MODE ? true : $sth->execute($row);
	}
	
	if ($response) {
		
		if (DEBUGGING_MODE) $response = true;
		else {
			$response = $exchange->db->exec("
				UPDATE sap_poslocations SET
					sap_poslocation_imported_into_mps = 1
				WHERE TRIM(sap_poslocation_id) = $key
			");
		}
		
		if ($response) $_CONSOLE[] = "Set exchange record $key as imported";
		else $_ERRORS[] = "Error on set exchange record $key as imported";
		
	} else {
		$_ERRORS[] = "Error on import record $key";
	}
}


// ignore imports ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && $_IGNORED_POS) {

	$sth = $exchange->db->prepare("
		UPDATE sap_poslocations SET
			sap_poslocation_imported_into_mps = 1
		WHERE TRIM(sap_poslocation_id) = ?
	");

	foreach ($_IGNORED_POS as $id => $v) {
		$response = DEBUGGING_MODE ? true : $sth->execute(array($id));
	}
}

// resonding :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONDING:

if ($_SERVER['SERVER_NAME']) {
	header('Content-Type: text/json');
	echo json_encode(array(
		'console' => $_CONSOLE,
		'errors' => $_ERRORS,
		'messages' => $_MESSAGES,
		'data' => DEBUGGING_MODE ? $_SOURCES : null,
		'ignored' => DEBUGGING_MODE ? $_IGNORED_POS : null
	));
}
	
	