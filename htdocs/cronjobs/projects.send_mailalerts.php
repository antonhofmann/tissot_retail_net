<?php

/*This routine daily sends out mail different mail alerts in the context of projects*/


$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';
require_once PATH_LIBRARIES.'phpmailer/class.phpmailer.php';

/*01 Call for CMS completion
Is sent as soon as a project gets an POS opening date
The alert is sent only once
Mailtemplate 46
https://retailnet.tissot.ch/administration/mailtemplates/data/46
*/

//update due dates of all concerned projects
$protocol = Settings::init()->http_protocol.'://';
$server = $_SERVER['SERVER_NAME'];

$model = new Model('project');

$query = "SELECT 
			project_number, 
			project_state, 
			project_order, 
			project_actual_opening_date
		 FROM projects 
		 INNER join orders on order_id = project_order 
		 INNER join project_costs on project_cost_order = order_id 
         WHERE project_cost_cms_completed <> 1 
			  and (project_actual_opening_date is not null and project_actual_opening_date <> '0000-00-00') 
			  and (order_archive_date is null or order_archive_date = '0000-00-00') 
			  and (project_cost_cms_completion_due_date is null or project_cost_cms_completion_due_date = '0000-00-00') 
			  and project_state in (1, 4, 5)
		 ";

$sth = $model->db->prepare($query);
$sth->execute();
$projects = $sth->fetchAll();

foreach($projects as $key=>$project)
{
	//set the due date
	//check if project has items
	$query = "SELECT count(order_item_id) as num_recs  
			  FROM order_items
			  WHERE order_item_type in (1, 2, 3, 6, 7) 
			        and order_item_order = ?
			  ";
	
	
	$sth = $model->db->prepare($query);
	$sth->execute(array($project["project_order"]));
	$result = $sth->fetch();
	
	if(count($result) > 0) 
	{
		//due date 3 months from POS opening date
		$pos_opening_date = "'" . $row_p["project_actual_opening_date"] . "'";
		$due_date = date('Y-m-d', strtotime("+3 months", strtotime($pos_opening_date)));

		$sql_u = "UPDATE project_costs 
				  SET project_cost_cms_completion_due_date = ?
				  WHERE project_cost_order = ?";
		
		$sth = $model->db->prepare($sql_u);
		$sth->execute(array($pos_opening_date, $project["project_order"]));

		//echo '<pre>';
		//print_r($sth->errorInfo());
	}
}

// ask for data and documents from client needed for CMS completen
// send mails after POS opening date is entered to client
$mail_template_id = 46;
$query = "SELECT
            project_id, order_id, 
			DATE_FORMAT(project_cost_cms_completion_due_date,'%d.%m.%y') as due_date,
			order_retail_operator
		 FROM projects 
		 INNER join orders on order_id = project_order 
		 INNER join project_costs on project_cost_order = order_id 
         WHERE project_cost_cms_completed <> 1
			  and project_cost_cms_completion_due_date < ? 
			  and (project_actual_opening_date is not null and project_actual_opening_date <> '0000-00-00') 
			  and (order_archive_date is null or order_archive_date = '0000-00-00') 
			  and project_cost_cms_completion_due_date is not null
			  and project_cost_cms_completion_due_date <> '0000-00-00'
			  and project_state in (1, 4, 5)
		 ";

$sth = $model->db->prepare($query);
$sth->execute(array(date("Y-m-d")));
$projects = $sth->fetchAll();


foreach($projects as $key=>$project)
{
	//check if mail was already sent
	$query = "SELECT count(mail_tracking_id) as num_recs  
			  FROM mail_trackings
			  WHERE mail_tracking_mail_template_id = ? 
					and mail_tracking_recipient_user_id = ?
			  ";
	$sth = $model->db->prepare($query);
	$sth->execute(array($mail_template_id, $project["order_retail_operator"]));
	$result = $sth->fetch();

	if($result['num_recs'] == 0) 
	{
		$project_data = new Project();
		$project_data->read($project["project_id"]);


		$actionmail = new ActionMail($mail_template_id);

		$actionmail->addRecipient($project["order_retail_operator"]);

		$actionmail->setDataloader(array(
			'order_number' => $project_data->order()->number,
			'order_shop_address_country_name' => $project_data->order()->shop_address_country_name,
			'address_company' => $project_data->order()->shop_address_company,
			'date'=>$project["due_date"],
			'link' => $protocol.$server."/user/project_costs_real_costs.php?pid=". $project["project_id"]
		));

		$actionmail->send();

		$_CONSOLE = $actionmail->getConsole();
		$totalSendMails = $actionmail->getTotalSentMails();

		if($actionmail->isSuccess())
		{
			$query = "SELECT mail_tracking_sender_user_id, mail_tracking_recipient_user_id , mail_tracking_content 
					  FROM mail_trackings
					  WHERE mail_tracking_mail_template_id = ? 
							and mail_tracking_recipient_user_id = ?
					  ";
				$sth = $model->db->prepare($query);
				$sth->execute(array($mail_template_id, $project["order_retail_operator"]));
				$result = $sth->fetch();
				
				//insert mail into order_mails
				if(count($result) >0) 
				{
					
					$mail_text = str_replace("<br />", "\n", $result["mail_tracking_content"]);
					$mail_text = str_replace("\n\n", "\n", $mail_text);
					$mail_text = strip_tags($mail_text);
					$mail_text = str_replace("body {font-family: arial, helvetica, sans-serif}", "", $mail_text);


					//get cc user
					$cc_user = array();
					$deputy_user = array();;
					$recipient = new User($project["order_retail_operator"]);
					if($recipient->data["user_email_cc"])
					{
						$query = "SELECT user_id 
								  FROM users
								  WHERE user_active = 1 and user_email = ?
								  ";
						$sth = $model->db->prepare($query);
						$sth->execute(array($recipient->data["user_email_cc"]));
						$cc_user = $sth->fetch();
					}

					if($recipient->data["user_email_deputy"])
					{
						$query = "SELECT user_id 
								  FROM users
								  WHERE user_active = 1 and user_email = ?
								  ";
						$sth = $model->db->prepare($query);
						$sth->execute(array($recipient->data["user_email_deputy"]));
						$deputy_user = $sth->fetch();
					}

							
					
					
					//insert mail into order_mails for recipient and his cc/deputy
					$sth = $model->db->prepare("
								INSERT INTO order_mails (
									order_mail_order,
									order_mail_user,
									order_mail_from_user,
									order_mail_text,
									date_created,
									user_created
								) VALUES (
									:order, :recipient, :sender, :text, :date_created, :user
								)
							");
								
							
					$action = $sth->execute(array(
						'order' => $project["order_id"],
						'recipient' => $result["mail_tracking_recipient_user_id"],
						'sender' => $result["mail_tracking_sender_user_id"],
						'text' => $mail_text,
						'date_created' => date("Y-m-d H:i:s"),
						'user' => 'cronjob'
					));


					if(count($cc_user) > 0 and array_key_exists('user_id', $cc_user) and  $cc_user["user_id"] > 0)
					{
						$sth = $model->db->prepare("
									INSERT INTO order_mails (
										order_mail_order,
										order_mail_user,
										order_mail_from_user,
										order_mail_text,
										order_mail_is_cc, 
										date_created,
										user_created
									) VALUES (
										:order, :recipient, :sender, :text, :is_cc, :date_created, :user
									)
								");
									
								
						$action = $sth->execute(array(
							'order' => $project["order_id"],
							'recipient' => $cc_user["user_id"],
							'sender' => $result["mail_tracking_sender_user_id"],
							'text' => $mail_text,
							'is_cc' => 1, 
							'date_created' => date("Y-m-d H:i:s"),
							'user' => 'cronjob'
						));
					}


					if(count($deputy_user) > 0 and array_key_exists('user_id', $deputy_user) and  $deputy_user["user_id"] > 0)
					{
						$sth = $model->db->prepare("
									INSERT INTO order_mails (
										order_mail_order,
										order_mail_user,
										order_mail_from_user,
										order_mail_text,
										order_mail_is_cc, 
										date_created,
										user_created
									) VALUES (
										:order, :recipient, :sender, :text, :is_cc, :date_created, :user
									)
								");
									
								
						$action = $sth->execute(array(
							'order' => $project["order_id"],
							'recipient' => $deputy_user["user_id"],
							'sender' => $result["mail_tracking_sender_user_id"],
							'text' => $mail_text,
							'is_cc' => 1, 
							'date_created' => date("Y-m-d H:i:s"),
							'user' => 'cronjob'
						));
					}
				}
		}
		unset($actionmail);
		unset($project_data);
		unset($recipient);
		unset($cc_user);
		unset($deputy_user);
		
		
	}
}


