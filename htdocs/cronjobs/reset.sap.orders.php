<?php

$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';

$application = $_REQUEST['application'];

$_ERRORS = array();
$_MESSAGES = array();
$_DATA = array();

$_START_DATE = '2014-10-01';

// shell scripting
if (!$application) {
	Connector::instance();
}

// exchange server
$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);

// connector identificators
$_CONNECTORS = array(
	'08' => new Model(Connector::DB_RETAILNET_MPS)
);

// application identificators
$_APPLICATIONS = array(
	'mps' => '08'
);


// sap not confirmed items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEMS = array();
$_PONS = array();

if (!$_ERRORS) {

	foreach ($_APPLICATIONS as $app => $division) {
		
		$model = $_CONNECTORS[$division];

		$sth = $model->db->prepare("
			SELECT 
				mps_ordersheet_item_delivered.mps_ordersheet_item_delivered_id AS distribution,
				mps_ordersheet_items.mps_ordersheet_item_id AS item, 
				mps_ordersheet_item_delivered.mps_ordersheet_item_delivered_sap_purchase_order_number AS pon,
				mps_ordersheet_item_delivered.mps_ordersheet_item_delivered_confirmed AS confirmed_date,
				sap_confirmed_items.sap_confirmed_item_id AS confirmed
			FROM mps_ordersheet_item_delivered 
			INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_delivered.mps_ordersheet_item_delivered_ordersheet_item_id = mps_ordersheet_items.mps_ordersheet_item_id
			LEFT JOIN sap_confirmed_items ON mps_ordersheet_item_delivered.mps_ordersheet_item_delivered_id = sap_confirmed_items.sap_confirmed_item_mps_distribution_id
			WHERE mps_ordersheet_item_delivered_confirmed IS NOT NULL 
			AND mps_ordersheet_item_delivered_sap_purchase_order_number IS NOT NULL 
			AND mps_ordersheet_item_delivered_confirmed >= ?
		");

		$sth->execute(array($_START_DATE));
		$result = $sth->fetchAll();

		if ($result) {

			foreach ($result as $row) {
				
				$id = $row['distribution'];

				if (!$row['confirmed']) {

					$_PONS[] = $row['pon'];
					
					$_ITEMS[$division][$id] = array(
						'item' => $row['item'],
						'pon' => $row['pon'],
					);
				}
			}
		}
	}

	$_PONS = array_unique(array_filter($_PONS));
}


// update sales orders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


if (!$_ERRORS && $_PONS) {

	$pon = "'".join("', '", $_PONS)."'";

	$sth = $exchange->db->prepare("
		SELECT sap_salesorder_item_id AS id
		FROM sap_salesorder_items
		WHERE sap_salesorder_item_fetched_by_sap IS NOT NULL 
		AND sap_salesorder_item_fetched_by_sap >= ?
		AND sap_salesorder_item_mps_po_number IN ($pon)
	");

	$sth->execute(array($_START_DATE));
	$result = $sth->fetchAll();

	if ($result) {

		$sth = $exchange->db->prepare("
			UPDATE sap_salesorder_items SET 
				sap_salesorder_item_fetched_by_sap = NULL
			WHERE sap_salesorder_item_id = ?
		");

		foreach ($result as $row) {
			
			$id = $row['id'];
			$response = $sth->execute(array($id));

			if (!$response) $_ERRORS[] = "Item $id cannot be updated";
		}
	}
}

