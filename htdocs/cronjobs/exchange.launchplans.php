<?php 

if (!$_SERVER['DOCUMENT_ROOT']) {
	$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));
}

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';

ini_set('memory_limit', '4096M');

$application = $_REQUEST['application'];

$IMPORT_KEY = date('U');

$errors = 0;

define('MODE_DEBUGGING', false);

$errors = array();
$messages = array();
$console = array();

// php comand lien, cronjob runtimes
if (!$application) {
	Connector::instance();
	$console['message'][] = "Cronjob Mode, set connector for live server: swatch.com";
}

// for browser request
$user = User::instance();

// connector identificators
$connectors = array(
	'08' => new Model('lps')
);

$_ORDER_LINE_TAG_CONFIRM_HEAD = 'B1';
$_ORDER_LINE_TAG_CONFIRM_ITEM = 'B2';
$_ORDER_LINE_TAG_SHIPPED_HEAD = 'S1';
$_ORDER_LINE_TAG_SHIPPED_ITEM = 'S2';

$_DIR_RAMCO = $_SERVER['DOCUMENT_ROOT']."/ramco_exchange";
$_DIR_PROCCESSD = "$_DIR_RAMCO/processed";
$_DIR_FAILURES = "$_DIR_RAMCO/failures";

$references = array();

// app dependency ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
	$queryGetOrders = "
		SELECT 
			lps_ramco_order_id AS id,
			lps_ramco_order_po_number AS pon
		FROM lps_ramco_orders
		WHERE lps_ramco_order_po_number IN (?)
	";

	$queryGetReferences = "
		SELECT
			lps_launchplan_item_week_quantity_id AS id,
			LOWER(REPLACE(lps_reference_code, ' ', '')) AS code,
			lps_launchplan_item_week_quantity_pon AS pon
		FROM lps_launchplan_items
		INNER JOIN lps_references ON lps_launchplan_item_reference_id = lps_reference_id
		INNER JOIN lps_launchplan_item_week_quantities ON lps_launchplan_item_week_quantity_item_id = lps_launchplan_item_id
		WHERE lps_launchplan_item_week_quantity_pon IN (?)
	";

	$queryInsertOrder = "
		INSERT INTO lps_ramco_orders (
			lps_ramco_order_type,
			lps_ramco_order_po_number,
			lps_ramco_order_customer_number,
			lps_ramco_order_sold_to_sapnr,
			lps_ramco_order_sales_order_number,
			lps_ramco_order_shipto_number,
			lps_ramco_order_confirmation_date,
			lps_ramco_order_payment_condition,
			lps_ramco_order_shipment_mode,
			lps_ramco_order_inco_terms,
			lps_ramco_order_delivered_by,
			lps_ramco_order_employee,
			lps_ramco_order_division,
			lps_ramco_order_order_deleted
		) VALUES (
			:type,
			:po_number,
			:customer_number,
			:sold_to_sapnr,
			:sales_order_number,
			:shipto_number,
			:confirmation_date,
			:payment_condition,
			:shipment_mode,
			:inco_terms,
			:delivered_by,
			:employee,
			:division,
			:order_deleted
		)
	";

	$queryUpdateOrder = "
		UPDATE lps_ramco_orders SET
			lps_ramco_order_type = :type,
			lps_ramco_order_po_number = :po_number,
			lps_ramco_order_customer_number = :customer_number,
			lps_ramco_order_sold_to_sapnr = :sold_to_sapnr,
			lps_ramco_order_sales_order_number = :sales_order_number,
			lps_ramco_order_shipto_number = :shipto_number,
			lps_ramco_order_confirmation_date = :confirmation_date,
			lps_ramco_order_payment_condition = :payment_condition,
			lps_ramco_order_shipment_mode = :shipment_mode,
			lps_ramco_order_inco_terms = :inco_terms,
			lps_ramco_order_delivered_by = :delivered_by,
			lps_ramco_order_employee = :employee,
			lps_ramco_order_division = :division,
			lps_ramco_order_order_deleted = :order_deleted
		WHERE lps_ramco_order_id = :id
	";

	$queryInsertConfirmation = "
		INSERT INTO lps_launchplan_item_confirmed (
			lps_launchplan_item_confirmed_week_item_id,
			lps_launchplan_item_confirmed_po_number,
			lps_launchplan_item_confirmed_line_number,
			lps_launchplan_item_confirmed_material_code,
			lps_launchplan_item_confirmed_ean_number,
			lps_launchplan_item_confirmed_quantity,
			lps_launchplan_item_confirmed_departure_date,
			lps_launchplan_item_confirmed_delivery_date,
			lps_launchplan_item_confirmed_order_state
		) VALUES (
			:week_item_id,
			:po_number,
			:line_number,
			:material_code,
			:ean_number,
			:quantity,
			:departure_date,
			:delivery_date,
			:order_state
		)
	";

	$queryInsertShippment = "
		INSERT INTO lps_launchplan_item_shipped (
			lps_launchplan_item_shipped_week_item_id,
			lps_launchplan_item_shipped_po_number,
			lps_launchplan_item_shipped_sales_order_number,
			lps_launchplan_item_shipped_line_number,
			lps_launchplan_item_shipped_material_code,
			lps_launchplan_item_shipped_ean_number,
			lps_launchplan_item_shipped_date,
			lps_launchplan_item_shipped_quantity,
			lps_launchplan_item_shipped_order_state
		) VALUES (
			:week_item_id,
			:po_number,
			:sales_order_number,
			:line_number,
			:material_code,
			:ean_number,
			:date,
			:quantity,
			:order_state
		)
	";


// get confirmed orders ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


$orders = array();
$confirmations = array();
$confirmedPons = array();
$involvedConfirmedFiles = array();
$importedConfirmedOrders = array();

$files = glob("$_DIR_RAMCO/lps.confirmed.*.{txt}", GLOB_BRACE);

if ($files) {

	foreach ($files as $file) {
		
		$lines = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

		if ($lines) {

			foreach ($lines as $i => $line) {
				
				$line = trim($line);
				$tag = trim(substr($line, 0, 2));

				// order header
				if ($tag == $_ORDER_LINE_TAG_CONFIRM_HEAD) {

					$pon = trim(substr($line, 5, 18));
					$division = trim(substr($line, 269, 3));
					$nextTag = $_ORDER_LINE_TAG_CONFIRM_ITEM;

					// involved pon's
					$confirmedPons[$division][] = $pon;
					$involvedConfirmedFiles[$file][] = $pon;

					$orders[$division][$pon] = array(
						'type' => trim(substr($line, 2, 3)),
						'po_number' => $pon,
						'customer_number' => trim(substr($line, 23, 18)),
						'sold_to_sapnr' => trim(substr($line, 41, 18)),
						'sales_order_number' => trim(substr($line, 59, 18)),
						'shipto_number' => trim(substr($line, 77, 18)),
						'confirmation_date' => trim(substr($line, 95, 8)),
						'payment_condition' => trim(substr($line, 103, 40)),
						'shipment_mode' => trim(substr($line, 143, 40)),
						'inco_terms' => trim(substr($line, 183, 40)),
						'delivered_by' => trim(substr($line, 223, 40)),
						'employee' => trim(substr($line, 263, 6)),
						'division' => $division,
						'order_deleted' => trim(substr($line, 272, 1))
					);
				}

				// order details
				if ($tag == $_ORDER_LINE_TAG_CONFIRM_ITEM && $tag == $nextTag) {

					$confirmations[$division][$pon][] = array(
						'po_number' => $pon,
						'line_number' => trim(substr($line, 2, 4)),
						'material_code' => trim(substr($line, 6, 16)),
						'ean_number' => trim(substr($line, 22, 13)),
						'quantity' => trim(substr($line, 35, 15)),
						'departure_date' => trim(substr($line, 50, 8)),
						'delivery_date' => trim(substr($line, 58, 8)),
						'order_state' => trim(substr($line, 66, 2))
					);
				}
			}
		}
	}

	$console['orders'] = $orders;
	$console['confirmations'] = $confirmations;
	$console['involved.confirmation.pons'] = $confirmedPons;
	$console['involved.confirmation.files'] = $involvedConfirmedFiles;
}


// import orders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$totalInsertedOrders = 0;
$totalUpdatedOrders = 0;
$consoleInsert = array();
$consoleUpdate = array();

if (!$errors && $orders) {

	foreach ($orders as $division => $_orders) {

		$model = $connectors[$division];

		// involved division pos
		$_pons = $confirmedPons[$division] ? join(',', array_unique(array_filter($confirmedPons[$division]))) : null;

		// po numbers
		$sth = $model->db->prepare(str_replace('?', $_pons, $queryGetOrders));
		$sth->execute(array($_pons));
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$pon = $row['pon'];
				$pons[$division][$pon] = $row['id'];
			}
		}

		// crud statements
		$sthInsert = $model->db->prepare($queryInsertOrder);
		$sthUpdate = $model->db->prepare($queryUpdateOrder);

		foreach ($_orders as $pon => $data) {

			if ($pons[$division][$pon]) {

				$data['id'] = $pons[$division][$pon];
				
				// update order
				if (!MODE_DEBUGGING) {
					$response = $sthUpdate->execute($data);
					$importedOrders[] = $pon;
				} else {
					$response = true;
				}

				$consoleUpdate[$division][] = $data;
				
				$totalUpdatedOrders = $totalUpdatedOrders + $response;

				if (!$response) {
					$errors[] = "Error on update order: $pon";
				}

			} else {
				
				// inser new order
				if (!MODE_DEBUGGING) {
					$response = $sthInsert->execute($data);
					$importedOrders[] = $pon;
				} else {
					$response = true;
				}

				$consoleInsert[$division][] = $data;

				if ($response) {
					$pons[$division][$pon] = $model->db->lastInsertId();
					$totalInsertedOrders = $totalInsertedOrders + $response;
				}

				if (!$response) {
					$errors[] = "Error on insert order: $pon";
				}
			}	
		}
	}


	$console['imported.orders'] = $importedOrders; 
	$console['insert.orders'][$division] = $consoleInsert; 
	$console['update.orders'][$division] = $consoleUpdate; 

	$messages[] = "Total insert orders: $totalInsertedOrders";
	$messages[] = "Total updat orders: $totalUpdatedOrders";
}


// import confirmations ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$totalImportedConfirmedOrders = 0;
$consoleInsert = array();

if (!$errors && $confirmations) {

	foreach ($confirmations as $division => $_confirmations) {

		$model = $connectors[$division];

		// involved division pos
		$_pons = $confirmedPons[$division] ? join(',', array_unique(array_filter($confirmedPons[$division]))) : null;

		// get references
		$sth = $model->db->prepare(str_replace('?', $_pons, $queryGetReferences));
		$sth->execute();
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$pon = $row['pon'];
				$code = $row['code'];
				$references[$division][$pon][$code] = $row['id'];
			}
		}
 
 		// insert confirmation statement 
		$sth = $model->db->prepare($queryInsertConfirmation);
		
		foreach ($_confirmations as $pon => $items) {

			foreach ($items as $data) {

				// find item id
				$code = strtolower(str_replace(' ', '', $data['material_code']));
				$data['week_item_id'] = $references[$division][$pon][$code];

				// conosole insert
				$consoleInsert[$division][] = $data;

				// insert order
				$response = MODE_DEBUGGING ? true : $sth->execute($data);

				if ($response) {
					$importedConfirmedOrders[] = $pon;
				} else {
					$errors[] = "Error on insert confirmed order: $pon";
				}
				
				$totalImportedConfirmedOrders = $totalImportedConfirmedOrders + $response;

				if (!$references[$division][$pon][$code]) {
					$messages[] = "Item not found: pon $pon, code $code";
				}
			}
		}
	}

	$console['references'] = $references; 
	$console['imported.confirmations'] = $importedConfirmedOrders; 
	$messages[] = "Total insert confirmed orders: $totalImportedConfirmedOrders";
	$console['insert.confirmations'] = $consoleInsert; 
}


// get shippment :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$shippment = array();
$shippedPons = array();
$involvedShippedFiles = array();

$files = glob("$_DIR_RAMCO/lps.shipped.*.{txt}", GLOB_BRACE);

if ($files) {

	// get all lps pon numbers
	$model = $connectors['08'];
	$result = $model->query("SELECT DISTINCT lps_launchplan_item_week_quantity_pon AS pon FROM lps_launchplan_item_week_quantities")->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$pon = $row['pon'];
			$divisions[$pon] = '08';
		}
	}


	foreach ($files as $file) {
	
		$lines = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

		if ($lines) {

			foreach ($lines as $i => $line) {

				$line = trim($line);
				$tag = trim(substr($line, 0, 2));

				if ($tag == $_ORDER_LINE_TAG_SHIPPED_HEAD) {
					$pon = trim(substr($line, 5, 18));
					$son = trim(substr($line, 23, 18));
					$division = $divisions[$pon] ?: '08';
					$nextTag = $_ORDER_LINE_TAG_SHIPPED_ITEM;
					$shippedPons[$division][] = $pon;
					$involvedShippedFiles[$file][] = $pon;
				}

				// order details
				if ($tag == $_ORDER_LINE_TAG_SHIPPED_ITEM && $tag == $nextTag) {

					$shippment[$division][$pon][] = array(
						'po_number' => $pon,
						'sales_order_number' => $son,
						'line_number' => trim(substr($line, 2, 4)),
						'material_code' => trim(substr($line, 6, 16)),
						'ean_number' => trim(substr($line, 22, 13)),
						'quantity' => trim(substr($line, 35, 15)),
						'date' => trim(substr($line, 50, 8)),
						'order_state' => trim(substr($line, 58, 2))
					);
				}
			}

		}
	}

	$console['shippment'] = $shippment;
	$console['involved.shippment.pons'] = $shippedPons;
	$console['involved.shippment.files'] = $involvedShippedFiles;
}


// import shippment ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


$totalImportedShippedOrders = 0;
$consoleInsert = array();

if (!$errors && $shippment) {

	foreach ($shippment as $division => $_shippment) {

		$model = $connectors[$division];

		// involved division pos
		$_pons = $shippedPons[$division] ? join(',', array_unique(array_filter($shippedPons[$division]))) : null;

		// get references
		$sth = $model->db->prepare(str_replace('?', $_pons, $queryGetReferences));
		$sth->execute();
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$pon = $row['pon'];
				$code = $row['code'];
				$references[$division][$pon][$code] = $row['id'];
			}
		}
 
 		// insert confirmation statement 
		$sth = $model->db->prepare($queryInsertShippment);
		
		foreach ($_shippment as $pon => $items) {

			foreach ($items as $data) {

				// find item id
				$code = strtolower(str_replace(' ', '', $data['material_code']));
				$data['week_item_id'] = $references[$division][$pon][$code];

				// conosole insert
				$consoleInsert[$division][] = $data;

				$response = MODE_DEBUGGING ? true : $sth->execute($data);

				if ($response) {
					$importedShippedOrders[] = $pon;
				} else {
					$errors[] = "Error on insert shipped order: $pon";
				}
				
				$totalImportedShippedOrders = $totalImportedShippedOrders + $response;


				if (!$references[$division][$pon][$code]) {
					$messages[] = "SHIPPMENT: Item not found: pon $pon, code $code";
				}
			}
		}
	}

	$console['references'] = $references; 
	$console['imported.confirmations'] = $importedShippedOrders; 
	$console['insert.shippment'] = $consoleInsert; 
	$messages[] = "Total insert shipped orders: $totalImportedShippedOrders";
}


// cleanup confirmed files :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


if ($involvedConfirmedFiles && $importedConfirmedOrders) {

	$totalImported = 0;

	foreach ($involvedConfirmedFiles as $file => $pons) {
		
		$totalOrders = count($pons);

		foreach ($pons as $pon) {
			if (in_array($pon, $importedConfirmedOrders)) $totalImported++;
		}

		$filename = basename($file);

		if ($totalOrders == $totalImported) {
			$target = $_DIR_PROCCESSD."/$filename";
			$console['processed'][] = $target;
		} else {
			$target = $_DIR_FAILURES."/$filename";
			$console['failures'][] = $target;
		}

		if (!MODE_DEBUGGING) {

			$response = copy($file, $target);

			if ($response) {
				@chmod($file, 0666);
				@unlink($file);
			}
		}
	}
}


// cleanup shippment files :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($involvedShippedFiles && $importedShippedOrders) {

	$totalImported = 0;

	foreach ($involvedShippedFiles as $file => $pons) {
		
		$totalOrders = count($pons);

		foreach ($pons as $pon) {
			if (in_array($pon, $importedShippedOrders)) $totalImported++;
		}

		$filename = basename($file);

		if ($totalOrders == $totalImported) {
			$target = $_DIR_PROCCESSD."/$filename";
			$console['processed'][] = $target;
		} else {
			$target = $_DIR_FAILURES."/$filename";
			$console['failures'][] = $target;
		}

		if (!MODE_DEBUGGING) {

			$response = copy($file, $target);

			if ($response) {
				@chmod($file, 0666);
				@unlink($file);
			}
		}
	}
}


// update launchplan confirmed items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$confirmedQuantities = array();

if ($confirmedPons) {

	foreach ($confirmedPons as $division => $pons) {
		
		$pons = join(',', array_unique(array_filter($pons)));

		$model = $connectors[$division];

		// get confirmed orders
		$result = $model->query("
			SELECT 
				lps_launchplan_item_confirmed_id AS id, 
				lps_launchplan_item_week_quantity_item_id AS item,
				lps_launchplan_item_week_quantity_week AS week,
				lps_launchplan_item_confirmed_quantity AS quantity
			FROM lps_launchplan_item_confirmed 
			INNER JOIN lps_launchplan_item_week_quantities ON lps_launchplan_item_week_quantity_id = lps_launchplan_item_confirmed_week_item_id
			WHERE lps_launchplan_item_confirmed_order_state <> '02' AND lps_launchplan_item_confirmed_po_number IN ($pons)
			ORDER BY lps_launchplan_item_confirmed_id DESC
		")->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$item = $row['item'];
				$week = $row['week'];
				if (!$confirmedQuantities[$item][$week]) {
					$confirmedQuantities[$item][$week] = $row['quantity'];
				}
			}
		}
	
		// update launchplan confirmed items
		if ($confirmedQuantities) {

			$sth = $model->db->prepare("
				UPDATE lps_launchplan_items SET
					lps_launchplan_item_quantity_confirmed = ?,
					user_modified = 'ramco',
					date_modified = NOW()
				WHERE lps_launchplan_item_id = ?
			");

			foreach ($confirmedQuantities as $item => $quantites) {
				
				$quantity = array_sum(array_values($quantites));

				$respone = MODE_DEBUGGING ? true : $sth->execute(array($quantity, $item));

				if ($respone) {
					$messages[] = "UPDATE CONFIRMATION item $item: quantity $quantity";
				} else {
					$errors[] = "UPDATE CONFIRMATION item $item: quantity $quantity";
				}
			}
		}
	}

	$console['update.launchplan.items.confirmed'] = $confirmedQuantities;
}


// update launchplan shipped items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$shippedQuantities = array();

if ($shippedPons) {

	foreach ($shippedPons as $division => $pons) {
		
		$pons = join(',', array_unique(array_filter($pons)));

		$model = $connectors[$division];

		// get confirmed orders
		$result = $model->query("
			SELECT 
				lps_launchplan_item_shipped_id AS id, 
				lps_launchplan_item_week_quantity_item_id AS item, 
				lps_launchplan_item_week_quantity_week AS week,
				lps_launchplan_item_shipped_quantity AS quantity
			FROM lps_launchplan_item_shipped 
			INNER JOIN lps_launchplan_item_week_quantities ON lps_launchplan_item_week_quantity_id = lps_launchplan_item_shipped_week_item_id
			WHERE lps_launchplan_item_shipped_po_number IN ($pons)
		")->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$item = $row['item'];
				$week = $row['week'];
				if (!$shippedQuantities[$item][$week]) {
					$shippedQuantities[$item][$week] = $row['quantity'];
				}
			}
		}
	
		// update launchplan confirmed items
		if ($shippedQuantities) {

			$sth = $model->db->prepare("
				UPDATE lps_launchplan_items SET
					lps_launchplan_item_quantity_shipped = ?,
					user_modified = 'ramco',
					date_modified = NOW()
				WHERE lps_launchplan_item_id = ?
			");

			foreach ($shippedQuantities as $item => $quantites) {
				
				$quantity = array_sum(array_values($quantites));

				$respone = MODE_DEBUGGING ? true : $sth->execute(array($quantity, $item));

				if ($respone) {
					$messages[] = "UPDATE SHIPPMENT item $item: quantity $quantity";
				} else {
					$errors[] = "UPDATE SHIPPMENT item $item: quantity $quantity";
				}
			}
		}
	}

	$console['update.launchplan.items.shipped'] = $shippedQuantities;
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
if ($application) {
	
	$totalOrders = $totalInsertedOrders+$totalUpdatedOrders;
	
	if ($totalOrders) {
		$message = "Total imported Orders: $totalOrders<br />";
	}
	
	if ($totalConfirmedOrders) {
		$message .= "Total imported confirmed items: $totalConfirmedOrders<br />";
	}
	
	if ($totalShippedOrders) {
		$message .= "Total imported shipped items: $totalShippedOrders<br />";
	}
	
	if ($errors) {
		$message .= "Total occured errors: ".count($errors);
	}
	
	header('Content-Type: text/json');
	
	echo json_encode(array(
		'response' => true,
		'message' => $message,
		'messages' => $messages,
		'errors' => $errors,
		'console' => MODE_DEBUGGING ? $console : null
	));
}
