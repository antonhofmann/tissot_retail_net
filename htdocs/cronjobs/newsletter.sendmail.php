<?php 

$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';
require_once PATH_LIBRARIES.'phpmailer/class.phpmailer.php';

ini_set('memory_limit', '4096M');

define(DEBUGGING_MODE, false);

$_APPLICATION = $_REQUEST['application'] ?: 'news';
$_ID = $_REQUEST['id'];
$_TEST = $_REQUEST['test'];
$_TERMINATED = $_ID ? false : true;

if (!$_TERMINATED) {
	session_name('retailnet');
	session_start();
}

$_ERRORS = array();
$_SUCCESSES = array();
$_CONSOLE = array();
$_DATA = array();

// Host name
$_HOST = 'http://'.$_SERVER['SERVER_NAME'];
$_CONSOLE[] = "Host: $_HOST";

if ($_TEST) {
	$_CONSOLE[] = "TEST for email address: $_TEST";
}

$user = User::instance();

$model = new Model($_APPLICATION);


// checkin :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_ID) {
	$query = "
		SELECT 
			newsletter_id AS id,
			newsletter_number AS number,
			newsletter_title AS title,
			newsletter_image AS image,
			newsletter_text AS text,
			newsletter_files AS files,
			DATE_FORMAT(newsletter_publish_date, '%m/%d/%Y') AS date,
			newsletter_publish_state_id AS state,
			newsletter_send_user_id AS sender
		FROM newsletters
		WHERE newsletter_id = $_ID
	";
} else {
	$query = "
		SELECT 
			newsletter_id AS id,
			newsletter_number AS number,
			newsletter_title AS title,
			newsletter_image AS image,
			newsletter_text AS text,
			newsletter_files AS files,
			DATE_FORMAT(newsletter_publish_date, '%m/%d/%Y') AS date,
			newsletter_publish_state_id AS state,
			newsletter_send_user_id AS sender
		FROM newsletters
		WHERE newsletter_publish_state_id = 3 AND newsletter_send_date = CURDATE()
		ORDER BY newsletter_publish_date DESC
		LIMIT 1
	";
}

$sth = $model->db->prepare($query);
$sth->execute();
$newsletter = $sth->fetch();

$_NEWSLETTER_NUMBER = $newsletter['number'];

if (!$newsletter['id']) {
	$_ERRORS[] = "Newsletter not found";
}

if (!$_ERRORS) {

	$_ID = $newsletter['id'];
	$newsletter['image'] = $_HOST.$newsletter['image'];

	// fro command line tools
	if ($newsletter['sender']) {
		$user = new User($newsletter['sender']);
		$_CONSOLE[] = "Sender $user->email";
	}
}

$_CONSOLE[] = "Newsletter {$newsletter[id]} found";


// styles & templates ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_TEMPLATE = File::load('/applications/templates/news/newsletter/mail.template.php');

if (!$_TEMPLATE) {
	$_ERRORS[] = "Mail template not found.";
}


// newsletter title
$_STYLES['newsletter-title'] = " 
	font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
	color: #000;
	font-size: 42px;
	line-height: 56px;
	padding: 20px 0 10px 0; 
";

// newsletter text
$_STYLES['newsletter-text'] = " 
	font-size: 14px;
	font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
";

// section title
$_STYLES['section'] = " 
	background-color: #E2001A;
	padding: 5px 10px 5px 10px;
	color: #ffffff;
	text-align: right;
	font-size: 28px;
	font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif; 
";

// section title
$_STYLES['article-title'] = " 
	font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
	color: #A4A4A4;
	font-size: 24px;
	line-height: 34px;
	padding: 20px 0 10px 0; 
";

// section title
$_STYLES['article-separator'] = "  
	height: 10px;
	border-bottom: 1px dashed #a4a4a4; 
";

// teaser title
$_STYLES['teaser-title'] = "  
	font-size: 16px;
	font-weight: 500;
	font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
	padding: 0;
	margin: 0; 
";

// teaser title
$_STYLES['teaser-text'] = "  
	font-size: 14px;
	font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif; 
";

// teaser title
$_STYLES['teaser-image'] = "  
	border: 1px solid #a6a6a6;
	display: block;
";

$_STYLES['button'] = " 
	font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif; 
	color: #000; 
	font-size: 13px; 
	background-color: #a4a4a4; 
	height: 29px; 
";

$_TEMPLATES['visual'] = "
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr><td class=\"newsletter-image\"><img src=\"{image}\" width=\"750\" /></td></tr>
	</table>
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr><td style=\"{$_STYLES['newsletter-title']}\" class=\"newsletter-title\" >{title}</td></tr>
	</table>
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr><td class=\"newsletter-text\" style=\"{$_STYLES['newsletter-text']}\"  >{text}</td></tr>
	</table>
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr><td class=\"newsletter-files\">{files}</td></tr>
	</table>
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr><td height=\"20\">&nbsp;</td></tr>
	</table>
";

$_TEMPLATES['button'] = "
	<table width=\"287\” border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr><td colspan=\"5\" height=\"10\">&nbsp;</td></tr>
		<tr>
			<td width=\"9\" bgcolor=\"#a4a4a4\">&nbsp;</td>
			<td bgcolor=\"#a4a4a4\" style=\"{$_STYLES[button]}\">
				<a class=\"auto-100pc\" style=\"display: block; width: 213px; height: 29px; text-transform: uppercase; color: #fff; line-height: 29px; text-decoration: none\" href=\"{url}\" target=\"_blank\">
					{title}
				</a>
			</td>
			<td width=\"15\" bgcolor=\"#a4a4a4\">
				<a href=\"{url}\" target=\"_blank\"><img src=\"http://webr.emv2.com/swatch/template_responsive/button_shadow.png\" width=\"15\" height=\"29\" border=\"0\"></a>
			</td>
			<td width=\"30\" bgcolor=\"#a4a4a4\">
				<a href=\"{url}\" target=\"_blank\"><img src=\"http://webr.emv2.com/swatch/template_responsive/button_arrow.png\" width=\"30\" height=\"29\" border=\"0\"></a>
			</td>
			<td width=\"10\"></td>
		</tr>
	</table>
";

$_TEMPLATES['teaser'] = "
	<table id=\"article-{id}\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr><td colspan=\"3\" class=\"article-title\" style=\"{$_STYLES['article-title']}\">{article_title}</td></tr>
	</table>
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr>
			{image}
			<td valign=\"top\" class=\"teaser-text\" style=\"{$_STYLES['teaser-text']}\">{title}{text}{readmore}{files}</td>
		</tr>
	</table>
";



// newsletter files ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NEWSLETTER_FILES = array();

if (!$_ERRORS) {
	
	$sth = $model->db->prepare("
		SELECT newsletter_file_path AS path
		FROM newsletter_files
		WHERE newsletter_file_newsletter_id = ?
	");

	$sth->execute(array($_ID));
	$_NEWSLETTER_FILES = $sth->fetchAll();
}

// authors :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NEWS_AUTHORS = array();
$_USER_AUTHORS = array();

if (!$_ERRORS) {

	// get news authors
	$sth = $model->db->prepare("
		SELECT DISTINCT
			news_author_id AS id,
			CONCAT(' - ', news_author_first_name, ' ', news_author_name) AS name
		FROM news_articles
		INNER JOIN news_authors ON news_author_id = news_article_author_id
		WHERE news_article_active = 1 AND news_article_publish_state_id IN (4,5)
	");

	$sth->execute();
	$result = $sth->fetchAll();
	$_NEWS_AUTHORS = _array::extract($result);

	// get user authors
	$sth = $model->db->prepare("
		SELECT DISTINCT
			user_id AS id,
			CONCAT(' - ', user_firstname, ' ', user_name) AS name
		FROM news_articles
		INNER JOIN db_retailnet.users ON user_id = news_article_author_user_id
		WHERE news_article_active = 1 AND news_article_publish_state_id IN (4,5)
	");

	$sth->execute();
	$result = $sth->fetchAll();
	$_USER_AUTHORS = _array::extract($result);
}


// recipients :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_RECIPIENTS = array();

if (!$_ERRORS) {

	if ($_TEST) {
		$_RECIPIENTS[] = array(
			'user' => 0,
			'name' => 'Newsletter Preview',
			'mail' => $_TEST
		);
	}
	else {

		$sth = $model->db->prepare("
			SELECT DISTINCT 
				newsletter_recipient_id AS id,
				user_id  AS user,
				user_address AS company,
				CONCAT(user_firstname, ' ', user_name) AS name,
				user_email AS mail,
				(
					SELECT GROUP_CONCAT(user_role_role)
					FROM db_retailnet.user_roles
					WHERE db_retailnet.user_roles.user_role_user = db_retailnet.users.user_id
				) AS roles
			FROM newsletter_recipients
			INNER JOIN db_retailnet.users ON user_id = newsletter_recipient_user_id
			WHERE db_retailnet.users.user_active = 1 AND newsletter_recipient_newsletter_id = ? AND newsletter_recipient_selected = 1
		");

		$sth->execute(array($_ID));
		$result = $sth->fetchAll();
		$_RECIPIENTS = $result ?: $_RECIPIENTS;


		$sth = $model->db->prepare("
			SELECT DISTINCT 
				newsletter_recipient_id AS id,
				news_recipient_id  AS additional,
				CONCAT(news_recipient_firstname, ' ', news_recipient_name) AS name,
				news_recipient_email AS mail
			FROM newsletter_recipients
			INNER JOIN news_recipients ON news_recipient_id = newsletter_recipient_additional_id
			WHERE news_recipient_active = 1 AND newsletter_recipient_newsletter_id = ?  AND newsletter_recipient_selected = 1
		");

		$sth->execute(array($_ID));
		$result = $sth->fetchAll();

		if ($result) {
			$_RECIPIENTS = array_merge($_RECIPIENTS, $result);
		}
	}
}


if (!$_ERRORS && !$_RECIPIENTS) {
	$_ERRORS[] = "Newsletter hasn't recipients";
}

$_CONSOLE[] = "Total recipients: ".count($_RECIPIENTS);

// newsletter articles :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT DISTINCT
			news_article_id AS id,
			news_article_title AS title,
			DATE_FORMAT(news_article_publish_date, '%m/%d/%Y') AS date,
			news_article_featured AS featured,
			news_article_new AS new,
			news_article_contact_information AS informations,
			news_article_author_id AS author_id,
			news_article_author_user_id AS author_user_id,
			news_section_id AS sid,
			news_section_name AS section,
			news_category_id AS cid,
			news_category_name AS category,
			news_article_content_id AS tid,
			news_article_content_data AS data,
			news_article_template_file_view AS file,
			(
				SELECT GROUP_CONCAT(news_article_role_role_id)
                FROM news_article_roles
                WHERE news_article_role_article_id = news_article_id
            ) AS roles,
            (
				SELECT GROUP_CONCAT(news_article_address_address_id)
                FROM news_article_addresses
                WHERE news_article_address_article_id = news_article_id
            ) AS companies,
			(
				SELECT COUNT(news_article_content_id)
				FROM news_article_contents
				WHERE news_article_content_article_id = news_article_id AND news_article_content_template_id != 1
			) AS totalTemplates
		FROM newsletter_articles
		INNER JOIN news_articles ON news_article_id = newsletter_article_article_id
		INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
		INNER JOIN news_article_templates ON news_article_content_template_id = news_article_template_id
		INNER JOIN news_categories ON news_article_category_id = news_category_id
		INNER JOIN news_sections ON news_section_id = news_category_section_id
		WHERE news_article_content_template_id = 1 
			AND news_article_active = 1
			AND IF(news_article_expiry_date,  CURRENT_DATE <= news_article_expiry_date, 1) = 1
			AND news_article_publish_state_id > 3
			AND newsletter_article_newsletter_id = ?
		ORDER BY 
			news_section_order, 
			news_section_name, 
			newsletter_article_sort,
			news_article_publish_date DESC, 
			news_article_title
	");

	$sth->execute(array($_ID));
	$articles = $sth->fetchAll();
}

if (!$_ERRORS && !$articles) {
	$_ERRORS[] = "Newsletter hasn't articles";
}

$_CONSOLE[] = "Total articles: ".count($articles);


// article content for additiona reciopients :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ARTICLE_CONTENTS = array();
$_ARTICLE_CONTENT_TEMPLATES = array();

if (!$_ERRORS) {

	// standard content
	$_ARTICLE_CONTENT_TEMPLATES[2] = "
		<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			<tr><td height=\"15\" >&nbsp;</td></tr>
			<tr><td class=\"teaser-title\" style=\"{$_STYLES['teaser-title']}\" >{title}</td></tr>
			<tr><td class=\"teaser-text\" style=\"{$_STYLES['teaser-text']}\" >{text}</td></tr>
		</table>
	";

	// pictures
	$_ARTICLE_CONTENT_TEMPLATES[3] = "
		<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			<tr><td height=\"15\" >&nbsp;</td></tr>
			<tr><td class=\"teaser-title\" style=\"{$_STYLES['teaser-title']}\" >{title}</td></tr>
			<tr><td class=\"article-template-pictures\" >{files}</td></tr>
		</table>
	";

	// box left picture
	$_ARTICLE_CONTENT_TEMPLATES[4] = "
		<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			<tr><td colspan=\"3\" height=\"15\" >&nbsp;</td></tr>
			<tr>
				<td width=\"320\" height=\"240\" valign=\"top\">
					<table width=\"320\" cellspacing=\"0\" cellpadding=\"0\">
						<tr><td><span  class=\"teaser-image\" style=\"{$_STYLES['teaser-image']}\"><img src=\"{image}\" /></span></td></tr>
					</table>
				</td>
				<td width=\"15\">&nbsp;</td>
				<td valign=\"top\" class=\"teaser-text\" style=\"{$_STYLES['teaser-text']}\" >
					{title}
					{text}
					{files}
				</td>
			</tr>
		</table>
	";

	$sth = $model->db->prepare("
		SELECT DISTINCT
			news_article_content_id AS id,
			news_article_content_article_id AS article,
			newsletter_recipient_additional_id AS recipient,
			news_article_content_data AS data,
			news_article_content_template_id AS template
		FROM newsletters
		INNER JOIN newsletter_recipients ON newsletter_recipient_newsletter_id = newsletter_id
		INNER JOIN newsletter_articles ON newsletter_article_newsletter_id = newsletter_id
		INNER JOIN news_articles ON news_article_id = newsletter_article_article_id
		INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
		WHERE newsletter_recipient_additional_id > 0
			AND news_article_content_template_id != 1 
			AND news_article_active = 1
			AND IF(news_article_expiry_date,  CURRENT_DATE <= news_article_expiry_date, 1) = 1
			AND news_article_publish_state_id > 3
			AND newsletter_article_newsletter_id = ?
		ORDER BY 
			newsletter_article_sort, 
			news_article_content_article_id, 
			news_article_publish_date DESC,
			news_article_content_order
	");

	$sth->execute(array($_ID));
	$result = $sth->fetchAll();

	if ($result) {

		foreach ($result as $row) {
			
			$id = $row['id'];
			$article = $row['article'];
			$recipient = $row['recipient'];
			$template = $row['template'];
		
			if ($_ARTICLE_CONTENT_TEMPLATES[$template]) {

				$files = null;
				
				$data = $row['data'] ? unserialize($row['data']) : array();
				$data['id'] = $id;
				$data['article'] = $article;	

				if ($data['files']) {
					
					if ($template==3) {
					
						// pictures render
						foreach ($data['files'] as $file) {
							
							$hash = Encryption::url($file);
							$file = $_HOST."/download/get/$hash";
							
							$files .= "
								<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
									<tr><td class=\"article-template-picture\" ><img width=\"750\" src=\"$file\" class=\"img-responsive\” /></td></tr>
									<tr><td height=\"15\" >&nbsp;</td></tr>
								</table>
							";
						}

						$data['files'] = $files;

					} else {

						// file button
						foreach ($data['files'] as $file) {
							
							$extension = strtoupper(pathinfo($file, PATHINFO_EXTENSION));
							$hash = Encryption::url($file);
							
							$search = array('{title}', '{url}');
							$replace = array('DOWNLOAD '.$extension, $_HOST."/download/news/$hash/$_ID"); 
							$files .= str_replace($search, $replace, $_TEMPLATES['button']);
						}
						
						$data['files'] = $files;
					}
				}

				if ($data['image']) {
					$hash = Encryption::url($data['image']);
					$data['image'] = $_HOST."/download/get/$hash";
				}

				$_ARTICLE_CONTENTS[$article][$recipient] .= Content::render($_ARTICLE_CONTENT_TEMPLATES[$template], $data, true);
			} 
		}
	}
}


// articles dataloader :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ARTICLES = array();

if (!$_ERRORS && $articles) {

	foreach ($articles as $row) {

		$id = $row['id'];
		$sid = $row['sid'];
		// section name
		$_ARTICLES[$sid]['section'] = $row['section'];

		// article role restrictions
		if ($row['roles']) {
			$_ARTICLES[$sid]['articles'][$id]['roles'] = explode(',', $row['roles']);
		}

		// article company restrictions
		if ($row['companies']) {
			$_ARTICLES[$sid]['articles'][$id]['companies'] = explode(',', $row['companies']);
		}

		// teaser data
		$data = $row['data'] ? unserialize($row['data']) : array();

		// article data
		$data['id'] = $row['id'];
		$data['date'] = $row['date'] ? 'Date: '.$row['date'] : null;
		$data['informations'] = $row['informations'];
		$data['article_title'] = $row['title'];

		// teaser image
		if ($data['image']) {

			$hash = Encryption::url($data['image']);
			$image = $_HOST."/download/get/$hash";

			$data['image'] = "
				<td width=\"320\" height=\"240\" valign=\"top\" >
					<table width=\"320\" cellspacing=\"0\" cellpadding=\"0\">
						<tr>
							<td valign=\"top\">
								<span class=\"teaser-image\" style=\"{$_STYLES['teaser-image']}\">
									<a href=\"$_HOST/news/#article-$id\"><img src=\"$image\" /></a>
								</span>
							</td>
						</tr>
					</table>
				</td>
				<td width=\"15\">&nbsp;</td>
			";
		}

		// teaser image
		//$data['image'] = $data['image'] ? $_HOST.$data['image'] : $_HOST."/public/images/camera.png";

		// teaser title
		if ($data['title']) {

			$title = $data['title'];

			$data['title'] = "
				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr><td class=\"teaser-title\" style=\"{$_STYLES['teaser-title']}\" >$title</td></tr>
					<tr><td height=\"15\" >&nbsp;</td></tr>
				</table>
			";
		}

		// teaser text
		if ($data['text']) {

			$text = $data['text'];

			$data['text'] = "
				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr><td class=\"teaser-text\" style=\"{$_STYLES['teaser-text']}\" >$text</td></tr>
					<tr><td height=\"15\" >&nbsp;</td></tr>
				</table>
			";
		}

		// teaser files
		if ($data['files']) {
			$teaserFiles = null;
			foreach ($data['files'] as $file) {
				$extension = strtoupper(pathinfo($file, PATHINFO_EXTENSION));
				$hash = Encryption::url($file);
				$search = array('{title}', '{url}');
				$replace = array('DOWNLOAD '.$extension, $_HOST."/download/news/$hash/$_ID"); 
				$teaserFiles .= str_replace($search, $replace, $_TEMPLATES['button']);
			}
			$data['files'] = $teaserFiles;
		}

		// news authors
		if ($row['author_id']) {
			$key = $row['author_id'];
			$data['author'] = $_NEWS_AUTHORS[$key];
		}

		// retailnet authors
		if ($row['author_user_id']) {
			$key = $row['author_user_id'];
			$data['author'] = $_USER_AUTHORS[$key];
		}

		// teaser parser
		$_ARTICLES[$sid]['articles'][$id]['teaser'] =  Content::render($_TEMPLATES['teaser'], $data);
	}
}
	

// newsletter visual :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


if (!$_ERRORS) {
				
	if ($_NEWSLETTER_FILES) {

		$visualFiles = null;

		foreach ($_NEWSLETTER_FILES as $file) {
			
			$extension = strtoupper(pathinfo($file['path'], PATHINFO_EXTENSION));
			$hash = Encryption::url($file['path']);
			$search = array('{title}', '{url}');
			$replace = array('DOWNLOAD '.$extension, $_HOST."/download/news/$hash/$_ID"); 
			$visualFiles .= str_replace($search, $replace, $_TEMPLATES['button']);
		}
	} 

	$newsletter['files'] = $visualFiles;

	if ($newsletter['image']) {
		$hash = Encryption::url($newsletter['image']);
		$newsletter['image'] = $_HOST."/download/get/$hash";
	}

	$_VISUAL = Content::render($_TEMPLATES['visual'], $newsletter, true);

	/*
	$gazette = "/data/news/newsletters/$_ID/gazette.png";

	if (!file_exists($_SERVER['DOCUMENT_ROOT'].$gazette)) {
		
		$id = 120;
		$width = 140 + strlen($_ID)*16;
		$height = 40;
		$font = $_SERVER['DOCUMENT_ROOT'].'/public/themes/swatch/fonts/Cookie-Regular.ttf';
		$text = "Gazette #$_ID";

		$img = imagecreatetruecolor($width, $height);
		$white = imagecolorallocate($img, 255, 255, 255);
		$grey = imagecolorallocate($img, 128, 128, 128);
		$black = imagecolorallocate($img, 0, 0, 0);

		imagefilledrectangle($img, 0, 0, $width, $height, $white);
		imagettftext($img, 30, 0, 10, 30, $black, $font, $text);
		imagepng($img, $_SERVER['DOCUMENT_ROOT'].$gazette);
		imagedestroy($img);
	}
	*/
}


// newsletter builder ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NEWSLETETRS = array();
$_SENT_ARTICLES = array();

if (!$_ERRORS) {

	foreach ($_RECIPIENTS as $recipient) {

		$_CONTENT = null;

		// recipient roles
		$recipient['roles'] = $recipient['roles'] ? explode(',', $recipient['roles']) : array();

		foreach ($_ARTICLES as $sid => $row) {

			$articles = null;

			foreach ($row['articles'] as $id => $article) {
				
				$access = true;

				// article  role restriction
				if ($recipient['user'] && !$_TEST) {
					
					if ($article['roles']) {
						$permittedRoles = array_intersect($article['roles'], $recipient['roles']);
						$access = count($permittedRoles) > 0 ? true : false;
					}

					// article company restriction
					if ($article['companies']) {
						$access = in_array($recipient['company'], $article['companies'])  ? true : false;
					}
				}

				if ($access) {

					$_SENT_ARTICLES[] = $id;

					// retailnet recipient
					if ($recipient['user'] || $_TEST) {
						
						// read more
						$search = array('{title}', '{url}');
						$replace = array('READ MORE', $_HOST."/news/#article-$id");
						$readMore = str_replace($search, $replace, $_TEMPLATES['button']);

						$article['teaser'] = Content::render($article['teaser'], array(
							'readmore' => $readMore
						), true);

					} elseif ($recipient['additional']) { 

						$article['teaser'] = Content::render($article['teaser'], array(), true);
						
						if ($_ARTICLE_CONTENTS[$id]) { 
							$additional = $recipient['additional'];
							$articleContent = $_ARTICLE_CONTENTS[$id][$additional];
							$article['teaser'] = $article['teaser'].$articleContent;
						}
					}

					$articles .= $article['teaser'];
					$articles .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tbody>";
					$articles .= "<tr><td class=\"article-separator\" style=\"{$_STYLES['article-separator']}\" >&nbsp;</td></tr>";
					$articles .= "</tbody></table>";
				}
			}

			if ($articles) {
				
				// section content
				$_CONTENT .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tbody>";
				$_CONTENT .= "<tr><td class=\"section\" style=\"{$_STYLES[section]}\" >{$row[section]}</td></tr>";
				$_CONTENT .= "<tr><td height=\"10\" >&nbsp;</td></tr>";
				$_CONTENT .= "<tr><td class=\"section-articles\">$articles</td></tr>";
				$_CONTENT .= "</tbody></table>";

				// section separator
				$_CONTENT .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
				$_CONTENT .= "<tr><td height=\"20\">&nbsp;</td></tr>";
				$_CONTENT .= "</table>";
			}
		}

		$content = Content::render($_TEMPLATE, array(
			'id' => $_ID,
			'number' => $_NEWSLETTER_NUMBER,
			'visual' => $_VISUAL,
			'content' => $_CONTENT
		), true);

		$_NEWSLETETRS[] = array(
			'recipient' => $recipient['name'],
			'email' => $recipient['mail'],
			'content' => $content
		);
	}
}

$_CONSOLE[] = "Total newsletters: ".count($_NEWSLETETRS);


// sendmail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_SENDMAIL_RECIPIENTS = array();

if (!$_ERRORS && $_NEWSLETETRS) {

	$senderName = "$user->firstname $user->name";
	$senderMail = $user->email;

	$tracker = $model->db->prepare("
		INSERT INTO newsletter_mail_trackers (
			newsletter_mail_tracker_newsletter_id,
			newsletter_mail_tracker_recipient_name,
			newsletter_mail_tracker_recipient_email,
			newsletter_mail_tracker_sender_name,
			newsletter_mail_tracker_sender_email
		)
		VALUES (?,?,?,?,?)
	");

	foreach ($_NEWSLETETRS as $i => $newsletter) {

		//echo $newsletter['content']; die;
		
		// recipient email
		$recipientMail = $newsletter['email'];
		
		if (!in_array($recipientMail, $_SENDMAIL_RECIPIENTS)) {
			

			$mail = new PHPMailer();
			$mail->CharSet = 'UTF-8';

			// mail subject
			$subject = "Swatch Gazette No. $_NEWSLETTER_NUMBER ";
			$mail->Subject = $subject;

			// mail content
			$body = $newsletter['content']; 

			// track opened mail
			$body .= "<img src='$_HOST/download/newslettertrack/$_ID/$user->id/6' width='1px' height='1px' >"; 

			$mail->AltBody = strip_tags($content);
			$mail->Body = $body;
			$mail->MsgHTML($body);
			$mail->IsHTML(true);

			// sender
			$mail->SetFrom($senderMail, $senderName);
			
			// recipient
			$recipientName = $newsletter['recipient'];
			$mail->AddAddress($recipientMail, $recipientName);

			// send
			$send = DEBUGGING_MODE ? true : $mail->Send();

			if ($send) {
				$_SENDMAIL_RECIPIENTS[] = $recipientMail;
			}

			if (!DEBUGGING_MODE && $send) {
				$tracker->execute(array(
					$_ID,
					$recipientName,
					$recipientMail,
					$senderName,
					$senderMail
				));
			}
		}
	}

	$_NEWSLETTER_SENT = true;
}


// set newsletter articles send date :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && !DEBUGGING_MODE && $_SENT_ARTICLES) {

	$_SENT_ARTICLES = array_unique($_SENT_ARTICLES);

	$sth = $model->db->prepare("
		UPDATE newsletter_articles SET 
			newsletter_article_send_date = NOW()
		WHERE newsletter_article_newsletter_id = ? AND newsletter_article_article_id = ?
	");

	foreach ($_SENT_ARTICLES as $article) {
		$sth->execute(array($_ID, $article));
	}
}


// reset newsletter recipients :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && !DEBUGGING_MODE && $_RECIPIENTS) {

	$sth = $model->db->prepare("
		UPDATE newsletter_recipients SET 
			newsletter_recipient_sent_date = NOW(),
			newsletter_recipient_selected = NULL
		WHERE newsletter_recipient_id = ?
	");

	foreach ($_RECIPIENTS as $recipient) {
		$sth->execute(array($recipient['id']));
	}
}


// ste newsletter sent date ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && !DEBUGGING_MODE) {

	$sth = $model->db->prepare("
		UPDATE newsletters SET 	
			newsletter_send_date = NULL,
			newsletter_sent_date = CURDATE()
		WHERE newsletter_id = ?
	");

	$sth->execute(array($_ID));
}

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_TERMINATED) {

	if (!DEBUGGING_MODE) {

		if ($_ERRORS) {
			foreach ($_ERRORS as $text) {
				$_NOTIFICATIONS[] = array(
					'type'=>'error', 
					'title'=>'Error', 
					'text'=>$text
				);
			}
		} else {

			$total = count($_NEWSLETETRS);

			if (!$_TEST) {
				$reload = true;
				Message::success("Total sent $total newsletter(s)");
			}
		}

		$_JSON = array(
			'success' => $_NEWSLETTER_SENT,
			'reload' => $reload
		);

		if ($_NEWSLETTER_SENT && $_TEST) {
			$_JSON['message'] = "Total sent $total newsletter(s)";
		}

	} else {

		$_JSON = array(
			'newsletter' => $_ID,
			'success' => $_NEWSLETTER_SENT,
			'errors' => $_ERRORS,
			'console' => $_CONSOLE,
			'recipients' => $_RECIPIENTS
		);
	}

	header('Content-Type: text/json');
	echo json_encode($_JSON);
}

