<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';

ini_set('memory_limit', '5120M');
ini_set('max_execution_time', 7200);
set_time_limit(7200);

// periodic mails
//include 'periodic.mails.php';

// cost monitoring sheet mails
include 'cms.mail.php';

// reset ramco failures
//include 'reset.ramco.php';

// import ramco orders
//include 'exchange.ordersheet.items.php';


// send new sap pos data alerts 
//include 'sap.pos.alerts.php';

// terminated newsletters
//include 'newsletter.terminated.php';

echo "Tissot Retail Net: Daily cronjob sucessfully executed.";

?>