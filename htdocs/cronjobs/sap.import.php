<?php 

$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';

ini_set('memory_limit', '8096M');

$application = $_REQUEST['application'];

$errors = 0;

// shell scripting
if (!$application) {
	Connector::instance();
}

// exchange server
$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);

// connector identificators
$connectors = array(
	'08' => Connector::DB_RETAILNET_MPS
);

// application identificators
$applications = array(
	'mps' => '08'
);

// SAP confirmet items
$confirmedItems = $exchange->query("
	SELECT 
		sap_confirmed_item_id,
		TRIM(sap_confirmed_item_action) AS sap_confirmed_item_action,
		TRIM(sap_confirmed_item_order_type) AS sap_confirmed_item_order_type,
		TRIM(sap_confirmed_item_sales_organization) AS sap_confirmed_item_sales_organization,
		TRIM(sap_confirmed_item_distributed_by) AS sap_confirmed_item_distributed_by,
		TRIM(sap_confirmed_item_division) AS sap_confirmed_item_division,
		TRIM(sap_confirmed_item_sold_to) AS sap_confirmed_item_sold_to,
		TRIM(sap_confirmed_item_ship_to) AS sap_confirmed_item_ship_to,
		TRIM(sap_confirmed_item_sap_order_number) AS sap_confirmed_item_sap_order_number,
		TRIM(sap_confirmed_item_sap_order_line_number) AS sap_confirmed_item_sap_order_line_number,
		TRIM(sap_confirmed_item_mps_po_number) AS sap_confirmed_item_mps_po_number,
		TRIM(sap_confirmed_item_ean_number) AS sap_confirmed_item_ean_number,
		TRIM(sap_confirmed_item_quantity) AS sap_confirmed_item_quantity,
		TRIM(sap_confirmed_item_confirmed_quantity) AS sap_confirmed_item_confirmed_quantity,
		TRIM(sap_confirmed_item_mps_po_line_number) AS sap_confirmed_item_mps_po_line_number,
		TRIM(sap_confirmed_item_date) AS sap_confirmed_item_date
	FROM sap_confirmed_items
	WHERE 
		sap_confirmed_item_imported_in_mps IS NULL 
		OR sap_confirmed_item_imported_in_mps = ''
		OR sap_confirmed_item_imported_in_mps = 0
")->fetchAll();


// SAP shipped items
$shippedItems = $exchange->query("
	SELECT 
		sap_shipped_item_id,
		TRIM(sap_shipped_item_sap_delivery_number) AS sap_shipped_item_sap_delivery_number,
		TRIM(sap_shipped_item_date_shipped) AS sap_shipped_item_date_shipped,
		TRIM(sap_shipped_item_shipped_to) AS sap_shipped_item_shipped_to,
		TRIM(sap_shipped_item_sap_order_number) AS sap_shipped_item_sap_order_number,
		TRIM(sap_shipped_item_sap_order_line_number) AS sap_shipped_item_sap_order_line_number,
		TRIM(sap_shipped_item_mps_po_number) AS sap_shipped_item_mps_po_number,
		TRIM(sap_shipped_item_mps_po_line_number) AS sap_shipped_item_mps_po_line_number,
		TRIM(sap_shipped_item_ean_number) AS sap_shipped_item_ean_number,
		TRIM(sap_shipped_item_quantity) AS sap_shipped_item_quantity
	FROM sap_shipped_items
	WHERE 
		sap_shipped_item_imported_in_mps IS NULL 
		OR sap_shipped_item_imported_in_mps = ''
		OR sap_shipped_item_imported_in_mps = 0
")->fetchAll();


if ($confirmedItems || $shippedItems) {
	
	$console['message'][] = "Start Import";
	
	$pon = array();
	
	// set all confirmed dependencies
	if ($confirmedItems)  {
		
		$console['data']['confirmed'] = $confirmedOrders;
		
		foreach ($confirmedItems as $row) {
			if ($row['sap_confirmed_item_mps_po_number']) {
				$pon[] = str_replace("'", '', $row['sap_confirmed_item_mps_po_number']);
			}
		}
	}
	
	// set all shipped dependencies
	if ($shippedItems)  {
		
		$console['data']['shipped'] = $shippedItems;
		
		foreach ($shippedItems as $row) {
			if ($row['sap_shipped_item_mps_po_number']) {
				$pon[] = str_replace("'", '', $row['sap_shipped_item_mps_po_number']);
			}
		}
	}
	
	// involved pon numbers
	$pon = ($pon) ? join("','", array_filter(array_unique($pon))) : null;
	$pon = "'$pon'";
	
	$console['pon'] = $pon;
			
	// check import data
	$result = $exchange->query("
		SELECT
			TRIM(sap_salesorder_item_mps_item_id) AS id,
			TRIM(sap_salesorder_item_sales_organization) AS organization,
			TRIM(sap_salesorder_item_sold_to) AS sap,
			TRIM(sap_salesorder_item_ship_to) AS ship,
			TRIM(sap_salesorder_item_mps_po_number) AS pon,
			TRIM(sap_salesorder_item_ean_number) AS ean,
			TRIM(sap_salesorder_item_mps_po_line_number) AS line,
			TRIM(sap_salesorder_item_division) AS division
		FROM sap_salesorder_items
		WHERE TRIM(sap_salesorder_item_mps_po_number) IN ($pon)
	")->fetchAll();

	// exchange/mps references
	$references = array();
	
	// company divisions
	$divisions = array();

	if ($result) {
		
		foreach ($result as $row) {
			
			$pon = $row['pon'];
			$ean = $row['ean'];
			$sap = $row['sap'];

			$divisions[$pon] = $row['division'];
			
			$references[$pon][$ean] = $row;
		}
	}
	
	$console['divisions'] = $divisions;
	$console['references'] = $references;

	
	// import confirmed items
	if ($confirmedItems) {
		
		$console['message'][] = "Start Confirmation";
		
		$query = "
			INSERT INTO sap_confirmed_items (
				sap_confirmed_item_id,
				sap_confirmed_item_mps_distribution_id,
				sap_confirmed_item_action,
				sap_confirmed_item_order_type,
				sap_confirmed_item_sales_organization,
				sap_confirmed_item_distributed_by,
				sap_confirmed_item_division,
				sap_confirmed_item_sold_to,
				sap_confirmed_item_ship_to,
				sap_confirmed_item_sap_order_number,
				sap_confirmed_item_sap_order_line_number,
				sap_confirmed_item_mps_po_number,
				sap_confirmed_item_ean_number,
				sap_confirmed_item_quantity,
				sap_confirmed_item_confirmed_quantity,
				sap_confirmed_item_mps_po_line_number,
				sap_confirmed_item_date,					
				sap_confirmed_item_imported_in_mps,
				sap_confirmed_item_status_code
			) VALUES (
				:sap_confirmed_item_id,
				:sap_confirmed_item_mps_distribution_id,
				:sap_confirmed_item_action,
				:sap_confirmed_item_order_type,
				:sap_confirmed_item_sales_organization,
				:sap_confirmed_item_distributed_by,
				:sap_confirmed_item_division,
				:sap_confirmed_item_sold_to,
				:sap_confirmed_item_ship_to,
				:sap_confirmed_item_sap_order_number,
				:sap_confirmed_item_sap_order_line_number,
				:sap_confirmed_item_mps_po_number,
				:sap_confirmed_item_ean_number,
				:sap_confirmed_item_quantity,
				:sap_confirmed_item_confirmed_quantity,
				:sap_confirmed_item_mps_po_line_number,
				:sap_confirmed_item_date,
				NOW(),
				:sap_confirmed_item_status_code
			)	
		";
		
		// confirmed orders
		$confirmedOrders = array();
		
		// group orders by division
		foreach ($confirmedItems as $item) {
			
			$pon = $item['sap_confirmed_item_mps_po_number'];
			$division = $divisions[$pon];

			if ($division) {
				$confirmedOrders[$division][] = $item;
			}
		}
		
		// division dependet confirmed orders
		foreach ($confirmedOrders as $division => $items) {
			
			// get only application dependent orders
			if ($application) {
				$division = $applications[$application]==$division ? $division : null;
			}
		
			if ($division) {
				
				// db model
				$conn = $connectors[$division];
				$model = new Model($conn);
				
				$console['message'][] = "Start division $conn";
				
				// query statement
				$sth = $model->db->prepare($query);
				
				foreach ($items as $item) {
				
					$id = null;
					$importStatusCode = 0;
						
					// mps sap sales order
					$pon = $item['sap_confirmed_item_mps_po_number'];
					$ean = $item['sap_confirmed_item_ean_number'];
					$sap = $item['sap_confirmed_item_sold_to'];
					
					// find mps item					
					if ($references[$pon][$ean]['id']) {
						
						$order = $references[$pon][$ean];
						
						$id = $order['id'];
						
						$console['message'][] = "Item $id: start";
						
						// check pon number
						if (!$importStatusCode) {
							$importStatusCode = $order['pon']==$item['sap_confirmed_item_mps_po_number'] ? $importStatusCode : 4;
							if ($importStatusCode)  $console['message'][] = "Item $id: Missing PON number";
						}
						
						// check sap pos number
						if (!$importStatusCode) {
							$importStatusCode = $order['sap']==$item['sap_confirmed_item_sold_to'] ? $importStatusCode : 2;
							if ($importStatusCode)  $console['message'][] = "Item $id: Missing SAP number";
						}
						
						// check ean number
						if (!$importStatusCode) {
							$importStatusCode = $order['ean']==$item['sap_confirmed_item_ean_number'] ? $importStatusCode : 6;
							if ($importStatusCode) $console['message'][] = "Item $id: Missing EAN number";
						}
						
						// check sales organization
						if (!$importStatusCode) {
							$importStatusCode = $order['organization']==$item['sap_confirmed_item_sales_organization'] ? $importStatusCode : 1;
							if ($importStatusCode) $console['message'][] = "Item $id: Missing sales organization";
						}

						// check shipto number
						if (!$importStatusCode) {
							
							if ($order['ship']) {
								$importStatusCode = $order['ship']==$item['sap_confirmed_item_ship_to'] ? $importStatusCode : 3;
							} elseif ($item['sap_confirmed_item_ship_to']) {
								$importStatusCode = $item['sap_confirmed_item_sold_to']==$item['sap_confirmed_item_ship_to'] ? $importStatusCode : 3;
							}
							
							if ($importStatusCode)  $console['message'][] = "Item $id: Missing Shipto number";
						}

						// check line number
						if (!$importStatusCode) {
							$importStatusCode = $order['line']==$item['sap_confirmed_item_mps_po_line_number'] ? $importStatusCode : 5;
							if ($importStatusCode) $console['message'][] = "Item $id: Missing PON Line number";
						}
					} 
					// mps item not found
					else {
						
						$console['message'][] = "Item $pon.$ean.$sap: not found";
					}
					
					// mps distribution id
					$item['sap_confirmed_item_mps_distribution_id'] = $id;

					// set status code
					$item['sap_confirmed_item_status_code'] = $importStatusCode;
					
					/*
					// get purchase order number from export mask [pon.sap.ship]
					if ($item['sap_confirmed_item_mps_po_number']) {
						$poNumber = explode('.', $item['sap_confirmed_item_mps_po_number']);
						$item['sap_confirmed_item_mps_po_number'] = $poNumber[0];
					}
					*/
					
					// insert order
					$response = $sth->execute($item);

					
					// confirmation key
					$key = $item['sap_confirmed_item_id'];
					
					if ($response) {

						$inserted = $model->db->lastInsertId();
						
						$importConfirmedOrders[] = $inserted;
						
						$console['message'][] = "Item $key: insert success ($inserted)";
						
						$update = $exchange->db->exec("
							UPDATE sap_confirmed_items SET
								sap_confirmed_item_status_code = $importStatusCode,
								sap_confirmed_item_imported_in_mps = NOW()
							WHERE sap_confirmed_item_id = $key
						");
						
						if ($update) {
							$console['message'][] = "Item $key: update exchange success";
						} else {
							$console['message'][] = "Item $key: update exchange failure";
						}
						
					} else {
						
						$console['message'][] = "Item $key: insert failure";
					}
				}
				
				$console['message'][] = "End division $conn";
			}
		}
		
		$console['message'][] = "End Confirmation";
	}
	
	// import shipped items
	if ($shippedItems) {
		
		$console['message'][] = "Start Shippment";
		
		$query = "
			INSERT INTO sap_shipped_items (
				sap_shipped_item_id,
				sap_shipped_item_mps_distribution_id,
				sap_shipped_item_sap_delivery_number,
				sap_shipped_item_date_shipped,
				sap_shipped_item_shipped_to,
				sap_shipped_item_sap_order_number,
				sap_shipped_item_sap_order_line_number,
				sap_shipped_item_mps_po_number,
				sap_shipped_item_mps_po_line_number,
				sap_shipped_item_ean_number,
				sap_shipped_item_quantity,
				sap_shipped_item_imported_in_mps,
				sap_shipped_item_status_code
			) VALUES (
				:sap_shipped_item_id,
				:sap_shipped_item_mps_distribution_id,
				:sap_shipped_item_sap_delivery_number,
				:sap_shipped_item_date_shipped,
				:sap_shipped_item_shipped_to,
				:sap_shipped_item_sap_order_number,
				:sap_shipped_item_sap_order_line_number,
				:sap_shipped_item_mps_po_number,
				:sap_shipped_item_mps_po_line_number,
				:sap_shipped_item_ean_number,
				:sap_shipped_item_quantity,
				1,
				:sap_shipped_item_status_code
			)	
		";
		
		// shipped orders
		$shippedOrders = array();
		
		// group orders by division
		foreach ($shippedItems as $item) {
			
			$pon = $item['sap_shipped_item_mps_po_number'];
			$division = $divisions[$pon];
			
			if ($division) {
				$shippedOrders[$division][] = $item;
			}
		}
		
		
		// division dependet shipped orders
		foreach ($shippedOrders as $division => $items) {
			
			// application model
			if ($application) {
				$division = $applications[$application]==$division ? $division : null;
			}
			
			if ($division) {
				
				// db model
				$conn = $connectors[$division];
				$model = new Model($conn);
				
				// query statement
				$sth = $model->db->prepare($query);
				
				$console['message'][] = "Start division $conn";
				
				foreach ($items as $item) {
					
					$id = null;
					$importStatusCode = 0;
						
					// mps sap sales order
					$pon = $item['sap_shipped_item_mps_po_number'];
					$ean = $item['sap_shipped_item_ean_number'];
					$sap = $item['sap_shipped_item_shipped_to'];
		
					if ($references[$pon][$ean]['id']) {
						
						$order = $references[$pon][$ean];
						
						$id = $order['id'];
							
						$console['message'][] = "Item $id: start";
						
						// check pon number
						if (!$importStatusCode) {
							$importStatusCode = $order['pon']==$pon ? $importStatusCode : 4;
							if ($importStatusCode)  $console['message'][] = "Item $id: Missing PON number";
						}
						
						// check sap pos number
						if (!$importStatusCode) {
							
							if ($order['ship']) {
								$importStatusCode = $order['ship']==$item['sap_shipped_item_shipped_to'] ? $importStatusCode : 5;
							} else {
								$importStatusCode = $order['sap']==$item['sap_shipped_item_shipped_to'] ? $importStatusCode : 5;
							}
							
							if ($importStatusCode)  $console['message'][] = "Item $id: Missing SAP number";
						}
						
						// check ean number
						if (!$importStatusCode) {
							$importStatusCode = $order['ean']==$ean ? $importStatusCode : 3;
							if ($importStatusCode)  $console['message'][] = "Item $id: Missing EAN order Number";
						}
						
						// check sap pos number
						if (!$importStatusCode) {
							$importStatusCode = !$item['sap_shipped_item_sap_order_number'] ? 1 : $importStatusCode;
							$console['message'][] = "Item $id: Missing SAP order number (generated from SAP)";
						}
							
						// check line number
						if (!$importStatusCode) {
							$importStatusCode = !$item['sap_shipped_item_sap_order_line_number'] ? 2 : $importStatusCode;
							$console['message'][] = "Item $id: Missing SAP order line number (generated from SAP)";
						}
					}
					else {
						$console['message'][] = "Item $pon.$ean.$sap: not found";
					}
					
					
					// mps distribution id
					$item['sap_shipped_item_mps_distribution_id'] = $id;
					
					// set status code
					$item['sap_shipped_item_status_code'] = $importStatusCode;
					
					/*
					// purchase order number from export mask [pon.sap.ship]
					if ($item['sap_shipped_item_mps_po_number']) {
						$poNumber = explode('.', $item['sap_shipped_item_mps_po_number']);
						$item['sap_shipped_item_mps_po_number'] = $poNumber[0];
					}
					*/
					
					// insert order
					$response = $sth->execute($item);
					
					// shipped key
					$key = $item['sap_shipped_item_id'];
					
					if ($response) {
						
						$inserted = $model->db->lastInsertId();
						
						$importShippedOrders[] = $inserted;
						
						$console['message'][] = "Item $key: insert success ($inserted)";
						
						$update = $exchange->db->exec("
							UPDATE sap_shipped_items SET
								sap_shipped_item_status_code = $importStatusCode,
								sap_shipped_item_imported_in_mps = 1
							WHERE sap_shipped_item_id = $key
						");
						
						if ($update) {
							$console['message'][] = "Item $key: update exchange success";
						} else {
							$console['message'][] = "Item $key: update exchange failure";
						}
					} 
					else {
						$console['message'][] = "Item $key: insert failure";
					}
				}
				
				$console['message'][] = "End division $conn";
			}
		}
		
		$console['message'][] = "End Shippment";
	}
	
	$response = true;
	$message = "Import procces completed.<br><br>";
	$message .= $importConfirmedOrders ? "Total import for confirmed orders: ".count($importConfirmedOrders) : null;
	$message .= $importShippedOrders ? "<br>Total import for shipped orders: ".count($importShippedOrders) : null;
} 
else {
	
	$response = false;
	$message = "No data for import";
	$console['message'][] = $message;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////// RESPONDING ////////////////777///////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
if ($application) {
	
	header('Content-Type: text/json');
	echo json_encode(array(
		'response' => true,
		'message' => $message,
		'console' => $console
	));
}
