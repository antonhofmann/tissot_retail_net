<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$api_key = 'HyO6tegFdXrettX5467htgffdrDXSWAPLN33ax77';

//check access
$allowed = false;
if(array_key_exists('apikey', $_GET) and $_GET['apikey'] = $api_key)
{
	$allowed = true;
}
else {
	die;
}

$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';

$translate = Translate::instance();
$settings = Settings::init();

Connector::instance();

$db_retailnet = new Model(Connector::DB_CORE);

//get all market data from addresses
$sql = "SELECT
			*
		FROM
			addresses
		LEFT JOIN countries ON country_id = address_country
		LEFT JOIN places ON place_id = address_place_id
		Left JOIN address_types on address_type_id = address_type
		Left join client_types on client_type_id = address_client_type
		left join provinces on province_id = place_province
		WHERE
			address_id > 0
		AND address_active = 1
		AND address_type = 1
		ORDER BY
			address_company";
$result = $db_retailnet->query($sql)->fetchAll();

$markets = array();
foreach($result as $key=>$data) {
	$markets[$data['address_id']] = $data;
}


//get all pos owner data from pos addresses
$sql = "SELECT
			*
		FROM
			posaddresses
		LEFT join addresses on address_id = posaddress_franchisee_id
		LEFT JOIN countries ON country_id = address_country
		LEFT JOIN places ON place_id = address_place_id
		Left JOIN address_types on address_type_id = address_type
		Left join client_types on client_type_id = address_client_type
		left join provinces on province_id = place_province
		WHERE
			address_id > 0
		ORDER BY
			address_id";
$result = $db_retailnet->query($sql)->fetchAll();

$poswoner_companies = array();
foreach($result as $key=>$data) {
	$poswoner_companies[$data['address_id']] = $data;
}



//get all poslocation data from pos addresses
$sql = "SELECT
			*
		FROM
			posaddresses
		LEFT JOIN countries ON country_id = posaddress_country
		LEFT JOIN places ON place_id = posaddress_place_id
		Left JOIN posowner_types on posowner_type_id = posaddress_ownertype
		Left join postypes on postype_id = posaddress_store_postype
		left join possubclasses on possubclass_id = posaddress_store_subclass
		left join product_lines on product_line_id = posaddress_store_furniture
		left join productline_subclasses on productline_subclass_id = posaddress_store_furniture_subclass
		left join floors on floor_id = posaddress_store_floor
		left join provinces on province_id = place_province
		WHERE
			posaddress_id > 0
		ORDER BY
			posaddress_client_id, posaddress_franchisee_id, posaddress_name";
$result = $db_retailnet->query($sql)->fetchAll();

$poslocations = array();
foreach($result as $key=>$data) {
	$poslocations[$data['posaddress_id']] = $data;
}

//exports XML
				
$xml = "<?xml version='1.0' encoding='UTF-8'?>" . "\r\n";
$xml .= "<data>" . "\r\n";


$xml .= "\t<markets>" . "\r\n";
foreach($markets as $key=>$data) {
	$xml .= "\t\t<market id=\"" . $data['address_id'] ."\"/>" . "\r\n";
	$xml .= "\t\t\t" . '<address_company><![CDATA[' . $data['address_company'] .']]></address_company>' . "\r\n";
	$xml .= "\t\t\t" . '<address_company2><![CDATA[' . $data['address_company2'] .']]></address_company2>' . "\r\n";
	$xml .= "\t\t\t" . '<address_street><![CDATA[' . $data['address_street'] .']]></address_street>' . "\r\n";
	$xml .= "\t\t\t" . '<address_streetnumber><![CDATA[' . $data['address_streetnumber'] .']]></address_streetnumber>' . "\r\n";
	$xml .= "\t\t\t" . '<address_address2><![CDATA[' . $data['address_address2'] .']]></address_address2>' . "\r\n";
	$xml .= "\t\t\t" . '<address_zip><![CDATA[' . $data['address_zip'] .']]></address_zip>' . "\r\n";
	$xml .= "\t\t\t" . '<address_place_id><![CDATA[' . $data['address_place_id'] .']]></address_place_id>' . "\r\n";
	$xml .= "\t\t\t" . '<place_name><![CDATA[' . $data['place_name'] .']]></place_name>' . "\r\n";
	$xml .= "\t\t\t" . '<place_province_id><![CDATA[' . $data['place_province'] .']]></place_province_id>' . "\r\n";
	$xml .= "\t\t\t" . '<province_name><![CDATA[' . $data['province_canton'] .']]></province_name>' . "\r\n";
	$xml .= "\t\t\t" . '<address_country><![CDATA[' . $data['address_country'] .']]></address_country>' . "\r\n";
	$xml .= "\t\t\t" . '<country_name><![CDATA[' . $data['country_name'] .']]></country_name>' . "\r\n";
	$xml .= "\t\t\t" . '<address_phone_country><![CDATA[' . $data['address_phone_country'] .']]></address_phone_country>' . "\r\n";
	$xml .= "\t\t\t" . '<address_phone_area><![CDATA[' . $data['address_phone_area'] .']]></address_phone_area>' . "\r\n";
	$xml .= "\t\t\t" . '<address_phone_number><![CDATA[' . $data['address_phone_number'] .']]></address_phone_number>' . "\r\n";
	$xml .= "\t\t\t" . '<address_mobile_phone_country><![CDATA[' . $data['address_mobile_phone_country'] .']]></address_mobile_phone_country>' . "\r\n";
	$xml .= "\t\t\t" . '<address_mobile_phone_area><![CDATA[' . $data['address_mobile_phone_area'] .']]></address_mobile_phone_area>' . "\r\n";
	$xml .= "\t\t\t" . '<address_mobile_phone_number><![CDATA[' . $data['address_mobile_phone_number'] .']]></address_mobile_phone_number>' . "\r\n";
	$xml .= "\t\t\t" . '<address_email><![CDATA[' . $data['address_email'] .']]></address_email>' . "\r\n";
	$xml .= "\t\t\t" . '<address_website><![CDATA[' . $data['address_website'] .']]></address_website>' . "\r\n";
	$xml .= "\t\t\t" . '<address_type><![CDATA[' . $data['address_type'] .']]></address_type>' . "\r\n";
	$xml .= "\t\t\t" . '<address_type_name><![CDATA[' . $data['address_type_name'] .']]></address_type_name>' . "\r\n";
	$xml .= "\t\t\t" . '<address_client_type><![CDATA[' . $data['address_client_type'] .']]></address_client_type>' . "\r\n";
	$xml .= "\t\t\t" . '<client_type_code><![CDATA[' . $data['client_type_code'] .']]></client_type_code>' . "\r\n";
	$xml .= "\t\t\t" . '<address_contact_name><![CDATA[' . $data['address_contact_name'] .']]></address_contact_name>' . "\r\n";
	$xml .= "\t\t\t" . '<address_contact_email><![CDATA[' . $data['address_contact_email'] .']]></address_contact_email>' . "\r\n";
	$xml .= "\t\t\t" . '<address_legnr><![CDATA[' . $data['address_legnr'] .']]></address_legnr>' . "\r\n";

	$xml .= "\t\t</market>" . "\r\n";

}
$xml .= "\t</markets>" . "\r\n";



$address_id = 0;
$xml .= "\t<pos_owner_companies>" . "\r\n";
foreach($poswoner_companies as $key=>$data) {
	
	if($data['address_id'] !=$address_id) {
		$address_id = $data['address_id'];
		$xml .= "\t\t<pos_owner_company_id=\"" . $data['address_id'] ."\"/>" . "\r\n";
		$xml .= "\t\t\t" . '<market_id><![CDATA[' . $data['posaddress_client_id'] .']]></market_id>' . "\r\n";
		$xml .= "\t\t\t" . '<address_company><![CDATA[' . $data['address_company'] .']]></address_company>' . "\r\n";
		$xml .= "\t\t\t" . '<address_company2><![CDATA[' . $data['address_company2'] .']]></address_company2>' . "\r\n";
		$xml .= "\t\t\t" . '<address_street><![CDATA[' . $data['address_street'] .']]></address_street>' . "\r\n";
		$xml .= "\t\t\t" . '<address_streetnumber><![CDATA[' . $data['address_streetnumber'] .']]></address_streetnumber>' . "\r\n";
		$xml .= "\t\t\t" . '<address_address2><![CDATA[' . $data['address_address2'] .']]></address_address2>' . "\r\n";
		$xml .= "\t\t\t" . '<address_zip><![CDATA[' . $data['address_zip'] .']]></address_zip>' . "\r\n";
		$xml .= "\t\t\t" . '<address_place_id><![CDATA[' . $data['address_place_id'] .']]></address_place_id>' . "\r\n";
		$xml .= "\t\t\t" . '<place_name><![CDATA[' . $data['place_name'] .']]></place_name>' . "\r\n";
		$xml .= "\t\t\t" . '<place_province_id><![CDATA[' . $data['place_province'] .']]></place_province_id>' . "\r\n";
		$xml .= "\t\t\t" . '<province_name><![CDATA[' . $data['province_canton'] .']]></province_name>' . "\r\n";
		$xml .= "\t\t\t" . '<address_country><![CDATA[' . $data['address_country'] .']]></address_country>' . "\r\n";
		$xml .= "\t\t\t" . '<country_name><![CDATA[' . $data['country_name'] .']]></country_name>' . "\r\n";
		$xml .= "\t\t\t" . '<address_phone_country><![CDATA[' . $data['address_phone_country'] .']]></address_phone_country>' . "\r\n";
		$xml .= "\t\t\t" . '<address_phone_area><![CDATA[' . $data['address_phone_area'] .']]></address_phone_area>' . "\r\n";
		$xml .= "\t\t\t" . '<address_phone_number><![CDATA[' . $data['address_phone_number'] .']]></address_phone_number>' . "\r\n";
		$xml .= "\t\t\t" . '<address_mobile_phone_country><![CDATA[' . $data['address_mobile_phone_country'] .']]></address_mobile_phone_country>' . "\r\n";
		$xml .= "\t\t\t" . '<address_mobile_phone_area><![CDATA[' . $data['address_mobile_phone_area'] .']]></address_mobile_phone_area>' . "\r\n";
		$xml .= "\t\t\t" . '<address_mobile_phone_number><![CDATA[' . $data['address_mobile_phone_number'] .']]></address_mobile_phone_number>' . "\r\n";
		$xml .= "\t\t\t" . '<address_email><![CDATA[' . $data['address_email'] .']]></address_email>' . "\r\n";
		$xml .= "\t\t\t" . '<address_website><![CDATA[' . $data['address_website'] .']]></address_website>' . "\r\n";
		$xml .= "\t\t\t" . '<address_type><![CDATA[' . $data['address_type'] .']]></address_type>' . "\r\n";
		$xml .= "\t\t\t" . '<address_type_name><![CDATA[' . $data['address_type_name'] .']]></address_type_name>' . "\r\n";
		$xml .= "\t\t\t" . '<address_client_type><![CDATA[' . $data['address_client_type'] .']]></address_client_type>' . "\r\n";
		$xml .= "\t\t\t" . '<client_type_code><![CDATA[' . $data['client_type_code'] .']]></client_type_code>' . "\r\n";
		$xml .= "\t\t\t" . '<address_contact_name><![CDATA[' . $data['address_contact_name'] .']]></address_contact_name>' . "\r\n";
		$xml .= "\t\t\t" . '<address_contact_email><![CDATA[' . $data['address_contact_email'] .']]></address_contact_email>' . "\r\n";
		$xml .= "\t\t\t" . '<address_legnr><![CDATA[' . $data['address_legnr'] .']]></address_legnr>' . "\r\n";
		$xml .= "\t\t</pos_owner_company>" . "\r\n";
	}
}
$xml .= "\t</pos_owner_companies>" . "\r\n";




$xml .= "\t<poslocations>" . "\r\n";
foreach($poslocations as $key=>$data) {
	
	$xml .= "\t\t<pos_location id=\"" . $data['posaddress_id'] ."\"/>" . "\r\n";
	$xml .= "\t\t\t" . '<market_id><![CDATA[' . $data['posaddress_client_id'] .']]></market_id>' . "\r\n";
	$xml .= "\t\t\t" . '<pos_owner_company_id><![CDATA[' . $data['posaddress_franchisee_id'] .']]></pos_owner_company_id>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_ownertype><![CDATA[' . $data['posaddress_ownertype'] .']]></posaddress_ownertype>' . "\r\n";
	$xml .= "\t\t\t" . '<posowner_type_name><![CDATA[' . $data['posowner_type_name'] .']]></posowner_type_name>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_store_postype><![CDATA[' . $data['posaddress_store_postype'] .']]></posaddress_store_postype>' . "\r\n";
	$xml .= "\t\t\t" . '<postype_name><![CDATA[' . $data['postype_name'] .']]></postype_name>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_store_subclass><![CDATA[' . $data['posaddress_store_subclass'] .']]></posaddress_store_postype>' . "\r\n";
	$xml .= "\t\t\t" . '<possubclass_name><![CDATA[' . $data['possubclass_name'] .']]></possubclass_name>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_store_furniture><![CDATA[' . $data['posaddress_store_furniture'] .']]></posaddress_store_furniture>' . "\r\n";
	$xml .= "\t\t\t" . '<product_line_name><![CDATA[' . $data['product_line_name'] .']]></product_line_name>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_store_furniture_subclass><![CDATA[' . $data['posaddress_store_furniture_subclass'] .']]></posaddress_store_furniture_subclass>' . "\r\n";
	$xml .= "\t\t\t" . '<productline_subclass_name><![CDATA[' . $data['productline_subclass_name'] .']]></productline_subclass_name>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_eprepnr><![CDATA[' . $data['posaddress_eprepnr'] .']]></posaddress_eprepnr>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_name><![CDATA[' . $data['posaddress_name'] .']]></posaddress_name>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_place_id><![CDATA[' . $data['posaddress_place_id'] .']]></posaddress_place_id>' . "\r\n";
	$xml .= "\t\t\t" . '<place_name><![CDATA[' . $data['place_name'] .']]></place_name>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_name2><![CDATA[' . $data['posaddress_name2'] .']]></posaddress_name2>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_street><![CDATA[' . $data['posaddress_street'] .']]></posaddress_street>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_street_number><![CDATA[' . $data['posaddress_street_number'] .']]></posaddress_street_number>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_address2><![CDATA[' . $data['posaddress_address2'] .']]></posaddress_address2>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_zip><![CDATA[' . $data['posaddress_zip'] .']]></posaddress_zip>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_place_id><![CDATA[' . $data['posaddress_place_id'] .']]></posaddress_place_id>' . "\r\n";
	$xml .= "\t\t\t" . '<place_name><![CDATA[' . $data['place_name'] .']]></place_name>' . "\r\n";
	$xml .= "\t\t\t" . '<place_province_id><![CDATA[' . $data['place_province'] .']]></place_province_id>' . "\r\n";
	$xml .= "\t\t\t" . '<province_name><![CDATA[' . $data['province_canton'] .']]></province_name>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_country><![CDATA[' . $data['posaddress_country'] .']]></posaddress_country>' . "\r\n";
	$xml .= "\t\t\t" . '<country_name><![CDATA[' . $data['country_name'] .']]></country_name>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_phone_country><![CDATA[' . $data['posaddress_phone_country'] .']]></posaddress_phone_country>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_phone_area><![CDATA[' . $data['posaddress_phone_area'] .']]></posaddress_phone_area>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_phone_number><![CDATA[' . $data['posaddress_phone_number'] .']]></posaddress_phone_number>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_mobile_phone_country><![CDATA[' . $data['posaddress_mobile_phone_country'] .']]></posaddress_mobile_phone_country>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_mobile_phone_area><![CDATA[' . $data['posaddress_mobile_phone_area'] .']]></posaddress_mobile_phone_area>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_mobile_phone_number><![CDATA[' . $data['posaddress_mobile_phone_number'] .']]></posaddress_mobile_phone_number>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_email><![CDATA[' . $data['posaddress_email'] .']]></posaddress_email>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_website><![CDATA[' . $data['posaddress_website'] .']]></posaddress_website>' . "\r\n";
	

	$xml .= "\t\t\t" . '<posaddress_contact_name><![CDATA[' . $data['posaddress_contact_name'] .']]></posaddress_contact_name>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_contact_email><![CDATA[' . $data['posaddress_contact_email'] .']]></posaddress_contact_email>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_store_grosssurface><![CDATA[' . $data['posaddress_store_grosssurface'] .']]></posaddress_store_grosssurface>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_store_totalsurface><![CDATA[' . $data['posaddress_store_totalsurface'] .']]></posaddress_store_totalsurface>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_store_retailarea><![CDATA[' . $data['posaddress_store_retailarea'] .']]></posaddress_store_retailarea>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_store_backoffice><![CDATA[' . $data['posaddress_store_backoffice'] .']]></posaddress_store_backoffice>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_store_numfloors><![CDATA[' . $data['posaddress_store_numfloors'] .']]></posaddress_store_numfloors>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_store_floorsurface1><![CDATA[' . $data['posaddress_store_floorsurface1'] .']]></posaddress_store_floorsurface1>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_store_floorsurface2><![CDATA[' . $data['posaddress_store_floorsurface2'] .']]></posaddress_store_floorsurface2>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_store_floorsurface3><![CDATA[' . $data['posaddress_store_floorsurface3'] .']]></posaddress_store_floorsurface3>' . "\r\n";

	$xml .= "\t\t\t" . '<posaddress_store_floor><![CDATA[' . $data['posaddress_store_floor'] .']]></posaddress_store_floor>' . "\r\n";
	$xml .= "\t\t\t" . '<floor_name><![CDATA[' . $data['floor_name'] .']]></floor_name>' . "\r\n";


	$xml .= "\t\t\t" . '<posaddress_store_openingdate><![CDATA[' . $data['posaddress_store_openingdate'] .']]></posaddress_store_openingdate>' . "\r\n";

	$xml .= "\t\t\t" . '<posaddress_google_lat><![CDATA[' . $data['posaddress_google_lat'] .']]></posaddress_google_lat>' . "\r\n";
	$xml .= "\t\t\t" . '<posaddress_google_long><![CDATA[' . $data['posaddress_google_long'] .']]></posaddress_google_long>' . "\r\n";


	

	
	
	$xml .= "\t\t</pos_location>" . "\r\n";
}
$xml .= "\t</poslocations>" . "\r\n";


$xml .= "</data>" . "\r\n";
$xml = str_replace('&', '&amp;', $xml);


$filename = "tissot_pos_locations_" . date("Y-m-d") . ".xml";
header('Content-disposition: attachment; filename=' . $filename);
header('Content-type: "text/xml"; charset="utf8"');

echo $xml;
exit;

?>