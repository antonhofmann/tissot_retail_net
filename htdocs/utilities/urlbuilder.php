<?php

/**
 * URL builder utility class
 *
 * Use this class to get URLs to various parts of the application
 */
class URLBuilder {
	/**
	 * Get URL to project task center
	 * @param  integer $projectID
	 * @return string
	 */
	public static function projectTaskCenter($projectID) {
		return sprintf('%s/user/project_task_center.php?pid=%d',
			url::host(),
			$projectID
			);
	}

	/**
	 * Get URL to order task center
	 * @param  integer $orderID
	 * @return string
	 */
	public static function orderTaskCenter($orderID) {
		return sprintf('%s/user/order_task_center.php?oid=%d',
			url::host(),
			$orderID
			);
	}

	/**
	 * Get URL to edit order traffic data page
	 * @param  integer $orderID
	 * @return string
	 */
	public static function editOrderTrafficData($orderID) {
		return sprintf('%s/user/order_edit_traffic_data.php?oid=%d',
			url::host(),
			$orderID
			);	
	}

	/**
	 * Get URL to edit project traffic data page
	 * @param  integer $projectID
	 * @return string
	 */
	public static function editProjectTrafficData($projectID) {
		return sprintf('%s/user/project_edit_traffic_data.php?pid=%d',
			url::host(),
			$projectID
			);	
	}

	/**
	 * Get URL to cost monitoring sheet edit page
	 * @param  integer $projectID
	 * @return string
	 */
	public static function editCMS($projectID) {
		return sprintf('%s/user/project_costs_real_costs.php?pid=%d',
			url::host(),
			$projectID
			);
	}

	/**
	 * Get URL to edit POS data page
	 * @param  integer $projectID
	 * @return string
	 */
	public static function editPOSData($projectID) {
		return sprintf('%s/user/project_edit_pos_data.php?pid=%d',
			url::host(),
			$projectID
			);
	}

	/**
	 * Get MPS POS page
	 * @param  integer $posAddressID
	 * @return string
	 */
	public static function mpsPOSAddress($posAddressID) {
		return sprintf('%s/mps/pos/address/%d',
			url::host(),
			$posAddressID
			);
	}

	/**
	 * Get link to edit POS opening hours
	 * @param  integer $countryID
	 * @param  integer $posAddressID
	 * @return string
	 */
	public static function editPOSOpeningHours($countryID, $posAddressID) {
		return sprintf('%s/pos/posopeninghr.php?country=%d&id=%d',
			url::host(),
			$countryID,
			$posAddressID
			);
	}

	/**
	 * Edit order supplier data
	 * @param  integer $orderID
	 * @return string
	 */
	public static function editOrderSupplierData($orderID) {
		return sprintf('%s/user/order_edit_supplier_data.php?oid=%d',
			url::host(),
			$orderID
			);
	}

	/**
	 * Edit project supplier data
	 * @param  integer $projectID
	 * @return string
	 */
	public static function editProjectSupplierData($projectID) {
		return sprintf('%s/user/project_edit_supplier_data.php?pid=%d',
			url::host(),
			$projectID
			);
	}

	/**
	 * Get pos customer service url
	 * @param  integer $posID
	 * @param  integer $countryID
	 * @return string
	 */
	public static function customerService($posID, $countryID) {
		return sprintf('%s/pos/posindex_customerservice.php?pos_id=%d&country=%d&ltf=all',
			url::host(),
			$posID,
			$countryID
			);
	}

	/**
	 * Edit catalog certificate
	 * @param  integer $certificateID
	 * @return string
	 */
	public static function editCertificate($certificateID) {
		return sprintf('%s/catalog/certificates/data/%d',
			url::host(),
			$certificateID
			);
	}

	/**
	 * Closing assesments
	 * @return string
	 */
	public static function closingAssessments() {
		return sprintf('%s/pos/pos_closing_assessments.php', url::host());
	}

	/**
	 * CER application lease
	 * @param  integer $projectID
	 * @return string
	 */
	public function cerApplicationLease($projectID) {
		return sprintf('%s/cer/cer_application_lease.php?pid=%d',
			url::host(),
			$projectID
			);
	}


	/**
	 * Get URL to view project traffic data
	 * @param  integer $projectID
	 * @return string
	 */
	public static function projectViewTrafficData($projectID) {
		return sprintf('%s/user/project_view_traffic_data.php?pid=%d',
			url::host(),
			$projectID
			);
	}
}