<?php

	class Connector {
		
		/**
		 * Singelton
		 * @return Connector
		 */
		private static $instance;
		
		/**
		 * Connections data
		 * @var array
		 */
		protected $connectors;

		/**
		 * Environment
		 * @var string
		 */
		protected $server;
		
		/**
		 * Selectors
		 * @var array
		 */
		protected $selectors;
		
		const DB_CORE = 'db_retailnet';
		const DB_RETAILNET_MPS = 'db_retailnet_mps';
		const DB_RETAILNET_MPS_FLIK_FLAK = 'db_retailnet_mps_flikflak';
		const DB_RETAILNET_RED = 'db_retailnet_red';
		const DB_RETAILNET_EXCHANGES = 'db_retailnet_ramco_exchange';
		const DB_RETAILNET_STORE_LOCATOR = 'db_storelocator';
		const DB_TOTARA = 'db_totara';
		
		/**
		 * Singelton
		 * @return Connector
		 */
		public static function instance($server=null) {
			
			if (!static::$instance) {
				static::$instance = new Connector($server);
			}
			
			return static::$instance;
		}
		
		public function __construct($server=null) {
			
			// curent server
			$this->server = ($server) ? $server : url::server();
				
			// load start connectors
			$data = parse_ini_file(PATH_SETTINGS."db.ini", true); 
			$drive = $data[$this->server]['drive'];
			$host = $data[$this->server]['host'];
			$dbname = $data[$this->server]['database'];
			$user = $data[$this->server]['username'];
			$pass = $data[$this->server]['password'];
			
			try {
				$pdo = new PDO("$drive:host=$host;dbname=$dbname", $user, $pass);
				$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
			}
			catch( PDOException $Exception ) {
				die("Connector ($host): PDO drive error.");
			}
			
			// get all connections data
			$sth = $pdo->query("
				SELECT
					db_connector_db,
					db_connector_server,
					db_name,
					db_connector_drive,
					db_connector_host,
					db_connector_username,
					db_connector_password
				FROM db_connectors
				INNER JOIN dbs ON db_id = db_connector_db
			");
				
			if ($sth) {
				
				$result = $sth->fetchAll();
				
				foreach ($result as $row) {
					
					$environment = $row['db_connector_server'];
					$db = $row['db_connector_db'];
					
					// database environments connectors
					$this->connectors[$environment][$db] = array(
						'drive' => $row['db_connector_drive'],
						'host' => $row['db_connector_host'],
						'database' => $row['db_name'],
						'username' => $row['db_connector_username'],
						'password' => $row['db_connector_password']
					);
					
					// db selector through database name
					$name = $row['db_name'];
					$this->selectors['database'][$name] = $db;
				}
				
				// db selector through application shortcuts
				$sth = $pdo->query("SELECT * FROM applications");
				
				if ($sth) {
					
					$result = $sth->fetchAll();
					
					foreach ($result as $row) {
						$app = $row['application_shortcut'];
						$db = $row['application_db'];
						$this->selectors['applications'][$app] = $db;
					}
				}
			}
		}
		
		/**
		 * Select Database from db name
		 * @param string $db, database name
		 * @param string $server, host
		 */
		public static function id($db, $server=null) {
			static::instance();			
			$server = ($server) ? $server : self::$instance->server;
			return self::$instance->connectors[$server][$db];
		}
		
		/**
		 * Select connector from application short name
		 * @param string $app, application name
		 * @param string $server, host
		 * @return array
		 */
		public static function application($app, $server=null) {
			self::instance();
			$server = ($server) ? $server : self::$instance->server;
			$db = self::$instance->selectors['applications'][$app];
			return self::$instance->connectors[$server][$db];
		}
		
		/**
		 * Select connector from db name
		 * @param string $app, application name
		 * @param string $server, host
		 * @return array
		 */
		public static function database($name, $server=null) {
			self::instance();
			$server = ($server) ? $server : self::$instance->server;
			$db = self::$instance->selectors['database'][$name];
			return self::$instance->connectors[$server][$db];
		}
	
		/**
		 * Select connector from db name or db shortcut or application shortcut
		 * @param string $param
		 * @param string $server
		 * @return array
		 */
		public static function select($param=null, $server=null) {
			
			static::instance();
			$param = ($param) ? $param : Settings::init()->db_default;
			
			// select from db name
			$connect = static::$instance->id($param, $server);
			
			// select from application shortcut
			if (!$connect) $connect = static::$instance->application($param, $server);
			
			// select from db shortcut
			if (!$connect) $connect = static::$instance->database($param, $server);
			
			return $connect;
		}
	}