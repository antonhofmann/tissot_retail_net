<?php 

class Connector {
	
	/**
	 * Instance
	 * 
	 * @return Connector
	 */
	private static $instance;
	
	/**
	 * Environments
	 * 
	 * @var array
	 */
	private $environments;

	/**
	 * Databases
	 * 
	 * @var stdClass
	 */
	private $databases;
	
	/**
	 * Connectors
	 * 
	 * @var array
	 */
	private $connectors;

	/**
	 * Application databases
	 * @var array
	 */
	protected $applications;


	/* deprected */
	const DB_CORE = 'db_retailnet';
	const DB_RETAILNET_MPS = 'db_retailnet_mps';
	const DB_RETAILNET_MPS_FLIK_FLAK = 'db_retailnet_mps_flikflak';
	const DB_RETAILNET_RED = 'db_retailnet_red';
	const DB_RETAILNET_EXCHANGES = 'db_retailnet_ramco_exchange';
	const DB_RETAILNET_STORE_LOCATOR = 'db_storelocator';
	const DB_RETAILNET_NEWS = 'db_retailnet_news';
	const DB_TOTARA = 'db_totara';

	
	public function __construct() {

		// Load environments
		$result = File::load("/applications/settings/environments.json");
		$this->environments = json_decode($result, true);

		// Load databases
		$result = File::load("/applications/settings/databases.json");
		$this->databases = json_decode($result);
	}

	/**
	 * Load PDO drive
	 * 
	 * @param  string $host server name
	 * @param  string $schema schema name
	 * @param  array $attributes schema name
	 * @return PDO
	 */
	private function connect($host, $schema) { 

		// set environment
		$environment = $this->environments[$host];
		
		if (!$environment) {
			die("CONNECTOR: environment not found ($host, $schema).\n");
		}
		
		if (!$this->connectors[$environment][$schema]) {
			
			// database
			$database = $this->databases->$environment->$schema;

			// dns data
			$dns = "$database->drive:host=$database->host;dbname=$schema";
			$username = $database->username;
			$password = $database->password;

			// drive attributes
			if ($database->charset) {
				$attributes = array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES $database->charset");
			}
			
			try {
				
				$pdo = new PDO($dns, $username, $password, $attributes);

				// fetching mode
				$pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);

				// set charset
				if ($database->charset) {
					$pdo->exec("SET CHARACTER SET $database->charset ");
					$pdo->exec("SET NAMES $database->charset ");
				}

				// set local time nameing
				if (isset($database->lc_time_names) and $database->lc_time_names) {
					$pdo->exec("SET lc_time_names = '$database->lc_time_names' ");
				}

				// set buffer length
				if (isset($database->net_buffer_length) and $database->net_buffer_length) {
					$pdo->exec("SET @@global.net_buffer_length = $database->net_buffer_length ");
				}
				
				// max included packeds
				if (isset($database->max_allowed_packet) and $database->max_allowed_packet) {
					$pdo->exec("SET @@global.max_allowed_packet = $database->max_allowed_packet ");
				}

				// group concat limit
				$pdo->exec("SET SESSION group_concat_max_len = 1000000");
				
				// store connector
				$this->connectors[$environment][$schema] = $pdo;
			}
			catch( \PDOException $e) { 
				die(sprintf("CONNECTOR: pdo not found (%s). Check database name or connection parameters.\n", $schema));
			}
		}
		
		return $this->connectors[$environment][$schema];
	}

	/**
	 * Connection data loader
	 * 
	 * @param  $database mixed, as string is database name or as array[host, name]
	 * @param  $host string hostname
	 * @return PDO
	 */
	static public function get($database=null, $host=null) { 

		// server host name
		$host = $host ?: $_SERVER['SERVER_NAME']; 
		
		// command line environment
		$host = $host ?: 'retailnet.tissot.ch';

		// get application database
		if ($database && !static::instance()->isDatabase($database, $host)) {
			$database = static::instance()->getApplicationDatabase($database, $host);
		}
		
		// database name
		if (!$database) {
			$database = Settings::init()->db_default;
		}

		return static::instance()->connect($host, $database);
	}

	/**
	 * Singelton pattern
	 * 
	 * @return Connector
	 */
	public static function instance() { 
		
		if (!static::$instance) {
			static::$instance = new Connector();
		}
		
		return static::$instance;
	}

	/**
	 * Get the current environment for the current host
	 * @throws Exception If there is no environment configured for current host
	 * @return string
	 */
	public function getEnvironment() {
		if (isset($this->environments[$_SERVER['SERVER_NAME']])) {
			return $this->environments[$_SERVER['SERVER_NAME']];
		}
		else {
			throw new Exception(sprintf('No environment configured for host %s', $_SERVER['SERVER_NAME']));
		}
	}

	/**
	 * Get databases for an environment
	 * @param  string $environment Defaults to environment for current host
	 * @return object
	 */
	public function getDatabases($environment = null) {
		if (is_null($environment)) {
			$environment = $this->getEnvironment();
		}
		return $this->databases->${environment};
	}

	/**
	 * Check is database valid
	 * 
	 * @param  $database mixed, as string is database name or as array[host, name]
	 * @param  $host string hostname
	 * @return PDO
	 */
	public function isDatabase($database, $host=null) {

		// server host name
		$host = $host ?: $_SERVER['SERVER_NAME']; 

		// command line environment
		$host = $host ?: 'retailnet.tissot.ch';

		// get environment from server name
		$environment = $this->environments[$host];

		if (!$environment || !$database) return;
		if (!$this->databases->$environment) return;
		if (!$this->databases->$environment->$database) return;

		return true;
	}

	/**
	 * Check has application registred database
	 * 
	 * @param  string $application application short name
	 * @param  string $host host name
	 * @return string
	 */
	public function getApplicationDatabase($application, $host=null) { 

		// server host name
		$host = $host ?: $_SERVER['SERVER_NAME']; 
		
		// command line environment
		$host = $host ?: 'retailnet.tissot.ch';

		if (!$this->applications) {
						
			// database name
			$dbDefault = Settings::init()->db_default;
			$db = $this->connect($host, $dbDefault);

			if (!$db) return;

			$sth = $db->prepare("
				SELECT db_name, application_shortcut
				FROM dbs
				INNER JOIN applications ON application_db = db_id
			");

			$sth->execute();
			$result = $sth->fetchAll();

			if ($result) {	
				foreach ($result as $row) {
					$name = $row['application_shortcut'];;
					$this->applications[$name] = $row['db_name'];
				}
			}
		}

		return $db = $this->applications[$application];
	}
}