<?php 

	class translate {
	
		private static $instance;
		public $_ = array();
		
		public static function instance() {
			
			if (!self::$instance) {
				self::$instance = new Translate();
			}
			
			return self::$instance;
		}
		
		public function __construct() {
			
			$settings = Settings::init();
			$model = new Model(Connector::DB_CORE);
			
			$sth = $model->db->query("
				SELECT DISTINCT
					translation_id,
					translation_category_name,
					translation_keyword_name AS keyword,
					translation_content AS content
				FROM translations
					INNER JOIN translation_categories ON translation_category_id = translation_category
					INNER JOIN translation_keywords ON translation_keyword_id = translation_keyword
					INNER JOIN languages ON language_id = translation_language
				WHERE language_iso639_1 = '".$settings->language."'
			");
				
			if ($sth) { 
				
				$result = $sth->fetchAll(); 
				
				foreach ($result as $row) {
					$key = ($row['translation_category_name'] == 'keyword') ? $row['keyword'] : $row['translation_category_name']."_".$row['keyword'];
					$this->_[$key] = $row['content'];
				}
			}
		}

		/**
		 * Translate a keyword
		 * @param  string $keyword
		 * @return string
		 */
		public function translate($keyword) {
			return $this->_[$keyword];
		}

		/**
		 * Get a translation by using magic class members
		 * Example echo $translate->save;
		 * @param  string $keyword
		 * @return string
		 */
		public function __get($keyword) {
			return $this->_[$keyword];
		}
		
		public function __set($keyword, $value) {
			$this->_[$keyword] = $value;
		}

		/**
		 * Pass variable(s) to a keyword
		 * 
		 * @code
		 * // translation: "Posted {nr_of_days} days ago"
		 * print $translate->posted_ago(array('nr_of_days' => 5));
		 * // Posted 5 days ago
		 * @endcode
		 * 
		 * @param  string $keyword
		 * @param  array $arguments
		 * @return string
		 */
		public function __call($keyword, $arguments) {
			
			if ($this->_[$keyword] && is_array($arguments[0])) {
				
				foreach ($arguments[0] as $key => $value) {
					$search[] = '{'.trim($key).'}';
					$replace[] = trim($value);
				}
				
				return str_replace($search, $replace, $this->_[$keyword]);
			}
		}
	}