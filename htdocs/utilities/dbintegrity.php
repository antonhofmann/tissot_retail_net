<?php

	/**
	 * Database Integrity
	 * @author adoweb
	 * @copyright mediaparx ag
	 * @version 1.2
	 */
	class dbIntegrity {

		/**
		 * Check Database intefrity
		 * 
		 * Table in DB can be deleted in folowing cases:
		 * 
		 * - is deletabled and hasn't references (not registred in DB Integrity System)
		 * - is deletabled and has deletabled and cascading permitted references
		 * 
		 * @param int $id
		 * @param string $table
		 * @param string $application
		 * @return boolean
		 */
		public static function check($id, $table, $application=null) {
						
			// determine application name
			$application = ($application) ? $application : url::application();
			
			// table
			$table = self::tableFromApplication($table, $application);
			
			if ($table) { 
				
				if ($table['db_table_records_can_be_deleted']) {
					
					$references = self::reference($table['db_table_id']); 
					
					if ($references) { 
						
						$model = new Model(Connector::DB_CORE);
						$return = true;
						
						foreach ($references as $row) {
							
							$reference = self::table($row['db_reference_referring_table']);
							$db_name = $reference['db_name'];
							$table_name = $reference['db_table_table'];
							$primary_key = $reference['db_table_primary_key'];
							$foreign_key = $row['db_reference_foreign_key'];
							
							$sth = $model->db->query("SELECT * FROM $db_name.$table_name WHERE $foreign_key = $id");
							$check = ($sth) ? $sth->fetchAll() : null;
							
							if ($check && $reference['db_table_records_can_be_deleted'] && !$reference['db_table_delete_cascade']) { 
								$return = false;
								break;
							}
						}
						
						return $return;
						
					} else {
						return true;
					}
					
				} else {
					return false;
				}
			}
			else {
				return true;
			}
		}
		
		/**
		 * Get table data
		 * @param int $table
		 * @return array
		 */
		protected static function table($table) {
			
			$model = new Model(Connector::DB_CORE);
			
			$sth = $model->db->query("
				SELECT
					db_table_id,
					db_table_table,
					db_table_delete_cascade,
					db_table_primary_key,
					db_table_records_can_be_deleted,
					db_table_parent_table,
					db_name
				FROM db_retailnet.db_tables
					INNER JOIN db_retailnet.dbs ON db_id = db_table_db
					INNER JOIN db_retailnet.applications ON db_id = application_db
				WHERE db_table_id = $table
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		/**
		 * Get References
		 * @param int $table
		 * @return array
		 */
		protected static function reference($table) {
			
			$model = new Model(Connector::DB_CORE);
			
			$sth = $model->db->query("
				SELECT
					db_reference_id,
					db_reference_referring_table,
					db_reference_foreign_key
				FROM db_retailnet.db_references
				WHERE db_reference_referenced_table_id = $table
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		/**
		 * Get table from table name and application
		 * @param string $table
		 * @param string $application
		 * @return array
		 */
		protected static function tableFromApplication($table, $application) {
			
			$model = new Model(Connector::DB_CORE);
			
			$sth = $model->db->query("
				SELECT
					db_table_id,
					db_table_table,
					db_table_delete_cascade,
					db_table_primary_key,
					db_table_records_can_be_deleted,
					db_table_parent_table,
					db_name
				FROM db_retailnet.db_tables
					INNER JOIN db_retailnet.dbs ON db_id = db_table_db
					INNER JOIN db_retailnet.applications ON db_id = application_db
				WHERE db_table_table = '$table' AND application_shortcut = '$application'
			");
			
			return ($sth) ? $sth->fetch() : null;
		}		
	}
