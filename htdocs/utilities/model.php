<?php

	class Model {
		
		/**
		 * Database
		 * 
		 * @return Database
		 */
		public $db;
		
		public $sql;


		const FLAG_DISTINCT = 'DISTINCT';
		const FLAG_FOUND_ROWS = 'SQL_CALC_FOUND_ROWS';

		public function __construct($param=null, $server=null) {

			if ($param && Application::is($param)) {
				$param = Application::is($param);
			}

			$this->db = Connector::get($param, $server);
		}

		public function query($query) {
			$this->query = $query;
			$this->sql = null;
			return $this;
		}

		public function bind($binds) {

			if (!$this->binds) {
				$this->binds = (is_array($binds)) ? $binds : array($binds);
			} else {
				if (is_array($binds)) array_merge($this->binds, $binds);
				else array_push($this->binds, $binds);
			}

			return $this;
		}

		public function filter($filter, $value=null) {

			if (is_array($filter)) {
				foreach ($filter as $key => $value) {
					$this->filters[$key] = $value;
				}
			} elseif ($value) {
				$this->filters[$filter] = $value;
			}

			return $this;
		}

		public function group($field) {
			$this->groups[] = $field;
			return $this;
		}

		public function exclude($key) {
			unset($this->filters[$key]);
			return $this;
		}

		public function extend($content) {
			
			if ($content) {
				$this->extend .= " $content";
			}
			
			return $this;
		}

		public function order($order, $direction='asc') {
			$this->orders[] = $order.' '.$direction;
			return $this;
		}

		public function offset($offset, $rows=null) {
			
			$rows = $rows ?: Settings::init()->limit_pager_rows;
			
			if (is_numeric($rows)) {
				$this->offset = $offset.', '.$rows;
			}
			
			return $this;
		}

		public function fetch($dump=false) {

			if ($this->query) {
				$query = $this->query;
				unset($this->query);
			}

			if ($this->binds) {
				$query .= ' '.join(' ', $this->binds);
				unset($this->binds);
			}

			if ($this->filters) {
				$query .= ' WHERE '.join(' AND ', $this->filters);
				unset($this->filters);
			}

			if ($this->extend) {
				$query .= $this->extend;
				unset($this->extend);
			}

			unset($this->orders);
			unset($this->offset);

			if ($query) {
				if ($dump) echo "<div class=preformated >$query</div>";
				$sth = $this->db->query($query);

				if ($this->fetchClass) {
					$sth->setFetchMode(PDO::FETCH_CLASS, $this->fetchClass);
				}
				
				$this->sql = $query;
				$result = ($sth) ? $sth->fetch() : null;

				// we can't keep fetchClass between queries
				unset($this->fetchClass);

				return $result;
			}
		}

		public function fetchAll($dump=false) {

			if ($this->query) {
				$query = $this->query;
				unset($this->query);
			}

			if (isset($this->binds) and $this->binds) {
				$query .= ' '.join(' ', $this->binds);
				unset($this->binds);
			}

			if (isset($this->filters) and $this->filters) {
				$filters = join(' AND ', $this->filters);
				$query .= ($this->extend) ? " WHERE ($filters)" : " WHERE $filters";
				unset($this->filters);
			}

			if (isset($this->extend) and $this->extend) {
				$query .= $this->extend;
				unset($this->extend);
			}

			if (isset($this->groups) and $this->groups) {
				$query .= ' GROUP BY '.join(', ', $this->groups);
				unset($this->groups);
			}

			if (isset($this->orders) and $this->orders) {
				$query .= ' ORDER BY '.join(', ', $this->orders);
				unset($this->orders);
			}

			if (isset($this->offset) and $this->offset) {
				$query .= ' LIMIT '.$this->offset;
				unset($this->offset);
			}

			if ($query) {	
				if ($dump) echo "<div class=preformated >".nl2br($query)."</div>";
				$sth = $this->db->query($query);

				if (isset($this->fetchClass) and $this->fetchClass) {
					$sth->setFetchMode(PDO::FETCH_CLASS, $this->fetchClass);
				}

				$this->sql = $query;
				$result = ($sth) ? $sth->fetchAll() : null;

				// we can't keep fetchClass between queries
				unset($this->fetchClass);

				return $result;
			}
		}

		/**
		 * Set fetch class for statement
		 * @param string
		 * @return object Instance of this class for chaining
		 */
		public function fetchClass($name) {
			$this->fetchClass = $name;
			return $this;
		}

		public function totalRows() {
			$sth = $this->db->query("SELECT FOUND_ROWS() AS totalrows");
			$result = ($sth) ? $sth->fetch() : null;
			return ($result['totalrows']) ? $result['totalrows'] : 0;
		}



		static function prepare($sql, array $parameters = array()) {
    
		    $pattern = '/"[^"]*"|\'[^\']*\'|\\\?|\\\:[[:alpha:]_][[:alnum:]_]*/';
		    $alias   = $sql;
		    
		    if(preg_match_all($pattern, $alias, $match, PREG_OFFSET_CAPTURE)) { 
		        foreach($match[0] as $replace) {
		            $length = strlen($replace[0]);
		            if($replace[0] === '\\')
		                $alias = substr_replace($alias, str_repeat('-', $length + 1), $replace[1], $length + 1);
		            else
		                $alias = substr_replace($alias, str_repeat('-', $length), $replace[1], $length);
		        }
		    }
		    

		    $pattern = '/(?<!(\"|\'|\\\\))(\?|\:[[:alpha:]_][[:alnum:]_]*)/';
		    
		    if(preg_match_all($pattern, $alias, $match, PREG_OFFSET_CAPTURE)) {
		        
		        $placeholders = array();
		        $i = 0;

		        if(count($match[0]) > count($parameters)) {
		            //throw new Exception('DB Model: Number of variables doesn\'t match number of parameters in prepared statement');
		            return false;
		        }
		        
		        foreach($match[0] as $placeholder) {
		            if($placeholder[0][0] === ':' && isset($parameters[($name = substr($placeholder[0], 1))]))
		                $placeholders[] = array($placeholder[0], $placeholder[1], $parameters[$name]);
		            elseif(isset($parameters[$i]))
		                $placeholders[] = array($placeholder[0], $placeholder[1], $parameters[$i++]);
		            else return false;
		                //throw new Exception('DB Model: Number of variables doesn\'t match number of parameters in prepared statement');
		        }                              
		        
		        $offset = 0;
		        $length = 0;
		        
		        foreach($placeholders as $placeholder) {
		            switch(gettype($placeholder[2])) {
		                case 'boolean':
		                    $replace = (string) (int) $placeholder[2];
		                    break;
		                case 'integer':
		                    $replace = (string) $placeholder[2];
		                    break;
		                case 'double':
		                case 'float':
		                    $replace = (string) $placeholder[2];
		                    break;
		                case 'string':
		                    $replace = '"' . $placeholder[2] . '"';
		                    break;
		                case 'array':
		                case 'object':
		                    $replace = '"' . serialize($placeholder[2]) . '"';
		                    break;
		                case 'resource':
		                    $replace = (string) (int) $placeholder[2];
		                    break;
		                case 'NULL':
		                default:
		                    $replace = 'NULL';
		                    break;
		            }

		           $length  = strlen($placeholder[0]);
		           $sql     = substr_replace($sql, $replace, $placeholder[1] + $offset, $length);
		           $offset += strlen($replace) - $length;
		        }
		    }      
		    
		    return $sql;
		}
	}