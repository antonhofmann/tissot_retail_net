<?php


	class Integrity extends Model {

		public $references;
		public $deletabled;
		public $cascadin;

		public function __construct() {
			parent::__construct();
			$this->deletabled = true;
		}

		public function check($table=null) {
			return ($this->cascadin || ($this->deletabled && !$this->references) ) ? true : false;
		}

		public function set($id, $table, $application=null) {

			$application = ($application) ? $application : url::application();

			$sth = $this->db->query("
				SELECT
					db_table_id,
					db_table_table,
					db_table_delete_cascade,
					db_table_primary_key,
					db_table_records_can_be_deleted,
					db_table_parent_table,
					db_name
				FROM db_retailnet.db_tables
					INNER JOIN db_retailnet.dbs ON db_id = db_table_db
					INNER JOIN db_retailnet.applications ON db_id = application_db
				WHERE db_table_table = '$table' AND application_shortcut = '$application'
			");

			$data = ($sth) ? $sth->fetch() : null; 
			
			if ($data['db_table_delete_cascade']) {
				$this->cascadin = true;
			}
			
			if ($data['db_table_records_can_be_deleted']) {

				$sth = $this->db->query("
					SELECT
						db_reference_id,
						db_reference_referring_table,
						db_reference_foreign_key
					FROM db_retailnet.db_references
					WHERE db_reference_referenced_table_id = ".$data['db_table_id']."
				");

				if ($sth) {
					
					$references = $sth->fetchAll(); 

					foreach ($references as $row) {

						$query = "
							SELECT DISTINCT
								db_table_table,
								db_table_primary_key,
								db_name
							FROM db_retailnet.db_tables
								INNER JOIN db_retailnet.dbs ON db_id = db_table_db
								INNER JOIN db_retailnet.applications ON db_id = application_db
							WHERE db_table_id = {$row[db_reference_referring_table]}
						";
						
						$sth = $this->db->query($query);
						$data = ($sth) ? $sth->fetch() : null;

						$db_name = $data['db_name'];
						$table_name = $data['db_table_table'];
						$primary_key = $data['db_table_primary_key'];
						$foreign_key = $row['db_reference_foreign_key'];

						// has records
						$sth = $this->db->query("SELECT * FROM $db_name.$table_name WHERE $foreign_key = $id");
						$result = ($sth) ? $sth->fetchAll() : null;

						if ($result) {
							foreach ($result as $row) {
								$this->references[$db_name][$table_name][] = $row;
							}
						}
					}
				}
			}
			else {
				$this->deletabled = false;
			}
			
		}
		
	}
