<?php 

	/**
	 * Static date functions library
	 * @package framework
	 * @author Admir Serifi 
	 * @version 1.0
	 */
	class date {
		
		
		/**
		 * Get unix timestamp
		 * @param string $date format(dd.mm.yyyy)
		 * @return string
		 */
		public static function timestamp($date=null) {
			$date = ($date) ? $date : date('d.m.Y');
			list($day,$month,$year) = explode('.', $date);
			return mktime(0, 0, 0, $month, $day, $year);
		}

		/**
		 * Convert any date format to sql
		 * 
		 * @param string $date
		 * @return string
		 */
		static public function sql($date = null) {
			if ($date) {
				$timestamp = strtotime($date);
			}
			else {
				$timestamp = time();
			}
			return date('Y-m-d H:i:s', $timestamp);
		}
		
		/**
		 * Get system date format
		 * format patter in /app/$app/config/settings.json
		 * 
		 * @param string $date
		 * @return string
		 */
		static public function system($date) {
			$timestamp = strtotime($date);
			return date('d.m.Y', $timestamp);
		}
		
		/**
		 * Get all Modays from year
		 * @param integer $year year four digits
		 * @return array 
		 */
		public static function year_mondays($year) {
		
			$newyear = $year;
			$week = 0;
			$day = 0;
			$mo = 1;
			$mondays = array();
			$i = 1;
			
			while ($week != 1) {
				$day++;
				$week = date("w", mktime(0, 0, 0, $mo,$day, $year));
			}
		
			array_push($mondays,date("d.m.Y", mktime(0, 0, 0, $mo,$day, $year)));
		
			while ($newyear == $year) {
				$date =  strtotime(date("r", mktime(0, 0, 0, $mo,$day, $year)) . "+" . $i . " week");
				if ($year == date("Y",$date)) array_push($mondays,date("d.m.Y", $date));
				$newyear = date("Y", $date);
				$i++;
			}
		
			return $mondays;
		}
		
		
		/**
		 * Get week range of days
		 * @param integer $week week one or two digits
		 * @param integer $year four digits
		 * @param integer $start start day in range (1=monday)
		 * @param integer $year stop day in range (7=Sunday)
		 * @return string
		 */
		public static function week_range($week, $year, $start=1, $end=7) {
			$week = ($week < 10) ? $week = "0".$week : $week;
			return date('d.m', strtotime($year."W".$week.$start))." - ".date('d.m', strtotime($year."W".$week.$end));
		}
		
		
		/**
		 * Build date range for each week in year
		 * only for working days in year (Monday-Friday)
		 * @param integer $year four digits
		 * @return array
		 */
		public static function week_ranges($year) {
		
			$mondays[] = '';
			$newyear = $year;
			$week = 0;
			$day = 0;
			$mo = 1;
			$i = 1;
		
			while ($week != 1) {
				$day++;
				$week = date("w", mktime(0, 0, 0, $mo,$day, $year));
			}
		
			$monday =  strtotime(date("r", mktime(0, 0, 0, $mo,$day, $year)));
			$friday =  strtotime(date("r", mktime(0, 0, 0, $mo,$day, $year)) . "+4 days");
			$range = date('d.m', $monday)." - ".date('d.m', $friday);
			array_push($mondays,$range);
		
			while ($newyear == $year) {
				$monday =  strtotime(date("r", mktime(0, 0, 0, $mo,$day, $year))."+$i week");
				$friday =  strtotime(date("r", mktime(0, 0, 0, $mo,$day, $year)) . "+$i week 4 days");
				$range = date('d.m', $monday)." - ".date('d.m', $friday);
				if ($year == date("Y",$monday))  array_push($mondays,$range);
				$newyear = date("Y", $monday);
				$i++;
			}
		
			unset($mondays[0]);
			unset($mondays[53]);
			
			return $mondays;
		}
		
		
		/**
		 * Get month number from week
		 * @param integer $week year week
		 * @param integer $year four digits
		 * @return array 
		 */
		public static function month_number_from_week($week, $year) {
			return date('n', mktime(0, 0, 0, 1, (($week-1)*7)+1, $year));
		}
		
		
		/**
		 * Build months range
		 * and set in each month weeks wange for working days (Monday-Friday)
		 * @param integer $year four digits
		 * @return array
		 */
		public static function month_weeks($year) {
			
			$mondays = self::week_ranges($year);
			
			for ($i = 1; $i <= count($mondays); $i++) {
				$month = self::month_number_from_week($i, $year);
				$months[$month][$i] = $mondays[$i];
			}
			return $months;
		}

		
		/**
		 * Get month name in English
		 * @param integer $month
		 * @return string
		 */
		public static function month_name($month) {
			return date( 'F', mktime(0, 0, 0, $month, 1, date('Y')));
		}
	}