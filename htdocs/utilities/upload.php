<?php 

	class Upload {
		
		public static  function file($id, $path) {
			
			if ($_FILES[$id]['size']) {

				if (!file_exists($_SERVER['DOCUMENT_ROOT'].$path)) {
					dir::make($path);
				}
				
				$file = $_FILES[$id]['tmp_name'];
				$filename = File::getSecuencedName($path.'/'.$_FILES[$id]['name'], true);
				
				$path = $_SERVER['DOCUMENT_ROOT'].$path;
				
				$upload = move_uploaded_file($file, $path."/".$filename);
				chmod($path."/".$filename, 0666);
			}
			
			return ($upload) ? $path : false;
		}
		
		public static function tmp($id) {
			
			if (!$_FILES[$id]['size']) {
				return false;
			}
				
			$tmp = '/public/data/tmp/';
			dir::make('/public/data/tmp');

			$file = $_FILES[$id]['tmp_name'];
			
			// file name
			$filename = $_FILES[$id]['name'];
			$filename = File::normalize($filename);
			
			// tmp file
			$tmpfile = $tmp.$filename;
			$filename = File::getSecuencedName($tmpfile, true);
			
			$prefix = Session::get('user_id');
			$path = $tmp.$prefix.$filename;
			$newfile = $_SERVER['DOCUMENT_ROOT'].$path;
			
			$upload = @move_uploaded_file($file, $newfile); 

			if (file_exists($newfile)) {
				@chmod($newfile, 0666);
				return $path;
			}
		}

		public static function move($file, $path) {

			$docroot = substr($path, 0, strlen($_SERVER['DOCUMENT_ROOT'])) === $_SERVER['DOCUMENT_ROOT'] ? true : false;

			// extnded dirname whith document root if no exist
			$file = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], null, $file);
			$path = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], null, rtrim($path, '/'));

			// pathinfo
			$pathinfo = pathinfo($file);
			$dirname = $pathinfo['dirname'];
			$basename = $pathinfo['basename'];
			$extension = $pathinfo['extension'];

			// user id
			$prefix = Session::get('user_id');

			// remove user prefix id from temporary filename
			if ($prefix && substr($basename, 0, strlen($prefix)) === $prefix ) {
				$basename = substr($basename, strlen($prefix));
				$pathinfo['basename'] = $basename;
				$pathinfo['filename'] = basename($basename, ".$extension");
			}

			$newfile = $path.'/'.$basename;
			$newfile = File::getSecuencedName($newfile);
			
			if (file_exists($file)) {

				// create dir
				dir::make($path);
				
				$copy = copy($file, $newfile);
				
				// make file writable
				if (file_exists($newfile)) {
					@chmod($newfile, 0666);
				}
				
				// remove temporary file
				@chmod($file, 0666);
				@unlink($file);
			}

			// return path
			if ($copy) {

				$ext = pathinfo($newfile, PATHINFO_EXTENSION);
				
				// Remove EXIF data from JPG 
				if (exif_imagetype($newfile)==IMAGETYPE_JPEG) {
					$img = imagecreatefromjpeg($newfile);
					imagejpeg ($img, $newfile, 100);
					imagedestroy ($img);
				}

				return $docroot ? $newfile : str_replace($_SERVER['DOCUMENT_ROOT'], null, $newfile);
			}
		}
		
		public static function moveTemporaryFile($file, $dir) {
			$copy = 0;
			// remove server name from temporary file
			$file = str_replace('//'.$_SERVER['SERVER_NAME'], '', $file);
			$filename = basename($file);
				
			// temporary
			$tmpdir = file::dirpath($file);
			$tmpfile = $_SERVER['DOCUMENT_ROOT'].$file;
			$tmpthumb = $_SERVER['DOCUMENT_ROOT']."$tmpdir/thumbnail/$filename";
				
			// target file
			$target = $_SERVER['DOCUMENT_ROOT']."$dir/$filename";
			
			if (file_exists($tmpfile)) { 
					
				// create dir if not exist
				if (!file_exists($_SERVER['DOCUMENT_ROOT'].$dir)) {
					dir::make($dir);
				}
					
				// copy temporary file
				$copy = copy($tmpfile, $target);
			
				if ($copy) {
						
					// thumbnail
					if (file_exists($tmpthumb)) {
			
						$thumbdir = "$dir/thumbnail";
						$thumbtarget = $_SERVER['DOCUMENT_ROOT']."$thumbdir/$filename";
			
						// create thumb dir if not exist
						if (!file_exists($_SERVER['DOCUMENT_ROOT'].$thumbdir)) {
							dir::make($thumbdir);
						}
			
						copy($tmpthumb, $thumbtarget);
			
						// set file permissions
						@chmod($tmpthumb, 0666);
						@chmod($thumbtarget, 0666);
							
						// remove tmp file
						@unlink($tmpthumb);
					}
						
					// set file permissions
					@chmod($tmpfile, 0666);
					@chmod($target, 0666);
			
					// remove tmp file
					@unlink($tmpfile);
				}
			}
			
			return $copy;
		}
		
		
		public static function get($dataloader, $application=null) {
			
			if (is_array($dataloader)) {
				
				$url = '//'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
					
				foreach ($dataloader as $row) {
				
					if (file_exists($_SERVER['DOCUMENT_ROOT'].$row['file'])) {
							
						$key = $row['id'];
						$file = $row['file'];
						$filename = basename($file);
						$dirpath = file::dirpath($file);
						$thumbpath = "$dirpath/thumbnail/$filename";
							
						if (file_exists($_SERVER['DOCUMENT_ROOT'].$thumbpath)) {
							$thumbnail = '//'.$_SERVER['SERVER_NAME'].$thumbpath;
							$gallery = true;
						} else {
				
							$ext = file::extension($filename);
							$thumbpath = "/public/images/icons/file/48px/$ext.png";
							$thumbblank = "/public/images/icons/file/48px/_blank.png";
				
							$thumbnail = file_exists($_SERVER['DOCUMENT_ROOT'].$thumbpath)
							? '//'.$_SERVER['SERVER_NAME'].$thumbpath
							: '//'.$_SERVER['SERVER_NAME'].$thumbblank;
				
							$gallery = false;
						}
							
							
						$obj = new stdClass;

						foreach($row as $field_name=>$value)
						{
							$obj->$field_name = $value;
						}

						$obj->id = $key;
						$obj->deleteType = "POST";
						$obj->deleteUrl = "$url?id=$key&section=delete&application=$application";
						$obj->name = $filename;
						$obj->gallery = $gallery;
						$obj->size = filesize($_SERVER['DOCUMENT_ROOT'].$file);
						$obj->thumbnailUrl = $thumbnail;
						$obj->url = '//'.$_SERVER['SERVER_NAME'].$file;
						$obj->title = $row['title'];
						$obj->description = $row['description'];
							
						$response['files'][] = $obj;
					}
				}
				
				return $response;
			}
		}
		
	}