<?php


class Query {

	/**
	 * Build SQL insert query string
	 * 
	 * @param string $table db table name
	 * @param array $data fields
	 * @return string
	 */
	public static function insert($table, $data) {
		
		$fields = array_keys($data);
		$flags = $fields;
		
		array_walk($flags, function(&$value){
			$value = ":$value";
		});

		$fields = join(', ', $fields);
		$flags= join(', ',$flags);
		
		return "INSERT INTO $table ( $fields ) VALUES ( $flags )";
	}
	
	/**
	 * Build SQL update query string
	 *
	 * @param string $table db table name
	 * @param string $entity causale entity field name
	 * @param array $data fields
	 * @return string
	 */
	public static function update($table, $entity, $data) {
		
		foreach ($data as $key => $value) {
			$fields[] = "$key = :$key";
		}

		$fields = join(', ', $fields);
		
		return "UPDATE $table SET $fields WHERE $entity = :$entity";
	}

	/**
	 * Build SQL read te query string
	 *
	 * @param string $table db table name
	 * @param string $entity causale entity field name
	 * @return string
	 */
	public static function read($table, $entity) {
		
		return "
			SELECT * 
			FROM $table
			WHERE $entity = :primaryKey
		";
	}

	/**
	 * Build SQL delete query string
	 *
	 * @param string $table db table name
	 * @param string $entity causale entity field name
	 * @return string
	 */
	public static function delete($table, $entity) {
		
		return "
			DELETE FROM	$table 
			WHERE $entity = :primaryKey
		";
	}
}