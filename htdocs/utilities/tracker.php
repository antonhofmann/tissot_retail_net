<?php

class Tracker {

	const ACTION_INSERT = 1;
	const ACTION_UPDATE = 2;
	const ACTION_DELETE = 3;

	private $db;

	private $entities = array();

	public function __construct($application=null) {
		$model = new Model($application);
		$this->db = $model->db;
	}

	public function setEntity($entity) {

		$this->entities = array();

		$sth = $this->db->prepare("
			SELECT 
				tracking_type_id AS id,
				tracking_type_action_id AS action
			FROM db_retailnet.tracking_types
			INNER JOIN db_retailnet.tracking_maps ON tracking_map_type_id = tracking_type_id
			WHERE tracking_map_name = ?
		");

		$sth->execute(array($entity));
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$action = $row['action'];
				$this->entities[$action] = $row['id'];
			}
		}
	}

	public function onCreate($message, $entityName=null, $entityValue=null) {

		$action = $this->entities[self::ACTION_INSERT];
		
		if ($action) {
			return $this->track($action, $message, $entityName, $entityValue);
		}
	}

	public function onChange($message, $entityName=null, $entityValue=null) {

		$action = $this->entities[self::ACTION_UPDATE];
		
		if ($action) {
			return $this->track($action, $message, $entityName, $entityValue);
		}
	}

	public function onRemove($message, $entityName=null, $entityValue=null) {

		$action = $this->entities[self::ACTION_DELETE];
		
		if ($action) {
			return $this->track($action, $message, $entityName, $entityValue);
		}
	}


	public function track($action, $message, $entityName=null, $entityValue=null) {

		$user = User::instance()->id;

		$sth = $this->db->prepare("
			INSERT INTO user_tracks (
				user_track_type_id,
				user_track_user_id,
				user_track_entity,
				user_track_entity_id,
				user_track_message
			)
			VALUES (?, ?, ?, ?, ?)
		");

		return $sth->execute(array($action, $user, $entityName, $entityValue, $message));
	}

}