<?php 

/**
 * Action Mail
 *
 * @version 2.0
 * @category MailModule
 * @author admir.serifi@mediaparx.ch
 * @license Mediaparx AG
 *
 * @todo Eliminate all deprected functions
 *
 * New on version 2.0
 * - Add recipient class
 * - Add getter and setter
 * - Bether method names
 * - Add group mails
 */
class ActionMail {

	/**
	 * Mail Settings
	 * 
	 * @var array
	 */
	protected $settings = array();

	/**
	 * Class consoling
	 * @var array
	 */
	protected $console = array();

	/**
	 * Response Data
	 *
	 * @return array
	 */
	protected $response = array();

	/**
	 * All action mail process are finished
	 * 
	 * @var boolean
	 */
	private $processed;
	
	/**
	 * Template keyword
	 * 
	 * @var string
	 */
	protected $template;

	/**
	 * Externe Parameters
	 * 
	 * @var array
	 */
	protected $params = array();
	
	/**
	 * Action File
	 * 
	 * @var string
	 */
	protected $actionfile = array();

	/**
	 * Global CC recipients
	 * @var array
	 */
	protected $ccRecipients = array();
	
	/**
	 * Sender
	 * 
	 * @var stdClass
	 */
	protected $sender;
	
	/**
	 * Permitted Roles
	 *
	 * @var array
	 */
	protected $recipientsRoles = array();	

	/**
	 * Permitted CC Roles
	 *
	 * @var array
	 */
	protected $recipientsCCroles = array();
	
	/**
	 * Dataloader
	 * 
	 * @var array
	 */
	protected $dataloader = array();
	
	/**
	 * Mail Attachemnts
	 * 
	 * @var array
	 */
	protected $attachments = array();

	/**
	 * Mail tracker
	 * @var Mail_Tracker
	 */
	protected $tracker;

	/**
	 * Template Contents
	 *
	 * @return array
	 */
	protected $contents = array();

	/**
	 * Mail recipients
	 * 
	 * @var array
	 */
	protected $recipients = array();	

	/**
	 * Standard Mail recipients
	 * 
	 * @var array
	 */
	protected $standardRecipients = array();	

	/**
	 * Mail recipients
	 * 
	 * @var array
	 */
	protected $groups = array();

	/**
	 * Action Mail version
	 * @var float
	 */
	protected static $version = 2.0;

	
	public function __construct($template, $sendmail=true) {
		
		$this->processed = false;
		$this->sender = new stdClass();
		
		if ($template) {
			$this->setTemplate($template);
		}

		if (!$sendmail) {
			$this->setDebugger(true);
		}
	}

	public function getVersion() {
		return $this->version;
	}

	public function getTemplateId() {
		return $this->template;
	}

	public function isDebuggingMode() {
		return $this->getSetting('debugger') ? true : false;
	}

	public function setDebugger($param=true) {
		$this->setSetting('debugger', $param);
	}

	public function console($message) {
		$this->console[] = $message;
		return $this;
	}

	public function getConsole() {
		return $this->console ?: array();
	}

	public function getResponse($name=null) {
		return $name && $this->response[$name] ? $this->response[$name] : $this->response;
	}

	public function setResponse($name, $value) {
		$this->response[$name] = $value;
		return $this;
	}

	public function isSuccess() {
		return $this->getResponse('success');
	}

	public function getTotalSentMails() {
		$total = $this->getResponse('totalmails');
		return $total ?: 0;
	}

	public function canTrackMails() {
		return  $this->getSetting('track');
	}

	public function canSendCCmails() {
		return  $this->getSetting('cc');
	}

	public function canSendBCCmails() {
		return  $this->getSetting('bcc');
	}

	public function getDevMail() {
		return $this->getParam('devemail');
	}

	public function setDevMail($email) {
		
		if (!check::email($email)) return; 

		$this->setParam('devemail', $email);
		$this->console("Add dev mail $email");
		
		return $this;
	}

	public function getTestMail() {
		return $this->getParam('testmail');
	}

	public function setTestMail($email) {
		
		if (!check::email($email)) return; 

		$this->setParam('testmail', $email);
		$this->console("Add test mail $email");

		$user = new User('');
		$data = $user->getActiveUserFromEmail($email);

		if ($data['user_id']) {
			$this->setSender($data['user_id']);
		}
		
		return $this;
	}

	public function getParams() {
		return $this->params;
	}

	public function getParam($name) {
		return $this->params[$name];
	}

	public function setParam($name, $value) {
		$this->params[$name] = $value;
		$this->console("Param $name: $value");
		return $this;
	}

	public function getSettings() {
		return $this->settings;
	}

	public function getSetting($name) {
		return $this->settings[$name];
	}

	public function setSetting($name, $value) {
		$this->settings[$name] = $value;
		$this->console("Setting $name: $value");
		return $this;
	}

	public function getActionFile() {
		return $this->actionfile;
	}

	public function addActionFile($file) {

		$file = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], '', $file);

		if (file_exists($file)) {
			$this->actionfile = $file;
			$this->setSetting('actionfile', true);
			$this->console("Add action file $file");
		}

		return $this;
	}

	public function checkRole($role, $cc=false) {

		$roles = $cc ? $this->roles['cc'] : $this->roles['recipient'];

		if (!$roles) return;

		return in_array($role, $roles);
	}

	public function checkRoles($roles, $cc=false) {

		$tplRoles = $cc ? $this->roles['cc'] : $this->roles['recipient'];

		if (!$tplRoles || !is_array($roles)) return;

		foreach ($roles as $role) {
			if (in_array($role, $tplRoles)) {
				return true;
				break;
			}
		}
	}

	public function getRoles($cc=false) {

		if (!$this->roles) return array();

		return $cc ? $this->roles['cc'] : $this->roles['recipient'];
	}

	public function setRoles($roles, $cc=false) {

		if (!is_array($roles)) return;

		foreach ($roles as $role => $vale) {
		 	$this->addRole($role, $cc);
		 } 

		return $this;
	}

	public function addRole($role, $cc=false) {
		
		if ($cc) {
			$this->roles['cc'][$role] = $role;
			$this->console("Add CC recipient role $role");
		}
		else {
			$this->roles['recipient'][$role] = $role;
			$this->console("Add recipient role $role");
		}
		
		return $this;
	}

	public function getSubject() {
		return $this->contents['subject'];
	}

	public function setSubject($content) {
		$this->contents['subject'] = $content;
		return $this;
	}

	public function getBody() {
		return $this->contents['body'];
	}

	public function setBody($content) {
		$this->contents['body'] = $content;
		return $this;
	}

	public function getFooter() {
		return $this->contents['footer'];
	}

	public function setFooter($content) {
		$this->contents['footer'] = $content;
	}

	public function getSignature() {
		return $this->contents['signature'];
	}

	public function setSignature($content) {
		$this->contents['signature'] = $content;
		return $this;
	}

	public function getData($name) {
		return $this->dataloader[$name];
	}

	public function setData($name, $value) {
		$this->dataloader[$name] = $value;
		return $this;
	}

	public function getDataloader() {
		return $this->dataloader ?: array();
	}

	public function setDataloader(array $data) {

		if (is_array($data)) {
			$this->dataloader = array_merge($this->dataloader, $data);
		}

		return $this;
	}

	public function getSender() {
		return $this->sender;
	}	
	
	public function setSender($id) {

		$user = $id instanceOf User ? $id : new User($id);
			
		if (!$user->id) {
			$this->console("Sender not found");
			return;
		}

		$this->sender->id = $id;
			
		foreach ($user->data as $key => $value) {
			
			// sender object
			$field = substr($key, 5);
			$this->sender->$field = $value;
			
			// dataloader
			$key = str_replace('user_', 'sender_', $key);
			$this->setData($key, $value);
		}

		$name = $user->firstname.' '.$user->name;
		$email = $user->email;
		$this->console("Sender $name ($email)");

		return $this;
	}

	public function hasRecipients() {
		return $this->recipients ? true : false;
	}

	public function isRecipient() {
		return $this->recipients[$id] ? true : false;
	}

	public function getFirstRecipient() {
		
		if (!$this->recipients) return;

		reset($this->recipients);
		
		$key = key($this->recipients);
		
		return $this->recipients[$key];
	}

	public function getRecipient($id) {
		return $this->recipients[$id];
	}

	public function addRecipient($id, array $data=null) {
		
		$id = $id instanceof User ? $id->id : $id;

		if ($this->recipients[$id] instanceof ActionMailRecipient) {
			return $this->recipients[$id];
		}
		
		$recipient = new ActionMailRecipient($id);

		$this->console("Add recipient $id");

		if ($data) {
			
			$recipient->setDataloader($data);

			if ($data['user_id']) {
				$recipient->id = $data['user_id'];
			}

			if ($data['user_email']) {
				$recipient->setEmail($data['user_email']);
			}

			if ($data['user_firstname']) {
				$recipient->setName($data['user_firstname'].' '.$data['user_name']);
			}

			$this->console("Update recipient data ".$recipient->id);
		}

		return $this->recipients[$id] = $recipient;
	}

	public function getRecipients() {
		return $this->recipients;
	}

	public function isStandardRecipient($id) {
		return in_array($id, $this->standardRecipients) && $this->recipients[$id] instanceof ActionMailRecipient ? true : false;
	}

	public function getStandardRecipients() {
		
		if (!$this->standardRecipients) return;

		$recipients = array();

		foreach ($this->standardRecipients as $id) {
			if ($this->recipients[$id] instanceof ActionMailRecipient) {
				$recipients[$id] = $this->recipients[$id];
			}
		}

		return $recipients;
	}

	public function getCCRecipients() {
		return array_filter(array_unique($this->ccRecipients));
	}
	
	public function addCCrecipient($email, $id=null) {

		if ($email and preg_match('/^\d+$/', $email)) {
			$user = new User($email);
			$user->read($email);
			$email = $user->email;
		}

		if (!check::email($email)) {
			$this->console("Email $email is not valid");
			return;
		}

		if ($id) {
			
			$recipient = $this->getRecipient($id);
			
			if ($recipient) {
				$recipient->addCCrecipient($email);
				$_name = $recipient->getName();
				$_email = $recipient->getEmail();
				$this->console("Add CC recipient $email for recipient $_name ($_email)");
			}
		}
		elseif (!in_array($email, $this->ccRecipients)) {
			$this->ccRecipients[] = $email;
			$this->console("Add CC recipient $email");
		}

		return $this;
	}

	public function addCCrecipients($cc, $recipient=null) {

		if (!is_array($cc)) {
			$this->addCCRecipient($cc, $recipient);
			return $this;
		}

		$user = new User();

		foreach ($cc as $value) {
			
			// user instance
			if ($value instanceof User) {
				$this->addCCRecipient($value->email, $recipient);
			}
			// user id
			else if (preg_match('/^\d+$/', $value)) {
				$user->read($value);
				$this->addCCRecipient($user->email, $recipient);
			} 
			// email address
			else {
				$this->addCCRecipient($value, $recipient);
			}
		}

		return $this;
	}
	
	public function addBCCrecipient($email, $id=null) {

		if (preg_match('/^\d+$/', $email)) {
			$user = new User($email);
			$user->read($email);
			$email = $user->email;
		}

		if (!check::email($email)) {
			$this->console("Email $email is not valid");
			return;
		}
			
		if ($id) {
			
			$recipient = $this->getRecipient($id);
			
			if ($recipient) {
				$recipient->addBCCrecipient($email);
				$_name = $recipient->getName();
				$_email = $recipient->getEmail();
				$this->console("Add BCC recipient $email for recipient $_name ($_email)");
			}
		}
		elseif (!in_array($email, $this->ccRecipients)) {
			$this->bccRecipients[] = $email;
			$this->console("Add BCC recipient $email");
		}

		return $this;
	}

	public function addBCCrecipients($bcc, $recipient=null) {

		if (!is_array($bcc)) {
			$this->addBCCrecipient($bcc, $recipient, $checkAccess=true);
			return $this;
		}

		$user = new User();

		foreach ($bcc as $value) {
			
			if ($value instanceof User) {
				$this->addBCCrecipient($value->email, $recipient);
			}
			else if (preg_match('/^\d+$/', $value)) {
				$user->read($value);
				$this->addBCCrecipient($user->email, $recipient);
			} 
			else {
				$this->addBCCrecipient($value, $recipient);
			}
		}

		return $this;
	}

	public function removeCCrecipient($email) {
		
		if (!preg_match('/^\d+$/', $value)) {
			$user = new User();
			$data = $user->getActiveUserFromEmail($email);
			$email = $data['user_email'];
		}

		if ($this->ccRecipients && in_array($email, $this->ccRecipients)) {
			
			$key = array_search($email, $this->ccRecipients);
			unset($this->ccRecipients[$key]);

			$this->console("Remove CC recipient $email");
		}

		return $this;
	}

	public function removeAllccRecipients() {
		$this->ccRecipients = array();
		$this->console("Remove all CC recipients");
		return $this;
	}

	public function addGroup($id) {

		if (!$this->groups[$id]) {

			$_DEV_MAIL = $this->getDevMail();
			$_DEBUGGING_MODE = $this->isDebuggingMode();
			$_ALLOW_CC_RECIPIENTS = $this->getSetting('cc');
			$_ALLOW_BCC_RECIPIENTS = $this->getSetting('bcc');
			
			$group = new ActionMailGroup($id);

			// set mail in development
			if ($_DEV_MAIL) {
				$group->setTestMail($_DEV_MAIL);
			}

			// activate debugger mode
			if ($_DEBUGGING_MODE) {
				$group->setDebugger(true);
			}

			// set subject content
			$subject = $this->getSubject();
			$group->setSubject($subject);

			// salutation
			$salutation = $this->getSetting('group_salutation');
			$group->setSalutation($salutation);

			// set body content
			$body = $this->getBody();
			$group->setBody($body);

			// set footer
			$footer = $this->getFooter();
			$group->setFooter($footer);
			
			$signature = $this->getSignature();
			$group->setSignature($signature);

			// global cc recipients
			if ($_ALLOW_CC_RECIPIENTS && $this->ccRecipients) {
				$group->addCCRecipients($this->ccRecipients);
			}

			// global bcc recipients
			if ($_ALLOW_BCC_RECIPIENTS && $this->bccRecipients) {
				$group->addBCCRecipients($this->bccRecipients);
			}

			// attachments
			if ($this->attachments) {
				foreach ($this->attachments as $file) {
					$group->addAttachment($file);
				}
			}

			// sender
			$sender = $this->getSender();
			$group->setSender($sender->id);

			$this->groups[$id] = $group;
		}

		return $this->groups[$id];
	}

	public function getGroup($id) {
		return $this->groups[$id];
	}


	public function removeGroup($id) {
		
		if ($this->groups[$id]) {
			unset($this->groups[$id]);
		}

		return $this;
	}

	public function addAttachment($file, $id=null) {
		
	 	$file = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], '', $file);

	 	if (!file_exists($file)) return;

		if ($id && $this->isRecipient($id)) {
			$recipient = $this->getRecipient($id);
			$recipient->addAttachment($file);
			$name = $recipient->getName();
			$email = $recipient->getEmail();
			$this->console("Add file attachment $file for recipient $name ($email)");
		} else {
			$this->attachments[] = $file;
			$this->console("Add file attachment $file");
		}

		return $this;
	}
	
	public function send($groupmail = false) { 

		if(!isset($this->sender->id))
		{
			$auth = User::instance();

			if (!$auth->id) {
				$this->console("Session timeout");
				return;
			}
		}

		if ($this->processed) return;

		if ($this->getSetting('actionfile')) {
			$this->executeActionFile();
		}

		if ($this->processed) return;

		return $this->populate(true, $groupmail);
	}

	public function executeActionFile() {

		$this->console("START ACTION FILE");

		$actionFile = $this->getActionFile();

		if (!$actionFile || !file_exists($actionFile)) {
			$this->console("Action file not found");
			return;
		}

		if ($this->getSetting('actionFileExecuted')) {
			$this->console("Operation cancelled: file $actionFile is executed.");
			return;
		}
			
		ob_start();
		
		require $actionFile;
		
		$response = ob_get_contents();
		
		ob_end_clean();

		$this->console("END ACTION FILE");

		if ($response) {

			if (check::json($response)) {

				$messages = json_decode($response, true);

				if (is_array($messages)) {
					foreach ($messages as $message) {
						$this->console($message);
					}
				} else {
					$message = sprintf('%s', $response);
					$this->console($message);
				}

			} else {
				$message = sprintf('%s', $response);
				$this->console($message);
			}
		}

		$this->setSetting('actionFileExecuted', true);

		return $this;
	}
	

	public function populate($sendmail=false, $groupmail = false) {

		if(!isset($this->sender->id))
		{
			$auth = User::instance();

			if (!$auth->id) {
				$this->console("Session timeout");
				return;
			}
		}

		$this->console("START POPULATE");

		// recipients
		$recipients = $this->recipients;

		if (!$recipients) {
			$this->console("No recipients found");
			$this->console("END POPULATE");
			return;
		}

		// group emails only in case with multiple recipients
		if (count($recipients)==1) {
			$this->setSetting('group', false);
		}

		// settings
		$_ALLOW_CC_RECIPIENTS = $this->getSetting('cc');
		$_ALLOW_BCC_RECIPIENTS = $this->getSetting('bcc');
		$_ALLOW_MAIL_TRACK = $this->getSetting('track');
		$_DEV_MAIL = $this->getDevMail();
		$_DEBUGGING_MODE = $this->isDebuggingMode();

		// content
		$_MAIL_SUBJECT = $this->getSubject();
		$_MAIL_BODY = $this->getBody();
		$_MAIL_FOOTER = $this->getFooter();
		$_MAIL_SIGNATURE = $this->getSignature();

		// popullate
		foreach ($recipients as $id => $recipient) {
			
			// dataloader
			$recipient->setDataloader($this->dataloader);

			// cc recipients
			if ($_ALLOW_CC_RECIPIENTS) {

				// recipient cc
				$ccEmail = $recipient->getData('user_email_cc');
				$recipient->addCCrecipient($ccEmail);

				// recipient deputy
				$ccEmail = $recipient->getData('user_email_deputy');
				$recipient->addCCrecipient($ccEmail);

				// global cc recipients
				if ($this->ccRecipients) {
					$recipient->addCCrecipients($this->ccRecipients);
				}
			}

			// global bcc recipients
			if ($_ALLOW_BCC_RECIPIENTS && $this->bccRecipients) {
				$recipient->addBCCrecipients($this->bccRecipients);
			}

			// set content
			$recipient->setSubject($_MAIL_SUBJECT);
			$recipient->setBody($_MAIL_BODY);
			$recipient->setFooter($_MAIL_FOOTER);
			$recipient->setSignature($_MAIL_SIGNATURE);

			// set test mail
			if ($_DEV_MAIL) {
				$recipient->setTestMail($_DEV_MAIL);
			}

			if ($_DEBUGGING_MODE) {
				$recipient->setDebugger($_DEBUGGING_MODE);
			}

			// attachments
			if ($this->attachments) {
				foreach ($this->attachments as $file) {
					$recipient->addAttachment($file);
				}
			}

			// sender
			$sender = $this->getSender();
			
			if ($sender->id && !$recipient->getSender()) {
				$recipient->setSender($sender->id);
			}
		}

		// group mails
		if ($this->getSetting('group')) {
			$this->groupByCompany();
		}

		if ($sendmail) {
			if ($this->getSetting('group')) {
				$this->sendMailGroups();
			}
			elseif($groupmail and count($recipients) > 1) {
				$this->groupRecipients();
				$this->setSetting('group', true);
				$this->sendMailGroups();
			}
			else {
				$this->sendMailRecipients();
			}
		}

		$this->console("END POPULATE");

		return $this;
	}

	private function sendMailGroups() {
		
		$this->console("START SENDMAIL GROUPS");

		if (!$this->getSetting('group')) {
			$this->console("Error: group mails is not activated, cannot send group mails");
			return;
		}

		$groups = $this->groups;

		if (!$groups) {
			$this->console("Error: group mails not found");
			return;
		}
		
		$_DEBUGGING_MODE = $this->isDebuggingMode();
		$_TEST_MAIL = $this->getTestMail();

		$totalMails = 0;

		foreach ($groups as $i => $group) {

			if (!$group instanceOf ActionMailGroup) continue;

			// send only one mail when test mail is activated
			if ($_TEST_MAIL && $totalMails>0) {
				$this->console("TESTMAIL: ignore group mail $i");
				continue;
			}

			if ($_DEBUGGING_MODE) {
				$group->setDebugger($_DEBUGGING_MODE);
			}

			if ($_TEST_MAIL) {
				$group->setTestMail($_TEST_MAIL);
			}

			$response = $group->send();

			if ($response) {
				
				$totalMails++;

				$recipients = $group->getRecipients();
				
				if ($recipients && $this->getSetting('track')) {

					$content = $group->getContent();

					foreach ($recipients as $i => $recipient) {
						$recipient->setContent($content);
						$this->track($recipient);
					}
				}
				
				$entity = $_DEBUGGING_MODE ? 'DEBUGGER' : 'SENDMAIL';
				$this->console("$entity: succesfully sent mail an group $i");
			}
			else {
				
				$this->console("SENDMAIL: error sent mail an group $i");

				$console = $group->getConsole();

				if ($console) {
					foreach ($console as $message) {
						$this->console($message);
					}
				}
			}
		}

		// response success
		$success = count($groups)==$totalMails ? true : false;
		$this->setResponse('success', $success);
		$this->setResponse('group', true);
		$this->setResponse('debugging', $_DEBUGGING_MODE);
		$this->setResponse('totalmails', $totalMails);

		$this->console("Total sent $totalMails mails");
		$this->console("END SENDMAIL GROUPS");

		return $this->processed = true;
	}

	private function sendMailRecipients() {

		$this->console("START SENDMAIL RECIPIENTS");

		if ($this->getSetting('group')) {
			$this->console("Error: Group mails is active, can not send recipient mails");
			return;
		}

		$recipients = $this->recipients;

		if (!$recipients) {
			$this->console("Error: recipients not found");
			return;
		}

		$_DEBUGGING_MODE = $this->isDebuggingMode();
		$_TEST_MAIL = $this->getTestMail();

		$totalMails = 0;

		foreach ($recipients as $id => $recipient) {

			// send only one mail when test mail is activated
			if ($_TEST_MAIL && $totalMails>0) {
				$this->console("TESTMAIL: ignore recipient mail $id");
				continue;
			}

			$name = $recipient->getName();
			$email = $recipient->getSendMailAddress();

			if ($_DEBUGGING_MODE) {
				$recipient->setDebugger($_DEBUGGING_MODE);
			}

			if ($_TEST_MAIL) {
				$recipient->setTestMail($_TEST_MAIL);
			}
			
			$response = $recipient->send();

			if ($response) {
				
				$totalMails++;
				
				$this->track($recipient);
				
				$entity = $_DEBUGGING_MODE ? 'DEBUGGER' : 'SENDMAIL';
				$this->console("$entity: succesfully sent mail an $name ($email)");
			}
			else {

				$this->console("SENDMAIL: error sent mail an $name ($email)");

				$console = $recipient->getConsole();

				if ($console) {
					foreach ($console as $message) {
						$this->console($message);
					}
				}
			}
		}

		// response success
		$success = count($recipients)==$totalMails ? true : false;
		$this->setResponse('success', $success);
		$this->setResponse('debugging', $_DEBUGGING_MODE);
		$this->setResponse('totalmails', $totalMails);

		$this->console("Total sent $totalMails mails");
		$this->console("END SENDMAIL RECIPIENTS");

		return $this->processed = true;
	}

	
	

	protected function groupRecipients() {

		$this->console("Start grouping recipients");

		// manually created groups
		if ($this->groups) {
			$this->console("Mail groups allready exist. Cancell group mails by company.");
			return;
		}

		// recipients
		$recipients = $this->recipients;

		if (!$recipients) {
			$this->console("No recipients found");
			return;
		}

		// group emails only in case with multiple recipients
		if (count($recipients)==1) {
			$this->setSetting('group', false);
			$this->console("It's only one recipient. Cancel group mails by companis.");
			return;
		}

	
		$group = $this->addGroup(13);

							
		// group dataloader
		$company = new Company();
		$company->read(13);

		$group->setDataloader($this->dataloader);
		$group->setDataloader($company->data);

		

		foreach ($recipients as $i => $recipient) {
			$group->addRecipient($recipient);
		}

		$this->console("End group mails by company");
	}
	
	protected function groupByCompany() {

		if (!$this->getSetting('group'))  return;

		$this->console("Start group mails by company");

		// manually created groups
		if ($this->groups) {
			$this->console("Mail groups allready exist. Cancell group mails by company.");
			return;
		}

		// recipients
		$recipients = $this->recipients;

		if (!$recipients) {
			$this->console("No recipients found");
			return;
		}

		// group emails only in case with multiple recipients
		if (count($recipients)==1) {
			$this->setSetting('group', false);
			$this->console("It's only one recipient. Cancel group mails by companis.");
			return;
		}

		// group recipients by companies
		foreach ($recipients as $id => $recipient) {
			$company = $recipient->getData('user_address') ?: 0;
			$companies[$company][$id] = $recipient;
		}

		$company = new Company();

		foreach ($companies as $id => $recipients) {
			
			$group = $this->addGroup($id);

						
			// group dataloader
			$company->read($id);
			$group->setDataloader($this->dataloader);
			$group->setDataloader($company->data);

			foreach ($recipients as $i => $recipient) {
				$group->addRecipient($recipient);
			}
		}

		$this->console("End group mails by company");
	}

	/**
	 * Get preview mail data
	 * Get only data from first permitted recipient
	 * @return array email data
	 */
	public function getMailData($id=null) {

		$this->console("PREVIEW MAIL");

		// load action file
		$this->executeActionFile();
		
		// pupulate recipients
		$this->populate(false);

		// group mode
		if ($this->getSetting('group')) {
			
			$group = $this->groups;
			$recipient = array_shift($group);

			if (!$recipient) {
				$this->console("Recipient not found");
				return;
			}

			$recipients = $recipient->getRecipients() ?: array();
			$email = $recipients ? join(', ', $recipients) : null;
			
		} else {
			
			$recipients = $this->recipients;
			$recipient = $id && $recipients[$id] ? $recipients[$id] : array_shift($recipients);

			if (!$recipient) {
				$this->console("Recipient not found");
				return;
			}

			$email = $recipient->getEmail();
		}

		if (!$recipient) return;

		$ccRecipients = $recipient->getCCrecipients();
		$ccRecipients = $ccRecipients ? join(', ', $ccRecipients) : null;
		
		return array(
			'recipient' => $recipient->getName(),
			'email' => $email,
			'cc' => $ccRecipients,
			'subject' => $recipient->getSubject(true),
			'content' => $recipient->getBody(true),
			'footer' => $recipient->getFooter(true),
			'date' => date('d.m.Y H:i:s'),
			'response' => true
		);
	}

	public function track($recipient) {

		if (!$this->getSetting('track')) {
			return;
		}

		if (!$recipient instanceof ActionMailRecipient) {
			$this->console("TRACK: recipient $recipient not found");
			return;
		}

		if (!$this->tracker) {
			$application = $this->getParam('application');
			$this->tracker = new Mail_Tracking($application);
		}

		$sender = $this->getSender();

		// additional recipients
		$ccRecipients = $recipient->getCCrecipients() ?: array();
		$bccRecipients = $recipient->getBCCrecipients() ?: array();
		$additionalRecipients = $ccRecipients+$bccRecipients;
		$additionalRecipients = $additionalRecipients ? serialize(array_unique($additionalRecipients)) : null;

		$name = $recipient->getName();
		$email = $recipient->getSendMailAddress();

		$response = $this->tracker->create(array(
			'mail_tracking_mail_template_id' => $this->template,
			'mail_tracking_sender_user_id' => $sender->id,
			'mail_tracking_recipient_user_id' => $recipient->id,
			'mail_tracking_subject' => $recipient->getSubject(true),
			'mail_tracking_content' => $recipient->getContent(),
			'mail_tracking_recipient_name' => $name,
			'mail_tracking_recipient_email' => $email,
			'mail_tracking_recipient_cc_email' => $recipient->getData('user_email_cc'),
			'mail_tracking_recipient_deputy_email' => $recipient->getData('user_email_deputy'),
			'mail_tracking_additional_recipient_emails' => $additionalRecipients,
			'date_created' => date('Y-m-d H:i:s'),
			'user_created' => User::instance()->login
		));

		if ($response) {
			$this->console("TRACK: succesfully mail track for recipient $name ($email)");
		}

		return $response;
	}

	public function setTemplate($id) {

		$tpl = new Mail_Template();
		$tpl->read(intval($id));
		
		if (!$tpl->id) {
			$tpl->read_from_shortcut($id);
		}

		if (!$tpl->id) {
			$this->console("Mail template $template not found");
			return;
		} else {
			$this->console("Pupullate from mail template $id");
		}
			
		$this->template = $tpl->id;
		
		// mail subject
		$content = $tpl->subject;
		$this->setSubject($content);
		
		// mail body
		$content = $tpl->text;
		$this->setBody($content);
		
		// mail footer
		$content = $tpl->footer;
		$this->setFooter($content);			
		
		// get standard recipients
		$standardRecipients = $tpl->recipient()->loadAll();
		
		if ($standardRecipients) {
			
			foreach ($standardRecipients as $row) {
				
				$id = $row['mail_template_recipient_user_id'];
				$cc = $row['mail_template_recipient_cc'];
				$this->standardRecipients[] = $id;
				
				if ($cc) $this->addCCrecipient($id);
				else $this->addRecipient($id);
			}
		}
		
		// standard sender
		$sender = $tpl->sender_id ? $tpl->sender_id : User::instance()->id;
		$this->setSender($sender);

		// template test mail
		if ($tpl->dev_email) {
			$this->setDevMail($tpl->dev_email);
		}

		// debugging mod
		$this->setDebugger($tpl->debugger);
			
		// template settings
		$this->setSetting('track', $tpl->track);
		$this->setSetting('cc', $tpl->cc);
		$this->setSetting('bcc', $tpl->bcc);
		
		// group mails throw roles
		if ($tpl->group) {
			$this->setSetting('group', $tpl->group);
			$this->setSetting('group_salutation', $tpl->group_salutation);
		}
		
		// action file
		if ($tpl->action_id) {
			
			$tplAction = new Mail_Template_Action();
			$tplAction->read($tpl->action_id);

			$file =$_SERVER['DOCUMENT_ROOT']."/actionmails/".$tplAction->file;
			$this->addActionFile($file);
		}

		// template roles
		$roles = $tpl->role()->loadAll();

		if ($roles['recipient']) {
			$this->setRoles($roles['recipient']);
		}

		if ($roles['cc']) {
			$this->setRoles($roles['cc'], true);
		}
		
		return $this;
	}
}