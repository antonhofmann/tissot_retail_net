<?php

	class file {

		public static function load($path) {
			$path = $_SERVER['DOCUMENT_ROOT'].$path;
			if (file_exists($path)) {
				$file = fopen($path, "r");
				$content = fread($file, filesize($path));
				fclose($file);
			}
			return $content;
		}

		public static function extension($file) {
			$file = basename(strtolower($file));
			$file = explode(".", $file);
			return end($file);
		}

		public static function validata($file) {
			return (preg_match('#^[a-z0-9._-]+\.[a-z]{2,4}$#i', basename($file))) ? true : false;
		}

		public static function show($path) {
			$extension = self::extension($path);
			$browser = $_SERVER['HTTP_USER_AGENT'];
			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Type: $extension");
			header("Content-Transfer-Encoding: binary");
			header("Content-Type: text/html");
			echo file_get_contents($path,true);
		}

		public static function name($file) {
			$file = strtolower(basename($file));
			$extension = self::extension($file);
			$count = strlen($extension)+1;
			return substr($file,0,-$count);
		}

		public static function isController($controller) { 
			return (file_exists(PATH_CONTROLLERS.$controller."_controller.php")) ? true : false;
		}


		/**
		 * File name validation
		 * @param unknown $file
		 * @return string
		 */
		public static function normalize($file) {
			return preg_replace("/[^a-zA-Z0-9.\/]/", "", strtolower($file));
		}

		/**
		 * Get sequenced filename
		 * @param string $file
		 * @param boolean $onlyBasename
		 * @return string
		 */
		public static function getSecuencedName($file, $onlyBasename=false) {
			
			$file = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], '', $file);
			
			$pathinfo = pathinfo($file);
			$dir = $pathinfo['dirname'];
			$basename = $pathinfo['basename'];
			$filename = $pathinfo['filename'];
			$extension = $pathinfo['extension'];

			$newpath = $dir.'/'.$basename;
			$newname = $basename;
			$counter = 0;

			while (file_exists($newpath)) {
				$newname = $filename .'_'. $counter .'.'. $extension;
				$newpath = $dir.'/'.$newname; 
				$counter++;
			}

			return $onlyBasename ? $newname : "$dir/$newname";
		}

		/**
		 * Get full dir path from file
		 * @param string $file
		 * @return dir path
		 */
		public static function dirpath($file) {
			
			$file = explode('/',$file);
			
			if (count($file)>0) {
				
				// remove file name part form path
				$filename = array_pop($file);
				
				return ($file) ? join('/',$file) : null;
			}
		}

		/**
		 *	Deletes a file
		 *  @param $file path (including doc root) to the file or directory
		 *  @return true if succesfully deleted, false if not
		 */
		public static function remove($file) {

			$file = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], '', $file);
			
			if (is_dir($file)) {
				return false;
			}

			if (file_exists($file)) {
				return @unlink($file);
			}
		}
	}
