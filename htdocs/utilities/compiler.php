<?php

class Compiler {

	/**
	 * Object instance
	 * 
	 * @return Compiler
	 */
	private static $instance;

	/**
	 * Public directory for compiled files
	 * 
	 * @var string
	 */
	protected $dir;
	
	/**
	 * File chmod
	 * 
	 * @var int
	 */
	protected $chmod = 0666;
	
	/**
	 * Error
	 * 
	 * @var boolean
	 */
	protected $error;
	
	/**
	 * Last changed attached files
	 * 
	 * @var int
	 */
	protected $stamp = 0;
	
	/**
	 * File sources
	 * 
	 * @var array
	 */
	protected $sources = array();

	protected $exports;
	
	
	/**
	 * Public directory for compiled files
	 * 
	 * @param string $path
	 * @return void
	 */
	public static function dir($path) {
		
		// make dir if not exist
		if (!file_exists($path)) {
			dir::make($path);
		}
		
		static::instance()->dir = $path;
	} 
	
	/**
	 * Set directory Chmod
	 * 
	 * @param int $mod
	 * @return void
	 */
	public static function chmod($mod) {
		
		static::instance()->chmod = $mod;
	} 
	
	/**
	 * Attach file to compilation process
	 * 
	 * @param array $source list of css files (width directory information)
	 * @return void
	 */
	public static function attach($source) { 
		
		if (is_array($source)) {
			
			foreach ($source as $file) {
				static::instance()->add($file);
			}
			
		} else {
			
			static::instance()->add($source);
		}
	}

	/**
	 * Reset compiler
	 * @return void
	 */
	public static function reset() {
		static::instance()->sources = array();
	}
	
	/**
	 * Get a copy of the sources
	 * This is so sources can be reset, then restored later on
	 * @return array
	 */
	public static function backup() {
		return static::instance()->sources;
	}

	/**
	 * Export Compiled Files
	 * 
	 * @param boolean $minify minify export files
	 * @return string
	 */
	public static function export($minify=true) {
		
		$sources = static::instance()->compilation($minify);
		
		// on error, return all sources
		if (static::instance()->error) {
			$sources = static::instance()->sources;
		}
		
		if ($sources) {
			
			foreach ($sources as $file) {
				
				$extension = File::extension($file);
				
				$file = str_replace($_SERVER['DOCUMENT_ROOT'], '', $file);
				
				if ($extension=='css') {
					static::$instance->exports['css'] = "\t<link rel=\"stylesheet\" type=\"text/css\" media=\"all\" charset=\"utf-8\" href=\"$file\" />\n";
				}
				
				if ($extension=='js') {
					static::$instance->exports['js'] = "\t<script type=\"text/javascript\" src=\"$file\" charset=\"utf-8\" ></script>\n";
				}
			}
		}

		return static::$instance->exports;
	}

	/**
	 * Get compiled file
	 * @param  [type] $file [description]
	 * @return [type]       [description]
	 */
	static public function get($file) {
		return static::$instance->exports[$file];
	}
	
	/**
	 * Add file to compiler
	 * 
	 * @param string $file file whith directory information
	 * @return void
	 */
	protected function add($file){
		
		$file = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], null, $file);
		
		if(!in_array($file, $this->sources) && file_exists($file)) {
			$this->sources[] = $file;
			$stamp = filemtime($file);
			$this->stamp = ( $stamp > $this->stamp ) ? $stamp : $this->stamp;	
		}
	}
	
	/**
	 * Compilation process
	 * 
	 * @param boolean $minify
	 * @return string html tag
	 */
	protected function compilation($minify) {

		if ($this->sources && !$this->error) {
			
			// group files by extensions
			foreach($this->sources as $file) {

				$pathinfo = pathinfo($file);
				$filename = $pathinfo['filename'];
				$extension = $pathinfo['extension'];
				
				$fileGroups[$extension][] = $file;
				$filenames[$extension][] = $filename;
			}
			
			// build compilations
			foreach ($fileGroups as $extension => $files) {

				// filename
				$compiledFilepath = join('.',$filenames[$extension]);
				$compiledFilename = join('.',$filenames[$extension]).'.'.$this->stamp.'.'.$extension;
				
				// cut lang filenames
				$length = strlen($compiledFilename);
				$compiledFilename = (255-$length > 0) ? $compiledFilename : substr($compiledFilename, -255);
				
				// filepath
				$compiledFile = $this->dir.$compiledFilename;
				
				// put all file contents in one file
				if (!file_exists($compiledFile)) {
					
					$compiledContent = null;
					
					// put all file contents in one file
					foreach($files as $file) {
							
						$content = file_get_contents($file);
							
						if($content !== false) {
							$compiledContent .= $content."\n\n";
						} else {
							$this->error = true;
						}
					}
				
					if (!$this->error && $compiledContent) {

						// remove old compilations
						//$oldComplations = glob($_SERVER['DOCUMENT_ROOT']."/public/compiled/$compiledFilepath*.$extension", GLOB_BRACE);

						if ($oldComplations) {
							foreach ($oldComplations as $oldfile) {
								@chmod($oldfile, 0666);
								@unlink($oldfile);
							}
						}
						
						// file minifigner
						if ($minify) {
							
							if ($extension=='css') {
								$compiledContent = CompressCSS::process($compiledContent);
							}
							
							if ($extension=='js') {
								$compiledContent = CompressJS::minify($compiledContent);
							}
						}
						
						// compiled name
						$export = file_put_contents($compiledFile, $compiledContent);
	
						// file successfully compiled
						if($export !== false) {
							chmod($compiledFile, $this->chmod);
						} else {
							$this->error = true;
						}
					}
					
					if (!$this->error) {
						
						// export filepath
						$sources[] = str_replace($_SERVER['DOCUMENT_ROOT'], '', $compiledFile); 
					}
				} 
				// compiled file exist and ist'n changed
				else {
					// export filepath
					$sources[] = str_replace($_SERVER['DOCUMENT_ROOT'], '', $compiledFile); 
				}
			}
		}
		
		return $sources;
	}
	
	
	/**
	 * Object instance
	 *
	 * @return Compiler
	 */
	protected static function instance() {
	
		if(!static::$instance) {
			
			static::$instance = new self();
			
			static::$instance->dir($_SERVER['DOCUMENT_ROOT']."/public/compiled/");
		}
	
		return static::$instance;
	}
}