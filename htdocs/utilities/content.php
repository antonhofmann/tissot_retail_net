<?php 

	class Content {
		
		public static function render($content, $data, $allMatches=false) {
			
			$match = self::match($content);
			
			if ($match) {
				
				foreach ($match as $keyword) {
					if ($allMatches) {
						$search[] = '{'.$keyword.'}';
						$replace[] = $data[$keyword];
					} elseif ($data[$keyword]) {
						$search[] = '{'.$keyword.'}';
						$replace[] = $data[$keyword];
					}
				}
				
				return str_replace($search,$replace,$content);
			}
			else return $content;
		}
		
		public static function match($content, $patter="#\{(.*?)\}#") {
			
			preg_match_all($patter, $content, $match);
			
			unset($match[0]);
			
			if ($match) {
				
				foreach ($match as $key => $array) {
					$array = array_unique($array);
					foreach ($array as $subkey => $value) {
						$return[] = $value;
					}
				}
			}
			
			return $return;
		}
		
		/**
		 * Data Render
		 *
		 * @param string $content
		 * @param array $data
		 * @return string
		 */
		public static function dataRender($content, $data) {
		
			if (is_array($data)) {
					
				$keywords = array_map(function($key) {
					return '{'.$key.'}';
				}, array_keys($data));
						
				$values = array_values($data);
					
				$content = preg_replace('#\{(.*?)\}#', '', str_replace($keywords, $values, $content));	
			}
			
			return $content;
		}
	}