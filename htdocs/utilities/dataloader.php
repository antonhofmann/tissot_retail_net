<?php 

	class dataloader {
		
		public static function material($param) {
			
			$model = new Model(Connector::DB_CORE);
			
			$result = $model->query("
				SELECT 
					translation_dataloader_value AS value, 
					translation_dataloader_label AS label 
				FROM translation_dataloaders
				WHERE translation_dataloader_language = 36 AND translation_dataloader_name = 'material.$param'
				ORDER BY translation_dataloader_label
			")->fetchAll();
			
			return $result;
		}
	}