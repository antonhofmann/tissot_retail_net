<?php

	class Table {
		
		static $attributes = array(
			'table' => array('id','class','dir','lang','style','title','border','cellpadding','cellspacing','bgcolor','align','frame','rules','summary','width'),
			'td' => array('id','class','dir','lang','style','title','abbr','align','axis','bgcolor','char','charoff','colspan','headers','height','nowrap','rowspan','scope','valign','width')
		);
		
		/**
		 * Datagrid<br />
		 * Array format: key => row where row is associative array
		 */
		public $datagrid;
		
		/**
		 * Table Order Values
		 * @var array
		 */
		public $order;
		
		const DATA_TYPE_LINK = "type=link";
		const DATA_TYPE_IMAGE = "type=image";
		const DATA_TYPE_TEXTBOX = "type=textbox";
		const DATA_TYPE_TEXTAREA = "type=textarea";
		const DATA_TYPE_CHECKBOX = "type=checkbox";
		const ATTRIBUTE_ALIGN_LEFT = "align=left";
		const ATTRIBUTE_ALIGN_RIGHT = "align=right";
		const ATTRIBUTE_ALIGN_CENTER = "align=center";
		const ATTRIBUTE_ALIGN_JUSTIFY = "align=justify";
		const ATTRIBUTE_VALIGN_TOP = "valign=top";
		const ATTRIBUTE_VALIGN_MIDDLE = "valign=middle";
		const ATTRIBUTE_VALIGN_BOTTOM = "valign=bottom";
		const ATTRIBUTE_VALIGN_BASELINE = "valign=baseline";
		const ATTRIBUTE_NOWRAP = "nowrap=nowrap";
		const PARAM_SORT = "param=sort";
		const PARAM_GET_FROM_LOADER = "param=loader";
		const FORMAT_NUMBER = "class=number";
		
		public function __construct($attributes=null) {
							
			if ($attributes) {
				foreach ($attributes as $param => $value) {
					if (in_array($param, self::$attributes['table'])) $this->table[$param] = $value;
					else $this->$param = $value;
				}
			}

			if (!$this->table['theme']) {
				$this->table['class'] = ($this->table['class']) ? $this->table['class'] : Settings::init()->theme;
			} else {
				$this->table['class'] = $this->table['theme'].' '.$this->table['class'];
			}
			
			$this->table['class'] = $this->table['class']." over-effects";
		}
		
		public function __call($column, $arguments) {
			
			if (!$this->columns[$column]) {
				$this->columns[$column] = array();
			}
			
			if ($arguments) {
				foreach ($arguments as $argument) {
					list($attribute,$value) = explode('=',$argument);
					if ($attribute=="param") $this->params[$column][] = trim($value);
					elseif ($attribute=="caption") $this->captions[$column] = trim($value);
					else $this->columns[$column][trim($attribute)] = trim($value);
				}
			}
		}
		
		public function caption($column, $value, $attributes=null) {
			
			$this->captions[$column] = $value;
			
			if (is_array($attributes)) {
				$this->attributes($column, $attributes);
			}
		}
		
		public function attributes($column, $attributes) {
			if (is_array($attributes)) {
				foreach ($attributes as $attribute => $value) {
					$this->column_attributes[$column][$attribute] = $value;
				}
			}
		}
			
		public function render() {
			
			$total_rows = count($this->datagrid);
			
			$this->thead = (isset($this->thead)) ? $this->thead : true;
			$this->emptybox = (isset($this->emptybox)) ? $this->emptybox : true;

			$translate = Translate::instance();
			
			if ($this->thead) {
				foreach ($this->columns as $column => $attributes) {
					
					if (isset($this->captions[$column])) $caption = $this->captions[$column];
					else $caption = ($translate->$column) ? $translate->$column : '&nbsp;';
			
					$canSort = ($this->params[$column] && in_array('sort', $this->params[$column])) ? true : false;
					$sort = ($canSort && $this->datagrid) ? "sort" : null;
					$direction = ($sort && $this->sort['column']==$column && $this->sort['direction']) ? $this->sort['direction'] : null;
					
					
					
					$attributes['class'] = ($attributes['class']) ? $column.' '.$attributes['class'] : $column;
					$attributes = $this->buildAttributes($attributes);
					$thead .= "<th $attributes ><strong class='$sort $direction'>$caption</strong></th>";
				}
			}
			
			if (is_array($this->datagrid)) {
				
				foreach ($this->datagrid as $row => $data) {
					
					$rowclass = ($i++ % 2) ? "-even" : "-odd";
					$first_child = ($i == 1) ? 'first-child' : null;
					$last_child = ($total_rows == $i) ? 'last-child' : null;
					$tbody .= "<tr id='row-$row' class='$rowclass $first_child $last_child'>";
					
					foreach ($this->columns as $column => $attributes) {
						
						$type = ($attributes['type']) ? "datatype_".$attributes['type'] : null;
						$type = ($attributes['href']) ? "datatype_link" : $type;
						
						unset($attributes['width']);
						unset($attributes['type']);
						unset($attributes['href']);
						
						if ($this->params[$column] && $this->dataloader[$column]) {
							$value = $this->dataloader[$column][$row];
						} else { 
							$value = $data[$column];
						}
						
						$attributes['class'] = ($attributes['class']) ? $column.' '.$attributes['class'] : $column;
						$attributes = $this->buildAttributes($attributes);
						
						$content = ($type) ? $this->$type($row,$column) : $value;
						
						$tbody .= "<td id='$column-$row' $attributes >$content</td>";
					}
					
					$tbody .= "</tr>";
				}
			}
			elseif($this->emptybox) {
				$message = ($translate->empty_result) ? $translate->empty_result : "No Result";
				$tbody .= "<tr><td colspan='".count($this->columns)."' class='-emptybox'>$message</td></tr>";
			}
			
			if ($this->tfoot) {
				if (is_array($this->tfoot[0])) {
					
					$tfoot = "<tr class=tfooter>";
					
					foreach ($this->tfoot as $i => $row) {
						$attributes = ($row['attributes']) ? $this->buildAttributes($row['attributes']) : null;
						$content = $row['content'];
						$tfoot .= "<td $attributes >$content</td>";
					}
					
					$tfoot .= "</tr>";
					
				} else {
					$tfoot = "<td colspan='".count($this->columns)."'>".join($this->tfoot)."</td>";
				}
			}
			
			$table = "<table ".$this->buildAttributes($this->table).">";
			$table .= "<thead>$thead</thead>";
			$table .= "<tbody>$tbody</tbody>";
			$table .= "<tfoot>$tfoot</tfoot>";
			$table .= "</table>";
	
			return $table;
		}
		
		public function dataloader($data) {
			if (is_array($data)) {
				foreach ($data as $key => $loader) {
					$this->dataloader[$key] = $loader;
				}
			}
		}
		
		public function footer($content) {
			$this->tfoot[] = $content;
		}

		private function datatype_image($row,$col) {
			return ($this->dataloader[$col]) ? $this->dataloader[$col][$row] : $this->datagrid[$row][$col];
		}
		
		private function datatype_link($row,$col) {
			
			if ($this->columns[$col]['href']) {	
				
				//$param = ($this->columns[$col]['param']) ? $this->columns[$col]['param'] : "id";

				$attributes = $this->column_attributes[$col] ?: array();
				$attributes = _array::rend_attributes($attributes);

				$link = $this->columns[$col]['href'];
				$caption = $this->datagrid[$row][$col];
				
				return "<a href='$link/$row' $attributes >$caption</a>";
			}
			else return $this->datagrid[$row][$col];
		}
		
		private function datatype_textbox($row,$col) { 
			
			if ($this->params[$col] && $this->dataloader[$col]) $value = $this->dataloader[$col][$row];
			else $value = $this->datagrid[$row][$col];

			$attributes = $this->column_attributes[$col] ?: array();
			$attributes['type'] = 'text';
			$attributes['index'] = $row;
			$attributes['class'] = $col . ' ' . $attributes['class'];
			$attributes['name'] = $col."[$row]";
			$attributes['value'] = $value;
			$attributes = _array::rend_attributes($attributes);

			return "<input $attributes />";
		}

		private function datatype_textarea($row,$col) { 
			
			if ($this->params[$col] && $this->dataloader[$col]) $value = $this->dataloader[$col][$row];
			else $value = $this->datagrid[$row][$col];

			$attributes = $this->column_attributes[$col] ?: array();
			$attributes['index'] = $row;
			$attributes['class'] = $col . ' ' . $attributes['class'];
			$attributes['name'] = $col."[$row]";
			
			$attributes = _array::rend_attributes($attributes);

			return "<textarea $attributes>" . $value . "</textarea>";
		}
		
		private function datatype_checkbox($row,$column) {

			$attributes = $this->column_attributes[$column] ?: array();
			$attributes['type'] = 'checkbox';
			$attributes['index'] = $row;
			$attributes['class'] = $column;
			$attributes['name'] = $column."[$row]";
			$attributes['value'] = $this->datagrid[$row][$column] ? $this->datagrid[$row][$column] : $row;

			if ($this->dataloader[$column]) {
				if ($this->dataloader[$column][$row]) $attributes['checked'] = "checked";
			} else {
				if ($this->datagrid[$row][$column]) $attributes['checked'] = "checked";
			}
			
			$attributes = _array::rend_attributes($attributes);
			
			return "<input $attributes />";
		}
		
		private function buildAttributes($attributes) {
			return ($attributes) ? _array::rend_attributes($attributes) : null;
		}
	}
	