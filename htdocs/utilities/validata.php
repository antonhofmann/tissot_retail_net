<?php

	class Validata {
			
		const PARAM_REQUIRED = "validata=required";
		const PARAM_INTEGER = "validata=custom[integer]";
		const PARAM_URL = "validata=custom[url]";
		const PARAM_EMAIL = "validata=custom[email]";
		const PARAM_PHONE = "validata=custom[phone]";
		const PARAM_DATE = "validata=custom[date]";
		const PARAM_YEAR = "validata=custom[year]";
		const PARAM_ALPHANUMERIC = "validata=custom[onlyLetterNumber]";
		const PARAM_NUMBER = "validata=custom[number]";
		const PARAM_ONLY_NUMBER = "validata=custom[onlyNumberSp]";
		const PARAM_NUMBER_DECIMAL = "validata=custom[decimal]";
		const PARAM_NUMBER_DECIMAL_2 = "validata=custom[decimal2]";
		const PARAM_ONLY_LETTER = "validata=custom[onlyLetterSp]";
		const PARAM_IPV4 = "validata=custom[ipv4]";
		const PARAM_CREDICARD = "validata=custom[creditCard]";
		const CLASS_DATAPICKER = "class=datepicker";
		
		public static function equals($field) {
			return "validata=equals[$field]";
		}
		
		public static function funcCall($callback) {
			return "validata=funcCall[$callback]";
		}
		
		public static function min($val) {
			return "validata=min[$val]";
		}
		
		public static function minsize($val) {
			return "validata=minSize[$val]";
		}
		
		public static function max($val) {
			return "validata=max[$val]";
		}
		
		public static function maxsize($val) {
			return "validata=maxSize[$val]";
		}
		
		public static function past($date="NOW") {
			return "validata=past[$date]";
		}
		
		public static function future($date="NOW") {
			return "validata=future[$date]";
		}
		
		public static function minCheckbox($val) {
			return "validata=minCheckbox[$val]";
		}
		
		public static function maxCheckbox($val) {
			return "validata=maxCheckbox[$val]";
		}
		
		public static function dataRange($field) {
			return "validata=dateRange[$field]";
		}
		
		public static function dataTimeRange($field) {
			return "validata=dateTimeRange[$field]";
		}
		
		public static function ajax($callback) {
			return "validata=ajax[$callback]";
		}
	
	}
