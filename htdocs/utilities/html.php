<?php 

	class html {
		
		public static function input($properties) {
			$properties['type'] = $properties['type'] ?: 'text';
			$properties = _array::rend_attributes($properties);
			return "<input $properties />";
		}
		
		public static function select($dataloader, $properties) {
			
			if (is_array($dataloader)) {
				
				// caption
				if ($properties['caption']!=false) {
					$caption = $properties['caption'] ? $properties['caption'] : "Select";
					unset($properties['caption']);
				}

				
				// attributes
				$attributes = _array::rend_attributes($properties);
				
				$return = "<select $attributes >";
				
				if ($caption) {
					$return .= "<option value=\"\">$caption&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
				}
				
				foreach ($dataloader as $key => $row) {
					
					if (is_array($row)) {
						list($id,$name) = array_values($row);
					} else {
						$id = $key;
						$name = $row;
					}
					
					$selected = ($properties['value']==$id) ? "selected=selected" : null;
					
					$return .= "<option value=\"$id\" $selected>$name</option>";
				}
				
				$return .= "</select>";
				
				return $return;
			}
		}
		
		public static function linkbutton($properties) {
			$caption = $properties['caption'];
			$icon = ($properties['icon']) ? '<span class="icon '.$properties['icon'].'"></span>' : null;
			unset($properties['caption']);
			unset($properties['icon']);
			$properties['class'] = "button ".$properties['class'];
			$properties = _array::rend_attributes($properties);
			return "<a $properties >$icon<span class=label >$caption</span></a>";
		}
		
		public static function dialog($data) {
			
			extract($data);
			
			$dialog .= "<div id=\"$id\" class=\"dialogbox\" >";
			$dialog .= "<div class=\"-title\"><b>$title</b></div>";
			$dialog .= "<div class=\"-content $content_class\">$content</div>";
			
			if ($cancel || $confirm) {
				
				$dialog .= "<div class=\"actions\">";
				
				if ($cancel) {
					$dialog .= "
						<a id=cancel class='button $cancel_class' >
							<span class='icon cancel'></span>
							<span class=label >$cancel</span>
						</a>";
				}
				
				if ($confirm) {
					$dialog .= "
						<a id=apply class='button $apply_class'>
							<span class='icon apply'></span>
							<span class=label >$confirm</span>
						</a>";
				}
				
				$dialog .= "</div>";
			}
			
			$dialog .= "</div>";
			
			return $dialog;
		}
	
		public static function emptybox($message) {
			return "<div class=\"emptybox\">$message</div>";
		}
		
		/**
		 * Build request form<br />
		 * Default MVC request vars will be prepended (/application/controller/archived/action)
		 * @param array $fields
		 * @return hidden requst form
		 */
		public function requestForm($fields=null) {
				
			$data = array();
			$lang = Request::instance()->language;
			$application = Request::instance()->application;
			$controller = Request::instance()->controller;
			$archived = Request::instance()->archived;
			$action = Request::instance()->action;
			
			if ($lang) $data['language'] = $lang;
			if ($application) $data['application'] = $application;
			if ($controller) $data['controller'] = $controller;
			if ($archived) $data['archived'] = $archived;
			if ($action) $data['action'] = $action;
			
			$data = (is_array($fields)) ? array_merge($data, $fields) : $data;
				
			if ($data) {
				foreach ($data as $name => $value) {
					if (is_array($value)) {
						foreach ($value as $subname => $subvalue) {
							$data[$name.$subname] = '<input type=hidden name='.$name.'['.$subname.'] id='.$name.'['.$subname.'] value="'.$subvalue.'" />';
						}
					} else {
						$data[$name] = "<input type=hidden name='$name' id='$name' value='$value' />";
					}
				}
			}
				
			return ($data) ? "<form class=request >".join(array_values($data))."</form>" : null;
		}
	}