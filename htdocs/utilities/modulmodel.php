<?php 


class ModulModel {
	
	/**
	 * Model Name
	 * 
	 * @var string
	 */
	protected $tableName;

	/**
	 * Primary Kay
	 * 
	 * @var string
	 */
	protected $primaryKeyName;
	
	/**
	 * PDO drive
	 * 
	 * @pdo
	 */
	protected $pdo;
	

	public function __construct($connector=null) {
		$this->pdo = Connector::get($connector);
	}

	/**
	 * Set table name
	 * 
	 * @var string $name model name
	 * @return void
	 */
	public function setTableName($name) {
		$this->tableName = $name;
		$this->primaryKeyName = $this->getIndex('Column_name');
	}

	/**
	 * Get primary key name
	 * 
	 * @return string
	 */
	public function getPrimaryKey() {
		return $this->primaryKeyName;
	}	

	/**
	 * Get model data
	 * 
	 * @return array
	 */
	public function read($id) {
		
		$query = Query::read($this->tableName, $this->primaryKeyName);

		$sth = $this->pdo->prepare($query);
		$sth->bindParam('primaryKey', $id, \PDO::PARAM_INT);
		$sth->execute();
		
		return $sth->fetch();
	}
	
	/**
	 * Insert new model record
	 * 
	 * @return int last inserted id
	 */
	public function create($data) {
			
		$query = Query::insert($this->tableName, $data);
	
		$sth = $this->pdo->prepare($query);
		$sth->execute($data);
		
		return $this->pdo->lastInsertId();
	}
	
	/**
	 * Update model record
	 * 
	 * @return boolean
	 */
	public function update($id, $data) {
		
		$query = Query::update($this->tableName, $this->primaryKeyName, $data);
		
		$sth = $this->pdo->prepare($query);

		$data[$this->primaryKeyName] = $id;

		return $sth->execute($data);
	}
	
	/**
	 * Delete model record
	 * 
	 * @return boolean
	 */
	public function delete($id) {
		
		$query = Query::delete($this->tableName, $this->primaryKeyName);

		$sth = $this->pdo->prepare($query);
		$sth->bindParam('primaryKey', $id, \PDO::PARAM_INT);
		
		return $sth->execute();
	}

	/**
	 * Get model index
	 * 
	 * @return mixed
	 */
	public function getIndex($key=null) {
		
		$sth = $this->pdo->prepare("SHOW INDEX FROM {$this->tableName} ");
		$sth->execute();
		$result = $sth->fetch();

		return $key ? $result[$key] : $result;
	}

	/**
	 * SQL total found rows (whithout offset)
	 * 
	 * @return number total rows
	 */
	public function totalRows() {
		
		$sth = $this->pdo->prepare("SELECT FOUND_ROWS() AS totalrows");
		$sth->execute();
		$result = $sth->fetch();
		
		return $result['totalrows'] ?: 0;
	}
}