<?php

	class Settings {
		
		private static $instance;
		
		public static function init() {
			if (!self::$instance) self::$instance = new Settings();
			return self::$instance;
		}
		
		private $_loaded = array();
		
		public function __construct() {
				
			$this->load("project");
			$this->load("data");
				
			if (isset($_SESSION) and $_SESSION) {
				if (Session::get('language')) $this->language = Session::get('language');
				if (Session::get('theme')) $this->theme = Session::get('theme');
			}
				
			$this->path_theme = '/public/themes/'.$this->theme."/";
			$this->path_theme_css = $this->path_theme."css/";
			$this->path_theme_images = $this->path_theme."images/";
			
			// set server name
			$this->server = url::server();
			
			// set dev mode
			//$this->devmod = (preg_match("/\/retailnet.".$this->shortcut.".com/", $_SERVER['SERVER_NAME'])) ? false : true;

			$this->devmod = false;
			
			// http protocol
			if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
				$this->http_protocol = ($_SERVER['HTTP_X_FORWARDED_FOR']) ? 'https' : 'http';
			}
			else {
				$this->http_protocol = 'http';
			}
		}
		
		public function __call($file,$arguments) {
			$this->load($file);
		}
				
		public function load($file) {
			
			if (!in_array($file, $this->_loaded)) {

				array_push($this->_loaded, $file);
				$file = PATH_SETTINGS."$file.ini";
				$settings = parse_ini_file($file, true);
				
				if ($settings) {
					foreach ($settings as $key => $value) {
						$this->$key = $value;
					}
				}			
			}
		}
	}