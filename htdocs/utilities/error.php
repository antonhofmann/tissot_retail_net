<?php 

	class Error {
		
		public function __construct($url) {
			$this->url = $url;
		}
		
		public function onBootstrap() {
			echo "BOOTSTRAP: Controller not found. Please check your URL: <b>".$this->url."</b>";
		}
	}