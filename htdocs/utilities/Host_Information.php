<?php

/**
 * Get information about host
 *
 * Add methods as we need them
 */
class Host_Information {
  /**
   * Get the PHP version without the extra bit that PHP_VERSION adds
   * @return string
   */
  public function getPHPVersion() {
    return PHP_MAJOR_VERSION . '.' . PHP_MINOR_VERSION;
  }
}