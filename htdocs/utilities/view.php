<?php

	class View {

		public $master;
		
		
		public $templates; 
		
		/**
		 * Intern stylesheet files
		 * @var array
		 */
		private $stylesheets = array();
		
		/**
		 * Theme Files
		 * @var array
		*/
		private $themes = array();
		
		/**
		 * Intern scipt files
		 * @var array
		*/
		private $scripts = array();
		
		/**
		 * Extern sources
		 * @var array
		*/
		private $extern = array();
		
		/**
		 * Theme Name
		 * @var string
		*/
		public $theme;

		public $dataloader = array();
		
		public function __construct() { 

			$this->settings = Settings::init();
			$this->settings->data();
			$this->translate = Translate::instance();
			$this->user = User::instance();
		}

		public function setTemplate($name, $template) {
			$this->templates[$name] = $template;
		} 
	
		public function __get($name) {
			return $this->templates[$name] ?: $this->dataloader[$name];
		}
		
		public function __isset($name) {
			if (isset($this->templates[$name])) return true;
			elseif($this->templates) {
				foreach ($this->templates as $template => $object) {
					if ($object->placeholder==$name || $object->modul==$name) {
						return true;
						break;
					}
				}
			}
		}

		public function __call($name, $arguments) { 
			if ($this->templates[$name]) {
				return $this->templates[$name]->show();
			}
			elseif($arguments[0]) {
				return $this->templates[$name] = new Template($arguments[0]);
			}
			else {
				return $this->groupTemplates($name);
			}
		}
		
		public function master($master) {
			$this->master = $master;
		}
		
		public function render() {
			
			$this->master = $this->master ? $this->master : "master.inc";
			
			// compile stylesheets
			if ($this->stylesheets) {
				Compiler::attach($this->stylesheets);
			}
				
			// compile stylesheets
			if ($this->themes) {
				Compiler::attach($this->themes);
			}
				
			// compile scripts
			if ($this->scripts) {
				Compiler::attach($this->scripts);
			}

			Compiler::export();
			
			$file = PATH_THEMES.$this->settings->theme."/".$this->master;
			ob_start();
			require $file;
			$content = ob_get_contents();
			ob_end_clean();
			
			echo $content;
		}
		
		private function groupTemplates($name) {
			if ($this->templates) {
				foreach ($this->templates as $template => $object) {
					if (is_object($object)) {
						if ($object->modul==$name || $object->placeholder==$name) {
							$content .= $object->show();
						}
					} else {
						//$content .= $object;
					}
				}
				return $content;
			}
		}


		public function dataloader($param, $value=null) {

			if (is_array($param)) {
				$this->dataloader = array_merge($this->dataloader, $param);
			} elseif($value) {
				$this->dataloader[$param] = $value;
			}
		}
	}