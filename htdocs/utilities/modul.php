<?php


class Modul {

	/**
	 * Primary Key
	 * @var int
	 */
	public $id;

	/**
	 * Primary Key name
	 * @var string
	 */
	protected $primaryKey;
	
	/**
	 * Dataloader
	 * 
	 * @var array
	 */
	public $data;


	public $map;
	
	/**
	 * Database Model
	 * 
	 * @return ModulModel
	 */
	protected $model;


	public function __construct($connector=null) {
		
		if ($connector && Application::is($connector) ) {
			$connector = Application::is($connector);
		}

		$this->model = new ModulModel($connector);
	}

	public function setDataMap($map) {
		if (is_array($map)) {
			$this->map = $map;
		}
	}

	/**
	 * Set table name
	 * 
	 * @param string $table Table name
	 * @return void
	 */
	public function setTable($name) {
		$this->model->setTableName($name);
		$this->primaryKey = $this->model->getPrimaryKey();
	}

	/**
	 * Reader
	 * 
	 * @param int $id
	 * @return array
	 */
	public function read($id) {

		$this->data = $this->model->read($id);
		$this->id = $this->data[$this->primaryKey];

		if ($this->map) {
			$this->data = _array::renameKeys($this->data, $this->map);
		}

		return $this->data;
	}
	
	/**
	 * Create
	 * 
	 * @param array $data
	 * @return int inserted ID
	 */
	public function create($data) {
		
		if (is_array($data)) {
			
			$response = $this->model->create($data);

			if ($response) {
				$this->read($response);
			}
			
			return $response;
		}
	}
	
	/**
	 * Update
	 * 
	 * @param array $data
	 * @return boolean true on success
	 */
	public function update($data) {

		if ($this->id && is_array($data)) {
			
			$response = $this->model->update($this->id, $data);
			
			if ($response) {

				if ($this->map) {
					$data = _array::renameKeys($data, $this->map);
				}

				$this->data = array_merge($this->data, $data);
			}

			return $response;
		}
	}

	/**
	 * Remove
	 * 
	 * @return boolean true on success
	 */
	public function delete() {
		
		if ($this->id) {
			return $this->model->delete($this->id);
		}
	}
}