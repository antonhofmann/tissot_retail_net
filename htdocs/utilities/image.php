<?php

class Image {

	public static function getSize($file, $optDpi=150) {

		$file = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], '', $file);

		if (!file_exists($file)) {
			return false;
		}

		// get image resolution
		$dpi = static::getDpi($file);
		$dpi = $dpi && $dpi < $optDpi ? $dpi : $optDpi;

		// get image size in mm
		$imagesize = getimagesize($file);
		$width = $imagesize[0]*25.4/$dpi;
		$height = $imagesize[1]*25.4/$dpi;

		return array(
			'width' => $width,
			'height' => $height,
			'dpi' => $dpi
		);
	}

	public static function getMaxSize($file, $maxWidth=null, $maxHeight=null, $optDpi=150) {

		$size = static::getSize($file, $optDpi);
		$width = $size['width'];
		$height = $size['height'];
		$dpi = $size['dpi'];

		if (!$width || !$height) {
			return;
		}

		// resize image for max width
		if ($maxWidth && $width > $maxWidth) {
			$scaleFactor = $maxWidth/$width;
			$width = $width * $scaleFactor;
			$height = $height * $scaleFactor;
		}

		// resize image for max height
		if ($maxHeight && $height > $maxHeight) {
			$scaleFactor = $maxHeight/$height;
			$width = $width * $scaleFactor;
			$height = $height * $scaleFactor;
		}

		return array(
			'width' => $width,
			'height' => $height,
			'dpi' => $dpi
		);
	}

	public static function getDpi($file){

		$file = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], '', $file);

		if (!file_exists($file)) {
			return false;
		}
		
		// open the file and read first 20 bytes.
		$a = fopen($file,'r');
		$string = fread($a,20);
		fclose($a);
		
		// get the value of byte 14th up to 18th
		$data = bin2hex(substr($string,14,4));
		$x = substr($data,0,4);
		$y = substr($data,4,4);
		
		return hexdec($x) ?: hexdec($y);
	}

	public function isTransparent($image) {

		$image = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], '', $image);

		if (!file_exists($image)) {
			return false;
		}
		
		$width = imagesx($image);
		$height = imagesy($image);

		for($i = 0; $i < $width; $i++) {
			
			for($j = 0; $j < $height; $j++) {
				
				$rgba = imagecolorat($im, $i, $j);
				
				if(($rgba & 0x7F000000) >> 24) {
					return true;
				}
			}
		}

		return false;
	}
}