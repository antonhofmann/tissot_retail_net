<?php 

	class Mail {
			
		public function __construct() {
			parent::__construct();
			$this->model = new Mail_Model();
		}
		
		public function content($data) {
			return content::render($this->content, $data);
		}
		
		public function template($template) {
			$result = $this->model->template($template);
			$this->subject = $result['subject'];
			$this->content = $result['content'];
		}
	
	}