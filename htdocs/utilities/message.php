<?php 

	class Message {
		
		const CATEGORY_KEYWORD = 'keyword';
		const CATEGORY_MESSAGE = 'message';
		const CATEGORY_ERROR = 'error';
		const CATEGORY_TOOLTIP = 'tooltip';
		const CATEGORY_DIALOG = 'dialog';
				
		public static function read($id) {
			$model = new Message_Model(Connector::DB_CORE);
			return $model->read($id);
		}
		
		public static function read_from_keyword($keyword, $category) {
			$model = new Message_Model(Connector::DB_CORE);
			return $model->read_from_keyword($keyword, $category);
		}
		
		public static function id($keyword, $category=null) {
			$category = ($category) ? $category : self::CATEGORY_MESSAGE;
 			$data = self::read_from_keyword($keyword, $category);
			return $data['id'];
		}
		
		public static function add($message) { 
			if (is_array($message)) { 
				Session::init(); 
				$messages = Session::get('messages'); 
				$messages = ($messages) ? unserialize($messages) : array(); 
				array_push($messages, $message);
				Session::set('messages', serialize($messages));
			}
		}
		
		public static function password_reset_confirmation() {
			self::add(array(
				'type'=>'message',
				'keyword' => 'message_password_reset_confirmation',
				'sticky' => true
			));
		}
		
		public static function password_reset_success() {
			self::add(array(
				'type'=>'message',
				'keyword'=>'message_password_reset_success',
				'life'=>5000
			));
		}
	
		public static function password_expired() {
			self::add(array(
				'type' => 'message',
				'keyword' => 'message_password_expired',
				'life' => 8000
			));
		}
		
		public static function login_success() {
			self::add(array(
				'type' => 'message',
				'keyword' => 'message_login_success',
				'life' => 3000
			));
		}
	
		public static function logout_success() {
			self::add(array(
				'type' => 'message',
				'keyword' => 'message_logout_success',
				'life' => 3000
			));
		}
		
		public static function invalid_permission() {
			self::add(array(
				'type' => 'error',
				'keyword' => 'message_invalid_permission',
				'life' => 5000
			));
		}
		
		public static function invalid_request() {
			self::add(array(
				'type' => 'error',
				'keyword' => 'message_invalid_request',
				'life' => 5000
			));
		}
		
		public static function request_inserted() {
			self::add(array(
				'type' => 'message',
				'keyword' => 'message_request_inserted',
				'life' => 5000
			));
		}
		
		public static function request_saved() {
			self::add(array(
				'type' => 'message',
				'keyword' => 'message_request_saved',
				'life' => 5000
			));
		}
		
		public static function request_updated() {
			self::add(array(
				'type' => 'message',
				'keyword' => 'message_request_updated',
				'life' => 5000
			));
		}
		
		public static function request_modified() {
			self::add(array(
				'type' => 'message',
				'keyword' => 'message_request_modified',
				'life' => 5000
			));
		}
		
		public static function request_deleted() {
			self::add(array(
				'type' => 'message',
				'keyword' => 'message_request_deleted',
				'life' => 5000
			));
		}
		
		public static function request_archived() {
			self::add(array(
				'type' => 'message',
				'keyword' => 'message_request_archived',
				'life' => 5000
			));
		}
		
		public static function request_failure() {
			self::add(array(
				'type'=>'error',
				'keyword'=>'message_request_failure',
				'life' => 3000
			));
		}
	
		public static function request_submitted() {
			self::add(array(
				'type'=>'message',
				'keyword'=>'message_request_submitted'
			));
		}
			
		public static function access_denied() {
			self::add(array(
				'type'=>'error',
				'keyword'=>'message_access_denied'
			));
		}
		
		public static function sendmail_failure() {
			self::add(array(
				'type'=>'error',
				'keyword'=>'message_sendmail_failure',
				'sticky' => true
			));
		}
	
		public static function failure_id() {
			self::add(array(
				'type' => 'error',
				'keyword'=>'message_failure_id',
				'life' => 3000
			));
		}
	
		public static function ordersheet_version_created() {
			self::add(array(
				'type' => 'message',
				'keyword' => 'message_ordersheet_version_created',
				'life' => 5000
			));
		}
	
		public static function empty_result() {
			self::add(array(
				'type' => 'message',
				'keyword' => 'empty_result',
				'life' => 5000
			));
		}
		
		public static function error_db_integrity() {
			self::add(array(
				'type'=>'error',
				'keyword'=>'error_db_integrity',
				'life' => 3000
			));
		}		

		public static function error($message) {
			self::add(array(
				'type'=>'error',
				'keyword'=> $message,
				'life' => 5000
			));
		}	

		public static function success($message) {
			self::add(array(
				'type'=>'message',
				'keyword'=> $message,
				'life' => 5000
			));
		}
	}
	