<?php 

class Request {
	
	
	/**
	 * Navigation ID
	 * @var integer
	 */
	public $id;
	
	/**
	 * Request Application Name
	 * First request parameter
	 * @var string
	 */
	public $application;
	
	/**
	 * Request Controller Name
	 * If application is defined second request parameter, otherwise is first request parameter
	 * @var string
	 */
	public $controller;
	
	/**
	 * Request Action Name
	 * After request Controller
	 * @var string
	 */
	public $action;
	
	/**
	 * Request Archived Parameter Name
	 * After request Controller
	 * @var string
	 */
	public $archived;
	
	/**
	 * Navigation title
	 * @var string
	 */
	public $title;
	
	/**
	 * Request parent
	 * @var integer
	 */
	public $parent;
	
	/**
	 * Request URL
	 * @var string
	 */
	public $url;
	
	/**
	 * Tab identificator
	 * @var integer
	 */
	public $tab;
	
	/**
	 * Request Order
	 * @var integer
	 */
	public $order;
	
	/**
	 * Current Request parents
	 * All current navigation parents ID's 
	 * @var array
	 */
	public $parents = array();
	
	/**
	 * Excluded URL's
	 * Registry urls which should be excluded from Menu Builder
	 * @var array
	 */
	public $excluded = array();
	
	/**
	 * Request form fields
	 * @var array
	 */
	public $fields = array();

	/**
	 * Request params
	 * @var array
	 */
	public $params = array();

	protected $navigation;

	protected $registredUrls;
	
	/**
	 * Object instance
	 * @var string
	 */
	private static $instance;

	public static function instance($url=null) {
		if (!self::$instance) self::$instance = new Request($url);
		return self::$instance;
	}
	
	public function __construct($url=null) {
		$this->navigation = new Navigation();
		$this->run($url);
	}

	static public function param($param=0) {
		return static::instance()->params[$param];
	}

	public function getParam($param=0) {
		return $this->params[$param];
	}

	public function getParasedParams($params=null) {

		if (!$this->params) return;

		if ($params) {
			$params = is_array($params) ? $params : array($params);
			$params = array_merge($this->params, $params);
		} else {
			$params = $this->params;
		}

		return join('/', $params);
	}

	public function getParsedUrl($url=null, $params=null) { 

		$request = array_filter(array(
			$url['application'] ?: $this->application,
			$url['controller'] ?: $this->controller,
			$url['archived'] ?: $this->archived,
			$url['action'] ?: $this->action
		));
		
		$params = $this->getParasedParams($params);

		if ($params) {
			array_push($request, $params);
		}

		return $request ? join('/', $request) : null;
	}

	public function check() {
		
		if (!$this->controller || !$this->action) { 
			return false;
		}

		if ($this->isPublicController()) {
			return $this->permitted = true;
		}

		if ($this->id && $this->secure) {
			return $this->isPermitted();
		}

		return true;
	}

	static public function get($name, $escape=true) {
		if (!$_REQUEST[$name]) return;
		return $escape ? htmlentities(trim($_REQUEST[$name])) : $_REQUEST[$name];
	}
	
	public function exclude($path) {
		$this->excluded[] = $path;
	}
	
	public function query($params=null) {
		
		$query = array_filter(array(
			$this->application,
			$this->controller,
			$this->archived ? 'archived' : null
		));

		if ($params) {
			if (is_array($params)) {
				foreach ($params as $param) {
					array_push($query, $param);
				}
			} else {
				array_push($query, $params);
			}
		}
		
		return ($query) ? '/'.join('/',$query) : null;
	}
	
	public function link($link, $params=null) {
		
		$query = array_filter(array(
			'application' => $this->application,
			'controller' => $this->controller,
			'archived' => $this->archived ? 1 : null,
			'action' => $this->action
		));
		
		$query = is_array($params) ? $query = array_merge($query, $params) : $query;
		
		return $query ? $link.'?'.http_build_query($query) : $link;
	}
	
	public function queryArray() {
		
		$query = array();
		$params = url::params();

		$query = array_filter(array(
			'application' => $this->application,
			'controller' => $this->controller,
			'archived' => $this->archived ? 1 : null,
			'action' => $this->action,
			'id' => $this->id
		));
		
		if ($params) {
			$query['params'] = serialize($params);
		}
		
		return $query;
	}

	protected function run($url=null) { 

		if (!$url) {
			$url = Url::get();
			$url =  $url ?: $settings->url_dashboards;
		}

		// metch request (recursive method)
		$data = $this->matchUrl($url);

		// registred url
		if ($data['navigation_url']) {

			// registred urls
			$this->registredUrls[] = $data['navigation_url'];

			$this->buildSystem($data['navigation_url']);

			if (!$this->controller) {

				// registred url is not system request (group of url's)
				// search recursive in set of child urls (only first child) 
				
				if (!$this->navigation->id) {
					$this->navigation->read($data['navigation_id']);
				}

				$data = $this->navigation->getFirstChild(true);

				if ($data['navigation_url']) {
					$this->run($data['navigation_url']);
				}
			}
		} 
		// manually build system
		else {
			$this->buildSystem($url);
		}

		// searching was not successfully
		if (!$this->controller) {
			//return;
		}

		// update system data
		if ($data) {
			$this->id = $data['navigation_id'];
			$this->parent = $data['navigation_parent'];
			$this->url = $data['navigation_url'];
			$this->title = $data['navigation_language_name'];
			$this->tab = $data['navigation_tab'];
			$this->active = $data['navigation_active'];
			$this->order = $data['navigation_order'];
			$this->secure = $data['navigation_secure'];
		}

		// build system parameters
		$this->buildParams();

		$controllerClass = $this->controller.'_controller';

		// set action manually
		if (!$this->action) {
			
			$url = explode('/', ltrim(Url::get(), '/'));

			if ($url) {
				foreach ($url as $key) {
					if (method_exists($controllerClass, $key)) {
						$this->action = $key;
						break;
					}
				}
			}
		}

		if (!$this->action && method_exists($controllerClass, 'index')) {
			$this->action = 'index';
		}

		if ($this->id) {
			array_push($this->parents, $this->id);
			$this->matchAllParents($this->id);
		}
	}

	protected function buildSystem($url) {

		if (!$url) return;

		$url = explode('/', ltrim($url, '/'));

		// application shortcut (only string param)
		$app = preg_match('/[A-Za-z]+/', $url[0]) ? $url[0] : null;

		if ($app) { 

			$application = new Application();
			$application->read_from_shortcut($app);

			if ($application->id) { 
				$this->application = array_shift($url);
			}
		}

		// controller name (only string param)
		$controller = $url[0] && preg_match('/[A-Za-z]+/', $url[0]) ? $url[0] : null;
		$controllerClass = $controller.'_controller';

		if ($controller) {
			
			if (class_exists($controllerClass)) {
				$this->controller = array_shift($url);
			}

		} 
		
		if (!$controller && $app) {

			if (class_exists($app.'_controller')) {
				$controllerClass = $app.'_controller';
				$this->controller = $app;
			}
		}

		if (!$this->controller) {
			return;
		}

		// controller action (only string param)
		$action = $url[0] && preg_match('/[A-Za-z]+/', $url[0]) ? $url[0] : null;


		if ($action=='archived') {
			$this->archived = array_shift($url);
			$action = $url[0] && preg_match('/[A-Za-z]+/', $url[0]) ? $url[0] : null;
		}

		if ($action && method_exists($controllerClass, $action)) {
			$this->action = array_shift($url);
		}
	}

	protected function buildParams() {

		$url = Url::get();

		if (!$url) return;

		if ($this->registredUrls) {
			$systemRequest = $this->registredUrls[0];
		} else {

			$req = array();
			$arr = explode('/', ltrim($url, '/'));

			if ($this->application && $this->application==$arr[0]) {
				$req[] = array_shift($arr);
			}

			if ($this->controller && $arr && $this->controller==$arr[0]) {
				$req[] = array_shift($arr);
			}

			if ($this->archived && $arr && $this->archived==$arr[0]) {
				$req[] = array_shift($arr);
			}

			if ($this->action && $arr && $this->action==$arr[0]) {
				$req[] = array_shift($arr);
			}

			$systemRequest = $req ? join('/', $req) : null;
		}

		// native query string
		//parse_str($_SERVER['QUERY_STRING'], $this->queryString);

		if (!$systemRequest) return;
		$params = ltrim(str_replace($systemRequest, '', $url), DIRECTORY_SEPARATOR);
		$this->params = $params ? explode('/', rtrim($params, DIRECTORY_SEPARATOR)) : array(); 
	}

	public function isPermitted($id=null) { 

		if (isset($this->permitted)) { 
			return $this->permitted;
		}

		if (!$this->secure) {
			return $this->permitted = true;
		}

		$id = $id ?: $this->id;
		$roles = join(',', user::roles());
		$db = Connector::get();

		$sth = $db->prepare("
			SELECT navigation_id
			FROM navigations
			INNER JOIN role_navigations ON role_navigation_navigation = navigation_id
			WHERE navigation_id = ? AND role_navigation_role IN ( $roles ) 
		");

		$sth->execute(array($id));
		$result = $sth->fetch();

		$this->permitted = $result['navigation_id'] ? true : false;

		return $this->permitted;
	}
	
	protected function matchUrl($url) {

		if (!$url) return;

		$data = $this->navigation->read_from_request($url);

		if ($data) return $data;

		$request = explode('/', $url);
		array_pop($request);
	
		if ($request) {
			$request = join('/', $request);
			return $this->matchUrl($request);
		}
	}

	protected function matchAllParents($id) {

		if (!$id) return;

		$db = Connector::get();
		$sth = $db->prepare("SELECT navigation_parent FROM navigations WHERE navigation_id = ?");
		$sth->execute(array($id));
		$result = $sth->fetch();
		$parent = $result['navigation_parent'];
		
		if ($parent) {
			array_push($this->parents, $parent);
			$this->matchAllParents($parent);
		} 
	}

	public function form($fields=null) {
		
		$data = array();
		$fields = (is_array($fields)) ? array_merge($this->fields, $fields) : $this->fields;
		
		if ($this->application) {
			$data['application'] = "<input type=hidden name=application id=application value='".$this->application."' />";
		}
		
		if ($this->controller) {
			$data['controller'] = "<input type=hidden name=controller id=controller value='".$this->controller."' />";
		}
		
		if ($this->action) {
			$data['action'] = "<input type=hidden name=action id=action value='".$this->action."' />";
		}
		
		if ($this->archived) {
			$data['archived'] = "<input type=hidden name=archived id=archived value='1' />";
		}
		
		if ($fields) {
			foreach ($fields as $name => $value) {
				if (is_array($value)) {
					foreach ($value as $subname => $subvalue) {
						$data[$name.$subname] = '<input type=hidden name='.$name.'['.$subname.'] id='.$name.'['.$subname.'] value="'.$subvalue.'" />';
					}
				} else {
					$data[$name] = "<input type=hidden name='$name' id='$name' value='$value' />";
				}
			}
		}
		
		return ($data) ? "<form name='request' class='request' >".join(array_values($data))."</form>" : null;
	}

	public function field($name,$value=null) {
		if (isset($value)) $this->fields[$name] = $value;
		else return $this->fields[$name];
	}
	
	public function isField($name) {
		return ($this->fields[$name]) ? true : false;
	}

	public function isPublicController() {

		return $this->controller && in_array($this->controller, array('security', 'messages', 'download')) ? true : false;
	}

	public static function isAjax() {
		return strtolower(filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH'))==='xmlhttprequest' ? true : false;
	}
}

