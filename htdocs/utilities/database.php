<?php

	class Database extends PDO {
		
		private static $instance;
		
		public static function instance($db) { 
			if (!self::$instance) self::$instance = new Database($db);
			return self::$instance;
		}
	
		public function __construct($dns, $username, $password) { 
			parent::__construct($dns, $username, $password,
				array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
			);
		}
		
		
		/**
		 * Build SQL Insert Statement
		 * 
		 * @param string $table db table name
		 * @param array $data fields
		 * @return string
		 */
		public static function insertStatement($table, $data) {
			
			$fields = array_keys($data);
			$flags = $fields;
			
			array_walk($flags, function(&$value){
				$value = ":$value";
			});
	
			$fields = join(', ', $fields);
			$flags= join(', ',$flags);
			
			return "
				INSERT INTO $table ( $fields )
				VALUES ( $flags )		
			";
		}
		
		/**
		 * Build SQL Update Statement
		 *
		 * @param string $table db table name
		 * @param string $entity causale entity field name
		 * @param array $data fields
		 * @return string
		 */
		public static function updateStatement($table, $entity, $data) {
			
			foreach ($data as $key => $value) {
				$fields[] = "$key = :$key";
			}
	
			$fields = join(', ', $fields);
			
			return "
				UPDATE $table SET 
					$fields
				WHERE $entity = :$entity
			";
		}
	}