<?php 

	class Template {
		
		private $template;
		private $data;
		public $modul;
		public $placeholder;
		
		public function __construct($placeholder=null) {
			$this->placeholder = $placeholder;
			$this->settings = Settings::init();
			$this->translate = Translate::instance();
		}
		
		public function __set($name, $value) {
			$this->data[$name] = $value;
		}
		
		public function __get($param) {
			return $this->data[$param];
		}
		
		public function __call($modul, $arguments) {
			$this->modul = $modul;
			$this->template($arguments[0]);
			return $this;
		}
		
		public function __toString() {
			return $this->show();
		}
		
		public function template($template) {
			
			$slashes = explode('/', $template);
			$modul = $this->modul ? $this->modul."." : null;

			$this->template = (count($slashes) > 1)
				? $template
				: PATH_VIEWS.$modul.$template.".inc";

			return $this;
		}
		
		public function data($param, $value=null) {
			if (is_array($param)) foreach ($param as $key => $value) $this->data[$key] = $value;
			else $this->data[$param] = $value;
			
			return $this;
		}
		
		public function show() {
			
			$this->template = ($this->template) ? $this->template : PATH_VIEWS.$this->placeholder.".inc";
			
			if (file_exists($this->template)) {
				ob_start();
				if ($this->data) extract($this->data, EXTR_SKIP);
				require $this->template;
				$content = ob_get_contents();
				ob_end_clean();
				return $content;
			}
			// @todo wouldn't it make sense to throw an error here?
			else {

			}
		}

		public function render() {
			return $this->show();
		}
		
		public static function load($template, $data=array()) {
			
			$t = new Template();
			$t->template($template);
			
			if ($data) {
				$t->data($data);
			}
			
			return $t->show();
		}
	}