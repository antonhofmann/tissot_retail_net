<?php 

	class spreadsheet {
		
		public static function index($column, $row=null) {
			
			$numeric = $column % 26;
		    $letter = strtolower(chr(65 + $numeric));
		    $num2 = intval($column / 26);

		    if ($num2 > 0) return self::index($num2 - 1, $row) . $letter.$row;
		    else return $letter.$row;
		}

		public static function key($column, $row=null) {
			
			$letters = range('a', 'z');
			$total_letters = count($letters);
			$trigger = ceil($column/$total_letters) - 1;
			$key = $column - (26 * $trigger);
			
			if ($column < $total_letters + 1) return $letters[$key-1].$row;
			else return $letters[$trigger-1].$letters[$key-1].$row;	
		}
	}