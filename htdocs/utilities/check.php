<?php 

	class check {
		
		public static function email($param) {
			return (filter_var($param, FILTER_VALIDATE_EMAIL)) ? true : false;
		}
		
		public static function alphanumeric($param) {
			return (preg_match('/^[a-z0-9]+$/i', $param)) ? true : false;
		}
		
		public static function char($param) {
			return (preg_match('/^[A-Za-z]+$/i', $param)) ? true : false;
		}
		
		public static function numeric($param) {
			return (preg_match('/^[\.0-9]+$/',$param)) ? true : false;
		}
		
		public static function integer($param) {
			return (preg_match('/^[0-9]+$/',$param)) ? true : false;
		}
		
		public static function float($param) {
			return (is_float($param)) ? true : false;
		}
		
		public static function string($param) {
			return (is_string($param)) ? true : false;
		}
		
		public static function url($param) {
			return (preg_match("#^www\.[a-z0-9_.-]+\.[a-z]{2,4}$#i", trim($param))) ? true : false;
		}
		
		public static function dateformat($param) {
			if ($param == '00.00.0000') return false;
			else return (preg_match("/^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}$/", $param)) ? true : false;
		}
		
		public static function timeformat($param) {
			if ($param && preg_match("/^(\d\d):(\d\d)(?::(\d\d))?$/", trim($param), $matches)) {
				if ($matches[1] < 24 && $matches[2] < 60 && (count($matches) == 3 || $matches[3] < 60)) return true;
				else return false;
			}
		}
		
		public static function image($file) {
			$file = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], '', $file);
			$info = pathinfo($file);
			$images = array('gif', 'jpg', 'jpeg', 'png', 'bmp', 'tif');
			return in_array($info['extension'], $images) ? true : false;
		}
		
		public static function pdf($file) {
			$file = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], '', $file);
			$info = pathinfo($file);
			return $info['extension']=='pdf' ? true : false;
		}
		
		public static function phone($param) {
			return (preg_match('/^\+\d{1,3}[\s()-]*(\d[\s()-]*){6,15}(x\d*)?$/', $param)) ? true : false;
		}
		
		public static function mobile_phone($param) {
			return (preg_match('/^\+\d{1,3}[\s()-]*(\d[\s()-]*){6,15}(x\d*)?$/', $param)) ? true : false;
		}

		public static function isCorruptedPDF($file) {
			
			$file = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], '', $file);

			if (!file_exists($file)) {
				return true;
			}
			
			if (!static::pdf($file)) return true;

			$fp = fopen($file, 'r');
			fseek($fp, 0);
			$data = fread($fp, 5);

			return $data=="%PDF-" ? false : true;
		} 

		public function json($string) {
			json_decode($string);
			return (json_last_error() == JSON_ERROR_NONE);
		}
		
	}