<?php

	class Bootstrap {
		
		/**
		 * Controllers who will skip this safety access mode
		 * @var array
		 */
		public static $startup_controllers = array(
			'security',
			'messages',
			'download'
		);

		/**
		 * Controllers who will skip this safety access mode
		 * @var array
		 */
		public static $public_controllers = array(
			'news'
		);
		
		public static function page_not_found() {
		
			$applications = Application::load(array('application_active = 1'));
			$current = url::application();
			
			if ($applications[$current]) {
				url::redirect('/messages/error/page_not_found');
			} else {
				url::redirect('/page_not_found.php');
			}
		}
		
		public static function statistic() {
				
			$model = new Model(Connector::DB_CORE);
			$ip = url::ip();
			$request = $_SERVER['SCRIPT_FILENAME'].'/'.url::get();
			$diff = round(time() - START_TIME);
			
			
			$model->db->exec("
				INSERT INTO statistics (
					statistic_user, statistic_ip, statistic_date, statistic_url, statistic_duration
				) VALUES (
					'".$_SESSION['user_id']."', '$ip', NOW(), '$request', '$diff'
				) 		
			");
		}
	}