<?php

	class url {

		public static function application() {
			return array_shift(explode('/', self::get()));
		}

		public static function first_controller() {
			
			$request = explode('/', self::get());
			
			if (file::isController($request[0])){
				return $request[0];
			}
		}
		
		public static function controller() {
			$request = explode('/', self::get());
			if (file::isController($request[0])) $controller =  $request[0];
			else $controller = (file::isController($request[1])) ? $request[1] : null;
			return $controller;
		}

		public static function action() {
			
			$request = explode('/', self::get());

			if (file::isController($request[0])) {
				$action = $request[1];
			} else {
				$action = (file::isController($request[1])) ? $request[2] : $request[1];
			}
			
			return ($action) ? $action : "index";
		}
		
		public static function param($i=0) {
			$params = self::params();
			return $params[$i];
		}
		
		public static function params() {
			
			$url = explode('/',self::get());
			$action = self::action();
			if (self::action() == 'archived') $slice = 4;
			else {
				if (file::isController($url[1])) $slice = 3;
				elseif (file::isController($url[0])) $slice = 2;
			}
			
			return array_slice($url,$slice);
		}
		
		public static function isParams($param) {
			$params = explode('/',self::get());
			return ($params[$param]) ? $params[$param] : null;
		}
		
		public static function request(){
			$request = array_slice(explode('/', self::get()),0,3);
			$request = (is_numeric(end($request))) ? array_slice($request,0,2) : $request;
			return join('/',$request);
		}
		
		public static function system_request($url=null){
			
			$url = $url ?: self::get();
			$url = explode('/', $url);
			$isString = '/[A-Za-z]/';
			
			if ($url[0] && preg_match($isString, $url[0])) {
				$request[] = $url[0];
			}
			
			if ($url[1] && preg_match($isString, $url[1]) ) {
				$request[] = $url[1];
			}
			
			if ($url[2] && preg_match($isString, $url[2]) ) {
				
				if ($url[2]=='archived') {
					
					$request[] = $url[2];
					
					if ($url[3] && preg_match($isString, $url[3]) ) {
						$request[] = $url[3];
					}
					
				} elseif ( preg_match($isString, $url[2]) ) {
					$request[] = $url[2];
				}
			}
			
			return ($request) ? join('/', $request) : null;
		}
		
		public static function protocol() {
			if(!empty($_SERVER['HTTP_X_FORWARDED_PROTO']))
		        return $_SERVER['HTTP_X_FORWARDED_PROTO'];
		    else 
		        return !empty($_SERVER['HTTPS']) ? "https" : "http";
		}
	
		public static function redirect($url) {
			@header("Location: ".$url);
			exit;
		}

		/**
		 * Send a http header with a status code
		 * Message is added automatically based on code
		 * @param  integer  HTTP status code
		 * @return void
		 */
		public static function header($code) {
			// https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
			$codeMessageMap = array(
				400 => 'Bad Request',
				403 => 'Forbidden',
				404 => 'Not Found',
				500 => 'Internal Server Error',
				503 => 'Service Unavailable',
				);

			header(sprintf('HTTP/1.0 %d %s',
				$code,
				isset($codeMessageMap[$code]) ? $codeMessageMap[$code] : ''
				));
		}
	
		public static function get() {
			return ($_GET['url']) ? rtrim($_GET['url'], '/') : 'index';
		}
		
		public static function current() {
			return ($_GET['url']) ? '/'.rtrim($_GET['url'], '/') : null;
		}
	
		public static  function ip () {
			if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
				$ips = $_SERVER['HTTP_X_FORWARDED_FOR'];
				if(is_array($ips)) {
					$i = count($ips);
					$ip = $ips[$i-1];
				}
				else {
				
					if(strpos($ips, ',') > 0) {
						$tmp = explode(', ', $ips);
						$ip = $tmp[count($tmp) - 1];
					}
					else {
						$ip = $ips;
					}
				
				}
			} else $ip = $_SERVER['REMOTE_ADDR'];
			

			return $ip;
		}
	
		public static function no_access_application() {
			$id = Message::id('no_access_application');
			self::redirect("/message/show/$id");
		}
		
		public static function invalid_request() {
			$id = Message::id('invalid_request');
			self::redirect("/message/show/$id");
		}
	
		public static function full() {
			$protocol = self::protocol();
			return "$protocol://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		}

		/**
		 * Get the current host url with appropriate protocol
		 * @return string
		 */
		public static function host() {
			if (isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])) {
				return sprintf('%s://%s',
					self::protocol(),
					$_SERVER['HTTP_HOST']
					);	
			}
			else {
				$settings = Settings::init();
				return sprintf('%s://%s',
					$settings->default_protocol,
					$settings->default_host
					);
			}
		}
	
		public static function build($link, $request) {
			return $link."?".http_build_query($request);
		}
	
		public static function server() {
			
			$server = explode('.',$_SERVER['SERVER_NAME']);
			
			if (count($server) > 1) {
				$server = array_slice($server, -2);
				return join('.', $server);
			} else {
				return 'localhost';
			}
		}
		
		public static function isAjax() {
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				return true;
			}
		}
	}