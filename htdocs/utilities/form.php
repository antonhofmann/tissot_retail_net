<?php

	class Form {

		const PARAM_REQUIRED = "param=required";
		const PARAM_DISABLED = "param=disabled";
		const PARAM_HIDDEN = "param=hidden";
		const PARAM_AJAX= "param=ajax";
		const PARAM_HORIZONTAL = "param=horizontal";
		const PARAM_VERTICAL = "param=vertical";
		const PARAM_LTR = "param=ltr";
		const PARAM_RTL = "param=rtl";
		const PARAM_LABEL = "param=label";
		const PARAM_NO_LABEL = "param=nolabel";
		const PARAM_NO_SELECT_EMPTY = "param=no_select_empty";
		const PARAM_SHOW_VALUE = "param=show_value";
		const PARAM_SHOW_ONLY_SELECTED = "param=show_only_selected";
		const TYPE_TEXT = "type=text";
		const TYPE_TEXT_GROUP = "type=textgroup";
		const TYPE_PASSWORD = "type=password";
		const TYPE_HIDDEN = "type=hidden";
		const TYPE_CHECKBOX = "type=checkbox";
		const TYPE_CHECKBOX_GROUP = "type=checkboxgroup";
		const TYPE_RADIO = "type=radio";
		const TYPE_SUBMIT = "type=submit";
		const TYPE_RESET = "type=reset";
		const TYPE_FILE = "type=file";
		const TYPE_BUTTON = "type=button";
		const TYPE_SELECT = "type=select";
		const TYPE_TEXTAREA = "type=textarea";
		const TYPE_WYSIWYG = "type=wysiwyg";
		const TYPE_AJAX = "type=placeholder";
		const TYPE_PLACEHOLDER = "type=placeholder";
		const TOOLTIP_EMAIL = "tooltip=email";
		const TOOLTIP_URL = "tooltip=url";
		const TOOLTIP_PHONE = "tooltip=phone";
		const TOOLTIP_INTEGER = "tooltip=integer";
		const TOOLTIP_FLOAT = "tooltip=float";
		const TOOLTIP_STRING = "tooltip=string";
		const TOOLTIP_NUMERIC = "tooltip=numeric";
		const TOOLTIP_NUMBER_DECIMAL = "tooltip=number_decimal";
		const TOOLTIP_NUMBER_DECIMAL_2 = "tooltip=number_decimal_2";
		const TOOLTIP_ALPHANUMERIC = "tooltip=alphanumeric";
		const TOOLTIP_FILENAME = "tooltip=filename";
		const TOOLTIP_TIME = "tooltip=time";
		const TOOLTIP_DATE = "tooltip=date";
		const TOOLTIP_DATETIME = "tooltip=datetime";
		const TOOLTIP_NOT_NULL = "tooltip=not_null";
		const TOOLTIP_NOT_ZERO = "tooltip=not_zero";
		const TOOLTIP_INDICATE_ENGLISH = "tooltip=indicate_english";
		const TOOLTIP_PERSONAL_CLASSIFICATION = "tooltip=personal_classification";
		const TOOLTIP_LEAST_ONE_LETTER_NUMBER = "tooltip=least_one_letter_number";
		const TOOLTIP_NOT_SPECIAL_CHARACTERS = "tooltip=not_special_characters";
		const TOOLTIP_NOT_INTERNATIONAL_LETTERS = "tooltip=not_international_letters";
		const TOOLTIP_LENGTH_EIGHT_CHARACTERS = "tooltip=length_eight_characters";
		const TOOLTIP_USE_LETETRS_NUMBERS = "tooltip=use_letters_numbers";
		const TOOLTIP_PASSWORD_NOT_PART_USERNAME = "tooltip=password_not_part_username";

			
		public function __construct($attributes=null) {
				
			$attributes['method'] = ($attributes['method']) ? $attributes['method'] : "post";
			
			if (!$attributes['theme']) {
				$settings = settings::init();
				$attributes['class'] = ($attributes['class']) ? $attributes['class'].' '.$settings->theme : $settings->theme;
			}
			
			$this->form = $attributes;
			$this->form['accept-charset'] = 'utf-8';
			$this->validationEngine = "validationEngine";
		}

		/**
		 * Overloading
		 * @param string $field Field Name
		 * @param param $arguments
		 */
		public function __call($field,$arguments) {
			$this->addField($field, $arguments);
		}

		/**
		 * Add a field to the form
		 * @param string $field
		 * @param array $arguments
		 */
		public function addField($field, $arguments) {
			$this->fields[$field]['name'] = $field;
			$this->fields[$field]['id'] = $field;

			if ($arguments) {
				foreach ($arguments as $argument) {
					
					list($attribute,$value) = explode('=',$argument);
					$attribute = trim($attribute);
					$value = trim($value);
					
					if ($attribute=="param") $this->params[$field][] = $value;
					elseif ($attribute=="validata") $this->validata[$field][] = $value;
					elseif ($attribute=="tooltip") $this->tooltips[$field][] = $value;
					elseif ($attribute=="label") $this->labels[$field] = $value;
					else $this->fields[$field][trim($attribute)] = trim($value);
					
					if ($value == "required" ) {
						$this->fields[$field]['required'] = 'required';
					}
				}
			}
		}

		/**
		 * Set Field Parameter
		 * @param string $field Field Name
		 * @param string $param Paramaeter Value (format: param=value)
		 */
		public function param($field, $param) {
			$this->params[$field][] = trim(end(explode('=',$param)));
		}

		/**
		 * Set Validator Rule
		 * @param string $field Field Name
		 * @param string $param Parameter (format: param=value)
		 */
		public function validata($field, $param) {
			$this->validata[$field][] = trim(end(explode('=',$param)));
		}

		/**
		 * Define Validator Engine
		 * @param string $engineName Validator Name
		 */
		public function setValidationEngine($engineName) {
			$this->validationEngine = $engineName;
		}

		/**
		 * Build All Form Elements <br />
		 * Generate hide field with all visible fields (comma separated)
		 */
		public function render() { 

			$attributes = _array::rend_attributes($this->form);

			$form .= "<form $attributes>";

			if ($this->fields) {

				// load form tooltips
				if ($this->tooltips) {
					Translate::instance()->load('tooltip');
				}

				// group fields in sets
				$fields = $this->groupFields();

				foreach ($fields as $key => $field) {

					if (is_array($field)) {

						$fieldCollection = null;

						foreach ($field as $fieldname) {

							if (!$this->isHidden($fieldname)) {
								
								if ($fieldCollection) $this->_bg = ($this->_bg == '-odd') ? '-even' : '-odd';
								else  $this->_bg = '-odd';
								
								$type = ($this->fields[$fieldname]['type']) ? "_".$this->fields[$fieldname]['type'] : "_text";
								$fieldCollection .= $this->$type($fieldname, $bg);
							}
						}

						if ($fieldCollection) {
							$legend = (Translate::instance()->$key) ? Translate::instance()->$key : $key;
							$form .= "<div class='-legend'><strong>$legend</strong></div>";
							$form .= "<div class='-fieldset $key'>$fieldCollection</div>";
						}
					}
					else {
						$type = ($this->fields[$field]['type']) ? "_".$this->fields[$field]['type'] : "_text";
						$form .= $this->$type($field);
					}
				}

				// form actions
				if ($this->buttons) {
					$form .= "<div class='actions'>".join($this->buttons)."</div>";
				}
				
				// assign al visible fields to form
				// is userfull for chackboxs, radioboxes and ajax loaders
				if (is_array($this->visible)) {
					$visible_fields = array_unique($this->visible);
					$visible_fields = join(',', $visible_fields);
					$form .= "<input type=hidden name=fields value='$visible_fields' >";
				}
				
				$form .= "</form>";
			}

			return $form;
		}

		/**
		 * Extend Field Content
		 * @param string $field Field name
		 * @param string $content html content
		 */
		public function extended($field, $content) {
			$this->extended[$field] = $content;
		}
		
		/**
		 * Set Label Caption
		 * @param string $field Field name
		 * @param string $label Caption
		 */
		public function caption($field, $label) {
			$this->labels[$field] = $label;
		}

		/**
		 * Form Button Action
		 * @param string $name Button Name
		 * @param array $attributes Button Attributes
		 */
		public function button($button) {
			$this->buttons[] = $button;
		}

		/**
		 * Set Field Attributes
		 * @param string $field Field Name
		 * @param string $attribute html attributes (format: name=value)
		 */
		public function attribute($field, $attribute) {
			list($attribute,$value) = explode('=',$attribute);
			$attribute = trim($attribute);
			$value = trim($value);
			$this->fields[$field][$attribute] = $value;
		}

		/**
		 * Include Form Error
		 * @param string $field Field Name
		 * @param string $error Error name
		 */
		public function errors($field, $error) {
			$this->errors[$field] = $error;
		}

		/**
		 * Data Loader
		 * @param array $data
		 */
	 	public function dataloader($data) { 
	 		if(is_array($data)) {
		 		foreach ($data as $key => $value) {
		 			if (is_array($value)) $this->data[$key] = $value;
		 			elseif($this->fields[$key]) $this->fields[$key]['value'] = $value;
		 		}
	 		}
	    }

	    /**
	     * Set Field Collection
	     * @param string $name Fieldset name
	     * @param array $fields Fields
	     */
		public function fieldset($name, $fields) {
			if (is_array($this->fieldset[$name]) && count($this->fieldset[$name])) {
				if (is_array($fields)) {
					$this->fieldset[$name] = array_merge($this->fieldset[$name], $fields);
				}
				else {
					$this->fieldset[$name][] = $fields;
				}
			}
			else {
				$this->fieldset[$name] = $fields;
			}
		}

		/**
		 * Check is field disabled
		 * @param string $field Field Name
		 * @return boolean true/false
		 */
		public function isDisabled($field) {
			return ($this->params[$field] && in_array('disabled', $this->params[$field])) ? true : false;
		}
		
		/**
		 * Check is field hide
		 * @param string $field Field Name
		 * @return boolean true/false
		 */
		public function isHidden($field) {
			return ($this->params[$field] && in_array('hidden', $this->params[$field])) ? true : false;
		}
		
		
//**************************************************************************************************************************************	
//		PRIVATE PART
//**************************************************************************************************************************************

		/**
		 * AJAX Placeholder
		 * @param string $field, Field Name
		 * @return string Form Row whith Lable and Element
		 */
		private function _placeholder($field) {
			
			// label
			$label = $this->label($field);
			
			// extend content
			$extend = $this->extended[$field];
			
			// bg
			$bg = $this->_bg;
			
			// value
			$value = ($this->params[$field] && in_array('show_value', $this->params[$field])) ? $this->fields[$field]['value'] : null;
			
			// attributes
			unset($this->fields[$field]['value']);
			$attributes = $this->buildAttributes($field);
			
			// element
			$element = "<div class='-element -placeholder'><div $attributes >$value</div>$extend</div>";
			
			// set element as visible
			$this->visible[] = $field;
			
			return "<div class='-row $field $bg'>$label $element</div>";
		}

		/**
		 * INPUT Type Text
		 * @param string $field, Field Name
		 * @return string Form Row whith Lable, Element, Tooltip and Etend Content
		 */
		private function _text($field) {
			
			// element label
			$label = $this->label($field);
			
			// bg
			$bg = $this->_bg;
			
			if ($this->isDisabled($field)) {
				$caption = $this->fields[$field]['value'];
				$element = "<div class='-element -text -disabled $bg' id='$field' >$caption</div>";
			} else {
				
				// set element as visible
				$this->visible[] = $field;
				
				// tooltip
				$tooltip = $this->tooltip($field);
				
				// atributes
				$attributes = $this->buildAttributes($field);
				
				// extend content
				$extend = $this->extended[$field];
				
				$element = "<div class='-element -text $bg'><input $attributes /></div>";
			}

			return "<div class='-row $field $bg'>$label $element $tooltip $extend </div>";
		}
		
		/**
		 * Input Type Text <br />
		 * Generate textbox elements from dataloader
		 * @return string Form Row whith Lable, Element, and Tooltip
		 */
		public function _textgroup($field) {
			
			if ($this->data[$field]) {

				// remove id attrinute
				unset($this->fields[$field]['id']);
				
				// define as text element
				$this->fields[$field]['type'] = 'text';
				
				$extend = $this->extended[$field];
				
				foreach ($this->data[$field] as $fieldname => $caption) {
					
					$bg = $this->_bg;
					$this->_bg = ($this->_bg == '-odd') ? '-even' : '-odd';
					
					$elements .= "<div class='-row $fieldname $bg'>"; 
					
					// label caption
					$elements .= $this->label($fieldname);
					
					if ($this->isDisabled($fieldname)) {
						$elements .= "<div class='-element -text -disabled'  >$caption</div>";
					} else {
						
						// build attributes
						$this->fields[$field]['value'] = $_POST[$field][$fieldname] ? $_POST[$field][$fieldname] : $caption;
						$this->fields[$field]['name'] = $field.'['.$fieldname.']';
						$this->fields[$field]['id'] = $fieldname;
						$attributes = $this->buildAttributes($field);
						
						// set element as visible
						$this->visible[] = $field;
						$this->visible[] = $fieldname;
						
						$elements .= "<div class='-element -text'><input $attributes />$extend</div>";
					}
					
					$elements .= "</div>";
				}
			} elseif (!$this->isDisabled($field)) {
				
				$elements .= "<div class='-row $fieldname $bg'>";
				$elements .= $this->label($field);
				$elements .= "<div class='-element -text'></div>";
				$elements .= "</div>";
			}
			
			return "<div class=textbox-group >$elements</div>";
		}

		/**
		 * Input Type Password
		 * @param string $field
		 * @return string Form Row whith Lable, Element, Tooltip and Extend Content
		 */
		private function _password($field) {

			// element label
			$label = $this->label($field);
			
			// bg
			$bg = $this->_bg;
			
			if ($this->isDisabled($field)) {
				$caption = $this->fields[$field]['value'];
				$element = "<div class='-element -password -disabled'>$caption</div>";
			} else {
				
				// set element as visible
				$this->visible[] = $field;
				
				// tooltip
				$tooltip = $this->tooltip($field);
				
				// atributes
				$attributes = $this->buildAttributes($field);
				
				// extend content
				$extend = $this->extended[$field];
				
				$element = "<div class='-element -password'><input $attributes /></div>";
			}

			return "<div class='-row $field $bg'>$label $element $tooltip $extend</div>";
		}

		/**
		 * Input Type File
		 * @param string $field
		 * @return string Form Row whith Lable, Element, Tooltip and Extend Content
		 */
		private function _file($field) {
			
			// element label
			$label = $this->label($field);
			
			// bg
			$bg = $this->_bg;
			
			if ($this->isDisabled($field)) {
				$caption = $this->fields[$field]['value'];
				$element = "<div class='-element -file -disabled'>$caption</div>";
			} else {
				
				// set element as visible
				$this->visible[] = $field;
				
				// tooltip
				$tooltip = $this->tooltip($field);
				
				// atributes
				$attributes = $this->buildAttributes($field);
				
				// extend content
				$extend = $this->extended[$field];
				
				$element = "<div class='-element -file'><input $attributes /></div>";
			}

			return "<div class='-row $field $bg'>$label $element $tooltip $extend</div>";
		}

		/**
		 * Input Type Hidden
		 * @param string $field
		 * @return Hidden Element
		 */
		private function _hidden($field) {
			$attributes = $this->buildAttributes($field);
			$this->visible[] = $field;
			return "<input $attributes />";
		}

		/**
		 * Submit Button
		 * @param string $field
		 * @return string Submit Button
		 */
		private function _submit($field) {
			
			$attributes = $this->buildAttributes($field);
			$attributes['value'] = (Translate::instance()->$field) ? Translate::instance()->$field : Translate::instance()->submit;
			
			// set element as visible
			$this->visible[] = $field;
			
			return "<input $attributes />";
		}

		/**
		 * Reset Button
		 * @param string $field
		 * @return string Reset Button
		 */
		private function _reset($field) {
			
			$attributes = $this->buildAttributes($field);
			$attributes['value'] = (Translate::instance()->$field) ? Translate::instance()->$field : Translate::instance()->reset;
			
			// set element as visible
			$this->visible[] = $field;
			
			return "<input $attributes />";
		}

		/**
		 * Button Element
		 * @param string $field
		 * @return string Reset Button
		 */
		private function _button($field) {
			
			$attributes = $this->buildAttributes($field);
			$caption = Translate::instance()->$field;
			
			// set element as visible
			$this->visible[] = $field;
			
			return "<button $attributes >$caption</button>";
		}

		/**
		 * Input Type Checkbox
		 * @param string $field
		 * @return Form Row with Element, Tooltip and Extend Content 
		 */
		private function _checkbox($field) {
			
			$value = ($_POST[$field]) ? $_POST[$field] : $this->fields[$field]['value'];
			
			// show label
			if ($this->params[$field] && in_array('label', $this->params[$field])) {
				$label = $this->label($field);
			} else {
				$label = "<label></label>";
			}
			
			// bg
			$bg = $this->_bg;

			// checkbox caption
			$caption = ($this->fields[$field]['caption']) ? $this->fields[$field]['caption'] : Translate::instance()->$field;
			$caption_data = ($this->fields[$field]['recaption']) ? 'data="'.$this->fields[$field]['recaption'].'"' : null;

			if ($this->isDisabled($field)) {

				$showOnlySelected = $this->params[$field] && in_array('show_only_selected', $this->params[$field]) ? true : false;
				$checked = ($value) ? "-checked" : "-unchecked";
				$placeholder = "<span class='-caption $checked' id='$field' >$caption</span>";
				$content = $showOnlySelected && !$value ? null : $placeholder;
				
				if ($content) $element .= "<div class='-element -checkbox'>$content</div>";
				else $label = $this->label($field);
			}
			else {
				
				// set element as visible
				$this->visible[] = $field;
				
				unset($this->fields[$field]['caption']);
				unset($this->fields[$field]['recaption']);
				
				// attributes
				$this->fields[$field]['value'] = 1;
				$attributes = $this->buildAttributes($field);
				$checked = ($value) ? "checked=checked" : null;
				
				// tooltip
				$tooltip = $this->tooltip($field);
				
				// extend content
				$extend = $this->extended[$field];
				
				$element = "<div class='-element -checkbox'><input $attributes $checked /><span class='-caption' $caption_data >$caption</span></div>";
			}

			return "<div class='-row $field $bg'>$label $element $tooltip $extend</div>";
		}

		/**
		 * Input Type Checkbox <br />
		 * Generate Checkbox element from Dataloader
		 * @param string $field Field Name
		 * @return Form Row with Elements
		 */
		private function _checkboxgroup($field) {

			if ($this->data[$field]) {
				
				$label = $this->label($field);
				
				// param disabled
				$disabled = $this->isDisabled($field);

				// show only selected
				$showOnlySelected = $this->params[$field] && in_array('show_only_selected', $this->params[$field]) ? true : false;
				
				// bg
				$bg = $this->_bg;
				
				// set element as visible
				if (!$disabled) {
					$this->visible[] = $field;
				}

				// curreent values
				$values = ($_POST[$field]) ? array_keys($_POST[$field]) : unserialize($this->fields[$field]['value']);
				$values = ($values) ? $values : array();
				
				// element orientation
				$direction = ($this->fields[$field]['direction']) ? $this->fields[$field]['direction'] : 'vertical';
				
				// reset attributes
				$this->fields[$field]['type'] = 'checkbox';
				$this->fields[$field]['class'] = $this->fields[$field]['class']." $field";

				// revent id attribute for array element
				unset($this->fields[$field]['id']);

				foreach ($this->data[$field] as $value => $row) {

					$fieldAttributes = null;
					$caption = null;
					
					if (is_array($row)) {
						
						$caption = $row['caption'];
						
						if (is_array($row['attributes'])) {
							$fieldAttributes = _array::rend_attributes($row['attributes']);
						}
						
					} else {
						$caption = $row;
					}
					
					if ($disabled) {
						
						if ($showOnlySelected) {

							if (in_array($value, $values)) {
								$element .= "<div class='-element -checkbox $direction'><span class='-caption -checked'>$caption</span></div>";
							}

						} else {
							$checked = (in_array($value, $values)) ? "-checked" : "-unchecked";
							$element .= "<div class='-element -checkbox $direction'><span class='-caption $checked'>$caption</span></div>";
						}
					}
					else {
						
						// attributes
						$this->fields[$field]['value'] = $value;
						$this->fields[$field]['name'] = $field.'['.$value.']';
						$attributes = $this->buildAttributes($field);
						
						$checked = (in_array($value, $values)) ? "checked=checked" : null;
						$element .= "<div class='-element -checkbox $direction'><input $attributes $checked $fieldAttributes /><span class='-caption'>$caption</span></div>";
					}
				}

				return "<div class='-row $field $bg'>$label <div class='checkbox-group'>$element</div></div>";
			}
		}

		/**
		 * Input Type Radio <br />
		 * Visible only with Dataloader values
		 * @param string $field Field Name
		 * @return Form Row with Label and Element
		 */
		private function _radio($field) {

			if ($this->data[$field]) {
				
				// element label
				$label = $this->label($field);
				
				$current = ($_POST[$field]) ? $_POST[$field] : $this->fields[$field]['value'];
				
				// bg
				$bg = $this->_bg;
					
				// build attrinutes
				unset($this->fields[$field]['id']);
				unset($this->fields[$field]['value']);
				$attributes = $this->buildAttributes($field);
					
				// orientation
				$direction = ($this->fields[$field]['direction']) ? $this->fields[$field]['direction'] : 'horizontal';

				// show only selected
				$showOnlySelected = $this->params[$field] && in_array('show_only_selected', $this->params[$field]) ? true : false;
				
				if ($this->isDisabled($field)) {
					
					foreach ($this->data[$field] as $value => $caption) {

						if ($showOnlySelected) {

							if ($value==$current) {
								$radio .= "<span class='-caption -checked'>$caption</span>";
							}

						} else {
							$checked = ($value == $current) ? "-checked" : "-unchecked";
							$radio .= "<span class='-caption $checked'>$caption</span>";
						}

					}

					if ($radio) {
						$element .= "<div class='-element -radio $direction'>$radio</div>";
					}
				}
				else {
					
					// set element as visible
					$this->visible[] = $field;
					
					foreach ($this->data[$field] as $value => $caption) {
						
						$checked = ($current==$value) ? "checked=checked" : null;
						$value = " value=$value";
						
						$radio .= "
							<div class='elembox'>
								<input $attributes $value $checked />
								<span class='-caption'>$caption</span>
							</div>
						";
					}

					$element .= "<div class='-element -radio $direction'>$radio</div>";
				}
			}

			return $element ? "<div class='-row $field $bg'>$label $element</div>" : null;
		}

		/**
		 * From Element Textarea
		 * @param string $field Field Name
		 * @return string Form Row with Label, Element and Extend Content
		 */
		private function _textarea($field) {

			// set content
			if ($this->fields[$field]['value']) {
				$content =$this->fields[$field]['value'];
				unset($this->fields[$field]['value']);
			}

			// show label
			if ($this->params[$field] && in_array('label', $this->params[$field])) {
				$label = $this->label($field);
			}
			
			if ($this->isDisabled($field)) {
				$element = "<div class='-element -textarea -disabled' id='$field' >".nl2br($content)."</div>";
			} else {
				
				// set element as visible
				$this->visible[] = $field;
				
				// build attributes
				$attributes = $this->buildAttributes($field);
				
				// extend content
				$extend = $this->extended[$field];
				
				$element = "<div class='-element -textarea'><textarea $attributes>$content</textarea></div>";
			}
			
			// bg
			$bg = $this->_bg;

			return "<div class='-row $field $bg'>$label $element $extend</div>";
		}

		/**
		 * From Element WYSIWYG
		 * @param string $field Field Name
		 * @return string Form Row with Label, Element and Extend Content
		 */
		private function _wysiwyg($field) {

			// set content
			if ($this->fields[$field]['value']) {
				$content =$this->fields[$field]['value'];
				unset($this->fields[$field]['value']);
			}

			// show label
			if ($this->params[$field] && in_array('label', $this->params[$field])) {
				$label = $this->label($field);
			}
			
			if ($this->isDisabled($field)) {
				$element = "<div class='-element -textarea -wysiwyg -disabled' id='$field' >".nl2br($content)."</div>";
			} else {
				
				// set element as visible
				$this->visible[] = $field;
				
				// build attributes
				$attributes = $this->buildAttributes($field);
				
				// extend content
				$extend = $this->extended[$field];
				
				$element = "<div class='-element -textarea -wysiwyg'><textarea $attributes>$content</textarea></div>";
			}
			
			// bg
			$bg = $this->_bg;

			return "<div class='-row $field $bg'>$label $element $extend</div>";
		}

		/**
		 * Form Element Select <br />
		 * Visible only with Dataloader values
		 * @param string $field Field Name
		 * @return string Form Row with Label, Element, Tooltip and Extend Content
		 */
		private function _select($field) {

			// label
			$label = $this->label($field);
			
			// current value
			$current_value = ($_POST[$field]) ? $_POST[$field] : $this->fields[$field]['value'];
			
			// bg
			$bg = $this->_bg;
			
			// extend content
			$extend = $this->extended[$field];
			
			if ($this->isDisabled($field)) {
				$caption = ($this->data[$field][$current_value]) ? $this->data[$field][$current_value] : null;
				$element = "<div class='-element -select -disabled' id='$field'>$caption</div>";
			} else {
				
				// param ajax
				$param_ajax = ($this->params[$field] && in_array('ajax', $this->params[$field])) ? true : false;
				
				// set element as visible
				$this->visible[] = $field;
				
				if ($param_ajax) { 
					$this->fields[$field]['title'] = $current_value; 
				}
				
				// attribute
				$attributes = $this->buildAttributes($field);
				
				// tooltip
				$tooltip = $this->tooltip($field);

				$element = "<div class='-element -select'><select $attributes >";
				
				$emptySelect = $this->params[$field] && in_array('no_select_empty', $this->params[$field]) ? false : true;

				if ($emptySelect) {
					$element .= "<option value=''>".Translate::instance()->select."</option>";
				}

				if($this->data[$field] && !$param_ajax) {
					
					$extended_attributes = $this->extended_attributes[$field];
					
					foreach ($this->data[$field] as $value => $caption) {
						$attrExt = ($extended_attributes[$value]) ? _array::rend_attributes($extended_attributes[$value]) : null;
						$selected = ($current_value == $value) ? "selected=selected" : null;
						$element .= "<option value='$value' $selected $attrExt >$caption</option>";
					}
				}

				$element .= "</select></div>";
			}

			return "<div class='-row $field $bg'>$label $element $tooltip $extend</div>";
		}

		/**
		 * Build Form Element Attributes
		 * @param string $field Field Name
		 * @return string HTML attributes
		 */
		private function buildAttributes($field=null) {
			
			if (!$this->fields[$field]['alt']) {
				$this->fields[$field]['alt'] = Translate::instance()->$field;
			}

			if ($this->fields[$field] && isset($_POST[$field])) {
				$this->fields[$field]['value'] = $_POST[$field];
			}

			if ($this->validata[$field] && $this->validationEngine) {
				$validator = $this->validationEngine;
				$this->fields[$field]['class'] .= " ".$this->$validator($field);
			}

			if ($this->params[$field] && in_array('required', $this->params[$field])) {
				$this->fields[$field]['tag'] = "required";
			}

			// XSS prevention
			if (in_array($this->fields[$field]['type'], array('text', 'password', 'textarea')) ) {
				$this->fields[$field]['value'] = htmlspecialchars(strip_tags($this->fields[$field]['value']), ENT_QUOTES, "UTF-8");
			}

			return _array::rend_attributes($this->fields[$field]);
		}

		/**
		 * Parse validators
		 * @param string $field Field Name
		 * @return string Validator class
		 */
		private function validationEngine($field) {
			if ($this->validata[$field]) {
				return "validate[".join(',', $this->validata[$field])."]";
			}
		}

		/**
		 * Form Label Element 
		 * @param string $field Field Name
		 * @return Label
		 */
		private function label($field) {
			
			if ($this->params[$field]) {
				$show_label = (in_array('nolabel', $this->params[$field])) ? false : true;
			} else {
				$show_label = true;
			}
			
			if ($show_label) {
				$disabled = $this->isDisabled($field);
				$required = (!$disabled && $this->validata[$field] && in_array('required', $this->validata[$field])) ? " <b>*</b>" : null;
				$label = (isset($this->labels[$field])) ? $this->labels[$field] : Translate::instance()->$field;
				//$label = $label ?: $field;
				return "<label for='$field' >$label $required</label>";
			}
		}

		/**
		 * Element Tooltip
		 * @param string $field Field Name
		 * @return string Tooltip
		 */
		private function tooltip($field) {

			$disabled = $this->isDisabled($field);

			if (!$disabled && $this->tooltips[$field]) {

				$tootltips = array_unique($this->tooltips[$field]);

				$content = null;

				foreach ($tootltips as $tooltip) {
					
					$keyword = $tooltip=='true' ? 'tooltip_'.$field : "tooltip_$tooltip";
					$text = Translate::instance()->$keyword;
					
					if ($text) {
						$content .= "<span class=qrow>".Translate::instance()->$keyword."</span>";
					}
				}

				return $content ? "<span class='-tooltip' title='$content'>".ui::icon('help')."</span>" : null;
			}
		}
		
		/**
		 * Group Form Fields for Fieldset
		 */
		private function groupFields() {
		
			$exclude = array();
		
			$fieldset = $this->fieldset;
			$fields = array_keys($this->fields);
		
			if ($fieldset) {
		
				foreach ($fields as $field) {
		
					if (!in_array($field, $exclude)) {
		
						$hasSet = _array::value_exists($field, $fieldset);
		
						if ($hasSet) {
							foreach ($fieldset as $setName => $setFields) {
								if(in_array($field, $setFields)) {
									array_push($exclude, $field);
									$return[$setName][] = $field;
								}
							}
						}
						else {
							array_push($exclude, $field);
							$return[] = $field;
						}
					}
				}
		
				return $return;
			}
			else return $fields;
		}
	}
