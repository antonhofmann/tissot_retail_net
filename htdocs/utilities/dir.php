<?php

	class dir {
				
		public static function make($dir,$mode=0777) {
			
			$dir = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], null, $dir);
			
			if (!file_exists($dir)) { 
				@mkdir($dir,$mode,true);
				@chmod($dir,$mode);
			} else {
				@chmod($dir,$mode);
				return true;
			}
		}
		
		public static function remove($dir, $empty=true) {
			
			$dir = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], '', $dir);
			
			if (is_dir($dir)) {
				
				@chmod($dir, 0777);
				
				$objects = scandir($dir);
				
				// count founded files
				// decrement for dot and doubledot
				$totalFiles = count($objects)-2;
				
				// delete all files in directory
				// and remove this directory
				if ($empty) {
				
					// delete all files (recursive)
					foreach ($objects as $object) {
						
						if ($object != "." && $object != "..") { 
							
							if (filetype($dir."/".$object) == "dir") {
								
								// go to subdir
								self::remove("$dir/$object", $empty);
								
							} else {
								
								// remove file
								$file = "$dir/$object";
 								@chmod($file, 0660);
								@unlink($file);
							}
						}
					}
					
					// remove directory
					reset($objects);
					@rmdir($dir);
				}
				// if directory is empty, remove it
				elseif ($totalFiles < 1) {
					rmdir($dir);
				}
			}
		}

		static public function emptyMe($dir) {
			$dir = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], '', $dir);
			@array_map('unlink', glob($dir));
		}
	}
