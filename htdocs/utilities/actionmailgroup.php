<?php

//require_once PATH_LIBRARIES.'phpmailer/class.phpmailer.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/phpmailer/phpmailer/class.phpmailer.php';

/**
 * Action Mail Recipinet
 * 
 * @author Admir Serifi
 * @copyright Mediaparx AG
 * @version 1.0
 */
class ActionMailGroup {

	const CHARSET = "UTF-8";

	public $id;

	public $name;

	protected $salutation;

	protected $subject;

	protected $body;

	protected $footer;

	protected $signature;

	protected $content;

	protected $testmail;

	private $sendmail;

	private $debugger;

	protected $processed;

	protected $recipients = array();

	protected $ccRecipients = array();

	protected $bccRecipients = array();

	protected $dataloader = array();

	protected $attachments = array();

	protected $sender = array();

	protected $console = array();

	public function __construct($id) {
		$this->setID($id);
	}

	public function getID() {
		$this->id;
	}

	public function setID($id) {
		$this->id = $id;
	}

	public function getName() {
		$this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function isSendMail() {
		return $this->sendmail;
	}

	public function getConsole() {
		return $this->console ?: array();
	}

	public function console($message=null) {
		
		if ($message) {
			$this->console[] = date('H:i:s').' '.$message;
		} 

		return $this->console;
	}

	public function getTestMail() {
		return $this->testmail;	
	}

	public function setTestMail($email) {
		$this->testmail = $email;
		return $this;
	}

	public function setDebugger($param) {
		$this->debugger = $param;
		return $this;
	}

	public function getSender($key=null) {
		return $key ? $this->sender[$key] : $this->sender;
	}

	public function setSender($sender) {

		if (is_array($sender)) {
			$this->sender = array_merge($this->sender, $sender);
			return;
		}

		if ($sender instanceof User) $user = $sender;
		else if (preg_match('/^\d+$/', $sender)) $user = new User($sender);
		elseif (!preg_match('/^\d+$/', $sender)) {
			$user = new User();
			$user->getActiveUserFromEmail($sender);
		}
			
		if (!$user->id) {
			$this->console("Sender not found");
			return;
		}
			
		foreach ($user->data as $key => $value) {
			
			// replace sender prefix
			$field = substr($key, 5);
			$this->sender[$field] = $value;
			
			// dataloader
			$key = str_replace('user_', 'sender_', $key);
			$this->setData($key, $value);
		}

		$this->console("Set sender ".$this->sender['id']);

		return $this;
	}

	public function isRecipient($id) {
		return $this->recipients[$id] ? true : false;
	}

	public function addRecipient(ActionMailRecipient $recipient) {
		
		if (!$recipient instanceof ActionMailRecipient) return;

		$id = $recipient->id;
		$this->recipients[$id] = $recipient;
		
		return $this;
	}

	public function getRecipient($id) {
		return $this->recipients[$id];
	}

	public function getRecipients() {
		return $this->recipients;
	}

	public function isCCrecipient($email) {
		return is_array($this->ccRecipients) && in_array($email, $this->ccRecipients) ? true : false;
	}

	public function getCCrecipients() {

		if ($this->recipients && $this->ccRecipients) {
			
			foreach ($this->recipients as $i => $recipient) {
				
				$email = $recipient->getEmail();
				
				if (in_array($email, $this->ccRecipients)) {
					$key = array_search($email, $this->ccRecipients); 
					unset($this->ccRecipients[$key]);
				}
			}
		}

		return array_filter(array_unique($this->ccRecipients));
	}

	public function addCCrecipient($email) {

		if (preg_match('/^\d+$/', $email)) {
			$user = new User($email);
			$user->read($email);
			$email = $user->email;
		}

		if (!in_array($email, $this->ccRecipients) && check::email($email)) {
			$this->ccRecipients[] = $email;
		}
		
		return $this;
	}

	public function addCCrecipients(array $recipients) {

		if (!is_array($recipients)) return;

		foreach ($recipients as $email) {
			if (!check::email($email)) continue;
			if (in_array($email, $this->ccRecipients)) continue;
			$this->ccRecipients[] = $email;
		}
		
		return $this;
	}

	public function isBCCrecipient($email) {
		return is_array($this->bccRecipients) && in_array($email, $this->bccRecipients) ? true : false;
	}

	public function addBCCrecipient($email) {

		if (!in_array($email) && check::email($email)) 
		{
			$this->bccRecipients[] = $email;
		}
		
		return $this;
	}

	public function addBCCrecipients($recipients) {

		if (!is_array($recipients)) return;

		foreach ($recipients as $email) {
			if (!check::email($email)) continue;
			if (in_array($email, $this->bccRecipients)) continue;
			$this->bccRecipients[] = $email;
		}
		
		return $this;
	}

	public function getBCCrecipients() {
		return array_filter(array_unique($this->bccRecipients));
	}

	public function getData($name) {
		return $this->dataloader[$name];
	}

	public function setData($name, $value) {
		$this->dataloader[$name] = $value;
		return $this;
	}

	public function getDataloader() {
		return $this->dataloader;
	}

	public function setDataloader(array $data) {

		if (!$data || !is_array($data)) return;

		$this->dataloader = array_merge($this->dataloader, $data);

		return $this;
	}

	public function getSalutation($render=false) {
		if (!$this->salutation) return;
		return $render ? Content::dataRender($this->salutation, $this->dataloader) : $this->salutation;
	}

	public function setSalutation($content) {
		$this->salutation = $content;
		return $this;
	}

	public function getSubject($render=false) {
		if (!$this->subject) return;
		return $render ? Content::dataRender($this->subject, $this->dataloader) : $this->subject;
	}

	public function setSubject($content) {
		$this->subject = $content;
		return $this;
	}

	public function getBody($render=false) {
		if (!$this->body) return;
		return $render ? Content::dataRender($this->body, $this->dataloader) : $this->body;
	}

	public function setBody($content) {
		$this->body = $content;
		return $this;
	}

	public function getFooter($render=false) {
		if (!$this->footer) return;
		return $render ? Content::dataRender($this->footer, $this->dataloader) : $this->footer;
	}

	public function setFooter($content) {
		$this->footer = $content;
		return $this;
	}

	public function getSignature($render=null) {
		if (!$this->signature) return;
		return $render ? Content::dataRender($this->signature, $this->dataloader) : $this->signature;
	}

	public function setSignature($content) {
		$this->signature = $content;
		return $this;
	}

	public function getContent($html=false) {
		
		if (!$this->content) return;
		
		$content = Content::dataRender($this->content, $this->dataloader);

		if ($html) {

			/*
			$content = str_replace('<br />', ' <br />', $content);
			$content = preg_replace('/(http[s]{0,1}\:\/\/\S{4,})\s{0,}/ims', '<a href="$1">$1</a>', $content);
			$content = preg_replace('/[\r\n]+/', '<br />', $content);
			$content = str_replace('<br /> ', '<br />', $content);
			$content = str_replace(' <br />', '<br />', $content);
			$content = str_replace('<br /><br />', '<br />', $content);
			$content = str_replace('<br />', '<br />' . "\r\n", $content);
			*/

			$content = str_replace(' <br />', '<br />', $content);
			$content = str_replace('<br /> ', '<br />', $content);
			$content = preg_replace('/[\r\n]+/', '<br /><br />', $content);
			$content = str_replace('<br /><br /><br /> <br /><br /><br />', '<br /><br />', $content);
			$content = str_replace('<br /><br /><br /><br /><br />', '<br /><br />', $content);
			$content = str_replace('<br /><br /><br /><br />', '<br /><br />', $content);
			$content = str_replace('<br /><br /><br />', '<br /><br />', $content);

			$content = str_replace('<br />', '<br>', $content);
			$content = str_replace('<br>', '<br>' . "\r\n", $content);

	  		$content = '
	  			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
					<style type="text/css">body {font-family: arial,helvetica,sans-serif}</style>
					</head>
					<body>' . $content. '</body>
				</html>	
	  		';
		}
		else {
			$content = strip_tags($content);
		}

		return $content;
	}

	public function setContent($content) {
		$this->content = $content;
		return $this;
	}

	public function getAttachments() {
		if ($this->attachments) {
			return array_filter(array_unique($this->attachments));
		}
	}

	public function addAttachment($file) {
		
	 	$file = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], '', $file);

	 	if (!file_exists($file) || in_array($file, $this->attachments)) return;

		$this->attachments[] = $file;

		return $this;
	}

	public function send() {

		if ($this->processed) return;

		$auth = User::instance();
		$testMail = $this->getTestMail();
		$recipients = $this->getRecipients() ?: array();

		if (!$recipients) {
			$this->console("No recipients");
			return;
		}

		if (count($recipients)==1) {

			reset($this->recipients);
			$key = key($this->recipients);
			$recipient = $this->recipients[$key];

			if ($recipient instanceOf ActionMailRecipient) {
				$data = $recipient->getDataloader();
				$this->setDataloader($data);
			}
		} else {
			$salutation = $this->getSalutation();
		}

		// mail content render
		if (!$this->content) {

			$content = $this->getBody();

			// replace first line of content with salutation
			if ($salutation) {
				$content = preg_replace('/^.+\n/', $salutation."\n", $content);
			}

			$content .= "\n\n".$this->getFooter();
			$content .= "\n\n".$this->getTestMailSignature();
			$content .= "\n\n".$this->getSignature();
			
			$dataloader = $this->getDataloader() ?: array();
			$content = Content::dataRender($content, $dataloader);
			
			$this->setContent($content);
		}

		// mail builder
		$mail = new PHPMailer();
		$mail->CharSet = self::CHARSET;

		// mail subject
		$mail->Subject = $this->getSubject(true);

		// plain text body
		$plainTextContent = $this->getContent();
		$mail->AltBody = strip_tags($plainTextContent);

		// html content
		$htmlContent = $this->getContent(true);
		$mail->Body = $htmlContent;
		$mail->IsHTML(true);

		if ($testMail) {
			$mail->AddAddress($testMail);
		} else {
			foreach ($recipients as $i => $recipient) {
  				$recipient->setDebugger(true);
  				$email = $recipient->getSendMailAddress();
  				$name = $recipient->getName();
  				$mail->AddAddress($email, $name);
  			}
		}
  		
  		if (!$this->testmail) {
			
			$ccRecipients = $this->getCCrecipients() ?: array();
			$bccRecipients = $this->getBCCrecipients() ?: array();

			foreach ($ccRecipients as $email) {
				$mail->AddCC($email);
			}

			foreach ($bccRecipients as $email) {
				$mail->AddBCC($email);
			}
		}

		$attachments = $this->getAttachments();

		if ($attachments) {
			foreach ($attachments as $file) {
				$mail->AddAttachment($file);
			}
		}

		// sender
		$sender = $this->getSender();
		$senderEmail = $sender['email'] ?: $auth->email;
		$senderName = $sender['name'] ? $sender['firstname'].' '.$sender['name'] : $auth->firstname.' '.$auth->name;
		$mail->SetFrom($senderEmail, $senderName);

		if (!$this->debugger) { 
			$this->sendmail = $mail->Send();
			$response = $this->sendmail;
			$this->processed = true;
		}
		else $response = true;

		if (!$response) {
			$this->console($mail->ErrorInfo);
		}

		return $response;
	}

	public function getTestMailSignature() {

		if (!$this->testmail || !$this->recipients) return;

		$recipients = $this->getRecipients();

		$emails = array();

		foreach ($recipients as $i => $recipient) {
			$emails[] = $recipient->getEmail();
		}

		$emails = join(', ', array_unique($emails));
		
		$ccRecipients = $this->getCCrecipients() ?: array();
		$bccRecipients = $this->getBCCrecipients() ?: array();
		$additionalRecipients = $ccRecipients+$bccRecipients;

		$content  = ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n\n";
		$content .= "IMPORTANT: Sendmail is in development mode.\n\n";
		$content .= "This email was dedicated for recipient(s): $emails \n\n";
		
		// user cc recipient
		if ($additionalRecipients) {
			$content .= "Additional recipients: ";
			$content .= join(', ', $additionalRecipients)."\n\n";
		}

		$content .= "Test mail: $this->testmail.\n\n";
		$content .= ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n\n";
		
		return $content;
	}

	public function reset() {
		$this->processed = false;
		$this->recipients = array();
	}
}