<?php 

	class Send_Mail {
		
		public function __construct($application=null) {
			$this->application = $application;
		}
		
		public function template($template) {
			$this->template = $template;
		}
		
		public function subject($id, $subject) {
			$this->subjects[$id] = $subject;
		}
		
		public function content($id, $content) {
			$this->contents[$id] = $content;
		}
		
		public function recipient($id, $name) {
			$this->recipients[$id]['name'] = $name;
		}
		
		public function email($id, $email) {
			if (check::email($email)) {
				$this->recipients[$id]['email'] = $email;
			}
		}

		public function cc($id, $email) {
			if (check::email($email)) {
				$this->recipients[$id]['cc'][] = $email;
			}
		}
		
		public function bcc($id, $email) {
			if (check::email($email)) {
				$this->recipients[$id]['bcc'] = $email;
			}
		}
		
		public function additionalRecipient($email) {
			if (check::email($email)) {
				$this->additional[] = $email;
			}
		}
				
		public function signature($signature) {
			$this->signature = $signature;
		}
		
		public function send() {
			
			if ($this->recipients) {
				
				$settings = Settings::init();
				$user = User::instance();
				
				$connector = ($this->application) ? $this->application : Connector::DB_CORE;
				$mail_tracking = new Mail_Tracking($connector);
				
				require_once PATH_LIBRARIES.'phpmailer/class.phpmailer.php';
				
				foreach ($this->recipients as $id => $recipient) {
					
					$mail = new PHPMailer();
					
					$cc_recipients = array();
					
					// mail subject
					$mail->Subject = $this->subjects[$id];
					
					// mail content
					$content = $this->contents[$id];
					
					// mail signature
					if ($this->signature) {
						$content .= "\n\n".$this->signature;	
					}
					
					// mail body
					$mail->Body = $content;
					
					// sender
					$mail->SetFrom($user->email, $user->firstname." ".$user->name);
						
					// replay to
					$mail->AddReplyTo($user->email, $user->firstname." ".$user->name);
					
					// recipient
					$mail->AddAddress($recipient['email'], $recipient['name']);
					
					// cc recipient
					if ($recipient['email'] && $recipient['cc']) {
						foreach ($recipient['cc'] as $cc) {
							$mail->AddCC($cc);
							$cc_recipients[] = $cc;
						}
					}
					
					// bcc recipient
					if ($recipient['email'] && $recipient['bcc']) {
						$mail->AddCC($recipient['bcc']);
					}
					
					// add additional recipients
					if ($this->additional) {
						foreach ($this->additional as $additional) {
							$mail->AddBCC($additional);
						}
					}
					
					// sendmail
					if ($settings->devmod && $settings->enable_devmode_sendmail) {
						$sendmail = $mail->Send();
					} else {
						$sendmail = $mail->Send();
					}
					
					if ($sendmail) {
						
						$mail_tracking->create(array(
							'mail_tracking_mail_template_id' => $this->template,
							'mail_tracking_subject' => $this->subjects[$id],
							'mail_tracking_content' => $this->contents[$id],
							'mail_tracking_recipient_name' => $recipient['name'],
							'mail_tracking_recipient_email' => $recipient['email'],
							'mail_tracking_recipient_cc_email' => join(',', $cc_recipients),
							'mail_tracking_recipient_deputy_email' => $recipient['bcc'],
							'mail_tracking_additional_recipient_emails' => ($this->additional) ? serialize($this->additional) : null,
							'date_created' => date('Y-m-d H:i:s'),
							'user_created' => $user->login
						));
					}
				}
				
				// unset all recipients
				unset($this->recipients);
				return $sendmail;
			}
		}
	}
?>