<?php 

	class menu {
		
		/**
		 * Object instance
		 * @var string class name
		 */
		private static $instance;
		
		/**
		 * Database Model
		 * @var unknown
		 */
		protected $model;
		
		/**
		 * Navigations Data (ordered)
		 * @var array
		 */
		public $data;

		/**
		 * Request Builder
		 * @var object
		 */
		public $request;
		
		/**
		 * Build Mode
		 * @var unknown
		 */
		public $recursive = true;
		
		/**
		 * Show first menu item
		 * @var boolean
		 */
		public $show_navigation_homepage;
		
		/**
		 * Open category item in new Tab
		 * @var boolean
		 */
		public $blank_navigation_category;



		private $excluded;
		
		/**
		 * Navigation Builder
		 */
		public function __construct($filterData=array()) { 
			
			$settings = settings::init();
			$this->show_navigation_homepage = $settings->show_navigation_homepage;
			$this->blank_navigation_category = $settings->blank_navigation_category;
			
			$this->request = request::instance();
			$navigation = new Navigation();
			
			$roles = join(',', user::roles()); 
			$filters = array("navigation_active = 1","role_navigation_role IN ( $roles )"); 
			$result = $navigation->load($filters, true);

			$this->model = new Model(Connector::DB_CORE);

			// set query filters
			$this->setQueryFilters($filterData);

			
			if ($result) {
				
				foreach ($result as $row) {
					
					$id = $row['navigation_id'];
					$parent = $row['navigation_parent'];
					$url = $row['navigation_url'];

					if (preg_match('#^news#',$url)===1) {
						$url = 'gazette'.substr($url,4);
					}
					
					$this->data[$parent][$id]['url'] = $url;
					$this->data[$parent][$id]['caption'] = $row['navigation_language_name'];
					$this->data[$parent][$id]['tab'] = $row['navigation_tab'];
					$this->data[$parent][$id]['order'] = $row['navigation_order'];
					$this->data[$parent][$id]['active'] = $row['navigation_active'];
				}
			}
		}
		
		/**
		 * Class Instanc
		 * @return string class name
		 */
		public static function instance($filterData=array()) {
			if (!self::$instance) self::$instance = new menu($filterData);
			return self::$instance;
		}

		public function getData($parent=0) {

			$data = array();

			if ($this->data[$parent]) {

				foreach ($this->data[$parent] as $navParent => $navigations) {
					
					if ($this->excluded[$navParent]) {
						continue;
					}

					foreach ($navigations as $id => $row) {
						
						if ($this->excluded[$id]) {
							continue;
						}

						$data[$navParent][$id] = $row;
					}
				}
			}

			return $data;
		}

		/**
		 * Build navigation menu <br />
		 * List all active records by navigation parents
		 * @param integer $parent
		 * @return string html navigation menu
		 */
		public function build($parent, $isTab=0) { 
			
			if ($this->data[$parent]) {	
				
				foreach ($this->data[$parent] as $id => $row) {

					if ($this->excluded[$id]) {
						continue;
					}

					list($url,$caption,$tab,$order,$active) = array_values($row);

					// is navigation item excluded
					$visible = (in_array($url, $this->request->excluded)) ? false : true;
					
					// show home page in navigation
					if ($url=="home") {
						$visible = ($this->show_navigation_homepage) ? true : false;
					}
					
					if ($visible && $tab==$isTab) {
						
						// open each category on new window(or tab) ENABLE_NAVIGATION_HOMEPAGE
						if ($this->blank_navigation_category && $parent==0) {
							$target = ($this->request->application <> $url) ? "target=_blank" : null;
						} else {
							$target = null;
						}
						
						// build class name
						$className = str_replace('/', '-', $url);

						// set cascading active menues
						$classActive = ($this->id==$id || ($this->request->parents && in_array($id, $this->request->parents))) ? "active" : null;

						// navigation item
						$navigation .= "<li class='$classActive $className'>";
						$navigation .= "<a href='/$url' class='$classActive' $target ><b>$caption</b></a>";
							
						// show level submenues
						if ($classActive && $this->data[$id]) {
							$navigation .= $this->build($id,$isTab);
						}

						$navigation .= "</li>";
					}
				}
			}

			return ($navigation) ? "<ul>$navigation</ul>" : null;
		}
		
		public function tree($parent=0,$isTab=0) {
		
			if ($this->data[$parent]) {

				if ($this->excluded[$parent]) {
					return false;
				}
		
				foreach ($this->data[$parent] as $id => $row) {

					if ($this->excluded[$id]) {
						continue;
					}
		
					list($url,$caption,$tab,$order,$active) = array_values($row);
						
					if ($tab==$isTab) {
						
						$submenu = ($this->data[$id]) ? $this->tree($id,$isTab) : null;
						$ceret = ($submenu) ? '<span class="arrow arrow-right" ></span>' : null;
						
						$navigation .= "
							<li url='$url' parent=$parent id=$id >
								$ceret
								<span class=label>$caption</span>
								$submenu
							</li>
						";
					}
				}
			}
		
			return ($navigation) ? "<ul>$navigation</ul>" : null;
		}


		public function setQueryFilters($data=array()) {

			$user = User::instance();
			$roles = join(',', user::roles()); 
			
			$_NAVIGATIONS = array();
			$_FILTERS = array();
			$_STATES = array();

			// get filters
			$sth = $this->model->db->prepare("
				SELECT 
					navigation_id AS navigation,
					navigation_filter_id AS filter,
					navigation_filter_content AS query
				FROM db_retailnet.navigation_filters
				INNER JOIN db_retailnet.navigation_filter_navigations ON navigation_filter_navigation_filter_id = navigation_filter_id
				INNER JOIN db_retailnet.navigations ON navigation_filter_navigation_navigation_id = navigation_id
				INNER JOIN db_retailnet.role_navigations ON role_navigation_navigation = navigation_id
				WHERE navigation_active = 1 AND role_navigation_role IN ( $roles )
			");

			$sth->execute();
			$result = $sth->fetchAll();			

			if ($result) {
				foreach ($result as $row) {
					$nav = $row['navigation'];
					$filter = $row['filter'];
					$_NAVIGATIONS[$nav][] = $filter;
					$_FILTERS[$filter] = $row['query'];
				}
			}

			// execute filters
			if ($_FILTERS && $data) {
				
				$data = array_merge($user->data, $data);

				foreach ($_FILTERS as $filter => $query) {
					
					$sql = Model::prepare($query, $data);

					$_STATES[$filter] = true;
					
					if ($sql) {
						
						$sth = $this->model->db->prepare($sql);
						$sth->execute();
						$result = $sth->fetch();
						
						$_STATES[$filter] = $result['value'] ? true : false;
					}
				}
			}

			if ($_NAVIGATIONS) {

				foreach ($_NAVIGATIONS as $navigation => $filters) {
					foreach ($filters as $filter) {
						if (isset($_STATES[$filter]) && !$_STATES[$filter]) {
							$this->excluded[$navigation] = true;
						}
					}
				}
			}


		}

		public function activate($url) {

			$sth = $this->model->db->prepare("
				SELECT navigation_id
				FROM navigations
				WHERE navigation_url = ?
			");

			$sth->execute(array($url));
			$result = $sth->fetch();

			if ($result['navigation_id']) {
				$this->request->parents[] = $result['navigation_id'];
			}
		}

		public function deactivate($url) {

			$sth = $this->model->db->prepare("
				SELECT navigation_id
				FROM navigations
				WHERE navigation_url = ?
			");

			$sth->execute(array($url));
			$result = $sth->fetch();

			if ($result['navigation_id'] && $this->request->parents && in_array($result['navigation_id'], $this->request->parents) ) {
				$key = array_search($result['navigation_id'], $this->request->parents);
				unset($this->request->parents[$key]);
			}
		}
	}