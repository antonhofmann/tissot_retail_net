<?php 


class Exchange {
	
	
	/**
	 * Import SAP POS data exchange
	 * 
	 * @return stdClass
	 */
	public static function sapPos() {
		
		
		$file = $_SERVER['DOCUMENT_ROOT']."/cronjobs/sap.pos.php";

		if (file_exists($file)) {
			
			ob_start();
			$user = User::instance()->id;
			require $file;
			$response = ob_get_contents();
			ob_end_clean();
			
			return $response ? json_decode($response) : new stdClass();	
		}

	}
}