<?php

	class Session {
		
		public static function init() {
			@session_start();
		}
		
		public static function set($key, $value) {
			$_SESSION[$key] = $value;
		}
			
		public static function get($key){
			return ($_SESSION[$key]) ? $_SESSION[$key]: '';
		}
		
		public static function remove($key){
			unset($_SESSION[$key]);
		}
		
		public static function destroy(){
			unset($_SESSION);
			session_destroy();
		}
		
		public static function filter($application, $session, $reset=false) {
				
			global $_REQUEST;
			
			$filters = unserialize(self::get('filters'));
			$filter = ($filters[$application][$session]) ? $filters[$application][$session] : array();
			$filter = ($_REQUEST) ? array_merge($filter, $_REQUEST) : $filter;
			
			if ($reset) {
				
				if ($filters[$application]) {
					unset($filters[$application]);
				}
				
				if (is_array($reset)) {
					foreach ($reset as $field) {
						if (!$_REQUEST[$field] && isset($filter[$field])) {
							unset($filter[$field]);
						}
					}
				}
			}
			
			$filters[$application][$session] = $filter;			
			Session::set('filters', serialize($filters));
				
			return $filter;
		}

		public function setFilter($application, $session, $name, $value=null) {
			$filters = unserialize(self::get('filters'));
			$filter = $filters[$application][$session] ?: array();
			$filter[$name] = $value;
			$filters[$application][$session] = $filter;			
			Session::set('filters', serialize($filters));
		}

		public function getFilter($application, $session) {
			$filters = unserialize(self::get('filters'));
			return $filters[$application][$session] ?: array();
		}

		public function resetFilter($application, $session) {
			$filters = unserialize(self::get('filters'));
			$filters[$application][$session] = array();			
			Session::set('filters', serialize($filters));
		}
		
		public static function request($key, $data=null) {
			if ($data) {
				self::set($key, serialize($data));
			}
			else {
				$data = self::get($key);
				self::remove($key);
				return unserialize($data);
			}
		}
		
		public static function referer($level=null) {
			
			$referers = self::get('referer_vars');
			$referers = ($referers) ? @unserialize($referers) : array();
			
			if (isset($level)) {
				$key = ($level=='back') ? 1 : $level;
				return $referers[$key];
			}
			else { 
				
				$url = url::current();
				
				if ($referers[0]) {
					if ($url <> $referers[0]) array_unshift($referers, $url);
				} else {
					array_unshift($referers, $url); 
				}
				
				if (count($referers) > 5) {
					$referers = array_slice($referers,0,5);
				}
				
				self::set('referer_vars', serialize($referers));
			}
		}
	}