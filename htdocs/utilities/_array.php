<?php


	class _array {

		static public function replaceKey($search, $replace, $data) {

			if (is_array($data)) {

				$return = array();

				array_walk($data, function($v,$k) use (&$return, $search, $replace) { 

					if (preg_match("#^$search#", $k)===1) {  
						$key = str_replace($search, $replace, $k);
						$return[$key] = $v;
					} else {
						$return[$k] = $v;
					}

				});
			}

			return $return;
		}
		static public function renameKeys($source, $map) {

			if (is_array($source)) {

				$return = array();

				array_walk($source, function($v,$k) use (&$return, $map) { 
					$key = $map[$k] ?: $k;
					$return[$key] = $v;
				});
			}

			return $return;
		}

		public static function rend_attributes($attributes) {
			@array_walk($attributes, create_function('&$i,$k','$i=" $k=\"$i\" ";'));
			$attributes = @array_values($attributes);
			return @join(' ', $attributes);
		}

		public static function key_exists($needle,$haystack) {

			if (is_array($haystack)) {
				
				foreach ( $haystack as $key => $value ) {
	
					if ( $needle == $key )  return true;
	
					if ( is_array( $value ) ) {
						if ( self::key_exists( $needle, $value ) == true )
							return true;
						else
							continue;
					}
	
				};
			}

			return false;
		}

		public static function get_key_from_value($needle,$haystack) {

			foreach ( $haystack as $key => $value ) {

				if ( $needle == $key )  $found = $key;
				elseif ( is_array( $value )  && !is_object($value)) {
					self::get_key_from_value( $needle, $value );
				}

			};

			return $found;
		}

		public static function value_exists($needle,$haystack) {
			$found = false;
			foreach ($haystack as $item) {
				if ($item === $needle) {
					$found = true;
					break;
				} elseif (is_array($item)) {
					$found = self::value_exists($needle, $item);
					if($found) {
						break;
					}
				}
			}
			return $found;
		}

		public static function unset_from_value($value, $array) {

			if (is_array($array)) {
				$key = array_search($value, $array);
				if (false !== $key) unset($array[$key]);
			}

			return $array;
		}

		public static function extract($array, $keyname=null) {
			
			if (is_array($array)) {
				
				foreach ($array as $row) {
					
					if(is_array($row)) {
						
						// extract array by keyname
						if ($keyname) {
							
							if ($row[$keyname])  {
								$return[] = $row[$keyname];
							}
						} 
						// extract first to values from array
						// its suitable for dropdown dataloader
						else {
							list($key, $value) = array_values($row);
							$return[$key] = $value;
						}
					}
				}
				
				return $return;
			}
		}

		/**
		 *
		 * Returns all values where key = $key
		 * @param $array
		 * @param $key
		 * @return array
		 */
		public static function extract_by_key($array, $key) {
			
			if (is_array($array)) {
				
				foreach ($array as $row) {
					
					$index = array_shift(array_values($row));
					
					foreach($row as $current_key => $value) {
						if ($current_key == $key) {
							$return[$index] = $value;
							break;
						}
					}
				}
				return $return;
			}
		}
		
		public static function extract_by_key_value($array, $key, $value) {
			
			$return = array();
			
			if (is_array($array)) {
				
				foreach ($array as $i => $row) {
					
					if ($row[$key] && $row[$value]) {
						$return[$row[$key]] = $row[$value];
					}
				}
			}
			
			return $return;
		}

		public static function get_first_value($array) {
			if (is_array($array)) {
				return array_shift(array_values($array));
			}
			return null;
		}

		public static function datagrid($array, $key=null) {
			if (is_array($array)) {
				foreach ($array as $row) {
					$id = ($key) ? $row[$key] : array_shift($row);
					$datagrid[$id] = $row;
				}
				return $datagrid;
			}
		}
		
		public static function sortByArray($array, $orderArray) {

			if (!$orderArray || !is_array($array) || !is_array($orderArray)) {
				return $array;
			}

			$ordered = array();
			$keys = array_keys($orderArray);

			// associativ
			if (array_keys($keys) !== $keys) {

				foreach($orderArray as $key => $value) {
					if(array_key_exists($value,$array)) {
						$ordered[$value] = $array[$value];
						unset($array[$value]);
					}
			    }

			    return $ordered + $array;

			} else {
		    
		    	foreach($orderArray as $key => $value) {
					if(in_array($value,$array)) {
						$ordered[] = $value;
						
						$key = array_search($value, $array);
						unset($array[$key]);
					}
			    }

			    return array_merge($ordered, $array);
		    }
		}

		public static function sum(array $input, $key) {
		   
			$sum = 0;
		   
		   array_walk($input, function($item, $index, $params) {
		         if (!empty($item[$params[1]]))
		            $params[0] += $item[$params[1]];
		      }, array(&$sum, $key)
		   );
		   
		   return $sum;
		}

		function partition( $list, $p ) {
			$listlen = count( $list );
			$partlen = floor( $listlen / $p );
			$partrem = $listlen % $p;
			$partition = array();
			$mark = 0;
			for ($px = 0; $px < $p; $px++) {
			    $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
			    $partition[$px] = array_slice( $list, $mark, $incr );
			    $mark += $incr;
			}
			return $partition;
		}
	}