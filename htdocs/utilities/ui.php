<?php 

	class ui {
		
		/**
		 * UI Button
		 * @param array $properties
		 * @return string
		 */
		public static function button($properties) {
			
			$caption = ($properties['caption']) ? $properties['caption'] : $properties['label'];
			$icon = ($properties['icon']) ? '<span class="icon '.$properties['icon'].'"></span>' : null;
			$properties['class'] = ($properties['class']) ? "button ".$properties['class'] : 'button';
			
			unset($properties['caption']);
			unset($properties['label']);
			unset($properties['icon']);
			$attributes = _array::rend_attributes($properties);
			
			return ($properties['href']) 
				? "<a $attributes >$icon<span class=label >$caption</span></a>" 
				: "<span $attributes >$icon<span class=label >$caption</span></span>";
		}
		
		/**
		 * UI Form Field
		 * @param string $type  all form native types
		 * @param array $properties  field attributes
		 * @return string html markup
		 */
		public static function field($type, $properties) {
			
			switch ($type) {
				
				case 'input':
					
					// captions
					$precaption = $properties['precaption'];
					$subcaption = $properties['subcaption'];
					unset($properties['precaption']);
					unset($properties['subcaption']);
					
					// field
					$attributes = _array::rend_attributes($properties);
					
					return "$precaption<input $attributes />$subcaption";
					
				break;
			}
		}
		
		/**
		 * UI Icon
		 * @param string $name
		 * @return string
		 */
		public static function icon($name) {
			return "<span class='gui-icon $name'></span>";
		}
		
		/**
		 * UI File Exension Icon
		 * @param string $extension, 32x32
		 * @param boolean $small, 16x16
		 * @return string
		 */
		public static function extension($extension, $small=false) {
			$small = ($small) ? 'small' : null;
			return "<span class='file-extension $extension $small'></span>";
		}
		
		/**
		 * UI Search Box
		 * @param string $name, field name
		 * @return string
		 */
		public static function searchbox($name='search') {
			
			$translate = translate::instance();
			$search = ($_REQUEST['search']) ? trim($_REQUEST['search']) : $translate->$name;
			$remover = ($_REQUEST[$name] && $_REQUEST[$name]<>$translate->$name) ? '<span class="icon close"></span>' : null;
			$active = ($remover) ? 'active' : null;
	
			return "
				<span class='button searchbox'>
					<span class='icon search'></span>
					<input type=text name=search id=$name class='submit $active' value=\"$search\" />
					$remover
				</span>
			";
		}
		
		public static function dropdown($dataloader,$properties) {
			
			if (!is_array($dataloader)) return;
				
			if ($properties['caption']===false) {
				$caption = null;
			} else {
				$caption = ($properties['caption']) ? $properties['caption'] : "Select";
			}

			unset($properties['caption']);
			
			$attributes = _array::rend_attributes($properties);
			
			$return = "<select $attributes >";

			if ($caption) {
				$return .= "<option value=\"\" >$caption</option>";
			}
			
			foreach ($dataloader as $key => $row) {

				if (is_array($row) && $row['group'] && is_array($row['options']) ) {

					$return .= "<optgroup label=\"{$row[group]}\">";

					foreach ($row['options'] as $optValue => $optName) {
						
						if (is_array($optName)) {
							list($id,$name,$class) = array_values($optName);
						} else {
							$id = $optValue;
							$name = $optName;
						}

						$class = $class ? "class=$class" : null;
						$selected = ($properties['value']==$id) ? "selected=selected" : null;
						$return .= "<option value=\"$id\" $selected $class >$name</option>";
					}

					$return .= "</optgroup>";

				} else {
				
					if (is_array($row)) {
						list($id,$name,$class) = array_values($row);
					} else {
						$id = $key;
						$name = $row;
					}
					
					$class = $class ? "class=$class" : null;
					$selected = ($properties['value']==$id) ? "selected=selected" : null;
					$return .= "<option value=\"$id\" $selected $class >$name</option>";
				}
			}
			
			$return .= "</select>";
			
			return $return;

		}
		
		public static function dialogbox($properties) {
			
			$id = $properties['id'];
			$theme = ($properties['theme']) ? $properties['theme'] : 'theme-default';
			$title = ($properties['title']) ? $properties['title'] : 'Confirm';
			$close = ($properties['close']) ? "<span class='icon ".$properties['close']."'>".$properties['close']."</span>" : null;
			$content = $properties['content'];
			
			if ($properties['buttons']) {
				
				$actions = "<div class='dialogbox-actions'>";
				
				foreach ($properties['buttons'] as $name => $button) {
					$actions .= "
						<a id=$name class='button ".$button['class']."' href='".$button['href']."' >
							<span class='icon ".$button['icon']."'></span>
							<span class=label >".$button['label']."</span>
						</a>
					";
				}
								
				$actions .= "</div>";
			}
			
			return "
				<div class='dialogbox-container {$properties['class']}'>
					<div id='$id' class='dialogbox $theme' >
						<div class=dialogbox-title >
							<span class=label >$title</span>
							$close
						</div>
						<div class='dialogbox-content'>
							$content
						</div>
						$actions
					</div>
				</div>
			";
		}
		
		public static function adomodal($properties) {
			
			$id = $properties['id'];
			$theme = ($properties['theme']) ? $properties['theme'] : 'theme-default';
			$title = ($properties['title']) ? $properties['title'] : 'Confirm';
			$close = ($properties['close']) ? "<span class='icon adomodal-close'>".$properties['close']."</span>" : null;
			$content = $properties['content'];
			
			if ($properties['buttons']) {
				
				$actions = "<div class='adomodal-actions'>";
				
				foreach ($properties['buttons'] as $name => $button) {
					$actions .= "
						<a id=$name class='button ".$button['class']."' href='".$button['href']."' >
							<span class='icon ".$button['icon']."'></span>
							<span class=label >".$button['label']."</span>
						</a>
					";
				}
								
				$actions .= "</div>";
			}
			
			return "
				<div class='adomodal-container'>
					<div id='$id' class='adomodal $theme' >
						<div class=adomodal-title >$title $close</div>
						<div class='adomodal-content'>
							$content
						</div>
						$actions
					</div>
				</div>
			";
		}
		
		/**
		 * Modal Box
		 * @param string $name, placehoolder ID
		 * @param string $title, header title
		 * @param string $content modal content
		 * @param array $buttons actions
		 * @return string
		 */
		public static function modalbox($name, $title, $content, $buttons) {
			
			
			$modalbox = "<div class='modalbox-container'><div id='$name' class='modalbox'>";
			
			// header
			if ($title) {
				$modalbox .= "<div class=modalbox-header ><div class='title'>$title</div></div>";
			}
			
			// content
			$modalbox .= "<div class=modalbox-content-container ><div class=modalbox-content >$content</div></div>";
		
			// footer
			if ($buttons) {
			
				$modalbox .= "<div class='modalbox-footer'><div class=modalbox-actions >";
			
				foreach ($buttons as $key => $properties) {
					
					$label = $properties['label'];
					$icon = $properties['icon'];
					$href = $properties['href'];
					$class = $properties['class'];
					
					if ($href) {
						$modalbox .= "<a id='$name-$key' href='$href' class='button $key $class' ><span class='icon $icon'></span><span class=label >$label</span></a>";
					} else {
						$modalbox .= "<span id='$name-$key' href='$href' class='button $key $class' ><span class='icon $icon'></span><span class=label >$label</span></span>";
					}
				}
			
				$modalbox .= "</div></div>";
			}
			
			$modalbox .= "</div></div>";
			
			return $modalbox;
		}
		
		/**
		 * Select Export options
		 * @param string $action, form action
		 * @param string $table, database table name
		 * @param string $db, database name
		 * @return string
		 */
		public static function exportbox($action, $name, $db=null, $extend='') {
			
			$db = ($db) ? $db : Connector::DB_CORE;
			$translate = translate::instance();
			$title = $translate->select_output_fields;
			$select_all = $translate->select_all;
			$deselect_all = $translate->deselect_all;
			
			// read table
			$table = new DB_Table();
			$table->read_from_name($name, $db);

			// get export data
			$fields = array();
			
			if ($table->export_fields) {
				$data = unserialize($table->export_fields);
				foreach ($data as $field) {
					$fields[$field] = 1;
				}
			}
			
			// show modal box
			$modal = ($fields) ? $table->export_fields_selectable : null;
			
			// read user export list
			$export = user::instance()->exportList()->read_from_table($table->id);
			
			// build form fields
			$selected_fields = array();
			if ($export['user_export_list_fields']) {
				
				$data = unserialize($export['user_export_list_fields']);
				if ($data) {
					foreach ($data as $field) {
						$selected_fields[$field] = 1;
					}
				}

			} else $selected_fields = $fields;
			
			// order array
			if ($export['user_export_list_sort']) $orderArray = unserialize($export['user_export_list_sort']);
			else $orderArray = ($table->export_fields_order) ? unserialize($table->export_fields_order) : array();
			
			// form content
			$content = "
				<form method=post action='$action' >
				<input type=hidden name=modal id='$name-modal' value='$modal' />
				<input type=hidden name=modal-sort id='$name-sort' value='".join(',',$orderArray)."' />
			";
			
			if ($fields) { 
				
				// order selected fields
				$fields = _array::sortByArray($fields, $orderArray); 
				
				// select all
				$check_all = (count($selected_fields)==count($fields)) ? 'checked=checked' : null;
				$selected_caption = ($check_all) ? $deselect_all : $select_all;
				$data_caption = ($check_all) ? $select_all : $deselect_all;
				$content .= "<p class=select_all ><input type=checkbox class='$field' name=selected_all value=1 $check_all /><span data='$data_caption' >$selected_caption</span></p>";
				
				foreach ($fields as $field => $value) {
					
					if ($modal && $selected_fields) {
						$checked = ($selected_fields[$field]) ? 'checked=checked' : null;
					} else {
						$checked = 'checked=checked';
					}
				
					$content .= "<p><input type=checkbox class='export_field' data='$field' name=columns[$field] value=1 $checked /><span>".$translate->$field."</span></p>";
				}
			}
			
			$content .= $extend;
			$content .= "</form>";
				
			// buttons
			$buttons = array(
				'cancel' => array(
					'label' => $translate->cancel,
					'icon' => 'cancel'
				),
				'apply' => array(
					'label' => $translate->apply,
					'icon' => 'apply'
				)
			);
			
			return self::modalbox("exportbox-$name", $title, $content, $buttons);
		}

		public function printBox(array $fields, array $properties, $modal=true) {

			if (!$fields) return;

			$translate = translate::instance();

			$content = "<div class='print-box'>";
			$content .= "<form method='post' action='{$properties[action]}' id='{$properties[id]}' >";
			$content .= "<p class='select-all'><input type='checkbox' > Select All</p>";
			$content .= "<div class='dd'>";
			$content .= "<ol class='dd-list'>";

			foreach ($fields as $name => $data) {
				
				$content .= "<li class='dd-item' data-name='$name' >";

				$input = $data['attributes'] ? "<input ". _array::rend_attributes($data['attributes'])." /> " : null;
				$content .= "<div class='dd3-content'>$input{$data[caption]}</div>";

				if ($data['sort']) {
					$content .= "<div class='dd-handle dd3-handle'></div>";
				}

				$content .= "</li>";
			}

			$content .= "</ol>";
			$content .= "</div>";
			$content .= "</form>";
			$content .= "</div>";

			// buttons
			$buttons = array(
				'cancel' => array(
					'label' => $translate->cancel,
					'icon' => 'cancel'
				),
				'apply' => array(
					'label' => $translate->print,
					'icon' => 'apply'
				)
			);

			$title = $properties['title'] ?: $translate->select_output_fields;
			$name = 'modal-'.$properties['id'];
			
			return self::modalbox($name, $title, $content, $buttons);
		}
		
		public static function filterLabel($caption) {
			return "<span class=filter-label >$caption</span>";
		}

		public static function pagerRows($current) {
		
			$rows = array(25,50,100,200,'all');

			$dropdown = "<select class='dropdown-rows' >";

			foreach ($rows as $k => $row) {
				$selected = $row==$current ? "selected=selected" : null;
				$dropdown .= "<option value='$row' $selected >$row</option>";
			}

			$dropdown .= "</select>";

			return $dropdown;
		}
	}