<?php
	
	class CssCompiler {
	
		/**
		 * Object instance
		 * @return CssCompiler
		 */
		private static $instance;
	
		/**
		 * Compiled dir
		 * @var string
		 */
		protected $dir;
	
		/**
		 * File chmod
		 * @var int
		 */
		protected $chmod;
		
		/**
		 * Errors
		 * @var boolean
		 */
		protected $error;
		
		/**
		 * Last changed attached files
		 * @var int
		 */
		protected $stamp = 0;
		
		/**
		 * File sources
		 * @var array
		 */
		protected $sources;
		
		/**
		 * Media Type
		 * @var string
		 */
		protected $media;

		/**
		 * General constructure
		 * @return void
		 */
		protected function __construct() {
			$this->dir =  DIR_COMPILED;
			$this->media = 'screen';
			$this->chmod = 0666;
			$this->sources = array();
		}
	
		/**
		 * Singelton
		 * @return CssCompiler
		 */
		protected static function instance() {
			
			if( !is_object( self::$instance ) ) { 
				self::$instance = new CssCompiler();
			}
			
			return self::$instance;
		}
		
		/**
		 * Attach file to compilation process
		 * @param array $files list of css files (width directory information)
		 * @return void
		 */
		public static function attach($files) { 
			if (is_array($files)) {
				foreach ($files as $file) self::instance()->add($file);
			} else {
				self::instance()->add($files);
			}
		}

		/**
		 * Compile Files
		 * @param boolean $minify minify export file
		 * @return string html tag
		 */
		public static function compile($minify=true) {
			return self::instance()->compilation($minify);
		}
		
		/**
		 * Add file to compiler
		 * @param string $file file whith directory information
		 * @return void
		 */
		protected function add($file) {
			if(!in_array($file, $this->sources) && file_exists($_SERVER['DOCUMENT_ROOT'].$file) && file::extension($file)=='css') { 
				$this->sources[] = $file;
				$stamp = filemtime($_SERVER['DOCUMENT_ROOT'].$file);
				$this->stamp = ( $stamp > $this->stamp ) ? $stamp : $this->stamp;
			} else {
				$this->error = true; 
			}
		}
		
		/**
		 * Compilation process
		 * @param boolean $minify
		 * @return string html tag
		 */
		protected function compilation($minify) {
			
			if ($this->sources && !$this->error) {
				
				$data = '';
				$labels = array();
				
				// build filename
				foreach($this->sources as $file) {
					array_push($labels, file::name($file));
				}
				
				$filename = join('.',$labels).'.'.$this->stamp.'.css';
				$length = strlen($filename);
				$filename = (255-$length > 0) ? $filename : substr($filename, -255);
				$compiled_file = $_SERVER['DOCUMENT_ROOT'].$this->dir.$filename;
				
				if (!file_exists($compiled_file)) {
			
					foreach($this->sources as $file) {
							
						$content = file_get_contents($_SERVER['DOCUMENT_ROOT'].$file);
						
						if (!$this->error && $content !== false) {
							$data .= $content."\n\n";
						} else {
							$this->error = true; 
						}
					}
				
					if (!$this->error && $data) {
						
						if ($minify) {
							$data = Minify_CSS_Compressor::process( $data );
						}
						
						// compiled name
						if ($data) {
							$export = file_put_contents($compiled_file,$data);
						}
	
						// chmode
						if( false !== $export) {
							@chmod( $compiled_file, $this->chmod );
						} else {
							$this->error = true; 
						}
					}
				}
			}
			
			return $this->export($filename);
		}
		
		/**
		 * Export links
		 * @return string
		 */
		protected function export($filename) {
			
			if (!$this->error && $filename) {
				$link = $this->dir.$filename;
				$media = $this->media;
				return "<link rel=\"stylesheet\" type=\"text/css\" media=\"$media\" href=\"$link\" />\n";
			} 
			elseif ($this->sources) {
				foreach( $this->sources as $file ) {
					$media = $this->media;
					$return .= "<link rel=\"stylesheet\" type=\"text/css\" media=\"$media\" href=\"$file\" />\n";
				}
				return $return;
			}
		}
	}