<?php

	class Pager {

		private static $instance;
		
		public function __construct($settings) {
			
			foreach ($settings as $key => $value) {
				$this->$key = $value;
			}
			
			$this->settings = Settings::init();
			$this->translate = Translate::instance();
			$this->buttons = (isset($this->buttons)) ? $this->buttons : true;
			$this->build();
		}
	
		private function build() {

			$rows = $this->rows ?: $this->settings->limit_pager_rows;

			if (is_numeric($rows)) {
				$this->pages = ceil($this->totalrows / $rows);
			} else {
				$this->pages = 1;
			}
			
			$Page = $this->translate->page;
				
			if ($this->pages > 1) {
		
				if ($this->page >= $this->settings->limit_pager_trigger) {
		
					$startLoop = $this->page - 6;
		
					if ($this->pages > $this->page + 5) $endLoop = $this->page + 5;
					else if ($this->page <= $this->pages && $this->page > $this->pages - 5) {
						$startLoop = $this->pages - $this->settings->limit_pager_trigger;
						$endLoop = $this->pages;
					}
					else $endLoop = $this->pages;
				}
				else {
					$startLoop = 1;
					if ($this->pages > $this->settings->limit_pager_trigger) $endLoop = $this->settings->limit_pager_trigger;
					else $endLoop = $this->pages;
				}
		
				// button first page
				if ($this->buttons && $this->page > 1) {
					$this->controlls .= "<li alt='1' title='$Page 1'>".$this->translate->page_first."</li>";
				}
		
				// button previous page
				if ($this->buttons && $this->page > 1) {
					$i = $this->page - 1;
					$this->controlls .= "<li alt='$i' title='$Page $i'>".$this->translate->page_previous."</li>";
				}
		
				// controlls
				for ($i = $startLoop; $i <= $endLoop; $i++) {
					$class = ($this->page == $i) ? "active" : null;
					$this->controlls .= "<li class='$class' alt='$i' title='$Page $i'>$i</li>";
				}
		
				// next page
				if ($this->buttons && $this->page < $this->pages) {
					$i = $this->page + 1;
					$this->controlls .= "<li alt='$i' title='$Page $i'>".$this->translate->page_next."</li>";
				}
		
				// last page
				if ($this->buttons && $this->page < $this->pages) {
					$this->controlls .= "<li alt='".$this->pages."' title='$Page ".$this->pages."'>".$this->translate->page_last."</li>";
				}
			}
		}
		
		public function controlls() {

			return "<ul class='table-controls'>".$this->controlls."</ul>$rows";
		}
		
		public function index() {
			
			if ($this->rows && $this->totalrows > $this->settings->limit_pager_rows) {
				$rows = ui::pagerRows($this->rows);
				$rows = "<div class='row-per-page'>Show $rows entries.</div>";
			}

			$index = $this->translate->page_index(array(
				'page' => $this->page,
				'pages' => $this->pages,
				'totalrows' => $this->totalrows
			));
			
			return "<div class='table-index'>$index. $rows</div>";
		}

		public function instance($config=array()) {

			if (!static::$instance) {
				static::$instance = new self($config);
			}

			return static::$instance;
		}
	}