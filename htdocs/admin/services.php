<?php
/********************************************************************

    services.php

    Lists services for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-06-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-06-18
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_browse_catalog_in_admin");
set_referer("service_alt.php");


$list = new ListView("select item_id, item_code, item_name, item_price, " .
                     "    if(item_visible, 'yes', 'no') as item_visible, " .
                     "    if(item_active, 'yes', 'no') as item_active, " .
                     "    item_category_name " .
                     "from items left join item_categories on item_category = item_category_id");

$list->set_entity("items");
$list->set_order("item_code");
$list->set_filter("item_type = " . ITEM_TYPE_SERVICES);

$list->add_column("item_code", "Code", "service.php", LIST_FILTER_FREE, COLUMN_NO_WRAP);

$list->add_column("item_name", "Name", "", LIST_FILTER_FREE);

$list->add_column("item_category_name", "Category Group", "", LIST_FILTER_LIST,
    "select item_category_name from item_categories", COLUMN_NO_WRAP);

$list->add_column("item_price", "Price", "", LIST_FILTER_FREE, COLUMN_NO_WRAP);
$list->add_column("item_active", "Active", "", LIST_FILTER_LIST, array(0 => "No", 1 => "Yes"));

if(has_access("can_edit_catalog"))
{
	$list->add_button(LIST_BUTTON_NEW, "New", "service.php");
	$list->add_button(LIST_BUTTON_FILTER, "Filter");
	$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");
}

$list->process();

$page = new Page("items");

$page->header();
$page->title("Services");
$list->render();
$page->footer();

?>
