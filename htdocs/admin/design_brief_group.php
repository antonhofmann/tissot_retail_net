<?php
/********************************************************************

    design_brief_group.php

    Creation and mutation of design briefing groups.

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2017-12-11
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2017-12-11
    Version:        1.0.0

    Copyright (c) 2017, TISSOT SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$form = new Form("design_brief_groups", "design_brief_group");

$form->add_section();

$form->add_edit("design_brief_group_name", "Name*", NOTNULL | UNIQUE);
$form->add_edit("design_brief_group_sort", "Sort Order*", NOTNULL, '', TYPE_INT, 2);
$form->add_checkbox("design_brief_group_active", "In Use", true);



$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("design_brief_groups");
$page->header();
$page->title(id() ? "Edit Design Briefing Section" : "Add Design Briefing Section");
$form->render();
$page->footer();

?>