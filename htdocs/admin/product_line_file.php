<?php
/********************************************************************

    product_line_file.php

    Creation and mutation of product line file records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-13
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-09-17
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("product_line_files", "file");

if (id())
{
    $sql = "select product_line_file_product_line from product_line_files where product_line_file_id = " . id();
    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);
    param("product_line", $row[0]);
}

$form->add_hidden("product_line", param("product_line"));

$form->add_section();
$form->add_lookup("product_line_file_product_line", "Product Line", "product_lines", "product_line_name", 0, param("product_line"));

$form->add_section();
$form->add_edit("product_line_file_title", "Title", NOTNULL);
$form->add_multiline("product_line_file_description", "Description", 4);

$sql = "select product_line_name from product_lines where product_line_id = " . param("product_line");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_row($res);
$product_line_name = make_valid_filename($row[0]);

$form->add_section();
$form->add_upload("product_line_file_path", "File", "/files/product_lines/$product_line_name");

$form->add_section();
$form->add_list("product_line_file_type", "Type",
    "select file_type_id, file_type_name from file_types order by file_type_name");
$form->add_list("product_line_file_purpose", "Purpose",
    "select file_purpose_id, file_purpose_name from file_purposes order by file_purpose_name");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("product_lines");

$page->header();
$page->title(id() ? "Edit Product Line File" : "Add Product Line File");
$form->render();
$page->footer();

?>
