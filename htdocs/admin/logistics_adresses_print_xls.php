<?php
/********************************************************************

    logistics_adresses_print_xls.php

    Generate Excel-File of Addresses

    Created by:     Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date created:   2015-01-06
    Modified by:    Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-06
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.
*********************************************************************/

require_once "../include/frame.php";

if(has_access("can_edit_catalog") 
  or has_access("can_edit_logistics_system_data"))
{

}
else
{
	check_access("can_edit_catalog");
}

require_once "../include/xls/Writer.php";
require_once "../pos/include/get_functions.php";

$user = get_user(user_id());

/********************************************************************
    prepare Data Needed
*********************************************************************/


/********************************************************************
    prepare Data
*********************************************************************/
$filter = "";
$header = "Retail Net Addresses " . " (" . date("d.m.Y G:i") . ")";

$sql_d = "select * " .
		 "from addresses " . 
		 "left join countries on address_country = country_id " .
		 "left join places on place_id = address_place_id " .
		 "left join provinces on province_id = place_province " .
		 "where address_active = 1 and address_type = 1 " . 
		 " order by country_name, address_company";


/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "retailnet_addresses_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename);
$xls->setVersion(8);

$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);
$f_normal->setTextWrap();
$f_normal->setVAlign('top');

$f_normal_bold =& $xls->addFormat();
$f_normal_bold->setSize(8);
$f_normal_bold->setAlign('left');
$f_normal_bold->setBorder(1);
$f_normal_bold->setBold();
$f_normal_bold->setVAlign('top');


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);
$f_number->setVAlign('top');

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);
$f_center->setVAlign('top');

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('yellow');



//captions
$captions = array();
//$captions[] = "Nr";
//$captions[] = "Geographical Region";
$captions[] = "Country";
$captions[] = "Company";
$captions[] = "Company 2";
$captions[] = "Address 1";
$captions[] = "Address 2";
$captions[] = "Province";
$captions[] = "Zip";
$captions[] = "City";
$captions[] = "VAT ID";
$captions[] = "Name";
$captions[] = "Phone";
$captions[] = "Email";
$captions[] = "FEDEX/DHL";
$captions[] = "Original COO and Invoice";
$captions[] = "Notes";
$captions[] = "Comments";

/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);
$sheet->writeRow(1, 0, $captions, $f_normal_bold);


$row_index = 2;
$cell_index = 0;
$counter = 0;
$col_widths = array();
for($i=0;$i<count($captions);$i++)
{
	$col_widths[$i] = strlen($captions[$i]);
}


$res = mysql_query($sql_d) or dberror($sql_d);
while ($row = mysql_fetch_assoc($res))
{

	$sheet->write($row_index, $cell_index, $row["country_name"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["country_name"]))
	{
		$col_widths[$cell_index] = strlen($row["country_name"]);
	}
	$cell_index++;

	
	$sheet->write($row_index, $cell_index, $row["address_company"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_company"]))
	{
		$col_widths[$cell_index] = strlen($row["address_company"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_company2"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_company2"]))
	{
		$col_widths[$cell_index] = strlen($row["address_company2"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_address"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_address"]))
	{
		$col_widths[$cell_index] = strlen($row["address_address"]);
	}
	$cell_index++;

	
	$sheet->write($row_index, $cell_index, $row["address_address2"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_address2"]))
	{
		$col_widths[$cell_index] = strlen($row["address_address2"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["province_canton"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["province_canton"]))
	{
		$col_widths[$cell_index] = strlen($row["province_canton"]);
	}
	$cell_index++;

	
	$sheet->write($row_index, $cell_index, $row["address_zip"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_zip"]))
	{
		$col_widths[$cell_index] = strlen($row["address_zip"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["place_name"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["place_name"]))
	{
		$col_widths[$cell_index] = strlen($row["place_name"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_vat_id"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_vat_id"]))
	{
		$col_widths[$cell_index] = strlen($row["address_vat_id"]);
	}
	$cell_index++;


	$sheet->write($row_index, $cell_index, $row["address_financecontroller_name"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_financecontroller_name"]))
	{
		$col_widths[$cell_index] = strlen($row["address_financecontroller_name"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_financecontroller_phone"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_financecontroller_phone"]))
	{
		$col_widths[$cell_index] = strlen($row["address_financecontroller_phone"]);
	}
	$cell_index++;
	
	
	$sheet->write($row_index, $cell_index, $row["address_financecontroller_email"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_financecontroller_email"]))
	{
		$col_widths[$cell_index] = strlen($row["address_financecontroller_email"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_logistics_fedex_dhl"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_logistics_fedex_dhl"]))
	{
		$col_widths[$cell_index] = strlen($row["address_logistics_fedex_dhl"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_logistics_coo"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_logistics_coo"]))
	{
		$col_widths[$cell_index] = strlen($row["address_logistics_coo"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_logistics_notes"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_logistics_notes"]))
	{
		$col_widths[$cell_index] = strlen($row["address_logistics_notes"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_logistics_comments"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_logistics_comments"]))
	{
		$col_widths[$cell_index] = strlen($row["address_logistics_comments"]);
	}
	$cell_index++;

		
	$cell_index = 0;
	$row_index++;

}

for($i=0;$i<count($captions);$i++)
{
	$sheet->setColumn($i, $i, $col_widths[$i]);
}


$xls->close(); 

?>