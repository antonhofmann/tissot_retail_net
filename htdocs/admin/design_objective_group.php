<?php
/********************************************************************

    design_objective_group.php

    Creation and mutation of design objective group records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-05
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2010-12-03
    Version:        2.0.0

    Copyright (c) 2002-2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_browse_catalog_in_admin");
set_referer("design_objective.php");

// Build form

/* old version depending on product line
$form = new Form("design_objective_groups", "design objective group");

$form->add_hidden("design_objective_group_priority");

$form->add_section();
$form->add_edit("design_objective_group_name", "Name", NOTNULL);
$form->add_list("design_objective_group_product_line", "Product Line",
    "select product_line_id, product_line_name " .
    "from product_lines " .
    "order by product_line_name", NOTNULL, param("product_line"));

$form->add_section();
$form->add_checkbox("design_objective_group_multiple", "Allows multiple selection of items");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
$form->add_button("add_objective", "Add Design Objective", "design_objective.php?group=" . id(), OPTIONAL);

$form->add_subtable("objectives", "Design Objectives", "design_objective.php", "Name",
    "select design_objective_item_id, design_objective_item_name as Name " .
    "from design_objective_items " .
    "where design_objective_item_group = " . id() . " " .
    "order by design_objective_item_priority", "design_objective_item_priority");
*/


// new version depending on pos type
$form = new Form("design_objective_groups", "design objective group");

$form->add_hidden("design_objective_group_priority");

$form->add_section();
$form->add_edit("design_objective_group_name", "Name", NOTNULL);
$form->add_list("design_objective_group_postype", "POS Type",
    "select postype_id, postype_name " .
    "from postypes " .
    "order by postype_name", NOTNULL);

$form->add_section();
$form->add_checkbox("design_objective_group_multiple", "Allows multiple selection of items");
$form->add_checkbox("design_objective_group_active", "Active");



if(has_access("can_edit_catalog"))
{
	$form->add_button(FORM_BUTTON_SAVE, "Save");
	$form->add_button(FORM_BUTTON_BACK, "Back");
	$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
	$form->add_button("add_objective", "Add Design Objective", "design_objective.php?group=" . id(), OPTIONAL);


	$form->add_subtable("objectives", "<br />Design Objectives", "design_objective.php", "Name",
    "select design_objective_item_id, design_objective_item_name as Name " .
    "from design_objective_items " .
    "where design_objective_item_group = " . id() . " " .
    "order by design_objective_item_priority", "design_objective_item_priority");

}
else
{
	$form->add_button(FORM_BUTTON_BACK, "Back");


	$form->add_subtable("objectives", "<br />Design Objectives", "design_objective.php", "Name",
    "select design_objective_item_id, design_objective_item_name as Name " .
    "from design_objective_items " .
    "where design_objective_item_group = " . id() . " " .
    "order by design_objective_item_priority");

}




// Populate form and process button clicks

$form->populate();

if (!$form->value("design_objective_group_priority"))
{
    $sql = "select max(design_objective_group_priority) from design_objective_groups";
    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);
    $form->value("design_objective_group_priority", $row[0] + 1);
}

$form->process();

// Render page

$page = new Page("design_objective_groups");

$page->header();
$page->title(id() ? "Edit Design Objective Group" : "Add Design Objective Group");
$form->render();
$page->footer();

?>