<?php
/********************************************************************

    doc_mailalerts.php

    add/edit mailalerts

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2013-05-01
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2013-05-01
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

/********************************************************************
    prepare all data needed
*********************************************************************/

$context = array();
$context["cer"] = "LNR/CER";
$context["archive"] = "Archiv";
$context["user"] = "Orders and Projects";
$context["posindex"] = "Pos Index";
$context["admin"] = "System Administration";

/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("doc_mailalerts", "doc_mailalerts");


$form->add_list("doc_mailalert_module", "Moduel", $context);
$form->add_edit("doc_mailalert_context", "Context*", NOTNULL);
$form->add_edit("doc_mailalert_description", "Description*", NOTNULL);
$form->add_edit("doc_mailalert_code", "Code*", NOTNULL);
$form->add_edit("doc_mailalert_event", "Event*", NOTNULL);
$form->add_edit("doc_mailalert_trigger", "Trigger*", NOTNULL);

$form->add_multiline("doc_mailalert_mailsubject", "Mail Subject*", 6, NOTNULL);
$form->add_multiline("doc_mailalert_mailtext", "Mail Text*", 20, NOTNULL);

$form->add_edit("doc_mailalert_attachments", "Attachments", NOTNULL);

$form->add_edit("doc_mailalert_sender", "Sender", NOTNULL);
$form->add_multiline("doc_mailalert_recipients", "Recipients", 8, NOTNULL);
$form->add_multiline("doc_mailalert_cc_recipients", "CC Recipients", 8, NOTNULL);


$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button("back", "Back");

$form->populate();
$form->process();



if($form->button("back"))
{
	redirect("doc_mailalerts.php");
}


// Render page

$page = new Page("mailalerts");



$page->header();
$page->title(id() ? "Edit Mail Alert" : "Add Mail Alert");
$form->render();


$page->footer();

?>