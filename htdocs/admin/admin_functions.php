<?php
/********************************************************************
    append records to table order_mails
*********************************************************************/
function append_mail($order_id, $recepient_id, $user_id, $text, $order_state_code, $type, $order_revision=0)
{

    $text = str_replace("\n\n", "\n", $text);
	$text = str_replace("\n\n", "\n", $text);

	$sql = "select order_state_id " .
           "from order_states ".
           "left join order_state_groups on order_state_group = order_state_group_id ".
           "where order_state_code = " . dbquote($order_state_code) .
           "    and order_state_group_order_type = " .$type;

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);

    $order_mail_fields = array();
    $order_mail_values = array();
    
    // insert record into table tasks

    $order_mail_fields[] = "order_mail_order";
    $order_mail_values[] = $order_id;

    if ($recepient_id != "")
    {
        $order_mail_fields[] = "order_mail_user";
        $order_mail_values[] = $recepient_id;
    }

    $order_mail_fields[] = "order_mail_from_user";
    $order_mail_values[] = $user_id;

    $order_mail_fields[] = "order_mail_text";
    $order_mail_values[] = dbquote($text);

    if ($order_state_code != "")
    {
        $order_mail_fields[] = "order_mail_order_state";
        $order_mail_values[] = $row["order_state_id"];
    }

	$order_mail_fields[] = "order_revisioned";
    $order_mail_values[] = $order_revision;

    $order_mail_fields[] = "date_created";
    $order_mail_values[] = "current_timestamp";

    $order_mail_fields[] = "date_modified";
    $order_mail_values[] = "current_timestamp";

    if (isset($_SESSION["user_login"]))
    {
        $order_mail_fields[] = "user_created";
        $order_mail_values[] = dbquote($_SESSION["user_login"]);

        $order_mail_fields[] = "user_modified";
        $order_mail_values[] = dbquote($_SESSION["user_login"]);
    }

    $sql = "insert into order_mails (" . join(", ", $order_mail_fields) . ") values (" . join(", ", $order_mail_values) . ")";
        mysql_query($sql) or dberror($sql);
}


/********************************************************************
    create a new project number
*********************************************************************/
function project_create_project_number_check_number($project_number)
{
	$sql = "select count(project_id) as num_recs " . 
		   "from projects " . 
		   "where project_number = " . dbquote($project_number);

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

    if($row["num_recs"] > 0)
	{
		return true;
	}
	else
	{
		return false;
	}

}

function project_create_new_project_number($new_year, $project_id)
{
    //get project data
	$sql = "select project_projectkind, project_postype, order_number from projects " . 
		   "left join orders on order_id = project_order " .
		   "where project_id = " . dbquote($project_id);
	
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);

	$project_number = $row["order_number"];
	$project_number_parts = explode('.'  , $project_number);

	$new_project_number = $new_year . '.' . $project_number_parts[1] . '.';
		
	$project_kind = $row['project_projectkind'];
	$pos_type = $row['project_postype'];
    
    // get project type's starting number
	if($project_kind == 4) // Take Over projects get separate number
	{
		// get latest project number
		$new_project_number .= 'T';
		
		$sql = "select project_number ".
			   "from projects ".
			   "where project_number Like '" . $new_project_number . "%' ".
			   "order by project_number desc ".
			   "limit 0,1";

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$last = substr($row["project_number"],9,2);
			$next = intval($last) + 1;

			if ($next < 10)
			{
				$next = "0" . $next;
			}
			$new_project_number=$new_project_number . $next;
		}
		else
		{
			$new_project_number= $new_project_number ."01";
		}

		//check if project number is occupied by another product line
		$occupied = project_create_project_number_check_number($new_project_number);

		while($occupied == true)
		{
			$last = substr($new_project_number,9,2);
			$next = intval($last) + 1;
			if ($next < 10)
			{
				$next = "0" . $next;
			}
			$new_project_number = substr($new_project_number,0,9) . $next;
			$occupied = project_create_project_number_check_number($new_project_number);
		}
		
	}
	elseif($project_kind == 5) // Lease Negotiation
	{
		// get latest project number
		$new_project_number .= 'L';
		
		$sql = "select project_number ".
			   "from projects ".
			   "where project_number Like '" . $new_project_number . "%' ".
			   "order by project_number desc ".
			   "limit 0,1";

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$last = substr($row["project_number"],9,2);
			$next = intval($last) + 1;

			if ($next < 10)
			{
				$next = "0" . $next;
			}
			$new_project_number=$new_project_number . $next;
		}
		else
		{
			$new_project_number= $new_project_number ."01";
		}

		//check if project number is occupied by another product line
		$occupied = project_create_project_number_check_number($new_project_number);

		while($occupied == true)
		{
			$last = substr($new_project_number,9,2);
			$next = intval($last) + 1;
			if ($next < 10)
			{
				$next = "0" . $next;
			}
			$new_project_number = substr($new_project_number,0,9) . $next;
			$occupied = project_create_project_number_check_number($new_project_number);
		}
		
	}
	elseif($project_kind == 8) // PopUp
	{
		// get latest project number
		$new_project_number .= 'P';
		
		$sql = "select project_number ".
			   "from projects ".
			   "where project_number Like '" . $new_project_number . "%' ".
			   "order by project_number desc ".
			   "limit 0,1";

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$last = substr($row["project_number"],9,2);
			$next = intval($last) + 1;

			if ($next < 10)
			{
				$next = "0" . $next;
			}
			$new_project_number=$new_project_number . $next;
		}
		else
		{
			$new_project_number= $new_project_number ."01";
		}

		//check if project number is occupied by another product line
		$occupied = project_create_project_number_check_number($new_project_number);

		while($occupied == true)
		{
			$last = substr($new_project_number,9,2);
			$next = intval($last) + 1;
			if ($next < 10)
			{
				$next = "0" . $next;
			}
			$new_project_number = substr($new_project_number,0,9) . $next;
			$occupied = project_create_project_number_check_number($new_project_number);
		}
		
	}
	else
	{
		$sql = "select * ".
			   "from product_line_pos_types ".
			   "where product_line_pos_type_pos_type = " . $pos_type . 
			   " and product_line_pos_type_product_line is NULL";

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$first = $row["product_line_pos_type_starting_number"];
		}
		

		if ($first < 10)
		{
			$first = "00" . $first;
		}
		elseif ($first < 100)
		{
			$first = "0" . $first;
		}

		// get latest project number of non take over project
		
		$sql = "select project_number ".
			   "from projects ".
			   "where project_number Like '" . $new_project_number . "%' ".
			   "    and project_postype = " . $pos_type . " " .
			   "    and project_projectkind <> 4 " . 
			   "order by project_number desc ".
			   "limit 0,1";

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$last = substr($row["project_number"],8,3);
			$next = intval($last) + 1;

			if ($next < 10)
			{
				$next = "00" . $next;
			}
			elseif ($next < 100)
			{
				$next = "0" . $next;
			}

			$new_project_number=$new_project_number . $next;
		}
		else
		{
			$new_project_number=$new_project_number . $first;
		}

		//check if project number is occupied by another product line
		$occupied = project_create_project_number_check_number($new_project_number);

		while($occupied == true)
		{
			$last = substr($new_project_number,8,3);
			$next = intval($last) + 1;
			if ($next < 10)
			{
				$next = "0" . $next;
			}
			elseif ($next < 100)
			{
				$next = "0" . $next;
			}
			$new_project_number = substr($new_project_number,0,8) . $next;
			$occupied = project_create_project_number_check_number($new_project_number);
		}
	}
	
    return $new_project_number;
}



/*************************************************************************
   get project_state_restrictions
**************************************************************************/
function get_project_state_restrictions($user_id)
{
    $project_state_restrictions = array();
	$project_state_restrictions['from_state'] = "";
	$project_state_restrictions['to_state'] = "";

    
    $sql = "select role_order_state_visible_from, role_order_state_visible_to ".
           "from user_roles ".
           "left join roles on user_role_role = role_id ".
           "where user_role_user = " . $user_id;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        if($row["role_order_state_visible_from"] and $row["role_order_state_visible_from"] > $project_state_restrictions['from_state'])
		{
			$project_state_restrictions['from_state'] = $row["role_order_state_visible_from"];
		}

		if($row["role_order_state_visible_to"] and ($project_state_restrictions['to_state'] == '' or $row["role_order_state_visible_to"] < $project_state_restrictions['to_state']))
		{
			$project_state_restrictions['to_state'] = $row["role_order_state_visible_to"];
		}
    }

    return $project_state_restrictions;
}

/*************************************************************************
   get all the roles of a user
**************************************************************************/
function get_user_roles($user_id, $application = '')
{
    $user_roles = array();
    
    $sql = "select role_name ".
           "from user_roles ".
           "left join roles on user_role_role = role_id ".
           "where user_role_user = " . $user_id . 
		   " and role_application = " . dbquote($application) . 
		   " order by role_name";

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $user_roles[] = $row["role_name"];
    }

    return $user_roles;
}



/*************************************************************************
   update all items and onging orders and projects with new prices
   after a change in suppliers exchange rate
**************************************************************************/
function update_new_prices()
{
	$order_filter = '';
	$order_ids = array();
	$order_exchange_rates = array();
	
	//01 get catalogue orders
	$sql = "select DISTINCT order_id, order_client_exchange_rate, currency_factor " . 
		   " from orders " .
		   " left join currencies on currency_id = order_client_currency " . 
		   " where order_type = 2 " . 
		   " and order_actual_order_state_code < '700' " . 
		   " and (order_archive_date is null or order_archive_date = '0000-00-00')";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$order_ids[] = $row["order_id"];
		$order_exchange_rates[$row["order_id"]] = array("exchange_rate"=>$row["order_client_exchange_rate"], "factor"=>$row["currency_factor"]);
	}

	//02 get projects
	$sql = " SELECT DISTINCT order_id, order_client_exchange_rate, currency_factor, " . 
			"sum(order_item_system_price_hq_freezed) as hq1, sum(order_item_system_price_freezed) as hq2 " . 
			"FROM orders " . 
			"LEFT JOIN currencies ON currency_id = order_client_currency " . 
			"LEFT JOIN order_items ON order_item_order = order_id " . 
			"WHERE order_type = 1 " . 
			"AND order_actual_order_state_code < '890' " . 
			"AND (order_archive_date IS NULL OR order_archive_date = '0000-00-00') " . 
			"AND order_item_type = 1  " . 
			"group by order_id, order_number, order_actual_order_state_code, order_client_exchange_rate, currency_factor";


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if($row["hq1"] == 0 and $row["hq2"] == 0)
		{
			$order_ids[] = $row["order_id"];
			$order_exchange_rates[$row["order_id"]] = array("exchange_rate"=>$row["order_client_exchange_rate"], "factor"=>$row["currency_factor"]);
		}
	}
	
	if(count($order_ids) > 0)
	{
		$order_filter = " order_item_order in (" . implode(',', $order_ids) . ")";
	}
	
	//03 update standard items and system price in order_items
	
	$items_updated = array();
	$sql = "select item_id, supplier_item_price, supplier_item_currency, " . 
		   " currency_exchange_rate_furniture, currency_factor " . 
		   " from items " .
		   " left join suppliers on supplier_item = item_id " .
		   " left join currencies on currency_id = supplier_item_currency " . 
		   " where item_active = 1 " . 
	       " and supplier_item_currency > 0 " . 
		   " and currency_exchange_rate_furniture > 0 and currency_factor > 0";


	$res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
		$system_price = $row["supplier_item_price"]*$row["currency_exchange_rate_furniture"]/$row["currency_factor"];

		if($system_price > 0)
		{
			$sql_u = "update items set item_price = " . dbquote($system_price) . 
				     " where item_id = " . dbquote($row["item_id"]) . 
				     " and item_active = 1";

			$result = mysql_query($sql_u) or dberror($sql_u);

			$items_updated[] = $row["item_id"];

			if($order_filter)
			{
				$sql_u = "update order_items set " . 
						 " order_item_system_price = " . dbquote($system_price) . 
						 " where "  . $order_filter . 
					     " and order_item_item = " . dbquote($row["item_id"]);
				
				$result = mysql_query($sql_u) or dberror($sql_u);
			}
		}
	}


	//04 update all client prices
	if(count($items_updated) > 0 and count($order_ids) > 0)
	{
		$item_filter = " order_item_item in (" . implode(',', $items_updated) . ")";
		foreach($order_ids as $key=>$order_id)
		{
			$sql_u = "update order_items set " . 
					 " order_item_client_price = order_item_system_price/" . $order_exchange_rates[$order_id]["exchange_rate"]*$order_exchange_rates[$order_id]["factor"] .  
					 " where order_item_order = " . dbquote($order_id) .
					 " and " . $item_filter;

			$result = mysql_query($sql_u) or dberror($sql_u);
		}
	}


	return true;


}

?>