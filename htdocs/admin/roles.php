<?php
/********************************************************************

    roles.php

    Lists roles for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-08-19
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("role.php");



$applications = array();
$applications['retaildevelopment'] = "Retail Development";
$applications['mps'] = "MPS";
$applications['red'] = "RED";
$applications['lps'] = "LPS";
$applications['news'] = "News";
$applications['uberalldata'] = "Uberall";


foreach($applications as $key=>$value)
{
	$listname=$key;

	$sql = "select role_id, role_application, role_code, role_name, " .
		   " concat(user_name, ' ', user_firstname) as responsible_admin," .
		   " if(role_active = 1, 'yes', 'no') as status " . 
		   "from roles " . 
		   " left join users on user_id = role_responsible_user_id ";
		   

	$$listname = new ListView($sql);

	$$listname->set_entity("roles");
	$$listname->set_order("role_name");
	$$listname->set_filter("role_application = " . dbquote($key));
	
	$$listname->set_title($value);


	//$list->add_column("role_application", "Application");
	$$listname->add_column("role_code", "Code");
	$$listname->add_column("role_name", "Name", "role.php");
	$$listname->add_column("responsible_admin", "Responsible");
	$$listname->add_column("status", "Active");

	$$listname->add_button(LIST_BUTTON_NEW, "New", "role.php");

	$$listname->process();
}

$page = new Page("roles");

$page->header();
$page->title("Roles");

foreach($applications as $key=>$value)
{
	$listname=$key;
	$$listname->render();
	echo "<p>&nbsp;</p>";
}
$page->footer();

?>
