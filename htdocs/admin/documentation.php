<?php
/********************************************************************

    documentation.php

    Entry page for the system documentation section.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2013-05-01
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2013-05-01
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$page = new Page("documentation");
$page->header();
$page->footer();

?>