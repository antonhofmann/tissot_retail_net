<?php
/********************************************************************

    Maialerts.php

    Lists alert reciepienst for mail alerts.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2010-03-19
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2010-03-19
    Version:        1.0.0

    Copyright (c) 2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("mailalert.php");


$sql ="select * from mail_alert_types";

$list = new ListView($sql);

$list->set_entity("mail_alert_types");
$list->set_order("mail_alert_type_text");

$list->add_column("mail_alert_type_shortcut", "Shortcut", "mailalert.php", "", "", COLUMN_NO_WRAP);
$list->add_column("mail_alert_type_sender_email", "Sender");
$list->add_column("mail_alert_type_recipients", "Recipients");
$list->add_column("mail_alert_type_cc1", "CC1");
$list->add_column("mail_alert_type_cc2", "CC2");
$list->add_column("mail_alert_type_cc3", "CC3");
$list->add_column("mail_alert_type_cc4", "CC4");

//$list->add_button(LIST_BUTTON_NEW, "New", "mailalert.php");

$list->process();

$page = new Page("mail_alert_types");

$page->header();
$page->title("Mail Alerts");
$list->render();
$page->footer();

?>
