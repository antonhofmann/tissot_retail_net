<?php
/********************************************************************

    logistics_standard_shipping_documents.php

    Lists standard shipping documents for editing .

    Created by:     Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date created:   2015-01-06
    Modified by:    Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-06
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(has_access("can_edit_catalog") 
  or has_access("can_edit_logistics_system_data"))
{

}
else
{
	check_access("can_edit_catalog");
}
set_referer("logistics_standard_shipping_document.php");

$sql = "select standard_shipping_document_id, standard_shipping_document_name " . 
       "from standard_shipping_documents ";


$sql_u = "select standard_shipping_document_id, " .
         "(select count(address_shipping_document_id) from address_shipping_documents where address_shipping_document_document_id = standard_shipping_document_id) as num_docs " . 
         "from standard_shipping_documents ";

$adrcount = array();
$res = mysql_query($sql_u) or dberror($sql_u);
while($row = mysql_fetch_assoc($res))
{
	$adrcount[$row["standard_shipping_document_id"]] = $row["num_docs"];
}


       

$list = new ListView($sql);

$list->set_entity("standard_shipping_documents");
$list->set_order("standard_shipping_document_name");

$list->add_column("standard_shipping_document_name", "Name", "logistics_standard_shipping_document.php");
$list->add_text_column("adrcount", "Clients", COLUMN_ALIGN_RIGHT, $adrcount);

$list->add_button(LIST_BUTTON_NEW, "New", "logistics_standard_shipping_document.php");

$list->process();

$page = new Page("logistics");
require "include/logistics_page_actions.php";
$page->header();
$page->title("Standard Shipping Documents");
$list->render();
$page->footer();

?>
