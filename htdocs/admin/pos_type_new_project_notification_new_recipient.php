<?php
/********************************************************************

    pos_type_new_project_notification_new_recipient.php

    Creation and editing of notification recipients.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-11-19
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-11-19
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");



$sql_client_types = "select client_type_id, client_type_code from client_types order by client_type_code";

$legal_types = array();
$legal_types[1] = "Corporate";
$legal_types[2] = "Franchisee/other";

$filter = "";
if(param("legal_type"))
{
	$filter = " where projecttype_newproject_notification_legal_type = " . param("legal_type");
}


if(param("client_type") and param("client_type") > 0)
{
	$sql_country = "select DISTINCT country_id, country_name " .
				   "from addresses " .
		           " left join countries on country_id = address_country " . 
		           " where address_client_type = " . param("client_type") . 
				   " order by country_name";
}
else
{
	$sql_country = "select DISTINCT country_id, country_name " .
				   "from countries " . 
				   " order by country_name";
}

$country_list = array();
$res = mysql_query($sql_country) or dberror($sql_country);
while($row = mysql_fetch_assoc($res)) 
{
	$country_list[] = $row["country_id"];
}


$sql_pos_types =  "select postype_id, postype_name from postypes " .
				  "order by postype_name";


$pos_type_list = array();
$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
while($row = mysql_fetch_assoc($res)) 
{
	$pos_type_list[] = $row["country_id"];
}


$form = new Form("projecttype_newproject_notifications", "New Project Notifications");


$form->add_section('Recipient');
$form->add_edit("email", "Email*", NOTNULL);

//type of mail
$form->add_section('Mail Alerts');
$form->add_checkbox("_new_project", "on submitting a project request", 0, 0, 'Send Mail');
$form->add_checkbox("lnsubmission", "on LNR-submission corporate projects");
$form->add_checkbox("lnresubmission", "on LNR-resubmission corporate projects");
$form->add_checkbox("afsubmission", "on LNR-submission third party projects");
$form->add_checkbox("afresubmission", "on LNR-resubmission third party projects");
$form->add_checkbox("cerafsubmission", "on CER-submission");
$form->add_checkbox("cerafresubmission", "on CER-resubmission");
$form->add_checkbox("inr03submission", "on INR03-submission");
$form->add_checkbox("inr03resubmission", "on INR03-resubmission");
$form->add_checkbox("rfafsubmission", "on submitting request for additional funding");
$form->add_checkbox("rfafresubmission", "on resubmitting request for additional funding");

$form->add_section('Client Type');
$form->add_list("client_type", "Client Type*", $sql_client_types, SUBMIT);

$form->add_section('Legal Type');
$form->add_list("legal_type", "Legal Type*", $legal_types, NOTNULL);


$form->add_section('POS Types');
$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
while ($row = mysql_fetch_assoc($res))
{
	$form->add_checkbox("PT_" . $row["postype_id"], "", 0, "", $row["postype_name"]);
}

$res = mysql_query($sql_country) or dberror($sql_country);
while($row = mysql_fetch_assoc($res)) 
{
	$country_list[] = $row["country_id"];
}


$form->add_section('Countries');
$form->add_label("select0", "Countries", RENDER_HTML, "<div id='select0'><a href='javascript:select_all_countries();'>select all countries</a></div>");

$res = mysql_query($sql_country) or dberror($sql_country);
while ($row = mysql_fetch_assoc($res))
{
	$form->add_checkbox("CO_" . $row["country_id"], "", 0, "", $row["country_name"]);
}




$form->add_button("save", "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

if($form->button("save") and $form->validate()) {
	
	
	$res = mysql_query($sql_country) or dberror($sql_country);
	while ($row = mysql_fetch_assoc($res))
	{
		if(array_key_exists("CO_" . $row["country_id"], $_POST) and  $_POST["CO_" . $row["country_id"]] == 1)
		{
			$can_update = 0;

			
			
			
			foreach($pos_type_list as $pos_type_id=>$pos_type_name)
			{
			
				if(array_key_exists("PT_" . $pos_type_id, $_POST) and  $_POST["PT_" . $pos_type_id] == 1)
				{
					
					if($form->value("legal_type") == 1)
					{
						$legal_type = 1;
					}
					else
					{
						$legal_type = 0;
					}
					
					//check if already there
					$projecttype_newproject_notification_id = 0;
					$sql_p = "select * from projecttype_newproject_notifications ". 
						   " where projecttype_newproject_notification_country = " . dbquote($row["country_id"]) . 
						   " and projecttype_newproject_notification_legal_type = " . dbquote($legal_type) . 
						   " and projecttype_newproject_notification_postype = " . dbquote($pos_type_id)  . 
						   " and (" . 
						   " projecttype_newproject_notification_email is null or projecttype_newproject_notification_email = '' " .
						   " or projecttype_newproject_notification_emailcc1 is null or projecttype_newproject_notification_emailcc1 = '' " .
						   " or projecttype_newproject_notification_emailcc2 is null or projecttype_newproject_notification_emailcc2 = '' " .
						   " or projecttype_newproject_notification_emailcc3 is null or projecttype_newproject_notification_emailcc3 = '' " .
						   " or projecttype_newproject_notification_emailcc4 is null or projecttype_newproject_notification_emailcc4 = '' " .
						   " or projecttype_newproject_notification_emailcc5 is null or projecttype_newproject_notification_emailcc5 = '' " .
						   " or projecttype_newproject_notification_emailcc6 is null or projecttype_newproject_notification_emailcc6 = '' " .
						   " or projecttype_newproject_notification_emailcc7 is null or projecttype_newproject_notification_emailcc7 = '' " .
						   " )";


					$res_p = mysql_query($sql_p) or dberror($sql_p);
					if ($row_p = mysql_fetch_assoc($res_p))
					{
						$fields = array();
		
						$value = dbquote($form->value("email"));
						if($row_p["projecttype_newproject_notification_email"] == null or $row_p["projecttype_newproject_notification_email"] == '')
						{
							$fields[] = "projecttype_newproject_notification_email = " . $value;
						}
						elseif($row_p["projecttype_newproject_notification_emailcc1"] == null or $row_p["projecttype_newproject_notification_emailcc1"] == '')
						{
							$fields[] = "projecttype_newproject_notification_emailcc1 = " . $value;
						}
						elseif($row_p["projecttype_newproject_notification_emailcc2"] == null or $row_p["projecttype_newproject_notification_emailcc2"] == '')
						{
							$fields[] = "projecttype_newproject_notification_emailcc2 = " . $value;
						}
						elseif($row_p["projecttype_newproject_notification_emailcc3"] == null or $row_p["projecttype_newproject_notification_emailcc3"] == '')
						{
							$fields[] = "projecttype_newproject_notification_emailcc3 = " . $value;
						}
						elseif($row_p["projecttype_newproject_notification_emailcc4"] == null or $row_p["projecttype_newproject_notification_emailcc4"] == '')
						{
							$fields[] = "projecttype_newproject_notification_emailcc4 = " . $value;
						}
						elseif($row_p["projecttype_newproject_notification_emailcc5"] == null or $row_p["projecttype_newproject_notification_emailcc5"] == '')
						{
							$fields[] = "projecttype_newproject_notification_emailcc5 = " . $value;
						}
						elseif($row_p["projecttype_newproject_notification_emailcc6"] == null or $row_p["projecttype_newproject_notification_emailcc6"] == '')
						{
							$fields[] = "projecttype_newproject_notification_emailcc6 = " . $value;
						}
						elseif($row_p["projecttype_newproject_notification_emailcc7"] == null or $row_p["projecttype_newproject_notification_emailcc7"] == '')
						{
							$fields[] = "projecttype_newproject_notification_emailcc7 = " . $value;
						}
						
						if(array_key_exists("new_project", $_POST) and $_POST["new_project"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_on_new_project = 1";
						}
						else
						{
							//$fields[] = "projecttype_newproject_notification_on_new_project = 0";
						}
						if(array_key_exists("lnsubmission", $_POST) and $_POST["lnsubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_on_lnsubmission = 1";
						}
						else
						{
							//$fields[] = "projecttype_newproject_notification_on_lnsubmission = 0";
						}
						if(array_key_exists("lnresubmission", $_POST) and $_POST["lnresubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_on_lnresubmission = 1";
						}
						else
						{
							//$fields[] = "projecttype_newproject_notification_on_lnresubmission = 0";
						}



						if(array_key_exists("afsubmission", $_POST) and $_POST["afsubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_on_afsubmission = 1";
						}
						else
						{
							//$fields[] = "projecttype_newproject_notification_on_lnsubmission = 0";
						}
						if(array_key_exists("afresubmission", $_POST) and $_POST["afresubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_on_lnresubmission = 1";
						}
						else
						{
							//$fields[] = "projecttype_newproject_notification_on_lnresubmission = 0";
						}


						if(array_key_exists("cerafsubmission", $_POST) and $_POST["cerafsubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_oncerafsubmission = 1";
						}
						else
						{
							//$fields[] = "projecttype_newproject_notification_oncerafsubmission = 0";
						}
						if(array_key_exists("cerafresubmission", $_POST) and $_POST["cerafresubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_oncerafresubmission = 1";
						}
						else
						{
							//$fields[] = "projecttype_newproject_notification_oncerafresubmission = 0";
						}
						
						
						if(array_key_exists("inr03submission", $_POST) and $_POST["inr03submission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_oninr03submission = 1";
						}
						else
						{
							//$fields[] = "projecttype_newproject_notification_on_lnsubmission = 0";
						}
						if(array_key_exists("inr03resubmission", $_POST) and $_POST["inr03resubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_oninr03resubmission = 1";
						}
						else
						{
							//$fields[] = "projecttype_newproject_notification_on_lnresubmission = 0";
						}
						
						
						if(array_key_exists("rfafsubmission", $_POST) and $_POST["rfafsubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_on_rfafsubmission = 1";
						}
						else
						{
							//$fields[] = "projecttype_newproject_notification_on_rfafsubmission = 0";
						}
						if(array_key_exists("rfafresubmission", $_POST) and $_POST["rfafresubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_on_rfafresubmission = 1";
						}
						else
						{
							//$fields[] = "projecttype_newproject_notification_on_rfafresubmission = 0";
						}


						$value = "current_timestamp";
						$fields[] = "date_modified = " . $value;

						$value = $_SESSION["user_login"];
						$fields[] = "user_modified = " . dbquote($value);
				   
						$sql = "update projecttype_newproject_notifications set " . join(", ", $fields) . 
							   " where projecttype_newproject_notification_country = " . dbquote($row["country_id"]) . 
							   " and projecttype_newproject_notification_legal_type = " . dbquote($legal_type) .
							   " and projecttype_newproject_notification_postype = " . dbquote($pos_type_id);

						
						$result = mysql_query($sql) or dberror($sql);
					}
					else
					{
						$fields = array();
						$values = array();


						$fields[] = "projecttype_newproject_notification_country";
						$values[] = dbquote($row["country_id"]);

						$fields[] = "projecttype_newproject_notification_legal_type";
						$values[] = dbquote($legal_type);

						$fields[] = "projecttype_newproject_notification_postype";
						$values[] = dbquote($pos_type_id);

						$fields[] = "projecttype_newproject_notification_email";
						$values[] = dbquote($form->value("email"));

						
						if(array_key_exists("new_project", $_POST) and $_POST["new_project"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_on_new_project";
							$values[] = 1;
						}
						else
						{
							$fields[] = "projecttype_newproject_notification_on_new_project";
							$values[] = 0;
						}
						if(array_key_exists("lnsubmission", $_POST) and $_POST["lnsubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_on_lnsubmission";
							$values[] = 1;
						}
						else
						{
							$fields[] = "projecttype_newproject_notification_on_lnsubmission";
							$values[] = 0;
						}
						if(array_key_exists("lnresubmission", $_POST) and $_POST["lnresubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_on_lnresubmission";
							$values[] = 1;
						}
						else
						{
							$fields[] = "projecttype_newproject_notification_on_lnresubmission";
							$values[] = 0;
						}


						if(array_key_exists("afsubmission", $_POST) and $_POST["afsubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_on_afsubmission";
							$values[] = 1;
						}
						else
						{
							$fields[] = "projecttype_newproject_notification_on_afsubmission";
							$values[] = 0;
						}
						if(array_key_exists("afresubmission", $_POST) and $_POST["afresubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_on_afresubmission";
							$values[] = 1;
						}
						else
						{
							$fields[] = "projecttype_newproject_notification_on_afresubmission";
							$values[] = 0;
						}

						if(array_key_exists("cerafsubmission", $_POST) and $_POST["cerafsubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_oncerafsubmission";
							$values[] = 1;
						}
						else
						{
							$fields[] = "projecttype_newproject_notification_oncerafsubmission";
							$values[] = 0;
						}
						if(array_key_exists("cerafresubmission", $_POST) and $_POST["cerafresubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_oncerafresubmission";
							$values[] = 1;
						}
						else
						{
							$fields[] = "projecttype_newproject_notification_oncerafresubmission";
							$values[] = 0;
						}

						if(array_key_exists("inr03submission", $_POST) and $_POST["inr03submission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_oninr03submission";
							$values[] = 1;
						}
						else
						{
							$fields[] = "projecttype_newproject_notification_oninr03submission";
							$values[] = 0;
						}
						if(array_key_exists("inr03resubmission", $_POST) and $_POST["inr03resubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_oninr03resubmission";
							$values[] = 1;
						}
						else
						{
							$fields[] = "projecttype_newproject_notification_oninr03resubmission";
							$values[] = 0;
						}

						if(array_key_exists("rfafsubmission", $_POST) and $_POST["rfafsubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_on_rfafsubmission";
							$values[] = 1;
						}
						else
						{
							$fields[] = "projecttype_newproject_notification_on_rfafsubmission";
							$values[] = 0;
						}
						if(array_key_exists("rfafresubmission", $_POST) and $_POST["rfafresubmission"] == 1)
						{
							$fields[] = "projecttype_newproject_notification_on_rfafresubmission";
							$values[] = 1;
						}
						else
						{
							$fields[] = "projecttype_newproject_notification_on_rfafresubmission";
							$values[] = 0;
						}

						$fields[] = "user_created";
						$values[] = dbquote($_SESSION["user_login"]);

						$fields[] = "date_created";
						$values[] = dbquote(date("Y-m-d H:i:s"));

					
						$sql = "insert into projecttype_newproject_notifications (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
				}
			}
		}
	}
	
	$link = "pos_type_new_project_notifications.php";
	redirect($link);
}

$page = new Page("projecttype_newproject_notifications");

$page->header();
$page->title("Add New Recipient");
$form->render();


?>

<script language='javascript'>

	function select_all_countries()
	{
		<?php
		foreach($country_list as $key=>$country_id)
		{
			echo "$('input[name=CO_" . $country_id . "]').attr('checked', true);";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:deselect_all_countries();'>deselect all countries</a>";
		
		
	}

	function deselect_all_countries()
	{
		<?php
		foreach($country_list as $key=>$country_id)
		{
			echo "$('input[name=CO_" . $country_id . "]').attr('checked', false);";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:select_all_countries();'>select all countries</a>";
	}

</script>



<?php
$page->footer();

?>
