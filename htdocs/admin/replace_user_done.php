<?php
/********************************************************************

    replace_user.php

    replaces a user by another

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-06
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

// Build form



// Render page

$page = new Page("users");

$page->header();
$page->title("Replace a User by another User");

echo "<p>", "The replacement has been performed.", "</p>";

$page->footer();

?>