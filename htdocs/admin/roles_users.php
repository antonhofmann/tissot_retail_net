<?php
/********************************************************************

    roles_users.php

    Lists roles for selecting.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2015-07-08
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2015-07-08
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("role.php");





$form = new Form("roles", "role");
$form->add_button("print_all", "Print all");
$form->add_button("print_selected", "Print selected");



$sql = "select client_type_id, client_type_code from client_types";

$list = new ListView($sql);

$list->set_entity("client_types");
$list->set_order("client_type_code");
$list->set_title("Client Types");

$list->add_checkbox_column("clienttype", "select", 0, "");
$list->add_column("client_type_code", "Client type");


$list->process();


$applications = array();
$applications['retaildevelopment'] = "Retail Development";
$applications['mps'] = "MPS";
$applications['red'] = "RED";
$applications['lps'] = "LPS";
$applications['news'] = "News";



foreach($applications as $key=>$value)
{
	$listname=$key;

	$sql = "select role_id, role_application, role_code, role_name, " .
		   " concat(user_name, ' ', user_firstname) as responsible_admin " . 
		   "from roles " . 
		   " left join users on user_id = role_responsible_user_id ";
		   

	$$listname = new ListView($sql);

	
	$$listname->set_entity("roles");
	$$listname->set_order("role_name");
	$$listname->set_filter("role_application = " . dbquote($key));
	
	$$listname->set_title($value);


	$$listname->add_checkbox_column("print", "print", 0, "");
	$$listname->add_column("role_code", "Code");
	$$listname->add_column("role_name", "Name", "");
	$$listname->add_column("responsible_admin", "Responsible");

	$$listname->process();
}



$form2 = new Form("roles", "role");
$form2->add_button("print_all2", "Print all");
$form2->add_button("print_selected2", "Print selected");

$form->process();
$form2->process();


if($form->button("print_all") or $form->button("print_all2"))
{
	$link="roles_users_xls.php";
	redirect($link);
}
elseif($form->button("print_selected") or $form->button("print_selected2"))
{
	$selected_roles = array();
	$selected_client_types = array();
	if(count($_POST) > 0)
	{
		foreach($_POST as $key=>$value)
		{
			if(strpos($key, 'clienttype') > 0)
			{
				if(substr($key, 26, strlen($key)) > 0)
				{
					$selected_client_types[] = substr($key, 26, strlen($key));
				}
			}
			elseif(strpos($key, 'print') > 0)
			{
				if(substr($key, 14, strlen($key)) > 0)
				{
					$selected_roles[] = substr($key, 14, strlen($key));
				}
			}
		}
	}

	if(count($selected_roles) > 0 or count($selected_client_types) > 0)
	{
		$tmp = implode('_', $selected_roles);
		$link="roles_users_xls.php?roles=" . $tmp;

		$tmp = implode('_', $selected_client_types);
		$link .="&ct=" . $tmp;

		redirect($link);
		
	}
}


$page = new Page("roles");

$page->header();
$page->title("Print User Roles");

$form->render();
echo "<p>&nbsp;</p>";

$list->render();

echo "<p>&nbsp;</p>";

foreach($applications as $key=>$value)
{
	$listname=$key;
	$$listname->render();
	echo "<p>&nbsp;</p>";
}

$form2->render();



$page->footer();

?>
