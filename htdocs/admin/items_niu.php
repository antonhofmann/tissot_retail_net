<?php
/********************************************************************

    items_niu.php

    Lists items for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-25
    Version:        1.0.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("item.php");


//set icons for picture indicating column
$images = array();

$sql = "select item_file_item " . 
       "from item_files " .
       "where item_file_purpose = 2";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    $images[$row["item_file_item"]] = "/pictures/bullet_ball_glass_green.gif";
}





$sql = "select DISTINCT item_id, item_code, item_name, item_price, " .
		 "    if(item_visible, 'yes', ' ') as item_visible, " .
		 "    if(item_visible_in_production_order, 'yes', ' ') as item_visible_in_production_order, " . 
		 "    if(item_visible_in_mps, 'yes', ' ') as item_visible_in_mps, " .
		  "   if(item_stock_property_of_swatch, 'yes', ' ') as item_stock_property_of_swatch, unit_name " .
		 "from items " .
		 "left join suppliers on supplier_item = item_id " .
		 "left join units on unit_id = item_unit";

$filter = "item_active = 0 and (item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . " or item_type <= " . ITEM_TYPE_COST_ESTIMATION . ")";

if(param("supplier_filter")) {
	$filter .= " and supplier_address = " . param("supplier_filter");
}

if(param("category_filter")) {
	
	$filter .= " and item_category = " . param("category_filter");
}

//get purchase price

$purchase_prices = array();
$property = array();
$dr_swatch = array();

$sql_p = "select item_id, supplier_item_price, currency_symbol, currency_factor, supplier_item_price, " . 
         "currency_exchange_rate, item_stock_property_of_swatch, item_is_dr_swatch_furniture " .
		 "from items " .
		 "left join suppliers on supplier_item = item_id " .
		 "left join currencies on currency_id = supplier_item_currency " . 
		 " where item_active = 0 and (item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . " or item_type <= " . ITEM_TYPE_COST_ESTIMATION . ")";


$res = mysql_query($sql_p) or dberror($sql_p);

while ($row = mysql_fetch_assoc($res))
{
    //$factor = 1;
	//if($row["currency_factor"] > 0) { $factor =$row["currency_factor"];}
	//$purchase_prices[$row["item_id"]] = number_format($row["supplier_item_price"]*$row["currency_exchange_rate"]/$factor, 2, ".", "'");

	$purchase_prices[$row["item_id"]] = number_format($row["supplier_item_price"], 2, ".", "'") . ' ' . $row["currency_symbol"];

	if($row["item_stock_property_of_swatch"] == 1)
	{
		$property[$row["item_id"]] = "/pictures/stockproperty.gif";
	}

	if($row["item_is_dr_swatch_furniture"] == 1)
	{
		$dr_swatch[$row["item_id"]] = "/pictures/customerservice.png";
	}


}



/********************************************************************
    Create Filter
*********************************************************************/ 
$sql_suppliers = "select DISTINCT supplier_address, address_company from suppliers " . 
                 "left join addresses on address_id = supplier_address " . 
				 "where address_active = 1 " . 
				 " order by address_company";

$sql_item_categories = "select item_category_id, item_category_name from item_categories " .
    "order by item_category_name";


$supplier_filter = array();
$res = mysql_query($sql_suppliers);
while($row = mysql_fetch_assoc($res))
{
	$supplier_filter[$row["supplier_address"]] = $row["address_company"];
}

$category_filter = array();
$res = mysql_query($sql_item_categories);
while($row = mysql_fetch_assoc($res))
{
	$category_filter[$row["item_category_id"]] = $row["item_category_name"];
}

/********************************************************************
    Create List
*********************************************************************/ 


if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
{
	$link = APPLICATION_URL . "/admin";
}
else
{
	$link = "/admin";
}


$list = new ListView($sql);

$list->set_entity("items");
$list->set_order("item_code");
$list->set_filter($filter);

$list->add_listfilters("supplier_filter", "Supplier", 'select', $supplier_filter, param("supplier_filter"));
$list->add_listfilters("category_filter", "Category", 'select', $category_filter, param("category_filter"));


$list->add_column("item_code", "Code", $link . "/item.php?id={item_id}", LIST_FILTER_FREE, COLUMN_NO_WRAP);

$list->add_image_column("pix", "Pix", 0, $images);

$list->add_column("item_name", "Name", "", LIST_FILTER_FREE);
$list->add_column("unit_name", "Unit");



//$list->add_column("item_type_name", "Type", "", LIST_FILTER_LIST, "select item_type_name from item_types", COLUMN_NO_WRAP);

//$list->add_column("item_category_name", "Category Group", "", LIST_FILTER_LIST, "select item_category_name from item_categories", COLUMN_NO_WRAP);

$list->add_text_column("purchase_price", "Purchase \nPrice", COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $purchase_prices);

$list->add_column("item_price", "Sales \nPrice CHF", "", LIST_FILTER_FREE, "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("item_visible", "Visible", "", LIST_FILTER_LIST, array(0 => " ", 1 => "Yes"));
$list->add_column("item_visible_in_production_order", "Planning", "", LIST_FILTER_LIST, array(0 => " ", 1 => "Yes"));
$list->add_column("item_visible_in_mps", "Merchandising", "", LIST_FILTER_LIST, array(0 => " ", 1 => "Yes"));

$list->add_image_column("propterty", "Property of \nTissot", COLUMN_BREAK | COLUMN_ALIGN_CENTER, $property);
$list->add_image_column("dr_swatch", "Customer Services", COLUMN_BREAK | COLUMN_ALIGN_CENTER, $dr_swatch);

$list->process();


$page = new Page("items");

$page->header();
$page->title("Items");
$list->render();
$page->footer();

?>
