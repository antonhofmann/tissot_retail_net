<?php
/********************************************************************

    logistics.php

    Main entry page after successful login.

    Created by:     Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date created:   2015-01-06
    Modified by:    Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-06
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(has_access("can_edit_catalog") 
  or has_access("can_edit_logistics_system_data"))
{

}
else
{
	check_access("can_edit_catalog");
}

$page = new Page("logistics");
require "include/logistics_page_actions.php";


$page->header();
echo "<p>Welcome to the logistics administration section of " . APPLICATION_NAME . ".</p>";



$page->footer();

?>