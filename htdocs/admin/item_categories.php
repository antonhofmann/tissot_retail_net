<?php
/********************************************************************

    item_categories.php

    Lists item_categorys for editing

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-05-29
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-29
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("item_category.php");


/********************************************************************
    prepare all data needed
*********************************************************************/

/********************************************************************
    item group list
*********************************************************************/

$sql = "select item_category_id, item_category_sortorder, item_category_name, " . 
		"if(item_category_include_in_masterplan, 'yes', ' ') as item_category_include_in_masterplan " .
	   "from item_categories ";


$list = new ListView($sql);

$list->set_entity("item_categorys");
$list->set_order("item_category_sortorder");

$list->add_column("item_category_sortorder", "Sortorder", "", LIST_FILTER_FREE);
$list->add_column("item_category_name", "Name", "item_category.php", LIST_FILTER_FREE);

//$list->add_column("item_category_include_in_masterplan", "Masterplan");

$list->add_button(LIST_BUTTON_NEW, "New", "item_category.php");

$list->process();

$page = new Page("item_categories");

$page->header();
$page->title("Item Categories");
$list->render();
$page->footer();

?>
