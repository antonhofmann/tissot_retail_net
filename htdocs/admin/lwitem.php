<?php
/********************************************************************

    project_offer_position

    Edit Local Constrction Cost Position

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-06
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("lwitems", "lwitem");

$form->add_section();


$form->add_list("lwitem_group", "Cost Group",
    "select lwgroup_id, concat(lwgroup_code, ' ' , lwgroup_text) " .
    "from lwgroups " .
    "order by lwgroup_group, lwgroup_code");

$form->add_edit("lwitem_code", "Code", NOTNULL | UNIQUE);
$form->add_edit("lwitem_text", "Description", NOTNULL);
$form->add_edit("lwitem_action", "Action");
$form->add_edit("lwitem_unit", "Unit");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_DELETE, "Delete");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

$page = new Page("lwitems");
$page->header();
$page->title("Edit Local Construction Cost Group");
$form->render();
$page->footer();

?>