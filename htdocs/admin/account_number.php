<?php
/********************************************************************

    account_number.php

    Creation and mutation of account numbers.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-27
    Version:        1.0.0

    Copyright (c) 2004, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("account_numbers", "account_number");

$form->add_section();
$form->add_edit("account_number_name", "Account Number", NOTNULL | UNIQUE);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("account_numbers");
$page->header();
$page->title(id() ? "Edit Account Number" : "Add Account Number");
$form->render();
$page->footer();

?>