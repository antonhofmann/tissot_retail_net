<?php
/********************************************************************

    item_regions.php

    Lists regions to be assigned to an item.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-09-17
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-09-17
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$list = new ListView("select region_id, region_name " .
                     "from regions", LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("regions");
$list->set_order("region_name");
$list->set_checkbox("item_regions", "items", param("item"));

$list->add_hidden("item", param("item"));
$list->add_column("region_name", "Name", "", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_SAVE, "Save");
$list->add_button(LIST_BUTTON_BACK, "Back");

$list->process();

$page = new Page("items");

$sql = "select item_code from items where item_id = " . param("item");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_row($res);

$page->header();
$page->title("Assign Regions To Item \"" . htmlspecialchars($row[0]) . "\"");
$list->render();
$page->footer();
?>
