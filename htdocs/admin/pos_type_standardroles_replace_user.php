<?php
/********************************************************************

    pos_type_standardroles_replace_user.php

    Repalce Users.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-03-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-03-14
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("pos_type_standardrole.php");


//get all users
$old_users = array();
$sql = "select DISTINCT standardrole_rtco_user, concat(user_name, ' ', user_firstname) as username " . 
       " from standardroles " . 
	   " left join users on user_id = standardrole_rtco_user " .
	   " where user_id > 0 " . 
	   " order by username";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$old_users[$row["standardrole_rtco_user"]] = $row["username"];
}

$sql = "select DISTINCT standardrole_dsup_user, concat(user_name, ' ', user_firstname) as username " . 
       " from standardroles " . 
	   " left join users on user_id = standardrole_dsup_user " . 
	   " where user_id > 0 " . 
	   " order by username";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$old_users[$row["standardrole_dsup_user"]] = $row["username"];
}

$sql = "select DISTINCT standardrole_rtop_user, concat(user_name, ' ', user_firstname) as username " . 
       " from standardroles " . 
	   " left join users on user_id = standardrole_rtop_user " . 
	   " where user_id > 0 " . 
	   " order by username";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$old_users[$row["standardrole_rtop_user"]] = $row["username"];
}

$sql = "select DISTINCT standardrole_cms_approver_user, concat(user_name, ' ', user_firstname) as username " . 
       " from standardroles " . 
	   " left join users on user_id = standardrole_cms_approver_user " . 
	   " where user_id > 0 " . 
	   " order by username";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$old_users[$row["standardrole_cms_approver_user"]] = $row["username"];
}

asort($old_users);


$sql = "";
$new_users = array();
if(param("for_role") == 1)
{
	$sql = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username from user_roles left join users on user_id = user_role_user where (user_role_role = 3 or user_role_role = 10) and user_active = 1 order by username ";
}
elseif(param("for_role") == 2)
{
	$sql = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username from user_roles left join users on user_id = user_role_user where (user_role_role = 8 ) and user_active = 1 order by username ";
}
elseif(param("for_role") == 3)
{
	$sql = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username from user_roles left join users on user_id = user_role_user where (user_role_role = 2 ) and user_active = 1 order by username ";
}
elseif(param("for_role") == 4)
{
	$sql = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username from user_roles left join users on user_id = user_role_user where (user_role_role in (3, 8, 10)) and user_active = 1 order by username ";
}

if($sql)
{
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$new_users[$row["user_id"]] = $row["username"];
	}
}

asort($new_users);

//roles
$roles = array();
$roles[1] = "Project Leader";
$roles[2] = "Design Supervisor";
//$roles[3] = "Logistics Coordinator";
$roles[4] = "CMS Approval by";



//get existing standard roles
$sql = "";
$selected_countries = array();
$selected_pos_types = array();
if(param("old_user") and param("for_role"))
{
	if(param("for_role") == 1)
	{
		$sql = "select DISTINCT standardrole_country, standardrole_postype " . 
			   "from standardroles " . 
			   " where standardrole_rtco_user = " . param("old_user");
	}
	elseif(param("for_role") == 2)
	{
		$sql = "select DISTINCT standardrole_country, standardrole_postype " . 
			   "from standardroles " . 
			   " where standardrole_dsup_user = " . param("old_user");
	}
	elseif(param("for_role") == 3)
	{
		$sql = "select DISTINCT standardrole_country, standardrole_postype " . 
			   "from standardroles " . 
			   " where standardrole_rtop_user = " . param("old_user");
	}
	elseif(param("for_role") == 4)
	{
		$sql = "select DISTINCT standardrole_country, standardrole_postype " . 
			   "from standardroles " . 
			   " where standardrole_cms_approver_user = " . param("old_user");
	}
}
if($sql)
{
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$selected_countries[$row["standardrole_country"]] = $row["standardrole_country"];
		$selected_pos_types[$row["standardrole_postype"]] = $row["standardrole_postype"];
	}
}



$sql_pos_types = "select postype_id, postype_name ".
				 "from postypes " .
				 "where postype_showin_openprojects = 1 " .
				 "order by postype_name";


$sql_country = "select DISTINCT country_id, country_salesregion, country_name, salesregion_name " .
			   "from standardroles " . 
			   "left join countries on country_id = standardrole_country " . 
			   "left join salesregions on salesregion_id = country_salesregion " . 
			   "order by salesregion_name, country_name";



$form = new Form("postype_notifications", "POS Type Notifications");


$form->add_list("old_user", "Replace the following user*", $old_users, SUBMIT | NOTNULL);
$form->add_list("for_role", "in the following role*", $roles, SUBMIT | NOTNULL);
$form->add_list("new_user", "by the following user*", $new_users, NOTNULL);


$form->add_comment("");
$form->add_label("L2", "POS Types", "", "Select the Project Types");
$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
while($row = mysql_fetch_assoc($res))
{
	$form->add_checkbox("Postype_" . $row["postype_id"], $row["postype_name"], 0, 0, "", false);
}

$salesregion = '';
$form->add_comment("");
$form->add_label("L5", "Country", "", "Select the Countries");
$sales_regions = array();
$res = mysql_query($sql_country) or dberror($sql_country);
while($row = mysql_fetch_assoc($res))
{
    if($salesregion != $row["salesregion_name"]) {
		$salesregion =  $row["salesregion_name"];
		$sales_region_id = $row['country_salesregion'];

		$form->add_section($row["salesregion_name"]);

		$form->add_label("select_" . $sales_region_id, "", RENDER_HTML, "<div id='select_" . $sales_region_id ."'><a href='javascript:select_all_" . $sales_region_id. "();'>select all</a></div>");

		$sales_regions[$sales_region_id]['region'] = $salesregion;
		$sales_regions[$sales_region_id]['countries'] = array();
	}
	
	$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], 0, 0, "", false);
	$sales_regions[$sales_region_id]['countries'][] = "CO_" . $row["country_id"];
}



$form->add_button("replace", "Replace User");
$form->add_button("back", "Back");
$form->populate();
$form->process();

if($form->button("back")) {

	redirect("pos_type_standardroles.php");
}
elseif($form->button("old_user") or $form->button("for_role")) {
	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res))
	{	
		if(in_array($row["postype_id"], $selected_pos_types))
		{
			$form->value("Postype_" . $row["postype_id"], 1);
		}
		else
		{
			$form->value("Postype_" . $row["postype_id"], 0);
		}
	}

	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res))
	{
		
		if(in_array($row["country_id"], $selected_countries))
		{
			$form->value("CO_" . $row["country_id"], 1);
		}
		else
		{
			$form->value("CO_" . $row["country_id"], 0);
		}
	}
}

elseif($form->button("replace") and $form->validate()) {
    
	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res)) 
	{
		if($form->value("Postype_" . $row["postype_id"]) == 1) {
			$res2 = mysql_query($sql_country) or dberror($sql_country);
			while($row2 = mysql_fetch_assoc($res2)) 
			{
				if($form->value("CO_" . $row2["country_id"]) == 1) {
					if($form->value("for_role") == 1)
					{
						$sql = "update standardroles SET " .
							   "standardrole_rtco_user = " . $form->value("new_user") . 
							   " where standardrole_postype = " .$row["postype_id"]  .
							   " and standardrole_country = " . $row2["country_id"];

						$result = mysql_query($sql) or dberror($sql);
					}
					elseif($form->value("for_role") == 2)
					{
						
						$sql = "update standardroles SET " .
							   "standardrole_dsup_user = " . $form->value("new_user") . 
							   " where standardrole_postype = " . $row["postype_id"]  .
							   " and standardrole_country = " . $row2["country_id"];

						$result = mysql_query($sql) or dberror($sql);
					}
					elseif($form->value("for_role") == 3)
					{
						
						$sql = "update standardroles SET " .
							   "standardrole_rtop_user = " . $form->value("new_user") . 
							   " where standardrole_postype = " . $row["postype_id"]  .
							   " and standardrole_country = " . $row2["country_id"];

						$result = mysql_query($sql) or dberror($sql);

					}
					elseif($form->value("for_role") == 4)
					{
						$sql = "update standardroles SET " .
							   "standardrole_cms_approver_user = " . $form->value("new_user") . 
							   " where standardrole_postype = " .$row["postype_id"]  .
							   " and standardrole_country = " . $row2["country_id"];

						$result = mysql_query($sql) or dberror($sql);
					}
				}
			}
		}
	}

	redirect("pos_type_standardroles.php");

}

$page = new Page("standardroles");

$page->header();
$page->title("POS Type Standard Roles: Replace User");

$form->render();


?>
<script language='javascript'>
<?php
foreach($sales_regions as $region_id=>$salesregion) {
?>	
	function select_all_<?php echo $region_id;?>()
	{
	
	<?php
	foreach($salesregion['countries'] as $key=>$formfield)
	{
		echo   "document.getElementById('" . $formfield . "').checked = true;" .  "\n";
	}
	?>

	var div = document.getElementById("select_<?php echo $region_id;?>");
    div.innerHTML = "<a href='javascript:deselect_all_<?php echo $region_id;?>();'>deselect all</a>";
	}


	function deselect_all_<?php echo $region_id;?>()
	{
		<?php
		foreach($salesregion['countries'] as $key=>$formfield)
		{
			echo   "document.getElementById('" . $formfield . "').checked = false;" .  "\n";
		}
		?>
		var div = document.getElementById("select_<?php echo$region_id;?>");
		div.innerHTML = "<a href='javascript:select_all_<?php echo $region_id;?>();'>select all</a>";
	}

<?php
}
?>
</script>

<?php

$page->footer();

?>
