<?php
/********************************************************************

    users_inactive.php

    Lists inactive users for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-07-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-07-17
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("user.php");


//list filter arrays
$address_type_filter = array();
$address_type_filter["all"] = "All";
$sql_address_types = "select address_type_id, address_type_name " . 
                     "from address_types " . 
					 " where address_type_id not in (4, 9, 10, 11) " . 
					 "order by address_type_name";
$res = mysql_query($sql_address_types) or dberror($sql_address_types);
while($row = mysql_fetch_assoc($res))
{
	$address_type_filter[$row["address_type_id"]] = $row["address_type_name"];
}


$list_filter = "user_active = 0";


if(param("at") == "all")
{
	unset($_SESSION["address_type_filter"]);
}
elseif(param("at"))
{
	$list_filter .= " and address_type = " . param("at");
	$_SESSION["address_type_filter"] = param("at");
}
elseif(array_key_exists("address_type_filter", $_SESSION) and $_SESSION["address_type_filter"] == "all")
{
	param("at",  $_SESSION["address_type_filter"]);
	unset($_SESSION["address_type_filter"]);
}
elseif(array_key_exists("address_type_filter", $_SESSION))
{
	$list_filter .= " and address_type = " .  $_SESSION["address_type_filter"];
	param("at",  $_SESSION["address_type_filter"]);
}


$list = new ListView("select user_id, user_firstname, user_name, address_company, user_phone, user_email, " .
                      " if(address_type = 7, \"<strong><font color='#FF0000'>!!!</font></strong>\", '') as 3rdpuser " .
                     "from users left join addresses on user_address = address_id");

$list->set_entity("users");
$list->set_filter($list_filter);
$list->set_order("user_name, user_firstname");

$list->add_listfilters("at", "Address Type", 'select', $address_type_filter, param("at"));

$list->add_column("user_firstname", "First Name", "", LIST_FILTER_FREE);
$list->add_column("user_name", "Last Name", "user.php", LIST_FILTER_FREE);
$list->add_column("3rdpuser", "", "", "", "", COLUMN_UNDERSTAND_HTML);
$list->add_column("address_company", "Company", "", LIST_FILTER_LIST,
    "select address_company from addresses order by address_company");
$list->add_column("user_phone", "Phone", "", LIST_FILTER_FREE);
$list->add_column("user_email", "Email", "", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");

$list->process();

$page = new Page("users");

$page->header();
$page->title("Users Inactive");
$list->render();
$page->footer();

?>
