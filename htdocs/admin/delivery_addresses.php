<?php
/********************************************************************

    delivery_addresses.php

    Lists warehouse_addresses for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-02-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-02-11
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("warehouse_address.php");



$sql = "select DISTINCT address_id, " . 
       "concat(country_name, ': ', address_company) as address " . 
	   "from order_addresses " .    
	   "left join addresses on address_id = order_address_parent " .
	   "left join countries on address_country = country_id " . 
	   " where address_active = 1 and order_address_type = 2  and order_address_inactive <> 1";

$res = mysql_query($sql);
while($row = mysql_fetch_assoc($res))
{
	$client_filter[$row["address_id"]] = $row["address"];
}
asort($client_filter);

$sql = "select DISTINCT " . 
       "concat(order_address_company, '@@', order_address_address, '@@', COALESCE(order_address_zip, ''), '@@', COALESCE(order_address_place, ''), '@@', COALESCE(order_address_contact, ''), '@@', order_address_parent) as okey, " . 
	   "address_shortcut, order_address_company, order_address_address, order_address_zip,  " .
	   "order_address_place,country_name, order_address_contact " .
	   "from order_addresses " . 
	   "left join countries on order_address_country = country_id " . 
	   "left join addresses on address_id = order_address_parent";




if(param("address_id") > 0)
{
	$_SESSION["address_id"] = param("address_id");
	$list_filter = " order_address_parent = " . param("address_id") . 
		            " and order_address_type = 2  and order_address_inactive <> 1 ";
}
elseif(array_key_exists("address_id", $_SESSION))
{
	 $list_filter = " order_address_parent = " . $_SESSION["address_id"] . 
		            " and order_address_type = 2  and order_address_inactive <> 1 ";
}
else
{
	$list_filter =  " address_type = 0 and order_address_inactive <> 1 ";
}

$sql_o = $sql . " where " . $list_filter;

$sql_o .= " order by order_address_company, order_address_id";

$res_o = mysql_query($sql_o) or dberror($sql_o);

$inactive_addresses = array();
$addresses = array();
$dates = array();

while($row_o = mysql_fetch_assoc($res_o))
{
	$inactive_addresses[$row_o["okey"]] = 0;
	
	$key = str_replace(" ", "_", $row_o["okey"]);
	$key = "__order_addresses_ai_" . str_replace(".", "_", $key);
	$addresses[$key] = $row_o["okey"];

	$sql_d = "select max(date_created) as lastdate from order_addresses " .
		     " where concat(order_address_company, '@@', order_address_address, '@@', COALESCE(order_address_zip, ''), '@@', COALESCE(order_address_place, ''), '@@', COALESCE(order_address_contact, ''), '@@', order_address_parent) = " . dbquote($row_o["okey"]);

	$res_d = mysql_query($sql_d) or dberror($sql_d);
	if($row_d = mysql_fetch_assoc($res_d)) {
		$dates[$row_o["okey"]] = $row_d["lastdate"];
	}

}

$list = new ListView($sql);

$list->set_entity("order_addresses");
$list->set_filter($list_filter);
$list->set_order("order_address_company, order_address_id");


$list->add_listfilters("address_id", "Client", 'select', $client_filter, param("address_id"));



$tmp = '<input type="checkbox" id="selector">';


//$list->add_column("date", "Created on", "", LIST_FILTER_FREE);
$list->add_text_column("dates", "Dates", 0, $dates);
$list->add_column("address_shortcut", "Shortcut", "", LIST_FILTER_FREE);
$list->add_checkbox_column("ai", $tmp , COLUMN_UNDERSTAND_HTML, $inactive_addresses);
$list->add_column("order_address_company", "Company", "", LIST_FILTER_FREE);
$list->add_column("order_address_contact", "Contact", "", LIST_FILTER_FREE);
$list->add_column("order_address_address", "Street", "", LIST_FILTER_FREE);
$list->add_column("order_address_zip", "Zip", "", LIST_FILTER_FREE);
$list->add_column("order_address_place", "City", "", LIST_FILTER_FREE);
$list->add_column("country_name", "Country", "", LIST_FILTER_LIST, "select country_name from countries order by country_name");
$list->add_hidden("okey", "City", "okey");
$list->add_button("save", "Set Selected to Inactive");

$list->add_hidden("address_id", param("address_id"));

$list->populate();
$list->process();
$i=1;
if ($list->button("save"))
{
	foreach($_POST as $key=>$value)
    {
		
		if($value == 1)
		{
			
			$key = str_replace("__order_addresses_ai_", "", $addresses[$key]);
			//echo $key . "<br />";
			
			$fields = array();
			$fields = explode("@@", $key);	
			
			$filter = "";
			$filter = " where order_address_company = \"" . $fields[0] . "\" ";
			$filter = $filter . " and order_address_address = \"" . $fields[1] . "\" ";
			
			if($fields[2] or $fields[2] == '0')
			{
				$filter = $filter . " and order_address_zip = \"" . $fields[2] . "\" ";
			}
			else
			{
				$filter = $filter . " and (order_address_zip is null or order_address_zip = '') ";	
			}


			if($fields[3])
			{
				$filter = $filter . " and order_address_place = \"" . $fields[3] . "\" ";
			}
			else
			{
				$filter = $filter . " and (order_address_place is null  or order_address_place = '')";	
			}

			if($fields[4])
			{
				$filter = $filter . " and order_address_contact = \"" . $fields[4] . "\" ";
			}
			else
			{
				$filter = $filter . " and (order_address_contact is null  or order_address_contact = '')";	
			}

			

			
			$filter = $filter . " and order_address_parent = \"" . $fields[5] . "\" ";


			//$filter = $filter . " and order_addresses.date_created = \"" . $fields[6] . "\" ";
						
			$sql_u = "Update order_addresses set order_address_inactive = 1 " . $filter;
			//echo $i++ . " " . $sql_u . "<br />";
			$res = mysql_query($sql_u) or dberror($sql_u);
		}
    }

}

$page = new Page("order_addresses1");

$page->header();
$page->title("Delivery Addresses");
$list->render();
?>
<script language="javascript">
	$(document).ready(function() {
		$('#selector').click(function(event) {  //on click
			if(this.checked) { // check select status
				 $("input:checkbox").prop('checked', $(this).prop("checked"));
			}else{
				 $("input:checkbox").prop('checked', "");        
			}
		});
	   
	});

</script>


<?php
$page->footer();

?>
