<?php
/********************************************************************

    standard_distribution_channels.php

    Lists standard distirbution channels for editing

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2012-06-26
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2012-06-26
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_administrate_posindex");
set_referer("standard_distribution_channel.php");


$sql = "select posdistributionchannel_id, posowner_type_name, postype_name, possubclass_name," . 
       "mps_distchannel_group_name, mps_distchannel_code, mps_distchannel_name, " .
	   "IF(posdistributionchannel_assigned_by_system = 1, 'yes', '') as by_system, " .
	   "IF(posdistributionchannel_assignable_by_user = 1, 'yes', '') as by_user, " . 
	   "IF(posdistributionchannel_assign_to_owner_agents = 1, 'yes', '') as aio " . 
       "from posdistributionchannels " . 
       "left join posowner_types on posowner_type_id = posdistributionchannel_legaltype_id " . 
	   "left join postypes on postype_id = posdistributionchannel_postype_id " .
	   "left join possubclasses on possubclass_id = posdistributionchannel_possubclass_id " .
	   "left join mps_distchannels on mps_distchannel_id = posdistributionchannel_dchannel_id " . 
	   "left join mps_distchannel_groups on mps_distchannel_groups.mps_distchannel_group_id = mps_distchannels.mps_distchannel_group_id";

$list = new ListView($sql);

$list->set_entity("posdistributionchannels");
$list->set_order("posowner_type_name, postype_name, mps_distchannel_group_name, mps_distchannel_code, mps_distchannel_name");

$list->add_column("posowner_type_name", "Legal Type", "standard_distribution_channel.php");
$list->add_column("postype_name", "POS Type");
$list->add_column("possubclass_name", "POS Subclass");
$list->add_column("mps_distchannel_group_name", "Group");
$list->add_column("mps_distchannel_code", "Code");
$list->add_column("mps_distchannel_name", "Name");
$list->add_column("by_system", "by default");
$list->add_column("aio", "AiO");
$list->add_column("by_user", "by user");

$list->add_button(LIST_BUTTON_NEW, "New", "standard_distribution_channel.php");

$list->process();

$page = new Page("posdistributionchannels");

$page->header();
$page->title("Standard Distribution Channels");
$list->render();
$page->footer();

?>
