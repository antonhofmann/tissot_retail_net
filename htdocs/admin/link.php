<?php
/********************************************************************

    link.php

    Creation and mutation of link records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-03
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-10-10
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$applications = array();
$applications['retaildevelopment'] = "Retail Development";
$applications['mps'] = "MPS";
$applications['red'] = "RED";
$applications['lps'] = "LPS";
$applications['news'] = "News";


//get all roles
$roles = array();
$sql = "select role_id, role_name from roles order by role_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$roles[] = $row["role_id"];
}


//get access rights
$access_rights = array();
$sql = "select download_roles_role " . 
       " from download_roles " . 
       " where download_roles_download = " . dbquote(param("id"));


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$access_rights[] = $row["download_roles_role"];
}


//get country_restircions
$download_countries = array();
$sql = "select download_countries_country " . 
       " from download_countries " . 
       " where download_countries_download = " . dbquote(param("id"));


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$download_countries[] = $row["download_countries_country"];
}

//get all sub_countries
$sub_countries = array();
$sql = "select distinct address_country from addresses where address_active = 1 and address_client_type = 2 ";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$sub_countries[] = $row["address_country"];
}

//get all agent countries
$agent_countries = array();
$sql = "select distinct address_country from addresses where address_active = 1 and address_client_type = 1 ";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$agent_countries[] = $row["address_country"];
}

//get all countries
$countries = array();
$sql = "select country_id, country_name from countries order by country_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$countries[] = $row["country_id"];
}


$form = new Form("links", "link");

$form->add_section();

$form->add_list("link_category", "Category",
    "select link_category_id, link_category_name from link_categories order by link_category_name",
    NOTNULL, param("category"));


$form->add_list("link_topic", "Topic",
    "select link_topic_id, link_topic_name from link_topics order by link_topic_name",
    NOTNULL, param("topic"));


$form->add_section();
$form->add_edit("link_title", "Title", NOTNULL);
$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("caf", param("caf"));
$form->add_hidden("archive", param("archive"));

$form->add_section();
$form->add_edit("link_url", "URL");
$form->add_upload("link_path", "File", "/files/links");



$form->add_section();
$form->add_multiline("link_description", "Description", 10);

$form->add_checkbox("link_archived", "is archived", false, 0, 'Archive');


$form->add_section("Accessibility for Roles");
$form->add_comment("Please indicate who is allowed to see this message.");

$form->add_label("s0", "Rol Selection", RENDER_HTML, "<div id='s0'><a href='javascript:select_all_roles();'>select all roles</a></div>");



foreach($applications as $key=>$value)
{
	
	$form->add_comment("<strong>" . $value . "</strong>");

	$sql = "select role_id, role_application, role_code, role_name, " .
		   " concat(user_name, ' ', user_firstname) as responsible_admin " . 
		   "from roles " . 
		   " left join users on user_id = role_responsible_user_id " . 
		   " where role_application = " . dbquote($key) . 
		   " order by role_name";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["role_id"], $access_rights))
		{
			$form->add_checkbox("download_roles_" . $row["role_id"], $row["role_name"], true, 0, "");
		}
		else
		{
			$form->add_checkbox("download_roles_" . $row["role_id"], $row["role_name"], false, 0, "");
		}

	}
}

$form->add_comment("<hr /><p>&nbsp;</p>");

$form->add_section("Accessibility for Countries");
$form->add_comment("Please indicate the countries that can see the message.");



$form->add_label("s1", "Country Selection", RENDER_HTML, "<div id='s1'><a href='javascript:select_all_countries();'>select all countries</a></div>");

$form->add_label("s2", "Subsidiaries", RENDER_HTML, "<div id='s2'><a href='javascript:select_all_subs();'>select all subsidiaries</a></div>");

$form->add_label("s3", "Agents", RENDER_HTML, "<div id='s3'><a href='javascript:select_all_agents();'>select all agents</a></div>");



$sql_c = "select country_id, salesregion_name, country_name 
from countries 
left join salesregions on salesregion_id = country_salesregion 
order by salesregion_name, country_name";

$sales_region_name = "";

$res = mysql_query($sql_c) or dberror($sql_c);
while($row = mysql_fetch_assoc($res))
{
	if($sales_region_name != $row["salesregion_name"])
	{
		$sales_region_name = $row["salesregion_name"];
		if(in_array($row["country_id"], $download_countries))
		{
			$form->add_checkbox("download_countries_" . $row["country_id"], $row["country_name"], true, 0, $sales_region_name);
		}
		else
		{
			$form->add_checkbox("download_countries_" . $row["country_id"], $row["country_name"], false, 0, $sales_region_name);
		}
	}
	else
	{
		if(in_array($row["country_id"], $download_countries))
		{
			$form->add_checkbox("download_countries_" . $row["country_id"], $row["country_name"], true, 0, "");
		}
		else
		{
			$form->add_checkbox("download_countries_" . $row["country_id"], $row["country_name"], false, 0, "");
		}
	}
}


//$form->add_checklist("download_countries", "", "download_countries", $sql_c);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button("back", "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->add_validation("is_web_address({link_url})", "The URL is invalid");
$form->add_validation("{link_url} || {link_path}", "Either an URL or a file must be specified");

$form->populate();
$form->process();

if($form->button(FORM_BUTTON_SAVE))
{

	
	if($form->validate())
	{
		$sql = "delete from download_roles where download_roles_download = " . dbquote(param("id"));
		$result = mysql_query($sql) or dberror($sql);

		
		foreach($roles as $key=>$role_id)
		{
			if(array_key_exists("download_roles_" . $role_id, $_POST) and $_POST["download_roles_" . $role_id] == true)
			{

				$sql = "Insert into download_roles (download_roles_role, download_roles_download, user_created, date_created) " . 
					   " values (" . 
					   dbquote($role_id) . ", " .
					   dbquote(param("id")) . ", " . 
					   dbquote(user_login()) . ", " . 
					   dbquote(date("Y-m-d h:s:i")) . ")";

				$result = mysql_query($sql) or dberror($sql);
			}
		
		}


		$sql = "delete from download_countries where download_countries_download = " . id();
		$result = mysql_query($sql) or dberror($sql);

		foreach($countries as $key=>$country_id)
		{
			if(array_key_exists("download_countries_" . $country_id, $_POST) and $_POST["download_countries_" . $country_id] == true)
			{
				$fields = array();
				$values = array();

				$fields[] = "download_countries_country";
				$values[] = $country_id;

				$fields[] = "download_countries_download";
				$values[] = id();

				$fields[] = "date_created";
				$values[] = "now()";

				$fields[] = "date_modified";
				$values[] = "now()";

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());
				
				$sql = "insert into download_countries (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
			}
		}
	}
}
elseif($form->button("back"))
{
	$link = "links.php?ltf=" .  param("ltf") . "&caf=" .  param("caf") . "&archive=" . param("archive");
	redirect($link);
}

$page = new Page("links");
$page->header();
$page->title(id() ? "Edit Link" : "Add Link");
$form->render();



?>


<script language='javascript'>

		
	function select_all_roles()
	{
		<?php
		foreach($roles as $key=>$role_id)
		{
			echo "document.getElementById('download_roles_" . $role_id . "').checked = true;";
		}
		?>
		
		var div = document.getElementById('s0');
		div.innerHTML = "<a href='javascript:deselect_all_roles();'>deselect all roles</a>";
		
		
	}

	function deselect_all_roles()
	{
		
		<?php
		foreach($roles as $key=>$role_id)
		{
			echo "document.getElementById('download_roles_" . $role_id . "').checked = false;";
		}
		?>

		var div = document.getElementById('s0');
		div.innerHTML = "<a href='javascript:select_all_roles();'>select all roles</a>";
	}



	function select_all_countries()
	{
		<?php
		foreach($countries as $key=>$country_id)
		{
			echo "document.getElementById('download_countries_" . $country_id . "').checked = true;";
		}
		?>
		
		var div = document.getElementById('s1');
		div.innerHTML = "<a href='javascript:deselect_all_countries();'>deselect all countries</a>";
		
		
	}

	function deselect_all_countries()
	{
		
		<?php
		foreach($countries as $key=>$country_id)
		{
			echo "document.getElementById('download_countries_" . $country_id . "').checked = false;";
		}
		?>

		var div = document.getElementById('s1');
		div.innerHTML = "<a href='javascript:select_all_countries();'>select all countries</a>";
	}







	function select_all_subs()
	{
		<?php
		foreach($sub_countries as $key=>$country_id)
		{
			echo "document.getElementById('download_countries_" . $country_id . "').checked = true;";
		}
		?>
		
		var div = document.getElementById('s2');
		div.innerHTML = "<a href='javascript:deselect_all_subs();'>deselect all subs</a>";
		
		
	}

	function deselect_all_subs()
	{
		
		<?php
		foreach($sub_countries as $key=>$country_id)
		{
			echo "document.getElementById('download_countries_" . $country_id . "').checked = false;";
		}
		?>

		var div = document.getElementById('s2');
		div.innerHTML = "<a href='javascript:select_all_subs();'>select all subs</a>";
	}



	function select_all_agents()
	{
		<?php
		foreach($agent_countries as $key=>$country_id)
		{
			echo "document.getElementById('download_countries_" . $country_id . "').checked = true;";
		}
		?>
		
		var div = document.getElementById('s3');
		div.innerHTML = "<a href='javascript:deselect_all_agents();'>deselect all agents</a>";
		
		
	}

	function deselect_all_agents()
	{
		
		<?php
		foreach($agent_countries as $key=>$country_id)
		{
			echo "document.getElementById('download_countries_" . $country_id . "').checked = false;";
		}
		?>

		var div = document.getElementById('s3');
		div.innerHTML = "<a href='javascript:select_all_agents();'>select all agents</a>";
	}

	

</script>

<?php

$page->footer();

?>