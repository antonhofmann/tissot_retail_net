<?php
/********************************************************************

    salesregions.php

    Lists salesregions for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-08-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-08-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("salesregion.php");

$list = new ListView("select salesregion_id, salesregion_name from salesregions");

$list->set_entity("salesregions");
$list->set_order("salesregion_name");

$list->add_column("salesregion_name", "Name", "salesregion.php", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_NEW, "New", "salesregion.php");
$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");

$list->process();

$page = new Page("salesregions");

$page->header();
$page->title("Geographical Region");
$list->render();
$page->footer();

?>