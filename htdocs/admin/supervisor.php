<?php
/********************************************************************

    supervisor.php

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2010-11-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2010-11-15
    Version:        1.0.0

    Copyright (c) 2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");



$sql_users = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username " .
			 "from users " .
			 "left join user_roles on user_role_user = user_id " . 
			 "where user_active = 1 " . 
			 " and user_role_role IN (1, 3, 13) " . 
			 "order by user_name, user_firstname";
$form = new Form("supervisingteam", "supervising team member");


if(id()) {
	$form->add_lookup("supervisingteam_user", "User", "users", "concat(user_name, ' ', user_firstname)",HIDEEMPTY ,id());
}
else {
	$form->add_list("supervisingteam_user", "User*", $sql_users, NOTNULL);
}

if(!id()) {
	$form->add_button(FORM_BUTTON_SAVE, "Save");
}
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("supervisingteam");
$page->header();
$page->title(id() ? "Edit Supervising Team Member" : "Add Supervising Team Member");
$form->render();
$page->footer();

?>