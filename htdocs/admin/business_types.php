<?php
/********************************************************************

    business_types.php

    Lists Business types for editing.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-03-28
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-03-28
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("business_type.php");
set_referer("business_type_replace.php");

$sql = "select businesstype_id, businesstype_text ".
       "from businesstypes ";

$list = new ListView($sql);

$list->set_entity("business_types");
$list->set_order("businesstype_text");

$list->add_column("businesstype_text", "Business Types", "business_type.php", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_NEW, "New", "business_type.php");

$list->populate();
$list->process();

$page = new Page("business_types");

$page->header();
$page->title("Business Types");
$list->render();
$page->footer();

?>
