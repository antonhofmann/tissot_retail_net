<?php
/********************************************************************

    address.php

    Creation and mutation of address records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-29
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2015-04-09
    Version:        2.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("user.php");
set_referer("supplier_client_invoice.php");
set_referer("supplier_clients_invoice.php");
set_referer("invoice_address.php");
set_referer("address_file.php");


$address_type = 0;
$client_type = 0;
$address_showinposindex = 0;

$sql = "select * from addresses where address_id = " . id();
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$address_type = $row["address_type"];
	$client_type = $row["address_client_type"];
	$address_showinposindex = $row["address_showinposindex"];
}
elseif(param("address_type") == 1)
{
	$address_type = 1;
	$client_type = param("address_client_type");
}
elseif(param("address_type") == 2)
{
	$address_type = 2;
}


$country_phone_prefix = '';
if(param("address_country"))
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = " . param("address_country") . " order by place_name";

	$country_phone_prefix = get_country_phone_prefix(param("address_country"));
	
}
else
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = {address_country} order by place_name";
}



$address_active = "";
$tracking_data = array();
$address_modified = "";
if(id())
{
	$sql = "select address_added_by, address_last_modified, address_last_modified_by, address_set_to_inactive_by, address_set_to_inactive_date, date_created, address_active " . 
		   "from addresses " . 
		   "where address_id = " . id();

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res)) 
	{
		
		$address_active = $row["address_active"];
		$tracking_data["address_added_by"] = $row["address_added_by"];
		$tracking_data["address_set_to_inactive_by"] = $row["address_set_to_inactive_by"];
		$tracking_data["address_last_modified_by"] = $row["address_last_modified_by"];

		$date_created["address_added_by"] = $row["date_created"];
		$date_created["address_set_to_inactive_by"] = $row["address_set_to_inactive_date"];
		$date_created["address_last_modified_by"] = $row["address_last_modified"];

		foreach($tracking_data as $key=>$user_id)
		{
			$sql = "select user_name, user_firstname " . 
				   "from users " . 
					"where user_id = " . dbquote($user_id);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) 
			{
				$tracking_data[$key] = $row["user_name"] . " " . $row["user_firstname"] . " " . $date_created[$key];
			}
		}
	}
}


// Build form

$form = new Form("addresses", "address");

if($address_showinposindex == 1)
{
	$form->add_comment('<a href="/mps/companies/address/' . param("id") . '" target="_blank">Access this Address in MPS</a>');
}

$form->add_hidden("af", param("af"));

$form->add_section("Name and address");
$form->add_list("address_type", "Address Type",
    "select address_type_id, address_type_name from address_types  where address_type_id IN (1, 2, 3, 5, 8, 12, 13) order by address_type_name", SUBMIT | NOTNULL);

$form->add_edit("address_number", "Tissot SAP Customer Code Merchandising");
$form->add_edit("address_number2", "Tissot SAP Customer Code Retail Furniture");

if($client_type == 2 or $client_type == 3)
{
	$form->add_edit("address_legnr", "SG Legal Number");
}
else
{
	$form->add_hidden("address_legnr");
}

if($address_type == 1)
{
	$form->add_edit("address_sapnr", "SAP Customer Code");
	$form->add_edit("address_sap_salesorganization", "SAP Sales Organization");
	//$form->add_hidden("address_shortcut");
	$form->add_edit("address_shortcut", "Shortcut", NOTNULL);
}
else
{
	$form->add_edit("address_shortcut", "Shortcut", NOTNULL);
}

$form->add_edit("address_company", "Company", NOTNULL);
$form->add_edit("address_company2", "");


$form->add_hidden("address_address");
$form->add_multi_edit("street", array("address_street", "address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6), array(), 0, '', '', array(40, 5));



$form->add_edit("address_address2", "Additional Address Info");


$form->add_list("address_country", "Country*",
    "select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);

$form->add_edit("address_zip", "Zip");
$form->add_list("address_place_id", "City*", $sql_places, NOTNULL | SUBMIT);
$form->add_edit("address_place", "", DISABLED);

$form->add_section("Communication");
$form->add_hidden("address_phone");

$form->add_multi_edit("phone_number", array("address_phone_country", "address_phone_area", "address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($country_phone_prefix, '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

$form->add_hidden("address_mobile_phone");
$form->add_multi_edit("mobile_phone_number", array("address_mobile_phone_country", "address_mobile_phone_area", "address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($country_phone_prefix, '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


$form->add_edit("address_email", "Email");
$form->add_edit("address_contact_name", "Contact Name");
$form->add_edit("address_website", "Website");

if($address_type == 1) // client
{
	$form->add_section("Logistics");
	$form->add_edit("address_vat_id", "VAT ID");
	$form->add_edit("address_financecontroller_name", "Name of finance controller");
	
	$form->add_hidden("address_financecontroller_phone");

	$form->add_multi_edit("financecontroller_phone_number", array("address_financecontroller_phone_country", "address_financecontroller_phone_area", "address_financecontroller_phone_number"), "Finance controller's phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($country_phone_prefix, '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


	$form->add_edit("address_financecontroller_email", "Email of finance controller");
	$form->add_edit("address_logistics_fedex_dhl", "FEDEX/DHL");
	$form->add_checkbox("address_logistics_coo", "to ship original COO and invoice to client",0, 0, "Original COO and Invoice");
	$form->add_multiline("address_logistics_notes", "Notes", 2);
	$form->add_multiline("address_logistics_comments", "Comments", 6);
}
else
{
	$form->add_hidden("address_vat_id");
	$form->add_hidden("address_financecontroller_name");
	$form->add_hidden("address_financecontroller_phone");
	$form->add_hidden("address_financecontroller_phone_country");
	$form->add_hidden("address_financecontroller_phone_area");
	$form->add_hidden("address_financecontroller_phone_number");
	$form->add_hidden("address_financecontroller_email");
	$form->add_hidden("address_logistics_fedex_dhl");
	$form->add_hidden("address_logistics_coo");
	$form->add_hidden("address_logistics_notes");
	$form->add_hidden("address_logistics_comments");
}



if($address_type == 1) // client
{
	$sql_invoice_addresses = "select address_id, concat(country_name, ': ', address_company) as company from addresses " .
                 "left join countries on country_id = address_country " .
				 "where (address_active = 1 and address_type = 1) or  {address_invoice_recipient}=address_id order by country_name, address_company";


	$form->add_section("Different Billing Address");
	$form->add_comment("Please indicate the corresponding billing address in case invoices are not sent to the client.");
	$form->add_list("address_invoice_recipient", "Company",$sql_invoice_addresses);
}
else
{
	$form->add_hidden("address_invoice_recipient");
}



$form->add_section("Other information");

$form->add_list("address_currency", "Currency",
				"select currency_id, currency_symbol from currencies order by currency_symbol",  NOTNULL);

if($address_type == 2) // supplier
{
	$form->add_list("address_currency2", "Currency 2",
			"select currency_id, currency_symbol from currencies order by currency_symbol");
}
else
{
	$form->add_hidden("address_currency2");
}
	

if($address_type == 1) // client
{
	$form->add_list("address_client_type", "Client Type",
					"select client_type_id, client_type_code from client_types order by client_type_code");
}
else
{
	$form->add_hidden("address_client_type");
}

$form->add_list("address_contact", "Contact",
    "select user_id, concat(user_name, ' ', user_firstname) from users " .
    "where user_active = 1 and user_address = " . id() . " order by user_name, user_firstname", OPTIONAL);

$form->add_section();
//$form->add_checkbox("address_canbefranchisor", "Can Be Franchisor", false);
$form->add_edit("address_company_is_partner_since", "Company is partner since", 0, "", TYPE_DATE);
$form->add_checkbox("address_canbefranchisee", "Can own POS locations", false);
$form->add_checkbox("address_showinposindex", "Show Address in POS Index", false);


$form->add_comment("Please indicate the corresponding client address in case this address is a POS owner address.");

$sql_addresses = "select address_id, concat(country_name, ': ', address_company) as company from addresses " .
                 "left join countries on country_id = address_country " .
				 "where address_type IN (1, 9)  order by country_name, address_company";

$form->add_list("address_parent", "Parent*",$sql_addresses);


$form->add_comment("Assign another address in case the legal entity for this address differs in Totara.");

$sql_addresses2 = "select address_id, concat(country_name, ': ', address_company) as company " . 
                 " from addresses " .
                 "left join countries on country_id = address_country " .
				 "where address_client_type in (2, 3) and address_type IN (1, 9)  order by country_name, address_company";

$form->add_list("address_totara_parent", "Legal Entity for Totara",$sql_addresses2);


$form->add_section();
$form->add_checkbox("address_active", "Address in Use", true);
$form->add_checkbox("address_checked", "Address checked", true);


if($address_type == 13 or ($address_type == 1 and $client_type > 0)) // client
{
	$form->add_section('Merchandising Planning');
	$form->add_checkbox("address_involved_in_planning", "can order Tissot merchandising material", false, 0, 'Merchandising');


	$form->add_checkbox("address_can_own_independent_retailers", "can own independent retailers", false, 0, 'Retailers');
	$form->add_checkbox("address_is_internal_address", "Address is an internal Address for MPS", false, 0, 'Internal Address');
	$form->add_checkbox("address_is_a_hq_address", "Address is a HQ address", false, 0, 'HQ Address');


	$form->add_label("address_mps_customernumber", "Customer Number");
	$form->add_label("address_mps_shipto", "Ship To Number");


	$form->add_section('Launch Plan');
	$form->add_checkbox("address_involved_in_lps", "can order Tissot LPS material", false, 0, 'Launch Plan');


}

$form->add_section('Retail Environment Development');
$form->add_checkbox("address_involved_in_red", "can be involved in retail environment development", false, 0, 'RED');


if($address_type == 2) // supplier
{
	$form->add_section('Default Supplying Options');
	$form->add_checkbox("address_can_be_requested_for_quote", "can get a request for offer without having items in the list of materials", false, 0, 'Request for offer');

	
	$form->add_checkbox("address_only_quantity_proposal", "Offers do only need a quantity proposal");


	$sql = "select project_cost_groupname_id, project_cost_groupname_name " . 
			"from project_cost_groupnames " . 
			"where project_cost_groupname_active = 1 and project_cost_groupname_id in (2, 10) order by project_cost_groupname_id";

	$form->add_list("address_cost_group", "Cost Group",$sql, 0);

	$sql = "select costmonitoringgroup_id, costmonitoringgroup_text from costmonitoringgroups order by costmonitoringgroup_text";
	$form->add_list("address_costmonitoring_group", "Type",$sql, 0);

	


}
else
{
	$form->add_hidden("address_can_be_requested_for_quote", 0);
	$form->add_hidden("address_only_quantity_proposal", 0);
	$form->add_hidden("address_cost_group", 0);
	$form->add_hidden("address_costmonitoring_group", 0);
}


if(!array_key_exists("address_added_by", $tracking_data))
{
	$form->add_hidden("address_added_by", user_id());
}
elseif(id() > 0) {
	$form->add_hidden("address_last_modified_by", user_id());
	$form->add_hidden("address_last_modified", date("Y-m-d H:i:s"));
}

if(array_key_exists("address_added_by", $tracking_data) and $tracking_data["address_added_by"] != "")
{
	$form->add_section("Tracking infos");
	$form->add_label("added_by", "Address created by", 0, $tracking_data["address_added_by"]);
	$form->add_label("modified_by", "Last modifed by", 0, $tracking_data["address_last_modified_by"]);

	if($address_active == 0 and array_key_exists("address_set_to_inactive_by", $tracking_data) and $tracking_data["address_set_to_inactive_by"] != "")
	{
		$form->add_label("set_to_inactive", "Address set to inactive by", 0, $tracking_data["address_set_to_inactive_by"]);
	}
}

$form->add_button("save", "Save");
$form->add_button("back", "Back");


//$form->add_validation("is_shortcut({address_shortcut})", "The shortcut is invalid. It may only contain lower case characters a-z, digits and the underscore");
//$form->add_validation("is_email_address({address_email})", "The email address is invalid.");
//$form->add_validation("is_web_address({address_website})", "The website url is invalid.");

// Populate form and process button clicks

$form->populate();

if($form->value("address_canbefranchisee") == 1)
{
	$form->add_validation("{address_parent}", "You must indicate a parent address for a POS owner address.");
}
$form->process();

if($form->button("address_type"))
{
	if($form->value("address_type") == 1)
	{
		$form->value("address_canbefranchisee", 1);
		$form->value("address_showinposindex", 1);
	}
	elseif($form->value("address_type") == 12) //RED external partner
	{
		$form->value("address_involved_in_red", 1);
	}
}
elseif($form->button("address_place_id"))
{
	$sql = "select place_name from places where place_id = " . dbquote($form->value("address_place_id"));
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value("address_place", $row["place_name"]);
	}

}
elseif($form->button("address_country"))
{
	$form->value("address_place", "");
	$form->value("address_place_id", "");
	$form->value("address_zip", "");
}
elseif($form->button("back"))
{
	redirect("addresses.php?af=" . param("af"));
}

if($form->button("save"))
{
	
	/*
	if($form->value("address_type") == 1
		and $form->value("address_currency") != 1)
	{
		$form->error("The currency of a client address must be CHF!");
	}
	*/

	$form->value("address_phone", $form->unify_multi_edit_field($form->items["phone_number"]));
	$form->value("address_mobile_phone", $form->unify_multi_edit_field($form->items["mobile_phone_number"]));

	$form->add_validation("{address_phone} != '' or {address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");


	if($form->validate())
	{
		
		
		
		
		if($form->value("address_type") == 1) // client
		{
			$form->value("address_financecontroller_phone", $form->unify_multi_edit_field($form->items["financecontroller_phone_number"]));
		}

		$form->value("address_address", $form->unify_multi_edit_field($form->items["street"], get_country_street_number_rule($form->value("address_country"))));


		$form->save();

	
		//set all users to inactive
		if(!$form->value("address_active"))
		{
			$sql = "update users set user_active = 0 where user_address = " . id();
			$result = mysql_query($sql) or dberror($sql);
		}

		//update franchisee address of all ongoing projects

		$sql = "select * from addresses where address_id = " . dbquote(id());
		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
		
			$fields = array();
			
			$value = dbquote($row["address_company"]);
			$fields[] = "order_franchisee_address_company = " . $value;

			$value = dbquote($row["address_company2"]);
			$fields[] = "order_franchisee_address_company2 = " . $value;

			$value = dbquote($row["address_address"]);
			$fields[] = "order_franchisee_address_address = " . $value;

			$value = dbquote($row["address_street"]);
			$fields[] = "order_franchisee_address_street = " . $value;

			$value = dbquote($row["address_streetnumber"]);
			$fields[] = "order_franchisee_address_streetnumber = " . $value;

			$value = dbquote($row["address_address2"]);
			$fields[] = "order_franchisee_address_address2 = " . $value;

			$value = dbquote($row["address_zip"]);
			$fields[] = "order_franchisee_address_zip = " . $value;

			$value = dbquote($row["address_place"]);
			$fields[] = "order_franchisee_address_place = " . $value;

			$value = dbquote($row["address_country"]);
			$fields[] = "order_franchisee_address_country = " . $value;

			$value = dbquote($row["address_phone"]);
			$fields[] = "order_franchisee_address_phone = " . $value;

			$value = dbquote($row["address_phone_country"]);
			$fields[] = "order_franchisee_address_phone_country = " . $value;

			$value = dbquote($row["address_phone_area"]);
			$fields[] = "order_franchisee_address_phone_area = " . $value;

			$value = dbquote($row["address_phone_number"]);
			$fields[] = "order_franchisee_address_phone_number = " . $value;

			$value = dbquote($row["address_mobile_phone"]);
			$fields[] = "order_franchisee_address_mobile_phone = " . $value;

			$value = dbquote($row["address_mobile_phone_country"]);
			$fields[] = "order_franchisee_address_mobile_phone_country = " . $value;

			$value = dbquote($row["address_mobile_phone_area"]);
			$fields[] = "order_franchisee_address_mobile_phone_area = " . $value;

			$value = dbquote($row["address_mobile_phone_number"]);
			$fields[] = "order_franchisee_address_mobile_phone_number = " . $value;

			$value = dbquote($row["address_email"]);
			$fields[] = "order_franchisee_address_email = " . $value;

			$value = dbquote($row["address_contact_name"]);
			$fields[] = "order_franchisee_address_contact = " . $value;

			$value = dbquote($row["address_website"]);
			$fields[] = "order_franchisee_address_website = " . $value;


			$sql = "update orders set " . join(", ", $fields) . " where order_type = 1 and (order_archive_date is null or order_archive_date = '0000-00-00') and order_franchisee_address_id = " . dbquote(id());

			mysql_query($sql) or dberror($sql);
		}


		if($form->value('address_active') != 1 and $address_active == 1)
		{
			$sql = 'update addresses set address_set_to_inactive_by  = ' . user_id() . ', address_set_to_inactive_date = ' . dbquote(date("Y-m-d H:i:s")) . ' where address_id = ' . id();
			mysql_query($sql) or dberror($sql);
		}
	}
	
}




/*List of active users*/

if(id() == 13) {

	/*List of active users including other departments*/
	$sql = "select user_id, user_firstname, user_name, address_company, user_phone, user_mobile_phone, " . 
		   "user_email, user_email_cc, user_email_deputy, address_company " .
		  
		   "from users " . 
		   "left join addresses on user_address = address_id";

	$ulist_filter = " user_active = 1 
						and (address_is_a_hq_address = 1 
						or address_is_internal_address = 1  
						or user_address = 13)";


	$sql_r = "select user_id, role_name " . 
         " from users " . 
		 " left join addresses on user_address = address_id " .
		 " left join user_roles on user_role_user = user_id " . 
		 " left join roles on role_id = user_role_role " . 
		 " where user_active = 1 
						and (address_is_a_hq_address = 1 
						or address_is_internal_address = 1  
						or user_address = 13) " .  
		 " order by role_name";
}
else {
	$sql = "select user_id, user_firstname, user_name, address_company, user_phone, user_mobile_phone, " . 
		   "user_email, user_email_cc, user_email_deputy, " .
		   " if(address_type = 7, \"<strong><font color='#FF0000'>!!!</font></strong>\", '') as 3rdpuser " .
		   "from users " . 
		   "left join addresses on user_address = address_id";

	$ulist_filter = "user_active = 1 and user_address = '" . id() . "'";


	$sql_r = "select user_id, role_name " . 
         " from users " . 
		 " left join user_roles on user_role_user = user_id " . 
		 " left join roles on role_id = user_role_role " . 
		 " where user_active = 1 and user_address = '" . id() . "' " . 
		 " order by role_name";
}




//get user roles
$roles = array();
$res = mysql_query($sql_r) or dberror($sql_r);
while($row = mysql_fetch_assoc($res))
{
	if(array_key_exists($row['user_id'], $roles))
	{
		$roles[$row['user_id']]	= $roles[$row['user_id']] . '<br />' . $row['role_name'];
	}
	else
	{
		$roles[$row['user_id']]	= $row['role_name'];
	}
}


$ulist = new ListView($sql);

$ulist->set_entity("users");
$ulist->set_title("Active Users");
$ulist->set_filter($ulist_filter);
$ulist->set_order("user_name, user_firstname");

if(id() == 13) {
	$ulist->set_group("address_company");
}

$ulist->add_column("user_firstname", "First Name", "", LIST_FILTER_FREE);
$ulist->add_column("user_name", "Last Name", "user.php", LIST_FILTER_FREE);
$ulist->add_text_column("roles", "Roles", COLUMN_UNDERSTAND_HTML, $roles);
$ulist->add_column("user_phone", "Phone", "", LIST_FILTER_FREE);
$ulist->add_column("user_mobile_phone", "Mobile Phone", "", LIST_FILTER_FREE);
$ulist->add_column("user_email", "Email", "", LIST_FILTER_FREE);
$ulist->add_column("user_email_cc", "CC", "", LIST_FILTER_FREE);
$ulist->add_column("user_email_deputy", "Deputy", "", LIST_FILTER_FREE);

$form->add_button("add_user", "Add User", "");
$ulist->populate();
$ulist->process();



//invoice customer relations (only for suppliers)
$form2 = new Form("addresses", "address");

$form2->add_section("Supplier sends Invoice directly to the Following Clients");
$form2->add_subtable("supplier_client_invoices", "", "supplier_client_invoice.php?address=" . id(), "Company",
    "select supplier_client_invoice_id, address_company as Company, country_name as Country, " .
	"supplier_client_invoice_startdate as FromDate, supplier_client_invoice_enddate as ToDate " . 
    "from supplier_client_invoices " .
	"left join addresses on address_id = supplier_client_invoice_client " . 
	"left join countries on country_id = address_country " . 
	"where supplier_client_invoice_supplier = " . id() . 
    " order by address_company");

$form2->add_button("add_client", "Add/Edit Clients", "supplier_clients_invoice.php?address=" . id(), OPTIONAL);

$form2->populate();
$form2->process();






//invoice addresses of the customer
$form3 = new Form("addresses", "address");

$form3->add_section("Invoice Addresses");
$form3->add_comment("Only add new invoice addresses in case an invoice can also be sent to a different address than the client's address. Client's address is always the dafult invoice address.");

$form3->add_subtable("invoice_addresses", "", "invoice_address.php?address=" . id(), "Company",
    "select invoice_address_id, invoice_address_company as Company, place_name as City, country_name as Country " .
    "from invoice_addresses " .
	"left join places on place_id = invoice_address_place_id " . 
	"left join countries on country_id = invoice_address_country_id " . 
	"where invoice_address_address_id = " . id() . 
    " order by invoice_address_company");

$form3->add_button("add_invoiceaddress", "Add Invoice Address", "invoice_address.php?address=" . id(), OPTIONAL);

$form3->populate();
$form3->process();



/********************************************************************
    Create List of files
*********************************************************************/ 

$sql_files = "select addressfile_id, addressfile_title, addressfile_path, " .
             " concat(DAY(date_created), '.', MONTH(date_created), '.', YEAR (date_created)) as date " . 
			 "from addressfiles ";
$list_filter = " addressfile_address = " . id();

//get all files
$files = array();
$edit_links = array();
$sql = $sql_files . " where " . $list_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$link = "http://" . $_SERVER["HTTP_HOST"] . "/" . $row["addressfile_path"];
	$link = "<a href=\"javascript:popup('" . $link . "',800,600);\"><img src=\"/pictures/view.gif\" border='0'/></a>";
		
	$files[$row["addressfile_id"]] = $link;

	$edit_links[$row["addressfile_id"]] = '<a href="address_file.php?id=' . $row['addressfile_id'] . '&address=' . id() . '">' . $row["addressfile_title"] . '</a>';
}


$list = new ListView($sql_files);
$list->set_title("Files");
$list->set_entity("addressfiles");
$list->set_filter($list_filter);
$list->set_order("addressfile_title");

$list->add_text_column("files", "", COLUMN_UNDERSTAND_HTML, $files);
$list->add_text_column("title", "Title", COLUMN_UNDERSTAND_HTML, $edit_links);

$list->add_column("date", "Date", "", "", "", COLUMN_NO_WRAP);

$list->add_button("newfile", "Add File", "");

$list->populate();
//$list->process();





if($list->button("newfile"))
{
	$link = "address_file.php?address=" . id() . "&country=" . param("country");
	redirect($link);
}

if($ulist->button("user_name"))
{
	$link = "user.php?address=" . id() . "&country=" . param("country");
	redirect($link);
}


if($form->button("add_user"))
{
	$link = "user.php?address=" . id() . "&country=" . param("country");
	redirect($link);
}



// Render page

$page = new Page("addresses");



$page->header();
$page->title(id() ? "Edit Retail Net Address" : "Add Retail Net Address");
$form->render();


echo "<p>&nbsp;</p>";
$ulist->render();

if($address_type == 2) // supplier
{
	echo "<p>&nbsp;</p>";
	$form2->render();
}
elseif($address_type == 1) // client
{
	echo "<p>&nbsp;</p>";
	$form3->render();
}

echo "<p>&nbsp;</p>";
$list->render();

$page->footer();

?>