<?php
/********************************************************************

    inactive_query_owners.php

    Lists inactive users still owning queries.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2013-12-28
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2013-12-28
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("inactive_query_owner.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
$users = array();
$mis = array();
$pos = array();

$sql = "select user_id " .
	   "from mis_queries " . 
	   "inner join users on user_id = mis_query_owner " . 
	   "where user_active = 0";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$users[$row["user_id"]] = $row["user_id"];
	$mis[$row["user_id"]] = array_key_exists($row["user_id"], $mis) ? $mis[$row["user_id"]] + 1 : 1;
}

$sql = "select user_id " .
	   "from posqueries " . 
	   "inner join users on user_id = posquery_owner " . 
	   "where user_active = 0";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$users[$row["user_id"]] = $row["user_id"];
	$pos[$row["user_id"]] = array_key_exists($row["user_id"], $pos) ? $pos[$row["user_id"]] + 1 : 1;
}


$sql = "select user_id, concat(user_name, ' ', user_firstname) as username, " .
		   "address_company " . 
           "from users " . 
		   "inner join addresses on address_id = user_address";

if(count($users) > 0)
{
	$list_filter = "user_id in(" . implode(',', $users) . ")";
}
else
{
	$list_filter = "user_id = 'x'";
}

/********************************************************************
    Create List of MIS Queries
*********************************************************************/ 

$list = new ListView($sql);

$list->set_entity("users");
$list->set_order("address_company, username");
$list->set_filter($list_filter);
$list->add_column("address_company", "Company");
$list->add_column("username", "User", "inactive_query_owner.php");
$list->add_text_column("mis", "MIS", COLUMN_ALIGN_RIGHT, $mis);
$list->add_text_column("pos", "POS Index", COLUMN_ALIGN_RIGHT, $pos);

/********************************************************************
    Populate and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

/********************************************************************
   Create Page
*********************************************************************/ 
$page = new Page("people");

$page->header();
$page->title("Inactive Users Owning Queries");
$list->render();
$page->footer();

?>
