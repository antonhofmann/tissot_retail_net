<?php
/********************************************************************

    country_selector.php

    Enter Country List

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-09-29
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-09-29
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

check_access("can_edit_catalog");


/********************************************************************
    prepare all data needed
*********************************************************************/
if(param("context") == 'user') {
	$user_id = param("user_id");
	$context = param("context");

	
	$sql_country = "select DISTINCT country_id, country_name " .
				   "from countries " .
			       "order by country_name";

	$country_list = array();
	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res)) 
	{
		$country_list[] = $row["country_id"];
	}

	$countries = array();
	$sql = "select country_access_country " . 
		   "from country_access " . 
		   "where country_access_user = " . $user_id;
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res)) 
	{
		$countries[] = $row["country_access_country"];
	}
	$address = param("address");
}
else
{
	exit;
}


/********************************************************************
    save data
*********************************************************************/

if(param("save_form") == "user")
{
	$selected_countries = array();

	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res))
	{
		if($_POST["CO_" . $row["country_id"]])
		{
			$selected_countries[] = $row["country_id"];
		}
	}
	
	$sql = "delete from country_access " . 
		   "where country_access_user = " . $user_id;

	$result = mysql_query($sql) or dberror($sql);

	foreach($selected_countries as $key=>$country_id) 
	{
		$sql = "insert into country_access (" . 
			   "country_access_user, country_access_country, user_created, date_created ) VALUES (" . 
			   $user_id . ", " . $country_id . ", '" . user_login() . "', '" . date("Y-m-d:H:i:s") . "')";

		$result = mysql_query($sql) or dberror($sql);
	
	}
}



/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("users", "country_selector");
$form->add_hidden("user_id", param("user_id"));
$form->add_hidden("address", param("address"));
$form->add_hidden("context", $context);

$page_title = "Country Selector";
$form->add_hidden("save_form", $context);

$form->add_label("select0", "Countries", RENDER_HTML, "<div id='select0'><a href='javascript:select_all_countries();'>select all countries</a></div>");

$res = mysql_query($sql_country) or dberror($sql_country);
while($row = mysql_fetch_assoc($res))
{
	if(in_array($row["country_id"], $countries))
	{
		$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], true);
	}
	else
	{
		$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], false);
	}
	
}

$form->add_input_submit("submit", "Save Values", 0);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("users");

$page->header();
$page->title($page_title);

$form->render();


if(param("save_form"))
{
?>

<script languege="javascript">
var back_link = "user.php?id=<?php echo param('user_id');?>&address=<?php echo param('address');?>"; 
$.nyroModalRemove();
</script>

<?php
}
?>

<script language='javascript'>

	function select_all_countries()
	{
		<?php
		foreach($country_list as $key=>$country_id)
		{

			//echo "document.getElementById('CO_" . $country_id . "').checked = true;";

			echo "$('input[name=CO_" . $country_id . "]').attr('checked', true);";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:deselect_all_countries();'>deselect all countries</a>";
		
		
	}

	function deselect_all_countries()
	{
		<?php
		foreach($country_list as $key=>$country_id)
		{
			echo "$('input[name=CO_" . $country_id . "]').attr('checked', false);";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:select_all_countries();'>select all countries</a>";
	}

</script>



<?php

$page->footer();
