<?php
/********************************************************************

    logistics_items_xls.php

    Generate Excel-File items

    Created by:     Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date created:   2015-01-06
    Modified by:    Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-06
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(has_access("can_edit_catalog") 
  or has_access("can_edit_logistics_system_data"))
{

}
else
{
	check_access("can_edit_catalog");
}

require_once "../include/xls/Writer.php";

/********************************************************************
    prepare Data Needed
*********************************************************************/


$header = "";
$header = "Item List: (" . date("d.m.Y G:i") . ")";

/********************************************************************
    prepare Data
*********************************************************************/

$sql = "select DISTINCT item_id, item_code, item_name, item_price, " .
       "unit_name, item_width, item_height, item_length, item_radius, item_gross_weight, " . 
	   "item_net_weight, unit_name, packaging_type_name, " .
	   " item_width*item_height*item_length/1000000 as cbm, " . 
		 " address_company, item_category_name, " .
		 " if(item_stackable = 1, 'yes', '') as stackable " .
		 "from items " .
		 "left join suppliers on supplier_item = item_id " .
		 "left join addresses on address_id = supplier_address ". 
		 "left join units on unit_id = item_unit " .
		 "left join packaging_types on packaging_type_id = item_packaging_type " .
		 "left join item_categories on item_category_id = item_category";

$filter = " item_active = 1 and item_type = 1";

if(param("su")) {
	$filter .= " and supplier_address = " . param("su");
}

if(param("ca")) {
	
	$filter .= " and item_category = " . param("ca");
}

$sql = $sql . " where " . $filter . " order by item_category_name, item_code";


/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "item_list_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename);

$xls->setVersion(8);

$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_normal_bold =& $xls->addFormat();
$f_normal_bold->setSize(8);
$f_normal_bold->setAlign('left');
$f_normal_bold->setBorder(1);
$f_normal_bold->setBold();


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_decimal =& $xls->addFormat();
$f_decimal->setSize(8);
$f_decimal->setAlign('right');
$f_decimal->setBorder(1);
$f_decimal->setNumFormat('0.00');


$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('yellow');



//captions
$captions = array();
//$captions[] = "Nr";
//$captions[] = "Geographical Region";
$captions[] = "Category";
$captions[] = "Code";
$captions[] = "Name";
$captions[] = "Supplier";
$captions[] = "Unit";
$captions[] = "Width in cm";
$captions[] = "Height in cm";
$captions[] = "Length in cm";
$captions[] = "CBM";
$captions[] = "Radius";
$captions[] = "Gross Weight in kg";
$captions[] = "Net Weight in kg";
$captions[] = "Packaging";
$captions[] = "Stackable";

$col_widths = array();
$col_widths[] = 30;
$col_widths[] = 20;
$col_widths[] = 50;
$col_widths[] = 12;
$col_widths[] = 12;
$col_widths[] = 12;
$col_widths[] = 12;
$col_widths[] = 12;
$col_widths[] = 12;
$col_widths[] = 12;
$col_widths[] = 15;
$col_widths[] = 15;
$col_widths[] = 15;
$col_widths[] = 8;

/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);


$sheet->writeRow(2, 0, $captions, $f_normal_bold);


$row_index = 3;
$cell_index = 0;
$counter = 0;





$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{

	
	$sheet->write($row_index, $cell_index, $row["item_category_name"], $f_normal);
	$cell_index++;


	$sheet->write($row_index, $cell_index, $row["item_code"], $f_normal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["item_name"], $f_normal);
	$cell_index++;


	$sheet->write($row_index, $cell_index, $row["address_company"], $f_normal);
	$cell_index++;


	$sheet->write($row_index, $cell_index, $row["unit_name"], $f_normal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["item_width"], $f_decimal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["item_height"], $f_decimal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["item_length"], $f_decimal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["cbm"], $f_decimal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["item_radius"], $f_decimal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["item_gross_weight"], $f_decimal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["item_net_weight"], $f_decimal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["packaging_type_name"], $f_normal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["stackable"], $f_normal);
	$cell_index++;

	
	
	
	$cell_index = 0;
	$row_index++;
	
}

for($i=0;$i<count($captions);$i++)
{
	$sheet->setColumn($i, $i, $col_widths[$i]);
}


$xls->close(); 

?>