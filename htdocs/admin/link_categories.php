<?php
/********************************************************************

    link_categories.php

    Lists link categories for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-03
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-08-19
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("link_category.php");

$list = new ListView("select link_category_id, link_category_name, link_category_priority " .
                     "from link_categories");

$list->set_entity("link categories");
$list->set_order("link_category_name");

$list->add_column("link_category_name", "Name", "link_category.php", LIST_FILTER_FREE);
$list->add_column("link_category_priority", "Priority", "", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_NEW, "New", "link_category.php");
$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");

$list->process();

$page = new Page("link_categories");

$page->header();
$page->title("Download/Link Categories");
$list->render();
$page->footer();

?>
