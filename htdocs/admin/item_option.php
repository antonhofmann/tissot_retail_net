<?php
/********************************************************************

    item_option.php

    Creation and mutation of item addon records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-02-10

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

set_referer("item_option_file.php");

$form = new Form("item_options", "option");


$form->add_hidden("item", param("item"));


$form->add_section();
$form->add_lookup("item_option_parent", "Item", "items", "concat(item_code, '\n', item_name)", 0, param("item"));

$form->add_section();

$form->add_edit("item_option_name", "Option Name", NOTNULL);

$form->add_list("item_option_child", "Item",
    "select item_id, item_code " .
    "from items " .
    "order by item_code");

$form->add_multiline("item_option_description", "Description", 4);

$form->add_section();
$form->add_edit("item_option_quantity", "Quantity", NOTNULL);

$form->add_edit("item_option_configcode", "Picture Code");

//files
$form->add_subtable("files", "<br />Files", "item_option_file.php", "Title",
    "select item_option_file_id, item_option_file_title as Title, file_purpose_name as Purpose, " .
    "    file_type_name as Type, item_option_file_path as Path " .
    "from item_option_files left join file_types on item_option_file_type = file_type_id " .
    "    left join file_purposes on item_option_file_purpose = file_purpose_id " .
    "where item_option_file_option = " . id() . " " .
    "order by item_option_file_title");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button("add_file", "Add File", "item_option_file.php?option=" . id(), OPTIONAL);$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("items");
$page->header();
$page->title(id() ? "Edit Option" : "Add Option");
$form->render();
$page->footer();

?>