<?php
/********************************************************************

    costmonitoringgroup.php

    Edit Cost Monitoring Group

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-06
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("costmonitoringgroups", "costmonitoringgroup");

$form->add_section();
$form->add_edit("costmonitoringgroup_code", "Code", NOTNULL);
$form->add_edit("costmonitoringgroup_text", "Description", NOTNULL);

$form->add_section("Masterplan");
$form->add_checkbox("costmonitoringgroup_include_in_masterplan", "Consider items of this cost group in the furniture section of the master plan", 0, 0, "Masterplan");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

$page = new Page("costmonitoringgroups");
$page->header();
$page->title("Edit Cost Monitoring Group for Items");
$form->render();
$page->footer();

?>