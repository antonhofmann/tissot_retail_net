<?php
/********************************************************************

    province.php

    Creation and mutation of province records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-07-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-07-12
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$country = param("country_filter");


if(!$country and array_key_exists("country_filter", $_SESSION) and $_SESSION["country_filter"] > 0)
{
	$country = $_SESSION["country_filter"];
}
else
{
	$_SESSION["country_filter"] = $country;
}

$sql = "select * from places where place_id = " . id();
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$country = $row["place_country"];
}


// Build form

$form = new Form("places", "City");
$form->add_hidden("country_filter", param("country_filter"));

$form->add_section("Name and regions");
$form->add_list("place_country", "Country*",
    "select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT, $country);
$form->add_list("place_province", "Province*",
    "select province_id, province_canton from provinces where province_country = " . dbquote($country) . " order by province_canton", NOTNULL);
//$form->add_edit("place_name", "Name*", NOTNULL);
$form->add_edit("place_name", "Name*", NOTNULL, "", TYPE_CHAR, 0, 0, 2, "place_name");


//list for replacements
if(id() > 0)
{
	$form->add_section("Replace");
	$form->add_comment("Relpace the above city with the following city.");
	
	$sql_r = "select place_id, place_name from places ";

	$list_filter = "";
	if(param("country_filter") > 1)
	{
		$list_filter .=	" place_country = " . param("country_filter") . " ";
	}
	elseif(array_key_exists("country_filter", $_SESSION) and $_SESSION["country_filter"] > 0)
	{
		$list_filter .=	" place_country = " . $_SESSION["country_filter"] . " ";
	}
	else
	{
		$list_filter .=	" place_country = 1 ";
	}

	$sql_r .=  " where " . $list_filter . " and place_id <> " . id() . " order by place_name";
	$form->add_list("replacement_city", "New City", $sql_r);
	$form->add_button("replace", "Replace");

}

$link = "place_occurrence.php?id=" . id();; 
$link = "javascript:popup('". $link . "', 1024, 800)";

$form->add_button("save", "Save");
$form->add_button(FORM_BUTTON_DELETE, "Delete");
$form->add_button("check_occurence", "Check Occurence", $link);
$form->add_button("back", "Back");

// Populate form and process button clicks

$form->populate();
$form->process();


if($form->button("back"))
{
	$link = "places.php?country_filter=" . $form->value("country_filter");
	redirect($link);
}
elseif($form->button("save"))
{
	if($form->validate())
	{
		$form->save();
		
		//get store_locator_country
		$sql = "select country_store_locator_id from countries where country_id = " . dbquote($form->value("place_country"));

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$store_locator_country = $row["country_store_locator_id"];
		

			//update store locator
			$db2 = mysql_connect(STORE_LOCATOR_SERVER, STORE_LOCATOR_USER, STORE_LOCATOR_PASSWORD);
			$dbname2 = STORE_LOCATOR_DB;
			mysql_query( "SET NAMES 'utf8'");
			mysql_select_db($dbname2, $db2);

			
			$sql = "Update places set " . 
				   "place_country_id = " . $store_locator_country . ", " .
				   "place_province_id = " . $form->value("place_province") . ", " .  
				   "place_name = " . dbquote($form->value("place_name")) . 
				   " where place_id = " . id();

			$result = mysql_query($sql) or dberror($sql);
		}


		
		$db = mysql_pconnect(RETAILNET_SERVER, RETAILNET_USER, RETAILNET_PASSWORD);
		$dbname = RETAILNET_DB;
		mysql_query( "SET NAMES 'utf8'");
		mysql_select_db($dbname, $db);


		//update place names 
		$sql = "Update posaddresses set " . 
			   "posaddress_place = " . dbquote($form->value("place_name")) . 
			   " where posaddress_place_id = " . id();

		$result = mysql_query($sql) or dberror($sql);


		$sql = "Update _posaddresses set " . 
			   "posaddress_place = " . dbquote($form->value("place_name")) . 
			   " where posaddress_place_id = " . id();

		$result = mysql_query($sql) or dberror($sql);


		$sql = "Update posaddressespipeline set " . 
			   "posaddress_place = " . dbquote($form->value("place_name")) . 
			   " where posaddress_place_id = " . id();

		$result = mysql_query($sql) or dberror($sql);


		$sql = "Update orders set " . 
			   "order_billing_address_place = " . dbquote($form->value("place_name")) . 
			   " where order_billing_address_place_id = " . id();

		$result = mysql_query($sql) or dberror($sql);


		$link = "places.php?country_filter=" . $form->value("country_filter");
		redirect($link);
	}
}
elseif($form->button("replace"))
{
	$id_old_city = id();
	$id_new_city = $form->value("replacement_city");
	if(!$id_new_city)
	{
		$form->error("You can not replace a city with a NULL-value!");
	}
	else
	{
				
		$sql = "update addresses set address_place_id = " . $id_new_city . 
			   " where address_place_id = " . $id_old_city;

		$result = mysql_query($sql) or dberror($sql);


		$sql = "update _addresses set address_place_id = " . $id_new_city . 
			   " where address_place_id = " . $id_old_city;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update invoice_addresses set invoice_address_place_id = " . $id_new_city . 
			   " where invoice_address_place_id = " . $id_old_city;

		$result = mysql_query($sql) or dberror($sql);


		$sql = "update order_addresses set order_address_place_id = " . $id_new_city . 
			   " where order_address_place_id = " . $id_old_city;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update orders set order_billing_address_place_id = " . $id_new_city . 
			   " where order_billing_address_place_id = " . $id_old_city;

		$result = mysql_query($sql) or dberror($sql);


		$sql = "update posaddresses set posaddress_place_id = " . $id_new_city . 
			   " where posaddress_place_id = " . $id_old_city;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update _posaddresses set posaddress_place_id = " . $id_new_city . 
			   " where posaddress_place_id = " . $id_old_city;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posaddressespipeline set posaddress_place_id = " . $id_new_city . 
			   " where posaddress_place_id = " . $id_old_city;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update standard_delivery_addresses set delivery_address_place_id = " . $id_new_city . 
			   " where delivery_address_place_id = " . $id_old_city;

		$result = mysql_query($sql) or dberror($sql);



		//update place names 
		$sql = "select place_name from places where place_id = " . $id_new_city;
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$place_name = $row["place_name"];
			
			$sql = "Update posaddresses set " . 
				   "posaddress_place = " . dbquote($place_name) . 
				   " where posaddress_place_id = " . $id_new_city;

			$result = mysql_query($sql) or dberror($sql);


			$sql = "Update _posaddresses set " . 
				   "posaddress_place = " . dbquote($place_name) . 
				   " where posaddress_place_id = " . $id_new_city;

			$result = mysql_query($sql) or dberror($sql);


			$sql = "Update posaddressespipeline set " . 
				   "posaddress_place = " . dbquote($place_name) . 
				   " where posaddress_place_id = " . $id_new_city;

			$result = mysql_query($sql) or dberror($sql);


			$sql = "Update orders set " . 
				   "order_billing_address_place = " . dbquote($place_name) . 
				   " where order_billing_address_place_id = " . $id_new_city;

			$result = mysql_query($sql) or dberror($sql);


			$sql = "select * from places " . 
				   "left join provinces on province_id = place_province " .
				   "left join countries on country_id = place_country " . 
				   "where place_id = " . $id_new_city;
		
			$res = mysql_query($sql) or dberror($sql);
			$new_place = $row = mysql_fetch_assoc($res);

		
			//update store locator
			$db2 = mysql_connect(STORE_LOCATOR_SERVER, STORE_LOCATOR_USER, STORE_LOCATOR_PASSWORD);
			$dbname2 = STORE_LOCATOR_DB;
			mysql_query( "SET NAMES 'utf8'");
			mysql_select_db($dbname2, $db2);

			
				//check if city is available
				$sql = "select place_id from places where place_id = " . $id_new_city;
				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$sql = "update posaddresses set posaddress_place_id = " . $id_new_city . 
						   " where posaddress_place_id = " . $id_old_city;
					$result = mysql_query($sql) or dberror($sql);
				}
				else // add new city
				{
					//check if province is there
					$sql = "select count(province_id) as num_recs from provinces " . 
						   "where province_id = " . $new_place["province_id"];
					$res = mysql_query($sql) or dberror($sql);
					$row = mysql_fetch_assoc($res);
					if ($row["num_recs"] == 0)
					{
						$sql = "Insert into provinces (province_id, province_country_id, province_canton, province_region, province_adminregion, province_shortcut) VALUES (" . 
						dbquote($new_place["province_id"]) . ", " .
						dbquote($new_place["country_store_locator_id"]) . ", " .
						dbquote($new_place["province_canton"]) . ", " .
						dbquote($new_place["province_region"]) . ", " .
						dbquote($new_place["province_adminregion"]) . ", " .
						dbquote($new_place["province_shortcut"]) . ")";
						$result = mysql_query($sql) or dberror($sql);
					}

					$sql = "Insert into places (place_id, place_country_id, place_province_id, place_name) VALUES (" . 
						dbquote($new_place["place_id"]) . ", " .
						dbquote($new_place["country_store_locator_id"]) . ", " .
						dbquote($new_place["place_province"]) . ", " .
						dbquote($new_place["place_name"]) . ")";
						$result = mysql_query($sql) or dberror($sql);

					$sql = "update posaddresses set posaddress_place_id = " . $id_new_city . 
						   " where posaddress_place_id = " . $id_old_city;


					$result = mysql_query($sql) or dberror($sql);
				}

			$db = mysql_pconnect(RETAILNET_SERVER, RETAILNET_USER, RETAILNET_PASSWORD);
			$dbname = RETAILNET_DB;
			mysql_query( "SET NAMES 'utf8'");
			mysql_select_db($dbname, $db);
		}


		
		$form->message("Place was replaced and can be deleted now.");
		$_SESSION["country_filter"] = $country;


	}
}







// Render page

$page = new Page("places");

$page->header();
$page->title(id() ? "Edit City" : "New City");
$form->render();


?>

<script language="javascript">
	$("#h_place_name").click(function() {
	   $('#place_name').val($('#place_name').val().toLowerCase());
	   var txt = $('#place_name').val();

	   $('#place_name').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	
</script>

<?php
$page->footer();

?>