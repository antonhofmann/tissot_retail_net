<?php
/********************************************************************

    file_categories.php

    Lists file categories for editing.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2015-06-24
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2015-06-24
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("file_category.php");

$list = new ListView("select order_file_category_id, order_file_category_name, order_file_category_priority, " .
                     " if(order_file_category_type = 1, 'Catalogue Orders', 'Projects') as type, " .
					 " if(order_file_category_attach_in_mails = 1, 'yes', 'no') as attachment " .
                     "from order_file_categories");

$list->set_entity("order_file_categories");
$list->set_order("type,order_file_category_priority");
$list->set_group("type");
$list->set_filter(" order_file_category_type = 2");

$list->add_column("order_file_category_priority", "");
$list->add_column("order_file_category_name", "Name", "file_category.php", LIST_FILTER_FREE);
$list->add_column("attachment", "Mail attachment");

$list->add_button(LIST_BUTTON_NEW, "New", "file_category.php");

$list->process();

$page = new Page("order_file_categories");

$page->header();
$page->title("File Categories");
$list->render();
$page->footer();

?>
