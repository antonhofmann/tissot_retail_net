<?php
/********************************************************************

    design_brief_position.php

    Creation and mutation of design briefing positions.

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2017-12-11
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2017-12-11
    Version:        1.0.0

    Copyright (c) 2017, TISSOT SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$sql_dbg = "select design_brief_group_id, design_brief_group_name 
       from design_brief_groups 
	   order by design_brief_group_sort";


$types = array();

$types[1] = 'Input Field for Text';
$types[2] = 'Input Field for Integer';
$types[3] = 'Input Field for Decimal Number';
$types[4] = 'Input Field for Date Value';
$types[5] = 'Text Area';
$types[6] = 'Selection of Possible Values';
$types[7] = 'Multiple Choice';

$form = new Form("design_brief_positions", "design_brief_position");

$form->add_section();

$form->add_list("design_brief_position_group_id", "Design Briefing Section*",
    $sql_dbg, NOTNULL);
$form->add_edit("design_brief_position_text", "Caption*", NOTNULL);
$form->add_list("design_brief_position_type", "Type*",$types, NOTNULL);

$form->add_multiline("design_brief_position_infotext", "Infotext for the user", 6);

$form->add_comment('Only to be filled for the type "Selection of Possible Values":<br />Pue one line per possible value.'); 
$form->add_multiline("design_brief_position_values", "Possible Values are", 6);

$form->add_edit("design_brief_position_sort", "Sort Order*", NOTNULL, '', TYPE_INT, 2);
$form->add_checkbox("design_brief_position_mandatory", "Is mandatory");

$form->add_checkbox("design_brief_position_active", "In Use", true);

$form->add_section("Applicable to Locations of Type");
$form->add_checkbox("design_brief_position_store", "Boutique");
$form->add_checkbox("design_brief_position_sis", "SIS");
$form->add_checkbox("design_brief_position_kiosk", "Kiosk");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("design_brief_positions");
$page->header();
$page->title(id() ? "Edit Design Briefing Position" : "Add Design Briefing Position");
$form->render();
$page->footer();

?>