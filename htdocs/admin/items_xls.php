<?php
/********************************************************************

    items_xls.php

    Generate Excel-File items

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-10-17
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-10-17
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

require_once "../include/xls/Writer.php";

/********************************************************************
    prepare Data Needed
*********************************************************************/


$header = "";
$header = "Item List: (" . date("d.m.Y G:i") . ")";

/********************************************************************
    prepare Data
*********************************************************************/

$sql = "select DISTINCT item_id, item_code, item_name, item_price, " .
		 "    if(item_visible, 'yes', ' ') as item_visible, " .
		 "    if(item_visible_in_production_order, 'yes', ' ') as item_visible_in_production_order, " . 
		 "    if(item_visible_in_mps, 'yes', ' ') as item_visible_in_mps, " .
		 "   if(item_stock_property_of_swatch, 'yes', ' ') as item_stock_property_of_swatch, unit_name, " .
		  "   if(item_is_dr_swatch_furniture, 'yes', ' ') as item_is_dr_swatch_furniture, " .
		 " address_company, item_category_name " .
		 "from items " .
		 "left join suppliers on supplier_item = item_id " .
		 "left join addresses on address_id = supplier_address ". 
		 "left join units on unit_id = item_unit " . 
		 "left join item_categories on item_category_id = item_category";

$filter = " item_active = 1 and (item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . " or item_type <= " . ITEM_TYPE_COST_ESTIMATION . ")";

if(param("su")) {
	$filter .= " and supplier_address = " . param("su");
}

if(param("ca")) {
	
	$filter .= " and item_category = " . param("ca");
}

$sql = $sql . " where " . $filter . " order by item_category_name, item_code";

//echo $sql;
//abc();

/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "item_list_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename);

$xls->setVersion(8);

$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_normal_bold =& $xls->addFormat();
$f_normal_bold->setSize(8);
$f_normal_bold->setAlign('left');
$f_normal_bold->setBorder(1);
$f_normal_bold->setBold();


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('yellow');



//captions
$captions = array();
//$captions[] = "Nr";
//$captions[] = "Geographical Region";
$captions[] = "Category";
$captions[] = "Code";
$captions[] = "Name";
$captions[] = "Unit";
$captions[] = "Price";
$captions[] = "Supplier";
$captions[] = "in PO-Planning";
$captions[] = "Visible in MPS";
$captions[] = "Property of Tissot";
$captions[] = "Customer Services";

$col_widths = array();
$col_widths[] = 30;
$col_widths[] = 20;
$col_widths[] = 50;
$col_widths[] = 10;
$col_widths[] = 10;
$col_widths[] = 50;
$col_widths[] = 20;
$col_widths[] = 20;
$col_widths[] = 20;
$col_widths[] = 20;

/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);


$sheet->writeRow(2, 0, $captions, $f_normal_bold);


$row_index = 3;
$cell_index = 0;
$counter = 0;





$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{

	
	$sheet->write($row_index, $cell_index, $row["item_category_name"], $f_normal);
	$cell_index++;


	$sheet->write($row_index, $cell_index, $row["item_code"], $f_normal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["item_name"], $f_normal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["unit_name"], $f_normal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["item_price"], $f_number);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["address_company"], $f_normal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["item_visible_in_production_order"], $f_normal);
	$cell_index++;
	
	
	$sheet->write($row_index, $cell_index, $row["item_visible_in_mps"], $f_normal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["item_stock_property_of_swatch"], $f_normal);
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["item_is_dr_swatch_furniture"], $f_normal);
	$cell_index++;

	
	
	$cell_index = 0;
	$row_index++;
	
}

for($i=0;$i<count($captions);$i++)
{
	$sheet->setColumn($i, $i, $col_widths[$i]);
}


$xls->close(); 

?>