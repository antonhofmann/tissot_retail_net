<?php
/********************************************************************

    logistics_item.php

    Creation and mutation of item records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-29
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-04
    Version:        1.2.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(has_access("can_edit_catalog") 
  or has_access("can_edit_logistics_system_data"))
{

}
else
{
	check_access("can_edit_catalog");
}

// Build form

$sql_units = "select unit_id, unit_name from units order by unit_name";
$sql_packaging_types = "select packaging_type_id, packaging_type_name from packaging_types order by packaging_type_name";

$form = new Form("items", "item");



$form->add_section("Description");
$form->add_label("item_code", "Code");
$form->add_label("item_name", "Name");
$form->add_label("item_description", "Description");


$form->add_section("Specification");
$form->add_multiline("item_packed_size", "Packed Size", 4);
$form->add_multiline("item_packed_weight", "Packed Weight", 4);

$form->add_list("item_unit", "Unit of measure", $sql_units, 0);
$form->add_edit("item_width", "Width in cm", 0, "" , TYPE_DECIMAL, 10, 2);
$form->add_edit("item_height", "Height in cm", 0, "" , TYPE_DECIMAL, 10, 2);
$form->add_edit("item_length", "Length in cm", 0, "" , TYPE_DECIMAL, 10, 2);
$form->add_label("cbm", "CBM", 0);
$form->add_edit("item_radius", "Radius in cm", 0, "" , TYPE_DECIMAL, 10, 2);
$form->add_edit("item_gross_weight", "Gross weight in kg", 0, "" , TYPE_DECIMAL, 10, 2);
$form->add_edit("item_net_weight", "Net weight in kg", 0, "" , TYPE_DECIMAL, 10, 2);
$form->add_list("item_packaging_type", "Packaging", $sql_packaging_types, 0);
$form->add_checkbox("item_stackable", "",0, 0, "Stackable");


$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");


// Populate form and process button clicks

$form->populate();
$form->process();


$cbm = $form->value("item_width")*$form->value("item_height")*$form->value("item_length")/1000000;
$form->value("cbm", $cbm);

// Render page

$page = new Page("logistics");
require "include/logistics_page_actions.php";

$page->header();

$page->title(id() ? "Edit Item" : "Add Item");
$form->render();


$page->footer();
?>