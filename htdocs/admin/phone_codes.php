<?php
/********************************************************************

    phone_codes.php

    Lists countrie's phone for editing.

    Created by:     Anton Hofmann (antzon.hofmann@mediaparx.ch)
    Date created:   2016-11-02
    Modified by:    Anton Hofmann (antzon.hofmann@mediaparx.ch)
    Date modified:  2016-11-02
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(has_access("can_edit_catalog") or user_id() == 1634) {}
else {	redirect("noaccess.php");}


set_referer("phone_code.php");




/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("addresses", "address");

$form->add_section("List Filter Selection");

$form->add_list("selected_country", "Address Type",
	"select DISTINCT phone_code_country_name, phone_code_country_name from international_phone_codes order by phone_code_country_name", SUBMIT | NOTNULL, 1);


if(param("selected_country"))
{
	$list_filter = 'phone_code_country_name = "' . param("selected_country") . '"';
}
else
{
	$list_filter = 'phone_code_country_name = "Afghanistan"';
}


$sql = "select phone_code_id, phone_code_country_code, phone_code_country_name, phone_code_area_name,  " . 
       "phone_code_area_code1, phone_code_area_code2, phone_code_area_code3, phone_code_area_code4, country_name " .
       "from international_phone_codes " .
	   "left join countries on country_id = phone_code_country_id_retailnet";

$list = new ListView($sql);

$list->set_entity("countries");
$list->set_order("phone_code_country_name, phone_code_area_name");
$list->set_filter($list_filter);
$list->set_order("phone_code_area_code2");

$list->add_column("phone_code_country_name", "Country", "", LIST_FILTER_FREE);
$list->add_column("phone_code_country_code", "Country Code", "", LIST_FILTER_FREE);
$list->add_column("phone_code_area_name", "Area", "", LIST_FILTER_FREE);
$list->add_column("phone_code_area_code1", "Area Code 1", "", LIST_FILTER_FREE);
$list->add_column("phone_code_area_code2", "Area Code 2", "", LIST_FILTER_FREE);
$list->add_column("phone_code_area_code3", "Area Code 3", "", LIST_FILTER_FREE);
$list->add_column("phone_code_area_code4", "Area Code 4", "", LIST_FILTER_FREE);
$list->add_column("country_name", "Retailnet", "", LIST_FILTER_FREE);


//$list->add_button(LIST_BUTTON_NEW, "New", "phone_code.php");
$form->populate();
$list->process();

$page = new Page("phone_codes");

$page->header();
$page->title("Phone Codes");
$form->render();
$list->render();
$page->footer();
?>
