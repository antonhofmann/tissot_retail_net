<?php
/********************************************************************

    project_type_subclasses.php

    Lists project type sub classes

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-12-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-12-09
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("project_type_subclass.php");

$sql = "select project_type_subclass_id, project_type_subclass_name,  " .
       " concat(pms.user_name, ' ', pms.user_firstname) as pm, " .
	   " concat(lcos.user_name, ' ', lcos.user_firstname) as lco " . 
       " from project_type_subclasses " . 
	   " left join users as pms on pms.user_id = project_type_subclass_project_manager " . 
	   " left join users as lcos on lcos.user_id = project_type_subclass_logistics_coordinator ";

$list = new ListView($sql);

$list->set_entity("project_type_subclasses");
$list->set_order("project_type_subclass_name");

$list->add_column("project_type_subclass_name", "Name", "project_type_subclass.php");
$list->add_column("pm", "Project Leader", "");
//$list->add_column("lco", "Logistics Coordinator", "");
$list->add_button(LIST_BUTTON_NEW, "New", "project_type_subclass.php");

$list->process();

$page = new Page("project_type_subclasses");

$page->header();
$page->title("Project Type Subclasses");
$list->render();
$page->footer();

?>
