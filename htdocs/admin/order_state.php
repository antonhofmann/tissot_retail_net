<?php
/********************************************************************

    order_state.php

    Mutation of order state records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-10-18
    Modified by:    Anton.Hofmann (aho@mediaparx.ch)
    Date modified:  2004-02-10
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("order_states", "order status");


$sql_mail_templates = "select mail_template_id, mail_template_code from mail_templates where mail_template_application_id = 4 order by mail_template_code";




$sql = "select order_state_group_order_type " .
       "from order_states left join order_state_groups on order_state_group = order_state_group_id " .
       "where order_state_id = " . id();

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_row($res);

$order_type = $row[0];

$form->add_section();
$form->add_label("order_state_code", "Code");

$form->add_section();
$form->add_edit("order_state_name", "Order Status", NOTNULL);
$form->add_edit("order_state_action_name", "Action Name");
$form->add_list("order_state_predecessor", "Predecessor when planning is required", 
    "select order_state_id, concat(order_state_code, ' ', order_state_name) " .
    "from order_states left join order_state_groups on order_state_group = order_state_group_id " .
    "where order_state_group_order_type = $order_type and order_state_id <> " . id() . " " .
    "order by order_state_code");

$form->add_list("order_state_predecessor_no_planning", "Predecessor when no planning is required", 
    "select order_state_id, concat(order_state_code, ' ', order_state_name) " .
    "from order_states left join order_state_groups on order_state_group = order_state_group_id " .
    "where order_state_group_order_type = $order_type and order_state_id <> " . id() . " " .
    "order by order_state_code");


$form->add_section();
$form->add_comment("If no notification recipient group is selected notifications can be sent to every person involved in a project.");
$form->add_list("order_state_notification_recipient", "Notification Recipient", 
    "select notification_recipient_id, notification_recipient_name " .
    "from notification_recipients " .
    "order by notification_recipient_name");

//$form->add_list("order_state_notification_recipient2", "Notification Recipient 2", 
//    "select notification_recipient_id, notification_recipient_name " .
//    "from notification_recipients " .
//    "order by notification_recipient_name");

$form->add_list("order_state_performer", "Action is performed by", 
    "select role_id, role_name " .
    "from roles " .
    "order by role_name");


$form->add_list("order_state_order_state_type", "Submission Type", 
    "select order_state_type_id, order_state_type_name " .
    "from order_state_types " .
    "order by order_state_type_name");

$form->add_section();

$form->add_checkbox("order_state_append_task", "Add task to recipient's task list", 0, 0);
$form->add_checkbox("order_state_send_email", "Send mail when performed", 0, 0);
$form->add_checkbox("order_state_change_state", "Change status when performed", 0, 0);
$form->add_checkbox("order_state_delete_tasks", "Delete the task when performed", 0, 0);
$form->add_checkbox("order_state_manually_deleted", "Task must be deleted manually", 0, 0);
$form->add_checkbox("order_state_delete_predecessor", "Deletes preceding task when performed", 0, 0);

if($order_type == 1)
{
	$form->add_checkbox("order_state_used_for_logistics", "Is a logistic order state", 0, 0);
}

if($order_type == 2)
{
	$form->add_checkbox("order_state_active", "Order state is active", 0, 0);
}


/* prepared if task center is switched to mail templates
$form->add_section("Mail Template");
$form->add_comment("Do not change these settings unless you are sure of what you are doing!");
$form->add_list("order_state_mail_template_id", "Mail Template", $sql_mail_templates);
*/

$form->add_section("Email Notification");
$form->add_list("order_state_email_copy_to", "Copy of Email to", 
    "select user_id, concat(user_name, ' ', user_firstname) as username " .
    "from users " .
	"where user_address = 13 and user_active = 1  " .
    "order by user_name");

/*
$form->add_list("order_state_email_copy2_to", "Copy of Email to", 
    "select user_id, concat(user_name, ' ', user_firstname) as username " .
    "from users " .
	"where user_active = 1  " .
    "order by user_name");

*/

if($order_type == 1)
{
	$form->add_section("Additional Selectable Recipients");
	$form->add_checkbox("order_state_regional_responsible_as_cc", "Sales Coordinators and Regional Sales managers HQ", 0, 0, "Regional Responsibles");
}




if($order_type == 1)
{
	
	//get data
	$data = array();
	$sql = "select order_state_legal_type_project_cost_type_id, order_state_legal_type_projectkind_id " . 
		   " from order_state_legal_types " . 
		   " where order_state_legal_type_order_state_id = " . dbquote(id());
	
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$data[] = $row["order_state_legal_type_project_cost_type_id"] . "-" . $row["order_state_legal_type_projectkind_id"];
	}


	$form->add_section("Uploading Files");
	$sql_attachment_categories = "select order_file_category_id, order_file_category_name ".
                             "from order_file_categories ".
                             "where order_file_category_type = 2 ".
                             "order by order_file_category_priority";
	
	$form->add_list("order_state_file_category_id", "Category", $sql_attachment_categories, 0);

	$form->add_section("Selecting Existing Files");
	$form->add_list("order_state_file_category_id_to_select_from", "Show File list from the following category", $sql_attachment_categories, 0);


	$form->add_section("Application of Step");
	$form->add_comment("Indicate the pos and legal types for which this step is applicable.");
	
	$form->add_checklist("order_state_pos_type", "POS Types", "order_state_pos_types",
    "select postype_id, postype_name from postypes order by postype_name");
	
	$form->add_comment("&nbsp;");


	
	
	$sql_project_kinds = "select projectkind_id, projectkind_name from projectkinds " . 
		                 " where projectkind_id > 0 order by projectkind_name";

 
	$sql_legal_types = "select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id in (1,2,6) order by project_costtype_text";
	$res_legal_types = mysql_query($sql_legal_types) or dberror($sql_legal_types);
	
	while($row_legal_types = mysql_fetch_assoc($res_legal_types))
	{
		$form->add_comment("<strong>" .$row_legal_types["project_costtype_text"] . "</strong>");

		$res_project_kinds = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
		while($row_project_kinds = mysql_fetch_assoc($res_project_kinds))
		{
			$key = $row_legal_types["project_costtype_id"] . "-" . $row_project_kinds["projectkind_id"];
			
			
			if(in_array ( $key , $data))
			{
				$form->add_checkbox("LT". $key, $row_project_kinds["projectkind_name"], 1, 0, "");
			}
			else
			{
				$form->add_checkbox("LT". $key, $row_project_kinds["projectkind_name"], 0, 0, "");
			}
		}

		
	}
	

	

}



$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

if($form->button(FORM_BUTTON_SAVE))
{
	
	
	//save legal_types
	if($order_type == 1)
	{
		$sql = "delete from order_state_legal_types " . 
			   " where order_state_legal_type_order_state_id = " . dbquote(id());
		$result = mysql_query($sql) or dberror($sql);


		$res_legal_types = mysql_query($sql_legal_types) or dberror($sql_legal_types);
	
		while($row_legal_types = mysql_fetch_assoc($res_legal_types))
		{

			$res_project_kinds = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
			while($row_project_kinds = mysql_fetch_assoc($res_project_kinds))
			{
				$key = $row_legal_types["project_costtype_id"] . "-" . $row_project_kinds["projectkind_id"];
				
				if(array_key_exists("LT" . $key, $form->items))
				{
					if($form->items["LT" . $key]["value"] == 1)
					{
					
						$sql_i = "insert into order_state_legal_types " . 
							     "(order_state_legal_type_order_state_id, order_state_legal_type_project_cost_type_id, order_state_legal_type_projectkind_id, user_modified, date_modified) " . 
							     " VALUES (" . 
							     dbquote(id()) . "," . 
							     dbquote($row_legal_types["project_costtype_id"]) . "," .
							     dbquote($row_project_kinds["projectkind_id"]) . "," .
							     dbquote(user_login()) . "," .
							     dbquote(date("Y-m-d h:i:s")) . ")";
						$result = mysql_query($sql_i) or dberror($sql_i);
					}

				}
			}
		}
	}
	
	redirect("order_states.php");
}


$page = new Page("order_states");
$page->header();
$page->title("Edit Order Status");
$form->render();
$page->footer();

?>