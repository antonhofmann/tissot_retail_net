<?php
/********************************************************************

    invoice_address.php

    Mutation of client's invoice addresses.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2012-06-20
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2012-06-20
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$country_phone_prefix = '';
if(param("invoice_address_country_id"))
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = " . param("invoice_address_country_id") . " order by place_name";

	$country_phone_prefix = get_country_phone_prefix(param("invoice_address_country_id"));
}
else
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = {invoice_address_country_id} order by place_name";
}


// Build form

$form = new Form("invoice_addresses", "invoice_address_address");

$form->add_section("Client");
$form->add_lookup("invoice_address_address_id", "Client", "addresses", "address_company", 0, param("address"));


$form->add_section("Invoice Address");

$form->add_edit("invoice_address_company", "Company", NOTNULL);
$form->add_edit("invoice_address_company2", "");


$form->add_hidden("invoice_address_address");
$form->add_multi_edit("street", array("invoice_address_street", "invoice_address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6), array(), 0, '', '', array(40, 5));


$form->add_edit("invoice_address_address2", "Additional Address Info");

$form->add_list("invoice_address_country_id", "Country*",
    "select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);

$form->add_edit("invoice_address_zip", "Zip");
$form->add_list("invoice_address_place_id", "City*", $sql_places, NOTNULL | SUBMIT);

$form->add_section("Communication");

$form->add_hidden("invoice_address_phone");

$form->add_multi_edit("phone_number", array("invoice_address_phone_country", "invoice_address_phone_area", "invoice_address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($country_phone_prefix, '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


$form->add_hidden("invoice_address_mobile_phone");
$form->add_multi_edit("mobile_phone_number", array("invoice_address_mobile_phone_country", "invoice_address_mobile_phone_area", "invoice_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($country_phone_prefix, '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));



$form->add_edit("invoice_address_email", "Email");
$form->add_edit("invoice_address_contact_name", "Contact Name");

$form->add_checkbox("invoice_address_active", "Address in Use", true);


$form->add_button("save", "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();


if($form->button("save"))
{
	
	$form->value("invoice_address_phone", $form->unify_multi_edit_field($form->items["phone_number"]));
		$form->value("invoice_address_mobile_phone", $form->unify_multi_edit_field($form->items["mobile_phone_number"]));

	$form->add_validation("{invoice_address_phone} != '' or {invoice_address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");
	
	if($form->validate())
	{
		


		$form->value("invoice_address_address", $form->unify_multi_edit_field($form->items["street"], get_country_street_number_rule($form->value("invoice_address_country_id"))));

		
		
		
		$form->save();

		$form->message("Your data has been saved.");
	}

}


// Render page

$page = new Page("addresses");

$page->header();
$page->title(id() ? "Edit Invoice Address" : "Add Invoice Address");
$form->render();

$page->footer();