<?php
/********************************************************************

    file_type.php

    Creation and mutation of file type records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-13
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-09-17
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("file_types", "file type");

$form->add_section();
$form->add_edit("file_type_name", "Name", NOTNULL | UNIQUE);
$form->add_edit("file_type_mime_type", "MIME Type", NOTNULL);
$form->add_edit("file_type_extension", "Extension", NOTNULL);

$form->add_section();
$form->add_multiline("file_type_description", "Description", 4);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("file_types");
$page->header();
$page->title(id() ? "Edit File Type" : "Add File Type");
$form->render();
$page->footer();

?>