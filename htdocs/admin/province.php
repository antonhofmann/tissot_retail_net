<?php
/********************************************************************

    province.php

    Creation and mutation of province records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-07-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-07-12
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$country = param("country_filter");

if(!$country and array_key_exists("country_filter", $_SESSION) and $_SESSION["country_filter"] > 0)
{
	$country = $_SESSION["country_filter"];
}
else
{
	$_SESSION["country_filter"] = $country;
}

// Build form

$form = new Form("provinces", "province");

$form->add_hidden("country_filter", param("country_filter"));


$form->add_section("Name and regions");
$form->add_list("province_country", "Country*",
    "select country_id, country_name from countries order by country_name", NOTNULL, $country);
$form->add_edit("province_canton", "Province*", NOTNULL);
$form->add_edit("province_region", "Region");
$form->add_edit("province_adminregion", "Administrative Region");
$form->add_edit("province_shortcut", "Shortcut");


//list for replacements
if(id() > 0)
{
	$form->add_section("Replace");
	$form->add_comment("Relpace the above province with the following province.");
	
	$sql_r = "select province_id, province_canton from provinces ";

	$list_filter = "";
	if(param("country_filter") > 1)
	{
		$list_filter .=	"province_country = " . param("country_filter") . " ";
	}
	elseif(array_key_exists("country_filter", $_SESSION) and $_SESSION["country_filter"] > 0)
	{
		$list_filter .=	" province_country = " . $_SESSION["country_filter"] . " ";
	}
	else
	{
		$list_filter .=	" province_country = 1 ";
	}

	$sql_r .=  " where " . $list_filter . " and province_id <> " . id() . " order by province_canton";
	$form->add_list("replacement_province", "New Province", $sql_r);
	$form->add_button("replace", "Replace");

}

$form->add_button("save", "Save");
$form->add_button(FORM_BUTTON_DELETE, "Delete");
$form->add_button("back", "Back");

// Populate form and process button clicks

$form->populate();
$form->process();

if($form->button("back"))
{
	$link = "provinces.php?province_country=" . param("province_country");
	redirect($link);
}
elseif($form->button("save"))
{
	if($form->validate())
	{
		$form->save();

		//update store locator
		$db2 = mysql_connect(STORE_LOCATOR_SERVER, STORE_LOCATOR_USER, STORE_LOCATOR_PASSWORD);
		$dbname2 = STORE_LOCATOR_DB;
		mysql_query( "SET NAMES 'utf8'");
		mysql_select_db($dbname2, $db2);

		$sql = "Update provinces set " . 
			   "province_country_id = " . $form->value("province_country") . ", " .
			   "province_canton = " . dbquote($form->value("province_canton")) . ", " .  
			   "province_region = " . dbquote($form->value("province_region")) . ", " .  
			   "province_adminregion = " . dbquote($form->value("province_adminregion")) . ", " .  
			   "province_region = " . dbquote($form->value("province_region")) . ", " .  
			   "province_shortcut = " . dbquote($form->value("province_shortcut")) . 
			   " where province_id = " . id();

		$result = mysql_query($sql) or dberror($sql);

		$db = mysql_pconnect(RETAILNET_SERVER, RETAILNET_USER, RETAILNET_PASSWORD);
		$dbname = RETAILNET_DB;
		mysql_query( "SET NAMES 'utf8'");
		mysql_select_db($dbname, $db);

		$link = "provinces.php?province_country=" . param("province_country");
		redirect($link);
	}
}
elseif($form->button("replace"))
{
	$id_old_province = id();
	$id_new_province = $form->value("replacement_province");
	if(!$id_new_province)
	{
		$form->error("You can not replace a province with a NULL-value!");
	}
	else
	{
				
		$sql = "update places set place_province = " . $id_new_province . 
			   " where place_province = " . $id_old_province;

		$result = mysql_query($sql) or dberror($sql);


				
		$form->message("Province was replaced and can be deleted now.");
		$_SESSION["country_filter"] = $country;


	}
}

// Render page

$page = new Page("provinces");

$page->header();
$page->title(id() ? "Edit Province" : "New Province");
$form->render();
$page->footer();

?>