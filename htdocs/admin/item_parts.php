<?php
/********************************************************************

    item_parts.php

    Lists items to be assigned as parts to another item.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-12
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-09-17
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$list = new ListView("select item_id, item_code, item_name, item_price, item_type_name " .
                     "from items left join item_types on item_type = item_type_id", LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("items");
$list->set_order("item_code");
//$list->set_group("item_type_priority", "item_type_name");
$list->set_filter("item_type = " . ITEM_TYPE_STANDARD);
$list->set_checkbox("parts", "items", param("item"));

$list->add_hidden("item", param("item"));

$list->add_column("item_code", "Code", "", LIST_FILTER_FREE);
$list->add_column("item_name", "Name", "", LIST_FILTER_FREE);
$list->add_column("item_price", "Price", "", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_SAVE, "Save");
$list->add_button(LIST_BUTTON_BACK, "Back");

$list->process();

$page = new Page("items");

$sql = "select item_code from items where item_id = " . param("item");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_row($res);

$page->header();
$page->title("Assign Parts To Item \"" . htmlspecialchars($row[0]) . "\"");
$list->render();
$page->footer();
?>
