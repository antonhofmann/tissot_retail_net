<?php
/********************************************************************

    supplier_client_invoice.php

    Mutation of supplier client relation in the context of invoicing records.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2012-06-20
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2012-06-20
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


// Build form

$form = new Form("supplier_client_invoices", "supplier_client_invoice");
$form->add_hidden("address", param("address"));
$form->add_hidden("saved", 1);

$form->add_section("Supplier");
$form->add_lookup("supplier_client_invoice_supplier", "Company", "addresses", "address_company", 0, param("address"));


$form->add_section("Time Period for direct Invoicing");
$form->add_comment("Please note that the year can be 2038 in maximum.");
$form->add_edit("supplier_client_invoice_startdate", "Start invoicing directly from date*", NOTNULL, 0, TYPE_DATE, 10);
$form->add_edit("supplier_client_invoice_enddate", "Stop invoicing directly to date*", NOTNULL, "1.1.2038", TYPE_DATE, 10);






$form->add_button("select_all", "Select all", "javascript:select_all_addresses();");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button("save", "Save");
//$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();



//list of companies


//get existing relations
$clients = array();
$sql = "select supplier_client_invoice_client " . 
       "from supplier_client_invoices " .
	   "where supplier_client_invoice_supplier = " . param("address");


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$clients[$row["supplier_client_invoice_client"]] = 1;

}

$sql = "select address_id, address_company, country_name " . 
       "from addresses " . 
	   "left join countries on country_id = address_country " . 
	   "where address_type = 1 ";

$addresses = array();
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$addresses[$row["address_id"]] = $row["address_company"];

}

$list = new ListView($sql);


$list->set_entity("addresses");
$list->set_order("country_name, address_company");

if(param("address") == 1)
{
	$list->add_checkbox_column("ai");
}
else
{
	$list->add_checkbox_column("ai", "", 0, $clients);
}
$list->add_column("country_name", "Country", "");
$list->add_column("address_company", "Company", "");

$list->populate();
$list->process();


if($form->button("save"))
{
	
	
	foreach($list->columns[0]['values'] as $address_id=>$value) 
	{
		
		
		if(array_key_exists('__addresses_ai_' . $address_id, $_POST))
		{
			
			
			//check if record exists
			$sql = "select * from supplier_client_invoices " . 
				   "where supplier_client_invoice_supplier = " . param("address") . 
				   " and supplier_client_invoice_client = " . $address_id;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				/*
				$sql = "update supplier_client_invoices SET " . 
					   "supplier_client_invoice_startdate = " . dbquote(from_system_date($form->value("supplier_client_invoice_startdate"))) . ", " . 
					   "supplier_client_invoice_enddate = " . dbquote(from_system_date($form->value("supplier_client_invoice_enddate"))) . ", " . 
					   "user_modified = " . dbquote(user_login()) . ", " . 
					   "date_modified = " . dbquote(date("Y-m-d H:i:s")) . 
					   "where supplier_client_invoice_supplier = " . param("address") . 
					   " and supplier_client_invoice_client = " . $address_id;
				$result = mysql_query($sql) or dberror($sql);
				*/
			}
			else
			{
				$start_date = "";
				$end_date = "";
				if($form->value("supplier_client_invoice_startdate"))
				{
					$start_date = from_system_date($form->value("supplier_client_invoice_startdate"));
				}
				if($form->value("supplier_client_invoice_enddate"))
				{
					$enddate_date = from_system_date($form->value("supplier_client_invoice_enddate"));
				}

				if(!$start_date)
				{
					$start_date = date("Y-m-d");
				}
				if(!$end_date)
				{
					$end_date = date("2038-01-01");
				}
				
				$sql = "Insert into supplier_client_invoices (" . 
					   "supplier_client_invoice_supplier, supplier_client_invoice_client, " . 
					   "supplier_client_invoice_startdate, supplier_client_invoice_enddate, " . 
					   "date_created, user_created) VALUES (" .
					   param("address") . ", " . 
					   $address_id . ", " .
					   dbquote($start_date)  . ", " .
					   dbquote($end_date) . ", " .
					   dbquote(date("Y-m-d H:i:s")) . ", " .
					   dbquote(user_login()) . ")";

					$result = mysql_query($sql) or dberror($sql);
			
			}
	
		
		}
		else
		{
			$sql = "delete from supplier_client_invoices " . 
				   "where supplier_client_invoice_supplier = " . param("address") . 
				   " and supplier_client_invoice_client = " . $address_id;

			$result = mysql_query($sql) or dberror($sql);
		}
		
		
	}
	redirect("address.php?id=" . param("address"));
}



// Render page

$page = new Page("addresses");

$page->header();
$page->title("Add/Edit Invoicing");
$form->render();
$list->render();


?>
<script language='javascript'>

	function select_all_addresses()
	{
		<?php
		foreach($addresses as $id=>$name)
		{
			echo "$('input[name=__addresses_ai_" . $id . "]').attr('checked', true);";
		}
		?>

		var a = document.getElementById('select_all');
		a.innerHTML = "<a href='javascript:deselect_all_addresses();'>Deselect All</a>";
		
		
	}


	function deselect_all_addresses()
	{
		<?php
		foreach($addresses as $id=>$name)
		{
			echo "$('input[name=__addresses_ai_" . $id . "]').attr('checked', false);";
		}
		?>

		var a = document.getElementById('select_all');
		a.innerHTML = "<a href='javascript:select_all_addresses();'>Select All</a>";
		
		
	}
</script>

<?php
$page->footer();