<?php
/********************************************************************

    design_brief_groups.php

    Lists design brief groups for editing .

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2017-12-11
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2017-12-11
    Version:        1.0.0

    Copyright (c) 2017, TISSOT SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("design_brief_group.php");

$sql = "select design_brief_group_id, design_brief_group_name, design_brief_group_sort " .
       "from design_brief_groups ";

$list = new ListView($sql);

$list->set_entity("design_brief_groups");
$list->set_order("design_brief_group_sort");

$list->add_column("design_brief_group_sort", "Sort", "");
$list->add_column("design_brief_group_name", "Name", "design_brief_group.php");

$list->add_button(LIST_BUTTON_NEW, "New", "design_brief_group.php");

$list->process();

$page = new Page("design_brief_groups");

$page->header();
$page->title("Design Briefing Sections");
$list->render();
$page->footer();

?>
