<?php
/********************************************************************

    place_occurrence.php

    Lists occurence of a place

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-02-10
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-02-10
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$sql = "select place_name from places " . 
       "where place_id = " . param("id");

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$place_name = $row["place_name"];



/********************************************************************
    Create List for Addresses
*********************************************************************/ 

$sql = "select DISTINCT address_id, address_company " . 
       " from addresses ";

$list10_filter = "address_type <> 7 and address_place_id = " . param("id");


$list10 = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list10->set_entity("adresses");
$list10->set_order("address_company");
$list10->set_filter($list10_filter);
$list10->set_title("Company Addresses");

//$list10->add_column("order_nuber", "City", "place.php?country_filter=" . param("country_filter"));
$list10->add_column("address_company", "Company", "/admin/address.php?id={address_id}");



/********************************************************************
    Create List for Third Party Addresses
*********************************************************************/ 

$sql = "select DISTINCT address_id, address_company " . 
       " from addresses ";

$list11_filter = "address_type = 7 and address_place_id = " . param("id");


$list11 = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list11->set_entity("adresses");
$list11->set_order("address_company");
$list11->set_filter($list11_filter);
$list11->set_title("<br />Third Party Company Addresses");

//$list11->add_column("order_nuber", "City", "place.php?country_filter=" . param("country_filter"));
$list11->add_column("address_company", "Company", "/admin/address3rd.php?id={address_id}");



/********************************************************************
    Create List for Addresses in Data Corrections
*********************************************************************/ 

$sql = "select DISTINCT address_id, address_company " . 
       " from _addresses ";

$list12_filter = "address_place_id = " . param("id");


$list12 = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list12->set_entity("adresses");
$list12->set_order("address_company");
$list12->set_filter($list12_filter);
$list12->set_title("<br />Company Addresses in POS Index Data Corrections");
$list12->add_column("address_company", "Company", "/pos/correct_address_data_address.php?id={address_id}");



/********************************************************************
    Create List for POS Addresses
*********************************************************************/ 

$sql = "select DISTINCT posaddress_id, posaddress_name " . 
       " from posaddresses ";

$list20_filter = "posaddress_place_id = " . param("id");


$list20 = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list20->set_entity("posadresses");
$list20->set_order("posaddress_name");
$list20->set_filter($list20_filter);
$list20->set_title("<br />POS-Addresses");

//$list20->add_column("order_nuber", "City", "place.php?country_filter=" . param("country_filter"));
$list20->add_column("posaddress_name", "Project Number", "/pos/posindex_pos.php?id={posaddress_id}");




/********************************************************************
    Create List for POS Addresses in Data Corrections
*********************************************************************/ 

$sql = "select DISTINCT posaddress_id, posaddress_name " . 
       " from _posaddresses ";

$list21_filter = "posaddress_place_id = " . param("id");


$list21 = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list21->set_entity("posadresses");
$list21->set_order("posaddress_name");
$list21->set_filter($list21_filter);
$list21->set_title("<br />POS-Addresses in POS Index data Corrections");

//$list21->add_column("order_nuber", "City", "place.php?country_filter=" . param("country_filter"));
$list21->add_column("posaddress_name", "Project Number", "/pos/correct_pos_data_pos.php?id={posaddress_id}");


/********************************************************************
    Create List for Order Adresses and Projects
*********************************************************************/ 

$sql = "select DISTINCT order_number, project_id " . 
       " from order_addresses " . 
	   "left join orders on order_id = order_address_order " . 
	   "left join projects on project_order = order_id";

$list100_filter = "order_type = 1 and order_address_type = 2 and order_address_place_id = " . param("id");


$list100 = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list100->set_entity("order_adresses");
$list100->set_order("order_number");
$list100->set_filter($list100_filter);
$list100->set_title("<br />Delivery Addresses in Projects");

//$list100->add_column("order_nuber", "City", "place.php?country_filter=" . param("country_filter"));
$list100->add_column("order_number", "Project Number", "/user/project_view_traffic_data.php?pid={project_id}");



/********************************************************************
    Create List for Order Adresses
*********************************************************************/ 

$sql = "select DISTINCT order_number, order_id " . 
       " from order_addresses " . 
	   "left join orders on order_id = order_address_order";

$list101_filter = "order_type = 2 and order_address_type = 2 and order_address_place_id = " . param("id");


$list101 = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list101->set_entity("order_adresses");
$list101->set_order("order_number");
$list101->set_filter($list101_filter);
$list101->set_title("<br />Delivery Addresses in Catalogue Orders");

//$list101->add_column("order_nuber", "City", "place.php?country_filter=" . param("country_filter"));
$list101->add_column("order_number", "Order Number", "/user/order_view_traffic_data.php?oid={order_id}");



/********************************************************************
    Create List for Order Adresses and Projects
*********************************************************************/ 

$sql = "select DISTINCT order_number, project_id " . 
       " from order_addresses " . 
	   "left join orders on order_id = order_address_order " . 
	   "left join projects on project_order = order_id";

$list102_filter = "order_type = 1 and order_address_type = 4 and order_address_place_id = " . param("id");


$list102 = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list102->set_entity("order_adresses");
$list102->set_order("order_number");
$list102->set_filter($list102_filter);
$list102->set_title("<br />Pickup Addresses in Projects");

//$list102->add_column("order_nuber", "City", "place.php?country_filter=" . param("country_filter"));
$list102->add_column("order_number", "Project Number", "/user/project_view_traffic_data.php?pid={project_id}");



/********************************************************************
    Create List for Order Adresses
*********************************************************************/ 

$sql = "select DISTINCT order_number, order_id " . 
       " from order_addresses " . 
	   "left join orders on order_id = order_address_order";

$list103_filter = "order_type = 4 and order_address_type = 2 and order_address_place_id = " . param("id");


$list103 = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list103->set_entity("order_adresses");
$list103->set_order("order_number");
$list103->set_filter($list103_filter);
$list103->set_title("<br />Pick Addresses in Catalogue Orders");

//$list103->add_column("order_nuber", "City", "place.php?country_filter=" . param("country_filter"));
$list103->add_column("order_number", "Order Number", "/user/order_view_traffic_data.php?oid={order_id}");




/********************************************************************
    Create List for Invoice Adresses
*********************************************************************/ 

$sql = "select DISTINCT invoice_address_id, invoice_address_company, address_id " . 
       " from invoice_addresses " .
	   " left join addresses on address_id = invoice_address_address_id";

$list200_filter = "invoice_address_place_id = " . param("id");


$list200 = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list200->set_entity("invoice_addresses");
$list200->set_order("invoice_address_company");
$list200->set_filter($list200_filter);
$list200->set_title("<br />Invoice Addresses");

//$list200->add_column("order_nuber", "City", "place.php?country_filter=" . param("country_filter"));
$list200->add_column("invoice_address_company", "Company", "/admin/invoice_address.php?address={address_id}&id={invoice_address_id}");


/********************************************************************
   Create Page
*********************************************************************/ 
$page = new Page("places");

$page->header();
$page->title("Occurence of " . $place_name . " in Retail Net");

$list10->render();

$list11->render();

$list12->render();

$list20->render();

$list21->render();

$list100->render();

$list101->render();

$list102->render();

$list103->render();

$list200->render();

$page->footer();

?>
