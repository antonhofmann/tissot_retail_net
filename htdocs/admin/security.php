<?php
/********************************************************************

    security.php

    Lists locked IPS for unlocking .

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-09-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-09-05
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_unlock_ips");
set_referer("security_unlock_ip.php");



$first_year = date("Y");
$years = array();
$sql = "select DISTINCT YEAR(sec_lockedip_date) as year from sec_lockedips order by YEAR(sec_lockedip_date) DESC";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    $years[$row["year"]] = $row["year"];
}


/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("sec_loginfailures", "sec_loginfailures");

$form->add_section("List Filter Selection");

if(param("year"))
{
	$form->add_list("year", "Year",	$sql, SUBMIT | NOTNULL, param("year"));
}
else
{
	$form->add_list("year", "Year", $sql, SUBMIT | NOTNULL, $first_year);
}

$form->populate();



/********************************************************************
    Create List
*********************************************************************/ 
$sql = "select sec_lockedip_id, sec_lockedip_ip, sec_lockedip_date from sec_lockedips";


$list = new ListView($sql);

$list->set_entity("sec_lockedips");
$list->set_order("sec_lockedip_ip, sec_lockedip_date");

if(param("year"))
{
	$list->set_filter("YEAR(sec_lockedip_date) = " . param("year"));
}
else
{
	$list->set_filter("YEAR(sec_lockedip_date) = " . $first_year);
}

$list->add_column("sec_lockedip_ip", "IP Address", "security_unlock_ip.php");
$list->add_column("sec_lockedip_date", "Date:Time");

$iplocked = get_session_value("iplocked");

$list->process();

$page = new Page("security");

$page->header();
$page->title("Login Failures: Locked IP Addresses");
$form->render();
$list->render();
$page->footer();

?>
