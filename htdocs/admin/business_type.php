<?php
/********************************************************************

    business_type.php

    Edit Business types 

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-03-28
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-03-28
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("businesstypes", "business type");

$form->add_section();

$form->add_edit("businesstype_text", "Business Type", NOTNULL | UNIQUE);
$form->add_checkbox("businesstype_neighbour_needed", "Indication of a neighbour is mandatory", "", 0, "Neighbour");


//list for replacements
if(id() > 0)
{
	$sql_r = "select businesstype_id, businesstype_text " . 
		     "from businesstypes " . 
		     " where businesstype_id <> " . id() . 
		     " order by businesstype_text";

		
	$form->add_section("Replace");
	$form->add_comment("Relpace the above business type with the following business type.");

	$form->add_list("replacement_type", "New Business Type", $sql_r);
	$form->add_button("replace", "Replace");

}



$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();


if($form->button("replace"))
{
	$id_old_type = id();
	$id_new_type = $form->value("replacement_type");
	if(!$id_new_type)
	{
		$form->error("You can not replace a business type with a NULL-value!");
	}
	else
	{
		$sql = "update _posaddresses set posaddress_neighbour_left_business_type = " . $id_new_type . 
			   " where posaddress_neighbour_left_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update _posaddresses set posaddress_neighbour_right_business_type = " . $id_new_type . 
			   " where posaddress_neighbour_right_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update _posaddresses set posaddress_neighbour_acrleft_business_type = " . $id_new_type . 
			   " where posaddress_neighbour_acrleft_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update _posaddresses set posaddress_neighbour_acrright_business_type = " . $id_new_type . 
			   " where posaddress_neighbour_acrright_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);


		
		$sql = "update posaddresses set posaddress_neighbour_left_business_type = " . $id_new_type . 
			   " where posaddress_neighbour_left_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posaddresses set posaddress_neighbour_right_business_type = " . $id_new_type . 
			   " where posaddress_neighbour_right_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posaddresses set posaddress_neighbour_acrleft_business_type = " . $id_new_type . 
			   " where posaddress_neighbour_acrleft_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posaddresses set posaddress_neighbour_acrright_business_type = " . $id_new_type . 
			   " where posaddress_neighbour_acrright_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);



		$sql = "update posaddressespipeline set posaddress_neighbour_left_business_type = " . $id_new_type . 
			   " where posaddress_neighbour_left_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posaddressespipeline set posaddress_neighbour_right_business_type = " . $id_new_type . 
			   " where posaddress_neighbour_right_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posaddressespipeline set posaddress_neighbour_acrleft_business_type = " . $id_new_type . 
			   " where posaddress_neighbour_acrleft_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posaddressespipeline set posaddress_neighbour_acrright_business_type = " . $id_new_type . 
			   " where posaddress_neighbour_acrright_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);



		$sql = "update posorders set posorder_neighbour_left_business_type = " . $id_new_type . 
			   " where posorder_neighbour_left_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posorders set posorder_neighbour_right_business_type = " . $id_new_type . 
			   " where posorder_neighbour_right_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posorders set posorder_neighbour_acrleft_business_type = " . $id_new_type . 
			   " where posorder_neighbour_acrleft_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posorders set posorder_neighbour_acrright_business_type = " . $id_new_type . 
			   " where posorder_neighbour_acrright_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);


		$sql = "update posorderspipeline set posorder_neighbour_left_business_type = " . $id_new_type . 
			   " where posorder_neighbour_left_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posorderspipeline set posorder_neighbour_right_business_type = " . $id_new_type . 
			   " where posorder_neighbour_right_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posorderspipeline set posorder_neighbour_acrleft_business_type = " . $id_new_type . 
			   " where posorder_neighbour_acrleft_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posorderspipeline set posorder_neighbour_acrright_business_type = " . $id_new_type . 
			   " where posorder_neighbour_acrright_business_type = " . $id_old_type;

		$result = mysql_query($sql) or dberror($sql);



		$form->message("Place was replaced and can be deleted now.");
	}
}

$page = new Page("business_types");
$page->header();
$page->title(id() ? "Edit Business Type" : "Add Business Type");
$form->render();
$page->footer();

?>