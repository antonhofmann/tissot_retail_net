<?php
/********************************************************************

	user_company_access_xls.php

    Generate Excel File of regional responsabilities

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-12-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-12-03
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

require_once "../include/xls/Writer.php";

/********************************************************************
    prepare Data Needed
*********************************************************************/
$header = "Users and Roles: " . " (" . date("d.m.Y") . ")";

/********************************************************************
    prepare Excel Sheet
*********************************************************************/

$filename = "roles_and_users.xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();


//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);


$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);


$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextWrap();

$f_caption1 =& $xls->addFormat();
$f_caption1->setSize(8);
$f_caption1->setAlign('left');
$f_caption1->setBorder(1);
$f_caption1->setBold();
$f_caption1->setTextRotation(270);
$f_caption1->setTextWrap();


/********************************************************************
    write header
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);

/********************************************************************
    write all roles
*********************************************************************/
$rowindex = 2;
$cellindex = 0;
//$sheet->setRow($rowindex, 120);


$sheet->write($rowindex,$cellindex, "User",$f_normal);
$cellindex++;

$sheet->write($rowindex,$cellindex, "Sales Region",$f_normal);
$cellindex++;

$sheet->write($rowindex,$cellindex, "Country",$f_normal);
$cellindex++;

$sheet->write($rowindex,$cellindex, "Company",$f_normal);
$cellindex++;

$sheet->write($rowindex,$cellindex, "Retail",$f_normal);
$cellindex++;

$sheet->write($rowindex,$cellindex, "Wholesale",$f_normal);
$cellindex++;



/********************************************************************
    write all permissions
*********************************************************************/
$rowindex = 3;


$sheet->setColumn($cellindex, $cellindex, 100);

$sql = "SELECT
user_firstname,
user_name,
country_name,
address_company,
if(user_company_responsible_retail = 1, 'x', '') as retail,
if(user_company_responsible_wholsale = 1, 'x', '') as wholsale,
salesregion_name
FROM
user_company_responsibles
INNER JOIN addresses ON user_company_responsibles.user_company_responsible_address_id = addresses.address_id
INNER JOIN countries ON addresses.address_country = countries.country_id
INNER JOIN users ON user_company_responsibles.user_company_responsible_user_id = users.user_id
INNER JOIN salesregions ON salesregion_id = country_salesregion
order by 
users.user_name, users.user_firstname,
countries.country_name,
addresses.address_company" ;

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$cellindex = 0;

	$sheet->write($rowindex,$cellindex, $row["user_name"] . " " . $row["user_firstname"],$f_normal);
	$cellindex++;
	
	

	$sheet->write($rowindex,$cellindex, $row["salesregion_name"],$f_normal);
	$cellindex++;

	$sheet->write($rowindex,$cellindex, $row["country_name"],$f_normal);
	$cellindex++;

	$sheet->write($rowindex,$cellindex, $row["address_company"],$f_normal);
	$cellindex++;

	$sheet->write($rowindex,$cellindex, $row["retail"],$f_normal);
	$cellindex++;

	$sheet->write($rowindex,$cellindex, $row["wholsale"],$f_normal);
	$cellindex++;

	

	

	$cellindex = 0;
	$rowindex++;
}


$sheet->setColumn(0, 0,20);
$sheet->setColumn(1, 1,25);
$sheet->setColumn(2, 2,20);
$sheet->setColumn(3, 3,40);
$sheet->setColumn(4, 5,10);

$xls->close(); 

?>