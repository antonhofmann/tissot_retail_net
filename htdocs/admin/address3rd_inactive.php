<?php
/********************************************************************

    address3rd_inactive.php

    Creation and mutation of address records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-29
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-10-17
    Version:        1.0.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("user.php");

// Build form


$country_phone_prefix = '';
if(param("address_country"))
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = " . param("address_country") . " order by place_name";

	$country_phone_prefix = get_country_phone_prefix(param("address_country"));
}
else
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = {address_country} order by place_name";
}



$address_active = "";
$tracking_data = array();
$address_modified = "";
if(id())
{
	$sql = "select address_added_by, address_last_modified, address_last_modified_by, address_set_to_inactive_by, address_set_to_inactive_date, date_created, address_active " . 
		   "from addresses " . 
		   "where address_id = " . id();

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res)) 
	{
		
		$address_active = $row["address_active"];
		$tracking_data["address_added_by"] = $row["address_added_by"];
		$tracking_data["address_set_to_inactive_by"] = $row["address_set_to_inactive_by"];
		$tracking_data["address_last_modified_by"] = $row["address_last_modified_by"];

		$date_created["address_added_by"] = $row["date_created"];
		$date_created["address_set_to_inactive_by"] = $row["address_set_to_inactive_date"];
		$date_created["address_last_modified_by"] = $row["address_last_modified"];

		foreach($tracking_data as $key=>$user_id)
		{
			$sql = "select user_name, user_firstname " . 
				   "from users " . 
					"where user_id = " . dbquote($user_id);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) 
			{
				$tracking_data[$key] = $row["user_name"] . " " . $row["user_firstname"] . " " . $date_created[$key];
			}
		}
	}
}


$form = new Form("addresses", "address");

$form->add_hidden("country", param("country"));
$form->add_hidden("type", param("type"));
$form->add_hidden("searchterm", param("searchterm"));

$form->add_section("Name and address");
//$form->add_edit("address_number", "Tissot SAP Customer Code Merchandising");
//$form->add_edit("address_number2", "Tissot SAP Customer Code Retail Furniture");
//$form->add_edit("address_legnr", "SG Legal Number");
$form->add_label("address_sapnr", "SAP Customer Code");
//$form->add_edit("address_shortcut", "Shortcut", NOTNULL | UNIQUE);
$form->add_edit("address_company", "Company", NOTNULL);
$form->add_edit("address_company2", "");
$form->add_hidden("address_address");
$form->add_multi_edit("street", array("address_street", "address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(40, 6), array(), 0, '', '', array(40, 5));
$form->add_edit("address_address2", "Additional Address Info");

$form->add_list("address_country", "Country*",
    "select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);

$form->add_edit("address_zip", "Zip");
$form->add_list("address_place_id", "City*", $sql_places, NOTNULL | SUBMIT);
$form->add_edit("address_place", "", DISABLED);

$form->add_section("Communication");

$form->add_hidden("address_phone");
$form->add_multi_edit("phone_number", array("address_phone_country", "address_phone_area", "address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($country_phone_prefix, '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

$form->add_hidden("address_mobile_phone");
$form->add_multi_edit("mobile_phone_number", array("address_mobile_phone_country", "address_mobile_phone_area", "address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($country_phone_prefix, '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

$form->add_edit("address_email", "Email");
$form->add_edit("address_contact_name", "Contact Name");
$form->add_edit("address_website", "Website");

$form->add_section("Other information");
//$form->add_list("address_currency", "Currency", "select currency_id, currency_symbol from currencies order by currency_symbol");

$form->add_hidden("address_currency", 1);

$form->add_list("address_type", "Address Type",
    "select address_type_id, address_type_name from address_types  where address_type_id in (1,7) order by address_type_name");

$form->add_section();
//$form->add_checkbox("address_canbefranchisor", "Can Be Franchisor", false);
$form->add_edit("address_company_is_partner_since", "Company is partner since", 0, "", TYPE_DATE);
$form->add_checkbox("address_canbefranchisee", "can own POS locations");
$form->add_checkbox("address_canbefranchisee_worldwide", "can have worldwide projects");
$form->add_checkbox("address_canbejointventure", "Is a Joint Venture Partner");
$form->add_checkbox("address_canbecooperation", "Is a Cooperation Partner");
$form->add_checkbox("address_showinposindex", "Show Address in POS Index");

$form->add_comment("Please indicate the corresponding client address in case this address is a POS owner address.");

$sql_addresses = "select address_id, concat(country_name, ': ', address_company) as company from addresses " .
                 "left join countries on country_id = address_country " .
				 "where address_type = 1  order by country_name, address_company";

$form->add_list("address_parent", "Parent*",$sql_addresses);

$form->add_section();

$form->add_checkbox("address_active", "Address in Use", true);
$form->add_checkbox("address_swatch_retailer", "Company is a present Tissot retailer");
$form->add_checkbox("address_is_independent_retailer", "Company is an independent retailer");
$form->add_checkbox("address_checked", "Address checked", true);

$form->add_section('Merchandising Planning');
$form->add_checkbox("address_involved_in_planning", "can order Tissot merchandising material", false, 0, 'Merchandising');

$form->add_checkbox("address_can_own_independent_retailers", "can own independent retailers", false, 0, 'Retailers');
$form->add_checkbox("address_is_internal_address", "Address is an internal Address for MPS", false, 0, 'Internal Address');

$form->add_label("address_mps_customernumber", "MPS Customer Number");
$form->add_label("address_mps_shipto", "MPS Ship To Number");

$form->add_section('Launch Plan');
$form->add_checkbox("address_involved_in_lps", "can order Tissot LPS material", false, 0, 'Launch Plan');


if(!array_key_exists("address_added_by", $tracking_data))
{
	$form->add_hidden("address_added_by", user_id());
}
elseif(id() > 0) {
	$form->add_hidden("address_last_modified_by", user_id());
	$form->add_hidden("address_last_modified", date("Y-m-d H:i:s"));
}

if(array_key_exists("address_added_by", $tracking_data) and $tracking_data["address_added_by"] != "")
{
	$form->add_section("Tracking infos");
	$form->add_label("added_by", "Address created by", 0, $tracking_data["address_added_by"]);
	$form->add_label("modified_by", "Last modifed by", 0, $tracking_data["address_last_modified_by"]);

	if($address_active == 0 and array_key_exists("address_set_to_inactive_by", $tracking_data) and $tracking_data["address_set_to_inactive_by"] != "")
	{
		$form->add_label("set_to_inactive", "Address set to inactive by", 0, $tracking_data["address_set_to_inactive_by"]);
	}
}

$form->add_button("save", "Save");
$form->add_button(FORM_BUTTON_DELETE, "Delete");
$form->add_button("back", "Back");

//$form->add_validation("is_shortcut({address_shortcut})", "The shortcut is invalid. It may only contain lower case characters a-z, digits and the underscore");
//$form->add_validation("is_email_address({address_email})", "The email address is invalid.");
//$form->add_validation("is_web_address({address_website})", "The website url is invalid.");


// Populate form and process button clicks

$form->populate();

if($form->value("address_canbefranchisee") == 1)
{
	$form->add_validation("{address_parent}", "You must indicate a parent address for a POS owner address.");
}

$form->process();

if($form->button("address_place_id"))
{
	$sql = "select place_name from places where place_id = " . dbquote($form->value("address_place_id"));
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
    {
		$form->value("address_place", $row["place_name"]);
	}
}
elseif($form->button("address_country"))
{
	$form->value("address_place", "");
	$form->value("address_place_id", "");
	$form->value("address_zip", "");
}
elseif($form->button("back"))
{
	redirect("addresses3rd_inactive.php?country=" . param("country") . "&type=" . param("type"). "&searchterm=" . param("searchterm"));
}
elseif($form->button("save"))
{
	
	$form->value("address_phone", $form->unify_multi_edit_field($form->items["phone_number"]));
	$form->value("address_mobile_phone", $form->unify_multi_edit_field($form->items["mobile_phone_number"]));

	$form->add_validation("{address_phone} != '' or {address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");
	
	if($form->validate())
	{

		$form->value("address_address", $form->unify_multi_edit_field($form->items["street"], get_country_street_number_rule($form->value("address_country"))));

		
		$form->save();
	}
}


// Render page

$page = new Page("addresses");

$page->header();
$page->title(id() ? "Edit Third Party Address" : "Add Third Party Address");
$form->render();
$page->footer();

?>