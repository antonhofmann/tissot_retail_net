<?php
/********************************************************************

    regions.php

    Lists regions for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-08-19
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("region.php");

$list = new ListView("select region_id, region_name, IF(region_active = 1, 'yes', 'no') as active from regions");

$list->set_entity("regions");
$list->set_order("region_name");
$list->set_filter("region_active = 1");

$list->add_column("region_name", "Name", "region.php", LIST_FILTER_FREE);
$list->add_column("active", "Active");

$list->add_button(LIST_BUTTON_NEW, "New", "region.php");
$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");

$list->process();

$page = new Page("regions");

$page->header();
$page->title("Supplied Regions");
$list->render();
$page->footer();

?>