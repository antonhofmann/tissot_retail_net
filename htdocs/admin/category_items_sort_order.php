<?php
/********************************************************************

    category_items_sort_order.php

    edit the sort order of items

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-07-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-07-15
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
register_param("category_id");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get category_name
$sql = "select category_name, category_product_line from categories where category_id = " . param("category_id"); 
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{            
            $category_name = $row["category_name"];
            $product_line = $row["category_product_line"];
}
else
{
    $category_name = "";
}


// get product line name
$sql = "select product_line_name from product_lines where product_line_id = " . $product_line; 
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{            
            $product_line_name = $row["product_line_name"];
}
else
{
    $product_line_name = "";
}


/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("categories", "category");

$form->add_section();
$form->add_hidden("category_id", param("category_id"));
$form->add_label("product_line", "Product Line", "", $product_line_name);
$form->add_label("category", "Category", "", $category_name);


/********************************************************************
    Create List for Catalog Items
*********************************************************************/ 
$sql = "select category_item_id, item_code, item_name, category_item_priority ".
       "from category_items " .
       "left join items on category_item_item = item_id ";

$list_filter = "category_item_category = " . param("category_id");


// get sortorders
$sortorder = array();
$sql_category = $sql . " where " . $list_filter;
$res = mysql_query($sql_category) or dberror($sql_category);
while ($row = mysql_fetch_assoc($res))
{            
    $sortorder[$row["category_item_id"]] = $row["category_item_priority"];
}

$list = new ListView($sql);

$list->add_hidden("category_id", param("category_id"));
$list->set_entity("category_items");
$list->set_order("category_item_priority");
$list->set_filter($list_filter);

$list->add_column("item_code", "Code", "", LIST_FILTER_FREE, COLUMN_NO_WRAP);
$list->add_column("item_name", "Name", "", LIST_FILTER_FREE);
$list->add_edit_column("category_item_priority", "Sort Order", 5, "", $sortorder);

$list->add_button("save", "Save");
$list->add_button(LIST_BUTTON_BACK, "Back");

$list->populate();
$list->process();


if ($list->button("save"))
{
    foreach ($list->values("category_item_priority") as $key=>$value)
    {
        // update record
        if ($value >0)
        {
            $category_item_fields = array();
    
            $category_item_fields[] = "category_item_priority = " . $value;

            $value1 = "current_timestamp";
            $project_fields[] = "date_modified = " . $value1;
    
            if (isset($_SESSION["user_login"]))
            {
                $value1 = dbquote($_SESSION["user_login"]);
                $project_fields[] = "user_modified = " . $value1;
            }
       
            $sql = "update category_items set " . join(", ", $category_item_fields) . " where category_item_id = " . $key;
            mysql_query($sql) or dberror($sql);
        }
    }
}


$form->populate();
$form->process();


$page = new Page("items");

$page->header();
$page->title("Edit Sort Order of Category Items");
$form->render();
$list->render();
$page->footer();

?>
