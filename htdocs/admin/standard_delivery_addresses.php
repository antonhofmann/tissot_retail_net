<?php
/********************************************************************

    standard_delivery_addresses.php

    Lists standard delivery addresses.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-03-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-03-22
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("standard_delivery_address.php");


/********************************************************************
    prepare all data needed
*********************************************************************/



/********************************************************************
    Create List
*********************************************************************/ 

$list = new ListView("select delivery_address_id, delivery_address_company, delivery_address_zip,  " .
                     "    delivery_address_place, country_name, address_shortcut, address_company " .
                     "from standard_delivery_addresses " .
					 "left join addresses on address_id = delivery_address_address_id " . 
					 "left join countries on address_country = country_id");

$list->set_entity("standard_delivery_addresses");
$list->set_order("address_shortcut,delivery_address_company");

$list->add_column("address_shortcut", "Shortcut", "standard_delivery_address.php");
$list->add_column("address_company", "");
$list->add_column("delivery_address_company", "Company");
$list->add_column("delivery_address_zip", "Zip");
$list->add_column("delivery_address_place", "City");
$list->add_column("country_name", "Country");

$list->add_button(LIST_BUTTON_NEW, "New", "standard_delivery_address.php");

/********************************************************************
    Populate and process button clicks
*********************************************************************/ 
$list->process();


/********************************************************************
   Create Page
*********************************************************************/ 
$page = new Page("standard_delivery_addresses");

$page->header();
$page->title("Standard Delivery Adresses");
$list->render();
$page->footer();

?>