<?php
/********************************************************************

    places.php

    Lists places for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-07-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-07-12
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("place.php");


$list_filter = "";

if(param("country_filter") > 1)
{
	$list_filter .=	" place_country = " . param("country_filter") . " ";
}
elseif(array_key_exists("country_filter", $_SESSION) and $_SESSION["country_filter"] > 0)
{
	$list_filter .=	" place_country = " . $_SESSION["country_filter"] . " ";
}
else
{
	$list_filter .=	" place_country = 1 ";
}

$country = 1;
if( param("country_filter") > 0)
{
	$country =  param("country_filter");
}
elseif(array_key_exists("country_filter", $_SESSION) and $_SESSION["country_filter"] > 0)
{
	$country = $_SESSION["country_filter"];
	unset($_SESSION["country_filter"]);
}


/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("places", "City");

$form->add_section("List Filter Selection");

$form->add_list("country_filter", "Country",
    "select country_id, country_name from countries order by country_name", SUBMIT | NOTNULL, $country);


/********************************************************************
    Create List
*********************************************************************/ 

$sql = "select place_id, place_country, place_name, province_canton, province_region from places " . 
       "left join provinces on province_id = place_province";

$list = new ListView($sql);

$list->set_entity("places");
$list->set_order("place_name");
$list->set_filter($list_filter);

$list->add_column("place_name", "City", "place.php?country_filter=" . param("country_filter"));
$list->add_column("province_canton", "Province", "");
$list->add_column("province_region", "Region");

$list->add_button(LIST_BUTTON_NEW, "New", "place.php?country_filter=" . param("country_filter"));



/********************************************************************
    Populate and process button clicks
*********************************************************************/ 
$form->populate();
$list->process();


/********************************************************************
   Create Page
*********************************************************************/ 
$page = new Page("places");

$page->header();
$page->title("Cities");
$form->render();
$list->render();
$page->footer();

?>
