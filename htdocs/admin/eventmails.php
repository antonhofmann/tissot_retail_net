<?php
/********************************************************************

    eventmails.php

    Lists notification reciepienst for system events.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-12-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-12-18
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("eventmail.php");


$sql_users = "select concat(user_name, ' ', user_firstname) as name from users order by user_name, user_firstname";

$sql = "select eventmail_id, eventmail_code, eventmail_description, " .
       "concat(users01.user_name, ' ', users01.user_firstname) as user01, " .
	   "concat(users02.user_name, ' ', users02.user_firstname) as user02, " . 
	   "concat(users03.user_name, ' ', users03.user_firstname) as user03, " . 
	   "concat(users04.user_name, ' ', users04.user_firstname) as user04 " . 
       "from eventmails " .
	   "left join users as users01 on eventmail_user01 = users01.user_id " . 
	   "left join users as users02 on eventmail_user02 = users02.user_id " . 
	   "left join users as users03 on eventmail_user03 = users03.user_id " . 
	   "left join users as users04 on eventmail_user04 = users04.user_id ";


$list = new ListView($sql);

$list->set_entity("eventmails");
$list->set_order("eventmail_code");

$list->add_column("eventmail_code", "Code", "eventmail.php");
$list->add_column("eventmail_description", "Description");
$list->add_column("user01", "Recipient 1");
$list->add_column("user02", "Recipient 2");
$list->add_column("user03", "Recipient 3");
$list->add_column("user04", "Recipient 4");

//$list->add_button(LIST_BUTTON_NEW, "New", "pos_type_notification.php");
//$list->add_button("replace", "Replace Recipients", "Repleca Recipients");
$list->process();


if($list->button("replace") ) {

	redirect("eventmail_replace_user.php");

}

$page = new Page("eventmails");

$page->header();
$page->title("Eventmails");

$list->render();
$page->footer();

?>
