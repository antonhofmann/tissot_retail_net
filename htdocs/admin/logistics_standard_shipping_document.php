<?php
/********************************************************************

    logistics_standard_shipping_document.php

    Creation and mutation of standard shipping documents.

    Created by:     Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date created:   2015-01-06
    Modified by:    Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-06
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(has_access("can_edit_catalog") 
  or has_access("can_edit_logistics_system_data"))
{

}
else
{
	check_access("can_edit_catalog");
}


/****************************************************************************
save data
****************************************************************************/
if(param("saved") == 1 and id() > 0)
{

	$sql = "delete from address_shipping_documents " . 
		   " where address_shipping_document_document_id = " . dbquote(id());

	$result = mysql_query($sql) or dberror($sql);
	
	foreach($_POST as $field=>$value)
	{
		

		if(strpos($field, "addresses_address_shipping_document_address_id") != false)
		{
			$tmp = explode("__addresses_address_shipping_document_address_id_", $field);
			$address_shipping_document_address_id = $tmp[1];
			$sql = "insert into address_shipping_documents " . 
				   "(address_shipping_document_address_id, address_shipping_document_document_id, user_created, date_created) VALUES (" . 
				   $address_shipping_document_address_id . ", " .
				   id() . ", " .
				   dbquote(user_id()) . ", " .
					dbquote(date("Y-m-d H:i:s")) . ")";
			$result = mysql_query($sql) or dberror($sql);
		}
	}
}


/****************************************************************************
create form
****************************************************************************/

$form = new Form("standard_shipping_documents", "standard_shipping_document");

$form->add_hidden("saved", 1);
$form->add_section();
$form->add_edit("standard_shipping_document_name", "Name*", NOTNULL | UNIQUE);
$form->add_checkbox("standard_shipping_document_used_by_forwarders", "this document is needed by forwarders", false, 0, "Forwarders");
$form->add_multiline("standard_shipping_document_description", "Description", 4);

$form->add_button("save", "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button("delete", "Delete", "", OPTIONAL);

$form->populate();
$form->process();


if($form->button("save"))
{
	if($form->validate())
	{
		$form->save();
		$form->message("Your data has been saved.");
	}
}
elseif($form->button("delete"))
{
	$sql = "delete from standard_shipping_documents " . 
		   " where standard_shipping_document_id = " . dbquote(id());

	$result = mysql_query($sql) or dberror($sql);

	$sql = "delete from address_shipping_documents " . 
		   " where address_shipping_document_document_id = " . dbquote(id());

	$result = mysql_query($sql) or dberror($sql);

	redirect("logistics_standard_shipping_documents.php");
}



/****************************************************************************
create List of client addresses
****************************************************************************/

$sql_u = "select address_shipping_document_address_id " . 
         " from address_shipping_documents ". 
		 " where address_shipping_document_document_id = " . dbquote(id());


$docvalues = array();
$res = mysql_query($sql_u) or dberror($sql_u);
while($row = mysql_fetch_assoc($res))
{
	$docvalues[$row["address_shipping_document_address_id"]] = 1;
}


$sql = "select address_id, address_company, address_zip,  " .
		 "    country_name, address_type_name, client_type_code, salesregion_name " .
		 "from addresses " .
		 " left join address_types on address_type_id = address_type " .
		 " left join client_types on client_type_id = address_client_type " .
		 " left join countries on address_country = country_id " . 
		 " left join salesregions on salesregion_id = country_salesregion";

$list_filter = "address_id <> 5 and address_active = 1 and address_type = 1 ";


$sql_u = $sql . " where " . $list_filter;

$position_ids = array();
$res = mysql_query($sql_u) or dberror($sql_u);
while($row = mysql_fetch_assoc($res))
{
	$position_ids[$row["address_id"]] = "__addresses_address_shipping_document_address_id_" . $row["address_id"];
}


$link = "javascript:select_all_positions();";
$c = '<a href="javascript:void(0);" onclick="' .$link . '"><span class="fa fa-check-square-o checker"></span></a><span id="selector"> </span>';


$list = new ListView($sql);
$list->set_title("The shipping document is used for the following clients");
$list->set_entity("addresses");
$list->set_group("salesregion_name");
$list->set_order("country_name, address_company");
$list->set_filter($list_filter);


$list->add_column("country_name", "Country");
$list->add_column("client_type_code", "Client Type");
$list->add_column("address_company", "Company");
$list->add_checkbox_column("address_shipping_document_address_id", $c, COLUMN_UNDERSTAND_HTML, $docvalues);

$list->populate();
$list->process();



$page = new Page("logistics");
require "include/logistics_page_actions.php";

$page->header();
$page->title(id() ? "Edit Standard Shipping Document" : "Add Standard Shipping Document");
$form->render();

if(id() > 0)
{
	$list->render();
}



?>
<script language="javascript">
	function select_all_positions()
	{
		var selector = "#selector";

		if($(selector).html() == '  ')
		{
			<?php
				foreach($position_ids as $key=>$id)
				{
					echo "$('#" . $id . "').attr('checked', false);";
				}
			?>
			$(selector).html(' ');
		}
		else
		{	<?php
				foreach($position_ids as $key=>$id)
				{
					echo "$('#" . $id . "').attr('checked', true);";
				}
			?>
			$(selector).html('  ');
		}
	}
</script>

<?php

$page->footer();

?>