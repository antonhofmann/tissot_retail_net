<?php
/********************************************************************

    supervisors.php

    List supervisors

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2010-11-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2010-11-15
    Version:        1.0.0

    Copyright (c) 2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("supervisor.php");

$list = new ListView("select supervisingteam_id, user_firstname, user_name, user_email " .
                     "from supervisingteam left join users on user_id = supervisingteam_user");

$list->set_entity("supervisingteam");
$list->set_order("user_name, user_firstname");

$list->add_column("user_name", "Name", "supervisor.php" );
$list->add_column("user_firstname", "Firstname", "");
$list->add_column("user_email", "Email", "");

$list->add_button(LIST_BUTTON_NEW, "New", "supervisor.php");


$list->process();

$page = new Page("supervisingteam");

$page->header();
$page->title("Retail Supervising Team");
$list->render();
$page->footer();

?>
