<?php
/********************************************************************

    security_unlockedips.php

    Lists unlocked locked IPS for unlocking .

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-10-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-10-12
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_unlock_ips");

$sql = "select user_tracking_id, user_tracking_track,  " . 
       "user_tracking.date_created as date, " . 
	   "concat(user_name, ' ', user_firstname) as username " . 
       "from user_tracking " . 
       "left join users on user_id = user_tracking_user " . 
	   " where user_tracking_track like '%unblocked IP%' " . 
	   " order by  user_tracking_id DESC " . 
	   " limit 0,100";



$list = new ListView($sql);

$list->set_entity("user_tracking");

$list->add_column("user_tracking_track", "IP Address");
$list->add_column("username", "By User");
$list->add_column("date", "Date:Time");

$list->process();

$page = new Page("security");

$page->header();
$page->title("Unlocked IP Addresses (latest 100)");

$list->render();
$page->footer();

?>
