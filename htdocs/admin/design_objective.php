<?php
/********************************************************************

    design_objective.php

    Creation and mutation of design objective records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-05
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2010-12-03
    Version:        2.0.0

    Copyright (c) 2002-2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("design_objective_items", "design objective");

$form->add_section();
$form->add_edit("design_objective_item_name", "Name", NOTNULL);
$form->add_hidden("design_objective_item_priority");

if (param("group"))
{
    $sql = "select design_objective_group_postype from design_objective_groups where design_objective_group_id = " . param("group");
    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);

    $form->add_list("design_objective_item_group", "Group",
        "select design_objective_group_id, design_objective_group_name " .
        "from design_objective_groups " .
        "where design_objective_group_postype = " . $row[0] . " " .
        "order by design_objective_group_name",
        NOTNULL, param("group"));
}
else
{
    $sql = "select design_objective_group_id, concat(postype_name, ': ', design_objective_group_name) " .
			"from design_objective_groups " . 
		    "left join postypes on design_objective_group_postype = postype_id " .
		    " where postype_id > 0 " . 
			"order by postype_name, design_objective_group_name";
	$form->add_list("design_objective_item_group", "Group",$sql,NOTNULL);
}



$form->add_checkbox("design_objective_item_isstandard", "Add to projects by default");
$form->add_checkbox("design_objective_item_active", "Active", true);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();

if (!$form->value("design_objective_item_priority"))
{
    $sql = "select max(design_objective_item_priority) from design_objective_items";
    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);
    $form->value("design_objective_item_priority", $row[0] + 1);
}

$form->process();

$page = new Page("design_objectives");
$page->header();
$page->title(id() ? "Edit Design Objective" : "Add Design Objective");
$form->render();
$page->footer();

?>