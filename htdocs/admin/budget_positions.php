<?php
/********************************************************************

    budget_positions.php

    Lists standard budget positions for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-09-26
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("budget_position.php");

$list = new ListView("select exclusion_notification_id, exclusion_notification_code, exclusion_notification_description, item_type_name " .
                     "from exclusion_notifications left join item_types on item_type_id = exclusion_notification_type_id",
                     LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("exclusion_notifications");

$list->add_column("item_type_name", "Type");
$list->add_column("exclusion_notification_code", "Code", "budget_position.php");
$list->add_column("exclusion_notification_description", "Description");

$list->add_button(LIST_BUTTON_NEW, "New", "budget_position.php");

$list->process();

$page = new Page("budget_positions");

$page->header();
$page->title("Exclusions And Notifications");
$list->render();
$page->footer();

?>