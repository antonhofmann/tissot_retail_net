<?php
/********************************************************************

    product_lines.php

    Lists product lines for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-07-14
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_browse_catalog_in_admin");
set_referer("product_line.php");

$list = new ListView("select product_line_id, product_line_name, product_line_priority, " .
                     "    if (product_line_visible, 'yes', 'no') as product_line_visible, " .
                     "    if (product_line_budget, 'yes', 'no') as product_line_budget, " .
                     "    if (product_line_clients, 'yes', 'no') as product_line_clients, " .
					 "    if (product_line_posindex, 'yes', 'no') as product_line_posindex, " .
					 "    if (product_line_mis, 'yes', 'no') as product_line_mis " .
                     "from product_lines",
                     LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("product lines");
if(has_access("can_edit_catalog"))
{
	$list->set_sorter("product_line_priority");
}
else
{
	$list->set_order("product_line_priority");
}

$list->add_column("product_line_name", "Name", "product_line.php", LIST_FILTER_FREE);
$list->add_column("product_line_visible", "Visible in Catalog", "", LIST_FILTER_LIST, array(0 => "No", 1 => "Yes"));
$list->add_column("product_line_budget", "in Projects", "", LIST_FILTER_LIST, array(0 => "No", 1 => "Yes"));
$list->add_column("product_line_clients", "to Clients", "", LIST_FILTER_LIST, array(0 => "No", 1 => "Yes"));
$list->add_column("product_line_posindex", "in POS Index", "", LIST_FILTER_LIST, array(0 => "No", 1 => "Yes"));
$list->add_column("product_line_mis", "in MIS", "", LIST_FILTER_LIST, array(0 => "No", 1 => "Yes"));

//$list->add_column("product_line_priority", "prio", "", LIST_FILTER_FREE);

if(has_access("can_edit_catalog"))
{
	$list->add_button(LIST_BUTTON_NEW, "New", "product_line.php");
}

//$list->add_button(LIST_BUTTON_FILTER, "Filter");
//$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");

$list->process();

$page = new Page("product_lines");

$page->header();
$page->title("Product Lines");
$list->render();
$page->footer();

?>
