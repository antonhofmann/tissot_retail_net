<?php
/********************************************************************

    design_brief_positions.php

    Lists design brief positions for editing .

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2017-12-11
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2017-12-11
    Version:        1.0.0

    Copyright (c) 2017, TISSOT SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("design_brief_position.php");



$types[1] = 'Input Field for Text';
$types[2] = 'Input Field for Integer';
$types[3] = 'Input Field for Decimal Number';
$types[4] = 'Input Field for Date Value';
$types[5] = 'Text Area';
$types[6] = 'Selection of Possible Values';
$types[7] = 'Multiple Choice';



$sql = "select design_brief_position_id, design_brief_position_sort, 
       design_brief_group_name, design_brief_group_sort,
	   concat(design_brief_group_sort, ' ', design_brief_group_name) as dbgroup,
	   design_brief_position_text, if(design_brief_position_mandatory = 1, 'yes', 'no') as design_brief_position_mandatory, design_brief_position_type, 
	   if(design_brief_position_store = 1, 'yes', 'no') as boutique,
	   if(design_brief_position_sis = 1, 'yes', 'no') as sis,
	   if(design_brief_position_kiosk = 1, 'yes', 'no') as kiosk
       from design_brief_groups 
	   left join design_brief_positions on design_brief_position_group_id = design_brief_group_id";



$ptypes = array();
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$ptypes[$row['design_brief_position_id']] = $types[$row['design_brief_position_type']]; 
}

$list = new ListView($sql);

$list->set_entity("design_brief_positions");
$list->set_group("dbgroup");
$list->set_order("design_brief_group_sort, design_brief_position_sort");

$list->add_column("design_brief_position_sort", "Sort");
$list->add_column("design_brief_position_text", "Position", "design_brief_position.php");
$list->add_column("design_brief_position_mandatory", "Mandatory", "", "", "", COLUMN_ALIGN_CENTER);
$list->add_column("boutique", "Boutique", "", "", "", COLUMN_ALIGN_CENTER);
$list->add_column("sis", "SIS", "", "", "", COLUMN_ALIGN_CENTER);
$list->add_column("kiosk", "Kiosk", "", "", "", COLUMN_ALIGN_CENTER);
$list->add_text_column("type", "Type", COLUMN_UNDERSTAND_HTML, $ptypes);

$list->add_button(LIST_BUTTON_NEW, "New", "design_brief_position.php");

$list->process();

$page = new Page("design_brief_positions");

$page->header();
$page->title("Design Briefing Positions");
$list->render();
$page->footer();

?>
