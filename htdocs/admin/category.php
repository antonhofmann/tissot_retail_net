<?php
/********************************************************************

    category.php

    Creation and mutation of category records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-05
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_browse_catalog_in_admin");
set_referer("item.php");
set_referer("category_items.php");
set_referer("category_file.php");
set_referer("category_items_sort_order.php");

// Build form

$form = new Form("categories", "category");

$form->add_hidden("category_priority");

$form->add_section();
$form->add_edit("category_name", "Name", NOTNULL);
$form->add_list("category_product_line", "Product Line",
    "select product_line_id, product_line_name " .
    "from product_lines " .
    "order by product_line_name", NOTNULL, param("product_line"));
$form->add_edit("category_cost_unit_number", "Cost Unit Number");

$form->add_section();
$form->add_checkbox("category_budget", "Appears in budget");
$form->add_checkbox("category_catalog", "Appears in catalog");
$form->add_checkbox("category_useconfigurator", "Use POS Configurator");

$form->add_section();
$form->add_checkbox("category_not_in_use", "not in use");


if(has_access("can_edit_catalog"))
{
	$form->add_subtable("items", "<br />Items", "item.php", "Code",
		"select category_item_id, item_id, item_code as Code, item_name as Name, item_price as Price, " .
		"    item_type_name as Type " .
		"from category_items left join items on category_item_item = item_id " .
		"    left join item_types on item_type = item_type_id " .
		"where category_item_category = " . id() . " " .
		"order by category_item_priority, item_code", "", "Remove");

	$form->add_subtable("files", "<br />Files", "category_file.php", "Title",
		"select category_file_id, category_file_title as Title, file_purpose_name as Purpose, " .
		"    file_type_name as Type, category_file_path as Path " .
		"from category_files left join file_types on category_file_type = file_type_id " .
		"    left join file_purposes on category_file_purpose = file_purpose_id " .
		"where category_file_category = " . id() . " " .
		"order by category_file_title");
}
else
{
	$form->add_subtable("items", "<br />Items", "item.php", "Code",
		"select category_item_id, item_id, item_code as Code, item_name as Name, item_price as Price, " .
		"    item_type_name as Type " .
		"from category_items left join items on category_item_item = item_id " .
		"    left join item_types on item_type = item_type_id " .
		"where category_item_category = " . id() . " " .
		"order by category_item_priority, item_code");
}



if(has_access("can_edit_catalog"))
{
	$form->add_button(FORM_BUTTON_SAVE, "Save");
	$form->add_button(FORM_BUTTON_BACK, "Back");
	$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
	$form->add_button("assign_items", "Assign Items", "category_items.php?category=" . id(), OPTIONAL);
	$form->add_button("add_file", "Add File", "category_file.php?category=" . id(), OPTIONAL);
	$form->add_button("edit_priority", "Edit Sort Order of Items", 
					  "category_items_sort_order.php?category_id=" . id(), OPTIONAL);
}
else
{
	$form->add_button(FORM_BUTTON_BACK, "Back");
}
// Populate form and process button clicks

$form->populate();

if (!$form->value("category_priority"))
{
    $sql = "select max(category_priority) from categories";
    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);
    $form->value("category_priority", $row[0] + 1);
}

$form->process();

// Render page

$page = new Page("categories");

$page->header();
if(has_access("can_edit_catalog"))
{
	$page->title(id() ? "Edit Category" : "Add Category");
}
else
{
	$page->title("View Category");
}
$form->render();
$page->footer();

?>