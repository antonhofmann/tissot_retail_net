<?php
/********************************************************************

   file_category.php

    Creation and mutation of file category records.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2015-06-24
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2015-06-24
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("order_file_categories", "order_file_category");

$form->add_section();

$form->add_list("order_file_category_type", "Context",
    array(1=>'Catalogue Orders', 2=>'Projects'), NOTNULL);
$form->add_edit("order_file_category_name", "Name", NOTNULL);
$form->add_edit("order_file_category_priority", "Priority", NOTNULL);
$form->add_checkbox("order_file_category_attach_in_mails", "add files from this category as attachments to mails",0, 0, "Uploads");



$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

if($form->button(FORM_BUTTON_SAVE))
{
	redirect("file_categories.php");
}

$page = new Page("order_file_categories");
$page->header();
$page->title(id() ? "Edit File Category" : "Add File Category");
$form->render();
$page->footer();

?>