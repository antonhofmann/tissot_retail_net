<?php
/********************************************************************

    category_items.php

    Lists items to be assigned to a category

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-12
    Modified by:    anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-04
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$list = new ListView("select item_id, item_code, item_name, item_price, item_type_name, item_visible, item_active " .
                     "from items left join item_types on item_type = item_type_id", LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("items");
$list->set_filter("item_active = 1");
$list->set_order("item_code");
$list->set_group("item_type_name");
$list->set_checkbox("category_items", "categories", param("category"));

$list->add_hidden("category", param("category"));
$list->add_column("item_code", "Code", "", LIST_FILTER_FREE);
$list->add_column("item_visible", "Visible", "", LIST_FILTER_FREE);
$list->add_column("item_active", "Active", "", LIST_FILTER_FREE);
$list->add_column("item_name", "Name", "", LIST_FILTER_FREE);
$list->add_column("item_price", "Price", "", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_SAVE, "Save");
$list->add_button(LIST_BUTTON_BACK, "Back");

$list->process();

$page = new Page("categories");

$sql = "select category_name from categories where category_id = " . param("category");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_row($res);

$page->header();
$page->title("Assign Items To Category \"" . htmlspecialchars($row[0]) . "\"");
$list->render();
$page->footer();
?>
