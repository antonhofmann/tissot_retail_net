<?php
/********************************************************************

    country.php

    Creation and mutation of country records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-30
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-09-17
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$time_formats = array("24"=>"24 hours", "12"=>"12 hours");

$form = new Form("countries", "country");

$form->add_edit("country_code", "Country Code", NOTNULL);
$form->add_edit("country_name", "Name", NOTNULL | UNIQUE);


$form->add_list("country_hemisphere_id", "Hemisphere", 
    "select hemisphere_id,hemisphere_name from hemispheres order by hemisphere_name");

$form->add_list("country_region", "Supplying Region", 
    "select region_id, region_name from regions where region_active = 1 order by region_name");

$form->add_list("country_salesregion", "Geographical Region", 
    "select salesregion_id, salesregion_name from salesregions order by salesregion_name");

$form->add_list("country_currency", "Currency", 
    "select currency_id,currency_symbol from currencies order by currency_symbol");

$form->add_edit("country_store_locator_id", "Store Locator ID", NOTNULL | UNIQUE);
$form->add_edit("country_phone_prefix", "Phone Prefix Country", NOTNULL);
$form->add_edit("country_phone_prefix2", "Phone Prefix2 Country");
$form->add_edit("country_phone_prefix3", "Phone Prefix3 Country");


$form->add_list("country_timeformat", "Time Format", $time_formats);

$form->add_checkbox("country_nr_before_streename", "Street numbers are shown before street names", false, 0, 'Address Format');


$form->add_edit("country_iso3166", "ISO Code 3361", NOTNULL | UNIQUE);
$form->add_checkbox("country_used_in_cross_trade_shipments", "Country is used in cross trade shipments by SAP", false, 0, 'Logistics');





$form->add_checkbox("country_provinces_complete", "The list of provinces for this country is complete", false, 0, 'Provinces');


$form->add_section("Market's Websites");
$form->add_edit("country_website_swatch", "URL for Tissot");



$form->add_section("Spoken Languages");
$form->add_checklist("country_languages", "", "country_languages",
    "select language_id, language_name from languages order by language_name");

$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
$form->add_button(FORM_BUTTON_BACK, "Back");
//$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button("save", "Save");

$form->populate();
$form->process();

if($form->button("save"))
{
	if($form->validate())
	{
		$form->save();
		redirect("countries.php");
	}
}



$page = new Page("countries");
$page->header();
$page->title(id() ? "Edit Country" : "Add Country");
$form->render();
$page->footer();

?>