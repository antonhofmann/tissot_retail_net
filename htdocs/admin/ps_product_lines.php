<?php
/********************************************************************

    ps_product_lines.php

    Lists project sheet product lines for editing .

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-27
    Version:        1.0.0

    Copyright (c) 2004, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("ps_product_line.php");

$sql = "select ps_product_line_id, ps_product_line_name from ps_product_lines ";

$list = new ListView($sql);

$list->set_entity("ps_product_lines");
$list->set_order("ps_product_line_name");

$list->add_column("ps_product_line_name", "Product Line Name", "ps_product_line.php");

$list->add_button(LIST_BUTTON_NEW, "New", "ps_product_line.php");

$list->process();

$page = new Page("ps_product_lines");

$page->header();
$page->title("Project Sheet Product Lines");
$list->render();
$page->footer();

?>
