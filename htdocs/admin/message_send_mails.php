<?php
/********************************************************************

    message_send_mails.php

    Send mails to inform checked notification recipients about
    a message.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2007-11-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2007-11-14
    Version:        1.0.0

    Copyright (c) 2007, Swatch AG, All Rights Reserved.

*********************************************************************/
$server_address = APPLICATION_URL;

$reciepients = array();
$reciepientemails = array();

$sql = "select user_email as sender, " .
       "concat(user_name, ' ', user_firstname) as user_fullname ".
       "from users ".
       "where user_id = " . user_id();

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
    $mail_sent_to = array();
	$sender_email = $row["sender"];
    $sender_name =  $row["user_fullname"];

    $subject = MAIL_SUBJECT_PREFIX . ": " . $form->items["message_title"]["value"];
    $bodytext = str_replace('"', "'", $form->items["message_text"]["value"]);
    
    //get attachment
	$attachments = array();

    if($form->items["message_file1"]["value"] > 0)
    {
        $link_id = $form->items["message_file1"]["value"];
        $sql_l = "select link_title, link_path from links where link_id = " . $link_id;
        $res_l = mysql_query($sql_l) or dberror($sql_l);
        if ($row_l = mysql_fetch_assoc($res_l))
        {
            $attachments[]  = $row_l["link_path"];
        }
    }
	if($form->items["message_file2"]["value"] > 0)
    {
        $link_id = $form->items["message_file2"]["value"];
        $sql_l = "select link_title, link_path from links where link_id = " . $link_id;
        $res_l = mysql_query($sql_l) or dberror($sql_l);
        if ($row_l = mysql_fetch_assoc($res_l))
        {
            $attachments[]  = $row_l["link_path"];
        }
    }
	if($form->items["message_file3"]["value"] > 0)
    {
        $link_id = $form->items["message_file3"]["value"];
        $sql_l = "select link_title, link_path from links where link_id = " . $link_id;
        $res_l = mysql_query($sql_l) or dberror($sql_l);
        if ($row_l = mysql_fetch_assoc($res_l))
        {
            $attachments[]  = $row_l["link_path"];
        }
    }
	if($form->items["message_file4"]["value"] > 0)
    {
        $link_id = $form->items["message_file4"]["value"];
        $sql_l = "select link_title, link_path from links where link_id = " . $link_id;
        $res_l = mysql_query($sql_l) or dberror($sql_l);
        if ($row_l = mysql_fetch_assoc($res_l))
        {
            $attachments[]  = $row_l["link_path"];
        }
    }

	
    // mails to users, agents and subs
	foreach($role_users  as $role_id=>$users)
    {
		foreach($users as $key=>$user_id)
		{
			if(isset($form->items["r" . $role_id . "_" . $user_id]["value"]))
			{
				if($form->items["r" . $role_id . "_" . $user_id]["value"] == true)
				{
					
					$sql = "select user_id, " .
						   "concat(user_name, ' ' , user_firstname) as username, " .
						   "user_email " .
						   "from users " .
						   "where user_id = " . $user_id;

						$res = mysql_query($sql) or dberror($sql);
						if ($row = mysql_fetch_assoc($res))
						{
							$reciepients[$row["user_id"]] = $row["user_id"];
							$reciepientemails[$row["user_email"]] = $row["username"];
							$mail_sent_to[$role_id] = $role_names[$role_id];
						}
				}
			}

			if(isset($form->items["ragents" . $role_id . "_" . $user_id]["value"]))
			{
				if($form->items["ragents" . $role_id . "_" . $user_id]["value"] == true)
				{
					
					$sql = "select user_id, " .
						   "concat(user_name, ' ' , user_firstname) as username, " .
						   "user_email " .
						   "from users " .
						   "where user_id = " . $user_id;

						$res = mysql_query($sql) or dberror($sql);
						if ($row = mysql_fetch_assoc($res))
						{
							$reciepients[$row["user_id"]] = $row["user_id"];
							$reciepientemails[$row["user_email"]] = $row["username"];
							$mail_sent_to[$role_id] = $role_names[$role_id];
						}
				}
			}


			if(isset($form->items["rsubs" . $role_id . "_" . $user_id]["value"]))
			{
				if($form->items["rsubs" . $role_id . "_" . $user_id]["value"] == true)
				{
					
					$sql = "select user_id, " .
						   "concat(user_name, ' ' , user_firstname) as username, " .
						   "user_email " .
						   "from users " .
						   "where user_id = " . $user_id;

						$res = mysql_query($sql) or dberror($sql);
						if ($row = mysql_fetch_assoc($res))
						{
							$reciepients[$row["user_id"]] = $row["user_id"];
							$reciepientemails[$row["user_email"]] = $row["username"];
							$mail_sent_to[$role_id] = $role_names[$role_id];
						}
				}
			}
		}

	}


	


	if(count($mail_sent_to) > 0) {
	
		$role_text = "P.S: This message was sent to " . implode(', ', $mail_sent_to);
		$bodytext .= "\n\n" . $role_text;

		
	}


	//send mails
	foreach($reciepientemails as $email=>$name) {
		
		$mail = new PHPMailer();
		$mail->Subject = $subject;
		$mail->SetFrom($sender_email, $sender_name);
		$mail->AddReplyTo($sender_email, $sender_name);

		$mail->AddAddress($email, $name);

		$mail->Body = $bodytext;

		foreach($attachments as $key=>$attachment)
		{
			$filepath = $_SERVER["DOCUMENT_ROOT"] . $attachment;
			$mail->AddAttachment($filepath);
			
		}  
		$mail->Send();
	}


	//insert into reciepients table
	foreach($reciepients as $key=>$value)
	{
		$sql = "Insert into message_email_sent (" .
			   "message_email_sent_user, " .
			   "message_email_sent_touser, " .
			   "message_email_sent_message, " . 
			   "date_created) " .
			   "values (" .
			   user_id() . ", " .
			   $key . ", " .
			   id() . ", '" .
			   date("Y-m-d G:i:s") . "')";

		$result = mysql_query($sql) or dberror($sql);

		//echo $sql;

	}
}
?>