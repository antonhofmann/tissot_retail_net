<?php
/********************************************************************

    project_kind.php

    Editdit projectkinds

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-07-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-07-15
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("projectkinds", "project kinds");


$rtc = 0;
$lct = 0;

$sql = "select projectkind_standard_project_manager,  projectkind_standard_logistics_coordinator " .
	   " from projectkinds " .
       " where projectkind_id = 8";

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$rtc = $row["projectkind_standard_project_manager"];
	$lct = $row["projectkind_standard_logistics_coordinator"];
}


$form->add_section();

$form->add_label("projectkind_code", "Code");
$form->add_label("projectkind_name", "Name");
$form->add_comment("Indicate recipient's email on creation of a new project of the above project type.");
$form->add_edit("projectkind_email1", "Recipient 1");
$form->add_edit("projectkind_email2", "Recipient 2");
$form->add_edit("projectkind_email3", "Recipient 3");

$form->add_section("Standard Responsibles");

$form->add_list("projectkind_standard_project_manager", "Standard Project Leader",
	"select DISTINCT user_id, concat(user_name, ' ', user_firstname) " .
	"from user_roles " .
	" left join users on user_id = user_role_user " . 
	"where (user_active = 1 and user_role_role = 3) or user_id = " . dbquote($rtc) . 
	"order by user_name, user_firstname");

/*
$form->add_list("projectkind_standard_logistics_coordinator", "Standard Logistics Coordinator",
	"select DISTINCT user_id, concat(user_name, ' ', user_firstname) " .
	"from user_roles " .
	" left join users on user_id = user_role_user " . 
	"where (user_active = 1 and user_role_role = 2) or user_id = " . dbquote($lct) .
	"order by user_name, user_firstname");
*/


$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->add_validation("is_email_address({projectkind_email1})", "The email address 1 is invalid.");
$form->add_validation("is_email_address({projectkind_email2})", "The email address 2 is invalid.");
$form->add_validation("is_email_address({projectkind_email2})", "The email address 3 is invalid.");


$form->populate();
$form->process();

$page = new Page("project_kinds");

$page->header();
$page->title(id() ? "Edit Project Type - Email Notification" : "Add Project Type - Email Notification");
$form->render();
$page->footer();

?>
