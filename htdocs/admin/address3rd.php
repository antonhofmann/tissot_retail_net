<?php
/********************************************************************

    address.php

    Creation and mutation of address records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-29
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-10-17
    Version:        1.0.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("user.php");
set_referer("invoice_address.php");
set_referer("address_file.php");

// Build form
$country_phone_prefix = '';
if(param("address_country"))
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = " . param("address_country") . " order by place_name";

	$country_phone_prefix = get_country_phone_prefix(param("address_country"));
}
else
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = {address_country} order by place_name";
}



$address_showinposindex = 0;
$sql = "select * from addresses where address_id = " . id();
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$address_showinposindex = $row["address_showinposindex"];
}


$address_active = "";
$tracking_data = array();
$address_modified = "";
if(id())
{
	$sql = "select address_added_by, address_last_modified, address_last_modified_by, address_set_to_inactive_by, address_set_to_inactive_date, date_created, address_active " . 
		   "from addresses " . 
		   "where address_id = " . id();

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res)) 
	{
		
		$address_active = $row["address_active"];
		$tracking_data["address_added_by"] = $row["address_added_by"];
		$tracking_data["address_set_to_inactive_by"] = $row["address_set_to_inactive_by"];
		$tracking_data["address_last_modified_by"] = $row["address_last_modified_by"];

		$date_created["address_added_by"] = $row["date_created"];
		$date_created["address_set_to_inactive_by"] = $row["address_set_to_inactive_date"];
		$date_created["address_last_modified_by"] = $row["address_last_modified"];

		foreach($tracking_data as $key=>$user_id)
		{
			$sql = "select user_name, user_firstname " . 
				   "from users " . 
					"where user_id = " . dbquote($user_id);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) 
			{
				$tracking_data[$key] = $row["user_name"] . " " . $row["user_firstname"] . " " . $date_created[$key];
			}
		}
	}
}


$form = new Form("addresses", "address");

if($address_showinposindex == 1)
{
	$form->add_comment('<a href="/mps/companies/address/' . param("id") . '" target="_blank">Access this Address in MPS</a>');
}

$form->add_hidden("country", param("country"));
$form->add_hidden("type", param("type"));
$form->add_hidden("searchterm", param("searchterm"));

$form->add_section("Name and address");
//$form->add_edit("address_number", "Tissot SAP Customer Code Merchandising");
//$form->add_edit("address_number2", "Tissot SAP Customer Code Retail Furniture");
//$form->add_edit("address_legnr", "SG Legal Number");
$form->add_label("address_sapnr", "SAP Customer Code");

//$form->add_edit("address_shortcut", "Shortcut", NOTNULL);
$form->add_edit("address_company", "Company", NOTNULL, "", TYPE_CHAR, 0, 0, 2, "address_company");
$form->add_edit("address_company2", "", 0, "", TYPE_CHAR, 0, 0, 2, "address_company2");

$form->add_hidden("address_address");

$form->add_multi_edit("street", array("address_street", "address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6), array('',''), 2, "address_address", '', array(40,5));

$form->add_edit("address_address2", "Additional Address Info", 0, "", TYPE_CHAR, 0, 0, 2, "address_address2");

$form->add_list("address_country", "Country*",
    "select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);

$form->add_edit("address_zip", "Zip");
$form->add_list("address_place_id", "City*", $sql_places, NOTNULL | SUBMIT);
$form->add_edit("address_place", "", DISABLED);

$form->add_section("Communication");

$form->add_hidden("address_phone");
$form->add_multi_edit("phone_number", array("address_phone_country", "address_phone_area", "address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($country_phone_prefix, '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


$form->add_hidden("address_mobile_phone");
$form->add_multi_edit("mobile_phone_number", array("address_mobile_phone_country", "address_mobile_phone_area", "address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($country_phone_prefix, '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

$form->add_edit("address_email", "Email");
$form->add_edit("address_contact_name", "Contact Name");
$form->add_edit("address_website", "Website");

$form->add_section("Other information");
//$form->add_list("address_currency", "Currency", "select currency_id, currency_symbol from currencies order by currency_symbol");

$form->add_hidden("address_currency", 1);

$form->add_list("address_type", "Address Type",
    "select address_type_id, address_type_name from address_types  where address_type_id in (1,7) order by address_type_name");

$form->add_section();
//$form->add_checkbox("address_canbefranchisor", "Can Be Franchisor", false);
$form->add_edit("address_company_is_partner_since", "Company is partner since", 0, "", TYPE_DATE);
$form->add_checkbox("address_canbefranchisee", "Can own POS locations");
$form->add_checkbox("address_canbefranchisee_worldwide", "can have worldwide projects");
$form->add_checkbox("address_canbejointventure", "Is a Joint Venture Partner");
$form->add_checkbox("address_canbecooperation", "Is a Cooperation Partner");
$form->add_checkbox("address_showinposindex", "Show Address in POS Index");



$form->add_comment("Please indicate the corresponding client address in case this address is a POS owner address.");

$sql_addresses = "select address_id, concat(country_name, ': ', address_company) as company from addresses " .
                 "left join countries on country_id = address_country " .
				 "where address_active = 1 and address_type = 1  order by country_name, address_company";

$form->add_list("address_parent", "Parent*",$sql_addresses);

$form->add_section();

$form->add_checkbox("address_active", "Address in Use", true);
$form->add_checkbox("address_swatch_retailer", "Company is a present Tissot retailer");
$form->add_checkbox("address_is_independent_retailer", "Company is an independent retailer");
$form->add_checkbox("address_checked", "Address checked", true);


$form->add_section('Merchandising Planning');
$form->add_checkbox("address_involved_in_planning", "can order Tissot merchandising material", false, 0, 'Merchandising');

$form->add_checkbox("address_can_own_independent_retailers", "can own independent retailers", false, 0, 'Retailers');
$form->add_checkbox("address_is_internal_address", "Address is an internal Address for MPS", false, 0, 'Internal Address');
$form->add_checkbox("address_is_a_hq_address", "Address is a HQ address", false, 0, 'HQ Address');


$form->add_label("address_mps_customernumber", "MPS Customer Number");
$form->add_label("address_mps_shipto", "MPS Ship To Number");


$form->add_section('Launch Plan');
$form->add_checkbox("address_involved_in_lps", "can order Tissot LPS material", false, 0, 'Launch Plan');



if(!array_key_exists("address_added_by", $tracking_data))
{
	$form->add_hidden("address_added_by", user_id());
}
elseif(id() > 0) {
	$form->add_hidden("address_last_modified_by", user_id());
	$form->add_hidden("address_last_modified", date("Y-m-d H:i:s"));
}

if(array_key_exists("address_added_by", $tracking_data) and $tracking_data["address_added_by"] != "")
{
	$form->add_section("Tracking infos");
	$form->add_label("added_by", "Address created by", 0, $tracking_data["address_added_by"]);
	$form->add_label("modified_by", "Last modifed by", 0, $tracking_data["address_last_modified_by"]);

	if($address_active == 0 and array_key_exists("address_set_to_inactive_by", $tracking_data) and $tracking_data["address_set_to_inactive_by"] != "")
	{
		$form->add_label("set_to_inactive", "Address set to inactive by", 0, $tracking_data["address_set_to_inactive_by"]);
	}
}

$form->add_button("save", "Save");
$form->add_button(FORM_BUTTON_DELETE, "Delete");
$form->add_button("back", "Back");



/*List of active users*/
$sql = "select user_id, user_firstname, user_name, address_company, user_phone, user_mobile_phone, " . 
	   "user_email, user_email_cc, user_email_deputy, " .
	   " if(address_type = 7, \"<strong><font color='#FF0000'>!!!</font></strong>\", '') as 3rdpuser " .
	   "from users " . 
	   "left join addresses on user_address = address_id";

$ulist_filter = "user_active = 1 and user_address = '" . id() . "'";

$sql_r = "select user_id, role_name " . 
         " from users " . 
		 " left join user_roles on user_role_user = user_id " . 
		 " left join roles on role_id = user_role_role " . 
		 " where user_active = 1 and user_address = '" . id() . "' " . 
		 " order by role_name";

//get user roles
$roles = array();
$res = mysql_query($sql_r) or dberror($sql_r);
while($row = mysql_fetch_assoc($res))
{
	if(array_key_exists($row['user_id'], $roles))
	{
		$roles[$row['user_id']]	= $roles[$row['user_id']] . '<br />' . $row['role_name'];
	}
	else
	{
		$roles[$row['user_id']]	= $row['role_name'];
	}
}


$ulist = new ListView($sql);

$ulist->set_entity("users");
$ulist->set_title("Active Users");
$ulist->set_filter($ulist_filter);
$ulist->set_order("user_name, user_firstname");

$ulist->add_column("user_firstname", "First Name", "", LIST_FILTER_FREE);
$ulist->add_column("user_name", "Last Name", "user.php", LIST_FILTER_FREE);
//$ulist->add_column("3rdpuser", "", "", "", "", COLUMN_UNDERSTAND_HTML);
$ulist->add_text_column("roles", "Roles", COLUMN_UNDERSTAND_HTML, $roles);
$ulist->add_column("user_phone", "Phone", "", LIST_FILTER_FREE);
$ulist->add_column("user_mobile_phone", "Mobile Phone", "", LIST_FILTER_FREE);
$ulist->add_column("user_email", "Email", "", LIST_FILTER_FREE);
$ulist->add_column("user_email_cc", "CC", "", LIST_FILTER_FREE);
$ulist->add_column("user_email_deputy", "Deputy", "", LIST_FILTER_FREE);

$form->add_button("add_user", "Add User", "");
$ulist->populate();


//$ulist->process();

//$form->add_validation("is_shortcut({address_shortcut})", "The shortcut is invalid. It may only contain lower case characters a-z, digits and the underscore");
//$form->add_validation("is_email_address({address_email})", "The email address is invalid.");
//$form->add_validation("is_web_address({address_website})", "The website url is invalid.");


// Populate form and process button clicks

$form->populate();

if($form->value("address_canbefranchisee") == 1)
{
	$form->add_validation("{address_parent}", "You must indicate a parent address for a POS owner address.");
}

$form->process();

if($form->button("address_place_id"))
{
	$sql = "select place_name from places where place_id = " . dbquote($form->value("address_place_id"));
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
    {
		$form->value("address_place", $row["place_name"]);
	}
}
elseif($form->button("address_country"))
{
	$form->value("address_place", "");
	$form->value("address_place_id", "");
	$form->value("address_zip", "");
}
elseif($form->button("back"))
{
	redirect("addresses3rd.php?country=" . param("country") . "&type=" . param("type"). "&searchterm=" . param("searchterm"));
}

if($form->button("save") and !$form->value("address_active"))
{
	$form->value("address_phone", $form->unify_multi_edit_field($form->items["phone_number"]));
	$form->value("address_mobile_phone", $form->unify_multi_edit_field($form->items["mobile_phone_number"]));

	$form->add_validation("{address_phone} != '' or {address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");
	
	if($form->validate())
	{
	
		$form->value("address_address", $form->unify_multi_edit_field($form->items["street"], get_country_street_number_rule($form->value("address_country"))));
		
		$form->save();
		//set all users to inactive
		$sql = "update users set user_active = 0 where user_address = " . id();
		$result = mysql_query($sql) or dberror($sql);


		//update franchisee address of all ongoing projects

		$sql = "select * from addresses where address_id = " . dbquote(id());
		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
		
			$fields = array();
			
			$value = dbquote($row["address_company"]);
			$fields[] = "order_franchisee_address_company = " . $value;

			$value = dbquote($row["address_company2"]);
			$fields[] = "order_franchisee_address_company2 = " . $value;

			$value = dbquote($row["address_address"]);
			$fields[] = "order_franchisee_address_address = " . $value;

			$value = dbquote($row["address_street"]);
			$fields[] = "order_franchisee_address_street = " . $value;

			$value = dbquote($row["address_streetnumber"]);
			$fields[] = "order_franchisee_address_streetnumber = " . $value;

			$value = dbquote($row["address_address2"]);
			$fields[] = "order_franchisee_address_address2 = " . $value;

			$value = dbquote($row["address_zip"]);
			$fields[] = "order_franchisee_address_zip = " . $value;

			$value = dbquote($row["address_place"]);
			$fields[] = "order_franchisee_address_place = " . $value;

			$value = dbquote($row["address_country"]);
			$fields[] = "order_franchisee_address_country = " . $value;

			$value = dbquote($row["address_phone"]);
			$fields[] = "order_franchisee_address_phone = " . $value;

			$value = dbquote($row["address_phone_country"]);
			$fields[] = "order_franchisee_address_phone_country = " . $value;

			$value = dbquote($row["address_phone_area"]);
			$fields[] = "order_franchisee_address_phone_area = " . $value;

			$value = dbquote($row["address_phone_number"]);
			$fields[] = "order_franchisee_address_phone_number = " . $value;

			$value = dbquote($row["address_mobile_phone"]);
			$fields[] = "order_franchisee_address_mobile_phone = " . $value;

			$value = dbquote($row["address_mobile_phone_country"]);
			$fields[] = "order_franchisee_address_mobile_phone_country = " . $value;

			$value = dbquote($row["address_mobile_phone_area"]);
			$fields[] = "order_franchisee_address_mobile_phone_area = " . $value;

			$value = dbquote($row["address_mobile_phone_number"]);
			$fields[] = "order_franchisee_address_mobile_phone_number = " . $value;

			$value = dbquote($row["address_email"]);
			$fields[] = "order_franchisee_address_email = " . $value;

			$value = dbquote($row["address_contact_name"]);
			$fields[] = "order_franchisee_address_contact = " . $value;

			$value = dbquote($row["address_website"]);
			$fields[] = "order_franchisee_address_website = " . $value;


			$sql = "update orders set " . join(", ", $fields) . " where order_type = 1 and (order_archive_date is null or order_archive_date = '0000-00-00') and order_franchisee_address_id = " . dbquote(id());

			mysql_query($sql) or dberror($sql);
		}
	}
}
elseif($form->button("save"))
{
	$form->value("address_phone", $form->unify_multi_edit_field($form->items["phone_number"]));
	$form->value("address_mobile_phone", $form->unify_multi_edit_field($form->items["mobile_phone_number"]));

	$form->add_validation("{address_phone} != '' or {address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");

	if($form->validate())
	{

		$form->value("address_address", $form->unify_multi_edit_field($form->items["street"], get_country_street_number_rule($form->value("address_country"))));
		
		$form->save();

		if($form->value('address_active') != 1 and $address_active == 1)
		{
			$sql = 'update addresses set address_set_to_inactive_by  = ' . user_id() . ', address_set_to_inactive_date = ' . dbquote(date("Y-m-d H:i:s")) . ' where address_id = ' . id();
			mysql_query($sql) or dberror($sql);
		}
	}
}
elseif($form->button("add_user"))
{
	$link = "user.php?address=" . id() . "&country=" . param("country");
	redirect($link);
}


//invoice addresses of the customer
$form3 = new Form("addresses", "address");

$form3->add_section("Invoice Addresses");
$form3->add_comment("Only add new invoice addresses in case an invoice can also be sent to a different address than the client's address. Client's address is always the dafult invoice address.");

$form3->add_subtable("invoice_addresses", "", "invoice_address.php?address=" . id(), "Company",
    "select invoice_address_id, invoice_address_company as Company, place_name as City, country_name as Country " .
    "from invoice_addresses " .
	"left join places on place_id = invoice_address_place_id " . 
	"left join countries on country_id = invoice_address_country_id " . 
	"where invoice_address_address_id = " . id() . 
    " order by invoice_address_company");

$form3->add_button("add_invoiceaddress", "Add Invoice Address", "invoice_address.php?address=" . id(), OPTIONAL);

$form3->populate();
$form3->process();



/********************************************************************
    Create List of files
*********************************************************************/ 

$sql_files = "select addressfile_id, addressfile_title, addressfile_path, " .
             " concat(DAY(date_created), '.', MONTH(date_created), '.', YEAR (date_created)) as date " . 
			 "from addressfiles ";
$list_filter = " addressfile_address = " . id();

//get all files
$files = array();
$edit_links = array();
$sql = $sql_files . " where " . $list_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$link = "http://" . $_SERVER["HTTP_HOST"] . "/" . $row["addressfile_path"];
	$link = "<a href=\"javascript:popup('" . $link . "',800,600);\"><img src=\"/pictures/view.gif\" border='0'/></a>";
		
	$files[$row["addressfile_id"]] = $link;

	$edit_links[$row["addressfile_id"]] = '<a href="address_file.php?id=' . $row['addressfile_id'] . '&address=' . id() . '">' . $row["addressfile_title"] . '</a>';
}


$list = new ListView($sql_files);
$list->set_title("Files");
$list->set_entity("addressfiles");
$list->set_filter($list_filter);
$list->set_order("addressfile_title");

$list->add_text_column("files", "", COLUMN_UNDERSTAND_HTML, $files);
$list->add_text_column("title", "Title", COLUMN_UNDERSTAND_HTML, $edit_links);

$list->add_column("date", "Date", "", "", "", COLUMN_NO_WRAP);

$list->add_button("newfile", "Add File", "");

$list->populate();
//$list->process();

if($list->button("newfile"))
{
	$link = "address_file.php?address=" . id() . "&country=" . param("country");
	redirect($link);
}

// Render page

$page = new Page("addresses");

$page->header();
$page->title(id() ? "Edit Third Party Address" : "Add Third Party Address");
$form->render();

echo "<p>&nbsp;</p>";
$ulist->render();

echo "<p>&nbsp;</p>";
$form3->render();

echo "<p>&nbsp;</p>";
$list->render();


?>

<script language="javascript">
	$("#h_address_company").click(function() {
	   $('#address_company').val($('#address_company').val().toLowerCase());
	   var txt = $('#address_company').val();

	   $('#address_company').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_address_company2").click(function() {
	   $('#address_company2').val($('#address_company2').val().toLowerCase());
	   var txt = $('#address_company2').val();

	   $('#address_company2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	
	$("#h_address_address2").click(function() {
	   $('#address_address2').val($('#address_address2').val().toLowerCase());
	   var txt = $('#address_address2').val();

	   $('#address_address2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_address_address").click(function() {
	   $('#address_street').val($('#address_street').val().toLowerCase());
	   var txt = $('#address_street').val();

	   $('#address_street').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	
</script>

$page->footer();

?>