<?php
/********************************************************************

    logistics_addresses.php

    Lists addresses for editing.

    Created by:     Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date created:   2015-01-06
    Modified by:    Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-06
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(has_access("can_edit_catalog") 
  or has_access("can_edit_logistics_system_data"))
{

}
else
{
	check_access("can_edit_catalog");
}
set_referer("logistics_address.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
$sql = "select address_id, address_company, address_zip,  " .
	 "    country_name, address_type_name, client_type_code, " .
	 " address_vat_id, address_financecontroller_name, address_financecontroller_phone, " . 
	 " address_financecontroller_email " . 
	 "from addresses " .
	 " left join address_types on address_type_id = address_type " .
	 " left join client_types on client_type_id = address_client_type " . 
	 "left join countries on address_country = country_id ";

$list_filter = "address_id <> 5 and address_active = 1 and address_type = 1 ";


/********************************************************************
    Create List
*********************************************************************/ 


$list = new ListView($sql);

$list->set_entity("addresses");
$list->set_order("country_name, address_company");
$list->set_filter($list_filter);

$list->add_column("country_name", "Country");
//$list->add_column("address_type_name", "Type");
$list->add_column("client_type_code", "Client Type");
$list->add_column("address_company", "Company", "logistics_address.php");
$list->add_column("address_vat_id", "VAT ID");
$list->add_column("address_financecontroller_name", "Name");
$list->add_column("address_financecontroller_phone", "Phone");
$list->add_column("address_financecontroller_email", "Email");

$popup_link = "javascript:popup('logistics_adresses_print_xls.php', 1024, 768);";
$list->add_button("print", "Print this List", $popup_link);

/********************************************************************
    Populate and process button clicks
*********************************************************************/ 
$list->process();


/********************************************************************
   Create Page
*********************************************************************/ 
$page = new Page("logistics");

require "include/logistics_page_actions.php";

$page->header();

$page->title("Retail Clients");
$list->render();
$page->footer();

?>
