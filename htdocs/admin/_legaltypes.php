<?php

require_once "../include/frame.php";

$sql = "SELECT
`orders`.`order_number`, project_id, order_archive_date, 
`project_costtypes`.`project_costtype_text`,
`orders`.`order_actual_order_state_code`,
`projects`.`project_id`,
`orders`.`order_archive_date`
FROM
`project_costs`
Left Join `project_costtypes` ON `project_costs`.`project_cost_type` = `project_costtypes`.`project_costtype_id`
Left Join `orders` ON `project_costs`.`project_cost_order` = `orders`.`order_id`
Left Join `projects` ON `orders`.`order_id` = `projects`.`project_order`
where (project_costtype_id <> 1 and project_costtype_id <> 2) or project_costtype_id is null
Order by `project_costtypes`.`project_costtype_text`, `orders`.`order_number`";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	if($row["order_archive_date"] == Null or $row["order_archive_date"] == '0000-00-00')
	{
		echo "<a target=\"_blank\" href=\"" . APPLICATION_URL ."/user/project_edit_client_data.php?pid=" . $row["project_id"] . "\">" . $row["order_number"] . "</a><br />";
	}
	else
	{
		echo "<a target=\"_blank\" href=\"" . APPLICATION_URL ."/archive/project_edit_client_data.php?pid=" . $row["project_id"] . "\">" . $row["order_number"] . "</a><br />";
	}
}

?>