<?php
/********************************************************************

    addresses.php

    Lists addresses for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-29
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-08-19
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("address3rd.php");



$images = array();
$sql_images = "select address_id " . 
			  "from addresses " .
			  "where address_active = 1 and address_type = 7 and address_checked = 0";

$res = mysql_query($sql_images) or dberror($sql_images);

while ($row = mysql_fetch_assoc($res))
{
    $images[$row["address_id"]] = "/pictures/wf_warning.gif";
}


/********************************************************************
   Prepare filters
*********************************************************************/
$type_filter = array();

$type_filter[0] = "All";
$type_filter[1] = "Franchisee";
$type_filter[2] = "Independent Retailers";
$type_filter[3] = "Others";


$country_filter = array();

//countries
$sql = "select distinct address_country, country_name from addresses " . 
       "left join countries on country_id = address_country " . 
	   "where address_active = 1 and address_type = 7 " . 
	   "order by country_name";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	$country_filter[$row['address_country']] = $row['country_name'];
}



$preselect_filter = "";
if(param('country')) 
{
	register_param('country',param('country'));
	$preselect_filter .= " and address_country = " . param('country');
}

if(param('type')) 
{
	
	register_param('type',param('type'));
	if($preselect_filter and param("type") == 1) {
		
		$preselect_filter .= " and address_type = 7 and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1) and address_is_independent_retailer <> 1";
	}
	elseif($preselect_filter and param("type") == 2) {
		
		$preselect_filter .= " and address_is_independent_retailer = 1 ";
	}
	elseif($preselect_filter and param("type") == 3) {
		
		$preselect_filter .= " and (address_type = 7 or address_type is null) and address_canbefranchisee <> 1 and address_canbefranchisee_worldwide <> 1 and address_is_independent_retailer <> 1 ";
	}
	else
	{
		if(param("type") == 1) {
		
			$preselect_filter = " and address_type = 7 and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1) ";
		}
		elseif(param("type") == 2) {
			
			$preselect_filter = " and address_is_independent_retailer = 1 ";
		}
		elseif(param("type") == 3) {
			
			$preselect_filter = " and (address_type = 7 or address_type is null) and address_canbefranchisee <> 1 and address_canbefranchisee_worldwide <> 1 and address_is_independent_retailer <> 1";
		}
		
	}
}




if(param('searchterm')) 
{	
	
		$preselect_filter = ' and (address_company like "%' . param('searchterm') . '%" or address_place like "%' . param('searchterm') . '%") ';

		param("country", 0);
		param("type", 0);

}

if($preselect_filter)
{

	$list_filter = "address_active = 1 " . $preselect_filter;
}
else
{
	$list_filter = "address_active = 1 and address_type = 7 and (address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1) and address_country = 1";
	param("country", 1);
	param("type", 1);
}

if(param("type") == 0)
{
	$list_filter .= " and (address_type = 7 or address_type is null) ";
}

$link = "address3rd.php?country=" . param("country") . "&type=" . param("type") . "&searchterm=" . param("searchterm");

asort($country_filter);

//echo $list_filter;




/********************************************************************
    Create List
*********************************************************************/ 

$sql = "select address_id, address_shortcut, address_company, address_zip,  " .
	   "    address_place, country_name, province_canton, " .
	   "IF(address_canbefranchisee = 1 or address_canbefranchisee_worldwide = 1, 'yes', '') as franchisee, " .
	   "IF(address_is_independent_retailer = 1, 'yes', '') as retailer " .
	   "from addresses " .
	   "left join countries on address_country = country_id " . 
	   "left join places on place_id = address_place_id " .
  	   "left join provinces on province_id = place_province ";

$list = new ListView($sql);

$list->set_entity("addresses");
$list->set_order("address_company");
$list->set_filter($list_filter);

$list->add_listfilters("searchterm", "Search Term", 'input', '', param("searchterm"));
$list->add_listfilters("country", "Country", 'select', $country_filter, param("country"));
$list->add_listfilters("type", "Type", 'select', $type_filter, param("type"));

$list->add_image_column("notchecked", "", 0, $images);
//$list->add_column("address_shortcut", "Shortcut", $link);
$list->add_column("address_company", "Company", $link);
$list->add_column("franchisee", "Franchisee");
$list->add_column("retailer", "Retailer");
$list->add_column("province_canton", "Province");
$list->add_column("address_zip", "Zip");
$list->add_column("address_place", "City");
$list->add_column("country_name", "Country");


$list->add_button(LIST_BUTTON_NEW, "New", $link);

$popup_link = "javascript:popup('adresses_print_xls.php?view=2&af=" . param("type") . "&country=" . param("country") . "', 1024, 768);";
$list->add_button("print", "Print this List", $popup_link);


$list->process();


/********************************************************************
    Populate and process button clicks
*********************************************************************/ 

$list->process();


$page = new Page("addresses");

$page->header();
$page->title("Third Party Active Addresses");
$list->render();
$page->footer();

?>
