<?php
/********************************************************************

    inactive_query_owner.php

    Lists queries for inactive users.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2013-12-28
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2013-12-28
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$user_id = id();
$sql = "select concat(user_name, ' ', user_firstname, ' - ', address_company) as title, " . 
       "user_address " .
	   "from users " . 
	   "left join addresses on address_id = user_address " . 
	   "where user_id = " . $user_id;

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
$user_address_id = $row["user_address"];

$page_title = "Queries of " . $row["title"];

/********************************************************************
	prepare all data needed
*********************************************************************/

$sql = "select mis_query_id, mis_query_name" .
	   " from mis_queries ";

$list_filter = "mis_query_owner = " . $user_id;

$sql2 = "select posquery_id, posquery_name" .
	   " from posqueries ";

$list_filter2 = "posquery_owner = " . $user_id;


$sql_owners = "select user_id, " . 
              "concat(user_name, ' ', user_firstname, ' - ', address_company) as title ". 
			  "from users " . 
			  " left join addresses on address_id = user_address " . 
			  " where user_active = 1 " .  
			  " order by user_name, user_firstname, address_company";


$mis_context = array();
$mis_context["projects_in_process"] = 1;
$mis_context["operating_pos"] = 2;
$mis_context["closed_pos"] = 3;
$mis_context["cancelled_projects"] = 4;
$mis_context["projects_state"] = 5;
$mis_context["masterplan"] = 6;
$mis_context["cmsoverview"] = 7;
$mis_context["klapproved"] = 8;
$mis_context["projectsheets"] = 9;
$mis_context["productionplanning"] = 10;
$mis_context["projectmilestones"] = 11;
$mis_context["cmsatatus"] = 12;
$mis_context["transportationcost"] = 13;
$mis_context["posequipment"] = 14;
$mis_context["deliverybyitem"] = 15;
$mis_context["cmscompleted"] = 16;


$mis_links = array();

$sql_d = "select mis_query_id, mis_query_name, mis_query_context " .
	   " from mis_queries " . 
	   " where mis_query_owner = " . $user_id;
$res = mysql_query($sql_d) or dberror($sql_d);
while($row = mysql_fetch_assoc($res))
{
	$mis_links[$row["mis_query_id"]] = '<a href="/mis/projects_query_filter.php?query_id=' . $row["mis_query_id"] . '&qt=' . $mis_context[$row["mis_query_context"]] . '" target="_blank">' . $row["mis_query_name"] . '</a>';
}


/********************************************************************
	Create form
*********************************************************************/ 
$form = new Form("users", "users");


$form->add_comment("Assign queries to a new owner<br /><br />");

$form->add_list("rpu", "New Owner", $sql_owners, 0);

$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button("assign", "Replace Owner");
$form->add_button("delete", "Delete Queries");

$form->populate();
$form->process();



//remove all inactive users from permissions
if($form->button("assign") or $form->button("delete"))
{
	$users = array();
	$sql_d = "select mis_querypermission_user " .
		   "from mis_querypermissions " . 
		   "inner join users on user_id = mis_querypermission_user " . 
		   "where user_active = 0 and mis_querypermission_user > 0";

	$res = mysql_query($sql_d) or dberror($sql_d);
	while($row = mysql_fetch_assoc($res))
	{
		$users[$row["mis_querypermission_user"]] = $row["mis_querypermission_user"];
	}

	if(count($users) > 0)
	{
		$sql_d = "Delete from mis_querypermissions where mis_querypermission_user IN(" . implode(',', $users) . ")";
		$result = mysql_query($sql) or dberror($sql);
	}


	$users = array();
	$sql_d = "select posquery_permission_user " .
		   "from posquery_permissions " . 
		   "inner join users on user_id = posquery_permission_user " . 
		   "where user_active = 0 and posquery_permission_user > 0";

	$res = mysql_query($sql_d) or dberror($sql_d);
	while($row = mysql_fetch_assoc($res))
	{
		$users[$row["posquery_permission_user"]] = $row["posquery_permission_user"];
	}
	if(count($users) > 0)
	{
		$sql_d = "Delete from posquery_permissions where posquery_permission_user IN(" . implode(',', $users) . ")";
		$result = mysql_query($sql) or dberror($sql);
	}

}

if($form->button("assign"))
{

	if(!$form->value("rpu"))
	{
		$form->error("Please select a new owner.");
	}
	else
	{
		$sql = "update mis_queries SET mis_query_owner = " . $form->value("rpu") .
			   " where mis_query_owner = " . $user_id;

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posqueries SET posquery_owner = " . $form->value("rpu") .
			   " where posquery_owner = " . $user_id;

		$result = mysql_query($sql) or dberror($sql);

		redirect("inactive_query_owners.php");
	
	}
}
elseif($form->button("delete"))
{
	
	$sql = $sql . " where " . $list_filter;
	
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$sql = "Delete from mis_querypermissions where mis_querypermission_query = " . $row["mis_query_id"];
		$result = mysql_query($sql) or dberror($sql);
	}

	
	$sql = "Delete from mis_queries where mis_query_owner = " . $user_id;

	$result = mysql_query($sql) or dberror($sql);

	
	$sql = $sql2 . " where " . $list_filter2;
	
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$sql = "Delete from posquery_permissions where posquery_permission_query = " . $row["posquery_id"];
		$result = mysql_query($sql) or dberror($sql);
	}

	$sql = "delete from posqueries where posquery_owner = " . $user_id;

	$result = mysql_query($sql) or dberror($sql);

	redirect("inactive_query_owners.php");
}



/********************************************************************
	Create List of MIS Queries
*********************************************************************/ 

$list = new ListView($sql, LIST_HAS_HEADER);

$list->set_title("MIS Queries");
$list->set_entity("mis_queris");
$list->set_order("mis_query_name");
$list->set_filter($list_filter);
$list->add_text_column("query_name", "Query", COLUMN_UNDERSTAND_HTML | COLUMN_ALIGN_LEFT, $mis_links);
$list->populate();



/********************************************************************
	Create List of POS Queries
*********************************************************************/ 

$list2 = new ListView($sql2, LIST_HAS_HEADER);

$list2->set_title("POS Queries");
$list2->set_entity("posqueris");
$list2->set_order("posquery_name");
$list2->set_filter($list_filter2);

$list2->add_column("posquery_name", "Query", "/pos/query_generator_fields.php?query_id={posquery_id}");
$list2->populate();

/********************************************************************
   Create Page
*********************************************************************/ 
$page = new Page("people");

$page->header();
$page->title($page_title);
$form->render();
echo '<p>&nbsp;</p>';
$list->render();
echo '<p>&nbsp;</p>';
$list2->render();
$page->footer();

?>
