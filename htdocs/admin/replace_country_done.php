<?php
/********************************************************************

    replace_country_done.php

    replaces a country by another

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-08-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-08-15
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

// Render page

$page = new Page("countries");

$page->header();
$page->title("Replace a Country by another Country");

echo "<p>", "The replacement has been performed.", "</p>";

$page->footer();

?>