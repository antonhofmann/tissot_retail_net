<?php
/********************************************************************

    countries.php

    Lists countries for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-08-19
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("country.php");



//get languages spoken
$languages = array();
$sql = "select country_language_country_id, language_name " . 
       "from country_languages " . 
	   "left join languages on language_id = country_language_language_id " . 
	   "order by country_language_country_id, language_name";



$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if(array_key_exists($row["country_language_country_id"], $languages))
	{
		$languages[$row["country_language_country_id"]] = $languages[$row["country_language_country_id"]] . ", " . $row["language_name"];
	}
	else
	{
		$languages[$row["country_language_country_id"]] = $row["language_name"];
	}

}



$sql = "select country_id, country_code, country_name, concat(country_timeformat, ' hours') as time_format, " . 
       "salesregion_name, country_currency, currency_symbol, country_phone_prefix, " .
       "region_name " .
       "from countries " .
	   "left join salesregions on country_salesregion = salesregion_id " .
	   "left join regions on country_region = region_id " . 
	   "left join currencies on currency_id = country_currency ";

$list = new ListView($sql);

$list->set_entity("countries");
$list->set_order("country_name");

$list->add_column("country_code", "Country Code", "", LIST_FILTER_FREE);
$list->add_column("country_name", "Name", "country.php", LIST_FILTER_FREE);
$list->add_column("region_name", "Supplying Region", "", LIST_FILTER_LIST);
$list->add_column("salesregion_name", "Geographical Region", "", LIST_FILTER_LIST);
$list->add_column("currency_symbol", "Currency", "", LIST_FILTER_LIST);
$list->add_column("time_format", "Time Format", "", LIST_FILTER_LIST);
$list->add_text_column("languages", "Languages", COLUMN_UNDERSTAND_HTML, $languages);
$list->add_column("country_phone_prefix", "Phone Prefix", "", LIST_FILTER_LIST, "", COLUMN_ALIGN_RIGHT);

$list->add_button(LIST_BUTTON_NEW, "New", "country.php");

$list->process();

$page = new Page("countries");

$page->header();
$page->title("Countries");
$list->render();
$page->footer();
?>
