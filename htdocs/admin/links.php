<?php
/********************************************************************

    links.php

    Lists links for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-03
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2013-06-06
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("link.php");


//compose filter_form
$topic_filter = array();
$sql = "select distinct link_topic_id, link_topic_name " .
                "from links " . 
				"left join link_topics on link_topic_id = link_topic " . 
				"order by link_topic_name";


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$topic_filter[$row["link_topic_id"]] = $row["link_topic_name"];
}


$category_filter = array();
$sql = "select distinct link_category_id, link_category_name " .
                "from links " . 
				"left join link_categories on link_category_id = link_category " . 
				"order by link_category_name";


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$category_filter[$row["link_category_id"]] = $row["link_category_name"];
}

$archive_filter = array();
$archive_filter[1] = "Archived Entries";



//compose list

$sql = "select link_id, link_title, link_url, link_category_name, link_topic_name, " .
       "    right(link_path, length(link_path) - 13) as link_path " .
       "from links " . 
	   "left join link_categories on link_category = link_category_id ". 
	   "left join link_topics on link_topic = link_topic_id ";
	   



$list = new ListView($sql);

$list->set_entity("links");
$list->set_order("link_category_name, link_topic_name, link_title");

if(param('ltf') > 0 and param('caf') > 0 and param('archive') > 0)
{
	$list->set_filter("link_topic = " . param('ltf') . " and link_category = " . param('caf') . " and link_archived = 1");
}
elseif(param('ltf') > 0 and param('archive') > 0)
{
	$list->set_filter("link_topic = " . param('ltf') . " and link_archived = 1");
}
elseif(param('caf') > 0 and param('archive') > 0)
{
	$list->set_filter("link_category = " . param('caf') . " and link_archived = 1");
}
elseif(param('ltf') > 0)
{
	$list->set_filter("link_archived = 0 and link_topic = " . param('ltf'));
}
elseif(param('caf') > 0)
{
	$list->set_filter("link_archived = 0 and link_category = " . param('caf'));
}
elseif(param('archive') > 0)
{
	$list->set_filter("link_archived = 1");
}
else
{
	$list->set_filter("link_archived = 0");
}


$list->add_listfilters("caf", "Category", 'select', $category_filter, param("caf"));
$list->add_listfilters("ltf", "Topic", 'select', $topic_filter, param("ltf"));
$list->add_listfilters("archive", "Archive", 'select', $archive_filter, param("archive"));



$list->add_column("link_category_name", "Category", "", LIST_FILTER_LIST,
    "select link_category_name from link_categories order by link_category_name");

$list->add_column("link_topic_name", "Topic", "", LIST_FILTER_LIST,
    "select link_topic_name from link_topic order by link_topic_name");

$list->add_column("link_title", "Title", "link.php?ltf=" . param("ltf") . "&caf=" . param("caf") . "&archive=" . param("archive"), LIST_FILTER_FREE , "", COLUMN_NO_WRAP);

$list->add_column("link_url", "URL", "", LIST_FILTER_FREE);
$list->add_column("link_path", "File", "", LIST_FILTER_FREE);



$list->add_button(LIST_BUTTON_NEW, "New", "link.php?ltf=" . param("ltf"));

if(param('ltf') > 0)
{
	$list->add_button("global_accessibility", "Set Global Accessibility", "javascript:set_global_accessibility();");	
}

$list->process();




$page = new Page("links");

$page->header();
$page->title("Downloads & Links");

$list->render();

?>
<script type="text/javascript">
  
  function set_global_accessibility()
  {
	  $.nyroModalManual({
		  url: "/admin/links_set_accessibility.php?ltf=<?php echo param("ltf");?>"
	   });
  }
</script>


<?php

$page->footer();

?>
