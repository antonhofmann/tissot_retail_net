<?php
/********************************************************************

    security_excludeips.php

    Edit excluded IPS.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-09-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-09-05
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_unlock_ips");
set_referer("security_excludeip.php");


$sql= "select sec_excluded_ip_id, sec_excluded_ip_ip, " . 
      "IF(sec_excluded_ip_exclude = 1, 'yes', 'no') as excludeip, sec_excluded_ip_maxtrials " . 
	  "from sec_excluded_ips";


$list = new ListView($sql);

$list->set_entity("sec_excluded_ips");
$list->set_order("sec_excluded_ip_ip");

$list->add_column("sec_excluded_ip_ip", "IP Address", "security_excludeip.php");
$list->add_column("sec_excluded_ip_maxtrials", "Max Trials");
$list->add_column("excludeip", "Exclude IP");

$list->add_button(LIST_BUTTON_NEW, "New", "security_excludeip.php");

$list->process();

$list->populate();
$list->process();

$page = new Page("security");
$page->header();
$page->title("IP Exceptions");
$list->render();
$page->footer();

?>