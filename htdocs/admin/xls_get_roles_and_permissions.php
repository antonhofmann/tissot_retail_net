<?php
/********************************************************************

    xls_get_roles_and_permissions.php

    Generate Excel File: Roles and permissions

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-01-04
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-01-04
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

require_once "../include/xls/Writer.php";



/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "roles_and_permissions" . date("Ymd") . ".xls";
$xls =& new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$f_header =& $xls->addFormat();
$f_header->setSize(12);
$f_header->setAlign('left');
$f_header->setBold();

$f_title =& $xls->addFormat();
$f_title->setSize(10);
$f_title->setAlign('left');
$f_title->setBold();

$f_group_title =& $xls->addFormat();
$f_group_title->setSize(10);
$f_group_title->setAlign('left');
$f_group_title->setBold();

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);


$f_unused =& $xls->addFormat();
$f_unused->setSize(8);
$f_unused->setBorder(1);
$f_unused->setPattern(2);
$f_unused->setBgColor('silver');

$f_border_left=& $xls->addFormat();
$f_border_left->setTop(1);
$f_border_left->setBottom(1);
$f_border_left->setLeft(1);
$f_border_left->setSize(10);
$f_border_left->setAlign('left');
$f_border_left->setBold();

$f_border_right=& $xls->addFormat();
$f_border_right->setTop(1);
$f_border_right->setBottom(1);
$f_border_right->setRight(1);


$f_border_middle=& $xls->addFormat();
$f_border_middle->setTop(1);
$f_border_middle->setBottom(1);



/********************************************************************
    write all roles
*********************************************************************/
$row_index = 2;
$col_index = 1;

$sql = 'select * from roles ' .
	   'order by role_name';

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	   $sheet->write($row_index, $col_index, $row['role_name'], $f_normal);
	   $col_index++;
}

/********************************************************************
    write all permissions
*********************************************************************/
$row_index = 3;
$col_index = 0;


$sql = "select * from permissions " . 
       "order by permission_description ";




$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	//$sheet->write($row_index, $col_index, $row['permission_name'], $f_normal);
	$sheet->write($row_index, $col_index, $row['permission_description'], $f_normal);
	

	$sql = 'select * from roles ' .
		   'order by role_name';

	$res1 = mysql_query($sql) or dberror($sql);
	while($row1 = mysql_fetch_assoc($res1))
	{
		   
		   $col_index++;
		   $sql = 'select count(role_permission_id) as num_recs ' . 
			      'from role_permissions ' . 
			       'where role_permission_permission = ' . $row['permission_id'] .
			       ' and role_permission_role = ' . $row1['role_id'];

			$res2 = mysql_query($sql) or dberror($sql);
			$row2 = mysql_fetch_assoc($res2);
			
			if($row2['num_recs'] > 0)
			{
				$sheet->write($row_index, $col_index, 'x', $f_center);
			}
			else
			{
				$sheet->write($row_index, $col_index, '', $f_center);
			}
	
	}
	
	$row_index++;
	$col_index = 0;
}


$xls->close(); 

?>