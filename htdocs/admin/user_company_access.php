<?php
/********************************************************************

    user_company_access.php

    Creation and mutation of regional responsabilities for
	Regional Sales managers and TAMs.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-05-31
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-05-31
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


/********************************************************************
    prepare all data needed
*********************************************************************/

//get all regional sales managers and terrotiry account managers
$users = array();
$sql = "SELECT role_name, user_id, user_firstname, user_name " . 
	   "FROM user_roles " . 
	   " INNER JOIN users ON user_role_user = user_id " .
	   " INNER JOIN roles ON user_role_role = role_id " .
	   " where user_role_role in (33,56) and user_active = 1 " . 
	   " order by user_name, user_firstname ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$users[$row["user_id"]] = $row;
}

//get all clients
$addresses = array();
$sql = "SELECT address_id, country_name, address_company, client_type_code " . 
	   " FROM addresses " . 
	   " INNER JOIN client_types ON address_client_type = client_type_id " .
	   " INNER JOIN countries ON address_country = country_id " . 
	   " where address_type = 1  " .
       " order by country_name, address_company ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$addresses[$row["address_id"]] = $row;
}


$sql_lists = "SELECT address_id, salesregion_name, country_name, address_company, client_type_code, " . 
             "if(address_active <> 1,'inactive', '') as inactive " . 
			 " FROM addresses " . 
			 " INNER JOIN client_types ON address_client_type = client_type_id " .
			 " INNER JOIN countries ON address_country = country_id " . 
			 " INNER JOIN salesregions ON countries.country_salesregion = salesregions.salesregion_id";
$list_filter = "address_type = 1  ";

/********************************************************************
    create lists
*********************************************************************/


$list_names = array();
$group_titles = array();
$group_ids = array();
foreach($users as $user_id=>$user_data)
{
	$retail_access = array();
	$wholesale_access = array();
	
	if(count($_POST) == 0)
	{
		$sql_a = "select user_company_responsible_address_id, user_company_responsible_retail, user_company_responsible_wholsale" . 
				 " from user_company_responsibles ". 
				 " where user_company_responsible_user_id = " . $user_id;

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		while ($row_a = mysql_fetch_assoc($res_a))
		{
			$retail_access[$row_a["user_company_responsible_address_id"]] = $row_a["user_company_responsible_retail"];
			$wholesale_access[$row_a["user_company_responsible_address_id"]] = $row_a["user_company_responsible_wholsale"];
		}
	}

	

	
	$listname = "list" . $user_id;
	$list_names[] = $listname;
	$list_owners[] = $user_id;
	$group_titles[] = $user_data["user_name"] . " " . $user_data["user_firstname"] . ": " . $user_data["role_name"];
	$group_ids[] = $user_data["user_id"];

	$save_buttonname = "save" . $user_id;
	$save_button_names[] = $save_buttonname;


	//$toggler = '<div class="toggler_pointer" id="l' . $user_data["user_id"] . '_on"><span class="fa fa-plus-square toggler"></span>' .$user_data["user_name"] . " " . $user_data["user_firstname"] . ": " . $user_data["role_name"] . '</div>';
	
	$$listname = new ListView($sql_lists, LIST_HAS_HEADER | LIST_HAS_FOOTER);

	$$listname->set_entity("addresses");
	$$listname->set_order("country_name, address_company");
	$$listname->set_filter($list_filter);
	//$$listname->set_title($toggler);
	$$listname->set_group("salesregion_name");

	$$listname->add_checkbox_column("R" . $user_id, "Retail", COLUMN_UNDERSTAND_HTML, $retail_access);
	$$listname->add_checkbox_column("W" . $user_id, "Wholesale", COLUMN_UNDERSTAND_HTML, $wholesale_access);
	$$listname->add_column("country_name", "Country", "", "", "", COLUMN_ALIGN_LEFT | COLUMN_NO_WRAP);
	$$listname->add_column("address_company", "Company", "", "", "", COLUMN_ALIGN_LEFT | COLUMN_NO_WRAP);
	$$listname->add_column("client_type_code", "type", "", "", "", COLUMN_ALIGN_LEFT | COLUMN_NO_WRAP);
	$$listname->add_column("inactive", " ", "", "", "", COLUMN_ALIGN_LEFT | COLUMN_NO_WRAP);

	

	$$listname->add_button($save_buttonname, "Save List");
	
	$$listname->populate();
	$$listname->process();
}

$save_lists = false;
foreach($save_button_names as $key=>$buttonname)
{
	$list = $list_names[$key];
	if($$list->button($buttonname))
	{
		$save_lists = true;
	}
}

if($save_lists == true)
{
	$sql = "delete from user_company_responsibles";
	$result = mysql_query($sql) or dberror($sql);
	
	foreach($users as $user_id=>$user_data)
	{
		$list = "list" . $user_id;
		$R = $$list->values("R" . $user_id);
		$W = $$list->values("W" . $user_id);

		
		
		foreach($addresses as $address_id=>$address_data)
		{
			$rvalue = 0;
			$wvalue = 0;
			if(array_key_exists($address_id, $R))
			{
				$rvalue = 1;
			}
			if(array_key_exists($address_id, $W))
			{
				$wvalue = 1;
			}

			if($rvalue == 1 or $wvalue == 1)
			{
				$fields = array();
				$values = array();

				$fields[] = "user_company_responsible_address_id";
				$values[] = dbquote($address_id);

				$fields[] = "user_company_responsible_user_id";
				$values[] = dbquote($user_id);

				$fields[] = "user_company_responsible_retail";
				$values[] = dbquote($rvalue);

				$fields[] = "user_company_responsible_wholsale";
				$values[] = dbquote($wvalue);

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-M-D H:i:s"));

				$sql = "insert into user_company_responsibles (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
			}
		}
	}
}

$page = new Page("users");
$page->header();
$page->title("Edit Regional Responsabilities");

foreach($list_names as $key=>$listname)
{
	
	if(array_key_exists("company_access", $_SESSION) 
		and array_key_exists($listname, $_SESSION["company_access"])
		and $_SESSION["company_access"][$listname] == 1
		)
	{
		$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $list_owners[$key] . '_on"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
		echo $toggler;

		$toggler = '<div class="toggler_pointer" id="l' . $list_owners[$key] . '_off"><span class="fa fa-minus-square toggler"></span>' . $group_titles[$key] . '</div>';
		echo $toggler;
		
		echo '<br />';
		echo '<div id="' . $listname . '">';
		$$listname->render();
		echo '</div>';
	}
	else
	{
		$toggler = '<div class="toggler_pointer" id="l' . $list_owners[$key] . '_on"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
		echo $toggler;

		$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $list_owners[$key] . '_off"><span class="fa fa-minus-square toggler"></span>' . $group_titles[$key] . '</div>';
		echo $toggler;
		
		echo '<br />';
		echo '<div class="toggler_pointer toggler_pointer_off" id="' . $listname . '">';
		$$listname->render();
		echo '</div>';
	}
}


?>
<script language="javascript">
jQuery(document).ready(function($) {
	<?php
	foreach($list_names as $key=>$listname)
	{
	?>
		$('#l<?php echo $group_ids[$key];?>_on').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'block');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'block');
			$('#l<?php echo $group_ids[$key];?>_on').css('display', 'none');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=1",
				url: "../shared/ajx_company_access_list_states.php",
				success: function(msg){
				}
			});
		});
		
		$('#l<?php echo $group_ids[$key];?>_off').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'none');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'none');
			$('#l<?php echo $group_ids[$key];?>_on').css('display', 'block');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=0",
				url: "../shared/ajx_company_access_list_states.php",
				success: function(msg){
				}
			});
			
			
		});
	<?php
	}
	?>
});



</script>

<?php

$page->footer();

?>