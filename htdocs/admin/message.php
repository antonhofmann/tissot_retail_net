<?php
/********************************************************************

    message.php

    Creation and mutation of message records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-02
    Modified by:    Anbton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-03-14
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$applications = array();
$applications['retaildevelopment'] = "Retail Development";
$applications['mps'] = "MPS";
$applications['red'] = "RED";
$applications['lps'] = "LPS";
$applications['news'] = "News";


//prepare data

$sql_roles = "select role_id, role_name " . 
             "from roles " . 
			 " order by role_application_number, role_name";



//get access rights
$access_rights = array();
$sql = "select message_roles_role " . 
       " from message_roles " . 
       " where message_roles_message = " . dbquote(param("id"));


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$access_rights[] = $row["message_roles_role"];
}


//create form

$form = new Form("messages", "message");

$form->add_section();

if(!param("qs"))
{
	$form->add_hidden("archive", param("archive"));
	$form->add_edit("message_title", "Title*", NOTNULL);
	$form->add_edit("message_date", "Date*", NOTNULL, date("d.m.y"));
	$form->add_edit("message_expiry_date", "Expiry Date*",NOTNULL);

	$form->add_section();
	$form->add_multiline("message_text", "Content", 10);

	$form->add_section("Attach Files");

	$form->add_comment("Attach the following File to the mail message:");
	$form->add_list("message_file1", "File 1",
		"select link_id, link_title from links " .
		"where link_path is not null " . 
		"order by link_title");

	$form->add_list("message_file2", "File 2",
		"select link_id, link_title from links " .
		"where link_path is not null " . 
		"order by link_title");

	$form->add_list("message_file3", "File 3",
		"select link_id, link_title from links " .
		"where link_path is not null " . 
		"order by link_title");

	$form->add_list("message_file4", "File 4",
		"select link_id, link_title from links " .
		"where link_path is not null " . 
		"order by link_title");

	$form->add_section("Importance");
	$form->add_checkbox("message_important", "Show Warning Icon");



	$form->add_section(" ");
	$form->add_section("Accessibility");
	$form->add_comment("Please indicate who is allowed to see this message.");

	$roles = array();

	$list_of_checkboxgroups= array();

	foreach($applications as $key=>$value)
	{
		$list_of_checkboxes = array();
		$i = 1;
		$sql = "select role_id, role_application, role_code, role_name, " .
			   " concat(user_name, ' ', user_firstname) as responsible_admin " . 
			   "from roles " . 
			   " left join users on user_id = role_responsible_user_id " . 
			   " where role_application = " . dbquote($key) . 
			   " order by role_name";


		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$role_caption = "";
			if($i == 1)
			{
				$i++;
				$role_caption = $value;
				$form->add_comment("&nbsp;");
				$form->add_label("a" . $key, "", RENDER_HTML, "<div id='a" .$key . "'><a href='javascript:select_all_" . $key . "();'>select all</a></div>");

			}
			
			if(in_array($row["role_id"], $access_rights))
			{
				$form->add_checkbox("message_roles_" . $row["role_id"], $row["role_name"], true, 0, $role_caption);
			}
			else
			{
				$form->add_checkbox("message_roles_" . $row["role_id"], $row["role_name"], false, 0, $role_caption);
			}

			$list_of_checkboxes[] = "message_roles_" . $row["role_id"];

			$roles[] = $row["role_id"];
		}

		$list_of_checkboxgroups[$key] = $list_of_checkboxes;
	}


	$form->add_section(" ");
	$link = "<a href=\"#\" onClick=\"button('save')\">Save whithout sending mails</a>";

	$form->add_label("save", "Save", RENDER_HTML, $link);

	//$form->add_checklist("message_roles", "", "message_roles",
	//    "select role_id, role_name from roles order by role_application_number, role_name");



	//NOTTIFICATION RECIPIENTS PERSONS PER ROLE

	$form->add_comment("<hr /><p>&nbsp;</p>");
	$form->add_section("Notification Recipients");
	$form->add_comment("Please Indicate the Group of Mail Recipients");

	$list_of_checkboxgroups2= array();

	foreach($applications as $key=>$value)
	{
		$list_of_checkboxes = array();
		$i = 1;
		$sql = "select role_id, role_application, role_code, role_name, " .
			   " concat(user_name, ' ', user_firstname) as responsible_admin " . 
			   "from roles " . 
			   " left join users on user_id = role_responsible_user_id " . 
			   " where role_application = " . dbquote($key) . 
			   " order by role_name";


		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$role_caption = "";
			if($i == 1)
			{
				$i++;
				$role_caption = $value;
				$form->add_comment("&nbsp;");
				$form->add_label("a2" . $key, "", RENDER_HTML, "<div id='a2" .$key . "'><a href='javascript:select_all2_" . $key . "();'>select all</a></div>");
			}
			
			$form->add_checkbox("mr_" . $row["role_id"], $row["role_name"], false, 0, $role_caption);
			$list_of_checkboxes[] = "mr_" . $row["role_id"];
		}
		$list_of_checkboxgroups2[$key] = $list_of_checkboxes;
	}
	$form->add_comment("&nbsp;");
	$form->add_checkbox("agents", "Agents", false, 0, "Others");
	$form->add_checkbox("subs", "Subsidiaries", false, 0, "");

	$form->add_button("continue", "Continue to Recipient Selection");
	$form->add_button("back", "Back");

	if(id() > 0)
	{
		$form->add_button(FORM_BUTTON_DELETE, "Delete this message", "", OPTIONAL);
	}
}
else
{
	$role_filter = ' where role_id in ("' . str_replace('.', '","', param('qs')) . '")';

	$client_type_filter = "";
	if(strpos(param('qs'), 'a.s') != false)
	{
		$client_type_filter = " and (address_id = 13 or address_client_type in (1, 2)) ";
	}
	elseif(strpos(param('qs'), 's') != false)
	{
		$client_type_filter = " and (address_id = 13 or address_client_type in (2)) ";
	}
	elseif(strpos(param('qs'), 'a') != false)
	{
		$client_type_filter = " and (address_id = 13 or address_client_type in (1)) ";
	}


	$sql_roles = "select role_id, role_name " . 
				"from roles " .
				$role_filter . 
				" order by role_application_number, role_name";

	
	$form->add_hidden("archive", param("archive"));
	$form->add_label("message_title", "Title");

	$form->add_hidden("message_text");

	
	$form->add_hidden("message_file1");
	$form->add_hidden("message_file2");
	$form->add_hidden("message_file3");
	$form->add_hidden("message_file4");

	

	$form->add_comment("<hr /><p>&nbsp;</p>");
	$form->add_section("Notification Recipients");
	$form->add_comment("Indicate all the persons to recieve an email notification containing the message.");

	$form->add_comment("");

	$sales_region_name = "";
	$roles = array();
	$role_names = array();
	$users = array();
	$res = mysql_query($sql_roles) or dberror($sql_roles);
	while ($row = mysql_fetch_assoc($res))
	{
		$form->add_section(" ");
		$roles[] = $row["role_id"];
		$role_users[$row["role_id"]] = array();
		$role_names[$row["role_id"]] = $row["role_name"];
		$form->add_label("s" .$row["role_id"], $row["role_name"], RENDER_HTML, "<div id='s" .$row["role_id"] . "'><a href='javascript:select_all_s" . $row["role_id"] . "();'>select all</a></div>");
		
		
		$sql_users = "select distinct user_id, " . 
					 "concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username, " .
			         "salesregion_name " . 
					 "from users " . 
					 "left join user_roles on user_role_user = user_id " . 
					 "left join roles on role_id = user_role_role " .
					 "left join addresses on address_id = user_address  " . 
					 "left join countries on country_id = address_country " .
			         "left join salesregions on salesregion_id = country_salesregion " . 
					 "where user_active = 1 and user_role_role = " . $row["role_id"] . 
			         $client_type_filter .
					 " order by salesregion_name, username";
		
		
		$res1 = mysql_query($sql_users) or dberror($sql_users);
		while ($row1 = mysql_fetch_assoc($res1))
		{
			$users[] = $row1["user_id"];
			$role_users[$row["role_id"]][$row1["user_id"]] = $row1["user_id"];

			if($sales_region_name != $row1["salesregion_name"])
			{
				$sales_region_name = $row1["salesregion_name"];
				$form->add_checkbox("r" .$row["role_id"] . "_" . $row1["user_id"], $row1["username"], false, 0, $sales_region_name);
			}
			else
			{
				$form->add_checkbox("r" .$row["role_id"] . "_" . $row1["user_id"], $row1["username"], false, 0, "");
			}
		}
	}

	//agents
	
	if(strpos(param('qs'), "a") != false or strpos(param('qs'), "a") === 0)
	{
		$form->add_section(" ");
		$roles[] = "agents";
		$role_names["agents"] = "Agents";
		$role_users["agents"] = array();
		$form->add_label("sagents", "Agents", RENDER_HTML, "<div id='sagents'><a href='javascript:select_all_sagents();'>select all</a></div>");


		$sql_agents = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address left join countries on country_id = address_country where address_client_type = 1 and user_active = 1 order by country_name, username";


		$res1 = mysql_query($sql_agents) or dberror($sql_agents);
		while ($row1 = mysql_fetch_assoc($res1))
		{
			$users[] = $row1["user_id"];
			$role_users['agents'][$row1["user_id"]] = $row1["user_id"];
			$form->add_checkbox("ragents_" . $row1["user_id"], $row1["username"], false, 0, "");
		}
	}



	//subsidaries
	if(strpos(param('qs'), "s") != false or strpos(param('qs'), "s") === 0)
	{
		$form->add_section(" ");
		$roles[] = "subs";
		$role_users["subs"] = array();
		$role_names["subs"] = "Subsidiaries";
		$form->add_label("ssubs", "Subsidiaries", RENDER_HTML, "<div id='ssubs'><a href='javascript:select_all_ssubs();'>select all</a></div>");


		$sql_subs = "select distinct user_id, concat(country_name, ', ', user_name, ' ' , user_firstname, ', ', address_company) as username from users left join user_roles on user_id = user_role_user left join addresses on address_id = user_address  left join countries on country_id = address_country where address_client_type = 2 and user_active = 1 order by country_name, username";


		$res1 = mysql_query($sql_subs) or dberror($sql_subs);
		while ($row1 = mysql_fetch_assoc($res1))
		{
			$users[] = $row1["user_id"];
			$role_users['subs'][$row1["user_id"]] = $row1["user_id"];
			$form->add_checkbox("rsubs_" . $row1["user_id"], $row1["username"], false, 0, "");
		}
	}



	$form->add_button("send_mail", "Send and Save Message");
	
	$form->add_button("cancel", "Cancel");
	$form->add_button("back2", "Back");

}

$form->populate();
$form->process();


if($form->button("continue"))
{
	$error = 0;
	$attachment_size = 0;
	//check the total size of files
	$sql = "select link_path from links " . 
		   " where link_id in (" . dbquote($form->value("message_file1")) . ", "  . dbquote($form->value("message_file2")) . ", ". dbquote($form->value("message_file3")) . ", ". dbquote($form->value("message_file4")) . ");";


	$res = mysql_query($sql) or dberror($sql);
    
	while($row = mysql_fetch_assoc($res))
	{
		$file = $_SERVER['DOCUMENT_ROOT'] . $row["link_path"];
		if(file_exists($file))
		{
			$attachment_size = $attachment_size + filesize ( $file);
		}
	}
	
	$attachment_size =$attachment_size / 1048576;
	
	if($attachment_size > 7)
	{
		$error=1;
	}
	


	
	if($error == 0)
	{
		$tmp = array();
		foreach($roles as $key=>$role_id)
		{
			if(array_key_exists("mr_" . $role_id, $_POST) and $_POST["mr_" . $role_id] == true)
			{
				$tmp[] = $role_id;
			}
		}

		if(array_key_exists("agents", $_POST) and $_POST["agents"] == true)
		{
			$tmp[] = "a";
		}

		if(array_key_exists("subs", $_POST) and $_POST["subs"] == true)
		{
			$tmp[] = "s";
		}

		if(count($tmp) > 0)
		{
			
			if($form->validate())
			{
				$form->value("message_text", str_replace('"', "'", $form->value("message_text")));

				$form->save();
				
				
				$sql = "delete from message_roles where message_roles_message = " . dbquote(param("id"));
				$result = mysql_query($sql) or dberror($sql);

				
				foreach($roles as $key=>$role_id)
				{
					if(array_key_exists("message_roles_" . $role_id, $_POST) and $_POST["message_roles_" . $role_id] == true)
					{

						$sql = "Insert into message_roles (message_roles_role, message_roles_message, user_created, date_created) " . 
							   " values (" . 
							   dbquote($role_id) . ", " .
							   dbquote(param("id")) . ", " . 
							   dbquote(user_login()) . ", " . 
							   dbquote(date("Y-m-d h:s:i")) . ")";

						$result = mysql_query($sql) or dberror($sql);
					}
				
				}
				
				$qs = implode(".", $tmp);
				$link = "message.php?id=" . id() . "&qs=" . $qs;
				
				redirect($link);
			}
		}
		else
		{
			$form->error("Please select at least one group of mail recipients.");
		}
	}
	else
	{
		$form->error("The total file size of attachments must not be more than 7MB due to restrictions in the Swatch Group's mail system.");
	}
}
elseif ($form->button("save"))
{
	if($form->validate())
	{
		
		$form->value('message_text', str_replace('"', "'", $form->value('message_text')));
		$form->save();
		
		
		$sql = "delete from message_roles where message_roles_message = " . dbquote(param("id"));
		$result = mysql_query($sql) or dberror($sql);

		
		foreach($roles as $key=>$role_id)
		{
			if(array_key_exists("message_roles_" . $role_id, $_POST) and $_POST["message_roles_" . $role_id] == true)
			{

				$sql = "Insert into message_roles (message_roles_role, message_roles_message, user_created, date_created) " . 
					   " values (" . 
					   dbquote($role_id) . ", " .
					   dbquote(param("id")) . ", " . 
					   dbquote(user_login()) . ", " . 
					   dbquote(date("Y-m-d h:s:i")) . ")";

				$result = mysql_query($sql) or dberror($sql);
			}
		
		}
		
		$link = "messages.php?archive=" . param("archive");
		redirect($link);
	}
}
elseif($form->button("send_mail"))
{
	if($form->validate())
	{
		require_once("message_send_mails.php");

		$link = "messages.php?archive=" . param("archive");
		redirect($link);
	}
}
elseif ($form->button("back") or $form->button("cancel"))
{
	$link = "messages.php?archive=" . param("archive");
	redirect($link);
}
elseif($form->button("back2"))
{
	$link = "message.php?archive=" . param("archive") . "&id=" . param("id");
	redirect($link);
}


$page = new Page("messages");
$page->header();
$page->title(id() ? "Edit Message" : "Add Message");
$form->render();

?>


<script language='javascript'>

	<?php
	if(isset($role_users))
	{
		foreach($roles as $key=>$role_id)
		{
		?>
		
		function select_all_s<?php echo $role_id;?>()
		{
			<?php
			foreach($role_users[$role_id]  as $key=>$user_id)
			{
				echo "document.getElementById('r" . $role_id . "_" . $user_id . "').checked = true;";
			}
			?>
			
			var div = document.getElementById('s<?php echo $role_id;?>');
			div.innerHTML = "<a href='javascript:deselect_all_s<?php echo $role_id;?>();'>deselect all</a>";
			
			
		}

		function deselect_all_s<?php echo $role_id;?>()
		{
			
			<?php
			foreach($role_users[$role_id]  as $key=>$user_id)
			{
				echo "document.getElementById('r" . $role_id . "_" .  $user_id . "').checked = false;";
			}
			?>

			var div = document.getElementById('s<?php echo $role_id;?>');
			div.innerHTML = "<a href='javascript:select_all_s<?php echo $role_id;?>();'>select all</a>";
		}

		<?php
		}
	}

	if(isset($list_of_checkboxgroups))
	{
		foreach($list_of_checkboxgroups as $key=>$checkbox_list)
		{
		?>
		
		function select_all_<?php echo $key;?>()
		{
			<?php
			foreach($checkbox_list  as $key2=>$checkbox_id)
			{
				echo "document.getElementById('" .$checkbox_id . "').checked = true;";
			}
			?>
			
			var div = document.getElementById('a<?php echo $key;?>');
			div.innerHTML = "<a href='javascript:deselect_all_<?php echo $key;?>();'>deselect all</a>";
			
			
		}

		function deselect_all_<?php echo $key;?>()
		{
			<?php
			foreach($checkbox_list  as $key2=>$checkbox_id)
			{
				echo "document.getElementById('" .$checkbox_id . "').checked = false;";
			}
			?>
			
			var div = document.getElementById('a<?php echo $key;?>');
			div.innerHTML = "<a href='javascript:select_all_<?php echo $key;?>();'>selec all</a>";
			
			
		}

		<?php
		}
		
		
		foreach($list_of_checkboxgroups2 as $key=>$checkbox_list)
		{
		?>
		
		function select_all2_<?php echo $key;?>()
		{
			<?php
			foreach($checkbox_list  as $key2=>$checkbox_id)
			{
				echo "document.getElementById('" .$checkbox_id . "').checked = true;";
			}
			?>
			
			var div = document.getElementById('a2<?php echo $key;?>');
			div.innerHTML = "<a href='javascript:deselect_all2_<?php echo $key;?>();'>deselect all</a>";
			
			
		}

		function deselect_all2_<?php echo $key;?>()
		{
			<?php
			foreach($checkbox_list  as $key2=>$checkbox_id)
			{
				echo "document.getElementById('" .$checkbox_id . "').checked = false;";
			}
			?>
			
			var div = document.getElementById('a2<?php echo $key;?>');
			div.innerHTML = "<a href='javascript:select_all2_<?php echo $key;?>();'>selec all</a>";
			
			
		}

		<?php
		}
	}
	?>

</script>

<?php



$list_of_checkboxes[] = "message_roles_" . $row["role_id"];
	$list_of_checkboxgroups[$key] = $list_of_checkboxes;
	$form->add_label("a" . $key, "", RENDER_HTML, "<div id='a" .$key . "'><a href='javascript:select_all_" . $key . "();'>select all</a></div>");

$page->footer();

?>