<?php
/********************************************************************

    security_loginfailures.php

    Lists login failures .

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2009-09-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-09-05
    Version:        1.0.0

    Copyright (c) 2009, Swatch AG, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";


if(has_access("can_unlock_ips") and !has_access("can_edit_catalog")) {
	redirect("security.php");

}
check_access("can_edit_catalog");
set_referer("security_unlock_ip.php");


$first_year = date("Y");
$years = array();
$sql = "select DISTINCT YEAR(sec_loginfailure_date) as sec_loginfailure_year from sec_loginfailures order by YEAR(sec_loginfailure_date) DESC";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    $years[$row["sec_loginfailure_year"]] = $row["sec_loginfailure_year"];
}


/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("sec_loginfailures", "sec_loginfailures");

$form->add_section("List Filter Selection");

if(param("year"))
{
	$form->add_list("year", "Year",	$sql, SUBMIT | NOTNULL, param("year"));
}
else
{
	$form->add_list("year", "Year", $sql, SUBMIT | NOTNULL, $first_year);
}

$form->populate();

/********************************************************************
    Create List
*********************************************************************/ 
$sql = "select sec_loginfailure_date,sec_loginfailure_ip, " . 
       "sec_loginfailure_source, concat(sec_loginfailure_user, ' ') as sec_loginfailure_user, " . 
	   " concat(sec_loginfailure_password,' ') as sec_loginfailure_password, " .
	   " concat(user_name, ' ', user_firstname) as username, " . 
	   " concat('<a href=\"user.php?id=', user_id, '\" target=\"_blank\">', user_name, ' ', user_firstname,  '</a>') as link " . 
	   "from sec_loginfailures " . 
	   " left join users on user_login = sec_loginfailure_user";



$list = new ListView($sql);

$list->set_entity("sec_loginfailures");
$list->set_order("sec_loginfailure_date DESC, sec_loginfailure_ip");

if(param("year"))
{
	$list->set_filter("YEAR(sec_loginfailure_date) = " . param("year"));
}
else
{
	$list->set_filter("YEAR(sec_loginfailure_date) = " . $first_year);
}

$list->add_column("link", "User Name", "", "", "", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP);

$list->add_column("sec_loginfailure_user", "User", "", "", "", COLUMN_NO_WRAP);
$list->add_column("sec_loginfailure_password", "Password", "", "", "", COLUMN_NO_WRAP);

$list->add_column("sec_loginfailure_date", "Date:Time", "", "", "", COLUMN_NO_WRAP);
$list->add_column("sec_loginfailure_ip", "IP Address", "", "", "", COLUMN_NO_WRAP);
$list->add_column("sec_loginfailure_source", "Source", "", "", "", COLUMN_NO_WRAP);


$list->add_button("delete", "Delete Event Log");

$list->process();

if($list->button("delete"))
{
	if(param("year"))
	{
		$sql = "delete from sec_loginfailures where YEAR(sec_loginfailure_date) = " . param("year");
	}
	else
	{
		$sql = "delete from sec_loginfailures where YEAR(sec_loginfailure_date) = " . $first_year;
	}
	
	$result = mysql_query($sql) or dberror($sql);
}

$page = new Page("security");

$page->header();
$page->title("Login Failures");
$form->render();
$list->render();
$page->footer();

?>
