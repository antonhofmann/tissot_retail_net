<?php
/********************************************************************

    role.php

    Creation and mutation of role records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-01
    Modified by:    Anton.Hofmann (aho@mediaparx.ch)
    Date modified:  2011-05-06
    Version:        2.0.0

    Copyright (c) 2002-2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$view = 'role';
if(array_key_exists('view', $_GET) and $_GET['view']) {
	$view = $_GET['view'];
}
elseif(param('view')) {
	$view = param('view');
}

$role_name = '';
$role_application_number = '';
$sql = 'select role_application_number, role_name from roles where role_id = ' . id();
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res)) {
	$role_name = $row['role_name'];
	$role_application_number = $row['role_application_number'];
}


$sql_order_states = 'select order_state_code, concat(order_state_code, " ", order_state_name) as orderstate ' . 
                    'from order_states ' .
					'left join order_state_groups on order_state_group_id = order_state_group ' . 
					'left join order_types on order_type_id = order_state_group_order_type ' . 
					'where order_type_id = 1 ' .
					'order by order_state_code';

$applications = array();
$applications['retaildevelopment'] = "Retail Development";
$applications['mps'] = "MPS";
$applications['red'] = "RED";
$applications['lps'] = "LPS";
$applications['news'] = "News";
$applications['uberalldata'] = "Uberall";

$applications_numbers = array();
$applications_numbers['retaildevelopment'] = "01";
$applications_numbers['mps'] = "02";
$applications_numbers['red'] = "04";
$applications_numbers['lps'] = "07";
$applications_numbers['news'] = "09";
$applications_numbers['uberalldata'] = "10";


$sql_role_admins = "select distinct user_id, concat(user_name, ' ' , user_firstname) as username " . 
                   "from users " . 
				   "left join user_roles on user_role_user = user_id " . 
				   "where user_active = 1 and user_role_role in (1, 41, 30, 35) " .
				   " order by username";

$file_categories = "select order_file_category_id, concat(if(order_file_category_type = 2, 'Projects: ', 'Orders: '), order_file_category_name) as order_file_category_name from order_file_categories order by order_file_category_name";

$form = new Form("roles", "role");
$form->add_hidden("view", $view);
$form->add_hidden("id", id());

if($view == 'role') {
	$form->add_section();
	
	$form->add_list("role_application", "Application", $applications, SUBMIT | NOTNULL | STRING);

	$form->add_hidden("role_application_number", $role_application_number);


	$form->add_edit("role_code", "Code*", NOTNULL);
	$form->add_edit("role_name", "Name*", NOTNULL | UNIQUE);

	
	$form->add_section('Responsible Role Administrator');
	$form->add_list("role_responsible_user_id", "User", $sql_role_admins);
	
	$form->add_checkbox("role_active", "Status", "", 0, "Role is active");

	$form->add_checkbox("role_is_visible_to_clients_in_my_company", "Role is visible to clients in My Company", false, 0, "Visibility to clients in My Company");
	$form->add_checkbox("role_is_visible_to_swatch_in_my_company", "Role is visible to Tissot HQ in My Company", false, 0, "Visibility to Tissot HQ users in My Company");
	


	$form->add_section('Restrictions in Visibility of Projects');
	$form->add_comment('Projects will be visible to the role only in the range of project states indicated.');

	$form->add_list("role_order_state_visible_from", "Visible from project state", $sql_order_states);
	$form->add_list("role_order_state_visible_to", "Visible to project state", $sql_order_states);


	$form->add_section("Visibility in Applications");
	$form->add_checkbox("role_used_in_mps", "Role is used in Merchandising Planning", false, 0, "MPS");
	$form->add_checkbox("role_used_in_red", "Role is used in Retail Environment Development", false, 0, "RED");
	$form->add_checkbox("role_is_visible_in_request_for_an_account", "Role is visible in request for an account", false, 0, "Request for an account");


	$form->add_section("Visibility in Acces Rights to Queries");
	$form->add_checkbox("role_include_in_queryselection_posindex", "Users of this role can be granted access to queries", false, 0, "POS Index");
	$form->add_checkbox("role_include_in_queryselection_mis", "Users of this role can be granted access to queries", false, 0, "MIS");
	$form->add_checkbox("role_include_in_queryselection_cer", "Users of this role can be granted access to queries", false, 0, "LNR/CER");

	

	$form->add_section("Restrictions in Visibility of Attachments");
	$form->add_checklist("order_file_categories", "Attachment Catgories", "role_file_categories", $file_categories);
}
elseif($view) {
	
	$permission_values = array();
	$sql = "select role_permission_permission from role_permissions " . 
		   "inner join permissions on permission_id = role_permission_permission " . 
	       "where permission_used = 1 ". 
		   " and role_permission_role = " . id() . " and permission_application = '" . $view . "' order by permission_description";
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res)) {
		$permission_values[$row['role_permission_permission']] = $row['role_permission_permission'];
	}

	$sql_permissions = "select permission_id, permission_description 
	                   from permissions 
					   where permission_used = 1
					   and permission_application = '" . $view . "' order by permission_description";

	$form->add_section('Permissions for ' . $role_name . ' in section ' . $view);
	
	$res = mysql_query($sql_permissions) or dberror($sql_permissions);
	while($row = mysql_fetch_assoc($res)) {
		
		if(array_key_exists($row['permission_id'], $permission_values)) {
			$form->add_checkbox("role_permissions_" . $row['permission_id'], "", 1,0, $row['permission_description']);
		}
		else {
			$form->add_checkbox("role_permissions_" . $row['permission_id'], "", 0, 0, $row['permission_description']);
		}
		
	}
	
	
}


if(id() and $view == 'role')
{
	
	$form->add_button("delete", "Delete");
	$form->add_button("copyrole", "Copy this Role");
}


$form->add_button("save", "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");


$form->populate();
$form->process();

if($form->button("save")) {

	if($form->value('view') == 'role') {
		
		$form->add_validation("strlen({role_code}) == 4 && is_upper_case({role_code})", "The role code is supposed to consist of exactly 4 uppercase characters.");

		if($form->validate())
		{
			
			$form->value("role_application_number", $applications_numbers[param("role_application")]);
			$form->save();
		}
	}
	elseif($form->validate()) {
		
		$res = mysql_query($sql_permissions) or dberror($sql_permissions);
		while($row = mysql_fetch_assoc($res)) {
			$sql = "delete from role_permissions " . 
				   "where role_permission_role = " . id() . 
				   " and role_permission_permission = " . $row["permission_id"];
			$result = mysql_query($sql) or dberror($sql);
		}
		
		

		foreach($form->items as $item) {
			if(substr($item['name'], 0, 17) == 'role_permissions_') {
				$permission_id = substr($item['name'], 17, strlen($item['name']));
				if($form->value($item['name']) == 1) {
					$sql = "insert into role_permissions (role_permission_role, role_permission_permission, user_created, date_created) VALUES (" . 
						   id() . ',' . $permission_id . ',"' . user_login() . '","' . date("Y-m-d:h:i:s") . '")';
					$res = mysql_query($sql) or dberror($sql);
				}
			}
		}

	}

	$form->message("The data has been saved!");
}
elseif($form->button("delete")) {

	$sql = "delete from role_permissions where role_permission_role = " . id();
	$res = mysql_query($sql) or dberror($sql);

	$sql = "delete from roles where role_id = " . id();
	$res = mysql_query($sql) or dberror($sql);

	$sql = "delete from user_roles where user_role_role = " . id();
	$res = mysql_query($sql) or dberror($sql);

	redirect('roles.php');

}
elseif($form->button("copyrole")) {
	
	//role data
	$sql = "select * from roles where role_id = " . dbquote(id());
	
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	$row["user_created"] = user_login();
	$row["date_created"] = date("Y-m-d H:i:s");
	$row["user_modified"] = "";
	$row["date_modified"] = "";
	
	$row["role_name"] =  $row["role_name"] . " - copy";

	
	$tmp_f = array();
	$tmp_v = array();
	foreach($row as $field_name=>$field_value)
	{
		if($field_name != 'role_id') {
			$tmp_f[] = $field_name;
			$tmp_v[] = dbquote($field_value);
		}

	}
	$sql = "insert into roles (" . implode(', ', $tmp_f). ") VALUES (" . implode(', ', $tmp_v) . ");";

	$result = mysql_query($sql) or dberror($sql);
	$new_role_id = mysql_insert_id();


	//role file categories
	$sql = "select * from role_file_categories where role_file_category_role_id = " . dbquote(id());
	
	$res = mysql_query($sql) or dberror($sql);
	
	while($row = mysql_fetch_assoc($res))
	{
		$row["role_file_category_role_id"] = $new_role_id;
		$row["user_created"] = user_login();
		$row["date_created"] = date("Y-m-d H:i:s");
		$row["user_modified"] = "";
		$row["date_modified"] = "";



		$tmp_f = array();
		$tmp_v = array();
		foreach($row as $field_name=>$field_value)
		{
			if($field_name != 'role_file_category_id') {
				$tmp_f[] = $field_name;
				$tmp_v[] = dbquote($field_value);
			}

		}
		$sql = "insert into role_file_categories (" . implode(', ', $tmp_f). ") VALUES (" . implode(', ', $tmp_v) . ");";

		$result = mysql_query($sql) or dberror($sql);
	}


	//role permissions
	$sql = "select * from role_permissions where role_permission_role = " . dbquote(id());
	
	$res = mysql_query($sql) or dberror($sql);
	
	while($row = mysql_fetch_assoc($res))
	{
		$row["role_permission_role"] = $new_role_id;
		$row["user_created"] = user_login();
		$row["date_created"] = date("Y-m-d H:i:s");
		$row["user_modified"] = "";
		$row["date_modified"] = "";

		$tmp_f = array();
		$tmp_v = array();
		foreach($row as $field_name=>$field_value)
		{
			if($field_name != 'role_permission_id') {
				$tmp_f[] = $field_name;
				$tmp_v[] = dbquote($field_value);
			}

		}
		$sql = "insert into role_permissions (" . implode(', ', $tmp_f). ") VALUES (" . implode(', ', $tmp_v) . ");";

		$result = mysql_query($sql) or dberror($sql);
	}


	//role applications
	$sql = "select * from role_applications where role_application_role = " . dbquote(id());
	
	$res = mysql_query($sql) or dberror($sql);
	
	while($row = mysql_fetch_assoc($res))
	{
		$row["role_application_role"] = $new_role_id;
		$row["user_created"] = user_login();
		$row["date_created"] = date("Y-m-d H:i:s");
		$row["user_modified"] = "";
		$row["date_modified"] = "";

		$tmp_f = array();
		$tmp_v = array();
		foreach($row as $field_name=>$field_value)
		{
			if($field_name != 'role_application_id') {
				$tmp_f[] = $field_name;
				$tmp_v[] = dbquote($field_value);
			}

		}
		$sql = "insert into role_applications (" . implode(', ', $tmp_f). ") VALUES (" . implode(', ', $tmp_v) . ");";

		$result = mysql_query($sql) or dberror($sql);
	}


	//role navigations
	$sql = "select * from role_navigations where role_navigation_role = " . dbquote(id());
	
	$res = mysql_query($sql) or dberror($sql);
	
	while($row = mysql_fetch_assoc($res))
	{
		$row["role_navigation_role"] = $new_role_id;
		$tmp_f = array();
		$tmp_v = array();
		foreach($row as $field_name=>$field_value)
		{
			if($field_name != 'role_navigation_id') {
				$tmp_f[] = $field_name;
				$tmp_v[] = dbquote($field_value);
			}

		}
		$sql = "insert into role_navigations (" . implode(', ', $tmp_f). ") VALUES (" . implode(', ', $tmp_v) . ");";

		$result = mysql_query($sql) or dberror($sql);
	}


	

	redirect("role.php?id=" . $new_role_id);

}

$page = new Page("roles");
$page->header();
$page->title(id() ? "Edit Role and Permissions" : "Add Role");

if(id())
{
	require_once("include/permission_tabs.php");
}

$form->render();
$page->footer();

?>