<?php
/********************************************************************

    service.php

    Creation and mutation of service records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-06-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-06-18
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_browse_catalog_in_admin");
set_referer("service_supplier.php");


$sql = "select postype_id,postype_name from postypes order by postype_name";
$pos_types = array();
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$pos_types[$row["postype_id"]] = $row["postype_name"];
}

$sql = "select product_line_id,product_line_name from product_lines where product_line_budget = 1 order by product_line_name";
$product_lines = array();
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$product_lines[$row["product_line_id"]] = $row["product_line_name"];
}


$pos_types_selected = array();
$product_lines_selected = array();
$sql = "select * from item_pos_types where item_pos_type_item = " . id();
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$pos_types_selected[] = $row["item_pos_type_pos_type"];
	$product_lines_selected[] = $row["item_pos_type_product_line"];
}

// Build form

$form = new Form("items", "item");

$form->add_section("Description");
$form->add_edit("item_code", "Code", NOTNULL | UNIQUE);
$form->add_edit("item_name", "Name", NOTNULL);

$form->add_hidden("item_type", ITEM_TYPE_SERVICES);


$form->add_list("item_category", "Category Group",
    "select item_category_id, item_category_name from item_categories " .
    "order by item_category_name", NOTNULL);

$form->add_edit("item_price", "Price " . get_system_currency_symbol());

$form->add_section();
$form->add_multiline("item_description", "Description", 6);


$form->add_section("Cost Monitoring");
$form->add_list("item_cost_group", "Cost Group",
    "select project_cost_groupname_id, project_cost_groupname_name from project_cost_groupnames " .
	"where  project_cost_groupname_active = 1 " . 
    "order by project_cost_groupname_name");

$form->add_list("item_costmonitoring_group", "Cost Monitoring Group",
    "select costmonitoringgroup_id, costmonitoringgroup_text from costmonitoringgroups " .
    "order by costmonitoringgroup_text");

$form->add_section("Available in Project Types");
foreach($pos_types as $key=>$name)
{
	if(in_array($key, $pos_types_selected))
	{
		$form->add_checkbox("pt" . $key, "", 1, 0, $name);
	}
	else
	{
		$form->add_checkbox("pt" . $key, "", 0, 0, $name);
	}
}

$form->add_section("Available in Product Lines");
foreach($product_lines as $key=>$name)
{
	if(in_array($key, $product_lines_selected))
	{
		$form->add_checkbox("pl" . $key, "", 1, 0, $name);
	}
	else
	{
		$form->add_checkbox("pl" . $key, "", 0, 0, $name);
	}
}

$form->add_section("Regions");
$form->add_checklist("item_regions", "", "item_regions",
    "select region_id, region_name from regions order by region_name");

$form->add_section("Other");
$form->add_checkbox("item_visible", "Visible in Catalog");
$form->add_checkbox("item_price_adjustable", "Price Adjustable");
$form->add_checkbox("item_active", "Active", true);

$form->add_subtable("suppliers", "<br />Suppliers", "service_supplier.php", "Code",
    "select supplier_id, supplier_item_code as Code, supplier_item_name as Name, " .
    "    supplier_item_price as Price, address_company as Supplier " .
    "from suppliers left join addresses on supplier_address = address_id " .
    "where supplier_item = " . id() . " " .
    "order by supplier_item_code");


if(has_access("can_edit_catalog"))
{
	$form->add_button(FORM_BUTTON_SAVE, "Save");
	$form->add_button(FORM_BUTTON_BACK, "Back");
	$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
	$form->add_button("add_supplier", "Add Supplier", "service_supplier.php?item=" . id(), OPTIONAL);
}
else
{
	$form->add_button(FORM_BUTTON_BACK, "Back");
}
// Populate form and process button clicks

$form->populate();
$form->process();

// delete all items from the categrory context if inactive
if ($form->button(FORM_BUTTON_SAVE))
{

    $sql = "DELETE FROM category_items USING category_items, items WHERE category_item_item = item_id and item_active = 0";
    $res = mysql_query($sql);

	//update pos types and product_lines
	$sql = "DELETE FROM item_pos_types WHERE item_pos_type_item = " . id();
    $res = mysql_query($sql);

	
	foreach($pos_types as $pos_type=>$name)
	{
		if($form->value("pt" . $pos_type) == 1)
		{
			foreach($product_lines as $product_line=>$name)
			{	
				if($form->value("pl" . $product_line) == 1)
				{
					$sql = "insert into item_pos_types (" . 
						   "item_pos_type_pos_type, " .
						   "item_pos_type_product_line, " .
						   "item_pos_type_item, " .
						   "user_created, " .
						   "date_created, " .
						   "user_modified, " .
						   "date_modified ) values (" .
						   $pos_type . ", " .
						   $product_line . ", " .
						   id() . ", " .
						   dbquote(user_login()) . ", " .
						   dbquote(date("Y-m-d")) . ", " .
						   dbquote(user_login()) . ", " .
						   dbquote(date("Y-m-d")) . ")";
					$res = mysql_query($sql);
				}
					   
			}
		}
	}
}

// Render page

$page = new Page("items");

$page->header();
if(has_access("can_edit_catalog"))
{
	$page->title(id() ? "Edit Service" : "Add Service");
}
else
{
	$page->title("View Service");
}
$form->render();
$page->footer();
?>