<?php
/********************************************************************

    ps_product_line.php

    Creation and mutation of project sheet product lines.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-27
    Version:        1.0.0

    Copyright (c) 2004, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("ps_product_lines", "ps_product_line");

$form->add_section();
$form->add_edit("ps_product_line_name", "Product Line", NOTNULL | UNIQUE);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("ps_product_lines");
$page->header();
$page->title(id() ? "Edit Project Sheet Product Line" : "Add Project Sheet Product Line");
$form->render();
$page->footer();

?>