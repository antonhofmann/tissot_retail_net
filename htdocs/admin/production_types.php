<?php
/********************************************************************

    production_types.php

    Lists production types for editing .

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2017-06-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2017-06-26
    Version:        1.0.0

    Copyright (c) 2017, RADO, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("production_type.php");

$sql = "select production_type_id, production_type_name from production_types ";

$list = new ListView($sql);

$list->set_entity("production_types");
$list->set_order("production_type_name");

$list->add_column("production_type_name", "Production type", "production_type.php");

//$list->add_button(LIST_BUTTON_NEW, "New", "production_type.php");

$list->process();

$page = new Page("production_types");

$page->header();
$page->title("Production Types");
$list->render();
$page->footer();

?>
