<?php
/********************************************************************

    milestone.php

    Creation and mutation of milestones.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-23
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$milestones = "select milestone_id, milestone_text from milestones order by milestone_code";



$form = new Form("milestones", "milestone");

$form->add_section();
$form->add_edit("milestone_code", "Code", NOTNULL | UNIQUE);
$form->add_edit("milestone_text", "Description", NOTNULL);
$form->add_edit("milestone_text_state", "State's Name", NOTNULL);


$form->add_section("Boutique - Notification to");
$form->add_edit("milestone_email_1", "Notification to");
$form->add_edit("milestone_ccmail_1", "CC Mail to");
$form->add_edit("milestone_ccmail_1_2", "CC Mail to");
$form->add_edit("milestone_ccmail_1_3", "CC Mail to");
$form->add_edit("milestone_ccmail_1_4", "CC Mail to");

$form->add_section("SIS - Notification to");
$form->add_edit("milestone_email_2", "Notification to");
$form->add_edit("milestone_ccmail_2", "CC Mail to");
$form->add_edit("milestone_ccmail_2_2", "CC Mail to");
$form->add_edit("milestone_ccmail_2_3", "CC Mail to");
$form->add_edit("milestone_ccmail_2_4", "CC Mail to");


$form->add_section("Kiosk - Notification to");
$form->add_edit("milestone_email_3", "Notification to");
$form->add_edit("milestone_ccmail_3", "CC Mail to");
$form->add_edit("milestone_ccmail_3_2", "CC Mail to");
$form->add_edit("milestone_ccmail_3_3", "CC Mail to");
$form->add_edit("milestone_ccmail_3_4", "CC Mail to");


$form->add_section("Independent Retailer - Notification to");
$form->add_edit("milestone_email_4", "Notification to");
$form->add_edit("milestone_ccmail_4", "CC Mail to");
$form->add_edit("milestone_ccmail_4_2", "CC Mail to");
$form->add_edit("milestone_ccmail_4_3", "CC Mail to");
$form->add_edit("milestone_ccmail_4_4", "CC Mail to");

$form->add_checkbox("milestone_on_check_cer_lease", "Check lease details and send mail to client", 0, 0, "On date entry");
$form->add_checkbox("milestone_in_new_project", "Insert date of submission", 0, 0, "On new project");
$form->add_checkbox("milestone_on_cer_submission", "Insert date of submission", 0, 0, "On form submission");
$form->add_checkbox("milestone_use_in_CER", "Show milestone in project overview", 0, 0, "Projects");
$form->add_checkbox("milestone_in_masterplan", "Show milestone in Master Plan", 0, 0, "Master Plan");
$form->add_checkbox("milestone_is_inr03_milestone", "Is an IN03-Milestone", 0, 0, "IN-03");


$form->add_checkbox("milestone_active", "Is Active");


$form->add_section("List of Project's State");
$form->add_checkbox("milestone_in_statelist", "Show milestone in project's state list", 0, 0, "List of Project's State");
$form->add_checkbox("milestone_in_statelist_showdate", "Show date instead of x", 0, 0, "");


$form->add_section("MIS: List of Milestones");
$form->add_list("milestone_start_id", "Sum up duration starting with", $milestones);
$form->add_list("milestone_start_id_replacement_id", "Conditional replacement of duration with", $milestones);
$form->add_list("milestone_replacement_if_empty", "If date is empty replace with", $milestones);
$form->add_edit("milestone_sumup_text", "Caption", 0);


if(id() != 1 and id() != 13 and id() != 14 and id() != 15 and id() != 16 and id() != 18 and id() !=39 and id() !=40) {
	$form->add_section("Standard Mail Text");
	$form->add_multiline('milestone_mailtext', "Mail Text", 12);
}
elseif(id() == 16 or id() == 18) {
	$form->add_section("Standard Mail Text");
	$form->add_comment('Please adapt the text here:<br /><a href="/admin/mailalert.php?id=48">Submission of LNR</a>');
}
elseif(id() == 13) {
	$form->add_section("Standard Mail Text");
	$form->add_comment('Please adapt the text here:<br /><a href="/admin/mailalert.php?id=44">LNR approval date is entered for production type with HQ-Layout</a><br /><a href="/admin/mailalert.php?id=45">LNR approval date is entered for production type with HQ-Layout</a><br /><a href="/admin/mailalert.php?id=46">LN approval date is entered for production type with local layout</a><br /><a href="/admin/mailalert.php?id=47">LN approval date is entered for production type with local layout</a>');
}
elseif(id() == 40) {
	$form->add_section("Standard Mail Text");
	$form->add_comment('Please adapt the text here:<br /><a href="/administration/mailtemplates/data/156">Investment Preapproval</a><br />and here<br /><a href="/administration/mailtemplates/data/157">Investment Preapproval</a>');
}
elseif(id() == 1) {
	$form->add_section("Standard Mail Text");
	$form->add_comment('Please adapt the text here for submissions:<br /><a href="/admin/mailalert.php?id=43">Submission of INR03</a><br /><a href="/admin/mailalert.php?id=4">Submission of CER</a><br /><a href="/admin/mailalert.php?id=20">Submission of LNR third party</a>');

	$form->add_comment('The text below only applies if the date was entered manually in the milestone section.');
	$form->add_multiline('milestone_mailtext', "Mail Text", 12);
}
elseif(id() == 15) {
	$form->add_section("Standard Mail Text");
	$form->add_comment('Please adapt the text here for resubmissions:<br /><a href="/admin/mailalert.php?id=52">Submission of INR03</a><br /><a href="/admin/mailalert.php?id=50">Submission of CER</a><br /><a href="/admin/mailalert.php?id=51">Submission of LNR third party</a>');

	$form->add_comment('The text below only applies if the date was entered manually in the milestone section.');
	$form->add_multiline('milestone_mailtext', "Mail Text", 12);
}

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

$page = new Page("milestones");
$page->header();
$page->title(id() ? "Edit Project Milestone" : "Add Project Milestone");
$form->render();
$page->footer();

?>