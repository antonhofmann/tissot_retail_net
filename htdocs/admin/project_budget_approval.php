<?php
/********************************************************************

    project_budget_approval.php

    Creation and mutation of budget-approval.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2003-06-14
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-06-14
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

//check_access("can_edit_catalog");


$form = new Form("budget_approvals", "budget_approval");

$form->add_hidden("budget_approval_priority");

$form->add_edit("budget_approval_user", "Approval by", NOTNULL);
$form->add_edit("budget_approval_budget_low", "Budget Min", NOTNULL);
//$form->add_edit("budget_approval_budget_high", "Budget Max");
$form->add_hidden("budget_approval_budget_high", 0);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);


$form->populate();

if (!$form->value("budget_approval_priority"))
{
    $sql = "select max(budget_approval_priority) from budget_approvals";
    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_row($res);
    $form->value("budget_approval_priority", $row[0] + 1);
}

$form->process();


$page = new Page("project_budget_approvals");
$page->header();
$page->title(id() ? "Edit Budget Approval" : "Add Budget Approval");
$form->render();
$page->footer();

?>