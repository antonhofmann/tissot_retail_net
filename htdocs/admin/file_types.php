<?php
/********************************************************************

    file_types.php

    Lists file types for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-13
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-08-19
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("file_type.php");

$list = new ListView("select file_type_id, file_type_name, file_type_mime_type, file_type_extension " .
                     "from file_types");

$list->set_entity("file types");
$list->set_order("file_type_name");

$list->add_column("file_type_name", "Name", "file_type.php", LIST_FILTER_FREE);
$list->add_column("file_type_mime_type", "MIME Type", "", LIST_FILTER_FREE);
$list->add_column("file_type_extension", "Extension", "", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_NEW, "New", "file_type.php");
$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");

$list->process();

$page = new Page("file_types");

$page->header();
$page->title("File Types");
$list->render();
$page->footer();

?>
