<?php
/********************************************************************

    production_type.php

    Creation and mutation of account numbers.

    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2017-06-26
    Version:        1.0.0

    Copyright (c) 2017, RADO, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("production_types", "production_types");

$form->add_section();
$form->add_edit("production_type_name", "Production Type Name", NOTNULL | UNIQUE);


$form->add_section("Workflow Steps");
$form->add_comment("Please indicate all workflow steps applying to this production type.");

$form->add_checklist("production_type_order_states", "", "production_type_order_states",
    "select order_state_id, concat(order_state_code, ' ', order_state_name) as os 
	from order_states 
	left join order_state_groups on order_state_group_id = order_state_group
	where order_state_group_order_type = 1 and order_state_not_used_anymore = 0
	and order_state_active = 1
	order by order_state_code");


$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
//$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("production_types");
$page->header();
$page->title(id() ? "Edit Production Type" : "Add Production Type");
$form->render();
$page->footer();

?>