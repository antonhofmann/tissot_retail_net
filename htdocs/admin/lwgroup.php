<?php
/********************************************************************

    lwgroup.php

    Edit Local Constrction Cost Group

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-06
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("lwgroups", "lwgroup");

$form->add_section();
$form->add_edit("lwgroup_group", "Group", NOTNULL);
$form->add_edit("lwgroup_code", "Code", NOTNULL | UNIQUE);
$form->add_edit("lwgroup_text", "Description", NOTNULL);

$sql = "select project_cost_groupname_id, project_cost_groupname_name " . 
       "from project_cost_groupnames " . 
	   "where project_cost_groupname_active = 1 order by project_cost_groupname_id";

$form->add_list("lwgroup_costgroup", "Cost Group*",$sql, NOTNULL);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_DELETE, "Delete");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();

$page = new Page("lwgroups");
$page->header();
$page->title("Edit Local Construction Cost Group");
$form->render();
$page->footer();

?>