<?php
/********************************************************************

    pos_type_notifications_replace_mail.php

    replece emails reciepienst of pos types.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-03-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-03-11
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("pos_type_notification.php");



$old_users = array();

$sql = "select DISTINCT postype_notification_email " .
	    "from postype_notifications " .
		"where postype_notification_email <> '' ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$old_users[$row["postype_notification_email"]] = $row["postype_notification_email"];
}

$sql = "select DISTINCT postype_notification_email2 " .
	    "from postype_notifications " .
		"where postype_notification_email2 <> '' ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$old_users[$row["postype_notification_email2"]] = $row["postype_notification_email2"];
}

$sql = "select DISTINCT postype_notification_email3 " .
	    "from postype_notifications " .
		"where postype_notification_email3 <> '' ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$old_users[$row["postype_notification_email3"]] = $row["postype_notification_email3"];
}


$sql = "select DISTINCT postype_notification_email4 " .
	    "from postype_notifications " .
		"where postype_notification_email4 <> '' ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$old_users[$row["postype_notification_email4"]] = $row["postype_notification_email4"];
}


$sql = "select DISTINCT postype_notification_email5 " .
	    "from postype_notifications " .
		"where postype_notification_email5 <> '' ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$old_users[$row["postype_notification_email5"]] = $row["postype_notification_email5"];
}


$sql = "select DISTINCT postype_notification_email6 " .
	    "from postype_notifications " .
		"where postype_notification_email6 <> '' ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$old_users[$row["postype_notification_email6"]] = $row["postype_notification_email6"];
}


$sql = "select DISTINCT postype_notification_email7 " .
	    "from postype_notifications " .
		"where postype_notification_email7 <> '' ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$old_users[$row["postype_notification_email7"]] = $row["postype_notification_email7"];
}

$sql = "select DISTINCT postype_notification_email8 " .
	    "from postype_notifications " .
		"where postype_notification_email8 <> '' ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$old_users[$row["postype_notification_email8"]] = $row["postype_notification_email8"];
}

$sql = "select DISTINCT postype_notification_email9 " .
	    "from postype_notifications " .
		"where postype_notification_email9 <> '' ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$old_users[$row["postype_notification_email9"]] = $row["postype_notification_email9"];
}

$sql = "select DISTINCT postype_notification_email10 " .
	    "from postype_notifications " .
		"where postype_notification_email10 <> '' ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$old_users[$row["postype_notification_email10"]] = $row["postype_notification_email10"];
}






$sql_pos_types = "select postype_id, postype_name ".
				 "from postypes " .
				 "where postype_showin_openprojects = 1 " .
				 "order by postype_name";


$sql_product_lines = "select DISTINCT product_line_id, product_line_name "  .
                     "from postype_notifications " .
					 "left join product_lines on product_line_id = postype_notification_prodcut_line " . 
					 "order by product_line_name ";

$form = new Form("postype_notifications", "POS Type Notifications");

$form->add_list("old_user", "Replace the following user*", $old_users, NOTNULL);
$form->add_edit("new_user", "Email of new user", NOTNULL);

/*
$form->add_section('POS Types');
$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
while($row = mysql_fetch_assoc($res))
{
    $form->add_checkbox("PT_" . $row["postype_id"], $row["postype_name"], false);
}

$form->add_section('Product Lines');
$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
while($row = mysql_fetch_assoc($res))
{
    $form->add_checkbox("PL_" . $row["product_line_id"], $row["product_line_name"], false);
}

*/
$form->add_button("replace", "Replace Recipients");
$form->add_button("back", "Back");
$form->populate();
$form->process();


if($form->button("back")) {
	redirect("pos_type_notifications.php");
}
elseif($form->button("replace")) {
	


	$sql = "update postype_notifications SET " .
		   "postype_notification_email = " . dbquote($form->value("new_user")) . 
		   " where postype_notification_email = " .dbquote($form->value("old_user"));

	$result = mysql_query($sql) or dberror($sql);

	$sql = "update postype_notifications SET " .
		   "postype_notification_email2 = " . dbquote($form->value("new_user")) . 
		   " where postype_notification_email2 = " .dbquote($form->value("old_user"));

	$result = mysql_query($sql) or dberror($sql);

	$sql = "update postype_notifications SET " .
		   "postype_notification_email3 = " . dbquote($form->value("new_user")) . 
		   " where postype_notification_email3 = " .dbquote($form->value("old_user"));

	$result = mysql_query($sql) or dberror($sql);

	
	$sql = "update postype_notifications SET " .
		   "postype_notification_email4 = " . dbquote($form->value("new_user")) . 
		   " where postype_notification_email4 = " .dbquote($form->value("old_user"));

	$result = mysql_query($sql) or dberror($sql);


	$sql = "update postype_notifications SET " .
		   "postype_notification_email5 = " . dbquote($form->value("new_user")) . 
		   " where postype_notification_email5 = " .dbquote($form->value("old_user"));

	$result = mysql_query($sql) or dberror($sql);


	$sql = "update postype_notifications SET " .
		   "postype_notification_email6 = " . dbquote($form->value("new_user")) . 
		   " where postype_notification_email6 = " .dbquote($form->value("old_user"));

	$result = mysql_query($sql) or dberror($sql);


	$sql = "update postype_notifications SET " .
		   "postype_notification_email7 = " . dbquote($form->value("new_user")) . 
		   " where postype_notification_email7 = " .dbquote($form->value("old_user"));

	$result = mysql_query($sql) or dberror($sql);


	$sql = "update postype_notifications SET " .
		   "postype_notification_email8 = " . dbquote($form->value("new_user")) . 
		   " where postype_notification_email8 = " .dbquote($form->value("old_user"));

	$result = mysql_query($sql) or dberror($sql);

	$sql = "update postype_notifications SET " .
		   "postype_notification_email9 = " . dbquote($form->value("new_user")) . 
		   " where postype_notification_email9 = " .dbquote($form->value("old_user"));

	$result = mysql_query($sql) or dberror($sql);

	$sql = "update postype_notifications SET " .
		   "postype_notification_email10 = " . dbquote($form->value("new_user")) . 
		   " where postype_notification_email10 = " .dbquote($form->value("old_user"));

	$result = mysql_query($sql) or dberror($sql);

	/*
	$sql = "delete from postype_notifications " .
		   "where (postype_notification_email2 is NULL " .
		   " and  postype_notification_email3 is NULL " .
		   " and  postype_notification_email4 is NULL " .
		   " and  postype_notification_email5 is NULL " .
		   " and  postype_notification_email6 is NULL " .
		   " and  postype_notification_email7 is NULL " .
		   " and  postype_notification_email8 is NULL) " .
		   " or (" .
		   " postype_notification_email2 = '' " .
		   " and  postype_notification_email3 = '' " .
		   " and  postype_notification_email4 = '' " .
		   " and  postype_notification_email5 = '' " .
		   " and  postype_notification_email6 = '' " .
		   " and  postype_notification_email7 = '' " .
		   " and  postype_notification_email8 = '') ";

	$result = mysql_query($sql) or dberror($sql);

	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res)) 
	{
		if($form->value("PT_" . $row["postype_id"]) == 1) {
			$res2 = mysql_query($sql_product_lines) or dberror($sql_product_lines);
			while($row2 = mysql_fetch_assoc($res2)) 
			{
				if($form->value("PL_" . $row2["product_line_id"]) == 1) {
					

					
					if($form->value("postype_notification_email2") != '')
					{
						$sql = "update postype_notifications SET " .
							   "postype_notification_email2 = " . dbquote($form->value("postype_notification_email2")) . 
							   " where postype_notification_postype = " .$row["postype_id"]  .
							   " and postype_notification_prodcut_line = " . $row2["product_line_id"];

						$result = mysql_query($sql) or dberror($sql);
					}

					if($form->value("postype_notification_email3") != '')
					{
						$sql = "update postype_notifications SET " .
							   "postype_notification_email3 = " . dbquote($form->value("postype_notification_email3")) . 
							   " where postype_notification_postype = " .$row["postype_id"]  .
							   " and postype_notification_prodcut_line = " . $row2["product_line_id"];

						$result = mysql_query($sql) or dberror($sql);
					}

					if($form->value("postype_notification_email4") != '')
					{
						$sql = "update postype_notifications SET " .
							   "postype_notification_email4 = " . dbquote($form->value("postype_notification_email4")) . 
							   " where postype_notification_postype = " .$row["postype_id"]  .
							   " and postype_notification_prodcut_line = " . $row2["product_line_id"];

						$result = mysql_query($sql) or dberror($sql);
					}

					if($form->value("postype_notification_email5") != '')
					{
						$sql = "update postype_notifications SET " .
							   "postype_notification_email5 = " . dbquote($form->value("postype_notification_email5")) . 
							   " where postype_notification_postype = " .$row["postype_id"]  .
							   " and postype_notification_prodcut_line = " . $row2["product_line_id"];

						$result = mysql_query($sql) or dberror($sql);
					}

					if($form->value("postype_notification_email6") != '')
					{
						$sql = "update postype_notifications SET " .
							   "postype_notification_email6 = " . dbquote($form->value("postype_notification_email6")) . 
							   " where postype_notification_postype = " .$row["postype_id"]  .
							   " and postype_notification_prodcut_line = " . $row2["product_line_id"];

						$result = mysql_query($sql) or dberror($sql);
					}

					if($form->value("postype_notification_email7") != '')
					{
						$sql = "update postype_notifications SET " .
							   "postype_notification_email7 = " . dbquote($form->value("postype_notification_email7")) . 
							   " where postype_notification_postype = " .$row["postype_id"]  .
							   " and postype_notification_prodcut_line = " . $row2["product_line_id"];

						$result = mysql_query($sql) or dberror($sql);
					}

					if($form->value("postype_notification_email8") != '')
					{
						$sql = "update postype_notifications SET " .
							   "postype_notification_email8 = " . dbquote($form->value("postype_notification_email8")) . 
							   " where postype_notification_postype = " .$row["postype_id"]  .
							   " and postype_notification_prodcut_line = " . $row2["product_line_id"];

						$result = mysql_query($sql) or dberror($sql);
					}
				}
			}
		}
	}
	*/
	redirect("pos_type_notifications.php");

}

$page = new Page("postype_notifications");

$page->header();
$page->title("Project Notification Recipients");

$form->render();

$page->footer();

?>
