<?php
/********************************************************************

    logistics_items.php

    Lists items for editing.

    Created by:     Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date created:   2015-01-06
    Modified by:    Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-06
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(has_access("can_edit_catalog") 
  or has_access("can_edit_logistics_system_data"))
{

}
else
{
	check_access("can_edit_catalog");
}
set_referer("logistics_item.php");



$sql = "select DISTINCT item_id, item_code, item_name, " .
		 "item_width, item_height, item_length, item_radius, item_net_weight, " . 
		 " item_gross_weight, unit_name, packaging_type_name, " .
		 " item_width*item_height*item_length/1000000 as cbm, " .
		 " if(item_stackable = 1, 'yes', '') as stackable " .
		 "from items " .
		 "left join suppliers on supplier_item = item_id " .
		 "left join units on unit_id = item_unit " . 
		 "left join packaging_types on packaging_type_id = item_packaging_type";

$filter = "item_active = 1 and item_type = 1";


if(param("supplier_filter") === '') {
	if(array_key_exists("supplier_filter", $_SESSION))
	{
		unset($_SESSION["supplier_filter"]);
	}
}
elseif(param("supplier_filter")) {
	$filter .= " and supplier_address = " . param("supplier_filter");
	$_SESSION["supplier_filter"] = param("supplier_filter");
}
elseif(array_key_exists("supplier_filter", $_SESSION))
{
	$filter .= " and supplier_address = " . $_SESSION["supplier_filter"];
	param("supplier_filter",$_SESSION["supplier_filter"]);
}


if(param("category_filter") === '') {
	if(array_key_exists("category_filter", $_SESSION))
	{
		unset($_SESSION["category_filter"]);
	}
}
elseif(param("category_filter")) {
	
	$filter .= " and item_category = " . param("category_filter");
	$_SESSION["category_filter"] = param("category_filter");
}
elseif(array_key_exists("category_filter", $_SESSION))
{
	$filter .= " and item_category = " . $_SESSION["category_filter"];
	param("category_filter", $_SESSION["category_filter"]);
}


//get purchase price


$suppliers = array();
$images = array();

$sql_p = "select item_id " . 
		 "from items " .
		 "left join suppliers on supplier_item = item_id " .
		 "left join currencies on currency_id = supplier_item_currency " . 
		 " where item_active = 1 and item_type = 1";


$res = mysql_query($sql_p) or dberror($sql_p);

while ($row = mysql_fetch_assoc($res))
{
    
	$sql_s = "select address_shortcut " .
	         "from items " . 
		     "left join suppliers on supplier_item = item_id " . 
			 "left join addresses on address_id = supplier_address ". 
			 " where item_id = " . $row["item_id"];

	$res_s = mysql_query($sql_s) or dberror($sql_s);

	while ($row_s = mysql_fetch_assoc($res_s))
	{
		if(array_key_exists($row["item_id"],$suppliers ))
		{
			$suppliers[$row["item_id"]] .= ', ' . $row_s["address_shortcut"];
		}
		else
		{
			$suppliers[$row["item_id"]] = $row_s["address_shortcut"];
		}
	}



	//set icons for picture indicating column
	

	$sql_i = "select item_file_item " . 
			 "from item_files " .
			 "where item_file_item = " . $row["item_id"];

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	if ($row_i = mysql_fetch_assoc($res_i))
	{
		$link = "<a href=\"javascript:popup('item_view_files.php?id=" . $row["item_id"] . "', 800, 640)\"><img border=\"0\" src=\"/pictures/document_view.gif\" /></a>";
		
		$images[$row["item_id"]] = $link;
	}
}


/********************************************************************
    Create Filter
*********************************************************************/ 
$sql_suppliers = "select DISTINCT supplier_address, address_company from suppliers " . 
                 "left join addresses on address_id = supplier_address " . 
				 "where address_active = 1 " . 
				 " order by address_company";

$sql_item_categories = "select item_category_id, item_category_name from item_categories " .
    "order by item_category_name";


$supplier_filter = array();
$res = mysql_query($sql_suppliers);
while($row = mysql_fetch_assoc($res))
{
	$supplier_filter[$row["supplier_address"]] = $row["address_company"];
}

$category_filter = array();
$res = mysql_query($sql_item_categories);
while($row = mysql_fetch_assoc($res))
{
	$category_filter[$row["item_category_id"]] = $row["item_category_name"];
}


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("items");
$list->set_order("item_code");
$list->set_filter($filter);

$list->add_listfilters("supplier_filter", "Supplier", 'select', $supplier_filter, param("supplier_filter"));
$list->add_listfilters("category_filter", "Category", 'select', $category_filter, param("category_filter"));

$list->add_column("item_code", "Code", "logistics_item.php?id={item_id}", "", COLUMN_NO_WRAP);
$list->add_text_column("pix", "Pix", COLUMN_UNDERSTAND_HTML, $images);

$list->add_column("item_name", "Name");
$list->add_text_column("supplier", "Suppliers", 0, $suppliers);

$list->add_column("unit_name", "Unit");
$list->add_column("item_width", "Width");
$list->add_column("item_height", "Height");
$list->add_column("item_length", "Length");
$list->add_column("cbm", "CBM");
$list->add_column("item_radius", "Radius");
$list->add_column("item_net_weight", "Net Weight");
$list->add_column("item_gross_weight", "Gross Weight");
$list->add_column("packaging_type_name", "Packaging");
$list->add_column("stackable", "Stackable");

$list->add_button("print", "Print List");

$list->process();


if($list->button("print"))
{
	$link = "logistics_items_xls.php?su=" . param("supplier_filter") . "&ca=" . param("category_filter");
	redirect($link);
}


$page = new Page("logistics");
require "include/logistics_page_actions.php";

$page->header();
$page->title("Items");

$list->render();
$page->footer();

?>
