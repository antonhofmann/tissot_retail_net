<?php
/********************************************************************

    ps_spent_for.php

    Creation and mutation of project sheet spent fors.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-27
    Version:        1.0.0

    Copyright (c) 2004, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("ps_spent_fors", "ps_spent_for");

$form->add_section();
$form->add_edit("ps_spent_for_name", "Spent For Name", NOTNULL | UNIQUE);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("ps_spent_fors");
$page->header();
$page->title(id() ? "Edit Project Sheet Spent For Names" : "Add Project Sheet Spent For Names");
$form->render();
$page->footer();

?>