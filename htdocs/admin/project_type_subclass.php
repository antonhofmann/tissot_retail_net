<?php
/********************************************************************

    project_type_subclass.php

    Creation and mutation of project type subclasses.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-12-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-12-09
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");


$sql_project_managers = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username " . 
                        " from users " . 
						" left join user_roles on user_role_user = user_id " . 
						" where user_active = 1  " . 
						" and user_role_role in (3) " .
						" order by user_name, user_firstname";

$sql_logistics_coordinatros = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username " . 
                        " from users " . 
						" left join user_roles on user_role_user = user_id " . 
						" where user_active = 1  " . 
						" and user_role_role in (2) " .
						" order by user_name, user_firstname";


$form = new Form("project_type_subclasses", "Project Type Subclass");

$form->add_section();
$form->add_edit("project_type_subclass_name", "Name*", NOTNULL | UNIQUE);
$form->add_list("project_type_subclass_project_manager", "Standard Project Leader", $sql_project_managers, 0);
//$form->add_list("project_type_subclass_logistics_coordinator", "Standard Logistics Coordinator", $sql_logistics_coordinatros, 0);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("project_type_subclasses");
$page->header();
$page->title(id() ? "Edit Project Type Subclass" : "Add Project Type Subclass");
$form->render();
$page->footer();

?>