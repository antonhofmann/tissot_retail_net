<?php
/********************************************************************

    snagging_template.php

    Creation and mutation of snagging_list templates.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2003-05-06
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-05-14
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

//check_access("can_edit_catalog");
set_referer("snagging_template_element.php");


$page = new Page("snagging_list_templates");
$page->header();
$page->title("Preview Snagging Template");
echo "This function is not yet implemented.";
$page->footer();

?>