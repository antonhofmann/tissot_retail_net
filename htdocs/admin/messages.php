<?php
/********************************************************************

    messages.php

    Lists messages for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-02
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-08-19
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("message.php");



$archive_filter = array();
$archive_filter[1] = "Archived Entries";




$list = new ListView("select message_id, message_date, message_expiry_date, message_title " .
                     "from messages");

$list->set_entity("messages");
$list->set_order("message_date desc");

if(param('archive') > 0)
{
	$list->set_filter("message_expiry_date < " . dbquote(date("Y-m-d")));
}
else
{
	$list->set_filter("message_expiry_date >= " . dbquote(date("Y-m-d")) . " or (message_expiry_date is null or message_expiry_date = '0000-00-00')");
}

$list->add_listfilters("archive", "Archive", 'select', $archive_filter, param("archive"));

$list->add_column("message_title", "Title", "message.php?archive=" . param("archive"), LIST_FILTER_FREE);
$list->add_column("message_date", "Date", "", LIST_FILTER_FREE);
$list->add_column("message_expiry_date", "Expiry", "", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_NEW, "New", "message.php");

$list->process();

$page = new Page("messages");

$page->header();
$page->title("Messages");
$list->render();
$page->footer();

?>
