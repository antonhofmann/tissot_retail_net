<?php
/********************************************************************

    logistics_address.php

    Edit address records.

    Created by:     Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date created:   2015-01-06
    Modified by:    Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-06
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(has_access("can_edit_catalog") 
  or has_access("can_edit_logistics_system_data"))
{

}
else
{
	check_access("can_edit_catalog");
}


// Build form

$form = new Form("addresses", "address");

$form->add_section("Name and address");

$form->add_label("address_number", "Tissot SAP Customer Code Merchandising");
$form->add_label("address_number2", "Tissot SAP Customer Code Retail Furniture");
$form->add_label("address_sapnr", "SAP Customer Code");
$form->add_label("address_sap_salesorganization", "SAP Sales Organization");
$form->add_label("address_company", "Company");
$form->add_label("address_address", "Street");


$form->add_lookup("address_country", "Country", "countries", "country_name");

$form->add_label("address_zip", "Zip");
$form->add_label("address_place", "City");

$form->add_section("Communication");
$form->add_label("address_phone", "Phone");
$form->add_label("address_mobile_phone", "Mobile Phone");
$form->add_label("address_email", "Email");
$form->add_label("address_contact_name", "Contact Name");
$form->add_label("address_website", "Website");

$form->add_section("Logistics");
$form->add_edit("address_vat_id", "VAT ID");
$form->add_edit("address_financecontroller_name", "Name of finance controller");
$form->add_hidden("address_financecontroller_phone");

$form->add_multi_edit("financecontroller_phone_number", array("address_financecontroller_phone_country", "address_financecontroller_phone_area", "address_financecontroller_phone_number"), "Finance controller's phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($country_phone_prefix, '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

$form->add_edit("address_financecontroller_email", "Email of finance controller");


$form->add_multiline("address_logistics_fedex_dhl", "FEDEX/DHL", 2);
$form->add_checkbox("address_logistics_coo", "to ship original COO and invoice to client",0, 0, "Original COO and Invoice");
$form->add_multiline("address_logistics_notes", "Notes", 2);
$form->add_multiline("address_logistics_comments", "Comments", 6);

$form->add_button("save", "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");


// Populate form and process button clicks

$form->populate();
$form->process();


if($form->button("save"))
{
	$form->value("address_financecontroller_phone", $form->unify_multi_edit_field($form->items["financecontroller_phone_number"]));
	
	$form->add_validation("{address_financecontroller_phone} != ''", "Please indcate the phone number!");
	if($form->validate())
	{
		

		$form->save();
	}
}

// Render page

$page = new Page("logistics");

require "include/logistics_page_actions.php";

$page->header();
$page->title(id() ? "Edit Client Data" : "Add Client");
$form->render();



$page->footer();

?>