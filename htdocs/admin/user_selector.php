<?php
/********************************************************************

    user_selector.php

    Enter User List

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-12-02
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-12-02
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

check_access("can_edit_catalog");


/********************************************************************
    prepare all data needed
*********************************************************************/
if(param("context") == 'user') {
	$user_id = param("user_id");
	$address_id = param("address_id");
	$context = param("context");

	
	$sql_users = "select user_id, concat(user_name, ' ', user_firstname) as username " .
					   "from users " .
					   " where user_active = 1 and user_address = " . dbquote($address_id) . 
					   " order by username";

	$user_list = array();
	$res = mysql_query($sql_users) or dberror($sql_users);
	while($row = mysql_fetch_assoc($res)) 
	{
		$user_list[] = $row["user_id"];
	}

	$users = array();
	$sql = "select project_access_granted_user, concat(user_name, ' ', user_firstname) as username " . 
		   "from project_access " . 
		   "left join users on user_id = project_access_granted_user " . 
		   "where project_access_owner_user = " . $user_id . 
		   " order by username";
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res)) 
	{
		$users[] = $row["project_access_granted_user"];
	}
}
else
{
	exit;
}


/********************************************************************
    save data
*********************************************************************/

if(param("save_form") == "user")
{
	$selected_users = array();

	$res = mysql_query($sql_users) or dberror($sql_users);
	while($row = mysql_fetch_assoc($res))
	{
		if($_POST["US_" . $row["user_id"]])
		{
			$selected_users[] = $row["user_id"];
		}
	}
	
	$sql = "delete from project_access " . 
		   "where project_access_owner_user = " . $user_id;

	$result = mysql_query($sql) or dberror($sql);

	foreach($selected_users as $key=>$granted_user_id) 
	{
		$sql = "insert into project_access (" . 
			   "project_access_owner_user, project_access_granted_user, user_created, date_created ) VALUES (" . 
			   $user_id . ", " . $granted_user_id . ", '" . user_login() . "', '" . date("Y-m-d:H:i:s") . "')";

		$result = mysql_query($sql) or dberror($sql);
	
	}
}



/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("users", "user_selector");
$form->add_hidden("user_id", param("user_id"));
$form->add_hidden("address_id", param("address_id"));
$form->add_hidden("context", $context);

$page_title = "User Selector";
$form->add_hidden("save_form", $context);

$form->add_label("select0", "Users", RENDER_HTML, "<div id='select0'><a href='javascript:select_all_users();'>select all users</a></div>");

$res = mysql_query($sql_users) or dberror($sql_users);
while($row = mysql_fetch_assoc($res))
{
	if(in_array($row["user_id"], $users))
	{
		$form->add_checkbox("US_" . $row["user_id"], $row["username"], true);
	}
	else
	{
		$form->add_checkbox("US_" . $row["user_id"], $row["username"], false);
	}
	
}

$form->add_input_submit("submit", "Save Values", 0);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("users");

$page->header();
$page->title($page_title);

$form->render();


if(param("save_form"))
{
?>

<script languege="javascript">
var back_link = "user.php?id=<?php echo param('user_id');?>&address=<?php echo param('address_id');?>"; 
$.nyroModalRemove();
</script>

<?php
}
?>

<script language='javascript'>

	function select_all_users()
	{
		<?php
		foreach($user_list as $key=>$user_id)
		{

			echo "$('input[name=US_" . $user_id . "]').attr('checked', true);";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:deselect_all_users();'>deselect all users</a>";
		
		
	}

	function deselect_all_users()
	{
		<?php
		foreach($user_list as $key=>$user_id)
		{
			echo "$('input[name=US_" . $user_id . "]').attr('checked', false);";
		}
		?>

		var div = document.getElementById('select0');
		div.innerHTML = "<a href='javascript:select_all_users();'>select all users</a>";
	}

</script>



<?php

$page->footer();
