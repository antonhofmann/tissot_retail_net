<?php
/********************************************************************

    closing_dates.php

    Lists clsoing dates for editing .

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-27
    Version:        1.0.0

    Copyright (c) 2004, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("closing_date.php");

$sql = "select closing_date_id, closing_date_name from closing_dates";

$list = new ListView($sql);

$list->set_entity("closing_dates");
$list->set_order("closing_date_name");

$list->set_filter("closing_date_active = 1");

$list->add_column("closing_date_name", "Closing Date Name", "closing_date.php");

$list->add_button(LIST_BUTTON_NEW, "New", "closing_date.php");

$list->process();

$page = new Page("closing_dates");

$page->header();
$page->title("Closing Date Names");
$list->render();
$page->footer();

?>
