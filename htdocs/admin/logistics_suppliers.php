<?php
/********************************************************************

    logistics_suppliers.php

    Lists suppliers for editing.

    Created by:     Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date created:   2015-01-06
    Modified by:    Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-06
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(has_access("can_edit_catalog") 
  or has_access("can_edit_logistics_system_data"))
{

}
else
{
	check_access("can_edit_catalog");
}
set_referer("logistics_supplier.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
$sql = "select address_id, address_company, address_zip,  " .
	 "    country_name, address_type_name, address_standard_custom_fee_amount, currency_symbol " . 
	 "from addresses " .
	 " left join address_types on address_type_id = address_type " .
	 " left join currencies on currency_id = address_standard_custom_fee_currency " . 
	 "left join countries on address_country = country_id ";

$list_filter = "address_id <> 5 and address_active = 1 and address_type = 2 ";


/********************************************************************
    Create List
*********************************************************************/ 


$list = new ListView($sql);

$list->set_entity("addresses");
$list->set_order("country_name, address_company");
$list->set_filter($list_filter);

$list->add_column("country_name", "Country");
$list->add_column("address_company", "Company", "logistics_supplier.php");
$list->add_column("address_standard_custom_fee_amount", "Custom Fee", "logistics_supplier.php");
$list->add_column("currency_symbol", "Currency", "logistics_supplier.php");


/********************************************************************
    Populate and process button clicks
*********************************************************************/ 
$list->process();


/********************************************************************
   Create Page
*********************************************************************/ 
$page = new Page("logistics");

require "include/logistics_page_actions.php";

$page->header();

$page->title("Retail Suppliers");
$list->render();
$page->footer();

?>
