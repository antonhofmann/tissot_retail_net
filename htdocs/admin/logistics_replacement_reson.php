<?php
/********************************************************************

    logistics_replacement_reson.php

    Creation and mutation of reasons for item replacements.

    Created by:     Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date created:   2015-01-06
    Modified by:    Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-06
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(has_access("can_edit_catalog") 
  or has_access("can_edit_logistics_system_data"))
{

}
else
{
	check_access("can_edit_catalog");
}


/****************************************************************************
/****************************************************************************
create form
****************************************************************************/

$form = new Form("order_item_replacement_reasons", "Reason for Replacement");


$form->add_edit("order_item_replacement_reason_text", "Text*", NOTNULL | UNIQUE);

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

$form->populate();
$form->process();




if($form->button(FORM_BUTTON_SAVE))
{
	redirect("logistics_replacement_resons.php");
}

$page = new Page("logistics");
require "include/logistics_page_actions.php";

$page->header();
$page->title(id() ? "Edit Replacement Reason" : "Replacement Reason");
$form->render();

$page->footer();

?>