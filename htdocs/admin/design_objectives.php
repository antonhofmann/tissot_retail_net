<?php
/********************************************************************

    design_objectives.php

    Lists design objectives for editing.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-05
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2010-12-03
    Version:        2.0.0

    Copyright (c) 2002-2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("design_objective.php");

/* old version depending on product line
$list = new ListView("select design_objective_item_id, design_objective_item_name, " .
                     "    design_objective_group_name, product_line_name " .
                     "from design_objective_items left join design_objective_groups on design_objective_item_group = design_objective_group_id " .
                     "    left join product_lines on design_objective_group_product_line = product_line_id");
                     
$list->set_entity("design objectives");
$list->set_order("product_line_name, design_objective_group_name, design_objective_item_priority");

$list->add_column("product_line_name", "Product Line", "", LIST_FILTER_LIST,   
    "select product_line_name from product_lines order by product_line_name");
$list->add_column("design_objective_group_name", "Group", "", LIST_FILTER_LIST,   
    "select distinct design_objective_group_name from design_objective_groups order by design_objective_group_name");
$list->add_column("design_objective_item_name", "Design Objective", "design_objective.php", LIST_FILTER_FREE);

$list->add_button(LIST_BUTTON_NEW, "New", "design_objective.php");
$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");

*/


// new version depending on pos type
$list = new ListView("select design_objective_item_id, design_objective_item_name, " .
                     "    design_objective_group_name, " . 
					 "    if (design_objective_item_isstandard, 'yes', 'no') as design_objective_item_isstandard, " . 
					 "     postype_name " .
                     "from design_objective_items left join design_objective_groups on design_objective_item_group = design_objective_group_id " .
                     "    left join postypes on design_objective_group_postype = postype_id");
                     
$list->set_entity("design objectives");
$list->set_order("postype_name, design_objective_group_name, design_objective_item_priority");
$list->set_filter("design_objective_group_postype > 0");

$list->add_column("postype_name", "POS Type");
$list->add_column("design_objective_group_name", "Group");
$list->add_column("design_objective_item_name", "Design Objective", "design_objective.php");
$list->add_column("design_objective_item_isstandard", "Default", "", "", "", COLUMN_ALIGN_CENTER);

$list->add_button(LIST_BUTTON_NEW, "New", "design_objective.php");


$list->process();

$page = new Page("design_objectives");

$page->header();
$page->title("Design Objectives");
$list->render();
$page->footer();

?>
