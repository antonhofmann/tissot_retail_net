<?php
/********************************************************************

    pcost_templates.php

    Lists project cost templates for editing .

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("pcost_template.php");

$sql = "select pcost_template_id, pcost_template_name, postype_name " .
       "from pcost_templates " .
	   "left join postypes on postype_id = pcost_template_postype_id ";

$list = new ListView($sql);

$list->set_entity("pcost_templates");
$list->set_order("pcost_template_name");

$list->add_column("pcost_template_name", "Template Name", "pcost_template.php");
$list->add_column("postype_name", "POS Type");

$list->add_button(LIST_BUTTON_NEW, "New", "pcost_template.php");

$list->process();

$page = new Page("pcost_templates");

$page->header();
$page->title("Project Cost Templates");
$list->render();
$page->footer();

?>
