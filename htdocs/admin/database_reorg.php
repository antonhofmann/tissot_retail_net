<?php
/********************************************************************

    database_reorg.php

    Reorganize Database

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-12-04
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2002-12-04
    Version:        1.0.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("database_reorg.php");


/********************************************************************
    Create  Form
*********************************************************************/
$form = new Form("", "", 600);


if($form->button("reorg"))
{
    $form->add_section("The data base has been reorganized");
    $form->add_label("title", "Order ID", 0, "Order Number");
    $form->add_label("spacer", "");


    //reorganize database

    $sql = "select order_id, order_number " .
           "from orders " .
           "where order_archive_date is not null ".
           "   and order_archive_date <>0";

    $res = mysql_query($sql) or dberror($sql);

    $i = 1;
    while ($row = mysql_fetch_assoc($res) and $i < 31)
    {
        // move record to archive
        $result = move_order_to_archive($db, "orders", $row["order_id"]);
        $result = delete_record_from_active_tables("orders", $row["order_id"]);
        $i++;
        $form->add_label("O" . $row["order_id"], $row["order_id"], 0, $row["order_number"]);

    }
        
}
else
{
    $form->add_section("This function will reorganize the data base");
    $form->add_button("reorg", "Start Reorganizing");
}

$form->populate();
$form->process();





/********************************************************************
    Create Page
*********************************************************************/
$page = new Page("strings");

$page->header();
$page->title("Reorganize Data Base");
$form->render();
$page->footer();


?>
