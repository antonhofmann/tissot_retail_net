<?php
/********************************************************************

    closing_date.php

    Creation and mutation of clsoing dates.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-27
    Version:        1.0.0

    Copyright (c) 2004, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("closing_dates", "closing_date");

$form->add_section();
$form->add_edit("closing_date_name", "Closing Date Name", NOTNULL | UNIQUE);
$form->add_checkbox("closing_date_active", "is active", true, 0, "Active");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
//$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("closing_dates");
$page->header();
$page->title(id() ? "Edit Closing Date Name" : "Add Closing Date Name");
$form->render();
$page->footer();

?>