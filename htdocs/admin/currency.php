<?php
/********************************************************************

    currency.php

    Creation and mutation of currency records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-30
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-10-10
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/admin_functions.php";

check_access("can_edit_catalog");

$form = new Form("currencies", "currency");

$form->add_edit("currency_name", "Name*", NOTNULL);
$form->add_edit("currency_symbol", "Code*", NOTNULL | UNIQUE);
$form->add_edit("currency_currency_symbol", "Symbol", 0);

$form->add_edit("currency_exchange_rate", "Exchange Rate Clients*", NOTNULL);
$form->add_edit("currency_exchange_rate_furniture", "Exchange Rate Suppliers", 0);
$form->add_edit("currency_factor", "Factor*", NOTNULL);
//$form->add_checkbox("currency_istop", "Is editable in List", 0, 0, $caption1 = "Currency");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->add_validation("is_upper_case({currency_symbol})", "The currency symbol must be given in uppercase characters.");

$form->populate();
$form->process();


if($form->button(FORM_BUTTON_SAVE)) {
	
	
	//update all items and onging orders and projects with new prices
	$result = update_new_prices();
	
	if($form->validate()) {
	
		$link = '/admin/currencies.php';
		redirect($link);
	}
}

$page = new Page("currencies");
$page->header();
$page->title(id() ? "Edit Currency" : "Add Currency");
$form->render();
$page->footer();

?>