<?php
/********************************************************************

    cost_centers.php

    Lists roles for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-27
    Version:        1.0.0

    Copyright (c) 2004, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("cost_center.php");

$sql = "select cost_center_id, cost_center_name from cost_centers ";

$list = new ListView($sql);

$list->set_entity("cost_centers");
$list->set_order("cost_center_name");

$list->add_column("cost_center_name", "Cost Center", "cost_center.php");

$list->add_button(LIST_BUTTON_NEW, "New", "cost_center.php");

$list->process();

$page = new Page("cost_centers");

$page->header();
$page->title("Cost Centers");
$list->render();
$page->footer();

?>
