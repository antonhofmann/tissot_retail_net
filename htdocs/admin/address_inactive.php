<?php
/********************************************************************

    address_inactive.php

    Creation and mutation of address records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-29
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-10-17
    Version:        1.0.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("user.php");
set_referer("address_file.php");


$address_type = 0;
$client_type = 0;

$sql = "select * from addresses where address_id = " . id();
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$address_type = $row["address_type"];
	$client_type = $row["address_client_type"];
}

$country_phone_prefix = '';
if(param("address_country"))
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = " . param("address_country") . " order by place_name";

	$country_phone_prefix = get_country_phone_prefix(param("address_country"));
}
else
{
	$sql_places = "select place_id, concat(place_name, ' (', province_canton, ')') as place " . 
				  "from places " .
				  "left join provinces on province_id = place_province " . 
				  "where place_country = {address_country} order by place_name";
}


$address_active = "";
$tracking_data = array();
$address_modified = "";
if(id())
{
	$sql = "select address_added_by, address_last_modified, address_last_modified_by, address_set_to_inactive_by, address_set_to_inactive_date, date_created, address_active " . 
		   "from addresses " . 
		   "where address_id = " . id();

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res)) 
	{
		
		$address_active = $row["address_active"];
		$tracking_data["address_added_by"] = $row["address_added_by"];
		$tracking_data["address_set_to_inactive_by"] = $row["address_set_to_inactive_by"];
		$tracking_data["address_last_modified_by"] = $row["address_last_modified_by"];

		$date_created["address_added_by"] = $row["date_created"];
		$date_created["address_set_to_inactive_by"] = $row["address_set_to_inactive_date"];
		$date_created["address_last_modified_by"] = $row["address_last_modified"];

		foreach($tracking_data as $key=>$user_id)
		{
			$sql = "select user_name, user_firstname " . 
				   "from users " . 
					"where user_id = " . dbquote($user_id);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) 
			{
				$tracking_data[$key] = $row["user_name"] . " " . $row["user_firstname"] . " " . $date_created[$key];
			}
		}
	}
}

// Build form

$form = new Form("addresses", "address");

$form->add_hidden("af", param("af"));

$form->add_section("Name and address");
$form->add_list("address_type", "Address Type",
    "select address_type_id, address_type_name from address_types  where address_type_id <> 7 order by address_type_name", SUBMIT | NOTNULL);

//$form->add_edit("address_number", "Tissot SAP Customer Code Merchandising");
//$form->add_edit("address_number2", "Tissot SAP Customer Code Retail Furniture");

if($client_type == 2 or $client_type == 3)
{
	$form->add_edit("address_legnr", "SG Legal Number");
}
else
{
	$form->add_hidden("address_legnr");
}
$form->add_edit("address_sapnr", "SAP Customer Code");


if($address_type == 1)
{
	$form->add_hidden("address_shortcut");
}
else
{
	$form->add_edit("address_shortcut", "Shortcut", NOTNULL | UNIQUE);
}
$form->add_edit("address_company", "Company", NOTNULL);
$form->add_edit("address_company2", "");
$form->add_hidden("address_address");
$form->add_multi_edit("street", array("address_street", "address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6), array(), 0, '', '', array(40, 5));
$form->add_edit("address_address2", "Additional Address Info");


$form->add_list("address_country", "Country*",
    "select country_id, country_name from countries order by country_name", NOTNULL | SUBMIT);

$form->add_edit("address_zip", "Zip");
$form->add_list("address_place_id", "City*", $sql_places, NOTNULL | SUBMIT);
$form->add_edit("address_place", "", DISABLED);

$form->add_section("Communication");
$form->add_hidden("address_phone");

$form->add_multi_edit("phone_number", array("address_phone_country", "address_phone_area", "address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($country_phone_prefix, '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

$form->add_hidden("address_mobile_phone");
$form->add_multi_edit("mobile_phone_number", array("address_mobile_phone_country", "address_mobile_phone_area", "address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($country_phone_prefix, '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

$form->add_edit("address_email", "Email");
$form->add_edit("address_contact_name", "Contact Name");
$form->add_edit("address_website", "Website");



if($address_type == 1) // client
{
	$sql_invoice_addresses = "select address_id, concat(country_name, ': ', address_company) as company from addresses " .
                 "left join countries on country_id = address_country " .
				 "where (address_active = 1 and address_type = 1) or  {address_invoice_recipient}=address_id order by country_name, address_company";


	$form->add_section("Different Billing Address");
	$form->add_comment("Please indicate the corresponding billing address in case invoices are not sent to the client.");
	$form->add_list("address_invoice_recipient", "Company",$sql_invoice_addresses);
}
else
{
	$form->add_hidden("address_invoice_recipient");
}


$form->add_section("Other information");

if(param("address_type") == 1) // client
{
	$form->add_hidden("address_currency", 1);
}
else
{
	$form->add_list("address_currency", "Currency","select currency_id, currency_symbol from currencies order by currency_symbol");
}


if($address_type == 1)
{
	$form->add_list("address_client_type", "Client Type",
		"select client_type_id, client_type_code from client_types order by client_type_code");
}
else
{
	$form->add_hidden("address_client_type");
}

$form->add_list("address_contact", "Contact",
    "select user_id, concat(user_name, ' ', user_firstname) from users " .
    "where user_active = 1 and user_address = " . id() . " order by user_name, user_firstname", OPTIONAL);

$form->add_section();
//$form->add_checkbox("address_canbefranchisor", "Can Be Franchisor", false);
$form->add_edit("address_company_is_partner_since", "Company is partner since", 0, "", TYPE_DATE);
$form->add_checkbox("address_canbefranchisee", "can own POS locations", false);
$form->add_checkbox("address_showinposindex", "Show Address in POS Index", false);


$form->add_comment("Please indicate the corresponding client address in case this address is a POS owner address.");

$sql_addresses = "select address_id, concat(country_name, ': ', address_company) as company from addresses " .
                 "left join countries on country_id = address_country " .
				 "where address_type = 1  order by country_name, address_company";

$form->add_list("address_parent", "Parent*",$sql_addresses);


$sql_addresses2 = "select address_id, concat(country_name, ': ', address_company) as company " . 
                 " from addresses " .
                 "left join countries on country_id = address_country " .
				 "where address_client_type in (2, 3) and address_type IN (1, 9)  order by country_name, address_company";

$form->add_list("address_totara_parent", "Legal Entity for Totara",$sql_addresses2);

$form->add_section();
$form->add_checkbox("address_active", "Address in Use", true);
$form->add_checkbox("address_checked", "Address checked", true);


$form->add_section('Merchandising Planning');
$form->add_checkbox("address_involved_in_planning", "can order Tissot merchandising material", false, 0, 'Merchandising');


$form->add_checkbox("address_can_own_independent_retailers", "can own independent retailers", false, 0, 'Retailers');
$form->add_checkbox("address_is_internal_address", "Address is an internal Address for MPS", false, 0, 'Internal Address');


$form->add_label("address_mps_customernumber", "Customer Number");
$form->add_label("address_mps_shipto", "Ship To Number");

$form->add_section('Launch Plan');
$form->add_checkbox("address_involved_in_lps", "can order Tissot LPS material", false, 0, 'Launch Plan');



$form->add_section('Retail Environment Development');
$form->add_checkbox("address_involved_in_red", "can be involved in retail environment development", false, 0, 'RED');


$form->add_subtable("users", "<br />Active Users", "user.php", "Name",
    "select user_id, user_firstname as Firstname, user_name as Name, user_phone as Phone, user_email as Email " .
    "from users " .
    "where user_active = 1 and user_address = " . id() . " " .
    "order by user_name, user_firstname");



if(!array_key_exists("address_added_by", $tracking_data))
{
	$form->add_hidden("address_added_by", user_id());
}
elseif(id() > 0) {
	$form->add_hidden("address_last_modified_by", user_id());
	$form->add_hidden("address_last_modified", date("Y-m-d H:i:s"));
}

if(array_key_exists("address_added_by", $tracking_data) and $tracking_data["address_added_by"] != "")
{
	$form->add_section("Tracking infos");
	$form->add_label("added_by", "Address created by", 0, $tracking_data["address_added_by"]);
	$form->add_label("modified_by", "Last modifed by", 0, $tracking_data["address_last_modified_by"]);

	if($address_active == 0 and array_key_exists("address_set_to_inactive_by", $tracking_data) and $tracking_data["address_set_to_inactive_by"] != "")
	{
		$form->add_label("set_to_inactive", "Address set to inactive by", 0, $tracking_data["address_set_to_inactive_by"]);
	}
}

$form->add_button("save", "Save");
$form->add_button("back", "Back");
$form->add_button("add_user", "Add User", "user.php?address=" . id(), OPTIONAL);

//$form->add_validation("is_shortcut({address_shortcut})", "The shortcut is invalid. It may only contain lower case characters a-z, digits and the underscore");
//$form->add_validation("is_email_address({address_email})", "The email address is invalid.");
//$form->add_validation("is_web_address({address_website})", "The website url is invalid.");

// Populate form and process button clicks

$form->populate();

if($form->value("address_canbefranchisee") == 1)
{
	$form->add_validation("{address_parent}", "You must indicate a parent address for a POS owner address.");
}
$form->process();

if($form->button("address_type"))
{
	if($form->value("address_type") == 1) // client
	{
		$form->value("address_canbefranchisee", 1);
		$form->value("address_showinposindex", 1);
	}
	elseif($form->value("address_type") == 12) //RED external partner
	{
		$form->value("address_involved_in_red", 1);
	}
}
elseif($form->button("address_place_id"))
{
	$sql = "select place_name from places where place_id = " . dbquote($form->value("address_place_id"));
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value("address_place", $row["place_name"]);
	}

}
elseif($form->button("address_country"))
{
	$form->value("address_place", "");
	$form->value("address_place_id", "");
	$form->value("address_zip", "");
}
elseif($form->button("back"))
{
	redirect("addresses_inactive.php?af=" . param("af"));
}
elseif($form->button("save"))
{
	
	/*
	if($form->value("address_type") == 1
		and $form->value("address_currency") != 1)
	{
		$form->error("The currency of a client address must be CHF!");
	}
	*/

	$form->value("address_phone", $form->unify_multi_edit_field($form->items["phone_number"]));
	$form->value("address_mobile_phone", $form->unify_multi_edit_field($form->items["mobile_phone_number"]));

	$form->add_validation("{address_phone} != '' or {address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");

	
	if($form->validate())
	{

		$form->value("address_address", $form->unify_multi_edit_field($form->items["street"], get_country_street_number_rule($form->value("address_country"))));

		
		
		$form->save();
	}
}



/********************************************************************
    Create List of files
*********************************************************************/ 

$sql_files = "select addressfile_id, addressfile_title, addressfile_path " . 
			 "from addressfiles ";
$list_filter = " addressfile_address = " . id();

//get all files
$files = array();
$sql = $sql_files . " where " . $list_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$link = "http://" . $_SERVER["HTTP_HOST"] . "/" . $row["addressfile_path"];
	$link = "<a href=\"javascript:popup('" . $link . "',800,600);\"><img src=\"/pictures/view.gif\" border='0'/></a>";
		
		$files[$row["addressfile_id"]] = $link;
}


$list = new ListView($sql_files);
$list->set_title("Files");
$list->set_entity("addressfiles");
$list->set_filter($list_filter);
$list->set_order("addressfile_title");

$list->add_text_column("files", "", COLUMN_UNDERSTAND_HTML, $files);

$link = "address_file.php?address=" . id();

$list->add_column("addressfile_title", "Title", $link, "", "", COLUMN_NO_WRAP);
$list->add_button("newfile", "Add File", "");

$list->populate();
//$list->process();

if($list->button("newfile"))
{
	$link = "address_file.php?address=" . id() . "&country=" . param("country");
	redirect($link);
}

// Render page

$page = new Page("addresses");

$page->header();
$page->title(id() ? "Edit Retail Net Address" : "Add Retail Net Address");
$form->render();

echo "<p>&nbsp;</p>";
$list->render();

$page->footer();

?>