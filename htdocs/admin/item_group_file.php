<?php
/********************************************************************

    item_group_file.php

    Creation and mutation of item file records of a group option.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-08-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-05
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("item_group_files", "group_file");

$form->add_hidden("option", param("option"));

$form->add_section();
$form->add_lookup("item_group_file_group_option", "Option", "item_group_options", "item_group_option_name", 0, param("option"));

$form->add_section();
$form->add_edit("item_group_file_title", "Title", NOTNULL);
$form->add_multiline("item_group_file_description", "Description", 4);

$group = make_valid_filename("group_" . param("option"));

$form->add_section();
$form->add_upload("item_group_file_path", "File", "/files/items/$group", NOTNULL);

$form->add_section();
$form->add_list("item_group_file_type", "Type",
    "select file_type_id, file_type_name from file_types order by file_type_name");
$form->add_list("item_group_file_purpose", "Purpose",
    "select file_purpose_id, file_purpose_name from file_purposes order by file_purpose_name");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();
$form->process();

$page = new Page("item_groups");

$page->header();
$page->title(id() ? "Edit Group Option File" : "Add Group Option File");
$form->render();
$page->footer();

?>
