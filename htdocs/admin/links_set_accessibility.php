<?php
/********************************************************************

    links_set_accessibility.php

    Set global accessibility for downloads and links.

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2013-06-06
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2013-06-06
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

check_access("can_edit_catalog");

if(!param("ltf"))
{
	exit;
}


///get links of the group
$links = array();
$sql = "select link_id " . 
       "from links " . 
	   "left join link_topics on link_topic = link_topic_id " . 
	   "where link_topic = " . param('ltf');

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$links[] = $row["link_id"];
}

$sql_roles = "select role_id, role_name from roles order by role_name";
$sql_countries = "select country_id, country_name from countries order by country_name";


$sql = "select link_topic_id, link_topic_name " .
	   "from link_topics " . 
	   "where link_topic_id = " . param("ltf");

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
$topic_id = $row["link_topic_name"];
$topic_name = $row["link_topic_name"];


/********************************************************************
    save data
*********************************************************************/
if(param("save_form") == 1)
{

	foreach($links as $key=>$link_id)
	{
	
		$sql = "delete from download_countries where download_countries_download = " . $link_id;
		$result = mysql_query($sql) or dberror($sql);

		$sql = "delete from download_roles where download_roles_download = " . $link_id;
		$result = mysql_query($sql) or dberror($sql);

		
		
		foreach($_POST["download_roles"] as $key=>$role_id)
		{
			$fields = array();
			$values = array();

			$fields[] = "download_roles_role";
			$values[] = $role_id;

			$fields[] = "download_roles_download";
			$values[] = $link_id;

			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "date_modified";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "user_modified";
			$values[] = dbquote(user_login());
			
			$sql = "insert into download_roles (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);

		}
		

		$res = mysql_query($sql_countries) or dberror($sql_countries);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["all_countries"] == 1 or in_array($row["country_id"], $_POST["download_countries"]))
			{
				$fields = array();
				$values = array();

				$fields[] = "download_countries_country";
				$values[] = $row["country_id"];

				$fields[] = "download_countries_download";
				$values[] = $link_id;

				$fields[] = "date_created";
				$values[] = "now()";

				$fields[] = "date_modified";
				$values[] = "now()";

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());
				
				$sql = "insert into download_countries (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
			}
		}
	}

	/*
	if($form->value("all_countries") == 1)
	{
		

		$sql = "select country_id from countries";
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			
			$fields = array();
			$values = array();

			$fields[] = "download_countries_country";
			$values[] = $row["country_id"];

			$fields[] = "download_countries_download";
			$values[] = id();

			$fields[] = "date_created";
			$values[] = "now()";

			$fields[] = "date_modified";
			$values[] = "now()";

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "user_modified";
			$values[] = dbquote(user_login());
			
			$sql = "insert into download_countries (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);

	
		}

		$link = "link.php?id=" . id() . "&ltf=" . param("ltf");
		redirect($link);

	}
	elseif($form->validate())
	{
		$link = "links.php?ltf=" .  param("ltf");
		redirect($link);
	}
	*/
}


/********************************************************************
    show form
*********************************************************************/

$form = new Form("links", "link");

$form->add_hidden("ltf", param("ltf"));
$form->add_hidden("save_form", 1);

$form->add_section();

$form->add_section("Accessibility for Roles");
$form->add_comment("Please indicate who is allowed to see documents of the topic \"" . $topic_name . "\".");
$form->add_checklist("download_roles", "", "download_roles", $sql_roles);

$form->add_section("Accessibility for Countries");
$form->add_comment("Please indicate the countries that can see documents of the topic \"" . $topic_name . "\".");
$form->add_checkbox("all_countries", "all countries", false);
$form->add_checklist("download_countries", "", "download_countries", $sql_countries);

$form->add_input_submit("submit", "Save Values", 0);


$form->populate();
$form->process();


$page = new Page_Modal("links");
$page->header();
$page->title($topic_name);
$form->render();

if(param("save_form"))
{
?>

<script languege="javascript">
var back_link = "links.php?ltf=<?php echo param('ltf');?>"; 
$.nyroModalRemove();
</script>

<?php
}
$page->footer();

?>