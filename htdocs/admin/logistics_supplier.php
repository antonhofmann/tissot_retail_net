<?php
/********************************************************************

    logistics_supplier.php

    Edit supplier records.

    Created by:     Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date created:   2015-01-06
    Modified by:    Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-06
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(has_access("can_edit_catalog") 
  or has_access("can_edit_logistics_system_data"))
{

}
else
{
	check_access("can_edit_catalog");
}


set_referer("logistics_supplier_invoice.php");


// Build form

$form = new Form("addresses", "address");

$form->add_section("Name and address");

$form->add_label("address_number", "Tissot SAP SAP Customer Code Merchandising");
$form->add_label("address_number2", "Tissot SAP SAP Customer Code Retail Furniture");
$form->add_label("address_sapnr", "SAP Customer Code");
$form->add_label("address_sap_salesorganization", "SAP Sales Organization");
$form->add_label("address_company", "Company");
$form->add_label("address_address", "Street");


$form->add_lookup("address_country", "Country", "countries", "country_name");

$form->add_label("address_zip", "Zip");
$form->add_label("address_place", "City");

$form->add_section("Communication");
$form->add_label("address_phone", "Phone");
$form->add_label("address_mobile_phone", "Mobile Phone");
$form->add_label("address_email", "Email");
$form->add_label("address_contact_name", "Contact Name");
$form->add_label("address_website", "Website");

$form->add_section("Standard Custom Fee");
$form->add_list("address_standard_custom_fee_currency", "Currency Custom Fee",
				"select currency_id, currency_symbol from currencies order by currency_symbol");
$form->add_edit("address_standard_custom_fee_amount", "Custom Fee", 0, "" , TYPE_DECIMAL, 10, 2);


$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");


// Populate form and process button clicks

$form->populate();
$form->process();


//invoice customer relations (only for suppliers)
$form2 = new Form("addresses", "address");

$form2->add_section("Supplier sends Invoice directly to the Following Clients");
$form2->add_subtable("supplier_client_invoices", "", "logistics_supplier_invoice.php?address=" . id(), "Company",
    "select supplier_client_invoice_id, address_company as Company, country_name as Country, " .
	"supplier_client_invoice_startdate as FromDate, supplier_client_invoice_enddate as ToDate " . 
    "from supplier_client_invoices " .
	"left join addresses on address_id = supplier_client_invoice_client " . 
	"left join countries on country_id = address_country " . 
	"where supplier_client_invoice_supplier = " . id() . 
    " order by address_company");

$form2->add_button("add_client", "Add/Edit Clients", "logistics_supplier_invoice.php?address=" . id(), OPTIONAL);

$form2->populate();
$form2->process();



// Render page

$page = new Page("logistics");

require "include/logistics_page_actions.php";

$page->header();
$page->title(id() ? "Edit Supplier Data" : "Add Supplier");
$form->render();
echo '<p>&nbsp;</p>';
$form2->render();

$page->footer();

?>