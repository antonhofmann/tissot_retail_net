<?php
/********************************************************************

    file_types.php

    Lists file types for editing.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2003-05-06
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2002-06-04
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

set_referer("snagging_template.php");

$sql = "select snagging_template_id, snagging_template_name, " .
                     "    snagging_template_product_line, product_line_name " .
                     "from snagging_templates " .
                     "left join product_lines on product_line_id = snagging_template_product_line ";

$list = new ListView($sql);
$list->set_entity("snagging_templates");
$list->set_order("snagging_template_name");

$list->add_column("snagging_template_name", "Name", "snagging_template.php", LIST_FILTER_FREE);

$list->set_group("product_line_name", "product_line_name");

$list->add_button(LIST_BUTTON_NEW, "New", "snagging_template.php");
$list->add_button(LIST_BUTTON_FILTER, "Filter");
$list->add_button(LIST_BUTTON_REMOVE_FILTER, "Remove filter");

$list->process();

$page = new Page("snagging_list_templates");

$page->header();
$page->title("Snagging List Templates");
$list->render();
$page->footer();

?>
