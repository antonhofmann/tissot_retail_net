<?php
/********************************************************************

    link_category.php

    Creation and mutation of link category records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-08-03
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-09-17
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("link.php");

$form = new Form("link_categories", "link category");

$form->add_section();
$form->add_edit("link_category_name", "Name", NOTNULL);
$form->add_edit("link_category_priority", "Priority");

$form->add_subtable("links", "<br />Links", "link.php", "Title",
    "select link_id, link_title as Title, link_url as URL " .
    "from links " .
    "where link_category = " . id() . " " .
    "order by link_title");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
$form->add_button("add_link", "Add link", "link.php?category=" . id(), OPTIONAL);

$form->populate();
$form->process();

$page = new Page("link_categories");
$page->header();
$page->title(id() ? "Edit Download/Link Category" : "Add Download/Link Category");
$form->render();
$page->footer();

?>