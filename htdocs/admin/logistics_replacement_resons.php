<?php
/********************************************************************

    logistics_replacement_resons.php

    Lists resons for item replacements for editing .

    Created by:     Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date created:   2015-01-06
    Modified by:    Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-06
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(has_access("can_edit_catalog") 
  or has_access("can_edit_logistics_system_data"))
{

}
else
{
	check_access("can_edit_catalog");
}
set_referer("logistics_replacement_reson.php");

$sql = "select order_item_replacement_reason_id, order_item_replacement_reason_text " . 
       "from order_item_replacement_reasons ";


$list = new ListView($sql);

$list->set_entity("order_item_replacement_reasons");
$list->set_order("order_item_replacement_reason_text");

$list->add_column("order_item_replacement_reason_text", "Text", "logistics_replacement_reson.php");
$list->add_button(LIST_BUTTON_NEW, "New", "logistics_replacement_reson.php");

$list->process();

$page = new Page("logistics");
require "include/logistics_page_actions.php";
$page->header();
$page->title("Reasons for Replacements");
$list->render();
$page->footer();

?>
