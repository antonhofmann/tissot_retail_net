<?php
/********************************************************************

    logistics_page_actions.php

    Defines the possible page actions depending on access rights

    Created by:     Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date created:   2015-01-06
    Modified by:    Anton Hofmann (Anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-06
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

$page->register_action('addresses', 'Clients', "logistics_addresses.php");
$page->register_action('suppliers', 'Suppliers', "logistics_suppliers.php");

//$page->register_action('items', 'Items', "logistics_items.php");

$page->register_action('shippingdocs', 'Standard Shipping Documents', "logistics_standard_shipping_documents.php");

?>