<?php
/********************************************************************

    lwitems.php

    Lists Local Construction Cost Positions

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-06
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("lwitem.php");

$sql = "select lwitem_id, lwitem_group, lwitem_code, " .
       "lwitem_text, lwitem_action, lwitem_unit, " .
       "concat(lwgroup_code,  ' ', lwgroup_text) as lwgroup " .
       "from lwitems " .       
       "left join lwgroups on lwgroup_id = lwitem_group ";


$list = new ListView($sql);

$list->set_entity("lwitems");
$list->set_group("lwgroup");
$list->set_order("lwitem_code");

$list->add_column("lwitem_code", "Code", "lwitem.php");
$list->add_column("lwitem_text", "Description");
$list->add_column("lwitem_action", "Action", "", "", "", COLUMN_NO_WRAP);
$list->add_column("lwitem_unit", "Unit", "", "", "", COLUMN_NO_WRAP);

$list->add_button(LIST_BUTTON_NEW, "New", "lwitem.php");

$list->process();

$page = new Page("lwitems");

$page->header();
$page->title("Local Construction Work Positions");
$list->render();
$page->footer();

?>
