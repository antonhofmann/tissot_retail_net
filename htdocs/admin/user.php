<?php
/********************************************************************

    user.php

    Mutation of user records.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.com)
    Date modified:  2015-04-11
    Version:        1.0.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../public/autorun.php";
require_once "../include/page_mvc.php";
require_once "include/admin_functions.php";

check_access("can_edit_catalog");

/***************************************************************************************
 	MVC HEADER
***************************************************************************************/
	
	$translate = translate::instance();
	$auth = User::instance();
	$user = new User(id());
	$roles = $user->roles($user->id);			
	$model = new Model(Connector::DB_CORE);
	
	$apps = $model->query("
		SELECT DISTINCT
			navigation_url
		FROM
			navigations
		INNER JOIN role_navigations ON role_navigation_navigation = navigation_id
		WHERE
			navigation_active = 1
		AND navigation_parent = 0
		AND navigation_id != 1
		AND role_navigation_role IN (".join(', ', $roles).")
	")->fetchAll();
	
/***************************************************************************************
 	/ MVC HEADER
***************************************************************************************/

//get user company
$address = "";
$address_id = 0;
$address_country = 0;
if(id() > 0)
{
	$sql = "select address_id, address_company, country_name, address_country " . 
		   "from addresses " . 
		   "left join countries on country_id = address_country " . 
		   "left join users on user_address = address_id " .
		   "where user_id = " . id();
}
elseif(param("address"))
{
	$sql = "select address_id, address_company, country_name, address_country " . 
       "from addresses " . 
	   "left join countries on country_id = address_country " . 
	   "left join users on user_address = address_id " .
	   "where address_id = " . param("address");
}



$country_phone_prefix = '';
$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res)) 
{
	$address = $row["address_company"] . " " . $row["country_name"];
	$address_id = $row["address_id"];
	$address_country = $row["address_country"];

	$country_phone_prefix = get_country_phone_prefix($address_country);
}


//get countries
/*
$sql_country = "select DISTINCT country_id, country_name " .
			   "from countries " .
			   "order by country_name";
*/
$countries = array();
$sql = "select country_access_country, country_name " . 
       "from country_access " . 
	   "left join countries on country_id = country_access_country " . 
	   "where country_access_user = " . id() . 
	   " order by country_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res)) 
{
	$countries[$row["country_access_country"]] = $row["country_name"];
}


//get users
/*
$sql_granted_users = "select user_id, concat(user_name, ' ', user_firstname) as username " .
					   "from users " .
					   " where user_address = " . dbquote($address_id) . 
					   " order by username";
*/
$granted_users = array();
$sql = "select project_access_granted_user, concat(user_name, ' ', user_firstname) as username " . 
       "from project_access " . 
	   "left join users on user_id = project_access_granted_user " . 
	   "where project_access_owner_user = " . id() . 
	   " order by username";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res)) 
{
	$granted_users[$row["project_access_granted_user"]] = $row["username"];
}

//get substitute user
$subsitute_user = 0;
$sql = "select user_substitute from users where user_id = " . id();

$res = mysql_query($sql) or dberror($sql);
if($row = mysql_fetch_assoc($res)) 
{
	$subsitute_user = $row["user_substitute"];
}

$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


$user_active = "";
$tracking_data = array();
$login_sent_by = "";
$user_modified = "";
if(id())
{
	$sql = "select user_added_by, user_last_modified, user_last_modified_by, user_set_to_inactive_by, user_set_to_inactive_date, date_created, user_active " . 
		   "from users " . 
		   "where user_id = " . id();

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res)) 
	{
		
		$user_active = $row["user_active"];
		$tracking_data["user_added_by"] = $row["user_added_by"];
		$tracking_data["user_set_to_inactive_by"] = $row["user_set_to_inactive_by"];
		$tracking_data["user_last_modified_by"] = $row["user_last_modified_by"];

		$date_created["user_added_by"] = $row["date_created"];
		$date_created["user_set_to_inactive_by"] = $row["user_set_to_inactive_date"];
		$date_created["user_last_modified_by"] = $row["user_last_modified"];

		foreach($tracking_data as $key=>$user_id)
		{
			$sql = "select user_name, user_firstname " . 
				   "from users " . 
					"where user_id = " . dbquote($user_id);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) 
			{
				$tracking_data[$key] = $row["user_name"] . " " . $row["user_firstname"] . " " . $date_created[$key];
			}
		}
	}


	//get date when login was sent
	$sql = "select  concat(user_name, ' ', user_firstname, ' ', mail_trackings.date_created) as login_sent_by"  . 
		   " from mail_trackings " . 
		   " left join users on user_id = mail_tracking_sender_user_id " . 
		   " where mail_tracking_mail_template_id = 13 " . 
		   " and mail_tracking_recipient_user_id = " . id() . 
		   " order by mail_tracking_id DESC";


	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res)) 
	{
		$login_sent_by = $row["login_sent_by"];
	}


}

$sql_companies = "select address_id, concat(address_company, ', ', place_name) as address_company " . 
                 "from addresses " . 
                 " left join places on place_id = address_place_id " .
                 "where address_country = " . dbquote($address_country) . 
				 " and (address_active = 1 or address_id = " . dbquote($address_id) . ") order by address_company";


//get user_roles
$retailnet_roles = get_user_roles(id(), 'retaildevelopment');
$mps_roles = get_user_roles(id(), 'mps');
$lps_roles = get_user_roles(id(), 'lps');
$red_roles = get_user_roles(id(), 'red');
$news_roles = get_user_roles(id(), 'news');
$marketing_roles = get_user_roles(id(), 'uberalldata');

// Build form

$form = new Form("users", "user");
$form->add_hidden("address", param("address"));
$form->add_hidden("user_address", param("address"));

$form->add_section("Personal data");
if(id())
{
	$form->add_list("user_address", "Company", $sql_companies, NOTNULL, $address_id);
}
else
{
	$form->add_label("company", "Company", 0, $address);
}


$form->add_section();
$form->add_edit("user_firstname", "First Name*", NOTNULL);
$form->add_edit("user_name", "Last Name*", NOTNULL);

$form->add_multiline("user_description", "Description/Position", 2);

$form->add_section("Communication");
$form->add_hidden("user_phone");

$form->add_multi_edit("phone_number", array("user_phone_country", "user_phone_area", "user_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($country_phone_prefix, '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


$form->add_hidden("user_mobile_phone");
$form->add_multi_edit("mobile_phone", array("user_mobile_phone_country", "user_mobile_phone_area", "user_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($country_phone_prefix, '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

/*
$form->add_hidden("user_fax");
$form->add_multi_edit("fax_number", array("user_fax_country", "user_fax_area", "user_fax_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($country_phone_prefix, '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));
*/

$form->add_edit("user_email", "Email*", NOTNULL);
$form->add_edit("user_email_cc", "CC");
$form->add_edit("user_email_deputy", "Deputy");



$form->add_section("Security");
$form->add_edit("user_login", "Username", UNIQUE);
$form->add_edit("user_password", "Password");

$form->add_section("Special Access Rules for My Company");
$form->add_checkbox("user_project_requester", "This user is not entitled to submit new projects", false, 0, "Projects");
$form->add_checkbox("user_can_set_swatch_roles_in_my_company", "Can set HQ roles for new users in My Company", true, 0, "My Company");

/*
$form->add_section("Special Access Rules for Projects and Catalogue Orders");
$form->add_checkbox("user_can_only_see_his_projects", "only to his own projects", true, 0, "Access");
$form->add_checkbox("user_can_only_see_projects_of_his_address", "only projects of his company", true, 0, "Access");
$form->add_checkbox("user_can_only_see_his_orders", "only to his own catalogue orders", true, 0, "Access");
$form->add_checkbox("user_can_only_see_orders_of_his_address", "only catalogue orders of his company", true, 0, "Access");
*/


$form->add_section("Special Access Rules for Projects");
$form->add_checkbox("user_can_only_see_his_projects", "only to his own projects", true, 0, "Access");
$form->add_checkbox("user_can_only_see_projects_of_his_address", "only projects of his company", true, 0, "Access");



$form->add_section("Other information");
$form->add_radiolist( "user_sex", "Sex",
	array("m" => "Male", "f" => "Female"), 0, "m");

$form->add_list("user_substitute", "Substitute",
	"select user_id, concat(user_name, ' ', user_firstname) " .
	"from users " .
	"where user_id = '" . $subsitute_user . "' or (user_active = 1 and user_address = {user_address} and user_id <> " . id() . ") " .
	"order by user_name, user_firstname");

$form->add_section();
$form->add_checkbox("user_active", "Active", true);
$form->add_checkbox("user_project_reciepient", "Appears as selectable email recipient for projects of his own country", false);
//$form->add_checkbox("user_order_reciepient", "Appears as selectable email recipient for catalogue orders of his own country", false);
$form->add_checkbox("user_cer_reciepient", "Appears as selectable email recipient in the LNR/CER mailbox for projects of his own country", false);
$form->add_checkbox("user_cer_mailbox_available_for_copy", "Can be selected as recipient for CC Mails in LNR/CER, projects and orders", false);

if(id() > 0) 
{

	$form->add_section("Roles");
	$form->add_label_selector("retailnet", "Retail Development", 0, implode(', ', $retailnet_roles), $icon, $link);
	
	/*
	$form->add_label_selector("mps", "MPS Tissot", 0, implode(', ', $mps_roles), $icon, $link);
	$form->add_label_selector("lps", "LPS Tissot", 0, implode(', ', $lps_roles), $icon, $link);
	$form->add_label_selector("red", "RED", 0, implode(', ', $red_roles), $icon, $link);
	$form->add_label_selector("news", "NEWS", 0, implode(', ', $news_roles), $icon, $link);
	$form->add_label_selector("uberalldata", "UBERALL", 0, implode(', ', $marketing_roles), $icon, $link);
	*/

	$form->add_section("Access Rules");
	$form->add_comment("Indicate the countries a user has access to.");
	$selected_countries = "";
	$i = 0;
	foreach($countries as $id=>$country_name)
	{
		if($i == 7)
		{
			$selected_countries .= "<br />";
			$i= 0;
		}
		$selected_countries .= $country_name . ", ";
		$i++;
	}
	$selected_countries = substr($selected_countries, 0, strlen($selected_countries) - 2);

	$form->add_label_selector("countries", "Countries", RENDER_HTML, $selected_countries, $icon, $link);

	
	$form->add_comment("Indicate the users having access to projects of this user.");
	$selected_users = "";
	$i = 0;
	foreach($granted_users as $id=>$username)
	{
		if($i == 5)
		{
			$selected_users .= "<br />";
			$i= 0;
		}
		$selected_users .= $username . ", ";
		$i++;
	}
	$selected_users = substr($selected_users, 0, strlen($selected_users) - 2);

	$form->add_label_selector("granted_users", "Granted Users", RENDER_HTML, $selected_users, $icon, $link);

	$form->add_hidden("new_user", 1);
	
}
else
{
	$form->add_hidden("new_user", 1);
}


if(!array_key_exists("user_added_by", $tracking_data))
{
	$form->add_hidden("user_added_by", user_id());
}
elseif(id() > 0) {
	$form->add_hidden("user_last_modified_by", user_id());
	$form->add_hidden("user_last_modified", date("Y-m-d H:i:s"));
}

if(array_key_exists("user_added_by", $tracking_data) and $tracking_data["user_added_by"] != "")
{
	$form->add_section("Tracking infos");
	$form->add_label("added_by", "User created by", 0, $tracking_data["user_added_by"]);
	$form->add_label("modified_by", "Last modifed by", 0, $tracking_data["user_last_modified_by"]);
	
	if($login_sent_by)
	{
		$form->add_label("login_snet_by", "Login data sent by", 0, $login_sent_by);
	}

	if($user_active == 0 and array_key_exists("user_set_to_inactive_by", $tracking_data) and $tracking_data["user_set_to_inactive_by"] != "")
	{
		$form->add_label("set_to_inactive", "User set to inactive by", 0, $tracking_data["user_set_to_inactive_by"]);
	}
}





$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button("save", "Save");

if(id() > 0) 
{
	
	$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
}

$form->add_button('send_login_data', 'Send Login Data', 'javascript:void(0)');


//$form->add_validation("is_unique_value_multiple('users', array('user_firstname', 'user_name'), array({user_firstname}, {user_name}), " . id() . ")",
//    "A user with this combination of firstname and name already exists.");
$form->add_validation("is_email_address({user_email})", "The email address is invalid.");

// Populate form and process button clicks

$form->populate();
$form->process();


if($form->button("save")) {
	
	
	$error = 0;
	if(!is_email_address($form->value("user_email")))
	{
		$error = 1;
		$form->error("Email address is invalid!");
	}
	elseif(!is_email_address($form->value("user_email_cc")))
	{
		$error = 1;
		$form->error("Email address CC is invalid!");
	}
	elseif(!is_email_address($form->value("user_email_deputy")))
	{
		$error = 1;
		$form->error("Email address deputy is invalid!");
	}


	$form->value("user_phone", $form->unify_multi_edit_field($form->items["phone_number"]));
	$form->value("user_mobile_phone", $form->unify_multi_edit_field($form->items["mobile_phone"]));

	$form->add_validation("{user_phone} != '' or {user_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");
	
	if($error == 0 and $form->validate())
	{
		
		//$form->value("user_fax", $form->unify_multi_edit_field($form->items["fax_number"]));
		
		$form->save();
	
		//delete user from standarroles
		//echo $form->value('user_active');

		if($form->value('user_active') != 1) {
			$sql = 'update standardroles set standardrole_rtco_user = 0 where standardrole_rtco_user = ' . id();
			mysql_query($sql) or dberror($sql);

			$sql = 'update standardroles set standardrole_dsup_user = 0 where standardrole_dsup_user = ' . id();
			mysql_query($sql) or dberror($sql);

			$sql = 'update standardroles set standardrole_rtop_user = 0 where standardrole_rtop_user = ' . id();
			mysql_query($sql) or dberror($sql);

			$sql = 'update standardroles set standardrole_cms_approver_user = 0 where standardrole_cms_approver_user = ' . id();
			mysql_query($sql) or dberror($sql);

			$sql = 'update users set user_substitute = 0 where user_substitute = ' . id();
			mysql_query($sql) or dberror($sql);

			$sql = 'update users set user_email_cc = "" where user_email_cc = ' . dbquote($form->value("user_email"));
			mysql_query($sql) or dberror($sql);

			$sql = 'update users set user_email_deputy = "" where user_email_deputy = ' . dbquote($form->value("user_email"));
			mysql_query($sql) or dberror($sql);

			$sql = 'delete from user_company_responsibles where user_company_responsible_user_id = ' . dbquote(id());
			mysql_query($sql) or dberror($sql);

			$sql = 'delete from mail_template_recipients where mail_template_recipient_user_id = ' . dbquote(id());
			mysql_query($sql) or dberror($sql);

			$sql = 'delete from user_roles where user_role_user = ' . dbquote(id());
			mysql_query($sql) or dberror($sql);
		}
		elseif(!in_array(33, $roles) and !in_array(56, $roles)) {
				$sql = 'delete from user_company_responsibles where user_company_responsible_user_id = ' . dbquote(id());
				mysql_query($sql) or dberror($sql);
		}


		if($form->value('user_active') != 1 and $user_active == 1)
		{
			$sql = 'update users set user_set_to_inactive_by  = ' . user_id() . ', user_set_to_inactive_date = ' . dbquote(date("Y-m-d H:i:s")) . ' where user_id = ' . id();
			mysql_query($sql) or dberror($sql);
		}

		if($form->value("new_user") == 1)
		{
			redirect("user.php?id=" . id());
		}
		
		/*
		if($form->value("new_user") == 1)
		{
			redirect("user.php?id=" . mysql_insert_id() . "&address=" . $form->value("user_address"));
		}
		*/

		//trim all user names and emails
		$sql = "update users set user_firstname = trim(user_firstname), user_name = trim(user_name), user_email = trim(user_email)";
		mysql_query($sql) or dberror($sql);

	}
}

// Render page

$page = new Page("users");

$page->header();
$page->title(id() ? "Edit User" : "Add User");
$form->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#countries_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/admin/country_selector.php?context=user&user_id=<?php echo id();?>&address=<?php echo param("address");?>'
    });
    return false;
  });

   $('#granted_users_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/admin/user_selector.php?context=user&user_id=<?php echo id();?>&address_id=<?php echo $address_id;?>'
    });
    return false;
  });

  $('#retailnet_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/admin/role_selector.php?id=<?php echo id();?>&application=retaildevelopment'
    });
    return false;
  });

  
  $('#mps_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/admin/role_selector.php?id=<?php echo id();?>&application=mps'
    });
    return false;
  });

  

  $('#lps_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/admin/role_selector.php?id=<?php echo id();?>&application=lps'
    });
    return false;
  });

  

  $('#red_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/admin/role_selector.php?id=<?php echo id();?>&application=red'
    });
    return false;
  });

  $('#news_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/admin/role_selector.php?id=<?php echo id();?>&application=news'
    });
    return false;
  });


  $('#uberalldata_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/admin/role_selector.php?id=<?php echo id();?>&application=uberalldata'
    });
    return false;
  });

});
</script>


<?php
$page->footer();

	
	// send user login data, modal email form
	if ($user->login && $user->password) {

		CssCompiler::attach(array(
			DIR_SCRIPTS."fancybox/fancybox.css",
			DIR_SCRIPTS."jgrowl/jgrowl.css",
			DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css",
			DIR_SCRIPTS."qtip/qtip.css",
			DIR_THEMES."default/css/modal.css"
		));
			
		JsCompiler::attach(array(
			DIR_SCRIPTS."fancybox/fancybox.js",
			DIR_SCRIPTS."spin/spin.min.js",
			DIR_SCRIPTS."jgrowl/jgrowl.js",
			DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js",
			DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js",
			DIR_SCRIPTS."qtip/qtip.js",
			DIR_SCRIPTS."retailnet.js",
			DIR_JS."user.js"
		));
		
		echo CssCompiler::compile();
		echo JsCompiler::compile();
			
		$template = new Mail_Template();
		$loginData = $template->read(13);
			
		if ($template->id) { 

			$link = Settings::init()->http_protocol.'://'.$_SERVER['SERVER_NAME'];
			
			if ($apps) {
				foreach ($apps as $row) {
					$app = $row['navigation_url'];
					
					$permitted_applications .= "\n\n" . "<a href=\"" . $link . "/" . $app ."\">" . $link . "/" . $app ."</a>";
				}
			}

			$render = array(
				'recipient_firstname' => $user->firstname,
				'recipient_name' => $user->name,
				'user_name' => $user->login,
				'password' => $user->password,
				'sender_firstname' => $auth->firstname,
				'sender_name' => $auth->name,
				'uri_list_of_modules' => $permitted_applications
			);
				
			// content render
			$template->data['mail_template_subject'] = Content::render($template->subject, $render, true);
			$template->data['mail_template_text'] = nl2br(Content::render($template->text, $render, true));
			$template->data['mail_template_footer'] = nl2br(Content::render($template->footer, $render, true));

			$loginData = $template->data;
			$loginData['user'] = $user->id;

			// set user creator as cc recipient if recipient get login data for the first time
			$res = $model->query("
				SELECT COUNT(mail_tracking_id) AS total
				FROM db_retailnet.mail_trackings
				WHERE mail_tracking_recipient_email = '$user->email' AND mail_tracking_mail_template_id = 13
			")->fetch();
			
			if (!$res['total']) {
				$createdFrom = $user->data['user_created'];
				$res = $model->query("SELECT * FROM db_retailnet.users WHERE user_login = '$createdFrom' ")->fetch(); 
				$loginData['login_data_cc'] = $user->email <> $res['user_email'] ? $res['user_email'] : null;
			}
		}
	}

?>

<script type="text/javascript" src="/public/scripts/tinymce4/js/tinymce/tinymce.min.js"></script>

<!-- mail template modal -->
<div id="loginMailContainer" class="ado-modal ado-box adomat">
	<div class="ado-modal-header">
		<div class="ado-title"><?php echo $translate->send_login_data; ?></div>
		<div class="ado-subtitle"><?php echo $data['user_firstname'].' '.$data['user_name']; ?></div>
	</div>
	<div class="ado-modal-body">
		<form method="post" id="loginMailForm" action="/applications/modules/user/sendmail.php" >
			<input type="hidden" name="application" value="<?php echo $request->application ?>" />
			<input type="hidden" name="controller" value="<?php echo $request->controller ?>" />
			<input type="hidden" name="redirect" value="<?php echo $loginData['redirect'] ?>" />
			<input type="hidden" name="user" class="user-id" value="<?php echo id(); ?>" />
			<input type="hidden" name="mail_template_id" class="mail-id" value="<?php echo $loginData['mail_template_id'] ?>" />
			<input type="hidden" name="mail_template_view_modal" class="modal-trigger" value="<?php echo $loginData['mail_template_view_modal'] ?>" />
			<div class="ado-row">
				<input type=text name="mail_template_subject" class="ado-modal-input required mail-subject" value="<?php echo $loginData['mail_template_subject'] ?>" />
			</div>
			<div class="ado-row">
				<textarea name="login_data_cc" id="login_data_cc" class="cc ado-modal-input mail-cc" placeholder="cc recipients (comma separated):"><?php echo $loginData['login_data_cc'] ?></textarea>
			</div>
			<div class="ado-row">
				<textarea name="mail_template_text" id="mailLoginContent" class="ado-modal-input required mail-content" data-height="460" ><?php echo $loginData['mail_template_text'] ?></textarea>
			</div>
		</form>
	</div>
	<div class="ado-modal-footer" >
		<div class="ado-row ado-actions">
			<a class="button cancel ado-modal-close">
				<span class="icon cancel"></span>
				<span class="label"><?php echo $translate->cancel ?></span>
			</a>
			<a class="button test-mail" href="#">
				<span class="icon mail"></span>
				<span class="label">Test Mail</span>
			</a>
			<a class="button preview-mail" href="/applications/modules/user/sendmail.preview.php">
				<span class="icon icon84"></span>
				<span class="label">Preview</span>
			</a>
			<a class="button sendmail" href="#">
				<span class="icon mail"></span>
				<span class="label"><?php echo $translate->sendmail ?></span>
			</a>
		</div>
	</div>
</div>

<!-- preview mail modal -->
<div id="preview-mail-template" class="ado-modal ado-box adomat">
	<div class="ado-modal-header">
		<span class="fa-stack ado-modal-close">
			<i class="fa fa-circle fa-stack-2x"></i>
			<i class="fa fa-times fa-stack-1x fa-inverse"></i>
		</span>
		<div class="ado-row ado-title mail-subject"></div>
		<div class="ado-row" >
			<span class="ado-label ado-width-30">To:</span>
			<span class="ado-label mail-address"></span>
		</div>			
		<div class="ado-row">
			<span class="ado-label ado-width-30">CC:</span>
			<span class="ado-label mail-cc-address"></span>
		</div>
		<div class="ado-notification-right-bottom">
			<span class="ado-label mail-date"></span>
		</div>
	</div>
	<div class="ado-modal-body ado-max-height-500 mail-content"></div>
	<div class="ado-modal-footer">
		<div class="mail-footer"></div>
	</div>
</div>

<!-- test mail modal dialog -->
<div id="test-mail-template"  class="ado-modal ado-dialog adomat">
	<div class="ado-modal-header">
		<div class="ado-title" >Send Test E-mail an:</div>
	</div>
	<div class="ado-modal-body">
		<input type="text" name="test-mail-recipient" id="test-mail-recipient" class="ado-modal-input required" value="<?php echo User::instance()->email ?>" placeholder="Recipient E-mail" />
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="/applications/modules/user/sendmail.php" >
				<span class="icon mail"></span>
				<span class="label" ><?php echo $translate->send ?></span>
			</a>
		</div>
	</div>
</div>