<?php
/********************************************************************

    account_numbers.php

    Lists account numbers for editing .

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2004-09-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-09-27
    Version:        1.0.0

    Copyright (c) 2004, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("account_number.php");

$sql = "select account_number_id, account_number_name from account_numbers ";

$list = new ListView($sql);

$list->set_entity("account_numbers");
$list->set_order("account_number_name");

$list->add_column("account_number_name", "Account Number", "account_number.php");

$list->add_button(LIST_BUTTON_NEW, "New", "account_number.php");

$list->process();

$page = new Page("account_numbers");

$page->header();
$page->title("Account Numbers");
$list->render();
$page->footer();

?>
