<?php
/********************************************************************

    lwgroups.php

    Lists Local Construction Cost Groups

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-06
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");
set_referer("lwgroup.php");

$list = new ListView("select lwgroup_id, lwgroup_group, lwgroup_code, lwgroup_text " .
                     "from lwgroups");

$list->set_entity("lwgroups");
$list->set_order("lwgroup_code");

$list->add_column("lwgroup_group", "Group", "");
$list->add_column("lwgroup_code", "Code", "lwgroup.php");
$list->add_column("lwgroup_text", "Description");

$list->add_button(LIST_BUTTON_NEW, "New", "lwgroup.php");

$list->process();

$page = new Page("lwgroups");

$page->header();
$page->title("Local Construction Work Groups");
$list->render();
$page->footer();

?>
