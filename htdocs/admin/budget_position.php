<?php
/********************************************************************

    budget_position.php

    Add and Edit standard budget positions for editing.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-09-26
    Version:        1.0.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_edit_catalog");

$form = new Form("exclusion_notifications", "budget position");

$form->add_section("Description");

$form->add_edit("exclusion_notification_code", "Code", NOTNULL | UNIQUE);
$form->add_list("exclusion_notification_type_id", "Type",
    "select item_type_id, item_type_name " .
    "from item_types " .
    "where item_type_id >= " . ITEM_TYPE_EXCLUSION, NOTNULL);

$form->add_multiline("exclusion_notification_description", "Description", 10, NOTNULL);
$form->add_checkbox("exclusion_notification_corporate", "Applicable for corporate projects");
$form->add_checkbox("exclusion_notification_non_corporate", "Applicable for non corporate projects");

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);

$form->populate();


$form->process();

$page = new Page("budget_positions");
$page->header();
$page->title(id() ? "Edit Exclusion/Notification" : "Add Exclusion/Notification");
$form->render();
$page->footer();

?>