<?php
/********************************************************************

    possubclasses.php

    Lists store POS Type Subclasses for editing.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-24
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_browse_catalog_in_admin");
set_referer("possubclass.php");

$list = new ListView("select possubclass_id, possubclass_name, " .
                     "possubclass_email1, possubclass_email2, possubclass_email3, possubclass_email4, possubclass_email5, possubclass_email6 " .
                     "from possubclasses");

$list->set_entity("possubclasses");
$list->set_order("possubclass_name");

if(has_access("can_edit_catalog"))
{
	$list->add_column("possubclass_name", "Name", "possubclass.php");
}
else
{
	$list->add_column("possubclass_name", "Name");
}

$list->add_column("possubclass_email1", "Recipient 1");
$list->add_column("possubclass_email2", "Recipient 2");
$list->add_column("possubclass_email3", "Recipient 3");
$list->add_column("possubclass_email4", "Recipient 4");
$list->add_column("possubclass_email5", "Recipient 5");
$list->add_column("possubclass_email6", "Recipient 6");
if(has_access("can_edit_catalog"))
{
	$list->add_button(LIST_BUTTON_NEW, "New", "possubclass.php");
}

$list->process();

$page = new Page("possubclasses");

$page->header();
$page->title("POS Subclasses");
$list->render();
$page->footer();

?>
