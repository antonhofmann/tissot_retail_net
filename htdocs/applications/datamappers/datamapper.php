<?php

/**
 * Datamapper abstract class
 * Handles generic datamapper related stuff
 * Extend with a specific implementation for a specific data topic, e.g. a news datamapper
 *
 * Contains wrapper methods for query and prepare as they are on different objects and
 * it's not intuitive to do $this->Model->db->prepare in a subclass.
 */
abstract class Datamapper {
  /**
   * Database accessor
   * This var is private to discourage subclasses of accessing it directly,
   * use the wrapper methods instead.
   * @var Model
   */
  private $Model;

  /**
   * The connector configured in constructor
   * @var string
   */
  protected $connector;

  /**
   * Setup db accessor
   * @param string Db to connect to
   */
  public function __construct($connector = null) {
    if (is_null($connector)) {
      $connector = Connector::DB_CORE;
    }
    $this->connector = $connector;
    $this->Model = new Model($connector);
  }

  /**
   * Get field bind definitions for SQL statement
   * @param array $data
   * @return string
   */
  protected function getFieldBindings(array $data) {
    $bindFields = array();

    foreach ($data as $key => $value) {
      $bindFields[] = "$key = :$key";
    }

    return join(',', $bindFields);
  }

  /**
   * Bind values to PDO statement
   * @param PDOStatement &$stmt
   * @param array $data
   * @return void
   */
  protected function bindValues(PDOStatement &$stmt, array $data) {
    foreach ($data as $key => $value) {
      $stmt->bindValue(":$key", $value);
    }
  }

  /**
   * Wrapper for query method on this::$Model
   * @param  string $query
   * @return mixed  Whatever original method returns
   */
  protected function query($query) {
    return $this->Model->query($query);
  }

  /**
   * Wrapper for prepare method on this::$Model::$db
   * @param  string $query
   * @return mixed  Whatever original method returns
   */
  protected function prepare($query) {
    return $this->Model->db->prepare($query);
  }
}
