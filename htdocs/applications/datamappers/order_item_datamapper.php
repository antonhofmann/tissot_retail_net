<?php

/**
 * Datamapper for order items
 */
class Order_Item_Datamapper extends Datamapper {
	/**
	 * Update an order item by an address and order ID
	 * 
	 * @throws Exception when supplying an invalid $addressType
	 * 
	 * @param integer $addressID
	 * @param string  $addressType forwarder|supplier
	 * @param integer $orderID
	 * @param array $data
	 * @return bool
	 */
	public function updateOrderItemByAddressAndOrderID($addressID, $addressType, $orderID, array $data) {
		if ($addressType == 'forwarder') {
			$addressField = 'order_item_forwarder_address';	
		}
		else if ($addressType == 'supplier') {
			$addressField = 'order_item_supplier_address';		
		}
		else {
			throw new Exception("unknown address type $addressType!");
		}

		$stmt = $this->prepare(sprintf("
			UPDATE 
				order_items 
			SET 
				%s
			WHERE
					order_item_order = :order_id
			  AND
			  	%s = :address_id
			",
			$this->getFieldBindings($data),
			$addressField
		));

		$stmt->bindValue(':order_id', $orderID);
		$stmt->bindValue(':address_id', $addressID);
		$this->bindValues($stmt, $data);
		$stmt->execute();
	}
}