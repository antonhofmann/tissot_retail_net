<?php

/**
 * Role datamapper
 */
class Role_Datamapper extends Datamapper {
  /**
   * Get roles for masterplan templates
   * @return array
   */
  public function getMasterplanRoles() {
    return $this->query("
      SELECT 
        role_id AS id,
        role_name AS name
      FROM
        roles
      WHERE
        role_include_in_masterplan_tpl = 1
      ORDER BY
        role_name
      ")->fetchAll();
  }
}
