<?php
/**
 * Point Of Sale datamapper
 * currently handles all queries regarding several pos-related tables.
 * Maybe we'll have to get more specific as this class grows.
 */
class POS_Datamapper extends Datamapper {
  /**
   * Get all types
   * @return array
   */
  public function getTypes() {
    return $this->query("
      SELECT 
        postype_id AS id,
        postype_alias AS alias,
        postype_name AS name,
        IF (LENGTH(postype_name) > 3, SUBSTR(postype_name, 1, 1), postype_name) AS abbreviation
      FROM
        postypes
      ")->fetchAll();
  }

  /**
   * Get postype by postype_id
   * @param  integer $posTypeID
   * @return array
   */
  public function getTypeByID($posTypeID) {
    return $this->query(sprintf("
      SELECT
        postype_alias AS alias,
        postype_name AS name
      FROM
        postypes
      WHERE
        postype_id = %d
      ", $posTypeID))->fetch();
  }

  /**
   * Update poslease table
   * @param integer $id
   * @param array $data
   * @return bool
   */
  public function updatePOSLease($id, array $data) {
    $stmt = $this->prepare(
      Database::updateStatement('posleases', 'poslease_id', $data)
      );
    $stmt->bindValue(':poslease_id', $id);
    foreach ($data as $param => $value) {
      $stmt->bindValue(":$param", $value);
    }
    return $stmt->execute();
  }



  /**
   * Update poslease table
   * @param integer $id
   * @param array $data
   * @return bool
   */
  public function getUntranslatedPOSLocations() {
	$untranslated_poslocations = array();

	$query = "select * from translationresponsibles";
	$result = $this->query($query);
	$rows = $result->fetchAll();
	

	foreach($rows as $key=>$translation_data) {
		//get all pos locations published in the store locator
		$query = "select posaddress_id from posaddresses 
	          where posaddress_client_id = '" . $translation_data["translationresponsible_address_id"] . "' " . 
			  " and posaddress_country = '" . $translation_data["translationresponsible_country_id"] . "' " . 
			  " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') 
			    and posaddress_export_to_web = 1";

		$result2 = $this->query($query);
		$rows2 = $result2->fetchAll();
		foreach($rows2 as $key=>$pos_data) {
			$poslocations[$user_data['user_address']][] = $pos_data["posaddress_id"];
		}
		
	}


	//check for missing translations
	$storeLocator = new Store_Locator();
	foreach($poslocations as $key=>$pos_ids) {
		$data = $storeLocator->getUntranslatedPOSlocations($pos_ids);
	
	}
	
	$tmp = array();
	$filter = '';
	foreach($data as $key=>$pos) {
		
		$tmp[] = $pos['posaddress_id'];
		
	}

	if(count($tmp) > 0) {
		$filter = implode(',', $tmp);

		$query = "select posaddress_client_id, posaddress_id, posaddress_country, country_name, posaddress_name, place_name 
					from posaddresses
					left join countries on country_id = posaddress_country
					left join places on place_id = posaddress_place_id
					where posaddress_id in ( " . $filter . ") 
					order by posaddress_client_id, country_name";


		$result = $this->query($query);

		
		$rows = $result->fetchAll();



		foreach($rows as $key=>$pos_data)
		{
			$untranslated_poslocations[$pos_data['posaddress_client_id']][$pos_data['posaddress_id']] = $pos_data;
		}
	}


	return $untranslated_poslocations;

  }
}
