<?php

/**
 * Country datamapper
 * Currently also handles sales regions. Maybe split into own datamapper if it gets to specific.
 */
class Country_Datamapper extends Datamapper {
  /**
   * Get all sales regions
   * @return array
   */
  public function getSalesRegions() {
    return $this->query("
      SELECT 
        salesregion_id AS id,
        salesregion_name AS name
      FROM
        salesregions
      ")->fetchAll();
  }

  /**
   * Get sales region for a country
   * @param  integer $countryID
   * @return array
   */
  public function getSalesRegionForCountry($countryID) {
    $query = sprintf("
      SELECT
        salesregion_id AS id,
        salesregion_name AS name
      FROM
        countries
      INNER JOIN 
        salesregions ON (salesregion_id = country_salesregion)
      WHERE
        country_id = %d
    ", $countryID);
    return $this->query($query)->fetch();
  }

  /**
   * Get countries by salesregion
   * If no region is supplied, all countries are fetched
   * @return array
   */
  public function getCountriesBySalesRegion($salesRegionID = null) {
    $constraint = is_null($salesRegionID)
      ? ''
      : sprintf('WHERE country_salesregion = %d', $salesRegionID);

    $countries = $this->query(sprintf("
      SELECT 
        country_id AS id,
        country_name AS name,
        country_salesregion AS salesregion_id
      FROM
        countries
      %s
      ORDER BY
        name",
      $constraint
    ))->fetchAll();

    return __::groupBy($countries, 'salesregion_id');
  }
}
