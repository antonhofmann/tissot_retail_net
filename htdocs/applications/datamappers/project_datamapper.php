<?php

/**
 * Handles all data operations on project tables
 */
class Project_Datamapper extends Datamapper {
  /**
   * Get all active project cost types
   * @return array
   */
  public function getProjectCostTypes() {
    return $this->query("
      SELECT 
        project_costtype_id AS id,
        project_costtype_alias AS alias,
        project_costtype_text AS name,
        IF (LENGTH(project_costtype_text) > 3, SUBSTR(project_costtype_text, 1, 1), project_costtype_text) AS abbreviation
      FROM
        project_costtypes
      WHERE
        is_active = '1'
      ")->fetchAll();
  }
}