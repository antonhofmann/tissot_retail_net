<?php

/**
 * Handles all data operations on item supplying groups
 * An item supplying group is a connection between an item and a supplying_group
 */
class Item_Supplying_Groups_Datamapper extends Datamapper {
  /**
   * Get all valid supplying groups choices for a given item
   * @param  integer $itemID
   * @return array   Array with 2 keys: selected and groups
   */
  public function getSupplyingGroupsForItem($itemID) {
    // get all active supplying groups and all supplyling groups that are selected 
    // for this item even though they may be inactive
    $stmt = $this->prepare("
      SELECT
        supplying_group_id,
        supplying_group_name,
        CASE WHEN item_supplying_group_item_id IS NOT NULL
          THEN 1
          ELSE 0
        END AS is_selected
      FROM
        supplying_groups
      LEFT JOIN
        item_supplying_groups ON (
          item_supplying_group_supplying_group_id = supplying_group_id
          AND item_supplying_group_item_id = :item_id
          )
      WHERE
        supplying_group_active = 1
        OR (item_supplying_group_item_id = :item_id)
      ORDER BY
        supplying_group_name
      ");
    $stmt->bindValue(':item_id', $itemID);
    $stmt->execute();
    $result = $stmt->fetchAll();

    // split data into group choices and selected groups
    $selected = array();
    $groups   = array();
    foreach ($result as $group) {
      $groups[$group['supplying_group_id']] = $group['supplying_group_name'];
      if ($group['is_selected']) {
        $selected[] = $group['supplying_group_id'];
      }
    }

    return array(
      'groups'   => $groups,
      'selected' => $selected,
      );
  }

  /**
   * Replace supplying groups for an item
   * Wrapper for this::clearConnections() and this::addConnections()
   * @param integer $itemID
   * @param array $supplyingGroupIDs
   * @return boolean
   */
  public function replaceConnections($itemID, array $supplyingGroupIDs) {
    $this->clearConnections($itemID);
    return $this->addConnections($itemID, $supplyingGroupIDs);
  }

  /**
   * Clear supplying groups from an item
   * @param  integer $itemID
   * @return void
   */
  public function clearConnections($itemID) {
    $stmt = $this->prepare("
      DELETE FROM item_supplying_groups
      WHERE item_supplying_group_item_id = ?
    ");
    $stmt->execute(array($itemID));
  }

  /**
   * Add supplying groups for an item
   * @param integer $itemID
   * @param array $supplyingGroupIDs
   * @return boolean
   */
  public function addConnections($itemID, array $supplyingGroupIDs) {
    $user = User::instance();
    $groups = array();

    foreach ($supplyingGroupIDs as $groupID) {
      $groups[] = sprintf("(%d, %d, '%s', NOW())",
        $itemID,
        $groupID,
        $user->login
        );
    }

    // build multi-insert statement
    $insert = sprintf("
      INSERT INTO 
        item_supplying_groups
        (
          item_supplying_group_item_id,
          item_supplying_group_supplying_group_id,
          user_created,
          date_created
        )
      VALUES
        %s
      ",
      join(",\n", $groups)
    );

    $stmt = $this->prepare($insert);
    return $stmt->execute();
  }
}