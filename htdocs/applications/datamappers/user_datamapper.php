<?php

/**
 * User datamapper
 */
class User_Datamapper extends Datamapper {
  /**
   * Get all users with role ADMN
   * @return array
   */
  public function getAdmins() {
    return $this->query("
      SELECT 
        u.user_id AS id,
        u.user_firstname AS firstname,
        u.user_name AS lastname
      FROM
        users u
      INNER JOIN
        user_roles ur ON (ur.user_role_user = u.user_id)
      INNER JOIN
        roles r ON (r.role_id = ur.user_role_role)
      WHERE
        r.role_code = 'ADMN'
      ORDER BY
        u.user_id
      ")->fetchAll();
  }

  /**
   * Get brand manager from a user address ID
   * @param  integer $addressID
   * @return User|false
   */
  public function getBrandManager($addressID) {
    $stmt = $this->prepare("
      SELECT
        user_id
      FROM
        users
      JOIN
        user_roles ON (user_role_user = user_id)
      WHERE
          user_address = :address_id
        AND
          user_active = 1
        AND
          user_role_role = :role_id
      ");
    $stmt->bindValue(':address_id', $addressID);
    $stmt->bindValue(':role_id', Role::BRAND_MANAGER);
    $stmt->execute();
    $userID = $stmt->fetchColumn();
    return $userID === false
      ? false
      : new User($userID);
  }

  /**
   * Not sure what an affiliate brand manager is, but this gets him
   * @param  integer $addressID
   * @return User|false
   */
  public function getAffiliateBrandManager($addressID) {
    $stmt = $this->prepare("
      SELECT
        user_id
      FROM
        users
      INNER JOIN
        addresses ON (address_parent = user_address)
      INNER JOIN
        user_roles ON (user_role_user = user_id)
      WHERE
          address_id = :address_id
        AND
          address_client_type = 3
        AND
          user_active = 1
        AND
          user_role_role = :role_id
      ");
    $stmt->bindValue(':address_id', $addressID);
    $stmt->bindValue(':role_id', Role::BRAND_MANAGER);
    $stmt->execute();
    $userID = $stmt->fetchColumn();
    return $userID === false
      ? false
      : new User($userID);
  }

  /**
   * Get regional sales manager HQ
   * @param  integer $orderID
   * @return User|false
   */
  public function getRegionalSalesManager($orderID) {
    $stmt = $this->prepare("
      SELECT
        user_id
      FROM
        orders
      JOIN
        user_company_responsibles ON (order_client_address = user_company_responsible_address_id)
      JOIN
        users ON (user_id = user_company_responsible_user_id)
      JOIN
        user_roles ON (user_role_user = user_id)
      JOIN
        project_costs ON (project_cost_order = order_id)
      WHERE
          order_id = :order_id
        AND
          user_role_role = :role_id
        AND
          user_active = 1
        -- select the proper responsible user according to project cost type
        AND
          IF (project_cost_type = 1, user_company_responsible_retail, user_company_responsible_wholsale) = 1
      ");
    $stmt->bindValue(':order_id', $orderID);
    $stmt->bindValue(':role_id', Role::REGIONAL_SALES_MANAGER);
    $stmt->execute();
    $userID = $stmt->fetchColumn();
    return $userID === false
      ? false
      : new User($userID);
  }

  /**
   * Get retail manager
   * @param  integer $addressID
   * @return array|false
   */
  public function getRetailManager($addressID) {
    $stmt = $this->prepare("
      SELECT
        user_id
      FROM
        users
      JOIN
        user_roles ON (user_role_user = user_id)
      WHERE
          user_address = :address_id
        AND
          user_role_role = :role_id
        AND
          user_active = 1
      ");
    $stmt->bindValue(':address_id', $addressID);
    $stmt->bindValue(':role_id', Role::RETAIL_MANAGER);
    $stmt->execute();

    $results = $stmt->fetchAll();
    $users = array();
    foreach ($results as $result) {
      $users[] = new User($result['user_id']);
    }
    return $users;
  }

  /**
   * Get local retail coordinator
   * @param  integer $addressID
   * @return User|false
   */
  public function getLocalRetailCoordinator($addressID) {
    $stmt = $this->prepare("
      SELECT
        user_id
      FROM
        users
      JOIN
        user_roles ON (user_role_user = user_id)
      WHERE
          user_address = :address_id
        AND
          user_role_role = :role_id
        AND
          user_active = 1
      ");
    $stmt->bindValue(':address_id', $addressID);
    $stmt->bindValue(':role_id', Role::LOCAL_RETAIL_COORDINATOR);
    $stmt->execute();
    $userID = $stmt->fetchColumn();
    return $userID === false
      ? false
      : new User($userID);
  }

  /**
   * Get project creator
   * @return User|false
   */
  public function getProjectCreator($posAddressID) {
    $stmt = $this->prepare("
      SELECT
        user_id
      FROM
        users
      INNER JOIN
        posaddresses ON (posaddresses.user_created = user_login)
      WHERE 
          posaddress_id = :pos_address_id
        AND
          user_active = 1
    ");
    $stmt->bindValue(':pos_address_id', $posAddressID);
    $stmt->execute();
    $userID = $stmt->fetchColumn();
    return $userID === false
      ? false
      : new User($userID);
  }

  /**
   * Get retail controllers
   * (there is actually only 1 but I'm doing it like this for future-proofness)
   * @return array
   */
  public function getRetailControllers() {
    $stmt = $this->prepare("
      SELECT 
        user_id
      FROM 
        users
      INNER JOIN 
        user_roles ON (user_role_user = user_id)
      WHERE
          user_active = 1
        AND
          user_role_role = :role_id
    ");
    $stmt->bindValue(':role_id', Role::RETAIL_CONTROLLER);
    $stmt->execute();
    $result = $stmt->fetchAll();
    
    $users = array();
    foreach ($result as $row) {
      $users[] = new User($row['user_id']);
    }
    return $users;
  }
}
