<?php
/**
 * Project masterplan value datamapper
 * This datamapper is not a regular datamapper. It provides values for project masterplans.
 *
 * @todo  add a little guideline for adding new values
 */
class Project_Masterplan_Values_Datamapper extends Datamapper {
  /**
   * @const string
   */
  const DATE_DISPLAY = '%d.%m.%Y';

  /**
   * @const string
   */
  const DATE_MYSQL = '%Y-%m-%d'; // H%:%i:%s

  /**
   * @const string
   */
  const DATE_EXCEL = '%d/%m/%Y';

  /**
   * @const string
   */
  const DATE_JS = '%Y/%m/%d';

  /**
   * Date format for all dates returned
   * @see self::setDateFormat()
   * @var string
   */
  protected $dateFormat = self::DATE_DISPLAY;

  /**
   * Maps milestone codes to masterplan step values
   * @var array
   */
  protected $milestoneMap = array(
	
	'0000' => 'project_submitted',
	'0010' => 'lnr_posted',
	'0012' => 'preapproval_merchansing',
  	'0050' => 'ln_approved_by_hq',
	'0060' => 'ln_sent_to_sg',
	'0070' => 'ln_rejected_by_sg',
	'0080' => 'ln_approved_by_sg',

	'0100' => 'cer_posted',
	'0800' => 'cer_approved_by_hq',
	'1250' => 'cer_approved_below_50k',
	'1500' => 'cer_deffered',

	'1300' => 'cer_to_sg',
    '1400' => 'cer_approved'
   
    
    );

  /**
   * Maps order states to masterplan step values
   * @var array
   */
  protected $orderStateMap = array(
	

	97 => 'design_briefing_submitted',       // 200
	98 => 'design_briefing_rejected',        // 210
	
	
	99 => 'design_briefing_accepted',        // 220
	5  => 'layout_briefing_submitted',       // 230
    9  => 'layout_preview_submitted',        // 270
	11 => 'layout_preview_accepted',         // 290
	15 => 'layout_preview_client',           // 330
    17 => 'client_accept_layout',            // 350
	
    72 => 'mini_booklet_submitted',             // 400
    77 => 'mini_booklet_accepted',              // 413
    78 => 'mini_booklet_hq_approval',           // 414
    79 => 'mini_booklet_client_approval',       // 415
    81 => 'mini_booklet_client_accepted',       // 417
   
    18 => 'booklet_request_submitted',          // 420
    24 => 'booklet_request_approved',           // 426
    82 => 'booklet_client_approval',            // 430
    84 => 'booklet_client_approved',            // 441   
  
	34 => 'budget_accepted_by_client',          // 620
	105 => 'budget_accepted_by_hq',             // 670

	87 => 'full_budget_accepted',               // 840

	6 => 'cer_approved_by_hq',                  // 800
	21 => 'cer_approved_below_50k',             // 1250
    23 => 'cer_deferred',                       // 1500 
	
	11 => 'cer_to_sg',                          // 1300
    12 => 'cer_approved_by_sq',                 // 1400 
    
	
	// are the following 2 IDs correct?
    //31 => 'offer_accepted',                     // 560
    //32 => 'budget_of_hq_items_to_client',       // 600
    
    //35 => 'order_submitted_to_supplier',      // 700
    //37 => 'order_confirmed',                  // 720
	
    );  

  /**
   * Maps construction date values to item category ids
   * @var array
   */
  protected $itemCategoryMap = array(
    'floor_arrival_date'        => array(3),
    'frames_arrival_date'       => array(24),
    'illumination_arrival_date' => array(16),
    'logos_arrival_date'        => array(6),
    'visuals_arrival_date'      => array(7),
    'furniture_arrival_date'    => array(1, 4, 5, 12),
    );

  /**
   * Use a fallback value for these delivery date types
   * @var array
   */
  protected $deliveryDatesWithFallbackValues = array(
    'floor_arrival_date'
    );

  /**
   * Return fallback values when no delivery date is available
   * This only affects categories in $deliveryDatesWithFallbackValues
   * Use this::setDeliveryDateDoFallback() to set this value
   * @var boolean
   */
  protected $doDeliveryDateFallback = true;

  /**
   * Get available values this datamapper supports
   * @return array
   */
  public function getAvailableValues() {
    // @todo can we get these names from db somehow like we do for order states?
    $valueMap = array(
      'project_submitted'           => 'Project submitted',
      'handover_date'               => 'Handover date',
      'rent_start_date'             => 'Rent start date',
      'lease_signature_date'        => 'Lease signature date',
      'preferred_opening_date'      => "Client's preferred opening date",
      'agreed_opening_date'         => 'HQ feedback on agreed opening date',
      'actual_opening_date'         => 'Actual opening date',
      'construction_startdate'      => 'Construction start date',
      //'floor_arrival_date'          => 'Floor arrival date',
      //'frames_arrival_date'         => 'Frames arrival date',
      //'visuals_arrival_date'        => 'Visuals arrival date',
      //'illumination_arrival_date'   => 'Illumination arrival date',
      //'furniture_arrival_date'      => 'Furniture arrival date',
      //'logos_arrival_date'          => 'Logos arrival date',
      //'preferred_transportation'    => 'Preferred transportation',
      // pseudo value using the latest item arrival date
      //'latest_item_arrival_date'    => 'Latest item arrival date',
      // these are "smart" substiutions of the corresponding step nr
      //'order_submitted_to_supplier' => '700 Order submitted to supplier',
      //'order_confirmed'             => '720 Order confirmed',
      );

    
    $orderStateName = $this->getOrderStateNames() ?: array();
    $milestoneTexts = $this->getMilestoneTexts() ?: array();

    // get order state and milestone texts from db
    $values = array_merge(
      $valueMap, 
      $orderStateName,
      $milestoneTexts
      );
    asort($values);
    return $values;
  }

  /**
   * Load project and all related data
   * @param  integer $projectID
   * @param  boolean $getAllValues  Get all values if there are multiple dates per step
   * @return array   Project data
   */
  public function loadProjectData($projectID, $getAllValues = false) {
    // load project
    $project = $this->loadProject($projectID);

    // merge related info into project values
    $project = array_merge(
      $project,
      $this->loadPOSLeaseData($project['project_order']),
      $this->loadMilestoneData($projectID),
      $this->loadOrderStateData($project['project_order'], $getAllValues),
      $this->loadDeliveryDates($project['project_order']),
      $this->loadOrderData($project['project_order']),
      $this->loadOrderSubmittedDate($project['project_order']),
      $this->loadOrderConfirmedDate($project['project_order'])
    );
    $project['latest_item_arrival_date'] = $this->getLatestDeliveryDate($project);
    unset($project['project_order']);

    // map values recursively and get rid of 0000 dates
    $project = __::mapRecursive($project, function ($value) {
      return (strpos($value, '0000') !== false || $value == '') ? null : $value;
    });

    if ($getAllValues) {
      return $this->splitIntoMultipleDatasets($project);
    }
    else {
      return $project;
    }
  }

  /**
   * Splits an array of project values into an array of multiple project values (when applicable)
   * @todo  describe the process here..
   * @param  array Project values
   * @return array Array of arrays of project values
   */
  protected function splitIntoMultipleDatasets($project) {
    $datasets = array();
    $datasets[0] = array();
    foreach ($project as $key => $value) {
      if (!is_array($value)) {
        $datasets[0][$key] = $value;
      }
      else if (is_array($value) && sizeof($value) == 1) {
        $datasets[0][$key] = $value[0];  
      }
      else {
        $datasets[0][$key] = array_shift($value);
        $i = 1;
        foreach ($value as $repeatedValue) {
          $datasets[$i][$key] = $repeatedValue;
          $i++;
        }
      }
    }
    return $datasets;
  }

  /**
   * Toggle if we do fallback values for delivery dates
   * @param boolean $doFallback
   */
  public function setDeliveryDateDoFallback($doFallback) {
    $this->doDeliveryDateFallback = (bool) $doFallback;
  }

  /**
   * Load a project's own data
   * @param  integer $projectID
   * @return array   Project values
   */
  public function loadProject($projectID) {
    $stmt = $this->prepare("
      SELECT
        project_order,
        DATE_FORMAT(p.date_created, :date_format) AS project_submitted,
        DATE_FORMAT(p.project_planned_opening_date, :date_format) AS preferred_opening_date,
        DATE_FORMAT(p.project_real_opening_date, :date_format) AS agreed_opening_date,
        DATE_FORMAT(p.project_actual_opening_date, :date_format) AS actual_opening_date,
        DATE_FORMAT(p.project_construction_startdate, :date_format) AS construction_startdate
      FROM
        projects p
      WHERE
        project_id = :id
      ");
    $stmt->bindValue(':id', $projectID);
    $stmt->bindValue(':date_format', $this->dateFormat);
    $stmt->execute();
    return $stmt->fetch();
  }

  /**
   * Set the date format for all dates returned
   * Call before using the "load" functions.
   * @param string $format One of the DATE constants of this class
   */
  public function setDateFormat($format) {
    $this->dateFormat = $format;
  }

  /**
   * Load project milestone data
   * @param  integer $projectID
   * @return array
   */
  protected function loadMilestoneData($projectID) {
    $stmt = $this->prepare("
      SELECT
        m.milestone_code AS code,
        DATE_FORMAT(pm.project_milestone_date, :date_format) as date,
        project_cost_type
      FROM
        project_milestones pm
      JOIN
        milestones m ON (m.milestone_id = pm.project_milestone_milestone)
      JOIN projects on project_id = project_milestone_project
      JOIN project_costs on project_cost_order = project_order 
      WHERE
        m.milestone_in_masterplan = 1
      AND
        pm.project_milestone_project = :project_id
      ");

    $stmt->bindValue(':project_id', $projectID);
    $stmt->bindValue(':date_format', $this->dateFormat);
    $stmt->execute();

    $results = $stmt->fetchAll();

    $values  = array();
    // map milestone dates to masterplan step values
    foreach ($results as $milestone) {
    
      $mile_stone_code = $milestone['code'];
      /*
	  if($milestone['project_cost_type'] == 1 and $mile_stone_code == '1300') // corporate projects
      {
        $mile_stone_code = $mile_stone_code . "cer";
      }
      elseif($milestone['project_cost_type'] == 1 and $mile_stone_code == '1400') // corporate projects
      {
        $mile_stone_code = $mile_stone_code . "cer";
      }
      elseif($milestone['project_cost_type'] != 1 and $mile_stone_code == '0020') // non corporate projects
      {
        $mile_stone_code = $mile_stone_code . "af";
      }
	  

	  if($milestone['project_cost_type'] == 1 and $mile_stone_code == '0070') // corporate projects
      {
        $mile_stone_code = $mile_stone_code . "ln";
      }
      elseif($milestone['project_cost_type'] != 1 and $mile_stone_code == '0070') // corporate projects
      {
        $mile_stone_code = $mile_stone_code . "af";
      }
	  */

	  if (array_key_exists($mile_stone_code, $this->milestoneMap)) {
        $values[$this->milestoneMap[$mile_stone_code]] = $milestone['date'];
      }
    }
    return $values;
  }

  /**
   * Load POS lease data
   * @param  integer $projectOrder
   * @return array
   */
  public function loadPOSLeaseData($projectOrder) {
    // data is either in posleases or posleasespipeline, the query is exactly the same,
    // so we define a template
    $queryTpl = "
      SELECT 
        DATE_FORMAT(poslease_handoverdate, '%1\$s') as handover_date,
        DATE_FORMAT(poslease_startdate, '%1\$s') as rent_start_date,
        DATE_FORMAT(poslease_signature_date, '%1\$s') as lease_signature_date
      FROM
        %2\$s
      WHERE
        poslease_order = %3\$d";
    
    // and do both queries as a union query so we'll be sure to get a result from one
    $result = $this->query(sprintf("
      %s
      UNION
      %s",
      sprintf($queryTpl, $this->dateFormat, 'posleases', $projectOrder),
      sprintf($queryTpl, $this->dateFormat, 'posleasespipeline', $projectOrder)
    ));

    return $result->fetch() ?: array();
  }

  /**
   * Load project order data
   * @param  integer $projectOrder
   * @return array
   */
  protected function loadOrderData($projectOrder) {
    $stmt = $this->prepare("
      SELECT
        t1.transportation_type_name AS transportation_arranged_by,
        t2.transportation_type_name AS transportation_mode
      FROM
        orders o
      JOIN 
        transportation_types t1 ON (t1.transportation_type_id = o.order_preferred_transportation_arranged)
      JOIN
        transportation_types t2 ON (t2.transportation_type_id = o.order_preferred_transportation_mode)
      WHERE
        o.order_id = :project_order
      ");
    $stmt->bindValue(':project_order', $projectOrder);
    $stmt->execute();
    $result = $stmt->fetch();
    $data = array(
      'preferred_transportation' => sprintf('%s %s',
        $result['transportation_arranged_by'],
        $result['transportation_mode']
        )
      );
    return $data;
  }

  /**
   * Load project order state data
   * @param  integer $projectOrder
   * @param  boolean $getAllValues  Get all values if there are multiple dates per step
   * @return array
   */
  protected function loadOrderStateData($projectOrder, $getAllValues = false) {
    $stmt = $this->prepare("
      SELECT
        actual_order_state_state AS state,
        DATE_FORMAT(date_created, :date_format) as date
      FROM
        actual_order_states
      WHERE
        actual_order_state_order = :project_order
      ");
    $stmt->bindValue(':project_order', $projectOrder);
    $stmt->bindValue(':date_format', $this->dateFormat);
    $stmt->execute();

    $results = $stmt->fetchAll();
    $values  = array();
    // map order states to masterplan step values
    foreach ($results as $orderState) {
      if (array_key_exists($orderState['state'], $this->orderStateMap)) {
        $value = $this->orderStateMap[$orderState['state']];
        // when getting all values, add to array instead of creating new one
        if (array_key_exists($value, $values) && $getAllValues) {
          $values[$value][] = $orderState['date'];
        }
        else {
          // add first value when getting all values
          if ($getAllValues) {
            $values[$value] = array($orderState['date']);
          }
          // otherwise directly store the value
          else {
            $values[$value] = $orderState['date']; 
          }
        }
      }
    }
    return $values;
  }

  /**
   * Load order_submitted_to_supplier date
   * This is actually order_state 700 from task centre, but since there can be n order_items, 
   * we use this method to get only the first
   * @param  integer $projectOrder
   * @return array
   */
  protected function loadOrderSubmittedDate($projectOrder) {
    $stmt = $this->prepare("
      SELECT
        DATE_FORMAT(MIN(order_item_ordered), :date_format) AS order_submitted_to_supplier
      FROM order_items
      WHERE 
        order_item_order = :project_order
      AND 
        order_item_type IN (1,2)
      ");
    $stmt->bindValue(':project_order', $projectOrder);
    $stmt->bindValue(':date_format', $this->dateFormat);
    $stmt->execute();

    $result = $stmt->fetch();
    return $result;
  }

  /**
   * Load order_confirmed date
   * This is actually order_state 720 from task centre, but since there can be n order_items,
   * we select only the highest date and only if there are no order_items that aren't unconfirmed
   * @param  integer $projectOrder
   * @return array
   */
  protected function loadOrderConfirmedDate($projectOrder) {
    $stmt = $this->prepare("
      SELECT
        DATE_FORMAT(MAX(order_item_arrival), :date_format) AS order_confirmed
      FROM order_items
      WHERE 
        order_item_order = :project_order
      AND 
        order_item_type IN (1,2)
      AND
        (
        SELECT
          COUNT(*)
        FROM
          order_items
        WHERE
          order_item_order = :project_order
        AND
          order_item_type IN (1,2)
        AND
          order_item_arrival IS NULL
        ) = 0
      ");
    $stmt->bindValue(':project_order', $projectOrder);
    $stmt->bindValue(':date_format', $this->dateFormat);
    $stmt->execute();

    $result = $stmt->fetch();
    return $result;
  }

  /**
   * Load delivery dates of construction items
   * @return array
   */
  protected function loadDeliveryDates($projectOrder) {
    $values = array();
    foreach ($this->itemCategoryMap as $item => $categories) {
      $doFallback = in_array($item, $this->deliveryDatesWithFallbackValues) 
                    && $this->doDeliveryDateFallback;
      $values[$item] = $this->loadDeliveryDate($projectOrder, $categories, $doFallback);
    }
    return $values;
  }

  /**
   * Load a delivery date of n item categories
   * @param  integer $projectOrder
   * @param  array $categories
   * @param  array $doFallback  Get a fallback value (defaults to false)
   * @return string
   */
  protected function loadDeliveryDate($projectOrder, array $categories, $doFallback = false) {
    $result = $this->query(sprintf("
      SELECT
        DATE_FORMAT(d.date_date, '%s') AS date,
        oi.order_item_ordered AS order_date
      FROM
        order_items oi
      JOIN
        dates d ON (d.date_order_item = oi.order_item_id)
      JOIN
        items i ON (i.item_id = oi.order_item_item)
      WHERE
        oi.order_item_order = %d
      AND
        i.item_category IN (%s)
      AND
        d.date_type = 4
      ORDER BY
        oi.order_item_id DESC,
        d.date_date DESC",
      $this->dateFormat,
      $projectOrder,
      join(',', $categories)
    ));
    $row = $result->fetch();

    // getFallbackDeliveryValue relies on these default values if we don't have a row
    $deliveryDate = null;
    $orderDate = false;
    if (is_array($row)) {
      $deliveryDate = $row['date'];
      $orderDate = $row['order_date'];
    }

    // get a fallback value for certain deliverable e.g. "local" when flooring is done locally
    if ($doFallback) {
      $deliveryDate = $this->getFallbackDeliveryValue($projectOrder, $deliveryDate, $orderDate);
    }

    return $deliveryDate;
  }

  /**
   * Get a fallback value for certain delivery dates
   * @param integer $projectOrder
   * @param string|null $deliveryDate
   * @param string|null|boolean $orderDate
   * @return string
   */
  protected function getFallbackDeliveryValue($projectOrder, $deliveryDate, $orderDate) {
    $itemInListOfMaterials = ($orderDate === false);
    $itemOrdered = (is_array($row) && $orderDate != '0000-00-00' && !is_null($orderDate));
    $Order = $this->loadOrder($projectOrder);

    // this is based on the code in mis/projects_query_6_xls.php. I don't really know the
    // logic behind it, I just re-implemented it

    if ($Order->actual_order_state_code >= 600) {
      if ($itemInListOfMaterials) {
        return 'local';
      }
      else if (!$itemOrdered && !$deliveryDate) {
        return 'n.a.';
      }
      else if ($itemOrdered && !$deliveryDate) {
        return 'ordered';
      }
      else {
        return $deliveryDate;
      }
    }
    else {
      if (!$itemInListOfMaterials || (!$itemOrdered && !$deliveryDate)) {
        return 'n.a';
      }
      else if ($itemOrdered && !$deliveryDate) {
        return 'ordered';
      }
      else {
        return $deliveryDate;
      }
    }
  }

  /**
   * Get the latest delivery date from all delivery dates we have for project
   * @param  array $project Project values
   * @return string|null Formatted date or null
   */
  protected function getLatestDeliveryDate($project) {
    // get all delivery date values we know
    $deliveryDates = array_keys($this->itemCategoryMap);
    $latestDate = 0;

    // go through all delivery date values and always save the latest one
    foreach ($project as $key => $value) {
      if (in_array($key, $deliveryDates) && !is_null($value)) {
        $date = strtotime($value);
        if ($date !== false && $date > $latestDate) {
          $latestDate = $date;
        }
      }
    }

    if (!$latestDate) {
      return null;
    }
    // mysql date format string to php.. kinda dirty I know..
    $dateFormat = str_replace('%', '', $this->dateFormat);
    return date($dateFormat, $latestDate);
  }

  /**
   * Load Order object for project order
   * @param  integer $projectOrder
   * @return Order
   */
  protected function loadOrder($projectOrder) {
    $Order = new Order;
    $Order->read($projectOrder);
    return $Order;
  }

  /**
   * Get names of the various order states we offer as values
   * @return array
   */
  protected function getOrderStateNames() {
    $result = $this->query(sprintf("
      SELECT
        order_state_id AS id,
        order_state_code AS code,
        order_state_name AS name
      FROM
        order_states
      WHERE
        order_state_id IN (%s)",
      join(',', array_keys($this->orderStateMap))
      ));

    $results = $result->fetchAll();
    $names = array();
    // map the alias in orderStateMap to order_state_name
    foreach ($results as $row) {
      $names[$this->orderStateMap[$row['id']]] = sprintf('%s %s', $row['code'], $row['name']);
    }
    return $names;
  }

  /**
   * Get milestone texts of milestones we offer as values
   * @return array
   */
  protected function getMilestoneTexts() {
    $result = $this->query(sprintf("
      SELECT
        milestone_code AS code,
        milestone_text AS text
      FROM
        milestones
      WHERE
        milestone_code IN (%s)",
      join(',', array_keys($this->milestoneMap))
      ));

    $results = $result->fetchAll();

    if (!$results) return;

    $names = array();
    // map the alias in milestoneMap to milestone_text
    foreach($results as $row) {
      $names[$this->milestoneMap[$row['code']]] = sprintf('%s %s', $row['code'], $row['text']);
    }
    return $names;
  }
}
