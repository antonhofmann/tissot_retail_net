<?php

/**
 * Project masterplan datamapper
 * Handles all queries related to project masterplans
 */
class Project_Masterplan_Datamapper extends Datamapper {
  /**
   * The default masterplan template to load
   * In theory, you could have multiple masterplan templates but we currently
   * only use one so we hardwire that here
   * @const integer
   */
  const DEFAULT_PROJECT_MASTERPLAN = 1;

  /**
   * Possible values for step_types field
   * @var array
   */
  protected $stepTypes = array(
    'duration',
    'milestone',
    'info'
    );

  /**
   * Get possible step types
   * @return array
   */
  public function getStepTypes() {
    return $this->stepTypes;
  }

  /**
   * Get the complete data for a masterplan
   * Flat means that phases and subphases will be on the same level.
   * Subphases may have steps.
   * @param  integer $masterplanID
   * @return array
   */
  public function getMasterplanDataFlat($masterplanID) {
    // get masterplan data
    $masterplan = $this->getMasterplan($masterplanID);

    // get masterplan phases and and add steps per phase
    $masterplan['phases'] = array();
    $phases = $this->getPhases($masterplanID);
    foreach ($phases as $phase) {
      $phase->steps = $this->getSteps($phase->id);
      $masterplan['phases'][] = $phase;
    }

    return $masterplan;
  }

  /**
   * Get the complete data for a masterplan in tree form
   * Tree means that subphases are children of phases and subphases may have steps
   * @param  integer $projectID
   * @param  integer $masterplanID
   * @param  boolean $filterExcludedPhases Filter out excluded phases according to user preferences
   * @return array
   */
  public function getMasterplanDataTree($projectID = null, $masterplanID = self::DEFAULT_PROJECT_MASTERPLAN, $filterExcludedPhases = true) {
    // get masterplan data
    $masterplan = $this->getMasterplan($masterplanID);

    // get excluded phases in user preferences
    $excluded = array();
    if ($filterExcludedPhases) {
      $User = new User;
      $prefs = $User->getPreferences('project.masterplan', 'excluded_phases');
      if ($prefs) {
        $excluded = explode(',', $prefs);
      }
    }

    // get masterplan phases and and add steps per phase
    $masterplan['phases'] = array();
    $phases = $this->getPhases($masterplanID, null);

    foreach ($phases as $phase) {
      $subphases = $this->getPhases($masterplanID, $phase->id);
      $phase->subphases = array();

      foreach ($subphases as $subphase) {
        if (in_array($subphase->id, $excluded) || $subphase->show_in_project_masterplan != '1') {
          continue;  
        }
        if (!is_null($projectID)) {
          $subphase->steps = new Project_Masterplan_Step_Iterator(
            new ArrayIterator($this->getSteps($subphase->id)), 
            $projectID
            );

          // filter out subphases that don't have steps that apply to this project
          if (iterator_count($subphase->steps) < 1) {
            continue;
          }
        }
        else {
          $subphase->steps = $this->getSteps($subphase->id); 
        }

        $phase->subphases[] = $subphase;
      }

      // add phase if it has subphases
      if (count($phase->subphases)) {
        $masterplan['phases'][] = $phase;
      }
    }

    return $masterplan;
  }

  /**
   * Get an Iterator for masterplan steps
   * @param  integer $masterplanID
   * @return Iterator|Generator
   */
  public function getStepIterator($masterplanID = self::DEFAULT_PROJECT_MASTERPLAN) {
    $masterplan = $this->getMasterplanDataTree(null, $masterplanID);
    // use generator if they're supported
    // disabled because yield causes syntax error on mpx02
    /*if (class_exists('Generator')) {
      function iterator ($masterplan) {
        foreach ($masterplan['phases'] as $phase) {
          foreach ($phase['subphases'] as $subphase) {
            foreach ($subphase['steps'] as $step) {
              yield $step;
            }
          }
        }
      }
      return iterator($masterplan);  
    }*/
    // fallback to ArrayIterator (uses more memory)
    $steps = array();
    foreach ($masterplan['phases'] as $phase) {
      foreach ($phase->subphases as $subphase) {
        foreach ($subphase->steps as $step) {
          $steps[] = $step;
        }
      }
    }
    return new ArrayIterator($steps);
  }

  /**
   * Get a flat list of subphases of a masterplan template
   * @param  boolean $filterExcludedPhases Filter out excluded phases according to user preferences
   * @param  integer $masterplanID
   * @return array
   */
  public function getSubPhases($filterExcludedPhases = true, $masterplanID = self::DEFAULT_PROJECT_MASTERPLAN) {
    $masterplan = $this->getMasterplanDataTree(null, $masterplanID, $filterExcludedPhases);
    $subphases = array();
    foreach ($masterplan['phases'] as $phase) {
      foreach ($phase->subphases as $subphase) {
        $subphase->parent = $phase;
        $subphases[] = $subphase;
      }
    }
    return $subphases;
  }

  /**
   * Get Iterator for masterplan of a specific project
   * This considers the step visibility settings for the given project (e.g. legal type)
   * @param  integer $masterplanID
   * @param  integer $projectID
   * @return Iterator
   */
  public function getProjectStepIterator($projectID, $masterplanID = self::DEFAULT_PROJECT_MASTERPLAN) {
    return new Project_Masterplan_Step_Iterator(
      $this->getStepIterator($masterplanID),
      $projectID
      );
  }

  /**
   * Get data for a masterplan (without linked data)
   * @param  integer $masterplanID
   * @return array
   */
  public function getMasterplan($masterplanID) {
    $result = $this->query(sprintf("
      SELECT
        project_masterplan_id AS id,
        title,
        date_created,
        date_modified
      FROM 
        project_masterplans
      WHERE 
        project_masterplan_id = %d",
      $masterplanID
    ));
    return $result->fetch();
  }

  /**
   * Get masterplan phases
   * - if parentID is false, all phases for $masterplanID are returned
   * - if parentID is null, only phases for $masterplanID with parent_id=NULL are returned
   * - if parentID is an integer, only phases for $masterplanID with parent_id=$parentID are returned
   * @param  integer $masterplanID
   * @param  mixed $parentID
   * @return array
   */
  public function getPhases($masterplanID, $parentID = false) {
    $constraint = '1';
    if (is_null($parentID)) {
      $constraint = 'parent_id IS NULL';
    }
    else if (is_numeric($parentID)) {
      $constraint = sprintf('parent_id = %d', $parentID);
    }

    $result = $this->query(sprintf("
      SELECT
        project_masterplan_phase_id AS id,
        project_masterplan_id AS masterplan_id,
        parent_id,
        pos,
        title,
        pos,
        default_start_value,
        fallback_start_value,
        default_end_value,
        fallback_end_value,
        show_in_project_masterplan,
        show_in_project_overview,
        show_in_excel_masterplan
      FROM project_masterplan_phases
      WHERE 
        project_masterplan_id = %d
        AND
        %s
      ORDER BY
        pos",
      $masterplanID,
      $constraint
    ));

    $result->fetchClass('Masterplan_Phase');
    return $result->fetchAll();
  }

  /**
   * Get all responsible users for a project
   * @param  integer $projectID
   * @param  integer $masterplanID Defaults to default project masterplan
   * @return array   Array with user_id keys and user-array values
   */
  public function getResponsibles($projectID, $masterplanID = null) {
    if (is_null($masterplanID)) {
      $masterplanID = self::DEFAULT_PROJECT_MASTERPLAN;
    }

    // get all roles applicable for masterplan
    $RoleMapper = new Role_Datamapper();
    $roles = $RoleMapper->getMasterplanRoles();

    // find which users hold these roles for the given project
    $responsibles = array();
    $UserModel = new User_Model($this->connector);
    
    foreach ($roles as $role) {
      $user = $this->getResponsibleUser($projectID, $role['id']);
      if (!is_null($user)) {
        $responsibles[$role['id']] = $user;
      }
    }

    return $responsibles;
  }

  /**
   * Get masterplan phase steps
   * @param  integer $phaseID
   * @return array
   */
  public function getSteps($phaseID) {
    $result = $this->query(sprintf("
      SELECT
        project_masterplan_step_id AS id,
        project_masterplan_phase_id AS phase_id,
        step_nr,
        step_type,
        pos,
        title,
        color,
        value,
        responsible_role_id,
        apply_corporate,
        apply_franchisee,
        apply_other,
        apply_store,
        apply_sis,
        apply_kiosk,
        apply_independent,
        is_active
      FROM 
        project_masterplan_steps
      WHERE 
        project_masterplan_phase_id = %d
      ORDER BY 
        pos",
      $phaseID
    ));

    // get step durations
    $steps = $result->fetchAll();
    foreach ($steps as &$step) {
      list($defaults, $exceptions) = $this->getStepDurationsAggregated($step['id']);
      $step['duration_defaults'] = $defaults;
      $step['duration_exceptions'] = $exceptions;
    }

    return $steps;
  }

  /**
   * Get all durations for a step  
   * @param  integer $stepID
   * @return array
   */
  public function getStepDurations($stepID) {
    $stmt = $this->prepare("
      SELECT
        project_masterplan_step_id AS step_id,
        salesregion_id,
        country_id,
        duration
      FROM 
        project_masterplan_step_durations
      WHERE 
        project_masterplan_step_id = :id
    ");
    $stmt->bindValue(':id', $stepID);
    $stmt->execute();
    return $stmt->fetchAll();
  }

  /**
   * Get step durations "aggregated" by defaults and exceptions
   * @param  integer $stepID
   * @return array  [0] => array of defaults [1] => array of exceptions
   */
  public function getStepDurationsAggregated($stepID) {
    $steps = $this->getStepDurations($stepID);
    $defaults = array();
    $exceptions = array();

    foreach ($steps as $step) {
      // country exceptions
      if ($step['country_id']) {
        $exceptions[] = array(
          'country_id'     => $step['country_id'],
          'salesregion_id' => $step['salesregion_id'],
          'duration'       => $step['duration'],
        );
      }
      // regional defaults
      else {
        $defaults[$step['salesregion_id']] = $step['duration'];
      }
    }
    return array($defaults, $exceptions);
  }

  /**
   * Get all durations for a salesregion and country
   * Returns an array with step IDs as keys containing an array with two
   * keys 'country' (if available) and 'salesregion', each containing the duration in days.
   * @param integer $salesRegionID
   * @param integer $countryID
   * @return array
   */
  public function getAllStepDurationsForPlace($salesRegionID, $countryID) {
    $query = "
      SELECT
        project_masterplan_step_id AS step_id,
        salesregion_id,
        country_id,
        duration
      FROM
        project_masterplan_step_durations
      WHERE
        (
          salesregion_id = :salesregion_id
            AND
          country_id IS NULL
        )";

    if ($countryID) {
      $query .= "
        OR 
        (
          salesregion_id = :salesregion_id
          AND
          country_id = :country_id
        )";
    }

    // execute query
    $stmt = $this->prepare($query);
    $stmt->bindValue(':salesregion_id', $salesRegionID);
    if ($countryID) {
      $stmt->bindValue(':country_id', $countryID);
    }
    $stmt->execute();

    // aggregate step durations
    $durations = array();
    while ($row = $stmt->fetch()) {
      // make an array for each step
      if (!is_array($durations[$row['step_id']])) {
        $durations[$row['step_id']] = array();
      }

      // add country value if it exists
      if (!is_null($row['country_id']) && intval($row['country_id']) > 0) {
        $durations[$row['step_id']]['country'] = $row['duration'];
      }
      // add salesregion if it exists (it should)
      if (!is_null($row['salesregion_id']) && intval($row['salesregion_id']) > 0 && !$row['country_id']) {
        $durations[$row['step_id']]['salesregion'] = $row['duration'];
      }
    }
    return $durations;
  }

  /**
   * Get user_id for a user that holds a role for a project
   * 
   * @todo  change data sql to use the role IDs we actually used
   * 
   * @param  integer $projectID
   * @param  integer $roleID
   * @return array   Array of userIDs
   */
  public function getResponsibleUser($projectID, $roleID) {
    // I hate doing this with the roleID but role names change 
    // frequently so it's the best we can do
    switch ($roleID) {
      // these roles are fields on the projects table
      case 3:
      case 7:
      case 8:
      case 10:
        $user = $this->getProjectResponsibilityHolder($projectID, $roleID);
        break;
      // these are country-responsibilities
      case 15:
      case 16:
        $user = $this->getCountryResponsibilityHolder($projectID, $roleID);
        break;
      case 2:
      case 4:
        $user = $this->getOrderResponsibilityHolder($projectID, $roleID);
        break;
      case 5:
      case 6:
        // array of responsibility holders
        $user = $this->getOrderItemsResponsibilityHolders($projectID, $roleID);
        break;
    }
    return $user;
  }

  /**
   * Get the user which holds a given role for a given project
   * @param  integer $projectID
   * @param  string  $roleID
   * @return array   Array with keys 'user_id' and 'name'
   */
  protected function getProjectResponsibilityHolder($projectID, $roleID) {
    // map the role id to it's corresponding field in projects table
    switch ($roleID) {
      case 3:
        $projectColumn = 'project_retail_coordinator';
        break;
      case 7:
        $projectColumn = 'project_design_contractor';
      case 8:
        $projectColumn = 'project_design_supervisor';
        break;
      case 10:
        $projectColumn = 'project_local_retail_coordinator';
        break;
    }

    $result = $this->query(sprintf("
      SELECT
        %1\$s AS user_id,
        CONCAT_WS(' ', user_firstname, user_name) AS name
      FROM
        projects
      JOIN
        users ON (users.user_id = projects.%1\$s)
      WHERE
        project_id = %2\$d
      ",
      $projectColumn,
      $projectID));
    return $result->fetch();
  }

  /**
   * Get the user which holds a given role for a country based on a project
   * @param  integer $projectID
   * @param  string  $roleID
   * @return array   Array with keys 'user_id' and 'name'
   */
  protected function getCountryResponsibilityHolder($projectID, $roleID) {
    $result = $this->query(sprintf("
      SELECT
        u.user_id,
        CONCAT_WS(' ', u.user_firstname, u.user_name) AS name,
        ur.user_role_role AS role_id
      FROM
        projects p
      JOIN 
        orders o ON (o.order_id = p.project_order)
      JOIN
        users u ON (u.user_address = o.order_client_address)
      JOIN
        user_roles ur ON (u.user_id = ur.user_role_user)
      WHERE
        u.user_active = 1
      AND
        p.project_id = %d
      ",
      $projectID));
    $results = $result->fetchAll();

    foreach ($results as $row) {
      if ($row['role_id'] == $roleID) {
        return $row;
      }
    }
    return null;
  }

  /**
   * Get logistics coordinator for a project
   * @param  integer $projectID
   * @return array   Array with keys 'user_id' and 'name'
   */
  protected function getOrderResponsibilityHolder($projectID, $roleID) {
    // map the role id to it's corresponding field in orders table
    switch ($roleID) {
      case 2:
        $projectColumn = 'order_retail_operator';
        break;
      case 4:
        $projectColumn = 'order_user';
    }

    $result = $this->query(sprintf("
      SELECT
        o.%s AS user_id,
        CONCAT_WS(' ', u.user_firstname, u.user_name) AS name
      FROM
        projects p
      JOIN
        orders o ON (o.order_id = p.project_order)
      WHERE
        p.project_id = %d
      ",
      $projectColumn,
      $projectID));
    return $result->fetch();
  }

  /**
   * Get responsibility holders for order items
   * @param  integer $projectID
   * @param  integer $roleID
   * @return array   Array of users with array keys 'user_id' and 'name'
   */
  protected function getOrderItemsResponsibilityHolders($projectID, $roleID) {
    $result = $this->query(sprintf("
      SELECT
        u.user_id,
        a.address_shortcut AS name,
        DATE_FORMAT(d.date_date, '%%d.%%m.%%Y') AS date
      FROM
        projects p
      JOIN
        orders o ON (o.order_id = p.project_order)
      JOIN
        order_items oi ON (oi.order_item_order = o.order_id)
      JOIN
        users u ON (u.user_address = oi.order_item_supplier_address)
      JOIN
        addresses a ON (a.address_id = u.user_address)
      JOIN
        dates d ON (d.date_order_item = oi.order_item_id AND d.date_type = 4)
      JOIN
        items i ON (i.item_id = order_item_item)
      WHERE
        u.user_active = 1
      AND
        p.project_id = %d
      GROUP BY
        u.user_id
      ",
      $projectID));
    return $result->fetchAll();
  }
}
