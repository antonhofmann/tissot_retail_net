<?php

/**
 * Handles all data operations conscerning project mails
 * @todo  rename this datamapper as it no longer only serves project mails - or move pos stuff to pos_mail_datamapper or something
 * @todo  replace subqueries with left joins for mui bien performance
 */
class Project_Mails_Datamapper extends Datamapper {
	/**
	 * @const string
	 */
	const CMS_PROJECTS_LATER_THAN = '2015-08-18';

	/**
	 * @const string
	 */
	const PROJECT_CMS_OVERDUE = 'cms_overdue';

	/**
	 * @const string
	 */
	const PROJECT_OPENS_IN_FUTURE = 'future_opening_date';

	/**
	 * Number of months before which lease termination deadline reminder should be sent
	 * @const
	 */
	const LEASE_DEADLINE_REMINDER = 3;

	/**
	 * Number of weeks before which POS actual opening date notice should be sent
	 * @const
	 */
	const POS_OPENING_DATE_REMINDER = 1;

	/**
	 * @const string
	 */
	const DATE_OVERDUE = 'overdue';

	/**
	 * @const string
	 */
	const DATE_IMMINENT = 'imminent';

	/**
	 * @const string
	 */
	const DATE_MISSING = 'missing';

	/**
	 * Get new projects with missing/overdue dates for mail alerts
	 *
	 * $type:
	 * DATE_MISSING 				project_real_opening_date is missing
	 * DATE_OVERDUE 				project_real_opening_date is overdue
	 * 
	 * @param  string $type One of the DATE_* constants
	 * @return array
	 */
	public function getNewProjectsForMailAlerts($type = self::DATE_MISSING) {
		// set mail_alert_types per type
		$mailAlertTypes = array();
		$mailAlertTypes[self::DATE_MISSING] = array(8, 12, 14);
		$mailAlertTypes[self::DATE_OVERDUE] = array(1, 13, 15);
		$mailAlertTypes[self::DATE_IMMINENT] = $mailAlertTypes[self::DATE_OVERDUE];

		// additional constraints per type
		$constraints = array();
		$constraints[self::DATE_MISSING] = "
			AND (
			  project_real_opening_date IS NULL 
			  OR project_real_opening_date = '0000-00-00'
			  ) 
			";
		$constraints[self::DATE_OVERDUE] = "
			AND (
				project_real_opening_date IS NOT NULL
				AND project_real_opening_date != '0000-00-00'
				AND project_real_opening_date <= CURDATE()
				)
			AND (project_projectkind <> 4 AND project_projectkind <> 5)
			";
		// opening date is in 1 week
		$constraints[self::DATE_IMMINENT] = sprintf("
			AND (
				project_real_opening_date IS NOT NULL
				AND project_real_opening_date != '0000-00-00'
				AND project_real_opening_date > NOW()
				AND project_real_opening_date <= DATE_ADD(CURDATE(), INTERVAL %d WEEK)
				)
			AND (project_projectkind <> 4 AND project_projectkind <> 5)",
			self::POS_OPENING_DATE_REMINDER
			);

	  

		// should it be order_actual_order_state_code <= 800?
	  $query = sprintf("
	    SELECT 
	      project_id,
	      project_projectkind,
	      project_order,
	      project_retail_coordinator,
	      project_real_opening_date
	    FROM 
	      projects
	    INNER JOIN
	      orders ON (order_id = project_order)
	    LEFT JOIN
	    	mail_alerts ON (mail_alert_order = order_id AND mail_alert_type IN (%s))
	    WHERE (
	        LEFT(order_number, 2) >= 26 
	        OR LEFT(order_number, 3) >= 210
	      ) 
	      AND 
	      	project_state <> 2 
	      AND 
	      	order_actual_order_state_code < '800' 
	      AND 
	      	(project_actual_opening_date IS NULL OR project_actual_opening_date = '0000-00-00')
	      AND 
	      	project_retail_coordinator > 1
	      AND 
	      	mail_alert_order IS NULL
	      %s",
	  	join(',', $mailAlertTypes[$type]),
	  	$constraints[$type]
	  );
	  
	  $result = $this->query($query);
	  return $result->fetchAll();
	}

	/**
	 * Get completed project with incomplete cost monitoring sheet
	 * 
	 * $timeConstraint may be
	 * future_opening_date    Projects that open later than NOW
	 * cms_overdue            Projects with overdue cms completion
	 * <number>               Projects where opening date was <number> months ago
	 * 
	 * @param  mixed What time constraint should be used
	 * @return array  Array of projects
	 */
	public function getCompletedProjectsWithIncompleteCMS($timeConstraint = self::PROJECT_OPENS_IN_FUTURE, $legal_type = 0) {
		$constraint = '';
	  
	  // project actual opening date was n months ago
	  $constraint = sprintf('AND NOW() >= DATE_ADD(project_actual_opening_date, INTERVAL %d MONTH)', $timeConstraint);


	  $query = sprintf("
		SELECT 
		  project_id,
		  project_order,
		  order_retail_operator,
		  project_actual_opening_date,
		  project_cost_cms_completion_due_date
		FROM 
		  projects
		JOIN 
		  project_costs ON (project_cost_order = project_order)
		JOIN 
		  orders ON (order_id = project_order)
		WHERE
				project_actual_opening_date > '%s'
		%s
		  AND 
			(order_archive_date is null or order_archive_date = '0000-00-00')
		  AND 
			(project_cost_cms_completed <> 1 OR project_cost_cms_completed IS NULL)
		  AND (project_production_type = 2 or project_production_type = 4)", 
		self::CMS_PROJECTS_LATER_THAN,
		$constraint
	  );
	  
	  if($legal_type == 'noncorporate') {
		$query .= " AND project_cost_type > 1";
	  }
	  elseif($legal_type == 'corporate') {
		$query .= " AND project_cost_type = 1";
	  }


	   //echo $query . "<br /><br /><br />";

	  return $this->query($query)->fetchAll();
	}

	/**
	 * Get completed project with incomplete cost monitoring sheet for product owners
	 * 
	 * $timeConstraint may be
	 * future_opening_date    Projects that open later than NOW
	 * cms_overdue            Projects with overdue cms completion
	 * <number>               Projects where opening date was <number> months ago
	 * 
	 * @param  mixed What time constraint should be used
	 * @return array  Array of projects
	 */
	public function getCompletedProjectsWithIncompleteCMSForProductOwners($timeConstraint = self::PROJECT_OPENS_IN_FUTURE, $legal_type = 0) {
		$constraint = '';
	  // project actual opening date was n months ago
	  if (intval($timeConstraint) > 0) {
	    $constraint = sprintf('AND NOW() >= DATE_ADD(project_actual_opening_date, INTERVAL %d MONTH)', $timeConstraint);
	  }
	  // cms completion is overdue
	  else if ($timeConstraint == self::PROJECT_CMS_OVERDUE) {
	    $constraint = 'AND NOW() >= project_cost_cms_completion_due_date';
	  }

	  $query = sprintf("
	    SELECT
	      project_id,
	      project_order,
	      order_user,
	      project_actual_opening_date,
	      project_cost_cms_completion_due_date
	    FROM
	      projects
	    JOIN 
	      project_costs ON (project_cost_order = project_order)
	    JOIN 
	      orders ON (order_id = project_order)
	    WHERE
	    		project_actual_opening_date > '%s'
      	%s
	      AND 
			(order_archive_date is null or order_archive_date = '0000-00-00')
		  AND 
	      	(project_cost_cms_completed2 <> 1 OR project_cost_cms_completed2 IS NULL)
	       AND (project_production_type = 2 or project_production_type = 4)
	    ",
	    self::CMS_PROJECTS_LATER_THAN,
	    $constraint);


		if($legal_type == 'noncorporate') {
			$query .= " AND project_cost_type > 1";
		  }
		  elseif($legal_type == 'corporate') {
			$query .= " AND project_cost_type = 1";
		  }
	  return $this->query($query)->fetchAll();
	}



	/**
	 * Update cost monitoring sheet due date on completed projects
	 * @return void
	 */
	public function updateCMSDueDates() {
	  // select completed projects with incomplete cms, make sure they have
	  // order items (from HQ?)
	  $query = "
	    SELECT 
	      project_number,
	      project_state,
	      project_order,
	      project_actual_opening_date
	    FROM 
	      projects
	    INNER JOIN
	      orders ON order_id = project_order
	    INNER JOIN
	      project_costs ON project_cost_order = order_id
	    WHERE 
	      	(project_cost_cms_completed <> 1 OR project_cost_cms_completed IS NULL)
	      AND (
	        project_actual_opening_date IS NOT NULL 
	        AND project_actual_opening_date <> '0000-00-00'
	        )
	      AND (
	        order_archive_date IS NULL 
	        OR order_archive_date = '0000-00-00'
	        )
	      AND (
	        project_cost_cms_completion_due_date IS NULL 
	        OR project_cost_cms_completion_due_date = '0000-00-00'
	        )
	      AND project_state IN (1, 4, 5)
	      AND (project_production_type = 2 or project_production_type = 4)
	  ";
	  $projects = $this->query($query)->fetchAll();

	  // @todo we could probably do all this in the above query
	  foreach ($projects as $project) {
	    // add 4 months to the pos opening date
	    $DueDate = new DateTime($project['project_actual_opening_date']);
	    $DueDate->add(new DateInterval('P4M'));

	    // update project costs with new due date for cms
	    $stmt = $this->prepare("
	      UPDATE
	        project_costs
	      SET
	        project_cost_cms_completion_due_date = :due_date
	      WHERE
	        project_cost_order = :project_order
	      ");
	    $stmt->bindValue(':due_date', $DueDate->format('Y-m-d'));
	    $stmt->bindValue(':project_order', $project['project_order']);
	    $stmt->execute();
	  }
	}

	/**
	 * Get completed projects that do not have an opening date
	 * @return array Array of projects
	 */
	public function getCompletedProjectsWithoutOpeningDate() {
	  $query = "
	    SELECT
	      project_id,
	      project_retail_coordinator,
	      project_order,
	      project_projectkind
	    FROM 
	      projects
	    JOIN 
	      orders ON (order_id = project_order)
	    LEFT JOIN
	    	mail_alerts ON (mail_alert_order = order_id AND mail_alert_type = 2)
	    WHERE 
	        (LEFT(order_number, 2) >= 26 OR LEFT(order_number, 3) >= 210) 
	      AND (
	        order_actual_order_state_code = '890' 
					OR (order_logistic_status = '800' and order_actual_order_state_code <= '890')
	      )
	      AND (
	        project_real_opening_date IS NULL 
	        OR project_real_opening_date = '0000-00-00' 
	        OR project_real_opening_date <= NOW()
	      )
	      AND 
	        project_state <> 2
	      AND 
	        (project_actual_opening_date IS NULL OR project_actual_opening_date = '0000-00-00') 
	      AND 
	        (project_projectkind <> 4 AND project_projectkind <> 5)
	      AND
	        order_client_address > 0
	      AND
	      	mail_alert_order IS NULL
	  ";
	  $result = $this->query($query);
	  return $result->fetchAll();
	}

	/**
	 * Get completed take over / lease renewal projects that do not have an opening date
	 * @return array Array of projects
	 */
	public function getCompletedToLrProjectsWithoutOpeningDate() {
	  $query = "
	    SELECT
	      project_id,
	      project_retail_coordinator,
	      project_order,
	      project_projectkind
	    FROM 
	      projects
	    JOIN 
	      orders ON (order_id = project_order)
	    LEFT JOIN
	    	mail_alerts ON (mail_alert_order = order_id AND mail_alert_type IN (17,18))
	    WHERE 
	        (LEFT(order_number, 2) >= 26 OR LEFT(order_number, 3) >= 210) 
	      AND 
	        order_actual_order_state_code <= '890' 
	      AND (
	        project_real_opening_date IS NULL 
	        OR project_real_opening_date = '0000-00-00' 
	        OR project_real_opening_date <= NOW()
	      )
	      AND 
	        project_state <> 2
	      AND 
	        (project_actual_opening_date IS NULL OR project_actual_opening_date = '0000-00-00') 
	      AND 
	        (project_projectkind = 4 OR project_projectkind = 5)
	      AND
	        order_client_address > 0
	      AND
	      	mail_alert_order IS NULL
	  ";
	  $result = $this->query($query);
	  return $result->fetchAll();
	}

	/**
	 * Get POS leases due for renewal
	 * @return array
	 */
	public function getPOSLeasesForRenewal() {
		$query = sprintf("
			SELECT 
				pl.poslease_id,
				posaddress_client_id,
				posaddress_name,
				country_name,
				posowner_type_alias AS posowner_type,
				DATE_SUB(pl.poslease_extensionoption, INTERVAL pl.poslease_termination_time MONTH) AS termination_deadline,
				pl.poslease_extensionoption AS extension_date,
				pl.poslease_enddate AS expiry_date,
				posaddress_id
			FROM 
				posleases pl
			INNER JOIN 
				posaddresses ON (posaddress_id = poslease_posaddress)
			INNER JOIN 
				countries ON (country_id = posaddress_country)
			LEFT JOIN
				posleases pll ON (
					pl.poslease_posaddress = pll.poslease_posaddress 
					AND pll.poslease_id <> pl.poslease_id 
					AND YEAR(pll.poslease_enddate) >= YEAR(pll.poslease_startdate)
					AND pll.poslease_extensionoption > pl.poslease_extensionoption
				)
			INNER JOIN
				posowner_types ON (posaddress_ownertype = posowner_type_id)
			WHERE 
				PERIOD_DIFF(
					DATE_FORMAT(pl.poslease_extensionoption, '%%Y%%m'), DATE_FORMAT(NOW(), '%%Y%%m')
				) <= pl.poslease_termination_time+%d
				AND (posaddress_store_closingdate IS NULL OR posaddress_store_closingdate = '0000-00-00')
	      AND pl.poslease_termination_time > 0
	      AND (
	      	pl.poslease_extensionoption >= NOW()
	      	AND pl.poslease_extensionoption IS NOT NULL
		    	AND pl.poslease_extensionoption <> '0000-00-00'
		    )
		    AND pl.poslease_mailalert IS NOT NULL
		    AND pl.poslease_mailalert2 IS NULL
		    AND pl.poslease_isindexed <> 1
		    AND pll.poslease_id IS NULL
		    ORDER BY 
		    	pl.poslease_posaddress, 
		    	pl.poslease_enddate DESC
			",
			self::LEASE_DEADLINE_REMINDER
			);
		$result = $this->query($query);
		return $result->fetchAll();
	}

	/**
	 * Get expiring POS leases
	 * @return array
	 */
	public function getExpiringPOSLeases() {
		$query = sprintf("
			SELECT 
				pl.poslease_id,
				posaddress_client_id,
				posaddress_name,
				country_name,
				posowner_type_alias AS posowner_type,
				DATE_SUB(pl.poslease_enddate, INTERVAL pl.poslease_termination_time MONTH) AS termination_deadline,
				pl.poslease_enddate AS expiry_date,
				posaddress_id
			FROM 
				posleases pl
			INNER JOIN 
				posaddresses ON (posaddress_id = poslease_posaddress)
			INNER JOIN 
				countries ON (country_id = posaddress_country)
			LEFT JOIN
				posleases pll ON (
					pl.poslease_posaddress = pll.poslease_posaddress 
					AND pll.poslease_id <> pl.poslease_id 
					AND YEAR(pll.poslease_enddate) >= YEAR(pll.poslease_startdate)
					AND pll.poslease_enddate > pl.poslease_enddate
				)
			INNER JOIN
				posowner_types ON (posaddress_ownertype = posowner_type_id)
			WHERE 
				PERIOD_DIFF(
					DATE_FORMAT(pl.poslease_enddate, '%%Y%%m'), DATE_FORMAT(NOW(), '%%Y%%m')
				) <= pl.poslease_termination_time+%d
				AND pl.poslease_enddate > NOW()
				AND (posaddress_store_closingdate IS NULL OR posaddress_store_closingdate = '0000-00-00')
	      AND pl.poslease_termination_time > 0
	      AND (
	      	pl.poslease_extensionoption >= NOW()
	      	AND pl.poslease_extensionoption IS NOT NULL
		    	AND pl.poslease_extensionoption <> '0000-00-00'
		    )
		    AND pl.poslease_mailalert IS NULL
		    AND pl.poslease_isindexed <> 1
		    AND pll.poslease_id IS NULL
		    ORDER BY 
		    	pl.poslease_posaddress, 
		    	pl.poslease_enddate DESC
			", 
			self::LEASE_DEADLINE_REMINDER
			);

		$result = $this->query($query);
		return $result->fetchAll();
	}

	/**
	 * Get tasks for order confirmation with unset reminders
	 * @return array
	 */
	public function getUnsentOrderTaskReminders() {
	  $query = "
	    SELECT
	      task_id,
	      task_user,
	      task_from_user,
	      tasks.date_created AS date,
	      order_id,
	      order_number,
	      order_type,
	      project_id
	    FROM 
	      tasks
	    INNER JOIN 
	      orders ON (order_id = task_order)
	    INNER JOIN 
	      projects ON (project_order = order_id)
	    WHERE 
	        task_order_state IN (35, 45)
	      AND 
	        (task_reminder_sent IS NULL OR task_reminder_sent = '0000-00-00')
	    ORDER BY 
	      task_order
	  ";
	  $results = $this->query($query)->fetchAll();

	  // only return tasks where today is > date-created + 2 days
	  $filtered = __::filter($results, array(self, '_filterFutureDates'));
	  return $filtered;
	}

	/**
	 * Get unsent reminders for item arrival confirmation for forwarders
	 * @return array
	 */
	public function getUnsentArrivalConfirmationReminders() {
		$query = "
			SELECT 
				order_type,
				project_id,
				order_id,
				order_item_forwarder_address,
				order_retail_operator,
				order_number,
				order_id,
				order_item_expected_arrival AS date
		  FROM
		  	order_items
		  INNER JOIN 
		  	orders ON (order_id = order_item_order)
		  INNER JOIN 
		  	projects ON (project_order = order_id)
		  WHERE
		  		order_item_forwarder_address > 0
		  	AND 
		  		(order_item_arrival IS NULL OR order_item_arrival = '0000-00-00')
		  	AND 
		  		(order_item_expected_arrival IS NOT NULL AND order_item_expected_arrival <> '0000-00-00')
		  	AND 
		  		(order_item_reminder_sent_to_forwarder IS NULL OR order_item_reminder_sent_to_forwarder = '0000-00-00')
		  	AND 
		  		(order_actual_order_state_code > '720' AND  order_actual_order_state_code < '800')
		  GROUP BY 
		  	order_id, order_item_forwarder_address
		";
		$results = $this->query($query)->fetchAll();
		$filtered = __::filter($results, array(self, '_filterFutureDates'));
		return $filtered;
	}

	/**
	 * Get unclosed temporary POS
	 * @return array
	 */
	public function getUnclosedTemporaryPOS() {
		$query = "
			SELECT 
				project_id,
				order_id,
				order_user,
				order_client_address,
				project_local_retail_coordinator,
				posorder_posaddress,
				country_name,
				project_number,
				project_planned_closing_date
		  FROM 
		  	projects
		  INNER JOIN 
		  	orders ON (order_id = project_order)
		  INNER JOIN 
		  	posorders ON (posorder_order = order_id)
		  INNER JOIN 
		  	posaddresses ON (posaddress_id = posorder_posaddress)
		  INNER JOIN 
		  	countries ON (country_id = order_shop_address_country)
		  LEFT JOIN
		  	mail_alerts ON (mail_alert_order = order_id AND mail_alert_type = 25)
		  WHERE 
					mail_alert_id IS NULL
		    AND 
		    	project_projectkind <> 8
		    AND 
		    	(project_planned_closing_date IS NOT NULL AND project_planned_closing_date <> '0000-00-00')
		    AND 
		    	(project_actual_opening_date IS NOT NULL AND project_actual_opening_date <> '0000-00-00')
		    AND 
		    	(project_shop_closingdate IS  NULL OR project_shop_closingdate = '0000-00-00')
		    AND 
		    	posaddress_store_subclass = 27 
		    AND 
		    	posaddress_store_planned_closingdate < NOW()
		";
		$results = $this->query($query);
		return $results->fetchAll();
	}

	/**
	 * Get POS with planned closing date that do not have a final closing date
	 * @return array
	 */
 	public function getClosingPOSWithoutClosingDate() {
	 	$query = "
	 		SELECT DISTINCT 
		 		posaddress_id, posaddress_client_id , posaddress_name,
		 		place_name, country_name, posaddress_ownertype, posaddress_country, posaddress_ownertype
	 		FROM 
	 			posaddresses
	 	  INNER JOIN 
	 	  	posaddress_customerservices ON (posaddress_customerservice_posaddress_id = posaddress_id)
	 	  INNER JOIN 
	 	  	countries ON (country_id = posaddress_country)
			INNER JOIN 
				places ON (place_id = posaddress_place_id)
	 	  WHERE 
	 	  		(posaddress_store_closingdate IS NULL OR posaddress_store_closingdate = '0000-00-00')
	 		  AND 
	 		  	(posaddress_store_planned_closingdate IS NOT NULL AND posaddress_store_planned_closingdate <> '0000-00-00')
	 		  AND 
	 		  	posaddress_store_subclass <> 27
	 		  AND 
	 		  	posaddress_store_planned_closingdate <= NOW()	
	  ";
	  $results = $this->query($query);
 		return $results->fetchAll();
	}

	/**
	 * Get project owner of the latest project for a POS
	 * This is here instead of User_Datamapper because it's quite specific to mails
	 * and I don't think it'll be reused elsewhere.
	 * @param  integer $posAddressID
	 * @param  integer $posAddressClientID
	 * @return User|false
	 */
	public function getOwnerOfLatestProject($posAddressID, $posAddressClientID = null) {
    $stmt = $this->prepare("
		 	SELECT 
		 		user_id
	    FROM 
	    	posorders
	    INNER JOIN 
	    	orders ON (posorder_order = order_id)
	    INNER JOIN 
	    	users ON (user_id = order_user)
	    WHERE 
	    	posorder_type = 1
	    AND 
	    	posorder_posaddress = :pos_address_id
	    AND 
	    	user_active = 1
	    ORDER BY 
	    	posorder_opening_date DESC
    ");
    $stmt->bindValue(':pos_address_id', $posAddressID);
    $stmt->execute();

    // fallback to first standard user of pos address
    if (!$stmt->rowCount() && !is_null($posAddressClientID)) {
    	$stmt = $this->prepare("
    		SELECT
    			user_id
    		FROM
    			users
    		INNER JOIN
    			user_roles ON (user_role_user = user_id)
    		WHERE
	    			user_address = :pos_address_client_id
	    		AND
	    			user_role_role = 4
	    		AND
	    			user_active = 1
    		");
    	$stmt->bindValue(':pos_address_client_id', $posAddressClientID);
    	$stmt->execute();
    }

    if (!$stmt->rowCount()) {
    	return false;
    }
    else {
    	$User = new User;
    	$User->read($stmt->fetchColumn(0));
    	return $User;	
    }
	}


	/**
	 * Get project local retail coordinator of the latest project for a POS
	 * This is here instead of User_Datamapper because it's quite specific to mails
	 * and I don't think it'll be reused elsewhere.
	 * @param  integer $posAddressID
	 * @param  integer $posAddressClientID
	 * @return User|false
	 */
	public function getlocalRetailCoordinator($posAddressID, $posAddressClientID = null) {
		$stmt = $this->prepare("
				SELECT 
					user_id
			FROM 
				posorders
			INNER JOIN 
				orders ON (posorder_order = order_id)
			INNER JOIN 
				projects ON (project_order = order_id)
			INNER JOIN 
				users ON (user_id = project_local_retail_coordinator)
			WHERE 
				posorder_type = 1
			AND 
				posorder_posaddress = :pos_address_id
			AND 
				user_active = 1
			ORDER BY 
				posorder_opening_date DESC
		");
		$stmt->bindValue(':pos_address_id', $posAddressID);
		$stmt->execute();

		$user_id =  $stmt->fetchColumn(0);


		if($user_id > 0) {
			$User = new User;
			$User->read($user_id);
			return $User;	
		}
		return false;
	}


	/**
	 * Get client's brand manager
	 * This is here instead of User_Datamapper because it's quite specific to mails
	 * and I don't think it'll be reused elsewhere.
	 * @param  integer $posAddressID
	 * @param  integer $posAddressClientID
	 * @return User|false
	 */
	
	
	public function getBrandManager($posAddressID, $posAddressClientID = null) {
	
		$stmt = $this->prepare("select user_id
								 from users 
								 left join user_roles on user_role_user = user_id  
								 where user_address = :client_address 
								  and user_role_role = 15 and user_active = 1");
		$stmt->bindValue(':client_address', $posAddressClientID);
		$stmt->execute();

		// client's brand manager
		if (1== 2 and $stmt->rowCount()) {
			$User = new User;
			$User->read($stmt->fetchColumn(0));
			return $User;	
		}
		else {

			$stmt = $this->prepare("select address_client_type, address_parent 
										from addresses 
										where address_id = :client_address");
			$stmt->bindValue(':client_address', $posAddressClientID);
			$stmt->execute();

			

			//parent's brand manager in case client is an affiliate
			if ($stmt->rowCount()) {
			
				$stmt = $this->prepare("select user_id
								 from users 
								 left join user_roles on user_role_user = user_id  
								 where user_address = :client_address 
								  and user_role_role = 15 and user_active = 1");
				$stmt->bindValue(':client_address', $posAddressClientID);
				$stmt->execute();

				// client's brand manager
				if ($stmt->rowCount()) {
					$User = new User;
					$User->read($stmt->fetchColumn(0));
					return $User;	
				}
			}
		}
	}


	/**
	 * Get retail manager country
	 * This is here instead of User_Datamapper because it's quite specific to mails
	 * and I don't think it'll be reused elsewhere.
	 * @param  integer $posAddressID
	 * @param  integer $posAddressClientID
	 * @return User|false
	 */
	
	
	public function getRetailManagerCountry($posAddressID, $posAddressClientID = null) {
	
		$stmt = $this->prepare("select user_id
								 from users 
								 left join user_roles on user_role_user = user_id  
								 where user_address = :client_address 
								  and user_role_role = 16 and user_active = 1");
		$stmt->bindValue(':client_address', $posAddressClientID);
		$stmt->execute();

		// client's brand manager
		if (1== 2 and $stmt->rowCount()) {
			$User = new User;
			$User->read($stmt->fetchColumn(0));
			return $User;	
		}
		else {

			$stmt = $this->prepare("select address_client_type, address_parent 
										from addresses 
										where address_id = :client_address");
			$stmt->bindValue(':client_address', $posAddressClientID);
			$stmt->execute();

			

			//parent's brand manager in case client is an affiliate
			if ($stmt->rowCount()) {
			
				$stmt = $this->prepare("select user_id
								 from users 
								 left join user_roles on user_role_user = user_id  
								 where user_address = :client_address 
								  and user_role_role = 16 and user_active = 1");
				$stmt->bindValue(':client_address', $posAddressClientID);
				$stmt->execute();

				// client's brand manager
				if ($stmt->rowCount()) {
					$User = new User;
					$User->read($stmt->fetchColumn(0));
					return $User;	
				}
			}
		}
	}
	

	/**
	 * Get POS with missing opening hours
	 * @return array
	 */
	public function getPosWithoutOpeningHours() {
		$query = "
			SELECT 
				DISTINCT posaddress_id, posaddress_client_id , posaddress_name,
		  	place_name, country_name, posaddress_ownertype, posaddress_country
		  FROM 
		  	posaddresses
		  LEFT JOIN 
		  	posopeninghrs ON (posopeninghr_posaddress_id = posaddress_id)
		  INNER JOIN 
		  	countries ON (country_id = posaddress_country)
		  INNER JOIN 
		  	places ON (place_id = posaddress_place_id)
		  LEFT JOIN
		  	posmails ON (posmail_mail_alert_type = 30 AND posmail_posaddress_id = posaddress_id) 
		  WHERE 
		  		posaddress_store_openingdate >= '2014-03-04'
		  	AND 
		  		posopeninghr_id IS NULL
		  	AND 
		  		posmail_posaddress_id IS NULL
		  	AND
		  		(posaddress_store_closingdate IS NULL OR posaddress_store_closingdate = '0000-00-00')	
		";
		$stmt = $this->prepare($query);
		$stmt->execute();
		return $stmt->fetchAll();
	}

	/**
	 * Get unclosed "popup" POS locations
	 * @return array
	 */
	public function getUnclosedPopupPOS() {
		$query = "
			SELECT 
				project_id,
				order_id,
				project_retail_coordinator,
				order_number,
				country_name,
				order_shop_address_company,
				project_planned_closing_date
		  FROM 
		  	projects
		  INNER JOIN 
		  	orders ON (order_id = project_order)
		  INNER JOIN 
		  	posorders ON (posorder_order = order_id)
		  INNER JOIN 
		  	posaddresses ON (posaddress_id = posorder_posaddress)
		  INNER JOIN 
		  	countries ON (country_id = order_shop_address_country )
		  LEFT JOIN 
		  	mail_alerts ON (mail_alert_order = order_id AND mail_alert_type IN (38))
		  WHERE 
		  		mail_alert_id IS NULL
		  	AND 
		   		(project_planned_closing_date IS NOT NULL AND project_planned_closing_date <> '0000-00-00')
		   	AND 
		   		(project_actual_opening_date IS NOT NULL AND project_actual_opening_date <> '0000-00-00')
		   	AND 
		   		(project_popup_closingdate IS  NULL OR project_popup_closingdate = '0000-00-00')
		   	AND 
		   		project_projectkind = 8 
		   	AND 
		   		project_planned_closing_date < NOW()
		";
	  $stmt = $this->prepare($query);
	  $stmt->execute();
	  return $stmt->fetchAll();
	}

	/**
	 * Get orders with unconfirmed pickup dates for order items 
	 * @return array
	 */
	public function getOrdersWithUncorfirmedPickup() {
		$query = "
			SELECT 
				order_item_id,
				order_item_supplier_address,
				order_item_expected_arrival AS date,
				order_item_arrival,
	   		order_id,
	   		order_number,
	   		order_type,
	   		project_id
	   	FROM 
	   		order_items
	   	INNER JOIN 
	   		orders ON (order_id = order_item_order)
	   	INNER JOIN 
	   		projects ON (project_order = order_id)
	   	WHERE 
	   		order_item_supplier_address > 0
			AND order_item_quantity > 0 
	    	AND order_item_ready_for_pickup < '" . date("Y-m-d") . "' . 
			AND (order_item_pickup IS NULL OR order_item_pickup = '0000-00-00')
      	AND (order_item_expected_arrival IS NOT NULL AND order_item_expected_arrival <> '0000-00-00')
	    	AND (order_item_reminder_sent_to_supplier IS NULL OR order_item_reminder_sent_to_supplier = '0000-00-00')
	    	AND (order_actual_order_state_code > '720' AND  order_actual_order_state_code < '800')
	    GROUP BY 
	    	order_id,
	    	order_item_supplier_address
	    ";
	 	$stmt = $this->prepare($query);
	 	$stmt->execute();

	 	// only return orders where today is > order_item_expected_arrival + 3 days
	 	$results = $stmt->fetchAll();
	 	$self = $this;
	 	$filtered = __::filter($results, function($row) use ($self) { 
	 		return $self->_filterFutureDates($row, 3); 
	 	});
	 	
	 	return $filtered; 
	}

	/**
	 * Get pos addresses without custom services
	 * @return array
	 */
	public function getPOSAddressesWithoutCustomerService() {
		$query = "
			SELECT DISTINCT 
				posaddress_id, posaddress_client_id , posaddress_name,
		  	place_name, country_name, posaddress_ownertype, posaddress_country
		  FROM 
		  	posaddresses
	    LEFT JOIN
	    	posaddress_customerservices on (posaddress_customerservice_posaddress_id = posaddress_id)
	    INNER JOIN
	    	countries on (country_id = posaddress_country)
	    INNER JOIN
	    	places on (place_id = posaddress_place_id)
	    LEFT JOIN
	    	posmails ON (posmail_mail_alert_type = 31 AND posmail_posaddress_id = posaddress_id)
	    WHERE
	    		posaddress_store_openingdate >= '2014-03-04'
	    	AND
	    		(posaddress_store_closingdate is NULL or posaddress_store_closingdate >= '0000-00-00')
	    	AND
	    		posaddress_customerservice_id is NULL
		  	AND
		  		posmail_posaddress_id IS NULL
		";
		$stmt = $this->prepare($query);
		$stmt->execute();
		return $stmt->fetchAll();
	}

	/**
	 * Get certificates that will expire within the next 6 months
	 * @return array
	 */
	public function getExpiringCertificates() {
		$query = "
			SELECT 
				certificate_file_id AS file_id,
				certificate_file_certificate_id AS id,
				certificate_file_expiry_date AS expiry_date
			FROM 
				certificate_files
			WHERE 
					DATE_ADD(CURDATE(), INTERVAL 6 MONTH) >= certificate_file_expiry_date
				AND
					certificate_file_expiry_notice_sent IS NULL
		";
		$stmt = $this->prepare($query);
		$stmt->execute();
		return $stmt->fetchAll();
	}

	/**
	 * Helper function for adjusting dates for weekend and filtering out future dates
	 * @param  array   Array with 'date' key (db record)
	 * @param  integer Number of days to add to date
	 * @return boolean
	 */
	public static function _filterFutureDates($row, $addDays = 2) {
		$Date = new DateTime($row['date']);
		// add 2 days
		$Date->add(new DateInterval(sprintf('P%dD', $addDays)));
		// add 2 days if it's a saturday
		if ($Date->format('N') == 6) {
			$Date->add(new DateInterval('P2D'));
		}
		// add 1 day if it's a sunday
		else if ($Date->format('N') == 7) {
			$Date->add(new DateInterval('P1D'));
		}
		$Today = new DateTime();
		return ($Today > $Date);
	}


	/**
	 * Get certificates that will expire within the next 6 months
	 * @return array
	 */
	public function getDataIntegrityIssue() {
		
		$data = array();

		//catalog orders with inactive owner
		$tmp = array();
		$query = "
			select order_id, order_number, concat(user_name, ' ', user_firstname) as uname
			from orders
			left join users on user_id = order_user
			where (order_archive_date is null or order_archive_date = '0000-00-00')
			and order_actual_order_state_code < '890' and user_active = 0 and order_type = 2;
		";
		$stmt = $this->prepare($query);
		$stmt->execute();

		$rows = $stmt->fetchAll();

		foreach($rows as $key=>$row) {
			$tmp[$row["order_id"]] = array('order_number' => $row["order_number"], 'uname'=> $row['uname'], 'link'=>'/user/order_edit_retail_data.php?oid=' . $row["order_id"]);
		}

		if(count($tmp) > 0) {
			
			$data['Catalog Orders: Inactive Owner'] = $tmp;
		}

		//catalog orders with inactive locistics coordinator
		$tmp = array();
		$query = "
			select order_id, order_number, concat(user_name, ' ', user_firstname) as uname
			from orders
			left join users on user_id = order_retail_operator
			where (order_archive_date is null or order_archive_date = '0000-00-00')
			and order_actual_order_state_code < '890' and user_active = 0 and order_type = 2;
		";
		$stmt = $this->prepare($query);
		$stmt->execute();

		$rows = $stmt->fetchAll();

		foreach($rows as $key=>$row) {
			$tmp[$row["order_id"]] = array('order_number' => $row["order_number"], 'uname'=> $row['uname'], 'link'=>'/user/order_edit_retail_data.php?oid=' . $row["order_id"]);
		}

		if(count($tmp) > 0) {
			
			$data['Catalog Orders: Inactive Logistics Coordinator'] = $tmp;
		}


		//projects with inactive owner
		$tmp = array();
		$query = "
			select project_id, order_number, concat(user_name, ' ', user_firstname) as uname
			from orders
			left join projects on project_order = order_id
			left join users on user_id = order_user
			where (order_archive_date is null or order_archive_date = '0000-00-00')
			and project_state <> 2 and order_actual_order_state_code < '890' and user_active = 0 and order_type = 1;
		";
		$stmt = $this->prepare($query);
		$stmt->execute();

		$rows = $stmt->fetchAll();

		foreach($rows as $key=>$row) {
			$tmp[$row["project_id"]] = array('order_number' => $row["order_number"], 'uname'=> $row['uname'], 'link'=>'/user/project_edit_retail_data.php?pid=' . $row["project_id"]);
		}

		if(count($tmp) > 0) {
			
			$data['Projects: Inactive Owner'] = $tmp;
		}

		//projects with inactive locistics coordinator
		$tmp = array();
		$query = "
			select project_id, order_number, concat(user_name, ' ', user_firstname) as uname
			from orders
			left join projects on project_order = order_id
			left join users on user_id = order_retail_operator
			where (order_archive_date is null or order_archive_date = '0000-00-00')
			and project_state <> 2 and order_actual_order_state_code < '890' and user_active = 0 and order_type = 1;
		";
		$stmt = $this->prepare($query);
		$stmt->execute();

		$rows = $stmt->fetchAll();

		foreach($rows as $key=>$row) {
			$tmp[$row["project_id"]] = array('order_number' => $row["order_number"], 'uname'=> $row['uname'], 'link'=>'/user/project_edit_retail_data.php?pid=' . $row["project_id"]);
		}

		if(count($tmp) > 0) {
			
			$data['Projects: Inactive Logistics Coordinator'] = $tmp;
		}

		//projects with inactive project leader
		$tmp = array();
		$query = "
			select project_id, order_number, concat(user_name, ' ', user_firstname) as uname
			from orders
			left join projects on project_order = order_id
			left join users on user_id = project_local_retail_coordinator
			where (order_archive_date is null or order_archive_date = '0000-00-00')
			and project_state <> 2 and order_actual_order_state_code < '890' and user_active = 0 and order_type = 1;
		";
		$stmt = $this->prepare($query);
		$stmt->execute();

		$rows = $stmt->fetchAll();

		foreach($rows as $key=>$row) {
			$tmp[$row["project_id"]] = array('order_number' => $row["order_number"], 'uname'=> $row['uname'], 'link'=>'/user/project_edit_retail_data.php?pid=' . $row["project_id"]);
		}

		if(count($tmp) > 0) {
			
			$data['Projects: Inactive Project Leader'] = $tmp;
		}

		//projects with inactive local project leader
		$tmp = array();
		$query = "
			select project_id, order_number, concat(user_name, ' ', user_firstname) as uname
			from orders
			left join projects on project_order = order_id
			left join users on user_id = project_local_retail_coordinator
			where (order_archive_date is null or order_archive_date = '0000-00-00')
			and project_state <> 2 and order_actual_order_state_code < '890' and user_active = 0 and order_type = 1;
		";
		$stmt = $this->prepare($query);
		$stmt->execute();

		$rows = $stmt->fetchAll();

		foreach($rows as $key=>$row) {
			$tmp[$row["project_id"]] = array('order_number' => $row["order_number"], 'uname'=> $row['uname'], 'link'=>'/user/project_edit_retail_data.php?pid=' . $row["project_id"]);
		}

		if(count($tmp) > 0) {
			
			$data['Projects: Inactive Local Project Leader'] = $tmp;
		}

		//projects with inactive design supervisor
		$tmp = array();
		$query = "
			select project_id, order_number, concat(user_name, ' ', user_firstname) as uname
			from orders
			left join projects on project_order = order_id
			left join users on user_id = project_design_supervisor
			where (order_archive_date is null or order_archive_date = '0000-00-00')
			and project_state <> 2 and order_actual_order_state_code < '890' and user_active = 0 and order_type = 1;
		";
		$stmt = $this->prepare($query);
		$stmt->execute();

		$rows = $stmt->fetchAll();

		foreach($rows as $key=>$row) {
			$tmp[$row["project_id"]] = array('order_number' => $row["order_number"], 'uname'=> $row['uname'], 'link'=>'/user/project_edit_retail_data.php?pid=' . $row["project_id"]);
		}

		if(count($tmp) > 0) {
			
			$data['Projects: Inactive Design Supervisor'] = $tmp;
		}

		//projects with inactive design contractors
		$tmp = array();
		$query = "
			select project_id, order_number, concat(user_name, ' ', user_firstname) as uname
			from orders
			left join projects on project_order = order_id
			left join users on user_id = project_design_contractor
			where (order_archive_date is null or order_archive_date = '0000-00-00')
			and project_state <> 2 and order_actual_order_state_code < '890' and user_active = 0 and order_type = 1;
		";
		$stmt = $this->prepare($query);
		$stmt->execute();

		$rows = $stmt->fetchAll();

		foreach($rows as $key=>$row) {
			$tmp[$row["project_id"]] = array('order_number' => $row["order_number"], 'uname'=> $row['uname'], 'link'=>'/user/project_edit_retail_data.php?pid=' . $row["project_id"]);
		}

		

		if(count($tmp) > 0) {
			
			$data['Projects: Inactive Design Contractor'] = $tmp;
		}

		return $data;
	}
}