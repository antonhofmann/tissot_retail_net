<?php 

class Item_Subcategory_Model extends Model {
	
	public function __construct($connector) { 
		parent::__construct($connector);
	}
	
	public function read($id) {
		
		$sth = $this->db->query("
			SELECT * 
			FROM item_subcategories 
			WHERE item_subcategory_id = $id
		");
		
		return ($sth) ? $sth->fetch() : null;
	}
	
	public function create($data) {
			
		foreach ($data as $key => $value) {
			$bindFields[] = $key;
			$bindValues[] = ":$key";
		}
	
		$bindFields = join(' , ', $bindFields);
		$bindValues= join(' , ', $bindValues);
		
		$sth = $this->db->prepare("
			INSERT INTO item_subcategories ( $bindFields )
			VALUES ( $bindValues )
		");
	
		foreach ($data as $key => $value) {
			$sth->bindValue(":$key", $value);
		}
	
		$sth->execute();
		
		return $this->db->lastInsertId();
	}
	
	public function update($id, $data) {
	
		foreach ($data as $key => $value) {
			$bindFields[] = "$key = :$key";
		}
	
		$bindFields = join(',', $bindFields);
			
		$sth = $this->db->prepare("
			UPDATE item_subcategories SET $bindFields
			WHERE item_subcategory_id = :id
		");
	
		$sth->bindValue(":id", $id);

		foreach ($data as $key => $value) {
			$sth->bindValue(":$key", $value);
		}

		return $sth->execute();
	}
	
	public function delete($id) {
		
		return $this->db->exec("
			DELETE FROM item_subcategories
			WHERE item_subcategory_id = $id
		");
	}

	public function loader($filters=null) {
		
		$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
	
		$sth = $this->db->query("
			SELECT 
				item_subcategory_id, item_subcategory_name
			FROM item_subcategories
			$filter
			ORDER BY item_subcategory_name
		");
	
		return ($sth) ? $sth->fetchAll() : null;
	}
}
	