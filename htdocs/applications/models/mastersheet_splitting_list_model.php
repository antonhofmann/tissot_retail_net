<?php 

	/**
	 * Master Sheet Splitting List Model
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Mastersheet_Splitting_List_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_mastersheet_splitting_lists 
				WHERE mps_mastersheet_splitting_list_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mps_mastersheet_splitting_lists ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE mps_mastersheet_splitting_lists SET $bindFields
				WHERE mps_mastersheet_splitting_list_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_mastersheet_splitting_lists 
				WHERE mps_mastersheet_splitting_list_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function deleteAll($mastersheet) {
			
			$sth = $this->db->query("
				DELETE FROM mps_mastersheet_splitting_lists 
				WHERE mps_mastersheet_splitting_list_mastersheet_id = $mastersheet
			");
			
			return ($sth) ? true : false;
		}
		
		public function load($mastersheet, $filters=null) { 
			
			$filter = (is_array($filters)) ? join(' AND ', $filters) : null; 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_mastersheet_splitting_lists 
				WHERE mps_mastersheet_splitting_list_mastersheet_id = $mastersheet $filter
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
	}
	