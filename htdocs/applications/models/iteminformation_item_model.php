<?php

	class Iteminformation_Item_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($iteminformation_id, $item) {
			
			$sth = $this->db->prepare("
				SELECT *
				FROM item_information_items
				WHERE item_information_item_item_information_id = :iteminformation_id 
				AND item_information_item_item_id = :item
			");

			$sth->execute(array(
				'iteminformation_id' => $iteminformation_id,
				'item' => $item
			));
			
			return $sth->fetch();
		}

		public function create($iteminformation_id, $item) {

			$sth = $this->db->prepare("
				INSERT INTO item_information_items ( 
					item_information_item_item_information_id,
					item_information_item_item_id,
					user_created,
					date_created
				)
				VALUES (
					:iteminformation_id,
					:item,
					:user,
					NOW()
				)
			");

			$sth->execute(array(
				'iteminformation_id' => $iteminformation_id,
				'item' => $item,
				'user' => User::instance()->login
			));

			return $this->db->lastInsertId();
		}

		public function delete($iteminformation_id, $item) {

			
			$sth = $this->db->prepare("
				DELETE FROM item_information_items
				WHERE item_information_item_item_information_id = :iteminformation_id 
				AND item_information_item_item_id = :item
			");

			return $sth->execute(array(
				'iteminformation_id' => $iteminformation_id,
				'item' => $item
			));
		}

		public function deleteAll($iteminformation_id) {

			$sth = $this->db->prepare("
				DELETE FROM item_information_items
				WHERE item_information_item_item_information_id = :iteminformation_id
			");

			return $sth->execute(array(
				'iteminformation_id' => $iteminformation_id
			));
		}
	}