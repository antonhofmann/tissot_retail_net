<?php 

	class DistChannel_Model extends Model {
	
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
				
			$sth = $this->db->query("
				SELECT *
				FROM mps_distchannels
				WHERE mps_distchannel_id = $id
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mps_distchannels ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE mps_distchannels 
				SET $bindFields
				WHERE mps_distchannel_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM	mps_distchannels
				WHERE mps_distchannel_id = :id
			");
				
			$sth->bindParam('id', $id, PDO::PARAM_INT);
			return $sth->execute();
		}
		
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT 
					mps_distchannel_id, 
					CONCAT(mps_distchannel_group_name, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) AS channel
				FROM mps_distchannels
				INNER JOIN mps_distchannel_groups ON mps_distchannels.mps_distchannel_group_id = mps_distchannel_groups.mps_distchannel_group_id
				$filter
				ORDER BY 
					mps_distchannel_group_name,
					mps_distchannels.mps_distchannel_name,
					mps_distchannels.mps_distchannel_code	
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	}