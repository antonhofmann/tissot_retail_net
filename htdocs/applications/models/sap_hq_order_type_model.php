<?php 

	class Sap_hq_order_type_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->query("
				SELECT *
				FROM sap_hq_order_types 
				WHERE mps_sap_hq_ordertype_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);

			$sth = $this->db->prepare("
				INSERT INTO sap_hq_order_types ( $bindFields )
				VALUES ( $bindValues )
			");
		
			$sth->execute($data);
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE sap_hq_order_types 
				SET $bindFields
				WHERE mps_sap_hq_ordertype_id = :id
			");
			
			$data['id'] = $id;


			return $sth->execute($data);
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM sap_hq_order_types 
				WHERE mps_sap_hq_ordertype_id = $id
			");
			
			return ($sth) ? true : false;
		}


		public function loader() {
			
			$sth = $this->db->query("
				SELECT sap_hq_order_type_id, sap_hq_order_type_name
				FROM sap_hq_order_types 
				ORDER BY sap_hq_order_type_name
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
	}
	