<?php

	class Red_file_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {

			$sth = $this->db->query("
				SELECT *
				FROM db_retailnet_red.red_files
				WHERE red_file_id = $id
			");

			return ($sth) ? $sth->fetch() : null;
		}

		public function create($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO db_retailnet_red.red_files ( $bindFields )
				VALUES ( $bindValues )
			");


			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();

			return $this->db->lastInsertId();
		}

		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE db_retailnet_red.red_files
				SET $bindFields
				WHERE red_file_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		public function delete($id) {

			$sth = $this->db->query("
				DELETE FROM db_retailnet_red.red_files
				WHERE red_file_id = $id
			");

			return ($sth) ? true : false;
		}

		/**
		 * Returns an array of all partners with access to this file
		 * in consideration of access from and access until dates
		 *
		 */
		 public function get_partners_with_access($id) {
			$sth = $this->db->query("
				SELECT * FROM db_retailnet_red.red_file_accesses
				WHERE red_file_access_file_id = $id
			");

			$result = $sth->fetchAll();

			$partners = _array::extract_by_key($result, 'red_file_access_project_partner_id');

			if (is_array($partners)) {
				$sth = $this->db->query("
					SELECT * 
					FROM db_retailnet_red.red_project_partners
					WHERE red_project_partner_id IN (".implode(',', $partners).")
					AND (red_project_partner_access_from = '' OR red_project_partner_access_from IS NULL OR red_project_partner_access_from <= CURDATE())
					AND (red_project_partner_access_until = '' OR red_project_partner_access_until IS NULL OR red_project_partner_access_until >= CURDATE())
				");

				$result = $sth->fetchAll();
				return _array::extract_by_key($result, 'red_project_partner_id');
			}

			return null;
		}

	}