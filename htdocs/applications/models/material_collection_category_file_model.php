<?php 

	/**
	 * Material Collection File Model
	 * @author aserifi
	 *
	 */
	class Material_Collection_Category_File_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) { 
				
			$sth = $this->db->query("
				SELECT *
				FROM mps_material_collection_category_files
				WHERE mps_material_collection_category_file_id = $id
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mps_material_collection_category_files ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE mps_material_collection_category_files 
				SET $bindFields
				WHERE mps_material_collection_category_file_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM	mps_material_collection_category_files
				WHERE mps_material_collection_category_file_id = $id
			");
				
			return ($sth) ? true : false;
		}
	
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT mps_material_collection_category_file_id, mps_material_collection_category_file_code
				FROM mps_material_collection_category_files
				$filter
				ORDER BY mps_material_collection_category_file_code
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	}