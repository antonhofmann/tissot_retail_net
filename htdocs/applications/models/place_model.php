<?php

	class Place_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {
			$sth = $this->db->query("
				SELECT *
				FROM places
				WHERE place_id = $id
			");

			return ($sth) ? $sth->fetch() : null;
		}

		public function create($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);

			$sth = $this->db->prepare("
				INSERT INTO places ( $bindFields )
				VALUES ( $bindValues )
			");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();

			return $this->db->lastInsertId();
		}

		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE places SET $bindFields
				WHERE place_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		public function delete($id) {

			$sth = $this->db->query("
				DELETE FROM places
				WHERE place_id = $id
			");

			return ($sth) ? true : false;
		}

		public function loader($filters) {

			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;

			$sth = $this->db->query("
				SELECT place_id, place_name
				FROM places
				$filter
				ORDER BY place_name
			");

			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function dropdown($filters) {

			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;

			$sth = $this->db->query("
				SELECT place_id, place_name
				FROM places
				$filter
				ORDER BY place_name
			");

			return ($sth) ? $sth->fetchAll() : null;
		}
	}
