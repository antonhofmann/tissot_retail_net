<?php 

	class Supplier_Model extends Model {
	
		public function __construct($connector) {
			parent::__construct($connector);
		}
		
		public function read($id) {
			$sth = $this->db->query("
				SELECT *
				FROM suppliers
				WHERE supplier_id = $id
			");	
			return ($sth) ? $sth->fetch() : false;
		}
		
		public function getFromItem($id) {
			$sth = $this->db->query("
				SELECT * 
				FROM suppliers 
				WHERE supplier_item = $id
			");	

			return ($sth) ? $sth->fetch() : false;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO suppliers ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE suppliers SET $bindFields
				WHERE supplier_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM suppliers
				WHERE supplier_id = $id
			");	
			
			return ($sth) ? true : false;
		}
		
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
				
			$sth = $this->db->query("
				SELECT supplier_id, address_company
				FROM suppliers
				INNER JOIN db_retailnet.addresses ON address_id = supplier_address
				$filter
				ORDER BY address_company
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	}
	