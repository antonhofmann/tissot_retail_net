<?php 

	class Navigation_Controller_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM navigation_controllers 
				WHERE navigation_controller_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO navigation_controllers ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE navigation_controllers SET $bindFields
				WHERE navigation_controller_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			return $this->db->exec("
				DELETE FROM navigation_controllers
				WHERE navigation_controller_id = $id
			");
		}
	
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT 
					navigation_controller_id,
					navigation_controller_caption
				FROM navigation_controllers
				$filter
				ORDER BY navigation_controller_caption
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function dropdown($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT 
					navigation_controller_name,
					navigation_controller_caption
				FROM navigation_controllers
				$filter
				ORDER BY navigation_controller_caption
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	}
	