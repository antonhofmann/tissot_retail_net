<?php 

	/**
	 * Order Sheet Model
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Ordersheet_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) { 
			
			$sth = $this->db->query("
				SELECT *,
					DATE_FORMAT(mps_ordersheet_openingdate,'%d.%m.%Y') AS mps_ordersheet_openingdate,
					DATE_FORMAT(mps_ordersheet_closingdate,'%d.%m.%Y') AS mps_ordersheet_closingdate
				FROM mps_ordersheets 
				WHERE mps_ordersheet_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function hasItems($id) { 
			
			$sth = $this->db->query("
				SELECT COUNT(mps_ordersheet_item_id) AS total
				FROM mps_ordersheet_items
				WHERE mps_ordersheet_item_ordersheet_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO mps_ordersheets ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE mps_ordersheets 
				SET $bindFields
				WHERE mps_ordersheet_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheets 
				WHERE mps_ordersheet_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function delete_items($id) {
				
			$sth = $this->db->query("
					DELETE FROM mps_ordersheet_items
					WHERE mps_ordersheet_item_ordersheet_id = $id
			");
				
			return ($sth) ? true : false;
		}
		
		public function get_versions($id) {
				
			$sth = $this->db->query("
				SELECT *
				FROM mps_ordersheet_versions
				WHERE mps_ordersheet_version_ordersheet_id = $id
			");
				
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function get_items($id) {
		
			$sth = $this->db->query("
				SELECT *
				FROM mps_ordersheet_items
				WHERE mps_ordersheet_item_ordersheet_id = $id
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function hasOwnerQuantities($ordersheet) {
			
			$sth = $this->db->query("
				SELECT COUNT(mps_ordersheet_item_id) AS total
				FROM mps_ordersheet_items
				WHERE mps_ordersheet_item_ordersheet_id = $ordersheet
				AND mps_ordersheet_item_quantity > 0
			");
			
			$result = ($sth) ? $sth->fetch() : null; 
			return ($result['total'] > 0) ? true : false;
		}
		
		public function hasApprovedQuantities($ordersheet) {
			
			$sth = $this->db->query("
				SELECT COUNT(mps_ordersheet_item_id) AS total
				FROM mps_ordersheet_items
				WHERE mps_ordersheet_item_ordersheet_id = $ordersheet
				AND mps_ordersheet_item_quantity_approved > 0
			");
			
			$result = ($sth) ? $sth->fetch() : null; 
			return ($result['total'] > 0) ? true : false;
		}
		
		public function hasConfirmedQuantities($ordersheet) {
			
			$sth = $this->db->query("
				SELECT COUNT(mps_ordersheet_item_id) AS total
				FROM mps_ordersheet_items
				WHERE mps_ordersheet_item_ordersheet_id = $ordersheet
				AND mps_ordersheet_item_quantity_confirmed > 0
			");
			
			$result = ($sth) ? $sth->fetch() : null; 
			return ($result['total'] > 0) ? true : false;
		}
		
		public function hasShippedQuantities($ordersheet) {
			
			$sth = $this->db->query("
				SELECT COUNT(mps_ordersheet_item_id) AS total
				FROM mps_ordersheet_items
				WHERE mps_ordersheet_item_ordersheet_id = $ordersheet
				AND mps_ordersheet_item_quantity_shipped > 0
			");
			
			$result = ($sth) ? $sth->fetch() : null; 
			return ($result['total'] > 0) ? true : false;
		}
		
		public function hasDistributedQuantities($ordersheet) {
			
			$sth = $this->db->query("
				SELECT COUNT(mps_ordersheet_item_id) AS total
				FROM mps_ordersheet_items
				WHERE mps_ordersheet_item_ordersheet_id = $ordersheet
				AND mps_ordersheet_item_quantity_distributed > 0
			");
			
			$result = ($sth) ? $sth->fetch() : null; 
			return ($result['total'] > 0) ? true : false;
		}
			
		public function isDistributed($id) {
			
			$sth = $this->db->query("
				SELECT COUNT(mps_ordersheet_item_id) as total
				FROM mps_ordersheet_items
				WHERE  mps_ordersheet_item_quantity_shipped > 0
				AND mps_ordersheet_item_quantity_distributed != mps_ordersheet_item_quantity_shipped
				AND mps_ordersheet_item_ordersheet_id = $id
			");
			
			$result = ($sth) ? $sth->fetch() : null;
			return ($result['total'] > 0) ? false : true;
		}
	}
	