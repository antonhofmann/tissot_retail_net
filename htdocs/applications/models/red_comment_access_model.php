<?php

	class Red_Comment_Access_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($comment, $partner) {

			$sth = $this->db->query("
				SELECT red_comment_access_id
				FROM red_comment_accesses
				WHERE red_comment_access_comment_id = $comment AND red_comment_access_project_partner_id = $partner
			");

			$result = ($sth) ? $sth->fetch() : null;
			return $result['red_comment_access_id'];
		}

		public function create($comment, $partner) {

			$user = user::instance();
			$user_login = $user->login;

			$sth = $this->db->query("
				INSERT INTO red_comment_accesses (
					red_comment_access_comment_id,
					red_comment_access_project_partner_id,
					user_created,
					date_created
				)
				VALUES (
					$comment,
					$partner,
					'$user_login',
					NOW()
				)
			");

			return $this->db->lastInsertId();
		}

		public function delete($comment, $partner) {

			$sth = $this->db->query("
				DELETE FROM red_comment_accesses
				WHERE red_comment_access_comment_id = $comment AND red_comment_access_project_partner_id = $partner
			");

			return ($sth) ? true : false;
		}
		
		public function deleteAll($comment) {

			$sth = $this->db->query("
				DELETE FROM red_comment_accesses
				WHERE red_comment_access_comment_id = $comment
			");

			return ($sth) ? true : false;
		}
	}