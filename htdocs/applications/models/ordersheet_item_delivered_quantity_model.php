<?php 

	class Ordersheet_Item_Delivered_Quantity_Model extends Model {
		
		/**
		 * Constructor
		 * @param string|integer $connector, application name|id
		 * @return Ordersheet_Item_Delivered_Quantity_Model
		 */
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_delivered_quantities
				WHERE mps_ordersheet_item_delivered_quantity_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function load($item, $filters=null) { 
			
			$filter = (is_array($filters)) ? ' AND '.join(' AND ', $filters) : null;
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_delivered_quantities 
				WHERE mps_ordersheet_item_delivered_quantity_ordersheet_item_id = $item $filter
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function confirm($id) {
			
			$user = user::instance();
			$user_modified = $user->login;
			$date = date('Y-m-d');
				
			return $this->db->query("
				UPDATE mps_ordersheet_item_delivered_quantities SET
					mps_ordersheet_item_delivered_quantity_confirmed = CURRENT_DATE,
					user_modified = '$user_modified'
				WHERE mps_ordersheet_item_delivered_quantity_id = $id
			");
		}
		
		public function add_pos($item, $pos, $quantity) { 
				
			$user = user::instance();
			$user_created = $user->login;
			
			$this->db->query("
				INSERT INTO mps_ordersheet_item_delivered_quantities (
					mps_ordersheet_item_delivered_quantity_ordersheet_item_id,
					mps_ordersheet_item_delivered_quantity_posaddress_id,
					mps_ordersheet_item_delivered_quantity_quantity,
					user_created
				)
				VALUES ( $item, $pos, $quantity, '$user_created' )
			");
			
			return $this->db->lastInsertId();
		}
		
		public function add_warehouse($item, $warehouse, $quantity) { 
				
			$user = user::instance();
			$user_created = $user->login;
			
			$this->db->query("
				INSERT INTO mps_ordersheet_item_delivered_quantities (
					mps_ordersheet_item_delivered_quantity_ordersheet_item_id,
					mps_ordersheet_item_delivered_quantity_warehouse_id,
					mps_ordersheet_item_delivered_quantity_quantity,
					user_created
				)
				VALUES ( $item, $warehouse, $quantity, '$user_created' )
			");
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $quantity) {
		
			$user = user::instance();
			$user_modified = $user->login;
		
			return $this->db->query("
				UPDATE mps_ordersheet_item_delivered_quantities SET
					mps_ordersheet_item_delivered_quantity_quantity = $quantity,
					user_modified = '$user_modified',
					date_modified = NOW()
				WHERE mps_ordersheet_item_delivered_quantity_id = $id
			");
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_item_delivered_quantities 
				WHERE mps_ordersheet_item_delivered_quantity_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function delete_all($item) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_item_delivered_quantities
				WHERE mps_ordersheet_item_delivered_quantity_ordersheet_item_id = $item
			");
			
			return ($sth) ? true : false;
		}
	}
	