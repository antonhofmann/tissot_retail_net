<?php 

	class Exchange_Order_Model extends Model {
		
		public function __construct($connector) {
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ramco_orders 
				WHERE mps_ramco_order_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_from_order_number($pon) {
			
			$sth = $this->db->query("
				SELECT *
				FROM mps_ramco_orders
				WHERE mps_ramco_order_po_number = $pon
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mps_ramco_orders ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE mps_ramco_orders 
				SET $bindFields
				WHERE mps_ramco_order_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ramco_orders 
				WHERE mps_ramco_order_id = $id
			");
			
			return ($sth) ? true : false;
		}
	}