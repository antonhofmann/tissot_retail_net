<?php 

	class Pos_Verification_Data_Model extends Model {
		
		public function __construct($connector=null) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM posverification_data
				WHERE posverification_data_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_verification($verification, $channel) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM posverification_data
				WHERE posverification_data_posverification_id = $verification AND posverification_data_distribution_channel_id = $channel
			");

			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO posverification_data ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE posverification_data SET $bindFields
				WHERE posverification_data_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM posverification_data
				WHERE posverification_data_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function load($id) {
				
			$sth = $this->db->query("
				SELECT *
				FROM posverification_data
				WHERE posverification_data_posverification_id = $id
			");
				
			return ($sth) ? $sth->fetchAll() : null;
		}
	}
	