<?php 

	/**
	 * Order Sheet Item Shipped Model
	 * @author adoweb
	 * @copyright mediaparx ag
	 * @version 1.0
	 */
	class Ordersheet_Item_Shipped_Model extends Model {
		
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_shipped
				WHERE mps_ordersheet_item_shipped_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_from_reference($ponumber, $reference) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_shipped
				WHERE mps_ordersheet_item_shipped_po_number = '$ponumber'
				AND mps_ordersheet_item_shipped_material_code = '$reference'
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function load($item, $filters=null) { 
			
			$filter = (is_array($filters)) ? ' AND '.join(' AND ', $filters) : null;
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_shipped 
				WHERE mps_ordersheet_item_shipped_ordersheet_item_id = $item $filter
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function create($data) { 
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);

			$sth = $this->db->prepare("
				INSERT INTO mps_ordersheet_item_shipped ( $bindFields )
				VALUES ( $bindValues )
			");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();

			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE mps_ordersheet_item_shipped SET $bindFields
				WHERE mps_ordersheet_item_shipped_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_item_shipped 
				WHERE mps_ordersheet_item_shipped_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function delete_all($item) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_item_shipped
				WHERE mps_ordersheet_item_shipped_ordersheet_item_id = $item
			");
			
			return ($sth) ? true : false;
		}
	}
	