<?php 

	/**
	 * Order Sheet Version Model
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Ordersheet_Version_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function load($ordersheet) { 
			
			$sth = $this->db->query("
				SELECT *,
					DATE_FORMAT(mps_ordersheet_version_openingdate,'%d.%m.%Y') AS mps_ordersheet_version_openingdate,
					DATE_FORMAT(mps_ordersheet_version_closingdate,'%d.%m.%Y') AS mps_ordersheet_version_closingdate
				FROM mps_ordersheet_versions 
				WHERE mps_ordersheet_version_ordersheet_id = $ordersheet
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function read($id) { 
			
			$sth = $this->db->query("
				SELECT *,
					DATE_FORMAT(mps_ordersheet_version_openingdate,'%d.%m.%Y') AS mps_ordersheet_version_openingdate,
					DATE_FORMAT(mps_ordersheet_version_closingdate,'%d.%m.%Y') AS mps_ordersheet_version_closingdate
				FROM mps_ordersheet_versions 
				WHERE mps_ordersheet_version_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mps_ordersheet_versions ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE mps_ordersheet_versions 
				SET $bindFields
				WHERE mps_ordersheet_version_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_versions 
				WHERE mps_ordersheet_version_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function delete_items($id) {
				
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_version_items
				WHERE mps_ordersheet_version_item_ordersheetversion_id = $id
			");
				
			return ($sth) ? true : false;
		}
	}