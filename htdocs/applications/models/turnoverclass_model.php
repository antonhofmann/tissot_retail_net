<?php 

	/**
	 * Turnover Class Model
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class TurnoverClass_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_turnoverclasses
				WHERE mps_turnoverclass_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
			
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
			
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
				
			$sth = $this->db->prepare("
				INSERT INTO mps_turnoverclasses ( $bindFields )
				VALUES ( $bindValues )
			");
			
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
			
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
		
			$sth = $this->db->prepare("
				UPDATE mps_turnoverclasses SET $bindFields
				WHERE mps_turnoverclass_id = :id
			");
	
			$sth->bindValue(":id", $id);
	
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
	
			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_turnoverclasses
				WHERE mps_turnoverclass_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
				
			$sth = $this->db->query("
				SELECT mps_turnoverclass_id, mps_turnoverclass_name 
				FROM mps_turnoverclasses 
				$filter
				ORDER BY mps_turnoverclass_name
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
	}