<?php 

	class SapBlockCode_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->query("
				SELECT *
				FROM sap_blockcodes 
				WHERE sap_blockcode_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);

			$sth = $this->db->prepare("
				INSERT INTO sap_blockcodes ( $bindFields )
				VALUES ( $bindValues )
			");
		
			$sth->execute($data);
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE sap_blockcodes 
				SET $bindFields
				WHERE sap_blockcode_id = :id
			");
			
			$data['id'] = $id;


			return $sth->execute($data);
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM sap_blockcodes 
				WHERE sap_blockcode_id = $id
			");
			
			return ($sth) ? true : false;
		}
	}
	