<?php 

	class Pos_Opening_Hour_Model extends Model {
	
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->query("
				SELECT *
				FROM posopeninghrs
				WHERE posopeninghr_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;  	
		}
		
		public function read_from_range($pos, $day, $from, $to) {
			
			$sth = $this->db->query("
				SELECT *
				FROM posopeninghrs
				WHERE 
					posopeninghr_posaddress_id = $pos
				AND	posopeninghr_weekday_id = $day
				AND (
					(posopeninghr_from_openinghr_id >= $from AND posopeninghr_from_openinghr_id < $to) OR
					(posopeninghr_to_openinghr_id <= $to AND  posopeninghr_to_openinghr_id > $from)
				)
			");
			
			return ($sth) ? $sth->fetch() : null;  	
		}
		
		public function load($pos) {
			
			$sth = $this->db->query("
				SELECT *
				FROM posopeninghrs
				WHERE posopeninghr_posaddress_id = $pos
			");
			
			return ($sth) ? $sth->fetchAll() : null;  	
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO posopeninghrs ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE posopeninghrs 
				SET $bindFields
				WHERE posopeninghr_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM	posopeninghrs
				WHERE posopeninghr_id = $id
			");
				
			return ($sth) ? true : false;
		}
	}