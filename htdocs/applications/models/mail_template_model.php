<?php

	Class Mail_Template_Model extends Model {
			
		public function __construct($connector) {
			parent::__construct($connector);
		}
		
		public function read($id) {
		
			$sth = $this->db->query("
				SELECT * 
				FROM mail_templates 
				WHERE mail_template_id = $id	
			");
		
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_from_shortcut($param) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM mail_templates 
				WHERE mail_template_shortcut = '$param'		
			");
			
			return ($sth)? $sth->fetch() : null;
		}
		
		public function create($data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO mail_templates ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
		
			$sth = $this->db->prepare("
				UPDATE mail_templates 
				SET $bindFields
				WHERE mail_template_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mail_templates 
				WHERE mail_template_id = $id
			");
			
			return ($sth) ? true : false;
		}
	}
