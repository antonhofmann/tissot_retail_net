<?php 

	class Security_Model extends Model {
	
		public function __construct($connnector) { 
			parent::__construct($connnector);
		}

		public function isExcluded($ip) {
			
			$sth = $this->db->query("
				SELECT sec_excluded_ip_id 
				FROM sec_excluded_ips 
				WHERE sec_excluded_ip_exclude = 1 AND sec_excluded_ip_ip = '$ip'
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function isLocked($ip) {
			
			$sth = $this->db->query("
				SELECT COUNT(sec_lockedip_ip) as total 
				FROM sec_lockedips 
				WHERE sec_lockedip_ip = '$ip'
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function lock($ip) {
			
			$sth = $this->db->prepare("
				INSERT INTO sec_lockedips (sec_lockedip_ip,sec_lockedip_date) 
				VALUES ( :ip, :date )
			");
			
			return $sth->execute(array(
				':ip' => $ip, 
				':date' => date('Y-m-d:H-i-s')
			));
		}
		
		public function unlock($ip) {
			
			$sth = $this->db->prepare("
				DELETE FROM sec_lockedips 
				WHERE sec_lockedip_ip = :ip
			");
			
			return $sth->execute(array(
				':ip' => $ip
			));
		}
		
		public function trials($ip) {
			
			$sth = $this->db->query("
				SELECT count(sec_lockedip_ip) AS total 
				FROM sec_lockedips 
				WHERE sec_lockedip_ip = '$ip'
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function traceFailure($ip,$source,$username,$password) {
			
			$sth = $this->db->prepare("
				INSERT INTO sec_loginfailures ( 
					sec_loginfailure_ip, 
					sec_loginfailure_source, 
					sec_loginfailure_user, 
					sec_loginfailure_password, 
					sec_loginfailure_date 
				) 
				VALUES ( :ip, :source, :username, :password, :date )
			");
				
			return $sth->execute(array(
				':ip' => $ip,
				':source' => $source,
				':username' => $username,
				':password' => $password,
				':date' => date('Y-m-d:H-i-s')
			));
		} 
		
		public function permissions() {
			
			$sth = $this->db->query("
				SELECT permission_name AS permission
				FROM permissions
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function userPermissions($user) {

			$sth = $this->db->query("
				SELECT DISTINCT permission_name AS permission
				FROM user_roles 
				LEFT JOIN roles ON user_role_role = role_id 
				LEFT JOIN role_permissions ON role_id = role_permission_role 
				LEFT JOIN permissions ON role_permission_permission = permission_id 
				WHERE user_role_user = $user
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
	}
	