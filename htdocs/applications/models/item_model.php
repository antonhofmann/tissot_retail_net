<?php 

	class Item_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM items 
				WHERE item_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO items ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE items SET $bindFields
				WHERE item_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			return $this->db->exec("
				DELETE FROM items
				WHERE item_id = $id
			");
		}
	
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT 
					item_id,
					CONCAT(item_code,', ', item_name) AS item_name
				FROM items
				$filter
				ORDER BY item_code, item_name
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	}
	