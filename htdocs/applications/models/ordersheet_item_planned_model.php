<?php 

	class Ordersheet_Item_Planned_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_planned
				WHERE mps_ordersheet_item_planned_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function load($item, $filters=null) { 
			
			$filter = (is_array($filters)) ? ' AND '.join(' AND ', $filters) : null;
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_planned 
				WHERE AND mps_ordersheet_item_planned_ordersheet_item_id = $item $filter
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function add_pos($item, $pos, $quantity) { 
			
			$user = user::instance();
			$user_created = $user->login;
			
			$this->db->query("
				INSERT INTO mps_ordersheet_item_planned (
					mps_ordersheet_item_planned_ordersheet_item_id,
					mps_ordersheet_item_planned_posaddress_id,
					mps_ordersheet_item_planned_quantity,
					user_created
				)
				VALUES ( $item, $pos, $quantity, '$user_created' )
			");
			
			return $this->db->lastInsertId();
		}
		
		public function add_warehouse($item, $warehouse, $quantity) { 
			
			$user = user::instance();
			$user_created = $user->login;
			
			$this->db->query("
				INSERT INTO mps_ordersheet_item_planned (
					mps_ordersheet_item_planned_ordersheet_item_id,
					mps_ordersheet_item_planned_warehouse_id,
					mps_ordersheet_item_planned_quantity,
					user_created
				)
				VALUES ( $item, $warehouse, $quantity, '$user_created' )
			");
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $quantity) {
			
			$user = user::instance();
			$user_modified = $user->login;
			
			$quantity = ($quantity) ? $quantity : 'NULL';
		
			return $this->db->query("
				UPDATE mps_ordersheet_item_planned SET
					mps_ordersheet_item_planned_quantity = $quantity,
					user_modified = '$user_modified',
					date_modified = NOW()
				WHERE mps_ordersheet_item_planned_id = $id
			");
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_item_planned 
				WHERE mps_ordersheet_item_planned_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function delete_all($item) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_item_planned 
				WHERE mps_ordersheet_item_planned_ordersheet_item_id = $item
			");
			
			return ($sth) ? true : false;
		}
		
		public function getStockReserve($item) {

			$sth = $this->db->query("
				SELECT 
					mps_ordersheet_item_planned_id,
					mps_ordersheet_item_planned_ordersheet_item_id,
					mps_ordersheet_item_planned_posaddress_id,
					mps_ordersheet_item_planned_warehouse_id,
					mps_ordersheet_item_planned_quantity
				FROM mps_ordersheet_item_planned
				INNER JOIN mps_ordersheet_warehouses ON mps_ordersheet_warehouse_id = mps_ordersheet_item_planned_warehouse_id
				WHERE mps_ordersheet_item_planned_ordersheet_item_id = $item
				AND mps_ordersheet_warehouse_stock_reserve = 1
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function sumQuantites($item, $reserve) {
			
			$filter = ($reserve) ? "AND mps_ordersheet_item_planned_id != $reserve" : null;
			
			$sth = $this->db->query("
				SELECT SUM(mps_ordersheet_item_planned_quantity) AS total
				FROM mps_ordersheet_item_planned
				WHERE mps_ordersheet_item_planned_ordersheet_item_id = $item $filter
			");
			
			$result = ($sth) ? $sth->fetch() : array();
			
			return $result['total'];
		}
	}
	