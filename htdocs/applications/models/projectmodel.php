<?php 

	class ProjectModel extends Model {
	
		public function __construct() { 
			
			parent::__construct();
		}
		
		public function read($id) {
			
			$sth = $this->db->prepare("
				SELECT 
					p.*,
					pt.project_costtype_alias AS project_costtype
				FROM 
					projects p
				LEFT JOIN 
					project_costs pc ON (pc.project_cost_order = p.project_order)
				LEFT JOIN
					project_costtypes pt ON (pt.project_costtype_id = pc.project_cost_type)
				WHERE 
					p.project_id = :id		
			");
			
			$sth->bindParam('id', $id, PDO::PARAM_INT);
			
			$sth->execute();
			
			return $sth->fetch();
		}

		/**
		 * Read project by it's project number instead of id
		 * @param  string $projectNr
		 * @return array  Result of this::read()
		 */
		public function readByProjectNumber($projectNr) {
			$stmt = $this->db->prepare("
				SELECT
					project_id
				FROM
					projects
				WHERE
					project_number = :project_number
				");
			$stmt->bindParam('project_number', $projectNr);
			$stmt->execute();
			return $this->read($stmt->fetchColumn(0));
		}
		
		public function create($data) {
			
			$statement = Database::insertStatement('projects', $data);
			
			$sth = $this->db->prepare($statement);
			
			$sth->execute($data);
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
			
			$statement = Database::updateStatement('projects', 'project_id', $data);
			
			$sth = $this->db->prepare($statement);
			
			$data['project_id'] = $id;
			
			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->prepare("
				DELETE FROM projects
				WHERE project_id = :id		
			");
			
			$sth->bindParam('id', $id, PDO::PARAM_INT);
			
			return$sth->execute();
		}

		/**
		 * Get a project's cost type
		 * @param  integer $id
		 * @return string
		 */
		public function getCostType($id) {
			$sth = $this->db->prepare("
				SELECT 
					pt.project_costtype_alias AS cost_type
				FROM 
					projects p
				LEFT JOIN 
					project_costs pc ON (pc.project_cost_order = p.project_order)
				LEFT JOIN
					project_costtypes pt ON (pt.project_costtype_id = pc.project_cost_type)
				WHERE 
					p.project_id = :id		
			");
			$sth->bindParam('id', $id, PDO::PARAM_INT);
			$sth->execute();
			return __::pick($sth->fetch(), 'cost_type');
		}
	}