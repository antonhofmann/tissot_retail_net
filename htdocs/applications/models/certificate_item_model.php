<?php

	class Certificate_Item_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($certificate, $item) {
			
			$sth = $this->db->prepare("
				SELECT *
				FROM item_certificates
				WHERE item_certificate_certificate_id = :certificate 
				AND item_certificate_item_id = :item
			");

			$sth->execute(array(
				'certificate' => $certificate,
				'item' => $item
			));
			
			return $sth->fetch();
		}

		public function create($certificate, $item) {

			$sth = $this->db->prepare("
				INSERT INTO item_certificates ( 
					item_certificate_certificate_id,
					item_certificate_item_id,
					user_created,
					date_created
				)
				VALUES (
					:certificate,
					:item,
					:user,
					NOW()
				)
			");

			$sth->execute(array(
				'certificate' => $certificate,
				'item' => $item,
				'user' => User::instance()->login
			));

			return $this->db->lastInsertId();
		}

		public function delete($certificate, $item) {

			$sth = $this->db->prepare("
				DELETE FROM item_certificates
				WHERE item_certificate_certificate_id = :certificate 
				AND item_certificate_item_id = :item
			");

			return $sth->execute(array(
				'certificate' => $certificate,
				'item' => $item
			));
		}

		public function deleteAll($certificate) {

			$sth = $this->db->prepare("
				DELETE FROM item_certificates
				WHERE item_certificate_certificate_id = :certificate
			");

			return $sth->execute(array(
				'certificate' => $certificate
			));
		}
	}