<?php

	class Mastersheet_Delivery_Date_Model extends Model {
		
		public function __construct($connector) {
			parent::__construct($connector);
		}
		
		public function read($mastersheet, $planning, $category) {
			
			$sth = $this->db->query("
				SELECT *,
					DATE_FORMAT(mps_mastersheet_delivery_date_date,'%d.%m.%Y') AS mps_mastersheet_delivery_date_date
				FROM mps_mastersheet_delivery_dates
				WHERE mps_mastersheet_delivery_date_mastersheet_id = $mastersheet
				AND mps_mastersheet_delivery_date_material_planning_type_id = $planning
				AND mps_mastersheet_delivery_date_material_collection_category_id = $category
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($mastersheet, $planning, $category, $date) {
			
			$sth = $this->db->prepare("
				INSERT INTO mps_mastersheet_delivery_dates (
					mps_mastersheet_delivery_date_mastersheet_id,
					mps_mastersheet_delivery_date_material_planning_type_id,
					mps_mastersheet_delivery_date_material_collection_category_id,
					mps_mastersheet_delivery_date_date,
					user_created,
					date_created
				) VALUES (
					:mastersheet,
					:planning,
					:category,
					:date,
					:user,
					:created
				)
			");
			
			$user = user::instance();
			
			$sth->execute(array(
				'mastersheet' => $mastersheet,
				'planning' => $planning,
				'category' => $category,
				'date' => $date,
				'user' => $user->login,
				'created' => date('Y-m-d')
			));
				
			return $this->db->lastInsertId();
		}
		
		public function update($mastersheet, $planning, $category, $date) {
			
			$sth = $this->db->prepare("
				UPDATE mps_mastersheet_delivery_dates SET (
					mps_mastersheet_delivery_date_date = :date,
					user_modified = :user,
					date_modified = :modified
				)
				WHERE 
					mps_mastersheet_delivery_date_mastersheet_id = :mastersheet,
					AND mps_mastersheet_delivery_date_material_planning_type_id = :planning,
					AND mps_mastersheet_delivery_date_material_collection_category_id = :category
			");
			
			$user = user::instance();
			
			return $sth->execute(array(
				'mastersheet' => $mastersheet,
				'planning' => $planning,
				'category' => $category,
				'date' => $date,
				'user' => $user->login,
				'modified' => date('Y-m-d')
			));
		}
		
		public function delete($mastersheet, $planning, $category) {
			
			$sth = $this->db->query("
				DELETE FROM mps_mastersheet_delivery_dates
				WHERE mps_mastersheet_delivery_date_mastersheet_id = $mastersheet
				AND mps_mastersheet_delivery_date_material_planning_type_id = $planning
				AND mps_mastersheet_delivery_date_material_collection_category_id = $category
			");
				
			return ($sth) ? true : false;
		}
		
		public function deleteAll($mastersheet) {
			
			$sth = $this->db->query("
				DELETE FROM mps_mastersheet_delivery_dates
				WHERE mps_mastersheet_delivery_date_mastersheet_id = $mastersheet
			");
				
			return ($sth) ? true : false;
		}
		
		public function load($mastersheet, $filters=null) {
			
			$filter = (is_array($filters)) ? join(' AND ', $filters) : null;
			
			$sth = $this->db->query("
				SELECT *,
					DATE_FORMAT(mps_mastersheet_delivery_date_date,'%d.%m.%Y') AS mps_mastersheet_delivery_date_date
				FROM mps_mastersheet_delivery_dates
				WHERE mps_mastersheet_delivery_date_mastersheet_id = $mastersheet $filter
			");
				
			return ($sth) ? $sth->fetchAll() : null;
		}
	}