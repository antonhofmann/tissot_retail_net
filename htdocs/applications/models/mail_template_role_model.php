<?php 

	class Mail_Template_Role_Model extends Model {

		
		public function __construct() {
			
			parent::__construct();
		}
		
		public function loadAll($template) {
			
			$sth = $this->db->prepare("
				SELECT * 
				FROM mail_template_roles
				WHERE mail_template_role_template_id = :template	
			");
			
			$sth->bindParam('template', $template);
			$sth->execute();
			
			return $sth->fetchAll();
		}
		
		public function read($template, $role) {
			
			$sth = $this->db->prepare("
				SELECT * 
				FROM mail_template_roles
				WHERE mail_template_role_template_id = :template
				AND mail_template_role_role_id = :role		
			");
			
			$sth->execute(array(
				'template' => $template,
				'role' => $role
			));
			
			return $sth->fetch();
		}
		
		public function create($template, $role, $cc) {
			
			$sth = $this->db->prepare("
				INSERT INTO mail_template_roles (
					mail_template_role_template_id,
					mail_template_role_role_id,
					mail_template_role_cc,
					user_created
				) VALUES (
					:template, :role, :cc, :created
				)	
			");
			
			$sth->execute(array(
				'template' => $template,
				'role' => $role,
				'cc' => $cc,
				'created' => User::instance()->login
			));
			
			return $this->db->lastInsertId();
		}
		
		public function delete($template, $role) {
			
			$sth = $this->db->prepare("
				DELETE FROM mail_template_roles
				WHERE mail_template_role_template_id = :template
				AND mail_template_role_role_id = :role		
			");
			
			return $sth->execute(array(
				'template' => $template,
				'role' => $role
			));
		}
	}