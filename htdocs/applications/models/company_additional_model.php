<?php 

	class Company_Additional_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
				
		public function read($id) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM address_additionals
				WHERE address_additional_address = $id 
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO address_additionals ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
			
			$bindFields = join(',', $bindFields);
		
			$sth = $this->db->prepare("
				UPDATE address_additionals 
				SET $bindFields 
				WHERE address_additional_address = :id
			");
		
			$sth->bindValue(":id", $id);
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			return $sth->execute();
		}
		
		public function delete($company) {
			
			$sth = $this->db->query("
				DELETE FROM address_additionals
				WHERE address_additional_address = $id
			");
			
			return ($sth) ? true : false;
		}
	}
	