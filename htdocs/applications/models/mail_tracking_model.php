<?php

	Class Mail_Tracking_Model extends Model {
			
		public function __construct($connector) {
			parent::__construct($connector);
		}
		
		public function create($data) {
			
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
				
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mail_trackings ( $bindFields )
				VALUES ( $bindValues )
			");
				
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
				
			$sth->execute();

			return $this->db->lastInsertId();
		}

		/**
		 * Check if a certain mail template was already sent to a recipient
		 * @param  integer $templateID
		 * @param  integer $recipientID
		 * @return boolean
		 */
		public function wasMailSentTo($templateID, $recipientID) {
			$sth = $this->db->prepare("
				SELECT 
					COUNT(*)
				FROM 
					mail_trackings
				WHERE 
						mail_tracking_mail_template_id = :template_id
					AND 
						mail_tracking_recipient_user_id = :recipient_id
				");
			$sth->bindValue(':template_id', $templateID);
			$sth->bindValue(':recipient_id', $recipientID);
			$sth->execute();
			return $sth->rowCount() > 0;
		}
		
		public function read($id) {
			
			$sth = $this->db->query(sprintf("
				SELECT * 
				FROM mail_trackings
				WHERE mail_tracking_id = %d
			", $id));
				
			return ($sth) ? $sth->fetch() : false;
		}
		
		public function delete($id) {
			
			$sth = $this->db->query(sprintf("
				DELETE FROM mail_trackings 
				WHERE mail_tracking_id = %d
			", $id));
			
			return ($sth) ? true : false;
		}
	}
