<?php 

	class Pos_Closing_Day_Model extends Model {
	
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->query("
				SELECT *
				FROM posclosinghrs
				WHERE posclosinghr_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;  	
		}
		
		public function read_from_pos($pos) {
			
			$sth = $this->db->query("
				SELECT *
				FROM posclosinghrs
				WHERE posclosinghr_posaddress_id = $pos
			");
			
			return ($sth) ? $sth->fetch() : null;  	
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO posclosinghrs ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE posclosinghrs 
				SET $bindFields
				WHERE posclosinghr_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM	posclosinghrs
				WHERE posclosinghr_id = $id
			");
				
			return ($sth) ? true : false;
		}
	}