<?php 

	class Product_Line_Subclass_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			$sth = $this->db->query("
				SELECT *
				FROM productline_subclasses
				WHERE productline_subclass_id = $id
			");
			return ($sth) ? $sth->fetch() : false;
		}

		public function create($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);

			$sth = $this->db->prepare("
				INSERT INTO productline_subclasses ( $bindFields )
				VALUES ( $bindValues )
			");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();

			return $this->db->lastInsertId();
		}

		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE productline_subclasses SET $bindFields
				WHERE productline_subclass_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		public function delete($id) {

			$sth = $this->db->query("
				DELETE FROM productline_subclasses
				WHERE productline_subclass_id = $id
			");

			return ($sth) ? true : false;
		}
						
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
			
			$sth = $this->db->query("
				SELECT productline_subclass_id, productline_subclass_name
				FROM productline_subclasses
				$filter
				ORDER BY productline_subclass_productline
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
	}