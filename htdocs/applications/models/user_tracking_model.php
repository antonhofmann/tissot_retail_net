<?php

	/**
	 * User Tracking
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class User_Tracking_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {
				
			$sth = $this->db->query("
				SELECT *,
					DATE_FORMAT(date_created,'%d.%m.%Y') AS date
				FROM user_trackings
				WHERE user_tracking_id = $id
			");

			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_from_entity($entity) {
				
			$sth = $this->db->query("
				SELECT *,
					DATE_FORMAT(date_created,'%d.%m.%Y') AS date
				FROM user_trackings
				WHERE user_tracking_entity_id = $entity
				ORDER BY date_created DESC
				LIMIT 0,1
			");

			return ($sth) ? $sth->fetch() : null;
		}

		public function create($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);

			$sth = $this->db->prepare("
				INSERT INTO user_trackings ( $bindFields )
				VALUES ( $bindValues )
			");
			
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();

			return $this->db->lastInsertId();
		}

		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE user_trackings SET $bindFields
				WHERE user_tracking_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		public function delete($id) {

			$sth = $this->db->query("
				DELETE FROM user_trackings
				WHERE user_tracking_id = $id
			");

			return ($sth) ? true : false;
		}
	}
