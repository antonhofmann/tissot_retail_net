<?php 

	class Pos_Verification_Model extends Model {
		
		public function __construct($connector=null) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM posverifications
				WHERE posverification_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_periode($company, $date) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM posverifications
				WHERE posverification_address_id = $company AND posverification_date = '$date'
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO posverifications ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE posverifications SET $bindFields
				WHERE posverification_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM posverifications
				WHERE posverification_id = $id
			");
			
			return ($sth) ? true : false;
		}
	}
	