<?php 

	class Application_Model extends Model {
	
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
				
			$sth = $this->db->query("
				SELECT *
				FROM applications
				WHERE application_id = $id
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_from_shortcut($application) {
		
			$sth = $this->db->query("
				SELECT *
				FROM applications
				WHERE application_shortcut = '$application'
			");
		
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO db_retailnet.applications ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();

			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE applications 
				SET $bindFields
				WHERE application_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM	applications
				WHERE application_id = $id
			");
				
			return ($sth) ? true : false;
		}
		
		public function file_extensions($id) {
		
			$sth = $this->db->query("
				SELECT file_type_id, file_type_extension
				FROM file_types
				INNER JOIN application_filetypes ON application_filetype_filetype = file_type_id
				WHERE application_filetype_application = $id
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	
		public function load($filters=null) {
				
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT *
				FROM applications
				$filter
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function loader($filters=null) {
				
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT application_id, application_name
				FROM applications
				$filter
				ORDER BY application_name
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function dropdown($filters=null) {
				
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT application_shortcut, application_name
				FROM applications
				$filter
				ORDER BY application_name
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	
		public function involved_in_planning($application) {
		
			$sth = $this->db->query("
				SELECT *
				FROM applications
				WHERE application_shortcut = '$application' AND application_involved_in_planning = 1
			");
		
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function get_all_involved_in_planning() {
		
			$sth = $this->db->query("
				SELECT *
				FROM applications
				WHERE application_involved_in_planning = 1
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	}