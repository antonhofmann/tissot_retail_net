<?php 

	class Staff_Type_Model extends Model {
	
		/**
		 * Staff Type Model
		 * @author aserifi
		 * @copyright mediaparx ag
		 */
		public function __construct($connector) {
			parent::__construct($connector);
		}
		
		public function read($id) {
			$sth = $this->db->query("
				SELECT *
				FROM mps_staff_types
				WHERE mps_staff_type_id = $id
			");	
			return ($sth) ? $sth->fetch() : false;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mps_staff_types ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE mps_staff_types SET $bindFields
				WHERE mps_staff_type_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_staff_types
				WHERE mps_staff_type_id = $id
			");	
			
			return ($sth) ? true : false;
		}
		
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
				
			$sth = $this->db->query("
				SELECT mps_staff_type_id, mps_staff_type_name 
				FROM mps_staff_types
				$filter
				ORDER BY mps_staff_type_name
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	}
	