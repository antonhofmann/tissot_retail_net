<?php 

	class Company_Language_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($company, $language) {
				
			$sth = $this->db->query("
				SELECT *
				FROM address_languages
				WHERE address_language_address = $company AND address_language_language = $language
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_all_languages($company) {
				
			$sth = $this->db->query("
				SELECT address_language_language, address_language_address
				FROM address_languages
				WHERE address_language_address = $company
			");
				
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function create($company, $language) {
			
			$user = User::instance();
		
			$this->db->query("
				INSERT INTO address_languages ( 
					address_language_address, 
					address_language_language, 
					user_created, 
					date_created 
				)
				VALUES ( 
					$company, $language, '".$user->login."', NOW() 
				)
			");
			
			return $this->db->lastInsertId();
		}
		
		public function delete($company, $language) {
			
			$sth = $this->db->query("
				DELETE FROM address_languages
				WHERE address_language_address = $company AND address_language_language = $language
			");
			
			return ($sth) ? true : false;
		}
	}