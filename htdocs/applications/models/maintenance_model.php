<?php 

	/**
	 * Maintenance Model
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Maintenance_Model extends Model {
	
		public function __construct($connector=null) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
				
			$sth = $this->db->query("
				SELECT *
				FROM maintenance_windows
				WHERE maintenance_window_id = $id
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO maintenance_windows ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE maintenance_windows 
				SET $bindFields
				WHERE maintenance_window_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM	maintenance_windows
				WHERE maintenance_window_id = $id
			");
				
			return ($sth) ? true : false;
		}
		
		public function warnings() {
			
			$sth = $this->db->query("
				SELECT maintenance_window_warning_content AS content
				FROM maintenance_windows
				WHERE maintenance_window_active = 1 AND NOW() >= maintenance_window_warning_start_date AND NOW() <= maintenance_window_stop	
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function getActives() {
			
			$sth = $this->db->query("
				SELECT *,
					UNIX_TIMESTAMP(maintenance_window_start) AS start_timestamp,
					UNIX_TIMESTAMP(maintenance_window_stop) AS stop_timestamp
				FROM maintenance_windows
				WHERE maintenance_window_active = 1 AND NOW() >= maintenance_window_start AND NOW() <= maintenance_window_stop 	
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
	}
	