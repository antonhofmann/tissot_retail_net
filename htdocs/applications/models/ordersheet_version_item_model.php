<?php 

	/**
	 * Order Sheet Version Item Model
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Ordersheet_Version_Item_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) { 
			
			$sth = $this->db->query("
				SELECT *
				FROM mps_ordersheet_version_items
				WHERE mps_ordersheet_version_item_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mps_ordersheet_version_items ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE mps_ordersheet_version_items 
				SET $bindFields
				WHERE mps_ordersheet_version_item_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_version_items 
				WHERE mps_ordersheet_version_item_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function deleteAll($version) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_version_items 
				WHERE mps_ordersheet_version_item_ordersheetversion_id = $version
			");
			
			return ($sth) ? true : false;
		}
	}