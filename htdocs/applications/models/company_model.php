<?php

	class Company_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {

			$sth = $this->db->query("
				SELECT * 
				FROM addresses 
				WHERE address_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}

		public function create($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO addresses ( $bindFields )
				VALUES ( $bindValues )
			");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();
			
			return $this->db->lastInsertId();
		}

		public function update($id, $data) {
			
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE addresses 
				SET $bindFields
				WHERE address_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM addresses
				WHERE address_id = $id
			");
			
			return ($sth) ? true : false;
		}

		public function get_pos($id) {

			$sth = $this->db->query("
				SELECT posaddress_id
				FROM posaddresses
				WHERE posaddress_identical_to_address = 1 AND posaddress_franchisee_id = $id
			");

			return ($sth) ? $sth->fetch() : null;
		}
		
		public function get_all_pos($id, $archived) {
			
			if ($archived) {
				$filter = "(
					posaddress_store_closingdate <> '0000-00-00'
					OR posaddress_store_closingdate <> NULL
				)";
			}
			else {
				$filter = "(
					posaddress_store_closingdate = '0000-00-00'
					OR posaddress_store_closingdate IS NULL
					OR posaddress_store_closingdate = ''
				)";
			}

			$sth = $this->db->query("
				SELECT *
				FROM posaddresses
				WHERE posaddress_franchisee_id = $id AND $filter
			");

			return ($sth) ? $sth->fetchAll() : null;
		}

		public function get_users($id) {
			
			$sth = $this->db->query("
				SELECT *
				FROM users
				WHERE user_address = $id
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
			
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT 
					address_id,
					CONCAT(country_name, ': ', address_company) AS client
				FROM addresses
				INNER JOIN countries ON country_id = address_country
				$filter
				ORDER BY country_name, address_company
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	}