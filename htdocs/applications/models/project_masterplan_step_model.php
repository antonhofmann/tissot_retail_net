<?php 
/**
 * Project masterplan step model
 * 
 * Represents one step of a phase of a project masterplan
 */
class Project_Masterplan_Step_Model extends Model {
  /**
   * Read a step from db
   * @param  integer $id
   * @return Masterplan_Step
   */
  public function read($id) {
    $sth = $this->db->prepare("
      SELECT
        project_masterplan_step_id AS id,
        project_masterplan_phase_id AS phase_id,
        step_nr,
        step_type,
        pos,
        title,
        color,
        value,
        responsible_role_id,
        apply_corporate,
        apply_franchisee,
        apply_other,
        apply_store,
        apply_sis,
        apply_kiosk,
        apply_independent,
        is_active
      FROM 
        project_masterplan_steps
      WHERE 
        project_masterplan_step_id = :id
      ");

    $sth->bindParam('id', $id, PDO::PARAM_INT);
    $sth->execute();
    $sth->setFetchMode(PDO::FETCH_CLASS, 'Masterplan_Step');
    return $sth->fetch();
  }

  /**
   * Create a step
   * @param  array $data
   * @return integer Step ID
   */
  public function create($data) {
    if (array_key_exists('duration_defaults', $data) || array_key_exists('duration_exceptions', $data)) {
      $defaults = isset($data['duration_defaults']) 
        ? (array) $data['duration_defaults']
        : array();
      $exceptions = isset($data['duration_exceptions']) 
        ? (array) $data['duration_exceptions']
        : array();
      unset($data['duration_defaults'], $data['duration_exceptions']);
    }

    $bindFields = array();
    foreach ($data as $key => $value) {
      $bindFields[] = "$key = :$key";
    }
    if (!count($bindFields)) {
      return;
    }

    $sth = $this->db->prepare(sprintf("
      INSERT INTO project_masterplan_steps
      SET %s",
      join(', ', $bindFields)
    ));
  
    foreach ($data as $key => $value) {
      $sth->bindValue(":$key", $value);
    }
  
    $sth->execute();
    $stepID = $this->db->lastInsertId();

    // save durations    
    if (isset($defaults)) {
      $this->saveDurationDefaults($id, $defaults);
    }
    if (isset($exceptions)) {
      $this->saveDurationExceptions($id, $exceptions);
    }
    
    return $stepID;
  }
  
  /**
   * Update a step
   * @param  integer $id
   * @param  array $data
   * @return integer Number of rows updated
   */
  public function update($id, $data) {
    if (array_key_exists('duration_defaults', $data) || array_key_exists('duration_exceptions', $data)) {
      $this->deleteDurations($id);
      $defaults = isset($data['duration_defaults']) 
        ? (array) $data['duration_defaults']
        : array();
      $this->saveDurationDefaults($id, $defaults);
      $exceptions = isset($data['duration_exceptions']) 
        ? (array) $data['duration_exceptions']
        : array();
      unset($data['duration_defaults'], $data['duration_exceptions']);
      $this->saveDurationExceptions($id, $exceptions);
    }

    $bindFields = array();
    foreach ($data as $key => $value) {
      $bindFields[] = "$key = :$key";
    }
    $sth = $this->db->prepare(sprintf("
      UPDATE project_masterplan_steps
      SET %s
      WHERE project_masterplan_step_id = :id
      ",
      join(',', $bindFields)
    ));
  
    $sth->bindValue(':id', $id);
  
    foreach ($data as $key => $value) {
      $sth->bindValue(":$key", $value);
    }
  
    $sth->execute();
    return $sth->rowCount();
  }

  /**
   * Save default step durations
   * @param  integer $stepID
   * @param  array $defaults
   * @return integer Number of rows affected
   */
  protected function saveDurationDefaults($stepID, array $defaults) {
    // assemble salesregion values
    $defaultValues = array();
    foreach ($defaults as $salesRegionID => $duration) {
      if (!(intval($duration) > 0) || !(intval($salesRegionID) > 0)) {
        continue;
      }
      $defaultValues[] = sprintf('(%d, %d, %d)',
        $stepID,
        $salesRegionID,
        $duration
      );
    }

    // save default values
    $affectedRows = 0;
    if (count($defaultValues) > 0) {
      $sth = $this->db->prepare(sprintf("
        INSERT INTO project_masterplan_step_durations
          (project_masterplan_step_id, salesregion_id, duration)
        VALUES
          %s",
        join(',', $defaultValues)
      ));
      $sth->execute();
      $affectedRows = $sth->rowCount();
    }

    return $affectedRows;
  }

  /**
   * Save step duration exceptions
   * @param  integer $stepID
   * @param  array $exceptions
   * @return integer Number of rows affected
   */
  protected function saveDurationExceptions($stepID, array $exceptions) {
    // assemble country values
    $exceptionValues = array();
    foreach ($exceptions as $exception) {
      if (!(intval($exception->country_id) > 0) || !(intval($exception->duration) > 0) || !(intval($exception->salesregion_id) > 0)) {
        continue;
      }
      $exceptionValues[] = sprintf('(%d, %d, %d, %d)',
        $stepID,
        $exception->salesregion_id,
        $exception->country_id,
        $exception->duration
      );
    }  
    
    // save exceptions
    $affectedRows = 0;
    if (count($exceptionValues) > 0) {
      $sth = $this->db->prepare(sprintf("
        INSERT INTO project_masterplan_step_durations
          (project_masterplan_step_id, salesregion_id, country_id, duration)
        VALUES
          %s",
        join(',', $exceptionValues)
      ));
      $sth->execute();
      $affectedRows += $sth->rowCount();
    }
    return $affectedRows;
  }

  /**
   * Remove all durations for a step
   * It makes sense to call this before saving durations
   * @param  integer $stepID
   * @return void
   */
  protected function deleteDurations($stepID) {
    $sth = $this->db->prepare("
      DELETE FROM project_masterplan_step_durations
      WHERE project_masterplan_step_id = :id"
    );
    $sth->bindValue(':id', $stepID);
    return $sth->execute();
  }

  /**
   * Delete a step
   * @param  integer $id
   * @return void
   */
  public function delete($id) {
    $sth = $this->db->prepare("
      DELETE FROM
        project_masterplan_steps
      WHERE
        project_masterplan_step_id = :id
    ");
    $sth->bindValue(':id', $id);
    $sth->execute();
  }
}
