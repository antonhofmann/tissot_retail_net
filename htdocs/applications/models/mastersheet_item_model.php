<?php
	
	class Mastersheet_Item_Model extends Model {
		
		/**
		 * Master Sheet Item Model
		 * @author aserifi
		 * @copyright mediaparx ag
		 */
		public function __construct($connector) {
			parent::__construct($connector);
		}
		
		public function read($mastersheet, $item) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_mastersheet_items
				WHERE mps_mastersheet_item_mastersheet_id = $mastersheet AND mps_mastersheet_item_material_id = $item
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($mastersheet, $item) {
			
			$user = user::instance();
			
			$sth = $this->db->prepare("
				INSERT INTO mps_mastersheet_items (
					mps_mastersheet_item_mastersheet_id,
					mps_mastersheet_item_material_id,
					user_created,
					date_created
				) VALUES (
					:mastersheet,
					:item,
					:user,
					:date
				)
			");
			
			$sth->execute(array(
				'mastersheet' => $mastersheet,
				'item' => $item,
				'user' => $user->login,
				'date' => date('Y-m-d')
			));
				
			return $this->db->lastInsertId();
		}
		
		public function delete($mastersheet, $item) {
			
			$sth = $this->db->query("
				DELETE FROM mps_mastersheet_items
				WHERE mps_mastersheet_item_mastersheet_id = $mastersheet AND mps_mastersheet_item_material_id = $item
			");
				
			return ($sth) ? true : false;
		}
		
		public function deleteAll($mastersheet) {
			
			$sth = $this->db->query("
				DELETE FROM mps_mastersheet_items
				WHERE mps_mastersheet_item_mastersheet_id = $mastersheet
			");
				
			return ($sth) ? true : false;
		}
		
		public function load($mastersheet, $filters=null) {
			
			$filter = (is_array($filters)) ? ' AND '.join(' AND ', $filters) : null;
			
			$sth = $this->db->query("
				SELECT *
				FROM mps_mastersheet_items
				WHERE mps_mastersheet_item_mastersheet_id = $mastersheet $filter
			");
				
			return ($sth) ? $sth->fetchAll() : null;
		}
	}