<?php

	class Certificate_Material_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($certificate, $material) {
			
			$sth = $this->db->prepare("
				SELECT *
				FROM certificate_materials
				WHERE certificate_material_certificate_id = :certificate AND certificate_material_catalog_material_id = :material
			");

			$sth->execute(array(
				'certificate' => $certificate,
				'material' => $material
			));
			
			return $sth->fetch();
		}

		public function create($certificate, $material) {

			$sth = $this->db->prepare("
				INSERT INTO certificate_materials ( 
					certificate_material_certificate_id,
					certificate_material_catalog_material_id,
					user_created,
					date_created
				)
				VALUES (
					:certificate,
					:material,
					:user,
					NOW()
				)
			");

			$sth->execute(array(
				'certificate' => $certificate,
				'material' => $material,
				'user' => User::instance()->login
			));

			return $this->db->lastInsertId();
		}

		public function delete($certificate, $material) {

			$sth = $this->db->prepare("
				DELETE FROM certificate_materials
				WHERE certificate_material_certificate_id = :certificate AND certificate_material_catalog_material_id = :material
			");

			return $sth->execute(array(
				'certificate' => $certificate,
				'material' => $material
			));
		}

		public function deleteAll($certificate) {

			$sth = $this->db->prepare("
				DELETE FROM certificate_materials
				WHERE certificate_material_certificate_id = :certificate
			");

			return $sth->execute(array(
				'certificate' => $certificate
			));
		}
	}