<?php 

	class Store_Locator_Model extends Model {
		
		public function __construct($connector) {
			parent::__construct($connector);
		}
		
		public function get_place($id) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM places 
				WHERE place_id = $id 
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function add_place($data) {
			
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
			
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO places ( $bindFields )
				VALUES ( $bindValues )
			");
			
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
			
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function get_province($id) {

			$sth = $this->db->query("
				SELECT * 
				FROM provinces 
				WHERE province_id = $id 
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function add_province($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
				
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO provinces ( $bindFields )
				VALUES ( $bindValues )
			");
				
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
						
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function add_pos_type($data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
			
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO postypes ( $bindFields )
				VALUES ( $bindValues )
			");
			
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
			
			$sth->execute();
			return $this->db->lastInsertId();
		}
		
		public function get_pos($id) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM posaddresses 
				WHERE posaddress_id = $id 
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function add_pos($data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
				
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO posaddresses ( $bindFields )
				VALUES ( $bindValues )
			");
				
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
						
			$sth->execute();

			return $this->db->lastInsertId();
		}
		
		public function update_pos($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
			
			$bindFields = join(' , ', $bindFields);
			
			$sth = $this->db->prepare("
				UPDATE posaddresses SET $bindFields
				WHERE posaddress_id = :id
			");
			
			$sth->bindValue(":id", $id);
			
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
			
			return $sth->execute();
		}
		
		public function delete_pos($id) {
			
			$sth = $this->db->exec("
				DELETE FROM posaddresses 
				WHERE posaddress_id = $id 
			");

			if ($sth) {

				$this->db->exec("
					DELETE FROM loc_posaddresses 
					WHERE loc_posaddress_posaddress_id = $id 
				");

				$this->db->exec("
					DELETE FROM posopeninghrs
					WHERE posopeninghr_posaddress_id = $id 
				");

				$this->db->exec("
					DELETE FROM posclosinghrs  
					WHERE posclosinghr_posaddress_id = $id 
				");

				$this->db->exec("
					DELETE FROM posaddress_customerservices 
					WHERE posaddress_customerservice_posaddress_id = $id 
				");

				$this->db->exec("
					DELETE FROM poaddress_popups  
					WHERE poaddress_popup_posaddress_id = $id 
				");

				return true;
			}
		}
	}
