<?php

	class User_Role_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($user, $role) {
			
			$sth = $this->db->query("
				SELECT *
				FROM user_roles 
				WHERE user_role_user = $user AND user_role_role = $role
			");
			
			return ($sth) ? $sth->fetch() : null;
		}

		public function create($user, $role) {
			
			$User = User::instance();
		
			$this->db->query("
				INSERT INTO user_roles (
					user_role_user, 
					user_role_role, 
					user_created, 
					date_created 
				)
				VALUES ( $user, $role, '".$User->login."', NOW() )
			");
			
			return $this->db->lastInsertId();
		}

		public function delete($user, $role) {
			
			$sth = $this->db->query("
				DELETE FROM user_roles
				WHERE user_role_user = $user AND user_role_role = $role
			");
			
			return ($sth) ? true : false;
		}
		
		public function delete_all($user) {
				
			$sth = $this->db->query("
				DELETE FROM user_roles
				WHERE user_role_user = $user
			");
				
			return ($sth) ? true : false;
		}
		
		public function roles($user) {
				
			$sth = $this->db->query("
				SELECT user_role_role
				FROM user_roles 
				WHERE user_role_user = $user
			");
				
			return ($sth) ? $sth->fetchAll() : null;
		}
	}