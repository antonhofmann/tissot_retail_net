<?php

	class Design_Objective_Group_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {
			$sth = $this->db->query("
				SELECT *
				FROM design_objective_groups
				WHERE design_objective_group_id = $id
			");
			return ($sth) ? $sth->fetch() : false;
		}

		public function create($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);

			$sth = $this->db->prepare("
				INSERT INTO design_objective_groups ( $bindFields )
				VALUES ( $bindValues )
			");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();

			return $this->db->lastInsertId();
		}

		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE design_objective_groups SET $bindFields
				WHERE design_objective_group_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		public function delete($id) {

			$sth = $this->db->query("
				DELETE FROM design_objective_groups
				WHERE design_objective_group_id = $id
			");

			return ($sth) ? true : false;
		}
	}