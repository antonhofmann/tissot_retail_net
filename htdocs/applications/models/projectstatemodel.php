<?php 

	class ProjectStateModel extends Model {
	
		public function __construct($connector=null) { 
			
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->prepare("
				SELECT *
				FROM project_states
				WHERE project_state_id = :id		
			");
			
			$sth->bindParam('id', $id, PDO::PARAM_INT);
			
			$sth->execute();
			
			return $sth->fetch();
		}
		
		public function create($data) {
			
			$statement = Database::insertStatement('project_states', $data);
			
			$sth = $this->db->prepare($statement);
			
			$sth->execute($data);
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
			
			$statement = Database::updateStatement('project_states', 'project_state_id', $data);
			
			$sth = $this->db->prepare($statement);
			
			$data['project_state_id'] = $id;
			
			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->prepare("
				DELETE FROM project_states
				WHERE project_state_id = :id		
			");
			
			$sth->bindParam('id', $id, PDO::PARAM_INT);
			
			return$sth->execute();
		}
	}