<?php 

	class MIS_Query_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->prepare("
				SELECT * 
				FROM mis_queries 
				WHERE mis_query_id = :id
			");
			$sth->bindValue(':id', $id);
			$sth->execute();
			
			return ($sth) ? $sth->fetch() : null;
		}
	}
	