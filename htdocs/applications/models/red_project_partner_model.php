<?php


	class Red_Project_Partner_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {

			$sth = $this->db->query("
				SELECT *,
					DATE_FORMAT(red_project_partner_access_from, '%d.%m.%Y') as red_project_partner_access_from,
					DATE_FORMAT(red_project_partner_access_until, '%d.%m.%Y') as red_project_partner_access_until
				FROM red_project_partners
				WHERE red_project_partner_id = $id
			");

			return ($sth) ? $sth->fetch() : null;
		}

		public function create($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO red_project_partners ( $bindFields )
				VALUES ( $bindValues )
			");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();

			return $this->db->lastInsertId();
		}

		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE red_project_partners
				SET $bindFields
				WHERE red_project_partner_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		public function delete($id) {

			$sth = $this->db->query("
				DELETE FROM red_project_partners
				WHERE red_project_partner_id = $id
			");

			return ($sth) ? true : false;
		}

	}