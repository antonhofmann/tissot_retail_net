<?php 

	class Ordersheet_Item_Planned_Quantity_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_planned_quantities
				WHERE mps_ordersheet_item_planned_quantity_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function load($item, $filters=null) { 
			
			$filter = (is_array($filters)) ? ' AND '.join(' AND ', $filters) : null;
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_planned_quantities 
				WHERE AND mps_ordersheet_item_planned_quantity_ordersheet_item_id = $item $filter
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function add_pos($item, $pos, $quantity) { 
			
			$user = user::instance();
			$user_created = $user->login;
			
			$this->db->query("
				INSERT INTO mps_ordersheet_item_planned_quantities (
					mps_ordersheet_item_planned_quantity_ordersheet_item_id,
					mps_ordersheet_item_planned_quantity_posaddress_id,
					mps_ordersheet_item_planned_quantity_quantity,
					user_created
				)
				VALUES ( $item, $pos, $quantity, '$user_created' )
			");
			
			return $this->db->lastInsertId();
		}
		
		public function add_warehouse($item, $warehouse, $quantity) { 
			
			$user = user::instance();
			$user_created = $user->login;
			
			$this->db->query("
				INSERT INTO mps_ordersheet_item_planned_quantities (
					mps_ordersheet_item_planned_quantity_ordersheet_item_id,
					mps_ordersheet_item_planned_quantity_warehouse_id,
					mps_ordersheet_item_planned_quantity_quantity,
					user_created
				)
				VALUES ( $item, $warehouse, $quantity, '$user_created' )
			");
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $quantity) {
			
			$user = user::instance();
			$user_modified = $user->login;
		
			return $this->db->query("
				UPDATE mps_ordersheet_item_planned_quantities SET
					mps_ordersheet_item_planned_quantity_quantity = $quantity,
					user_modified = '$user_modified',
					date_modified = NOW()
				WHERE mps_ordersheet_item_planned_quantity_id = $id
			");
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_item_planned_quantities 
				WHERE mps_ordersheet_item_planned_quantity_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function delete_all($item) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_item_planned_quantities 
				WHERE mps_ordersheet_item_planned_quantity_ordersheet_item_id = $item
			");
			
			return ($sth) ? true : false;
		}
	}
	