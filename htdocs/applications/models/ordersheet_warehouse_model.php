<?php 

	class Ordersheet_Warehouse_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_warehouses
				WHERE mps_ordersheet_warehouse_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_from_warehouse($ordersheet, $warehouse) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_warehouses
				WHERE mps_ordersheet_warehouse_ordersheet_id = $ordersheet 
				AND mps_ordersheet_warehouse_address_warehouse_id = $warehouse
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function load($ordersheet, $filters=null) { 
			
			$filter = (is_array($filters)) ? ' AND '.join(' AND ', $filters) : null;
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_warehouses 
				WHERE mps_ordersheet_warehouse_ordersheet_id = $ordersheet $filter
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function create($ordersheet, $warehouse) { 
				
			$user = user::instance();
			$user_created = $user->login;
			
			$this->db->query("
				INSERT INTO mps_ordersheet_warehouses (
					mps_ordersheet_warehouse_ordersheet_id,
					mps_ordersheet_warehouse_address_warehouse_id,
					user_created
				)
				VALUES ( $ordersheet, $warehouse, '$user_created' )
			");
			
			return $this->db->lastInsertId();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_warehouses 
				WHERE mps_ordersheet_warehouse_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function delete_from_warehouse($ordersheet, $warehouse) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_warehouses 
				WHERE mps_ordersheet_warehouse_ordersheet_id = $ordersheet 
				AND mps_ordersheet_warehouse_address_warehouse_id = $warehouse
			");
			
			return ($sth) ? true : false;
		}
		
		public function delete_all($ordersheet) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_warehouses
				WHERE mps_ordersheet_warehouse_ordersheet_id = $ordersheet
			");
			
			return ($sth) ? true : false;
		}
		
		public function getStockReserve($ordersheet) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_warehouses 
				WHERE mps_ordersheet_warehouse_ordersheet_id = $ordersheet
				AND mps_ordersheet_warehouse_stock_reserve = 1
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function addStockReserve($ordersheet, $warehouse) {
			
			$user = user::instance();
			$user_created = $user->login;
			
			$sth = $this->db->query("
				INSERT INTO mps_ordersheet_warehouses (
					mps_ordersheet_warehouse_ordersheet_id,
					mps_ordersheet_warehouse_address_warehouse_id,
					mps_ordersheet_warehouse_stock_reserve,
					user_created	
				)
				VALUES ($ordersheet, $warehouse, 1, '$user_created')
			");
			
			return $this->db->lastInsertId();
		}
	}
	