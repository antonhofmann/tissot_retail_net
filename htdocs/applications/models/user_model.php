<?php

	class User_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM users 
				WHERE user_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}

		/**
		 * Read user by user_address
		 * @param  integer $addressID
		 * @return array
		 */
		public function readByAddress($addressID) {
			$sth = $this->db->prepare("
				SELECT 
					*
				FROM 
					users
				WHERE 
						user_address = :address_id
					AND
						user_active = 1
				");
			$sth->bindValue(':address_id', $addressID);
			$sth->execute();
			return $sth->fetch();
		}

		public function getPreferences($id) {
			
			$sth = $this->db->query("
				SELECT 
					user_preference_entity AS entity,
					user_preference_name AS name,
					user_preference_value AS value
				FROM user_preferences
				WHERE user_preference_user_id = $id
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}

		public function addPreference($user, $entity, $name, $value) {
			
			return  $this->db->exec("
				INSERT INTO user_preferences (
					user_preference_user_id,
					user_preference_entity,
					user_preference_name,
					user_preference_value
				)
				VALUES (
					$user, '$entity', '$name', '$value'
				)
			");
		}

		public function updatePreference($user, $entity, $name, $value) { 
			
			return  $this->db->exec("
				UPDATE user_preferences SET
					user_preference_value = '$value',
					date_modified = NOW()
				WHERE
					user_preference_user_id = $user
				AND user_preference_entity = '$entity'
				AND user_preference_name = '$name'
			");
		}
		
		public function create($data) {
			
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO users ( $bindFields )
				VALUES ( $bindValues )
			");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);
			
			$sth = $this->db->prepare("
				UPDATE users SET $bindFields
				WHERE user_id = :user
			");

			$sth->bindValue(":user", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM users 
				WHERE user_id = $id
			");

			return ($sth) ? true : false;
		}

		public function check($id) {
			
			$sth = $this->db->query("
				SELECT user_id 
				FROM users 
				WHERE user_active = 1 AND user_id = $id
			");
			
			return ($sth) ? true : false;
		}

		public function roles($id) {
			
			$sth = $this->db->query("
				SELECT user_role_role AS role 
				FROM user_roles 
				WHERE user_role_user = $id
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}

		public function resetPassword($id, $md5) {

			$sth = $this->db->query("
				UPDATE users 
				SET user_password_reset_id = '$md5'
				WHERE user_id = $id
			");

			return ($sth) ? true : false;
		}

		public function isActive($id) {
			
			$sth = $this->db->query("
				SELECT user_id 
				FROM users 
				WHERE user_active = 1 
				AND DATE_SUB(CURDATE(),INTERVAL 60 DAY) <= user_password_reset 
				AND user_id = ".$id
			);
			
			$result = ($sth) ? $sth->fetch() : null;
			
			return ($result['user_id']) ? true : false;
		}

		public function getUsedPasswords($id) {
			
			$sth = $this->db->query("
				SELECT user_used_passwords 
				FROM users 
				WHERE user_id = ".$id);
			
			return ($sth) ? $sth->fetch() : null;
		}

		public function updateUsedPasswords($id, $passwords) {

			$sth = $this->db->prepare("
				UPDATE users 
				SET user_used_passwords = :passwords
				WHERE user_id = :user
			");

			return $sth->execute(array(
				':user' => $id,
				':passwords' => serialize($passwords)
			));
		}

		public function canUnlockIP($id) {

			$sth = $this->db->query("
				SELECT permission_name
				FROM user_roles
				LEFT JOIN roles ON user_role_role = role_id
				LEFT JOIN role_permissions ON role_id = role_permission_role
				LEFT JOIN permissions ON role_permission_permission = permission_id
				WHERE user_role_user = $id AND permission_name = 'can_unlock_ips'
			");

			return ($sth) ? true : false;
		}

		public function identification($data) {

			if (is_array($data)) {

				foreach ($data as $key => $value) {
					$bindFields[] = "$key = :$key";
				}

				$bindFields = join(' AND ', $bindFields);
				
				$sth = $this->db->prepare("
					SELECT * 
					FROM users 
					WHERE $bindFields 
					order by user_id DESC
				");

				foreach ($data as $key => $value) {
					$sth->bindValue(":$key", $value);
				}

				$sth->execute();
				
				return $sth->fetch();
			}
		}
	
		public function application($id, $application) {
			
			$sth = $this->db->query("
				SELECT application_id
				FROM applications 
				INNER JOIN role_applications ON application_id = role_application_application
				INNER JOIN user_roles ON role_application_role = user_role_role
				WHERE application_shortcut = '$application' AND user_role_user = $id		
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
	}