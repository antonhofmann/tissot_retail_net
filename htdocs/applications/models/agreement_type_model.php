<?php

	class Agreement_Type_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {
			
			$sth = $this->db->query("
				SELECT *
				FROM agreement_types
				WHERE agreement_type_id= $id
			");
			
			return ($sth) ? $sth->fetch() : false;
		}

		public function create($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);

			$sth = $this->db->prepare("
				INSERT INTO agreement_types ( $bindFields )
				VALUES ( $bindValues )
			");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();

			return $this->db->lastInsertId();
		}

		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE agreement_types SET $bindFields
				WHERE agreement_type_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		public function delete($id) {

			return $this->db->exec("
				DELETE FROM agreement_types
				WHERE agreement_type_id = $id
			");
		}

		public function loader($filters=null) {

			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;

			$sth = $this->db->query("
				SELECT agreement_type_id, agreement_type_name
				FROM agreement_types
				$filter
				ORDER BY agreement_type_name
			");

			return ($sth) ? $sth->fetchAll() : null;
		}
	}