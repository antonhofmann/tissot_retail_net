<?php 

	class Translation_Model extends Model {
	
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
				
			$sth = $this->db->query("
				SELECT *
				FROM db_retailnet.translations
				WHERE translation_id = $id
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_from_keyword($keyword, $ctaegory) {
				
			$sth = $this->db->query("
				SELECT *
				FROM db_retailnet.translations
				WHERE translation_keyword = $keyword AND translation_category = $category
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO db_retailnet.translations ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE db_retailnet.translations SET $bindFields
				WHERE translation_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM	db_retailnet.translations
				WHERE translation_id = $id
			");
				
			return ($sth) ? true : false;
		}
		
		public function get_categories($language) {
			
			$sth = $this->db->query("
				SELECT translation_category
				FROM db_retailnet.translations
				WHERE translation_language = $language
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
	}