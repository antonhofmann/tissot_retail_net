<?php

	class Red_File_Category_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {

			$sth = $this->db->query("
				SELECT *
				FROM db_retailnet_red.red_filecategories
				WHERE red_filecategory_id = $id
			");

			return ($sth) ? $sth->fetch() : null;
		}

		public function create($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO db_retailnet_red.red_filecategories ( $bindFields )
				VALUES ( $bindValues )
			");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value); 
			}

			$sth->execute();

			return $this->db->lastInsertId();
		}

		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE db_retailnet_red.red_filecategories
				SET $bindFields
				WHERE red_filecategory_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		public function delete($id) {

			$sth = $this->db->query("
				DELETE FROM db_retailnet_red.red_filecategories
				WHERE red_filecategory_id = $id
			");

			return ($sth) ? true : false;
		}

		public function dropdown_loader($filters=null) {
		
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
				
			$sth = $this->db->query("
				SELECT
					red_filecategory_id,
					red_filecategory_name
				FROM db_retailnet_red.red_filecategories
				$filter
				ORDER BY red_filecategory_name
			");
						
			return ($sth) ? $sth->fetchAll() : null;
		}
	}