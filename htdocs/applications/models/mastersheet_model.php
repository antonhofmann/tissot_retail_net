<?php 

	class Mastersheet_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_mastersheets 
				WHERE mps_mastersheet_id = $id
			");

			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mps_mastersheets ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE mps_mastersheets SET $bindFields
				WHERE mps_mastersheet_id = :id
			");
			
			$data['id'] = $id;

			return $sth->execute($data);
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_mastersheets 
				WHERE mps_mastersheet_id = $id
			");
			
			return ($sth) ? true : false;
		}
	
		public function getCollections($id) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_mastersheet_collections 
				WHERE mps_mastersheet_collection_mastersheet_id = $id
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function dropdownLoader($connector, $filters=null) {
			
			$filter = ($filters) ? 'WHERE '.join(' AND ', $filters) : null;
			
			$sth = $this->db->query("
				SELECT 
					mps_mastersheet_id AS id, 
					mps_mastersheet_name AS name 
				FROM mps_mastersheets
				$filters
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
		

		public function getExportedItems($id) {

			$sth = $this->db->query("
				SELECT DISTINCT mps_ordersheet_items.mps_ordersheet_item_id
				FROM mps_ordersheet_items 
				INNER JOIN mps_materials ON mps_materials.mps_material_id = mps_ordersheet_items.mps_ordersheet_item_material_id
				INNER JOIN mps_ordersheets ON mps_ordersheet_items.mps_ordersheet_item_ordersheet_id = mps_ordersheets.mps_ordersheet_id
				WHERE mps_ordersheet_items.mps_ordersheet_item_purchase_order_number > 0
				AND mps_ordersheet_items.mps_ordersheet_item_quantity_approved > 0
				AND mps_materials.mps_material_locally_provided <> 1
				AND mps_ordersheets.mps_ordersheet_mastersheet_id = $id
			");
			
			return ($sth) ? $sth->fetchAll() : null;

		}
		
	}
	