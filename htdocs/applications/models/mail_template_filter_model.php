<?php 

	class Mail_Template_Filter_Model extends Model {

		
		public function __construct() {
			
			parent::__construct();
		}
		
		public function loadAll($template) {
			
			$sth = $this->db->prepare("
				SELECT * 
				FROM mail_template_filters
				WHERE mail_template_filter_template_id = :template	
			");
			
			$sth->bindParam('template', $template);
			$sth->execute();
			
			return $sth->fetchAll();
		}
		
		public function read($id) {
			
			$sth = $this->db->prepare("
				SELECT * 
				FROM mail_template_filters
				WHERE mail_template_filter_id = :id		
			");
			
			$sth->bindParam('id', $id);
			$sth->execute();
			
			return $sth->fetch();
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mail_template_filters ( $bindFields ) 
				VALUES ( $bindValues )
			");
		
			$sth->execute($data);
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
			
			$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
			
			$bindFields = join(' , ', $bindFields);

			$sth = $this->db->prepare("
				UPDATE mail_template_filters SET $bindFields
				WHERE mail_template_filter_id = :id
			");
		
			$data['id'] = $id;
			
			return $sth->execute($data);
		}
		
		public function delete($id) {
			
			$sth = $this->db->prepare("
				DELETE FROM mail_template_filters
				WHERE mail_template_filter_id = :id		
			");
			
			$sth->bindParam('id', $id);
			return $sth->execute();
		}
	}