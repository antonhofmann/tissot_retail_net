<?php 

	class Pos_Subclass_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
				
			$sth = $this->db->query("
				SELECT * 
				FROM possubclasses
				WHERE possubclass_id = $id;
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
			
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
				
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO possubclasses ( $bindFields )
				VALUES ( $bindValues )
			");
				
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
				
			$sth->execute();
			
			return $this->db->lastInsertId();
		}

		public function update($id, $data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
				
			$bindFields = join(' , ', $bindFields);
			
			$sth = $this->db->prepare("
				UPDATE possubclasses SET $bindFields 
				WHERE possubclass_id = :id
			");
				
			$sth->bindValue(":id", $id);
				
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
				
			return $sth->execute();
		}

		public function delete($id) {
			
			return $this->db->exec("
				DELETE FROM possubclasses
				WHERE possubclass_id = $id
			");
		}
	
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT possubclass_id, possubclass_name
				FROM possubclasses
				$filter
				ORDER BY possubclass_name
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	}
	