<?php 

	class Assistance_Section_Model extends Model {
		
		public function __construct($connector=null) { 
			
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->prepare("
				SELECT * 
				FROM assistance_sections
				WHERE assistance_section_id = :id
			");
			
			$sth->bindParam('id', $id, PDO::PARAM_INT);
			$sth->execute();
			
			return $sth->fetch();
		}
		
		public function create($data) {
			
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
				
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO assistance_sections ( $bindFields )
				VALUES ( $bindValues )
			");
				
			$sth->execute($data);
			
			return $this->db->lastInsertId();
		}

		public function update($id, $data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
				
			$bindFields = join(' , ', $bindFields);
			
			$sth = $this->db->prepare("
				UPDATE assistance_sections SET $bindFields 
				WHERE assistance_section_id = :id
			");
				
			$data['id'] = $id;
				
			return $sth->execute($data);
		}

		public function delete($id) {
			
			$sth = $this->db->prepare("
				DELETE FROM assistance_sections
				WHERE assistance_section_id = :id
			");
				
			$sth->bindParam('id', $id);
			
			return $sth->execute();
		}

		public function deleteAllLinks($id) {
			
			$sth = $this->db->prepare("
				DELETE FROM assistance_section_links
				WHERE assistance_section_link_section_id = :id
			");
			
			$sth->bindParam('id', $id);
				
			return $sth->execute();
		}
	}
	