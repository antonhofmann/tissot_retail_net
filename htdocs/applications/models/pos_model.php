<?php 

	class Pos_Model extends Model {
			
		public function read($id) {
			
			$sth = $this->db->query("
				SELECT *,
					IF(posaddress_store_closingdate = '0000-00-00', '', DATE_FORMAT(posaddress_store_closingdate,'%d.%m.%Y')) AS posaddress_store_closingdate,
					IF(posaddress_store_openingdate = '0000-00-00', '', DATE_FORMAT(posaddress_store_openingdate,'%d.%m.%Y')) AS posaddress_store_openingdate,
					IF(posaddress_fagrstart = '0000-00-00', '', DATE_FORMAT(posaddress_fagrstart,'%d.%m.%Y')) AS posaddress_fagrstart, 
					IF(posaddress_fagrend = '0000-00-00', '', DATE_FORMAT(posaddress_fagrend,'%d.%m.%Y')) AS posaddress_fagrend,
					IF(posaddress_fagcancellation = '0000-00-00', '', DATE_FORMAT(posaddress_fagcancellation,'%d.%m.%Y')) AS posaddress_fagcancellation,
					IF(posaddress_takeover_date = '0000-00-00', '', DATE_FORMAT(posaddress_takeover_date,'%d.%m.%Y')) AS posaddress_takeover_date
				FROM posaddresses 
				LEFT JOIN postypes ON postype_id = posaddress_store_postype
				LEFT JOIN addresses ON address_id = posaddress_client_id
				left join places on place_id = posaddress_place_id
				left join countries on country_id = posaddress_country
				WHERE posaddress_id = $id
			");

			$sth ? $pos_data = $sth->fetch() : null;


			if(!$pos_data) {
				return ($pos_data);
			}

			//get languages
			$languages = array();
			$sth = $this->db->query("select language_iso639_1
		       from country_languages 
			   left join languages on language_id = country_language_language_id 
			   where country_language_country_id = " . $pos_data['posaddress_country']);

			$language_data = $sth->fetchAll();
			foreach($language_data as $key=>$row)
			{
				$languages[] = $row["language_iso639_1"];
			}

			if(count($languages) > 0) {
				$pos_data["languages"] = implode(';', $languages);
			}
			else {
				$pos_data["languages"] = "";
			}

			//get airport area
			$sth = $this->db->query("select posarea_area from posareas 
			             where posarea_area in (4,5) and posarea_posaddress = " . $pos_data['posaddress_id']);

  
			$areas = $sth->fetchAll();
			foreach($areas as $key=>$row)
			{
				$pos_data["posarea_area"] = $row["posarea_area"];
			}
			
			return ($pos_data) ? $pos_data : null;  	
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO posaddresses ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE posaddresses 
				SET $bindFields
				WHERE posaddress_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM posaddresses 
				WHERE posaddress_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function isIndependentRetailer($id) {
			
			$sth = $this->db->query("
				SELECT address_id
				FROM addresses
				WHERE address_type = 7 AND address_is_independent_retailer = 1 AND address_id = $id
			");
			
			return ($sth) ? true : false;
		}

		/**
		 * Track mails regarding a pos
		 * @param  array $data
		 * @return integer  ID of posmail
		 */
		public function posMailTrack($data) {
			$data['date_created'] = date('Y-m-d H:i:s');
			$data['date_modified'] = date('Y-m-d H:i:s');
			$query = Database::insertStatement('posmails', $data);
			
			$sth = $this->db->prepare($query);
			$sth->execute($data);
			
			return $this->db->lastInsertId();
		}

		/**
		 * Was a pos mail already sent?
		 * @param  integer $posAddressID
		 * @param  integer $userID
		 * @param  integer $mailTemplateID
		 * @return boolean
		 */
		public function isPosMailSent($posAddressID, $mailTemplateID) {
			//var_dump($posAddressID, $mailTemplateID);
			$sth = $this->db->prepare("
				SELECT 
					*
				FROM 
					posmails
				WHERE
					posmail_posaddress_id = :pos_address_id
				AND
					posmail_mail_template_id = :template_id
				");
			$sth->bindParam(':pos_address_id', $posAddressID, PDO::PARAM_INT);
			$sth->bindParam(':template_id', $mailTemplateID, PDO::PARAM_INT);
			$sth->execute();
			return $sth->rowCount() > 0;
		}
	}
	