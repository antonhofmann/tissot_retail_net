<?php 

	/**
	 * Order Sheet Item Model
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Ordersheet_Item_Model extends Model {
		
		public function __construct($connector) {
			parent::__construct($connector);
		}
		
		public function read($id) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_items 
				WHERE mps_ordersheet_item_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_from_material($ordersheet, $material) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_items 
				WHERE mps_ordersheet_item_ordersheet_id = $ordersheet AND mps_ordersheet_item_material_id = $material
			");

			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_from_purchase_orders($ponumber, $material) { 
			
			$sth = $this->db->query("
				SELECT *
				FROM mps_ordersheet_items
				WHERE mps_ordersheet_item_purchase_order_number = $ponumber AND mps_ordersheet_item_material_id = $material			
			");
			
			return ($sth) ? $sth->fetch() : null; 
		}
		
		public function load($ordersheet, $filters=null) { 
			
			$filter = '';
			
			if ($filters) {
				$filter = (is_array($filters)) ? ' AND '.join(' AND ', $filters) : " AND $filters";
			}
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_items 
				WHERE mps_ordersheet_item_ordersheet_id = $ordersheet $filter
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mps_ordersheet_items ( $bindFields )
				VALUES ( $bindValues )
			");
			
			foreach ($data as $key => $value) {
				$sth->bindParam("$key", $value);
			}
		
			$sth->execute($data);
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE mps_ordersheet_items SET $bindFields
				WHERE mps_ordersheet_item_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_items 
				WHERE mps_ordersheet_item_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function deleteAll($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_items 
				WHERE mps_ordersheet_item_ordersheet_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function hasPlannedQuantities($id) {
			
			$sth = $this->db->query("
				SELECT COUNT(mps_ordersheet_item_planned_id) as total
				FROM mps_ordersheet_item_planned
				INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_planned_ordersheet_item_id
				WHERE mps_ordersheet_item_ordersheet_id = $id AND mps_ordersheet_item_planned_posaddress_id > 0
			");
			
			$result = ($sth) ? $sth->fetch() : null; 
			return ($result['total'] > 0) ? true : false;
		}
		
		public function hasDeliveredQuantities($id) {
			
			$sth = $this->db->query("
				SELECT COUNT(mps_ordersheet_item_delivered_id) as total
				FROM mps_ordersheet_item_delivered
				INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id
				WHERE mps_ordersheet_item_ordersheet_id = $id
			");
			
			$result = ($sth) ? $sth->fetch() : null; 
			return ($result['total'] > 0) ? true : false;
		}
		
		public function hasConfirmedQuantities($id) {
			
			$sth = $this->db->query("
				SELECT COUNT(mps_ordersheet_item_confirmed_id) as total
				FROM mps_ordersheet_item_confirmed
				INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_confirmed_ordersheet_item_id
				WHERE mps_ordersheet_item_ordersheet_id = $id
			");
			
			$result = ($sth) ? $sth->fetch() : null; 
			return ($result['total'] > 0) ? true : false;
		}
		
		public function hasShippedQuantities($id) {
			
			$sth = $this->db->query("
				SELECT COUNT(mps_ordersheet_item_shipped_id) as total
				FROM mps_ordersheet_item_shipped
				INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_shipped_ordersheet_item_id
				WHERE mps_ordersheet_item_ordersheet_id = $id
			");
			
			$result = ($sth) ? $sth->fetch() : null; 
			return ($result['total'] > 0) ? true : false;
		}
	}
	