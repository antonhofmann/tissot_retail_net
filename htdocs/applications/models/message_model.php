<?php

	class Message_Model extends Model {
		
		public function __construct($connector) {
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$settings = Settings::init();
				
			$sth = $this->db->query("
				SELECT 
					translation_id AS id,
					translation_keyword_name AS keyword,
					translation_content AS content
				FROM db_retailnet.translations
				INNER JOIN db_retailnet.translation_categories ON translation_category_id = translation_category
				INNER JOIN db_retailnet.translation_keywords ON translation_keyword_id = translation_keyword
				INNER JOIN db_retailnet.languages ON language_id = translation_language
				WHERE language_iso639_1 = '".$settings->language."' AND translation_id = $id
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_from_keyword($keyword, $category) {
			
			$settings = Settings::init();
			
			$sth = $this->db->query("
				SELECT 
					translation_id AS id,
					translation_keyword_name AS keyword,
					translation_content AS content
				FROM db_retailnet.translations
				INNER JOIN db_retailnet.translation_categories ON translation_category_id = translation_category
				INNER JOIN db_retailnet.translation_keywords ON translation_keyword_id = translation_keyword
				INNER JOIN db_retailnet.languages ON language_id = translation_language
				WHERE language_iso639_1 = '".$settings->language."' AND translation_keyword_name = '$keyword' AND translation_category_name = '$category'
			");

			return ($sth) ? $sth->fetch() : null;
		}	
	}