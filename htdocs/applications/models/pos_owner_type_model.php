<?php 

	class Pos_Owner_Type_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM posowner_types
				WHERE posowner_type_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO posowner_types ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE posowner_types SET $bindFields
				WHERE posowner_type_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM posowner_types
				WHERE posowner_type_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function loader($filters=null) {
				
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
			
			$sth = $this->db->query("
				SELECT posowner_type_id, posowner_type_name
				FROM posowner_types 
				$filter
				ORDER BY posowner_type_name
			");
				
			return ($sth) ? $sth->fetchAll() : null;
		}
	}
	