<?php 

	/**
	 * Material Planning Type Model
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Material_Planning_Type_Model extends Model {
	
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
				
			$sth = $this->db->query("
				SELECT *
				FROM mps_material_planning_types
				WHERE mps_material_planning_type_id = $id
			");
				
			return ($sth) ? $sth->fetch() : null;
		}

		public function getFromName($name) {
				
			$name = str_replace(' ', '', strtolower($name));

			$sth = $this->db->query("
				SELECT *
				FROM mps_material_planning_types
				WHERE REPLACE(LOWER(mps_material_planning_type_name), ' ', '') = '$name'
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO mps_material_planning_types ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE mps_material_planning_types 
				SET $bindFields
				WHERE mps_material_planning_type_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM	mps_material_planning_types
				WHERE mps_material_planning_type_id = $id
			");
				
			return ($sth) ? true : false;
		}
		
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT mps_material_planning_type_id, mps_material_planning_type_name
				FROM mps_material_planning_types
				$filter
				ORDER BY mps_material_planning_type_name
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	}