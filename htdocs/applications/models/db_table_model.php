<?php 

	class DB_Table_Model extends Model {
	
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
				
			$sth = $this->db->query("
				SELECT *
				FROM db_tables
				WHERE db_table_id = $id
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_from_name($table, $db) {
				
			$sth = $this->db->query("
				SELECT 
					db_table_id,
					db_table_db,
					db_table_table,
					db_table_primary_key,
					db_table_description,
					db_table_records_can_be_deleted,
					db_table_delete_cascade,
					db_table_parent_table,
					db_table_export_fields,
					db_table_export_fields_order,
					db_table_export_fields_selectable,
					db_tables.user_created,
					db_tables.date_created,
					db_tables.user_modified,
					db_tables.date_modified
				FROM db_tables
				INNER JOIN dbs ON db_id = db_table_db
				WHERE db_table_table = '$table' AND db_name = '$db'
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO db_tables ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE db_tables
				SET $bindFields
				WHERE db_table_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM	db_tables
				WHERE db_table_id = $id
			");
				
			return ($sth) ? true : false;
		}
		
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT db_table_id, db_table_description
				FROM db_tables
				$filter
				ORDER BY db_table_description
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function get_references($id) {
		
			$sth = $this->db->query("
				SELECT *
				FROM db_references
				WHERE db_reference_referenced_table_id = $id
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function delete_references($id) {
				
			$sth = $this->db->query("
				DELETE FROM	db_references
				WHERE db_reference_referenced_table_id = $id
			");
		
			return ($sth) ? true : false;
		}
	}