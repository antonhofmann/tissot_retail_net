<?php

/**
 * Represents a task for a user
 */
class Task_Model extends Model {
	/**
	 * Read task from db
	 * @param  integer $id
	 * @return array
	 */
	public function read($id) {
		$sth = $this->db->prepare("
			SELECT *
			FROM tasks
			WHERE task_id = :id
		");
		$sth->bindValue(':id', $id);
		$sth->execute();
		return $sth->fetch();
	}

	/**
	 * Create a task
	 * @param  array $data
	 * @return integer TaskID
	 */
	public function create($data) {
		$bindFields = array();
		$bindValues = array();

		foreach ($data as $key => $value) {
			$bindFields[] = $key;
			$bindValues[] = ":$key";
		}

		$bindFields = join(' , ', $bindFields);
		$bindValues = join(' , ', $bindValues);

		$sth = $this->db->prepare(sprintf("
			INSERT INTO tasks ( %s )
			VALUES ( %s )",
			$bindFields,
			$bindValues
			));

		foreach ($data as $key => $value) {
			$sth->bindValue(":$key", $value);
		}

		$sth->execute();
		return $this->db->lastInsertId();
	}

	/**
	 * Update a task
	 * @param integer $id
	 * @param array $data
	 * @return bool
	 */
	public function update($id, $data) {
		$bindFields = array();

		foreach ($data as $key => $value) {
			$bindFields[] = "$key = :$key";
		}

		$bindFields = join(',', $bindFields);

		$sth = $this->db->prepare("
			UPDATE 
				tasks
			SET 
				$bindFields
			WHERE 
				task_id = :id
		");

		$sth->bindValue(":id", $id);

		foreach ($data as $key => $value) {
			$sth->bindValue(":$key", $value);
		}

		return $sth->execute();
	}
}