<?php 

	class Supplying_Group_Model extends Model {
		
		public function read($id) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM supplying_groups 
				WHERE supplying_group_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO supplying_groups ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE supplying_groups SET $bindFields
				WHERE supplying_group_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			return $this->db->exec("
				DELETE FROM supplying_groups
				WHERE supplying_group_id = $id
			");
		}
	
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
			$sth = $this->db->query("
				SELECT 
					supplying_group_id, 
					supplying_group_name
				FROM supplying_groups
				$filter
				ORDER BY supplying_group_name
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	}
	