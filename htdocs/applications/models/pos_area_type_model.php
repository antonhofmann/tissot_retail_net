<?php 

	class Pos_Area_Type_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
		
			$sth = $this->db->query("
				SELECT *
				FROM posareatypes
				WHERE posareatype_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$sth = $this->db->prepare("
				INSERT INTO posareatypes ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE posareatypes SET $bindFields
				WHERE posareatype_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->prepare("
				DELETE FROM posareatypes
				WHERE posareatype_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT posareatype_id, posareatype_name
				FROM posareatypes
				$filter
				ORDER BY posareatype_name
			");
				
			return ($sth) ? $sth->fetchAll() : null;
		}
	}
	