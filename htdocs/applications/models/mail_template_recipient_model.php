<?php 

	class Mail_Template_Recipient_Model extends Model {

		
		public function __construct() {
			
			parent::__construct();
		}
		
		public function loadAll($template) {
			
			$sth = $this->db->prepare("
				SELECT * 
				FROM mail_template_recipients
				WHERE mail_template_recipient_template_id = :template	
			");
			
			$sth->bindParam('template', $template);
			$sth->execute();
			
			return $sth->fetchAll();
		}
		
		public function read($template, $user) {
			
			$sth = $this->db->prepare("
				SELECT * 
				FROM mail_template_recipients
				WHERE mail_template_recipient_template_id = :template
				AND mail_template_recipient_user_id = :user		
			");
			
			$sth->execute(array(
				'template' => $template,
				'user' => $user
			));
			
			return $sth->fetch();
		}
		
		public function create($template, $user, $cc) {
			
			$sth = $this->db->prepare("
				INSERT INTO mail_template_recipients (
					mail_template_recipient_template_id,
					mail_template_recipient_user_id,
					mail_template_recipient_cc,
					user_created
				) VALUES (
					:template, :user, :cc, :created
				)	
			");
			
			$sth->execute(array(
				'template' => $template,
				'user' => $user,
				'cc' => $cc,
				'created' => User::instance()->login
			));
			
			return $this->db->lastInsertId();
		}
		
		public function delete($template, $user) {
			
			$sth = $this->db->prepare("
				DELETE FROM mail_template_recipients
				WHERE mail_template_recipient_template_id = :template
				AND mail_template_recipient_user_id = :user		
			");
			
			return $sth->execute(array(
				'template' => $template,
				'user' => $user
			));
		}
	}