<?php 

	class Application_Language_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id, $language) {
				
			$sth = $this->db->query("
				SELECT *
				FROM application_languages
				WHERE application_language_application = $id AND application_language_language = $language
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_all_languages($id) {
				
			$sth = $this->db->query("
				SELECT application_language_language, application_language_application
				FROM application_languages
				WHERE application_language_application = $id
			");
				
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function create($id, $language) {
			
			$user = User::instance();
		
			$this->db->query("
				INSERT INTO application_languages ( 
					application_language_application, 
					application_language_language, 
					user_created, 
					date_created 
				)
				VALUES ( 
					$id, $language, '".$user->login."', NOW() 
				)
			");
			
			return $this->db->lastInsertId();
		}
		
		public function delete($id, $language) {
			
			return $this->db->exec("
				DELETE FROM application_languages
				WHERE application_language_application = $id AND application_language_language = $language
			");
		}
	}