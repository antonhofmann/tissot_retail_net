<?php

	class Red_Comment_Category_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {

			$sth = $this->db->query("SELECT *
				FROM red_commentcategories
				WHERE red_commentcategory_id = $id
			");

			$commentcategory = ($sth) ? $sth->fetch() : null;

			return $commentcategory;
		}

		public function create($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);

			$sth = $this->db->prepare("
				INSERT INTO red_commentcategories ( $bindFields )
				VALUES ( $bindValues )
			");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();
			return $this->db->lastInsertId();
		}

		public function update($id, $data) {
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE red_commentcategories
				SET $bindFields
				WHERE red_commentcategory_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		public function delete($id) {

			$sth = $this->db->query("
				DELETE FROM red_commentcategories
				WHERE red_commentcategory_id = $id
			");

			return ($sth) ? true : false;
		}
		
		public function dropdown_loader($filters=null) {
		
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
			
			$sth = $this->db->query("
				SELECT
					red_commentcategory_id,
					red_commentcategory_name
				FROM red_commentcategories
				$filter
				ORDER BY red_commentcategory_name
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}

	}