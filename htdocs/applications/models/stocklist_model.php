<?php 

	class Stocklist_Model extends Model {
	
		public function __construct($connector) {
			parent::__construct($connector);
		}
		
		public function read($id) {
			$sth = $this->db->query("
				SELECT *
				FROM scpps_stocklists
				WHERE scpps_stocklist_id = $id
			");	
			return ($sth) ? $sth->fetch() : false;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO scpps_stocklists ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE scpps_stocklists SET $bindFields
				WHERE scpps_stocklist_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM scpps_stocklists
				WHERE scpps_stocklist_id = $id
			");	
			
			return ($sth) ? true : false;
		}
		
		public function get_weeks_data($id) {
			
			$sth = $this->db->query("
				SELECT *
				FROM scpps_weeks
				WHERE scpps_week_stocklist_id = $id
			");
			
			return ($sth) ? $sth->fetchAll() : false;
		}
		
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
				
			$sth = $this->db->query("
				SELECT scpps_stocklist_id, scpps_stocklist_title
				FROM scpps_stocklists
				$filter
				ORDER BY scpps_stocklist_title
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	}
	