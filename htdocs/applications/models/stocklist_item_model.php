<?php 

	class Stocklist_Item_Model extends Model {
	
		public function __construct($connector) {
			parent::__construct($connector);
		}
		
		public function read($stocklist, $item) {
			
			$sth = $this->db->query("
				SELECT *
				FROM scpps_stocklist_items
				WHERE scpps_stocklist_item_stocklist_id = $stocklist AND scpps_stocklist_item_item_id = $item
			");	
			
			return ($sth) ? $sth->fetch() : false;
		}
		
		public function create($stocklist, $item) {
				
			$user = user::instance();
			$username = $user->login;
			
			$sth = $this->db->query("
				INSERT INTO scpps_stocklist_items ( 
					scpps_stocklist_item_stocklist_id, 
					scpps_stocklist_item_item_id,
					user_created,
					date_created )
				VALUES ( $stocklist, $item,  '$username', NOW() )
			");
			
			return $this->db->lastInsertId();
		}
		
		public function delete($stocklist, $item) {
			
			$sth = $this->db->query("
				DELETE FROM scpps_stocklist_items
				WHERE scpps_stocklist_item_stocklist_id = $stocklist AND scpps_stocklist_item_item_id = $item
			");	
			
			return ($sth) ? true : false;
		}
		
		public function load($stocklist) {
			
			$sth = $this->db->query("
				SELECT scpps_stocklist_item_id, scpps_stocklist_item_item_id
				FROM scpps_stocklist_items
				WHERE scpps_stocklist_item_stocklist_id = $stocklist
			");
			
			return ($sth) ? $sth->fetchAll() : false;
		}
	}
	