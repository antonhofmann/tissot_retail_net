<?php 

	class Visual_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_visuals
				WHERE mps_visual_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
			
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
			
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
				
			$sth = $this->db->prepare("
				INSERT INTO mps_visuals ( $bindFields )
				VALUES ( $bindValues )
			");
			
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
			
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
		
			$sth = $this->db->prepare("
				UPDATE mps_visuals SET $bindFields
				WHERE mps_visual_id = :id
			");
	
			$sth->bindValue(":id", $id);
	
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
	
			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->prepare("
				DELETE FROM mps_visuals
				WHERE mps_visual_id = :id
			");
			
			$sth->bindParam('id', $id, PDO::PARAM_INT);
			return $sth->execute();
		}
		
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
			
			$sth = $this->db->query("
				SELECT mps_visual_id, mps_visual_name
				FROM mps_visuals
				$filter
				ORDER BY mps_visual_name;
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
	}
	