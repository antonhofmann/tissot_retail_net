<?php 

	class Mastersheet_File_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_mastersheet_files 
				WHERE mps_mastersheet_file_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mps_mastersheet_files ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
		
			$sth = $this->db->prepare("
				UPDATE mps_mastersheet_files 
				SET $bindFields
				WHERE mps_mastersheet_file_id = :id
			");
		
			$sth->bindValue(":id", $id);
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_mastersheet_files 
				WHERE mps_mastersheet_file_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function deleteAll($mastersheet) {
			
			$sth = $this->db->query("
				DELETE FROM mps_mastersheet_files 
				WHERE mps_mastersheet_file_mastersheet_id = $mastersheet
			");
			
			return ($sth) ? true : false;
		}
		
		public function load($mastersheet) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_mastersheet_files 
				WHERE mps_mastersheet_file_mastersheet_id = $mastersheet
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
	}