<?php 

	class Pos_Area_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($pos) {

			$sth = $this->db->query("
				SELECT * 
				FROM posareas
				WHERE posarea_posaddress = $pos 
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function create($pos, $area) {
			
			$user = User::instance();
		
			$sth = $this->db->prepare("
				INSERT INTO posareas ( 
					posarea_posaddress,
					posarea_area,
					user_created,
					date_created )
				VALUES (:pos, :area, :user, :date)
			");
		
			$sth->execute(array(
				':pos' => $pos,
				':area' => $area,
				':user' => $user->login,
				':date' =>  date('Y-m-d H:i:s')
			));
			
			return $this->db->lastInsertId();
		}
		
		public function delete($pos) {
			
			$sth = $this->db->query("
				DELETE FROM posareas
				WHERE posarea_posaddress = $pos
			");
			
			return ($sth) ? true : false;
		}
	}
	