<?php 

	class OrderModel extends Model {
	
		public function __construct($connector=null) { 
			
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->prepare("
				SELECT *
				FROM orders
				LEFT JOIN countries on country_id = order_shop_address_country 
				WHERE order_id = :id		
			");
			
			$sth->bindParam('id', $id, PDO::PARAM_INT);
			
			$sth->execute();
			
			return $sth->fetch();
		}
		
		public function create($data) {
			
			$statement = Database::insertStatement('orders', $data);
			
			$sth = $this->db->prepare($statement);
			
			$sth->execute($data);
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
			
			$statement = Database::updateStatement('orders', 'project_id', $data);
			
			$sth = $this->db->prepare($statement);
			
			$data['order_id'] = $id;
			
			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->prepare("
				DELETE FROM orders
				WHERE order_id = :id		
			");
			
			$sth->bindParam('id', $id, PDO::PARAM_INT);
			
			return $sth->execute();
		}

		public function orderMailTrack($data) {
			
			$statement = Database::insertStatement('order_mails', $data);
			
			$sth = $this->db->prepare($statement);
			$sth->execute($data);
			
			return $this->db->lastInsertId();
		}

		/**
		 * Was an order mail already sent?
		 * @param  integer $orderID
		 * @param  integer $userID
		 * @param  integer $mailTemplateID
		 * @return boolean
		 */
		public function isOrderMailSent($orderID, $userID, $mailTemplateID) {
			//var_dump($orderID, $mailTemplateID);
			$sth = $this->db->prepare("
				SELECT 
					*
				FROM 
					order_mails
				WHERE
					order_mail_order = :order_id
				AND
					order_mail_user = :user_id
				AND
					order_mail_template_id = :template_id
				");
			$sth->bindParam(':order_id', $orderID, PDO::PARAM_INT);
			$sth->bindParam(':user_id', $userID, PDO::PARAM_INT);
			$sth->bindParam(':template_id', $mailTemplateID, PDO::PARAM_INT);
			$sth->execute();
			return $sth->rowCount() > 0;
		}

		/**
		 * Get project costs for an order
		 * @param  integer $orderID
		 * @return array
		 */
		public function getProjectCosts($orderID) {
			$sth = $this->db->prepare("
				SELECT
					*
				FROM
					project_costs
				WHERE
					project_cost_order = :order_id
				");
			$sth->bindParam(':order_id', $orderID);
			$sth->execute();
			return $sth->fetch();
		}
	}