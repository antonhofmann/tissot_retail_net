<?php 

	class SAP_Parameter_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->query("
				SELECT *
				FROM sap_systemparameters 
				WHERE sap_systemparameter_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);

			$sth = $this->db->prepare("
				INSERT INTO sap_systemparameters ( $bindFields )
				VALUES ( $bindValues )
			");
		
			$sth->execute($data);
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE sap_systemparameters 
				SET $bindFields
				WHERE sap_systemparameter_id = :id
			");
			
			$data['id'] = $id;


			return $sth->execute($data);
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM sap_systemparameters 
				WHERE sap_systemparameter_id = $id
			");
			
			return ($sth) ? true : false;
		}
	}
	