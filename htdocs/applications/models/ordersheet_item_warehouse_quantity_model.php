<?php 

	class Ordersheet_Item_Warehouse_Quantity_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_warehouse_quantities
				WHERE mps_ordersheet_item_warehouse_quantity_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_from_warehouse($ordersheet, $item, $warehouse) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_warehouse_quantities
				WHERE 
					mps_ordersheet_item_warehouse_quantity_ordersheet_id = $ordersheet 
					AND mps_ordersheet_item_warehouse_quantity_ordersheet_item_id = $item
					AND mps_ordersheet_item_warehouse_quantity_warehouse_id = $warehouse
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function load($ordersheet, $item, $filters=null) { 
			
			$filter = (is_array($filters)) ? ' AND '.join(' AND ', $filters) : null;
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_warehouse_quantities 
				WHERE 
					mps_ordersheet_item_warehouse_quantity_ordersheet_id = $ordersheet 
					AND mps_ordersheet_item_warehouse_quantity_ordersheet_item_id = $item 
					$filter
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function create_planned($ordersheet, $item, $pos, $quantity) { 
			
			$user = user::instance();
			$user_created = $user->login;
			
			$this->db->query("
				INSERT INTO mps_ordersheet_item_warehouse_quantities (
					mps_ordersheet_item_warehouse_quantity_ordersheet_id,
					mps_ordersheet_item_warehouse_quantity_ordersheet_item_id,
					mps_ordersheet_item_warehouse_quantity_warehouse_id,
					mps_ordersheet_item_warehouse_quantity_planned,
					user_created
				)
				VALUES ( $ordersheet, $item, $pos, $quantity, '$user_created' )
			");
			
			return $this->db->lastInsertId();
		}
		
		public function create_delivered($ordersheet, $item, $pos, $quantity) { 
			
			$user = user::instance();
			$user_created = $user->login;
			
			$this->db->query("
				INSERT INTO mps_ordersheet_item_warehouse_quantities (
					mps_ordersheet_item_warehouse_quantity_ordersheet_id,
					mps_ordersheet_item_warehouse_quantity_ordersheet_item_id,
					mps_ordersheet_item_warehouse_quantity_warehouse_id,
					mps_ordersheet_item_warehouse_quantity_delivered,
					user_created
				)
				VALUES ( $ordersheet, $item, $pos, $quantity, '$user_created' )
			");
			
			return $this->db->lastInsertId();
		}
		
		public function update_planned($id, $quantity) {
			
			$user = user::instance();
			$user_modified = $user->login;
		
			return $this->db->query("
				UPDATE mps_ordersheet_item_warehouse_quantities SET
					mps_ordersheet_item_warehouse_quantity_planned = $quantity,
					user_modified = '$user_modified',
					date_modified = NOW()
				WHERE mps_ordersheet_item_warehouse_quantity_id = $id
			");
		}
		
		public function update_delivered($id, $quantity) {
			
			$user = user::instance();
			$user_modified = $user->login;
		
			return $this->db->query("
				UPDATE mps_ordersheet_item_warehouse_quantities SET
					mps_ordersheet_item_warehouse_quantity_delivered = $quantity,
					user_modified = '$user_modified',
					date_modified = NOW()
				WHERE mps_ordersheet_item_warehouse_quantity_id = $id
			");
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_item_warehouse_quantities 
				WHERE mps_ordersheet_item_warehouse_quantity_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function delete_from_pos($ordersheet, $item, $warehouse) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_item_warehouse_quantities 
				WHERE 
					mps_ordersheet_item_warehouse_quantity_ordersheet_id = $ordersheet 
					AND mps_ordersheet_item_warehouse_quantity_ordersheet_item_id = $item
					AND mps_ordersheet_item_warehouse_quantity_warehouse_id = $warehouse
			");
			
			return ($sth) ? true : false;
		}
		
		public function delete_from_item($ordersheet, $item) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_item_warehouse_quantities 
				WHERE 
					mps_ordersheet_item_warehouse_quantity_ordersheet_id = $ordersheet
					AND mps_ordersheet_item_warehouse_quantity_ordersheet_item_id = $item
			");
			
			return ($sth) ? true : false;
		}
		
		public function delete_all($ordersheet) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_item_warehouse_quantities
				WHERE mps_ordersheet_item_warehouse_quantity_ordersheet_id = $ordersheet
			");
			
			return ($sth) ? true : false;
		}
	}
	