<?php 
/**
 * Project masterplan phase model
 * 
 * Represents one phase of a project masterplan. A project masterplan
 * may have several phases. A phase may have several subphases.
 * We do not differentiate between phases and subphases on the model-level,
 * a subphase is just a phase with a parentID. A parent phase does not have
 * any steps assigned directly and does not have a parentID.
 */
class Project_Masterplan_Phase_Model extends Model {
  /**
   * Create a phase
   * @param  array $data
   * @return integer Phase ID
   */
  public function create($data) {
    $steps = $data['steps'];
    unset($data['steps']);

    foreach ($data as $key => $value) {
      $bindFields[] = $key;
      $bindValues[] = ":$key";
    }
    
    $sth = $this->db->prepare(sprintf("
      INSERT INTO project_masterplan_phases (%s)
      VALUES (%s)
      ",
      join(',', $bindFields),
      join(',', $bindValues)
    ));
    
    foreach ($data as $key => $value) {
      $sth->bindValue(":$key", $value);
    }
  
    $sth->execute();
    $phaseID = $this->db->lastInsertId();

    // save steps
    if (isset($data['steps']) && is_array($data['steps'])) {
      $this->saveSteps($data['steps'], $phaseID);
    }

    return $phaseID;
  }
  
  /**
   * Update a phase
   * @param  integer $id
   * @param  array $data
   * @return boolean
   */
  public function update($id, $data) {
    // save steps
    if (isset($data['steps']) && is_array($data['steps'])) {
      $this->saveSteps($data['steps'], $id);
      unset($data['steps']);
    }

    foreach ($data as $key => $value) {
      $bindFields[] = "$key = :$key";
    }

    $sth = $this->db->prepare(sprintf("
      UPDATE project_masterplan_phases
      SET %s
      WHERE project_masterplan_phase_id = :id
      ",
      join(',', $bindFields)
    ));

    $sth->bindValue(":id", $id);
  
    foreach ($data as $key => $value) {
      if (empty($value) && $value !== '0') {
        $value = null;
      }
      $sth->bindValue(":$key", $value);
    }
  
    return $sth->execute();
  }

  /**
   * Save steps
   * Delegates the saving of steps to step model
   * @param array $steps
   * @param integer $phaseID
   * @return void
   */
  protected function saveSteps(array $steps, $phaseID = null) {
    foreach($steps as $step) {
      $data = array(
        'project_masterplan_phase_id' => $phaseID,
        'step_nr'                     => $step->step_nr,
        'pos'                         => $step->pos,
        'title'                       => $step->title,
        'step_type'                   => $step->step_type,
        'color'                       => $step->color,
        'value'                       => $step->value,
        'responsible_role_id'         => $step->responsible_role_id,
        'responsible_user_id'         => $step->responsible_user_id,
        'apply_corporate'             => $step->apply_corporate,
        'apply_franchisee'            => $step->apply_franchisee,
        'apply_other'                 => $step->apply_other,
        'apply_store'                 => $step->apply_store,
        'apply_sis'                   => $step->apply_sis,
        'apply_kiosk'                 => $step->apply_kiosk,
        'apply_independent'           => $step->apply_independent,
        'is_active'                   => $step->is_active,
        'duration_defaults'           => $step->duration_defaults,
        'duration_exceptions'         => $step->duration_exceptions,
      );
      $stepModel = new Project_Masterplan_Step_Model();
      if (isset($step->id)) {
        $stepModel->update($step->id, $data);
      }
      else {
        $stepModel->create($data);
      }
    }
  }
}
