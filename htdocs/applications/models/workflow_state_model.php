<?php 


	/**
	 * Workflow State Model
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class WorkFlow_State_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) { 
			
			$sth = $this->db->query("
				SELECT *
				FROM mps_workflow_states 
				WHERE mps_workflow_state_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mps_workflow_states ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE mps_workflow_states 
				SET $bindFields
				WHERE mps_workflow_state_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_workflow_states 
				WHERE mps_workflow_state_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
				
			$sth = $this->db->query("
				SELECT mps_workflow_state_id, mps_workflow_state_name
				FROM mps_workflow_states
				$filter
				ORDER BY mps_workflow_state_code
			");
				
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function dropdown($filters=null) {
			
			if ($filters) {
				$filters = (is_array($filters)) ? join(' AND ', $filters) : $filters;
				$filters = "WHERE $filters";
			}
			
			$sth = $this->db->query("
				SELECT 
					mps_workflow_state_id, 
					mps_workflow_state_name
				FROM mps_workflow_states
				$filters
				ORDER BY mps_workflow_state_code
			");
				
			return ($sth) ? $sth->fetchAll() : null;
		}
	}