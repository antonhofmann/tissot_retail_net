<?php 

	class Mail_Template_Action_Model extends Model {

		
		public function __construct() {
			
			parent::__construct();
		}
		
		public function read($id) {
			
			$sth = $this->db->prepare("
				SELECT * 
				FROM mail_template_actions
				WHERE mail_template_action_id = :id		
			");
			
			$sth->bindParam('id', $id);
			$sth->execute();
			
			return $sth->fetch();
		}
		
		public function create($data) {
			
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO mail_template_actions ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
		
			$sth = $this->db->prepare("
				UPDATE mail_template_actions 
				SET $bindFields
				WHERE mail_template_action_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($template, $user) {
			
			$sth = $this->db->prepare("
				DELETE FROM mail_template_actions
				WHERE  mail_template_action_id = :id		
			");
			
			$sth->bindParam('id', $id);
			
			return $sth->execute();
		}
	}