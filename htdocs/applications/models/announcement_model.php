<?php 

	class Announcement_Model extends Model {
	
		public function __construct($connector) { 
			parent::__construct($connector);
		}
				
		public function read($id) {
	
			$sth = $this->db->query("
				SELECT *,
					DATE_FORMAT(mps_announcement_date, '%d.%m.%Y') as mps_announcement_date,
					DATE_FORMAT(mps_announcement_expiry_date, '%d.%m.%Y') as mps_announcement_expiry_date
				FROM mps_announcements
				WHERE mps_announcement_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
			
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
				
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mps_announcements ( $bindFields )
				VALUES ( $bindValues )
			");
				
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
				
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id,$data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
				
			$bindFields = join(' , ', $bindFields);
			
			$sth = $this->db->prepare("
				UPDATE mps_announcements SET $bindFields 
				WHERE mps_announcement_id = :id
			");
				
			$sth->bindValue(":id", $id);
				
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
				
			return $sth->execute();
		}
	
		public function delete($id) {
			
			$sth = $this->db->prepare("
				DELETE FROM mps_announcements 
				WHERE mps_announcement_id = :id
			");
			
			$sth->bindParam('id', $id, PDO::PARAM_INT);
			return $sth->execute(); 
		}
	}