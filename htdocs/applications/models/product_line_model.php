<?php

	class Product_Line_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {
			$sth = $this->db->query("
						SELECT *
						FROM product_lines
						WHERE product_line_id = $id
					");
			return ($sth) ? $sth->fetch() : false;
		}

		public function create($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);

			$sth = $this->db->prepare("
						INSERT INTO product_lines ( $bindFields )
						VALUES ( $bindValues )
					");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();

			return $this->db->lastInsertId();
		}

		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
						UPDATE product_lines SET $bindFields
						WHERE product_line_id = :id
					");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		public function delete($id) {

			$sth = $this->db->query("
				DELETE FROM product_lines
				WHERE product_line_id = $id
			");

			return ($sth) ? true : false;
		}

		public function loader($filters=null) {

			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;

			$sth = $this->db->query("
				SELECT product_line_id, product_line_name
				FROM product_lines
				$filter
				ORDER BY product_line_name
			");

			return ($sth) ? $sth->fetchAll() : null;
		}
	}