<?php

	class Red_Project_File_Access_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($file, $partner) {

			$sth = $this->db->query("
				SELECT red_file_access_id
				FROM red_file_accesses
				WHERE red_file_access_file_id = $file AND red_file_access_project_partner_id = $partner
			");

			$result = ($sth) ? $sth->fetch() : null;
			return $result['red_comment_access_id'];
		}

		public function create($file, $partner) {

			$user = user::instance();
			$user_login = $user->login;

			$sth = $this->db->query("
				INSERT INTO red_file_accesses (
					red_file_access_file_id,
					red_file_access_project_partner_id,
					user_created,
					date_created
				)
				VALUES (
					$file,
					$partner,
					'$user_login',
					NOW()
				)
			");

			return $this->db->lastInsertId();
		}

		public function delete($file, $partner) {

			$sth = $this->db->query("
				DELETE FROM red_file_accesses
				WHERE red_file_access_file_id = $file AND red_file_access_project_partner_id = $partner
			");

			return ($sth) ? true : false;
		}
		
		public function deleteAll($file) {

			$sth = $this->db->query("
				DELETE FROM red_file_accesses
				WHERE red_file_access_file_id = $file
			");

			return ($sth) ? true : false;
		}
	}