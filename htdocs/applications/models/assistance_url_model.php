<?php 

	class Assistance_URL_Model extends Model {
		
		public function __construct($connector=null) { 
			parent::__construct($connector);
		}
		
		public function read($id, $url) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM assistance_urls
				WHERE assistance_url_assistance_id = $id AND assistance_url_url = '$url'
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($id, $url) {

			$user = user::instance()->login;
			
			$this->db->exec("
				INSERT INTO assistance_urls (
					assistance_url_assistance_id,
					assistance_url_url,
					user_created
				)
				VALUES (
					$id,
					'$url',
					'$user'
				)
			");
			
			return $this->db->lastInsertId();
		}

		public function delete($id, $url) {
			return $this->db->exec("
				DELETE FROM assistance_urls
				WHERE assistance_url_assistance_id = $id AND assistance_url_url = '$url'
			");
		}

		public function deleteAll($id) {
			return $this->db->exec("
				DELETE FROM assistance_urls
				WHERE assistance_url_assistance_id = $id
			");
		}
	}
	