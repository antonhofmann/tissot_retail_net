<?php 

	class ProjectTrackerModel extends Model {
	
		public function __construct($connector=null) { 
			
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->prepare("
				SELECT *
				FROM projecttracking
				WHERE projecttracking_id = :id		
			");
			
			$sth->bindParam('id', $id, PDO::PARAM_INT);
			
			$sth->execute();
			
			return $sth->fetch();
		}
		
		public function create($data) {
			
			$statement = Database::insertStatement('projecttracking', $data);
			
			$sth = $this->db->prepare($statement);
			
			$sth->execute($data);
			
			return $this->db->lastInsertId();
		}
		
		public function delete($id) {
			
			$sth = $this->db->prepare("
				DELETE FROM projecttracking
				WHERE projecttracking_id = :id		
			");
			
			$sth->bindParam('id', $id, PDO::PARAM_INT);
			
			return$sth->execute();
		}
	}