<?php 

	/**
	 * Material Model
	 * @author aserifi
	 *
	 */
	class Material_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) { 
			
			$sth = $this->db->query("
				SELECT *,
					IF(mps_material_standard_order_date = '0000-00-00', '', DATE_FORMAT(mps_material_standard_order_date,'%d.%m.%Y')) AS mps_material_standard_order_date
				FROM mps_materials 
				WHERE mps_material_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mps_materials ( $bindFields )
				VALUES ( $bindValues )
			");
			
			$sth->execute($data);
			
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE mps_materials SET $bindFields
				WHERE mps_material_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_materials 
				WHERE mps_material_id = $id
			");
			
			return ($sth) ? true : false;
		}
	}
	