<?php


	class Red_Comment_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {

			$sth = $this->db->query("
				SELECT *
				FROM red_comments
				WHERE red_comment_id = $id
			");

			$record = ($sth) ? $sth->fetch() : null;

			return $record;
		}

		public function create($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);

			$sth = $this->db->prepare("
				INSERT INTO red_comments ( $bindFields )
				VALUES ( $bindValues )
			");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();

			return $this->db->lastInsertId();
		}


		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE red_comments
				SET $bindFields
				WHERE red_comment_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		public function delete($id) {

			$sth = $this->db->query("
				DELETE FROM red_comments
				WHERE red_comment_id = $id
			");

			return ($sth) ? true : false;
		}

		/**
		 * Returns an array of all partners with access to this comment
		 * in consideration of access from and access until dates
		 *
		 */
		 public function get_partners_with_access($id) {
			
		 	$sth = $this->db->query("
				SELECT * FROM red_comment_accesses
				WHERE red_comment_access_comment_id = $id
			");

			$result = $sth->fetchAll();

			$partners = _array::extract_by_key($result, 'red_comment_access_project_partner_id');

			if (is_array($partners)) {
				
				$sth = $this->db->query("
					SELECT * 
					FROM red_project_partners
					WHERE red_project_partner_id IN (".join(',', $partners).")
					AND (red_project_partner_access_from = '' OR red_project_partner_access_from IS NULL OR red_project_partner_access_from <= CURDATE() )
					AND (red_project_partner_access_until = '' OR red_project_partner_access_until IS NULL OR red_project_partner_access_until >= CURDATE())
				");


				$result = ($sth) ? $sth->fetchAll() : null;
				return ($result) ?  _array::extract_by_key($result, 'red_project_partner_id') : null;
			}

			return null;
		}

	}