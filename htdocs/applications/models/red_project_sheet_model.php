<?php


	class Red_Project_Sheet_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {
			$sth = $this->db->query("
				SELECT *,
					DATE_FORMAT(red_projectsheet_openingdate, '%d.%m.%Y') as red_projectsheet_openingdate,
					DATE_FORMAT(red_projectsheet_approval_deadline01, '%d.%m.%Y') as red_projectsheet_approval_deadline01,
					DATE_FORMAT(red_projectsheet_approval_deadline02, '%d.%m.%Y') as red_projectsheet_approval_deadline02,
					DATE_FORMAT(red_projectsheet_approval_deadline03, '%d.%m.%Y') as red_projectsheet_approval_deadline03,
					DATE_FORMAT(red_projectsheet_approval_deadline04, '%d.%m.%Y') as red_projectsheet_approval_deadline04,
					DATE_FORMAT(red_projectsheet_approval_deadline05, '%d.%m.%Y') as red_projectsheet_approval_deadline05,
					DATE_FORMAT(red_projectsheet_approval_deadline06, '%d.%m.%Y') as red_projectsheet_approval_deadline06
				FROM red_projectsheets
				WHERE red_projectsheet_project_id = $id
			");

			return ($sth) ? $sth->fetch() : null;
		}

		public function create($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO red_projectsheets ( $bindFields )
				VALUES ( $bindValues )
			");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();
			return $this->db->lastInsertId();
		}

		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE red_projectsheets
				SET $bindFields
				WHERE red_projectsheet_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
	}
