<?php 

	/**
	 * Master Sheet Splitting List Item Model
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Mastersheet_Splitting_List_Item_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) { 
			$sth = $this->db->query("
				SELECT * 
				FROM mps_mastersheet_splitting_list_items 
				WHERE mps_mastersheet_splitting_list_item_id = $id
			");
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO mps_mastersheet_splitting_list_items ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE mps_mastersheet_splitting_list_items SET $bindFields
				WHERE mps_mastersheet_splitting_list_item_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_mastersheet_splitting_list_items 
				WHERE mps_mastersheet_splitting_list_item_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function deleteAll($splittinglist) {
			
			$sth = $this->db->query("
				DELETE FROM mps_mastersheet_splitting_list_items 
				WHERE mps_mastersheet_splitting_list_item_splittinglist_id = $splittinglist
			");
			
			return ($sth) ? true : false;
		}
		
		public function load($splittinglist, $filters=null) { 
			
			$filter = (is_array($filters)) ? join(' AND ', $filters) : null;
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_mastersheet_splitting_list_items 
				WHERE mps_mastersheet_splitting_list_item_splittinglist_id = $splittinglist $filter
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
	}
	