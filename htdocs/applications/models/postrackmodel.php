<?php 

	class PosTrackModel extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function create($pos, $user, $filed, $oldValue, $newValue, $comment) {
			
			$sth = $this->db->prepare("
				INSERT INTO postracking ( 
					postracking_pos_id, 
					postracking_user_id,
					postracking_field,    
					postracking_oldvalue, 
					postracking_newvalue, 
					postracking_comment, 
					postracking_time
			) VALUES (?,?,?,?,?,?,NOW())
			");
				
			$sth->execute(array($pos, $user, $filed, $oldValue, $newValue, $comment));
			
			return $this->db->lastInsertId();
		}
	}
	