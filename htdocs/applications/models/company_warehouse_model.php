<?php 

	class Company_Warehouse_Model extends Model {
	
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
				
			$sth = $this->db->query("
				SELECT *
				FROM address_warehouses
				WHERE address_warehouse_id = $id
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function getStockReserve($company) {
				
			$sth = $this->db->query("
				SELECT *
				FROM address_warehouses
				WHERE address_warehouse_address_id = $company AND address_warehouse_stock_reserve = 1
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO address_warehouses ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();

			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE address_warehouses 
				SET $bindFields
				WHERE address_warehouse_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM	address_warehouses
				WHERE address_warehouse_id = $id
			");
				
			return ($sth) ? true : false;
		}
		
		public function delete_all($id) {
			
			$sth = $this->db->query("
				DELETE FROM	address_warehouses
				WHERE address_warehouse_address_id = $id
			");
				
			return ($sth) ? true : false;
		}
	
		public function load($company, $filters=null) {
				
			$filter = (is_array($filters)) ? ' AND '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT *
				FROM address_warehouses
				WHERE address_warehouse_address_id = $company $filter
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	}