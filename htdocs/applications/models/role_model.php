<?php 

	class Role_Model extends Model {
	
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
				
			$sth = $this->db->query("
				SELECT *
				FROM roles
				WHERE role_id = $id
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function readCode($code) {
				
			$sth = $this->db->query("
				SELECT *
				FROM roles
				WHERE role_code = '$code'
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO roles ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(' , ', $bindFields);
				
			$sth = $this->db->prepare("
				UPDATE roles SET $bindFields
				WHERE role_id = :id
			");
		
			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM	roles
				WHERE role_id = $id
			");
				
			return ($sth) ? true : false;
		}
		
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT role_id, role_name
				FROM roles
				$filter
				ORDER BY role_name
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}

		public function get_user_roles($user) {
		
			$sth = $this->db->query("
				SELECT role_id, role_name
				FROM roles
				INNER JOIN user_roles ON user_role_role = role_id
				WHERE user_role_user = $user
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function get_application_roles($application) {
		
			$sth = $this->db->query("
				SELECT role_id, role_name
				FROM application_roles
				INNER JOIN roles ON role_id = application_role_role
				INNER JOIN applications ON application_id = application_role_application
				WHERE application_shortcut = '$application'
				ORDER BY role_name
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	}