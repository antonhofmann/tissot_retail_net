<?php 

	class Assistance_Section_Link_Model extends Model {
		
		public function __construct($connector=null) { 
			
			parent::__construct($connector);
		}
		
		public function read($section, $link) {
			
			$sth = $this->db->prepare("
				SELECT assistance_section_link_id 
				FROM assistance_section_links
				WHERE assistance_section_link_section_id = :section AND assistance_section_link_link = :link
			");
			
			$sth->execute(array(
				'section' => $section,
				'link' => $link
			));
			
			return $sth->fetch();
		}
		
		public function create($section, $link) {

			$sth = $this->db->prepare("
				INSERT INTO assistance_section_links (
					assistance_section_link_section_id,
					assistance_section_link_link,
					user_created
				) VALUES (
					:section, :link, :user
				)
			");
			
			$sth->execute(array(
				'section' => $section,
				'link' => $link,
				'user' => User::instance()->login
			));
			
			return $this->db->lastInsertId();
		}

		public function delete($section, $link) {
			
			$sth = $this->db->prepare("
				DELETE FROM assistance_section_links
				WHERE assistance_section_link_section_id = :section AND assistance_section_link_link = :link
			");
			
			return $sth->execute(array(
				'section' => $section,
				'link' => $link 
			));
		}
	}
	