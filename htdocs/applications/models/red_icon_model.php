<?php

	class Red_Icon_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {

			$sth = $this->db->query("
				SELECT *
				FROM db_retailnet_red.red_icons
				WHERE red_icon_id = $id
			");

			return ($sth) ? $sth->fetch() : null;
		}
	}