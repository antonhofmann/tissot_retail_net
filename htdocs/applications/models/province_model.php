<?php 

	class Province_Model extends Model {
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
				
			$sth = $this->db->query("
				SELECT * 
				FROM provinces
				WHERE province_id = $id;
			");
				
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($data) {
			
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
				
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO provinces ( $bindFields )
				VALUES ( $bindValues )
			");
				
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
				
			$sth->execute();
			
			return $this->db->lastInsertId();
		}

		public function update($id, $data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
				
			$bindFields = join(' , ', $bindFields);
			
			$sth = $this->db->prepare("
				UPDATE provinces SET $bindFields 
				WHERE province_id = :id
			");
				
			$sth->bindValue(":id", $id);
				
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
				
			return $sth->execute();
		}

		public function delete($id) {
			
			return $this->db->exec("
				DELETE FROM provinces
				WHERE province_id = $id
			");
		}
	
		public function loader($filters=null) {
			
			$filter = (is_array($filters)) ? ' WHERE '.join(' AND ', $filters) : null;
		
			$sth = $this->db->query("
				SELECT province_id, province_canton
				FROM provinces
				$filter
				ORDER BY province_canton
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	}
	