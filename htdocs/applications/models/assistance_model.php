<?php 

	class Assistance_Model extends Model {
		
		public function __construct($connector=null) { 
			parent::__construct($connector);
		}
		
		public function read($id) {
			
			$sth = $this->db->prepare("
				SELECT * 
				FROM assistances
				WHERE assistance_id = :id
			");
			
			$sth->bindParam('id', $id);
			
			$sth->execute();
			
			return $sth->fetch();
		}
		
		public function create($data) {
			
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
				
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			
			$sth = $this->db->prepare("
				INSERT INTO assistances ( $bindFields )
				VALUES ( $bindValues )
			");
				
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
				
			$sth->execute();
			
			return $this->db->lastInsertId();
		}

		public function update($id, $data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
				
			$bindFields = join(' , ', $bindFields);
			
			$sth = $this->db->prepare("
				UPDATE assistances SET $bindFields 
				WHERE assistance_id = :id
			");
				
			$data['id'] = $id;
			
				
			return $sth->execute($data);
		}

		public function delete($id) {
			
			$sth = $this->db->prepare("
				DELETE FROM assistances
				WHERE assistance_id = :id
			");
			
			$sth->bindParam('id', $id);

			return $sth->execute();
		}

		public function deleteAllSections($id) {
			
			$sth = $this->db->prepare("
				DELETE FROM assistance_sections
				WHERE assistance_section_assistance_id = :assistance
			");
			
			$sth->bindParam('assistance', $id);
			
			return $sth->execute();
		}
		
		public function getAllSections($id) {
		
			$sth = $this->db->prepare("
				SELECT *
				FROM assistance_sections
				WHERE assistance_section_assistance_id = :assistance
			");
		
			$sth->bindParam('assistance', $id);
				
			$sth->execute();
		
			return $sth->fetchAll();
		}
	}
	