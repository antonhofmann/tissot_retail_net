<?php 

	/**
	 * Order Sheet Item Confirmed Model
	 * @author adoweb
	 * @copyright mediaparx ag
	 * @version 1.0
	 */
	class Ordersheet_Item_Confirmed_Model extends Model {
		
		
		public function __construct($connector) { 
			parent::__construct($connector);
		}
		
		public function read($id) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_confirmed
				WHERE mps_ordersheet_item_confirmed_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		/**
		 * Get last confirmed record for requested item
		 * @param int $item
		 * @return array confirmed item
		 */
		public function read_from_item($item) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_confirmed
				WHERE mps_ordersheet_item_confirmed_ordersheet_item_id = $item
				ORDER BY mps_ordersheet_item_confirmed_id DESC
				LIMIT 1
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_from_reference($ponumber,$reference) { 
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_confirmed
				WHERE mps_ordersheet_item_confirmed_po_number = '$ponumber'
				AND mps_ordersheet_item_confirmed_material_code = '$reference'
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function load($item, $filters=null) { 
			
			$filter = (is_array($filters)) ? ' AND '.join(' AND ', $filters) : null;
			
			$sth = $this->db->query("
				SELECT * 
				FROM mps_ordersheet_item_confirmed 
				WHERE mps_ordersheet_item_confirmed_ordersheet_item_id = $item $filter
			");
			
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function create($data) { 
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);

			$sth = $this->db->prepare("
				INSERT INTO mps_ordersheet_item_confirmed ( $bindFields )
				VALUES ( $bindValues )
			");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();

			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE mps_ordersheet_item_confirmed SET $bindFields
				WHERE mps_ordersheet_item_confirmed_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}
		
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_item_confirmed 
				WHERE mps_ordersheet_item_confirmed_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		public function delete_all($item) {
			
			$sth = $this->db->query("
				DELETE FROM mps_ordersheet_item_confirmed
				WHERE mps_ordersheet_item_confirmed_ordersheet_item_id = $item
			");
			
			return ($sth) ? true : false;
		}
	}
	