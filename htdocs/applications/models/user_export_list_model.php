<?php

	class User_Export_List_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {
			
			$sth = $this->db->query("
				SELECT * 
				FROM user_export_lists 
				WHERE user_export_list_id = $id
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function read_from_table($user, $table) {
			
			$sth = $this->db->query("
				SELECT *
				FROM user_export_lists
				WHERE user_export_list_user_id = $user AND user_export_list_table_id = $table
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		public function create($user, $table, $fields, $sort) {

			$sth = $this->db->prepare("
				INSERT INTO user_export_lists (  
					user_export_list_user_id,
					user_export_list_table_id,
					user_export_list_fields,
					user_export_list_sort
				)
				VALUES (
					:user,
					:table,
					:fields,
					:sort	
				)
			");

			$sth->execute(array(
				'user' => $user,
				'table' => $table,
				'fields' => $fields,
				'sort' => $sort
			));
			
			return $this->db->lastInsertId();
		}
		
		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);
			
			$sth = $this->db->prepare("
				UPDATE user_export_lists SET $bindFields
				WHERE user_export_list_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM user_export_lists 
				WHERE user_export_list_id = $id
			");

			return ($sth) ? true : false;
		}
	}