<?php 

	class Navigation_Model extends Model {
		
		public function __construct($connector) {
			
			parent::__construct($connector);
			
			$settings = Settings::init();
			
			$sth = $this->db->query("
				SELECT language_id
				FROM languages
				WHERE language_iso639_1 = '".$settings->language."' 	
			");
			
			$result = ($sth) ? $sth->fetch() : null;
			$this->language = $result['language_id'];
		}
		
		/**
		 * Load navigation data from navigation ID
		 * @param integer $id
		 * @return array navigation data
		 */
		public function read($id) {
		
			$sth = $this->db->query("
				SELECT
					navigation_id,
					navigation_parent,
					navigation_url,
					navigation_language_name,
					navigation_tab,
					navigation_order,
					navigation_active
				FROM navigations
					INNER JOIN navigation_languages ON navigation_language_navigation = navigation_id
				WHERE 
					navigation_id = $id AND navigation_language_language = ".$this->language."
				ORDER BY navigation_order
			");
		
			return ($sth) ? $sth->fetch() : null;
		}
		
		/**
		 * Load navigations data from request <br />
		 * @param string $request, URL
		 * @param string $limeted, not mandatory, get only navigations permitted for currens user
		 * @return array navigations data
		 */
		public function read_from_request($request, $limeted=false) {
			
			if ($limeted) {
				$binds = Navigation::DB_BIND_ROLES;
				$roles = join(',', user::roles());
				$filters = "AND navigation_active = 1 AND role_navigation_role IN ( $roles )";
			}
			
			$sth = $this->db->query("
				SELECT 
					navigation_id,
					navigation_parent,
					navigation_url,
					navigation_language_name,
					navigation_tab,
					navigation_order,
					navigation_active
				FROM navigations
					INNER JOIN navigation_languages ON navigation_language_navigation = navigation_id
					$binds
				WHERE 
					navigation_url = '$request' 
					AND navigation_language_language = ".$this->language." 
					$filters
			");
			
			return ($sth) ? $sth->fetch() : null;
		}
		
		/**
		 * Add new navigation
		 * @param array $data
		 * @return inserted ID
		 */
		public function create($data) {
				
			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}
		
			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO navigations ( $bindFields )
				VALUES ( $bindValues )
			");
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			$sth->execute();
			return $this->db->lastInsertId();
		}
		
		/**
		 * Update navigation data
		 * @param array $data
		 * @return boolean
		 */
		public function update($id, $data) {
		
			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}
		
			$bindFields = join(',', $bindFields);
		
			$sth = $this->db->prepare("
				UPDATE navigations SET $bindFields
				WHERE navigation_id = :id
			");
		
			$sth->bindValue(":id", $id);
		
			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}
		
			return $sth->execute();
		}
		
		/**
		 * Remove navigation data
		 * @return boolean
		 */
		public function delete($id) {
			
			$sth = $this->db->query("
				DELETE FROM navigations 
				WHERE navigation_id = $id
			");
			
			return ($sth) ? true : false;
		}
		
		/**
		 * Check request URL
		 * @param string $request
		 * @return boolean
		 */
		public function check_request($request, $access=false) { 
			
			if ($access) {
				$roles = join(',', user::roles());
				$binds = Navigation::DB_BIND_ROLES;
				$filters = "AND navigation_active = 1 AND role_navigation_role IN ( $roles )";
			}
			
			$sth = $this->db->query("
				SELECT navigation_id
				FROM navigations $binds
				WHERE navigation_url = '$request' $filters
			");
			
			$result = ($sth) ? $sth->fetch() : false;
			return ($result['navigation_id']) ? true : false;
		}
		
		/**
		 * Load navigations data from request <br />
		 * @param array $filters, fields names
		 * @param string $limeted, not mandatory, get only navigations permitted for current user 
		 * @return array navigations data
		 */
		public function load($filters=null, $limited=false) {
		
			$filters = ($filters) ? ' AND '.join(' AND ', $filters) : null;
			
			if ($limited) {
				$binds = Navigation::DB_BIND_ROLES;
			}
			
			$sth = $this->db->query("
				SELECT DISTINCT
					navigation_id,
					navigation_parent,
					navigation_url,
					navigation_language_name,
					navigation_tab,
					navigation_order,
					navigation_active
				FROM navigations
					INNER JOIN navigation_languages ON navigation_language_navigation = navigation_id
					$binds
				WHERE 
					navigation_language_language = ".$this->language."
					$filters
				ORDER BY 
					navigation_order
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		/**
		 * Load navigations data from parent <br />
		 * @param integer $parent, navigation parent ID
		 * @param boolean $limted, not mandatory, get only navigations permitted for current user 
		 * @return array navigations data
		 */
		public function load_from_parent($parent, $limited=false) {
			
			if ($limited) {
				$binds = Navigation::DB_BIND_ROLES;
				$roles = join(',', user::roles());
				$filters = "AND navigation_active = 1 AND role_navigation_role IN ( $roles )";
			}
			
			$sth = $this->db->query("
				SELECT DISTINCT
					navigation_id,
					navigation_parent,
					navigation_url,
					navigation_language_name,
					navigation_tab,
					navigation_order,
					navigation_active
				FROM navigations
					INNER JOIN navigation_languages ON navigation_language_navigation = navigation_id
					$binds
				WHERE 
					navigation_parent = $parent 
					AND navigation_language_language = ".$this->language." 
					$filters
				ORDER BY 
					navigation_order
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
		
		public function get_user_applications($user=null) {
			
			$roles = join(',', user::roles($user));
			
			$sth = $this->db->query("
				SELECT DISTINCT
					navigation_id,
					navigation_parent,
					navigation_url,
					navigation_language_name,
					navigation_tab,
					navigation_order,
					navigation_active
				FROM navigations
					INNER JOIN navigation_languages ON navigation_language_navigation = navigation_id
					INNER JOIN role_navigations ON role_navigation_navigation = navigation_id
				WHERE 
					navigation_parent = 0 
					AND navigation_active = 1 
					AND role_navigation_role IN ( $roles ) 
					AND navigation_language_language = ".$this->language." 
				ORDER BY 
					navigation_order
			");
		
			return ($sth) ? $sth->fetchAll() : null;
		}
	
		public function access($id, $user) {
			
			$roles = join(',', user::roles($user));
			
			$sth = $this->db->query("
				SELECT *
				FROM role_navigations
				WHERE role_navigation_navigation = $id AND role_navigation_role IN ($roles)
			");
			
			return ($sth) ? $sth->fetchAll() : false;
		}
	}
	