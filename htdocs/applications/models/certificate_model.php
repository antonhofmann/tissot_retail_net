<?php

	class Certificate_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {
			$sth = $this->db->query("
				SELECT *
				FROM certificates
				WHERE certificate_id = $id
			");
			return ($sth) ? $sth->fetch() : false;
		}

		public function create($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);

			$sth = $this->db->prepare("
				INSERT INTO certificates ( $bindFields )
				VALUES ( $bindValues )
			");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();

			return $this->db->lastInsertId();
		}

		/**
		 * Get supplier company for certificate
		 * @param  integer $id certificate id
		 * @return array
		 */
		public function getSupplier($id) {
			$sth = $this->db->prepare("
				SELECT
					address_company
				FROM
					certificates
				INNER JOIN
					addresses ON (certificate_address_id = address_id AND address_active = '1')
				WHERE
					certificate_id = :id
				");
			$sth->bindValue(':id', $id);
			$sth->execute();
			return $sth->fetch();
		}

		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE certificates SET $bindFields
				WHERE certificate_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		public function delete($id) {

			$sth = $this->db->query("
				DELETE FROM certificates
				WHERE certificate_id = $id
			");

			return ($sth) ? true : false;
		}
	}