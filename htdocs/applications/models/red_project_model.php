<?php


	class Red_Project_Model extends Model {

		public function __construct($connector) {
			parent::__construct($connector);
		}

		public function read($id) {

			$sth = $this->db->query("
				SELECT *,
					DATE_FORMAT(red_project_startdate, '%d.%m.%Y') as red_project_startdate,
					DATE_FORMAT(red_project_foreseen_enddate, '%d.%m.%Y') as red_project_foreseen_enddate,
					DATE_FORMAT(red_project_enddate, '%d.%m.%Y') as red_project_enddate
				FROM red_projects
				WHERE red_project_id = $id
			");

			return ($sth) ? $sth->fetch() : null;
		}

		public function create($data) {

			foreach ($data as $key => $value) {
				$bindFields[] = $key;
				$bindValues[] = ":$key";
			}

			$bindFields = join(' , ', $bindFields);
			$bindValues= join(' , ', $bindValues);
			$sth = $this->db->prepare("
				INSERT INTO red_projects ( $bindFields )
				VALUES ( $bindValues )
			");

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			$sth->execute();

			return $this->db->lastInsertId();
		}

		public function update($id, $data) {

			foreach ($data as $key => $value) {
				$bindFields[] = "$key = :$key";
			}

			$bindFields = join(',', $bindFields);

			$sth = $this->db->prepare("
				UPDATE red_projects
				SET $bindFields
				WHERE red_project_id = :id
			");

			$sth->bindValue(":id", $id);

			foreach ($data as $key => $value) {
				$sth->bindValue(":$key", $value);
			}

			return $sth->execute();
		}

		/**
		 *
		 * Checks whether $user_id is a user of a partner of $project_id
		 * in consideration of red_project_partner_access_from and red_project_partner_access_until
		 * @param $project_id
		 * @param $user_id
		 */
		public function user_is_partner($project_id, $user_id) {

			//Get all partners allocated to this prject
			$filters['red_project_partner_project_id'] = "red_project_partner_project_id = ".$project_id;
			$filters['red_project_partner_access_from'] = "(red_project_partner_access_from = '' OR red_project_partner_access_from IS NULL OR red_project_partner_access_from <= CURDATE())";
			$filters['red_project_partner_access_until'] = "(red_project_partner_access_until = '' OR red_project_partner_access_until IS NULL OR red_project_partner_access_until >= CURDATE())";

			$model = new Model(Connector::DB_RETAILNET_RED);
			$result = $model->query("
							  SELECT SQL_CALC_FOUND_ROWS
							   	red_project_partner_id
							  FROM red_project_partners
							 ")
			->filter($filters)
			->fetchAll();

			if (is_array($result)) {
				$partner = new Red_Project_Partner();
				foreach ($result as $row) {
					$partner->read($row['red_project_partner_id']);
					if ($partner->is_user($user_id)) {
						return true;
					}
				}
			}
			
			return false;
		}

		public function delete($id) {

			$sth = $this->db->query("
				DELETE FROM red_projects
				WHERE red_project_id = $id
			");

			return ($sth) ? true : false;
		}

	}
	