<?php

/**
 * FilterIterator that filters out steps that do not apply to a project
 */
class Project_Masterplan_Step_Iterator extends FilterIterator {
  /**
   * POSType (alias) for project
   * @var string
   */
  protected $projectPosType;

  /**
   * Project data
   * @var array
   */
  protected $project;

  /**
   * Read project data
   * @param Iterator $iterator
   * @param integer $projectID
   */
  public function __construct(Iterator $iterator, $projectID) {
    parent::__construct($iterator);

    $ProjectModel = new ProjectModel();
    $PosMapper = new POS_Datamapper();
    
    // get project data
    $this->project = $ProjectModel->read($projectID);
    $postype = $PosMapper->getTypeByID($this->project['project_postype']);
    $this->projectPosType = $postype['alias'];
  }

  /**
   * Defines which iterator elements are accepted
   * @return boolean
   */
  public function accept() {
    $current = $this->current();

    // filter out steps that are not applicable for this project's postype
    $applyPosType = sprintf('apply_%s', $this->projectPosType);
    if ((bool) $current[$applyPosType] === false) {
      return false;
    }

    // filter out steps that are not applicable for this project's costtype
    $applyCostType = sprintf('apply_%s', $this->project['project_costtype']);
    if ((bool) $current[$applyCostType] === false) {
      return false;
    }
    return true;
  }
}