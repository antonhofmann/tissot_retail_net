<?php 

	class Mail_Template_Role {
		
		/**
		 * Dataloader
		 * 
		 * @var array
		 */
		public $data;
		
		/**
		 * Template ID
		 * 
		 * @var int
		 */
		protected $template;
		
		/**
		 * DB Mmodel
		 * 
		 * @return Mail_Template_Role_Model
		 */
		protected $model;
		
		public function __construct() {
			$this->model = new Mail_Template_Role_Model();
		}
		
		public function setTemplate($template) {
			
			$this->template = $template;
		}
		
		public function loadAll() {
			
			$roles = array();
			
			if ($this->template) {
				
				$result = $this->model->loadAll($this->template);
				
				if ($result) {

					foreach ($result as $row) {
						
						$role = $row['mail_template_role_role_id'];;
						$cc = $row['mail_template_role_cc'];;
						
						if ($cc) {
							$roles['cc'][$role] = $role;
						} else {
							$roles['recipient'][$role] = $role;
						}
					}
				}
			}
			
			return $roles;
		}
		
		public function read($user) {
			
			if ($this->template) {
				
				return $this->data = $this->model->read($this->template, $user);
			}
		}
		
		public function create($user, $cc=0) {
			
			$id = $this->model->create($this->template, $user, $cc);
			
			if ($id) {
				
				$this->data = array(
					'mail_template_role_id' => $id,
					'mail_template_role_template_id' => $this->template,
					'mail_template_role_role_id' => $user,
					'mail_template_role_cc' => $cc
				);
			}
			
			return $id;
		}
		
		public function delete($user) {
			
			if ($this->template) {
				
				return $this->model->delete($this->template, $user);
			}
		}
	}