<?php

	class File_Type {
		
		const PERMISSION_EDIT = 'can_administrate_system_data';
		const DB_BIND_APPLICATIONS = "INNER JOIN db_retailnet.file_types ON file_type_id = application_filetype_filetype";
		
		public $data;
	
		public function __construct() {
			$this->model = new File_Type_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["file_type_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['file_type_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($filters=null) {
			$model = new File_Type_Model(Connector::DB_CORE);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
	
		public static function get_id_from_extension($extension) {
				
			$model = new File_Type_Model(Connector::DB_CORE);
			$sth = $model->db->query("SELECT * FROM file_types");
			$result = ($sth) ? $sth->fetchAll() : null;
				
			if ($result) {
				foreach ($result as $row) {
					if ($extension == $row['file_type_extension']) {
						return $row['file_type_id'];
						break;
					}
					elseif (strpos($row['file_type_extension'],';') !== false) {
						$array = explode(';', $row['file_type_extension']);
						if (in_array($extension, $array)) {
							return $row['file_type_id'];
							break;
						}
					}
				}
			}
		}
	}