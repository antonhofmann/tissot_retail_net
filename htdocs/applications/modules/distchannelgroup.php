<?php

	class DistChannelGroup {
		
		/**
		 * Group ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Group Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Database Model
		 * @return DistChannelGroup_Model
		 */
		protected $model;
		
		/**
		 * Edit all disribution channels
		 * @var string
		 */
		const PERMISSION_EDIT = 'can_edit_mps_dist_channels';
		
		/**
		 * view all distribution channels
		 * @var string
		 */
		const PERMISSION_VIEW = 'can_view_mps_dist_channels';
		
		
		/**
		 * Distribution Channel Group
		 * 
		 * @copyright mediaparx ag
		 */
		public function __construct() {
			$this->model = new DistChannelGroup_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["mps_distchannel_group_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_distchannel_group_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($filters=null) {
			$model = new DistChannelGroup_Model(Connector::DB_CORE);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
	}
	