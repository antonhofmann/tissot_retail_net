<?php 

	class Pos_Material {
		
		/**
		 * POS Material ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * POS Material Data
		 * @var array
		 */
		public $data;
		
		/**
		 * POS
		 * @var object
		 */
		public $pos;
		
		/**
		 * Material
		 * @var object
		 */
		public $material;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @var object
		 */
		protected $model;
		
		/**
		 * Join posaddresses
		 * @var string
		 */
		const DB_BIND_POSADDRESSES = 'INNER JOIN db_retailnet.posaddresses ON posaddress_id = mps_pos_material_posaddress';
		
		/**
		 * Join Materials
		 * @var unknown
		 */
		const DB_BIND_MATERIALS = "INNER JOIN mps_materials ON mps_material_id = mps_pos_material_material_id";
		
		
		/**
		 * POS Material<br />
		 * Application Dependet Modul
		 * @param string $connector
		 */
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Pos_Material_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_pos_material_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_pos_material_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public function pos() {
			
			if (!$this->pos) {
				$this->pos = new Pos();
			}
			
			if ($this->posaddress) {
				$this->pos->read($this->posaddress);
			}
			
			return $this->pos;
		}
		
		public function material() {
				
			if (!$this->material) {
				$this->material = new Material($this->connector);
			}
				
			if ($this->material_id) {
				$this->material->read($this->material_id);
			}
				
			return $this->material;
		}
	}