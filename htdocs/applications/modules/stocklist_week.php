<?php

	class Stocklist_Week { 
		
		const DB_BIND_STOCKLISTS = 'INNER JOIN db_retailnet.scpps_stocklists ON scpps_stocklist_id = scpps_stocklist_item_stocklist_id';
		const DB_BIND_ITEMS = 'INNER JOIN db_retailnet.items ON item_id = scpps_stocklist_item_item_id';

		public $data;
		
		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : Connector::DB_CORE;
			$this->model = new Stocklist_Week_Model($connector);
		}
		
		public function __get($key) {
			return $this->data["scpps_week_$key"];
		}
		
		public function read($id) {
			$this->id = $id;
			return $this->data = $this->model->read($id);
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}
		
		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
	}