<?php

	class Iteminformation_Item {

		public $id;

		public $iteminformation_id;

		public $data;

		public function __construct() {
			$this->model = new Iteminformation_Item_Model(Connector::DB_CORE);
		}

		public function setIteminformation($iteminformation_id) {
			$this->iteminformation_id = $iteminformation_id;
		}

		public function read($item) {
			if ($this->iteminformation_id) {
				return $this->model->read($this->iteminformation_id, $item);
			}
		}

		public function create($item) {
			if ($this->iteminformation_id) {
				return $this->model->create($this->iteminformation_id, $item);
			}
		}

		public function delete($item) {
						if ($this->iteminformation_id) {
				return $this->model->delete($this->iteminformation_id, $item);
			}
		}

		public function deleteAll() {
			if ($this->iteminformation_id) {
				return $this->model->deleteAll($this->iteminformation_id);
			}
		}
	}