<?php

	/**
	 * Material Category
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Material_Category {
		
		/**
		 * Material Category ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Material Category Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Material ID
		 * @var integer
		 */
		public $material;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @var object
		 */
		protected $model;
		
		/**
		 * Edit all material categories
		 * @var string
		 */
		const PERMISSION_EDIT = 'can_edit_mps_ltm_categories';
		
		/**
		 * View all material categories
		 * @var string
		 */
		const PERMISSION_VIEW = 'can_view_mps_ltm_categories';
		
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Material_Category_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_material_category_$key"];
		}
		
		public function material($material) {
			$this->material = $material;
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_material_category_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
	
		public static function loader($connector, $filters=null) {
			$model = new Material_Category_Model($connector);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
	}