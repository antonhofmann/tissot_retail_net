<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'];
$_SECTION = $_REQUEST['section'];
$_ID = $_REQUEST['id'];

$_JSON = array();
$model = new Model($_APPLICATION);

$_FIELD_TYPES = array(
	'news_article_confirmed_date' => 'date',
	'news_article_publish_date' => 'date',
	'news_article_desired_publish_date' => 'date',
	'news_article_expiry_date' => 'date'
);

// read article
$article = new Modul($_APPLICATION);
$article->setTable('news_articles');
$article->read($_ID);

// permissions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PERMISSIONS = array(
	'view.all' => user::permission('can_view_all_news_articles'),
	'view.his' => user::permission('can_view_only_his_news_articles'),
	'edit.all' => user::permission('can_edit_all_news_articles'),
	'edit.his' => user::permission('can_edit_only_his_news_articles')
);

$_CAN_VIEW = $_PERMISSIONS['view.all'] || $_PERMISSIONS['view.his'] ? true : false;
$_CAN_EDIT = $_PERMISSIONS['edit.all'] || $_PERMISSIONS['edit.his'] ? true : false;
$_ACCESS_FULL = $_PERMISSIONS['view.all'] || $_PERMISSIONS['edit.all'] ? true : false;
$_ACCESS_LIMITED = !$_ACCESS_FULL && ($_PERMISSIONS['view.his'] || $_PERMISSIONS['edit.his']) ? true : false;



// tracks ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$track = $model->db->prepare("
	INSERT INTO news_article_tracks (
		news_article_track_article_id,
		news_article_track_user_id,
		news_article_track_type_id,
		news_article_track_entity
	) VALUES (?, ?, ?, ?)
");


// submit ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

switch ($_SECTION) {
	
	case 'article-field-change':

		$field = $_REQUEST['field'];
		$value = $_REQUEST['value'];
		$value = $value ?: null;

		if ($value && $_FIELD_TYPES[$field]=='date') {
			$value = date::sql($value);
		}

		// store news author as text
		if ($field=='news_article_author_id') {

			// reset author fields
			$model->db->exec("
				UPDATE news_articles SET
					news_article_author_id = NULL,
					news_article_author_user_id = NULL
				WHERE news_article_id = $_ID
			");

			// for integer value, store retailnet user
			// otherwise store news author id
			if (preg_match('/^\d+$/', $value)) $field = 'news_article_author_user_id';
			else $value = substr($value, 1);
		}

		$sth = $model->db->prepare("
			UPDATE news_articles SET 
				$field = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE news_article_id = ?
		");

		$_JSON['success'] = $sth->execute(array($value, $user->login, $_ID));

		if ($_JSON['success']) {

			// track: update news article
			$track->execute(array($_ID, $user->id, 32, $field));

			// check for section confirmer
			if ($value && $field=='news_article_category_id' && !$article->data['news_article_confirmed_user_id']) {

				$sth = $model->db->prepare("
					SELECT news_section_confirm_user_id AS id
					FROM news_sections
					INNER JOIN news_categories ON news_category_section_id = news_section_id
					WHERE news_category_id = ?
				");

				$sth->execute(array($value));
				$result = $sth->fetch();

				if ($result['id']) {

					$sth = $model->db->prepare("
						UPDATE news_articles SET 
							news_article_confirmed_user_id = ?
						WHERE news_article_id = ?
					");

					$response = $sth->execute(array($result['id'], $_ID));

					if ($response) {
						
						// track: update news article
						$track->execute(array($_ID, $user->id, 32, 'news_article_confirmed_user_id'));

						$_JSON['data']['news_article_confirmed_user_id'] = $result['id'];
					}
				}
			}
		}

	break;

	case 'article-access':

		$items = $_REQUEST['items'];
		$selected = $_REQUEST['selected'] ?: array();

		if (!$items) {
			$_JSON['success'] = false;
			$_JSON['error'] = "Roles not found";;
		}

		// get article company restrictions
		$sth = $model->db->prepare("
			SELECT GROUP_CONCAT(news_article_role_role_id) AS roles
			FROM news_article_roles
			WHERE news_article_role_article_id = ?
		");

		$sth->execute(array($_ID));
		$result = $sth->fetch();
		$currentRoles = $result['roles'] ? explode(',', $result['roles']) : array();

		$add = $model->db->prepare("
			INSERT INTO news_article_roles (
				news_article_role_article_id,
				news_article_role_role_id,
				user_created,
				date_created
			) VALUES ( ?, ?, ?, NOW())
		");

		$remove = $model->db->prepare("
			DELETE FROM news_article_roles
			WHERE news_article_role_article_id = ? AND news_article_role_role_id = ?
		");

		foreach ($items as $role) {

			$response = false;

			if (!$role) continue; 
			
			// add company restriction
			if (in_array($role, $selected) && !in_array($role, $currentRoles)) {
				$trackType = 43;
				$response = $add->execute(array($_ID, $role, $user->login));
			}

			// remove company restriction
			if (!in_array($role, $selected) && in_array($role, $currentRoles)) {
				$trackType = 44;
				$response = $remove->execute(array($_ID, $role));
			}

			if ($response) {
				$track->execute(array($_ID, $user->id, $trackType, $role));
			}
		}

		$_JSON['success'] = true;

	break;

	case 'company-access':

		$items = $_REQUEST['items'];
		$selected = $_REQUEST['selected'] ?: array();

		if (!$items) {
			$_JSON['success'] = false;
			$_JSON['error'] = "Companies not found";;
		}

		// get article company restrictions
		$sth = $model->db->prepare("
			SELECT GROUP_CONCAT(news_article_address_address_id) AS companies
			FROM news_article_addresses
			WHERE news_article_address_article_id = ?
		");

		$sth->execute(array($_ID));
		$result = $sth->fetch();
		$currentCompanies = $result['companies'] ? explode(',', $result['companies']) : array();

		$add = $model->db->prepare("
			INSERT INTO news_article_addresses (
				news_article_address_article_id,
				news_article_address_address_id,
				user_created,
				date_created
			) VALUES ( ?, ?, ?, NOW())
		");

		$remove = $model->db->prepare("
			DELETE FROM news_article_addresses
			WHERE news_article_address_article_id = ? AND news_article_address_address_id = ?
		");

		foreach ($items as $company) {

			$response = false;

			if (!$company) continue; 
			
			// add company restriction
			if (in_array($company, $selected) && !in_array($company, $currentCompanies)) {
				$trackType = 45;
				$response = $add->execute(array($_ID, $company, $user->login));
			}

			// remove company restriction
			if (!in_array($company, $selected) && in_array($company, $currentCompanies)) {
				$trackType = 46;
				$response = $remove->execute(array($_ID, $company));
			}

			if ($response) {
				$track->execute(array($_ID, $user->id, $trackType, $company));
			}
		}

		$_JSON['success'] = true;

	break;

	case 'add-template':

		$template = $_REQUEST['template'];

		$sth = $model->db->prepare("
			SELECT 
				news_article_template_id AS id,
				news_article_template_file_form AS file,
				news_article_template_container AS container
			FROM news_article_templates
			WHERE news_article_template_id = ?
		");

		$sth->execute(array($template));
		$tpl = $sth->fetch();

		if ($tpl['file']) {
			
			$content = File::load($tpl['file']);
			
			if ($content) {

				$sth = $model->db->prepare("
					SELECT COUNT(news_article_content_id) AS total
					FROM news_article_contents
					WHERE news_article_content_article_id = ?
				");

				$sth->execute(array($_ID));
				$result = $sth->fetch();
				$order = $result['total']+1;

				$data = null;

				if ($template==5) {
					$data = array();
					$data['dataloader']['settings']['colHeaders'][0] = 'A';
					$data['dataloader']['settings']['colHeaders'][1] = 'B';
					$data = serialize($data);
				}

				$sth = $model->db->prepare("
					INSERT INTO news_article_contents (
						news_article_content_article_id,
						news_article_content_template_id,
						news_article_content_data,
						news_article_content_order,
						user_created
					) VALUES (?,?,?,?,?)
				");

				$sth->execute(array($_ID, $template, $data, $order, $user->login));
				
				$id = $model->db->lastInsertId();

				if ($id) {

					// track: add news article template
					$track->execute(array($_ID, $user->id, 39, $template));

					$data = array(
						'article' => $_ID,
						'container' => $tpl['container'],
						'id' => $id
					);

					$content = Content::render($content, $data, true);

					$_JSON['template'] = $content;
					$_JSON['container'] = $tpl['container'];
					$_JSON['id'] = $id;

				} else {
					$_JSON['error'] = "Article template cannot be generated.";
				}
			} else {
				$_JSON['error'] = "Article template does not exist.";
			}
		}

	break;

	case 'article-tpl-field-change':

		$template = $_REQUEST['template'];
		$field = $_REQUEST['field'];
		$value = $_REQUEST['value'];

		if ($field=='media' && $value && !strpos($value,'www')) {
			$value = "https://www.swatch.com/it_it/swatch-tv/embed/$value";
		}

		$sth = $model->db->prepare("
			SELECT * 
			FROM news_article_contents
			WHERE news_article_content_id = ?
		");

		$sth->execute(array($template));
		$result = $sth->fetch();

		if ($result) {

			$data = $result['news_article_content_data'] ? unserialize($result['news_article_content_data']) : array();
			$data[$field] = $value;
			$data = serialize($data);

			$sth = $model->db->prepare("
				UPDATE news_article_contents SET 
					news_article_content_data = ?,
					user_modified = ?,
					date_modified = NOW()
				WHERE news_article_content_id = ?
			");

			$response = $sth->execute(array($data, $user->login, $template));

			if ($response) {
				
				$key = $field=='image' || $field=='file' ? 55 : 40;
				$text = $field=='image' || $field=='file' ? $value : $field;

				// track: update news article template
				$track->execute(array($_ID, $user->id, $key, $text));
			}
		}

		$_JSON['success'] = $response;

	break;

	case 'remove-template':

		$template = $_REQUEST['template'];

		// remove template folder
		dir::remove("/data/news/articles/$_ID/$template");

		$sth = $model->db->prepare("
			DELETE FROM news_article_contents
			WHERE news_article_content_id = ?
		");

		$response = $sth->execute(array($template));

		if ($response) {
			// track: delete news article template
			$track->execute(array($_ID, $user->id, 41, $template));
		}

		$_JSON['success'] = $response;

	break;

	case 'add-tpl-files':

		$path = $_REQUEST['path'];

		if ($path) { 

			$upload_handler = new UploadHandler(array(
				'path' => $path,
				'upload_dir' => $_SERVER['DOCUMENT_ROOT'].$path,
				'upload_url' => '//'.$_SERVER['SERVER_NAME'].$path
			));

			$file = $path.$_FILES['files']['name'][0];
			$template = $_REQUEST['template'];

			$sth = $model->db->prepare("
				SELECT * 
				FROM news_article_contents
				WHERE news_article_content_id = ?
			");

			$sth->execute(array($template));
			$result = $sth->fetch();

			if ($result) {

				$data = $result['news_article_content_data'] ? unserialize($result['news_article_content_data']) : array();
				
				$files = $data['files'] ?: array();
				
				$filename = basename($file);
				$file = str_replace($filename, preg_replace("/[^a-zA-Z0-9.]/", "_", $filename), $file);
				$file = strtolower($file);
				
				$files[] = $file;
				$data['files'] = $files;

				$sth = $model->db->prepare("
					UPDATE news_article_contents SET 
						news_article_content_data = ?,
						user_modified = ?,
						date_modified = NOW()
					WHERE news_article_content_id = ?
				");

				$response = $sth->execute(array(serialize($data), $user->login, $template));

				if ($response) {
					// track: update news article template
					$track->execute(array($_ID, $user->id, 55, $file));
				}
			}
		}

	break;

	case 'title-tpl-files':

		$file = $_REQUEST['file'];
		$title = $_REQUEST['title'];
		$template = $_REQUEST['template'];

		if (!$title) {
			$_JSON['error'] = "File title not defined.";
			break;
		}

		$sth = $model->db->prepare("
			SELECT * 
			FROM news_article_contents
			WHERE news_article_content_id = ?
		");

		$sth->execute(array($template));
		$result = $sth->fetch();

		if ($result) {

			$data = unserialize($result['news_article_content_data']);
			$fileTitles = $data['fileTitles'] ?: array();
			$fileTitles[$file] = $title;
			$data['fileTitles'] = $fileTitles;
			$data = serialize($data);

			$sth = $model->db->prepare("
				UPDATE news_article_contents SET 
					news_article_content_data = ?,
					user_modified = ?,
					date_modified = NOW()
				WHERE news_article_content_id = ?
			");

			$response = $sth->execute(array($data, $user->login, $template));

			if ($response) {
				// track: update news article template file
				$track->execute(array($_ID, $user->id, 55, $file));
			}
			
		}

		$_JSON['success'] = $response;

	break;

	case 'remove-tpl-files':

		$file = $_REQUEST['file'];
		$template = $_REQUEST['template'];

		$sth = $model->db->prepare("
			SELECT * 
			FROM news_article_contents
			WHERE news_article_content_id = ?
		");

		$sth->execute(array($template));
		$result = $sth->fetch();

		if ($result) {

			$data = unserialize($result['news_article_content_data']);
			
			$files = $data['files'] ?: array();
			$key = array_search($file, $files);
			unset($files[$key]);
			
			$fullpath = $_SERVER['DOCUMENT_ROOT'].$file;

			if (file_exists($fullpath)) {
				@chmod($fullpath, 0666);
				@unlink($fullpath);
			}

			if ($data['fileTitles'] && $data['fileTitles'][$file]) {
				unset($data['fileTitles'][$file]);
			}

			if (!$files) unset($data['files']);
			else $data['files'] = $files;

			$data = serialize($data);

			$sth = $model->db->prepare("
				UPDATE news_article_contents SET 
					news_article_content_data = ?,
					user_modified = ?,
					date_modified = NOW()
				WHERE news_article_content_id = ?
			");

			$response = $sth->execute(array($data, $user->login, $template));

			if ($response) {
				// track: update news article template
				$track->execute(array($_ID, $user->id, 56, $file));
			}
		}

		$_JSON['success'] = $response;

	break;

	case 'template-sort':

		$sort = $_REQUEST['sort'];

		if (is_array($sort)) {

			$sth = $model->db->prepare("
				UPDATE news_article_contents SET
					news_article_content_order = ?
				WHERE news_article_content_id = ?
			");

			foreach ($sort as $order => $template) {
				$i = $order+2;
				$sth->execute(array($i, $template));
			}

			// track: sort news article content
			$track->execute(array($_ID, $user->id, 55, serialize($sort)));
		}

	break;


	case 'workflow-states':

		$sth = $model->db->prepare("
			SELECT news_workflow_state_id, news_workflow_state_name
			FROM news_workflow_states
			ORDER BY news_workflow_state_name, news_workflow_state_name
		");

		$sth->execute();
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$id = $row['news_workflow_state_id'];
				$_JSON[$id] = $row['news_workflow_state_name'];
			}
		}

	break;
}

if ($_JSON) {
	header('Content-Type: text/json');
	echo json_encode($_JSON);
}