<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

ob_start();

define('DEBUGGING_MODE', false);

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'];
$_ID = $_REQUEST['id'];

$_JSON = array();
$_ERRORS = array();
$_SUCCESSES = array();

$model = new Model($_APPLICATION);

// article
$article = new Modul($_APPLICATION);
$article->setTable('news_articles');
$article->read($_ID);

$_OWNER = $article->data['news_article_owner_user_id'];

// permissions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PERMISSIONS = array(
	'view.all' => user::permission('can_view_all_news_articles'),
	'view.his' => user::permission('can_view_only_his_news_articles'),
	'edit.all' => user::permission('can_edit_all_news_articles'),
	'edit.his' => user::permission('can_edit_only_his_news_articles'),
	'confirm.articles' => user::permission('can_confirm_news_article')
);

$_CAN_EDIT = $_PERMISSIONS['edit.all'] || $_PERMISSIONS['edit.his'] || $_PERMISSIONS['confirm.articles'] ? true : false;

if ($article->data['news_article_confirmed_user_id']) {
	$_CONFIRMER = $article->data['news_article_confirmed_user_id'];
}
elseif ($user->id && $_PERMISSIONS['edit.all']) {
	$_CONFIRMER = $user->id;
}

// checkin :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_CAN_EDIT) {
	$_ERRORS[] = "You don't have permissions to submit this action.";
}

if (!$article->id) {
	$_ERRORS[] = "Article is not defined.";
}

if (!$_CONFIRMER) {
	$_ERRORS[] = "You don't have permissions to approve this article.";
}

if (!$article->data['news_article_active']) {
	$_ERRORS[] = "Inactive article cannot be approved. Please set article as active.";
}


// track :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_TRACK_TYPE_APPROVED = 35;
$_TRACK_TYPE_PUBLISHED = 36;

$track = $model->db->prepare("
	INSERT INTO news_article_tracks (
		news_article_track_article_id,
		news_article_track_user_id,
		news_article_track_type_id,
		news_article_track_entity
	) VALUES (?, ?, ?, ?)
");

// submit ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {

	$updateArticle = $model->db->prepare("
		UPDATE news_articles SET 
			news_article_publish_state_id = ?,
			news_article_rejected = NULL,
			user_modified = ?,
			date_modified = NOW()
		WHERE news_article_id = ?
	");

	$response = DEBUGGING_MODE ? true : $updateArticle->execute(array(3, $user->login, $_ID));

	if ($response) {

		if (!DEBUGGING_MODE) {

			$state = 3;
			$message = "Article is approved.";

			// track state change
			$track->execute(array($_ID, $user->id, $_TRACK_TYPE_APPROVED, null));

			// send mail to owner
			$mail = new ActionMail(39);
			
			$mail->addRecipient($_OWNER);
			
			$dataloader = $article->data;
			$dataloader['link'] = 'http://'.$_SERVER['SERVER_NAME']."/gazette/articles/article/$_ID";
			
			$mail->setDataloader($dataloader);

			// get all chief editors
			$sth = $model->db->prepare("
				SELECT DISTINCT 
					user_id AS id,
					db_retailnet.users.user_email AS email
				FROM db_retailnet.users
				INNER JOIN db_retailnet.user_roles ON user_role_user = user_id
				WHERE user_active = 1 AND user_role_role = 71
			");

			$sth->execute();
			$result = $sth->fetchAll();
			$_CHIEFS = _array::datagrid($result);

			$ccRecipients = array();

			// if owner is editor i chief
			// send CC mail to  all chief editors
			if ($_CHIEFS[$_OWNER]) {
				foreach ($_CHIEFS as $id => $row) {
					if ($id != $_OWNER) {
						$ccRecipients[] = $row['email'];
					}
				}
			}

			// add cc recipeints
			if ($ccRecipients) {
				$mail->addCCRecipients($ccRecipients);
			}
			
			$mail->send();
		}

		// if article belong to newsletters
		// upgrade article state to published/archived
		$sth = $model->db->prepare("
			SELECT DISTINCT
				newsletter_id AS id,
				newsletter_publish_state_id AS state
			FROM newsletter_articles
			INNER JOIN newsletters ON newsletter_id = newsletter_article_newsletter_id
			WHERE newsletter_article_article_id = ?	
		");

		$sth->execute(array($article->id));
		$result = $sth->fetchAll();

		if ($result) {

			$newsletterPublished = false;
			$newsletterArchived = false;

			foreach ($result as $row) {
				
				// is newsletter published
				if (!$newsletterPublished && $row['state']==3) {
					$newsletterPublished = true;
				}

				// is newsletter published
				if (!$newsletterArchived && $row['state']==4) {
					$newsletterArchived = true;
				}
			}

			// set article as published
			if ($newsletterPublished) {
				
				$state = 4;
				$message = "Article is published.";
				$updateArticle->execute(array($state, $user->login, $_ID));
				
				// track: set news article as published
				$track->execute(array($_ID, $user->id, $_TRACK_TYPE_PUBLISHED, null));
				
				$sth = $model->db->prepare("
					UPDATE news_articles SET 
						news_article_publish_date = CURDATE()
					WHERE news_article_id = ?
				");

				$sth->execute(array($_ID));
			}

			// set article as archived
			if (!$newsletterPublished && $newsletterArchived) {
				$state = 5;
				$message = "Article is archived.";
				$updateArticle->execute(array($state, $user->login, $_ID));
				$tracker->execute(array($_ID, $user->id, $message, $state));
			}
		}

		$_JSON['state'] = $state;
		$_JSON['success'] = true;
		$_SUCCESSES[] = $message;
		
		$_JSON['stateName'] = "Approved";
		$_JSON['panel'] = 'panel-success';
		$_JSON['remove'][] = '#btnReject';

		if ($state==4 || $state==5) {
			$_JSON['remove'][] = '#btnDelete';
		}

		// update confirmation
		$sth = $model->db->prepare("
			UPDATE news_articles SET 
				news_article_confirmed_user_id = ?,
				news_article_confirmed_date = CURRENT_DATE(),
				user_modified = ?,
				date_modified = NOW()
			WHERE news_article_id = ?
		");

		if (!DEBUGGING_MODE) {
			$sth->execute(array($user->id, $user->login, $_ID));
		}
	}
}
	
// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_ERRORS) {
	$_JSON['notifications'][] = array(
		'type'=>'error', 
		'title'=>'Error', 
		'text' => join('<br />', $_ERRORS)
	);
}

if ($_SUCCESSES) {
	$_JSON['notifications'][] = array(
		'type'=>'success', 
		'title'=>'Success', 
		'text' => join('<br />', $_SUCCESSES)
	);
}

ob_end_clean();
header('Content-Type: text/json');
echo json_encode($_JSON);
