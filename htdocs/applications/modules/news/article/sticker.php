<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$application = $_REQUEST['application'];
$article = $_REQUEST['article'];

$response = array();
$error = false;

$article = new Modul($application);
$article->setTable('news_articles');
$article->read($_REQUEST['article']); 

if (!Request::isAjax() || !$user->id || !$article->id) {
	$response['notifications'][] = array(
		'type' => 'error',
		'title' => 'Error',
		'text' => 'Bad request.'
	);
	goto BLOCK_RESPONSE;
}

switch ($_REQUEST['section']) {
	
	case 'get':

		$data = $article->data['news_article_sticker'];
		$data = $data ? unserialize($data) : array();
		$response['data'] = $data;
		
	break;

	case 'submit':

		$dataSticker = $article->data['news_article_sticker'];
		$dataSticker = $dataSticker ? unserialize($dataSticker) : array();

		if ($_REQUEST['sticker']) {
			
			$data['news_article_sticker'] = serialize(array_merge($dataSticker, array_filter(array(
				'id' => $_REQUEST['sticker'],
				'width' => $_REQUEST['width'],
				'height' => $_REQUEST['height'],
				'left' => $_REQUEST['left'],
				'top' => $_REQUEST['top'],
				'background-image' => $_REQUEST['background-image']
			))));

			$response['success'] = $article->update($data);
		} else {
			$response['notifications'][] = array(
				'type' => 'error',
				'title' => 'Error',
				'text' => 'Bad request. Sticker ID undefined.'
			);
		}
		
	break;

	case 'delete':

		$data['news_article_sticker'] = null;
		$response['success'] = $article->update($data);
		
	break;

	default:
		$response['notifications'][] = array(
			'type' => 'error',
			'title' => 'Error',
			'text' => 'Bad request. Section undefined.'
		);
	break;
}

BLOCK_RESPONSE:

ob_clean();
header('Content-Type: text/json');
echo json_encode($response);