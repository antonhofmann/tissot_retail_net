<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

ob_start();

define('DEBUGGING_MODE', false);

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'];
$_ID = $_REQUEST['id'];
$_COMMENT = $_REQUEST['reject_comment'];

$_JSON = array();
$_ERRORS = array();
$_SUCCESSES = array();

$model = new Model($_APPLICATION);

// article
$article = new Modul($_APPLICATION);
$article->setTable('news_articles');
$article->read($_ID);

$_OWNER = $article->data['news_article_owner_user_id'];

// permissions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PERMISSIONS = array(
	'view.all' => user::permission('can_view_all_news_articles'),
	'view.his' => user::permission('can_view_only_his_news_articles'),
	'edit.all' => user::permission('can_edit_all_news_articles'),
	'edit.his' => user::permission('can_edit_only_his_news_articles'),
	'confirm.articles' => user::permission('can_confirm_news_article')
);

// checkin :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_PERMISSIONS['edit.all'] and ! $_PERMISSIONS['confirm.articles']) {
	$_ERRORS[] = "You don't have permissions to submit this action.";
}

if (!$article->id) {
	$_ERRORS[] = "Article is not defined.";
}

if ($article->data['news_article_publish_state_id'] <> 2) {
	$_ERRORS[] = "The article can be rejected only in completed state.";
}


// track :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$track = $model->db->prepare("
	INSERT INTO news_article_tracks (
		news_article_track_article_id,
		news_article_track_user_id,
		news_article_track_type_id,
		news_article_track_entity,
	)
	VALUES (?,?,?,?)
");

// submit ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {

	// set article as rejected
	$sth = $model->db->prepare("
		UPDATE news_articles SET 
			news_article_publish_state_id = 1,
			news_article_rejected = ?,
			user_modified = ?,
			date_modified = NOW()
		WHERE news_article_id = ?
	");

	$_RESPONSE = DEBUGGING_MODE ? true : $sth->execute(array($user->id, $user->login, $_ID));

	if ($_RESPONSE) {

		// track: set news article as rejected
		$track->execute(array($_ID, $user->id, 38, null));

		$comment = $model->db->prepare("
			INSERT INTO news_article_comments (
				news_article_comment_article_id,
				news_article_comment_user_id,
				news_article_comment_text
			)
			VALUES (?,?,?)
		");

		// add news article comment
		$addComment = DEBUGGING_MODE ? true : $comment->execute(array($_ID, $user->id, $_COMMENT));

		// track: add news article comment
		if ($addComment && !DEBUGGING_MODE) {
			$track->execute(array($_ID, $user->id, 51, null));
		}
		
		$_SUCCESSES[] = "The Article is rejected.";
		$_JSON['success'] = true;
		$_JSON['state'] = 1;
		$_JSON['remove'][] = '#btnConfirm';

	} else {
		$_ERRORS[] = "The Article is not rejected. Please contact system administrator.";
		$_JSON['db'] = $sth->errorInfo();;
	}
}


// send mail to author :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_RESPONSE && $_OWNER) {

	$mail = new ActionMail(38);
	
	$mail->addRecipient($_OWNER);
	
	$dataloader = $article->data;
	$dataloader['link'] = 'http://'.$_SERVER['SERVER_NAME']."/gazette/articles/article/$_ID";
	$dataloader['comment'] = $_COMMENT;
	
	$mail->setDataloader($dataloader);

	// get all chief editors
	$sth = $model->db->prepare("
		SELECT DISTINCT 
			user_id AS id,
			db_retailnet.users.user_email AS email
		FROM db_retailnet.users
		INNER JOIN db_retailnet.user_roles ON user_role_user = user_id
		WHERE user_active = 1 AND user_role_role = 71
	");

	$sth->execute();
	$result = $sth->fetchAll();
	$_CHIEFS = _array::datagrid($result);

	// if owner is editor in chief
	// send cc mail to all chief editors
	if ($_CHIEFS[$_OWNER]) {
		foreach ($_CHIEFS as $id => $row) {
			if ($id != $_OWNER) {
				$ccRecipients[] = $row['email'];
			}
		}
	}

	// send email to user who has article completed
	$sth = $model->db->prepare("
		SELECT news_article_track_user_id AS id
		FROM news_article_tracks
		WHERE news_article_track_article_id = ? AND news_article_track_type_id = 34 
		ORDER BY news_article_track_id DESC
		LIMIT 1
	");

	$sth->execute(array($_ID));
	$result = $sth->fetch();
	
	if ($result['id'] && $result['id'] <> $_OWNER && $result['id'] <> $user->id) {
		$mail->addRecipient($result['id']);
	}

	$sendmail = DEBUGGING_MODE ? true : $mail->send();
}

// actions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_RESPONSE) {
	
	// button complete
	if ($_OWNER==$user->id) {
		$_JSON['buttons'][] = array(
			'title' => 'Completed',
			'icon' => '<i class="fa fa-check" ></i> ',
			'attributes' => array(
				'id' => 'btnComplete',
				'class' => 'btn btn-sm btn-primary btn-state',
				'href' => '/applications/modules/news/article/complete.php'
			)
		); 
	}
}

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_ERRORS) {
	$_JSON['notifications'][] = array(
		'type'=>'error', 
		'title'=>'Error', 
		'text' => join('<br />', $_ERRORS)
	);
}

if ($_SUCCESSES) {
	$_JSON['notifications'][] = array(
		'type'=>'success', 
		'title'=>'Success', 
		'text' => join('<br />', $_SUCCESSES)
	);
}

ob_end_clean();
header('Content-Type: text/json');
echo json_encode($_JSON);
