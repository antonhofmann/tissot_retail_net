<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$_APPLICATION = $_REQUEST['application'];
$_ARCHIVED = $_REQUEST['archived'];
$_SEARCH = $_REQUEST['search'];
$_SECTION = $_REQUEST['section'];

// page load
$_PAGE = $_REQUEST['page'] ?: 1;
$_PAGE_ITEMS = $_REQUEST['items'] ?: 12;
$_OFFSET = ($_PAGE-1) * $_PAGE_ITEMS;

$model = new Model($_APPLICATION);

$_JSON = array();

$_PERMISSIONS = array(
	'view.all' => user::permission('can_view_all_news_articles'),
	'view.his' => user::permission('can_view_only_his_news_articles'),
	'edit.all' => user::permission('can_edit_all_news_articles'),
	'edit.his' => user::permission('can_edit_only_his_news_articles')
);

$_CAN_VIEW = $_PERMISSIONS['view.all'] || $_PERMISSIONS['view.his'] ? true : false;
$_CAN_EDIT = $_PERMISSIONS['edit.all'] || $_PERMISSIONS['edit.his'] ? true : false;
$_ACCESS_FULL = $_PERMISSIONS['view.all'] || $_PERMISSIONS['edit.all'] ? true : false;
$_ACCESS_LIMITED = !$_ACCESS_FULL && ($_PERMISSIONS['view.his'] || $_PERMISSIONS['edit.his']) ? true : false;

$_PANEL_TYPES = array(
	1 => "panel-warning",
	2 => "panel-info",
	3 => "panel-success",
	4 => "panel-primary",
	5 => "panel-default",
	6 => "panel-danger"
);

// load adata ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_FILTERS = array();

$_FILTERS['title'] = "(news_article_title IS NOT NULL OR news_article_title != '')";
$_FILTERS['teasers'] = "news_article_content_template_id = 1";
$_FILTERS['default'] = $_ARCHIVED ? "news_article_publish_state_id = 5" : "(news_article_multiple = 1 OR news_article_publish_state_id < 5)";

if (!$_ACCESS_FULL) {
	$_FILTERS['limited'] = "(news_article_owner_user_id = $user->id OR news_article_confirmed_user_id = '$user->id' )";
}

if ($_SEARCH && $_ARCHIVED) {
	$_FILTERS['search'] = "(
		news_article_title LIKE '%$_SEARCH%'
		OR news_workflow_state_name LIKE '%$_SEARCH%'
		OR news_section_name LIKE '%$_SEARCH%'
		OR news_article_content_data LIKE '%$_SEARCH%'
	)";
}



if ($_SECTION) {
	$_FILTERS['section'] = "news_section_id = $_SECTION";
}
elseif(array_key_exists('filters', $_SESSION))
{
	$_session_filters = unserialize($_SESSION['filters']);
	if (array_key_exists('news', $_session_filters)
	and array_key_exists('articles.archive', $_session_filters['news'])
	and array_key_exists('section', $_session_filters['news']['articles.archive'])
	and $_session_filters['news']['articles.archive']['section'] > 0
	)
	{
		$_FILTERS['section'] = "news_section_id = " . $_session_filters['news']['articles.archive']['section'];
	}
}

$_FILTERS = join(' AND ', $_FILTERS);
$_LIMIT = $_ARCHIVED ? "LIMIT $_OFFSET, $_PAGE_ITEMS" : null;
$_CAN_LOAD = !$_LIMIT && $_PAGE > 1 ? false : true;

if ($_CAN_LOAD) {
	$sth = $model->db->prepare("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			news_article_id AS id, 
			news_article_owner_user_id AS owner,
			news_article_title AS title, 
			news_article_active AS active, 
			news_article_publish_state_id AS state,
			news_workflow_state_name AS state_name, 
			LOWER(REPLACE(news_workflow_state_name, ' ', '-')) AS state_filter,
			news_section_name AS section_name,
			LOWER(REPLACE(news_section_name, ' ', '-')) AS section_filter,
			DATE_FORMAT(news_articles.date_created, '%d.%m.%Y') AS created_on,
			UNIX_TIMESTAMP(news_articles.date_created) AS created_on_stamp,
			DATE_FORMAT(news_article_expiry_date, '%d.%m.%Y') AS expiry_on,
			UNIX_TIMESTAMP(news_article_expiry_date) AS expiry_on_stamp,
			news_article_content_data as teaser,
			news_article_confirmed_user_id AS confirmer,
			news_article_featured AS featured,
			news_article_rejected AS rejected
		FROM news_articles 
		INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
		INNER JOIN news_workflow_states ON news_article_publish_state_id = news_workflow_state_id	 
		LEFT JOIN news_categories ON news_article_category_id = news_category_id
		LEFT JOIN news_sections ON news_category_section_id = news_section_id
		WHERE $_FILTERS
		ORDER BY news_articles.news_article_title
		$_LIMIT 
	");

	$sth->execute();
	$result = $sth->fetchAll();
}

$articles = array();

if ($result) {

	$rejector = new User();

	foreach ($result as $row) {
		
		$id = $row['id'];
		$state = $row['state'];

		$_OWNER = $row['news_article_owner_user_id']==$user->id ? true : false;
		$_CONFIRMER = $_PERMISSIONS['edit.all'] || $user->id==$row['news_article_confirmed_user_id'] ? true : false;
		$_CAN_EDIT = $_ACCESS_LIMITED && !$_OWNER ? false : $_CAN_EDIT;

		$article = array();
		$article['id'] = $row['id'];
		$article['title'] = $row['title'];
		$article['type'] = $row['rejected'] ? $_PANEL_TYPES[6] : $_PANEL_TYPES[$state];
		$article['link'] = $_CAN_VIEW || $_CAN_EDIT ? "/gazette/articles/article/$id" : '#';
		$article['state'] = $row['state_name'];
		$article['section'] = $row['section_name'];
		$article['teaser'] = $row['teaser'] ? unserialize($row['teaser']) : array();

		unset($article['teaser']['files']);

		// filter
		$article['filter'] = $row['section_filter'].' '.$row['state_filter'];

		// created on
		$article['info'][] = array(
			'label' => 'Created On:',
			'class' => 'created',
			'sort' => $row['created_on_stamp'],
			'title' => $row['created_on']
		);

		// expired on
		if ($row['expiry_on_stamp']) {
			$article['info'][] = array(
				'label' => 'Expired On:',
				'class' => 'expired',
				'sort' => $row['expiry_on_stamp'],
				'title' => $row['expiry_on']
			);
		}

		// section
		if ($row['section_name']) {
			$article['info'][] = array(
				'label' => 'Section:',
				'class' => 'section',
				'title' => $row['section_name']
			);
		}

		// state
		$article['info'][] = array(
			'label' => 'Current State:',
			'class' => 'state',
			'title' => $row['state_name']
		);

		if ($row['rejected']) {
			
			$rejector->read($row['rejected']);

			$article['info'][] = array(
				'label' => 'Rejected from:',
				'class' => 'rejected',
				'title' => "$rejector->firstname $rejector->name"
			);
		}

		$options = array_filter(array(
			$row['featured'] ? 'Featured' : null,
			$row['mutiple'] ? 'Multiple' : null
		));

		// featured
		if ($options) {
			
			$options = join(', ', $options);
			
			$article['info'][] = array(
				'label' => 'Options:',
				'class' => strtolower($options),
				'title' => $options
			);
		}
		
		// button complete
		if ($_CAN_EDIT && $state==1 && $_OWNER) {
			$article['buttons'][] = array(
				'type' => 'primary',
				'icon' => 'check-circle',
				'title' => 'Complete',
				'state' => 2
			);
		}

		// button confirm
		if ($_CAN_EDIT && $state==2 && $_CONFIRMER) {

			$article['button'] = array(
				'type' => 'info',
				'icon' => 'thumbs-up',
				'title' => 'Approve',
				'url' => '/applications/modules/news/article/confirm.php',
				'state' => 3
			);
		}

		$_JSON[] = $article;
	}
}

header('Content-Type: text/json');
echo json_encode($_JSON);
