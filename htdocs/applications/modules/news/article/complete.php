<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

ob_start();

define('DEBUGGING_MODE', false);

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'];
$_ID = $_REQUEST['id'];

$_JSON = array();
$_ERRORS = array();
$_SUCCESSES = array();
$_CONSOLE = array();

$model = new Model($_APPLICATION);

// article
$article = new Modul($_APPLICATION);
$article->setTable('news_articles');
$article->read($_ID);

$_CONFIRMER = $article->data['news_article_confirmed_user_id'];

// permissions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PERMISSIONS = array(
	'view.all' => user::permission('can_view_all_news_articles'),
	'view.his' => user::permission('can_view_only_his_news_articles'),
	'edit.all' => user::permission('can_edit_all_news_articles'),
	'edit.his' => user::permission('can_edit_only_his_news_articles')
);


$_CAN_EDIT = $_PERMISSIONS['edit.all'] || $_PERMISSIONS['edit.his'] ? true : false;
$_OWNER = $user->id==$article->data['news_article_owner_user_id'] ? true : false;


// checkin :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_CAN_EDIT) {
	$_ERRORS[] = "You don't have permissions to submit this action.";
}

if (!$article->id) {
	$_ERRORS[] = "Article is not defined.";
}


if (!$article->data['news_article_category_id']) {
	$_ERRORS[] = "Please select a category.";
}


// tracks ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$track = $model->db->prepare("
	INSERT INTO news_article_tracks (
		news_article_track_article_id,
		news_article_track_user_id,
		news_article_track_type_id,
		news_article_track_entity
	) VALUES (?, ?, ?, ?)
");

// submit ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		UPDATE news_articles SET 
			news_article_publish_state_id = 2,
			news_article_rejected = NULL,
			user_modified = ?,
			date_modified = NOW()
		WHERE news_article_id = ?
	");

	$response = DEBUGGING_MODE ? true : $sth->execute(array($user->login, $_ID));

	if ($response) {
		
		$_SUCCESSES[] = "The Article is completed.";
		$_JSON['success'] = true;
		$_JSON['state'] = 2;


		if (!DEBUGGING_MODE) {

			$track->execute(array($_ID, $user->id, 34, null));

			$mail = new ActionMail(40);

			// mail dataloader
			$dataloader = $article->data;
			$dataloader['link'] = 'http://'.$_SERVER['SERVER_NAME']."/gazette/articles/article/$_ID";
			
			$mail->setDataloader($dataloader);

			// get all chief editors
			$sth = $model->db->prepare("
				SELECT DISTINCT 
					user_id AS id,
					db_retailnet.users.user_email AS email
				FROM db_retailnet.users
				INNER JOIN db_retailnet.user_roles ON user_role_user = user_id
				WHERE user_active = 1 AND user_role_role = 71
			");

			$sth->execute();
			$result = $sth->fetchAll();
			$_CHIEFS = _array::datagrid($result);

			if ($_CONFIRMER) {

				// add confirmer as recipient
				$mail->addRecipient($_CONFIRMER);

				$ccRecipients = array();

				// if confirmer is editor i chief
				// send CC mail to  all chief editors
				if ($_CHIEFS[$_CONFIRMER]) {
					foreach ($_CHIEFS as $id => $row) {
						if ($id != $_CONFIRMER) {
							$ccRecipients[] = $row['email'];
						}
					}
				}

				// add cc recipeints
				if ($ccRecipients) {
					$mail->addCCRecipients($ccRecipients);
				}

			} else {

				// send mails to all chief editors
				if ($_CHIEFS) {
					foreach ($_CHIEFS as $id => $row) {
						$mail->addRecipient($id);
					}
				}
			}

			$mail->send();
		}

		// button reject
		if ( $_PERMISSIONS['edit.all'] && !$_OWNER ) {
			$_JSON['buttons'][] = array(
				'title' => 'Reject',
				'icon' => '<i class="fa fa-thumbs-down" ></i> ',
				'attributes' => array(
					'id' => 'btnReject',
					'class' => 'btn btn-sm btn-primary',
					'href' => '/applications/modules/news/article/reject.php'
				)
			);
		}

		// button confirm
		if ($_PERMISSIONS['edit.all']) {
			$_JSON['buttons'][] = array(
				'title' => 'Approve',
				'type' => 'btn-state btn-info',
				'icon' => '<i class="fa fa-thumbs-up" ></i> ',
				'attributes' => array(
					'id' => 'btnConfirm',
					'class' => 'btn btn-sm btn-info btn-state',
					'href' => '/applications/modules/news/article/confirm.php'
				)
			);
		}

	} else $_ERRORS[] = "The Article is not completed. Please contact system administrator.";
}


// resonding :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_ERRORS) {
	$_JSON['notifications'][] = array(
		'type'=>'error', 
		'title'=>'Error', 
		'text' => join('<br />', $_ERRORS)
	);
}

if ($_SUCCESSES) {
	$_JSON['notifications'][] = array(
		'type'=>'success', 
		'title'=>'Success', 
		'text' => join('<br />', $_SUCCESSES)
	);
}

ob_end_clean();
header('Content-Type: text/json');
echo json_encode($_JSON);
