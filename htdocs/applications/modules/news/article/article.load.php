<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc'; 

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'];
$_ID = $_REQUEST['id'];

$_JSON = array();

$model = new Model($_APPLICATION);

// permissions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PERMISSIONS = array(
	'view.all' => user::permission('can_view_all_news_articles'),
	'view.his' => user::permission('can_view_only_his_news_articles'),
	'edit.all' => user::permission('can_edit_all_news_articles'),
	'edit.his' => user::permission('can_edit_only_his_news_articles')
);

$_CAN_VIEW = $_PERMISSIONS['view.all'] || $_PERMISSIONS['view.his'] ? true : false;
$_CAN_EDIT = $_PERMISSIONS['edit.all'] || $_PERMISSIONS['edit.his'] ? true : false;
$_ACCESS_FULL = $_PERMISSIONS['view.all'] || $_PERMISSIONS['edit.all'] ? true : false;
$_ACCESS_LIMITED = !$_ACCESS_FULL && ($_PERMISSIONS['view.his'] || $_PERMISSIONS['edit.his']) ? true : false;


// article :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT 
		news_article_id,
		news_article_owner_user_id,
		news_article_title,
		news_article_author_id,
		news_article_author_user_id,
		news_article_category_id,
		news_article_publish_state_id,
		news_article_confirmed_date,
		news_article_confirmed_user_id,
		news_article_contact_information,
		news_article_featured,
		news_article_active,
		user_created,
		user_modified,
		DATE_FORMAT(news_article_desired_publish_date, '%d.%m.%Y') AS news_article_desired_publish_date,
		DATE_FORMAT(news_article_publish_date, '%d.%m.%Y') AS news_article_publish_date,
		DATE_FORMAT(news_article_expiry_date, '%d.%m.%Y') AS news_article_expiry_date,
		DATE_FORMAT(news_article_confirmed_date, '%d.%m.%Y') AS news_article_confirmed_date,
		DATE_FORMAT(date_created, '%d.%m.%Y') AS date_created,
		DATE_FORMAT(date_modified, '%d.%m.%Y') AS date_modified,
		news_article_multiple,
		news_article_new,
		news_article_sticker AS sticker
	FROM news_articles
	WHERE news_article_id = ?
");

$sth->execute(array($_ID));
$article = $sth->fetch();

$state = $article['news_article_publish_state_id'];

// news author id
if ($article['news_article_author_id']) {
	$article['news_article_author_id'] = 'n'.$article['news_article_author_id'];
}

// user author id
if ($article['news_article_author_user_id']) {
	$article['news_article_author_id'] = $article['news_article_author_user_id'];
}

// owner
$owner = new User();
$owner->read($article['news_article_owner_user_id']);
$article['owner'] = "$owner->firstname $owner->name";

if ($article['sticker']) {
	$_JSON['sticker'] = unserialize($article['sticker']);
	unset($article['sticker']);
}

// form fields
$_JSON['article'] = $article;
$fields = $article ? array_keys($article) : array();


// access visibility :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_CHIEF = $_PERMISSIONS['edit.all'] ? true : false;
$_OWNER = $user->id==$article['news_article_owner_user_id'] ? true : false;
$_CAN_EDIT = $_ACCESS_LIMITED && !$_OWNER ? false : $_CAN_EDIT;
$_CAN_CONFIRME = $_PERMISSIONS['edit.all'] || $article['news_article_confirmed_user_id']==$user->id ? true : false;
$_CAN_DELETE = $_PERMISSIONS['edit.all'] || ($_CAN_EDIT && $_OWNER) ? true : false;
$_DISABLE_ALL = !$_ACCESS_FULL && !$_OWNER ? true : false;

// make possible approval to edit article
if ($user->id == $article['news_article_confirmed_user_id']) {
	$_CAN_EDIT = true;
	$_CAN_CONFIRME = true;
	$_CAN_DELETE = true;
	$_DISABLE_ALL = false;
}

// disable all fields
if ($_DISABLE_ALL) {
	$_JSON['disabled'] = $fields ? array_fill_keys($fields, true) : array();
	$_JSON['remove'] = array('#template-icons', '.fileinput-button', '.tpl-file-remover', '.tpl-remover');
}

// disable confirmator
if ($article['news_article_confirmed_date']) {
	//$_JSON['disabled']['news_article_confirmed_user_id'] = true;
}

if (!$_CHIEF) {
	$_JSON['remove'][] = '#row_news_article_featured';
	$_JSON['remove'][] = '#row_news_article_active';
	$_JSON['remove'][] = '#row_news_article_multiple';
	$_JSON['remove'][] = '#row_news_article_new';
}

$_JSON['disabled']['news_article_publish_date'] = true;


// get article content :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT 
		news_article_content_id AS id,
		news_article_content_data AS data,
		news_article_template_file_form AS file,
		news_article_template_container AS container
	FROM news_article_contents
	INNER JOIN news_article_templates ON news_article_template_id = news_article_content_template_id
	WHERE news_article_content_article_id = ?
	ORDER BY news_article_content_order
");

$sth->execute(array($_ID));
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$id = $row['id'];
		$content = File::load($row['file']);
	
		if ($content) {

			if ($_DISABLE_ALL) {
				
				$tplFields = Content::match($content);
				
				if ($tplFields) {
					
					array_walk($tplFields, function(&$item) use ($id) { 
						$item = $item.'-'.$id; 
					});
					
					$tplFields = array_fill_keys($tplFields, true);
					$_JSON['disabled'] = array_merge($_JSON['disabled'], $tplFields);
				}
			}
			
			$container = $row['container'];
			
			$data = $row['data'] ? unserialize($row['data']) : array();
			$data['id'] = $id;
			$data['container'] = $row['container'];
			$data['article'] = $_ID;
			
			if ($data['files']) {
				$_JSON['files'][$id] = $data['files'];
				unset($data['files']);
			}

			if ($data['fileTitles']) {
				$_JSON['fileTitles'][$id] = $data['fileTitles'];
				unset($data['fileTitles']);
			}

			if ($data['dataloader']) {
				$_JSON['dataloader'][$id] = $data['dataloader'];
			}

			$content = Content::render($content, $data, true);
			$_JSON['templates'][$container] .= $content;
		} 
	}
}


// article tracks ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT
		UNIX_TIMESTAMP(news_article_tracks.date_created) AS id,
		track_type_id AS type_id,
		track_type_title AS content,
		news_article_track_entity AS entity,
		CONCAT(user_firstname, ' ', user_name) AS name,
		DATE_FORMAT(news_article_tracks.date_created, '%d.%m.%Y %H:%i') AS date,
		CONCAT(address_company, ', ', country_name) AS company
	FROM news_article_tracks
		INNER JOIN db_retailnet.track_types ON track_type_id = news_article_track_type_id
		INNER JOIN db_retailnet.users ON user_id = news_article_track_user_id
		INNER JOIN db_retailnet.addresses ON address_id = user_address
		INNER JOIN db_retailnet.countries ON country_id = address_country
	WHERE news_article_track_article_id = :id
	UNION ALL
	SELECT 
		UNIX_TIMESTAMP(news_article_tracks.date_created) AS id,
		track_type_id AS type_id,
		track_type_title AS content,
		news_article_track_entity AS entity,
		CONCAT(news_recipient_firstname, ' ', news_recipient_name) AS name,
		DATE_FORMAT(news_article_tracks.date_created, '%d.%m.%Y %H:%i') AS date,
		CONCAT('Additional Recipient') AS company
	FROM news_article_tracks
		INNER JOIN db_retailnet.track_types ON track_type_id = news_article_track_type_id
		INNER JOIN news_recipients ON news_recipient_id = news_article_track_additional_user_id
	WHERE news_article_track_article_id = :id
	ORDER BY id DESC
");

$sth->execute(array('id' => $_ID));
$result = $sth->fetchAll();

$tracks = array();

if ($result) {
	
	foreach ($result as $row) {
		
		if ($row['type_id']==48) {
			$file = $row['entity'];
			$row['content'] = "Download <a href='$file' target='_blank' >file</a>";
		}

		$tracks[$row['id']] = $row;
	}
}

$sth = $model->db->prepare("
	SELECT
		UNIX_TIMESTAMP(news_article_comments.date_created) AS id,
		CONCAT(user_firstname, ' ', user_name) AS name,
		news_article_comment_text AS content,
		DATE_FORMAT(news_article_comments.date_created, '%d.%m.%Y %H:%i') AS date
	FROM news_article_comments
	INNER JOIN db_retailnet.users ON user_id = news_article_comment_user_id
	WHERE news_article_comment_article_id = ?
	ORDER BY news_article_comment_id
");

$sth->execute(array($_ID));
$result = $sth->fetchAll();
$comments = $result ? _array::datagrid($result) : array();

$tracks = array_merge($tracks, $comments);
ksort($tracks);
$_JSON['tracks'] = $tracks;


// article actions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// button complete
if ($_CAN_EDIT && $_OWNER && $state==1) {
	$_JSON['buttons'][] = array(
		'title' => 'Completed',
		'icon' => '<i class="fa fa-check" ></i> ',
		'attributes' => array(
			'id' => 'btnComplete',
			'class' => 'btn btn-sm btn-primary btn-state btn-dialog',
			'href' => '/applications/modules/news/article/complete.php',
			'data-message' => 'Did you complete all the fields, in particular the Visibility restrictions Roles and Companies?'
		)
	); 
}

// button confirm
if ($_CAN_CONFIRME && $state==2) {
	
	$_JSON['buttons'][] = array(
		'title' => 'Approve',
		'type' => 'btn-state btn-info',
		'icon' => '<i class="fa fa-thumbs-up" ></i> ',
		'attributes' => array(
			'id' => 'btnConfirm',
			'class' => 'btn btn-sm btn-info btn-state',
			'href' => '/applications/modules/news/article/confirm.php'
		)
	); 

	$_JSON['buttons'][] = array(
		'title' => 'Reject',
		'icon' => '<i class="fa fa-thumbs-down" ></i> ',
		'attributes' => array(
			'id' => 'btnReject',
			'class' => 'btn btn-sm btn-primary',
			'href' => '/applications/modules/news/article/reject.php'
		)
	);
}

// button archive
if ($_CAN_DELETE && $state < 4) {
	$_JSON['buttons'][] = array(
		'title' => 'Delete',
		'icon' => '<i class="fa fa-times" ></i> ',
		'attributes' => array(
			'id' => 'btnDelete', 
			'class' => 'btn btn-sm btn-danger btn-dialog',
			'href' => '/applications/modules/news/article/delete.php'
		)
	); 
}

// button confirm
if ($_CAN_EDIT) {
	$_JSON['buttons'][] = array(
		'title' => 'Copy',
		'icon' => '<i class="fa fa-files-o" ></i> ',
		'attributes' => array(
			'href' => '/applications/modules/news/article/copy.php',
			'class' => 'btn btn-sm btn-warning',
			'id' => 'btnCopy'
		)
	);
}

$_JSON['buttons'][] = array(
	'title' => 'Preview',
	'icon' => '<i class="fa fa-external-link"></i> ',
	'attributes' => array(
		'id' => 'btn-preview',
		'class' => 'btn btn-info preview',
		'href' => "/gazette/articles/preview/$_ID",
		'target' => '_blank'
	)
);


// get settings ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT DISTINCT
		news_article_setting_id AS id,
		setting_property_name AS property,
		news_article_setting_field AS field,
		news_article_setting_value AS value
	FROM news_article_settings
	INNER JOIN db_retailnet.setting_properties ON setting_property_id = news_article_setting_property_id
");

$sth->execute();
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {

		$field = $row['field'];

		switch ($row['property']) {

			case 'maxlength':

				$_JSON['settings'][$field] = array(
					'className' => 'has-popover', 
					'maxlength' => $row['value'],
					'data' => array(
						'toggle' => 'popover',
						'content' => "Max. length ".$row['value']
					)
				);
				
			break;
		}
	}
}


header('Content-Type: text/json');
echo json_encode($_JSON);
