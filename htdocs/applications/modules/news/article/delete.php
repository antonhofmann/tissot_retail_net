<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

ob_start();

$user = User::instance();
$translate = Translate::instance();

// request
$_APPLICATION = $_REQUEST['application'];
$_ID = trim($_REQUEST['id']);

// article
$article = new Modul($_APPLICATION);
$article->setTable('news_articles');
$article->read($_ID);

// helpers
$_ERRORS = array();
$_JSON = array();

$model = new Model($_APPLICATION);

// permissions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PERMISSIONS = array(
	'view.all' => user::permission('can_view_all_news_articles'),
	'view.his' => user::permission('can_view_only_his_news_articles'),
	'edit.all' => user::permission('can_edit_all_news_articles'),
	'edit.his' => user::permission('can_edit_only_his_news_articles')
);

$_CAN_EDIT = $_PERMISSIONS['edit.all'] || $_PERMISSIONS['edit.his'] ? true : false;
$_ACCESS_FULL = $_PERMISSIONS['view.all'] || $_PERMISSIONS['edit.all'] ? true : false;
$_ACCESS_LIMITED = !$_ACCESS_FULL && ($_PERMISSIONS['view.his'] || $_PERMISSIONS['edit.his']) ? true : false;
$_OWNER = $user->id==$article->data['news_article_owner_user_id'] ? true : false;
$_CAN_DELETE = $_OWNER ? true : $_PERMISSIONS['edit.all'];

// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_CAN_EDIT) {
	$_ERRORS[] = "You don't have access to this page.";
}

if (!$article->id) {
	$_ERRORS[] = "Article not found";
}

if (!$_CAN_DELETE) {
	$_ERRORS[] = "Article cannot be deleted. You need owner permission.";
}

if ($article->data['news_article_publish_state_id']>3) {
	$_ERRORS[] = "Published articles cannot be delete.";
}


// track :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$track = $model->db->prepare("
	INSERT INTO news_article_tracks (
		news_article_track_article_id,
		news_article_track_user_id,
		news_article_track_type_id,
		news_article_track_entity,
	)
	VALUES (?,?,?,?)
");


// submit ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {

	// delete article
	$sth = $model->db->prepare("
		DELETE FROM news_articles
		WHERE news_article_id = ?
	");

	$respone = $sth->execute(array($_ID));

	if ($respone) {

		// responding
		$_JSON['success'] = true;
		$_JSON['redirect'] = '/gazette/articles';

		// track: delete news article
		$track->execute(array($_ID, $user->id, 33, null));

		// remove article templates
		$sth = $model->db->prepare("
			DELETE FROM news_article_contents
			WHERE news_article_content_article_id = ?
		");

		$delete = $sth->execute(array($_ID));

		// track: delete news article template
		if ($delete) {
			$track->execute(array($_ID, $user->id, 41, null));
		}

		// remove article role restrictions
		$sth = $model->db->prepare("
			DELETE FROM news_article_roles
			WHERE news_article_role_article_id = ?
		");

		$delete = $sth->execute(array($_ID));

		// track: remove news article role restriction
		if ($delete) {
			$track->execute(array($_ID, $user->id, 44, null));
		}

		// remove article company restrictions
		$sth = $model->db->prepare("
			DELETE FROM news_article_addresses
			WHERE news_article_address_article_id = ?
		");

		$delete = $sth->execute(array($_ID));

		// track: remove news article company restriction
		if ($delete) {
			$track->execute(array($_ID, $user->id, 46, null));
		}

		// remove article comments
		$sth = $model->db->prepare("
			DELETE FROM news_article_comments
			WHERE news_article_comment_article_id = ?
		");

		$delete = $sth->execute(array($_ID));

		// track: remove news article comments
		if ($delete) {
			$track->execute(array($_ID, $user->id, 51, null));
		}

		// remove article files
		dir::remove("/data/news/articles/$_ID", true);
	}
}

$_JSON['errors'] = $_ERRORS;

if ($_ERRORS) {
	foreach ($_ERRORS as $message) {
		$_JSON['notifications'][] = array(
			'type' => 'error',
			'text' => $message
		);
	}
}

ob_end_clean();
header('Content-Type: text/json');
echo json_encode($_JSON);
