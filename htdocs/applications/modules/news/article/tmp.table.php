<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'];
$_SECTION = $_REQUEST['section'];
$_ID = $_REQUEST['id'];
$_TPL = $_REQUEST['tpl'];
$_ROW = $_REQUEST['row'];
$_COL = $_REQUEST['col'];
$_VALUE = $_REQUEST['value'];

$_JSON = array();
$_ERRORS = array();

$model = new Model($_APPLICATION);

// read article
$article = new Modul($_APPLICATION);
$article->setTable('news_articles');
$article->read($_ID);


$sth = $model->db->prepare("
	SELECT * 
	FROM news_article_contents
	WHERE news_article_content_id = ?
");

$sth->execute(array($_TPL));
$_TEMPLATE = $sth->fetch();


// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$article->id) {
	$_ERRORS[] = "Article not found";
	goto BLOCK_RESPONSE;
}


if (!$_TEMPLATE) {
	$_ERRORS[] = "Template not found";
	goto BLOCK_RESPONSE;
}


// permissions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PERMISSIONS = array(
	'view.all' => user::permission('can_view_all_news_articles'),
	'view.his' => user::permission('can_view_only_his_news_articles'),
	'edit.all' => user::permission('can_edit_all_news_articles'),
	'edit.his' => user::permission('can_edit_only_his_news_articles')
);

$_CAN_VIEW = $_PERMISSIONS['view.all'] || $_PERMISSIONS['view.his'] ? true : false;
$_CAN_EDIT = $_PERMISSIONS['edit.all'] || $_PERMISSIONS['edit.his'] ? true : false;
$_ACCESS_FULL = $_PERMISSIONS['view.all'] || $_PERMISSIONS['edit.all'] ? true : false;
$_ACCESS_LIMITED = !$_ACCESS_FULL && ($_PERMISSIONS['view.his'] || $_PERMISSIONS['edit.his']) ? true : false;


switch ($_SECTION) {

	case 'header-save':

		$data = unserialize($_TEMPLATE['news_article_content_data']);
		$data['dataloader']['settings']['colHeaders'][$_COL] = $_VALUE;
		$data = serialize($data);

		$sth = $model->db->prepare("
			UPDATE news_article_contents SET 
				news_article_content_data = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE news_article_content_id = ?
		");

		$_JSON['response'] = $sth->execute(array($data, $user->login, $_TPL));

	break;

	case 'data-save':

		$data = unserialize($_TEMPLATE['news_article_content_data']);
		$data['dataloader']['sources'][$_ROW][$_COL] = $_VALUE;
		$data = serialize($data);

		$sth = $model->db->prepare("
			UPDATE news_article_contents SET 
				news_article_content_data = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE news_article_content_id = ?
		");

		$_JSON['response'] = $sth->execute(array($data, $user->login, $_TPL));

	break;

	case 'remove-row':

		$data = unserialize($_TEMPLATE['news_article_content_data']);
		$sources = $data['dataloader']['sources'];
		$datagrid = array();

		if (!$_ROW || !$sources) {
			goto BLOCK_RESPONSE;
		}

		foreach ($sources as $row => $cols) {
			if ($_ROW <> $row) {
				$datagrid[$row] = $cols;
			}	
		}

		$data['dataloader']['sources'] = $datagrid;
		$data = serialize($data);

		$sth = $model->db->prepare("
			UPDATE news_article_contents SET 
				news_article_content_data = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE news_article_content_id = ?
		");

		$_JSON['response'] = $sth->execute(array($data, $user->login, $_TPL));

	break;

	case 'add-col':

		$_VALUE = $_VALUE ?: ucfirst(spreadsheet::key($_COL+1));

		$data = unserialize($_TEMPLATE['news_article_content_data']);
		
		$data['dataloader']['settings']['colHeaders'][$_COL] = $_VALUE;
		$data['dataloader']['sources'][0][$_COL] = ' ';
		
		$data = serialize($data);

		$sth = $model->db->prepare("
			UPDATE news_article_contents SET 
				news_article_content_data = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE news_article_content_id = ?
		");

		$_JSON['response'] = $sth->execute(array($data, $user->login, $_TPL));

	break;

	case 'remove-col':

		$data = unserialize($_TEMPLATE['news_article_content_data']);
		$sources = $data['dataloader']['sources'];
		$datagrid = array();

		if (!$_COL || !$sources) {
			goto BLOCK_RESPONSE;
		}
			
		foreach ($sources as $row => $cols) {
			
			$_cols = array();

			foreach ($cols as $col => $value) {
				if ($col <> $_COL) {
					$_cols[$col] = $value;
				}
			}

			$datagrid[$row] = $_cols;
		}

		$data['dataloader']['sources'] = $datagrid;

		if ($data['dataloader']['settings'] && $data['dataloader']['settings']['colHeaders']) {
			unset($data['dataloader']['settings']['colHeaders'][$_COL]);
		}

		$data = serialize($data);

		$sth = $model->db->prepare("
			UPDATE news_article_contents SET 
				news_article_content_data = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE news_article_content_id = ?
		");

		$_JSON['response'] = $sth->execute(array($data, $user->login, $_TPL));

	break;
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONSE:

header('Content-Type: text/json');
echo json_encode($_JSON);
