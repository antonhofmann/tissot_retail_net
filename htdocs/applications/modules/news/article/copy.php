<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

ob_start();

define('DEBUGGING_MODE', false);

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'] ?: 'news';
$_ID = $_REQUEST['id'];

$_JSON = array();
$_ERRORS = array();
$_SUCCESSES = array();
$_DATA = array();

$model = new Model($_APPLICATION);

// article
$article = new Modul($_APPLICATION);
$article->setTable('news_articles');
$article->read($_ID);

$articleTitle = $article->data['news_article_title'];

// permissions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PERMISSIONS = array(
	'edit.all' => user::permission('can_edit_all_news_articles'),
	'edit.his' => user::permission('can_edit_only_his_news_articles')
);

$_CAN_EDIT = $_PERMISSIONS['edit.all'] || $_PERMISSIONS['edit.his'] ? true : false;
$_OWNER = $user->id==$article->data['news_article_owner_user_id'] ? true : false;


// checkin :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_CAN_EDIT) {
	$_ERRORS[] = "You don't have permissions to submit this action.";
}

if (!$article->id) {
	$_ERRORS[] = "Article is not defined.";
}


// track article :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$tracker = $model->db->prepare("
	INSERT INTO news_article_tracks (
		news_article_track_article_id,
		news_article_track_user_id,
		news_article_track_type_id,
		news_article_track_entity,
	)
	VALUES (?,?,?,?)
");

// clone article :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		INSERT INTO news_articles (
			news_article_owner_user_id,
			news_article_author_id,
			news_article_author_user_id,
			news_article_category_id,
			news_article_publish_state_id,
			news_article_confirmed_user_id,
			news_article_title,
			news_article_contact_information,
			news_article_featured,
			news_article_multiple,
			news_article_new,
			news_article_active,
			news_article_rejected,
			user_created
		)
		VALUES (?,?,?,?,1,?,?,?,?,?,?,?,?,?)
	");

	$updateData = array(
		$user->id,
		$article->data['news_article_author_id'],
		$article->data['news_article_author_user_id'],
		$article->data['news_article_category_id'],
		$article->data['news_article_confirmed_user_id'],
		$article->data['news_article_title'],
		$article->data['news_article_contact_information'],
		$article->data['news_article_featured'],
		$article->data['news_article_multiple'],
		$article->data['news_article_new'],
		$article->data['news_article_active'],
		$article->data['news_article_rejected'],
		$user->login
	);

	if (DEBUGGING_MODE) {

		$ai = $model->db->prepare("
			SELECT AUTO_INCREMENT AS ai
			FROM  INFORMATION_SCHEMA.TABLES
			WHERE TABLE_SCHEMA = 'db_retailnet_news' AND TABLE_NAME = 'news_articles'
		");

		$ai->execute();
		$res = $ai->fetch();

		$response = true;
		$_NEW_ID = $res['ai'];

	} else {
		$response = $sth->execute($updateData);
		$_NEW_ID = $response ? $model->db->lastInsertId() : null;
	}

	if ($_NEW_ID) {

		$_DATA['article'] = $updateData;
		
		if (!DEBUGGING_MODE) {
			
			// respondin
			$_JSON['success'] = true;
			$_JSON['redirect'] = "/gazette/articles/article/$_NEW_ID";
			
			// session message
			Message::success("The article \"$articleTitle\" is successfully cloned");
			
			// track: create news article
			$tracker->execute(array($_NEW_ID, $user->id, 31, null));
		}

	} else {
		$_ERRORS[] = "Cannot copy article, please conact system administrator.";
	}
}


// clone article content :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT * 
		FROM news_article_contents
		WHERE news_article_content_article_id = ?
	");

	$sth->execute(array($_ID));
	$result = $sth->fetchAll();

	if ($result) {

		$sth = $model->db->prepare("
			INSERT INTO news_article_contents (
				news_article_content_article_id,
				news_article_content_template_id,
				news_article_content_data,
				news_article_content_order,
				user_created
			)
			VALUES (?,?,?,?,?)
		");

		$ai = $model->db->prepare("
			SELECT AUTO_INCREMENT AS ai
			FROM  INFORMATION_SCHEMA.TABLES
			WHERE TABLE_SCHEMA = 'db_retailnet_news' AND TABLE_NAME   = 'news_article_contents'
		");

		$ai->execute();
		$res = $ai->fetch();
		$newContentId = $res['ai'];

		foreach ($result as $row) {

			if ($row['news_article_content_data']) {

				$data = unserialize($row['news_article_content_data']);

				if ($data['image']) {

					$_FILES[] = array(
						'source' => $data['image'],
						'target' => "/data/news/articles/$_NEW_ID/$newContentId/".basename($data['image'])
					);
					
					$data['image'] = "/data/news/articles/$_NEW_ID/$newContentId/".basename($data['image']);
				}

				if ($data['files']) {

					$files = array();

					foreach ($data['files'] as $file) {
						
						$_FILES[] = array(
							'source' => $file,
							'target' => "/data/news/articles/$_NEW_ID/$newContentId/".basename($file)
						);

						$files[] = "/data/news/articles/$_NEW_ID/$newContentId/".basename($file);
					}

					$data['files'] = $files;
				}

				$row['news_article_content_data'] = serialize($data);
			}

			$updateData = array(
				$_NEW_ID,
				$row['news_article_content_template_id'],
				$row['news_article_content_data'],
				$row['news_article_content_order'],
				$user->login
			);

			$response = DEBUGGING_MODE ? true : $sth->execute($updateData);

			$_DATA['content'][] = $updateData;
		
			if ($response) $newContentId++;
		}
	}
}


// clone article role restrictions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT * 
		FROM news_article_roles
		WHERE news_article_role_article_id = ?
	");

	$sth->execute(array($_ID));
	$result = $sth->fetchAll();

	if ($result) {

		$sth = $model->db->prepare("
			INSERT INTO news_article_roles (
				news_article_role_article_id,
				news_article_role_role_id,
				user_created
			)
			VALUES (?,?,?)
		");

		foreach ($result as $row) {
			
			$updateData = array(
				$_NEW_ID,
				$row['news_article_role_role_id'],
				$user->login
			);

			$response = DEBUGGING_MODE ? true : $sth->execute($updateData);
			$_DATA['roles'][] = $updateData;
		}
	}
}


// clone article companies restrictions ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {
		
	$sth = $model->db->prepare("
		SELECT * 
		FROM news_article_addresses
		WHERE news_article_address_article_id = ?
	");

	$sth->execute(array($_ID));
	$result = $sth->fetchAll();

	if ($result) {

		$sth = $model->db->prepare("
			INSERT INTO news_article_addresses (
				news_article_address_article_id,
				news_article_address_address_id,
				user_created
			)
			VALUES (?,?,?)
		");

		foreach ($result as $row) {
			
			$updateData = array(
				$_NEW_ID,
				$row['news_article_address_address_id'],
				$user->login
			);

			$response = DEBUGGING_MODE ? true : $sth->execute($updateData);
			$_DATA['companies'][] = $updateData;
		}
	}
}

// clone article files :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && $_FILES) {

	foreach ($_FILES as $file) {

		$source = $_SERVER['DOCUMENT_ROOT'].$file['source'];
		$target = $_SERVER['DOCUMENT_ROOT'].$file['target'];

		if (file_exists($source)) {

			$dir = pathinfo($target, PATHINFO_DIRNAME);

			if (!DEBUGGING_MODE) {
				Dir::make($dir);
				copy($source, $target);
			}
			
			if (!DEBUGGING_MODE && file_exists($target)) {
				chmod($target, 0666);
			}
		}
	}

	$_DATA['files'][] = $_FILES;
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (DEBUGGING_MODE) {
	$_JSON['errors'] = $_ERRORS;
	$_JSON['success'] = $_SUCCESSES;
	$_JSON['data'] = $_DATA;
	$_JSON['test'] = $_TEST;
} 

if (!DEBUGGING_MODE && $_ERRORS) {
	$_JSON['notifications'][] = array(
		'type'=>'error', 
		'title'=>'Error', 
		'text' => join('<br />', $_ERRORS)
	);
}

if (!DEBUGGING_MODE && $_SUCCESSES) {
	$_JSON['notifications'][] = array(
		'type'=>'success', 
		'title'=>'Success', 
		'text' => join('<br />', $_SUCCESSES)
	);
}

ob_end_clean();
header('Content-Type: text/json');
echo json_encode($_JSON);
