<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$_ID = $_REQUEST['id'];
$_APPLICATION = $_REQUEST['application'];

$model = new Model($_APPLICATION);

// read article
$article = new Modul($_APPLICATION);
$article->setTable('news_articles');
$article->read($_ID);
$_HAS_TITLE = $article->data['news_article_title'] ? true : false;

if (!$article->id) exit;

// article tracker
$track = $model->db->prepare("
	INSERT INTO news_article_tracks (
		news_article_track_article_id,
		news_article_track_user_id,
		news_article_track_type_id,
		news_article_track_entity
	)
	VALUES (?,?,?,?)
");

// check data :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_HAS_DATA = false;

$_ARTICLE_FIELDS = array(
	'news_article_author_id',
	'news_article_author_user_id',
	'news_article_category_id',
	'news_article_confirmed_user_id',
	'news_article_title',
	'news_article_desired_publish_date',
	'news_article_publish_date',
	'news_article_expiry_date',
	'news_article_confirmed_date',
	'news_article_contact_information',
	'news_article_featured',
	'news_article_multiple',
	'news_article_new'
);

// check article fields
foreach ($_ARTICLE_FIELDS as $field) {
	if ($article->data[$field]) {
		$_HAS_DATA = true;
		break;
	}
}

// check article templates
if (!$_HAS_DATA) {
	
	$sth = $model->db->prepare("
		SELECT news_article_content_data AS data
		FROM news_article_contents
		WHERE news_article_content_article_id = ?
	");

	$sth->execute(array($_ID));
	$result = $sth->fetchAll();

	foreach ($result as $row) {
		
		if ($row['data']) {
			
			$data = unserialize($row['data']);

			foreach ($data as $key => $value) {
				if ($value) {
					$_HAS_DATA = true;
					break;
				}
			}
		}
	}
}

// check article company restrictions
if (!$_HAS_DATA) {
	
	$sth = $model->db->prepare("
		SELECT COUNT(news_article_address_id) AS total
		FROM news_article_addresses
		WHERE news_article_address_article_id = ?
	");

	$sth->execute(array($_ID));
	$result = $sth->fetch();
	$_HAS_DATA = $result['total'] > 0 ? true : false;
}

// check article role restrictions
if (!$_HAS_DATA) {
	
	$sth = $model->db->prepare("
		SELECT COUNT(news_article_role_id) AS total
		FROM news_article_roles
		WHERE news_article_role_article_id = ?
	");

	$sth->execute(array($_ID));
	$result = $sth->fetch();
	$_HAS_DATA = $result['total'] > 0 ? true : false;
}

// remove article ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_HAS_DATA) {

	$sth = $model->db->prepare("
		DELETE FROM news_articles
		WHERE news_article_id = ?
	");

	$response = $sth->execute(array($_ID));

	if ($response) {

		// track: delete news article
		$track->execute(array($_ID, $user->id, 33, null));

		$sth = $model->db->prepare("
			DELETE FROM news_article_contents
			WHERE news_article_content_article_id = ?
		");

		$sth->execute(array($_ID));
	}

} 

// update article title ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_HAS_DATA && !$_HAS_TITLE) {

	$sth = $model->db->prepare("
		UPDATE news_articles SET 
			news_article_title = 'Untitled Article'
		WHERE news_article_id = ?
	");

	$response = $sth->execute(array($_ID));

	if ($response) {
		$track->execute(array($_ID, $user->id, 65, null));
	}
}
