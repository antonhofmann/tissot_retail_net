<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$_JSON = array();
$_DATA = array();

$_ID = $_REQUEST['news_recipient_id'];
$_APPLICATION = $_REQUEST['application'] ?: 'news';

if ($_REQUEST['news_recipient_firstname']) {
	$_DATA['news_recipient_firstname'] = trim($_REQUEST['news_recipient_firstname']);
}

if ($_REQUEST['news_recipient_name']) {
	$_DATA['news_recipient_name'] = trim($_REQUEST['news_recipient_name']);
}

if ($_REQUEST['news_recipient_email']) {
	$_DATA['news_recipient_email'] = trim($_REQUEST['news_recipient_email']);
}

if ($_REQUEST['news_recipient_active']) $_DATA['news_recipient_active'] = 1;
elseif($_REQUEST['news_recipient_active_old']) $_DATA['news_recipient_active'] = 0;

if ($_DATA) {

	$model = new Model($_APPLICATION);

	if ($_ID) {
		$_DATA['user_modified'] = $user->login;
		$query = Query::update('news_recipients', 'news_recipient_id', $_DATA);
		$_DATA['news_recipient_id'] = $_ID;
	} else {
		$_DATA['user_created'] = $user->login;
		$query = Query::insert('news_recipients', $_DATA);
	}

	$sth = $model->db->prepare($query);
	$response = $sth->execute($_DATA);

	if ($response) $_JSON['success'] = "The recipient is succesfully submitted";
	else $_JSON['error'] = "The recipient is not submitted. Please conatct system adminstrator.";

	if ($response && !$_ID) {
		$_ID = $model->db->lastInsertId();
		$_JSON['redirect'] = "/gazette/recipients/data/$_ID";
	}
}

header('Content-Type: text/json');
echo json_encode($_JSON);