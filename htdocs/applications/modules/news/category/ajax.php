<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'] ?: 'news';
$_SECTION = $_REQUEST['section'];
$_ID = $_REQUEST['id'];
$_JSON = array();

switch ($_SECTION) {
	
	case 'sort':

		$data = $_REQUEST['data'];
		
		if (is_array($data)) {

			$model = new Model($_APPLICATION);

			$sth = $model->db->prepare("
				UPDATE news_categories SET
					news_category_order = ?
				WHERE news_category_id = ?
			");

			foreach ($data as $i => $id) {
				$sth->execute(array($i+1, $id));
			}
		}

	break;
}


header('Content-Type: text/json');
echo json_encode($_JSON);