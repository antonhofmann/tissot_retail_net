<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$_JSON = array();
$_DATA = array();

$_ID = $_REQUEST['news_category_id'];
$_APPLICATION = $_REQUEST['application'] ?: 'news';

if ($_REQUEST['news_category_name']) {
	$_DATA['news_category_name'] = trim($_REQUEST['news_category_name']);
}

if ($_REQUEST['news_category_section_id']) {
	$_DATA['news_category_section_id'] = trim($_REQUEST['news_category_section_id']);
}

if ($_DATA) {

	$model = new Model($_APPLICATION);

	if ($_ID) {
		$_DATA['user_modified'] = $user->login;
		$query = Query::update('news_categories', 'news_category_id', $_DATA);
		$_DATA['news_category_id'] = $_ID;
	} else {

		// order
		$result = $model->query("SELECT COUNT(news_category_id) AS total FROM news_categories")->fetch();
		$_DATA['news_category_order'] = $result['total']+1;

		$_DATA['user_created'] = $user->login;
		$query = Query::insert('news_categories', $_DATA);
	}

	$sth = $model->db->prepare($query);
	$response = $sth->execute($_DATA);

	if ($response) $_JSON['success'] = "The category is succesfully submitted";
	else $_JSON['error'] = "The category is not submitted";

	if ($response && !$_ID) {
		$_ID = $model->db->lastInsertId();
		$_JSON['redirect'] = "/news/categories/data/$_ID";
	}
}

header('Content-Type: text/json');
echo json_encode($_JSON);