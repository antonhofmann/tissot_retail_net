<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$_ERRORS = array();

$_APPLICATION = $_REQUEST['application'] ?: 'news';
$_AUTHOR_FIRSTNAME = trim($_REQUEST['auchor_firstname']);
$_AUTHOR_NAME = trim($_REQUEST['auchor_name']);
$_AUTHOR_EMAIL = trim($_REQUEST['auchor_email']);

$_JSON = array();

$model = new Model($_APPLICATION);

if (!$_AUTHOR_FIRSTNAME) {
	$_ERRORS[] = "Author first name is mandatory";
}

if (!$_AUTHOR_NAME) {
	$_ERRORS[] = "Author name is mandatory";
}

if (!$_AUTHOR_EMAIL) {
	$_ERRORS[] = "Author email is mandatory";
}

// email address exist
$sth = $model->db->prepare("
	SELECT news_author_id
	FROM news_authors
	WHERE news_author_email = ?
");

$sth->execute(array($_AUTHOR_EMAIL));
$result = $sth->fetch();

if ($result['news_author_id']) {
	$_ERRORS[] = "The E-mail address <b>$_AUTHOR_EMAIL</b> is taken.";
}

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		INSERT INTO news_authors (
			news_author_first_name,
			news_author_name,
			news_author_email,
			news_author_active,
			user_created
		) VALUES (?, ?, ?, 1, ?)
	");

	$_JSON['success'] = $sth->execute(array($_AUTHOR_FIRSTNAME, $_AUTHOR_NAME, $_AUTHOR_EMAIL, $user->login));


	if ($_JSON['success']) {
		
		$_JSON['id'] = $model->db->lastInsertId();
		$_JSON['name'] = $_AUTHOR_FIRSTNAME." ".$_AUTHOR_NAME;

		$_JSON['data'] = array(
			'auchor_firstname' => $_AUTHOR_FIRSTNAME,
			'auchor_name' => $_AUTHOR_NAME,
			'auchor_email' => $_AUTHOR_EMAIL,
		);
	}
}

$_JSON['errors'] = $_ERRORS;

header('Content-Type: text/json');
echo json_encode($_JSON);
