<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."tcpdf/tcpdf.php";

$_ID = $_REQUEST['newsletter'];

$sessionKey = $_REQUEST['preview'] ? 'portal-preview' : 'portal'; 
$_REQUEST = array_merge($_REQUEST, Session::getFilter('news', $sessionKey));

$_SEARCH = trim($_REQUEST['search']);
$_SECTION = trim($_REQUEST['section']);
$_CATEGORY = trim($_REQUEST['category']);
$_YEAR = trim($_REQUEST['year']);
$_MONTH = trim($_REQUEST['month']);
$_PREVIEW = trim($_REQUEST['preview']);
$_CONTROLLER = $_REQUEST['controller'];

$_ERRORS = array();
$_JSON = array();

$_HOST = 'http://'.$_SERVER['SERVER_NAME'];

$user = User::instance();
$translate = Translate::instance();
$model = new Model('news');

// request execution time
ini_set('max_execution_time', 240);
ini_set('memory_limit', '2048M');


function strNormalizer($string) {
    $table = array(
        'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Ă'=>'A', 'Ā'=>'A', 'Ą'=>'A', 'Æ'=>'A', 'Ǽ'=>'A',
        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'ă'=>'a', 'ā'=>'a', 'ą'=>'a', 'æ'=>'a', 'ǽ'=>'a',

        'Þ'=>'B', 'þ'=>'b', 'ß'=>'Ss',

        'Ç'=>'C', 'Č'=>'C', 'Ć'=>'C', 'Ĉ'=>'C', 'Ċ'=>'C',
        'ç'=>'c', 'č'=>'c', 'ć'=>'c', 'ĉ'=>'c', 'ċ'=>'c',

        'Đ'=>'Dj', 'Ď'=>'D', 'Đ'=>'D',
        'đ'=>'dj', 'ď'=>'d',

        'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ĕ'=>'E', 'Ē'=>'E', 'Ę'=>'E', 'Ė'=>'E',
        'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ĕ'=>'e', 'ē'=>'e', 'ę'=>'e', 'ė'=>'e',

        'Ĝ'=>'G', 'Ğ'=>'G', 'Ġ'=>'G', 'Ģ'=>'G',
        'ĝ'=>'g', 'ğ'=>'g', 'ġ'=>'g', 'ģ'=>'g',

        'Ĥ'=>'H', 'Ħ'=>'H',
        'ĥ'=>'h', 'ħ'=>'h',

        'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'İ'=>'I', 'Ĩ'=>'I', 'Ī'=>'I', 'Ĭ'=>'I', 'Į'=>'I',
        'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'į'=>'i', 'ĩ'=>'i', 'ī'=>'i', 'ĭ'=>'i', 'ı'=>'i',

        'Ĵ'=>'J',
        'ĵ'=>'j',

        'Ķ'=>'K',
        'ķ'=>'k', 'ĸ'=>'k',

        'Ĺ'=>'L', 'Ļ'=>'L', 'Ľ'=>'L', 'Ŀ'=>'L', 'Ł'=>'L',
        'ĺ'=>'l', 'ļ'=>'l', 'ľ'=>'l', 'ŀ'=>'l', 'ł'=>'l',

        'Ñ'=>'N', 'Ń'=>'N', 'Ň'=>'N', 'Ņ'=>'N', 'Ŋ'=>'N',
        'ñ'=>'n', 'ń'=>'n', 'ň'=>'n', 'ņ'=>'n', 'ŋ'=>'n', 'ŉ'=>'n',

        'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ō'=>'O', 'Ŏ'=>'O', 'Ő'=>'O', 'Œ'=>'O',
        'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ō'=>'o', 'ŏ'=>'o', 'ő'=>'o', 'œ'=>'o', 'ð'=>'o',

        'Ŕ'=>'R', 'Ř'=>'R',
        'ŕ'=>'r', 'ř'=>'r', 'ŗ'=>'r',

        'Š'=>'S', 'Ŝ'=>'S', 'Ś'=>'S', 'Ş'=>'S',
        'š'=>'s', 'ŝ'=>'s', 'ś'=>'s', 'ş'=>'s',

        'Ŧ'=>'T', 'Ţ'=>'T', 'Ť'=>'T',
        'ŧ'=>'t', 'ţ'=>'t', 'ť'=>'t',

        'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ũ'=>'U', 'Ū'=>'U', 'Ŭ'=>'U', 'Ů'=>'U', 'Ű'=>'U', 'Ų'=>'U',
        'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ũ'=>'u', 'ū'=>'u', 'ŭ'=>'u', 'ů'=>'u', 'ű'=>'u', 'ų'=>'u',

        'Ŵ'=>'W', 'Ẁ'=>'W', 'Ẃ'=>'W', 'Ẅ'=>'W',
        'ŵ'=>'w', 'ẁ'=>'w', 'ẃ'=>'w', 'ẅ'=>'w',

        'Ý'=>'Y', 'Ÿ'=>'Y', 'Ŷ'=>'Y',
        'ý'=>'y', 'ÿ'=>'y', 'ŷ'=>'y',

        'Ž'=>'Z', 'Ź'=>'Z', 'Ż'=>'Z', 'Ž'=>'Z',
        'ž'=>'z', 'ź'=>'z', 'ż'=>'z', 'ž'=>'z',

        '“'=>'"', '”'=>'"', '‘'=>"'", '’'=>"'", '•'=>'-', '…'=>'...', '—'=>'-', '–'=>'-', '¿'=>'?', '¡'=>'!', '°'=>' degrees ',
        '¼'=>' 1/4 ', '½'=>' 1/2 ', '¾'=>' 3/4 ', '⅓'=>' 1/3 ', '⅔'=>' 2/3 ', '⅛'=>' 1/8 ', '⅜'=>' 3/8 ', '⅝'=>' 5/8 ', '⅞'=>' 7/8 ',
        '÷'=>' divided by ', '×'=>' times ', '±'=>' plus-minus ', '√'=>' square root ', '∞'=>' infinity ',
        '≈'=>' almost equal to ', '≠'=>' not equal to ', '≡'=>' identical to ', '≤'=>' less than or equal to ', '≥'=>' greater than or equal to ',
        '←'=>' left ', '→'=>' right ', '↑'=>' up ', '↓'=>' down ', '↔'=>' left and right ', '↕'=>' up and down ',
        '℅'=>' care of ', '℮' => ' estimated ',
        'Ω'=>' ohm ',
        '♀'=>' female ', '♂'=>' male ',
        '©'=>' Copyright ', '®'=>' Registered ', '™' =>' Trademark ', "&rsquo;" => "'", '&ndash;' => '-'
    );

    $string = strtr($string, $table);
    // Currency symbols: £¤¥€  - we dont bother with them for now
    $string = preg_replace("/[^\x9\xA\xD\x20-\x7F]/u", "", $string);

    return $string;
}

function px2mm($image, $dpi) {
    $img = ImageCreateFromJpeg($image);
    $x = ImageSX($img);
    $y = ImageSY($img);
    $h = $x * 25.4 / $dpi;
    $l = $y * 25.4 / $dpi;
    $px2cm[] = number_format($h, 2, ',', ' ');
    $px2cm[] = number_format($l, 2, ',', ' ');
    return $px2cm;
}


// permissions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PERMISSIONS = array(
	'view.all' => user::permission('can_view_all_news_articles'),
	'view.his' => user::permission('can_view_only_his_news_articles'),
	'edit.all' => user::permission('can_edit_all_news_articles'),
	'edit.his' => user::permission('can_edit_only_his_news_articles')
);

$_CAN_VIEW = $_PERMISSIONS['view.all'] || $_PERMISSIONS['view.his'] ? true : false;
$_ACCESS_FULL = $_PERMISSIONS['view.all'] || $_PERMISSIONS['edit.all'] ? true : false;


// filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_REQUEST['articles']) {
	$articles = join(',', $_REQUEST['articles']);
	$_FILTERS['articles'] = "news_article_id IN ($articles)";
} 


if (!$_REQUEST['articles'] && $_CONTROLLER=='search') {
	
	if ($_SEARCH) {
		$_FILTERS['search'] = "(
			news_article_title LIKE '%$_SEARCH%'
			OR news_article_contact_information LIKE '%$_SEARCH%'
			OR news_article_content_data LIKE '%$_SEARCH%'
		)";
	}

	if ($_SECTION) {
		$_FILTERS['section'] = "news_section_id = $_SECTION";
	}

	if ($_CATEGORY) {
		$_FILTERS['category'] = "news_category_id = $_CATEGORY";
	}

	if ($_YEAR) {
		$_FILTERS['year'] = "YEAR(news_article_publish_date) = $_YEAR";
	}

	if ($_MONTH) {
		$_FILTERS['month'] = "MONTH(news_article_publish_date) = $_MONTH";
	}
}


// newsletter ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NEWSLETTER = array ();

if (!$_REQUEST['articles'] && $_CONTROLLER<>'search') {

	if ($_ID) {
		$sql = "
			SELECT 
				newsletter_id AS id,
				newsletter_number AS number,
				newsletter_title AS title,
				newsletter_image AS image,
				newsletter_text AS content,
				DATE_FORMAT(newsletter_publish_date, '%m/%d/%Y') AS date
			FROM newsletters
			WHERE newsletter_id = $_ID
		";
	} else {
		$sql = "
			SELECT 
				newsletter_id AS id,
				newsletter_number AS number,
				newsletter_title AS title,
				newsletter_image AS image,
				newsletter_text AS content,
				DATE_FORMAT(newsletter_publish_date, '%m/%d/%Y') AS date
			FROM newsletters
			WHERE newsletter_publish_state_id = 3
			ORDER BY newsletter_publish_date DESC
			LIMIT 1
		";
	}

	$sth = $model->db->prepare($sql);
	$sth->execute();
	$_NEWSLETTER = $sth->fetch();

	$_FILTERS['newsletter'] = "newsletter_article_newsletter_id = ".$_NEWSLETTER['id'];
}


// newsletter files ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NEWSLETTER_FILES = array();

if (!$_ERRORS && $_NEWSLETTER) {
	
	$sth = $model->db->prepare("
		SELECT newsletter_file_path AS file
		FROM newsletter_files
		WHERE newsletter_file_newsletter_id = ?
	");

	$sth->execute(array($_NEWSLETTER['id']));
	$_NEWSLETTER_FILES = $sth->fetchAll();
}


// news authors ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NEWS_AUTHORS = array();

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT DISTINCT
			news_author_id AS id,
			CONCAT(' - ', news_author_first_name, ' ', news_author_name) AS name
		FROM news_articles
		INNER JOIN news_authors ON news_author_id = news_article_author_id
		WHERE news_article_active = 1 AND news_article_publish_state_id IN (4,5)
	");

	$sth->execute();
	$result = $sth->fetchAll();
	$_NEWS_AUTHORS = _array::extract($result);
}


// retalnet authors ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_AUTHORS = array();

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT DISTINCT
			user_id AS id,
			CONCAT(' - ', user_firstname, ' ', user_name) AS name
		FROM news_articles
		INNER JOIN db_retailnet.users ON user_id = news_article_author_user_id
		WHERE news_article_active = 1 AND news_article_publish_state_id IN (4,5)
	");

	$sth->execute();
	$result = $sth->fetchAll();
	$_AUTHORS = _array::extract($result);
}


// role restrictions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ARTICLE_ROLES = array();
$_USER_ROLES = $user->roles() ?: array();

if (!$_ERRORS && !$_PREVIEW) {

	$sth = $model->db->prepare("
		SELECT DISTINCT
			news_article_id AS id,
			GROUP_CONCAT(news_article_role_role_id) AS roles
		FROM newsletter_articles
		INNER JOIN news_articles ON news_article_id = newsletter_article_article_id
		INNER JOIN news_article_roles ON news_article_role_article_id = news_article_id
		WHERE newsletter_article_newsletter_id = ?
		AND news_article_active = 1
		AND news_article_publish_state_id IN (4,5)
		GROUP BY news_article_id
	");

	$sth->execute(array($_NEWSLETTER['id']));
	$result = $sth->fetchAll();
	$_ARTICLE_ROLES = _array::extract($result);
}


// company restrictions ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ARTICLE_COMPANIES = array();

if (!$_ERRORS && !$_PREVIEW) {

	$sth = $model->db->prepare("
		SELECT DISTINCT
			news_article_id AS id,
			GROUP_CONCAT(news_article_address_address_id) AS addresses
		FROM newsletter_articles
		INNER JOIN news_articles ON news_article_id = newsletter_article_article_id
		INNER JOIN news_article_addresses ON news_article_address_article_id = news_article_id
		WHERE newsletter_article_newsletter_id = 1
		AND news_article_active = 1
		AND news_article_publish_state_id IN (4,5)
		GROUP BY news_article_id
	");

	$sth->execute(array($_NEWSLETTER['id']));
	$result = $sth->fetchAll();
	$_ARTICLE_COMPANIES = _array::extract($result);
}


// articles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ARTICLES = array();

$articleFilters = $_FILTERS ? ' AND '.join(' AND ', array_values($_FILTERS)) : null;

$articlesOrder = $_NEWSLETTER['id']
	? "news_section_order, news_section_name, newsletter_article_sort, news_article_publish_date DESC, news_article_title, news_article_content_order" 
	: "news_section_order, news_section_name, news_article_publish_date DESC, news_article_title, news_article_content_order";

if (!$_ERRORS) {

	$result = $model->query("
		SELECT DISTINCT
			news_article_id AS id,
			news_article_title AS title,
			DATE_FORMAT(news_article_publish_date, '%m/%d/%Y') AS date,
			news_article_featured AS featured,
			news_article_new AS new,
			news_article_contact_information AS informations,
			news_article_author_id AS author_id,
			news_article_author_user_id AS author_user_id,
			news_section_id AS sid,
			news_section_name AS section,
			news_category_id AS cid,
			news_category_name AS category,
			news_article_content_id AS tid,
			news_article_content_data AS data,
			news_article_content_template_id AS template
		FROM newsletter_articles
		INNER JOIN news_articles ON news_article_id = newsletter_article_article_id
		INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
		INNER JOIN news_article_templates ON news_article_content_template_id = news_article_template_id
		INNER JOIN news_categories ON news_article_category_id = news_category_id
		INNER JOIN news_sections ON news_section_id = news_category_section_id
		WHERE news_article_active = 1 AND IF(news_article_expiry_date,  CURRENT_DATE <= news_article_expiry_date, 1) = 1 
		$articleFilters
		ORDER BY $articlesOrder
	")->fetchAll();

	if (!$result) {
		$_ERRORS[] = "Articles not found";
		goto BLOCK_RESPONSE;
	}
}
	

// content parser ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ARTICLES_DENIED = array();
$_TEASER = array();

if (!$_ERRORS) {

	foreach ($result as $row) {

		$id = $row['id'];
		$sid = $row['sid'];
		$tid = $row['tid'];

		$access = true;

		// check article role and company restrictions
		if (!$_ACCESS_FULL && !$_PREVIEW) {

			$checkRolesAccess = true;
			$checkCompaniesAccess = true;
			
			// role restrictions
			if ($_ARTICLE_ROLES[$id]) {
				$roles = explode(',', $_ARTICLE_ROLES[$id]);
				$permittedRoles = array_intersect($roles, $_USER_ROLES);
				$checkRolesAccess = count($permittedRoles) > 0 ? true : false;
			}

			// company restrictions
			if ($_ARTICLE_COMPANIES[$id]) {
				$companies = explode(',', $_ARTICLE_COMPANIES[$id]);
				$checkCompaniesAccess = in_array($user->address, $companies) ? true : false;
			}

			$access = $checkRolesAccess && $checkCompaniesAccess ? true : false;
		}

		if ($access) {

			$_ARTICLES[$sid]['name'] = $row['section'];
			$_ARTICLES[$sid]['articles'][$id]['title'] = $row['title'];

			$data = $row['data'] ? unserialize($row['data']) : array();
			
			$data['template'] = $row['template'];
			//$data['text'] = nl2br($data['text']);

			// teaser
			if ($row['template']==1) {
				
				$author = null;
				$info = null;

				$_TEASER[$id] = $tid;

				// news authors
				if ($row['author_id'] && $_NEWS_AUTHORS) {
					$key = $row['author_id'];
					$author = '- Author: '.$_NEWS_AUTHORS[$key];
				}

				// retailnet authors
				if ($row['author_user_id'] && $_AUTHORS && $_AUTHORS[$row['author_user_id']]) {
					$key = $row['author_user_id'];
					$author = '- Author: '.$_AUTHORS[$key];
				}

				if ($row['date']) {
					$info .= "Date: {$row[date]} $author";
				}

				if ($row['informations']) {
					$info .= "<br />".nl2br($row['informations']);
				}

				$data['info'] = "<p>$info</p>";
			}

			// group article contents
			$_ARTICLES[$sid]['articles'][$id]['templates'][$tid] = $data;
		} 
		else {
			$_ARTICLES_DENIED[] = $id;
		}
	}
}

// pdf builder :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

class MYPDF extends TCPDF {
		
	public function Header() {

		global $_NEWSLETTER;

		$page = $this->PageNo();

		// swatch logo
		$this->Image( $_SERVER['DOCUMENT_ROOT'].'/public/images/logo.swatch.145x40.png', 10, 8, 28);

		if ($_NEWSLETTER['id']) {
			if ($page==1) {
				$this->SetFontSize(14);
				$this->Cell(0, 26,'Gazette No.'.$_NEWSLETTER['number'], 0, 0, 'C');
				$this->SetFontSize(10);
				$this->SetTextColor(80);
				$this->Cell(0, 26, $_NEWSLETTER['date'], 0, 1, 'R');
			} else {
				$this->SetFontSize(9);
				$this->SetTextColor(120);
				$this->Cell(0, 26,'Gazette No.'.$_NEWSLETTER['number'], 0, 1, 'R');
			}
		}
	}

	public function Footer() {
		$this->SetY(-15);
		$this->SetFontSize(9);
		$this->SetTextColor(120);
		$this->Cell(0, 15, date('m/d/Y'), 0, 0, 'L');
		$this->Cell(0, 15, 'Page '.$this->PageNo(), 0, 0, 'R');
	}

	public function pageBreakControll($h,$y,$addPage=false) {
		$this->checkPageBreak($h,$y,$addPage);
	}

	// Colored table
    public function tplTable($settings, $data) {

    	global $_WORKAREA_WIDTH;

		$this->SetFillColor(160);
		$this->SetTextColor(255);
		$this->SetDrawColor(128);
		$this->SetLineWidth(0.1);
		$this->SetFont('', 'B');

		$headers = $settings['colHeaders'];
		$dimensions = $settings['dimensions'] ?: array();

		$celWidth = $headers ? $_WORKAREA_WIDTH / count($headers) : 20;

		// header
		if ($headers) {

			$totalColumns = count($headers);

			foreach ($headers as $i => $caption) {
				$width = $dimensions[$i] ?: $celWidth;
				$this->Cell($width, 7, $caption, 1, 0, 'L', 1);
			}

			$this->Ln();
		}

		// Color and font restoration
		$this->SetFillColor(224, 235, 255);
		$this->SetTextColor(0);
		$this->SetFont('');

		// body
		if ($data) {

			$fill = 0;
			
			foreach ($data as $cols) {
				
				foreach ($cols as $i => $value) {

					if ($totalColumns > 1 && count($cols)==1) {
						$width = $_WORKAREA_WIDTH;
					} else {
						$width = $dimensions[$i] ?: $celWidth;
					}

					$imgs = Content::match($value, "#\[(.*?)\]#");

					if ($imgs) {
						foreach ($imgs as $img) {
							//$image = file_exists($_SERVER['DOCUMENT_ROOT']."/data/news/images/$img") ? "<img src=\"/data/news/images/$img\" />" : null;
							$value = str_replace("[$img]", $image, $value);
						}
					}
					
					$this->MultiCell($width, 7, $value, 1, 'L', $fill, 0, null, null, true, 0, true);
					$fill=!$fill;
				}

				$this->Ln();
			}
		}
    }
}


$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);

$_MARGIN_LEFT = 10;
$_MARGIN_RIGHT = 10;
$_MARGIN_TOP = 20;
$_MARGIN_BOTTOM = 10;
$_PAGE_WIDTH = $pdf->getPageWidth();
$_PAGE_HEIGHT = $pdf->getPageHeight();
$_WORKAREA_WIDTH = $_PAGE_WIDTH - $_MARGIN_LEFT - $_MARGIN_RIGHT;
$_WORKAREA_HEIGHT = $_PAGE_HEIGHT - 10 - $_MARGIN_BOTTOM;

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor($user->firstname.' '.$user->name);
$pdf->SetTitle('Newsletter #'.$_NEWSLETTER['number']);

// set margins
$pdf->SetMargins($_MARGIN_LEFT, $_MARGIN_TOP, $_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, $_MARGIN_BOTTOM);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// bind swatch font
$pdf->setFontSubsetting(true);
$fontname = TCPDF_FONTS::addTTFfont($_SERVER['DOCUMENT_ROOT'].'/libraries/tcpdf/fonts/swatchCT.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont($fontname, '', 12, '', false);

$pdf->SetLineWidth(0.1);
$pdf->SetDrawColor(120);


// newsletter visual :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_NEWSLETTER['id']) {

	$pdf->AddPage();

	// gazette number
	//$pdf->SetFontSize(16);
	//$pdf->MultiCell(0, 0, "Gazette #{$_NEWSLETTER[number]}", 0, 'C', 0, 1);
	//$pdf->Ln();
	
	// visual
	$pdf->MultiCell(0, 0, "<img src=\"{$_NEWSLETTER[image]}\">", 0, 'L', 0,1,null,null,true,0,true);

	// newsletter title
	$pdf->SetFontSize(14);
	$txt = strNormalizer($_NEWSLETTER['title']);
	$pdf->MultiCell(0,0, $txt, 0, 'L');
	$pdf->Ln();

	// newsletter content
	$pdf->SetFontSize(11);
	$txt = strNormalizer($_NEWSLETTER['content']);
	$txt = preg_replace('#\<span.*?\>(.*?)\<\/span\>#si', '$1', $txt);
	$pdf->MultiCell(0, 0, $txt, 0, 'L', 0,1,null,null,true,0,true);
}

if ($_NEWSLETTER_FILES) {

	$pdf->SetTextColor(255);
	$pdf->SetFillColor(160);
	$pdf->SetFontSize(10);
	$pdf->setHtmlLinksStyle(array(255,255,255), 'N');
	$pdf->setCellPaddings(1,1,1,1);
	$pdf->ln(10);

	foreach ($_NEWSLETTER_FILES as $row) {
		$file = $row['file'];
		$hash = Encryption::url($file);
		$link = $_HOST."/download/news/$hash/".$_NEWSLETTER['id'];
		$ext = strtoupper(pathinfo($file, PATHINFO_EXTENSION));
		$pdf->writeHTMLCell(60, 0, null, null, "<a href=\"$link\">DOWNLOAD $ext</a>", 0, 1, 1, true, 'L');
	}

	$pdf->setCellPadding(0);
}


// articles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$i=0;

$_TEASER_IMAGE_WIDTH = 70;

$borderArticleSeparator = array(
   'T' => array('width' => 0),
   'R' => array('width' => 0),
   'B' => array('width' => 1, 'color' => array(0,0,0), 'dash' => 1, 'cap' => 'square'),
   'L' => array('width' => 0)
);

$_PICTURES_DIMENSIONS = array();
$reduce = 0.1;

// calcullate template pictures
foreach ($_ARTICLES as $sid => $section) {

	foreach ($section['articles'] as $id => $article) {

		$lastTemplateType = null;

		foreach ($article['templates'] as $tid => $data) {

			// tpl images
			if ($data['files'] && $data['template']==3) {

				$dpi = 72;				
					
				foreach ($data['files'] as $file) {

					$file = $_SERVER['DOCUMENT_ROOT'].$file;

					if (file_exists($file)) {
						
						$totalHeight = $lastTemplateType==3 ? $totalHeight : 0;
						$pGroups = $lastTemplateType==3 ? $pGroups : array();
						
						$size = px2mm($file, $dpi);						
						$width = $size[0];
						$height = $size[1];

						// aspect ratio calculator
						// new height = original height / original width x new width
						$newHeight = $height > $_WORKAREA_HEIGHT ? $height / $width * $_WORKAREA_WIDTH : $height;
						$newWidth = $width > $_WORKAREA_WIDTH ? $width / $height * $newHeight : $width;	

						$_PICTURES_DIMENSIONS[$file]['width'] = $newWidth;				
						$_PICTURES_DIMENSIONS[$file]['height'] = $newHeight;	

						// total block height
						$totalHeight += $newHeight;

						if (!$_PICTURES_DIMENSIONS[$tid]['height']) {
							$_PICTURES_DIMENSIONS[$tid]['height'] = $newHeight;
						}

						$interval = $_WORKAREA_HEIGHT - $_WORKAREA_HEIGHT*$reduce;
						
						if ($totalHeight > $interval ) {

							if ($totalHeight > $interval && $totalHeight <= $_WORKAREA_HEIGHT) $prop = $reduce;
							else $prop = $totalHeight/$_WORKAREA_HEIGHT-1;

							if ($reduce >= $prop) {

								$pGroups[] = $file;
								
								foreach ($pGroups as $_file) {
									$_PICTURES_DIMENSIONS[$_file]['width'] = $_PICTURES_DIMENSIONS[$_file]['width'] - $reduce*$_PICTURES_DIMENSIONS[$_file]['width'];
									$_PICTURES_DIMENSIONS[$_file]['height'] = $_PICTURES_DIMENSIONS[$_file]['height'] - $reduce*$_PICTURES_DIMENSIONS[$_file]['height'];
								}

								$totalHeight = 0;
								$pGroups = array();		
								$_PICTURES_DIMENSIONS[$file]['break'] = true;					

							} else {
								$totalHeight = $newHeight;
								$pGroups = array($file);
								$_PICTURES_DIMENSIONS[$lastfile]['break'] = true;
							}

						} else {
							$pGroups[] = $file;	
						}

						$lastfile = $file;
					}
				}
			}			

			$lastTemplateType = $data['template'];
		}
	}
}
//$_SUCCESS = true;
//goto BLOCK_RESPONSE;

foreach ($_ARTICLES as $sid => $section) {

	$i++;

	$pdf->AddPage();
	$pdf->SetFontSize(16);
	$pdf->setCellPaddings(2, 2, 2, 2);
	$pdf->SetFillColor(226, 0, 26);
	$pdf->SetTextColor(255, 255, 255);

	// section title
	$x = $pdf->GetX();
	$y = $pdf->GetY();
	$y = $y > $_MARGIN_TOP ? $y + $_MARGIN_TOP : $y;
	
	$pdf->SetFontSize(15);
	$pdf->MultiCell(0, 0, strtoupper($section[name]), 0, 'R', 1, 1, $x, $y);
	
	$pdf->setCellPadding(0);

	foreach ($section['articles'] as $id => $article) {

		// article title
		$pdf->SetTextColor(120);

		// pagebreak controll
		$height = 0;		
		
		if ($_TEASER[$id]) {

			$y = $pdf->GetY();
			$tid = $_TEASER[$id];
			$teaser = $article['templates'][$tid];

			$content = $teaser['title'].$teaser['text'].$teaser['info'];
			
			// teaser content height
			if ($content) {
				$pdf->SetFontSize(14);
				$pdf->startTransaction();
				$linesTitle = $pdf->MultiCell(120, 0, $content, 0, 'L', 0, 0, '', '', true, 0, false, false, 0);
				$pdf = $pdf->rollbackTransaction();
				$height += $linesTitle*5;
			}

			if ($teaser['files']) {
				$height += count($teaser['files'])*10;
			}

			$height += 30;
			$pdf->pageBreakControll($height,$y, true);
		}
		
		$x = $pdf->GetX();
		$y = $pdf->GetY()+5;
		$pdf->SetFontSize(15);
		$txt = strNormalizer($article['title']);
		$pdf->MultiCell(0, 0, $txt, 0, 'L', 0, 1, $x, $y);

		foreach ($article['templates'] as $tid => $data) {

			switch ($data['template']) {
				
				case 1: // tpl: teaser img, title, content, info, files

					$pdf->SetTextColor(0);

					$x = $pdf->GetX();
					$y = $pdf->GetY() + 3;
					$startY = $y;

					$img = $_SERVER['DOCUMENT_ROOT'].$data['image'];

					// teaser image
					if ($img && file_exists($img)) {
						$pdf->Image($img, $x, $y, $_TEASER_IMAGE_WIDTH, null, null, null, null, false, 72, null, false, false, 1);
					}

					// teaser title
					if ($data['title']) {
						$pdf->SetFontSize(12);
						$x = $pdf->GetX() + $_TEASER_IMAGE_WIDTH + 5;
						$y = $pdf->GetY() + 3;
						$txt = strNormalizer($data['title']);
						$pdf->MultiCell(0, 0, $txt, 0, 'L', 0, 1, $x, $y);
					}

					// teaser text
					if ($data['text']) {
						$pdf->SetFontSize(10);
						$x = $pdf->GetX() + $_TEASER_IMAGE_WIDTH + 5;
						$y = $pdf->GetY() + 3;
						$txt = strNormalizer($data['text']);
						$pdf->writeHTMLCell(0, 0, $x, $y, $txt, 0, 1, 0, true, 'L');
					}

					if ($data['info']) {
						$pdf->SetTextColor(140);
						$pdf->SetFontSize(9);
						$x = $pdf->GetX() + $_TEASER_IMAGE_WIDTH + 5;
						$y = $pdf->GetY() + 3;
						$txt = strNormalizer($data['info']);
						$pdf->writeHTMLCell(0, 0, $x, $y, $txt, 0, 1, 0, true, 'L');
					}

					if ($data['files']) {
						
						$pdf->SetTextColor(255);
						$pdf->SetFillColor(160);
						$pdf->SetFontSize(10);
						$pdf->setHtmlLinksStyle(array(255,255,255), 'N');
						$pdf->setCellPaddings(1,1,1,1);
						$pdf->ln(3);

						foreach ($data['files'] as $file) {
							$x = $pdf->GetX() + $_TEASER_IMAGE_WIDTH + 5;
							$y = $pdf->GetY() + 1;
							$hash = Encryption::url($file);
							$link = $_HOST."/download/news/$hash/".$_NEWSLETTER['id'];
							$ext = strtoupper(pathinfo($file, PATHINFO_EXTENSION));
							$pdf->writeHTMLCell(60, 0, $x, $y, "<a href=\"$link\">DOWNLOAD $ext</a>", 0, 1, 1, true, 'L');
						}

						$pdf->setCellPadding(0);
						$pdf->ln(3);
					}

					if ($pdf->GetY()-$startY <= 55) {
						$pdf->setY($startY + 60);
					}

				break;

				case 2: // tpl: title, content, files

					$pdf->SetTextColor(0);

					// title
					if ($data['title']) {
						$pdf->SetFontSize(12);
						$x = $pdf->GetX();
						$y = $pdf->GetY() + 3;
						$txt = strNormalizer($data['title']);
						$pdf->MultiCell(0, 0, $txt, 0, 'L', 0, 1, $x, $y);
					}

					// text
					if ($data['text']) {
						$pdf->SetFontSize(10);
						$x = $pdf->GetX();
						$y = $pdf->GetY() + 3;
						$txt = strNormalizer($data['text']);
						$pdf->writeHTMLCell(0, 0, $x, $y, $txt, 0, 1, 0, true, 'L');
					}

					// files
					if ($data['files']) {
						
						$pdf->SetTextColor(255);
						$pdf->SetFillColor(160);
						$pdf->SetFontSize(10);
						$pdf->setHtmlLinksStyle(array(255,255,255), 'N');
						$pdf->setCellPaddings(1,1,1,1);
						$pdf->ln(3);

						foreach ($data['files'] as $file) {
							$x = $pdf->GetX();
							$y = $pdf->GetY() + 1;
							$hash = Encryption::url($file);
							$link = $_HOST."/download/news/$hash/".$_NEWSLETTER['id'];
							$ext = strtoupper(pathinfo($file, PATHINFO_EXTENSION));
							$pdf->writeHTMLCell(60, 0, $x, $y, "<a href=\"$link\">DOWNLOAD $ext</a>", 0, 1, 1, true, 'L');
						}

						$pdf->setCellPadding(0);
					}

				break;

				case 3: // tpl: title, picture

					$pdf->SetTextColor(0);

					$contentHeight = 0;

					$x = $pdf->GetX();
					$y = $pdf->GetY();
					//$pdf->MultiCell(0, 0, "[$x,$y]", 0, 'L', 0, 1, $x, $y);

					if ($data['text']) {
						$pdf->SetFontSize(12);
						$pdf->startTransaction();
						$linesTitle = $pdf->MultiCell(120, 0, $data['text'], 0, 'L', 0, 0, '', '', true, 0, false, false, 0);
						$contentHeight = $linesTitle*5;
						$pdf = $pdf->rollbackTransaction();
					}

					// check page
					if ($_PICTURES_DIMENSIONS[$tid]['height']) {
						$height = $_PICTURES_DIMENSIONS[$tid]['height'] + $contentHeight;
						$pdf->pageBreakControll($height, $y, true);
					}

					// title
					if ($data['text']) {
						$x = $pdf->GetX();
						$y = $pdf->GetY();
						$pdf->SetFontSize(12);
						$txt = strNormalizer($data['text']);
						$pdf->MultiCell(0, 0, $txt, 0, 'L', 0, 1, $x, $y);
					}

					// files
					if ($data['files']) {	

						$dpi = 72;					
						
						foreach ($data['files'] as $file) {
							
							$file = $_SERVER['DOCUMENT_ROOT'].$file;
							
							if (file_exists($file)) {

								if ($_PICTURES_DIMENSIONS[$file]) { //die($file);
									$width = $_PICTURES_DIMENSIONS[$file]['width'];
									$height = $_PICTURES_DIMENSIONS[$file]['height'];
								} else {
									$size = px2mm($file, $dpi);
									$width = $size[0] > $_WORKAREA_WIDTH ? $_WORKAREA_WIDTH : $size[0];
									$height = $size[1] > $_WORKAREA_HEIGHT ? $_WORKAREA_HEIGHT : $size[1];
								}
								
								$x = $pdf->GetX();
								$y = $pdf->GetY();
								$pdf->Image($file, $x, $y, $width, null, null, null, null, false, $dpi);
								$pdf->setY($y+$height);
							}
						}
					}

				break;

				case 4: // tpl: left picture, title, text

					$pdf->SetTextColor(0);

					$x = $pdf->GetX();
					$y = $pdf->GetY() + 3;
					$startY = $y;

					$img = $_SERVER['DOCUMENT_ROOT'].$data['image'];

					// teaser image
					if ($img && file_exists($img)) {
						$pdf->Image($img, $x, $y, $_TEASER_IMAGE_WIDTH, null, null, null, null, false, 72, null, false, false, 1);
					}

					// teaser title
					if ($data['title']) {
						$pdf->SetFontSize(12);
						$x = $pdf->GetX() + $_TEASER_IMAGE_WIDTH + 5;
						$y = $pdf->GetY() + 3;
						$txt = strNormalizer($data['title']);
						$pdf->MultiCell(0, 0, $txt, 0, 'L', 0, 1, $x, $y);
					}

					// teaser text
					if ($data['text']) {
						$pdf->SetFontSize(10);
						$x = $pdf->GetX() + $_TEASER_IMAGE_WIDTH + 5;
						$y = $pdf->GetY() + 3;
						$txt = strNormalizer($data['text']);
						$pdf->writeHTMLCell(0, 0, $x, $y, $txt, 0, 1, 0, true, 'L');
					}

					if ($pdf->GetY()-$startY <= 55) {
						$pdf->setY($startY + 60);
					}

				break;

				case 5:

					$settings = $data['dataloader']['settings'];
					$sources = $data['dataloader']['sources'];
					$pdf->tplTable($settings, $sources);

				break;
			}

			$lastTemplateType = $data['template'];

		} // end templates

		// article separator
		$x = $pdf->GetX();
		$y = $pdf->GetY() + 5;
		$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
		$pdf->Line($x, $y, $x+$_WORKAREA_WIDTH, $y, $style);
		$pdf->ln(10);
	}
}

$pdf->SetFontSize(10);
$pdf->SetTextColor(0);
$pdf->MultiCell(0, 0, "DON'T FORGET THAT I AM AN INTERNAL DOCUMENT WHICH MUST NOT BE SHARED OUTSIDE OF SWATCH OR LEFT BEHIND!");

// export ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


$file = '/data/tmp/newsletter.'.$_SERVER['REQUEST_TIME'].'.pdf';
$pdf->Output($_SERVER['DOCUMENT_ROOT'].$file, 'F');

$_SUCCESS = file_exists($_SERVER['DOCUMENT_ROOT'].$file) ? true : false;

if (!$_ERRORS) {
	$hash = Encryption::url($file);
	$_FILE_URL = "/download/tmp/$hash";
}


// tracking ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$trackArticle = $model->db->prepare("
	INSERT INTO news_article_tracks (
		news_article_track_article_id,
		news_article_track_newsletter_id,
		news_article_track_user_id,
		news_article_track_type_id,
		news_article_track_entity
	)
	VALUES (?, ?, ?, ?, ?)
");

$trackNewsletter = $model->db->prepare("
	INSERT INTO newsletter_tracks (
		newsletter_track_newsletter_id,
		newsletter_track_user_id,
		newsletter_track_type_id
	)
	VALUES (?, ?, ?)
");

if (!$_ERRORS && $_REQUEST['newsletter']) {
	
	// track: the newsletter is printed
	$trackNewsletter->execute(array($_NEWSLETTER['id'], $user->id, 21));
}

if (!$_ERRORS && $_REQUEST['articles']) {

	foreach ($_REQUEST['articles'] as $i => $article) {
		
		// track: news article print
		$trackArticle->execute(array($article, $_ID, $user->id, 49, null));	
	}
}

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONSE:

ob_end_clean();


header('Content-Type: text/json');
echo json_encode(array(
	'success' => $_SUCCESS,
	'file' => $_FILE_URL,
	'test' => $_TEST
));

