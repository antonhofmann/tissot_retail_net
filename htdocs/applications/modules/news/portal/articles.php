<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc'; 

$user = User::instance();
$translate = Translate::instance();

$_NEWSLETTER = trim($_REQUEST['newsletter']);
$_APPLICATION = trim($_REQUEST['application']);
$_CONTROLLER = trim($_REQUEST['controller']);

$sessionKey = $_REQUEST['preview'] ? 'portal-preview' : 'portal'; 
$_REQUEST = array_merge($_REQUEST, Session::getFilter('news', $sessionKey));

$_SEARCH = trim($_REQUEST['search']);
$_SECTION = trim($_REQUEST['section']);
$_CATEGORY = trim($_REQUEST['category']);
$_YEAR = trim($_REQUEST['year']);
$_MONTH = trim($_REQUEST['month']);
$_PREVIEW = trim($_REQUEST['preview']);
$_ARTICLE = trim($_REQUEST['article']);

$totalArticles = 0;

$_JSON = array();
$_FILTERS = array();

$model = new Model($_APPLICATION);


// permissions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PERMISSIONS = array(
	'view.all' => user::permission('can_view_all_news_articles'),
	'view.his' => user::permission('can_view_only_his_news_articles'),
	'edit.all' => user::permission('can_edit_all_news_articles'),
	'edit.his' => user::permission('can_edit_only_his_news_articles')
);

$_CAN_VIEW = $_PERMISSIONS['view.all'] || $_PERMISSIONS['view.his'] ? true : false;
$_ACCESS_FULL = $_PERMISSIONS['view.all'] || $_PERMISSIONS['edit.all'] ? true : false;


// filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_CONTROLLER != 'search' && !$_ARTICLE) {
	$_FILTERS['newsletter'] = "newsletter_article_newsletter_id = $_NEWSLETTER";
}

if ($_ARTICLE) {
	$_FILTERS['article'] = "news_article_id = $_ARTICLE";
}

if (!$_PREVIEW) {
	$_FILTERS['article.states'] = "news_article_publish_state_id IN (4,5)";
}

if ($_SEARCH) {
	$_FILTERS['search'] = "(
		news_article_title LIKE '%$_SEARCH%'
		OR news_article_contact_information LIKE '%$_SEARCH%'
		OR news_article_content_data LIKE '%$_SEARCH%'
	)";
}

if ($_SECTION) {
	$_FILTERS['section'] = "news_section_id = $_SECTION";
}

if ($_CATEGORY) {
	$_FILTERS['category'] = "news_category_id = $_CATEGORY";
}

if ($_YEAR) {
	$_FILTERS['year'] = "YEAR(news_article_publish_date) = $_YEAR";
}

if ($_MONTH) {
	$_FILTERS['month'] = "MONTH(news_article_publish_date) = $_MONTH";
}

if (!$_ARTICLE) {
	$_FILTERS['active'] = "news_article_active = 1";
}

if ($_CONTROLLER != 'search' && !$_ARTICLE && !$_PREVIEW) {
	$_FILTERS['expiry'] = "IF(news_article_expiry_date,  CURRENT_DATE <= news_article_expiry_date, 1) = 1";
}

$_JSON['filters'] = $_FILTERS;

$_FILTERS = $_FILTERS ? ' AND '.join(' AND ', array_values($_FILTERS)) : null;

$_BIND_NEWSLETETR_ARTICLES = $_ARTICLE ? null : "INNER JOIN newsletter_articles ON news_article_id = newsletter_article_article_id";

$_SORT = !$_ARTICLE 
	? "news_section_order, news_section_name, newsletter_article_sort, news_article_publish_date DESC, news_article_title"
	: "news_section_order, news_section_name, news_article_publish_date DESC, news_article_title";

if ($_CONTROLLER=='search') {
	$_SORT = 'news_article_publish_date DESC, news_section_order, news_section_name, newsletter_article_sort, news_article_title';
}


$filterNewsletter = $_CONTROLLER != 'search' ? " AND newsletter_article_newsletter_id = $_NEWSLETTER " : null;
$filterState = $_PREVIEW ? null : " AND news_article_publish_state_id IN (4,5) ";

// company restrictions ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT DISTINCT
		news_article_id AS id,
		GROUP_CONCAT(news_article_address_address_id) AS addresses
	FROM newsletter_articles
	INNER JOIN news_articles ON news_article_id = newsletter_article_article_id
	INNER JOIN news_article_addresses ON news_article_address_article_id = news_article_id
	WHERE news_article_active = 1 AND news_article_publish_state_id IN (4,5)
	$filterNewsletter $filterState
	GROUP BY news_article_id
");

$sth->execute();
$result = $sth->fetchAll();
$_COMPANY_RESTRICTED_ARTICLES = _array::extract($result);

// role restrictions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT DISTINCT
		news_article_id AS id,
		GROUP_CONCAT(news_article_role_role_id) AS roles
	FROM newsletter_articles
	INNER JOIN news_articles ON news_article_id = newsletter_article_article_id
	INNER JOIN news_article_roles ON news_article_role_article_id = news_article_id
	WHERE news_article_active = 1 AND news_article_publish_state_id IN (4,5)
	$filterNewsletter $filterState
	GROUP BY news_article_id
");

$sth->execute();
$result = $sth->fetchAll();
$_ROLE_RESTRICTED_ARTICLES = _array::extract($result);


// article authors :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT DISTINCT
		news_author_id AS id,
		CONCAT(news_author_first_name, ' ', news_author_name) AS name
	FROM news_articles
	INNER JOIN news_authors ON news_author_id = news_article_author_id
");

$sth->execute();
$result = $sth->fetchAll();
$_NEWS_AUTHORS = _array::extract($result);

// get user authors
$sth = $model->db->prepare("
	SELECT DISTINCT
		user_id AS id,
		CONCAT(user_firstname, ' ', user_name) AS name
	FROM news_articles
	INNER JOIN db_retailnet.users ON user_id = news_article_author_user_id
");

$sth->execute();
$result = $sth->fetchAll();
$_USER_AUTHORS = _array::extract($result);


// articles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ARTICLES = array();

$result = $model->query("
	SELECT DISTINCT
		news_article_id AS id,
		news_article_title AS title,
		DATE_FORMAT(news_article_publish_date, '%m/%d/%Y') AS date,
		news_article_featured AS featured,
		news_article_new AS new,
		news_article_contact_information AS informations,
		news_article_author_id AS author_id,
		news_article_author_user_id AS author_user_id,
		news_section_id AS sid,
		news_section_name AS section,
		news_category_id AS cid,
		news_category_name AS category,
		(
			SELECT COUNT(news_article_content_id)
			FROM news_article_contents
			WHERE news_article_content_article_id = news_article_id
		) AS totalArticles,
		news_article_content_id AS tid,
		news_article_content_data AS data,
		news_article_template_file_view AS file,
		news_article_sticker AS sticker
	FROM news_articles
	$_BIND_NEWSLETETR_ARTICLES
	INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
	INNER JOIN news_article_templates ON news_article_content_template_id = news_article_template_id
	INNER JOIN news_categories ON news_article_category_id = news_category_id
	INNER JOIN news_sections ON news_section_id = news_category_section_id
	WHERE news_article_content_template_id = 1 $_FILTERS
	ORDER BY $_SORT
")->fetchAll();

if ($result) {
	
	// teaser template
	$teaser = File::load('/applications/templates/news/article/tpl.teaser.view.php');

	// user roles
	$userRoles = $user->roles() ?: array();

	foreach ($result as $row) {

		$id = $row['id'];
		$sid = $row['sid'];
		$file = $row['file'];
		$files = null;

		$access = true;

		// check article role and company restrictions
		if (!$_ACCESS_FULL && !$_PREVIEW) {

			$checkRolesAccess = true;
			$checkCompaniesAccess = true;

			// role restrictions
			if ($_ROLE_RESTRICTED_ARTICLES[$id]) {
				$roles = explode(',', $_ROLE_RESTRICTED_ARTICLES[$id]);
				$permittedRoles = array_intersect($roles, $userRoles);
				$checkRolesAccess = count($permittedRoles) > 0 ? true : false;
			}

			// company restrictions
			if (!$_ACCESS_FULL && !$_PREVIEW && $_COMPANY_RESTRICTED_ARTICLES[$id]) {
				$companies = explode(',', $_COMPANY_RESTRICTED_ARTICLES[$id]);
				$checkCompaniesAccess = in_array($user->address, $companies) ? true : false;
			}

			$access = $checkRolesAccess && $checkCompaniesAccess ? true : false;
		}
			
		if ($access && $teaser) {

			$data = $row['data'] ? unserialize($row['data']) : array();

			$_ARTICLES[$sid]['name'] = $row['section'];
			$_ARTICLES[$sid]['articles'][$id]['title'] = $row['title'];
			$_ARTICLES[$sid]['articles'][$id]['new'] = $row['new'];

			// identificators
			$data['id'] = $row['tid'];
			$data['article'] = $id;

			$data['date'] = $row['date'] ? 'Date: '.$row['date'] : null;
			$data['informations'] = nl2br($row['informations']);

			// parse teaser files
			if ($data['files']) {
				
				foreach ($data['files'] as $file) {
					
					$extension = strtoupper(pathinfo($file, PATHINFO_EXTENSION));
					$hash = Encryption::url($file);
					$link = $_HOST."/download/news/$hash/".$_NEWSLETTER;
					$fileTitle = $data['fileTitles'] && $data['fileTitles'][$file] ? $data['fileTitles'][$file] : "Download $extension"; 
					$fileTitle = substr($fileTitle, 0, 50);

					$files .= "<a class='blocklink' href='$link' target='_blank' >$fileTitle</a>";
				}
				
				$data['files'] = $files;
			}

			if ($data['image']) {
				$data['image'] = "<img src='{$data[image]}' class='img-responsive'>";
			} else {
				$data['class_no_image'] = "teaser-no-image";
			}

			// bind media frame
			if ($data['media']) {
				$data['media'] = "<iframe src='{$data[media]}' class='img-responsive' id='media-$tid' scrolling='no' frameborder='0' allowfullscreen='true' allowtransparency='true' width='320' height='213'></iframe>";
				$data['image'] = null;
			}

			// news authors
			if ($row['author_id']) {
				$key = $row['author_id'];
				$data['author'] = ' - Author: '.$_NEWS_AUTHORS[$key];
			}

			// retailnet authors
			if ($row['author_user_id']) {
				$key = $row['author_user_id'];
				$data['author'] = ' - Author: '.$_USER_AUTHORS[$key];
			}

			// article sticker
			if ($row['sticker']) {
				$sticker = unserialize($row['sticker']);
				$_ARTICLES[$sid]['articles'][$id]['sticker'] = $sticker;
			}

			$data['text'] = nl2br($data['text']);

			// body togler
			$data['readmore'] = $row['totalArticles'] > 1 ? 'Read More' : null;

			// content render
			$content = Content::render($teaser, $data, true);

			// group article contents
			$_ARTICLES[$sid]['articles'][$id]['content'] .= $content;
		}
	}
}


// split article in sections :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_ARTICLES) {

	foreach ($_ARTICLES as $sid => $section) {
						
		$_JSON['articles'] .= "<section>";
		$_JSON['articles'] .= "<div class='section-banner' data-id='$sid' ><h1>{$section[name]}</h1></div>";

		foreach ($section['articles'] as $id => $article) {

			$sticker = null;

			$totalArticles++;

			$new = $article['new'] ? "<span class='new-badge'>New</span>" : null;

			if ($article['sticker']) {

				if ($_PREVIEW && $_ARTICLE) {
					$_JSON['sticker'] = $article['sticker'];
				} else {
				
					$attributes = '';

					if ($article['id']) unset($article['id']);

					foreach ($article['sticker'] as $key => $value) {
						$value = $key=='width' || $key=='height' ? $value.'px' : $value;
						$attributes .= "$key:$value; ";
					}

					$sticker = "<div class='article-sticker' style='$attributes' ></div>";
				}
			}
			
			$_JSON['articles'] .= "
				<article id='article-$id' name='article-$id' class='news-article' data-id='$id'>
					<div class='article-title'>
						<h5>$new{$article[title]}</h5>
					</div>
					<div class='article-teaser'>{$article[content]}</div>
					<div class='article-content' id='article-body-$id'></div>
					<div class='article-bottom'>
						<div class='row'>
							<div class='col-xs-12'>
								<p class='text-right'><span class='badge add-print-basket'><i class='fa fa-book'></i></span></p>
							</div>
						</div>
					</div>
					$sticker
				</article>
			";
		}
		
		$_JSON['articles'] .= "</section>";
	}
}


// featured articles :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$filterState = $_PREVIEW ? null : " AND news_article_publish_state_id IN (4,5) ";

$result = $model->query("
	SELECT DISTINCT
		news_article_id AS id,
		news_article_title AS title,
		news_article_content_data AS data,
		news_category_name AS category
	FROM news_articles
	INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
	INNER JOIN news_categories ON news_article_category_id = news_category_id
	INNER JOIN newsletter_articles ON news_article_id = newsletter_article_article_id
	WHERE news_article_featured = 1 AND news_article_content_template_id = 1
	AND news_article_active = 1
	AND IF(news_article_expiry_date,  CURRENT_DATE <= news_article_expiry_date, 1) = 1
	AND newsletter_article_newsletter_id = $_NEWSLETTER
	$filterState
	ORDER BY 
		news_article_publish_date DESC, 
		news_article_title
	LIMIT 10
")->fetchAll();

if ($result) { 

	foreach ($result as $row) {

		$id = $row['id'];
		$access = true;

		// check article role and company restrictions
		if (!$_ACCESS_FULL && !$_PREVIEW) {

			$checkRolesAccess = true;
			$checkCompaniesAccess = true;

			// role restrictions
			if ($_ROLE_RESTRICTED_ARTICLES[$id]) {
				$roles = explode(',', $_ROLE_RESTRICTED_ARTICLES[$id]);
				$permittedRoles = array_intersect($roles, $userRoles);
				$checkRolesAccess = count($permittedRoles) > 0 ? true : false;
			}

			// company restrictions
			if (!$_ACCESS_FULL && !$_PREVIEW && $_COMPANY_RESTRICTED_ARTICLES[$id]) {
				$companies = explode(',', $_COMPANY_RESTRICTED_ARTICLES[$id]);
				$checkCompaniesAccess = in_array($user->address, $companies) ? true : false;
			}

			$access = $checkRolesAccess && $checkCompaniesAccess ? true : false;
		}

		if ($access) {
			$data = $row['data'] ? unserialize($row['data']) : array();
			$featured .= "<li>";
			$featured .= "<h5>{$row[category]}</h5>";
			$featured .= "<a href='#article-$id' ><img src='{$data[image]}' class='img-icon' /></a>";
			$featured .= "<p>{$row[title]}</p>";
			$featured .= "<div class='clearfix'></div>";
			$featured .= "</li>";
		}
	}

	if ($featured) {
		$content  = "<h3 class='section-title red'>Latest & Greatest</h3>";
		$content .= "<article>"; 
		$content .= "<ul class='list-unstyled'>";
		$content .= $featured;
		$content .= "</ul></article>";
		$content .= "<div class='clearfix'></div>";
		$_JSON['featured'] = $content;
	}
}

$_JSON['total'] = $totalArticles;

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

header('Content-Type: text/json');
echo json_encode($_JSON);
