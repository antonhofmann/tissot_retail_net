<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$_SECTION = $_REQUEST['section'];
$_NEWSLETTER = $_REQUEST['newsletter'];
$_ARTICLE = $_REQUEST['id'];

$user = User::instance();
$translate = Translate::instance();
$model = new Model('news');

$track = $model->db->prepare("
	INSERT INTO news_article_tracks (
		news_article_track_article_id,
		news_article_track_newsletter_id,
		news_article_track_user_id,
		news_article_track_type_id,
		news_article_track_entity
	)
	VALUES (?, ?, ?, ?, ?)
");

switch ($_SECTION) {
	
	case 'read-more':
		$track->execute(array($_ARTICLE, $_NEWSLETTER, $user->id, 47, null));
		break;	

	case 'hash':
		$track->execute(array($_ARTICLE, $_NEWSLETTER, $user->id, 47, null));
		break;	
}
