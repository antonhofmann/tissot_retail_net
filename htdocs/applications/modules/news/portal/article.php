<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc'; 

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'];
$_NEWSLETTER = $_REQUEST['newsletter'];
$_ARTICLE = $_REQUEST['article'];

$_JSON = array();

$model = new Model($_APPLICATION);

// get article templates
$sth = $model->db->prepare("
	SELECT DISTINCT
		news_article_content_id AS id,
		news_article_content_template_id AS tid,
		news_article_content_data AS data,
		news_article_template_file_view AS file
	FROM news_article_contents
	INNER JOIN news_article_templates ON news_article_content_template_id = news_article_template_id
	WHERE news_article_content_article_id = ? AND news_article_template_id != 1
	ORDER BY news_article_content_order
");

$sth->execute(array($_ARTICLE));
$result = $sth->fetchAll();

$templates = array();
$content = array();

if ($result) {
	
	foreach ($result as $row) {
		
		$id = $row['id'];
		$tid = $row['tid'];
		$filepath = $row['file'];

		// load template file
		if (!$templates[$filepath]) {
			if (file_exists($_SERVER['DOCUMENT_ROOT'].$filepath)) {
				$templates[$filepath] = File::load($filepath);
			}
		}

		$template = $templates[$filepath];

		if ($template) {

			$files = null;
			$data = $row['data'] ? unserialize($row['data']) : array();
			$data['id'] = $id;
			$data['article'] = $_ARTICLE;	

			if ($data['title']) {
				$data['title'] = nl2br($data['title']);
			}

			if ($data['text']) {
				$data['text'] = nl2br($data['text']);
			}

			if ($data['files']) {
				
				// pictures render
				if ($tid==3) {
					foreach ($data['files'] as $file) $files .= "<img src='$file' class='img-responsive' />";
					$data['files'] = $files;
				}
				else {
					
					foreach ($data['files'] as $file) {
						
						$extension = strtoupper(pathinfo($file, PATHINFO_EXTENSION));
						$hash = Encryption::url($file);
						$link = $_HOST."/download/news/$hash/".$_NEWSLETTER;

						$files .= "<a class='blocklink' href='$link' target='_blank' >Download $extension</a>";
					}

					$data['files'] = $files;
				}
			}

			if ($tid==5) {

				$headers = $data['dataloader']['settings']['colHeaders'];
				$sources = $data['dataloader']['sources'];

				$_JSON['sources'] = $sources;

				$table = "<table class='table table-condensed' >";

				if ($headers) {

					$table .= "<thead><tr>";

					foreach ($headers as $header) {
						$table .= "<th valign=top >$header</th>";
					}

					$table .= "</tr></thead>";

					$totalHeaders = count($headers);

					if ($sources) {

						$table .= "<tbody>";
						
						foreach ($sources as $i => $cols) {
							
							$table .= "<tr>";
							
							$colspan = $totalHeaders > 1 && count($cols)==1 ? "colspan=$totalHeaders" : null;

							foreach ($cols as $j => $value) {

								$imgs = Content::match($value, "#\[(.*?)\]#");

								if ($imgs) {
									foreach ($imgs as $img) {
										$image = file_exists($_SERVER['DOCUMENT_ROOT']."/data/news/images/$img") ? "<img src=\"/data/news/images/$img\" >" : null;
										$value = str_replace("[$img]", $image, $value);
									}
								}
								
								$table .= "<td $colspan valign=top >$value</td>";
							}

							$table .= "</tr>";
						}

						$table .= "</tbody>";
					}
				}

				$table .= "</table>";

				$data['table'] = $table;
			}

			$_JSON['content'] .= Content::render($template, $data, true);
		}
	}
}


header('Content-Type: text/json');
echo json_encode($_JSON);
