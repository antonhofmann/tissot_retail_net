<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$sessionKey = $_REQUEST['preview'] ? 'portal-preview' : 'portal'; 

$_APPLICATION = trim($_REQUEST['application']);
$_CONTROLLER = trim($_REQUEST['controller']);

if ($_REQUEST['load']) {
	$_REQUEST = array_merge($_REQUEST, Session::getFilter('news', $sessionKey));
}

$_REQUEST['controller'] = $_CONTROLLER;

// session request
$_REQUEST = session::filter('news', $sessionKey, false);

$_NEWSLETTER = trim($_REQUEST['newsletter']);
$_SEARCH = trim($_REQUEST['search']);
$_SECTION = trim($_REQUEST['section']);
$_CATEGORY = trim($_REQUEST['category']);
$_YEAR = trim($_REQUEST['year']);
$_MONTH = trim($_REQUEST['month']);
$_PREVIEW = trim($_REQUEST['preview']);


$_JSON = array();
$_JSON['request'] = $_REQUEST;

$model = new Model($_APPLICATION);


// filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_FILTERS = array();

$_FILTERS['active'] = "news_article_active = 1";
//$_FILTERS['expiry'] = "IF(news_article_expiry_date,  CURRENT_DATE <= news_article_expiry_date, 1) = 1";


if (!$_PREVIEW) {
	$_FILTERS['article.states'] = "news_article_publish_state_id IN (4,5)";
}

// search
if ($_SEARCH) {
	$_FILTERS['search'] = "(
		news_article_title LIKE '%$_SEARCH%'
		OR news_article_contact_information LIKE '%$_SEARCH%'
	)";
}

if ($_SECTION) {
	$_FILTERS['section'] = "news_section_id = $_SECTION";
}

if ($_CATEGORY) {
	$_FILTERS['category'] = "news_category_id = $_CATEGORY";
}

if ($_YEAR) {
	$_FILTERS['year'] = "YEAR(news_article_publish_date) = $_YEAR";
}

if ($_MONTH) {
	$_FILTERS['month'] = "MONTH(news_article_publish_date) = $_MONTH";
}

$_BINDS = array(
	'LEFT JOIN news_categories ON news_category_id = news_article_category_id',
	'LEFT JOIN news_sections ON news_section_id = news_category_section_id'
);


// dataloaders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// sections
$_JSON['section'] = $model->query("
	SELECT DISTINCT 
		news_section_id AS value, 
		news_section_name AS name
	FROM news_articles
")
->bind($_BINDS)
->filter($_FILTERS)
->filter('empty', "news_section_id > 0")
->exclude('section')
->exclude('search')
->extend(" OR news_section_id = '$_SECTION' ")
->order('name')
->fetchAll();

// categories
$_JSON['category'] = $model->query("
	SELECT DISTINCT 
		news_category_id AS value, 
		news_category_name AS name
	FROM news_articles
")
->bind($_BINDS)
->filter($_FILTERS)
->filter('empty', "news_category_id > 0")
->exclude('category')
->exclude('search')
->extend(" OR news_category_id = '$_CATEGORY' ")
->order('name')
->fetchAll();

// years
$_JSON['year'] = $model->query("
	SELECT DISTINCT 
		YEAR(news_article_publish_date) AS value, 
		YEAR(news_article_publish_date) AS name
	FROM news_articles
")
->bind($_BINDS)
->filter($_FILTERS)
->filter('notnull', "news_article_publish_date IS NOT NULL")
->exclude('year')
->exclude('search')
->order('name')
->fetchAll();

// month
$_JSON['month'] = $model->query("
	SELECT DISTINCT 
		MONTH(news_article_publish_date) AS value, 
		DATE_FORMAT(news_article_publish_date, '%M') AS name
	FROM news_articles
")
->bind($_BINDS)
->filter($_FILTERS)
->filter('notnull', "news_article_publish_date IS NOT NULL")
->exclude('month')
->exclude('search')
->order('value')
->fetchAll();


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

header('Content-Type: text/json');
echo json_encode($_JSON);
