<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$_JSON = array();
$_DATA = array();

$_ID = $_REQUEST['news_section_id'];
$_APPLICATION = $_REQUEST['application'] ?: 'news';

if ($_REQUEST['news_section_name']) {
	$_DATA['news_section_name'] = trim($_REQUEST['news_section_name']);
}

if ($_REQUEST['news_section_confirm_user_id']) {
	$_DATA['news_section_confirm_user_id'] = trim($_REQUEST['news_section_confirm_user_id']);
}

if ($_DATA) {

	$model = new Model($_APPLICATION);

	if ($_ID) {
		$_DATA['user_modified'] = $user->login;
		$query = Query::update('news_sections', 'news_section_id', $_DATA);
		$_DATA['news_section_id'] = $_ID;
	} else {

		// order
		$result = $model->query("SELECT COUNT(news_section_id) AS total FROM news_sections")->fetch();
		$_DATA['news_section_order'] = $result['total']+1;

		$_DATA['user_created'] = $user->login;
		$query = Query::insert('news_sections', $_DATA);
	}

	$sth = $model->db->prepare($query);
	$response = $sth->execute($_DATA);

	if ($response) $_JSON['success'] = "The secetion is succesfully submitted";
	else $_JSON['error'] = "The section is not submitted";

	if ($response && !$_ID) {
		$_ID = $model->db->lastInsertId();
		$_JSON['redirect'] = "/gazette/sections/data/$_ID";
	}
}

header('Content-Type: text/json');
echo json_encode($_JSON);