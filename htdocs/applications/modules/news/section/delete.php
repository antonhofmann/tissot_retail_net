<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$_JSON = array();

$_ID = $_REQUEST['id'];
$_APPLICATION = $_REQUEST['application'] ?: 'news';

$integrity = new Integrity();
$integrity->set($_ID, 'news_sections', $_APPLICATION);

if (User::permission('can_edit_news_system_data') && $_ID && $integrity->check()) {

	$model = new Model($_APPLICATION);

	$sth = $model->db->prepare("
		DELETE FROM news_sections
		WHERE news_section_id = ?
	"); 

	$response = $sth->execute(array($_ID));

	if ($response) {
		Message::success('The section is successfully deleted');
		$_JSON['redirect'] = "/gazette/sections";
	} else {
		$_JSON['error'] = "The section is not deleted. Please contact system administrator";
	}
}

header('Content-Type: text/json');
echo json_encode($_JSON);