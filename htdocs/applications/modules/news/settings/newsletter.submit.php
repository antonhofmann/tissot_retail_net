<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$_JSON = array();
$_DATA = array();

$_ID = $_REQUEST['newsletter_setting_id'];
$_APPLICATION = $_REQUEST['application'] ?: 'news';

if ($_REQUEST['newsletter_setting_property_id']) {
	$_DATA['newsletter_setting_property_id'] = trim($_REQUEST['newsletter_setting_property_id']);
}

if ($_REQUEST['newsletter_setting_field']) {
	$_DATA['newsletter_setting_field'] = trim($_REQUEST['newsletter_setting_field']);
}

if ($_REQUEST['newsletter_setting_value']) {
	$_DATA['newsletter_setting_value'] = trim($_REQUEST['newsletter_setting_value']);
}

if ($_DATA) {

	$model = new Model($_APPLICATION);

	if ($_ID) {
		$_DATA['user_modified'] = $user->login;
		$_DATA['date_modified'] = date('Y-m-d H:i:s');
		$query = Query::update('newsletter_settings', 'newsletter_setting_id', $_DATA);
		$_DATA['newsletter_setting_id'] = $_ID;
	} else {
		$_DATA['user_created'] = $user->login;
		$query = Query::insert('newsletter_settings', $_DATA);
	}

	$sth = $model->db->prepare($query);
	$response = $sth->execute($_DATA);

	if ($response) $_JSON['success'] = "Your action is succesfully submitted";
	else $_JSON['error'] = "Your action is not submitted, please contact system administrator";

	if ($response && !$_ID) {
		$_ID = $model->db->lastInsertId();
		$_JSON['redirect'] = "/gazette/settings/newsletter/$_ID";
	}
}

header('Content-Type: text/json');
echo json_encode($_JSON);