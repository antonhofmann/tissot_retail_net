<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$_RESPONSE = array();

$user = User::instance();
$translate = Translate::instance();

$_SECTION = $_REQUEST['section'];
$_FILE = $_REQUEST['file'];

// checkin :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_SECTION) {
	$_RESPONSE['error'] = "Action is not defined.";
	goto BLOCK_RESPNDING;
}

if (!$_FILE) {
	$_RESPONSE['error'] = "File is not defined.";
	goto BLOCK_RESPNDING;
}

// extend dirname with document root
$_FILE = $_SERVER['DOCUMENT_ROOT'].str_replace($_SERVER['DOCUMENT_ROOT'], null, $_FILE);

// action ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

switch ($_SECTION) {

	case 'add':

		$img = Upload::move($_FILE, "/data/news/images/");

		if ($img) {
			$_RESPONSE['file'] = $img;
			$_RESPONSE['name'] = "[".basename($img)."]";
		}

	break;

	case 'remove':

		if (file_exists($_FILE)) {
			@chmod($_FILE, 0666);
			$_RESPONSE['success'] = unlink($_FILE);
		}

	break;
}

// response ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPNDING:

header('Content-Type: text/json');
echo json_encode($_RESPONSE);