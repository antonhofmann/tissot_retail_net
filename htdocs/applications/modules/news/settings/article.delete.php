<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$_JSON = array();

$_ID = $_REQUEST['id'];
$_APPLICATION = $_REQUEST['application'] ?: 'news';

if (User::permission('can_edit_news_system_data') && $_ID) {

	$model = new Model($_APPLICATION);

	$sth = $model->db->prepare("
		DELETE FROM news_article_settings
		WHERE news_article_setting_id = ?
	"); 

	$response = $sth->execute(array($_ID));

	if ($response) {
		Message::success('The setting is successfully deleted');
		$_JSON['redirect'] = "/gazette/settings";
	} else {
		$_JSON['error'] = "The setting is not deleted, please contact system administrator";
	}
}

header('Content-Type: text/json');
echo json_encode($_JSON);