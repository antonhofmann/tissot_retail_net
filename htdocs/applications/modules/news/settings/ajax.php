<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'] ?: 'news';
$_SECTION = $_REQUEST['section'];
$_ID = $_REQUEST['id'];
$_VALUE = $_REQUEST['value'];
$_JSON = array();

$model = new Model($_APPLICATION);

switch ($_SECTION) {
	
	case 'article':
		
		$sth = $model->db->prepare("
			UPDATE news_article_settings SET
				news_article_setting_value = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE news_article_setting_id = ?
		");

		$_JSON['success'] = $sth->execute(array($_VALUE, $user->login, $_ID));

	break;	

	case 'newsletter':
		
		$sth = $model->db->prepare("
			UPDATE newsletter_settings SET
				newsletter_setting_value = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE newsletter_setting_id = ?
		");

		$_JSON['success'] = $sth->execute(array($_VALUE, $user->login, $_ID));

	break;

	case 'quicklinks':

		$field = $_REQUEST['field'];
		
		$sth = $model->db->prepare("
			UPDATE news_quicklinks SET
				$field = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE news_quicklink_id = ?
		");

		$_JSON['success'] = $sth->execute(array($_VALUE, $user->login, $_ID));

	break;
}


header('Content-Type: text/json');
echo json_encode($_JSON);