<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$_SECTION = $_REQUEST['section'];
$_ID = $_REQUEST['id'];

$user = User::instance();
$translate = Translate::instance();
$model = new Model('news');

// track type entry
$_VIEW_FROM_BROWSER = 19;

switch ($_SECTION) {
	
	case 'open-browser':
		
		$sth = $model->db->prepare("
			INSERT INTO newsletter_tracks (
				newsletter_track_newsletter_id,
				newsletter_track_user_id,
				newsletter_track_type_id
			)
			VALUES (?,?,?)
		");

		$sth->execute(array($_ID, $user->id, $_VIEW_FROM_BROWSER));

	break;
}
