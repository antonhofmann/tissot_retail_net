<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = 'news';
$_SECTION = $_REQUEST['section'];
$_ID = $_REQUEST['id'];

// response
$_JSON = array();

// db model
$model = new Model($_APPLICATION);

// permissions
$_PERMISSIONS = array(
	'edit' => user::permission('can_edit_newsletter'),
	'sent' => user::permission('can_sent_newsletter')
);

$_FIELD_TYPES = array(
	'newsletter_publish_date' => 'date',
	'newsletter_send_date' => 'date',
	'newsletter_sent_date' => 'date'
);


switch ($_SECTION) {

	case 'load':

		$sth = $model->db->prepare("
			SELECT 
				newsletter_id,
				newsletter_title,
				newsletter_text,
				newsletter_publish_state_id,
				newsletter_image,
				newsletter_files,
				newsletter_background,
				DATE_FORMAT(newsletter_publish_date, '%d.%m.%Y') AS newsletter_publish_date
			FROM newsletters
			WHERE newsletter_id = ?
		");

		$sth->execute(array($_ID));
		$newsletter = $sth->fetch();

		$state = $newsletter['newsletter_publish_state_id'];

		// fields
		$fields = $newsletter ? array_keys($newsletter) : array();

		// disable form
		if (!$_PERMISSIONS['edit']) {
			$_JSON['disabled'] = $fields ? array_fill_keys($fields, true) : array();
			$_JSON['remove'] = array('.fileinput-button', '.file-remover', '.article-remover');
		}

		// button ready to sent
		if ($_PERMISSIONS['edit'] && $state==1) {
			$_JSON['buttons'][] = array(
				'title' => 'Ready to Send',
				'data' => array('state'=>2),
				'attributes' => array(
					'id' => 'btn-complete',
					'class' => 'btn btn-primary btn-state',
					'href' => '/applications/modules/news/newsletter/complete.php'
				)
			); 
		}

		// button sent
		if ($_PERMISSIONS['sent'] && $state==2) {
			$_JSON['buttons'][] = array(
				'title' => 'Publish',
				'icon' => '<i class="fa fa-globe"></i> ',
				'data' => array(
					'state'=>3
				),
				'attributes' => array(
					'id' => 'btn-publish',
					'class' => 'btn btn-success btn-state btn-dialog',
					'href' => '/applications/modules/news/newsletter/publish.php'
				)
			); 
		}

		// button delete
		if ($_PERMISSIONS['edit'] && $state < 3) {
			$_JSON['buttons'][] = array(
				'title' => 'Delete',
				'attributes' => array(
					'id' => 'btn-delete',
					'class' => 'btn btn-danger btn-dialog',
					'href' => "/applications/modules/news/newsletter/delete.php?id=$_ID"
				)
			); 
		}

		// button preview
		$_JSON['buttons'][] = array(
			'title' => 'Preview',
			'icon' => '<i class="fa fa-external-link"></i> ',
			'attributes' => array(
				'id' => 'btn-preview',
				'class' => 'btn btn-info',
				'href' => "/gazette/newsletters/preview/$_ID",
				'target' => '_blank'
			)
		); 

		$_JSON['data'] = $newsletter;

		// get newsletter articles
		$sth = $model->db->prepare("
			SELECT DISTINCT
				news_article_id AS id,
				CONCAT(user_firstname, ' ', user_name) AS author,
				news_article_title AS title,
				news_category_name AS category,
				news_workflow_state_name AS state,
				IF(news_article_featured, 'Yes', 'No') AS featured,
				news_article_content_data AS teaser,
				newsletter_article_send_date AS sent,
				DATE_FORMAT(news_article_publish_date, '%d.%m.%Y') AS publish_date,
				DATE_FORMAT(news_article_expiry_date, '%d.%m.%Y') AS expiry_date,
				DATE_FORMAT(news_article_confirmed_date, '%d.%m.%Y') AS confirmed_date,
				DATE_FORMAT(news_articles.date_created, '%d.%m.%Y') AS created_date
			FROM news_articles
			INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
			INNER JOIN db_retailnet.users ON user_id = news_article_owner_user_id
			INNER JOIN news_workflow_states ON news_workflow_state_id = news_article_publish_state_id
			LEFT JOIN news_categories ON news_category_id = news_article_category_id
			LEFT JOIN newsletter_articles ON newsletter_article_article_id = news_article_id
			WHERE news_article_active=1 
			AND news_article_content_template_id = 1
			AND newsletter_article_newsletter_id = ?
			ORDER BY news_article_title
		");

		$sth->execute(array($_ID));
		$result = $sth->fetchAll();

		if ($result) {

			foreach ($result as $article) {

				$id = $article['id'];

				$article['teaser'] = $article['teaser'] ? unserialize($article['teaser']) : array();

				if (!$_PERMISSIONS['edit'] || $article['sent']) {
					$_JSON['remove'][] = ".panel-box.article-$id .action";
				}

				$_JSON['articles'][$id] = $article;
			}
		}


		// get newsletter files
		$sth = $model->db->prepare("
			SELECT newsletter_file_path AS file, newsletter_file_title AS title
			FROM newsletter_files
			WHERE newsletter_file_newsletter_id = ?
		");

		$sth->execute(array($_ID));
		$_JSON['files']['newsletter_files'] = $sth->fetchAll();


		// get settings
		$sth = $model->db->prepare("
			SELECT DISTINCT
				newsletter_setting_id AS id,
				setting_property_name AS property,
				newsletter_setting_field AS field,
				newsletter_setting_value AS value
			FROM newsletter_settings
			INNER JOIN db_retailnet.setting_properties ON setting_property_id = newsletter_setting_property_id
		");

		$sth->execute();
		$result = $sth->fetchAll();

		if ($result) {
			
			foreach ($result as $row) {

				$field = $row['field'];
				$value = $row['value'];

				switch ($row['property']) {

					case 'maxlength':

						if ($value > 0) {
							$_JSON['settings'][$field] = array(
								'className' => 'has-popover', 
								'maxlength' => $value,
								'data' => array(
									'toggle' => 'popover',
									'content' => "Max. length $value"
								)
							);
						}
						
					break;
				}
			}
		}


	break;

	case 'save':

		$field = $_REQUEST['field'];
		$value = $_REQUEST['value'];
		$value = $value ?: null;

		// sql date format
		if ($value && $_FIELD_TYPES[$field]=='date') {
			$value = date::sql($value);
		}

		// track: update newsletter [2]
		$track = $model->db->prepare("
			INSERT INTO newsletter_tracks (
				newsletter_track_newsletter_id,
				newsletter_track_user_id,
				newsletter_track_type_id,
				newsletter_track_entity
			)
			VALUES (?,?,?,?)
		");

		$sth = $model->db->prepare("
			UPDATE newsletters SET 
				$field = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE newsletter_id = ?
		");

		$response = $sth->execute(array($value, $user->login, $_ID));
		
		if ($response) {

			$track->execute(array($_ID, $user->id, 2, $field));

			// set send user id for terminated newsletters
			if ($field=='newsletter_send_date') {
				
				$sth = $model->db->prepare("
					UPDATE newsletters SET 
						newsletter_send_user_id = ?
					WHERE newsletter_id = ?
				");

				$sth->execute(array($user->id, $_ID));
			}
		}

		$_JSON['success'] = $response;

	break;

	case 'files':

		$path = $_REQUEST['path'];

		// read newsletter
		$newsletter = new Modul($_APPLICATION);
		$newsletter->setTable('newsletters');
		$newsletter->read($_ID);

		if ($newsletter->id && $path) { 

			$handler = new UploadHandler(array(
				'path' => $path,
				'upload_dir' => $_SERVER['DOCUMENT_ROOT'].$path,
				'upload_url' => '//'.$_SERVER['SERVER_NAME'].$path
			));

			$sth = $model->db->prepare("
				INSERT INTO newsletter_files (
					newsletter_file_newsletter_id,
					newsletter_file_path,
					user_created,
					date_created
				) 
				VALUES (?,?,?,NOW())
			");

			$file = $path.$_FILES['files']['name'][0];
			$filename = basename($file);
			$file = str_replace($filename, preg_replace("/[^a-zA-Z0-9.]/", "_", $filename), $file);
			$file = strtolower($file);
			
			$response = $sth->execute(array($_ID, $file, $user->login));

			if ($response) {

				// track: Add newsletter file [7]
				$track = $model->db->prepare("
					INSERT INTO newsletter_tracks (
						newsletter_track_newsletter_id,
						newsletter_track_user_id,
						newsletter_track_type_id,
						newsletter_track_entity
					)
					VALUES (?,?,?,?)
				");

				$track->execute(array($_ID, $user->id, 7, $file));
			}

		} else {
			$_JSON['error'] = "Newsletter not found";
		}

	break;

	case 'background':

		$path = $_REQUEST['path'];

		// read newsletter
		$newsletter = new Modul($_APPLICATION);
		$newsletter->setTable('newsletters');
		$newsletter->read($_ID);

		if ($newsletter->id && $path) {

			//Dir::remove($path);

			$handler = new UploadHandler(array(
				'path' => $path,
				'param_name' => 'newsletter_background',
				'upload_dir' => $_SERVER['DOCUMENT_ROOT'].$path,
				'upload_url' => '//'.$_SERVER['SERVER_NAME'].$path
			));

			$file = $path.$_FILES['newsletter_background']['name'];
			$filename = basename($file);
			$file = str_replace($filename, preg_replace("/[^a-zA-Z0-9.]/", "_", $filename), $file);
			$file = strtolower($file);
			
			if (file_exists($_SERVER['DOCUMENT_ROOT'].$file)) {

				// save file in database
				$sth = $model->db->prepare("
					UPDATE newsletters SET 
						newsletter_background = ?,
						user_modified = ?,
						date_modified = NOW()
					WHERE newsletter_id = ?
				");

				$response = $sth->execute(array($file, $user->login, $_ID));

				// track: Add newsletter backgrund file
				if ($response) {
					
					$track = $model->db->prepare("
						INSERT INTO newsletter_tracks (
							newsletter_track_newsletter_id,
							newsletter_track_user_id,
							newsletter_track_type_id,
							newsletter_track_entity
						)
						VALUES (?,?,?,?)
					");

					$track->execute(array($_ID, $user->id, 64, $file));
				}
			}
			else $_JSON['error'] = "File $file isn't uploaded.";

		} else {
			$_JSON['error'] = "File ";
		}

	break;

	case 'background-remove':

		$file = trim($_REQUEST['file']);

		// read newsletter
		$newsletter = new Modul($_APPLICATION);
		$newsletter->setTable('newsletters');
		$newsletter->read($_ID);

		if ($newsletter->id && $file) { 

			// track: remove newsletter file [8]
			$track = $model->db->prepare("
				INSERT INTO newsletter_tracks (
					newsletter_track_newsletter_id,
					newsletter_track_user_id,
					newsletter_track_type_id,
					newsletter_track_entity
				)
				VALUES (?,?,?,?)
			");

			$sth = $model->db->prepare("
				UPDATE newsletters SET 
					newsletter_background = NULL,
					user_modified = ?,
					date_modified = NOW()
				WHERE newsletter_id = ?
			");

			$response = $sth->execute(array($user->login, $_ID));

			if ($response) {
				$_JSON['success'] = true;
				$track->execute(array($_ID, $user->id, 66, $file));
			}

			$path = $_SERVER['DOCUMENT_ROOT'].$file; 

			if (file_exists($path)) {
				@chmod($path, 0666);
				@unlink($path);
			}
			else $_JSON['warning'] = "File not found."; 
		}

	break;

	case 'file-title':

		$file = trim($_REQUEST['file']);
		$title = trim($_REQUEST['title']);

		// read newsletter
		$newsletter = new Modul($_APPLICATION);
		$newsletter->setTable('newsletters');
		$newsletter->read($_ID);

		if (!$newsletter->id || !$file || !$title) {
			$_JSON['error'] = "Bed request";
			break;
		}

		$sth = $model->db->prepare("
			UPDATE newsletter_files SET
				newsletter_file_title = ?
			WHERE newsletter_file_newsletter_id = ? AND newsletter_file_path = ?
		");

		$response = $sth->execute(array($title, $_ID, $file));

		if ($response) {

			$track = $model->db->prepare("
				INSERT INTO newsletter_tracks (
					newsletter_track_newsletter_id,
					newsletter_track_user_id,
					newsletter_track_type_id,
					newsletter_track_entity
				) VALUES (?,?,?,?)
			");

			$track->execute(array($_ID, $user->id, 8, $file));
			$_JSON['success'] = true;

		} else {
			$_JSON['error'] = "System Error. Please, contact customer services.";
			$_JSON['errorInfo'] = $sth->errorInfo();
		}

	break;

	case 'file-remove':

		$file = trim($_REQUEST['file']);

		// read newsletter
		$newsletter = new Modul($_APPLICATION);
		$newsletter->setTable('newsletters');
		$newsletter->read($_ID);

		if (!$newsletter->id || !$file) {
			$_JSON['error'] = "Bed request";
			break;
		}

		$sth = $model->db->prepare("
			DELETE FROM newsletter_files
			WHERE newsletter_file_newsletter_id = ? AND newsletter_file_path = ?
		");

		$response = $sth->execute(array($newsletter->id, $file));

		if ($response) {

			$_JSON['success'] = true;

			// track: remove newsletter file [8]
			$track = $model->db->prepare("
				INSERT INTO newsletter_tracks (
					newsletter_track_newsletter_id,
					newsletter_track_user_id,
					newsletter_track_type_id,
					newsletter_track_entity
				)
				VALUES (?,?,?,?)
			");

			$track->execute(array($_ID, $user->id, 8, $file));
			
			$path = $_SERVER['DOCUMENT_ROOT'].$file; 
			@chmod($path, 0666);
			@unlink($path);
		}
		else $_JSON['error'] = "System error.";

	break;

	case 'articles':

		$tab = $_REQUEST['tab'];

		if ($tab=="unassigned") {

			$sth = $model->db->prepare("
				SELECT DISTINCT
					news_article_id AS id,
					CONCAT(user_firstname, ' ', user_name) AS author,
					news_article_title AS title,
					news_category_name AS category,
					news_workflow_state_name AS state,
					IF(news_article_featured, 'Yes', 'No') AS featured,
					news_article_content_data AS teaser,
					DATE_FORMAT(news_article_publish_date, '%d.%m.%Y') AS publish_date,
					DATE_FORMAT(news_article_expiry_date, '%d.%m.%Y') AS expiry_date,
					DATE_FORMAT(news_article_confirmed_date, '%d.%m.%Y') AS confirmed_date,
					DATE_FORMAT(news_articles.date_created, '%d.%m.%Y') AS created_date
				FROM news_articles
				INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
				INNER JOIN db_retailnet.users ON user_id = news_article_owner_user_id
				INNER JOIN news_workflow_states ON news_workflow_state_id = news_article_publish_state_id
				LEFT JOIN news_categories ON news_category_id = news_article_category_id
				LEFT JOIN newsletter_articles ON newsletter_article_article_id = news_article_id
				WHERE news_article_active=1 
				AND news_article_content_template_id = 1
				AND newsletter_article_article_id IS NULL
				ORDER BY news_article_title
			");

			$sth->execute();
			$result = $sth->fetchAll();

			if ($result) {
				foreach ($result as $article) {
					$id = $article['id'];
					$article['teaser'] = $article['teaser'] ? unserialize($article['teaser']) : array();
					$article['type'] = "unassigned";
					$_JSON[$id] = $article;
				}
			}

		} 
		elseif ($tab=="other") {

			// get current articles
			$sth = $model->db->prepare("
				SELECT GROUP_CONCAT(DISTINCT newsletter_article_article_id) AS articles
				FROM newsletter_articles
				WHERE newsletter_article_newsletter_id = ?
			");

			$sth->execute(array($_ID));
			$result = $sth->fetch();

			if ($result['articles']) {
				$filter = "AND news_article_id NOT IN ({$result[articles]})";
			}

			$sth = $model->db->prepare("
				SELECT DISTINCT
					news_article_id AS id,
					CONCAT(user_firstname, ' ', user_name) AS author,
					news_article_title AS title,
					news_category_name AS category,
					news_workflow_state_name AS state,
					IF(news_article_featured, 'Yes', 'No') AS featured,
					news_article_content_data AS teaser,
					DATE_FORMAT(news_article_publish_date, '%d.%m.%Y') AS publish_date,
					DATE_FORMAT(news_article_expiry_date, '%d.%m.%Y') AS expiry_date,
					DATE_FORMAT(news_article_confirmed_date, '%d.%m.%Y') AS confirmed_date,
					DATE_FORMAT(news_articles.date_created, '%d.%m.%Y') AS created_date
				FROM news_articles
				INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
				INNER JOIN db_retailnet.users ON user_id = news_article_owner_user_id
				INNER JOIN news_workflow_states ON news_workflow_state_id = news_article_publish_state_id
				LEFT JOIN news_categories ON news_category_id = news_article_category_id
				INNER JOIN newsletter_articles ON newsletter_article_article_id = news_article_id
				WHERE news_article_active=1 
				AND news_article_content_template_id = 1
				AND newsletter_article_newsletter_id != ? AND news_article_multiple = 1
				$filter
				ORDER BY news_article_title
			");

			$sth->execute(array($_ID));
			$result = $sth->fetchAll();

			if ($result) {
				foreach ($result as $article) {
					$id = $article['id'];
					$article['teaser'] = $article['teaser'] ? unserialize($article['teaser']) : array();
					$article['type'] = "other";
					$_JSON[$id] = $article;
				}
			}

		} else {
			$_JSON['error'] = "Article tab not defined";
		}

	break;

	case 'workflow-states':

		$sth = $model->db->prepare("
			SELECT newsletter_workflow_state_id, newsletter_workflow_state_name
			FROM newsletter_workflow_states
			ORDER BY newsletter_workflow_state_order
		");

		$sth->execute();
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$id = $row['newsletter_workflow_state_id'];
				$_JSON[$id] = $row['newsletter_workflow_state_name'];
			}
		}

	break;

	default:
		$_JSON['error'] = "Section not defined.";
	break;
}


if ($_JSON) {
	header('Content-Type: text/json');
	echo json_encode($_JSON);
}