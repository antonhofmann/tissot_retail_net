<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
include_once $_SERVER['DOCUMENT_ROOT'].'/utilities/simpleimage.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/vendor/phpoffice/phpword/vendor/autoload.php';

ini_set('max_execution_time', 240);
ini_set('memory_limit', '2048M');

$_ID = $_REQUEST['newsletter'];

$sessionKey = $_REQUEST['preview'] ? 'portal-preview' : 'portal'; 
$_REQUEST = array_merge($_REQUEST, Session::getFilter('news', $sessionKey));

$_SEARCH = trim($_REQUEST['search']);
$_SECTION = trim($_REQUEST['section']);
$_CATEGORY = trim($_REQUEST['category']);
$_YEAR = trim($_REQUEST['year']);
$_MONTH = trim($_REQUEST['month']);
$_PREVIEW = trim($_REQUEST['preview']);
$_CONTROLLER = $_REQUEST['controller'];

$_ERRORS = array();
$_JSON = array();

$_HOST = 'http://'.$_SERVER['SERVER_NAME'];

$user = User::instance();
$translate = Translate::instance();
$db = Connector::get(Connector::DB_RETAILNET_NEWS);


// permissions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PERMISSIONS = array(
    'view.all' => user::permission('can_view_all_news_articles'),
    'view.his' => user::permission('can_view_only_his_news_articles'),
    'edit.all' => user::permission('can_edit_all_news_articles'),
    'edit.his' => user::permission('can_edit_only_his_news_articles')
);

$_CAN_VIEW = $_PERMISSIONS['view.all'] || $_PERMISSIONS['view.his'] ? true : false;
$_ACCESS_FULL = $_PERMISSIONS['view.all'] || $_PERMISSIONS['edit.all'] ? true : false;


// filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_REQUEST['articles']) {
    $articles = join(',', $_REQUEST['articles']);
    $_FILTERS['articles'] = "news_article_id IN ($articles)";
} 


if (!$_REQUEST['articles'] && $_CONTROLLER=='search') {
    
    if ($_SEARCH) {
        $_FILTERS['search'] = "(
            news_article_title LIKE '%$_SEARCH%'
            OR news_article_contact_information LIKE '%$_SEARCH%'
            OR news_article_content_data LIKE '%$_SEARCH%'
        )";
    }

    if ($_SECTION) {
        $_FILTERS['section'] = "news_section_id = $_SECTION";
    }

    if ($_CATEGORY) {
        $_FILTERS['category'] = "news_category_id = $_CATEGORY";
    }

    if ($_YEAR) {
        $_FILTERS['year'] = "YEAR(news_article_publish_date) = $_YEAR";
    }

    if ($_MONTH) {
        $_FILTERS['month'] = "MONTH(news_article_publish_date) = $_MONTH";
    }
}

// newsletter ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NEWSLETTER = array();

if (!$_REQUEST['articles'] && $_CONTROLLER<>'search') {

    if ($_ID) {
        $sql = "
            SELECT 
                newsletter_id AS id,
                newsletter_number AS number,
                newsletter_title AS title,
                newsletter_image AS image,
                newsletter_text AS content,
                DATE_FORMAT(newsletter_publish_date, '%m/%d/%Y') AS date
            FROM newsletters
            WHERE newsletter_id = $_ID
        ";
    } else {
        $sql = "
            SELECT 
                newsletter_id AS id,
                newsletter_number AS number,
                newsletter_title AS title,
                newsletter_image AS image,
                newsletter_text AS content,
                DATE_FORMAT(newsletter_publish_date, '%m/%d/%Y') AS date
            FROM newsletters
            WHERE newsletter_publish_state_id = 3
            ORDER BY newsletter_publish_date DESC
            LIMIT 1
        ";
    }

    $sth = $db->prepare($sql);
    $sth->execute();
    $_NEWSLETTER = $sth->fetch();

    $_FILTERS['newsletter'] = "newsletter_article_newsletter_id = ".$_NEWSLETTER['id'];
}


// newsletter files ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NEWSLETTER_FILES = array();

if (!$_ERRORS && $_NEWSLETTER) {
    
    $sth = $db->prepare("
        SELECT newsletter_file_path AS file
        FROM newsletter_files
        WHERE newsletter_file_newsletter_id = ?
    ");

    $sth->execute(array($_NEWSLETTER['id']));
    $_NEWSLETTER_FILES = $sth->fetchAll();
}


// news authors ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NEWS_AUTHORS = array();

if (!$_ERRORS) {

    $sth = $db->prepare("
        SELECT DISTINCT
            news_author_id AS id,
            CONCAT(' - ', news_author_first_name, ' ', news_author_name) AS name
        FROM news_articles
        INNER JOIN news_authors ON news_author_id = news_article_author_id
        WHERE news_article_active = 1 AND news_article_publish_state_id IN (4,5)
    ");

    $sth->execute();
    $result = $sth->fetchAll();
    $_NEWS_AUTHORS = _array::extract($result);
}

// retalnet authors ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_AUTHORS = array();

if (!$_ERRORS) {

    $sth = $db->prepare("
        SELECT DISTINCT
            user_id AS id,
            CONCAT(' - ', user_firstname, ' ', user_name) AS name
        FROM news_articles
        INNER JOIN db_retailnet.users ON user_id = news_article_author_user_id
        WHERE news_article_active = 1 AND news_article_publish_state_id IN (4,5)
    ");

    $sth->execute();
    $result = $sth->fetchAll();
    $_AUTHORS = _array::extract($result);
}

// role restrictions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ARTICLE_ROLES = array();
$_USER_ROLES = $user->roles() ?: array();

if (!$_ERRORS && !$_PREVIEW) {

    $sth = $db->prepare("
        SELECT DISTINCT
            news_article_id AS id,
            GROUP_CONCAT(news_article_role_role_id) AS roles
        FROM newsletter_articles
        INNER JOIN news_articles ON news_article_id = newsletter_article_article_id
        INNER JOIN news_article_roles ON news_article_role_article_id = news_article_id
        WHERE newsletter_article_newsletter_id = ?
        AND news_article_active = 1
        AND news_article_publish_state_id IN (4,5)
        GROUP BY news_article_id
    ");

    $sth->execute(array($_NEWSLETTER['id']));
    $result = $sth->fetchAll();
    $_ARTICLE_ROLES = _array::extract($result);
}


// company restrictions ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ARTICLE_COMPANIES = array();

if (!$_ERRORS && !$_PREVIEW) {

    $sth = $db->prepare("
        SELECT DISTINCT
            news_article_id AS id,
            GROUP_CONCAT(news_article_address_address_id) AS addresses
        FROM newsletter_articles
        INNER JOIN news_articles ON news_article_id = newsletter_article_article_id
        INNER JOIN news_article_addresses ON news_article_address_article_id = news_article_id
        WHERE newsletter_article_newsletter_id = 1
        AND news_article_active = 1
        AND news_article_publish_state_id IN (4,5)
        GROUP BY news_article_id
    ");

    $sth->execute(array($_NEWSLETTER['id']));
    $result = $sth->fetchAll();
    $_ARTICLE_COMPANIES = _array::extract($result);
}


// articles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ARTICLES = array();

$articleFilters = $_FILTERS ? ' AND '.join(' AND ', array_values($_FILTERS)) : null;

$articlesOrder = $_NEWSLETTER['id']
    ? "news_section_order, news_section_name, newsletter_article_sort, news_article_publish_date DESC, news_article_title, news_article_content_order" 
    : "news_section_order, news_section_name, news_article_publish_date DESC, news_article_title, news_article_content_order";

if (!$_ERRORS) {

    $sth = $db->prepare("
        SELECT DISTINCT
            news_article_id AS id,
            news_article_title AS title,
            DATE_FORMAT(news_article_publish_date, '%m/%d/%Y') AS date,
            news_article_featured AS featured,
            news_article_new AS new,
            news_article_contact_information AS informations,
            news_article_author_id AS author_id,
            news_article_author_user_id AS author_user_id,
            news_section_id AS sid,
            news_section_name AS section,
            news_category_id AS cid,
            news_category_name AS category,
            news_article_content_id AS tid,
            news_article_content_data AS data,
            news_article_content_template_id AS template
        FROM newsletter_articles
        INNER JOIN news_articles ON news_article_id = newsletter_article_article_id
        INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
        INNER JOIN news_article_templates ON news_article_content_template_id = news_article_template_id
        INNER JOIN news_categories ON news_article_category_id = news_category_id
        INNER JOIN news_sections ON news_section_id = news_category_section_id
        WHERE news_article_active = 1 AND IF(news_article_expiry_date,  CURRENT_DATE <= news_article_expiry_date, 1) = 1 
        $articleFilters
        ORDER BY $articlesOrder
    ");

    $sth->execute();
    $result = $sth->fetchAll();

    if (!$result) {
        $_ERRORS[] = "Articles not found";
        goto BLOCK_RESPONSE;
    }
}


// content parser ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ARTICLES_DENIED = array();
$_TEASER = array();

if (!$_ERRORS) {

    foreach ($result as $row) {

        $id = $row['id'];
        $sid = $row['sid'];
        $tid = $row['tid'];

        $access = true;

        // check article role and company restrictions
        if (!$_ACCESS_FULL && !$_PREVIEW) {

            $checkRolesAccess = true;
            $checkCompaniesAccess = true;
            
            // role restrictions
            if ($_ARTICLE_ROLES[$id]) {
                $roles = explode(',', $_ARTICLE_ROLES[$id]);
                $permittedRoles = array_intersect($roles, $_USER_ROLES);
                $checkRolesAccess = count($permittedRoles) > 0 ? true : false;
            }

            // company restrictions
            if ($_ARTICLE_COMPANIES[$id]) {
                $companies = explode(',', $_ARTICLE_COMPANIES[$id]);
                $checkCompaniesAccess = in_array($user->address, $companies) ? true : false;
            }

            $access = $checkRolesAccess && $checkCompaniesAccess ? true : false;
        }

        if ($access) {

            $_ARTICLES[$sid]['name'] = $row['section'];
            $_ARTICLES[$sid]['articles'][$id]['title'] = $row['title'];

            $data = $row['data'] ? unserialize($row['data']) : array();
            
            $data['template'] = $row['template'];

            // teaser
            if ($row['template']==1) {
                
                $author = null;
                $info = null;

                $_TEASER[$id] = $tid;

                // news authors
                if ($row['author_id'] && $_NEWS_AUTHORS) {
                    $key = $row['author_id'];
                    $author = '- Author: '.$_NEWS_AUTHORS[$key];
                }

                // retailnet authors
                if ($row['author_user_id'] && $_AUTHORS) {
                    $key = $row['author_user_id'];
                    $author = '- Author: '.$_AUTHORS[$key];
                }

                if ($row['date']) {
                    $info .= "Date: {$row[date]} $author";
                }

                if ($row['informations']) {
                    $info .= "<br />".nl2br($row['informations']);
                }

                $data['info'] = "<p>$info</p>";
            }

            // group article contents
            $_ARTICLES[$sid]['articles'][$id]['templates'][$tid] = $data;
        } 
        else {
            $_ARTICLES_DENIED[] = $id;
        }
    }
}

// simpleImage extending :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

class mySimpleImage extends SimpleImage {

    public function getBestFit($max_width, $max_height) {

        // If it already fits, there's nothing to do
        if ($this->width <= $max_width && $this->height <= $max_height) {
            return array(
	        	'width' => $this->width, 
	        	'height' => $this->height
	        );
        }

        // Determine aspect ratio
        $aspect_ratio = $this->height / $this->width;

        // Make width fit into new dimensions
        if ($max_width && $this->width > $max_width) {
            $width = $max_width;
            $height = $width * $aspect_ratio;
        } else {
            $width = $this->width;
            $height = $this->height;
        }

        // Make height fit into new dimensions
        if ($max_height && $height > $max_height) {
            $height = $max_height;
            $width = $height / $aspect_ratio;
        }

        return array(
        	'width' => $width, 
        	'height' => $height
        );

    }
}


// phpword document ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

\PhpOffice\PhpWord\Autoloader::register();

$phpWord = new \PhpOffice\PhpWord\PhpWord();

$phpWord->setDefaultFontName('century gothic');
$phpWord->setDefaultFontSize(12);

$phpWord->setDefaultParagraphStyle(
    array(
        'align'      => 'left',
        'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(12)
    )
);

// document properties :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$properties = $phpWord->getDocInfo();
$properties->setCreator('Swatch Retail Net');
$properties->setCompany('Swatch AG');
$properties->setTitle('My title');
$properties->setDescription('My description');
$properties->setCategory('My category');
$properties->setLastModifiedBy('My name');
$properties->setCreated(mktime(0, 0, 0, 3, 12, 2014));
$properties->setModified(mktime(0, 0, 0, 3, 14, 2014));
$properties->setSubject('My subject');
$properties->setKeywords('my, key, word');

$sectionWidth = \PhpOffice\PhpWord\Shared\Converter::cmToTwip(19);
$_TEASER_IMAGE_TWIP = \PhpOffice\PhpWord\Shared\Converter::cmToTwip(5) + 1200;
$_TEASER_IMAGE_WIDTH = \PhpOffice\PhpWord\Shared\Converter::cmToPixel(5);
$_TEASER_CONTENT_TWIP = $sectionWidth - $_TEASER_IMAGE_TWIP;
$_IMAGE_MAX_FULL_WIDTH = $maxWidth = \PhpOffice\PhpWord\Shared\Converter::cmToPixel(14.2);
$pageMargin = \PhpOffice\PhpWord\Shared\Converter::cmToTwip(1);

$basciSection = array(
    'marginLeft'   => $pageMargin,
    'marginRight'  => $pageMargin,
    'marginTop'    => $pageMargin,
    'marginBottom' => $pageMargin,
    'headerHeight' => 50,
    'footerHeight' => 50,
);

$fontStyleCategoryName = array(
    'bold' => true, 
    'align' => 'left', 
    'color' => 'ffffff', 
    'size' => 20,
    'lineHeight' => 1
);

$fontStyleArticleTitle = array(
    'align' => 'left', 
    'color' => 'A6A6A6', 
    'size' => 18
);

$fontStyleTeaserTitle = array(
    'align' => 'left', 
    'color' => '000000', 
    'size' => 14
);

$styleBasicCell = array(
    'valign' => 'top', 
    'borderSize' => 0,
    'border' => 0
);

$styleFileCell = array(
    'valign' => 'center',
    'bgColor' => '666666',
    'borderSize' => 4, 
    'borderBottomSize' => 30,
    'borderColor' => '6666666', 
    'borderTopColor' => '666666',
    'borderRightColor' => '666666',
    'borderBottomColor' => 'FFFFFF',
    'borderLeftColor' => '666666'
);

$fontStyleFileCell = array(
    'bold' => true,
    'color' => 'FFFFFF',
    'borderSize' => 5,
    'borderTopColor' => '666666',
    'borderRightColor' => '666666',
    'borderBottomColor' => 'FFFFFF',
    'borderLeftColor' => '666666'
);

$styleTeaserImage = array(
    'borderSize' => 5,
    'borderTopSize' => 10,
    'borderColor' => '666666',
    'spaceAfter' => 0
);

$phpWord->addTableStyle('basicTable', array(
    'width' => '100', 
    'borderSize' => 0, 
    'cellMargin' => 0,
    'borderColor' => 'ffffff'
));

$phpWord->addTableStyle('categoryName', array(
    'width' => '100', 
    'borderSize' => 0, 
    'cellMargin' => 80, 
    'borderColor' => 'ff0000', 
    'bgColor' => 'ff0000'
));

$phpWord->addTableStyle('fileTable', array(
    'borderSize' => 0, 
    'cellMargin' => 80
));

$phpWord->addTableStyle('downloadButton', 
    array(
        'cellMargin' => 80,
        'bgColor' => '666666',
        'border' => 0,
        'vMerge' => 'restart'
    )
);

$cellRowSpan = array('vMerge' => 'restart', 'valign' => 'top');
$cellRowContinue = array('vMerge' => 'continue');
$cellColSpan = array('gridSpan' => 3, 'valign' => 'center');
$noBreakRow = array('cantSplit' => true);

// newsletter visual :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


if ($_NEWSLETTER['id']) {

    $section = $phpWord->addSection($basciSection);

    $visualTitle = "Gazette #{$_NEWSLETTER[number]}";
    $visualPicture = $_NEWSLETTER['image'] ? $_SERVER['DOCUMENT_ROOT'].$_NEWSLETTER['image'] : null;

    // gazette number
    $section->addText($visualTitle, 
        array("size" => 24),
        array(
            'align' => 'center',
            'spaceBefore' => 400,
            'spaceAfter' => 1000
        )
    );
    
    // visual banner
    if (file_exists($visualPicture)) {

        $maxWidth = \PhpOffice\PhpWord\Shared\Converter::cmToPixel(14.2);
        $si = new mySimpleImage();
        $si->load($visualPicture);
        $imgSize = $si->getBestFit($maxWidth, 0);
        
        $section->addImage($visualPicture, array(
            'width'            => $imgSize['width'],
            'height'           => $imgSize['height'],
            'marginLeft'       => 0,
            'marginTop'        => 0
        ));

        $section->addTextBreak(1);
    }

    $section->addTextBreak(1);
    
    // visual title
    if ($_NEWSLETTER['title']) {
        $section->addText(htmlspecialchars($_NEWSLETTER['title']), $fontStyleArticleTitle);
    }
        
    // visual text
    if ($_NEWSLETTER['content']) {
        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $_NEWSLETTER['content']);
    }

    if ($_NEWSLETTER_FILES) {

        $table = $section->addTable('fileTable');

        foreach ($_NEWSLETTER_FILES as $row) {
            
            $file = $row['file'];

            if (file_exists($_SERVER['DOCUMENT_ROOT'].$file)) {
            
                // link
                $hash = Encryption::url($file);
                $link = $_HOST."/download/news/$hash/".$_NEWSLETTER['id'];
                $ext = strtoupper(pathinfo($file, PATHINFO_EXTENSION));

                // button
                $table->addRow();
                $cell1 = $table->addCell(3000, $styleFileCell);
                $cell1->addLink($link, htmlspecialchars("DOWNLOAD $ext"), $fontStyleFileCell, array('spaceAfter' => 0));
                $cell2 = $table->addCell(200, $styleFileCell);
                $cell2->addLink($link, htmlspecialchars('>'), $fontStyleFileCell, array('spaceAfter' => 0));
            }
        }
    }
}


// newsletter articles :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$i = 0;

$htmlAllowableTags = "<p><span><br><h1><h2><h3><h4><h5><h6><b><strong><em><i><sup><sub><table><tr><td><ul><ol><li><style>";

foreach ($_ARTICLES as $catid => $category) {

    //$section->addPageBreak();    
    $section = $phpWord->addSection($basciSection);

    // section name
    $table = $section->addTable('categoryName');
    $table->addRow();
    $table->addCell($sectionWidth)->addText(htmlspecialchars($category['name']), $fontStyleCategoryName, array('spaceAfter' => 0));
    $section->addTextBreak(1);

    foreach ($category['articles'] as $id => $article) { 

        $i++;

        // article title
        $section->addText(htmlspecialchars($article['title']), $fontStyleArticleTitle, array('spaceAfter' => 0));
        $section->addTextBreak(1);

        foreach ($article['templates'] as $tid => $data) {

            switch ($data['template']) {
                
                case 1: // tpl: teaser img, title, content, info, files

                    // teaser 
                    $teaser = $section->addTable('basicTable');
                    $teaser->addRow($noBreakRow);

                    $img = $_SERVER['DOCUMENT_ROOT'].$data['image'];

                    // teaser image
                    if ($img && file_exists($img)) {

                        $teaserContentWidth = $_TEASER_CONTENT_TWIP;
                        
                        $si = new mySimpleImage();
                        $si->load($img);
                        $imgSize = $si->getBestFit($_TEASER_IMAGE_WIDTH, 0);

                        $cell = $teaser->addCell($_TEASER_IMAGE_TWIP, $cellRowSpan);
                        
                        $textBox = $cell->addTextBox(array(
                            'width' => $imgSize['width'], 
                            'height' => $imgSize['height']-1, 
                            'stroke' => 0, 
                            'innerMargin' => 0, 
                            'align' => 'center', 
                            'borderSize' => 1
                        ));
                        
                        $textBox->addImage($img, array(
                            'width'      => $imgSize['width'],
                            'height'     => $imgSize['height'],
                            'marginLeft' => 0,
                            'marginTop'  => 0
                        ));

                    } else {
                        $teaserContentWidth = $sectionWidth;
                    }

                    // cel right
                    $cellTeaserContent = $teaser->addCell($teaserContentWidth, $cellRowSpan);
                   
                    // teaser title
                    if ($data['title']) {
                        $content = htmlspecialchars($data['title']);
                        $teaserTitle = $cellTeaserContent->addTable('basicTable');
                        $teaserTitle->addRow($noBreakRow);
                        $cell = $teaserTitle->addCell($teaserContentWidth);
                        $cell->addText($content, $fontStyleTeaserTitle);
                    }

                    // teaser text
                    if ($data['text']) {
                        $content = strip_tags($data['text'], $htmlAllowableTags); 
                        $teaserText = $cellTeaserContent->addTable('basicTable');
                        $teaserText->addRow($noBreakRow);
                        $cell = $teaserText->addCell($teaserContentWidth);
                        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $content);
                    }
                    
                    // teaser text
                    if ($data['info']) {
                        $content = strip_tags($data['info'], $htmlAllowableTags); 
                        $teaserInfo = $cellTeaserContent->addTable('basicTable');
                        $teaserInfo->addRow($noBreakRow);
                        $cell = $teaserInfo->addCell($teaserContentWidth);
                        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $content);
                    }

                    if ($data['files']) {

                        $teaserFiles = $cellTeaserContent->addTable('basicTable');
                        $teaserFiles->addRow($noBreakRow);
                        $cell = $teaserFiles->addCell($teaserContentWidth);

                        foreach ($data['files'] as $file) {
                            
                            if (file_exists($_SERVER['DOCUMENT_ROOT'].$file)) {

                                // link
                                $hash = Encryption::url($file);
                                $link = $_HOST."/download/news/$hash/".$_NEWSLETTER['id'];
                                $ext = strtoupper(pathinfo($file, PATHINFO_EXTENSION));
                                
                                // button
                                $downloadButton = $cell->addTable('downloadButton');
                                $downloadButton->addRow();
                                $cell1 = $downloadButton->addCell(3000, $styleFileCell);
                                $cell1->addLink($link, htmlspecialchars("DOWNLOAD $ext"), $fontStyleFileCell, array('spaceAfter' => 0));
                                $cell2 = $downloadButton->addCell(180, $styleFileCell);
                                $cell2->addLink($link, htmlspecialchars('>'), $fontStyleFileCell, array('spaceAfter' => 0));
                            }
                        }
                    }

                break;
                
                case 2: // tpl: title, content, files

                    $section->addTextBreak(1);

                    // title
                    if ($data['title']) {
                        $section->addText(htmlspecialchars($data['title']), $fontStyleTeaserTitle);
                    }

                    // text
                    if ($data['text']) {
                        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $data['text']);
                    }

                    // files
                    if ($data['files']) {

                        foreach ($data['files'] as $file) {
                            
                            if (file_exists($_SERVER['DOCUMENT_ROOT'].$file)) {
                                
                                // link
                                $hash = Encryption::url($file);
                                $link = $_HOST."/download/news/$hash/".$_NEWSLETTER['id'];
                                $ext = strtoupper(pathinfo($file, PATHINFO_EXTENSION));

                                // button
                                $downloadButton = $section->addTable('downloadButton');
                                $downloadButton->addRow();
                                $cell1 = $downloadButton->addCell(3000, $styleFileCell);
                                $cell1->addLink($link, htmlspecialchars("DOWNLOAD $ext"), $fontStyleFileCell, array('spaceAfter' => 0));
                                $cell2 = $downloadButton->addCell(180, $styleFileCell);
                                $cell2->addLink($link, htmlspecialchars('>'), $fontStyleFileCell, array('spaceAfter' => 0));
                            }
                        }
                    }

                break;


                case 3: // tpl: title, picture

                    $section->addTextBreak(1);

                    // title
                    if ($data['title']) {
                        $content = htmlspecialchars($data['title']);
                        $section->addText($content, $fontStyleTeaserTitle, array('spaceAfter' => 0));
                    }

                    // text
                    if ($data['text']) {
                        $content = strip_tags($data['text'], $htmlAllowableTags);
                        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $content);
                    }

                    // files
                    if ($data['files']) {   

                        foreach ($data['files'] as $file) {
                            
                            $file = $_SERVER['DOCUMENT_ROOT'].$file;
                            
                            if (file_exists($file)) {

                                $si = new mySimpleImage();
                                $si->load($file);
                                $size = $si->getBestFit($_IMAGE_MAX_FULL_WIDTH, 0);
                                
                                $section->addImage($file, array(
                                    'width'            => $size['width'],
                                    'height'           => $size['height'],
                                    'marginLeft'       => 0,
                                    'marginTop'        => 0
                                ));
                            }
                        }
                    }

                break;

                case 4: // tpl: left picture, title, text

                    $section->addTextBreak(1);

                    // teaser 
                    $table = $section->addTable('basicTable');
                    $table->addRow();
                    
                    $img = $_SERVER['DOCUMENT_ROOT'].$data['image'];

                    // teaser image
                    if ($img && file_exists($img)) {

                        $celRightWidth = $_TEASER_CONTENT_TWIP;
                        
                        $si = new mySimpleImage();
                        $si->load($img);
                        $size = $si->getBestFit($_TEASER_IMAGE_WIDTH, 0);

                        $cellLeft = $table->addCell($_TEASER_IMAGE_TWIP, $cellRowSpan);

                        $cellLeft->addImage($img, array(
                            'width'      => $size['width'],
                            'height'     => $size['height'],
                            'marginLeft' => 0,
                            'marginTop'  => 0
                        ));

                    } else {
                        $celRightWidth = $sectionWidth;
                    }

                    // cel right
                    $cellRight = $table->addCell($celRightWidth, $cellRowSpan);

                    // title
                    if ($data['title']) {
                        $content = htmlspecialchars($data['title']);
                        $tableTitle = $cellRight->addTable('basicTable');
                        $tableTitle->addRow($noBreakRow);
                        $cell = $tableTitle->addCell($celRightWidth);
                        $cell->addText($content, $fontStyleTeaserTitle);
                    }

                    // text
                    if ($data['text']) {
                        $content = strip_tags($data['text'], $htmlAllowableTags); 
                        $tableText = $cellRight->addTable('basicTable');
                        $tableText->addRow($noBreakRow);
                        $cell = $tableText->addCell($celRightWidth);
                        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $content);
                    }

                    if ($data['files']) {

                        $tableFiles = $cellRight->addTable('basicTable');
                        $tableFiles->addRow($noBreakRow);
                        $cell = $tableFiles->addCell($celRightWidth);

                        foreach ($data['files'] as $file) {
                            
                            if (file_exists($_SERVER['DOCUMENT_ROOT'].$file)) {

                                // link
                                $hash = Encryption::url($file);
                                $link = $_HOST."/download/news/$hash/".$_NEWSLETTER['id'];
                                $ext = strtoupper(pathinfo($file, PATHINFO_EXTENSION));
                                
                                // button
                                $downloadButton = $cell->addTable('downloadButton');
                                $downloadButton->addRow();
                                $cell1 = $downloadButton->addCell(3000, $styleFileCell);
                                $cell1->addLink($link, htmlspecialchars("DOWNLOAD $ext"), $fontStyleFileCell, array('spaceAfter' => 0));
                                $cell2 = $downloadButton->addCell(180, $styleFileCell);
                                $cell2->addLink($link, htmlspecialchars('>'), $fontStyleFileCell, array('spaceAfter' => 0));
                            }
                        }
                    }

                break;
            }

        } // end templates

         // article separator
        $table = $section->addTable(array('borderBottomSize' => 6, 'borderBottomColor' => 'CCCCCC'));
        $table->addRow()->addCell($sectionWidth);
        $section->addTextBreak(1);
    }
}

$section->addTextBreak(1);
$section->addText(htmlspecialchars("DON'T FORGET THAT I AM AN INTERNAL DOCUMENT WHICH MUST NOT BE SHARED OUTSIDE OF SWATCH OR LEFT BEHIND!"), array('bold'=>true));


// export ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$file = '/data/tmp/newsletter.'.$_SERVER['REQUEST_TIME'].'.docx';
$objWriter->save($_SERVER['DOCUMENT_ROOT'].$file);

if (file_exists($_SERVER['DOCUMENT_ROOT'].$file)) {
    $_SUCCESS = true; 
    chmod($_SERVER['DOCUMENT_ROOT'].$file, 0777);
    $hash = Encryption::url($file);
    $_FILE_URL = "/download/tmp/$hash";
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONSE:

ob_end_clean();

header('Content-Type: text/json');
echo json_encode(array(
    'errors' => $_ERRORS,
    'success' => $_SUCCESS,
    'file' => $_FILE_URL
));


