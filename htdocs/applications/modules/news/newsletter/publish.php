<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

ob_start();

define('DEBUGGING_MODE', $_REQUEST['debuge'] ?: false);

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = 'news';
$_STATE = $_REQUEST['state'];
$_ID = $_REQUEST['id'];

// response
$_JSON = array();

// db model
$model = new Model($_APPLICATION);

// permissions
$_PERMISSIONS = array(
	'edit' => user::permission('can_edit_newsletter'),
	'sent' => user::permission('can_sent_newsletter')
);


// read newsletter
$newsletter = new Modul($_APPLICATION);
$newsletter->setTable('newsletters');
$newsletter->read($_ID);

$newsletterTitle = $newsletter->data['newsletter_title'];


// checkin :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_PERMISSIONS['edit']) {
	$_ERRORS[] = "You don't have permissions to submit this action.";
}

if (!$newsletter->id) {
	$_ERRORS[] = "Newsletter not found.";
}

if ($_STATE<>3) {
	$_ERRORS[] = "The publish state is wrong.";
}

if ($newsletter->data['newsletter_publish_state_id'] > 3) {
	$date = date('%d.%m.%Y', strtotime($newsletter->data['newsletter_publish_date']));
	$_ERRORS[] = "The newsletter is already published. Publishing was on $date.";
}

// get newslettter articles
if (!$_ERRORS) {
	
	$sth = $model->db->prepare("
		SELECT COUNT(DISTINCT news_article_id) as total
		FROM news_articles
		INNER JOIN newsletter_articles ON newsletter_article_article_id = news_article_id
		WHERE newsletter_article_newsletter_id = ?
	");

	$sth->execute(array($_ID));
	$articles = $sth->fetch();

	if (!$articles['total']) {
		$_ERRORS[] = "Newsletter has no article(s)";
	}
}

// get newslettter articles
if (!$_ERRORS) {
	
	$sth = $model->db->prepare("
		SELECT COUNT(DISTINCT news_article_id) as total
		FROM news_articles
		INNER JOIN newsletter_articles ON newsletter_article_article_id = news_article_id
		WHERE newsletter_article_newsletter_id = ? AND news_article_publish_state_id < 3
	");

	$sth->execute(array($_ID));
	$articles = $sth->fetch();

	if ($articles['total']>0) {
		$_ERRORS[] = "The Newsletter cannot be published, please confirm all Article(s)";
	}
}


// trackers ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$trackArticle = $model->db->prepare("
	INSERT INTO news_article_tracks (
		news_article_track_article_id,
		news_article_track_newsletter_id,
		news_article_track_user_id,
		news_article_track_type_id,
		news_article_track_entity
	)
	VALUES (?,?,?,?,?)
");

$trackNewsletter = $model->db->prepare("
	INSERT INTO newsletter_tracks (
		newsletter_track_newsletter_id,
		newsletter_track_user_id,
		newsletter_track_type_id
	)
	VALUES (?,?,?)
");


// newletter publishing ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		UPDATE newsletters SET 
			newsletter_number = ?,
			newsletter_publish_state_id = 3,
			newsletter_publish_date = NOW(),
			user_modified = ?,
			date_modified = NOW()
		WHERE newsletter_id = ?
	");

	if (DEBUGGING_MODE) {
		$_JSON['success'] = true;
	} else {

		// get newsletter number
		$sthNumber = $model->db->prepare("
			SELECT MAX(newsletter_number) AS number
			FROM newsletters
			WHERE newsletter_publish_state_id > 2
		");

		$sthNumber->execute();
		$result = $sthNumber->fetch();
		$newsletterNumber = $result['number'] + 1;

		$_JSON['success'] = $sth->execute(array($newsletterNumber, $user->login, $_ID));
	}

	if ($_JSON['success']) {

		$_SUCCESSES[] = "Newsletter <b>$newsletterTitle</b> is published"; 
		$_JSON['remove'][] = '#btn-delete';

	} else {
		$_ERRORS[] = "The Newsletter ist not published. Please contact system adminstrator.";
	}

	if (!DEBUGGING_MODE && $_JSON['success']) {
		$trackNewsletter->execute(array($_ID, $user->id, 11));
	}
}

// archive newsletters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT newsletter_id AS id
		FROM newsletters 
		WHERE newsletter_id != ? AND newsletter_publish_state_id = 3
	");

	$sth->execute(array($_ID));
	$result = $sth->fetchAll();

	if ($result) {

		$sth = $model->db->prepare("
			UPDATE newsletters SET 
				newsletter_publish_state_id = 4,
				date_modified = NOW(),
				user_modified = ?
			WHERE newsletter_id = ?
		");
		
		foreach ($result as $row) {
			
			$response = DEBUGGING_MODE ? true : $sth->execute(array($user->login, $row['id']));

			if (!$response) {
				$_ERRORS[] = "Newsletter {$row[id]} cannot be archived.";
			} 

			if (!DEBUGGING_MODE && $response) {
				$trackNewsletter->execute(array($row['id'], $user->id, 12));
			}
		}
	}

}


// archive articles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT DISTINCT 
			news_article_id AS id
		FROM news_articles
		INNER JOIN newsletter_articles ON newsletter_article_article_id = news_article_id
		WHERE newsletter_article_newsletter_id != ? AND news_article_publish_state_id = 4
	");

	$sth->execute(array($_ID));
	$result = $sth->fetchAll();

	if ($result) {

		$sth = $model->db->prepare("
			UPDATE news_articles SET 
				news_article_publish_state_id = 5,
				news_article_new = NULL
			WHERE news_article_id = ?
		");

		foreach ($result as $article) {
			
			$response = DEBUGGING_MODE ? true : $sth->execute(array($article['id']));

			// track: set news article as archived
			if (!DEBUGGING_MODE && $response) {
				$trackArticle->execute(array($article['id'], $_ID, $user->id, 37, null));
			}
		}
	}
}


// publishing ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT news_article_id AS id
		FROM news_articles
		INNER JOIN newsletter_articles ON newsletter_article_article_id = news_article_id
		WHERE newsletter_article_newsletter_id = ? AND (news_article_publish_date IS NULL OR news_article_publish_date = '')
	");

	$sth->execute(array($_ID));
	$result = $sth->fetchAll();

	if ($result) {

		$sth = $model->db->prepare("
			UPDATE news_articles SET 
				news_article_publish_state_id = 4,
				news_article_publish_date = CURDATE()
			WHERE news_article_id = ?
		");

		$updatePublishDate = $model->db->prepare("
			UPDATE newsletter_articles SET 
				newsletter_article_publish_date = NOW
			WHERE newsletter_article_newsletter_id = ? AND newsletter_article_article_id = ?
		");

		foreach ($result as $article) {
			
			$response = DEBUGGING_MODE ? true : $sth->execute(array($article['id']));

			if (!$response) {
				$_ERRORS[] = "Article {$article[id]} is not published";
			}

			if (!DEBUGGING_MODE && $response) {
				
				// track: set news article as published
				$trackArticle->execute(array($article['id'], $_ID, $user->id, 36, null));
				
				// update newsletter article publish date
				$updatePublishDate->execute(array($_ID, $article['id']));
			}
		}
	}
}

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_ERRORS) {
	foreach ($_ERRORS as $text) {
		$_JSON['notifications'][] = array(
			'type'=>'error', 
			'title'=>'Error', 
			'text'=>$text
		);
	}
}

if ($_SUCCESSES) {
	foreach ($_SUCCESSES as $text) {
		$_JSON['notifications'][] = array(
			'type'=>'success', 
			'title'=>'Success', 
			'text'=>$text
		);
	}
}

ob_end_clean();
header('Content-Type: text/json');
echo json_encode($_JSON);
