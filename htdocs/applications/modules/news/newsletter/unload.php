<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$_ID = $_REQUEST['id'];
$_APPLICATION = $_REQUEST['application'] ?: 'news';

$model = new Model($_APPLICATION);

// read newsletter
$newsletter = new Modul($_APPLICATION);
$newsletter->setTable('newsletters');
$newsletter->read($_ID);
$_HAS_TITLE = $newsletter->data['newsletter_title'] ? true : false;

if (!$newsletter->id) exit;

// newsletter tracker
$track = $model->db->prepare("
	INSERT INTO newsletter_tracks (
		newsletter_track_newsletter_id,
		newsletter_track_user_id,
		newsletter_track_type_id
	)
	VALUES (?,?,?)
");

// check newsletter data :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_CHECK_FIELDS = array(
	'newsletter_title',
	'newsletter_text',
	'newsletter_image',
	'newsletter_files',
	'news_article_title',
	'newsletter_background'
);

$_HAS_DATA = false;

foreach ($_CHECK_FIELDS as $field) {
	if ($newsletter->data[$field]) {
		$_HAS_DATA = true;
		break;
	}
}

// count newsletter articles
if (!$_HAS_DATA) {
	
	$sth = $model->db->prepare("
		SELECT COUNT(newsletter_article_id) AS total
		FROM newsletter_articles
		WHERE newsletter_article_newsletter_id = ?
	");

	$sth->execute(array($_ID));
	$result = $sth->fetch();
	$_HAS_DATA = $result['total'] > 0 ? true : false;
}

// count newsletter recipients
if (!$_HAS_DATA) {
	
	$sth = $model->db->prepare("
		SELECT COUNT(newsletter_recipient_id) AS total
		FROM newsletter_recipients
		WHERE newsletter_recipient_newsletter_id = ?
	");

	$sth->execute(array($_ID));
	$result = $sth->fetch();
	$_HAS_DATA = $result['total'] > 0 ? true : false;
}


// remove newsletter :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_HAS_DATA) {

	$sth = $model->db->prepare("DELETE FROM newsletters WHERE newsletter_id = ?");
	$response = $sth->execute(array($_ID));

	if ($response) {
		$track->execute(array($_ID, $user->id, 57));
	}

	exit;
}

// update newsletter title :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_HAS_DATA && !$_HAS_TITLE) {

	$sth = $model->db->prepare("
		UPDATE newsletters SET 
			newsletter_title = 'Untitled Newsletter',
			user_modified = ?,
			date_modified = NOW()
		WHERE newsletter_id = ?
	");

	$response = $sth->execute(array($user->login, $_ID));

	if ($response) {
		$track->execute(array($_ID, $user->id, 64));
	}

	exit;
}
