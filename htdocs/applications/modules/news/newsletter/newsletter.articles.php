<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = 'news';
$_SECTION = $_REQUEST['section'];
$_ID = $_REQUEST['id'];

// response
$_JSON = array();

// db model
$model = new Model($_APPLICATION);

// permissions
$_PERMISSIONS = array(
	'edit' => user::permission('can_edit_newsletter'),
	'sent' => user::permission('can_sent_newsletter')
);


switch ($_SECTION) {

	case 'load':

		$sth = $model->db->prepare("
			SELECT 
				newsletter_id,
				newsletter_title,
				newsletter_text,
				newsletter_publish_state_id,
				newsletter_image,
				newsletter_files,
				DATE_FORMAT(newsletter_publish_date, '%d.%m.%Y') AS newsletter_publish_date
			FROM newsletters
			WHERE newsletter_id = ?
		");

		$sth->execute(array($_ID));
		$newsletter = $sth->fetch();

		$files = $newsletter['newsletter_files'];
		unset($newsletter['newsletter_files']);

		$state = $newsletter['newsletter_publish_state_id'];

		// fields
		$fields = $newsletter ? array_keys($newsletter) : array();

		// disable form
		if (!$_PERMISSIONS['edit']) {
			$_JSON['disabled'] = $fields ? array_fill_keys($fields, true) : array();
		}

		$_JSON['data'] = $newsletter;

		$model->db->exec("SET SESSION group_concat_max_len = 1000000;");

		// get newsletter articles
		$sth = $model->db->prepare("
			SELECT DISTINCT
				news_article_id AS id,
				CONCAT(user_firstname, ' ', user_name) AS author,
				news_article_title AS title,
				news_category_name AS category,
				IF(news_section_id, news_section_id, 0) AS section,
				IF(news_section_id, news_section_name, 'Uncategorised') AS section_name,
				news_workflow_state_name AS state,
				IF(news_article_featured, 'Yes', 'No') AS featured,
				news_article_content_data AS teaser,
				newsletter_article_send_date AS sent,
				DATE_FORMAT(news_article_publish_date, '%d.%m.%Y') AS publish_date,
				DATE_FORMAT(news_article_desired_publish_date, '%d.%m.%Y') AS desired_publish_date,
				DATE_FORMAT(news_article_expiry_date, '%d.%m.%Y') AS expiry_date,
				DATE_FORMAT(news_article_confirmed_date, '%d.%m.%Y') AS confirmed_date,
				DATE_FORMAT(news_articles.date_created, '%d.%m.%Y') AS created_date,
				(
					SELECT GROUP_CONCAT(DISTINCT role_name SEPARATOR '</p><p>')
					FROM db_retailnet.roles
					INNER JOIN news_article_roles ON news_article_role_role_id = role_id
					WHERE news_article_role_article_id = news_article_id
					ORDER BY role_name
				) AS roles_restrictions,
				(
					SELECT GROUP_CONCAT(DISTINCT address_company SEPARATOR '</p><p>')
					FROM db_retailnet.addresses
					INNER JOIN news_article_addresses ON news_article_address_address_id = address_id
					WHERE news_article_address_article_id = news_article_id
					ORDER BY address_company
				) AS companies_restrictions
			FROM news_articles
			INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
			INNER JOIN db_retailnet.users ON user_id = news_article_owner_user_id
			INNER JOIN news_workflow_states ON news_workflow_state_id = news_article_publish_state_id
			LEFT JOIN news_categories ON news_category_id = news_article_category_id
			LEFT JOIN news_sections ON news_category_section_id = news_section_id
			LEFT JOIN newsletter_articles ON newsletter_article_article_id = news_article_id
			WHERE news_article_active=1 
			AND news_article_title IS NOT NULL
			AND news_article_content_template_id = 1
			AND newsletter_article_newsletter_id = ?
			ORDER BY section_name, newsletter_article_sort, news_article_title
		");

		$sth->execute(array($_ID));
		$result = $sth->fetchAll();

		$uncategorised = false;

		if ($result) {

			foreach ($result as $article) {

				$id = $article['id'];

				$article['teaser'] = $article['teaser'] ? unserialize($article['teaser']) : array();

				if ($article['sent']) {
					$_JSON['remove'][] = ".article-$id .article-remove";
				} elseif (!$_PERMISSIONS['edit']) {
					$_JSON['remove'][] = ".article-$id .article-remove";
				}

				if ($article['desired_publish_date']) {
					$article['info'][] = array(
						'caption' => 'Desired Publishing Date',
						'value' => $article['desired_publish_date']
					); 
				}

				if ($article['roles_restrictions']) {
					$article['roles_restrictions'] = "<p>".$article['roles_restrictions']."</p>";
				}

				if ($article['companies_restrictions']) {
					$article['companies_restrictions'] = "<p>".$article['companies_restrictions']."</p>";
				}

				if (!$uncategorised && !$article['section']) {
					$uncategorised = true;
				}

				$_JSON['articles'][] = $article;
			}
		}

		$sth = $model->db->prepare("
			SELECT DISTINCT
				news_section_id AS id,
				news_section_name AS name
			FROM news_articles
			INNER JOIN news_categories ON news_category_id = news_article_category_id
			INNER JOIN news_sections ON news_category_section_id = news_section_id
			INNER JOIN newsletter_articles ON newsletter_article_article_id = news_article_id
			WHERE news_article_active=1 AND newsletter_article_newsletter_id = ?
			ORDER BY news_section_order,news_section_name
		");

		$sth->execute(array($_ID));
		$sections = $sth->fetchAll();

		if ($uncategorised) {
			$sections[] = array(
				'id' => 0,
				'name' => 'Uncategorised'
			);
		}

		$_JSON['sections'] = $sections;

	break;

	case 'load-unassigned-articles':

		$sth = $model->db->prepare("
		SELECT DISTINCT
			news_article_id AS id,
			CONCAT(user_firstname, ' ', user_name) AS author,
			news_article_title AS title,
			news_category_name AS category,
			news_section_id AS section,
			news_section_name AS section_name,
			news_workflow_state_name AS state,
			IF(news_article_featured, 'Yes', 'No') AS featured,
			news_article_content_data AS teaser,
			DATE_FORMAT(news_article_desired_publish_date, '%d.%m.%Y') AS desired_publish_date,
			DATE_FORMAT(news_article_publish_date, '%d.%m.%Y') AS publish_date,
			DATE_FORMAT(news_article_expiry_date, '%d.%m.%Y') AS expiry_date,
			DATE_FORMAT(news_article_confirmed_date, '%d.%m.%Y') AS confirmed_date,
			DATE_FORMAT(news_articles.date_created, '%d.%m.%Y') AS created_date
		FROM news_articles
		INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
		INNER JOIN db_retailnet.users ON user_id = news_article_owner_user_id
		INNER JOIN news_workflow_states ON news_workflow_state_id = news_article_publish_state_id
		LEFT JOIN news_categories ON news_category_id = news_article_category_id
		LEFT JOIN news_sections ON news_category_section_id = news_section_id
		LEFT JOIN newsletter_articles ON newsletter_article_article_id = news_article_id
		WHERE news_article_active=1 
		AND news_article_title IS NOT NULL
		AND news_article_content_template_id = 1
		AND newsletter_article_article_id IS NULL
		AND IF(news_article_expiry_date,  CURRENT_DATE <= news_article_expiry_date, 1) = 1
		ORDER BY news_article_title
	");

	$sth->execute();
	$result = $sth->fetchAll();

	if ($result) {
		foreach ($result as $article) {
			$id = $article['id'];
			$article['teaser'] = $article['teaser'] ? unserialize($article['teaser']) : array();
			$article['type'] = "unassigned";
			$_JSON[] = $article;
		}
	}

	break;

	case 'load-multiple-articles':

		// get current articles
		$sth = $model->db->prepare("
			SELECT GROUP_CONCAT(DISTINCT newsletter_article_article_id) AS articles
			FROM newsletter_articles
			WHERE newsletter_article_newsletter_id = ?
		");

		$sth->execute(array($_ID));
		$result = $sth->fetch();

		if ($result['articles']) {
			$filter = "AND news_article_id NOT IN ({$result[articles]})";
		}

		$sth = $model->db->prepare("
			SELECT DISTINCT
				news_article_id AS id,
				CONCAT(user_firstname, ' ', user_name) AS author,
				news_article_title AS title,
				news_category_name AS category,
				news_section_id AS section,
				news_section_name AS section_name,
				news_workflow_state_name AS state,
				IF(news_article_featured, 'Yes', 'No') AS featured,
				news_article_content_data AS teaser,
				DATE_FORMAT(news_article_desired_publish_date, '%d.%m.%Y') AS desired_publish_date,
				DATE_FORMAT(news_article_publish_date, '%d.%m.%Y') AS publish_date,
				DATE_FORMAT(news_article_expiry_date, '%d.%m.%Y') AS expiry_date,
				DATE_FORMAT(news_article_confirmed_date, '%d.%m.%Y') AS confirmed_date,
				DATE_FORMAT(news_articles.date_created, '%d.%m.%Y') AS created_date
			FROM news_articles
			INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
			INNER JOIN db_retailnet.users ON user_id = news_article_owner_user_id
			INNER JOIN news_workflow_states ON news_workflow_state_id = news_article_publish_state_id
			LEFT JOIN news_categories ON news_category_id = news_article_category_id
			LEFT JOIN news_sections ON news_category_section_id = news_section_id
			INNER JOIN newsletter_articles ON newsletter_article_article_id = news_article_id
			WHERE news_article_active=1 
			AND news_article_title IS NOT NULL
			AND news_article_content_template_id = 1
			AND newsletter_article_newsletter_id != ? AND news_article_multiple = 1
			AND IF(news_article_expiry_date,  CURRENT_DATE <= news_article_expiry_date, 1) = 1
			$filter
			ORDER BY news_article_title
		");

		$sth->execute(array($_ID));
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $article) {
				$id = $article['id'];
				$article['teaser'] = $article['teaser'] ? unserialize($article['teaser']) : array();
				$article['type'] = "other";
				$_JSON[] = $article;
			}
		}

	break;

	case 'sort':

		$sort = $_REQUEST['sort'];

		if (is_array($sort)) {

			$sth = $model->db->prepare("
				UPDATE newsletter_articles SET
					newsletter_article_sort = ?
				WHERE newsletter_article_article_id = ? AND newsletter_article_newsletter_id = ?
			");

			// track: Sort newsletter articles
			$track = $model->db->prepare("
				INSERT INTO newsletter_tracks (
					newsletter_track_newsletter_id,
					newsletter_track_user_id,
					newsletter_track_type_id,
					newsletter_track_entity
				)
				VALUES (?,?,?,?)
			");

			foreach ($sort as $order => $article) {
				
				$i = $order+1;
				
				$response = $sth->execute(array($i, $article, $_ID));

			}

			if ($response) {
				$track->execute(array($_ID, $user->id, 13, serialize($sort)));
			}
		}

	break;

	default:
		$_JSON['error'] = "Section not defined.";
	break;
}


if ($_JSON) {
	header('Content-Type: text/json');
	echo json_encode($_JSON);
}