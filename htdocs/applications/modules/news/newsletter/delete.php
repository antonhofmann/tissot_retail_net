<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

ob_start();

$user = User::instance();
$translate = Translate::instance();

// request
$_APPLICATION = $_REQUEST['application'] ?: 'news';
$_ID = trim($_REQUEST['id']);

// helpers
$_ERRORS = array();
$_JSON = array();

$_PERMISSIONS = array(
	'edit' => user::permission('can_edit_newsletter'),
	'sent' => user::permission('can_sent_newsletter')
);

// read newsletter
$newsletter = new Modul($_APPLICATION);
$newsletter->setTable('newsletters');
$newsletter->read($_ID);


// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_PERMISSIONS['edit']) {
	$_ERRORS[] = "You don't have permissions to submit this action.";
}

if (!$newsletter->id) {
	$_ERRORS[] = "Newsletter not found.";
}

if ($newsletter->data['newsletter_publish_state_id']>2) {
	$_ERRORS[] = "Published newsletters cannot be delete.";
}

if (!$_ERRORS) {

	$model = new Model($_APPLICATION);

	$track = $model->db->prepare("
		INSERT INTO newsletter_tracks (
			newsletter_track_newsletter_id,
			newsletter_track_user_id,
			newsletter_track_type_id
		)
		VALUES (?,?,?)
	");

	// delete article
	$sth = $model->db->prepare("
		DELETE FROM newsletters
		WHERE newsletter_id = ?
	");

	$respone = $sth->execute(array($_ID));

	if ($respone) {

		$_JSON['success'] = true;
		$_JSON['redirect'] = '/gazette/newsletters';

		// track: remove article templates
		$track->execute(array($_ID, $user->id, 9));

		// remove newsletter articles
		$sth = $model->db->prepare("
			DELETE FROM newsletter_articles
			WHERE newsletter_article_newsletter_id = ?
		");

		$sth->execute(array($_ID));

		// remove newsletter recipients
		$sth = $model->db->prepare("
			DELETE FROM newsletter_recipients
			WHERE newsletter_recipient_newsletter_id = ?
		");

		$sth->execute(array($_ID));

		// remove newsletter files
		$sth = $model->db->prepare("
			DELETE FROM newsletter_files
			WHERE newsletter_file_newsletter_id = ?
		");

		$sth->execute(array($_ID));

		// remove article files
		dir::remove("/data/news/newsletters/$_ID", true);
	}
}

$_JSON['errors'] = $_ERRORS;

if ($_ERRORS) {
	foreach ($_ERRORS as $message) {
		$_JSON['notifications'][] = array(
			'type' => 'error',
			'text' => $message
		);
	}
}

ob_end_clean();
header('Content-Type: text/json');
echo json_encode($_JSON);
