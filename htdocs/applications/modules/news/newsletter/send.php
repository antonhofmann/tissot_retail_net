<?php 

session_name('retailnet');
session_start();

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/libraries/phpmailer/class.phpmailer.php';

ini_set('memory_limit', '4096M');

define(DEBUGGING_MODE, $_REQUEST['debuge'] ?: false);

$_APPLICATION = $_REQUEST['application'] ?: 'news';
$_ID = $_REQUEST['id'];
$_TEST = $_REQUEST['test'];
$_TERMINATED = $_REQUEST['terminated'];
//$_DEV_MAIL = 'admir.serifi@mediaparx.com';
$_LETTER_PX = 7;
$_FILE_TITLE_LENGTH = 50;

$_SENDER_NAME = 'Gazette';
$_SENDER_ADDRESS = 'gazette@swatch.com';

$_ERRORS = array();
$_SUCCESSES = array();
$_CONSOLE = array();
$_DATA = array();

// Host name
$_SERVER['SERVER_NAME'] = $_SERVER['SERVER_NAME'] ?: 'retailnet.tissot.ch';
$_HOST = 'http://'.$_SERVER['SERVER_NAME'];
$_LINK = $_HOST."/gazette/$_ID";
$_CONSOLE[] = "Host: $_HOST";

if ($_TEST) {
	$_CONSOLE[] = "TEST for email address: $_TEST";
}

$model = Connector::get('db_retailnet_news');

$user = User::instance();


// permissions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->prepare("
	SELECT user_role_user, 1
	FROM db_retailnet.user_roles
	WHERE user_role_role = 71
");

$sth->execute();
$result = $sth->fetchAll();
$_NEWS_ADMINISTRATORS = $result ? _array::extract($result) : array();
$_DATA['administrators'] = $_NEWS_ADMINISTRATORS;

// checkin :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ID) {
	$_ERRORS[] = "Request failure. please check request data.";
	goto BLOCK_RESPNDING;
}

$sth = $model->prepare("
	SELECT 
		newsletter_id AS id,
		newsletter_number AS number,
		newsletter_title AS title,
		newsletter_image AS image,
		newsletter_background AS background,
		newsletter_text AS text,
		newsletter_files AS files,
		DATE_FORMAT(newsletter_publish_date, '%m/%d/%Y') AS date,
		newsletter_publish_state_id AS state,
		newsletter_send_user_id AS sender
	FROM newsletters
	WHERE newsletter_id = ?
");

$sth->execute(array($_ID));
$newsletter = $sth->fetch();

if (!$newsletter['id']) {
	$_ERRORS[] = "Newsletter not found";
	goto BLOCK_RESPNDING;
}

$_NEWSLETTER_NUMBER = $newsletter['number'];
$newsletter['image'] = $_HOST.$newsletter['image'];

// from command line tools
if ($newsletter['sender']) {
	$user = new User($newsletter['sender']);
	$_CONSOLE[] = "Sender $user->email";
}

$_CONSOLE[] = "Newsletter {$newsletter[id]} found";


// tracks ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$track = $model->prepare("
	INSERT INTO newsletter_tracks (
		newsletter_track_newsletter_id,
		newsletter_track_user_id,
		newsletter_track_additional_user_id,
		newsletter_track_type_id,
		newsletter_track_entity
	)
	VALUES (?,?,?,?,?)
");

// styles & templates ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_TEMPLATE = File::load('/applications/templates/news/newsletter/mail.template.php');

if (!$_TEMPLATE) {
	$_ERRORS[] = "Mail template not found.";
	goto BLOCK_RESPNDING;
}

// newsletter title
$_STYLES['newsletter-title'] = " 
	font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
	color: #000;
	font-size: 42px;
	line-height: 56px;
	padding: 20px 0 10px 0; 
";

// newsletter text
$_STYLES['newsletter-text'] = " 
	font-size: 14px;
	font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
";

// section title
$_STYLES['section'] = " 
	background-color: #E2001A;
	padding: 5px 10px 5px 10px;
	color: #ffffff;
	text-align: right;
	font-size: 28px;
	font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif; 
";

// section title
$_STYLES['article-title'] = " 
	font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
	color: #A4A4A4;
	font-size: 24px;
	line-height: 34px;
	padding: 20px 0 10px 0; 
";

// section title
$_STYLES['article-separator'] = "  
	height: 10px;
	border-bottom: 1px dashed #a4a4a4; 
";

// teaser title
$_STYLES['teaser-title'] = "  
	font-size: 16px;
	font-weight: 500;
	font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
	padding: 0;
	margin: 0; 
";

// teaser title
$_STYLES['teaser-text'] = "  
	font-size: 14px;
	font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif; 
";

// teaser title
$_STYLES['teaser-image'] = "  
	border: 1px solid #a6a6a6;
	display: block;
";

$_STYLES['button'] = " 
	font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif; 
	color: #000; 
	font-size: 13px; 
	background-color: #a4a4a4; 
	height: 29px; 
";

$_TEMPLATES['visual'] = "
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr><td><a href=\"{link}\"><img src=\"{image}\" class=\"img-visual img-responsive\" width=\"750\" ></a></td></tr>
	</table>
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr><td style=\"{$_STYLES['newsletter-title']}\" class=\"newsletter-title\" >{title}</td></tr>
	</table>
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr><td class=\"newsletter-text\" style=\"{$_STYLES['newsletter-text']}\"  >{text}</td></tr>
	</table>
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr><td class=\"newsletter-files\">{files}</td></tr>
	</table>
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr><td height=\"20\">&nbsp;</td></tr>
	</table>
";

$_TEMPLATES['button'] = "
	<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"tbl-button\">
		
		<tr>
			<td width=\"9\" bgcolor=\"#a4a4a4\">&nbsp;</td>
			<td bgcolor=\"#a4a4a4\" style=\"{$_STYLES[button]}\">
				<a class=\"auto-100pc\" style=\"display: inline-block; overflow:hidden;width: 100%; height: 29px; text-transform: uppercase; color: #fff; line-height: 29px; text-decoration: none\" href=\"{url}\" target=\"_blank\">
					{title}
				</a>
			</td>
			<td width=\"15\" bgcolor=\"#a4a4a4\">
				<a href=\"{url}\" target=\"_blank\"><img src=\"http://webr.emv2.com/swatch/template_responsive/button_shadow.png\" width=\"15\" height=\"29\" border=\"0\"></a>
			</td>
			<td width=\"30\" bgcolor=\"#a4a4a4\">
				<a href=\"{url}\" target=\"_blank\"><img src=\"http://webr.emv2.com/swatch/template_responsive/button_arrow.png\" width=\"30\" height=\"29\" border=\"0\"></a>
			</td>
			<td width=\"10\"></td>
		</tr>
		<tr><td colspan=\"5\" height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td></tr>

	</table>
";

$_TEMPLATES['teaser'] = "
	<table id=\"article-{id}\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"tbl-article-title\">
		<tr><td colspan=\"3\" class=\"article-title\" style=\"{$_STYLES['article-title']}\">{article_title}</td></tr>
	</table>
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr>
			<td>
				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr>
						{image}
						<td valign=\"top\" class=\"col-teaser-text\" style=\"{$_STYLES['teaser-text']}\" >{title}{text}{readmore}{files}</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
";


// newsletter files ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NEWSLETTER_FILES = array();
	
$sth = $model->prepare("
	SELECT newsletter_file_path AS path, newsletter_file_title AS title
	FROM newsletter_files
	WHERE newsletter_file_newsletter_id = ?
");

$sth->execute(array($_ID));
$_NEWSLETTER_FILES = $sth->fetchAll();

// authors :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NEWS_AUTHORS = array();
$_USER_AUTHORS = array();

// get news authors
$sth = $model->prepare("
	SELECT DISTINCT
		news_author_id AS id,
		CONCAT(' - ', news_author_first_name, ' ', news_author_name) AS name
	FROM news_articles
	INNER JOIN news_authors ON news_author_id = news_article_author_id
	WHERE news_article_active = 1 AND news_article_publish_state_id IN (4,5)
");

$sth->execute();
$result = $sth->fetchAll();
$_NEWS_AUTHORS = _array::extract($result);

// get user authors
$sth = $model->prepare("
	SELECT DISTINCT
		user_id AS id,
		CONCAT(' - ', user_firstname, ' ', user_name) AS name
	FROM news_articles
	INNER JOIN db_retailnet.users ON user_id = news_article_author_user_id
	WHERE news_article_active = 1 AND news_article_publish_state_id IN (4,5)
");

$sth->execute();
$result = $sth->fetchAll();
$_USER_AUTHORS = _array::extract($result);


// recipients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_RECIPIENTS = array();

if ($_TEST) {

	$sth = $model->prepare("
		SELECT user_id
		FROM db_retailnet.users
		WHERE user_email = ?
	");

	$sth->execute(array($_TEST));
	$res = $sth->fetch();
	$_TEST_MAIL_IS_RETAILNET_USER = $res['user_id'] ? false : true;

	$_RECIPIENTS[] = array(
		'user' => $res['user_id'] ?: 0,
		'name' => 'Newsletter Preview',
		'mail' => $_TEST,
		'additional' => $_TEST_MAIL_IS_RETAILNET_USER
	);
}
else {

	$sth = $model->prepare("
		SELECT DISTINCT 
			newsletter_recipient_id AS id,
			user_id  AS user,
			user_address AS company,
			CONCAT(user_firstname, ' ', user_name) AS name,
			user_email AS mail,
			(
				SELECT GROUP_CONCAT(user_role_role)
				FROM db_retailnet.user_roles
				WHERE db_retailnet.user_roles.user_role_user = db_retailnet.users.user_id
			) AS roles
		FROM newsletter_recipients
		INNER JOIN db_retailnet.users ON user_id = newsletter_recipient_user_id
		WHERE db_retailnet.users.user_active = 1 
		AND newsletter_recipient_newsletter_id = ? 
		AND newsletter_recipient_selected = 1 
		AND user_login IS NOT NULL 
		AND user_login != ''
		AND user_password IS NOT NULL 
		AND user_password != ''
		GROUP BY user_id
	");

	$sth->execute(array($_ID));
	$result = $sth->fetchAll();
	$_RECIPIENTS = $result ?: $_RECIPIENTS;

	$sth = $model->prepare("
		SELECT DISTINCT 
			newsletter_recipient_id AS id,
			news_recipient_id  AS additional,
			CONCAT(news_recipient_firstname, ' ', news_recipient_name) AS name,
			news_recipient_email AS mail
		FROM newsletter_recipients
		INNER JOIN news_recipients ON news_recipient_id = newsletter_recipient_additional_id
		WHERE news_recipient_active = 1 AND newsletter_recipient_newsletter_id = ?  AND newsletter_recipient_selected = 1
	");

	$sth->execute(array($_ID));
	$_ADDITIONAL_RECIPIENTS = $sth->fetchAll();

	if ($_ADDITIONAL_RECIPIENTS) {
		$_RECIPIENTS = array_merge($_RECIPIENTS, $_ADDITIONAL_RECIPIENTS);
	}
}

if (!$_RECIPIENTS) {
	$_ERRORS[] = "Newsletter hasn't recipients";
	goto BLOCK_RESPNDING;
}

$_CONSOLE[] = "Total recipients: ".count($_RECIPIENTS);

// newsletter articles :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$filter = $_TEST ? null : " AND news_article_publish_state_id > 3";

$sth = $model->prepare("
	SELECT DISTINCT
		news_article_id AS id,
		news_article_title AS title,
		DATE_FORMAT(news_article_publish_date, '%m/%d/%Y') AS date,
		news_article_featured AS featured,
		news_article_new AS new,
		news_article_contact_information AS informations,
		news_article_author_id AS author_id,
		news_article_author_user_id AS author_user_id,
		news_section_id AS sid,
		news_section_name AS section,
		news_category_id AS cid,
		news_category_name AS category,
		news_article_content_id AS tid,
		news_article_content_data AS data,
		news_article_template_file_view AS file,
		news_article_sticker AS sticker,
		(
			SELECT GROUP_CONCAT(news_article_role_role_id)
            FROM news_article_roles
            WHERE news_article_role_article_id = news_article_id
        ) AS roles,
        (
			SELECT GROUP_CONCAT(news_article_address_address_id)
            FROM news_article_addresses
            WHERE news_article_address_article_id = news_article_id
        ) AS companies,
		(
			SELECT COUNT(news_article_content_id)
			FROM news_article_contents
			WHERE news_article_content_article_id = news_article_id AND news_article_content_template_id != 1
		) AS totalTemplates
	FROM newsletter_articles
	INNER JOIN news_articles ON news_article_id = newsletter_article_article_id
	INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
	INNER JOIN news_article_templates ON news_article_content_template_id = news_article_template_id
	INNER JOIN news_categories ON news_article_category_id = news_category_id
	INNER JOIN news_sections ON news_section_id = news_category_section_id
	WHERE news_article_content_template_id = 1 
		AND news_article_active = 1
		AND IF(news_article_expiry_date,  CURRENT_DATE <= news_article_expiry_date, 1) = 1
		$filter
		AND newsletter_article_newsletter_id = ?
	ORDER BY 
		news_section_order, 
		news_section_name, 
		newsletter_article_sort,
		news_article_publish_date DESC, 
		news_article_title
");

$sth->execute(array($_ID));
$articles = $sth->fetchAll();

if (!$articles) {
	$_ERRORS[] = "Newsletter hasn't articles";
	goto BLOCK_RESPNDING;
}

$_CONSOLE[] = "Total articles: ".count($articles);


// article content for additiona reciopients :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ARTICLE_CONTENTS = array();
$_ARTICLE_CONTENT_TEMPLATES = array();

// standard content
$_ARTICLE_CONTENT_TEMPLATES[2] = "
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr><td height=\"15\" >&nbsp;</td></tr>
		<tr><td class=\"teaser-title\" style=\"{$_STYLES['teaser-title']}\" >{title}</td></tr>
		<tr><td class=\"teaser-text\" style=\"{$_STYLES['teaser-text']}\" >{text}</td></tr>
	</table>
";

// pictures
$_ARTICLE_CONTENT_TEMPLATES[3] = "
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr><td height=\"15\" >&nbsp;</td></tr>
		<tr><td class=\"teaser-title\" style=\"{$_STYLES['teaser-title']}\" >{title}</td></tr>
		<tr><td class=\"article-template-pictures\" >{files}</td></tr>
	</table>
";

// box left picture
$_ARTICLE_CONTENT_TEMPLATES[4] = "
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr><td colspan=\"3\" height=\"15\" >&nbsp;</td></tr>
		<tr>
			<td width=\"320\" height=\"240\" valign=\"top\" class=\"col-teaser-image\">
				<table width=\"320\" cellspacing=\"0\" cellpadding=\"0\">
					<tr><td><span  class=\"teaser-image\" style=\"{$_STYLES['teaser-image']}\"><img src=\"{image}\" /></span></td></tr>
				</table>
			</td>
			<td width=\"15\" class=\"col-teaser-image-sep\" >&nbsp;</td>
			<td valign=\"top\" class=\"col-teaser-text\" style=\"{$_STYLES['teaser-text']}\" >
				{title}
				{text}
				{files}
			</td>
		</tr>
	</table>
";

$sql = null;

if ($_TEST) {

	$sql = "
		SELECT DISTINCT
			news_article_content_id AS id,
			news_article_content_article_id AS article,
			news_article_content_data AS data,
			news_article_content_template_id AS template
		FROM newsletters
		INNER JOIN newsletter_articles ON newsletter_article_newsletter_id = newsletter_id
		INNER JOIN news_articles ON news_article_id = newsletter_article_article_id
		INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
		WHERE 
			news_article_content_template_id != 1 
			AND news_article_active = 1
			AND IF(news_article_expiry_date,  CURRENT_DATE <= news_article_expiry_date, 1) = 1
			AND newsletter_article_newsletter_id = ?
		ORDER BY 
			newsletter_article_sort, 
			news_article_content_article_id, 
			news_article_publish_date DESC,
			news_article_content_order
	";

} elseif ($_ADDITIONAL_RECIPIENTS) {
	$sql = "
		SELECT DISTINCT
			news_article_content_id AS id,
			news_article_content_article_id AS article,
			newsletter_recipient_additional_id AS recipient,
			news_article_content_data AS data,
			news_article_content_template_id AS template
		FROM newsletters
		INNER JOIN newsletter_recipients ON newsletter_recipient_newsletter_id = newsletter_id
		INNER JOIN newsletter_articles ON newsletter_article_newsletter_id = newsletter_id
		INNER JOIN news_articles ON news_article_id = newsletter_article_article_id
		INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
		WHERE 
			news_article_content_template_id != 1 
			AND news_article_active = 1
			AND IF(news_article_expiry_date,  CURRENT_DATE <= news_article_expiry_date, 1) = 1
			AND news_article_publish_state_id > 3
			AND newsletter_recipient_additional_id > 0 
			AND newsletter_article_newsletter_id = ?
		ORDER BY 
			newsletter_article_sort, 
			news_article_content_article_id, 
			news_article_publish_date DESC,
			news_article_content_order
	";
}

$sth = $model->prepare($sql);
$sth->execute(array($_ID));
$result = $sth->fetchAll();

if ($result) {

	foreach ($result as $row) {
		
		$id = $row['id'];
		$article = $row['article'];
		$recipient = $_TEST ? 0 : $row['recipient'];
		$template = $row['template'];
	
		if ($_ARTICLE_CONTENT_TEMPLATES[$template]) {

			$files = null;
			
			$data = $row['data'] ? unserialize($row['data']) : array();
			$data['id'] = $id;
			$data['article'] = $article;	

			if ($data['files']) {
				
				if ($template==3) {
				
					// pictures render
					foreach ($data['files'] as $file) {
						
						$hash = Encryption::url($file);
						$file = $_HOST."/download/get/$hash";
						
						$files .= "
							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
								<tr><td class=\"article-template-picture\" ><img width=\"100%\" src=\"$file\" class=\"img-responsive\” /></td></tr>
								<tr><td height=\"15\" >&nbsp;</td></tr>
							</table>
						";
					}

					$data['files'] = $files;

				} else {

					// file button
					foreach ($data['files'] as $file) {
						
						$extension = strtoupper(pathinfo($file, PATHINFO_EXTENSION));
						$hash = Encryption::url($file);
						$fileTitle = $data['fileTitles'] && $data['fileTitles'][$file] ? $data['fileTitles'][$file] : 'DOWNLOAD '.$extension;
						$fileTitle = strlen($fileTitle) > $_FILE_TITLE_LENGTH ? substr($fileTitle, 0, $_FILE_TITLE_LENGTH).'..' : $fileTitle;
						$btnWidth = strlen($fileTitle)*$_LETTER_PX;
						$btnLinkWidth = $btnWidth-7;
						
						$search = array('{title}', '{url}', '{btnWidth}', '{btnLinkWidth}');
						$replace = array($fileTitle, $_HOST."/download/news/$hash/$_ID/{user_id}/{additional}", $btnWidth, $btnLinkWidth); 
						$files .= str_replace($search, $replace, $_TEMPLATES['button']);
					}
					
					$data['files'] = $files;
				}
			}

			if ($data['image']) {
				$hash = Encryption::url($data['image']);
				$data['image'] = $_HOST."/download/get/$hash";
			}

			$_ARTICLE_CONTENTS[$article][$recipient] .= Content::render($_ARTICLE_CONTENT_TEMPLATES[$template], $data);
		} 
	}
}


// articles dataloader :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ARTICLES = array();
$_ARTICLE_DATA = array();

foreach ($articles as $row) {

	$id = $row['id'];
	$sid = $row['sid'];

	$_ARTICLE_DATA[$id] = $row;

	// section name
	$_ARTICLES[$sid]['section'] = $row['section'];

	// article role restrictions
	if ($row['roles']) {
		$_ARTICLES[$sid]['articles'][$id]['roles'] = explode(',', $row['roles']);
	}

	// article company restrictions
	if ($row['companies']) {
		$_ARTICLES[$sid]['articles'][$id]['companies'] = explode(',', $row['companies']);
	}

	// teaser data
	$data = $row['data'] ? unserialize($row['data']) : array();

	// article data
	$data['id'] = $row['id'];
	$data['date'] = $row['date'] ? 'Date: '.$row['date'] : null;
	$data['informations'] = $row['informations'];
	$data['article_title'] = $row['title'];

	// teaser image
	if ($data['image']) {

		$hash = Encryption::url($data['image']);
		$image = $_HOST."/download/get/$hash";

		$data['image'] = "
			<td width=\"320\" height=\"240\" valign=\"top\" class=\"col-teaser-image\" >
				<table width=\"320\" cellspacing=\"0\" cellpadding=\"0\">
					<tr>
						<td valign=\"top\">
							<span class=\"teaser-image\" style=\"{$_STYLES['teaser-image']}\">
								<a href=\"$_LINK#article-$id\"><img width=\"320\" src=\"$image\" /></a>
							</span>
						</td>
					</tr>
				</table>
			</td>
			<td width=\"15\" class=\"col-teaser-image-sep\" >&nbsp;</td>
		";
	}

	// teaser title
	if ($data['title']) {

		$title = $data['title'];

		$data['title'] = "
			<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				<tr><td class=\"teaser-title\" style=\"{$_STYLES['teaser-title']}\" >$title</td></tr>
				<tr><td height=\"15\" >&nbsp;</td></tr>
			</table>
		";
	}

	// teaser text
	if ($data['text']) {

		$text = $data['text'];

		$data['text'] = "
			<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				<tr><td class=\"teaser-text\" style=\"{$_STYLES['teaser-text']}\" >$text</td></tr>
				<tr><td height=\"15\" >&nbsp;</td></tr>
			</table>
		";
	}

	// teaser files
	if ($data['files']) {
		$teaserFiles = null;
		foreach ($data['files'] as $file) {
			$extension = strtoupper(pathinfo($file, PATHINFO_EXTENSION));
			$hash = Encryption::url($file);
			$fileTitle = $data['fileTitles'] && $data['fileTitles'][$file] ? $data['fileTitles'][$file] : 'DOWNLOAD '.$extension;
			$fileTitle = strlen($fileTitle) > $_FILE_TITLE_LENGTH ? substr($fileTitle, 0, $_FILE_TITLE_LENGTH).'..' : $fileTitle;
			$btnWidth = strlen($fileTitle)*$_LETTER_PX;
			$btnLinkWidth = $btnWidth-7;
			$search = array('{title}', '{url}', '{btnWidth}', '{btnLinkWidth}');
			$replace = array($fileTitle, $_HOST."/download/news/$hash/$_ID/{user_id}/{additional}", $btnWidth, $btnLinkWidth); 
			$teaserFiles .= str_replace($search, $replace, $_TEMPLATES['button']);
		}
		$data['files'] = $teaserFiles;
	}

	// news authors
	if ($row['author_id']) {
		$key = $row['author_id'];
		$data['author'] = $_NEWS_AUTHORS[$key];
	}

	// retailnet authors
	if ($row['author_user_id']) {
		$key = $row['author_user_id'];
		$data['author'] = $_USER_AUTHORS[$key];
	}

	// articke sticker
	/*
	if ($row['sticker']) {
		
		$sticker = unserialize($row['sticker']);
		$title = $sticker['title'];
		
		if ($sticker['id']) unset($sticker['id']);
		if ($sticker['title']) unset($sticker['title']);

		$background = $sticker['background-image']; // url(/data/stickers/fielname)
		$background = str_replace('url(', '', $background);
		$background = str_replace(')', '', $background);
		$hash = Encryption::url($background);
		$background = $_HOST."/download/get/$hash";

		$left = $sticker['left'];
		$left = str_replace('px', '', $left);
		$left = $left ? $left : 0;
		$left = $left."px";

		$top = $sticker['top'];
		$top = str_replace('px', '', $top);
		$top = $top ? $top-100 : 0;
		$top = $top."px";

		$data['sticker'] = Content::render($_TEMPLATES['sticker'], array(
			'title' => $title,
			'width' => $sticker['width']."px",
			'height' => $sticker['height']."px",
			'style' => "
				background-repeat:no-repeat; 
				background-size:cover; 
				background-image:url($background); 
				position:absolute; 
				left: $left; 
				top: $top
			"
		));
	}
	*/

	// teaser parser
	$_ARTICLES[$sid]['articles'][$id]['teaser'] =  Content::render($_TEMPLATES['teaser'], $data);
}
	

// newsletter visual :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
				
if ($_NEWSLETTER_FILES) {

	$visualFiles = null;

	foreach ($_NEWSLETTER_FILES as $file) {
		
		$extension = strtoupper(pathinfo($file['path'], PATHINFO_EXTENSION));
		$hash = Encryption::url($file['path']);
		$fileTitle = $file['title'] ?: 'DOWNLOAD '.$extension;
		$fileTitle = strlen($fileTitle) > $_FILE_TITLE_LENGTH ? substr($fileTitle, 0, $_FILE_TITLE_LENGTH).'..' : $fileTitle;
		$btnWidth = strlen($fileTitle)*$_LETTER_PX;
		$btnLinkWidth = $btnWidth-7;
		$search = array('{title}', '{url}', '{btnWidth}', '{btnLinkWidth}');
		$replace = array($fileTitle, $_HOST."/download/newsletter/$hash/$_ID/{user_id}/{additional}", $btnWidth, $btnLinkWidth); 
		$visualFiles .= str_replace($search, $replace, $_TEMPLATES['button']);
	}
} 

$newsletter['files'] = $visualFiles;
$newsletter['link'] = $_LINK;

if ($newsletter['image']) {
	$hash = Encryption::url($newsletter['image']);
	$newsletter['image'] = $_HOST."/download/get/$hash";
}

$_VISUAL = Content::render($_TEMPLATES['visual'], $newsletter);


// newsletter builder ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NEWSLETETRS = array();
$_SENT_ARTICLES = array();

foreach ($_RECIPIENTS as $recipient) {

	$_CONTENT = null;

	// recipient roles
	$recipient['roles'] = $recipient['roles'] ? explode(',', $recipient['roles']) : array();


	foreach ($_ARTICLES as $sid => $row) {

		$articles = null;

		foreach ($row['articles'] as $id => $article) {
			
			$access = true;

			// article  role restriction
			if (!$_NEWS_ADMINISTRATORS[$recipient['user']] && ($article['roles'] || $article['companies']) && !$_TEST) {

				$checkRolesAccess = $recipient['additional'] ? false : true;
				$checkCompaniesAccess = $recipient['additional'] ? false : true;
				
				if ($article['roles'] && $recipient['roles']) {
					$permittedRoles = array_intersect($article['roles'], $recipient['roles']);
					$checkRolesAccess = count($permittedRoles) > 0 ? true : false;
				}

				// article company restriction
				if ($article['companies'] && $recipient['company']) {
					$checkCompaniesAccess = in_array($recipient['company'], $article['companies'])  ? true : false;
				}

				$access = $checkRolesAccess && $checkCompaniesAccess ? true : false;
			}

			if ($access) {

				$readMore = null;
				$_SENT_ARTICLES[] = $id;

				$usr = $recipient['user'];

				// retailnet recipient
				if ($usr) {
					
					// read more button
					if (($_ARTICLE_CONTENTS[$id] && $_ARTICLE_CONTENTS[$id][$usr]) || ($_ARTICLE_DATA[$id] && $_ARTICLE_DATA[$id]['totalTemplates']) ) {
						$search = array('{title}', '{url}');
						$replace = array('READ MORE', $_LINK."#article-$id");
						$readMore = str_replace($search, $replace, $_TEMPLATES['button']);
					}

					// render user id and hash for download url's
					$article['teaser'] = Content::render($article['teaser'], array(
						'readmore' => $readMore,
						'user_id' => $usr
					), true);

				} elseif ($recipient['additional']) { 


					// render additional user id and hash for download url's
					$article['teaser'] = Content::render($article['teaser'], array(
						'user_id' => $recipient['additional'],
						'additional' => 1
					), true);
					
					if ($_ARTICLE_CONTENTS[$id]) { 
						
						$additional = $_TEST ? 0 : $recipient['additional'];
						
						$articleContent = $_ARTICLE_CONTENTS[$id][$additional];

						$article['teaser'] += Content::render($articleContent, array(
							'user_id' => $recipient['additional'],
							'additional' => 1
						), true);
					}
				}

				if ($access && $article['teaser']) {
					$articles .= $article['teaser'];
					$articles .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tbody>";
					$articles .= "<tr><td class=\"article-separator\" style=\"{$_STYLES['article-separator']}\" >&nbsp;</td></tr>";
					$articles .= "</tbody></table>";
				}
			}
		}

		if ($articles) {
			
			// section content
			$_CONTENT .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tbody>";
			$_CONTENT .= "<tr><td class=\"section\" style=\"{$_STYLES[section]}\" >{$row[section]}</td></tr>";
			$_CONTENT .= "<tr><td height=\"10\" >&nbsp;</td></tr>";
			$_CONTENT .= "<tr><td class=\"section-articles\">$articles</td></tr>";
			$_CONTENT .= "</tbody></table>";

			// section separator
			$_CONTENT .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$_CONTENT .= "<tr><td height=\"20\">&nbsp;</td></tr>";
			$_CONTENT .= "</table>";
		}
	}

	if ($_CONTENT) {

		$visual = null;

		// render all user id by download url's
		if ($recipient['user']) {
			$visual = Content::render($_VISUAL, array(
				'user_id' => $recipient['user']
			), true);
		}

		// render all user additional id by download url's
		if ($recipient['additional']) {
			$visual = Content::render($_VISUAL, array(
				'user_id' => $recipient['additional'],
				'additional' => 1
			), true);
		}

		$dataRender = array(
			'id' => $_ID,
			'number' => $_NEWSLETTER_NUMBER,
			'visual' => $visual,
			'content' => $_CONTENT
		);

		if ($newsletter['background']) {
			$hash = Encryption::url($newsletter['background']);
			$bgLink = $_HOST."/download/get/$hash";
			$dataRender['background'] = 'style="background: url('.$bgLink.'); background-position: top center;"';
			$dataRender['background_style'] = $bgLink;
		}
		
		// render all master template data
		$content = Content::render($_TEMPLATE, $dataRender, true);

		$_NEWSLETETRS[] = array(
			'recipient' => $recipient['name'],
			'email' => $recipient['mail'],
			'content' => $content,
			'user' => $recipient['user'],
			'additional' => $recipient['additional']
		);
	}
	else {

		$recipientId = $recipient['user'] ?: $recipient['additional'];
		$additionalText = $recipient['additional'] ? "Additional recipient" : null;

		$_CONSOLE[] = "No permitted newsletter articels for recipient $recipientId $additionalText";
		
		// track: no permitted newsletter articels
		if (!DEBUGGING_MODE) {
			$track->execute(array($_ID, $recipient['user'], $recipient['additional'], 14, null));
		}
	}

}

if (!$_NEWSLETETRS) {
	$_ERRORS = "Newsletter hasn't permitted recipients. Contact system administrator.";
	goto BLOCK_RESPNDING;
}

$_CONSOLE[] = "Total newsletters: ".count($_NEWSLETETRS);
$_DATA['newsletters'] = $_NEWSLETETRS;


// sendmail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$_SENDMAIL_EMAILS = array();
$_SENDMAIL_USERS = array();

$senderID = $user->id;
$senderName = "$user->firstname $user->name";
$senderMail = $user->email;

// update user sent date
$updateUserSentDate = $model->prepare("
	UPDATE newsletter_recipients SET 
		newsletter_recipient_sent_date = NOW()
	WHERE newsletter_recipient_newsletter_id = ? AND newsletter_recipient_user_id = ?
");

// update user sent date
$updateAdditionalUserSentDate = $model->prepare("
	UPDATE newsletter_recipients SET 
		newsletter_recipient_sent_date = NOW()
	WHERE newsletter_recipient_newsletter_id = ? AND newsletter_recipient_additional_id = ?
");

foreach ($_NEWSLETETRS as $i => $newsletter) {
	
	// recipient email
	$recipientMail = $newsletter['email'];
	
	if (!in_array($recipientMail, $_SENDMAIL_EMAILS)) {
		

		$mail = new PHPMailer();
		$mail->CharSet = 'UTF-8';

		// mail subject
		$subject = "Swatch Gazette No. $_NEWSLETTER_NUMBER ";
		$mail->Subject = $subject;

		// mail content
		$body = $newsletter['content']; 

		// track opened mail hack
		$userId = $newsletter['user'] ?: $newsletter['additional'];
		$additional = $newsletter['additional'] ? 'additional' : null;
		$body .= "<img src='$_HOST/download/newslettertrack/$_ID/$userId/18/$additional' width='1px' height='1px' >"; 

		$mail->AltBody = strip_tags($content);
		$mail->Body = $body;
		$mail->MsgHTML($body);
		$mail->IsHTML(true);

		// sender
		$mail->SetFrom($_SENDER_ADDRESS, $_SENDER_NAME);
		
		// recipient
		$recipientName = $newsletter['recipient'];
		$email = $_DEV_MAIL ? $_DEV_MAIL : $recipientMail;
		$mail->AddAddress($email, $recipientName);

		// send
		$send = DEBUGGING_MODE ? true : $mail->Send();

		if ($send) {
			$_SENDMAIL_EMAILS[] = $recipientMail;
		}

		if ($send) {

			// for standard recipient
			if (!DEBUGGING_MODE && $newsletter['user']) {

				// track: successfully send newsletter to recipient.
				$key = $_TEST ? 63 : 15;
				$track->execute(array($_ID, $newsletter['user'], null, $key, $user->id));

				// update recipient send date
				$updateUserSentDate->execute(array($_ID, $newsletter['user']));
			}

			// for additional recipient
			if (!DEBUGGING_MODE && $newsletter['additional']) {

				// track: successfully send newsletter to recipient.
				$key = $_TEST ? 63 : 30;
				$track->execute(array($_ID, null, $newsletter['additional'], $key, $user->id));
				
				// update adiotional recipient send date
				$updateAdditionalUserSentDate->execute(array($_ID, $newsletter['additional']));
			}
		}
		else {

			$_CONSOLE[] = "Error on sent newsletter for user $userId";

			// track: system error on send newsletter to recipient
			if (!DEBUGGING_MODE) {
				$additionalText = $newsletter['additional'] ? "Additional recipient" : null;
				$track->execute(array($_ID, $newsletter['user'], $newsletter['additional'], 16, null));
			}
		}
	}
}

$_NEWSLETTER_SENT = $_SENDMAIL_EMAILS ? true : false;


// set newsletter articles send date :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && !$_TEST && !DEBUGGING_MODE && $_SENT_ARTICLES) {

	$_SENT_ARTICLES = array_unique($_SENT_ARTICLES);

	$sth = $model->prepare("
		UPDATE newsletter_articles SET 
			newsletter_article_send_date = NOW()
		WHERE newsletter_article_newsletter_id = ? AND newsletter_article_article_id = ?
	");

	foreach ($_SENT_ARTICLES as $article) {
		$sth->execute(array($_ID, $article));
	}
}


// reset newsletter selected recipients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && !$_TEST  && !DEBUGGING_MODE && $_NEWSLETTER_SENT) {

	// reset all selected recipients
	$sth = $model->prepare("
		UPDATE newsletter_recipients SET 
			newsletter_recipient_selected = NULL
		WHERE newsletter_recipient_newsletter_id = ?
	");

	$response = $sth->execute(array($_ID));

	// track: reset newsletter selected recipients
	if ($response) {
		$track->execute(array($_ID, $user->id, null, 17, null));
	}
}


// update newsletter sent/send date ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && !$_TEST  && !DEBUGGING_MODE) {

	$sth = $model->prepare("
		UPDATE newsletters SET 	
			newsletter_send_date = NULL,
			newsletter_sent_date = CURDATE(),
			newsletter_send_user_id = NULL
		WHERE newsletter_id = ?
	");

	$respone = $sth->execute(array($_ID));

	// track: send newsletter successfully submitted
	if ($response) {
		$track->execute(array($_ID, $user->id, null, 23, null));
	}
}

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPNDING:

if (!DEBUGGING_MODE) {

	if ($_ERRORS) {
		foreach ($_ERRORS as $text) {
			$_NOTIFICATIONS[] = array(
				'type'=>'error', 
				'title'=>'Error', 
				'text'=>$text
			);
		}
	} else {

		$total = count($_NEWSLETETRS);

		if (!$_TEST) {
			$reload = true;
			Message::success("Total sent $total newsletter(s)");
		}
	}

	$_JSON = array(
		'success' => $_NEWSLETTER_SENT,
		'reload' => $reload,
		'console' => $_CONSOLE
	);

	if ($_NEWSLETTER_SENT && $_TEST) {
		$_JSON['message'] = "Total sent $total newsletter(s)";
	}

} else {

	$_JSON = array(
		'newsletter' => $_ID,
		'success' => $_NEWSLETTER_SENT,
		'errors' => $_ERRORS,
		'console' => $_CONSOLE
	);
}

header('Content-Type: text/json');
echo json_encode($_JSON);

