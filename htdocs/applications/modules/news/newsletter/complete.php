<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

ob_start();

define('DEBUGGING_MODE', false);

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = 'news';
$_STATE = $_REQUEST['state'];
$_ID = $_REQUEST['id'];

// response
$_JSON = array();

// db model
$model = new Model($_APPLICATION);

// permissions
$_PERMISSIONS = array(
	'edit' => user::permission('can_edit_newsletter'),
	'sent' => user::permission('can_sent_newsletter')
);


// read newsletter
$newsletter = new Modul($_APPLICATION);
$newsletter->setTable('newsletters');
$newsletter->read($_ID);

$newsletterTitle = $newsletter->data['newsletter_title'];


// checkin :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_PERMISSIONS['edit']) {
	$_ERRORS[] = "You don't have permissions to submit this action.";
}

if (!$newsletter->id) {
	$_ERRORS[] = "Newsletter not found.";
}

if ($_STATE<>2) {
	$_ERRORS[] = "The submitting state is wrong.";
}

// get newslettter articles
if (!$_ERRORS) {
	
	$sth = $model->db->prepare("
		SELECT COUNT(DISTINCT news_article_id) as total
		FROM news_articles
		INNER JOIN newsletter_articles ON newsletter_article_article_id = news_article_id
		WHERE newsletter_article_newsletter_id = ?
	");

	$sth->execute(array($_ID));
	$articles = $sth->fetch();

	if (!$articles['total']) {
		$_ERRORS[] = "Newsletter has no article(s)";
	}
}

// get article state
if (!$_ERRORS) {
	
	$sth = $model->db->prepare("
		SELECT COUNT(DISTINCT news_article_id) as total
		FROM news_articles
		INNER JOIN newsletter_articles ON newsletter_article_article_id = news_article_id
		WHERE newsletter_article_newsletter_id = ? AND news_article_publish_state_id < 3
	");

	$sth->execute(array($_ID));
	$articles = $sth->fetch();

	if ($articles['total']>0) {
		$_ERRORS[] = "The Newsletter cannot be published, please approve all Article(s)";
	}
}


// trackers ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$track = $model->db->prepare("
	INSERT INTO newsletter_tracks (
		newsletter_track_newsletter_id,
		newsletter_track_user_id,
		newsletter_track_type_id
	)
	VALUES (?,?,?)
");


// newletter completing ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		UPDATE newsletters SET 
			newsletter_publish_state_id = 2,
			user_modified = ?,
			date_modified = NOW()
		WHERE newsletter_id = ?
	");

	$_JSON['success'] = DEBUGGING_MODE ? true : $sth->execute(array($user->login, $_ID));

	if ($_JSON['success']) {

		$_SUCCESSES[] = "Newsletter <b>$newsletterTitle</b> is ready to send"; 

		$_JSON['button'] = array(
			'title' => 'Publish',
			'icon' => '<i class="fa fa-globe"></i> ',
			'data' => array(
				'state'=>3
			),
			'attributes' => array(
				'id' => 'btn-publish',
				'class' => 'btn btn-success btn-state btn-dialog',
				'href' => '/applications/modules/news/newsletter/publish.php'
			)
	); 

		$_JSON['delete'] = $_PERMISSIONS['edit'] ? true : false;

	} else {
		$_ERRORS[] = "The Newsletter ist not submitted. Please contact system adminstrator.";
	}

	// tracker
	if (!DEBUGGING_MODE && $_JSON['success']) {
		$track->execute(array($_ID, $user->id, 10));
	}
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_ERRORS) {
	foreach ($_ERRORS as $text) {
		$_JSON['notifications'][] = array(
			'type'=>'error', 
			'title'=>'Error', 
			'text'=>$text
		);
	}
}

if ($_SUCCESSES) {
	foreach ($_SUCCESSES as $text) {
		$_JSON['notifications'][] = array(
			'type'=>'success', 
			'title'=>'Success', 
			'text'=>$text
		);
	}
}

$_JSON['debugging'] = DEBUGGING_MODE;

ob_end_clean();
header('Content-Type: text/json');
echo json_encode($_JSON);

