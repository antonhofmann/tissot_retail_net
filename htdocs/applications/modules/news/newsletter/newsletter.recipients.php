<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = 'news';
$_SECTION = $_REQUEST['section'];
$_ID = $_REQUEST['id'];

// response
$_JSON = array();

// db model
$model = new Model($_APPLICATION);

// permissions
$_PERMISSIONS = array(
	'edit' => user::permission('can_edit_newsletter'),
	'sent' => user::permission('can_sent_newsletter')
);

// checkin :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ID) {
	$_ERRORS[] = "Newsletter not found. Please check your request.";
	goto BLOCK_RESPONDING;
}


// trackers ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$track = $model->db->prepare("
	INSERT INTO newsletter_tracks (
		newsletter_track_newsletter_id,
		newsletter_track_user_id,
		newsletter_track_type_id,
		newsletter_track_entity
	)
	VALUES (?,?,?,?)
");

// submitting ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

switch ($_SECTION) {

	case 'standard-recipients':

		$recipient = $_REQUEST['recipient'];
		$role = $_REQUEST['role'];
		$selected = $_REQUEST['action'];

		if (!$role || !$recipient) {
			$_ERRORS[] = "Request failure. Please contact system administrator";
			goto BLOCK_RESPONDING;
		}

		// get recipient
		$sth = $model->db->prepare("
			SELECT 
				newsletter_recipient_id AS id,
				newsletter_recipient_selected AS selected,
				newsletter_recipient_sent_date AS sent_date
			FROM newsletter_recipients
			WHERE newsletter_recipient_newsletter_id = ? 
			AND newsletter_recipient_user_id = ?
			AND newsletter_recipient_role_id = ?
		");

		$sth->execute(array($_ID, $recipient, $role));
		$result = $sth->fetch();

		// ACTION: ADD RECIPIENT
		if ($selected) {

			// recipient belong to newsletter,
			// set recipient as selected
			if ($result['id']) {

				$sth = $model->db->prepare("
					UPDATE newsletter_recipients SET 
						newsletter_recipient_selected = 1
					WHERE newsletter_recipient_id = ?
				");

				$response = $sth->execute(array($result['id']));
			}
			// add recipient to newsletter
			else {

				$sth = $model->db->prepare("
					INSERT INTO newsletter_recipients (
						newsletter_recipient_newsletter_id,
						newsletter_recipient_role_id,
						newsletter_recipient_user_id,
						newsletter_recipient_selected,
						user_created
					)
					VALUES (?,?,?,1,?)
				");

				$response = $sth->execute(array($_ID, $role, $recipient, $user->login));
			}

			if ($response) {
				// track: add newsletter recipient [5]
				$track->execute(array($_ID, $user->id, 5, $recipient));
			}
		}

			
		// ACTION: REMOVE RECIPINET
		if ($result['id'] && !$selected) {

			// in case recipient has sand date
			// unselect recipient from newsletter
			if ($result['sent_date']) {

				$sth = $model->db->prepare("
					UPDATE newsletter_recipients SET 
						newsletter_recipient_selected = NULL
					WHERE newsletter_recipient_id = ?
				");

				$response = $sth->execute(array($result['id']));
			}
			// otherwise, remove recipient from newsletter
			else {
				$sth = $model->db->prepare("
					DELETE FROM newsletter_recipients
					WHERE newsletter_recipient_id = ?
				");

				$response = $sth->execute(array($result['id']));
			}

			if ($response) {
				// track: remove newsletter recipient [6]
				$track->execute(array($_ID, $user->id, 6, $recipient));
			}
		}

		$_JSON['success'] = $response;

	break;

	case 'group-standard-recipients' :

		$recipients = $_REQUEST['recipients'];
		$role = $_REQUEST['role'];

		if (!$role) {
			$_ERRORS[] = "Request failure. Please contact system administrator";
			goto BLOCK_RESPONDING;
		}

		// get recipient
		$sth = $model->db->prepare("
			SELECT 
				newsletter_recipient_id AS id,
				newsletter_recipient_user_id AS user,
				newsletter_recipient_sent_date AS sentdate
			FROM newsletter_recipients
			WHERE newsletter_recipient_newsletter_id = ? AND newsletter_recipient_role_id = ?
		");

		$sth->execute(array($_ID, $role));
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$usr = $row['user'];
				$_RECIPIENTS[$usr] = $row['id'];
			}
		}

		// add statement
		$addRecipient = $model->db->prepare("
			INSERT INTO newsletter_recipients (
				newsletter_recipient_newsletter_id,
				newsletter_recipient_role_id,
				newsletter_recipient_user_id,
				newsletter_recipient_selected,
				user_created
			)
			VALUES (?,?,?,1,?)
		");

		// update statement
		$updateRecipient = $model->db->prepare("
			UPDATE newsletter_recipients SET 
				newsletter_recipient_selected = ?
			WHERE newsletter_recipient_id = ?
		");		
		
		// remove statement
		$removeRecipient = $model->db->prepare("
			DELETE FROM newsletter_recipients
			WHERE newsletter_recipient_id = ?
		");

		// ACTION: ADD RECIPIENTS
		if ($recipients) {
			
			foreach ($recipients as $recipient) {

				$response = false;
				
				// recipient belong to newsletter
				if ($_RECIPIENTS[$recipient]) {
					$response = $updateRecipient->execute(array(1, $_RECIPIENTS[$recipient]));
				}
				// add recipient to newsletter
				else {
					$response = $addRecipient->execute(array($_ID, $role, $recipient, $user->login));
				}

				if ($response) {
					// track: add newsletter recipient [5]
					$track->execute(array($_ID, $user->id, 5, $recipient));
				}
			}
		}

		// ACTION: REMOVE RECIPIENTS
		if ($result && !$recipients) {

			foreach ($result as $row) {

				$response = false;
				$id = $row['id'];
				$recipient = $row['user'];

				// remove newsletter recipient only in case when send date is null
				if (!$row['sentdate']) {
					$response = $removeRecipient->execute(array($id));
				}
				// unselect recipient from newsletter
				else {
					$response = $updateRecipient->execute(array(null, $id));
				}

				if ($response) {
					// track: remove newsletter recipient [6]
					$track->execute(array($_ID, $user->id, 6, $recipient));
				}
			}
		} 

		$_JSON['success'] = true;

	break;

	case 'additional-recipients':

		$recipient = $_REQUEST['recipient'];
		$selected = $_REQUEST['action'];

		if ($recipient) {

			// get recipient
			$sth = $model->db->prepare("
				SELECT 
					newsletter_recipient_id AS id,
					newsletter_recipient_selected AS selected,
					newsletter_recipient_sent_date AS sent_date
				FROM newsletter_recipients
				WHERE newsletter_recipient_newsletter_id = ? AND newsletter_recipient_additional_id = ?
			");

			$sth->execute(array($_ID, $recipient));
			$result = $sth->fetch();

			// ACTION: ADD
			if ($selected) {

				// in case recipint belong to newsletter
				if ($result['id']) {
					
					$sth = $model->db->prepare("
						UPDATE newsletter_recipients SET 
							newsletter_recipient_selected = 1
						WHERE newsletter_recipient_id = ?
					");

					$response = $sth->execute(array($result['id']));
				}
				// add recipient to newsletter
				else {

					$sth = $model->db->prepare("
						INSERT INTO newsletter_recipients (
							newsletter_recipient_newsletter_id,
							newsletter_recipient_additional_id,
							newsletter_recipient_selected,
							user_created
						)
						VALUES (?,?,1,?)
					");

					$response = $sth->execute(array($_ID, $recipient, $user->login));
				}

				if ($response) {
					// track: add newsletter addiotional recipient [25]
					$track->execute(array($_ID, $user->id, 25, $recipient));
				}
			}


			// ACTION: DELETE
			if ($result['id'] && !$selected) {

				// in case recipient has sand date
				if ($result['sent_date']) {

					$sth = $model->db->prepare("
						UPDATE newsletter_recipients SET 
							newsletter_recipient_selected = NULL
						WHERE newsletter_recipient_id = ?
					");

					$response = $sth->execute(array($result['id']));
				}
				// remove reciopient from newsletter
				else {

					$sth = $model->db->prepare("
						DELETE FROM newsletter_recipients
						WHERE newsletter_recipient_id = ?
					");

					$response = $sth->execute(array($result['id']));
				}

				if ($response) {
					// track: remove newsletter additional recipient [26]
					$track->execute(array($_ID, $user->id, 26, $recipient));
				}
			}

			$_JSON['success'] = $response;
		}

	break;

	case 'group-additional-recipients':

		$recipients = $_REQUEST['recipients'];

		// get recipient
		$sth = $model->db->prepare("
			SELECT 
				newsletter_recipient_id AS id,
				newsletter_recipient_additional_id AS user,
				newsletter_recipient_sent_date AS sentdate
			FROM newsletter_recipients
			WHERE newsletter_recipient_newsletter_id = ? AND newsletter_recipient_additional_id IS NOT NULL
		");

		$sth->execute(array($_ID));
		$_JSON['result'] = $result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$usr = $row['user'];
				$_RECIPIENTS[$usr] = $row['id'];
			}
		}

		// add statement
		$addRecipient = $model->db->prepare("
			INSERT INTO newsletter_recipients (
				newsletter_recipient_newsletter_id,
				newsletter_recipient_additional_id,
				newsletter_recipient_selected,
				user_created
			)
			VALUES (?,?,1,?)
		");

		// update statement
		$updateRecipient = $model->db->prepare("
			UPDATE newsletter_recipients SET 
				newsletter_recipient_selected = ?
			WHERE newsletter_recipient_id = ?
		");		
		
		// remove statement
		$removeRecipient = $model->db->prepare("
			DELETE FROM newsletter_recipients
			WHERE newsletter_recipient_id = ?
		");

		// ACTION ADD
		if ($recipients) {
			
			foreach ($recipients as $recipient) {
				
				$response = false;

				// select recipient for newsletter
				if ($_RECIPIENTS[$recipient]) {
					$response = $updateRecipient->execute(array(1, $_RECIPIENTS[$recipient]));
				}
				// add recipient to newsletter
				else {
					$response = $addRecipient->execute(array($_ID, $recipient, $user->login));
				}

				if ($response) {
					// track: add newsletter addiotional recipient [25]
					$track->execute(array($_ID, $user->id, 25, $recipient));
				}
			}
		}

		// ACTION: DELETE
		if ($result && !$recipients) {
			
			foreach ($result as $row) {

				$response = false;
				$id = $row['id'];
				$recipient = $row['user'];
				
				// remove newsletter additional recipient 
				// only in case when send date is null
				if (!$row['sentdate']) {
					$response = $removeRecipient->execute(array($id));
				}
				// otherwise, unselect additional recipient
				else {
					$response = $updateRecipient->execute(array(null, $id));
				}

				if ($response) {
					// track: remove newsletter additional recipient [26]
					$track->execute(array($_ID, $user->id, 26, $recipient));
				}
			}
		} 

		$_JSON['success'] = true;

	break;

	case 'add-additional-recipient':

		$firstName = $_REQUEST['news_recipient_firstname'];
		$name = $_REQUEST['news_recipient_name'];
		$email = $_REQUEST['news_recipient_email'];

		if (!$firstName || !$name || !$email) {
			$_ERRORS[] = "Red marked fields are required.";
			break;
		}

		$sth = $model->db->prepare("
			SELECT *
			FROM news_recipients
			WHERE news_recipient_email = ?
		");

		$sth->execute(array($email));
		$result = $sth->fetch();

		if ($result['news_recipient_id']) {
			$_ERRORS[] = "The email address <b>$email</b> already exists";
			break;
		}

		$sth = $model->db->prepare("
			INSERT INTO news_recipients (
				news_recipient_firstname,
				news_recipient_name,
				news_recipient_email,
				news_recipient_active,
				user_created
			) 
			VALUES (?,?,?,1,?)
		");

		$sth->execute(array($firstName, $name, $email, $user->login));
		$id = $model->db->lastInsertId();

		if ($id) {

			if ($response) {
				// track: add additional recipient
				$track->execute(array($_ID, $user->id, 27, $id));
			}

			$_JSON['success'] = true;

			$_JSON['recipient'] = array(
				'id' => $id,
				'name' => "$firstName $name"
			);

		} else {
			$_ERRORS[] = "DB failure: Unable to add new recipient, please conact system adminsitrator. ";
		}

	break;

	case 'load':

		$sth = $model->db->prepare("
			SELECT 
				newsletter_id,
				newsletter_title,
				newsletter_text,
				newsletter_publish_state_id,
				newsletter_image,
				newsletter_files,
				DATE_FORMAT(newsletter_publish_date, '%d.%m.%Y') AS newsletter_publish_date,
				DATE_FORMAT(newsletter_send_date, '%d.%m.%Y') AS newsletter_send_date,
				DATE_FORMAT(newsletter_sent_date, '%M, %e %Y') AS sentDate
			FROM newsletters
			WHERE newsletter_id = ?
		");

		$sth->execute(array($_ID));
		$newsletter = $sth->fetch();
		unset($newsletter['newsletter_files']);
		$_JSON['data'] = $newsletter;

		$title = $newsletter['newsletter_title'];
		$state = $newsletter['newsletter_publish_state_id'];
		$publishDate = $newsletter['newsletter_publish_date'];
		$sentDate = $newsletter['sentDate'];

		// fields
		$fields = $newsletter ? array_keys($newsletter) : array();

		// disable form
		if (!$_PERMISSIONS['edit']) {
			$_JSON['disabled'] = $fields ? array_fill_keys($fields, true) : array();
		}


		// info block: count articles
		$sth = $model->db->prepare("
			SELECT COUNT(newsletter_article_id) AS total
			FROM newsletter_articles
			WHERE newsletter_article_newsletter_id = ?
		");

		$sth->execute(array($_ID));
		$result = $sth->fetch();
		$totalArticles = $result['total'];
		$_JSON['info']['newsletter-total-articles'] = $totalArticles;

		if (!$_PERMISSIONS['sent']) {

			$_JSON['alerts'][] = array(
				'type' => 'alert-danger',
				'content' => "You are not permitted to send newsletters."
			);

			$_JSON['remove'][] = '#action-send-newsletter';
			$_JSON['remove'][] = '#action-test-newsletter';
			$_JSON['remove'][] = '#newsletter_publish_date';

			break;
		} 

		// dialog sent
		$dialogSendNewsletter = $sentDate 
			? "This newsletter was sent on $sentDate. Are you sure to send this newsletter once again?"
			: "Are you sure you want to send this Newsletter?";

		$_JSON['dialogs'][] = array(
			'button' => '#action-send-newsletter',
			'content' => $dialogSendNewsletter
		);

		if (!$totalArticles) {
			
			$_JSON['alerts'][] = array(
				'type' => 'alert-warning',
				'dismissible' => true,
				'content' => "The newsletter <b>$title</b> has no articles. To send this newsletter or to set <i>Newsletter Send Date</i> you must select at least one artikel."
			);

			$_JSON['disabled'][] = 'action-send-newsletter';
			$_JSON['disabled'][] = 'action-test-newsletter';
			$_JSON['disabled'][] = 'newsletter_publish_date';
		}

		if ($state==1) {
			
			$_JSON['alerts'][] = array(
				'type' => 'alert-danger',
				'dismissible' => true,
				'content' => "The newsletter <b>$title</b> is in \"Draft\" status. All send mail actions are disabled."
			);

			$_JSON['disabled'][] = 'action-test-newsletter';
			$_JSON['disabled'][] = 'newsletter_publish_date';
		}


		if ($state < 3) {
			$_JSON['disabled'][] = 'action-send-newsletter';
		}

		// get newsletter send date
		$sth = $model->db->prepare("
			SELECT COUNT(newsletter_article_id) AS total
			FROM newsletter_articles
			WHERE newsletter_article_newsletter_id = ?
		");

		$sth->execute(array($_ID));
		$result = $sth->fetch();

	break;

	default:
		$_JSON['error'] = "Section not defined.";
	break;
}


// block responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONDING:

if ($_ERRORS) {
	foreach ($_ERRORS as $message) {
		$_JSON['notifications'][] = array(
			'type' => 'error',
			'text' => $message
		);
	}
}

header('Content-Type: text/json');
echo json_encode($_JSON);
