<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = 'news';
$_SECTION = $_REQUEST['section'];
$_ID = $_REQUEST['id'];
$_ARTICLE = $_REQUEST['article'];

// response
$_JSON = array();

// db model
$model = new Model($_APPLICATION);

// permissions
$_PERMISSIONS = array(
	'edit' => user::permission('can_edit_newsletter'),
	'sent' => user::permission('can_sent_newsletter')
);

$newsletter = new Modul($_APPLICATION);
$newsletter->setTable('newsletters');
$newsletter->read($_ID);

// article
$article = new Modul($_APPLICATION);
$article->setTable('news_articles');
$article->read($_ARTICLE);


// checkin :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_PERMISSIONS['edit']) {
	$_ERRORS[] = "You don't have permissions to submit this action.";
}

if (!$newsletter->id) {
	$_ERRORS[] = "Newsletter not found.";
}

if (!$article->id) {
	$_ERRORS[] = "Article not found.";
}


// trackers ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$trackNewsletter = $model->db->prepare("
	INSERT INTO newsletter_tracks (
		newsletter_track_newsletter_id,
		newsletter_track_user_id,
		newsletter_track_type_id,
		newsletter_track_entity
	)
	VALUES (?,?,?,?)
");

$trackArticle = $model->db->prepare("
	INSERT INTO news_article_tracks (
		news_article_track_article_id,
		news_article_track_newsletter_id,
		news_article_track_user_id,
		news_article_track_type_id,
		news_article_track_entity
	)
	VALUES (?,?,?,?,?)
");

// submit ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {
	
	switch ($_SECTION) {
		
		case 'add':

			$maxSort=1;

			$sth = $model->db->prepare("
				SELECT news_section_id
				FROM news_articles
				INNER JOIN news_categories ON news_category_id = news_article_category_id
				INNER JOIN news_sections ON news_category_section_id = news_section_id
				WHERE news_article_id = ?
			");

			$sth->execute(array($article->id));
			$result = $sth->fetch();

			// get max sort
			if ($result['news_section_id']) {
				
				$sth = $model->db->prepare("
					SELECT MAX(newsletter_article_sort) AS maxsort 
					FROM newsletter_articles
					INNER JOIN news_articles ON news_article_id = newsletter_article_article_id
					INNER JOIN news_categories ON news_category_id = news_article_category_id
					INNER JOIN news_sections ON news_category_section_id = news_section_id
					WHERE newsletter_article_newsletter_id = ? AND news_section_id = ?
				");

				$sth->execute(array($_ID, $result['news_section_id']));
				$result = $sth->fetch();
				$maxSort = $result['maxsort'] ? $result['maxsort']+1 : 1;
			}

			$sth = $model->db->prepare("
				INSERT INTO newsletter_articles (
					newsletter_article_newsletter_id,
					newsletter_article_article_id,
					newsletter_article_sort,
					user_created,
					newsletter_article_publish_date
				)
				VALUES (?,?,?,?,NOW())
			");

			$response = $sth->execute(array($_ID, $article->id, $maxSort, $user->login));

			if ($response) {

				// track: add newsletter article [3]
				$trackNewsletter->execute(array($_ID, $user->id, 3, $article->id));

				$_JSON['success'] = true;
				$newsletterState = $newsletter->data['newsletter_publish_state_id'];
				$articleState = $article->data['news_article_publish_state_id'];

				// publish article
				if ($newsletterState>2 && $articleState==3) {
					
					$updateArticleState = $model->db->prepare("
						UPDATE news_articles SET 
							news_article_publish_state_id = ?,
							news_article_publish_date = NOW(),
							user_modified = ?,
							date_modified = NOW() 
						WHERE news_article_id = ?
					");

					$action = $updateArticleState->execute(array(4, $user->login, $article->id));
					
					// track: set news article as published
					if ($action) {
						$trackArticle->execute(array($article->id, $_ID, $user->id, 36, null));
					}
				}

				// archive article
				if ($newsletterState==4) {
						
					$updateArticleState->execute(array(5, $user->login, $article->id));
					
					// track: set news article as archived
					$trackArticle->execute(array($article->id, $_ID, $user->id, 37, null));
				}
			}
			
		break;

		case 'remove':

			// check has articel mail send date
			$sth = $model->db->prepare("
				SELECT COUNT(newsletter_article_id) AS total
				FROM newsletter_articles 
				WHERE newsletter_article_article_id = ? AND newsletter_article_send_date IS NOT NULL
			");

			$sth->execute(array($article->id));
			$result = $sth->fetch();
			$hasMailSendDate = $result['total'] > 0 ? true : false;

			$sth = $model->db->prepare("
				DELETE FROM newsletter_articles
				WHERE newsletter_article_newsletter_id = ? AND newsletter_article_article_id = ?
			");

			$_JSON['success'] = $sth->execute(array($_ID, $article->id));

			if ($_JSON['success']) {

				// track: remove newsletter article
				$trackNewsletter->execute(array($_ID, $user->id, 4, $article->id));
				
				// if article is multiple uses
				// put aticle in "Other" container
				// otherwise put it in "Unassigned"
				$sth = $model->db->prepare("
					SELECT COUNT(newsletter_article_id) AS total
					FROM newsletter_articles
					WHERE newsletter_article_article_id = ?
				");

				$sth->execute(array($article->id));
				$result = $sth->fetch();
				

				if ($result['total']) {
					$_JSON['entity'] = '#other';
				}
				else {
					
					$_JSON['entity'] = '#unassigned';

					// if article is published and has not mail send date, reset to approved state
					if ($article->data['news_article_publish_state_id']==4 && !$hasMailSendDate) {

						$sth = $model->db->prepare("
							UPDATE news_articles SET 
								news_article_publish_state_id = 3,
								user_modified = ?,
								date_modified = NOW()
							WHERE news_article_id = ?
						");

						$response = $sth->execute(array($user->login, $article->id));

						if ($response) {
							$trackArticle->execute(array($article->id, $_ID, $user->id, 35, null));
						}
					}
				}
			}
			
		break;
		
		default:
			$_JSON['error'] = "Section not defined.";
		break;
	}
}


// article data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_JSON['success']) {

	$sth = $model->db->prepare("
		SELECT DISTINCT
			news_article_id AS id,
			CONCAT(user_firstname, ' ', user_name) AS author,
			news_article_title AS title,
			news_category_name AS category,
			IF(news_section_id, news_section_id, 0) AS section,
			IF(news_section_id, news_section_name, 'Uncategorised') AS section_name,
			news_workflow_state_name AS state,
			IF(news_article_featured, 'Yes', 'No') AS featured,
			news_article_content_data AS teaser,
			DATE_FORMAT(news_article_publish_date, '%d.%m.%Y') AS publish_date,
			DATE_FORMAT(news_article_expiry_date, '%d.%m.%Y') AS expiry_date,
			DATE_FORMAT(news_article_confirmed_date, '%d.%m.%Y') AS confirmed_date,
			DATE_FORMAT(news_articles.date_created, '%d.%m.%Y') AS created_date,
			news_article_active AS active,
			news_article_multiple AS multiple
		FROM news_articles
		INNER JOIN news_article_contents ON news_article_content_article_id = news_article_id
		INNER JOIN db_retailnet.users ON user_id = news_article_owner_user_id
		INNER JOIN news_workflow_states ON news_workflow_state_id = news_article_publish_state_id
		LEFT JOIN news_categories ON news_category_id = news_article_category_id
		LEFT JOIN news_sections ON news_category_section_id = news_section_id
		WHERE news_article_id = ? AND news_article_content_template_id = 1
	");

	$sth->execute(array($article->id));
	$result = $sth->fetch();
	$result['teaser'] = $result['teaser'] ? unserialize($result['teaser']) : array();
	$_JSON['article'] = $result;
}


if ($_ERRORS) {
	foreach ($_ERRORS as $text) {
		$_JSON['notifications'][] = array(
			'type'=>'error', 
			'title'=>'Error', 
			'text'=>$text
		);
	}
}

if ($_SUCCESSES) {
	foreach ($_SUCCESSES as $text) {
		$_JSON['notifications'][] = array(
			'type'=>'success', 
			'title'=>'Success', 
			'text'=>$text
		);
	}
}


header('Content-Type: text/json');
echo json_encode($_JSON);
