<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$_APPLICATION = $_REQUEST['application'];
$_PAGE = $_REQUEST['page'] ?: 1;
$_PAGE_ITEMS = $_REQUEST['items'] ?: 12;
$_OFFSET = ($_PAGE-1) * $_PAGE_ITEMS;

$_ARCHIVED = $_REQUEST['archived'];
$_SEARCH = $_REQUEST['search'];
$_YEAR = $_REQUEST['year'];
$_NUMBER = $_REQUEST['number'];

$model = new Model($_APPLICATION);
$_JSON = array();

$_PERMISSIONS = array(
	'edit' => user::permission('can_edit_newsletter'),
	'sent' => user::permission('can_sent_newsletter')
);

// filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_FILTERS = array();

if ($_SEARCH) {
	$_FILTERS['search'] = "(
		newsletter_title LIKE '%$_SEARCH%'
		OR newsletter_workflow_state_name LIKE '%$_SEARCH%'
		OR newsletter_number LIKE '%$_SEARCH%'
		OR YEAR(newsletter_publish_date) LIKE '%$_SEARCH%'
	)";
}

if ($_YEAR) {
	$_FILTERS['year'] = "YEAR(newsletter_publish_date) = '$_YEAR'";
}

if ($_NUMBER) {
	$_FILTERS['number'] = "newsletter_number = $_NUMBER";
}

$_FILTERS = $_FILTERS ? 'WHERE '.join(' AND ', array_values($_FILTERS)) : null;

$_LIMIT = $_FILTERS ? null : "LIMIT $_OFFSET, $_PAGE_ITEMS";

// load adata ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		newsletter_id AS id,
		IF(newsletter_number, CONCAT('#', newsletter_number),'') AS number,
		newsletter_title AS title,
		newsletter_text AS text,
		newsletter_publish_state_id AS state,
		newsletter_workflow_state_name AS state_name,
		LOWER(REPLACE(newsletter_workflow_state_name, ' ', '-')) AS filter,
		newsletter_image AS image,
		DATE_FORMAT(newsletter_publish_date, '%m/%d/%Y') AS published,
		UNIX_TIMESTAMP(newsletter_publish_date) AS published_stamp,
		DATE_FORMAT(newsletters.date_created, '%m/%d/%Y') AS created_date,
		UNIX_TIMESTAMP(newsletters.date_created) AS created_date_timestamp,
		CONCAT('/gazette/newsletters/composer/', newsletter_id) AS link
	FROM newsletters
	INNER JOIN newsletter_workflow_states ON newsletter_workflow_state_id = newsletter_publish_state_id
	$_FILTERS
	ORDER BY newsletters.date_created DESC
	$_LIMIT
");

$sth->execute();
$_JSON = $sth->fetchAll();

header('Content-Type: text/json');
echo json_encode($_JSON);
