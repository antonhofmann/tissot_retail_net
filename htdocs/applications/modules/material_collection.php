<?php

	/**
	 * Material Collection
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Material_Collection {
		
		/**
		 * Collection ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Collection Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Material ID
		 * @var integer
		 */
		public $material;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @var object
		 */
		protected $model;


		private $prefix;
		
		/**
		 * Edit all material collections
		 * @var string
		 */
		const PERMISSION_EDIT = 'can_edit_mps_collections';
		
		/**
		 * View all material collections
		 * @var string
		 */
		const PERMISSION_VIEW = 'can_view_mps_collections';
		
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Material_Collection_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_material_collection_$key"];
		}
		
		public function material($material) {
			$this->material = $material;
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_material_collection_id'];
			return $this->data;
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($application, $filters=null) {
			$model = new Material_Collection_Model($application);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
	}