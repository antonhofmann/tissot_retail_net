<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc'; 
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

$user = User::instance();
$translate = Translate::instance();
$settings = Settings::init();

ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 1800);
set_time_limit(1800);

$_ERRORS = array();
$_MESSAGES = array();
$_DATA = array();

$model = new Model($application);

$_SHEET_CAPTION = 'Available in Countries';
$_PAGE_TITLE = 'Available in Countries';


// request :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action']; 
$keyword = $_REQUEST['search'];
	
$_FILTERS = array();
$_FILTER_CAPTIONS = array();

// default filter
$_FILTERS['default'] = "item_id > 0";

// full text search
if ($keyword && $keyword<>$translate->search ) {
	
	$_FILTERS['search'] = "(
		item_code LIKE \"%$keyword%\" 
		OR item_name LIKE \"%$keyword%\" 
		OR item_description LIKE \"%$keyword%\"
		OR item_price LIKE \"%$keyword%\"
		OR supplier_item_price LIKE \"%$keyword%\"
		OR address_company LIKE \"%$keyword%\"
	)";

	$_FILTER_CAPTIONS[] = "Search \"$keyword\"";
}
// filter active
$active = isset($_REQUEST['active']) ? $_REQUEST['active'] : 1;

$_FILTERS['active'] = "item_active = $active";

$_FILTER_CAPTIONS[] = $active ? 'Active Items' : 'Inactive Items';

// filter: supplier
if ($_REQUEST['suppliers']) {
	
	$_FILTERS['suppliers'] = "supplier_address = ".$_REQUEST['suppliers'];

	$result = $model->query("
		SELECT address_company AS caption
		FROM db_retailnet.addresses
		WHERE address_id = {$_REQUEST[suppliers]}
	")->fetch();

	$_FILTER_CAPTIONS[] = $result['caption'];
}

// filter: item category
if ($_REQUEST['item_categories']) {
	
	$_FILTERS['item_categories'] = "item_category = ".$_REQUEST['item_categories'];

	$result = $model->query("
		SELECT item_category_name AS caption
		FROM db_retailnet.item_categories
		WHERE item_category_id = {$_REQUEST[item_categories]}
	")->fetch();

	$_FILTER_CAPTIONS[] = $result['caption'];
}

// filter: product lines
if ($_REQUEST['product_lines']) {

	$_FILTERS['product_lines'] = "product_line_supplying_group_line_id = ".$_REQUEST['product_lines'];

	$binds['item_supplying_groups'] = "INNER JOIN db_retailnet.item_supplying_groups ON item_supplying_group_item_id = item_id";
	$binds['supplying_groups'] = "INNER JOIN db_retailnet.supplying_groups ON supplying_group_id = item_supplying_group_supplying_group_id";
	$binds['product_line_supplying_groups'] = "INNER JOIN db_retailnet.product_line_supplying_groups ON product_line_supplying_group_group_id = supplying_group_id";

	$result = $model->query("
		SELECT product_line_name AS caption
		FROM db_retailnet.product_lines
		WHERE product_line_id = {$_REQUEST[product_lines]}
	")->fetch();

	$_FILTER_CAPTIONS[] = $result['caption'];
}

// filter: item types
if ($_REQUEST['item_types']) {

	$_FILTERS['item_types'] = "item_type = ".$_REQUEST['item_types'];

	$result = $model->query("
		SELECT item_type_name AS caption
		FROM db_retailnet.item_types
		WHERE item_type_id = {$_REQUEST[item_types]}
	")->fetch();

	$_FILTER_CAPTIONS[] = $result['caption'];
}

$_FILTERS = $_FILTERS ? 'WHERE '.join(' AND ', array_values($_FILTERS)) : null;
$_BIND = $binds ? join(' ', $binds) : null;


// items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("
	SELECT DISTINCT
		item_id,
		item_code,
		item_name,
		CONCAT(address_company, ', ', place_name) As company
	FROM db_retailnet.items	
	INNER JOIN db_retailnet.suppliers ON supplier_item = item_id
	INNER JOIN db_retailnet.addresses ON supplier_address = address_id
	INNER JOIN db_retailnet.places ON place_id = address_place_id
	INNER JOIN db_retailnet.item_countries ON item_country_item_id = item_id
	$_BIND
	$_FILTERS
	ORDER BY item_code
")->fetchAll();
$_TEST = $model->sql;
if (!$result) {
	$_ERRORS[] = "Items not found";
	goto BLOCK_RESPONSE;
}

$_ITEMS = _array::datagrid($result);

// get country :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("
	SELECT DISTINCT country_id, country_name
	FROM db_retailnet.items	
	INNER JOIN db_retailnet.item_countries ON item_country_item_id = item_id
	INNER JOIN db_retailnet.countries ON country_id = item_country_country_id
	INNER JOIN db_retailnet.suppliers ON supplier_item = item_id
	INNER JOIN db_retailnet.addresses ON supplier_address = address_id
	INNER JOIN db_retailnet.places ON place_id = address_place_id
	$_BIND
	$_FILTERS
	ORDER BY country_name
")->fetchAll();

if (!$result) {
	$_ERRORS[] = "Countries not found";
	goto BLOCK_RESPONSE;
}

$_COUNTRIES = _array::extract($result);

// datagrid :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_DATAGRID = array();

$result = $model->query("
	SELECT item_country_id, item_country_item_id, item_country_country_id
	FROM db_retailnet.items	
	INNER JOIN db_retailnet.item_countries ON item_country_item_id = item_id
	INNER JOIN db_retailnet.countries ON country_id = item_country_country_id
	INNER JOIN db_retailnet.suppliers ON supplier_item = item_id
	INNER JOIN db_retailnet.addresses ON supplier_address = address_id
	INNER JOIN db_retailnet.places ON place_id = address_place_id
	$_BIND
	$_FILTERS
")->fetchAll();

if ($result) {

	foreach ($result as $row) {
		$item = $row['item_country_item_id'];
		$country = $row['item_country_country_id'];
		$_DATAGRID[$item][$country] = $row['item_country_id'];
	}
}


// spreadsheet styles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_STYLES = array(
	'borders' => array(
		'borders' => array( 
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	'number' => array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
		)
	),
	'string' => array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
		)
	),
	'page-title' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 18, 
			'bold'=>true,
			'color' => array('rgb'=>'000000')
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		)
	),
	'header' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 11,
			'color' => array('rgb'=>'000000')
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_BOTTOM,
			'wrap'=> true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'ededed')
		)
	),
	'group' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12, 
			'bold'=>true,
			'color' => array('rgb'=>'ffffff')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'404040')
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		)
	),
	'quantity-label' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 11,
			'bold'=>true,
			'color' => array('rgb'=>'000000')
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		)
	),
	'item' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 11,
			'bold'=>true,
			'color' => array('rgb'=>'000000')
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'dedede')
		)
	),
	'pos' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 11,
			'color' => array('rgb'=>'000000')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'ededed')
		)
	)
);

// php excel :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$phpExcel = new PHPExcel();
$phpExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name)->setTitle($_SHEET_CAPTION);
$phpExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$phpExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

$_FIRST_ROW = 1;
$_FIRST_COL = 0;
$_TOTAL_FIXED_COLUMNS = 3;
$_TOTAL_COLUMNS = $_TOTAL_FIXED_COLUMNS + count($_COUNTRIES)-1;

// page header :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row = $_FIRST_ROW;
$col = $_FIRST_COL;

$column = PHPExcel_Cell::stringFromColumnIndex($col);
$phpExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(50);

// page title
$phpExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $_TOTAL_COLUMNS, $row);
$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $_PAGE_TITLE);
$phpExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['page-title']);

// row filters
$row++;
$col = $_FIRST_COL;
$caption = $_FILTER_CAPTIONS ? 'Filter(s): '.join(', ', array_filter($_FILTER_CAPTIONS)) : null;
$phpExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $_TOTAL_COLUMNS, $row);
$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $caption);

// sheet header ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++;
$col = $_FIRST_COL;
$_ROW_HEADER = $row;

$phpExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(140);

// colum item code
$colName = PHPExcel_Cell::stringFromColumnIndex($col);
$phpExcel->getActiveSheet()->getColumnDimension($colName)->setWidth(25);
$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $translate->item_code);

// colum item text
$col++;
$colName = PHPExcel_Cell::stringFromColumnIndex($col);
$phpExcel->getActiveSheet()->getColumnDimension($colName)->setWidth(40);
$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $translate->item_name);

// colum item supplier
$col++;
$colName = PHPExcel_Cell::stringFromColumnIndex($col);
$phpExcel->getActiveSheet()->getColumnDimension($colName)->setWidth(60);
$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $translate->address_company);

foreach ($_COUNTRIES as $country => $name) {
	
	$col++;

	$colName = PHPExcel_Cell::stringFromColumnIndex($col);
	$phpExcel->getActiveSheet()->getColumnDimension($colName)->setWidth(5);

	$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $name);
	$phpExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setTextRotation(90);
}

// styling: categories
$startCel = PHPExcel_Cell::stringFromColumnIndex($_FIRST_COL).$row;
$endCel = PHPExcel_Cell::stringFromColumnIndex($_TOTAL_COLUMNS).$row;
$phpExcel->getActiveSheet()->getStyle("$startCel:$endCel")->applyFromArray($_STYLES['header'], false);

$startCel = PHPExcel_Cell::stringFromColumnIndex($_FIRST_COL).$row;
$endCel = PHPExcel_Cell::stringFromColumnIndex($_TOTAL_FIXED_COLUMNS).$row;
$phpExcel->getActiveSheet()->getStyle("$startCel:$endCel")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


// grid ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

foreach ($_ITEMS as $i => $item) {
	
	$row++;

	// item code
	$col = $_FIRST_COL;
	$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $item['item_code']);

	// item name
	$col++;
	$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $item['item_name']);

	// item name
	$col++;
	$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $item['company']);

	foreach ($_COUNTRIES as $country => $name) {
		
		$col++;
		
		if ($_DATAGRID[$i][$country]) {
			$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, 'x');
		}
	}
}

// styling: border
$startCel = PHPExcel_Cell::stringFromColumnIndex($_FIRST_COL).$_ROW_HEADER;
$endCel = PHPExcel_Cell::stringFromColumnIndex($_TOTAL_COLUMNS).$row;
$phpExcel->getActiveSheet()->getStyle("$startCel:$endCel")->applyFromArray($_STYLES['borders'], false);

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONSE:

ob_end_clean();

if (!$_ERRORS) {

	$stamp = $_SERVER['REQUEST_TIME'];
	$file = "/data/tmp/items.$stamp.xlsx";
	$hash = Encryption::url($file);
	$url = "/download/tmp/$hash";
	
	// set active sheet
	$phpExcel->getActiveSheet()->setTitle('Available in Countries');
	$phpExcel->setActiveSheetIndex(0);
	$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
	$objWriter->save($_SERVER['DOCUMENT_ROOT'].$file);
}

header('Content-Type: text/json');

echo json_encode(array(
	'success' => file_exists($_SERVER['DOCUMENT_ROOT'].$file) ? true : false,
	'errors' => $_ERRORS,
	'file' => $url,
	'test' => $_TEST
));