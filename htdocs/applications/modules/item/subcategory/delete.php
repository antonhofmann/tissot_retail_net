<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$appliction = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$category = $_REQUEST['category'];
$id = $_REQUEST['id'];

$permission_edit = user::permission('can_edit_catalog');
$permission_edit_items = user::permission('can_edit_items_in_catalogue');
$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

if ($id && $_CAN_EDIT) {
	
	$itemSubcategory = new Item_SubCategory();
	$itemSubcategory->read($id);
	
	$delete = $itemSubcategory->delete();
	
	if ($delete) {
		Message::request_deleted();
		url::redirect("/$appliction/$controller/subcategories/$category");
	} else {
		Message::request_failure();
		url::redirect("/$appliction/$controller/subcategories/$category/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$appliction/$controller");
}