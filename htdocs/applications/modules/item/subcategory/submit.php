<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

// disabled fields
$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

// url request
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['item_subcategory_id'];

$data = array();

if (in_array('item_subcategory_category_id', $fields)) {
	$data['item_subcategory_category_id'] = $_REQUEST['item_subcategory_category_id'];
}

if (in_array('item_subcategory_name', $fields)) {
	$data['item_subcategory_name'] = $_REQUEST['item_subcategory_name'];
}

if (in_array('item_subcategory_spare_part', $fields)) {
	$data['item_subcategory_spare_part'] = $_REQUEST['item_subcategory_spare_part'] ? 1 : 0;
}


if (in_array('item_subcategory_active', $fields)) {
	$data['item_subcategory_active'] = $_REQUEST['item_subcategory_active'] ? 1 : 0;
}

if ($data) {

	$itemSubcategory = new Item_Subcategory();
	$itemSubcategory->read($id);

	if ($itemSubcategory->id) {

		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		
		$response = $itemSubcategory->update($data);

		$message = $response ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		
		$data['user_created'] = $user->login;
		
		$response = $id = $itemSubcategory->create($data);
		
		$message = $response ? Message::request_inserted() : $translate->message_request_failure;
		
		$redirect = $_REQUEST['redirect'] ? $_REQUEST['redirect'].'/'.$id : null;
	}
}
else {
	
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect
));
