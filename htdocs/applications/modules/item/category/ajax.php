<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$response = array();

switch ($_REQUEST['section']) {

	case 'get.subcategories':

		$category = $_REQUEST['value'];
		$current = $_REQUEST['current'];

		$response[][''] = $translate->select;
			
		if ($category) {
			
			$model = new Model(Connector::DB_CORE);
			
			$result = $model->query("
				SELECT DISTINCT 
					item_subcategory_id, 
					item_subcategory_name
				FROM item_subcategories
				WHERE item_subcategory_category_id = $category AND (item_subcategory_active=1 OR item_subcategory_id='$current')
				ORDER BY item_subcategory_name
			")->fetchAll();
			
			if ($result) {
				foreach ($result as $row) {
					$response[][$row['item_subcategory_id']] = $row['item_subcategory_name'];
				}
			}
		}

	break;
}

// response with json header
header('Content-Type: text/json');
echo json_encode($response);

