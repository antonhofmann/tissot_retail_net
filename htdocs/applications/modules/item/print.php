<?php
/**
 * Excel Copmanies List
 * 
 * Created by:     	Admir Serifi (admir.serifi@mediaparx.com)
 * Date created:   	2012-02-01
 * Version:        	1.1
 * Updated: 		2012-10-19, Roger Bucher
 * 
 * Copyright (c) 2008, Swatch AG, All Rights Reserved.
 */

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

$model = new Model(Connector::DB_CORE);

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];

// request execution time
ini_set('max_execution_time', 120);

// request
//$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

$title = $application=='catalog' ? 'Item List' : 'Furniture';
$fileName = "furniture.".date('Y.m.d');

define('SHEET_NAME', $title);
define('SHEET_FILE_NAME', $fileName);
define('SHEET_VERSION', "Excel2007");

// sort list
$sort = $application=='mps' ? "item_category_name, item_code, item_name" : "item_code, item_name";
$order = $_REQUEST['sort'] ?: $sort; 
$direction = $_REQUEST['direction'];

// list fields :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_FIELDS = array();

if ($_REQUEST['columns']) {

	$_FIELDS = array_keys($_REQUEST['columns']);		

	$sth = $model->db->exec("
		DELETE FROM user_preferences
		WHERE user_preference_user_id = $user->id 
		AND user_preference_entity = 'items.print' 
		AND user_preference_name = 'fields.selected'
	");

	$sth = $model->db->prepare("
		INSERT INTO user_preferences (
			user_preference_user_id,
			user_preference_entity,
			user_preference_name,
			user_preference_value
		) 
		VALUES (?, 'items.print', 'fields.selected', ?)
	");

	$sth->execute(array($user->id, serialize($_FIELDS)));

}  
elseif ($table->export_fields)  {
	$table = new DB_Table();
	$table->read_from_name('items');
	$_FIELDS = unserialize($table->export_fields);
}

if (!$_FIELDS) {
	$_ERRORS[] = "No item fields selected";
	goto BLOCK_RESPONSE;
}

// extend price with currency symbole
if (in_array('supplier_item_price', $_FIELDS)) {
	$key = array_search('supplier_item_price', $_FIELDS);
	$ar1 = array_slice($_FIELDS, 0, $key+1, true);
	$ar2 = array_slice($_FIELDS, $key+1, count($_FIELDS)-1, true);
	$_FIELDS = array_merge($ar1, array("supplier_item_currency"), $ar2);
}

// column properties :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PROPERTIES = array(
	'item_id' => array(
		'width' => 5,
		'caption' => 'ID'
	),
	'item_code' => array(
		'width' => 20,
		'caption' => $translate->item_code
	),
	'item_name' => array(
		'width' => 60,
		'caption' => $translate->item_name
	),
	'item_type' => array(
		'width' => 20,
		'caption' => $translate->item_type,
		'field' => 'item_type_name',
		'table' => 'item_types',
		'bind' => 'LEFT JOIN item_types ON item_type_id = item_type'
	),
	'item_category' => array(
		'width' => 20,
		'caption' => $translate->item_category,
		'field' => 'item_category_name',
		'table' => 'item_categories',
		'bind' => 'LEFT JOIN item_categories ON item_category_id = item_category'
	),
	'item_subcategory' => array(
		'width' => 20,
		'caption' => $translate->item_subcategory,
		'field' => 'item_subcategory_name',
		'table' => 'item_subcategories',
		'bind' => 'LEFT JOIN item_subcategories ON item_subcategory_id = item_subcategory'
	),
	'item_price' => array(
		'width' => 15,
		'caption' => 'Sales Price CHF'
	),
	'item_description' => array(
		'width' => 30,
		'caption' => $translate->item_description
	),
	'item_watches_displayed' => array(
		'width' => 15,
		'caption' => $translate->item_watches_displayed
	),
	'item_watches_stored' => array(
		'width' => 15,
		'caption' => $translate->item_watches_stored
	),
	'item_packed_size' => array(
		'width' => 20,
		'caption' => $translate->item_packed_size
	),
	'item_packed_weight' => array(
		'width' => 20,
		'caption' => $translate->item_packed_weight
	),
	'item_materials' => array(
		'width' => 30,
		'caption' => $translate->item_materials,
		'field' => "
			(
				SELECT GROUP_CONCAT(item_material_name SEPARATOR ', ')
				FROM item_materials
				WHERE item_material_item_id = items.item_id
			) AS item_materials
		"
	),
	'item_electrical_specifications' => array(
		'width' => 30,
		'caption' => $translate->item_electrical_specifications,
		'field' => "
			(
				SELECT GROUP_CONCAT(item_electricspec_name SEPARATOR ', ')
				FROM item_electricspecs
				WHERE item_electricspec_item_id = items.item_id
			) AS item_electrical_specifications
		"
	),
	'item_lights_used' => array(
		'width' => 20,
		'caption' => $translate->item_lights_used
	),
	'item_regulatory_approvals' => array(
		'width' => 20,
		'caption' => $translate->item_regulatory_approvals
	),
	'item_install_requirements' => array(
		'width' => 20,
		'caption' => $translate->item_install_requirements
	),
	'item_install_requirements' => array(
		'width' => 30,
		'caption' => $translate->item_install_requirements
	),
	'item_visible' => array(
		'width' => 10,
		'caption' => $translate->item_visible,
		'field' => "IF (item_visible, 'Yes', 'No') AS item_visible"
	),
	'item_price_adjustable' => array(
		'width' => 12,
		'caption' => $translate->item_price_adjustable,
		'field' => "IF (item_price_adjustable, 'Yes', 'No') AS item_price_adjustable"
	),
	'item_active' => array(
		'width' => 10,
		'caption' => $translate->item_active,
		'field' => "IF (item_active, 'Yes', 'No') AS item_active"
	),
	'item_cost_group' => array(
		'width' => 20,
		'caption' => $translate->item_cost_group,
		'field' => "project_cost_groupname_name",
		'table' => 'project_cost_groupnames',
		'bind' => 'LEFT JOIN project_cost_groupnames ON project_cost_groupname_id = item_cost_group'
	),
	'item_visible_in_production_order' => array(
		'width' => 10,
		'caption' => 'POP',
		'field' => "IF (item_visible_in_production_order, 'Yes', 'No') AS item_visible_in_production_order",
		'shortcut' => $translate->item_visible_in_production_order
	),
	'item_visible_in_mps' => array(
		'width' => 10,
		'caption' => 'Visible in MPS',
		'field' => "IF (item_visible_in_mps, 'Yes', 'No') AS item_visible_in_mps"
	),
	'item_addable_in_mps' => array(
		'width' => 10,
		'caption' => 'Addable in MPS',
		'field' => "IF (item_addable_in_mps, 'Yes', 'No') AS item_addable_in_mps"
	),
	'item_width' => array(
		'width' => 12,
		'caption' => $translate->item_width
	),
	'item_height' => array(
		'width' => 12,
		'caption' => $translate->item_height
	),
	'item_length' => array(
		'width' => 12,
		'caption' => $translate->item_length
	),
	'item_radius' => array(
		'width' => 12,
		'caption' => $translate->item_radius
	),
	'item_gross_weight' => array(
		'width' => 12,
		'caption' => $translate->item_gross_weight
	),
	'item_net_weight' => array(
		'width' => 12,
		'caption' => $translate->item_net_weight
	),
	'item_unit' => array(
		'width' => 15,
		'caption' => $translate->item_unit,
		'field' => 'unit_name',
		'table' => 'units',
		'bind' => 'LEFT JOIN units ON unit_id = item_unit'
	),
	'item_packaging_type' => array(
		'width' => 70,
		'caption' => $translate->item_packaging_type,
		'field' => 'packaging_type_name',
		'table' => 'packaging_types',
		'bind' => 'LEFT JOIN packaging_types ON item_packaging_type = packaging_type_id'
	),
	'item_stackable' => array(
		'width' => 10,
		'caption' => $translate->item_stackable,
		'field' => "IF (item_stackable, 'Yes', 'No') AS item_stackable"
	),
	'item_stock_property_of_swatch' => array(
		'width' => 12,
		'caption' => 'is Swatch',
		'field' => "IF (item_stock_property_of_swatch, 'Yes', 'No') AS item_stock_property_of_swatch",
		'shortcut' => $translate->item_stock_property_of_swatch
	),
	'item_is_dr_swatch_furniture' => array(
		'width' => 12,
		'caption' => 'Dr. Swatch',
		'field' => "IF (item_is_dr_swatch_furniture, 'Yes', 'No') AS item_is_dr_swatch_furniture",
		'shortcut' => $translate->item_is_dr_swatch_furniture
	),
	'item_suppleir' => array(
		'width' => 30,
		'caption' => $translate->item_suppleir
	),
	'supplier_item_price' => array(
		'width' => 15,
		'caption' => $translate->supplier_item_price
	),
	'packaging_type_name' => array(
		'width' => 70,
		'caption' => $translate->packaging_type_name
	),
	'supplying_groups' => array(
		'width' => 30,
		'caption' => $translate->supplying_groups,
		'table' => 'supplying_groups',
		'field' => "
			(
				SELECT GROUP_CONCAT(supplying_group_name SEPARATOR ', ')
				FROM supplying_groups
				INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = supplying_group_id
				WHERE item_supplying_group_item_id = items.item_id
			) AS supplying_groups
		"
	),
	'supplier_address' => array(
		'width' => 30,
		'caption' => 'Supplier',
		'field' => 'address_company AS supplier_address',
		'table' => 'supplier_address',
		'bind' => 'LEFT JOIN addresses ON address_id = supplier_address'
	),
	'supplier_item_currency' => array(
		'width' => 10,
		'caption' => 'Currency',
		'field' => 'currency_symbol',
		'table' => 'currencies',
		'bind' => 'LEFT JOIN currencies ON currency_id = supplier_item_currency'
	)
);

// query builder::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_BINDS = array();
$_BINDS['suppliers'] = "LEFT JOIN suppliers ON supplier_item = item_id";

$_FILTERS = array();

// default filter
$_FILTERS['default'] = "item_id > 0";

// item type
$_FILTERS['type'] = $_REQUEST['type'] ? "item_type = ".$_REQUEST['type'] : "item_type IN (1, 2, 3, 7)";

// filter active
$active = isset($_REQUEST['active']) ? $_REQUEST['active'] : 1;
$_FILTERS['active'] = "item_active = $active";

// mps filter
if ($application == 'mps') {
	$_FILTERS['mps'] = "item_visible_in_mps = 1";
}
// filter: full text search
if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search ) {
	
	$keyword = $_REQUEST['search'];

	if ($application=='catalog') {
		
		$_FILTERS['search'] = "(
			item_code LIKE '%$keyword%' 
			OR item_name LIKE '%$keyword%' 
			OR item_description LIKE '%$keyword%'
			OR item_price LIKE '%$keyword%'
			OR supplier_item_price LIKE '%$keyword%'
			OR unit_name LIKE '%$keyword%'
			OR address_company LIKE '%$keyword%'
		)";

		$_BINDS['addresses'] = "LEFT JOIN db_retailnet.addresses ON address_id = supplier_address";
	} 
	else {
		$_FILTERS['search'] = "(
			item_code LIKE '%$keyword%' 
			OR item_name LIKE '%$keyword%' 
			OR item_description LIKE '%$keyword%'
			OR item_category_name LIKE '%$keyword%'
		)";
	}

	$captions[] = $translate->search.": $keyword";
}

// filter: supplier
if ($_REQUEST['suppliers']) {
	$_FILTERS['suppliers'] = "supplier_address = ".$_REQUEST['suppliers'];
	$company = new Company();
	$company->read($_REQUEST['suppliers']);
	$captions[] = "Supplier: $company->company";
}

// filter: item category
if ($_REQUEST['item_categories']) {
	$_FILTERS['item_categories'] = "item_category_id = ".$_REQUEST['item_categories'];
	$_BINDS['item_categories'] = "INNER JOIN item_categories ON item_category = item_category_id";
	$itemCategory = new Item_Category();
	$itemCategory->read($_REQUEST['item_categories']);
	$captions[] = "Item Category: $itemCategory->name";
}

// filter: item subcategory
if ($_REQUEST['item_subcategories']) {
	$_FILTERS['item_subcategories'] = "item_subcategory_id = ".$_REQUEST['item_subcategories'];
	$_BINDS['item_subcategories'] = "INNER JOIN item_subcategories ON item_subcategory = item_subcategory_id";
	$res = $model->query("SELECT item_subcategory_name FROM item_subcategories WHERE item_subcategory_id = ".$_REQUEST['item_subcategories'] )->fetch();
	$captions[] = "Item Subcategory: ".$res['item_subcategory_name'];
}

// filter: product lines
if ($_REQUEST['product_lines']) {
	
	$_FILTERS['product_lines'] = "product_line_supplying_group_line_id = ".$_REQUEST['product_lines'];

	$_BINDS['item_supplying_groups'] = "INNER JOIN db_retailnet.item_supplying_groups ON item_supplying_group_item_id = item_id";
	$_BINDS['supplying_groups'] = "INNER JOIN db_retailnet.supplying_groups ON supplying_group_id = item_supplying_group_supplying_group_id";
	$_BINDS['product_line_supplying_groups'] = "INNER JOIN db_retailnet.product_line_supplying_groups ON product_line_supplying_group_group_id = supplying_group_id";

	$productLine = new Product_Line();
	$productLine->read($_REQUEST['product_lines']);
	$captions[] = "Product Line: $productLine->name";
}

// filter: item types
if ($_REQUEST['item_types']) {
	$_FILTERS['item_types'] = "item_type = ".$_REQUEST['item_types'];
	$itemType = new Item_Type();
	$itemType->read($_REQUEST['item_types']);
	$captions[] = "Item Type: $itemType->name";
}

$queryFields = array();
$queryFields[] = 'item_id';

foreach ($_FIELDS as $col) {
	
	$properties = $_PROPERTIES[$col] ?: array();

	$queryFields[] = $properties['field'] ?: $col;

	if ($properties['bind']) {
		$table = $properties['table'] ?: $col;
		$_BINDS[$table] = $properties['bind'];
	}
}

$sql = array();
$sql[] = "SELECT DISTINCT";
$sql[] = join(', ', $queryFields);
$sql[] = "FROM items";
$sql[] = $_BINDS ? join('  ', array_values($_BINDS)) : null;
$sql[] = $_FILTERS ? 'WHERE '.join(' AND ', array_values($_FILTERS)) : null;
$sql[] = "ORDER BY $order $direction";
$sql = join(' ', array_filter($sql));




$result = $model->query($sql)->fetchAll();
//die($model->sql);
if (!$result) {
	$_ERRORS[] = "Items not found";
	goto BLOCK_RESPONSE;
}

$datagrid = _array::datagrid($result);


//replace item packaging type by logistics information
if(strpos($sql, 'packaging_type_name') != false)
{
	foreach ($datagrid as $i => $array) {
		$tmp = '';

		$sql_p = "select *, " . 
			     " ROUND((item_packaging_number*(item_packaging_length*item_packaging_width*item_packaging_height/1000000)), 2) as cbm  " .
		         " from item_packaging " . 
			     " left join units on unit_id = item_packaging_unit_id " . 
			     " left join packaging_types on packaging_type_id = item_packaging_packaging_id " . 
			     " where item_packaging_item_id = " . $i;

		$result_p = $model->query($sql_p)->fetchAll();
		$count = count($result_p);
		$k = 1;

		foreach($result_p as $key=>$data) {
			
			$tmp .= $data["item_packaging_number"] .  " " . $data["unit_name"] . ", " . $data["packaging_type_name"] . ", " . $data["item_packaging_length"] . "x" . $data["item_packaging_width"] . "x" . $data["item_packaging_height"] . ", " . $data["cbm"] . "cbm, " . $data["item_packaging_weight_net"] . "kg/" . $data["item_packaging_weight_gross"] . "kg";

			if($k < $count) {$tmp .= "\n";}
			$k++;
		
		}

		$datagrid[$i]['packaging_type_name'] = $tmp;

	}
}


// php excel :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle(SHEET_NAME);
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

// styling
$styles['border'] = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_TOP,
		'wrap'=> true
	),
);

$styles['header'] = array(
	'font' => array(
		'name'=>'Arial',
		'size'=> 11, 
		'bold'=>false,
		'color' => array('rgb'=>'ffffff')
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
		'wrap'=> true
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb'=>'444444')
	)
);

$totalColumns = count($_FIELDS);
$lastColumn = PHPExcel_Cell::stringFromColumnIndex($totalColumns-1);

// sheet header
$row = 1;
$range = "A$row:$lastColumn$row";
$objPHPExcel->getActiveSheet()->mergeCells($range);
$objPHPExcel->getActiveSheet()->setCellValue('A1', SHEET_NAME);
$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setSize(18);

$row++;
$range = "A$row:$lastColumn$row";
$objPHPExcel->getActiveSheet()->mergeCells($range);
$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: ".date('d.m.Y  H:i:s'));

// filter captions
if ($captions) {
	$row++;
	$range = "A$row:$lastColumn$row";
	$objPHPExcel->getActiveSheet()->mergeCells($range);
	$captions = join(', ', $captions);
	$objPHPExcel->getActiveSheet()->setCellValue("A$row", $captions);
	$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
}

// separator
$row++;
$range = "A$row:$lastColumn$row";
$objPHPExcel->getActiveSheet()->mergeCells($range);

// column captions
$row++;
$col = 0;
$startBorderedCell = "A$row";

$_SHORTCUTS = array();

foreach ($_FIELDS as $column) {

	$index = spreadsheet::index($col);
	$prop = $_PROPERTIES[$column] ?: array();
	$caption = $prop['caption'] ?: $translate->$column;
	$width = $prop['width'] ?: 20;

	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $caption);
	$objPHPExcel->getActiveSheet()->getColumnDimension($index)->setWidth($prop['width']);
	$objPHPExcel->getActiveSheet()->getStyle($index.$row)->applyFromArray($styles['header']);

	if ($prop['shortcut']) {

		$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray(array('font' => array('color' => array('rgb'=>'fff000'))));
		
		$_SHORTCUTS[] = array(
			'name' => $caption,
			'description' => $prop['shortcut']
		);
	}

	$col++;
}

// table header styling
$range = "A$row:$lastColumn$row";
$objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setBold(true);

foreach ($datagrid as $i => $array) {

	$row++;
	$col = 0;
	
	foreach($array as $key=>$value) {
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
		$col++;
	}
	
	
}

// set borders
$stopBorderCell = "$lastColumn$row";
$objPHPExcel->getActiveSheet()->getStyle("$startBorderedCell:$stopBorderCell")->applyFromArray($styles['border'], false);

if ($_SHORTCUTS) {

	$row++;
	$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");

	$row++;
	$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, "Shortname descriptions");
	$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);

	foreach ($_SHORTCUTS as $shortcut) {
		$row++;
		$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $shortcut['name'].', '.$shortcut['description']);
	}
}

// export
$objPHPExcel->getActiveSheet()->setTitle(SHEET_NAME);
$objPHPExcel->setActiveSheetIndex(0);
	

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONSE:

ob_end_clean();

if (!$_ERRORS) {

	$stamp = $_SERVER['REQUEST_TIME'];
	$file = "/data/tmp/".SHEET_FILE_NAME.".$stamp.xlsx";
	$hash = Encryption::url($file);
	$url = "/download/tmp/$hash";
	
	// set active sheet
	$objPHPExcel->getActiveSheet()->setTitle(SHEET_NAME);
	$objPHPExcel->setActiveSheetIndex(0);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save($_SERVER['DOCUMENT_ROOT'].$file);
}

header('Content-Type: text/json');

echo json_encode(array(
	'success' => file_exists($_SERVER['DOCUMENT_ROOT'].$file) ? true : false,
	'errors' => $_ERRORS,
	'file' => $url,
	'fields' => $_FIELDS
));