<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once $_SERVER['DOCUMENT_ROOT']."/vendor/tecnickcom/tcpdf/tcpdf.php";
require_once $_SERVER['DOCUMENT_ROOT']."/vendor/setasign/fpdi/fpdi.php";
require_once  $_SERVER['DOCUMENT_ROOT'].'/include/SetaPDF/Autoload.php';

// request execution time
ini_set('max_execution_time', 240);
ini_set('memory_limit', '2048M');

$user = User::instance();
$translate = Translate::instance();
$db = Connector::get();

$_ITEMS = isset($_REQUEST['items']) ? array_keys($_REQUEST['items']) : array($_REQUEST['id']);
$_IS_MULTIPLE_ITEMS = count($_ITEMS)==1 ? false : true;
$_TABLE_ROW_BORDER = 'TB';

$_SECTION_COVER = $_REQUEST['sections']['cover'];
$_SECTION_PROPERTIES = $_REQUEST['sections']['properties'];
$_SECTION_TECHNICAL = $_REQUEST['sections']['technical'];
$_SECTION_LOGISTIC = $_REQUEST['sections']['logistic'];
$_SECTION_REMARKS = $_REQUEST['sections']['remarks'];
$_SECTION_PACKING = $_REQUEST['sections']['packing'];
$_SECTION_SPARE_PARTS = $_REQUEST['sections']['spareparts'];
$_SECTION_SPARE_PARTS = $_REQUEST['sections']['spareparts'];
$_SECTION_FILES = $_REQUEST['sections']['files'];
$_SECTION_CERTIFICATES = $_REQUEST['sections']['certificates'];
$_SECTION_ITEMINFORMATION = $_REQUEST['sections']['iteminformation'];


// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!Request::isAjax() || !$user->id || !$_ITEMS || !$_REQUEST['sections']) {
	$_ERRORS[] = "Bed request";
	goto BLOCK_RESPONSE;
}

// temporary location
$_DIR_TMP = "/data/tmp/$user->id";
Dir::make($_DIR_TMP);
Dir::emptyMe($_DIR_TMP."/*");

// builders ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// get item units
$sth = $db->prepare("
	SELECT unit_id, unit_name 
	FROM units 
	ORDER BY unit_name
");

$sth->execute();
$result = $sth->fetchAll();
$_UNITS = _array::extract($result);

// get packaging_types
$sth = $db->prepare("
	SELECT packaging_type_id, packaging_type_name 
	FROM packaging_types 
	ORDER BY packaging_type_name
");

$sth->execute();
$result = $sth->fetchAll();
$_PACKAGING_TYPES = _array::extract($result);

// get cost groups
$sth = $db->prepare("
	SELECT project_cost_groupname_id, project_cost_groupname_name 
	FROM project_cost_groupnames
	ORDER BY project_cost_groupname_name
");

$sth->execute();
$result = $sth->fetchAll();
$_COST_GROUPS = _array::extract($result);

// get cost groups
$sth = $db->prepare("
	SELECT costmonitoringgroup_id, costmonitoringgroup_text 
	FROM costmonitoringgroups 
	ORDER BY costmonitoringgroup_text
");

$sth->execute();
$result = $sth->fetchAll();
$_COST_MONITORING_GROUPS = _array::extract($result);

// get item
$getItem = $db->prepare("
	SELECT *
	FROM items
	WHERE item_id = ?
");

// get item electrical specifications
$getSupplier = $db->prepare("
	SELECT CONCAT(address_company, ', ', country_name) AS company, supplier_item_price, currency_symbol
	FROM suppliers 
	INNER JOIN addresses ON supplier_address = address_id
	INNER JOIN countries ON country_id = address_country
	INNER JOIN currencies on currency_id = address_currency 
	WHERE supplier_item = ?
	LIMIT 1
");

if ($_SECTION_PROPERTIES) {

	// get certificate materials
	$getCertificateMaterials = $db->prepare("
		SELECT DISTINCT catalog_material_id, catalog_material_name
		FROM catalog_materials 
		INNER JOIN item_catalog_materials ON item_catalog_material_material_id = catalog_material_id
		WHERE item_catalog_material_item_id = ?
		ORDER BY catalog_material_name
	");
	
	// get item materials
	$getMaterials = $db->prepare("
		SELECT item_material_name
		FROM item_materials
		WHERE item_material_item_id = ?
	");

	// get item electrical specifications
	$getElectricalSpecifications = $db->prepare("
		SELECT item_electricspec_name, item_electricspec_description
		FROM item_electricspecs
		WHERE item_electricspec_item_id = ?
	");


	// get available product lines
	$getAvailableProductLines = $db->prepare("
		SELECT DISTINCT product_line_name 
		FROM product_lines
	    INNER JOIN product_line_supplying_groups ON product_line_supplying_group_line_id = product_line_id
	    INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = product_line_supplying_group_group_id
		INNER JOIN items ON item_id = item_supplying_group_item_id
		INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id 
		INNER JOIN productline_regions ON productline_region_productline = product_line_id
		INNER JOIN item_categories ON item_category_id = item_category
		INNER JOIN item_countries ON item_country_item_id = item_id 
		LEFT join item_pos_types on item_pos_type_item = item_id 
		WHERE product_line_active = 1 and item_id = ?");
}


if ($_SECTION_PACKING) {
	
	// get item packaging informations
	$getPackagingInformations = $db->prepare("
		SELECT 
			item_packaging_id AS id, 
			item_packaging_number AS number,
			item_packaging_unit_id AS unit,
			item_packaging_packaging_id AS packaging,
			item_packaging_width AS width,
			item_packaging_height AS height,
			item_packaging_length AS length,
			item_packaging_weight_net AS weight_net,
			item_packaging_weight_gross AS weight_gross
		FROM item_packaging
		WHERE item_packaging_item_id = ?
	");
}

if ($_SECTION_SPARE_PARTS) {

	// get item spare parts
	$getSpareParts = $db->prepare("
		SELECT DISTINCT item_id, item_code, item_name
		FROM parts
		INNER JOIN items ON item_id = part_child
		WHERE part_parent = ?
	");
}

if ($_SECTION_COVER || $_SECTION_TECHNICAL || $_SECTION_FILES) {
	
	// get item files
	$getFiles = $db->prepare("
		SELECT DISTINCT
			item_file_id,
			item_file_title,
			item_file_description,
			item_file_path,
			file_type_name,
			file_type_id,
			file_purpose_id,
			file_purpose_name,
			DATE_FORMAT(item_files.date_created, '%d.%m.%Y') AS date
		FROM item_files
		LEFT JOIN file_types ON item_file_type = file_type_id
		LEFT JOIN file_purposes ON file_purpose_id = item_file_purpose
		WHERE item_file_item = ?
		ORDER BY date, item_file_title
	");
}

if ($_SECTION_CERTIFICATES) {
	
	// get certificates
	$getCertificates = $db->prepare("
		SELECT DISTINCT 
			certificate_files.certificate_file_file, 
			certificate_files.certificate_file_certificate_id,
			certificate_titles.certificate_title_title,
			certificate_titles.certificate_title_description,
			DATE_FORMAT(certificate_file_expiry_date, '%d.%m.%Y') AS certificate_file_expiry_date,
			address_company,
			address_address,
			address_zip,
			place_name,
			country_name
		FROM certificates 
		INNER JOIN certificate_files ON certificate_files.certificate_file_certificate_id = certificates.certificate_id
		INNER JOIN item_certificates ON certificates.certificate_id = item_certificates.item_certificate_certificate_id
		LEFT JOIN certificate_titles ON certificate_titles.certificate_title_id = certificates.certificate_title_id
		LEFT JOIN addresses ON address_id = certificate_address_id
		LEFT JOIN countries ON country_id = address_country
		LEFT JOIN places ON place_id = address_place_id
		WHERE item_certificates.item_certificate_item_id = ? AND certificate_file_expiry_date >= CURRENT_DATE
		ORDER BY certificate_file_expiry_date DESC
	");
}



if ($_SECTION_ITEMINFORMATION) {
	
	// get item information
	$getIteminformation = $db->prepare("
		SELECT DISTINCT 
			item_information_id, item_information_name, item_information_file,
			item_information_title_title, 
			DATE_FORMAT(item_information_version_date, '%d.%m.%Y') AS item_information_version_date,
			address_company,
			address_address,
			address_zip,
			place_name,
			country_name
		FROM item_informations 
		INNER JOIN item_information_items ON item_information_item_item_information_id = item_information_id
		LEFT JOIN item_information_titles ON item_information_titles.item_information_title_id = item_informations.item_information_title_id
		LEFT JOIN addresses ON address_id = item_information_address_id
		LEFT JOIN countries ON country_id = address_country
		LEFT JOIN places ON place_id = address_place_id
		WHERE item_information_item_item_id = ? 
		ORDER BY item_information_version_date DESC
	");
}


class MYPDF extends TCPDF {

	protected $footerCaption;

	protected $customStartPageNumber;

	public function getStartPageNumber() {
        return $this->customStartPageNumber;
    }

	public function setStartPageNumber($pageNumber) {
        $this->customStartPageNumber = $pageNumber;
    }

	public function setFooterCaption($caption) {
        $this->footerCaption = $caption;
    }
			
	public function Header() {
		
		$file = $_SERVER['DOCUMENT_ROOT'].'/public/images/logo.jpg';
		$x=170; $y=10; $width=30;
		$this->Image($file, $x, $y, $width);
	}

	public function Footer() {

		
		$this->SetY(-14);
	
		// footer left caption
		$this->SetFont('arialn', 'N', 9);
		$this->Cell(60, 8, $this->footerCaption, 0, 0, 'L');
		
		// page number
		$this->SetFont('arialn', 'N', 9);
		$pageNumber = $this->customStartPageNumber ? $this->PageNo() + $this->customStartPageNumber - 1 : $this->PageNo();
		$this->Cell(130, 8, 'Page '.$pageNumber.'  '.date("d.m.Y"), 0, 1, 'R');
		
	}

}

class MYFPDI extends FPDI {

    var $_tplIdx;
    
    protected $footerCaption;

    protected $customStartPageNumber;

    protected $showHeader;

	public function setHeaderVisibility($visible=true) {
        $this->showHeader = $visible;
    }

	public function setFooterVisibility($visible=true) {
        $this->showFooter = $visible;
    }

	public function setFooterCaption($caption) {
        $this->footerCaption = $caption;
    }

	public function getStartPageNumber() {
        return $this->customStartPageNumber;
    }

	public function setStartPageNumber($pageNumber) {
        $this->customStartPageNumber = $pageNumber;
    }

    public function Header() {
		if ($this->showHeader) {
			$file = $_SERVER['DOCUMENT_ROOT'].'/public/images/logo.jpg';
			$x=170; $y=10; $width=30;
			$this->Image($file, $x, $y, $width);
		}
	}
    
    function Footer() {
        
        if ($this->showFooter) {
			$this->SetY(-14);
			
			// footer left caption
			$this->SetFont('arialn', 'N', 9);
			$this->Cell(60, 8, $this->footerCaption, 0, 0, 'L');
			
			// page number
			$this->SetFont('arialn', 'N', 9);
			$pageNumber = $this->customStartPageNumber ? $this->PageNo() + $this->customStartPageNumber - 1 : $this->PageNo();
			$this->Cell(130, 8, 'Page '.$pageNumber.'  '.date("d.m.Y"), 0, 1, 'R');
		}
    }
}


// item booklets :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


$item = new Item();

$_MARGIN_LEFT = 10;
$_MARGIN_RIGHT = 10;
$_MARGIN_TOP = 20;
$_MARGIN_BOTTOM = 10;
$_TABLE_ROW_HEIGHT = 10;
$_SECTION_ROW_HEIGHT = 20;
$_SECTION_DIFF = 10;

$_FILE_PURPOSE_COVER_IMAGE = 9;
$_FILE_PURPOSE_TECHNICAL_DRAWING= 10;

foreach ($_ITEMS as $id) {

	$item->read($id);

	if (!$item->id) {
		$_ERRORS[] = "Item $id not found";
		goto BLOCK_RESPONSE;
		break;
	}

	// pdf instance
	$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor($user->firstname.' '.$user->name);
	$pdf->SetTitle('Product Info Sheet');
	$pdf->SetMargins($_MARGIN_LEFT, $_MARGIN_TOP, $_MARGIN_RIGHT);
	$pdf->SetAutoPageBreak(TRUE, $_MARGIN_BOTTOM);
	$pdf->SetLineWidth(0.1);
	$pdf->SetDrawColor(120);
	
	$pdf->setFooterCaption($item->code.', '.$item->name);

	$materials = array();
	$packingInformations = array();
	$spareParts = array();
	$certificates = array();
	$iteminformations = array();
	$pdfDocuments = array();
	$pictures = array();
	$technicalDrawing = array();
	$coverImage = null;
	$merger = null;
	$globalPageNumber=1;

	if ($_SECTION_PROPERTIES) {

		// certifictae materials
		$getCertificateMaterials->execute(array($id));
		$certificateMaterials = $getCertificateMaterials->fetchAll();
		
		// certifictae materials
		$getCertificateMaterials->execute(array($id));
		$certificateMaterials = $getCertificateMaterials->fetchAll();
		
		// get materials
		$getMaterials->execute(array($id));
		$materials = $getMaterials->fetchAll();

		// get electrical specifications
		$getElectricalSpecifications->execute(array($id));
		$electricalSpecifications = $getElectricalSpecifications->fetchAll();

		// supplier
		$getSupplier->execute(array($id));
		$res = $getSupplier->fetch();
		$ItemSupplier = $res['company'];
		$CurrencySymbol = $res['currency_symbol'];
		$SupplierPrice = $res['supplier_item_price'];

		// available for product lines
		$getAvailableProductLines->execute(array($id));
		$productLines = $getAvailableProductLines->fetchAll();
		
		$tmp = array();
		foreach($productLines as $key=>$data)
		{
			$tmp[] = $data["product_line_name"];
		}

		$productLineNames = "";
		if(count($tmp) > 0) {
			$productLineNames = implode(', ', $tmp);
		}
	}

	if ($_SECTION_PACKING) {
		
		// get packing informations
		$getPackagingInformations->execute(array($id));
		$packingInformations = $getPackagingInformations->fetchAll();
	}

	if ($_SECTION_SPARE_PARTS) {
		
		// get spare parts
		$getSpareParts->execute(array($id));
		$spareParts = $getSpareParts->fetchAll();
	}
	
	if ($_SECTION_COVER || $_SECTION_TECHNICAL || $_SECTION_FILES) {

		$getFiles->execute(array($id));
		$result = $getFiles->fetchAll();

		if ($result) {

			foreach ($result as $row) {

				$filepath = $_SERVER['DOCUMENT_ROOT'].$row['item_file_path'];
				$filePorpose = $row['file_purpose_id'];

				if (!file_exists($filepath)) continue;

				// cover image
				if (!$coverImage && $filePorpose==$_FILE_PURPOSE_COVER_IMAGE && check::image($filepath) ) {
					$coverImage = $filepath;
				} 
				// technical drawing
				elseif ($filePorpose==$_FILE_PURPOSE_TECHNICAL_DRAWING && check::pdf($filepath)) {
					$technicalDrawing[] = $filepath;
				}
				// pictures
				elseif (check::image($filepath)) {
					$pictures[] = $filepath;
				}
				// pdf documents
				elseif (check::pdf($filepath)) {
					$pdfDocuments[] = $filepath;
				}
			}
		}
	}

	if ($_SECTION_CERTIFICATES) {
		
		$getCertificates->execute(array($id));
		$result = $getCertificates->fetchAll();

		if ($result) {
			
			foreach ($result as $row) {
				
				$file = $_SERVER['DOCUMENT_ROOT'].$row['certificate_file_file'];
				$cid = $row['certificate_file_certificate_id'];

				if (!$certificates[$cid] && $row['certificate_file_file'] && file_exists($file)) {
					if ( check::image($file) || check::pdf($file)) {
						$certificates[$cid] = $row;
						$certificates[$cid]['file'] = $file;
					}
				}
			}
		}
	}



	if ($_SECTION_ITEMINFORMATION) {
		
		$getIteminformation->execute(array($id));
		$result = $getIteminformation->fetchAll();

		if ($result) {
			
			foreach ($result as $row) {
				
				$file = $_SERVER['DOCUMENT_ROOT'].$row['item_information_file'];
				$iid = $row['item_information_id'];

				if (!$iteminformations[$iid] && $row['item_information_file'] && file_exists($file)) {
					if ( check::image($file) || check::pdf($file)) {
						$iteminformations[$iid] = $row;
						$iteminformations[$iid]['file'] = $file;
					}
				}
			}
		}
	}


	// pdf merger instance :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$merger = 
		($_SECTION_TECHNICAL && $technicalDrawing) || 
		($_SECTION_FILES && $pdfDocuments) || 
		($_SECTION_CERTIFICATES && $certificates || 
		 $_SECTION_ITEMINFORMATION && $iteminformations) 
		? new SetaPDF_Merger() : null;

	// section: cover ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	if ($_SECTION_COVER) {

		$globalPageNumber++;
		
		$pdf->AddPage();

		// item title
		$pdf->setY(25);
		$pdf->SetFont('arialn', 'B', 18);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Cell(0, 10, $item->code , 0, 1, 'C', 0);

		// item description
		$pdf->SetFont('arialn', null, 16);
		$pdf->SetTextColor(80, 80, 80);
		$pdf->Cell(0,10, $item->name, 0, 1, 'C', 0);

		if (file_exists($coverImage)) {

			$x = $pdf->getX();
			$y = $pdf->getY() + $_SECTION_DIFF;
			$heightToFooter = 270-ceil($y+40);

			// get image in mm unit and resize it if max values are exceeded
			$resize = Image::getMaxSize($coverImage, 190, $heightToFooter, 300);
			$width = $resize['width'];
			$height = $resize['height'];

			$x = ceil((190-$width)/2)+$_MARGIN_LEFT;
			$pdf->Image($coverImage, $x, $y, $width, $height);
		}
	}

	
	
	// technical drawing :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	if ($_SECTION_TECHNICAL && $merger && $technicalDrawing) {

		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		$pdf = null;

		$technicalDrawingPageNumber = $globalPageNumber;

		foreach ($technicalDrawing as $file) {
	
			// initiate PDF
			$fpdi = new MYFPDI();
			$fpdi->setStartPageNumber($technicalDrawingPageNumber);
			$fpdi->setHeaderVisibility(true);
			$fpdi->setFooterVisibility(true);
			$fpdi->SetMargins($_MARGIN_LEFT, $_MARGIN_TOP, $_MARGIN_RIGHT);
			$fpdi->SetAutoPageBreak(true, 40);
			$fpdi->setFooterCaption($item->code.', '.$item->name);

			// get source pdf
			$pageCount = $fpdi->setSourceFile($file);

			for ($i=1; $i <= $pageCount; $i++) { 
				$fpdi->AddPage();
				$tpl = $fpdi->ImportPage($i);
				$fpdi->useTemplate($tpl, $_MARGIN_LEFT, $_MARGIN_TOP, 190, 0, true);

				$technicalDrawingPageNumber++;


				// item title
				$fpdi->setY(25);
				$fpdi->SetFont('arialn', 'B', 18);
				$fpdi->SetTextColor(0, 0, 0);
				$fpdi->Cell(0, 10, $item->code , 0, 1, 'C', 0);

				// item description
				$fpdi->SetFont('arialn', null, 16);
				$fpdi->SetTextColor(80, 80, 80);
				$fpdi->Cell(0,10, $item->name, 0, 1, 'C', 0);
			}

			$pdfString = $fpdi->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
			$merger->addDocument($tmp);
		}

		$globalPageNumber = $technicalDrawingPageNumber-1;

		
		
		if (
			$_SECTION_PROPERTIES || $_SECTION_LOGISTIC || 
			($_SECTION_PACKING && $packingInformations) 
			|| ($_SECTION_SPARE_PARTS && $spareParts) || 
			($_SECTION_FILES && $pictures)
		) 
		{

			$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor($user->firstname.' '.$user->name);
			$pdf->SetTitle('Product Info Sheet');
			$pdf->SetMargins($_MARGIN_LEFT, $_MARGIN_TOP, $_MARGIN_RIGHT);
			$pdf->SetAutoPageBreak(TRUE, $_MARGIN_BOTTOM);
			$pdf->SetTextColor(0, 0, 0);
			$pdf->SetFillColor(220, 220, 220);
			$pdf->SetDrawColor(100, 100, 100);
			$pdf->SetLineWidth(0.1);

			$globalPageNumber++;
			$pdf->setStartPageNumber($globalPageNumber);
			$pdf->setFooterCaption($item->code.', '.$item->name);
			
			//$pdf->AddPage();
		}
		
	}
	
	// section: product properties :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	if ($_SECTION_PROPERTIES) {

		$globalPageNumber++;
		
		$pdf->AddPage();

		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFillColor(220, 220, 220);
		$pdf->SetDrawColor(100, 100, 100);
		$pdf->SetLineWidth(0.1);

		$pdf->SetFont('arialn', 'B', 14);
		$pdf->Cell(190, 10, 'Product Properties', 0, 1, 'L', 0);

		$pdf->SetFont('arialn', null, 10);

		$border = 0; $fill = false;

		if ($item->code) {
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$fill = !$fill;
			$pdf->Cell(50, 7, 'Product Code', $border, 0, 'L', $fill);
			$pdf->Cell(140, 7, $item->code, $border, 1, 'L', $fill);
		}
		
		if ($item->name) {
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$fill = !$fill;
			$pdf->Cell(50, 7, 'Product Name', $border, 0, 'L', $fill);
			$pdf->Cell(140, 7, $item->name, $border, 1, 'L', $fill);
		}
		
		if ($item->description) {
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$fill = !$fill;
			$x = $pdf->getX();
			$y = $pdf->getY();
			$pdf->writeHTMLCell(140, null, $x+50, $y, $item->description, $border, 1, $fill, true, 'L', true);
			$height = $pdf->getY()-$y;
			$pdf->MultiCell(50, $height, 'Description', $border, 'L', $fill, 1, $x, $y);
		}

		if($SupplierPrice) {
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$fill = !$fill;
			$pdf->Cell(50, 7, 'Price', $border, 0, 'L', $fill);
			$pdf->Cell(140, 7, $CurrencySymbol . " " .$SupplierPrice, $border, 1, 'L', $fill);	
		}
		elseif ($item->price) {
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$fill = !$fill;
			$pdf->Cell(50, 7, 'Price', $border, 0, 'L', $fill);
			$pdf->Cell(140, 7, 'CHF '.$item->price, $border, 1, 'L', $fill);	
		}

		if ($item->watches_displayed) {
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$fill = !$fill;
			$pdf->Cell(50, 7, 'Products Displayed', $border, 0, 'L', $fill);
			$pdf->Cell(140, 7, $item->watches_displayed, $border, 1, 'L', $fill);
		}

		if ($item->watches_stored) {
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$fill = !$fill;
			$pdf->Cell(50, 7, 'Products Stored', $border, 0, 'L', $fill);
			$pdf->Cell(140, 7, $item->watches_stored, $border, 1, 'L', $fill);
		}
		
		if ($certificateMaterials || $materials) {

			$x = $pdf->getX();
			$y = $pdf->getY();

			$text = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" >";
			
			if ($certificateMaterials) {
				foreach ($certificateMaterials as $row) {
					if ($row['catalog_material_name']) {
						$text .= "<tr>";
						$text .= "<td>{$row[catalog_material_name]}</td>";
						$text .= "</tr>";
					}
				}
			}

			if ($materials) {
				foreach ($materials as $row) {
					if ($row['item_material_name']) {
						$text .= "<tr>";
						$text .= "<td>{$row[item_material_name]}</td>";
						$text .= "</tr>";
					}
				}
			}

			$text .= "</table>";
			
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$fill = !$fill;
			$pdf->writeHTMLCell(140, null, $x+50, $y, $text, $border, 1, $fill, true, 'L', true);

			$height = $pdf->getY()-$y;
			$pdf->MultiCell(50, $height, 'Materials', $border, 'L', $fill, 1, $x, $y);
		} 

		if ($electricalSpecifications) {

			$x = $pdf->getX();
			$y = $pdf->getY();

			$text = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" >";
			
			foreach ($electricalSpecifications as $row) {
				$text .= "<tr>";
				$text .= "<td width=\"44%\">{$row[item_electricspec_name]}</td>";
				$text .= "<td width=\"2%\">&nbsp;</td>";
				$text .= "<td width=\"44%\">{$row[item_electricspec_description]}</td>";
				$text .= "</tr>";
			}

			$text .= "</table>";

			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$fill = !$fill;
			$pdf->writeHTMLCell(140, null, $x+50, $y, $text, $border, 1, $fill, true, 'L', true);
			
			$height = $pdf->getY()-$y;
			$pdf->MultiCell(50, $height, 'Electrical specifications', $border, 'L', $fill, 1, $x, $y);

		} 

		if ($item->regulatory_approvals) {
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$fill = !$fill;
			$pdf->Cell(50, 7, 'Regulatory Approvals', $border, 0, 'L', $fill);
			$pdf->Cell(140, 7, $item->regulatory_approvals, $border, 1, 'L', $fill);
		}

		if ($item->install_requirements) {
			
			$x = $pdf->getX();
			$y = $pdf->getY();

			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$fill = !$fill;

			$pdf->MultiCell(140, null, $item->install_requirements, $border, 'L', $fill, 1, $x+50, $y);

			$height = $pdf->getY()-$y;
			$pdf->MultiCell(50, $height, 'Installation Information', $border, 'L', $fill, 1, $x, $y);
		}

		if ($_COST_GROUPS[$item->cost_group]) {
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$fill = !$fill;
			$pdf->Cell(50, 7, 'Cost Group', $border, 0, 'L', $fill);
			$pdf->Cell(140, 7, $_COST_GROUPS[$item->cost_group], $border, 1, 'L', $fill);
		}

		if ($ItemSupplier) {
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$fill = !$fill;
			$pdf->Cell(50, 7, 'Supplier', $border, 0, 'L', $fill);
			$pdf->Cell(140, 7, $ItemSupplier, $border, 1, 'L', $fill);
		}
		
		if ($productLineNames) {
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$fill = !$fill;
			$x = $pdf->getX();
			$y = $pdf->getY();
			$pdf->writeHTMLCell(140, null, $x+50, $y, $productLineNames, $border, 1, $fill, true, 'L', true);
			$height = $pdf->getY()-$y;
			$pdf->MultiCell(50, $height, 'Available for', $border, 'L', $fill, 1, $x, $y);
		}
	}

	
	if (!$_SECTION_PROPERTIES && !$_SECTION_TECHNICAL 
		&& ($_SECTION_LOGISTIC || $_SECTION_REMARKS || $_SECTION_PACKING || $_SECTION_SPARE_PARTS) ) 
	{
		$pdf->AddPage();
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFillColor(220, 220, 220);
	}

	// Logistic Information ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$showItemLength = $item->length && $item->length <> '0.00' ? true : false;
	$showItemWidth = $item->width && $item->width <> '0.00' ? true : false;
	$showItemHeight = $item->height && $item->height <> '0.00' ? true : false;
	$showNetWeight = $item->net_weight && $item->net_weight <> '0.00' ? true : false;
	$showUnit = $_UNITS[$item->unit] ? true : false;
	$showStackable = $item->stackable ? true : false;

	$showLogistic = array_filter(array($showItemLength,$showItemWidth,$showItemHeight,$showNetWeight,$showUnit,$showStackable));

	if ($_SECTION_LOGISTIC && $showLogistic) {

		if (!$merger) {
			$totalSectionHeight = 7*$_TABLE_ROW_HEIGHT+$_SECTION_ROW_HEIGHT;
			$heightToFooter = 270-ceil($pdf->getY());
			
			if ($totalSectionHeight >= $heightToFooter ) {
				$pdf->AddPage();
				$globalPageNumber++;
			}
			else $pdf->Ln($_SECTION_DIFF);
		}
		else
		{
			$pdf->Ln($_SECTION_DIFF);
		}

		$border = 0; $fill = false;
		
		$pdf->SetFont('arialn', 'B', 14);
		$pdf->Cell(190, 10, "Dimensions and Weight", 0, 1, 'L', 0);

		$pdf->SetFont('arialn', null, 10);

		if ($showItemLength) {
			$fill = !$fill;
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$pdf->Cell(50, 7, 'Length in cm', $border, 0, 'L', $fill);
			$pdf->Cell(140, 7, $item->length, $border, 1, 'L', $fill);
		}

		if ($showItemWidth) {
			$fill = !$fill;
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$pdf->Cell(50, 7, 'Width in cm', $border, 0, 'L', $fill);
			$pdf->Cell(140, 7, $item->width, $border, 1, 'L', $fill);
		}

		if ($showItemHeight) {
			$fill = !$fill;
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$pdf->Cell(50, 7, 'Height in cm', $border, 0, 'L', $fill);
			$pdf->Cell(140, 7, $item->height, $border, 1, 'L', $fill);
		}

		if ($showNetWeight) {
			$fill = !$fill;
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$pdf->Cell(50, 7, 'Net weight in kg', $border, 0, 'L', $fill);
			$pdf->Cell(140, 7, $item->net_weight, $border, 1, 'L', $fill);
		}

		if ($showUnit) {
			$fill = !$fill;
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$pdf->Cell(50, 7, 'Unit', $border, 0, 'L', $fill);
			$pdf->Cell(140, 7, $_UNITS[$item->unit], $border, 1, 'L', $fill);
		}

		if ($showStackable) {
			$fill = !$fill;
			$border = $border ? 0 : $_TABLE_ROW_BORDER;
			$pdf->Cell(50, 7, 'Stackable', $border, 0, 'L', 0);
			$pdf->Cell(140, 7, 'yes', $border, 1, 'L', $fill);
		}
	}

	// remarks :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	if ($_SECTION_REMARKS) {

		$totalSectionHeight = $_TABLE_ROW_HEIGHT+$_SECTION_ROW_HEIGHT;
		$heightToFooter = 270-ceil($pdf->getY());

		if ($totalSectionHeight >= $heightToFooter ) {
			$pdf->AddPage();
			$globalPageNumber++;
		}
		else $pdf->Ln($_SECTION_DIFF);

		$pdf->SetFont('arialn', 'B', 14);
		$pdf->Cell(190, 10, 'Remarks', 0, 1, 'L', 0);

		$pdf->SetFont('arialn', null, 10);

		$pdf->Cell(190, 7, 'Prices and technical data may change at any given time.', 'TB', 0, 'L', 1);

		$pdf->Ln($_SECTION_DIFF);
	}

	// Packing Informations ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	if ($_SECTION_PACKING && $packingInformations) {

		$totalSectionHeight = count($packingInformations)*$_TABLE_ROW_HEIGHT+$_SECTION_ROW_HEIGHT;
		$heightToFooter = 270-ceil($pdf->getY());

		if ($totalSectionHeight >= $heightToFooter ) {
			$pdf->AddPage();
			$globalPageNumber++;
		}
		else $pdf->Ln($_SECTION_DIFF);

		$pdf->SetFont('arialn', 'B', 14);
		$pdf->Cell(190, 10, 'Packing Information', 0, 1, 'L', 0);

		$fill = true;
		$border = $_TABLE_ROW_BORDER; 

		$pdf->SetFont('arialn', null, 10);
		$pdf->SetTextColor(255, 255, 255);
		$pdf->SetFillColor(80, 80, 80);
		$pdf->Cell(14, 7, 'Number', $border, 0, 'L', $fill);
		$pdf->Cell(25, 7, 'Unit', $border, 0, 'L', $fill);
		$pdf->Cell(30, 7, 'Packaging', $border, 0, 'L', $fill);
		$pdf->Cell(17, 7, 'Length cm', $border, 0, 'L', $fill);
		$pdf->Cell(17, 7, 'Width cm', $border, 0, 'L', $fill);
		$pdf->Cell(17, 7, 'Height cm', $border, 0, 'L', $fill);
		$pdf->Cell(20, 7, 'CBM', $border, 0, 'L', $fill);
		$pdf->Cell(25, 7, 'Net Weight kg', $border, 0, 'L', $fill);
		$pdf->Cell(25, 7, 'Gross Weight kg', $border, 1, 'L', $fill);
		
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFillColor(220, 220, 220);

		$totalCbm = 0;
		$totalWeightNet = 0;
		$totalWeightGross = 0;

		foreach ($packingInformations as $row) {
			
			$fill = !$fill;
			$border = $border ? 0 : $_TABLE_ROW_BORDER;

			$cbm = $row['length'] && $row['width'] && $row['height'] 
				? number_format($row['length']*$row['width']*$row['height']/1000000, 4, '.', '') 
				: 0;

			$totalCbm += $cbm;
			$totalWeightNet += $row['weight_net'];
			$totalWeightGross += $row['weight_gross'];

			$cbm = $cbm ?: null;

			$pdf->Cell(14, 7, $row['number'], $border, 0, 'L', $fill);
			$pdf->Cell(25, 7, $_UNITS[$row['unit']], $border, 0, 'L', $fill);
			$pdf->Cell(30, 7, $_PACKAGING_TYPES[$row['packaging']], $border, 0, 'L', $fill);
			$pdf->Cell(17, 7, $row['length'], $border, 0, 'L', $fill);
			$pdf->Cell(17, 7, $row['width'], $border, 0, 'L', $fill);
			$pdf->Cell(17, 7, $row['height'], $border, 0, 'L', $fill);
			$pdf->Cell(20, 7, $cbm, $border, 0, 'L', $fill);
			$pdf->Cell(25, 7, $row['weight_net'], $border, 0, 'L', $fill);
			$pdf->Cell(25, 7, $row['weight_gross'], $border, 1, 'L', $fill);
		}

		$fill = false;
		$border = $border ? 0 : $_TABLE_ROW_BORDER;

		// totals
		$pdf->Cell(120, 7, "Total:", $border, 0, 'R', $fill);
		$pdf->Cell(20, 7, number_format($totalCbm, 4, '.', ''), $border, 0, 'L', $fill);
		$pdf->Cell(25, 7, number_format($totalWeightNet, 2, '.', ''), $border, 0, 'L', $fill);
		$pdf->Cell(25, 7, number_format($totalWeightGross, 2, '.', ''), $border, 1, 'L', $fill);
	}
	
	
	
	// Spare Parts :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	if ($_SECTION_SPARE_PARTS && $spareParts) {

		/*
		$totalSectionHeight = count($spareParts)*$_TABLE_ROW_HEIGHT+$_SECTION_ROW_HEIGHT;
		$heightToFooter = 270-ceil($pdf->getY());

		if ($totalSectionHeight >= $heightToFooter ) {
			$pdf->AddPage();
			$globalPageNumber++;
		}
		else $pdf->Ln($_SECTION_DIFF);
		*/

		$pdf->Ln($_SECTION_DIFF);

		$border = 0; $fill = false;

		$pdf->SetFont('arialn', 'B', 14);
		$pdf->Cell(190, 10, 'Spare Parts', 0, 1, 'L', 0);

		$pdf->SetFont('arialn', null, 10);
		$pdf->SetTextColor(255, 255, 255);
		$pdf->SetFillColor(80, 80, 80);

		$fill = true;
		$border = $_TABLE_ROW_BORDER; 

		$pdf->Cell(50, 7, 'Item Code', $border, 0, 'L', $fill);
		$pdf->Cell(140, 7, 'Item Name', $border, 1, 'L', $fill);

		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFillColor(220, 220, 220);

		foreach ($spareParts as $row) {
			
			$fill = !$fill;
			$border = $border ? 0 : $_TABLE_ROW_BORDER;

			$pdf->Cell(50, 7, $row['item_code'], $border, 0, 'L', $fill);
			$pdf->Cell(140, 7, $row['item_name'], $border, 1, 'L', $fill);
		}
	}

	// item pictures :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	if ($_SECTION_FILES && $pictures) {

		// resize factor in procent
		$resizeFactor = 30;

		$pdf->AddPage();
		$globalPageNumber++;

		$pdf->SetFont('arialn', 'B', 14);
		$pdf->Cell(190, 10, "Pictures", 0, 1, 'L', 0);	

		foreach ($pictures as $picture) {

			$x = $_MARGIN_LEFT;
			$y = ceil($pdf->GetY());
			$heightToFooter = 270-$y;

			// get image in mm unit and resize it if max values are exceeded
			$resize = Image::getMaxSize($picture, 190, null, 300);
			$width = $resize['width'];
			$height = $resize['height'];

			// tuneup reisizing
			// if image height is greater as cuurent height to document footer
			// and image resizing is less then $resizeFactor
			if ($height > $heightToFooter) {
				
				$diff = $height-$heightToFooter;
				$factor = $diff*100/$height;

				if ($factor <= $resizeFactor) {
					$resize = Image::getMaxSize($picture, 190, $heightToFooter, 300);
					$width = $resize['width'];
					$height = $resize['height'];
				}
				else {
					$globalPageNumber++;
					$pdf->AddPage();
					$y = ceil($pdf->GetY());
					$heightToFooter = 270-$y;
					$resize = Image::getMaxSize($picture, 190, $heightToFooter, 300);
					$width = $resize['width'];
					$height = $resize['height'];
				}
			}

			$x = ceil((190-$width)/2)+$_MARGIN_LEFT;
			$pdf->Image($picture, $x, $y, $width, $height);
			$pdf->SetY($y+$height+$_SECTION_DIFF);
		}
	}

	
	// export pdf to merger ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	if ($pdf && $merger) {
		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		$pdf = null;
	}

	// bind pdf documents ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	if ($_SECTION_FILES && $pdfDocuments && $merger) {

		$globalPageNumber++;
		
		foreach ($pdfDocuments as $file) {

			// initiate PDF
			$fpdi = new MYFPDI();
			$fpdi->setStartPageNumber($globalPageNumber);
			$fpdi->SetMargins($_MARGIN_LEFT, $_MARGIN_TOP, $_MARGIN_RIGHT);
			$fpdi->SetAutoPageBreak(true, 40);
			$fpdi->setFooterCaption($item->code.', '.$item->name);

			// get source pdf
			$pageCount = $fpdi->setSourceFile($file);

			for ($i=1; $i <= $pageCount; $i++) { 
				$fpdi->AddPage();
				$tpl = $fpdi->ImportPage($i);
				$fpdi->useTemplate($tpl, $_MARGIN_LEFT, $_MARGIN_TOP, 190, 0, true);
				$globalPageNumber++;
			}

			$pdfString = $fpdi->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
			$merger->addDocument($tmp);
		}
	}

	// bind certificates :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	if ($_SECTION_CERTIFICATES && $certificates && $merger) {
		
		foreach ($certificates as $certificate) {
			
			// certificate title page
			if ($certificate['certificate_title_title'] || $certificate['certificate_title_description']) {

				$globalPageNumber++;

				$pdfTitle = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
				$pdfTitle->SetCreator(PDF_CREATOR);
				$pdfTitle->SetAuthor($user->firstname.' '.$user->name);
				$pdfTitle->SetTitle('Product Info Sheet');
				$pdfTitle->SetMargins($_MARGIN_LEFT, $_MARGIN_TOP, $_MARGIN_RIGHT);
				$pdfTitle->setFooterCaption($item->code.', '.$item->name);

				$pdfTitle->setStartPageNumber($globalPageNumber);

				$pdfTitle->AddPage();
				$pdfTitle->Ln(80);

				// title
				if ($certificate['certificate_title_title']) {
					$pdfTitle->SetFont('arialn', 'B', 24);
					$pdfTitle->SetTextColor(176, 0, 4);
					$pdfTitle->MultiCell(0, 15, $certificate['certificate_title_title'], 0, 'C', 0, 1);
				}

				// item description
				if ($certificate['certificate_title_description']) {
					$pdfTitle->SetFont('arialn', null, 14);
					$pdfTitle->SetTextColor(120, 120, 120);
					$pdfTitle->MultiCell(0, 15, $certificate['certificate_title_description'], 0, 'C', 0, 1);
				}

				$pdfTitle->Ln(110);
				$pdfTitle->SetFont('arialn', null, 10);
				$pdfTitle->SetTextColor(0, 0, 0);
	
				$pdfTitle->MultiCell(0,5, "Supplier:", 0, 'L', 0, 1, 40);
				$pdfTitle->MultiCell(0,5, trim($certificate['address_company']), 0, 'L', 0, 1, 40);
				$pdfTitle->MultiCell(0,5, trim($certificate['address_address']), 0, 'L', 0, 1, 40);
				$pdfTitle->MultiCell(0,5, trim($certificate['address_zip'])." ".trim($certificate['place_name']), 0, 'L', 0, 1, 40);
				$pdfTitle->MultiCell(0,5, trim($certificate['country_name']), 0, 'L', 0, 0, 40);
				$pdfTitle->MultiCell(0,5, "Expiry Date: ".$certificate['certificate_file_expiry_date'], 0, 'L', 0, 1, 140);

				$pdfString = $pdfTitle->output('', 'S');
				$tmp = SetaPDF_Core_Document::loadByString($pdfString);
				$merger->addDocument($tmp);
			}

			$globalPageNumber++;

			// initiate PDF
			$fpdi = new MYFPDI();
			$fpdi->SetMargins($_MARGIN_LEFT, $_MARGIN_TOP, $_MARGIN_RIGHT);
			$fpdi->SetAutoPageBreak(true, 40);
			$fpdi->setFooterCaption($item->code.', '.$item->name);

			$fpdi->setStartPageNumber($globalPageNumber);

			// get source pdf
			$pageCount = $fpdi->setSourceFile($certificate['file']);

			for ($i=1; $i <= $pageCount; $i++) { 
				$fpdi->AddPage();
				$tpl = $fpdi->ImportPage($i);
				$fpdi->useTemplate($tpl, $_MARGIN_LEFT, 10, 190, 0, true);
				$globalPageNumber++;
			}

			$globalPageNumber--;

			$pdfString = $fpdi->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
			$merger->addDocument($tmp);
		}
	}



	// bind item informations :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	if ($_SECTION_ITEMINFORMATION && $iteminformations && $merger) {
		
		foreach ($iteminformations as $iteminformation) {
			
			// iteminformation title page
			if ($iteminformation['item_information_title_title'] || $iteminformation['item_information_title_description']) {

				$globalPageNumber++;

				$pdfTitle = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
				$pdfTitle->SetCreator(PDF_CREATOR);
				$pdfTitle->SetAuthor($user->firstname.' '.$user->name);
				$pdfTitle->SetTitle('Product Info Sheet');
				$pdfTitle->SetMargins($_MARGIN_LEFT, $_MARGIN_TOP, $_MARGIN_RIGHT);
				$pdfTitle->setFooterCaption($item->code.', '.$item->name);

				$pdfTitle->setStartPageNumber($globalPageNumber);

				$pdfTitle->AddPage();
				$pdfTitle->Ln(80);

				// title
				
				
				if ($iteminformation['item_information_title_title']) {
					$pdfTitle->SetFont('arialn', 'B', 24);
					$pdfTitle->SetTextColor(176, 0, 4);
					$pdfTitle->MultiCell(0, 15, $iteminformation['item_information_title_title'], 0, 'C', 0, 1);
				}

				// item description
				if ($iteminformation['item_information_title_description']) {
					$pdfTitle->SetFont('arialn', null, 14);
					$pdfTitle->SetTextColor(120, 120, 120);
					$pdfTitle->MultiCell(0, 15, $certificate['item_information_title_description'], 0, 'C', 0, 1);
				}

				
				$pdfTitle->Ln(110);
				$pdfTitle->SetFont('arialn', null, 10);
				$pdfTitle->SetTextColor(0, 0, 0);
	
				$pdfTitle->MultiCell(0,5, "Supplier:", 0, 'L', 0, 1, 40);
				$pdfTitle->MultiCell(0,5, trim($iteminformation['address_company']), 0, 'L', 0, 1, 40);
				$pdfTitle->MultiCell(0,5, trim($iteminformation['address_address']), 0, 'L', 0, 1, 40);
				$pdfTitle->MultiCell(0,5, trim($iteminformation['address_zip'])." ".trim($iteminformation['place_name']), 0, 'L', 0, 1, 40);
				$pdfTitle->MultiCell(0,5, trim($iteminformation['country_name']), 0, 'L', 0, 0, 40);
				$pdfTitle->MultiCell(0,5, "Version Date: ".$iteminformation['item_information_version_date'], 0, 'L', 0, 1, 140);
				

				$pdfString = $pdfTitle->output('', 'S');
				$tmp = SetaPDF_Core_Document::loadByString($pdfString);
				$merger->addDocument($tmp);
			}

			$globalPageNumber++;

			// initiate PDF
			$fpdi = new MYFPDI("P", "mm", "A4", true, 'UTF-8', true);
			$fpdi->SetMargins($_MARGIN_LEFT, $_MARGIN_TOP, $_MARGIN_RIGHT);
			$fpdi->SetAutoPageBreak(true, 40);
			$fpdi->setFooterCaption($item->code.', '.$item->name);
			$fpdi->setHeaderVisibility(false);
			$fpdi->setFooterVisibility(false);

			$fpdi->setStartPageNumber($globalPageNumber);

			// get source pdf
			$pageCount = $fpdi->setSourceFile($iteminformation['file']);

			for ($i=1; $i <= $pageCount; $i++) { 
				$fpdi->AddPage();
				$tpl = $fpdi->ImportPage($i);
				//$fpdi->useTemplate($tpl);
				$fpdi->useTemplate($tpl, null, null, 0, 0, true);
				$globalPageNumber++;
			}


			$globalPageNumber--;

			$pdfString = $fpdi->output('', 'S');
			$tmp = SetaPDF_Core_Document::loadByString($pdfString);
			$merger->addDocument($tmp);
		}
	}

	// export ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$filename = !$_IS_MULTIPLE_ITEMS 
		? "product_info_sheet_".str_replace(' ', '', strtolower($item->code))
		: "product_info_sheet_".$item->id."_".$_SERVER['REQUEST_TIME'];

	$filepath = $_DIR_TMP."/$filename.pdf";
	$file = $_SERVER['DOCUMENT_ROOT'].$filepath;

	if ($merger) {

		// merge attachments and print file in temporary location

		$merger->merge();
		$document = $merger->getDocument();
		$document->setWriter(new SetaPDF_Core_Writer_File($file));
		$document->save()->finish();
	} 
	elseif ($pdf) {
		
		// print file in temporary location

		$pdf->Output($file, 'F');
	}

	// check output file
	if (file_exists($file)) {
		
		// for multi items request
		// add item to booklet merger
		
		if ($_IS_MULTIPLE_ITEMS) $_BOOKLETS[] = $file;
		else $_SUCCESS = true;

	} else {
		$_ERRORS[] = "PDF output failure $filepath";
		break;
	}
}


// export multi booklets :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_GROUP_FILES = array();

if (!$_ERRORS && $_IS_MULTIPLE_ITEMS && $_BOOKLETS) {

	// split booklets into the chunks
	$files = array_chunk($_BOOKLETS, 50);
	$multipleChunks = count($files)>1 ? true : false;

	foreach ($files as $i => $booklets) {
		
		$merger = new SetaPDF_Merger();

		foreach ($booklets as $file) {
			$merger->addFile($file);
		}

		$merger->merge();

		// filename
		$filename = $multipleChunks ? "item.booklet.$i" : "item.booklet";
		$filepath = $_DIR_TMP."/$filename.pdf";
		$file = $_SERVER['DOCUMENT_ROOT'].$filepath;

		// export merget documents
		$document = $merger->getDocument();
		$document->setWriter(new SetaPDF_Core_Writer_File($file));
		$document->save()->finish();

		// remove temporaray files
		if (file_exists($file)) {
			
			$_SUCCESS = true;

			if ($multipleChunks) {
				$_GROUP_FILES[] = $file;
			}

			// remove temporary item files
			foreach ($booklets as $tmpFile) {
				@chmod($tmpFile, 0666);
				@unlink($tmpFile);
			}
		}
	}	
}

if (!$_ERRORS && $_IS_MULTIPLE_ITEMS && $_GROUP_FILES) {

	$merger = new SetaPDF_Merger();

	foreach ($_GROUP_FILES as $file) {
		$merger->addFile($file);
	}

	$merger->merge();

	// filename
	$filepath = $_DIR_TMP."/item.booklet.pdf";
	$file = $_SERVER['DOCUMENT_ROOT'].$filepath;

	// export merget documents
	$document = $merger->getDocument();
	$document->setWriter(new SetaPDF_Core_Writer_File($file));
	$document->save()->finish();

	// remove temporaray files
	if (file_exists($file)) {
		
		$_SUCCESS = true;

		// remove temporary item files
		foreach ($_GROUP_FILES as $tmpFile) {
			@chmod($tmpFile, 0666);
			@unlink($tmpFile);
		}
	}
}

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONSE:

if ($_SUCCESS && $filepath) {
	$hash = Encryption::url($filepath);
	$_FILE_URL = "/download/tmp/$hash";
}

ob_end_clean();
header('Content-Type: text/json');
echo json_encode(array(
	'success' => $_SUCCESS,
	'file' => $_FILE_URL
));