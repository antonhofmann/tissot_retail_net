<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();
$db = Connector::get();

if (!Request::isAjax() || !$user->id) {
	goto BLOCK_RESPONSE;
}

// entity data
$_ENTITY = $_REQUEST['entity'];
$_ENTITY_DATA = $_REQUEST['entity_values'];

// sort list
$_SORT = $_REQUEST['sort'] ?: 'item_code';
$_SORT_FIELDS = $_REQUEST['sort'] ? explode(',', $_REQUEST['sort']) : array();

//default filters
$_FILTERS = array();
$_FILTERS[] = "item_id > 0";


if (!User::permission('can_edit_catalog')) {
	$_FILTERS[] = "item_active = 1";
}
elseif ($_REQUEST['active']) {
	$itemVisibility = $_REQUEST['active']==1 ? 1 : 0;
	$_FILTERS[] = "item_active = $itemVisibility";
}


// catalog category items ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
if ($_ENTITY=='catalog_category') {

	// filter by fulltext search
	if ($_ENTITY_DATA['searchterm']) {

		$searchTerm = strtolower($_ENTITY_DATA['searchterm']);

		$_FILTERS[] = "(
			LOWER(item_code) LIKE '%$searchTerm%' OR
			LOWER(item_name) LIKE '%$searchTerm%' OR
			LOWER(item_category_name) LIKE '%$searchTerm%' OR
			LOWER(product_line_name) LIKE '%$searchTerm%'
		)";

		if (isset($itemVisibility)) {
			$_FILTERS[] = "item_active = $itemVisibility";
		}

		$_FILTERS[] = "item_type = 1";
		$_FILTERS[] = "item_category_active = 1";
		$_FILTERS[] = "product_line_active = 1";
		
		// limited view
		if (!User::permission('can_edit_catalog')) {

			$company = new Company();
			$company->read($user->address);

			$country = new Country();
			$country->read($company->country);

			$_FILTERS[] = "(product_line_visible = 1 or product_line_budget = 1)";
			$_FILTERS[] = "(item_visible = 1 or item_visible_in_projects)";
			$_FILTERS[] = "item_country_country_id = ".$country->id;
		} 
		else {
			$_FILTERS[] = "(product_line_visible = 1 or product_line_budget = 1)";
		}
	}
	
	// category and product line
	if ($_ENTITY_DATA['cid'] && $_ENTITY_DATA['plid']) {

		$_FILTERS[] = "item_category = ".$_ENTITY_DATA['cid'];		
		$_FILTERS[] = "product_line_id = ".$_ENTITY_DATA['plid'];	  
		$_FILTERS[] = "product_line_active = 1";
		$_FILTERS[] = "item_category_active = 1";
		$_FILTERS[] = "item_type = 1";
		

		if (isset($itemVisibility)) {
			$_FILTERS[] = "item_active = $itemVisibility";
		}

		if (!User::permission('can_edit_catalog') and !User::permission('can_enter_orders_for_other_companies')) {

			$company = new Company();
			$company->read($user->address);

			$_FILTERS[] = "item_country_country_id = ".$company->country;
			$_FILTERS[] = "(item_visible = 1 or item_visible_in_projects)";
			$_FILTERS[] = "product_line_clients = 1";
		}
		else {
			$_FILTERS[] = "(product_line_clients = 1 or product_line_budget = 1)";
		}

	}

	// item subcategory
	if($_ENTITY_DATA['scid'] > 0)
	{
		$_FILTERS[] = "item_subcategory = ".$_ENTITY_DATA['scid'];
	}


	// supplying groups
	if($_ENTITY_DATA['sg']) {
		$_FILTERS[] = "item_supplying_group_supplying_group_id = ".$_ENTITY_DATA['sg'];
	}

	$_FILTERS = join(' AND ', $_FILTERS);


	$sql = "
		SELECT DISTINCT
			item_id AS id,
			item_code AS code,
			item_name AS name,
			IF (item_active = 1, 'active', 'inactive') AS active
		FROM product_lines
		INNER JOIN product_line_supplying_groups ON product_line_supplying_group_line_id = product_line_id
		INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = product_line_supplying_group_group_id
		INNER JOIN items ON item_id = item_supplying_group_item_id
		INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id 
		INNER JOIN productline_regions ON productline_region_productline = product_line_id
		INNER JOIN item_categories ON item_category_id = item_category
		LEFT JOIN item_subcategories on item_subcategory_id = item_subcategory
		INNER JOIN item_countries ON item_country_item_id = item_id 
		LEFT JOIN item_pos_types on item_pos_type_item = item_id 
		LEFT JOIN suppliers on supplier_item = item_id
		LEFT JOIN addresses on address_id = supplier_address
		WHERE $_FILTERS
		ORDER BY $_SORT
	";
} 


// project view material list ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_ENTITY == 'project_material_list') {

	$project = new Project();
	$project->read($_ENTITY_DATA['pid']);

	$sql = "
		SELECT 
			order_item_id, 
			item_id AS id,
			item_code AS code,
			item_name AS name,
			IF (item_active = 1, 'active', 'inactive') AS active
		FROM order_items
		LEFT JOIN items ON order_item_item = item_id
	";

	$_FILTERS[] = "order_item_cost_group = 2";
	$_FILTERS[] = "order_item_type = 1";
	$_FILTERS[] = "order_item_order = ".$project->order;

	$_FILTERS = join(' AND ', $_FILTERS);
	$sql .= " WHERE $_FILTERS";
	$sql .= " ORDER BY $_SORT";
}

// order material list :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_ENTITY == 'order_material_list') {

	$sql = "
		SELECT 
			order_item_id, 
			item_id AS id,
			item_code AS code,
			item_name AS name,
			IF (item_active = 1, 'active', 'inactive') AS active
		FROM order_items
		LEFT JOIN items ON order_item_item = item_id
	";

	$_FILTERS[] = "order_item_type = 1";
	$_FILTERS[] = "order_item_order = ".$_ENTITY_DATA['oid'];

	$_FILTERS = join(' AND ', $_FILTERS);
	
	$sql .= " WHERE $_FILTERS";
	$sql .= " ORDER BY $_SORT"; 
}

// load default list :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_REQUEST['entity']) {

	$_FILTERS[] = "item_id > 0 AND item_type = 1";

	if ($_REQUEST['categories']) {
		$categories = join(',', array_keys(array_filter($_REQUEST['categories'])));
		$_FILTERS[] = "item_category IN ($categories)";
	}

	if ($_REQUEST['subcategories']) {
		$subcategories = join(',', array_keys(array_filter($_REQUEST['subcategories'])));
		$_FILTERS[] = "item_subcategory IN ($subcategories)";
	}

	if ($_REQUEST['product_lines']) {
		$productlines = join(',', array_keys(array_filter($_REQUEST['product_lines'])));
		$_FILTERS[] = "product_line_id IN ($productlines)";
	}

	if ($_REQUEST['suppliers']) {
		$suppliers = join(',', array_keys(array_filter($_REQUEST['suppliers'])));
		$_FILTERS[] = "address_id IN ($suppliers)";
	}

	$_FILTERS = join(' AND ', $_FILTERS);

	$sql = "
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			item_id AS id,
			item_code AS code,
			item_name AS name,
			IF (item_active = 1, 'active', 'inactive') AS active
		FROM items	
		LEFT JOIN item_supplying_groups ON item_supplying_group_item_id = item_id
		LEFT JOIN product_line_supplying_groups ON product_line_supplying_group_group_id = item_supplying_group_supplying_group_id
		LEFT JOIN product_lines ON product_line_id = product_line_supplying_group_line_id
		LEFT JOIN item_categories ON item_category_id = item_category
		LEFT JOIN suppliers ON supplier_item = item_id
		LEFT JOIN addresses ON address_id = supplier_address
		WHERE $_FILTERS
		ORDER BY $_SORT
	";
}


// build list ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($sql) {
	
	$sth = $db->prepare($sql);
	$sth->execute();
	$result = $sth->fetchAll();

	if ($result) {
		
		foreach ($result as $i => $row) {
			
			$id = $row['id'];
			$result[$i]['active'] = "<i class=\"fa fa-circle {$row[active]}\"></i>";
			
			if ($_REQUEST['items'] && $_REQUEST['items'][$id]) {
				$result[$i]['checked'] = 1;
			}
		}
	}
}


BLOCK_RESPONSE:

ob_end_clean();
header('Content-Type: text/json');
echo json_encode($result);