<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'announcement_date';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "desc";
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

// filters
$fSearch = ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) ? $_REQUEST['search'] : null;

// session request
$_REQUEST = session::filter($application, "$controller.$archived.$action", true);


$model = new Model($application);

switch ($application) {
	
	case 'mps':
	

		// filter: full text search
		if ($fSearch) {
			$filters['search'] = "(
				mps_announcement_title LIKE \"%$fSearch%\"
				OR mps_announcement_text LIKE \"%$fSearch%\"
				OR mps_announcement_date LIKE \"%$fSearch%\"
			)";
		}

		// filter: active
		$filters['archived'] = ($archived)
			? "(mps_announcement_expiry_date < CURDATE() AND mps_announcement_expiry_date <> '' AND mps_announcement_expiry_date IS NOT NULL)"
			: "(mps_announcement_expiry_date >= CURDATE() OR mps_announcement_expiry_date = '' OR mps_announcement_expiry_date IS NULL)";

		$result = $model->query("
			SELECT SQL_CALC_FOUND_ROWS
				mps_announcement_id AS id,
				mps_announcement_important AS announcement_important,
				mps_announcement_title AS announcement_title,
				LEFT(mps_announcement_text, 100) as text,
				mps_announcement_date AS announcement_date,
				DATE_FORMAT(mps_announcement_date, '%d.%m.%Y') as date
			FROM mps_announcements
		")
		->filter($filters)
		->order($order, $direction)
		->offset($offset, $rowsPerPage)
		->fetchAll();
		
	break;

	case 'lps':
	

		// filter: full text search
		if ($fSearch) {
			$filters['search'] = "(
				lps_announcement_title LIKE \"%$fSearch%\"
				OR lps_announcement_text LIKE \"%$fSearch%\"
				OR lps_announcement_date LIKE \"%$fSearch%\"
			)";
		}

		// filter: active
		$filters['archived'] = ($archived)
			? "(lps_announcement_expiry_date < CURDATE() AND lps_announcement_expiry_date <> '' AND lps_announcement_expiry_date IS NOT NULL)"
			: "(lps_announcement_expiry_date >= CURDATE() OR lps_announcement_expiry_date = '' OR lps_announcement_expiry_date IS NULL)";

		$result = $model->query("
			SELECT SQL_CALC_FOUND_ROWS
				lps_announcement_id AS id,
				lps_announcement_important AS announcement_important,
				lps_announcement_title AS announcement_title,
				LEFT(lps_announcement_text, 100) as text,
				lps_announcement_date AS announcement_date,
				DATE_FORMAT(lps_announcement_date, '%d.%m.%Y') as date
			FROM lps_announcements
		")
		->filter($filters)
		->order($order, $direction)
		->offset($offset, $rowsPerPage)
		->fetchAll();
		
	break;
}

if ($result) {
	
	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);
	
	foreach ($datagrid as $key => $row) {
		
		if ($row['announcement_important']) {
			$datagrid[$key]['important'] = ui::icon('star');
		}
		
		$title = $row['announcement_title'];
		$content = strip_tags($row['text']);
		$datagrid[$key]['announcement_title'] = "<a href='".$_REQUEST['show']."/$key'>$title</a><span class=description >$content..</span>";
		
		// date
		$datagrid[$key]['announcement_date'] = $row['date'];
	}
	
	$pager = new Pager(array(
		'page' => $_REQUEST['page'],
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));
	
	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}

$table = new Table(array(
	'sort' => array('column' => $order, 'direction' => $direction)
));

$table->datagrid = $datagrid;

$table->caption('important', null);

if ($datagrid && _array::key_exists('important', $datagrid)) {
	$table->important('width=20px');
}

$table->announcement_date(
	Table::PARAM_SORT,
	'width=20%'
);

$table->announcement_title(
	Table::PARAM_SORT
);

$table->footer($list_index);
$table->footer($list_controlls);

$table = $table->render();

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// toolbox: add
if ($_REQUEST['add'])  {
	$toolbox[] = ui::button(array(
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'caption' => $translate->add_new
	));
}

$toolbox[] = ui::searchbox();

if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

echo $toolbox.$table;
	