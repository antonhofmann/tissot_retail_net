<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

switch ($application) {
				
	case 'mps':
	
		$permissionEdit = user::permission('can_edit_mps_announcements');
		$tableName = 'mps_announcements';
		$file = 'mps_announcement_file';
	break;

	case 'lps':
	
		$permissionEdit = user::permission('can_edit_lps_announcements');
		$tableName = 'lps_announcements';
		$file = 'lps_announcement_file';
	break;
}

// get announcement
$announcement = new Modul($application);
$announcement->setTable($tableName);
$announcement->read($id);

if ($announcement->id && $permissionEdit) {
	
	$filePath = $announcement->data[$file];
	$response = $announcement->delete();
	
	if ($response) {

		if ($filePath) {
			file::remove($filePath);
		}

		Message::request_deleted();
		url::redirect("/$application/$controller");

	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}