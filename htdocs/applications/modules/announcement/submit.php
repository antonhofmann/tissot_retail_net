<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['announcement_id'];
$filePath = $data['announcement_file'];
$uploadPath = "/public/data/files/$application/announcements/";

$data = array();

switch ($application) {
			
	case 'mps':
	
		
		$tableName = 'mps_announcements';

		if (isset($_REQUEST['announcement_date'])) {
			$data['mps_announcement_date'] = $_REQUEST['announcement_date'] ? date::sql($_REQUEST['announcement_date']) : null;
		}
		
		if (isset($_REQUEST['announcement_expiry_date'])) {
			$data['mps_announcement_expiry_date'] = $_REQUEST['announcement_expiry_date'] ? date::sql($_REQUEST['announcement_expiry_date']) : null;
		}
		
		if (isset($_REQUEST['announcement_text'])) {
			$data['mps_announcement_text'] = $_REQUEST['announcement_text'];
		}
		
		if (isset($_REQUEST['announcement_title'])) {
			$data['mps_announcement_title'] = $_REQUEST['announcement_title'];
		}
		
		if (in_array('announcement_important', $fields)) {
			$data['mps_announcement_important'] = ($_REQUEST['announcement_important']) ? 1 : 0;
		}
		
		if (isset($_REQUEST['announcement_file_title'])) {
			$data['mps_announcement_file_title'] = $_REQUEST['announcement_file_title'];
		}
		
		if ($_REQUEST['has_upload'] && $_REQUEST['announcement_file']) {
			$data['mps_announcement_file'] = upload::move($_REQUEST['announcement_file'], $uploadPath);
		}

	break;

	case 'lps':
	
		
		$tableName = 'lps_announcements';

		if (isset($_REQUEST['announcement_date'])) {
			$data['lps_announcement_date'] = $_REQUEST['announcement_date'] ? date::sql($_REQUEST['announcement_date']) : null;
		}
		
		if (isset($_REQUEST['announcement_expiry_date'])) {
			$data['lps_announcement_expiry_date'] = $_REQUEST['announcement_expiry_date'] ? date::sql($_REQUEST['announcement_expiry_date']) : null;
		}
		
		if (isset($_REQUEST['announcement_text'])) {
			$data['lps_announcement_text'] = $_REQUEST['announcement_text'];
		}
		
		if (isset($_REQUEST['announcement_title'])) {
			$data['lps_announcement_title'] = $_REQUEST['announcement_title'];
		}
		
		if (in_array('announcement_important', $fields)) {
			$data['lps_announcement_important'] = ($_REQUEST['announcement_important']) ? 1 : 0;
		}
		
		if (isset($_REQUEST['announcement_file_title'])) {
			$data['lps_announcement_file_title'] = $_REQUEST['announcement_file_title'];
		}
		
		if ($_REQUEST['has_upload'] && $_REQUEST['announcement_file']) {
			$data['lps_announcement_file'] = upload::move($_REQUEST['announcement_file'], $uploadPath);
		}

	break;
}


if($data) { 
	
	$announcement = new Modul($application);
	$announcement->setTable($tableName);
	$announcement->read($id);

	if ($announcement->id) {
		$data['user_modified'] = $user->login;
		$response = $announcement->update($data);
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		
		$response = $id = $announcement->create($data);
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		
		if ($_REQUEST['redirect']) {
			$redirect = $_REQUEST['redirect'].'/'.$id;
		}
	}

	if ($response && $_REQUEST['has_upload']) {
		$path = $filePath;
	}
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'redirect' => $redirect,
	'message' => $message,
	'path' => $path
));
