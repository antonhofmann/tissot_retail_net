<?php

	class Application { 

		static private $_applications = array();
		
		const PERMISSION_EDIT = 'can_administrate_system_data';
		const DB_BIND_DBS = 'INNER JOIN dbs ON db_id = application_db';
		
		public $data;

		public function __construct() {
			$this->model = new Application_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["application_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['application_id'];
			return $this->data;
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public function read_from_shortcut($application) {
			$this->data = $this->model->read_from_shortcut($application);
			$this->id = $this->data['application_id'];
			return $this->data;
		}
		
		public function file_extensions() {
			if ($this->id) {
				$result = $this->model->file_extensions($this->id);
				return _array::extract($result);
			}
		}
	
		public static function load($filters=null) {
			$model = new Application_Model(Connector::DB_CORE);
			$result = $model->load($filters);
			return _array::datagrid($result, 'application_shortcut');
		}
		
		public static function involved_in_planning($application) {
			$model = new Application_Model(Connector::DB_CORE);
			$result = $model->involved_in_planning($application);
			return ($result) ? true : false;
		}
		
		public static function loader($filters=null) {
			$model = new Application_Model(Connector::DB_CORE);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
		
		public static function get_all_involved_in_planning() {
			$model = new Application_Model(Connector::DB_CORE);
			return $model->get_all_involved_in_planning();
		}
		
		public static function dropdown($filters=null) {
			$model = new Application_Model(Connector::DB_CORE);
			$result = $model->dropdown($filters);
			return _array::extract($result);
		}

		static public function is($name) {

			if (!static::$_applications) {
				
				$pdo = Connector::get();

				$sth = $pdo->prepare("
					SELECT 
						IF (dbs.db_name IS NOT NULL, dbs.db_name, 'db_retailnet') AS db_name,
						applications.application_shortcut
					FROM applications 
					LEFT JOIN dbs ON applications.application_db = dbs.db_id
				");

				$sth->execute();
				$result = $sth->fetchAll();
				
				if ($result) {
					foreach ($result as $row) {
						$shortcut = $row['application_shortcut'];
						static::$_applications[$shortcut] = $row['db_name'];
					}
				}
			}
			
			if(isset($_applications) and array_key_exists($name, $_applications)) {
				return static::$_applications[$name];
			}
			return '';
			
		}
	}