<?php

	class Red_Comment_Access {
		
		public $id;
		public $comment;
		public $partner;

		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new Red_Comment_Access_Model($connector);
		}
		
		public function comment($comment) {
			$this->comment = $comment;
		}

		public function read($partner) {
			
			if ($this->comment) {
				
				$id = $this->model->read($this->comment, $partner);
				
				if ($id) {
					$this->partner = $partner;
					return $this->id = $id;
				}
			}
		}

		public function create($partner) {
			
			if ($this->comment) {
				
				$id =  $this->model->create($this->comment, $partner);
				
				if ($id) {
					$this->partner = $partner;
					return $this->id = $id;
				}
			}
		}

		public function delete($partner) {
			
			if ($this->comment) {
				
				$delete = $this->model->delete($this->comment, $partner);
				
				if ($delete) {
					$this->id = null;
					$this->partner = null;
					return true;
				}
			}
		}
		
		public function deleteAll() {
			
			if ($this->comment) {
				
				$delete = $this->model->deleteAll($this->comment);
				
				if ($delete) {
					$this->id = null;
					$this->partner = null;
					return true;
				}
			}
		}
	}
