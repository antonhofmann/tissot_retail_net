<?php

	/**
	 * Material Purpose
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Material_Purpose {
		
		/**
		 * Purpose ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Purpose Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Material ID
		 * @var integer
		 */
		public $material;
		
		/**
		 * Database Model
		 * @var object
		 */
		protected $model;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Edit all material purposes
		 * @var string
		 */
		const PERMISSION_EDIT = 'can_edit_mps_ltm_purposes';
		
		/**
		 * View all material purposes
		 * @var string
		 */
		const PERMISSION_VIEW = 'can_view_mps_ltm_purposes';
		
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Material_Purpose_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_material_purpose_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_material_purpose_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}	
	
		public static function loader($connector, $filters=null) {
			$model = new Material_Purpose_Model($connector);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
	}