<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$user = User::instance();
	$translate = Translate::instance();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];

	$db = Connector::get();
	
	// product line
	$_item_information = $_REQUEST['id'];
	$_ITEM = $_REQUEST['item'];
	
	$item_information = new Iteminformation();
	$item_information->read($_item_information);

	
	if ($item_information->id && $_ITEM) {
	
		if ($_REQUEST['checked']) {
			$response = $item_information->item()->create($_ITEM);
		} else {
			$response = $item_information->item()->delete($_ITEM);
		}

		$message = ($response) ? $translate->message_request_submitted : $translate->message_request_failure;
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}

	// update item materials :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$itemMaterials = array();
	$item_informationMaterials = array();
	$odheritem_informationMaterials = array();

	if ($response) {

		// get item materials
		$sth = $db->prepare("
			SELECT item_catalog_material_material_id
			FROM item_catalog_materials
			WHERE item_catalog_material_item_id = ?
		");

		$sth->execute(array($_ITEM));
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$itemMaterials[] = $row['item_catalog_material_material_id'];
			}
		}

		// get item_information materials
		$sth = $db->prepare("
			SELECT item_information_material_catalog_material_id
			FROM item_information_materials
			WHERE item_information_material_item_information_id = ?
		");

		$sth->execute(array($_item_information));
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$item_informationMaterials[] = $row['item_information_material_catalog_material_id'];
			}
		}

		// get other item_information materials
		// by item removing
		// need to check other item_information materials
		if (!$_REQUEST['checked']==0) {
			
			// get item suppliers
			$sth = $db->prepare("
				SELECT GROUP_CONCAT(DISTINCT address_id) as suppliers
				FROM suppliers 
				INNER JOIN addresses ON supplier_address = address_id
				WHERE supplier_item = ?
			");

			$sth->execute(array($_ITEM));
			$suppliers = $sth->fetch();

			// get other item_information materials
			$sth = $db->prepare("
				SELECT DISTINCT item_information_material_catalog_material_id
				FROM item_informations
				INNER JOIN item_information_materials ON item_information_material_item_information_id = item_information_id
				WHERE item_information_address_id IN ($suppliers) AND item_information_id != ?
			");

			$sth->execute(array($_item_information));
			$result = $sth->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$odheritem_informationMaterials[] = $row['item_information_material_catalog_material_id'];
				}
			}
		}
	}


	if ($item_informationMaterials) {

		$addMaterial = $db->prepare("
			INSERT INTO item_catalog_materials (
				item_catalog_material_item_id,
				item_catalog_material_material_id,
				user_created
			) VALUES (?, ?, ?)
		");

		$removeMaterial = $db->prepare("
			DELETE FROM item_catalog_materials
			WHERE item_catalog_material_item_id = ? AND item_catalog_material_material_id = ?
		");

		foreach ($item_informationMaterials as $material) {
			
			// add materials to item materials
			if ($_REQUEST['checked']==1) {
				if (!in_array($material, $itemMaterials)) {
					$addMaterial->execute(array($_ITEM, $material, $user->login));
				}
			}
			// remove materials from items materials
			else {

				$canRemove = true;

				if ($odheritem_informationMaterials) {
					$canRemove = in_array($material, $odheritem_informationMaterials) ? false : true;
				}

				if ($canRemove) {
					$removeMaterial->execute(array($_ITEM, $material));
				}
			}
		}
	}

	
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));
	