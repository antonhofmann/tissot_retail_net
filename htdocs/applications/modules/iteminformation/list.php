<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$model = new Model($application);

// sql order
$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'address_company, item_information_name';

$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

// filter: full text search
if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {

	$keyword = $_REQUEST['search'];

	$filters['search'] = "(
		item_information_name LIKE \"%$keyword%\"
		OR address_company LIKE \"%$keyword%\"
	)";
}


$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS
		item_informations.item_information_id, 
		item_informations.item_information_name, 
		item_information_file,
		item_information_version_date, 
		addresses.address_company,
		(
			SELECT COUNT(DISTINCT item_id) AS total
			FROM item_information_items
			INNER JOIN items ON item_id = item_information_item_item_id
			WHERE item_information_item_item_information_id = item_informations.item_information_id
		) AS total_items
	FROM item_informations 
	INNER JOIN addresses ON addresses.address_id = item_informations.item_information_address_id
")
->filter($filters)
->order($order, $direction)
->offset($offset, $rowsPerPage)
->fetchAll();


if ($result) {
	
	$totalrows = $model->totalRows();
	
	$today = date::timestamp();

	$downloadIcon = "
		<span class='fa-stack'>
		  <i class='fa fa-circle fa-stack-2x'></i>
		  <i class='fa fa-download fa-stack-1x fa-inverse'></i>
		</span>
	";

	$icoItemInfo = "
		<span class='fa-stack'>
		  <i class='fa fa-circle fa-stack-2x'></i>
		  <i class='fa fa-info-circle fa-stack-1x'></i>
		</span>
	";

	foreach ($result as $row) {
		
		$item_information_id = $row['item_information_id'];
		$file = $row['item_information_file_id'];
		$key = "$item_information_id/$file";

		$datagrid[$key]['address_company'] = $row['address_company'];
		$datagrid[$key]['item_information_name'] = $row['item_information_name'];

		$datagrid[$key]['item_information_version_date'] = date::system($row['item_information_version_date']);

		
		if ($row['total_items']) {
			$ico = "<i class='fa fa-info-circle'></i> {$row[total_items]}";
			$link = "<a href='/applications/templates/iteminformation/items.php?frame=1&id=$item_information_id' data-fancybox-type='iframe' title='Click to see item_information items' class='infobox btn btn-default btn-xs' >$ico</a>";
			$datagrid[$key]['items'] = "<div class='bootstrap'>$link</div>";
		}

		if ($row['item_information_file']) {
			$datagrid[$key]['downloads'] = "<a href='{$row['item_information_file']}' target='_blank' title='Download File' >$downloadIcon</a>";
		}
	}
	
	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));
	
	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}


// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$order' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// toolbox: add
if ($_REQUEST['add']) {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();


// toolbox: form
if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

$table = new Table(array(
	'sort' => array('column'=>$order,'direction'=>$direction)
));

$table->datagrid = $datagrid;

$table->address_company(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=160px'
);

$table->item_information_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	"href=".$_REQUEST['data']
);

$table->item_information_version_date(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=100px'
);


//$table->items();

$table->items(
	Table::ATTRIBUTE_NOWRAP,
	'width=20px'
);

$table->downloads(
	Table::ATTRIBUTE_NOWRAP,
	'width=20px'
);

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;


