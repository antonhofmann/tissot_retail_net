<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// disabled fields
$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

// url request
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];

// item information
$id = $_REQUEST['item_information_id'];

$upload_path = "/public/data/files/$application/iteminformation/$id";

if (in_array('item_information_name', $fields)) {
	$data['item_information_name'] = $_REQUEST['item_information_name'] ? $_REQUEST['item_information_name'] : null;
}

if (in_array('item_information_title_id', $fields)) {
	$data['item_information_title_id'] = $_REQUEST['item_information_title_id'] ? $_REQUEST['item_information_title_id'] : null;
}	

if (in_array('item_information_address_id', $fields)) {
	$data['item_information_address_id'] = $_REQUEST['item_information_address_id'] ? $_REQUEST['item_information_address_id'] : null;
}

if ($_REQUEST['item_information_version_date']) {
	$data['item_information_version_date'] = date::sql($_REQUEST['item_information_version_date']);
}

$invalid_extension = false;
if ($_REQUEST['has_upload'] && $_REQUEST['item_information_file']) {
	
	if(strpos($_REQUEST['item_information_file'], '.pdf') === false
		and strpos($_REQUEST['item_information_file'], '.PDF') === false)
	{
		$invalid_extension = true;
	}
	else {
		$path = upload::move($_REQUEST['item_information_file'], $upload_path);
		$data['item_information_file'] = $path;
	}
}

if ($data and $invalid_extension == false) {

	$item_information = new Iteminformation();
	$item_information->read($id);

	if ($item_information->id) {
		
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		
		$response = $item_information->update($data);
		
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		
		$response = $id = $item_information->create($data);
		
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		
		$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
	}
}
else {
	$response = false;
	
	if($invalid_extension == true) {

		$message = $translate->only_pdf_allowed;
	}
	else {
		$message = $translate->message_request_failure;
	}
}


echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect,
	'test' => $test
));