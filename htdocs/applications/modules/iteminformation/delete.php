<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$appliction = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$item_information = new Iteminformation();
$item_information->read($id);

$_PERMISSION_CAN_EDIT_CATALOG = user::permission('can_edit_catalog');
$_PERMISSION_CAN_EDIT_item_information = user::permission('can_edit_item_information');

if ($item_information->id && ($_PERMISSION_CAN_EDIT_CATALOG || $_PERMISSION_CAN_EDIT_item_information) ) {
	
	$delete = $item_information->delete();
	
	if ($delete) {

		$model = new Model();

		// remove files
		dir::remove("/public/data/files/$appliction/iteminformation/$id");

		Message::request_deleted();
		url::redirect("/$appliction/$controller");

	} else { 
		Message::request_failure();
		url::redirect("/$appliction/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$appliction/$controller");
}