<?php

	class IdeaFile {
		
		public $id;
		
		public $data;
		
		protected $model;
	
		public function __construct() {
			$this->model = new IdeaFile_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["idea_file_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['idea_file_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
	}