<?php

	/**
	 * Order Sheet Item Shipped
	 * @author adoweb
	 * @category mediaparx ag
	 * @version 1.0
	 */
	class Ordersheet_Item_Shipped {
		
		/**
		 * Item ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Order Sheet Item ID
		 * @var integer
		 */
		public $item;
		
		/**
		 * Item Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Database Model
		 * @return Ordersheet_Item_Shipped_Model
		 */
		protected $model;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Join Order Sheet Items
		 * @var string
		 */
		const DB_BIND_ORDERSHEET_ITEMS = 'INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_shipped_ordersheet_item_id';
		
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Ordersheet_Item_Shipped_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_ordersheet_item_shipped_$key"];
		}
		
		public function read($id) {
			
			$data = $this->model->read($id);
			
			if ($data) {
				$this->data = $data;
				$this->id = $data['mps_ordersheet_item_shipped_id'];
				$this->item = $data['mps_ordersheet_item_shipped_ordersheet_item_id'];
				return $data;
			}
		}
		
		public function read_from_reference($ponumber, $reference) {
			
			$data = $this->model->read_from_reference($ponumber, trim($reference));
			
			if ($data) {
				$this->data = $data;
				$this->id = $data['mps_ordersheet_item_shipped_id'];
				$this->item = $data['mps_ordersheet_item_shipped_ordersheet_item_id'];
				return $data;
			}
		}
		
		public function create($data) {
			
			if (is_array($data)) {
				
				if (!$data['mps_ordersheet_item_shipped_ordersheet_item_id']) {
					$data['mps_ordersheet_item_shipped_ordersheet_item_id'] = $this->item;
				}
				
				$id = $this->model->create($data);
				$this->read($id);
				
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) { 
				return $this->model->delete($this->id);
			}
		}

		public function delete_all() {
			if ($this->item) {
				return $this->model->delete_all($this->item);
			}
		}
		
		public function load($filters=null) { 
			if ($this->item) { 
				return $this->model->load($this->item, $filters);
			}
		}
	}
	