<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."tcpdf/tcpdf.php";
require_once  $_SERVER['DOCUMENT_ROOT'].'/include/SetaPDF/Autoload.php';

// request execution time
ini_set('max_execution_time', 240);
ini_set('memory_limit', '2048M');

$user = User::instance();
$translate = Translate::instance();
$db = Connector::get();

$_ID = $_REQUEST['id'];
$_FILES = $_REQUEST['files'];

// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$project = new Project();
$project->read($_ID);

if (!$_FILES) {
	$_ERRORS[] = "Bed request.";
	goto BLOCK_RESPONSE;
}

if (!User::permission('can_view_attachments_in_projects')) {
	$_ERRORS[] = "You don't have access to this page.";
	goto BLOCK_RESPONSE;
}


// dataloader ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$files = join(',', array_keys($_FILES));

$sth = $db->prepare("
	SELECT DISTINCT 
		order_file_id, 
		order_file_title, 
		order_file_path, 
		order_file_category_id,
		DATE_FORMAT(order_files.date_created, '%d.%m.%Y %H:%i') AS date,
		CONCAT(user_name, ' ', user_firstname) AS owner,
	 	CONCAT(order_file_category_priority, ' ', order_file_category_name) AS group_name
	FROM order_files 
	LEFT JOIN order_file_categories ON order_file_category_id = order_file_category
	LEFT JOIN users ON user_id = order_file_owner
	LEFT JOIN file_types ON order_file_type = file_type_id
	WHERE order_file_id IN ($files)
	ORDER BY order_file_category_priority, order_files.date_created DESC
");

$sth->execute();
$result = $sth->fetchAll();

$_PICTURES = array();
$_DOCUMENTS = array();

if ($result) {
	
	foreach ($result as $row) {

		$file = $_SERVER['DOCUMENT_ROOT'].$row['order_file_path'];

		if (!file_exists($file)) {
			continue;
		}

		if (check::image($file)) {
			$file = $row['order_file_id'];
			$category = $row['order_file_category_id'];
			$_PICTURES[$category]['name'] = $row['group_name'];
			$_PICTURES[$category]['files'][$file]['owner'] = $row['owner'];
			$_PICTURES[$category]['files'][$file]['title'] = $row['order_file_title'];
			$_PICTURES[$category]['files'][$file]['path'] = $row['order_file_path'];
			$_PICTURES[$category]['files'][$file]['type'] = $row['file_type_name'];
			$_PICTURES[$category]['files'][$file]['date'] = $row['date'];
		} 
		elseif (check::pdf($file)) {
			$_DOCUMENTS[] = $row['order_file_path'];
		}
	}

} 

if (!$_PICTURES && !$_DOCUMENTS) {
	$_ERRORS[] = "Images or PDF documents not found.";
	goto BLOCK_RESPONSE;
}


// pdf images ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_PICTURES) {

	class MYPDF extends TCPDF {
				
		function Header() {
			$file = $_SERVER['DOCUMENT_ROOT'].'/public/images/logo.jpg';
			$x=170; $y=10; $width=30;
			$this->Image($file, $x, $y, $width);
		}

		function Footer() {
			$this->SetY(-14);
			$this->SetFont('arialn', 'I', 8);
			$this->Cell(60, 8, 'Date: '.date("d.m.Y"), 0, 0, 'L');
			$this->Cell(130, 8, 'Page '.$this->PageNo(), 0, 1, 'R');
		}

	}

	$_MARGIN_LEFT = 10;
	$_MARGIN_RIGHT = 10;
	$_MARGIN_TOP = 20;
	$_MARGIN_BOTTOM = 10;
	$_TABLE_ROW_HEIGHT = 10;
	$_SECTION_ROW_HEIGHT = 20;
	$_SECTION_DIFF = 10;

	$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
	
	$_PAGE_WIDTH = $pdf->getPageWidth();
	$_PAGE_HEIGHT = $pdf->getPageHeight();
	$_WORKAREA_WIDTH = $_PAGE_WIDTH - $_MARGIN_LEFT - $_MARGIN_RIGHT;
	$_WORKAREA_HEIGHT = $_PAGE_HEIGHT - 10 - $_MARGIN_BOTTOM;

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor($user->firstname.' '.$user->name);
	$pdf->SetTitle('Download Attachemnts');

	// set margins
	$pdf->SetMargins($_MARGIN_LEFT, $_MARGIN_TOP, $_MARGIN_RIGHT);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, $_MARGIN_BOTTOM);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	    require_once(dirname(__FILE__).'/lang/eng.php');
	    $pdf->setLanguageArray($l);
	}

	$pdf->SetLineWidth(0.1);
	$pdf->SetDrawColor(120);

	// resize factor in procent
	$resizeFactor = 30;

	foreach ($_PICTURES as $gid => $group) {

		$pdf->AddPage();

		$pdf->SetFont('arialn', 'B', 14);
		$pdf->SetFillColor(220, 220, 220);
		$pdf->Cell(190, 10, $group['name'], 'B', 1, 'L', 0);
		$pdf->ln();

		$pdf->SetFont('arialn', '', 9);

		foreach ($group['files'] as $fid => $file) {
			
			$x = $_MARGIN_LEFT;
			$y = ceil($pdf->GetY());
			$heightToFooter = 270-$y;
			$image = $_SERVER['DOCUMENT_ROOT'].$file['path'];

			// get image in mm unit and resize it if max values are exceeded
			$resize = Image::getMaxSize($image, 190);
			$width = $resize['width'];
			$height = $resize['height'];

			if ($height > $heightToFooter) {
				
				$diff = $height-$heightToFooter;
				$factor = $diff*100/$height;

				if ($factor <= $resizeFactor) {
					$resize = Image::getMaxSize($image, 190, $heightToFooter);
					$width = $resize['width'];
					$height = $resize['height'];
				}
				else {
					$pdf->AddPage();
					$y = ceil($pdf->GetY());
					$heightToFooter = 270-$y;
					$resize = Image::getMaxSize($image, 190, $heightToFooter);
					$width = $resize['width'];
					$height = $resize['height'];
				}
			}

			$x = ceil((190-$width)/2)+$_MARGIN_LEFT;
			$pdf->Image($image, $x, $y, $width, $height);

			$pdf->SetY($y+$height+$_SECTION_DIFF);
		}
	}

	// if request containt pdf docments
	// merger pictures document
	if ($_DOCUMENTS && !$merger) {

		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);

		$merger = new SetaPDF_Merger();
		$merger->addDocument($tmp);
	}
}

// pdf documents :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


if ($_DOCUMENTS) {

	if (!$merger) {
		$merger = new SetaPDF_Merger();
	}

	foreach ($_DOCUMENTS as $file) {
		$merger->addFile($_SERVER['DOCUMENT_ROOT'].$file);
	}
}


// export ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$key = $_ID.'_'.date('YmdHis');
$file = "/data/tmp/attachments_$key.pdf";
$fullpath = $_SERVER['DOCUMENT_ROOT'].$file;

if ($merger) {
	$merger->merge();
	$document = $merger->getDocument();
	$document->setWriter(new SetaPDF_Core_Writer_File($fullpath));
	$document->save()->finish();
} else {
	$pdf->Output($fullpath, 'F');
}

$_SUCCESS = file_exists($fullpath) ? true : false;


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONSE:

ob_end_clean();

if ($_SUCCESS) {
	$hash = Encryption::url($file);
	$_FILE_URL = "/download/tmp/$hash";
}

header('Content-Type: text/json');
echo json_encode(array(
	'errors' => $_ERRORS,
	'success' => $_SUCCESS,
	'file' => $_FILE_URL,
	'pictures' => $_PICTURES,
	'documents' => $_DOCUMENTS
));
