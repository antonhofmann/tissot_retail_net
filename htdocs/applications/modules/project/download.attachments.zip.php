<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

// request execution time
ini_set('max_execution_time', 240);
ini_set('memory_limit', '2048M');

$user = User::instance();
$translate = Translate::instance();
$db = Connector::get();

$_ID = $_REQUEST['id'];
$_FILES = $_REQUEST['files'];

// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$project = new Project();
$project->read($_ID);

if (!$project->id || !$_FILES) {
	$_ERRORS[] = "Bed request.";
	goto BLOCK_RESPONSE;
}

if (!User::permission('can_view_attachments_in_projects')) {
	$_ERRORS[] = "You don't have access to this page.";
	goto BLOCK_RESPONSE;
}


// dataloader ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$files = join(',', array_keys($_FILES));

$sth = $db->prepare("
	SELECT DISTINCT 
		order_file_id, 
		order_file_title, 
		order_file_path, 
		order_file_category_id,
		DATE_FORMAT(order_files.date_created, '%d.%m.%Y %H:%i') AS date,
		CONCAT(user_name, ' ', user_firstname) AS owner,
	 	CONCAT(order_file_category_priority, ' ', order_file_category_name) AS group_name
	FROM order_files 
	LEFT JOIN order_file_categories ON order_file_category_id = order_file_category
	LEFT JOIN users ON user_id = order_file_owner
	LEFT JOIN file_types ON order_file_type = file_type_id
	WHERE order_file_id IN ($files)
	ORDER BY order_file_category_priority, order_files.date_created DESC
");

$sth->execute();
$result = $sth->fetchAll();

$_FILES = array();

if ($result) {
	
	foreach ($result as $row) {

		$file = $_SERVER['DOCUMENT_ROOT'].$row['order_file_path'];

		if (!file_exists($file)) {
			continue;
		}

		$_FILES[] = $row['order_file_path'];
	}

} 

if (!$_FILES) {
	$_ERRORS[] = "File(s) not found.";
	goto BLOCK_RESPONSE;
}


// export ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$key = $_ID.'_'.date('YmdHis');
$_FILE = "/data/tmp/attachments_$key.zip";
$_ZIP = $_SERVER['DOCUMENT_ROOT'].$_FILE;


$zip = new ZipArchive();
$zip->open($_ZIP, ZipArchive::CREATE);

foreach ($_FILES as $file) {
	
	$file = $_SERVER['DOCUMENT_ROOT'].$file;
	$response = $zip->addFile($file, basename($file));

	if (!$response) {
		$_ERRORS[] = "File $file cannot be compressed.";
	}
}

$zip->close();
@chmod($_ZIP, 0666);

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONSE:

ob_end_clean();

$_SUCCESS = file_exists($_ZIP) ? true : false;

if ($_SUCCESS) {
	$hash = Encryption::url($_FILE);
	$_FILE_URL = "/download/tmp/$hash";
}

header('Content-Type: text/json');
echo json_encode(array(
	'errors' => $_ERRORS,
	'success' => $_SUCCESS,
	'file' => $_FILE_URL,
	'files' => $_FILES
));
