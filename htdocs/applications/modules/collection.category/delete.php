<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$category = new Modul($application);

switch ($application) {
	
	case 'mps':
	

		// set table name
		$category->setTable('mps_material_collection_categories');

		$permission = user::permission(Material_Collection_Category::PERMISSION_EDIT);
		
	break;

	case 'lps':
	

		// set table name
		$category->setTable('lps_collection_categories');

		$permission = user::permission('can_edit_lps_collection_categories');

	break;
}

$category->read($id);

if ($category->id && $permission) {
	
	$delete = $category->delete();
	
	if ($delete) {
		Message::request_deleted();
		url::redirect("/$application/$controller");
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}