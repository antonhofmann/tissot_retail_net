<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['file_id'];
$category = $_REQUEST['file_entity'];

$upload_path = "/public/data/files/$application/collectioncategories/$category";

$file = new Modul($application);

$data = array();

switch ($application) {
	
	case 'mps':
	

		$file->setTable('mps_material_collection_category_files');

		$data['mps_material_collection_category_file_collection_id'] = $category;

		if ($_REQUEST['file_title']) {
			$data['mps_material_collection_category_file_title'] = $_REQUEST['file_title'];
		}

		if ($_REQUEST['has_upload'] && $_REQUEST['file_path']) {
			$path = $data['mps_material_collection_category_file_path'] = upload::move($_REQUEST['file_path'], $upload_path);
			$extension = file::extension($path);
			$file_type = new File_Type();
			$data['mps_material_collection_category_file_type'] = $file_type->get_id_from_extension($extension);
		}

		if (isset($_REQUEST['file_description'])) {
			$data['mps_material_collection_category_file_description'] = $_REQUEST['file_description'];
		}

		if (in_array('file_visible', $fields)) {
			$data['mps_material_collection_category_file_visible'] = ($_REQUEST['file_visible']) ? 1 : 0;
		}
		
	break;

	case 'lps':
	

		$file->setTable('lps_collection_category_files');

		$data['lps_collection_category_file_collection_id'] = $category;

		if ($_REQUEST['file_title']) {
			$data['lps_collection_category_file_title'] = $_REQUEST['file_title'];
		}

		if ($_REQUEST['has_upload'] && $_REQUEST['file_path']) {
			$path = $data['lps_collection_category_file_path'] = upload::move($_REQUEST['file_path'], $upload_path);
			$extension = file::extension($path);
			$file_type = new File_Type();
			$data['lps_collection_category_file_type'] = $file_type->get_id_from_extension($extension);
		}

		if (isset($_REQUEST['file_description'])) {
			$data['lps_collection_category_file_description'] = $_REQUEST['file_description'];
		}

		if (in_array('file_visible', $fields)) {
			$data['lps_collection_category_file_visible'] = ($_REQUEST['file_visible']) ? 1 : 0;
		}

	break;
}


if ($data) {
	
	$file->read($id);

	if ($file->id) {
		$data['user_modified'] = $user->login;
		$response = $file->update($data);
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		$response = $id = $file->create($data);
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		
		if ($response && $_REQUEST['redirect']) {
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect,
	'path' => $path,
	'fiile' => $file
));
