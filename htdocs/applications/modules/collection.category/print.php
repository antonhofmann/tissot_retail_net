<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

// execution time
ini_set('max_execution_time', 120);
ini_set('memory_limit', '1024M');

$settings = Settings::init();
$settings->load('data');
$user = User::instance();
$translate = Translate::instance();

// request vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

// request
$_REQUEST = session::filter($application, "$controller.$archived.$action", false);
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'collection_category_code';

// db model
$model = new Model($application);

switch ($application) {
	
	case 'mps':
	
		
		$sheetName = 'Collection Categories';
		
		// filter: full text search
		if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) { 
			
			$search = $_REQUEST['search'];
			
			$filters['search'] = "(
				mps_material_collection_category_code LIKE '%$search%'
			)";
			
			$captions[] = $translate->search.' Term: '.$search;
		}

		$result = $model->query("
			SELECT
				mps_material_collection_category_id AS collection_category_id,
				mps_material_collection_category_code AS collection_category_code
			FROM mps_material_collection_categories
		")
		->filter($filters)
		->order($order, $direction)
		->fetchAll(); 

	break;

	case 'lps':
	

		$sheetName = 'Collection Categories';

		// filter: full text search
		if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) { 
			
			$search = $_REQUEST['search'];
			
			$filters['search'] = "(
				lps_collection_category_code LIKE '%$search%'
			)";
			
			$captions[] = $translate->search.' Term: '.$search;
		}

		$result = $model->query("
			SELECT
				lps_collection_category_id AS collection_category_id,
				lps_collection_category_code AS collection_category_code
			FROM lps_collection_categories
		")
		->filter($filters)
		->order($order, $direction)
		->fetchAll();
		
	break;
}

	
if ($result) {
	
	require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
	require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

	$datagrid = _array::datagrid($result);
	$columns = array_keys(end($datagrid));
	$totalColumns = count($columns);

	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle($sheetName);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

	$styles = array(
		'borders' => array(
			'borders' => array(
				'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		),
		'number' => array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
		),
		'string' => array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		),
		'header' => array(
			'font' => array(
				'name'=>'Arial',
				'size'=> 11,
				'bold'=>true
			),
			'alignment' => array(
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'EEEEEE')
			),
			'borders' => array(
				'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		)
	);
	
	// cel dimensions
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
	$lastColumn = "A";


	$row=1;
	$range = "A$row:".$lastColumn.$row;
	$objPHPExcel->getActiveSheet()->mergeCells($range);
	$objPHPExcel->getActiveSheet()->setCellValue("A$row", $sheetName);
	$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(18);

	$row++;
	$range = "A$row:".$lastColumn.$row;
	$objPHPExcel->getActiveSheet()->mergeCells($range);
	$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: ".date('d.m.Y  H:i:s'));

	$row++;
	$range = "A$row:".$lastColumn.$row;
	$objPHPExcel->getActiveSheet()->mergeCells($range);

	// filter captions
	if ($captions) {
		
		foreach ($captions as $caption) {
			$row++;
			$range = "A$row:".$lastColumn.$row;
			$objPHPExcel->getActiveSheet()->mergeCells($range);
			$objPHPExcel->getActiveSheet()->setCellValue("A$row", $caption);
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
		}

		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);
	}

	// columns
	$row++;
	$col = 0;
	$range = "A$row:".$lastColumn.$row;

	foreach ($columns as $value) {
		$index = spreadsheet::index($col, $row);
		$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['header']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $translate->$value);
		$col++;
	}

	$row++;

	foreach ($datagrid as $i => $array) {
		
		$col = 0;
		
		foreach($array as $key => $value) {
			$index = spreadsheet::index($col, $row);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
			$col++;
	    }

		$range = "A$row:".$lastColumn.$row;
	    $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styles['borders']);
	    $row++;
	}

	$row++;
	$range = "A$row:".$lastColumn.$row;
	$range = "A$row:".$lastColumn.$row;
	$objPHPExcel->getActiveSheet()->mergeCells($range);

	$objPHPExcel->getActiveSheet()->setTitle($sheetName);
	$objPHPExcel->setActiveSheetIndex(0);

	// redirect output to client browser
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="collection-categories-'.date('Y-m-d').'.xlsx" ');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
}
else {
		message::empty_result();
		url::redirect("/$application/$controller$archived/$action");
}
	