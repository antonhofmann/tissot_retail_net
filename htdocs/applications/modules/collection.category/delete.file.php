<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$file = new Modul($application);

switch ($application) {
	
	case 'mps':
	
		$file->setTable('mps_material_collection_category_files');
		$permission = user::permission(Material_Collection_Category::PERMISSION_EDIT);
		$path = 'mps_material_collection_category_file_path';
		$category = 'mps_material_collection_category_file_collection_id';
	break;

	case 'lps':
	
		$file->setTable('lps_collection_category_files');
		$permission = user::permission('can_edit_lps_collection_categories');
		$path = 'lps_collection_category_file_path';
		$category = 'lps_collection_category_file_collection_id';
	break;
}

$file->read($id);

if ($file->id && $permission) {
	
	$path = $file->data[$path];
	$category = $file->data[$category];
	
	$delete = $file->delete();
	
	if ($delete) {
		file::remove($path);
		Message::request_deleted();
		url::redirect("/$application/$controller/$action/$category");
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$category/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}