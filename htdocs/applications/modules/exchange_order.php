<?php

	class Exchange_Order {

		public $id;
		
		/**
		 * Dataloader
		 * @var array
		 */
		public $data;
		
		/**
		 * Model builder
		 * @return Exchange_Order_Model
		 */
		protected $model;
		
		/**
		 * DB connector
		 * @var string
		 */
		protected $connector;

	
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Exchange_Order_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_ramco_order_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_ramco_order_id'];
			return $this->data;
		}
		
		public function read_from_order_number($number) {
			$this->data = $this->model->read_from_order_number($number);
			$this->id = $this->data['mps_ramco_order_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
	}