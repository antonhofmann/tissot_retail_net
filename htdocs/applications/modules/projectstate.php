<?php 


	class ProjectState {
		
		/**
		 * State ID
		 * 
		 * @var int
		 */
		public $id;
		
		/**
		 * Dataloader
		 * 
		 * @var array
		 */
		public $data;
		
		/**
		 * DB Model
		 * 
		 * @return ProjectStateModel
		 */
		protected $model;
		
		
		public function __construct() {
		
			$this->model = new ProjectStateModel();
		}
		
		public function __get($key) {
			return $this->data["project_state_$key"];
		}
		
		public function read($id) {
				
			$this->id = $id;
				
			return $this->data = $this->model->read($id);
		}
		
		public function create($data) {
				
			if (is_array($data)) {
		
				$this->data = $data;
		
				return $this->id = $this->model->create($data);
			}
		}
		
		public function update($data) {
				
			if ($this->id) {
		
				$this->data = array_merge($this->data, $data);
		
				return $this->model->update($this->id, $data);
			}
		}
		
		public function delete() {
				
			if ($this->id) {
		
				return $this->model->delete($this->id);
			}
		}
	}