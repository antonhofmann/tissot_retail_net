<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

switch ($application) {
	
	case 'mps':
		
	break;

	case 'lps':
		$tableName = 'lps_references';
		$permissionEdit = user::permission('can_edit_lps_materials');
	break;
}

$material = new Modul($application);
$material->setTable($tableName);
$material->read($id);

if ($material->id && $permissionEdit) {
	
	$response = $material->delete();
	
	if ($response) {
		Message::request_deleted();
		url::redirect("/$application/$controller");
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}