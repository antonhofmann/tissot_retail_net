<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['reference_id'];

	$data = array();

	$reference = new Modul($application);

	switch ($application) {
		
		case 'lps':

			// set table name
			$reference->setTable('lps_references');
			
			// collection
			if ($_REQUEST['collection_code']) {
				$data['lps_reference_collection_id'] = $_REQUEST['collection_code'];
			}

			// collection category
			if (isset($_REQUEST['collection_category_code'])) {
				$data['lps_reference_collection_category_id'] = $_REQUEST['collection_category_code'];
			}

			// product group
			if (isset($_REQUEST['product_group_name'])) {
				$data['lps_reference_product_group_id'] = $_REQUEST['product_group_name'];
			}

			// product subgroup
			if (isset($_REQUEST['product_subgroup_name'])) {
				$data['lps_reference_product_subgroup_id'] = $_REQUEST['product_subgroup_name'];
			}

			// product type
			if (isset($_REQUEST['product_type_name'])) {
				$data['lps_reference_product_type_id'] = $_REQUEST['product_type_name'];
			}

			// ean number
			if (isset($_REQUEST['reference_ean_number'])) {
				$data['lps_reference_ean_number'] = $_REQUEST['reference_ean_number'];
			}

			// reference code
			if (isset($_REQUEST['reference_code'])) {
				$data['lps_reference_code'] = $_REQUEST['reference_code'];
			}

			// reference name
			if (isset($_REQUEST['reference_name'])) {
				$data['lps_reference_name'] = $_REQUEST['reference_name'];
			}

			// reference price
			if (isset($_REQUEST['reference_price_point'])) {
				$data['lps_reference_price_point'] = $_REQUEST['reference_price_point'];
			}

			// reference currency
			if (isset($_REQUEST['currency'])) {
				$data['lps_reference_currency_id'] = $_REQUEST['currency'];
			}

			// reference description
			if (isset($_REQUEST['reference_description'])) {
				$data['lps_reference_description'] = $_REQUEST['reference_description'];
			}

			// active reference
			if (in_array('reference_active', $fields)) {
				$data['lps_reference_active'] = ($_REQUEST['reference_active']) ? 1 : 0;
			}

			// reference date
			if (!$id) {
				$data['lps_reference_date'] = date('Y-m-01');
			}

		break;
	}


	if ($data) {

		$reference->read($id);

		if ($reference->id) {
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			$response = $reference->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$response = $id = $reference->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			if ($response && $_REQUEST['redirect']) {
				$redirect = $_REQUEST['redirect']."/$id";
			}
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}



	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));