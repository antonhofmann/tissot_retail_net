<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

$model = new Model($application);

// sql order
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : '
	collection_category_code, 
	collection_code, 
	product_group_name,
	reference_code
';

// sql direction
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

// filters
$fSearch = $_REQUEST['search'] && $_REQUEST['search']<>$translate->search ? $_REQUEST['search'] : null;
$fCategory = $_REQUEST['category'];
$fCollection = $_REQUEST['collection'];
$fProductGroup = $_REQUEST['productgroup'];
$fYear = $_REQUEST['year'];
$fMonth = $_REQUEST['month'];
$fActive = isset($_REQUEST['active']) ? $_REQUEST['active'] : 1;

switch ($application) {
	
	case 'lps':

		$binds = array(
			'LEFT JOIN lps_collection_categories ON lps_collection_categories.lps_collection_category_id = lps_references.lps_reference_collection_category_id',
			'INNER JOIN lps_collections ON lps_collections.lps_collection_id = lps_references.lps_reference_collection_id',
			'INNER JOIN lps_product_groups ON lps_product_groups.lps_product_group_id = lps_references.lps_reference_product_group_id'
		);

		// filter: full text search
		if ($fSearch) {
			$filters['search'] = "(
				lps_collection_code LIKE \"%$fSearch%\" 
				OR lps_collection_category_code LIKE \"%$fSearch%\" 
				OR lps_product_group_name LIKE \"%$fSearch%\" 
				OR lps_reference_code LIKE \"%$fSearch%\" 
				OR lps_reference_name LIKE \"%$fSearch%\" 
				OR lps_reference_price_point LIKE \"%$fSearch%\"
			)";
		}

		// filter: category
		if ($fCategory) {
			$filters['category'] = "lps_reference_collection_category_id = $fCategory";
		}

		// filter: collection
		if ($fCollection) {
			$filters['collection'] = "lps_reference_collection_id = $fCollection";
		}

		// filter: collection
		if ($fProductGroup) {
			$filters['productgroup'] = "lps_reference_product_group_id = $fProductGroup";
		}

		// filter: year
		if ($fYear) {
			$filters['year'] = "YEAR(lps_reference_date) = $fYear";
		}

		// filter: month
		if ($fMonth) {
			$filters['month'] = "MONTH(lps_reference_date) = $fMonth";
		}

		// filter: active
		$filters['active'] = "lps_reference_active = $fActive";

		$result = $model->query("
			SELECT SQL_CALC_FOUND_ROWS DISTINCT
				lps_references.lps_reference_id, 
				lps_collection_categories.lps_collection_category_code AS collection_category_code, 
				lps_collections.lps_collection_code AS collection_code, 
				lps_product_groups.lps_product_group_name AS product_group_name, 
				lps_references.lps_reference_code AS reference_code, 
				lps_references.lps_reference_name AS reference_name, 
				lps_references.lps_reference_price_point AS reference_price_point
			FROM lps_references
		")
		->bind($binds)
		->filter($filters)
		->order($sort, $direction)
		->offset($offset, $rowsPerPage)
		->fetchAll();
		
	break;
}


if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);

	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// toolbox: button print
if ($datagrid && $_REQUEST['print']) {
	$toolbox[] = ui::button(array(
		'id' => 'print',
		'icon' => 'print',
		'href' => $_REQUEST['print'],
		'label' => $translate->print
	));
}

// toolbox: add
if ($_REQUEST['add'])  {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();

switch ($application) {
	
	case 'lps':

		// dropdown: years
		$result = $model->query("
			SELECT DISTINCT 
				YEAR(lps_reference_date) AS value, 
				YEAR(lps_reference_date) AS caption
			FROM lps_references
		")
		->bind($binds)
		->filter($filters)
		->filter('default', 'lps_reference_date > 0')
		->exclude('year')
		->order('caption')
		->fetchAll();

		if ($result) {
			$toolbox[] = ui::dropdown($result, array(
				'name' => 'year',
				'id' => 'year',
				'class' => 'submit',
				'value' => $fYear,
				'caption' => $translate->all_years
			));
		}

		// dropdown: months
		$result = $model->query("
			SELECT DISTINCT 
				MONTH(lps_reference_date) AS value, 
				MONTHNAME(lps_reference_date) AS caption
			FROM lps_references
		")
		->bind($binds)
		->filter($filters)
		->filter('inc', 'lps_reference_date IS NOT NULL')
		->exclude('month')
		->order('value')
		->fetchAll();

		if ($result) {
			$toolbox[] = ui::dropdown($result, array(
				'name' => 'month',
				'id' => 'month',
				'class' => 'submit',
				'value' => $fMonth,
				'caption' => 'All Months'
			));
		}

		// dropdown: collection categories
		$result = $model->query("
			SELECT DISTINCT 
				lps_collection_category_id, 
				lps_collection_category_code 
			FROM lps_references
		")
		->bind($binds)
		->filter($filters)
		->filter('default', 'lps_reference_collection_category_id > 0')
		->exclude('category')
		->order('lps_collection_category_code')
		->fetchAll();

		if ($result) {
			$toolbox[] = ui::dropdown($result, array(
				'name' => 'category',
				'id' => 'category',
				'class' => 'submit',
				'value' => $fCategory,
				'caption' => $translate->all_collection_categories
			));
		}

		// dropdown: collections
		$result = $model->query("
			SELECT DISTINCT 
				lps_collection_id, 
				lps_collection_code 
			FROM lps_references
		")
		->bind($binds)
		->filter($filters)
		->filter('default', 'lps_reference_collection_id > 0')
		->exclude('collection')
		->order('lps_collection_code')
		->fetchAll();

		if ($result) {
			$toolbox[] = ui::dropdown($result, array(
				'name' => 'collection',
				'id' => 'collection',
				'class' => 'submit',
				'value' => $fCollection,
				'caption' => $translate->all_collections
			));
		}

		// dropdown: product groups
		$result = $model->query("
			SELECT DISTINCT 
				lps_product_group_id, 
				lps_product_group_name 
			FROM lps_references
		")
		->bind($binds)
		->filter($filters)
		->filter('default', 'lps_reference_product_group_id > 0')
		->exclude('productgroup')
		->order('lps_product_group_name')
		->fetchAll();

		if ($result) {
			$toolbox[] = ui::dropdown($result, array(
				'name' => 'productgroup',
				'id' => 'productgroup',
				'class' => 'submit',
				'value' => $fProductGroup,
				'caption' => 'All Product Groups'
			));
		}

		$actives = array(
			1 => 'Active',
			0 => 'Inactive'
		);

		$toolbox[] = ui::dropdown($actives, array(
			'name' => 'active',
			'id' => 'active',
			'class' => 'submit',
			'value' => $fActive,
			'caption' => false
		));

	break;
}


if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

$table->datagrid = $datagrid;

$table->collection_category_code(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

$table->collection_code(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

$table->product_group_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

$table->reference_code(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

$table->reference_name(
	Table::PARAM_SORT,
	"href=".$_REQUEST['data']
);

$table->reference_price_point(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);


$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;

	