<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

// execution time
ini_set('max_execution_time', 240);
ini_set('memory_limit', '1024M');

$fileName = 'references-'.date('Y-m-d');

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// request vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

// filters
$fSearch = $_REQUEST['search'] && $_REQUEST['search']<>$translate->search ? $_REQUEST['search'] : null;
$fCategory = $_REQUEST['category'];
$fCollection = $_REQUEST['collection'];
$fProductGroup = $_REQUEST['productgroup'];
$fYear = $_REQUEST['year'];
$fMonth = $_REQUEST['month'];
$fActive = isset($_REQUEST['active']) ? $_REQUEST['active'] : 1;

$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'collection_category_code, collection_code, product_group_name, reference_code';

// db model   
$model = new Model($application);

switch ($application) {

	case 'lps':

		$sheetName = 'Watches and Watch Straps';

		// filter: full text search
		if ($fSearch ) {

			$filters['search'] = "(
				lps_collection_code LIKE \"%$fSearch%\" 
				OR lps_collection_category_code LIKE \"%$fSearch%\" 
				OR lps_product_group_name LIKE \"%$fSearch%\" 
				OR lps_reference_code LIKE \"%$fSearch%\" 
				OR lps_reference_name LIKE \"%$fSearch%\" 
				OR lps_reference_price_point LIKE \"%$fSearch%\"
			)";

			$captions[] = "Search Term: $fSearch";
		}

		// filter: category
		if ($fCategory) {

			$filters['category'] = "lps_reference_collection_category_id = $fCategory";

			$result = $model->query("
				SELECT lps_collection_category_code As caption
				FROM lps_collection_categories
				WHERE lps_collection_category_id = $fCategory
			")->fetch();

			$captions[] = "Collection Category: {$result[caption]}";
		}

		// filter: collection
		if ($fCollection) {

			$filters['collection'] = "lps_reference_collection_id = $fCollection";

			$result = $model->query("
				SELECT lps_collection_code As caption
				FROM lps_collections
				WHERE lps_collection_id = $fCollection
			")->fetch();

			$captions[] = "Collection: {$result[caption]}";
		}

		// filter: product group
		if ($fProductGroup) {
			
			$filters['productgroup'] = "lps_reference_product_group_id = $fProductGroup";

			$result = $model->query("
				SELECT lps_product_group_name As caption
				FROM lps_product_groups
				WHERE lps_product_group_id = $fProductGroup
			")->fetch();

			$captions[] = "Product Group: {$result[caption]}";
		}

		// filter: year
		if ($fYear) {
			$filters['year'] = "YEAR(lps_reference_date) = $fYear";
			$captions[] = "Year: $fYear";
		}

		// filter: month
		if ($fMonth) {
			
			$filters['month'] = "MONTH(lps_reference_date) = $fMonth";

			$result = $model->query("
				SELECT MONTHNAME(STR_TO_DATE($fMonth, '%m')) As caption
			")->fetch();

			$captions[] = "Month: {$result[caption]}";
		}

		// filter: active
		$filters['active'] = "lps_reference_active = $fActive";

		$result = $model->query("
			SELECT SQL_CALC_FOUND_ROWS DISTINCT
				lps_reference_id, 
				lps_collection_category_code AS collection_category_code, 
				lps_collection_code AS collection_code, 
				lps_product_group_name AS product_group_name, 
				lps_reference_code AS reference_code, 
				lps_reference_name AS reference_name, 
				lps_reference_price_point AS reference_price_point
			FROM lps_references
			LEFT JOIN lps_collection_categories ON lps_collection_categories.lps_collection_category_id = lps_references.lps_reference_collection_category_id
			INNER JOIN lps_collections ON lps_collections.lps_collection_id = lps_references.lps_reference_collection_id
			INNER JOIN lps_product_groups ON lps_product_groups.lps_product_group_id = lps_references.lps_reference_product_group_id
		")
		->filter($filters)
		->order($order, $direction)
		->fetchAll();
		
	break;
}

	
if ($result) {
	
	require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
	require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

	$datagrid = _array::datagrid($result);
	$columns = array_keys(end($datagrid));
	$totalColumns = count($columns);

	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle($sheetName);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

	$styles = array(
		'borders' => array(
			'borders' => array(
				'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		),
		'number' => array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
		),
		'string' => array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		),
		'header' => array(
			'font' => array(
				'name'=>'Arial',
				'size'=> 11,
				'bold'=>true
			),
			'alignment' => array(
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'EEEEEE')
			),
			'borders' => array(
				'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		)
	);
	
	// last col
	$lastColumn = "F";

	// column dimensions
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(60);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

	// sheet caption
	$row=1;
	$range = "A$row:".$lastColumn.$row;
	$objPHPExcel->getActiveSheet()->mergeCells($range);
	$objPHPExcel->getActiveSheet()->setCellValue("A$row", $sheetName);
	$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(18);

	// print date
	$row++;
	$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
	$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: ".date('d.m.Y  H:i:s'));

	// separator
	$row++;
	$range = "A$row:".$lastColumn.$row;
	$objPHPExcel->getActiveSheet()->mergeCells($range);

	// filter captions
	if ($captions) {
		
		foreach ($captions as $caption) {
			$row++;
			$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
			$objPHPExcel->getActiveSheet()->setCellValue("A$row", $caption);
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
		}

		$row++;
		$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
	}

	// columns
	$row++;
	$col = 0;

	foreach ($columns as $value) {
		$index = spreadsheet::index($col, $row);
		$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['header']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $translate->$value);
		$col++;
	}

	foreach ($datagrid as $i => $array) {
		
		$row++; $col = 0;
		
		foreach($array as $key => $value) {
			$index = spreadsheet::index($col, $row);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
			$col++;
	    }

	    $objPHPExcel->getActiveSheet()->getStyle("A$row:$lastColumn$row")->applyFromArray($styles['borders']);
	}

	$row++;
	$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");

	$objPHPExcel->getActiveSheet()->setTitle($sheetName);
	$objPHPExcel->setActiveSheetIndex(0);

	// redirect output to client browser
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx" ');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
}
else {
		message::empty_result();
		url::redirect("/$application/$controller$archived/$action");
}
	