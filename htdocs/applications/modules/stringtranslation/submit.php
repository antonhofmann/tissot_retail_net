<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['string_translation_id'];


	$data = array();

	$stringtranslation = new Modul($application);

	// set table name
	$stringtranslation->setTable('string_translations');
	
	if ($_REQUEST['string_translation_keyword']) {
		$data['string_translation_keyword'] = $_REQUEST['string_translation_keyword'];
	}

	if (isset($_REQUEST['string_translation_language_id'])) {
		$data['string_translation_language_id'] = $_REQUEST['string_translation_language_id'];
	}

	if (in_array('string_translation_text', $fields)) {
		$data['string_translation_text'] = $_REQUEST['string_translation_text'];
	}

	
	if ($data) {

		$stringtranslation->read($id);

		if ($stringtranslation->id) {
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			$response = $stringtranslation->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $stringtranslation->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}



	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));