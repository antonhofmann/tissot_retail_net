<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$stringtranslation = new Modul($application);

// set table name
$stringtranslation->setTable('string_translations');

$permission = user::permission('can_administrate_system_data');


$stringtranslation->read($id);

if ($stringtranslation->id && $permission) {
	
	$delete = $stringtranslation->delete();
	
	if ($delete) {
		Message::request_deleted();
		url::redirect("/$application/$controller");
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}