<?php 

	/**
	 * Master Sheet Item
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Mastersheet_Item {
		
		public $id;
		
		/**
		 * Master Sheet ID
		 * @var integer
		 */
		public $mastersheet;
		
		/**
		 * Master Sheet Item ID
		 * @var integer
		 */
		public $item;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @return Mastersheet_Item_Model
		 */
		protected $model;
		
		/**
		 * Join master sheets
		 * @var string
		 */
		const DB_BIND_MASTERSHEETS = 'INNER JOIN mps_mastersheets ON mps_mastersheet_id = mps_mastersheet_item_mastersheet_id';
		
		/**
		 * Join materials
		 * @var string
		 */
		const DB_BIND_MATERIALS = 'INNER JOIN mps_materials ON mps_material_id = mps_mastersheet_item_material_id';
		
		
		public function __construct($connector) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Mastersheet_Item_Model($this->connector);
		}
		
		public function mastersheet($id) {
			$this->mastersheet = $id;
		}
		
		public function read($item) {
			
			if ($this->mastersheet) {
				
				$result = $this->model->read($this->mastersheet, $item);
				
				if ($result) {
					return $this->id = $result['mps_mastersheet_item_id'];
				}
			}
		}
		
		public function create($item) {
			if ($this->mastersheet) {
				return $this->model->create($this->mastersheet, $item);
			}
		}
		
		public function delete($item) {
			if ($this->mastersheet) {
				return $this->model->delete($this->mastersheet, $item);
			}
		}
		
		public function deleteAll() {
			if ($this->mastersheet) {
				return $this->model->deleteAll($this->mastersheet);
			}
		}
		
		/**
		 * Master Sheet Items Loader
		 * @param integer $mastersheet, master sheet id
		 * @param string $connector, database connector
		 * @return NULL
		 */
		public function load($filters=null) {
			if ($this->mastersheet) {
				$result = $this->model->load($this->mastersheet, $filters);
				return ($result) ? _array::datagrid($result) : null;
			}
		}
	}