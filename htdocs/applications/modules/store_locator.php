<?php 

	class Store_Locator {
		
		public function __construct() {
			$this->model = new Store_Locator_Model(Connector::DB_RETAILNET_STORE_LOCATOR);
		}

		public function add_place($data) {
			return $this->model->add_place($data);
		}
		
		public function add_province($data) {
			return $this->model->add_province($data);
		}
		
		public function add_pos_type($postype) {
			return $this->model->add_pos_type($data);
		}
		
		public function get_pos($id) {
			return $this->model->get_pos($id);
		}
		
		public function add_pos($data) {
			$this->model->add_pos($data);
		}
		
		public function update_pos($id, $data) {
			return $this->model->update_pos($id, $data);
		}
		
		public function delete_pos($id) {
			return $this->model->delete_pos($id);
		}
		
		public function pos($id) { 
			
			$pos = new Pos();
			$data = $pos->read($id);
				
			// get pos loacator country id
			$country = new Country();
			$country->read($pos->country);
			$data['posaddress_country'] = $country->store_locator_id;
				
			// get pos name
			$province = new Province();
			$province->read($pos->province_id);
			$data['province_name'] = $province->canton;
			
			$place = $data['posaddress_place_id'];
			$province = $data['posaddress_province_id'];
			$country = $data['posaddress_country'];
			$pos = ($this->get_pos($id)) ? true : false;
			
			// add new place
			if ($country && $place && $province && !$this->model->get_place($place)) {
				$this->add_place(array(
					'place_id' => $place,
					'place_country_id' => $country,
					'place_province_id' => $province,
					'place_name' => $data['posaddress_place']
				));
			} 
			
			// add new province
			if ($country && $province && !$this->model->get_province($province)) {
				$this->add_province(array(
					'province_id' => $province, 
					'province_country_id' => $country, 
					'province_canton' => $data['province_name']
				));
			}
			
			// remove pos from store locator
			if ($data['posaddress_store_closingdate'] || !$data['posaddress_export_to_web']) {
				
				$this->delete_pos($id);
			}
			elseif ($data['posaddress_store_openingdate'] && $data['posaddress_export_to_web']) {

				// pos area
				if ($data['posarea_area'] == 4) $data['posaddress_airportinfo'] = "Airport Landside";
				elseif($data['posarea_area'] == 5) $data['posaddress_airportinfo'] = "Airport Airside";
				
				$array = array(
					'posaddress_id'  => $id,
					'posaddress_name' => $data['posaddress_name'],
					'posaddress_name2' => $data['posaddress_name2'],
					'posaddress_address' => $data['posaddress_address'],
					'posaddress_address2' => $data['posaddress_address2'],
					'posaddress_zip' => $data['posaddress_zip'],
					'posaddress_place_id' => $data['posaddress_place_id'],
					'posaddress_country_id' => $data['posaddress_country'],
					'posaddress_phone' => $data['posaddress_phone'],
					'posaddress_mobile_phone' => $data['posaddress_mobile_phone'],
					'posaddress_email' => ($data['posaddress_email_on_web']) ? $data['posaddress_email']: null,
					'posaddress_website' => $data['posaddress_website'],
					'posaddress_store_postype' => $data['posaddress_store_postype'],
					'posaddress_google_lat' => $data['posaddress_google_lat'],
					'posaddress_google_long' => $data['posaddress_google_long'],
					'posaddress_airportinfo' => $data['posaddress_airportinfo'],
					'posaddress_offers_free_battery' => $data['posaddress_offers_free_battery'],
					'posaddress_show_in_club_jublee_stores' => $data['posaddress_show_in_club_jublee_stores'],
					'posaddress_vipservice_on_web' => $data['posaddress_vipservice_on_web'],
					'posaddress_birthday_vaucher_on_web' => $data['posaddress_birthday_vaucher_on_web'],
					'posaddress_eprepnr' => $data['posaddress_eprepnr']
				);

				
				($pos) ? $this->update_pos($id, $array) : $this->add_pos($array);

				$this->updatePopupStores($id);
			}

			
		}

		public function geodata($id) {
			
			$pos = new Pos(Connector::DB_CORE);
			$pos->read($id);
			
			if ($pos->store_openingdate && $pos->export_to_web) {
				$this->update_pos($id, array(
					'posaddress_google_lat' => $pos->google_lat,
					'posaddress_google_long' => $pos->google_long
				));
			}
		}

		public function pos_opening_hours($id) {
			
			$pos = new Pos();
			$pos->read($id);
			
			$pos_opening_hour = new Pos_Opening_Hour(Connector::DB_CORE);
			$result = $pos_opening_hour->load($id);
			
			$this->model->db->query("
				DELETE FROM posopeninghrs
				WHERE posopeninghr_posaddress_id = $id
			");
			
			if ($result && $pos->store_openingdate && $pos->export_to_web) {
				
				$pos_opening_hour = new Pos_Opening_Hour(Connector::DB_RETAILNET_STORE_LOCATOR);
				
				foreach ($result as $id => $row) {	
					$pos_opening_hour->create(array(
						//'posopeninghr_id' => $id,
						'posopeninghr_posaddress_id' => $row['posopeninghr_posaddress_id'],
						'posopeninghr_weekday_id' => $row['posopeninghr_weekday_id'],
						'posopeninghr_from_openinghr_id' => $row['posopeninghr_from_openinghr_id'],
						'posopeninghr_to_openinghr_id' => $row['posopeninghr_to_openinghr_id']
					));
				}
			}
		}
			
		public function pos_closed_days($id) {
			
			$pos = new Pos();
			$pos->read($id);
			
			$pos_closing_day = new Pos_Closing_Day(Connector::DB_CORE);
			$data = $pos_closing_day->read_from_pos($id); 
			
			$this->model->db->query("
				DELETE FROM posclosinghrs
				WHERE posclosinghr_posaddress_id = $id
			");
			
			if ($data && $pos->store_openingdate && $pos->export_to_web) { 
					
				$posClosingDay = new Pos_Closing_Day(Connector::DB_RETAILNET_STORE_LOCATOR);
				
				$posClosingDay->create(array(
					//'posclosinghr_id' => $data['posclosinghr_id'],
					'posclosinghr_posaddress_id' => $data['posclosinghr_posaddress_id'],
					'posclosinghr_text' => $data['posclosinghr_text']
				));
			} 
		}	
	
		public function customerServices($id) {
			
			$pos = new Pos();
			$pos->read($id);
			
			$this->model->db->exec("
				DELETE FROM posaddress_customerservices
				WHERE posaddress_customerservice_posaddress_id = $id
			");
			
			if ($pos->store_openingdate && $pos->export_to_web) {
				
				$model = new Model(Connector::DB_CORE);
				
				$result = $model->query("
					SELECT *
					FROM posaddress_customerservices
					WHERE posaddress_customerservice_posaddress_id = $id
				")->fetchAll();
				
				if ($result) {
					
					$sth = $this->model->db->prepare("
						INSERT INTO posaddress_customerservices (
							posaddress_customerservice_posaddress_id,
							posaddress_customerservice_customerservice_id
						)
						VALUES (:pos, :service)
					");
					
					foreach ($result as $row) {
						$sth->execute(array(
							//':id' => $row['posaddress_customerservice_id'],
							':pos' => $row['posaddress_customerservice_posaddress_id'],
							':service' => $row['posaddress_customerservice_customerservice_id']
						));
					}
				}
			}
		}
		
		public function update($id) {
			
			// pos data
			$this->pos($id);
			
			// pos opening hourse
			$this->pos_opening_hours($id);
			
			// pos closing days
			$this->pos_closed_days($id);
			
			// customer services
			$this->customerServices($id);
		}


		public function updatePopupStores($pos) {

			$model = new Model(Connector::DB_CORE);

			$sth = $model->db->prepare("
				SELECT 
					posorder_id, 
					posorder_popup_name, 
					posorder_opening_date, 
					posorder_popup_closingdate
				FROM posorders
				WHERE posorder_posaddress = :pos
				AND posorder_project_kind = 8
				AND posorder_popup_name <> ''
				AND posorder_opening_date IS NOT NULL
				AND posorder_opening_date <> '0000-00-00'
			");
			
			$sth->bindParam('pos', $pos, PDO::PARAM_INT);
			$sth->execute();
			$orders = $sth->fetchAll();

			if ($orders) {

				// get current addresses
				$sth = $this->model->db->prepare("
					SELECT 
						poaddress_popup_posorder_id AS id,
						poaddress_popup_posaddress_id AS pos
					FROM poaddress_popups
					WHERE poaddress_popup_posaddress_id = :pos
				");

				$sth->bindParam('pos', $pos, PDO::PARAM_INT);
				$sth->execute();
				$result = $sth->fetchAll();
				$popups = _array::extract($result);

				// insert statement
				$sthInsert = $this->model->db->prepare("
					INSERT INTO poaddress_popups (
						poaddress_popup_posaddress_id, 
						poaddress_popup_posorder_id, 
						poaddress_popup_name, 
						poaddress_popup_openingdate, 
						poaddress_popup_endingdate 
					)
					VALUES (?,?,?,?,?)

				");

				// update statement
				$sthUpdate = $this->model->db->prepare("
					UPDATE poaddress_popups SET
						poaddress_popup_name = ?,
						poaddress_popup_openingdate = ?,
						poaddress_popup_endingdate = ?
					WHERE	
						poaddress_popup_posorder_id = ?

				");
				
				foreach ($orders as $row) {
					
					$order = $row['posorder_id'];

					if ($popups[$order]) {
						$sthUpdate->execute(array(
							$row['posorder_popup_name'],
							$row['posorder_opening_date'],
							$row['posorder_popup_closingdate'],
							$row['posorder_id']
						));
					} 
					else {
						$sthInsert->execute(array(
							$pos,
							$row['posorder_id'],
							$row['posorder_popup_name'],
							$row['posorder_opening_date'],
							$row['posorder_popup_closingdate']
						));
					}
				}
			}

			return true;
		}

		public function getUntranslatedPOSlocations($pos_location_ids = array()) {
			
			$pos_ids = array();
			
			if(count($pos_location_ids) > 0) {
				$filter = " where posaddress_id in (" .  implode(',', $pos_location_ids) . ") ";
				$filter .= " and (loc_posaddress_name = '' or loc_posaddress_name is null) ";
			
			$sth = $this->model->db->prepare("
						SELECT posaddress_id
						from posaddresses
						left join loc_posaddresses on loc_posaddress_posaddress_id = posaddress_id 
						$filter
					");
					$sth->execute();
					return $sth->fetchAll();	
			}
		}
	}