<?php 

	class Salesorders {
		
		public $data;
		
		public function __construct() {
			$this->model = new Salesorders_Model(Connector::DB_RETAILNET_EXCHANGES);
		}
		
		public function __get($key) {
			return $this->data["mps_salesorder_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_salesorder_id'];
			return $this->data;
		}
		
		public function read_from_purchase_order_number($number) {
			$this->data = $this->model->read_from_purchase_order_number($number);
			$this->id = $this->data['mps_salesorder_id'];
			return $this->data;
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
	
	}