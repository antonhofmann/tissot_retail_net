<?php 

	class Role {
		// define role ids here instead of hardcoding them everywhere throughout the code
		const LOGISTICS_COORDINATOR    = 2;
		const BRAND_MANAGER            = 15;
		const RETAIL_MANAGER           = 16;
		const PROJECT_LEADER           = 3;
		const REGIONAL_SALES_MANAGER   = 33;
		const STANDARD_USER            = 4;
		const LOCAL_RETAIL_COORDINATOR = 10;
		const RETAIL_CONTROLLER	       = 78;
		const DESIGN_CONTRACTOR        = 7;
		const DESIGN_SUPERVISOR        = 8;

		const DB_BIND_USER_ROLES = 'INNER JOIN db_retailnet.user_roles ON user_role_role = role_id';
		const DB_BIND_ROLE_APPLICATIONS = 'INNER JOIN db_retailnet.role_applications ON role_application_role = role_id';
		
		public $data;
		
		public function __construct() {
			$this->model = new Role_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["role_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['role_id'];
			return $this->data;
		}
		
		public function readCode($code) {
			$this->data = $this->model->readCode($code);
			$this->id = $this->data['role_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($filters=null) {
			$model = new Role_Model(Connector::DB_CORE);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
	
		public static function get_user_roles($user) {
			$model = new Role_Model(Connector::DB_CORE);
			return $model->get_user_roles($user);
		}
		
		public static function get_application_roles($application) {
			$model = new Role_Model(Connector::DB_CORE);
			$result = $model->get_application_roles($application);
			return _array::extract($result);
		}
	}