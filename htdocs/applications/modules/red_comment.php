<?php

	class Red_Comment {

		const BIND_USER_ID = "INNER JOIN db_retailnet.users ON user_id  = red_comment_owner_user_id";
		const BIND_COMMENT_CATEGORY = "INNER JOIN db_retailnet_red.red_commentcategories ON red_commentcategory_id = red_comment_category_id";

		public $id;
		public $data;

		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new Red_Comment_Model($connector);
		}

		public function __get($key) {
			return $this->data["red_comment_$key"];
		}

		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $id;
			return $this->data;
		}

		public function create($data) {
			return $this->model->create($data);
		}


		public function update($data) {
			if ($this->id) {
				return $this->model->update($this->id, $data);
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}

	    /**
		* Checks if the $user_id is a user of a partner which has access to this comment.
		* (in consideration of access_from and access_until dates)
		* OR if the user is the owner of the comment
		*
		* @param  $user_id
		* @return true or false
		*/
		public function user_has_access($user_id) {
			
			$return_value = false;

			//Check if user is owner
			if ($this->user_is_owner($user_id)) {
				$return_value = true;
			}
			else {
				
				//get all partners with access
				$partners_with_access = $this->model->get_partners_with_access($this->id);

				//look if the user is one of partners users
				if (is_array($partners_with_access)) {
					
					$partner = new Red_Project_Partner();
					
					foreach ($partners_with_access as $partner_id) {
						
						$partner->read($partner_id);
						
						if ($partner->is_user($user_id)) {
							$return_value = true;
							break;
						}
					}
				}
			}
			return $return_value;
		}

		/**
		* Checks if the $user_id is the owner of the comment
		*
		* @param  $user_id
		* @return true or false
		*/
		public function user_is_owner($user_id) {
			return ($this->owner_user_id == $user_id) ? true : false;
		}
	}
