<?php

	class Ordersheet_Warehouse {
		
		/**
		 * Item ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Order Sheet ID
		 * @return Ordersheet
		 */
		public $ordersheet;
		
		/**
		 * Order Sheet Item ID
		 * @var integer
		 */
		public $warehouse;
		
		/**
		 * Item Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Database Model
		 * @return Ordersheet_Warehouse_Model
		 */
		protected $model;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;

		/**
		 * Join company warehouses
		 * @var string
		 */
		const DB_BIND_COMPANY_WAREHOUSES = 'INNER JOIN address_warehouses ON  address_warehouse_id = mps_ordersheet_warehouse_address_warehouse_id';
		
		/**
		 * Join Order Sheets
		 * @var string
		 */
		const DB_BIND_ORDERSHEETS = 'INNER JOIN mps_ordersheets ON  mps_ordersheet_id = mps_ordersheet_warehouse_ordersheet_id';

		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Ordersheet_Warehouse_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_ordersheet_warehouse_$key"];
		}
		
		/**
		 * Set Warehouse Order Sheet
		 * @param int $ordersheet
		 */
		public function ordersheet($ordersheet) {
			$this->ordersheet = $ordersheet;
		}
		
		public function read($id) {
			
			$this->data = $this->model->read($id);
			
			if ($this->data) {
				$this->id = $this->data['mps_ordersheet_warehouse_id'];
				$this->ordersheet = $this->data['mps_ordersheet_warehouse_ordersheet_id'];
				$this->warehouse = $this->data['mps_ordersheet_warehouse_address_warehouse_id'];
				return $this->data;
			}
		}
		
		public function read_from_warehouse($warehouse) {
			
			$this->data = $this->model->read_from_warehouse($this->ordersheet, $warehouse);
			
			if ($this->data) {
				$this->id = $this->data['mps_ordersheet_warehouse_id'];
				$this->ordersheet = $this->data['mps_ordersheet_warehouse_ordersheet_id'];
				$this->warehouse = $this->data['mps_ordersheet_warehouse_address_warehouse_id'];
				return $this->data;
			}
		}
		
		public function create($warehouse) {
			if ($this->ordersheet) {
				$id = $this->model->create($this->ordersheet, $warehouse);
				$this->read($id);
				return $id;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public function delete_from_warehouse($warehouse) {
			if ($this->ordersheet) {
				return $this->model->delete_from_pos($this->ordersheet, $warehouse);
			}
		}
		
		public function delete_all() {
			if ($this->ordersheet) {
				return $this->model->delete_all($this->ordersheet);
			}
		}
		
		public function load($filters=null) { 
			if ($this->ordersheet) { 
				return $this->model->load($this->ordersheet, $filters);
			}
		}
		
		public function getStockReserve() {
			
			if ($this->ordersheet) {
				return $this->model->getStockReserve($this->ordersheet);
			}
		}
		
		/**
		 * Add order sheet stock reserve warehouse
		 */
		public function addStockReserve() { 
			
			$stockReserve = $this->getStockReserve();
			
			if (!$stockReserve['mps_ordersheet_warehouse_id'] && $this->ordersheet) {
				
				$user = user::instance();
				$translate = translate::instance();
				
				$ordersheet = new Ordersheet($this->connector);
				$ordersheet->read($this->ordersheet);
				
				$companyWarehouse = new Company_Warehouse();
				$companyWarehouse->company($ordersheet->address_id);
				
				$stock_reserve = $companyWarehouse->getStockReserve();
				
				if ($stock_reserve['address_warehouse_id']) {
					$warehouse = $stock_reserve['address_warehouse_id'];
				} else {
					$warehouse = $companyWarehouse->create(array(
						'address_warehouse_address_id' => $ordersheet->address_id,
						'address_warehouse_name' => $translate->stock_reserve,
						'address_warehouse_active' => 1,
						'address_warehouse_stock_reserve' => 1,
						'user_created' => $user->login,
						'date_created' => date('Y-m-d')
					));
				}
				
				return $this->model->addStockReserve($this->ordersheet, $warehouse);
			}
		}
	}
	