<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	if($_REQUEST['customer_service_id'] > 0 and $_REQUEST['language_id'])
	{

		$model = new Model();
		$model_store_locator = new Model(CONNECTOR::DB_RETAILNET_STORE_LOCATOR);


		//get customerservices data
		$sth = $model->db->prepare("select customerservice_id, customerservice_text
			from customerservices
			WHERE customerservice_id = ?");
		$response = $sth->execute(array($_REQUEST['customer_service_id']));

		
		if($response == true)
		{
			$customerservice_data = $sth->fetch();

		
			//check if customerservice is in store locator db
			$sth = $model_store_locator->db->prepare("select count(customerservice_id) as num_recs from customerservices
			WHERE customerservice_id = ?");
			$response = $sth->execute(array($customerservice_data['customerservice_id']));
			
			if($response == true)
			{
				$row = $sth->fetch();

				if($row['num_recs'] == 0)
				{
					$query = "Insert into customerservices (customerservice_id, customerservice_text) VALUES (?,?)";

					$sth = $model_store_locator->db->prepare($query);
					$response = $sth->execute(array($customerservice_data['customerservice_id'], $customerservice_data['customerservice_text']));
				}
			}

			
			//check if translation is present in db retail net
			$sth = $model_store_locator->db->prepare("select count(loc_customerservice_id) as num_recs from loc_customerservices
		    WHERE loc_customerservice_customerservice_id = ? and loc_customerservice_language_id = ?");
			$response = $sth->execute(array($_REQUEST['customer_service_id'], $_REQUEST['language_id']));
			if($response == true)
			{
				
				$row = $sth->fetch();
				if($row['num_recs'] == 0)
				{
					$query = "Insert into loc_customerservices (loc_customerservice_customerservice_id, loc_customerservice_language_id, loc_customerservice_text) VALUES (?,?,?)";

					$sth = $model_store_locator->db->prepare($query);
					$response = $sth->execute(array($customerservice_data['customerservice_id'], $_REQUEST['language_id'], $_REQUEST['customer_service_text']));

				}
				else
				{
					$query = "UPDATE loc_customerservices SET 
						loc_customerservice_text = ? 
					WHERE loc_customerservice_customerservice_id = ? and loc_customerservice_language_id = ?";
					
					//update store locator
					$sth = $model_store_locator->db->prepare($query);
					$response = $sth->execute(array($_REQUEST['customer_service_text'], $_REQUEST['customer_service_id'], $_REQUEST['language_id']));
				}
			}
		}
	}

?>