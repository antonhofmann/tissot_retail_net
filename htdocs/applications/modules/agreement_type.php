<?php

	/**
	 * Agreement Type
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Agreement_Type {

		public $data;

		public function __construct() {
			$this->model = new Agreement_Type_Model(Connector::DB_CORE);
		}

		public function __get($key) {
			return $this->data["agreement_type_$key"];
		}

		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['agreement_type_id'];
			return $this->data;
		}

		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}

		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}

		public static function loader($filters) {
			$model = new Aroduct_Type_Model(Connector::DB_CORE);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
	}