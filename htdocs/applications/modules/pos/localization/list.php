<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$permissionEdit = user::permission('can_edit_all_localizations');

$company = New Company();
$company->read($user->address);
$user_country = $company->country;


$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", true);


// sql order
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'province_canton, place_name, posaddress_name';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;



$model = new Model();
$model_store_locator = new Model(CONNECTOR::DB_RETAILNET_STORE_LOCATOR);

//filter country
$fCountry = $_REQUEST['country'];
$default_country = '';
// filer: countries
if ($fCountry) {
	$default_country =  $fCountry;
}
else
{
	$default_country = $user_country;
}


//filter Language
$fLanguage= $_REQUEST['language'];
$default_Language = 0;
// filer: countries
if ($fLanguage) {
	$default_Language =  $fLanguage;
}

//get storelocator_country
$filters2['country'] = "country_id = $default_country";

$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		country_store_locator_id
	FROM countries
")
->filter($filters2)
->order('country_name')
->fetchAll();

$store_locator_country = $result[0]['country_store_locator_id'];


//get language name
if(!$default_Language)
{
	//get first of user's languages
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			language_id, language_name,
			language_from_right_to_left
		FROM languages
		left join country_languages on country_language_language_id = language_id 
	")
	->order("language_name")
	->filter("country_language", "country_language_country_id=" . $default_country)
	->offset(0, 1)
	->fetchAll();


	$default_Language = $result[0]['language_id'];
	$language_name = $result[0]['language_name'];
	$rtl = $result[0]['language_from_right_to_left'];
}
elseif($default_Language)
{
	//get first of user's languages
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			language_id, language_name,
			language_from_right_to_left
		FROM languages
		left join country_languages on country_language_language_id = language_id 
	")
	->order("language_name")
	->filter("country_language", "country_language_country_id=" . $default_country . " and language_id = " . $default_Language)
	->offset(0, 1)
	->fetchAll();

	if($result)
	{
		$default_Language = $result[0]['language_id'];
		$language_name = $result[0]['language_name'];
		$rtl = $result[0]['language_from_right_to_left'];
	}
	else
	{
		//get first of user's languages
		$result = $model->query("
			SELECT SQL_CALC_FOUND_ROWS DISTINCT
				language_id, language_name,
				language_from_right_to_left
			FROM languages
			left join country_languages on country_language_language_id = language_id 
		")
		->order("language_name")
		->filter("country_language", "country_language_country_id=" . $default_country)
		->offset(0, 1)
		->fetchAll();


		$default_Language = $result[0]['language_id'];
		$language_name = $result[0]['language_name'];
		$rtl = $result[0]['language_from_right_to_left'];
	}
}

//echo $model->sql;

//get existing translations
$translations_name = array();
$translations_name2 = array();
$translations_address = array();
$translations_address2 = array();
$translations_zip = array();
$translations_phone = array();
$translations_mobile_phone = array();
$filters3['language'] = "loc_posaddress_language_id = $default_Language and loc_posaddress_country_id = $store_locator_country";

$result = $model_store_locator->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		loc_posaddress_posaddress_id,
		loc_posaddress_language_id, 
		loc_posaddress_name,
		loc_posaddress_name2, 
		loc_posaddress_address, 
		loc_posaddress_address2,
		loc_posaddress_zip,
		loc_posaddress_phone,
		loc_posaddress_mobile_phone
	FROM loc_posaddresses
")
->filter($filters3)
->fetchAll();

//echo $model_store_locator->sql;

if($result)
{
	foreach($result as $key=>$translation)
	{
		$translations_name[$translation['loc_posaddress_posaddress_id']] = $translation['loc_posaddress_name'];
		$translations_name2[$translation['loc_posaddress_posaddress_id']] = $translation['loc_posaddress_name2'];
		$translations_address[$translation['loc_posaddress_posaddress_id']] = $translation['loc_posaddress_address'];
		$translations_address2[$translation['loc_posaddress_posaddress_id']] = $translation['loc_posaddress_address2'];
		$translations_zip[$translation['loc_posaddress_posaddress_id']] = $translation['loc_posaddress_zip'];
		$translations_phone[$translation['loc_posaddress_posaddress_id']] = $translation['loc_posaddress_phone'];
		$translations_mobile_phone[$translation['loc_posaddress_posaddress_id']] = $translation['loc_posaddress_mobile_phone'];
	}
}


//get pos locations
$filters['posaddress_country'] = "posaddress_country_id = $store_locator_country";
$result = $model_store_locator->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		posaddress_id, 
		posaddress_name,
		posaddress_name2,
		posaddress_address,
		posaddress_address2,
		posaddress_zip,
		posaddress_phone,
		posaddress_mobile_phone,
		place_name,
		province_canton
	FROM posaddresses
	LEFT join places on place_id = posaddress_place_id
	LEFT join provinces on province_id = place_province_id
")
->filter($filters)
->order($sort, $direction)
->offset($offset, $rowsPerPage)
->fetchAll();

//echo $model_store_locator->sql;

$datagrid = _array::datagrid($result);
$totalrows = $model_store_locator->totalRows();

if($totalrows > 0)
{
	foreach($datagrid as $posaddress_id=>$fields)
	{
		
		$translated_value = '';
		if(array_key_exists($posaddress_id, $translations_name))
		{
			$translated_value = $translations_name[$posaddress_id];
		}
		$datagrid[$posaddress_id]['posaddress_name_'] = $translated_value;

		if(array_key_exists($posaddress_id, $translations_name))
		{
			$translated_value = $translations_name2[$posaddress_id];
		}
		$datagrid[$posaddress_id]['posaddress_name2_'] = $translated_value;

		if(array_key_exists($posaddress_id, $translations_name))
		{
			$translated_value = $translations_address[$posaddress_id];
		}
		$datagrid[$posaddress_id]['posaddress_address_'] = $translated_value;

		if(array_key_exists($posaddress_id, $translations_name))
		{
			$translated_value = $translations_address2[$posaddress_id];
		}
		$datagrid[$posaddress_id]['posaddress_address2_'] = $translated_value;

		if(array_key_exists($posaddress_id, $translations_name))
		{
			$translated_value = $translations_zip[$posaddress_id];
		}
		$datagrid[$posaddress_id]['posaddress_zip_'] = $translated_value;

		
			
	}


	
	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
	
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
$toolbox[] = "<input type=hidden id=application name=application value='$application' />";



// dropdown countries ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if(user::permission(Pos::PERMISSION_EDIT_LIMITED))
{
	/*
	$country_filters['country'] = "country_id = $default_country";
	$result = $model->query("
		SELECT DISTINCT country_id AS value, country_name AS caption 
		FROM countries
	")
	->filter($country_filters)
	->order('caption')
	->fetchAll();
	*/
	
	$result = $model->query("
		SELECT DISTINCT posaddress_country AS value, country_name AS caption 
		FROM posaddresses 
		LEFT JOIN countries on country_id = posaddress_country 
		LEFT join country_languages on country_language_country_id = posaddress_country
		where posaddress_client_id = " . $user->address .
	    " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') 
		and posaddress_export_to_web = 1
		AND country_language_language_id <> 36
	")
	->order('caption')
	->fetchAll();
	
}
else
{	
	$result = $model->query("
		SELECT DISTINCT country_id AS value, country_name AS caption 
		FROM countries
	")
	->order('caption')
	->fetchAll();
}


if ($result and $permissionEdit == true) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'country',
		'id' => 'country',
		'class' => 'submit',
		'value' => $default_country,
		'caption' => false
	));
}


// dropdown languages ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		language_id, language_name,
		language_from_right_to_left
	FROM languages
	LEFT join country_languages on country_language_language_id = language_id 
")
->order("language_name")
->filter("country_language", "country_language_country_id=" . $default_country)
->fetchAll();


if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'language',
		'id' => 'language',
		'class' => 'submit',
		'value' => $default_Language,
		'caption' => false
	));
}

// toolbox: form
$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";


$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

$table->datagrid = $datagrid;
$table->dataloader($dataloader);



$table->province_canton(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
	
);

$table->place_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
	
);

//POS Name

$table->posaddress_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
	
);


$table->posaddress_name_(
	Table::ATTRIBUTE_NOWRAP,
	Table::DATA_TYPE_TEXTBOX,
	"caption=" . $language_name,
	"data-language_id=" . $default_Language,
	"data-country_id=" . $store_locator_country
);

if($rtl == 1)
{
	$table->attributes("posaddress_name_", array("style"=>"direction:RTL;width:300px;"));
}
else
{
	$table->attributes("posaddress_name_", array("style"=>"width:300px;"));
}

//POS Name2
$table->posaddress_name2(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
	
);

$table->posaddress_name2_(
	Table::ATTRIBUTE_NOWRAP,
	Table::DATA_TYPE_TEXTBOX,
	"caption=" . $language_name,
	"data-language_id=" . $default_Language,
	"data-country_id=" . $store_locator_country
);

if($rtl == 1)
{
	$table->attributes("posaddress_name2_", array("style"=>"direction:RTL;width:300px;"));
}
else
{
	$table->attributes("posaddress_name2_", array("style"=>"width:300px;"));
}

//POS Address

$table->posaddress_address(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
	
);

$table->posaddress_address_(
	Table::ATTRIBUTE_NOWRAP,
	Table::DATA_TYPE_TEXTBOX,
	"caption=" . $language_name,
	"data-language_id=" . $default_Language,
	"data-country_id=" . $store_locator_country
);

if($rtl == 1)
{
	$table->attributes("posaddress_address_", array("style"=>"direction:RTL;width:300px;"));
}
else
{
	$table->attributes("posaddress_address_", array("style"=>"width:300px;"));
}


//POS Address2
$table->posaddress_address2(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
	
);

$table->posaddress_address2_(
	Table::ATTRIBUTE_NOWRAP,
	Table::DATA_TYPE_TEXTBOX,
	"caption=" . $language_name,
	"data-language_id=" . $default_Language,
	"data-country_id=" . $store_locator_country
);

if($rtl == 1)
{
	$table->attributes("posaddress_address2_", array("style"=>"direction:RTL;width:300px;"));
}
else
{
	$table->attributes("posaddress_address2_", array("style"=>"width:300px;"));
}


//POS zip
$table->posaddress_zip(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
	
);

$table->posaddress_zip_(
	Table::ATTRIBUTE_NOWRAP,
	Table::DATA_TYPE_TEXTBOX,
	"caption=" . $language_name,
	"data-language_id=" . $default_Language,
	"data-country_id=" . $store_locator_country
);

if($rtl == 1)
{
	$table->attributes("posaddress_zip_", array("style"=>"direction:RTL;width:80px;"));
}
else
{
	$table->attributes("posaddress_zip_", array("style"=>"width:80px;"));
}




//$table->footer($translate->total_rows . " " . $totalrows);
$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;
