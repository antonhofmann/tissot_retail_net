<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$dbName = Connector::DB_RETAILNET_STORE_LOCATOR;
$db = Connector::get($dbName);

$_POS = $_REQUEST['posaddress_id'];
$_LANGUAGE = $_REQUEST['language_id'];
$_COUNTRY = $_REQUEST['country_id'];

$_SUCCESS = false;
$_ERROR = false;

// check in request
if (!$_POS || !$_LANGUAGE || !$_COUNTRY) {
	$_ERROR = true;
	$_MESSAGE = "Bad request";
	$_REQUEST['section'] = null;
}

// check if translation is present in db retail net
if (!$_ERROR) {
	
	$sth = $db->prepare("
		SELECT loc_posaddress_id
		FROM loc_posaddresses
		WHERE loc_posaddress_posaddress_id = ? AND loc_posaddress_language_id = ? AND loc_posaddress_country_id = ?
	");

	$sth->execute(array($_POS, $_LANGUAGE, $_COUNTRY));
	$result = $sth->fetch();
	
	$_ID = $result['loc_posaddress_id'];
}


switch ($_REQUEST['section']) {
	
	case 'posaddress_name':

		if ($_ID) {
			
			$sth = $db->prepare("
				UPDATE loc_posaddresses SET 
					loc_posaddress_name = ? 
				WHERE loc_posaddress_id = ?
			");

			$_SUCCESS = $sth->execute(array($_REQUEST['posaddress_name'], $_ID));
		}
		else {

			$sth = $db->prepare("
				INSERT INTO loc_posaddresses (
					loc_posaddress_country_id,
					loc_posaddress_posaddress_id,
					loc_posaddress_language_id,
					loc_posaddress_name
				) 
				VALUES (?,?,?,?)
			");

			$_SUCCESS = $sth->execute(array($_COUNTRY, $_POS, $_LANGUAGE, $_REQUEST['posaddress_name']));
		}
		
	break;

	case 'posaddress_name2':

		if ($_ID) {
			
			$sth = $db->prepare("
				UPDATE loc_posaddresses SET 
					loc_posaddress_name2 = ? 
				WHERE loc_posaddress_id = ?
			");

			$_SUCCESS = $sth->execute(array($_REQUEST['posaddress_name2'], $_ID));
		}
		else {

			$sth = $db->prepare("
				INSERT INTO loc_posaddresses (
					loc_posaddress_country_id,
					loc_posaddress_posaddress_id,
					loc_posaddress_language_id,
					loc_posaddress_name2
				) 
				VALUES (?,?,?,?)
			");

			$_SUCCESS = $sth->execute(array($_COUNTRY, $_POS, $_LANGUAGE, $_REQUEST['loc_posaddress_name2']));
		}
		
	break;

	case 'posaddress_address':
		
		if ($_ID) {
			
			$sth = $db->prepare("
				UPDATE loc_posaddresses SET 
					loc_posaddress_address = ? 
				WHERE loc_posaddress_id = ?
			");

			$_SUCCESS = $sth->execute(array($_REQUEST['posaddress_address'], $_ID));
		}
		else {

			$sth = $db->prepare("
				INSERT INTO loc_posaddresses (
					loc_posaddress_country_id,
					loc_posaddress_posaddress_id,
					loc_posaddress_language_id,
					loc_posaddress_address
				) 
				VALUES (?,?,?,?)
			");

			$_SUCCESS = $sth->execute(array($_COUNTRY, $_POS, $_LANGUAGE, $_REQUEST['posaddress_address']));
		}

	break;

	case 'posaddress_address2':
		
		if ($_ID) {
			
			$sth = $db->prepare("
				UPDATE loc_posaddresses SET 
					loc_posaddress_address2 = ? 
				WHERE loc_posaddress_id = ?
			");

			$_SUCCESS = $sth->execute(array($_REQUEST['posaddress_address2'], $_ID));
		}
		else {

			$sth = $db->prepare("
				INSERT INTO loc_posaddresses (
					loc_posaddress_country_id,
					loc_posaddress_posaddress_id,
					loc_posaddress_language_id,
					loc_posaddress_address2
				) 
				VALUES (?,?,?,?)
			");

			$_SUCCESS = $sth->execute(array($_COUNTRY, $_POS, $_LANGUAGE, $_REQUEST['posaddress_address2']));
		}

	break;

	case 'posaddress_zip':
		
		if ($_ID) {
			
			$sth = $db->prepare("
				UPDATE loc_posaddresses SET 
					loc_posaddress_zip = ? 
				WHERE loc_posaddress_id = ?
			");

			$_SUCCESS = $sth->execute(array($_REQUEST['posaddress_zip'], $_ID));
		}
		else {

			$sth = $db->prepare("
				INSERT INTO loc_posaddresses (
					loc_posaddress_country_id,
					loc_posaddress_posaddress_id,
					loc_posaddress_language_id,
					loc_posaddress_zip
				) 
				VALUES (?,?,?,?)
			");

			$_SUCCESS = $sth->execute(array($_COUNTRY, $_POS, $_LANGUAGE, $_REQUEST['posaddress_zip']));
		}

	break;
}
	

if (!$_SUCCESS && $sth) {
	$_MESSAGE = $translate->message_request_failure;
	$_ERROR = $sth->errorInfo();
} else {
	$_MESSAGE = $translate->message_request_submitted;
}

header('Content-Type: text/json');
echo json_encode(array(
	'success' => $_SUCCESS,
	'error' => $_ERROR,
	'message' => $_MESSAGE
));
