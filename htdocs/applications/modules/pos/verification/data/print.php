<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

// execution time
ini_set('max_execution_time', 600);
ini_set('memory_limit', '102400M');

$settings = Settings::init();
$settings->load('data');
$user = User::instance();
$translate = Translate::instance();

// request vars
$_APPLICATION = $_REQUEST['application']; 
$_PERIOD = $_REQUEST['period'];
$_MONTHS = $_REQUEST['months'];
$_ID = $_REQUEST['id'];
$_OPTIONS = $_REQUEST['options'];

// current period
$d = new DateTime(date('Y-m-d'));
$d->modify('first day of this month');
$_CURRENT_PERIOD = $d->format('U'); 

$model = new Model($_APPLICATION);

$_ERRORS = array();
$_RESPONSE = array();

$_PERMISSIONS = array(
	'edit.all' => user::permission(Pos_Verification::PERMISSION_EDIT_ALL),
	'edit.limited' => user::permission(Pos_Verification::PERMISSION_EDIT_LIMITED)
);


$_FILTER_APPLICATION = '';


// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT * 
	FROM posverifications
	WHERE posverification_address_id = ? AND posverification_date = ?
");

$sth->execute(array($_ID, date('Y-m-d', $_PERIOD)));
$result = $sth->fetch(); 
$_VERIFICATION = $result['posverification_id'];


if (!$_VERIFICATION) {
	$_ERRORS[] = "Actual number by all distribution channels are empty. Please fill at least one actual number and try again.";
	goto BLOCK_RESPONSE;
}

$company = new Company();
$company->read($_ID);

if (!$company->id) {
	$_ERRORS[] = "Client not found. Please contact system administrator";
	goto BLOCK_RESPONSE;
}

$sth = $model->db->prepare("
	SELECT CONCAT(address_company, ', ', country_name ) AS company
	FROM posverifications
	INNER JOIN db_retailnet.addresses ON address_id = posverification_address_id
	INNER JOIN db_retailnet.countries ON country_id = address_country
	WHERE posverification_id = ?

");

$sth->execute(array($_VERIFICATION));
$result = $sth->fetch(); 
$_SHEET_HEADER = $result['company'];

// periodes ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PERIODS = array();

$sth = $model->db->prepare("
	SELECT DISTINCT
		posverification_id,
		UNIX_TIMESTAMP(posverification_date) AS period,
		DATE_FORMAT(posverification_date, '%M %Y') AS caption,
		IF (posverification_confirm_date, DATE_FORMAT(posverifications.date_modiefied, '%d.%m.%Y %H:%i:%s'), '') AS submit_date,
		IF (posverification_confirm_user_id > 0, CONCAT(user_firstname, ' ', user_name), '') AS submit_user
	FROM posverifications
	LEFT JOIN db_retailnet.users ON user_id = posverification_confirm_user_id
	WHERE posverification_address_id = ?
	ORDER BY posverification_date DESC	
");

$sth->execute(array($_ID));
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {

		$period = $row['period'];
		$_PERIODS[$period]['name'] = $row['caption'];

		if ($row['submit_user'] && $_MONTHS[$period]) {
			$_SHOW_SUBMITTERS = true;
			$_PERIODS[$period]['submit_date'] = $row['submit_date'];
			$_PERIODS[$period]['submit_user'] = $row['submit_user'];
		}
	}
}

if ($_MONTHS) {

	foreach ($_MONTHS as $period => $value) {
		
		if (!$_PERIODS[$period]) {
			$_PERIODS[$period]['name'] = date('F Y', $period);
		}
	}
}

// current month caption
//$_PERIODS[$_PERIOD] = date('F Y');

$_DATA['periods'] = $_PERIODS;

// current in posindex  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_IN_POSINDEX = array();
$_ACTUAL_NUMBERS = array();
$_TOTAL_WHITHOUT_CHANNELS = array();

if ($_MONTHS[$_CURRENT_PERIOD]) {

	// is current month confirmed
	$sth = $model->db->prepare("
		SELECT posverification_id AS id
		FROM posverifications
		WHERE posverification_address_id  = ? AND UNIX_TIMESTAMP(posverification_date) = ? AND posverification_confirm_date IS NOT NULL
	");

	$sth->execute(array($_ID, $_CURRENT_PERIOD));
	$result = $sth->fetch();
	$_CURRENT_MMONTH_CONFIRMED = $result['id'] ? true : false;
}

if ($_MONTHS[$_CURRENT_PERIOD] && !$_CURRENT_MMONTH_CONFIRMED) {
	
	$sth = $model->db->prepare("
		SELECT
			posaddress_distribution_channel AS channel,
			COUNT(posaddress_id) AS total
		FROM db_retailnet.posaddresses
		LEFT JOIN db_retailnet.addresses AS addresses ON addresses.address_id = posaddress_client_id
		LEFT JOIN db_retailnet.addresses AS parentaddresses ON parentaddresses.address_id = posaddress_franchisee_id
		WHERE (
			posaddress_client_id = :id
			AND addresses.address_active = 1 
			AND addresses.address_type = 1
			AND (
				addresses.address_involved_in_planning = 1 
				OR addresses.address_involved_in_planning_ff = 1
				
			)
			AND (
				posaddress_store_closingdate IS NULL
				OR posaddress_store_closingdate = '0000-00-00'
				OR posaddress_store_closingdate = ''
			)
			AND $_FILTER_APPLICATION
		)
		OR (
			parentaddresses.address_parent = :id
			AND parentaddresses.address_active = 1 
			AND parentaddresses.address_type = 1
			AND (
				posaddress_store_closingdate IS NULL
				OR posaddress_store_closingdate = '0000-00-00'
				OR posaddress_store_closingdate = ''
			)
			AND addresses.address_client_type = 3
			AND $_FILTER_APPLICATION
		)
		GROUP BY posaddress_distribution_channel	
	");

	$sth->execute(array('id' => $_ID));
	$result = $sth->fetchAll();

	if ($result) {

		foreach ($result as $row) {

			$channel = $row['channel'];

			if ($channel) {
				$_IN_POSINDEX[$_CURRENT_PERIOD][$channel] = $row['total'];
			} else {
				$_TOTAL_WHITHOUT_CHANNELS[$_CURRENT_PERIOD] += 1;
			}

			$_TOTAL_IN_POSINDEX += $row['total'];
		}
	}
}


// get actual number :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT
		posverification_data_distribution_channel_id AS channel,
		posverification_data_actual_number AS actual_number,
		posverification_data_in_posindex AS in_posindex,
		UNIX_TIMESTAMP(posverification_date) AS period,
		posverification_confirm_date AS confirm_date,
		posverification_whithout_channels AS whithout_channels
	FROM posverification_data
	INNER JOIN posverifications ON posverification_id = posverification_data_posverification_id
	WHERE posverification_address_id = ?
");

$sth->execute(array($_ID));
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$channel = $row['channel'];
		$period = $row['period'];

		// actual number
		$_ACTUAL_NUMBERS[$channel][$period] = $row['actual_number'];
		
		// in pos index
		// for confirmed verifications or verifications in past
		if ($row['confirm_date'] || ($period <> $_CURRENT_PERIOD) ) {
			$_IN_POSINDEX[$period][$channel] = $row['in_posindex'];
			$_TOTAL_WHITHOUT_CHANNELS[$period] = $row['whithout_channels'];
		}
	}
}


if ($_MONTHS) {

	foreach ($_MONTHS as $period => $value) {
		
		if (!$_IN_POSINDEX[$period]) {

			$periodYear = date('Y', $period);
			$periodMonth = date('m', $period);
				
			$result = $model->query("
				SELECT 
				    mps_distchannels.mps_distchannel_id,
				    COUNT(posaddresses.posaddress_id) AS total
				FROM db_retailnet.posaddresses
				LEFT JOIN db_retailnet.mps_distchannels ON mps_distchannels.mps_distchannel_id = posaddresses.posaddress_distribution_channel
				LEFT JOIN db_retailnet.countries ON posaddresses.posaddress_country = country_id
				LEFT JOIN db_retailnet.addresses ON posaddresses.posaddress_client_id = addresses.address_id
				WHERE
				addresses.address_id = $_ID
				AND (
					YEAR(posaddresses.posaddress_store_openingdate) < $periodYear
					OR ( 
						YEAR(posaddresses.posaddress_store_openingdate) = $periodYear 
						AND MONTH(posaddresses.posaddress_store_openingdate) <= $periodMonth
					)
				)
				AND (
					posaddresses.posaddress_store_closingdate IS NULL
					OR posaddresses.posaddress_store_closingdate = '000-00-00'
		            OR YEAR(posaddresses.posaddress_store_closingdate) > $periodYear
		            OR (
		            	YEAR(posaddresses.posaddress_store_closingdate) = $periodYear
						AND MONTH(posaddresses.posaddress_store_closingdate) > $periodMonth
					)
		        )
				AND $_FILTER_APPLICATION
				GROUP BY addresses.address_id , mps_distchannels.mps_distchannel_id
				ORDER BY countries.country_name , addresses.address_company , mps_distchannels.mps_distchannel_id , mps_distchannels.mps_distchannel_name , mps_distchannels.mps_distchannel_code
			")->fetchAll();

			if ($result) {
					
				foreach ($result as $row) {
					
					$channel = $row['mps_distchannel_id'];

					if ($channel) $_IN_POSINDEX[$period][$channel] = $row['total'];
					else $_TOTAL_WHITHOUT_CHANNELS[$period] += $row['total'];
				}
			}
		}
	}
}


// get distribution channels :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_DISTRIBUTION_CHANNELS = array();

$sth = $model->db->prepare("
	SELECT DISTINCT
		mps_distchannel_id,
		mps_distchannel_groups.mps_distchannel_group_id,
		mps_distchannel_group_name,
		mps_distchannel_group_allow_for_agents,
		mps_distchannel_code,
		mps_distchannel_name
	FROM db_retailnet.mps_distchannels
	INNER JOIN db_retailnet.mps_distchannel_groups ON db_retailnet.mps_distchannels.mps_distchannel_group_id = db_retailnet.mps_distchannel_groups.mps_distchannel_group_id
	WHERE mps_distchannel_active = 1 
	ORDER BY mps_distchannel_group_name, mps_distchannel_name
");

$sth->execute();
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {

		$id = $row['mps_distchannel_id'];
		$permitted = true;
					
		// corporate pos allowed for agents
		if ($company->client_type==1 && $row['mps_distchannel_group_id']==1) {
			$permitted = $row['mps_distchannel_group_allow_for_agents'];
		}

		if ($permitted) {
			
			$key = strtolower(str_replace(' ', '', $row['mps_distchannel_group_name']));			
			$_DISTRIBUTION_CHANNELS[$key]['caption'] = $row['mps_distchannel_group_name'];
			
			$_DISTRIBUTION_CHANNELS[$key]['data'][$id] = array(
				'name' => $row['mps_distchannel_name'],
				'code' => $row['mps_distchannel_code']
			);
		}
	}

} else {
	$_ERRORS[] = "Distribution channels not found. Please contact system administrator";
	goto BLOCK_RESPONSE;
}

if (!$_DISTRIBUTION_CHANNELS) {
	$_ERRORS[] = "No permitted channels. Please contact system administrator";
	goto BLOCK_RESPONSE;
}

$_DATA['distribution.channels'] = $_DISTRIBUTION_CHANNELS;
	
// php excel :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator(settings::init()->project_name)->setLastModifiedBy(settings::init()->project_name) ->setTitle($_PAGE_TITLE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	
$_STYLES['borders'] = array(
	'borders' => array( 
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);

$_STYLES['align-left'] = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
	)
);

$_STYLES['align-center'] = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
	)
);


$_STYLES['bg-total-group'] = array(
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb'=>'f3f7e4')
	)
);

$_STYLES['bg-grand-total'] = array(
	'font' => array(
		'name'=>'Arial',
		'size'=> 12
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb'=>'f3f7e4')
	),
	'alignment' => array(
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
	)
);

$_STYLES['caption'] = array(
	'font' => array(
		'name'=>'Arial',
		'size'=> 12
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
		'wrap'=> true
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb'=>'efefef')
	)
);

$_STYLES['submitter'] = array(
	'font' => array(
		'name'=>'Arial',
		'size'=> 9
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
		'wrap'=> true
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb'=>'efefef')
	)
);

$_STYLES['delta'] = array(
	'font' => array(
		'color' => array('rgb'=>'ff0000')
	)
);


// spreadshee global settings ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
$_TOTAL_OPTION_COLUMNS = count($_OPTIONS);
$_TOTAL_PERIOD_COLUMNS = count($_MONTHS);
$_TOTAL_COLUMNS = 2 + $_TOTAL_PERIOD_COLUMNS*$_TOTAL_OPTION_COLUMNS;
$_LAST_CEL_INDEX = spreadsheet::key($_TOTAL_COLUMNS);
	
// dimensions
$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);


// sheet header ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row=1; $col=0;
$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $_TOTAL_COLUMNS-1, $row);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "POS Verification: $_SHEET_HEADER");
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getFont()->setSize(16);

// periodes
$row++; $col=0;
$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $_TOTAL_COLUMNS-1, $row);

$periodCaptions = array();

foreach ($_MONTHS as $period => $val) {
	$caption = $_PERIODS[$period]['name'];
	$periodCaptions[] = $_PERIODS[$period]['name'];
}

$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "Period(s): ".join(', ', $periodCaptions));

// print date
$row++; $col=0;
$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $_TOTAL_COLUMNS-1, $row);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "Date: ".date('d.m.Y H:i:s'));

// separator
$row++; $col=0;
$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $_TOTAL_COLUMNS-1, $row);


// periods caption :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++; 
$col = 1;

$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);

foreach ($_MONTHS as $period => $caption) {
	$col++;
	$colGroup = $col+$_TOTAL_OPTION_COLUMNS-1;
	$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $colGroup, $row);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $_PERIODS[$period]['name']);
	$col = $colGroup;
}

// border periods
$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':'.$_LAST_CEL_INDEX.$row)->applyFromArray($_STYLES['caption'], false);
$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':'.$_LAST_CEL_INDEX.$row)->applyFromArray($_STYLES['borders'], false);


// submitters ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_SHOW_SUBMITTERS) {

	$row++; 
	$col = 1;

	$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);

	foreach ($_MONTHS as $period => $caption) {
		
		$col++;
		$colGroup = $col+$_TOTAL_OPTION_COLUMNS-1;
		$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $colGroup, $row);
		
		if ($_PERIODS[$period]['submit_user']) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "Submitted by ".$_PERIODS[$period]['submit_user']."\n".$_PERIODS[$period]['submit_date']);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setWrapText(true);
		}
		
		$col = $colGroup;
	}

	$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':'.$_LAST_CEL_INDEX.$row)->applyFromArray($_STYLES['submitter'], false);
	$objPHPExcel->getActiveSheet()->getStyle('C'.$row.':'.$_LAST_CEL_INDEX.$row)->applyFromArray($_STYLES['borders'], false);
}


// distribution channels grid ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_TOTAL_GROUP = array();
$_GRAND_TOTAL = array();


foreach ($_DISTRIBUTION_CHANNELS as $key => $group) {
	
	$row++; 
	$firstGroupRow=$row;
	$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);

	// table header styles
	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':'.$_LAST_CEL_INDEX.$row)->applyFromArray($_STYLES['caption'], false);

	// group caption
	$col=0;
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $group['caption']);
	$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['align-left'], false);

	// code caption
	$col=1;
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "Code");
	$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['align-left'], false);
	
	// table header
	foreach ($_MONTHS as $period => $val) {
		foreach ($_OPTIONS as $option => $optionCaption) {
			$col++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $optionCaption);
		}
	}

	foreach ($group['data'] as $id => $channel) {
		
		$row++; 

		// channel name
		$col=0;
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $channel['name']);

		// channel code
		$col++;
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $channel['code']);

		// datagrid
		foreach ($_MONTHS as $period => $val) {
			
			foreach ($_OPTIONS as $option => $optionCaption) {
				
				$col++;

				$inPosIndex = $_IN_POSINDEX[$period] ? $_IN_POSINDEX[$period][$id] : null;
				$actulNumber = $_ACTUAL_NUMBERS[$id] ? $_ACTUAL_NUMBERS[$id][$period] : null;
				$delta = $inPosIndex - $actulNumber;

				switch ($option) {
					
					case 'posindex':
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $inPosIndex);
						$_TOTAL_GROUP[$key][$col] += $inPosIndex;
					break;

					case 'actual':
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $actulNumber);
						$_TOTAL_GROUP[$key][$col] += $actulNumber;
					break;

					case 'delta':
						if ($delta != 0) {
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $delta);
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['delta'], false);
							$_TOTAL_GROUP[$key][$col] += $delta;
						}
					break;
				}					
			}
		}
	}

	// table row borders
	$objPHPExcel->getActiveSheet()->getStyle('A'.$firstGroupRow.':'.$_LAST_CEL_INDEX.$row)->applyFromArray($_STYLES['borders'], false);
	
	// total group caption
	$row++; $col=0;
	$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $col+1, $row);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, 'Total '.$group['caption']);

	$col=1;

	foreach ($_MONTHS as $period => $val) {
		
		foreach ($_OPTIONS as $option => $optionCaption) {
			
			$col++;
			$value = $_TOTAL_GROUP[$key][$col];
			
			if ($value) {
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
				$_GRAND_TOTAL[$col] += $value;
			}
		}
	}

	// table group style
	$range = 'A'.$row.':'.$_LAST_CEL_INDEX.$row;
	$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($_STYLES['bg-total-group'], false);
	$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($_STYLES['borders'], false);

	$row++; $col=0;
	$index = 'A'.$row.':'.$_LAST_CEL_INDEX.$row;
	$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $_TOTAL_COLUMNS-1, $row);
	$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
}


// locations without pos locations :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++; $col=0;
$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(24);
$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $col+1, $row);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, 'Total Locations without distribution channel or with inactive distribution channel');
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['bg-grand-total'], false);
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['borders'], false);

$col=1;
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['borders'], false);

foreach ($_MONTHS as $period => $val) {
	
	foreach ($_OPTIONS as $option => $optionCaption) {
		
		$col++;
		
		if ($option=='posindex') {
			
			$inPosIndex = $_IN_POSINDEX[$period] ? array_sum(array_values($_IN_POSINDEX[$period])) : null;
			$whithoutChannels = isset($_TOTAL_WHITHOUT_CHANNELS[$period]) ? $_TOTAL_WHITHOUT_CHANNELS[$period] : $inPosIndex - $_GRAND_TOTAL[$col];

			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['bg-grand-total'], false);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['borders'], false);
			
			if ($whithoutChannels) {
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $whithoutChannels);
			}
		}
	}
}

// grand total :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++; $col=0;
$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(40);
$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $col+1, $row);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, 'Grand Total of all POS Locations');

$col=1;

foreach ($_MONTHS as $period => $val) {
	
	foreach ($_OPTIONS as $option => $optionCaption) {
		
		$col++;

		if ($option=='posindex') {
			$inPosIndex = $_IN_POSINDEX[$period] ? array_sum(array_values($_IN_POSINDEX[$period])) : null;
			$whithoutChannels = isset($_TOTAL_WHITHOUT_CHANNELS[$period]) ? $_TOTAL_WHITHOUT_CHANNELS[$period] : $inPosIndex - $_GRAND_TOTAL[$col];
			$totalColumn = $_GRAND_TOTAL[$col] + $whithoutChannels;
		} else {
			$totalColumn = $_GRAND_TOTAL[$col];
		}
		
		if ($totalColumn) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $totalColumn);
		}
	}
}

$range = 'A'.$row.':'.$_LAST_CEL_INDEX.$row;
$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($_STYLES['bg-grand-total'], false);
$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($_STYLES['borders'], false);



// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONSE:

ob_end_clean();

if (!$_ERRORS) {

	$country = new Country();
	$country->read($company->country);

	// pos_verification_[country]_swatch_[year_month]_[systemdate].xlsx
	$countryName = str_replace(' ', '', strtolower($country->name));
	$brandName = 'tissot';
	$filename = "pos_verification_".$countryName."_".$brandName."_".date('Y_m_d').".xlsx";
	
	// file path
	$file = "/data/tmp/$filename";
	$hash = Encryption::url($file);
	$url = "/download/tmp/$hash";
	
	// set active sheet
	$objPHPExcel->getActiveSheet()->setTitle('POS Verification');
	$objPHPExcel->setActiveSheetIndex(0);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save($_SERVER['DOCUMENT_ROOT'].$file);
}

header('Content-Type: text/json');

echo json_encode(array(
	'success' => file_exists($_SERVER['DOCUMENT_ROOT'].$file) ? true : false,
	'errors' => $_ERRORS,
	'file' => $url
));
