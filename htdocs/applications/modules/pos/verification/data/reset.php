<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'];
$_ID = $_REQUEST['id'];
$_PERIOD = $_REQUEST['period'];

$model = new Model($_APPLICATION);


// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT * 
	FROM posverifications
	WHERE posverification_address_id = ? AND posverification_date = ?
");

$sth->execute(array($_ID, date('Y-m-d', $_PERIOD)));
$result = $sth->fetch(); 
$_VERIFICATION = $result['posverification_id'];

if (!$_VERIFICATION) {
	$_ERRORS[] = "Verification not found. Please conatct system administrator";
	goto BLOCK_RESONDING;
}


// confirm verification ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

 $sth = $model->db->prepare("
	UPDATE posverifications SET 
		posverification_confirm_date = NULL,
		posverification_confirm_user_id = NULL,
		posverification_whithout_channels = NULL,
		posverification_deleted = 1, 
		user_modiefied = ?,
		date_modiefied = NOW()
	WHERE posverification_id = ?
");

$response = $sth->execute(array($user->login, $_VERIFICATION));

if (!$response) {
	$_ERRORS[] = "Verification is not deleted. Please conatct system administrator";
	goto BLOCK_RESONDING;
}


// update verification data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

/*
$sth = $model->db->prepare("
	SELECT 
		posverification_data_id
	FROM posverification_data
	WHERE posverification_data_posverification_id = ?
");

$sth->execute(array($_VERIFICATION));
$result = $sth->fetchAll();

$sth = $model->db->prepare("
	UPDATE posverification_data SET 
		posverification_data_actual_number = NULL,
		user_modiefied = ?,
		date_modiefied = NOW()
	WHERE posverification_data_id = ?
");

if ($result) {
	foreach ($result as $row) {
		$sth->execute(array($user->login, $row['posverification_data_id']));
	}
}
*/


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESONDING:

header('Content-Type: text/json');

echo json_encode(array(
	'response' => $response,
	'notification' => array(
		'content' => $response ? $translate->message_request_saved : $translate->message_request_failure,
		'properties' => array(
			'theme' => $response ? 'message' : 'error'
		)
	)
));
