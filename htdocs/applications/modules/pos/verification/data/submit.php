<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'];
$_ID = $_REQUEST['id'];
$_CHANNEL = $_REQUEST['channel'];
$_PERIOD = $_REQUEST['period'];
$_VALUE = $_REQUEST['value'];

$model = new Model($_APPLICATION);


// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT * 
	FROM posverifications
	WHERE posverification_address_id = ? AND posverification_date = ?
");

$sth->execute(array($_ID, date('Y-m-d', $_PERIOD)));
$result = $sth->fetch(); 
$_VERIFICATION = $result['posverification_id'];


if (!$_VERIFICATION) {

	$sth = $model->db->prepare("
		INSERT INTO posverifications (
			posverification_address_id,
			posverification_date,
			user_created
		)
		VALUES (?,?,?)
	");

	$response = $sth->execute(array($_ID, date('Y-m-d', $_PERIOD), $user->login));
	$_VERIFICATION = $response ? $model->db->lastInsertId() : null;
}

if (!$_VERIFICATION) {
	$_ERRORS[] = "Verification not found. Please conatct system administrator";
	goto BLOCK_RESONDING;
}

if (!$_CHANNEL) {
	$_ERRORS[] = "Distribution channel is not defined. Please conatct system administrator";
	goto BLOCK_RESONDING;
}


// submitting ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT *
	FROM posverification_data
	WHERE posverification_data_posverification_id = ? AND posverification_data_distribution_channel_id = ?
");

$sth->execute(array($_VERIFICATION, $_CHANNEL));
$_DATA = $sth->fetch();

if ($_VALUE) { 

	if ($_DATA['posverification_data_id']) {

		 $sth = $model->db->prepare("
			UPDATE posverification_data SET 
				posverification_data_actual_number = ?,
				user_modiefied = ?,
				date_modiefied = NOW()
			WHERE posverification_data_id = ?
		");

		$response = $sth->execute(array($_VALUE, $user->login, $_DATA['posverification_data_id']));

	} else {
		
		$sth = $model->db->prepare("
			INSERT INTO posverification_data ( 
				posverification_data_posverification_id,
				posverification_data_distribution_channel_id,
				posverification_data_actual_number,
				user_created
			)
			VALUES (?,?,?,?)
		");

		$response = $sth->execute(array($_VERIFICATION, $_CHANNEL, $_VALUE, $user->login));
	}

} elseif($_DATA['posverification_data_id']) {

	$sth = $model->db->prepare("
		DELETE FROM posverification_data
		WHERE posverification_data_id = ?
	");
	
	$response = $sth->execute(array($_DATA['posverification_data_id']));
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESONDING:

header('Content-Type: text/json');

echo json_encode(array(
	'response' => $response,
	'notification' => array(
		'content' => $response ? $translate->message_request_saved : $translate->message_request_failure,
		'properties' => array(
			'theme' => $response ? 'message' : 'error'
		)
	)
));
