<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'] ?: 'mps';
$_ID = $_REQUEST['id'];
$_PERIOD = $_REQUEST['period'];

$d = new DateTime(date('Y-m-d'));
$d->modify('first day of this month');
$_CURRENT_MONTH = $d->format('U'); 
$_IS_CURRENT_MONTH = $_CURRENT_MONTH==$_PERIOD ? true : false;

$model = new Model($_APPLICATION);

$_FILTER_APPLICATION = '';


// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$user->id || !$_APPLICATION || !$_ID || !$_PERIOD) {
	$_ERRORS[] = "Bed request.";
	goto BLOCK_RESONDING;
}

$sth = $model->db->prepare("
	SELECT * 
	FROM posverifications
	WHERE posverification_address_id = ? AND posverification_date = ?
");

$sth->execute(array($_ID, date('Y-m-d', $_PERIOD)));
$result = $sth->fetch(); 
$_VERIFICATION = $result['posverification_id'];

if (!$_VERIFICATION) {
	$_ERRORS[] = "Verification not found. Please conatct system administrator";
	goto BLOCK_RESONDING;
}


// get channel data
$_CHANNEL_DATA = array();

$sth = $model->db->prepare("
	SELECT 
		posverification_data_id,
		posverification_data_distribution_channel_id
	FROM posverification_data
	WHERE posverification_data_posverification_id = ?
");

$sth->execute(array($_VERIFICATION));
$result = $sth->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$channel = $row['posverification_data_distribution_channel_id'];
		$_CHANNEL_DATA[$channel] = $row['posverification_data_id'];
	}
}

// get current pos distribution channel states :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_CURRENT_IN_POSINDEX = array();
$_TOTAL_WHITHOUT_CHANNELS = 0;

if ($_IS_CURRENT_MONTH) {
	$sth = $model->db->prepare("
		SELECT
			posaddress_distribution_channel AS channel,
			COUNT(posaddress_id) AS total
		FROM db_retailnet.posaddresses
		LEFT JOIN db_retailnet.addresses AS addresses ON addresses.address_id = posaddress_client_id
		LEFT JOIN db_retailnet.addresses AS parentaddresses ON parentaddresses.address_id = posaddress_franchisee_id
		WHERE (
			posaddress_distribution_channel > 0 
			AND posaddress_client_id = :id
			AND addresses.address_active = 1 
			AND addresses.address_type = 1
			AND (
				addresses.address_involved_in_planning = 1 
				OR addresses.address_involved_in_planning_ff = 1
				
			)
			AND (
				posaddress_store_closingdate IS NULL
				OR posaddress_store_closingdate = '0000-00-00'
				OR posaddress_store_closingdate = ''
			)
			AND $_FILTER_APPLICATION
		)
		OR (
			posaddress_distribution_channel > 0 
			AND parentaddresses.address_parent = :id
			AND parentaddresses.address_active = 1 
			AND parentaddresses.address_type = 1
			AND addresses.address_client_type = 3
			AND (
				posaddress_store_closingdate IS NULL
				OR posaddress_store_closingdate = '0000-00-00'
				OR posaddress_store_closingdate = ''
			)
			AND $_FILTER_APPLICATION
		)
		GROUP BY posaddress_distribution_channel	
	");

	$sth->execute(array('id' => $_ID));
	$result = $sth->fetchAll();

} else {

	$periodYear = date('Y', $_PERIOD);
	$periodMonth = date('m', $_PERIOD);

	$sth = $model->db->prepare("
		SELECT 
			mps_distchannels.mps_distchannel_id AS channel,
			COUNT(posaddresses.posaddress_id) AS total
		FROM db_retailnet.posaddresses
		LEFT JOIN db_retailnet.mps_distchannels ON mps_distchannels.mps_distchannel_id = posaddresses.posaddress_distribution_channel
		LEFT JOIN db_retailnet.countries ON posaddresses.posaddress_country = country_id
		LEFT JOIN db_retailnet.addresses ON posaddresses.posaddress_client_id = addresses.address_id
		WHERE
		addresses.address_id = :id
		AND (
			YEAR(posaddresses.posaddress_store_openingdate) < $periodYear
			OR ( 
				YEAR(posaddresses.posaddress_store_openingdate) = $periodYear 
				AND MONTH(posaddresses.posaddress_store_openingdate) <= $periodMonth
			)
		)
		AND (
			posaddresses.posaddress_store_closingdate IS NULL
			OR posaddresses.posaddress_store_closingdate = '000-00-00'
			OR YEAR(posaddresses.posaddress_store_closingdate) > $periodYear
			OR (
				YEAR(posaddresses.posaddress_store_closingdate) = $periodYear
				AND MONTH(posaddresses.posaddress_store_closingdate) > $periodMonth
			)
		)
		 AND $_FILTER_APPLICATION
		GROUP BY addresses.address_id, mps_distchannels.mps_distchannel_id
		ORDER BY countries.country_name , addresses.address_company , mps_distchannels.mps_distchannel_id , mps_distchannels.mps_distchannel_name , mps_distchannels.mps_distchannel_code
	");

	$sth->execute(array('id' => $_ID));
	$result = $sth->fetchAll();	
}

if ($result) {

	$insert = $model->db->prepare("
		INSERT INTO posverification_data (
			posverification_data_posverification_id,
			posverification_data_distribution_channel_id,
			posverification_data_in_posindex,
			user_created,
			date_created
		)
		VALUES (?,?,?,?,NOW())
	");

	$update = $model->db->prepare("
		UPDATE posverification_data SET 
			posverification_data_in_posindex = ?,
			user_modiefied = ?,
			date_modiefied = NOW()
		WHERE posverification_data_id = ?
	");
	
	$exitisng_channels = array();
	
	foreach ($result as $row) {
		
		$totalPos = $row['total'];
		$channel = $row['channel'];
		$exitisng_channels[] = $channel;
		
		if ($channel) {

			// update verification data
			if ($_CHANNEL_DATA[$channel]) {
				$id = $_CHANNEL_DATA[$channel];
				$update->execute(array($totalPos, $user->login, $id));
			}
			// insert verification data
			else {
				$insert->execute(array($_VERIFICATION, $channel, $totalPos, $user->login));
			}
		}
		else $_TOTAL_WHITHOUT_CHANNELS += $totalPos;
	}


	//Fix
	//this fix is needed to delete all existing data with channels not being present anymor in the POS Index
	//this can happen if a user changes the channels in a pos deletes the confirmations and confirms it again.

	if(count($exitisng_channels) > 0)
	{
		$delete = $model->db->prepare("
			DELETE from posverification_data
			WHERE posverification_data_posverification_id = ? 
			AND posverification_data_distribution_channel_id not in ( " . implode(',', $exitisng_channels) . ")
		");

		$delete->execute(array($_VERIFICATION));
	}
}


// confirm verification ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

 $sth = $model->db->prepare("
	UPDATE posverifications SET 
		posverification_confirm_date = ?,
		posverification_confirm_user_id = ?,
		posverification_whithout_channels = ?,
		user_modiefied = ?,
		date_modiefied = NOW()
	WHERE posverification_id = ?
");

$response = $sth->execute(array(
	date('Y-m-d', $_PERIOD), 
	$user->id, 
	$_TOTAL_WHITHOUT_CHANNELS,
	$user->login,
	$_VERIFICATION
));

if ($response) {

	Message::add(array(
		'type' => 'message',
		'content' => $translate->message_request_saved,
		'life' => 5000
	));
	
	$mail = new ActionMail(48);

	$mail->setSender($user->id);

	$mail->setParam('id', $_ID);
	$mail->setParam('period', $_PERIOD);
	$mail->setParam('application', $_APPLICATION);

	$company = new Company();
	$company->read($_ID);

	$country = new Country();
	$country->read($company->country);

	$mail->setDataloader(array(
		'address_company' => $company->name,
		'country' => $country->name,
		'year' => date('Y', $_PERIOD),
		'month' => date('F', $_PERIOD),
		'type' => 'tissot'
	));

	$mail->send();

	$console = $mail->getConsole();

} else {
	$_ERRORS[] = "Verification not confirmed. Please conatct system administrator";
	goto BLOCK_RESONDING;
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESONDING:

header('Content-Type: text/json');

echo json_encode(array(
	'response' => $response,
	'console' => $console,
	'notification' => array(
		'content' => $response ? $translate->message_request_saved : $translate->message_request_failure,
		'properties' => array(
			'theme' => $response ? 'message' : 'error'
		)
	)
));
