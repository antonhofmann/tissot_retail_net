<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'];
$_CONTROLLER = $_REQUEST['controller'];
$_ARCHIVED = $_REQUEST['archived'];
$_ACTION = $_REQUEST['action'];

$_REQUEST = session::filter($_APPLICATION, "$_CONTROLLER.$_ARCHIVED.$_ACTION", true);
$_PERIOD = $_REQUEST['periodes'];
$_REGION = $_REQUEST['regions'];
$_STATE = $_REQUEST['states'];
$_ADDRESS_TYPE = $_REQUEST['addresstypes'];
$_LEGAL_TYPE = $_REQUEST['legaltypes'];
$_CLIENT = $_REQUEST['clients'];

// db model
$model = new Model($_APPLICATION);


// calendar vars
$d = new DateTime(date('Y-m-d'));
$d->modify('first day of this month');
$_CURRENT_MONTH = $d->format('U'); 
$_IS_CURRENT_MONTH = $_CURRENT_MONTH==$_PERIOD ? true : false;

$_COMPANIES = array();
$_ACTUAL_NUMBERS = array();
$_IN_POS_INDEX = array();
$_WHITHOUT_CHANNELS = array();
$_CONFIRMED_VERIFICATIONS = array();

// global filters ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_FILTER_APPLICATION = ''

$_FILTER_ADDRESS_TYPE =  $_ADDRESS_TYPE 
	? " AND addresses.address_client_type = $_ADDRESS_TYPE"
	: " AND addresses.address_client_type IN (1, 2, 3)";

$_FILTER_REGION = $_REGION ? " AND country_region = $_REGION" : null;

$_FILTER_CLIENT = $_CLIENT ? " AND (address_id = $_CLIENT OR address_parent = $_CLIENT)" : null;
	
	
// verificvation data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sql = "
	SELECT DISTINCT
		posverification_data_id,
		posverification_data_distribution_channel_id,
		posverification_data_actual_number,
		posverification_data_in_posindex,
		posverification_address_id,
		posverification_whithout_channels,
		addresses.address_id,
		addresses.address_parent,
		posverification_confirm_date
	FROM posverification_data	
	INNER JOIN posverifications ON posverification_id = posverification_data_posverification_id 
	INNER JOIN db_retailnet.addresses ON address_id = posverification_address_id 
	LEFT JOIN db_retailnet.posaddresses ON posaddress_client_id = posverification_address_id 
	LEFT JOIN db_retailnet.addresses AS clients ON clients.address_id = posaddress_client_id 
	LEFT JOIN db_retailnet.countries on country_id = addresses.address_country 
	WHERE UNIX_TIMESTAMP(posverification_date) = ?
		$_FILTER_APPLICATION
		$_FILTER_REGION
";

if ($_ADDRESS_TYPE) {
	$sql .= " AND clients.address_client_type = $_ADDRESS_TYPE";
	$sql .= " AND ( clients.address_involved_in_planning = 1 OR clients.address_involved_in_planning_ff = 1 )";
}

$sth = $model->db->prepare($sql);
$sth->execute(array($_PERIOD));
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$key = $row['address_parent'];
		$channel = $row['posverification_data_distribution_channel_id'];
		
		$permitted = true;
		
		if ($_STATE) {
			if ($_STATE==1) $permitted = $row['posverification_confirm_date'] ? true : false;
			if ($_STATE==2) $permitted = $row['posverification_confirm_date'] ? false : true;
		}
		
		if ($permitted) {

			$_ACTUAL_NUBERS[$channel][$key] += $row['posverification_data_actual_number'];
			
			if ($row['posverification_data_in_posindex'] > 0 && ($row['posverification_confirm_date'] || !$_IS_CURRENT_MONTH) ) {
				$_IN_POS_INDEX[$channel][$key] = $row['posverification_data_in_posindex'];
			}
			
			if (!$_IS_CURRENT_MONTH) {
				$_WHITHOUT_CHANNELS[$key] = $row['posverification_whithout_channels'];
			}
		}
		
		// confirmed verification
		$_CONFIRMED_VERIFICATIONS[$key] = $row['posverification_confirm_date'] ? true : false;
	}
}


// not confirmed verifications for current month :::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_IS_CURRENT_MONTH)	{
	
	$sql = "
		SELECT DISTINCT
			posaddress_id,
			addresses.address_id,
			addresses.address_parent,
			posaddress_client_id,
			posaddress_distribution_channel
		FROM db_retailnet.posaddresses
		LEFT JOIN db_retailnet.addresses as addresses ON addresses.address_id = posaddress_client_id
		LEFT JOIN db_retailnet.addresses as parentaddresses ON parentaddresses.address_id = posaddress_franchisee_id
		LEFT JOIN db_retailnet.countries on country_id = addresses.address_country
		WHERE (
			addresses.address_active = 1 
			AND addresses.address_type = 1
			AND (
				addresses.address_involved_in_planning = 1 
				OR addresses.address_involved_in_planning_ff = 1
			)
			AND (
				posaddress_store_closingdate IS NULL
				OR posaddress_store_closingdate = '0000-00-00'
				OR posaddress_store_closingdate = ''
			)
			$_FILTER_APPLICATION
			$_FILTER_REGION
			$_FILTER_ADDRESS_TYPE
		)
	";

	if (!$_ADDRESS_TYPE) {
		
		$sql .= " OR (
			parentaddresses.address_active = 1 
			AND parentaddresses.address_type = 1
			AND addresses.address_client_type = 3
			AND (
				posaddress_store_closingdate IS NULL
				OR posaddress_store_closingdate = '0000-00-00'
				OR posaddress_store_closingdate = ''
			)
			$_FILTER_APPLICATION
			$_FILTER_REGION
		)";
	}

	$sth = $model->db->prepare($sql);
	$sth->execute();
	$result = $sth->fetchAll();
		
	if ($result) {
		
		foreach ($result as $row) {
			
			$key = $row['address_parent'];
			$channel = $row['posaddress_distribution_channel'];
			$permitted = true;
			
			if ($_STATE) {
				if ($_STATE==1) $permitted = $_CONFIRMED_VERIFICATIONS[$key] ? true : false;
				if ($_STATE==2) $permitted = $_CONFIRMED_VERIFICATIONS[$key] ? false : true;
			}
			
			if ($permitted) {

				// count distribution channel for each company separated
				if (!$_CONFIRMED_VERIFICATIONS[$key]) {
					if ($channel) $_IN_POS_INDEX[$channel][$key] += 1;
					else $_WHITHOUT_CHANNELS[$key] += 1;
				}
			}
		}
	}
}


// clients :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$sql = "
	SELECT DISTINCT
		address_id,
		address_parent,
		address_company,
		country_name
	FROM db_retailnet.addresses
	LEFT JOIN db_retailnet.countries on country_id = address_country
	WHERE address_active = 1
		AND address_type = 1
		AND (
			address_involved_in_planning = 1 
			OR address_involved_in_planning_ff = 1
		)
		$_FILTER_ADDRESS_TYPE
		$_FILTER_REGION
		$_FILTER_CLIENT
	ORDER BY country_name, address_company
";

$sth = $model->db->prepare($sql);
$sth->execute();
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$key = $row['address_parent'];
		$permitted = true;

		if ($_STATE) {
			if ($_STATE==1) $permitted = $_CONFIRMED_VERIFICATIONS[$key] ? true : false;
			if ($_STATE==2) $permitted = $_CONFIRMED_VERIFICATIONS[$key] ? false : true;
		}
		
		if ($permitted) {
			$_COMPANIES[$key]['country_name'] = $row['country_name'];
			$_COMPANIES[$key]['address_company'] = $row['address_company'];
		}
	}

} else {
	$_ERRORS[] = "Distribution channels not found. Please contact system administrator";
	goto BLOCK_RESPNDING;
}


// distribution channels :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT DISTINCT
		mps_distchannel_id,
		mps_distchannel_group_name,
		mps_distchannel_code,
		mps_distchannel_name
	FROM db_retailnet.mps_distchannels
	INNER JOIN db_retailnet.mps_distchannel_groups ON mps_distchannels.mps_distchannel_group_id = mps_distchannel_groups.mps_distchannel_group_id	
	WHERE mps_distchannel_active = 1 
	ORDER BY mps_distchannel_group_name, mps_distchannel_name
");

$sth->execute();
$result = $sth->fetchAll();

$_DISTRIBUTION_CHANNELS = array();
$_CHANNELS = array();

if ($result) {

	foreach ($result as $row) {
		
		$channel = $row['mps_distchannel_id'];
		$key = strtolower(str_replace(' ', '', $row['mps_distchannel_group_name']));

		$_CHANNELS[$channel] = $row['mps_distchannel_code'];
		
		// group name
		$_DISTRIBUTION_CHANNELS[$key]['group'] = $row['mps_distchannel_group_name'];
		
		// distribution channel
		$_DISTRIBUTION_CHANNELS[$key]['channels'][$channel] = array(
			'mps_distchannel_name' => $row['mps_distchannel_name'],
			'mps_distchannel_code' => $row['mps_distchannel_code']
		);
	}
} else {
	$_ERRORS[] = "Companies not found. Please contact system administrator";
	goto BLOCK_RESPNDING;
}



// get data from passt :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && !$_IS_CURRENT_MONTH) {

	$parents = array();

	foreach ($_CHANNELS as $channel => $name) {
		foreach ($_COMPANIES as $id => $company) {
			if (!$_IN_POS_INDEX[$channel][$id]) {
				$parents[] = $id;
			}
		}
	}

	if ($parents) {

		$parents = join(',', array_filter($parents));

		$periodYear = date('Y', $_PERIOD);
		$periodMonth = date('m', $_PERIOD);
			
		$result = $model->query("
			SELECT 
			    addresses.address_parent,
			    mps_distchannels.mps_distchannel_id,
			    COUNT(posaddresses.posaddress_id) AS total
			FROM db_retailnet.posaddresses
			LEFT JOIN db_retailnet.mps_distchannels ON mps_distchannels.mps_distchannel_id = posaddresses.posaddress_distribution_channel
			LEFT JOIN db_retailnet.countries ON posaddresses.posaddress_country = country_id
			LEFT JOIN db_retailnet.addresses ON posaddresses.posaddress_client_id = addresses.address_id
			WHERE
			addresses.address_parent IN ($parents)
			AND (
				YEAR(posaddresses.posaddress_store_openingdate) < $periodYear
				OR ( 
					YEAR(posaddresses.posaddress_store_openingdate) = $periodYear 
					AND MONTH(posaddresses.posaddress_store_openingdate) <= $periodMonth
				)
			)
			AND (
				posaddresses.posaddress_store_closingdate IS NULL
				OR posaddresses.posaddress_store_closingdate = '000-00-00'
	            OR YEAR(posaddresses.posaddress_store_closingdate) > $periodYear
	            OR (
	            	YEAR(posaddresses.posaddress_store_closingdate) = $periodYear
					AND MONTH(posaddresses.posaddress_store_closingdate) > $periodMonth
				)
	        )
			$_FILTER_APPLICATION
			GROUP BY addresses.address_id , mps_distchannels.mps_distchannel_id
			ORDER BY countries.country_name , addresses.address_company , mps_distchannels.mps_distchannel_id , mps_distchannels.mps_distchannel_name , mps_distchannels.mps_distchannel_code
		")->fetchAll();

		if ($result) {
				
			foreach ($result as $row) {

				$key = $row['address_parent'];
				$channel = $row['mps_distchannel_id'];

				if (!$_CONFIRMED_VERIFICATIONS[$key]) {
					if ($channel) $_IN_POS_INDEX[$channel][$key] = $row['total'];
					else $_WHITHOUT_CHANNELS[$key] += $row['total'];
				}
			}
		}
	}
}

// datagrid ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$_DATAGRID = array();

$_TOTAL_FIXED_COLUMNS=5;
$_TOTAL_OPTIONS = 3;


// first blank row :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row=1; // (jquery plugin bug on cel merge)


// spreadsheet header ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++;

// column: over all countries
$col=3;
$_DATAGRID[$row][$col] = array(
	'text' => "Over all Countries",
	'colspan' => $_TOTAL_OPTIONS,
	'cls' => 'cel country all-countries'
);

$col=$col+$_TOTAL_OPTIONS; // separator

foreach ($_COMPANIES as $key => $company) {
	
	$confirmed = $_CONFIRMED_VERIFICATIONS[$key] ? 'confirmed' : null;

	$col++;
	
	// row countries
	$_DATAGRID[$row][$col] = array(
		'text' => $company['address_company'],
		'cls' => "cel country $confirmed",
		'colspan' => $_TOTAL_OPTIONS
	);

	// separaor
	$col = $col+$_TOTAL_OPTIONS; 
	$_DATAGRID[$row][$col] = array(
		'cls' => 'sep'
	);

	$_DATAGRID[1][$col] = array(
		'cls' => 'sep'
	);
}	


// fixed row area separator ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++;

// column: actual number (over all countries)
$col = $_TOTAL_FIXED_COLUMNS+1;
$_DATAGRID[$row][$col] = array(
	'cls' => 'sep'
);

	
// datagrid ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$_SUM_GROUP = array();
$_SUM_TOTAL = array();

foreach ($_DISTRIBUTION_CHANNELS as $group => $channels) {
	

	// group header ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$row++;

	$col=1;
	$_DATAGRID[$row][$col] = array(
		'id' => $group,
		'text' => $channels['group'],
		'cls' => 'cel group'
	);
	
	// column: distribution code
	$col++;
	$_DATAGRID[$row][$col] = array(
		'text' => translate::instance()->code,
		'cls' => 'cel group'
	);
	
	// column: in pos index (over all countries)
	$col++;
	$_DATAGRID[$row][$col] = array(
		'text' => 'POS',//translate::instance()->in_pos_index,
		'cls' => 'cel group text-center'
	);
	
	// column: actual number (over all countries)
	$col++;
	$_DATAGRID[$row][$col] = array(
		'text' => translate::instance()->posverification_data_actual_number,
		'cls' => 'cel group text-center'
	);
	
	// column: delta (over all countries)
	$col++;
	$_DATAGRID[$row][$col] = array(
		'text' => translate::instance()->delta,
		'cls' => 'cel group text-center'
	);

	$col++; // separator

	// country names
	foreach ($_COMPANIES as $key => $company) {
		
		$col++;
		
		$_DATAGRID[$row][$col] = array(
			'text' => ucfirst(strtolower($company['country_name'])),
			'cls' => 'cel group country',
			'colspan' => $_TOTAL_OPTIONS
		);
			
		$col=$col+$_TOTAL_OPTIONS;
	}
		
	// distribution channel ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	foreach ($channels['channels'] as $id => $channel) {

		$row++;


		$actualNumber = $_ACTUAL_NUBERS[$id] ? array_sum(array_values($_ACTUAL_NUBERS[$id])) : null;
		$inPosIndex = $_IN_POS_INDEX[$id] ? array_sum(array_values($_IN_POS_INDEX[$id])) : null;
		$delta = $actualNumber-$inPosIndex;
		
		$col=1;
		$_DATAGRID[$row][$col] = array(
			'id' => $id,
			'text' => $channel['mps_distchannel_name'],
			'cls' => 'cel channel'
		);
		
		$col++;
		$_DATAGRID[$row][$col] = array(
			'text' => $channel['mps_distchannel_code'],
			'cls' => 'cel'
		);
		
		$col++;
		$_SUM_GROUP[$group][$col] += $inPosIndex;
		$_DATAGRID[$row][$col] = array(
			'text' => $inPosIndex ?: null,
			'cls' => 'cel'
		);
		
		$col++;
		$_SUM_GROUP[$group][$col] += $actualNumber;
		$_DATAGRID[$row][$col] = array(
			'text' => $actualNumber ?: null,
			'cls' => 'cel'
		);
		
		$col++;
		$_SUM_GROUP[$group][$col] += $delta;
		$_DATAGRID[$row][$col] = array(
			'text' => $delta ?: null,
			'cls' => "cel alert"
		);
				
		$col++; // separator
			
		foreach ($_COMPANIES as $key => $company) {

			// total company distribution channels
			$col++;
			$inPosIndex = $_IN_POS_INDEX[$id] ? $_IN_POS_INDEX[$id][$key] : null;
			$_SUM_GROUP[$group][$col] += $inPosIndex;
			$_DATAGRID[$row][$col] = array(
				'text' => $inPosIndex ?: null,
				'cls' => 'cel'
			);
			
			// total company actual numbers
			$col++;
			$actualNumber = $_ACTUAL_NUBERS[$id] ? $_ACTUAL_NUBERS[$id][$key] : null;
			$_SUM_GROUP[$group][$col] += $actualNumber;
			$_DATAGRID[$row][$col] = array(
				'text' => $actualNumber ?: null,
				'cls' => 'cel'
			);
			
			// total company actual numbers
			$col++;
			$delta = $inPosIndex - $actualNumber;
			$_SUM_GROUP[$group][$col] += $delta;
			$_DATAGRID[$row][$col] = array(
				'text' => $delta ?: null,
				'cls' => "function(value) { return value==0 ? 'cel' : 'cel alert' }"
			);
			
			$col++; // separator
		}
	}

	// group total :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
	$row++;

	// total group caption
	$col=1;
	$_DATAGRID[$row][$col] = array(
		'text' => "Total ".$channels['group'],
		'cls' => "cel total-group",
		'colspan' => 2
	);

	$col++;  // merget cell
		
	// total group in pos index
	$col++;
	$_SUM_TOTAL[$col] += $_SUM_GROUP[$group][$col];
	$_DATAGRID[$row][$col] = array(
		'text' => $_SUM_GROUP[$group][$col] ? : null,
		'cls' => 'cel total-group'
	);

	// total group actual number
	$col++;
	$_SUM_TOTAL[$col] += $_SUM_GROUP[$group][$col];
	$_DATAGRID[$row][$col] = array(
		'text' => $_SUM_GROUP[$group][$col] ?: null,
		'cls' => 'cel total-group'
	);
		
	// total group delta
	$col++;
	$_SUM_TOTAL[$col] += $_SUM_GROUP[$group][$col];
	$_DATAGRID[$row][$col] = array(
		'text' => $_SUM_GROUP[$group][$col] ?: null,
		'cls' => 'cel total-group alert'
	);
		
	$col++; // separator

	foreach ($_COMPANIES as $key => $company) {

		$col++;
		$_SUM_TOTAL[$col] += $_SUM_GROUP[$group][$col];
		$_DATAGRID[$row][$col] = array(
			'text' => $_SUM_GROUP[$group][$col] ?: null,
			'cls' => 'cel total-group'
		);
		
		$col++;
		$_SUM_TOTAL[$col] += $_SUM_GROUP[$group][$col];
		$_DATAGRID[$row][$col] = array(
			'text' => $_SUM_GROUP[$group][$col] ?: null,
			'cls' => 'cel total-group'
		);
	
		$col++;
		$_SUM_TOTAL[$col] += $_SUM_GROUP[$group][$col];
		$_DATAGRID[$row][$col] = array(
			'text' => $_SUM_GROUP[$group][$col] ?: null,
			'cls' => "cel total-group alert"
		);
		
		$col++; // col separator
	}

	$row++; // row group separator
}


// pos whithout distribution channels ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++;

$col=1;
$_DATAGRID[$row][$col] = array(
	'text' => "POS Locations whithout distribution channel or with inactive distribution channel",
	'cls' => "cel grand-total",
	'colspan' => 2
);

$col++; // merget cel

$col++;
$totalPosWithoutChannels = array_sum(array_values($_WHITHOUT_CHANNELS));
$_DATAGRID[$row][$col] = array(
	'text' => $totalPosWithoutChannels,
	'cls' => 'cel grand-total'
);

$col = $_TOTAL_FIXED_COLUMNS+1;

foreach ($_COMPANIES as $key => $company) {

	$col++;
			
	$_DATAGRID[$row][$col] = array(
		'text' => $_WHITHOUT_CHANNELS[$key] ?: null,
		'cls' => 'cel grand-total'
	);
	
	$col=$col+$_TOTAL_OPTIONS;
}

// grand total :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++;

$col=1;
$_DATAGRID[$row][$col] = array(
	'text' => "Grand Total of POS Locations",
	'cls' => "cel grand-total",
	'colspan' => 2
);

$col++; // merged cel

// grand total pos locations over all countries
$col++;
$_DATAGRID[$row][$col] = array(
	'text' => $totalPosWithoutChannels + $_SUM_TOTAL[$col],
	'cls' => 'cel grand-total'
);

// grand total actual number over all countries
$col++;
$_DATAGRID[$row][$col] = array(
	'text' => $_SUM_TOTAL[$col] ?: null,
	'cls' => 'cel grand-total'
);

// grand total delta over all countries
$col++;
$_DATAGRID[$row][$col] = array(
	'text' => $_SUM_TOTAL[$col] ?: null,
	'cls' => 'cel grand-total alert'
);

$col++;

foreach ($_COMPANIES as $key => $data) {

	// grand total pos locations
	$col++;
	$_DATAGRID[$row][$col] = array(
		'text' => $_SUM_TOTAL[$col] ?: null,
		'cls' => 'cel grand-total'
	);

	// grand total actual number
	$col++;
	$_DATAGRID[$row][$col] = array(
		'text' => $_SUM_TOTAL[$col] ?: null,
		'cls' => 'cel grand-total'
	);

	// grand total delta
	$col++;
	$_DATAGRID[$row][$col] = array(
		'text' => $_SUM_TOTAL[$col] ?: null,
		'cls' => 'cel grand-total'
	);

	$col++; // separator
}


// build spreadsheet data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_COLSPANS = array();
$_DATA = array();

if ($_DATAGRID) {
		
	foreach ($_DATAGRID as $row => $cols) {
		
		foreach ($cols as $col => $cel) {
			
			$index = spreadsheet::key($col, $row);
			
			$cel['readonly'] = 'true';
			
			// cell attribute tag
			if ($cel['colspan']) {
				$_COLSPANS[$index] = $cel['colspan'];
				unset($cel['colspan']);
			}
			
			$_DATA[$index] = $cel;
		}
	}
}

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPNDING:

header('Content-Type: text/json');
echo json_encode(array(
	'data' => $_DATA,
	'merge' => $_COLSPANS,
	'top' => 3,
	'left' => $_TOTAL_FIXED_COLUMNS+1,
	'parents' => $parents
));
	