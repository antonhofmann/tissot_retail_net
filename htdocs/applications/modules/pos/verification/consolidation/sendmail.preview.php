<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
$user = User::instance();
$translate = Translate::instance();

$_APP = $_REQUEST['application'];
$_TPL = $_REQUEST['mail_template_id'];
$period = $_REQUEST['periodes'];
$region = $_REQUEST['regions'];
$addresstype = $_REQUEST['addresstypes'];
$client = $_REQUEST['clients'];
$selected = $_REQUEST['selected'];

$_SUBJECT = $_REQUEST['mail_template_subject'];
$_CONTENT = $_REQUEST['mail_template_text'];

if (!$period || !$_TPL || !$selected) {
	$_NOTIFICATIONS[] = "Bad request";
	goto BLOCK_RESPONSE;
}

$mail = new ActionMail($_TPL);

// assign params 
$mail->setParam('application', $_APP);
$mail->setParam('period', $period);
$mail->setParam('region', $region);
$mail->setParam('addresstype', $addresstype);
$mail->setParam('client', $client);
$mail->setParam('selected', $selected);

// mail content
if ($_SUBJECT) $mail->setSubject($_SUBJECT);
if ($_CONTENT) $mail->setBody($_CONTENT);
	
$_RESPONSE = $mail->getMailData();
	
BLOCK_RESPONSE:	

header('Content-Type: text/json');
echo json_encode($_RESPONSE);
