<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."tcpdf/tcpdf.php";

$_ERRORS = array();
$_JSON = array();

// execution time
ini_set('max_execution_time', 600);
ini_set('memory_limit', '102400M');

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// request vars
$_APPLICATION = $_REQUEST['application']; 
$_PERIOD = $_REQUEST['periodes'];
$_REGION = $_REQUEST['regions'];
$_STATE = $_REQUEST['states'];
$_ADDRESS_TYPE = $_REQUEST['addresstypes'];
$_LEGAL_TYPE = $_REQUEST['legaltypes'];
$_CLIENT = $_REQUEST['clients'];
$_OPTIONS = $_REQUEST['options'];

$_TOTAL_OPTIONS = count($_OPTIONS);

$_ERRORS = array();

// db model
$model = new Model($_APPLICATION);

// calendar vars
$d = new DateTime(date('Y-m-d'));
$d->modify('first day of this month');
$_CURRENT_MONTH = $d->format('U'); 
$_IS_CURRENT_MONTH = $_CURRENT_MONTH==$_PERIOD ? true : false;

$_COMPANIES = array();
$_ACTUAL_NUMBERS = array();
$_IN_POS_INDEX = array();
$_CONFIRMED_VERIFICATIONS = array();
$_WHITHOUT_CHANNELS = array();


// filter captions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$_FILTER_CAPTIONS = $translate->period.": ".date('F Y', $_PERIOD);

if ($_REGION) {

	$sth = $model->db->prepare("
		SELECT region_name
		FROM db_retailnet.addresses
		INNER JOIN db_retailnet.countries ON country_id = address_country
		INNER JOIN db_retailnet.regions ON region_id = country_region
		WHERE region_id = ?
	");

	$sth->execute(array($_REGION));
	$result = $sth->fetch();
	$_FILTER_CAPTIONS .= "\nRegion: ".$result['region_name'];
}

if ($_STATE) {
	$_STATES = array(1 => 'Completed', 2 => 'Uncompleted');
	$_FILTER_CAPTIONS .= "\nData Entry State: ".$_STATES[$_STATE];
}

if ($_ADDRESS_TYPE) {

	$sth = $model->db->prepare("
		SELECT client_type_code
		FROM db_retailnet.client_types
		WHERE client_type_id = ?
	");

	$sth->execute(array($_REGION));
	$result = $sth->fetch();
	$_FILTER_CAPTIONS .= "\nAddress Type: ".$result['client_type_code'];
}

// global filters ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_FILTER_APPLICATION '';

$_FILTER_ADDRESS_TYPE =  $_ADDRESS_TYPE 
	? " AND addresses.address_client_type = $_ADDRESS_TYPE"
	: " AND addresses.address_client_type IN (1, 2, 3)";

$_FILTER_REGION = $_REGION ? " AND country_region = $_REGION" : null;

$_FILTER_CLIENT = $_CLIENT ? " AND address_id = $_CLIENT" : null;


	
// actual number :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$sql = "
	SELECT DISTINCT
		posverification_data_id,
		posverification_data_distribution_channel_id,
		posverification_data_actual_number,
		posverification_data_in_posindex,
		posverification_whithout_channels,
		posverification_address_id,
		addresses.address_id,
		addresses.address_parent,
		posverification_confirm_date
	FROM posverification_data	
	INNER JOIN posverifications ON posverification_id = posverification_data_posverification_id
	INNER JOIN db_retailnet.addresses ON address_id = posverification_address_id
	LEFT JOIN db_retailnet.posaddresses ON posaddress_client_id = posverification_address_id
	LEFT JOIN db_retailnet.addresses AS clients ON clients.address_id = posaddress_client_id
	LEFT JOIN db_retailnet.countries on country_id = addresses.address_country
	WHERE UNIX_TIMESTAMP(posverification_date) = ?
		$_FILTER_APPLICATION
		$_FILTER_REGION
";

if ($_ADDRESS_TYPE) {
	$sql .= " 
		AND clients.address_client_type = $_ADDRESS_TYPE 
		AND (clients.address_involved_in_planning = 1 OR clients.address_involved_in_planning_ff = 1)
	";
}

$sth = $model->db->prepare($sql);
$sth->execute(array($_PERIOD));
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$key = $row['address_parent'];
		$channel = $row['posverification_data_distribution_channel_id'];
		$permitted = true;
		
		if ($_STATE) {
			$permitted = false;
			$permitted = $_STATE==1 && $row['posverification_confirm_date'] ? true : $permitted;
			$permitted = $_STATE==2 && !$row['posverification_confirm_date'] ? true : $permitted;
		}
		
		if ($permitted) {

			if ($row['posverification_confirm_date']) {
				$_IN_POS_INDEX[$key][$channel] = $row['posverification_data_in_posindex'];
			}

			$_ACTUAL_NUMBERS[$key][$channel] = $row['posverification_data_actual_number'];

			$_CONFIRMED_VERIFICATIONS[$key] = $row['posverification_confirm_date'] ? true : false;

			if (!$_IS_CURRENT_MONTH) {
				$_WHITHOUT_CHANNELS[$key] = $row['posverification_whithout_channels'];
			}
		}
	}
}	


// current pos distribution channels :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
if ($_IS_CURRENT_MONTH) {

	$sql = "
		SELECT DISTINCT
			posaddress_id,
			addresses.address_id,
			addresses.address_parent,
			posaddress_client_id,
			posaddress_distribution_channel
		FROM db_retailnet.posaddresses
		LEFT JOIN db_retailnet.addresses as addresses ON addresses.address_id = posaddress_client_id
		LEFT JOIN db_retailnet.addresses as parentaddresses ON parentaddresses.address_id = posaddress_franchisee_id
		LEFT JOIN db_retailnet.countries on country_id = addresses.address_country
		WHERE (
			addresses.address_active = 1 
			AND addresses.address_type = 1
			AND (
				addresses.address_involved_in_planning = 1 
				OR addresses.address_involved_in_planning_ff = 1
				
			)
			AND (
				posaddress_store_closingdate IS NULL
				OR posaddress_store_closingdate = '0000-00-00'
				OR posaddress_store_closingdate = ''
			)
			$_FILTER_APPLICATION
			$_FILTER_REGION
			$_FILTER_ADDRESS_TYPE
		)
	";


	if (!$_ADDRESS_TYPE) {
		$sql .= "
			OR (
				parentaddresses.address_active = 1 
				AND parentaddresses.address_type = 1
				AND addresses.address_client_type = 3
				AND (
					posaddress_store_closingdate IS NULL
					OR posaddress_store_closingdate = '0000-00-00'
					OR posaddress_store_closingdate = ''
				)
				$_FILTER_APPLICATION
				$_FILTER_REGION
			)
		";
	}

	$sth = $model->db->prepare($sql);
	$sth->execute();
	$result = $sth->fetchAll();
		
	if ($result) {
		
		foreach ($result as $row) {
			
			$key = $row['address_parent'];
			$channel = $row['posaddress_distribution_channel'];
			$permitted = true;
			
			if ($_STATE) {
				$permitted = false;
				$permitted = $_STATE==1 && $_CONFIRMED_VERIFICATIONS[$key] ? true : $permitted;
				$permitted = $_STATE==2 && !$_CONFIRMED_VERIFICATIONS[$key] ? true : $permitted;
			}
			
			if ($permitted && !$_CONFIRMED_VERIFICATIONS[$key]) {
				if ($channel) $_IN_POS_INDEX[$key][$channel] += 1;
				else $_WHITHOUT_CHANNELS[$key] += 1;
			}
		}
	}
}

	
// clients :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$sql = "
	SELECT DISTINCT
		address_id,
		address_parent,
		address_company,
		country_name
	FROM db_retailnet.addresses
	LEFT JOIN db_retailnet.countries on country_id = address_country
	WHERE address_active = 1
		AND address_type = 1
		AND (
			address_involved_in_planning = 1 
			OR address_involved_in_planning_ff = 1
		)
		$_FILTER_ADDRESS_TYPE
		$_FILTER_REGION
		$_FILTER_CLIENT
	ORDER BY country_name, address_company
";

$sth = $model->db->prepare($sql);
$sth->execute();
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$key = $row['address_parent'];
		$permitted = true;

		if ($_STATE) {
			$permitted = false;
			$permitted = $_CONFIRMED_VERIFICATIONS[$key] ? true : $permitted;
			$permitted = $_CONFIRMED_VERIFICATIONS[$key] ? true : $permitted;
		}
		
		if ($permitted) {
			$_COMPANIES[$key]['country_name'] = $row['country_name'];
			$_COMPANIES[$key]['address_company'] = $row['address_company'];
		}
	}
}
	

// distribution channels :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT DISTINCT
		mps_distchannel_id,
		mps_distchannel_groups.mps_distchannel_group_id,
		mps_distchannel_group_name,
		mps_distchannel_code,
		mps_distchannel_name
	FROM db_retailnet.mps_distchannels
	INNER JOIN db_retailnet.mps_distchannel_groups ON mps_distchannels.mps_distchannel_group_id = mps_distchannel_groups.mps_distchannel_group_id	
	WHERE mps_distchannel_active = 1 
	ORDER BY mps_distchannel_group_name, mps_distchannel_name
");

$sth->execute();
$result = $sth->fetchAll();

$_CHANNELS = array();
$_DISTRIBUTION_CHANNELS = array();

if ($result) {

	foreach ($result as $row) {
		$channel = $row['mps_distchannel_id'];
		$group = $row['mps_distchannel_group_id'];
		$_CHANNELS[$channel] = $row['mps_distchannel_code'];
		$_DISTRIBUTION_CHANNELS[$group]['caption'] = $row['mps_distchannel_group_name'];
		$_DISTRIBUTION_CHANNELS[$group]['channels'][$channel]['code'] = $row['mps_distchannel_code'];
		$_DISTRIBUTION_CHANNELS[$group]['channels'][$channel]['name'] = $row['mps_distchannel_name'];
		$_TOTAL_CHANNELS++;
	}
}


// get data from passt :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && !$_IS_CURRENT_MONTH) {

	$parents = array();

	foreach ($_COMPANIES as $key => $row) {
		foreach ($_CHANNELS as $channel => $name) {
			if (!$_IN_POS_INDEX[$key][$channel]) {
				$parents[] = $key;
			}
		}
	}

	if ($parents) {

		$parents = join(',', array_filter($parents));

		$periodYear = date('Y', $_PERIOD);
		$periodMonth = date('m', $_PERIOD);
			
		$result = $model->query("
			SELECT 
			    addresses.address_parent,
			    mps_distchannels.mps_distchannel_id,
			    COUNT(posaddresses.posaddress_id) AS total
			FROM db_retailnet.posaddresses
			LEFT JOIN db_retailnet.mps_distchannels ON mps_distchannels.mps_distchannel_id = posaddresses.posaddress_distribution_channel
			LEFT JOIN db_retailnet.countries ON posaddresses.posaddress_country = country_id
			LEFT JOIN db_retailnet.addresses ON posaddresses.posaddress_client_id = addresses.address_id
			WHERE
			addresses.address_parent IN ($parents)
			AND (
				YEAR(posaddresses.posaddress_store_openingdate) < $periodYear
				OR ( 
					YEAR(posaddresses.posaddress_store_openingdate) = $periodYear 
					AND MONTH(posaddresses.posaddress_store_openingdate) <= $periodMonth
				)
			)
			AND (
				posaddresses.posaddress_store_closingdate IS NULL
				OR posaddresses.posaddress_store_closingdate = '000-00-00'
	            OR YEAR(posaddresses.posaddress_store_closingdate) > $periodYear
	            OR (
	            	YEAR(posaddresses.posaddress_store_closingdate) = $periodYear
					AND MONTH(posaddresses.posaddress_store_closingdate) > $periodMonth
				)
	        )
			$_FILTER_APPLICATION
			GROUP BY addresses.address_id , mps_distchannels.mps_distchannel_id
			ORDER BY countries.country_name , addresses.address_company , mps_distchannels.mps_distchannel_id , mps_distchannels.mps_distchannel_name , mps_distchannels.mps_distchannel_code
		")->fetchAll();

		if ($result) {
				
			foreach ($result as $row) {

				$key = $row['address_parent'];
				$channel = $row['mps_distchannel_id'];

				if ($channel) $_IN_POS_INDEX[$key][$channel] = $row['total'];
				else $_WHITHOUT_CHANNELS[$key] += $row['total'];
			}
		}
	}
}


// document instance :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

class MYPDF extends TCPDF {
		
	function Header() {
		$this->Image( $_SERVER['DOCUMENT_ROOT'].'/public/images/logo.swatch.145x40.png', 10, 8, 28);
		$this->SetFontSize(12);
		$this->Cell($_COL_CLIENT_WIDTH);
		$this->Cell(0,20,'POS Verifications',0,1,'R');
		$this->drawTableHeader();
	}

	function drawTableHeader() {

		global 	$_DISTRIBUTION_CHANNELS, 
				$_OPTIONS, 
				$_COL_CLIENT_WIDTH, 
				$_COL_WITHOUT_CHANNEL_WIDTH,
				$_COL_CHANNEL_WIDTH, 
				$_COL_OPTION_WIDTH,
				$_MARGIN_LEFT,
				$_MARGIN_RIGHT,
				$_MARGIN_TOP,
				$_MARGIN_BOTTOM,
				$_FILTER_CAPTIONS;

		$this->SetLineWidth(0.1);
		$this->SetDrawColor(180, 180, 180);
		$this->SetTextColor(0);

		// distribution groups
		
		$this->SetFontSize(10);
		$this->SetFillColor(235, 235, 245);

		$celHeight = 10;
		$x = $_COL_CLIENT_WIDTH + $_COL_CHANNEL_WIDTH + $_COL_WITHOUT_CHANNEL_WIDTH + $_MARGIN_LEFT;
		$y = $this->GetY() + $celHeight - 12;
		$this->SetXY($x,$y);

		foreach ($_DISTRIBUTION_CHANNELS as $group => $channels) {
			$celWidth = count($channels['channels'])*$_COL_CHANNEL_WIDTH;
			$this->Cell($celWidth, $celHeight, ' '.$channels['caption'], 1, 0, 'L', 1);
		}

		$this->Ln();

		// distribution channels name

		$celHeight = 70;

		$x = $_MARGIN_LEFT;
		$y = $this->GetY();
		$this->SetXY($x,$y);

		// filter captions
		$this->SetFontSize(8);
		//$this->Cell($_COL_CLIENT_WIDTH, $celHeight, $_FILTER_CAPTIONS, 0, 0, 'L', 0, null, 0, false, 'T', 'T');
		//$w, $h, $txt, $border=0, $align='J', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='T', $fitcell=false 
		$this->MultiCell($_COL_CLIENT_WIDTH, $celHeight, $_FILTER_CAPTIONS, 0, 'L', 0, 0, null, null, false, 0, false, false, 0, T);

		$i=0;
		$x = $_COL_CLIENT_WIDTH + $_MARGIN_LEFT;
		$y = $y + $celHeight;
		$this->SetXY($x,$y);

		$this->SetFontSize(10);

		// without channels
		$this->SetFillColor(245, 225, 225);
		$this->StartTransform();
		$this->Rotate(90);
		$this->Cell($celHeight, $_COL_WITHOUT_CHANNEL_WIDTH, " POS without channels", 1, 0, 'L', 1);
		$this->StopTransform();
		$this->SetX($x+$_COL_WITHOUT_CHANNEL_WIDTH);

		$x = $this->getX();

		// over all channels
		$this->SetFillColor(225, 235, 245);
		$this->StartTransform();
		$this->Rotate(90);
		$this->Cell($celHeight, $_COL_CHANNEL_WIDTH, " Over All Channels", 1, 0, 'L', 1);
		$this->StopTransform();
		$this->SetX($x+$_COL_CHANNEL_WIDTH);

		foreach ($_DISTRIBUTION_CHANNELS as $group => $channels) {
			
			foreach ($channels['channels'] as $id => $channel) {

				$i++;
				if ($i%2==0) $this->SetFillColor(232, 232, 232);
				else $this->SetFillColor(244, 244, 244);

				$x = $this->GetX();
				$this->StartTransform();
				$this->Rotate(90);
				$this->Cell($celHeight, $_COL_CHANNEL_WIDTH,  ' '.$channel['name'].', '.$channel['code'], 1, 0, 'L', 1);
				$this->StopTransform();
				$this->SetX($x+$_COL_CHANNEL_WIDTH);
			}
		}

		$this->Ln();

		// option columns

		$cellHeight = 7;

		$this->SetFontSize(6);

		$i=0;
		$x = $_COL_CLIENT_WIDTH + $_MARGIN_LEFT;
		$y = $this->GetY()-$_COL_CHANNEL_WIDTH;
		$this->SetXY($x,$y);

		// widthout channels
		$this->SetFillColor(245, 225, 225);
		$this->Cell($_COL_WITHOUT_CHANNEL_WIDTH, $cellHeight,  'POS', 1, 0, 'C', 1);

		// over all channels
		$this->SetFillColor(225, 235, 245);

		foreach ($_OPTIONS as $name => $caption) {
			$this->Cell($_COL_OPTION_WIDTH, $cellHeight,  $caption, 1, 0, 'C', 1);
		}

		foreach ($_DISTRIBUTION_CHANNELS as $group => $channels) {
			
			foreach ($channels['channels'] as $id => $channel) {
				
				$i++;
				if ($i%2==0) $this->SetFillColor(232, 232, 232);
				else $this->SetFillColor(244, 244, 244);

				foreach ($_OPTIONS as $name => $caption) {
					$this->Cell($_COL_OPTION_WIDTH, $cellHeight,  $caption, 1, 0, 'C', 1);
				}
			}
		}
	}


	function Footer() {
		$this->SetY(-15);
		$this->SetFontSize(9);
		$this->Cell(0, 15, 'Date: '.date('m/d/Y'), 0, 0, 'L');
		$this->Cell(0, 15, 'Page '.$this->PageNo(), 0, 0, 'R');
	}
}

$pdf = new MYPDF("L", "mm", "A2", true, 'UTF-8', false);

$_MARGIN_LEFT = 10;
$_MARGIN_RIGHT = 10;
$_MARGIN_TOP = 10;
$_MARGIN_BOTTOM = 10;
$_PAGE_WIDTH = $pdf->getPageWidth();
$_PAGE_HEIGHT = $pdf->getPageHeight();
$_WORKAREA_WIDTH = $_PAGE_WIDTH - $_MARGIN_LEFT - $_MARGIN_RIGHT;
$_WORKAREA_HEIGHT = $_PAGE_HEIGHT - 10 - $_MARGIN_BOTTOM;
$_COL_CLIENT_WIDTH = 65;
$_COL_WITHOUT_CHANNEL_WIDTH = 10;
$_COL_CHANNEL_WIDTH = ($_WORKAREA_WIDTH-$_COL_CLIENT_WIDTH-$_COL_WITHOUT_CHANNEL_WIDTH) / ($_TOTAL_CHANNELS+1);
$_COL_OPTION_WIDTH = $_COL_CHANNEL_WIDTH / $_TOTAL_OPTIONS;


$_COL_CLIENT_WIDTH = $_WORKAREA_WIDTH - $_COL_WITHOUT_CHANNEL_WIDTH - (($_TOTAL_CHANNELS+1)*$_COL_CHANNEL_WIDTH);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor($user->firstname.' '.$user->name);
$pdf->SetTitle('POS Verifications');

// set margins
$pdf->SetMargins($_MARGIN_LEFT, 105, $_MARGIN_RIGHT, true);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, $_MARGIN_BOTTOM);

// add a page
$pdf->AddPage();

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}


// global settings
$pdf->SetLineWidth(0.1);
$pdf->SetDrawColor(180, 180, 180);
$pdf->SetTextColor(0);

// datagrid ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_SUMCOL = array();

$cellHeight = 7;

$pdf->SetFontSize(7);

$x = $_MARGIN_LEFT;
$y = $pdf->GetY();
$pdf->SetXY($x,$y);

foreach ($_COMPANIES as $key => $company) {

	$pdf->SetTextColor(0);
	$pdf->SetFillColor(235, 235, 245);
	$pdf->Cell($_COL_CLIENT_WIDTH, $cellHeight,  $company['country_name'].', '.$company['address_company'], 1, 0, 'L', 1);

	// without channels
	$pdf->SetFillColor(245, 225, 225);
	$widthoutChannels = $_WHITHOUT_CHANNELS[$key];
	$pdf->Cell($_COL_WITHOUT_CHANNEL_WIDTH, $cellHeight,  $widthoutChannels ?: null, 1, 0, 'R', 1);
	$_SUMCOL['without'] += $widthoutChannels;

	// over all channels
	$pdf->SetFillColor(225, 235, 245);

	$totalPosIndex = $_IN_POS_INDEX[$key] ? array_sum(array_values($_IN_POS_INDEX[$key])) : 0;
	$totalActualNumber = $_ACTUAL_NUMBERS[$key] ? array_sum(array_values($_ACTUAL_NUMBERS[$key])) : 0;
	$totalDelta = $totalPosIndex-$totalActualNumber;

	foreach ($_OPTIONS as $name => $caption) {

		if ($name=='posindex') {
			$pdf->Cell($_COL_OPTION_WIDTH, $cellHeight,  $totalPosIndex ?: null, 1, 0, 'R', 1);
			$_SUMCOL[$name] += $totalPosIndex;
		}

		if ($name=='actual') {
			$pdf->Cell($_COL_OPTION_WIDTH, $cellHeight,  $totalActualNumber ?: null, 1, 0, 'R', 1);
			$_SUMCOL[$name] += $totalActualNumber;
		}

		if ($name=='delta') {
			$pdf->SetTextColor(255,0,0);
			$pdf->Cell($_COL_OPTION_WIDTH, $cellHeight,  $totalDelta ?: null, 1, 0, 'R', 1);
			$_SUMCOL[$name] += $totalDelta;
		}
	}

	$i=0;
	
	foreach ($_DISTRIBUTION_CHANNELS as $group => $channels) {
		
		foreach ($channels['channels'] as $id => $channel) {
			
			$inPosIndex = $_IN_POS_INDEX[$key][$id];
			$actualNumber = $_ACTUAL_NUMBERS[$key][$id];
			$delta = $inPosIndex-$actualNumber;

			$pdf->SetTextColor(0);

			$i++;
			if ($i%2==0) $pdf->SetFillColor(232, 232, 232);
			else $pdf->SetFillColor(244, 244, 244);

			foreach ($_OPTIONS as $name => $caption) {

				if ($name=='posindex') {
					$pdf->Cell($_COL_OPTION_WIDTH, $cellHeight,  $inPosIndex ?: null, 1, 0, 'R', 1);
					$_SUMCOL[$id.$name] += $inPosIndex;
				}

				if ($name=='actual') {
					$pdf->Cell($_COL_OPTION_WIDTH, $cellHeight,  $actualNumber ?: null, 1, 0, 'R', 1);
					$_SUMCOL[$id.$name] += $actualNumber;
				}

				if ($name=='delta') {
					$pdf->SetTextColor(255,0,0);
					$pdf->Cell($_COL_OPTION_WIDTH, $cellHeight,  $delta ?: null, 1, 0, 'R', 1);
					$_SUMCOL[$id.$name] += $delta;
				}
			}
		}
	}

	$pdf->Ln();
}


// grand total :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$pdf->Ln();

$cellHeight = 10;

$x = $_MARGIN_LEFT;
$y = $pdf->GetY();
$pdf->SetXY($x,$y);

$pdf->SetTextColor(0);
$pdf->SetFillColor(235, 235, 245);
$pdf->SetFontSize(9);
$pdf->Cell($_COL_CLIENT_WIDTH, $cellHeight,  "Grand Total of POS Locations", 1, 0, 'L', 1);

$pdf->SetFontSize(7);

// without channels
$pdf->SetFillColor(245, 225, 225);
$widthoutChannels = $_SUMCOL['without'];
$pdf->Cell($_COL_WITHOUT_CHANNEL_WIDTH, $cellHeight,  $widthoutChannels ?: null, 1, 0, 'L', 1);

// over all channels
$pdf->SetFillColor(225, 235, 245);

foreach ($_OPTIONS as $name => $caption) {

	if ($name=='posindex') {
		$pdf->Cell($_COL_OPTION_WIDTH, $cellHeight,  $_SUMCOL[$name] ?: null, 1, 0, 'R', 1);
	}

	if ($name=='actual') {
		$pdf->Cell($_COL_OPTION_WIDTH, $cellHeight,  $_SUMCOL[$name] ?: null, 1, 0, 'R', 1);
	}

	if ($name=='delta') {
		$pdf->SetTextColor(255,0,0);
		$pdf->Cell($_COL_OPTION_WIDTH, $cellHeight,  $_SUMCOL[$name] ?: null, 1, 0, 'R', 1);
	}
}

$i=0;

foreach ($_DISTRIBUTION_CHANNELS as $group => $channels) {
	
	foreach ($channels['channels'] as $id => $channel) {

		$pdf->SetTextColor(0);

		$i++;
		if ($i%2==0) $pdf->SetFillColor(232, 232, 232);
		else $pdf->SetFillColor(244, 244, 244);

		foreach ($_OPTIONS as $name => $caption) {

			if ($name=='posindex') {
				$pdf->Cell($_COL_OPTION_WIDTH, $cellHeight,  $_SUMCOL[$id.$name] ?: null, 1, 0, 'R', 1);dex;
			}

			if ($name=='actual') {
				$pdf->Cell($_COL_OPTION_WIDTH, $cellHeight,  $_SUMCOL[$id.$name] ?: null, 1, 0, 'R', 1);
			}

			if ($name=='delta') {
				$pdf->SetTextColor(255,0,0);
				$pdf->Cell($_COL_OPTION_WIDTH, $cellHeight,  $_SUMCOL[$id.$name] ?: null, 1, 0, 'R', 1);
			}
		}
	}
}

$pdf->Ln();

// export ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$file = '/data/tmp/pos.verifications.'.$_SERVER['REQUEST_TIME'].'.pdf';
$pdf->Output($_SERVER['DOCUMENT_ROOT'].$file, 'F');

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONSE:

ob_end_clean();

if (!$_ERRORS) {
	$hash = Encryption::url($file);
	$url = "/download/tmp/$hash";
}

header('Content-Type: text/json');
echo json_encode(array(
	'success' => file_exists($_SERVER['DOCUMENT_ROOT'].$file) ? true : false,
	'file' => $url
));

