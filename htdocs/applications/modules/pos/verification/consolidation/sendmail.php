<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// request
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$tpl = $_REQUEST['mail_template_id'];
$period = $_REQUEST['periodes'];
$region = $_REQUEST['regions'];
$addresstype = $_REQUEST['addresstypes'];
$client = $_REQUEST['clients'];
$selected = $_REQUEST['selected'];

$_MAIL_SUBJECT = $_REQUEST['mail_template_subject'];
$_MAIL_CONTENT = $_REQUEST['mail_template_text'];
$_MAIL_TEST = $_REQUEST['test_recipient'];

if (!$period || !$tpl || !$selected) {
	$_NOTIFICATIONS[] = "Bad request";
	goto BLOCK_RESPONSE;
}
		
$mail = new ActionMail($tpl);

// assign params 
$mail->setParam('application', $application);
$mail->setParam('period', $period);
$mail->setParam('region', $region);
$mail->setParam('addresstype', $addresstype);
$mail->setParam('client', $client);
$mail->setParam('selected', $selected);

// mail content
if ($_MAIL_SUBJECT) $mail->setSubject($_MAIL_SUBJECT);
if ($_MAIL_CONTENT) $mail->setBody($_MAIL_CONTENT);

// test mail
if ($_MAIL_TEST) $mail->setTestMail($_MAIL_TEST);
	
$mail->send();

// total send mails
$totalSendMails = $mail->getTotalSentMails();
	
$_RESPONSE = $totalSendMails > 0 ? true : false;
$_NOTIFICATIONS[] = "Total send $totalSendMails E-mails.";
$_CONSOLE = $mail->getConsole();


BLOCK_RESPONSE:	

header('Content-Type: text/json');

echo json_encode(array(
	'response' => $_RESPONSE,
	'reload' => $_RELOAD,
	'notification' => join('<br>', $_NOTIFICATIONS),
	'console' => $_CONSOLE
));
