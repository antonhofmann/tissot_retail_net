<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

// execution time
ini_set('max_execution_time', 600);
ini_set('memory_limit', '102400M');

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// request vars
$_APPLICATION = $_REQUEST['application']; 
$_PERIOD = $_REQUEST['periodes'];
$_REGION = $_REQUEST['regions'];
$_STATE = $_REQUEST['states'];
$_ADDRESS_TYPE = $_REQUEST['addresstypes'];
$_LEGAL_TYPE = $_REQUEST['legaltypes'];
$_CLIENT = $_REQUEST['clients'];
$_OPTIONS = $_REQUEST['options'];

$_ERRORS = array();

// db model
$model = new Model($_APPLICATION);

// calendar vars
$d = new DateTime(date('Y-m-d'));
$d->modify('first day of this month');
$_CURRENT_MONTH = $d->format('U'); 

$_IS_CURRENT_MONTH = $_CURRENT_MONTH==$_PERIOD ? true : false;

$_CONFIRMED_VERIFICATIONS = array();
$_ACTUAL_NUMBERS = array();
$_IN_POSINDEX = array();
$_WHITHOUT_CHANNELS = array();
	

// filter captions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$_FILTER_CAPTIONS = $translate->period.": ".date('F Y', $_PERIOD);

if ($_REGION) {

	$sth = $model->db->prepare("
		SELECT region_name
		FROM db_retailnet.addresses
		INNER JOIN db_retailnet.countries ON country_id = address_country
		INNER JOIN db_retailnet.regions ON region_id = country_region
		WHERE region_id = ?
	");

	$sth->execute(array($_REGION));
	$result = $sth->fetch();
	$_FILTER_CAPTIONS .= "\nRegion: ".$result['region_name'];
}

if ($_STATE) {
	$_STATES = array(1 => 'Completed', 2 => 'Uncompleted');
	$_FILTER_CAPTIONS .= "\nData Entry State: ".$_STATES[$_STATE];
}

if ($_ADDRESS_TYPE) {

	$sth = $model->db->prepare("
		SELECT client_type_code
		FROM db_retailnet.client_types
		WHERE client_type_id = ?
	");

	$sth->execute(array($_REGION));
	$result = $sth->fetch();
	$_FILTER_CAPTIONS .= "\nAddress Type: ".$result['client_type_code'];
}


// global filters ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_FILTER_APPLICATION = '';

$_FILTER_ADDRESS_TYPE =  $_ADDRESS_TYPE 
	? " AND addresses.address_client_type = $_ADDRESS_TYPE"
	: " AND addresses.address_client_type IN (1, 2, 3)";

$_FILTER_REGION = $_REGION ? " AND country_region = $_REGION" : null;

$_FILTER_CLIENT = $_CLIENT ? " AND address_id = $_CLIENT" : null;
	

// actual number :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$sql = "
	SELECT DISTINCT
		posverification_data_id,
		posverification_data_distribution_channel_id,
		posverification_data_actual_number,
		posverification_data_in_posindex,
		posverification_whithout_channels,
		posverification_address_id,
		addresses.address_id,
		addresses.address_parent,
		posverification_confirm_date
	FROM posverification_data	
	INNER JOIN posverifications ON posverification_id = posverification_data_posverification_id
	INNER JOIN db_retailnet.addresses ON address_id = posverification_address_id
	LEFT JOIN db_retailnet.posaddresses ON posaddress_client_id = posverification_address_id
	LEFT JOIN db_retailnet.addresses AS clients ON clients.address_id = posaddress_client_id
	LEFT JOIN db_retailnet.countries on country_id = addresses.address_country
	WHERE UNIX_TIMESTAMP(posverification_date) = ?
		$_FILTER_APPLICATION
		$_FILTER_REGION
";

if ($_ADDRESS_TYPE) {
	$sql .= " 
		AND clients.address_client_type = $_ADDRESS_TYPE 
		AND (clients.address_involved_in_planning = 1 OR clients.address_involved_in_planning_ff = 1)
	";
}

$sth = $model->db->prepare($sql);
$sth->execute(array($_PERIOD));
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$key = $row['address_parent'];
		$channel = $row['posverification_data_distribution_channel_id'];
		$permitted = true;
		
		if ($_STATE) {
			$permitted = false;
			$permitted = $_STATE==1 && $row['posverification_confirm_date'] ? true : $permitted;
			$permitted = $_STATE==2 && !$row['posverification_confirm_date'] ? true : $permitted;
		}
		
		if ($permitted) {

			$_ACTUAL_NUMBERS[$channel][$key] += $row['posverification_data_actual_number'];

			if ($row['posverification_data_in_posindex'] > 0 && ($row['posverification_confirm_date'] || !$_IS_CURRENT_MONTH) ) {
				$_IN_POSINDEX[$channel][$key] = $row['posverification_data_in_posindex'];
			}
			
			if ($row['posverification_confirm_date']) {
				
				$_CONFIRMED_VERIFICATIONS[$key] = true;

				if (!$_IS_CURRENT_MONTH) {
					$_WHITHOUT_CHANNELS[$key] = $row['posverification_whithout_channels'];
				}
			}
		}
	}
}


// current pos distribution channels :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_IS_CURRENT_MONTH) {

	$sql = "
		SELECT DISTINCT
			posaddress_id,
			addresses.address_id,
			addresses.address_parent,
			posaddress_client_id,
			posaddress_distribution_channel
		FROM db_retailnet.posaddresses
		LEFT JOIN db_retailnet.addresses as addresses ON addresses.address_id = posaddress_client_id
		LEFT JOIN db_retailnet.addresses as parentaddresses ON parentaddresses.address_id = posaddress_franchisee_id
		LEFT JOIN db_retailnet.countries on country_id = addresses.address_country
		WHERE (
			addresses.address_active = 1 
			AND addresses.address_type = 1
			AND (
				addresses.address_involved_in_planning = 1 
				OR addresses.address_involved_in_planning_ff = 1
				
			)
			AND (
				posaddress_store_closingdate IS NULL
				OR posaddress_store_closingdate = '0000-00-00'
				OR posaddress_store_closingdate = ''
			)
			$_FILTER_APPLICATION
			$_FILTER_REGION
			$_FILTER_ADDRESS_TYPE
		)
	";

	if (!$_ADDRESS_TYPE) {
		$sql .= "
			OR (
				parentaddresses.address_active = 1 
				AND parentaddresses.address_type = 1
				AND addresses.address_client_type = 3
				AND (
					posaddress_store_closingdate IS NULL
					OR posaddress_store_closingdate = '0000-00-00'
					OR posaddress_store_closingdate = ''
				)
				$_FILTER_APPLICATION
				$_FILTER_REGION
			)
		";
	}

	$sth = $model->db->prepare($sql);
	$sth->execute();
	$result = $sth->fetchAll();
		
	if ($result) {
		
		foreach ($result as $row) {
			
			$key = $row['address_parent'];
			$channel = $row['posaddress_distribution_channel'];
			$permitted = true;
			
			if ($_STATE) {
				$permitted = false;
				$permitted = $_STATE==1 && $_CONFIRMED_VERIFICATIONS[$key] ? true : $permitted;
				$permitted = $_STATE==2 && !$_CONFIRMED_VERIFICATIONS[$key] ? true : $permitted;
			}
			
			if ($permitted) {

				// count distribution channel for each company separated
				if ($_IS_CURRENT_MONTH && !$_CONFIRMED_VERIFICATIONS[$key]) {
					if ($channel) $_IN_POSINDEX[$channel][$key] += 1;
					else $_WHITHOUT_CHANNELS[$key] += 1;
				}
			}
		}
	}
}

	
// clients :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$_COMPANIES = array();

$sql = "
	SELECT DISTINCT
		address_id,
		address_parent,
		address_company,
		country_name
	FROM db_retailnet.addresses
	LEFT JOIN db_retailnet.countries on country_id = address_country
	WHERE address_active = 1
		AND address_type = 1
		AND (
			address_involved_in_planning = 1 
			OR address_involved_in_planning_ff = 1
		)
		$_FILTER_ADDRESS_TYPE
		$_FILTER_REGION
		$_FILTER_CLIENT
	ORDER BY country_name, address_company
";

$sth = $model->db->prepare($sql);
$sth->execute();
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$key = $row['address_parent'];
		$permitted = true;

		if ($_STATE) {
			$permitted = false;
			$permitted = $_STATE==1 && $_CONFIRMED_VERIFICATIONS[$key] ? true : $permitted;
			$permitted = $_STATE==2 && !$_CONFIRMED_VERIFICATIONS[$key] ? true : $permitted;
		}
		
		if ($permitted) {

			$countryName = $row['country_name'];
			$_COMPANIES[$key]['country_name'] = trim($countryName);

			$companyName = $row['address_company'];
			//$companyName = str_replace("(". ucfirst(strtolower($countryName)) .")", '', $companyName);
			$companyName = strlen($companyName) > 45 ? substr($companyName, 0, 43).'..' : $companyName;
			$_COMPANIES[$key]['address_company'] = trim($companyName);
		}
	}
}
	

// distribution channels :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT DISTINCT
		mps_distchannel_id,
		mps_distchannel_group_name,
		mps_distchannel_code,
		mps_distchannel_name
	FROM db_retailnet.mps_distchannels
	INNER JOIN db_retailnet.mps_distchannel_groups ON mps_distchannels.mps_distchannel_group_id = mps_distchannel_groups.mps_distchannel_group_id	
	WHERE mps_distchannel_active = 1 
	ORDER BY mps_distchannel_group_name, mps_distchannel_name
");

$sth->execute();
$result = $sth->fetchAll();

$_DISTRIBUTION_CHANNELS = array();
$_CHANNELS = array();

if ($result) {

	foreach ($result as $row) {
		
		$channel = $row['mps_distchannel_id'];
		$key = strtolower(str_replace(' ', '', $row['mps_distchannel_group_name']));

		$_CHANNELS[$channel] = $row['mps_distchannel_code'];
		
		// group name
		$_DISTRIBUTION_CHANNELS[$key]['group'] = $row['mps_distchannel_group_name'];
		
		// distribution channel
		$_DISTRIBUTION_CHANNELS[$key]['channels'][$channel] = array(
			'mps_distchannel_name' => $row['mps_distchannel_name'],
			'mps_distchannel_code' => $row['mps_distchannel_code']
		);
	}
}


// get data from passt :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && !$_IS_CURRENT_MONTH) {

	$parents = array();

	foreach ($_CHANNELS as $channel => $name) {
		foreach ($_COMPANIES as $id => $company) {
			if (!$_IN_POSINDEX[$channel][$id]) {
				$parents[] = $id;
			}
		}
	}

	if ($parents) {

		$parents = join(',', array_filter($parents));

		$periodYear = date('Y', $_PERIOD);
		$periodMonth = date('m', $_PERIOD);
			
		$result = $model->query("
			SELECT 
			    addresses.address_parent,
			    mps_distchannels.mps_distchannel_id,
			    COUNT(posaddresses.posaddress_id) AS total
			FROM db_retailnet.posaddresses
			LEFT JOIN db_retailnet.mps_distchannels ON mps_distchannels.mps_distchannel_id = posaddresses.posaddress_distribution_channel
			LEFT JOIN db_retailnet.countries ON posaddresses.posaddress_country = country_id
			LEFT JOIN db_retailnet.addresses ON posaddresses.posaddress_client_id = addresses.address_id
			WHERE
			addresses.address_parent IN ($parents)
			AND (
				YEAR(posaddresses.posaddress_store_openingdate) < $periodYear
				OR ( 
					YEAR(posaddresses.posaddress_store_openingdate) = $periodYear 
					AND MONTH(posaddresses.posaddress_store_openingdate) <= $periodMonth
				)
			)
			AND (
				posaddresses.posaddress_store_closingdate IS NULL
				OR posaddresses.posaddress_store_closingdate = '000-00-00'
	            OR YEAR(posaddresses.posaddress_store_closingdate) > $periodYear
	            OR (
	            	YEAR(posaddresses.posaddress_store_closingdate) = $periodYear
					AND MONTH(posaddresses.posaddress_store_closingdate) > $periodMonth
				)
	        )
			$_FILTER_APPLICATION
			GROUP BY addresses.address_id , mps_distchannels.mps_distchannel_id
			ORDER BY countries.country_name , addresses.address_company , mps_distchannels.mps_distchannel_id , mps_distchannels.mps_distchannel_name , mps_distchannels.mps_distchannel_code
		")->fetchAll();

		if ($result) {
				
			foreach ($result as $row) {

				$key = $row['address_parent'];
				$channel = $row['mps_distchannel_id'];

				if ($channel) $_IN_POSINDEX[$channel][$key] = $row['total'];
				else $_WHITHOUT_CHANNELS[$key] += $row['total'];
			}
		}
	}
}
		
	
// excel builder :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$objPHPExcel = new PHPExcel();

// page settings
$objPHPExcel->getProperties()
->setCreator(settings::init()->project_name)
->setLastModifiedBy(settings::init()->project_name)
->setTitle('POS Verifications');

// page orientation
$objPHPExcel->getActiveSheet()
->getPageSetup()
->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// page size
$objPHPExcel->getActiveSheet()
->getPageSetup()
->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);



// styles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_STYLES['borders'] = array(
	'borders' => array( 
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);

$_STYLES['align-left'] = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
	)
);

$_STYLES['align-center'] = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
	)
);

$_STYLES['small-caption'] = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb'=>'ededed')
	),
	'font' => array(
		'name'=>'Arial',
		'size'=> 10, 
		'color' => array(
			'rgb' => '000000'
		)
	)
);

$_STYLES['bg-total-group'] = array(
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb'=>'f3f7e4')
	)
);

$_STYLES['bg-grand-total'] = array(
	'font' => array(
		'name'=>'Arial',
		'size'=> 11
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb'=>'e4f5f7')
	)
);

$_STYLES['confirmed'] = array(
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb'=>'C3FACE')
	)
);

$_STYLES['caption'] = array(
	'font' => array(
		'name'=>'Arial',
		'size'=> 11
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
		'wrap'=> true
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb'=>'ededed')
	)
);

$_STYLES['group'] = array(
	'font' => array(
		'name'=>'Arial',
		'size'=> 11
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
		'wrap'=> true
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb'=>'ededed')
	)
);

$_STYLES['delta'] = array(
	'font' => array(
		'name'=>'Arial',
		'size'=> 12
	)
);
		
	
// total clients
$_TOTAL_COMPANIES = count($_COMPANIES);

// fixed columns: 5
// seperator: 1
// datagrid: clients*options + companies separators
$_TOTAL_OPTIONS = count($_OPTIONS);
$_TOTAL_COLUMNS = 5 + $_TOTAL_COMPANIES*$_TOTAL_OPTIONS + $_TOTAL_COMPANIES;

// last column
$_FIRST_FIXED_COL_INDEX = 'A';
$_LAST_FIXED_COL_INDEX = PHPExcel_Cell::stringFromColumnIndex($_TOTAL_OPTIONS+1);
$_LAST_COL_INDEX = PHPExcel_Cell::stringFromColumnIndex($_TOTAL_COLUMNS);

$_COL_OPTION_WIDTH = $_TOTAL_OPTIONS==1 ? 9 : 6;

// fixed area col dimensions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$col=0;
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setWidth(40);

$col++;
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setWidth(8);

foreach ($_OPTIONS as $opt => $name) {
	$col++;
	$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setWidth(15);
}

$col++;
$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setWidth(1);

// sheet header ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row=1; 
$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(40);

$col=0;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "POS Verifications");
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getFont()->setSize(20);
$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $_TOTAL_COLUMNS-1, $row);

// seperator
$row++; 

$col=0;
$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $_TOTAL_COLUMNS-1, $row);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "Date: ".date('d.m.Y H:i:s') );


// company headers :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++; 
$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(150);

$col=0; // filters cell
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $_FILTER_CAPTIONS);
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, $row, $col+1, $row);

$col++; // merget cel

$col++; // over all countries
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "Over all Countries");
$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $col+$_TOTAL_OPTIONS-1, $row);
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setTextRotation(90);

// styling
$cel1 = PHPExcel_Cell::stringFromColumnIndex($col);
$cel2 = PHPExcel_Cell::stringFromColumnIndex($col+$_TOTAL_OPTIONS-1);
$range = $cel1.$row.':'.$cel2.$row;
$style = $_STYLES['caption']+$_STYLES['borders'];
$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($style, false);

$col = 2+$_TOTAL_OPTIONS; // separator

// company name
foreach ($_COMPANIES as $key => $company) {

	$col++;

	$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $col+$_TOTAL_OPTIONS-1, $row);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $company['country_name']."\r".$company['address_company']);
	$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setTextRotation(90);
	$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setWrapText(true);
	
	$cel = PHPExcel_Cell::stringFromColumnIndex($col);
	$lastcel = PHPExcel_Cell::stringFromColumnIndex($col+$_TOTAL_OPTIONS-1);
	
	$style = $_STYLES['caption']+$_STYLES['borders'];
	$style = $_CONFIRMED_VERIFICATIONS[$key] ? $_STYLES['confirmed']+$style : $style;
	$objPHPExcel->getActiveSheet()->getStyle($cel.$row.':'.$lastcel.$row)->applyFromArray($style, false);

	// separator
	$col = $col+$_TOTAL_OPTIONS;
	$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setWidth(1);
}

// freeze area
$col= 2+$_TOTAL_OPTIONS;
$cel = PHPExcel_Cell::stringFromColumnIndex($col);
$_row = $row+1;
$objPHPExcel->getActiveSheet()->freezePane($cel.$_row);


// countries headers ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
/*
$row++; 
$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(24);

$col=1; // merge cells
$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, $row, $col+$_TOTAL_OPTIONS, $row);

// freeze area
$col= 2+$_TOTAL_OPTIONS;
$cel = PHPExcel_Cell::stringFromColumnIndex($col);
$_row = $row+1;
$objPHPExcel->getActiveSheet()->freezePane($cel.$_row);

foreach ($_COMPANIES as $key => $company) {

	$col++;
	$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $col+$_TOTAL_OPTIONS-1, $row);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $company['country_name']);

	$cel = PHPExcel_Cell::stringFromColumnIndex($col);
	$lastcel = PHPExcel_Cell::stringFromColumnIndex($col+$_TOTAL_OPTIONS-1);
	$style = $_STYLES['group']+$_STYLES['borders'];
	$style = $_CONFIRMED_VERIFICATIONS[$key] ? $_STYLES['confirmed']+$style : $style;
	$objPHPExcel->getActiveSheet()->getStyle($cel.$row.':'.$lastcel.$row)->applyFromArray($style, false);
	$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getFont()->setSize(11);

	$col = $col+$_TOTAL_OPTIONS;
}
*/
// distribution channels :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_SUM_GROUP = array();
$_SUM_TOTAL = array();

foreach ($_DISTRIBUTION_CHANNELS as $group => $data) {
		
	// distribution group ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$row++;

	$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
	
	$col=0; // distribution group name
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data['group']);
	
	$col++; // distribution group code
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, Translate::instance()->code);

	$style = $_STYLES['group']+$_STYLES['borders'];
	$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':'.'B'.$row)->applyFromArray($style, false);

	// option headers
	$firstcol = $col+1;
	foreach ($_OPTIONS as $option => $name) {
		$col++;
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setWidth(10);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $name);
	}

	$cel1 = PHPExcel_Cell::stringFromColumnIndex($firstcol);
	$cel2 = PHPExcel_Cell::stringFromColumnIndex($col);
	$style = $_STYLES['small-caption']+$_STYLES['borders'];
	$objPHPExcel->getActiveSheet()->getStyle($cel1.$row.':'.$cel2.$row)->applyFromArray($style, false);

	$col++; // separator

	// country names
	foreach ($_COMPANIES as $key => $company) {

		$firstcol = $col+1;

		foreach ($_OPTIONS as $option => $name) {
			$col++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setWidth($_COL_OPTION_WIDTH);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $name);
		}

		$cel1 = PHPExcel_Cell::stringFromColumnIndex($firstcol);
		$cel2 = PHPExcel_Cell::stringFromColumnIndex($col);
		$style = $_STYLES['small-caption']+$_STYLES['borders'];
		$style = $_CONFIRMED_VERIFICATIONS[$key] ? $_STYLES['confirmed']+$style : $style;
		$objPHPExcel->getActiveSheet()->getStyle($cel1.$row.':'.$cel2.$row)->applyFromArray($style, false);

		$col++; // separator
	}

	// distribution channel ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	foreach ($data['channels'] as $channel => $value) {

		$row++;

		// total channel groups over all companies
		$totalChannelInPosIndex = $_IN_POSINDEX[$channel] ? array_sum(array_values($_IN_POSINDEX[$channel])) : 0;
		$totalChannelActualNumber = $_ACTUAL_NUMBERS[$channel] ? array_sum(array_values($_ACTUAL_NUMBERS[$channel])) : 0;
		$delta = $totalChannelActualNumber-$totalChannelInPosIndex;
		
		$col=0; // distribution channel name
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value['mps_distchannel_name']);
		
		$col++; // distribution channel code
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value['mps_distchannel_code']);



		foreach ($_OPTIONS as $opt => $name) {
			
			 // in pos index (over all countries)
			if ($opt=='posindex') {
				$col++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $totalChannelInPosIndex ?: null);
				$_SUM_GROUP[$group][$col] += $totalChannelInPosIndex;
			}

			// actual number (over all countries)
			if ($opt=='actual') {
				$col++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $totalChannelActualNumber ?: null);
				$_SUM_GROUP[$group][$col] += $totalChannelActualNumber;
			}

			// delta (over all countries)
			if ($opt=='delta') {
				
				$col++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $delta ?: null);
				$_SUM_GROUP[$group][$col] += $delta;
				
				if ($delta <> 0) {
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getFont()->getColor()->setRGB('FF0000');
				}
			}
		}
		
		// styling
		$range = $_FIRST_FIXED_COL_INDEX.$row.':'.$_LAST_FIXED_COL_INDEX.$row;
		$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($_STYLES['borders'], false);

		$col++; // separator
		
		foreach ($_COMPANIES as $company => $value) {

			$firstcol = $col+1;

			foreach ($_OPTIONS as $opt => $name) { 

				$inPosIndex = $_IN_POSINDEX[$channel] ? $_IN_POSINDEX[$channel][$company] : 0;
				$actualNumber = $_ACTUAL_NUMBERS[$channel] ? $_ACTUAL_NUMBERS[$channel][$company] : 0;
				$delta = $inPosIndex - $actualNumber;
				
				// in pos index
				if ($opt=='posindex') {
					$col++;
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $inPosIndex ?: null);
					$_SUM_GROUP[$group][$col] += $_IN_POSINDEX[$channel][$company];
				}

				// actual number
				if ($opt=='actual') {
					$col++;
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $actualNumber ?: null);
					$_SUM_GROUP[$group][$col] += $_ACTUAL_NUMBERS[$channel][$company];
				}

				// delta
				if ($opt=='delta') {

					$col++;

					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $delta ?: null);
					$_SUM_GROUP[$group][$col] += $delta;

					if ($delta <> 0) {
						$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getFont()->getColor()->setRGB('FF0000');
					}
				}
			}
			
			$cel1 = PHPExcel_Cell::stringFromColumnIndex($firstcol);
			$cel2 = PHPExcel_Cell::stringFromColumnIndex($col);
			$objPHPExcel->getActiveSheet()->getStyle($cel1.$row.':'.$cel2.$row)->applyFromArray($_STYLES['borders'], false);
			
			$col++; //separator
		}
	}

	// total group :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
	$row++;
	
	$col=0; // caption total group name
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, 'Total '.$data['group']);
	$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $col++, $row);

	foreach ($_OPTIONS as $opt => $name) {
		
		$col++;

		$value = $_SUM_GROUP[$group][$col];
		$_SUM_TOTAL[$col] += $value;

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value ?: null);

		if ($opt=='delta' && $value <> 0) {
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getFont()->getColor()->setRGB('FF0000');
		}
	}

	// styling
	$range = $_FIRST_FIXED_COL_INDEX.$row.':'.$_LAST_FIXED_COL_INDEX.$row;
	$styles = $_STYLES['bg-total-group']+$_STYLES['borders'];
	$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styles, false);


	$col++; // separator

	// total group for each company
	foreach ($_COMPANIES as $company => $value) {

		$firstcol = $col+1;

		foreach ($_OPTIONS as $opt => $name) {
			
			$col++;

			$value = $_SUM_GROUP[$group][$col];
			$_SUM_TOTAL[$col] += $value;
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value ?: null);

			if ($opt=='delta' && $value <> 0) {
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getFont()->getColor()->setRGB('FF0000');
			}
		}

		$cel1 = PHPExcel_Cell::stringFromColumnIndex($firstcol);
		$cel2 = PHPExcel_Cell::stringFromColumnIndex($col);
		$styles = $_STYLES['bg-total-group']+$_STYLES['borders'];
		$objPHPExcel->getActiveSheet()->getStyle($cel1.$row.':'.$cel2.$row)->applyFromArray($styles, false);

		$col++; // separator
	}
	
	$row++; // group separator
}


// pos without channels ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++;

$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(24);

$col=0;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, 'POS Locations whithout distribution channel or with inactive distribution channel');
$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $col++, $row);
$style = $_STYLES['bg-grand-total']+$_STYLES['borders'];
$objPHPExcel->getActiveSheet()->getStyle("A$row:B$row")->applyFromArray($style);

// pos whithout channels over all groups
$totalWithoutChannels = array_sum(array_values($_WHITHOUT_CHANNELS));

foreach ($_OPTIONS as $opt => $name) {

	$col++;

	if ($opt=='posindex') {
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $totalWithoutChannels ?: 0);

		$style = $_STYLES['bg-grand-total']+$_STYLES['borders'];
		$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($style);
	}
}

$col++; // separator

foreach ($_COMPANIES as $company => $val) {
	
	foreach ($_OPTIONS as $opt => $name) {

		$col++;

		if ($opt=='posindex') {
			
			$value = $_WHITHOUT_CHANNELS[$company];
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value ?: 0);
			
			$style = $_STYLES['bg-grand-total']+$_STYLES['borders'];
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($style);
		}
	}

	$col++;
}

$range = $_FIRST_FIXED_COL_INDEX.$row.':'.$_LAST_COL_INDEX.$row;
//$objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setSize(12);


// grand total :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++;

$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(32);

$col=0;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, 'Grand Total of POS Locations');
$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $col++, $row);
$style = $_STYLES['bg-grand-total']+$_STYLES['borders'];
$objPHPExcel->getActiveSheet()->getStyle("A$row:B$row")->applyFromArray($style);

foreach ($_OPTIONS as $opt => $name) {

	$col++;
		
	$value = $_SUM_TOTAL[$col];
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value ?: 0);

	$style = $_STYLES['bg-grand-total']+$_STYLES['borders'];
	$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($style);
}

$col++; // separator

foreach ($_COMPANIES as $company => $val) {
	
	foreach ($_OPTIONS as $opt => $name) {

		$col++;
			
		$value = $_SUM_TOTAL[$col];
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value ?: 0);
		
		$style = $_STYLES['bg-grand-total']+$_STYLES['borders'];
		$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($style);
	}

	$col++;
}

$range = $_FIRST_FIXED_COL_INDEX.$row.':'.$_LAST_COL_INDEX.$row;
//$objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setSize(13);


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONSE:

ob_end_clean();

if (!$_ERRORS) {

	$stamp = $_SERVER['REQUEST_TIME'];
	$file = "/data/tmp/pos.verification.consolidation.$stamp.xlsx";
	$hash = Encryption::url($file);
	$url = "/download/tmp/$hash";
	
	// set active sheet
	$objPHPExcel->getActiveSheet()->setTitle('POS Verificatios');
	$objPHPExcel->setActiveSheetIndex(0);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save($_SERVER['DOCUMENT_ROOT'].$file);
}

header('Content-Type: text/json');

echo json_encode(array(
	'success' => file_exists($_SERVER['DOCUMENT_ROOT'].$file) ? true : false,
	'errors' => $_ERRORS,
	'file' => $url,
	'test' => $_COL_OPTION_WIDTH
));
