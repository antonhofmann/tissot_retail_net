<?php

	class PosTrack {

		protected $pos;
		
		public function __construct() {
			$this->model = new PosTrackModel(Connector::DB_CORE);
		}

		public function getPos() {
			return $this->pos;
		}

		public function setPos($pos) {
			$this->pos = $pos;
		}
		
		public function create($user, $filed, $oldValue, $newValue, $comment=null) {
			if ($this->pos) {
				return $this->model->create($this->pos, $user, $filed, $oldValue, $newValue, $comment);
			}
		}
	}