<?php

	/**
	 * Material Collection Category
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Material_Collection_Category {
		
		/**
		 * Collection Category ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Collection Category Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Material ID
		 * @var integer
		 */
		public $material;
		
		/**
		 * Collection Category File
		 * @var object
		 */
		public $file;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @var object
		 */
		protected $model;
		
		/**
		 * Edit all material collection categories
		 * @var string
		 */
		const PERMISSION_EDIT = 'can_edit_mps_collection_categories';
		
		/**
		 * View All material collection categories
		 * @var string
		 */
		const PERMISSION_VIEW = 'can_view_mps_collection_categories';
		
		const DB_BIND_COLLECTION_CATEGORY_FILES = 'INNER JOIN mps_material_collection_category_files ON mps_material_collection_category_file_collection_id = mps_material_collection_category_id';
		
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Material_Collection_Category_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_material_collection_category_$key"];
		}
		
		public function material($material) {
			$this->material = $material;
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_material_collection_category_id'];
			return $this->data;
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($application, $filters=null) {
			$model = new Material_Collection_Category_Model($application);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
	}