<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES.'phpmailer/class.phpmailer.php';

define(DEBUGGING_MODE, false);

$user = User::instance();
$translate = Translate::instance();
$settings = Settings::init();
$settings->load('data');

$db = Connector::get();

$_ERRORS = array();
$_MESSAGES = array();
$_CONSOLE = array();

$_SERVER['SERVER_NAME'] = $_SERVER['SERVER_NAME'] ?: 'retailnet.tissot.ch;
$_HOST = 'http://'.$_SERVER['SERVER_NAME'];
$_TEST = $_REQUEST['test'];

// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_TEST) {
	if (Check::email($_TEST)) {
		goto BLOCK_RECIPIENTS;
	} else {
		$_ERRORS[] = "Test e-mail is not valid.";
		goto BLOCK_RESPONDING;
	}
}

if (!$_REQUEST['mail_template_subject']) {
	$_ERRORS[] = "Missing mail subject";
}

if (!$_REQUEST['mail_template_text']) {
	$_ERRORS[] = "Missing mail content";
}

if (!$_REQUEST['mail_template_recipients']) {
	$_ERRORS[] = "Missing mail recipients";
}

if ($_ERRORS) {
	goto BLOCK_RESPONDING;
}

// recipients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RECIPIENTS:

$_RECIPIENTS = array();

if ($_TEST) {
	$_RECIPIENTS[] = $_TEST;
	goto BLOCK_SENDMAIL;
}


$emails = preg_split("/[\s,;]+/", $_REQUEST['mail_template_recipients']);

foreach ($emails as $row) {
	
	$email = trim($row);
	
	if ($email && check::email($email)) {
		$_RECIPIENTS[] = $email;
	}
}

if (!$_RECIPIENTS) {
	$_ERRORS[] = count($emails)>1 ? "E-Mail addresses are not valid" : "E-Mail address is not valid.";
	goto BLOCK_RESPONDING;
}


// sendmail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_SENDMAIL:

$tracker = new Mail_Tracking();

$SUBJECT = $_REQUEST['mail_template_subject'];
$CONTENT = $_REQUEST['mail_template_text'];

$_RECIPIENTS = array_unique($_RECIPIENTS);

$totalEmails = 0;

$addInvitation = $db->prepare("
	INSERT INTO user_invitations ( 
		user_invitation_email,
		user_created
	)
	VALUES (?,?)
");

$senderName = $user->firstname ? $user->firstname." ".$user->name : $settings->project_name;;
$senderEmail = $user->email ?: $settings->mail_noreply;

foreach ($_RECIPIENTS as $email) {
	
	// link hash
	$base64 = base64_encode($email);
	$hash = strtr($base64, '+/', '-_');
	$link = "<a href=\"$_HOST/security/invitation/$hash\">$_HOST/security/invitation/$hash</a>"; 

	// plain text body
	$AltBody = strip_tags($CONTENT);;
	$AltBody = Content::dataRender($AltBody, array('link' => $link));
		
	// html body
	$HtmlBody = $CONTENT==strip_tags($CONTENT) ? preg_replace('/[\r\n]+/', '<br /><br />', $CONTENT) : $CONTENT;
	$HtmlBody = Content::dataRender($HtmlBody, array('link' => $link));
	$HtmlBody = "<html><head><meta charset=\"utf-8\"><style type=\"text/css\">body {font-family: arial, helvetica, sans-serif}</style></head><body>$HtmlBody</body></html>";
	
	if (!DEBUGGING_MODE) {

		$mail = new PHPMailer();
		$mail->CharSet = 'UTF-8';
		$mail->Subject = $SUBJECT;
		$mail->AltBody = $AltBody;
		$mail->MsgHTML($HtmlBody);
		$mail->IsHTML(true);
		$mail->SetFrom($senderEmail, $senderName);
		$mail->AddAddress($email);
		$response = $mail->Send();

		if ($response) {

			$totalEmails++;

			$addInvitation->execute(array($email, $user->login));

			$tracked = $tracker->create(array(
				'mail_tracking_mail_template_id' => $_REQUEST['mail_template_id'],
				'mail_tracking_sender_user_id' => $user->id,
				'mail_tracking_subject' => $SUBJECT,
				'mail_tracking_content' => $HtmlBody,
				'mail_tracking_recipient_email' => $email,
				'date_created' => date('Y-m-d H:i:s'),
				'user_created' => $user->login
			));
		}

	} else {
		$_MAILS[] = array(
			'email' => $email,
			'subject' => $SUBJECT,
			'body' => $HtmlBody
		);
	}
}

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONDING:

if (!$_ERRORS) {
	$_MESSAGES[] = $translate->message_request_submitted;
	$_MESSAGES[] = "Total sent $totalEmails email(s)";
}

header('Content-Type: text/json');
echo json_encode(array(
	'response' => $_ERRORS ? false : true,
	'errors' => $_ERRORS,
	'messages' => $_MESSAGES
));
