<?php
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$auth = User::instance();
$translate = Translate::instance();
$model = new Model(Connector::DB_CORE);

$_APP = $_REQUEST['application'];
$_TPL = $_REQUEST['mail_template_id'];
$user = new User($_REQUEST['user']);

$_SUBJECT = $_REQUEST['mail_template_subject'];
$_CONTENT = $_REQUEST['mail_template_text'];
$_TEST = $_REQUEST['test_recipient'];
$_REASON = $_REQUEST['close_account_reason'];
$_CC_RECIPIENTS = $_REQUEST['login_data_cc'];


if (!$_TPL || !$user->id) {
	$_NOTIFICATIONS[] = "Bad request";
	goto BLOCK_RESPONSE;
}


$mail = new ActionMail($_TPL);

// assign params 
$mail->setParam('application', $_APP);
$mail->setParam('id', $user->id);
$mail->setParam('cc', $_CC_RECIPIENTS);

// mail content
if ($_SUBJECT) $mail->setSubject($_SUBJECT);
if ($_CONTENT) $mail->setBody($_CONTENT);

// test mail
if ($_TEST) $mail->setTestMail($_TEST);

// content reason
if ($_REASON) {
	$content  = $mail->getBody();
	$content .= "<br /><br />The reaseon for closing the account is:<br />";
	$content .= $_REASON."<br /><br />";
	$mail->setBody($content);
}

$mail->send();

$_RESPONSE = $mail->isSuccess();
$_CONSOLE = $mail->getConsole();

if (!$_RESPONSE) {
	$_NOTIFICATIONS[] = "Send mail error. Please contact customer services";
	goto BLOCK_RESPONSE;
}

// close user account
if ($_REQUEST['close']) {
	
	if (!$_TEST) { 

		// remove user roles
		$model->db->exec("
			DELETE FROM user_roles
			WHERE user_role_user = $user->id
		");
		
		// deactivate user account
		$user->update(array(
			'user_active' => '0',
			'user_modified' => $auth->login,
			'date_modified' => date('')
		));
	}

	$_NOTIFICATIONS[] = $translate->message_request_close_account;
} 
else {
	$_NOTIFICATIONS[] = $_TPL==13 ? $translate->message_send_login_data : $translate->message_request_open_account;
}

if ($_TEST) {
	$_NOTIFICATIONS[] = "Successfully sent test mail an $_TEST";
}
else {
	
	Message::add(array(
		'type' => 'message',
		'content' => join('<br>', $_NOTIFICATIONS),
		'life' => 5000
	));

	$_RELOAD = $_REQUEST['redirect'] ? false : true;
	$_REDIRECT = $_REQUEST['redirect'];
}


BLOCK_RESPONSE:	
header('Content-Type: text/json');
echo json_encode(array(
	'response' => $_RESPONSE,
	'reload' => $_RELOAD,
	'redirect' => $_REDIRECT,
	'message' => join('<br>', $_NOTIFICATIONS),
	'console' => $_CONSOLE
));
