<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
$user = User::instance();
$translate = Translate::instance();

$_APP = $_REQUEST['application'];
$_TPL = $_REQUEST['mail_template_id'];
$_SUBJECT = $_REQUEST['mail_template_subject'];
$_CONTENT = $_REQUEST['mail_template_text'];
$_REASON = $_REQUEST['close_account_reason'];

$user = new User($_REQUEST['user']);

if (!$user->id || !$_TPL) {
	$_RESPONSE['notification'] = "Bad request";
	goto BLOCK_RESPONSE;
}


$mail = new ActionMail($_TPL);

// set parameters
$mail->setParam('application', $_APP);
$mail->setParam('id', $user->id);
$mail->setParam('cc', $_REQUEST['login_data_cc']);

// mail content
if ($_SUBJECT) $mail->setSubject($_SUBJECT);
if ($_CONTENT) $mail->setBody($_CONTENT);

// content reason
if ($_REASON) {
	$content  = $mail->getBody();
	$content .= "<br /><br />The reaseon for closing the account is:<br />";
	$content .= $_REASON."<br /><br />";
	$mail->setBody($content);
}

$_RESPONSE = $mail->getMailData();
$_RESPONSE['response'] = true;	

BLOCK_RESPONSE:	
header('Content-Type: text/json');
echo json_encode($_RESPONSE);
