<?php

	class Ordersheet_Item_Delivered {
		
		/**
		 * Item ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Order Sheet Item ID
		 * @var integer
		 */
		public $item;
		
		/**
		 * Item Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Database Model
		 * @return Ordersheet_Item_Delivered_Model
		 */
		protected $model;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Join Order Sheet Items
		 * @var string
		 */
		const DB_BIND_ORDERSHEET_ITEMS = 'INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id';
	
		/**
		 * Join POS Addresses
		 * @var unknown
		 */
		const DB_BIND_POSADDRESSES = 'INNER JOIN db_retailnet.posaddresses ON posaddress_id = mps_ordersheet_item_delivered_posaddress_id';
		
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Ordersheet_Item_Delivered_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_ordersheet_item_delivered_$key"];
		}
		
		public function read($id) {
			
			$data = $this->model->read($id);
			
			if ($data) {
				$this->data = $data;
				$this->id = $data['mps_ordersheet_item_delivered_id'];
				$this->item = $data['mps_ordersheet_item_delivered_ordersheet_item_id'];
				return $data;
			}
		}
		
		public function add_pos($pos, $quantity) {
			if ($this->item) {
				$id = $this->model->add_pos($this->item, $pos, $quantity);
				$this->read($id);
				return $id;
			}
		}
		
		public function add_warehouse($warehouse, $quantity) {
			if ($this->item) {
				$id = $this->model->add_warehouse($this->item, $warehouse, $quantity);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($quantity) {
			if ($this->id) {
				$result = $this->model->update($this->id, $quantity);
				$this->read($this->id);
				return $result;
			}
		}
		
		public function confirm() {
			if ($this->id) {
				return $this->model->confirm($this->id);
			}
		}

		public function delete() {
			if ($this->id) { 
				return $this->model->delete($this->id);
			}
		}

		public function delete_all() {
			if ($this->item) {
				return $this->model->delete_all($this->item);
			}
		}
		
		public function load($filters=null) { 
			if ($this->item) { 
				return $this->model->load($this->item, $filters);
			}
		}
	}
	