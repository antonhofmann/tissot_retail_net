<?php 
/**
 *  Master Sheet Sales Orders Export Items to Excel File
 * 
 *  Export all client launch plans with customer numbers and week number
 * 
 *  @author admir.serifi@mediaparx.ch
 *  @copyright Mediaparx AG
 *
 *  Date Created: 	2014-11-02
 *  Date Modified:	2012-11-02
 */
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";

// execution time
ini_set('max_execution_time', 120);
ini_set('memory_limit', '1024M');

define(DEBUGGING_MODE, false);

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// request :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$id = $_REQUEST['id'];
$_CUSTOMER_NUMBERS = $_REQUEST['customernumber'];
$_REQUEST_TIME = $_SERVER['REQUEST_TIME'];
$_CURRENT_DATE = date('Y-m-d');

$_CONSOLE = array();
$_ERRORS = array();
$_MESSAGES = array();

// database model
$model = new Model($application);


// mastet sheet ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$mastersheet = new Modul($application);
$mastersheet->setTable('lps_mastersheets');
$mastersheet->read($id);

if (!$mastersheet->id) {
	$_ERRORS[] = "Master Sheet not found.";
}

$_MASTERSHEET_TOTAL_WEEKS = $mastersheet->data['lps_mastersheet_weeks'];
$_MASTERSHEET_FIRST_WEEK = $mastersheet->data['lps_mastersheet_week_number_first'];

$_STATE_EXPORTED = 10;
$_STATE_PARTIALLY_EXPORTED = 8;

$mastersheetName = File::normalize($mastersheet->data['lps_mastersheet_name']);
$mastersheetName = str_replace('/', '', $mastersheetName);

// export file
$_EXPORT_PATH = "/data/$application/salesorders/".date('Y');
$_FILENAME  = "salesorder_".$mastersheetName;


//	get launch plann items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEMS = array();

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT
			lps_launchplan_item_id AS item,
			lps_launchplan_id AS launchplan,
			lps_launchplan_address_id AS company,
			lps_reference_code AS code,
			lps_reference_ean_number AS ean,
			lps_launchplan_item_quantity_approved AS quantity,
			lps_launchplan_item_order_date AS order_date
		FROM lps_launchplan_items
		INNER JOIN lps_references ON lps_reference_id = lps_launchplan_item_reference_id
		INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
		WHERE lps_launchplan_item_quantity_approved > 0 AND lps_launchplan_mastersheet_id = ?
		ORDER BY lps_reference_code
	");

	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();

	if ($result) {
		
		foreach ($result as $row) {

			$launchplan = $row['launchplan'];
			$item = $row['item'];

			if ($_CUSTOMER_NUMBERS[$item]) {
				$_ITEMS[$launchplan][$item]['code'] = $row['code'];
				$_ITEMS[$launchplan][$item]['quantity'] = $row['quantity'];
			}
		}

	} else {
		$_ERRORS[] = 'Launch plan items not found';
	}
}

$_CONSOLE['items'] = $_ITEMS;


//	get launch plan item week quantities :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEM_WEEK_QUANTITIES = array();

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT 
			lps_launchplan_item_week_quantity_id AS id,
			lps_launchplan_item_week_quantity_item_id AS item, 
			lps_launchplan_item_week_quantity_week AS week, 
			lps_launchplan_item_week_quantity_quantity_approved AS quantity,
			lps_launchplan_item_week_quantity_pon AS pon
		FROM lps_launchplan_items 
		INNER JOIN lps_launchplan_item_week_quantities ON lps_launchplan_item_id = lps_launchplan_item_week_quantity_item_id
		INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
		WHERE lps_launchplan_mastersheet_id = ?
	");

	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();

	if ($result) {
		
		foreach ($result as $row) {
			
			$id = $row['id'];
			$item = $row['item'];
			$week = $row['week'];
			$quantity = $row['quantity'];

			if ($_CUSTOMER_NUMBERS[$item]) {
				$_ITEM_WEEK_QUANTITIES[$item][$week] = $quantity;
			}
		}
	}
}

$_CONSOLE['item.weeks'] = $_ITEM_WEEK_QUANTITIES;


//	build orders data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ORDERS = array();
$_ITEMS_UPDATE_DATA = array();
$_ITEM_WEEKS_UPDATE_DATA = array();

if (!$_ERRORS && $_ITEMS) {
			
	foreach ($_ITEMS as $launchplan => $items) {
		foreach ($items as $item => $row) {

			// item is requested for export
			if ($_CUSTOMER_NUMBERS[$item]) {

				// set item update data
				$_ITEMS_UPDATE_DATA[$item] = array(
					'customer' => $_CUSTOMER_NUMBERS[$item],
					'date' => $_CURRENT_DATE
				);

				// group orders by order and week
				for ($i=0; $i < $_MASTERSHEET_TOTAL_WEEKS; $i++) { 
					
					$week = $_MASTERSHEET_FIRST_WEEK+$i;
					$key = "$item|$week";
					$quantity = $_ITEM_WEEK_QUANTITIES[$item][$week];

					if ($quantity) {

						$_ORDERS[$launchplan][$week][$item] = array(
							'quantity' => $quantity,
							'customer' => $_CUSTOMER_NUMBERS[$item],
							'code' => $row['code']
						);

					} else {
						//$_ERRORS[] = "Missing quantities for item $item";
					}
				}
			}
		}
	}
}

$_CONSOLE['items.update.data'] = $_ITEMS_UPDATE_DATA;
$_CONSOLE['orders'] = $_ORDERS;
	

//	build export data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$_EXPORTS = array();
$_PONS = array();

if (!$_ERRORS && $_ORDERS) {
	
	foreach ($_ORDERS as $launchplan => $weeks) {
		
		foreach ($weeks as $week => $items) {

			$data = array();
			
			// purchase order number
			$pon = $_PONS[$pon] ? $_PONS[$pon]+1 : $_REQUEST_TIME+1;
			$_PONS[$pon] = $pon;

			$data['launchplan'] = $launchplan;
			$data['week'] = $week;
			$data['pon'] = $pon;
			
			$line = 1;

			foreach ($items as $item => $row) {

				$data['customer'] = $row['customer'];

				$data['items'][$item] = array(
					'line' => $line,
					'code' => $row['code'],
					'quantity' => $row['quantity']
				);

				$_ITEM_WEEKS_UPDATE_DATA[$item][$week] = array(
					'pon' => $pon,
					'line' => $line,
					'date' => date('Y-m-d', strtotime($row['date']))
				);

				$line++;
			}

			$_EXPORTS[] = $data;
		}
	}
}

$_CONSOLE['exports'] = $_EXPORTS;

//	export excel files :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_SALES_ORDERS = array();

if (!$_ERRORS && $_EXPORTS) {

	$_EXPORT_STAMP = $_SERVER['REQUEST_TIME'];

	$sth = $model->db->prepare("
		INSERT INTO lps_salesorders (
			lps_salesorders_launchplan_id,
			lps_salesorders_stamp,
			lps_salesorders_file,
			user_created
		) VALUES (?,?,?,?)
	");

	foreach ($_EXPORTS as $export) {

		$row=1;
		$launchplan = $export['launchplan'];
		$week = $export['week'];
		$customer = str_replace('/', '', $export['customer']);
		$pon = $export['pon'];

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, 'Reference');
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, 'Quantity');

		if ($export['items']) {
			
			foreach ($export['items'] as $i => $item) {
				$row++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $item['code']);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $item['quantity']);
			}
		}

		// create launchplan directory
		$dir = $_EXPORT_PATH.'/'.$launchplan;
		Dir::make($dir);

		// filename
		$filename = $dir.'/'.$_FILENAME.'_'.$customer."_".$week.'_'.$pon.'.xls';
		$filename = str_replace(' ', '_', $filename);

		// export file
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save($_SERVER['DOCUMENT_ROOT'].$filename);
		
		if (file_exists($_SERVER['DOCUMENT_ROOT'].$filename)) {

			@chmod($_SERVER['DOCUMENT_ROOT'].$filename, 0666);

			$data = array($launchplan, $_EXPORT_STAMP, $filename, $user->login);

			if (DEBUGGING_MODE) $update = true;
			else $update = $sth->execute($data);
			
			if ($update) {
				$_MESSAGES[] = "File $filename is exported";
				$_SALES_ORDERS[] = $data;
			}
			else $_ERRORS[] = "File $filename is NOT exported";

		} else {
			$_ERRORS[] = "File $filename is NOT exported";
		}
	}
}

$_CONSOLE['sales.orders'] = $_SALES_ORDERS;


//	update launch plan items week data :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$failures = false;

if (!$_ERRORS && $_SALES_ORDERS && $_ITEM_WEEKS_UPDATE_DATA) {

	$sth = $model->db->prepare("
		UPDATE lps_launchplan_item_week_quantities SET
			lps_launchplan_item_week_quantity_pon = ?,
			lps_launchplan_item_week_quantity_line = ?,
			lps_launchplan_item_week_quantity_delivery_date = ?,
			user_modified = ?,
			date_modified = NOW()
		WHERE lps_launchplan_item_week_quantity_item_id = ? AND lps_launchplan_item_week_quantity_week = ?
	");

	foreach ($_ITEM_WEEKS_UPDATE_DATA as $item => $weeks) {

		foreach ($weeks as $week => $row) {
		
			if (!$failures) {
				
				if (DEBUGGING_MODE) $update = true;
				else $update = $sth->execute(array($row['pon'], $row['line'], $row['date'], $user->login, $item, $week));

				$failures = !$update;
				$_MESSAGES[] = $update ? "Update item week: $item, $week" : "ERROR on update item week: $item, $week";
			}
		}
	}

	if ($failures) {
		$_ERRORS[] = "Error by updating Launch Plan Item Week Data.";
	}
}


//	update launch plan items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$failures = false;

if (!$_ERRORS && $_SALES_ORDERS && $_ITEMS_UPDATE_DATA) {

	$sth = $model->db->prepare("
		UPDATE lps_launchplan_items SET
			lps_launchplan_item_customernumber = ?,
			lps_launchplan_item_order_date = ?,
			user_modified = ?
		WHERE lps_launchplan_item_id = ?
	");

	foreach ($_ITEMS_UPDATE_DATA as $item => $row) {
		
		if (!$failures) {

			if (DEBUGGING_MODE) $update = true;
			else $update = $sth->execute(array($row['customer'], $row['date'],$user->login, $item));

			$failures = !$update;
			$_MESSAGES[] = $update ? "Update item: $item" : "ERROR on update item: $item";
		}
	}

	if ($failures) {
		$_ERRORS[] = "Error by updating Launch Plan Items.";
	}
}

//	update launch plans ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$failures = false;
$_LAUNCHPLANS = array();

if (!$_ERRORS && $_SALES_ORDERS) {

	// get launchplan items
	$sth = $model->db->prepare("
		SELECT
			lps_launchplan_item_id AS item,
			lps_launchplan_id AS launchplan,
			lps_launchplan_item_order_date AS order_date
		FROM lps_launchplan_items
		INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
		WHERE lps_launchplan_item_quantity_approved > 0 AND lps_launchplan_mastersheet_id = ?
	");

	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();

	foreach ($result as $row) {

		$launchplan = $row['launchplan'];

		$_LAUNCHPLANS[$launchplan]['items'] = $_LAUNCHPLANS[$launchplan]['items']+1;

		if ($row['order_date']) {
			$_LAUNCHPLANS[$launchplan]['exported'] = $_LAUNCHPLANS[$launchplan]['exported']+1;
		}
	}

	if ($_LAUNCHPLANS) {

		$sth = $model->db->prepare("
			UPDATE lps_launchplans SET 
				lps_launchplan_workflowstate_id = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE lps_launchplan_id = ?
		");

		foreach ($_LAUNCHPLANS as $launchplan => $total) {
			
			if ($total['exported'] > 0) {
				if (!$failures) {
					
					$state = $total['exported']==$total['items'] ? $_STATE_EXPORTED : $_STATE_PARTIALLY_EXPORTED;
					
					if (DEBUGGING_MODE) $update = true;
					else $update = $sth->execute(array($state, $user->login, $launchplan));
					
					$failures = !$update;
					$_MESSAGES[] = $update ? "Update order sheet: $launchplan" : "ERROR on update order sheet: $order";
				}
			}
		}

		if ($failures) {
			$_ERRORS[] = "Error by updating Launch Plans.";
		}
	}	
}

$_CONSOLE['launchplans'] = $_LAUNCHPLANS;


//	arichve master sheet :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$failures = false;

if (!$_ERRORS && $_SALES_ORDERS) {

	// total master sheet orders
	$sth = $model->db->prepare("
		SELECT COUNT(lps_launchplan_id) AS total
		FROM lps_launchplans
		WHERE lps_launchplan_mastersheet_id = ?
	");

	$sth->execute(array($mastersheet->id));
	$result = $sth->fetch();
	$totalMasterSheetOrders = $result['total'];

	// total master sheet exported orders
	$sth = $model->db->prepare("
		SELECT COUNT(lps_launchplan_id) AS total
		FROM lps_launchplans
		WHERE lps_launchplan_mastersheet_id = ? AND lps_launchplan_workflowstate_id IN (9,10)
	");

	$sth->execute(array($mastersheet->id));
	$result = $sth->fetch();
	$totalMasterSheetExportedOrders = $result['total'];

	$_CONSOLE['mastersheet.launchplans']['total'] = $totalMasterSheetOrders;
	$_CONSOLE['mastersheet.launchplans']['exported'] = $totalMasterSheetExportedOrders;

	// set master sheet in archive
	if ($totalMasterSheetOrders && $totalMasterSheetOrders == $totalMasterSheetExportedOrders) {
		
		$sth = $model->db->prepare("
			UPDATE lps_mastersheets SET 
				lps_mastersheet_archived = 1,
				user_modified = ?,
				date_modifeid = NOW()
			WHERE lps_mastersheet_id = ?
		");

		if (DEBUGGING_MODE) $update = true;
		else $update = $sth->execute(array($user->login, $mastersheet->id));
		
		$failures = !$update;
		$_MESSAGES[] = $update ? "Master sheet archived" : "ERROR on master sheet archive";
	}

	if ($failures) {
		$_ERRORS[] = "Error by updating Master Sheet.";
	}
}


//	responding :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$response['errors'] = $_ERRORS;

if (DEBUGGING_MODE) {
	$response['messages'] = $_MESSAGES;
	$response['sources'] = $_CONSOLE;
} else {
	$response['success'] = $_ERRORS ? false : true;
	$response['message'] = $_ERRORS ? 'Sales orders are not submitted.' : 'Sales orders are succefully submitted.';
}

header('Content-Type: text/json');
echo json_encode($response);
	