<?php 
/**
 * 	Master Sheet Sales Orders Items List
 * 
 * 	Load all companies with customer numbers and shipto numbers and goup by items
 * 	Assign all master sheet material collection delivery dates as proposed delivery date for companies
 * 	For all companies with more then une customer or shipto number, build select dropdowns as opcion
 * 	
 * 	IMPORTANT: It's will be loaded only selectetd items from sales orders export page
 * 
 * 	@author admir.serifi@mediaparx.ch
 * 	@copyright Mediaparx AG
 * 	@version 1.1 (removed item collections)
 */
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$selectedItems = $_REQUEST['item'];
$id = $_REQUEST['mastersheet'];

$MPS = in_array($application, array('mps')) ? true : false;
$LPS = in_array($application, array('lps')) ? true : false;

// db application model
$model = new Model($application);

switch ($application) {
	
	case 'mps':
	

		$tableName = 'mps_mastersheets';
		
		$qClinets = "
			SELECT DISTINCT
				mps_ordersheet_id AS id,
				address_company AS company,
				address_mps_customernumber AS customernumber,
				address_mps_shipto AS shipto
			FROM mps_ordersheet_items	
			INNER JOIN mps_ordersheets ON  mps_ordersheet_id = mps_ordersheet_item_ordersheet_id 
			INNER JOIN db_retailnet.addresses ON mps_ordersheet_address_id = address_id 
			WHERE mps_ordersheet_mastersheet_id =  ?
		";

		$qCompanyAdditionals = "
			SELECT 
				address_id AS company,
				address_mps_customernumber AS customerno,
				address_additional_customerno2 AS customerno2,
				address_additional_customerno3 AS customerno3,
				address_additional_customerno4 AS customerno4,
				address_additional_customerno5 AS customerno5,
				address_mps_shipto AS shiptono,
				address_additional_shiptono2 AS shiptono2,
				address_additional_shiptono3 AS shiptono3,
				address_additional_shiptono4 AS shiptono4,
				address_additional_shiptono5 AS shiptono5
			FROM db_retailnet.address_additionals
			INNER JOIN mps_ordersheets ON mps_ordersheet_address_id = address_additional_address
			INNER JOIN db_retailnet.addresses ON address_id = address_additional_address
			WHERE mps_ordersheet_mastersheet_id = ?
		";

		$qCompanyDeliveryDataRules = "
			SELECT 
				mps_sap_hq_ordertype_id as sap_hq_ordertype_id,
				mps_sap_hq_ordertype_address_id ,
				mps_sap_hq_ordertype_customer_number ,
				mps_sap_hq_ordertype_shipto_number ,
				sap_hq_order_type_name ,
				sap_hq_order_reason_name ,
				mps_sap_hq_ordertype_is_default,
				mps_sap_hq_ordertype_is_free_goods ,
				mps_sap_hq_ordertype_is_for_dummies
			FROM db_retailnet.mps_sap_hq_ordertypes
			LEFT JOIN mps_ordersheets ON mps_ordersheet_address_id = mps_sap_hq_ordertype_address_id
			LEFT JOIN db_retailnet.addresses ON address_id = mps_sap_hq_ordertype_address_id
			LEFT JOIN db_retailnet.sap_hq_order_types ON sap_hq_order_type_id = mps_sap_hq_ordertype_sap_hq_ordertype_id
            LEFT JOIN db_retailnet.sap_hq_order_reasons ON  sap_hq_order_reason_id = mps_sap_hq_ordertype_sap_hq_orderreason_id
			WHERE mps_ordersheet_mastersheet_id = ? 
			ORDER BY mps_sap_hq_ordertype_address_id
		";

		$qMastersheetDeliveryDates = "
			SELECT 
				mps_mastersheet_delivery_date_material_planning_type_id AS group_id,
				mps_mastersheet_delivery_date_material_collection_category_id AS subgroup_id,
				DATE_FORMAT(mps_mastersheet_delivery_date_date,'%d.%m.%Y') AS delivery_date
			FROM mps_mastersheet_delivery_dates
			WHERE mps_mastersheet_delivery_date_mastersheet_id = ?
		";

		$qMastersheetItems = "
			SELECT DISTINCT
				mps_ordersheet_id AS ordersheet,
				mps_ordersheet_address_id AS client_id,
				address_company AS client_name,
				mps_ordersheet_item_id AS item,
				mps_material_id AS reference_id,
				CONCAT(mps_material_code, ', ', mps_material_name) AS reference_name,
				mps_material_material_planning_type_id AS group_id,
				mps_material_material_collection_category_id AS subgroup_id,
				mps_material_locally_provided AS locally_provided,
				mps_ordersheet_item_quantity_approved AS quantity_approved,
				country_name, 
				address_mps_customernumber AS customernumber,
				address_mps_shipto AS shipto
			FROM mps_ordersheet_items	
			INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id 
			INNER JOIN mps_ordersheets ON  mps_ordersheet_id = mps_ordersheet_item_ordersheet_id 
			INNER JOIN db_retailnet.addresses ON mps_ordersheet_address_id = address_id 
			INNER JOIN db_retailnet.countries ON address_country = country_id 
			WHERE mps_ordersheet_mastersheet_id = ?
			AND ( 
				mps_ordersheet_item_purchase_order_number IS NULL 
				OR mps_ordersheet_item_purchase_order_number = ''
			) 
			ORDER BY 
				mps_material_code asc, 
				mps_material_name asc, 
				country_name asc,
				address_company asc
		";

	break;

	case 'lps':
	

		$tableName = 'lps_mastersheets';

		$qMastersheetItems = "
			SELECT DISTINCT
				lps_launchplan_id AS ordersheet,
				lps_launchplan_address_id AS client_id,
				address_company AS client_name,
				country_name, 
				lps_launchplan_customer_number AS customernumber,
				lps_launchplan_item_id AS item,
				lps_reference_id AS reference_id,
				CONCAT(lps_reference_code, ', ', lps_reference_name) AS reference_name,
				lps_reference_product_group_id AS group_id,
				lps_reference_collection_category_id AS subgroup_id,
				lps_launchplan_item_quantity_approved AS quantity_approved
			FROM lps_launchplan_items	
			INNER JOIN lps_references ON lps_reference_id = lps_launchplan_item_reference_id 
			INNER JOIN lps_launchplans ON  lps_launchplan_id = lps_launchplan_item_launchplan_id 
			INNER JOIN db_retailnet.addresses ON lps_launchplan_address_id = address_id 
			INNER JOIN db_retailnet.countries ON address_country = country_id 
			WHERE lps_launchplan_mastersheet_id = ? AND lps_launchplan_item_quantity_approved > 0
			AND ( lps_launchplan_item_order_date IS NULL OR lps_launchplan_item_order_date = '' ) 
			ORDER BY 
				lps_reference_code asc, 
				lps_reference_name asc, 
				country_name asc,
				address_company asc
		";

		$qWeekQuantities = "
			SELECT 
				lps_launchplan_item_week_quantity_id AS id,
				lps_launchplan_item_week_quantity_item_id AS item, 
				lps_launchplan_item_week_quantity_week AS week, 
				lps_launchplan_item_week_quantity_quantity_approved AS quantity_approved,
				lps_launchplan_item_week_quantity_pon AS pon
			FROM lps_launchplan_items 
			INNER JOIN lps_launchplan_item_week_quantities ON lps_launchplan_item_id = lps_launchplan_item_week_quantity_item_id
			INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
			WHERE lps_launchplan_mastersheet_id = ?
		";

	break;
}

// master sheet ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$mastersheet = new Modul($application);
$mastersheet->setTable($tableName);
$mastersheet->read($id);

$firstWeek = $mastersheet->data['lps_mastersheet_week_number_first'];
$totalWeeks = $mastersheet->data['lps_mastersheet_weeks'];

// sap export data :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("
	SELECT sap_hq_order_type_id, TRIM(sap_hq_order_type_name) AS name
	FROM db_retailnet.sap_hq_order_types
")->fetchAll();

$_SAP_ORDER_TYPES = _array::extract($result);

$result = $model->query("
	SELECT sap_hq_order_reason_id, TRIM(sap_hq_order_reason_name) AS name
	FROM db_retailnet.sap_hq_order_reasons
")->fetchAll();


$_SAP_ORDER_REASONS = _array::extract($result);
$_SAP_ORDER_REASONS[0] = "";
asort($_SAP_ORDER_REASONS);

$result = $model->query("
	SELECT country_iso3166, country_name 
	FROM db_retailnet.countries 
	WHERE country_used_in_cross_trade_shipments = 1 
	ORDER BY country_name
")->fetchAll();

$_CROSS_TRADE_SHIPPMENTS = _array::extract($result);

if ($_CROSS_TRADE_SHIPPMENTS) {
	
	$dropdownCrossShippments = "<select name='cross_trade_shipments[{item}]' class='ado-modal-input ado-modal-input-small no-block' >";
	$dropdownCrossShippments .= "<option value=''>Select</option>";
							
	foreach ($_CROSS_TRADE_SHIPPMENTS as $key => $value) {
		$dropdownCrossShippments .= "<option value='$key'>$value</option>";
	}

	$dropdownCrossShippments .="</select>";
}


// company additionals data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$CompaniesCustomerNumbers = array();
$CompaniesShiptoNumbers = array();
$companiesFreeGoodsNumbers = array();

if ($mastersheet->id && $qCompanyAdditionals) {

	$sth = $model->db->prepare($qCompanyAdditionals);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();

	if ($result) {
					
		foreach ($result as $row) {
			
			$company = $row['company'];

			// customer numbers
			$CompaniesCustomerNumbers[$company] = array_filter(array(
				$row['customerno'],
				$row['customerno2'],
				$row['customerno3'],
				$row['customerno4'],
				$row['customerno5']
			));

			// shipto numbers
			$CompaniesShiptoNumbers[$company] = array_filter(array(
				$row['shiptono'],
				$row['shiptono2'],
				$row['shiptono3'],
				$row['shiptono4'],
				$row['shiptono5']
			));
		}
	}
}


// company default delivery data rules ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$CompaniesDeliveryDataRules = array();

if ($mastersheet->id && $qCompanyDeliveryDataRules) {

	$sth = $model->db->prepare($qCompanyDeliveryDataRules);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();

	if ($result) {
					
		foreach ($result as $row) {
			$CompaniesDeliveryDataRules[$row["mps_sap_hq_ordertype_address_id"]][$row["sap_hq_ordertype_id"]] = $row;

			if ($row['mps_sap_hq_ordertype_is_free_goods']) {
				$companiesFreeGoodsNumbers[$row['mps_sap_hq_ordertype_address_id']] = $row['mps_sap_hq_ordertype_customer_number'];
			}
		}
	}
}

// get delivery dates ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$mastersheetDeliveryDates = array();

if ($mastersheet->id && $qMastersheetDeliveryDates) {

	$sth = $model->db->prepare($qMastersheetDeliveryDates);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$group = $row['group_id'];
			$subgroup = $row['subgroup_id'];
			$mastersheetDeliveryDates[$group][$subgroup] = $row['delivery_date'];
		}
	}
}


// get item week data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$weekQuantities = array();

if ($mastersheet->id && $qWeekQuantities) {

	$sth = $model->db->prepare($qWeekQuantities);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$item = $row['item'];
			$week = $row['week'];
			$weekQuantities[$item][$week] = $row['quantity_approved'];
		}
	}
}

// get master sheet items ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$datagrid = array();
$clinetsWithoutAdditionals = array();
$hiddenFields = null;
$visibleWeeks = array();
$freeGoodReferences = array();

if ($mastersheet->id && $qMastersheetItems) {

	$sth = $model->db->prepare($qMastersheetItems);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();

	if ($result) {

		foreach ($result as $row) { 
			
			$ordersheet 	= $row['ordersheet'];
			$client 		= $row['client_id'];
			$item 			= $row['item'];
			$reference 		= $row['reference_id'];
			$group 			= $row['group_id'];
			$subgroup 		= $row['subgroup_id'];
			$customerNumber = $row['customernumber'];
			$shiptoNumber 	= $row['shipto'];
			$deliveryDate 	= $mastersheetDeliveryDates ? $mastersheetDeliveryDates[$group][$subgroup] : null;

			$fieldCustomerNumber = null;
			$fieldShiptoNumber = null;

			// export data validation
			$error = false;
			if ($MPS) $error = !$customerNumber || !$shiptoNumber ? true : false;
			if ($LPS) $error = !$customerNumber ? true : false;
			
			// failure registry
			if ($error) {
				//$clinetsWithoutAdditionals[$client] = "<span class=error>{$row[company]}</span>";
				$clinetsWithoutAdditionals[$client] = "<span class=error>{$row[client_name]}</span>";

				
			}
			
			// if reference is selected from user
			if (!$error && $selectedItems[$reference]) { 
				
				//check if there is a default setting
				$default_setting = array();
				$default_setting_dummies = array();
				if($MPS and array_key_exists($client, $CompaniesDeliveryDataRules)) {
				
					foreach($CompaniesDeliveryDataRules[$client] as $key=>$default_settings) {
						
						if($default_settings['mps_sap_hq_ordertype_is_default'] == 1) {
							$default_setting = $default_settings;
						}

						if($default_settings['mps_sap_hq_ordertype_is_for_dummies'] == 1) {
							$default_setting_dummies = $default_settings;
						}
					}
				
				}
				
				//apply default settings
				if($MPS and (count($default_setting) > 0 or count($default_setting_dummies) > 0)) {

					
					//planning type dummies with separate default for EULOG
					if($group == 32 and count($default_setting_dummies) > 0) {
						$default_setting = $default_setting_dummies;
					}

					// if company has more then one customer numbers
					// show this field as dropdown field
					if ($CompaniesCustomerNumbers[$client]) {

						$CompaniesCustomerNumbers[$client] = array_unique($CompaniesCustomerNumbers[$client]);
						$classFreeGood = $companiesFreeGoodsNumbers[$client] ? 'free-good' : null;
						
						$fieldCustomerNumber = "<select name=customernumber[$item] class='ado-modal-input ado-modal-input-small $classFreeGood' onchange='javascript:change_delivery_details($item, $client)'>";

						
						
						foreach ($CompaniesCustomerNumbers[$client] as $value) {
							$selected = ($value==$default_setting['mps_sap_hq_ordertype_customer_number']) ? "selected=selected" : null;
							$fieldCustomerNumber .= "<option value='$value' $selected>$value</option>";
						}
						
						$fieldCustomerNumber .="</select>";

					} else {
						$fieldCustomerNumber = $customerNumber;
						$hiddenFields .= "<input type=hidden name=customernumber[$item] value='$customerNumber'>";
					}
					
				
					// if company has more then one shipto numbers
					// show this field as dropdown field
					
						
					if ($CompaniesShiptoNumbers[$client]) {
					
						$CompaniesShiptoNumbers[$client] = array_unique($CompaniesShiptoNumbers[$client]);
						$fieldShiptoNumber ="<select name=shiptonumber[$item] class='ado-modal-input ado-modal-input-small' >";
						
						foreach ($CompaniesShiptoNumbers[$client] as $value) {
							$selected = ($value==$default_setting['mps_sap_hq_ordertype_shipto_number']) ? "selected=selected" : null;
							$fieldShiptoNumber .= "<option value='$value' $selected>$value</option>";
						}
						
						$fieldShiptoNumber .="</select>";

					} else {
						$fieldShiptoNumber = $shiptoNumber;
						$hiddenFields .= "<input type=hidden name=shiptonumber[$item] value='$shiptoNumber'>";
					}

					// sap order type
					if ($_SAP_ORDER_TYPES) {

						$fieldSapOrderType = "<select name=sap_order_types[$item] class='ado-modal-input ado-modal-input-small' >";

						foreach ($_SAP_ORDER_TYPES as $key => $value) {
							$selected = ($value==$default_setting['sap_hq_order_type_name']) ? "selected=selected" : null;
							$fieldSapOrderType .= "<option value='$value' $selected>$value</option>";
						}

						$fieldSapOrderType .= "</select>";
					}

					// sap order reason
					if ($_SAP_ORDER_REASONS) {

						$fieldSapOrderReason = "<select name=sap_order_reasons[$item] class='ado-modal-input ado-modal-input-small' >";

						foreach ($_SAP_ORDER_REASONS as $key => $value) {
							$selected = ($value==$default_setting['sap_hq_order_reason_name']) ? "selected=selected" : null;
							$fieldSapOrderReason .= "<option value='$value' $selected>$value</option>";
						}

						$fieldSapOrderReason .= "</select>";
					}
					

					// locally povided items
					if ($row['locally_provided']) {
						$date = $deliveryDate ? $deliveryDate : date('d.m.Y');
						$hiddenFields .= "<input type=hidden name=customernumber[$item] value='$customerNumber'>";
						$hiddenFields .= "<input type=hidden name=shiptonumber[$item] value='$shiptoNumber'>";
						$hiddenFields .= "<input type=hidden class=date name=desire[$item] value='$date'>";
					}
					
					// company free goods
					if($companiesFreeGoodsNumbers[$client]) {
						$freeGoodReferences[$reference] = true;
					}

					// item has approved quantites and is not localy provided, show it as list item
					if ($row['quantity_approved'] && !$row['locally_provided']) { 
						
						$datagrid[$reference]['name'] = $row['reference_name'];
						$datagrid[$reference]['data'][$ordersheet]['country_name'] = $row['country_name'];
						$datagrid[$reference]['data'][$ordersheet]['address_company'] = $row['client_name'];
						$datagrid[$reference]['data'][$ordersheet]['address_id'] = $client;
						$datagrid[$reference]['data'][$ordersheet]['quantity'] = $row['quantity_approved'];
						$datagrid[$reference]['data'][$ordersheet]['address_mps_customernumber'] = $fieldCustomerNumber;
						
						if ($MPS) {
							$datagrid[$reference]['data'][$ordersheet]['sap_order_type'] = $fieldSapOrderType;
							$datagrid[$reference]['data'][$ordersheet]['sap_order_reason'] = $fieldSapOrderReason;
							$datagrid[$reference]['data'][$ordersheet]['address_mps_shipto'] = $fieldShiptoNumber;
							$datagrid[$reference]['data'][$ordersheet]['is_cross_trade_shipment'] = "<input type='checkbox' name='is_cross_trade_shipment[$item]' value='$item' class='is_cross_trade_shipment' /> $translate->mps_salesorder_sap_is_cross_trade_shipment";
							$datagrid[$reference]['data'][$ordersheet]['cross_trade_shipment'] = str_replace('{item}', $item, $dropdownCrossShippments);
							$datagrid[$reference]['data'][$ordersheet]['delivery_date'] = "<input type=text class='date required ado-modal-input ado-modal-input-small' name=desire[$item] value='$deliveryDate'>";
						}

					}
				}
				else {
					// sap order type
					if ($MPS && $_SAP_ORDER_TYPES) {

						$fieldSapOrderType = "<select name=sap_order_types[$item] class='ado-modal-input ado-modal-input-small' >";

						foreach ($_SAP_ORDER_TYPES as $key => $value) {
							$fieldSapOrderType .= "<option value='$value'>$value</option>";
						}

						$fieldSapOrderType .= "</select>";
					}

					// sap order reason
					if ($MPS && $_SAP_ORDER_REASONS) {

						$fieldSapOrderReason = "<select name=sap_order_reasons[$item] class='ado-modal-input ado-modal-input-small' >";

						foreach ($_SAP_ORDER_REASONS as $key => $value) {
							$fieldSapOrderReason .= "<option value='$value'>$value</option>";
						}

						$fieldSapOrderReason .= "</select>";
					}

					// if company has more then one customer numbers
					// show this field as dropdown field
					if ($MPS) {
						
						if ($CompaniesCustomerNumbers[$client]) {

							$CompaniesCustomerNumbers[$client] = array_unique($CompaniesCustomerNumbers[$client]);
							$classFreeGood = $companiesFreeGoodsNumbers[$client] ? 'free-good' : null;
							
							$fieldCustomerNumber = "<select name=customernumber[$item] class='ado-modal-input ado-modal-input-small $classFreeGood' >";
							
							foreach ($CompaniesCustomerNumbers[$client] as $value) {
								$selected = ($value==$customerNumber) ? "selected=selected" : null;
								$fieldCustomerNumber .= "<option value='$value' $selected>$value</option>";
							}
							
							$fieldCustomerNumber .="</select>";

						} else {
							$fieldCustomerNumber = $customerNumber;
							$hiddenFields .= "<input type=hidden name=customernumber[$item] value='$customerNumber'>";
						}
					}
				
					// if company has more then one shipto numbers
					// show this field as dropdown field
					if ($MPS) {
						
						if ($CompaniesShiptoNumbers[$client]) {
						
							$CompaniesShiptoNumbers[$client] = array_unique($CompaniesShiptoNumbers[$client]);
							$fieldShiptoNumber ="<select name=shiptonumber[$item] class='ado-modal-input ado-modal-input-small' >";
							
							foreach ($CompaniesShiptoNumbers[$client] as $value) {
								$selected = ($value==$shiptoNumber) ? "selected=selected" : null;
								$fieldShiptoNumber .= "<option value='$value' $selected>$value</option>";
							}
							
							$fieldShiptoNumber .="</select>";

						} else {
							$fieldShiptoNumber = $shiptoNumber;
							$hiddenFields .= "<input type=hidden name=shiptonumber[$item] value='$shiptoNumber'>";
						}
					}

					if ($LPS) {
						$fieldCustomerNumber = $customerNumber;
						$hiddenFields .= "<input type=hidden name=customernumber[$item] value='$customerNumber'>";
					}

					// locally povided items
					if ($row['locally_provided']) {
						$date = $deliveryDate ? $deliveryDate : date('d.m.Y');
						$hiddenFields .= "<input type=hidden name=customernumber[$item] value='$customerNumber'>";
						$hiddenFields .= "<input type=hidden name=shiptonumber[$item] value='$shiptoNumber'>";
						$hiddenFields .= "<input type=hidden class=date name=desire[$item] value='$date'>";
					}

					// company free godds
					if ($MPS && $companiesFreeGoodsNumbers[$client]) {
						$freeGoodReferences[$reference] = true;
					}

					// item has approved quantites and is not localy provided, show it as list item
					if ($row['quantity_approved'] && !$row['locally_provided']) { 
						
						$datagrid[$reference]['name'] = $row['reference_name'];
						$datagrid[$reference]['data'][$ordersheet]['country_name'] = $row['country_name'];
						$datagrid[$reference]['data'][$ordersheet]['address_company'] = $row['client_name'];
						$datagrid[$reference]['data'][$ordersheet]['address_id'] = $client;
						$datagrid[$reference]['data'][$ordersheet]['quantity'] = $row['quantity_approved'];
						$datagrid[$reference]['data'][$ordersheet]['address_mps_customernumber'] = $fieldCustomerNumber;
						
						if ($MPS) {
							$datagrid[$reference]['data'][$ordersheet]['sap_order_type'] = $fieldSapOrderType;
							$datagrid[$reference]['data'][$ordersheet]['sap_order_reason'] = $fieldSapOrderReason;
							$datagrid[$reference]['data'][$ordersheet]['address_mps_shipto'] = $fieldShiptoNumber;
							$datagrid[$reference]['data'][$ordersheet]['is_cross_trade_shipment'] = "<input type='checkbox' name='is_cross_trade_shipment[$item]' value='$item' class='is_cross_trade_shipment' /> $translate->mps_salesorder_sap_is_cross_trade_shipment";
							$datagrid[$reference]['data'][$ordersheet]['cross_trade_shipment'] = str_replace('{item}', $item, $dropdownCrossShippments);
							$datagrid[$reference]['data'][$ordersheet]['delivery_date'] = "<input type=text class='date required ado-modal-input ado-modal-input-small' name=desire[$item] value='$deliveryDate'>";
						}

						if ($LPS && $totalWeeks) {

							for ($i=0; $i < $totalWeeks; $i++) { 
								
								$week = $firstWeek+$i;
								$key = "$item|$week";
								
								if ($weekQuantities[$item][$week]) {
									$visibleWeeks[$reference][$week] = true;
									$datagrid[$reference]['data'][$ordersheet]["week_quantity_$week"] = $weekQuantities[$item][$week];
									$datagrid[$reference]['data'][$ordersheet]["week_date_$week"] = "<input type=text class='date required ado-modal-input ado-modal-input-small' name=week_desire_date[$key] >";
								}
							}
						}
					}
				}
			}// end selected item
		} // end foreache
	} // end result
}


// output content ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($datagrid || $hiddenFields) {    
					
	$json['response'] = true;

	$content = null;

	if ($datagrid) {

		$btnFreeGood = "
			<label id='switch_free_goods' class='switch switch-green' style='width:95px'>
				<input type='checkbox' class='switch-input'>
				<span class='switch-label' data-on='Free Item' data-off='Free Item'></span>
				<span class='switch-handle'></span>
			</label>
		";
		
		foreach ($datagrid as $reference => $row) {

			$freeGood = $freeGoodReferences[$reference] ? $btnFreeGood  : null;

			$content  .= "<h5 >{$row[name]} $freeGood</h5>"; 

			$className = $MPS ? 'table-mps' : '';

			$content .= "<table class='listing over-effects $className'>";
			$content .= "<thead><tr>";
			
			$content .= "<th width='5%' valign='top' nowrap='nowrap' class='country_name' ><strong>$translate->country_name</strong></th>";
			$content .= "<th valign='top' class='address_company' style='min-width:240px' ><strong>$translate->address_company</strong></th>";
			$content .= "<th width='60px' valign='top' nowrap='nowrap' class='quantity' align='right' ><strong>$translate->quantity</strong></th>";


			$content .= "<th width='10%' valign='top' nowrap='nowrap' class='address_mps_customernumber' ><strong>$translate->address_mps_customernumber</strong></th>";

			if ($MPS) {
				$content .= "<th width='10%' valign='top' nowrap='nowrap' class='address_mps_shipto' ><strong>$translate->address_mps_shipto</strong></th>";
				
				$content .= "<th width='10%' valign='top' nowrap='nowrap' class='sap_order_type' ><strong>$translate->sap_order_type</strong></th>";
				$content .= "<th width='10%' valign='top' nowrap='nowrap' class='sap_order_reason' ><strong>$translate->sap_order_reason</strong></th>";
				$content .= "<th width='10%' valign='top' nowrap='nowrap' class='cross_trade_shipment' ><strong>Country for Cross Trade Shipment</strong></th>";

				$content .= "<th width='6%' valign='top' nowrap='nowrap' class='delivery_date' ><strong>$translate->delivery_date</strong></th>";
			}

			if ($LPS && $totalWeeks) {
				for ($i=0; $i < $totalWeeks; $i++) { 
					$week = $firstWeek+$i;
					if ($visibleWeeks[$reference][$week]) {
						$content .= "<th width='60px' valign='top' nowrap='nowrap' class='week_quantity_$week' align='right' ><strong>Week $week</strong></th>";
						//$content .= "<th width='100px' valign='top' nowrap='nowrap' class='week_date_$week' align='right' ><strong>Delivery Date</strong></th>";
					}
				}
			}
			
			$content .= "</tr></thead>";

			$content .= "<tbody>";

			foreach ($row['data'] as $i => $data) {

				$s++;

				$className = $s % 2 == 1 ? '-odd' : '-even';
				$rowspan = $MPS ? 1 : 1;
				
				$content .= "<tr id='row-$i' class='$className' >";

				$content .= "<td rowspan='$rowspan' id='country_name-$i' nowrap='nowrap' class='country_name'>{$data[country_name]}</td>";
				$content .= "<td rowspan='$rowspan' data_address_id='$data[address_id]' id='address_company-$i' class='address_company'>{$data[address_company]}</td>";

				$content .= "<td rowspan='$rowspan' id='quantity-$i' class='quantity' align='right' >{$data[quantity]}</td>";

				

				$content .= "<td id='address_mps_customernumber-$i' class='address_mps_customernumber' align='right' >{$data[address_mps_customernumber]}</td>";

				if ($MPS) {
					$content .= "<td id='address_mps_shipto-$i' class='address_mps_shipto' >{$data[address_mps_shipto]}</td>";
					
					$content .= "<td id='sap_order_type-$i' class='sap_order_type' >{$data[sap_order_type]}</td>";
					$content .= "<td id='sap_order_reason-$i' class='sap_order_reason' >{$data[sap_order_reason]}</td>";
					$content .= "<td id='cross_trade_shipment-$i' class='cross_trade_shipment' >{$data[cross_trade_shipment]}</td>";
					
					$content .= "<td id='delivery_date-$i' class='delivery_date' >{$data[delivery_date]}</td>";
				}


				if ($LPS && $totalWeeks) {
					for ($i=0; $i < $totalWeeks; $i++) { 
						$week = $firstWeek+$i;
						if ($visibleWeeks[$reference][$week]) {
							$k = "week_quantity_$week";
							$content .= "<td id='$k-$i' class='$k' align='right' >{$data[$k]}</td>";
							//$k = "week_date_$week";
							//$content .= "<td id='$k-$i' class='$k' align='right' >{$data[$k]}</td>";
						}
					}
				}

				$content .= "</tr>";

				/*
				if ($MPS) {
					$content .= "<tr class='$className'>";
					$content .= "<td colspan='2' class='is_cross_trade_shipment'><label for='is_cross_trade_shipment[$i]'>{$data[is_cross_trade_shipment]}</label></td>";
					$content .= "<td colspan='3' class='cross_trade_shipment'>{$data[cross_trade_shipment]}</td>";
					$content .= "</tr>";
				}
				*/
			}

			$content .= "</tbody>";
			$content .= "</table>";
			
			/*
			$table = new Table(array('class' => 'listing'));
			$table->datagrid = $row['data'];
			$table->country_name('width=5%', Table::ATTRIBUTE_NOWRAP);
			$table->address_company();
			$table->quantity('width=60px', Table::ATTRIBUTE_NOWRAP, Table::ATTRIBUTE_ALIGN_RIGHT);

			if ($MPS) {
				$table->sap_order_type('width=10%', Table::ATTRIBUTE_NOWRAP, 'caption=SAP Order Type');
				$table->sap_order_reason('width=10%', Table::ATTRIBUTE_NOWRAP, 'caption=SAP Order Reason');
			}

			$table->address_mps_customernumber('width=10%', Table::ATTRIBUTE_NOWRAP);
			
			if ($MPS) {
				$table->address_mps_shipto('width=10%', Table::ATTRIBUTE_NOWRAP);
				$table->delivery_date('width=6%', Table::ATTRIBUTE_NOWRAP);
			}

			if ($LPS && $totalWeeks) {
				
				for ($i=0; $i < $totalWeeks; $i++) { 
					
					$week = $firstWeek+$i;
					
					if ($visibleWeeks[$reference][$week]) {
						$fieldQuantity = "week_quantity_$week";
						$fieldDate = "week_date_$week";
						$table->$fieldQuantity('width=70px', "caption=Week $week", Table::ATTRIBUTE_NOWRAP, Table::ATTRIBUTE_ALIGN_RIGHT);
						//$table->$fieldDate('width=100px', "caption=Delivery Date", Table::ATTRIBUTE_NOWRAP);
					}
				}
			}
			
			$content .= $table->render();
			*/
		}
	}
	
	$json['content'] = $content.$hiddenFields;

} else {
	$json['response'] = false;
	$json['content']  = $json['notification'] = "Bad request. Master Sheet items not found";
}


// failures registry :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
if ($clinetsWithoutAdditionals) {	
	$json['response'] = false;
	$json['content']  = "<p class=error>Operation is not possible since either the customer number or the ship to informatione for the following companies is missing:</p>";
	$json['content'] .= "<p>".join(' ', array_values($clinetsWithoutAdditionals))."</p>";
}

if (!$mastersheet->id) {
	$json['response'] = false;
	$json['content']  = $json['notification'] = "Master Sheet not found";
}

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!headers_sent()) {
	header('Content-Type: text/json');
}

echo json_encode($json);
