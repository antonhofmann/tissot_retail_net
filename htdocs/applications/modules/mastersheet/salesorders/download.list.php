<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();


// request :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

// request filters
$_YEAR = $_REQUEST['year'];
$_MASTERSHEET = $_REQUEST['mastersheet'];
$_SEARCH =  $_REQUEST['search'] && $_REQUEST['search']<>$translate->search ? $_REQUEST['search'] : null;
$_DOWNLOADED = isset($_REQUEST['downloaded']) ? $_REQUEST['downloaded'] : 2;

// sql order
$_SORT = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "lps_mastersheet_name, lps_salesorders_stamp";
$_SORT_DIRECTION = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "ASC";

// pager
$_PAGE = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$_ROW_PER_PAGE =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$_OFFSET = ($_PAGE-1) * $_ROW_PER_PAGE;


// filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_FILTERS = array();

if ($_SEARCH) {
	$_FILTERS['search'] = "(
		lps_mastersheet_name LIKE '%$_SEARCH%'
		OR lps_salesorders.date_created LIKE '%$_SEARCH%'
		OR lps_salesorders.lps_salesorders_download_date LIKE '%$_SEARCH%'
		OR user_firstname LIKE '%$_SEARCH%'
		OR user_name LIKE '%$_SEARCH%'
	)";
}

if ($_MASTERSHEET) {
	$_FILTERS['country'] = "lps_mastersheet_id = $_MASTERSHEET";
}

if ($_YEAR) {
	$_FILTERS['year'] = "YEAR(lps_salesorders.date_created) = $_YEAR";
}

if ($_DOWNLOADED==1) $_FILTERS['default'] = "lps_salesorders_download_date IS NOT NULL";
elseif($_DOWNLOADED==2) $_FILTERS['default'] = "lps_salesorders_download_date IS NULL";


// dataloader ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$model = new Model($application);

$_DB_BINDS = array(
	'INNER JOIN lps_launchplans ON lps_launchplan_id = lps_salesorders_launchplan_id',
	'INNER JOIN lps_mastersheets ON lps_mastersheet_id = lps_launchplan_mastersheet_id',
	'INNER JOIN db_retailnet.addresses ON address_id = lps_launchplan_address_id',
	'INNER JOIN db_retailnet.countries ON country_id = address_country',
	'LEFT JOIN db_retailnet.users ON user_id = lps_salesorders_download_user'
);

$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		lps_salesorders_stamp,
		lps_mastersheet_id,
		lps_mastersheet_name,
		lps_salesorders.date_created AS created_on,
		DATE_FORMAT(lps_salesorders.date_created, '%d.%m.%Y %H:%i') AS created_date,
		lps_salesorders_download_date AS downloaded_on,
		DATE_FORMAT(lps_salesorders_download_date, '%d.%m.%Y') AS downloaded_date,
		IF (lps_salesorders_download_user, 
			CONCAT(user_firstname, ' ', user_name),
			''
		) AS downloaded_from
	FROM lps_salesorders
")
->bind($_DB_BINDS)
->filter($_FILTERS)
->order($_SORT, $_SORT_DIRECTION)
->offset($_OFFSET, $_ROW_PER_PAGE)
->fetchAll(); 

if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);

	$_LINK = "/applications/modules/mastersheet/salesorders/create.zip.php";

	$sth = $model->db->prepare("
		SELECT COUNT(lps_salesorders_id) AS total  
		FROM lps_salesorders
		WHERE lps_salesorders_stamp = ?
	");

	$icon = ui::extension('zip');

	foreach ($datagrid as $key => $row) {
		
		$sth->execute(array($key));
		$res = $sth->fetch();
		$datagrid[$key]['total_files'] = $res['total'];

		$datagrid[$key]['created_on'] = $row['created_date'];
		$datagrid[$key]['downloaded_on'] = $row['downloaded_date'];

		$id = $row['lps_mastersheet_id'];	
		$datagrid[$key]['download_button'] = "<a class='download-zip' href='$_LINK?application=$application&id=$id&download=$key' >$icon</a>";
	}

	Pager::instance(array(
		'page' => $_PAGE,
		'rows' => $_ROW_PER_PAGE,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$listIndex = Pager::instance()->index();
	$listControlls = Pager::instance()->controlls();
}


// toolbox :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_TOOLBOX = array();
$_TOOLBOX[] = "<input type=hidden id=page name=page value='$_PAGE' class='submit' />";
$_TOOLBOX[] = "<input type=hidden id=sort name=sort value='$_SORT' class='submit' />";
$_TOOLBOX[] = "<input type=hidden id=direction name=direction value='$_SORT_DIRECTION' class='submit' />";
$_TOOLBOX[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$_ROW_PER_PAGE' class='submit' />";

$_TOOLBOX[] = ui::searchbox();

$result = $model->query("
	SELECT DISTINCT lps_mastersheet_id AS id, lps_mastersheet_name AS caption
	FROM lps_salesorders
")->bind($_DB_BINDS)
->filter($_FILTERS)
->exclude('mastersheet')
->order('caption')
->fetchAll();

if ($result) {
	$_TOOLBOX[] = html::select($result, array(
		'name' => 'mastersheet',
		'id' => 'mastersheet',
		'class' => 'submit',
		'value' => $_MASTERSHEET,
		'caption' => $translate->all_mastersheets
	));
}

$downloaded = array(
	1 => 'Downloaded',
	2 => 'Not downloaded',
);

$_TOOLBOX[] = html::select($downloaded, array(
	'name' => 'downloaded',
	'id' => 'downloaded',
	'class' => 'submit',
	'value' => $_DOWNLOADED,
	'caption' => 'All'
));


$result = $model->query("
	SELECT DISTINCT YEAR(lps_salesorders.date_created) AS id,  YEAR(lps_salesorders.date_created) AS caption
	FROM lps_salesorders
")->bind($_DB_BINDS)
->filter($_FILTERS)
->exclude('year')
->order('caption')
->fetchAll();

if ($result) {
	$_TOOLBOX[] = html::select($result, array(
		'name' => 'year',
		'id' => 'year',
		'class' => 'submit',
		'value' => $_YEAR,
		'caption' => $translate->all_years
	));
}


$toolbox = join($_TOOLBOX);
$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>$toolbox</form></div>";


// render ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$table = new Table(array(
	'sort' => array(
		'column' => $_SORT, 
		'direction' => $_SORT_DIRECTION
	)
));

$table->datagrid = $datagrid;

$table->lps_mastersheet_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'caption=Master Sheet'
);

$table->created_on(
	'width=10%',
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'caption=Created On'
);

$table->downloaded_on(
	'width=10%',
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'caption=Downloaded On'
);

$table->downloaded_from(
	'width=10%',
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'caption=Downloade by'
);

$table->total_files(
	'width=5%',
	Table::ATTRIBUTE_NOWRAP,
	'caption=Files'
);


$table->download_button(
	'width=5%',
	Table::ATTRIBUTE_NOWRAP,
	'caption=Zip'
);



$table->footer($listIndex);
$table->footer($listControlls);
$table = $table->render();

echo $toolbox.$table;
