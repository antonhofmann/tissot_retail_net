<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

define(DEBUGGING_MODE, false);

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$ID = $_REQUEST['id'];
$_STAMP = $_REQUEST['download'];

$_CONSOLE = array();
$_ERRORS = array();
$_MESSAGES = array();
$_RESPONSE = array();

// db model
$model = new Model($application);

// mastersheet :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$mastersheet = new Modul($application);
$mastersheet->setTable('lps_mastersheets');
$mastersheet->read($ID);

if (!$mastersheet->id) {
	$_ERRORS[] = "Master Sheet not found.";
}

$filename = str_replace(' ', '_', strtolower($mastersheet->data['lps_mastersheet_name']));
$filename = preg_replace("/[^A-Za-z0-9_]/", '', $filename);
$_ZIP_FILE = $_SERVER['DOCUMENT_ROOT']."/data/tmp/".$filename.'_'.$_STAMP.'.zip';

// get master sheet files ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_SALES_ORDERS = array();

$sth = $model->db->prepare("
	SELECT 
		lps_salesorders_id AS id,
		lps_salesorders_file AS file
	FROM lps_salesorders
	WHERE lps_salesorders_stamp = ?
");

$sth->execute(array($_STAMP));
$result = $sth->fetchAll();

if ($result) {

	foreach ($result as $row) {

		$id = $row['id'];
		$file = $row['file'];
		
		if (file_exists($_SERVER['DOCUMENT_ROOT'].$file)) {
			$_SALES_ORDERS[$id] = $_SERVER['DOCUMENT_ROOT'].$file;
		} else {
			$_ERRORS[] = "File $file not found";
		}
	}

} else {
	$_ERRORS[] = "Files not found";
}

$_CONSOLE['sales.orders'] = $_SALES_ORDERS;


// generate zip files ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


if (!$_ERRORS && $_SALES_ORDERS) {

	// empty temporary dir
	Dir::emptyMe("/data/tmp/*");
	
	$zip = new ZipArchive();
	$zip->open($_ZIP_FILE, ZipArchive::CREATE);

	foreach ($_SALES_ORDERS as $key => $file) {
		
		$response = $zip->addFile($file, basename($file));

		if (!$response) {
			$_ERRORS[] = "File $file cannot be compressed.";
		}
	}

	$zip->close();
	@chmod($_ZIP_FILE, 0666);
}


// set files as uploaded :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && file_exists($_ZIP_FILE)) {

	$sth = $model->db->prepare("
		UPDATE lps_salesorders SET
			lps_salesorders_download_date = NOW(),
			lps_salesorders_download_user = ?
		WHERE lps_salesorders_id = ?
	");

	foreach ($_SALES_ORDERS as $key => $file) {
		
		if (DEBUGGING_MODE) $update = true;
		else $update = $sth->execute(array($user->id, $key));

		if ($update) $_MESSAGES[] = "File ".basename($file)." is updated";
		else $_ERRORS[] = "File ".basename($file)." cannot be updated";
	}
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (DEBUGGING_MODE) {
	$_RESPONSE['messages'] = $_MESSAGES;
	$_RESPONSE['console'] = $_CONSOLE;
}

$_RESPONSE['errors'] = $_ERRORS;
$_RESPONSE['success'] = $_ERRORS ? false : true;
$_RESPONSE['file'] = $_ERRORS ? null : str_replace($_SERVER['DOCUMENT_ROOT'], '', $_ZIP_FILE);
$_RESPONSE['date'] = date('d.m.Y');
$_RESPONSE['user'] = $user->firstname.' '.$user->name;
$_RESPONSE['message'] = $_ERRORS ? 'Error on file processing.' : 'Download file(s) is succefully submitted.';

header('Content-Type: text/json');
echo json_encode($_RESPONSE);
