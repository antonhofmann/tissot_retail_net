<?php 	
/**
* Master Sheet Items
* 
* Load all master sheet items. Outcome will depend on the controller.
* 	
* This file should be used for:
* 	- Master Sheet Consolidation Items
* 	- Generate Sales Orders
* 	- Show Purchase Orders
* 	
* @author: admir.serifi@mediaparx.ch
* @copyright (c) Swatch AG
* @version 1.1
* 
*/


require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$hasErrors = $_REQUEST['error'];
$search = $_REQUEST['search'];
$fields = $_REQUEST['fields'];
$id = $_REQUEST['mastersheet'];

$MPS = in_array($application, array('mps')) ? true : false;
$LPS = in_array($application, array('lps')) ? true : false;

// db connector
$model = new Model($application);


switch ($application) {
			
	case 'mps':
	

		$tableName = 'mps_mastersheets';
		$permissionEdit = user::permission(Mastersheet::PERMISSION_EDIT);

		$qGetItems = "
			SELECT 
				mps_material_id AS id,
				mps_material_planning_type_id AS group_id, 
				mps_material_planning_type_name AS group_caption, 
				mps_material_collection_category_id AS subgroup_id, 
				mps_material_collection_category_code AS subgroup_caption, 
				mps_material_collection_code AS collection, 
				mps_material_code AS code, 
				mps_material_name AS name, 
				mps_material_hsc AS hsc,
				mps_material_setof AS setof, 
				mps_material_locally_provided AS locally, 
				mps_ordersheet_item_purchase_order_number AS pon,
				mps_ordersheet_item_price As price, 
				currency_symbol AS currency,
				SUM(mps_ordersheet_item_quantity) AS quantity,
				SUM(mps_ordersheet_item_quantity_approved) AS quantity_approved, 
				(mps_ordersheet_item_price * Sum(mps_ordersheet_item_quantity_approved)) AS total_cost
			FROM mps_ordersheet_items 
			INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_ordersheet_id 
			INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id 
			INNER JOIN mps_material_collections ON mps_material_collection_id = mps_material_material_collection_id 
			INNER JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id 
			INNER JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id 
			INNER JOIN db_retailnet.currencies ON currency_id = mps_material_currency_id 
			WHERE mps_ordersheet_mastersheet_id = $id AND mps_ordersheet_item_quantity_approved > 0
			GROUP BY mps_material_id 
			ORDER BY 
				mps_material_planning_type_name asc, 
				mps_material_collection_category_code asc, 
				mps_material_code asc, 
				mps_material_name asc
		";

		$qNotOrderedItems = "
			SELECT DISTINCT mps_ordersheet_item_material_id AS item
			FROM mps_ordersheet_items
			INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
			INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
			WHERE mps_ordersheet_mastersheet_id = ? AND mps_ordersheet_item_quantity_approved > 0
			AND mps_material_locally_provided <> 1
			AND (mps_ordersheet_item_purchase_order_number IS NULL OR mps_ordersheet_item_purchase_order_number = '')
		";

	break; 

	case 'lps':
	

		$tableName = 'lps_mastersheets';
		$permissionEdit = user::permission('can_create_lps_sales_orders');

		$qGetItems = "
			SELECT
				lps_reference_id AS id,
				lps_product_group_id AS group_id, 
				lps_collection_category_id AS subgroup_id,
				lps_product_group_name AS group_caption, 
				lps_collection_category_code AS subgroup_caption,
				lps_mastersheet_estimate_month AS estimate_month,
				lps_collection_code AS collection,
				lps_collection_category_code AS collection_category,
				lps_reference_code AS code, 
				lps_reference_name AS name, 
				lps_launchplan_item_price AS price, 
				lps_launchplan_item_comment AS comment,
				currency_symbol AS currency, 
				SUM(lps_launchplan_item_quantity) AS quantity,
				SUM(lps_launchplan_item_quantity_approved) AS quantity_approved,
				(lps_launchplan_item_price * SUM(lps_launchplan_item_quantity_approved)) AS total_cost
			FROM lps_launchplan_items 
			INNER JOIN lps_references ON lps_launchplan_items.lps_launchplan_item_reference_id = lps_references.lps_reference_id
			INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
			INNER JOIN lps_mastersheets ON lps_mastersheet_id = lps_launchplan_mastersheet_id
			INNER JOIN lps_product_groups ON lps_references.lps_reference_product_group_id = lps_product_groups.lps_product_group_id
			INNER JOIN lps_collections ON lps_references.lps_reference_collection_id = lps_collections.lps_collection_id
			INNER JOIN lps_collection_categories ON lps_references.lps_reference_collection_category_id = lps_collection_categories.lps_collection_category_id
			INNER JOIN db_retailnet.currencies ON currency_id = lps_launchplan_item_currency
			WHERE lps_launchplan_mastersheet_id = $id AND lps_launchplan_item_quantity_approved > 0
			GROUP BY lps_reference_id
			ORDER BY 
				lps_product_group_name,
				lps_collection_category_code,
				lps_collection_code,
				lps_reference_code
		";

		$qGetExportedItems = "
			SELECT 
				lps_launchplan_item_week_quantity_id AS id,
				lps_launchplan_item_reference_id AS item
			FROM lps_launchplan_items 
			INNER JOIN lps_launchplan_item_week_quantities ON lps_launchplan_item_id = lps_launchplan_item_week_quantity_item_id
			INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
			WHERE lps_launchplan_mastersheet_id = ? AND lps_launchplan_item_week_quantity_pon > 0
		";

	break;
}

$mastersheet = new Modul($application);
$mastersheet->setTable($tableName);
$mastersheet->setDataMap($mapMasterSheet);
$mastersheet->read($id);

$datagrid = array();
$notOrdered = array();
$exportedItems = array();

if ($mastersheet->id) {

	// get mps not ordered items
	if ($qNotOrderedItems) {
	
		$sth = $model->db->prepare($qNotOrderedItems);
		$sth->execute(array($mastersheet->id));
		$result = $sth->fetchAll(); 

		if ($result) {
			foreach ($result as $row) {
				$item = $row['item'];
				$notOrdered[$item] = true;
			}
		}
	}

	// get lps exportd items
	if ($qGetExportedItems) {

		$sth = $model->db->prepare($qGetExportedItems);
		$sth->execute(array($mastersheet->id));
		$result = $sth->fetchAll(); 

		if ($result) {
			foreach ($result as $row) {
				$item = $row['item'];
				$exportedItems[$item] = true;
			}
		}
	}

	$result = $model->query($qGetItems)->fetchAll();

	if ($result) {

		$iconOK = "<img src='/public/images/icon-checked.png' />";

		foreach ($result as $row) {
			
			$exported = false;
			$group = $row['group_id'];
			$subgroup = $row['subgroup_id'];
			$item = $row['id'];
			
			$datagrid[$group]['caption'] = $row['group_caption'];
			$datagrid[$group]['subgroup'][$subgroup]['caption'] = $row['subgroup_caption'];
			$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['collection'] = $row['collection'];
			$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['collection_category'] = $row['collection_category'];
			$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['code'] = $row['code'];
			$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['name'] = $row['name'];
			$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['hsc'] = $row['hsc'];
			$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['price'] = $row['price'];
			$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['comment'] = $row['comment'];
			$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['currency'] = $row['currency'];
			$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['setof'] = $row['setof'];
			$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['quantity'] = $row['quantity'] ?: 0;
			$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['quantity_approved'] = $row['quantity_approved'] ?: 0;
			$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['total_cost'] = number_format($row['total_cost'], 2, '.', '');
			

			if ($MPS) $exported = $notOrdered[$item] ? false : true;
			elseif ($LPS) $exported = $exportedItems[$item];

			if (!$exported) $checkboxes[$group][$subgroup] = true;
				
			// item is locally provided
			if ($row['mps_material_locally_provided']) { 
				if (!$exportd) $hidden_fields .= "<input type=hidden name=item[$item] value=$item  class=locally_provided >";
			} else {
				if ($exported) $checkBox = $iconOK;
				else $checkBox = $hasErrors ? null : "<input type=checkbox name=item[$item] value='$item' class='item'  checked=checked >";
				$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['checkbox'] = $checkBox;
			}
			
			$checkAll = 'checked=checked';
		}
	}
}

if ($datagrid) {

	foreach ($datagrid as $group => $row) {

		$list .= "<h5 data=$group >{$row[caption]}</h5>";

		foreach ($row['subgroup'] as $subgroup => $items) {
				
			$list .= "<h6 data=$subgroup >{$items[caption]}</h6>";

			$totalprice = 0;
			$tableKey = "$group-$subgroup";
				
			$table = new Table(array('id' => $tableKey));
			$table->datagrid = $items['items'];
			$table->dataloader($dataloader);

			// items in revision
			if (_array::key_exists('checkbox', $table->datagrid)) {
				$field = $checkboxes[$group][$subgroup] && !$hasErrors ? "<input type='checkbox' class='checkall' $checkAll />" : null;
				$table->checkbox('width=20px');
				$table->caption('checkbox', $field);
				$colspan = $MPS ? 9 : 10;
			} else {
				$colspan = $MPS ? 8 : 9;
			}

			$table->collection(
				Table::ATTRIBUTE_NOWRAP,
				'width=10%',
				'caption=Collection'
			);

			$caption = $MPS ? 'Code' : 'Reference';
			$table->code(
				Table::ATTRIBUTE_NOWRAP,
				'width=10%',
				"caption=$caption"
			);

			$caption = $MPS ? 'Material Name' : 'Name';
			$table->name("caption=$caption");

			if ($LPS) {

				$table->collection_category(
					Table::ATTRIBUTE_NOWRAP,
					'width=15%',
					"caption=Collection Category"
				);
			}

			if ($MPS) {

				$table->hsc(
					Table::ATTRIBUTE_NOWRAP,
					'width=5%',
					"caption=".$translate->mps_material_hsc
				);
				
				$table->setof(
					Table::ATTRIBUTE_ALIGN_RIGHT,
					Table::ATTRIBUTE_NOWRAP,
					'caption=Set of',
					'width=5%'
				);
			}

			if ($LPS) {
				$table->comment(
					Table::ATTRIBUTE_ALIGN_RIGHT,
					Table::ATTRIBUTE_NOWRAP,
					'caption=Comment',
					'width=10%'
				);
			}

			$table->price(
				Table::ATTRIBUTE_ALIGN_RIGHT,
				Table::ATTRIBUTE_NOWRAP,
				'caption=Price',
				'width=8%'
			);

			$table->currency(
				Table::ATTRIBUTE_NOWRAP,
				'caption=Currency',
				'width=20px'
			);

			$table->quantity(
				Table::ATTRIBUTE_ALIGN_RIGHT,
				Table::ATTRIBUTE_NOWRAP,
				Table::PARAM_GET_FROM_LOADER,
				'caption=Quantity',
				'width=5%'
			);

			$table->quantity_approved(
				Table::ATTRIBUTE_ALIGN_RIGHT,
				Table::ATTRIBUTE_NOWRAP,
				'caption=Approved',
				'width=5%'
			);

			$table->total_cost(
				Table::ATTRIBUTE_ALIGN_RIGHT,
				Table::ATTRIBUTE_NOWRAP,
				'width=5%'
			);

			// total collection price
			foreach ($items['items'] as $key => $array) {
				$totalprice = $totalprice+$array['total_cost'];
			}

			$totalprice = number_format($totalprice, 2, '.', '');

			$table->footer(array(
				'attributes' => array('colspan' => $colspan),
				'content' => "$translate->total_cost {$items[caption]}"
			));

			$table->footer(array(
				'attributes' => array('align' => 'right'),
				'content' => "<b class='total-items $tableKey'>$totalprice</b>"
			));
				
			$list .= $table->render(array('order'=>true));
			$list .= "<br />";
		}
	}
}
else {
	//$list = html::emptybox($translate->empty_result);
}

if ($hidden_fields) {
	$list .= "<div style='display:none' class=hidden_fields >$hidden_fields</div>";
}

echo $list;
	