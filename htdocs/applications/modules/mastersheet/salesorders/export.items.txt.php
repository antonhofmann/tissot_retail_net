<?php 
/**
 *  Master Sheet Sales Orders Export Items to Flat File
 * 
 *  Export all client order sheets with customer numbers and shipto numbers
 * 
 *  @author admir.serifi@mediaparx.ch
 *  @copyright Mediaparx AG
 *
 *  Date Created: 	2014-09-02
 *  Date Modified:	2012-10-02
 */
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

define(DEBUG_MODE, false);

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$id = $_REQUEST['id'];
$customerNumbers = $_REQUEST['customernumber'];
$shiptoNumbers = $_REQUEST['shiptonumber'];
$deliverDates = $_REQUEST['week_desire_date'];
$orderNumber = $_SERVER['REQUEST_TIME'];
$currentDate = date('Y-m-d');

$console = array();
$errors = array();
$messages = array();

// database model
$model = new Model($application);

switch ($application) {

	case 'lps':

		$tableName = 'lps_mastersheets';
		$_STATE_EXPORTED = 10;
		$_STATE_PARTIALLY_EXPORTED = 8;

		$qOrderSheets = "
			SELECT 
				lps_launchplan_id AS ordersheet,
				lps_launchplan_address_id AS company
			FROM lps_launchplans
			WHERE lps_launchplan_mastersheet_id = ?
		";

		$qGetOrderSheetApprovedItems = "
			SELECT
				lps_launchplan_item_id AS item,
				lps_launchplan_id AS ordersheet,
				lps_launchplan_address_id AS company,
				lps_reference_code AS code,
				lps_reference_ean_number AS ean,
				lps_launchplan_item_quantity_approved AS quantity,
				lps_launchplan_item_order_date AS order_date
			FROM lps_launchplan_items
			INNER JOIN lps_references ON lps_reference_id = lps_launchplan_item_reference_id
			INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
			WHERE lps_launchplan_item_quantity_approved > 0 AND lps_launchplan_mastersheet_id = ?
		";

		$qWeekQuantities = "
			SELECT 
				lps_launchplan_item_week_quantity_id AS id,
				lps_launchplan_item_week_quantity_item_id AS item, 
				lps_launchplan_item_week_quantity_week AS week, 
				lps_launchplan_item_week_quantity_quantity_approved AS quantity,
				lps_launchplan_item_week_quantity_pon AS pon
			FROM lps_launchplan_items 
			INNER JOIN lps_launchplan_item_week_quantities ON lps_launchplan_item_id = lps_launchplan_item_week_quantity_item_id
			INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
			WHERE lps_launchplan_mastersheet_id = ?
		";

		$qUpdateOrderSheetItem = "
			UPDATE lps_launchplan_items SET
				lps_launchplan_item_customernumber = ?,
				lps_launchplan_item_order_date = ?,
				user_modified = ?
			WHERE lps_launchplan_item_id = ?
		";

		$qUpdateItemWeekData = "
			UPDATE lps_launchplan_item_week_quantities SET
				lps_launchplan_item_week_quantity_pon = ?,
				lps_launchplan_item_week_quantity_line = ?,
				lps_launchplan_item_week_quantity_delivery_date = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE lps_launchplan_item_week_quantity_item_id = ? AND lps_launchplan_item_week_quantity_week = ?
		";

		$qUpdateOrderSheets = "
			UPDATE lps_launchplans SET 
				lps_launchplan_workflowstate_id = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE lps_launchplan_id = ?
		";

		$qUpdateMasterSheet = "
			UPDATE lps_mastersheets SET 
				lps_mastersheet_archived = 1,
				user_modified = ?,
				date_modifeid = NOW()
			WHERE lps_mastersheet_id = ?
		";

		$qGetTotalMasterSheetOrders = "
			SELECT COUNT(lps_launchplan_id) AS total
			FROM lps_launchplans
			WHERE lps_launchplan_mastersheet_id = ?
		";

		$qGetTotalMasterSheetExportedOrders = "
			SELECT COUNT(lps_launchplan_id) AS total
			FROM lps_launchplans
			WHERE lps_launchplan_mastersheet_id = ? AND lps_launchplan_workflowstate_id IN (9,10)
		";

	break;
}

// get master sheet
$mastersheet = new Modul($application);
$mastersheet->setTable($tableName);
$mastersheet->read($id);

if (!$mastersheet->id) {
	$messages[] = "Master Sheet not found.";
} else {
	$firstWeek = $mastersheet->data['lps_mastersheet_week_number_first'];
	$totalWeeks = $mastersheet->data['lps_mastersheet_weeks'];
}


//	get order sheets items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$orderSheetItems = array();

if (!$errors && $qGetOrderSheetApprovedItems) {

	$sth = $model->db->prepare($qGetOrderSheetApprovedItems);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();

	if ($result) {
		
		foreach ($result as $row) {

			$ordersheet = $row['ordersheet'];
			$item = $row['item'];

			if ($customerNumbers[$item]) {
				$orderSheetItems[$ordersheet][$item]['code'] = $row['code'];
				$orderSheetItems[$ordersheet][$item]['ean'] = $row['ean'];
				$orderSheetItems[$ordersheet][$item]['quantity'] = $row['quantity'];
			}
		}

		$console['ordershet.items'] = $orderSheetItems;
	}
}


//	get order sheet item week quantities :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$orderSheetItemWeekQuantities = array();

if (!$errors && $qWeekQuantities) {

	$sth = $model->db->prepare($qWeekQuantities);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();

	if ($result) {
		
		foreach ($result as $row) {
			
			$id = $row['id'];
			$item = $row['item'];
			$week = $row['week'];
			$quantity = $row['quantity'];

			if ($customerNumbers[$item]) {
				$orderSheetItemWeekQuantities[$item][$week] = $row['quantity'];
			}
		}

		$console['item.weeks'] = $orderSheetItemWeekQuantities;
	}
}


//	build orders data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$orders = array();
$orderSheetItemsDataUpdate = array();
$updateItemWeekData = array();

if (!$errors && $orderSheetItems) {
			
	foreach ($orderSheetItems as $ordersheet => $items) {

		foreach ($items as $item => $row) {
			
			$customerNumber = $customerNumbers[$item];
			$code = $row['code'];
			$ean = $row['ean'];

			if ($customerNumber) {

				// set item update data
				$orderSheetItemsDataUpdate[$item] = array(
					'customer' => $customerNumber,
					'date' => $currentDate
				);

				// group orders by order and week
				for ($i=0; $i < $totalWeeks; $i++) { 
					
					$week = $firstWeek+$i;
					$key = "$item|$week";
					$quantity = $orderSheetItemWeekQuantities[$item][$week];

					if ($quantity) {

						$orders[$ordersheet][$week][$item] = array(
							'quantity' => $quantity,
							'date' => $deliverDates[$key],
							'customer' => $customerNumber,
							'code' => $code,
							'ean' => $ean
						);
					}
				}
			}
		}
	}

	$console['items.update.data'] = $orderSheetItemsDataUpdate;
	$console['orders'] = $orders;
}
	

//	build export data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$exports = array();
$poNumbers = array();

if (!$errors && $orders) {
	
	foreach ($orders as $ordersheet => $weeks) {
		
		foreach ($weeks as $week => $items) {

			$data = array();
			
			// purchase order number
			$pon = $poNumbers[$pon] ? $poNumbers[$pon]+1 : $orderNumber+1;
			$poNumbers[$pon] = $pon;

			$data['pon'] = $pon;
			$line = 1;

			foreach ($items as $item => $row) {

				$data['customer'] = $row['customer'];
				
				$data['items'][$item] = array(
					'line' => $line,
					'code' => $row['code'],
					'ean' => $row['ean'],
					'quantity' => $row['quantity'],
					'date' => $row['date']
				);

				$updateItemWeekData[$item][$week] = array(
					'pon' => $pon,
					'line' => $line,
					'date' => date('Y-m-d', strtotime($row['date']))
				);

				$line++;
			}

			$exports[] = $data;
		}
	}

	$console['exports'] = $exports;
}

	
//	generate flat file content :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$fileContent = null;

if (!$errors && $exports) {

	$date = date('Ymd');
	$tagHeaderStart = "B1";
	$tagHeaderEnd = "BA";
	$tagBodyStart = "B2";
	$tagFooterStart = "B9";

	foreach ($exports as $export) {
		
		if ($export['items']) {

			$totalQuantites = 0;

			// purchase order number
			$pon = str_pad($export['pon'], 18);

			// customber number
			$customer = str_pad($export['customer'], 18);

			//if ($fileContent) $fileContent .= "\n\r";
			
			// sales orders header
			$header = $tagHeaderStart.$pon.$customer.$date.$tagHeaderEnd;
			$fileContent .= "$header\n\r";
			
			if (DEBUG_MODE) {
				$console['file'][$pon][] = $header;
			}

			$details = null;

			foreach ($export['items'] as $item => $row) {
				
				// line no for the PON (increment)
				$line = str_pad($row['line'], 4);

				// reference code
				$code = str_pad($row['code'], 16);

				// ean
				$ean = str_pad($row['ean'], 13);

				// order quantity
				$quantity = str_pad($row['quantity'], 15);

				$totalQuantites = $totalQuantites + $quantity;

				// order quantity
				$requestedDate = date('Ymd', strtotime($row['date']));

				$body = $tagBodyStart.$line.$code.$ean.$quantity.$requestedDate;
				$details .= "$body\n\r";
				
				if (DEBUG_MODE) {
					$console['file'][$pon][] = $body;
				}
			}

			// items details
			$fileContent .= $details;
			
			// footer content
			$footer = $tagFooterStart.$line.str_pad($totalQuantites, 15);
			$fileContent .= "$footer\n\r";
			
			if (DEBUG_MODE) {
				$console['file'][$pon][] = $footer;
			}
		}
	}
}


//	export file ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$fileExported = false;

if (!$errors && $fileContent && !DEBUG_MODE) {

	$dirpath = $_SERVER['DOCUMENT_ROOT']."/ramco_exchange";
	dir::make($dirpath);

	$timestamp = date('YmdHis').'.'.str_pad(substr((float)microtime(), 2), 4, '0', STR_PAD_LEFT);
	$filename = "$dirpath/lps.salesorder.$timestamp.txt";
	$handle = fopen($filename, "x+");
	
	if ($handle) {

		fwrite($handle, $fileContent);
		fclose($handle);
		chmod($filename, 0755);

		$fileExported = true;
		
		$message = "Successfully export.";
		$response['message'] = "Successfully export.";
		$response['success'] = $fileExported;

		Message::add(array(
			'type' => 'message',
			'keyword' => "Successfully export."
		));

	} else {
		$message = "Error on file export. Check directory permissions for path: $dirpath";
		$response['message'] = $message;
		$response['success'] = false;
	}

	$messages[] = $message;
}


//	update ordered sheet items week data :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$failures = false;

if (!$errors && $fileExported && $updateItemWeekData) {

	$sth = $model->db->prepare($qUpdateItemWeekData);

	foreach ($updateItemWeekData as $item => $weeks) {

		foreach ($weeks as $week => $row) {
		
			if (!$failures) {
				
				$update = $sth->execute(array(
					$row['pon'],
					$row['line'],
					$row['date'],
					$user->login,
					$item,
					$week
				));

				$failures = !$update;
				$messages[] = $update ? "Update item week: $item, $week" : "ERROR on update item week: $item, $week";
			}
		}
	}

	if ($failures) {
		$errors[] = "Error by updating Launch Plan Item Week Data.";
	}
}


//	update ordered sheet items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$failures = false;

if (!$errors && $fileExported && $orderSheetItemsDataUpdate) {

	$sth = $model->db->prepare($qUpdateOrderSheetItem);

	foreach ($orderSheetItemsDataUpdate as $item => $row) {
		
		if (!$failures) {

			$update = $sth->execute(array(
				$row['customer'],
				$row['date'],
				$user->login,
				$item
			));

			$failures = !$update;
			$messages[] = $update ? "Update item: $item" : "ERROR on update item: $item";
		}
	}

	if ($failures) {
		$errors[] = "Error by updating Launch Plan Items.";
	}
}

//	update ordered sheets ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$failures = false;
$orders = array();

if (!$errors && $mastersheet->id && !DEBUG_MODE) {

	// get ordersheet items
	$sth = $model->db->prepare($qGetOrderSheetApprovedItems);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();

	foreach ($result as $row) {

		$order = $row['ordersheet'];

		$orders[$order]['items'] = $orders[$order]['items'] + 1;

		if ($row['order_date']) {
			$orders[$order]['exported'] = $orders[$order]['exported'] + 1;
		}
	}

	if ($orders) {

		$sth = $model->db->prepare($qUpdateOrderSheets);

		foreach ($orders as $order => $total) {
			
			if ($total['exported'] > 0) {
				
				if (!$failures) {
					$state = $total['exported']==$total['items'] ? $_STATE_EXPORTED : $_STATE_PARTIALLY_EXPORTED;
					$update = $sth->execute(array($state, $user->login, $order));
					$failures = !$update;
					$messages[] = $update ? "Update order sheet: $order" : "ERROR on update order sheet: $order";
				}
			}
		}

		if ($failures) {
			$errors[] = "Error by updating Launch Plans.";
		}
	}	

	$console['ordersheets.compare'] = $orders;
}

//	arichve master sheet :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$failures = false;

if (!$errors && $mastersheet->id && !DEBUG_MODE) {

	// total master sheet orders
	$sth = $model->db->prepare($qGetTotalMasterSheetOrders);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetch();
	$totalMasterSheetOrders = $result['total'];

	// total master sheet exported orders
	$sth = $model->db->prepare($qGetTotalMasterSheetExportedOrders);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetch();
	$totalMasterSheetExportedOrders = $result['total'];

	$console['mastersheet.compare']['ordersheets'] = $totalMasterSheetOrders;
	$console['mastersheet.compare']['exported'] = $totalMasterSheetExportedOrders;

	// set master sheet in archive
	if ($totalMasterSheetOrders && $totalMasterSheetOrders == $totalMasterSheetExportedOrders) {
		
		$sth = $model->db->prepare($qUpdateMasterSheet);
		$update = $sth->execute(array($user->login, $mastersheet->id));
		$failures = !$update;
		$messages[] = $update ? "Master sheet archived" : "ERROR on master sheet archive";
	}

	if ($failures) {
		$errors[] = "Error by updating Master Sheet.";
	}
}


//	responding :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$response['errors'] = $errors;
$response['messages'] = $messages;

if (DEBUG_MODE) {
	$response['sources'] = $console;
}

if (!$mastersheet->id) {
	$response['success'] = false;
	$response['message'] = $translate->error_request;
}

header('Content-Type: text/json');
echo json_encode($response);
	