<?php 
		
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";

// execution time
ini_set('max_execution_time', 120);
ini_set('memory_limit', '1024M');

$settings = Settings::init();
$settings->load('data');
$user = User::instance();
$translate = Translate::instance();

$_ERRORS = array();
$_MESSAGES = array();
$_DATA = array();

define(DEBUGGING_MODE, false);

// request :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['mastersheet'];

// quantity columns
$_COLUMNS = array();
$_COLUMNS['estimate_quantity'] = $_REQUEST['estimate_quantity'];
$_COLUMNS['desired_quantity'] = $_REQUEST['desired_quantity'];
$_COLUMNS['desired_quantity_total'] = $_REQUEST['desired_quantity_total'];
$_COLUMNS['approved_quantity'] = $_REQUEST['approved_quantity'];
$_COLUMNS['approved_quantity_total'] = $_REQUEST['approved_quantity_total'];

// regerences group
$_GROUPS = $_REQUEST['group']; 


// get session stored request
$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

// initialisation ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// db model
$model = new Model($application);

// mastersheet
$mastersheet = new Modul($application);
$mastersheet->setTable('lps_mastersheets');
$mastersheet->read($id);

if ($mastersheet->id) {

	// titles
	$_SHEET_CAPTION = $archived ? 'Archived Detail List' : 'Detail List';
	$_PAGE_TITLE = $mastersheet->data['lps_mastersheet_name'].', '.$mastersheet->data['lps_mastersheet_year'];
	
	// subtitle
	$_PAGE_SUBTITLE = date('d.m.Y H:i:s');

	// export file name
	$_FILENAME  = "lps_detail_list_";
	$_FILENAME .= str_replace(' ', '_', strtolower($mastersheet->data['lps_mastersheet_name']));
	$_FILENAME .= "_".date('Ymd') ;

	// master sheet data
	$_MASTERSHEET_ESTIMATE_MONTH = $mastersheet->data['lps_mastersheet_estimate_month'];
	$_MASTERSHEET_FIRST_WEEK = $mastersheet->data['lps_mastersheet_week_number_first'];
	$_MASTERSHEET_TOTAL_WEEKS = $mastersheet->data['lps_mastersheet_weeks'];

} else $_ERRORS[] = "Detail list $id not found.";


// get clinets :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_CLIENTS= array();

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT DISTINCT
			lps_launchplan_id AS launchplan,
			lps_launchplan_customer_number AS customer_number,
			CONCAT(country_name,', ', address_company) AS client_name
		FROM lps_launchplans
		INNER JOIN db_retailnet.addresses ON address_id = lps_launchplan_address_id
		INNER JOIN db_retailnet.countries ON country_id = address_country
		WHERE lps_launchplan_mastersheet_id = ?
		ORDER BY client_name, customer_number
	");

	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();

	if ($result) {

		foreach ($result as $row) {
			$_CLIENTS[$row['launchplan']] = array(
				'name' => $row['client_name'],
				'number' => $row['customer_number']
			);
		}
	}
}


// get mastersheet detail list items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_REFERENCES = array();

if (!$_ERRORS) {
	
	$sth = $model->db->prepare("
		SELECT DISTINCT
			lps_launchplan_id AS id,
			lps_launchplan_item_reference_id AS reference,
			lps_reference_code AS reference_code, 
			lps_product_group_id AS group_id,	
			lps_product_group_name AS product_group,	
			lps_launchplan_item_price AS price, 
			SUM(lps_launchplan_item_quantity) AS quantity,
			SUM(lps_launchplan_item_quantity_approved) AS quantity_approved,
			SUM(lps_launchplan_item_quantity_estimate) AS quantity_estimate
		FROM lps_launchplan_items 
		INNER JOIN lps_references ON lps_launchplan_item_reference_id = lps_reference_id
		INNER JOIN lps_product_groups ON lps_reference_product_group_id = lps_product_group_id
		INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
		WHERE lps_launchplan_mastersheet_id = ?
		GROUP BY lps_launchplan_id, lps_reference_id
		ORDER BY reference_code, product_group
	");

	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();

	if ($result) {
		
		foreach ($result as $row) {
			
			$launchplan = $row['id'];
			$reference = $row['reference'];	
			$group = $row['group_id'];

			if ($_GROUPS[$group]) {

				$_REFERENCES[$reference]['code'] = $row['reference_code'];
				$_REFERENCES[$reference]['group'] = $row['product_group'];
				$_REFERENCES[$reference]['price'] = $row['price'];
				$_REFERENCES[$reference]['currency'] = $row['currency_symbol'];
			
				$_QUANTITIES[$launchplan][$reference]['quantity'] = $row['quantity'];
				$_QUANTITIES[$launchplan][$reference]['quantity_approved'] = $row['quantity_approved'];
				$_QUANTITIES[$launchplan][$reference]['quantity_estimate'] = $row['quantity_estimate'];
				$_QUANTITIES[$launchplan][$reference]['total_cost_quantity'] = $row['price']*$row['quantity'];
				$_QUANTITIES[$launchplan][$reference]['total_cost_quantity_approved'] = $row['price']*$row['quantity_approved'];
				$_QUANTITIES[$launchplan][$reference]['total_cost_quantity_estimate'] = $row['price']*$row['quantity_estimate'];
			}
		}	
	}
}


// spreadsheet header ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_DATAGRID = array();
$_ROW_SEPARATORS = array();
$_COL_SEPARATORS = array();

$_FIRST_ROW = 1;
$_FIRST_COL = 1;

if (!$_ERRORS) {

	$row=$_FIRST_ROW;
	$col=$_FIRST_COL;
	
	// row index page title
	$_ROW_PAGE_TITLE = $row;
	
	// page title
	$_DATAGRID[$row][$col]['value'] = $_PAGE_TITLE;
	$_DATAGRID[$row][$col]['height'] = 50;

	$row++;
	$col=$_FIRST_COL;
	
	// row index page subtitle
	$_ROW_PAGE_SUBTITLE=$row;

	// page subtitle
	$_DATAGRID[$row][$col]['value'] = $_PAGE_SUBTITLE;
	$_DATAGRID[$row][$col]['height'] = 25;

	if ($_FILTERS_CAPTIONS) {

		$row++;
		$col=$_FIRST_COL;
		
		//  row index filter captions
		$_ROW_FILTER_CAPTIONS=$row;

		$_DATAGRID[$row][$col]['value'] = join(', ', $_FILTERS_CAPTIONS);
		$_DATAGRID[$row][$col]['height'] = 25;
	}

	// separator
	$row++;
	$col=$_FIRST_COL;

	// row separtor index
	$_ROW_SEPARATORS[$row] = true;
}


// spreadsheet table headers :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_COL_ITEMS = array();

if (!$_ERRORS && $_REFERENCES) {

	$row++;

	$_FIRST_TABLE_ROW=$row;

	// row index reference
	$_ROW_REFERENCE=$row;

	$row++;
	
	// row index product group
	$_ROW_PRODUCT_GROUP=$row;

	$row++;

	// row index quantity type
	$_ROW_QUANTITY_TYPE=$row;

	$col=$_FIRST_COL;

	// set height for quantity type row
	$_DATAGRID[$_ROW_QUANTITY_TYPE][$col]['height'] = 30;

	// customer number
	$_DATAGRID[$_ROW_REFERENCE][$col]['value'] = 'Customer No.';
	$_DATAGRID[$_ROW_REFERENCE][$col]['width'] = 20;
	$_DATAGRID[$_ROW_REFERENCE][$col]['height'] = 25;
	$_DATAGRID[$_ROW_REFERENCE][$col]['vmerge'] = 3;

	$col++;

	// client name
	$_DATAGRID[$_ROW_REFERENCE][$col]['value'] = 'Company';
	$_DATAGRID[$_ROW_REFERENCE][$col]['width'] = 60;
	$_DATAGRID[$_ROW_REFERENCE][$col]['vmerge'] = 3;

	foreach ($_REFERENCES as $key => $item) {
		
		$merge=0;

		// quantity estimatet
		if ($_COLUMNS['estimate_quantity']) {
			$merge++;
			$col++;
			$_DATAGRID[$_ROW_QUANTITY_TYPE][$col]['value'] = "Estimates $_MASTERSHEET_ESTIMATE_MONTH months";
			$_DATAGRID[$_ROW_QUANTITY_TYPE][$col]['width'] = 18;
		}

		// total launch
		if ($_COLUMNS['desired_quantity']) {
			$col++; 
			$merge++;
			$_DATAGRID[$_ROW_QUANTITY_TYPE][$col]['value'] = 'Total Launch Quantity';
			$_DATAGRID[$_ROW_QUANTITY_TYPE][$col]['width'] = 18;
		}

		// total launch cost
		if ($_COLUMNS['desired_quantity_total']) {
			$col++; $merge++;
			$_DATAGRID[$_ROW_QUANTITY_TYPE][$col]['value'] = 'Total Launch Cost';
			$_DATAGRID[$_ROW_QUANTITY_TYPE][$col]['width'] = 18;
		}

		// total approved quantities
		if ($_COLUMNS['approved_quantity']) {
			$col++; $merge++;
			$_DATAGRID[$_ROW_QUANTITY_TYPE][$col]['value'] = 'Total Approved Quantity';
			$_DATAGRID[$_ROW_QUANTITY_TYPE][$col]['width'] = 19;
		}

		// total approved quantities
		if ($_COLUMNS['approved_quantity_total']) {
			$col++; $merge++;
			$_DATAGRID[$_ROW_QUANTITY_TYPE][$col]['value'] = 'Total Approved Cost';
			$_DATAGRID[$_ROW_QUANTITY_TYPE][$col]['width'] = 18;
		}

		$merge = $merge ? $merge-1 : 0;

		// reference code
		$_col = $col-$merge;
		$_DATAGRID[$_ROW_REFERENCE][$_col]['value'] = $item['code'];
		$_DATAGRID[$_ROW_REFERENCE][$_col]['merge'] = $merge;

		// product group
		$_DATAGRID[$_ROW_PRODUCT_GROUP][$_col]['value'] = $item['group'];
		$_DATAGRID[$_ROW_PRODUCT_GROUP][$_col]['merge'] = $merge;		

		// total spreadsheet colums
		$_TOTAL_COLS = $_TOTAL_COLS > $col ? $_TOTAL_COLS : $col;
	}

	// merge header captions
	$_DATAGRID[$_ROW_PAGE_TITLE][$_FIRST_COL]['merge'] = $_TOTAL_COLS-1;
	$_DATAGRID[$_ROW_PAGE_SUBTITLE][$_FIRST_COL]['merge'] = $_TOTAL_COLS-1;
	
	if ($_ROW_FILTER_CAPTIONS) {
		$_DATAGRID[$_ROW_PAGE_TITLE][$_ROW_FILTER_CAPTIONS]['merge'] = $_TOTAL_COLS-1;
	}
}


// spreadsheet grid ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && $_CLIENTS) {

	$_FIRST_GRID_ROW=$row+1;

	foreach ($_CLIENTS as $launchplan => $client) {

		$row++;

		// customer number
		$col=$_FIRST_COL;
		$_DATAGRID[$row][$col]['value'] = $client['number'];
		$_DATAGRID[$row][$col]['group'] = $launchplan;
		
		// col index customer number
		$_COL_CUSTOMER_NUMBER=$col;

		// client name
		$col++;
		$_DATAGRID[$row][$col]['value'] = $client['name'];

		// col index client
		$_COL_CLIENT=$col;

		if ($_REFERENCES) {

			foreach ($_REFERENCES as $reference => $item) {

				// estimated quantitiy
				if ($_COLUMNS['estimate_quantity']) {
					$col++;
					$value = $_QUANTITIES[$launchplan][$reference]['quantity_estimate'];
					$_DATAGRID[$row][$col]['value'] = $value;
					$_SUMCOL[$col] = $_SUMCOL[$col] + $value;
				}
				
				// total launch
				if ($_COLUMNS['desired_quantity']) {
					$col++;
					$value = $_QUANTITIES[$launchplan][$reference]['quantity'];
					$_DATAGRID[$row][$col]['value'] = $value;
					$_SUMCOL[$col] = $_SUMCOL[$col] + $value;
				}

				// total launch cost
				if ($_COLUMNS['desired_quantity_total']) {
					$col++;
					$value = $_QUANTITIES[$launchplan][$reference]['total_cost_quantity'];
					$_DATAGRID[$row][$col]['value'] = $value;
					$_SUMCOL[$col] = $_SUMCOL[$col] + $value;
				}

				// total approved quantities
				if ($_COLUMNS['approved_quantity']) {
					$col++;
					$value = $_QUANTITIES[$launchplan][$reference]['quantity_approved'];
					$_DATAGRID[$row][$col]['value'] = $value;
					$_SUMCOL[$col] = $_SUMCOL[$col] + $value;
				}

				// total approved quantities
				if ($_COLUMNS['approved_quantity_total']) {
					$col++;
					$value = $_QUANTITIES[$launchplan][$reference]['total_cost_quantity_approved'];
					$_DATAGRID[$row][$col]['value'] = $value;
					$_SUMCOL[$col] = $_SUMCOL[$col] + $value;
				}
			}
		}
	}

	$_LAST_GRID_ROW = $row;

	// grid separator
	$row++;
	$_ROW_SEPARATORS[$row] = true;

	$row++;
	$_ROW_TOTAL=$row;
	$_TOTAL_ROWS=$row;
}


// spreadsheet styles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_STYLES = array(
	'allborders' => array(
		'borders' => array( 
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	'header-background' => array(
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'EFEFEF')
		)
	),
	'client-caption' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 14, 
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		)
	),
	'reference-caption' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 14, 
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap' => true
		)
	),
	'group-caption' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 11
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap' => true
		)
	),
	'quantity-caption' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 10
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap' => true
		)
	),
	'page-title' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 20, 
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		)
	),
	'page-subtitle' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 11
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		)
	),
	'totals' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12, 
			'bold'=>true
		),
		'alignment' => array(
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'EFEFEF')
		)
	)
);


// spreadsheet builder :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


if (!$_ERRORS && $_TOTAL_ROWS && $_TOTAL_COLS) {

	// excel init
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name)->setTitle($_SHEET_CAPTION);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

	// sheet columns
	$_FIRST_COL_INDEX = spreadsheet::key($_FIRST_COL);
	$_LAST_COL_INDEX = spreadsheet::key($_TOTAL_COLS);

	for ($row=1; $row <= $_TOTAL_ROWS; $row++) {
			
		for ($col=1; $col <= $_TOTAL_COLS; $col++) {

			// column index
			$_COL_INDEX = spreadsheet::key($col);

			// row range
			$_ROW_RANGE_INDEX = $_FIRST_COL_INDEX.$row.":".$_LAST_COL_INDEX.$row;

			// cel index
			$index = "$_COL_INDEX$row";
			
			// set row width
			if ($_DATAGRID[$row][$col]['width']) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($_COL_INDEX)->setWidth($_DATAGRID[$row][$col]['width']);
			}

			// set row height
			if ($_DATAGRID[$row][$col]['height']) {
				$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight($_DATAGRID[$row][$col]['height']);
			}
			
			// merge horizontal
			if ($_DATAGRID[$row][$col]['merge']) {
				$_index = spreadsheet::key($col+$_DATAGRID[$row][$col]['merge'], $row);
				$objPHPExcel->getActiveSheet()->mergeCells("$index:$_index");
			}
			
			// merge vertical
			if ($_DATAGRID[$row][$col]['vmerge']) {
				$_row = $_DATAGRID[$row][$col]['vmerge']+$row-1;
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col-1,$row,$col-1,$_row);
			}

			switch ($row) {

				case $_ROW_SEPARATORS[$row]:
					if ($col==$_FIRST_COL) {
						$objPHPExcel->getActiveSheet()->mergeCells($_ROW_RANGE_INDEX);
						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(25);
					}
				break;
				
				// page headers
				case $_ROW_PAGE_TITLE:	
				case $_ROW_PAGE_SUBTITLE:
				case $_ROW_FILTER_CAPTIONS:	

					if ($col==$_FIRST_COL) {

						$objPHPExcel->getActiveSheet()->setCellValue($index, $_DATAGRID[$row][$col]['value']);

						if ($row==$_ROW_PAGE_TITLE) {
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['page-title']);
						}

						if ($row==$_ROW_PAGE_SUBTITLE || $row==$_ROW_FILTER_CAPTIONS) {
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['page-subtitle']);
						}
					}

				break;

				// item headers
				case $_ROW_REFERENCE:
				case $_ROW_PRODUCT_GROUP:
				case $_ROW_QUANTITY_TYPE:

					$objPHPExcel->getActiveSheet()->setCellValue($index, $_DATAGRID[$row][$col]['value']);

					if ($row==$_ROW_REFERENCE && $col<3) {
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['client-caption'], false);
					}

					if ($row==$_ROW_REFERENCE && $col>2) {
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['reference-caption'], false);
					}

					if ($row==$_ROW_PRODUCT_GROUP) {
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['group-caption'], false);
					}

					if ($row==$_ROW_QUANTITY_TYPE) {
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['quantity-caption'], false);
					}


				break;

				case $row >= $_FIRST_GRID_ROW && $row <= $_LAST_GRID_ROW:

					$objPHPExcel->getActiveSheet()->setCellValue($index, $_DATAGRID[$row][$col]['value']);

					if ($col==$_FIRST_COL) {
						$objPHPExcel->getActiveSheet()->getStyle($index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					}

				break;

				case $row==$_TOTAL_ROWS:

					if ($col==$_FIRST_COL) { 
						$_index = spreadsheet::key($col+1, $row);
						$objPHPExcel->getActiveSheet()->mergeCells("$index:$_index");
						$objPHPExcel->getActiveSheet()->setCellValue($index, "Total Detail List");
						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
					}

					if ($_SUMCOL[$col]) {
						$objPHPExcel->getActiveSheet()->setCellValue($index, $_SUMCOL[$col]);
					}

				break;

			} // end switch row
		} // end each cols
	} // end each rows

	// set table header background
	$range = $_FIRST_COL_INDEX.$_FIRST_TABLE_ROW.':'.$_LAST_COL_INDEX.$_ROW_QUANTITY_TYPE;
	$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($_STYLES['header-background'], false);

	// set borders over all cells
	$range = $_FIRST_COL_INDEX.$_FIRST_TABLE_ROW.':'.$_LAST_COL_INDEX.$_LAST_GRID_ROW;
	$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($_STYLES['allborders'], false);

	// set style fro grand totals
	$range = $_FIRST_COL_INDEX.$_TOTAL_ROWS.':'.$_LAST_COL_INDEX.$_TOTAL_ROWS;
	$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($_STYLES['totals']);
	$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($_STYLES['allborders'], false);
}

if ($_ERRORS) {

	echo "<pre>";
	echo "ERRORS:";
	print_r($_ERRORS);
	echo "</pre>";

} elseif ($objPHPExcel) {
		
	$objPHPExcel->getActiveSheet()->setTitle($_SHEET_CAPTION);
	$objPHPExcel->setActiveSheetIndex(0);

	// redirect output to client browser
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$_FILENAME.'.xlsx"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
}
else {
	message::empty_result();
	url::redirect("/$application/$controller$archived/$action/$id");
}
