<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['file_id'];
$entity = $_REQUEST['file_entity'];

$uploadPath = "/public/data/files/$application/mastersheets/$entity";

$data = array();

switch ($application) {
	
	case 'mps':
	

		$tableName = 'mps_mastersheet_files';

		$data['mps_mastersheet_file_mastersheet_id'] = $entity;

		if (isset($_REQUEST['file_title'])) {
			$data['mps_mastersheet_file_title'] = $_REQUEST['file_title'];
		}

		if ($_REQUEST['has_upload'] && isset($_REQUEST['file_path'])) {
			$path = $data['mps_mastersheet_file_path'] = upload::move($_REQUEST['file_path'], $uploadPath);
			$extension = file::extension($path);
			$file_type = new File_Type();
			$data['mps_mastersheet_file_type'] = $file_type->get_id_from_extension($extension);
		}

		if (isset($_REQUEST['file_description'])) {
			$data['mps_mastersheet_file_description'] = $_REQUEST['file_description'];
		}

		if (in_array('file_visible', $fields)) {
			$data['mps_mastersheet_file_visible'] = ($_REQUEST['file_visible']) ? 1 : 0;
		}
		
	break;

	case 'lps':
	

		$tableName = 'lps_mastersheet_files';

		$data['lps_mastersheet_file_mastersheet_id'] = $entity;

		if (isset($_REQUEST['file_title'])) {
			$data['lps_mastersheet_file_title'] = $_REQUEST['file_title'];
		}

		if ($_REQUEST['has_upload'] && isset($_REQUEST['file_path'])) {
			$path = $data['lps_mastersheet_file_path'] = upload::move($_REQUEST['file_path'], $uploadPath);
			$extension = file::extension($path);
			$file_type = new File_Type();
			$data['lps_mastersheet_file_type'] = $file_type->get_id_from_extension($extension);
		}

		if (isset($_REQUEST['file_description'])) {
			$data['lps_mastersheet_file_description'] = $_REQUEST['file_description'];
		}

		if (in_array('file_visible', $fields)) {
			$data['lps_mastersheet_file_visible'] = ($_REQUEST['file_visible']) ? 1 : 0;
		}

	break;
}


if ($data) {
	
	$file = new Modul($application);
	$file->setTable($tableName);
	$file->read($id);

	if ($file->id) {
		$data['user_modified'] = $user->login;
		$response = $file->update($data);
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		$response = $id = $file->create($data);
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		
		if ($response && $_REQUEST['redirect']) {
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect,
	'path' => $path
));
