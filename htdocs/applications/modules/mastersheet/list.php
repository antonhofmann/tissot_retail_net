<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$filter = $_REQUEST['filter'];

$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'mastersheet_year desc, mastersheet_name';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

// session request
$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
// filters
$fSearch = ($_REQUEST['search'] && $_REQUEST['search']<>$translate->search) ? $_REQUEST['search'] : null;
$fYear = $_REQUEST['year'];

// db model
$model = new Model($application);

switch ($application) {
				
	case 'mps':
	
		
		// filter: full text search
		if ($fSearch) {
			$filters['search'] = "(
				mps_mastersheet_year LIKE \"%$fSearch%\" 
				OR mps_mastersheet_name LIKE \"%$fSearch%\"
			)";
		}

		// filer: collection year
		if ($fYear) {
			$filters['year'] = "mps_mastersheet_year = $fYear";
		}

		// filter: consolidation
		if ($controller=='consolidations') {
			$filters['default'] = "
				mps_ordersheet_workflowstate_id IN (3,4,5)
				OR (
					mps_ordersheet_workflowstate_id = 2
					AND mps_ordersheet_closingdate < CURRENT_DATE
				)
			";
		} elseif ($controller=='salesorders') {
			$filters['default'] = "mps_ordersheet_workflowstate_id = 5";
		} elseif ($archived) {
			$filters['default'] = "mps_mastersheet_archived = 1";
		} else {
			$filters['default'] = "mps_mastersheet_archived = 0";
		}

		if ($controller=='consolidations' || $controller=='salesorders' || $archived) {
			
			$query = "
				SELECT SQL_CALC_FOUND_ROWS DISTINCT
					mps_mastersheet_id AS mastersheet_id, 
					mps_mastersheet_year AS mastersheet_year, 
					mps_mastersheet_name AS mastersheet_name,
					mps_mastersheet_consolidated AS mastersheet_consolidated,
					mps_mastersheet_archived AS mastersheet_archived,
					(
						SELECT COUNT(mps_ordersheet_id) AS total
						FROM mps_ordersheets
						WHERE mps_ordersheet_mastersheet_id = mps_mastersheet_id
					) as ordersheets
				FROM mps_ordersheets
				INNER JOIN mps_mastersheets ON mps_mastersheet_id = mps_ordersheet_mastersheet_id
			";

			$queryTotalCosts = "
				SELECT 
					SUM(mps_ordersheet_item_price * mps_ordersheet_item_quantity_approved) AS total
				FROM mps_ordersheet_items
				INNER JOIN mps_ordersheets ON mps_ordersheet_item_ordersheet_id = mps_ordersheet_id
				WHERE mps_ordersheet_item_quantity_approved > 0 AND mps_ordersheet_mastersheet_id = ?
			";

		} else {
			$query = "
				SELECT SQL_CALC_FOUND_ROWS DISTINCT
					mps_mastersheet_id AS mastersheet_id, 
					mps_mastersheet_year AS mastersheet_year, 
					mps_mastersheet_name AS mastersheet_name,
					mps_mastersheet_consolidated AS mastersheet_consolidated,
					mps_mastersheet_archived AS mastersheet_archived
				FROM mps_mastersheets
			";
		}

	break;

	case 'lps':
	
		
		// filter: full text search
		if ($fSearch) {
			$filters['search'] = "(
				lps_mastersheet_year LIKE \"%$fSearch%\" 
				OR lps_mastersheet_name LIKE \"%$fSearch%\"
				OR lps_mastersheet_pos_date LIKE \"%$fSearch%\"
				OR lps_mastersheet_phase_out_date LIKE \"%$fSearch%\"
			)";
		}

		// filer: collection year
		if ($fYear) {
			$filters['year'] = "lps_mastersheet_year = $fYear";
		}

		// filter: consolidation
		if ($controller=='consolidations') {
			$filters['default'] = "
				lps_launchplan_workflowstate_id IN (4,5,7)
				OR (
					lps_launchplan_workflowstate_id = 3
					AND lps_launchplan_closingdate < CURRENT_DATE
				)
			";
		} elseif ($controller=='salesorders') {
			$filters['default'] = "lps_launchplan_workflowstate_id IN (7,8)";
		} elseif ($archived) {
			$filters['default'] = "( lps_mastersheet_archived = 1 OR lps_launchplan_workflowstate_id IN (10,11) )";
		} else {
			$filters['default'] = "lps_mastersheet_archived = 0";
		}

		if ($controller=='consolidations' || $controller=='salesorders' || $archived) {
			
			$query = "
				SELECT SQL_CALC_FOUND_ROWS DISTINCT
					lps_mastersheet_id AS mastersheet_id, 
					lps_mastersheet_year AS mastersheet_year, 
					lps_mastersheet_name AS mastersheet_name,
					lps_mastersheet_consolidated AS mastersheet_consolidated,
					lps_mastersheet_archived AS mastersheet_archived,
					(
						SELECT COUNT(lps_launchplan_id) AS total
						FROM lps_launchplans
						WHERE lps_launchplan_mastersheet_id = lps_mastersheet_id
					) AS ordersheets
				FROM lps_launchplans
				INNER JOIN lps_mastersheets ON lps_mastersheet_id = lps_launchplan_mastersheet_id
			";

			$queryTotalCosts = "
				SELECT 
					SUM(lps_launchplan_item_price * lps_launchplan_item_quantity_approved) AS total
				FROM lps_launchplans
				INNER JOIN lps_launchplan_items ON lps_launchplan_item_launchplan_id = lps_launchplan_id
				WHERE lps_launchplan_item_quantity_approved > 0 AND lps_launchplan_mastersheet_id = ?
			";

		} else {
			$query = "
				SELECT SQL_CALC_FOUND_ROWS DISTINCT
					lps_mastersheet_id AS mastersheet_id, 
					lps_mastersheet_year AS mastersheet_year, 
					lps_mastersheet_name AS mastersheet_name,
					lps_mastersheet_consolidated AS mastersheet_consolidated,
					lps_mastersheet_archived AS mastersheet_archived,
					lps_mastersheet_pos_date AS mastersheet_pos_date,
					lps_mastersheet_phase_out_date AS mastersheet_phase_out_date,
					DATE_FORMAT(lps_mastersheet_pos_date, '%d.%m.%Y') AS posdate,
					DATE_FORMAT(lps_mastersheet_phase_out_date, '%d.%m.%Y') AS outdate
				FROM lps_mastersheets
			";
		}

	break;
}

$result = $model->query($query)
->filter($filters)
->order($sort, $direction)
->offset($offset, $rows)
->fetchAll();

if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);

	// mastersheet total costs
	if ($controller=='consolidations' || $controller=='salesorders' || $archived) {		 
		$sth = $model->db->prepare($queryTotalCosts);
	}
	
	foreach ($datagrid as $key => $row) {
		
		if ($row['mastersheet_archived']) {
			$datagrid[$key]['state'] = $translate->archived;
		}
		elseif ($row['mastersheet_consolidated']) {
			$datagrid[$key]['state'] = $translate->consolidated;
		}
		
		if ($controller=='consolidations' || $controller=='salesorders' || $archived) {			
			$sth->execute(array($key));
			$cost = $sth->fetch();
			$datagrid[$key]['total_cost'] = $cost['total'];
		}

		if ($application=='lps') {
			$datagrid[$key]['mastersheet_pos_date'] = $row['posdate'];
			$datagrid[$key]['mastersheet_phase_out_date'] = $row['outdate'];
		}
	}

	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// toolbox: button print
if ($datagrid && $_REQUEST['print']) {
	$toolbox[] = ui::button(array(
		'id' => 'print',
		'icon' => 'print',
		'href' => $_REQUEST['print'],
		'label' => $translate->print
	));
}

// toolbox: add
if ($_REQUEST['add'])  {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();

// dropdowns
switch ($application) {
	
	case 'mps':
	
		
		// get mastersheet years
		$years = $model->query("
			SELECT DISTINCT
				mps_mastersheet_year, 
				mps_mastersheet_year AS year
			FROM mps_mastersheets
		")
		->filter($filters)
		->exclude('year')
		->order('mps_mastersheet_year')
		->fetchAll();

	break;

	case 'lps':
	
		
		// get mastersheet years
		$years = $model->query("
			SELECT DISTINCT
				lps_mastersheet_year, 
				lps_mastersheet_year AS year
			FROM lps_mastersheets
		")
		->filter($filters)
		->exclude('year')
		->order('lps_mastersheet_year')
		->fetchAll();

	break;
}


// dropdown years
if ($years) {
	$toolbox[] = ui::dropdown($years, array(
		'name' => 'year',
		'id' => 'year',
		'class' => 'submit',
		'value' => $fYear,
		'caption' => $translate->all_years
	));
}

// toolbox: form
if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

$table->datagrid = $datagrid;

$table->caption('mastersheet_year', $translate->year);

$table->mastersheet_year(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=8%'
);

if ( ($application=='lps') && ($controller<>'consolidations' && $controller<>'salesorders' && !$archived)) {

	$table->mastersheet_pos_date(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=20%'
	);

	$table->mastersheet_phase_out_date(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=20%'
	);
}

$table->mastersheet_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	"href=".$_REQUEST['data']
);

// state column
if ($datagrid && _array::key_exists('state', $datagrid)) {
	$table->state(
		Table::ATTRIBUTE_NOWRAP,
		'width=10%'
	);
}

// ordersheets column
if ($datagrid && _array::key_exists('ordersheets', $datagrid)) {
	
	$caption = $application=='lps' ? 'Launch Plans' : 'Order Sheets';
	$table->attributes('ordersheets', array('class'=>'number'));
	
	$table->ordersheets(
		Table::ATTRIBUTE_NOWRAP,
		Table::FORMAT_NUMBER,
		'width=10%',
		"caption=$caption"
	);
}

// state column
if ($datagrid && _array::key_exists('total_cost', $datagrid)) {
	$table->attributes('total_cost', array('class'=>'number'));
	$table->total_cost(
		Table::ATTRIBUTE_NOWRAP,
		Table::FORMAT_NUMBER,
		'width=10%'
	);
}

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;

