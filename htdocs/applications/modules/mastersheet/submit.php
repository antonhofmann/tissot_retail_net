<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['mastersheet_id'];

$data = array();

switch ($application) {
				
	case 'mps':
	

		$tableName = 'mps_mastersheets';

		if (isset($_REQUEST['mastersheet_year'])) {
			$data['mps_mastersheet_year'] = $_REQUEST['mastersheet_year'];
		}

		if (isset($_REQUEST['mastersheet_name'])) {
			$data['mps_mastersheet_name'] = $_REQUEST['mastersheet_name'];
		}

	break;

	case 'lps':
	

		$tableName = 'lps_mastersheets';

		if (isset($_REQUEST['mastersheet_year'])) {
			$data['lps_mastersheet_year'] = $_REQUEST['mastersheet_year'];
		}

		if (isset($_REQUEST['mastersheet_name'])) {
			$data['lps_mastersheet_name'] = $_REQUEST['mastersheet_name'];
		}

		if (isset($_REQUEST['mastersheet_pos_date'])) {
			$data['lps_mastersheet_pos_date'] = $_REQUEST['mastersheet_pos_date'] ? date::sql($_REQUEST['mastersheet_pos_date']) : null;
		}

		if (isset($_REQUEST['mastersheet_phase_out_date'])) {
			$data['lps_mastersheet_phase_out_date'] = $_REQUEST['mastersheet_phase_out_date'] ? date::sql($_REQUEST['mastersheet_phase_out_date']) : null;
		}

		if (isset($_REQUEST['mastersheet_week_number_first'])) {
			$data['lps_mastersheet_week_number_first'] = $_REQUEST['mastersheet_week_number_first'];
		}

		if (isset($_REQUEST['mastersheet_weeks'])) {
			$data['lps_mastersheet_weeks'] = $_REQUEST['mastersheet_weeks'];
		}

		if (isset($_REQUEST['mastersheet_estimate_month'])) {
			$data['lps_mastersheet_estimate_month'] = $_REQUEST['mastersheet_estimate_month'];
		}

	break;
}

if ($data) {

	// get mastersheet
	$mastersheet = new Modul($application);
	$mastersheet->setTable($tableName);
	$mastersheet->read($id);

	if ($mastersheet->id) {
		
		$data['user_modified'] = $user->login;
		$data['date_modifeid'] = date('Y-m-d H:i:s');
		
		$response = $mastersheet->update($data);
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		
		$response = $id = $mastersheet->create($data);
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		
		if ($response = $_REQUEST['redirect']) {
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect,
	'id' => $id
));
