<?php 
		
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";

// execution time
ini_set('max_execution_time', 120);
ini_set('memory_limit', '1024M');

$settings = Settings::init();
$settings->load('data');
$user = User::instance();
$translate = Translate::instance();

$_ERRORS = array();
$_MESSAGES = array();
$_DATA = array();

define(DEBUGGING_MODE, false);

// request :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$filter = $_REQUEST['filter'];
$id = $_REQUEST['version'];

// get session stored request
$_REQUEST = session::filter($application, "$controller.$archived.$action", false);


// initialisation ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// db model
$model = new Model($application);

// version
$version = new Modul($application);
$version->setTable('lps_mastersheet_detail_lists');
$version->read($id);

if ($version->id) {

	// mastersheet
	$mastersheet = new Modul($application);
	$mastersheet->setTable('lps_mastersheets');
	$mastersheet->read($version->data['lps_mastersheet_detail_list_mastersheet_id']);

	// show approvment
	$_SHOW_APPROVED_COLUMN = true;

	// page headers
	$_SHEET_CAPTION = $archived ? 'Archived Master Sheet Items' : 'Master Sheet Items';
	$_PAGE_TITLE = $mastersheet->data['lps_mastersheet_name'].', '.$mastersheet->data['lps_mastersheet_year'];
	$_PAGE_SUBTITLE = ucfirst($version->data['lps_mastersheet_detail_list_name']);
	$_PAGE_PRINT = "Date: ".date('d.m.Y H:i:s', strtotime($version->data['date_created']));

	// export file name
	$_FILENAME  = "lps_detail_list_items";
	$_FILENAME .= str_replace(' ', '_', strtolower($mastersheet->data['lps_mastersheet_name']));
	$_FILENAME .= "_version_$id_".date('Ymd') ;

} else {
	$_ERRORS[] = "Detail list $id not found.";
}


// get mastersheet detail list items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEMS = array();

if (!$_ERRORS) {
	
	$sth = $model->db->prepare("
		SELECT
			lps_mastersheet_detail_list_item_id AS id,
			lps_mastersheet_detail_list_item_reference_id AS reference_id,
			lps_product_group_id AS group_id,
			lps_product_group_name AS group_name,
			lps_collection_code AS collection_code,
			lps_reference_code AS reference_code,
			lps_reference_name AS reference_name,
			currency_symbol,
			lps_mastersheet_detail_list_item_price AS price,
			lps_mastersheet_detail_list_item_total_quantity AS quantity,
			lps_mastersheet_detail_list_item_total_quantity_approved AS quantity_approved,
			lps_mastersheet_detail_list_item_total_quantity_estimate AS quantity_estimate,
			(lps_mastersheet_detail_list_item_price * lps_mastersheet_detail_list_item_total_quantity_approved) AS total_cost
		FROM lps_mastersheet_detail_list_items
		INNER JOIN lps_references ON lps_reference_id = lps_mastersheet_detail_list_item_reference_id
		LEFT JOIN lps_collections ON lps_collection_id = lps_reference_collection_id
		LEFT JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id
		INNER JOIN db_retailnet.currencies ON currency_id = lps_reference_currency_id
		WHERE lps_mastersheet_detail_list_item_list_id = ?
		ORDER BY lps_product_group_name ASC, lps_reference_code ASC
	");

	$sth->execute(array($version->id));
	$result = $sth->fetchAll();

	if ($result) {
		
		foreach ($result as $row) {
			
			$group = $row['group_id'];
			$item = $row['id'];	
			
			$_ITEMS[$group]['caption'] = $row['group_name'];
			$_ITEMS[$group]['data'][$item]['collection_code'] = $row['collection_code'];
			$_ITEMS[$group]['data'][$item]['reference_code'] = $row['reference_code'];
			$_ITEMS[$group]['data'][$item]['reference_name'] = $row['reference_name'];
			$_ITEMS[$group]['data'][$item]['price'] = $row['price'];
			$_ITEMS[$group]['data'][$item]['currency_symbol'] = $row['currency_symbol'];
			$_ITEMS[$group]['data'][$item]['quantity'] = $row['quantity'];
			$_ITEMS[$group]['data'][$item]['quantity_estimate'] = $row['quantity_estimate'];
			$_ITEMS[$group]['data'][$item]['quantity_approved'] = $row['quantity_approved'];
			$_ITEMS[$group]['data'][$item]['total_cost'] = $row['total_cost'];
		}	
	}
}

// spreadsheet references ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_FIRST_ROW = 1;
$_FIRST_COL = 1;
$_FIRST_FIXED_ROW = 1;
$_LAST_FIXED_ROW = 3;
$_FIRST_FIXED_COL = 1;
$_TOTAL_FIXED_ROWS = 4;

$_ROW_PAGE_TITLE = 1;
$_ROW_PAGE_SUBTITLE = 2;
$_ROW_PRINT_DATE = 3;

$_DATAGRID = array();
$_ROW_GROUPS = array();
$_ROW_GROUP_HEADERS = array();
$_ROW_SEPARATORS = array();
$_ROW_GROUP_TOTALS = array();
$_ROW_ITEMS = array();

$_DATAGRID[$_ROW_PAGE_TITLE][$_FIRST_FIXED_COL] = array(
	'value' => $_PAGE_TITLE,
	'style' => 'page-title',
	'height' => 50 
);

$_DATAGRID[$_ROW_PAGE_SUBTITLE][$_FIRST_FIXED_COL] = array(
	'value' => $_PAGE_SUBTITLE,
	'style' => 'page-subtitle',
	'height' => 30 
);

$_DATAGRID[$_ROW_PRINT_DATE][$_FIRST_FIXED_COL] = array(
	'value' => $_PAGE_PRINT
);

$_ROW_SEPARATORS[4] = true;

if (!$_ERRORS && $_ITEMS) {

	$row = $_TOTAL_FIXED_ROWS;

	foreach ($_ITEMS as $g => $group) {

		$row++;
		$_ROW_GROUPS[$row] = true;
		
		$col=$_FIRST_COL;
		$_DATAGRID[$row][$col]['caption'] = $group['caption'];
		$_DATAGRID[$row][$col]['group'] = $g;

		$row++;
		$_ROW_GROUP_HEADERS[$row] = true;

		$col=$_FIRST_COL;
		$_DATAGRID[$row][$col]['caption'] = 'Collection Code';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 20;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Reference';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 20;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Name';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 30;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Comment';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 20;

		$col++;
		$_COL_ESTIMATE_QUANTITY = $col;
		$_DATAGRID[$row][$col]['caption'] = 'Estimated Price';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 20;
		$_DATAGRID[$row][$col]['numeric'] = true;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Currency';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 10;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = "Estimated";
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 25;
		$_DATAGRID[$row][$col]['numeric'] = true;

		$col++;
		$_COL_PLANNED_QUANTITY = $col;
		$_DATAGRID[$row][$col]['caption'] = 'Total Quantity';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 15;
		$_DATAGRID[$row][$col]['numeric'] = true;	

		if ($_SHOW_APPROVED_COLUMN) {
			$col++;
			$_COL_TOTAL_APPROVED_QUANTITY = $col;
			$_DATAGRID[$row][$col]['caption'] = 'Total Approved';
			$_DATAGRID[$row][$col]['header'] = true;
			$_DATAGRID[$row][$col]['width'] = 15;
			$_DATAGRID[$row][$col]['numeric'] = true;
		}

		$col++;
		$_COL_TOTAL_PRICE = $col;
		$_DATAGRID[$row][$col]['caption'] = 'Total Price';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 15;
		$_DATAGRID[$row][$col]['numeric'] = true;

		// total datagrid columns
		$_TOTAL_GRID_COLS = $col;
			
		foreach ($group['data'] as $key => $item) {

			$row++;
			$_ROW_ITEMS[$row] = true;
			
			$col=$_FIRST_COL;
			$_DATAGRID[$row][$col]['value'] = $item['collection_code'];
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['reference_code'];
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['reference_name'];			

			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['reference_description'];
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['price'];
			$_DATAGRID[$row][$col]['numeric'] = true;
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['currency_symbol'];
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['quantity_estimate'];
			$_DATAGRID[$row][$col]['item'] = $key;
			$_DATAGRID[$row][$col]['numeric'] = true;
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['quantity'];
			$_DATAGRID[$row][$col]['numeric'] = true;

			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['quantity_approved'];
			$_DATAGRID[$row][$col]['numeric'] = true;

			// total price
			$col++;
			$quantity = $_SHOW_APPROVED_COLUMN ? $item['quantity_approved'] : $item['quantity']; 
			$totalPrice = $item['price']*$quantity;
			$_DATAGRID[$row][$col]['value'] = $totalPrice;
			$_DATAGRID[$row][$col]['numeric'] = true;
		}

		$row++;
		$_ROW_GROUP_TOTALS[$row] = true;

		$col=$_FIRST_COL;
		$_DATAGRID[$row][$col]['caption'] = 'Total '.$group['caption'];

		$row++;
		$_ROW_SEPARATORS[$row] = true;

		$col=$_FIRST_COL;
		$_DATAGRID[$row][$col]['separator'] = true;
	}

	$row++;
	$_ROW_SEPARATORS[$row] = true;

	// grand total
	$row++;
	$_ROW_GRAND_TOTAL = $row;
	$_DATAGRID[$row][$_FIRST_COL]['caption'] = 'Master Sheet Totals';

	$_TOTAL_GRID_ROWS = $row;
}


// spreadsheet styles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_STYLES = array(

	'allborders' => array(
		'borders' => array( 
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	'number' => array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
		)
	),
	'string' => array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
		)
	),
	'total-group' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12, 
			'bold'=>true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'f3f3f3')
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	'total-sheet' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 14, 
			'bold'=>true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'dedede')
		),
		'alignment' => array(
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	'group-header' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 16, 
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		)
	),
	'table-header' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 11, 
			'bold'=>true,
			'color' => array('rgb'=>'FFFFFF')
		),
		'alignment' => array(
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'000000')
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	'page-title' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 20, 
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		)
	),
	'page-subtitle' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 14,
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		)
	)
);


// spreadsheet builder :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


if (!$_ERRORS && $_TOTAL_GRID_ROWS && $_TOTAL_GRID_COLS) {

	// excel init
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name)->setTitle($_SHEET_CAPTION);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

	// sheet columns
	$_FIRST_COL_INDEX = spreadsheet::key($_FIRST_COL);
	$_LAST_COL_INDEX = spreadsheet::key($_TOTAL_GRID_COLS);

	for ($row=1; $row <= $_TOTAL_GRID_ROWS; $row++) {

		// row separator
		if ($_ROW_SEPARATORS[$row]) {
			$rowRange = $_FIRST_COL_INDEX.$row.":".$_LAST_COL_INDEX.$row;
			$objPHPExcel->getActiveSheet()->mergeCells($rowRange);
			continue;
		}

		// page title
		if ($row <= $_LAST_FIXED_ROW) {

			$index = $_FIRST_COL_INDEX.$row;
			$rowRange = $_FIRST_COL_INDEX.$row.":".$_LAST_COL_INDEX.$row;

			$value = $_DATAGRID[$row][$_FIRST_FIXED_COL]['value'];
			$style = $_DATAGRID[$row][$_FIRST_FIXED_COL]['style'];
			$height = $_DATAGRID[$row][$_FIRST_FIXED_COL]['height'];

			$objPHPExcel->getActiveSheet()->mergeCells($rowRange);
			$objPHPExcel->getActiveSheet()->setCellValue($index, $value);

			if ($style) $objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES[$style]);
			if ($height) $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight($height);
		}

		// page title
		if ($row <= $_LAST_FIXED_ROW) {

			$index = $_FIRST_COL_INDEX.$row;
			$rowRange = $_FIRST_COL_INDEX.$row.":".$_LAST_COL_INDEX.$row;

			$value = $_DATAGRID[$row][$_FIRST_FIXED_COL]['value'];
			$style = $_DATAGRID[$row][$_FIRST_FIXED_COL]['style'];
			$height = $_DATAGRID[$row][$_FIRST_FIXED_COL]['height'];

			$objPHPExcel->getActiveSheet()->mergeCells($rowRange);
			$objPHPExcel->getActiveSheet()->setCellValue($index, $value);

			if ($style) $objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES[$style]);
			if ($height) $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight($height);

			continue;
		}
			
		for ($col=1; $col <= $_TOTAL_GRID_COLS; $col++) {

			$_COL_INDEX = spreadsheet::key($col);
			$index = "$_COL_INDEX$row";
			$_ROW_RANGE_INDEX = $_FIRST_COL_INDEX.$row.":".$_LAST_COL_INDEX.$row;

			switch ($row) {

				// group title: collection
				case $_ROW_GROUPS[$row]:

					if ($col==$_FIRST_COL) {
						$group = $_DATAGRID[$row][$col]['group'];
						$objPHPExcel->getActiveSheet()->mergeCells($_ROW_RANGE_INDEX);
						$objPHPExcel->getActiveSheet()->setCellValue($index, $_DATAGRID[$row][$col]['caption']);
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['group-header']);
						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(40);
					}

				break;
			
				// group table header
				case $_ROW_GROUP_HEADERS[$row]:
					
					$objPHPExcel->getActiveSheet()->setCellValue($index, $_DATAGRID[$row][$col]['caption']);

					if ($_DATAGRID[$row][$col]['width']) {
						$objPHPExcel->getActiveSheet()->getColumnDimension($_COL_INDEX)->setWidth($_DATAGRID[$row][$col]['width']);
					}

					if ($col==$_TOTAL_GRID_COLS) {
						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
						$objPHPExcel->getActiveSheet()->getStyle($_ROW_RANGE_INDEX)->applyFromArray($_STYLES['table-header'], false);
					}

					if ($_DATAGRID[$row][$col]['numeric']) {
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']);
					}

				break;

				// lanchplan items
				case $_ROW_ITEMS[$row]:

					$value = $_DATAGRID[$row][$col]['value'];

					if ($_DATAGRID[$row][$col]['numeric']) {
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']);
					}
					
					switch ($col) {

						// total price
						case $_COL_TOTAL_PRICE:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
							$objPHPExcel->getActiveSheet()->getStyle($_ROW_RANGE_INDEX)->applyFromArray($_STYLES['allborders'], false);
							$_SUM_COL['total-price'][$col][$group] = $_SUM_COL['total-price'][$col][$group] + $value;
						break;

						default:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
							$_SUM_COL['total-price'][$col][$group] = $_SUM_COL['total-price'][$col][$group] + $value;
						break;
					}
					
				break;

				// collection subtotal
				case $_ROW_GROUP_TOTALS[$row]:

					switch ($col) {
						
						// subtotal caption
						case $_FIRST_COL:
							$range = "$index:".spreadsheet::key($col+5,$row);
							$objPHPExcel->getActiveSheet()->mergeCells($range);
							$objPHPExcel->getActiveSheet()->setCellValue($index, $_DATAGRID[$row][$col]['caption']);
						break;

						// total price
						case $_COL_TOTAL_PRICE:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $_SUM_COL['total-price'][$col][$group]);
							$objPHPExcel->getActiveSheet()->getStyle($_ROW_RANGE_INDEX)->applyFromArray($_STYLES['total-group'], false);
							$_SUM_COL['grand-total-price'][$col] = $_SUM_COL['grand-total-price'][$col] + $_SUM_COL['total-price'][$col][$group];
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
						break;

						default:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $_SUM_COL['total-price'][$col][$group]);
							$_SUM_COL['grand-total-price'][$col] = $_SUM_COL['grand-total-price'][$col] + $_SUM_COL['total-price'][$col][$group];
						break;
					}

				break;

				case $_ROW_GRAND_TOTAL:
					
					switch ($col) {

						// grand total caption
						case $_FIRST_COL:
							$range = "$index:".spreadsheet::key($col+5,$row);
							$objPHPExcel->getActiveSheet()->mergeCells($range);
							$objPHPExcel->getActiveSheet()->setCellValue($index, $_DATAGRID[$row][$col]['caption']);
						break;

						// total price
						case $_COL_TOTAL_PRICE:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $_SUM_COL['grand-total-price'][$col]);
							$objPHPExcel->getActiveSheet()->getStyle($_ROW_RANGE_INDEX)->applyFromArray($_STYLES['total-sheet'], false);
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
						break;

						default:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $_SUM_COL['grand-total-price'][$col]);
						break;
					}

				break;
			}
		}
	}	
}

if ($_ERRORS) {

	echo "<pre>";
	echo "ERRORS:";
	print_r($_ERRORS);
	echo "</pre>";

} elseif ($objPHPExcel) {
		
	$objPHPExcel->getActiveSheet()->setTitle($_SHEET_CAPTION);
	$objPHPExcel->setActiveSheetIndex(0);

	// redirect output to client browser
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$_FILENAME.'.xlsx"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
}
else {
	message::empty_result();
	url::redirect("/$application/$controller$archived/$action/$id");
}
