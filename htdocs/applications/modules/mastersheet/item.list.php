<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

// session request
$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

$MPS = in_array($application, array('mps')) ? true : false;
$LPS = in_array($application, array('lps')) ? true : false;

// database model
$model = new Model($application);


switch ($application) {
				
	case 'mps':
	

		$tableName = 'mps_mastersheets';

		$sqlMasterSheetItems = "
			SELECT DISTINCT
				mps_material_material_planning_type_id AS group_id,
				mps_material_material_collection_category_id AS subgroup_id
			FROM mps_mastersheet_items
			INNER JOIN mps_materials ON mps_material_id = mps_mastersheet_item_material_id
			WHERE mps_mastersheet_item_mastersheet_id = ?
		";

	break;

	case 'lps':
	

		$tableName = 'lps_mastersheets';
		$permissionEdit = user::permission('can_edit_lps_master_sheets');

		$sqlMasterSheetItems = "
			SELECT DISTINCT
				lps_reference_collection_id AS group_id,
				lps_reference_collection_category_id AS subgroup_id
			FROM lps_mastersheet_items
			INNER JOIN lps_references ON lps_reference_id = lps_mastersheet_item_reference_id
			WHERE lps_reference_collection_id > 0 AND lps_reference_collection_category_id > 0
			AND lps_mastersheet_item_mastersheet_id = ?
		";

	break;
}

// get mastersheet
$mastersheet = new Modul($application);
$mastersheet->setTable($tableName);
$mastersheet->read($id);


// get collection categories from master sheet items
$sth = $model->db->prepare($sqlMasterSheetItems);
$sth->execute(array($mastersheet->id));
$result = $sth->fetchAll();

if ($result) {
	
	$datagrid = array();
	$currentItems = array();
	$deliveryDates = array();

	switch ($application) {
				
		case 'mps':
		

			$sqlCurrentItems = "
				SELECT mps_mastersheet_item_material_id AS item
				FROM mps_mastersheet_items
				WHERE mps_mastersheet_item_mastersheet_id = ?
			";

			$sqlDeliverDates = "
				SELECT 
					mps_mastersheet_delivery_date_material_planning_type_id AS planning, 
					mps_mastersheet_delivery_date_material_collection_category_id AS category,
					DATE_FORMAT(mps_mastersheet_delivery_date_date, '%d.%m.%Y') AS date
				FROM mps_mastersheet_delivery_dates
				WHERE mps_mastersheet_delivery_date_mastersheet_id = ?
			";

			$sqlItems = "
				SELECT DISTINCT
					mps_material_id AS id,
					mps_material_hsc AS hsc,
					mps_material_code AS code,
					mps_material_name AS name,
					mps_material_price AS price,
					mps_material_setof AS setof,
					mps_material_collection_code AS collection,
					mps_material_planning_type_id AS group_id,
					mps_material_planning_type_name AS group_name,
					mps_material_collection_category_id AS subgroup_id,
					mps_material_collection_category_code AS subgroup_name,
					currency_symbol AS currency
				FROM mps_materials
				INNER JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id
				INNER JOIN mps_material_collections ON mps_material_collection_id = mps_material_material_collection_id
				INNER JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id
				INNER JOIN db_retailnet.currencies ON currency_id = mps_material_currency_id
				WHERE mps_material_material_planning_type_id = ? AND mps_material_collection_category_id = ?
				ORDER BY
					mps_material_planning_type_name,
					mps_material_collection_category_code,
					mps_material_code
			";

		break;

		case 'lps':
		

			$sqlCurrentItems = "
				SELECT 
					lps_mastersheet_item_reference_id AS item,
					lps_mastersheet_item_reference_comment AS comment
				FROM lps_mastersheet_items
				WHERE lps_mastersheet_item_mastersheet_id = ?
			";

			$sqlItems = "
				SELECT DISTINCT 
					lps_reference_id AS id,
					lps_reference_code AS code,
					lps_reference_name AS name,
					lps_reference_price_point AS price,
					lps_collection_code AS collection,
					lps_product_group_id AS group_id,
					lps_product_group_name AS group_name,
					lps_collection_category_id AS subgroup_id,
					lps_collection_category_code AS subgroup_name,
					currency_symbol AS currency
				FROM lps_references
				INNER JOIN lps_collections ON lps_collection_id = lps_reference_collection_id
				INNER JOIN lps_collection_categories ON lps_collection_category_id = lps_reference_collection_category_id
				INNER JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id
				INNER JOIN db_retailnet.currencies ON currency_id = lps_reference_currency_id
				WHERE lps_collection_id = ? AND lps_collection_category_id = ?
				ORDER BY
					lps_collection_code,
					lps_collection_category_code,
					lps_reference_code
			";

		break;
	}
	
	// current master sheet items
	$sth = $model->db->prepare($sqlCurrentItems);
	$sth->execute(array($mastersheet->id));
	$res = $sth->fetchAll();
	$currentItems = array();
	
	if ($res) {
		foreach ($res as $row) {
			$k = $row['item'];
			$currentItems[$k] = $row['comment'];
		}
	}
	//echo "<pre>"; print_r($currentItems); echo "</pre>";
	// current delivery dates
	if ($sqlDeliverDates) {
		
		$sth = $model->db->prepare($sqlDeliverDates);
		$sth->execute(array($mastersheet->id));
		$res = $sth->fetchAll();

		if ($res) {
			foreach ($res as $row) {
				$planning = $row['planning'];
				$collection = $row['category'];
				$deliveryDates[$planning][$collection] = $row['date'];
			}
		}
	}

	// get items
	$sth = $model->db->prepare($sqlItems);

	foreach ($result as $row) {
		
		$sth->execute(array_filter(array($row['group_id'], $row['subgroup_id'])));
		$items = $sth->fetchAll();
		
		if ($items) {

			$iconChecked = ui::icon('checked');
			
			foreach ($items as $item) {
				
				$group = $item['group_id'];
				$subgroup = $item['subgroup_id'];
				$key = $item['id'];	

				$checked = array_key_exists($key,$currentItems) ? 'checked=checked' : null;
				$commentDisabled = !$archived && $permissionEdit && $checked ? null : 'disabled=disabled';
				$comment = $currentItems[$key];
				
				$datagrid[$group]['caption'] = $item['group_name'];
				$datagrid[$group]['subgroup'][$subgroup]['caption'] = $item['subgroup_name'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['collection'] = $item['collection'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['hsc'] = $item['hsc'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['code'] = $item['code'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['name'] = $item['name'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['price'] = number_format($item['price'], 2, '.', '');
				$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['comment'] = "<input type='text' class='input-comment' name=comment[$key] value='$comment' data-id='$key' $commentDisabled >";
				$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['currency'] = $item['currency'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['setof'] = $item['setof'];
				
				if ($_REQUEST['editabled']) {
					$box = "<input type=checkbox name=material[$key] class=material value=$key $checked />";
					$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['box'] = $box;
					if ($checked) $checkall[$group][$subgroup] = $checkall[$group][$subgroup]+1;
				} 
				elseif (!$archived) {
					$box = isset($currentItems[$key]) ? $iconChecked : null;
					$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['box'] = $box;
				}
			}
		}
	}
}

if ($datagrid) {

	foreach ($datagrid as $group => $data) {
			
		$list  .= "<h5>{$data[caption]}</h5>";

		foreach ($data['subgroup'] as $subgroup => $items) {

			$delivery = null;
			$checkbox = null;
			$collection_delivery_date = $deliveryDates[$group][$subgroup];
			
			// checkbox: check all
			if ($_REQUEST['editabled']) {
				$checked = ($checkall[$group][$subgroup] == count($items['items'])) ? 'checked=checked' : null;
				$checkbox = "<input type=checkbox name=checkall[$subgroup] data=table_$group$subgroup class=checkall value=1 $checked />";
			}

			// input: delivery date
			if ($MPS) {
				if ($_REQUEST['editabled']) {
					$delivery = "
						<span class=delivery >
							Delivery Date: <input type=text name=delivery[$subgroup] class='deliverydate' planning=$group  category=$subgroup  value='$collection_delivery_date'/>
						</span>
					";
				}
				elseif ($collection_delivery_date) {
					$delivery = "<span class=delivery >Delivery Date: $collection_delivery_date</span>";
				}
			}

			$list  .= "<h6>{$items[caption]} $delivery</h6>";

			$table = new Table(array(
				'id' => 'table_'.$group.$subgroup		
			));
			
			$table->datagrid = $items['items'];
			
			$table->collection(
				Table::ATTRIBUTE_NOWRAP, 
				'width=10%',
				'caption=Collection'
			);
			
			$caption =  $MPS ? $translate->mps_material_code : $translate->reference_code;

			$table->code(
				Table::ATTRIBUTE_NOWRAP, 
				'width=15%',
				"caption=$caption"
			);
			
			$caption = $MPS ? $translate->mps_material_name : $translate->reference_name;

			$table->name(
				"caption=$caption"
			);

			if ($MPS) {
				$table->hsc(
					Table::ATTRIBUTE_NOWRAP, 
					'width=5%',
					'caption='.$translate->mps_material_hsc
				);
			}

			if ($LPS) {
				$table->comment(
					Table::ATTRIBUTE_NOWRAP,
					'width=10%',
					'caption=Comment'
				);
			}
			
			if ($MPS) {
				$table->setof(
					Table::ATTRIBUTE_NOWRAP,
					Table::ATTRIBUTE_ALIGN_RIGHT, 
					'width=5%',
					'caption=Set of'
				);
			}
			
			$caption = $MPS ? $translate->mps_material_price : $translate->reference_price_point;

			$table->price(
				Table::ATTRIBUTE_NOWRAP, 
				Table::ATTRIBUTE_ALIGN_RIGHT, 
				'width=5%',
				"caption=$caption" 
			);
			
			$table->currency(
				Table::ATTRIBUTE_NOWRAP, 
				'width=20px',
				'caption=Currency'
			);
			
			if (_array::key_exists('box', $items['items'])) {
				
				$table->caption('box', $checkbox);
			
				$table->box(
					Table::ATTRIBUTE_NOWRAP, 
					'width=1%'
				);
			}
			
			$list .= $table->render();
		}
	}
}
else  {
	$list = html::emptybox($translate->empty_result);
}


echo $list;
	