<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

define('DEBUGGING_MODE', false);

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$_ERRORS = array();
$_MESSAGES = array();


switch ($application) {
				
	case 'mps':
	

		$MPS = true;
		$_STATE_ARCHIVED = 16;
		$tableName = "mps_mastersheets";

		$qArchiveMastersheet = "
			UPDATE mps_mastersheets SET 
				mps_mastersheet_consolidated = 1, 
				mps_mastersheet_archived = 1,
				user_modified = ?,
				date_modifeid = NOW()
			WHERE mps_mastersheet_id = ?
		";

		$qGetOrders = "
			SELECT mps_ordersheet_id AS ordersheet
			FROM mps_ordersheets
			WHERE mps_ordersheet_mastersheet_id = ? AND mps_ordersheet_workflowstate_id NOT IN (6,15,16)
		";

		$qArchiveOrders = "
			UPDATE mps_ordersheets SET 
				mps_ordersheet_workflowstate_id = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE mps_ordersheet_id = ?
		";

		$qGetItems = "
			SELECT DISTINCT
				mps_ordersheet_item_id AS item,
				mps_ordersheet_item_quantity AS quantity,
				mps_ordersheet_item_quantity_approved AS approved,
				mps_ordersheet_item_desired_delivery_date AS date,
				address_mps_customernumber AS customernumber,
				address_mps_shipto AS shipto
			FROM mps_ordersheet_items	
			INNER JOIN mps_ordersheets ON mps_ordersheet_item_ordersheet_id = mps_ordersheet_id
			INNER JOIN db_retailnet.addresses ON address_id = mps_ordersheet_address_id
			WHERE mps_ordersheet_mastersheet_id = ? AND mps_ordersheet_workflowstate_id NOT IN (6,15,16)
		";

		$qUpdateItems = "
			UPDATE mps_ordersheet_items SET
				mps_ordersheet_item_quantity_approved = :approved,
				mps_ordersheet_item_quantity_confirmed = 0,
				mps_ordersheet_item_customernumber = :customernumber,
				mps_ordersheet_item_shipto = :shipto,
				user_modified = :user,
				date_modified = NOW()
			WHERE mps_ordersheet_item_id = :item
		";

		$qTracker = "
			INSERT INTO user_trackings (
				user_tracking_entity,
				user_tracking_entity_id,
				user_tracking_user_id,
				user_tracking_action,
				user_created,
				date_created
			)
			VALUES ('order sheet', ?, ?, 'archived', ?, NOW())
		";

	break;
				
	case 'lps':
	

		$LPS = true;
		$_STATE_ARCHIVED = 11;
		$tableName = "lps_mastersheets";

		$qArchiveMastersheet = "
			UPDATE lps_mastersheets SET 
				lps_mastersheet_consolidated = 1, 
				lps_mastersheet_archived = 1,
				user_modified = ?,
				date_modifeid = NOW()
			WHERE lps_mastersheet_id = ?
		";

		$qGetOrders = "
			SELECT lps_launchplan_id AS ordersheet
			FROM lps_launchplans
			WHERE lps_launchplan_mastersheet_id = ? AND lps_launchplan_workflowstate_id NOT IN (9,10,11)
		";

		$qArchiveOrders = "
			UPDATE lps_launchplans SET 
				lps_launchplan_workflowstate_id = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE lps_launchplan_id = ?
		";

		$qGetItems = "
			SELECT 
				lps_launchplan_item_id AS item,
				lps_launchplan_item_quantity AS quantity,
				lps_launchplan_item_quantity_approved AS approved,
				lps_launchplan_item_desired_delivery_date AS date,
				address_mps_customernumber AS customernumber,
				address_mps_shipto AS shipto
			FROM lps_launchplan_items	
			INNER JOIN lps_launchplans ON lps_launchplan_item_launchplan_id = lps_launchplan_id
			INNER JOIN db_retailnet.addresses ON address_id = lps_launchplan_address_id
			WHERE lps_launchplan_mastersheet_id = ? AND lps_launchplan_workflowstate_id NOT IN (9,10,11)
		";

		$qUpdateItems = "
			UPDATE lps_launchplan_items SET
				lps_launchplan_item_quantity_approved = :approved,
				lps_launchplan_item_customernumber = :customernumber,
				user_modified = :user,
				date_modified = NOW()
			WHERE lps_launchplan_item_id = :item
		";

		$qTracker = "
			INSERT INTO user_trackings (
				user_tracking_entity,
				user_tracking_entity_id,
				user_tracking_user_id,
				user_tracking_action,
				user_created,
				date_created
			)
			VALUES ('launch plan', ?, ?, 'archived', ?, NOW())
		";

	break;
}

$model = new Model($application);

$mastersheet = new Modul($application);
$mastersheet->setTable($tableName);
$mastersheet->read($id);

if (!$mastersheet->id) {
	$_ERRORS[] = "The Master Sheet not found";
	Message::request_failure();
}


//  set master sheet to archive ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ARCHIVED = false;

if (!$_ERRORS) {
	$sth = $model->db->prepare($qArchiveMastersheet);
	$_ARCHIVED = !DEBUGGING_MODE ? $sth->execute(array($user->login, $mastersheet->id)) : true;
}

if ($_ARCHIVED) {
	$_MESSAGES[] = "The master Sheet is archived";
	$redirect = "/$application/$controller/archived/$action/$id";
	Message::success('message_mastersheet_put_to_archive');
} else {
	$_ERRORS[] = "The master sheet cannot be archived.";
	Message::error('error_mastersheet_put_to_archive');
}


// update order sheet items data :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {

	$_MESSAGES[] = "<< START UPDATE ORDER SHEET ITEMS >>";

	// get all involved ordersheet items
	$sth = $model->db->prepare($qGetItems);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll() ?: array();

	// update order sheet items customer data
	$sth = $model->db->prepare($qUpdateItems);
	
	foreach ($result as $row) {
		
		$approved = $row['approved'] ?: $row['quantity'];

		$data = array(
			'approved' => $approved ?: 0,
			'customernumber' => $row['customernumber'],
			'user' => $user->login,
			'item' => $row['item']
		);

		if ($MPS) $data['shipto'] = $row['shipto'];

		if (DEBUGGING_MODE) $response = true;
		else $response = $sth->execute($data);

		if ($response) {
			$_MESSAGES[] = "Update Item {$row[item]}";
		}
	}

	$_MESSAGES[] = "<< END UPDATE ORDER SHEET ITEMS >>";
}


// update order sheet states :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS) {

	$_MESSAGES[] = "<< START UPDATE ORDER SHEETS >>";

	// get all involved order sheets
	$sth = $model->db->prepare($qGetOrders);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();

	if ($result) {
		
		// set all involved ordersheets
		$sth = $model->db->prepare($qArchiveOrders);
		$tracker = $model->db->prepare($qTracker);

		foreach ($result as $row) {
			
			$key = $row['ordersheet']; 
			
			if (DEBUGGING_MODE) $response = true;
			else $response = $sth->execute(array($_STATE_ARCHIVED, $user->login, $key));

			if ($response) {
				$_MESSAGES[] = "Update order sheet $key";
				$tracker->execute(array($key, $user->id, $user->login));
			} else {
				$info = $sth->errorInfo();
				$_ERRORS[] = $info[2];
			}
		}
	}

	$_MESSAGES[] = "<< END UPDATE ORDER SHEETS >>";
}

if (DEBUGGING_MODE) {
	echo "<pre>";
	print_r($_ERRORS);
	print_r($_MESSAGES);
	die();
}
else {

	if ($_ERRORS) {
		$redirect = "/$application/$controller/$action/$id";
	}

	url::redirect($redirect);
}


