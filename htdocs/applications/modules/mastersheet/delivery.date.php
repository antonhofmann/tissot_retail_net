<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$translate = translate::instance();

$application = $_REQUEST['application'];
$id = $_REQUEST['mastersheet'];
$group = $_REQUEST['group'];
$subgroup = $_REQUEST['subgroup'];
$date = $_REQUEST['date'];

// mastersheet
$mastersheet = new Mastersheet($application);
$mastersheet->read($id);

if ($mastersheet->id && $group && $subgroup) {
	
	// master sheet delivery dates
	$mastersheet->deliveryDate()->read($group, $subgroup);
	
	// delete date
	if (!$date) {
		$response = $mastersheet->deliveryDate()->delete($group, $subgroup);
	}
	// update delivery date
	else {
		
		$date = (check::dateformat($date)) ? date::sql($date) : null;
		
		if ($date) {
			
			if ($mastersheet->deliveryDate()->id) {
				$response = $mastersheet->deliveryDate()->update($group, $subgroup, $date);
			} 
			// create new delivery date
			else {
				$response = $mastersheet->deliveryDate()->create($group, $subgroup, $date);
			}
			
			$message = ($response) ? $translate->message_request_saved : $translate->message_request_failure;
		}
		else {
			$response = false;
			$message = $translate->error_date(array('field' => $translate->delivery_date));
		}
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message	
	));
}