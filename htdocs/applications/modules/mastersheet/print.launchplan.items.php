<?php 
		
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";

// execution time
ini_set('max_execution_time', 120);
ini_set('memory_limit', '1024M');

$settings = Settings::init();
$settings->load('data');
$user = User::instance();
$translate = Translate::instance();

$_ERRORS = array();
$_MESSAGES = array();
$_DATA = array();

define(DEBUGGING_MODE, false);

// request :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$filter = $_REQUEST['filter'];
$id = $_REQUEST['mastersheet'];

// get session stored request
$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

$_LAUNCHPLAN = $_REQUEST['launchplan'];
$_FILTER = $_LAUNCHPLAN ? " AND lps_launchplan_id = $_LAUNCHPLAN" : null;

// initialisation ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// db model
$model = new Model($application);

// mastersheet
$mastersheet = new Modul($application);
$mastersheet->setTable('lps_mastersheets');
$mastersheet->read($id);

$_MASTERSHEET_FIRST_WEEK = $mastersheet->data['lps_mastersheet_week_number_first'];
$_MASTERSHEET_TOTAL_WEEKS = $mastersheet->data['lps_mastersheet_weeks'];
$_MASTERSHEET_ESTIMATE_MONTHS = $mastersheet->data['lps_mastersheet_estimate_month'];

// titles
$_SHEET_CAPTION = $archived ? 'Archived Master Sheet Items' : 'Master Sheet Items';
$_PAGE_TITLE = $mastersheet->data['lps_mastersheet_name'].', '.$mastersheet->data['lps_mastersheet_year'];

// show approvment
$_SHOW_APPROVED_COLUMN = true;

// export file name
$_FILENAME  = "lps_consolidated_launchplan_items_";
$_FILENAME .= str_replace(' ', '_', strtolower($mastersheet->data['lps_mastersheet_name']));
$_FILENAME .= "_".date('Ymd') ;


// get launchplan items ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEMS = array();

$sth = $model->db->prepare("
	SELECT DISTINCT
		lps_launchplan_item_reference_id AS id,
		lps_launchplan_item_price AS price, 
		currency_symbol AS currency_symbol, 
		lps_launchplan_item_exchangrate AS currency_exchangrate, 
		lps_launchplan_item_factor AS currency_factor, 
		SUM(lps_launchplan_item_quantity) AS quantity,
		SUM(lps_launchplan_item_quantity_approved) AS quantity_approved,
		SUM(lps_launchplan_item_quantity_estimate) AS quantity_estimate,
		lps_reference_code AS reference_code, 
		lps_reference_name AS reference_name, 
		lps_launchplan_item_comment AS reference_description,
		lps_collection_code AS collection_code,
		lps_collection_category_code AS collection_category,
		lps_mastersheet_estimate_month AS estimate_month,							
		lps_product_group_id AS group_id, 
		lps_product_group_name AS group_name, 
		lps_collection_category_id AS subgroup_id,
		lps_collection_category_code AS subgroup_name
	FROM lps_launchplan_items 
	INNER JOIN lps_references ON lps_launchplan_items.lps_launchplan_item_reference_id = lps_references.lps_reference_id
	INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
	INNER JOIN lps_mastersheets ON lps_mastersheet_id = lps_launchplan_mastersheet_id
	INNER JOIN lps_product_groups ON lps_references.lps_reference_product_group_id = lps_product_groups.lps_product_group_id
	INNER JOIN lps_collections ON lps_references.lps_reference_collection_id = lps_collections.lps_collection_id
	INNER JOIN lps_collection_categories ON lps_references.lps_reference_collection_category_id = lps_collection_categories.lps_collection_category_id
	INNER JOIN db_retailnet.currencies ON currency_id = lps_launchplan_item_currency
	WHERE lps_launchplan_mastersheet_id = ? $_FILTER
	GROUP BY lps_reference_id
	ORDER BY 
		lps_product_group_name,
		lps_collection_category_code,
		lps_collection_code,
		lps_reference_code
");

$sth->execute(array($mastersheet->id));
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$group = $row['group_id'];
		$item = $row['id'];	
		
		$_ITEMS[$group]['caption'] = $row['group_name'];
		$_ITEMS[$group]['data'][$item]['collection_code'] = $row['collection_code'];
		$_ITEMS[$group]['data'][$item]['collection_category'] = $row['collection_category'];
		$_ITEMS[$group]['data'][$item]['reference_code'] = $row['reference_code'];
		$_ITEMS[$group]['data'][$item]['reference_name'] = $row['reference_name'];
		$_ITEMS[$group]['data'][$item]['reference_description'] = $row['reference_description'];
		$_ITEMS[$group]['data'][$item]['estimate_month'] = $row['estimate_month'];
		$_ITEMS[$group]['data'][$item]['price'] = $row['price'];
		$_ITEMS[$group]['data'][$item]['currency_symbol'] = $row['currency_symbol'];
		$_ITEMS[$group]['data'][$item]['currency_exchangrate'] = $row['currency_exchangrate'];
		$_ITEMS[$group]['data'][$item]['currency_factor'] = $row['currency_factor'];
		$_ITEMS[$group]['data'][$item]['quantity'] = $row['quantity'];
		$_ITEMS[$group]['data'][$item]['quantity_estimate'] = $row['quantity_estimate'];
		$_ITEMS[$group]['data'][$item]['quantity_approved'] = $row['quantity_approved'];
	}	
}

// get item week quantities ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEM_WEEK_QUANTITIES = array();
$_TOTAL_ITEM_WEEK_QUANTITIES = array();
	
$sth = $model->db->prepare("
	SELECT DISTINCT
		lps_launchplan_item_reference_id AS item,
		lps_launchplan_item_week_quantity_week AS week,
		SUM(lps_launchplan_item_week_quantity_quantity) AS quantity,
		SUM(lps_launchplan_item_week_quantity_quantity_approved) AS quantity_approved
	FROM lps_launchplan_item_week_quantities
	INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_week_quantity_item_id
	INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
	WHERE lps_launchplan_mastersheet_id = ? $_FILTER
	GROUP BY lps_launchplan_item_reference_id, lps_launchplan_item_week_quantity_week
");

$sth->execute(array($mastersheet->id));
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$item = $row['item'];
		$week = $row['week'];
		
		$_ITEM_WEEK_QUANTITIES[$item][$week] = array(
			'id' => $item,
			'quantity' => $row['quantity'],
			'quantity_approved' => $row['quantity_approved']
		);

		$_TOTAL_ITEM_WEEK_QUANTITIES[$item] += $row['quantity'];
	}
}

// get launch plan items planned quantities ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEM_PLANNED_QUANTITIES = array();

$sth = $model->db->prepare("
	SELECT 
		lps_launchplan_item_reference_id AS item,
		SUM(lps_launchplan_item_planned_quantity_quantity) AS total_quantities
	FROM lps_launchplan_item_planned_quantities
	INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_planned_quantity_item_id
	INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
	WHERE lps_launchplan_mastersheet_id = ? $_FILTER
	GROUP BY lps_launchplan_item_reference_id
");

$sth->execute(array($mastersheet->id));
$result = $sth->fetchAll();

$_ITEM_PLANNED_QUANTITIES = _array::extract($result);


// spreadsheet references ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


$_FIRST_FEXED_ROW = 1;
$_FIRST_FIXED_COL = 1;
$_TOTAL_FIXED_ROWS = 1;
$_TOTAL_FIXED_COLS = 10;

$_DATAGRID = array();
$_ROW_GROUPS = array();
$_ROW_GROUP_HEADERS = array();
$_ROW_SEPARATORS = array();
$_ROW_GROUP_TOTALS = array();

if ($_ITEMS) {

	$row=$_TOTAL_FIXED_ROWS;

	foreach ($_ITEMS as $g => $group) {

		$row++;
		$_ROW_GROUPS[$row] = true;
		
		$col=$_FIRST_FIXED_COL;
		$_DATAGRID[$row][$col]['caption'] = $group['caption'];
		$_DATAGRID[$row][$col]['group'] = $g;

		$row++;
		$_ROW_GROUP_HEADERS[$row] = true;

		$col=$_FIRST_FIXED_COL;
		$_DATAGRID[$row][$col]['caption'] = 'Collection Code';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 20;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Reference';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 20;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Name';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 30;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Collection Category';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 30;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Comment';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 20;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Estimated Price';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 20;
		$_DATAGRID[$row][$col]['numeric'] = true;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Currency';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 10;

		$col++;
		$_COL_ESTIMATE_QUANTITY = $col;
		$_DATAGRID[$row][$col]['caption'] = "Estimated for $_MASTERSHEET_ESTIMATE_MONTHS months";
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 25;
		$_DATAGRID[$row][$col]['numeric'] = true;

		$col++;
		$_COL_PLANNED_QUANTITY = $col;
		$_DATAGRID[$row][$col]['caption'] = 'Total Planned';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 15;
		$_DATAGRID[$row][$col]['numeric'] = true;

		$col++;
		$_COL_ESTIMATE_QUOTE = $col;
		$_DATAGRID[$row][$col]['caption'] = '% of Estimate';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 20;
		$_DATAGRID[$row][$col]['numeric'] = true;

		for ($i=0; $i < $_MASTERSHEET_TOTAL_WEEKS; $i++) { 
			$col++;
			$week = $_MASTERSHEET_FIRST_WEEK+$i;
			$_DATAGRID[$row][$col]['caption'] = "Week $week";
			$_DATAGRID[$row][$col]['header'] = true;
			$_DATAGRID[$row][$col]['numeric'] = true;
		}

		$col++;
		$_COL_TOTAL_QUANTITY = $col;
		$_DATAGRID[$row][$col]['caption'] = 'Total Launch';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 15;
		$_DATAGRID[$row][$col]['numeric'] = true;

		if ($_SHOW_APPROVED_COLUMN) {
			
			for ($i=0; $i < $_MASTERSHEET_TOTAL_WEEKS; $i++) { 
				$col++;
				$week = $_MASTERSHEET_FIRST_WEEK+$i;
				$_DATAGRID[$row][$col]['caption'] = "Week $week";
				$_DATAGRID[$row][$col]['header'] = true;
				$_DATAGRID[$row][$col]['numeric'] = true;
			}

			$col++;
			$_COL_TOTAL_APPROVED_QUANTITY = $col;
			$_DATAGRID[$row][$col]['caption'] = 'Total Approved';
			$_DATAGRID[$row][$col]['header'] = true;
			$_DATAGRID[$row][$col]['width'] = 15;
			$_DATAGRID[$row][$col]['numeric'] = true;
		} 

		$col++;
		$_COL_TOTAL_PRICE = $col;
		$_DATAGRID[$row][$col]['caption'] = 'Total Price';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 15;
		$_DATAGRID[$row][$col]['numeric'] = true;

		// total datagrid columns
		$_TOTAL_GRID_COLS = $col;
			
		foreach ($group['data'] as $key => $item) {

			$row++;
			$_ROW_GRIDS[$row] = true;
			
			$col=$_FIRST_FIXED_COL;
			$_DATAGRID[$row][$col]['value'] = $item['collection_code'];
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['reference_code'];
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['reference_name'];	
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['collection_category'];			

			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['reference_description'];
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['price'];
			$_DATAGRID[$row][$col]['numeric'] = true;
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['currency_symbol'];
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['quantity_estimate'];
			$_DATAGRID[$row][$col]['item'] = $key;
			$_DATAGRID[$row][$col]['numeric'] = true;
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $_ITEM_PLANNED_QUANTITIES[$key];
			$_DATAGRID[$row][$col]['numeric'] = true;
			
			$col++;
			$totalWeekQuantities = $_TOTAL_ITEM_WEEK_QUANTITIES[$key];
			$_DATAGRID[$row][$col]['value'] = $item['estimate_month'] ? ($totalWeekQuantities/$item['estimate_month'])*100 : null;
			$_DATAGRID[$row][$col]['numeric'] = true;

			$totalQuantity = 0;

			for ($i=0; $i < $_MASTERSHEET_TOTAL_WEEKS; $i++) { 
				
				$col++;
				$week = $_MASTERSHEET_FIRST_WEEK+$i;
				
				$_DATAGRID[$row][$col]['item'] = $key;
				$_DATAGRID[$row][$col]['week'] = $week;
				$_DATAGRID[$row][$col]['id'] = $_ITEM_WEEK_QUANTITIES[$key][$week]['id'];
				$_DATAGRID[$row][$col]['value'] = $_ITEM_WEEK_QUANTITIES[$key][$week]['quantity'];
				$totalQuantity += $_ITEM_WEEK_QUANTITIES[$key][$week]['quantity'];
			}

			// col total quantities
			$col++;
			$_DATAGRID[$row][$col]['value'] = $totalQuantity;

			if ($_SHOW_APPROVED_COLUMN) {

				$totalQuantityApproved = 0;
				
				for ($i=0; $i < $_MASTERSHEET_TOTAL_WEEKS; $i++) { 
					
					$col++;
					$week = $_MASTERSHEET_FIRST_WEEK+$i;
					
					$_DATAGRID[$row][$col]['item'] = $key;
					$_DATAGRID[$row][$col]['week'] = $week;
					$_DATAGRID[$row][$col]['id'] = $_ITEM_WEEK_QUANTITIES[$key][$week]['id'];
					$_DATAGRID[$row][$col]['value'] = $_ITEM_WEEK_QUANTITIES[$key][$week]['quantity_approved'];
					$totalQuantityApproved = $totalQuantityApproved + $_ITEM_WEEK_QUANTITIES[$key][$week]['quantity_approved'];
				}

				// col total approved quantities
				$col++;
				$_DATAGRID[$row][$col]['value'] = null;
			}

			// total price
			$col++;
			$quantity = $_SHOW_APPROVED_COLUMN ? $totalQuantityApproved : $totalQuantity; 
			$totalPrice = $item['price']*$quantity;
			$_DATAGRID[$row][$col]['value'] = $totalPrice;
			$_DATAGRID[$row][$col]['numeric'] = true;
		}

		$row++;
		$_ROW_GROUP_TOTALS[$row] = true;

		$col=$_FIRST_FIXED_COL;
		$_DATAGRID[$row][$col]['caption'] = 'Total '.$group['caption'];

		$row++;
		$_ROW_SEPARATORS[$row] = true;

		$col=$_FIRST_FIXED_COL;
		$_DATAGRID[$row][$col]['separator'] = true;
	}

	$row++;
	$_ROW_SEPARATORS[$row] = true;

	// grand total
	$row++;
	$_ROW_GRAND_TOTAL = $row;
	$_DATAGRID[$row][$_FIRST_FIXED_COL]['caption'] = 'Master Sheet Totals';

	$_TOTAL_GRID_ROWS = $row-1;
}


// spreadsheet styles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_STYLES = array(

	'allborders' => array(
		'borders' => array( 
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	'number' => array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
		)
	),
	'string' => array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
		)
	),
	'total-group' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12, 
			'bold'=>true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'f3f3f3')
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	'total-sheet' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 14, 
			'bold'=>true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'dedede')
		),
		'alignment' => array(
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	'group-header' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 16, 
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		)
	),
	'table-header' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 11, 
			'bold'=>true,
			'color' => array('rgb'=>'FFFFFF')
		),
		'alignment' => array(
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'000000')
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	'sheet-caption' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 20, 
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		)
	),
	'input' => array(
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'F9F7D4')
		),
	)
);


// spreadsheet builder :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_COL_FIRST_WEEK = $_TOTAL_FIXED_COLS + 1;
$_COL_LAST_WEEK = $_COL_FIRST_WEEK + $_MASTERSHEET_TOTAL_WEEKS - 1;
$_COL_FIRST_WEEK_APPROVED_QUANTITY = $_COL_LAST_WEEK + 2;
$_COL_LAST_WEEK_APPROVED_QUANTITY = $_COL_FIRST_WEEK_APPROVED_QUANTITY + $_MASTERSHEET_TOTAL_WEEKS - 1;
$_TOTAL_SPREADSHEET_ROWS = $_TOTAL_FIXED_ROWS + $_TOTAL_GRID_ROWS;
$_TOTAL_SPREADSHEET_COLS = $_TOTAL_GRID_COLS;


if ($_TOTAL_SPREADSHEET_ROWS && $_TOTAL_SPREADSHEET_COLS) {

	// excel init
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name)->setTitle($_SHEET_CAPTION);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

	// sheet columns
	$_FIRST_COL_INDEX = spreadsheet::key($_FIRST_FIXED_COL);
	$_LAST_COL_INDEX = spreadsheet::key($_TOTAL_SPREADSHEET_COLS);

	for ($row=1; $row <= $_TOTAL_SPREADSHEET_ROWS; $row++) {
			
		for ($col=1; $col <= $_TOTAL_SPREADSHEET_COLS; $col++) {

			$_COL_INDEX = spreadsheet::key($col);
			$index = "$_COL_INDEX$row";
			$_ROW_RANGE_INDEX = $_FIRST_COL_INDEX.$row.":".$_LAST_COL_INDEX.$row;

			switch ($row) {
				
				// sheet title
				case $_FIRST_FIXED_COL;		

					if ($col==1) {
						$objPHPExcel->getActiveSheet()->mergeCells($_ROW_RANGE_INDEX);
						$objPHPExcel->getActiveSheet()->setCellValue($index, $_PAGE_TITLE);
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['sheet-caption']);
						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(50);
					}

				break;

				// filters
				case $_ROW_FILTERS:

				break;

				// group title: collection
				case $_ROW_GROUPS[$row]:

					if ($col==$_FIRST_FIXED_COL) {
						$group = $_DATAGRID[$row][$col]['group'];
						$objPHPExcel->getActiveSheet()->mergeCells($_ROW_RANGE_INDEX);
						$objPHPExcel->getActiveSheet()->setCellValue($index, $_DATAGRID[$row][$col]['caption']);
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['group-header']);
						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(40);
					}

				break;
			
				// group table header
				case $_ROW_GROUP_HEADERS[$row]:
					
					$objPHPExcel->getActiveSheet()->setCellValue($index, $_DATAGRID[$row][$col]['caption']);

					if ($_DATAGRID[$row][$col]['width']) {
						$objPHPExcel->getActiveSheet()->getColumnDimension($_COL_INDEX)->setWidth($_DATAGRID[$row][$col]['width']);
					}

					if ($col==$_TOTAL_SPREADSHEET_COLS) {
						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
						$objPHPExcel->getActiveSheet()->getStyle($_ROW_RANGE_INDEX)->applyFromArray($_STYLES['table-header'], false);
					}

					if ($_DATAGRID[$row][$col]['numeric']) {
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']);
					}

				break;

				// lanchplan items
				case $_ROW_GRIDS[$row]:

					$value = $_DATAGRID[$row][$col]['value'];

					if ($_DATAGRID[$row][$col]['numeric']) {
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']);
					}
					
					switch ($col) {

						// estimated quantity
						case $_COL_ESTIMATE_QUANTITY:

							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']+$_STYLES['input']);
							
							$_SUM_COL['estimate-quantity'][$col][$group] += $value;

						break;

						case $_COL_PLANNED_QUANTITY:

							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
							$_SUM_COL['planned-quantity'][$col][$group] += $value;

						break;

						// estimated quote
						case $_COL_ESTIMATE_QUOTE:

							$plannedQuantity  = $_DATAGRID[$row][$_COL_TOTAL_QUANTITY] ? $_DATAGRID[$row][$_COL_TOTAL_QUANTITY]['value'] : 0;
							$estimateQuantity = $_DATAGRID[$row][$col-2] ? $_DATAGRID[$row][$col-2]['value'] : 0;
							
							$qty = $plannedQuantity>0 && $estimateQuantity>0 ? ($plannedQuantity/$estimateQuantity)*100 : 0;
							
							if ($qty) {
								$qty = number_format($qty, 2, ".", "'");
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}
							
							$_SUM_COL['estimate-quote'][$col][$group] += $qty;

						break;
						
						// week quantity
						case $col >= $_COL_FIRST_WEEK && $col <= $_COL_LAST_WEEK:
							
							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
							
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']+$_STYLES['input']);
							
							$rowsum['week-planned'][$row] += $value;
							
							$_SUM_COL['week-quantity'][$col][$group] += $value;

						break;

						// total week planned quantites
						case $_COL_TOTAL_QUANTITY:

							$qty = $rowsum['week-planned'][$row];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}

							$_SUM_COL['week-quantity-total'][$col][$group] += $qty;

						break;

						// week approved quantity
						case $col >= $_COL_FIRST_WEEK_APPROVED_QUANTITY && $col <= $_COL_LAST_WEEK_APPROVED_QUANTITY:

							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']+$_STYLES['input']);
							
							$rowsum['week-approved'][$row] += $value;
							$_SUM_COL['week-approved'][$col][$group] += $value;

						break;

						// total week approved quantites
						case $_COL_TOTAL_APPROVED_QUANTITY: 

							$qty = $rowsum['week-approved'][$row];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}

							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);

							$_SUM_COL['week-approved-total'][$col][$group] += $qty;

						break;

						// total price
						case $_COL_TOTAL_PRICE:

							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
							$objPHPExcel->getActiveSheet()->getStyle($_ROW_RANGE_INDEX)->applyFromArray($_STYLES['allborders'], false);
							
							$_SUM_COL['total-price'][$col][$group] += $value;

						break;

						default:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
						break;
					}
					
				break;

				// collection subtotal
				case $_ROW_GROUP_TOTALS[$row]:

					switch ($col) {
						
						// subtotal caption
						case $_FIRST_FIXED_COL:
							$range = "$index:".spreadsheet::key($col+6,$row);
							$objPHPExcel->getActiveSheet()->mergeCells($range);
							$objPHPExcel->getActiveSheet()->setCellValue($index, $_DATAGRID[$row][$col]['caption']);
						break;
						
						// estimated quantity
						case $_COL_ESTIMATE_QUANTITY:

							$qty = $_SUM_COL['estimate-quantity'][$col][$group];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}
							
							$_SUM_COL['grand-estimate-quantity'][$col] += $qty;

						break;
						
						// planned quantity
						case $_COL_PLANNED_QUANTITY:

							$qty = $_SUM_COL['planned-quantity'][$col][$group];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}

							$_SUM_COL['grand-planned-quantity'][$col] += $qty;

						break;

						// estimated quote
						case $_COL_ESTIMATE_QUOTE:

							$plannedQuantity  = $_SUM_COL['week-quantity-total'][$_COL_TOTAL_QUANTITY][$group];
							$estimateQuantity = $_SUM_COL['estimate-quantity'][$col-2][$group];

							$qty = $plannedQuantity>0 && $estimateQuantity>0 ? ($plannedQuantity/$estimateQuantity)*100 : 0;

							if ($qty) {
								$qty = number_format($qty, 2, ".", "'");
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}
							
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']);

						break;

						// week quantity
						case $col >= $_COL_FIRST_WEEK && $col <= $_COL_LAST_WEEK:

							$qty = $_SUM_COL['week-quantity'][$col][$group];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}

							$_SUM_COL['grand-week-quantity'][$col] += $qty;

						break;

						// total week planned quantites
						case $_COL_TOTAL_QUANTITY:
							
							$qty = $_SUM_COL['week-quantity-total'][$col][$group];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}

							$_SUM_COL['grand-week-quantity-total'][$col] += $qty;

						break;

						// week approved quantity
						case $col >= $_COL_FIRST_WEEK_APPROVED_QUANTITY && $col <= $_COL_LAST_WEEK_APPROVED_QUANTITY:

							$qty = $_SUM_COL['week-approved'][$col][$group];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}
							
							$_SUM_COL['grand-week-approved'][$col] += $qty;

						break;

						// total week approved quantites
						case $_COL_TOTAL_APPROVED_QUANTITY:

							$qty = $_SUM_COL['week-approved-total'][$col][$group];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}

							$_SUM_COL['grand-week-approved-total'][$col] += $qty;

						break;

						// total price
						case $_COL_TOTAL_PRICE:

							$qty = $_SUM_COL['total-price'][$col][$group];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}

							$objPHPExcel->getActiveSheet()->getStyle($_ROW_RANGE_INDEX)->applyFromArray($_STYLES['total-group'], false);
							
							$_SUM_COL['grand-total-price'][$col] += $qty;
							
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);

						break;
					}

				break;

				case $_ROW_GRAND_TOTAL:
					
					switch ($col) {

						// grand total caption
						case $_FIRST_FIXED_COL:
							$range = "$index:".spreadsheet::key($col+6,$row);
							$objPHPExcel->getActiveSheet()->mergeCells($range);
							$objPHPExcel->getActiveSheet()->setCellValue($index, $_DATAGRID[$row][$col]['caption']);
						break;
						
						// estimated quantity
						case $_COL_ESTIMATE_QUANTITY:

							$qty = $_SUM_COL['grand-estimate-quantity'][$col];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index,  $qty);
							}

						break;

						case $_COL_PLANNED_QUANTITY:

							$qty = $_SUM_COL['grand-planned-quantity'][$col];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}

						break;

						// estimated quote
						case $_COL_ESTIMATE_QUOTE:

							$plannedQuantity  = $_SUM_COL['grand-week-quantity-total'][$_COL_TOTAL_QUANTITY];
							$estimateQuantity = $_SUM_COL['grand-estimate-quantity'][$col-2];
							
							$qty = $plannedQuantity>0 && $estimateQuantity>0 ? ($plannedQuantity/$estimateQuantity)*100 : 0;

							if ($qty) {
								$qty = number_format($qty, 2, ".", "'");
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}
							
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']);

						break;
						
						// week quantity
						case $col >= $_COL_FIRST_WEEK && $col <= $_COL_LAST_WEEK:
							
							$qty = $_SUM_COL['grand-week-quantity'][$col];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}

						break;

						// total week planned quantites
						case $_COL_TOTAL_QUANTITY:

							$qty = $_SUM_COL['grand-week-quantity-total'][$col];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}

						break;

						// week approved quantity
						case $col >= $_COL_FIRST_WEEK_APPROVED_QUANTITY && $col <= $_COL_LAST_WEEK_APPROVED_QUANTITY:
							
							$qty = $_SUM_COL['grand-week-approved'][$col];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}

						break;

						// total week approved quantites
						case $_COL_TOTAL_APPROVED_QUANTITY: 

							$qty = $_SUM_COL['grand-week-approved-total'][$col];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}

						break;

						// total price
						case $_COL_TOTAL_PRICE:

							$qty = $_SUM_COL['grand-total-price'][$col];

							if ($qty) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $qty);
							}

							$objPHPExcel->getActiveSheet()->getStyle($_ROW_RANGE_INDEX)->applyFromArray($_STYLES['total-sheet'], false);
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);

						break;
					}

				break;

				// collection separators
				case $_ROW_SEPARATORS[$row]:
					
					if ($col==1) {
						$objPHPExcel->getActiveSheet()->mergeCells($_ROW_RANGE_INDEX);
					}

				break;
			}
		}
	}	
}

if (!DEBUGGING_MODE) {

	if (!$_ERRORS) {
		
		$objPHPExcel->getActiveSheet()->setTitle($_SHEET_CAPTION);
		$objPHPExcel->setActiveSheetIndex(0);

		// redirect output to client browser
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$_FILENAME.'.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	} else {
		message::empty_result();
		url::redirect("/$application/$controller$archived/$action/$id");
	}
}
