<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

switch ($application) {
				
	case 'mps':
	
		$tableName = 'mps_mastersheets';
		$permissionEdit = user::permission('can_edit_mps_master_sheets');
	break;

	case 'lps':
	
		$tableName = 'lps_references';
		$permissionEdit = user::permission('can_edit_lps_master_sheets');
	break;
}

// get mastersheet
$mastersheet = new Modul($application);
$mastersheet->setTable($tableName);
$mastersheet->read($id);

if (!$mastersheet->id) {
	
	message::add(array(
		'keyword' => 'Master Sheet not found.', 
		'theme' => 'error'
	));
	
	url::redirect("/$appliction/$controller");
}

if ($permissionEdit) {
	
	$response = $mastersheet->delete();
		
	if ($response) {

		$model = new Model($application);

		switch ($application) {
				
			case 'mps':
			
				
				// remove all master sheet items
				$model->db->exec("
					DELETE FROM mps_mastersheet_items
					WHERE mps_mastersheet_item_mastersheet_id = $id
				");

				// remove all master sheet deliver dates
				$model->db->exec("
					DELETE FROM mps_mastersheet_delivery_dates
					WHERE mps_mastersheet_delivery_date_mastersheet_id = $id
				");

				// remove all master sheet files
				$model->db->exec("
					DELETE FROM mps_mastersheet_files
					WHERE mps_mastersheet_file_mastersheet_id = $id
				");

				// remove mastersheet file folder
				$dir = "/public/data/files/$application/mastersheets/$id";
				dir::remove($dir, true);

			break;

			case 'lps':
			
				
				// remove all master sheet items
				$model->db->exec("
					DELETE FROM lps_mastersheet_items
					WHERE lps_mastersheet_item_mastersheet_id = $id
				");

				// remove all master sheet deliver dates
				$model->db->exec("
					DELETE FROM lps_mastersheet_delivery_dates
					WHERE lps_mastersheet_delivery_date_mastersheet_id = $id
				");

				// remove all master sheet files
				$model->db->exec("
					DELETE FROM lps_mastersheet_files
					WHERE lps_mastersheet_file_mastersheet_id = $id
				");

				// remove mastersheet file folder
				$dir = "/public/data/files/$application/mastersheets/$id";
				dir::remove($dir, true);

			break;
		}

		message::request_deleted();
		url::redirect("/$application/$controller");
		
	} else {
		message::request_failure();
		url::redirect("/$application/$controller/$action/$id");
	}
}
else {
	message::access_denied();
	url::redirect("/$application/$controller");
}