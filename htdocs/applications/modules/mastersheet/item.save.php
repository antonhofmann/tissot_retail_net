<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$translate = translate::instance();
$user = user::instance();

$application = $_REQUEST['application'];
$id = $_REQUEST['mastersheet'];
$group = $_REQUEST['group'];
$subgroup = $_REQUEST['subgroup'];
$item = $_REQUEST['item'];
$checked = $_REQUEST['checked'];

switch ($application) {
				
	case 'mps':
	
		$tableName = 'mps_mastersheets';
		$tablePrefix = 'mps_';
		$permissionEdit = user::permission('can_edit_mps_master_sheets');
	break;

	case 'lps':
	
		$tableName = 'lps_mastersheets';
		$tablePrefix = 'lps_';
		$permissionEdit = user::permission('can_edit_lps_master_sheets');
	break;
}

// get mastersheet
$mastersheet = new Modul($application);
$mastersheet->setTable($tableName);
$mastersheet->read($id);


if ($mastersheet->id) {
	
	// db model
	$model = new Model($application);

	// get current master sheet items
	switch ($application) {
				
		case 'mps':
		
			$query = "
				SELECT 
					mps_mastersheet_item_material_id AS material, 
					mps_mastersheet_item_id AS id
				FROM mps_mastersheet_items
				WHERE mps_mastersheet_item_mastersheet_id = ?
			";
		break;

		case 'lps':
		
			$query = "
				SELECT 
					lps_mastersheet_item_reference_id AS reference, 
					lps_mastersheet_item_id AS id
				FROM lps_mastersheet_items
				WHERE lps_mastersheet_item_mastersheet_id = ?
			";
		break;
	}

	// current master sheet items
	$sth = $model->db->prepare($query);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();
	$currentItems = $result ? _array::extract($result) : array();

	// add items from planning type and collection category
	if ($group && $subgroup) {

		switch ($application) {
				
			case 'mps':
			
				
				$queryMaterials = "
					SELECT DISTINCT mps_material_id AS item
					FROM mps_materials
					INNER JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id
					INNER JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id
					WHERE mps_material_material_planning_type_id = ? AND mps_material_collection_category_id = ?
				";

				$queryAddMaterial = "
					INSERT INTO mps_mastersheet_items (
						mps_mastersheet_item_mastersheet_id,
						mps_mastersheet_item_material_id,
						user_created,
						date_created
					) VALUES (?, ?, ?, NOW())
				";

			break;

			case 'lps':
			
				
				$queryMaterials = "
					SELECT DISTINCT lps_reference_id AS item
					FROM lps_references
					INNER JOIN lps_collections ON lps_collection_id = lps_reference_collection_id
					INNER JOIN lps_collection_categories ON lps_collection_category_id = lps_reference_collection_category_id
					WHERE lps_collection_id = ? AND lps_collection_category_id = ?
				";

				$queryAddMaterial = "
					INSERT INTO lps_mastersheet_items (
						lps_mastersheet_item_mastersheet_id,
						lps_mastersheet_item_reference_id,
						user_created,
						date_created
					) VALUES (?, ?, ?, NOW())
				";

			break;
		}
		
		// get materials
		$sth = $model->db->prepare($queryMaterials);
		$sth->execute(array_filter(array($group, $subgroup)));
		$items = $sth->fetchAll();
		
		if ($items) {

			$sth = $model->db->prepare($queryAddMaterial);

			foreach ($items as $row) {
				
				$itm = $row['item'];
				
				if (!$currentItems[$itm]) {
					
					$response = $sth->execute(array(
						$mastersheet->id,
						$itm,
						$user->login
					));
				}
			}
			
			$message = ($response) ? $translate->message_request_inserted : null;
		}
	}
	else {

		switch ($application) {
				
			case 'mps':
			
				
				$queryInsert = "
					INSERT INTO mps_mastersheet_items (
						mps_mastersheet_item_mastersheet_id,
						mps_mastersheet_item_material_id,
						user_created,
						date_created
					) VALUES (?, ?, ?, NOW())
				";

				$queryDelete = "
					DELETE FROM mps_mastersheet_items 
					WHERE mps_mastersheet_item_mastersheet_id = ? AND mps_mastersheet_item_material_id = ?
				";

			break;

			case 'lps':
			

				$queryInsert = "
					INSERT INTO lps_mastersheet_items (
						lps_mastersheet_item_mastersheet_id,
						lps_mastersheet_item_reference_id,
						user_created,
						date_created
					) VALUES (?, ?, ?, NOW())
				";

				$queryDelete = "
					DELETE FROM lps_mastersheet_items 
					WHERE lps_mastersheet_item_mastersheet_id = ? AND lps_mastersheet_item_reference_id = ?
				";
				
			break;
		}

		// add new item
		if ($checked && !$currentItems[$item]) {

			 $sth = $model->db->prepare($queryInsert);

			 $response = $sth->execute(array(
			 	$mastersheet->id,
			 	$item,
			 	$user->login
			 ));

			$message = ($response) ? $translate->message_request_saved : null;
		}	
		
		// remove item
		if (!$checked && $currentItems[$item]) { 
			
			$sth = $model->db->prepare($queryDelete);

			$response = $sth->execute(array(
			 	$mastersheet->id,
			 	$item
			 ));
			
			$message = ($response) ? $translate->message_request_deleted : null;
		}
	}

} else {
	$response = false;
	$message = "Master Sheet not found";
}

// response with json header
header('Content-Type: text/json');
echo json_encode(array(
	'response' => $response,
	'message' => $message	
));
	