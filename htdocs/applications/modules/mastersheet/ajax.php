<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$translate = Translate::instance();

// request
$application = $_REQUEST['application'];
$model = new Model($application);

switch ($_REQUEST['section']) {
	
	case 'dropdown.items.collection.categories':

		$collection = $_REQUEST['value']; // collection

		$json[][''] = $translate->all_collection_categories;

		if ($collection) {
		
			switch ($application) {
						
				case 'mps':
				
					
					$query = "
						SELECT DISTINCT
							mps_material_collection_category_id AS id,
							mps_material_collection_category_code AS caption
						FROM mps_materials
						INNER JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id
						WHERE mps_material_active = 1 AND mps_material_material_planning_type_id = ?
						ORDER BY mps_material_collection_category_code
					";

				break;

				case 'lps':
				
					
					$query = "
						SELECT DISTINCT
							lps_collection_category_id AS id,
							lps_collection_category_code AS caption
						FROM lps_references
						INNER JOIN lps_collection_categories ON lps_collection_category_id = lps_reference_collection_category_id
						WHERE lps_reference_active = 1 AND lps_reference_collection_id = ?
						ORDER BY lps_collection_category_code
					";

				break;
			}
	
			$sth = $model->db->prepare($query);
			$sth->execute(array($collection));
			$result = $sth->fetchAll();
				
			if ($result) {
				foreach ($result as $row) {
					$json[][$row['id']] = $row['caption'];
				}
			}
		}

	break;

	case 'dropdown.items.product.groups':

		$collection = $_REQUEST['group']; 			// collection
		$collectionCategory = $_REQUEST['value']; 	// collection category

		$json[][''] = 'All Product Groups';

		if ($collection && $collectionCategory) {
		
			switch ($application) {

				case 'lps':
				
					
					$query = "
						SELECT DISTINCT
							lps_product_group_id AS id,
							lps_product_group_name AS caption
						FROM lps_references
						INNER JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id
						WHERE lps_reference_active = 1 AND lps_reference_collection_id = ? AND lps_reference_collection_category_id = ?
						ORDER BY lps_product_group_name
					";
					
				break;
			}
	
			$sth = $model->db->prepare($query);
			$sth->execute(array($collection, $collectionCategory));
			$result = $sth->fetchAll();
				
			if ($result) {
				foreach ($result as $row) {
					$json[][$row['id']] = $row['caption'];
				}
			}
		}

	break;

	case 'item.comment':

		$mastersheet = $_REQUEST['mastersheet'];
		$item = $_REQUEST['item'];
		$comment = $_REQUEST['comment'];

		if ($mastersheet && $item ) {
			
			$sth = $model->db->prepare("
				UPDATE lps_mastersheet_items SET
					lps_mastersheet_item_reference_comment = ?
				WHERE lps_mastersheet_item_mastersheet_id = ? AND lps_mastersheet_item_reference_id = ?
			");

			$json['response'] = $sth->execute(array($comment, $mastersheet, $item));

			if ($json['response']) {

				// get all master sheet launchplans not exported
				$sth = $model->db->prepare("
					SELECT lps_launchplan_id AS launchplan
					FROM lps_launchplans
					WHERE lps_launchplan_mastersheet_id = ? AND lps_launchplan_workflowstate_id NOT IN (8,9,10)
				");

				$sth->execute(array($mastersheet));
				$res = $sth->fetchAll();

				if ($res) { 

					$sth = $model->db->prepare("
						UPDATE lps_launchplan_items SET
							lps_launchplan_item_comment = ?
						WHERE lps_launchplan_item_reference_id = ? AND lps_launchplan_item_launchplan_id = ?
					");

					foreach ($res as $row) $sth->execute(array($comment, $item, $row['launchplan']));
				}
			}
		}

	break;
}



// response with json header
header('Content-Type: text/json');
echo json_encode($json);
