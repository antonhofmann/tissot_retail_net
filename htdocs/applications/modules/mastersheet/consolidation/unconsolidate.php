<?php 
/**
 * 	Master Sheet unconsolidation
 * 
 * 	This process is possible only if master sheet has not generated sales orders.
 * 	Reset Master sheet state as unconsolidated.
 * 	Reset all consolidateed order sheets as approved.
 * 	Reste all consolidated not ordered order sheet items as null value.
 * 	
 * 	@author: admir.serifi@mediaparx.ch
 * 	@copyright (c) Swatch AG
 *  @version 1.1
 * 
 */
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];
	
// db connector
$model = new Model($application);
	
	
switch ($application) {
				
	case 'mps':
	

		$MPS = true;
		$stateConsolidated = 5;
		$stateArchived = 10;

		$tableMasterSheet = 'mps_mastersheets';

		$mapMasterSheet = array(
			'mps_mastersheet_year' => 'mastersheet_year',
			'mps_mastersheet_name' => 'mastersheet_name',
			'mps_mastersheet_consolidated' => 'mastersheet_consolidated',
			'mps_mastersheet_archived' => 'mastersheet_archived'
		);


		$queryTotalMasterSheetItems = "
			SELECT COUNT(mps_ordersheet_item_id) AS total
			FROM mps_ordersheet_items
			INNER JOIN mps_ordersheets ON mps_ordersheet_item_ordersheet_id = mps_ordersheet_id
			WHERE mps_ordersheet_mastersheet_id = ?
		";

		$queryTotalMasterSheetItemsNotOrdered = "
			SELECT COUNT(mps_ordersheet_item_id) AS total
			FROM mps_ordersheet_items
			INNER JOIN mps_ordersheets ON mps_ordersheet_item_ordersheet_id = mps_ordersheet_id
			WHERE mps_ordersheet_mastersheet_id = ? AND (mps_ordersheet_item_order_date IS NULL OR mps_ordersheet_item_order_date = '')
		";

		$queryUpdateMasterSheet = "
			UPDATE mps_mastersheets SET
				mps_mastersheet_consolidated = 0,
				mps_mastersheet_archived = 0,
				user_modified = ?,
				date_modifeid = NOW()
			WHERE mps_mastersheet_id = ?
		";

		$queryUpdateOrderSheets = "
			UPDATE mps_ordersheets SET 
				mps_ordersheet_workflowstate_id = 4,
				user_modified = ?,
				date_modified = NOW()
			WHERE mps_ordersheet_mastersheet_id = ?
		";

		$queryGetAllNotOrderedItems = "
			SELECT DISTINCT mps_ordersheet_item_id AS id
			FROM mps_ordersheet_items
			INNER JOIN mps_ordersheets ON  mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
			WHERE mps_ordersheet_mastersheet_id = ?
			AND ( 
				mps_ordersheet_item_quantity_approved IS NULL 
				OR mps_ordersheet_item_quantity_approved = '' 
				OR mps_ordersheet_item_quantity_approved = 0 
			)
		";

		$queryUpdateItemNotOrdered = "
			UPDATE mps_ordersheet_items SET
				mps_ordersheet_item_order_date = NULL,
				mps_ordersheet_item_purchase_order_number = NULL,
				mps_ordersheet_item_shipto = NULL,
				mps_ordersheet_item_customernumber = NULL,
				user_modified = ?,
				date_modified = NOW()
			WHERE mps_ordersheet_item_id = ?
		";

	break;
				
	case 'lps':
	

		$LPS = true;
		$stateConsolidated = 7;
		$stateArchived = 10;

		$tableMasterSheet = 'lps_mastersheets';

		$mapMasterSheet = array(
			'lps_mastersheet_year' => 'mastersheet_year',
			'lps_mastersheet_name' => 'mastersheet_name',
			'lps_mastersheet_consolidated' => 'mastersheet_consolidated',
			'lps_mastersheet_archived' => 'mastersheet_archived',
			'lps_mastersheet_pos_date' => 'mastersheet_pos_date',
			'lps_mastersheet_phase_out_date' => 'mastersheet_phase_out_date',
			'lps_mastersheet_week_number_first' => 'mastersheet_week_number_first',
			'lps_mastersheet_weeks' => 'mastersheet_weeks',
			'lps_mastersheet_estimate_month' => 'mastersheet_estimate_month'
		);

		$queryTotalMasterSheetItems = "
			SELECT COUNT(lps_launchplan_item_id) AS total
			FROM lps_launchplan_items
			INNER JOIN lps_launchplans ON lps_launchplan_item_launchplan_id = lps_launchplan_id
			WHERE lps_launchplan_mastersheet_id = ?
		";

		$queryTotalMasterSheetItemsNotOrdered = "
			SELECT COUNT(lps_launchplan_item_id) AS total
			FROM lps_launchplan_items
			INNER JOIN lps_launchplans ON lps_launchplan_item_launchplan_id = lps_launchplan_id
			WHERE lps_launchplan_mastersheet_id = ? AND (lps_launchplan_item_order_date IS NULL OR lps_launchplan_item_order_date = '')
		";

		$queryUpdateMasterSheet = "
			UPDATE lps_mastersheets SET
				lps_mastersheet_consolidated = 0,
				lps_mastersheet_archived = 0,
				user_modified = ?,
				date_modifeid = NOW()
			WHERE lps_mastersheet_id = ?
		";

		$queryUpdateOrderSheets = "
			UPDATE lps_launchplans SET 
				lps_launchplan_workflowstate_id = 5,
				user_modified = ?,
				date_modified = NOW()
			WHERE lps_launchplan_mastersheet_id = ?
		";

		$queryGetAllNotOrderedItems = "
			SELECT DISTINCT lps_launchplan_item_id AS id
			FROM lps_launchplan_items
			INNER JOIN lps_launchplans ON  lps_launchplan_id = lps_launchplan_item_launchplan_id
			WHERE lps_launchplan_mastersheet_id = ?
			AND ( 
				lps_launchplan_item_quantity_approved IS NULL 
				OR lps_launchplan_item_quantity_approved = '' 
				OR lps_launchplan_item_quantity_approved = 0 
			)
		";

		$queryUpdateItemNotOrdered = "
			UPDATE lps_launchplan_items SET
				lps_launchplan_item_order_date = NULL,
				lps_launchplan_item_customernumber = NULL,
				user_modified = ?,
				date_modified = NOW()
			WHERE lps_launchplan_item_id = ?
		";

	break;
}

// master sheet
$mastersheet = new Modul($application);
$mastersheet->setTable($tableMasterSheet);
$mastersheet->setDataMap($mapMasterSheet);
$mastersheet->read($id);

if ($mastersheet->id) {

	// total mastersheets items
	$sth = $model->db->prepare($queryTotalMasterSheetItems);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetch();
	$totalMasterSheetItems = $result['total'];

	// total mastersheet ordered items
	$sth = $model->db->prepare($queryTotalMasterSheetItemsNotOrdered);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetch();
	$totalMasterSheetItemsNotOrdered = $result['total'];

	$unconsolidated = ($mastersheet->data['mastersheet_consolidated'] && $totalMasterSheetItems == $totalMasterSheetItemsNotOrdered) ? true : false;

} else {
	Message::add(array(
		'type' => 'error',
		'content' => 'The Master Sheet not found.'
	));
}

if ($unconsolidated) {
	
	// set mastersheet as unconsolidated
	$sth = $model->db->prepare($queryUpdateMasterSheet);
	$response = $sth->execute(array($user->login, $mastersheet->id));
	
	
	if ($response) {

		// update all order sheets
		$sth = $model->db->prepare($queryUpdateOrderSheets);
		$sth->execute(array($user->login, $mastersheet->id));

		// get all not ordered items
		$sth = $model->db->prepare($queryGetAllNotOrderedItems);
		$sth->execute(array($mastersheet->id));
		$result = $sth->fetchAll();
		
		if ($result) {
			
			$sth = $model->db->prepare($queryUpdateItemNotOrdered);
			
			foreach ($result as $row) {
				$sth->execute(array($user->login, $row['id']));
			}
		}

		Message::add(array(
			'type' => 'message',
			'keyword' => 'message_mastersheet_unconsolidate',
			'data' => array('mps_mastersheet_name' => $mastersheet->data['mastersheet_name'])
		));

	} else {

		Message::add(array(
			'type' => 'error',
			'keyword' => 'error_mastersheet_unconsolidate',
			'sticky' => true
		));
	}
}
else {
	
	message::add(array(
		'type' => 'error',
		'keyword' => 'error_mastersheet_unconsolidate',
		'sticky' => true
	));
}

url::redirect("/$application/$controller/$action/$id");
	
	