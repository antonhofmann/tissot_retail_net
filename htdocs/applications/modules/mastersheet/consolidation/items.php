<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

define('CELL_READONLY', 'true');

$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['mastersheet'];
$version = $_REQUEST['version'];
$_LAUNCHPLAN = $_REQUEST['launchplan'];

// get session stored request
$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

// model
$model = new Model($application);

// master sheet
$mastersheet = new Modul($application);
$mastersheet->setTable('lps_mastersheets');
$mastersheet->read($id);

$firstWeek = $mastersheet->data['lps_mastersheet_week_number_first'];
$totalWeeks = $mastersheet->data['lps_mastersheet_weeks'];
$estimateMonth = $mastersheet->data['lps_mastersheet_estimate_month'];

// launchplan filter
$filter = $_LAUNCHPLAN ? " AND lps_launchplan_id = $_LAUNCHPLAN" : null;

// items
$items = array();
$itemRevision = array();
	
$sth = $model->db->prepare("
	SELECT DISTINCT
		lps_launchplan_item_reference_id AS id,
		lps_launchplan_item_price AS price, 
		currency_symbol AS currency_symbol, 
		lps_launchplan_item_exchangrate AS currency_exchangrate, 
		lps_launchplan_item_factor AS currency_factor, 
		SUM(lps_launchplan_item_quantity) AS quantity,
		SUM(lps_launchplan_item_quantity_approved) AS quantity_approved,
		SUM(lps_launchplan_item_quantity_estimate) AS quantity_estimate,
		lps_reference_code AS reference_code, 
		lps_reference_name AS reference_name, 
		lps_launchplan_item_comment AS reference_description,
		lps_collection_code AS collection_code,
		lps_mastersheet_estimate_month AS estimate_month,							
		lps_product_group_id AS group_id, 
		lps_product_group_name AS group_name, 
		lps_collection_category_id AS subgroup_id,
		lps_collection_category_code AS subgroup_name
	FROM lps_launchplan_items 
	INNER JOIN lps_references ON lps_launchplan_items.lps_launchplan_item_reference_id = lps_references.lps_reference_id
	INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
	INNER JOIN lps_mastersheets ON lps_mastersheet_id = lps_launchplan_mastersheet_id
	INNER JOIN lps_product_groups ON lps_references.lps_reference_product_group_id = lps_product_groups.lps_product_group_id
	INNER JOIN lps_collections ON lps_references.lps_reference_collection_id = lps_collections.lps_collection_id
	INNER JOIN lps_collection_categories ON lps_references.lps_reference_collection_category_id = lps_collection_categories.lps_collection_category_id
	INNER JOIN db_retailnet.currencies ON currency_id = lps_launchplan_item_currency
	WHERE lps_launchplan_mastersheet_id = ? $filter
	GROUP BY lps_reference_id
	ORDER BY 
		lps_product_group_name,
		lps_collection_category_code,
		lps_collection_code,
		lps_reference_code
");

$sth->execute(array($mastersheet->id));
$result = $sth->fetchAll();
	
if ($result) {
	foreach ($result as $row) {
		
		$group = $row['group_id'];
		$item = $row['id'];	
		
		$items[$group]['caption'] = $row['group_name'];
		$items[$group]['data'][$item]['collection_code'] = $row['collection_code'];
		$items[$group]['data'][$item]['reference_code'] = $row['reference_code'];
		$items[$group]['data'][$item]['reference_name'] = $row['reference_name'];
		$items[$group]['data'][$item]['reference_description'] = $row['reference_description'];
		$items[$group]['data'][$item]['estimate_month'] = $row['estimate_month'];
		$items[$group]['data'][$item]['price'] = $row['price'];
		$items[$group]['data'][$item]['currency_symbol'] = $row['currency_symbol'];
		$items[$group]['data'][$item]['currency_exchangrate'] = $row['currency_exchangrate'];
		$items[$group]['data'][$item]['currency_factor'] = $row['currency_factor'];
		$items[$group]['data'][$item]['quantity'] = $row['quantity'];
		$items[$group]['data'][$item]['quantity_estimate'] = $row['quantity_estimate'];
		$items[$group]['data'][$item]['quantity_approved'] = $row['quantity_approved'];

		if ($row['status']) {
			$itemRevision[$item] = true;
		}
	}
}

// item week quantities
$itemWeekQuantities = array();
	
$sth = $model->db->prepare("
	SELECT DISTINCT
		lps_launchplan_item_reference_id AS item,
		lps_launchplan_item_week_quantity_week AS week,
		SUM(lps_launchplan_item_week_quantity_quantity) AS quantity,
		SUM(lps_launchplan_item_week_quantity_quantity_approved) AS quantity_approved
	FROM lps_launchplan_item_week_quantities
	INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_week_quantity_item_id
	INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
	WHERE lps_launchplan_mastersheet_id = ? $filter
	GROUP BY lps_launchplan_item_reference_id, lps_launchplan_item_week_quantity_week
");

$sth->execute(array($mastersheet->id));
$result = $sth->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$item = $row['item'];
		$week = $row['week'];
		$itemWeekQuantities[$item][$week] = array(
			'id' => $item,
			'quantity' => $row['quantity'],
			'quantity_approved' => $row['quantity_approved']
		);
	}
}

// item planned quantities
$sth = $model->db->prepare("
	SELECT 
		lps_launchplan_item_reference_id AS item,
		SUM(lps_launchplan_item_planned_quantity_quantity) AS total_quantities
	FROM lps_launchplan_item_planned_quantities
	INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_planned_quantity_item_id
	INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
	WHERE lps_launchplan_mastersheet_id = ? $filter
	GROUP BY lps_launchplan_item_reference_id
");

$sth->execute(array($mastersheet->id));
$result = $sth->fetchAll();
$itemPlannedQuantities = _array::extract($result);

// fixed rows
$fixedRows = 1;
$firstFixedRow = 1;
$lastFixedRow = 1;
$fixedRowsSeparator = 0;

// fixed columns
$fixedColumns = 10;
$firstFixedColumn = 2;
$lastFixedColumn = 10;
$fixedColumnSeparator = 0;
$totalFixedRows = $fixedRows + $fixedRowsSeparator;
$totalFixedColumns = $fixedColumns + $fixedColumnSeparator;

// datagrid
$datagrid = array();
$rowGroupSeparator = 1;

if ($items) {

	$row=1;

	foreach ($items as $g => $group) {

		$row++;
		$rowGroups[$row] = true;
		
		$col=$firstFixedColumn;
		$datagrid[$row][$col]['caption'] = $group['caption'];
		$datagrid[$row][$col]['group'] = $g;

		$row++;
		$rowGroupHeaders[$row] = true;

		$col=$firstFixedColumn;
		$datagrid[$row][$col]['caption'] = 'Collection Code';
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Reference';
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Name';
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Comment';
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Estimated Price';
		$datagrid[$row][$col]['header'] = true;
		$colPrice = $col;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Currency';
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = "Estimated for $estimateMonth months";
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Total Planned';
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = '% of Estimate';
		$datagrid[$row][$col]['header'] = true;

		for ($i=0; $i < $totalWeeks; $i++) { 
			$col++;
			$week = $firstWeek+$i;
			$datagrid[$row][$col]['caption'] = "Week $week";
			$datagrid[$row][$col]['header'] = true;
		}

		$col++;
		$datagrid[$row][$col]['caption'] = 'Total Launch';
		$datagrid[$row][$col]['header'] = true;

		// approvals
		for ($i=0; $i < $totalWeeks; $i++) { 
			$col++;
			$week = $firstWeek+$i;
			$datagrid[$row][$col]['caption'] = "Week $week";
			$datagrid[$row][$col]['header'] = true;
		}

		$col++;
		$datagrid[$row][$col]['caption'] = 'Total Approved';
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Total Cost';
		$datagrid[$row][$col]['header'] = true;

		// total datagrid columns
		$totalDataGridColumns = $col;
			
		foreach ($group['data'] as $key => $item) {

			$row++;

			if (!$hideRevisionColumn) {
				if ($isAdmin) {
					$checked = $itemRevision[$key] ? 'checked=checked' : null;
					$datagrid[$row][1]['value'] = "<input type=checkbox name=revision[$key] value=1 $checked />";
				} elseif ($inRevision && $itemRevision[$key]) {
					$datagrid[$row][1]['value'] = "<i class=\"fa fa-refresh\" ></i>";
				}
			}
			
			$col=$firstFixedColumn;
			$datagrid[$row][$col]['value'] = $item['collection_code'];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['reference_code'];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['reference_name'];			

			$col++;
			$datagrid[$row][$col]['value'] = $item['reference_description'];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['price'];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['currency_symbol'];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['quantity_estimate'];
			$datagrid[$row][$col]['item'] = $key;
			
			$col++;
			$datagrid[$row][$col]['value'] = $itemPlannedQuantities[$key];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['estimate_month'] ? ($item['total_planned']/$item['estimate_month'])*100 : null;

			for ($i=0; $i < $totalWeeks; $i++) { 
				
				$col++;
				$week = $firstWeek+$i;
				
				$datagrid[$row][$col]['item'] = $key; 
				$datagrid[$row][$col]['week'] = $week; 
				$datagrid[$row][$col]['id'] = $itemWeekQuantities[$key][$week]['id'];
				$datagrid[$row][$col]['value'] = $itemWeekQuantities[$key][$week]['quantity'];
			}

			// col total quantities
			$col++;
			$datagrid[$row][$col]['value'] = null;

			// appruvals
			for ($i=0; $i < $totalWeeks; $i++) { 
				
				$col++;
				$week = $firstWeek+$i;
				
				$datagrid[$row][$col]['item'] = $key;
				$datagrid[$row][$col]['week'] = $week;
				$datagrid[$row][$col]['id'] = $itemWeekQuantities[$key][$week]['id'];
				$datagrid[$row][$col]['value'] = $itemWeekQuantities[$key][$week]['quantity_approved'];
			}

			// col total approved quantities
			$col++;
			$datagrid[$row][$col]['value'] = null;

			// col total cost
			$col++;
			$datagrid[$row][$col]['value'] = null;
		}

		$row++;
		$rowGroupSubtotals[$row] = true;

		$col=$firstFixedColumn;
		$datagrid[$row][$col]['caption'] = 'Total '.$group['caption'];

		$row++;
		$rowGroupSeparators[$row] = true;

		$col=$firstFixedColumn;
		$datagrid[$row][$col]['separator'] = true;
	}

	// grand total
	$row++;
	$rowGrandTotal = $row;
	$datagrid[$row][2]['caption'] = 'Master Sheet Totals';

	$totalDataGridRows = $row-1;
}


// triggers
$firstWeekCol = $totalFixedColumns + 1;
$lastWeekCol = $firstWeekCol + $totalWeeks - 1;
$totalWeekCol = $lastWeekCol + 1;
$firstApprovedWeekCol = $lastWeekCol + 2;
$lastApprovedWeekCol = $firstApprovedWeekCol + $totalWeeks - 1;
$totalApprovedWeekCol = $lastApprovedWeekCol + 1;
$colRevision = 1;
$colEstimateQuantity = $firstWeekCol - 3;
$colPlanningQuantity = $firstWeekCol - 2;
$colEstimateQuote = $firstWeekCol - 1;
$colTotalCost = $totalDataGridColumns;
$firstDataGridRow = $totalFixedRows+1;
$lastDataGridRow = $firstDataGridRow + $totalDataGridRows - 1;
$firstDataGridCol = 1;
$totalRows = $totalFixedRows + $totalDataGridRows;
$totalColumns = $totalDataGridColumns;

if ($totalRows && $totalColumns) {

	for ($row=1; $row <= $totalRows; $row++) {
			
		for ($col=1; $col <= $totalColumns; $col++) {

			$index = spreadsheet::key($col, $row);

			switch ($row) {
				
				case 1;					
					$grid[$index]['html'] = "&nbsp;";
					$grid[$index]['readonly'] = CELL_READONLY;
				break;

				case $rowGroups[$row]:

					if ($col==$colRevision) {
						$grid[$index]['cls'] = "group cel-revision";
					}

					if ($col==$firstFixedColumn) {
						$caption = $datagrid[$row][$col]['caption'];
						$group = $datagrid[$row][$col]['group'];
						$grid[$index]['html'] = "<h4 data-id=$group >$caption</h4>";
						$grid[$index]['cls'] = "group";
						$mergecel[$index] = $totalColumns - 1;
					}

					$grid[$index]['readonly']  = CELL_READONLY;

				break;
				
				case $rowGroupHeaders[$row]:

					$groupFirstRow = $row+1;
					$class = $col >= $firstWeekCol ? 'week-column' : null;
					$grid[$index]['html'] = $datagrid[$row][$col]['caption'];
					$grid[$index]['cls'] = "header $class";
					$grid[$index]['readonly']  = CELL_READONLY;

				break;
				
				case $rowGroupSeparators[$row];
					$grid[$index]['cls'] = "cel-separator";
					$grid[$index]['readonly']  = CELL_READONLY;
				break;


				case $rowGroupSubtotals[$row]:

					$class = null;

					if ($col==$firstFixedColumn) {
						$grid[$index]['text'] = $datagrid[$row][$col]['caption'];
						$class  = "subtotal-caption";
						$mergecel[$index] = 6;
					}
					elseif ($col==$colEstimateQuote) {

						$celPlanned = spreadsheet::key($lastWeekCol+1,$row);
						$celEstimate = spreadsheet::key($col-2,$row);
							
						$grid[$index]['text']  = "function($celPlanned, $celEstimate) { 
							var t = $celPlanned>0 && $celEstimate>0 ? ($celPlanned/$celEstimate)*100 : 0;
							var n = t.toFixed(2);
							return n ? n+' %' : null;
						}";
					}
					elseif ($col >= $colEstimateQuantity) {
						
						$class = "subtotal-estimate-quantities";
						$range = spreadsheet::key($col,$groupFirstRow).'_'.spreadsheet::key($col,$row-1);
						
						if ($col == $colTotalCost) {
							$grid[$index]['text']  = "function($range) { 
								var t = sum($range);
								var n = t.toFixed(2);
								return n || null;
							}";
						} else {
							$grid[$index]['text']  = "function($range) { 
								var t = sum($range);
								return t || null;
							}";
						}

						$columnIndex[$col][] = $index;
					}

					$grid[$index]['cls'] = "cel-subtotal $class";
					$grid[$index]['readonly']  = CELL_READONLY;

				break;

				case $rowGrandTotal:

					$class = null;

					if ($col==$firstFixedColumn) {
						$grid[$index]['text'] = $datagrid[$row][$col]['caption'];
						$class  = "total-caption";
						$mergecel[$index] = 6;
					}
					elseif ($col==$colEstimateQuote) {

						$celPlanned = spreadsheet::key($lastWeekCol+1,$row);
						$celEstimate = spreadsheet::key($col-2,$row);
							
						$grid[$index]['text']  = "function($celPlanned, $celEstimate) { 
							var t = $celPlanned>0 && $celEstimate>0 ? ($celPlanned/$celEstimate)*100 : 0;
							var n = t.toFixed(2);
							return n ? n+' %' : null;
						}";
					}
					elseif ($col >= $colEstimateQuantity && $col <> $colEstimateQuote) {

						$indexes = $columnIndex[$col];

						if ($indexes) {

							$args = join(',',$indexes);
							$sum = join('+',$indexes);

							if ($col == $colTotalCost) {
								$grid[$index]['text']  = "function($args) { 
									var t = $sum;
									var n = t.toFixed(2);
									return n || null;
								}";
							} else {
								$grid[$index]['text']  = "function($args) { 
									var t = $sum;
									return t || null;
								}";
							}
						}
						
					}

					$grid[$index]['cls'] = "grand-total $class";
					$grid[$index]['readonly']  = CELL_READONLY;

				break;

				case $row >= $firstDataGridRow && $row <= $lastDataGridRow:

					
					$value = $datagrid[$row][$col]['value'];

					if ($col >= $firstWeekCol && $col <= $lastWeekCol) {
						
						$grid[$index]['text'] = $value;
						$grid[$index]['hiddenAsNull'] = 'true';
						$grid[$index]['numeric'] = 'true';
						
						if ($datagrid[$row][$col]['input']) {
							$grid[$index]['cls'] = $datagrid[$row][$col]['class'] ?: "cel-input";
							$item = $datagrid[$row][$col]['item'];
							$week = $datagrid[$row][$col]['week'];
							$id = $datagrid[$row][$col]['id'];
							$grid[$index]['tag'] = "week-quantity;$item;$week;$id";
						} else {
							$grid[$index]['readonly']  = CELL_READONLY;
						}
					}
					elseif($col==$totalWeekCol) {
						$range = spreadsheet::key($firstWeekCol,$row).'_'.spreadsheet::key($lastWeekCol,$row);
						$grid[$index]['text']  = "function($range) { total = sum($range); return total ? total : null; }";
						$grid[$index]['cls'] = "total-row-quantities";
					}
					elseif ($col >= $firstApprovedWeekCol && $col <= $lastApprovedWeekCol) {
						
						$grid[$index]['text'] = $value;
						$grid[$index]['hiddenAsNull'] = 'true';
						$grid[$index]['numeric'] = 'true';

						if ($datagrid[$row][$col]['input']) {
							$grid[$index]['cls'] = "cel-input";
							$item = $datagrid[$row][$col]['item'];
							$week = $datagrid[$row][$col]['week'];
							$id = $datagrid[$row][$col]['id'];
							$grid[$index]['tag'] = "week-quantity-approved;$item;$week;$id";
						} else {
							$grid[$index]['readonly']  = CELL_READONLY;
						}
					}
					elseif ($col == $totalApprovedWeekCol) {
						
						$grid[$index]['cls'] = "total-row-approved-quantities";
						$range = spreadsheet::key($firstApprovedWeekCol,$row).'_'.spreadsheet::key($lastApprovedWeekCol,$row);

						$grid[$index]['text']  = "function($range) { 
							var total = sum($range); 
							return total ? total : null; 
						}";
						
					}
					elseif ($col == $colTotalCost) {

						$celApproved = spreadsheet::key($col-1,$row);
						$celPrice = spreadsheet::key($colPrice,$row);

						$grid[$index]['text']  = "function($celApproved, $celPrice) { 
							var p = $celApproved*$celPrice;
							var n = p.toFixed(2);
							return n || null;
						}";
					}
					else {

						if (!$hideRevisionColumn && $col==$colRevision) {
							$grid[$index]['readonly']  = CELL_READONLY;
							$grid[$index]['html'] = $datagrid[$row][$col]['value'];
						}
						elseif ($col==$colEstimateQuote) {

							$celEstimateQuantity = spreadsheet::key($col-2,$row);
							$celPlannedQuantity = spreadsheet::key($lastWeekCol+1,$row);
							
							$grid[$index]['text']  = "function($celEstimateQuantity,$celPlannedQuantity) { 
								var total = ($celEstimateQuantity>0 && $celPlannedQuantity>0) ? ($celPlannedQuantity/$celEstimateQuantity)*100 : 0;
								var n = total.toFixed(2);
								return n+' %';
							}";

							$grid[$index]['cls'] = "cel-estimate-quote";

						} elseif ($col==$colEstimateQuantity) {

							
							$grid[$index]['text'] = $value;
							$grid[$index]['hiddenAsNull'] = 'true';
							$grid[$index]['numeric'] = 'true';

							if ($datagrid[$row][$col]['input']) {
								$grid[$index]['cls'] = $datagrid[$row][$col]['class'] ?: "cel-input cel-estimate-quantity";
								$item = $datagrid[$row][$col]['item'];
								$grid[$index]['tag'] = "week-quantity-estimate;$item;0;0";
							} else {
								$grid[$index]['readonly']  = CELL_READONLY;
							}

						} else {
							$grid[$index]['readonly']  = CELL_READONLY;
							$grid[$index]['text'] = $value;
							$grid[$index]['class'] = $datagrid[$row][$col];
						}
					}
					
				break;
				
				default:
					$grid[$index]['readonly']  = CELL_READONLY;
				break;

			}
		}
	}	
}

if (!headers_sent()) {
	header('Content-Type: text/json');
}

echo json_encode(array(
	'response' => true,
	'data' => $grid,
	'merge' => $mergecel,
	'top' => 1,
	'left' => 1,
	'revisionColumn' => 'a',
	'hideRevisionColumn' => true
));
