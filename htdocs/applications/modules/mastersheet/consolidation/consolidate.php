<?php 
/**
 * 	Master Sheet Consolidation
 * 
 * 	Consolidate all master sheet items.
 * 	Chack whether all order sheets are approved.
 * 	Set order sheet workflow state as consolidated
 * 	Reset all item NULL values to Zero.
 * 	
 * 	@author: admir.serifi@mediaparx.ch
 * 	@copyright (c) MediaparX AG
 * 
 */
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];
$timestamp = $_SERVER['REQUEST_TIME'];

// db application
$model = new Model($application);

switch ($application) {
				
	case 'mps':
	

		$MPS = true;
		$stateConsolidated = 5;
		$stateArchived = 10;

		$tableMasterSheet = 'mps_mastersheets';

		$mapMasterSheet = array(
			'mps_mastersheet_year' => 'mastersheet_year',
			'mps_mastersheet_name' => 'mastersheet_name',
			'mps_mastersheet_consolidated' => 'mastersheet_consolidated',
			'mps_mastersheet_archived' => 'mastersheet_archived'
		);

		$queryCountOrdersheets = "
			SELECT COUNT(mps_ordersheet_id) AS total
			FROM mps_ordersheets
			WHERE mps_ordersheet_mastersheet_id = ?	
		";

		$queryOrderSheets = "
			SELECT DISTINCT mps_ordersheet_id AS id
			FROM mps_ordersheets
			INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_ordersheet_id = mps_ordersheet_id
			WHERE mps_ordersheet_mastersheet_id = ?
			AND (
				mps_ordersheet_workflowstate_id IN (3,4,5)
				OR (
					mps_ordersheet_workflowstate_id = 2
					AND mps_ordersheet_closingdate < CURRENT_DATE
				)
				OR (
					mps_ordersheet_workflowstate_id IN (1,7)
					AND mps_ordersheet_closingdate < CURRENT_DATE
					AND ( 
						mps_ordersheet_item_quantity IS NULL 
						OR  mps_ordersheet_item_quantity = ''
						OR  mps_ordersheet_item_quantity = 0
					)
				)
			)
		";

		$queryOrderSheetItems = "
			SELECT 
				mps_ordersheet_id AS id,
				mps_ordersheet_workflowstate_id AS state,
				mps_ordersheet_item_id AS item,
				mps_ordersheet_item_quantity AS quantity,
				mps_ordersheet_item_quantity_approved AS quantity_approved,
				address_mps_customernumber AS customernumber,
				address_mps_shipto AS shipto
			FROM mps_ordersheets
			INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_ordersheet_id = mps_ordersheet_id
			INNER JOIN db_retailnet.addresses ON address_id = mps_ordersheet_address_id
			WHERE mps_ordersheet_mastersheet_id = ?
		";

		$queryGetWorkflowStates = "
			SELECT mps_workflow_state_id, mps_workflow_state_name
			FROM mps_workflow_states
		";

		$queryUpdateOrdersheetState = "
			UPDATE mps_ordersheets SET
				mps_ordersheet_workflowstate_id = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE mps_ordersheet_id = ?
		";

		$queryUpdateMasterSheet = "
			UPDATE mps_mastersheets SET
				mps_mastersheet_consolidated = 1,
				user_modified = ?,
				date_modifeid = NOW()
			WHERE mps_mastersheet_id = ?
		";

		$queryArchiveMasterSheet = "
			UPDATE mps_mastersheets SET
				mps_mastersheet_archived = 1,
				user_modified = ?,
				date_modifeid = NOW()
			WHERE mps_mastersheet_id = ?
		";

		$queryItemReset = "
			UPDATE mps_ordersheet_items SET
				mps_ordersheet_item_quantity_approved = 0,
		 		mps_ordersheet_item_desired_delivery_date =  NULL,
		 		mps_ordersheet_item_order_date = NULL,
		 		mps_ordersheet_item_purchase_order_number = ?,
		 		mps_ordersheet_item_shipto = ?,
		 		mps_ordersheet_item_customernumber = ?,
		 		user_modified = ?,
				date_modified = NOW()
		 	WHERE mps_ordersheet_item_id = ?
		";

		$queryUpdateApprovedQuantity = "
			UPDATE mps_ordersheet_items SET 
				mps_ordersheet_item_quantity_approved = ?
			WHERE mps_ordersheet_item_id = ?
		";

	break;
				
	case 'lps':
	

		$LPS = true;
		$stateConsolidated = 7;
		$stateArchived = 10;

		$tableMasterSheet = 'lps_mastersheets';

		$mapMasterSheet = array(
			'lps_mastersheet_year' => 'mastersheet_year',
			'lps_mastersheet_name' => 'mastersheet_name',
			'lps_mastersheet_consolidated' => 'mastersheet_consolidated',
			'lps_mastersheet_archived' => 'mastersheet_archived',
			'lps_mastersheet_pos_date' => 'mastersheet_pos_date',
			'lps_mastersheet_phase_out_date' => 'mastersheet_phase_out_date',
			'lps_mastersheet_week_number_first' => 'mastersheet_week_number_first',
			'lps_mastersheet_weeks' => 'mastersheet_weeks',
			'lps_mastersheet_estimate_month' => 'mastersheet_estimate_month'
		);

		$queryCountOrdersheets = "
			SELECT COUNT(lps_launchplan_id) AS total
			FROM lps_launchplans
			WHERE lps_launchplan_mastersheet_id = ?	
		";

		$queryOrderSheets = "
			SELECT DISTINCT lps_launchplan_id AS id
			FROM lps_launchplans
			INNER JOIN lps_launchplan_items ON lps_launchplan_item_launchplan_id = lps_launchplan_id
			WHERE lps_launchplan_mastersheet_id = ?
			AND (
				lps_launchplan_workflowstate_id IN (4,5,7)
				OR (
					lps_launchplan_workflowstate_id = 3
					AND lps_launchplan_closingdate < CURRENT_DATE
				)
				OR (
					lps_launchplan_workflowstate_id IN (1,2)
					AND lps_launchplan_closingdate < CURRENT_DATE
					AND ( 
						lps_launchplan_item_quantity IS NULL 
						OR  lps_launchplan_item_quantity = ''
						OR  lps_launchplan_item_quantity = 0
					)
				)
			)
		";

		$queryOrderSheetItems = "
			SELECT 
				lps_launchplan_id AS id,
				lps_launchplan_workflowstate_id AS state,
				lps_launchplan_item_id AS item,
				lps_launchplan_item_quantity AS quantity,
				lps_launchplan_item_quantity_approved AS quantity_approved,
				address_mps_customernumber AS customernumber,
				address_mps_shipto AS shipto
			FROM lps_launchplans
			INNER JOIN lps_launchplan_items ON lps_launchplan_item_launchplan_id = lps_launchplan_id
			INNER JOIN db_retailnet.addresses ON address_id = lps_launchplan_address_id
			WHERE lps_launchplan_mastersheet_id = ?
		";

		$queryGetWorkflowStates = "
			SELECT lps_workflow_state_id, lps_workflow_state_name
			FROM lps_workflow_states
		";

		$queryUpdateOrdersheetState = "
			UPDATE lps_launchplans SET
				lps_launchplan_workflowstate_id = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE lps_launchplan_id = ?
		";

		$queryUpdateMasterSheet = "
			UPDATE lps_mastersheets SET
				lps_mastersheet_consolidated = 1,
				user_modified = ?,
				date_modifeid = NOW()
			WHERE lps_mastersheet_id = ?
		";

		$queryArchiveMasterSheet = "
			UPDATE lps_mastersheets SET
				lps_mastersheet_archived = 1,
				user_modified = ?,
				date_modifeid = NOW()
			WHERE lps_mastersheet_id = ?
		";

		$queryItemReset = "
			UPDATE lps_launchplan_items SET
				lps_launchplan_item_quantity_approved = 0,
		 		lps_launchplan_item_order_date = NULL,
		 		lps_launchplan_item_customernumber = ?,
		 		user_modified = ?,
				date_modified = NOW()
		 	WHERE lps_launchplan_item_id = ?
		";

		$queryUpdateApprovedQuantity = "
			UPDATE lps_launchplan_items SET 
				lps_launchplan_item_quantity_approved = ?
			WHERE lps_launchplan_item_id = ?
		";

	break;
}

// master sheet
$mastersheet = new Modul($application);
$mastersheet->setTable($tableMasterSheet);
$mastersheet->setDataMap($mapMasterSheet);
$mastersheet->read($id);

if ($mastersheet->id) {

	// count order sheets
	$sth = $model->db->prepare($queryCountOrdersheets);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetch();
	$totalOrdersheets = $result['total'];


	// Get all approved, completed or expired order sheets
	$sth = $model->db->prepare($queryOrderSheets);
	$sth->execute(array($mastersheet->id));
	$ordersheets = $sth->fetchAll();

	// if number of conditional fetched orde sheets
	// is equal whith number of total order sheets
	// master sheet can be consolidated
	$consolidate = $ordersheets && count($ordersheets)==$totalOrdersheets ? true : false;

} else {
	Message::add(array(
		'type' => 'error',
		'content' => 'The Master Sheet not found.'
	));
}

if ($consolidate) {
	
	// controll arrays
	$ordersheetHasQuantities = array();
	$archiveOrderesheets = array();
	$consolidatedOrdersheets = array();
	$updateItems = array();
	
	$resetOrderSheetItems = array();
	$updateAprrovedQuantites = array();
	
	// workflow states
	$sth = $model->db->prepare($queryGetWorkflowStates);
	$sth->execute();
	$result = $sth->fetchAll();
	$workflowStates = _array::extract($result);
	
	// get all order sheets items
	$sth = $model->db->prepare($queryOrderSheetItems);
	$sth->execute(array($mastersheet->id));
	$result = $sth->fetchAll();

	// items analyse
	if ($result) {
		
		// set all customer/shipping numbers to all not ordered items
		foreach ($result as $row) {
			
			$ordersheet = $row['id'];
			$item = $row['item'];

			$quantity = null;
			$qtyApproved = $row['quantity_approved'];
			$qtyClient = $row['quantity'];

			if (!$qtyApproved) {
				$qtyApproved = $qtyApproved===0 || $qtyApproved==='0' ? 0 : null;
			}

			if ($qtyApproved > 0 || $qtyApproved===0) $quantity = $qtyApproved;
			// update approved quantity from client quantity
			// this should be possible only for order sheets with workflow states as not approved
			else $quantity = $qtyClient;

			// ordersheet involved in consolidation
			if ($quantity) {
				$ordersheetHasQuantities[$ordersheet] = true;
			} else {
				// reset ordersheet item data
				// for not ordered data
				$resetOrderSheetItems[$item] = array(
					'shipto' => $row['shipto'],
			 		'customernumber' => $row['customernumber']
				);
			}

			$updateAprrovedQuantites[$item] = $quantity;
		}
	}
	
	// set ordersheets new workflow states
	foreach ($ordersheets as $row) {
		
		$ordersheet = $row['id'];
		
		// set ordersheet as consolidated
		if ($ordersheetHasQuantities[$ordersheet]) $consolidatedOrdersheets[$ordersheet] = true;
		// set ordersheet as archived
		else $archiveOrderesheets[$ordersheet] = true;
	}
	
	// total consolidated and archived
	$totalConsolidatedOrdersheets = count($consolidatedOrdersheets);
	$totalArchivedOrdersheets = count($archiveOrderesheets);
	
	// consolidate master sheet
	if ($totalOrdersheets == $totalConsolidatedOrdersheets + $totalArchivedOrdersheets) {
		
		$response = false;
		

		// reset not orderer items
		if ($resetOrderSheetItems) {
			
			$sth = $model->db->prepare($queryItemReset);

			foreach ($resetOrderSheetItems as $item => $data) {
				
				if ($MPS) {
					$sth->execute(array($timestamp, $data['shipto'], $data['customernumber'], $user->login, $item));
				}

				if ($LPS) {
					$sth->execute(array($data['customernumber'], $user->login, $item ));
				}
			}
		}


		// update approved items
		if ($updateAprrovedQuantites) {

			$sth = $model->db->prepare($queryUpdateApprovedQuantity);

			foreach ($updateAprrovedQuantites as $item => $quantity) {
				$sth->execute(array($quantity, $item));
			}
		}


		// update order sheet states
		$sth = $model->db->prepare($queryUpdateOrdersheetState);

		// traking
		$tracking = new User_Tracking($application);

		$totalConsolidated = 0;
		
		foreach ($ordersheets as $row) {
			
			$ordersheet = $row['id'];

			// new order sheet state
			$state = $archiveOrderesheets[$ordersheet] ? $stateArchived : $stateConsolidated;
			
			$response = $sth->execute(array($state, $user->login, $ordersheet));
			
			if ($response) {

				$totalConsolidated++;

				$tracking->create(array(
					'user_tracking_entity' => $MPS ? 'order sheet' : 'launch plan',
					'user_tracking_entity_id' => $ordersheet,
					'user_tracking_user_id' => $user->id,
					'user_tracking_action' => $workflowStates[$state],
					'user_created' => $user->login,
					'date_created' => date('Y-m-d H:i:s')
				));
			}
		}
		
		if ($totalConsolidated==$totalOrdersheets) {
			
			// set mastersheet as consolidated
			$sth = $model->db->prepare($queryUpdateMasterSheet);
			$sth->execute(array($user->login, $mastersheet->id));
			
			// set master sheet as archived if all ordersheets are archived
			if ($totalOrdersheets == $totalArchivedOrdersheets) {
				$sth = $model->db->prepare($queryArchiveMasterSheet);
				$sth->execute(array($user->login, $mastersheet->id));
			}
		}
	} 
	else { 
		$response = false;
	}
}
else {
	$response = false;
}

if ($response) {
	Message::add(array(
		'type' => 'message',
		'keyword' => 'message_mastersheet_consolidate',
		'data' => array('mps_mastersheet_name' => $mastersheet->data['mastersheet_name'])
	));
} else {
	Message::add(array(
		'type' => 'error',
		'keyword' => 'error_mastersheet_consolidate',
		'sticky' => true
	));
}

url::redirect("/$application/$controller/$action/$id");

	