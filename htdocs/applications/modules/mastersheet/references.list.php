<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

// session request
$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

$MPS = in_array($application, array('mps')) ? true : false;
$LPS = in_array($application, array('lps')) ? true : false;

// database model
$model = new Model($application);


switch ($application) {

	case 'lps':
	

		$tableName = 'lps_mastersheets';

		$sqlMasterSheetItems = "
			SELECT DISTINCT
				lps_reference_collection_id AS group_id,
				lps_reference_collection_category_id AS subgroup_id,
				lps_reference_product_group_id AS progroup_id
			FROM lps_mastersheet_items
			INNER JOIN lps_references ON lps_reference_id = lps_mastersheet_item_reference_id
			WHERE lps_reference_product_group_id > 0 AND lps_mastersheet_item_mastersheet_id = ?
		";

	break;
}

// get mastersheet
$mastersheet = new Modul($application);
$mastersheet->setTable($tableName);
$mastersheet->read($id);

// get collection categories from master sheet items
$sth = $model->db->prepare($sqlMasterSheetItems);
$sth->execute(array($mastersheet->id));
$result = $sth->fetchAll();

if ($result) {
	
	$datagrid = array();
	$currentItems = array();

	switch ($application) {

		case 'lps':
		

			$sqlCurrentItems = "
				SELECT lps_mastersheet_item_reference_id AS item
				FROM lps_mastersheet_items
				WHERE lps_mastersheet_item_mastersheet_id = ?
			";

			$sqlItems = "
				SELECT DISTINCT
					lps_reference_id AS id,
					lps_reference_code AS code,
					lps_reference_name AS name,
					lps_reference_price_point AS price,
					lps_reference_description AS comment,
					lps_collection_code AS collection,
					lps_product_group_id AS group_id,
					lps_product_group_name AS group_name,
					lps_collection_category_id AS subgroup_id,
					lps_collection_category_code AS subgroup_name,
					currency_symbol AS currency
				FROM lps_references
				INNER JOIN lps_collections ON lps_collection_id = lps_reference_collection_id
				INNER JOIN lps_collection_categories ON lps_collection_category_id = lps_reference_collection_category_id
				INNER JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id
				INNER JOIN db_retailnet.currencies ON currency_id = lps_reference_currency_id
				WHERE lps_collection_id = ? AND lps_collection_category_id = ? AND lps_product_group_id = ?
				ORDER BY
					lps_collection_code,
					lps_collection_category_code,
					lps_reference_code
			";

		break;
	}
	
	// current master sheet items
	$sth = $model->db->prepare($sqlCurrentItems);
	$sth->execute(array($mastersheet->id));
	$res = $sth->fetchAll();
	
	if ($res) {
		foreach ($res as $row) $currentItems[] = $row['item'];
	}

	// get references
	$sth = $model->db->prepare($sqlItems);

	foreach ($result as $row) {
		
		$sth->execute(array($row['group_id'],$row['subgroup_id'],$row['progroup_id']));
		$items = $sth->fetchAll();

		if ($items) {
			
			foreach ($items as $item) {
				
				$group = $item['group_id'];
				$subgroup = $item['subgroup_id'];
				$key = $item['id'];	
				
				$datagrid[$group]['caption'] = $item['group_name'];
				$datagrid[$group]['subgroup'][$subgroup]['caption'] = $item['subgroup_name'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['collection'] = $item['collection'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['code'] = $item['code'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['name'] = $item['name'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['price'] = $item['price'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['comment'] = $item['comment'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['currency'] = $item['currency'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['setof'] = $item['setof'];
				
				if ($_REQUEST['editabled']) {
					
					// checkboxes
					$checked = (in_array($key, $currentItems)) ? 'checked=checked' : null;
					$box = "<input type=checkbox name=material[$key] class=material value=$key $checked />";
					$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['box'] = $box;
					if ($checked) $checkall[$group][$subgroup] = $checkall[$group][$subgroup]+1;
				} 
				elseif (!$archived) {
					$box = (in_array($key, $currentItems)) ? ui::icon('checked') : null;
					$datagrid[$group]['subgroup'][$subgroup]['items'][$key]['box'] = $box;
				}
			}
		}
	}
}

if ($datagrid) {

	foreach ($datagrid as $group => $data) {
			
		$list  .= "<h5>{$data[caption]}</h5>";

		foreach ($data['subgroup'] as $subgroup => $items) {

			$delivery = null;
			$checkbox = null;
			
			// checkbox: check all
			if ($_REQUEST['editabled']) {
				$checked = ($checkall[$group][$subgroup] == count($items['items'])) ? 'checked=checked' : null;
				$checkbox = "<input type=checkbox name=checkall[$subgroup] data=table_$group$subgroup class=checkall value=1 $checked />";
			}

			$list  .= "<h6>{$items[caption]} $delivery</h6>";

			$table = new Table(array(
				'id' => 'table_'.$group.$subgroup		
			));
			
			$table->datagrid = $items['items'];
			
			$table->collection(
				Table::ATTRIBUTE_NOWRAP, 
				'width=10%',
				'caption=Collection'
			);
			
			$caption =  $MPS ? $translate->mps_material_code : $translate->reference_code;

			$table->code(
				Table::ATTRIBUTE_NOWRAP, 
				'width=15%',
				"caption=$caption"
			);
			
			$caption = $MPS ? $translate->mps_material_name : $translate->reference_name;

			$table->name(
				"caption=$caption"
			);
			
			if ($MPS) {
				$table->setof(
					Table::ATTRIBUTE_NOWRAP,
					Table::ATTRIBUTE_ALIGN_RIGHT, 
					'width=5%',
					'caption=Set of'
				);
			}
			
			$caption = $MPS ? $translate->mps_material_price : $translate->reference_price_point;

			$table->price(
				Table::ATTRIBUTE_NOWRAP, 
				Table::ATTRIBUTE_ALIGN_RIGHT, 
				'width=5%',
				"caption=$caption" 
			);
			
			$table->currency(
				Table::ATTRIBUTE_NOWRAP, 
				'width=20px',
				'caption=Currency'
			);
			
			if (_array::key_exists('box', $items['items'])) {
				
				$table->caption('box', $checkbox);
			
				$table->box(
					Table::ATTRIBUTE_NOWRAP, 
					'width=1%'
				);
			}
			
			$list .= $table->render();
		}
	}
}
else  {
	$list = html::emptybox($translate->empty_result);
}


echo $list;
	