<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['mastersheet'];
$version = $_REQUEST['version'];

switch ($application) {
				
	case 'mps':
	

		$tableMasterSheet = 'mps_mastersheets';
		$permissionEdit = user::permission('can_edit_mps_master_sheets');

		$queryList = "
			DELETE FROM mps_mastersheet_splitting_lists
			WHERE mps_mastersheet_splitting_list_id = ?
		";

		$queryItem = "
			DELETE FROM mps_mastersheet_splitting_list_items
			WHERE mps_mastersheet_splitting_list_item_splittinglist_id = ?
		";

	break;

	case 'lps':
	

		$tableMasterSheet = 'lps_mastersheets';
		$permissionEdit = user::permission('can_edit_lps_master_sheets');

		$queryList = "
			DELETE FROM lps_mastersheet_detail_lists
			WHERE lps_mastersheet_detail_list_id = ?
		";

		$queryItem = "
			DELETE FROM lps_mastersheet_detail_list_items
			WHERE lps_mastersheet_detail_list_item_list_id = ?
		";

	break;
}

// master sheet
$mastersheet = new Modul($application);
$mastersheet->setTable($tableMasterSheet);
$mastersheet->read($id);

if ($mastersheet->id && $permissionEdit) {

	$model = new Model($application);
	
	// remove splitting list
	$sth = $model->db->prepare($queryList);
	$response = $sth->execute(array($version));
	
	if ($response) {
		
		// remove all splitting list items
		$sth = $model->db->prepare($queryItem);
		$sth->execute(array($version));
		
		message::request_deleted();
		url::redirect("/$application/$controller/$action/$id");
		
	} else {
		message::request_failure();
		url::redirect("/$application/$controller/$action/$id/$version");
	}
	
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}
