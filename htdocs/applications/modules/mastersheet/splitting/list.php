<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$search = $_REQUEST['search'];
$id = $_REQUEST['id'];

$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'date';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "desc";
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rows = $settings->limit_pager_rows;
$offset = ($page-1) * $rows;

$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

$model = new Model($application);

switch ($application) {
				
	case 'mps':
	

		$stateConsolidated = 5;
		$stateArchived = 10;

		$tableMasterSheet = 'mps_mastersheets';
		$tableSplitingList = 'mps_mastersheet_splitting_lists';

		$mapMasterSheet = array(
			'mps_mastersheet_year' => 'mastersheet_year',
			'mps_mastersheet_name' => 'mastersheet_name',
			'mps_mastersheet_consolidated' => 'mastersheet_consolidated',
			'mps_mastersheet_archived' => 'mastersheet_archived'
		);

		$query = "
			SELECT SQL_CALC_FOUND_ROWS DISTINCT
				mps_mastersheet_splitting_list_id AS id,
				mps_mastersheet_splitting_list_name AS name,
				DATE_FORMAT(date_created, '%d.%m.%Y') AS date
			FROM mps_mastersheet_splitting_lists
			WHERE mps_mastersheet_splitting_list_mastersheet_id = ?
			ORDER BY ? ?
		";

	break;

	case 'lps':
	


		$tableMasterSheet = 'lps_mastersheets';
		$tableSplitingList = 'lps_mastersheet_detail_lists';

		$mapMasterSheet = array(
			'lps_mastersheet_year' => 'mastersheet_year',
			'lps_mastersheet_name' => 'mastersheet_name',
			'lps_mastersheet_consolidated' => 'mastersheet_consolidated',
			'lps_mastersheet_archived' => 'mastersheet_archived',
			'lps_mastersheet_pos_date' => 'mastersheet_pos_date',
			'lps_mastersheet_phase_out_date' => 'mastersheet_phase_out_date',
			'lps_mastersheet_week_number_first' => 'mastersheet_week_number_first',
			'lps_mastersheet_weeks' => 'mastersheet_weeks',
			'lps_mastersheet_estimate_month' => 'mastersheet_estimate_month'
		);

		$query = "
			SELECT SQL_CALC_FOUND_ROWS DISTINCT
				lps_mastersheet_detail_list_id AS id,
				lps_mastersheet_detail_list_name AS name,
				DATE_FORMAT(date_created, '%d.%m.%Y') AS date
			FROM lps_mastersheet_detail_lists
			WHERE lps_mastersheet_detail_list_mastersheet_id = ?
			ORDER BY ? ?
		";

	break;
}

$sth = $model->db->prepare($query);
$sth->execute(array($id,$sort,$direction));
$result = $sth->fetchAll();

if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);

	$pager = new Pager(array(
		'page' => $page,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";

// toolbox: form
if ($toolbox) {
	$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
}

$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

$table->datagrid = $datagrid;

$table->name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	"href=".$_REQUEST['form'],
	'caption=Version Name'
);

$table->date(
	Table::ATTRIBUTE_NOWRAP,
	'width=10%'
);

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;

	