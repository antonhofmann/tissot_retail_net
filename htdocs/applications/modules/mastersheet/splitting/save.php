<?php 
/**
 * 	Master Sheet Consolidated Version
 * 
 * 	Save all master sheet items as consolidated splitting list.
 * 	
 * 	@author: admir.serifi@mediaparx.ch
 * 	@copyright (c) Mediaparx Ag
 *  @version 1.1
 */

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];
$title = $_REQUEST['title'];

// db application
$model = new Model($application);

switch ($application) {
				
	case 'mps':
	

		$stateConsolidated = 5;
		$stateArchived = 10;

		$tableMasterSheet = 'mps_mastersheets';

		$mapMasterSheet = array(
			'mps_mastersheet_year' => 'mastersheet_year',
			'mps_mastersheet_name' => 'mastersheet_name',
			'mps_mastersheet_consolidated' => 'mastersheet_consolidated',
			'mps_mastersheet_archived' => 'mastersheet_archived'
		);

		$queryCreateSplittingList = "
			INSERT INTO mps_mastersheet_splitting_lists (
				mps_mastersheet_splitting_list_name,
				mps_mastersheet_splitting_list_mastersheet_id,
				user_created,
				date_created
			)
			VALUES (?, ?, ?, NOW())
		";

		$queryGetMasterSheetItems = "
			SELECT DISTINCT
				mps_ordersheet_item_id AS id,
				mps_ordersheet_item_material_id AS reference, 
				mps_ordersheet_item_price AS price,
				SUM(mps_ordersheet_item_quantity) AS quantity,
				SUM(mps_ordersheet_item_quantity_approved) AS quantity_approved,
				mps_ordersheet_item_currency AS currency,
				mps_ordersheet_item_exchangrate AS exchangrate,
				mps_ordersheet_item_factor AS factor
			FROM mps_ordersheet_items 
			INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
			WHERE mps_ordersheet_mastersheet_id = ?
			GROUP BY mps_ordersheet_item_material_id
		";

		$queryCreateSplittingListItem = "
			INSERT INTO mps_mastersheet_splitting_list_items (
				mps_mastersheet_splitting_list_item_splittinglist_id,
				mps_mastersheet_splitting_list_item_material_id,
				mps_mastersheet_splitting_list_item_price,
				mps_mastersheet_splitting_list_item_total_quantity,
				mps_mastersheet_splitting_list_item_total_quantity_approved,
				mps_mastersheet_splitting_list_item_currency,
				mps_mastersheet_splitting_list_item_exchangrate,
				mps_mastersheet_splitting_list_item_factor,
				user_created,
				date_created
			)
			VALUES (?,?,?,?,?,?,?,?,?,NOW())
		";

	break;

	case 'lps':
	

		$stateConsolidated = 7;
		$stateArchived = 10;

		$tableMasterSheet = 'lps_mastersheets';

		$mapMasterSheet = array(
			'lps_mastersheet_year' => 'mastersheet_year',
			'lps_mastersheet_name' => 'mastersheet_name',
			'lps_mastersheet_consolidated' => 'mastersheet_consolidated',
			'lps_mastersheet_archived' => 'mastersheet_archived',
			'lps_mastersheet_pos_date' => 'mastersheet_pos_date',
			'lps_mastersheet_phase_out_date' => 'mastersheet_phase_out_date',
			'lps_mastersheet_week_number_first' => 'mastersheet_week_number_first',
			'lps_mastersheet_weeks' => 'mastersheet_weeks',
			'lps_mastersheet_estimate_month' => 'mastersheet_estimate_month'
		);

		$queryCreateSplittingList = "
			INSERT INTO lps_mastersheet_detail_lists (
				lps_mastersheet_detail_list_name,
				lps_mastersheet_detail_list_mastersheet_id,
				user_created,
				date_created
			)
			VALUES (?, ?, ?, NOW())
		";

		$queryGetMasterSheetItems = "
			SELECT DISTINCT
				lps_launchplan_item_id AS id,
				lps_launchplan_item_reference_id AS reference, 
				lps_launchplan_item_price AS price,
				SUM(lps_launchplan_item_quantity) AS quantity,
				SUM(lps_launchplan_item_quantity_approved) AS quantity_approved,
				SUM(lps_launchplan_item_quantity_estimate) AS quantity_estimate,
				lps_launchplan_item_currency AS currency,
				lps_launchplan_item_exchangrate AS exchangrate,
				lps_launchplan_item_factor AS factor
			FROM lps_launchplan_items 
			INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
			WHERE lps_launchplan_mastersheet_id = ?
			GROUP BY lps_launchplan_item_reference_id
		";

		$queryCreateSplittingListItem = "
			INSERT INTO lps_mastersheet_detail_list_items (
				lps_mastersheet_detail_list_item_list_id,
				lps_mastersheet_detail_list_item_reference_id,
				lps_mastersheet_detail_list_item_price,
				lps_mastersheet_detail_list_item_total_quantity,
				lps_mastersheet_detail_list_item_total_quantity_approved,
				lps_mastersheet_detail_list_item_total_quantity_estimate,
				lps_mastersheet_detail_list_item_currency,
				lps_mastersheet_detail_list_item_exchangrate,
				lps_mastersheet_detail_list_item_factor,
				user_created,
				date_created
			)
			VALUES (?,?,?,?,?,?,?,?,?,?,NOW())
		";

	break;
}

// master sheet
$mastersheet = new Modul($application);
$mastersheet->setTable($tableMasterSheet);
$mastersheet->setDataMap($mapMasterSheet);
$mastersheet->read($id);

if ($mastersheet->id && $title) {
	
	// create splitting list
	$sth = $model->db->prepare($queryCreateSplittingList);
	$response = $sth->execute(array($title, $mastersheet->id, $user->login));
	$inserted = $model->db->lastInsertId();
	
	if ($response && $inserted) {

		$message = $translate->message_request_inserted;

		// get master sheet items
		$sth = $model->db->prepare($queryGetMasterSheetItems);
		$sth->execute(array($mastersheet->id));
		$result = $sth->fetchAll();
		
		if ($result) {

			$sth = $model->db->prepare($queryCreateSplittingListItem);
			
			foreach ($result as $row) {

				switch ($application) {
				
					case 'mps':
					
						$sth->execute(array(
							$inserted,
							$row['reference'],
							$row['price'],
							$row['quantity'],
							$row['quantity_approved'],
							$row['currency'],
							$row['exchangrate'],
							$row['factor'],
							$user->login
						));
					break;

					case 'lps':
					
						$sth->execute(array(
							$inserted,
							$row['reference'],
							$row['price'],
							$row['quantity'],
							$row['quantity_approved'],
							$row['quantity_estimate'],
							$row['currency'],
							$row['exchangrate'],
							$row['factor'],
							$user->login
						));
					break;
				}
			}
		}

	} else {
		$message = $translate->error_request;
	}
}
else {
	$message = $translate->error_request;
}


// response with json header
header('Content-Type: text/json');
echo json_encode(array(
	'response' => $response,
	'message' => $message		
));

	