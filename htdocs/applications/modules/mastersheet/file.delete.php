<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

switch ($application) {
	
	case 'mps':
	
		$table = 'mps_mastersheet_files';
		$path = 'mps_mastersheet_file_path';
		$mastersheet = 'mps_mastersheet_file_mastersheet_id';
		$permissionEdit = user::permission('can_edit_mps_master_sheets');
	break;

	case 'lps':
	
		$table = 'lps_mastersheet_files';
		$path = 'lps_mastersheet_file_path';
		$mastersheet = 'lps_mastersheet_file_mastersheet_id';
		$permissionEdit = user::permission('can_edit_lps_master_sheets');
	break;
}

$file = new Modul($application);
$file->setTable($table);
$file->read($id);

if ($file->id && $permissionEdit) {
	
	$path = $file->data[$path];
	$mastersheet = $file->data[$mastersheet];
	$delete = $file->delete();
	
	if ($delete) {
		file::remove($path);
		Message::request_deleted();
		url::redirect("/$application/$controller/$action/$mastersheet");
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$mastersheet/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}