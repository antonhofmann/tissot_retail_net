<?php

	class Iteminformation {

		public $data;

		protected $file;

		protected $material;

		protected $item;

		public function __construct() {
			$this->model = new Iteminformation_Model(Connector::DB_CORE);
		}

		public function __get($key) {
			return $this->data["item_information_$key"];
		}

		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['item_information_id'];
			return $this->data;
		}

		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}

		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}

		/**
		 * Get supplier company
		 * @return string
		 */
		public function getSupplierCompany() {
			if ($this->id) {
				$supplier = $this->model->getSupplier($this->id);
				return $supplier['address_company'];
			}
		}

		

		public function item() {
			
			if (!$this->item) {
				$this->item = new Iteminformation_Item();
			}

			if ($this->id && $this->id <> $this->item->iteminformation) {
				$this->item->setIteminformation($this->id);
			}

			return $this->item;
		}
	}