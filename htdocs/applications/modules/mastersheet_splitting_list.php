<?php

	/**
	 * Master Sheet Splitting List
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Mastersheet_Splitting_List {
		
		/**
		 * List ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Master Sheet ID
		 * @var integer
		 */
		public $mastersheet;
		
		/**
		 * List Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Splitting List Item
		 * @var object
		 */
		public $item;
		
		/** 
		 * Database Model
		 * @var object
		 */
		protected $model;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * DB join master sheets
		 * @var string
		 */
		const DB_BIND_MASTERSHEETS = 'INNER JOIN mps_mastersheets ON mps_mastersheet_id = mps_mastersheet_splitting_list_mastersheet_id';
		
		/**
		 * DB join order sheets
		 * @var string
		 */
		const DB_BIND_ORDERSHEET = 'INNER JOIN mps_ordersheets ON mps_ordersheet_mastersheet_id = mps_mastersheet_splitting_list_mastersheet_id';

		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Mastersheet_Splitting_List_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_mastersheet_splitting_list_$key"];
		}
		
		public function item() {
			
			if (!$this->item) {
				$this->item = new Mastersheet_Splitting_List_Item($this->connector);
			}
			
			$this->item->splittinglist = $this->id;
			return $this->item;
		}
		
		public function read($id) { 
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_mastersheet_splitting_list_id'];
			$this->mastersheet = $this->data['mps_mastersheet_splitting_list_mastersheet_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public function deleteAll() {
			if ($this->mastersheet) {
				return $this->model->deleteAll($this->mastersheet);
			}
		}
		
		public function load($filters=null) {
			if ($this->mastersheet) {
				$result = $this->model->load($this->mastersheet, $filters);
				return ($result) ? _array::datagrid($result) : null;
			}
		}
	}
	