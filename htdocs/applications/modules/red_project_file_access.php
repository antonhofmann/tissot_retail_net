<?php

	class Red_Project_File_Access {
		
		public $id;
		public $file;
		public $partner;

		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new Red_Project_File_Access_Model($connector);
		}
		
		public function file($file) {
			$this->file = $file;
		}

		public function read($partner) {
			
			if ($this->file) {
				
				$id = $this->model->read($this->file, $partner);
				
				if ($id) {
					$this->partner = $partner;
					return $this->id = $id;
				}
			}
		}

		public function create($partner) {
			
			if ($this->file) {
				
				$id =  $this->model->create($this->file, $partner);
				
				if ($id) {
					$this->partner = $partner;
					return $this->id = $id;
				}
			}
		}

		public function delete($partner) {
			
			if ($this->file) {
				
				$delete = $this->model->delete($this->file, $partner);
				
				if ($delete) {
					$this->id = null;
					$this->partner = null;
					return true;
				}
			}
		}
		
		public function deleteAll() {
			
			if ($this->file) {
				
				$delete = $this->model->deleteAll($this->file);
				
				if ($delete) {
					$this->id = null;
					$this->partner = null;
					return true;
				}
			}
		}
	}
