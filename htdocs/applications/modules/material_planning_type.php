<?php

	/**
	 * Material Planning Type
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Material_Planning_Type {
		
		/**
		 * Planning Type ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Planning Type Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Material
		 * @var object
		 */
		public $material;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @var object
		 */
		protected $model;
		
		/**
		 * Edit all planning types
		 * @var string
		 */
		const PERMISSION_EDIT = 'can_edit_mps_planning_types';
		
		/**
		 * View all planning types
		 * @var string
		 */
		const PERMISSION_VIEW = 'can_view_mps_planning_types';
		
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Material_Planning_Type_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_material_planning_type_$key"];
		}
		
		public function material($material=null) {
			
			if (!$this->material) {
				$this->material = new Material($this->connector);
			}
			
			if ($material) {
				$this->material->read($material);
			}
			
			return $this->material;
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_material_planning_type_id'];
			return $this->data;
		}
		
		public function getFromName($name) {
			$this->data = $this->model->getFromName($name);
			$this->id = $this->data['mps_material_planning_type_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($connector, $filters=null) {
			$model = new Material_Planning_Type_Model($connector);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
	}