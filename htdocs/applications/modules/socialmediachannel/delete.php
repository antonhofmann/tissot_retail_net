<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$socialmediachannel = new Modul($application);

// set table name
$socialmediachannel->setTable('socialmediachannels');

$permission = user::permission('can_administrate_system_data');


$socialmediachannel->read($id);

if ($socialmediachannel->id && $permission) {
	
	$delete = $socialmediachannel->delete();
	
	if ($delete) {
		Message::request_deleted();
		url::redirect("/$application/$controller");
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}