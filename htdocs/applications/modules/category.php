<?php 

	class Category {
		
		const DB_BIND_PRODUCT_LINES = "INNER JOIN db_retailnet.product_lines ON product_line_id = category_product_line";

		public $data;

		public function __construct() {
			$this->model = new Category_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["category_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['category_id'];
			return $this->data;
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
	}
?>