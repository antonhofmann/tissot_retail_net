<?php 

	/**
	 * Order Sheet Statments
	 * 
	 * checking tool for singel and group workflow states
	 * 
	 * @author admir.serifi@mediaparx.ch
	 * @copyright mediaparx ag
	 * @version 1.0
	 */
	class Ordersheet_State {
		
		/**
		 * Order Sheet Data
		 * @var int
		 */
		public $ordersheet;
		
		/** Order Sheet Open
		* @var boolean
		*/
		public $open;
		
		/**
		 * Order Sheet Close
		 * @var boolean
		 */
		public $close;
		
		/**
		 * Order Sheet Expired
		 * @var boolean
		 */
		public $expired;
		
		/**
		 * Order Sheet Manager
		 * @var boolean
		 */
		public $manager;
		
		/**
		 * Order Sheet Administrator
		 * @var boolean
		 */
		public $administrator;
		
		/**
		 * Order Sheet Owner
		 * @var boolean
		 */
		public $owner;
		
	
		public function __construct() {
			$this->build();
		}
		
		public function __get($key) {
			return $this->ordersheet["mps_ordersheet_$key"];
		}
		
		public function build() {
			
			// permissions 
			$permission_view = user::permission(Ordersheet::PERMISSION_VIEW);
			$permission_view_limited = user::permission(Ordersheet::PERMISSION_VIEW_LIMITED);
			$permission_edit = user::permission(Ordersheet::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Ordersheet::PERMISSION_EDIT_LIMITED);
			$permission_manage = user::permission(Ordersheet::PERMISSION_MANAGE);
			
			// timestamp for today
			$today = strtotime(date('Y-m-d'));
				
			// check ordersheet opening date
			$this->open = ( $today >= date::timestamp($this->openingdate) ) ? true : false;
				
			// check ordersheet closing date
			$this->close = ( $today <= date::timestamp($this->closingdate)) ? false : true;
				
			// check is ordersheet date range expired
			$this->expired = ($this->open && $this->close) ? true : false;
				
			// order sheet administrator
			$this->administrator = ($permission_manage || $permission_edit) ? true : false;
			$this->canEdit = $this->administrator;
				
			// order sheet manager
			$this->manager = ($permission_manage) ? true : false;
			
			if (!$this->administrator && $permission_edit_limited) {

				// order sheet owner
				$company = new Company();
				$company->read($this->address_id);
				$owner = $company->owner();

				$this->owner = $company->owner();
				
				if (!$this->owner) {
					$this->owner = $company->canEdit();
				}

				$this->canEdit = $this->owner;
			}
		}
		
		public function isPreparation() {
			return ($this->workflowstate_id==Workflow_State::STATE_PREPARATION) ? true : false;
		}
		
		public function isOpen() {
			return ($this->open) ? true : false;
		}
		
		public function isExpired() {
			return ($this->close) ? true : false;
		}
		
		public function isCompleted() {
			return ($this->workflowstate_id==Workflow_State::STATE_COMPLETED) ? true : false;
		}
		
		public function isApproved() {
			return ($this->workflowstate_id==Workflow_State::STATE_APPROVED) ? true : false;
		}
		
		public function isRevision() {
			return ($this->workflowstate_id==Workflow_State::STATE_REVISION) ? true : false;
		}
		
		public function isConsolidated() {
			return ($this->workflowstate_id==Workflow_State::STATE_CONSOLIDATED) ? true : false;
		}
		
		public function isSalesOrderSubmitted() {
			return ($this->workflowstate_id==Workflow_State::STATE_SALES_ORDER_SUBITTED) ? true : false;
		}
		
		public function isOrderConfirmed() {
			return ($this->workflowstate_id==Workflow_State::STATE_ORDER_CONFIRMED) ? true : false;
		}
		
		public function isShipped() {
			return ($this->workflowstate_id==Workflow_State::STATE_SHIPPED) ? true : false;
		}
		
		public function inDistribution() {
			return ($this->workflowstate_id==Workflow_State::STATE_IN_DISTRIBUTION) ? true : false;
		}
		
		public function isPartiallyDistributed() {
			return ($this->workflowstate_id==Workflow_State::STATE_PARTIALLY_DISTRIBUTED) ? true : false;
		}
		
		public function isPartiallyShipped() {
			return ($this->workflowstate_id==Workflow_State::STATE_PARTIALLY_SHIPPED) ? true : false;
		}
		
		public function isInDistribution() {
			return ($this->workflowstate_id==Workflow_State::STATE_IN_DISTRIBUTION) ? true : false;
		}
		
		public function isDistributed() {
			return ($this->workflowstate_id==Workflow_State::STATE_DISTRIBUTED || $this->workflowstate_id==Workflow_State::STATE_ARCHIVED) ? true : false;
		}
		
		public function isManuallyArchived() {
			return ($this->workflowstate_id==Workflow_State::STATE_MANUALLY_ARCHIVED) ? true : false;
		}
		
		/**
		 * Order Sheet cab be deleted
		 * @return boolena
		 */
		public function canDelete() {
			return ($this->manager && $this->workflowstate_id==Workflow_State::STATE_PREPARATION) ? true : false;
		}
		
		/**
		 * Order Sheet can be submited
		 * @return boolean
		 */
		public function canSubmit() {
			$states = self::loader('complete');
			return ($this->owner && in_array($this->workflowstate_id , $states)) ? true : false;
		}
		
		/**
		 * Order Sheet can be approved
		 * @return boolean
		 */
		public function canApprov() {
			$states = self::loader('approve');
			return ($this->canAdministrate() && in_array($this->id, $states)) ? true : false;
		}

		/**
		 * User can administrate order sheet
		 * @return boolean
		 */
		public function canAdministrate() {
			return ($this->manager || $this->administrator) ? true : false;
		}
		
		/**
		 * Order Sheet is in distribution mode <br />
		 * Permitted workflow states: [preparation] [open] [progress] [completed] [approved] [revision]
		 * @return boolean
		 */
		public function onPreparation() {
			$states = self::loader('preparation');
			return (in_array($this->workflowstate_id , $states)) ? true : false;
		}
		
		/**
		 * Order Sheet cab be completed <br />
		 * Permitted workflow states: [open] [progress] [revision]
		 * @return boolean
		 */
		public function onCompleting() {
			$states = self::loader('complete');
			return (in_array($this->workflowstate_id , $states)) ? true : false;
		}
		
		/**
		 * Order Sheet cab be completed <br />
		 * Permitted workflow states: [open] [progress] [revision]
		 * @return boolean
		 */
		public function onApproving() {
			$states = self::loader('approve');
			return (in_array($this->workflowstate_id , $states)) ? true : false;
		}
		
		/**
		 * Order Sheet is in distribution mode <br />
		 * Permitted workflow states: [sales order subitted] [order confirmed] [in distribution]
		 * @return boolean
		 */
		public function onConfirmation() {
			$states = self::loader('confirmation');
			return (in_array($this->workflowstate_id , $states)) ? true : false;
		}
		
		/**
		 * Order Sheet is in distribution mode <br />
		 * Permitted workflow states: [shipped] [in distribution]
		 * @return boolean
		 */
		public function onDistribution() {
			$states = self::loader('distribution');
			return (in_array($this->workflowstate_id , $states)) ? true : false;
		}
		
		/**
		 * Group Statments
		 * @param string $group <br />
		 * 	[preparation]  when a order sheet is in preparation mode <br />
		 * 	[complete] when a order sheet can be submitted<br /> 
		 * 	[approve] when a order sheet can be approved <br />
		 * 	[revision] when a order sheet can be send in revision <br />
		 * 	[confirmation] order sheet is on confirmation by externe system <br />
		 * 	[distribution] when a order sheet items can be distributed <br />
		 * 	[delete] when a order sheet can be deleted
		 * @return array indexed workflow states
		 */
		public static function loader($group) {
			
			switch ($group) {
				
				case 'preparation':
					return array(
						Workflow_State::STATE_PREPARATION,
						Workflow_State::STATE_OPEN,
						Workflow_State::STATE_PROGRESS,
						Workflow_State::STATE_COMPLETED,
						Workflow_State::STATE_APPROVED,
						Workflow_State::STATE_REVISION
					);
				break;
				
				case 'complete':
					return array(
						Workflow_State::STATE_OPEN,
						Workflow_State::STATE_PROGRESS,
						Workflow_State::STATE_REVISION
					);
				break;
				
				case 'approve':
					return array(
						Workflow_State::STATE_PREPARATION,
						Workflow_State::STATE_OPEN,
						Workflow_State::STATE_PROGRESS,
						Workflow_State::STATE_COMPLETED,
						Workflow_State::STATE_REVISION
					);
				break;
				
				case 'revision':
					return array(
						Workflow_State::STATE_COMPLETED,
						Workflow_State::STATE_APPROVED
					);
				break;
				
				case 'confirmation':
					return array(
						Workflow_State::STATE_CONSOLIDATED,
						Workflow_State::STATE_SALES_ORDER_SUBITTED,
						Workflow_State::STATE_ORDER_CONFIRMED
					);
				break;
				
				case 'distribution':
					return array(
						Workflow_State::STATE_SHIPPED,
						Workflow_State::STATE_IN_DISTRIBUTION,
						Workflow_State::STATE_PARTIALLY_SHIPPED,
						Workflow_State::STATE_PARTIALLY_DISTRIBUTED,
						Workflow_State::STATE_MANUALLY_ARCHIVED
					);
				break;
				
				case 'delete':
					return array(
						Workflow_State::STATE_PREPARATION,
						Workflow_State::STATE_OPEN,
						Workflow_State::STATE_PROGRESS,
						Workflow_State::STATE_REVISION
					);
				break;
			}
		}
	}