<?php

	class Red_Comment_Category {

		public $data;

		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new Red_Comment_Category_Model($connector);
		}

		public function __get($key) {
			return $this->data["red_commentcategory_$key"];
		}

		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $id;
			return $this->data;
		}

		public function create($data) {
			return $this->model->create($data);
		}

		public function update($data) {
			if ($this->id) {
				return $this->model->update($this->id, $data);
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}

		public function owner() {
			$result = $this->model->owner($this->id);
			return ($result) ? true : false;
		}
		
		public static function dropdown_loader($connector, $filters=null) {
			$model = new Red_Comment_Category_Model($connector);
			$result = $model->dropdown_loader($filters);
			return _array::extract($result);
		}
	}
