<?php

	/**
	 * Master Sheet Splitting List Item
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Mastersheet_Splitting_List_Item {
		
		/**
		 * Item ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Splitting list ID
		 * @var integer
		 */
		public $splittinglist;
		
		/**
		 * Item Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Item Data
		 * @var array
		 */
		public $item;
		
		/**
		 * Database Model
		 * @var object
		 */
		public $model;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * DB join splitting lists
		 */
		const DB_BIND_MASTERSHEET_SPLITTING_LISTS = 'INNER JOIN mps_mastersheet_splitting_lists ON mps_mastersheet_splitting_list_id = mps_mastersheet_splitting_list_item_splittinglist_id';
		
		/**
		 * DB join materials
		 * @var unknown
		 */
		const DB_BIND_MATERIALS = 'INNER JOIN mps_materials ON mps_material_id = mps_mastersheet_splitting_list_item_material_id';
		
		
		public function __construct($connector=null) { 
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Mastersheet_Splitting_List_Item_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_mastersheet_splitting_list_item_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_mastersheet_splitting_list_item_id'];
			$this->splittinglist = $this->data['mps_mastersheet_splitting_list_item_splittinglist_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) { 
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}	
		
		public function deleteAll() {
			if ($this->splittinglist) {
				return $this->model->deleteAll($this->splittinglist);
			}
		}	
		
		public function load($filters=null) {
			if ($this->splittinglist) {
				return $this->model->deleteAll($this->splittinglist, $filters);
			}
		}	
	}
	