<?php 

	class Mastersheet_Delivery_Date {
		
		
		/**
		 * Master Sheet Delivery Date
		 * @var integer
		 */
		public $id;
		
		/**
		 * Master Sheet
		 * @var integer
		 */
		public $mastersheet;
		
		/**
		 * Material Planning Type
		 * @var integer
		 */
		public $planning_type;
		
		/**
		 * Material Collection Category
		 * @var integer
		 */
		public $collection_category;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @return Mastersheet_Delivery_Date_Model
		 */
		protected $model;

		/**
		 * DB Join Master Sheets
		 * @var string
		 */
		const DB_BIND_MASTERSHEETS = 'INNER JOIN mps_mastersheets ON mps_mastersheet_id = mps_mastersheet_delivery_date_mastersheet_id';
		
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Mastersheet_Delivery_Date_Model($this->connector);
		}
		
		public function mastersheet($id) {
			$this->mastersheet = $id;
		}
		
		public function read($planning, $category) {
			
			if ($this->mastersheet) {
				
				$data =  $this->model->read($this->mastersheet, $planning, $category);
				
				if ($data) {
					$this->planning_type = $data['mps_mastersheet_delivery_date_material_planning_type_id'];
					$this->collection_category = $data['mps_mastersheet_delivery_date_material_collection_category_id'];
					return $data;
				}
			}
		}
		
		public function create($planning, $category, $date) {
			if ($this->mastersheet && $date) {
				return $this->id = $this->model->create($this->mastersheet, $planning, $category, $date);
			}
		}
		
		public function update($planning, $category, $date) {
			if ($this->mastersheet && $date) {
				return $this->id = $this->model->update($this->mastersheet, $planning, $category, $date);
			}
		}
		
		public function delete($planning, $category) {
			if ($this->mastersheet) {
				return $this->model->delete($this->mastersheet, $planning, $category);
			}
		}
		
		public function deleteAll() {
			if ($this->mastersheet) {
				return $this->model->deleteAll($this->mastersheet);
			}
		}
		
		/**
		 * Master Sheet Delivery Dates Loader
		 * @param integer $mastersheet, master sheet id
		 * @param string $connector, database connector
		 * @return NULL
		 */
		public function load($filters=null) {
			if ($this->mastersheet) {
				$result = $this->model->load($this->mastersheet, $filters);
				return ($result) ? _array::datagrid($result) : null;
			}
		}
	}