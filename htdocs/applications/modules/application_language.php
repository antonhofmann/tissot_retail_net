<?php 

	/**
	 * Application Language
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Application_Language {
		
		/**
		 * ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Language
		 * @var integer
		 */
		public $language;
		
		/**
		 * Application
		 * application_language_application
		 * @var integer
		 */
		public $application;
		
		/**
		 * Database Model
		 * @var object
		 */
		protected $model;
		
		/**
		 * DB bind Languages
		 * @var unknown
		 */
		const DB_BIND_LANGUAGES = 'INNER JOIN db_retailnet.languages ON language_id = application_language_language';
		
		
		public function __construct() {
			$this->model = new Application_Language_Model(Connector::DB_CORE);
		}
		
		public function id($id) {
			$this->id = $id;
		}
		
		public function read($language) {
			if ($this->id) {
				return $this->model->read($this->id, $language);
			}
		}
		
		public function create($language) {
			if ($this->id) {
				return $this->model->create($this->id, $language);
			}
		}
		
		public function delete($language) {
			if ($this->id) {
				return $this->model->delete($this->id, $language);
			}
		}
		
		public function read_all_languages() {
			if ($this->id) {
				$result = $this->model->read_all_languages($this->id);
				return _array::extract($result);
			}
		}
	}