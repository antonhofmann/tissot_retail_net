<?php

	class Red_Icon  {

		public $data;

		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new Red_Icon_Model($connector);
		}

		public function __get($name) {
			return $this->data['red_icon_'.$name];
		}

		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $id;
			return $this->data;
		}

	}
