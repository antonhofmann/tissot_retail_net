<?php 

	class Pos_Order {
		
		const DB_BIND_PROJECTS = "INNER JOIN db_retailnet.projects ON project_order = posorder_order";
		const DB_BIND_ORDERS = "INNER JOIN db_retailnet.orders ON order_id = posorder_order";
		const DB_BIND_PRODUCT_LINES = "INNER JOIN db_retailnet.product_lines ON product_line_id = posorder_product_line";
		const DB_BIND_PROJECT_KINDS = "INNER JOIN db_retailnet.projectkinds ON projectkind_id = posorder_project_kind";
		
	}