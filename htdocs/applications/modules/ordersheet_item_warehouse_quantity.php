<?php

	class Ordersheet_Item_Warehouse_Quantity {
		
		/**
		 * Item ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Order Sheet ID
		 * @var integer
		 */
		public $ordersheet;
		
		/**
		 * Order Sheet Item ID
		 * @var integer
		 */
		public $item;
		
		/**
		 * Item Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Database Model
		 * @var object
		 */
		protected $model;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Join Order Sheet Items
		 * @var string
		 */
		const DB_BIND_ORDERSHEET_ITEMS = 'INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_warehouse_quantity_ordersheet_item_id';
	
		/**
		 * Join Order Sheets
		 * @var string
		 */
		const DB_BIND_ORDERSHEETS = 'INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_warehouse_quantity_ordersheet_id';
		
		/**
		 * Join POS Addresses
		 * @var unknown
		 */
		const DB_BIND_WAREHOUSE = 'INNER JOIN db_retailnet.address_warehouses ON address_warehouse_id = mps_ordersheet_item_warehouse_quantity_warehouse_id';
		
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Ordersheet_Item_Warehouse_Quantity_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_ordersheet_item_warehouse_quantity_$key"];
		}
		
		/**
		 * Define Order Sheet
		 * @param object $ordersheet
		 */
		public function setOrdersheet($ordersheet) {
			$this->ordersheet = $ordersheet;
		}
		
		/**
		 * Define Order Sheet Item
		 * @param object $item
		 */
		public function setItem($item) {
			$this->item = $item;
		}
		
		/**
		 * Get Item Planned Quantity
		 * @param integer $id, quantity identificator
		 * @return array quantity
		 */
		public function read($id) {
			
			$data = $this->model->read($id);
			
			if ($data) {
				$this->data = $data;
				$this->id = $data['mps_ordersheet_item_warehouse_quantity_id'];
				$this->ordersheet = $data['mps_ordersheet_item_warehouse_quantity_ordersheet_id'];
				$this->item = $data['mps_ordersheet_item_warehouse_quantity_ordersheet_item_id'];
				return $data;
			}
		}
		
		/**
		 * Load all Item Planned Quantity from POS ID
		 * @param integer $id, pos identificator
		 * @return array quantities
		 */
		public function read_from_warehouse($id) {
			
			$data = $this->model->read_from_warehouse($this->ordersheet, $this->item, $id);
			
			if ($data) {
				$this->data = $data;
				$this->id = $data['mps_ordersheet_item_warehouse_quantity_id'];
				$this->ordersheet = $data['mps_ordersheet_item_warehouse_quantity_ordersheet_id'];
				$this->item = $data['mps_ordersheet_item_warehouse_quantity_ordersheet_item_id'];
				return $data;
			}
		}
		
		/**
		 * Add New Quantity
		 * @param integer $quantity
		 * @return integer inserted ID
		 */
		public function create_planned($pos, $quantity) {
			if ($this->ordersheet && $this->item) {
				$id = $this->model->create_planned($this->ordersheet, $this->item, $pos, $quantity);
				$this->read($id);
				return $id;
			}
		}
		
		/**
		 * Add New Quantity
		 * @param integer $quantity
		 * @return integer inserted ID
		 */
		public function create_delivered($pos, $quantity) {
			if ($this->ordersheet && $this->item) {
				$id = $this->model->create_delivered($this->ordersheet, $this->item, $pos, $quantity);
				$this->read($id);
				return $id;
			}
		}
		
		/**
		 * Update Item Quantity
		 * @param integer $quantity
		 * @return boolean, true on success
		 */
		public function update_planned($quantity) {
			if ($this->id) {
				$result = $this->model->update_planned($this->id, $quantity);
				$this->read($this->id);
				return $result;
			}
		}
		
		/**
		 * Update Item Quantity
		 * @param integer $quantity
		 * @return boolean, true on success
		 */
		public function update_delivered($quantity) {
			if ($this->id) {
				$result = $this->model->update_delivered($this->id, $quantity);
				$this->read($this->id);
				return $result;
			}
		}

		/**
		 * Remove Item Quantity
		 * @return boolean, true on success
		 */
		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		/**
		 * Remove all item quantites created for $pos
		 * @param integer $pos, pos identificator
		 * @ return boolena, true on success
		 */
		public function delete_from_warehouse($id) {
			if ($this->ordersheet && $this->item) {
				return $this->model->delete_from_warehouse($this->ordersheet, $this->item, $id);
			}
		}
		
		/**
		 * Remove all item quantites
		 * @param integer $item, item identificator
		 * @return bollean, true on success
		 */
		public function delete_from_item() {
			if ($this->ordersheet && $this->item) {
				return $this->model->delete_from_item($this->ordersheet, $this->item);
			}
		}
		
		/**
		 * Remove all Order sheet Item quantites
		 * @return boolean, true on success
		 */
		public function delete_all() {
			if ($this->ordersheet) {
				return $this->model->delete_all($this->ordersheet);
			}
		}
		
		/**
		 * Load all item quantites
		 * @param array $filters, query filters
		 * @return array quantites
		 */
		public function load($filters=null) { 
			if ($this->ordersheet && $this->item) { 
				return $this->model->load($this->ordersheet, $this->item, $filters);
			}
		}
	}
	