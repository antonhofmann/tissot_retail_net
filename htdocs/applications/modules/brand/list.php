<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

// sql order
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'brand_name';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;
	
$model = new Model($application);

// filter: full text search
if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
	
	$search = $_REQUEST['search'];
	
	$filters['search'] = "(
		brand_name LIKE '%$search%' 
		OR brand_short_description LIKE '%$search%'
	)";
}

$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			brand_id,
			brand_name,
			language_name
		FROM brands
		LEFT JOIN languages on language_id = brand_language_id  
	")
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();

if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);
	
	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// toolbox: add
if ($_REQUEST['add'])  {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();

if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

$table->datagrid = $datagrid;
$table->dataloader($dataloader);

$table->brand_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	"href=".$_REQUEST['form']
);

$table->language_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);


$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;
