<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['brand_id'];


	$upload_path = "/uberall/logos/";

	$data = array();

	$brand = new Modul($application);

	// set table name
	$brand->setTable('brands');
	
	if ($_REQUEST['brand_name']) {
		$data['brand_name'] = $_REQUEST['brand_name'];
	}

	if (isset($_REQUEST['brand_language_id'])) {
		$data['brand_language_id'] = $_REQUEST['brand_language_id'];
	}

	if (in_array('brand_short_description', $fields)) {
		$data['brand_short_description'] = $_REQUEST['brand_short_description'];
	}

	if (in_array('brand_long_desicrption', $fields)) {
		$data['brand_long_desicrption'] = $_REQUEST['brand_long_desicrption'];
	}

	if (in_array('brand_keywords', $fields)) {
		$data['brand_keywords'] = $_REQUEST['brand_keywords'];
	}

	if (in_array('brand_uberal_category_ids', $fields)) {
		$data['brand_uberal_category_ids'] = $_REQUEST['brand_uberal_category_ids'];
	}

	if (in_array('brand_video_url', $fields)) {
		$data['brand_video_url'] = $_REQUEST['brand_video_url'];
	}

	if (in_array('brand_video_description', $fields)) {
		$data['brand_video_description'] = $_REQUEST['brand_video_description'];
	}

	if ($_REQUEST['has_upload'] && $_REQUEST['brand_logo']) {
		$path = upload::move($_REQUEST['brand_logo'], $upload_path);
		$data['brand_logo'] = $path;
	}
	

	if ($data) {

		$brand->read($id);

		if ($brand->id) {
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			$response = $brand->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $brand->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}



	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));