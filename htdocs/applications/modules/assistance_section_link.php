<?php 

	class Assistance_Section_Link {
		
		/**
		 * Link ID
		 * 
		 * @var int
		 */
		public $id;

		/**
		 * Section ID
		 * 
		 * @var int
		 */
		protected $section;
		
		/**
		 * Dataloader
		 * 
		 * @var string
		 */
		public $link;
	
		
		public function __construct() {
			
			$this->model = new Assistance_Section_Link_Model(Connector::DB_CORE);
		}
		
		public function setSection($section) {
			
			$this->section = $section;
		}
		
		public function read($link) {
			
			if ($this->section) {
				
				$this->link = $link;
				
				$result =  $this->model->read($this->section, $link);
				
				return $this->id = $result['assistance_section_link_id'];
			}
		}
		
		public function create($link) {
			
			if ($this->section) { 
				
				$this->link = $link;
				
				return $this->id = $this->model->create($this->section, $link);
			}
		}

		public function delete($link) {
			
			if ($this->section) {
				
				$this->link = null;
				
				return $this->model->delete($this->section, $link);
			}
		}
	}
	