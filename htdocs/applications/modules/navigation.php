<?php

	class Navigation {
		
		/**
		 * Navigation ID
		 * @var integer
		 */
		public $id;

		/**
		 * Navigation Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Database Model
		 * @var unknown
		 */
		protected $model;
		
		/**
		 * Current navigation childs
		 * @var array
		 */
		public $childs;
		
		/**
		 * Current navigation parents
		 * @var array
		 */
		public $parents;
		
		/**
		 * Temporary dataloader
		 * @var array
		 */
		private $dataloader;
		
		const DB_BIND_ROLES = 'INNER JOIN role_navigations ON role_navigation_navigation = navigation_id';
		const DB_BIND_LANGUAGES = 'INNER JOIN navigation_languages ON navigation_language_navigation = navigation_id';
		
		
		/**
		 * Build Navigation object
		 */
		public function __construct() { 
			$this->model = new Navigation_Model(Connector::DB_CORE);
		}
		
		public function __get($keyword) {
			return $this->data["navigation_$keyword"];
		}

		/**
		 * Load navigation data from navigation ID
		 * @param integer $id
		 * @return array navigation data 2D
		 */
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['navigation_id'];
			return $this->data;
		}
		
		/**
		 * Load navigations data from request <br />
		 * @param string $request, URL
		 * @param boolean $limeted, not mandatory, get only navigations permitted for currens user 
		 * @return array navigations data
		 */
		public function read_from_request($request, $limited=false) {
			$this->data = $this->model->read_from_request($request, $limited);
			$this->id = $this->data['navigation_id'];
			return $this->data;
		}
		
		/**
		 * Add new navigation
		 * @param array $data
		 * @return inserted ID
		 */
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}

		/**
		 * Update navigation data
		 * @param array $data
		 * @return boolean
		 */
		public function update($data) {
			if ($this->id && is_array($data)) {
				return $this->model->update($this->id, $data);
			}
		}
		
		/**
		 * Remove navigation data
		 * @return boolean
		 */
		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		/**
		 * Get all navigation childs
		 * @param string $id
		 * @return multitype:
		 */
		public function get_childs($id=null) {
			
			if ($this->id) {
				
				$id = ($id) ? $id : $this->id;
			
				if (!$this->dataloader) {
					$navigations = self::load();
					$this->dataloader = self::buildTree($navigations);
				}
				
				if ($this->dataloader[$id]) {
					
					foreach ($this->dataloader[$id] as $parent => $row) {
						
						$this->childs[] = $row['id'];
						
						if ($this->dataloader[$row['id']]) {
							$this->get_childs($row['id']);
						}
					}
				}
				
				return $this->childs;
			}
		}
		
		/**
		 * Load navigations data from request <br />
		 * @param array $filters, fields names
		 * @param string $limeted, get only navigations permitted for curren user 
		 * @return array navigations data
		 */
		public static function load($filters=null, $limited=false) {
			$model = new Navigation_Model(Connector::DB_CORE);
			return $model->load($filters,$limited);
		}
		
		/**
		 * Load navigations data from parent <br />
		 * @param integer $parent, navigation parent id
		 * @param boolean $limted, not mandatory, get only navigations permitted for current user 
		 * @return array navigations data
		 */
		public static function load_from_parent($parent, $limited=false) {
			$model = new Navigation_Model(Connector::DB_CORE);
			return $model->load_from_parent($parent, $limited);
		}
		
		/**
		 * Check request URL
		 * @param string $request
		 * @return boolean
		 */
		public static function check_request($request, $access=false) {
			$model = new Navigation_Model(Connector::DB_CORE);
			return $model->check_request($request, $access);
		}
		
		/**
		 * Get user permitted applications
		 * @param string $request
		 * @return boolean
		 */
		public static function get_user_applications($user=null) {
			$model = new Navigation_Model(Connector::DB_CORE);
			return $model->get_user_applications($user);
		}
		
		/**
		 * Build navigations tree
		 * @param array $navigations
		 * @return array, navigation tree dependet from navigation parent
		 */
		public static function buildTree($navigations) {
			
			if (is_array($navigations)) {
				
				$data = array();
				
				foreach ($navigations as $row) {
					
					$id = $row['navigation_id'];
					$parent = $row['navigation_parent'];
					$url = $row['navigation_url'];
					$caption = $row['navigation_language_name'];
					$tab = $row['navigation_tab'];
					$order = $row['navigation_order'];
					$active = $row['navigation_active'];
					
					$data[$parent][$id]['id'] = $id;
					$data[$parent][$id]['parent'] = $parent;
					$data[$parent][$id]['url'] = $url;
					$data[$parent][$id]['caption'] = $caption;
					$data[$parent][$id]['tab'] = $tab;
					$data[$parent][$id]['order'] = $order;
					$data[$parent][$id]['active'] = $active;
					$data[$parent][$id]['public'] = $row['navigation_active'];
				}
				
				return $data;
			}
		}
	
		public function access($user=null) {

			if ($this->id) {
				$user = ($user) ? $user : session::get('user_id');
				$result = $this->model->access($this->id, $user);
				return ($result) ? true : false;
			}
		}

		public function getParents($id) {

			$nav = new self();
			$res = $nav->read($id);

			$this->parents[] = $res['navigation_id'];

			if ($res['navigation_parent']) {
				$this->getParents($res['navigation_parent']);
			}

			return $this->parents;
		}

		public function getFirstChild($onlyActive) {

			if (!$this->id) return;

			$result = $this->load_from_parent($this->id, $onlyActive);

			if ($result) {
				return array_shift($result);
			}
		}
		
	}