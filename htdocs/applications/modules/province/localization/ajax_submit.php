<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	if($_REQUEST['province_id'] > 0 and $_REQUEST['language_id'])
	{
		
		$model = new Model();
		$model_store_locator = new Model(CONNECTOR::DB_RETAILNET_STORE_LOCATOR);


		//get province data
		$sth = $model->db->prepare("select province_id, province_country, province_canton, province_region, 
		    province_adminregion, province_shortcut, country_store_locator_id 
			from provinces
			left join countries on country_id = province_country 
			WHERE province_id = ?");
		$response = $sth->execute(array($_REQUEST['province_id']));

		if($response == true)
		{
			$province_data = $sth->fetch();

		
			//check if province is in store locator db
			$sth = $model_store_locator->db->prepare("select count(province_id) as num_recs from provinces
			WHERE province_id = ?");
			$response = $sth->execute(array($province_data['province_id']));
			
			if($response == true)
			{
				$row = $sth->fetch();

				if($row['num_recs'] == 0)
				{
					$query = "Insert into provinces (province_id, province_country_id, province_canton, province_region, province_adminregion, province_shortcut) VALUES (?,?,?,?,?,?)";

					$sth = $model_store_locator->db->prepare($query);
					$response = $sth->execute(array($province_data['province_id'], $province_data['country_store_locator_id'], $province_data['province_canton'], $province_data['province_region'], $province_data['province_adminregion'], $province_data['province_shortcut']));
				}
			}

			
			//check if translation is present in store locator
			$sth = $model_store_locator->db->prepare("select count(loc_province_id) as num_recs from loc_provinces
				    WHERE loc_province_province_id = ? and loc_province_language_id = ?");
			$response = $sth->execute(array($_REQUEST['province_id'], $_REQUEST['language_id']));
			if($response == true)
			{
				$row = $sth->fetch();
				if($row['num_recs'] == 0)
				{
					if(array_key_exists('province_canton', $_REQUEST))
					{
						$query = "Insert into loc_provinces (loc_province_country_id, loc_province_language_id, loc_province_province_id, loc_province_canton) VALUES (?,?,?,?)";

						
						$sth = $model_store_locator->db->prepare($query);
						$response = $sth->execute(array($province_data['country_store_locator_id'], $_REQUEST['language_id'], $province_data['province_id'], $_REQUEST['province_canton']));

					}
					elseif(array_key_exists('province_region', $_REQUEST))
					{
						$query = "Insert into loc_provinces (loc_province_country_id, loc_province_language_id, loc_province_province_id, loc_province_region) VALUES (?,?,?,?)";

						
						$sth = $model_store_locator->db->prepare($query);
						$response = $sth->execute(array($province_data['country_store_locator_id'], $_REQUEST['language_id'], $province_data['province_id'], $_REQUEST['province_region']));
					}
					elseif(array_key_exists('province_shortcut', $_REQUEST))
					{
						$query = "Insert into loc_provinces (loc_province_country_id, loc_province_language_id, loc_province_province_id, loc_province_shortcut) VALUES (?,?,?,?)";

						
						$sth = $model_store_locator->db->prepare($query);
						$response = $sth->execute(array($province_data['country_store_locator_id'], $_REQUEST['language_id'], $province_data['province_id'], $_REQUEST['province_shortcut']));
					}
				}
				else
				{
					
					if(array_key_exists('province_canton', $_REQUEST))
					{
						$query = "UPDATE loc_provinces SET 
							loc_province_canton = ? 
						WHERE loc_province_province_id = ? and loc_province_language_id = ?";
						
						//update store locator
						$sth = $model_store_locator->db->prepare($query);
						$response = $sth->execute(array($_REQUEST['province_canton'], $_REQUEST['province_id'], $_REQUEST['language_id']));
						
					}
					elseif(array_key_exists('province_region', $_REQUEST))
					{
						$query = "UPDATE loc_provinces SET 
							loc_province_region = ? 
						WHERE loc_province_province_id = ? and loc_province_language_id = ?";
						
						
						//update store locator
						$sth = $model_store_locator->db->prepare($query);
						$response = $sth->execute(array($_REQUEST['province_region'], $_REQUEST['province_id'], $_REQUEST['language_id']));
					}
					elseif(array_key_exists('province_shortcut', $_REQUEST))
					{
						$query = "UPDATE loc_provinces SET 
							loc_province_shortcut = ? 
						WHERE loc_province_province_id = ? and loc_province_language_id = ?";
						
						
						//update store locator
						$sth = $model_store_locator->db->prepare($query);
						$response = $sth->execute(array($_REQUEST['province_shortcut'], $_REQUEST['province_id'], $_REQUEST['language_id']));
					}

				}
			}
		}
	}

?>