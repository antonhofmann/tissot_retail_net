<?php

	class Ordersheet_Item_Planned {
		
		/**
		 * Item ID
		 * @var integer
		 */
		public $id;
				
		/**
		 * Order Sheet Item ID
		 * @var integer
		 */
		public $ordersheet;
		
		/**
		 * Order Sheet Item ID
		 * @var integer
		 */
		public $item;
		
		/**
		 * Item Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Database Model
		 * @return Ordersheet_Item_Planned_Model
		 */
		protected $model;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Join Order Sheet Items
		 * @var string
		 */
		const DB_BIND_ORDERSHEET_ITEMS = 'INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_planned_ordersheet_item_id';
			
		/**
		 * Join POS Addresses
		 * @var unknown
		 */
		const DB_BIND_POSADDRESSES = 'INNER JOIN db_retailnet.posaddresses ON posaddress_id = mps_ordersheet_item_planned_posaddress_id';
		
		const DB_LEFT_ORDERSHEET_WAREHOUSES = 'LEFT JOIN mps_ordersheet_warehouses ON mps_ordersheet_warehouse_id = mps_ordersheet_item_planned_warehouse_id';
		
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Ordersheet_Item_Planned_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_ordersheet_item_planned_$key"];
		}
		
		/**
		 * Define Order Sheet Item
		 * @param object $item
		 */
		public function setItem($item) {
			$this->item = $item;
		}
		
		/**
		 * Get Item Planned Quantity
		 * @param integer $id, quantity identificator
		 * @return array quantity
		 */
		public function read($id) {
			
			$data = $this->model->read($id);
			
			if ($data) {
				$this->data = $data;
				$this->id = $data['mps_ordersheet_item_planned_id'];
				$this->item = $data['mps_ordersheet_item_planned_ordersheet_item_id'];
				return $data;
			}
		}
		
		/**
		 * Add New POS Quantity
		 * @param integer $quantity
		 * @return integer inserted ID
		 */
		public function add_pos($pos, $quantity) {
			if ($this->item) {
				$id = $this->model->add_pos($this->item, $pos, $quantity);
				$this->read($id);
				return $id;
			}
		}
		
		/**
		 * Add New Warehouse Quantity
		 * @param integer $warehouse
		 * @return integer inserted ID
		 */
		public function add_warehouse($warehouse, $quantity) {
			if ($this->item) {
				$id = $this->model->add_warehouse($this->item, $warehouse, $quantity);
				$this->read($id);
				return $id;
			}
		}
		
		/**
		 * Update Warehouse Item Quantity
		 * @param integer $quantity
		 * @return boolean, true on success
		 */
		public function update($quantity) {
			if ($this->id) {
				$result = $this->model->update($this->id, $quantity);
				$this->read($this->id);
				return $result;
			}
		}

		/**
		 * Remove Item Quantity
		 * @return boolean, true on success
		 */
		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		/**
		 * Remove all Order sheet Item quantites
		 * @return boolean, true on success
		 */
		public function delete_all() {
			if ($this->item) {
				return $this->model->delete_all($this->item);
			}
		}
		
		/**
		 * Load all item quantites
		 * @param array $filters, query filters
		 * @return array quantites
		 */
		public function load($filters=null) { 
			if ($this->item) { 
				return $this->model->load($this->item, $filters);
			}
		}
		
		/**
		 * SUM all planned quantities
		 * @param boolean $sum_stock_reserve, sum stock reserve 
		 * @return integer
		 */
		public function sumQuantities() {
			if ($this->item) { 
				$result = $this->getStockReserve(); 
				$reserve = $result['mps_ordersheet_item_planned_id'];
				$total = $this->model->sumQuantites($this->item, $reserve); 
				return ($total) ? $total : 0;
			}
		}
		
		/**
		 * Get Stock reserve data
		 * @return array
		 */
		public function getStockReserve() {
			if ($this->item) {
				return $this->model->getStockReserve($this->item); 
			}
		}
		
		/**
		 * Update stock reserve
		 * @param unknown $quantity
		 * @return boolean,
		 */
		public function setStockReserve($quantity) {
			
			if ($this->ordersheet && $this->item) {
				
				$result = $this->getStockReserve();
				$data = $this->read($result['mps_ordersheet_item_planned_id']);
				
				if ($data['mps_ordersheet_item_planned_id']) {
					return $this->update($quantity);
				} else {
					
					$warehouse = new Ordersheet_Warehouse($this->connector);
					$warehouse->ordersheet = $this->ordersheet;
					
					// get order sheet
					$data = $warehouse->getStockReserve();
					
					return $this->add_warehouse($data['mps_ordersheet_warehouse_id'], $quantity);
				}
			}
		}
	}
	