<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

// request vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

// db application model
$model = new Model($application);

// order sheet deletable statments
$_DELETABLE_STATES = array(1,2);

// ordersheet
$launchplan = new Modul($application);
$launchplan->setTable('lps_launchplans');
$launchplan->read($id);

if ($launchplan->id) {

	// get ordersheet versions
	$getOrderSheetVersions = $model->db->prepare("
		SELECT GROUP_CONCAT(lps_launchplan_version_id) as versions
		FROM lps_launchplan_versions
		WHERE lps_launchplan_version_launchplan_id = ?
	");

	// get ordersheet items
	$getOrderSheetItems = $model->db->prepare("
		SELECT GROUP_CONCAT(lps_launchplan_item_id) AS items
		FROM lps_launchplan_items
		WHERE lps_launchplan_item_launchplan_id = ?
	");

	// delete order sheet warehouses
	$deleteOrderSheetWarehouses = $model->db->prepare("
		DELETE FROM lps_launchplan_warehouses
		WHERE lps_launchplan_warehouse_launchplan_id = ?
	");

	// delete order sheet versions
	$deleteOrderSheetVersions = $model->db->prepare("
		DELETE FROM lps_launchplan_versions
		WHERE lps_launchplan_version_launchplan_id = ?
	");

	// delete order sheet version items
	$deleteOrderSheetVersionItems = $model->db->prepare("
		DELETE FROM lps_launchplan_version_items
		WHERE lps_launchplan_version_item_version_id IN (?)
	");

	// delete order sheet items
	$deleteOrderSheetItems = $model->db->prepare("
		DELETE FROM lps_launchplan_items
		WHERE lps_launchplan_item_launchplan_id = ?
	");

	// delete order sheet planned items
	$deleteOrderSheetPlannedItems = $model->db->prepare("
		DELETE FROM lps_launchplan_item_planned
		WHERE lps_launchplan_item_planned_item_id IN (?)
	");

	// delete order sheet confirmed items
	$deleteOrderSheetConfirmedItems = $model->db->prepare("
		DELETE FROM lps_launchplan_item_confirmed
		WHERE lps_launchplan_item_confirmed_item_id IN (?)
	");

	// delete order sheet confirmed items
	$deleteOrderSheetShippedItems = $model->db->prepare("
		DELETE FROM lps_launchplan_item_shipped
		WHERE lps_launchplan_item_shipped_item_id IN (?)
	");

	// delete order sheet distrubuted items
	$deleteOrderSheetDistributedItems = $model->db->prepare("
		DELETE FROM lps_launchplan_item_delivered
		WHERE lps_launchplan_item_delivered_item_id IN (?)
	");

	// is standard ordersheet
	$standard = ($_REQUEST['standard']==$launchplan->data['lps_ordersheet_mastersheet_id']) ? true : false;

	if ($standard) { 
		
		// order sheet workflow state is deletable
		if (in_array($launchplan->data['lps_launchplan_workflowstate_id'], $_DELETABLE_STATES)) {
				
			// delete order sheet
			$response = $launchplan->delete();
			
			if ($response) {
				
				// delete order sheet warehouses
				$deleteOrderSheetWarehouses->execute(array($id));

				// get order sheet versions
				$getOrderSheetVersions->execute(array($id));
				$result = $getOrderSheetVersions->fetch();
				$versions = $result['versions'];

				// delete order sheet versions
				$deleteOrderSheetVersions->execute(array($id));
				$deleteOrderSheetVersionItems->execute(array($versions));

				// get order sheet items
				$getOrderSheetItems->execute(array($id));
				$result = $getOrderSheetItems->fetch();
				$items = $result['items'];

				// delete order sheet items
				$deleteOrderSheetItems->execute(array($id));
				$deleteOrderSheetPlannedItems->execute(array($items));
				$deleteOrderSheetConfirmedItems->execute(array($items));
				$deleteOrderSheetShippedItems->execute(array($items));
				$deleteOrderSheetDistributedItems->execute(array($items));

				Message::request_deleted();
				url::redirect("/$application/$controller");
			}
			else {
				Message::request_failure();
				url::redirect("/$application/$controller/$action/$id");
			}
		} else {
			Message::add(array(
				'type'=>'error',
				'content'=>'The Order Sheet is not in deletabled states.',
				'life'=>3000
			));
			url::redirect("/$application/$controller/$action/$id");
		}
	} else {
		Message::add(array(
			'type'=>'error',
			'content'=>'Only Standard Order Sheet can be deleted.',
			'life'=>3000
		));
		url::redirect("/$application/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}

