<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

// request
$application = $_REQUEST['application'];
$section = $_REQUEST['section'];
$id = $_REQUEST['id'];

$model = new Model($application);

switch ($section) {

	case 'data':

		if ($id) {
			$sth = $model->db->prepare("
				SELECT * 
				FROM lps_launchplans 
				INNER JOIN lps_mastersheets ON lps_mastersheet_id = lps_launchplan_mastersheet_id
				WHERE lps_launchplan_id = ?
			");
			$sth->execute(array($id));
			$json = $sth->fetch();
		}

	break;
}

if (!headers_sent()) {
	header('Content-Type: text/json');
}

echo json_encode($json);