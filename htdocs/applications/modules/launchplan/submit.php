<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$id = $_REQUEST['lps_launchplan_id'];
$newState = $_REQUEST['lps_launchplan_workflowstate_id'];

// load launchplan
$launchplan = new Modul($application);
$launchplan->setTable('lps_launchplans');
$launchplan->read($id);

$data = array();
		
if (isset($_REQUEST['lps_launchplan_workflowstate_id'])) {
	$data['lps_launchplan_workflowstate_id'] = $_REQUEST['lps_launchplan_workflowstate_id'];
}

if (isset($_REQUEST['lps_launchplan_address_id'])) {
	$data['lps_launchplan_address_id'] = $_REQUEST['lps_launchplan_address_id'];
}

if (isset($_REQUEST['lps_launchplan_mastersheet_id'])) {
	$data['lps_launchplan_mastersheet_id'] = $_REQUEST['lps_launchplan_mastersheet_id'];
}

if (isset($_REQUEST['lps_launchplan_comment'])) {
	$data['lps_launchplan_comment'] = $_REQUEST['lps_launchplan_comment'];
}

if (isset($_REQUEST['lps_launchplan_openingdate'])) {
	$data['lps_launchplan_openingdate'] = date::sql($_REQUEST['lps_launchplan_openingdate']);
}

if (isset($_REQUEST['lps_launchplan_closingdate'])) {
	$data['lps_launchplan_closingdate'] = date::sql($_REQUEST['lps_launchplan_closingdate']);
}


if ($data && $launchplan->id) {

	$currentState = $launchplan->data['lps_launchplan_workflowstate_id'];
	
	// track state change
	$track = $newState && $newState<>$currentState ? true : false;
	
	$data['user_modified'] = $user->login;
	$data['date_modified'] = date('Y-m-d H:i:s');
	
	// update order shet data
	$response = $launchplan->update($data);
	
	if ($response) {

		$model = new Model($application);

		Message::request_saved();
		
		$redirect = $_REQUEST['redirect'];

		// reset items on state change open
		if ($newState && $newState<>$currentState && in_array($newState, array(1,2,3,4,5,6))) {
			
			$clientQuantities = in_array($newState, array(1,7)) ? "lps_launchplan_item_quantity = NULL," : null;

			$sth = $model->db->prepare("
				UPDATE lps_launchplan_items SET
					$clientQuantities
					lps_launchplan_item_quantity_approved = NULL,
					lps_launchplan_item_customernumber = NULL,
					lps_launchplan_item_order_date = NULL
				WHERE lps_launchplan_item_launchplan_id = ?
			");
			
			$sth->execute(array($launchplan->id));

			$items = $model->query("
				SELECT lps_launchplan_item_id
				FROM lps_launchplan_items
				WHERE lps_launchplan_item_launchplan_id = $launchplan->id
			")->fetchAll();

			if ($items) {
				
				if (in_array($newState, array(1,2)) ) {
					$sth = $model->db->prepare("
						DELETE FROM lps_launchplan_item_week_quantities
						WHERE lps_launchplan_item_week_quantity_item_id = ?
					");
				} else {
					$sth = $model->db->prepare("
						UPDATE lps_launchplan_item_week_quantities SET
							lps_launchplan_item_week_quantity_quantity_approved = NULL,
							lps_launchplan_item_week_quantity_pon = NULL,
							lps_launchplan_item_week_quantity_line = NULL,
							lps_launchplan_item_week_quantity_delivery_date = NULL
						WHERE lps_launchplan_item_week_quantity_item_id = ?
					");
				}

				foreach ($items as $row) {
					$sth->execute(array($row['lps_launchplan_item_id']));
				}
			}
		}

	} else {
		Message::request_failure();
	}

	// track workflow state changes
	if ($response && $track) {

		$workflowState = new Modul($application);
		$workflowState->setTable('lps_workflow_states');
		$workflowState->read($newState);
			
		$tracking = new User_Tracking($application);
		$tracking->create(array(
			'user_tracking_entity' => 'launch plan',
			'user_tracking_entity_id' => $launchplan->id,
			'user_tracking_user_id' => $user->id,
			'user_tracking_action' => $workflowState->data['lps_workflow_state_name'],
			'user_created' => $user->login,
			'date_created' => date('Y-m-d H:i:s')
		));
	}

} else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect
));
	