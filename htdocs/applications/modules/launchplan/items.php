<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

// request :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['launchplan'];
$version = $_REQUEST['version'];
$_REQUEST = session::filter($application, "$controller.$archived.$action", false);


// launchplan data :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// model
$model = new Model($application);

// launchplan
$launchplan = new Modul($application);
$launchplan->setTable('lps_launchplans');
$launchplan->read($id);

// master sheet
$mastersheet = new Modul($application);
$mastersheet->setTable('lps_mastersheets');
$mastersheet->read($launchplan->data['lps_launchplan_mastersheet_id']);

$_MASTERSHEET_FIRST_WEEK = $mastersheet->data['lps_mastersheet_week_number_first'];
$_MASTERSHEET_TOTAL_WEEKS = $mastersheet->data['lps_mastersheet_weeks'];
$_MASTERSHEET_ESTIMATE_MONTHS = $mastersheet->data['lps_mastersheet_estimate_month'];

// company
$company = new Company();
$company->read($launchplan->data['lps_launchplan_adress_id']);


// launchplan states :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

define('CELL_READONLY', 'true');

// state handler
$state = new State($launchplan->data['lps_launchplan_workflowstate_id'], $application);
$state->setOwner($launchplan->data['lps_launchplan_address_id']);
$state->setDateIntervals($launchplan->data['lps_launchplan_openingdate'], $launchplan->data['lps_launchplan_closingdate']);

$_STATE_COMPLETED = 4;
$_STATE_REVISION = 6;

$_IS_EDITABLED = $state->canEdit();
$_EDIT_ESTIMATE_QUANTITY_ARCHIVE = user::permission('can_edit_launchplans_in_archive') && $state->isExported() ? true : false;
$_IS_REVISION = $state->isRevision();

if ($state->isOwner()) {
	$_IS_EDITABLED = in_array($state->state, array(2,3,8)) ? $_IS_EDITABLED : false;
}

// show approvment
if ($state->canAdministrate()) $_SHOW_APPROVED_COLUMN = true;
else $_SHOW_APPROVED_COLUMN = $state->state >= 5 == !$state->isRevision() ? true : false;

// revision column
if ($state->canAdministrate()) {
	$_HIDE_REVISION_COLUMN = $state->isCompleted() || $state->isRevision() ? false : true;
} else {
	$_HIDE_REVISION_COLUMN = $state->isRevision() ? false : true;
}

// get items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEMS = array();
$_ITEMS_REVISION = array();
	
$sth = $model->db->prepare("
	SELECT DISTINCT
		lps_launchplan_item_id AS id,
		lps_launchplan_item_price AS price, 
		currency_symbol AS currency_symbol, 
		lps_launchplan_item_exchangrate AS currency_exchangrate, 
		lps_launchplan_item_factor AS currency_factor, 
		lps_launchplan_item_quantity AS quantity, 
		lps_launchplan_item_quantity_proposed AS quantity_proposed, 
		lps_launchplan_item_quantity_approved AS quantity_approved, 
		lps_launchplan_item_quantity_estimate AS quantity_estimate, 
		lps_launchplan_item_status AS status, 
		lps_launchplan_item_comment AS reference_description,
		lps_reference_code AS reference_code, 
		lps_reference_name AS reference_name, 
		lps_collection_code AS collection_code,
		lps_collection_category_code AS collection_category,
		lps_mastersheet_estimate_month AS estimate_month,							
		lps_product_group_id AS group_id, 
		CONCAT(lps_product_group_code, ', ', lps_product_group_name) AS group_name, 
		lps_collection_category_id AS subgroup_id,
		lps_collection_category_code AS subgroup_name
	FROM lps_launchplan_items INNER JOIN lps_references ON lps_launchplan_items.lps_launchplan_item_reference_id = lps_references.lps_reference_id
	INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
	INNER JOIN lps_mastersheets ON lps_mastersheet_id = lps_launchplan_mastersheet_id
	INNER JOIN lps_product_groups ON lps_references.lps_reference_product_group_id = lps_product_groups.lps_product_group_id
	INNER JOIN lps_collections ON lps_references.lps_reference_collection_id = lps_collections.lps_collection_id
	INNER JOIN lps_collection_categories ON lps_references.lps_reference_collection_category_id = lps_collection_categories.lps_collection_category_id
	INNER JOIN db_retailnet.currencies ON currency_id = lps_launchplan_item_currency
	WHERE lps_launchplan_item_launchplan_id = ?
	ORDER BY 
		group_name,
		lps_collection_category_code,
		lps_collection_code,
		lps_reference_code
");

$sth->execute(array($launchplan->id));
$result = $sth->fetchAll();
	
if ($result) {
	
	foreach ($result as $row) {
		
		$group = $row['group_id'];
		$item = $row['id'];	
		
		$_ITEMS[$group]['caption'] = $row['group_name'];
		$_ITEMS[$group]['data'][$item]['collection_code'] = $row['collection_code'];
		$_ITEMS[$group]['data'][$item]['collection_category'] = $row['collection_category'];
		$_ITEMS[$group]['data'][$item]['reference_code'] = $row['reference_code'];
		$_ITEMS[$group]['data'][$item]['reference_name'] = $row['reference_name'];
		$_ITEMS[$group]['data'][$item]['reference_description'] = $row['reference_description'];
		$_ITEMS[$group]['data'][$item]['estimate_month'] = $row['estimate_month'];
		$_ITEMS[$group]['data'][$item]['price'] = number_format($row['price'], 2, '.', '');
		$_ITEMS[$group]['data'][$item]['currency_symbol'] = $row['currency_symbol'];
		$_ITEMS[$group]['data'][$item]['currency_exchangrate'] = $row['currency_exchangrate'];
		$_ITEMS[$group]['data'][$item]['currency_factor'] = $row['currency_factor'];
		$_ITEMS[$group]['data'][$item]['quantity'] = $row['quantity'];
		$_ITEMS[$group]['data'][$item]['quantity_estimate'] = $row['quantity_estimate'];
		$_ITEMS[$group]['data'][$item]['quantity_approved'] = $row['quantity_approved'];

		if ($row['status']) {
			$_ITEMS_REVISION[$item] = true;
		}
	}
}

// check items in revision
if ($state->state==$_STATE_REVISION && !$_ITEMS_REVISION) {
	$_HIDE_REVISION_COLUMN = true;
}



// item week data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEMS_WEEK_DATA = array();
	
$sth = $model->db->prepare("
	SELECT DISTINCT
		lps_launchplan_item_week_quantity_id AS id,
		lps_launchplan_item_week_quantity_item_id AS item,
		lps_launchplan_item_week_quantity_week AS week,
		lps_launchplan_item_week_quantity_quantity AS quantity,
		lps_launchplan_item_week_quantity_quantity_approved AS quantity_approved
	FROM lps_launchplan_item_week_quantities
	INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_week_quantity_item_id
	WHERE lps_launchplan_item_launchplan_id = ?
");

$sth->execute(array($launchplan->id));
$result = $sth->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$item = $row['item'];
		$week = $row['week'];
		$_ITEMS_WEEK_DATA[$item][$week] = array(
			'id' => $row['id'],
			'quantity' => $row['quantity'],
			'quantity_approved' => $row['quantity_approved']
		);
	}
}

// item planned quantities :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEMS_PLANNED_QUANTITIES = array();

$sth = $model->db->prepare("
	SELECT 
		lps_launchplan_item_planned_quantity_item_id AS item,
		SUM(lps_launchplan_item_planned_quantity_quantity) AS total_quantities
	FROM lps_launchplan_item_planned_quantities
	INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_planned_quantity_item_id
	WHERE lps_launchplan_item_launchplan_id = ?
	GROUP BY lps_launchplan_item_planned_quantity_item_id
");

$sth->execute(array($launchplan->id));
$result = $sth->fetchAll();
$_ITEMS_PLANNED_QUANTITIES = _array::extract($result);


// spreadsheet references ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$fixedRows = 1;
$firstFixedRow = 1;
$lastFixedRow = 1;
$fixedRowsSeparator = 0;

$fixedColumns = 11;
$firstFixedColumn = 2;
$lastFixedColumn = 11;
$fixedColumnSeparator = 0;
$totalFixedRows = $fixedRows + $fixedRowsSeparator;
$totalFixedColumns = $fixedColumns + $fixedColumnSeparator;

// datagrid
$datagrid = array();
$rowGroupSeparator = 1;


if ($_ITEMS) {

	$row=1;

	foreach ($_ITEMS as $g => $group) {

		$row++;
		$rowGroups[$row] = true;
		
		$col=$firstFixedColumn;
		$datagrid[$row][$col]['caption'] = $group['caption'];
		$datagrid[$row][$col]['group'] = true;

		$row++;
		$rowGroupHeaders[$row] = true;

		$col=$firstFixedColumn;
		$datagrid[$row][$col]['caption'] = 'Collection Code';
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Reference';
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Name';
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Collection Category';
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Comment';
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Estimate Price';
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Currency';
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = "Estimate for $_MASTERSHEET_ESTIMATE_MONTHS months";
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Total Planned';
		$datagrid[$row][$col]['header'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = '% of Estimate';
		$datagrid[$row][$col]['header'] = true;

		for ($i=0; $i < $_MASTERSHEET_TOTAL_WEEKS; $i++) { 
			$col++;
			$week = $_MASTERSHEET_FIRST_WEEK+$i;
			$datagrid[$row][$col]['caption'] = "Week $week";
			$datagrid[$row][$col]['header'] = true;
		}

		$col++;
		$datagrid[$row][$col]['caption'] = 'Total Launch';
		$datagrid[$row][$col]['header'] = true;

		if ($_SHOW_APPROVED_COLUMN) {
			
			for ($i=0; $i < $_MASTERSHEET_TOTAL_WEEKS; $i++) { 
				$col++;
				$week = $_MASTERSHEET_FIRST_WEEK+$i;
				$datagrid[$row][$col]['caption'] = "Week $week";
				$datagrid[$row][$col]['header'] = true;
			}

			$col++;
			$datagrid[$row][$col]['caption'] = 'Total Approved';
			$datagrid[$row][$col]['header'] = true;
		}

		// total datagrid columns
		$totalDataGridColumns = $col;
			
		foreach ($group['data'] as $key => $item) {

			$row++;

			if (!$_HIDE_REVISION_COLUMN) {
				if ($_IS_EDITABLED) {
					$checked = $_ITEMS_REVISION[$key] ? 'checked=checked' : null;
					$datagrid[$row][1]['value'] = "<input type=checkbox name=revision[$key] value=1 $checked />";
				} elseif ($_IS_REVISION && $_ITEMS_REVISION[$key]) {
					$datagrid[$row][1]['value'] = "<i class=\"fa fa-refresh\" ></i>";
				}
			}
			
			$col=$firstFixedColumn;
			$datagrid[$row][$col]['value'] = $item['collection_code'];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['reference_code'];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['reference_name'];		

			$col++;
			$datagrid[$row][$col]['value'] = $item['collection_category'];	

			$col++;
			$datagrid[$row][$col]['value'] = $item['reference_description'];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['price'];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['currency_symbol'];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['quantity_estimate'];
			$datagrid[$row][$col]['item'] = $key;

			if ($_IS_EDITABLED) $datagrid[$row][$col]['input'] = $_IS_EDITABLED;
			elseif ($_IS_REVISION) {
				$datagrid[$row][$col]['input'] = $_ITEMS_REVISION[$key] ? true : false;
				$datagrid[$row][$col]['class'] = $_ITEMS_REVISION[$key] ? 'cel cel-revision' : null;
			}
			else $datagrid[$row][$col]['input'] = $_IS_EDITABLED;

			if ($_EDIT_ESTIMATE_QUANTITY_ARCHIVE) {
				$datagrid[$row][$col]['input'] = true;
			}
			
			$col++;
			$datagrid[$row][$col]['value'] = $_ITEMS_PLANNED_QUANTITIES[$key];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['estimate_month'] ? ($item['total_planned']/$item['estimate_month'])*100 : null;

			for ($i=0; $i < $_MASTERSHEET_TOTAL_WEEKS; $i++) { 
				
				$col++;
				$week = $_MASTERSHEET_FIRST_WEEK+$i;
				
				$datagrid[$row][$col]['item'] = $key;
				$datagrid[$row][$col]['week'] = $week;
				$datagrid[$row][$col]['id'] = $_ITEMS_WEEK_DATA[$key][$week]['id'];
				$datagrid[$row][$col]['value'] = $_ITEMS_WEEK_DATA[$key][$week]['quantity'];

				if ($_IS_EDITABLED) $datagrid[$row][$col]['input'] = $_IS_EDITABLED;
				elseif ($_IS_REVISION) {
					$datagrid[$row][$col]['input'] = $_ITEMS_REVISION[$key] ? true : false;
					$datagrid[$row][$col]['class'] = $_ITEMS_REVISION[$key] ? 'cel cel-revision' : null;
				}
				else $datagrid[$row][$col]['input'] = $_IS_EDITABLED;
			}

			// col total quantities
			$col++;
			$datagrid[$row][$col]['value'] = null;

			if ($_SHOW_APPROVED_COLUMN) {
				
				for ($i=0; $i < $_MASTERSHEET_TOTAL_WEEKS; $i++) { 
					
					$col++;
					$week = $_MASTERSHEET_FIRST_WEEK+$i;
					
					$datagrid[$row][$col]['item'] = $key;
					$datagrid[$row][$col]['week'] = $week;
					$datagrid[$row][$col]['id'] = $_ITEMS_WEEK_DATA[$key][$week]['id'];
					$datagrid[$row][$col]['value'] = $_ITEMS_WEEK_DATA[$key][$week]['quantity_approved'];

					if ($_IS_EDITABLED) $datagrid[$row][$col]['input'] = $_IS_EDITABLED;
					elseif ($_IS_REVISION) $datagrid[$row][$col]['input'] = $_ITEMS_REVISION[$key] ? true : false;
					else $datagrid[$row][$col]['input'] = $_IS_EDITABLED;
				}

				// col total approved quantities
				$col++;
				$datagrid[$row][$col]['value'] = null;
			}
		}

		$row++;
		$rowGroupSubtotals[$row] = true;

		$col=$firstFixedColumn;
		$datagrid[$row][$col]['caption'] = 'Total '.$group['caption'];

		$row++;
		$rowGroupSeparators[$row] = true;

		$col=$firstFixedColumn;
		$datagrid[$row][$col]['separator'] = true;
	}

	// grand total
	$row++;
	$rowGrandTotal = $row;
	$datagrid[$row][2]['caption'] = 'Launch Plan Totals';

	$totalDataGridRows = $row-1;
}


// triggers
$firstWeekCol = $totalFixedColumns + 1;
$lastWeekCol = $firstWeekCol + $_MASTERSHEET_TOTAL_WEEKS - 1;
$totalWeekCol = $lastWeekCol + 1;
$firstApprovedWeekCol = $lastWeekCol + 2;
$lastApprovedWeekCol = $firstApprovedWeekCol + $_MASTERSHEET_TOTAL_WEEKS - 1;
$totalApprovedWeekCol = $lastApprovedWeekCol + 1;
$colRevision = 1;
$colEstimateQuantity = $firstWeekCol - 3;
$colPlanningQuantity = $firstWeekCol - 2;
$colEstimateQuote = $firstWeekCol - 1;
$firstDataGridRow = $totalFixedRows+1;
$lastDataGridRow = $firstDataGridRow + $totalDataGridRows - 1;
$firstDataGridCol = 1;
$totalRows = $totalFixedRows + $totalDataGridRows;
$totalColumns = $totalDataGridColumns;

if ($totalRows && $totalColumns) {

	for ($row=1; $row <= $totalRows; $row++) {
			
		for ($col=1; $col <= $totalColumns; $col++) {

			$index = spreadsheet::key($col, $row);

			switch ($row) {
				
				case 1: // first blank row

					$grid[$index]['html'] = "&nbsp;";
					$grid[$index]['readonly'] = CELL_READONLY;

				break;

				case $rowGroups[$row]: // product group name

					if ($col==$colRevision) {
						$grid[$index]['cls'] = "group cel cel-revision";
					}

					if ($col==$firstFixedColumn) {
						$caption = $datagrid[$row][$col]['caption'];
						$grid[$index]['html'] = "<h4>$caption</h4>";
						$grid[$index]['cls'] = "group";
						$mergecel[$index] = $totalColumns - 1;
					}

					$grid[$index]['readonly']  = CELL_READONLY;

				break;
				
				case $rowGroupHeaders[$row]: // table header

					$groupFirstRow = $row+1;
					$class = $col >= $firstWeekCol ? 'week-column' : null;
					$grid[$index]['html'] = $datagrid[$row][$col]['caption'];
					$grid[$index]['cls'] = "header $class";
					$grid[$index]['readonly']  = CELL_READONLY;

				break;
				
				case $rowGroupSeparators[$row];
					$grid[$index]['cls'] = "cel-separator";
					$grid[$index]['readonly']  = CELL_READONLY;
				break;


				case $rowGroupSubtotals[$row]: // group subtotal

					$class = null;

					if ($col==$firstFixedColumn) {
						$grid[$index]['text'] = $datagrid[$row][$col]['caption'];
						$class  = "subtotal-caption";
						$mergecel[$index] = 7;
					}
					elseif ($col==$colEstimateQuote) {

						$celTotalLaunchplan = spreadsheet::key($lastWeekCol+1,$row);
						$celEstimate = spreadsheet::key($col-2,$row);
							
						$grid[$index]['text']  = "function($celTotalLaunchplan, $celEstimate) { 
							var t = $celTotalLaunchplan>0 && $celEstimate>0 ? ($celTotalLaunchplan/$celEstimate)*100 : 0;
							var n = t.toFixed(2);
							return n ? n+' %' : null;
						}";
					}
					elseif ($col >= $colEstimateQuantity) {
						
						$class = "subtotal-estimate-quantities";
						$range = spreadsheet::key($col,$groupFirstRow).'_'.spreadsheet::key($col,$row-1);
						
						$grid[$index]['text']  = "function($range) { 
							var t = sum($range);
							return t || null;
						}";

						$columnIndex[$col][] = $index;
					}

					$grid[$index]['cls'] = "cel-subtotal $class";
					$grid[$index]['readonly']  = CELL_READONLY;

				break;

				case $rowGrandTotal: // total lanchplan

					$class = null;

					if ($col==$firstFixedColumn) {
						$grid[$index]['text'] = $datagrid[$row][$col]['caption'];
						$class  = "total-caption";
						$mergecel[$index] = 7;
					}
					elseif ($col==$colEstimateQuote) {

						$celTotalLaunchplan = spreadsheet::key($lastWeekCol+1,$row);
						$celEstimate = spreadsheet::key($col-2,$row);
							
						$grid[$index]['text']  = "function($celTotalLaunchplan, $celEstimate) { 
							var t = $celTotalLaunchplan>0 && $celEstimate>0 ? ($celTotalLaunchplan/$celEstimate)*100 : 0;
							var n = t.toFixed(2);
							return n ? n+' %' : null;
						}";
					}
					elseif ($col >= $colEstimateQuantity && $col <> $colEstimateQuote) {

						$indexes = $columnIndex[$col];

						if ($indexes) {

							$args = join(',',$indexes);
							$sum = join('+',$indexes);
							
							$grid[$index]['text']  = "function($args) { 
								var t = $sum;
								return t || null;
							}";
						}
						
					}

					$grid[$index]['cls'] = "grand-total $class";
					$grid[$index]['readonly']  = CELL_READONLY;

				break;

				case $row >= $firstDataGridRow && $row <= $lastDataGridRow :

					
					$value = $datagrid[$row][$col]['value'];

					if ($col >= $firstWeekCol && $col <= $lastWeekCol) {
						
						$grid[$index]['text'] = $value;
						$grid[$index]['hiddenAsNull'] = 'true';
						$grid[$index]['numeric'] = 'true';
						
						if ($datagrid[$row][$col]['input']) {
							$grid[$index]['cls'] = $datagrid[$row][$col]['class'] ?: "cel cel-input";
							$item = $datagrid[$row][$col]['item'];
							$week = $datagrid[$row][$col]['week'];
							$id = $datagrid[$row][$col]['id'];
							$grid[$index]['tag'] = "week-quantity;$item;$week;$id";
						} else {
							$grid[$index]['readonly']  = CELL_READONLY;
							$grid[$index]['cls'] = 'cel';
						}
					}
					elseif($col==$totalWeekCol) {
						$range = spreadsheet::key($firstWeekCol,$row).'_'.spreadsheet::key($lastWeekCol,$row);
						$grid[$index]['text']  = "function($range) { total = sum($range); return total ? total : null; }";
						$grid[$index]['cls'] = "cel total-row-quantities";
					}
					elseif ($col >= $firstApprovedWeekCol && $col <= $lastApprovedWeekCol) {
						
						$grid[$index]['text'] = $value;
						$grid[$index]['hiddenAsNull'] = 'true';
						$grid[$index]['numeric'] = 'true';

						if ($datagrid[$row][$col]['input']) {
							$grid[$index]['cls'] = "cel cel-input";
							$item = $datagrid[$row][$col]['item'];
							$week = $datagrid[$row][$col]['week'];
							$id = $datagrid[$row][$col]['id'];
							$grid[$index]['tag'] = "week-quantity-approved;$item;$week;$id";
						} else {
							$grid[$index]['readonly']  = CELL_READONLY;
							$grid[$index]['cls'] = 'cel';
						}
					}
					elseif ($col == $totalApprovedWeekCol) {
						
						$grid[$index]['cls'] = "cel total-row-approved-quantities";
						$range = spreadsheet::key($firstApprovedWeekCol,$row).'_'.spreadsheet::key($lastApprovedWeekCol,$row);

						$grid[$index]['text']  = "function($range) { 
							var total = sum($range); 
							return total ? total : null; 
						}";
						
					}
					else {

						// revision column
						if (!$_HIDE_REVISION_COLUMN && $col==$colRevision) {
							$grid[$index]['readonly']  = CELL_READONLY;
							$grid[$index]['html'] = $datagrid[$row][$col]['value'];
							$grid[$index]['cls'] = "cel";
						}
						// % of estimatet quantities
						elseif ($col==$colEstimateQuote) {

							$celEstimateQuantity = spreadsheet::key($col-2,$row);
							$celTotalLaunchplan = spreadsheet::key($lastWeekCol+1,$row);
							
							$grid[$index]['text']  = "function($celEstimateQuantity, $celTotalLaunchplan) { 
								var total = ($celEstimateQuantity>0 && $celTotalLaunchplan>0) ? ($celTotalLaunchplan/$celEstimateQuantity)*100 : 0;
								var n = total.toFixed(2);
								return n+' %';
							}";

							$grid[$index]['cls'] = "cel cel-estimate-quote";

						} 
						// estimated quantities
						elseif ($col==$colEstimateQuantity) {

							
							$grid[$index]['text'] = $value;
							$grid[$index]['hiddenAsNull'] = 'true';
							$grid[$index]['numeric'] = 'true';

							if ($datagrid[$row][$col]['input']) {
								$grid[$index]['cls'] = $datagrid[$row][$col]['class'] ?: "cel cel-input cel-estimate-quantity";
								$item = $datagrid[$row][$col]['item'];
								$grid[$index]['tag'] = "week-quantity-estimate;$item;0;0";
							} else {
								$grid[$index]['readonly']  = CELL_READONLY;
								$grid[$index]['cls'] = "cel";
							}

						} else {
							$grid[$index]['readonly']  = CELL_READONLY;
							$grid[$index]['text'] = $value;
							$grid[$index]['cls'] = $datagrid[$row][$col]['class'].' cel';
						}
					}
					
				break;
				
				default:
					$grid[$index]['readonly']  = CELL_READONLY;
				break;

			}
		}
	}	
}

if (!headers_sent()) {
	header('Content-Type: text/json');
}

echo json_encode(array(
	'response' => true,
	'data' => $grid,
	'merge' => $mergecel,
	'top' => 1,
	'left' => 1,
	'revisionColumn' => 'a',
	'hideRevisionColumn' => $_HIDE_REVISION_COLUMN,
	'test' => $state
));
