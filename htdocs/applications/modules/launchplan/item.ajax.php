<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application'];
$id = $_REQUEST['launchplan'];
$section = $_REQUEST['section'];
$item = $_REQUEST['item'];
$week = $_REQUEST['week'];
$weekID = $_REQUEST['weekID'];
$value = $_REQUEST['value'];

$model = new Model($application);

switch ($application) {
	
	case 'lps':
	

		$tableOrderSheet = 'lps_launchplans';
		$tableWorkflowState = "lps_workflow_states";

		$stateOpen = 2;
		$stateProgress = 3;

		$mapOrderSheet = array(
			'lps_launchplan_id' => 'ordersheet_id',
			'lps_launchplan_workflowstate_id' => 'ordersheet_workflowstate_id',
			'lps_launchplan_address_id' => 'ordersheet_address_id',
			'lps_launchplan_mastersheet_id' => 'ordersheet_mastersheet_id',
			'lps_launchplan_openingdate' => 'ordersheet_openingdate',
			'lps_launchplan_closingdate' => 'ordersheet_closingdate',
			'lps_launchplan_comment' => 'ordersheet_comment'
		);

		$mapWorkflowStates = array(
			'lps_workflow_state_name' => 'workflow_state_name'
		);

		$queryUpdateOrderSheetState = "
			UPDATE lps_launchplans SET
				lps_launchplan_workflowstate_id = 3,
				user_modified = ?
			WHERE lps_launchplan_id = ?
		";

		$queryUpdateQuantityEstimate = "
			UPDATE lps_launchplan_items SET
				lps_launchplan_item_quantity_estimate = ?
			WHERE lps_launchplan_item_id = ?
		";

		$queryAddWeekQuantity = "
			INSERT INTO lps_launchplan_item_week_quantities (
				lps_launchplan_item_week_quantity_item_id,
				lps_launchplan_item_week_quantity_week,
				lps_launchplan_item_week_quantity_quantity,
				user_created
			)
			VALUES (?,?,?,?)
		";

		$queryUpdateWeekQuantity = "
			UPDATE lps_launchplan_item_week_quantities SET
				lps_launchplan_item_week_quantity_quantity = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE lps_launchplan_item_week_quantity_id = ?
		";

		$queryAddWeekQuantityApproved = "
			INSERT INTO lps_launchplan_item_week_quantities (
				lps_launchplan_item_week_quantity_item_id,
				lps_launchplan_item_week_quantity_week,
				lps_launchplan_item_week_quantity_quantity_approved,
				user_created
			)
			VALUES (?,?,?,?)
		";

		$queryUpdateWeekQuantityApproved = "
			UPDATE lps_launchplan_item_week_quantities SET
				lps_launchplan_item_week_quantity_quantity_approved = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE lps_launchplan_item_week_quantity_id = ?
		";

		$qGetWeekQuantityFromItem = "
			SELECT lps_launchplan_item_week_quantity_id AS id
			FROM lps_launchplan_item_week_quantities
			WHERE lps_launchplan_item_week_quantity_item_id = ? AND lps_launchplan_item_week_quantity_week = ?
		";

		$qRemoveEmptyRow = "
			DELETE FROM lps_launchplan_item_week_quantities
			WHERE lps_launchplan_item_week_quantity_id = ?
			AND (lps_launchplan_item_week_quantity_quantity = 0 OR lps_launchplan_item_week_quantity_quantity IS NULL)
			AND (lps_launchplan_item_week_quantity_quantity_approved = 0 OR lps_launchplan_item_week_quantity_quantity_approved IS NULL)
		";
		
	break;
}

// order sheet
$ordersheet = new Modul($application);
$ordersheet->setTable($tableOrderSheet);
$ordersheet->setDataMap($mapOrderSheet);
$ordersheet->read($id);

if ($ordersheet->id) {

	switch ($section) {
		
		case 'week-quantity-estimate':
			
			if ($item) {

				$sth = $model->db->prepare($queryUpdateQuantityEstimate);
				$response = $sth->execute(array($value, $item));
				$json['id'] = $item;
				$inProgress = $response && $ordersheet->data['ordersheet_workflowstate_id']==$stateOpen ? true : false;
			}
			
		break;
		
		case 'week-quantity':
			
			if ($item && $week) {

				// has item approved quantity
				if (!$weekID) {
					$sth = $model->db->prepare($qGetWeekQuantityFromItem);
					$sth->execute(array($item, $week));
					$result = $sth->fetch();
					$weekID = $result['id'];
				}

				if ($weekID) {
					$sth = $model->db->prepare($queryUpdateWeekQuantity);
					$response = $sth->execute(array($value, $user->login, $weekID));
					$json['id'] = $weekID;
				} else {
					$sth = $model->db->prepare($queryAddWeekQuantity);
					$response = $sth->execute(array($item, $week, $value, $user->login));
					$json['id'] = $model->db->lastInsertId();
					$inProgress = $response && $ordersheet->data['ordersheet_workflowstate_id']==$stateOpen ? true : false;
				}

				if (!$value && $weekID) {
					$sth = $model->db->prepare($qRemoveEmptyRow);
					$sth->execute(array($weekID));
				}
			}
			
		break;
		
		case 'week-quantity-approved':
			
			if ($item && $week) {

				// has item approved quantity
				if (!$weekID) {
					$sth = $model->db->prepare($qGetWeekQuantityFromItem);
					$sth->execute(array($item, $week));
					$result = $sth->fetch();
					$weekID = $result['id'];
				}

				if ($weekID) {
					$sth = $model->db->prepare($queryUpdateWeekQuantityApproved);
					$response = $sth->execute(array($value, $user->login, $weekID));
					$json['id'] = $weekID;
				} else {
					$sth = $model->db->prepare($queryAddWeekQuantityApproved);
					$response = $sth->execute(array($item, $week, $value, $user->login));
					$json['id'] = $model->db->lastInsertId();
					$inProgress = $response && $ordersheet->data['ordersheet_workflowstate_id']==$stateOpen ? true : false;
				}

				if (!$value) {
					$sth = $model->db->prepare($qRemoveEmptyRow);
					$sth->execute(array($item, $week));
				}
			}
			
		break;
	}

} else {
	$json['response'] = false;
	$json['message'] = "The Order Sheet not found.";
}


// track workflow state change
if ($inProgress) {

	$sth = $model->db->prepare($queryUpdateOrderSheetState);
	$response = $sth->execute(array($user->id, $ordersheet->id));

	if ($response) {
		
		$workflowState = new Modul($application);
		$workflowState->setTable($tableWorkflowState);
		$workflowState->setDataMap($mapWorkflowStates);
		$workflowState->read($stateProgress);
		$newState = $workflowState->data['workflow_state_name'];

		$userTrack = new User_Tracking($application);
		$userTrack->create(array(
			'user_tracking_entity' => 'order sheet',
			'user_tracking_entity_id' => $ordersheet->id,
			'user_tracking_user_id' => $user->id,
			'user_tracking_action' => $workflowState->data['workflow_state_name'],
			'user_created' => $user->login,
			'date_created' => date('Y-m-d H:i:s')
		));
	}
}

	
if (!headers_sent()) {
header('Content-Type: text/json');
}

echo json_encode($json);
	