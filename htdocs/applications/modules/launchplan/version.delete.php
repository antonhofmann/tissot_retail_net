<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

// request vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

// db application model
$model = new Model($application);

// ordersheet
$version = new Modul($application);
$version->setTable('lps_launchplan_versions');
$version->read($id);

if ($version->id) {

	$launchplan = $version->data['lps_launchplan_version_launchplan_id'];
	$response = $version->delete();

	if ($response) { 

		$sth = $model->db->prepare("
			DELETE FROM lps_launchplan_version_items
			WHERE lps_launchplan_version_item_version_id IN (?)
		");

		$sth->execute(array($id));

		Message::request_deleted();
		url::redirect("/$application/$controller/$action/$launchplan");

	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$launchplan/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}

