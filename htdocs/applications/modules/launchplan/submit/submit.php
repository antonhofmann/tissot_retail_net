<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$sendmail = $_REQUEST['sendmail'];

// selectd launch plans
$launchplans = $_REQUEST['launchplans'] ? explode(',', $_REQUEST['launchplans']) : false;

$model = new Model($application);


switch ($action) {
	
	// launchplan submiter
	case 'submit':
		
	if ($launchplans) {

		$sth = $model->db->prepare("
			UPDATE lps_launchplans SET
				lps_launchplan_workflowstate_id = 2,
				user_modified = ?,
				date_modified = NOW()
			WHERE lps_launchplan_id = ?
		");	

		// set ordersheets in workflow state "open"
		foreach ($launchplans as $id) {
			$sth->execute(array($user->login, $id));
		}
		
		$json['response'] = true;
		$json['reload'] = true;
		$json['notification'] = "The Launch Plans are submitted.";
		$json['sendmail'] = $sendmail;
		
		// onload message
		Message::request_submitted();

	}  else {
		$json['response'] = false;
		$json['notification'] = "Bad request. Check form data.";
	}

	break;	

	// launchplan rimainder
	case 'remind':
		
		// no actions
		if ($launchplans) {
			$json['response'] = true;
			$json['sendmail'] = $sendmail;
			$json['notification'] = "The Launch Plans are submitted.";
		} else {
			$json['response'] = false;
			$json['notification'] = "Bad request. Check form data.";
		}

	break;
}


if (!headers_sent()) {
	header('Content-Type: text/json');
}

echo json_encode($json);
