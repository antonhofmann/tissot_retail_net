<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
$user = User::instance();
$translate = Translate::instance();

$_APP = $_REQUEST['application'];
$_TPL = $_REQUEST['mail_template_id'];
$_LAUNCHPLANS = $_REQUEST['launchplans'];
$_SUBJECT = $_REQUEST['mail_template_subject'];
$_CONTENT = $_REQUEST['mail_template_text'];
$_TEST = $_REQUEST['test_recipient'];

$_CONSOLE = array();
$_NOTIFICATIONS = array();


if (!$_LAUNCHPLANS || !$_TPL) {
	
	$_NOTIFICATIONS[] = "Bad request";

	if (!$_LAUNCHPLANS) {
		$_NOTIFICATIONS[] = "Select at least one launch plan. ";
	}

	goto BLOCK_RESPONSE;
}
	

$mail = new ActionMail($_TPL);

// set parameters
$mail->setParam('application', $_APP);
$mail->setParam('launchplans', $_LAUNCHPLANS);	
	
// mail content
if ($_SUBJECT) $mail->setSubject($_SUBJECT);
if ($_CONTENT) $mail->setBody($_CONTENT);

// test mail
if ($_TEST) $mail->setTestMail($_TEST);
	
$mail->send();
	
// total send mails
$totalSendMails = $mail->getTotalSentMails();
	
$_RESPONSE = $totalSendMails > 0 ? true : false;
$_NOTIFICATIONS[] = "Total send $totalSendMails E-mails.";
$_CONSOLE = $mail->getConsole();

if ($totalSendMails > 0) {
	$_RELOAD = $_REQUEST['action']=='remind' ? false : true;
}
	

BLOCK_RESPONSE:	
header('Content-Type: text/json');
echo json_encode(array(
	'response' => $_RESPONSE,
	'reload' => $_RELOAD,
	'notification' => join('<br>', $_NOTIFICATIONS),
	'console' => $_CONSOLE
));