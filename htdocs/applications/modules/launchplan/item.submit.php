<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$_STATE_COMPLETED = 4;

// db model
$model = new Model($application);

$launchplan = new Modul($application);
$launchplan->setTable('lps_launchplans');
$launchplan->read($id);

if ($launchplan->id) {

	$sth = $model->db->prepare("
		UPDATE lps_launchplans SET
			lps_launchplan_workflowstate_id = ?,
			user_modified = ?
		WHERE lps_launchplan_id = ?
	");

	$response = $sth->execute(array($_STATE_COMPLETED, $user->id, $launchplan->id));

	if ($response) {
		$track = true;
		$reload = true;
		Message::add(array('content' => "The launch plan is successfully submitted."));
	} else {
		$message = "The launch plan is not submitted.";
	}

	// track workflow state change
	if ($track) {
			
		$workflowState = new Modul($application);
		$workflowState->setTable('lps_workflow_states');
		$workflowState->read($_STATE_COMPLETED);
			
		$userTrack = new User_Tracking($application);
		$userTrack->create(array(
			'user_tracking_entity' => 'launch plan',
			'user_tracking_entity_id' => $launchplan->id,
			'user_tracking_user_id' => $user->id,
			'user_tracking_action' => $workflowState->data['lps_workflow_state_name'],
			'user_created' => $user->login,
			'date_created' => date('Y-m-d H:i:s')
		));
	}
}
else {
	$response = false;
	$message = "Launch plan not found.";
}

header('Content-Type: text/json');
echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'reload' => $reload
));