<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];
$sendmail = $_REQUEST['sendmail'];
$revisions = $_REQUEST['revision'];

$_STATE_REVISION = 6;

$model = new Model($application);

$launchplan = new Modul($application);
$launchplan->setTable('lps_launchplans');
$launchplan->read($id);

if ($launchplan->id) {

	if ($revisions ) {

		$sth = $model->db->prepare("
			UPDATE lps_launchplan_items SET
				lps_launchplan_item_status = 1,
				user_modified = ?,
				date_modified = NOW()
			WHERE lps_launchplan_item_id = ?
		");

		// set order sheet items in revision
		foreach ($revisions as $item => $value) {
			$sth->execute(array($user->login, $item));
		}

		// set order sheet in revision
		$sth = $model->db->prepare("
			UPDATE lps_launchplans SET
				lps_launchplan_workflowstate_id = ?,
				user_modified = ?
			WHERE lps_launchplan_id = ?
		");

		$response = $sth->execute(array($_STATE_REVISION, $user->login, $launchplan->id));

		if ($response) {
			
			$track = true;
			$json['response'] = true;
			$json['reload'] = true;
			$json['sendmail'] = $sendmail;
			
			Message::add("Selected Items are in Revision state.");

		} else {
			$message = 'The launch plan can not revise.';
		}

	} else {
		$json['response'] = false;
		$json['message']= "Error, select at least one Item in Revision.";
	}

	// track workflow state change
	if ($track) {
			
		$workflowState = new Modul($application);
		$workflowState->setTable('lps_workflow_states');
		$workflowState->read($_STATE_REVISION);
			
		$userTrack = new User_Tracking($application);
		$userTrack->create(array(
			'user_tracking_entity' => 'launch plan',
			'user_tracking_entity_id' => $launchplan->id,
			'user_tracking_user_id' => $user->id,
			'user_tracking_action' => $workflowState->data['lps_workflow_state_name'],
			'user_created' => $user->login,
			'date_created' => date('Y-m-d H:i:s')
		));
	}
}
else {
	$json['response'] = false;
	$json['message']= "The launch plan not found.";
}

header('Content-Type: text/json');
echo json_encode($json);
