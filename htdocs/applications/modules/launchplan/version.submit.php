<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

define(DEBUGGING_MODE, false);

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];

// request data
$_ID = $_REQUEST['id'];
$_TITLE = $_REQUEST['title'];

$_ERRORS = array();
$_MESSAGES = array();
$_DATA = array();

// db model
$model = new Model($application);

// ordersheet
$launchplan = new Modul($application);
$launchplan->setTable('lps_launchplans');
$launchplan->read($_ID);

if (!$launchplan->id) {
	$_ERRORS[] = "The launch plan not found.";
}

if (!$_TITLE) {
	$_ERRORS[] = "Version title is empty.";
}

$_DATA['launchplan'] = $launchplan->data;


// create launch plan version ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && $_TITLE) {

	$sth = $model->db->prepare("
		INSERT INTO lps_launchplan_versions (
			lps_launchplan_version_launchplan_id,
			lps_launchplan_version_workflowstate_id,
			lps_launchplan_version_address_id,
			lps_launchplan_version_customer_number,
			lps_launchplan_version_mastersheet_id,
			lps_launchplan_version_title,
			lps_launchplan_version_openingdate,
			lps_launchplan_version_closingdate,
			lps_launchplan_version_comment,
			user_created,
			date_created
		)
		VALUES (?,?,?,?,?,?,?,?,?,?,NOW())
	");
	
	// add order sheet version
	if (!DEBUGGING_MODE) {
		
		$response = $sth->execute(array(
			$launchplan->id,
			$launchplan->data['lps_launchplan_workflowstate_id'],
			$launchplan->data['lps_launchplan_address_id'],
			$launchplan->data['lps_launchplan_customer_number'],
			$launchplan->data['lps_launchplan_mastersheet_id'],
			$_TITLE,
			$launchplan->data['lps_launchplan_openingdate'],
			$launchplan->data['lps_launchplan_closingdate'],
			$launchplan->data['lps_launchplan_comment'],
			$user->login
		));

		$_VERSION = $model->db->lastInsertId();

	} else $_VERSION = true;

	if ($_VERSION) $_MESSAGES[] = "The launch plan version $_VERSION is created";
	else $_ERRORS[] = "The launch plan version cannot be created, check Database query.";
}

// get launch plan items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEMS = array();

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT *
		FROM lps_launchplan_items
		WHERE lps_launchplan_item_launchplan_id = ?
	");

	$sth->execute(array($launchplan->id));
	$result = $sth->fetchAll();

	$_ITEMS = _array::datagrid($result);

	if ($result) $_MESSAGES[] = "Found total launch plan items: ".count($_ITEMS); 
	else $_ERRORS[] = "Launch plan items not found";
}

$_DATA['items'] = $_ITEMS;


// get launch plan item week quantities ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEMS_WEEK_QUANTITIES = array();

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT DISTINCT
			lps_launchplan_item_week_quantity_id AS id,
			lps_launchplan_item_week_quantity_item_id AS item,
			lps_launchplan_item_week_quantity_week AS week,
			lps_launchplan_item_week_quantity_quantity AS quantity,
			lps_launchplan_item_week_quantity_quantity_approved AS approved
		FROM lps_launchplan_item_week_quantities
		INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_week_quantity_item_id
		WHERE lps_launchplan_item_launchplan_id = ?
	");

	$sth->execute(array($launchplan->id));
	$result = $sth->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$id = $row['id'];
			$item = $row['item'];
			$_ITEMS_WEEK_QUANTITIES[$item][$id]['week'] = $row['week'];
			$_ITEMS_WEEK_QUANTITIES[$item][$id]['quantity'] = $row['quantity'];
			$_ITEMS_WEEK_QUANTITIES[$item][$id]['approved'] = $row['approved'];
		}
	}

	if ($_ITEMS_WEEK_QUANTITIES) {
		$_MESSAGES[] = "Found total launch plan week data items: ".count($result); 
	}
}

$_DATA['weeks'] = $_ITEMS_WEEK_QUANTITIES;


// create launch plan items version ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && $_ITEMS) {

	// add launch plan item statement
	$sth = $model->db->prepare("
		INSERT INTO lps_launchplan_version_items (
			lps_launchplan_version_item_version_id,
			lps_launchplan_version_item_reference_id,
			lps_launchplan_version_item_price,
			lps_launchplan_version_item_currency,
			lps_launchplan_version_item_exchangrate,
			lps_launchplan_version_item_factor,
			lps_launchplan_version_item_quantity,
			lps_launchplan_version_item_quantity_estimate,
			lps_launchplan_version_item_quantity_approved,
			lps_launchplan_version_item_status,
			lps_launchplan_version_item_order_date,
			lps_launchplan_version_item_customernumber,
			lps_launchplan_version_item_comment,
			user_created,
			date_created
		)
		VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW())
	");

	// add launch plan item quantity statement
	$sthWeek = $model->db->prepare("
		INSERT INTO lps_launchplan_version_item_week_quantities (
			lps_launchplan_version_item_week_quantity_item_id,
			lps_launchplan_version_item_week_quantity_week,
			lps_launchplan_version_item_week_quantity_quantity,
			lps_launchplan_version_item_week_quantity_approved,
			user_created,
			date_created
		)
		VALUES (?,?,?,?,?,NOW())
	");

	$inserted = 0;

	foreach ($_ITEMS as $id => $item) {

		$data = array(
			$_VERSION,
			$item['lps_launchplan_item_reference_id'],
			$item['lps_launchplan_item_price'],
			$item['lps_launchplan_item_currency'],
			$item['lps_launchplan_item_exchangrate'],
			$item['lps_launchplan_item_factor'],
			$item['lps_launchplan_item_quantity'],
			$item['lps_launchplan_item_quantity_estimate'],
			$item['lps_launchplan_item_quantity_approved'],
			$item['lps_launchplan_item_status'],
			$item['lps_launchplan_item_order_date'],
			$item['lps_launchplan_item_customernumber'],
			$item['lps_launchplan_item_comment'],
			$user->login
		);

		if (!DEBUGGING_MODE) {
			$response = $sth->execute($data);
		} else $response = true;

		if ($response) {

			if (!DEBUGGING_MODE) $inserted = $model->db->lastInsertId();
			else $inserted++;

			$_MESSAGES[] = "Insert launch plan item $inserted";

			// add item week quantities
			if ($_ITEMS_WEEK_QUANTITIES[$id] && $inserted) {
				
				foreach ($_ITEMS_WEEK_QUANTITIES[$id] as $row) {
					
					if (!DEBUGGING_MODE) {
						$response = $sthWeek->execute(array(
							$inserted,
							$row['week'],
							$row['quantity'],
							$row['approved'],
							$user->login
						));
					} else $response = true;

					if ($response) $_MESSAGES[] = "Insert version quantity {$row[quantity]} for week {$row[week]} ";
					else $_ERRORS[] = "Cannot insert week quantity for item $inserted";
				}
			}

		} else {
			$_ERRORS[] = "The launch plan item for reference {$item[lps_launchplan_item_reference_id]} is not inserted";
		}
	}
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (DEBUGGING_MODE) {
	echo "<pre>";
	print_r($_ERRORS);
	print_r($_MESSAGES);
	print_r($_DATA);
	echo "</pre>";
} else {
	
	if (!$_ERRORS) {
		$response = true;
		Message::ordersheet_version_created();
	}
	else {
		$response = false;
		Message::request_failure();
	}

	url::redirect("/$application/$controller/$action/$_ID");
}
