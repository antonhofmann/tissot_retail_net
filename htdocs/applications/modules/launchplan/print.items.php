<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

// execution time
ini_set('max_execution_time', 120);
ini_set('memory_limit', '1024M');

define(DEBUGGING, true);

$settings = Settings::init();
$settings->load('data');
$user = User::instance();
$translate = Translate::instance();

// request :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

// launchplan initialisation::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// model
$model = new Model($application);

// launchplan
$launchplan = new Modul($application);
$launchplan->setTable('lps_launchplans');
$launchplan->read($id);

// master sheet
$mastersheet = new Modul($application);
$mastersheet->setTable('lps_mastersheets');
$mastersheet->read($launchplan->data['lps_launchplan_mastersheet_id']);

$_MASTERSHEET_FIRST_WEEK = $mastersheet->data['lps_mastersheet_week_number_first'];
$_MASTERSHEET_TOTAL_WEEKS = $mastersheet->data['lps_mastersheet_weeks'];
$_MASTERSHEET_ESTIMATE_MONTHS = $mastersheet->data['lps_mastersheet_estimate_month'];

// company
$company = new Company();
$company->read($launchplan->data['lps_launchplan_address_id']);

// workflow state
$workflowState = new Modul($application);
$workflowState->setTable('lps_workflow_states');
$workflowState->read($launchplan->data['lps_launchplan_workflowstate_id']);


// spreadsheet  initialisation :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// sheet caption
$_SHEET_CAPTION = ($archived) ? 'Archived Launch Plan Items' : 'Launch Plan Items';

// page title
$_PAGE_TITLE = $company->company.', '.$mastersheet->data['lps_mastersheet_name'].', '.$mastersheet->data['lps_mastersheet_year'];

// output file name
$_FILENAME  = "launchplan_";
$_FILENAME .= str_replace(' ', '_', strtolower($company->company));
$_FILENAME .= str_replace(' ', '_', strtolower($mastersheet->data['lps_mastersheet_name']));
$_FILENAME .= "-".date('Y-m-d');


// state handler :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$state = new State($launchplan->data['lps_launchplan_workflowstate_id'], $application);
$state->setOwner($launchplan->data['lps_launchplan_address_id']);
$state->setDateIntervals($launchplan->data['lps_launchplan_openingdate'], $launchplan->data['lps_launchplan_closingdate']);

// show approvment
if ($state->canAdministrate()) $_SHOW_APPROVED_COLUMN = true;
else $_SHOW_APPROVED_COLUMN = $state->state >= 5 == !$state->isRevision() ? true : false;


// launchplan items ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEMS = array();

$sth = $model->db->prepare("
	SELECT DISTINCT
		lps_launchplan_item_id AS id,
		lps_launchplan_item_price AS price, 
		currency_symbol AS currency_symbol, 
		lps_launchplan_item_exchangrate AS currency_exchangrate, 
		lps_launchplan_item_factor AS currency_factor, 
		lps_launchplan_item_quantity AS quantity, 
		lps_launchplan_item_quantity_proposed AS quantity_proposed, 
		lps_launchplan_item_quantity_approved AS quantity_approved, 
		lps_launchplan_item_quantity_estimate AS quantity_estimate, 
		lps_launchplan_item_status AS status, 
		lps_launchplan_item_comment AS reference_description,
		lps_reference_code AS reference_code, 
		lps_reference_name AS reference_name, 
		lps_collection_code AS collection_code,
		lps_collection_category_code AS collection_category,
		lps_mastersheet_estimate_month AS estimate_month,							
		lps_product_group_id AS group_id, 
		CONCAT(lps_product_group_code, ', ', lps_product_group_name) AS group_name, 
		lps_collection_category_id AS subgroup_id,
		lps_collection_category_code AS subgroup_name
	FROM lps_launchplan_items INNER JOIN lps_references ON lps_launchplan_items.lps_launchplan_item_reference_id = lps_references.lps_reference_id
	INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
	INNER JOIN lps_mastersheets ON lps_mastersheet_id = lps_launchplan_mastersheet_id
	INNER JOIN lps_product_groups ON lps_references.lps_reference_product_group_id = lps_product_groups.lps_product_group_id
	INNER JOIN lps_collections ON lps_references.lps_reference_collection_id = lps_collections.lps_collection_id
	INNER JOIN lps_collection_categories ON lps_references.lps_reference_collection_category_id = lps_collection_categories.lps_collection_category_id
	INNER JOIN db_retailnet.currencies ON currency_id = lps_launchplan_item_currency
	WHERE lps_launchplan_item_launchplan_id = ?
	ORDER BY 
		group_name,
		lps_collection_category_code,
		lps_collection_code,
		lps_reference_code
");

$sth->execute(array($launchplan->id));
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$group = $row['group_id'];
		$item = $row['id'];	
		
		$_ITEMS[$group]['caption'] = $row['group_name'];
		$_ITEMS[$group]['data'][$item]['collection_code'] = $row['collection_code'];
		$_ITEMS[$group]['data'][$item]['collection_category'] = $row['collection_category'];
		$_ITEMS[$group]['data'][$item]['reference_code'] = $row['reference_code'];
		$_ITEMS[$group]['data'][$item]['reference_name'] = $row['reference_name'];
		$_ITEMS[$group]['data'][$item]['reference_description'] = $row['reference_description'];
		$_ITEMS[$group]['data'][$item]['estimate_month'] = $row['estimate_month'];
		$_ITEMS[$group]['data'][$item]['price'] = $row['price'];
		$_ITEMS[$group]['data'][$item]['currency_symbol'] = $row['currency_symbol'];
		$_ITEMS[$group]['data'][$item]['currency_exchangrate'] = $row['currency_exchangrate'];
		$_ITEMS[$group]['data'][$item]['currency_factor'] = $row['currency_factor'];
		$_ITEMS[$group]['data'][$item]['quantity'] = $row['quantity'];
		$_ITEMS[$group]['data'][$item]['quantity_estimate'] = $row['quantity_estimate'];
		$_ITEMS[$group]['data'][$item]['quantity_approved'] = $row['quantity_approved'];
	}
}

// item week quantities ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEM_WEEK_QUANTITIES = array();
$_TOTAL_ITEM_WEEK_QUANTITIES = array();
	
$sth = $model->db->prepare("
	SELECT DISTINCT
		lps_launchplan_item_week_quantity_id AS id,
		lps_launchplan_item_week_quantity_item_id AS item,
		lps_launchplan_item_week_quantity_week AS week,
		lps_launchplan_item_week_quantity_quantity AS quantity,
		lps_launchplan_item_week_quantity_quantity_approved AS quantity_approved
	FROM lps_launchplan_item_week_quantities
	INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_week_quantity_item_id
	WHERE lps_launchplan_item_launchplan_id = ?
");

$sth->execute(array($launchplan->id));
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$item = $row['item'];
		$week = $row['week'];
		
		$_ITEM_WEEK_QUANTITIES[$item][$week] = array(
			'id' => $row['id'],
			'quantity' => $row['quantity'],
			'quantity_approved' => $row['quantity_approved']
		);

		$_TOTAL_ITEM_WEEK_QUANTITIES[$item] += $row['quantity'];
	}
}


// item planned quantities :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEM_PLANNED_QUANTITIES = array();

$sth = $model->db->prepare("
	SELECT 
		lps_launchplan_item_planned_quantity_item_id AS item,
		SUM(lps_launchplan_item_planned_quantity_quantity) AS total_quantities
	FROM lps_launchplan_item_planned_quantities
	INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_planned_quantity_item_id
	WHERE lps_launchplan_item_launchplan_id = ?
	GROUP BY lps_launchplan_item_planned_quantity_item_id
");

$sth->execute(array($launchplan->id));
$result = $sth->fetchAll();

$_ITEM_PLANNED_QUANTITIES = _array::extract($result);


// spreadsheet references ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


$_FIRST_FEXED_ROW = 1;
$_FIRST_FIXED_COL = 1;
$_TOTAL_FIXED_ROWS = 1;
$_TOTAL_FIXED_COLS = 10;

// datagrid
$_DATAGRID = array();

if ($_ITEMS) {

	$row=$_TOTAL_FIXED_ROWS;

	foreach ($_ITEMS as $g => $group) {

		$row++;
		$rowGroups[$row] = true;
		
		$col=$_FIRST_FIXED_COL;
		$_DATAGRID[$row][$col]['caption'] = $group['caption'];
		$_DATAGRID[$row][$col]['group'] = $g;

		$row++;
		$rowGroupHeaders[$row] = true;

		$col=$_FIRST_FIXED_COL;
		$_DATAGRID[$row][$col]['caption'] = 'Collection Code';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 20;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Reference';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 20;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Name';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 30;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Collection Category';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 30;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Comment';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 20;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Estimated Price';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 20;
		$_DATAGRID[$row][$col]['numeric'] = true;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Currency';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 10;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = "Estimate for $_MASTERSHEET_ESTIMATE_MONTHS months";
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 25;
		$_DATAGRID[$row][$col]['numeric'] = true;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Total Planned';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 15;
		$_DATAGRID[$row][$col]['numeric'] = true;

		$col++;
		$_DATAGRID[$row][$col]['caption'] = '% of Estimate';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 20;
		$_DATAGRID[$row][$col]['numeric'] = true;

		for ($i=0; $i < $_MASTERSHEET_TOTAL_WEEKS; $i++) { 
			$col++;
			$week = $_MASTERSHEET_FIRST_WEEK+$i;
			$_DATAGRID[$row][$col]['caption'] = "Week $week";
			$_DATAGRID[$row][$col]['header'] = true;
			$_DATAGRID[$row][$col]['numeric'] = true;
		}

		$col++;
		$_DATAGRID[$row][$col]['caption'] = 'Total Launch';
		$_DATAGRID[$row][$col]['header'] = true;
		$_DATAGRID[$row][$col]['width'] = 15;
		$_DATAGRID[$row][$col]['numeric'] = true;

		if ($_SHOW_APPROVED_COLUMN) {
			
			for ($i=0; $i < $_MASTERSHEET_TOTAL_WEEKS; $i++) { 
				$col++;
				$week = $_MASTERSHEET_FIRST_WEEK+$i;
				$_DATAGRID[$row][$col]['caption'] = "Week $week";
				$_DATAGRID[$row][$col]['header'] = true;
				$_DATAGRID[$row][$col]['numeric'] = true;
			}

			$col++;
			$_DATAGRID[$row][$col]['caption'] = 'Total Approved';
			$_DATAGRID[$row][$col]['header'] = true;
			$_DATAGRID[$row][$col]['width'] = 15;
			$_DATAGRID[$row][$col]['numeric'] = true;
		}

		// total datagrid columns
		$_TOTAL_GRID_COLS = $col;
			
		foreach ($group['data'] as $key => $item) {

			$row++;
			$rowGrid[$row] = true;
			
			$col=$_FIRST_FIXED_COL;
			$_DATAGRID[$row][$col]['value'] = $item['collection_code'];
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['reference_code'];
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['reference_name'];		

			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['collection_category'];			

			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['reference_description'];
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['price'];
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['currency_symbol'];
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $item['quantity_estimate'];
			$_DATAGRID[$row][$col]['item'] = $key;
			
			$col++;
			$_DATAGRID[$row][$col]['value'] = $_ITEM_PLANNED_QUANTITIES[$key];
			
			$col++;
			$totalWeekQuantities = $_TOTAL_ITEM_WEEK_QUANTITIES[$key];
			$value = $item['estimate_month'] ? ($totalWeekQuantities/$item['estimate_month'])*100 : null;
			$_DATAGRID[$row][$col]['value'] = $value ? number_format($value, 2, '.', '') : null;

			$totalQuantity = null;

			for ($i=0; $i < $_MASTERSHEET_TOTAL_WEEKS; $i++) { 
				
				$col++;
				$week = $_MASTERSHEET_FIRST_WEEK+$i;
				
				$_DATAGRID[$row][$col]['item'] = $key;
				$_DATAGRID[$row][$col]['week'] = $week;
				$_DATAGRID[$row][$col]['id'] = $_ITEM_WEEK_QUANTITIES[$key][$week]['id'];
				$_DATAGRID[$row][$col]['value'] = $_ITEM_WEEK_QUANTITIES[$key][$week]['quantity'];

				$totalQuantity += $_ITEM_WEEK_QUANTITIES[$key][$week]['quantity'];
			}

			// col total quantities
			$col++;
			$_DATAGRID[$row][$col]['value'] = $totalQuantity;

			if ($_SHOW_APPROVED_COLUMN) {
				
				for ($i=0; $i < $_MASTERSHEET_TOTAL_WEEKS; $i++) { 
					
					$col++;
					$week = $_MASTERSHEET_FIRST_WEEK+$i;
					
					$_DATAGRID[$row][$col]['item'] = $key;
					$_DATAGRID[$row][$col]['week'] = $week;
					$_DATAGRID[$row][$col]['id'] = $_ITEM_WEEK_QUANTITIES[$key][$week]['id'];
					$_DATAGRID[$row][$col]['value'] = $_ITEM_WEEK_QUANTITIES[$key][$week]['quantity_approved'];
				}

				// col total approved quantities
				$col++;
				$_DATAGRID[$row][$col]['value'] = null;
			}
		}

		$row++;
		$rowGroupSubtotals[$row] = true;

		$col=$_FIRST_FIXED_COL;
		$_DATAGRID[$row][$col]['caption'] = 'Total '.$group['caption'];

		$row++;
		$_ROW_GROUP_SEPARATORS[$row] = true;

		$col=$_FIRST_FIXED_COL;
		$_DATAGRID[$row][$col]['separator'] = true;
	}

	$row++;
	$_ROW_GROUP_SEPARATORS[$row] = true;

	// grand total
	$row++;

	$_ROW_GRAND_TOTAL = $row;
	$_DATAGRID[$row][$_FIRST_FIXED_COL]['caption'] = 'Launch Plan Totals';
	$_TOTAL_GRID_ROWS = $row-1;
}


// spreadsheet styles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_STYLES = array(

	'allborders' => array(
		'borders' => array( 
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	'number' => array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
		)
	),
	'string' => array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
		)
	),
	'total-group' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12, 
			'bold'=>true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'f3f3f3')
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	'total-sheet' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 14, 
			'bold'=>true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'dedede')
		),
		'alignment' => array(
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	'group-header' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 16, 
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		)
	),
	'table-header' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 11, 
			'bold'=>true,
			'color' => array('rgb'=>'FFFFFF')
		),
		'alignment' => array(
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'000000')
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	'sheet-caption' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 20, 
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		)
	),
	'input' => array(
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'FFFF90')
		),
	)
);


// spreadsheet triggers ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_FIRST_WEEK_COL = $_TOTAL_FIXED_COLS + 1;
$_LAST_WEEK_COL = $_FIRST_WEEK_COL + $_MASTERSHEET_TOTAL_WEEKS - 1;
$_TOTAL_WEEK_COLS = $_LAST_WEEK_COL + 1;
$_FIRST_WEEK_APPROVED_COL = $_LAST_WEEK_COL + 2;
$_LAST_WEEK_APPROVED_COL = $_FIRST_WEEK_APPROVED_COL + $_MASTERSHEET_TOTAL_WEEKS - 1;
$_TOTAL_WEEK_APPROVED_COLS = $_LAST_WEEK_APPROVED_COL + 1;
$_COL_ESTIMATE_QUANTITY = $_FIRST_WEEK_COL - 3;
$_COL_PLANNED_QUANTITY = $_FIRST_WEEK_COL - 2;
$_COL_ESTIMATE_QUOTE = $_FIRST_WEEK_COL - 1;
$_TOTAL_SPREADSHEET_ROWS = $_TOTAL_FIXED_ROWS + $_TOTAL_GRID_ROWS;
$_TOTAL_SPREADSHEET_COLS = $_TOTAL_GRID_COLS;


if ($_TOTAL_SPREADSHEET_ROWS && $_TOTAL_SPREADSHEET_COLS) {

	// excel init
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name)->setTitle($_SHEET_CAPTION);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

	// sheet columns
	$_FIRST_COL_INDEX = spreadsheet::key($_FIRST_FIXED_COL);
	$_LAST_COL_INDEX = spreadsheet::key($_TOTAL_SPREADSHEET_COLS);

	for ($row=1; $row <= $_TOTAL_SPREADSHEET_ROWS; $row++) {
			
		for ($col=1; $col <= $_TOTAL_SPREADSHEET_COLS; $col++) {

			$_COL_INDEX = spreadsheet::key($col);
			$index = "$_COL_INDEX$row";
			$rowRange = $_FIRST_COL_INDEX.$row.":".$_LAST_COL_INDEX.$row;

			switch ($row) {
				
				// sheet title
				case $_FIRST_FIXED_COL;		

					if ($col==1) {
						$objPHPExcel->getActiveSheet()->mergeCells($rowRange);
						$objPHPExcel->getActiveSheet()->setCellValue($index, $_PAGE_TITLE);
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['sheet-caption']);
						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(50);
					}

				break;

				// filters
				case $rowFilterCaptions:

				break;

				// group title: collection
				case $rowGroups[$row]:

					if ($col==$_FIRST_FIXED_COL) {
						$group = $_DATAGRID[$row][$col]['group'];
						$objPHPExcel->getActiveSheet()->mergeCells($rowRange);
						$objPHPExcel->getActiveSheet()->setCellValue($index, $_DATAGRID[$row][$col]['caption']);
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['group-header']);
						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(40);
					}

				break;
			
				// group table header
				case $rowGroupHeaders[$row]:
					
					$objPHPExcel->getActiveSheet()->setCellValue($index, $_DATAGRID[$row][$col]['caption']);

					if ($_DATAGRID[$row][$col]['width']) {
						$objPHPExcel->getActiveSheet()->getColumnDimension($_COL_INDEX)->setWidth($_DATAGRID[$row][$col]['width']);
					}

					if ($col==$_TOTAL_SPREADSHEET_COLS) {
						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
						$objPHPExcel->getActiveSheet()->getStyle($rowRange)->applyFromArray($_STYLES['table-header'], false);
					}

					if ($col==$_COL_ESTIMATE_QUOTE) {
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']);
					}

				break;

				// lanchplan items
				case $rowGrid[$row]:

					$value = $_DATAGRID[$row][$col]['value'];
					
					switch ($col) {

						// estimated quantity
						case $_COL_ESTIMATE_QUANTITY:
							
							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
							
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']+$_STYLES['input']);

							$colsum['estimate-quantity'][$col][$group] += $value;

						break;

						// total planned 
						case $_COL_PLANNED_QUANTITY:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
							$colsum['planned-quantity'][$col][$group] += $value;
						break;

						// estimated quote
						case $_COL_ESTIMATE_QUOTE:
							
							$plannedQuantity  = $_DATAGRID[$row][$_TOTAL_WEEK_COLS] ? $_DATAGRID[$row][$_TOTAL_WEEK_COLS]['value'] : false;
							$estimateQuantity = $_DATAGRID[$row][$col-2] ? $_DATAGRID[$row][$col-2]['value'] : false;
							
							$quantity = $plannedQuantity>0 && $estimateQuantity>0 ? ($plannedQuantity/$estimateQuantity)*100 : 0;
							
							if ($quantity) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']);
							}
							
							$colsum['estimate-quote'][$col][$group] += $quantity;

						break;
						
						// week quantity
						case $col >= $_FIRST_WEEK_COL && $col <= $_LAST_WEEK_COL:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']+$_STYLES['input']);
							$rowsum['week-planned'][$row] += $value;
							$colsum['week-quantity'][$col][$group] += $value;
						break;

						// total week planned quantites
						case $_TOTAL_WEEK_COLS:

							if ($rowsum['week-planned'][$row]) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $rowsum['week-planned'][$row]);
							}

							$colsum['week-quantity-total'][$col][$group] += $rowsum['week-planned'][$row];

						break;

						// week approved quantity
						case $col >= $_FIRST_WEEK_APPROVED_COL && $col <= $_LAST_WEEK_APPROVED_COL:
							
							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']+$_STYLES['input']);
							
							$rowsum['week-approved'][$row] += $value;
							$colsum['week-approved'][$col][$group] += $value;

						break;

						// total week approved quantites
						case $_TOTAL_WEEK_APPROVED_COLS: 
							
							if ($rowsum['week-approved'][$row]) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $rowsum['week-approved'][$row]);
							}

							$objPHPExcel->getActiveSheet()->getStyle($rowRange)->applyFromArray($_STYLES['allborders'], false);
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
							
							$colsum['week-approved-total'][$col][$group] += $rowsum['week-approved'][$row];

						break;

						default:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
						break;
					}
					
				break;

				// collection subtotal
				case $rowGroupSubtotals[$row]:

					switch ($col) {
						
						// subtotal caption
						case $_FIRST_FIXED_COL:
							$range = "$index:".spreadsheet::key($col+5,$row);
							$objPHPExcel->getActiveSheet()->mergeCells($range);
							$objPHPExcel->getActiveSheet()->setCellValue($index, $_DATAGRID[$row][$col]['caption']);
						break;
						
						// estimated quantity
						case $_COL_ESTIMATE_QUANTITY:

							if ($colsum['estimate-quantity'][$col][$group]) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['estimate-quantity'][$col][$group]);
							}

							$colsum['grand-estimate-quantity'][$col] += $colsum['estimate-quantity'][$col][$group];

						break;
						
						// planned quantity
						case $_COL_PLANNED_QUANTITY:

							if ($colsum['planned-quantity'][$col][$group]) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['planned-quantity'][$col][$group]);
							}

							$colsum['grand-planned-quantity'][$col] += $colsum['planned-quantity'][$col][$group];

						break;

						// estimated quote
						case $_COL_ESTIMATE_QUOTE:

							$plannedQuantity  = $colsum['week-quantity-total'][$_TOTAL_WEEK_COLS][$group];
							$estimateQuantity = $colsum['estimate-quantity'][$col-2][$group];

							$quantity = $plannedQuantity>0 && $estimateQuantity>0 ? ($plannedQuantity/$estimateQuantity)*100 : 0;
							
							if ($quantity) {
								$quantity = number_format($quantity, 2, ".", "'");
								$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
							}

							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']);

						break;

						// week quantity
						case $col >= $_FIRST_WEEK_COL && $col <= $_LAST_WEEK_COL:

							if ($colsum['week-quantity'][$col][$group]) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['week-quantity'][$col][$group]);
							}
							
							$colsum['grand-week-quantity'][$col] += $colsum['week-quantity'][$col][$group];

						break;

						// total week planned quantites
						case $_TOTAL_WEEK_COLS:

							if ($colsum['week-quantity-total'][$col][$group]) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['week-quantity-total'][$col][$group]);
							}

							$colsum['grand-week-quantity-total'][$col] += $colsum['week-quantity-total'][$col][$group];

						break;

						// week approved quantity
						case $col >= $_FIRST_WEEK_APPROVED_COL && $col <= $_LAST_WEEK_APPROVED_COL:

							if ($colsum['week-approved'][$col][$group]) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['week-approved'][$col][$group]);
							}

							$colsum['grand-week-approved'][$col] += $colsum['week-approved'][$col][$group];

						break;

						// total week approved quantites
						case $_TOTAL_WEEK_APPROVED_COLS:
							
							if ($colsum['week-approved-total'][$col][$group]) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['week-approved-total'][$col][$group]);
							}
							
							$objPHPExcel->getActiveSheet()->getStyle($rowRange)->applyFromArray($_STYLES['total-group'], false);
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
							
							$colsum['grand-week-approved-total'][$col] += $colsum['week-approved-total'][$col][$group];

						break;
					}

				break;

				case $_ROW_GRAND_TOTAL:
					
					switch ($col) {

						// grand total caption
						case $_FIRST_FIXED_COL:
							$range = "$index:".spreadsheet::key($col+5,$row);
							$objPHPExcel->getActiveSheet()->mergeCells($range);
							$objPHPExcel->getActiveSheet()->setCellValue($index, $_DATAGRID[$row][$col]['caption']);
						break;
						
						// estimated quantity
						case $_COL_ESTIMATE_QUANTITY:

							if ($colsum['grand-estimate-quantity'][$col]) {
								$objPHPExcel->getActiveSheet()->setCellValue($index,  $colsum['grand-estimate-quantity'][$col]);
							}

						break;

						case $_COL_PLANNED_QUANTITY:
							
							if ($colsum['grand-planned-quantity'][$col]) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['grand-planned-quantity'][$col]);
							}

						break;

						// estimated quote
						case $_COL_ESTIMATE_QUOTE:
							
							$plannedQuantity  = $colsum['grand-week-quantity-total'][$_TOTAL_WEEK_COLS];
							$estimateQuantity = $colsum['grand-estimate-quantity'][$col-2];
							
							$quantity = $plannedQuantity>0 && $estimateQuantity>0 ? ($plannedQuantity/$estimateQuantity) * 100 : 0;
							
							if ($quantity) {
								$quantity = number_format($quantity, 2, ".", "'");
								$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
							}

							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($_STYLES['number']);

						break;
						
						// week quantity
						case $col >= $_FIRST_WEEK_COL && $col <= $_LAST_WEEK_COL:

							if ($colsum['grand-week-quantity'][$col]) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['grand-week-quantity'][$col]);
							}

						break;

						// total week planned quantites
						case $_TOTAL_WEEK_COLS:

							if ($colsum['grand-week-quantity-total'][$col]) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['grand-week-quantity-total'][$col]);
							}

						break;

						// week approved quantity
						case $col >= $_FIRST_WEEK_APPROVED_COL && $col <= $_LAST_WEEK_APPROVED_COL:

							if ($colsum['grand-week-approved'][$col]) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['grand-week-approved'][$col]);
							}

						break;

						// total week approved quantites
						case $_TOTAL_WEEK_APPROVED_COLS: 

							if ($colsum['grand-week-approved-total'][$col]) {
								$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['grand-week-approved-total'][$col]);
							}

							$objPHPExcel->getActiveSheet()->getStyle($rowRange)->applyFromArray($_STYLES['total-sheet'], false);
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);

						break;
					}

				break;

				// collection separators
				case $_ROW_GROUP_SEPARATORS[$row]:
					
					if ($col==1) {
						$objPHPExcel->getActiveSheet()->mergeCells($rowRange);
					}

				break;
			}
		}
	}	

	$objPHPExcel->getActiveSheet()->setTitle($_SHEET_CAPTION);
	$objPHPExcel->setActiveSheetIndex(0);

	// redirect output to client browser
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$_FILENAME.'.xlsx"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
}
else {
	message::empty_result();
	url::redirect("/$application/$controller$archived/$action/$id");
}
