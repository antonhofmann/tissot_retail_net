<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];

$section = $_REQUEST['section'];		// section name
$item = $_REQUEST['item'];				// item_id
$entity = $_REQUEST['entity'];			// pos_id or warehouse_id
$reference = $_REQUEST['reference'];	// reference_id
$id = $_REQUEST['id'];					// planning_id or distribution_id

$quantity = is_numeric($_REQUEST['quantity']) ? $_REQUEST['quantity'] : null;

$model = new Model($application);
	
// reference
$sth = $model->db->prepare("
	SELECT 
		lps_launchplan_item_id AS id
	FROM lps_launchplan_items
	WHERE lps_launchplan_item_id = ?
");

$sth->execute(array($item));
$data = $sth->fetch();

if ($data['id']) {
	
	switch ($section) {
	
		case 'planning-pos':
			
			if ($quantity) {	
				
				if ($id) {
					
					$sth = $model->db->prepare("
						UPDATE lps_launchplan_item_planned_quantities SET
							lps_launchplan_item_planned_quantity_quantity = ?,
							user_modified = ?,
							date_modified = NOW()
						WHERE lps_launchplan_item_planned_quantity_id = ?
					");
					
					$response = $sth->execute(array($quantity, $user->login, $id));

				} else {
					
					$sth = $model->db->prepare("
						INSERT INTO lps_launchplan_item_planned_quantities (
							lps_launchplan_item_planned_quantity_item_id,
							lps_launchplan_item_planned_quantity_posaddress_id,
							lps_launchplan_item_planned_quantity_quantity,
							user_created,
							date_created
						)
						VALUES (?,?,?,?,NOW())
					");
					
					$response = $sth->execute(array($item, $entity, $quantity, $user->login));

					$id = $model->db->lastInsertId();
				}
			}
			else {
				
				$sth = $model->db->prepare("
					DELETE FROM lps_launchplan_item_planned_quantities
					WHERE lps_launchplan_item_planned_quantity_id = ?
				");
				
				$response = $sth->execute(array($id));
				
				unset($id);
			}
		
		break;
	
		case 'planning-warehouse':
			
			if ($quantity) {	
				
				if ($id) {
					
					$sth = $model->db->prepare("
						UPDATE lps_launchplan_item_planned_quantities SET
							lps_launchplan_item_planned_quantity_quantity = ?,
							user_modified = ?,
							date_modified = NOW()
						WHERE lps_launchplan_item_planned_quantity_id = ?
					");
					
					$response = $sth->execute(array($quantity, $user->login, $id));

				} else {
					
					$sth = $model->db->prepare("
						INSERT INTO lps_launchplan_item_planned_quantities (
							lps_launchplan_item_planned_quantity_item_id,
							lps_launchplan_item_planned_quantity_warehouse_id,
							lps_launchplan_item_planned_quantity_quantity,
							user_created,
							date_created
						)
						VALUES (?,?,?,?,NOW())
					");
					
					$response = $sth->execute(array($item, $entity, $quantity, $user->login));

					$id = $model->db->lastInsertId();
				}
			}
			else {
				
				$sth = $model->db->prepare("
					DELETE FROM lps_launchplan_item_planned_quantities
					WHERE lps_launchplan_item_planned_quantity_id = ?
				");

				$response = $sth->execute(array($id));
				
				unset($id);
			}
		
		break;

		default:
			$message = "Section not found.";
		break;
	}
}
else {
	$message = "Reference not found.";
}

// response with json header
header('Content-Type: text/json');
echo json_encode(array(
	'id' => $id,
	'response' => $response,
	'message' => $message
));
	