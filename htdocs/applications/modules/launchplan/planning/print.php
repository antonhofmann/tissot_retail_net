<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc'; 
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

$user = User::instance();
$translate = Translate::instance();
$settings = Settings::init();

define(DEBUGGIN_MODE, true);
define(COLUMN_PLANNING, 'planning');
define(COLUMN_DISTRIBUTION, 'delivered');
define(COLUMN_PARTIALY_CONFIRMED, 'partial');
define(CELL_READONLY, 'true');

$_ERRORS = array();
$_MESSAGES = array();
$_DATA = array();


// request :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['launchplan'];

$version = $_REQUEST['version'];
$_DISTRIBUTION_CHANNEL = $_REQUEST['distribution_channel'];
$_TURNOVER_CLASS_WATCH = $_REQUEST['turnoverclass_watch'];
$_SALES_REPRESENTATIVE = $_REQUEST['sale_representative'];
$_DECORATION_PERSON = $_REQUEST['decoration_person'];
$_PROVINCE = $_REQUEST['province'];

// launch plan :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$model = new Model($application);

$launchplan = new Modul($application);
$launchplan->setTable('lps_launchplans');
$launchplan->read($id);

// master sheet
$mastersheet = new Modul($application);
$mastersheet->setTable('lps_mastersheets');
$mastersheet->read($launchplan->data['lps_launchplan_mastersheet_id']);

// state handler
$currentState = $launchplan->data['lps_launchplan_workflowstate_id'];
$state = new State($currentState, $application);
$state->setOwner($launchplan->data['lps_launchplan_address_id']);
$state->setDateIntervals($launchplan->data['lps_launchplan_openingdate'], $launchplan->data['lps_launchplan_closingdate']);

// company
$company = new Company();
$company->read($launchplan->data['lps_launchplan_address_id']);

$_SHEET_CAPTION = $archived ? 'Archived Launch Plan' : 'Launch Plan';

// page title
$_PAGE_TITLE = $company->company.', '.$mastersheet->data['lps_mastersheet_name'].', '.$mastersheet->data['lps_mastersheet_year'];

// output file name
$_FILENAME  = "launchplan_";
$_FILENAME .= str_replace(' ', '_', strtolower($company->company));
$_FILENAME .= str_replace(' ', '_', strtolower($mastersheet->data['lps_mastersheet_name']));
$_FILENAME .= "-".date('Y-m-d');

// state and permissions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_STATE_EDIT = $state->canEdit(false);
$_MODE_PLANNING = in_array($currentState, array(1,2,3,4,6)) ? true : false;
$_MODE_APPROVED = !$_MODE_PLANNING;
$_MODE_EXPORTED = in_array($currentState, array(9,10,11)) ? true : false;
	

// request filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$_FILTERS = array();
$_FILTER_CAPTIONS = array();
	
// filter: pos
$_FILTERS['default'] = "(
	posaddress_client_id = $company->id 
	OR (address_parent = $company->id AND address_client_type = 3)
)";

// filter: distribution channels
if ($_DISTRIBUTION_CHANNEL) {
	
	$_FILTERS['distribution_channels'] = "posaddress_distribution_channel = $_DISTRIBUTION_CHANNEL";

	$result = $model->query("
		SELECT CONCAT(mps_distchannel_name, ' - ', mps_distchannel_code) AS caption
		FROM db_retailnet.mps_distchannels
		WHERE mps_distchannel_id = $_DISTRIBUTION_CHANNEL
	")->fetch();

	$_FILTER_CAPTIONS[] = $result['caption'];
}

// filter: turnover classes
if ($_TURNOVER_CLASS_WATCH) {
	
	$_FILTERS['turnoverclass_watches'] = "posaddress_turnoverclass_watches = $_TURNOVER_CLASS_WATCH";

	$result = $model->query("
		SELECT mps_turnoverclass_name AS caption
		FROM db_retailnet.mps_turnoverclasses
		WHERE mps_turnoverclass_id = $_TURNOVER_CLASS_WATCH
	")->fetch();

	$_FILTER_CAPTIONS[] = $result['caption'];
}

// filter: sales represenatives
if ($_SALES_REPRESENTATIVE) {
	
	$_FILTERS['sales_representative'] = "posaddress_sales_representative = $_SALES_REPRESENTATIVE";

	$result = $model->query("
		SELECT CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS caption
		FROM db_retailnet_mps.mps_staffs
		WHERE mps_staff_id = $_SALES_REPRESENTATIVE
	")->fetch();

	$_FILTER_CAPTIONS[] = $result['caption'];
}

// filter: decoration persons
if ($_DECORATION_PERSON) {

	$_FILTERS['decoration_person'] = "posaddress_decoration_person = $_DECORATION_PERSON";

	$result = $model->query("
		SELECT CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS caption
		FROM db_retailnet_mps.mps_staffs
		WHERE mps_staff_id = $_DECORATION_PERSON
	")->fetch();

	$_FILTER_CAPTIONS[] = $result['caption'];
}

// provinces
if ($_PROVINCE) {
	
	$_FILTERS['provinces'] = "province_id = $_PROVINCE";

	$result = $model->query("
		SELECT province_canton AS caption
		FROM db_retailnet.provinces
		WHERE province_id = $_PROVINCE
	")->fetch();

	$_FILTER_CAPTIONS[] = $result['caption'];
}

// for archived ordersheets show only POS locations which has distributed quantities 
if ($_MODE_EXPORTED) {
	
	// get only pos whith exported items
	$sth = $model->db->prepare("
		SELECT GROUP_CONCAT(lps_launchplan_item_planned_quantity_posaddress_id) AS pos
		FROM lps_launchplan_item_planned_quantities
		INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_planned_quantity_item_id
		WHERE lps_launchplan_item_launchplan_id = ?
	");

	$sth->execute(array($launchplan->id));
	$result = $sth->fetch();

	if ($result['pos']) {
		$_FILTERS['active'] = "posaddress_id IN ({$result[pos]})";
	} else {
		$_FILTERS['active'] = "(
			posaddress_store_closingdate = '0000-00-00' 
			OR posaddress_store_closingdate IS NULL 
			OR posaddress_store_closingdate = ''
		)";
	}

} else {
	$_FILTERS['active'] = "(
		posaddress_store_closingdate = '0000-00-00' 
		OR posaddress_store_closingdate IS NULL 
		OR posaddress_store_closingdate = ''
	)";
}


// spreadsheet styles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_STYLES = array(
	'borders' => array(
		'borders' => array( 
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	'number' => array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
		)
	),
	'string' => array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
		)
	),
	'page-title' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 18, 
			'bold'=>true,
			'color' => array('rgb'=>'000000')
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		)
	),
	'group' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12, 
			'bold'=>true,
			'color' => array('rgb'=>'ffffff')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'404040')
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		)
	),
	'quantity-label' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 11,
			'bold'=>true,
			'color' => array('rgb'=>'000000')
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		)
	),
	'category' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12,
			'bold'=>true,
			'color' => array('rgb'=>'ffffff')
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'404040')
		)
	),
	'item' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 11,
			'bold'=>true,
			'color' => array('rgb'=>'000000')
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'dedede')
		)
	),
	'pos' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 11,
			'color' => array('rgb'=>'000000')
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'ededed')
		)
	)
);
	

// data: pos locations :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_POS = array();

$result = $model->query("
	SELECT DISTINCT
		posaddress_id,
		posaddress_name,
		posaddress_place,
		postype_id,
		postype_name,
		posaddress_store_subclass
	FROM db_retailnet.posaddresses
	INNER JOIN db_retailnet.addresses ON address_id = posaddress_client_id
	INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
	INNER JOIN db_retailnet.provinces ON province_id = place_province
	INNER JOIN db_retailnet.postypes ON postype_id = posaddress_store_postype
")
->filter($_FILTERS)
->order('posaddress_name, posaddress_place')
->fetchAll();

if (!$result) {
	$_ERRORS[] = "POS not found";
	goto BLOCK_RESPONSE;
}

$_TOTAL_POS = 0;
	
foreach ($result as $row) {
	
	$pos = $row['posaddress_id'];
	$type = $row['postype_id'];

	$_TOTAL_POS++;
	
	$_POS[$type]['caption'] = $row['postype_name'];
	$_POS[$type]['pos'][$pos] = $row['posaddress_name'].' ('.$row['posaddress_place'].')';
}


// data: launch plan items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEMS = array();
$_GROUPS = array();

$sth = $model->db->prepare("
	SELECT DISTINCT
		lps_launchplan_item_id AS id,
		lps_launchplan_item_quantity AS quantity,
		lps_launchplan_item_quantity_approved AS quantity_approved,
		lps_launchplan_item_quantity_estimate AS quantity_estimate,
		lps_reference_id AS reference_id,
		UPPER(TRIM(lps_reference_code)) AS reference_code,
		lps_reference_name AS reference_name,
		lps_product_group_id AS group_id,
		lps_product_group_name AS group_name
	FROM lps_launchplan_items
	INNER JOIN lps_references ON lps_reference_id = lps_launchplan_item_reference_id
	INNER JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id
	WHERE lps_launchplan_item_launchplan_id = ?
	ORDER BY group_name, reference_code
");

$sth->execute(array($launchplan->id));
$result = $sth->fetchAll();

if (!$result) {
	$_ERRORS[] = "Items not found";
	goto BLOCK_RESPONSE;
}

$_TOTAL_ITEMS = 0;

foreach ($result as $row) {

	// on distribution mode check approved quantities
	if (!$_MODE_PLANNING && !$row['quantity_approved']) {
		continue;
	}
	
	$_TOTAL_ITEMS++;

	$group = $row['group_id'];
	
	$_GROUPS[$group]['name'] = $row['group_name'];
	$_GROUPS[$group]['length'] += 1;
	
	$item = $row['id'];
	
	$_ITEMS[$item] = array(
		'id' => $row['reference_id'],
		'code' => $row['reference_code'],
		'name' => $row['reference_name'],
		'quantity' => $row['quantity'],
		'approved' => $row['quantity_approved'],
		'estimate' => $row['quantity_estimate']
	);
}

if (!$_TOTAL_ITEMS) {
	$_ERRORS[] = "Items not found";
	goto BLOCK_RESPONSE;
}


// data: warehouses ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_WAREHOUSES = array();

$sth = $model->db->prepare("
	SELECT 
		lps_launchplan_warehouse_id AS id,
		address_warehouse_name AS name
	FROM db_retailnet.address_warehouses
	INNER JOIN lps_launchplan_warehouses ON lps_launchplan_warehouse_address_warehouse_id = address_warehouse_id
	WHERE address_warehouse_stock_reserve IS NULL AND lps_launchplan_warehouse_launchplan_id = ?
");

$sth->execute(array($launchplan->id));
$result = $sth->fetchAll();
$_WAREHOUSES = _array::extract($result);


// data: pos and warehose quantities :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_POS_PLANNED_QUANTITIES = array();
$_WAREHOUSE_PLANNED_QUANTITIES = array();
$_TOTAL_PLANNED_QUANTITIES = array();

$sth = $model->db->prepare("
	SELECT DISTINCT
		lps_launchplan_item_planned_quantity_id AS id,
		lps_launchplan_item_planned_quantity_item_id AS item,
		lps_launchplan_item_planned_quantity_posaddress_id AS pos,
		lps_launchplan_item_planned_quantity_warehouse_id AS warehouse,
		lps_launchplan_item_planned_quantity_quantity AS quantity
	FROM lps_launchplan_item_planned_quantities
	INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_planned_quantity_item_id
	WHERE lps_launchplan_item_launchplan_id = ?
");

$sth->execute(array($launchplan->id));
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$pos = $row['pos'];
		$item = $row['item'];
		$warehouse = $row['warehouse'];
		
		// pos quantities
		if ($pos) {
			$_POS_PLANNED_QUANTITIES[$pos][$item] = $row['quantity'];
			$_TOTAL_PLANNED_QUANTITIES[$item] += $row['quantity'];
		}
		
		// warehouse quantities
		if ($warehouse) {
			$_WAREHOUSE_PLANNED_QUANTITIES[$warehouse][$item] = $row['quantity'];
		}
	}
}

// php excel :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$phpExcel = new PHPExcel();
$phpExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name)->setTitle($_SHEET_CAPTION);
$phpExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$phpExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

$_FIRST_ROW = 1;
$_FIRST_COL = 0;
$_TOTAL_FIXED_ROWS = 7;
$_TOTAL_FIXED_COLS = 0;
$_SPAN_FIXED_COLS = 3;
$_TOTAL_COLS = $_TOTAL_FIXED_COLS + $_TOTAL_ITEMS;
$_FIRST_ITEMS_COL = $_TOTAL_FIXED_COLS + 1;

// page header :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row = $_FIRST_ROW;
$col = $_FIRST_COL;

$column = PHPExcel_Cell::stringFromColumnIndex($col);
$phpExcel->getActiveSheet()->getColumnDimension($column)->setWidth(65);
$phpExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(50);

// page title
$phpExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $_TOTAL_ITEMS, $row);
$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $_PAGE_TITLE);
$phpExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['page-title']);

// row filters
$row++;
$col = $_FIRST_COL;
$caption = $_FILTER_CAPTIONS ? 'Filter(s): '.join(', ', array_filter($_FILTER_CAPTIONS)) : null;
$phpExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $_TOTAL_ITEMS, $row);
$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $caption);

// row categories ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++;
$col = $_FIRST_ITEMS_COL;

$_ROW_CATEGORIES = $row;

$phpExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);

foreach ($_GROUPS as $i => $group) {
	
	if ($group['length']>1) {
		$merge = $col + $group['length']-1;
		$phpExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $merge, $row);
	}
	
	$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $group['name']);
	$col = $col + $group['length'];
}

// styling: categories
$startCel = PHPExcel_Cell::stringFromColumnIndex($_FIRST_ITEMS_COL).$row;
$endCel = PHPExcel_Cell::stringFromColumnIndex($_TOTAL_COLS).$row;
$phpExcel->getActiveSheet()->getStyle("$startCel:$endCel")->applyFromArray($_STYLES['category'], false);

// row references ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++;
$col = $_FIRST_ITEMS_COL;

$phpExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(25);

foreach ($_ITEMS as $i => $item) {
	
	$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $item['code']);

	$column = PHPExcel_Cell::stringFromColumnIndex($col);
	$phpExcel->getActiveSheet()->getColumnDimension($column)->setWidth(15);

	$col++;
}

// styling: items
$startCel = PHPExcel_Cell::stringFromColumnIndex($_FIRST_ITEMS_COL).$row;
$endCel = PHPExcel_Cell::stringFromColumnIndex($_TOTAL_COLS).$row;
$phpExcel->getActiveSheet()->getStyle("$startCel:$endCel")->applyFromArray($_STYLES['item'], false);

// row planned/approved quantities :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++;
$col = $_FIRST_COL;

$phpExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);

$caption = $_MODE_PLANNING ? 'Total Launch Quantity' : 'Total Launch Quantity Approved';
$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $caption);
$phpExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['quantity-label']);

$col = $_FIRST_ITEMS_COL;

foreach ($_ITEMS as $i => $item) {
	$quantity = $_MODE_PLANNING ? $item['quantity'] : $item['approved'];
	$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $quantity);
	$col++;
}

// row total planned :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++;
$col = $_FIRST_COL;

$phpExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);

$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, 'Total Planned');
$phpExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['quantity-label']);

$col = $_FIRST_ITEMS_COL;

foreach ($_ITEMS as $i => $item) {
	$quantity = $_TOTAL_PLANNED_QUANTITIES[$i];
	$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $quantity);
	$col++;
}

// styling: borders
$startCel = PHPExcel_Cell::stringFromColumnIndex($_FIRST_ITEMS_COL).$_ROW_CATEGORIES;
$endCel = PHPExcel_Cell::stringFromColumnIndex($_TOTAL_COLS).$row;
$phpExcel->getActiveSheet()->getStyle("$startCel:$endCel")->applyFromArray($_STYLES['borders'], false);

// grid ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$firstRow = $row+1;

foreach ($_POS as $type => $data) {

	// pos type name
	$row++;
	$col = $_FIRST_COL;

	$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data['caption']);
	$phpExcel->getActiveSheet()->mergeCellsByColumnAndRow($_FIRST_COL, $row, $_TOTAL_ITEMS, $row);
	$phpExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
	$phpExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['group']);

	foreach ($data['pos'] as $pos => $name) {

		// pos name
		$row++;
		$col = $_FIRST_COL;
		$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $name);
		$phpExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['pos']);

		$col = $_FIRST_ITEMS_COL;

		foreach ($_ITEMS as $i => $item) {
			$value = $_POS_PLANNED_QUANTITIES[$pos][$i];
			$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
			$col++;
		}
	}
}

$lastRow = $row;

// styling block header
$startCel = PHPExcel_Cell::stringFromColumnIndex($_FIRST_COL).$firstRow;
$endCel = PHPExcel_Cell::stringFromColumnIndex($_TOTAL_COLS).$lastRow;
$phpExcel->getActiveSheet()->getStyle("$startCel:$endCel")->applyFromArray($_STYLES['borders'], false);

// warehouses ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_WAREHOUSES) {

	// warehouse caption
	$row++;
	$col = $_FIRST_COL;

	$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, 'Warehouses');
	$phpExcel->getActiveSheet()->mergeCellsByColumnAndRow($_FIRST_COL, $row, $_TOTAL_ITEMS, $row);
	$phpExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
	$phpExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['group']);

	$firstRow = $row;

	foreach ($_WAREHOUSES as $warehose => $name) {

		// pos name
		$row++;
		$col = $_FIRST_COL;
		$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $name);

		$col = $_FIRST_ITEMS_COL;

		foreach ($_ITEMS as $i => $item) {
			$value = $_WAREHOUSE_PLANNED_QUANTITIES[$warehose][$i];
			$phpExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
			$col++;
		}
	}

	$lastRow = $row;

	// styling block header
	$startCel = PHPExcel_Cell::stringFromColumnIndex($_FIRST_COL).$firstRow;
	$endCel = PHPExcel_Cell::stringFromColumnIndex($_TOTAL_COLS).$lastRow;
	$phpExcel->getActiveSheet()->getStyle("$startCel:$endCel")->applyFromArray($_STYLES['borders'], false);
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONSE:

ob_end_clean();

if (!$_ERRORS) {

	$stamp = $_SERVER['REQUEST_TIME'];
	$file = "/data/tmp/launchplan.planning.$stamp.xlsx";
	$hash = Encryption::url($file);
	$url = "/download/tmp/$hash";
	
	// set active sheet
	$phpExcel->getActiveSheet()->setTitle('Launch Plan Planning');
	$phpExcel->setActiveSheetIndex(0);
	$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
	$objWriter->save($_SERVER['DOCUMENT_ROOT'].$file);
}

header('Content-Type: text/json');

echo json_encode(array(
	'success' => file_exists($_SERVER['DOCUMENT_ROOT'].$file) ? true : false,
	'errors' => $_ERRORS,
	'file' => $url,
	'test' => $_POS_PLANNED_QUANTITIES
));
