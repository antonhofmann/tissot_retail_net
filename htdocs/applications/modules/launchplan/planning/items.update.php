<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$id = $_REQUEST['ordersheet'];

// database model
$model = new Model($application);

$stateProgress = 3;

$map = array(
	'lps_launchplan_id' => 'ordersheet_id',
	'lps_launchplan_workflowstate_id' => 'ordersheet_workflowstate_id',
	'lps_launchplan_address_id' => 'ordersheet_address_id',
	'lps_launchplan_mastersheet_id' => 'ordersheet_mastersheet_id',
	'lps_launchplan_openingdate' => 'ordersheet_openingdate',
	'lps_launchplan_closingdate' => 'ordersheet_closingdate',
	'lps_launchplan_comment' => 'ordersheet_comment'
);

	
// order sheet
$launchplan = new Modul($application);
$launchplan->setTable('lps_launchplans');
$launchplan->setDataMap($map);
$launchplan->read($id);

// state handler
$state = new State($data['lps_launchplan_workflowstate_id']); 
$state->setOwner($data['lps_launchplan_address_id']);
$state->setDateIntervals($data['lps_launchplan_openingdate'], $data['lps_launchplan_closingdate']);
	

if ($launchplan->id) {

	$response = true;

	$tracker = new User_Tracking($application);
	
	// update planning
	//if ($state->onPreparation()) {
	if ($state->isOpen() || $state->isProgress()) {

		// get planned quantities
		$sth = $model->db->prepare("
			SELECT
				lps_launchplan_item_planned_quantity_item_id AS id,
				SUM(lps_launchplan_item_planned_quantity_quantity) AS quantity
			FROM lps_launchplan_item_planned_quantities
			INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_planned_quantity_item_id
			WHERE lps_launchplan_item_launchplan_id = ?
			GROUP BY lps_launchplan_item_planned_quantity_item_id
		");

		$sth->execute(array($launchplan->id));
		$items = $sth->fetchAll();
		
		// update order sheet item quantities
		if ($items) {

			$sth = $model->db->prepare("
				UPDATE lps_launchplan_items SET
					lps_launchplan_item_quantity = ?,
					user_modified = ?,
					date_modified = NOW()
				WHERE lps_launchplan_item_id = ?
			");
		
			foreach ($items as $item) {
				$sth->execute(array($item['quantity'], $user->login, $item['id']));
			}
			
			// update order sheet state
			if ($state->isOpen()) {
				
				// workflow state
				$sth = $model->db->prepare("
					SELECT lps_workflow_state_name AS name
					FROM lps_workflow_states
					WHERE lps_workflow_state_id = ?
				");
				
				$sth->execute(array($stateProgress));
				$workflowState = $sth->fetch();

				// update sheet state
				$sth = $model->db->prepare("
					UPDATE lps_launchplans SET
						lps_launchplan_workflowstate_id = ?,
						user_modified =?,
						date_modified = NOW()
					WHERE lps_launchplan_id = ?
				");

				$inProgress = $sth->execute(array($stateProgress, $user->id, $launchplan->id));
			}
		}
	}

	$message = ($response) ? message::request_updated() : $translate->message_request_failure;
	
} 
else {
	$response = false;
	$message = "The Order Sheet not found.";
}

// trackering
if ($response && $inProgress) {
	$tracker->create(array(
		'user_tracking_entity' => 'order sheet',
		'user_tracking_entity_id' => $launchplan->id,
		'user_tracking_user_id' => $user->id,
		'user_tracking_action' => $workflowState['name'],
		'user_created' => $user->login,
		'date_created' => date('Y-m-d H:i:s')
	));
}
	
// response with json header
header('Content-Type: text/json');
echo json_encode(array(
	'response' => $response,
	'message' => $message
));
	