<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// request
$application = $_REQUEST['application'];
$section = $_REQUEST['section'];
$id = $_REQUEST['launchplan'];
$warehouse = $_REQUEST['warehouse'];
$warehouseName = $_REQUEST['address_warehouse_name'];
$addressWarehouseID= $_REQUEST['address_warehouse_id'];

$model = new Model($application);

$launchplan = new Modul($application);
$launchplan->setTable('lps_launchplans');
$launchplan->read($id);

if ($launchplan->id) {
	
	switch ($section) {
		
		case 'check':
			
			// has warehouse planned quantities
			$sth = $model->db->prepare("
				SELECT COUNT(lps_launchplan_item_planned_quantity_id) AS items
				FROM lps_launchplan_item_planned_quantities
				INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_planned_quantity_item_id
				WHERE lps_launchplan_item_launchplan_id = ? 
				AND lps_launchplan_item_planned_quantity_warehouse_id = ? 
				AND lps_launchplan_item_planned_quantity_quantity > 0
			");
			
			$sth->execute(array($launchplan->id, $warehouse));
			$result = $sth->fetch();
			
			if (!$result['items']) $response = true;
			else {
				$response = false;
				$message = $translate->warning_warehouse;
			}
			
		break;
		
		//Add new order sheet warehouse
		//if warehouse is not by Company, add new warehouse to company
		case 'add':
			
			// add company warehouse to order sheet
			if ($addressWarehouseID && $addressWarehouseID <> 'new') {
				
				$sth = $model->db->prepare("
					INSERT INTO lps_launchplan_warehouses (
						lps_launchplan_warehouse_launchplan_id,
						lps_launchplan_warehouse_address_warehouse_id,
						user_created,
						date_created
					)
					VALUES (?,?,?,NOW())
				");
				
				$response = $sth->execute(array($launchplan->id, $addressWarehouseID, $user->login));
			}
			// create new company warehouse
			// and add this warehouse to order sheet
			else {
				
				$company = $launchplan->data['ordersheet_address_id'];
				
				// add new company warehouse
				$sth = $model->db->prepare("
					INSERT INTO db_retailnet.address_warehouses (
						address_warehouse_address_id,
						address_warehouse_name,
						address_warehouse_active,
						user_created,
						date_created
					)
					VALUES (?,?,1,?,NOW())
				");

				$sth->execute(array($company, $warehouseName, $user->login));
				$addressWarehouseID = $model->db->lastInsertId();

				if ($addressWarehouseID) {
					
					$sth = $model->db->prepare("
						INSERT INTO lps_launchplan_warehouses (
							lps_launchplan_warehouse_launchplan_id,
							lps_launchplan_warehouse_address_warehouse_id,
							user_created,
							date_created
						)
						VALUES (?,?,?,NOW())
					");
					
					$response = $sth->execute(array($launchplan->id, $addressWarehouseID, $user->login));
				}
			}
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		
		break;
		
		// Remove warehose from order sheets
		case 'delete':
			
			// has warehouse planned quantities
			$sth = $model->db->prepare("
				SELECT COUNT(lps_launchplan_item_planned_quantity_id) AS items
				FROM lps_launchplan_item_planned_quantities
				INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_planned_quantity_item_id
				WHERE lps_launchplan_item_launchplan_id = ? 
				AND lps_launchplan_item_planned_quantity_warehouse_id = ? 
				AND lps_launchplan_item_planned_quantity_quantity > 0
			");

			$sth->execute(array($launchplan->id, $warehouse));
			$result = $sth->fetch();
			
			if ($result['items']>0) {
				$message = 'The Warehouse has planned quantities and cannot be deleted.';
			} else {

				$sth = $model->db->prepare("
					DELETE FROM lps_launchplan_warehouses
					WHERE lps_launchplan_warehouse_id = ?
				");

				$response = $sth->execute(array($warehouse));
			}
		
		break;
	}
}
else {
	$response = false;
	$message = 'The order Sheet not found';
}

header('Content-Type: text/json');
echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'reload' => $response
));
	