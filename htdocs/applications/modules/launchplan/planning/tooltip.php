<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$section = $_REQUEST['section'];
$application = $_REQUEST['application'];
$id = $_REQUEST['id'];

$model = new Model($application);

switch ($application) {
	
	case 'lps':
	

		$tableOrderSheet = 'lps_launchplans';

		$queryReference = "
			SELECT 
				lps_reference_id AS id,
				lps_product_group_name AS group_name,
				lps_product_subgroup_name AS subgroup,
				lps_product_type_name AS type,
				lps_collection_code AS collection,
				lps_collection_category_code AS category,
				lps_reference_code AS code,
				lps_reference_name AS name,
				lps_reference_price_point AS price
			FROM lps_references
			LEFT JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id
			LEFT JOIN lps_collections ON lps_collection_id = lps_reference_collection_id
			LEFT JOIN lps_collection_categories ON lps_collection_category_id = lps_reference_collection_category_id
			LEFT join lps_product_subgroups ON lps_product_subgroup_id = lps_reference_product_subgroup_id
			LEFT JOIN lps_product_types ON lps_product_type_id = lps_reference_product_type_id
			WHERE lps_reference_id = ?
		";

		$queryPos = "
			SELECT
				posaddress_franchisee_id,
				posaddress_name AS name,
				posaddress_address AS address,
				CONCAT_WS(' ',posaddress_zip,country_name) AS country,
				CONCAT(mps_distchannel_group_name, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) AS distribution_channel,
				posowner_type_name AS owner_type,
				postype_name AS type,
				watches.mps_turnoverclass_code AS watch,
				bijoux.mps_turnoverclass_code AS bijoux,
				address_sapnr
			FROM db_retailnet.posaddresses
			INNER JOIN db_retailnet.addresses ON address_id = posaddress_franchisee_id
			INNER JOIN db_retailnet.countries ON country_id = posaddress_country
			INNER JOIN db_retailnet.posowner_types ON posowner_type_id = posaddress_ownertype
			INNER JOIN db_retailnet.postypes ON postype_id = posaddress_store_postype
			LEFT JOIN db_retailnet.mps_distchannels ON mps_distchannel_id = posaddress_distribution_channel
			LEFT JOIN db_retailnet.mps_distchannel_groups ON mps_distchannels.mps_distchannel_group_id = mps_distchannel_groups.mps_distchannel_group_id
			LEFT JOIN db_retailnet.mps_turnoverclasses watches ON watches.mps_turnoverclass_id = posaddress_turnoverclass_watches
			LEFT JOIN db_retailnet.mps_turnoverclasses bijoux ON bijoux.mps_turnoverclass_id = posaddress_turnoverclass_bijoux
			WHERE posaddress_id = ?
		";

	break;
}

switch ($section) {
		
	case 'reference':
		
		$sth = $model->db->prepare($queryReference);
		$sth->execute(array($id));
		$reference = $sth->fetch();
		
		if ($reference) {
		
			$json['content'] = "
				<table class=\"default tooltip\" >
					<tr>
						<td valign=top >Product Group: </td>
						<td valign=top >{$reference[group_name]}</td>
					</tr>
					<tr>
						<td valign=top >Product Subgroup: </td>
						<td valign=top >{$reference[subgroup]}</td>
					</tr>
					<tr>
						<td valign=top >Product Type: </td>
						<td valign=top >{$reference[type]}</td>
					</tr>
					<tr>
						<td valign=top >Collection Code: </td>
						<td valign=top >{$reference[collection]}</td>
					</tr>
					<tr>
						<td valign=top >Category Code: </td>
						<td valign=top >{$reference[category]}</td>
					</tr>
					<tr>
						<td valign=top >Item Code: </td>
						<td valign=top >{$reference[code]}</td>
					</tr>
					<tr>
						<td valign=top >Item Name: </td>
						<td valign=top >{$reference[name]}</td>
					</tr>
					<tr>
						<td valign=top >Price: </td>
						<td valign=top >{$reference[price]}</td>
					</tr>
				</table>
			";
		}
		
	break;
	
	case 'pos':
		
		$sth = $model->db->prepare($queryPos);
		$sth->execute(array($id));
		$pos = $sth->fetch();
		
		if ($pos) {
			
			$json['content'] = "
				<table class=\"default tooltip\" >
					<tr>
						<td valign=top >POS Name: </td>
						<td valign=top >{$pos[name]}</td>
					</tr>
					<tr>
						<td valign=top >Address: </td>
						<td valign=top >{$pos[address]}</td>
					</tr>
					<tr>
						<td valign=top >ZIP Country: </td>
						<td valign=top >{$pos[country]}</td>
					</tr>
					<tr>
						<td valign=top >Distribution Channel: </td>
						<td valign=top >{$pos[distribution_channel]}</td>
					</tr>
					<tr>
						<td valign=top >Legal Type: </td>
						<td valign=top >{$pos[owner_type]}</td>
					</tr>
					<tr>
						<td valign=top >POS Type: </td>
						<td valign=top >{$pos[type]}</td>
					</tr>
					<tr>
						<td valign=top >SAP: </td>
						<td valign=top >{$pos[address_sapnr]}</td>
					</tr>
					$material_list
				</table>
			";
		}
		
	break;
	
	case 'warehouse-info':
		
		$application = $_REQUEST['application'];
		$id = $_REQUEST['id'];
		
		$companyWarehouse = new Company_Warehouse();
		$companyWarehouse->read($id);
		
		if ($companyWarehouse->id) {

			$json['content'] = "
				<table class=\"default tooltip\" >
					<tr>
						<td valign=top nowrap=nowrap width='20%' >Warehouse Name: </td>
						<td valign=top >$companyWarehouse->name</td>
					</tr>
					<tr>
						<td valign=top nowrap=nowrap >SAP Customer Code: </td>
						<td valign=top >$companyWarehouse->sap_customer_number</td>
					</tr>
					<tr>
						<td valign=top nowrap=nowrap >SAP Shipto NUmber: </td>
						<td valign=top >$companyWarehouse->sap_shipto_number</td>
					</tr>
				</table>
			";
		}
		
	break;

}

header('Content-Type: text/json');
echo json_encode($json);
	