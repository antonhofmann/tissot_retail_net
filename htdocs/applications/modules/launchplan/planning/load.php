<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc'; 

$user = User::instance();
$translate = Translate::instance();

define(DEBUGGIN_MODE, true);
define(COLUMN_PLANNING, 'planning');
define(COLUMN_DISTRIBUTION, 'delivered');
define(COLUMN_PARTIALY_CONFIRMED, 'partial');
define(CELL_READONLY, 'true');

$_ERRORS = array();
$_MESSAGES = array();
$_DATA = array();


// request :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['launchplan'];

$version = $_REQUEST['version'];
$_DISTRIBUTION_CHANNEL = $_REQUEST['distribution_channel'];
$_TURNOVER_CLASS_WATCH = $_REQUEST['turnoverclass_watch'];
$_SALES_REPRESENTATIVE = $_REQUEST['sale_representative'];
$_DECORATION_PERSON = $_REQUEST['decoration_person'];
$_PROVINCE = $_REQUEST['province'];

// launch plan :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$model = new Model($application);

$launchplan = new Modul($application);
$launchplan->setTable('lps_launchplans');
$launchplan->read($id);

// state handler
$currentState = $launchplan->data['lps_launchplan_workflowstate_id'];
$state = new State($currentState, $application);
$state->setOwner($launchplan->data['lps_launchplan_address_id']);
$state->setDateIntervals($launchplan->data['lps_launchplan_openingdate'], $launchplan->data['lps_launchplan_closingdate']);

// company
$company = new Company();
$company->read($launchplan->data['lps_launchplan_address_id']);


// state and permissions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_STATE_EDIT = $state->canEdit(false);
$_MODE_PLANNING = in_array($currentState, array(1,2,3,4,6)) ? true : false;
$_MODE_APPROVED = !$_MODE_PLANNING;
$_MODE_EXPORTED = in_array($currentState, array(9,10,11)) ? true : false;
	

// request filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$filters = array();
	
// filter: pos
$filters['default'] = "(posaddress_client_id = $company->id OR (address_parent = $company->id AND address_client_type = 3))";

// filter: pos
//$filters['default'] = "posaddress_client_id = $company->id";

// filter: distribution channels
if ($_DISTRIBUTION_CHANNEL) {
	$filters['distribution_channels'] = "posaddress_distribution_channel = $_DISTRIBUTION_CHANNEL";
}

// filter: turnover classes
if ($_TURNOVER_CLASS_WATCH) {
	$filters['turnoverclass_watches'] = "posaddress_turnoverclass_watches = $_TURNOVER_CLASS_WATCH";
}

// filter: sales represenatives
if ($_SALES_REPRESENTATIVE) {
	$filters['sales_representative'] = "posaddress_sales_representative = $_SALES_REPRESENTATIVE";
}

// filter: decoration persons
if ($_DECORATION_PERSON) {
	$filters['decoration_person'] = "posaddress_decoration_person = $_DECORATION_PERSON";
}

// provinces
if ($_PROVINCE) {
	$filters['provinces'] = "province_id = $_PROVINCE";
}

// for archived ordersheets show only POS locations which has distributed quantities 
if ($_MODE_EXPORTED) {
	
	// get only pos whith exported items
	$sth = $model->db->prepare("
		SELECT GROUP_CONCAT(lps_launchplan_item_planned_quantity_posaddress_id) AS pos
		FROM lps_launchplan_item_planned_quantities
		INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_planned_quantity_item_id
		WHERE lps_launchplan_item_launchplan_id = ?
	");

	$sth->execute(array($launchplan->id));
	$result = $sth->fetch();

	if ($result['pos']) {
		$filters['active'] = "posaddress_id IN ({$result[pos]})";
	} else {
		$filters['active'] = "(
			posaddress_store_closingdate = '0000-00-00' 
			OR posaddress_store_closingdate IS NULL 
			OR posaddress_store_closingdate = ''
		)";
	}

} else {
	$filters['active'] = "(
		posaddress_store_closingdate = '0000-00-00' 
		OR posaddress_store_closingdate IS NULL 
		OR posaddress_store_closingdate = ''
	)";
}
	

// data: pos locations :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_POS_LOCATIONS = array();

$result = $model->query("
	SELECT DISTINCT
		posaddress_id,
		posaddress_name,
		posaddress_place,
		postype_id,
		postype_name,
		posaddress_store_subclass
	FROM db_retailnet.posaddresses
	INNER JOIN db_retailnet.addresses ON address_id = posaddress_client_id
	INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
	INNER JOIN db_retailnet.provinces ON province_id = place_province
	INNER JOIN db_retailnet.postypes ON postype_id = posaddress_store_postype
")
->filter($filters)
->order('posaddress_name, posaddress_place')
->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$pos = $row['posaddress_id'];
		$type = $row['postype_id'];
		$_POS_LOCATIONS[$type]['caption'] = $row['postype_name'];
		$_POS_LOCATIONS[$type]['pos'][$pos]['name'] = $row['posaddress_name'];
		$_POS_LOCATIONS[$type]['pos'][$pos]['place'] = $row['posaddress_place'];
	}
}


// data: order sheet items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_LAUNCHPLAN_ITEMS = array();
$_REFERENCES = array();

$sth = $model->db->prepare("
	SELECT DISTINCT
		lps_launchplan_item_id AS id,
		lps_launchplan_item_quantity AS quantity,
		lps_launchplan_item_quantity_approved AS quantity_approved,
		lps_launchplan_item_quantity_estimate AS quantity_estimate,
		lps_reference_id AS reference_id,
		lps_reference_code AS reference_code,
		lps_reference_name AS reference_name,
		lps_product_group_id AS group_id,
		lps_product_group_name AS group_name
	FROM lps_launchplan_items
	INNER JOIN lps_references ON lps_reference_id = lps_launchplan_item_reference_id
	INNER JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id
	WHERE lps_launchplan_item_launchplan_id = ?
	ORDER BY group_name, reference_code
");

$sth->execute(array($launchplan->id));
$result = $sth->fetchAll();

if ($result) {

	foreach ($result as $row) {

		// on distribution mode check approved quantities
		if (!$_MODE_PLANNING && !$row['quantity_approved']) {
			continue;
		}
		
		$item = $row['id'];
		$group = $row['group_id'];

		$_REFERENCES[$item] = array(
			'reference' => $row['reference_id'],
			'reference_code' => $row['reference_code'],
			'reference_name' => $row['reference_name'],
			'quantity' => $row['quantity'],
			'quantity_approved' => $row['quantity_approved'],
			'quantity_estimate' => $row['quantity_estimate']
		);

		$_LAUNCHPLAN_ITEMS[$group]['caption'] = $row['group_name'];
		$_LAUNCHPLAN_ITEMS[$group]['items'][] = $item;
	}
}


// data: warehouses ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_WAREHOUSES = array();

$sth = $model->db->prepare("
	SELECT 
		lps_launchplan_warehouse_id AS id,
		address_warehouse_name AS name
	FROM db_retailnet.address_warehouses
	INNER JOIN lps_launchplan_warehouses ON lps_launchplan_warehouse_address_warehouse_id = address_warehouse_id
	WHERE address_warehouse_stock_reserve IS NULL AND lps_launchplan_warehouse_launchplan_id = ?
");

$sth->execute(array($launchplan->id));
$result = $sth->fetchAll();

$_WAREHOUSES = _array::extract($result);


// data: pos and warehose quantities :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_POS_PLANNED_QUANTITIES = array();
$_WAREHOUSE_PLANNED_QUANTITIES = array();

$sth = $model->db->prepare("
	SELECT DISTINCT
		lps_launchplan_item_planned_quantity_id AS id,
		lps_launchplan_item_planned_quantity_posaddress_id AS pos,
		lps_launchplan_item_planned_quantity_warehouse_id AS warehouse,
		lps_launchplan_item_planned_quantity_quantity AS quantity,
		lps_launchplan_item_reference_id AS reference
	FROM lps_launchplan_item_planned_quantities
	INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_planned_quantity_item_id
	WHERE lps_launchplan_item_launchplan_id = ?
");

$sth->execute(array($launchplan->id));
$result = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$pos = $row['pos'];
		$warehouse = $row['warehouse'];
		$reference = $row['reference'];
		
		// pos quantities
		if ($pos) {
			$_POS_PLANNED_QUANTITIES[$pos][$reference]['id'] = $row['id'];
			$_POS_PLANNED_QUANTITIES[$pos][$reference]['quantity'] = $row['quantity'];
		}
		
		// warehouse quantities
		if ($warehouse) {
			$_WAREHOUSE_PLANNED_QUANTITIES[$warehouse][$reference]['id'] = $row['id'];
			$_WAREHOUSE_PLANNED_QUANTITIES[$warehouse][$reference]['quantity'] = $row['quantity'];
		}
	}
}


// spreadsheet headers :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_DATAGRID = array();
$_ROW_SEPARATORS = array();
$_COL_SEPARATORS = array();
$_ROW_SPANS = array();
$_COL_SPANS = array();

$_ROW_BLANK = 1;
$_FIRST_ROW = 2;
$_FIRST_COL = 1;

$_SPAN_FIXED_COLS = 3;

// product groups
$row = $_FIRST_ROW;
$col = $_FIRST_COL;
$_ROW_PRODUCT_GROUP=$row;
$_DATAGRID[$row][$col] = array(
	'text' => $filteIndex ? 'Filter(s): '.join(', ',$filteIndex) : null,
	'cls' => 'filters',
	'colspan' => $_SPAN_FIXED_COLS
);

// references
$row++;
$col = $_FIRST_COL;
$_ROW_REFERENCE=$row; 
$_DATAGRID[$row][$col] = array(
	'colspan' => $_SPAN_FIXED_COLS
);

// item quantities
$row++;
$col = $_FIRST_COL;
$_ROW_QUANTITY=$row;
$_DATAGRID[$row][$col] = array(
	'colspan' => $_SPAN_FIXED_COLS,
	'text' => $_MODE_PLANNING ? 'Total Launch Quantiy' : 'Total Launch Quantity Approved',
	'cls' => 'calculated-caption'
);

// planned quantities
$row++;
$col = $_FIRST_COL;
$_ROW_PLANNED_QUANTITY=$row;
$_DATAGRID[$row][$_FIRST_COL] = array(
	'text' => 'Total Planned',
	'colspan' => $_SPAN_FIXED_COLS,
	'cls' => 'calculated-caption'
);

$_LAST_FIXED_ROW=$row;	// row index last fixed
$_LAST_FIXED_COL = $_SPAN_FIXED_COLS; 	// col index last fixed

// separators
$row++;
$_ROW_SEPARATORS[$row] = true;
$_COL_SEPARATORS[4] = true;

$_TOTAL_FIXED_ROWS=$row;
$_TOTAL_FIXED_COLS=4;


// section: pos locations ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_POS_LOCATIONS) {

	$_FIRST_POS_ROW=$row+1;

	$separator = 0;

	foreach ($_POS_LOCATIONS as $type => $data) {

		$row++;
		$col=$_FIRST_COL;

		// type trigger arrow
		$_DATAGRID[$row][$col] = array(
			'html' =>  "<span class=\"fa fa-caret-down type arrow_$type type_$type\"  type=\"$type\" row=\"$row\" ></span>",
			'cls' => 'postype-trigger postype-caption row-caption',
			'group' => true
		);

		$col++;

		// pos type name
		$_DATAGRID[$row][$col] = array(
			'group' => true,
			'type' => $type,
			'html' => htmlspecialchars($data['caption'], ENT_QUOTES),
			'cls' => 'postype-caption row-caption'
		);

		$col++;

		// pos type name
		$_DATAGRID[$row][$col] = array(
			'group' => true,
			'type' => $type,
			'html' =>'FF',
			'cls' => 'postype-caption row-caption'
		);


		foreach ($data['pos'] as $pos => $value) {

			// caption cleaning
			$caption = str_replace("'", "`", "{$value[name]} ({$value[place]})");
			$caption = strlen($caption)>50 ? substr($caption, 0, 50).'..' : $caption;

			$row++;
			$col = $_FIRST_COL;

			// pos info icon
			$_DATAGRID[$row][$col] = array(
				'html' =>  "<span class=\"icon info-full-circle infotip type_$type\" data-section=\"pos\" data-id=\"$pos\" data-row=\"$row\" ></span>",
				'pos' => $pos
			);

			$col++;

			// pos caption
			$_DATAGRID[$row][$col] = array(
				'type' => $type,
				'pos' => $pos,
				'html' => $caption,
				'cls' => 'pos-caption'
			);

			$col++;

		}

		if ($separator) {
			$row++;
			$_ROW_SEPARATORS[$row] = true;
		}
	}

	$_LAST_POS_ROW = $separator ? $row-$separator : $row;
}


// section: warehouses :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++;
$_ROW_WAREHOUSE = $row; // row index warehouse caption

if ($_STATE_EDIT) {

	$col = $_FIRST_COL;

	// add new warehouse
	$_DATAGRID[$row][$col] = array(
		'html' => "<span id=\"add_warehouse\" class=\"icon add dialog\" ></span>",
		'cls' => 'row-caption'
	);

	$col++;

	// warehouse caption
	$_DATAGRID[$row][$col] = array(
		'html' => 'Warehouses',
		'cls' => 'warehouse-caption row-caption',
		'colspan' => 2
	);

} else {

	$col = $_FIRST_COL;

	// warehouse caption
	$_DATAGRID[$row][$col] = array(
		'html' => 'Warehouses',
		'cls' => 'warehouse-caption row-caption',
		'colspan' => 3
	);
}

if ($_WAREHOUSES) {

	$_FIRST_WAREHOUSE_ROW = $row+1;

	foreach ($_WAREHOUSES as $warehouse => $name) {
		
		$row++;
		$col = $_FIRST_COL;

		if ($_STATE_EDIT) {

			// remove warehouse
			$_DATAGRID[$row][$col] = array(
				'reserve' => $value['reserve'],
				'html' => "<span class=\"icon cancel remove-warehouse\" data-row=\"$row\" data-id=\"$warehouse\"></span>",
				'warehouse' => $warehouse
			);

			$caption = str_replace("'", "`", $name);
			$caption = (strlen($caption) > 50) ? substr($caption, 0, 50).'..' : $caption;

			// remove warehouse
			$col++;
			$_DATAGRID[$row][$col] = array(
				'warehouse' => $warehouse,
				'html' => $name,
				'cls' => 'warehouse-caption',
				'id' => $warehouse,
				'colspan' => 2
			);

		} else {

			$_DATAGRID[$row][$col] = array(
				'warehouse' => $warehouse,
				'html' => $name,
				'cls' => 'warehouse-caption',
				'id' => $warehouse,
				'colspan' => 3
			);
		}	
	}

	$_LAST_WAREHOUSE_ROW = $row;
}

$_TOTAL_ROWS = $row;

// section: references :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_LAUNCHPLAN_ITEMS) {

	// col reference for first item
	$_FIRST_REFERENCE_COL = $_TOTAL_FIXED_COLS+1;
	
	$separator=1;

	$col=$_TOTAL_FIXED_COLS;

	foreach ($_LAUNCHPLAN_ITEMS as $group => $data) {

		$col++;
		$colspanGroup=0;
		$colkey = $col;

		// group captions
		$_DATAGRID[$_ROW_PRODUCT_GROUP][$col] = array(
			'group' => $group,
			'html' => htmlspecialchars($data['caption'], ENT_QUOTES),
			'cls' => 'cel-group'
		);
		
		// group item
		foreach ($data['items'] as $item) {

			$colspanGroup++;

			$reference = $_REFERENCES[$item]['reference'];
			$code = $_REFERENCES[$item]['reference_code'];
			$caption = "<span class=\"icon-holder\" ><span class=\"icon info-full-circle infotip\" data-section=\"reference\" data-id=\"$reference\"></span></span><span class=\"code\">$code</span>";

			// reference caption
			$_DATAGRID[$_ROW_REFERENCE][$col] = array(
				'item' => $item,
				'reference' => $reference,
				'html' => $caption,
				'cls' => 'cel-reference',
				'column' => COLUMN_PLANNING
			);

			$col++;
		}

		// colspan product group
		$_DATAGRID[$_ROW_PRODUCT_GROUP][$colkey]['colspan'] = $colspanGroup;

		// col separator
		if ($separator) {
			$_COL_SEPARATORS[$col] = true;
		}
	}

	$_LAST_REFERENCE_COL = $separator ? $col-1 : $col;
	$_TOTAL_COLS = $_LAST_REFERENCE_COL;
}


// spreadsheet :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_TOTAL_ROWS && $_TOTAL_COLS) {

	for ($row=1; $row <= $_TOTAL_ROWS; $row++) {
		
		for ($col=1; $col <= $_TOTAL_COLS; $col++) {

			$index = spreadsheet::key($col, $row);

			// colspan
			if ($_DATAGRID[$row][$col]['colspan']) {
				$_COL_SPANS[$index] = $_DATAGRID[$row][$col]['colspan'];
			}

			if ($_ROW_SEPARATORS[$row] || $_COL_SEPARATORS[$col]) {
				$clsRow = $rowSeparators[$row] ? 'row-sep' : null;
				$clsCol = $_COL_SEPARATORS[$col] ? ' col-sep' : null;
				$_DATA[$index]['cls'] = $clsRow.$clsCol;
				$_DATA[$index]['readonly'] = CELL_READONLY;
			}
			else {

				switch ($row) {

					case $_ROW_PRODUCT_GROUP:
					case $_ROW_REFERENCE:
						$classCell = $col>=$_FIRST_REFERENCE_COL ? 'cel ' : null;
						$_DATA[$index]['cls'] = $classCell.$_DATAGRID[$row][$col]['cls'];
						$_DATA[$index]['html'] = $_DATAGRID[$row][$col]['html'] ?: $_DATAGRID[$row][$col]['text'];
						$_DATA[$index]['readonly']  = CELL_READONLY;
					break;

					case $_ROW_QUANTITY:

						if ($col<=$_LAST_FIXED_COL) {
							$_DATA[$index]['html'] = $_DATAGRID[$row][$col]['html'] ?: $_DATAGRID[$row][$col]['text'];
							$_DATA[$index]['cls'] = $_DATAGRID[$row][$col]['cls'];
						}
						elseif ($col >= $_FIRST_REFERENCE_COL && $col <= $_LAST_REFERENCE_COL) {
							
							$item = $_DATAGRID[$_ROW_REFERENCE][$col]['item'];
							$column = $_DATAGRID[$_ROW_REFERENCE][$col]['column'];

							$_DATA[$index]['hiddenAsNull'] = 'true';
							$_DATA[$index]['numeric'] = 'true';
							$_DATA[$index]['cls'] = 'cel '. $_DATAGRID[$row][$col]['cls'];
							$_DATA[$index]['text'] = $state->onApproving() ? $_REFERENCES[$item]['quantity'] : $_REFERENCES[$item]['quantity_approved'];
						}

						$_DATA[$index]['readonly']=CELL_READONLY;

					break;

					case $_ROW_PLANNED_QUANTITY:

						if ($col<=$_LAST_FIXED_COL) {
							$_DATA[$index]['html'] = $_DATAGRID[$row][$col]['html'] ?: $_DATAGRID[$row][$col]['text'];
							$_DATA[$index]['cls'] = $_DATAGRID[$row][$col]['cls'];
						}
						elseif ($col >= $_FIRST_REFERENCE_COL && $col <= $_LAST_REFERENCE_COL) {
											
							$column = $_DATAGRID[$_ROW_REFERENCE][$col]['column'];
							$item = $_DATAGRID[$_ROW_REFERENCE][$col]['item'];

							$_DATA[$index]['hiddenAsNull'] = 'true';
							$_DATA[$index]['numeric'] = 'true';
							$_DATA[$index]['cls'] = 'cel '. $_DATAGRID[$row][$col]['cls'];
							$range = spreadsheet::key($col, $_FIRST_POS_ROW).'_'.spreadsheet::key($col, $_LAST_POS_ROW);
							$_DATA[$index]['text']  = "function($range) { total = sum($range); return total ? total : null; }";
						}

						$_DATA[$index]['readonly']=CELL_READONLY;

					break;
				
					case $row >= $_FIRST_POS_ROW && $row <= $_LAST_POS_ROW:

						if ($col==$_FIRST_COL) {
							$group = $_DATAGRID[$row][$col]['group'];
							$pos = $_DATAGRID[$row][$col]['pos'];
						}

						if ($col<=$_LAST_FIXED_COL) {
							$_DATA[$index]['html'] = $_DATAGRID[$row][$col]['html'] ?: $_DATAGRID[$row][$col]['text'];
							$_DATA[$index]['cls'] = 'cel '. $_DATAGRID[$row][$col]['cls'];
							$_DATA[$index]['readonly']  = CELL_READONLY;
						}
						elseif ($col >= $_FIRST_REFERENCE_COL && $col <= $_LAST_REFERENCE_COL && !$group) {

							$column = $_DATAGRID[$_ROW_REFERENCE][$col]['column'];
							$item = $_DATAGRID[$_ROW_REFERENCE][$col]['item'];
							$reference = $_DATAGRID[$_ROW_REFERENCE][$col]['reference'];

							$_DATA[$index]['hiddenAsNull'] = 'true';
							$_DATA[$index]['numeric'] = 'true';
							$_DATA[$index]['cls'] = 'cel '. $_DATAGRID[$row][$col]['cls'];
							$_DATA[$index]['text'] = $_POS_PLANNED_QUANTITIES[$pos][$reference]['quantity'];

							if ($_STATE_EDIT) {

								$section = $column."-pos";
								$id = $_POS_PLANNED_QUANTITIES[$pos][$reference]['id'];

								// pattern: [section][pos][reference][id]
								$_DATA[$index]['tag'] = "$section;$item;$pos;$reference;$id";

							} else {
								$_DATA[$index]['readonly']  = CELL_READONLY;
							}

						} else {
							$_DATA[$index]['readonly']  = CELL_READONLY;
						}

					break;

					case $_ROW_WAREHOUSE:

						if ($col<=$_LAST_FIXED_COL) {
							$_DATA[$index]['html'] = $_DATAGRID[$row][$col]['html'] ?: $_DATAGRID[$row][$col]['text'];
							$_DATA[$index]['cls'] = 'cel '. $_DATAGRID[$row][$col]['cls'];
						}

						$_DATA[$index]['readonly']  = CELL_READONLY;

					break;

					case $row >= $_FIRST_WAREHOUSE_ROW && $row <= $_LAST_WAREHOUSE_ROW:

						if ($col<=$_LAST_FIXED_COL) {
							$_DATA[$index]['readonly']  = CELL_READONLY;
							$_DATA[$index]['html'] = $_DATAGRID[$row][$col]['html'] ?: $_DATAGRID[$row][$col]['text'];
							$_DATA[$index]['cls'] = 'cel '. $_DATAGRID[$row][$col]['cls'];
						}
						elseif ($col >= $_FIRST_REFERENCE_COL && $col <= $_LAST_REFERENCE_COL && !$group) {

							$column = $_DATAGRID[$_ROW_REFERENCE][$col]['column'];
							$item = $_DATAGRID[$_ROW_REFERENCE][$col]['item'];
							$reference = $_DATAGRID[$_ROW_REFERENCE][$col]['reference'];
							$warehouse = $_DATAGRID[$row][$_FIRST_COL]['warehouse'];

							$_DATA[$index]['hiddenAsNull'] = 'true';
							$_DATA[$index]['numeric'] = 'true';
							$_DATA[$index]['cls'] = 'cel '. $_DATAGRID[$row][$col]['cls'];
							$_DATA[$index]['text'] = $_WAREHOUSE_PLANNED_QUANTITIES[$warehouse][$reference]['quantity'];

							if ($_STATE_EDIT) {

								$section = $column.'-warehouse';
								$id = $_WAREHOUSE_PLANNED_QUANTITIES[$warehouse][$reference]['id'];

								// pattern: [section][pos][reference][id][cel-total][cel-planned]
								$_DATA[$index]['tag'] = "$section;$item;$warehouse;$reference;$id";
							}
						}
						
					break;
					
					default:
						$_DATA[$index]['readonly']  = CELL_READONLY;
					break;

				} // end switch row
			} // end if separator 
		} // end each column
	} // end eache row
}

// resonding :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_CONSOLE = array(
	'mode' => array(
		'planning' => $_MODE_PLANNING,
		'distribution' => $_MODE_DISTRIBUTION
	),
	'separators' => array(
		'rows' => $_ROW_SEPARATORS,
		'cols' => $_COL_SEPARATORS
	),
	'rows' => array(
		'blank' => $_ROW_BLANK,
		'product.group' => $_ROW_PRODUCT_GROUP,
		'reference' => $_ROW_REFERENCE,
		'quantity' => $_ROW_QUANTITY,
		'planned.quantity' => $_ROW_PLANNED_QUANTITY,
		'first.pos' => $_FIRST_POS_ROW,
		'last.pos' => $_LAST_POS_ROW,
		'warehouse' => $_ROW_WAREHOUSE,
		'first.warehouse' => $_FIRST_WAREHOUSE_ROW,
		'last.warehouse' => $_LAST_WAREHOUSE_ROW,
		'totals' => $_TOTAL_ROWS
	),
	'cols' => array(
		'first' => $_FIRST_COL,
		'first.reference' => $_FIRST_REFERENCE_COL,
		'last.reference' => $_LAST_REFERENCE_COL,
		'totals' => $_TOTAL_COLS
	)
);

header('Content-Type: text/json');
echo json_encode(array(
	'response' => true,
	'data' => $_DATA,
	'merge' => $_COL_SPANS,
	'top' => $_TOTAL_FIXED_ROWS,
	'left' => $_TOTAL_FIXED_COLS,
	'console' => $_CONSOLE,
	'datagrid' => $_DATAGRID
));
