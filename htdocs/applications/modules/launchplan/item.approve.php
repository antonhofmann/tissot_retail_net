<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];
$sendmail = $_REQUEST['sendmail'];

$_STATE_APPROVE = 5;

$model = new Model($application);

$launchplan = new Modul($application);
$launchplan->setTable('lps_launchplans');
$launchplan->read($id);

if ($launchplan->id) {

	// reset all revision items
	$sth = $model->db->prepare("
		UPDATE lps_launchplan_items SET 
			lps_launchplan_item_status = 0
		WHERE lps_launchplan_item_launchplan_id = ?
	");

	$sth->execute(array($launchplan->id));

	// get week quantities
	$sth = $model->db->prepare("
		SELECT 
			lps_launchplan_item_week_quantity_id AS id,
			lps_launchplan_item_week_quantity_item_id AS item,
			lps_launchplan_item_week_quantity_quantity AS quantity,
			lps_launchplan_item_week_quantity_quantity_approved AS approved
		FROM lps_launchplan_item_week_quantities
		INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_week_quantity_item_id
		WHERE lps_launchplan_item_launchplan_id = ?
	");
	
	$sth->execute(array($launchplan->id));
	$result = $sth->fetchAll();

	if ($result) {

		$sum = array();

		// update launch plan item week quantities
		$sth = $model->db->prepare("
			UPDATE lps_launchplan_item_week_quantities SET 
				lps_launchplan_item_week_quantity_quantity_approved = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE lps_launchplan_item_week_quantity_id = ?
		");

		foreach ($result as $row) {
			
			$item = $row['item'];
			$quantity = $row['approved'] ?: $row['quantity'];
			
			$sum[$item]['quantity'][] = $row['quantity'];
			$sum[$item]['approved'][] = $quantity;
			
			$sth->execute(array($quantity, $user->login, $row['id']));
		}

		// updatelaunch plan items
		$sth = $model->db->prepare("
			UPDATE lps_launchplan_items SET 
				lps_launchplan_item_quantity = ?,
				lps_launchplan_item_quantity_approved = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE lps_launchplan_item_id = ?
		");
		
		foreach ($sum as $item => $quantities) {
			$quantity = array_sum(array_filter($quantities['quantity']));
			$approved = array_sum(array_filter($quantities['approved']));
			$sth->execute(array($quantity, $approved, $user->login, $item));
		}
	}

	// update launch plan workflow state
	$sth = $model->db->prepare("
		UPDATE lps_launchplans SET 
			lps_launchplan_workflowstate_id = ?,
			user_modified = ?,
			date_modified = NOW()
		WHERE lps_launchplan_id = ?
	");

	$response = $sth->execute(array($_STATE_APPROVE, $user->login, $launchplan->id));

	if ($response) {
		
		$track = true;
		$json['response'] = true;
		$json['reload'] = true;
		$json['sendmail'] = $sendmail;

		Message::add("Launch plan is succesfully approved.");

	} else {
		$json['response'] = false;
		$json['message']= "Launch plan can not revise.";
	}

	// track workflow state change
	if ($track) {
			
		$workflowState = new Modul($application);
		$workflowState->setTable('lps_workflow_states');
		$workflowState->read($_STATE_APPROVE);
			
		$userTrack = new User_Tracking($application);
		$userTrack->create(array(
			'user_tracking_entity' => 'launch plan',
			'user_tracking_entity_id' => $launchplan->id,
			'user_tracking_user_id' => $user->id,
			'user_tracking_action' => $workflowState->data['lps_workflow_state_name'],
			'user_created' => $user->login,
			'date_created' => date('Y-m-d H:i:s')
		));
	}
}
else {
	$json['response'] = false;
	$json['message']= "Launch plan not found.";
}

header('Content-Type: text/json');
echo json_encode($json);
