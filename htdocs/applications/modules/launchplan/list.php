<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$add_button = $_REQUEST['submit_standard_ordersheet'];
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'country_name, address_company, mastersheet_name';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

// session request
$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

// filters
$fSearch = ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) ? $_REQUEST['search'] : null;
$fYear = $_REQUEST['year'];
$fCountry = $_REQUEST['country'];
$fMasterSheet = $_REQUEST['mastersheet'];
$fWorkflowState = $_REQUEST['workflowstate'];

// db model
$model = new Model($application);

$permissionView = user::permission('can_view_all_lps_sheets');
$permissionViewLimited = user::permission('can_view_only_his_lps_sheets');
$permissionEdit = user::permission('can_edit_all_lps_sheets');
$permissionEditLimited = user::permission('can_edit_only_his_lps_sheets');
$permissionManage = user::permission('can_manage_lps_sheets');


// user has limited permissions
$_LIMITED_ACCESS = !$permissionView && !$permissionEdit && !$permissionManage ? true : false;

$binds = array(
	'INNER JOIN lps_mastersheets ON lps_mastersheet_id = lps_launchplan_mastersheet_id',
	'INNER JOIN lps_workflow_states ON lps_workflow_state_id = lps_launchplan_workflowstate_id',
	'INNER JOIN db_retailnet.addresses ON address_id = lps_launchplan_address_id',
	'INNER JOIN db_retailnet.countries ON country_id = address_country',
	'INNER JOIN db_retailnet.places ON place_id = address_place_id'
);

// filter: limited permission
if($_LIMITED_ACCESS) { 

	$filters['not_in_preparation'] = "lps_launchplan_workflowstate_id <> 1";

	// filter access countries
	$countryAccess = User::getCountryAccess();	
	$filterCountryAccess = $countryAccess ? " OR address_country IN ($countryAccess)" : null;

	// regional access companies
	$regionalAccessCompanies = User::getRegionalAccessCompanies();
	$filterRegionalAccess = $regionalAccessCompanies ? " OR address_parent IN (".join(',', $regionalAccessCompanies).") " : null;

	$filters['limited'] = "(
		address_id = $user->address
		$filterCountryAccess 
		$filterRegionalAccess
	)";
}
	
// filter: full text search
if ($fSearch) {
	$filters['search'] = "(
		address_company LIKE \"%$fSearch%\" 
		OR country_name LIKE \"%$fSearch%\" 
		OR lps_mastersheet_name LIKE \"%$fSearch%\" 
		OR lps_workflow_state_name LIKE \"%$fSearch%\" 
		OR place_name LIKE \"%$fSearch%\"
		OR lps_launchplan_customer_number LIKE \"%$fSearch%\"
	)";
}

// filer: mastersheet year
if ($fYear) {
	$filters['year'] = "lps_mastersheet_year = $fYear";
}

// filer: countries
if ($fCountry) {
	$filters['country'] = "country_id = $fCountry";
}

// filer: mastersheet
if ($fMasterSheet) {
	$filters['mastersheet'] = "lps_launchplan_mastersheet_id = $fMasterSheet";
}

// filer: workflowstate
if ($fWorkflowState) {
	$filters['workflowstate'] = "lps_launchplan_workflowstate_id = $fWorkflowState";
}

if ($archived) $filters['visibility'] = " lps_launchplan_workflowstate_id IN (9,10,11) ";
else $filters['visibility'] = " lps_launchplan_workflowstate_id NOT IN (9,10,11) ";

// datagrid
$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT	
		lps_launchplan_id AS id,
		lps_launchplan_workflowstate_id AS ordersheet_workflowstate_id,
		address_company,
		country_name,
		lps_launchplan_customer_number AS customer_number,
		lps_mastersheet_name AS mastersheet_name,
		lps_workflow_state_name AS workflow_state_name,
		lps_launchplan_openingdate AS ordersheet_openingdate,
		DATE_FORMAT(lps_launchplan_openingdate,'%d.%m.%Y') AS openingdate,
		lps_launchplan_closingdate AS ordersheet_closingdate,
		DATE_FORMAT(lps_launchplan_closingdate,'%d.%m.%Y') AS closingdate
	FROM lps_launchplans
")
->bind($binds)
->filter($filters)
->extend($extendedFilter)
->order($sort, $direction)
->offset($offset, $rowsPerPage)
->fetchAll();

if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);
	$timestamp = date::timestamp();
	$icon_warning = ui::icon('warrning');

	// count order sheet items
	$ordersheetsKeys = join(',', array_keys($datagrid));

	$result = $model->query("
		SELECT 
			lps_launchplan_item_launchplan_id AS id,
			COUNT(lps_launchplan_item_id) AS total
		FROM lps_launchplan_items
		WHERE lps_launchplan_item_launchplan_id IN ($ordersheetsKeys)
		GROUP BY lps_launchplan_item_launchplan_id
	")->fetchAll();
	
	$items = _array::extract($result);

	foreach ($datagrid as $key => $row) {
		
		// dates
		$datagrid[$key]['ordersheet_openingdate'] = $row['openingdate'];
		$datagrid[$key]['ordersheet_closingdate'] = $row['closingdate'];
		
		// expired closing dates
		if ( ($timestamp > date::timestamp($row['closingdate'])) AND in_array($row['ordersheet_workflowstate_id'], array(1,2,3,6))) {
			$datagrid[$key]['ordersheet_closingdate'] = "<span class=error>".$row['closingdate']." !!!</span>";
		}
		
		if (!$items[$key]) {
			$datagrid[$key]['warningicon'] = $icon_warning;
		}
	}

	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";


// add standard master sheet :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($add_button) {
	
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'label' => $translate->add_new
	));
}

// serach full text and bulk operations drop down ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$toolbox[] = ui::searchbox();

if($_REQUEST['manage'] || $_REQUEST['submit'] || $_REQUEST['remind'] || $_REQUEST['exchange_ordersheet_items'] || $_REQUEST['exchange_sap']) {

	$dropdown = array();
	
	if ($_REQUEST['manage']) {
		array_push($dropdown, array(
			'value' => $_REQUEST['manage'],
			'caption' => $MPS ? $translate->bulk_operations : 'Bulk Operations Launch Plans'
		));
	}
	
	if ($_REQUEST['submit']) {
		
		if ($MPS) $caption = "Submit Order Sheets";
		elseif ($LPS) $caption = "Submit Launch Plans";
		else $caption = "Submit";

		array_push($dropdown, array(
			'value' => $_REQUEST['submit'],
			'caption' => $caption
		));
	}
	
	if ($_REQUEST['remind']) {
		
		if ($MPS) $caption = "Remind Order Sheets";
		elseif ($LPS) $caption = "Remind Launch Plans";
		else $caption = "Remind";

		array_push($dropdown, array(
			'value' => $_REQUEST['remind'],
			'caption' => $caption
		));
	}
	
	if ($_REQUEST['exchange_ordersheet_items']) {
		array_push($dropdown, array(
			'value' => $_REQUEST['exchange_ordersheet_items'],
			'caption' => $translate->import_items,
			'class' => 'request_modal'
		));
	}
	
	if ($_REQUEST['exchange_sap'] && $MPS) {
		array_push($dropdown, array(
			'value' => $_REQUEST['exchange_sap'],
			'caption' => 'SAP Import',
			'class' => 'request_modal'
		));
	}
	
	$toolbox[] = ui::dropdown($dropdown, array(
		'name' => 'actions',
		'id' => 'actions',
		'caption' => $translate->actions
	));
}


// mastersheet years :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$result = $model->query("
	SELECT DISTINCT lps_mastersheet_year AS value, lps_mastersheet_year AS caption 
	FROM lps_launchplans
")->bind($binds)
->filter($filters)
->exclude('year')
->order('caption')
->fetchAll(); 

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'year',
		'id' => 'year',
		'class' => 'submit',
		'value' => $fYear,
		'caption' => $translate->all_years
	));
}



// dropdown countries ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("
	SELECT DISTINCT country_id AS value, country_name AS caption 
	FROM lps_launchplans
")
->bind($binds)
->filter($filters)
->exclude('country')
->order('caption')
->fetchAll();


if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'country',
		'id' => 'country',
		'class' => 'submit',
		'value' => $fCountry,
		'caption' => $translate->all_countries
	));
}


// dropdown workflow states ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("
	SELECT DISTINCT lps_workflow_state_id AS value, lps_workflow_state_name AS caption 
	FROM lps_launchplans
")
->bind($binds)
->filter($filters)
->exclude('workflowstate')
->order('caption')
->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'workflowstate',
		'id' => 'workflowstate',
		'class' => 'submit',
		'value' => $fWorkflowState,
		'caption' => $translate->all_workflow_states
	));
}

// dropdown mastersheets :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("
	SELECT DISTINCT lps_mastersheet_id AS value, lps_mastersheet_name AS caption 
	FROM lps_launchplans
")
->bind($binds)
->filter($filters)
->exclude('mastersheet')
->order('caption')
->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'mastersheet',
		'id' => 'mastersheet',
		'class' => 'submit',
		'value' => $fMasterSheet,
		'caption' => $translate->all_mastersheets
	));
}

// toolbox: form
$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";


$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

$table->datagrid = $datagrid;

$table->country_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=15%'
);

$table->address_company(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=20%'
);


$table->customer_number(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

$table->mastersheet_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	"href=".$_REQUEST['form']
);

$table->ordersheet_openingdate(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

$table->ordersheet_closingdate(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

$table->workflow_state_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

if ( !$archived && $datagrid && _array::key_exists('warningicon', $datagrid)) {
	$table->warningicon(
		'width=20px'
	);
}

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;
