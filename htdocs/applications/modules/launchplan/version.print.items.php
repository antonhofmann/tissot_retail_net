<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

ini_set('max_execution_time', 120);
ini_set('memory_limit', '1024M');

define(DEBUGGING_MODE, true);

$settings = Settings::init();
$settings->load('data');
$user = User::instance();
$translate = Translate::instance();

// request vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['ordersheet'];
$version = $_REQUEST['version'];

// db model
$model = new Model($application);

$ownerEditabledStates = array(2,3);
$adminEditabledStates = array(1,2,3,4,5,6);

$stateCompleted = 4;
$stateRevision = 6;


// get launch plan :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$launchplan = new Modul($application);
$launchplan->setTable('lps_launchplans');
$launchplan->read($id);

// mastersheet
$mastersheet = new Modul($application);
$mastersheet->setTable('lps_mastersheets');
$mastersheet->read($launchplan->data['lps_launchplan_mastersheet_id']);

// company
$company = new Company($application);
$company->read($launchplan->data['lps_launchplan_address_id']); 

// workflow state
$workflowState = new Modul($application);
$workflowState->setTable($tableWorkflowState);
$workflowState->read($launchplan->data['lps_launchplan_workflowstate_id']);


// get version data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT *
	FROM lps_launchplan_versions
	WHERE lps_launchplan_version_id = ?
");

$sth->execute(array($version));
$result = $sth->fetch();

$versionCaption = "Version name: ".ucfirst($result['lps_launchplan_version_title']);


// launch plan states ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$permissionView = user::permission('can_view_all_lps_sheets');
$permissionViewLimited = user::permission('can_view_only_his_lps_sheets');
$permissionEdit = user::permission('can_edit_all_lps_sheets');
$permissionEditLimited = user::permission('can_edit_only_his_lps_sheets');
$permissionManage = user::permission('can_manage_lps_sheets');

$state = new OrdersheetState($launchplan->data, $application);

// administrator
$isAdmin = $permissionManage || $permissionEdit ? true : false;

$editabledStates = $state->canAdministrate() ? $adminEditabledStates : $ownerEditabledStates;

// can edit order sheet
$canEdit = ($isAdmin || $permissionEditLimited) ? true : false;

// expired ordersheets on planning phase
if ($state->owner) {
	if (!$state->open || $state->close  ) $canEdit = false;
	else $canEdit = ($editabledStates && in_array($state->state, $editabledStates)) ? $permissionEditLimited : false;
}

// show approvment
$showApprovedQuantities = ($isAdmin || $state->state==5 || $state->state > 6) ? true : false;

// readonly mod
$modReadonly =  ($state->isDistributed() || !in_array($state->state, $editabledStates)) ? true: false;


// output file :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// page title
$pagetitle = ($archived) ? 'Archived Launch Plan Items' : 'Launch Plan Items';

// title
$title = $company->company.', '.$mastersheet->data['name'].', '.$mastersheet->data['year'];

// file name
$filename  = "launchplan_";
$filename .= str_replace(' ', '_', strtolower($company->company));
$filename .= str_replace(' ', '_', strtolower($mastersheet->mastersheet_name));
$filename .= "-version-$version";
$filename .= "-".date('Y-m-d');


// get launch plan version items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEMS = array();

$sth = $model->db->prepare("
	SELECT DISTINCT
		lps_launchplan_version_item_id AS id,
		lps_launchplan_version_item_price AS price, 
		lps_launchplan_version_item_exchangrate AS currency_exchangrate, 
		lps_launchplan_version_item_factor AS currency_factor, 
		lps_launchplan_version_item_quantity AS quantity, 
		lps_launchplan_version_item_quantity_approved AS quantity_approved, 
		lps_launchplan_version_item_quantity_estimate AS quantity_estimate, 
		lps_launchplan_version_item_status AS status, 
		lps_launchplan_version_item_comment AS reference_description,
		lps_reference_code AS reference_code, 
		lps_reference_name AS reference_name, 
		lps_collection_code AS collection_code,
		lps_mastersheet_estimate_month AS estimate_month,							
		lps_product_group_id AS group_id, 
		lps_product_group_name AS group_name, 
		lps_collection_category_id AS subgroup_id,
		lps_collection_category_code AS subgroup_name,
	    currency_symbol AS currency_symbol
	FROM lps_launchplan_version_items 
	INNER JOIN lps_launchplan_versions ON lps_launchplan_versions.lps_launchplan_version_id = lps_launchplan_version_items.lps_launchplan_version_item_version_id
	INNER JOIN lps_mastersheets ON lps_mastersheets.lps_mastersheet_id = lps_launchplan_versions.lps_launchplan_version_mastersheet_id
	INNER JOIN lps_references ON lps_references.lps_reference_id = lps_launchplan_version_items.lps_launchplan_version_item_reference_id
	INNER JOIN lps_product_groups ON lps_references.lps_reference_product_group_id = lps_product_groups.lps_product_group_id
	INNER JOIN lps_collections ON lps_references.lps_reference_collection_id = lps_collections.lps_collection_id
	INNER JOIN lps_collection_categories ON lps_references.lps_reference_collection_category_id = lps_collection_categories.lps_collection_category_id
	INNER JOIN db_retailnet.currencies ON db_retailnet.currencies.currency_id = lps_launchplan_version_items.lps_launchplan_version_item_currency
	WHERE lps_launchplan_version_id = ?
	ORDER BY 
		lps_product_group_name,
		lps_collection_category_code,
		lps_collection_code,
		lps_reference_code
");

$sth->execute(array($version));
$result = $sth->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$group = $row['group_id'];
		$item = $row['id'];	
		$_ITEMS[$group]['caption'] = $row['group_name'];
		$_ITEMS[$group]['data'][$item]['collection_code'] = $row['collection_code'];
		$_ITEMS[$group]['data'][$item]['reference_code'] = $row['reference_code'];
		$_ITEMS[$group]['data'][$item]['reference_name'] = $row['reference_name'];
		$_ITEMS[$group]['data'][$item]['reference_description'] = $row['reference_description'];
		$_ITEMS[$group]['data'][$item]['estimate_month'] = $row['estimate_month'];
		$_ITEMS[$group]['data'][$item]['price'] = $row['price'];
		$_ITEMS[$group]['data'][$item]['currency_symbol'] = $row['currency_symbol'];
		$_ITEMS[$group]['data'][$item]['currency_exchangrate'] = $row['currency_exchangrate'];
		$_ITEMS[$group]['data'][$item]['currency_factor'] = $row['currency_factor'];
		$_ITEMS[$group]['data'][$item]['quantity'] = $row['quantity'];
		$_ITEMS[$group]['data'][$item]['quantity_estimate'] = $row['quantity_estimate'];
		$_ITEMS[$group]['data'][$item]['quantity_approved'] = $row['quantity_approved'];
	}
}


// get item week quantities ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEM_WEEK_QUANTITIES = array();
	
$sth = $model->db->prepare("
	SELECT DISTINCT
		lps_launchplan_version_item_week_quantity_id AS id,
		lps_launchplan_version_item_week_quantity_item_id AS item,
		lps_launchplan_version_item_week_quantity_week AS week,
		lps_launchplan_version_item_week_quantity_quantity AS quantity,
		lps_launchplan_version_item_week_quantity_approved AS quantity_approved
	FROM lps_launchplan_version_item_week_quantities
	INNER JOIN lps_launchplan_version_items ON lps_launchplan_version_items.lps_launchplan_version_item_id = lps_launchplan_version_item_week_quantity_item_id
	WHERE lps_launchplan_version_item_version_id = ?
");

$sth->execute(array($version));
$result = $sth->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$item = $row['item'];
		$week = $row['week'];
		$_ITEM_WEEK_QUANTITIES[$item][$week] = array(
			'id' => $row['id'],
			'quantity' => $row['quantity'],
			'quantity_approved' => $row['quantity_approved']
		);
	}
}


// get items planned quantities ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEM_PLANNED_QUANTITIES = array();

$sth = $model->db->prepare("
	SELECT 
		lps_launchplan_item_planned_quantity_item_id AS item,
		SUM(lps_launchplan_item_planned_quantity_quantity) AS total_quantities
	FROM lps_launchplan_item_planned_quantities
	INNER JOIN lps_launchplan_items ON lps_launchplan_item_id = lps_launchplan_item_planned_quantity_item_id
	WHERE lps_launchplan_item_launchplan_id = ?
	GROUP BY lps_launchplan_item_planned_quantity_item_id
");

$sth->execute(array($launchplan->id));
$result = $sth->fetchAll();
$_ITEM_PLANNED_QUANTITIES = _array::extract($result);


// spreadsheet fixed area ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// fixed rows
$fixedRows = 2;
$firstFixedRow = 1;
$lastFixedRow = 1;
$fixedRowsSeparator = 0;

// fixed columns
$fixedColumns = 9;
$firstFixedColumn = 1;
$lastFixedColumn = 9;
$fixedColumnSeparator = 0;
$totalFixedRows = $fixedRows + $fixedRowsSeparator;
$totalFixedColumns = $fixedColumns + $fixedColumnSeparator;

$rowFilterCaptions = 2;

// datagrid
$datagrid = array();
$rowGroupSeparator = 1;

$firstWeek = $mastersheet->data['lps_mastersheet_week_number_first'];
$totalWeeks = $mastersheet->data['lps_mastersheet_weeks'];
$estimateMonth = $mastersheet->data['lps_mastersheet_estimate_month'];


// spreadsheet data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_ITEMS) {

	$row=$totalFixedRows;

	foreach ($_ITEMS as $g => $group) {

		$row++;
		$rowGroups[$row] = true;
		
		$col=$firstFixedColumn;
		$datagrid[$row][$col]['caption'] = $group['caption'];
		$datagrid[$row][$col]['group'] = $g;

		$row++;
		$rowGroupHeaders[$row] = true;

		$col=$firstFixedColumn;
		$datagrid[$row][$col]['caption'] = 'Collection Code';
		$datagrid[$row][$col]['header'] = true;
		$datagrid[$row][$col]['width'] = 20;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Reference';
		$datagrid[$row][$col]['header'] = true;
		$datagrid[$row][$col]['width'] = 20;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Name';
		$datagrid[$row][$col]['header'] = true;
		$datagrid[$row][$col]['width'] = 30;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Comment';
		$datagrid[$row][$col]['header'] = true;
		$datagrid[$row][$col]['width'] = 20;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Estimated Price';
		$datagrid[$row][$col]['header'] = true;
		$datagrid[$row][$col]['width'] = 20;
		$datagrid[$row][$col]['numeric'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Currency';
		$datagrid[$row][$col]['header'] = true;
		$datagrid[$row][$col]['width'] = 10;

		$col++;
		$datagrid[$row][$col]['caption'] = "Estimated for $estimateMonth months";
		$datagrid[$row][$col]['header'] = true;
		$datagrid[$row][$col]['width'] = 25;
		$datagrid[$row][$col]['numeric'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = 'Total Planned';
		$datagrid[$row][$col]['header'] = true;
		$datagrid[$row][$col]['width'] = 15;
		$datagrid[$row][$col]['numeric'] = true;

		$col++;
		$datagrid[$row][$col]['caption'] = '% of Estimate';
		$datagrid[$row][$col]['header'] = true;
		$datagrid[$row][$col]['width'] = 20;
		$datagrid[$row][$col]['numeric'] = true;

		for ($i=0; $i < $totalWeeks; $i++) { 
			$col++;
			$week = $firstWeek+$i;
			$datagrid[$row][$col]['caption'] = "Week $week";
			$datagrid[$row][$col]['header'] = true;
			$datagrid[$row][$col]['numeric'] = true;
		}

		$col++;
		$datagrid[$row][$col]['caption'] = 'Total Launch';
		$datagrid[$row][$col]['header'] = true;
		$datagrid[$row][$col]['width'] = 15;
		$datagrid[$row][$col]['numeric'] = true;

		if ($showApprovedQuantities) {
			
			for ($i=0; $i < $totalWeeks; $i++) { 
				$col++;
				$week = $firstWeek+$i;
				$datagrid[$row][$col]['caption'] = "Week $week";
				$datagrid[$row][$col]['header'] = true;
				$datagrid[$row][$col]['numeric'] = true;
			}

			$col++;
			$datagrid[$row][$col]['caption'] = 'Total Approved';
			$datagrid[$row][$col]['header'] = true;
			$datagrid[$row][$col]['width'] = 15;
			$datagrid[$row][$col]['numeric'] = true;
		}

		// total datagrid columns
		$totalDataGridColumns = $col;
			
		foreach ($group['data'] as $key => $item) {

			$row++;
			$rowGrid[$row] = true;
			
			$col=$firstFixedColumn;
			$datagrid[$row][$col]['value'] = $item['collection_code'];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['reference_code'];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['reference_name'];			

			$col++;
			$datagrid[$row][$col]['value'] = $item['reference_description'];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['price'];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['currency_symbol'];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['quantity_estimate'];
			$datagrid[$row][$col]['item'] = $key;
			
			$col++;
			$datagrid[$row][$col]['value'] = $_ITEM_PLANNED_QUANTITIES[$key];
			
			$col++;
			$datagrid[$row][$col]['value'] = $item['estimate_month'] ? ($item['total_planned']/$item['estimate_month'])*100 : null;

			for ($i=0; $i < $totalWeeks; $i++) { 
				
				$col++;
				$week = $firstWeek+$i;
				
				$datagrid[$row][$col]['item'] = $key;
				$datagrid[$row][$col]['week'] = $week;
				$datagrid[$row][$col]['id'] = $_ITEM_WEEK_QUANTITIES[$key][$week]['id'];
				$datagrid[$row][$col]['value'] = $_ITEM_WEEK_QUANTITIES[$key][$week]['quantity'];
			}

			// col total quantities
			$col++;
			$datagrid[$row][$col]['value'] = null;

			if ($showApprovedQuantities) {
				
				for ($i=0; $i < $totalWeeks; $i++) { 
					
					$col++;
					$week = $firstWeek+$i;
					
					$datagrid[$row][$col]['item'] = $key;
					$datagrid[$row][$col]['week'] = $week;
					$datagrid[$row][$col]['id'] = $_ITEM_WEEK_QUANTITIES[$key][$week]['id'];
					$datagrid[$row][$col]['value'] = $_ITEM_WEEK_QUANTITIES[$key][$week]['quantity_approved'];
				}

				// col total approved quantities
				$col++;
				$datagrid[$row][$col]['value'] = null;
			}
		}

		$row++;
		$rowGroupSubtotals[$row] = true;

		$col=$firstFixedColumn;
		$datagrid[$row][$col]['caption'] = 'Total '.$group['caption'];

		$row++;
		$rowGroupSeparators[$row] = true;

		$col=$firstFixedColumn;
		$datagrid[$row][$col]['separator'] = true;
	}

	$row++;
	$rowGroupSeparators[$row] = true;

	// grand total
	$row++;
	$rowGrandTotal = $row;
	$datagrid[$row][$firstFixedColumn]['caption'] = 'Launch Plan Totals';

	$totalDataGridRows = $row-1;
}


// spreadsheet main triggers :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// planned column week triggers
$firstWeekCol = $totalFixedColumns + 1;
$lastWeekCol = $firstWeekCol + $totalWeeks - 1;
$totalWeekCol = $lastWeekCol + 1;

// approved column week triggers
$firstApprovedWeekCol = $lastWeekCol + 2;
$lastApprovedWeekCol = $firstApprovedWeekCol + $totalWeeks - 1;
$totalApprovedWeekCol = $lastApprovedWeekCol + 1;

// dedicated triggers
$colRevision = 1;
$colEstimateQuantity = $firstWeekCol - 3;
$colPlanningQuantity = $firstWeekCol - 2;
$colEstimateQuote = $firstWeekCol - 1;

// grid triggers
$firstDataGridRow = $totalFixedRows+1;
$lastDataGridRow = $firstDataGridRow + $totalDataGridRows - 1;
$firstDataGridCol = 1;

// global sheet triggers
$totalRows = $totalFixedRows + $totalDataGridRows;
$totalColumns = $totalDataGridColumns;
$firstSheetRow = $firstFixedRow;
$lastSheetRow = $totalRows;
$firstSheetColumn = $firstFixedColumn;
$lastSheetColumn = $totalColumns;

// spreadsheet styles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$styles = array(

	'allborders' => array(
		'borders' => array( 
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),

	'number' => array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
		)
	),
	
	'string' => array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
		)
	),
	
	'total-group' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12, 
			'bold'=>true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'f3f3f3')
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	
	'total-sheet' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 14, 
			'bold'=>true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'dedede')
		),
		'alignment' => array(
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),
	
	'group-header' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 16, 
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		)
	),
	
	'table-header' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 11, 
			'bold'=>true,
			'color' => array('rgb'=>'FFFFFF')
		),
		'alignment' => array(
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'000000')
		),
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		)
	),

	'sheet-caption' => array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 20, 
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		)
	)
);



// spreadsheet data builder ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($totalRows && $totalColumns) {

	// excel init
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name)->setTitle($pagetitle);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

	// sheet columns
	$firstColumn = spreadsheet::key($firstSheetColumn);
	$lastColumn = spreadsheet::key($lastSheetColumn);

	for ($row=1; $row <= $totalRows; $row++) {
			
		for ($col=1; $col <= $totalColumns; $col++) {

			$column = spreadsheet::key($col);
			$index = "$column$row";
			$rowRange = "$firstColumn$row:$lastColumn$row";

			switch ($row) {
				
				// sheet title
				case $firstSheetColumn;		

					if ($col==1) {
						$objPHPExcel->getActiveSheet()->mergeCells($rowRange);
						$objPHPExcel->getActiveSheet()->setCellValue($index, $title);
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['sheet-caption']);
						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(50);
					}

				break;

				// filters
				case $rowFilterCaptions:

					$objPHPExcel->getActiveSheet()->mergeCells($rowRange);
					$objPHPExcel->getActiveSheet()->setCellValue($index, $versionCaption);

				break;

				// group title: collection
				case $rowGroups[$row]:

					if ($col==$firstFixedColumn) {
						$group = $datagrid[$row][$col]['group'];
						$objPHPExcel->getActiveSheet()->mergeCells($rowRange);
						$objPHPExcel->getActiveSheet()->setCellValue($index, $datagrid[$row][$col]['caption']);
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['group-header']);
						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(40);
					}

				break;
			
				// group table header
				case $rowGroupHeaders[$row]:
					
					$objPHPExcel->getActiveSheet()->setCellValue($index, $datagrid[$row][$col]['caption']);

					if ($datagrid[$row][$col]['width']) {
						$objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth($datagrid[$row][$col]['width']);
					}

					if ($col==$lastSheetColumn) {
						$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
						$objPHPExcel->getActiveSheet()->getStyle($rowRange)->applyFromArray($styles['table-header'], false);
					}

					if ($col==$colEstimateQuote) {
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['number']);
					}

				break;

				// lanchplan items
				case $rowGrid[$row]:

					$value = $datagrid[$row][$col]['value'];
					
					switch ($col) {

						// estimated quantity
						case $colEstimateQuantity:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
							$colsum['estimate-quantity'][$col][$group] = $colsum['estimate-quantity'][$col][$group] + $value;
						break;

						case $colPlanningQuantity:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
							$colsum['planned-quantity'][$col][$group] = $colsum['planned-quantity'][$col][$group] + $value;
						break;

						// estimated quote
						case $colEstimateQuote:
							$plannedQuantity  = $datagrid[$row][$col-1] ? $datagrid[$row][$col-1]['value'] : false;
							$estimateQuantity = $datagrid[$row][$col-2] ? $datagrid[$row][$col-2]['value'] : false;
							$quantity = $plannedQuantity>0 && $estimateQuantity>0 ? ($plannedQuantity/$estimateQuantity)*100 : 0;
							$objPHPExcel->getActiveSheet()->setCellValue($index, "$quantity %");
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['number']);
							$colsum['estimate-quote'][$col][$group] = $colsum['estimate-quote'][$col][$group] + $value;
						break;
						
						// week quantity
						case $col >= $firstWeekCol && $col <= $lastWeekCol:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
							$rowsum['week-planned'][$row] = $rowsum['week-planned'][$row] + $value;
							$colsum['week-quantity'][$col][$group] = $colsum['week-quantity'][$col][$group] + $value;
						break;

						// total week planned quantites
						case $totalWeekCol:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $rowsum['week-planned'][$row]);
							$colsum['week-quantity-total'][$col][$group] = $colsum['week-quantity-total'][$col][$group] + $rowsum['week-planned'][$row];
						break;

						// week approved quantity
						case $col >= $firstApprovedWeekCol && $col <= $lastApprovedWeekCol:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
							$rowsum['week-approved'][$row] = $rowsum['week-approved'][$row] + $value;
							$colsum['week-approved'][$col][$group] = $colsum['week-approved'][$col][$group] + $value;
						break;

						// total week approved quantites
						case $totalApprovedWeekCol: 
							$objPHPExcel->getActiveSheet()->setCellValue($index, $rowsum['week-approved'][$row]);
							$objPHPExcel->getActiveSheet()->getStyle($rowRange)->applyFromArray($styles['allborders'], false);
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
							$colsum['week-approved-total'][$col][$group] = $colsum['week-approved-total'][$col][$group] + $rowsum['week-approved'][$row];
						break;

						default:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
						break;
					}
					
				break;

				// collection subtotal
				case $rowGroupSubtotals[$row]:

					switch ($col) {
						
						// subtotal caption
						case $firstFixedColumn:
							$range = "$index:".spreadsheet::key($col+5,$row);
							$objPHPExcel->getActiveSheet()->mergeCells($range);
							$objPHPExcel->getActiveSheet()->setCellValue($index, $datagrid[$row][$col]['caption']);
						break;
						
						// estimated quantity
						case $colEstimateQuantity:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['estimate-quantity'][$col][$group]);
							$colsum['grand-estimate-quantity'][$col] = $colsum['grand-estimate-quantity'][$col] + $colsum['estimate-quantity'][$col][$group];
						break;
						
						// planned quantity
						case $colPlanningQuantity:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['planned-quantity'][$col][$group]);
							$colsum['grand-planned-quantity'][$col] = $colsum['grand-planned-quantity'][$col] + $colsum['planned-quantity'][$col][$group];
						break;

						// estimated quote
						case $colEstimateQuote:
							$plannedQuantity  = $colsum['planned-quantity'][$col-1][$group];
							$estimateQuantity = $colsum['estimate-quantity'][$col-2][$group];
							$quantity = $plannedQuantity>0 && $estimateQuantity>0 ? number_format(($plannedQuantity/$estimateQuantity)*100, 2, ".", "'") : 0;
							$objPHPExcel->getActiveSheet()->setCellValue($index, "$quantity %");
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['number']);
						break;

						// week quantity
						case $col >= $firstWeekCol && $col <= $lastWeekCol:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['week-quantity'][$col][$group]);
							$colsum['grand-week-quantity'][$col] = $colsum['grand-week-quantity'][$col] + $colsum['week-quantity'][$col][$group];
						break;

						// total week planned quantites
						case $totalWeekCol:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['week-quantity-total'][$col][$group]);
							$colsum['grand-week-quantity-total'][$col] = $colsum['grand-week-quantity-total'][$col] + $colsum['week-quantity-total'][$col][$group];
						break;

						// week approved quantity
						case $col >= $firstApprovedWeekCol && $col <= $lastApprovedWeekCol:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['week-approved'][$col][$group]);
							$colsum['grand-week-approved'][$col] = $colsum['grand-week-approved'][$col] + $colsum['week-approved'][$col][$group];
						break;

						// total week approved quantites
						case $totalApprovedWeekCol:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['week-approved-total'][$col][$group]);
							$objPHPExcel->getActiveSheet()->getStyle($rowRange)->applyFromArray($styles['total-group'], false);
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
							$colsum['grand-week-approved-total'][$col] = $colsum['grand-week-approved-total'][$col] + $colsum['week-approved-total'][$col][$group];
						break;
					}

				break;

				case $rowGrandTotal:
					
					switch ($col) {

						// grand total caption
						case $firstFixedColumn:
							$range = "$index:".spreadsheet::key($col+5,$row);
							$objPHPExcel->getActiveSheet()->mergeCells($range);
							$objPHPExcel->getActiveSheet()->setCellValue($index, $datagrid[$row][$col]['caption']);
						break;
						
						// estimated quantity
						case $colEstimateQuantity:
							$objPHPExcel->getActiveSheet()->setCellValue($index,  $colsum['grand-estimate-quantity'][$col]);
						break;

						case $colPlanningQuantity:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['grand-planned-quantity'][$col]);
						break;

						// estimated quote
						case $colEstimateQuote:
							$plannedQuantity  = $colsum['grand-planned-quantity'][$col-1];
							$estimateQuantity = $colsum['grand-estimate-quantity'][$col-2];
							$quantity = $plannedQuantity>0 && $estimateQuantity>0 ? number_format(($plannedQuantity/$estimateQuantity)*100, 2, ".", "'") : 0;
							$objPHPExcel->getActiveSheet()->setCellValue($index, "$quantity %");
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['number']);
						break;
						
						// week quantity
						case $col >= $firstWeekCol && $col <= $lastWeekCol:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['grand-week-quantity'][$col]);
						break;

						// total week planned quantites
						case $totalWeekCol:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['grand-week-quantity-total'][$col]);
						break;

						// week approved quantity
						case $col >= $firstApprovedWeekCol && $col <= $lastApprovedWeekCol:
							$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['grand-week-approved'][$col]);
						break;

						// total week approved quantites
						case $totalApprovedWeekCol: 
							$objPHPExcel->getActiveSheet()->setCellValue($index, $colsum['grand-week-approved-total'][$col]);
							$objPHPExcel->getActiveSheet()->getStyle($rowRange)->applyFromArray($styles['total-sheet'], false);
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
						break;
					}

				break;

				// collection separators
				case $rowGroupSeparators[$row]:
					
					if ($col==1) {
						$objPHPExcel->getActiveSheet()->mergeCells($rowRange);
					}

				break;
			}
		}
	}	

	$objPHPExcel->getActiveSheet()->setTitle($pagetitle);
	$objPHPExcel->setActiveSheetIndex(0);

	// redirect output to client browser
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
}
else {
	message::empty_result();
	url::redirect("/$application/$controller$archived/$action/$id");
}
