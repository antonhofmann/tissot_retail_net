<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
$user = User::instance();
$translate = Translate::instance();

$_APP = $_REQUEST['application'];
$_TPL = $_REQUEST['mail_template_id'];
$_LAUNCHPLANS = $_REQUEST['launchplans'];
$_SUBJECT = $_REQUEST['mail_template_subject'];
$_CONTENT = $_REQUEST['mail_template_text'];

if (!$_LAUNCHPLANS || !$_TPL) {
	
	$_RESPONSE['notification'] = "Bad request";
	
	if (!$_LAUNCHPLANS) {
		$_RESPONSE['notification'] = "Select at least one launch plan. ";
	}

	goto BLOCK_RESPONSE;
}

// action mail
$mail = new ActionMail($_REQUEST['mail_template_id']);

// set parameters
$mail->setParam('application', $_APP);
$mail->setParam('launchplans', $_LAUNCHPLANS);	
	
// mail content
if ($_SUBJECT) $mail->setSubject($_SUBJECT);
if ($_CONTENT) $mail->setBody($_CONTENT);
	
$_RESPONSE = $mail->getMailData();

BLOCK_RESPONSE:	
header('Content-Type: text/json');
echo json_encode($_RESPONSE);