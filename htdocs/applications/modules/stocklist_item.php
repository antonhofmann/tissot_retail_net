<?php

	class Stocklist_Item { 
		
		const DB_BIND_STOCKLISTS = 'INNER JOIN db_retailnet.scpps_stocklists ON scpps_stocklist_id = scpps_stocklist_item_stocklist_id';
		const DB_BIND_ITEMS = 'INNER JOIN items ON item_id = scpps_stocklist_item_item_id';

		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : Connector::DB_CORE;
			$this->model = new Stocklist_Item_Model($connector);
		}
		
		public function stocklist($id) {
			$this->id = $id;
		}
		
		public function read($item) {
			if ($this->id) {
				return $this->model->read($this->id, $item);
			}
		}
		
		public function create($item) {
			if ($this->id) {
				return $this->model->create($this->id, $item);
			return $id;
			}
		}

		public function delete($item) {
			if ($this->id) {
				return $this->model->delete($this->id, $item);
			}
		}
	
		public function load() {
			if ($this->id) {
				return $this->model->load($this->id);
			}
		}
	}