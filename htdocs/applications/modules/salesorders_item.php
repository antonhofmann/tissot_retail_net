<?php 

	class Salesorders_Item {
		
		public $data;
		
		public function __construct() {
			$this->model = new Salesorders_Item_Model(Connector::DB_RETAILNET_EXCHANGES);
		}
		
		public function __get($key) {
			return $this->data["mps_salesorderitem_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_salesorderitem_id'];
			return $this->data;
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
	
	}