<?php

	/**
	 * Master Sheet
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Mastersheet {
		
		/**
		 * Master Sheet ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Master Sheet Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Master Sheet Item
		 * @var objec
		 */
		public $item;
		
		/**
		 * Master Sheet File
		 * @var objec
		 */
		public $file;
		
		/**
		 * Master Sheet Delivery Date
		 * @var object
		 */
		public $delivery_date;
		
		/**
		 * Master Sheet Splitting List
		 * @var object
		 */
		public $splittinglist;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @return Mastersheet_Model
		 */
		protected $model;
		
		/**
		 * Edit all master sheets
		 * @var string
		 */
		const PERMISSION_EDIT = 'can_edit_mps_master_sheets';
		
		/**
		 * View all master sheets
		 * @var string
		 */
		const PERMISSION_VIEW = 'can_edit_mps_master_sheets';
		
		/**
		 * DB join master sheet items
		 * @var string
		 */
		const DB_BIND_MASTERSHEET_ITEMS = 'INNER JOIN mps_mastersheet_items ON mps_mastersheet_item_mastersheet_id = mps_mastersheet_id';
	
		/**
		 * DB join master sheet delivery dates
		 * @var string
		 */
		const DB_BIND_MASTERSHEET_DELIVERY_DATES = 'INNER JOIN mps_mastersheet_delivery_dates ON mps_mastersheet_delivery_date_mastersheet_id = mps_mastersheet_id';

		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Mastersheet_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_mastersheet_$key"];
		}
		
		public function read($id) { 
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_mastersheet_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
	
		/**
		 * Master Sheet Item
		 * @return Mastersheet_Item
		 */
		public function item() {
			
			if ($this->id && !$this->item) {
				$this->item = new Mastersheet_Item($this->connector);
			}

			$this->item->mastersheet = $this->id;
			return $this->item;
		}
		
		/**
		 * Master Sheet File
		 * @return Mastersheet_File
		 */
		public function file() {
			
			if (!$this->file) {
				$this->file = new Mastersheet_File($this->connector);
			}
				
			$this->file->mastersheet = $this->id;
			return $this->file;
		}
		
		/**
		 * Master Sheet Delivery Date
		 * @return Mastersheet_Delivery_Date
		 */
		public function deliveryDate() {
				
			if (!$this->delivery_date) {
				$this->delivery_date = new Mastersheet_Delivery_Date($this->connector);
			}
		
			$this->delivery_date->mastersheet = $this->id;
			return $this->delivery_date;
		}
		
		/**
		 * Master Sheet Splitting List
		 * @return Mastersheet_Splitting_List
		 */
		public function splittinglist() {
				
			if (!$this->splittinglist) {
				$this->splittinglist = new Mastersheet_Splitting_List($this->connector);
			}
		
			$this->splittinglist->mastersheet = $this->id;
			return $this->splittinglist;
		}
		
		/**
		 * Master Sheet Header Caption
		 * @return string Master Sheet Name, Master Sheet Year
		 */
		public function header() {
			return ($this->id) ? $this->name.', '.$this->year : null;
		}
		
		/**
		 * Master Sheet Dropdown Loader
		 * @param string $connector, DB connector identificator
		 * @param array $filters, query filters
		 * @return master sheets array
		 */
		public static function dropdownLoader($connector, $filters=null) {
			$model = new Mastersheet_Model($connector);
			$result = $model->dropdownLoader($connector, $filters);
			return ($result) ? _array::extract($result) : null;
		}

		/**
		 * Check if master sheet has exported items
		 * 
		 * @return boolean [description]
		 */
		public function hasExportedItems() {

			if ($this->id && $this->consolidated) {
				$result = $this->model->getExportedItems($this->id); 
				return count($result) > 0 ? true : false;
			}
		}
	}
	