<?php

	/**
	 * Master Sheet File
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Mastersheet_File {
		
		/**
		 * File ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * File Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Master Sheet ID
		 * @var integer
		 */
		public $mastersheet;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @var Mastersheet_File_Model
		 */
		protected $model;
		
		/**
		 * DB join file types
		 * @var string
		 */
		const DB_BIND_FILE_TYPES = 'INNER JOIN db_retailnet.file_types ON file_type_id = mps_mastersheet_file_type';
		
		/**
		 * Join order sheets
		 * @var string
		 */
		const DB_BIND_ORDERSHEETS = 'INNER JOIN mps_ordersheets ON mps_ordersheet_mastersheet_id = mps_mastersheet_file_mastersheet_id';
		
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Mastersheet_File_Model($this->connector);
		}
	
		public function __get($key) {
			return $this->data["mps_mastersheet_file_$key"];
		}
		
		public function mastersheet($mastersheet) {
			$this->mastersheet = $mastersheet;
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->mastersheet = $this->data['mps_mastersheet_file_mastersheet_id'];
			$this->id = $this->data['mps_mastersheet_file_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				return $result;
			}
		}
		
		public function delete($file=true) {
			
			if ($this->id) {
				
				if ($file) {
					
					// remove file from server
					file::remove($this->path);
					
					// remove directory if is empty
					$dir = $this->dirpath();
					dir::remove($dir, false);
				}
				
				return $this->model->delete($this->id);
			}
		}
		
		/**
		 * Remove all master sheet files
		 * @param boolean $file, remove files also
		 * @return boolean true if records are removed
		 */
		public function deleteAll($file=true) {
			
			if ($this->mastersheet) {
				
				$files = self::load($this->mastersheet, $this->connector);
				
				if ($files) {
					dir::remove($this->dirpath());
					return $this->model->deleteAll($this->mastersheet);
				}
			}
		}
		
		public function dirpath() {
			if ($this->mastersheet) {
				return '/public/data/files/'.$this->connector.'/mastersheets/'.$this->mastersheet;
			}
		}
		
		/**
		 * Load all master sheet files
		 * @param integer $mastersheet, Master Sheet ID
		 * @param string $connector, DB Connector
		 * @return array, all master sheet files
		 */
		public static function load($mastersheet, $connector) {
			$model = new Mastersheet_File_Model($connector);
			$result = $model->load($mastersheet);
			return ($result) ? _array::datagrid($result) : null;
		}
	}