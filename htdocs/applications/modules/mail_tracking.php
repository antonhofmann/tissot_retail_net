<?php 

	class Mail_Tracking {
		
		public $data;

		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : Connector::DB_CORE;
			$this->model = new Mail_Tracking_Model($connector);
		}
		
		public function __get($key) {
			return $this->data["mail_tracking_$key"];
		}
		
		public function create($data) {
			if (is_array($data)) {
				return $this->model->create($data);
			}
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mail_tracking_id'];
			return $this->data;
		}
		
		public function delete($id) {
			return $this->model->delete($id);
		}

		/**
		 * Was this mail already tracked
		 * @param  integer $recipientID
		 * @return boolean
		 */
		public function wasTracked($recipientID) {
			return $this->model->wasMailSentTo($this->id, $recipientID);
		}
	}