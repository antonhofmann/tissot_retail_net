<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	if($_REQUEST['store_locator_country_id'] > 0 and $_REQUEST['language_id'])
	{
		$model_store_locator = new Model(CONNECTOR::DB_RETAILNET_STORE_LOCATOR);
		
		
		//check if translation is present in db retail net
		$sth = $model_store_locator->db->prepare("select count(country_id) as num_recs from country_translations
				WHERE country_id = ? and language_id = ?");
		$response = $sth->execute(array($_REQUEST['store_locator_country_id'], $_REQUEST['language_id']));

		if($response == true)
		{
			$row = $sth->fetch();
			if($row['num_recs'] == 0)
			{
				$query = "Insert into country_translations (country_id, language_id, name) VALUES (?,?,?)";

				$sth = $model_store_locator->db->prepare($query);
				$response = $sth->execute(array($_REQUEST['store_locator_country_id'], $_REQUEST['language_id'], $_REQUEST['country_name']));

			}
			else
			{
				$query = "UPDATE country_translations SET 
					name = ? 
				WHERE country_id = ? and language_id = ?";
				
				//update store locator
				$sth = $model_store_locator->db->prepare($query);
				$response = $sth->execute(array($_REQUEST['country_name'], $_REQUEST['store_locator_country_id'], $_REQUEST['language_id']));
			}
		}
	}

?>