<?php 

	class Ordersheet_Exchange {
		
		public $ordersheet;
		
		public $totalApprovedItems;
		
		public $totalApprovedQuantites;
		
		public $totalConfirmedItems;
		
		public $totalConfirmedQuantities;
		
		public $totalShippedItems;
		
		public $totalShippedQuantities;
		
		public $totalCompletedItems;
		
		public $totalCancelledItems;
		
		public function __construct($ordersheet, $connector) {
			
			$this->ordersheet = $ordersheet;
			
			$model = new Model($connector);
			
			$items = $model->query("
				SELECT 
					mps_ordersheet_item_id,
					mps_ordersheet_item_quantity_approved,
					mps_ordersheet_item_quantity_confirmed,
					mps_ordersheet_item_quantity_shipped	
				FROM mps_ordersheet_items
				INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
				WHERE mps_ordersheet_item_ordersheet_id = $ordersheet
				AND mps_material_locally_provided != 1 
				AND mps_ordersheet_item_quantity_approved > 0
				AND mps_ordersheet_item_purchase_order_number > 0
			")->fetchAll();
			
			if ($items) { 
				
				$confirmed = array();
				$shipped = array();
				$shortclose = array();
				
				// confirmed items
				$result = $model->query("
					SELECT 
						mps_ordersheet_item_id,
						mps_ordersheet_item_confirmed_quantity,
						mps_ordersheet_item_confirmed_order_state
					FROM mps_ordersheet_item_confirmed
					INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_confirmed_ordersheet_item_id = mps_ordersheet_item_id		
					WHERE mps_ordersheet_item_ordersheet_id = $ordersheet
					ORDER BY mps_ordersheet_item_confirmed_id DESC
				")->fetchAll();
				
				if ($result) {
					foreach ($result as $row) {
						
						$id = $row['mps_ordersheet_item_id'];
						
						if (!$confirmed[$id]) {
							$confirmed[$id] = array(
								'quantity' => $row['mps_ordersheet_item_confirmed_quantity'],
								'state' => $row['mps_ordersheet_item_confirmed_order_state']
							);
						}
					}
				}
				
				// shipped items
				$result = $model->query("
					SELECT 
						mps_ordersheet_item_id,
						mps_ordersheet_item_shipped_quantity,
						mps_ordersheet_item_shortclosed
					FROM mps_ordersheet_item_shipped
					INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_shipped_ordersheet_item_id = mps_ordersheet_item_id		
					WHERE mps_ordersheet_item_ordersheet_id = $ordersheet
					ORDER BY mps_ordersheet_item_shipped_id DESC
				")->fetchAll();
				
				if ($result) {
					foreach ($result as $row) {
						$id = $row['mps_ordersheet_item_id'];
						if (!$shipped[$id] && $confirmed[$id]) {
							$shipped[$id] = array(
								'quantity' => $row['mps_ordersheet_item_shipped_quantity'],
								'shortclosed' => $row['mps_ordersheet_item_shortclosed']
							);
						}	
					}
				}
				
				
				foreach ($items as $item) {
						
					$id = $item['mps_ordersheet_item_id'];
						
					$this->totalApprovedItems++;
					$this->totalApprovedQuantites = $this->totalApprovedQuantites + $item['mps_ordersheet_item_quantity_approved'];
						
					if ($confirmed[$id]) {
				
						$confirmedQuantity = $confirmed[$id]['quantity'];
						$shippedQuantity = $shipped[$id]['quantity'];
						$shortclose = $shipped[$id]['shortclosed'];
				
						$this->totalCancelledItems++;
						$this->totalConfirmedQuantities = $this->totalConfirmedQuantities + $confirmedQuantity;
				
						if ($confirmed[$id]['state'] !='02' && $shipped[$id]) {
								
							$this->totalShippedItems++;
							$this->totalShippedQuantities = $this->totalShippedQuantities + $shippedQuantity;
								
							if ($shippedQuantity > 0) {
				
								if ($confirmedQuantity <= $shippedQuantity) {
									$this->totalCompletedItems++;
								}
								elseif($shippedQuantity < $confirmedQuantity && $shortclose==1) {
									$this->totalCompletedItems++;
								}
							}
							elseif($shortclose) {
								$this->totalCancelledItems++;
							}
								
						} else {
							$this->totalCancelledItems++;
						}
					}
				}
			}
		}
		
		public function cancelled() {
			return ($this->totalApprovedItems == $this->totalCancelledItems) ? true : false;
		}
		
		public function confirmed() {
			return ($this->totalApprovedItems == $this->totalConfirmedItems) ? true : false;
		}
		
		public function shipped() {
			return ($this->totalApprovedItems == $this->totalShippedItems && $this->totalApprovedItems == $this->totalCompletedItems) ? true : false;
		}
		
		public function partiallyShipped() {
			return ($this->totalShippedQuantities > 0 && $this->totalApprovedItems > $this->totalShippedItems) ? true : false;
		}
	}