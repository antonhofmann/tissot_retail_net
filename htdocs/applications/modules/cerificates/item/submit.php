<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$user = User::instance();
	$translate = Translate::instance();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];

	$db = Connector::get();
	
	// product line
	$_CERTIFICATE = $_REQUEST['id'];
	$_ITEM = $_REQUEST['item'];
	
	$certificate = new Certificate();
	$certificate->read($_CERTIFICATE);
	
	if ($certificate->id && $_ITEM) {
	
		if ($_REQUEST['checked']) {
			$response = $certificate->item()->create($_ITEM);
		} else {
			$response = $certificate->item()->delete($_ITEM);
		}

		$message = ($response) ? $translate->message_request_submitted : $translate->message_request_failure;
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}

	// update item materials :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$itemMaterials = array();
	$certificateMaterials = array();
	$odherCertificateMaterials = array();

	if ($response) {

		// get item materials
		$sth = $db->prepare("
			SELECT item_catalog_material_material_id
			FROM item_catalog_materials
			WHERE item_catalog_material_item_id = ?
		");

		$sth->execute(array($_ITEM));
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$itemMaterials[] = $row['item_catalog_material_material_id'];
			}
		}

		// get certificate materials
		$sth = $db->prepare("
			SELECT certificate_material_catalog_material_id
			FROM certificate_materials
			WHERE certificate_material_certificate_id = ?
		");

		$sth->execute(array($_CERTIFICATE));
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$certificateMaterials[] = $row['certificate_material_catalog_material_id'];
			}
		}

		// get other certificate materials
		// by item removing
		// need to check other certificate materials
		if (!$_REQUEST['checked']==0) {
			
			// get item suppliers
			$sth = $db->prepare("
				SELECT GROUP_CONCAT(DISTINCT address_id) as suppliers
				FROM suppliers 
				INNER JOIN addresses ON supplier_address = address_id
				WHERE supplier_item = ?
			");

			$sth->execute(array($_ITEM));
			$suppliers = $sth->fetch();

			// get other certificate materials
			$sth = $db->prepare("
				SELECT DISTINCT certificate_material_catalog_material_id
				FROM certificates
				INNER JOIN certificate_materials ON certificate_material_certificate_id = certificate_id
				WHERE certificate_address_id IN ($suppliers) AND certificate_id != ?
			");

			$sth->execute(array($_CERTIFICATE));
			$result = $sth->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$odherCertificateMaterials[] = $row['certificate_material_catalog_material_id'];
				}
			}
		}
	}


	if ($certificateMaterials) {

		$addMaterial = $db->prepare("
			INSERT INTO item_catalog_materials (
				item_catalog_material_item_id,
				item_catalog_material_material_id,
				user_created
			) VALUES (?, ?, ?)
		");

		$removeMaterial = $db->prepare("
			DELETE FROM item_catalog_materials
			WHERE item_catalog_material_item_id = ? AND item_catalog_material_material_id = ?
		");

		foreach ($certificateMaterials as $material) {
			
			// add materials to item materials
			if ($_REQUEST['checked']==1) {
				if (!in_array($material, $itemMaterials)) {
					$addMaterial->execute(array($_ITEM, $material, $user->login));
				}
			}
			// remove materials from items materials
			else {

				$canRemove = true;

				if ($odherCertificateMaterials) {
					$canRemove = in_array($material, $odherCertificateMaterials) ? false : true;
				}

				if ($canRemove) {
					$removeMaterial->execute(array($_ITEM, $material));
				}
			}
		}
	}

	
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));
	