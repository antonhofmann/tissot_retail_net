<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$id = $_REQUEST['certificate_title_id'];

$data = array();

$modul = new Modul(Connector::DB_CORE);
$modul->setTable('certificate_titles');
$modul->read($id);

$data['certificate_title_title'] = $_REQUEST['certificate_title_title'];
$data['certificate_title_description'] = $_REQUEST['certificate_title_description'];

if ($data) {

	if ($modul->id) {
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		$response = $modul->update($data);
		$message = $response ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		$data['user_created'] = $user->login;
		$response = $id = $modul->create($data);
		$message = $response ? Message::request_inserted() : $translate->message_request_failure;
		$redirect = $_REQUEST['redirect']."/$id";
	}
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}



echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect,
	'id' => $id
));