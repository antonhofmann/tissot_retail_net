<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$modul = new Modul(Connector::DB_CORE);
$modul->setTable('certificate_titles');
$modul->read($id);

$permission_catalog = user::permission('can_edit_catalog');
$permission_edit_certificates = user::permission('can_edit_certificates');

if ($modul->id && ($permission_catalog || $permission_edit_certificates)) {
	
	$delete = $modul->delete();
	
	if ($delete) {
		Message::request_deleted();
		url::redirect("/$application/$controller/$action");
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller/$action");
}