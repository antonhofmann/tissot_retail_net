<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// disabled fields
$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

// url request
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];

// product line
$id = $_REQUEST['certificate_id'];

$data = array();

if (in_array('certificate_name', $fields)) {
	$data['certificate_name'] = $_REQUEST['certificate_name'] ? $_REQUEST['certificate_name'] : null;
}

if (in_array('certificate_title_id', $fields)) {
	$data['certificate_title_id'] = $_REQUEST['certificate_title_id'] ? $_REQUEST['certificate_title_id'] : null;
}	

if (in_array('certificate_address_id', $fields)) {
	$data['certificate_address_id'] = $_REQUEST['certificate_address_id'] ? $_REQUEST['certificate_address_id'] : null;
}	

if ($data) {

	$certificate = new Certificate();
	$certificate->read($id);

	if ($certificate->id) {
		
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		
		$response = $certificate->update($data);
		
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		
		$response = $id = $certificate->create($data);
		
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		
		$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
	}
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}

// add certificate materials
if ($certificate->id && $response && in_array('materials', $fields)) { 

	$certificate->material()->deleteAll();

	if ($_REQUEST['materials']) {
		foreach ($_REQUEST['materials'] as $key => $value) { 
			$certificate->material()->create($key);
		}
	}
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect,
	'test' => $test
));
	