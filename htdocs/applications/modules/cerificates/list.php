<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$model = new Model($application);

$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

// sql order
$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'certificate_file_expiry_date DESC, address_company, certificate_name, certificate_file_version';

$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

// filter: full text search
if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {

	$keyword = $_REQUEST['search'];

	$filters['search'] = "(
		certificate_name LIKE \"%$keyword%\"
		OR certificate_file_version LIKE \"%$keyword%\"
		OR certificate_file_expiry_date LIKE \"%$keyword%\"
		OR address_company LIKE \"%$keyword%\"
	)";
}

if (isset($_REQUEST['certificate'])) {
	if ($_REQUEST['certificate']) {
		$active = $_REQUEST['certificate'];
		$filters['certificate'] = $active==1 ? "certificate_file_expiry_date < CURDATE()" : "certificate_file_expiry_date >= CURDATE()";
	}
} else {
	$active = 2;
	$filters['certificate'] = "certificate_file_expiry_date >= CURDATE()";
}

$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS
		certificate_files.certificate_file_id,
		certificate_files.certificate_file_version, 
		certificate_files.certificate_file_expiry_date, 
		certificate_files.certificate_file_file, 
		certificates.certificate_id, 
		certificates.certificate_name, 
		addresses.address_company,
		(
			SELECT COUNT(DISTINCT item_id) AS total
			FROM item_certificates
			INNER JOIN items ON item_id = item_certificate_item_id
			WHERE item_active = 1 AND item_certificate_certificate_id = certificates.certificate_id
		) AS total_items
	FROM certificates 
	INNER JOIN addresses ON addresses.address_id = certificates.certificate_address_id
	LEFT JOIN certificate_files ON certificates.certificate_id = certificate_files.certificate_file_certificate_id
")
->filter($filters)
->order($order, $direction)
->offset($offset, $rowsPerPage)
->fetchAll();


if ($result) {
	
	$totalrows = $model->totalRows();

	/*
	$items = $model->query("
		SELECT DISTINCT
			items.item_id,
			items.item_code,
			certificates.certificate_id
		FROM items
		INNER JOIN item_certificates ON item_certificate_item_id = item_id
		INNER JOIN certificates ON certificate_id = item_certificate_certificate_id
		WHERE item_active = 1
	")->fetchAll();

	if ($items) {
		foreach ($items as $row) {
			$certificate = $row['certificate_id'];
			$certificateItems[$certificate][] = $row['item_code'];
		}
	}
	*/

	$today = date::timestamp();

	$downloadIcon = "
		<span class='fa-stack'>
		  <i class='fa fa-circle fa-stack-2x'></i>
		  <i class='fa fa-download fa-stack-1x fa-inverse'></i>
		</span>
	";

	$icoItemInfo = "
		<span class='fa-stack'>
		  <i class='fa fa-circle fa-stack-2x'></i>
		  <i class='fa fa-info-circle fa-stack-1x'></i>
		</span>
	";

	foreach ($result as $row) {
		
		$certificate = $row['certificate_id'];
		$file = $row['certificate_file_id'];
		$key = "$certificate/$file";

		$datagrid[$key]['address_company'] = $row['address_company'];
		$datagrid[$key]['certificate_name'] = $row['certificate_name'];
		$datagrid[$key]['certificate_file_version'] = $row['certificate_file_version'];

		if ($row['certificate_file_expiry_date']) {
			$expired = strtotime($row['certificate_file_expiry_date']) < $today ? 'class=error' : null;
			$datagrid[$key]['certificate_file_expiry_date'] = "<span $expired>".date::system($row['certificate_file_expiry_date'])."</span>";
		}

		if ($certificateItems[$certificate]) {
			//$datagrid[$key]['items'] = join(', ',$certificateItems[$certificate]);
		}

		if ($row['total_items']) {
			$ico = "<i class='fa fa-info-circle'></i> {$row[total_items]}";
			$link = "<a href='/applications/templates/certificate/items.php?frame=1&id=$certificate&version=$file' data-fancybox-type='iframe' title='Click to see certificate items' class='infobox btn btn-default btn-xs' >$ico</a>";
			$datagrid[$key]['items'] = "<div class='bootstrap'>$link</div>";
		}

		if ($row['certificate_file_file']) {
			$datagrid[$key]['downloads'] = "<a href='{$row['certificate_file_file']}' target='_blank' title='Download File' >$downloadIcon</a>";
		}
	}
	
	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));
	
	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}


// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$order' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// toolbox: add
if ($_REQUEST['add']) {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();

$actives = array(
	1 => 'expired certificates',
	2 => 'valid certificates'
);

$toolbox[] = ui::dropdown($actives, array(
	'name' => 'certificate',
	'id' => 'certificate',
	'class' => 'submit',
	'value' => $active,
	'caption' => 'All Certificates'
));

// toolbox: form
if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

$table = new Table(array(
	'sort' => array('column'=>$order,'direction'=>$direction)
));

$table->datagrid = $datagrid;

$table->address_company(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);

$table->certificate_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	"href=".$_REQUEST['data']
);

$table->certificate_file_version(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);

$table->certificate_file_expiry_date(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);

//$table->items();

$table->items(
	Table::ATTRIBUTE_NOWRAP,
	'width=20px'
);

$table->downloads(
	Table::ATTRIBUTE_NOWRAP,
	'width=20px'
);

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;


