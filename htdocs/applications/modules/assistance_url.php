<?php 

	class Assistance_URL {
		
		public $id;
		
		protected $assistance;
		
		public $data;
	
		public function __construct() {
			$this->model = new Assistance_URL_Model(Connector::DB_CORE);
		}
		
		public function setAssistance($id) {
			$this->assistance = $id;
		}
		
		public function __get($key) {
			return $this->data["assistance_url_$key"];
		}
		
		public function read($url) {
			$this->data = $this->model->read($this->assistance, $url);
			$this->id = $this->data['assistance_url_id'];
			$this->assistance = $this->data['assistance_url_assistance_id'];
			return $this->data;
		}
		
		public function create($url) {
			$id = $this->model->create($this->assistance, $url);
			$this->read($url);
			return $id;
		}

		public function delete($url) {
			if ($this->id && $this->assistance) {
				return $this->model->delete($this->assistance, $url);
			}
		}

		public function deleteAll() {
			if ($this->assistance) {
				return $this->model->deleteAll($this->assistance);
			}
		}
	}
	