<?php

	class Ordersheet_Version_Item {
		
		const DB_BIND_ORDERSHEET_VERSIONS = 'INNER JOIN mps_ordersheet_versions ON  mps_ordersheet_version_id = mps_ordersheet_version_item_ordersheetversion_id';
		const DB_BIND_MATERIALS = 'INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_version_item_material_id';
	
		/**
		 * Version Item ID
		 * @var id
		 */
		public $id;
		
		/**
		 * Version Item Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Order Sheet Version ID
		 * @var integer
		 */
		public $version;
		
		/**
		 * DB Connector
		 * @var string
		 */
		protected $connector;
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Ordersheet_Version_Item_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_ordersheet_version_item_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_ordersheet_version_item_id'];
			$this->version = $this->data['mps_ordersheet_version_item_ordersheetversion_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public function deleteAll() {
			if ($this->version) {
				return $this->model->deleteAll($this->version);
			}
		}
	}