<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$type = new Modul(Connector::DB_CORE);
$type->setTable('packaging_types');
$type->read($id);

$permission_catalog = user::permission('can_edit_catalog');

if ($type->id && $permission_catalog) {
	
	$delete = $type->delete();
	
	if ($delete) {
		Message::request_deleted();
		url::redirect("/$application/$controller");
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}