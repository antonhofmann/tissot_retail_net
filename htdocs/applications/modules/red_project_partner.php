<?php

	class Red_Project_Partner {

		const BIND_USER_ID = "INNER JOIN db_retailnet.users ON user_id  =  red_project_partner_user_id";
		const BIND_ADDRESSES = "INNER JOIN db_retailnet.addresses ON address_id = red_project_partner_address_id";
		const BIND_ADDRESS_TYPE = "INNER JOIN db_retailnet.address_types ON address_type_id = address_type";


		public $id;
		
		public $data;

		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new Red_Project_Partner_Model($connector);
		}

		public function __get($key) {
			return $this->data["red_project_partner_$key"];
		}

		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $id;
			return $this->data;
		}

		public function create($data) {
			return $this->model->create($data);
		}

		public function update($data) {
			if ($this->id) {
				return $this->model->update($this->id, $data);
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}

		public function owner() {
			$result = $this->model->owner($this->id);
			return ($result) ? true : false;
		}

		/**
		* Checks if $user_id is a user of one of partner's users
		* @param $user_id	user id
		* @return boolean
		*/
		public function is_user($user_id) {
			
			$return_value= false;
			
			if ($this->id) {
				
				$address = new Company();
				$address->read($this->address_id);
				$users = $address->get_users();
				
				if (is_array($users) && in_array($user_id, $users)) {
					$return_value = true;
				}

			}
			
			return $return_value;
		}
	}
