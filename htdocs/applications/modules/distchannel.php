<?php

	class DistChannel {
		
		/**
		 * Distribution Channel ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Distribution Channel Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Database Model
		 * @return DistChannel_Model
		 */
		protected $model;
		
		/**
		 * Distribution Channel Group
		 * 
		 * @return DistChannelGroup
		 */
		protected $group;
		
		/**
		 * Edit all disribution channels
		 * @var string
		 */
		const PERMISSION_EDIT = 'can_edit_mps_dist_channels';
		
		/**
		 * view all distribution channels
		 * @var string
		 */
		const PERMISSION_VIEW = 'can_view_mps_dist_channels';
		
		
		/**
		 * Distribution Channel<br />
		 * Application Dependet Modul
		 * @author aserifi
		 * @copyright mediaparx ag
		 */
		public function __construct() {
			$this->model = new DistChannel_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["mps_distchannel_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_distchannel_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		/**
		 * Distribution Channel Group
		 * 
		 * @return DistChannelGroup
		 */
		public function group() {
			
			if (!$this->group) {
				$this->group = new DistChannelGroup();
			}
			
			return $this->group;
		}
		
		public static function loader($application, $filters=null) {
			$model = new DistChannel_Model(Connector::DB_CORE, $filters);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
		
		public function header () {
			
			if ($this->id) {
				
				$this->group()->read($this->group_id);
				
				return $this->group()->name.', '.$this->code.', '.$this->name;
			}
		}
	}
	