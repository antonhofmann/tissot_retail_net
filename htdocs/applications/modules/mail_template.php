<?php 

	class Mail_Template {
		
		/**
		 * Template ID
		 * 
		 * @var int
		 */
		public $id;
		
		/**
		 * Dataloader
		 * 
		 * @var array
		 */
		public $data;
		
		/**
		 * Standard Recipients
		 * 
		 * @return Mail_Template_Recipient
		 */
		protected $recipient;
		
		/**
		 * Template Role
		 * 
		 * @return Mail_Template_Role
		 */
		protected $role;
		
		/**
		 * Template Filter
		 * 
		 * @return Mail_Template_Filter
		 */
		protected $filter;
		
		
		const PERMISSION_EDIT = 'can_edit_mail_templates';

		
		public function __construct() {
			$this->model = new Mail_Template_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["mail_template_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mail_template_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public function read_from_shortcut($param) {
			$this->data = $this->model->read_from_shortcut($param);
			$this->id = $this->data['mail_template_id'];
			return $this->data;
		}
		
		/**
		 * Standard Recipient
		 * 
		 * @return Mail_Template_Recipient
		 */
		public function recipient() {
			
			if (!$this->recipient) {
				$this->recipient = new Mail_Template_Recipient();
			}
			
			if ($this->id) {
				$this->recipient->setTemplate($this->id);
			}
			
			return $this->recipient;
		}
		
		/**
		 * Template Role
		 * 
		 * @return Mail_Template_Role
		 */
		public function role() {
			
			if (!$this->role) {
				$this->role = new Mail_Template_Role();
			}
			
			if ($this->id) {
				$this->role->setTemplate($this->id);
			}
			
			return $this->role;
		}
		
		/**
		 * Template Filter
		 * 
		 * @return Mail_Template_Filter
		 */
		public function filter() {
			
			if (!$this->filter) {
				$this->filter = new Mail_Template_Filter();
			}
			
			return $this->filter;
		}
	}
?>