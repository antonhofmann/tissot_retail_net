<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$permissionEdit = user::permission('can_edit_all_localizations');

$company = New Company();
$company->read($user->address);
$user_country = $company->country;


$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

// sql order
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'posclosinghr_text';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;
	

$model = new Model();
$model_store_locator = new Model(CONNECTOR::DB_RETAILNET_STORE_LOCATOR);


// filter: full text search
if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
	
	$search = $_REQUEST['search'];
	
	$filters['search'] = "(
		posclosinghr_text LIKE '%$search%'
	)";
}

//filter country
$fCountry = $_REQUEST['country'];
$default_country = '';
// filer: countries
if ($fCountry) {
	$default_country =  $fCountry;
}
else
{
	$default_country = $user_country;
}
$filters['country'] = "country_id = $default_country";


//get languages of the country
$languages = array();
$filters2['closinghrs'] = "country_language_country_id = $default_country";

$languages = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		country_language_language_id,
		language_name,
		language_from_right_to_left
	FROM country_languages
	LEFT join languages on language_id = country_language_language_id
")
->filter($filters2)
->order('language_name', 'ASC')
->fetchAll();


//get all pos locations of the country in question
$poslocations = array();
$filters2['closinghrs'] = "posaddress_country = $default_country";
$filters2['operating'] = "(posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')";

$poslocations = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		posaddress_id
	FROM posaddresses
")
->filter($filters2)
->order('posaddress_id', 'ASC')
->fetchAll();

$tmp = array();
foreach($poslocations as $key=>$poslocation)
{
	$tmp[] = $poslocation["posaddress_id"];
}
$filters2['poslocation'] = "loc_posclosinghr_posaddress_id in (" . implode(",", $tmp) . ")";
$filters3['poslocation'] = "posclosinghr_posaddress_id in (" . implode(",", $tmp) . ")";
$filters3['poslocationtext'] = "posclosinghr_text > ''";

//get existing translations
$tmp = array();
$filters2 = array();
foreach($languages as $key=>$language)
{
	$tmp[] = $language["country_language_language_id"];
}
$filters2['closinghrs'] = "loc_posclosinghr_language_id in (" . implode(",", $tmp) . ")";

$translations = array();
$result = $model_store_locator->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		loc_posclosinghr_language_id, 
		loc_posclosinghr_posaddress_id,
		loc_posclosinghr_text 
	FROM loc_posclosinghrs
")
->filter($filters2)
->fetchAll();

if($result)
{
	foreach($result as $key=>$translation)
	{
		$translations[$translation['loc_posclosinghr_posaddress_id']][$translation['loc_posclosinghr_language_id']] = $translation['loc_posclosinghr_text'];
	}
}
//echo $model_store_locator->sql;
//echo '<pre>';
//print_r($translations);



//get posclosinhrs
$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		posclosinghr_posaddress_id, 
		TRIM(posclosinghr_text) as posclosinghr_text
	FROM posclosinghrs
")
->filter($filters3)
->order($sort, $direction)
->fetchAll();


if ($result) {

	$datagrid = array();
	$totalrows = $model->totalRows();
	$data = _array::datagrid($result);

	
	foreach($data as $posclosinghr_posaddress_id=>$fields)
	{
		if(!in_array($fields['posclosinghr_text'], $datagrid))
		{
			$datagrid[$posclosinghr_posaddress_id] = $fields['posclosinghr_text'];
		}
	}

	foreach($datagrid as $posclosinghr_posaddress_id=>$value)
	{
		$datagrid[$posclosinghr_posaddress_id] = array('posclosinghr_text' => $value);
	}

	
	foreach($datagrid as $posclosinghr_posaddress_id=>$fields)
	{
		foreach($languages as $language)
		{
			$translated_value = '';
			
			$translations[$translation['loc_posclosinghr_posaddress_id']][$translation['loc_posclosinghr_language_id']] = $translation['loc_posclosinghr_text'];
			
			if(array_key_exists($posclosinghr_posaddress_id, $translations) and array_key_exists($language['country_language_language_id'], $translations[$posclosinghr_posaddress_id]))
			{
				$translated_value = $translations[$posclosinghr_posaddress_id][$language['country_language_language_id']];
			}
			$datagrid[$posclosinghr_posaddress_id]['posclosinghr_text_' . $language['country_language_language_id']] = $translated_value;
		}
	}

	/*
	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
	*/
	
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
$toolbox[] = "<input type=hidden id=application name=application value='$application' />";

// toolbox: serach full text
//$toolbox[] = ui::searchbox();

// dropdown countries ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
if(user::permission(Pos::PERMISSION_EDIT_LIMITED))
{
	/*
	$result = $model->query("
		SELECT DISTINCT country_id AS value, country_name AS caption 
		FROM countries
	")
	->filter($filters)
	->order('caption')
	->fetchAll();
	*/

	$result = $model->query("
		SELECT DISTINCT posaddress_country AS value, country_name AS caption 
		FROM posaddresses 
		LEFT JOIN countries on country_id = posaddress_country 
		LEFT join country_languages on country_language_country_id = posaddress_country
		where posaddress_client_id = " . $user->address .
	    " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') 
		and posaddress_export_to_web = 1
		AND country_language_language_id <> 36
	")
	->order('caption')
	->fetchAll();
}
else
{	
	$result = $model->query("
		SELECT DISTINCT country_id AS value, country_name AS caption 
		FROM countries
	")
	->order('caption')
	->fetchAll();

}

if ($result and $permissionEdit == true) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'country',
		'id' => 'country',
		'class' => 'submit',
		'value' => $default_country,
		'caption' => false
	));
}

// toolbox: form
$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";


$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

$table->datagrid = $datagrid;
$table->dataloader($dataloader);


$table->posclosinghr_text(
	Table::PARAM_SORT,
	"width=450"
);

//$table->attributes("posclosinghr_text", array("style"=>"width:200px;"));

if(count($languages) > 0)
{
	foreach($languages as $language)
	{
		$fieldName = "posclosinghr_text_".$language['country_language_language_id'];
	
		
		$table->$fieldName(
			Table::ATTRIBUTE_NOWRAP,
			Table::DATA_TYPE_TEXTAREA,
			"caption=" .$language['language_name'],
			"data-language_id=" . $language['country_language_language_id']
		);

		if($rtl == 1)
		{
			$table->attributes("posclosinghr_text_".$language['country_language_language_id'], array("style"=>"direction:RTL;", "class"=>"textarea-md"));
		}
		else
		{
			$table->attributes("posclosinghr_text_".$language['country_language_language_id'], array("class"=>"textarea-md"));
		}

	}
}

$table->footer($translate->total_rows . " " . $totalrows);
//$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;
