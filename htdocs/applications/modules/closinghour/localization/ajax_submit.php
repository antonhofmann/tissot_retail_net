<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	if($_REQUEST['closing_id_string'] and $_REQUEST['language_id'] > 0)
	{
		
		$model_store_locator = new Model(CONNECTOR::DB_RETAILNET_STORE_LOCATOR);

		//update store locator
		$sth = $model_store_locator->db->prepare("select * from posclosinghrs
				WHERE posclosinghr_posaddress_id = ?");
		$response = $sth->execute(array($_REQUEST['closing_id_string']));
		
		if($response == true)
		{
			$records = $sth->fetchAll();
					
			if(count($records) > 0)
			{
				//update store locator
				$sth = $model_store_locator->db->prepare("select * from posclosinghrs
						WHERE REPLACE(posclosinghr_text, '\"', '') = ?");
				$response = $sth->execute(array($records[0]['posclosinghr_text']));

				
				if($response == true)
				{
					$records = $sth->fetchAll();
					
					if(count($records) > 0)
					{
						foreach($records as $key=>$data)
						{
							
							$query = "delete from loc_posclosinghrs 
							   where loc_posclosinghr_language_id = ?
							   and loc_posclosinghr_posaddress_id = ?";

							$sth = $model_store_locator->db->prepare($query);

							$response = $sth->execute(array($_REQUEST['language_id'],  $data["posclosinghr_posaddress_id"]));

							
							$query = "Insert into loc_posclosinghrs (loc_posclosinghr_posaddress_id, loc_posclosinghr_language_id, loc_posclosinghr_text) VALUES (?,?,?)";

							$sth = $model_store_locator->db->prepare($query);
							$response = $sth->execute(array($data["posclosinghr_posaddress_id"], $_REQUEST['language_id'],  $_REQUEST['closing_name']));
						}
					}
				}
			}
		}
	}

?>