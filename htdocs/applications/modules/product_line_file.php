<?php 

	class Product_Line_File {
	
		public $data;
		
		public function __construct() {
			$this->model = new Product_Line_File_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["product_line_file_$key"];
		}

		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['product_line_file_id'];
			return $this->data;
		}

		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}

		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
	}