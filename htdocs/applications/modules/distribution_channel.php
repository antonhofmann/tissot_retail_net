<?php

	class Distribution_Channel {
		
		/**
		 * Distribution Channel ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Distribution Channel Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Database Model
		 * @var objec
		 */
		protected $model;
		
		/**
		 * Edit all disribution channels
		 * @var string
		 */
		const PERMISSION_EDIT = 'can_edit_mps_dist_channels';
		
		/**
		 * view all distribution channels
		 * @var string
		 */
		const PERMISSION_VIEW = 'can_view_mps_dist_channels';
		
		
		/**
		 * Distribution Channel<br />
		 * Application Dependet Modul
		 * @author aserifi
		 * @copyright mediaparx ag
		 */
		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new Distribution_Channel_Model($connector);
		}
		
		public function __get($key) {
			return $this->data["mps_distribution_channel_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_distribution_channel_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($application, $filters=null) {
			$model = new Distribution_Channel_Model($application, $filters);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
		
		public function header () {
			if ($this->id) {
				return $this->group.', '.$this->code.', '.$this->name;
			}
		}
	}
	