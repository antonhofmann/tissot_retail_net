<?php

/**
 * Represents a phase in a masterplan
 */
class Masterplan_Phase {
	/**
	 * @var integer
	 */
	public $id;
	
	/**
	 * @var integer
	 */
	public $masterplan_id;
	
	/**
	 * @var integer
	 */
	public $parent_id;
	
	/**
	 * @var string
	 */
	public $title;
	
	/**
	 * @var integer
	 */
	public $pos;
	
	/**
	 * @var integer|null
	 */
	public $default_start_value;
	
	/**
	 * @var integer|null
	 */
	public $fallback_start_value;
	
	/**
	 * @var integer|null
	 */
	public $default_end_value;
	
	/**
	 * @var integer|null
	 */
	public $fallback_end_value;

	/**
	 * @var array
	 */
	public $steps;

	/**
	 * @var array
	 */
	public $subphases;

	/**
	 * Get the earliest start date for this phase
	 * @param  array Array of array-of-values (@see this::getAllDates())
	 * @return string|null Date value or null
	 */
	public function getStartDate($allValues) {
		// determine which value to use
		$value = null;
		if ($this->default_start_value) {
			$value = $this->default_start_value;
		}
		else {
			$value = $this->fallback_start_value;
		}
		if (is_null($value)) {
			return;
		}

		// find earliest date if we have multiple
		// @see this::getAllDates() for an explanation
		$values = $this->getAllDates($allValues, $value);
		if (count($values)) {
			return $this->findEarliestDate($values);
		}
		else {
			return null;
		}
	}

	/**
	 * Get the latest end date for this phase
	 * @param  array Array of array-of-values (@see this::getAllDates())
	 * @return string|null Date value or null
	 */
	public function getEndDate($allValues) {
		// determine which value to use
		$value = null;
		if ($this->default_end_value) {
			$value = $this->default_end_value;
		}
		else {
			$value = $this->fallback_end_value;
		}
		if (is_null($value)) {
			return null;
		}

		// find latest date if we have multiple
		// @see this::getAllDates() for an explanation
		$values = $this->getAllDates($allValues, $value);
		if (count($values)) {
			return $this->findLatestDate($values);
		}
		else {
			return null;
		}
	}

	/**
	 * Get all values of a certain date
	 * E.g. get all layout_preview_client date values from an array
	 * that may look something like this (simplified):
	 *
	 * @code
	 * // $allValues
	 * array
	 *   0 => 
	 *     array
	 *       'project_submitted' => string '2010/11/12'
	 *       'preferred_opening_date' => string '2011/01/31'
	 *       'layout_preview_approval' => string '2010/11/18'
	 *       'layout_preview_accepted' => string '2010/11/29'
	 *   1 => 
	 *     array
	 *       'layout_preview_approval' => string '2011/01/17'
	 *       'layout_preview_accepted' => string '2011/01/18'
	 *       
   * // passing the above array with $value = 'layout_preview_accepted'
   * // would result in:
   * array
   *   0 => string '2010/11/29'
   *   1 => string '2011/01/18'
	 * @endcode
	 *  
	 * @param  array $allValues
	 * @param  string $value
	 * @return array
	 */
	protected function getAllDates($allValues, $value) {
		$values = array();
		foreach ($allValues as $valueSet) {
			if (isset($valueSet[$value]) && !is_null($valueSet[$value])) {
				$values[] = $valueSet[$value];
			}
		}
		return $values;
	}

	/**
	 * Find earliest date in an array of dates
	 * @param  array $dates
	 * @return string Date in Y/m/d format
	 */
	protected function findEarliestDate($dates) {
		$earliest = null;
		foreach ($dates as $date) {
			$time = strtotime($date);
			if (is_null($earliest) || $time < $earliest) {
				$earliest = $time;
			}
		}
		return date('Y/m/d', $earliest);
	}

	/**
	 * Find latest date in an array of dates
	 * @param  array $dates
	 * @return string Date in Y/m/d format
	 */
	protected function findLatestDate($dates) {
		$latest = null;
		foreach ($dates as $date) {
			$time = strtotime($date);
			if (is_null($latest) || $time > $latest) {
				$latest = $time;
			}
		}
		return date('Y/m/d', $latest);
	}
}