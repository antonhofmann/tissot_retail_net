<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();


$_SECTION = trim($_REQUEST['section']);
$_CLIENT = trim($_REQUEST['client']);
$_PLANNING_TYPE = trim($_REQUEST['type']);
$_MPS_CHANNEL = trim($_REQUEST['channel']);
$_VALUE = trim($_REQUEST['value']);

$_JSON = array();
$_ERRORS = array();
$_SUCCESS = array();
$_CONSOLE = array();


// checkin :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_SECTION) {
	$_ERRORS[] = "Submit action is not defined.";
}

if (!$_CLIENT) {
	$_ERRORS[] = "Client is not defined.";
}

if (!$_PLANNING_TYPE) {
	$_ERRORS[] = "Planning type is not defined.";
}

if (!$_MPS_CHANNEL) {
	$_ERRORS[] = "MPS distribution channel is not defined.";
}

if ($_ERRORS) {
	$_ERRORS[] = "Invalid request. Your request is not submitted.<br>Check your data and please try again.";
	goto BLOCK_RESPONDING;
}


// matrix ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$model = new Model();

$sth = $model->db->prepare("
	SELECT *
	FROM sap_exchange_matrix
	WHERE sap_exchange_matrix_address_id = ? 
	AND sap_exchange_matrix_planning_type_id = ? 
	AND sap_exchange_matrix_mps_channel_id = ?
");

$sth->execute(array($_CLIENT, $_PLANNING_TYPE, $_MPS_CHANNEL));
$result = $sth->fetch();
$_ID = $result['sap_exchange_matrix_id'];

if ($_ID) $_CONSOLE[] = "Matrix $_ID found";
else {

	$sth = $model->db->prepare("
		INSERT INTO sap_exchange_matrix (
			sap_exchange_matrix_address_id,
			sap_exchange_matrix_planning_type_id,
			sap_exchange_matrix_mps_channel_id,
			user_created
		)
		VALUES (?,?,?,?)
	");

	$response = $sth->execute(array($_CLIENT, $_PLANNING_TYPE, $_MPS_CHANNEL, $user->id));
	$_ID = $model->db->lastInsertId();
	$_CONSOLE[] = "Add new matrix: $_ID";
}

if (!$_ID) {
	$_ERRORS[] = "Error on data processing, please contact system administrator.";
	goto BLOCK_RESPONDING;
}

// submitting ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


switch ($_SECTION) {
	
	case 'sap-channel':

		$_SAP_CHANNEL_ID = null;

		if ($_VALUE) {

			$_CONSOLE[] = "Start SAP channel updating";
			
			$sth = $model->db->prepare("
				SELECT sap_distribution_channel_id AS id
				FROM sap_distribution_channels
				WHERE REPLACE(LOWER(sap_distribution_channel_code), ' ', '') = ?
			");

			$sth->execute(array(strtolower(str_replace(' ', '', $_VALUE))));
			$result = $sth->fetch();
			$_SAP_CHANNEL_ID = $result['id'];

			if (!$_SAP_CHANNEL_ID) {
				$_ERRORS[] = "Distribution channel <b>$_VALUE</b> not found.";
				goto BLOCK_RESPONDING;
			}

		} else {
			$_CONSOLE[] = "Start SAP channel deleting";
		}

		$sth = $model->db->prepare("
			UPDATE sap_exchange_matrix SET
				sap_exchange_matrix_sap_channel_id = ?,
				user_modified = ?
			WHERE sap_exchange_matrix_id = ?
		");

		$response = $sth->execute(array($_SAP_CHANNEL_ID, $user->login, $_ID));

		if ($response) $_JSON['success'] = true;
		else $_ERRORS[] = "Error on data submitting for sap distribution channel $_VALUE, please contact system administrator.";
		
	break;

	case 'order-type':

		$_ORDER_TYPE_ID = null;

		if ($_VALUE) {

			$_CONSOLE[] = "Start order type updating";

			$sth = $model->db->prepare("
				SELECT sap_order_type_id AS id
				FROM sap_order_types
				WHERE REPLACE(LOWER(sap_order_type_name), ' ', '') = ?
			");

			$sth->execute(array(strtolower(str_replace(' ', '', $_VALUE))));
			$result = $sth->fetch();
			$_ORDER_TYPE_ID = $result['id'];

			if (!$_ORDER_TYPE_ID) {
				$_ERRORS[] = "SAP order type <b>$_VALUE</b> not found.";
				goto BLOCK_RESPONDING;
			}

		} else {
			$_CONSOLE[] = "Start order type deleting";
		}

		$sth = $model->db->prepare("
			UPDATE sap_exchange_matrix SET
				sap_exchange_matrix_order_type_id = ?,
				user_modified = ?
			WHERE sap_exchange_matrix_id = ?
		");

		$response = $sth->execute(array($_ORDER_TYPE_ID, $user->login, $_ID));

		if ($response) $_JSON['success'] = true;
		else $_ERRORS[] = "Error on data submitting for oder type $_VALUE, please contact system administrator.";

	break;
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONDING:

$_JSON['console'] = $_CONSOLE;
$_JSON['errors'] = $_ERRORS;
$_JSON['success'] = $_SUCCESS;

header('Content-Type: text/json');
echo json_encode($_JSON);
