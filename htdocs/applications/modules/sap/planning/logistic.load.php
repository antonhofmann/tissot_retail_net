<?php

/**
 * Load SAP/MPS exchange data
 * 
 * IMPORTANT: On page load get all headers and matrix data, otherwise load only matrix data.
 *
 * Author: admir.serifi@mediaparx.com
 * Date Created: 2015-02-05
 */


require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$_APPLICATION = $_REQUEST['application'];
$_CLIENT = $_REQUEST['client'];
$_PAGE_ONLOAD = $_CLIENT ? false : true;

if ($_CLIENT)  {
	session::setFilter($_APPLICATION, 'matrix', 'client', $_CLIENT);
} else {
	$sessionFilter = session::getFilter($_APPLICATION, 'matrix');
	$_CLIENT = $sessionFilter['client'];
}

$_ERRORS = array();

$_CAN_EDIT_ALL = user::permission('can_edit_all_sap_order_matrix');
$_CAN_EDIT_HIS = user::permission('can_edit_his_sap_order_matrix');
$_READ_ONLY = !$_CAN_EDIT_ALL && !$_CAN_EDIT_HIS ? true : false;

$model = new Model();


// clients :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_PAGE_ONLOAD) {
	
	$sth= $model->db->prepare("
		SELECT 
			address_id AS id, 
			CONCAT(country_name, ': ', address_company) AS name
		FROM addresses
		INNER JOIN countries ON country_id = address_country
		WHERE address_involved_in_planning = 1 AND address_active = 1 AND address_do_data_export_from_mps_to_sap = 1
		ORDER BY country_name, address_company
	");

	$sth->execute(array($_CLIENT));
	$result = $sth->fetchAll();

	// set selected client
	$_CLIENTS = _array::extract($result);
	
	if (!$_CLIENT) $_CLIENT = key($_CLIENTS);
}


// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_CAN_EDIT_ALL) {

	$company = new Company();
	$company->read($user->address);

	if (!$company->id) {
		$_ERRORS[] = "Client company not found.";
	}

	if ($company->id && !$company->do_data_export_from_mps_to_sap) {
		$_ERRORS[] = "Your company cannot export data to SAP.";
	}
}

// restricted view
if (!$_CAN_EDIT_ALL) {
	$_CLIENT = $company->id;
}

if (!$_CLIENT) {
	$_ERRORS[] = "Invalid request. Client is not defined. Please contact system administrator.";
}

if ($_ERRORS) {
	goto BLOCK_RESPONDING;
}

// sap order types :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_PAGE_ONLOAD) {
	
	$sth = $model->db->prepare("
		SELECT
			sap_order_type_id,
			sap_order_type_name
		FROM sap_order_types
		ORDER BY sap_order_type_name
	");

	$sth->execute();
	$result = $sth->fetchAll();
	$_ORDER_TYPES = _array::extract($result);
	array_unshift($_ORDER_TYPES, '');
}


// sap distribution channels :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_PAGE_ONLOAD) {
	
	$sth = $model->db->prepare("
		SELECT DISTINCT
			sap_distribution_channel_id AS id,
			sap_distribution_channel_code as channel
		FROM sap_distribution_channels
		ORDER BY channel
	");

	$sth->execute();
	$result = $sth->fetchAll();
	$_SAP_DISTRIBUTION_CHANNELS = _array::extract($result);
	array_unshift($_SAP_DISTRIBUTION_CHANNELS, '');
}


// mps distribution channels :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$sth = $model->db->prepare("
	SELECT DISTINCT
		mps_distchannel_id AS id,
		CONCAT(mps_distchannel_code, ' ', mps_distchannel_name) AS channel
	FROM mps_distchannels
	ORDER BY channel
");

$sth->execute();
$result = $sth->fetchAll();
$_MPS_DISTRIBUTION_CHANNELS = _array::extract($result);


// planning types ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$sth = $model->db->prepare("
	SELECT DISTINCT
		mps_material_planning_type_id AS id, 
		mps_material_planning_type_name AS caption
	FROM db_retailnet_mps.mps_material_planning_types
	ORDER BY caption
");

$sth->execute();
$result = $sth->fetchAll();
$_PLANNING_TYPES = _array::extract($result);


// matrix data :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_MATRIX_CHANNELS = array();
$_MATRIX_ORDER_TYPES = array();

	
$sth = $model->db->prepare("
	SELECT 
		sap_exchange_matrix_id AS id,
		sap_exchange_matrix_planning_type_id AS type,
		sap_exchange_matrix_mps_channel_id AS channel,
		sap_exchange_matrix_sap_channel_id AS sap_channel_id,
		sap_distribution_channel_code AS sap_channel_code,
		sap_exchange_matrix_order_type_id AS order_type_id,
		sap_order_type_name AS order_type_name
	FROM sap_exchange_matrix
	LEFT JOIN sap_order_types ON sap_order_type_id = sap_exchange_matrix_order_type_id
	LEFT JOIN sap_distribution_channels ON sap_distribution_channel_id = sap_exchange_matrix_sap_channel_id
	WHERE sap_exchange_matrix_address_id = ?
");

$sth->execute(array($_CLIENT));
$result = $sth->fetchAll();

if ($result) {

	foreach ($result as $row) {

		$type = $row['type'];
		$channel = $row['channel'];
		
		if ($row['sap_channel_id']) {
			$_MATRIX_CHANNELS[$channel][$type] = $row['sap_channel_code'];
		}

		if ($row['order_type_id']) {
			$_MATRIX_ORDER_TYPES[$channel][$type] = $row['order_type_name'];
		}
	}
}


// headers :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_HEADERS = array();
$_COLUMNS = array();

if ($_PAGE_ONLOAD && $_PLANNING_TYPES) {

	$row=0;
	$col=0;

	// dropdown clients
	if ($_PAGE_ONLOAD) {

		$_HEADERS[$col] = $_CLIENTS[$_CLIENT];

		$_COLUMNS[$col] = array(
			'data' => "channel.value",
		);
	}
	
	foreach ($_PLANNING_TYPES as $type => $typeName) {
		
		$col++;

		// planning type caption	
		$_GROUPS[$col] = $typeName;

		// sap channel code
		$_HEADERS[$col] = "SAP<br>Channel";
		$_COLUMNS[$col] = array(
			'data' => "sap_channel_$type.value",
			'type' => 'dropdown',
			'source' => $_SAP_DISTRIBUTION_CHANNELS,
			'strict' => true,
			'allowInvalid' => false,
			'readOnly' => $_READ_ONLY
		);
		
		$col++;

		// order type
		$_HEADERS[$col] = 'Order<br>Type';
		$_COLUMNS[$col] = array(
			'data' => "order_$type.value",
			'type' => 'dropdown',
			'source' => $_ORDER_TYPES,
			'strict' => true,
			'allowInvalid' => false,
			'readOnly' => $_READ_ONLY
		);
	}
}

// sap clients :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_DATA = array();

if ($_MPS_DISTRIBUTION_CHANNELS) {

	$row = 0;

	foreach ($_MPS_DISTRIBUTION_CHANNELS as $channel => $channelName) {

		// market name
		$_DATA[$row]['channel'] = array(
			'id' => $channel,
			'value' => $channelName
		);

		foreach ($_PLANNING_TYPES as $type => $typeName) {
			
			// sap channel code
			$_DATA[$row]["sap_channel_$type"] = array(
				'id' => $type,
				'section' => 'sap-channel',
				'value' => $_MATRIX_CHANNELS[$channel][$type]
			);
			
			// order type
			$_DATA[$row]["order_$type"] = array(
				'id' => $type,
				'section' => 'order-type',
				'value' => $_MATRIX_ORDER_TYPES[$channel][$type]
			);
		}

		$row++;
	}
}

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONDING:

header('Content-Type: text/json');
echo json_encode(array(
	'client' => $_CLIENT,
	'headers' => $_HEADERS,
	'columns' => $_COLUMNS,
	'data' => $_DATA,
	'groups' => $_GROUPS,
	'errors' => $_ERRORS
));
