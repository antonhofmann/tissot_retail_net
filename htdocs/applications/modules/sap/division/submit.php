<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['sap_division_id'];

$data = array();

$division = new Modul($application);
$division->setTable('sap_divisions');

if (isset($_REQUEST['sap_division_code'])) {
	$data['sap_division_code'] = $_REQUEST['sap_division_code'];
}

if (isset($_REQUEST['sap_division_name'])) {
	$data['sap_division_name'] = $_REQUEST['sap_division_name'];
}

if ($data) {

	$division->read($id);

	if ($division->id) {
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		$response = $division->update($data);
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		$response = $id = $division->create($data);
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		$redirect = $_REQUEST['redirect']."/$id";
	}
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect,
	'id' => $id
));