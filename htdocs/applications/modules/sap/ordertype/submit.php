<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['sap_order_type_id'];

$data = array();

$orderType = new Modul();
$orderType->setTable('sap_order_types');

if (isset($_REQUEST['sap_order_type_name'])) {
	$data['sap_order_type_name'] = $_REQUEST['sap_order_type_name'];
}

if ($data) {

	$orderType->read($id);

	if ($orderType->id) {
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		$response = $orderType->update($data);
		$message = $response ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		$response = $id = $orderType->create($data);
		$message = $response ? Message::request_inserted() : $translate->message_request_failure;
		$redirect = $_REQUEST['redirect']."/$id";
	}
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}



echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect,
	'id' => $id
));