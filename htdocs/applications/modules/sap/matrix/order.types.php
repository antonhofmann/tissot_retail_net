<?php

/**
 * Load SAP/MPS order types
 *
 * Author: admir.serifi@mediaparx.com
 * Date Created: 2015-02-11
 */


require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$_APPLICATION = $_REQUEST['application'];

$_ERRORS = array();

$_CAN_EDIT_ALL = user::permission('can_edit_all_sap_order_matrix');
$_CAN_EDIT_HIS = user::permission('can_edit_his_sap_order_matrix');
$_READ_ONLY = !$_CAN_EDIT_ALL && !$_CAN_EDIT_HIS ? true : false;

$model = new Model();


// clients :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth= $model->db->prepare("
	SELECT 
		address_id AS id, 
		CONCAT(country_name, ': ', address_company) AS name
	FROM addresses
	INNER JOIN countries ON country_id = address_country
	WHERE address_involved_in_planning = 1 AND address_active = 1 AND address_do_data_export_from_mps_to_sap = 1
	ORDER BY country_name, address_company
");

$sth->execute(array($_CLIENT));
$result = $sth->fetchAll();

// set selected client
$_CLIENTS = _array::extract($result);

if (!$_CLIENTS) {
	$_ERRORS[] = "Clients not found.";
	goto BLOCK_RESPONDING;
}


// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_CAN_EDIT_ALL) {

	$company = new Company();
	$company->read($user->address);

	if (!$company->id) {
		$_ERRORS[] = "Client company not found.";
	}

	if ($company->id && !$company->do_data_export_from_mps_to_sap) {
		$_ERRORS[] = "Your company cannot export data to SAP.";
	}

	$_CLIENT = $company->id;

	$_CLIENTS = array(
		$_CLIENT => $_CLIENTS[$_CLIENT]
	);
}

if ($_ERRORS) {
	goto BLOCK_RESPONDING;
}

// sap order types :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT
		sap_order_type_id,
		sap_order_type_name
	FROM sap_order_types
	ORDER BY sap_order_type_name
");

$sth->execute();
$result = $sth->fetchAll();
$_ORDER_TYPES = _array::extract($result);
array_unshift($_ORDER_TYPES, '');

if (!$_ORDER_TYPES) {
	$_ERRORS[] = "Order types not found.";
	goto BLOCK_RESPONDING;
}


// mps distribution channels :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT DISTINCT
		mps_distchannel_id AS id,
		mps_distchannel_code AS channel
	FROM mps_distchannels
	ORDER BY channel
");

$sth->execute();
$result = $sth->fetchAll();
$_MPS_DISTRIBUTION_CHANNELS = _array::extract($result);


// matrix data :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_MATRIX = array();

$sth = $model->db->prepare("
	SELECT 
		sap_matrix_ordertype_id,
		sap_matrix_ordertype_address_id,
		sap_matrix_ordertype_channel_id,
		sap_order_type_name
	FROM db_retailnet.sap_matrix_ordertypes
	INNER JOIN sap_order_types ON sap_order_type_id = sap_matrix_ordertype_type_id
");

$sth->execute();
$result = $sth->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$client = $row['sap_matrix_ordertype_address_id'];
		$channel = $row['sap_matrix_ordertype_channel_id'];
		$_MATRIX[$client][$channel] = $row['sap_order_type_name'];
	}
}


// headers :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_HEADERS = array();
$_COLUMNS = array();

$col=0;

// dropdown clients
$_HEADERS[$col] = "Clients";

$_COLUMNS[$col] = array(
	'data' => "client.value",
	'width' => 400
);

foreach ($_MPS_DISTRIBUTION_CHANNELS as $channel => $channelCode) {
	
	$col++;

	// sap channel code
	$_HEADERS[$col] = wordwrap($channelCode, 10, '<br />');

	$_COLUMNS[$col] = array(
		'data' => "channel_$channel.value",
		'width' => 85 ,
		'type' => 'dropdown',
		'source' => $_ORDER_TYPES,
		'strict' => true,
		'allowInvalid' => false,
		'readOnly' => $_READ_ONLY
	);
}

// sap clients :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_DATA = array();

$row = 0;

foreach ($_CLIENTS as $client => $clientName) {

	$_DATA[$row]['client'] = array(
		'id' => $client,
		'value' => $clientName
	);

	foreach ($_MPS_DISTRIBUTION_CHANNELS as $channel => $channelCode) {
		$_DATA[$row]["channel_$channel"] = array(
			'id' => $channel,
			'value' => $_MATRIX[$client][$channel]
		);
	}

	$row++;
}

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONDING:

header('Content-Type: text/json');
echo json_encode(array(
	'headers' => $_HEADERS,
	'columns' => $_COLUMNS,
	'data' => $_DATA,
	'errors' => $_ERRORS
));
