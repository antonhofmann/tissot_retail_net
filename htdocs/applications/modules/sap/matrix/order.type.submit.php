<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$_CLIENT = trim($_REQUEST['client']);
$_CHANNEL = trim($_REQUEST['channel']);
$_VALUE = trim($_REQUEST['value']);

$_JSON = array();
$_ERRORS = array();
$_SUCCESS = array();
$_CONSOLE = array();


// checkin :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_CLIENT) {
	$_ERRORS[] = "Client is not defined.";
}

if (!$_CHANNEL) {
	$_ERRORS[] = "Distribution channel not found.";
}

if ($_ERRORS) {
	$_ERRORS[] = "Invalid request. Your request is not submitted.<br>Check your data and please try again.";
	goto BLOCK_RESPONDING;
}


// matrix ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$model = new Model();

$sth = $model->db->prepare("
	SELECT *
	FROM sap_matrix_ordertypes
	WHERE sap_matrix_ordertype_address_id = ? 
	AND sap_matrix_ordertype_channel_id = ?
");

$sth->execute(array($_CLIENT, $_CHANNEL));
$result = $sth->fetch();
$_ID = $result['sap_matrix_ordertype_id'];

if ($_ID) {
	$_CONSOLE[] = "Matrix $_ID found";
}

// get sap channels ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_VALUE) {

	$sth = $model->db->prepare("
		SELECT sap_order_type_id AS id
		FROM sap_order_types
		WHERE REPLACE(LOWER(sap_order_type_name), ' ', '') = ?
	");

	$sth->execute(array(strtolower(str_replace(' ', '', $_VALUE))));
	$result = $sth->fetch();
	$_ORDER_TYPE_ID = $result['id'];

	if (!$_ORDER_TYPE_ID) {
		$_ERRORS[] = "SAP order type <b>$_VALUE</b> not found.";
		goto BLOCK_RESPONDING;
	}
}


// submitting ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_ID) {

	$sth = $model->db->prepare("
		DELETE FROM sap_matrix_ordertypes
		WHERE sap_matrix_ordertype_id = ?
	");

	$response = $sth->execute(array($_ID));
}

if ($_ORDER_TYPE_ID) {

	$sth = $model->db->prepare("
		INSERT INTO sap_matrix_ordertypes (
			sap_matrix_ordertype_address_id,
			sap_matrix_ordertype_channel_id,
			sap_matrix_ordertype_type_id,
			user_created
		)
		VALUES (?,?,?,?)
	");
	
	$response = $sth->execute(array($_CLIENT, $_CHANNEL, $_ORDER_TYPE_ID, $user->login));
}


if ($response) $_JSON['success'] = true;
else $_ERRORS[] = "Error on data submitting for sap order type $_VALUE, please contact system administrator.";


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONDING:

$_JSON['console'] = $_CONSOLE;
$_JSON['errors'] = $_ERRORS;

header('Content-Type: text/json');
echo json_encode($_JSON);
