<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$_CLIENT = trim($_REQUEST['client']);
$_PLANNING_TYPE = trim($_REQUEST['type']);
$_VALUE = trim($_REQUEST['value']);

$_JSON = array();
$_ERRORS = array();
$_SUCCESS = array();
$_CONSOLE = array();


// checkin :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_CLIENT) {
	$_ERRORS[] = "Client is not defined.";
}

if (!$_PLANNING_TYPE) {
	$_ERRORS[] = "Planning type is not defined.";
}

if ($_ERRORS) {
	$_ERRORS[] = "Invalid request. Your request is not submitted.<br>Check your data and please try again.";
	goto BLOCK_RESPONDING;
}


// matrix ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$model = new Model();

$sth = $model->db->prepare("
	SELECT *
	FROM sap_matrix_channels
	WHERE sap_matrix_channel_address_id = ? 
	AND sap_matrix_channel_planning_type_id = ?
");

$sth->execute(array($_CLIENT, $_PLANNING_TYPE));
$result = $sth->fetch();
$_ID = $result['sap_matrix_channel_id'];

if ($_ID) {
	$_CONSOLE[] = "Matrix $_ID found";
}

// get sap channels ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_VALUE) {

	$sth = $model->db->prepare("
		SELECT sap_distribution_channel_id AS id
		FROM sap_distribution_channels
		WHERE REPLACE(LOWER(sap_distribution_channel_code), ' ', '') = ?
	");

	$sth->execute(array(strtolower(str_replace(' ', '', $_VALUE))));
	$result = $sth->fetch();
	$_SAP_CHANNEL_ID = $result['id'];

	if (!$_SAP_CHANNEL_ID) {
		$_ERRORS[] = "Distribution channel <b>$_VALUE</b> not found.";
		goto BLOCK_RESPONDING;
	}
}


// submitting ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_ID) {

	$sth = $model->db->prepare("
		DELETE FROM sap_matrix_channels
		WHERE sap_matrix_channel_id = ?
	");

	$response = $sth->execute(array($_ID));
}

if ($_SAP_CHANNEL_ID) {

	$sth = $model->db->prepare("
		INSERT INTO sap_matrix_channels (
			sap_matrix_channel_address_id,
			sap_matrix_channel_planning_type_id,
			sap_matrix_channel_distchannel_id,
			user_created
		)
		VALUES (?,?,?,?)
	");
	
	$response = $sth->execute(array($_CLIENT, $_PLANNING_TYPE, $_SAP_CHANNEL_ID, $user->login));
}


if ($response) $_JSON['success'] = true;
else $_ERRORS[] = "Error on data submitting for sap distribution channel $_VALUE, please contact system administrator.";


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONDING:

$_JSON['console'] = $_CONSOLE;
$_JSON['errors'] = $_ERRORS;

header('Content-Type: text/json');
echo json_encode($_JSON);
