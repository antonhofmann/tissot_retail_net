<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['sap_parameter_id'];

$data = array();

$param = new Modul($application);
$param->setTable('sap_parameters');

if ($_REQUEST['sap_parameter_name']) {
	$data['sap_parameter_name'] = $_REQUEST['sap_parameter_name'];
}

if (isset($_REQUEST['sap_parameter_value'])) {
	$data['sap_parameter_value'] = $_REQUEST['sap_parameter_value'];
}

if ($data) {

	$param->read($id);

	if ($param->id) {
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		$response = $param->update($data);
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		$response = $id = $param->create($data);
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		$redirect = $_REQUEST['redirect']."/$id";
	}
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect,
	'id' => $id
));