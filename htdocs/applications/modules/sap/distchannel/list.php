<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$linkForm = $_REQUEST['form'];

$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'sap_distribution_channel_code, sap_distribution_channel_name';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "desc";
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

// filters
$fSearch = ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) ? $_REQUEST['search'] : null;

// session request
$_REQUEST = session::filter($application, "$controller.$archived.$action", true);


$model = new Model($application);

// filter: full text search
if ($fSearch) {
	$filters['search'] = "(
		sap_distribution_channel_code LIKE \"%$fSearch%\"
		OR sap_distribution_channel_name LIKE \"%$fSearch%\"
	)";
}


$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS
		sap_distribution_channel_id AS id,
		sap_distribution_channel_code AS code,
		sap_distribution_channel_name AS distribution_channel
	FROM sap_distribution_channels
")
->filter($filters)
->order($order, $direction)
->offset($offset, $rowsPerPage)
->fetchAll();


if ($result) {
	
	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);
	
	$pager = new Pager(array(
		'page' => $_REQUEST['page'],
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));
	
	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}

$table = new Table(array(
	'sort' => array('column' => $order, 'direction' => $direction)
));

$table->datagrid = $datagrid;

$table->code(
	Table::PARAM_SORT,
	'width=20%'
);

$table->distribution_channel(
	Table::PARAM_SORT,
	"href=$linkForm"
);

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// toolbox: add
if ($_REQUEST['add'])  {
	$toolbox[] = ui::button(array(
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'caption' => $translate->add_new
	));
}

$toolbox[] = ui::searchbox();

if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

echo $toolbox.$table;
	