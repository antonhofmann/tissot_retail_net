<?php

class Task {
	/**
	 * @var Task_Model
	 */
	protected $model;

	/**
	 * @var integer
	 */
	protected $id;

	/**
	 * Setup task model
	 */
	public function __construct() {
		$this->model = new Task_Model;
	}

	/**
	 * Allow getter access to task attributes
	 * @param  string $key
	 * @return mixed
	 */
	public function __get($key) {
		return $this->data["task_$key"];
	}

	/**
	 * Read a task from db
	 * @param  integer $id
	 * @return array  Task data
	 */
	public function read($id) {
		$this->data = $this->model->read($id);
		$this->id = $this->data['task_id'];
		return $this->data;
	}
	
	/**
	 * Create a task
	 * @param  array $data
	 * @return bool
	 */
	public function create($data) {
		if (is_array($data)) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
	}

	/**
	 * Update a task
	 * @param  array $data
	 * @return bool
	 */
	public function update($data) {
		if ($this->id && is_array($data)) {
			$result = $this->model->update($this->id, $data);
			$this->read($this->id);
			return $result;
		}
	}
}