<?php 

	class Mail_Template_Action {
		
		/**
		 * Template ID
		 *
		 * @var int
		 */
		public $id;
		
		/**
		 * Dataloader
		 * 
		 * @var array
		 */
		public $data;
		
		/**
		 * DB Mmodel
		 * 
		 * @return Mail_Template_Action_Model
		 */
		protected $model;
		
		
		public function __construct() {
			
			$this->model = new Mail_Template_Action_Model();
		}
		
		public function __get($key) {
			
			return $this->data["mail_template_action_$key"];
		}
		
		public function read($id) {
			
			$this->id = $id;
			
			return $this->data = $this->model->read($id);
		}
		
		public function create($data) {
			
			if (is_array($data)) {
				
				$this->data = $data;
				
				return $this->id = $this->model->create($data);
			}
		}
		
		public function update($data) {
			
			if ($this->id && is_array($data)) {
				
				$this->data = array_merge($this->data, $data);
				
				return $this->model->update($this->id, $data);
			}
		}
		
		public function delete($id) {
			
			if ($this->id) {
				
				return $this->model->delete($this->id);
			}
		}
	}