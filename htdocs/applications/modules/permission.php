<?php

	class permission { 
		
		const PERMISSION_EDIT = 'can_edit_pemissions';
		const DB_BIND_ROLES = 'INNER JOIN db_retailnet.role_permissions ON role_permission_permission = permission_id';
		
		public $data;

		public function __construct() {
			$this->model = new permission_model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["language_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['language_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($filters=null, $access=false) {
			$model = new permission_model(Connector::DB_CORE);
			$result = $model->loader($filters, $access);
			return _array::extract($result);
		}

		/**
		 * Get all permissions
		 * 
		 * @return array
		 */
		public function getAll() {

			$sth = $this->model->db->prepare("
				SELECT permission_name AS permission
				FROM permissions
			");

			$sth->execute();

			return $sth->fetchAll();
		}

		/**
		 * Get all user permissions
		 * 
		 * @return array
		 */
		public function getUserPermissions($user) {

			$sth = $this->model->db->prepare("
				SELECT DISTINCT 
					permission_name AS permission
				FROM user_roles 
				INNER JOIN roles ON user_role_role = role_id 
				INNER JOIN role_permissions ON role_id = role_permission_role 
				INNER JOIN permissions ON role_permission_permission = permission_id 
				WHERE user_role_user = ?
			");

			$sth->execute(array($user));

			return $sth->fetchAll();
		}

	}
	