<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// request vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

// mastersheet vars
$mastersheet = $_REQUEST['mps_mastersheet_id'];
$selected_companies = $_REQUEST['selected_companies'];

if ($mastersheet && $selected_companies) {

	$comment = trim($_REQUEST['mps_ordersheet_comment']);
	$openingDate = $_REQUEST['mps_ordersheet_openingdate'] ? date::sql($_REQUEST['mps_ordersheet_openingdate']) : null;
	$closingDate = $_REQUEST['mps_ordersheet_closingdate'] ? date::sql($_REQUEST['mps_ordersheet_closingdate']) : null;
	$state = $_REQUEST['mps_ordersheet_workflowstate_id'];
	
	$data = array();
	
	switch ($application) {
				
		case 'mps':
		

			$tableName = 'mps_ordersheets';
			
			// order sheet opening date
			if ($openingDate) $data['mps_ordersheet_openingdate'] = $openingDate;
			
			// order sheet closing date
			if ($closingDate) $data['mps_ordersheet_closingdate'] = $closingDate;
			
			// order sheet description
			if ($comment) $data['mps_ordersheet_comment'] = $comment;
			
			// order sheet workflow state
			if ($state) $data['mps_ordersheet_workflowstate_id'] = $state;

		break;

		case 'lps':
		
			
			$tableName = 'lps_launchplans';
			
			// order sheet opening date
			if ($openingDate) $data['lps_launchplan_openingdate'] = $openingDate;
			
			// order sheet closing date
			if ($closingDate) $data['lps_launchplan_closingdate'] = $closingDate;
			
			// order sheet description
			if ($comment) $data['lps_launchplan_comment'] = $comment;
			
			// order sheet workflow state
			if ($state) $data['lps_launchplan_workflowstate_id'] = $state;

		break;
	}

	if ($data) {
		
		$ordersheets = array_filter(explode(',', $selected_companies));
		
		// ordersheet builder
		$ordersheet = new Modul($application);
		$ordersheet->setTable($tableName);
		
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
	
		foreach ($ordersheets as $id) {
			
			$ordersheet->read($id);
			
			$response = $ordersheet->update($data);
		}
	}
	
	$message = $response ? $translate->message_request_updated : $translate->message_request_failure;
	$tab = 'existing_ordersheets';
} 
else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'tab' => $tab,
	'reset' => true,
	'selected_companies' => $selected_companies,
	'data' => $data
));
