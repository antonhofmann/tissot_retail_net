<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// request vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

// mastersheet vars
$mastersheet = $_REQUEST['mps_mastersheet_id'];
$ordersheets = $_REQUEST['selected_companies'];
$items = $_REQUEST['selected_items'];
$proposedQuantities = $_REQUEST['proposed'];

if ($ordersheets && $items) {
	
	// user selected order sheets and items
	$selectedOrdersheets = array_filter(explode(',', $ordersheets));
	$selectedItems = array_filter(explode(',', $items));
	
	// db model
	$model = new Model($application);

	// application dependency
	switch ($application) {
				
		case 'mps':
			
			$tableName = 'mps_ordersheets';
			$filedState = 'mps_ordersheet_workflowstate_id';

			// order sheet states
			$stateCompleted = 3;
			$stateApproved = 4;
			$stateRevision = 8;

			$queryGetMasterSheetItems = "
				SELECT DISTINCT
					mps_material_id AS id,
					mps_material_price AS price, 
					currency_id, 
					currency_exchange_rate, 
					currency_factor
				FROM mps_mastersheet_items
				INNER JOIN mps_materials ON mps_material_id = mps_mastersheet_item_material_id
				INNER JOIN db_retailnet.currencies ON currency_id = mps_material_currency_id
				WHERE mps_mastersheet_item_mastersheet_id = ?
			";

			$queryGetOrderSheetItems = "
				SELECT 
					mps_ordersheet_item_id AS id,
					mps_ordersheet_item_material_id AS item
				FROM mps_ordersheet_items
				WHERE mps_ordersheet_item_ordersheet_id = ?
			";

			$queryAddItem = "
				INSERT INTO mps_ordersheet_items (
					mps_ordersheet_item_ordersheet_id,
					mps_ordersheet_item_material_id,
					mps_ordersheet_item_price,
					mps_ordersheet_item_currency,
					mps_ordersheet_item_exchangrate,
					mps_ordersheet_item_factor,
					mps_ordersheet_item_quantity_proposed,
					mps_ordersheet_item_status,
					user_created,
					date_created
				)
				VALUES (?,?,?,?,?,?,?,?,?,NOW()) 
			";

			$queryUpdateItem = "
				UPDATE mps_ordersheet_items SET
					mps_ordersheet_item_quantity_proposed = ?,
					user_modified = ?,
					date_modified = NOW()
				WHERE mps_ordersheet_item_id = ?
			";

			$queryChangeOrderSheetState = "
				UPDATE mps_ordersheets SET
					mps_ordersheet_workflowstate_id = ?
				WHERE mps_ordersheet_id = ?
			";

		break;

		case 'lps':
			
			$tableName = 'lps_launchplans';
			$filedState = 'lps_launchplan_workflowstate_id';

			// launchplan states
			$stateCompleted = 3;
			$stateApproved = 4;
			$stateRevision = 8;

			$queryGetMasterSheetItems = "
				SELECT DISTINCT
					lps_reference_id AS id,
					lps_reference_price_point AS price, 
					currency_id, 
					currency_exchange_rate, 
					currency_factor
				FROM lps_mastersheet_items
				INNER JOIN lps_references ON lps_reference_id = lps_mastersheet_item_reference_id
				INNER JOIN db_retailnet.currencies ON currency_id = lps_reference_currency_id
				WHERE lps_mastersheet_item_mastersheet_id = ?
			";

			$queryGetOrderSheetItems = "
				SELECT 
					lps_launchplan_item_id AS id,
					lps_launchplan_item_reference_id AS item
				FROM lps_launchplan_items
				WHERE lps_launchplan_item_launchplan_id = ?
			";

			$queryAddItem = "
				INSERT INTO lps_launchplan_items (
					lps_launchplan_item_launchplan_id,
					lps_launchplan_item_reference_id,
					lps_launchplan_item_price,
					lps_launchplan_item_currency,
					lps_launchplan_item_exchangrate,
					lps_launchplan_item_factor,
					lps_launchplan_item_quantity_proposed,
					lps_launchplan_item_status,
					user_created,
					date_created
				)
				VALUES (?,?,?,?,?,?,?,?,?,NOW()) 
			";

			$queryUpdateItem = "
				UPDATE lps_launchplan_items SET
					lps_launchplan_item_quantity_proposed = ?,
					user_modified = ?,
					date_modified = NOW()
				WHERE lps_launchplan_item_id = ?
			";

			$queryChangeOrderSheetState = "
				UPDATE lps_launchplans SET
					lps_launchplan_workflowstate_id = ?
				WHERE lps_launchplan_id = ?
			";

		break;
	}

	
	// get mastersheet items
	$sth = $model->db->prepare($queryGetMasterSheetItems);
	$sth->execute(array($mastersheet));
	$mastersheetItems = $sth->fetchAll();
	
	$data = array();
	
	if ($mastersheetItems) {
		
		// set order sheet in revision state
		$set_in_revision = false;
		
		// ordersheet
		$ordersheet = new Modul($application);
		$ordersheet->setTable($tableName);

		// get order sheet items
		$getOrderSheetItems = $model->db->prepare($queryGetOrderSheetItems);
		
		// add order sheet item
		$addOrderSheetItem = $model->db->prepare($queryAddItem);

		// update order sheet item
		$updateOrderSheetItem = $model->db->prepare($queryUpdateItem);

		// change order sheet state
		$changeOrderSheetState = $model->db->prepare($queryChangeOrderSheetState);

		
		foreach ($selectedOrdersheets as $id) {
			
			// current order sheet items
			$currentItems = array();
			
			// read order sheet instance
			$ordersheet->read($id);
			
			// get order sheet items
			$getOrderSheetItems->execute(array($id));
			$ordersheetItems = $getOrderSheetItems->fetchAll();
			
			// gruop material keys
			if ($ordersheetItems) {
				foreach ($ordersheetItems as $row) {
					$k = $row['item'];
					$currentItems[$k] = $row['id'];
				}
			}
			
			// for eache master sheet item
			// save item in order sheet items
			foreach ($mastersheetItems as $row) { 
				
				$item = $row['id']; 
				
				// is this item selected from user
				if (in_array($item, $selectedItems)) { 
					
					// item exist in order sheet
					if ($currentItems[$item]) { 
						
						// if proposed quantiti is not null update them
						if ($proposedQuantities[$item]) {
							$response = $updateOrderSheetItem->execute(array(
								$proposedQuantities[$item],
								$user->login,
								$currentItems[$item]
							));
						}
					}
					// create new item for this order sheet
					else {

						$response = $addOrderSheetItem->execute(array(
							$id,
							$item,
							$row['price'],
							$row['currency_id'],
							$row['currency_exchange_rate'],
							$row['currency_factor'],
							$proposedQuantities[$item],
							1,
							$user->login
						));
						
						$set_in_revision = true;
					}
				}
			}
		} 
	
		// if order sheet item map is changed and order sheet is approved or completed 
		// set this order sheet in revision statemant
		if ($set_in_revision && in_array($state, array($stateCompleted, $stateApproved))) {
			$changeOrderSheetState->execute(array($stateRevision, $id));
		}
	
	}
	
	
	$message = $response ? $translate->message_request_saved : $translate->message_request_failure;
} 
else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'tab_items' => true,
	'reset' => false
));
	