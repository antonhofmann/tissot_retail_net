<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// request vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$MPS = in_array($application, array('mps')) ? true : false;
$LPS = in_array($application, array('lps')) ? true : false;

// mastersheet vars
$mastersheet = $_REQUEST['mps_mastersheet_id'];
$selected_companies = $_REQUEST['selected_companies'];
$comment = trim($_REQUEST['mps_ordersheet_comment']);
$openningDate = date::sql($_REQUEST['mps_ordersheet_openingdate']);
$closingDate = date::sql($_REQUEST['mps_ordersheet_closingdate']);

if (!$mastersheet || !$selected_companies) {
	$message = "Bad request";
	goto BLOCK_RESPONSE;
}
	
// clients
$companies = array_filter(explode(',', $selected_companies));
$selected_companies = join(',', $companies);

// get mastersheet items
$model = new Model($application);

switch ($application) {
			
	case 'mps':
	

		// workflow state preparation
		$state = 7;
		
		// master sheet items
		$queryMasterSheetItems = "
			SELECT DISTINCT
				mps_material_id AS item,
				mps_material_price AS price, 
				currency_id, 
				currency_exchange_rate, 
				currency_factor,
				mps_material_material_planning_type_id AS planning_type
			FROM mps_mastersheet_items
			INNER JOIN mps_materials ON mps_material_id = mps_mastersheet_item_material_id
			INNER JOIN db_retailnet.currencies ON currency_id = mps_material_currency_id
			WHERE mps_mastersheet_item_mastersheet_id = ?
		";

		$queryOrderSheet = "
			INSERT INTO mps_ordersheets (
				mps_ordersheet_address_id,
				mps_ordersheet_mastersheet_id,
				mps_ordersheet_workflowstate_id,
				mps_ordersheet_openingdate,
				mps_ordersheet_closingdate,
				mps_ordersheet_comment,
				user_created,
				date_created
			)
			VALUES (?,?,?,?,?,?,?,NOW())
		";

		$queryOrderSheetItem = "
			INSERT INTO mps_ordersheet_items (
				mps_ordersheet_item_ordersheet_id,
				mps_ordersheet_item_material_id,
				mps_ordersheet_item_quantity,
				mps_ordersheet_item_quantity_proposed,
				mps_ordersheet_item_quantity_approved,
				mps_ordersheet_item_price,
				mps_ordersheet_item_currency,
				mps_ordersheet_item_exchangrate,
				mps_ordersheet_item_factor,
				user_created,
				date_created
			)
			VALUES (?,?,?,?,?,?,?,?,?,?,NOW())
		";

		$queryGetStandardDummyQuantities = "
			SELECT address_additional_address, address_additional_dummie_quantity
			FROM db_retailnet.address_additionals
			WHERE address_additional_address IN ($selected_companies) 
			AND address_additional_dummie_quantity > 0
		";

		$queryGetDummyPlanningTypes = "
			SELECT 
				mps_material_planning_type_id, 
				mps_material_planning_ordersheet_standrad_dummy_quantity,
				mps_material_planning_ordersheet_approvment_automatically
			FROM mps_material_planning_types
			WHERE mps_material_planning_is_dummy = 1
		";

		$queryUpdateOrderSheet = "
			UPDATE mps_ordersheets SET 
				mps_ordersheet_workflowstate_id = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE mps_ordersheet_id = ?
		";

	break;

	case 'lps':
	

		// workflow state preparation
		$state = 1;
		
		// master sheet items
		$queryMasterSheetItems = "
			SELECT DISTINCT
				lps_reference_id AS item,
				lps_mastersheet_item_reference_comment AS comment,
				lps_reference_price_point AS price, 
				currency_id, 
				currency_exchange_rate, 
				currency_factor
			FROM lps_mastersheet_items
			INNER JOIN lps_references ON lps_reference_id = lps_mastersheet_item_reference_id
			INNER JOIN db_retailnet.currencies ON currency_id = lps_reference_currency_id
			WHERE lps_mastersheet_item_mastersheet_id = ?
		";

		$queryOrderSheet = "
			INSERT INTO lps_launchplans (
				lps_launchplan_address_id,
				lps_launchplan_customer_number,
				lps_launchplan_mastersheet_id,
				lps_launchplan_workflowstate_id,
				lps_launchplan_openingdate,
				lps_launchplan_closingdate,
				lps_launchplan_comment,
				user_created,
				date_created
			)
			VALUES (?,?,?,?,?,?,?,?,NOW())
		";

		$queryOrderSheetItem = "
			INSERT INTO lps_launchplan_items (
				lps_launchplan_item_launchplan_id,
				lps_launchplan_item_reference_id,
				lps_launchplan_item_price,
				lps_launchplan_item_currency,
				lps_launchplan_item_exchangrate,
				lps_launchplan_item_factor,
				lps_launchplan_item_comment,
				user_created,
				date_created
			)
			VALUES (?,?,?,?,?,?,?,?,NOW())
		";

	break;
}

// master sheet items
$sth = $model->db->prepare($queryMasterSheetItems);	
$sth->execute(array($mastersheet));	
$mastersheetItems = $sth->fetchAll();
$totalMastersheetItems = count($mastersheetItems);	

if (!$mastersheetItems) {
	$message = "Master sheet items not found";
	goto BLOCK_RESPONSE;
}

// ordersheet statement
$ordersheet = $model->db->prepare($queryOrderSheet);

// ordersheet item statement
$ordersheetItem = $model->db->prepare($queryOrderSheetItem);

$createdOrdersheets = 0;

$dummyPlanningTypes = array();
$dummyStandradQuantities = array();
$dummyTotalItems = array();

if ($MPS) {

	// get dummy planning types
	$result = $model->query($queryGetDummyPlanningTypes)->fetchAll();	

	if ($result) {
		
		foreach ($result as $row) {
			
			$key = $row['mps_material_planning_type_id'];
			
			$dummyPlanningTypes[$key] = array(
				'quantity' => $row['mps_material_planning_ordersheet_standrad_dummy_quantity'],
				'approvment' => $row['mps_material_planning_ordersheet_approvment_automatically']
			);
		}
	}

	// get standard dummy quantities
	$result = $model->query($queryGetStandardDummyQuantities)->fetchAll();	

	if ($result) {
		foreach ($result as $row) {
			$key = $row['address_additional_address'];
			$dummyStandradQuantities[$key] = $row['address_additional_dummie_quantity'];
		}
	}
}

if ($MPS) {
	$sthUpdateOrdersheet = $model->db->prepare($queryUpdateOrderSheet);
}

// for each selected companie,
// create new order sheet and new order sheet items set
foreach ($companies as $company) {

	$data = array();

	if (!$company) continue;

	$_CONSOLE[] = "START to create order sheet for company $company";

	if ($LPS) {

		$ar = explode('|', $company);

		if ($ar[0] && $ar[1]) {
			$data = array($ar[0], $ar[1], $mastersheet, $state, $openningDate, $closingDate, $comment, $user->login);
		}

	} else {
		$data = array($company, $mastersheet, $state, $openningDate, $closingDate, $comment, $user->login);
	}
	

	$ordersheet->execute($data);
	$newOrderSheetId = $model->db->lastInsertId();

	// create new items from master sheet items
	if (!$newOrderSheetId) {
		$_ERRORS[] = "Add order sheet failure for company $company";
		continue;
	}
		
	$createdOrdersheets++;
	$totalDummyItems = 0;
	$totalAutoApprovment = 0;

	$_CONSOLE[] = "New order sheet $newOrderSheetId";
		
	foreach ($mastersheetItems as $item) {

		if ($LPS) {

			$newitem = $ordersheetItem->execute(array(
				$newOrderSheetId, 
				$item['item'], 
				$item['price'], 
				$item['currency_id'], 
				$item['currency_exchange_rate'], 
				$item['currency_factor'], 
				$item['comment'], 
				$user->login
			));

			$newitem  = $newitem ? $model->db->lastInsertId() : null;

		} else {

			$type = $item['planning_type'];
			$planningType = $dummyPlanningTypes[$type] ?: array();
			$dummyQuantities = null;

			if ($planningType['quantity'] && $dummyStandradQuantities[$company] > 0) {
				$dummyQuantities = $dummyStandradQuantities[$company];
			}

			$newitem = $ordersheetItem->execute(array(
				$newOrderSheetId, 
				$item['item'], 
				$dummyQuantities,
				$dummyQuantities,
				$dummyQuantities,
				$item['price'], 
				$item['currency_id'], 
				$item['currency_exchange_rate'], 
				$item['currency_factor'], 
				$user->login
			));

			$newitem  = $newitem ? $model->db->lastInsertId() : null;

			if ($newitem) {
				$_CONSOLE[] = "New item $newitem for order sheet $newOrderSheetId";
			}

			//if ($newitem && $dummyQuantities > 0) {
			if ($newitem and count($dummyStandradQuantities) > 0) {
				$_CONSOLE[] = "Dummy quantities $dummyQuantities for item $newitem";

				$totalDummyItems++;
				
				if ($planningType['approvment']) {
					$totalAutoApprovment++;
					$_CONSOLE[] = "Approved dummy quantities $dummyQuantities for item $newitem";
				}
			}
		}			
	}

	// auto approvment
	if ($MPS && $newOrderSheetId && $totalDummyItems > 0) {

		$_CONSOLE[] = "Total dummy items: $totalDummyItems";
		$_CONSOLE[] = "Total dummy approved items: $totalAutoApprovment";

		$state = $totalDummyItems==$totalAutoApprovment ? Workflow_State::STATE_APPROVED : Workflow_State::STATE_PREPARATION;
		$action = $sthUpdateOrdersheet->execute(array($state, $user->login, $newOrderSheetId));

		if ($action) $_CONSOLE[] = "Set order sheet $newOrderSheetId state $state";
	}

	$_CONSOLE[] = "END order sheet $newOrderSheetId";
	$_CONSOLE[] = "";
}

$response = count($companies)==$createdOrdersheets ? true : false;

if ($response) $message = $translate->message_request_inserted;
else $message = $createdOrdersheets ? "Some of orrder sheets are not created.<br>Total created: $createdOrdersheets" : $translate->message_request_failure;

$tab = 'existing_ordersheets';

BLOCK_RESPONSE:

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'tab' => $tab,
	'reset' => true,
	'errors' => $_ERRORS,
	'console' => $_CONSOLE
));
	