<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// request vars
$application = $_REQUEST['application'];
$year = $_REQUEST['mps_mastersheet_year'];
$mastersheet = $_REQUEST['mps_mastersheet_id'];
$year = $_REQUEST['mps_mastersheet_year'];
$selected_companies = $_REQUEST['selected_companies'];
$selected_items = $_REQUEST['selected_items'];
$selected_tab = $_REQUEST['selected_tab'];
$search = $_REQUEST['search'];
$country = $_REQUEST['countries'];
$counter = 0;

$MPS = in_array($application, array('mps')) ? true : false;
$LPS = in_array($application, array('lps')) ? true : false;

// db connector
$model = new Model($application);


switch ($selected_tab) {
	
	case 'new_ordersheets':
		
		// filter: active companies
		$filters['active'] = "address_active = 1";
		
		// filter: full text search
		if ($search && $search <> $translate->search) {
			$filters['search'] = "(address_company LIKE '%$search%' OR country_name LIKE '%$search%')";
		}
		
		// filer: countries
		if ($_REQUEST['countries']) {
			$filters['countries'] = "country_id = ".$_REQUEST['countries'];
		}

		switch ($application) {
			
			case 'mps':
			

				// filter: planning
				
				
				$filters['planning'] = "address_involved_in_planning = 1";
				


				$queryClients = "
					SELECT DISTINCT	
						address_id, 
						address_company, 
						country_name 
					FROM db_retailnet.addresses
					INNER JOIN db_retailnet.countries ON country_id = address_country
				";

				$qDropDownCountries = "
					SELECT DISTINCT country_id, country_name 
					FROM db_retailnet.addresses
					INNER JOIN db_retailnet.countries ON country_id = address_country
				";

				$qExistedOrderSheets = "
					SELECT mps_ordersheet_address_id AS client
					FROM mps_ordersheets
					WHERE mps_ordersheet_mastersheet_id = ?
				";

			break;

			case 'lps':
			

				// filter: planning
				$filters['planning'] = "address_involved_in_lps = 1";

				$filters['default'] = "(
					address_additional_lps_customerno1 IS NOT NULL
					OR address_additional_lps_customerno2 IS NOT NULL
					OR address_additional_lps_customerno3 IS NOT NULL
					OR address_additional_lps_customerno4 IS NOT NULL
					OR address_additional_lps_customerno5 IS NOT NULL
				)";

				$queryClients = "
					SELECT DISTINCT	
						address_id, 
						address_company, 
						country_name 
					FROM db_retailnet.addresses
					INNER JOIN db_retailnet.countries ON country_id = address_country
					INNER JOIN db_retailnet.address_additionals ON address_id = address_additional_address
				";

				$queryGetCustomerNumbers = "
					SELECT *
					FROM db_retailnet.address_additionals
					INNER JOIN db_retailnet.addresses ON address_id = address_additional_address
					WHERE address_active = 1 AND address_involved_in_lps = 1
				";

				$qDropDownCountries = "
					SELECT DISTINCT country_id, country_name 
					FROM db_retailnet.addresses
					INNER JOIN db_retailnet.countries ON country_id = address_country
					INNER JOIN db_retailnet.address_additionals ON address_id = address_additional_address
				";

				$qExistedOrderSheets = "
					SELECT 
						lps_launchplan_address_id AS client,
						lps_launchplan_customer_number AS customer_number
					FROM lps_launchplans
					WHERE lps_launchplan_mastersheet_id = ?
				";	

			break;
		}

		if ($mastersheet) {

			// filter: exclude existing companies
			$sth = $model->db->prepare($qExistedOrderSheets);
			$sth->execute(array($mastersheet));
			$res = $sth->fetchAll();
			$existedOrderSheets = array();
			
			if ($res) {
				foreach ($res as $row) {
					$key = $LPS ? $row['client'].'|'.$row['customer_number'] : $row['client'];
					$existedOrderSheets[$key] = true;
				}
			}
		
			// datagrid	
			$result = $model->query($queryClients)->filter($filters)->order('country_name, address_company')->fetchAll(); 
		
			if ($result) {

				$customerNumbers = array();
				$datagrid = array();

				if ($queryGetCustomerNumbers) {

					$sth = $model->db->prepare($queryGetCustomerNumbers);
					$sth->execute();
					$res = $sth->fetchAll();

					if ($res) {
						foreach ($res as $row) {
							$client = $row['address_additional_address'];
							if ($row['address_additional_lps_customerno1']) $customerNumbers[$client][] = $row['address_additional_lps_customerno1'];
							if ($row['address_additional_lps_customerno2']) $customerNumbers[$client][] = $row['address_additional_lps_customerno2'];
							if ($row['address_additional_lps_customerno3']) $customerNumbers[$client][] = $row['address_additional_lps_customerno3'];
							if ($row['address_additional_lps_customerno4']) $customerNumbers[$client][] = $row['address_additional_lps_customerno4'];
							if ($row['address_additional_lps_customerno5']) $customerNumbers[$client][] = $row['address_additional_lps_customerno5'];
						}
					}
				}
			
				foreach ($result as $row) {

					$key = $row['address_id'];

					// for clinets with additiona customer nubers
					// for eache custom number generate an order sheet
					if ($customerNumbers[$key]) {

						foreach ($customerNumbers[$key] as $cn) {
							
							$k = "$key|$cn";
							
							if (!$existedOrderSheets[$k]) {
								$row['customer_number'] = $cn;
								$row['companies'] = "<input type='checkbox' name=companies[$k] value='$k' class='checkbox' />";
								$datagrid[$k] = $row;
							}
						}

					} elseif (!$existedOrderSheets[$key]) {
						$row['companies'] = "<input type=checkbox name=companies[$key] value=$key class='checkbox' />";
						$datagrid[$key] = $row;
					}
				}
			}
		
			if ($datagrid) {

				$table = new Table();
				$table->datagrid = $datagrid;
				
				$table->caption('companies', "<input type=checkbox class='checkall' />");
			
				$table->country_name(
					Table::ATTRIBUTE_NOWRAP,
					'width=20%'
				);
				
				$table->address_company(
					Table::ATTRIBUTE_NOWRAP
				);

				if ($LPS) {
					$table->customer_number(
						Table::ATTRIBUTE_NOWRAP,
						'width=10%'
					);
				}
				
				$table->companies(
					Table::ATTRIBUTE_NOWRAP,
					'width=20px',
					'align=center'
				);

				$table = $table->render();
				
			}
			else {
				$table = "<div class=emptybox>No matching records found</div>";
			}

			// toolbox: serach full text
			$toolbox[] = ui::searchbox();
			
			// toolbox: countries
			$result = $model->query($qDropDownCountries)
			->filter($filters)
			->exclude('countries')
			->order('country_name')
			->fetchAll();
			
			$toolbox[] = ui::dropdown($result, array(
				'name' => 'countries',
				'id' => 'countries',
				'class' => 'submit',
				'value' => $_REQUEST['countries'],
				'caption' => $translate->all_countries
			));
		} 
		else {
			$table = "<div class=emptybox>Select Master Sheet</div>";
		}
	
	break;
	
	case 'existing_ordersheets' :
		
		// selected companies
		$selected_companies = ($selected_companies) ? explode(',', $selected_companies) : array();
		
		// filter: active companies
		$filters['active'] = "address_active = 1";

		switch ($application) {
			
			case 'mps':
			

				// filter: planning
				$filters['planning'] = "address_involved_in_planning = 1";
				
				// filter: full text search
				if ($search && $search <> $translate->search) {
				
					$filters['search'] = "(
						address_company LIKE '%$search%'
						OR country_name LIKE '%$search%'
						OR mps_workflow_state_name LIKE '%$search%'
						OR mps_ordersheet_openingdate LIKE '%$search%'
						OR mps_ordersheet_closingdate LIKE '%$search%'
					)";
				}
				
				// filer: countries
				if ($_REQUEST['countries']) {
					$filters['countries'] = "country_id = ".$_REQUEST['countries'];
				}
				
				// filter: workflow state
				$filters['workflowstates'] = "mps_ordersheet_workflowstate_id IN (1,2,3,4,7,8)";
				
				// filter: mastersheet
				$filters['mastersheet'] = "mps_ordersheet_mastersheet_id = $mastersheet";
				
				// datagrid
				$result = $model->query("
					SELECT DISTINCT
						mps_ordersheet_id AS id, 
						mps_workflow_state_name AS workflow_state, 
						DATE_FORMAT(mps_ordersheet_openingdate,'%d.%m.%Y') AS opening_date,
						address_company, 
						country_name,
						(
							SELECT COUNT(mps_ordersheet_item_id)
							FROM mps_ordersheet_items
							WHERE mps_ordersheet_item_ordersheet_id = mps_ordersheet_id
						) AS total_items
					FROM mps_ordersheets
					INNER JOIN db_retailnet.addresses ON mps_ordersheet_address_id = address_id
					INNER JOIN db_retailnet.countries ON address_country = country_id
					INNER JOIN mps_workflow_states ON mps_workflow_state_id = mps_ordersheet_workflowstate_id
				")
				->filter($filters)
				->order('country_name')
				->order('address_company')
				->order('mps_ordersheet_openingdate')
				->fetchAll();

				// toolbox: countries
				$countries = $model->query("
					SELECT DISTINCT 
						country_id, country_name 
					FROM mps_ordersheets
					INNER JOIN db_retailnet.addresses ON mps_ordersheet_address_id = address_id
					INNER JOIN db_retailnet.countries ON address_country = country_id
					INNER JOIN mps_workflow_states ON mps_workflow_state_id = mps_ordersheet_workflowstate_id
				")
				->filter($filters)
				->exclude('countries')
				->order('country_name')
				->fetchAll();

			break;

			case 'lps':
			

				// filter: planning
				$filters['planning'] = "address_involved_in_lps = 1";
				
				// filter: full text search
				if ($search && $search <> $translate->search) {
				
					$filters['search'] = "(
						address_company LIKE '%$search%'
						OR country_name LIKE '%$search%'
						OR lps_workflow_state_name LIKE '%$search%'
						OR lps_launchplan_openingdate LIKE '%$search%'
						OR lps_launchplan_closingdate LIKE '%$search%'
					)";
				}
				
				// filer: countries
				if ($_REQUEST['countries']) {
					$filters['countries'] = "country_id = ".$_REQUEST['countries'];
				}
				
				// filter: workflow state
				$filters['workflowstates'] = "lps_launchplan_workflowstate_id IN (1,2,3,4,7,8)";
				
				// filter: mastersheet
				$filters['mastersheet'] = "lps_launchplan_mastersheet_id = $mastersheet";
				
				// datagrid
				$result = $model->query("
					SELECT DISTINCT
						lps_launchplan_id AS id, 
						lps_workflow_state_name AS workflow_state, 
						lps_launchplan_customer_number AS customer_number,
						DATE_FORMAT(lps_launchplan_openingdate,'%d.%m.%Y') AS opening_date,
						address_company, 
						country_name,
						(
							SELECT COUNT(lps_launchplan_item_id)
							FROM lps_launchplan_items
							WHERE lps_launchplan_item_launchplan_id = lps_launchplan_id
						) AS total_items
					FROM lps_launchplans
					INNER JOIN db_retailnet.addresses ON lps_launchplan_address_id = address_id
					INNER JOIN db_retailnet.countries ON address_country = country_id
					INNER JOIN lps_workflow_states ON lps_workflow_state_id = lps_launchplan_workflowstate_id
				")
				->filter($filters)
				->order('country_name')
				->order('address_company')
				->order('lps_launchplan_openingdate')
				->fetchAll(); 

				// toolbox: countries
				$countries = $model->query("
					SELECT DISTINCT 
						country_id, country_name 
					FROM lps_launchplans
					INNER JOIN db_retailnet.addresses ON lps_launchplan_address_id = address_id
					INNER JOIN db_retailnet.countries ON address_country = country_id
					INNER JOIN lps_workflow_states ON lps_workflow_state_id = lps_launchplan_workflowstate_id
				")
				->filter($filters)
				->exclude('countries')
				->order('country_name')
				->fetchAll();

			break;
		}
			
		$datagrid = array();
		$counter = 0;

		if ($result) {

			foreach ($result as $row) {
				$key = $row['id'];
				$checked = (in_array($key, $selected_companies)) ? 'checked=checked' : null;
				$counter = $checked ? $counter+1 : $counter;
				$row['companies'] = "<input type=checkbox name=companies[$key] value=$key class='checkbox' $checked />";
				$row['items'] = $row['total_items'];
				$datagrid[$key] = $row;
			}
			
			$checkedAll = (count($datagrid) == $counter) ? 'checked=checked' : null;
		}
		
		if ($datagrid) {
			
			$table = new Table();
			$table->datagrid = $datagrid;
			
			$table->caption('companies', "<input type=checkbox class='checkall' $checkedAll />");
			
			$table->country_name(
				Table::ATTRIBUTE_NOWRAP,
				'width=10%'
			);
			
			$table->address_company();

			if ($LPS) {
				$table->customer_number(
					Table::ATTRIBUTE_NOWRAP,
					'width=10%'
				);
			}
			
			$table->workflow_state(
				Table::ATTRIBUTE_NOWRAP,
				'width=5%'
			);
			
			$table->opening_date(
				Table::ATTRIBUTE_NOWRAP,
				'width=5%'
			);
			
			$table->items(
				Table::ATTRIBUTE_NOWRAP,
				Table::ATTRIBUTE_ALIGN_CENTER,
				'width=5%'
			);
			
			$table->companies(
				Table::ATTRIBUTE_NOWRAP,
				'width=20px'
			);
			
			$table = $table->render();
		}
		else {
			$table = "<div class=emptybox>No matching records found</div>";
		}
	
		
		// toolbox: serach full text
		$toolbox[] = ui::searchbox();
		
		// toolbox: countries
		$result = $model
		->query("SELECT DISTINCT country_id, country_name FROM mps_ordersheets")
		->bind(Ordersheet::DB_BIND_COMPANIES)
		->bind(Company::DB_BIND_COUNTRIES)
		->bind(Ordersheet::DB_BIND_WORKFLOW_STATES)
		->filter($filters)
		->exclude('countries')
		->order('country_name')
		->fetchAll();
		
		$toolbox[] = ui::dropdown($countries, array(
			'name' => 'countries',
			'id' => 'countries',
			'class' => 'submit',
			'value' => $_REQUEST['countries'],
			'caption' => $translate->all_countries
		));
		
	break;
	
	case 'ordersheet_items':
		
		// selected companies
		$selected_items = ($selected_items) ? explode(',', $selected_items) : array();

		$mastersheetItems = array();
		$ordersheetItems = array();

		switch ($application) {
			
			case 'mps':
			
				
				// master sheet items
				$result = $model->query("
					SELECT DISTINCT
						mps_material_id AS id,
						mps_material_code AS code,
						mps_material_name AS name,
						mps_material_collection_code AS collection,
						mps_material_planning_type_id AS group_id,
						mps_material_planning_type_name AS group_name,
						mps_material_collection_category_id AS subgroup_id,
						mps_material_collection_category_code AS subgroup_name
					FROM mps_mastersheet_items
					INNER JOIN mps_materials ON mps_material_id = mps_mastersheet_item_material_id
					INNER JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id
					INNER JOIN mps_material_collections ON mps_material_collection_id = mps_material_material_collection_id
					INNER JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id
					WHERE mps_mastersheet_item_mastersheet_id = $mastersheet
					ORDER BY 
						mps_material_planning_type_name,
						mps_material_collection_category_code,
						mps_material_code
				")->fetchAll();

				$mastersheetItems = $result ? _array::datagrid($result) : array();
				
				// get existing items form selected companies
				$result = $model->query("
					SELECT DISTINCT
						mps_material_id AS id,
						mps_material_code AS code,
						mps_material_name AS name,
						mps_material_collection_code AS collection,
						mps_material_planning_type_id AS group_id,
						mps_material_planning_type_name AS group_name,
						mps_material_collection_category_id AS subgroup_id,
						mps_material_collection_category_code AS subgroup_name
					FROM mps_ordersheet_items 	
					INNER JOIN mps_ordersheets ON  mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
					INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
					INNER JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id
					INNER JOIN mps_material_collections ON mps_material_collection_id = mps_material_material_collection_id
					INNER JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id
					WHERE mps_ordersheet_item_ordersheet_id IN ($selected_companies)
					ORDER BY 
						mps_material_planning_type_name,
						mps_material_collection_category_code,
						mps_material_code
				")->fetchAll();

				$ordersheetItems = $result ? _array::datagrid($result) : array();

				$hasProposedItem = true;

				$captions = array(
					'collection' => $translate->mps_material_collection_code,
					'code' => $translate->mps_material_code,
					'name' => $translate->mps_material_name,
					'proposed_quantity' => $translate->mps_ordersheet_item_quantity_proposed
				);

			break;

			case 'lps':
			

				// master sheet items
				$result = $model->query("
					SELECT DISTINCT
						lps_reference_id AS id,
						lps_reference_code AS code,
						lps_reference_name AS name,
						lps_mastersheet_item_reference_comment AS comment,
						lps_collection_code AS collection,
						lps_product_group_id AS group_id,
						lps_product_group_name AS group_name,
						lps_collection_category_id AS subgroup_id,
						lps_collection_category_code AS subgroup_name
					FROM lps_mastersheet_items
					INNER JOIN lps_references ON lps_reference_id = lps_mastersheet_item_reference_id
					INNER JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id
					INNER JOIN lps_collections ON lps_collection_id = lps_reference_collection_id
					INNER JOIN lps_collection_categories ON lps_collection_category_id = lps_reference_collection_category_id
					WHERE lps_mastersheet_item_mastersheet_id = $mastersheet
					ORDER BY 
						lps_product_group_name,
						lps_collection_category_code,
						lps_reference_code
				")->fetchAll();

				$mastersheetItems = $result ? _array::datagrid($result) : array();
				
				// get existing items form selected companies
				$result = $model->query("
					SELECT DISTINCT
						lps_reference_id AS id,
						lps_reference_code AS code,
						lps_reference_name AS name,
						lps_collection_code AS collection,
						lps_product_group_id AS group_id,
						lps_product_group_name AS group_name,
						lps_collection_category_id AS subgroup_id,
						lps_collection_category_code AS subgroup_name
					FROM lps_launchplan_items 	
					INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
					INNER JOIN lps_references ON lps_reference_id = lps_launchplan_item_reference_id
					INNER JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id
					INNER JOIN lps_collections ON lps_collection_id = lps_reference_collection_id
					INNER JOIN lps_collection_categories ON lps_collection_category_id = lps_reference_collection_category_id
					WHERE lps_launchplan_item_launchplan_id IN ($selected_companies)
					ORDER BY 
						lps_product_group_name,
						lps_collection_category_code,
						lps_reference_code
				")->fetchAll();

				$ordersheetItems = $result ? _array::datagrid($result) : array();
				
				$hasProposedItem = false;

				$captions = array(
					'collection' => $translate->collection_code,
					'code' => $translate->reference_code,
					'name' => $translate->reference_name
				);

			break;
		}
		
		// summary of items
		$items = $mastersheetItems + $ordersheetItems;
		
		if ($items) {

			foreach ($items as $item => $row) {
				
				$group = $row['group_id'];
				$subgroup = $row['subgroup_id'];

				// item visibility
				$checked = ($ordersheetItems[$item]) ? 'checked=checked' : null;
				$checkbox = "<input type='checkbox' class='checkbox' name='item[$item]' value = '$item' $checked />";
				
				$datagrid[$group]['caption'] = $row['group_name'];
				$datagrid[$group]['subgroup'][$subgroup]['caption'] = $row['subgroup_name'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['code'] = $row['code'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['name'] = $row['name'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['comment'] = $row['comment'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['collection'] = $row['collection'];
				$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['item'] = $checkbox;

				//proposed textbox
				if ($hasProposedItem) {
					$textbox = "<input type='text' class='textbox'  name='proposed[$item]' index='$item' />";
					$datagrid[$group]['subgroup'][$subgroup]['items'][$item]['proposed_quantity'] = $textbox;
				}
				
				if ($checked) {
					$subgroupCheckAll[$subgroup] = $subgroupCheckAll[$subgroup] + 1;
				}
			}
		}
		
		if ($datagrid) { 
				
			foreach ($datagrid as $group => $data) {
					
				$list  .= "<h5>{$data[caption]}</h5>";
		
				foreach ($data['subgroup'] as $subgroup => $items) {
		
					$list  .= "<h6>{$item[caption]}</h6>";
		
					$table = new Table();
					$table->datagrid = $items['items'];
					
					$checked = (count($table->datagrid) == $subgroupCheckAll[$subgroup]) ? 'checked=checked' : null;
					$table->caption('item', "<input type=checkbox class='checkall' $checked />");
					
					$table->collection(
						Table::ATTRIBUTE_NOWRAP, 
						'width=20%',
						'caption='.$captions['collection']
					);
					
					$table->code(
						Table::ATTRIBUTE_NOWRAP, 
						'width=20%',
						'caption='.$captions['code']
					);
					
					$table->name(
						'caption='.$captions['name']
					);

					if ($LPS) {
						$table->comment(
							Table::ATTRIBUTE_NOWRAP,
							'width=20%',
							'caption=Comment'
						);
					}
					
					if ($hasProposedItem) {
						$table->proposed_quantity(
							Table::ATTRIBUTE_NOWRAP, 
							Table::ATTRIBUTE_ALIGN_RIGHT, 
							'width=5%',
							'caption='.$captions['proposed_quantity']
						);
					}
					
					$table->item(
						Table::ATTRIBUTE_NOWRAP, 
						'width=20px'
					);
					
					$list .= $table->render();
				}
			}
			
			$table = $list;
		}
		else  $table = html::emptybox($translate->empty_result);
	
	break;
}

// toolbox: form
if ($toolbox) {
	$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
}

echo $toolbox.$table;

