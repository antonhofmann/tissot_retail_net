<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// request vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

// mastersheet vars
$mastersheet = $_REQUEST['mps_mastersheet_id'];
$companies = $_REQUEST['selected_companies'];
$items = $_REQUEST['selected_items'];

if ($companies && $items) {
	
	$deleted = array();
	$not_deleted = array();
	$return_items = array();

	// database model
	$model = new Model($application);
	
	// user selected ordersheets
	$selectedOrdersheets = explode(',', $companies);
	
	// user selected items
	$selectedItems = explode(',', $items);

	// application dependency
	switch ($application) {
				
		case 'mps':
		
			
			$tableName = 'mps_ordersheets';
			$filedState = 'mps_ordersheet_workflowstate_id';
			
			// order sheet states
			$stateCompleted = 3;
			$stateApproved = 4;
			$stateRevision = 8;

			// order sheet deletable statments
			$deletableStates = array(1,2,7,8);

			$queryGetOrderSheetItems = "
				SELECT 
					mps_ordersheet_item_id AS id,
					mps_ordersheet_item_material_id AS item
				FROM mps_ordersheet_items
				WHERE mps_ordersheet_item_ordersheet_id = ?
			";

			$queryDeleteOrderSheetItem = "
				DELETE FROM mps_ordersheet_items
				WHERE mps_ordersheet_item_id = ?
			";

			$queryChangeOrderSheetState = "
				UPDATE mps_ordersheets SET
					mps_ordersheet_workflowstate_id = ?
				WHERE mps_ordersheet_id = ?
			";

		break;

		case 'lps':
		
			
			$tableName = 'lps_launchplans';
			$filedState = 'lps_launchplan_workflowstate_id';
			
			// launchplan states
			$stateCompleted = 3;
			$stateApproved = 4;
			$stateRevision = 8;

			// order sheet deletable statments
			$deletableStates = array(1,2,3,6);

			$queryGetOrderSheetItems = "
				SELECT 
					lps_launchplan_item_id AS id,
					lps_launchplan_item_reference_id AS item
				FROM lps_launchplan_items
				WHERE lps_launchplan_item_launchplan_id = ?
			";

			$queryDeleteOrderSheetItem = "
				DELETE FROM lps_launchplan_items
				WHERE lps_launchplan_item_id = ?
			";

			$queryChangeOrderSheetState = "
				UPDATE lps_launchplans SET
					lps_launchplan_workflowstate_id = ?
				WHERE lps_launchplan_id = ?
			";

		break;
	}


	// ordersheet
	$ordersheet = new Modul($application);
	$ordersheet->setTable($tableName);

	// get ordersheet items
	$getOrderSheetItems = $model->db->prepare($queryGetOrderSheetItems);
	
	// delete ordersheet item
	$deleteOrderSheetItem = $model->db->prepare($queryDeleteOrderSheetItem);
	
	// change order sheet state
	$changeOrderSheetState = $model->db->prepare($queryChangeOrderSheetState);


	foreach ($selectedOrdersheets as $id) {
		
		// set order sheet in revision state
		$set_in_revision = false;
		
		// read order sheet instance
		$ordersheet->read($id);

		// workflow state
		$state = $ordersheet->data[$filedState];
		
		// get order sheet items
		$getOrderSheetItems->execute(array($id));
		$ordersheetItems = $getOrderSheetItems->fetchAll();
 
		if ($ordersheetItems && $selectedItems) {
			
			foreach ($ordersheetItems as $row) {
				
				$item = $row['item'];
				
				// if item is selected
				if (in_array($item, $selectedItems)) {
					
					// is order sheet in deletable state
					if (in_array($state, $deletableStates)){
						
						// remove item from order sheet
						$response = $deleteOrderSheetItem->execute(array($row['id']));
						
						// set order sheet in revision state
						$set_in_revision = true;
						
						// store delete items
						array_push($deleted, $item);
					}
					else {
						
						// store not delete items
						array_push($not_deleted, $item);
					}
				} 
				else {
					
					// store returned item (not deleted)
					array_push($return_items, $item);
				}
			}
		}
		
		// if order sheet item map is changed and order sheet is approved or completed 
		// set this order sheet in revision statemant
		if ($set_in_revision && in_array($state, array($stateCompleted, $stateApproved))) {
			$changeOrderSheetState->execute(array($stateRevision, $id));
		}
	}
	
	$message = $translate->message_request_deleted;
	
	if ($return_items) {
		$return_items = array_unique($return_items);
		$selected = join(',', $return_items);
	}
	
	if ($not_deleted) {			
		$message .= "<br /><br />";
		$message .= "Only order sheets with the one of the following workflow states can be deleted: ";
		$message .= "<b><b>in preparation</b>, <b>open</b>,, in progress</b>, and <b>in revision</b>. ";
	}
} 
else {
	$response = false;
	$message =$translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'tab_items' => true,
	'tab' => 'ordersheet_items',
	'selected_items' => $selected,
	'reset' => false
));
	