<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$application = $_REQUEST['application'];
	$section = $_REQUEST['section'];
	
	$model = new Model($application);
	
	switch ($section) {
		
		case 'mastersheets':
			
			$value = $_REQUEST['value'];
			
			$json[][] = "Select";

			switch ($application) {
				
				case 'mps':
				
					
					$query = "
						SELECT mps_mastersheet_id, mps_mastersheet_name 
						FROM mps_mastersheets
						WHERE mps_mastersheet_archived = 0 AND mps_mastersheet_consolidated = 0 AND mps_mastersheet_year = ?
						ORDER BY mps_mastersheet_name
					";

				break;

				case 'lps':
				
					
					$query = "
						SELECT lps_mastersheet_id, lps_mastersheet_name 
						FROM lps_mastersheets
						WHERE lps_mastersheet_archived = 0 AND lps_mastersheet_consolidated = 0 AND lps_mastersheet_year = ?
						ORDER BY lps_mastersheet_name
					";

				break;
			}

			$sth = $model->db->prepare($query);
			$sth->execute(array($value));
			$result = $sth->fetchAll();
			
			if ($result) {
				foreach ($result as $row) {
					list($key,$value) = array_values($row);
					$json[][$key] = $value;
				}
			}
			
		break;
		
		case 'has_ordersheets':

			switch ($application) {
				
				case 'mps':
				
					
					$query = "
						SELECT * 
						FROM mps_ordersheets 
						WHERE mps_ordersheet_mastersheet_id = ?
					";

				break;

				case 'lps':
				
					
					$query = "
						SELECT * 
						FROM lps_launchplans 
						WHERE lps_launchplan_mastersheet_id = ?
					";

				break;
			}
				
			$sth = $model->db->prepare($query);
			$sth->execute(array($_REQUEST['mps_mastersheet_id']));
			$result = $sth->fetchAll();
			
			$json['response'] = ($result) ? true : false;
				
		break;
	}
	
	echo json_encode($json);
	