<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

// session request
$_REQUEST = session::filter($application, "$controller.$archived.$action", true);


// filters
$fSearch = ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) ? $_REQUEST['search'] : null;

// db model
$model = new Model($application);
$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);

// application identificators
$_DIVISIONS = array(
	'mps' => '08'
);


/**
 * Get all submitted order sheet items
 **************************************************************************************************/

$_ORDERSHEET_ITEMS = array();

$filter =  $fSearch ? " AND (
	mps_ordersheet_item_purchase_order_number LIKE \"%$fSearch%\"
	OR mps_material_code LIKE \"%$fSearch%\"
)" : null;

$res = $model->query("
	SELECT DISTINCT
		REPLACE(LOWER(mps_ordersheet_item_purchase_order_number), ' ', '') AS id,
		mps_ordersheet_item_purchase_order_number AS pon
	FROM mps_ordersheet_items
	INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
	INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
	WHERE mps_ordersheet_workflowstate_id IN (9,10,11,12,13,14,15) 
	AND mps_ordersheet_item_quantity_approved > 0
	$filter
")->fetchAll();

$_ORDERSHEET_ITEMS = _array::datagrid($res);


/**
 * Exchange references
 **************************************************************************************************/

$_EXCHANGE_DATA = array();

$division = $_DIVISIONS[$application];

$filter =  $fSearch ? " AND mps_salesorder_ponumber LIKE \"%$fSearch%\"" : null;

$res = $exchange->query("
	SELECT DISTINCT
		REPLACE(LOWER(mps_salesorder_ponumber), ' ', '') AS id,
		ramco_division AS division
	FROM ramco_orders
	WHERE (
		mps_order_completely_imported = 0 
		OR mps_order_completely_imported = '' 
		OR mps_order_completely_imported IS NULL
	) 
	AND TRIM(ramco_division) = $division
	$filter
")->fetchAll();


$_EXCHANGE_DATA = _array::datagrid($res); 


/**
 * Get confirmed orphans
 **************************************************************************************************/

$_DATAGRID = array();

$filter =  $fSearch ? " WHERE (
	mps_salesorder_ponumber LIKE \"%$fSearch%\"
	OR mps_salesorderitem_material_code LIKE \"%$fSearch%\"
)" : null;

$result = $exchange->query("
	SELECT DISTINCT
		REPLACE(LOWER(mps_salesorder_ponumber), ' ', '') AS id,
		mps_salesorder_ponumber AS pon,
		mps_salesorder_customernumber AS customer,
		ramco_shipto_number AS shipto,
		ramco_sold_to_sapnr AS sap,
		date_created
	FROM ramco_confirmed_items
	$filter
	ORDER BY mps_salesorder_ponumber
")->fetchAll();

if ($result) {
	
	foreach ($result as $row) {

		$id = $row['id'];
		$pon = $_ORDERSHEET_ITEMS[$id]['pon'];
		$division = $_EXCHANGE_DATA[$id]['division'];

		if (!$pon && $division) {
			$_DATAGRID[$id]['pon'] = $row['pon'];
			$_DATAGRID[$id]['customer'] = $row['customer'];
			$_DATAGRID[$id]['shipto'] = $row['shipto'];
			$_DATAGRID[$id]['sap'] = $row['sap'];
			$_DATAGRID[$id]['date_created'] = $row['date_created'];
			$_DATAGRID[$id]['source'] = 'Confirmed Items';
		}
	}
}


/**
 * Get shipped orphans
 **************************************************************************************************/

$filter =  $fSearch ? " WHERE (
	mps_salesorderitem_ponumber LIKE \"%$fSearch%\"
	OR mps_salesorderitem_material_code LIKE \"%$fSearch%\"
)" : null;

$result = $exchange->query("
	SELECT DISTINCT
		REPLACE(LOWER(mps_salesorderitem_ponumber), ' ', '') AS id,
		mps_salesorderitem_ponumber AS pon,
		mps_salesorder_customernumber AS customer,
		ramco_shipto_number AS shipto,
		ramco_sold_to_sapnr AS sap,
		date_created
	FROM ramco_shipped_items
	$filter
	ORDER BY mps_salesorderitem_ponumber
")->fetchAll();

if ($result) {
	
	foreach ($result as $row) {

		$id = $row['id'];
		$pon = $_ORDERSHEET_ITEMS[$id]['pon'];
		$division = $_EXCHANGE_DATA[$id]['division'];

		if (!$pon && $division) {
			$_DATAGRID[$id]['pon'] = $row['pon'];
			$_DATAGRID[$id]['customer'] = $row['customer'];
			$_DATAGRID[$id]['shipto'] = $row['shipto'];
			$_DATAGRID[$id]['sap'] = $row['sap'];
			$_DATAGRID[$id]['date_created'] = $row['date_created'];
			$_DATAGRID[$id]['source'] = 'Shipped Items';
		}
	}
}

/**
 * Response
 **************************************************************************************************/

if ($_DATAGRID) {
	ksort($_DATAGRID);
}

$table = new Table();

$table->datagrid = $_DATAGRID;

$table->pon(
	Table::ATTRIBUTE_NOWRAP,
	'width=200px',
	'caption=Purchase Order Number',
	'href='.$_REQUEST['data']
);

$table->customer(
	Table::ATTRIBUTE_NOWRAP,
	'width=10%',
	'caption=Customer Number'
);

$table->shipto(
	Table::ATTRIBUTE_NOWRAP,
	'width=10%',
	'caption=Shipto Number'
);

$table->sap(
	Table::ATTRIBUTE_NOWRAP,
	'width=10%',
	'caption=Soldto Number'
);

$table->date_created(
	Table::ATTRIBUTE_NOWRAP,
	'width=10%',
	'caption=Data Created on'
);

$table->source(
	Table::ATTRIBUTE_NOWRAP,
	'width=10%',
	'caption=Data Source'
);

$table = $table->render();

echo "<div class='table-toolbox'><form class=toolbox method=post>".ui::searchbox()."</form></div>";
echo $table;
