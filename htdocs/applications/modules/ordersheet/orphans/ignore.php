<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$pon = str_replace(' ', '', strtolower($_REQUEST['pon']));
$application = $_REQUEST['application'];

if ($pon) {

	$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);

	// set orders as importet
	$exchange->db->exec("
		DELETE FROM ramco_orders
		WHERE REPLACE(LOWER(mps_salesorder_ponumber), ' ', '') = '$pon'
	");

	// set order confirmed as importet
	$exchange->db->exec("
		DELETE FROM ramco_confirmed_items
		WHERE REPLACE(LOWER(mps_salesorder_ponumber), ' ', '') = '$pon'
	");

	// set order shipped as importet
	$exchange->db->exec("
		DELETE FROM ramco_shipped_items
		WHERE REPLACE(LOWER(mps_salesorderitem_ponumber), ' ', '') = '$pon'
	");

	$json['response'] = true;
	$json['redirect'] = "/$application/exchangeorphans";
	$json['message'] = "Ramco Items from order $pon successfully ignored";
	Message::success("Ramco Items successfully ignored");

} else {
	$json['response'] = false;
	$json['message'] = "Order Sheet not found.";
}

header('Content-Type: text/json');
echo json_encode($json);
