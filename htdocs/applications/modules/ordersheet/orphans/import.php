<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$id = trim($_REQUEST['id']);
$pon = trim($_REQUEST['pon']);
$ordersheet = $_REQUEST['ordersheet'];
$application = $_REQUEST['application'];

define('DEBUGGING_MODE', $_REQUEST['debug'] ?: false);

$_EXCHANGE_ROUTINE = $_SERVER['DOCUMENT_ROOT']."/cronjobs/exchange.ordersheet.items.php";

$_CONSOLE = array();
$_MESSAGE = array();

// db models
$model = new Model($application);
$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);

// check order sheet
if (!$ordersheet) {
	$_MESSAGE[] = "Bad request.";
	$_CONSOLE[] = "Order Sheet not found.";
	goto BLOCK_RESPONSE;
}

// check import routine
if (!file_exists($_EXCHANGE_ROUTINE )) {
	$_MESSAGE[] = "Import failur. Please contact customer services.";
	$_CONSOLE[] = "Exchange routine file not found. Process cancelled.";
	goto BLOCK_RESPONSE;
}

/**
 * Get confirmed orphans
 **************************************************************************************************/

$_RAMCO_ITEMS = array();

$result = $exchange->query("
	SELECT REPLACE(LOWER(mps_salesorderitem_material_code), ' ', '') AS item
	FROM ramco_confirmed_items
	WHERE REPLACE(LOWER(mps_salesorder_ponumber), ' ', '') = '$id'
")->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$_RAMCO_ITEMS[] = $row['item'];
	}
} 
else $_CONSOLE[] = "RAMCO confirmed items not found.";


/**
 * Get shipped orphans
 **************************************************************************************************/

$result = $exchange->query("
	SELECT REPLACE(LOWER(mps_salesorderitem_material_code), ' ', '') AS item
	FROM ramco_shipped_items
	WHERE REPLACE(LOWER(mps_salesorderitem_ponumber), ' ', '') = '$id'
")->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$_RAMCO_ITEMS[] = $row['item'];
	}
} 
else $_CONSOLE[] = "RAMCO confirmed items not found.";

if (!$_RAMCO_ITEMS) {
	$_MESSAGE[] = "RAMCO items not found.";
	$_CONSOLE[] = "RAMCO confirmed/shipped items not found. Process cancelled.";
	goto BLOCK_RESPONSE;
}


/**
 * Get references
 **************************************************************************************************/

$_RAMCO_ITEMS = array_unique($_RAMCO_ITEMS);
$materials = "'".join("' , '", $_RAMCO_ITEMS)."'";

$OrderSheetItems = $model->query("
	SELECT 
		mps_ordersheet_item_id AS id,
		mps_ordersheet_item_purchase_order_number AS pon
	FROM mps_ordersheet_items
	INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
	WHERE mps_ordersheet_item_ordersheet_id = $ordersheet 
	AND REPLACE(LOWER(mps_material_code), ' ', '') IN ($materials)
")->fetchAll();

if (!$OrderSheetItems) {
	$_MESSAGE[] = "Order sheet items not found.";
	$_CONSOLE[] = "Order sheet items not found. Process cancelled.";
	goto BLOCK_RESPONSE;
}


/**
 * Replace order sheet item PON
 **************************************************************************************************/

$updatedOrderSheetItems = array();

$sth = $model->db->prepare("
	UPDATE mps_ordersheet_items SET
		mps_ordersheet_item_purchase_order_number = ?,
		user_modified = ?,
		date_modified = NOW()
	WHERE mps_ordersheet_item_id = ?
");

foreach ($OrderSheetItems as $row) {

	$item = $row['id'];
	
	$response = DEBUGGING_MODE ? true : $sth->execute(array($pon, $user->login, $item ));

	if ($response) {
		$updatedOrderSheetItems[] = $row['pon'];
		$_CONSOLE[] = "DEBUGGER: update item $item";
	} else {
		$_CONSOLE[] = "ERROR: update item $item";
	}
}

$_RESPONSE = count($OrderSheetItems) == count($updatedOrderSheetItems) ? true : false;

// rollback
if (!$_RESPONSE) {
	
	if (!DEBUGGING_MODE) {
		foreach ($OrderSheetItems as $row) {
			$sth->execute(array($row['pon'], $user->login, $row['id'] ));
		}
	}

	$_MESSAGE[] = "Import failur. Please contact customer services.";
	$_CONSOLE[] = "Can not update all order sheet items. Process cancelled.";
	goto BLOCK_RESPONSE;
}


/**
 * Update sales orders
 **************************************************************************************************/

$updatedOrderSheetItems = array_unique($updatedOrderSheetItems);

foreach ($updatedOrderSheetItems as $p) {

	if (DEBUGGING_MODE) {
		$_CONSOLE[] = "Replace sales order $p with $pon";
	} else {
	
		$exchange->db->exec("
			UPDATE mps_salesorders SET
				mps_salesorder_ponumber = '$pon'
			WHERE mps_salesorder_ponumber = '$p'
		");

		$exchange->db->exec("
			UPDATE mps_salesorderitems SET
				mps_salesorderitem_ponumber = '$pon'
			WHERE mps_salesorderitem_ponumber = '$p'
		");
	}
}


/**
 * Start exchange import routine
 **************************************************************************************************/

// start bufering
ob_start();

$_REQUEST['application'] = $application;
$_REQUEST['pon'] = $id;
$_REQUEST['orphans'] = true;
$_REQUEST['debug'] = DEBUGGING_MODE;

require $_EXCHANGE_ROUTINE;
$console = ob_get_contents();

// end buffering
ob_end_clean();

$_IMPORT_CONSOLE = $console ? json_decode($console, true) : array();


/**
 * RESPONSE
 **************************************************************************************************/

BLOCK_RESPONSE:

// session message
if ($_RESPONSE && !DEBUGGING_MODE) {
	Message::success("Ramco Items successfully imported");
	$_MESSAGE[] = "Ramco Items successfully imported";
} 

if (!$_RESPONSE) {
	$_MESSAGE[] = "Error on import";
}

header('Content-Type: text/json');

echo json_encode(array(
	'response' => $_RESPONSE,
	'message' => $_MESSAGE ? join($_MESSAGE) : null,
	'redirect' => $_RESPONSE && !DEBUGGING_MODE ? "/$application/exchangeorphans" : null,
	'console' => $_CONSOLE,
	'import' => $_IMPORT_CONSOLE
));
