<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$id = $_REQUEST['ordersheet'];
$application = $_REQUEST['application'];
$clientQuantities = $_REQUEST['mps_ordersheet_item_quantity'];

$_STATE_COMPLETED = 3;
$_SUBMITTED_STATES = array(1,2);

// order sheet
$ordersheet = new Ordersheet($application);
$ordersheet->read($id);

if ($ordersheet->id) {

	// state handler
	$state = new State($ordersheet->workflowstate_id, $application); 
	$state->setOwner($ordersheet->address_id);
	$state->setDateIntervals($ordersheet->openingdate, $ordersheet->closingdate);

	// db model
	$model = new Model($application);

	// workflow state
	$workflowState = new Workflow_State($application);
	
	// tracker
	$tracker = new User_Tracking($application);

	$response = true;

	// get order sheet items
	$sth = $model->db->prepare("
		SELECT 
			mps_ordersheet_item_id,
			mps_ordersheet_item_quantity,
			mps_ordersheet_item_quantity_approved,
			mps_ordersheet_item_quantity_proposed
		FROM mps_ordersheet_items
		WHERE mps_ordersheet_item_ordersheet_id = ?
	");

	$sth->execute(array($ordersheet->id));
	$items = $sth->fetchAll();

	if ($items) { 

		// add stock reserve warehouse if not exist
		if (in_array($state->state, $_SUBMITTED_STATES)) {
			$ordersheet->warehouse()->addStockReserve();
		}

		// order sheet items
		foreach ($items as $item) {

			$data = array();

			$key = $item['mps_ordersheet_item_id'];
			$ordersheet->item()->read($key);

			// item quantity
			$quantity = isset($clientQuantities[$key]) ? $clientQuantities[$key] : $item['mps_ordersheet_item_quantity'];
			$data['mps_ordersheet_item_quantity'] = $quantity;

			// owner quantity
			if ($clientQuantities[$key]) {
					$totalPlanned = $ordersheet->item()->planned()->sumQuantities();
					$stockReserve = $clientQuantities[$key] - $totalPlanned;
					$stockReserve = ($stockReserve > 0) ? $stockReserve : null;
					$ordersheet->item()->planned()->setStockReserve($stockReserve);
			} else $ordersheet->item()->planned()->setStockReserve(null);
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');

			// update order sheet items
			$ordersheet->item()->update($data);
		}

		// update order sheet workflow state
		$sth = $model->db->prepare("
			UPDATE mps_ordersheets SET 
				mps_ordersheet_workflowstate_id = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE mps_ordersheet_id = ?
		");

		$response = $sth->execute(array($_STATE_COMPLETED, $user->login, $ordersheet->id));

		if ($response) {
			
			$reload = true;
			Message::success("The Order Sheet is completed.");

			$workflowState = new Modul($application);
			$workflowState->setTable('mps_workflow_states');
			$workflowState->read($_STATE_COMPLETED);
				
			$track = new User_Tracking($application);
			$track->create(array(
				'user_tracking_entity' => 'order sheet',
				'user_tracking_entity_id' => $ordersheet->id,
				'user_tracking_user_id' => $user->id,
				'user_tracking_action' => $workflowState->data['mps_workflow_state_name'],
				'user_created' => $user->login,
				'date_created' => date('Y-m-d H:i:s')
			));

		} else {
			$message = "The Order Sheet can not revise.";
		}

	} else {
		$sendmail = false;
		$message = "The Order Sheet has no items.";
	}

} else {
	$response = false;
	$message = "The Order Sheet not found.";
}


header('Content-Type: text/json');
echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'reload' => $reload
));
