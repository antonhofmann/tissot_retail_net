<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$id = $_REQUEST['id'];
$application = $_REQUEST['application'];
$section = $_REQUEST['section'];

$model = new Model($application);

switch ($section) {
	
	case 'exchange.import.items':

		$json = $model->query("
			SELECT 
				mps_ordersheet_item_id AS id,
				mps_material_code AS code,
				mps_ordersheet_item_quantity AS quantity,
				mps_ordersheet_item_quantity_approved AS approved,
				mps_ordersheet_item_purchase_order_number AS pon,
				mps_ordersheet_item_customernumber AS customer,
				mps_ordersheet_item_shipto AS shipto
			FROM mps_ordersheet_items
			INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
			WHERE mps_ordersheet_item_ordersheet_id = $id AND mps_ordersheet_item_quantity_approved > 0
			ORDER BY mps_material_code
		")->fetchAll();
	
	break;

	case 'ordersheet':

		$json = $model->query("
			SELECT 
				mps_ordersheet_id, 
				DATE_FORMAT(mps_ordersheet_openingdate, '%d.%m.%Y') AS mps_ordersheet_openingdate, 
				DATE_FORMAT(mps_ordersheet_closingdate, '%d.%m.%Y') AS mps_ordersheet_closingdate, 
				mps_ordersheet_comment, 
				mps_mastersheet_name, 
				mps_mastersheet_year, 
				mps_workflow_state_name,
				db_retailnet.addresses.address_company, 
				db_retailnet.countries.country_name
			FROM mps_ordersheets INNER JOIN db_retailnet.addresses ON mps_ordersheets.mps_ordersheet_address_id = db_retailnet.addresses.address_id
				 INNER JOIN db_retailnet.countries ON db_retailnet.addresses.address_country = db_retailnet.countries.country_id
				 INNER JOIN mps_mastersheets ON mps_ordersheets.mps_ordersheet_mastersheet_id = mps_mastersheets.mps_mastersheet_id
				 INNER JOIN mps_workflow_states ON mps_workflow_states.mps_workflow_state_id = mps_ordersheets.mps_ordersheet_workflowstate_id
			WHERE mps_ordersheet_id = $id
		")->fetch();

		if ($json['mps_ordersheet_id']) {
			$protocol = Settings::init()->http_protocol.'://';
			$server = $_SERVER['SERVER_NAME'];
			$json['link'] = $protocol.$server."/$application/ordersheets/data/$id";
		}

	break;
}

header('Content-Type: text/json');
echo json_encode($json);