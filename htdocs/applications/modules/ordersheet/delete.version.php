<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

// request vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

// db application model
$model = new Model($application);

// application dependency
switch ($application) {
			
	case 'mps':
	

		$tableName = 'mps_ordersheet_versions';
		$fieldOrdersheet = 'mps_ordersheet_version_ordersheet_id';

		$query = "
			DELETE FROM mps_ordersheet_version_items
			WHERE mps_ordersheet_version_item_ordersheetversion_id IN (?)
		";

	break;

	case 'lps':
	
		
		$tableName = 'lps_launchplan_versions';
		$fieldOrdersheet = 'lps_launchplan_version_launchplan_id';

		$query = "
			DELETE FROM lps_launchplan_version_items
			WHERE lps_launchplan_version_item_version_id IN (?)
		";

	break;
}

// ordersheet
$version = new Modul($application);
$version->setTable($tableName);
$version->read($id);

if ($version->id) {

	$ordersheet = $version->data[$fieldOrdersheet];

	$response = $version->delete();

	if ($response) { 

		$sth = $model->db->prepare($query);
		$sth->execute(array($id));

		Message::request_deleted();
		url::redirect("/$application/$controller/$action/$ordersheet");

	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$ordersheet/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}

