<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

define('DEBUGGING_MODE', false);

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'];
$_ORDERSHEET = $_REQUEST['ordersheet'];
$_ACTION = $_REQUEST['action'];
$_ITEM = $_REQUEST['item'];
$_ID = $_REQUEST['id'];

$_JSON = array();
$_ERRORS = array();
$_SUCCESSES = array();
$_CONSOLE = array();

$model = new Model($_APPLICATION);
$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);

// order sheet	
$ordersheet = new Ordersheet($_APPLICATION);
$ordersheet->read($_ORDERSHEET);

// company
$company = new Company();
$company->read($ordersheet->address_id);

// state handler
$state = new State($ordersheet->workflowstate_id, $_APPLICATION); 
$state->setOwner($ordersheet->address_id);
$state->setDateIntervals($ordersheet->openingdate, $ordersheet->closingdate);


$_CAN_EDIT = $state->canEdit(false);
$_EXPORT_SAP = $company->do_data_export_from_mps_to_sap;
$_MODE_DISTRIBUTION = $state->onDistribution() || $state->isDistributed() ? true : false;

$_DIVISION = '08';

// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$ordersheet->id) {
	$_ERRORS[] = "The order sheet not found.";
}

if (!$_ERRORS && !$_CAN_EDIT) {
	$_ERRORS[] = "You don't have permissions to submit this action.";
	goto BLOCK_RESPONDING;
}

if (!$_ERRORS && !$_EXPORT_SAP) {
	$_ERRORS[] = "The Company $company->company cannot export orders to SAP.";
	goto BLOCK_RESPONDING;
}

if (!$_ERRORS && !$_MODE_DISTRIBUTION) {
	$_ERRORS[] = "Reset exported orders is permitted only in distributin status.";
	goto BLOCK_RESPONDING;
}

if (!$_ITEM) {
	$_ERRORS[] = "Order sheet item is not defined.";
	goto BLOCK_RESPONDING;
}

if (!$_ACTION) {
	$_ERRORS[] = "Submit action is not defined.";
	goto BLOCK_RESPONDING;
}

if ($_ACTION!='all' && !$_ID) {
	$_ERRORS[] = "Action ID is not defined.";
	goto BLOCK_RESPONDING;
}


// order sheet item ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $model->db->prepare("
	SELECT * 
	FROM mps_ordersheet_items
	WHERE mps_ordersheet_item_id = ?
");

$sth->execute(array($_ITEM));
$_ITEM = $sth->fetch();


// get distributions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_DISTRIBUTED_ITEMS = array();

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT 
			mps_ordersheet_item_delivered_id AS id,
			mps_ordersheet_item_delivered_ordersheet_item_id AS item,
			mps_ordersheet_item_delivered_posaddress_id AS pos,
			mps_ordersheet_item_delivered_quantity AS quantity,
			DATE_FORMAT(mps_ordersheet_item_delivered_confirmed, '%d.%m.%y') AS date,
			UNIX_TIMESTAMP(mps_ordersheet_item_delivered_confirmed) AS confirmed_timestamp,
			CONCAT(posaddress_name, ', ', place_name) As posName
		FROM mps_ordersheet_item_delivered
		INNER JOIN db_retailnet.posaddresses ON posaddress_id = mps_ordersheet_item_delivered_posaddress_id
		INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
		WHERE mps_ordersheet_item_delivered_ordersheet_item_id = ? 
		AND mps_ordersheet_item_delivered_confirmed IS NOT NULL
		ORDER BY posName
	");

	$sth->execute(array($_ITEM['mps_ordersheet_item_id']));
	$result = $sth->fetchAll();

	if ($result) {
		$_DISTRIBUTED_ITEMS = _array::datagrid($result);
	} else {
		$_ERRORS[] = "Distributed items not found";
		goto BLOCK_RESPONDING;
	}
}


// get sales orders ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_SAP_SALES_ORDERS = array();

if (!$_ERRORS && $_DISTRIBUTED_ITEMS) {

	$invlvedItems = join(',', array_keys($_DISTRIBUTED_ITEMS));

	$result = $exchange->query("
		SELECT 
			sap_salesorder_item_id AS id,
			sap_salesorder_item_mps_item_id AS item,
			sap_salesorder_item_mps_po_number AS pon,
			sap_salesorder_item_ean_number AS ean,
			sap_salesorder_itemr_quantity AS quantity
		FROM sap_salesorder_items
		WHERE sap_salesorder_item_mps_item_id IN ($invlvedItems) 
		AND sap_salesorder_item_division = '$_DIVISION'
	")->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$pon = $row['pon'];
			$ean = $row['ean'];
			$_SAP_SALES_ORDERS[$pon][$ean]['id'] = $row['id'];
			$_SAP_SALES_ORDERS[$pon][$ean]['item'] = $row['item'];
			$_SAP_SALES_ORDERS[$pon][$ean]['quantity'] = $row['quantity'];
		}
	} else {
		$_ERRORS[] = "Sales orders items not found";
		goto BLOCK_RESPONDING;
	}
}


// get sap confiremd items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_SAP_FAILURE_ITEMS = array();
$_SAP_CONFIRMATIONS = array();

if (!$_ERRORS && $_SAP_SALES_ORDERS) {

	$pons = "'".join("', '", array_keys($_SAP_SALES_ORDERS))."'";

	$result = $exchange->query("
		SELECT 
			sap_confirmed_item_id AS id,
			sap_confirmed_item_mps_po_number AS pon,
			sap_confirmed_item_ean_number AS ean,
			sap_confirmed_item_confirmed_quantity AS quantity
		FROM sap_confirmed_items
		WHERE sap_confirmed_item_mps_po_number IN ($pons) 
		AND sap_confirmed_item_division = '$_DIVISION'
		AND sap_confirmed_item_imported_in_mps IS NOT NULL
	")->fetchAll();

	if ($result) {

		foreach ($result as $row) {
			
			$pon = $row['pon'];
			$ean = $row['ean'];

			// order
			$order = $_SAP_SALES_ORDERS[$pon][$ean] ?: array();
			$item = $order['item'];

			$confirmedQuantity = $row['quantity'];
			$salesOrderQuantity = $order['quantity'];

			// sap failure quantities
			if ($order && $confirmedQuantity < $salesOrderQuantity)  {
				$_SAP_FAILURE_ITEMS[$item]['salesorder_id'] = $_SAP_SALES_ORDERS[$pon][$ean]['id'];
				$_SAP_FAILURE_ITEMS[$item]['confirmation_id'] = $row['id'];
				$_SAP_FAILURE_ITEMS[$item]['quantity'] = $salesOrderQuantity;
				$_SAP_FAILURE_ITEMS[$item]['confirmed'] = $confirmedQuantity;
				$_SAP_FAILURE_ITEMS[$item]['pos'] = $_DISTRIBUTED_ITEMS[$item]['pos'];
				$_SAP_FAILURE_ITEMS[$item]['confirmed_timestamp'] = $_DISTRIBUTED_ITEMS[$item]['confirmed_timestamp'];
			}

			$_SAP_CONFIRMATIONS[$id] = $row;
		}
	}

	if (!$_SAP_FAILURE_ITEMS) {
		$_ERRORS[] = "SAP order failures not found";
		goto BLOCK_RESPONDING;
	}
}


// sql statemenets :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$resetSelesOrder = $exchange->db->prepare("
	UPDATE sap_salesorder_items SET 
		sap_salesorder_itemr_quantity = ?
	WHERE sap_salesorder_item_id = ?
"); 

$deleteSelesOrder = $exchange->db->prepare("
	DELETE FROM sap_salesorder_items
	WHERE sap_salesorder_item_id = ?
"); 

$resetDistribution = $model->db->prepare("
	UPDATE mps_ordersheet_item_delivered SET
		mps_ordersheet_item_delivered_quantity = ?,
		user_modified = ?,
		date_modified = NOW()
	WHERE mps_ordersheet_item_delivered_id = ?
"); 

$deleteDistribution = $model->db->prepare("
	DELETE FROM mps_ordersheet_item_delivered
	WHERE mps_ordersheet_item_delivered_id = ?
"); 

$resetItem = $model->db->prepare("
	UPDATE mps_ordersheet_items SET
		mps_ordersheet_item_quantity_distributed = ?,
		user_modified = ?,
		date_modified = NOW()
	WHERE mps_ordersheet_item_id = ?
"); 

$track = $model->db->prepare("
	INSERT INTO user_tracks ( 
		user_track_entity,
		user_track_entity_id,
		user_track_user_id,
		user_track_type_id,
		user_track_message
	)
	VALUES (?,?,?,?,?)
");


// reset statement :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


function resetItem($id) {

	global $resetSelesOrder, $deleteSelesOrder, $resetDistribution, $deleteDistribution, $resetItem, $track;
	global $_ITEM, $_SAP_FAILURE_ITEMS, $_DISTRIBUTED_ITEMS;
	global $_CONSOLE;

	// identificators
	$idDistribution = $id;
	$idSalesOrder = $_SAP_FAILURE_ITEMS[$id]['salesorder_id'];
	$idConfirmation = $_SAP_FAILURE_ITEMS[$id]['confirmation_id'];

	// quantities
	$qtyDistributed = $_DISTRIBUTED_ITEMS[$id]['quantity'];
	$qtySelsOrder = $_SAP_FAILURE_ITEMS[$id]['quantity'];
	$qtyConfirmed = $_SAP_FAILURE_ITEMS[$id]['confirmed'];
	$qtyReset = $qtySelsOrder - $qtyConfirmed;

	if (DEBUGGING_MODE) {
		$actionName = $qtyConfirmed>0 ? 'reset' : 'delete';
		$_CONSOLE[] = "DEBUGGING MODE: $actionName sales order quantity $idSalesOrder whith quantity $qtyConfirmed";
		$_CONSOLE[] = "DEBUGGING MODE: $actionName distribution quantity $idDistribution with quantity $qtyConfirmed";
	} else {

		if ($qtyConfirmed > 0) {

			// reset sales order item
			$resetSelesOrder->execute(array($qtyConfirmed, $idSalesOrder));
			$track->execute(array('sap_salesorder_items', $idSalesOrder, $user->id, 1, "old value $qtySelsOrder, new value $qtyConfirmed"));
			$_CONSOLE[] = "Reset sales order quantity $idSalesOrder whith quantity $qtyConfirmed";

			// reset distribution item
			$resetDistribution->execute(array($qtyConfirmed, $user->login, $idDistribution));
			$track->execute(array('mps_ordersheet_item_delivered', $idDistribution, $user->id, 1, "old value $qtyDistributed, new value $qtyConfirmed"));
			$_CONSOLE[] = "Reset distribution quantity $idDistribution with quantity $qtyConfirmed";

		} else {

			// delete sales order item
			$deleteSelesOrder->execute(array($idSalesOrder));
			$track->execute(array('sap_salesorder_items', $idSalesOrder, $user->id, 2, "Delete from SAP reset quantity"));
			$_CONSOLE[] = "Delete sales order quantity $idSalesOrder";

			// delete distribution
			$deleteDistribution->execute(array($idDistribution));
			$track->execute(array('mps_ordersheet_item_delivered', $idDistribution, $user->id, 1, "delete from SAP reset quantity"));
			$_CONSOLE[] = "Delete distribution quantity $idDistribution";
		}
	}

	return $qtyReset;
}


// reset distribution quantity :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && $_ACTION=='quantity') {
	
	if ($_SAP_FAILURE_ITEMS[$_ID]) {
		
		$resetQuantity = resetItem($_ID);
		$distributedQuantity = $_ITEM['mps_ordersheet_item_quantity_distributed'] - $resetQuantity;

		// reset order sheet item
		if (!DEBUGGING_MODE) {
			$resetItem->execute(array($distributedQuantity, $user->login, $_ITEM['mps_ordersheet_item_id']));
			$track->execute(array('mps_ordersheet_items', $_ITEM['mps_ordersheet_item_id'], $user->id, 1, "Reset distributed quantities"));
			$_CONSOLE[] = "Reset distribution quantity $_ID with quantity $resetQuantity";
		}

		$_JSON['success'] = true;

		$_CONSOLE[] = "Reset order sheet item {$_ITEM[mps_ordersheet_item_id]}: old value {$_ITEM[mps_ordersheet_item_quantity_distributed]}, new value $distributedQuantity";
	}
}


// reset all pos distributions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && $_ACTION=='pos') {

	$totalResetQuantity = 0;

	foreach ($_SAP_FAILURE_ITEMS as $id => $failure) {
		
		if ($_ID==$failure['pos']) {
			$resetQuantity = resetItem($id);
			$totalResetQuantity = $totalResetQuantity + $resetQuantity;
		}
	}

	$distributedQuantity = $_ITEM['mps_ordersheet_item_quantity_distributed'] - $totalResetQuantity;

	// reset order sheet item
	if (!DEBUGGING_MODE) {
		$resetItem->execute(array($distributedQuantity, $user->login, $_ITEM['mps_ordersheet_item_id']));
		$track->execute(array('mps_ordersheet_items', $_ITEM['mps_ordersheet_item_id'], $user->id, 1, "Reset distributed quantities"));
	}

	$_JSON['success'] = true;
	
	$_CONSOLE[] = "Reset order sheet item {$_ITEM[mps_ordersheet_item_id]}: old value {$_ITEM[mps_ordersheet_item_quantity_distributed]}, new value $distributedQuantity";
}


// reset all item distributions ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && $_ACTION=='item') {

	$totalResetQuantity = 0;

	foreach ($_SAP_FAILURE_ITEMS as $id => $failure) {
		
		if ($_ID==$failure['confirmed_timestamp']) {
			$resetQuantity = resetItem($id);
			$totalResetQuantity = $totalResetQuantity + $resetQuantity;
		}
	}

	$distributedQuantity = $_ITEM['mps_ordersheet_item_quantity_distributed'] - $totalResetQuantity;

	// reset order sheet item
	if (!DEBUGGING_MODE) {
		$resetItem->execute(array($distributedQuantity, $user->login, $_ITEM['mps_ordersheet_item_id']));
		$track->execute(array('mps_ordersheet_items', $_ITEM['mps_ordersheet_item_id'], $user->id, 1, "Reset distributed quantities"));
	}

	$_JSON['success'] = true;
	
	$_CONSOLE[] = "Reset order sheet item {$_ITEM[mps_ordersheet_item_id]}: old value {$_ITEM[mps_ordersheet_item_quantity_distributed]}, new value $distributedQuantity";
}


// reset all distributions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && $_ACTION=='all') {

	$totalResetQuantity = 0;

	foreach ($_SAP_FAILURE_ITEMS as $id => $failure) {
		$resetQuantity = resetItem($id);
		$totalResetQuantity = $totalResetQuantity + $resetQuantity;
	}

	$distributedQuantity = $_ITEM['mps_ordersheet_item_quantity_distributed'] - $totalResetQuantity;

	// reset order sheet item
	if (!DEBUGGING_MODE) {
		$resetItem->execute(array($distributedQuantity, $user->login, $_ITEM['mps_ordersheet_item_id']));
		$track->execute(array('mps_ordersheet_items', $_ITEM['mps_ordersheet_item_id'], $user->id, 1, "Reset distributed quantities"));
	}

	$_JSON['success'] = true;
	$_JSON['reload'] = true;
	
	$_CONSOLE[] = "Reset order sheet item {$_ITEM[mps_ordersheet_item_id]}: old value {$_ITEM[mps_ordersheet_item_quantity_distributed]}, new value $distributedQuantity";
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONDING:

if (DEBUGGING_MODE) {
	$_JSON['distributions'] = $_DISTRIBUTED_ITEMS;
	$_JSON['orders'] = $_SAP_SALES_ORDERS;
	$_JSON['confirmations'] = $_SAP_CONFIRMATIONS;
	$_JSON['failures'] = $_SAP_FAILURE_ITEMS;
}

if ($_ERRORS) {
	$_JSON['errors'] = $_ERRORS;
}

if ($_SUCCESSES) {
	$_JSON['notifications'] = $_SUCCESSES;
}

$_JSON['console'] = $_CONSOLE;

header('Content-Type: text/json');
echo json_encode($_JSON);


