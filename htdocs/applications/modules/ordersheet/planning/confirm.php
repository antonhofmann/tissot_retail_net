<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

define(DEBUGGING_MODE, false);

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// request
$_APPLICATION = $_REQUEST['application'];
$_CONTROLLER = $_REQUEST['controller'];
$_ID = $_REQUEST['ordersheet'];

// consoling
$_ERRORS = array();
$_CONSOLE = array();
$_NOTIFICATIONS = array();
$_DATA = array();

// item confirmation permission
$_CAN_CONFIRM_ITEMS = true;

// export to SAP permission
$_CAN_EXPORT_SAP = false;

// db model
$model = new Model($_APPLICATION);

//  user tracker
$tracker = new Tracker($_APPLICATION);


// order sheet :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$ordersheet = new Ordersheet($_APPLICATION);
$ordersheet->read($_ID);

// company
$company = new Company();

if (!$ordersheet->id) {
	$_ERRORS[] = "Order sheet $_ID not found";
	goto BLOCK_RESPONDING;
}

// ordersheet client
$company->read($ordersheet->address_id);

// export data to SAP
$_CAN_EXPORT_SAP = $company->do_data_export_from_mps_to_sap;

if ($_CAN_EXPORT_SAP) $_CONSOLE[] = "Company ($company->id) $company->company can export items in SAP";
else $_CONSOLE[] = "Company $company->company can NOT export items in SAP";



// distributed quantities ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NOT_CONFIRMED_ITEMS = array();
$_TOTAL_ITEM_QUANTITIES = array();
$_SAP_INVOLVED_ITEMS = array();
$_SAP_INVOLVED_POS = array();
$_SAP_INVOLVED_WAREHOUSES = array();

$sth = $model->db->prepare("
	SELECT DISTINCT
		mps_ordersheet_item_delivered_id,
		mps_ordersheet_item_delivered_ordersheet_item_id,
		mps_ordersheet_item_delivered_posaddress_id,
		posaddress_distribution_channel,
		mps_ordersheet_item_delivered_warehouse_id,
		mps_ordersheet_item_delivered_quantity,
		mps_ordersheet_item_delivered_confirmed
	FROM mps_ordersheet_item_delivered
	INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id
	LEFT JOIN db_retailnet.posaddresses ON posaddress_id = mps_ordersheet_item_delivered_posaddress_id
	WHERE mps_ordersheet_item_ordersheet_id = ?
");

$sth->execute(array($ordersheet->id));
$result = $sth->fetchAll();

if (!$result) {
	$_ERRORS[] = "The order sheet hasn't distributed quantities.";
	goto BLOCK_RESPONDING;
}
	
foreach ($result as $row) {

	$key = $row['mps_ordersheet_item_delivered_id'];
	$item = $row['mps_ordersheet_item_delivered_ordersheet_item_id'];
	$pos = $row['mps_ordersheet_item_delivered_posaddress_id'];
	$warehouse = $row['mps_ordersheet_item_delivered_warehouse_id'];
	$quantity = $row['mps_ordersheet_item_delivered_quantity'];

	// sum all distributed quantities
	$_TOTAL_ITEM_QUANTITIES[$item] += $quantity;

	// quantity is not confirmed
	if (!$row['mps_ordersheet_item_delivered_confirmed']) {
			
		// distributed items
		$_NOT_CONFIRMED_ITEMS[$key]['item'] = $item;
		$_NOT_CONFIRMED_ITEMS[$key]['quantity'] = $quantity;
			
		// SAP sales order
		if ($_CAN_EXPORT_SAP) {
			
			$_SAP_INVOLVED_ITEMS[$item] = true;
			
			if ($pos) {
				$_NOT_CONFIRMED_ITEMS[$key]['pos'] = $pos;
				$_NOT_CONFIRMED_ITEMS[$key]['distChannel'] = $row['posaddress_distribution_channel'];
				$_SAP_INVOLVED_POS[$pos] = true;
			}
			
			if ($warehouse) {
				$_NOT_CONFIRMED_ITEMS[$key]['warehouse'] = $warehouse;
				$_SAP_INVOLVED_WAREHOUSES[$warehouse] = true;
			}
		}

	}
}

if (!$_NOT_CONFIRMED_ITEMS) {
	$_NOTIFICATIONS[] = "Unconfirmed order sheet items not found.";
	goto BLOCK_RESPONDING;
}

$_DATA['not.confirmed.items'] = $_NOT_CONFIRMED_ITEMS;
$_DATA['sap.involved.items'] = $_SAP_INVOLVED_ITEMS;
$_DATA['sap.involved.pos'] = $_SAP_INVOLVED_POS;
$_DATA['sap.involved.warehouses'] = $_SAP_INVOLVED_WAREHOUSES;



// sap mps mandatory data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_MATERIALS = array();	
$_ORDERSHEET_ITEM_SHIPPED = array();

if (!$_ERRORS && $_CAN_EXPORT_SAP && $_SAP_INVOLVED_ITEMS) {
		
	// involved order sheet items in sap export
	$keys = join(',',array_keys($_SAP_INVOLVED_ITEMS));
			
	// order sheet items shipped
	$result = $model->query("
		SELECT DISTINCT 
			mps_ordersheet_item_shipped_ordersheet_item_id AS item,
			mps_ordersheet_item_shipped_po_number AS pon,
			mps_ordersheet_item_shipped_line_number AS line
		FROM mps_ordersheet_item_shipped
		WHERE mps_ordersheet_item_shipped_ordersheet_item_id IN ($keys)		
		ORDER BY mps_ordersheet_item_shipped_date DESC
	")->fetchAll();
			
	$_ORDERSHEET_ITEM_SHIPPED = _array::datagrid($result);
	$_DATA['ordersheet.item.shipped'] = $_ORDERSHEET_ITEM_SHIPPED;		

	// materials
	$result = $model->query("
		SELECT DISTINCT
			mps_ordersheet_item_id AS id,
			mps_material_ean_number AS ean,
			mps_material_material_planning_type_id AS planning
		FROM mps_ordersheet_items
		INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
		WHERE mps_ordersheet_item_id IN ($keys)		
	")->fetchAll();
			
	if ($result) {
		foreach ($result as $row) {
			$key = $row['id'];
			$_MATERIALS[$key]['ean'] = $row['ean'];	
			$_MATERIALS[$key]['planning'] = $row['planning'];	
		}
	}	

	$_DATA['materials'] = $_MATERIALS;	
}


// sap pos mandatory data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_POS = array();

if (!$_ERRORS && $_CAN_EXPORT_SAP && $_SAP_INVOLVED_POS) {
			
	$keys = join(',',array_keys($_SAP_INVOLVED_POS));
	
	$result = $model->query("
		SELECT DISTINCT
			posaddress_id,
			posaddress_sapnumber,
			posaddress_sap_shipto_number,
			address_sap_salesorganization
		FROM db_retailnet.posaddresses
		INNER JOIN db_retailnet.addresses ON address_id = posaddress_client_id
		WHERE posaddress_id IN($keys)
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$key = $row['posaddress_id'];

			$_POS[$key] = array(
				'sap' => $row['posaddress_sapnumber'],
				'shipp' => $row['posaddress_sap_shipto_number'],
				'organization' => $row['address_sap_salesorganization']
			);
		}
	}

	$_DATA['pos'] = $_POS;	
}


// sap warehouse mandatory data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_WAREHOUSES = array();

if (!$_ERRORS && $_CAN_EXPORT_SAP && $_SAP_INVOLVED_WAREHOUSES) {

	$keys = join(',',array_keys($_SAP_INVOLVED_WAREHOUSES));

	$result = $model->query("
		SELECT DISTINCT
			mps_ordersheet_warehouse_id,
			address_warehouse_sap_customer_number,
			address_warehouse_sap_shipto_number,
			address_sap_salesorganization
		FROM db_retailnet.address_warehouses
		INNER JOIN mps_ordersheet_warehouses ON mps_ordersheet_warehouse_address_warehouse_id = address_warehouse_id
		INNER JOIN db_retailnet.addresses ON address_id = address_warehouse_address_id
		WHERE mps_ordersheet_warehouse_id IN ($keys) AND mps_ordersheet_warehouse_stock_reserve <> 1		
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$key = $row['mps_ordersheet_warehouse_id'];

			$_WAREHOUSES[$key] = array(
				'sap' => $row['address_warehouse_sap_customer_number'],
				'shipp' => $row['address_warehouse_sap_shipto_number'],
				'organization' => $row['address_sap_salesorganization']
			);
		}
	}

	$_DATA['warehouses'] = $_WAREHOUSES;	
}


// stock adjustemnt quantities ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ADJUSTED_QUANTITIES = array();

if (!$_ERRORS && $_CAN_EXPORT_SAP) {

	$sth = $model->db->prepare("
		SELECT DISTINCT
			mps_ordersheet_items_adjusted_id AS id,
			mps_ordersheet_items_adjusted_item_id AS item,
			mps_ordersheet_items_adjusted_quantity AS quantitiy,
			mps_ordersheet_items_adjusted_confirmed_quantity AS confirmed
		FROM mps_ordersheet_items_adjusted
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_items_adjusted_item_id
		WHERE mps_ordersheet_item_ordersheet_id = ?
	");

	$sth->execute(array($_ID));
	$result = $sth->fetchAll();

	if ($result) {
		
		foreach ($result as $row) {
			
			$item = $row['item'];

			$_ADJUSTED_QUANTITIES[$item] = array(
				'id' => $row['id'],
				'quantitiy' => $row['quantitiy'],
				'confirmed' => $row['confirmed']
			);
		}
	}

	$_DATA['adjusted.quantities'] = $_ADJUSTED_QUANTITIES;
}



// get sap data :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_DIVISIONS = array();
$_SAP_MATRIX_DATA = array();
$_SAP_PARAMETERS = array();
$_SAP_MATRIX_CHANNELS = array();
$_SAP_MATRIX_ORDERTYPES = array();
$_SAP_MATRIX_ORDERTYPES_WAREHOUSES = array();

if (!$_ERRORS && $_CAN_EXPORT_SAP) {

	// divisions
	$_DIVISIONS = array(
		'mps' => '08'
	);

	// get sap parameters
	$result = $model->query("
		SELECT
			sap_parameter_name AS param, 
			sap_parameter_value AS value
		FROM db_retailnet.sap_parameters
	")->fetchAll();

	$_SAP_PARAMETERS = _array::extract($result);
	$_DATA['sap.parameters'] = $_SAP_PARAMETERS;


	// matrix sap distribution channels
	$sth = $model->db->prepare("
		SELECT 
			sap_matrix_channel_planning_type_id,
			sap_distribution_channel_code
		FROM db_retailnet.sap_matrix_channels
		INNER JOIN db_retailnet.sap_distribution_channels ON sap_distribution_channel_id = sap_matrix_channel_distchannel_id
		WHERE sap_matrix_channel_address_id = ?
	");

	$sth->execute(array($ordersheet->address_id));
	$result = $sth->fetchAll();
	$_SAP_MATRIX_CHANNELS = _array::extract($result);
	$_DATA['sap.matrix.channels'] = $_SAP_MATRIX_CHANNELS;


	// matrix sap order types
	$sth = $model->db->prepare("
		SELECT 
			sap_matrix_ordertype_channel_id,
			sap_order_type_name
		FROM db_retailnet.sap_matrix_ordertypes
		INNER JOIN db_retailnet.sap_order_types ON sap_order_type_id = sap_matrix_ordertype_type_id
		WHERE sap_matrix_ordertype_address_id = ?
	");

	$sth->execute(array($ordersheet->address_id));
	$result = $sth->fetchAll();
	$_SAP_MATRIX_ORDERTYPES = _array::extract($result);
	$_DATA['sap.matrix.ordertypes'] = $_SAP_MATRIX_ORDERTYPES;
	

	if ($_SAP_INVOLVED_WAREHOUSES) {
		
		$keys = join(',',array_keys($_SAP_INVOLVED_WAREHOUSES));

		// warehouses sap order types
		$result = $model->query("
			SELECT DISTINCT
				mps_ordersheet_warehouse_id, 
				db_retailnet.sap_order_types.sap_order_type_name
			FROM mps_ordersheet_warehouses 
			INNER JOIN db_retailnet.address_warehouses ON mps_ordersheet_warehouses.mps_ordersheet_warehouse_address_warehouse_id = db_retailnet.address_warehouses.address_warehouse_id
			INNER JOIN db_retailnet.sap_matrix_ordertypes ON db_retailnet.address_warehouses.address_warehouse_address_id = db_retailnet.sap_matrix_ordertypes.sap_matrix_ordertype_address_id
			INNER JOIN db_retailnet.mps_distchannels ON db_retailnet.mps_distchannels.mps_distchannel_id = db_retailnet.sap_matrix_ordertypes.sap_matrix_ordertype_channel_id
			INNER JOIN db_retailnet.sap_order_types ON db_retailnet.sap_matrix_ordertypes.sap_matrix_ordertype_type_id = db_retailnet.sap_order_types.sap_order_type_id
			WHERE mps_ordersheet_warehouse_stock_reserve != 1 
			AND mps_distchannel_use_for_warehouses_export_to_sap = 1
			AND mps_ordersheet_warehouse_id IN ($keys)
		")->fetchAll();

		$_SAP_MATRIX_ORDERTYPES_WAREHOUSES = _array::extract($result);
		$_DATA['sap.matrix.ordertypes.warehouses'] = $_SAP_MATRIX_ORDERTYPES_WAREHOUSES;
	}
	
}


// sap sales orders ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_SALES_ORDERS = array();
$_USED_PONS = array();

if ($_CAN_EXPORT_SAP && $_NOT_CONFIRMED_ITEMS) {

			
	foreach ($_NOT_CONFIRMED_ITEMS as $key => $row) {
				
		$item = $row['item'];
		$pos = $row['pos'];
		$warehouse = $row['warehouse'];
		
		// sap order type
		$distChannel = $row['distChannel'];
		
		if ($warehouse) {
			$sapOrderType = $_SAP_MATRIX_ORDERTYPES_WAREHOUSES[$warehouse] ?: null;
		} else {
			$sapOrderType = $_SAP_MATRIX_ORDERTYPES[$distChannel] ?: null;
		}

		// sap distribution channel
		$planningType = $_MATERIALS[$item]['planning'];
		$sapDistChannel = $_SAP_MATRIX_CHANNELS[$planningType] ?: null; //$_SAP_PARAMETERS['distribution_channel'];


		$order = array();

		// order sheet item
		$order['sap_salesorder_item_mps_item_id'] = $key; 

		// order division code
		if ($_DIVISIONS[$_APPLICATION]) $order['sap_salesorder_item_division'] = $_DIVISIONS[$_APPLICATION];
		else $_ERRORS[] = "Missing division code for application $_APPLICATION";
			
		// order sheet quantity
		if ($row['quantity']) $order['sap_salesorder_itemr_quantity'] = $row['quantity'];	
		else $_ERRORS[] = "Missing item quantiti ($key)";
		
		// material ean number
		if ($_MATERIALS[$item]['ean']) $order['sap_salesorder_item_ean_number'] = $_MATERIALS[$item]['ean'];
		else $_ERRORS[] = "Missing item EAN number ($item)";
		
		// shipped quantity orer line
		//if ($_ORDERSHEET_ITEM_SHIPPED[$item]['line']) $order['sap_salesorder_item_mps_po_line_number'] = $_ORDERSHEET_ITEM_SHIPPED[$item]['line'];
		//else $_ERRORS[] = "Missing order sheet item shipped order line ($key)";

		$order['sap_salesorder_item_mps_po_line_number'] = $_ORDERSHEET_ITEM_SHIPPED[$item]['line'];

		// sales orders organization
		if ($_POS[$pos]['organization']) $order['sap_salesorder_item_sales_organization'] = $_POS[$pos]['organization'];
		elseif($_WAREHOUSES[$warehouse]['organization']) $order['sap_salesorder_item_sales_organization'] = $_WAREHOUSES[$warehouse]['organization'];
		else $_ERRORS[] = "Missing sales order organization (pos: $pos, warehouse: $warehouse)";

		// sold to number
		if ($_POS[$pos]['sap']) $order['sap_salesorder_item_sold_to'] = $_POS[$pos]['sap'];
		elseif($_WAREHOUSES[$warehouse]['sap']) $order['sap_salesorder_item_sold_to'] = $_WAREHOUSES[$warehouse]['sap'];
		else $_ERRORS[] = "Missing soldto number (pos: $pos, warehouse: $warehouse)";

		if ($_POS[$pos]['shipp'] && $_POS[$pos]['shipp'] <> $_POS[$pos]['sap']) {
			$order['sap_salesorder_item_ship_to'] = $_POS[$pos]['shipp'];
		} elseif($_WAREHOUSES[$warehouse]['shipp'] && $_WAREHOUSES[$warehouse]['shipp'] <> $_WAREHOUSES[$warehouse]['sap']) {
			$order['sap_salesorder_item_ship_to'] = $_WAREHOUSES[$warehouse]['shipp'];
		} else {
			$order['sap_salesorder_item_ship_to'] = null;
		}

		// shipto number
		//if ($_POS[$pos]['shipp']) $order['sap_salesorder_item_ship_to'] = $_POS[$pos]['shipp'];
		//elseif($_WAREHOUSES[$warehouse]['shipp']) $order['sap_salesorder_item_ship_to'] = $_WAREHOUSES[$warehouse]['shipp'];
		//else $_ERRORS[] = "Missing shipto number (pos: $pos, warehouse: $warehouse)";

		// order reason
		if ($_SAP_PARAMETERS['order_reason']) $order['sap_salesorder_item_order_reason'] = $_SAP_PARAMETERS['order_reason'];
		else $_ERRORS[] = "Missing sap parameter order reason";
		
		// order type
		if ($sapOrderType) $order['sap_salesorder_item_order_type'] = $sapOrderType;
		else $_ERRORS[] = "Missing sap parameter order type";
		
		// discount code
		if ($_SAP_PARAMETERS['discount_code']) $order['sap_salesorder_item_discount_code'] = $_SAP_PARAMETERS['discount_code'];
		else $_ERRORS[] = "Missing sap parameter discount code";
		
		// discount percentage
		if ($_SAP_PARAMETERS['discount_percentage']) $order['sap_salesorder_item_discount_percentage'] = $_SAP_PARAMETERS['discount_percentage'];
		else $_ERRORS[] = "Missing sap parameter discount percentage";

		if (!$sapDistChannel) {
			$_ERRORS[] = "Missing sap distribution channel code";
		}	

		if (!$_ERRORS) {
			$_SALES_ORDERS[$sapDistChannel][$key] = $order;
		} else {

			$_CAN_EXPORT_SAP = false;
			$_CAN_CONFIRM_ITEMS = false;
			
			$notification = "Confirm Distribution not possible, please check followin data:<br>";
					
			if (!$order['sap_salesorder_item_sales_organization']) 	$notification .= "<br>- Company Sales Organization";
			if (!$order['sap_salesorder_item_sold_to']) 			$notification .= "<br>- POS SAP Number";
			//if (!$order['sap_salesorder_item_mps_po_line_number']) 	$notification .= "<br>- Order Sheet Purchase Order Line Number";
			if (!$order['sap_salesorder_item_ean_number']) 			$notification .= "<br>- Item EAN Number";
			if (!$order['sap_salesorder_itemr_quantity']) 			$notification .= "<br>- Item Quantity";
			if (!$order['sap_salesorder_item_discount_code']) 		$notification .= "<br>- SAP Discount Code";
			if (!$order['sap_salesorder_item_discount_percentage']) $notification .= "<br>- SAP Discount Percentage";
			if (!$order['sap_salesorder_item_order_reason']) 		$notification .= "<br>- SAP order reason";
			if (!$order['sap_salesorder_item_order_type']) 			$notification .= "<br>- SAP order type";
			if (!$order['sap_salesorder_item_division'] ) 			$notification .= "<br>- SAP Division";
			if (!$sapDistChannel) 									$notification .= "<br>- SAP Distribution Channel Code";

			$_NOTIFICATIONS[] = $notification;
			goto BLOCK_RESPONDING;
			
			break;
		}
	}

	$_DATA['sales.orders'] = $_SALES_ORDERS;
}		


// confirm distributed items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_CONFIRMED_ITEMS = array();
$_TOTAL_CONFIRMED_QUANTITIES = array();
	
if ($_CAN_CONFIRM_ITEMS && $_NOT_CONFIRMED_ITEMS) {

	$sth = $model->db->prepare("
		UPDATE mps_ordersheet_item_delivered SET
			mps_ordersheet_item_delivered_confirmed = CURRENT_DATE(),
			user_modified = ?,
			date_modified = NOW()
		WHERE mps_ordersheet_item_delivered_id = ?
	");

	$_CONSOLE[] = "<< START CONFIRMATION >>";
	
	foreach ($_NOT_CONFIRMED_ITEMS as $id => $row) {
		
		$item = $row['item'];

		$update = DEBUGGING_MODE ? true : $sth->execute(array($user->login, $id));

		if ($update) {
			$_CONFIRMED_ITEMS[$id] = $row;
			$_TOTAL_CONFIRMED_QUANTITIES[$item] += $row['quantity'];
			$_CONSOLE[] = "Item $id is confirmed";
		} else {
			$_ERRORS[] = "Item $id is not confirmed;";
			goto BLOCK_RESPONDING;
		}
	}

	$_SUCCESS_CONFIRMATION = count($_NOT_CONFIRMED_ITEMS)==count($_CONFIRMED_ITEMS) ? true : false;
	
	$_CONSOLE[] = "<< END CONFIRMATION >>";

	$_DATA['confirmed.items'] = $_CONFIRMED_ITEMS;
	$_DATA['total.confirmed.items'] = $_TOTAL_CONFIRMED_QUANTITIES;
}
		

// update order sheet distributed quantities :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
if ($_CONFIRMED_ITEMS && $_TOTAL_ITEM_QUANTITIES) {
	
	$sth = $model->db->prepare("
		UPDATE mps_ordersheet_items SET
			mps_ordersheet_item_quantity_distributed = ?,
			user_modified = ?,
			date_modified = NOW()
		WHERE mps_ordersheet_item_id = ?
	");

	$_CONSOLE[] = "<< START UPDATE ORDER SHEET ITEM DISTRIBUTED QUANTITY >>";
	
	foreach ($_TOTAL_ITEM_QUANTITIES as $item => $quantity) {
		
		$response = DEBUGGING_MODE ? true : $sth->execute(array($quantity, $user->login, $item));
		
		if ($response) {
			$_CONSOLE[] = "Update item $item with quantiy $quantity";
		}
	}
	
	$_CONSOLE[] = "<< END UPDATE ORDER SHEET ITEM DISTRIBUTED QUANTITY >>";
}


// update stock adjustment quantities ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_CAN_EXPORT_SAP && $_CONFIRMED_ITEMS) {

	$_CONSOLE[] = "<< START UPDATE ADJUSTED QUANTITIES >>";

	$tracker->setEntity('mps_ordersheet_items_adjusted');

	$sth = $model->db->prepare("
		UPDATE mps_ordersheet_items_adjusted SET
			mps_ordersheet_items_adjusted_quantity = ?,
			mps_ordersheet_items_adjusted_confirmed_quantity = ?,
			mps_ordersheet_items_adjusted_confirmed_date = CURDATE(),
			user_modified = ?,
			date_modified = NOW()
		WHERE mps_ordersheet_items_adjusted_id = ?
	");

	
	

	foreach ($_CONFIRMED_ITEMS as $id => $row) {
		
		$item = $row['item'];
		$item_quantity = $row["quantity"]; // aho

		$adjusted = $_ADJUSTED_QUANTITIES[$item] ?: array();
		$adjustedId = $adjusted['id'];
		$adjustetQuantity = $adjusted['quantitiy'];
		$totalAdjustedConfirmedQuantitiy = $adjusted['confirmed'];
		$totalItemConfirmedQuantity =  $_TOTAL_CONFIRMED_QUANTITIES[$item];

		if ($adjustetQuantity) {

			//$dif =  $adjustetQuantity - $totalItemConfirmedQuantity;
			$dif =  $adjustetQuantity - $item_quantity;
			
			$_adj = $dif > 0 ? $item_quantity : $adjustetQuantity;
			
			$totalAdjustedConfirmedQuantitiy += $_adj;


			$_ADJUSTED_QUANTITIES[$item]['confirmed'] = $totalAdjustedConfirmedQuantitiy;

			$_remaining = $adjustetQuantity - $totalAdjustedConfirmedQuantitiy;

			$respone = DEBUGGING_MODE ? true : $sth->execute(array($_remaining, $totalAdjustedConfirmedQuantitiy, $user->login, $adjustedId));

			if ($respone) {
				$_CONSOLE[] = "Reset adjsuted quantitiy $adjustedId";
			}
			
			if ($respone && !DEBUGGING_MODE) {
				$tracker->onChange("Confirmation: Reset adjusted quantitiy and set $totalAdjustedConfirmedQuantitiy confirmed quantities", $item, $adjustedId);
			}
		}
	}

	$_CONSOLE[] = "<< END UPDATE ADJUSTED QUANTITIES >>";
}


// upgrade order sheet state :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_CONFIRMED_ITEMS) {

	$workflowState = new Workflow_State($_APPLICATION);
	$tracking = new User_Tracking($_APPLICATION);

	$_CONSOLE[] = "<< START CHANGE ORDER SHEET STATE >>";

	// set order sheet state in distribution
	if ($ordersheet->state()->isShipped() || $ordersheet->state()->isPartiallyShipped()) {

		$workflowState->read(Workflow_State::STATE_IN_DISTRIBUTION);

		if (!DEBUGGING_MODE) {
			
			$ordersheet->update(array(
				'mps_ordersheet_workflowstate_id' => Workflow_State::STATE_IN_DISTRIBUTION,
				'user_modified' => $user->login
			));
		
			$tracking->create(array(
				'user_tracking_entity' => 'order sheet',
				'user_tracking_entity_id' => $ordersheet->id,
				'user_tracking_user_id' => $user->id,
				'user_tracking_action' => $workflowState->name,
				'user_created' => $user->login,
				'date_created' => date('Y-m-d H:i:s')
			));
		}

		$_CONSOLE[] = "Set Order Sheet workflow state as $workflowState->name";
	}


	// get order sheet item quantities
	$items = $model->query("
		SELECT 
			mps_ordersheet_item_id AS id,
			mps_ordersheet_item_quantity_shipped AS shipped,
			mps_ordersheet_item_quantity_distributed AS distributed
		FROM mps_ordersheet_items
		WHERE mps_ordersheet_item_ordersheet_id = $ordersheet->id AND mps_ordersheet_item_quantity_shipped  > 0
	")->fetchAll();
		
	if ($items) {
			
		$distributed = 0;				
			
		// count distributed items
		foreach ($items as $row) {
			
			$item = $row['id'];
			$adjustedQuantity = $_ADJUSTED_QUANTITIES[$item] ? $_ADJUSTED_QUANTITIES[$item]['confirmed'] : 0;
			$_CONSOLE[] = $row['shipped'].", $adjustedQuantity, ".$row['distributed'];
			if ($row['shipped']+$adjustedQuantity==$row['distributed']) {
				$distributed++;
			}
		}
			
		// if all items are distributed
		// set order sheet as distributed
		if (count($items)==$distributed) {

			$state = ($ordersheet->exchangeOrderCompleted()) ? Workflow_State::STATE_DISTRIBUTED : Workflow_State::STATE_PARTIALLY_DISTRIBUTED;
			$workflowState->read($state);
			
			if (!DEBUGGING_MODE) {
				
				// update order sheet state
				$ordersheet->update(array(
					'mps_ordersheet_workflowstate_id' => $state,
					'user_modified' => $user->login,
					'date_modified' => date('Y-m-d H:i:s')
				));

				// user tracking
				$tracking->create(array(
					'user_tracking_entity' => 'order sheet',
					'user_tracking_entity_id' => $ordersheet->id,
					'user_tracking_user_id' => $user->id,
					'user_tracking_action' => $workflowState->name,
					'user_created' => $user->login,
					'date_created' => date('Y-m-d H:i:s')
				));

				$redirect = "/$_APPLICATION/ordersheets/archived/planning/$ordersheet->id";
			}

			$_CONSOLE[] = "Set ordersheet workflow state as $workflowState->name";
		}
	}

	$_CONSOLE[] = "<< END CHANGE ORDER SHEET STATE >>";
}


// upgrade master sheet states :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_CONFIRMED_ITEMS) {

	// get all master sheet orders
	$result = $model->query("
		SELECT 
			mps_ordersheet_id AS id, 
			mps_ordersheet_workflowstate_id As state
		FROM mps_ordersheets
		WHERE mps_ordersheet_mastersheet_id = $ordersheet->mastersheet_id
	")->fetchAll();

	if ($result) {

		$totalOrdersheets = count($result);
		$totalArchivedOrdersheets = 0;

		// sum archived order sheets
		foreach ($result as $row) {
			if ($row['state']==Workflow_State::STATE_DISTRIBUTED || $row['state']==Workflow_State::STATE_PARTIALLY_DISTRIBUTED) {
				$totalArchivedOrdersheets++;
			}
		}

		// if all order sheet are archived
		// send master sheeet also to archive
		if ($totalOrdersheets==$totalArchivedOrdersheets) {
			
			$mastersheet = new Mastersheet($_APPLICATION);
			$mastersheet->read($ordersheet->mastersheet_id);

			if (!DEBUGGING_MODE) {
				$mastersheet->update(array(
					'mps_mastersheet_archived' => 1,
					'user_modified' => $user->login,
					'date_modifeid' => date('Y-m-d H:i:s')
				));
			}
			
			$_CONSOLE[] = "Master Sheet $mastersheet->name is archived.";
		}
	}
}


// generate sap export orders ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
$_EXPORTS = array();

if (!$_ERRORS && $_CAN_EXPORT_SAP && $_SALES_ORDERS) {

	// timestamp as purchase order number
	$pon = $_SERVER['REQUEST_TIME'];

	// get all existed purchase order numbers
	$result = $model->query("
		SELECT DISTINCT
			LEFT(mps_ordersheet_item_delivered_sap_purchase_order_number, 10) AS pon, 
			mps_ordersheet_item_delivered_id AS id
		FROM mps_ordersheet_item_delivered
		WHERE mps_ordersheet_item_delivered_sap_purchase_order_number IS NOT NULL 
		OR mps_ordersheet_item_delivered_sap_purchase_order_number != ''
	")->fetchAll();

	$pons = _array::extract($result);
	
	foreach ($_SALES_ORDERS as $distributor => $orders) {
		
		$pon++;
		
		if ($pons[$pon]) {
			$pon++;
		}

		foreach ($orders as $item => $order) {
			
			// purchase number
			$number  = array();
			$number[] = $pon;
			$number[] = $order['sap_salesorder_item_sold_to'];
			$number[] = $order['sap_salesorder_item_ship_to'];

			// export data
			$order['sap_salesorder_item_mps_po_number'] = join('.', array_filter($number));
			$order['sap_salesorder_item_distribution_by'] = $distributor;
			$_EXPORTS[] = $order;

			// register pon
			$pons[$pon] = $item;
		}
	}

	$_DATA['exports'] = $_EXPORTS;
}



// sap export orders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NEW_ITEM_PONS = array();

if (!$_ERRORS && $_CAN_EXPORT_SAP && $_EXPORTS) {
		
	// db exchange model
	$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);
			
	$_CONSOLE[] = "START EXPORT ORDERS";
			
	// sales order statement
	$sth = $exchange->db->prepare("
		INSERT INTO sap_salesorder_items (
			sap_salesorder_item_mps_item_id,
			sap_salesorder_item_order_reason,
			sap_salesorder_item_order_type,
			sap_salesorder_item_sales_organization,
			sap_salesorder_item_distribution_by,
			sap_salesorder_item_division,
			sap_salesorder_item_sold_to,
			sap_salesorder_item_ship_to,
			sap_salesorder_item_mps_po_number,
			sap_salesorder_item_mps_po_line_number,
			sap_salesorder_item_ean_number,
			sap_salesorder_itemr_quantity,
			sap_salesorder_item_discount_code,
			sap_salesorder_item_discount_percentage
		) VALUES (
			:sap_salesorder_item_mps_item_id,
			:sap_salesorder_item_order_reason,
			:sap_salesorder_item_order_type,
			:sap_salesorder_item_sales_organization,
			:sap_salesorder_item_distribution_by,
			:sap_salesorder_item_division,
			:sap_salesorder_item_sold_to,
			:sap_salesorder_item_ship_to,
			:sap_salesorder_item_mps_po_number,
			:sap_salesorder_item_mps_po_line_number,
			:sap_salesorder_item_ean_number,
			:sap_salesorder_itemr_quantity,
			:sap_salesorder_item_discount_code,
			:sap_salesorder_item_discount_percentage
		)		
	");

			
	if ($settings->devmod) {
		
		// simulatet sap confirmed orders
		$sthConfirmed = $exchange->db->prepare("
			INSERT INTO sap_confirmed_items (
				sap_confirmed_item_action,
				sap_confirmed_item_order_type,
				sap_confirmed_item_sales_organization,
				sap_confirmed_item_distributed_by,
				sap_confirmed_item_division,
				sap_confirmed_item_sold_to,
				sap_confirmed_item_ship_to,
				sap_confirmed_item_sap_order_number,
				sap_confirmed_item_sap_order_line_number,
				sap_confirmed_item_mps_po_number,
				sap_confirmed_item_ean_number,
				sap_confirmed_item_quantity,
				sap_confirmed_item_confirmed_quantity,
				sap_confirmed_item_mps_po_line_number,
				sap_confirmed_item_date
			)
			VALUES (
				:sap_confirmed_item_action,
				:sap_confirmed_item_order_type,
				:sap_confirmed_item_sales_organization,
				:sap_confirmed_item_distributed_by,
				:sap_confirmed_item_division,
				:sap_confirmed_item_sold_to,
				:sap_confirmed_item_ship_to,
				:sap_confirmed_item_sap_order_number,
				:sap_confirmed_item_sap_order_line_number,
				:sap_confirmed_item_mps_po_number,
				:sap_confirmed_item_ean_number,
				:sap_confirmed_item_quantity,
				:sap_confirmed_item_confirmed_quantity,
				:sap_confirmed_item_mps_po_line_number,
				:sap_confirmed_item_date
			)
		");
		
		// simulatet sap shipped orders
		$sthShipped = $exchange->db->prepare("
			INSERT INTO sap_shipped_items (
				sap_shipped_item_sap_delivery_number,
				sap_shipped_item_date_shipped,
				sap_shipped_item_shipped_to,
				sap_shipped_item_sap_order_number,
				sap_shipped_item_sap_order_line_number,
				sap_shipped_item_mps_po_number,
				sap_shipped_item_mps_po_line_number,
				sap_shipped_item_ean_number,
				sap_shipped_item_quantity
			)
			VALUES (
				:sap_shipped_item_sap_delivery_number,
				:sap_shipped_item_date_shipped,
				:sap_shipped_item_shipped_to,
				:sap_shipped_item_sap_order_number,
				:sap_shipped_item_sap_order_line_number,
				:sap_shipped_item_mps_po_number,
				:sap_shipped_item_mps_po_line_number,
				:sap_shipped_item_ean_number,
				:sap_shipped_item_quantity
			)");
	}
			
	foreach ($_EXPORTS as $order) {	

		// export order to ramco
		$insert = DEBUGGING_MODE ? true : $sth->execute($order);
		
		if ($insert) {
			
			$item = $order['sap_salesorder_item_mps_item_id'];
			$pon = $order['sap_salesorder_item_mps_po_number'];
			$_NEW_ITEM_PONS[$item] = $pon;

			// inserted id
			$inserted = $exchange->db->lastInsertId();

			$_CONSOLE[] = "Export sales order $inserted";
			
			if ($settings->devmod && !DEBUGGING_MODE) {

				$confirmedQuantity = $order['sap_salesorder_itemr_quantity'];
				$shippedQuantity = $order['sap_salesorder_itemr_quantity'];
				
				if ($order['sap_salesorder_itemr_quantity'] > 100) {
					$confirmedQuantity = $order['sap_salesorder_itemr_quantity'];
					$shippedQuantity =  ceil($order['sap_salesorder_itemr_quantity']/2);
				}
				
				$sthConfirmed->execute(array(
					'sap_confirmed_item_action' => '004',
					'sap_confirmed_item_order_type' => $order['sap_salesorder_item_order_type'],
					'sap_confirmed_item_sales_organization' => $order['sap_salesorder_item_sales_organization'],
					'sap_confirmed_item_distributed_by' => $order['sap_salesorder_item_distribution_by'],
					'sap_confirmed_item_division' => $order['sap_salesorder_item_division'],
					'sap_confirmed_item_sold_to' => $order['sap_salesorder_item_sold_to'],
					'sap_confirmed_item_ship_to' => $order['sap_salesorder_item_ship_to'],
					'sap_confirmed_item_sap_order_number' => null,
					'sap_confirmed_item_sap_order_line_number' => null,
					'sap_confirmed_item_mps_po_number' => $order['sap_salesorder_item_mps_po_number'],
					'sap_confirmed_item_ean_number' => $order['sap_salesorder_item_ean_number'],
					'sap_confirmed_item_quantity' => $order['sap_salesorder_itemr_quantity'],
					'sap_confirmed_item_confirmed_quantity' => $confirmedQuantity,
					'sap_confirmed_item_mps_po_line_number' => $order['sap_salesorder_item_mps_po_line_number'],
					'sap_confirmed_item_date' => date('Y-m-d')
				));
				
				$sthShipped->execute(array(
					'sap_shipped_item_sap_delivery_number' => '004',
					'sap_shipped_item_date_shipped' => date('Y-m-d'),
					'sap_shipped_item_shipped_to' => $order['sap_salesorder_item_sold_to'],
					'sap_shipped_item_sap_order_number' => null,
					'sap_shipped_item_sap_order_line_number' => null,
					'sap_shipped_item_mps_po_number' => $order['sap_salesorder_item_mps_po_number'],
					'sap_shipped_item_mps_po_line_number' => $order['sap_salesorder_item_mps_po_line_number'],
					'sap_shipped_item_ean_number' => $order['sap_salesorder_item_ean_number'],
					'sap_shipped_item_quantity' => $shippedQuantity
				));
			}

		} else {
			$pon = $order['sap_salesorder_item_mps_po_number'];
			$_ERRORS[] = "Salse order $pon NOT exported";
		}
	}
	
	$_CONSOLE[] = "END EXPORT ORDERS";
}


// update item purchase order number :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && $_NEW_ITEM_PONS) {

	$_CONSOLE[] = "<< START UPDATE NEW PO NUMBERS";

	$sth = $model->db->prepare("
		UPDATE mps_ordersheet_item_delivered SET 
			mps_ordersheet_item_delivered_sap_purchase_order_number = ?
		WHERE mps_ordersheet_item_delivered_id = ?
	");

	foreach ($_NEW_ITEM_PONS as $item => $pon) {
		
		if (!DEBUGGING_MODE) {
			$sth->execute(array($pon, $item));
		}

		$_CONSOLE[] = "Update item $item whith po number $pon";
	}

	$_CONSOLE[] = "<< END UPDATE NEW PO NUMBERS";
}
		

// resonding :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	

BLOCK_RESPONDING:

$json['errors'] = $_ERRORS;
$json['console'] = $_CONSOLE;
$json['notifications'] = $_NOTIFICATIONS;

if (!DEBUGGING_MODE) {	
	
	$json['response'] = $_ERRORS ? false : $_SUCCESS_CONFIRMATION;
			
	if (!$_ERRORS && !DEBUGGING_MODE && $_SUCCESS_CONFIRMATION) {
		message::success("Successfully confirmation");
		$json['reload'] = true;
		$json['redirect'] = $redirect;
	}

	$json['message'] = $_NOTIFICATIONS ? join('<br />', $_NOTIFICATIONS) : null;

} else {
	$json['data'] = $_DATA;
}

// response with json header
header('Content-Type: text/json');
echo json_encode($json);
	