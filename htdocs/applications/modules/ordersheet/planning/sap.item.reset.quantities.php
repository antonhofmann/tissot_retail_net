<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

define('DEBUGGING_MODE', true);

$user = User::instance();
$translate = Translate::instance();

$_APPLICATION = $_REQUEST['application'];
$_ORDERSHEET = $_REQUEST['ordersheet'];
$_ITEM = $_REQUEST['item'];

$_JSON = array();
$_ERRORS = array();
$_SUCCESSES = array();

$model = new Model($_APPLICATION);
$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);

// order sheet	
$ordersheet = new Ordersheet($_APPLICATION);
$ordersheet->read($_ORDERSHEET);

// company
$company = new Company();
$company->read($ordersheet->address_id);

// state handler
$state = new State($ordersheet->workflowstate_id, $_APPLICATION); 
$state->setOwner($ordersheet->address_id);
$state->setDateIntervals($ordersheet->openingdate, $ordersheet->closingdate);


$_CAN_EDIT = $state->canEdit(false);
$_EXPORT_SAP = $company->do_data_export_from_mps_to_sap;
$_MODE_DISTRIBUTION = $state->onDistribution() || $state->isDistributed() ? true : false;

$_DIVISION =  '08';

// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$ordersheet->id) {
	$_ERRORS[] = "The order sheet not found.";
}

if (!$_ERRORS && !$_CAN_EDIT) {
	$_ERRORS[] = "You don't have permissions to submit this action.";
	goto BLOCK_RESPONDING;
}

if (!$_ERRORS && !$_EXPORT_SAP) {
	$_ERRORS[] = "The Company $company->company cannot export orders to SAP.";
	goto BLOCK_RESPONDING;
}

if (!$_ERRORS && !$_MODE_DISTRIBUTION) {
	$_ERRORS[] = "Reset exported orders is permitted only in distributin status.";
	goto BLOCK_RESPONDING;
}

if (!$_ITEM) {
	$_ERRORS[] = "Order sheet item is not defined.";
	goto BLOCK_RESPONDING;
}


// get distributions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_DISTRIBUTED_ITEMS = array();

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT 
			mps_ordersheet_item_delivered_id AS id,
			mps_ordersheet_item_delivered_posaddress_id AS pos,
			mps_ordersheet_item_delivered_quantity AS quantity,
			DATE_FORMAT(mps_ordersheet_item_delivered_confirmed, '%d.%m.%Y') AS date,
			UNIX_TIMESTAMP(mps_ordersheet_item_delivered_confirmed) AS confirmed_timestamp,
			CONCAT(posaddress_name, ', ', place_name) As posName
		FROM mps_ordersheet_item_delivered
		INNER JOIN db_retailnet.posaddresses ON posaddress_id = mps_ordersheet_item_delivered_posaddress_id
		INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
		WHERE mps_ordersheet_item_delivered_ordersheet_item_id = ? 
		AND mps_ordersheet_item_delivered_confirmed IS NOT NULL
		ORDER BY posName
	");

	$sth->execute(array($_ITEM));
	$result = $sth->fetchAll();

	if ($result) $_DISTRIBUTED_ITEMS = _array::datagrid($result);
	else $_ERRORS[] = "Distributed items not found";
}


// get sales orders ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_SAP_SALES_ORDERS = array();

if (!$_ERRORS && $_DISTRIBUTED_ITEMS) {

	$invlvedItems = join(',', array_keys($_DISTRIBUTED_ITEMS));

	$result = $exchange->query("
		SELECT 
			sap_salesorder_item_id AS id,
			sap_salesorder_item_mps_item_id AS item,
			sap_salesorder_item_mps_po_number AS pon,
			sap_salesorder_item_ean_number AS ean,
			sap_salesorder_itemr_quantity AS quantity
		FROM sap_salesorder_items
		WHERE sap_salesorder_item_mps_item_id IN ($invlvedItems) 
		AND sap_salesorder_item_division = '$_DIVISION'
	")->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$pon = $row['pon'];
			$ean = $row['ean'];
			$_SAP_SALES_ORDERS[$pon][$ean]['id'] = $row['id'];
			$_SAP_SALES_ORDERS[$pon][$ean]['item'] = $row['item'];
			$_SAP_SALES_ORDERS[$pon][$ean]['quantity'] = $row['quantity'];
		}
	} else {
		$_ERRORS[] = "Sales orders items not found";
	}
}


// get sap confiremd items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_SAP_CONFIRMED_ITEMS = array();
$_SAP_FAILURE_ITEMS = array();

if (!$_ERRORS && $_SAP_SALES_ORDERS) {

	$pons = "'".join("', '", array_keys($_SAP_SALES_ORDERS))."'";

	$result = $exchange->query("
		SELECT 
			sap_confirmed_item_mps_po_number AS pon,
			sap_confirmed_item_ean_number AS ean,
			sap_confirmed_item_confirmed_quantity AS quantity
		FROM sap_confirmed_items
		WHERE sap_confirmed_item_mps_po_number IN ($pons) 
		AND sap_confirmed_item_division = '$_DIVISION'
		AND sap_confirmed_item_imported_in_mps IS NOT NULL
	")->fetchAll();

	if ($result) {

		foreach ($result as $row) {
			

			$pon = $row['pon'];
			$ean = $row['ean'];

			// order
			$order = $_SAP_SALES_ORDERS[$pon][$ean] ?: array();
			$item = $order['item'];

			$confirmedQuantity = $row['quantity'];
			$salesOrderQuantity = $order['quantity'];

			// sap failure quantities
			if ($order && $confirmedQuantity < $salesOrderQuantity)  {
				$_SAP_FAILURE_ITEMS[$item]['ordered'] = $salesOrderQuantity;
				$_SAP_FAILURE_ITEMS[$item]['confirmed'] = $confirmedQuantity;
				$_SAP_FAILURE_ITEMS[$item]['reset'] = $salesOrderQuantity - $confirmedQuantity;
			}

			$_SAP_CONFIRMED_ITEMS[$item]['ordered'] = $salesOrderQuantity;
			$_SAP_CONFIRMED_ITEMS[$item]['confirmed'] = $confirmedQuantity; 
		}
	}
}

// datagrid :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


$_HEADERS = array();
$_DATA = array();


if (!$_ERRORS && $_SAP_FAILURE_ITEMS) {

	foreach ($_SAP_FAILURE_ITEMS as $id => $quantity) {

		$item = $_DISTRIBUTED_ITEMS[$id];
		$pos = $item['pos'];
		$timestamp = $item['confirmed_timestamp'];
		
		// header
		$_HEADERS[$timestamp]['caption'] = $item['date'];

		// datagrid
		$_DATA[$pos]['pos'] = $item['posName'];
		$_DATA[$pos]['quantities'][$timestamp]['distributed'] = intval($item['quantity']);
		$_DATA[$pos]['quantities'][$timestamp]['ordered'] = $quantity['ordered'];
		$_DATA[$pos]['quantities'][$timestamp]['confirmed'] = intval($quantity['confirmed']);
		$_DATA[$pos]['quantities'][$timestamp]['reset'] = $quantity['reset'];
		$_DATA[$pos]['quantities'][$timestamp]['id'] = $id;
	}
}

$_JSON['headers'] = $_HEADERS;
$_JSON['data'] = $_DATA;

if (DEBUGGING_MODE) {
	$_JSON['distribution'] = $_DISTRIBUTED_ITEMS;
	$_JSON['orders'] = $_SAP_SALES_ORDERS;
	$_JSON['confirmation'] = $_SAP_CONFIRMED_ITEMS;
	$_JSON['failures'] = $_SAP_FAILURE_ITEMS;
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONDING:

if ($_ERRORS) {
	$_JSON['errors'] = $_ERRORS;
}

if ($_SUCCESSES) {
	$_JSON['notifications'] = $_SUCCESSES;
}

header('Content-Type: text/json');
echo json_encode($_JSON);


