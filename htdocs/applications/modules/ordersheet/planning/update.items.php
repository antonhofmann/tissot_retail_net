<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$id = $_REQUEST['ordersheet'];

// ordersheet
$ordersheet = new Ordersheet($application);
$ordersheet->read($id);

// state handler
$state = new State($ordersheet->workflowstate_id, $application); 
$state->setOwner($ordersheet->address_id);
$state->setDateIntervals($ordersheet->openingdate, $ordersheet->closingdate);

if ($ordersheet->id) {
	
	$model = new Model($application);
	
	$workflowState = new Workflow_State($application);
	$tracker = new User_Tracking($application);
	
	// update planning
	if ($state->onPreparation()) {
		
		$result = $model->query("
			SELECT *
			FROM mps_ordersheet_item_planned
		")
		->bind(Ordersheet_Item_Planned::DB_BIND_ORDERSHEET_ITEMS)
		->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = $id")
		->fetchAll();
		
		if ($result) {
		
			$quantities = array();
		
			foreach ($result as $row) {
				$item = $row['mps_ordersheet_item_planned_ordersheet_item_id'];
				$quantities[$item] = $quantities[$item] + $row['mps_ordersheet_item_planned_quantity'];
			}
		}
		
		// update order sheet item quantities
		if ($quantities) {
		
			$response = true;
		
			foreach ($quantities as $item => $quantity) {
				
				$ordersheet->item()->read($item);
				
				$ordersheet->item()->update(array(
					'mps_ordersheet_item_quantity' => $quantity,
					'user_modified' => $user->login,
					'date_modified' => date('Y-m-d H:i:s')
				));
			}
			
			// update order sheet state
			if ($ordersheet->workflowstate_id==Workflow_State::STATE_OPEN) {
				
				// workflow state
				$workflowState->read(Workflow_State::STATE_PROGRESS);
				
				// set state in progress
				$inProgress = $ordersheet->update(array(
					'mps_ordersheet_workflowstate_id' => $workflowState->id,
					'user_modified' => $user->login,
					'date_modified' => date('Y-m-d H:i:s')
				));
				
				// trackering
				if ($inProgress) {
					$tracker->create(array(
						'user_tracking_entity' => 'order sheet',
						'user_tracking_entity_id' => $id,
						'user_tracking_user_id' => $user->id,
						'user_tracking_action' => $workflowState->name,
						'user_created' => $user->login,
						'date_created' => date('Y-m-d H:i:s')
					));
				}
			}
		}
	}
	// update distribution
	elseif ($state->isShipped() || $state->isPartiallyShipped()) {
		
		$planned_items = $model->query("
			SELECT DISTINCT
				mps_ordersheet_item_planned_id,
				mps_ordersheet_item_planned_ordersheet_item_id,
				mps_ordersheet_item_planned_posaddress_id,
				mps_ordersheet_item_planned_warehouse_id,
				mps_ordersheet_item_planned_quantity,
				mps_ordersheet_warehouse_stock_reserve
			FROM mps_ordersheet_item_planned
		")
		->bind(Ordersheet_Item_Planned::DB_BIND_ORDERSHEET_ITEMS)
		->bind(Ordersheet_Item_Planned::DB_LEFT_ORDERSHEET_WAREHOUSES)
		->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = $id")
		->fetchAll();
		
		if ($planned_items) { 
		
			$planned_quantities = array();
			
			// sum planned quantities
			foreach ($planned_items as $row) {
				
				$item = $row['mps_ordersheet_item_planned_ordersheet_item_id'];
				
				if (!$row['mps_ordersheet_warehouse_stock_reserve']) {
					$planned_quantities[$item] = $planned_quantities[$item] + $row['mps_ordersheet_item_planned_quantity'];
				}
			}
			
			$items = array();
			
			// transfer planned quantities to delivered quantities
			foreach ($planned_items as $row) {
				
				$key = $row['mps_ordersheet_item_planned_id'];
				$item = $row['mps_ordersheet_item_planned_ordersheet_item_id'];
				$stock_reserve = $row['mps_ordersheet_warehouse_stock_reserve'];
				
				// read item
				$ordersheet->item()->read($item);
				$quantity_shipped = $ordersheet->item()->quantity_shipped;
				
				if (!$stock_reserve && $quantity_shipped > 0 && $quantity_shipped >= $planned_quantities[$item] ) {
					
					$items[$item][$key] = array(
						'pos' => $row['mps_ordersheet_item_planned_posaddress_id'],
						'warehouse' => $row['mps_ordersheet_item_planned_warehouse_id'],
						'quantity' => $row['mps_ordersheet_item_planned_quantity']
					);
					
					$in_distribution = true;
				}
			}

			if ($items) {
				
				foreach ($items as $item => $row) {
					
					$ordersheet->item()->read($item);
					$ordersheet->item()->delivered()->item = $item;
					$ordersheet->item()->delivered()->delete_all();
					
					foreach ($row as $value) {
						if ($value['pos']) $response = $ordersheet->item()->delivered()->add_pos($value['pos'], $value['quantity']);
						elseif($value['warehouse']) $response = $ordersheet->item()->delivered()->add_warehouse($value['warehouse'], $value['quantity']);
					}
				}
			}
		}
	}
	
	if ($in_distribution) {
		
		$ordersheet->update(array(
			'mps_ordersheet_workflowstate_id' => Workflow_State::STATE_IN_DISTRIBUTION,
			'user_modified' => $user->login,
			'date_modified' => date('Y-m-d H:i:s')
		));
		
		$workflowState->read(Workflow_State::STATE_IN_DISTRIBUTION);
		
		// user tracking
		$tracker->create(array(
			'user_tracking_entity' => 'order sheet',
			'user_tracking_entity_id' => $ordersheet->id,
			'user_tracking_user_id' => $user->id,
			'user_tracking_action' => $workflowState->name,
			'user_created' => $user->login,
			'date_created' => date('Y-m-d H:i:s')
		));
	}
	
	// show session message
	// for not updated items
	if ($not_transfered) {
		message::add(array(
			'type' => 'warning',
			'keyword' => 'warning_update_ordersheet_from_planning',
			'life' => 3000
		));
	}
	
	$response = true;
	message::request_updated();
	
} else {
	$response = false;
	$message = $translate->invalid_request;
}

// response with json header
header('Content-Type: text/json');
echo json_encode(array(
	'response' => $response,
	'message' => $message
));

