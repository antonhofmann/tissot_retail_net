<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

define(DEBUGGING_MODE, false);

$user = User::instance();
$translate = Translate::instance();

// request
$_APPLICATION = $_REQUEST['application'];
$_ID = $_REQUEST['ordersheet'];

// consoling
$_RESPONSE = array();
$_ERRORS = array();
$_CONSOLE = array();
$_NOTIFICATIONS = array();
$_DATA = array();

// db model
$model = new Model($_APPLICATION);

//  user tracker
$tracker = new Tracker($_APPLICATION);


// checkin :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (DEBUGGING_MODE) {
	$_CONSOLE[] = "DEBUGGIMG MODE";
}

$ordersheet = new Ordersheet($_APPLICATION);
$ordersheet->read($_ID);

if (!$ordersheet->id) {
	$_ERRORS[] = "Order sheet $_ID not found";
	goto BLOCK_RESPONDING;
} else {
	$_CONSOLE[] = "Order sheet $_ID found";
}

// state handler
$state = new State($ordersheet->workflowstate_id, $application); 
$state->setOwner($ordersheet->address_id);
$state->setDateIntervals($ordersheet->openingdate, $ordersheet->closingdate);

if (!$state->isOwner() && !$state->canAdministrate() ) {
	$_ERRORS[] = "You are not permitted to activate this order sheet";
	goto BLOCK_RESPONDING;
}

// submitting ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$tracking = new User_Tracking($_APPLICATION);

$workflowState = new Workflow_State($_APPLICATION);
$workflowState->read(Workflow_State::STATE_IN_DISTRIBUTION);

if (!DEBUGGING_MODE) {

	$_RESPONSE['success'] = $ordersheet->update(array(
		'mps_ordersheet_workflowstate_id' => $workflowState->id,
		'user_modified' => $user->login,
		'date_modified' => date('Y-m-d H:i:s')
	));
				
				
	$tracking->create(array(
		'user_tracking_entity' => 'order sheet',
		'user_tracking_entity_id' => $ordersheet->id,
		'user_tracking_user_id' => $user->id,
		'user_tracking_action' => "$workflowState->name (activated)",
		'user_created' => $user->login,
		'date_created' => date('Y-m-d H:i:s')
	));

} else {
	$_RESPONSE['success'] = true;
}


// resonding :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	

BLOCK_RESPONDING:

$_RESPONSE['console'] = $_CONSOLE;
$_RESPONSE['errors'] = $_ERRORS;
$_RESPONSE['messages'] = $_NOTIFICATIONS;

if (!DEBUGGING_MODE && $_RESPONSE['success']) {
	message::success($translate->message_activate_ordersheet);
	//$_RESPONSE['reload'] = true;
	$_RESPONSE['redirect'] = "/$_APPLICATION/ordersheets/planning/$ordersheet->id";;
}

// response with json header
header('Content-Type: text/json');
echo json_encode($_RESPONSE);

