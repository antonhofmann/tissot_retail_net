<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc'; 

$user = User::instance();
$translate = Translate::instance();

//	request ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['ordersheet'];

$_VERSION = $_REQUEST['version'];
$_DISTRIBUTION_CHANNEL = $_REQUEST['distribution_channels'];
$_TURNOVER_CLASS_WATCH = $_REQUEST['turnoverclass_watches'];
$_SALES_REPRESENTATIVE = $_REQUEST['sales_representative'];
$_DECORATION_PERSON = $_REQUEST['decoration_person'];
$_PROVINCE = $_REQUEST['provinces'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

//	order sheet ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// model
$model = new Model($application);

// current order sheet	
$ordersheet = new Ordersheet($application);
$ordersheet->read($id);

// company
$company = new Company();
$company->read($ordersheet->address_id);

// state handler
$state = new State($ordersheet->workflowstate_id, $application); 
$state->setOwner($ordersheet->address_id);
$state->setDateIntervals($ordersheet->openingdate, $ordersheet->closingdate);
	

//	spreadsheet settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

define(COLUMN_PLANNING, 'planned');					// column planning
define(COLUMN_DISTRIBUTION, 'delivered');			// column distribution
define(COLUMN_PARTIALY_CONFIRMED, 'partial');		// column partially distribution
define('CELL_READONLY', 'true');					// spreadsheet readonly attribute


// some important states
$_STATE_EDIT = $state->canEdit(false);
$_STATE_APPROVED = $state->onApproving() ? false : true;
$_STATE_DISTRIBUTED = $state->isDistributed() ? true : false;

// company can export items in SAP
$_EXPORT_SAP = $company->do_data_export_from_mps_to_sap;

// readonly mod
if ($_VERSION) $_STATE_READONLY = true;
elseif ($state->isDistributed()) $_STATE_READONLY = $_EXPORT_SAP ? false : true;
elseif ($state->isOwner() && $state->isPreparation()) $_STATE_READONLY = true;
else $_STATE_READONLY = !$_STATE_EDIT;

// working mode
$_MODE_PLANNING = $state->onPreparation() || $state->onConfirmation() ? true : false;
$_MODE_DISTRIBUTION = $state->onDistribution() || $state->isDistributed() ? true : false;
$_MODE_ARCHIVE = $state->isManuallyArchived() || $state->isDistributed() ? true : false;

// spreadsheet attribute readonly
$_ATTR_READONLY =  $_STATE_READONLY ? 'true' : 'false';

// extended rows (warehouses)
$_AREA_WAREHOUSE = $_STATE_DISTRIBUTED || $_VERSION ? false : true;

$_STOCK_RESERVE_EDIT = false;

// stock reserve edit
if ($_STATE_EDIT) {
	if ($state->canAdministrate()) $_STOCK_RESERVE_EDIT = $state->onApproving() ? false : true;
	else $_STOCK_RESERVE_EDIT = $state->onCompleting() ? true : false;
}

// set item quantity validator
$_VALIDATA_TOTAL_ITEM_QUANTITY = $state->onApproving() ? false : true;

// check if delivered quantities are once confirmed
/*
if ($_MODE_DISTRIBUTION) {
	$result = $model->query("
		SELECT COUNT(mps_ordersheet_item_delivered_id) AS total
		FROM mps_ordersheet_item_delivered	
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id 
		WHERE mps_ordersheet_item_ordersheet_id = $ordersheet->id AND mps_ordersheet_item_delivered_confirmed IS NOT NULL
	")->fetch();

	$OrderSheetHasConfirmedItems = $result['total'] > 0 ? true : false;
}
*/


//	filters ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$_FILTERS = array();

// filter: pos
$_FILTERS['default'] = "(
	posaddress_client_id = $ordersheet->address_id 
	OR (address_parent = $ordersheet->address_id AND address_client_type = 3)
)";

// filter: distribution channels
if ($_DISTRIBUTION_CHANNEL) {
	$_FILTERS['distribution_channels'] = "posaddress_distribution_channel = $_DISTRIBUTION_CHANNEL";
}

// filter: turnover classes
if ($_TURNOVER_CLASS_WATCH) {
	$_FILTERS['turnoverclass_watches'] = "posaddress_turnoverclass_watches = $_TURNOVER_CLASS_WATCH";
}

// filter: sales represenatives
if ($_SALES_REPRESENTATIVE) {
	$_FILTERS['sales_representative'] = "posaddress_sales_representative = $_SALES_REPRESENTATIVE";
}

// filter: decoration persons
if ($_DECORATION_PERSON) {
	$_FILTERS['decoration_person'] = "posaddress_decoration_person = $_DECORATION_PERSON";
}

// provinces
if ($_PROVINCE) {
	$_FILTERS['provinces'] = "province_id = $_PROVINCE";
}

// for archived ordersheets show only POS locations which has distributed quantities 
if ($_STATE_DISTRIBUTED and false) {
	
	$result = $model->query("
		SELECT GROUP_CONCAT(DISTINCT mps_ordersheet_item_delivered_posaddress_id) AS pos
		FROM mps_ordersheet_item_delivered
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id 
		WHERE mps_ordersheet_item_ordersheet_id = $ordersheet->id AND mps_ordersheet_item_delivered_posaddress_id > 0
	")->fetch();
	
	if ($result) {
		$_FILTERS['active'] = "posaddress_id IN ({$result[pos]})";
	}

} else {
	$_FILTERS['active'] = "(
		posaddress_store_closingdate = '0000-00-00'
		OR posaddress_store_closingdate IS NULL
		OR posaddress_store_closingdate = ''
	)";
}

$_FILTERS = $_FILTERS ? join(' AND ', array_values($_FILTERS)) : null;
	
	
//	pos locations ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$_POS_LOCATIONS = array();
$_SAP_INVOLVED_POS = array();

$result = $model->query("
	SELECT DISTINCT
		posaddress_id,
		posaddress_name,
		posaddress_sapnumber,
		posaddress_sap_shipto_number,
		posaddress_place,
		postype_id,
		postype_name,
		posaddress_store_subclass
	FROM db_retailnet.posaddresses
	INNER JOIN db_retailnet.addresses ON address_id = posaddress_client_id
	INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
	INNER JOIN db_retailnet.provinces ON province_id = place_province
	INNER JOIN db_retailnet.postypes ON postype_id = posaddress_store_postype
	WHERE $_FILTERS
	ORDER BY posaddress_name, posaddress_place
")->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$pos = $row['posaddress_id'];
		$type = $row['postype_id'];
		
		// group pos locations by pos type
		$_POS_LOCATIONS[$type]['name'] = $row['postype_name'];
		$_POS_LOCATIONS[$type]['data'][$pos]['name'] = $row['posaddress_name'].', '.$row['posaddress_place'];
		$_POS_LOCATIONS[$type]['data'][$pos]['sap'] = $row['posaddress_sapnumber'];
		$_POS_LOCATIONS[$type]['data'][$pos]['shipto'] = $row['posaddress_sap_shipto_number'];
		
		if ($_EXPORT_SAP) {
			$_SAP_INVOLVED_POS[$pos] = true;
		}
	}
}


//	items ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEMS = array();
$_REFERENCES = array();

$result = $model->query("
	SELECT
		mps_ordersheet_item_id,
		mps_ordersheet_item_material_id,
		mps_ordersheet_item_quantity,
		mps_ordersheet_item_quantity_approved,
		mps_ordersheet_item_quantity_confirmed,
		mps_ordersheet_item_quantity_shipped,
		mps_ordersheet_item_quantity_distributed,
		mps_material_planning_type_id,
		mps_material_planning_type_name,
		mps_material_collection_category_id,
		mps_material_collection_category_code,
		mps_material_id,
		mps_material_code,
		mps_material_name
	FROM mps_ordersheet_items
 	INNER JOIN mps_ordersheets ON  mps_ordersheet_id = mps_ordersheet_item_ordersheet_id 
 	INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id 
 	LEFT JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id 
 	LEFT JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id 
 	WHERE mps_ordersheet_id = $ordersheet->id 
 	ORDER BY 
 		mps_material_planning_type_name asc, 
 		mps_material_collection_category_code asc, 
 		mps_material_code asc, mps_material_name asc
")->fetchAll();


if ($result) {

	$onApproving = $state->onApproving();

	$totalFinishedItems = 0;
	
	foreach ($result as $row) {
		
		$visible = true;
		$quantityPlanned = $row['mps_ordersheet_item_quantity'];
		$quantityApproved = $row['mps_ordersheet_item_quantity_approved'];
		$quantityConfirmed = $row['mps_ordersheet_item_quantity_confirmed'];
		$quantityShipped = $row['mps_ordersheet_item_quantity_shipped'];
		$quantityDistributed = $row['mps_ordersheet_item_quantity_distributed'];
		$item = $row['mps_ordersheet_item_id'];
		$material = $row['mps_ordersheet_item_material_id'];
		$group = $row['mps_material_planning_type_id'];
		$subgroup = $row['mps_material_collection_category_id'];
		
		// show item only in case:
		// mod planning: quantity approved greater then zero or null
		// mod distribution: quantity shipped greater then zero
		if ($_MODE_DISTRIBUTION) $visible = $quantityShipped > 0 ? true : false;
		elseif (!$onApproving) $visible = $quantityApproved > 0 ? true : false;

		if (!$visible) {
			continue;
		}

		// group order sheet by planning type and collection category
		$_ITEMS[$group]['caption'] = $row['mps_material_planning_type_name'];
		$_ITEMS[$group]['data'][$subgroup]['caption'] = $row['mps_material_collection_category_code'];
		$_ITEMS[$group]['data'][$subgroup]['data'][$item]['item'] = $row['mps_material_id'];
		$_ITEMS[$group]['data'][$subgroup]['data'][$item]['code'] = $row['mps_material_code'];
		$_ITEMS[$group]['data'][$subgroup]['data'][$item]['name'] = $row['mps_material_name'];

		// reference material
		$_REFERENCES[$item]['material']['id'] = $material;
		$_REFERENCES[$item]['material']['group'] = $group;
		$_REFERENCES[$item]['material']['subgroup'] = $subgroup;
		$_REFERENCES[$item]['material']['code'] = $row['mps_material_code'];
		$_REFERENCES[$item]['material']['name'] = $row['mps_material_name'];
		
		// reference quantity
		$_REFERENCES[$item]['quantity']['planned'] = $quantityPlanned;
		$_REFERENCES[$item]['quantity']['approved'] = $quantityApproved;
		$_REFERENCES[$item]['quantity']['confirmed'] = $quantityConfirmed;
		$_REFERENCES[$item]['quantity']['shipped'] = $quantityShipped;
		$_REFERENCES[$item]['quantity']['distributed'] = $quantityDistributed;
		
		// reference state
		$_REFERENCES[$item]['state']['edit'] = $_STATE_READONLY ? false : $_STATE_EDIT;

		if ($quantityDistributed > 0) {
			
			if ($quantityDistributed >=$quantityShipped) {
				
				$_REFERENCES[$item]['state']['distributed'] = true;
				
				if ($_MODE_ARCHIVE) $_REFERENCES[$item]['state']['finished'] = true;
				else $_REFERENCES[$item]['state']['finished'] = $_EXPORT_SAP ? false : true;

			} else {
				$_REFERENCES[$item]['state']['partially'] = true;
			}
		}

		$totalFinishedItems = $_REFERENCES[$item]['state']['finished'] ? $totalFinishedItems+1 : $totalFinishedItems;
	}

	$_ALL_ITEMS_FINISHED = $totalFinishedItems==count($result) ? true : false;
}


//	set warehouse stock reserve id :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("
	SELECT mps_ordersheet_warehouse_id
	FROM mps_ordersheet_warehouses
	WHERE mps_ordersheet_warehouse_stock_reserve = 1 AND mps_ordersheet_warehouse_ordersheet_id = $ordersheet->id
")->fetch();

$_WAREHOUSE_STOCK_RESERVE = $result['mps_ordersheet_warehouse_id'];


//	item planned quantities ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$_POS_PLANNED_QUANTITES = array();
$_WAREHOUSE_PLANNED_QUANTITIES = array();
$_WARWHOUSE_RESERVE_QUANTITIES = array();

$result = $model->query("
	SELECT DISTINCT
		mps_ordersheet_item_planned_id AS id,
		mps_ordersheet_item_planned_ordersheet_item_id AS item,
		mps_ordersheet_item_planned_posaddress_id AS pos,
		mps_ordersheet_item_planned_warehouse_id AS warehouse,
		mps_ordersheet_item_planned_quantity AS quantity,
		mps_ordersheet_warehouse_stock_reserve AS reserve
	FROM mps_ordersheet_item_planned
	INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_planned_ordersheet_item_id 
	LEFT JOIN mps_ordersheet_warehouses ON mps_ordersheet_warehouse_id = mps_ordersheet_item_planned_warehouse_id 
	WHERE mps_ordersheet_item_ordersheet_id = $ordersheet->id
")->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$pos = $row['pos'];
		$item = $row['item'];
		$warehouse = $row['warehouse'];

		$data = array(
			'id' => $row['id'],
			'quantity' => $row['quantity']
		);
		
		// planned quantities
		if ($pos) {
			$_POS_PLANNED_QUANTITES[$item][$pos] = $data;
		}

		// planned warehouse quantities
		if ($warehouse) {
			if ($row['reserve']) {
				$_WARWHOUSE_RESERVE_QUANTITIES[$item][$warehouse] = $data;
			} else {
				$_WAREHOUSE_PLANNED_QUANTITIES[$item][$warehouse] = $data;
			}
		}
	}
}
	
	
// items distributed quantities ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_POS_DISTRIBUTED_QUANTITIES = array();
$_POS_DISTRIBUTED_QUANTITIES_NOT_CONFIRMED = array();
$_WAREHOUSE_DISTRIBUTED_QUANTITIES = array();
$_WAREHOUSE_DISTRIBUTED_QUANTITIES_NOT_CONFIRMED = array();
$_WAREHOUSE_DISTRIBUTED_RESERVE_QUANTITIES = array();
$_WAREHOUSE_DISTRIBUTED_RESERVE_QUANTITIES_NOT_CONFIRMED = array();
$_CONFIRMATION_DATES = array();

$_ITEMS_CONFIRMED = array();

if ($_MODE_DISTRIBUTION) { 

	$result = $model->query("
		SELECT DISTINCT
			mps_ordersheet_item_delivered_id,
			mps_ordersheet_item_delivered_ordersheet_item_id,
			mps_ordersheet_item_delivered_posaddress_id,
			mps_ordersheet_item_delivered_warehouse_id,
			mps_ordersheet_item_delivered_quantity,
			mps_ordersheet_item_delivered_confirmed,
			DATE_FORMAT(mps_ordersheet_item_delivered_confirmed,'%d.%m.%Y') AS confirmed_date,
			mps_ordersheet_item_id,
			mps_ordersheet_item_material_id,
			mps_material_material_planning_type_id,
			mps_material_material_collection_category_id,
			mps_ordersheet_warehouse_stock_reserve
		FROM mps_ordersheet_item_delivered
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id 
		INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id 
		LEFT JOIN mps_ordersheet_warehouses ON mps_ordersheet_warehouse_id = mps_ordersheet_item_delivered_warehouse_id 
		WHERE mps_ordersheet_item_ordersheet_id = $ordersheet->id 
		ORDER BY 
			mps_ordersheet_item_delivered_confirmed DESC, 
			mps_ordersheet_item_delivered.date_created DESC
	")->fetchAll();

	if ($result) { 
		
		foreach ($result as $row) {

			$id = $row['mps_ordersheet_item_delivered_id'];
			$planning = $row['mps_material_material_planning_type_id'];
			$collection = $row['mps_material_material_collection_category_id'];
			$item = $row['mps_ordersheet_item_id'];
			$pos = $row['mps_ordersheet_item_delivered_posaddress_id'];
			$warehouse = $row['mps_ordersheet_item_delivered_warehouse_id'];
			$stockreserve = $row['mps_ordersheet_warehouse_stock_reserve'];
			$quantity = $row['mps_ordersheet_item_delivered_quantity'];
			$timestamp = ($row['confirmed_date']) ? date::timestamp($row['confirmed_date']) : 0;

			// distributed items
			if ($row['confirmed_date']) {
				$_ITEMS_CONFIRMED[$id] = $row;
			}
			
			// build versions data
			if ($timestamp) {
				$_CONFIRMATION_DATES[$timestamp] = $row['confirmed_date'];
			}

			$data = array(
				'id' => $id,
				'quantity' => $quantity
			);
			
			// show quantities on required confirmed date
			if ($_VERSION) {
			
				if ($_VERSION==$timestamp) {
					
					// pos delivered quantities
					if ($pos) {
						$_POS_DISTRIBUTED_QUANTITIES[$item][$pos] = $data;
					}
					
					// warehouse delivered quantities
					if ($warehouse) {
						if ($stockreserve) $_WAREHOUSE_DISTRIBUTED_RESERVE_QUANTITIES[$item][$warehouse] = $data;
						else $_WAREHOUSE_DISTRIBUTED_QUANTITIES[$item][$warehouse] = $data;
					}
				}
			} 
			else {
				
				// delivered quantities are once confirmed
				// sum all confirmed quantities for and set not confiremd quantities as not confirmed
				if ($_REFERENCES[$item]['quantity']['distributed']) {
						
					// confirmed quantity
					if ($row['mps_ordersheet_item_delivered_confirmed']) {
						
						// pos quantities
						if ($pos) {
							$_POS_DISTRIBUTED_QUANTITIES[$item][$pos]['id'] = $id;
							$_POS_DISTRIBUTED_QUANTITIES[$item][$pos]['quantity'] = $_POS_DISTRIBUTED_QUANTITIES[$item][$pos]['quantity'] + $quantity;
						}
						
						// warehouse quantities, only last insertetd
						if ($warehouse && $quantity) {
							if ($stockreserve) $_WAREHOUSE_DISTRIBUTED_RESERVE_QUANTITIES[$item][$warehouse] = $data;
							elseif (!$_WAREHOUSE_DISTRIBUTED_QUANTITIES[$item][$warehouse]) $_WAREHOUSE_DISTRIBUTED_QUANTITIES[$item][$warehouse] = $data;
						}

					} else {
						
						// pos quantities
						if ($pos) {
							$_POS_DISTRIBUTED_QUANTITIES_NOT_CONFIRMED[$item][$pos] = $data;
						}
						
						// warehouse quantities
						if ($warehouse) {
							if ($stockreserve) $_WAREHOUSE_DISTRIBUTED_RESERVE_QUANTITIES_NOT_CONFIRMED[$item][$warehouse] = $data;
							elseif (!$_WAREHOUSE_DISTRIBUTED_QUANTITIES_NOT_CONFIRMED[$item][$warehouse]) $_WAREHOUSE_DISTRIBUTED_QUANTITIES_NOT_CONFIRMED[$item][$warehouse] = $data;
						}
					}
				}
				// not confirmed quantities
				else {
					
					// pos quantities
					if ($pos) {
						$_POS_DISTRIBUTED_QUANTITIES[$item][$pos] = $data;
					}
					
					// warehouse quantities
					if ($warehouse) {
						if ($stockreserve) $_WAREHOUSE_DISTRIBUTED_RESERVE_QUANTITIES[$item][$warehouse] = $data;
						elseif (!$_WAREHOUSE_DISTRIBUTED_QUANTITIES[$item][$warehouse]) $_WAREHOUSE_DISTRIBUTED_QUANTITIES[$item][$warehouse] = $data;
					}
				}
			}
		}
	}
}



// aadjusted quantities ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ADJUSTED_QUANTITIES = array();

$sth = $model->db->prepare("
	SELECT DISTINCT
		mps_ordersheet_items_adjusted_id,
		mps_ordersheet_items_adjusted_item_id,
		mps_ordersheet_items_adjusted_quantity,
		mps_ordersheet_items_adjusted_confirmed_quantity,
		mps_ordersheet_items_adjusted_confirmed_date
	FROM mps_ordersheet_items_adjusted
	INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_items_adjusted_item_id
	WHERE mps_ordersheet_item_ordersheet_id = ?
");

$sth->execute(array($ordersheet->id));
$result = $sth->fetchAll();

if ($result) {

	foreach ($result as $row) {
		
		$item = $row['mps_ordersheet_items_adjusted_item_id'];

		$_ADJUSTED_QUANTITIES[$item] = array(
			'id' => $row['mps_ordersheet_items_adjusted_id'],
			'quantity' => $row['mps_ordersheet_items_adjusted_quantity'],
			'confirmed' => $row['mps_ordersheet_items_adjusted_confirmed_quantity']
		);

		$_REFERENCES[$item]['quantity']['adjusted'] = $_ADJUSTED_QUANTITIES[$item]['confirmed'];
		
		$stockReserve = $_REFERENCES[$item]['quantity']['shipped'] - $_REFERENCES[$item]['quantity']['distributed'] + $_ADJUSTED_QUANTITIES[$item]['confirmed'] + $row['mps_ordersheet_items_adjusted_quantity'];

		if (!$_EXPORT_SAP && $stockReserve==0) {
			$_REFERENCES[$item]['state']['finished'] = true;
		}
	}

	$_HAS_ADJUSTED_QUANTITIS = count($_ADJUSTED_QUANTITIES);
}


// sap pos export warning messages :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_SAP_POS_BLOCKED = array();
$_SAP_POS_DELETED = array();
$_SAP_IMPORT_DATA = array();

// missing pos sap numbers
if ($state->onDistribution() && $_SAP_INVOLVED_POS && $_EXPORT_SAP) {

	// get sap import data
	$sth = $model->db->prepare("
		SELECT 
			TRIM(sap_imported_posaddress_sapnr) AS sap,
			REPLACE(LOWER(sap_imported_posaddress_zip), ' ', '') AS zip,
			REPLACE(LOWER(sap_imported_posaddress_distribution_channel), ' ', '') AS channel
		FROM db_retailnet.sap_imported_posaddresses
		WHERE sap_imported_posaddress_sapnr > 0  AND sap_imported_posaddress_date_checked IS NOT NULL
	");

	$sth->execute();
	$result = $sth->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$sap = $row['sap'];
			$zip = $row['zip'];
			$_SAP_IMPORT_DATA[$sap][$zip] = str_replace('x', '', $row['channel']);
		}
	}
	
	$sapInvolvedPos = join(',',array_keys($_SAP_INVOLVED_POS));
	
	$result = $model->query("
		SELECT DISTINCT
			posaddress_id,
			posaddress_sapnumber,
			posaddress_sap_shipto_number,
			address_sap_salesorganization,
			REPLACE(LOWER(mps_distchannel_code), ' ', '') AS channel,
    		REPLACE(LOWER(posaddress_zip), ' ', '') AS zip
		FROM db_retailnet.posaddresses
		INNER JOIN db_retailnet.addresses ON address_id = posaddress_client_id
		LEFT JOIN db_retailnet.mps_distchannels ON mps_distchannel_id = posaddress_distribution_channel
		WHERE posaddress_id IN($sapInvolvedPos)
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
				
			$message = null;

			$pos = $row['posaddress_id'];
			$mpsSapNumber = $row['posaddress_sapnumber'];
			
			$mpsDistChannel = str_replace('x', '', $row['channel']);
			$mpsZipNumber = $row['zip'];
	
			if (!$mpsSapNumber) {
				$message = "<i>The SAP customer code for this POS location is missing.</i>";
			}

			if (!$mpsDistChannel) {
				$message .= "<i>The Distribution channel for this POS location is missing.</i>";
			}

			// imported sap number
			$importedSapNumber = null;
			$importedSapChannel = null;

			if ($_SAP_IMPORT_DATA[$mpsSapNumber]) {

				if (count($_SAP_IMPORT_DATA[$mpsSapNumber])==1) {
					$importedSapNumber = $mpsSapNumber;
					$key = key($_SAP_IMPORT_DATA[$importedSapNumber]);
					$importedSapChannel = $_SAP_IMPORT_DATA[$importedSapNumber][$key];
				} else {

					foreach ($_SAP_IMPORT_DATA[$mpsSapNumber] as $importedZipNumber => $channel) {
						if ($mpsZipNumber==$importedZipNumber) {
							$importedSapNumber = $mpsSapNumber;
							$importedSapChannel = $channel;
						}
					}
				}

				if (!$importedSapNumber) {
					$message .= "<i>The SAP customer code does not match with SAP imported number.</i>";
				}

				if (!$importedSapChannel) {
					$message .= "<i>The distribution channel for this POS location is missing in SAP.</i>";
				} elseif ($mpsDistChannel <> $importedSapChannel) {
					$message .= "<i>The distribtion channels for this POS in SAP and in retail net don't match.</i>";
				}

			} else {
				//$message .= "<i>POS was never imported from SAP.</i>";
			}
	
			if (!$row['address_sap_salesorganization']) {
				$message .= "<i>The Sales Organisation for this Client Company is missing.</i>";
			}
				
			if ($message) {
				$message .= "<i>Export to SAP is not possible.</i>";
				$sapWarningPOS[$pos] = $message;
			}
		}
	}
	
	$result = $model->query("
		SELECT DISTINCT
			posaddress_id,
			sap_imported_posaddress_order_block AS order_block,
			sap_imported_posaddress_order_block_sales_area AS order_block_sales_area,
			sap_imported_posaddress_deletion_indicator AS deletion_indicator,
			sap_imported_posaddress_deletion_sales_area_indicator AS deletion_sales_area_indicator
		FROM db_retailnet.sap_imported_posaddresses
		INNER JOIN db_retailnet.posaddresses ON posaddress_sapnumber = sap_imported_posaddress_sapnr
		WHERE posaddress_id IN($sapInvolvedPos)
		ORDER BY posaddress_id DESC
	")->fetchAll();
	
	if ($result) {

		$controlled = array();

		foreach ($result as $row) {
			
			$pos = $row['posaddress_id'];				
			
			if (!$controlled[$pos]) {
				
				$controlled[$pos] = true;

				if ($row['order_block'] || $row['order_block_sales_area'] ) {
					$_SAP_POS_BLOCKED[$pos] = 'The POS is blocked for orders in SAP.<br \>Export to SAP is not possible.';
				}
				
				if ($row['deletion_indicator'] || $row['deletion_sales_area_indicator'] ) {
					$_SAP_POS_DELETED[$pos] = 'This POS is closed in SAP..<br \>Export to SAP is not possible.';
				}
			}
		}
	}
	
}


// get warehouses ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_WAREHOUSES = array();

$sth = $model->db->prepare("
	SELECT 
		mps_ordersheet_warehouses.mps_ordersheet_warehouse_id AS id,  
		db_retailnet.address_warehouses.address_warehouse_id AS warehouse,  
		db_retailnet.address_warehouses.address_warehouse_name AS name, 
		db_retailnet.address_warehouses.address_warehouse_sap_customer_number AS sap, 
		db_retailnet.address_warehouses.address_warehouse_sap_shipto_number AS shipto
	FROM mps_ordersheet_warehouses 
	INNER JOIN db_retailnet.address_warehouses ON mps_ordersheet_warehouses.mps_ordersheet_warehouse_address_warehouse_id = db_retailnet.address_warehouses.address_warehouse_id
	WHERE mps_ordersheet_warehouses.mps_ordersheet_warehouse_stock_reserve != 1 
	AND db_retailnet.address_warehouses.address_warehouse_active = 1
	AND mps_ordersheet_warehouses.mps_ordersheet_warehouse_ordersheet_id = ?
");

$sth->execute(array($ordersheet->id));
$result = $sth->fetchAll();
$_WAREHOUSES = _array::datagrid($result);

	
// warehouse sap failures ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_SAP_WAREHOUSE_FAILURE = array();
$_WAREHOUSES_ALERTS = array();

if ($state->onDistribution() && $_EXPORT_SAP && $_WAREHOUSES) {
	
	$result = $model->query("
		SELECT DISTINCT
			mps_ordersheet_warehouse_id,
			CONCAT('The SAP customer code for warehouse <b>', address_warehouse_name,'</b> is missing.<br>Export to SAP is not possible.') as message
		FROM mps_ordersheet_warehouses
		INNER JOIN db_retailnet.address_warehouses ON address_warehouse_id = mps_ordersheet_warehouse_address_warehouse_id
		WHERE address_warehouse_address_id = $company->id
		AND mps_ordersheet_warehouse_stock_reserve <> 1 
		AND (address_warehouse_sap_customer_number IS NULL OR address_warehouse_sap_customer_number = '')
	")->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$key = $row['mps_ordersheet_warehouse_id'];
			$_WAREHOUSES_ALERTS[$key] .= "<i>The SAP customer code for this warehouse is missing.</i>";
		}
	}

	// warehouses sap order types
	$result = $model->query("
		SELECT DISTINCT
			mps_ordersheet_warehouse_id, 
			db_retailnet.sap_order_types.sap_order_type_name
		FROM mps_ordersheet_warehouses 
		INNER JOIN db_retailnet.address_warehouses ON mps_ordersheet_warehouses.mps_ordersheet_warehouse_address_warehouse_id = db_retailnet.address_warehouses.address_warehouse_id
		INNER JOIN db_retailnet.sap_matrix_ordertypes ON db_retailnet.address_warehouses.address_warehouse_address_id = db_retailnet.sap_matrix_ordertypes.sap_matrix_ordertype_address_id
		INNER JOIN db_retailnet.mps_distchannels ON db_retailnet.mps_distchannels.mps_distchannel_id = db_retailnet.sap_matrix_ordertypes.sap_matrix_ordertype_channel_id
		INNER JOIN db_retailnet.sap_order_types ON db_retailnet.sap_matrix_ordertypes.sap_matrix_ordertype_type_id = db_retailnet.sap_order_types.sap_order_type_id
		WHERE mps_ordersheet_warehouse_stock_reserve != 1 
		AND mps_distchannel_use_for_warehouses_export_to_sap = 1
		AND address_warehouse_address_id = $company->id
	")->fetchAll();

	$_SAP_WAREHOUSE_ORDERTYPES = _array::extract($result);

	foreach ($_WAREHOUSES as $key => $row) {
		if (!$_SAP_WAREHOUSE_ORDERTYPES[$key]) {
			$_WAREHOUSES_ALERTS[$key] .= "<i>The SAP order type for this warehouse is missing.</i>";
		}
	}
}



// sap failure orders ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_SAP_SALES_ORDERS = array();
$_SAP_FAILURE_ITEMS = array();	
$_SAP_CONFIRMED_ITEMS = array();	

if ($_ITEMS_CONFIRMED) {

	$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);

	$invlvedItems = join(',', array_keys($_ITEMS_CONFIRMED));
	$division = '08';

	$result = $exchange->query("
		SELECT 
			sap_salesorder_item_id AS id,
			sap_salesorder_item_mps_item_id AS item,
			sap_salesorder_item_mps_po_number AS pon,
			sap_salesorder_item_ean_number AS ean,
			sap_salesorder_itemr_quantity AS quantity
		FROM sap_salesorder_items
		WHERE sap_salesorder_item_mps_item_id IN ($invlvedItems) 
		AND sap_salesorder_item_division = '$division' 
		AND sap_salesorder_item_fetched_by_sap IS NOT NULL
	")->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$pon = $row['pon'];
			$ean = $row['ean'];
			$_SAP_SALES_ORDERS[$pon][$ean]['id'] = $row['id'];
			$_SAP_SALES_ORDERS[$pon][$ean]['item'] = $row['item'];
			$_SAP_SALES_ORDERS[$pon][$ean]['quantity'] = $row['quantity'];
		}
	}

	$pons = "'".join("', '", array_keys($_SAP_SALES_ORDERS))."'";

	$result = $exchange->query("
		SELECT 
			sap_confirmed_item_mps_po_number AS pon,
			sap_confirmed_item_ean_number AS ean,
			sap_confirmed_item_confirmed_quantity AS quantity
		FROM sap_confirmed_items
		WHERE sap_confirmed_item_mps_po_number IN ($pons) 
		AND sap_confirmed_item_division = '$division'
		AND sap_confirmed_item_imported_in_mps IS NOT NULL
	")->fetchAll();

	if ($result) {

		foreach ($result as $row) {
			
			$pon = $row['pon'];
			$ean = $row['ean'];

			// order
			$order = $_SAP_SALES_ORDERS[$pon][$ean] ?: array();
			$distributionID = $order['item'];
			$item = $_ITEMS_CONFIRMED[$distributionID]['mps_ordersheet_item_delivered_ordersheet_item_id'];

			if ($order && $row['quantity'] < $order['quantity'])  {
				$_SAP_FAILURE_ITEMS[$item][$distributionID] = $order['quantity'] - $row['quantity'];
			}

			$_SAP_CONFIRMED_ITEMS[$item][$distributionID]['ordered'] = $order['quantity'];
			$_SAP_CONFIRMED_ITEMS[$item][$distributionID]['confirmed'] = $row['quantity']; 
		}
	}
}




//	spreadsheet references :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_DATAGRID = array();
$_ROW_SEPARATORS = array();
$_COL_SEPARATORS = array();

$_FIRST_FIXED_ROW = 2;
$_FIRST_FIXED_COL = 1;

// rows
$_ROW_BLANK = 1;
$_ROW_FIRST = 2;
$_ROW_REFERENCE_GROUP = 2;
$_ROW_REFERENCE = 3;
$_ROW_REFERENCE_QUANTITY = 4; 
$_ROW_PLANNED_QUANTITY = 5; 
$_ROW_SHIPPED_QUANTITY = $_MODE_DISTRIBUTION ? 5 : null;
$_ROW_DISTRIBUTED_QUANTITY = $_MODE_DISTRIBUTION ? 6 : null;
$_ROW_STOCK_RESERVE = $_STATE_APPROVED && $_MODE_PLANNING ? 6 : 7;
$_ROW_STOCK_RESERVE = $_STATE_DISTRIBUTED ? null : $_ROW_STOCK_RESERVE;
$_TOTAL_FIXED_ROWS = $_ROW_STOCK_RESERVE ? $_ROW_STOCK_RESERVE : $_ROW_DISTRIBUTED_QUANTITY;
$_LAST_FIXED_ROW = $_TOTAL_FIXED_ROWS;

// cols
$_FIRST_COL = 1;
$_COL_POS_ICON = 1;
$_COL_POS_CAPTION = 2;
$_COL_SAP_NUMBER = 3;
$_COL_SHIPTO_NUMBER = 4;
$_COL_SEPARATORS[] = 5;
$_TOTAL_FIXED_COLS = 5;
$_LAST_FIXED_COL = $_TOTAL_FIXED_COLS;

$_COLSPAN_CALCULLATED_CAPTIONS = 4;
$_COLSPAN_POSTYPE_CAPTION = 4;


// spreadsheet headers :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_DATAGRID[$_ROW_REFERENCE_QUANTITY][$_COL_POS_CAPTION] = array(
	'value' => $_MODE_DISTRIBUTION ? 'Total Planned' : 'Total Quantity',
	'cls' => 'calcullated-caption',
	'merge' => $_COLSPAN_CALCULLATED_CAPTIONS
);

$_DATAGRID[$_ROW_DISTRIBUTED_QUANTITY][$_COL_POS_CAPTION] = array(
	'value' => $_EXPORT_SAP && $_HAS_ADJUSTED_QUANTITIS ? 'Total Stock Adjustment / Distributed' : 'Distributed',
	'cls' => 'calcullated-caption',
	'merge' => $_COLSPAN_CALCULLATED_CAPTIONS
);

$_DATAGRID[$_ROW_PLANNED_QUANTITY][$_COL_POS_CAPTION] = array(
	'value' => $_MODE_DISTRIBUTION ? 'Total Shipped' : 'Total Planned',
	'cls' => 'calcullated-caption',
	'merge' => $_COLSPAN_CALCULLATED_CAPTIONS
);

// row stock reserve [7,2]
if ($_ROW_STOCK_RESERVE) {	
	
	if ($_MODE_ARCHIVE) {
		$stockResereveCaption = $_EXPORT_SAP ? 'Total Stock Adjustment / Distributed' : 'Distributed';
	} else {
		if ($state->isShipped() || $state->isPartiallyShipped()) $stockResereveCaption = 'Stock Reserve';
		else $stockResereveCaption = $_EXPORT_SAP ? 'Stock Adjustment / Total Stock Adjustment / Stock Reserve' : 'Stock Reserve';
	}

	$_DATAGRID[$_ROW_STOCK_RESERVE][$_COL_POS_CAPTION] = array(
		'value' => $stockResereveCaption,
		'cls' => 'calcullated-caption',
		'merge' => $_COLSPAN_CALCULLATED_CAPTIONS
	);
}

//	item captions ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_ITEMS) {

	$totalItemColumns = 0;
	$separator = 1;
	
	$col = $_TOTAL_FIXED_COLS;
						
	foreach ($_ITEMS as $group_id => $group) {

		// group caption
		$groupCaption = htmlspecialchars($group['caption'], ENT_QUOTES);

		foreach ($group['data'] as $subgroup_id => $subgroup) {
			
			// count subgroup items
			$totalSubgroupColumns = 0;

			// subgroup caption
			$subgroupCaption = htmlspecialchars($subgroup['caption'], ENT_QUOTES);

			// group index
			$col++;
			$_DATAGRID[$_ROW_REFERENCE_GROUP][$col] = array(
				'id' => $group_id,
				'subgroup' => $subgroup_id,
				'value' => "<strong>$groupCaption</strong><span>$subgroupCaption</span>",
				'cls' => 'col-caption'
			);

			$index = $col;
			
			foreach ($subgroup['data'] as $id => $item) {
				
				// planned or approved quantities
				$totalSubgroupColumns++;

				$ico = "<span class=\"icon-holder left\" ><span class=\"icon info-full-circle infotip \" rel=\"material-info\" data=\"{$item[item]}\" ></span></span>";
				//$icoRefersh = "<span class=\"fa-stack\"><i class=\"fa fa-circle fa-stack-2x\"></i><i class=\"fa fa-refresh fa-stack-1x fa-inverse\"></i></span>";
				//$resetIco = $_EXPORT_SAP && $_SAP_FAILURE_ITEMS[$id] ? "<span class=\"icon-holder right export-reset\" title=\"Reset exported quantities\" data-item=\"$id\" >$icoRefersh</span>" : null;
				
				// column planned quantties
				$_DATAGRID[$_ROW_REFERENCE][$col] = array(
					'id' => $id,
					'value' => "$ico<span class=\"code\">{$item[code]}</span>$resetIco",
					'cls' => 'item-caption',
					'column' => COLUMN_PLANNING
				);

				if ($_MODE_DISTRIBUTION) {
					
					$col++;
					$totalSubgroupColumns++;
					
					// column distributed quantitites
					$_DATAGRID[$_ROW_REFERENCE][$col]['id'] = $id;
					$_DATAGRID[$_ROW_REFERENCE][$col]['column'] = COLUMN_DISTRIBUTION;
					$_DATAGRID[$_ROW_REFERENCE][$col-1]['merge'] = 2;
				}

				// item has distributed quantities and  has available stock reserve
				// partially distribution colum
				if ($_REFERENCES[$id]['quantity']['distributed'] && (!$_REFERENCES[$id]['state']['finished'] || $_REFERENCES[$id]['state']['partially'] ) ) {

					$col++;
					$totalSubgroupColumns++;
						
					// column partially distributed quantities
					$_DATAGRID[$_ROW_REFERENCE][$col]['id'] = $id;
					$_DATAGRID[$_ROW_REFERENCE][$col]['column'] = COLUMN_PARTIALY_CONFIRMED;
					$_DATAGRID[$_ROW_REFERENCE][$col-2]['merge'] = 3;
				}

				$col++;
			}

			if ($totalSubgroupColumns > 1) {
				$_DATAGRID[$_ROW_REFERENCE_GROUP][$index]['merge'] = $totalSubgroupColumns;
			}

			$col--;
			$col = $col + $separator;

			$totalItemColumns = $totalItemColumns  + $totalSubgroupColumns + $separator;
		}
	}

	// set total spreadsheet columns
	$totalItemColumns = $separator ? $totalItemColumns - $separator : $totalItemColumns;
}

// items area triggers
$_FIRST_ITEM_COL = $_TOTAL_FIXED_COLS + 1;
$_LAST_ITEM_COL = $_TOTAL_FIXED_COLS + $totalItemColumns;


// spreadsheet pos locations area ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


$seprow = 0;
$totalPosRows = 0;

if ($_POS_LOCATIONS) {

	$row = $_TOTAL_FIXED_ROWS;

	foreach ($_POS_LOCATIONS as $type_id => $types) {

		$row++;
		$totalPosRows++;

		// pos type toggle trigger
		$_DATAGRID[$row][$_COL_POS_ICON] = array(
			'type' => $type_id,
			'value' => "<span class=\"arrow arrow_$type_id arrow-down type_$type_id\"  data-type=\"$type_id\" data-row=\"$row\" ></span>",
			'cls' => 'cel postype row-group'
		);

		// caption pos type name
		$_DATAGRID[$row][$_COL_POS_CAPTION] = array(
			'type' => $type_id,
			'value' => $types['name'],
			'cls' => 'cel postype row-group'
		);


		// caption pos sap number
		$_DATAGRID[$row][$_COL_SAP_NUMBER] = array(
			'value' => 'SAP No.',
			'cls' => 'cel row-group'
		);

		// caption pos shipto number
		$_DATAGRID[$row][$_COL_SHIPTO_NUMBER] = array(
			'value' => 'Shipto No.',
			'cls' => 'cel row-group'
		);

		foreach ($types['data'] as $pos_id => $pos) {
			
			$row++;
			$totalPosRows++;

			$warningIcon = null;

			$_DATAGRID[$row][$_COL_POS_ICON] = array(
				'id' => $pos_id,
				'value' => "<span class=\"icon info-full-circle infotip \" rel=\"pos-info\" data=\"$pos_id\" ></span>",
				'cls' => 'cel postype'
			);

			if ($_SAP_POS_DELETED[$pos_id]) $warningIcon = "<i class=\"fa fa-ban infotip warrning\" data-title=\"{$_SAP_POS_DELETED[$pos_id]}\"></i>";
			elseif ($_SAP_POS_BLOCKED[$pos_id]) $warningIcon = "<i class=\"fa fa-ban infotip warrning\" data-title=\"{$_SAP_POS_BLOCKED[$pos_id]}\"></i>";
			elseif ($sapWarningPOS[$pos_id]) $warningIcon = "<i class=\"fa fa-exclamation-triangle infotip warrning\" data-title=\"{$sapWarningPOS[$pos_id]}\"></i>";

			$strlen = $warningIcon ? 40 : 45;
			$caption = $pos['name'];
			$caption = strlen($caption)>$strlen ? substr($caption, 0, $strlen).'..' : $caption;
			$caption = htmlspecialchars($caption, ENT_QUOTES);

			// pos name
			$_DATAGRID[$row][$_COL_POS_CAPTION] = array(
				'id' => $pos_id,
				'value' => "<span class=\"caption type_$type_id\" data-type=\"$type_id\" data-row=\"$row\" >$caption</span> $warningIcon",
				'cls' => 'cel postype',
				'sap-readonly' => $warningIcon ? true : false
			);


			// pos sap no.
			$_DATAGRID[$row][$_COL_SAP_NUMBER] = array(
				'value' => $pos['sap'],
				'cls' => 'cel',
			);

			// pos sap no.
			$_DATAGRID[$row][$_COL_SHIPTO_NUMBER] = array(
				'value' => $pos['shipto'],
				'cls' => 'cel',
			);
		}

		$row = $row + $seprow;
	}

	$row = $seprow ? $row-$seprow : $row;
}

// pos area triggers
$_FIRST_POS_ROW = $_TOTAL_FIXED_ROWS+1;
$_LAST_POS_ROW = $row;


// spreadsheet warehouses rows :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$separea = 0;
$totalWarehouseRows = 0;

if ($_AREA_WAREHOUSE) {

	$row = $_TOTAL_FIXED_ROWS + $totalPosRows + $separea + 1;
	
	$_ROW_WAREHOUSE_CAPTION = $row;

	$totalWarehouseRows++;

	if ($_STATE_EDIT) {

		$_DATAGRID[$row][$_COL_POS_ICON] = array(
			'value' => "<a id=\"add_warehouse\" class=\"icon add\" href=\"#add_warehouse_dialog\" ></a>",
			'cls' => 'cel row-group'
		);

		$_DATAGRID[$row][$_COL_POS_CAPTION] = array(
			'value' => 'Warehouses',
			'cls' => 'cel row-group',
			'merge' => 2
		);

	} else {

		$_DATAGRID[$row][$_COL_POS_ICON] = array(
			'value' => 'Warehouses',
			'merge' => 3,
			'cls' => 'cel row-group'
		);
	}

	// caption warehouse sap number
	$_DATAGRID[$row][$_COL_SAP_NUMBER] = array(
		'value' => 'SAP No.',
		'cls' => 'cel row-group'
	);

	// caption warehouse shipto number
	$_DATAGRID[$row][$_COL_SHIPTO_NUMBER] = array(
		'value' => 'Shipto No.',
		'cls' => 'cel row-group'
	);

	if ($_WAREHOUSES) {

		$_FIRST_WAREHOUSE_ROW = $row+1;

		foreach ($_WAREHOUSES as $key => $warehouse) {
			
			//$warehouse = $value['mps_ordersheet_warehouse_address_warehouse_id'];
			//$key = $value['mps_ordersheet_warehouse_id'];

			$row++;
			$totalWarehouseRows++;

			$warningIcon = null;
			$readonly = false;

			// warning icon
			//$warningIcon = ($_SAP_WAREHOUSE_FAILURE[$key]) ? "<i class=\"fa fa-exclamation-triangle infotip warrning\" data-title=\"{$_SAP_WAREHOUSE_FAILURE[$key]}\"></i>" : null;

			if ($_WAREHOUSES_ALERTS[$key]) {
				$msg = $_WAREHOUSES_ALERTS[$key]."Export to SAP is not possible.";
				$warningIcon = "<i class=\"fa fa-exclamation-triangle infotip warrning\" data-title=\"$msg\"></i>";
				$readonly = true;
			}

			// w<arehose name
			$strlen = $warningIcon ? 40 : 45;
			$warehouseName = str_replace("'", "", $warehouse['name']);
			$warehouseName = strlen($warehouseName)>$strlen ? substr($warehouseName, 0, $strlen).'..' : $warehouseName;

			if ($_STATE_EDIT) {

				$_DATAGRID[$row][$_COL_POS_ICON] = array(
					'id' =>  $key,
					'value' => "<span class=\"icon cancel remove-warehouse\" data-row=\"$row\" data-warehouse=\"$key\"  ></span>",
					'cls' => 'cel warehouse'
				);

				$_DATAGRID[$row][$_COL_POS_CAPTION] = array(
					'id' =>  $key,
					'value' => $warehouseName.$warningIcon,
					'cls' => 'cel warehouse',
					'merge' => 2
				);

			} else {
				$_DATAGRID[$row][$_COL_POS_ICON] = array(
					'id' =>  $key,
					'value' => $warehouseName.$warningIcon,
					'merge' => 3,
					'cls' => 'cel warehouse'
				);
			}

			// caption warehouse sap number
			$_DATAGRID[$row][$_COL_SAP_NUMBER] = array(
				'value' => $warehouse['sap'],
				'cls' => 'cel'
			);

			// caption warehouse shipto number
			$_DATAGRID[$row][$_COL_SHIPTO_NUMBER] = array(
				'value' => $warehouse['shipto'],
				'cls' => 'cel'
			);
		}

		// last warehouse row
		$_LAST_WAREHOUSE_ROW = $row;
	}
}
	
// spreadsheet builder :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$totalSpreadsheetRows = $_TOTAL_FIXED_ROWS + $totalPosRows + $totalWarehouseRows;
$totalSpreadsheetCols = $_TOTAL_FIXED_COLS + $totalItemColumns;
$_LAST_SPREADSHEET_ROW = $totalSpreadsheetRows;
$_LAST_SPREADSHEET_COL = $totalSpreadsheetCols;
	

$grid = array();
$mergecel = array();

if ($totalSpreadsheetRows && $totalSpreadsheetCols) {

	for ($row=1; $row <= $totalSpreadsheetRows; $row++) {

		$_SAP_ROW_READONLY = false;
			
		for ($col=1; $col <= $totalSpreadsheetCols; $col++) {

			// index
			$index = spreadsheet::key($col, $row);

			// merge cel index
			if ($_DATAGRID[$row][$col]['merge']) {
				$mergecel[$index] = $_DATAGRID[$row][$col]['merge'];
			}

			switch ($row) {
				
				case $_ROW_BLANK ;
					if ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {
						$grid[$index]['cls'] = $_DATAGRID[$_ROW_REFERENCE][$col]['id'] ? null : 'col-separator';
					}
				break;
				
				case $_ROW_REFERENCE_GROUP:		// PLANNING TYPE CAPTION
				case $_ROW_REFERENCE:			// MATERIAL CAPTION

					if ($col==$_COL_POS_CAPTION) {
						/*
						$mergecel[$index] = $total_fixed_columns;
						$grid[$index]['html'] = ($filerCaptions) ? 'Filter(s): '.join(', ', $filerCaptions) : null;
						$grid[$index]['cls'] = "filters";
						*/
					}
					elseif ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {
						
						$item = $_DATAGRID[$_ROW_REFERENCE][$col]['id'];

						if ($item) {

							$cls = " cel";
							
							if ($_DATAGRID[$row][$col]['value']) {
								$grid[$index]['html'] = $_DATAGRID[$row][$col]['value'];
							}
						
							$grid[$index]['cls'] = $_DATAGRID[$row][$col]['cls'].$cls;
						}
					}
						
				break;

				// ITEM QUANTITY OVER ALL POS
				case $_ROW_REFERENCE_QUANTITY:

					if ($col==$_COL_POS_CAPTION) {

						$grid[$index]['text'] = $_DATAGRID[$row][$col]['value'];
						$grid[$index]['cls'] = $_DATAGRID[$row][$col]['cls'];

					} elseif ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {

						$item = $_DATAGRID[$_ROW_REFERENCE][$col]['id'];

						if ($item) {

							$column = $_DATAGRID[$_ROW_REFERENCE][$col]['column'];

							// COLUMN PLANNING
							if ($column==COLUMN_PLANNING) {

								$cls = " cel";

								if ($_MODE_PLANNING) {
									$cls .= ' calculated';
									$celTotalPlanned = spreadsheet::key($col, $_ROW_PLANNED_QUANTITY);
									$celStockReserve = spreadsheet::key($col, $_ROW_STOCK_RESERVE);
									$grid[$index]['text'] = "function($celTotalPlanned, $celStockReserve) { return $celTotalPlanned+$celStockReserve || null; }"; // output: total planned + stock reserve
								}

								if ($_MODE_DISTRIBUTION) {
									$cls .= ' planned-readonly';
									$grid[$index]['text'] = $_REFERENCES[$item]['quantity']['approved'];
								}

								$grid[$index]['cls'] = $_DATAGRID[$row][$col]['cls'].$cls;

							} // END COLUMN PLANNING
						}
					}

				break;

				case $_ROW_PLANNED_QUANTITY : // mode planning: total planned quantity (function)

					if ($col==$_COL_POS_CAPTION) {
						$grid[$index]['text'] = $_DATAGRID[$row][$col]['value'];
						$grid[$index]['cls'] = $_DATAGRID[$row][$col]['cls'];
					}
					elseif ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {
						
						$item = $_DATAGRID[$_ROW_REFERENCE][$col]['id'];

						if ($item) {

							$column = $_DATAGRID[$_ROW_REFERENCE][$col]['column'];
							
							// COLUMN PLANNING
							if ($column==COLUMN_PLANNING) {

								$cls = " cel";
									
								if ($_MODE_PLANNING) {
									$range = spreadsheet::key($col,$_FIRST_POS_ROW).'_'.spreadsheet::key($col, $_LAST_SPREADSHEET_ROW);
									$grid[$index]['text']  = "function($range) { return sum($range) || null }"; // sum planned quantities (pos + warehouses)
								}

								if ($_MODE_DISTRIBUTION) {
									$cls .= ' planned-readonly';
									$grid[$index]['text'] = $_REFERENCES[$item]['quantity']['shipped'];
								}

								$grid[$index]['cls'] = $_DATAGRID[$row][$col]['cls'].$cls;

							} // END COLUMN PLANNING
						}
					}

				break;

				case $_ROW_DISTRIBUTED_QUANTITY:

					if ($col==$_COL_POS_CAPTION) {
						$grid[$index]['text'] = $_DATAGRID[$row][$col]['value'];
						$grid[$index]['cls'] = $_DATAGRID[$row][$col]['cls'];
					}
					elseif ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {
						
						$item = $_DATAGRID[$_ROW_REFERENCE][$col]['id'];

						if ($item) {

							$cls = null; 
							$column = $_DATAGRID[$_ROW_REFERENCE][$col]['column'];

							if ($_MODE_ARCHIVE && $column==COLUMN_PLANNING) {
								$cls = "cel cel-adjustment";
								$grid[$index]['text'] = $_ADJUSTED_QUANTITIES[$item] ? $_ADJUSTED_QUANTITIES[$item]['confirmed'] : null;
								$grid[$index]['readonly'] = false;
							}

							// COLUMN DISTRIBUTION
							if ($column==COLUMN_DISTRIBUTION && $_REFERENCES[$item]['quantity']['distributed']) {
								
								$cls = " cel";

								$value = $_REFERENCES[$item]['quantity']['distributed'];
								$grid[$index]['text'] = $_REFERENCES[$item]['quantity']['distributed'];

								if ($_REFERENCES[$item]['quantity']['distributed'] > 0) {
									$cls .= ' delivered-readonly';
								}

							} // END COLUMN DISTRIBUTION

							$grid[$index]['cls'] = $_DATAGRID[$row][$col]['cls'].$cls;
						}

					}
					
				break;

				case $_ROW_STOCK_RESERVE :

					// stock reserve caption
					if ($col==$_COL_POS_CAPTION) {
						$grid[$index]['text'] = $_DATAGRID[$row][$col]['value'];
						$grid[$index]['cls'] = $_DATAGRID[$row][$col]['cls'];
					}
					elseif ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {
						
						$item = $_DATAGRID[$_ROW_REFERENCE][$col]['id'];
						$material = $_REFERENCES[$item]['material']['id'];

						if ($item) {

							// references
							$column = $_DATAGRID[$_ROW_REFERENCE][$col]['column'];
							$warehouse = $_WAREHOUSE_STOCK_RESERVE;
							$quantity = $_WARWHOUSE_RESERVE_QUANTITIES[$item][$warehouse]['quantity'] ?: null;

							$cls = null;

							// COLUMN PLANNING
							if ($column==COLUMN_PLANNING) {

								if ($_MODE_PLANNING) {

									$cls = " cel";

									$cls .= ' reserve';
	
									if ($_STOCK_RESERVE_EDIT) {;
										
										$grid[$index]['text'] = $quantity;

										// tag pattern: section;quantitty_id;warehouse_id;material_id
										$material = $_REFERENCES[$item]['material']['id'];
										$id = $_WARWHOUSE_RESERVE_QUANTITIES[$item][$warehouse]['id'];
										$grid[$index]['tag'] = "reserve-planned;$id;$warehouse;$material";

									} else {

										if ($_STATE_APPROVED) {
											// stock reserve: total approved quantites - sum of planned pos quantities
											$approved = $_REFERENCES[$item]['quantity']['approved'];
											$cel = spreadsheet::key($col,$_ROW_PLANNED_QUANTITY);
											$grid[$index]['text'] = "function($cel) { return $approved-$cel || null; }";
										} else {
											$grid[$index]['text'] = $quantity;
										}
										
										$grid[$index]['readonly'] = CELL_READONLY;
									}
								}

								if ($_MODE_DISTRIBUTION && $_EXPORT_SAP && $_REFERENCES[$item]['quantity']['distributed'] && !$_REFERENCES[$item]['state']['finished']) {
										
									$cls = "cel cel-adjustment";
									$adjusted = $_ADJUSTED_QUANTITIES[$item] ?: array();
									$grid[$index]['text'] = $adjusted['quantity'] ?: null;
									$grid[$index]['readonly'] = false;

									// TAG: [section] [item] [pos] [material]
									$aid = $adjusted['id'];
									$celSum= spreadsheet::key($col+2, $row);
									$grid[$index]['tag'] = "adjusted;$aid;$celSum;$material";
								}

							} // END COLUMN PLANNING


							// COLUMN DISTRIBUTION
							if ($column == COLUMN_DISTRIBUTION) {

								$shipped = $_REFERENCES[$item]['quantity']['shipped'];
								$range = spreadsheet::key($col,$_FIRST_POS_ROW).'_'.spreadsheet::key($col, $_LAST_SPREADSHEET_ROW);

								if (!$_REFERENCES[$item]['quantity']['distributed']) {
									$cls = " cel";									
									$grid[$index]['text']  = "function($range) {
										return $shipped-sum($range) || null; 
									}";
								}
								elseif ($_EXPORT_SAP && $_ADJUSTED_QUANTITIES[$item]['confirmed']) {
									$cls = "cel cel-adjustment-total";
									$grid[$index]['text'] = $_ADJUSTED_QUANTITIES[$item]['confirmed'];
								}
								
							} // END COLUMN DISTRIBUTION


							// COLUMN PARTIALLY DISTRIBUTION
							if ($column == COLUMN_PARTIALY_CONFIRMED) {

								$cls = " cel";

								$availableStock = $_REFERENCES[$item]['quantity']['shipped'] - $_REFERENCES[$item]['quantity']['distributed'];

								if ($_EXPORT_SAP) {
									$celAdjusted = spreadsheet::key($col-2, $row);
									$celTotalAdjusted = spreadsheet::key($col-1, $row);
									$range = spreadsheet::key($col,$_FIRST_POS_ROW).'_'.spreadsheet::key($col, $_LAST_SPREADSHEET_ROW);
									$grid[$index]['text']  = "function($range, $celAdjusted, $celTotalAdjusted) { 
										var total = $availableStock - sum($range) + $celAdjusted + $celTotalAdjusted;
										return total || null;
									}";
								} else {
									$range = spreadsheet::key($col,$_FIRST_POS_ROW).'_'.spreadsheet::key($col, $_LAST_SPREADSHEET_ROW);
									$grid[$index]['text']  = "function($range) {return $availableStock-sum($range) || null; } ";
								}

							} // END COLUMN PARTIALLY DISTRIBUTION

							$grid[$index]['cls'] = $_DATAGRID[$row][$col]['cls'].$cls;

						}
					}

				break;

				case $row >= $_FIRST_POS_ROW && $row <= $_LAST_POS_ROW:

					if ($col < $_FIRST_ITEM_COL) {

						$grid[$index]['html'] = $_DATAGRID[$row][$col]['value'];
						$grid[$index]['cls'] = $_DATAGRID[$row][$col]['cls'];

						if ($_DATAGRID[$row][$col]['sap-readonly']) $_SAP_ROW_READONLY = true;

						$readonly = CELL_READONLY;

					} elseif ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {
						
						$readonly = false;

						// references
						$item = $_DATAGRID[$_ROW_REFERENCE][$col]['id'];
						$pos = $_DATAGRID[$row][$_COL_POS_CAPTION]['id'];

						if ($item && $pos) {

							$column = $_DATAGRID[$_ROW_REFERENCE][$col]['column'];
							$nextColumn = $_DATAGRID[$_ROW_REFERENCE][$col+1]['column'];
							$material = $_REFERENCES[$item]['material']['id'];
							
							$cls = " cel";

							$nextColumn = $_DATAGRID[$_ROW_REFERENCE][$col+1]['column'];

							// cel attribtes
							$grid[$index]['hiddenAsNull'] = 'true';
							$grid[$index]['numeric'] = 'true';


							// COLUMN PLANNING
							if ($column == COLUMN_PLANNING) {

								$grid[$index]['text'] = $_POS_PLANNED_QUANTITES[$item][$pos]['quantity'];

								if ($_MODE_PLANNING) { 

									if ($_STATE_EDIT) {

										if ($_VALIDATA_TOTAL_ITEM_QUANTITY) {
											$celReserve = spreadsheet::key($col, $_ROW_STOCK_RESERVE);
										}
										
										// TAG: [section] [item] [pos] [material] [total] [used]
										$id = $_POS_PLANNED_QUANTITES[$item][$pos]['id'];
										$grid[$index]['tag'] = "$column;$id;$pos;$material;$celReserve";

									} else {
										$readonly = CELL_READONLY;
									}
								}

								if ($_MODE_DISTRIBUTION) {
									$readonly = CELL_READONLY;
									$cls .= ' planned-readonly';
								}

							} // END COLUMN PLANNING
							

							// COLUMN DISTRIBUTION
							if ($column == COLUMN_DISTRIBUTION) {

								if( $_VERSION || $_REFERENCES[$item]['state']['finished']) {
									
									$cls .= ' delivered-readonly ';
									$readonly = CELL_READONLY;
									
									$quantity = $_VERSION ? $DistributedVersionQuantity[$item][$pos] : $_POS_DISTRIBUTED_QUANTITIES[$item][$pos]['quantity'];
									$grid[$index]['text'] = $quantity;
								} 
								else {

									$grid[$index]['text'] = $_POS_DISTRIBUTED_QUANTITIES[$item][$pos]['quantity'];

									if (!$_SAP_ROW_READONLY && $_STATE_EDIT) {
										
										if ($_VALIDATA_TOTAL_ITEM_QUANTITY) {
											$celReserve = spreadsheet::key($col, $_ROW_STOCK_RESERVE);
										}
									
										// TAG: [section] [item] [pos] [material] [total] [planned] [reserve]
										$id = $_POS_DISTRIBUTED_QUANTITIES[$item][$pos]['id'];
										$grid[$index]['tag'] = "$column;$id;$pos;$material;$celReserve";

										$readonly = $_DATAGRID[$_ROW_REFERENCE][$col+1]['column']==COLUMN_PARTIALY_CONFIRMED ? CELL_READONLY : false;

									} else {
										$readonly = CELL_READONLY;
									}

									 if ($nextColumn == COLUMN_PARTIALY_CONFIRMED) {
									 	$cls .= ' delivered-readonly';
									 }
								}

								$sapFailureClass = null;

								if ($_SAP_FAILURE_ITEMS[$item]) {
									$id = $_POS_DISTRIBUTED_QUANTITIES[$item][$pos]['id'];
									$cls .= $_SAP_FAILURE_ITEMS[$item][$id] ? ' sap-failure' : null;
								}

							} // END COLUMN DISTRIBUTION
							

							// COLUMN PARTIALLY DISTRIBUTION
							if ($column==COLUMN_PARTIALY_CONFIRMED) {

								$grid[$index]['text'] = $_POS_DISTRIBUTED_QUANTITIES_NOT_CONFIRMED[$item][$pos]['quantity'];

								if (!$_SAP_ROW_READONLY && $_STATE_EDIT) {

									if ($_VALIDATA_TOTAL_ITEM_QUANTITY) {
										$celReserve = spreadsheet::key($col, $_ROW_STOCK_RESERVE);
									}
									
									// TAG: [section] [item] [pos] [material] [total] [planned] [reserve]
									$id = $_POS_DISTRIBUTED_QUANTITIES_NOT_CONFIRMED[$item][$pos]['id'];
									$grid[$index]['tag'] = "$column;$id;$pos;$material;$celReserve";

								} else {
									$readonly = CELL_READONLY;
								}


							} // END PARTIALLY COLUMN DISTRIBUTION


							$grid[$index]['cls'] = $_DATAGRID[$row][$col]['cls'].$cls;

						} // END ITEM CEL 
						else {
							$readonly = CELL_READONLY;
						}

						$grid[$index]['readonly'] = $readonly;
					}

				break;

				case $_ROW_WAREHOUSE_CAPTION:

					if ($_DATAGRID[$row][$col]) {
						$grid[$index]['html'] = $_DATAGRID[$row][$col]['value'];
						$grid[$index]['cls'] = $_DATAGRID[$row][$col]['cls'];
					}

					$grid[$index]['readonly']  = CELL_READONLY;

				break;

				case $row >= $_FIRST_WAREHOUSE_ROW && $row <= $_LAST_WAREHOUSE_ROW:

					if ($col < $_FIRST_ITEM_COL) {
						$grid[$index]['html'] = $_DATAGRID[$row][$col]['value'];
						$grid[$index]['cls'] = $_DATAGRID[$row][$col]['cls'];
					}
					elseif ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {
						
						// references
						$readonly = false;
						$item = $_DATAGRID[$_ROW_REFERENCE][$col]['id'];
						$warehouse = $_DATAGRID[$row][$_COL_POS_CAPTION]['id'];

						if ($item && $warehouse) {

							// references
							$column = $_DATAGRID[$_ROW_REFERENCE][$col]['column'];
							$nextColumn = $_DATAGRID[$_ROW_REFERENCE][$col+1]['column'];
							$material = $_REFERENCES[$item]['material']['id'];

							$cls = " cel";

							$grid[$index]['hiddenAsNull'] = 'true';
							$grid[$index]['numeric'] = 'true';
							
							// COLUMN PLANNING
							if ($column == COLUMN_PLANNING) {

								$grid[$index]['text'] = $_WAREHOUSE_PLANNED_QUANTITIES[$item][$warehouse]['quantity'];

								if ($_MODE_PLANNING) { 

									if ($_STATE_EDIT) {
										
										if ($_VALIDATA_TOTAL_ITEM_QUANTITY) {
											$celReserve = spreadsheet::key($col, $_ROW_STOCK_RESERVE);
										}

										// TAG: [section] [quantity_id] [warehouse] [material] [total] [used]
										$id = $_WAREHOUSE_PLANNED_QUANTITIES[$item][$warehouse]['id'];
										$grid[$index]['tag'] = "reserve-$column;$id;$warehouse;$material;$celReserve";

										$readonly = $_DATAGRID[$_ROW_REFERENCE][$col+1]['column']==COLUMN_DISTRIBUTION ? CELL_READONLY : false;

									} else {
										$readonly = CELL_READONLY;
									}
								}

								if ($_MODE_DISTRIBUTION) {
									$cls .= ' planned-readonly';
									$readonly = CELL_READONLY;
								}

							} // END COLUMN PLANNING


							// COLUMN DISTRIBUTION
							if ($column == COLUMN_DISTRIBUTION) {

								$grid[$index]['text'] = $_WAREHOUSE_DISTRIBUTED_QUANTITIES[$item][$warehouse]['quantity'];

								if ($_STATE_EDIT) {

									if ($_VALIDATA_TOTAL_ITEM_QUANTITY) {
										$celReserve = spreadsheet::key($col, $_ROW_STOCK_RESERVE);
									}

									// TAG: [section] [quantity_id] [warehouse] [material] [total] [used]
									$id = $_WAREHOUSE_DISTRIBUTED_QUANTITIES[$item][$warehouse]['id'];
									$grid[$index]['tag'] = "reserve-$column;$id;$warehouse;$material;$celReserve";

									$readonly = $_DATAGRID[$_ROW_REFERENCE][$col+1]['column']==COLUMN_PARTIALY_CONFIRMED ? CELL_READONLY : false;

								} else {
									$readonly = CELL_READONLY;
								}

								if ($nextColumn == COLUMN_PARTIALY_CONFIRMED) {
								 	$cls .= ' delivered-readonly';
								 }

							} // END COLUMN DISTRIBUTION


							// COLUMN PARTIALLY DISTRIBUTION
							if ($column==COLUMN_PARTIALY_CONFIRMED) {

								$grid[$index]['text'] = $_WAREHOUSE_DISTRIBUTED_QUANTITIES_NOT_CONFIRMED[$item][$warehouse]['quantity'];

								if ($_STATE_EDIT) {

									if ($_VALIDATA_TOTAL_ITEM_QUANTITY) {
										$celReserve = spreadsheet::key($col, $_ROW_STOCK_RESERVE);
									}

									// TAG: [section] [quantity_id] [warehouse] [material] [total] [used]
									$id = $_WAREHOUSE_DISTRIBUTED_QUANTITIES_NOT_CONFIRMED[$item][$warehouse]['id'];
									$grid[$index]['tag'] = "reserve-$column;$id;$warehouse;$material;$celReserve";

								} else {
									$readonly = CELL_READONLY;
								}

							} // END COLUMN PARTIALLY DISTRIBUTION

							// alerts
							if ($_WAREHOUSES_ALERTS[$warehouse]) {
								$readonly = CELL_READONLY;
								$cls .= " sap-readonly";
							}

							$grid[$index]['cls'] = $_DATAGRID[$row][$col]['cls'].$cls;

						} // END ITEM CEL 
						else {
							$readonly = CELL_READONLY;
						}

					} // END POS ROW

					$grid[$index]['readonly'] = $readonly;

				break;
								
				default:
					$grid[$index]['readonly']  = CELL_READONLY;
					//$grid[$index]['html'] = "&nbsp;";
				break;

			}
		}
	}	
}

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


if (!headers_sent()) {
	header('Content-Type: text/json');
}

$dist = $_EXPORT_SAP && ($state->state) ? 6 : 7;

echo json_encode(array(
	'response' => true,
	'data' => $grid,
	'merge' => $mergecel,
	'top' => $_MODE_DISTRIBUTION ? $dist : 6,
	'left' => $_TOTAL_FIXED_COLS,
	'references' => $_REFERENCES
));
