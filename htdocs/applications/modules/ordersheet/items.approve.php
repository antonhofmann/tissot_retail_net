<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];
$sendmail = $_REQUEST['sendmail'];

$proposedQuantities = $_REQUEST['mps_ordersheet_item_quantity_proposed'];
$clientQuantities = $_REQUEST['mps_ordersheet_item_quantity'];
$approvedQuantities = $_REQUEST['mps_ordersheet_item_quantity_approved'];

$_STATE_APPROVED = 4;

// order sheet
$ordersheet = new Ordersheet($application);
$ordersheet->read($id);

if ($ordersheet->id) {

	// db model
	$model = new Model($application);

	// get order sheet items
	$sth = $model->db->prepare("
		SELECT 
			mps_ordersheet_item_id,
			mps_ordersheet_item_quantity,
			mps_ordersheet_item_quantity_approved
		FROM mps_ordersheet_items
		WHERE mps_ordersheet_item_ordersheet_id = ?
	");

	$sth->execute(array($ordersheet->id));
	$items = $sth->fetchAll();

	if ($items) {

		// update statemenet
		$sth = $model->db->prepare("
			UPDATE mps_ordersheet_items SET 
				mps_ordersheet_item_quantity = ?,
				mps_ordersheet_item_quantity_approved = ?,
				mps_ordersheet_item_status = 0,
				user_modified = ?,
				date_modified = NOW()
			WHERE mps_ordersheet_item_id = ?
		");

		foreach ($items as $item) {
			
			$key = $item['mps_ordersheet_item_id'];

			// client quantity
			$clientQuantity = isset($clientQuantities[$key]) ? $clientQuantities[$key] : $item['mps_ordersheet_item_quantity'];

			// approved quantity
			if ($approvedQuantities[$key] <> null) $approvedQuantity = $approvedQuantities[$key];
			elseif ($clientQuantities[$key] <> null) $approvedQuantity = $clientQuantities[$key];
			else $approvedQuantity = $proposedQuantities[$key] ?: 0;

			$sth->execute(array($clientQuantity, $approvedQuantity, $user->login, $key));
		}
	}

	// update order sheet workflow state
	$sth = $model->db->prepare("
		UPDATE mps_ordersheets SET 
			mps_ordersheet_workflowstate_id = ?,
			user_modified = ?,
			date_modified = NOW()
		WHERE mps_ordersheet_id = ?
	");

	$response = $sth->execute(array($_STATE_APPROVED, $user->login, $ordersheet->id));

	if ($response) {

		$json['response'] = true;
		$json['reload'] = true;
		$json['sendmail'] = $sendmail;
		
		Message::success("The Order Sheet is approved.");
		
		$workflowState = new Modul($application);
		$workflowState->setTable('mps_workflow_states');
		$workflowState->read($_STATE_APPROVED);
			
		$track = new User_Tracking($application);
		$track->create(array(
			'user_tracking_entity' => 'order sheet',
			'user_tracking_entity_id' => $ordersheet->id,
			'user_tracking_user_id' => $user->id,
			'user_tracking_action' => $workflowState->data['mps_workflow_state_name'],
			'user_created' => $user->login,
			'date_created' => date('Y-m-d H:i:s')
		));

	} else {
		$json['response'] = false;
		$json['message']= "The Order Sheet can not revise.";
	}
}
else {
	$json['response'] = false;
	$json['message']= "The Order Sheet not found.";
}

header('Content-Type: text/json');
echo json_encode($json);
