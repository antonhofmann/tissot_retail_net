<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
$user = User::instance();
$translate = Translate::instance();

$_APP = $_REQUEST['application'];
$_TPL = $_REQUEST['mail_template_id'];
$_ORDERSHEETS = $_REQUEST['ordersheets'];
$_SUBJECT = $_REQUEST['mail_template_subject'];
$_CONTENT = $_REQUEST['mail_template_text'];

if (!$_ORDERSHEETS || !$_TPL) {
	
	$_RESPONSE['notification'] = "Bad request";
	
	if (!$_REQUEST['ordersheets']) {
		$_RESPONSE['notification'] = "Select at least one order sheets. ";
	}

	goto BLOCK_RESPONSE;
}


// action mail
$mail = new ActionMail($_REQUEST['mail_template_id']);

// set parameters
$mail->setParam('application', $_APP);
$mail->setParam('ordersheets', $_ORDERSHEETS);	
	
// mail content
if ($_SUBJECT) $mail->setSubject($_SUBJECT);
if ($_CONTENT) $mail->setBody($_CONTENT);
	
$_RESPONSE = $mail->getMailData();
$_RESPONSE['response'] = true;	

BLOCK_RESPONSE:	

header('Content-Type: text/json');
echo json_encode($_RESPONSE);
