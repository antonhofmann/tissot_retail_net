<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$workflow_states = $_REQUEST['workflow_states'];

// request
$ordersheets = ($_REQUEST['ordersheets']) ? explode(',',$_REQUEST['ordersheets']) : array();
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'country_name, address_company, mps_mastersheet_name';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rows = $settings->limit_pager_rows;
$offset = ($page-1) * $settings->limit_pager_rows;

// filters
$fSearch = ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) ? $_REQUEST['search'] : null;
$fYear = $_REQUEST['year'];
$fCountry = $_REQUEST['country'];
$fMasterSheet = $_REQUEST['mastersheet'];

$permissionView = user::permission(Ordersheet::PERMISSION_VIEW);
$permissionViewLimited = user::permission(Ordersheet::PERMISSION_VIEW_LIMITED);
$permissionEdit = user::permission(Ordersheet::PERMISSION_EDIT);
$permissionEditLimited = user::permission(Ordersheet::PERMISSION_EDIT_LIMITED);
$permissionManage = user::permission(Ordersheet::PERMISSION_MANAGE);

$binds = array(
	'INNER JOIN mps_ordersheet_item_shipped ON ramco_shipped_reports.ramco_shipped_report_item_id = mps_ordersheet_item_shipped.mps_ordersheet_item_shipped_id',
	'INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_shipped.mps_ordersheet_item_shipped_ordersheet_item_id = mps_ordersheet_items.mps_ordersheet_item_id',
	'INNER JOIN mps_ordersheets ON mps_ordersheet_items.mps_ordersheet_item_ordersheet_id = mps_ordersheets.mps_ordersheet_id',
	'INNER JOIN mps_mastersheets ON mps_mastersheets.mps_mastersheet_id = mps_ordersheets.mps_ordersheet_mastersheet_id',
	'INNER JOIN mps_workflow_states ON mps_workflow_states.mps_workflow_state_id = mps_ordersheets.mps_ordersheet_workflowstate_id',
	'INNER JOIN db_retailnet.addresses ON db_retailnet.addresses.address_id = mps_ordersheets.mps_ordersheet_address_id',
	'INNER JOIN db_retailnet.countries ON db_retailnet.countries.country_id = db_retailnet.addresses.address_country'
);

if ($fSearch) {
	$filters['search'] = "(
		address_company LIKE \"%$fSearch%\" 
		OR country_name LIKE \"%$fSearch%\" 
		OR mps_mastersheet_name LIKE \"%$fSearch%\" 
		OR mps_workflow_state_name LIKE \"%$fSearch%\"
	)";
}

// filer: mastersheet year
if ($fYear) {
	$filters['year'] = "YEAR(ramco_shipped_report_mail_date) = $fYear";
} else {
	$filters['year'] = "(ramco_shipped_report_mail_date IS NULL OR ramco_shipped_report_mail_date = '')";
}

// filer: countries
if ($fCountry) {
	$filters['country'] = "country_id = $fCountry";
}

// filer: mastersheet
if ($fMasterSheet) {
	$filters['mastersheet'] = "mps_ordersheet_mastersheet_id = $fMasterSheet";
}

// default filter
if ($fNotify) {
	$filters['state'] = "ramco_shipped_report_mail_date IS NULL";
}

	
// database model
$model = new Model($application);

// datagrid
$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT	
		mps_ordersheets.mps_ordersheet_id AS ordersheet,
		mps_mastersheets.mps_mastersheet_name AS mastersheet,
		db_retailnet.addresses.address_company AS company,
		db_retailnet.countries.country_name AS country,
		mps_workflow_states.mps_workflow_state_name AS stateid,
		mps_workflow_states.mps_workflow_state_name AS state,
		DATE_FORMAT(ramco_shipped_reports.ramco_shipped_report_mail_date, '%d.%m.%Y') AS sendmail_date
	FROM ramco_shipped_reports 
")
->bind($binds)
->filter($filters)
->order($sort, $direction)
->offset($offset, $rows)
->fetchAll();

$_ITEMS = array();
$_DATAGRID = array();

if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = array();


	// get items
	$sth = $model->db->prepare("
		SELECT DISTINCT
			mps_ordersheet_item_shipped_id AS id,
			mps_ordersheet_item_shipped_quantity AS quantity_shipped,
			mps_ordersheet_item_shipped_po_number AS pon,
			mps_ordersheet_item_id AS item,
			mps_ordersheet_item_confirmed_quantity AS quantity_confirmed,
			mps_material_code AS material_code,
			mps_material_name AS material_name,
			DATE_FORMAT(ramco_shipped_report_mail_date, '%d.%m.%Y') AS sendmail_date,
			DATE_FORMAT(mps_ordersheet_item_shipped_date, '%d.%m.%Y') AS shipped_date
		FROM ramco_shipped_reports
		INNER JOIN mps_ordersheet_item_shipped ON ramco_shipped_report_item_id = mps_ordersheet_item_shipped_id
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_shipped_ordersheet_item_id = mps_ordersheet_item_id
		INNER JOIN mps_ordersheet_item_confirmed ON mps_ordersheet_item_confirmed_ordersheet_item_id = mps_ordersheet_item_id
		INNER JOIN mps_materials ON mps_ordersheet_item_material_id = mps_material_id
		WHERE mps_ordersheet_item_ordersheet_id = ? AND mps_ordersheet_item_confirmed_order_state != '02'
		ORDER BY ramco_shipped_report_mail_date DESC, mps_ordersheet_item_shipped_id DESC, material_code
	");

	foreach ($result as $row) {
		
		$id = $row['id'];
		$item = $row['item'];
		$ordersheet = $row['ordersheet'];
		$ordersheetDate = null;

		// get items
		$sth->execute(array($ordersheet));
		$items = $sth->fetchAll();

		if ($items) {

			foreach ($items as $item) {
				
				$id = $item['id'];
				$reference = $item['item'];
				$date = $item['sendmail_date'];

				$data = array(
					'material_code' => $item['material_code'],
					'material_name' => $item['material_name'],
					'quantity_confirmed' => $item['quantity_confirmed'],
					'quantity_shipped' => $item['quantity_shipped'],
					'pon' => $item['pon'],
					'shipped_date' => $item['shipped_date']
				);

				// group items by notificatio  date
				if ($date) {

					if (!$ordersheetDate) $ordersheetDate = $date;
					else $ordersheetDate = strtotime($date) > strtotime($ordersheetDate) ? $date : $ordersheetDate;
					
					if (!$_ITEMS[$ordersheet]['notified'][$date]['items'][$reference]) {
						$_ITEMS[$ordersheet]['notified'][$date]['date'] = $item['sendmail_date'];
						$_ITEMS[$ordersheet]['notified'][$date]['items'][$reference] = $data;
					}
				} 
				// group items by item id
				elseif (!$_ITEMS[$ordersheet]['unnotified'][$reference]) {
					$_ITEMS[$ordersheet]['unnotified'][$reference] = $data;
				}
			}

			// order sheet
			$_DATAGRID[$ordersheet]['country'] = $row['country'];
			$_DATAGRID[$ordersheet]['company'] = $row['company'];
			$_DATAGRID[$ordersheet]['mastersheet'] = $row['mastersheet'];
			$_DATAGRID[$ordersheet]['state'] = $row['state'];
			$_DATAGRID[$ordersheet]['template'] = $row['state_id']==14 ? 29 : 28;
			$_DATAGRID[$ordersheet]['date'] = $ordersheetDate;
		}

	}

	$pager = new Pager(array(
		'page' => $page,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}

//echo "<pre>"; 
//print_r($_ITEMS); die();


// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";

// toolbox: serach full text
$toolbox[] = ui::searchbox();


// data: mastersheet years
$result = $model->query("SELECT DISTINCT YEAR(ramco_shipped_report_mail_date) AS value, YEAR(ramco_shipped_report_mail_date) AS caption FROM ramco_shipped_reports")
->bind($binds)
->filter($filters)
->filter('defyear', 'ramco_shipped_report_mail_date IS NOT NULL')
->exclude('year')
->order('caption')
->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'year',
		'id' => 'year',
		'class' => 'submit',
		'value' => $fYear,
		'caption' => 'Year'
	));
}

// data: countries
$result = $model->query("SELECT DISTINCT country_id AS value, country_name AS caption FROM ramco_shipped_reports")
->bind($binds)
->filter($filters)
->exclude('country')
->order('caption')
->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'country',
		'id' => 'country',
		'class' => 'submit',
		'value' => $fCountry,
		'caption' => $translate->all_countries
	));
}

// data: mastersheets
$result = $model->query("SELECT DISTINCT mps_mastersheet_id AS value, mps_mastersheet_name AS caption FROM ramco_shipped_reports")
->bind($binds)
->filter($filters)
->exclude('mastersheet')
->order('caption')
->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'mastersheet',
		'id' => 'mastersheet',
		'class' => 'submit',
		'value' => $fMasterSheet,
		'caption' => $translate->all_mastersheets
	));
}

// toolbox: form
if ($toolbox) {
	$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
}


$list = "<table class='default'>";

$list .="
	<thead>
		<tr>
			<th width='20px'>&nbsp;</th>
			<th nowrap=nowrap width='10%' ><strong>$translate->country_name</strong></th>
			<th nowrap=nowrap width='20%'><strong>$translate->address_company</strong></th>
			<th nowrap=nowrap ><strong>$translate->mps_mastersheet_name</strong></th>
			<th nowrap=nowrap width='10%'><strong>Workflow State</strong></th>
			<th nowrap=nowrap width='3%'><strong>Mail Sent</strong></th>
		</tr>
	</thead>
	<tbody>
";

if ($_DATAGRID) {

	foreach ($_DATAGRID as $id => $row) {
		
		$template = $row['template'];
		$rowClass = $rowClass=='-odd' ? '-even' : '-odd'; 
		
		$itemToggler = $_ITEMS[$id] ? "<a href='#' class='table-toggler' ><i class='fa fa-chevron-circle-right' ></i></a>" : null;

		$list .= "<tr class='$rowClass has-subtable row-$id' >";
		$list .= "<td nowrap=nowrap  class='item-trigger' title='Items' ><a href='#' class='table-toggler' ><i class='fa fa-chevron-circle-right' ></i></a></td>";
		$list .= "<td nowrap=nowrap >{$row[country]}</td>";
		$list .= "<td nowrap=nowrap >{$row[company]}</td>";
		$list .= "<td nowrap=nowrap >{$row[mastersheet]}</td>";
		$list .= "<td nowrap=nowrap >{$row[state]}</td>";
		$list .= "<td nowrap=nowrap class='sendmail_date' >{$row[date]}</td>";
		$list .= "</tr>";

		if ($_ITEMS[$id]['notified'] || $_ITEMS[$id]['unnotified']) {

			$list .= "<tr class='row-subtable $rowClass' id='subtable-$id'>";
			$list .= "<td colspan='6' class='cell-subtable'>";
			
			$list .= "<div class='subtable-container' id='row-$id' >";

			// unnotified items
			if ($_ITEMS[$id]['unnotified']) {

				$list .= "<div class='order-container unnotified' >";
				$list .= "<div class='order-header' >";
				$list .= "<h5>Unnotified order sheet items</h5>";
				$list .= "<div class='order-actions bootstrap' >";
				$list .= "<button type='button' class='btn btn-success btn-xs test-mail' data-ordersheet='$id' data-template='$template' ><i class='fa fa-envelope'></i> Test Mail</button>";
				$list .= "<button type='button' class='btn btn-info btn-xs preview-mail' data-ordersheet='$id' data-template='$template' ><i class='fa fa-square'></i> Preview Mail</button>";
				$list .= "<button type='button' class='btn btn-primary btn-xs send-mail' data-ordersheet='$id' data-template='$template' ><i class='fa fa-envelope'></i> Send Mail</button>";
				$list .= "</div>";
				$list .= "</div>";
				
				$list .= "<table class='subtable'><thead>";
				$list .="<tr>";
				$list .="<th nowrap=nowrap width='20%' ><strong>Item Code</strong></th>";
				$list .="<th nowrap=nowrap ><strong>Item Name</strong></th>";
				$list .="<th nowrap=nowrap width='10%' ><strong>Confirmed</strong></th>";
				$list .="<th nowrap=nowrap width='10%' ><strong>Shipped</strong></th>";
				$list .="<th nowrap=nowrap width='10%' ><strong>PO Number</strong></th>";
				$list .="<th nowrap=nowrap width='10%' ><strong>Shipped Date</strong></th>";
				$list .="</tr>";
				$list .="</thead><tbody>";
				
				foreach ($_ITEMS[$id]['unnotified'] as $i => $item) {
					$list .= "<tr>";
					$list .= "<td nowrap=nowrap>{$item[material_code]}</td>";
					$list .= "<td nowrap=nowrap>{$item[material_name]}</td>";
					$list .= "<td nowrap=nowrap>{$item[quantity_confirmed]}</td>";
					$list .= "<td nowrap=nowrap>{$item[quantity_shipped]}</td>";
					$list .= "<td nowrap=nowrap>{$item[pon]}</td>";
					$list .= "<td nowrap=nowrap>{$item[shipped_date]}</td>";
					$list .= "</tr>";
				}

				$list .= "</tbody></table>";
				$list .= "</div>";
			}

			// notified items
			if ($_ITEMS[$id]['notified']) {
				
				foreach ($_ITEMS[$id]['notified'] as $key => $data) {

					$list .= "<div class='order-container' >";
					
					$list .= "<div class='order-header' >";
					$list .= "<h5>Notified on {$data[date]}</h5>";
					$list .= "</div>";
					
					$list .= "<table class='subtable'><thead>";
					$list .="<tr>";
					$list .="<th nowrap=nowrap width='20%' ><strong>Item Code</strong></th>";
					$list .="<th nowrap=nowrap ><strong>Item Name</strong></th>";
					$list .="<th nowrap=nowrap width='10%' ><strong>Confirmed</strong></th>";
					$list .="<th nowrap=nowrap width='10%' ><strong>Shipped</strong></th>";
					$list .="<th nowrap=nowrap width='10%' ><strong>PO Number</strong></th>";
					$list .="<th nowrap=nowrap width='10%' ><strong>Shipped Date</strong></th>";
					$list .="</tr>";
					$list .="</thead><tbody>";
					
					foreach ($data['items'] as $i => $item) {
						$list .= "<tr>";
						$list .= "<td nowrap=nowrap>{$item[material_code]}</td>";
						$list .= "<td nowrap=nowrap>{$item[material_name]}</td>";
						$list .= "<td nowrap=nowrap>{$item[quantity_confirmed]}</td>";
						$list .= "<td nowrap=nowrap>{$item[quantity_shipped]}</td>";
						$list .= "<td nowrap=nowrap>{$item[pon]}</td>";
						$list .= "<td nowrap=nowrap>{$item[shipped_date]}</td>";
						$list .= "</tr>";
					}

					$list .= "</tbody></table>";
					$list .= "</div>";
				}
			}
			
			$list .= "</div>";

			$list .= "</td>";
			$list .= "</tr>";
		}
	}
} else {
	$list .= "<tr><td colspan='6' class='-emptybox'>No matching records found</td></tr>";
}

$list .= "</tbody>";

if ($_DATAGRID) {
	$list .= "<tfoot>";
	$list .= "<tr>";
	$list .= "<td colspan='3' >$list_index</td>";
	$list .= "<td colspan='4' align=right >$list_controlls</td>";
	$list .= "</tr>";
	$list .= "</tfoot>";
}

$list .= "</table>";

echo $toolbox.$list;
	