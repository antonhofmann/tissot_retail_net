<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$id = $_REQUEST['id'];
$application = $_REQUEST['application'];

$model = new Model($application);

$ordersheet = new Ordersheet($application);
$ordersheet->read($id);

if ($ordersheet->id) {

	$result = $model->query("
		SELECT ramco_shipped_report_id
		FROM ramco_shipped_reports 
		INNER JOIN mps_ordersheet_item_shipped ON ramco_shipped_report_item_id = mps_ordersheet_item_shipped_id
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_shipped_ordersheet_item_id = mps_ordersheet_item_id
		WHERE ramco_shipped_report_mail_date IS NULL AND mps_ordersheet_item_ordersheet_id = $ordersheet->id
	")->fetchAll();

	if ($result) {

		$sth = $model->db->prepare("
			UPDATE ramco_shipped_reports SET 
				ramco_shipped_report_mail_date = CURRENT_DATE(),
				ramco_shipped_report_mail_sender = ?
			WHERE ramco_shipped_report_id = ?
		");

		foreach ($result as $row) {
			$sth->execute(array($user->id, $row['ramco_shipped_report_id']));
		}

		$json['date'] = date('d.m.Y');
	}

	$json['response'] = true;
	$json['notification'] = "Your request ist submitted.";

} else {
	$json['response'] = false;
	$json['notification'] = "The order sheet not found.";
}

header('Content-Type: text/json');
echo json_encode($json);
