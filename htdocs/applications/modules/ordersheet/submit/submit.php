<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$sendmail = $_REQUEST['sendmail'];

// selectd ordersheets
$ordersheets = $_REQUEST['ordersheets'] ? explode(',', $_REQUEST['ordersheets']) : false;

switch ($action) {
	
	// launchplan submiter
	case 'submit':

	$model = new Model($application);
		
	if ($ordersheets) {

		$sth = $model->db->prepare("
			UPDATE mps_ordersheets SET
				mps_ordersheet_workflowstate_id = 1,
				user_modified = ?,
				date_modified = NOW()
			WHERE mps_ordersheet_id = ?
		");	

		// set ordersheets in workflow state "open"
		foreach ($ordersheets as $id) {
			$sth->execute(array($user->login, $id));
		}
		
		$json['response'] = true;
		$json['reload'] = true;
		$json['notification'] = "The Order Sheet(s) are submitted.";
		$json['sendmail'] = $sendmail;
		
		// onload message
		Message::request_submitted();

	}  else {
		$json['response'] = false;
		$json['notification'] = "Bad request. Check form data.";
	}

	break;	

	// launchplan rimainder
	case 'remind':
		
		// no actions
		if ($ordersheets) {
			$json['response'] = true;
			$json['sendmail'] = $sendmail;
			$json['notification'] = "The Order Sheet(s) are submitted.";
		} else {
			$json['response'] = false;
			$json['notification'] = "Bad request. Check form data.";
		}

	break;
}


if (!headers_sent()) {
	header('Content-Type: text/json');
}

echo json_encode($json);

