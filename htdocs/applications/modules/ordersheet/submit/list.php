<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$workflow_states = $_REQUEST['workflow_states'];

// request
$ordersheets = ($_REQUEST['ordersheets']) ? explode(',',$_REQUEST['ordersheets']) : array();
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'country_name, address_company, mastersheet_name';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rows = $settings->limit_pager_rows;
$offset = ($page-1) * $settings->limit_pager_rows;

// filters
$fSearch = ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) ? $_REQUEST['search'] : null;
$fYear = $_REQUEST['year'];
$fCountry = $_REQUEST['country'];
$fMasterSheet = $_REQUEST['mastersheet'];

switch ($application) {
				
	case 'mps':
	

		$permissionView = user::permission(Ordersheet::PERMISSION_VIEW);
		$permissionViewLimited = user::permission(Ordersheet::PERMISSION_VIEW_LIMITED);
		$permissionEdit = user::permission(Ordersheet::PERMISSION_EDIT);
		$permissionEditLimited = user::permission(Ordersheet::PERMISSION_EDIT_LIMITED);
		$permissionManage = user::permission(Ordersheet::PERMISSION_MANAGE);

		$binds = array(
			Ordersheet::DB_BIND_COMPANIES,
			Ordersheet::DB_BIND_MASTERSHEETS,
			Ordersheet::DB_BIND_WORKFLOW_STATES,
			Company::DB_BIND_COUNTRIES,
			Company::DB_BIND_PLACES
		);

		if ($fSearch) {
			$filters['search'] = "(
				address_company LIKE \"%$fSearch%\" 
				OR country_name LIKE \"%$fSearch%\" 
				OR mps_mastersheet_name LIKE \"%$fSearch%\" 
				OR mps_workflow_state_name LIKE \"%$fSearch%\" 
				OR place_name LIKE \"%$fSearch%\"
			)";
		}

		// filer: mastersheet year
		if ($fYear) {
			$filters['year'] = "mps_mastersheet_year = $fYear";
		}

		// filer: countries
		if ($fCountry) {
			$filters['country'] = "country_id = $fCountry";
		}

		// filer: mastersheet
		if ($fMasterSheet) {
			$filters['mastersheet'] = "mps_ordersheet_mastersheet_id = $fMasterSheet";
		}

		// default filter
		$filters['state'] = "mps_ordersheet_workflowstate_id IN ($workflow_states)";

		$query = "
			SELECT SQL_CALC_FOUND_ROWS DISTINCT	
				mps_ordersheet_id AS id,
				mps_ordersheet_workflowstate_id AS ordersheet_workflowstate_id,
				address_company,
				country_name,
				mps_mastersheet_name AS mastersheet_name,
				mps_workflow_state_name AS workflow_state_name,
				mps_ordersheet_openingdate AS ordersheet_openingdate,
				DATE_FORMAT(mps_ordersheet_openingdate,'%d.%m.%Y') AS openingdate,
				mps_ordersheet_closingdate AS ordersheet_closingdate,
				DATE_FORMAT(mps_ordersheet_closingdate,'%d.%m.%Y') AS closingdate
			FROM mps_ordersheets
		";

		// dropdown queries
		$queryDropDownMasterSheetYears = "SELECT DISTINCT mps_mastersheet_year AS value, mps_mastersheet_year AS caption FROM mps_ordersheets";
		$queryDropDownCountries = "SELECT DISTINCT country_id AS value, country_name AS caption FROM mps_ordersheets";
		$queryDropdownMasterSheets= "SELECT DISTINCT mps_mastersheet_id AS value, mps_mastersheet_name AS caption FROM mps_ordersheets";

	break;

	case 'lps':
	
		
		$permissionView = user::permission('can_view_all_lps_sheets');
		$permissionViewLimited = user::permission('can_view_only_his_lps_sheets');
		$permissionEdit = user::permission('can_edit_all_lps_sheets');
		$permissionEditLimited = user::permission('can_edit_only_his_lps_sheets');
		$permissionManage = user::permission('can_manage_lps_sheets');
		
		$binds = array(
			'INNER JOIN lps_mastersheets ON lps_mastersheet_id = lps_launchplan_mastersheet_id',
			'INNER JOIN lps_workflow_states ON lps_workflow_state_id = lps_launchplan_workflowstate_id',
			'INNER JOIN db_retailnet.addresses ON address_id = lps_launchplan_address_id',
			'INNER JOIN db_retailnet.countries ON country_id = address_country',
			'INNER JOIN db_retailnet.places ON place_id = address_place_id'
		);
		
		// filter: limited permission
		if(!$permissionManage && !$permissionEdit && !$permissionView) { 
			$filters['limited'] = "lps_launchplan_address_id = $user->address AND lps_launchplan_workflowstate_id <> 1 ";
		}
			
		// filter: full text search
		if ($fSearch) {
			$filters['search'] = "(
				address_company LIKE \"%$fSearch%\" 
				OR country_name LIKE \"%$fSearch%\" 
				OR lps_mastersheet_name LIKE \"%$fSearch%\" 
				OR lps_workflow_state_name LIKE \"%$fSearch%\" 
				OR place_name LIKE \"%$fSearch%\"
			)";
		}

		// filer: mastersheet year
		if ($fYear) {
			$filters['year'] = "lps_mastersheet_year = $fYear";
		}

		// filer: countries
		if ($fCountry) {
			$filters['country'] = "country_id = $fCountry";
		}

		// filer: mastersheet
		if ($fMasterSheet) {
			$filters['mastersheet'] = "lps_launchplan_mastersheet_id = $fMasterSheet";
		}

		// default filter
		$filters['state'] = "lps_launchplan_workflowstate_id IN ($workflow_states)";

		$query = "
			SELECT SQL_CALC_FOUND_ROWS DISTINCT	
				lps_launchplan_id AS id,
				lps_launchplan_workflowstate_id AS ordersheet_workflowstate_id,
				address_company,
				country_name,
				lps_mastersheet_name AS mastersheet_name,
				lps_workflow_state_name AS workflow_state_name,
				lps_launchplan_openingdate AS ordersheet_openingdate,
				DATE_FORMAT(lps_launchplan_openingdate,'%d.%m.%Y') AS openingdate,
				lps_launchplan_closingdate AS ordersheet_closingdate,
				DATE_FORMAT(lps_launchplan_closingdate,'%d.%m.%Y') AS closingdate,
				(
					SELECT COUNT(lps_launchplan_item_id) AS total
					FROM lps_launchplan_items
					WHERE lps_launchplan_item_launchplan_id = lps_launchplan_id
				) AS items
			FROM lps_launchplans
		";

		// dropdown queries
		$queryDropDownMasterSheetYears = "SELECT DISTINCT lps_mastersheet_year AS value, lps_mastersheet_year AS caption FROM lps_launchplans";
		$queryDropDownCountries = "SELECT DISTINCT country_id AS value, country_name AS caption FROM lps_launchplans";
		$queryDropdownMasterSheets= "SELECT DISTINCT lps_mastersheet_id AS value, lps_mastersheet_name AS caption FROM lps_launchplans";

	break;
}
	
// database model
$model = new Model($application);

// datagrid
$result = $model->query($query)
->bind($binds)
->filter($filters)
->order($sort, $direction)
->offset($offset, $rows)
->fetchAll();

if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);
	$count = 0;
	
	foreach ($datagrid as $key => $row) {
		$checked = (in_array($key, $ordersheets)) ? 'checked=checked' : null;
		$datagrid[$key]['box'] = "<input type=checkbox class=ordersheet name=ordersheet value=$key $checked>";
		if ($checked) $count++;
	}

	$pager = new Pager(array(
		'page' => $page,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
	$checkall = (count($datagrid) == $count) ? 'checked=checked' : null;
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=clients name=clients value='".join(',',$ordersheets)."' />";
	

// toolbox: serach full text
$toolbox[] = ui::searchbox();

// data: mastersheet years
$result = $model->query($queryDropDownMasterSheetYears)
->bind($binds)
->filter($filters)
->exclude('year')
->order('caption')
->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'year',
		'id' => 'year',
		'class' => 'submit',
		'value' => $fYear,
		'label' => $translate->all_years
	));
}

// data: countries
$result = $model->query($queryDropDownCountries)
->bind($binds)
->filter($filters)
->exclude('country')
->order('caption')
->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'country',
		'id' => 'country',
		'class' => 'submit',
		'value' => $fCountry,
		'label' => $translate->all_countries
	));
}

// data: mastersheets
$result = $model->query($queryDropdownMasterSheets)
->bind($binds)
->filter($filters)
->exclude('mastersheet')
->order('caption')
->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'mastersheet',
		'id' => 'mastersheet',
		'class' => 'submit',
		'value' => $fMasterSheet,
		'caption' => $translate->all_mastersheets
	));
}

// toolbox: form
if ($toolbox) {
	$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
}

$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

$table->datagrid = $datagrid;

if ($datagrid) {
	$table->caption('box', "<input type=checkbox name=checkall class=checkall $checkall>");
}

$table->country_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=15%'
);

$table->address_company(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=20%'
);

$table->mastersheet_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	"href=".$_REQUEST['button']['form']
);

$table->ordersheet_openingdate(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

$table->ordersheet_closingdate(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

$table->box(
	Table::ATTRIBUTE_ALIGN_CENTER,
	'width=20px'
);

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;
	