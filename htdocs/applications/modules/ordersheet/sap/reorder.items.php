<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.cronjob.php';

define(DEBUGGING_MODE, false);

$user = User::instance();
$translate = Translate::instance();


$_START_DATE = '2014-11-01';
$_ERRORS = array();
$_CONSOLE = array();

$application = $_REQUEST['application'];
$id = $_REQUEST['ordersheet'];

$ordersheet = new Ordersheet($application);
$ordersheet->read($id);

if (!$ordersheet->id) {
	$_ERRORS[] = "Order sheet not found";
}

// db model
$model = new Model($application);
$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);


// sap not confirmed items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PONS = array();

if (!$_ERRORS) {

	$result = $model->query("
		SELECT 
			mps_ordersheet_item_delivered_id AS distribution,
			mps_ordersheet_item_id AS item, 
			mps_ordersheet_item_delivered_sap_purchase_order_number AS pon,
			mps_ordersheet_item_delivered_confirmed AS confirmed_date,
			sap_confirmed_items.sap_confirmed_item_id AS confirmed
		FROM mps_ordersheet_item_delivered 
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_delivered_ordersheet_item_id = mps_ordersheet_item_id
		LEFT JOIN sap_confirmed_items ON mps_ordersheet_item_delivered_id = sap_confirmed_item_mps_distribution_id
		WHERE mps_ordersheet_item_delivered_confirmed IS NOT NULL 
		AND mps_ordersheet_item_delivered_sap_purchase_order_number IS NOT NULL 
		AND mps_ordersheet_item_delivered_confirmed >= '$_START_DATE' AND mps_ordersheet_item_ordersheet_id = $ordersheet->id
	")->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			if (!$row['confirmed']) {
				$_PONS[] = $row['pon'];
			}
		}
		$_PONS = array_unique(array_filter($_PONS));
	}
}

$_CONSOLE['pons'] = $_PONS;


// update sales orders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


if (!$_ERRORS && $_PONS) {

	$pon = "'".join("', '", $_PONS)."'";

	$result = $exchange->query("
		SELECT sap_salesorder_item_id AS id
		FROM sap_salesorder_items
		WHERE sap_salesorder_item_fetched_by_sap IS NOT NULL AND sap_salesorder_item_mps_po_number IN ($pon)
	")->fetchAll();

	if ($result) {

		$sth = $exchange->db->prepare("
			UPDATE sap_salesorder_items SET 
				sap_salesorder_item_fetched_by_sap = NULL
			WHERE sap_salesorder_item_id = ?
		");

		foreach ($result as $row) {
			
			$id = $row['id'];
			
			if (DEBUGGING_MODE) $response = true;
			else $response = $sth->execute(array($id));

			if (!$response) {
				$_ERRORS[] = "Item $id cannot be updated";
				break;
			}
		}
	}
}

$_CONSOLE['sales.order.items'] = $result;


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (DEBUGGING_MODE) {
	$json['console'] = $_CONSOLE;
}

if ($_ERRORS) {
	array_unshift($queue, $translate->message_request_failure);
	$json['success'] = false;
	$json['message'] = join('<br>', $_ERRORS);
} else {
	$json['success'] = true;
	$json['message'] = $translate->message_request_submitted;
}

header('Content-Type: text/json');
echo json_encode($json);

