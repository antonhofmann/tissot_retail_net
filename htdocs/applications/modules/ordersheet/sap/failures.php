<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$_START_DATE = '2014-11-01';

$add_button = $_REQUEST['submit_standard_ordersheet'];
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'ordersheet_openingdate DESC, country_name, address_company, mastersheet_name';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

// session request
$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

// filters
$fSearch = ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) ? $_REQUEST['search'] : null;
$fYear = $_REQUEST['year'];
$fCountry = $_REQUEST['country'];
$fMasterSheet = $_REQUEST['mastersheet'];
$fWorkflowState = $_REQUEST['workflowstate'];

// db models
$model = new Model($application);
$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);

$permissionView = user::permission(Ordersheet::PERMISSION_VIEW);
$permissionViewLimited = user::permission(Ordersheet::PERMISSION_VIEW_LIMITED);
$permissionEdit = user::permission(Ordersheet::PERMISSION_EDIT);
$permissionEditLimited = user::permission(Ordersheet::PERMISSION_EDIT_LIMITED);
$permissionManage = user::permission(Ordersheet::PERMISSION_MANAGE);

$binds = array(
	Ordersheet::DB_BIND_COMPANIES,
	Ordersheet::DB_BIND_MASTERSHEETS,
	Ordersheet::DB_BIND_WORKFLOW_STATES,
	Company::DB_BIND_COUNTRIES,
	Company::DB_BIND_PLACES
);

// user has limited permissions
$_LIMITED_ACCESS = !$permissionView && !$permissionEdit && !$permissionManage ? true : false;

// filter: limited permission
if($_LIMITED_ACCESS) {

	$filters['not_in_preparation'] = "mps_ordersheet_workflowstate_id <> 7";

	// filter access countries
	$countryAccess = User::getCountryAccess();	
	$filterCountryAccess = $countryAccess ? " OR address_country IN ($countryAccess)" : null;

	// regional access companies
	$regionalAccessCompanies = User::getRegionalAccessCompanies();
	$filterRegionalAccess = $regionalAccessCompanies ? " OR address_parent IN (".join(',', $regionalAccessCompanies).") " : null;

	$filters['limited'] = "(
		address_id = $user->address
		$filterCountryAccess 
		$filterRegionalAccess
	)";
}
	
// filter: full text search
if ($fSearch) {
	$filters['search'] = "(
		address_company LIKE \"%$fSearch%\" 
		OR country_name LIKE \"%$fSearch%\" 
		OR mps_mastersheet_name LIKE \"%$fSearch%\" 
		OR mps_workflow_state_name LIKE \"%$fSearch%\" 
		OR place_name LIKE \"%$fSearch%\"
	)";
}

// filer: mastersheet year
if ($fYear) {
	$filters['year'] = "mps_mastersheet_year = $fYear";
}

// filer: countries
if ($fCountry) {
	$filters['country'] = "country_id = $fCountry";
}

// filer: mastersheet
if ($fMasterSheet) {
	$filters['mastersheet'] = "mps_ordersheet_mastersheet_id = $fMasterSheet";
}

// filer: workflowstate
if ($fWorkflowState) {
	$filters['workflowstate'] = "mps_ordersheet_workflowstate_id = $fWorkflowState";
}


// default filter
$filters['visibility'] = " mps_ordersheet_workflowstate_id IN (11,13,15,16) ";


$_PONS = array();
$_ORDERSHEETS = array();

$result = $model->query("
	SELECT 
		mps_ordersheet_item_delivered_id AS distribution,
		mps_ordersheet_item_id AS item, 
		mps_ordersheet_item_delivered_sap_purchase_order_number AS pon,
		mps_ordersheet_item_delivered_confirmed AS confirmed_date,
		mps_ordersheet_item_ordersheet_id AS ordersheet,
		sap_confirmed_items.sap_confirmed_item_id AS confirmed
	FROM mps_ordersheet_item_delivered 
	INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_delivered_ordersheet_item_id = mps_ordersheet_item_id
	LEFT JOIN sap_confirmed_items ON mps_ordersheet_item_delivered_id = sap_confirmed_item_mps_distribution_id
	WHERE mps_ordersheet_item_delivered_confirmed IS NOT NULL 
	AND mps_ordersheet_item_delivered_sap_purchase_order_number IS NOT NULL 
	AND mps_ordersheet_item_delivered_confirmed >= '$_START_DATE'
")->fetchAll();

if ($result) {

	foreach ($result as $row) {
		if (!$row['confirmed']) {
			$item = $row['distribution'];
			$_ORDERSHEETS[$item] = $row['ordersheet'];
			$_PONS[] = $row['pon'];
		}
	}

	$_PONS = array_unique(array_filter($_PONS));
}


$failures = array();

if ($_PONS) { 

	$pon = "'".join("', '", $_PONS)."'";

	$result = $exchange->query("
		SELECT sap_salesorder_item_id AS id, sap_salesorder_item_mps_item_id AS item
		FROM sap_salesorder_items
		WHERE sap_salesorder_item_fetched_by_sap IS NOT NULL 
		AND sap_salesorder_item_mps_po_number IN ($pon)
	")->fetchAll();

	if ($result) {

		foreach ($result as $row) {
			$item = $row['item'];
			if ($_ORDERSHEETS[$item]) {
				$failures[] = $_ORDERSHEETS[$item];
			}
		}
	}
}

// filter failures
$failures = join(',', array_unique(array_filter($failures)));
$filters['failures'] = "mps_ordersheet_id IN ( $failures ) ";


// datagrid
$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT	
		mps_ordersheet_id AS id,
		mps_ordersheet_workflowstate_id AS ordersheet_workflowstate_id,
		address_company,
		country_name,
		mps_mastersheet_name AS mastersheet_name,
		mps_workflow_state_name AS workflow_state_name,
		mps_ordersheet_openingdate AS ordersheet_openingdate,
		DATE_FORMAT(mps_ordersheet_openingdate,'%d.%m.%Y') AS openingdate,
		mps_ordersheet_closingdate AS ordersheet_closingdate,
		DATE_FORMAT(mps_ordersheet_closingdate,'%d.%m.%Y') AS closingdate
	FROM mps_ordersheets
")
->bind($binds)
->filter($filters)
->order($sort, $direction)
->offset($offset, $rowsPerPage)
->fetchAll();

if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);
	$timestamp = date::timestamp();
	$icon_warning = ui::icon('warrning');

	// count order sheet items
	$ordersheetsKeys = join(',', array_keys($datagrid));

	$result = $model->query("
		SELECT 
			mps_ordersheet_item_ordersheet_id AS id,
			COUNT(mps_ordersheet_item_id) AS total
		FROM mps_ordersheet_items
		WHERE mps_ordersheet_item_ordersheet_id IN ($ordersheetsKeys)
		GROUP BY mps_ordersheet_item_ordersheet_id
	")->fetchAll();

	$items = _array::extract($result);

	foreach ($datagrid as $key => $row) {
		
		// dates
		$datagrid[$key]['ordersheet_openingdate'] = $row['openingdate'];
		$datagrid[$key]['ordersheet_closingdate'] = $row['closingdate'];
		
		// expired closing dates
		if ( ($timestamp > date::timestamp($row['closingdate'])) AND in_array($row['ordersheet_workflowstate_id'], array(1,2,7,8))) {
			$datagrid[$key]['ordersheet_closingdate'] = "<span class=error>".$row['closingdate']." !!!</span>";
		}
		
		if (!$items[$key]) {
			$datagrid[$key]['warningicon'] = $icon_warning;
		}
	}

	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// standard mastersheet add button :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($add_button) {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'label' => $translate->add_new
	));
}

// full text search field ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$toolbox[] = ui::searchbox();

// dropdown bulk operations ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
if($_REQUEST['manage'] || $_REQUEST['submit'] || $_REQUEST['remind'] || $_REQUEST['exchange_ordersheet_items'] || $_REQUEST['exchange_sap']) {

	$dropdown = array();
	
	if ($_REQUEST['manage']) {
		array_push($dropdown, array(
			'value' => $_REQUEST['manage'],
			'caption' => $translate->bulk_operations
		));
	}
	
	if ($_REQUEST['submit']) {
		
		array_push($dropdown, array(
			'value' => $_REQUEST['submit'],
			'caption' => "Submit Order Sheets"
		));
	}
	
	if ($_REQUEST['remind']) {

		array_push($dropdown, array(
			'value' => $_REQUEST['remind'],
			'caption' => "Remind Order Sheets"
		));
	}
	
	if ($_REQUEST['exchange_ordersheet_items']) {
		array_push($dropdown, array(
			'value' => $_REQUEST['exchange_ordersheet_items'],
			'caption' => $translate->import_items,
			'class' => 'request_modal'
		));
	}
	
	if ($_REQUEST['exchange_sap'] && $MPS) {
		array_push($dropdown, array(
			'value' => $_REQUEST['exchange_sap'],
			'caption' => 'SAP Import',
			'class' => 'request_modal'
		));
	}
	
	$toolbox[] = ui::dropdown($dropdown, array(
		'name' => 'actions',
		'id' => 'actions',
		'caption' => $translate->actions
	));
}


// dropdown master sheet years :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("SELECT DISTINCT mps_mastersheet_year AS value, mps_mastersheet_year AS caption FROM mps_ordersheets")
->bind($binds)->filter($filters)->exclude('year')->order('caption')->fetchAll(); 

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'year',
		'id' => 'year',
		'class' => 'submit',
		'value' => $fYear,
		'caption' => $translate->all_years
	));
}


// dropdown countries ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("SELECT DISTINCT country_id AS value, country_name AS caption FROM mps_ordersheets")
->bind($binds)->filter($filters)->exclude('country')->order('caption')->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'country',
		'id' => 'country',
		'class' => 'submit',
		'value' => $fCountry,
		'caption' => $translate->all_countries
	));
}


// dropdown workflow states ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("SELECT DISTINCT mps_workflow_state_id AS value, mps_workflow_state_name AS caption FROM mps_ordersheets")
->bind($binds)->filter($filters)->exclude('workflowstate')->order('caption')->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'workflowstate',
		'id' => 'workflowstate',
		'class' => 'submit',
		'value' => $fWorkflowState,
		'caption' => $translate->all_workflow_states
	));
}

// dropdown master sheets ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("SELECT DISTINCT mps_mastersheet_id AS value, mps_mastersheet_name AS caption FROM mps_ordersheets")
->bind($binds)->filter($filters)->exclude('mastersheet')->order('caption')->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'mastersheet',
		'id' => 'mastersheet',
		'class' => 'submit',
		'value' => $fMasterSheet,
		'caption' => $translate->all_mastersheets
	));
}

$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";

$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

$table->datagrid = $datagrid;

$table->country_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=15%'
);

$table->address_company(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=20%'
);

$table->mastersheet_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	"href=".$_REQUEST['form']
);

$table->ordersheet_openingdate(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

$table->ordersheet_closingdate(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

$table->workflow_state_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

if ( !$archived && $datagrid && _array::key_exists('warningicon', $datagrid)) {
	$table->warningicon(
		'width=20px'
	);
}

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;
