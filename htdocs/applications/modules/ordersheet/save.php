<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$id = $_REQUEST['mps_ordersheet_id'];
$newState = $_REQUEST['mps_ordersheet_workflowstate_id'];

// db model
$model = new Model($application);

$ordersheet = new Modul($application);
$ordersheet->setTable('mps_ordersheets');
$ordersheet->read($id);

// reset states
$resetStates = array(1,7);

$data = array();	
		
if (isset($_REQUEST['mps_ordersheet_workflowstate_id'])) {
	$data['mps_ordersheet_workflowstate_id'] = $_REQUEST['mps_ordersheet_workflowstate_id'];
}

if (isset($_REQUEST['mps_ordersheet_address_id'])) {
	$data['mps_ordersheet_address_id'] = $_REQUEST['mps_ordersheet_address_id'];
}

if (isset($_REQUEST['mps_ordersheet_mastersheet_id'])) {
	$data['mps_ordersheet_mastersheet_id'] = $_REQUEST['mps_ordersheet_mastersheet_id'];
}

if (isset($_REQUEST['mps_ordersheet_comment'])) {
	$data['mps_ordersheet_comment'] = $_REQUEST['mps_ordersheet_comment'];
}

if (isset($_REQUEST['mps_ordersheet_openingdate'])) {
	$data['mps_ordersheet_openingdate'] = date::sql($_REQUEST['mps_ordersheet_openingdate']);
}

if (isset($_REQUEST['mps_ordersheet_closingdate'])) {
	$data['mps_ordersheet_closingdate'] = date::sql($_REQUEST['mps_ordersheet_closingdate']);
}

if ($data && $ordersheet->id) {

	$currentState = $ordersheet->data['mps_ordersheet_workflowstate_id'];
	
	// track state change
	$track = $newState<>$currentState ? true : false;
	
	$data['user_modified'] = $user->login;
	$data['date_modified'] = date('Y-m-d H:i:s');
	
	// update order shet data
	$response = $ordersheet->update($data);
	
	if ($response) {

		Message::request_saved();
		
		$redirect = $_REQUEST['redirect'];
			
		// reset approvment
		if ($currentState<>$newState && in_array($newState, Ordersheet_State::loader('approve'))) {
			
			$clientQuantities = in_array($newState, array(1,7)) ? "mps_ordersheet_item_quantity = NULL," : null;

			$sth = $model->db->prepare("
				UPDATE mps_ordersheet_items SET
					$clientQuantities
					mps_ordersheet_item_quantity_approved = NULL,
					mps_ordersheet_item_quantity_confirmed = NULL,
					mps_ordersheet_item_quantity_shipped = NULL,
					mps_ordersheet_item_quantity_distributed = NULL,
					mps_ordersheet_item_customernumber = NULL,
					mps_ordersheet_item_shipto = NULL,
					mps_ordersheet_item_order_date = NULL,
					mps_ordersheet_item_purchase_order_number = NULL
				WHERE mps_ordersheet_item_ordersheet_id = ?
			");
		
			$sth->execute(array($ordersheet->id));
		}

	} else {
		Message::request_failure();
	}

	// track workflow state changes
	if ($response && $track) {

		$workflowState = new Modul($application);
		$workflowState->setTable('mps_workflow_states');
		$workflowState->read($newState);
			
		$user_tracking = new User_Tracking($application);
		$user_tracking->create(array(
			'user_tracking_entity' => 'order sheet',
			'user_tracking_entity_id' => $ordersheet->id,
			'user_tracking_user_id' => $user->id,
			'user_tracking_action' => $workflowState->data['mps_workflow_state_name'],
			'user_created' => $user->login,
			'date_created' => date('Y-m-d H:i:s')
		));
	}

} else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect
));
	