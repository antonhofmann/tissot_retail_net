<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];

// request data
$_ID = $_REQUEST['id'];
$_TITLE = $_REQUEST['title'];

$_ERRORS = array();
$_MESSAGES = array();
$_DATA = array();

// db model
$model = new Model($application);

// ordersheet
$ordersheet = new Modul($application);
$ordersheet->setTable('mps_ordersheets');
$ordersheet->read($_ID);

if (!$ordersheet->id) {
	$_ERRORS[] = "The order not found.";
}

if (!$_TITLE) {
	$_ERRORS[] = "Version title is empty.";
}

$_DATA['launchplan'] = $ordersheet->data;


// create order sheet version ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && $_TITLE) {

	$sth = $model->db->prepare("
		INSERT INTO mps_ordersheet_versions (
			mps_ordersheet_version_ordersheet_id,
			mps_ordersheet_version_workflowstate_id,
			mps_ordersheet_version_address_id,
			mps_ordersheet_version_mastersheet_id,
			mps_ordersheet_version_title,
			mps_ordersheet_version_openingdate,
			mps_ordersheet_version_closingdate,
			mps_ordersheet_version_comment,
			user_created,
			date_created
		)
		VALUES (?,?,?,?,?,?,?,?,?,NOW())
	");
	
	// add order sheet version
	if (!DEBUGGING_MODE) {
		
		$response = $sth->execute(array(
			$ordersheet->id,
			$ordersheet->data['mps_ordersheet_workflowstate_id'],
			$ordersheet->data['mps_ordersheet_address_id'],
			$ordersheet->data['mps_ordersheet_mastersheet_id'],
			$_TITLE,
			$ordersheet->data['mps_ordersheet_openingdate'],
			$ordersheet->data['mps_ordersheet_closingdate'],
			$ordersheet->data['mps_ordersheet_comment'],
			$user->login
		));

		$_VERSION = $model->db->lastInsertId();

	} else $_VERSION = true;

	if ($_VERSION) $_MESSAGES[] = "The order sheet version $_VERSION is created";
	else $_ERRORS[] = "The order sheet version cannot be created, check Database query.";
}


// get order sheet items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ITEMS = array();

if (!$_ERRORS) {

	$sth = $model->db->prepare("
		SELECT *
		FROM mps_ordersheet_items
		WHERE mps_ordersheet_item_ordersheet_id = ?
	");

	$sth->execute(array($launchplan->id));
	$result = $sth->fetchAll();

	$_ITEMS = _array::datagrid($result);

	if ($result) $_MESSAGES[] = "Total order sheet items: ".count($_ITEMS); 
	else $_ERRORS[] = "The order sheet items not found";
}

$_DATA['items'] = $_ITEMS;


// create order sheet items version ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && $_ITEMS) {

	$sth = $model->db->prepare("
		INSER INTO mps_ordersheet_version_items (
			mps_ordersheet_version_item_ordersheetversion_id,
			mps_ordersheet_version_item_material_id,
			mps_ordersheet_version_item_price,
			mps_ordersheet_version_item_currency,
			mps_ordersheet_version_item_exchangrate,
			mps_ordersheet_version_item_factor,
			mps_ordersheet_version_item_quantity,
			mps_ordersheet_version_item_quantity_proposed,
			mps_ordersheet_version_item_quantity_approved,
			mps_ordersheet_version_item_quantity_confirmed,
			mps_ordersheet_version_item_quantity_shipped,
			mps_ordersheet_version_item_quantity_distributed,
			mps_ordersheet_version_item_status,
			mps_ordersheet_item_order_date,
			mps_ordersheet_item_customernumber,
			mps_ordersheet_version_item_shipto,
			mps_ordersheet_version_item_desired_delivery_date,
			mps_ordersheet_version_item_purchase_order_number,
			user_created,
			date_created
		)
		VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW())
	");

	foreach ($_ITEMS as $id => $item) {

		$data = array(
			$_VERSION,
			$item['mps_ordersheet_item_material_id'],
			$item['mps_ordersheet_item_price'],
			$item['mps_ordersheet_item_currency'],
			$item['mps_ordersheet_item_exchangrate'],
			$item['mps_ordersheet_item_factor'],
			$item['mps_ordersheet_item_quantity'],
			$item['mps_ordersheet_item_quantity_proposed'],
			$item['mps_ordersheet_item_quantity_approved'],
			$item['mps_ordersheet_item_quantity_confirmed'],
			$item['mps_ordersheet_item_quantity_shipped'],
			$item['mps_ordersheet_item_quantity_distributed'],
			$item['mps_ordersheet_item_status'],
			$item['mps_ordersheet_item_order_date'],
			$item['mps_ordersheet_item_customernumber'],
			$item['mps_ordersheet_item_shipto'],
			$item['mps_ordersheet_item_desired_delivery_date'],
			$item['mps_ordersheet_item_purchase_order_number'],
			$user->login
		);

		if (!DEBUGGING_MODE) {
			$response = $sth->execute($data);
		} else $response = true;

		if ($response) {
			$inserted = (DEBUGGING_MODE) ? true : $model->db->lastInsertId();
			$_MESSAGES[] = "Insert order sheet item $inserted";
		} else {
			$_ERRORS[] = "The order sheet item for reference {$item[mps_ordersheet_item_material_id]} is not inserted";
		}
	}
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (DEBUGGING_MODE) {
	echo "<pre>";
	print_r($_ERRORS);
	print_r($_MESSAGES);
	print_r($_DATA);
	echo "</pre>";
} else {
	
	if (!$_ERRORS) {
		$response = true;
		Message::ordersheet_version_created();
	}
	else {
		$response = false;
		Message::request_failure();
	}

	url::redirect("/$application/$controller/$action/$_ID");
}
