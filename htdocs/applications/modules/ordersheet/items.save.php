<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$id = $_REQUEST['ordersheet'];
$application = $_REQUEST['application'];
$proposedQuantities = $_REQUEST['mps_ordersheet_item_quantity_proposed'];
$clientQuantities = $_REQUEST['mps_ordersheet_item_quantity'];
$approvedQuantities = $_REQUEST['mps_ordersheet_item_quantity_approved'];

// order sheet
$ordersheet = new Ordersheet($application);
$ordersheet->read($id);

if ($ordersheet->id) {

	// db model
	$model = new Model($application);

	// state handler
	$state = new State($ordersheet->workflowstate_id, $application); 
	$state->setOwner($ordersheet->address_id);
	$state->setDateIntervals($ordersheet->openingdate, $ordersheet->closingdate);

	// workflow state
	$workflowState = new Workflow_State($application);
	
	// tracker
	$tracker = new User_Tracking($application);

	$response = true;

	// get order sheet items
	$sth = $model->db->prepare("
		SELECT 
			mps_ordersheet_item_id,
			mps_ordersheet_item_quantity,
			mps_ordersheet_item_quantity_approved,
			mps_ordersheet_item_quantity_proposed
		FROM mps_ordersheet_items
		WHERE mps_ordersheet_item_ordersheet_id = ?
	");

	$sth->execute(array($ordersheet->id));
	$items = $sth->fetchAll();

	if ($items) { 

		$newState = false;
		$hasQuantitiy = false;
		
		// add stock reserve warehouse if not exist
		$ordersheet->warehouse()->addStockReserve();

		if ($state->state==7 || $state->state==1) {
			
			$quantities = array_merge(
				$clientQuantities ?: array(), 
				$approvedQuantities ?: array()
			);

			foreach ($quantities as $key => $quantity) {
				if ($quantity) {
					$hasQuantitiy = true;
					break;
				}
			}

			$newState = $hasQuantitiy ? 2 : false;
		}

		// change order sheet state
		if ($newState) {

			$reload = true;
			
			$workflowState->read($newState);
			
			// change workflow state
			$stateChange = $ordersheet->update(array(
				'mps_ordersheet_workflowstate_id' => $workflowState->id,
				'user_modified' => $user->login,
				'date_modified' => date('Y-m-d H:i:s')
			));
			
			// user tracking
			if ($stateChange) {
				$tracker->create(array(
					'user_tracking_entity' => 'order sheet',
					'user_tracking_entity_id' => $ordersheet->id,
					'user_tracking_user_id' => $user->id,
					'user_tracking_action' => $workflowState->name,
					'user_created' => $user->login,
					'date_created' => date('Y-m-d H:i:s')
				));
			}
		}
		
		// order sheet items
		foreach ($items as $item) {

			$data = array();

			$key = $item['mps_ordersheet_item_id'];
			$ordersheet->item()->read($key);

			// proposed quantity
			if (isset($proposedQuantities[$key])) {
				$data['mps_ordersheet_item_quantity_proposed'] = $proposedQuantities[$key] ?: null;
			}
			
			// owner quantity
			if (isset($clientQuantities[$key])) {
				
				$data['mps_ordersheet_item_quantity'] = $clientQuantities[$key] ?: null;
				
				if ( $state->onApproving() ) {				
					
					if ($clientQuantities[$key] > 0) { 
						$totalPlanned = $ordersheet->item()->planned()->sumQuantities();
						$stockReserve = $clientQuantities[$key] - $totalPlanned;
						$stockReserve = ($stockReserve > 0) ? $stockReserve : null;
						$ordersheet->item()->planned()->setStockReserve($stockReserve);
					} else {
						$ordersheet->item()->planned()->setStockReserve(null);
					}
				}
			}

			// approved quantity
			if ($state->canAdministrate()) {
	
				if (isset($approvedQuantities[$key])) {

					$approved = $approvedQuantities[$key];

					if (!$approved) { 
						$approved = $approved==='0' ? 0 : null;
					}

					$data['mps_ordersheet_item_quantity_approved'] = $approved;
				} 
			}

			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');

			// update order sheet items
			$ordersheet->item()->update($data);
		}

		$message = $translate->message_request_saved;

	} else {
		$sendmail = false;
		$message = "The Order Sheet has no items.";
	}

} else {
	$response = false;
	$sendmail = false;
	$message = "The Order Sheet not found.";
}


header('Content-Type: text/json');
echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'reload' => $reload
));
