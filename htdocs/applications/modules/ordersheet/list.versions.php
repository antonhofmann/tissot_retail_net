<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];

	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'date';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "desc";
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rows = $settings->limit_pager_rows;
	$offset = ($page-1) * $rows;

	switch ($application) {
		
		case 'lps':
		

			$query = "
				SELECT SQL_CALC_FOUND_ROWS DISTINCT
					lps_launchplan_version_id,
					lps_launchplan_version_title AS version_title,
					DATE_FORMAT(lps_launchplan_versions.date_created, '%d.%m.%Y') AS date
				FROM lps_launchplan_versions
				WHERE lps_launchplan_version_launchplan_id = $id
 			";
			
		break;
	}

	// db connector
	$model = new Model($application);

	// datagrid	
	$result = $model->query($query)
	->order($sort, $direction)
	->offset($offset, $rows)
	->fetchAll();
	
	if ($result) {
		
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));
		
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	
	// toolbox: form
	if ($toolbox) {
		$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	
	$table->version_title(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		"href=".$_REQUEST['form']
	);
	
	$table->date(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=15%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	