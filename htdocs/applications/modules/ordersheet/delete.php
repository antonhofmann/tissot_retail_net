<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

// request vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

// db application model
$model = new Model($application);

// application dependency
switch ($application) {
			
	case 'mps':
	

		$tableName = 'mps_ordersheets';
		$filedState = 'mps_ordersheet_workflowstate_id';
		$filedMasterSheet = 'mps_ordersheet_mastersheet_id';

		// order sheet deletable statments
		$deletableStates = array(7,1);
		
		$queryDeleteOrderSheet = "
			DELETE FROM mps_ordersheets
			WHERE mps_ordersheet_id = ?
		";

		$queryDeleteOrderSheetWarehouses = "
			DELETE FROM mps_ordersheet_warehouses
			WHERE mps_ordersheet_warehouse_ordersheet_id = ?
		";

		$queryGetOrderSheetVersions = "
			SELECT GROUP_CONCAT(mps_ordersheet_version_id) as versions
			FROM mps_ordersheet_versions
			WHERE mps_ordersheet_version_ordersheet_id = ?
		";

		$queryDeleteOrderSheetVersions = "
			DELETE FROM mps_ordersheet_versions
			WHERE mps_ordersheet_version_ordersheet_id = ?
		";

		$queryDeleteOrderSheetVersionItems = "
			DELETE FROM mps_ordersheet_version_items
			WHERE mps_ordersheet_version_item_ordersheetversion_id IN (?)
		";

		$queryGetOrderSheetItems = "
			SELECT GROUP_CONCAT(mps_ordersheet_item_id) AS items
			FROM mps_ordersheet_items
			WHERE mps_ordersheet_item_ordersheet_id = ?
		";

		$queryDeleteOrderSheetItems = "
			DELETE FROM mps_ordersheet_items
			WHERE mps_ordersheet_item_ordersheet_id = ?
		";

		$queryDeletePlannedQuantities = "
			DELETE FROM mps_ordersheet_item_planned
			WHERE mps_ordersheet_item_planned_ordersheet_item_id IN (?)
		";

		$queryDeleteConfirmedQuantities = "
			DELETE FROM mps_ordersheet_item_confirmed
			WHERE mps_ordersheet_item_confirmed_ordersheet_item_id IN (?)
		";

		$queryDeleteShippedQuantities = "
			DELETE FROM mps_ordersheet_item_shipped
			WHERE mps_ordersheet_item_shipped_ordersheet_item_id IN (?)
		";

		$queryDeleteDistributedQuantities = "
			DELETE FROM mps_ordersheet_item_delivered
			WHERE mps_ordersheet_item_delivered_ordersheet_item_id IN (?)
		";

	break;

	case 'lps':
	
		
		$tableName = 'lps_launchplans';
		$filedState = 'lps_launchplan_workflowstate_id';
		$filedMasterSheet = 'lps_ordersheet_mastersheet_id';

		// order sheet deletable statments
		$deletableStates = array(1,2);
		
		$queryDeleteOrderSheet = "
			DELETE FROM lps_launchplans
			WHERE lps_launchplan_id = ?
		";

		$queryDeleteOrderSheetWarehouses = "
			DELETE FROM lps_launchplan_warehouses
			WHERE lps_launchplan_warehouse_launchplan_id = ?
		";

		$queryGetOrderSheetVersions = "
			SELECT GROUP_CONCAT(lps_launchplan_version_id) as versions
			FROM lps_launchplan_versions
			WHERE lps_launchplan_version_launchplan_id = ?
		";

		$queryDeleteOrderSheetVersions = "
			DELETE FROM lps_launchplan_versions
			WHERE lps_launchplan_version_launchplan_id = ?
		";

		$queryDeleteOrderSheetVersionItems = "
			DELETE FROM lps_launchplan_version_items
			WHERE lps_launchplan_version_item_version_id IN (?)
		";

		$queryGetOrderSheetItems = "
			SELECT GROUP_CONCAT(lps_launchplan_item_id) AS items
			FROM lps_launchplan_items
			WHERE lps_launchplan_item_launchplan_id = ?
		";

		$queryDeleteOrderSheetItems = "
			DELETE FROM lps_launchplan_items
			WHERE lps_launchplan_item_launchplan_id = ?
		";

		$queryDeletePlannedQuantities = "
			DELETE FROM lps_launchplan_item_planned
			WHERE lps_launchplan_item_planned_item_id IN (?)
		";

		$queryDeleteConfirmedQuantities = "
			DELETE FROM lps_launchplan_item_confirmed
			WHERE lps_launchplan_item_confirmed_item_id IN (?)
		";

		$queryDeleteShippedQuantities = "
			DELETE FROM lps_launchplan_item_shipped
			WHERE lps_launchplan_item_shipped_item_id IN (?)
		";

		$queryDeleteDistributedQuantities = "
			DELETE FROM lps_launchplan_item_delivered
			WHERE lps_launchplan_item_delivered_item_id IN (?)
		";

	break;
}

// ordersheet
$ordersheet = new Modul($application);
$ordersheet->setTable($tableName);
$ordersheet->read($id);

if ($ordersheet->id) {

	// get ordersheet versions
	$getOrderSheetVersions = $model->db->prepare($queryGetOrderSheetVersions);

	// get ordersheet items
	$getOrderSheetItems = $model->db->prepare($queryGetOrderSheetItems);

	// delete order sheet warehouses
	$deleteOrderSheetWarehouses = $model->db->prepare($queryDeleteOrderSheetWarehouses);

	// delete order sheet versions
	$deleteOrderSheetVersions = $model->db->prepare($queryDeleteOrderSheetVersions);

	// delete order sheet version items
	$deleteOrderSheetVersionItems = $model->db->prepare($queryDeleteOrderSheetVersionItems);

	// delete order sheet items
	$deleteOrderSheetItems = $model->db->prepare($queryDeleteOrderSheetItems);

	// delete order sheet planned items
	$deleteOrderSheetPlannedItems = $model->db->prepare($queryDeletePlannedQuantities);

	// delete order sheet confirmed items
	$deleteOrderSheetConfirmedItems = $model->db->prepare($queryDeleteConfirmedQuantities);

	// delete order sheet confirmed items
	$deleteOrderSheetShippedItems = $model->db->prepare($queryDeleteShippedQuantities);

	// delete order sheet distrubuted items
	$deleteOrderSheetDistributedItems = $model->db->prepare($queryDeleteDistributedQuantities);

	// get ordershet data
	$ordersheet->read($id);

	// is standard ordersheet
	$standard = ($_REQUEST['standard']==$ordersheet->data[$filedMasterSheet]) ? true : false;

	if ($standard) { 
		
		// order sheet workflow state is deletable
		if (in_array($ordersheet->data[$filedState], $deletableStates)) {
				
			// delete order sheet
			$response = $ordersheet->delete();
			
			if ($response) {
				
				// delete order sheet warehouses
				$deleteOrderSheetWarehouses->execute(array($id));

				// get order sheet versions
				$getOrderSheetVersions->execute(array($id));
				$result = $getOrderSheetVersions->fetch();
				$versions = $result['versions'];

				// delete order sheet versions
				$deleteOrderSheetVersions->execute(array($id));
				$deleteOrderSheetVersionItems->execute(array($versions));

				// get order sheet items
				$getOrderSheetItems->execute(array($id));
				$result = $getOrderSheetItems->fetch();
				$items = $result['items'];

				// delete order sheet items
				$deleteOrderSheetItems->execute(array($id));
				$deleteOrderSheetPlannedItems->execute(array($items));
				$deleteOrderSheetConfirmedItems->execute(array($items));
				$deleteOrderSheetShippedItems->execute(array($items));
				$deleteOrderSheetDistributedItems->execute(array($items));

				Message::request_deleted();
				url::redirect("/$application/$controller");
			}
			else {
				Message::request_failure();
				url::redirect("/$application/$controller/$action/$id");
			}
		} else {
			Message::add(array(
				'type'=>'error',
				'content'=>'The Order Sheet is not in deletabled states.',
				'life'=>3000
			));
			url::redirect("/$application/$controller/$action/$id");
		}
	} else {
		Message::add(array(
			'type'=>'error',
			'content'=>'Only Standard Order Sheet can be deleted.',
			'life'=>3000
		));
		url::redirect("/$application/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}

