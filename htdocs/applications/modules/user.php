<?php

	class User {
		
		/**
		 * User ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * User Data
		 * @var array
		 */
		public $data;
		
		/**
		 * User Roles
		 * @var array
		 */
		public $roles;


		/**
		 * User Preferences
		 * @var array
		 */
		public $preferences;
		
		/**
		 * Class Instance
		 * @var string, class name
		 */
		private static $instance;
		
		/**
		 * DB Model
		 * @return Model
		 */
		protected $model;
		
		/**
		 * User Export Lists
		 * @return User_Export_List
		 */
		public $export_list;

		/**
		 * Permission: can edit system data
		 * @var string
		 */
		const PERMISSION_EDIT = 'can_administrate_system_data';
		
		/**
		 * SQL query join table companies
		 * @var string
		 */
		const DB_BIND_COMPANIES = 'INNER JOIN db_retailnet.addresses ON address_id = user_address';
		
		/**
		 * SQL query join table user roles
		 * @var unknown
		 */
		const DB_BIND_USER_ROLES = 'INNER JOIN db_retailnet.user_roles ON user_role_user = user_id';

		/**
		 * Class Instance
		 * @return User
		 */
		public static function instance() {
			if (!self::$instance) self::$instance = new User();
			return self::$instance;
		}

		public function __construct($id=null) {
			
			$id = ($id) ? $id : session::get('user_id');
			$this->model = new User_Model(Connector::DB_CORE);
			
			if ($id) {
				$this->read($id);
				$this->getPreferences();
			}
		}

		public function __get($key) {
			return $this->data["user_$key"];
		}
		
		public static function id() {
			return self::instance()->id;
		}

		public function read($id) { 
			$this->data = $this->model->read($id);
			$this->id = $this->data['user_id'];
			return $this->data;
		}

		/**
		 * Read user object by user_address
		 * @param  integer $addressID
		 * @return array   Array of user data
		 */
		public function readByAddress($addressID) { 
			$this->data = $this->model->readByAddress($addressID);
			$this->id = $this->data['user_id'];
			return $this->data;
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
				
			if ($this->id) {
		
				$response = $this->model->delete($this->id);
		
				if ($response) {
					$user_role = new User_Role();
					$user_role->user($this->id);
					$user_role->delete_all();
				}
		
				return $response;
			}
		}

		public static function permission($permission) {
			return (preg_match("/\b$permission\b/", Session::get('user_permissions'))) ? true : false;
		}

		public static function roles($id=null) { 

			$id = ($id) ? $id : session::get('user_id');
			$model = new User_Model(Connector::DB_CORE);
			$roles = $model->roles($id);

			if ($roles) {
				foreach ($roles as $row) {
					$return[] = $row['role'];
				}
			}
			else {
				
				$settings = Settings::init();
				
				if($settings->access_public_role) {
					$return[] = $settings->access_public_role;
				}
			}

			return ($return) ? $return : array();
		}

		public function isActive() {
			if ($this->id) {
				return $this->model->isActive($this->id);
			}
		}

		public function canUnlockIP() {
			if ($this->id) {
				return $this->model->canUnlockIP($this->id);
			}
		}

		public function resetPassword($md5) {
			if ($this->id) {
				return $this->model->resetPassword($this->id, $md5);
			}
		}

		public function getUsedPasswords() {
			if ($this->id) {
				$result = $this->model->getUsedPasswords($this->id);
				return ($result) ? unserialize($result['user_used_passwords']) : null;
			}
		}

		public function updateUsedPasswords($passwords) {
			if ($this->id) {
				return $this->model->updateUsedPasswords($this->id, $passwords);
			}
		}
		
		public static function identification($data) {
			$model = new User_Model(Connector::DB_CORE);
			return $model->identification($data);
			return ($result) ? $result['user_id'] : false;
		}
	
		public function application($application) {
			$result = $this->model->application($this->id, $application);
			return ($result) ? true : false;
		}

		/**
		 * Get user preferences
		 * All preferences are loaded into $this->preferences and also returned.
		 * If you want a specific entity or name within an entity, supply the 
		 * appropriate parameters.
		 * 	
		 * @param  string $entity
		 * @param  string $name
		 * @return mixed|null  Preference, array of preferences or null
		 */
		public function getPreferences($entity = null, $name = null) {

			if (!$this->id) return;

			if (!$this->preferences) {
				
				$result = $this->model->getPreferences($this->id);

				if (!$result) return;

				foreach ($result as $row) {
					$serialized = @unserialize($row['value']);
					$value = $serialized!==false ? $serialized : $row['value'];
					$kEntity = $row['entity'];
					$kName = $row['name'];
					$this->preferences[$kEntity][$kName] = $value;
				}
			}

			if ($entity && $this->preferences[$entity]) {
				return $name ? $this->preferences[$entity][$name] : $this->preferences[$entity];
			}

			return $this->preferences;
		}

		/**
		 * Get preference
		 * 
		 * @param  string $entity
		 * @param  string $name
		 * @return void
		 */
		public static function getPreference($entity, $name=null) {
			$preferences = self::instance()->getPreferences();
			return $name ? $preferences[$entity][$name] : $preferences[$entity];
		}

		/**
		 * Add a new preference
		 * You might want to use $this->updatePreference() instead, which handles both cases
		 * @param  string $entity
		 * @param  string $name
		 * @param  mixed $value
		 * @return void
		 */
		public function addPreference($entity, $name, $value) {
			if ($this->id) {
				return $this->model->addPreference($this->id, $entity, $name, $value);
			}
		}

		/**
		 * Add or update a user preference
		 * If preference does not yet exist, it will be created
		 * @param  string $entity
		 * @param  string $name
		 * @param  mixed $value
		 * @return boolean
		 */
		public function updatePreference($entity, $name, $value) {
			if (!$this->id) {
				return;
			}

			// check for existing preference
			$existing = $this->getPreferences($entity, $name);
			// add new pref
			if (is_null($existing)) {
				return $this->addPreference($entity, $name, $value);
			}
			// update existing
			else {
				return $this->model->updatePreference($this->id, $entity, $name, $value);
			}
		}
		
		
		/**
		 * User Export List
		 * @param id, Export List ID
		 * @return User_Export_List
		 */
		public static function exportList($id=null) {
			
			if (!self::instance()->export_list) {
				$user = self::instance()->id;
				self::instance()->export_list = new User_Export_List($user);
			}
			
			// read export list
			if ($id) self::instance()->export_list->read($id);
			
			return self::instance()->export_list;
		}

		public function preference($entity,$name) {
			static::instance()->preferences[$entity][$name];
		}

		
		static public function rowsPerPage($entity, $rows) {

			$rowsPerPage = static::instance()->preferences[$entity]['rows'];

			if (!$rowsPerPage && !$rows) {
				$rowsPerPage = Settings::init()->limit_pager_rows;
				static::instance()->addPreference($entity, 'rows', $rowsPerPage);
				static::instance()->preferences[$entity]['rows'] = $rowsPerPage;
			}
			
			if ($rows && $rowsPerPage && $rowsPerPage<>$rows) { 
				$rowsPerPage = $rows;
				static::instance()->updatePreference($entity, 'rows', $rowsPerPage);
				static::instance()->preferences[$entity]['rows'] = $rowsPerPage;
			}

			return $rowsPerPage;
		}

		static public function getCountryAccess() {

			$user = static::instance();
			$model = new Model(Connector::DB_CORE);

			$result = $model->query("
				SELECT GROUP_CONCAT( DISTINCT country_access_country) as countries
				FROM db_retailnet.country_access
				WHERE country_access_user = $user->id
			")->fetch();

			return $result['countries'];
		}


		static public function getRegionalAccessPos() {
			
			$user = static::instance();
			$model = new Model(Connector::DB_CORE);
			$filters = array();

			$result = $model->query("
				SELECT * 
				FROM user_company_responsibles 
				WHERE user_company_responsible_user_id = $user->id
			")->fetchAll();

			if ($result) {
				
				foreach ($result as $row) {
					
					$company = $row["user_company_responsible_address_id"];
					$retail = $row["user_company_responsible_retail"];
					$wholsale = $row["user_company_responsible_wholsale"];
					
					if($retail and $wholsale) {
						$filters[] = "posaddress_client_id = $company";
					} elseif($retail) {
						$filters[] = "(posaddress_client_id = $company AND posaddress_ownertype IN (1, 3, 4, 5))";
					} elseif($wholsale){
						$filters[] = "(posaddress_client_id = $company AND posaddress_ownertype IN (2, 6))";
					}
				}
			}

			return $filters ? join(' OR ', $filters) : null;
			
		}
		

		static public function getRegionalAccessCompanies() {
			
			$user = static::instance();
			$model = new Model(Connector::DB_CORE);
			
			$companies = array();

			$result = $model->query("
				SELECT user_company_responsible_address_id 
				FROM user_company_responsibles
				WHERE user_company_responsible_user_id = $user->id
			")->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$companies[] = $row["user_company_responsible_address_id"];
				}
			}
			
			return $companies;
		}

		static public function getRegionalAccessCountries() {
			
			$user = static::instance();
			$model = new Model(Connector::DB_CORE);
			
			$countries = array();

			$result = $model->query("
				SELECT DISTINCT address_country
				FROM user_company_responsibles
				INNER JOIN addresses ON address_id = user_company_responsible_address_id
				WHERE user_company_responsible_user_id = $user->id
			")->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$countries[] = $row["address_country"];
				}
			}

			return $countries;
		}

		public function getActiveUserFromEmail($email) {

			if (!$email) return;

			$sth = $this->model->db->prepare("
				SELECT *
				FROM users
				WHERE user_email = ? AND user_active = 1
				LIMIT 1
			");

			$sth->execute(array($email));
			$this->data = $sth->fetch();

			$this->id = $this->data['user_id'];
			return $this->data;
		}

		/**
		 * Is user password expired
		 * 
		 * @return boolean true on success
		 */
		public function isPasswordExpired() {

			if (!$this->id) return array();

			$settings = Settings::init();
			$passwordResetDays = $settings->limit_password_reset_days;

			$sth = $this->model->db->prepare("
				SELECT user_id 
				FROM users 
				WHERE DATE_SUB(CURDATE(), INTERVAL ? DAY) >= user_password_reset 
				AND user_id = ?
			");
			
			$sth->execute(array($passwordResetDays, $this->id));
			$result = $sth->fetch();

			return $result['user_id'] ? true : false;
		}

		public function getFirstPermittedUrl() {

			if (!$this->id) return array();

			$sth = $this->db->prepare("
				SELECT DISTINCT navigation_url
				FROM navigations
				INNER JOIN role_navigations ON role_navigation_navigation = navigation_id
				INNER JOIN user_roles ON user_role_role = role_navigation_role
				WHERE user_role_user = ?
				ORDER BY navigation_parent, navigation_order
				LIMIT 1
			");

			$sth->execute(array($this->id));
			$result = $sth->fetch(); 

			return $result['navigation_url'];
		}

		public function getPrintFields($entity, array $fields) {

			$translate = Translate::instance();

			$userPrintFields = self::getPreference($entity, 'fields') ?: array();
			$userSelectedFields = self::getPreference($entity, 'fields.selected') ?: array();

			// order selected fields
			if ($userPrintFields) { 
				$fields = _array::sortByArray($fields, $userPrintFields);
			}

			$return = array();

			foreach ($fields as $field) {
				
				$return[$field] = array(
					'caption' => $translate->$field ?: $field,
					'attributes' => array(
						'name' => "columns[$field]",
						'type' => 'checkbox',
						'value' => 1,
						'class' => 'export-field'
					),
					'sort' => true
				);

				if (in_array($field, $userSelectedFields)) {
					$return[$field]['attributes']['checked'] = 'checked';
				}
			}

			return $return;
		}
	}