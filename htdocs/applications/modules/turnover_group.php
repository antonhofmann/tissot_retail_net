<?php 

	class Turnover_Group {
		
		/**
		 * Turnover Group ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Turnover Group Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @var object
		 */
		protected $model;
		
		/**
		 * Can edit all turnover groups
		 * @var string
		 */
		const PERMISSION_EDIT = 'can_edit_mps_turnover_groups';
		
		/**
		 * Can view all turnover groups
		 * @var string
		 */
		const PERMISSION_VIEW = 'can_view_mps_turnover_groups';
		
		
		/**
		 * Turnover Group<br />
		 * Application Dependet Modul
		 * @author aserifi
		 * @copyright mediaparx ag
		 */
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Turnover_Group_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_turnover_group_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_turnover_group_id'];
			return $this->data;
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($application, $filters=null) {
			$model = new Turnover_Group_Model($application);
			$result = $model->loader($filter);
			return _array::extract($result);
		}
	}