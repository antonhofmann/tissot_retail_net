<?php 

	class Navigation_Language {
		
		public $data;
		
		public function __construct() {
			$this->model = new Navigation_Language_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["navigation_language_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['navigation_language_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				return $this->model->update($this->id, $data);
			}
		}
		
		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public function read_from_navigation($navigation,$language) {
			$this->data = $this->model->read_from_navigation($navigation,$language);
			$this->id = $this->data['navigation_language_id'];
			return $this->data;
		}
	}