<?php 

	class Staff {
		
		/**
		 * Staff ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Staff Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Staff Company
		 * @var object
		 */
		public $company;
		
		/**
		 * Staff Type
		 * @var object
		 */
		public $type;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @var object
		 */
		protected $model;
		

		const PERMISSION_EDIT = 'can_edit_mps_staff';
		const PERMISSION_EDIT_LIMITED = 'can_edit_only_his_mps_staff';
		const PERMISSION_VIEW = 'can_view_all_mps_staff';
		const PERMISSION_VIEW_LIMITED = 'can_view_only_his_mps_staff';
		
		/**
		 * Join staff companies
		 * @var string
		 */
		const DB_BIND_COMPANIES = "INNER JOIN db_retailnet.addresses ON address_id = mps_staff_address_id";
		
		/**
		 * Join staff types
		 * @var string
		 */
		const DB_BIND_STAFF_TYPES = "INNER JOIN mps_staff_types ON mps_staff_type_id = mps_staff_staff_type_id";
		
		
		/**
		 * Staff<br />
		 * Application Dependent Modul
		 * @author aserifi
		 * @copyright mediaparx ag
		 */
		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new Staff_Model($connector);
		}
		
		public function __get($key) {
			return $this->data["mps_staff_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_staff_id'];
			return $this->data;
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($application, $filters=null) {
			$model = new Staff_Model($application);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
		
		public function company() {
		
			if (!$this->company) {
				$this->company = new Company();
			}
		
			// set group id from turnover class
			$this->company->id = $this->address_id;
		
			return $this->company;
		}
		
		public function type() {
				
			if (!$this->type) {
				$this->type = new Staff_Type($this->connector);
			}
				
			if ($this->staff_type_id) {
				$this->type->read($this->staff_type_id);
			}
				
			return $this->type;
		}
		
		public function header() {
			if ($this->id) {
				return $this->firstname." ".$this->name." (".$this->type()->name.")";
			}
		}
	
		public function access() {
			
			$access = false;
			$user = User::instance();
			
			if($this->id && $user->id) { 

				// permissions
				$permission_view = user::permission(Staff::PERMISSION_VIEW);
				$permission_view_limited = user::permission(Staff::PERMISSION_VIEW_LIMITED);
				$permission_edit = user::permission(Staff::PERMISSION_EDIT);
				$permission_edit_limited = user::permission(Staff::PERMISSION_EDIT_LIMITED);
				
				if ($permissions_edit || $permission_view) {
					$access = true;
				} elseif ($permission_view_limited || $permission_edit_limited) {
					$access = $this->address_id==$user->address ? true : false;			
				}

				if (!$access && $this->address_id) {
					$company = new Company();
					$company->read($this->address_id);
					$access = $company->access();
				}

				/*
				// country access
				if (!$access) {

					$company = new Company();
					$company->read($this->address_id);

					$model = new Model(Connector::DB_CORE);
					
					$result = $model->query("
						SELECT COUNT(country_access_id) as total
						FROM country_access
						WHERE country_access_user = $user->id AND country_access_country = $company->country
					")->fetch();

					$access = $result['total'] > 0 ? true : false;
				}

				// country access
				if (!$access) {

					// regional access companies
					$regionalAccessCompanies = User::getRegionalAccessCompanies();

					if ($regionalAccessCompanies) {
						foreach ($regionalAccessCompanies as $company) {
							if ($this->address_id==$company) {
								$access = true;
								break;
							}
						}
					}
				}
				*/
			}

			return $access;			
		}

		public function canEdit() {

			$return = false;

			$permissionEdit = user::permission(Staff::PERMISSION_EDIT);
			$permissionEditLimited = user::permission(Staff::PERMISSION_EDIT_LIMITED);

			// system administrator
			if ($permissionEdit) {
				$return = true;
			} 
			// limited edit
			elseif ($permissionEditLimited) {

				$company = new Company();
				$company->read($this->address_id);

				// owner
				if ($company->owner()) $return = true;
				// regional access
				else $return = $company->hasRegionalAccess();
			}

			return $return;
		}
	}