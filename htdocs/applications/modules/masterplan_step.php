<?php

/**
 * Represents a step in a masterplan
 */
class Masterplan_Step {
	/**
	 * @var integer
	 */
	public $id;

	/**
	 * @var integer
	 */
	public $phase_id;

	/**
	 * @var integer
	 */
	public $step_nr;

	/**
	 * @var integer
	 */
	public $step_type;

	/**
	 * @var integer
	 */
	public $pos;

	/**
	 * @var integer
	 */
	public $title;

	/**
	 * @var integer
	 */
	public $color;

	/**
	 * @var integer
	 */
	public $value;

	/**
	 * @var integer
	 */
	public $responsible_role_id;

	/**
	 * @var integer
	 */
	public $apply_corporate;

	/**
	 * @var integer
	 */
	public $apply_franchisee;

	/**
	 * @var integer
	 */
	public $apply_other;

	/**
	 * @var integer
	 */
	public $apply_store;

	/**
	 * @var integer
	 */
	public $apply_sis;

	/**
	 * @var integer
	 */
	public $apply_kiosk;

	/**
	 * @var integer
	 */
	public $apply_independent;
	
	/**
	 * @var integer
	 */
	public $is_active;

	/**
	 * Read from db
	 * @param  integer $id
	 * @return Masterplan_Step
	 */
	public static function read($id) {
		$model = new Project_Masterplan_Step_Model;
		return $model->read($id);
	}
}