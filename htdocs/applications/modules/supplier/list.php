<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

$model = new Model(Connector::DB_CORE);

// sql order
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'address_company';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

// filter: full text search
if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
	
	$keyword = $_REQUEST['search'];
	
	$filters['search'] = "( 
		address_company LIKE \"%$keyword%\"
		OR country_name LIKE \"%$keyword%\"
		OR place_name LIKE \"%$keyword%\" 
	)";
}

$filters['active'] = "item_active = 1";

// datagrid
$result = $model->query("
	SELECT DISTINCT
		addresses.address_id,
		addresses.address_company,
		countries.country_name,
		places.place_name
	FROM items
	INNER JOIN suppliers ON items.item_id = suppliers.supplier_item
	INNER JOIN addresses ON suppliers.supplier_address = addresses.address_id
	INNER JOIN places ON addresses.address_place_id = places.place_id
	INNER JOIN countries ON addresses.address_country = countries.country_id
")
->filter($filters)
->order($sort, $direction)
->offset($offset, $rowsPerPage)
->fetchAll();

if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);

	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// toolbox: button print
if ($datagrid && $_REQUEST['print'])  {
	$toolbox[] = ui::button(array(
		'id' => 'print',
		'icon' => 'print',
		'href' => $_REQUEST['print'],
		'label' => $translate->print
	));
}

// toolbox: add
if ($_REQUEST['add'])  {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();

if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

$table->datagrid = $datagrid;
$table->dataloader($dataloader);

$table->address_company(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	"href=".$_REQUEST['form']
);

$table->country_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=30%'
);

$table->place_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=30%'
);

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;
	