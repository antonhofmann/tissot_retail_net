<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

define('DEBUGGING_MODE', false);

$user = User::instance();
$translate = Translate::instance();

$region = $_REQUEST['region'];
$action = $_REQUEST['action'];

$db = Connector::get();

// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$response = array();
$error = false;

$company = new Company();
$company->read($_REQUEST['company']);

if (!Request::isAjax() || !$user->id || !$company->id) {
	
	$response['notifications'][] = array(
		'type' => 'error',
		'title' => 'Error',
		'text' => 'Bad request.'
	);

	goto BLOCK_RESPONSE;

} else {
	$response['company'] = $company->id;
}

// get supplier items
$sth = $db->prepare("
	SELECT DISTINCT item_id
	FROM suppliers
	INNER JOIN items ON item_id = supplier_item
	WHERE item_active = 1 AND supplier_address = ?
");

$sth->execute(array($company->id));
$result = $sth->fetchAll();

if (!$result) {

	$response['notifications'][] = array(
		'type' => 'error',
		'title' => 'Error',
		'text' => 'Supplier has not items.'
	);

	goto BLOCK_RESPONSE;

} else {
	
	foreach ($result as $row) {
		$_ITEMS[] = $row['item_id'];
	}
}

		


// submitter :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

switch ($_REQUEST['section']) {
	
	case 'country':

		$country = $_REQUEST['country'];

		if (!$country) {

			$response['notifications'][] = array(
				'type' => 'error',
				'title' => 'Error',
				'text' => 'Bed request'
			);

			goto BLOCK_RESPONSE;
		}

		// get current items
		$sth = $db->prepare("
			SELECT GROUP_CONCAT(DISTINCT item_country_item_id) AS items
			FROM item_countries
			INNER JOIN suppliers ON supplier_item = item_country_item_id
			WHERE item_country_country_id = ? AND supplier_address = ?
		");

		$sth->execute(array($country, $company->id));
		$result = $sth->fetch();
		$_CURRENT_ITEMS = $result ? explode(',', $result['items']) : array();

		if (DEBUGGING_MODE) {
			$response['current.items'] = $_CURRENT_ITEMS;
		}

		if ($action) {

			$sth = $db->prepare("
				INSERT INTO item_countries (
					item_country_item_id,
					item_country_country_id,
					user_created
				)
				VALUES (?,?,?)
			");

			foreach ($_ITEMS as $item) {
				if (!in_array($item, $_CURRENT_ITEMS)) {
					if (DEBUGGING_MODE) $response['debugger']['add.item'][] = $item;
					else $sth->execute(array($item, $country, $user->login));
				}
			}

			$response['success'] = true;

		} else {

			$sth = $db->prepare("
				DELETE FROM item_countries
				WHERE item_country_item_id = ? AND item_country_country_id = ?
			");

			foreach ($_ITEMS as $item) {
				if (in_array($item, $_CURRENT_ITEMS)) {
					if (DEBUGGING_MODE) $response['debugger']['delete.item'][] = $item;
					else $sth->execute(array($item, $country));
				}
			}

			$response['success'] = true;
		}

	break;

	case 'countries':

		$region = $_REQUEST['region'];
		$countries = $_REQUEST['countries'];

		// request validation
		if (!$region || ($action && !$countries)) {

			$response['notifications'][] = array(
				'type' => 'error',
				'title' => 'Error',
				'text' => 'Bed request'
			);

			goto BLOCK_RESPONSE;
		}

		$_REGION_COUNTRIES = array();

		// get region countries
		$sth = $db->prepare("
			SELECT country_id
			FROM countries
			WHERE country_region = ?
		");

		$sth->execute(array($region));
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$_REGION_COUNTRIES[] = $row['country_id'];
			}
		}

		if (!$_REGION_COUNTRIES) {

			$response['notifications'][] = array(
				'type' => 'error',
				'title' => 'Error',
				'text' => 'Region has not countries.'
			);

			goto BLOCK_RESPONSE;
		}

		$keyCountries = $_REGION_COUNTRIES ? join(',', $_REGION_COUNTRIES) : null;

		// get current items from item countries
		$sth = $db->prepare("
			SELECT item_country_country_id, item_country_item_id
			FROM item_countries
			INNER JOIN suppliers ON supplier_item = item_country_item_id
			WHERE item_country_country_id IN ($keyCountries) AND supplier_address = ?
		");

		$sth->execute(array($company->id));
		$result = $sth->fetchAll();
		
		$_CURRENT_ITEMS = array();

		if ($result) {
			foreach ($result as $row) {
				$k = $row['item_country_country_id'];
				$_CURRENT_ITEMS[$k][] = $row['item_country_item_id'];
			}
		}

		if (DEBUGGING_MODE) {
			$response['current.items'] = $_CURRENT_ITEMS;
		}

		if ($action) {

			$sth = $db->prepare("
				INSERT INTO item_countries (
					item_country_item_id,
					item_country_country_id,
					user_created
				)
				VALUES (?,?,?)
			");

			foreach ($_ITEMS as $item) {
				foreach ($countries as $country) {
					if ($_CURRENT_ITEMS[$country] && in_array($item, $_CURRENT_ITEMS[$country])) {
						if (DEBUGGING_MODE) $response['debugger']['item.exist'][$country][] = $item;
					}
					else {
						if (DEBUGGING_MODE) $response['debugger']['add.item'][$country][] = $item;
						else $sth->execute(array($item, $country, $user->login));
					}
				}
			}

		} else {

			if ($result) {

				// for selected region
				// remove all items from item counties
				$sth = $db->prepare("
					DELETE FROM item_countries
					WHERE item_country_item_id = ? AND item_country_country_id = ?
				");

				foreach ($_ITEMS as $item) {
					foreach ($_REGION_COUNTRIES as $country) {
						if ($_CURRENT_ITEMS[$country] && in_array($item, $_CURRENT_ITEMS[$country]) ) {
							if (DEBUGGING_MODE) $response['debugger']['delete.item'][$country][] = $item;
							else $sth->execute(array($item, $country));
						}
					}
				}
			}
		}

		$response['success'] = true;

	break;
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONSE:

ob_clean();
header('Content-Type: text/json');
echo json_encode($response);
