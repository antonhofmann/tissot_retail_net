<?php

	class Ordersheet_Item_Planned_Quantity {
		
		/**
		 * Item ID
		 * @var integer
		 */
		public $id;
				
		/**
		 * Order Sheet Item ID
		 * @var integer
		 */
		public $item;
		
		/**
		 * Item Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Database Model
		 * @var object
		 */
		protected $model;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Join Order Sheet Items
		 * @var string
		 */
		const DB_BIND_ORDERSHEET_ITEMS = 'INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_planned_quantity_ordersheet_item_id';
			
		/**
		 * Join POS Addresses
		 * @var unknown
		 */
		const DB_BIND_POSADDRESSES = 'INNER JOIN db_retailnet.posaddresses ON posaddress_id = mps_ordersheet_item_planned_quantity_posaddress_id';
		
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Ordersheet_Item_Planned_Quantity_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_ordersheet_item_planned_quantity_$key"];
		}
		
		/**
		 * Define Order Sheet Item
		 * @param object $item
		 */
		public function setItem($item) {
			$this->item = $item;
		}
		
		/**
		 * Get Item Planned Quantity
		 * @param integer $id, quantity identificator
		 * @return array quantity
		 */
		public function read($id) {
			
			$data = $this->model->read($id);
			
			if ($data) {
				$this->data = $data;
				$this->id = $data['mps_ordersheet_item_planned_quantity_id'];
				$this->item = $data['mps_ordersheet_item_planned_quantity_ordersheet_item_id'];
				return $data;
			}
		}
		
		/**
		 * Add New POS Quantity
		 * @param integer $quantity
		 * @return integer inserted ID
		 */
		public function add_pos($pos, $quantity) {
			if ($this->item) {
				$id = $this->model->add_pos($this->item, $pos, $quantity);
				$this->read($id);
				return $id;
			}
		}
		
		/**
		 * Add New Warehouse Quantity
		 * @param integer $warehouse
		 * @return integer inserted ID
		 */
		public function add_warehouse($warehouse, $quantity) {
			if ($this->item) {
				$id = $this->model->add_warehouse($this->item, $warehouse, $quantity);
				$this->read($id);
				return $id;
			}
		}
		
		/**
		 * Update Warehouse Item Quantity
		 * @param integer $quantity
		 * @return boolean, true on success
		 */
		public function update($quantity) {
			if ($this->id) {
				$result = $this->model->update($this->id, $quantity);
				$this->read($this->id);
				return $result;
			}
		}

		/**
		 * Remove Item Quantity
		 * @return boolean, true on success
		 */
		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		/**
		 * Remove all Order sheet Item quantites
		 * @return boolean, true on success
		 */
		public function delete_all() {
			if ($this->item) {
				return $this->model->delete_all($this->item);
			}
		}
		
		/**
		 * Load all item quantites
		 * @param array $filters, query filters
		 * @return array quantites
		 */
		public function load($filters=null) { 
			if ($this->item) { 
				return $this->model->load($this->item, $filters);
			}
		}
	}
	