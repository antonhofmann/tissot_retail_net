<?php

	/**
	 * Material Collection Category File
	 * @author aserifi
	 * @ copyright mediaparx ag
	 */
	class Material_Collection_Category_File {
		
		/**
		 * Material collection file ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Material collection file data
		 * @var array
		 */
		public $data;
		
		/**
		 * Material ID
		 * @var integer
		 */
		public $material;
		
		/**
		 * Database Connector
		 * @var string
		 */
		public $connector;
		
		/**
		 * Database Model
		 * @var object
		 */
		protected $model;
		
		/**
		 * Join file types
		 * @var string
		 */
		const DB_BIND_FILES_TYPES = 'INNER JOIN db_retailnet.file_types ON file_type_id = mps_material_collection_category_file_type';
		
		/**
		 * Join material collection categories
		 * @var string
		 */
		const DB_BIND_COLLECTION_CATEGORIES = 'INNER JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_collection_category_file_collection_id';
		
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Material_Collection_Category_File_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_material_collection_category_file_$key"];
		}
		
		public function material($material) {
			$this->material = $material;
		}
		
		public function read($id) {
			$this->data =$this->model->read($id);
			$this->id = $this->data['mps_material_collection_category_file_id'];
			return $this->data;
		} 
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}
		
		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}

		public static function loader($application, $filters=null) {
			$model = new Material_Collection_Category_File_Model($application);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
	}