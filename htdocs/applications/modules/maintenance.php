<?php 

	class Maintenance {
		
		/**
		 * Maintenance ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Maintenance Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @return Maintenance_Model
		 */
		protected $model;
		
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Maintenance_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["maintenance_window_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['maintenance_window_id'];
			return $this->data;
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}
		
		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function warnings() {
			
			$model = new Maintenance_Model(Connector::DB_CORE);
			$result = $model->warnings();
			
			if ($result) {
				
				foreach ($result as $row) {
					$content .= $row['content'];
				}
				
				return nl2br($content);
			}
		}
		
		public static function getActives() {
			$model = new Maintenance_Model(Connector::DB_CORE);
			return $model->getActives();
		}
	}
	