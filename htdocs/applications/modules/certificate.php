<?php

	class Certificate {

		public $data;

		protected $file;

		protected $material;

		protected $item;

		public function __construct() {
			$this->model = new Certificate_Model(Connector::DB_CORE);
		}

		public function __get($key) {
			return $this->data["certificate_$key"];
		}

		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['certificate_id'];
			return $this->data;
		}

		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}

		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}

		/**
		 * Get supplier company
		 * @return string
		 */
		public function getSupplierCompany() {
			if ($this->id) {
				$supplier = $this->model->getSupplier($this->id);
				return $supplier['address_company'];
			}
		}

		public function file() {
			
			if (!$this->file) {
				$this->file = new Certificate_File();
			}

			return $this->file;
		}

		public function material() {
			
			if (!$this->material) {
				$this->material = new Certificate_Material();
			}

			if ($this->id && $this->id <> $this->material->certificate) {
				$this->material->setCertificate($this->id);
			}

			return $this->material;
		}


		public function item() {
			
			if (!$this->item) {
				$this->item = new Certificate_Item();
			}

			if ($this->id && $this->id <> $this->item->certificate) {
				$this->item->setCertificate($this->id);
			}

			return $this->item;
		}
	}