<?php 

	class Pos_Closing_Day {
		
		const DB_BIND_POSADDRESSES = "INNER JOIN db_retailnet.posaddresses ON posaddress_id = posclosinghr_posaddress_id";
		
		public $data;
		
		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : Connector::DB_CORE;
			$this->model = new Pos_Closing_Day_Model($connector);
		}
		
		public function __get($key) {
			return $this->data["posclosinghr"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['posclosinghr_id'];
			return $this->data;
		}
		
		public function read_from_pos($pos) {
			$this->data = $this->model->read_from_pos($pos);
			$this->id = $this->data['posclosinghr_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
	}