<?php

	class MIS_Query {
		
		public $id;
		
		public $data;
		
		protected $model;
	
		public function __construct() {
			$this->model = new MIS_Query_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["mis_query_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mis_query_id'];
			return $this->data;
		}
	}