<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$permissionEdit = user::permission('can_edit_all_localizations');

$company = New Company();
$company->read($user->address);
$user_country = $company->country;


$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

// sql order
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'postype_name';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;
	

$model = new Model();
$model_store_locator = new Model(CONNECTOR::DB_RETAILNET_STORE_LOCATOR);


// filter: full text search
if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
	
	$search = $_REQUEST['search'];
	
	$filters['search'] = "(
		postype_name LIKE '%$search%'
	)";
}

//filter country
$fCountry = $_REQUEST['country'];
$default_country = '';
// filer: countries
if ($fCountry) {
	$default_country =  $fCountry;
}
else
{
	$default_country = $user_country;
}
$filters['country'] = "country_id = $default_country";


//get languages of the country
$languages = array();
$filters2['postype'] = "country_language_country_id = $default_country";

$languages = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		country_language_language_id,
		language_name,
		language_from_right_to_left
	FROM country_languages
	LEFT join languages on language_id = country_language_language_id
")
->filter($filters2)
->order('language_name', 'ASC')
->fetchAll();


//get storelocator_country
$filters2['postype'] = "country_id = $default_country";

$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		country_store_locator_id
	FROM countries
")
->filter($filters2)
->order('country_name')
->fetchAll();

$store_locator_country = $result[0]['country_store_locator_id'];

//get existing translations
$tmp = array();
foreach($languages as $key=>$language)
{
	$tmp[] = $language["country_language_language_id"];
}

$translations = array();
$filters2['postype'] = "loc_postype_country_id = " . $store_locator_country . " and loc_postype_language_id in (" . implode(",", $tmp) . ")";

$result = $model_store_locator->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		loc_postype_postype_id, 
		loc_postype_language_id, 
		loc_postype_name 
	FROM loc_postypes
")
->filter($filters2)
->fetchAll();



if($result)
{
	foreach($result as $key=>$translation)
	{
		$translations[$translation['loc_postype_postype_id']][$translation['loc_postype_language_id']] = $translation;
	}
}

//echo "<pre>";
//var_dump($translations);

//get countries
$result = $model_store_locator->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		postype_id,
		postype_name
	FROM postypes
")
->order($sort, $direction)
->fetchAll();

//echo $model_store_locator->sql;

if ($result) {

	$totalrows = $model_store_locator->totalRows();
	$datagrid = _array::datagrid($result);
	
	foreach($datagrid as $postype_id=>$fields)
	{
		foreach($languages as $language)
		{
			$translated_value = '';
			if(array_key_exists($postype_id, $translations))
			{
				$translated_value = $translations[$postype_id][$language['country_language_language_id']]['loc_postype_name'];
			}
			
			$datagrid[$postype_id]['postype_name_' . $language['country_language_language_id']] = $translated_value;
		}
	}

	//echo "<pre>";
	//var_dump($datagrid);

	/*
	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
	*/
	
}



// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
$toolbox[] = "<input type=hidden id=application name=application value='$application' />";
$toolbox[] = "<input type=hidden id=store_locator_country_id name=store_locator_country_id value='$store_locator_country' />";


// toolbox: serach full text
//$toolbox[] = ui::searchbox();

// dropdown countries ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
if(user::permission(Pos::PERMISSION_EDIT_LIMITED))
{
	/*
	$result = $model->query("
		SELECT DISTINCT country_id AS value, country_name AS caption 
		FROM countries
	")
	->filter($filters)
	->order('caption')
	->fetchAll();
	*/

	$result = $model->query("
		SELECT DISTINCT posaddress_country AS value, country_name AS caption 
		FROM posaddresses 
		LEFT JOIN countries on country_id = posaddress_country 
		LEFT join country_languages on country_language_country_id = posaddress_country
		where posaddress_client_id = " . $user->address .
	    " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') 
		and posaddress_export_to_web = 1
		AND country_language_language_id <> 36
	")
	->order('caption')
	->fetchAll();
}
else
{	
	$result = $model->query("
		SELECT DISTINCT country_id AS value, country_name AS caption 
		FROM countries
	")
	->order('caption')
	->fetchAll();
}

if ($result and $permissionEdit == true) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'country',
		'id' => 'country',
		'class' => 'submit',
		'value' => $default_country,
		'caption' => false
	));
}

// toolbox: form
$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";


$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

$table->datagrid = $datagrid;
$table->dataloader($dataloader);

$table->postype_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);

if($rtl == 1)
{
	$table->attributes("postype_name", array("style"=>"direction:RTL;"));
}

if(count($languages) > 0)
{
	foreach($languages as $language)
	{
		$fieldName = "postype_name_".$language['country_language_language_id'];
	
		
		$table->$fieldName(
			Table::ATTRIBUTE_NOWRAP,
			Table::DATA_TYPE_TEXTBOX,
			"caption=" .$language['language_name'],
			"data-language_id=" . $language['country_language_language_id']
		);

		if($language["language_from_right_to_left"] == 1)
		{
			$table->attributes($fieldName, array("style"=>"direction:RTL;"));
		}
	}
}

$table->footer($translate->total_rows . " " . $totalrows);
//$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;
