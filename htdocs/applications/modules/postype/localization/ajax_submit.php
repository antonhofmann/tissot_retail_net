<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	
	if($_REQUEST['store_locator_country_id'] > 0 and $_REQUEST['postype_id'] > 0 and $_REQUEST['language_id'])
	{
		$model_store_locator = new Model(CONNECTOR::DB_RETAILNET_STORE_LOCATOR);
		
		//check if translation is present in db retail net
		$sth = $model_store_locator->db->prepare("select count(loc_postype_id) as num_recs from loc_postypes
				WHERE loc_postype_country_id = ? and loc_postype_postype_id = ? and loc_postype_language_id = ?");
		$response = $sth->execute(array($_REQUEST['store_locator_country_id'], $_REQUEST['postype_id'], $_REQUEST['language_id']));

		if($response == true)
		{
			$row = $sth->fetch();
			if($row['num_recs'] == 0)
			{
				$query = "Insert into loc_postypes (loc_postype_country_id, loc_postype_language_id, loc_postype_postype_id, loc_postype_name) VALUES (?,?,?,?)";

				$sth = $model_store_locator->db->prepare($query);
				$response = $sth->execute(array($_REQUEST['store_locator_country_id'], $_REQUEST['language_id'], $_REQUEST['postype_id'], $_REQUEST['postype_name']));

			}
			else
			{
				$query = "UPDATE loc_postypes SET 
					loc_postype_name = ? 
				WHERE loc_postype_country_id = ? and loc_postype_postype_id = ? and loc_postype_language_id = ?";
				
				//update store locator
				$sth = $model_store_locator->db->prepare($query);
				$response = $sth->execute(array($_REQUEST['postype_name'], $_REQUEST['store_locator_country_id'],  $_REQUEST['postype_id'], $_REQUEST['language_id']));
			}
		}
	}

?>