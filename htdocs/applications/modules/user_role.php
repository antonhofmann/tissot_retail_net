<?php

	class User_Role {

		const DB_BIND_USERS = 'INNER JOIN db_retailnet.users ON user_id = user_role_user';
		
		public function __construct() {
			$this->model = new User_Role_Model(Connector::DB_CORE);
		}
		
		public function user($user) {
			$this->user = $user;
		}

		public function read($role) {
			if ($this->user) {
				return $this->model->get($this->user, $role);
			}
		}

		public function create($role) {
			if ($this->user) {
				return $this->model->create($this->user, $role);
			}
		}

		public function delete($role) {
			if ($this->user) {
				return $this->model->delete($this->user, $role);
			}
		}
		
		public function delete_all() {
			if ($this->user) {
				return $this->model->delete_all($this->user);
			}
		}
		
		public static function roles($user) {
			
			$model = new User_Role_Model(Connector::DB_CORE);
			$result = $model->roles($user);
			
			if ($result) {
				foreach ($result as $row) {
					$roles[] = $row['user_role_role'];
				}
			}
			
			return $roles;
		}
	}
