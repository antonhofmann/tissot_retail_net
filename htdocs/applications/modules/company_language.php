<?php 

	class Company_Language {
		
		public function __construct() {
			$this->model = new Company_Language_Model(Connector::DB_CORE);
		}
		
		public function id($id) {
			$this->id = $id;
		}
		
		public function read($language) {
			if ($this->id) {
				return $this->model->read($this->id, $language);
			}
		}
		
		public function create($language) {
			if ($this->id) {
				return $this->model->create($this->id, $language);
			}
		}
		
		public function delete($language) {
			if ($this->id) {
				return $this->model->delete($this->id, $language);
			}
		}
		
		public function read_all_languages() {
			if ($this->id) {
				$result = $this->model->read_all_languages($this->id);
				return _array::extract($result);
			}
		}
	}