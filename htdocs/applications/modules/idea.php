<?php

	class Idea {
		
		const PERMISSION_NEW = 'can_create_new_ideas';
		const PERMISSION_EDIT = 'can_edit_ideas';
		
		public $id;
		
		public $data;
		
		protected $category;
		
		protected $file;
		
		protected $model;
	
		public function __construct() {
			$this->model = new Idea_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["idea_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['idea_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		/**
		 * Idea Category
		 * 
		 * @return IdeaCategory
		 */
		public function category() {
			
			if (!$this->category) {
				$this->category = new IdeaCategory();
			}
			
			if ($this->idea_category_id <> $this->category->id) {
				$this->category->read($this->idea_category_id);
			}
			
			return $this->category;
		}
		
		/**
		 * Idea File
		 * 
		 * @return IdeaFile
		 */
		public function file() {
			
			if (!$this->file) {
				$this->file = new IdeaFile();
			}
			
			return $this->file;
		}
		
		
		public static function box() {
			
			if (user::permission(self::PERMISSION_NEW)) {
				
				$modal = new Template('idea.modal');
				$modal->data('categories', IdeaCategory::loader());
				
				$modal->data('fields', array(
					'extensions' => 'pdf,doc,docx,gif,jpg,jpeg,png'
				));
				
				$modal->data('attributes', array(
					'data-url' => '/applications/helpers/file.uploader.php',
					'data-upload-template-id' => 'idea-template-upload',
					'data-download-template-id' => 'idea-template-download',
					'data-auto-upload' => true,
					'data-max-number-of-files' => 3,
					'data-sequential-uploads' => true
				));
				
				echo $modal->show();
			}
		}
	}