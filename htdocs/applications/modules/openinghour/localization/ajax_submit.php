<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	if($_REQUEST['openinghr_id'] > 0 and $_REQUEST['language_id'] > 0)
	{
		
		$model = new Model();
		$model_store_locator = new Model(CONNECTOR::DB_RETAILNET_STORE_LOCATOR);

		//check if translation is present in db retail net
		$sth = $model_store_locator->db->prepare("select count(loc_openinghr_id) as num_recs from loc_openinghrs
				WHERE loc_openinghr_openinghr_id = ? and loc_openinghr_language_id = ?");
		$response = $sth->execute(array($_REQUEST['openinghr_id'], $_REQUEST['language_id']));


		if($response == true)
		{
			$row = $sth->fetch();
			if($row['num_recs'] == 0)
			{

				if(array_key_exists('openinghr_daytime', $_REQUEST))
				{

					$query = "Insert into loc_openinghrs (loc_openinghr_openinghr_id, loc_openinghr_language_id, loc_openinghr_daytime) VALUES (?,?,?)";

					$sth = $model_store_locator->db->prepare($query);
					$response = $sth->execute(array($_REQUEST['openinghr_id'],  $_REQUEST['language_id'],  $_REQUEST['openinghr_daytime']));

				}
				elseif(array_key_exists('openinghr_time_24', $_REQUEST))
				{
					$query = "Insert into loc_openinghrs (loc_openinghr_openinghr_id, loc_openinghr_language_id, loc_openinghr_time_24) VALUES (?,?,?)";

					$sth = $model_store_locator->db->prepare($query);
					$response = $sth->execute(array($_REQUEST['openinghr_id'],  $_REQUEST['language_id'],  $_REQUEST['openinghr_time_24']));
				}
				elseif(array_key_exists('openinghr_time_12', $_REQUEST))
				{
					$query = "Insert into loc_openinghrs (loc_openinghr_openinghr_id, loc_openinghr_language_id, loc_openinghr_time_12) VALUES (?,?,?)";

					$sth = $model_store_locator->db->prepare($query);
					$response = $sth->execute(array($_REQUEST['openinghr_id'],  $_REQUEST['language_id'],  $_REQUEST['openinghr_time_12']));
				}
			}
			else
			{
				
				if(array_key_exists('openinghr_daytime', $_REQUEST))
				{
					$query = "UPDATE loc_openinghrs SET 
						loc_openinghr_daytime = ? 
					WHERE loc_openinghr_openinghr_id = ? and loc_openinghr_language_id = ?";
					//update store locator
					$sth = $model_store_locator->db->prepare($query);
					$response = $sth->execute(array($_REQUEST['openinghr_daytime'], $_REQUEST['openinghr_id'], $_REQUEST['language_id']));
				}
				elseif(array_key_exists('openinghr_time_24', $_REQUEST))
				{
					$query = "UPDATE loc_openinghrs SET 
						loc_openinghr_time_24 = ? 
					WHERE loc_openinghr_openinghr_id = ? and loc_openinghr_language_id = ?";
					//update store locator
					$sth = $model_store_locator->db->prepare($query);
					$response = $sth->execute(array($_REQUEST['openinghr_time_24'], $_REQUEST['openinghr_id'], $_REQUEST['language_id']));
				}
				elseif(array_key_exists('openinghr_time_12', $_REQUEST))
				{
					$query = "UPDATE loc_openinghrs SET 
						loc_openinghr_time_12 = ? 
					WHERE loc_openinghr_openinghr_id = ? and loc_openinghr_language_id = ?";
					//update store locator
					$sth = $model_store_locator->db->prepare($query);
					$response = $sth->execute(array($_REQUEST['openinghr_time_12'], $_REQUEST['openinghr_id'], $_REQUEST['language_id']));
				}
				
			}
		}
	}

?>