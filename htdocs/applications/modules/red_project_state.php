<?php

	class Red_Project_State {

		public $data;

		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new Red_Project_State_Model($connector);
		}

		public function __get($name) {
			return $this->data['red_projectstate_'.$name];
		}

		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $id;
			return $this->data;
		}

		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}

		public function update($data) {
			if ($this->id) {
				return $this->model->update($this->id, $data);
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}

		public function owner() {
			$result = $this->model->owner($this->id);
			return ($result) ? true : false;
		}
		
		public static function dropdown_loader($connector, $filters=null) {
			$model = new Red_Project_State_Model($connector);
			$result = $model->dropdown_loader($filters);
			return _array::extract($result);
		}
	}
