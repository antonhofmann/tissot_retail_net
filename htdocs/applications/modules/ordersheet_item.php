<?php

	/**
	 * Order Sheet Items
	 * @author aserifi
	 * @copyright mediaparx ag
	 */
	class Ordersheet_Item {
		
		/**
		 * Item ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Order Sheet ID
		 * @var integer
		 */
		public $ordersheet;
		
		/**
		 * Item Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Planned quantities
		 * @return Ordersheet_Item_Planned
		 */
		public $planned;
		
		/**
		 * Delivered quantities
		 * @return Ordersheet_Item_Delivered
		 */
		public $delivered;
		
		/**
		 * Confirmed quantities
		 * @return Ordersheet_Item_Confirmed
		 */
		public $confirmed;
		
		/**
		 * Shipped quantities
		 * @return Ordersheet_Item_Shipped
		 */
		public $shipped;
		
		/**
		 * Database Model
		 * @return Ordersheet_Item_Model
		 */
		protected $model;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
	
		/**
		 * Join Order Sheets
		 * @var string
		 */
		const DB_BIND_ORDERSHEETS = 'INNER JOIN mps_ordersheets ON  mps_ordersheet_id = mps_ordersheet_item_ordersheet_id';
		
		/**
		 * Join Material
		 * @var string
		 */
		const DB_BIND_MATERIALS = 'INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id';
		
		/**
		 * Join currencies
		 * @var unknown
		 */
		const DB_BIND_CURRENCIES = 'INNER JOIN db_retailnet.currencies ON currency_id = mps_ordersheet_item_currency';
		
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Ordersheet_Item_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_ordersheet_item_$key"];
		}
		
		public function read($id) {
			
			$data = $this->model->read($id);
			
			if ($data) {
				$this->data = $data;
				$this->id = $data['mps_ordersheet_item_id'];
				$this->ordersheet = $data['mps_ordersheet_item_ordersheet_id'];
				return $data;
			}
		}
		
		public function read_from_material($material) {
			
			$data = $this->model->read_from_material($this->ordersheet, $material);
			
			if ($data) {
				$this->data = $data;
				$this->id = $data['mps_ordersheet_item_id'];
				$this->ordersheet = $data['mps_ordersheet_item_ordersheet_id'];
				return $data;
			}
		}
		
		public function read_from_purchase_orders($ponumber,$material) { 
			
			$data = $this->model->read_from_purchase_orders($ponumber, $material);
			
			if ($data) { 
				$this->data = $data;
				$this->id = $data['mps_ordersheet_item_id'];
				$this->ordersheet = $data['mps_ordersheet_item_ordersheet_id'];
				return $data;
			}
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public function deleteAll() {
			if ($this->ordersheet) {
				return $this->model->deleteAll($this->ordersheet);
			}
		}
		
		public function load($filters=null, $exract=true) { 
			if ($this->ordersheet) { 
				$result = $this->model->load($this->ordersheet, $filters);
				return ($result && $exract) ? _array::datagrid($result) : $result;
			}
		}
		
		/**
		 * Order Sheet Planned Quantity
		 * @return Ordersheet_Item_Planned
		 */
		public function planned($item=null) {
			
			// read item
			if ($item) $this->read($item);
			
			if (!$this->planned) {
				$this->planned = new Ordersheet_Item_Planned($this->connector);
			}
			
			$this->planned->ordersheet = $this->ordersheet_id;
			$this->planned->item = $this->id;
			return $this->planned;
		}
		
		/**
		 * Order sheet item delivered quantity
		 * @return Ordersheet_Item_Delivered
		 */
		public function delivered($item=null) {
		
			// read item
			if ($item) $this->read($item);
			
			if (!$this->delivered) {
				$this->delivered = new Ordersheet_Item_Delivered($this->connector);
			}
		
			$this->delivered->ordersheet = $this->ordersheet_id;
			$this->delivered->item = $this->id;
			return $this->delivered;
		}
		
		/**
		 * Order sheet item confirmed quantity
		 * @return Ordersheet_Item_Confirmed
		 */
		public function confirmed($item=null) {
			
			// read item
			if ($item) $this->read($item);
			
			if (!$this->confirmed) {
				$this->confirmed = new Ordersheet_Item_Confirmed($this->connector);
			}
			
			$this->confirmed->item = $this->id;
			return $this->confirmed;
		}
		
		/**
		 * Order sheet item confirmed quantity
		 * @return Ordersheet_Item_Shipped
		 */
		public function shipped($item=null) {
			
			// read item
			if ($item) $this->read($item);
			
			if (!$this->shipped) {
				$this->shipped = new Ordersheet_Item_Shipped($this->connector);
			}
			
			$this->shipped->item = $this->id;
			return $this->shipped;
		}
		
		/**
		 * Order sheet has planned quantities
		 * @return boolean
		 */
		public function hasPlannedQuantities() {
			if ($this->ordersheet) {
				return $this->model->hasPlannedQuantities($this->ordersheet);
			}
		}
		
		/**
		 * Order sheet has delivered quantities
		 * @return boolean
		 */
		public function hasDeliveredQuantities() {
			if ($this->ordersheet) {
				return $this->model->hasDeliveredQuantities($this->ordersheet);
			}
		}
		
		/**
		 * Order sheet has confirmed quantities
		 * @return boolean
		 */
		public function hasConfirmedQuantities() {
			if ($this->ordersheet) {
				return $this->model->hasConfirmedQuantities($this->ordersheet);
			}
		}
		
		/**
		 * Order sheet has shipped quantities
		 * @return boolean
		 */
		public function hasShippedQuantities() {
			if ($this->ordersheet) {
				return $this->model->hasShippedQuantities($this->ordersheet);
			}
		}
	}
	