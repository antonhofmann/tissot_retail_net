<?php 

	class Mail_Template_Recipient {
		
		/**
		 * Dataloader
		 * 
		 * @var array
		 */
		public $data;
		
		/**
		 * Template ID
		 * 
		 * @var int
		 */
		protected $template;
		
		/**
		 * DB Mmodel
		 * 
		 * @return Mail_Template_Recipient_Model
		 */
		protected $model;
		
		
		public function __construct() {
			$this->model = new Mail_Template_Recipient_Model();
		}
		
		public function setTemplate($template) {
			
			$this->template = $template;
		}
		
		/**
		 * Get all template recipients
		 * 
		 * @return array
		 */
		public function loadAll() {
			
			if ($this->template) {
				
				return $this->model->loadAll($this->template);
			}
		}
		
		public function read($user) {
			
			if ($this->template) {
				
				return $this->data = $this->model->read($this->template, $user);
			}
		}
		
		public function create($user, $cc=0) {
			
			$id = $this->model->read($this->template, $user, $cc);
			
			if ($id) {
				
				$this->data = array(
					'mail_template_recipient_id' => $id,
					'mail_template_recipient_template_id' => $this->template,
					'mail_template_recipient_user_id' => $user,
					'mail_template_recipient_cc' => $cc
				);
			}
			
			return $id;
		}
		
		public function delete($user) {
			
			if ($this->template) {
				
				return $this->model->delete($this->template, $user);
			}
		}
	}