<?php 

	class Company_Mps_sap_hq_ordertype {
		
		/**
		 * Mps_sap_hq_ordertype ID
		 * @var int
		 */
		public $id;
		
		/**
		 * Mps_sap_hq_ordertype Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Company Builder
		 * @return Company
		 */
		public $mps_sap_hq_ordertype;
		
		/**
		 * DB Company Mps_sap_hq_ordertype Model
		 * @return Mps_sap_hq_ordertype
		 */
		protected $model;
		
		/**
		 * DB: Join companies
		 * @var string
		 */
		const DB_BIND_COMPANIES = 'INNER JOIN db_retailnet.addresses ON address_id = mps_sap_hq_ordertype_address_id';

		const DB_BIND_SAP_HQ_ORDER_TYPES = 'INNER JOIN db_retailnet.sap_hq_order_types ON sap_hq_order_type_id = mps_sap_hq_ordertype_sap_hq_ordertype_id';

		const DB_BIND_SAP_HQ_ORDER_REASONS = 'LEFT JOIN db_retailnet.sap_hq_order_reasons ON sap_hq_order_reason_id = mps_sap_hq_ordertype_sap_hq_orderreason_id';
		
		public function __construct() {
			$this->model = new Company_Mps_sap_hq_ordertype_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["mps_sap_hq_ordertype_$key"];
		}
		
		public function id($id) {
			$this->id = $id;
		}
		
		public function read($id) {
			
			$this->data = $this->model->read($id);
			
			if ($this->data) {
				
				$this->id = $this->data['mps_sap_hq_ordertype_id'];
				
				if ($this->mps_sap_hq_ordertype) {
					$this->mps_sap_hq_ordertype->read($this->mps_sap_hq_ordertype_id);
				}
			}
			
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id) {
				return $this->model->update($this->id, $data);
			}
		}
		
		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		
		public function load($filters=null) {
			return $this->model->load($this->mps_sap_hq_ordertype->id, $filters);
		}
	
		
	}