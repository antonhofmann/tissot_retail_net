<?php 


	class ProjectTracker {
		
		/**
		 * State ID
		 * 
		 * @var int
		 */
		public $id;
		
		/**
		 * Dataloader
		 * 
		 * @var array
		 */
		public $data;
		
		/**
		 * DB Model
		 * 
		 * @return ProjectTrackerModel
		 */
		protected $model;
		
		
		public function __construct() {
		
			$this->model = new ProjectTrackerModel();
		}
		
		public function __get($key) {
			return $this->data["projecttracking_$key"];
		}
		
		public function read($id) {
				
			$this->id = $id;
				
			return $this->data = $this->model->read($id);
		}
		
		public function create($data) {
				
			if (is_array($data)) {
		
				$this->data = $data;
		
				return $this->id = $this->model->create($data);
			}
		}
		
		public function delete() {
				
			if ($this->id) {
		
				return $this->model->delete($this->id);
			}
		}
	}