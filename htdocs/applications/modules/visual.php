<?php 

	class Visual {
		
		const PERMISSION_ACCESS = 'has_access_to_mps_visuals';
		const PERMISSION_EDIT = 'can_edit_mps_visuals';
		const PERMISSION_VIEW = 'can_view_mps_visuals';
		
		public $data;
	
		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new Visual_Model($connector);
		}
		
		public function __get($key) {
			return $this->data["mps_visual_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_visual_id'];
			return $this->data;
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($application, $filters=null) {
			$model = new Visual_Model($application);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
		
		public function header() {
			if ($this->id) {
				return $this->code.', '.$this->name;
			}
		}
	}