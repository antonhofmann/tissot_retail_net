<?php

	class Supplier { 
		
		const DB_BIND_COMPANIES = 'INNER JOIN db_retailnet.addresses ON address_id = supplier_address';
		const DB_BIND_ITEMS = 'INNER JOIN db_retailnet.items ON item_id = supplier_item';
		const DB_BIND_STOCKLISTS = 'INNER JOIN db_retailnet.scpps_stocklists ON scpps_stocklist_supplier_address = supplier_address';
		const DB_BIND_CURRENCIES = 'INNER JOIN currencies ON currency_id = supplier_item_currency';
		
		public $data;

		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : Connector::DB_CORE;
			$this->model = new Supplier_Model($connector);
		}
		
		public function __get($key) {
			return $this->data["supplier_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['supplier_id'];
			return $this->data;
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
	
		public static function loader($filters=null) {
			$model = new Supplier_Model(Connector::DB_CORE);
			$result = $model->loader($filters);
			return _array::extract($result);
		}

		public function getFromItem($item) {
			$this->data = $this->model->getFromItem($item);
			$this->id = $this->data['supplier_id'];
			return $this->data;
		}

		public function updatePrice($price) {

			if (!$this->id) return;

			$user = User::instance();
			//$price = Item::roundPrice($price);

			$sth = $this->model->db->prepare("
				UPDATE suppliers SET 
					supplier_item_price = ?,
					user_modified = ?,
					date_modified = NOW()
				WHERE supplier_id = ?
			");

			$success = $sth->execute(array($price, $user->login, $this->id));

			if ($success) {
				$this->data['supplier_item_price'] = $price;
			}

			return $success;
		}
	}