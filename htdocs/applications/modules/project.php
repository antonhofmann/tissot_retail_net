<?php 

	
/**
 * Project Builder
 * 
 * @author adoweb
 * @copyright Mediaparx AG
 * @package Project
 */
class Project {
	
	/**
	 * Project ID
	 * 
	 * @var int
	 */
	public $id;
	
	/**
	 * Dataloader
	 * @var array
	 */
	public $dataloader;

	/**
	 * Internal storage of actual order values
	 * @var array
	 */
	protected $data;
	
	/**
	 * DB Model
	 * 
	 * @return ProjectModel 
	 */
	protected $model;
	
	/**
	 * Project Order
	 * 
	 * @return Order
	 */
	protected $_order;
	
	/**
	 * Project Type
	 * 
	 * @return ProjectType
	 */
	protected $_type;
	
	/**
	 * Project Subclass
	 * 
	 * @return ProjectSubClass
	 */
	protected $_subclass;
	
	/**
	 * Project State
	 * 
	 * @return ProjectState
	 */
	protected $_state;
	
	/**
	 * Product Line
	 * 
	 * @return ProductLine
	 */
	protected $_productline;
	
	/**
	 * Product Line Subclass
	 * 
	 * @return ProductLineSubClass
	 */
	protected $_productline_subclass;
	
	/**
	 * Project tracker
	 * 
	 * @return ProjectTracker
	 */
	protected $_track;
	
	
	public function __construct() {
		$this->model = new ProjectModel();
	}
	
	public function __get($key) {
		return $this->data["project_$key"];
	}

	/*public function __set($key, $value) {
		if (substr($key, 0, 8) == 'project_') {
			$this->data[$key] = $value;
		}
	}*/
	
	public function read($id) {
			
		$this->id = $id;
			
		return $this->data = $this->model->read($id);
	}

	/**
	 * Read project by project_number instead of id
	 * @param  string $projectNr
	 * @return array  Array of data
	 */
	public function readByProjectNumber($projectNr) {
		$this->data = $this->model->readByProjectNumber($projectNr);
		$this->id = $this->data['id'];
		return $this->data;
	}
	
	public function create($data) {
			
		if (is_array($data)) {
	
			$this->data = $data;
	
			return $this->id = $this->model->create($data);
		}
	}
	
	public function update($data) {
			
		if ($this->id) {
	
			$this->data = array_merge($this->data, $data);
	
			return $this->model->update($this->id, $data);
		}
	}
	
	public function delete() {
			
		if ($this->id) {
	
			return $this->model->delete($this->id);
		}
	}
	
	/**
	 * Project Order
	 * 
	 * @return Order
	 */
	public function order() {
		
		if (!$this->_order) {
			
			$this->_order = new Order();
			$this->_order->read($this->order);
		}

		return $this->_order;
	}
	
	/**
	 * Project State
	 * 
	 * @return ProjectState
	 */
	public function state() {
		
		if (!$this->_state) {
			
			$this->_state = new ProjectState();
		}

		return $this->_state;
	}
	
	/**
	 * Project Tracker
	 * 
	 * @return ProjectTracker
	 */
	public function track() {
		
		if (!$this->_track) {
			
			$this->_track = new ProjectTracker();
		}

		return $this->_track;
	}

	/**
	 * Get country corresponding to this project
	 * @return Country
	 */
	public function getCountry() {
		$Order = $this->order();

		$Country = new Country;
		$Country->read($Order->shop_address_country);
		return $Country;
	}

	/**
	 * Get sales region corresponding to this project
	 * @return array
	 */
	public function getSalesRegion() {
		$Country = $this->getCountry();
		$CountryMapper = new Country_Datamapper();
		return $CountryMapper->getSalesRegionForCountry($Country->id);
	}

	/**
	 * Get project's local retail coordinator
	 * @return User
	 */
	public function getRetailCoordinator() {
		$User = new User();
		$User->read($this->local_retail_coordinator);
		return $User;
	}

	/**
	 * Get project's design contractor
	 * @return User
	 */
	public function getDesignContractor() {
		$User = new User();
		$User->read($this->design_contractor);
		return $User;
	}

	/**
	 * Get project's design supervisor
	 * @return User
	 */
	public function getDesignSupervisor() {
		$User = new User();
		$User->read($this->design_supervisor);
		return $User;
	}

	/**
	 * Get project's POS address
	 * @param  boolean $fullAddress Include full address with zip and place
	 * @return string
	 */
	public function getPOSAddress($fullAddress = true) {
		$Order = $this->order();

		$Country = $this->getCountry();
		
		if ($fullAddress) {
			$address = sprintf('%s, %s %s, %s',
				$Order->shop_address_company,
				$Order->shop_address_zip,
				$Order->shop_address_place,
				$Country->name
			);
		}
		else {
			$address = sprintf('%s, %s',
				$Order->shop_address_company,
				$Country->name
			);
		}

		return $address;
	}
	
}