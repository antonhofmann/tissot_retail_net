<?php 

	class Pos_Verification_Data {
		
		/**
		 * ID
		 * @var int
		 */
		public  $id;
	
		/**
		 * Dataloader
		 * @var array
		 */
		public $data;
		
		/**
		 * POS Builder
		 * @return Pos
		 */
		public $verification;
		
		/**
		 * MPS Distribution Channels
		 * @return DistChannel
		 */
		public $distribution_channel;
		
		/**
		 * POS Verification Pos Model
		 * @return Pos_Verification_Data_Model
		 */
		protected $model;
		
		/**
		 * DB Connector name
		 * @var string
		 */
		protected $connector;
		
		/**
		 * DB JOIN: posaddresses
		 * @var string
		 */
		const DB_BIND_POS_VERIFICATIONS = "INNER JOIN posverifications ON posverification_id = posverification_data_posverification_id";
		
		/**
		 * DB JOIN: posaddresses
		 * @var string
		 */
		const DB_BIND_DISTRIBUTION_CHANNELS = "INNER JOIN db_retailnet.mps_distchannels ON mps_distchannel_id = posverification_data_distribution_channel_id";
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : Connector::DB_CORE;
			$this->model = new Pos_Verification_Data_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["posverification_data_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['posverification_data_id'];
			$this->verification = $this->data['posverification_data_posverification_id'];
			return $this->data;
		}
		
		public function read_verification($verification, $channel) {
			$this->data = $this->model->read_verification($verification, $channel);
			$this->id = $this->data['posverification_data_id'];
			$this->verification = $this->data['posverification_data_posverification_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		/**
		 * Set verification id
		 * @param int $id, pos verification id
		 */
		public function verification($id) {
			$this->verification = $id;
		}
		
		/**
		 * Load verification data
		 * @return array
		 */
		public function load() {
			if ($this->verification) {
				return $this->model->load($this->verification);
			}
		}
	}