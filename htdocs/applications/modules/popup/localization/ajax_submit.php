<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	if($_REQUEST['popup_id_string'] and $_REQUEST['language_id'] > 0)
	{
		
		$model_store_locator = new Model(CONNECTOR::DB_RETAILNET_STORE_LOCATOR);

		//check if translation is present in db retail net
		$sth = $model_store_locator->db->prepare("select * from poaddress_popups
				WHERE poaddress_popup_name = ?");
		$response = $sth->execute(array($_REQUEST['popup_id_string']));

		
		if($response == true)
		{
			$records = $sth->fetchAll();
			
			if(count($records) > 0)
			{
				foreach($records as $key=>$data)
				{
					
					$query = "delete from loc_poaddress_popups 
					   where loc_poaddress_popup_language_id = ?
					   and loc_poaddress_popup_poaddress_popup_id = ?";

					$sth = $model_store_locator->db->prepare($query);

					$response = $sth->execute(array($_REQUEST['language_id'],  $data["poaddress_popup_id"]));


					$query = "Insert into loc_poaddress_popups (loc_poaddress_popup_language_id, loc_poaddress_popup_poaddress_popup_id, loc_poaddress_popup_name) VALUES (?,?,?)";

					$sth = $model_store_locator->db->prepare($query);
					$response = $sth->execute(array($_REQUEST['language_id'],  $data["poaddress_popup_id"], $_REQUEST['popup_name']));
				}
			}
		}
	}

?>