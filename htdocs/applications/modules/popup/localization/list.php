<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$permissionEdit = user::permission('can_edit_all_localizations');

$company = New Company();
$company->read($user->address);
$user_country = $company->country;


$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", true);


//filter Language
$fLanguage= $_REQUEST['language'];
$default_Language = 0;
// filer: countries
if ($fLanguage) {
	$default_Language =  $fLanguage;
}


$model = new Model();
$model_store_locator = new Model(CONNECTOR::DB_RETAILNET_STORE_LOCATOR);

//get language name
$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		language_id, language_name,
		language_from_right_to_left
	FROM languages
")
->filter("language" , "language_id = " . $default_Language)
->fetchAll();

if($result)
{
	$language_name = $result[0]['language_name'];
	$rtl = $result[0]['language_from_right_to_left'];
}

if(!$default_Language)
{
	//get first of user's languages
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			language_id, language_name,
			language_from_right_to_left
		FROM languages
		left join country_languages on country_language_language_id = language_id 
	")
	->order("language_name")
	->filter("country_language", "country_language_country_id=" . $company->country)
	->offset(0, 1)
	->fetchAll();


	$default_Language = $result[0]['language_id'];
	$language_name = $result[0]['language_name'];
	$rtl = $result[0]['language_from_right_to_left'];
}



$translations = array();
$filters2['language'] = "loc_poaddress_popup_language_id = $default_Language";

$result = $model_store_locator->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		loc_poaddress_popup_name, poaddress_popup_name
	FROM loc_poaddress_popups
	left join poaddress_popups on poaddress_popup_id = loc_poaddress_popup_poaddress_popup_id
")
->filter($filters2)
->fetchAll();

//echo $model_store_locator->sql;

if($result)
{
	foreach($result as $key=>$translation)
	{
		$translations[$translation['poaddress_popup_name']] = $translation['loc_poaddress_popup_name'];
	}
}


//get popup posaddresses
$result = $model_store_locator->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		poaddress_popup_name as popup_name, 
		poaddress_popup_name
	FROM poaddress_popups
")
->order('poaddress_popup_name')
->fetchAll();


$datagrid = _array::datagrid($result);
$totalrows = $model_store_locator->totalRows();


foreach($datagrid as $popup_name=>$fields)
{
	
	$translated_value = '';
	if(array_key_exists($popup_name, $translations))
	{
		$translated_value = $translations[$popup_name];
	}
	$datagrid[$popup_name]['poaddress_popup_name_'] = $translated_value;

		
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
$toolbox[] = "<input type=hidden id=application name=application value='$application' />";


// dropdown languages ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
if(user::permission(Pos::PERMISSION_EDIT_LIMITED))
{
	$result = $model->query("
		SELECT DISTINCT language_id AS value, language_name AS caption 
		FROM languages
		left join country_languages on country_language_language_id = language_id
	")
	->order('caption')
	->filter("country_language", "country_language_country_id=" . $company->country)
	->fetchAll();
}
else
{
	$result = $model->query("
		SELECT DISTINCT language_id AS value, language_name AS caption 
		FROM languages
	")
	->order('caption')
	->fetchAll();
}


if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'language',
		'id' => 'language',
		'class' => 'submit',
		'value' => $default_Language,
		'caption' => false
	));
}

// toolbox: form
$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";


$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

$table->datagrid = $datagrid;
$table->dataloader($dataloader);

$table->poaddress_popup_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);


$table->poaddress_popup_name_(
	Table::ATTRIBUTE_NOWRAP,
	Table::DATA_TYPE_TEXTBOX,
	"caption=" . $language_name,
	"data-language_id=" . $default_Language
);

if($rtl == 1)
{
	$table->attributes("poaddress_popup_name_", array("style"=>"direction:RTL;"));
}




$table->footer($translate->total_rows . " " . $totalrows);
//$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;
