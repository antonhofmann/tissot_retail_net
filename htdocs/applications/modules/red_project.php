<?php

	class Red_Project {


		//Permissions
		const PERMISSION_VIEW_PROJECTS = "can_view_all_red_projects";
		const PERMISSION_VIEW_LIMITED_PROJECTS = "can_view_only_his_red_projects";
		const PERMISSION_DELETE_DATA = "can_delete_red_data";
		const PERMISSION_EDIT_PROJECT_SHEET = "can_edit_red_project_sheet";
		const PERMISSION_VIEW_PARTNERLIST = "can_view_red_project_partnerlist";
		const PERMISSION_EDIT_PARTNERLIST = "can_edit_red_project_partnerlist";
		const PERMISSION_ADD_NEW_PROJECTS = "can_add_new_red_projects";
		const PERMISSION_EDIT_DESCRIPTION = "can_edit_red_project_description";
		const PERMISSION_EDIT_BASIC_DATA  = "can_edit_red_project_basic_data";
		const PERMISSION_ADD_COMMENTS     = "can_add_red_comments";
		const PERMISSION_ADD_FILES        = "can_add_red_files";
		const PERMISSION_EDIT_COMMENTS     = "can_edit_red_comments";
		const PERMISSION_EDIT_FILES        = "can_edit_red_files";

		const PROJECT_FILE_UPLOAD_PATH  = "/public/data/files/red/projects/";

		//Database binds
		const BIND_PROJECT_TYPES = "INNER JOIN db_retailnet_red.red_projecttypes ON red_projecttype_id  = red_project_projecttype_id";
		const BIND_PROJECT_STATES = "INNER JOIN db_retailnet_red.red_projectstates ON red_projectstate_id  = red_project_projectstate_id";
		const BIND_PARTNER_TYPE = "INNER JOIN db_retailnet_red.red_partnertypes ON red_partnertype_id  = red_project_partner_partnertype_id";
		const BIND_USER_ID = "INNER JOIN db_retailnet.users ON user_id  =  red_project_partner_user_id";
		const BIND_PROJECT_MANAGER = "INNER JOIN db_retailnet.users ON user_id  =  red_project_leader_user_id";
		const BIND_PROJECT_ASSISTENT = "LEFT JOIN db_retailnet.users ON user_id = red_project_assistant_user_id ";
		const BIND_ADDRESSES = "INNER JOIN db_retailnet.addresses ON address_id = red_project_partner_address_id";
		const BIND_ADDRESS_TYPE = "INNER JOIN db_retailnet.address_types ON address_type_id = address_type";
		const BIND_PROJECT_PARTNER = "INNER JOIN db_retailnet_red.red_project_partners ON red_project_id = red_project_partner_project_id 	";
		const BIND_PROJECT_SHEET = "INNER JOIN db_retailnet_red.red_projectsheets ON red_project_id = red_projectsheet_project_id";

		public $data;

		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new Red_project_Model($connector);
		}

		public function __get($key) {
			return $this->data['red_project_'.$key];
		}

		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $id;
			return $this->data;
		}

		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}

		public function update($data) {
			if ($this->id && is_array($data)) {
				return $this->model->update($this->id, $data);
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}

		/**
		*
		* Checks whether $user_id is a partner of $project_id
		* in consideration of red_project_partner_access_from and red_project_partner_access_until
		* @param $project_id
		* @param $user_id
		*/
		public function user_is_partner($user_id) {
			$result = $this->model->user_is_partner($this->id, $user_id);
			return ($result) ? true : false;
		}

		public function is_archive() {
			if ($this->id) {
				return ($this->enddate=='0000-00-00' || $this->enddate=='' || $this->enddate==null) ? false : true;
			}
		}
		
		/**
		 * Generate new project number
		 * @return string project number
		 */
		public function generateNumber() {
			
			$result = $this->model->query("
				SELECT
				 	red_sysparam_key,
					red_sysparam_value
				FROM red_sysparams
			")
			 ->filter('red_sysparams', 'red_sysparam_key = "KST"')
			->fetchAll();
		
			$kst = _array::get_first_value(_array::extract($result));
		
			$prjNumber = $kst.date("y");
		
			//get last prj number
			$result = $this->model->query("
				SELECT
				 	red_project_id,
					red_project_projectnumber
				FROM red_projects
			")
			->filter('project_number', 'red_project_projectnumber LIKE "'.$prjNumber.'%"')
			->order('red_project_projectnumber', 'DESC')
			->fetchAll();
		
			$lastNo = _array::get_first_value(_array::extract($result));
			$lastNo = sprintf('%03d', substr($lastNo, -3) + 1);
		
			if ($lastNo != 1000) {
				$prjNumber.= $lastNo;
				return $prjNumber;
			}
		}

	}
