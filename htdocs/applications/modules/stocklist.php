<?php

	class Stocklist { 
		
		const PERMISSION_ACCESS_ALL_STOCK_DATA = 'has_access_to_all_stock_data';
		const PERMISSION_ENTER_GLOBAL_ORDER = 'can_enter_global_order';
		const PERMISSION_CONFIRM_GLOBAL_ORDERS = 'can_confirm_global_orders';
		const PERMISSION_EDIT = 'can_edit_scpps';
		const PERMISSION_EDIT_LIMITED = 'can_edit_only_his_scpps';
		const PERMISSION_VIEW = 'can_view_stock';
		
		const DB_BIND_SUPPLIERS = 'INNER JOIN db_retailnet.suppliers ON supplier_address = scpps_stocklist_supplier_address';
		const DB_BIND_COMPANIES = 'INNER JOIN db_retailnet.addresses ON address_id = scpps_stocklist_supplier_address';
		const DB_BIND_STOCKLIST_ITEMS = 'INNER JOIN scpps_stocklist_items ON scpps_stocklist_item_stocklist_id = scpps_stocklist_id';
		const DB_BIND_STOCKLIST_WEEKS = 'INNER JOIN scpps_weeks ON scpps_week_stocklist_id = scpps_stocklist_id';
		
		public $data;
		public $item;

		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : Connector::DB_CORE;
			$this->model = new Stocklist_Model($connector);
			$this->item = new Stocklist_Item($connector);
		}
		
		public function __get($key) {
			return $this->data["scpps_stocklist_$key"];
		}
		
		public function read($id) {
			$this->id = $id;
			$this->item->id = $id;
			return $this->data = $this->model->read($id);
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public function get_weeks_data() {
			if ($this->id) {
				return $this->model->get_weeks_data($this->id);
			}
		}
	
		public static function loader($filters=null) {
			$model = new Stocklist_Model(Connector::DB_CORE);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
	}