<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// disabled fields
$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

// url request
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];

// product line
$id = $_REQUEST['catalog_material_id'];

$data = array();

switch ($application) {
			
	case 'mps':
	

		$tableName = 'mps_materials';

	break;

	case 'catalog':
		
		$tableName = 'catalog_materials';

		if (isset($_REQUEST['catalog_material_name'])) {
			$data['catalog_material_name'] = $_REQUEST['catalog_material_name'];
		}	

	break;
}


if ($data) {

	$material = new Modul($application);
	$material->setTable($tableName);
	$material->read($id);

	if ($material->id) {
		
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		
		$response = $material->update($data);
		
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		
		$response = $id = $material->create($data);
	
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		
		if ($response && $_REQUEST['redirect']) {
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect
));
