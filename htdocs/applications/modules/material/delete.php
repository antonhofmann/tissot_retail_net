<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

switch ($application) {
	
	case 'mps':
	
		$tableName = 'mps_materials';
		$permissionEdit = user::permission(Material::PERMISSION_EDIT);
	break;

	case 'catalog':
		$tableName = 'catalog_materials';
		$permissionEdit = user::permission('can_edit_catalog');
		$permissionEditCertificates = user::permission('can_edit_certificates');
	break;
}

$material = new Modul($application);
$material->setTable($tableName);
$material->read($id);

if ($material->id && ($permissionEdit || $permissionEditCertificates) ) {
	
	$response = $material->delete();
	
	if ($response) {
		Message::request_deleted();
		url::redirect("/$application/$controller");
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}