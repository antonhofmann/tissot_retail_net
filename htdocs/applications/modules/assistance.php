<?php 

	class Assistance {
		
		/**
		 * Assistance ID
		 * 
		 * @var int
		 */
		public $id;
		
		/**
		 * Dataloader
		 * 
		 * @var arrray
		 */
		public $data;
		
		/**
		 * Assistance Content
		 * 
		 * @return Assistance_Section
		 */
		protected $section;
		
		/**
		 * DB Model
		 * 
		 * @return Assistance_Model
		 */
		protected $model;
	
		
		public function __construct() {
			
			$this->model = new Assistance_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			
			return $this->data["assistance_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['assistance_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			
			if ($this->id) {
				
				$this->deleteAllSections();
				
				$this->model->delete($this->id);
				
				return true;
			}
		}
		
		/**
		 * Assistance Content
		 * 
		 * @return Assistance_Section
		 */
		public function section() {
			
			if (!$this->section) {
				
				$this->section = new Assistance_Section(Connector::DB_CORE);
			}
			
			return $this->section;
		}
		
		/**
		 * Get all sections
		 *
		 * @return array
		 */
		public function getAllSections() {
				
			if ($this->id) {
		
				return $this->model->getAllSections($this->id);
			}
		}
		
		/**
		 * Delete All Sections
		 * 
		 * @return boolean
		 */
		protected function deleteAllSections() {
			
			if ($this->id) {
				
				$sections = $this->getAllSections();
				
				if ($sections) {
					foreach ($sections as $row) {
						$this->section()->read($row['assistance_section_id']);
						$response = $this->section()->delete();
					}
				}
				
				return $response;
			}
		}
		
		/**
		 * 
		 * Load assistance content from URL
		 * @param string $url Navigation URL
		 * @return array Assistance title and content
		 */
		public static function show() {
			
			$link = trim($_SERVER['PHP_SELF'], '/');
			
			// for mvc request
			if ($link=='public/index.php') {
				$link = url::system_request();
			}
			

			$model = new Model(Connector::DB_CORE);
			
			$result = $model->query("
				SELECT DISTINCT
					assistance_section_id,
					assistance_id,
					assistance_title,
					assistance_section_title,
					assistance_section_content
				FROM assistance_sections
				INNER JOIN assistance_section_links ON assistance_section_link_section_id = assistance_section_id
				INNER JOIN assistances ON assistance_id = assistance_section_assistance_id
				WHERE assistance_section_published = 1 AND assistance_section_link_link = '$link'
				ORDER BY 
					assistance_section_order,
					assistance_title,
					assistance_section_title		
			")->fetchAll();
			
			if ($result) {
				
				// build help content
				foreach ($result as $row) {
					
					$assistance = $row['assistance_id'];
					$section = $row['assistance_section_id'];
					
					if (isset($datagrid) and array_key_exists('caption', $datagrid[$assistance]) and !$datagrid[$assistance]['caption']) {
						$datagrid[$assistance]['caption'] = $row['assistance_title'];
					}
					
					$datagrid[$assistance]['sections'][$section]['title'] = nl2br($row['assistance_section_title']);
					$datagrid[$assistance]['sections'][$section]['content'] = $row['assistance_section_content'];
				}
				
				// include styles
				echo '<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">';
				echo '<link rel="stylesheet" type="text/css" href="/public/scripts/assistance/assistance.css" />';
				echo '<script type="text/javascript" src="/public/scripts/assistance/assistance.js"  ></script>';

				// content contain more then one book
				$multiple = (count($datagrid) > 1) ? true : false;
				
				// help box caption
				if (count($datagrid) > 1) {
					$caption = 'Help';
				} else {
					reset($datagrid);
					$key = key($datagrid);
					$caption = '';
					if(array_key_exists('caption', $datagrid[$key])) {
						$caption = $datagrid[$key]['caption'];
					}
				}
				
				// box container
				echo "<div class=\"assistance assistance-box\">";
				
				// box caption
				echo "<div class=\"assistance-box-caption\">";
				echo "<i class=\"fa fa-question-circle trigger\"></i>";
				echo "<strong>$caption</strong>";
				echo "<a href='/applications/exports/pdf/assistance.php?url=$link' target=\"_blank\" class=\"fa fa-print\"></a>";
				echo "</div>";
				
				// box content
				echo "<div class=\"assistance-box-content\">";
				echo "<div class=\"topics\" >";
				
				foreach ($datagrid as $assistance => $row) {
					
					// assistance title for multiple contents
					if ($multiple) {
						echo "<div class=\"assistance-title\">";
						echo "<strong>".$row['caption']."</strong>";
						echo "<a href='/applications/exports/pdf/assistance.php?topic=$assistance' target=\"_blank\" class=\"fa fa-print\"></a>";
						echo "</div>";
					}
					
					// assistance sections
					foreach ($row['sections'] as $section => $data) {
						
						$title = $data['title'];
						$content = $data['content'];
						
						echo "
							<div class=\"assistance-section-title\">
								<i class=\"fa fa-caret-right\"></i> 
								<strong>$title</strong>
								<a href=\"/applications/exports/pdf/assistance.php?topic=$assistance&section=$section\" target = \"blank\" class=\"fa fa-print\"></a>
							</div>
							<div class=\"assistance-section-content\">$content</div>
						";
					}
				}
				
				echo "</div></div></div>";
			}
		}
	}
	