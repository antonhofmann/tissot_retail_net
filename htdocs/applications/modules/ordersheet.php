<?php

	class Ordersheet {
	
		/**
		 * Permission edit all order sheets
		 * @var string
		 */
		const PERMISSION_EDIT = 'can_edit_all_mps_order_sheets';
		
		/**
		 * Permission edit only owners order sheets
		 * @var string
		 */
		const PERMISSION_EDIT_LIMITED = 'can_edit_only_his_mps_order_sheets';
		
		/**
		 * Permission view all order sheet
		 * @var string
		 */
		const PERMISSION_VIEW = 'can_view_all_mps_order_sheets';
		
		/**
		 * Permission view only owner order sheet
		 * @var string
		 */
		const PERMISSION_VIEW_LIMITED = 'can_view_only_his_mps_order_sheets';
		
		/**
		 * Permission Manager, all permitted
		 * @var string
		 */
		const PERMISSION_MANAGE = 'can_manage_mps_order_sheets';
		
		/**
		 * Join Companies
		 * @return string
		 */
		const DB_BIND_COMPANIES = 'INNER JOIN db_retailnet.addresses ON mps_ordersheet_address_id = address_id';
		
		/**
		 * Join Master Sheets
		 * @return string
		 */
		const DB_BIND_MASTERSHEETS = 'INNER JOIN mps_mastersheets ON mps_mastersheet_id = mps_ordersheet_mastersheet_id';
		
		/**
		 * Join Workflow States
		 * @return string
		 */
		const DB_BIND_WORKFLOW_STATES = 'INNER JOIN mps_workflow_states ON mps_workflow_state_id = mps_ordersheet_workflowstate_id';
		
		/**
		 * Join Order Sheet Items
		 * @return string
		 */
		const DB_BIND_ORDERSHEET_ITEMS = 'INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_ordersheet_id = mps_ordersheet_id';
		
		/**
		 * Order Sheet ID
		 * @var int
		 */
		public $id;
		
		/**
		 * Order Sheet Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Master Sheet
		 * @return Mastersheet
		 */
		public $mastersheet;
		
		/**
		 * Company Builder
		 * @return Company
		 */
		public $company;
		
		/**
		 * Order Sheet State
		 * @return Ordersheet_State
		 */
		public $state;
		
		/**
		 * Order Sheet Item
		 * @return Ordersheet_Item
		 */
		public $item;
		
		/**
		 * Order Sheet Version
		 * @return Ordersheet_Version
		 */
		public $version;
		
		/**
		 * Order Sheet Warehouse
		 * @return Ordersheet_Warehouse
		 */
		public $warehouse;
		
		/**
		 * Order sheet workflow state
		 * @return Workflow_State
		 */
		public $workflowstate;
		
		/**
		 * DB Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Order Sheet
		 * @param string $connector
		 */
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Ordersheet_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_ordersheet_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_ordersheet_id'];
			$this->state()->build();
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		/**
		 * Order Sheet State
		 * @return Ordersheet_State
		 */
		public function state() {
			
			if (!$this->state) {
				$this->state = new Ordersheet_State();
			}
	
			$this->state->ordersheet = $this->data;
			return $this->state;
		}
		
		/**
		 * Order Sheet Item
		 * @return Ordersheet_Item
		 */
		public function item() {
			
			if (!$this->item) {
				$this->item = new Ordersheet_Item($this->connector);
			}
			
			$this->item->ordersheet = $this->id;
			return $this->item;
		}
		
		/**
		 * Order Sheet Version
		 * @return Ordersheet_Version
		 */
		public function version() {
			
			if (!$this->version) {
				$this->version = new Ordersheet_Version($this->connector);
			}
			
			$this->version->ordersheet = $this->id;
			return $this->version;
		}
		
		/**
		 * Order Sheet Warehouse
		 * @return Ordersheet_Warehouse
		 */
		public function warehouse() {
			
			if (!$this->warehouse) {
				$this->warehouse = new Ordersheet_Warehouse($this->connector);
			}
			
			if ($this->id) {
				$this->warehouse->ordersheet($this->id);
			}
			
			return $this->warehouse;
		}
		
		/**
		 * Master Sheet Builder
		 * @return Company
		 */
		public function company() {
			
			if (!$this->company) {
				$this->company = new Company();
			}
			
			if ($this->address_id) {
				$this->company->read($this->address_id);
			}
			
			return $this->company;
		}
		
		/**
		 * Master Sheet Builder
		 * @return Mastersheet
		 */
		public function mastersheet() {
			
			if (!$this->mastersheet) {
				$this->mastersheet = new Mastersheet($this->connector);
			}
			
			if ($this->mastersheet_id) {
				$this->mastersheet->read($this->mastersheet_id);
			}
			
			return $this->mastersheet;
		}

		/**
		 * Workflow state
		 * @return Workflow_State
		 */
		public function workflowstate() {
			
			if (!$this->workflowstate) {
				$this->workflowstate = new Workflow_State();
			}
				
			if ($this->workflowstate_id) {
				$this->workflowstate->read($this->workflowstate_id);
			}
				
			return $this->workflowstate;
		}
		
		/**
		 * Order Sheet Header
		 * @return string
		 */
		public function header() {
			
			if ($this->id) {
			
				$model = new Model($this->connector);
				
				$result = $model->query("
					SELECT address_company, country_name, mps_mastersheet_year, mps_mastersheet_name
					FROM mps_ordersheets
				")
				->bind(Ordersheet::DB_BIND_MASTERSHEETS)
				->bind(Ordersheet::DB_BIND_COMPANIES)
				->bind(Company::DB_BIND_COUNTRIES)
				->filter('ordersheet', 'mps_ordersheet_id = '.$this->id)
				->fetch();
				
				if ($result) {
					extract($result);
					return "$address_company, $country_name, $mps_mastersheet_year, $mps_mastersheet_name";
				}
			}
		}
		
		/**
		 * Has Order Sheet Items
		 * @return boolean true if has items
		 */
		public function hasItems() {
			if ($this->id) {
				$result = $this->model->hasItems($this->id);
				return ($result['total'] > 0) ? true : false;
			}
		}
		
		/**
		 * Check order sheet has approved quantities
		 * @return boolean
		 */
		public function hasOwnerQuantities() {
			if ($this->id) {
				return $this->model->hasOwnerQuantities($this->id);
			}
		}
		
		/**
		 * Check order sheet has approved quantities
		 * @return boolean
		 */
		public function hasApprovedQuantities() {
			if ($this->id) {
				return $this->model->hasApprovedQuantities($this->id);
			}
		}
		
		/**
		 * Order sheet has confirmed quantities
		 * @return boolean
		 */
		public function hasConfirmedQuantities() {
			if ($this->id) {
				return $this->model->hasConfirmedQuantities($this->id);
			}
		}
		
		/**
		 * Order sheet has shipped quantities
		 * @return boolean
		 */
		public function hasShippedQuantities() {
			if ($this->id) { 
				return $this->model->hasShippedQuantities($this->id);
			}
		}
		
		/**
		 * Order sheet has distributed quantities
		 * @return boolean
		 */
		public function hasDistributedQuantities() {
			if ($this->id) {
				return $this->model->hasDistributedQuantities($this->id);
			}
		}
		
		/**
		 * Order sheet has reserve quantities
		 * @return boolean
		 */
		public function hasReserveQuantities() {
			
			if ($this->id) {
				
				$model = new Model($this->connector);
				
				// count planning reserve quantities
				$planningReserve = $model->query("
					SELECT SUM(mps_ordersheet_item_planned_id) AS total
					FROM mps_ordersheet_item_planned
				")
				->bind(Ordersheet_Item_Planned::DB_BIND_ORDERSHEET_ITEMS)
				->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = ".$this->id)
				->filter('reserve', "mps_ordersheet_item_planned_warehouse_id IS NOT NULL ")
				->fetch();
				
				// count planning reserve quantities
				$distributedReserve = $model->query("
					SELECT SUM(mps_ordersheet_item_delivered_id) AS total
					FROM mps_ordersheet_item_delivered
				")
				->bind(Ordersheet_Item_Delivered::DB_BIND_ORDERSHEET_ITEMS)
				->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = ".$this->id)
				->filter('reserve', "mps_ordersheet_item_delivered_warehouse_id IS NOT NULL ")
				->fetch();
				
				return ($planningReserve['total'] > 0 || $distributedReserve['total'] > 0) ? true : false;
			}
		}
		
		/**
		 * Check is order sheet manually archived
		 * @return boolean
		 */
		public function isManuallyArchived() {
			
			$total = 0;
			
			if ($this->state()->isDistributed()) {
				
				$model = new Model($this->connector);
					
				$result = $model->query("
					SELECT COUNT(mps_ordersheet_item_id) as total
					FROM mps_ordersheet_items
				")
				->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
				->filter('ordersheet', 'mps_ordersheet_id = '.$this->id )
				->filter('shipped', 'mps_ordersheet_item_quantity_shipped > 0')
				->filter('manually', "(
					mps_ordersheet_item_quantity_distributed IS NULL
					OR mps_ordersheet_item_quantity_distributed < mps_ordersheet_item_quantity_shipped
				)")
				->fetch();
				
				$total = $result['total'];
			}
				
			return ($total > 0) ? true : false;
		}

		/**
		 * Is order sheet partially shipped
		 * @return boolean
		 */
		public function isPartiallyShipped() {
				
			$model = new Model($this->connector);
			
			$items = $model->query("
				SELECT 
					mps_ordersheet_item_id,
					mps_ordersheet_item_quantity_confirmed,
					mps_ordersheet_item_quantity_shipped	
				FROM mps_ordersheet_items
				INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
				WHERE mps_ordersheet_item_ordersheet_id = $this->id 
				AND mps_material_locally_provided != 1 
				AND mps_ordersheet_item_quantity_approved > 0
			")->fetchAll();
			
			if ($items) { 
				
				$confirmed = array();
				$shipped = array();
				$shortclose = array();
				$cancelled = array();
				
				// confirmed items
				$result = $model->query("
					SELECT 
						mps_ordersheet_item_id,
						mps_ordersheet_item_confirmed_quantity,
						mps_ordersheet_item_confirmed_order_state
					FROM mps_ordersheet_item_confirmed
					INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_confirmed_ordersheet_item_id = mps_ordersheet_item_id		
					WHERE mps_ordersheet_item_ordersheet_id = $this->id
					ORDER BY mps_ordersheet_item_confirmed_id DESC
				")->fetchAll();
				
				if ($result) {
					foreach ($result as $row) {
						$id = $row['mps_ordersheet_item_id'];
						if (!$confirmed[$id]) {
							$confirmed[$id] = array(
								'quantity' => $row['mps_ordersheet_item_confirmed_quantity'],
								'state' => $row['mps_ordersheet_item_confirmed_order_state']
							);
						}
					}
				}
				
				// shipped items
				$result = $model->query("
					SELECT 
						mps_ordersheet_item_id,
						mps_ordersheet_item_shipped_quantity,
						mps_ordersheet_item_shortclosed
					FROM mps_ordersheet_item_shipped
					INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_shipped_ordersheet_item_id = mps_ordersheet_item_id		
					WHERE mps_ordersheet_item_ordersheet_id = $this->id
					ORDER BY mps_ordersheet_item_shipped_id DESC
				")->fetchAll();
				
				if ($result) {
					foreach ($result as $row) {
						$id = $row['mps_ordersheet_item_id'];
						if (!$shipped[$id] && $confirmed[$id]) {
							$shipped[$id] = array(
								'quantity' => $row['mps_ordersheet_item_shipped_quantity'],
								'shortclosed' => $row['mps_ordersheet_item_shortclosed']
							);
						}	
					}
				}
				
				$completed = 0;
				$totalItems = 0;
				$totalConfimred = 0;
				
				foreach ($items as $item) {
					
					$totalItems++;
					
					$id = $item['mps_ordersheet_item_id'];
					
					if ($confirmed[$id]) {
						
						$totalConfimred++;
						
						if ($confirmed[$id]['state']!='02' && $shipped[$id]) {
							if ( ($confirmed[$id]['quantity']<=$shipped[$id]['quantity']) || ($shipped[$id]['quantity'] < $confirmed[$id]['quantity'] && $shipped[$id]['shortclosed']) ) {
								$completed++;
							}
						}
					}
				}
				
				// is partially shipped
				return ($totalItems==$totalConfimred && $totalItems==$completed) ? false : true;
			}
		}
		
		/**
		 * Exchange data completed
		 * @return boolean
		 */
		public function exchangeOrderCompleted() {
				
			$model = new Model($this->connector);
			
			$items = $model->query("
				SELECT 
					mps_ordersheet_item_id,
					mps_ordersheet_item_quantity_confirmed,
					mps_ordersheet_item_quantity_shipped	
				FROM mps_ordersheet_items
				INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
				WHERE mps_ordersheet_item_ordersheet_id = $this->id 
				AND mps_material_locally_provided != 1 
				AND mps_ordersheet_item_quantity_approved > 0
			")->fetchAll();
			
			if ($items) { 
				
				$confirmed = array();
				$shipped = array();
				$shortclose = array();
				
				// confirmed items
				$result = $model->query("
					SELECT 
						mps_ordersheet_item_id,
						mps_ordersheet_item_confirmed_quantity,
						mps_ordersheet_item_confirmed_order_state
					FROM mps_ordersheet_item_confirmed
					INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_confirmed_ordersheet_item_id = mps_ordersheet_item_id		
					WHERE mps_ordersheet_item_ordersheet_id = $this->id
					ORDER BY mps_ordersheet_item_confirmed_id DESC
				")->fetchAll();
				
				if ($result) {
					foreach ($result as $row) {
						
						$id = $row['mps_ordersheet_item_id'];
						
						if (!$confirmed[$id]) {
							$confirmed[$id] = array(
								'quantity' => $row['mps_ordersheet_item_confirmed_quantity'],
								'state' => $row['mps_ordersheet_item_confirmed_order_state']
							);
						}
					}
				}
				
				// shipped items
				$result = $model->query("
					SELECT 
						mps_ordersheet_item_id,
						mps_ordersheet_item_shipped_quantity,
						mps_ordersheet_item_shortclosed
					FROM mps_ordersheet_item_shipped
					INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_shipped_ordersheet_item_id = mps_ordersheet_item_id		
					WHERE mps_ordersheet_item_ordersheet_id = $this->id
					ORDER BY mps_ordersheet_item_shipped_id DESC
				")->fetchAll();
				
				if ($result) {
					foreach ($result as $row) {
						$id = $row['mps_ordersheet_item_id'];
						if (!$shipped[$id] && $confirmed[$id]) {
							$shipped[$id] = array(
								'quantity' => $row['mps_ordersheet_item_shipped_quantity'],
								'shortclosed' => $row['mps_ordersheet_item_shortclosed']
							);
						}	
					}
				}
				
				$completed = 0;
				$totalItems = 0;
				$totalConfimred = 0;

				foreach ($items as $item) {
					
					$totalItems++;
					
					$id = $item['mps_ordersheet_item_id'];
					
					if ($confirmed[$id]) {
						
						$totalConfimred++;
						
						$confirmedQuantity = $confirmed[$id]['quantity'];
						$shippedQuantity = $shipped[$id]['quantity'];
						$shortclose = $shipped[$id]['shortclosed'];
						
						if ($confirmed[$id]['state'] !='02' && $shipped[$id]) {
							
							// regulary shipped
							if ($confirmedQuantity <= $shippedQuantity) {
								$completed++;
							}
							// partially shipped
							elseif($shippedQuantity < $confirmedQuantity && $shortclose==1) {
								$completed++;
							}
							// cancelld
							elseif ($shippedQuantity==0 && $shortclose==0) {
								$completed++;
							}
						}
					}
				}
				// is partially shipped
				return ($totalItems==$totalConfimred && $totalItems==$completed) ? true : false;
			}
		}
		
		/**
		 * Exchange data cancelled
		 * @return boolean
		 */
		public function exchangeOrderCancelled() {
			
			$model = new Model($this->connector);
				
			$items = $model->query("
				SELECT COUNT(DISTINCT mps_ordersheet_item_id) AS total
				FROM mps_ordersheet_items
				INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
				WHERE 
					mps_ordersheet_item_ordersheet_id = $this->id
					AND mps_material_locally_provided != 1
					AND mps_ordersheet_item_quantity_approved > 0
			")->fetch();
			
			$cancelled = $model->query("
				SELECT COUNT(DISTINCT mps_ordersheet_item_confirmed_ordersheet_item_id) as total
				FROM mps_ordersheet_item_confirmed 
				INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_confirmed_ordersheet_item_id
				WHERE 
					mps_ordersheet_item_ordersheet_id = $this->id 
					AND mps_ordersheet_item_confirmed_order_state = '02'		
			")->fetch();
			
			return ($items['total'] == $cancelled['total']) ? true : false;
		}
		
		
		/**
		 * Check if all shipped quantities are distributed
		 * @return boolean
		 */
		public function isDistributed() {
			return $this->model->isDistributed($this->id);
		}
		
		/**
		 * Check access to order sheet
		 * @return boolean
		 */
		public function access() {
			
			$access = false;
			$user = User::instance(); 
			
			if ($this->id && $user->id) {
				
				// permissions
				$permission_view = user::permission(self::PERMISSION_VIEW);
				$permission_view_limited = user::permission(self::PERMISSION_VIEW_LIMITED);
				$permission_edit = user::permission(self::PERMISSION_EDIT);
				$permission_edit_limited = user::permission(self::PERMISSION_EDIT_LIMITED);
				$permission_manage = user::permission(self::PERMISSION_MANAGE);
					
				// administrator access 
				if ($permission_manage || $permission_view || $permission_edit) { 
					$access = true;
				} 
				// owner access
				elseif ($permission_view_limited || $permission_edit_limited) { 
					$access = $this->address_id==$user->address ? true : false;
				}

				// country access
				if (!$access) { 

					$company = new Company();
					$company->read($this->address_id);

					$model = new Model(Connector::DB_CORE);
					
					$result = $model->query("
						SELECT COUNT(country_access_id) as total
						FROM country_access
						WHERE country_access_user = $user->id AND country_access_country = $company->country
					")->fetch();

					$access = $result['total'] > 0 ? true : false;
				}

				// regional access
				if (!$access) { 

					// regional access companies
					$regionalAccessCompanies = User::getRegionalAccessCompanies();

					if ($regionalAccessCompanies) {
						foreach ($regionalAccessCompanies as $company) {
							if ($this->address_id==$company) {
								$access = true;
								break;
							}
						}
					}
				}			
			}

			return $access;
		}
		
		public function trackings() {
			
			// order sheet tracking
			if ($this->id) {
				
				$trackings = array();
				
				$model = new Model($this->connector);
			
				$result = $model->query("
					SELECT
						user_tracking_id,
						DATE_FORMAT(user_trackings.date_created,'%d.%m.%Y') AS date,
						user_tracking_action,
						CONCAT(user_firstname,' ', user_name) AS user, 
						user_trackings.user_created as user2
					FROM user_trackings
				")
				->bind(User_Tracking::DB_BIND_USERS)
				->filter('entity', "user_tracking_entity = '".User_Tracking::ENTITY_ORDER_SHEETS."'")
				->filter('ordersheet', "user_tracking_entity_id = ".$this->id)
				->fetchAll();
			
				if ($result) {	
					foreach ($result as $row) {
						$trackings[] = array(
							'date' => $row['date'],
							'workflow_state' => ucfirst($row['user_tracking_action']),
							'user' => ($row['user']) ? $row['user'] : $row['user2']
						);
					}
				}
				
				return $trackings;
			}
		}
	}