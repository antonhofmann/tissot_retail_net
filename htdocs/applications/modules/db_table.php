<?php

	class DB_Table { 
		
		const PERMISSION_EDIT = 'can_administrate_system_data';
		
		public $data;

		public function __construct() {
			$this->model = new DB_Table_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["db_table_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['db_table_id'];
			return $this->data;
		}
		
		public function read_from_name($table, $db=null) {
			$db = ($db) ? $db : Connector::DB_CORE;
			$this->data = $this->model->read_from_name($table, $db);
			$this->id = $this->data['db_table_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($filters=null) {
			$model = new DB_Table_Model(Connector::DB_CORE);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
		
		public function get_references() {
			if ($this->id) {
				return $this->model->get_references($this->id);
			}
		}
		
		public function delete_references() {
			if ($this->id) {
				return $this->data = $this->model->delete_references($this->id);
			}
		}
	}