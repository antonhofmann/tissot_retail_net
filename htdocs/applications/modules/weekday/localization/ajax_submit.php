<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	if($_REQUEST['weekday_id'] > 0 and $_REQUEST['language_id'])
	{
		$model_store_locator = new Model(CONNECTOR::DB_RETAILNET_STORE_LOCATOR);
		
		
		//check if translation is present in db retail net
		$sth = $model_store_locator->db->prepare("select count(loc_weekday_id) as num_recs from loc_weekdays
				WHERE loc_weekday_weekday_id = ? and loc_weekday_language_id = ?");
		$response = $sth->execute(array($_REQUEST['weekday_id'], $_REQUEST['language_id']));

		if($response == true)
		{
			$row = $sth->fetch();
			if($row['num_recs'] == 0)
			{
				$query = "Insert into loc_weekdays (loc_weekday_weekday_id, loc_weekday_language_id, loc_weekday_name) VALUES (?,?,?)";

				$sth = $model_store_locator->db->prepare($query);
				$response = $sth->execute(array($_REQUEST['weekday_id'], $_REQUEST['language_id'], $_REQUEST['weekday_name']));

			}
			else
			{
				$query = "UPDATE loc_weekdays SET 
					loc_weekday_name = ? 
				WHERE loc_weekday_weekday_id = ? and loc_weekday_language_id = ?";
				
				//update store locator
				$sth = $model_store_locator->db->prepare($query);
				$response = $sth->execute(array($_REQUEST['weekday_name'], $_REQUEST['weekday_id'], $_REQUEST['language_id']));
			}
		}
	}

?>