<?php 

	class RED {
		
		const PERMISSION_VIEW_SYSTEM_DATA  = "can_view_red_system_data";
		const PERMISSION_EDIT_SYSTEM_DATA  = "can_edit_red_system_data";
		const PERMISSION_DELETE_SYSTEM_DATA  = "can_delete_red_system_data";
	}