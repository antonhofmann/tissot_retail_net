<?php 

	class Application_Role {
		
		const DB_BIND_ROLES = 'INNER JOIN db_retailnet.roles ON role_id = role_application_role';
		const DB_BIND_APPLICATIONS = 'INNER JOIN db_retailnet.applications ON application_id = role_application_application';
	}