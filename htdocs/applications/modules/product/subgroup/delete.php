<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

switch ($application) {

	case 'lps':
		$permissionEdit = user::permission('can_edit_lps_product_groups');
		$tableName = 'lps_product_subgroups';
	break;
}

$modul = new Modul($application);
$modul->setTable($tableName);
$modul->read($id);

if ($modul->id) {
	
	if ($permissionEdit) { 
		
		$response = $modul->delete();
		
		if ($response) {
			Message::request_deleted();
			url::redirect("/$application/$controller");
		} else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$id");
		}
	}
	else { 
		Message::access_denied();
		url::redirect("/$application/$controller/$action/$id");
	}
}
else { 
	Message::error('Product Subgroup not found.');
	url::redirect("/$application/$controller");
}