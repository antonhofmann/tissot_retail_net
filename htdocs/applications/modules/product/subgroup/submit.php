<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// disabled fields
$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

// url request
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['product_subgroup_id'];

$data = array();

switch ($application) {
	
	case 'lps':
		$tableName = 'lps_product_subgroups';

		if (in_array('product_subgroup_group_id', $fields)) {
			$data['lps_product_subgroup_group_id'] = $_REQUEST['product_subgroup_group_id'] ?: null;
		}
		
		if (in_array('product_subgroup_name', $fields)) {
			$data['lps_product_subgroup_name'] = $_REQUEST['product_subgroup_name'] ?: null;
		}

		if (in_array('product_subgroup_active', $fields)) {
			$data['lps_product_subgroup_active'] = $_REQUEST['product_subgroup_active'] ? 1 : 0;
		}
		
	break;
}

if ($data) {

	$modul = new Modul($application);
	$modul->setTable($tableName);
	$modul->read($id);

	if ($modul->id) {
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		$response = $modul->update($data);
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	elseif (!$id) {
		$data['user_created'] = $user->login;
		$response = $id = $modul->create($data);
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		$redirect = $_REQUEST['redirect'] ? $_REQUEST['redirect']."/$id" : null;
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect
));
