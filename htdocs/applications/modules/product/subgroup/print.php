<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

// execution time
ini_set('max_execution_time', 240);
ini_set('memory_limit', '1024M');

$fileName = 'product-subgroups-'.date('Y-m-d');

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// request vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

// filters
$fSearch = $_REQUEST['search'] && $_REQUEST['search']<>$translate->search ? $_REQUEST['search'] : false;
$fGroup = $_REQUEST['group'];

$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'product_group_name, product_subgroup_name';

// db model   
$model = new Model($application);

switch ($application) {

	case 'lps':
		$sheetName = 'Product Subgroups';

		// filter: full text search
		if ($fSearch ) {

			$filters['search'] = "(
				OR lps_product_subgroup_name LIKE '%$fSearch%'
				OR lps_product_group_name LIKE '%$fSearch%'
			)";

			$captions[] = "Search Term: $fSearch";
		}

		// filter: product group
		if ($fGroup) {
			
			$filters['group'] = "lps_product_subgroup_group_id = $fGroup";

			$result = $model->query("
				SELECT lps_product_group_name
				FROM lps_product_groups
				WHERE lps_product_group_id = $fGroup
			")->fetch();

			$captions[] = "Product Group: {$result[lps_product_group_name]}";
		}
		
		$result = $model->query("
			SELECT SQL_CALC_FOUND_ROWS DISTINCT
				lps_product_subgroup_id AS product_subgroup_id,
				lps_product_group_name AS product_group_name,
				lps_product_subgroup_name AS product_subgroup_name
			FROM lps_product_subgroups
			INNER JOIN lps_product_groups ON lps_product_group_id = lps_product_subgroup_group_id
		")
		->filter($filters)
		->order($order, $direction)
		->fetchAll();
		
	break;
}

	
if ($result) {
	
	require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
	require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

	$datagrid = _array::datagrid($result);
	$columns = array_keys(end($datagrid));
	$totalColumns = count($columns);

	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle($sheetName);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

	$styles = array(
		'borders' => array(
			'borders' => array(
				'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		),
		'number' => array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
		),
		'string' => array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		),
		'header' => array(
			'font' => array(
				'name'=>'Arial',
				'size'=> 11,
				'bold'=>true
			),
			'alignment' => array(
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'EEEEEE')
			),
			'borders' => array(
				'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		)
	);
	
	// last col
	$lastColumn = "B";

	// column dimensions
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);

	// sheet caption
	$row=1;
	$range = "A$row:".$lastColumn.$row;
	$objPHPExcel->getActiveSheet()->mergeCells($range);
	$objPHPExcel->getActiveSheet()->setCellValue("A$row", $sheetName);
	$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(18);

	// print date
	$row++;
	$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
	$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: ".date('d.m.Y  H:i:s'));

	// separator
	$row++;
	$range = "A$row:".$lastColumn.$row;
	$objPHPExcel->getActiveSheet()->mergeCells($range);

	// filter captions
	if ($captions) {
		
		foreach ($captions as $caption) {
			$row++;
			$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
			$objPHPExcel->getActiveSheet()->setCellValue("A$row", $caption);
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
		}

		$row++;
		$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
	}

	// columns
	$row++;
	$col = 0;

	foreach ($columns as $value) {
		$index = spreadsheet::index($col, $row);
		$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['header']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $translate->$value);
		$col++;
	}

	foreach ($datagrid as $i => $array) {
		
		$row++; $col = 0;
		
		foreach($array as $key => $value) {
			$index = spreadsheet::index($col, $row);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
			$col++;
	    }

	    $objPHPExcel->getActiveSheet()->getStyle("A$row:$lastColumn$row")->applyFromArray($styles['borders']);
	}

	$row++;
	$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");

	$objPHPExcel->getActiveSheet()->setTitle($sheetName);
	$objPHPExcel->setActiveSheetIndex(0);

	// redirect output to client browser
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx" ');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
}
else {
		message::empty_result();
		url::redirect("/$application/$controller$archived/$action");
}
	