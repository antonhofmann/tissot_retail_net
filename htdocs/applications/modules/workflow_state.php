<?php

	/**
	 * Workflow States
	 * 
	 * management of workflow states
	 * 
	 * @author aserifi
	 * @copyright mediaparx ag
	 * @version 1.1
	 */
	class Workflow_State {
		
		/**
		 * Workflow State ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Workflow state data
		 * @var array
		 */
		public $data;
	
		/**
		 * State Open
		 * @var int
		 */
		const STATE_OPEN=1;
		
		/**
		 * State Progress
		 * @var int
		 */
		const STATE_PROGRESS=2;
		
		/**
		 * State Completed
		 * @var int
		 */
		const STATE_COMPLETED=3;
		
		/**
		 * State Approved
		 * @var int
		 */
		const STATE_APPROVED=4;
		
		/**
		 * State Consolidated
		 * @var int
		 */
		const STATE_CONSOLIDATED=5;
		
		/**
		 * State Archived
		 * @var int
		 */
		const STATE_DISTRIBUTED=6;
		
		/**
		 * State Preparation
		 * @var int
		 */
		const STATE_PREPARATION=7;
	
		/**
		 * State Revision
		 * @var int
		 */
		const STATE_REVISION=8;
		
		/**
		 * State Sales Order Subitted
		 * @var int
		 */
		const STATE_SALES_ORDER_SUBITTED=9;
		
		/**
		 * State Order Confirmed
		 * @var int
		 */
		const STATE_ORDER_CONFIRMED=10;
		
		/**
		 * State In Distribution
		 * @var int
		 */
		const STATE_IN_DISTRIBUTION = 11;
		
		/**
		 * State In Distribution
		 * @var int
		 */
		const STATE_SHIPPED=12;
		
		/**
		 * State partially distributed
		 * @var int
		 */
		const STATE_PARTIALLY_DISTRIBUTED = 13;
		
		/**
		 * State partially shipped
		 * @var int
		 */
		const STATE_PARTIALLY_SHIPPED=14;
		
		/**
		 * State in distribution manually archived
		 * @var int
		 */
		const STATE_MANUALLY_ARCHIVED=15;
		
		/**
		 * State in archive
		 * @var int
		 */
		const STATE_ARCHIVED=16;
		
		
		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new WorkFlow_State_Model($connector);
		}
		
		public function __get($key) {
			return $this->data["mps_workflow_state_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_workflow_state_id'];
			return $this->data;
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($application, $filters=null) {
			$model = new WorkFlow_State_Model($application);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
		
		public static function dropdown($application, $filters=null) {
			$model = new WorkFlow_State_Model($application);
			$result = $model->dropdown($filters);
			return _array::extract($result);
		}
	}