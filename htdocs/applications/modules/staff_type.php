<?php 

	class Staff_Type {
		
		/**
		 * Staff Type ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Staff Type Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @var object
		 */
		protected $model;
		
		/**
		 * Edit all staff
		 * @var string
		 */
		const PERMISSION_EDIT = 'can_edit_mps_staff';
		
		/**
		 * Edit only owner staff
		 * @var string
		 */
		const PERMISSION_EDIT_LIMITED = 'can_edit_only_his_mps_staff';
		
		/**
		 * View all staf
		 * @var string
		 */
		const PERMISSION_VIEW = 'can_view_all_mps_staff';
		
		/**
		 * View only owner staff
		 * @var string
		 */
		const PERMISSION_VIEW_LIMITED = 'can_view_only_his_mps_staff';
		
		/**
		 * Join Staff
		 * @var string
		 */
		const DB_BIND_STAFF = "INNER JOIN mps_staff ON mps_staff_staff_type_id = mps_staff_type_id";
		
		
		/**
		 * Staff Type<br />
		 * Application Dependent Modul
		 * @author aserifi
		 * @copyright mediaparx ag
		 */
		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new Staff_Type_Model($connector);
		}
		
		public function __get($key) {
			return $this->data["mps_staff_type_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_staff_type_id'];
			return $this->data;
		}
		
		public function create($data) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($application, $filters=null) {
			$model = new Staff_Type_Model($application);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
	}
	
	