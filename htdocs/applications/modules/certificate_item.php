<?php

	class Certificate_Item {

		public $id;

		public $certificate;

		public $data;

		public function __construct() {
			$this->model = new Certificate_Item_Model(Connector::DB_CORE);
		}

		public function setCertificate($certificate) {
			$this->certificate = $certificate;
		}

		public function read($item) {
			if ($this->certificate) {
				return $this->model->read($this->certificate, $item);
			}
		}

		public function create($item) {
			if ($this->certificate) {
				return $this->model->create($this->certificate, $item);
			}
		}

		public function delete($item) {
			if ($this->certificate) {
				return $this->model->delete($this->certificate, $item);
			}
		}

		public function deleteAll() {
			if ($this->certificate) {
				return $this->model->deleteAll($this->certificate);
			}
		}
	}