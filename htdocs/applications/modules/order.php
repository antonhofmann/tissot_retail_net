<?php 

	class Order {
		
		public $id;
		
		public $data;
		
		protected $model;
		
		const DB_BIND_ORDER_ITEMS = "INNER JOIN db_retailnet.order_items ON order_item_order = order_id";
		
		
		public function __construct() {
			
			$this->model = new OrderModel(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["order_$key"];
		}
		
		public function read($id) {
				
			$this->id = $id;
				
			return $this->data = $this->model->read($id);
		}
		
		public function create($data) {
				
			if (is_array($data)) {
		
				$this->data = $data;
		
				return $this->id = $this->model->create($data);
			}
		}
		
		public function update($data) {
				
			if ($this->id) {
		
				$this->data = array_merge($this->data, $data);
		
				return $this->model->update($this->id, $data);
			}
		}
		
		public function delete() {
				
			if ($this->id) {
		
				return $this->model->delete($this->id);
			}
		}

		/**
		 * Get retail operator for this order
		 * @return User
		 */
		public function getRetailOperator() {
			$User = new User();
			$User->read($this->retail_operator);
			return $User;
		}

		/**
		 * Get brand manager
		 * @return User
		 */
		public function getBrandManager() {
			$UserRoleMapper = new User_Datamapper();
			return $UserRoleMapper->getBrandManager($this->client_address);
		}

		/**
		 * Get regional sales manager
		 * @return User
		 */
		public function getRegionalSalesManager() {
			$UserRoleMapper = new User_Datamapper();
			return $UserRoleMapper->getRegionalSalesManager($this->id);
		}

		/**
		 * Get order_user (client)
		 * @return User
		 */
		public function getUser() {
			$User = new User();
			$User->read($this->user);
			return $User;
		}

		/**
		 * Get project costs for order
		 * @return array
		 */
		public function getProjectCosts() {
			return $this->model->getProjectCosts($this->id);
		}

		public function orderMailTrack($data) {

			if (is_array($data)) {
				return $this->model->orderMailTrack($data);
			}
		}

		public function orderMailTracks(ActionMail $Mail) {

			$recipients = $Mail->getRecipients();
			$sender = $Mail->getSender();
			
			if (!$recipients) return;

			foreach ($recipients as $id => $recipient) {
				
				//if (!$recipient->isSendMail()) continue;

				$data = array(
					'order_mail_order'       => $this->id,
					'order_mail_user'        => $recipient->id,
					'order_mail_from_user'   => $sender->id,
					'order_mail_text'        => $recipient->getContent(),
					'order_mail_is_cc'       => 0,
					'order_mail_template_id' => $Mail->getTemplateId(),
					'user_created'           => $sender->login,
					'date_created'           => date('Y-m-d H:i:s')
				);
				$this->orderMailTrack($data);

				$ccRecipients = $recipient->getCCRecipients();

				if (!$ccRecipients) continue;

				// track cc recipients
				foreach ($ccRecipients as $email) {
					$data['posmail_recipeint_email'] = $email;
					$this->orderMailTrack($data);
				}
			}
		}
	}
