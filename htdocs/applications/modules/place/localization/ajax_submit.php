<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	if($_REQUEST['place_id'] > 0 and $_REQUEST['language_id'])
	{
		
		$model = new Model();
		$model_store_locator = new Model(CONNECTOR::DB_RETAILNET_STORE_LOCATOR);


		//get places data
		$sth = $model->db->prepare("select place_id, place_province, place_name, country_store_locator_id 
			from places
			left join countries on country_id = place_country 
			WHERE place_id = ?");
		$response = $sth->execute(array($_REQUEST['place_id']));

		if($response == true)
		{
			$place_data = $sth->fetch();

		
			//check if place is in store locator db
			$sth = $model_store_locator->db->prepare("select count(place_id) as num_recs from places
			WHERE place_id = ?");
			$response = $sth->execute(array($place_data['place_id']));
			
			if($response == true)
			{
				$row = $sth->fetch();

				if($row['num_recs'] == 0)
				{
					$query = "Insert into places (place_id, place_country_id, place_province_id, place_name) VALUES (?,?,?,?)";

					$sth = $model_store_locator->db->prepare($query);
					$response = $sth->execute(array($place_data['place_id'], $place_data['country_store_locator_id'], $place_data['place_province'], $place_data['place_name']));
				}
			}

			
			//check if translation is present in db retail net
			$sth = $model_store_locator->db->prepare("select count(loc_place_id) as num_recs from loc_places
				    WHERE loc_place_place_id = ? and loc_place_language_id = ?");
			$response = $sth->execute(array($_REQUEST['place_id'], $_REQUEST['language_id']));
			if($response == true)
			{
				
				$row = $sth->fetch();
				if($row['num_recs'] == 0)
				{
					$query = "Insert into loc_places (loc_place_country_id, loc_place_language_id, loc_place_place_id, loc_place_name) VALUES (?,?,?,?)";

					$sth = $model_store_locator->db->prepare($query);
					$response = $sth->execute(array($place_data['country_store_locator_id'], $_REQUEST['language_id'], $place_data['place_id'], $_REQUEST['place_name']));

				}
				else
				{
					$query = "UPDATE loc_places SET 
						loc_place_name = ? 
					WHERE loc_place_place_id = ? and loc_place_language_id = ?";
					
					//update store locator
					$sth = $model_store_locator->db->prepare($query);
					$response = $sth->execute(array($_REQUEST['place_name'], $_REQUEST['place_id'], $_REQUEST['language_id']));
				}
			}
		}
	}

?>