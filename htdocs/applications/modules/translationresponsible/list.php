<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];

$_REQUEST = session::filter($application, "$controller.$action", true);

// sql order
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'address_company';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;
	
$model = new Model(Connector::DB_CORE);

// filter: full text search
if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
	
	$search = $_REQUEST['search'];
	
	$filters['search'] = "(
		address_company LIKE '%$search%'
	)";
}

$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			translationresponsible_id,
			address_company,
			concat(user_name, ' ', user_firstname) as username,
			role_name, 
			country_name, language_name
		FROM translationresponsibles
		LEFT JOIN addresses on address_id = translationresponsible_address_id  
		left JOIN countries on country_id = translationresponsible_country_id
		LEFT JOIN users on user_id = translationresponsible_user_id
		LEFT Join roles on role_id = translationresponsible_role_id
		LEFT JOIN languages on language_id = translationresponsible_language_id
	")
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();

if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);
	
	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// toolbox: add
if ($_REQUEST['add'])  {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();

if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

$table->datagrid = $datagrid;
$table->dataloader($dataloader);

$table->address_company(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	"href=".$_REQUEST['form']
);

$table->country_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);

$table->role_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);

$table->username(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);

$table->language_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);



$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;
