<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['translationresponsible_id'];

	$data = array();

	$translationresponsible = new Modul(DB_CORE);

	// set table name
	$translationresponsible->setTable('translationresponsibles');
	
	if ($_REQUEST['translationresponsible_address_id']) {
		$data['translationresponsible_address_id'] = $_REQUEST['translationresponsible_address_id'];
	}

	if ($_REQUEST['translationresponsible_country_id']) {
		$data['translationresponsible_country_id'] = $_REQUEST['translationresponsible_country_id'];
	}

	if ($_REQUEST['translationresponsible_role_id']) {
		$data['translationresponsible_role_id'] = $_REQUEST['translationresponsible_role_id'];
	}

	if ($_REQUEST['translationresponsible_user_id']) {
		$data['translationresponsible_user_id'] = $_REQUEST['translationresponsible_user_id'];
	}

	if ($_REQUEST['translationresponsible_language_id']) {
		$data['translationresponsible_language_id'] = $_REQUEST['translationresponsible_language_id'];
	}

	
	

	if ($data) {

		$translationresponsible->read($id);

		if ($translationresponsible->id) {
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			$response = $translationresponsible->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $translationresponsible->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}



	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));