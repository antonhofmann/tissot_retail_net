<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$translationresponsible = new Modul(DB_CORE);

// set table name
$translationresponsible->setTable('translationresponsibles');

$permission = user::permission('can_edit_catalog');


$translationresponsible->read($id);

if ($translationresponsible->id && $permission) {
	
	$delete = $translationresponsible->delete();
	
	if ($delete) {
		Message::request_deleted();
		url::redirect("/$application/$controller");
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}