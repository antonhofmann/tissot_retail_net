<?php

	class Ordersheet_Version {
		
		const DB_BIND_ORDERSHEETS = 'INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_version_ordersheet_id';
		const DB_BIND_COMPANIES = 'INNER JOIN db_retailnet.addresses ON address_id = mps_ordersheet_version_address_id';
		const DB_BIND_MASTERSHEETS = 'INNER JOIN mps_mastersheets ON mps_mastersheet_id = mps_ordersheet_version_mastersheet_id';
		const DB_BIND_WORKFLOW_STATES = 'INNER JOIN mps_workflow_states ON mps_workflow_state_id = mps_ordersheet_version_workflowstate_id';
	
		/**
		 * Version ID
		 */
		public $id;
		
		/**
		 * Version data
		 * @var array
		 */
		public $data;
		
		/**
		 * Version Order Sheet
		 * @var unknown
		 */
		public $ordersheet;
		
		/**
		 * Version Item
		 * @var object
		 */
		public $item;
		
		/**
		 * DB Connector
		 * @var string
		 */
		protected $connector;
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Ordersheet_Version_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_ordersheet_version_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_ordersheet_version_id'];
			$this->ordersheet = $this->data['mps_ordersheet_version_ordersheet_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public function load() {
			if ($this->ordersheet) {
				$result = $this->model->load($this->ordersheet);
				return ($result) ? _array::datagrid($result) : null;
			}
		}
		
		/**
		 * Order Sheet Version Item
		 * @return Ordersheet_Version_Item
		 */
		public function item() {
			
			if (!$this->item) {
				$this->item = new Ordersheet_Version_Item($this->connector);
			}
			
			$this->item->version = $this->id;
			return $this->item;
		}
	}