<?php

	class Pos {

		const PERMISSION_ACCESS = "has_access_to_mps_posindex";
		const PERMISSION_ADMINISTRATE = "can_administrate_posindex";
		const PERMISSION_EDIT = "can_edit_mps_posindex";
		const PERMISSION_EDIT_LIMITED = "can_edit_only_his_mps_posindex";
		const PERMISSION_VIEW = "can_view_mps_posindex";
		const PERMISSION_VIEW_POS = "can_view_posindex";
		const PERMISSION_VIEW_LIMITED = "can_view_mps_his_posindex";
		const PERMISSION_VIEW_LIMITED_POS = "can_view_his_posindex";


		const DB_BIND_COUNTRIES = 'INNER JOIN db_retailnet.countries ON country_id = posaddress_country';
		const DB_BIND_PLACES = 'INNER JOIN db_retailnet.places ON place_id = posaddress_place_id';
		const DB_BIND_PROVINCES = 'INNER JOIN db_retailnet.provinces ON province_id = place_province';
		const DB_BIND_POSTYPES = 'INNER JOIN db_retailnet.postypes ON postype_id = posaddress_store_postype';
		const DB_BIND_POSOWNER_TYPES = 'INNER JOIN db_retailnet.posowner_types ON posowner_type_id = posaddress_ownertype';
		const DB_BIND_DISTRIBUTION_CHANNELS = 'INNER JOIN db_retailnet.mps_distchannels ON mps_distchannel_id = posaddress_distribution_channel';
		const DB_BIND_TURNOVER_CLASS_WATCHES = 'INNER JOIN db_retailnet.mps_turnoverclasses ON mps_turnoverclass_id = posaddress_turnoverclass_watches';
		const DB_BIND_TURNOVER_CLASS_BIJOUX = 'INNER JOIN db_retailnet.mps_turnoverclasses ON mps_turnoverclass_id = posaddress_turnoverclass_bijoux';
		const DB_BIND_MPS_STAFF_SALES_REPRESENTATIVES = 'INNER JOIN mps_staffs ON mps_staff_id = posaddress_sales_representative';
		const DB_BIND_MPS_STAFF_DECORATOR_PERSONS = 'INNER JOIN mps_staffs ON mps_staff_id = posaddress_decoration_person';
		const DB_BIND_PRODUCT_LINES = 'INNER JOIN db_retailnet.product_lines ON product_line_id = posaddress_store_furniture';

		const DB_BIND_FRANCHISEE_ID = 'INNER JOIN db_retailnet.addresses ON address_id = posaddress_franchisee_id';
		const DB_BIND_CLIENTS = 'INNER JOIN db_retailnet.addresses ON address_id = posaddress_client_id';


		public $data;

		protected $track;

		public function __construct() {
			$this->model = new Pos_Model(Connector::DB_CORE);
		}

		public function __get($key) {
			return $this->data["posaddress_$key"];
		}

		public function read($id) {

			$this->data = $this->model->read($id);
			$this->id = $this->data['posaddress_id'];

			if ($this->data) {
				$place = new Place();
				$place->read($this->place_id);
				$this->data['posaddress_province_id'] = $place->province;
			}

			return $this->data;
		}

		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}

		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}

		public function isIndependentRetailer() {
			if ($this->id) {
				return $this->model->isIndependentRetailer($this->id);
			}
		}
		
		public function header() {
			
			if ($this->id) {
				
				$company = new Company();
				$company->read($this->franchisee_id);

				$captions[] = $company->company;
				$captions[] = $this->name;

				$country = new Country();
				$country->read($this->country);
				$captions[] = $country->name;

				$province = new Province();
				$province->read($this->province_id);
				$captions[] = $province->canton;

				$place = new Place();
				$place->read($this->place_id);
				$captions[] = $place->name;

				return join(', ', $captions);
			}
		}
		
		public function access() {
			
			$access = false;
			$user = User::instance();

			if($this->id) { 

				// permissions
				$permission_edit = user::permission(Pos::PERMISSION_EDIT);
				$permission_edit_limited = user::permission(Pos::PERMISSION_EDIT_LIMITED);
				$permission_view = user::permission(Pos::PERMISSION_VIEW);
				$permission_view_pos = user::permission(Pos::PERMISSION_VIEW_POS);
				$permission_view_limited = user::permission(Pos::PERMISSION_VIEW_LIMITED);
				$permission_view_limited_pos = user::permission(Pos::PERMISSION_VIEW_LIMITED_POS);
				$permission_administrate = user::permission(Pos::PERMISSION_ADMINISTRATE);
				$permission_travelling = User::permission('has_access_to_all_travalling_retail_data');
				
				// system administrator access
				if ($permissions_edit || $permission_view || $permission_administrate) {
					$access = true;
				} 
				// limited access
				elseif ($permission_view_limited || $permission_edit_limited) {
					$access = $this->client_id == $user->address ? true : false;			
				}

				// travelling
				if (!$access && $permission_travelling) {

					if ($pos->store_subclass==17) {
						$access = true;
					} else {
						
						$model = new Model(Connector::DB_CORE);

						$result = $model->query("
							SELECT COUNT(posarea_id) AS total
							FROM posareas
							WHERE posarea_posaddress = $this->id AND posarea_area IN (4,5)
						")->fetch();

						$access = $result['total'] > 0 ? true : false;
					}
				}

				// country access
				if (!$access && $this->country) {

					$model = new Model(Connector::DB_CORE);
						
					$result = $model->query("
						SELECT COUNT(country_access_id) as total
						FROM country_access
						WHERE country_access_user = $user->id AND country_access_country = $this->country
					")->fetch();

					$access = $result['total'] > 0 ? true : false;
				}

				// regional access
				if (!$access && $this->client_id) {

					$model = new Model(Connector::DB_CORE);

					$result = $model->query("
						SELECT * 
						FROM user_company_responsibles 
						WHERE user_company_responsible_user_id = $user->id AND user_company_responsible_address_id = $this->client_id
					")->fetch();

					if ($result) {

						$company = $result["user_company_responsible_address_id"];
						$retail = $result["user_company_responsible_retail"];
						$wholsale = $result["user_company_responsible_wholsale"];
						
						if($retail and $wholsale) {
							$access = $this->client_id==$company ? true : false;
						} elseif($retail) {
							$access = $this->client_id==$company && in_array($this->ownertype, array(1,3,4,5)) ? true : false;
						} elseif($wholsale){
							$access = $this->client_id==$company && in_array($this->ownertype, array(2,6)) ? true : false;
						}
					}
				}
			}
			
			return $access;
		}

		public function track() {

			if (!$this->track) {
				$this->track = new PosTrack();
			}

			if ($this->id && $this->id <> $this->track->getPos()) {
				$this->track->setPos($this->id);
			}

			return $this->track;
		}

		/**
		 * Track mails regarding a POS
		 * @param  array $data
		 * @return integer ID of instered posmail record
		 */
		public function posMailTrack(array $data) {
			if (is_array($data)) {
				return $this->model->posMailTrack($data);
			}
		}

		public function posMailTracks(ActionMail $Mail) { 
			
			$recipients = $Mail->getRecipients();

						
			if (!$recipients) return;

			$sender = $Mail->getSender();

			foreach ($recipients as $id => $recipient) {

				
				
				//if (!$recipient->isSendMail()) continue;
	
				// track mail
				$data = array(
					'posmail_posaddress_id'    => $this->id,
					'posmail_recipeint_email'  => $recipient->getEmail(),
					'posmail_mail_template_id' => $Mail->getTemplateId(),
					'posmail_sender_email'     => $sender->email,
					'posmail_subject'          => $recipient->getSubject(true),
					'posmail_text'             => $recipient->getContent(),
					'user_created'             => $sender->login
				);
				
				$this->posMailTrack($data);

				$ccRecipients = $recipient->getCCRecipients();

				if (!$ccRecipients) continue;

				// track cc recipients
				foreach ($ccRecipients as $email) {
					$data['posmail_recipeint_email'] = $email;
					$this->posMailTrack($data);
				}
			}
		}

		public function canEdit() {

			$return = false;

			$user = User::instance();

			$permissionEdit = user::permission(Pos::PERMISSION_EDIT);
			$permissionEditLimited = user::permission(Pos::PERMISSION_EDIT_LIMITED);
			$permissionView = user::permission('can_view_posindex');
			$permissionViewLimited = user::permission('can_view_his_posindex');


			// system administrator
			if ($permissionEdit) {
				$return = true;
			}
			// limited edit
			elseif ($permissionEditLimited) {
				
				$company = new Company();
				$company->read($this->client_id);
				$return =  $company->canEdit();

				if (!$return && $this->country) {

					$user = User::instance();
						
					$result = $this->model->query("
						SELECT COUNT(country_access_id) as total
						FROM country_access
						WHERE country_access_user = $user->id AND country_access_country = $this->country
					")->fetch();
				
					$return = $result['total'] > 0 ? true : false;
				}
			}
			
			return $return;
		}

	}