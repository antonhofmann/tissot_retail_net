<?php

	class Translation { 
		
		const PERMISSION_EDIT = 'can_administrate_system_data';
		
		public $data;

		public function __construct() {
			$this->model = new Translation_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["translation_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['translation_id'];
			return $this->data;
		}
		
		public function read_from_keyword($keyword, $category) {
			$this->data = $this->model->read_from_keyword($keyword, $category);
			$this->id = $this->data['translation_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}		
	
		public function get_categoris($language) {
			
			$result = $this->model->get_categories($language);
			
			if ($result) {
				
				foreach ($result as $row) {
					$array[$row['translation_category']] = ucfirst($row['translation_category']);
				}
				
				foreach ($array as $value => $caption) {
					$categories[] = array(
						'translation_category_id' => $value,
						'translation_category_name' => $caption
					);
				}
				
				return $categories;
			}
		}
	}