<?php 

	class TurnoverClass {
		
		/**
		 * Turnover Class ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Turnover Class Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Turnover Group
		 * @var object
		 */
		public $group;
		
		/**
		 * Database Model
		 * @return TurnoverClass_Model
		 */
		protected $model;
		
		/**
		 * Can Edit All Turnover Classes
		 * @var string
		 */
		const PERMISSION_EDIT = 'can_edit_mps_turnover_classes';
		
		/**
		 * Can View All Turnover Classes
		 * @var string
		 */
		const PERMISSION_VIEW = 'can_view_mps_turnover_classes';
		
		/**
		 * Join Turnover Groups
		 * @var string
		 */
		const DB_BIND_TURNOVER_GROUPS = 'INNER JOIN db_retailnet.mps_turnovergroups ON mps_turnovergroup_id = mps_turnoverclass_group_id';
		
		
		/**
		 * Turnover Class<br />
		 * Allpication Dependet Modul
		 * @author aserifi
		 * @copyright mediaparx ag
		 */
		public function __construct($connector=null) {
			$this->model = new TurnoverClass_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["mps_turnoverclass_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_turnoverclass_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($application, $filters=null) {
			$model = new TurnoverClass_Model(Connector::DB_CORE);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
		
		public function group() {
			
			if (!$this->group) {
				$this->group = new TurnoverGroup(Connector::DB_CORE);
				$this->group->read($this->group_id);
			}
			
			return $this->group;
		}
		
		public function header() {
			if ($this->id) {
				return $this->group()->name.', '.$this->code.', '.$this->name;
			}
		}
	}