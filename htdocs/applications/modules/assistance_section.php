<?php 

	class Assistance_Section {
		
		/**
		 * Section ID
		 * 
		 * @var int
		 */
		public $id;
		
		/**
		 * Dataloader
		 * 
		 * @var arrray
		 */
		public $data;
		
		/**
		 * Section Link
		 * 
		 * @return Assistance_Section_Link
		 */
		protected $link;
		
		/**
		 * DB Model
		 * 
		 * @return Assistance_Section_Model
		 */
		protected $model;
	
		public function __construct() {
			
			$this->model = new Assistance_Section_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			
			return $this->data["assistance_section_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['assistance_section_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			
			if ($this->id) {
				
				$this->deleteAllLinks($this->id);
				
				$this->model->delete($this->id);
				
				return true;
			}
		}
		
		/**
		 * Delete all section links
		 * 
		 * @return boolean
		 */
		protected function deleteAllLinks() {
			
			if ($this->id) {
				
				return $this->model->deleteAllLinks($this->id);
			}
		}
		
		/**
		 * Section Link
		 * 
		 * @return Assistance_Section_Link
		 */
		public function link() {
			
			if (!$this->link) {
				$this->link = new Assistance_Section_Link(Connector::DB_CORE);
			}
			
			if ($this->id) {
				$this->link->setSection($this->id);
			}
			
			return $this->link;
		}
	}
	