<?php

	class Company {
		
		/**
		 * Company ID
		 * @var int
		 */
		public $id;
		
		/**
		 * Company Data
		 * @var int
		 */
		public $data;
		
		/**
		 * Company POS
		 * @return Pos
		 */
		public $pos;
		
		/**
		 * Company ware houses
		 * @return Company_Warehouse
		 */
		public $warehouse;
		
		/**
		 * Company Language
		 * @return Company_Language
		 */
		public $language;
		
		/**
		 * Company Additional Data
		 * @return Company_Additional
		 */
		public $additional;

		/**
		 * DB Company Model
		 * @return Company_Model
		 */
		protected $model;
		

		const PERMISSION_MANAGER = "can_manage_companies";
		const PERMISSION_EDIT = "can_edit_mps_companies";
		const PERMISSION_EDIT_LIMITED = "can_edit_only_his_mps_companies";
		const PERMISSION_VIEW = "can_view_mps_companies";
		const PERMISSION_VIEW_LIMITED = "can_view_only_his_mps_companies";
		const PERMISSION_EDIT_CATALOG = "can_edit_catalog";
		
		/**
		 * DB: Join company types
		 * @var string
		 */
		const DB_BIND_ADDRESS_TYPES = "INNER JOIN db_retailnet.address_types ON address_type_id = address_type";
		
		/**
		 * DB: Join company countires
		 * @var string
		 */
		const DB_BIND_COUNTRIES = "INNER JOIN db_retailnet.countries ON address_country = country_id";
		
		/**
		 * DB: Join company places
		 * @var string
		 */
		const DB_BIND_PLACES = "INNER JOIN db_retailnet.places ON place_id = address_place_id";
		
		/**
		 * DB: Join company provinces
		 * @var string
		 */
		const DB_BIND_PROVINCES = "INNER JOIN db_retailnet.provinces ON province_id = place_province";
		
		/**
		 * DB: Join company users
		 * @var string
		 */
		const DB_BIND_USERS = 'INNER JOIN db_retailnet.users ON user_address = address_id';
		
		/**
		 * DB: Join company currencies
		 * @var string
		 */
		const DB_BIND_CURRENCIES = 'INNER JOIN currencies ON currency_id = address_currency';

		public function __construct() {
			$this->model = new Company_Model(Connector::DB_CORE);
		}

		public function __get($key) {
			return $this->data["address_$key"];
		}

		public function read($id) {

			$this->data = $this->model->read($id);
			$this->id = $this->data['address_id'];

			if ($this->data) {
				$place = new Place();
				$place->read($this->place_id);
				$this->data['address_province_id'] = $place->province;
			}

			return $this->data;
		}

		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}

		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}

		/**
		 * POS Builder
		 * @param int $pos
		 * @return Pos
		 */
		public function pos($pos=null) {
			
			if (!$this->pos) {
				$this->pos = new Pos();
			}
			
			if ($pos) {
				$this->pos->read($pos);
			}
			
			return $this->pos;
		}

		/**
		 * Warehouse Builder
		 * @param int $warehouse
		 * @return Company_Warehouse
		 */
		public function warehouse($warehouse=null) {
			
			if (!$this->warehouse) {
				$this->warehouse = new Company_Warehouse();
			}
			
			if ($this->id) {
				$this->warehouse->company($this->id);
			}
			
			if ($this->id && $warehouse) {
				$this->warehouse->read($warehouse);
			}
			
			return $this->warehouse;
		}

		/**
		 * Language Builder
		 * @param int $language
		 * @return Company_Language
		 */
		public function language($language=null) {
			
			if (!$this->language) {
				$this->language = new Company_Language();
			}
			
			if ($language) {
				$this->language->read($language);
			}
			
			return $this->language;
		}

		/**
		 * Additonal Builder
		 * @param int $id
		 * @return Company_Additional
		 */
		public function additional($id=null) {
			
			if (!$this->additional) {
				$this->additional = new Company_Additional();
			}
			
			if ($id) {
				$this->additional->read($id);
			}
			
			return $this->additional;
		}
		
		/**
		 * Check ownership
		 * @param int $company
		 * @return boolean
		 */
		public function owner($company=null) {
			
			if (!$company) {
				$user = user::instance();
				$company = $user->address;	
			}
			
			return ( $this->id == $company || $this->parent == $company) ? true : false;
		}

		// REPLACE THIS FUNCTION WHITH NEW SUBOBJEC "POS"
		public function get_pos() {
			$result = $this->model->get_pos($this->id);
			return $result['posaddress_id'];
		}
		
		// REPLACE THIS FUNCTION WHITH NEW SUBOBJEC "POS"
		public function get_all_pos($archived=false) {
			return $this->model->get_all_pos($this->id, $archived);
		}

		// REPLACE THIS FUNCTION IM RED APP WHITH NEW SUBOBJEC "USER"
		public function get_users() {
			if ($this->id) {
				$result = $this->model->get_users($this->id);
				$users = _array::extract($result);
				return is_array($users) ? array_keys($users) : null;
			}
		}

		/**
		 * Company Dropdown loader
		 * @param string $filters
		 * @return multitype:
		 */
		public static function loader($filters=null) {
			$model = new Company_Model(Connector::DB_CORE);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
		
		/**
		 * Build Company Header
		 */
		public function header() {
			if ($this->id) {
				
				$captions[] = $this->company;
				
				$country = new Country();
				$country->read($this->country);
				$captions[] = $country->name;
					
				$province = new Province();
				$province->read($this->province_id);
				$captions[] = $province->canton;
					
				$place = new Place();
				$place->read($this->place_id);
				$captions[] = $place->name;
				
				return join(', ', $captions); 
			}
		}
		
		/**
		 * Check company access permissions
		 * @return boolean
		 */
		public function access() {
			
			$access = false;
			$user = User::instance();

			$permission_catalog = user::permission(Company::PERMISSION_EDIT_CATALOG);
			$permission_view = user::permission(Company::PERMISSION_VIEW);
			$permission_edit = user::permission(Company::PERMISSION_EDIT);
			$permission_view_limited = user::permission(Company::PERMISSION_VIEW_LIMITED);
			$permission_edit_limited = user::permission(Company::PERMISSION_EDIT_LIMITED);
			
			if ($this->id)  { 
				
				// system administrators
				if ($permission_catalog || $permission_view || $permission_edit) {
					$access = true;
				}
				// limited access
				elseif ($permission_view_limited || $permission_edit_limited) { 
					$access = ($this->id == $user->address || $this->parent == $user->address) ? true : false; 
				}

				// regional access
				if (!$access) {				
					$access = $this->hasRegionalAccess();
				}

				// country access
				if (!$access) {				
					$access = $this->hasCountryAccess();
				}
			}

			return $access;
		}

		public function canEdit() {

			$return = false;

			$permissionView = user::permission(Company::PERMISSION_VIEW);
			$permissionViewLimited = user::permission(Company::PERMISSION_VIEW_LIMITED);
			$permissionEdit = user::permission(Company::PERMISSION_EDIT);
			$permissionEditLimited = user::permission(Company::PERMISSION_EDIT_LIMITED);
			$permissionMenager = user::permission(Company::PERMISSION_MANAGER);
			$permissionCatalog = user::permission(Company::PERMISSION_EDIT_CATALOG);

			// system administrator
			if ($permissionEdit || $permissionCatalog) {
				$return = true;
			} 
			// limited edit
			elseif ($permissionEditLimited) { 

				// owner
				if ($this->owner()) $return = true;
				// regional access
				else $return = $this->hasRegionalAccess();
			}

			return $return;
		}

		public function hasRegionalAccess() {

			if ($this->id) {
				
				$user = User::instance();
				$model = new Model(Connector::DB_CORE);

				$result = $model->query("
					SELECT user_company_responsible_id
					FROM db_retailnet.user_company_responsibles
					WHERE user_company_responsible_user_id = $user->id AND user_company_responsible_address_id = $this->parent
				")->fetch();

				return $result['user_company_responsible_id'] ? true : false;
			}
		}

		public function hasCountryAccess() {

			if ($this->id) {

				$user = User::instance();
				$model = new Model(Connector::DB_CORE);
					
				$result = $model->query("
					SELECT COUNT(country_access_id) as total
					FROM country_access
					WHERE country_access_user = $user->id AND country_access_country = $this->country
				")->fetch();
			
				return $result['total'] > 0 ? true : false;
			}
		}
	}
