<?php

	class Material {
		
		/**
		 * Material ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Material Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Material Planning Type
		 * @var object
		 */
		public $planning_type;
		
		/**
		 * Material Collection
		 * @var object
		 */
		public $collection;
		
		/**
		 * Material Collection Category
		 * @var object
		 */
		public $collection_category;
		
		/**
		 * Material Category
		 * @var object
		 */
		public $category;
		
		/**
		 * Material Purpose
		 * @var object
		 */
		public $purpose;
		
		/**
		 * Database Connector
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @return Material_Model
		 */
		protected $model;
	
		/**
		 * Edit all long term materials
		 * @var string
		 */
		const PERMISSION_LTM_EDIT = 'can_edit_mps_ltmaterials';
		
		/**
		 * View all long term materials
		 * @var string
		 */
		const PERMISSION_LTM_VIEW = 'can_view_mps_ltmaterials';
		
		/**
		 * Edit all short term materials
		 * @var string
		 */
		const PERMISSION_STM_EDIT = 'can_edit_mps_stmaterials';
		
		/**
		 * View all short term materials
		 * @var unknown
		 */
		const PERMISSION_STM_VIEW = 'can_view_mps_stmaterials';
		
		/**
		 * DB join material categories
		 * @var unknown
		 */
		const DB_BIND_CATEGORIES = "LEFT JOIN mps_material_categories ON mps_material_category_id = mps_material_material_category_id";
		
		/**
		 * DB join material purposes
		 * @var unknown
		 */
		const DB_BIND_PURPOSES = "LEFT JOIN mps_material_purposes ON mps_material_purpose_id = mps_material_material_purpose_id";
		
		/**
		 * DB join material planning types
		 * @var string
		 */
		const DB_BIND_PLANNING_TYPES = 'LEFT JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id';
		
		/**
		 * DB join material collections
		 * @var string
		 */
		const DB_BIND_COLLECTIONS = 'LEFT JOIN mps_material_collections ON mps_material_collection_id = mps_material_material_collection_id';
		
		/**
		 * DB join material collection categories
		 * @var string
		 */
		const DB_BIND_COLLECTION_CATEGORIES = 'LEFT JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id';
		
		/**
		 * DB join currencies
		 * @var string
		 */
		const DB_BIND_CURRENCIES = 'INNER JOIN db_retailnet.currencies ON currency_id = mps_material_currency_id';
		
		
		/**
		 * Material<br />
		 * Application Dependent Modul
		 * @author aserifi
		 * @copyright mediaparx ag
		 */
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Material_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_material_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_material_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public function planning_type() {
			
			// create object if not exist
			if ($this->id && !$this->planning_type) {
				$this->planning_type = new Material_Planning_Type($this->connector);
			}
			
			// assign material id
			$this->planning_type->material = $this->id;
			
			return $this->planning_type;
		}
		
		public function collection() {

			// create object if not exist
			if ($this->id && !$this->collection) {
				$this->collection = new Material_Collection($this->connector);
			}
				
			// assign material id
			$this->collection->material = $this->id;
				
			return $this->collection;
		}
		
		public function collection_category() {
		
			// create object if not exist
			if ($this->id && !$this->collection_category) {
				$this->collection_category = new Material_Collection_Category($this->connector);
			}
		
			// assign material id
			$this->collection_category->material = $this->id;
		
			return $this->collection_category;
		}
		
		public function category() {
		
			// create object if not exist
			if ($this->id && !$this->category) {
				$this->category = new Material_Category($this->connector);
			}
		
			// assign material id
			$this->category->material = $this->id;
		
			return $this->category;
		}
		
		public function purpose() {
		
			// create object if not exist
			if ($this->id && !$this->purpose) {
				$this->purpose = new Material_Purpose($this->connector);
			}
		
			// assign material id
			$this->purpose->material = $this->id;
		
			return $this->purpose;
		}
		
		public function header() {
			if ($this->id) {
				return $this->code.', '.$this->name;
			}
		}
		
	}