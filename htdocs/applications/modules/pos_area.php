<?php 

	class Pos_Area {
	
		public function __construct() {
			$this->model = new Pos_Area_Model(Connector::DB_CORE);
		}
		
		public function pos($id) {
			$this->pos = $id;
		}
		
		public function read($area) {
			return $this->model->read($this->pos, $area);
		}
		
		public function create($area) {
			return $this->model->create($this->pos, $area);
		}
		
		public function delete() {
			return $this->model->delete($this->pos);
		}
	}