<?php

	class Cost_Monitoring_Group {
		
		public $data;
	
		public function __construct() {
			$this->model = new Cost_Monitoring_Group_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["costmonitoringgroup_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['costmonitoringgroup_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
	}