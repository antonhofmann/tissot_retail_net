<?php

	class User_Export_List {
		
		/**
		 * List ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * User ID
		 * @var integer
		 */
		public $user;
		
		/**
		 * List Data
		 * @var array
		 */
		public $data;
		
		/**
		 * DB application model
		 * @return Model
		 */
		protected $model;
		
		/**
		 * DB Connector
		 * @var integer|string
		 */
		protected $connector;
	
		/**
		 * SQL query join to db_tables
		 * @var string
		 */
		const DB_BIND_USERS = 'INNER JOIN db_retailnet.db_tables ON table_id = user_export_list_table_id';
		
		/**
		 * SQL query join to users
		 * @var string
		 */
		const DB_BIND_TABLES = 'INNER JOIN db_retailnet.users ON user_id = user_export_list_user_id';

		public function __construct($user) {
			$this->connector = Connector::DB_CORE;
			$this->user = $user;
			$this->model = new User_Export_List_Model($this->connector);
		}

		public function __get($key) {
			return $this->data["user_export_list_$key"];
		}

		public function read($id) { 
			$this->data = $this->model->read($id);
			$this->id = $this->data['user_export_list_id']; 
			return $this->data;
		}
		
		public function read_from_table($table) {
			$this->data = $this->model->read_from_table($this->user, $table);
			$this->id = $this->data['user_export_list_id'];
			return $this->data;
		}
		
		public function create($table, $fields, $sort=null) {
			$id = $this->model->create($this->user, $table, $fields, $sort);
			$this->read($id);
			return $id;
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
	}