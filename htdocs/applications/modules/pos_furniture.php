<?php 

	class Pos_Furniture {
		
		/** POS Furniture ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * POS Furniture Data
		 * @var array
		 */
		public $data;
		
		/**
		 * POS
		 * @var object
		 */
		public $pos;
		
		/**
		 * Furniture
		 * @var object
		 */
		public $item;
		
		/**
		 * Product Line
		 * @var object
		 */
		public $productline;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @var object
		 */
		protected $model;
		
		/**
		 * Join furnitures
		 * @var string
		 */
		const DB_BIND_ITEMS = "INNER JOIN db_retailnet.items ON item_id = mps_pos_furniture_item_id";
		
		/**
		 * Join product lines
		 * @var string
		 */
		const DB_BIND_PRODUCT_LINES = "INNER JOIN db_retailnet.product_lines ON product_line_id = mps_pos_furniture_product_line_id";
		
		
		/**
		 * POS Furniture<br />
		 * Application Dependent Modul
		 * @param string $connector
		 * @author aserifi
		 * @copyright mediaparx ag
		 */
		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new Pos_Furniture_Model($connector);
		}
		
		public function __get($key) {
			return $this->data["mps_pos_furniture_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_pos_furniture_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public function pos() {
			
			if (!$this->pos) {
				$this->pos = new Pos();
			}
			
			if ($this->posaddress) {
				$this->pos->read($this->posaddress);
			}
			
			return $this->pos;
		}
		
		public function item() {
				
			if (!$this->item) {
				$this->item = new Item();
			}
				
			if ($this->item_id) {
				$this->item->read($this->item_id);
			}
				
			return $this->pos;
		}
		
		public function productline() {
		
			if (!$this->productline) {
				$this->productline = new Product_Line();
			}
		
			if ($this->product_line) {
				$this->productline->read($this->product_line_id);
			}
		
			return $this->productline;
		}
	}