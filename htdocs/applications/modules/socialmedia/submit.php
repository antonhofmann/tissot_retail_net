<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['socialmedia_id'];


	$data = array();

	$socialmedia = new Modul($application);

	// set table name
	$socialmedia->setTable('socialmedias');
	
	if ($_REQUEST['socialmedia_brand_id']) {
		$data['socialmedia_brand_id'] = $_REQUEST['socialmedia_brand_id'];
	}

	if ($_REQUEST['socialmedia_channel_id']) {
		$data['socialmedia_channel_id'] = $_REQUEST['socialmedia_channel_id'];
	}

	if ($_REQUEST['socialmedia_country_id']) {
		$data['socialmedia_country_id'] = $_REQUEST['socialmedia_country_id'];
	}

	if ($_REQUEST['socialmedia_language_id']) {
		$data['socialmedia_language_id'] = $_REQUEST['socialmedia_language_id'];
	}

	if ($_REQUEST['socialmedia_posaddress_id']) {
		$data['socialmedia_posaddress_id'] = $_REQUEST['socialmedia_posaddress_id'];
	}

	if ($_REQUEST['socialmedia_url']) {
		$data['socialmedia_url'] = $_REQUEST['socialmedia_url'];
	}

	if ($_REQUEST['socialmedia_video_url']) {
		$data['socialmedia_video_url'] = $_REQUEST['socialmedia_video_url'];
	}

	if ($_REQUEST['socialmedia_video_description']) {
		$data['socialmedia_video_description'] = $_REQUEST['socialmedia_video_description'];
	}

	
	if ($data) {

		$socialmedia->read($id);

		if ($socialmedia->id) {
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			$response = $socialmedia->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $socialmedia->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}



	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));