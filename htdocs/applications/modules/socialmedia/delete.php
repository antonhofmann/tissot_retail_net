<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$socialmedia = new Modul($application);

// set table name
$socialmedia->setTable('socialmedias');

$permission = user::permission('can_edit_socialmedias');


$socialmedia->read($id);

if ($socialmedia->id && $permission) {
	
	$delete = $socialmedia->delete();
	
	if ($delete) {
		Message::request_deleted();
		url::redirect("/$application/$controller");
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}