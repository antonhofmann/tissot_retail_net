<?php 

	class Company_Warehouse {
		
		/**
		 * Warehouse ID
		 * @var int
		 */
		public $id;
		
		/**
		 * Warehouse Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Company Builder
		 * @return Company
		 */
		public $company;
		
		/**
		 * DB Company Warehouse Model
		 * @return Company_Warehouse_Model
		 */
		protected $model;
		
		/**
		 * DB: Join companies
		 * @var string
		 */
		const DB_BIND_COMPANIES = 'INNER JOIN db_retailnet.addresses ON address_id = address_warehouse_address_id';
		
		public function __construct() {
			$this->model = new Company_Warehouse_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["address_warehouse_$key"];
		}
		
		public function id($id) {
			$this->id = $id;
		}
		
		public function read($id) {
			
			$this->data = $this->model->read($id);
			
			if ($this->data) {
				
				$this->id = $this->data['address_warehouse_id'];
				
				if ($this->company) {
					$this->company->read($this->address_id);
				}
			}
			
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id) {
				return $this->model->update($this->id, $data);
			}
		}
		
		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public function delete_all() {
			if ($this->company->id) {
				$result = $this->model->delete_all($this->company->id);
			}
		}
		
		public function load($filters=null) {
			return $this->model->load($this->company->id, $filters);
		}
	
		/**
		 * Get Warehouse for Stock Reserve (suitable for order sheets)
		 */
		public function getStockReserve() {
			if ($this->company->id) {
				return $this->model->getStockReserve($this->company->id);
			}
		}
		
		/**
		 * Company Builder
		 * @return Company
		 */
		public function company($company=null) {
			
			if (!$this->company) {
				$this->company = new Company();
			}
			
			if ($company) {
				$this->company->read($company);
			}
			
			return $this->company;
		}
	}