<?php

	class User_Tracking {
	
		public $data;
		
		const DB_BIND_USERS = 'LEFT JOIN db_retailnet.users ON user_id = user_tracking_user_id';
		const ENTITY_ORDER_SHEETS = 'order sheet';
		const ACTION_WORKFLOW_STATE_COMPLETED = 'order sheet completed';
		const ACTION_WORKFLOW_STATE_APPROVED = 'order sheet approved';
		
		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new User_Tracking_Model($connector); 
		}
		
		public function __get($key) {
			return $this->data["user_tracking_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['user_tracking_id'];
			return $this->data;
		}
		
		public function read_from_entity($entity) {
			$this->data = $this->model->read_from_entity($entity);
			$this->id = $this->data['user_tracking_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) { 
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
	}