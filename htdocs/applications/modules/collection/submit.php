<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['collection_id'];

	$data = array();

	$collection = new Modul($application);

	switch ($application) {
		
		case 'mps':
		

			// set table name
			$collection->setTable('mps_material_collections');
			
			if ($_REQUEST['collection_code']) {
				$data['mps_material_collection_code'] = $_REQUEST['collection_code'];
			}

			if (isset($_REQUEST['collection_name'])) {
				$data['mps_material_collection_name'] = $_REQUEST['collection_name'];
			}

			if (in_array('collection_active', $fields)) {
				$data['mps_material_collection_active'] = ($_REQUEST['collection_active']) ? 1 : 0;
			}
			
		break;
		
		case 'lps':
		

			// set table name
			$collection->setTable('lps_collections');
			
			if ($_REQUEST['collection_code']) {
				$data['lps_collection_code'] = $_REQUEST['collection_code'];
			}

			if (isset($_REQUEST['collection_name'])) {
				$data['lps_collection_name'] = $_REQUEST['collection_name'];
			}

			if (in_array('collection_active', $fields)) {
				$data['lps_collection_active'] = ($_REQUEST['collection_active']) ? 1 : 0;
			}
			
		break;
	}

	

	if ($data) {

		$collection->read($id);

		if ($collection->id) {
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			$response = $collection->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $collection->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}



	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));