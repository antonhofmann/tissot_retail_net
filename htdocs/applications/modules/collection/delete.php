<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$collection = new Modul($application);

switch ($application) {
	
	case 'mps':
	

		// set table name
		$collection->setTable('mps_material_collections');

		$permission = user::permission(Material_Collection::PERMISSION_EDIT);
		
	break;

	case 'lps':
	

		// set table name
		$collection->setTable('lps_collections');

		$permission = user::permission('can_edit_lps_collections');

	break;
}

$collection->read($id);

if ($collection->id && $permission) {
	
	$delete = $collection->delete();
	
	if ($delete) {
		Message::request_deleted();
		url::redirect("/$application/$controller");
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$application/$controller");
}