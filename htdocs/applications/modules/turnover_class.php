<?php 

	class Turnover_Class {
		
		/**
		 * Turnover Class ID
		 * @var integer
		 */
		public $id;
		
		/**
		 * Turnover Class Data
		 * @var array
		 */
		public $data;
		
		/**
		 * Turnover Group
		 * @var object
		 */
		public $group;
		
		/**
		 * Database Connector
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Database Model
		 * @var object
		 */
		protected $model;
		
		/**
		 * Can Edit All Turnover Classes
		 * @var string
		 */
		const PERMISSION_EDIT = 'can_edit_mps_turnover_classes';
		
		/**
		 * Can View All Turnover Classes
		 * @var string
		 */
		const PERMISSION_VIEW = 'can_view_mps_turnover_classes';
		
		/**
		 * Join Turnover Groups
		 * @var string
		 */
		const DB_BIND_TURNOVER_GROUPS = 'INNER JOIN mps_turnover_groups ON mps_turnover_group_id = mps_turnover_class_group_id';
		
		
		/**
		 * Turnover Class<br />
		 * Allpication Dependet Modul
		 * @author aserifi
		 * @copyright mediaparx ag
		 */
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : url::application();
			$this->model = new Turnover_Class_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["mps_turnover_class_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['mps_turnover_class_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($application, $filters=null) {
			$model = new Turnover_Class_Model($application);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
		
		public function group() {
			
			if (!$this->group) {
				$this->group = new Turnover_Group($this->connector);
				$this->group->read($this->group_id);
			}
			
			return $this->group;
		}
		
		public function header() {
			if ($this->id) {
				return $this->group()->name.', '.$this->code.', '.$this->name;
			}
		}
	}