<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

// sql order
$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "unit_name";
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

// search keyword
$search_keyword = ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) ? $_REQUEST['search'] : null;

$model = new Model($application);

if ($search_keyword) {

	$filters['search'] = "(
		unit_name LIKE \"%$search_keyword%\"
	)";
}

$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		unit_id,
		unit_name
	FROM db_retailnet.units
")
->filter($filters)
->order($order, $direction)
->offset($offset, $rowsPerPage)
->fetchAll();

if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);

	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$pageIndex = $pager->index();
	$pageControlls = $pager->controlls();
}

// toolbox: hide utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$order' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => 'Add New'
		));
	}

if ($toolbox) {
	$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
}

$table = new Table(array(
	'sort' => array('column' => $order, 'direction' => $direction)
));

$table->datagrid = $datagrid;

$table->unit_name(
	"href=".$_REQUEST['data'],
	Table::PARAM_SORT
);

$table->footer($pageIndex);
$table->footer($pageControlls);
$table = $table->render();

echo $toolbox.$table;
