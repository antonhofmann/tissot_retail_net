<?php

	class DB_Reference { 
		
		const PERMISSION_EDIT = 'can_administrate_system_data';
		
		public $data;

		public function __construct() {
			$this->model = new DB_Reference_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["db_reference_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['db_reference_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
	}