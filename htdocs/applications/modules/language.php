<?php

	class Language { 
		
		const PERMISSION_EDIT = 'can_administrate_system_data';
		const DB_BIND_APPLICATIONS = 'INNER JOIN db_retailnet.language_applications ON language_application_language = language_id';
		
		public $data;

		public function __construct() {
			$this->model = new Language_Model(Connector::DB_CORE);
		}
		
		public function __get($key) {
			return $this->data["language_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['language_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public static function loader($filters=null) {
			$model = new Language_Model(Connector::DB_CORE);
			$result = $model->loader($filters);
			return _array::extract($result);
		}
		
		public static function dropdown($filters=null) {
			$model = new Language_Model(Connector::DB_CORE);
			$result = $model->dropdown($filters);
			return _array::extract($result);
		}
	}
	