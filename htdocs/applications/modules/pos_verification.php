<?php 

	class Pos_Verification {
		
		/**
		 * POS Verification ID
		 * @var int
		 */
		public  $id;
	
		/**
		 * Dataloader
		 * @var array
		 */
		public $data;
		
		/**
		 * Company Builder
		 * @return Company
		 */
		public $company;
		
		/**
		 * Verifications data
		 * @return Pos_Verification_Data
		 */
		public $verification;
		
		/**
		 * POS Verification Model
		 * @return Pos_Verification_Model
		 */
		protected $model;
		
		/**
		 * DB Connector name
		 * @var string
		 */
		protected $connector;
		
		/**
		 * Permission: view all data
		 * @var string
		 */
		const PERMISSION_VIEW_ALL = 'can_view_all_pos_verification_data';
		
		/**
		 * Permission: edit all data
		 * @var string
		 */
		const PERMISSION_EDIT_ALL = 'can_edit_all_pos_verification_data';
		
		/**
		 * Permission: view owner data
		 * @var string
		 */
		const PERMISSION_VIEW_LIMITED = 'can_view_his_pos_verification_data';
		
		/**
		 * Permission: edit owner data
		 * @var string
		 */
		const PERMISSION_EDIT_LIMITED = 'can_edit_his_pos_verification_data';
		
		/**
		 * DB JOIN: addresses
		 * @var string
		 */
		const DB_BIND_COMPANIES = "INNER JOIN addresses ON address_id = posverification_address_id";
		
		/**
		 * DB JOIN: addresses
		 * @var string
		 */
		const DB_BIND_POS = "INNER JOIN posaddresses ON posaddress_client_id = posverification_address_id";
		
		public function __construct($connector=null) {
			$this->connector = ($connector) ? $connector : Connector::DB_CORE;
			$this->model = new Pos_Verification_Model($this->connector);
		}
		
		public function __get($key) {
			return $this->data["posverification_$key"];
		}
		
		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $this->data['posverification_id'];
			return $this->data;
		}
		
		public function read_periode($company, $date) {
			$this->data = $this->model->read_periode($company, $date);
			$this->id = $this->data['posverification_id'];
			return $this->data;
		}
		
		public function create($data) {
			if (is_array($data)) {
				$id = $this->model->create($data);
				$this->read($id);
				return $id;
			}
		}
		
		public function update($data) {
			if ($this->id && is_array($data)) {
				$result = $this->model->update($this->id, $data);
				$this->read($this->id);
				return $result;
			}
		}

		public function delete() {
			if ($this->id) {
				return $this->model->delete($this->id);
			}
		}
		
		public function company() {
			
			if (!$this->company) {
				$this->company = new Company();
			}
			
			if ($this->address_id) {
				$this->company->read($this->address_id);
			}
			
			return $this->company;
		}
		
		public function setFromPreviousMonth($company, $timestamp) {
			
			$model = new Model(Connector::DB_CORE);
			
			$d = new DateTime($date);
			$d->setTimestamp($timestamp);
			$currentMonth = $d->format('U');
			
			$d->modify('first day of last month');
			$lastMonth = $d->format('Y-m-d');
			
			// get last month data
			$data = $this->read_periode($company, $lastMonth);
			
			// add data from previous month to current month
			if ($this->id) {
					
				$id = $this->create(array(
					'posverification_address_id' => $this->address_id,
					'posverification_date' => date('Y-m-01', $currentMonth),
					'user_created' => user::instance()->login
				));
					
				if ($id) {
			
					$result = $model->query("
						SELECT *
						FROM posverification_data
						WHERE posverification_data_posverification_id = ".$data['posverification_id']."
					")->fetchAll();
			
					if ($result) {
							
						$posVerificationData = new Pos_Verification_Data();
							
						foreach ($result as $row) {
			
							$channel = $row['posverification_data_distribution_channel_id'];
							$number = $row['posverification_data_actual_number'];
			
							$posVerificationData->create(array(
								'posverification_data_posverification_id' => $this->id,
								'posverification_data_distribution_channel_id' => $channel,
								'posverification_data_actual_number' => $number,
								'user_created' => user::instance()->login
							));
						}
					}
				}
			}
			
			return $this->data;
		}
		
		/**
		 * Pos verification Data
		 * @return Pos_Verification_Data
		 */
		public function verification() {
			
			if (!$this->verification) {
				$this->verification = new Pos_Verification_Data();
			}
			
			if ($this->id) {
				$this->verification->verification($this->id);
			}
			
			return $this->verification;
		}
	}
	