<?php 

	class Security {
		
		public function __construct() {
			$this->model = new Security_Model(Connector::DB_CORE);
		}
		
		public function permissions() {
			return $this->model->permissions();
		}
		
		public function userPermissions() {
			$user = User::instance();
			return $this->model->userPermissions($user->id);
		}
		
		public function traceFailure($ip,$source,$username,$password) {
			return $this->model->traceFailure($ip,$source,$username,$password);
		}
		
		public function trials() {
			$ip = url::ip();
			$result = $this->model->trials($ip);
			return ($result) ? $result['total'] : 0;
		}
		
		public function unlock() {
			$ip = url::ip();
			return $this->model->unlock($ip);
		}
		
		public function lock() {
			$ip = url::ip();
			return $this->model->lock($ip);
		}
		
		public function isLocked() {
			$ip = url::ip();
			$result = $this->model->isLocked($ip);
			return ($result) ? true : false;
		}
		
		public function isExcluded() {
			$ip = url::ip();
			$result = $this->model->isExcluded($ip);
			return ($result) ? true : false;
		}
	}