<?php

	class Red_Project_Sheet {


		public $id;
		public $data;


		public function __construct($connector=null) {
			$connector = ($connector) ? $connector : url::application();
			$this->model = new Red_Project_Sheet_Model($connector);
		}

		public function __get($name) {
			return $this->data["red_projectsheet_$name"];
		}

		public function read($id) {
			$this->data = $this->model->read($id);
			$this->id = $id;
			return $this->data;
		}

		public function create($data) {
			return $this->model->create($data);
		}

		public function update($data) {
			if ($this->id) {
				return $this->model->update($this->id, $data);
			}
		}
	
	}
