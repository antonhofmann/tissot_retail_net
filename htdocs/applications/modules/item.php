<?php

class Item {
	
	const PERMISSION_VIEW_MPS_FURNITURE = 'can_view_mps_furniture';
	const PERMISSION_EDIT_MPS_FURNITURE = 'can_edit_mps_furniture';
	
	const DB_BIND_ITEM_CATEGORIES = "INNER JOIN db_retailnet.item_categories ON item_category_id = item_category ";

	const TYPE_STANDARD = 1;
	const TYPE_SPECIAL = 2;
	const TYPE_COST_ESTIMATION = 3; 
	const TYPE_COST_EXLUSION = 4; 
	const TYPE_COST_NOTIFICATION = 5;
	const TYPE_COST_LOCAL_CONSTRUCTION_COST = 6;
	const TYPE_COST_DESIGN = 7;
	
	public $data;

	public function __construct() {
		$this->model = new Item_Model(Connector::DB_CORE);
	}
	
	public function __get($key) {
		return $this->data["item_$key"];
	}
	
	public function read($id) {
		$this->data = $this->model->read($id);
		$this->id = $this->data['item_id'];
		return $this->data;
	}
	
	public function create($data) {
		if (is_array($data)) {
			$id = $this->model->create($data);
			$this->read($id);
			return $id;
		}
	}
	
	public function update($data) {
		if ($this->id && is_array($data)) {
			$result = $this->model->update($this->id, $data);
			$this->read($this->id);
			return $result;
		}
	}

	public function delete() {
		if ($this->id) {
			return $this->model->delete($this->id);
		}
	}
	
	public static function loader($filters=null) {
		$model = new Item_Model(Connector::DB_CORE);
		$result = $model->loader($filters);
		return _array::extract($result);
	}
	
	public function header() {
		if ($this->id) {
			return $this->code.", ".$this->name;
		}
	}

	static public function roundPrice($price) {

		return $price ? round($price*20, 0) /20 : null;
	}

	public function updatePrice($price) {

		if (!$this->id) return;

		$user = User::instance();
		$price = self::roundPrice($price);

		// update item price (calculate width exchange rate)		
		$sth = $this->model->db->prepare("
			SELECT currency_exchange_rate_furniture , currency_factor
			FROM suppliers
			LEFT JOIN currencies ON currency_id = supplier_item_currency
			WHERE supplier_item = ? 
		");

		$sth->execute(array($this->id));
		$result = $sth->fetch();

		if (!$result) break;

		$exchangeRate = $result['currency_exchange_rate_furniture'] ?: 1;
		$currencyFactor = $result['currency_factor'] ?: 1;
		
		$itemPrice = ($price * $exchangeRate) / $currencyFactor;
		$itemPrice = $this->roundPrice($itemPrice); 

		$sth = $this->model->db->prepare("
			UPDATE items SET 
				item_price = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE item_id = ?
		");

		$success = $sth->execute(array($itemPrice, $user->login, $this->id));

		if ($success) {
			$this->data['item_price'] = $itemPrice;
		}

		return $success;
	}

	public function updateOrderPrices() {

		if (!$this->id) return;

		$orders = array();
		$updatedOrders = array();

		// get catalogue orders
		$sth = $this->model->db->prepare("
			SELECT DISTINCT order_id, order_client_exchange_rate, currency_factor 
			FROM orders 
			LEFT JOIN currencies ON currency_id = order_client_currency 
			WHERE order_type = 2 
			AND order_actual_order_state_code < 700 
			AND (order_archive_date IS NULL OR order_archive_date = '0000-00-00')
		");

		$sth->execute();
		$result = $sth->fetchAll();

		if ($result) {
			
			foreach ($result as $row) {
				
				$id = $row['order_id'];

				$orders[$id] = array(
					'rate' => $row['order_client_exchange_rate'], 
					'factor' => $row['currency_factor']
				);
			}
		}

		// get project orders
		$sth = $this->model->db->prepare("
			SELECT DISTINCT 
				order_id, 
				order_client_exchange_rate, 
				currency_factor, 
				SUM(order_item_system_price_hq_freezed) AS hq1, 
				sum(order_item_system_price_freezed) AS hq2 
			FROM orders 
			LEFT JOIN currencies ON currency_id = order_client_currency 
			LEFT JOIN order_items ON order_item_order = order_id 
			WHERE order_type = 1 
			AND order_item_type = 1  
			AND order_actual_order_state_code < 890 
			AND (order_archive_date IS NULL OR order_archive_date = '0000-00-00') 
			GROUP BY order_id, order_number, order_actual_order_state_code, order_client_exchange_rate, currency_factor
		");

		$sth->execute();
		$result = $sth->fetchAll();

		if ($result) {
			
			foreach ($result as $row) {

				$id = $row['order_id'];

				if ($row['hq2'] == 0) {
					$orders[$id] = array(
						'rate' => $row['order_client_exchange_rate'], 
						'factor' => $row['currency_factor']
					);
				}
			}
		}

		if (!$orders) return;

		// get item currency data
		$sth = $this->model->db->prepare("
			SELECT 
				supplier_item_price, 
				supplier_item_currency, 
				currency_exchange_rate_furniture, 
				currency_factor 
			FROM items 
			INNER JOIN suppliers ON supplier_item = item_id
			INNER JOIN currencies ON currency_id = supplier_item_currency 
			WHERE item_id = ? 
			AND supplier_item_currency > 0 
			AND currency_exchange_rate_furniture > 0 
			AND currency_factor > 0
		");

		$sth->execute(array($this->id));
		$result = $sth->fetch();

		if (!$result) return;

		$supplier_price = $result['supplier_item_price'];
		$system_price = $result['supplier_item_price'] * $result['currency_exchange_rate_furniture'] / $result['currency_factor'];
		$system_price = ceil($system_price/0.05)*0.05;

		if ($system_price <= 0) return;

		// update system price in order_items
		$sth = $sth = $this->model->db->prepare("
			UPDATE order_items SET
				order_item_system_price = ?,
				order_item_supplier_price = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE order_item_order = ? AND order_item_item = ?
		");

		foreach ($orders as $order => $row) {
			
			$success = $sth->execute(array($system_price, $supplier_price, $user->login, $order, $this->id));

			if ($success) $updatedOrders[] = $order;
		}

		if (!$updatedOrders) return;

		// update client prices CEILING(order_item_system_price / ? / 0.05) * 0.05
		$sth = $this->model->db->prepare("
			UPDATE order_items SET
				order_item_client_price = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE order_item_order = ? AND order_item_item = ?
		");
		
		foreach($updatedOrders as $order) {

			$rate = $orders[$order]['rate'];
			$factor = $orders[$order]['factor'];

			if (!$rate && !$factor) continue;

			$price = $system_price / $rate * $factor;
			$price = round($price*20, 0) /20;
			
			$success = $sth->execute(array($price, $user->login, $order, $this->id));
		}

		return $updatedOrders;
	}
}