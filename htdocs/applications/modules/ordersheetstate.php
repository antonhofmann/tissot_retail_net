<?php 

/**
 * Order Sheet Statments
 * 
 * checking tool for singel and group workflow states
 * 
 * @author admir.serifi@mediaparx.ch
 * @copyright mediaparx ag
 * @version 1.0
 */
class OrdersheetState {

	public $ordersheet;

	/**
	 * Current order sheet state
	 * @var int
	 */
	public $state;
	
	/** Order Sheet Open
	* @var boolean
	*/
	public $open;
	
	/**
	 * Order Sheet Close
	 * @var boolean
	 */
	public $close;
	
	/**
	 * Order Sheet Expired
	 * @var boolean
	 */
	public $expired;
	
	/**
	 * Order Sheet Manager
	 * @var boolean
	 */
	public $manager;
	
	/**
	 * Order Sheet Administrator
	 * @var boolean
	 */
	public $administrator;
	
	/**
	 * Order Sheet Owner
	 * @var boolean
	 */
	public $owner;

	/**
	 * Current application
	 * @var string
	 */
	private $application;
	

	public function __construct($data, $application=null) {

		$permissionView = user::permission('can_view_all_lps_sheets');
		$permissionViewLimited = user::permission('can_view_only_his_lps_sheets');
		$permissionEdit = user::permission('can_edit_all_lps_sheets');
		$permissionEditLimited = user::permission('can_edit_only_his_lps_sheets');
		$permissionManage = user::permission('can_manage_lps_sheets');

		// application
		$this->application = $application ?: Request::instance()->application;

		// current request
		$this->ordersheet = $data['ordersheet_id'];

		// order sheet state
		$this->state = $data['ordersheet_workflowstate_id'];
		
		// timestamp for today
		$today = strtotime(date('Y-m-d'));
			
		// check ordersheet opening date
		$this->open = ($today >= strtotime($data['ordersheet_openingdate'])) ? true : false;
			
		// check ordersheet closing date
		$this->close = ( $today <= strtotime($data['ordersheet_closingdate'])) ? false : true;
			
		// check is ordersheet date range expired
		$this->expired = ($this->open && $this->close) ? true : false;
			
		// order sheet administrator
		$this->administrator = ($permissionManage  || $permissionEdit) ? true : false;
			
		// order sheet manager
		$this->manager = $permissionManage;
			
		if (!$this->administrator && $permissionEditLimited) {

			// order sheet owner
			$company = new Company();
			$company->read($data['ordersheet_address_id']);
			$owner = $company->owner();

			$this->owner = $company->owner();
			
			if (!$this->owner) {
				$this->owner = $company->canEdit();
			}

			$this->canEdit = $this->owner;
		}
	}
	
	public function isPreparation() {

		switch ($this->application) {
				
			case 'mps':
				return ($this->state==7) ? true : false;
			break;

			case 'lps':
				return ($this->state==1) ? true : false;
			break;
		}
	}
	
	public function isOpen() {

		switch ($this->application) {
				
			case 'mps':
				return ($this->state==1) ? true : false;
			break;

			case 'lps':
				return ($this->state==2) ? true : false;
			break;
		}
	}
	
	public function isExpired() {
		return ($this->close) ? true : false;
	}
	
	public function isCompleted() {

		switch ($this->application) {
				
			case 'mps':
					return ($this->state==3) ? true : false;
			break;

			case 'lps':
				return ($this->state==4) ? true : false;
			break;
		}
	}
	
	public function isApproved() {

		switch ($this->application) {
				
			case 'mps':
				return ($this->state==4) ? true : false;
			break;

			case 'lps':
				return ($this->state==5) ? true : false;
			break;
		}
	}
	
	public function isRevision() {
		
		switch ($this->application) {
				
			case 'mps':
				return ($this->state==8) ? true : false;
			break;

			case 'lps':
				return ($this->state==6) ? true : false;
			break;
		}
	}
	
	public function isConsolidated() {
		
		switch ($this->application) {
				
			case 'mps':
				return ($this->state==5) ? true : false;
			break;

			case 'lps':
				return ($this->state==7) ? true : false;
			break;
		}
	}
	
	public function isSalesOrderSubmitted() {

		switch ($this->application) {
				
			case 'mps':
				return ($this->state==9) ? true : false;
			break;

			case 'lps':
				
			break;
		}
	}
	
	public function isOrderConfirmed() {

		switch ($this->application) {
				
			case 'mps':
				return ($this->state==10) ? true : false;
			break;

			case 'lps':
				
			break;
		}
	}
	
	public function isShipped() {

		switch ($this->application) {
				
			case 'mps':
				return ($this->state==12) ? true : false;
			break;

			case 'lps':
				
			break;
		}
	}
	
	public function inDistribution() {

		switch ($this->application) {
				
			case 'mps':
				return ($this->state==11) ? true : false;
			break;

			case 'lps':
				
			break;
		}
	}
	
	public function isPartiallyDistributed() {

		switch ($this->application) {
				
			case 'mps':
				return ($this->state==13) ? true : false;
			break;

			case 'lps':

				
			break;
		}
	}
	
	public function isPartiallyShipped() {

		switch ($this->application) {
				
			case 'mps':
				return ($this->state==14) ? true : false;
			break;

			case 'lps':

				
			break;
		}
	}
	
	public function isDistributed() {

		switch ($this->application) {
				
			case 'mps':
				return ($this->state==6) ? true : false;
			break;

			case 'lps':
				return ($this->state==10) ? true : false;
			break;
		}
	}
	
	public function isManuallyArchived() {

		switch ($this->application) {
				
			case 'mps':
				return ($this->state==15) ? true : false;
			break;

			case 'lps':
				
			break;
		}
	}
	
	public function isPartiallyExported() {

		switch ($this->application) {
				
			case 'mps':
				
			break;

			case 'lps':
				return ($this->state==8) ? true : false;
			break;
		}
	}
	
	public function isNotExported() {

		switch ($this->application) {
				
			case 'mps':
				
			break;

			case 'lps':
				return ($this->state==9) ? true : false;
			break;
		}
	}
	
	public function isExported() {

		switch ($this->application) {
				
			case 'mps':
				
			break;

			case 'lps':
				return ($this->state==10) ? true : false;
			break;
		}
	}
	
	public function isArchived() {

		switch ($this->application) {
				
			case 'mps':
				return ($this->state==16) ? true : false;
			break;

			case 'lps':
				return ($this->state==11) ? true : false;
			break;
		}
	}
	
	public function canDelete() {
		
		switch ($this->application) {
				
			case 'mps':
				return ($this->manager && in_array($this->state,array(1,7))) ? true : false;
			break;

			case 'lps':
				return ($this->manager && in_array($this->state,array(1,2))) ? true : false;
			break;
		}
	}
	
	public function canSubmit() {
		
		$states = static::loader('complete', $this->application); 
		
		return ($this->owner && $this->open && !$this->close && in_array($this->state , $states)) ? true : false;
	}
	
	public function canApprov() {

		$states = static::loader('approve', $this->application);

		return ($this->canAdministrate() && in_array($this->state, $states)) ? true : false;
	}
	
	public function canSetRevisions() {

		$states = static::loader('approve', $this->application);

		return ($this->canAdministrate() && $this->isCompleted()) ? true : false;
	}

	public function canAdministrate() {
		return ($this->manager || $this->administrator) ? true : false;
	}
	
	public function onPreparation() {
		$states = static::loader('preparation', $this->application); 
		return $states && in_array($this->state , $states) ? true : false;
	}
	
	public function onCompleting() {
		$states = static::loader('complete', $this->application);
		return (in_array($this->state , $states)) ? true : false;
	}
	
	public function onApproving() {
		$states = static::loader('approve', $this->application);
		return (in_array($this->state , $states)) ? true : false;
	}
	
	public function onConfirmation() {
		$states = static::loader('confirmation', $this->application);
		return (in_array($this->state , $states)) ? true : false;
	}
	
	public function onDistribution() {
		$states = static::loader('distribution', $this->application);
		return (in_array($this->state , $states)) ? true : false;
	}	

	public function onExporting() {
		$states = static::loader('exporting', $this->application); 
		return (is_array($states) && in_array($this->state , $states)) ? true : false;
	}
	
	/**
	 * Group Statments
	 * @param string $group <br />
	 * 	[preparation]  when a order sheet is in preparation mode <br />
	 * 	[complete] when a order sheet can be submitted<br /> 
	 * 	[approve] when a order sheet can be approved <br />
	 * 	[revision] when a order sheet can be send in revision <br />
	 * 	[confirmation] order sheet is on confirmation by externe system <br />
	 * 	[distribution] when a order sheet items can be distributed <br />
	 * 	[delete] when a order sheet can be deleted
	 * @return array indexed workflow states
	 */
	static public function loader($group, $application=null) {

		if (!$application) {
			$application = Request::instance()->application;
		}
		
		switch ($group) {

			case 'delete':

				switch ($application) {
				
					case 'mps':
						return array(1,2,7,8);
					break;

					case 'lps':
						return array(1,2,3,6);
					break;
				}

			break;
			
			case 'preparation':

				switch ($application) {
				
					case 'mps':
						return array(1,2,3,4,7,8);
					break;

					case 'lps':
						return array(1,2,3,4,5,6);
					break;
				}

			break;
			
			case 'complete':

				switch ($application) {
				
					case 'mps':
						return array(1,2,8);
					break;

					case 'lps':
						return array(2,3,6);
					break;
				}

			break;
			
			case 'approve':

				switch ($application) {
				
					case 'mps':
						return array(1,2,3,7,8);
					break;

					case 'lps':
						return array(1,2,3,4,6);
					break;
				}

			break;
			
			case 'revision':

				switch ($application) {
				
					case 'mps':
							return array(3,4);
					break;

					case 'lps':
						return array(4,5);
					break;
				}

			break;
			
			case 'confirmation':

				switch ($application) {
				
					case 'mps':
						return array(5,9,10);
					break;

					case 'lps':
						return array();
					break;
				}

			break;
			
			case 'distribution':

				switch ($application) {
				
					case 'mps':
						return array(11,12,13,14,15);
					break;

					case 'lps':
						return array();
					break;
				}

			break;
			
			case 'exporting':

				switch ($application) {
				
					case 'mps':
						return array();
					break;

					case 'lps':
						return array(8,9);
					break;
				}

			break;
		}
	}
}