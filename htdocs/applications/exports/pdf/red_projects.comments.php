<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once(PATH_LIBRARIES.'/tcpdf/tcpdf.php');

$user = User::instance();
if (!($user->permission(Red_project::PERMISSION_VIEW_PROJECTS) || $user->permission(Red_project::PERMISSION_EDIT_COMMENTS)))  {
	die('No permission');
}
define("CATEGORY_X_POS", 15);
define("COMMENT_X_POS", 15);
define("FILE_X_POS", 15);

define("DISTANCE_CATEGORY_COMMENT", 8);
define("DISTANCE_COMMENT_CATEGORY", 5);
define("DISTANCE_COMMENT_COMMENT", 5);


define("CONTENT_Y_POS", 25);

define("STANDARD_FONT_SIZE", 7);
define("CATEGORY_FONT_SIZE", 10);
define("FILE_FONT_SIZE", 5);
define("STANDARD_CELL_HEIGHT", 3.5);

$project_id = $_REQUEST['project_id'];
$project = new Red_project('red');
$project->read($project_id);
$project_title = $project->data['red_project_title'];

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

  //Page header
  public function Header() {
    global $project_title;
    // Logo
    $image_file = $_SERVER['DOCUMENT_ROOT'].'/public/images/logo.jpg';
    $this->Image($image_file, 16,8,33, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    // Set font
    $this->SetFont('arialn', 'B', 12);
    // Title
    $this->SetXY(60, 4);
    $this->Cell(0, 15, $project_title, 0, 0, 'R');
    $this->SetXY(60, 9);
    $this->SetFont('arialn', '', 12);
    $this->Cell(0, 15, 'Comments', 0, 0, 'R');
  }

  // Page footer
  public function Footer() {
    // Position at 15 mm from bottom
    $this->SetY(-15);
    // Set font
    $this->SetFont('arialn', 'I', 8);
    // Page number
	$this->Cell(0, 0, 'Page '.$this->PageNo().'/'.$this->getNumPages().', '.date('d.m.Y'). ' '.date('H:i'), 0, 0, 'R');
  }
}

function getFormattedNumber($number) {
  return number_format($number, 2, '.' , "'");
}

function limitChars($str, $length=MAX_CHARS) {
  return substr($str, 0, $length);
}


// Get comments allocated to this project
$model = new Model(Connector::DB_RETAILNET_RED);
$comments = $model->query("
				  SELECT SQL_CALC_FOUND_ROWS
			          red_comment_id,
			          red_comment_project_id,
			          red_comment_file_id,
			          red_comment_category_id,
			          red_comment_owner_user_id,
			          red_comment_comment,
			          red_commentcategory_name,
			          user_firstname,
			          user_name,
			          red_comment_comment,
			          DATE_FORMAT(red_comments.date_created, '%d.%m.%Y') AS date_created
				  FROM red_comments
				 ")
->bind(Red_Comment::BIND_USER_ID)
->bind(Red_Comment::BIND_COMMENT_CATEGORY)
->filter('red_comment_project_id', 'red_comment_project_id='.$project_id)
->order('red_comments.date_created', 'DESC')
->fetchAll();

if ($comments) {
  foreach ($comments as $row) {
    extract($row);
    $data[$red_comment_category_id]['caption'] = $red_commentcategory_name;
    $data[$red_comment_category_id]['comment'][$red_comment_id]['comment'] = "$red_comment_comment";
    $data[$red_comment_category_id]['comment'][$red_comment_id]['user'] = "$user_firstname $user_name";
    $data[$red_comment_category_id]['comment'][$red_comment_id]['date_created'] = $date_created;
    $file_ids = unserialize($red_comment_file_id);
    if (is_array($file_ids)) {
      foreach ($file_ids as $file_id) {
        $file = new Red_file('red');
        $file->read($file_id);
        if (isset($file->data['red_file_id'])) {
          $id = $file->data['red_file_id'];
          $data[$red_comment_category_id]['comment'][$red_comment_id]['files'][$id]['title']  = $file->data['red_file_title'];
          $data[$red_comment_category_id]['comment'][$red_comment_id]['files'][$id]['name']  = basename($file->data['red_file_path']);
        }
      }
    }
  }
}

// Create and setup PDF document
$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

$pdf->Open();
$pdf->SetTitle($row['project_sheet_name']);
$pdf->SetAuthor("Retailnet");
$pdf->SetDisplayMode(150);

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');


// set header data
$pdf->SetX(CATEGORY_X_POS);
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

$pdf->AddPage();

$pdf->SetXY(CATEGORY_X_POS, CONTENT_Y_POS);

$pdf->SetFillColor(238, 243, 246);

if (isset($data)) {
	foreach ($data as $category) {
	  //Category name
	  $pdf->SetFont("arialn", "B", CATEGORY_FONT_SIZE);
	  $pdf->SetX(CATEGORY_X_POS);
	  $pdf->Cell(0, STANDARD_CELL_HEIGHT, $category['caption'],'B');

	  //Comments and Files
	  $pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
	  $pdf->SetXY(COMMENT_X_POS, $pdf->GetY() + DISTANCE_CATEGORY_COMMENT);
	  foreach ($category['comment'] as $comment) {
	    //Comment
	    $pdf->SetFont("arialn", "I", FILE_FONT_SIZE);
	    $pdf->MultiCell(0, STANDARD_CELL_HEIGHT-2, $comment['user'].', '.$comment['date_created'],  "", "L", true);
	    $pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
	    $pdf->MultiCell(0, STANDARD_CELL_HEIGHT, $comment['comment'],  "", "L", true);

	    //Files
	    if (is_array($comment['files'])) {
	      $pdf->SetX(FILE_X_POS);
	      $pdf->SetFont("arialn", "B", FILE_FONT_SIZE);
	      $pdf->MultiCell(0, 0, '', "", "L", true);
	      $pdf->MultiCell(0, STANDARD_CELL_HEIGHT/10, 'Files:', "", "L", true);
	      $pdf->SetFont("arialn", "", FILE_FONT_SIZE);
	      foreach ($comment['files'] as $file) {
	        $pdf->SetX(FILE_X_POS);
	        $pdf->MultiCell(0, STANDARD_CELL_HEIGHT/10, "- ".$file['title'] ." (". $file['name'].")", "", "L", true );
	      }
	    }
	    $pdf->SetXY(COMMENT_X_POS, $pdf->GetY()+DISTANCE_COMMENT_COMMENT);
	  }
	  $pdf->SetXY(CATEGORY_X_POS, $pdf->GetY() + DISTANCE_COMMENT_CATEGORY);
	}
}



$pdf->Output();
