<?php 
/*
	Creat a COMPANY Overview

    Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2011-04-04
    Modified by:    
    Date modified:  
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*/
		

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	require_once PATH_LIBRARIES."tcpdf/tcpdf.php";

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$topic = $_REQUEST['topic'];
	$section = $_REQUEST['section'];
	$url = $_REQUEST['url'];
	
	
	$model = new Model(Connector::DB_CORE);
	
	// get contents by URL
	if ($url) {

		$result = $model->query("
			SELECT DISTINCT
				assistance_section_id,
				assistance_id,
				assistance_title,
				assistance_section_title,
				assistance_section_content
			FROM assistance_sections
			INNER JOIN assistance_section_links ON assistance_section_link_section_id = assistance_section_id
			INNER JOIN assistances ON assistance_id = assistance_section_assistance_id
			WHERE assistance_section_published = 1 AND assistance_section_link_link = '$url'
			ORDER BY 
				assistance_section_order,
				assistance_title,
				assistance_section_title		
		")->fetchAll();
	} 
	// get contents by section
	elseif($section) {
		
		$result = $model->query("
			SELECT DISTINCT
				assistance_section_id,
				assistance_id,
				assistance_title,
				assistance_section_title,
				assistance_section_content
			FROM assistance_sections
			INNER JOIN assistances ON assistance_id = assistance_section_assistance_id
			WHERE assistance_section_id = $section
			ORDER BY 
				assistance_section_order,
				assistance_title,
				assistance_section_title		
		")->fetchAll();
	} 
	// get contents by topic
	elseif($topic) {
		
		$result = $model->query("
			SELECT DISTINCT
				assistance_section_id,
				assistance_id,
				assistance_title,
				assistance_section_title,
				assistance_section_content
			FROM assistance_sections
			INNER JOIN assistances ON assistance_id = assistance_section_assistance_id
			WHERE assistance_id = $topic
			ORDER BY 
				assistance_section_order,
				assistance_title,
				assistance_section_title		
		")->fetchAll(); 
	}
	
		
	if ($result) {
		
		// build help content
		foreach ($result as $row) {
			
			$topic = $row['assistance_id'];
			$section = $row['assistance_section_id'];
			
			if (!$topics[$topic]['caption']) {
				$topics[$topic]['caption'] = $row['assistance_title'];
			}
			
			$topics[$topic]['sections'][$section]['title'] = nl2br($row['assistance_section_title']);
			$topics[$topic]['sections'][$section]['content'] = $row['assistance_section_content'];
		}
		
		$totalTopics = count($topics);
		
		// content contain more then one book
		$multiple = ($totalTopics > 1) ? true : false;
		
		// help box caption
		if ($multiple) {
			$caption = 'Help';
		} else {
			reset($topics);
			$key = key($topics);
			$caption = $topics[$key]['caption'];
		}
	
		
		// headers
		class MYPDF extends TCPDF {
				
			function Header() {
				$this->Image( $_SERVER['DOCUMENT_ROOT'].'/pictures/brand_logo.jpg',12,8,33);
				//Arial bold 15
				$this->SetFont('arialn','B',12);
				//Move to the right
				$this->Cell(80);
				//Title
				$this->Cell(0,34,'Retailnet Help',0,0,'R');
				//Line break
				$this->Ln(20);
			}
		
			function Footer()
			{
				//Position at 1.5 cm from bottom
				$this->SetY(-15);
				//arialn italic 8
				$this->SetFont('arialn','I',8);
				//Page number
				$this->Cell(0,10, date("d.m.y") . ' / Page '.$this->PageNo(),0,0,'R');
			}
		}
		
		
		// Instanciation of inherited class
		$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
		$pdf->SetMargins(10, PDF_MARGIN_TOP, 11);
		
		$pdf->Open();
	
		$pdf->AddFont('arialn');
		
		$pdf->AddPage();
		
		// set text shadow effect
		$pdf->setTextShadow(array(
			'enabled'=>true, 
			'depth_w'=>0.2, 
			'depth_h'=>0.2, 
			'color'=>array(196,196,196), 
			'opacity'=>1, 
			'blend_mode'=>'Normal'
		));
		
		$i = 0;
		
		// build content
		foreach ($topics as $topic => $row) {
			
			$i++;
			
			// multiple topics
			if ($row['caption']) {
				$pdf->SetFont('arialn','B',20);
				$pdf->Cell(0, 0, $row['caption'], 0, 1, 'L', 0);
				$pdf->Ln();
			}
			
			// assistance sections
			foreach ($row['sections'] as $section => $data) {

				
				$tmp = "http://" . $_SERVER["HTTP_HOST"];
				$data['content'] = str_replace($tmp, "", $data['content']);
				$tmp = "https://" . $_SERVER["HTTP_HOST"];
				$data['content'] = str_replace($tmp, "", $data['content']);
			
				$pdf->SetFont('arialn','',15);
				
				$pdf->Ln();
				
				$pdf->writeHTMLCell(0, 0, '', '', nl2br($data['title']), 0, 1, 0, true, '', true);
				$pdf->Cell(0, 0, '', 0, 1, 'L', 0);
				
				$pdf->SetFont('arialn','',12);
				$pdf->writeHTMLCell(0, 0, '', '', $data['content'], 0, 1, 0, true, '', true);
				$pdf->Cell(0, 0, '', 0, 1, 'L', 0);
			}
			
			if ($i < $totalTopics) {
				$pdf->AddPage();
			}
		}
		

		$pdf->Output();
	}
	else {
		
		die('Error: Check Request.');
	}
