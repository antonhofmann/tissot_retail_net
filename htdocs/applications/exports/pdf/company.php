<?php 
/*
	Creat a COMPANY Overview

    Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2011-04-04
    Modified by:    
    Date modified:  
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*/
		

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	require_once PATH_LIBRARIES."tcpdf/tcpdf.php";

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	
	$company = new Company();
	$company->read($id);
	

	if ($company->id) {
		
		// country
		$country = new Country();
		$country->read($company->country);
		
		// place
		$place = new Place();
		$place->read($company->place_id);
		
		// header
		$header = $translate->company.': '.$company->company.", ".$place->name.", ".$country->name;

		// classifications
		$classifications = array();
	
		if($company->canbefranchisee) {
			$classifications[] = $translate->address_franchisee;	
		}
		
		if($company->canbefranchisee_worldwide) {
			$classifications[] = $translate->address_canbefranchisee_worldwide;
		}
		
		if($company->canbejointventure) {
			$classifications[] = $translate->address_canbejointventure;
		}
		
		if($company->canbecooperation) {
			$classifications[] = $translate->address_canbecooperation;
		}
		
		if($company->swatch_retailer) {
			$classifications[] = $translate->address_swatch_retailer;
		}
		
		if ($classifications) {
			$classification = join(', ', $classifications);
		}
	
		class MYPDF extends TCPDF {
			
			//Page header
			function Header() {
				
				global $lang;
				//Logo
				$this->Image( $_SERVER['DOCUMENT_ROOT'].'/public/images/logo.jpg',10,8,33);
				//Arial bold 15
				$this->SetFont('arialn','B',12);
				//Move to the right
				$this->Cell(80);
				//Title
				$this->Cell(0,34,'Company Sheet',0,0,'R');
				//Line break
				$this->Ln(20);
	
			}
	
			//Page footer
			function Footer()
			{
				//Position at 1.5 cm from bottom
				$this->SetY(-15);
				//arialn italic 8
				$this->SetFont('arialn','I',8);
				//Page number
				$this->Cell(0,10, date("d.m.y") . ' / Page '.$this->PageNo(),0,0,'R');
			}
	
		}

		//Instanciation of inherited class
		$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
		$pdf->SetMargins(10, PDF_MARGIN_TOP, 11);
	
		$pdf->Open();
		
	
		$pdf->SetFillColor(220, 220, 220); 
	
		$pdf->AddFont('arialn');
		$pdf->AddFont('arialn', 'B');
	
		$pdf->AddPage();
	
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190,7, $header, 1, 0, 'L', 1);
		$pdf->Ln();
		$pdf->SetFont('arialn','',10);
		$pdf->Ln();	
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(30, 5, $translate->address_company ,1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(160, 5, $company->company.' '.$company->company2, 1, 0, 'L', 0);

		$pdf->Ln();
	
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(30, 5, $translate->address_address, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(160, 5, $company->street.' '.$company->streetnumber, 1, 0, 'L', 0);

		$pdf->Ln();
	
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(30, 5, $translate->address_zip, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(65, 5, $company->zip.' '. $place->name, 1, 0, 'L', 0);
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(30, 5, $translate->address_country ,1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(65, 5, $country->name, 1, 0, 'L', 0);
		$pdf->Ln();
	
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(30, 5, $translate->address_phone, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(65, 5, $company->phone, 1, 0, 'L', 0);
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(30, 5, $translate->address_mobile_phone,1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(65, 5, $company->mobile_phone, 1, 0, 'L', 0);
		$pdf->Ln();
	
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(30, 5,$translate->address_email, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(65, 5, $company->email, 1, 0, 'L', 0);
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(30, 5, $translate->address_website, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(65, 5, $company->website, 1, 0, 'L', 0);
		$pdf->Ln();
	
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(30, 5, $translate->address_contact_name,1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(65, 5, $company->contact_name, 1, 0, 'L', 0);
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(30, 5, $translate->address_contact_email, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(65, 5, $company->contact_email, 1, 0, 'L', 0);
		
		$pdf->Ln();

		// print pdf
		$pdf->Output();
	} else {
		message::invalid_request();
		url::redirect("/$application/$controller.$archived");
	}
