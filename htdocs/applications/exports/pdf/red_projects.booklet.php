<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once(PATH_LIBRARIES.'/tcpdf/tcpdf.php');

$user = User::instance();
if (!($user->permission(Red_project::PERMISSION_VIEW_PROJECTS) || $user->permission(Red_project::PERMISSION_EDIT_BASIC_DATA)))  {
  die('No permission');
}

define("TAB_NAME_X_POS", 15);

define("CELL_1_WIDTH", 50);

define("NEW_LINE_TITLE_CONTENT", 10);
define("NEW_LINE_CONTENT_TITLE", 12);

define("CATEGORY_X_POS", 15);
define("COMMENT_X_POS", 15);
define("FILE_X_POS", 15);

define("DISTANCE_CATEGORY_COMMENT", 8);
define("DISTANCE_COMMENT_CATEGORY", 5);
define("DISTANCE_COMMENT_COMMENT", 5);


define("STANDARD_FONT_SIZE", 7);
define("TAB_FONT_SIZE", 16);
define("CATEGORY_FONT_SIZE", 10);

define("FILE_FONT_SIZE", 5);
define("STANDARD_CELL_HEIGHT", 3.5);


$project_id = $_REQUEST['id'];
$project = new Red_project('red');
$project->read($project_id);
$project_title = $project->data['red_project_title'];


function get_substring($string, $limit, $add_string='...') {
	return (strlen($string) > $limit) ? substr($string,0,$limit).'...' : $string;
}


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

  //Page header
  public function Header() {
    global $project_title;
    // Logo
    $image_file = $_SERVER['DOCUMENT_ROOT'].'/public/images/logo.jpg';
    $this->Image($image_file, 16,8,'',6, 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    // Set font
    $this->SetFont('arialn', 'B', 12);
    // Title
    $this->SetXY(60, 4);
    $this->Cell(0, 15, $project_title, 0, 0, 'R');
    $this->SetXY(60, 9);
    $this->SetFont('arialn', '', 12);
    $this->Cell(0, 15, 'Project booklet', 0, 0, 'R');
  }

  // Page footer
  public function Footer() {
    // Position at 15 mm from bottom
    $this->SetY(-15);
    // Set font
    $this->SetFont('arialn', 'I', 8);
    // Page number
	$this->Cell(0, 0, 'Page '.$this->PageNo().'/'.$this->getNumPages().', '.date('d.m.Y'). ' '.date('H:i'), 0, 0, 'R');
  }

  // Colored table
  public function ColoredTable($header,$data, $array_header_width) {
  	// Colors, line width and bold font
  	$this->SetFillColor(238, 243, 246);
  	//$this->SetTextColor(255);
  	//$this->SetDrawColor(128, 0, 0);
  	$this->SetLineWidth(0.3);
  	$this->SetFont('', 'B', STANDARD_FONT_SIZE);
  	// Header
  	$num_headers = count($header);
  	for($i = 0; $i < $num_headers; ++$i) {
  		$this->Cell($array_header_width[$i], 7, $header[$i], 1, 0, 'L', 1);
  	}
  	$this->Ln();
  	// Color and font restoration
  	$this->SetFillColor(238, 243, 246);
  	$this->SetTextColor(0);
  	//$this->SetFont('');
  	// Data
  	$fill = 0;
  	//krumo::dump($data);
  	$this->SetFont('', '', STANDARD_FONT_SIZE);
  	foreach($data as $row) {
		$i=0;
  		foreach($row as $value) {
  			$this->Cell($array_header_width[$i], 6, $value, 'LR', 0, 'L', $fill);
  			$i++;
  		}
  		$this->Ln();
  		$fill=!$fill;
  	}
  	$this->Cell(array_sum($array_header_width), 0, '', 'T');
  }
}

function getFormattedNumber($number) {
  return number_format($number, 2, '.' , "'");
}

function limitChars($str, $length=MAX_CHARS) {
  return substr($str, 0, $length);
}


$project = new Red_project('red');
$project->read($project_id);
$projectstate = new Red_Project_State('red');
$projectstate->read($project->data['red_project_projectstate_id']);
$projecttype = new Red_Project_Type('red');
$projecttype->read($project->data['red_project_projecttype_id']);

$project_leader = new User();
$project_leader->read($project->data['red_project_leader_user_id']);

// Create and setup PDF document
$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

$pdf->Open();
$pdf->SetTitle($row['project_sheet_name']);
$pdf->SetAuthor("Retailnet");
$pdf->SetDisplayMode(150);

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');


// set header data
$pdf->SetX(CATEGORY_X_POS);
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

$pdf->AddPage();

$pdf->SetFillColor(238, 243, 246);


// Writing Content

// BASICDATA
$pdf->SetFont("arialn", "B", TAB_FONT_SIZE);
$pdf->SetY(25);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, 'Basicdata');
$pdf->Ln(NEW_LINE_TITLE_CONTENT);

//Project Title
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->Cell(CELL_1_WIDTH, STANDARD_CELL_HEIGHT, 'Project Title:');
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, $project->data['red_project_title']);
$pdf->Ln();
//Project Number
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->Cell(CELL_1_WIDTH, STANDARD_CELL_HEIGHT, 'Project Number:');
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, $project->data['red_project_projectnumber']);
$pdf->Ln();
//Project State
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->Cell(CELL_1_WIDTH, STANDARD_CELL_HEIGHT, 'Project State:');
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, $projectstate->data['red_projectstate_name']);
$pdf->Ln();
//Project Leader
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->Cell(CELL_1_WIDTH, STANDARD_CELL_HEIGHT, 'Project Leader:');
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, $project_leader->name.', '.$project_leader->firstname);
$pdf->Ln();
//Type
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->Cell(CELL_1_WIDTH, STANDARD_CELL_HEIGHT, 'Type:');
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, $projecttype->data['red_projecttype_name']);
$pdf->Ln();
//Start Date
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->Cell(CELL_1_WIDTH, STANDARD_CELL_HEIGHT, 'Start Date:');
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, $project->data['red_project_startdate']);
$pdf->Ln();
//Foreseen Enddate
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->Cell(CELL_1_WIDTH, STANDARD_CELL_HEIGHT, 'Foreseen Enddate:');
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, $project->data['red_project_foreseen_enddate']);
$pdf->Ln();
//Enddate
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->Cell(CELL_1_WIDTH, STANDARD_CELL_HEIGHT, 'Enddate:');
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, $project->data['red_project_enddate']);
$pdf->Ln(NEW_LINE_CONTENT_TITLE);

// DESCRIPTION
$pdf->SetFont("arialn", "B", TAB_FONT_SIZE);
$pdf->SetX(TAB_NAME_X_POS);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, 'Description');
$pdf->Ln(NEW_LINE_TITLE_CONTENT);

//Context
$pdf->SetFont("arialn", "B", CATEGORY_FONT_SIZE);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, 'Context:');
$pdf->Ln(STANDARD_CELL_HEIGHT * 1.5);
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->MultiCell(0, STANDARD_CELL_HEIGHT, $project->data['red_project_context'], '', 'L');
$pdf->Ln(STANDARD_CELL_HEIGHT);
//Description
$pdf->SetFont("arialn", "B", CATEGORY_FONT_SIZE);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, 'Description:');
$pdf->Ln(STANDARD_CELL_HEIGHT * 1.5);
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->MultiCell(0, STANDARD_CELL_HEIGHT, $project->data['red_project_description'], '', 'L');
$pdf->Ln(STANDARD_CELL_HEIGHT);
//Resctrictions
$pdf->SetFont("arialn", "B", CATEGORY_FONT_SIZE);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, 'Resctrictions:');
$pdf->Ln(STANDARD_CELL_HEIGHT * 1.5);
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->MultiCell(0, STANDARD_CELL_HEIGHT, $project->data['red_project_restrictions'], '', 'L');
$pdf->Ln(STANDARD_CELL_HEIGHT);
//Timeline
$pdf->SetFont("arialn", "B", CATEGORY_FONT_SIZE);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, 'Timeline:');
$pdf->Ln(STANDARD_CELL_HEIGHT * 1.5);
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->MultiCell(0, STANDARD_CELL_HEIGHT, $project->data['red_project_timeline'], '', 'L');
$pdf->Ln(STANDARD_CELL_HEIGHT);

/*
//External Partners
$pdf->SetFont("arialn", "B", CATEGORY_FONT_SIZE);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, 'External Partners:');
$pdf->Ln(STANDARD_CELL_HEIGHT * 1.5);
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->MultiCell(0, STANDARD_CELL_HEIGHT, $project->data['red_project_external_partners'], '', 'L');
$pdf->Ln(NEW_LINE_CONTENT_TITLE);
*/
// PARTNERS
if ($user->permission(Red_project::PERMISSION_VIEW_PARTNERLIST)) {
	$pdf->SetFont("arialn", "B", TAB_FONT_SIZE);
	$pdf->SetX(TAB_NAME_X_POS);
	$pdf->Cell(0, STANDARD_CELL_HEIGHT, 'Partners');

	// Get partners allocated to this project
	$model = new Model(Connector::DB_RETAILNET_RED);
	$result = $model->query("
					  SELECT SQL_CALC_FOUND_ROWS
						red_partnertype_name,
						address_company,
						user_name,
						user_firstname,
						DATE_FORMAT(red_project_partner_access_from, '%d.%m.%Y') as red_project_partner_access_from,
						DATE_FORMAT(red_project_partner_access_until, '%d.%m.%Y') as red_project_partner_access_until
					  FROM red_project_partners
					 ")
	->bind(Red_project::BIND_USER_ID)
	->bind(Red_project::BIND_ADDRESSES)
	->bind(Red_project::BIND_PARTNER_TYPE)
	->filter('red_project_partners', 'red_project_partner_project_id='.$project_id)
	->fetchAll();

	//Write Table
	if (is_array($result)) {
		$header = array('Partner Type', 'Company Place', 'Last Name', 'First Name', 'Grant access from', 'Grant access until');
		$pdf->Ln(NEW_LINE_TITLE_CONTENT);
		$pdf->ColoredTable($header, $result, array(30, 50, 26, 26, 24, 24));
	}
	$pdf->Ln(NEW_LINE_CONTENT_TITLE);
}

// COMMENTS
$pdf->AddPage();
$pdf->SetFont("arialn", "B", TAB_FONT_SIZE);
$pdf->SetX(TAB_NAME_X_POS);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, 'Comments');
$pdf->Ln(NEW_LINE_TITLE_CONTENT);

// Get comments allocated to this project
$model = new Model(Connector::DB_RETAILNET_RED);
$comments = $model->query("
	  SELECT SQL_CALC_FOUND_ROWS
          red_comment_id,
          red_comment_project_id,
          red_comment_file_id,
          red_comment_category_id,
          red_comment_owner_user_id,
          red_comment_comment,
          red_commentcategory_name,
          user_firstname,
          user_name,
          red_comment_comment,
          DATE_FORMAT(red_comments.date_created, '%d.%m.%Y') AS date_created
	  FROM red_comments
	 ")
->bind(Red_Comment::BIND_USER_ID)
->bind(Red_Comment::BIND_COMMENT_CATEGORY)
->filter('project_id', 'red_comment_project_id='.$project_id)
->order('red_comments.date_created', 'DESC')
->fetchAll();

if ($comments) {
	foreach ($comments as $row) {
		extract($row);
		$data[$red_comment_category_id]['caption'] = $red_commentcategory_name;
		$data[$red_comment_category_id]['comment'][$red_comment_id]['comment'] = "$red_comment_comment";
		$data[$red_comment_category_id]['comment'][$red_comment_id]['user'] = "$user_firstname $user_name";
		$data[$red_comment_category_id]['comment'][$red_comment_id]['date_created'] = $date_created;
		$file_ids = unserialize($red_comment_file_id);
		if (is_array($file_ids)) {
			foreach ($file_ids as $file_id) {
				$file = new Red_file('red');
				$file->read($file_id);
				if (isset($file->data['red_file_id'])) {
					$id = $file->data['red_file_id'];
					$data[$red_comment_category_id]['comment'][$red_comment_id]['files'][$id]['title']  = $file->data['red_file_title'];
					$data[$red_comment_category_id]['comment'][$red_comment_id]['files'][$id]['name']  = basename($file->data['red_file_path']);
				}
			}
		}
	}
}
if (isset($data)) {
	foreach ($data as $category) {
	  //Category name
	  $pdf->SetFont("arialn", "B", CATEGORY_FONT_SIZE);
	  $pdf->SetX(CATEGORY_X_POS);
	  $pdf->Cell(0, STANDARD_CELL_HEIGHT, $category['caption'],'B');

	  //Comments and Files
	  $pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
	  $pdf->SetXY(COMMENT_X_POS, $pdf->GetY() + DISTANCE_CATEGORY_COMMENT);
	  foreach ($category['comment'] as $comment) {
	    //Comment
	    $pdf->SetFont("arialn", "I", FILE_FONT_SIZE);
	    $pdf->MultiCell(0, STANDARD_CELL_HEIGHT-2, $comment['user'].', '.$comment['date_created'],  "", "L", true);
	    $pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
	    $pdf->MultiCell(0, STANDARD_CELL_HEIGHT, $comment['comment'],  "", "L", true);

	    //Files
	    if (is_array($comment['files'])) {
	      $pdf->SetX(FILE_X_POS);
	      $pdf->SetFont("arialn", "B", FILE_FONT_SIZE);
	      $pdf->MultiCell(0, 0, '', "", "L", true);
	      $pdf->MultiCell(0, STANDARD_CELL_HEIGHT/10, 'Files:', "", "L", true);
	      $pdf->SetFont("arialn", "", FILE_FONT_SIZE);
	      foreach ($comment['files'] as $file) {
	        $pdf->SetX(FILE_X_POS);
	        $pdf->MultiCell(0, STANDARD_CELL_HEIGHT/10, "- ".$file['title'] ." (". $file['name'].")", "", "L", true );
	      }
	    }
	    $pdf->SetXY(COMMENT_X_POS, $pdf->GetY()+DISTANCE_COMMENT_COMMENT);
	  }
	  $pdf->SetXY(CATEGORY_X_POS, $pdf->GetY() + DISTANCE_COMMENT_CATEGORY);
	}
}

$pdf->AddPage();

// Files
$pdf->SetFont("arialn", "B", TAB_FONT_SIZE);
$pdf->SetX(TAB_NAME_X_POS);
$pdf->Cell(0, STANDARD_CELL_HEIGHT, 'Files');
$pdf->Ln(NEW_LINE_TITLE_CONTENT);

// Get files allocated to this project
$model = new Model(Connector::DB_RETAILNET_RED);
$result = $model->query("
				  SELECT SQL_CALC_FOUND_ROWS
			          red_filecategory_name,
			          red_file_title,
			          red_file_path,
					  DATE_FORMAT(red_files.date_created, '%d.%m.%Y') AS date_created,
			          CONCAT (user_name, ', ', user_firstname) as name
				  FROM red_files
				 ")
->bind(Red_file::BIND_USER_ID)
->bind(Red_file::BIND_FILE_CATEGORY)
->filter('project_id', 'red_file_project_id='.$project_id)
->order('red_files.date_created', 'DESC')
->fetchAll();

//Preparing table data (sort by category and remove category
if (is_array($result)) {
	foreach ($result as $row) {
		$category_name = $row['red_filecategory_name'];
		unset($row['red_filecategory_name']);
		$row['red_file_title'] = get_substring(basename($row['red_file_title']), 78);
		$row['red_file_path'] = get_substring(basename($row['red_file_path']), 20);
		$table_data[$category_name][] = $row;
	}
}

//Write Tables
if (is_array($table_data)) {
	$category_names = array_keys($table_data);
	$i=0;
	foreach ($table_data as $category) {
	  
	  //Category name
	  $pdf->SetFont("arialn", "B", CATEGORY_FONT_SIZE);
	  $pdf->Cell(0, STANDARD_CELL_HEIGHT, $category_names[$i]);
	  $pdf->Ln();

	  $header = array('Title', 'Filename', 'Date', 'Name');

	  $pdf->ColoredTable($header, $category, array(80, 30, 30, 40));
	  $pdf->Ln();
	  $i++;
	}
}

$filename = "project_booklet_" . $project->data['red_project_title'] . ".pdf";

$pdf->Output($filename);
