<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once(PATH_LIBRARIES.'/tcpdf/tcpdf.php');

$user = User::instance();
if (!($user->permission(Red_project::PERMISSION_VIEW_PROJECTS) || $user->permission(Red_project::PERMISSION_EDIT_PROJECT_SHEET)))  {
  die('No permission');
}

define("COLUMN1_XPOS", 15);
define("COLUMN1_DATA_OFFSET", 25);
define("COLUMN1_DATA_WIDTH", 40);
define("COLUMN1_DATA2_OFFSET", 40);
define("COLUMN1_DATA2_WIDTH", 30);

define("COLUMN2_XPOS", 100);
define("COLUMN2_DATA_OFFSET", 40);
define("COLUMN2_DATA_WIDTH", 60);
define("COLUMN2_DATA_WIDTH_SMALL", 10);


define("COLUMN3_XPOS", 160);
define("COLUMN3_DATA_OFFSET", 30);
define("COLUMN3_DATA_WIDTH", 10);


define("TITLE_YPOS", 15);
define("GROUP1_YPOS", 25);
define("GROUP2_YPOS", 49);
define("GROUP3_YPOS", 112);
define("JUSTIFICATION_YPOS", 184);
define("JUSTIFICATION_COST_XPOS", 176);
define("SUPPLIER_YPOS", 250);

define("FULL_WIDTH", 185);

define("MAX_CHARS", 132);

define("NUMBER_OF_DESC_ROWS", 15);
define("NUMBER_OF_SUPPLIER_ROWS", 5);

define("STANDARD_FONT_SIZE", 7);


function getFormattedNumber($number) {
	return number_format($number, 2, '.' , "'");
}

function getBudgetStillAvailable($total, $committed) {
	return getFormattedNumber($total - $committed);
}

function limitChars($str, $length=MAX_CHARS) {
	return substr($str, 0, $length);
}

$sheet_id = $_REQUEST['sheet_id'];
$sheet = new Red_Project_Sheet('red');
$sheet->read($sheet_id);
$data = $sheet->data;

$project = new Red_project('red');

$project->read($data['red_projectsheet_project_id']);
$data['project_number'] = $project->data['red_project_projectnumber'];

$data['project_name'] = $project->data['red_project_title'];

// get currencies
$model = new Model(Connector::DB_CORE);
$result = $model->query("
				  SELECT
				   	currency_id,
					currency_symbol
				  FROM currencies
				 ")
->filter('currency_id', 'currency_id = '.$sheet->data['red_projectsheet_currency_id'])
->fetchAll();
$data['currency_symbol'] = _array::get_first_value(_array::extract($result));

// get business_unit_name
$result = $model->query("
				  SELECT
				   	business_unit_id,
					business_unit_name
				  FROM business_units
				 ")
->filter('business_unit_name', 'business_unit_id = '.$sheet->data['red_projectsheet_business_unit_id'])
->fetchAll();
$data['business_unit_name'] = _array::get_first_value(_array::extract($result));

// get cost_center name
$result = $model->query("
				  SELECT
				   	cost_center_id,
					cost_center_name
				  FROM cost_centers
				 ")
->filter('cost_center_name', 'cost_center_id = '.$sheet->data['red_projectsheet_costcenter_id'])
->fetchAll();
$data['cost_center_name'] = _array::get_first_value(_array::extract($result));

// get account_number_name name
$result = $model->query("
				  SELECT
				   	account_number_id,
					account_number_name
				  FROM account_numbers
				 ")
->filter('account_number_name', 'account_number_id = '.$sheet->data['red_projectsheet_accountnumber_id'])
->fetchAll();

$data['account_number_name'] = _array::get_first_value(_array::extract($result));

// get address_company name
$model_red = new Model(Connector::DB_RETAILNET_RED);
$result = $model_red->query("
				  SELECT DISTINCT
					red_project_partner_id,
					red_project_partner_user_id,
					address_company,
					user_firstname,
					user_name,
					user_phone,
					user_email
				  FROM red_project_partners
				 ")
->bind(Red_project::BIND_ADDRESSES)
->bind(Red_project::BIND_USER_ID)
->filter('red_project_partner_project_id', 'red_project_partner_id = '.$sheet->data['red_projectsheet_mainsupplier_partner_id'])
->order('address_company')
->fetchAll();

$data['supplier_company'] = _array::get_first_value(_array::extract_by_key($result, 'address_company'));
$data['supplier_userfirstname'] = _array::get_first_value(_array::extract_by_key($result, 'user_firstname'));
$data['supplier_username'] = _array::get_first_value(_array::extract_by_key($result, 'user_name'));
$data['supplier_phone'] = _array::get_first_value(_array::extract_by_key($result, 'user_phone'));
$data['supplier_email'] = _array::get_first_value(_array::extract_by_key($result, 'user_email'));

// Create and setup PDF document
$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 8);
$pdf->setPrintHeader(false);

$pdf->Open();
$pdf->SetTitle($row['project_sheet_name']);
$pdf->SetAuthor("Retailnet");
$pdf->SetDisplayMode(150);

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->AddPage();
$pdf->SetLeftMargin(0);

//Add Retailnet Logo
$pdf->Image($_SERVER['DOCUMENT_ROOT'].'/public/images/logo.jpg',16,8,33);
$pdf->SetFont('arialn','B',12);
$pdf->SetY(10);
$pdf->Cell(0,12, "Project Sheet", 0, 0, 'R');


//cost_center_name
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN1_XPOS, GROUP1_YPOS);
$pdf->Cell(0, 6, "Cost Center");
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA_OFFSET, GROUP1_YPOS);
$pdf->Cell(COLUMN1_DATA_WIDTH, 6, limitChars($data['cost_center_name'], 34), 1, 0, "L");

//account_number_name
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN1_XPOS, GROUP1_YPOS + 6 + 6);
$pdf->Cell(0, 6, "Account Number");
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA_OFFSET, GROUP1_YPOS + 6 + 6);
$pdf->Cell(COLUMN1_DATA_WIDTH, 6, limitChars($data['account_number_name'], 34), 1, 0, "L");

//red_projectsheet_openingdate
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN1_XPOS, GROUP2_YPOS);
$pdf->Cell(0, 6, "Opening Date");
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA_OFFSET, GROUP2_YPOS);
$pdf->Cell(COLUMN1_DATA_WIDTH, 6, $data['red_projectsheet_openingdate'], 1, 0, "L");


//project_number
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS);
$pdf->Cell(0, 6, "Project Number");
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS);
$pdf->Cell(COLUMN2_DATA_WIDTH, 6, $data['project_number'], 1, 0, "L");


//project_name
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS + 6 + 6);
$pdf->Cell(0, 6, "Project Name");
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS + 6 + 6);
$pdf->Cell(COLUMN2_DATA_WIDTH, 6, limitChars($data['project_name'], 50), 1, 0, "L");


//red_projectsheet_plannedamount_current_year
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN2_XPOS, GROUP2_YPOS);
$pdf->Cell(0, 6, "Planned amount cur. year");
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP2_YPOS);
$pdf->Cell(COLUMN2_DATA_WIDTH, 6, $data['red_projectsheet_plannedamount_current_year'], 1, 0, "L");


//currency_symbol
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN2_XPOS, GROUP2_YPOS + 6 + 6);
$pdf->Cell(0, 6, "Local currency");
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP2_YPOS + 6 + 6);
$pdf->Cell(COLUMN2_DATA_WIDTH, 6, $data['currency_symbol'], 1, 0, "L");

//red_projectsheet_share_swatch
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN2_XPOS, GROUP2_YPOS + 12 + 12);
$pdf->Cell(0, 6, "Share swatch in %");
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP2_YPOS + 12 + 12);
$pdf->Cell(COLUMN2_DATA_WIDTH_SMALL, 6, $data['red_projectsheet_share_swatch'], 1, 0, "L");


//red_projectsheet_share_other
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN3_XPOS, GROUP2_YPOS + 12 + 12);
$pdf->Cell(0, 6, $translate->share_swatch_in_procent);
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN3_XPOS + COLUMN3_DATA_OFFSET, GROUP2_YPOS + 12 + 12);
$pdf->Cell(COLUMN3_DATA_WIDTH, 6, $data['red_projectsheet_share_others'], 1, 0, "L");



// BUDGET SITUATION
$pdf->SetFont("arialn", "B", 10);
$pdf->SetXY(COLUMN1_XPOS, GROUP3_YPOS - 10);
$pdf->Cell(0, 4, "BUDGET SITUATION (in ".$data['currency_symbol'].")");

// red_projectsheet_totalbudget
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN1_XPOS, GROUP3_YPOS);
$pdf->Cell(0, 6, "Total Budget");
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, GROUP3_YPOS);
$pdf->Cell(COLUMN1_DATA2_WIDTH, 6, getFormattedNumber($data['red_projectsheet_totalbudget']), 1, 0, "R");

// red_projectsheet_alreadycommitted
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN1_XPOS, GROUP3_YPOS + 12 );
$pdf->Cell(0, 6, "Alread committed");
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, GROUP3_YPOS  + 6 + 6 );
$pdf->Cell(COLUMN1_DATA2_WIDTH, 6, getFormattedNumber($data['red_projectsheet_alreadycommitted']), 1, 0, "R");

// red_projectsheet_alreadyspent
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN1_XPOS, GROUP3_YPOS + 24 );
$pdf->Cell(0, 6, "Already spent");
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, GROUP3_YPOS + 12 + 12 );
$pdf->Cell(COLUMN1_DATA2_WIDTH, 6, getFormattedNumber($data['red_projectsheet_alreadyspent']), 1, 0, "R");

// budget still available
$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN1_XPOS, GROUP3_YPOS + 36 );
$pdf->Cell(0, 6, "Budget still available");
$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, GROUP3_YPOS + 18 + 18);
$pdf->Cell(COLUMN1_DATA2_WIDTH, 6, getBudgetStillAvailable($data['red_projectsheet_totalbudget'], $data['red_projectsheet_alreadycommitted']), 1, 0, "R");



// APPROVAL
$pdf->SetFont("arialn", "B", 10);
$pdf->SetXY(COLUMN2_XPOS, GROUP3_YPOS - 10);
$pdf->Cell(0, 4, "APPROVAL");


// Column headers for approval table
$width = COLUMN2_DATA_OFFSET + COLUMN2_DATA_WIDTH;
$width1 = floor($width * 0.41);
$width2 = floor($width * 0.17);
$width3 = floor($width * 0.25);
$width4 = $width - $width1 - $width2 - $width3;

$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN2_XPOS, GROUP3_YPOS);
$pdf->Cell($width1, 7, "Name", 1, 0, "C");
$pdf->Cell($width2, 7, "", 1, 0, "C");
$pdf->Cell($width3, 7, "Signature", 1, 0, "C");
$pdf->Cell($width4, 7, "Date", 1, 0, "C");
$pdf->SetXY(COLUMN2_XPOS + $width1, GROUP3_YPOS + 1);
$pdf->SetXY(COLUMN2_XPOS + $width1, GROUP3_YPOS + 1);
$pdf->MultiCell($width2, 2.5, "Deadline for\napproval", 0, "C");

$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetXY(COLUMN2_XPOS, GROUP3_YPOS + 7);
$pdf->Cell($width1, 7, limitChars($data['red_projectsheet_approvalname01'], 34), 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS, GROUP3_YPOS + 14);
$pdf->Cell($width1, 7, limitChars($data['red_projectsheet_approvalname02'], 34), 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS, GROUP3_YPOS + 21);
$pdf->Cell($width1, 7, limitChars($data['red_projectsheet_approvalname03'], 34), 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS, GROUP3_YPOS + 28);
$pdf->Cell($width1, 7, limitChars($data['red_projectsheet_approvalname04'], 34), 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS, GROUP3_YPOS + 35);
$pdf->Cell($width1, 7, limitChars($data['red_projectsheet_approvalname05'], 34), 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS, GROUP3_YPOS + 42);
$pdf->Cell($width1, 7, limitChars($data['red_projectsheet_approvalname06'], 34), 1, 0, "L");

$pdf->SetXY(COLUMN2_XPOS + $width1, GROUP3_YPOS + 7);
$pdf->Cell($width2, 7, $data['red_projectsheet_approval_deadline01'], 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS + $width1, GROUP3_YPOS + 14);
$pdf->Cell($width2, 7, $data['red_projectsheet_approval_deadline02'], 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS + $width1, GROUP3_YPOS + 21);
$pdf->Cell($width2, 7, $data['red_projectsheet_approval_deadline03'], 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS + $width1, GROUP3_YPOS + 28);
$pdf->Cell($width2, 7, $data['red_projectsheet_approval_deadline04'], 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS + $width1, GROUP3_YPOS + 35);
$pdf->Cell($width2, 7, $data['red_projectsheet_approval_deadline05'], 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS + $width1, GROUP3_YPOS + 42);
$pdf->Cell($width2, 7, $data['red_projectsheet_approval_deadline06'], 1, 0, "L");

$pdf->SetXY(COLUMN2_XPOS + $width1 + $width2, GROUP3_YPOS + 7);
$pdf->Cell($width3, 7, '', 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS + $width1 + $width2, GROUP3_YPOS + 14);
$pdf->Cell($width3, 7, '', 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS + $width1 + $width2, GROUP3_YPOS + 21);
$pdf->Cell($width3, 7, '', 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS + $width1 + $width2, GROUP3_YPOS + 28);
$pdf->Cell($width3, 7, '', 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS + $width1 + $width2, GROUP3_YPOS + 35);
$pdf->Cell($width3, 7, '', 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS + $width1 + $width2, GROUP3_YPOS + 42);
$pdf->Cell($width3, 7, '', 1, 0, "L");

$pdf->SetXY(COLUMN2_XPOS + $width1 + $width2 + $width3, GROUP3_YPOS + 7);
$pdf->Cell($width4, 7, '', 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS + $width1 + $width2 + $width3, GROUP3_YPOS + 14);
$pdf->Cell($width4, 7, '', 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS + $width1 + $width2 + $width3, GROUP3_YPOS + 21);
$pdf->Cell($width4, 7, '', 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS + $width1 + $width2 + $width3, GROUP3_YPOS + 28);
$pdf->Cell($width4, 7, '', 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS + $width1 + $width2 + $width3, GROUP3_YPOS + 35);
$pdf->Cell($width4, 7, '', 1, 0, "L");
$pdf->SetXY(COLUMN2_XPOS + $width1 + $width2 + $width3, GROUP3_YPOS + 42);
$pdf->Cell($width4, 7, '', 1, 0, "L");



// Table for description and financial justification
$text_width = COLUMN2_XPOS - COLUMN1_XPOS + COLUMN2_DATA_OFFSET;
$full_width = $text_width + COLUMN2_DATA2_WIDTH;

$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetTextColor(255, 0, 0);
$pdf->SetXY(COLUMN1_XPOS, JUSTIFICATION_YPOS - 6);
$pdf->Cell(COLUMN1_DATA2_OFFSET, 3, "DESCRIPTION", 0, 2);
$pdf->Cell(COLUMN1_DATA2_OFFSET, 3, "FINANCIAL JUSTIFICATION");

$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetTextColor(0, 0, 0);
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, JUSTIFICATION_YPOS - 6);
$pdf->Cell(80, 3, "(Goals, Actions, Timing, Quantities)", 0, 2);
$pdf->Cell(80, 3, "(Costs planning by items, return on investment, ...)");

$pdf->SetXY(COLUMN1_XPOS, JUSTIFICATION_YPOS+3);
$pdf->Cell(0, 6, "Business Unit");
$pdf->SetXY(COLUMN1_XPOS + COLUMN1_DATA2_OFFSET, JUSTIFICATION_YPOS+3);
$pdf->Cell(80, 6, $data['business_unit_name'], 0, 0, "L");

$pdf->SetTextColor(0, 0, 0);
$pdf->SetXY(COLUMN1_XPOS, JUSTIFICATION_YPOS + 6);

$ypos = JUSTIFICATION_YPOS + 6;
$lineCounter = 1;

foreach (explode("\n", $data['red_projectsheet_financial_justification']) as $line) {
	if ($lineCounter > NUMBER_OF_DESC_ROWS) break;
	$pdf->SetXY(COLUMN1_XPOS, $ypos + (3.5 * $lineCounter));
	$pdf->Cell(FULL_WIDTH, 3.5, limitChars($line), 1, 0, "L");
	$lineCounter++;
}



// Table for supplier information
//$pdf->SetLineWidth(0.5);
//$pdf->Rect(COLUMN1_XPOS, SUPPLIER_YPOS, FULL_WIDTH, 6 * 4.5);
//$pdf->SetLineWidth(0.2);

$pdf->SetFont("arialn", "B", STANDARD_FONT_SIZE);
$pdf->SetTextColor(255, 0, 0);
$pdf->SetXY(COLUMN1_XPOS, SUPPLIER_YPOS);
$pdf->Cell(COLUMN1_DATA2_OFFSET, 3, "MAIN SUPPLIER", 0, 2);

$pdf->SetFont("arialn", "", STANDARD_FONT_SIZE);
$pdf->SetTextColor(0, 0, 0);

$ypos = SUPPLIER_YPOS + 6;

$pdf->SetXY(COLUMN1_XPOS, $ypos);
$pdf->Cell(FULL_WIDTH, 3.5, $data['supplier_company'], 1, 0, "L");
$pdf->SetXY(COLUMN1_XPOS, $ypos + 3.5);
$pdf->Cell(FULL_WIDTH, 3.5, $data['supplier_userfirstname'].' '.$data['supplier_username'], 1, 0, "L");
$pdf->SetXY(COLUMN1_XPOS, $ypos + 7);
$pdf->Cell(FULL_WIDTH, 3.5, $data['supplier_username'], 1, 0, "L");
$pdf->SetXY(COLUMN1_XPOS, $ypos + 10.5);
$pdf->Cell(FULL_WIDTH, 3.5, $data['supplier_phone'], 1, 0, "L");
$pdf->SetXY(COLUMN1_XPOS, $ypos + 14);
$pdf->Cell(FULL_WIDTH, 3.5, $data['supplier_email'], 1, 0, "L");

$filename = "project_sheet_" . $data['project_number'] . ".pdf"; 

$pdf->Output($filename);