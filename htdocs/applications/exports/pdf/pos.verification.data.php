<?php 
/*
	Creat a COMPANY Overview

    Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2011-04-04
    Modified by:    
    Date modified:  
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*/
		

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	require_once PATH_LIBRARIES."tcpdf/tcpdf.php";

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$id = $_REQUEST['id'];
	$period = $_REQUEST['period'];
	
	$periodDate = date('Y-m-d', $period);
	
	$model = new Model($application);
	
	$company = new Company();
	$company->read($id);
	
	// permission
	$permission_edit = user::permission(Pos_Verification::PERMISSION_EDIT_ALL);
	$permission_edit_limited = user::permission(Pos_Verification::PERMISSION_EDIT_LIMITED);


	// pos verification data
	$posVerification = new Pos_Verification($application);
	$data = $posVerification->read_periode($id, $periodDate);
	
	// verifictiona data
	if ($data['posverification_id']) {
		
		$result = $model->query("
			SELECT 
				posverification_data_distribution_channel_id,
				posverification_data_actual_number
			FROM posverification_data
			WHERE posverification_data_posverification_id = ".$data['posverification_id']."		
		")->fetchAll();
		
		if ($result) {
			$actualNumber = _array::extract($result);
		}
	}
	
	// count all distribution channels for curren company
	$result = $model->query("
		SELECT 
			posaddress_distribution_channel,
			COUNT(posaddress_id) as total
		FROM db_retailnet.posaddresses
		WHERE 
			posaddress_distribution_channel > 0 
			AND posaddress_client_id = $id
			AND (
				posaddress_store_closingdate IS NULL 
				OR posaddress_store_closingdate = '0000-00-00'
				OR posaddress_store_closingdate = ''
			)
		GROUP BY posaddress_distribution_channel	
	")->fetchAll();
	
	$inPosIndex = _array::extract($result);
	
	
	// count all pos locations for current company
	$result = $model->query("
		SELECT 
			COUNT(posaddress_id) as total
		FROM db_retailnet.posaddresses
		WHERE 
			posaddress_client_id = $id
			AND (
				posaddress_store_closingdate IS NULL 
				OR posaddress_store_closingdate = '0000-00-00'
				OR posaddress_store_closingdate = ''
			)
	")->fetch();
	
	// grand total pos locations
	$data['pos-total'] = $result['total'];
	
	// total without channels
	$totalInPosIndex = is_array($inPosIndex) ? array_sum($inPosIndex) : 0;
	$posWidthoutChannels = $result['total'] - $totalInPosIndex;
	
	// submitted from
	if ($data['posverification_confirm_user_id']) {
		
		$User = new User($data['posverification_confirm_user_id']);
		
		$date = ($data['date_modiefied']) ? $data['date_modiefied'] : $data['posverification_confirm_date'];
				
		$d = new DateTime($date);
		$confirmedDate = $d->format("d.m.Y H:i:s");
				
		$submittedBy = "Submitted by $User->firstname $User->name, $confirmedDate";
	}

	
	// distribution channels	
	$result = $model->query("
		SELECT DISTINCT
			mps_distchannel_id,
			mps_distchannel_group_name,
			mps_distchannel_group_allow_for_agents,
			mps_distchannel_code,
			mps_distchannel_name
		FROM db_retailnet.mps_distchannels
		INNER JOIN db_retailnet.mps_distchannel_groups ON mps_distchannels.mps_distchannel_group_id = mps_distchannel_groups.mps_distchannel_group_id
		WHERE mps_distchannel_active = 1 
		ORDER BY mps_distchannel_group_name, mps_distchannel_name
	")->fetchAll();
	
	if ($result) {
		
		$datagrid = array();
		
		foreach ($result as $row) {
			
			if ($client && !$row['mps_distchannel_group_allow_for_agents']) {
				
				continue;
				
			} else {
				
				$key = strtolower(str_replace(' ', '', $row['mps_distchannel_group_name']));
				
				$i = $row['mps_distchannel_id'];
				$delta = $actualNumber[$i] - $inPosIndex[$i]; 
				
				$datagrid[$key]['caption'] = $row['mps_distchannel_group_name'];
				
				$datagrid[$key]['data'][$row['mps_distchannel_id']] = array(
					'mps_distchannel_name' => $row['mps_distchannel_name'],
					'mps_distchannel_code' => $row['mps_distchannel_code'],
					'in_pos_index' => $inPosIndex[$i],
					'posverification_data_actual_number' => $actualNumber[$i],
					'delta' => $delta
				);
				
				$totalColumn['in_pos_index'][$key] = $totalColumn['in_pos_index'][$key] + $inPosIndex[$i];
				$totalColumn['posverification_data_actual_number'][$key] = $totalColumn['posverification_data_actual_number'][$key] + $actualNumber[$i];
				$totalColumn['delta'][$key] = $totalColumn['delta'][$key] + $delta;
			}
		}
	}
	
	
	if ($datagrid) {
		
		
			
		$country = new Country();
		$country->read($company->country);
		
		// periode
		$d = new DateTime(date('Y-m-d', $period));
		$d->modify('first day of this month');
		$currentPeriod = $d->format('F Y');
		
		// header and footer
		class MYPDF extends TCPDF {

			function Header() {
				
				$this->Image( $_SERVER['DOCUMENT_ROOT'].'/pictures/brand_logo.jpg',12,8,33);
				$this->SetFont('arialn','B',12);
				$this->Cell(80);
				$this->Cell(0,34,'POS Verification',0,0,'R');
			}
		
			//Page footer
			function Footer() {
				$this->SetY(-15);
				$this->SetFont('arialn','I',8);
				$this->Cell(0,10, date("d.m.y") . ' / Page '.$this->PageNo(),0,0,'R');
			}
		}
		
		// instanciation of inherited class
		$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
		$pdf->SetMargins(10, PDF_MARGIN_TOP, 10);
		
		$pdf->Open();
		
		// fill collor
		$pdf->SetFillColor(232, 232, 232);
		
		// set border width
		$pdf->SetLineWidth(0.1);
		
		$pdf->AddFont('arialn');
		$pdf->AddFont('arialn', 'B');
		
		$pdf->AddPage();
		
		// header: POS Verification [Monat] [Jahr]: [Client] [Country]
		$pdf->SetFont('arialn','B',16);
		$pdf->SetTextColor(0,0,0);
		$pdf->Cell(0,15, "POS Verification $currentPeriod: $company->company $country->name", 0, 1, 'L', 0);
		
		$linestyle = array(
			'width' => 0.1, 
			'cap' => 
			'butt', 
			'join' => 'miter', 
			'dash' => '', 
			'phase' => 0, 
			'color' => array(0, 0, 0)
		);
		
		foreach ($datagrid as $key => $row) {
			
			// distribution channel
			$pdf->SetFont('arialn','B',12);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(0,10, $row['caption'], 0, 1, 'L', 0);
			
			// table header font
			$pdf->SetFont('arialn','',10);
			
			// table columns
			$pdf->Cell(88, 8, $translate->mps_distchannel_name, 1, 0, 'L', 1);
			$pdf->Cell(25, 8, $translate->mps_distchannel_code, 1, 0, 'L', 1);
			$pdf->Cell(25, 8, $translate->in_pos_index, 1, 0, 'R', 1);
			$pdf->Cell(25, 8, $translate->posverification_data_actual_number, 1, 0, 'R', 1);
			$pdf->Cell(25, 8, $translate->delta, 1, 1, 'R', 1);
			
			// table body font
			$pdf->SetFont('arialn','',9);
			
			foreach ($row['data'] as $channel => $value) {
				
				$pdf->SetTextColor(0,0,0);
				
				$pdf->Cell(88, 6, $value['mps_distchannel_name'], 1, 0, 'L', 0);
				$pdf->Cell(25, 6, $value['mps_distchannel_code'], 1, 0, 'L', 0);
				$pdf->Cell(25, 6, $value['in_pos_index'], 1, 0, 'R', 0);
				$pdf->Cell(25, 6, $value['posverification_data_actual_number'], 1, 0, 'R', 0);
				
				if ($value['delta'] <> 0) {
					$pdf->SetTextColor(255,0,0);
				}
				
				$pdf->Cell(25, 6, $value['delta'], 1, 1, 'R', 0);
			}
			
			// total Columns
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(90, 8, "Total ".$row['caption'], 0, 0, 'L', 0);
			$pdf->Cell(23, 8, '', 0, 0, 'L', 0);
			$pdf->Cell(25, 8, $totalColumn['in_pos_index'][$key], 0, 0, 'R', 0);
			$pdf->Cell(25, 8, $totalColumn['posverification_data_actual_number'][$key], 0, 0, 'R', 0);
			
			if ($totalColumn['delta'][$key] <> 0) {
				$pdf->SetTextColor(255,0,0);
			}
			
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(25, 8, $totalColumn['delta'][$key], 0, 1, 'R', 0);
			$pdf->Ln(1);
			
			// grand totals
			$grandTotal['in_pos_index'] = $grandTotal['in_pos_index'] + $totalColumn['in_pos_index'][$key];
			$grandTotal['actual_number'] = $grandTotal['actual_number'] + $totalColumn['posverification_data_actual_number'][$key];
			$grandTotal['delta'] = $grandTotal['delta'] + $totalColumn['delta'][$key];
		}
		
		$pdf->SetTextColor(0,0,0);
		
		$pdf->Ln(4);
		
		// draw line
		$y = $pdf->getY();
		$pdf->Line(10, $y, 198, $y, $linestyle);
		
		// grand total without distribution channels
		$pdf->Cell(90, 8, "Total Locations without distribution channels", 0, 0, 'L', 0);
		$pdf->Cell(23, 8, '', 0, 0, 'L', 0);
		$pdf->Cell(25, 8, $posWidthoutChannels, 0, 1, 'R', 0);
		
		// grand total without distribution channels
		$pdf->Cell(90, 8, "Grand Total of all POS Locations", 0, 0, 'L', 0);
		$pdf->Cell(23, 8, '', 0, 0, 'L', 0);
		$pdf->Cell(25, 8, $grandTotal['in_pos_index'], 0, 0, 'R', 0);
		$pdf->Cell(25, 8, $grandTotal['actual_number'], 0, 0, 'R', 0);
		
		if ($grandTotal['delta'] <> 0) {
			$pdf->SetTextColor(255,0,0);
		}
		
		$pdf->Cell(25, 8, $grandTotal['delta'], 0, 1, 'R', 0);

		$pdf->SetTextColor(0,0,0);
		
		// draw line
		$y = $pdf->getY();
		$pdf->Line(10, $y, 198, $y, $linestyle);
		
		$pdf->Ln(3);
		
		// submittet by
		if ($submittedBy) {
			$pdf->SetFont('arialn','N',10);
			$pdf->Cell(0, 0, $submittedBy, 0, 0, 'L', 0);
		}
		
		$pdf->Output();
	}
	else {
		message::invalid_request();
		url::redirect("/$application/$controller.$archived");
	}
