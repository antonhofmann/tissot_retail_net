<?php 
/*
	Creat a POS Overview

    Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2011-04-04
    Modified by:    
    Date modified:  
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*/
		

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	require_once PATH_LIBRARIES."tcpdf/tcpdf.php";

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	// planning model
	$model = new Model($application);
	
	// pos location
	$pos = new Pos();
	$pos->read($id);

	
	if ($pos->id) {
		
		// pos owner
		$company = new Company();
		$company->read($pos->franchisee_id);
		
		// pos owner country
		$company_country = new Country();
		$company_country->read($company->country);
		
		// pos owner place
		$company_place = new Place();
		$company_place->read($company->place_id);
		
		// pos country
		$pos_country = new Country();
		$pos_country->read($pos->country);
		
		// pos place
		$pos_place = new Place();
		$pos_place->read($pos->place_id);
		
		// project cost type
		$project_cost_type = new Project_Cost_Type();
		$project_cost_type->read($pos->ownertype);
		
		// pos type
		$pos_type = new Pos_Type();
		$pos_type->read($pos->store_postype);
		
		// pos subclass
		$pos_subclass = new Pos_Subclass();
		$pos_subclass->read($pos->store_subclass);
		
		// product line
		$product_line = new Product_Line();
		$product_line->read($pos->store_furniture);
		
		// product line subclasses
		$product_line_subclass = new Product_Line_Subclass();
		$product_line_subclass->read($pos->store_furniture_subclass);
		
		// currency
		$currency = new Currency();
		$currency->read($pos->takeover_currency);
		
		// agreement type
		$agreement_type = new Agreement_Type();
		$agreement_type->read($pos->fagagreement_type);
		
		// staff
		$staff = new Staff($application);
		
		// distribution channel
		$distribution_channel = new DistChannel($application);
		
		// turnover class
		$turnover_class = new TurnoverClass($application);
		
		// pos materials
		$materials = $model->query("
			SELECT
				mps_pos_material_id,
				mps_material_purpose_name,
				mps_material_category_name,
				mps_material_code,
				mps_material_name,
				mps_pos_material_quantity_in_use,
				mps_pos_material_quantity_on_stock,
				mps_pos_material_installation_year,
				mps_pos_material_deinstallation_year
			FROM mps_pos_materials 
		")
		->bind(Pos_Material::DB_BIND_MATERIALS)
		->bind(Material::DB_BIND_CATEGORIES)
		->bind(Material::DB_BIND_PURPOSES)
		->filter('pos', "mps_pos_material_posaddress = $id")
		->order('mps_material_code')
		->order('mps_material_name')
		->order('mps_material_purpose_name')
		->order('mps_material_category_name')
		->fetchAll();
		
		
		// pos furnitures
		$furniture =  $model->query("
			SELECT 
				mps_pos_furniture_id, 
				product_line_name,
				item_code,
				item_name,
				mps_pos_furniture_quantity_in_use, 
				mps_pos_furniture_quantity_on_stock, 
				mps_pos_furniture_installation_year, 
				mps_pos_furniture_deinstallation_year
			FROM mps_pos_furniture
		")
		->bind(Pos_Furniture::DB_BIND_ITEMS)
		->bind(Pos_Furniture::DB_BIND_PRODUCT_LINES)
		->filter('pos', "mps_pos_furniture_posaddress = $id")
		->order('product_line_name')
		->order('item_code')
		->order('item_name')
		->fetchAll();
		
		
		
		$project_orders = $model->query("
			SELECT 
				posorder_order, 
				posorder_project_kind 
			FROM db_retailnet.posorders 
				LEFT JOIN db_retailnet.projects ON project_order = posorder_order 
				LEFT JOIN db_retailnet.orders ON order_id = posorder_order 
			WHERE
				posorder_type = 1 AND posorder_project_kind IN (1,2,3) 
				AND project_actual_opening_date <> '0000-00-00' AND project_actual_opening_date IS NOT NULL 
				AND (project_closing_date <> '0000-00-00' OR project_closing_date IS NULL) 
				AND order_actual_order_state_code != '900' 
				AND posorder_posaddress = $id
			ORDER BY
				posorder_order DESC
		")->fetchAll();
		
		
		// catalog order
		$catalog_order = $model->query("
			SELECT 
				posorder_posaddress, 
				project_opening_date 
			FROM db_retailnet.posorders 
				LEFT JOIN db_retailnet.projects ON project_order = posorder_order 
				LEFT JOIN db_retailnet.orders ON order_id = posorder_order 
			WHERE 
				posorder_type = 1 AND posorder_project_kind IN (1,2,3) 
				AND project_actual_opening_date <> '0000-00-00' AND project_actual_opening_date IS NOT NULL 
				AND (project_closing_date <> '0000-00-00' OR project_closing_date IS NULL) 
				AND order_actual_order_state_code != '900' 
				AND posorder_posaddress = $id
			ORDER BY posorder_order DESC LIMIT 0,1
		")->fetch();


		class MYPDF extends TCPDF {
				
			public function Header() {
				$this->Image($_SERVER['DOCUMENT_ROOT'].'/public/images/logo.jpg' ,10,8,33);
				$this->SetFont('arialn','B',12);
				$this->Cell(80);
				$this->Cell(0,34, 'POS Overview', 0,0,'R');
				$this->Ln(20);
			}
		
			public function Footer() {
				$this->SetY(-15);
				$this->SetFont('arialn','I',8);
				$this->Cell(0,10, date("d.m.y") . ' / Page '.$this->PageNo(),0,0,'R');
			}
		}
	
		//Instanciation of inherited class
		$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
		$pdf->SetMargins(10, PDF_MARGIN_TOP, 11);
	
		$pdf->Open();
		
	
		$pdf->SetFillColor(220, 220, 220);
	
		$pdf->AddFont('arialn');
		$pdf->AddFont('arialn', 'B');
	
		$pdf->AddPage();
		
		// caption
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190, 5, "Owner Company" ,0, 0, 'L', 0);
		$pdf->Ln();
		$pdf->Ln();
		
		
		// company name
		$company_header = ($company->company2) ? $company->company.", ".$company->company2 : $company->company;
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190,7, $translate->address_company.": " . $company_header, 1, 0, 'L', 1);
		$pdf->Ln();
		
		
		// company address
		$owner_address[] = $company->address;
		
		if ($company->address2) {
			$owner_address[] = $company->address2;
		}
		
		if ($company->zip) {
			$owner_address[] = $company->zip;
		}
		
		$owner_address[] = $company_place->name;
		$owner_address[] = $company_country->name;
		
		$pdf->SetFont('arialn','',10);
		$pdf->Cell(190,7, $translate->address.": " . join(', ', $owner_address),1, 0, 'L', 0);
		$pdf->Ln();
		
		
		// company phone
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->address_phone,1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $company->phone, 1, 0, 'L', 0);
		
		// company mobile_phone
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->address_mobile_phone, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $company->mobile_phone, 1, 0, 'L', 0);
		$pdf->Ln();
		
		// company email
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->address_email, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $company->email, 1, 0, 'L', 0);
		
		// company website
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->address_website, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $company->website, 1, 0, 'L', 0);
		$pdf->Ln();
		
		// company contact name
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->address_contact_name, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $company->contact_name, 1, 0, 'L', 0);
		
		//  contact email
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->address_contact_email, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $company->contact_email, 1, 0, 'L', 0);
		$pdf->Ln();

		
		// separator
		$pdf->Ln();
		
		
		// caption
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190, 5, "POS Location" ,0, 0, 'L', 0);
		$pdf->Ln();
		$pdf->Ln();
		
	
		// pos name
		$pos_header = ($pos->name2) ? $pos->name.", ".$pos->name2 : $pos->name;
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190,7, $translate->posaddress_name.": " . $pos_header, 1, 0, 'L', 1);
		$pdf->Ln();
		
		// pos address
		$pos_address[] = $pos->street.' '.$pos->street_number;
		
		if ($pos->address2) {
			$pos_address[] = $pos->address2;
		}
		
		if ($pos->zip) {
			$pos_address[] = $pos->zip;
		}
		
		$pos_address[] = $pos_place->name;
		$pos_address[] = $pos_country->name;
		
		$pdf->SetFont('arialn','',10);
		$pdf->Cell(190,7, $translate->posaddress_address.": " . join(', ', $pos_address),1, 0, 'L', 0);
		$pdf->Ln();		
		
		// pos phone
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->posaddress_phone, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $pos->phone, 1, 0, 'L', 0);
	
		// pos mobile_phone
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->posaddress_mobile_phone, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $pos->mobile_phone, 1, 0, 'L', 0);
		$pdf->Ln();
	
		// pos email
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->posaddress_email, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5,$pos->email, 1, 0, 'L', 0);
		
		// pos website
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->posaddress_website, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $pos->website, 1, 0, 'L', 0);
		$pdf->Ln();
		
		
		// contact name
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->posaddress_contact_name, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $pos->contact_name, 1, 0, 'L', 0);
		
		// contact email
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->posaddress_contact_email, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $pos->contact_email, 1, 0, 'L', 0);
		$pdf->Ln();
		
		// pos arreas
		$result = $model->query("
			SELECT * 
			FROM db_retailnet.posareas 
			LEFT JOIN db_retailnet.posareatypes on posareatype_id = posarea_area 
			WHERE posarea_posaddress = $id
		")->fetchAll();
		
		if ($result) {
			
			foreach ($result as $row) {
				$posareas[] = $row['posareatype_name'];
			}
			
			$pos_areas = join(', ', $posareas);
			
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(35, 5, $translate->areas, 1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Cell(155, 5, $pos_areas, 1, 0, 'L', 0);
			$pdf->Ln();
		}
		
		// separator
		$pdf->Ln();
		
		// caption
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190, 5, $translate->pos_data, 0, 0, 'L', 0);
		$pdf->Ln();
		$pdf->Ln();
	
		// pos opening date
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->posaddress_store_openingdate, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $pos->store_openingdate, 1, 0, 'L', 0);
	
		// pos closing date
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->posaddress_store_closingdate, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $pos->store_closingdate, 1, 0, 'L', 0);
		$pdf->Ln();
	
		// pos legal type
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->project_costtype_text, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $project_cost_type->text, 1, 0, 'L', 0);
	
		// pos Raportin No.
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, 'ER No.', 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $pos->eprepnr, 1, 0, 'L', 0);
		$pdf->Ln();
	
		// pos TYpe
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->postype_name, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $pos_type->name, 1, 0, 'L', 0);
	
		// pos type subclasses
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->possubclass_name, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $pos_subclass->name, 1, 0, 'L', 0);
		$pdf->Ln();
	
		// furniture
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->product_line_name, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $product_line->name, 1, 0, 'L', 0);
	
		// furniture subclass
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(35, 5, $translate->productline_subclass_name, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(60, 5, $product_line_subclass->name, 1, 0, 'L', 0);
		$pdf->Ln();
		
		
		// separator
		$pdf->Ln();

		
		// caption
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190, 5, $translate->sales, 0, 0, 'L', 0);
		$pdf->Ln();
		$pdf->Ln();
		
		// Sales Representative
		$staff->read($pos->sales_representative);
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(70, 5, $translate->posaddress_sales_representative, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(120, 5, $staff->firstname.''.$staff->name, 1, 0, 'L', 0);
		$pdf->Ln();
		
		// Decoration Person
		$staff->read($pos->decoration_person);
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(70, 5, $translate->posaddress_decoration_person, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(120, 5, $staff->firstname.''.$staff->name, 1, 0, 'L', 0);
		$pdf->Ln();
		
		// Distribution Channel
		$distribution_channel->read($pos->distribution_channel);
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(70, 5, $translate->posaddress_distribution_channel, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(120, 5, $distribution_channel->group.', '.$distribution_channel->code, 1, 0, 'L', 0);
		$pdf->Ln();
		
		// Turnovertype Watches
		$turnover_class->read($pos->turnoverclass_bijoux);
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(70, 5, "Turnovertype Watches",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(25, 5, $turnover_class->code, 1, 0, 'L', 0);
		
		// Turnovertype Watch Straps
		$turnover_class->read($pos->turnoverclass_watches);
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(70, 5,"Turnovertype Watch Straps",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(25, 5, $turnOverWatches,1, 0, 'L', 0);
		$pdf->Ln();
		
		// Sales Classifications
		$classifications = array();
		
		if ($pos->sales_classification01) {
			$classifications[] = $pos->sales_classification01;
		}
		
		if ($pos->sales_classification02) {
			$classifications[] = $pos->sales_classification02;
		}
		
		if ($pos->sales_classification03) {
			$classifications[] = $pos->sales_classification03;
		}
		
		if ($pos->sales_classification04) {
			$classifications[] = $pos->sales_classification04;
		}
		
		if ($pos->sales_classification05) {
			$classifications[] = $pos->sales_classification05;
		}
		
		if ($classifications) {
			$sales_classifications = join(', ', $classifications);
		}
				
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(70, 5, $translate->sales_classifications, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(120, 5, $sales_classifications, 1, 0, 'L', 0);
		$pdf->Ln();
		
		// Selling Watch Straps
		$selling_bijoux = ($pos->selling_bijoux) ? $translate->yes : $translate->no;
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(70, 5, $translate->selling_bijoux, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(25, 5, $selling_bijoux, 1, 0, 'L', 0);
		
		
		// Maximum number of watches displayed
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(70, 5, $translate->posaddress_max_watches, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(120, 5, $pos->max_watches, 1, 0, 'L', 0);
		$pdf->Ln();
		
		// Maximum number of bijoux displayed
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(70, 5, $translate->posaddress_max_bijoux, 1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(120, 5, $pos->max_bijoux, 1, 0, 'L', 0);
		$pdf->Ln();
		
		
		// for independent retailers hide this fields
		if ($pos->store_postype == 4) {
			
			// Retailers overall sales surface
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(70, 5, $translate->posaddress_overall_sqms, 1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Cell(120, 5, $pos->overall_sqms,1, 0, 'L', 0);
			$pdf->Ln();
			
			// Sales wall surface dedicated to swatch
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(70, 5, $translate->posaddress_dedicated_sqms_wall,1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Cell(120, 5, $pos->dedicated_sqms_wall, 1, 0, 'L', 0);
			$pdf->Ln();
			
			// Sales freestanding surface dedicated to swatch
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(70, 5, $translate->posaddress_dedicated_sqms_free, 1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Cell(120, 5, $pos->dedicated_sqms_free, 1, 0, 'L', 0);
			$pdf->Ln();
		}
		else {
			
			// Total Surface in sqms
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(70, 5,$translate->posaddress_store_totalsurface, 1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Cell(120, 5, $pos->store_totalsurface, 1, 0, 'L', 0);
			$pdf->Ln();
			
			// Sales Surface in sqms
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(70, 5, $translate->posaddress_store_retailarea, 1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Cell(120, 5, $pos->store_retailarea, 1, 0, 'L', 0);
			$pdf->Ln();
			
			// Back Office Surface in sqms
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(70, 5, $translate->posaddress_store_backoffice, 1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Cell(120, 5, $pos->store_backoffice, 1, 0, 'L', 0);
			$pdf->Ln();
			
			// Number of Floors
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(70, 5, $translate->posaddress_store_numfloors,1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Cell(120, 5, $pos->store_numfloors, 1, 0, 'L', 0);
			$pdf->Ln();

			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(70, 5, $translate->posaddress_store_floorsurface1, 1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Cell(120, 5, $pos->store_floorsurface1, 1, 0, 'L', 0);
			$pdf->Ln();
			
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(70, 5,  $translate->posaddress_store_floorsurface2, 1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Cell(120, 5, $pos->store_floorsurface2, 1, 0, 'L', 0);
			$pdf->Ln();
			
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(70, 5, $translate->posaddress_store_floorsurface3, 1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Cell(120, 5, $pos->store_floorsurface3, 1, 0, 'L', 0);
			$pdf->Ln();
		}

		
		// google map
		$pdf->AddPage();
		
		$pdf->SetFont('arialn','B',10);
		$pdf->Cell(190, 5, "Maps" ,0, 0, 'L', 0);
		$pdf->Ln();
		$pdf->Ln();
		
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		
		$latitude = $pos->google_lat;
		$longitude = $pos->google_long;
		
		if($latitude and $longitude) {
		
			$tmpfilename1 = "posmap1.jpg";
			$url = "https://maps.google.com/maps/api/staticmap?center=$latitude,$longitude&zoom=16&size=600x450&markers=color:red%7Clabel:S%7C$latitude,$longitude&sensor=false&key=AIzaSyD2wMUs74upe6ZvEOflrrMDRCoPYABu1Qs";
			$host = $settings->shortcut;

			// geodata streaming
			if (preg_match("/retailnet.$host/", $_SERVER["HTTP_HOST"])) {
				$opts	= array('http'	=> array('proxy'=>'proxy01.sharedit.ch:8082', 'request_fulluri'=>true));
				$context = stream_context_create($opts);
				$mapImage1 = file_get_contents($url, false, $context) or die("google url not loading");
			}
			else $mapImage1 = file_get_contents($url) or die("url not loading");
				
			// create directory if not exist
			dir::make('/public/data/tmp');
			
			// open temporary file
			$fh = fopen($_SERVER['DOCUMENT_ROOT']."/public/data/tmp/".$tmpfilename1,'w') or die("can't open temporary file");
			fwrite($fh, $mapImage1);
			fclose($fh);			
			
			$tmpfilename2 = "posmap2.jpg";
			$url = "https://maps.google.com/maps/api/staticmap?center=$latitude,$longitude&zoom=13&size=600x450&markers=color:red%7Clabel:S%7C$latitude,$longitude&sensor=false&key=AIzaSyD2wMUs74upe6ZvEOflrrMDRCoPYABu1Qs";
				
			// geodata
			if (preg_match("/retailnet.$host/", $_SERVER["HTTP_HOST"])) {
				$opts	= array('http'	=> array('proxy'=>'proxy01.sharedit.ch:8082', 'request_fulluri'=>true));
				$context = stream_context_create($opts);
				$mapImage2 = file_get_contents($url, false, $context) or die("url not loading");
			}
			else $mapImage2 = file_get_contents($url) or die("url not loading");
		
			// open temporary file
			$fh = fopen($_SERVER['DOCUMENT_ROOT']."/public/data/tmp/". $tmpfilename2, 'w') or die("can't open file");
			fwrite($fh, $mapImage2);
			fclose($fh);
				
			$pdf->Image($_SERVER['DOCUMENT_ROOT']."/public/data/tmp/".$tmpfilename1, 30, $pdf->getY(), 150);
			$pdf->Ln();
			$pdf->Ln();
			$pdf->Image($_SERVER['DOCUMENT_ROOT']."/public/data/tmp/".$tmpfilename2, 30, 155, 150);
		}

		
		// add new page
		if ($materials || $furniture || $project_orders || $catalog_order) {
			$pdf->AddPage();
		}
		
		if ($materials) {

			$pdf->SetFont('arialn','B',10);
			$pdf->Cell(190, 5, "Long Term Materials" ,0, 0, 'L', 0);
			$pdf->Ln();
			$pdf->Ln();
			
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(30, 5,"Purpose Name",1, 0, 'L', 1);
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(30, 5,"Category Name",1, 0, 'L', 1);
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(25, 5,"Material Code",1, 0, 'L', 1);
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(45, 5,"Material Name",1, 0, 'L', 1);
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(15, 5,"In Use",1, 0, 'R', 1);
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(15, 5,"On Stock",1, 0, 'R', 1);
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(15, 5,"Ins. Year",1, 0, 'R', 1);
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(15, 5,"Deins. Year",1, 0, 'R', 1);
			$pdf->Ln();
			
			
			foreach ($materials as $row) {
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(30, 5,$row['mps_material_purpose_name'],1, 0, 'L', 0);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(30, 5,$row['mps_material_category_name'],1, 0, 'L', 0);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(25, 5,$row['mps_material_code'],1, 0, 'L', 0);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(45, 5,$row['mps_material_name'],1, 0, 'L', 0);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(15, 5,$row['mps_pos_material_quantity_in_use'],1, 0, 'R', 0);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(15, 5,$row['mps_pos_material_quantity_on_stock'],1, 0, 'R', 0);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(15, 5,$row['mps_pos_material_installation_year'],1, 0, 'R', 0);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(15, 5,$row['mps_pos_material_deinstallation_year'],1, 0, 'R', 0);
				$pdf->Ln();
			}
			
			$pdf->Ln();
		}
		
		
		if ($furniture) {
		
			$pdf->SetFont('arialn','B',10);
			$pdf->Cell(190, 5, "Individual Furniture" ,0, 0, 'L', 0);
			$pdf->Ln();
			$pdf->Ln();
				
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(35, 5,"Product Line",1, 0, 'L', 1);
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(35, 5,"Furniture Code",1, 0, 'L', 1);
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(60, 5,"Furniture Name",1, 0, 'L', 1);
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(15, 5,"In Use",1, 0, 'R', 1);
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(15, 5,"On Stock",1, 0, 'R', 1);
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(15, 5,"Ins. Year",1, 0, 'R', 1);
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(15, 5,"Deins. Year",1, 0, 'R', 1);
			$pdf->Ln();
				
			foreach ($furniture as $row) {
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(35, 5,$row['product_line_name'],1, 0, 'L', 0);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(35, 5,$row['item_code'],1, 0, 'L', 0);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(60, 5,$row['item_name'],1, 0, 'L', 0);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(15, 5,$row['mps_pos_furniture_quantity_in_use'],1, 0, 'R', 0);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(15, 5,$row['mps_pos_furniture_quantity_on_stock'],1, 0, 'R', 0);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(15, 5,$row['mps_pos_furniture_installation_year'],1, 0, 'R', 0);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(15, 5,$row['mps_pos_furniture_deinstallation_year'],1, 0, 'R', 0);
				$pdf->Ln();
			}
				
			$pdf->Ln();	
		}
		
	
		
		if ($project_orders) {
		
			foreach ($project_orders as $order) {
					
				$items = $model->query("
					SELECT 
						item_id, 
						FORMAT(order_item_quantity, 0) AS quantity, 
						item_code, 
						item_name, 
						item_width, 
						item_height, 
						item_length, 
						item_radius 
					FROM db_retailnet.posorders 
						LEFT JOIN db_retailnet.orders ON order_id = posorder_order 
						LEFT JOIN db_retailnet.order_items ON order_item_order = order_id 
						LEFT JOIN db_retailnet.items ON item_id = order_item_item 
					WHERE item_visible_in_mps = 1 AND posorder_type = 1 AND posorder_order = ".$order['posorder_order']."
					ORDER BY 
						item_code,
						item_name
				")->fetchAll();
					

				if ($items) {
					
					$header = $model->query("
						SELECT 
							CONCAT(posorder_ordernumber, ', ', product_line_name, ', ', projectkind_name, ' (Status ',order_actual_order_state_code, ')') AS header 
						FROM db_retailnet.posorders 
							LEFT JOIN db_retailnet.product_lines ON product_line_id = posorder_product_line 
							LEFT JOIN db_retailnet.projectkinds ON projectkind_id = posorder_project_kind 
							LEFT JOIN db_retailnet.orders ON order_id = posorder_order 
						WHERE posorder_order = ".$order['posorder_order']."
					")->fetch();
					
					
					$pdf->SetFont('arialn','B',10);
					$pdf->Cell(190, 5, "Project: ".$header['header'] ,0, 0, 'L', 0);
					$pdf->Ln();
					$pdf->Ln();
						
					$pdf->SetFont('arialn','B',8);
					$pdf->Cell(40, 5,"Furniture Code",1, 0, 'L', 1);
					$pdf->SetFont('arialn','B',8);
					$pdf->Cell(130, 5,"Furniture Name",1, 0, 'L', 1);
					$pdf->SetFont('arialn','B',8);
					$pdf->Cell(20, 5,"Quantity",1, 0, 'R', 1);
					$pdf->Ln();
					
					
					foreach ($items as $row) {
						$pdf->SetFont('arialn','B',8);
						$pdf->Cell(40, 5,$row['item_code'],1, 0, 'L', 0);
						$pdf->SetFont('arialn','B',8);
						$pdf->Cell(130, 5,$row['item_name'],1, 0, 'L', 0);
						$pdf->SetFont('arialn','B',8);
						$pdf->Cell(20, 5,$row['quantity'],1, 0, 'R', 0);
						$pdf->Ln();
					}
					
					$pdf->Ln();
					
					break;
				}
			}
		}
		
		
		if ($catalog_order['project_opening_date']) {
		
			$result = $model->query("
				SELECT 
					DATE_FORMAT(order_date, '%d.%m.%Y') AS order_date,  
					FORMAT(order_item_quantity, 0) AS quantity, 
					posorder_ordernumber, 
					item_code, 
					item_name
				FROM db_retailnet.posorders 
					LEFT JOIN db_retailnet.orders ON order_id = posorder_order 
					LEFT JOIN db_retailnet.order_items ON order_item_order = order_id 
					LEFT JOIN db_retailnet.items ON item_id = order_item_item 
				WHERE 
					item_visible_in_mps = 1 
					AND posorder_type = 2 
					AND posorder_posaddress = ".$catalog_order['posorder_posaddress']."
					AND order_date >= ".$catalog_order['project_opening_date']."
				ORDER BY
					item_code,
					item_name
			")->fetchAll();


			if ($result) {
				
				$pdf->SetFont('arialn','B',10);
				$pdf->Cell(190, 5, "Catalogue Orders: ",0, 0, 'L', 0);
				$pdf->Ln();
				$pdf->Ln();
				
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(15, 5,"Order Date",1, 0, 'L', 1);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(25, 5,"Order Number",1, 0, 'L', 1);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(40, 5,"Furniture Code",1, 0, 'L', 1);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(90, 5,"Furniture Name",1, 0, 'L', 1);
				$pdf->SetFont('arialn','B',8);
				$pdf->Cell(20, 5,"Quantity",1, 0, 'R', 1);
				$pdf->Ln();
					
					
				foreach ($result as $row) {
					$pdf->SetFont('arialn','B',8);
					$pdf->Cell(15, 5,$row['order_date'],1, 0, 'L', 0);
					$pdf->SetFont('arialn','B',8);
					$pdf->Cell(25, 5,$row['posorder_ordernumber'],1, 0, 'L', 0);
					$pdf->SetFont('arialn','B',8);
					$pdf->Cell(40, 5,$row['item_code'],1, 0, 'L', 0);
					$pdf->SetFont('arialn','B',8);
					$pdf->Cell(90, 5,$row['item_name'],1, 0, 'L', 0);
					$pdf->SetFont('arialn','B',8);
					$pdf->Cell(20, 5,$row['quantity'],1, 0, 'R', 0);
					$pdf->Ln();
				}
					
				$pdf->Ln();
			}
		}
			
		// write pdf
		$pdf->Output("mps_pos_sheet_".date('Y-m-d').".pdf");
	}
	