<?php 
/*
	Export POS Furniture List

    Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2012-04-11
    Modified by:    
    Date modified:  
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*/
		
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	define('SHEET_NAME', "POS Furniture List");
	define('SHEET_FILE_NAME', "pos_furniture_".date('Y-m-d'));
	define('SHEET_VERSION', "Excel2007");

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$filter = $_REQUEST['filter'];

	$model = new Model($application);

	// request execution time
	ini_set('max_execution_time', 120);
	ini_set('memory_limit', '1024M');

	$_REQUEST = session::filter($application, "$controller.$archived.$action", false);
	
	// title
	$title = ($_REQUEST['archived']) ? "Closed POS Locations" : "POS Locations";
	
	// sql order
	$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "country_name, place_name, posaddress_name";
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

	// filter: address parents
	if($user->permission(Pos::PERMISSION_VIEW_LIMITED)) {
		$filters['parent'] = "posaddress_client_id = ".$user->address;
	}
	
	// filter: country
	if($_REQUEST['countries']) {
		
		$filters['countries'] = "posaddress_country = ".$_REQUEST['countries'];
		
		$country = new Country();
		$country->read($_REQUEST['countries']);
		
		$captions[] = $translate->posaddress_country.": ".$country->name;
	}
	elseif($user->permission(Pos::PERMISSION_VIEW_LIMITED)) {
		
		$result = $model->query("
			SELECT DISTINCT posaddress_country
			FROM db_retailnet.posaddresses
			WHERE posaddress_client_id = ".$user->address."
		")->fetchAll();

		if ($result) {

			foreach ($result as $row) {
				$countries[] = $row['posaddress_country']; 
			}

			$user_countries = join(',', $countries);
			$filters['countries'] = "posaddress_country IN ($user_countries)";
		}
	}
		
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {

		$keyword = $_REQUEST['search'];

		$filters['search'] = "(
			posaddress_name LIKE '%$keyword%'
			OR posaddress_street LIKE '%$keyword%'
			OR country_name LIKE '%$keyword%'
			OR province_canton LIKE '%$keyword%'
			OR place_name LIKE '%$keyword%'
			OR posaddress_zip LIKE '%$keyword%'
			OR postype_name LIKE '%$keyword%'
		)";
		
		$captions[] = $translate->search.': '.$keyword;
	}
	
	// filter: pos type
	if ($_REQUEST['pos_types']) 	{
		
		$filters['pos_types'] = "posaddress_store_postype = ".$_REQUEST['pos_types'];
		
		$pos_type = new Pos_Type();
		$pos_type->read($_REQUEST['pos_types']);
		
		$captions[] = $translate->posaddress_store_postype.": ".$pos_type->name;
	}

	// filter: client
	if ($_REQUEST['client']) 	{

		$fClient = $_REQUEST['client'];

		$filters['client'] = "posaddress_client_id = $fClient";

		$res = $model->query("SELECT address_company AS caption FROM db_retailnet.addresses WHERE address_id = $fClient ")->fetch();
		$captions[] = "Client: {$res[caption]}";
	}
	
	// filter: legal types (pos owner types)
	if ($_REQUEST['posowner_types']) {

		$legalTypes = join(',', array_keys($_REQUEST['posowner_types']));
		$filters['posowner_types'] = "posaddress_ownertype IN ($legalTypes)";

		$res = $model->query("SELECT GROUP_CONCAT(posowner_type_name) AS caption FROM db_retailnet.posowner_types WHERE posowner_type_id IN ($legalTypes)")->fetch();
		$captions[] =  $translate->posaddress_ownertype.": ".$res['caption'];
	}
	
	// filter: legal types (pos owner types)
	if ($_REQUEST['possubclasses']) {

		$subclasses = join(',', array_keys($_REQUEST['possubclasses']));
		$filters['possubclasses'] = "posaddress_store_subclass IN ($subclasses)";
		
		$res = $model->query("SELECT GROUP_CONCAT(possubclass_name) AS caption FROM db_retailnet.possubclasses WHERE possubclass_id IN ($subclasses)")->fetch();
		$captions[] = "POS Type Subclasses: {$res[caption]}";
	}
	
	// filter: channel
	if ($_REQUEST['distribution_channels']) {
		
		if ($_REQUEST['distribution_channels']=='null') {
			
			$filters['distribution_channels'] = "(
				posaddress_distribution_channel = NULL
				OR posaddress_distribution_channel = ''
				OR posaddress_distribution_channel = 0
			)";
			
			$captions[] = $translate->posaddress_distribution_channel.": No Distribution Channel Assigned";
			
		} else {
			
			$distribution_channel = new DistChannel($application);
			$distribution_channel->read($_REQUEST['distribution_channels']);
			
			$filters['distribution_channels'] = "posaddress_distribution_channel=".$_REQUEST['distribution_channels'];
			
			$captions[] = $translate->posaddress_distribution_channel.": ".$distribution_channel->group.', '.$distribution_channel->code;
		}
		
		$binds['posaddress_distribution_channel'] = $properties['posaddress_distribution_channel']['bind'];
	}
	
	// filter: sales representative
	if ($_REQUEST['sales_representative']) {

		$filters['sales_representative'] = "posaddress_sales_representative = ".$_REQUEST['sales_representative'];
		
		$staff = new Staff($application);
		$staff->read($_REQUEST['sales_representative']);
		
		$staff_type = new Staff_Type($application);
		$staff_type->read($staff->type_id);
		
		$captions[] = $translate->posaddress_sales_representative.": ".$staff_type->name;
	}

	// filter: decoration person
	if ($_REQUEST['decoration_person']) {
		
		$filters['decoration_person'] = "posaddress_decoration_person=".$_REQUEST['decoration_person'];
		
		$staff = new Staff($application);
		$staff->read($_REQUEST['decoration_person']);
		
		$staff_type = new Staff_Type($application);
		$staff_type->read($staff->type_id);
		
		$captions[] = $translate->posaddress_decoration_person.": ".$staff_type->name;
	}
	
	// filter: turnoverclass_watches
	if ($_REQUEST['turnoverclass_watches']) {
		
		$filters['turnoverclass_watches'] = "posaddress_turnoverclass_watches=".$_REQUEST['turnoverclass_watches'];
		
		$turnover_class = new TurnoverClass($application);
		$turnover_class->read($_REQUEST['turnoverclass_watches']);
		
		$captions[] = $translate->posaddress_turnoverclass_watches.": ".$turnover_class->name;
	}

	// filter: turnoverclass bijoux
	if ($_REQUEST['turnoverclass_bijoux']) {
		
		$filters['turnoverclass_bijoux'] = "posaddress_turnoverclass_bijoux=".$_REQUEST['turnoverclass_bijoux'];
		
		$turnover_class = new TurnoverClass($application);
		$turnover_class->read($_REQUEST['turnoverclass_bijoux']);
		
		$captions[] = $translate->posaddress_turnoverclass_bijoux.": ".$turnover_class->name;
	}


	// filter: product lines
	if ($_REQUEST['product_lines']) {

		$productLines = join(',', array_keys($_REQUEST['product_lines']));
		$filters['product_lines'] = "posaddress_store_furniture IN ($productLines)";
		
		$res = $model->query("SELECT GROUP_CONCAT(product_line_name) AS caption FROM db_retailnet.product_lines WHERE product_line_id IN ($productLines)")->fetch();
		$captions[] = $translate->posaddress_store_furniture.": ".$res['caption'];
	}
	
	// filter: store state
	if ($archived) {
		$filters['archived'] = "(
			posaddress_store_closingdate <> '0000-00-00'
			OR posaddress_store_closingdate <> NULL
		)";
	}
	else {
		$filters['active'] = "(
			posaddress_store_closingdate = '0000-00-00'
			OR posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = ''
		)";
	}
	
	
	
	// country access filter
	if (!$_REQUEST['countries']) {
	
		$accessCountries = array();
	
		$result = $model->query("
			SELECT DISTINCT country_access_country
			FROM db_retailnet.country_access
			WHERE country_access_user = $user->id
		")->fetchAll();
	
		if ($result) {
			foreach ($result as $row) {
				$accessCountries[] = $row['country_access_country'];
			}
		}
	}
	
	
	// country access extended filter
	if (!$_REQUEST['countries'] && $accessCountries) {
	
		if ($filters) {
	
			$basicFiletrs = $filters;
			unset($basicFiletrs['parent']);
			unset($basicFiletrs['countries']);
	
			$extendedFilter = " OR ( ".join(' AND ', $basicFiletrs)."  AND posaddress_country IN ( ".join(' AND ', $accessCountries).") )";
	
		} else {
	
			$filters['countries'] = "posaddress_country IN ( ".join(',',$accessCountries)." )";
		}
	}
	
	
	// datagrid
	$result = $model->query("
		SELECT
			posaddress_id, 
			posaddress_name,
			country_name,
			province_canton, 
			place_name
		FROM db_retailnet.posaddresses
		INNER JOIN db_retailnet.addresses ON address_id = posaddress_franchisee_id
		INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
		INNER JOIN db_retailnet.provinces ON province_id = place_province
		INNER JOIN db_retailnet.countries ON country_id = posaddress_country
		LEFT JOIN db_retailnet.posowner_types ON posowner_type_id = posaddress_ownertype
		LEFT JOIN db_retailnet.postypes ON postype_id = posaddress_store_postype
		LEFT JOIN db_retailnet.mps_distchannels ON mps_distchannel_id = posaddress_distribution_channel
		LEFT JOIN db_retailnet.mps_turnoverclasses ON mps_turnoverclass_id = posaddress_turnoverclass_watches
	")
	->filter($filters)
	->extend($extendedFilter)
	->order($order, $direction)
	->fetchAll();
	
	if ($result) {
	
		// posaddresses filter
		$pos_result = _array::datagrid($result);
		$posaddresses = join(',',array_keys($pos_result));
	
		
		// pos furniture
		$pos_furnitures = $model->query("
			SELECT
				mps_pos_furniture_id,
				mps_pos_furniture_posaddress,
				item_code,
				item_name,
				mps_pos_furniture_quantity_in_use,
				mps_pos_furniture_quantity_on_stock
			FROM mps_pos_furniture
			LEFT JOIN db_retailnet.items ON item_id = mps_pos_furniture_item_id
			WHERE mps_pos_furniture_posaddress IN ($posaddresses)
			ORDER BY item_code, item_name
		")->fetchAll();
	
		
		if ($pos_furnitures) {
			
			foreach ($pos_furnitures as $row) {
				
				$furniture = array_shift($row);
				$pos = array_shift($row);
				
				$datagrid[$pos]['furniture'][$furniture] = $row;
			}
		}

	
		// get last project orders
		$last_project_orders = $model->query("
			SELECT
				posaddress_id,
				max(posorder_order) AS posorder_order
			FROM db_retailnet.posorders
			LEFT JOIN db_retailnet.posaddresses ON posaddress_id = posorder_posaddress
			WHERE
				posorder_type = 1
			AND posaddress_store_openingdate IS NOT NULL
			AND posaddress_store_openingdate <> '0000-00-00'
			AND (
				posaddress_store_closingdate IS NULL
				OR posaddress_store_closingdate = '0000-00-00'
				)
			AND posorder_order > 0
			AND posaddress_id IN ($posaddresses)
			GROUP BY posaddress_id
		")->fetchAll();
	
	
		if ($last_project_orders) {
			
			// filter last orders
			$filter = null;
			$orders = array();
			
			foreach ($last_project_orders as $row) {
				$orders[] = $row['posorder_order'];
			}
			
			$orders = join(',',$orders);
			$filter = " AND order_item_order IN ( $orders )";

			// last project furniture
			$last_project_furniture = $model->query("
				SELECT DISTINCT
					item_id,
					posorder_posaddress,
					item_code,
					item_name,
					posorder_ordernumber AS ordernumber,
					order_item_quantity AS quantity
				FROM db_retailnet.items
				INNER JOIN db_retailnet.order_items ON order_item_item = item_id
				INNER JOIN db_retailnet.orders ON order_item_order = order_id
				INNER JOIN db_retailnet.posorders ON posorder_order = order_id
				WHERE item_type = 1
				AND (item_visible_in_mps = 1 OR item_addable_in_mps = 1 )
				$filter
				ORDER BY item_code, item_name
			")->fetchAll();
			
			if ($last_project_furniture) {
				
				foreach ($last_project_furniture as $row) {
					
					$furniture = array_shift($row);
					$pos = array_shift($row);
					
					$ordernumber = ($loop==$pos) ? $ordernumber : null;
					$ordernumber = (!$ordernumber) ? $row['ordernumber'] : $ordernumber;
					
					if ($ordernumber==$row['ordernumber']) {
						$datagrid[$pos]['project'][$furniture] = $row;
					}
					
					$loop = $pos;
				}
			}
		}	
			
		// get last catalogue orders
		$last_catalog_orders = $model->query("
			SELECT
				posaddress_id,
				max(posorder_order) AS posorder_order
			FROM db_retailnet.posorders
			LEFT JOIN db_retailnet.posaddresses ON posaddress_id = posorder_posaddress
			WHERE posorder_type <> 1
			AND posaddress_store_openingdate IS NOT NULL
			AND posaddress_store_openingdate <> '0000-00-00'
			AND (
				posaddress_store_closingdate IS NULL
				OR posaddress_store_closingdate = '0000-00-00'
			)
			AND posorder_order > 0
			AND posaddress_id IN ($posaddresses)
			GROUP BY posaddress_id
		")->fetchAll();
			
			
		if ($last_catalog_orders) {
			
			$filter = null;
			$orders = array();
			
			foreach ($last_catalog_orders as $row) {
				$orders[] = $row['posorder_order'];
			}
			
			$orders = join(',',$orders);
			$filter = " AND order_item_order IN ( $orders )";
		
			// last catalogue furniture
			$catalog_furniture = $model->query("
				SELECT DISTINCT
					item_id,
					posorder_posaddress,
					item_code,
					item_name,
					posorder_ordernumber AS ordernumber,
					order_item_quantity AS quantity
				FROM db_retailnet.items
				INNER JOIN db_retailnet.order_items ON order_item_item = item_id
				INNER JOIN db_retailnet.orders ON order_item_order = order_id
				INNER JOIN db_retailnet.posorders ON posorder_order = order_id
				WHERE item_type = 1
				AND (item_visible_in_mps = 1 OR item_addable_in_mps = 1)
				$filter
				ORDER BY item_code, item_name
			")->fetchAll();
					
			if ($catalog_furniture) {
				
				foreach ($catalog_furniture as $row) {
					
					$furniture = array_shift($row);
					$pos = array_shift($row);
					
					$ordernumber = ($loop==$pos) ? $ordernumber : null;
					$ordernumber = (!$ordernumber) ? $row['ordernumber'] : $ordernumber;
					
					if ($ordernumber==$row['ordernumber']) {
						$datagrid[$pos]['catalog'][$furniture] = $row;
					}
					
					$loop = $pos;
				}
			}
		}
			
		// datagrid
		if ($datagrid) { 
			
			require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
			require_once PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php';
			
			// sort datagrid from pos copmanies
			foreach ($pos_result as $id => $row) {
				if ($datagrid[$id]) {
					$datagrid[$id]['caption'] = $row['posaddress_name'].", ".$row['country_name'].", ".$row['province_canton'].", ".$row['place_name'];
				}
			}
			
			function cmp($a, $b) {
				return strcmp($a["caption"], $b["caption"]);
			}
			
			usort($datagrid, "cmp");

			
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle(SHEET_NAME);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			
			
			// styling
			$border = array(
					'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
					'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,)
			);
			
			$styleHeader = array(
					'font' => array('name'=>'Arial','size'=> 13, 'bold'=>true),
					'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
			);
			
			$styleNumber = array(
					'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
					'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
			);
			
			
			$styleString = array(
					'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
					'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
			);
			
			
			$styleHeaderString = array(
					'font' => array('name'=>'Arial','size'=> 10, 'bold'=>true),
					'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
					'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> true),
					'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'EFEFEF')),
					'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
			);
			
			
			$styleHeaderNumber = array(
					'font' => array('name'=>'Arial','size'=> 10, 'bold'=>true),
					'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
							'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> true),
					'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'EFEFEF')),
					'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
			);
			
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$lastColumn = "D";

			$row=1;
			$range = "A$row:$lastColumn"."$row";
			$objPHPExcel->getActiveSheet()->mergeCells($range);
			$objPHPExcel->getActiveSheet()->setCellValue("A$row", "POS Locations: Furniture List");
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(18);
			
			$row++;
			$range = "A$row:$lastColumn"."$row";
			$objPHPExcel->getActiveSheet()->mergeCells($range);
			
			$row++;
			$range = "A$row:$lastColumn"."$row";
			$date = date('d.m.Y  H:i:s');
			$objPHPExcel->getActiveSheet()->mergeCells($range);
			$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: $date");
			
			// filter captions
			if ($captions) {
				
				foreach ($captions as $value) {
					$row++;
					$range = "A$row:$lastColumn"."$row";
					$objPHPExcel->getActiveSheet()->mergeCells($range);
					$objPHPExcel->getActiveSheet()->setCellValue("A$row", $value);
					$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
				}
			}
			
			
			$row++;
			$range = "A$row:$lastColumn"."$row";
			$objPHPExcel->getActiveSheet()->mergeCells($range);
			
			$row++;
			$range = "A$row:$lastColumn"."$row";
			$objPHPExcel->getActiveSheet()->mergeCells($range);
			
			
			foreach ($datagrid as $data) {
				
				$row++;
				$range = "A$row:$lastColumn"."$row";
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				$objPHPExcel->getActiveSheet()->setCellValue("A$row", $data['caption']);
				$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleHeader);
				
				$row++;
				$range = "A$row:$lastColumn"."$row";
				$objPHPExcel->getActiveSheet()->mergeCells($range);
			
				if ($data['furniture']) {
					
					$row++;
					$range = "A$row:$lastColumn"."$row";
					$objPHPExcel->getActiveSheet()->mergeCells($range);
					$objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Individual Furniture');
					$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleHeaderString);
					
					$row++;
					$col=0;
					$columns = array_keys(end($data['furniture']));
					
					// individual furniture header
					foreach ($columns as $value) {
						$index = spreadsheet::index($col,$row);
						$format = ($col>1) ? $styleHeaderNumber : $styleHeaderString;
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $translate->$value);
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($format);
						$col++;
					}
					
					foreach ($data['furniture'] as $array) {
						
						$row++; 
						$col = 0;
							
						foreach($array as $key => $value) {
							$index = spreadsheet::index($col,$row);
							$format = ($col>1) ? $styleNumber : $styleString;
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($format);
							$col++;
						}
					}
					
					$row++;
					$range = "A$row:$lastColumn"."$row";
					$objPHPExcel->getActiveSheet()->mergeCells($range);
				}
				
				
				if ($data['project']) {
						
					$row++;
					$range = "A$row:$lastColumn"."$row";
					$objPHPExcel->getActiveSheet()->mergeCells($range);
					$objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Projects');
					$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleHeaderString);
						
					$row++;
					$col=0;
					$columns = array_keys(end($data['project']));
						
					// individual furniture header
					foreach ($columns as $value) {
						$index = spreadsheet::index($col,$row);
						$format = ($col>2) ? $styleHeaderNumber : $styleHeaderString;
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $translate->$value);
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($format);
						$col++;
					}
						
					foreach ($data['project'] as $array) {
				
						$row++;
						$col = 0;
							
						foreach($array as $key => $value) {
							$index = spreadsheet::index($col,$row);
							$format = ($col>2) ? $styleNumber : $styleString;
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($format);
							$col++;
						}
					}
						
					$row++;
					$range = "A$row:$lastColumn"."$row";
					$objPHPExcel->getActiveSheet()->mergeCells($range);
				}
				
				
				if ($data['catalog']) {
				
					$row++;
					$range = "A$row:$lastColumn"."$row";
					$objPHPExcel->getActiveSheet()->mergeCells($range);
					$objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Catalogue Orders');
					$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleHeaderString);
				
					$row++;
					$col=0;
					$columns = array_keys(end($data['catalog']));
				
					// individual furniture header
					foreach ($columns as $value) {
						$index = spreadsheet::index($col,$row);
						$format = ($col>2) ? $styleHeaderNumber : $styleHeaderString;
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $translate->$value);
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($format);
						$col++;
					}
				
					foreach ($data['catalog'] as $array) {
				
						$row++;
						$col = 0;
							
						foreach($array as $key => $value) {
							$index = spreadsheet::index($col,$row);
							$format = ($col>2) ? $styleNumber : $styleString;
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($format);
							$col++;
						}
					}
				
					$row++;
					$range = "A$row:$lastColumn"."$row";
					$objPHPExcel->getActiveSheet()->mergeCells($range);
				}
				
				
				$row++;
				$range = "A$row:$lastColumn"."$row";
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				
				$row++;
				$range = "A$row:$lastColumn"."$row";
				$objPHPExcel->getActiveSheet()->mergeCells($range);
			}
			
			
			$row++;
			$range = "A$row:$lastColumn"."$row";
			$objPHPExcel->getActiveSheet()->mergeCells($range);
			
			// export
			$objPHPExcel->getActiveSheet()->setTitle(SHEET_NAME);
			$objPHPExcel->setActiveSheetIndex(0);
	
			// redirect output to client browser
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.SHEET_FILE_NAME.'.xlsx"');
			header('Cache-Control: max-age=0');
	
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('php://output');
			
			exit;
		}
		else {
			message::empty_result();
			$archived = ($archived) ? '/archived' : null;
			url::redirect("/$application/$controller$archived/$action");
		}
	}
	else {
		message::empty_result();
		$archived = ($archived) ? '/archived' : null;
		url::redirect("/$application/$controller$archived/$action");
	}
	