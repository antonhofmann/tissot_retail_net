<?php
/*
    Excel List Long Term Materials

    Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2011-03-22
    Modified by:	Roger Bucher
    Date modified:	2012-10-23
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*/

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	// request execution time
	ini_set('max_execution_time', 120);
	ini_set('memory_limit', '1024M');

	$settings = Settings::init();
	$settings->load('data');
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$filter = $_REQUEST['filter'];

	$model = new Model($application);

	// request
	$_REQUEST = session::filter($application, "$controller.$archived.$action", false);
	
	// page title
	$pagetitle = ($archived) ? 'Inactive Long Term Materials' : 'Long Term Materials';
	
	// file name
	$filename = "long_term_materials_".date('Y-m-d');
	
	// sql order
	$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'mps_material_category_name, mps_material_purpose_name, mps_material_code';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			mps_material_collection_category_code LIKE '%$keyword%' 
			OR mps_material_collection_code LIKE '%$keyword%' 
			OR mps_material_category_name LIKE '%$keyword%' 
			OR mps_material_purpose_name LIKE '%$keyword%' 
			OR mps_material_code LIKE '%$keyword%' 
			OR mps_material_name LIKE '%$keyword%'
			OR mps_material_planning_type_name LIKE '%$keyword%'
		)";
		
		$captions[] = $translate->serach.': '.$keyword;
	}


	// filter: categories
	if ($_REQUEST['categories']) {
		
		$filters['categories'] = "mps_material_material_category_id = ".$_REQUEST['categories'];
		
		$category = new Material_Category($application);
		$category->read($_REQUEST['categories']);
		
		$captions[] = $translate->mps_material_material_category_id.': '.$category->name;
	}

	// filter: collections
	if ($_REQUEST['collections']) {
		
		$filters['collections'] = "mps_material_material_collection_id = ".$_REQUEST['collections'];
		
		$collections = new Material_Collection($application);
		$collections->read($_REQUEST['collections']);
		
		$captions[] = $translate->mps_material_material_collection_id.': '.$collections->code;
	}

	// filter collection_categories
	if ($_REQUEST['collection_categories']) {
		
		$filters['collection_categories'] = "mps_material_material_collection_category_id = ".$_REQUEST['collection_categories'];
		
		$collection_categories = new Material_Collection_Category($application);
		$collection_categories->read($_REQUEST['collection_categories']);
		
		$captions[] = $translate->mps_material_material_collection_category_id.': '.$collection_categories->code;
	}

	// filter: default
	$active = ($archived) ? 0 : 1;
	$filters['default'] = "mps_material_islongterm = 1 AND mps_material_active = $active";

	// long term materials
	$result = $model->query("
		SELECT
			mps_material_id,
			mps_material_collection_category_code,
			mps_material_collection_code,
			mps_material_ean_number,
			mps_material_code,
			mps_material_name,
			mps_material_hsc,
			mps_material_category_name,
			mps_material_purpose_name,
			mps_material_width,
			mps_material_height,
			mps_material_length,
			mps_material_radius,
			mps_material_setof AS set_of,
			currency_symbol,
			FORMAT(mps_material_price,2) AS mps_material_price
		FROM mps_materials
	")
	->bind(Material::DB_BIND_CATEGORIES)
	->bind(Material::DB_BIND_PURPOSES)
	->bind(Material::DB_BIND_COLLECTIONS)
	->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
	->bind('LEFT JOIN db_retailnet.currencies ON currency_id = mps_material_currency_id')
	->filter($filters)
	->order($order, $direction)
	->fetchAll();


	if ($result) {
		
		require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
		require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

		$datagrid = _array::datagrid($result);
		$columns = array_keys(end($datagrid));
		$totalColumns = count($columns);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle($pagetitle);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

		// styling
		$border = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);

		$styleNumber = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
			)
		);


		$styleString = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
			)
		);


		$styleHeaderString = array(
			'font' => array(
				'name'=>'Arial',
				'size'=> 11,
				'bold'=>true
			),
			'alignment' => array(
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array(
					'rgb'=>'EEEEEE'
				)
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);


		$styleHeaderNumber = array(
			'font' => array(
				'name'=>'Arial',
				'size'=> 11,
				'bold'=>true
			),
			'alignment' => array(
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true,
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array(
					'rgb'=>'EEEEEE'
				)
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);


		// cel dimensions
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(60);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
		$lastColumn = "O";


		$row=1;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", $pagetitle);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(18);

		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: ".date('d.m.Y  H:i:s'));

		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		// filter captions
		if (isset($captions)) {
			foreach ($captions as $caption) {
				$row++;
				$range = "A$row:".$lastColumn.$row;
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				$objPHPExcel->getActiveSheet()->setCellValue("A$row", $caption);
				$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
			}
		}

		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		// columns
		$row++;
		$col = 0;
		$range = "A$row:".$lastColumn.$row;

		foreach ($columns as $value) {
			$column = PhpExcel_Cell::stringFromColumnIndex($col);
			$styling = ($col>6) ? $styleHeaderNumber : $styleHeaderString;
			$objPHPExcel->getActiveSheet()->getStyle($column.$row)->applyFromArray($styling);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $translate->$value);
			$col++;
		}

		$row++;

		foreach ($datagrid as $i => $array) {
			$col = 0;
			foreach($array as $key=>$value) {
				
				$column = PhpExcel_Cell::stringFromColumnIndex($col);
				
				if ($col>6) {
					$objPHPExcel->getActiveSheet()->getStyle($column.$row)->applyFromArray($styleNumber);
					if ($column==$lastColumn) $objPHPExcel->getActiveSheet()->getStyle($column.$row)->getNumberFormat()->setFormatCode("0.0000");
				}

				if ($column=="C") $objPHPExcel->getActiveSheet()->getCell($column.$row)->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_STRING);
				else $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);

		        $col++;
		    }

			$range = "A$row:".$lastColumn.$row;
		    $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($border);
		    $row++;
		}

		$row++;
		$range = "A$row:".$lastColumn.$row;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		$objPHPExcel->getActiveSheet()->setTitle($pagetitle);
		$objPHPExcel->setActiveSheetIndex(0);

		// redirect output to client browser
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}
	else {
		message::empty_result();
		url::redirect("/$application/$controller$archived/$action");
	}
