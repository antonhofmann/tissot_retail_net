<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	// execution time
	ini_set('max_execution_time', 120);
	ini_set('memory_limit', '1024M');
	
	$settings = Settings::init();
	$settings->load('data');
	$user = User::instance();
	$translate = Translate::instance();
	
	// request vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	// order Sheet
	$ordersheet = new Ordersheet($application);
	$ordersheet->read($id);
	
	// block statments
	$statment_on_preparation = $ordersheet->state()->onPreparation();
	$statment_on_confirmation = $ordersheet->state()->onConfirmation();
	$statment_on_distribution = $ordersheet->state()->onDistribution();
	
	// mastersheet
	$mastersheet = new Mastersheet($application);
	$mastersheet->read($ordersheet->mastersheet_id);
	
	// company
	$company = new Company($application);
	$company->read($ordersheet->address_id);
	
	// workflow state
	$workflow_state = new Workflow_State($application);
	$workflow_state->read($ordersheet->workflowstate_id);
	
	// page title
	$pagetitle = ($archived) ? 'Archived Order Sheet Items' : 'Order Sheet Items';
	
	// title
	$title = $company->company.', '.$mastersheet->name.', '.$mastersheet->year;
	
	// file name
	$filename  = "mps_ordersheet_";
	$filename .= str_replace(' ', '_', strtolower($company->company));
	$filename .= str_replace(' ', '_', strtolower($mastersheet->name));
	$filename .= date('Ymd');
	
	// db model
	$model = new Model($application);
	
	// datagrid
	$result = $model->query("
		SELECT 
			mps_ordersheet_item_id,
			mps_ordersheet_item_price,
			mps_ordersheet_item_quantity,
			mps_ordersheet_item_quantity_proposed,
			mps_ordersheet_item_quantity_approved,
			mps_ordersheet_item_quantity_distributed,
			mps_ordersheet_item_quantity_confirmed,
			mps_ordersheet_item_order_date,
			mps_material_collection_code,
			mps_material_collection_category_id,
			mps_material_collection_category_code,
			mps_material_planning_type_id,
			mps_material_planning_type_name,
			mps_material_code,
			mps_material_name,
			mps_material_hsc,
			mps_material_setof,
			currency_symbol,
			(mps_ordersheet_item_price * mps_ordersheet_item_quantity_confirmed) AS total_cost_ordered
		FROM mps_ordersheet_items
	")
	->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
	->bind(Ordersheet_Item::DB_BIND_MATERIALS)
	->bind(Material::DB_BIND_PLANNING_TYPES)
	->bind(Material::DB_BIND_COLLECTIONS)
	->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
	->bind(Material::DB_BIND_CURRENCIES)
	->filter('ordersheets', "mps_ordersheet_id = $id")
	->order('mps_material_planning_type_name')
	->order('mps_material_collection_category_code')
	->order('mps_material_code')
	->order('mps_material_name')
	->fetchAll();
	
	if ($result) {
		
		require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
		require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');
		
		
		foreach ($result as $row) {
			
			$ordersheet_item_id = $row['mps_ordersheet_item_id'];
			$planning = $row['mps_material_planning_type_id'];
			$collection = $row['mps_material_collection_category_id'];
			
			// item prise
			$item_price = $row['mps_ordersheet_item_price'];
			
			// item ordered date
			$item_order_date = $row['mps_ordersheet_item_order_date'];
			
			// item quantity
			$quantity = $row['mps_ordersheet_item_quantity'];
			$quantity = (($statment_on_confirmation || $statment_on_distribution) && !$quantity) ? 0 : $quantity;
			
			// item proposed quantity
			$quantity_proposed = $row['mps_ordersheet_item_quantity_proposed'];
			$quantity_proposed = (($statment_on_confirmation || $statment_on_distribution) && !$quantity_proposed) ? 0 : $quantity_proposed;
			
			// item approved quantity
			$quantity_approved = $row['mps_ordersheet_item_quantity_approved'];
			$quantity_approved = (($statment_on_confirmation || $statment_on_distribution) && !$quantity_approved) ? 0 : $quantity_approved;
			
			// item ordered quantity
			$quantity_ordered = $row['mps_ordersheet_item_quantity_confirmed'];
			
			// item delivered quantity
			$quantity_delivered = $row['mps_ordersheet_item_quantity_distributed'];
			
			if ($quantity_delivered) {
				$totalprice = $item_price * $quantity_delivered;
			}
			elseif ($quantity_ordered) {
				$totalprice = $item_price * $quantity_ordered;
			}
			elseif ($quantity_approved) {
				$totalprice = $item_price * $quantity_approved;
			}
			elseif ($quantity) {
				$totalprice = $item_price*$quantity;
			}elseif ($quantity_proposed) {
				$totalprice = $item_price*$quantity_proposed;
			}
			else $totalprice = 0;
			
			// total cost
			$total_cost = number_format($totalprice, $settings->decimal_places, '.', '') ;
				
			// datagrid
			$datagrid[$planning]['caption'] = $row['mps_material_planning_type_name'];
			$datagrid[$planning]['data'][$collection]['caption'] = $row['mps_material_collection_category_code'];
			$datagrid[$planning]['data'][$collection]['data'][$ordersheet_item_id]['mps_material_collection_code'] = $row['mps_material_collection_code'];
			$datagrid[$planning]['data'][$collection]['data'][$ordersheet_item_id]['mps_material_code'] = $row['mps_material_code'];
			$datagrid[$planning]['data'][$collection]['data'][$ordersheet_item_id]['mps_material_name'] = $row['mps_material_name'];
			$datagrid[$planning]['data'][$collection]['data'][$ordersheet_item_id]['mps_material_hsc'] = $row['mps_material_hsc'];
			$datagrid[$planning]['data'][$collection]['data'][$ordersheet_item_id]['mps_material_setof'] = $row['mps_material_setof'];
			$datagrid[$planning]['data'][$collection]['data'][$ordersheet_item_id]['mps_ordersheet_item_price'] = $row['mps_ordersheet_item_price'];
			$datagrid[$planning]['data'][$collection]['data'][$ordersheet_item_id]['currency_symbol'] = strtoupper($row['currency_symbol']);
			$datagrid[$planning]['data'][$collection]['data'][$ordersheet_item_id]['mps_ordersheet_item_quantity_proposed'] = $quantity_proposed;
			$datagrid[$planning]['data'][$collection]['data'][$ordersheet_item_id]['mps_ordersheet_item_quantity'] = $quantity;
			$datagrid[$planning]['data'][$collection]['data'][$ordersheet_item_id]['mps_ordersheet_item_quantity_approved'] = $quantity_approved;
			$datagrid[$planning]['data'][$collection]['data'][$ordersheet_item_id]['mps_ordersheet_item_quantity_confirmed'] = $quantity_ordered;
			$datagrid[$planning]['data'][$collection]['data'][$ordersheet_item_id]['mps_ordersheet_item_quantity_distributed'] = $quantity_delivered;
			$datagrid[$planning]['data'][$collection]['data'][$ordersheet_item_id]['total_cost_pre_ordered'] = $total_cost;
		}
	
		
		$columns = array_keys(end($datagrid));
		$totalColumns = count($columns);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle($pagetitle);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);


		// styling
		$border = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,)
		);
		
		$styleNumber = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
		);
		
		
		$styleString = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		);
		
		
		$styleTotalCollection = array(
			'font' => array('name'=>'Arial','size'=> 10, 'bold'=>true),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> false)
		);
		
		
		$styleTotalOrdersheet = array(
			'font' => array('name'=>'Arial','size'=> 14, 'bold'=>true),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> false)
		);
		
		
		$styleHeaderString = array(
			'font' => array('name'=>'Arial','size'=> 10, 'bold'=>true),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> true),
			'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'EFEFEF')),
			'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
		);
		
		
		$styleHeaderNumber = array(
			'font' => array('name'=>'Arial','size'=> 10, 'bold'=>true),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> true),
			'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'EFEFEF')),
			'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
		);

		
		// column dimensions
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
		$lastColumn = "M";
		
		$numeric_columns = array(4,5,7,8,9,10,11,12);
		$currency_columns = array(4,11);

		$row=1;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", $title);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(18);
		
		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", '');
		
		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Workflow State: ".$workflow_state->name);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
		
		$row++;
		$range = "A$row:$lastColumn"."$row";
		$date = date('d.m.Y  H:i:s');
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: $date");
		
		
		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", '');
		
		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", '');
		
		
		foreach ($datagrid as $key => $data) {
			
			// planning type name
			$row++;
			$range = "A$row:$lastColumn"."$row";
			$objPHPExcel->getActiveSheet()->mergeCells($range);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $data['caption']);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setBold(true);
			
			$row++;
			$range = "A$row:$lastColumn"."$row";
			$objPHPExcel->getActiveSheet()->mergeCells($range);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, '');
			
			foreach ($data['data'] as $subkey => $collection) {
				
				// item collection name
				$row++;
				$range = "A$row:$lastColumn"."$row";
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $collection['caption']);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(12);
				$objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setBold(true);
				
				$row++;
				$range = "A$row:$lastColumn"."$row";
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, '');
				
				$row++;
				$col = 0;
				$range = "A$row:$lastColumn"."$row";
				
				$columns = array_keys(end($collection['data']));
				
				foreach ($columns as $value) {
					
					$index = spreadsheet::index($col,$row);
					
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $translate->$value);
					
					if (in_array($col, $numeric_columns)) $objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleHeaderNumber);
					else $objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleHeaderString);
					
					$col++;
				}
				
				
				$row++;
				$totalCollection = 0;
				
				foreach ($collection['data'] as $i => $array) {
				
					$col = 0;
				
					foreach($array as $key=>$value) {
						
						$index = spreadsheet::index($col,$row);
						
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
						
						if (in_array($col, $numeric_columns)) $objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleNumber);
						else $objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleString);
						
						if (in_array($col, $currency_columns)) {
							$objPHPExcel->getActiveSheet()->getStyle($index)->getNumberFormat()->setFormatCode("0.0000");
						}
						
						$col++;
					}
					 
					$row++;
					$totalCollection = $totalCollection + $array['total_cost_pre_ordered'];
				}
				
				$totalCollection = number_format($totalCollection, $settings->decimal_places, '.', '');
				$totalOrdersheet = $totalOrdersheet + $totalCollection;
				
				$range = "A$row:$lastColumn"."$row";
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "Total Cost (".$collection['caption']."): ".$totalCollection);
				$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleTotalCollection);
				
				$row++;
				$range = "A$row:$lastColumn"."$row";
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, '');
			}
			
			$row++;
			$range = "A$row:$lastColumn"."$row";
			$objPHPExcel->getActiveSheet()->mergeCells($range);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, '');
		}
		
		
		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, '');
		
		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, '');
		
		
		$row++;
		$range = "A$row:$lastColumn"."$row";
		$totalOrdersheet = number_format($totalOrdersheet, $settings->decimal_places, '.', '');
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "Total Order Sheet: ".$totalOrdersheet);
		$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleTotalOrdersheet);
		
		
		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, '');
		

		$objPHPExcel->getActiveSheet()->setTitle($pagetitle);
		$objPHPExcel->setActiveSheetIndex(0);

		// redirect output to client browser
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}
	else {
			message::empty_result();
			url::redirect("/$application/$controller$archived/$action/$id");
	}
	