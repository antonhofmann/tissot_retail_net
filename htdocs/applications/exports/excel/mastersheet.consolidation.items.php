<?php 
		
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	// execution time
	ini_set('max_execution_time', 120);
	ini_set('memory_limit', '1024M');
	
	$settings = Settings::init();
	$settings->load('data');
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$filter = $_REQUEST['filter'];
	$id = $_REQUEST['mastersheet'];

	
	$model = new Model($application);
	
	// get session stored request
	$_REQUEST = session::filter($application, "$controller.$archived.$action", false);
	
	// mastersheet
	$mastersheet = new Mastersheet($application);
	$mastersheet->read($id);
	
	// export file name
	$filename  = "mps_consolidated_ordersheet_items_";
	$filename .= str_replace(' ', '_', strtolower($mastersheet->name));
	$filename .= "_".date('Ymd') ;
	
	// titles
	$pagetitle = $translate->consolidated_order_sheet_items;
	$title = $mastersheet->name.', '.$mastersheet->year;
	
	// caption: client
	if ($_REQUEST['ordersheets']) {
		
		$filters['ordersheet'] = "mps_ordersheet_id = ".$_REQUEST['ordersheets'];
	
		$result = $model->query("
			SELECT CONCAT(country_name, ', ',address_company) AS client
			FROM mps_ordersheets
		")
		->bind(Ordersheet::DB_BIND_COMPANIES)
		->bind(Company::DB_BIND_COUNTRIES)
		->filter('client', $filters['ordersheet'])
		->fetch();
		
		$captions[] = "Client: ".$result['client'];
	}
	
	// caption: workflow state
	if ($_REQUEST['workflowstates']) {
		
		$workflowstate = new Workflow_State($application);
		$workflowstate->read($_REQUEST['workflowstates']);
		
		$captions[] = "Workflow State: : ".$workflowstate->name;
		
		$filters['ordersheet'] = "mps_ordersheet_workflowstate_id = ".$_REQUEST['workflowstates'];
	}
	
	$filters['default'] = "mps_ordersheet_mastersheet_id = $id";
	
	// ordersheet items
	$result = $model->query("
		SELECT
			mps_ordersheet_id,
			mps_material_planning_type_id,
			mps_material_planning_type_name,
			mps_material_collection_category_id,
			mps_material_collection_category_code,
			mps_material_collection_code,
			mps_material_id,
			mps_material_code,
			mps_material_name,
			mps_material_hsc,
			mps_material_setof,
			mps_ordersheet_item_price,
			currency_symbol,
			mps_ordersheet_item_desired_delivery_date,
			mps_ordersheet_item_order_date,
			SUM(mps_ordersheet_item_quantity) AS quantity,
			SUM(mps_ordersheet_item_quantity_proposed) AS quantity_proposed,
			SUM(mps_ordersheet_item_quantity_approved) AS quantity_approved,
			SUM(mps_ordersheet_item_quantity_confirmed) AS quantity_ordered,
			SUM(mps_ordersheet_item_quantity_distributed) AS quantity_delivered,
			(mps_ordersheet_item_price * Sum(mps_ordersheet_item_quantity_approved)) AS total_cost,
			(mps_ordersheet_item_price * Sum(mps_ordersheet_item_quantity_confirmed)) as total_cost_ordered
		FROM mps_ordersheet_items
	")
	->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
	->bind(Ordersheet_Item::DB_BIND_MATERIALS)
	->bind(Material::DB_BIND_COLLECTIONS)
	->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
	->bind(Material::DB_BIND_PLANNING_TYPES)
	->bind(Material::DB_BIND_CURRENCIES)
	->filter($filters)
	->group('mps_material_id')
	->order('mps_material_planning_type_name')
	->order('mps_material_collection_category_code')
	->order('mps_material_code')
	->order('mps_material_name')
	->fetchAll();

	if ($result) {
		
		require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
		
		foreach ($result as $row) {
			
			$planning = $row['mps_material_planning_type_id'];
			$collection = $row['mps_material_collection_category_id'];
			$item = $row['mps_material_id'];
			
			$datagrid[$planning]['caption'] = $row['mps_material_planning_type_name'];
			$datagrid[$planning]['collections'][$collection]['caption'] = $row['mps_material_collection_category_code'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_collection_code'] = $row['mps_material_collection_code'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_code'] = $row['mps_material_code'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_name'] = $row['mps_material_name'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_hsc'] = $row['mps_material_hsc'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['set_of'] = $row['mps_material_setof'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_item_price'] = $row['mps_ordersheet_item_price'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['currency_symbol'] = strtoupper($row['currency_symbol']);
			$datagrid[$planning]['collections'][$collection]['items'][$item]['proposed'] = $row['quantity_proposed'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['quantity'] = $row['quantity'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['approved'] = $row['quantity_approved'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['total_cost'] = $row['total_cost'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_item_quantity_confirmed'] = $row['quantity_ordered'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['total_cost_ordered'] = $row['total_cost_ordered'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_item_quantity_distributed'] = $row['quantity_delivered'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['total_cost_delivered'] = $row['total_cost_delivered'];
		}	
	
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle($pagetitle);						 
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		// styling
		$border = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,)
		);
		
		$styleNumber = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
		);
		
		
		$styleString = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		);
		
		
		$styleTotalCollection = array(
			'font' => array('name'=>'Arial','size'=> 10, 'bold'=>true),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> false)
		);
		
		
		$styleTotalOrdersheet = array(
			'font' => array('name'=>'Arial','size'=> 14, 'bold'=>true),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> false)
		);
		
		$styleHeaderString = array(
			'font' => array(
				'name'=>'Arial',
				'size'=> 11,
				'bold'=>true
			),
			'alignment' => array(
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array(
						'rgb'=>'EEEEEE'
				)
			),
			'borders' => array(
				'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		
		$styleHeaderNumber = array(
			'font' => array(
				'name'=>'Arial',
				'size'=> 11,
				'bold'=>true
			),
			'alignment' => array(
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true,
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array(
						'rgb'=>'EEEEEE'
				)
			),
			'borders' => array(
				'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		
		
		// column dimensions
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
		$lastColumn = "O";
		
		// numeric columns
		$numeric_columns = array(4,5,7,8,9,10,11,12,13);
	
		$row=1;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", $title);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(18);
		
		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		
		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", $translate->date.": ".date('d.m.Y  H:i:s'));
		
		// filter captions
		if ($captions) {
			foreach ($captions as $caption) {
				$row++;
				$range = "A$row:".$lastColumn.$row;
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				$objPHPExcel->getActiveSheet()->setCellValue("A$row", $caption);
				$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
			}
		}
		
		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		
		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		
		
		foreach ($datagrid as $key => $data) {
			
			$row++;
			$range = "A$row:".$lastColumn.$row;
			$objPHPExcel->getActiveSheet()->mergeCells($range);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $data['caption']);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setBold(true);
			
			$row++;
			$range = "A$row:".$lastColumn.$row;
			$objPHPExcel->getActiveSheet()->mergeCells($range);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, '');
			
			foreach ($data['collections'] as $subkey => $collection) {
				
				$row++;
				$range = "A$row:".$lastColumn.$row;
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $collection['caption']);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getFont()->setSize(11);
				$objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setBold(true);
				
				$row++;
				$range = "A$row:".$lastColumn.$row;
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, '');
				
				$row++;
				$col = 0;
				$range = "A$row:".$lastColumn.$row;
				
				$columns = array_keys(end($collection['items']));
				
				foreach ($columns as $value) {
					$index = spreadsheet::index($col,$row);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $translate->$value);
					$cell_style = (in_array($col, $numeric_columns)) ? $styleHeaderNumber : $styleHeaderString;
					$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($cell_style);
					
					$col++;
				}
				
				// column dimensions
				$objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setBold(true);
				
				
				$row++;
				$totalCollection = 0;
				
				foreach ($collection['items'] as $i => $array) {
				
					$col = 0;
				
					foreach($array as $key=>$value) {
						$index = spreadsheet::index($col,$row);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
						$cell_style = (in_array($col, $numeric_columns)) ? $styleNumber : $styleString;
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($cell_style);
						$col++;
					}
					 
					$range = "A$row:".$lastColumn.$row;
					$row++;
					
					$totalCollection = $totalCollection + $array['total_cost'];
				}
				
				$totalCollection = number_format($totalCollection, $settings->decimal_places, '.', '');
				$totalOrdersheet = $totalOrdersheet + $totalCollection;
				
				$range = "A$row:".$lastColumn.$row;
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $translate->total_cost." (".$collection['caption']."): ".$totalCollection);
				$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleTotalCollection);
				
				$row++;
				$range = "A$row:".$lastColumn.$row;
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, '');
			}
			
			$row++;
			$range = "A$row:".$lastColumn.$row;
			$objPHPExcel->getActiveSheet()->mergeCells($range);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, '');
		}
		
		
		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, '');
		
		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, '');
		
		
		$row++;
		$range = "A$row:".$lastColumn.$row;
		$totalOrdersheet = number_format($totalOrdersheet, $settings->decimal_places, '.', '');
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $translate->total_master_sheet.": ".$totalOrdersheet);
		$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleTotalOrdersheet);
		
		
		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, '');
	
		$objPHPExcel->getActiveSheet()->setTitle($pagetitle);
		$objPHPExcel->setActiveSheetIndex(0);

		// redirect output to client browser
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}
	else {
		message::empty_result();
		url::redirect("/$application/$controller/$action/$id");
	}
							 