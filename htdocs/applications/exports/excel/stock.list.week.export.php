<?php 
/*
    Summary of Stock value

    Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2011-10-11
    Modified by:    
    Date modified:  
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*/

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";

$model = new Model(Connector::DB_CORE);

// request vars
$year = $_REQUEST['scpps_week_year'];
$suppliers = $_REQUEST['suppliers'];

if (!$year || !$suppliers) {
	die("Bad request.");
}
		
$suppliers = array_keys($suppliers); 
$supplier_addresses = join(',', $suppliers);

// dataloader product lines ::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("
	SELECT DISTINCT product_line_id, product_line_name
	FROM product_lines
")->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$key = $row['product_line_id'];
		$_PRODUCT_LINES[$key] = $row['product_line_name'];
	}
}

// dataloader suppliers ::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("
	SELECT DISTINCT address_id, address_company
	FROM suppliers
	INNER JOIN addresses ON address_id = supplier_address
	WHERE supplier_address IN ($supplier_addresses)
")->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$key = $row['address_id'];
		$_SUPPLIERS[$key] = $row['address_company'];
	}
}

// data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("
	SELECT
		scpps_stocklist_id,
	    scpps_stocklist_productline_id,
	    scpps_stocklist_productline_productline_id,
	    scpps_stocklist_supplier_address,
	    scpps_week_item,
	    scpps_week_week,
		scpps_week_stock,
		scpps_week_item_supplier_price,
		scpps_week_currency_exchange_rate,
		scpps_week_currency_factor
	FROM scpps_weeks
	INNER JOIN scpps_stocklists ON scpps_week_stocklist_id = scpps_stocklist_id
	INNER JOIN scpps_stocklist_productlines ON scpps_stocklist_productline_stocklist_id = scpps_stocklist_id
	WHERE scpps_week_year = $year
		AND scpps_stocklist_supplier_address IN ($supplier_addresses)
")->fetchAll();

$sources = array();

if ($result) {

	foreach ($result as $row) {
		
		$supplier = $row['scpps_stocklist_supplier_address'];
		$list = $row['scpps_stocklist_id'];
		$list_productline_id = $row['scpps_stocklist_productline_id'];
		$productline = $row['scpps_stocklist_productline_productline_id'];
		$item = $row['scpps_week_item'];

		// price
		$stock = $row['scpps_week_stock'];
		$price = $row['scpps_week_item_supplier_price'];
		$exchange_rate = $row['scpps_week_currency_exchange_rate'];
		$currency_factor = $row['scpps_week_currency_factor'];
		$price = ($stock*$price*$exchange_rate) / $exchange_rate;

		// month
		$month = Date::month_number_from_week($row['scpps_week_week'], $year);
		
		$sources[$supplier][$list][$productline][$item][$month] += $price;
	}

	foreach ($sources as $supplier => $stocklists) {
		foreach ($stocklists as $stocklist => $productlines) {
			foreach ($productlines as $productline => $items) {	
				foreach ($items as $item => $months) {
					foreach ($months as $month => $stocks) {
						$datagrid[$supplier][$productline][$month] += $stocks;
					}
				}
			}
		}
	}
}

//echo "<pre>"; print_r($_SUPPLIERS); print_r($_PRODUCT_LINES); print_r($datagrid); die;

// spreadsheet data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// total suppliers
$totalSuppliers = count($suppliers);
	
// total productlines
if ($datagrid) {
	foreach ($datagrid as $supplier => $productlines) {
		$totalProductLines += count($productlines);
	}
}
	
// + counter
// + supplier
// + product line
$fixColumns = 3;
	
// + sheet title
// + separator
// + months
// + cuurency
$fixRows = 5;

// row separator
$rowSeparators = 1;

// column separator
$columnSeparators = 0;

// stock value
// total amount / stock value
// movement of stock
$columnsPerMonth = 3;

// suppliers total value of stock
$extendedRows = 1;

// no defined
$extendedColumns = 0;

// total month year
$totalMonths = 12;

// total month year
$totalMonthColumns = $totalMonths * $columnsPerMonth;

// last item row
$lastProductLineRow = $fixRows + $totalProductLines;

// last week colum
$lastSupplierColumn = $fixColumns + $totalMonthColumns;

// first extended row index
$firstExtendedRow = $lastProductLineRow + $rowSeparators + 1;

// first extended column index
$firstExtendedColumn = $lastSupplierColumn + $columnSeparators + 1;

// total grid rows
$totalRows = $lastProductLineRow + $extendedRows + $rowSeparators;

// total grid columns
$totalColumns = $lastSupplierColumn + $extendedColumns + $columnSeparators;

// last indexs
$indexLastColumn = spreadsheet::key($totalColumns);
$indexLastRow = spreadsheet::key($totalRows);
	
	
// php excel :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$objPHPExcel = new PHPExcel();
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);


$borders = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));;

$textTitle = array(
		'font' => array('name'=>'Arial','size'=> 18, 'bold'=>true),
		'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> true),
		'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'EEEEEE')),
		'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
);

$textHeader = array(
		'font' => array('name'=>'Arial','size'=> 9, 'bold'=>true),
		'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> false),
		'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'EEEEEE')),
		'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
);

$textCenter = array(
		'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> false),
		'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
);

$textSupplier = array(
		'font' => array('name'=>'Arial','size'=> 8, 'bold'=>true),
		'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> false),
		'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
);

$textProductLine = array(
		'font' => array('name'=>'Arial','size'=> 8, 'bold'=>true),
		'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> false),
		'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
);

$textdefault = array(
		'font' => array('name'=>'Arial','size'=> 10),
		'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> false),
		'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
);

// row and column dimensions :::::::::::::::::::::::::::::::::::::::::::::::::::

$objPHPExcel->getActiveSheet()->getColumnDimension('a')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('b')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('c')->setWidth(5);

$trigger = 3;
	
for ($i = 4; $i <= $totalColumns; $i++) {

	$key = $i-$trigger;
	$col = spreadsheet::key($i);

	switch ($key) {
		case 1:
			$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setWidth(20);
			break;
		case 2:
			$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setWidth(5);
			break;
		case 3:
			$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setWidth(20);
			break;
	}

	if ($i-$trigger == 3) $trigger = $trigger + 3;
}
	
	
// spreadsheet captions ::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row = 1;
$range = "a$row:".spreadsheet::key($totalColumns,$row);
$objPHPExcel->getActiveSheet()->mergeCells($range);

$row++;
$range = "a$row:".spreadsheet::key($totalColumns,$row);
$objPHPExcel->getActiveSheet()->mergeCells($range);
$objPHPExcel->getActiveSheet()->setCellValue("a$row", 'SUMMARY OF STOCK VALUE STOCK RETAIL DEPARTMENT '.$year);
$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($textTitle);

$row++;
$range = "a$row:".spreadsheet::key($totalColumns,$row);
$objPHPExcel->getActiveSheet()->mergeCells($range);

$trigger = 3;
$month = 1;

for ($col = 1; $col <= $totalColumns; $col++) {

	$index = spreadsheet::key($col,4);

	switch ($col) {
			
		case 1:
			$objPHPExcel->getActiveSheet()->setCellValue($index, '');
			$objPHPExcel->getActiveSheet()->getStyle($index)->getAlignment()->setTextRotation(90);
			break;
				
		case 2:
			$objPHPExcel->getActiveSheet()->setCellValue($index, 'SUPPLIERS');
			$objPHPExcel->getActiveSheet()->getStyle($index)->getAlignment()->setTextRotation(90);
			break;
				
		case 3:
			$objPHPExcel->getActiveSheet()->setCellValue($index, '');
			$objPHPExcel->getActiveSheet()->getStyle($index)->getAlignment()->setTextRotation(90);
			break;
				
		case $col <= $totalColumns:

			$key = $col - $trigger;

			switch ($key) {
				case 1:
					$objPHPExcel->getActiveSheet()->setCellValue($index, strtoupper(date('F', mktime(0, 0, 0, $month, 1, $year)))."    ");
					$objPHPExcel->getActiveSheet()->getStyle($index)->getAlignment()->setTextRotation(90);
					break;
				case 2:
					$objPHPExcel->getActiveSheet()->setCellValue($index, '%');
					break;
				case 3:
					$objPHPExcel->getActiveSheet()->setCellValue($index, '+/-');
					break;
			}

			if ($col-$trigger == 3) {
				$trigger = $trigger + 3;
				$month++;
			}

			break;
	}
}
	
$row++;
$range = "a$row:".spreadsheet::key($totalColumns,$row);
$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($textHeader);

$trigger = 3;
	
for ($col = $fixColumns+1; $col <= $totalColumns; $col++) {

	$index = spreadsheet::key($col,5);
	$key = $col - $trigger;

	if ($key == 1) $objPHPExcel->getActiveSheet()->setCellValue($index, 'CHF');

	if ($col-$trigger == 3) $trigger = $trigger + 3;
}

$row++;
$range = "a$row:".spreadsheet::key($totalColumns,$row);
$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($textCenter);

if ($datagrid) {

	$index_supplier = 1;

	foreach ($datagrid as $supplier => $productlines) {

		$supplier_name = $_SUPPLIERS[$supplier];
		$total_productlines = count($productlines);
		$index_productlines = ($total_productlines > 1) ? 1 : 0;
		$start_row_merge = ($total_productlines > 1) ? $row+1 : 0;

		foreach ($productlines as $productline => $months) {
			
			$row++;
			$i++;
			$month = 1;
			$trigger = 3;
			$index = spreadsheet::key($col, $row);
			$productline_name = $_PRODUCT_LINES[$productline];
			$counter = ($index_productlines) ? $index_supplier.".".$index_productlines : $index_supplier;
			
			for ($col=1; $col<=$totalColumns; $col++) {
			
				$index = spreadsheet::key($col, $row);
			
				switch ($col) {
			
					case 1:
						$objPHPExcel->getActiveSheet()->setCellValue($index, $counter);
						$objPHPExcel->getActiveSheet()->getStyle($index)->getAlignment()->setTextRotation(90);
						break;
			
					case 2:
						$objPHPExcel->getActiveSheet()->setCellValue($index, $supplier_name);
						$objPHPExcel->getActiveSheet()->getStyle($index)->getAlignment()->setTextRotation(90);
						break;
			
					case 3:
						$objPHPExcel->getActiveSheet()->setCellValue($index, $productline_name);
						$objPHPExcel->getActiveSheet()->getStyle($index)->getAlignment()->setTextRotation(90);
						break;
			
					case $col > $fixColumns && $col <= $totalColumns :
			
						$key = $col-$trigger;
			
						switch ($key) {
			
							case 1:
								$stock_value = ($months[$month]) ? $months[$month] : 0;
								$objPHPExcel->getActiveSheet()->setCellValue($index, $stock_value);
								break;
			
							case 2:
								$average = ($month_total_stock[$month] && $months[$month]) ? $month_total_stock[$month]/$months[$month] : 0;
								$objPHPExcel->getActiveSheet()->setCellValue($index, $average);
								break;
			
							case 3:
								$movement_of_stock = ($month < 12) ? $months[$month+1]-$months[$month] : $months[$month];
								$total_movement_of_stock[$month] = $total_movement_of_stock[$month] + $movement_of_stock;
								$objPHPExcel->getActiveSheet()->setCellValue($index, $movement_of_stock);
								$trigger = $trigger + 3;
								$month++;
								break;
						}
			
						break;
				}
			}
				
			$bgColor = ($i % 2) ? "f6f6f6f6" : "ffffffff";
			$range = "a".$row.":".$indexLastColumn.$row;
			$objPHPExcel->getActiveSheet()->getStyle($range)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($bgColor);
		
			if ($index_productlines) $index_productlines++;
		}
		
		// merge supplier column
		if ($start_row_merge && ($start_row_merge>$row)) {
			$objPHPExcel->getActiveSheet()->mergeCells("b".$start_row_merge.":b".$row);
		}
		
		// supplier increment
		$index_supplier++;
	}
}
		
$row++;
$objPHPExcel->getActiveSheet()->mergeCells("a$row:$indexLastColumn$row");


// bottom totals :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row++;
$rangeTotal = "a$row:$indexLastColumn$row";
$objPHPExcel->getActiveSheet()->setCellValue("a$row", 'TOTAL');
$objPHPExcel->getActiveSheet()->mergeCells("a$row:c$row");

$trigger = 3;
$month = 1;

for ($col = 4; $col <= $totalColumns; $col++) {

	$index = spreadsheet::key($col,$row);

	$key = $col-$trigger;

	switch ($key) {
		case 1:
			$total = ($month_total_stock[$month]) ? $month_total_stock[$month] : 0;
			$objPHPExcel->getActiveSheet()->setCellValue($index, $total);
			break;
			
		case 3:
			$objPHPExcel->getActiveSheet()->setCellValue($index, $total_movement_of_stock[$month]);
			$trigger = $trigger + 3;
			$month++;
			break;
	}
}

// column counter
$range = "a6:a".$totalRows;
$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($textSupplier);

// column suppliers
$range = "b6:b".$totalRows;
$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($textSupplier);

// column productlines
$range = "c6:c".$totalRows;
$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($textProductLine);

// movement of stock
for ($i = 4; $i <= $totalColumns; $i++) {
	$col = spreadsheet::key($i);
	$range = $col."6:".$col.$totalRows;
	$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($textdefault);
	$objPHPExcel->getActiveSheet()->getStyle($range)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
}

// range total amount
$objPHPExcel->getActiveSheet()->getStyle($rangeTotal)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle($rangeTotal)->getFont()->setSize(12);
$objPHPExcel->getActiveSheet()->getStyle($rangeTotal)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('f6f6f6f6');


// export ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$objPHPExcel->getActiveSheet()->setTitle("Summary of Stock Value");
$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment;filename=stock_value_".date('Ymd').".xlsx");
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
