<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel.php');
require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

if (isset($_REQUEST['archived'])) {
  $mode = 'archive';
}
else {
	$mode = 'active';
}

$settings = Settings::init();
$user = User::instance();

$translate = Translate::instance();

// load data: projects
$bindTables = array(
    'project_types' => Red_project::BIND_PROJECT_TYPES,
    'project_states' => Red_project::BIND_PROJECT_STATES,
    'project_sheets' => "LEFT OUTER JOIN db_retailnet_red.red_projectsheets ON red_project_id  = red_projectsheet_project_id",
);

//Get projects
//Show only active projects or archived projects resp.
if ($mode == 'active') {
  $filters['red_project_enddate'] = "(red_project_enddate = '0000-00-00' OR red_project_enddate = '' OR red_project_enddate IS NULL)";
}
elseif ($mode == 'archive') {
  $filters['red_project_enddate'] = "(red_project_enddate <> '0000-00-00' AND red_project_enddate <> '' AND red_project_enddate IS NOT NULL)";
}

//Define filters
if ($_REQUEST['search']) {
	$keyword = $_REQUEST['search'];
	$filters['search'] = "
			(red_project_title LIKE '%$keyword%'
			OR red_project_projectnumber LIKE '%$keyword%')
		";
	$print_filters[] = array('caption' => 'Keyword(s)', 'value' => $keyword);
}
if ($_REQUEST['project_types']) {
	$filters['project_types'] = 'red_project_projecttype_id = '.$_REQUEST['project_types'];
	$projecttype_filter = new Red_Project_Type('red');
	$projecttype_filter->read($_REQUEST['project_types']);
	$print_filters[] = array('caption' => 'Projecttype', 'value' => $projecttype_filter->data['red_projecttype_name'] );
}
if ($_REQUEST['project_states']) {
	$filters['project_states'] = 'red_project_projectstate_id = '.$_REQUEST['project_states'];
	$projectstate_filter = new Red_Project_State('red');
	$projectstate_filter->read($_REQUEST['project_states']);
	$print_filters[] = array('caption' => 'Projectstate', 'value' => $projectstate_filter->data['red_projectstate_name']);
}

//Permission Check
if (!($user->permission(Red_project::PERMISSION_VIEW_PROJECTS))) {
	$filters['no_access'] = '1 = 0';
}

$model = new Model(Connector::DB_RETAILNET_RED);
$projects = $model->query("
				  SELECT SQL_CALC_FOUND_ROWS
				    red_project_id,
				    red_project_title,
				    red_project_projectnumber,
				    red_project_projecttype_id,
				    red_project_projectstate_id,
				    DATE_FORMAT(red_project_startdate, '%d.%m.%Y') as red_project_startdate,
				    DATE_FORMAT(red_project_foreseen_enddate, '%d.%m.%Y') as red_project_foreseen_enddate,
				    red_project_enddate,
				    red_projecttype_name,
				    red_projectstate_name,
					IF(red_projectsheet_plannedamount_current_year = '0', NULL, red_projectsheet_plannedamount_current_year) as red_projectsheet_plannedamount_current_year,
					IF(red_projectsheet_totalbudget = '0', NULL, red_projectsheet_totalbudget) as red_projectsheet_totalbudget,
					IF(red_projectsheet_alreadycommitted = '0', NULL, red_projectsheet_alreadycommitted) as red_projectsheet_alreadycommitted,
					IF(red_projectsheet_alreadyspent = '0', NULL, red_projectsheet_alreadyspent) as red_projectsheet_alreadyspent
				  FROM red_projects
				 ")
->bind($bindTables)
->filter($filters)
->order('red_project_title')
->fetchAll();

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
			),
		),
	);

$document_title = 'RED - Active Projects';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setCreator("retailnet");
$objPHPExcel->getProperties()->setTitle($document_title);
$objPHPExcel->getProperties()->setSubject($document_title);
$objPHPExcel->getProperties()->setDescription($document_title);

//Writing table headers
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, 1)->getFont()->setSize(14);
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, 1)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'Retail Net: Environment Development');
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'List of Projects: '.date('d.m.Y H:i.s'));


$row = 4;
if (is_array($print_filters)) {
	foreach ($print_filters as $filter) {
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $filter['caption'].': '.$filter['value']);
		$row++;
	}
	$row++;
}

$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $translate->red_project_title);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $translate->red_project_projectnumber);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $translate->red_projecttype_name);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $translate->red_projectstate_name);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $translate->red_project_startdate);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $translate->red_project_foreseen_enddate);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $translate->red_project_enddate);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, 'Planned amount cur. year');
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $translate->total_budget);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, 'Already committed');
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, 'Already spent');
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $row, 'Budget still available');

//Set fill color
$objPHPExcel->getActiveSheet()->getStyle("A$row:L$row")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFEFEFEF');
//Set bold
$objPHPExcel->getActiveSheet()->getStyle("A$row:L$row")->getFont()->setBold(true);
//Set border
$objPHPExcel->getActiveSheet()->getStyle("A$row:L$row")->applyFromArray($styleArray);

//Set alignment for number cells and project Number(header only)
$objPHPExcel->getActiveSheet()->getStyle("B$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle(PHPExcel_Cell::stringFromColumnIndex(7).$row.":".PHPExcel_Cell::stringFromColumnIndex(11).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);


//Writing project data
$startRow = $row + 1;
$highestColumnIndex = 11;
$highestRow = count($projects) + $startRow;
$i=0;
for ($row = $startRow; $row < $highestRow; ++$row) {
  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $projects[$i]['red_project_title']);
  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $projects[$i]['red_project_projectnumber']);
  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $projects[$i]['red_projecttype_name']);
  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $projects[$i]['red_projectstate_name']);
  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $projects[$i]['red_project_startdate']);
  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $projects[$i]['red_project_foreseen_enddate']);
  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $projects[$i]['red_project_enddate']);

  $value = $projects[$i]['red_projectsheet_plannedamount_current_year'] == 0 ? '' : $projects[$i]['red_projectsheet_plannedamount_current_year'];
  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $projects[$i]['red_projectsheet_plannedamount_current_year']);
  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $row,  $projects[$i]['red_projectsheet_totalbudget']);
  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $row,  $projects[$i]['red_projectsheet_alreadycommitted']);
  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $row,  $projects[$i]['red_projectsheet_alreadyspent']);
  $still_available = (int)$projects[$i]['red_projectsheet_totalbudget'] - (int)$projects[$i]['red_projectsheet_alreadycommitted'];
  if ($still_available != 0) $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $row,  $still_available);

  //Set number format
  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7, $row)->getNumberFormat()->setFormatCode('#,##0.00');
  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(8, $row)->getNumberFormat()->setFormatCode('#,##0.00');
  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(9, $row)->getNumberFormat()->setFormatCode('#,##0.00');
  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(10, $row)->getNumberFormat()->setFormatCode('#,##0.00');
  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(11, $row)->getNumberFormat()->setFormatCode('#,##0.00');

  $i++;
}


//Set Border
$highest_cell_string = PHPExcel_Cell::stringFromColumnIndex($highestColumnIndex);
$highest_row = $row - 1;
$objPHPExcel->getActiveSheet()->getStyle("A$startRow:$highest_cell_string$highest_row")->applyFromArray($styleArray);


//Set column autosize
for ($column = 0; $column <= $highestColumnIndex; $column++) {
	$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
}




// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('RED - Active Projects');

$filename = 'red_projects_'.date('ymd').'.xlsx';
// redirect output to client browser
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

