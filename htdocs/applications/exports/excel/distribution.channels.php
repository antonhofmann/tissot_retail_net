<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	// execution time
	ini_set('max_execution_time', 120);
	ini_set('memory_limit', '1024M');
	
	$settings = Settings::init();
	$settings->load('data');
	$user = User::instance();
	$translate = Translate::instance();
	
	// request vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];

	// request
	$_REQUEST = session::filter($application, "$controller.$archived.$action", false);
	
	// page title
	$pagetitle = ($archived) ? 'Inactive Distribution Channels' : 'Distribution Channels';
	
	// file name
	$filename = "distribution_channels_".date('Y-m-d');
	
	// db model
	$model = new Model(Connector::DB_CORE);

	// sql order
	$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : '
		mps_distchannel_group_name, 
		mps_distchannel_code, 
		mps_distchannel_name
	';
	
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
		
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			mps_distchannel_group_name LIKE '%$keyword%' 
			OR mps_distchannel_code LIKE '%$keyword%' 
			OR mps_distchannel_name LIKE '%$keyword%'
		)";
		
		$captions[] = $translate->search.': '.$keyword;
	}

	// filter: turnover groups
	if ($_REQUEST['channel_groups']) {
		
		$filters['channel_groups'] = "mps_distchannel_group_id = ".$_REQUEST['channel_groups'];
		
		$distributionGroup = new DistChannelGroup();
		$distributionGroup->read($_REQUEST['channel_groups']);
		
		$captions[] = $translate->mps_distchannel_group_name.': '.$distributionGroup->name;
	}
	
	// datagrid
	$result = $model->query("
		SELECT 
			mps_distchannel_id,
			mps_distchannel_group_name,
			mps_distchannel_code,
			mps_distchannel_name
		FROM mps_distchannels
	")
	->bind('INNER JOIN mps_distchannel_groups ON mps_distchannels.mps_distchannel_group_id = mps_distchannel_groups.mps_distchannel_group_id')
	->filter($filters)
	->order($order, $direction)
	->fetchAll();
	
	if ($result) {
		
		require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
		require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

		$datagrid = _array::datagrid($result);
		$columns = array_keys(end($datagrid));
		$totalColumns = count($columns);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle($pagetitle);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);


		$border = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
		);

		$styleNumber = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
		);


		$styleString = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		);

		$styleHeaderString = array(
			'font' => array(
				'name'=>'Arial',
				'size'=> 11,
				'bold'=>true
			),
			'alignment' => array(
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array(
					'rgb'=>'EEEEEE'
				)
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);


		$styleHeaderNumber = array(
			'font' => array(
				'name'=>'Arial',
				'size'=> 11,
				'bold'=>true
			),
			'alignment' => array(
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true,
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array(
					'rgb'=>'EEEEEE'
				)
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);

		
		// cel dimensions
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$lastColumn = "C";


		$row=1;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", $pagetitle);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(18);

		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: ".date('d.m.Y  H:i:s'));

		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		// filter captions
		if ($captions) {
			
			foreach ($captions as $caption) {
				$row++;
				$range = "A$row:".$lastColumn.$row;
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				$objPHPExcel->getActiveSheet()->setCellValue("A$row", $caption);
				$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
			}
		}

		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		$row++;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		// columns
		$row++;
		$col = 0;
		$range = "A$row:".$lastColumn.$row;

		foreach ($columns as $value) {
			$index = spreadsheet::index($col, $row);
			$styling = ($col>5) ? $styleHeaderNumber : $styleHeaderString;
			$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styling);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $translate->$value);
			$col++;
		}


		$row++;

		foreach ($datagrid as $i => $array) {
			
			$col = 0;
			
			foreach($array as $key => $value) {
				$index = spreadsheet::index($col, $row);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
				$col++;
		    }

			$range = "A$row:".$lastColumn.$row;
		    $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($border);
		    $row++;
		}

		$row++;
		$range = "A$row:".$lastColumn.$row;
		$range = "A$row:".$lastColumn.$row;
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		$objPHPExcel->getActiveSheet()->setTitle($pagetitle);
		$objPHPExcel->setActiveSheetIndex(0);

		// redirect output to client browser
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}
	else {
			message::empty_result();
			url::redirect("/$application/$controller$archived/$action");
	}
	