<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	// execution time
	ini_set('max_execution_time', 600);
	ini_set('memory_limit', '102400M');
	
	$settings = Settings::init();
	$settings->load('data');
	$user = User::instance();
	$translate = Translate::instance();
	
	// request vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['ordersheet'];
	$version = $_REQUEST['version'];
	
	// order Sheet
	$ordersheet = new Ordersheet($application);
	$ordersheet->read($id);
	
	// page title
	$pagetitle = 'Order Sheet SAP Export';
	
	// is archived
	$isDistributed = ($ordersheet->state()->isDistributed() && $ordersheet->isDistributed()) ? true : false;
	
	// db model
	$model = new Model($application);
	
	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Global Properties
	Set editabled and visibility vars
********************************************************************************************************************************************************************************************/
	
	// planning column keyword
	define(COLUMN_DISTRIBUTED, 'distributed');
	
	// distribution column keyword
	define(COLUMN_SAP_CONFIRMED, 'sap-confirmed');
	
	// partially confirmed distribution keyword
	define(COLUMN_SAP_SHIPPED, 'sap-shipped');
	
	// spreadsheet readonly
	define('CELL_READONLY', 'true');
	
	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Filters
********************************************************************************************************************************************************************************************/
	
	$filters = array();
	$filer_labels = array();
	
	// filter: pos
	$filters['default'] = "posaddress_client_id = ".$ordersheet->address_id;
	
	// filter: distribution channels
	if ($_REQUEST['distribution_channels']) {
		$filters['distribution_channels'] = 'posaddress_distribution_channel = '.$_REQUEST['distribution_channels'];
		$filter_active = 'active';
	}
	
	// filter: turnover classes
	if ($_REQUEST['turnoverclass_watches']) {
		$filters['turnoverclass_watches'] = 'posaddress_turnoverclass_watches = '.$_REQUEST['turnoverclass_watches'];
		$filter_active = 'active';
	}
	
	// filter: sales represenatives
	if ($_REQUEST['sales_representative']) {
		$filters['sales_representative'] = 'posaddress_sales_representative = '.$_REQUEST['sales_representative'];
		$filter_active = 'active';
	}
	
	// filter: decoration persons
	if ($_REQUEST['decoration_person']) {
		$filters['decoration_person'] = 'posaddress_decoration_person = '.$_REQUEST['decoration_person'];
		$filter_active = 'active';
	}
	
	// provinces
	if ($_REQUEST['provinces']) {
		$filters['provinces'] = 'province_id = '.$_REQUEST['provinces'];
		$filter_active = 'active';
	}
	
	// for archived ordersheets show only POS locations which has distributed quantities 
	if ($isDistributed) {
		
		$result = $model->query("
			SELECT DISTINCT 
				mps_ordersheet_item_delivered_posaddress_id AS pos
			FROM mps_ordersheet_item_delivered	
			INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id
			WHERE mps_ordersheet_item_ordersheet_id = $ordersheet->id AND mps_ordersheet_item_delivered_posaddress_id > 0
		")->fetchAll();
		
		if ($result) {
			
			$array = array();
			
			foreach ($result as $row) {
				$array[] = $row['pos'];
			}

			$filters['active'] = 'posaddress_id IN ('.join(',', $array).')';
		}
	}
	// otherwise show all active pos locations
	else {
		$filters['active'] = "(
			posaddress_store_closingdate = '0000-00-00'
			OR posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = ''
		)";
	}
	
	
/*******************************************************************************************************************************************************************************************
	Dataloader
	Distribution Channels
********************************************************************************************************************************************************************************************/

	$formFilter = array();
	
	// load distribution channels
	$result = $model->query("
		SELECT DISTINCT
			mps_distchannel_id,
			CONCAT(mps_distchannel_name, ' - ', mps_distchannel_code) AS distribution_channel
		FROM db_retailnet.posaddresses
	")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->bind(Pos::DB_BIND_DISTRIBUTION_CHANNELS)
	->filter($filters)
	->exclude('distribution_channels')
	->order('mps_distchannel_name, mps_distchannel_code')
	->fetchAll();
	
	if ($result) {
		
		$result = _array::extract($result);
		
		$formFilter[] = ui::dropdown($result, array(
			'id' => 'distribution_channels',
			'name' => 'distribution_channels',
			'value' => $_REQUEST['distribution_channels'],
			'caption' => $translate->all_distribution_channels
		));
		
		// show active filter labels
		if ($_REQUEST['distribution_channels']) {
			$filer_labels[] = $result[$_REQUEST['distribution_channels']];	
		}
	}
	

/*******************************************************************************************************************************************************************************************
	Dataloader
	Turnover Class Watches
********************************************************************************************************************************************************************************************/
	
	$result = $model->query("
		SELECT DISTINCT
			mps_turnoverclass_id, mps_turnoverclass_name
		FROM db_retailnet.posaddresses
	")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->bind(Pos::DB_BIND_TURNOVER_CLASS_WATCHES)
	->filter('company', "posaddress_franchisee_id = ".$ordersheet->address_id)
	->order('mps_turnoverclass_name')
	->fetchAll();
	
	if ($result) {
		
		$result = _array::extract($result);
		
		$formFilter[] = ui::dropdown($result, array(
			'name' => 'turnoverclass_watches',
			'id' => 'turnoverclass_watches',
			'value' => $_REQUEST['turnoverclass_watches'],
			'caption' => $translate->all_turnover_class_watches
		));
		
		// show active filter labels
		if ($_REQUEST['turnoverclass_watches']) {
			$filer_labels[] = $result[$_REQUEST['turnoverclass_watches']];
		}
	}

	
/*******************************************************************************************************************************************************************************************
	Dataloader
	Sales Representative
********************************************************************************************************************************************************************************************/
	
	$model->query("
		SELECT DISTINCT
			mps_staff_id,
			CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS name
		FROM db_retailnet.posaddresses
	")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->bind(Pos::DB_BIND_MPS_STAFF_SALES_REPRESENTATIVES)
	->filter($filters)
	->filter('staff_type', 'mps_staff_staff_type_id = 1')
	->exclude('sales_representative')
	->order('mps_staff_name, mps_staff_firstname');
	
	if ($ordersheet->state()->owner) {
		$model->filter('limited', 'mps_staff_address_id='.$user->address);
	}
	
	$result = $model->fetchAll();

	if ($result) {
		
		$result = _array::extract($result);
		
		$formFilter[] = ui::dropdown($result, array(
			'name' => 'sales_representative',
			'id' => 'sales_representative',
			'value' => $_REQUEST['sales_representative'],
			'caption' => $translate->all_sales_representatives
		));
		
		// show active filter labels
		if ($_REQUEST['sales_representative']) {
			$filer_labels[] = $result[$_REQUEST['sales_representative']];
		}
	}
	

/*******************************************************************************************************************************************************************************************
	Dataloader
	Deecoration Persons
********************************************************************************************************************************************************************************************/
	
	$model->query("
		SELECT DISTINCT
			mps_staff_id,
			CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS name
		FROM db_retailnet.posaddresses
	")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->bind(Pos::DB_BIND_MPS_STAFF_DECORATOR_PERSONS)
	->filter($filters)
	->filter('staff_type', 'mps_staff_staff_type_id = 2')
	->exclude('decoration_person')
	->order('mps_staff_name, mps_staff_firstname');
	
	if ($ordersheet->state()->owner) {
		$model->filter('limited', 'mps_staff_address_id='.$user->address);
	}
	
	$result = $model->fetchAll();

	if ($result) {
		
		$result = _array::extract($result);
		
		$formFilter[] = ui::dropdown($result, array(
			'name' => 'decoration_person',
			'id' => 'decoration_person',
			'value' => $_REQUEST['decoration_person'],
			'caption' => $translate->all_decoration_persons
		));
		
		// show active filter labels
		if ($_REQUEST['decoration_person']) {
			$filer_labels[] = $result[$_REQUEST['decoration_person']];
		}
	}
	
	
/*******************************************************************************************************************************************************************************************
	Dataloader
	Provinces
********************************************************************************************************************************************************************************************/
	
	$result = $model->query("
		SELECT DISTINCT
			province_id, province_canton
		FROM db_retailnet.posaddresses")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->filter($filters)
	->exclude('provinces')
	->order('province_canton')
	->fetchAll();
	
	if ($result) {
		
		$result = _array::extract($result);
		
		$formFilter[] = ui::dropdown($result, array(
			'name' => 'provinces',
			'id' => 'provinces',
			'value' => $_REQUEST['provinces'],
			'caption' => $translate->all_provinces
		));
		
		// show active filter labels
		if ($_REQUEST['provinces']) {
			$filer_labels[] = $result[$_REQUEST['provinces']];
		}
	}
	
	
/*******************************************************************************************************************************************************************************************
	Dataloader
	POS Locations
********************************************************************************************************************************************************************************************/
	
	$posLocations = array();
	
	$pos_filter = $filters ? join(' AND ', array_values($filters)) : null;
	
	$result = $model->query("
		SELECT DISTINCT
			posaddress_id,
			posaddress_name,
			posaddress_place,
			postype_id,
			postype_name
		FROM db_retailnet.posaddresses
		INNER JOIN db_retailnet.addresses ON address_id = posaddress_client_id
		INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
		INNER JOIN db_retailnet.provinces ON province_id = place_province
		INNER JOIN db_retailnet.postypes ON postype_id = posaddress_store_postype
		WHERE $pos_filter
		ORDER BY posaddress_name, posaddress_place
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$pos = $row['posaddress_id'];
			$type = $row['postype_id'];
			
			$posLocations[$type]['caption'] = $row['postype_name'];
			$posLocations[$type]['pos'][$pos]['name'] = $row['posaddress_name'];
			$posLocations[$type]['pos'][$pos]['place'] = $row['posaddress_place'];
		}
	}
	
/*******************************************************************************************************************************************************************************************
	Dataloader
	Order Sheet Items
********************************************************************************************************************************************************************************************/
	
	$ordersheetItems = array();
	
	$result = $model->query("
		SELECT
			mps_ordersheet_item_id,
			mps_ordersheet_item_material_id,
			mps_ordersheet_item_quantity_distributed,
			mps_ordersheet_item_purchase_order_number,
			mps_material_planning_type_id,
			mps_material_planning_type_name,
			mps_material_collection_category_id,
			mps_material_collection_category_code,
			mps_material_id,
			mps_material_code,
			mps_material_name,
			mps_material_ean_number
		FROM mps_ordersheet_items
		INNER JOIN mps_ordersheets ON  mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
		INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
		LEFT JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id
		LEFT JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id
		WHERE 
			mps_ordersheet_id = $ordersheet->id
			AND mps_ordersheet_item_quantity_shipped > 0
		ORDER BY
			mps_material_planning_type_name,
			mps_material_collection_category_code,
			mps_material_code,
			mps_material_name
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$item = $row['mps_ordersheet_item_id'];
			$material = $row['mps_ordersheet_item_material_id'];
			$planning = $row['mps_material_planning_type_id'];
			$collection = $row['mps_material_collection_category_id'];
			
			$ordersheetItems[$planning]['caption'] = $row['mps_material_planning_type_name'];
			$ordersheetItems[$planning]['collections'][$collection]['caption'] = $row['mps_material_collection_category_code'];
			$ordersheetItems[$planning]['collections'][$collection]['items'][$item]['id'] = $row['mps_material_id'];
			$ordersheetItems[$planning]['collections'][$collection]['items'][$item]['code'] = $row['mps_material_code'];
			$ordersheetItems[$planning]['collections'][$collection]['items'][$item]['name'] = $row['mps_material_name'];
			$ordersheetItems[$planning]['collections'][$collection]['items'][$item]['quantity'] = $row['mps_ordersheet_item_quantity_distributed'];
		}
	}
	

/*******************************************************************************************************************************************************************************************
	Dataloader
	Order Sheet Items Delivered Quantities
********************************************************************************************************************************************************************************************/

	$distributedItems = array();
	$distributedPosItems = array();
	$distributedWarehouseItems = array();
	
	$result = $model->query("
		SELECT DISTINCT
			mps_ordersheet_item_delivered_id,
			mps_ordersheet_item_delivered_posaddress_id,
			mps_ordersheet_item_delivered_warehouse_id,
			mps_ordersheet_item_delivered_quantity,
			mps_ordersheet_item_delivered_confirmed,
			mps_ordersheet_item_id,
			mps_ordersheet_item_material_id,
			mps_material_material_planning_type_id,
			mps_material_material_collection_category_id,
			mps_ordersheet_warehouse_stock_reserve,
			posaddress_sapnumber,
			address_warehouse_sap_customer_number
		FROM mps_ordersheet_item_delivered
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id
		INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
		LEFT JOIN mps_ordersheet_warehouses ON mps_ordersheet_warehouse_id = mps_ordersheet_item_delivered_warehouse_id
		LEFT JOIN db_retailnet.posaddresses ON posaddress_id = mps_ordersheet_item_delivered_posaddress_id
		LEFT JOIN db_retailnet.address_warehouses ON address_warehouse_id = mps_ordersheet_warehouse_address_warehouse_id
		WHERE
			mps_ordersheet_item_ordersheet_id = $ordersheet->id 
			AND mps_ordersheet_item_delivered_confirmed IS NOT NULL
		ORDER BY 
			mps_ordersheet_item_delivered_confirmed DESC,
			mps_ordersheet_item_delivered.date_created DESC
	")->fetchAll();
	
	if ($result) { 

		foreach ($result as $row) {

			$id = $row['mps_ordersheet_item_delivered_id'];
			$material = $row['mps_ordersheet_item_material_id'];
			$item = $row['mps_ordersheet_item_id'];
			$pos = $row['mps_ordersheet_item_delivered_posaddress_id'];
			$warehouse = $row['mps_ordersheet_item_delivered_warehouse_id'];
			$quantity = $row['mps_ordersheet_item_delivered_quantity'];
			
			$distributedItems[$id] = $row;
			
			// pos quantities
			if ($pos) {
				$distributedPosItems[$item][$pos] = $distributedPosItems[$item][$pos] + $quantity;				
			}
			
			// warehouse quantities
			if ($warehouse && !$row['mps_ordersheet_warehouse_stock_reserve']) {
				$distributedWarehouseItems[$item][$warehouse] = $distributedWarehouseItems[$item][$warehouse] + $quantity;
			}
		}
	}
	
	// all involved distributions
	$involvedDistributedItems = $distributedItems ? join(',', array_filter(array_keys($distributedItems))) : null;
	
	
/*******************************************************************************************************************************************************************************************
	SAP confirmed items
********************************************************************************************************************************************************************************************/
	
	$sapPosConfirmedItems = array();
	$sapWarehouseConfirmedItems = array();
	
	$result = $model->query("
		SELECT * 
		FROM sap_confirmed_items
		WHERE sap_confirmed_item_mps_distribution_id IN ($involvedDistributedItems)
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$id = $row['sap_confirmed_item_mps_distribution_id'];
			$item = $distributedItems[$id]['mps_ordersheet_item_id'];
			$pos = $distributedItems[$id]['mps_ordersheet_item_delivered_posaddress_id'];
			$warehouse = $distributedItems[$id]['mps_ordersheet_item_delivered_warehouse_id'];
			$quantity = $row['sap_confirmed_item_confirmed_quantity'];
			
			if ($pos) {
				$sapPosConfirmedItems[$item][$pos] = $sapPosConfirmedItems[$item][$pos] + $quantity;
			} elseif($warehouse) {
				$sapWarehouseConfirmedItems[$item][$warehouse] = $sapWarehouseConfirmedItems[$item][$warehouse] + $quantity;
			}
		}
	}
	
	
/*******************************************************************************************************************************************************************************************
	SAP shipped items
********************************************************************************************************************************************************************************************/
	
	$sapPosShippedItems = array();
	$sapWarehouseShippedItems = array();
	
	$result = $model->query("
		SELECT * 
		FROM sap_shipped_items
		WHERE sap_shipped_item_mps_distribution_id IN ($involvedDistributedItems)
	")->fetchAll();
	
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$id = $row['sap_shipped_item_mps_distribution_id'];
			$item = $distributedItems[$id]['mps_ordersheet_item_id'];
			$pos = $distributedItems[$id]['mps_ordersheet_item_delivered_posaddress_id'];
			$warehouse = $distributedItems[$id]['mps_ordersheet_item_delivered_warehouse_id'];
			$quantity = $row['sap_shipped_item_quantity'];
			
			if ($pos) {
				$sapPosShippedItems[$item][$pos] = $sapPosShippedItems[$item][$pos] + $quantity;
			} elseif ($warehouse) {
				$sapWarehouseShippedItems[$item][$warehouse] = $sapWarehouseShippedItems[$item][$warehouse] + $quantity;
			}
		}
	}
	

/*******************************************************************************************************************************************************************************************
	Fixed Area
********************************************************************************************************************************************************************************************/
	
	$rowFixed = 6;
	$rowFixedTotal = $rowFixed;
	$rowFixedFirst = 1;
	$rowFixedLast = $rowFixedTotal;

	$colFiexed = 1;
	$colFiexedTotal = $colFiexed;
	$colFixedFirst = 1;
	$colFixedLast = $colFiexedTotal;
	
	$rowSeparatorFixed = 0;
	$rowSeparatorFixedArea = $rowSeparatorFixed ? $rowFixedTotal+1 : null;
	
/*******************************************************************************************************************************************************************************************
	Calculated Area
********************************************************************************************************************************************************************************************/
	
	$rowCalculated = array();
	
	// totals
	$rowCalculated[0] = "Exported / SAP Confirmed / SAP Shipped";
	
	// total calculated rows
	$rowCalculatedTotal = count($rowCalculated);
	
	// calcullated rows
	if ($rowCalculatedTotal) {
		$step = $rowSeparatorFixedArea ? 2 : 1;
		$rowCalculatedFirst = $rowFixedLast ? $rowFixedLast+$step : null;
		$rowCalculatedLast  = $rowCalculatedFirst ? $rowCalculatedFirst+$rowCalculatedTotal-1 : null;
	}
	
	// row separator calculated area
	$rowSeparatorCalculated = 1;
	$rowSeparatorCalculatedArea = $rowCalculatedLast && $rowSeparatorCalculated ? $rowCalculatedLast+1 : 0;

/*******************************************************************************************************************************************************************************************
	Datagrid Rows
********************************************************************************************************************************************************************************************/
	
	$row=0;
	
	$rowDataGrid = array();
	$rowDataGridSeparator = 0;
	$group = array();
	
	// row group separators
	$rowGroupSeparator = 1;
	$rowGroupSeparators = 0;
	
	if ($posLocations) {
	
		foreach ($posLocations as $type => $data) {
	
			$rowDataGrid[$row]['type'] = $type;
			$rowDataGrid[$row]['caption'] = str_replace("'", "`", $data['caption']);
			$rowDataGrid[$row]['group'] = true;
	
			$row++;
	
			foreach ($data['pos'] as $pos => $value) {
	
				$rowDataGrid[$row]['type'] = $type;
				$rowDataGrid[$row]['pos'] = $pos;
	
				$caption = $value['name'].' ('.$value['place'].')';
				$caption = str_replace("'", "", $caption);
				$rowDataGrid[$row]['caption'] = (strlen($caption) > 50) ? substr($caption, 0, 50).'..' : $caption;
				$row++;
			}
	
			$row = $row + $rowGroupSeparator;
	
			$rowGroupSeparators = $rowGroupSeparators+$rowGroupSeparator;
		}
	
		// remove last seperator
		$rowGroupSeparators = ($rowGroupSeparators) ? $rowGroupSeparators-1 : $rowGroupSeparators;
	}
	
	// total row references
	$rowDataGridTotal = ($rowDataGrid) ? count($rowDataGrid) + $rowGroupSeparators + $rowDataGridSeparator : 0;
	
	// referenced rows
	if ($rowDataGridTotal) {
		$step = $rowSeparatorCalculatedArea ? 2 : 1;
		$rowDataGridFirst = $rowCalculatedLast ? $rowCalculatedLast+$step : null;
		$rowDataGridLast  = $rowDataGridFirst ? $rowDataGridFirst+$rowDataGridTotal-1 : null;
	}
	
	// row separator datagrid area
	$rowSeparatorDatagrid = 1;
	$rowSeparatorDatagridArea = $rowSeparatorDatagrid && $rowDataGridLast ? $rowDataGridLast+1 : 0;

/*******************************************************************************************************************************************************************************************
	Datagrid Columns
********************************************************************************************************************************************************************************************/
	
	$col=0;
	
	$colDataGrid = array();
	$planningTypes = array();
	$collectionCategories = array();
	
	// column constants
	$coltrigger = 0;
	$colGroupSeparator = 1;
	$colGroupSeparators = 0;
	$columnPerItem = 3;
	
	if ($ordersheetItems) {
	
		foreach ($ordersheetItems as $i => $planning) {
			
			$totalPlanningCollections = count($planning['collections']);
				
			foreach ($planning['collections'] as $j => $collection) {
				
				$totalCollectionItems = count($collection['items']);
				
				// planning type
				$planningTypes[$col]['caption'] = str_replace("'", "'", $planning['caption']);
				$planningTypes[$col]['merge'] = $totalPlanningCollections * $totalCollectionItems * $columnPerItem;
				
				// collection category
				$collectionCategories[$col]['caption'] = str_replace("'", "'", $collection['caption']);
				$collectionCategories[$col]['merge'] = $totalCollectionItems * $columnPerItem;
	
				foreach ($collection['items'] as $k => $item) {
						
					// skip separator columns
					if ($col==$coltrigger) {
							
						// item caption
						$colDataGrid[$col]['merge'] = $columnPerItem;
						$colDataGrid[$col]['caption'] = $item['code'];
							
						// next item index
						$coltrigger = $col+$columnPerItem;
					
						$sumPos = is_array($distributedPosItems[$k]) ? array_sum($distributedPosItems[$k]) : 0;
						$sumWarehouse = is_array($distributedWarehouseItems[$k]) ? array_sum($distributedWarehouseItems[$k]) : 0;
						
						// distributed quantites
						$colDataGrid[$col]['key'] = COLUMN_DISTRIBUTED;
						$colDataGrid[$col]['id'] = $k;
						$colDataGrid[$col]['quantity'] = $sumPos+$sumWarehouse ?: null;
						$col++;
		
						$sumPos = is_array($sapPosConfirmedItems[$k]) ? array_sum($sapPosConfirmedItems[$k]) : 0;
						$sumWarehouse = is_array($sapWarehouseConfirmedItems[$k]) ? array_sum($sapWarehouseConfirmedItems[$k]) : 0;
	
						// sap confirmed quantite
						$colDataGrid[$col]['key'] = COLUMN_SAP_CONFIRMED;
						$colDataGrid[$col]['id'] = $k;
						$colDataGrid[$col]['quantity'] = $sumPos+$sumWarehouse ?: null;
						$col++;
						
						$sumPos = is_array($sapPosShippedItems[$k]) ? array_sum($sapPosShippedItems[$k]) : 0;
						$sumWarehouse = is_array($sapWarehouseShippedItems[$k]) ? array_sum($sapWarehouseShippedItems[$k]) : 0;
	
						// sap shipped items
						$colDataGrid[$col]['key'] = COLUMN_SAP_SHIPPED;
						$colDataGrid[$col]['id'] = $k;
						$colDataGrid[$col]['quantity'] =  $sumPos+$sumWarehouse ?: null;
						$col++;
					}
						
					// next first item index
					$coltrigger = $col;
				}
	
				// increment
				$col = $col+$colGroupSeparator;
	
				// next first item index
				$coltrigger = $col;
	
				// colgroup separator
				$colGroupSeparators = $colGroupSeparators + $colGroupSeparator;
			}
		}
	
		// remove last seperator
		$colGroupSeparators = ($colGroupSeparators) ? $colGroupSeparators-1 : $colGroupSeparators;
	}
	
	// totall column references
	$colDataGridTotal = ($colDataGrid) ? count($colDataGrid) + $colGroupSeparators : 0;
	
	// referenced columns
	if ($colDataGridTotal) {
		$colDataGridFirst = $colFiexedTotal + $colCalculatedTotal + 1;
		$colDataGridLast = $colDataGridFirst + $colDataGridTotal - 1;
	}

	
/*******************************************************************************************************************************************************************************************
	Extended Rows (Order sheet warehouses)
********************************************************************************************************************************************************************************************/
	
	$rowExtended = array();
	
	// load all ordersheet registred warehouses
	$ordersheetWarehouses = $ordersheet->warehouse()->load();
	
	if ($ordersheetWarehouses) {
		
		$row=0;
		
		$rowExtended[$row] = "Warehouses";
		
		// load all active company warehouses
		$companyWarehouses = $ordersheet->company()->warehouse()->load();
		$companyWarehouses = _array::datagrid($companyWarehouses);
		
		foreach ($ordersheetWarehouses as $item) {
			
			$warehouse = $item['mps_ordersheet_warehouse_address_warehouse_id'];

			if (!$item['mps_ordersheet_warehouse_stock_reserve']) {
				$row++;
				$rowExtended[$row]['id'] = $item['mps_ordersheet_warehouse_id'];
				$rowExtended[$row]['warehouse'] = $warehouse;
				$rowExtended[$row]['name'] = $companyWarehouses[$warehouse]['address_warehouse_name'];
			}
		}
	}
	
	$rowExtendedTotal = count($rowExtended);

	// row limits
	if ($rowExtendedTotal > 1) {
		$step = $rowSeparatorDatagrid ? 2 : 1;
		$rowExtendedFirst = $rowDataGridLast ? $rowDataGridLast+$step : null;
		$rowExtendedLast  = $rowExtendedFirst ? $rowExtendedFirst+$rowExtendedTotal-1 : null;
	}
	
	// row separator datagrid area
	$rowSeparatorExtended = 0;
	$rowSeparatorExtendedArea = $rowSeparatorExtended && $rowExtendedLast ? $rowExtendedLast+1 : null;
	
/*******************************************************************************************************************************************************************************************
	PHPExcel BUILDER
********************************************************************************************************************************************************************************************/
	
	
	require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
	require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle($pagetitle);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

	
	$styles = array(
		'borders' => array(
			'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
		),
		'planning-type' => array(
			'borders' => array(
					'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			),
			'font' => array('name'=>'Arial','size'=> 14, 'bold'=>true),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'EFEFEF')
			)
		),
		'collection-category' => array(
			'borders' => array(
				'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			),
			'font' => array(
				'name'=>'Arial',
				'size'=> 12, 
				'bold'=>true,
				'color' => array(
					'rgb' => '666666'
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'EFEFEF')
			)
		),
		'item' => array(
			'borders' => array(
				'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			),
			'font' => array(
				'name'=>'Arial',
				'size'=> 12
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			)
		),
		COLUMN_DISTRIBUTED => array(
			'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'f4fced')
			)
		),
		COLUMN_SAP_CONFIRMED => array(
			'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'DBEFFF')
			)
		),
		COLUMN_SAP_SHIPPED => array(
			'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'FFFAD6')
			)
		),
		'pos-type' => array(
			'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'font' => array(
				'bold'=>true,
				'size'=> 12
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'EFEFEF')
			)
		),
		'pos-name' => array(
			'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			)
		),
		'label' => array(
			'font' => array(
				'bold'=>true,
				'size'=> 12
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			)
		)
	);
	

/*******************************************************************************************************************************************************************************************
	SPREADSHEED BUILDER
********************************************************************************************************************************************************************************************/

	$debug = false;
	
	$rowSeparatorsTotal = $rowSeparatorFixed+$rowSeparatorCalculated+$rowSeparatorDatagrid+$rowSeparatorExtended;
	$colSeparatorsTotal = $colSeparatorFixed+$colSeparatorCalculated+$colSeparatorDatagrid+$colSeparatorExtended;
	$rowSpreadSheetTotal = $rowFixedTotal+$rowCalculatedTotal+$rowDataGridTotal+$rowExtendedTotal+$rowSeparatorsTotal;
	$colSpreadSheetTotal = $colFiexedTotal+$colCalculatedTotal+$colDataGridTotal+$colExtendedTotal+$colSeparatorsTotal;
	
	if ($rowSpreadSheetTotal && $colSpreadSheetTotal) {
		
		$export = true;
		
		$step_max = 3;
		
		$lastColumn = spreadsheet::key($colSpreadSheetTotal);
	
		for ($row=1; $row <= $rowSpreadSheetTotal; $row++) {
				
			for ($col=1; $col <= $colSpreadSheetTotal; $col++) {
				
				$column = spreadsheet::key($col);
				$index = $column.$row;
				$step = ($step==$step_max) ? 1 : $step;

				switch ($row) {
					
					case 1: // spreadsheet title
						
						if ($col==1) {
							
							// column dimension
							$objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth(50);
								
							// merge cells
							$objPHPExcel->getActiveSheet()->mergeCells("$index:$lastColumn$row");
								
							// content & styling
							$objPHPExcel->getActiveSheet()->setCellValue($index, $ordersheet->header());
							$objPHPExcel->getActiveSheet()->getStyle($index)->getFont()->setBold(true);
							$objPHPExcel->getActiveSheet()->getStyle($index)->getFont()->setSize(20);
							
							$console[] = "$row:$col  Spreadsheet Header";
						}
					
					break;
					
					case 2: // request filters
					
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->mergeCells("$index:$lastColumn$row");
							$console[] = "$row:$col  Merge Filters Row";
						}
					
					break;
					
					case 3: // separator
					
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->mergeCells("$index:$lastColumn$row");
							$console[] = "$row:$col Header Separator";
						}
					
					break;
					
					case 4; // planning type captions
						
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(40);
							$console[] = "$row:$col Set Planing Type row height";
						}
						elseif ($col >= $colDataGridFirst && $col <= $colDataGridLast) {
						
							// set column dimension
							$cWidth = ($col==$sColumn) ? 5 : 10;
							$objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth($cWidth);
						
							$j = $col-$colDataGridFirst;
							$caption = $planningTypes[$j]['caption'];
						
							if ($caption) {
						
								$sIndex = $index;
								
								if ($planningTypes[$j]['merge'] > 1) {
									$mergeCell = spreadsheet::index($j+$planningTypes[$j]['merge']);
									$sIndex = $index.':'.$mergeCell.$row;
									$objPHPExcel->getActiveSheet()->mergeCells($sIndex);
									$console[] = "$row:$col Merge Planing type cels";
								}
									
								// content
								$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
								$objPHPExcel->getActiveSheet()->getStyle($sIndex)->applyFromArray($styles['planning-type'], false);
								$console[] = "$row:$col Set planing type caption: $caption";
						
								// next colgroup seperator
								$sColumn = $col + $planningTypes[$j]['merge'];
							}
						
							// merge colgroup columns
							if ($col==$sColumn) {
								//$objPHPExcel->getActiveSheet()->mergeCells("$index:$column$rowSpreadSheetTotal");
							}
						}
						
					break;
					
					case 5; // collection category
					
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(25);
							$console[] = "$row:$col Set collection category row height";
						}
						elseif ($col >= $colDataGridFirst && $col <= $colDataGridLast) {
								
							$j = $col-$colDataGridFirst;
							
							if ($collectionCategories[$j]) {
						
								$caption = $collectionCategories[$j]['caption'];
								$mergeLength = $collectionCategories[$j]['merge'];
								
								$sIndex = $index;
									
								// merge planning type cell
								if ($mergeLength > 1) {
									$mergeCell = spreadsheet::index($j+$mergeLength);
									$sIndex = $index.':'.$mergeCell.$row;
									$objPHPExcel->getActiveSheet()->mergeCells($sIndex);
									$console[] = "$row:$col Merge collection category cells";
								}
									
								// content
								$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
								$objPHPExcel->getActiveSheet()->getStyle($sIndex)->applyFromArray($styles['collection-category'], false);
								$console[] = "$row:$col Set collection category caption: $caption";
							}
						}
					
					break;
					
					case 6; // item code
					
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(25);
						}
						elseif ($col >= $colDataGridFirst && $col <= $colDataGridLast) {
								
							$j = $col-$colDataGridFirst;
							
							if ($colDataGrid[$j]) {
						
								$caption = $colDataGrid[$j]['caption'];
								$mergeLength = $colDataGrid[$j]['merge'];
								$sIndex = $index;
									
								// merge planning type cell
								if ($mergeLength > 1) {
									$mergeCell = spreadsheet::index($j+$mergeLength);
									$sIndex = $index.':'.$mergeCell.$row;
									$objPHPExcel->getActiveSheet()->mergeCells($sIndex);
									$console[] = "$row:$col Merge Item cells";
								}
								
								if ($caption) {
									$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
									$objPHPExcel->getActiveSheet()->getStyle($sIndex)->applyFromArray($styles['item'], false);
									$console[] = "$row:$col Set Item caption: $caption";
								}
							}
						}
					
					break;
					
					case $rowSeparatorFixedArea; // fixed area separator
					
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->mergeCells($index.':'.$lastColumn.$row);
							$console[] = "$row:$col Merge fixed area separator row";
						}
					
					break;
					
					case $rowCalculatedFirst: // calculated row
						
						if ($col==1) {
							$caption = "Exported / SAP Confirmed / SAP Shipped";
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(25);
							$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['label'], false);
							$console[] = "$row:$col Set calculated row caption: $caption";
						}
						elseif ($col >= $colDataGridFirst && $col <= $colDataGridLast) {
						
							$j = $col-$colDataGridFirst;
								
							if ($colDataGrid[$j]) {
								$key = $colDataGrid[$j]['key'];
								$quantity = $colDataGrid[$j]['quantity'];
								$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);								
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles[$key], false);
								$console[] = "$row:$col Set total quantity for column $key";
							}
						}
						
					break;
					
					case $rowSeparatorCalculatedArea: // calculated area separator
						
						if ($col==1) {
							$console[] = "$row:$col Merge calculation separator cells";
							$objPHPExcel->getActiveSheet()->mergeCells($index.':'.$lastColumn.$row);
						}
						
					break;
					
					case $row >= $rowDataGridFirst && $row <= $rowDataGridLast: // datagrid
						
						if ($col==1) {

							$i = $row-$rowDataGridFirst;
								
							if ($rowDataGrid[$i]['group']) {
								$caption = $rowDataGrid[$i]['caption'];
								$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(25);
								$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['pos-type'], false);
								$console[] = "$row:$col Set Pos Type caption: $caption";
							} 
							elseif ($rowDataGrid[$i]['pos']) {
								$caption = $rowDataGrid[$i]['caption'];
								$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['pos-name'], false);
								$console[] = "$row:$col Set Pos Name caption: $caption";
							} else {
								$objPHPExcel->getActiveSheet()->mergeCells($index.':'.$lastColumn.$row);
								$console[] = "$row:$col Unknow";
							}
						}
						elseif ($col >= $colDataGridFirst && $col <= $colDataGridLast) {
							
							$i = $row-$rowDataGridFirst;
							$j = $col-$colDataGridFirst;

							if ($rowDataGrid[$i]['group'] && $col==$colDataGridFirst) {
								$objPHPExcel->getActiveSheet()->mergeCells($index.':'.$lastColumn.$row);
								$console[] = "$row:$col Merge pos type rest cells";
							}
							elseif ($rowDataGrid[$i]['pos']) {
								
								$key = $colDataGrid[$j]['key'];
								$pos = $rowDataGrid[$i]['pos'];
								$item = $colDataGrid[$j]['id'];

								if ($key==COLUMN_DISTRIBUTED) $quantity = $distributedPosItems[$item][$pos];
								elseif ($key==COLUMN_SAP_CONFIRMED) $quantity = $sapPosConfirmedItems[$item][$pos];
								elseif ($key==COLUMN_SAP_SHIPPED) $quantity = $sapPosShippedItems[$item][$pos];
								else $quantity = null;
													
								$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles[$key], false);
								
								if ($quantity) {
									$console[] = "$row:$col Set quantity $quantity for column $key";
								}
							}
						}
	
					break;
					
					case $rowSeparatorDatagridArea: // datagrid area separator
					
						if ($col==1) {
							$console[] = "$row:$col Merge datagrid separator cells";
							$objPHPExcel->getActiveSheet()->mergeCells($index.':'.$lastColumn.$row);
						}
					
					break;
					
					case $rowExtendedFirst: // warehouse group
						
						if ($col==1) {	
							$caption = $rowExtended[0];
							$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['pos-type'], false);
							$console[] = "$row:$col First extended row caption: $caption";
						}
						elseif ($col==$colDataGridFirst) {
							$objPHPExcel->getActiveSheet()->mergeCells($index.':'.$lastColumn.$row);
							$console[] = "$row:$col Merge rest cells from first extended row";
						}
						
					break;
					
					// WAREHOUSES
					case $row > $rowExtendedFirst && $row <= $rowExtendedLast && $rowExtendedTotal > 1 :
						
						$i = $row-$rowExtendedFirst;
						$j = $col-$colDataGridFirst;
						$warehouse = $rowExtended[$i]['warehouse'];
						
						if ($col==1) {
							$caption = $rowExtended[$i]['name'];
							$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['pos-name'], false);
							$console[] = "$row:$col Extended Caption: $caption";
						}
						elseif ($col >= $colDataGridFirst && $col <= $colDataGridLast) {
							
							if ($colDataGrid[$j]) {

								$key = $colDataGrid[$j]['key'];
								$item = $colDataGrid[$j]['id'];
								$warehouse = $rowExtended[$i]['id'];	
																														
								if ($key==COLUMN_DISTRIBUTED) $quantity = $distributedWarehouseItems[$item][$warehouse];
								elseif ($key==COLUMN_SAP_CONFIRMED) $quantity = $sapWarehouseConfirmedItems[$item][$warehouse];
								elseif ($key==COLUMN_SAP_SHIPPED) $quantity = $sapWarehouseShippedItems[$item][$warehouse];
								else $quantity = null;
								
								$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles[$key], false);
								
								if ($quantity) {
									$console[] = "$row:$col Extended quantity $quantity for column $key";
								}
							}
						}
						
					break;
					
					case $rowSeparatorExtendedArea: // extended area separator
							
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->mergeCells($index.':'.$lastColumn.$row);
							$console[] = "$row:$col Merge cells for extended separator";
						}
							
					break;
				}
			}
		}	
	}
	
	
	if ($debug) {
	
		echo "<pre>";
		
		print_r($console);
		
		print_r(array(
			'Rows' => array(
				'Fixed' => array(
					'First' => $rowFixedFirst,
					'Last' => $rowFixedLast,
					'Separator' => $rowSeparatorFixedArea
				),
				'Calculated' => array(
					'First' => $rowCalculatedFirst,
					'Last' => $rowCalculatedLast,
					'Separator' => $rowSeparatorCalculatedArea
				),
				'Datagrid' => array(
					'First' => $rowDataGridFirst,
					'Last' => $rowDataGridLast,
					'Separator' => $rowSeparatorDatagridArea
				),
				'Extended' => array(
					'First' => $rowExtendedFirst,
					'Last' => $rowExtendedLast,
					'Separator' => $rowSeparatorExtendedArea
				)
			),
			'Cols' => array(
				'Fixed' => array(
					'First' => $colFixedFirst,
					'Last' => $colFixedLast,
					'Separator' => $colSeparatorFixedArea,
					'Total' => $colFiexedTotal
				),
				'Calculated' => array(
					'First' => $colCalculatedFirst,
					'Last' => $colCalculatedLast,
					'Separator' => $colSeparatorCalculatedArea
				),
				'Datagrid' => array(
					'First' => $colDataGridFirst,
					'Last' => $colDataGridLast,
					'Separator' => $colSeparatorDatagridArea
				),
				'Extended' => array(
					'First' => $colExtendedFirst,
					'Last' => $colExtendedLast,
					'Separator' => $colSeparatorExtendedArea
				),
			),
			'Totals' => array(
				'Rows' => array(
					'Fixed' => $rowFixedTotal,
					'Calculated' => $rowCalculatedTotal,
					'Datagrid' => $rowDataGridTotal,
					'Extended' => $rowExtendedTotal
				),
				'Cols' => array(
					'Fixed' => $colFiexedTotal,
					'Calculated' => $colCalculatedTotal,
					'Datagrid' => $colDataGridTotal,
					'Extended' => $colExtendedTotal
				),
				'Spreadsheet' => array(
					'Rows' => $rowSpreadSheetTotal,
					'Cols' => $colSpreadSheetTotal
				)
			)
		));
		
		echo "</pre>";
		die();
	}
	

/*******************************************************************************************************************************************************************************************
	EXPORT
********************************************************************************************************************************************************************************************/
	
	if ($export) {
		
		// file name
		$filename = 'ordersheet.sap.export.'.date('YmdHis');
		
		// set active sheet
		$objPHPExcel->getActiveSheet()->setTitle($pagetitle);
		$objPHPExcel->setActiveSheetIndex(0);

		// phpexcel headers
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}
	else {
		message::empty_result();
		url::redirect("/$application/$controller$archived/$action/$id");
	}
	