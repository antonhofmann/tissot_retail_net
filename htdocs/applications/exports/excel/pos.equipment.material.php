<?php 
/*
   POS Equipment Material List

    Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2011-03-22
    Modified by:    
    Date modified:  
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*/
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	define('SHEET_NAME', "POS Equipment List");
	define('SHEET_FILE_NAME', "equipment_list_".date('Y-m-d'));
	define('SHEET_VERSION', "Excel2007");

	$settings = Settings::init();
	$settings->load('data');
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$filter = $_REQUEST['filter'];
	$id = $_REQUEST['material'];

	$model = new Model($application);

	// request execution time
	ini_set('max_execution_time', 180);

	// material
	$material = new Material($application);
	$material->read($id);
	
	// filter: client limited
	if(!$user->permission(Pos::PERMISSION_VIEW) && !$user->permission(Pos::PERMISSION_EDIT)) { 
		
		$filters['limited'] = "posaddress_client_id = $user->address";

		$result = $model->query("
			SELECT DISTINCT posaddress_country 
			FROM db_retailnet.posaddresses 
			WHERE posaddress_client_id = ".$user->address."
		")->fetchAll();
	
		if ($result) {
			
			$countrys = array();
			
			foreach ($result as $row) {
				$user_countrys[] = $row['posaddress_country'];
			}
			
			$user_countrys = join(', ', $user_countrys);
			$filters['countries'] = "posaddress_country IN ( $user_countrys ) ";
		}
	}
	
	// filter: only opened shops
	$filters['opened'] = "
		posaddress_store_openingdate IS NOT NULL 
		AND posaddress_store_openingdate <> '0000-00-00'
		AND (
			posaddress_store_closingdate = '0000-00-00'
			OR posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = ''
		)
	";
	
	// filter: material
	$filters['material'] = "mps_pos_material_material_id = $id";


	// extended country access
	$accessCountries = array();
		
	$result = $model->query("
		SELECT DISTINCT country_access_country
		FROM db_retailnet.country_access
		WHERE country_access_user = $user->id
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			$accessCountries[] = $row['country_access_country'];
		}
		
		$accessCountriesFilter = join(',', $accessCountries);
	}
	
	
	// filter: country access
	if ($accessCountriesFilter) {
		
		$filter = $filters;
		unset($filter['limited']);

		if ($filter) {
			$filter = join(' AND ', $filter);
			$extendedFilter = " OR ($filter AND posaddress_country IN ($accessCountriesFilter))";
		} else {
			$filters['countries'] = "posaddress_country IN ($accessCountriesFilter)";
		}
	}

	// refional access companies
	$regionalAccessFilter = User::getRegionalAccessPos();

	// filter: regional access
	if ($regionalAccessFilter) {
		
		$filter = $filters;
		unset($filter['limited']);

		if ($filter) {
			$filter = join(' AND ', $filter);
			$extendedFilter .= " OR ($filter AND $regionalAccessFilter)";
		} else {
			$filters['regional'] = $regionalAccessFilter;
		}
	}

	
	// datagrid
	$result = $model->query("
		SELECT
			posaddress_id,
			country_name,
			posaddress_name,
			province_canton,
			place_name,
			posaddress_zip,
			posaddress_address,
			posaddress_address2, 
			posaddress_phone, 
			posaddress_mobile_phone,
			posaddress_email,
			postype_name,
			mps_pos_material_quantity_in_use,
			mps_pos_material_quantity_on_stock
		FROM mps_pos_materials
	")
	->bind(Pos_Material::DB_BIND_POSADDRESSES)
	->bind(Pos::DB_BIND_COUNTRIES)
	->bind(Pos::DB_BIND_PLACES)
	->bind(Pos::DB_BIND_PROVINCES)
	->bind(Pos::DB_BIND_POSTYPES)
	->filter($filters)
	->extend($extendedFilter)
	->order('country_name')
	->order('posaddress_name')
	->order('place_name')
	->fetchAll();
	
	if ($result) {
		
		$datagrid = _array::datagrid($result);
		$columns = array_keys(end($datagrid));
		
		// phpexcel
		require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
		
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle(SHEET_NAME);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		
		// styling
		$border = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
		);
		
		$styleNumber = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
		);
		
		
		$styleString = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		);
		
		
		$styleHeaderString = array(
			'font' => array('name'=>'Arial','size'=> 10, 'bold'=>true),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> true),
			'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'EFEFEF')),
			'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
		);
		
		
		$styleHeaderNumber = array(
			'font' => array('name'=>'Arial','size'=> 10, 'bold'=>true),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
					'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> true),
			'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'EFEFEF')),
			'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
		);
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
		$lastColumn = "M";
		
		
		$row=1;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Equipment");
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(18);
		
		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		
		$row++;
		$range = "A$row:$lastColumn"."$row";
		$date = date('d.m.Y  H:i:s');
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: $date");
		
		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Long Term Material: ".$material->code.', '.$material->name);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
		
		
		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		
		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		
		$row++;
		$col = 0;
		
		// columns
		foreach ($columns as $value) {
			$index = spreadsheet::index($col,$row);
			$format = ($col==11 || $col==12) ? $styleHeaderNumber : $styleHeaderString;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $translate->$value);
			$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($format);
			$col++;
		}

		// grid
		foreach ($datagrid as $i => $array) {
		
			$row++; 
			$col = 0;
			
			foreach($array as $key => $value) {
				
				$index = spreadsheet::index($col,$row);
				$format = ($col==11 || $col==12) ? $styleNumber : $styleString;
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
				$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($format);
				$col++;
			}
		}
		
		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		
		// export
		$objPHPExcel->getActiveSheet()->setTitle(SHEET_NAME);
		$objPHPExcel->setActiveSheetIndex(0);
		
		// redirect output to client browser
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.SHEET_FILE_NAME.'.xlsx"');
		header('Cache-Control: max-age=0');
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, SHEET_VERSION);
		$objWriter->save('php://output');
		
		exit;
	}
	else {
		message::empty_result();
		url::redirect("/$application/$controller/$action/$id");
	}
				 