<?php 
		
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	// execution time
	ini_set('max_execution_time', 120);
	ini_set('memory_limit', '1024M');
	
	$settings = Settings::init();
	$settings->load('data');
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['mastersheet'];
	$planning_types = $_REQUEST['planning_type'] ?: $_REQUEST['group'];
	
	$model = new Model($application);
	
	// mastersheet
	$mastersheet = new Mastersheet($application);
	$mastersheet->read($id);
	
	// export filename
	$filename  = "mps_consolidated_splitting_list_";
	$filename .= str_replace(' ', '_', strtolower($mastersheet->name));
	$filename .= "_".date('Ymd') ;
	
	// titles
	$pagetitle = $translate->splitting_list;
	$title = $translate->splitting_list;
	
	// desired quantities
	if ($_REQUEST['proposed_quantity']) {
		$request['proposed_quantity'] = $_REQUEST['proposed_quantity'];
	}
	
	// desired quantities
	if ($_REQUEST['desired_quantity']) {
		$request['quantity'] = $_REQUEST['desired_quantity'];
	}
	
	// total cost of desired quantities
	if ($_REQUEST['desired_quantity_total']) {
		$request['quantity_cost'] = $_REQUEST['desired_quantity_total'];
	}
	
	// approved quantities
	if ($_REQUEST['approved_quantity']) {
		$request['approved'] = $_REQUEST['approved_quantity'];
	}
	
	// total cost of approved quantities
	if ($_REQUEST['approved_quantity_total']) {
		$request['approved_cost'] = $_REQUEST['approved_quantity_total'];
	}
	
	// orderedquantities
	if ($_REQUEST['ordered_quantity']) {
		$request['ordered'] = $_REQUEST['ordered_quantity'];
	}
	
	// total cost of ordered quantities
	if ($_REQUEST['ordered_quantity_total']) {
		$request['ordered_cost'] = $_REQUEST['ordered_quantity_total'];
	}
	
	// visible columns
	$showColumns = array_keys($request);
	
	// currency columns
	$currencyColumns = array(
		'quantity_cost',
		'approved_cost',
		'ordered_cost'
	);
	
	// request
	$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

	// caption: client
	if ($_REQUEST['ordersheets']) {
		
		$filters['ordersheet'] = "mps_ordersheet_id = ".$_REQUEST['ordersheets'];
	
		$result = $model->query("
			SELECT CONCAT(country_name, ', ',address_company) AS client
			FROM mps_ordersheets
		")
		->bind(Ordersheet::DB_BIND_COMPANIES)
		->bind(Company::DB_BIND_COUNTRIES)
		->filter('client', $filters['ordersheet'])
		->fetch();
		
		$captions[] = "Client: ".$result['client'];
	}
	
	// caption: workflow state
	if ($_REQUEST['workflowstates']) {
		
		$workflowstate = new Workflow_State($application);
		$workflowstate->read($_REQUEST['workflowstates']);
		
		$captions[] = "Workflow State: : ".$workflowstate->name;
		
		$filters['ordersheet'] = "mps_ordersheet_workflowstate_id = ".$_REQUEST['workflowstates'];
	}
	
	// default filter
	$filters['default'] = "mps_ordersheet_mastersheet_id = $id";
	
	// datagrid
	$result = $model->query("
		SELECT DISTINCT
			mps_ordersheet_id,
			mps_material_id,
			mps_material_code, 
			mps_material_name,
			mps_material_planning_type_id,
			mps_material_planning_type_name,
			mps_ordersheet_item_quantity_proposed AS total_proposed,
			mps_ordersheet_item_quantity AS total_quantity,
			mps_ordersheet_item_quantity_approved AS total_approved,
			mps_ordersheet_item_quantity_confirmed AS total_ordered,
			mps_ordersheet_item_price,
			CONCAT(country_name,', ', address_company) AS customer
		FROM mps_ordersheet_items 
	")
	->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
	->bind(Ordersheet_Item::DB_BIND_MATERIALS)
	->bind(Material::DB_BIND_PLANNING_TYPES)
	->bind(Ordersheet::DB_BIND_COMPANIES)
	->bind(Company::DB_BIND_COUNTRIES)
	->filter($filters)
	->order('country_name')
	->order('mps_material_code')
	->order('mps_material_name')
	->fetchAll();

	
	if ($result && $planning_types && $showColumns) { 
		
		foreach ($result as $row) {
			
			$planning = $row['mps_material_planning_type_id'];
			$ordersheet = $row['mps_ordersheet_id'];
			$item = $row['mps_material_id'];
			
			if ($planning_types[$planning]) {
				
				// customer
				$datagrid[$ordersheet]['name'] = $row['customer'];
				
				// customer desired quantity
				if ($request['proposed_quantity']) {
					$datagrid[$ordersheet]['items'][$item]['proposed_quantity'] = $row['total_proposed'];
				}
				
				// customer desired quantity
				if ($request['quantity']) {
					$datagrid[$ordersheet]['items'][$item]['quantity'] = $row['total_quantity'];
				}
				
				// total cost of desired quantity
				if ($request['quantity_cost']) {
					$datagrid[$ordersheet]['items'][$item]['quantity_cost'] = $row['total_quantity']*$row['mps_ordersheet_item_price'];
				}
				
				// approved quantity
				if ($request['approved']) {
					$datagrid[$ordersheet]['items'][$item]['approved'] = $row['total_approved'];
				}
				
				// total cost of approved quantity
				if ($request['approved_cost']) {
					$datagrid[$ordersheet]['items'][$item]['approved_cost'] = $row['total_approved']*$row['mps_ordersheet_item_price'];
				}
				
				// ordered quantity
				if ($request['ordered']) {
					$datagrid[$ordersheet]['items'][$item]['ordered'] = $row['total_ordered'];
				}
				
				// total cost of ordered quantity
				if ($request['ordered_cost']) {
					$datagrid[$ordersheet]['items'][$item]['ordered_cost'] = $row['total_ordered']*$row['mps_ordersheet_item_price'];
				}
				
				// item columns
				$columns[$item]['code'] = $row['mps_material_code'];
				$columns[$item]['name'] = $row['mps_material_name'];
			}
		}
	
		
		// number of displayed item columns
		$totalItemColumns = count($showColumns);
		$totalColumns = count($columns)*$totalItemColumns;
		$lastColumn = spreadsheet::index($totalColumns);
	
		// phpexcel
		require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
		
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle($pagetitle);							 
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		// styling
		$border = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,)
		);
		
		$styleNumber = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
		);
		
		
		$styleString = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		);
		
		
		$styleTotalCollection = array(
			'font' => array('name'=>'Arial','size'=> 10, 'bold'=>true),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> false)
		);
		
		
		$styleTotalOrdersheet = array(
			'font' => array('name'=>'Arial','size'=> 14, 'bold'=>true),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> false)
		);
		
		
		$styleHeaderString = array(
			'font' => array('name'=>'Arial','size'=> 10, 'bold'=>false),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> true),
			'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'EFEFEF')),
			'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
		);
		
		
		$styleHeaderNumber = array(
			'font' => array('name'=>'Arial','size'=> 10, 'bold'=>false),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, 
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> true),
			'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'EFEFEF')),
			'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
		);
		
		
		$row=1;
		$range = "A$row:$lastColumn$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", $title );
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(16);
		
		$row++;
		$range = "A$row:$lastColumn$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		
		$row++;
		$range = "A$row:$lastColumn$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", $mastersheet->name.', '.$mastersheet->year);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(14);
		
		$row++;
		$range = "A$row:$lastColumn$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		
		$row++;
		$range = "A$row:$lastColumn$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: ".date('d.m.Y  H:i:s'));
		
		// filter captions
		if ($captions) {
			foreach ($captions as $caption) {
				$row++;
				$range = "A$row:$lastColumn$row";
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				$objPHPExcel->getActiveSheet()->setCellValue("A$row", $caption);
				$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
			}
		}
		
		$row++;
		$range = "A$row:$lastColumn$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		
		$row++;
		$range = "A$row:$lastColumn$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		
		// table header: Customer
		$row++;
		$col = 0;
		$_row = $row+2;
		$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$row,0,$_row);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", $translate->customer );
		$objPHPExcel->getActiveSheet()->getStyle("A$row:A$_row")->applyFromArray($styleHeaderString);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(12);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

		// captions
		$captions = array(
			'proposed_quantity' => "Proposed Quantity",
			'quantity' => "Quantity",
			'quantity_cost' => "Total Cost of Quantity",
			'approved' => "Approved",
			'approved_cost' => "Total Cost of Approved",
			'ordered' => "Pre Ordered",
			'ordered_cost' => "Total Cost of Pre Ordered"
		);
		
		// items header
		foreach ($columns as $key => $value) {

			$caption = null;
			$currentColumns = $showColumns;
			
			for ($i = 1; $i<=$totalItemColumns; $i++) {
				
				$col++;
				$column = spreadsheet::index($col);
				$current = array_shift($currentColumns);
				
				if ($i==1) {
					
					$mergedIndex = ($totalItemColumns==1) ? $col : $col+$totalItemColumns-1;
					$_column = spreadsheet::index($mergedIndex);
						
					// header: item code
					$objPHPExcel->getActiveSheet()->mergeCells($column.$row.":".$_column.$row);
					$objPHPExcel->getActiveSheet()->getStyle($column.$row.":".$_column.$row)->applyFromArray($styleHeaderString);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $value['code']);
					$objPHPExcel->getActiveSheet()->getStyle($column.$row.":".$_column.$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);
						
					// header: item name
					$row3 = $row+1;
					$objPHPExcel->getActiveSheet()->mergeCells($column.$row3.":".$_column.$row3);
					$objPHPExcel->getActiveSheet()->getStyle($column.$row3.":".$_column.$row3)->applyFromArray($styleHeaderString);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row3, $value['name']);
					$objPHPExcel->getActiveSheet()->getStyle($column.$row3.":".$_column.$row3)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);
				}
				
				// header caption
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$_row, $captions[$current]);
				
				// header style
				$objPHPExcel->getActiveSheet()->getStyle($column.$_row)->applyFromArray($styleHeaderNumber);
				
				// column dimensions
				if ($totalItemColumns==1) $objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth(30);
				else $objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth(20);
			}
		}
		
		
		$row = $_row;
		
		foreach ($datagrid as $key => $data) {
			
			$country = $data['name'];
			$items = $data['items'];
			
			// country name
			$row++;
			$objPHPExcel->getActiveSheet()->setCellValue("A$row", $country);
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->applyFromArray($styleString);
			
			$col = 0;
			
			foreach ($columns as $key => $value) {
				
				$current = null;
				$currentColumns = $showColumns;
			
				for ($i = 1; $i<=$totalItemColumns; $i++) {
			
					$col++;
					$column = spreadsheet::index($col);
					$current = array_shift($currentColumns);
					
					// column value
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $items[$key][$current]);
					
					// currency column
					if (in_array($current, $currencyColumns)) {
						$objPHPExcel->getActiveSheet()->getStyle($column.$row)->getNumberFormat()->setFormatCode("0.0000");
					}
			
					// data styles		
					$objPHPExcel->getActiveSheet()->getStyle($column.$row)->applyFromArray($styleNumber);
					
					// sub colum values
					$sumColumn[$current][$key] = $sumColumn[$current][$key] + $items[$key][$current];
				}
			}
		}	


		$row++;
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", $translate->total);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->applyFromArray($styleHeaderString);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(12);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
		$col = 0;
		
		foreach ($columns as $key => $value) {
			
			$current = null;
			$currentColumns = $showColumns;
				
			for ($i = 1; $i<=$totalItemColumns; $i++) {
					
				$col++;
				$column = spreadsheet::index($col);
				$current = array_shift($currentColumns);
				
				// column value
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $sumColumn[$current][$key]);
					
				// currency column
				if (in_array($current, $currencyColumns)) {
					$objPHPExcel->getActiveSheet()->getStyle($column.$row)->getNumberFormat()->setFormatCode("0.0000");
				}
					
				// data styles
				$objPHPExcel->getActiveSheet()->getStyle($column.$row)->applyFromArray($styleHeaderNumber);
				$objPHPExcel->getActiveSheet()->getStyle($column.$row)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle($column.$row)->getFont()->setSize(12);
			}
		}
		
		$row++;
		$range = "A$row:$lastColumn$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", '');
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);

		$objPHPExcel->getActiveSheet()->setTitle($pagetitle);
		$objPHPExcel->setActiveSheetIndex(0);

		// redirect output to client browser
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}
	else {
		message::empty_result();
		url::redirect("/$application/$controller/$action/$id");
	}
							 