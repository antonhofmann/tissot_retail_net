<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	// execution time
	ini_set('max_execution_time', 120);
	ini_set('memory_limit', '1024M');
	
	$settings = Settings::init();
	$settings->load('data');
	$user = User::instance();
	$translate = Translate::instance();
	
	// request vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['ordersheet'];
	$version = $_REQUEST['version'];
	
	// order Sheet
	$ordersheet = new Ordersheet($application);
	$ordersheet->read($id);
	
	// page title
	$pagetitle = 'Order Sheet Planning';
	
	
	// db model
	$model = new Model($application);
	
	
/*******************************************************************************************************************************************************************************************
	Ordersheet States
********************************************************************************************************************************************************************************************/
	
	$states_preparation = Ordersheet_State::loader('preparation');
	$states_complete = Ordersheet_State::loader('complete');
	$states_revision = Ordersheet_State::loader('revision');
	$states_confirmation = Ordersheet_State::loader('confirmation');
	$states_distribution = Ordersheet_State::loader('distribution');
	
	// quantity statments
	$ordersheet_has_approved_items = $ordersheet->hasApprovedQuantities();
	
	// group statments
	$statment_on_preparation = $ordersheet->state()->onPreparation();
	$statment_on_completing = $ordersheet->state()->onCompleting();
	$statment_on_revision = $ordersheet->state()->onCompleting();
	$statment_on_confirmation = $ordersheet->state()->onConfirmation();
	$statment_on_distribution = $ordersheet->state()->onDistribution();
	
	// editabled states
	if ($ordersheet->state()->owner) {
		$editabled_states = $states_complete;
	}
	elseif ($ordersheet->state()->canAdministrate()) {
		$editabled_states = array_merge($states_preparation, $states_confirmation, $states_distribution);
	}
	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Global Properties
	Set editabled and visibility vars
********************************************************************************************************************************************************************************************/
	
	// column keywords
	define(COLUMN_PLANNED, 'planned');
	define(COLUMN_DELIVERED, 'delivered');
	define(COLUMN_PARTIAL_DELIVERED, 'partial');
	
	// readonly mode
	$spreadsheet_mode_readonly =  ($ordersheet->state()->isDistributed() || $version || !in_array($ordersheet->workflowstate_id, $editabled_states)) ? true: false;
	$spreadsheet_mode_planning = ($statment_on_preparation) ? true : false;
	
	// spreadsheet attribute readonly
	$spreadsheet_attribute_readonly =  ($spreadsheet_mode_readonly) ? 'true' : 'false';
	
	// item column visibility
	$spreadsheet_column_planned = true;
	$spreadsheet_column_distributed = (in_array($ordersheet->workflowstate_id, $states_distribution) || $ordersheet->state()->isDistributed()) ? true : false;

	// show extended rows
	if ($version) $spreadsheet_show_extended_rows = true;
	else $spreadsheet_show_extended_rows = ($ordersheet->state()->isDistributed()) ? false : true;
	
	// show calcullated planning row planning
	$spreadsheet_row_planning = true;
	
	// show calcullated owner quantities row
	$spreadsheet_row_owner_quantity = ($statment_on_preparation && $ordersheet->hasOwnerQuantities()) ? true : false;
	
	// show calcullated approved row
	if ($ordersheet_has_approved_items || $spreadsheet_column_distributed) {
		$spreadsheet_row_confirmed_quantity = true;
	}
	
	// show calcullated unused row
	if ($spreadsheet_row_owner_quantity || $ordersheet_has_approved_items || $spreadsheet_column_distributed) {
		$spreadsheet_row_unused_quantity = (!$version && $ordersheet->state()->isDistributed()) ? false : true;
	}

	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Filters
********************************************************************************************************************************************************************************************/
	
	$filters = array();
	
	// filter: pos
	$filters['default'] = "posaddress_client_id = ".$ordersheet->address_id;
	
	// filter: distribution channels
	if ($_REQUEST['distribution_channels']) {
		$filters['distribution_channels'] = 'posaddress_distribution_channel = '.$_REQUEST['distribution_channels'];
	}
	
	// filter: turnover classes
	if ($_REQUEST['turnoverclass_watches']) {
		$filters['turnoverclass_watches'] = 'posaddress_turnoverclass_watches = '.$_REQUEST['turnoverclass_watches'];
	}
	
	// filter: sales represenatives
	if ($_REQUEST['sales_representative']) {
		$filters['sales_representative'] = 'posaddress_sales_representative = '.$_REQUEST['sales_representative'];
	}
	
	// filter: decoration persons
	if ($_REQUEST['decoration_person']) {
		$filters['decoration_person'] = 'posaddress_decoration_person = '.$_REQUEST['decoration_person'];
	}
	
	// provinces
	if ($_REQUEST['provinces']) {
		$filters['provinces'] = 'province_id = '.$_REQUEST['provinces'];
	}
	
	// for not archive order sheets, show only active POS
	if (!$ordersheet->state()->isDistributed()) {
		$filters['active'] = "(
			posaddress_store_closingdate = '0000-00-00'
			OR posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = ''
		)";
	} 
	// otherwise show only POS locations which has distributed quantities 
	else {
		
		$result = $model->query("
			SELECT DISTINCT 
				mps_ordersheet_item_delivered_posaddress_id AS pos
			FROM mps_ordersheet_item_delivered		
		")
		->bind(Ordersheet_Item_Delivered::DB_BIND_ORDERSHEET_ITEMS)
		->filter('ordersheet', 'mps_ordersheet_item_ordersheet_id = '.$ordersheet->id)
		->filter('null', 'mps_ordersheet_item_delivered_posaddress_id > 0')
		->fetchAll();
		
		if ($result) {
			
			$array = array();
			
			foreach ($result as $row) {
				$array[] = $row['pos'];
			}

			$filters['active'] = 'posaddress_id IN ('.join(',', $array).')';
		}
	}
	
	
/*******************************************************************************************************************************************************************************************
	Dataloader: Distribution Channels
********************************************************************************************************************************************************************************************/
	
	
	// load distribution channels
	$result = $model->query("
		SELECT DISTINCT
			mps_distchannel_id,
			CONCAT(mps_distchannel_group, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) AS distribution_channel
		FROM db_retailnet.posaddresses
	")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->bind(Pos::DB_BIND_DISTRIBUTION_CHANNELS)
	->filter($filters)
	->exclude('distribution_channels')
	->order('mps_distchannel_group, mps_distchannel_name, mps_distchannel_code')
	->fetchAll();
	
	if ($result) {
		$dropdowns .= ui::dropdown($result, array(
			'id' => 'distribution_channels',
			'name' => 'distribution_channels',
			'value' => $_REQUEST['distribution_channels'],
			'caption' => $translate->all_distribution_channels
		));
	}
	

/*******************************************************************************************************************************************************************************************
	Dataloader: Turnover Class Watches
********************************************************************************************************************************************************************************************/
	
	$result = $model->query("
		SELECT DISTINCT
			mps_turnoverclass_id, mps_turnoverclass_name
		FROM db_retailnet.posaddresses
	")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->bind(Pos::DB_BIND_TURNOVER_CLASS_WATCHES)
	->filter('company', "posaddress_franchisee_id = ".$ordersheet->address_id)
	->order('mps_turnoverclass_name')
	->fetchAll();
	
	if ($result) {
		$dropdowns .= ui::dropdown($result, array(
			'name' => 'turnoverclass_watches',
			'id' => 'turnoverclass_watches',
			'value' => $_REQUEST['turnoverclass_watches'],
			'caption' => $translate->all_turnover_class_watches
		));
	}

	
/*******************************************************************************************************************************************************************************************
	Dataloader: Sales Representative
********************************************************************************************************************************************************************************************/
	
	$model->query("
		SELECT DISTINCT
			mps_staff_id,
			CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS name
		FROM db_retailnet.posaddresses
	")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->bind(Pos::DB_BIND_MPS_STAFF_SALES_REPRESENTATIVES)
	->filter($filters)
	->filter('staff_type', 'mps_staff_staff_type_id = 1')
	->exclude('sales_representative')
	->order('mps_staff_name, mps_staff_firstname');
	
	if ($ordersheet->state()->owner) {
		$model->filter('limited', 'mps_staff_address_id='.$user->address);
	}
	
	$result = $model->fetchAll();

	if ($result) {
		$dropdowns .= ui::dropdown($result, array(
			'name' => 'sales_representative',
			'id' => 'sales_representative',
			'value' => $_REQUEST['sales_representative'],
			'caption' => $translate->all_sales_representatives
		));
	}
	

/*******************************************************************************************************************************************************************************************
	Dataloader: Deecoration Persons
********************************************************************************************************************************************************************************************/
	
	$model->query("
		SELECT DISTINCT
			mps_staff_id,
			CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS name
		FROM db_retailnet.posaddresses
	")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->bind(Pos::DB_BIND_MPS_STAFF_DECORATOR_PERSONS)
	->filter($filters)
	->filter('staff_type', 'mps_staff_staff_type_id = 2')
	->exclude('decoration_person')
	->order('mps_staff_name, mps_staff_firstname');
	
	if ($ordersheet->state()->owner) {
		$model->filter('limited', 'mps_staff_address_id='.$user->address);
	}
	
	$result = $model->fetchAll();

	if ($result) {
		$dropdowns .= ui::dropdown($result, array(
			'name' => 'decoration_person',
			'id' => 'decoration_person',
			'value' => $_REQUEST['decoration_person'],
			'caption' => $translate->all_decoration_persons
		));
	}
	
	
/*******************************************************************************************************************************************************************************************
	Dataloader: Provinces
********************************************************************************************************************************************************************************************/
	
	$result = $model->query("
		SELECT DISTINCT
			province_id, province_canton
		FROM db_retailnet.posaddresses")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->filter($filters)
	->exclude('provinces')
	->order('province_canton')
	->fetchAll();
	
	if ($result) {
		$dropdowns .= ui::dropdown($result, array(
			'name' => 'provinces',
			'id' => 'provinces',
			'value' => $_REQUEST['provinces'],
			'caption' => $translate->all_provinces
		));
	}
	
	
/*******************************************************************************************************************************************************************************************
	Dataloader: POS Locations
********************************************************************************************************************************************************************************************/
	
	$pos_locations = array();
	
	$result = $model->query("
		SELECT DISTINCT
			posaddress_id,
			posaddress_name,
			posaddress_place,
			postype_id,
			postype_name
		FROM db_retailnet.posaddresses
	")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->bind(Pos::DB_BIND_POSTYPES)
	->filter($filters)
	->order('posaddress_name')
	->order('posaddress_place')
	->fetchAll();
	
	if ($result) {
		foreach ($result as $row) {
			$pos = $row['posaddress_id'];
			$type = $row['postype_id'];
			$pos_locations[$type]['caption'] = $row['postype_name'];
			$pos_locations[$type]['pos'][$pos]['name'] = $row['posaddress_name'];
			$pos_locations[$type]['pos'][$pos]['place'] = $row['posaddress_place'];
		}
	}
	
/*******************************************************************************************************************************************************************************************
	Dataloader: Order Sheet Items
********************************************************************************************************************************************************************************************/
	
	$ordersheet_items = array();
	$material_partial_columns = array();
	$delivered_materials = array();
	
	$result = $model->query("
		SELECT
			mps_ordersheet_item_id,
			mps_ordersheet_item_material_id,
			mps_ordersheet_item_quantity,
			mps_ordersheet_item_quantity_approved,
			mps_ordersheet_item_quantity_confirmed,
			mps_ordersheet_item_quantity_shipped,
			mps_ordersheet_item_quantity_distributed,
			mps_material_planning_type_id,
			mps_material_planning_type_name,
			mps_material_collection_category_id,
			mps_material_collection_category_code,
			mps_material_id,
			mps_material_code,
			mps_material_name
		FROM mps_ordersheet_items
	")
	->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
	->bind(Ordersheet_Item::DB_BIND_MATERIALS)
	->bind(Material::DB_BIND_PLANNING_TYPES)
	->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
	->filter('ordersheets', "mps_ordersheet_id = ".$ordersheet->id)
	->order('mps_material_planning_type_name')
	->order('mps_material_collection_category_code')
	->order('mps_material_code')
	->order('mps_material_name')
	->fetchAll();
	
	if ($result) {
		
		// check if delivered quantities are once confirmed
		$check_confirmation = $model->query("
			SELECT COUNT(mps_ordersheet_item_delivered_id) AS total
			FROM mps_ordersheet_item_delivered	
		")
		->bind(Ordersheet_Item_Delivered::DB_BIND_ORDERSHEET_ITEMS)
		->filter('ordersheet', 'mps_ordersheet_item_ordersheet_id = '.$ordersheet->id)
		->filter('confirmed', 'mps_ordersheet_item_delivered_confirmed IS NOT NULL')
		->fetch();
		
		foreach ($result as $row) {
			
			$approved_quantity = $row['mps_ordersheet_item_quantity_approved'];
			$confirmed_quantity = $row['mps_ordersheet_item_quantity_confirmed'];
				
			// for distribution, show only approved quantities
			if ($spreadsheet_column_distributed) $show_item = ($confirmed_quantity > 0) ? true : false;
			else $show_item = ($approved_quantity > 0 || is_null($approved_quantity)) ? true : false;
			
			if ($show_item) {
				
				$item = $row['mps_ordersheet_item_id'];
				$material = $row['mps_ordersheet_item_material_id'];
				$planning = $row['mps_material_planning_type_id'];
				$collection = $row['mps_material_collection_category_id'];
				
				$ordersheet_items[$planning]['caption'] = $row['mps_material_planning_type_name'];
				$ordersheet_items[$planning]['data'][$collection]['caption'] = $row['mps_material_collection_category_code'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['id'] = $row['mps_material_id'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['code'] = $row['mps_material_code'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['name'] = $row['mps_material_name'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity'] = $row['mps_ordersheet_item_quantity'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity_approved'] = $row['mps_ordersheet_item_quantity_approved'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity_confirmed'] = $row['mps_ordersheet_item_quantity_confirmed'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity_shipped'] = $row['mps_ordersheet_item_quantity_shipped'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity_distributed'] = $row['mps_ordersheet_item_quantity_distributed'];
				
				if ($spreadsheet_column_distributed && $check_confirmation['total'] && $row['mps_ordersheet_item_quantity_distributed']) {
					
					// items for partial distribution
					if ($row['mps_ordersheet_item_quantity_shipped']-$row['mps_ordersheet_item_quantity_distributed'] > 0) {
						$material_partial_columns[$planning][$collection][$material] = true;
					} 
					// delivered items
					else {
						$delivered_materials[$material] = true;
					}
				}
			}
		}

		// if all items are delivered hide unused row
		if (!$version && $delivered_materials[$material] && !$material_partial_columns) {
			$spreadsheet_row_unused_quantity = false;
		}
	}


/*******************************************************************************************************************************************************************************************
	Dataloader: Order Sheet Items Planned Quantities
********************************************************************************************************************************************************************************************/
	
	$planned_quantities = array();
	$planned_warehouse_quantities = array();
	$sum_planned = array();
	
	if ($spreadsheet_column_planned) {
		
		$result = $model->query("
			SELECT DISTINCT
				mps_ordersheet_item_planned_id,
				mps_ordersheet_item_planned_posaddress_id,
				mps_ordersheet_item_planned_warehouse_id,
				mps_ordersheet_item_planned_quantity,
				mps_ordersheet_item_material_id
			FROM mps_ordersheet_item_planned
		")
		->bind(Ordersheet_Item_Planned::DB_BIND_ORDERSHEET_ITEMS)
		->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = ".$ordersheet->id)
		->fetchAll();
		
		if ($result) {
			
			foreach ($result as $row) {
				
				$material = $row['mps_ordersheet_item_material_id'];
				
				// planned quantities
				if ($row['mps_ordersheet_item_planned_posaddress_id']) {
					$pos = $row['mps_ordersheet_item_planned_posaddress_id'];
					$planned_quantities[$pos][$material]['id'] = $row['mps_ordersheet_item_planned_id'];
					$planned_quantities[$pos][$material]['quantity'] = $row['mps_ordersheet_item_planned_quantity'];
					$sum_planned[$material] = $sum_planned[$material] + $row['mps_ordersheet_item_planned_quantity'];
				}
				
				// planned warehouse quantities
				if ($row['mps_ordersheet_item_planned_warehouse_id']) {
					$warehouse = $row['mps_ordersheet_item_planned_warehouse_id'];
					$planned_warehouse_quantities[$warehouse][$material]['id'] = $row['mps_ordersheet_item_planned_id'];
					$planned_warehouse_quantities[$warehouse][$material]['quantity'] = $row['mps_ordersheet_item_planned_quantity'];
				}
			}
		}
	}
	

/*******************************************************************************************************************************************************************************************
	Dataloader: Order Sheet Items Delivered Quantities
********************************************************************************************************************************************************************************************/

	$delivered_quantities = array();
	$delivered_quantities_not_confirmed = array();
	$delivered_warehouse_quantities = array();
	$delivered_warehouse_quantities_not_confirmed = array();
	$delivered_confirmed_dates = array();
	
	if ($spreadsheet_column_distributed) { 
	
		$result = $model->query("
			SELECT DISTINCT
				mps_ordersheet_item_delivered_id,
				mps_ordersheet_item_delivered_posaddress_id,
				mps_ordersheet_item_delivered_warehouse_id,
				mps_ordersheet_item_delivered_quantity,
				mps_ordersheet_item_delivered_confirmed,
				DATE_FORMAT(mps_ordersheet_item_delivered_confirmed,'%d.%m.%Y') AS confirmed_date,
				mps_ordersheet_item_id,
				mps_ordersheet_item_material_id,
				mps_material_material_planning_type_id,
				mps_material_material_collection_category_id
			FROM mps_ordersheet_item_delivered
		")
		->bind(Ordersheet_Item_Delivered::DB_BIND_ORDERSHEET_ITEMS)
		->bind(Ordersheet_Item::DB_BIND_MATERIALS)
		->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = ".$ordersheet->id)
		->order('mps_ordersheet_item_delivered_confirmed', 'DESC')
		->order('mps_ordersheet_item_delivered.date_created', 'DESC')
		->fetchAll();
	
		if ($result) { 
			
			foreach ($result as $row) {
	
				$id = $row['mps_ordersheet_item_delivered_id'];
				$planning = $row['mps_material_material_planning_type_id'];
				$collection = $row['mps_material_material_collection_category_id'];
				$material = $row['mps_ordersheet_item_material_id'];
				$item = $row['mps_ordersheet_item_id'];
				$pos = $row['mps_ordersheet_item_delivered_posaddress_id'];
				$warehouse = $row['mps_ordersheet_item_delivered_warehouse_id'];
				$quantity = $row['mps_ordersheet_item_delivered_quantity'];
				$timestamp = ($row['confirmed_date']) ? date::timestamp($row['confirmed_date']) : 0;
				
				// build versions data
				if ($timestamp) {
					$delivered_confirmed_dates[$timestamp] = 'Delivery Date: '.$row['confirmed_date'];
				}
				
				// show quantities on required confirmed date
				if ($version) {
				
					if ($version==$timestamp) {
						
						// pos delivered quantities
						if ($pos) {
							$delivered_quantities[$pos][$material]['id'] = $id;
							$delivered_quantities[$pos][$material]['quantity'] = $quantity;
						}
						
						// warehouse delivered quantities
						if ($warehouse) {
							$delivered_warehouse_quantities[$warehouse][$material]['id'] = $id;
							$delivered_warehouse_quantities[$warehouse][$material]['quantity'] = $quantity;
						}
					}
				} 
				else {
					
					// delivered quantities are once confirmed
					// sum all confirmed quantities for and set not confiremd quantities as not confirmed
					if ($ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity_distributed']) {
							
						if ($row['mps_ordersheet_item_delivered_confirmed']) {
							
							// pos quantities
							if ($pos) {
								$delivered_quantities[$pos][$material]['id'] = $id;
								$delivered_quantities[$pos][$material]['quantity'] = $delivered_quantities[$pos][$material]['quantity'] + $quantity;
							}
							
							// warehouse quantities, only last insertetd
							if ($warehouse && !$delivered_warehouse_quantities[$warehouse][$material] && $quantity) {
								$delivered_warehouse_quantities[$warehouse][$material]['id'] = $id;
								$delivered_warehouse_quantities[$warehouse][$material]['quantity'] = $quantity;
							}

						} else {
							
							// pos quantities
							if ($pos) {
								$delivered_quantities_not_confirmed[$pos][$material]['id'] = $id;
								$delivered_quantities_not_confirmed[$pos][$material]['quantity'] = $quantity;
							}
							
							// warehouse quantities
							if ($warehouse) {
								$delivered_warehouse_quantities_not_confirmed[$warehouse][$material]['id'] = $id;
								$delivered_warehouse_quantities_not_confirmed[$warehouse][$material]['quantity'] = $quantity;
							}
						}
					}
					// not confirmed quantities
					else {
						
						// pos quantities
						if ($pos) {
							$delivered_quantities[$pos][$material]['id'] = $id;
							$delivered_quantities[$pos][$material]['quantity'] = $quantity;
						}
						
						// warehouse quantities
						if ($warehouse) {
							$delivered_warehouse_quantities[$warehouse][$material]['id'] = $id;
							$delivered_warehouse_quantities[$warehouse][$material]['quantity'] = $quantity;
						}
					}
				}
			}
		}
	}

	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Fixed Area
	Fixed Rows, Columns and group separators
********************************************************************************************************************************************************************************************/
	
	// fixed rows
	$fixed_rows = 5;
	
	// fixed columns
	$fixed_columns = 1;
	
	// row group separator
	$rowgroup_separator = 0;
	
	// column group separator
	$colgroup_separator = 1;
	
	// step index
	$step_max = ($material_partial_columns) ? 3 : 2;
	
	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Referenced Rows
	POS locations grouped by POS types
********************************************************************************************************************************************************************************************/	

	$referenced_rows = array();
	$group = array();
	$total_rowgroup_separators = 0;
	$i=0;

	if ($pos_locations) {

		foreach ($pos_locations as $type => $data) {
				
			$referenced_rows[$i]['type'] = $type;
			$referenced_rows[$i]['caption'] = $data['caption'];
			$referenced_rows[$i]['group'] = true;
	
			$j = $i+1;
	
			foreach ($data['pos'] as $pos => $value) {
	
				$referenced_rows[$j]['type'] = $type;
				$referenced_rows[$j]['pos'] = $pos;
				
				$caption = $value['name'].' ('.$value['place'].')'; 
				$caption = str_replace("'", "", $caption);
				$referenced_rows[$j]['caption'] = $caption;	
				$j++;
			}
	
			$i = $j + $rowgroup_separator;
				
			$total_rowgroup_separators = $total_rowgroup_separators + $rowgroup_separator;
		}
	
		// remove last seperator
		if ($total_rowgroup_separators) $total_rowgroup_separators = $total_rowgroup_separators - 1;
	}
	
	// total row references
	$total_referenced_rows = count($referenced_rows) + $total_rowgroup_separators;
	
	// column references separators
	$referenced_rows_separator = 0;

	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Referenced Columns
	Order sheet items grouped by material planning types
********************************************************************************************************************************************************************************************/

	$referenced_columns = array();
	$planning_type_captions = array();
	$total_colgroup_separators = 0;
	$planning_column_index = 0;
	$item_colum_index = 0;
	$i=0;
	
	// column multiplicator
	$multiple = ($spreadsheet_column_distributed) ? 2 : 1;
	
	if ($ordersheet_items && ($spreadsheet_column_planned || $spreadsheet_column_distributed)) {
		
		foreach ($ordersheet_items as $planning => $data) {
			
			foreach ($data['data'] as $collection => $value) {
				
				// merge length for planning caption
				$total_item_columns = count($value['data']) * $multiple;
				$total_partial_columns = (!$version && $material_partial_columns[$planning][$collection]) ? count($material_partial_columns[$planning][$collection]) : 0;
				$planning_type_captions[$i]['merge'] = $total_item_columns + $total_partial_columns;
					
				// planning type caption
				$caption_planning = $data['caption'];
				$caption_collection = $value['caption'];
				$planning_type_captions[$i]['planning'] = $caption_planning;
				$planning_type_captions[$i]['collection'] = $caption_collection;
	
				foreach ($value['data'] as $item => $row) { 
					
					// skip separator columns
					if ($i==$item_colum_index) {
							
						// merge length for item caption
						$merge_length = 1;
						$merge_length = ($spreadsheet_column_distributed) ? 2 : $merge_length;
						
						if (!$version) {
							$merge_length = ($material_partial_columns[$planning][$collection][$row['id']] && $row['quantity_distributed']) ? 3 : $merge_length;
						}
							
						// item caption
						$referenced_columns[$i]['merge'] = $merge_length;
						$referenced_columns[$i]['code'] = $row['code'];
							
						// next item index
						$item_colum_index = $i+$merge_length;
						
						// planned column
						if ($spreadsheet_column_planned) {
							$referenced_columns[$i]['key'] = COLUMN_PLANNED;
							$referenced_columns[$i]['id'] = $row['id'];
							$referenced_columns[$i]['quantity'] = $row['quantity_approved'];
							$referenced_columns[$i]['quantity_owner'] = $row['quantity'];
							$referenced_columns[$i]['data'] = $planned_quantities;
							$referenced_columns[$i]['warehouse'] = $planned_warehouse_quantities;
							$i++;
						}
						
						// delivered column
						if ($spreadsheet_column_distributed) {
							$referenced_columns[$i]['key'] = COLUMN_DELIVERED;
							$referenced_columns[$i]['id'] = $row['id'];
							$referenced_columns[$i]['quantity'] = $row['quantity_confirmed'];
							$referenced_columns[$i]['quantity_distributed'] = $row['quantity_distributed'];
							$referenced_columns[$i]['data'] = $delivered_quantities;
							$referenced_columns[$i]['warehouse'] = $delivered_warehouse_quantities;
							$i++;
						}
						
						// partiale delivered column
						if (!$version && $material_partial_columns[$planning][$collection][$row['id']]) {
							$referenced_columns[$i]['key'] = COLUMN_PARTIAL_DELIVERED;
							$referenced_columns[$i]['id'] = $row['id'];
							$referenced_columns[$i]['data'] = $delivered_quantities_not_confirmed;
							$referenced_columns[$i]['quantity'] = $row['quantity_confirmed'];
							$referenced_columns[$i]['quantity_notused'] = $row['quantity_confirmed'] - $row['quantity_distributed'];
							$referenced_columns[$i]['warehouse'] = $delivered_warehouse_quantities_not_confirmed;
							$i++;
						}
					}
					
					// next first item index
					$item_colum_index = $i;
				}
				
				// increment 
				$i = $i+$colgroup_separator;
				
				// next first item index
				$item_colum_index = $i;

				// colgroup separator
				$total_colgroup_separators = $total_colgroup_separators + $colgroup_separator;
			}
		}
		
		// remove last seperator
		if ($total_colgroup_separators) $total_colgroup_separators = $total_colgroup_separators - 1;
		
		// totall column references
		$total_referenced_columns = count($referenced_columns) + $total_colgroup_separators;
	}
	
	// row references separator
	$referenced_columns_separator = 0;
	
	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Extend Rows
	Order sheet warehouses
********************************************************************************************************************************************************************************************/
	
	$extended_rows = array();
	
	if ($spreadsheet_show_extended_rows) {
		
		$extended_rows[0] = "Reserve";
		
		// load all active company warehouses
		$company_warehouses = $ordersheet->company()->warehouse()->load();
		$company_warehouses = _array::datagrid($company_warehouses);
		
		// load all ordersheet registred warehouses
		$ordersheet_warehouses = $ordersheet->warehouse()->load();
		
		if ($ordersheet_warehouses) {
		
			$i = 1;
		
			foreach ($ordersheet_warehouses as $row) {
				
				$warehouse = $row['mps_ordersheet_warehouse_address_warehouse_id'];
				$stockReserve = $row['mps_ordersheet_warehouse_stock_reserve'];
				
				$extended_rows[$i]['id'] = $row['mps_ordersheet_warehouse_id'];
				$extended_rows[$i]['name'] = $company_warehouses[$warehouse]['address_warehouse_name'];
				
				if ($stockReserve) {
					$extended_rows[$i]['reserve'] = true;
				}
				
				$i++;
			}
		}
		
		// total extended rows
		$total_extended_rows = count($extended_rows);
		
		// extended rows separator
		$extended_rows_separator = ($total_extended_rows) ? 1 : 0;
	}
	
	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Extend Columns
	Not used
********************************************************************************************************************************************************************************************/
	
	// total extended columns
	$total_extended_columns = 0;
	
	// extended columns separator
	$extended_columns_separator = ($total_extended_columns) ? 1 : 0;
	
	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Calcullated Rows
	Order sheet warehouses
********************************************************************************************************************************************************************************************/
	
	$calcullated_rows = array();
	$i=0;

	if ($spreadsheet_row_planning) {
		$calcullated_rows[$i] = ($spreadsheet_mode_planning) ? "Total Planned" : "Total Planned/Distributed";
	}
	
	if ($spreadsheet_row_owner_quantity) {
		$i++;
		$calcullated_rows[$i] = "Quantity";
	}
	
	if ($spreadsheet_row_confirmed_quantity) {
		$i++;
		$calcullated_rows[$i] = ($spreadsheet_mode_planning) ? "Total Approved" : "Total Shipped";
	}
	
	if ($spreadsheet_row_unused_quantity) {
		$i++;
		$calcullated_rows[$i] = ($spreadsheet_mode_planning) ? "Available for Planning" : "Available for Distribution";
	}
	
	// total calculated rows
	$total_calcullated_rows = count($calcullated_rows);
	
	// calculated rows separator
	$calcullated_rows_separator = ($total_calcullated_rows) ? 1 : 0;
	
	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Calcullated Columns
	Not used
********************************************************************************************************************************************************************************************/
	
	// total calcullated columns
	$total_calcullated_columns = 0;
	
	// calculated columns separator
	$calcullated_columns_separator = ($total_calcullated_columns) ? 1 : 0;

	
/*******************************************************************************************************************************************************************************************
	SPREADSHEED DATA PREPARATION
********************************************************************************************************************************************************************************************/	
	
	// total horizontal separators
	$total_rows_separators = $referenced_rows_separator + $extended_rows_separator + $calcullated_rows_separator;
	
	// total vertical separators
	$total_columns_separators = $referenced_columns_separator + $extended_columns_separator + $calcullated_columns_separator;
	
	// total fixed rows
	$total_fixed_rows = $fixed_rows + $referenced_rows_separator;
	
	// last fixed row
	$last_fixed_row = $total_fixed_rows;
	
	// referenced row separator
	$cel_referenced_row_separator = $last_fixed_row;
	
	// first referenced row
	$first_referenced_row = $last_fixed_row + 1;
	
	// last referenced row
	$last_referenced_row = $first_referenced_row + $total_referenced_rows - 1;
	
	// extended row
	if ($total_extended_rows) {
		
		// extended row separator
		$cel_extended_row_separator = $last_referenced_row + 1;
			
		// first extended row
		$first_extended_row = $last_referenced_row + $extended_rows_separator + 1;
			
		// last extended row
		$last_extended_row = $first_extended_row + $total_extended_rows - 1;
	}
	
	// calcullated rows
	if ($total_calcullated_rows) {
		
		// calcullated row separator
		$cel_calcullated_row_separator = $last_referenced_row + $extended_rows_separator + $total_extended_rows + 1;
			
		// first calcullated row
		$first_calcullated_row = $last_referenced_row + $extended_rows_separator + $total_extended_rows + $calcullated_rows_separator + 1;
			
		// last calcullated row
		$last_calcullated_row = $first_calcullated_row + $total_calcullated_rows - 1;
	}
	
	// planned calcullation row
	if ($last_referenced_row) {
		$planned_calcullation_row = $last_referenced_row + $extended_rows_separator + $total_extended_rows + $calcullated_rows_separator + 1;
	}
	
	// owner calullated row
	if ($spreadsheet_row_owner_quantity) {
		$owner_calcullated_row = $planned_calcullation_row + 1;
	}
	
	// approved calcullation row
	if ($spreadsheet_row_confirmed_quantity) {
		$approved_calcullation_row = ($owner_calcullated_row) ? $planned_calcullation_row+2 : $planned_calcullation_row+1;
	}
	
	if ($spreadsheet_row_unused_quantity && ($owner_calcullated_row || $approved_calcullation_row)) {
		if ($approved_calcullation_row) $unused_calcullation_row = $approved_calcullation_row + 1;
		else $unused_calcullation_row = ($owner_calcullated_row) ? $owner_calcullated_row+1 : $planned_calcullation_row+1;
	}

	// total fixed column
	$total_fixed_columns = $fixed_columns + $referenced_columns_separator;

	// last fixed column
	$last_fixed_column = $total_fixed_columns;
	
	// referenced colum separator
	$cel_referenced_column_separator = $total_fixed_columns;
	
	// first referenced column
	$first_referenced_column = $last_fixed_column + 1;
	
	// last referenced row
	$last_referenced_column = $first_referenced_column + $total_referenced_columns - 1;
	
	// extended columns
	if ($total_extended_columns) {
		
		// extended column
		$cel_extended_column_separator = $last_referenced_column + 1;
	
		// first extended row
		$first_extended_column = $last_referenced_column + $extended_columns_separator + 1;
	
		// last extended row
		$last_extended_column = $first_extended_column + $total_extended_columns - 1;
	}
	
	// calcullated columns
	if ($total_calcullated_columns) {
		
		// calullated column separator
		$cel_calcullated_column_separator = $last_referenced_column + $extended_columns_separator + $total_extended_columns + 1;
	
		// first calcullated row
		$first_calcullated_column = $last_referenced_column + $extended_columns_separator + $total_extended_columns + $calcullated_columns_separator + 1;
	
		// last calcullated row
		$last_calcullated_column = $first_calcullated_column + $total_calcullated_columns - 1;
	}

	// total spreadsheet rows
	$total_spreadsheet_rows = $fixed_rows + $total_referenced_rows + +$total_extended_rows + $total_calcullated_rows + $total_rows_separators;
	
	// total spreadsheet columns
	$total_spreadsheet_columns = $fixed_columns + $total_referenced_columns + $total_extended_columns + $total_calcullated_columns + $total_columns_separators;

	
	
/*******************************************************************************************************************************************************************************************
	PHPExcel BUILDER
********************************************************************************************************************************************************************************************/
	
	
	require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
	require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle($pagetitle);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	
	$styleBorder = array(
		'borders' => array( 
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);

	
	$stylePlanningType = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 14, 
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'EFEFEF')
		)
	);
	
	$styleCollection = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12, 
			'bold'=>true,
			'color' => array(
				'rgb' => '666666'
			)
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'EFEFEF')
		)
	);
	
	$styleVerticalText = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_BOTTOM,
			'rotation' => 90,
			'wrap'=> true
		)
	);
	
	$stylePosType = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12,
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'EFEFEF')
		)
	);
	
	$stylePlanned = array(
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'DBEFFF')
		)
	);
	
	$styleDelivered = array(
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'FFFAD6')
		)
	);
	

/*******************************************************************************************************************************************************************************************
	SPREADSHEED BUILDER
********************************************************************************************************************************************************************************************/
	
	if ($total_referenced_rows && $total_referenced_columns) {
		
		$export = true;
		
		$sum = array();
		$lastColumn = spreadsheet::key($total_spreadsheet_columns);
	
		for ($row=1; $row <= $total_spreadsheet_rows; $row++) {
			
			for ($col=1; $col <= $total_spreadsheet_columns; $col++) {
	
				$column = spreadsheet::key($col);
				$index = $column.$row;
				$step = ($step==$step_max) ? 1 : $step;
	
				switch ($row) {

					// spreadsheet title
					case 1:
						if ($col==1) {
							
							// merge cells
							$range = "$index:$lastColumn$row";
							$objPHPExcel->getActiveSheet()->mergeCells($range);
							
							// content & styling
							$objPHPExcel->getActiveSheet()->setCellValue($index, $ordersheet->header());
							$objPHPExcel->getActiveSheet()->getStyle($index)->getFont()->setBold(true);
							$objPHPExcel->getActiveSheet()->getStyle($index)->getFont()->setSize(20);
							
							// column dimension
							$objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth(50);
						}
						
					break;
					
					// spreadsheet title separator
					case 2:
						
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->mergeCells($index.':'.$lastColumn.$row);
						}
						
					break;
							
					// planning type captions
					case 3;
						
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
						}
						elseif ($col >= $first_referenced_column && $col <= $last_referenced_column) {
							
							// column dimension
							$column_width = ($col==$separator_key) ? 5 : 10;
							$objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth($column_width);
							
							// increment
							$i = $col - $first_referenced_column;
							$j = $col-$first_referenced_column;
							$caption = $planning_type_captions[$i]['planning'];
							$merge_length = $planning_type_captions[$i]['merge'];
														
							if ($caption) {
								
								// merge planning type cell
								if ($merge_length > 1) {
									$merge_cell = spreadsheet::index($i+$merge_length); 
									$objPHPExcel->getActiveSheet()->mergeCells($index.':'.$merge_cell.$row);
									$objPHPExcel->getActiveSheet()->getStyle($index.':'.$merge_cell.$row)->applyFromArray($styleBorder);
								}
							
								// content
								$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($stylePlanningType+$styleBorder, false);

								// next colgroup seperator
								$separator_key = $col + $merge_length;
							}
							
							// merge colgroup columns
							if ($col==$separator_key) {
								$objPHPExcel->getActiveSheet()->mergeCells("$index:$column$last_calcullated_row");
							}
						}
						
					break;
					
					// collection captions
					case 4;
						
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
						}
						elseif ($col >= $first_referenced_column && $col <= $last_referenced_column) {
							
							// increment
							$i = $col-$first_referenced_column;
							
							if ($planning_type_captions[$i]) {
								
								$caption = $planning_type_captions[$i]['collection'];
								$merge_length = $planning_type_captions[$i]['merge'];
							
								// merge planning type cell
								if ($merge_length > 1) {
									$merge_cell = spreadsheet::index($i+$merge_length);
									$objPHPExcel->getActiveSheet()->mergeCells($index.':'.$merge_cell.$row);
									$objPHPExcel->getActiveSheet()->getStyle($index.':'.$merge_cell.$row)->applyFromArray($styleBorder);
								}
							
								// content
								$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleCollection+$styleBorder, false);
							}
						}
						
					break;
					
					// material references
					case 5:
						
						// row diemension
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(160);
						}
	
						// references
						if ($col >= $first_referenced_column && $col <= $last_referenced_column) {
	
							$i = $col - $first_referenced_column;
							
							if ($referenced_columns[$i]) {
								
								$item_id = $referenced_columns[$i]['id'];
								$merge_length = $referenced_columns[$i]['merge'];
								$caption = $referenced_columns[$i]['code'];
								
								// merge item label
								if ($merge_length > 1) {
									$merge_cell = spreadsheet::index($col-$first_referenced_column+$merge_length);
									$objPHPExcel->getActiveSheet()->mergeCells($index.':'.$merge_cell.$row);
									$objPHPExcel->getActiveSheet()->getStyle($index.':'.$merge_cell.$row)->applyFromArray($styleBorder);
								}

								// content && styling
								$objPHPExcel->getActiveSheet()->setCellValue($index, '     '.$caption);
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleVerticalText+$styleBorder, false);
							}
						}
	
					break;
					
					// crossing: referenced rows referenced columns
					case $row >= $first_referenced_row && $row <= $last_referenced_row :
							
						switch ($col) {
								
							// pos caption
							case 1:
								
								$i = $row - $first_referenced_row;								
								$caption = $referenced_rows[$i]['caption'];
								$group = $referenced_rows[$i]['group'];

								// content && styling
								$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);		

								if ($group) {
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($stylePosType+$styleBorder, false);
								} else {
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleBorder, false);
								}
	
							break;
	
							// datagrid
							case $col >= $first_referenced_column && $col <= $last_referenced_column :
	
								$i = $row - $first_referenced_row;
								$j = $col - $first_referenced_column;
								$group = $referenced_rows[$i]['group'];
								$pos = $referenced_rows[$i]['pos'];
								$material = $referenced_columns[$j]['id'];
					
								if (!$group) {
									
									$key = $referenced_columns[$j]['key'];
									$partial_column = ($referenced_columns[$j+1]['key']==COLUMN_PARTIAL_DELIVERED) ? true : false;
									$quantity = $referenced_columns[$j]['data'][$pos][$material]['quantity'];
									
									if ($material) {
										
										$sum[$col] = $sum[$col] + $quantity;
				
										// content && styling	
										$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
										
										// style borders
										$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleBorder, false);
										
										// distribution mode
										if ($spreadsheet_column_distributed) {
											
											// planned column
											if ($key==COLUMN_PLANNED) {
												$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($stylePlanned, false);
											}
											//delivered column
											elseif($key==COLUMN_DELIVERED) {
												if ($delivered_materials[$material] || $partial_column) {
													$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleDelivered, false);
												}
											}
										}
									}
								}
								else {
									
									if ($planning_type_captions[$j]) {
										$merge_cell = $j+$planning_type_captions[$j]['merge'];
										$range = $index.':'.spreadsheet::index($merge_cell, $row);
										$objPHPExcel->getActiveSheet()->mergeCells($range);
										$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($stylePosType+$styleBorder, false);
									}
								}
		
							break;
						}
	
					break;
					
					case $first_extended_row:
						
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->setCellValue($index, $extended_rows[0]);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($stylePosType+$styleBorder, false);
						}
						elseif ($col >= $first_referenced_column && $col <= $last_referenced_column) {
							
							$j = $col - $first_referenced_column;
							
							if ($planning_type_captions[$j]) {
								$merge_cell = $j+$planning_type_captions[$j]['merge'];
								$range = $index.':'.spreadsheet::index($merge_cell, $row);
								$objPHPExcel->getActiveSheet()->mergeCells($range);
								$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($stylePosType+$styleBorder, false);
							}
						}
																		
					break;
					
					// extended rows
					case $row > $first_extended_row && $row <= $last_extended_row :
						
						$i = $row - $first_extended_row;
						$j = $col - $first_referenced_column;
						
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->setCellValue($index, $extended_rows[$i]['name']);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleBorder, false);
						}
						elseif ($col >= $first_referenced_column && $col <= $last_referenced_column) {

							if ($referenced_columns[$j]) {
									
								$key = $referenced_columns[$j]['key'];
								$material = $referenced_columns[$j]['id'];
								$warehouse_id = $extended_rows[$i]['id'];
								$partial_column = ($referenced_columns[$j+1]['key']==COLUMN_PARTIAL_DELIVERED) ? true : false;
								
								// reset quantity if material is deliverd
								$quantity = ($key==COLUMN_DELIVERED && $delivered_materials[$material] && !$version) ? null : $referenced_columns[$j]['warehouse'][$warehouse_id][$material]['quantity'];
								
								// sum warehouse quantities
								$sum[$col] = $sum[$col] + $quantity;
								
								$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);

								// distribution mode
								if ($spreadsheet_column_distributed) {
								
									// planned column
									if ($key==COLUMN_PLANNED) {
										$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($stylePlanned, false);
									}
									//delivered column
									elseif($key==COLUMN_DELIVERED) {
										if ($delivered_materials[$material] || $partial_column) {
											$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleDelivered, false);
										}
									}
								}
								
								// style borders
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleBorder, false);
							}
						}
						
					break;
					
					// datagrid separator
					case $cel_calcullated_row_separator:
						
						if ($col >= $first_referenced_column && $col <= $last_referenced_column) {
								
							$j = $col - $first_referenced_column;
								
							if ($planning_type_captions[$j]) {
								$merge_cell = spreadsheet::index($j+$planning_type_captions[$j]['merge']);
								$objPHPExcel->getActiveSheet()->mergeCells($index.':'.$merge_cell.$row);
							}
						}
					break;
					
					// planned calcullated rows
					case $planned_calcullation_row:
						
						$i = $row-$first_calcullated_row;
						$j = $col-$first_referenced_column;
						
						if ($col==1) {
							$caption = $calcullated_rows[$i];
							$objPHPExcel->getActiveSheet()->setCellValue($index, $calcullated_rows[0]);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($stylePosType+$styleBorder, false);
						}
						elseif ($col >= $first_referenced_column && $col <= $last_referenced_column) {
							
							if ($referenced_columns[$j]) {
								
								// distribution mode
								if ($spreadsheet_column_distributed) {
									
									$key = $referenced_columns[$j]['key'];
									$next_key = $referenced_columns[$j+1]['key'];
									$quantity = $referenced_columns[$j]['quantity'];
									$material = $referenced_columns[$j]['id'];
									
									if ($key==COLUMN_PLANNED) {
										
										$value = ($sum[$col]) ? $sum[$col] : null;
										$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
										$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($stylePlanned+$styleBorder, false);
									}
									elseif($key==COLUMN_DELIVERED) {
										
										$value = ($next_key==COLUMN_PARTIAL_DELIVERED) ? $referenced_columns[$j]['quantity_distributed'] : $sum[$col];
										if ($value) $objPHPExcel->getActiveSheet()->setCellValue($index, $value);
										
										if ($next_key==COLUMN_PARTIAL_DELIVERED || $delivered_materials[$material]) {
											$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleDelivered+$styleBorder, false);
										} else {
											$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleBorder, false);
										}
									}
									elseif($key==COLUMN_PARTIAL_DELIVERED) {
										$value = ($sum[$col]) ? $sum[$col]+$referenced_columns[$j]['quantity_distributed'] : null;
										if ($value) $objPHPExcel->getActiveSheet()->setCellValue($index, $value);
										$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleBorder, false);
									}
								}
								// planning mode
								else {
									$value = ($sum[$col]) ? $sum[$col] : null;
									$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleBorder, false);
								}
							}
						}
						
			
					break;
					
					// owner calullated row
					case $owner_calcullated_row:
						
						$i = $row-$planned_calcullation_row;
						$j = $col-$first_referenced_column;
						
						if ($col==1) {
							$caption = $calcullated_rows[$i];
							$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($stylePosType+$styleBorder, false);
						}
						elseif ($col >= $first_referenced_column && $col <= $last_referenced_column) {
							
							if ($referenced_columns[$j]) {
								$value = $referenced_columns[$j]['quantity_owner'];								
								$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleBorder, false);
							
							}
						}
						
					break;
					
					// approved calcullated rows
					case $approved_calcullation_row:
						
						$i = $row-$planned_calcullation_row;
						$j = $col-$first_referenced_column;
						
						if ($col==1) {
							$caption = $calcullated_rows[$i];
							$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($stylePosType+$styleBorder, false);
						}
						elseif ($col >= $first_referenced_column && $col <= $last_referenced_column) {
								
							if ($referenced_columns[$j]) {
						
								$key = $referenced_columns[$j]['key'];
								$next_key = $referenced_columns[$j+1]['key'];
								$quantity = $referenced_columns[$j]['quantity'];
								$material = $referenced_columns[$j]['id'];
								
								// distributtion mode
								if ($spreadsheet_column_distributed) {
									
									if ($key<>COLUMN_PLANNED) {
										if ($delivered_materials[$material]) {
											$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
											$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleDelivered, false);
										}
										elseif ($next_key<>COLUMN_PARTIAL_DELIVERED) {
											$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
										}
									}
								}
								// planning mode
								else {
									if ($quantity) {
										$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
									}
								}
								
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleBorder, false);
							}
						}
			
					break;
					
					// unused cquantities
					case $unused_calcullation_row;
					
						$i = $row-$planned_calcullation_row;
						$j = $col-$first_referenced_column;
					
						if ($col==1) {
							$caption = $calcullated_rows[$i];
							$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($stylePosType+$styleBorder, false);
						}
						elseif($col >= $first_referenced_column && $col <= $last_referenced_column) {
							
							if ($referenced_columns[$j]) {
								
								$key = $referenced_columns[$j]['key'];
								$next_key = $referenced_columns[$j+1]['key'];
								$quantity = $referenced_columns[$j]['quantity'];
								$quantity_unused = $referenced_columns[$j]['quantity_notused'];
								$material = $referenced_columns[$j]['id'];
								
								// distribution mode
								if ($spreadsheet_column_distributed) {
									
									// delivered colum
									if ($key==COLUMN_DELIVERED && $next_key<>COLUMN_PARTIAL_DELIVERED) {
										$value = ($sum[$col]>0) ?  $quantity-$sum[$col]: $quantity;
										if ($value) $objPHPExcel->getActiveSheet()->setCellValue($index, $value);
									}
									// partial column
									elseif($key==COLUMN_PARTIAL_DELIVERED) {
										$quantity_delivered = $referenced_columns[$j-1]['quantity_distributed'];
										$value = ($sum[$col]) ?  $quantity-$sum[$col] : $quantity-$quantity_delivered;
										if ($value)  $objPHPExcel->getActiveSheet()->setCellValue($index, $value);
									}
								}
								// planning mode
								else {
									
									if ($quantity) {
										$value = ($sum[$col] && $quantity) ? $quantity-$sum[$col] : $quantity;
	 									$objPHPExcel->getActiveSheet()->setCellValue($index, $value);	 									
									}
								}
								
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleBorder, false);
							}
						}
						
					break;
					
				} // end switch row 
			} // end each column
		} // end each row
		
		// merge cells
		$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
		
		if ($spreadsheet_column_distributed) {
			
			$row++;
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->applyFromArray($stylePlanned);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$row,"Planned Quantities");
			
			$row++;
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->applyFromArray($styleDelivered);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$row,"Delivered Quantities");
		}
		
	} // end if total rows
		
/*******************************************************************************************************************************************************************************************
	EXPORT
********************************************************************************************************************************************************************************************/
	
	if ($export) {
		
		// file name
		$filename = 'ordersheet_planning_'.date('Ymd');
		
		// set active sheet
		$objPHPExcel->getActiveSheet()->setTitle($pagetitle);
		$objPHPExcel->setActiveSheetIndex(0);

		// phpexcel headers
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}
	else {
		message::empty_result();
		url::redirect("/$application/$controller$archived/$action/$id");
	}
	