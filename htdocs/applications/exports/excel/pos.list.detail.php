<?php
/*
   Export POS List

    Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2011-03-22
    Modified by:    Roger Bucher
    Date modified:  2012-10-22
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*/
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	define('SHEET_NAME', "POS List");
	define('SHEET_FILE_NAME', "pos_detail_list_".date('Y-m-d'));

	$settings = Settings::init();
	$settings->load('data');
	
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$filter = $_REQUEST['filter'];

	$model = new Model(Connector::DB_CORE);

	// request execution time
	ini_set('max_execution_time', 300);
	ini_set('memory_limit', '2048M');

	$_REQUEST = session::filter($application, "$controller.$archived.$action", false);
	
	// title
	$title = ($_REQUEST['archived']) ? "Closed POS Detail List" : "POS Detail List";

	// sql order
	$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "country_name, place_name, posaddress_name";
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

	// filter: address parents
	if($user->permission(Pos::PERMISSION_VIEW_LIMITED)) {
		$filters['parent'] = "posaddress_client_id = ".$user->address;
	}

	// filter: country
	if($_REQUEST['countries']) {
		
		$filters['countries'] = "posaddress_country = ".$_REQUEST['countries'];
		
		$country = new Country();
		$country->read($_REQUEST['countries']);
		
		$captions[] = $translate->country.": ".$country->name;
	}
	elseif($user->permission(Pos::PERMISSION_VIEW_LIMITED)) {
		
		$result = $model->query("
			SELECT DISTINCT posaddress_country
			FROM posaddresses
			WHERE posaddress_client_id = ".$user->address."
		")->fetchAll();

		if ($result) {

			foreach ($result as $row) {
				$countries[] = $row['posaddress_country']; 
			}

			$user_countries = join(',', $countries);
			$filters['countries'] = "posaddress_country IN ($user_countries)";
		}
	}

	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {

		$keyword = $_REQUEST['search'];

		$filters['search'] = "(
			posaddress_name LIKE '%$keyword%'
			OR posaddress_street LIKE '%$keyword%',
			OR country_name LIKE '%$keyword%'
			OR province_canton LIKE '%$keyword%'
			OR place_name LIKE '%$keyword%'
			OR posaddress_zip LIKE '%$keyword%'
			OR postype_name LIKE '%$keyword%'
		)";
		
		$captions[] = $traslate->search.': '.$keyword;
	}

	// filter: pos type
	if ($_REQUEST['pos_types']) 	{
		
		$filters['pos_types'] = "posaddress_store_postype = ".$_REQUEST['pos_types'];
		
		$pos_type = new Pos_Type();
		$pos_type->read($_REQUEST['pos_types']);
		
		$captions[] = $translate->posaddress_store_postype.": ".$pos_type->name;
	}

	// filter: legal types (pos owner types)
	if ($_REQUEST['posowner_types']) {

		$legalTypes = join(',', array_keys($_REQUEST['posowner_types']));
		$filters['posowner_types'] = "posaddress_ownertype IN ($legalTypes)";

		$res = $model->query("SELECT GROUP_CONCAT(posowner_type_name) AS caption FROM db_retailnet.posowner_types WHERE posowner_type_id IN ($legalTypes)")->fetch();
	}

	// filter: legal types (pos owner types)
	if ($_REQUEST['possubclasses']) {

		$subclasses = join(',', array_keys($_REQUEST['possubclasses']));
		$filters['possubclasses'] = "posaddress_store_subclass IN ($subclasses)";
		
		$res = $model->query("SELECT GROUP_CONCAT(possubclass_name) AS caption FROM db_retailnet.possubclasses WHERE possubclass_id IN ($subclasses)")->fetch();
		$captions[] = "POS Type Subclasses: {$res[caption]}";
	}

	// filter: channel
	if ($_REQUEST['distribution_channels']) {
		
		if ($_REQUEST['distribution_channels']=='null') {
			
			$filters['distribution_channels'] = "(
				posaddress_distribution_channel = NULL
				OR posaddress_distribution_channel = ''
				OR posaddress_distribution_channel = 0
			)";
			
			$captions[] = $translate->posaddress_distribution_channel.": No Distribution Channel Assigned";
			
		} else {
			
			$distribution_channel = new DistChannel($application);
			$distribution_channel->read($_REQUEST['distribution_channels']);
			
			$filters['distribution_channels'] = "posaddress_distribution_channel=".$_REQUEST['distribution_channels'];
			
			$captions[] = $translate->posaddress_distribution_channel.": ".$distribution_channel->group.', '.$distribution_channel->code;
		}
		
		$binds['posaddress_distribution_channel'] = $properties['posaddress_distribution_channel']['bind'];
	}

	// filter: sales representative
	if ($_REQUEST['sales_representative']) {

		$filters['sales_representative'] = "posaddress_sales_representative = ".$_REQUEST['sales_representative'];
		
		$staff = new Staff($application);
		$staff->read($_REQUEST['sales_representative']);
		
		$captions[] = $translate->posaddress_sales_representative.": ".$staff->firstname.' '.$staff->name;
	}

	// filter: decoration person
	if ($_REQUEST['decoration_person']) {
		
		$filters['decoration_person'] = "posaddress_decoration_person=".$_REQUEST['decoration_person'];
		
		$staff = new Staff($application);
		$staff->read($_REQUEST['decoration_person']);
		
		$captions[] = $translate->posaddress_decoration_person.": ".$staff->firstname.' '.$staff->name;
	}

	// filter: turnoverclass_watches
	if ($_REQUEST['turnoverclass_watches']) {
		
		$filters['turnoverclass_watches'] = "posaddress_turnoverclass_watches=".$_REQUEST['turnoverclass_watches'];
		
		$turnover_class = new TurnoverClass($application);
		$turnover_class->read($_REQUEST['turnoverclass_watches']);
		
		$captions[] = $translate->posaddress_turnoverclass_watches.": ".$turnover_class->name;
	}

	// filter: turnoverclass bijoux
	if ($_REQUEST['turnoverclass_bijoux']) {
		
		$filters['turnoverclass_bijoux'] = "posaddress_turnoverclass_bijoux=".$_REQUEST['turnoverclass_bijoux'];
		
		$turnover_class = new TurnoverClass($application);
		$turnover_class->read($_REQUEST['turnoverclass_bijoux']);
		
		$captions[] = $translate->posaddress_turnoverclass_bijoux.": ".$turnover_class->name;
	}


	// filter: product lines
	if ($_REQUEST['product_lines']) {

		$productLines = join(',', array_keys($_REQUEST['product_lines']));
		$filters['product_lines'] = "posaddress_store_furniture IN ($productLines)";
		
		$res = $model->query("SELECT GROUP_CONCAT(product_line_name) AS caption FROM db_retailnet.product_lines WHERE product_line_id IN ($productLines)")->fetch();
		$captions[] = $translate->posaddress_store_furniture.": ".$res['caption'];
	}

	// filter: store state
	if ($archived) {
		$filters['archived'] = "(
			posaddress_store_closingdate <> '0000-00-00'
			OR posaddress_store_closingdate <> NULL
		)";
	}
	else {
		$filters['active'] = "(
			posaddress_store_closingdate = '0000-00-00'
			OR posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = ''
		)";
	}
	
	// country access filter
	if (!$_REQUEST['countries']) {
	
		$accessCountries = array();
	
		$result = $model->query("
			SELECT DISTINCT country_access_country
			FROM db_retailnet.country_access
			WHERE country_access_user = $user->id
		")->fetchAll();
	
		if ($result) {
			foreach ($result as $row) {
				$accessCountries[] = $row['country_access_country'];
			}
		}
	}
	
	
	// country access extended filter
	if (!$_REQUEST['countries'] && $accessCountries) {
	
		if ($filters) {
	
			$basicFiletrs = $filters;
			unset($basicFiletrs['parent']);
			unset($basicFiletrs['countries']);
	
			$extendedFilter = " OR ( ".join(' AND ', $basicFiletrs)."  AND posaddress_country IN ( ".join(' AND ', $accessCountries).") )";
	
		} else {
	
			$filters['countries'] = "posaddress_country IN ( ".join(',',$accessCountries)." )";
		}
	}
	
	

	// datagrid
	$result = $model->query("
		SELECT
			posaddress_id,
			country_name,
			IF(posaddress_name <> '', posaddress_name, 'n.a.') as posaddress_name,
			posaddress_address,
			place_name,
			mps_distchannel_name,
			mps_distchannel_code,
			postype_name,
			posaddress_store_furniture,
			posaddress_store_furniture_subclass,
			posaddress_turnoverclass_bijoux,
			posaddress_turnoverclass_watches,
			posaddress_sales_classification01,
			posaddress_sales_classification02,
			posaddress_sales_classification03,
			posaddress_sales_classification04,
			posaddress_sales_classification05
		FROM posaddresses
		INNER JOIN db_retailnet.addresses ON address_id = posaddress_franchisee_id
		INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
		INNER JOIN db_retailnet.provinces ON province_id = place_province
		INNER JOIN db_retailnet.countries ON country_id = posaddress_country
		LEFT JOIN db_retailnet.posowner_types ON posowner_type_id = posaddress_ownertype
		LEFT JOIN db_retailnet.postypes ON postype_id = posaddress_store_postype
		LEFT JOIN db_retailnet.mps_distchannels ON mps_distchannel_id = posaddress_distribution_channel
		LEFT JOIN db_retailnet.mps_turnoverclasses ON mps_turnoverclass_id = posaddress_turnoverclass_watches
	")
	->filter($filters)
	->extend($extendedFilter)
	->order($order, $direction)
	->fetchAll();

	if ($result) {
		
		require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
		require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

		$datagrid = _array::datagrid($result);
		$columns = array_keys(end($datagrid));
		$totalColumns = count($columns);
		
		$turnover_classes = TurnoverClass::loader($application);
		$product_lines = Product_Line::loader();
		$product_line_subclasses = Product_Line_Subclass::loader();
		
		foreach ($datagrid as $key => $row) {
			$datagrid[$key]['posaddress_store_furniture'] = $product_lines[$row['posaddress_store_furniture']];
			$datagrid[$key]['posaddress_store_furniture_subclass'] = $product_line_subclasses[$row['posaddress_store_furniture_subclass']];
			$datagrid[$key]['posaddress_turnoverclass_bijoux'] = $turnover_classes[$row['posaddress_turnoverclass_bijoux']];
			$datagrid[$key]['posaddress_turnoverclass_watches'] = $turnover_classes[$row['posaddress_turnoverclass_watches']];
		}
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle(SHEET_NAME);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(25);
		$lastColumn = 'P';

		// styling
		$border = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
		);

		$styleNumber = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
		);


		$styleString = array(
			'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
		);


		$styleHeaderString = array(
			'font' => array('name'=>'Arial','size'=> 10, 'bold'=>true),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> true),
			'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'EFEFEF')),
			'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
		);
		
		$styleTableHeader = array(
			'font' => array('name'=>'Arial','size'=> 18, 'bold'=>true, 'color' => array('rgb'=>'000000')),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> true),
			'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'00b0f0')),
			'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
		);


		$styleHeaderNumber = array(
			'font' => array('name'=>'Arial','size'=> 10, 'bold'=>true),
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,'wrap'=> true),
			'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'EFEFEF')),
			'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))
		);

		$row=1;
		$objPHPExcel->getActiveSheet()->mergeCells('A1:'.$lastColumn.'1');
		$objPHPExcel->getActiveSheet()->setCellValue("A1", $title);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize(18);

		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		$row++;
		$range = "A$row:$lastColumn"."$row";
		$date = date('d.m.Y  H:i:s');
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: $date");

		// filter captions
		if ($captions) {
			foreach ($captions as $caption) {
				$row++;
				$range = "A$row:".$lastColumn.$row;
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				$objPHPExcel->getActiveSheet()->setCellValue("A$row", $caption);
				$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
			}
		}


		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		$row++;
		$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30); 
		
		// columns
		foreach ($columns as $value) {
			$column = PhpExcel_Cell::stringFromColumnIndex($col);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $translate->$value);
			$col++;
		}
		
		$objPHPExcel->getActiveSheet()->getStyle("A$row:$lastColumn$row")->applyFromArray($styleHeaderString);
		
		// grid
		foreach ($datagrid as $i => $array) {

			$row++; 
			$col = 0;

			foreach($array as $key=>$value) {
				$column = PhpExcel_Cell::stringFromColumnIndex($col);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
				$col++;
			}
			
			$objPHPExcel->getActiveSheet()->getStyle("A$row:$lastColumn$row")->applyFromArray($border);
		}

		$row++;
		$range = "A$row:$lastColumn"."$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		$objPHPExcel->getActiveSheet()->setTitle($title);
		$objPHPExcel->setActiveSheetIndex(0);

		// redirect output to client browser
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.SHEET_FILE_NAME.'.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}
	else {
		message::empty_result();
		$archived = ($archived) ? '/archived' : null;
		url::redirect("/$application/$controller$archived/$action");
	}
