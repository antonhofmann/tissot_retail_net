<?php
/***********************************************************************************************************************************************
	
	Export POS List

    Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2011-03-22
    Modified by:    Roger Bucher
    Date modified:  2012-10-22
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
    
************************************************************************************************************************************************/
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

$settings = settings::init();
$settings->load('data');
$user = user::instance();
$translate = translate::instance();
	
ini_set('max_execution_time', 300);
ini_set('memory_limit', '2048M');

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$filter = $_REQUEST['filter'];
$group = $_REQUEST['group'];

// session request
$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

// request filter
$fCountry = $_REQUEST['countries'];
$fSearch = $_REQUEST['search'] && $_REQUEST['search']<>$translate->search ? $_REQUEST['search'] : null;
$fPosType = $_REQUEST['pos_types'];
$fPosOwnerType = $_REQUEST['posowner_types'];
$fPosSubClass = $_REQUEST['possubclasses'];
$fDistributionChannel = $_REQUEST['distribution_channels'];
$fSalesRepresentative = $_REQUEST['sales_representative'];
$fDecorationPerson = $_REQUEST['decoration_person'];
$fTurnoverWatch = $_REQUEST['turnoverclass_watches'];
$fTuronoverBijoux = $_REQUEST['turnoverclass_bijoux'];
$fProductLines = $_REQUEST['product_lines'];
$fClient = $_REQUEST['client'];

// database model
$model = new Model($application);

// spreadsheet title
$header = ($_REQUEST['archived']) ? "Closed POS Address List" : "POS Address List";

// spreadsheet file name
$filename = "pos_addresses_".date('Y_m_d');
$sheet = "POS List";
$YES = strtoupper($translate->yes);
$NO = strtoupper($translate->no);

// permissions
$permission_view = User::permission(Pos::PERMISSION_VIEW);
$permission_view_limited = User::permission(Pos::PERMISSION_VIEW_LIMITED);
$permission_edit = User::permission(Pos::PERMISSION_EDIT);
$permission_edit_limited = User::permission(Pos::PERMISSION_EDIT_LIMITED);

// has limited permission
$hasLimitedPermission = (!$permission_view && !$permission_edit) ? true : false;
	
$table = new DB_Table();
$table->read_from_name('posaddresses');
$table_fields = ($table->export_fields) ? unserialize($table->export_fields) : array();

// user selected fields
if ($_REQUEST['columns'] && $_REQUEST['modal']) {

	$fields = array_keys($_REQUEST['columns']);		
	$user_fields = serialize($fields);

	// read export list instance
	user::exportList()->read_from_table($table->id);

	if (user::exportList()->id) {
		user::exportList()->update(array(
		'user_export_list_fields' => $user_fields
		));
	} else {
		user::exportList()->create($table->id, $user_fields);
	}

} else {
	$fields = $table_fields;
}


if (!$archived && in_array('posaddress_store_closingdate', $fields)) {
	$key = array_search('posaddress_store_closingdate', $fields);
	unset($fields[$key]);
}
	
	
// field attributes
$properties = array(
	posaddress_id => array(
		'label' => 'ID',
		'width' => 3,
		'field' => 'posaddress_id AS id',
		'number' => true
	),
	company_id_divya => array(
		'label' => $translate->company_id_divya,
		'width' => 10
	),
	posaddress_client_id => array(
		'label' => $translate->posaddress_client_id,
		'width' => 40,
		'field' => 'clients.address_company AS posaddress_client_id',
		'bind' => 'LEFT JOIN db_retailnet.addresses clients ON clients.address_id = posaddress_client_id',
		'sort' => 'address_company'
	),
	posaddress_ownertype => array(
		'label' => $translate->posaddress_ownertype,
		'width' => 20,
		'field' => 'posowner_type_name AS posaddress_ownertype',
		'bind' => Pos::DB_BIND_POSOWNER_TYPES,
		'sort' => 'posowner_type_name'
	),
	posaddress_eprepnr => array(
		'label' => $translate->posaddress_eprepnr,
		'width' => 20
	),
	posaddress_sapnumber => array(
		'label' => $translate->posaddress_sapnumber,
		'width' => 20
	),
	posaddress_franchisor_id => array(
		'label' => $translate->posaddress_franchisor_id,
		'width' => 30,
		'field' => 'franchisor.address_company AS posaddress_franchisor_id',
		'bind' => 'LEFT JOIN db_retailnet.addresses franchisor ON franchisor.address_id = posaddress_franchisor_id',
		'sort' => 'address_company'
	),
	posaddress_franchisee_id => array(
		'label' => $translate->posaddress_franchisee_id,
		'width' => 30,
		'field' => 'franchisee.address_company AS posaddress_franchisee_id',
		'bind' => 'LEFT JOIN db_retailnet.addresses franchisee ON franchisee.address_id = posaddress_franchisee_id',
		'sort' => 'address_company'
	),
	address_sapnr => array(
		'label' => $translate->address_sapnr,
		'width' => 20,
		'field' => 'franchisee.address_company AS posaddress_franchisee_id',
		'bind' => 'LEFT JOIN db_retailnet.addresses franchisee ON franchisee.address_id = posaddress_franchisee_id',
		'sort' => 'address_company'
	),
	posaddress_name => array(
		'label' => $translate->posaddress_name,
		'width' => 40
	),
	posaddress_name2 => array(
		'label' => $translate->posaddress_name2,
		'width' => 40
	),
	posaddress_address => array(
		'label' => $translate->posaddress_address,
		'width' => 30
	),
	posaddress_address2 => array(
		'label' => $translate->posaddress_address2,
		'width' => 30
	),
	posaddress_zip => array(
		'label' => $translate->posaddress_zip,
		'width' => 10,
		'number' => true
	),
	posaddress_place_id => array(
		'label' => $translate->posaddress_place_id,
		'width' => 20,
		'field' => 'place_name AS posaddress_place_id',
		'bind' => Pos::DB_BIND_PLACES,
		'sort' => 'place_name'
	),
	posaddress_country => array(
		'label' => $translate->posaddress_country,
		'width' => 30,
		'field' => 'country_name AS posaddress_country',
		'bind' => Pos::DB_BIND_COUNTRIES,
		'sort' => 'country_name'
	),
	posaddress_province_id => array(
		'label' => $translate->posaddress_province_id,
		'width' => 30,
		'field' => 'province_canton AS posaddress_province_id',
		'bind' => Pos::DB_BIND_PROVINCES,
		'sort' => 'province_canton'
	),
	posaddress_phone => array(
		'label' => $translate->posaddress_phone,
		'width' => 20
	),
	posaddress_mobile_phone => array(
		'label' => $translate->posaddress_mobile_phone,
		'width' => 20
	),
	posaddress_email => array(
		'label' => $translate->posaddress_email,
		'width' => 40
	),
	posaddress_website => array(
		'label' => $translate->posaddress_website,
		'width' => 40
	),
	posaddress_contact_name => array(
		'label' => $translate->posaddress_contact_name,
		'width' => 30
	),
	posaddress_contact_email => array(
		'label' => $translate->posaddress_contact_email,
		'width' => 40
	),
	posaddress_fagagreement_type => array(
		'label' => $translate->posaddress_fagagreement_type,
		'width' => 20
	),
	posaddress_fagrsent => array(
		'label' => $translate->posaddress_fagrsent,
		'width' => 20
	),
	posaddress_fagrsigned => array(
		'label' => $translate->posaddress_fagrsigned,
		'width' => 20
	),
	posaddress_fagrstart => array(
		'label' => $translate->posaddress_fagrstart,
		'width' => 20
	),
	posaddress_fagrend => array(
		'label' => $translate->posaddress_fagrend,
		'width' => 20
	),
	posaddress_fargrduration => array(
		'label' => $translate->posaddress_fargrduration,
		'width' => 20
	),
	posaddress_fagcancellation => array(
		'label' => $translate->posaddress_fagcancellation,
		'width' => 20
	),
	posaddress_fag_territory => array(
		'label' => $translate->posaddress_fag_territory,
		'width' => 10
	),
	posaddress_fag_comment => array(
		'label' => $translate->posaddress_fag_comment,
		'width' => 20
	),
	posaddress_fag_city_pasted => array(
		'label' => $translate->posaddress_fag_city_pasted,
		'width' => 10
	),
	posaddress_takeover_date => array(
		'label' => $translate->posaddress_takeover_date,
		'width' => 10
	),
	posaddress_takeover_amount => array(
		'label' => $translate->posaddress_takeover_amount,
		'width' => 10
	),
	posaddress_takeover_currency => array(
		'label' => $translate->posaddress_takeover_currency,
		'width' => 5,
		'number' => true
	),
	posaddress_takeover_from => array(
		'label' => $translate->posaddress_takeover_from,
		'width' => 10
	),
	posaddress_takeover_comment => array(
		'label' => $translate->posaddress_takeover_comment,
		'width' => 20
	),
	posaddress_store_postype => array(
		'label' => $translate->posaddress_store_postype,
		'width' => 40,
		'field' => 'postype_name AS posaddress_store_postype',
		'bind' => Pos::DB_BIND_POSTYPES,
		'sort' => 'postype_name'
	),
	posaddress_store_subclass => array(
		'label' => $translate->posaddress_store_subclass,
		'width' => 20,
		'field' => 'possubclass_name AS posaddress_store_subclass',
		'bind' => 'LEFT JOIN db_retailnet.possubclasses ON possubclass_id = posaddress_store_subclass',
		'sort' => 'possubclass_name'
	),
	posaddress_store_furniture => array(
		'label' => $translate->posaddress_store_furniture,
		'width' => 40,
		'field' => 'product_line_name AS posaddress_store_furniture',
		'bind' => 'LEFT JOIN db_retailnet.product_lines ON product_line_id = posaddress_store_furniture',
		'sort' => 'product_line_name'
	),
	posaddress_store_furniture_subclass => array(
		'label' => $translate->posaddress_store_furniture_subclass,
		'width' => 30,
		'field' => 'productline_subclass_name AS posaddress_store_furniture_subclass',
		'bind' => 'LEFT JOIN db_retailnet.productline_subclasses ON productline_subclass_id = posaddress_store_furniture_subclass',
		'sort' => 'productline_subclass_name'
	),
	posaddress_store_totalsurface => array(
		'label' => $translate->posaddress_store_totalsurface,
		'width' => 20,
		'number' => true
	),
	posaddress_store_retailarea => array(
		'label' => $translate->posaddress_store_retailarea,
		'width' => 20
	),
	posaddress_store_backoffice => array(
		'label' => $translate->posaddress_store_retailarea,
		'width' => 20,
		'number' => true
	),
	posaddress_store_numfloors => array(
		'label' => $translate->posaddress_store_numfloors,
		'width' => 20,
		'number' => true
	),
	posaddress_store_floorsurface1 => array(
		'label' => $translate->posaddress_store_floorsurface1,
		'width' => 20,
		'number' => true
	),
	posaddress_store_floorsurface2 => array(
		'label' => $translate->posaddress_store_floorsurface2,
		'width' => 20,
		'number' => true
	),
	posaddress_store_floorsurface3 => array(
		'label' => $translate->posaddress_store_floorsurface3,
		'width' => 20,
		'number' => true
	),
	posaddress_store_headcounts => array(
		'label' => $translate->posaddress_store_headcounts,
		'width' => 20,
		'number' => true
	),
	posaddress_store_fulltimeeqs => array(
		'label' => $translate->posaddress_store_fulltimeeqs,
		'width' => 20
	),
	posaddress_store_openingdate => array(
		'label' => $translate->posaddress_store_openingdate,
		'width' => 20,
		'field' => "IF(posaddress_store_openingdate = '0000-00-00', '', DATE_FORMAT(posaddress_store_openingdate,'%d.%m.%Y')) AS posaddress_store_openingdate"
	),
	posaddress_store_closingdate => array(
		'label' => $translate->posaddress_store_closingdate,
		'width' => 20,
		'field' => "IF(posaddress_store_closingdate = '0000-00-00', '', DATE_FORMAT(posaddress_store_closingdate,'%d.%m.%Y')) AS posaddress_store_closingdate"
	),
	posaddress_neighbour_left => array(
		'label' => $translate->posaddress_neighbour_left,
		'width' => 20
	),
	posaddress_neighbour_right => array(
		'label' => $translate->posaddress_neighbour_right,
		'width' => 20
	),
	posaddress_neighbour_acrleft => array(
		'label' => $translate->posaddress_neighbour_acrleft,
		'width' => 20
	),
	posaddress_neighbour_acrright => array(
		'label' => $translate->posaddress_neighbour_acrright,
		'width' => 20
	),
	posaddress_neighbour_brands => array(
		'label' => $translate->posaddress_neighbour_brands,
		'width' => 20
	),
	posaddress_neighbour_comment => array(
		'label' => $translate->posaddress_neighbour_comment,
		'width' => 20
	),
	posaddress_perc_class => array(
		'label' => $translate->posaddress_perc_class,
		'width' => 20
	),
	posaddress_perc_tourist => array(
		'label' => $translate->posaddress_perc_tourist,
		'width' => 20
	),
	posaddress_perc_transport => array(
		'label' => $translate->posaddress_perc_transport,
		'width' => 20
	),
	posaddress_perc_people => array(
		'label' => $translate->posaddress_perc_people,
		'width' => 20
	),
	posaddress_perc_parking => array(
		'label' => $translate->posaddress_perc_parking,
		'width' => 20
	),
	posaddress_perc_visibility1 => array(
		'label' => $translate->posaddress_perc_visibility1,
		'width' => 20
	),
	posaddress_perc_visibility2 => array(
		'label' => $translate->posaddress_perc_visibility2,
		'width' => 20
	),
	posaddress_best_benchmark_pos => array(
		'label' => $translate->posaddress_best_benchmark_pos,
		'width' => 20
	),
	posaddress_old_project_nr => array(
		'label' => $translate->posaddress_old_project_nr,
		'width' => 20
	),
	posaddress_old_project_nr2 => array(
		'label' => $translate->posaddress_old_project_nr2,
		'width' => 20
	),
	posaddress_old_project_nr3 => array(
		'label' => $translate->posaddress_old_project_nr3,
		'width' => 20
	),
	posaddress_old_project_nr4 => array(
		'label' => $translate->posaddress_old_project_nr4,
		'width' => 20
	),
	posaddress_keymoney_recoverable => array(
		'label' => $translate->posaddress_keymoney_recoverable,
		'width' => 20
	),
	posaddress_google_lat => array(
		'label' => $translate->posaddress_google_lat,
		'width' => 20,
		'number' => true
	),
	posaddress_google_long => array(
		'label' => $translate->posaddress_google_long,
		'width' => 20,
		'number' => true
	),
	posaddress_google_precision => array(
		'label' => $translate->posaddress_google_precision,
		'width' => 20,
		'number' => true
	),
	posaddress_oldcurrency => array(
		'label' => $translate->posaddress_oldcurrency,
		'width' => 10
	),
	posaddress_oldexchangerate => array(
		'label' => $translate->posaddress_oldexchangerate,
		'width' => 10,
		'nuber' => true
	),
	posaddress_export_to_web => array(
		'label' => 'Show on Web',
		'width' => 10,
		'field' => "IF(posaddress_export_to_web=1, '$YES', '') AS posaddress_export_to_web",
		'description' => $translate->posaddress_export_to_web
	),
	posaddress_email_on_web => array(
		'label' => 'E-mail on web',
		'width' => 10,
		'field' => "IF(posaddress_email_on_web=1, '$YES', '') AS posaddress_email_on_web",
		'description' => $translate->posaddress_email_on_web
	),
	posaddress_offers_free_battery => array(
		'label' => 'Free Battery Change',
		'width' => 10,
		'field' => "IF(posaddress_offers_free_battery=1, '$YES', '') AS posaddress_offers_free_battery",
		'description' => $translate->posaddress_offers_free_battery
	),
	posaddress_show_in_club_jublee_stores => array(
		'label' => 'Show on Jubilee Store Locator',
		'width' => 10,
		'field' => "IF(posaddress_show_in_club_jublee_stores=1, '$YES', '') AS posaddress_show_in_club_jublee_stores",
		'description' => $translate->posaddress_show_in_club_jublee_stores
	),
	posaddress_local_production => array(
		'label' => $translate->posaddress_local_production,
		'width' => 20
	),
	posaddress_special_project => array(
		'label' => $translate->posaddress_special_project,
		'width' => 20
	),
	posaddress_furniture_type_store => array(
		'label' => $translate->posaddress_furniture_type_store,
		'width' => 20
	),
	posaddress_furniture_type_sis => array(
		'label' => $translate->posaddress_furniture_type_sis,
		'width' => 20
	),
	posaddress_uses_icedunes_visuals => array(
		'label' => $translate->posaddress_uses_icedunes_visuals,
		'width' => 20
	),
	posaddress_checked => array(
		'label' => $translate->posaddress_checked,
		'width' => 10,
		'field' => "IF(posaddress_checked=1, '$YES', '') AS posaddress_checked",
		'description' => $translate->posaddress_checked
	),
	posaddress_checkdate => array(
		'label' => $translate->posaddress_checkdate,
		'width' => 20,
		'field' => "IF(posaddress_checkdate = '0000-00-00', '', DATE_FORMAT(posaddress_checkdate,'%d.%m.%Y')) AS posaddress_checkdate"
	),
	checked2 => array(
		'label' => $translate->checked2,
		'width' => 20
	),
	posaddress_neighbour_checked => array(
		'label' => $translate->posaddress_neighbour_checked,
		'width' => 20
	),
	posaddress_investment_checked => array(
		'label' => $translate->posaddress_investment_checked,
		'width' => 20
	),
	posaddress_store_postype_tmp => array(
		'label' => $translate->posaddress_store_postype_tmp,
		'width' => 20
	),
	posaddress_data_check => array(
		'label' => $translate->posaddress_data_check,
		'width' => 20
	),
	imported_from_china => array(
		'label' => $translate->imported_from_china,
		'width' => 20
	),
	posaddress_turnoverclass_watches => array(
		'label' => 'TCW',
		'width' => 10,
		'field' => 'watches.mps_turnoverclass_code AS posaddress_turnoverclass_watches',
		'bind' => 'LEFT JOIN db_retailnet.mps_turnoverclasses watches ON watches.mps_turnoverclass_id = posaddress_turnoverclass_watches',
		'sort' => 'watches.mps_turnoverclass_code',
		'description' => $translate->posaddress_turnoverclass_watches
	),
	posaddress_turnoverclass_bijoux => array(
		'label' => 'TCB',
		'width' => 10,
		'field' => 'bijoux.mps_turnoverclass_code AS posaddress_turnoverclass_bijoux',
		'bind' => 'LEFT JOIN db_retailnet.mps_turnoverclasses bijoux ON bijoux.mps_turnoverclass_id = posaddress_turnoverclass_bijoux',
		'sort' => 'bijoux.mps_turnoverclass_code',
		'description' => $translate->posaddress_turnoverclass_bijoux
	),
	posaddress_selling_bijoux => array(
		'label' => $translate->posaddress_selling_bijoux,
		'width' => 20,
		'field' => "IF(posaddress_selling_bijoux=1, '$YES', '') AS posaddress_selling_bijoux"
	),
	posaddress_distribution_channel => array(
		'label' => $translate->posaddress_distribution_channel,
		'width' => 60,
		'field' => "CONCAT(mps_distchannel_group_name, ', ', mps_distchannel_name, ' ', mps_distchannel_code) AS posaddress_distribution_channel",
		'bind' => '
			LEFT JOIN db_retailnet.mps_distchannels ON mps_distchannel_id = posaddress_distribution_channel 
			LEFT JOIN db_retailnet.mps_distchannel_groups ON db_retailnet.mps_distchannels.mps_distchannel_group_id = db_retailnet.mps_distchannel_groups.mps_distchannel_group_id
		',
		'sort' => 'mps_distchannel_code'
	),
	posaddress_sales_representative => array(
		'label' => $translate->posaddress_sales_representative,
		'width' => 40,
		'field' => "CONCAT(representatives.mps_staff_name, ' ', representatives.mps_staff_firstname) AS posaddress_sales_representative",
		'bind' => 'LEFT JOIN mps_staffs representatives ON representatives.mps_staff_id = posaddress_sales_representative',
		'sort' => 'representatives.mps_staff_name'
	),
	posaddress_decoration_person => array(
		'label' => $translate->posaddress_decoration_person,
		'width' => 40,
		'field' => "CONCAT(decorators.mps_staff_name, ' ', decorators.mps_staff_firstname) AS posaddress_decoration_person",
		'bind' => 'LEFT JOIN mps_staffs decorators ON decorators.mps_staff_id = posaddress_decoration_person',
		'sort' => 'decorators.mps_staff_name'
	),
	posaddress_sales_classification01 => array(
		'label' => $translate->posaddress_sales_classification01,
		'width' => 40
	),
	posaddress_sales_classification02 => array(
		'label' => $translate->posaddress_sales_classification02,
		'width' => 40
	),
	posaddress_sales_classification03 => array(
		'label' => $translate->posaddress_sales_classification03,
		'width' => 40
	),
	posaddress_sales_classification04 => array(
		'label' => $translate->posaddress_sales_classification04,
		'width' => 40
	),
	posaddress_sales_classification05 => array(
		'label' => $translate->posaddress_sales_classification05,
		'width' => 40
	),
	posaddress_identical_to_address => array(
		'label' => 'IDENT',
		'width' => 10,
		'field' => "IF(posaddress_identical_to_address=1, '$YES', '') AS posaddress_identical_to_address",
		'description' => $translate->posaddress_identical_to_address
	),
	posaddress_overall_sqms => array(
		'label' => $translate->posaddress_overall_sqms,
		'width' => 20,
		'number' => true
	),
	posaddress_dedicated_sqms_wall => array(
		'label' => $translate->posaddress_dedicated_sqms_wall,
		'width' => 20,
		'number' => true
	),
	posaddress_dedicated_sqms_free => array(
		'label' => $translate->posaddress_dedicated_sqms_free,
		'width' => 20,
		'number' => true
	),
	posaddress_max_bijoux => array(
		'label' => $translate->posaddress_max_bijoux,
		'width' => 20,
		'number' => true
	),
	posaddress_max_watches => array(
		'label' => $translate->posaddress_max_watches,
		'width' => 20,
		'number' => true
	)
	user_created => array(
		'label' => $translate->user_created,
		'width' => 20,
	),
	user_modified => array(
		'label' => $translate->user_modified,
		'width' => 20
	),
	date_created => array(
		'label' => $translate->date_created,
		'width' => 20,
		'field' => "IF(posaddress.date_created = '0000-00-00', '', DATE_FORMAT(posaddress.date_created,'%d.%m.%Y')) AS date_created"
	),
	date_modified => array(
		'label' => $translate->date_modified,
		'width' => 20,
		'field' => "IF(posaddress.date_modified = '0000-00-00', '', DATE_FORMAT(posaddress.date_modified,'%d.%m.%Y')) AS date_modified"
	),
);
	
	


// filter: country
if($_REQUEST['countries']) {
	
	$filters['countries'] = "posaddress_country = ".$_REQUEST['countries'];
	$binds['posaddress_country'] = $properties['posaddress_country']['bind'];
	
	$country = new Country();
	$country->read($_REQUEST['countries']);
	$captions[] = $translate->country.": ".$country->name;
}
	
	

// filter: full text search
if ($fSearch) {

	$filters['search'] = "(
		posaddress_name LIKE '%$fSearch%'
		OR posaddress_street LIKE '%$fSearch%'
		OR country_name LIKE '%$fSearch%'
		OR province_canton LIKE '%$fSearch%'
		OR place_name LIKE '%$fSearch%'
		OR posaddress_zip LIKE '%$fSearch%'
		OR postype_name LIKE '%$fSearch%'
		OR posaddress_sapnumber LIKE \"%$fSearch%\"
		OR posaddress_sap_shipto_number LIKE \"%$fSearch%\"
	)";
	
	$binds['posaddress_country'] = $properties['posaddress_country']['bind'];
	$binds['posaddress_place_id'] = $properties['posaddress_place_id']['bind'];
	$binds['posaddress_province_id'] = $properties['posaddress_province_id']['bind'];
	$binds['posaddress_store_postype'] = $properties['posaddress_store_postype']['bind'];
	
	$captions[] = "$translate->search: $fSearch";
}

// filter: pos type
if ($fPosType) 	{
	
	$filters['pos_types'] = "posaddress_store_postype = $fPosType";
	$binds['posaddress_store_postype'] = $properties['posaddress_store_postype']['bind'];
	
	$pos_type = new Pos_Type();
	$pos_type->read($fPosType);
	$captions[] = "$translate->posaddress_store_postype: $pos_type->name";
}

// filter: client
if ($fClient) 	{

	$filters['client'] = "posaddress_client_id = $fClient";

	$res = $model->query("SELECT address_company AS caption FROM db_retailnet.addresses WHERE address_id = $fClient ")->fetch();
	$captions[] = "Client: {$res[caption]}";
}

// filter: legal types (pos owner types)
if ($fPosOwnerType) {
	
	$types = join(',', array_keys($fPosOwnerType));
	$filters['posowner_types'] = "posaddress_ownertype IN ($types)";
	$binds['posaddress_ownertype'] = $properties['posaddress_ownertype']['bind'];
	
	$res = $model->query("SELECT GROUP_CONCAT(posowner_type_name) AS caption FROM db_retailnet.posowner_types WHERE posowner_type_id IN ($types)")->fetch();
	$captions[] = "$translate->posaddress_ownertype: {$res[caption]}";
}

// filter: postype subclass
if ($fPosSubClass) {
	
	$subclasses = join(',', array_keys($fPosSubClass));
	$filters['possubclasses'] = "posaddress_store_subclass IN ($subclasses)";
	
	$res = $model->query("SELECT GROUP_CONCAT(possubclass_name) AS caption FROM db_retailnet.possubclasses WHERE possubclass_id IN ($subclasses)")->fetch();
	$captions[] = "POS Type Subclasses: {$res[caption]}";
}

// filter: channel
if ($fDistributionChannel) {

	if ($fDistributionChannel=='null') {
		$filters['distribution_channels'] = "(posaddress_distribution_channel IS NULL OR posaddress_distribution_channel='' OR posaddress_distribution_channel=0)";
		$captions[] = $translate->posaddress_distribution_channel.": No Distribution Channel Assigned";
	} else {
		$filters['distribution_channels'] = "posaddress_distribution_channel = $fDistributionChannel";

		$distributionChannel = new DistChannel($application);
		$distributionChannel->read($fDistributionChannel);
		$captions[] = "$translate->posaddress_distribution_channel: $distribution_channel->group, $distribution_channel->code";
	}
		
	$binds['posaddress_distribution_channel'] = $properties['posaddress_distribution_channel']['bind'];
}

// filter: sales representative
if ($fSalesRepresentative) {

	$filters['sales_representative'] = "posaddress_sales_representative = $fSalesRepresentative";
	$binds['posaddress_sales_representative'] = $properties['posaddress_sales_representative']['bind'];
	
	$staff = new Staff($application);
	$staff->read($fSalesRepresentative);
	$captions[] = "$translate->posaddress_sales_representative: $staff->firstname $staff->name";
}

// filter: decoration person
if ($fDecorationPerson) {
	
	$filters['decoration_person'] = "posaddress_decoration_person = $fDecorationPerson";
	$binds['posaddress_decoration_person'] = $properties['posaddress_decoration_person']['bind'];
	
	$staff = new Staff($application);
	$staff->read($fDecorationPerson);
	$captions[] = "$translate->posaddress_decoration_person: $staff->firstname $staff->name";
}

// filter: turnoverclass watches
if ($fTurnoverWatch) {
	
	$filters['turnoverclass_watches'] = "posaddress_turnoverclass_watches = $fTurnoverWatch";
	$binds['posaddress_turnoverclass_watches'] = $properties['posaddress_turnoverclass_watches']['bind'];
	
	$turnover_class = new TurnoverClass($application);
	$turnover_class->read($fTurnoverWatch);
	$captions[] = "$translate->posaddress_turnoverclass_watches: $turnover_class->name";
}

// filter: turnoverclass bijoux
if ($fTuronoverBijoux) {
	
	$filters['turnoverclass_bijoux'] = "posaddress_turnoverclass_bijoux = $fTuronoverBijoux";
	$binds['posaddress_turnoverclass_bijoux'] = $properties['posaddress_turnoverclass_bijoux']['bind'];
	
	$turnover_class = new TurnoverClass($application);
	$turnover_class->read($fTuronoverBijoux);
	$captions[] = "$translate->posaddress_turnoverclass_bijoux: $turnover_class->name";
}

// filter: product lines
if ($fProductLines) {
	
	$lines = join(',', array_keys($fProductLines));
	$filters['posowner_types'] = "posaddress_store_furniture IN ($lines)";
	$binds['posaddress_store_furniture'] = $properties['posaddress_store_furniture']['bind'];
	
	$res = $model->query("SELECT GROUP_CONCAT(product_line_name) AS caption FROM db_retailnet.product_lines WHERE product_line_id IN ($lines)")->fetch(); 
	$captions[] = "$translate->posaddress_store_furniture: {$res[caption]}";
}

// filter: actives
if ($archived) $filters['archived'] = "(posaddress_store_closingdate <> '0000-00-00' OR posaddress_store_closingdate <> NULL)";
else $filters['active'] = "(posaddress_store_closingdate = '0000-00-00' OR posaddress_store_closingdate IS NULL OR posaddress_store_closingdate = '')";


// for limited view
if ($hasLimitedPermission) {

	$countryAccess = User::getCountryAccess();

	// filter access countries
	$filterCountryAccess = $countryAccess ? " OR posaddress_country IN($countryAccess)" : null;

	// regional access companies
	$regionalAccessCompanies = User::getRegionalAccessPos();
	$filterRegionalAccess = $regionalAccessCompanies ? " OR $regionalAccessCompanies " : null;

	$filters['limited'] = "(
		posaddress_client_id = $user->address
		$filterCountryAccess 
		$filterRegionalAccess
	)";
}

	
if ($fields) {
	
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	if ($_REQUEST['sort'])  {
		
		$order = $_REQUEST['sort'];
		
		// check if sorted field containt bind table
		foreach ($properties as $field => $row) {
			if ($row['sort']==$order && $row['bind']) {
				$binds[$field] = $row['bind'];
			}
		}
		
		$captions[] = "Sort by: ".$translate->$order;
		$captions[] = "Direction: ".$translate->$direction;
	}
	else {
		$order = 'country_name, place_name, posaddress_name';
		$binds['posaddress_country'] = $properties['posaddress_country']['bind'];
		$binds['posaddress_place_id'] = $properties['posaddress_place_id']['bind']; 
	}
	
	foreach ($fields as $field) {
		$query[] = ($properties[$field]['field']) ? $properties[$field]['field'] : $field;
		if ($properties[$field]['bind']) $binds[$field] = $properties[$field]['bind'];
	}
	
	// append group to fields
	if ($group) {
		
		$query[] = "$group AS request_group";
		
		if (!in_array($group, $fields)) {
			$query[] = ($properties[$group]['field']) ? $properties[$group]['field'] : $group;
		}
		
		if ($properties[$group]['bind']) {
			$binds[$group] = $properties[$group]['bind'];
		}
	}
	
	// datagrid
	$result = $model->query("
		SELECT DISTINCT	posaddress_id, ".join(', ', $query)."
		FROM db_retailnet.posaddresses
	")
	->bind(array_values($binds))
	->filter($filters)
	->extend($extendedFilter)
	->order($order, $direction)
	->fetchAll();
	
	
	if ($result) {
		
		// set datagrid
		foreach ($result as $row) {
			if ($group) {
				if ($row['request_group']) {
					$datagrid[$row['request_group']][] = $row;
				}
			} else {
				$datagrid[] = $row;
			}
		}
		
		// set groups
		if ($group) {
			foreach ($datagrid as $id => $data) {
				$groups[$id] = array(
					'name' => $data[0][$group],
					'total' => count($data)
				);
			}
		}
	}
}


if ($datagrid) {
	

	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle($sheet);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	
	$export = true;
	$totalColumns = count($fields);
	$lastColumn = PHPExcel_Cell::stringFromColumnIndex($totalColumns-1);		

	// header
	$row = 1;
	$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
	$objPHPExcel->getActiveSheet()->setCellValue('A1', $header);
	$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A$row:$lastColumn$row")->getFont()->setSize(18);
	$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(40);

	// separator
	$row++;
	$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");

	// current date
	$row++;
	$range = "A$row:$lastColumn$row";
	$objPHPExcel->getActiveSheet()->mergeCells($range);
	$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: ".date('d.m.Y  H:i:s'));

	// filter captions
	if ($captions) {
		foreach ($captions as $value) {
			$row++;
			$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
			$objPHPExcel->getActiveSheet()->setCellValue("A$row", $value);
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
		}
	}

	// separator
	$row++;
	$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");

	// table caption
	if (!$group) {
		
		$row++;
		$col = 0;
		$first_datagrid_row = $row;
				
		// column captions
		foreach ($fields as $field) {
			
			$index = spreadsheet::index($col);
			$width = ($properties[$field]['width']) ? $properties[$field]['width'] : 20;

			if ($properties[$field]['number']) {
				$objPHPExcel->getActiveSheet()->getStyle($index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			}
			
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $properties[$field]['label']);
			$objPHPExcel->getActiveSheet()->getColumnDimension($index)->setWidth($width);
			
			if ($properties[$field]['description']) {
				$has_description = true;
			}
			
			$col++;
		}
		
		// header background
		$objPHPExcel->getActiveSheet()->getStyle("A$row:$lastColumn$row")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('efefef');
		$objPHPExcel->getActiveSheet()->getStyle("A$row:$lastColumn$row")->getFont()->setBold(true);
	}

	
	$row++;
	
	if ($group) {
		
		foreach ($groups as $id => $data) {
			
			// borders index
			$first_group_row = $row;
			
			// group caption
			$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow('A',$row, $translate->$group.': '.$data['name'].' ('.$data['total'].')');
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('cbe3f2');
			
			$row++;
			$col=0;
			
			// build table captions
			foreach ($fields as $field) {
			
				$index = spreadsheet::index($col);
				$width = ($properties[$field]['width']) ? $properties[$field]['width'] : 20;
	
				if ($properties[$field]['number']) {
					$objPHPExcel->getActiveSheet()->getStyle($index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				}
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $properties[$field]['label']);
				$objPHPExcel->getActiveSheet()->getColumnDimension($index)->setWidth($width);
				
				if ($properties[$field]['description']) {
					$has_description = true;
				}
				
				$col++;
			}
			
			// table header background
			$objPHPExcel->getActiveSheet()->getStyle("A$row:$lastColumn$row")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('efefef');
			$objPHPExcel->getActiveSheet()->getStyle("A$row:$lastColumn$row")->getFont()->setBold(true);
			
			$row++;
			
			foreach ($datagrid[$id] as $key => $value) {
				$col=0;
				foreach($fields as $field) {
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $value[$field]);
					$col++;
				}
				$row++;
			}
			
			$last_group_row = $row - 1;
			
			// borders
			$borders["A$first_group_row"] = $lastColumn.$last_group_row;
			
			// group separator
			$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
			$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
			
			$row++;
		}
		
	} 
	else {
	
		foreach ($datagrid as $i => $data) {
			$col=0;
			foreach($fields as $field) {
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $data[$field]);				
				$col++;
			}
			$row++;
		}
	}
	
	// set borders
	if (!$group) {
		$prev_row = $row-1;
		$borders["A$first_datagrid_row"] = $lastColumn.$prev_row;
	}

	// set borders
	if ($borders) {
		foreach ($borders as $start => $stop) {
			$objPHPExcel->getActiveSheet()->getStyle("$start:$stop")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); 
		}
	}
	
	if ($has_description) {
		
		$row++;
		$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Shortcut Descriptions');
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
		
		$row++;
		
		// descriptions
		foreach ($fields as $field) {
			if ($properties[$field]['description']) {
				$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
				$objPHPExcel->getActiveSheet()->setCellValue("A$row", $properties[$field]['label'].': '.$properties[$field]['description']);
				$row++;
			}
		}
	}
}

if ($export) {	
	
	$objPHPExcel->getActiveSheet()->setTitle($sheet);
	$objPHPExcel->setActiveSheetIndex(0);

	// redirect output to client browser
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	
	exit;
}
else {
	message::empty_result();
	$archived = ($archived) ? '/archived' : null;
	url::redirect("/$application/$controller$archived/$action");
}
