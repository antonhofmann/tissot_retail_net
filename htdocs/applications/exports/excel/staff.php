<?php
/*
    Add or Edit Companie Addresses (company addresses)

    Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2011-03-22
    Modified by:    Roger Bucher
    Date modified:  2012-10-22
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*/
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

define('SHEET_NAME', "Staff List");
define('SHEET_FILE_NAME', "mpsStaff_".date('Y-m-d'));
define('SHEET_VERSION', "Excel2007");

$settings = Settings::init();
$settings->load('data');

$user = User::instance();
$translate = Translate::instance();

// request execution time
ini_set('max_execution_time', 120);

// request vars
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

// request
$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
$fSearch = $_REQUEST['search'] && $_REQUEST['search']<>$translate->search ? $_REQUEST['search'] : null;
$fCountry = $_REQUEST['countries'];
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "country_name, address_company, mps_staff_name";
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// db model
$model = new Model($application);

// limited view
if( !$user->permission(Staff::PERMISSION_EDIT) && !$user->permission(Staff::PERMISSION_VIEW)) {

	// country access filter
	$countryAccess = User::getCountryAccess();
	$filterCountryAccess = $countryAccess ? " OR address_country IN ( $countryAccess )" : null;

	// regional access filter
	$regionalAccessCompanies = User::getRegionalAccessCompanies();
	$filterRegionalAccess = $regionalAccessCompanies ? " OR address_id IN (".join(',', $regionalAccessCompanies).") " : null;

	$filters['limited'] = "(
		mps_staff_address_id = $user->address
		$filterCountryAccess
		$filterRegionalAccess
	)";
}

// filter: full text search
if ($fSearch) {
	
	$filters['search'] = "(
		country_name LIKE \"%$fSearch%\"
		OR address_company LIKE \"%$fSearch%\"
		OR mps_staff_type_name LIKE \"%$fSearch%\"
		OR mps_staff_name LIKE \"%$fSearch%\"
		OR mps_staff_firstname LIKE \"%$fSearch%\"
		OR mps_staff_email LIKE \"%$fSearch%\"
	)";

	$captions[] = "Search: $fSearch";
}

// filter: country
if ($fCountry) {
	
	$filters['countries'] = "address_country = $fCountry";
	
	$country = new Country();
	$country->read($fCountry);
	$captions[] = "Country: $country->name";
}


$result = $model->query("
	SELECT
		mps_staff_id,
		country_name,
		address_company,
		mps_staff_type_name,
		mps_staff_name,
		mps_staff_firstname,
		mps_staff_email,
		mps_staff_phone
	FROM mps_staffs
")
->bind(Staff::DB_BIND_COMPANIES)
->bind(Staff::DB_BIND_STAFF_TYPES)
->bind(Company::DB_BIND_COUNTRIES)
->filter($filters)
->extend($extendedFilter)
->order($sort, $direction)
->fetchAll();


if ($result) {

	$datagrid = _array::datagrid($result);
	$columns = array_keys(end($datagrid));
	$totalColumns = count($columns);
	$lastColumn = PHPExcel_Cell::stringFromColumnIndex($totalColumns-1);

	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle(SHEET_NAME);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);


	// styling
	$styleBorder = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);

	$styleNumber = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
		)
	);


	$styleString = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
		)
	);


	$styleHeaderString = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 11,
			'bold'=>true
		),
		'alignment' => array(
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array(
				'rgb'=>'EEEEEE'
			)
		),
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);


	$styleHeaderNumber = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 11,
			'bold'=>true
		),
		'alignment' => array(
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true,
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array(
				'rgb'=>'EEEEEE'
			)
		),
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);


	$row=1;
	$range = "A$row:".$lastColumn.$row;
	$objPHPExcel->getActiveSheet()->mergeCells($range);
	$objPHPExcel->getActiveSheet()->setCellValue("A$row", SHEET_NAME);
	$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(18);

	$row++;
	$range = "A$row:".$lastColumn.$row;
	$objPHPExcel->getActiveSheet()->mergeCells($range);

	$row++;
	$range = "A$row:".$lastColumn.$row;
	$objPHPExcel->getActiveSheet()->mergeCells($range);
	$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: ".date('d.m.Y  H:i:s'));

	$row++;
	$range = "A$row:".$lastColumn.$row;
	$objPHPExcel->getActiveSheet()->mergeCells($range);

	// filter captions
	if (isset($captions)) {
		foreach ($captions as $caption) {
			$row++;
			$range = "A$row:".$lastColumn.$row;
			$objPHPExcel->getActiveSheet()->mergeCells($range);
			$objPHPExcel->getActiveSheet()->setCellValue("A$row", $caption);
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
		}
	}

	$row++;
	$range = "A$row:".$lastColumn.$row;
	$objPHPExcel->getActiveSheet()->mergeCells($range);

	$row++;
	$range = "A$row:".$lastColumn.$row;
	$objPHPExcel->getActiveSheet()->mergeCells($range);

	// columns
	$row++;
	$col = 0;
	$range = "A$row:".$lastColumn.$row;

	$translate->mps_staff_id = 'ID';

	foreach ($columns as $value) {
		$column = PhpExcel_Cell::stringFromColumnIndex($col);
		$objPHPExcel->getActiveSheet()->getStyle($column.$row)->applyFromArray($styleHeaderString);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $translate->$value);
		$col++;
	}

	$row++;

	foreach ($datagrid as $i => $array) {

		$col = 0;

		foreach($array as $key=>$value) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
	        $col++;
	    }

	    $range = "A$row:".$lastColumn.$row;
	    $objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleString);
	    $row++;
	}


	$row++;
	$range = "A$row:".$lastColumn.$row;
	$objPHPExcel->getActiveSheet()->mergeCells($range);

	// export
	$objPHPExcel->getActiveSheet()->setTitle(SHEET_NAME);
	$objPHPExcel->setActiveSheetIndex(0);


	// redirect output to client browser
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.SHEET_FILE_NAME.'.xlsx"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
}
else {
	message::empty_result();
	url::redirect("/$application/$controller/$action/$id");
}
