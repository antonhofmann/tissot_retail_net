<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	// execution time
	ini_set('max_execution_time', 600);
	ini_set('memory_limit', '102400M');
	
	$settings = Settings::init();
	$settings->load('data');
	$user = User::instance();
	$translate = Translate::instance();
	
	// request vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['ordersheet'];
	$version = $_REQUEST['version'];

	// include session filter
	$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

	$_DISTRIBUTION_CHANNEL = $_REQUEST['distribution_channels'];
	$_TURNOVERCLASS_WATCHES = $_REQUEST['turnoverclass_watches'];
	$_SALES_REPRESENTATIVE = $_REQUEST['sales_representative'];
	$_DECORATION_PERSON = $_REQUEST['decoration_person'];
	$_PROVINCE = $_REQUEST['provinces'];
	
	// order Sheet
	$ordersheet = new Ordersheet($application);
	$ordersheet->read($id);
	
	// page title
	$pagetitle = 'Order Sheet Planning';
	
	// is archived
	$isDistributed = ($ordersheet->state()->isDistributed() && $ordersheet->isDistributed()) ? true : false;
	
	// db model
	$model = new Model($application);
	

/*******************************************************************************************************************************************************************************************
	Ordersheet States
********************************************************************************************************************************************************************************************/
	
	// group statments
	$states_preparation = Ordersheet_State::loader('preparation');
	$states_complete = Ordersheet_State::loader('complete');
	$states_revision = Ordersheet_State::loader('revision');
	$states_confirmation = Ordersheet_State::loader('confirmation');
	$states_distribution = Ordersheet_State::loader('distribution');
	
	// is in group state
	$statment_on_preparation = $ordersheet->state()->onPreparation();
	$statment_on_completing = $ordersheet->state()->onCompleting();
	$statment_on_approving = $ordersheet->state()->onApproving();
	$statment_on_revision = $ordersheet->state()->onCompleting();
	$statment_on_confirmation = $ordersheet->state()->onConfirmation();
	$statment_on_distribution = $ordersheet->state()->onDistribution();
	
	// order sheet editabled states
	if ($ordersheet->state()->owner) {
		$editabled_states = array_merge($states_complete, $states_revision, $states_confirmation, $states_distribution);
	}
	elseif ($ordersheet->state()->canAdministrate()) {
		$editabled_states = array_merge($states_preparation, $states_confirmation, $states_distribution);
	}
	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Global Properties
	Set editabled and visibility vars
********************************************************************************************************************************************************************************************/
	
	// planning column keyword
	define(COLUMN_PLANNING, 'planned');
	
	// distribution column keyword
	define(COLUMN_DISTRIBUTION, 'delivered');
	
	// partially confirmed distribution keyword
	define(COLUMN_PARTIALY_CONFIRMED, 'partial');
	
	// spreadsheet readonly
	define('CELL_READONLY', 'true');
	
	// readonly mod
	if ($version) $mod_readonly = true;
	elseif($ordersheet->state()->isManuallyArchived()) $mod_readonly = false;
	else $mod_readonly =  ($ordersheet->state()->isDistributed() || !in_array($ordersheet->workflowstate_id, $editabled_states)) ? true: false;
	
	// planning mod
	$mod_planning = ($statment_on_preparation || $statment_on_confirmation) ? true : false;
	
	// distribution mod
	$mod_distribution = (in_array($ordersheet->workflowstate_id, $states_distribution) || $ordersheet->state()->isDistributed()) ? true : false;
	
	// planning columns
	$show_planning_columns = true;
	
	// distribution columns
	$show_distribution_columns = ($mod_distribution) ? true : false;
	
	// extended rows (warehouses)
	$show_extended_rows = (!$isDistributed || $version) ? true : false;
	
	// stock reserve edit
	if ($ordersheet->state()->canAdministrate()) {
		$stock_reserve_edit = ($ordersheet->state()->onApproving()) ? true : false;
	} else {
		$stock_reserve_edit = ($ordersheet->state()->onCompleting()) ? true : false;
	}
	
	// stock reserve id
	if ($stock_reserve_edit) {
		$result = $ordersheet->warehouse()->getStockReserve();
		$stock_rserve_id = $result['mps_ordersheet_warehouse_id'];
	}

	// calculation area (first in calculation set)
	$show_calculation_area = true;

	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Filters
********************************************************************************************************************************************************************************************/
	
	$filters = array();
	$filer_labels = array();
	
	// filter: pos
	$filters['default'] = "(
		posaddress_client_id = $ordersheet->address_id 
		OR (
			address_parent = $ordersheet->address_id 
			AND address_client_type = 3
		)  
	)";
	
	// filter: distribution channels
	if ($_DISTRIBUTION_CHANNEL) {
		
		$filters['distribution_channels'] = "posaddress_distribution_channel = $_DISTRIBUTION_CHANNEL";
		
		$result = $model->query("
			SELECT CONCAT(mps_distchannel_name, ' - ', mps_distchannel_code) AS caption
			FROM db_retailnet.mps_distchannels
			INNER JOIN db_retailnet.mps_distchannel_groups ON db_retailnet.mps_distchannel_groups.mps_distchannel_group_id = db_retailnet.mps_distchannels.mps_distchannel_group_id
			WHERE mps_distchannel_id = $_DISTRIBUTION_CHANNEL
		")->fetch();

		$_FILTER_CAPTIONS[] = $result['caption'];	
	}
	
	// filter: turnover classes
	if ($_TURNOVERCLASS_WATCHES) {
		
		$filters['turnoverclass_watches'] = 'posaddress_turnoverclass_watches = '.$_REQUEST['turnoverclass_watches'];
		
		$result = $model->query("
			SELECT mps_turnoverclass_name AS caption
			FROM mps_turnoverclasses
			WHERE mps_turnoverclass_id = $_TURNOVERCLASS_WATCHES
		")->fetch();

		$_FILTER_CAPTIONS[] = $result['caption'];	
	}
	
	// filter: sales represenatives
	if ($_SALES_REPRESENTATIVE) {
		
		$filters['sales_representative'] = "posaddress_sales_representative = $_SALES_REPRESENTATIVE";

		$result = $model->query("
			SELECT CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS caption
			FROM mps_staff
			WHERE mps_staff_id = $_SALES_REPRESENTATIVE
		")->fetch();

		$_FILTER_CAPTIONS[] = $result['caption'];
		
	}
	
	// filter: decorator
	if ($_DECORATION_PERSON) {
		
		$filters['decoration_person'] = "posaddress_decoration_person = $_DECORATION_PERSON";

		$result = $model->query("
			SELECT CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS caption
			FROM mps_staff
			WHERE mps_staff_id = $_DECORATION_PERSON
		")->fetch();

		$_FILTER_CAPTIONS[] = $result['caption'];
		
	}
	
	// provinces
	if ($_PROVINCE) {
		
		$filters['provinces'] = "province_id = $_PROVINCE";

		$result = $model->query("
			SELECT province_canton AS caption
			FROM db_retailnet.provinces
			WHERE province_id = $_PROVINCE
		")->fetch();

		$_FILTER_CAPTIONS[] = $result['caption'];
	}


	
	// for archived ordersheets show only POS locations which has distributed quantities 
	if ($isDistributed) {
		
		$result = $model->query("
			SELECT DISTINCT 
				mps_ordersheet_item_delivered_posaddress_id AS pos
			FROM mps_ordersheet_item_delivered		
		")
		->bind(Ordersheet_Item_Delivered::DB_BIND_ORDERSHEET_ITEMS)
		->filter('ordersheet', 'mps_ordersheet_item_ordersheet_id = '.$ordersheet->id)
		->filter('null', 'mps_ordersheet_item_delivered_posaddress_id > 0')
		->fetchAll();
		
		if ($result) {
			
			$array = array();
			
			foreach ($result as $row) {
				$array[] = $row['pos'];
			}

			$filters['active'] = 'posaddress_id IN ('.join(',', $array).')';
		}
	}
	// otherwise show all active pos locations
	else {
		$filters['active'] = "(
			posaddress_store_closingdate = '0000-00-00'
			OR posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = ''
		)";
	}
	
/*******************************************************************************************************************************************************************************************
	Dataloader
	POS Locations
********************************************************************************************************************************************************************************************/
	
	$pos_locations = array();
	
	$result = $model->query("
		SELECT DISTINCT
			posaddress_id,
			posaddress_name,
			posaddress_place,
			postype_id,
			postype_name
		FROM db_retailnet.posaddresses
		INNER JOIN db_retailnet.addresses ON address_id = posaddress_client_id
		INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
		INNER JOIN db_retailnet.provinces ON province_id = place_province
		INNER JOIN db_retailnet.postypes ON postype_id = posaddress_store_postype
	")
	->filter($filters)
	->order('posaddress_name, posaddress_place')
	->fetchAll();
	
	if ($result) {
		foreach ($result as $row) {
			$pos = $row['posaddress_id'];
			$type = $row['postype_id'];
			$pos_locations[$type]['caption'] = $row['postype_name'];
			$pos_locations[$type]['pos'][$pos]['name'] = $row['posaddress_name'];
			$pos_locations[$type]['pos'][$pos]['place'] = $row['posaddress_place'];
		}
	}
	
/*******************************************************************************************************************************************************************************************
	Dataloader
	Order Sheet Items
********************************************************************************************************************************************************************************************/
	
	$ordersheet_items = array();
	$item_partially_columns = array();
	$distrubuted_items = array();
	
	$result = $model->query("
		SELECT
			mps_ordersheet_item_id,
			mps_ordersheet_item_material_id,
			mps_ordersheet_item_quantity,
			mps_ordersheet_item_quantity_approved,
			mps_ordersheet_item_quantity_confirmed,
			mps_ordersheet_item_quantity_shipped,
			mps_ordersheet_item_quantity_distributed,
			mps_material_planning_type_id,
			mps_material_planning_type_name,
			mps_material_collection_category_id,
			mps_material_collection_category_code,
			mps_material_id,
			mps_material_code,
			mps_material_name
		FROM mps_ordersheet_items
	")
	->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
	->bind(Ordersheet_Item::DB_BIND_MATERIALS)
	->bind(Material::DB_BIND_PLANNING_TYPES)
	->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
	->filter('ordersheets', "mps_ordersheet_id = ".$ordersheet->id)
	->order('mps_material_planning_type_name')
	->order('mps_material_collection_category_code')
	->order('mps_material_code')
	->order('mps_material_name')
	->fetchAll();
	
	if ($result) {
		
		// check if delivered quantities are once confirmed
		$has_confirmation = $model->query("
			SELECT COUNT(mps_ordersheet_item_delivered_id) AS total
			FROM mps_ordersheet_item_delivered	
		")
		->bind(Ordersheet_Item_Delivered::DB_BIND_ORDERSHEET_ITEMS)
		->filter('ordersheet', 'mps_ordersheet_item_ordersheet_id = '.$ordersheet->id)
		->filter('confirmed', 'mps_ordersheet_item_delivered_confirmed IS NOT NULL')
		->fetch();
		
		foreach ($result as $row) {
			
			$approved_quantity = $row['mps_ordersheet_item_quantity_approved'];
			$confirmed_quantity = $row['mps_ordersheet_item_quantity_confirmed'];
			
			// show item in spreadsheet
			// for mod planning: quantity approved greater then zero or null
			// for mod distribution: quantity confirmed greater then zero
			if ($mod_distribution) $show_item = ($confirmed_quantity > 0) ? true : false;
			else $show_item = ($approved_quantity > 0 || is_null($approved_quantity)) ? true : false;
			
			if ($show_item) {
				
				$item = $row['mps_ordersheet_item_id'];
				$material = $row['mps_ordersheet_item_material_id'];
				$planning = $row['mps_material_planning_type_id'];
				$collection = $row['mps_material_collection_category_id'];
				$ordersheet_items[$planning]['caption'] = $row['mps_material_planning_type_name'];
				$ordersheet_items[$planning]['data'][$collection]['caption'] = $row['mps_material_collection_category_code'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['id'] = $row['mps_material_id'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['code'] = $row['mps_material_code'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['name'] = $row['mps_material_name'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity'] = $row['mps_ordersheet_item_quantity'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity_approved'] = $row['mps_ordersheet_item_quantity_approved'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity_confirmed'] = $row['mps_ordersheet_item_quantity_confirmed'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity_shipped'] = $row['mps_ordersheet_item_quantity_shipped'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity_distributed'] = $row['mps_ordersheet_item_quantity_distributed'];
				
				if ($mod_distribution && $has_confirmation['total'] && $row['mps_ordersheet_item_quantity_distributed']) {
					
					// items for partialy distribution
					if ($row['mps_ordersheet_item_quantity_shipped']-$row['mps_ordersheet_item_quantity_distributed'] > 0) {
						$item_partially_columns[$planning][$collection][$material] = true;
					} 
					// delivered items
					else {
						$distrubuted_items[$material] = true;
					}
				}
			}
		}

		// if all items are delivered hide unused row
		if (!$version && $distrubuted_items[$material] && !$item_partially_columns) {
			$spreadsheet_row_unused_quantity = false;
		}
	}

/*******************************************************************************************************************************************************************************************
	Dataloader
	Order Sheet Items Planned Quantities
********************************************************************************************************************************************************************************************/
	
	$planning_pos_quantities = array();
	$planning_warehouse_quantities = array();
	$planning_reserve_quantities = array();
	
	$sum_planning_quantities = array();
	
	if ($show_planning_columns) {
		
		$result = $model->query("
			SELECT DISTINCT
				mps_ordersheet_item_planned_id,
				mps_ordersheet_item_planned_posaddress_id,
				mps_ordersheet_item_planned_warehouse_id,
				mps_ordersheet_item_planned_quantity,
				mps_ordersheet_item_material_id,
				mps_ordersheet_warehouse_stock_reserve
			FROM mps_ordersheet_item_planned
		")
		->bind(Ordersheet_Item_Planned::DB_BIND_ORDERSHEET_ITEMS)
		->bind('LEFT JOIN mps_ordersheet_warehouses ON mps_ordersheet_warehouse_id = mps_ordersheet_item_planned_warehouse_id')
		->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = ".$ordersheet->id)
		->fetchAll();
		
		if ($result) {
			
			foreach ($result as $row) {
				
				$material = $row['mps_ordersheet_item_material_id'];
				
				// planned quantities
				if ($row['mps_ordersheet_item_planned_posaddress_id']) {
					$pos = $row['mps_ordersheet_item_planned_posaddress_id'];
					$planning_pos_quantities[$pos][$material]['id'] = $row['mps_ordersheet_item_planned_id'];
					$planning_pos_quantities[$pos][$material]['quantity'] = $row['mps_ordersheet_item_planned_quantity'];
					$sum_planning_quantities[$material] = $sum_planning_quantities[$material] + $row['mps_ordersheet_item_planned_quantity'];
				}
				
				// planned warehouse quantities
				if ($row['mps_ordersheet_item_planned_warehouse_id']) {
					
					$warehouse = $row['mps_ordersheet_item_planned_warehouse_id'];
					
					if ($row['mps_ordersheet_warehouse_stock_reserve']) {
						$planning_reserve_quantities[$material]['id'] = $row['mps_ordersheet_item_planned_id'];
						$planning_reserve_quantities[$material]['warehouse'] = $warehouse;
						$planning_reserve_quantities[$material]['quantity'] = $row['mps_ordersheet_item_planned_quantity'];
					} else {
						
						$planning_warehouse_quantities[$warehouse][$material]['id'] = $row['mps_ordersheet_item_planned_id'];
						$planning_warehouse_quantities[$warehouse][$material]['quantity'] = $row['mps_ordersheet_item_planned_quantity'];
						
						if ($show_extended_rows) {
							$sum_planning_quantities[$material] = $sum_planning_quantities[$material] + $row['mps_ordersheet_item_planned_quantity'];
						}
					}
				}
			}
		}
	}
	
	
/*******************************************************************************************************************************************************************************************
	Dataloader
	Order Sheet Items Delivered Quantities
********************************************************************************************************************************************************************************************/

	$distribution_pos_quantities = array();
	$distribution_pos_quantities_not_confirmed = array();
	$distribution_warehouse_quantities = array();
	$distribution_warehouse_quantities_not_confirmed = array();
	$distribution_reserve_quantities = array();
	$distribution_reserve_quantities_not_confirmed = array();
	$distribution_confirmation_dates = array();
	
	$sum_distribution_quantities = array();
	$sum_distribution_quantities_not_confirmed = array();
	
	if ($show_distribution_columns) { 
	
		$result = $model->query("
			SELECT DISTINCT
				mps_ordersheet_item_delivered_id,
				mps_ordersheet_item_delivered_posaddress_id,
				mps_ordersheet_item_delivered_warehouse_id,
				mps_ordersheet_item_delivered_quantity,
				mps_ordersheet_item_delivered_confirmed,
				DATE_FORMAT(mps_ordersheet_item_delivered_confirmed,'%d.%m.%Y') AS confirmed_date,
				mps_ordersheet_item_id,
				mps_ordersheet_item_material_id,
				mps_material_material_planning_type_id,
				mps_material_material_collection_category_id,
				mps_ordersheet_warehouse_stock_reserve
			FROM mps_ordersheet_item_delivered
		")
		->bind(Ordersheet_Item_Delivered::DB_BIND_ORDERSHEET_ITEMS)
		->bind(Ordersheet_Item::DB_BIND_MATERIALS)
		->bind('LEFT JOIN mps_ordersheet_warehouses ON mps_ordersheet_warehouse_id = mps_ordersheet_item_delivered_warehouse_id')
		->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = ".$ordersheet->id)
		->order('mps_ordersheet_item_delivered_confirmed', 'DESC')
		->order('mps_ordersheet_item_delivered.date_created', 'DESC')
		->fetchAll();
	
		if ($result) { 
			
			foreach ($result as $row) { 
	
				$id = $row['mps_ordersheet_item_delivered_id'];
				$planning = $row['mps_material_material_planning_type_id'];
				$collection = $row['mps_material_material_collection_category_id'];
				$material = $row['mps_ordersheet_item_material_id'];
				$item = $row['mps_ordersheet_item_id'];
				$pos = $row['mps_ordersheet_item_delivered_posaddress_id'];
				$warehouse = $row['mps_ordersheet_item_delivered_warehouse_id'];
				$quantity = $row['mps_ordersheet_item_delivered_quantity'];
				$timestamp = ($row['confirmed_date']) ? date::timestamp($row['confirmed_date']) : 0;
				
				// build versions data
				if ($timestamp) {
					$distribution_confirmation_dates[$timestamp] = $row['confirmed_date'];
				}
				
				// show quantities on required confirmed date
				if ($version) {
				
					if ($version==$timestamp) {
						
						// pos delivered quantities
						if ($pos) {
							$distribution_pos_quantities[$pos][$material]['id'] = $id;
							$distribution_pos_quantities[$pos][$material]['quantity'] = $quantity;
							$sum_distribution_quantities[$material] = $sum_distribution_quantities[$material] + $quantity;
						}
						
						// warehouse delivered quantities
						if ($warehouse) {
							
							if ($row['mps_ordersheet_warehouse_stock_reserve']) {
								$distribution_reserve_quantities[$material]['id'] = $id;
								$distribution_reserve_quantities[$material]['warehouse'] = $warehouse;
								$distribution_reserve_quantities[$material]['quantity'] = $quantity;
							} else {
								$distribution_warehouse_quantities[$warehouse][$material]['id'] = $id;
								$distribution_warehouse_quantities[$warehouse][$material]['quantity'] = $quantity;
								$sum_distribution_quantities[$material] = $sum_distribution_quantities[$material] + $quantity;
							}
						}
					}
				} 
				else {
					
					// delivered quantities are once confirmed
					// sum all confirmed quantities for and set not confiremd quantities as not confirmed
					if ($ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity_distributed']) {
							
						if ($row['mps_ordersheet_item_delivered_confirmed']) {
							
							// pos quantities
							if ($pos) {
								$distribution_pos_quantities[$pos][$material]['id'] = $id;
								$distribution_pos_quantities[$pos][$material]['quantity'] = $distribution_pos_quantities[$pos][$material]['quantity'] + $quantity;
								$sum_distribution_quantities[$material] = $sum_distribution_quantities[$material] + $quantity;
							}
							
							// warehouse quantities, only last insertetd
							if ($warehouse && $quantity) {
								if ($row['mps_ordersheet_warehouse_stock_reserve']) {
									$distribution_reserve_quantities[$material]['id'] = $id;
									$distribution_reserve_quantities[$material]['warehouse'] = $warehouse;
									$distribution_reserve_quantities[$material]['quantity'] = $quantity;
								} elseif (!$distribution_warehouse_quantities[$warehouse][$material]) {
									
									$distribution_warehouse_quantities[$warehouse][$material]['id'] = $id;
									$distribution_warehouse_quantities[$warehouse][$material]['quantity'] = $quantity;
									
									if ($show_extended_rows) {
										$sum_distribution_quantities[$material] = $sum_distribution_quantities[$material] + $quantity;
									}
								}
							}

						} else {
							
							// pos quantities
							if ($pos) {
								$distribution_pos_quantities_not_confirmed[$pos][$material]['id'] = $id;
								$distribution_pos_quantities_not_confirmed[$pos][$material]['quantity'] = $quantity;
								$sum_distribution_quantities_not_confirmed[$material] = $sum_distribution_quantities_not_confirmed[$material] + $quantity;
							}
							
							// warehouse quantities
							if ($warehouse) {
								if ($row['mps_ordersheet_warehouse_stock_reserve']) {
									$distribution_reserve_quantities_not_confirmed[$material]['id'] = $id;
									$distribution_reserve_quantities_not_confirmed[$material]['warehouse'] = $warehouse;
									$distribution_reserve_quantities_not_confirmed[$material]['quantity'] = $quantity;
								} elseif (!$distribution_warehouse_quantities[$warehouse][$material]) {
									$distribution_warehouse_quantities_not_confirmed[$warehouse][$material]['id'] = $id;
									$distribution_warehouse_quantities_not_confirmed[$warehouse][$material]['quantity'] = $quantity;
									$sum_distribution_quantities_not_confirmed[$material] = $sum_distribution_quantities_not_confirmed[$material] + $quantity;
								}
							}
						}
					}
					// not confirmed quantities
					else {
						
						// pos quantities
						if ($pos) {
							$distribution_pos_quantities[$pos][$material]['id'] = $id;
							$distribution_pos_quantities[$pos][$material]['quantity'] = $quantity;
							$sum_distribution_quantities[$material] = $sum_distribution_quantities[$material] + $quantity;
						}
						
						// warehouse quantities
						if ($warehouse) {
							if ($row['mps_ordersheet_warehouse_stock_reserve']) {
								$distribution_reserve_quantities[$material]['id'] = $id;
								$distribution_reserve_quantities[$material]['warehouse'] = $warehouse;
								$distribution_reserve_quantities[$material]['quantity'] = $quantity;
							} elseif (!$distribution_warehouse_quantities[$warehouse][$material]) {
								$distribution_warehouse_quantities[$warehouse][$material]['id'] = $id;
								$distribution_warehouse_quantities[$warehouse][$material]['quantity'] = $quantity;
								$sum_distribution_quantities[$material] = $sum_distribution_quantities[$material] + $quantity;
							}
						}
					}
				}
			}
		}
	}

	/*******************************************************************************************************************************************************************************************
	 SHEET FIXED AREA
	First blank row, planning type captions, material code
	********************************************************************************************************************************************************************************************/
	
	$fixed_rows = 5;
	$first_fixed_row = 1;
	$last_fixed_row = 5;
	$fixed_rows_separator = 0;
	$fixed_columns = 1;
	$first_fixed_column = 1;
	$last_fixed_column = 1;
	$fixed_column_separator = 0;
	
	$total_fixed_rows = $fixed_rows + $fixed_rows_separator;
	$total_fixed_columns = $fixed_columns + $fixed_column_separator;
	
	// step index
	$step_max = ($item_partially_columns) ? 3 : 2;
	
	/*******************************************************************************************************************************************************************************************
	 SHEET CALCULATION AREA
	Total column calculation, item quantities
	********************************************************************************************************************************************************************************************/
	
	$sheet_calculation_rows = array();
	$sheet_calculation_rows_separator = ($stock_reserve_edit) ? 1 : 0;
	
	if ($show_calculation_area) {
	
		$caption = ($ordersheet->hasDistributedQuantities()) ? 'Total Planned/Delivered' : 'Total Planned/Distributed';
		$caption = ($item_partially_columns) ? "Total Planned/Delivered/Distributed" : $caption;
	
		$sheet_calculation_rows[0] = ($mod_planning) ? "Total Planned" : $caption;
		$sheet_calculation_rows[1] = ($mod_planning) ? "Quantity" : "Total Confirmed/Shipped";
	}
	
	// total calculated rows
	$sheet_calculation_rows_total = ($sheet_calculation_rows) ? count($sheet_calculation_rows) + $sheet_calculation_rows_separator : 0;
	
	// calcullated rows
	if ($sheet_calculation_rows_total) {
		$sheet_calculation_rows_first = $total_fixed_rows + 1;
		$sheet_calculation_rows_last = $sheet_calculation_rows_first + $sheet_calculation_rows_total - 1;
	}
	
	// total planned calculated row
	$row_calculation = $sheet_calculation_rows_first;
	
	// item quantity row
	$row_quantity = $sheet_calculation_rows_first + 1;
	
	/*******************************************************************************************************************************************************************************************
	 SHEET STOCK RESERVE
	Total column calculation, item quantities
	********************************************************************************************************************************************************************************************/
	
	$sheet_stock_reserve = array();
	
	$sheet_stock_reserve[0] = 'Stock Reserve';
	
	$sheet_stock_reserve_separator = 0;
	$sheet_stock_reserve_total = 1 + $sheet_stock_reserve_separator;
	$sheet_stock_reserve_first = $total_fixed_rows + $sheet_calculation_rows_total + 1;
	$sheet_stock_reserve_last = $sheet_stock_reserve_first + $sheet_stock_reserve_total - 1;
	
	$row_reserve = $sheet_stock_reserve_first;
	
	/*******************************************************************************************************************************************************************************************
	 SPREADSHEED DATAGRID AREA
	POS locations, group by POS types
	Order sheet items, group by material planning types
	********************************************************************************************************************************************************************************************/
	
	$i=0;
	
	$sheet_datagrid_rows = array();
	$sheet_datagrid_rows_separator = 0;
	$group = array();
	
	// row group separators
	$rowgroup_separator = 0;
	$total_rowgroup_separators = 0;
	
	if ($pos_locations) {
	
		foreach ($pos_locations as $type => $data) {
	
			$sheet_datagrid_rows[$i]['type'] = $type;
			$sheet_datagrid_rows[$i]['caption'] = htmlentities($data['caption'], ENT_QUOTES);
			$sheet_datagrid_rows[$i]['group'] = true;
	
			$j = $i+1;
	
			foreach ($data['pos'] as $pos => $value) {
	
				$sheet_datagrid_rows[$j]['type'] = $type;
				$sheet_datagrid_rows[$j]['pos'] = $pos;
	
				$caption = $value['name'].' ('.$value['place'].')';
				$caption = str_replace("'", "", $caption);
				$sheet_datagrid_rows[$j]['caption'] = $caption;
				$j++;
			}
	
			$i = $j + $rowgroup_separator;
	
			$total_rowgroup_separators = $total_rowgroup_separators + $rowgroup_separator;
		}
	
		// remove last seperator
		if ($total_rowgroup_separators) $total_rowgroup_separators = $total_rowgroup_separators - 1;
	}
	
	$i=0;
	
	$sheet_datagrid_columns = array();
	$planning_type_captions = array();
	$planning_column_index = 0;
	$item_colum_index = 0;
	
	// row references separator
	$referenced_column_separator = 0;
	
	// column group separators
	$colgroup_separator = 1;
	$total_colgroup_separators = 0;
	
	// column multiplicator
	$multiple = ($mod_distribution) ? 2 : 1;
	
	if ($ordersheet_items && ($show_planning_columns || $mod_distribution)) {
	
		foreach ($ordersheet_items as $planning => $data) {
	
			foreach ($data['data'] as $collection => $value) {
	
				// merge length for planning caption
				$total_item_columns = count($value['data']) * $multiple;
				$total_partial_columns = (!$version && $item_partially_columns[$planning][$collection]) ? count($item_partially_columns[$planning][$collection]) : 0;
				$planning_type_captions[$i]['merge'] = $total_item_columns + $total_partial_columns;
					
				// planning type caption
				$planning_type_captions[$i]['planning'] = $data['caption'];
				$planning_type_captions[$i]['collection'] = $value['caption'];
	
				foreach ($value['data'] as $item => $row) {
	
					// skip separator columns
					if ($i==$item_colum_index) {
							
						// merge length for item caption
						$merge_length = 1;
						$merge_length = ($mod_distribution) ? 2 : $merge_length;
	
						if (!$version) {
							$merge_length = ($item_partially_columns[$planning][$collection][$row['id']] && $row['quantity_distributed']) ? 3 : $merge_length;
						}
							
						// item caption
						$sheet_datagrid_columns[$i]['merge'] = $merge_length;
						$sheet_datagrid_columns[$i]['code'] = $row['code'];
							
						// next item index
						$item_colum_index = $i+$merge_length;
	
						// planned column
						if ($show_planning_columns) {
							$sheet_datagrid_columns[$i]['key'] = COLUMN_PLANNING;
							$sheet_datagrid_columns[$i]['id'] = $row['id'];
							$sheet_datagrid_columns[$i]['quantity'] = $row['quantity'];
							$sheet_datagrid_columns[$i]['quantity_approved'] = $row['quantity_approved'];
							$sheet_datagrid_columns[$i]['quantity_confirmed'] = $row['quantity_confirmed'];
							$sheet_datagrid_columns[$i]['quantity_shipped'] = $row['quantity_shipped'];
							$sheet_datagrid_columns[$i]['data'] = $planning_pos_quantities;
							$sheet_datagrid_columns[$i]['warehouse'] = $planning_warehouse_quantities;
							$i++;
						}
	
						// delivered column
						if ($mod_distribution) {
							$sheet_datagrid_columns[$i]['key'] = COLUMN_DISTRIBUTION;
							$sheet_datagrid_columns[$i]['id'] = $row['id'];
							$sheet_datagrid_columns[$i]['quantity_approved'] = $row['quantity_approved'];
							$sheet_datagrid_columns[$i]['quantity_confirmed'] = $row['quantity_confirmed'];
							$sheet_datagrid_columns[$i]['quantity_shipped'] = $row['quantity_shipped'];
							$sheet_datagrid_columns[$i]['quantity_distributed'] = $row['quantity_distributed'];
							$sheet_datagrid_columns[$i]['data'] = $distribution_pos_quantities;
							$sheet_datagrid_columns[$i]['warehouse'] = $distribution_warehouse_quantities;
							$i++;
						}
	
						// partiale delivered column
						if (!$version && $item_partially_columns[$planning][$collection][$row['id']]) {
							$sheet_datagrid_columns[$i]['key'] = COLUMN_PARTIALY_CONFIRMED;
							$sheet_datagrid_columns[$i]['id'] = $row['id'];
							$sheet_datagrid_columns[$i]['data'] = $distribution_pos_quantities_not_confirmed;
							$sheet_datagrid_columns[$i]['quantity_confirmed'] = $row['quantity_confirmed'];
							$sheet_datagrid_columns[$i]['quantity_shipped'] = $row['quantity_shipped'];
							$sheet_datagrid_columns[$i]['quantity_distributed'] = $row['quantity_distributed'];
							$sheet_datagrid_columns[$i]['quantity_notused'] = $row['quantity_shipped'] - $row['quantity_distributed'];
							$sheet_datagrid_columns[$i]['warehouse'] = $distribution_warehouse_quantities_not_confirmed;
							$i++;
						}
					}
	
					// next first item index
					$item_colum_index = $i;
				}
	
				// increment
				$i = $i+$colgroup_separator;
	
				// next first item index
				$item_colum_index = $i;
	
				// colgroup separator
				$total_colgroup_separators = $total_colgroup_separators + $colgroup_separator;
			}
		}
	
		// remove last seperator
		if ($total_colgroup_separators) $total_colgroup_separators = $total_colgroup_separators - 1;
	}
	
	// total row references
	$sheet_datagrid_rows_total = ($sheet_datagrid_rows) ? count($sheet_datagrid_rows) + $total_rowgroup_separators + $sheet_datagrid_rows_separator : 0;
	
	// totall column references
	$sheet_datagrid_columns_total = ($sheet_datagrid_columns) ? count($sheet_datagrid_columns) + $total_colgroup_separators + $referenced_column_separator : 0;
	
	// referenced rows
	if ($sheet_datagrid_rows_total) {
		$sheet_datagrid_rows_first = $total_fixed_rows + $sheet_calculation_rows_total + $sheet_stock_reserve_total + 1;
		$sheet_datagrid_rows_last = $sheet_datagrid_rows_first + $sheet_datagrid_rows_total - 1;
	}
	
	// referenced columns
	if ($sheet_datagrid_columns_total) {
		$sheet_datagrid_columns_first = $total_fixed_columns + $total_calcullated_columns + 1;
		$sheet_datagrid_columns_last = $sheet_datagrid_columns_first + $sheet_datagrid_columns_total - 1;
	}
	
	/*******************************************************************************************************************************************************************************************
	 SHEET POST DATAGRID AREA
	Order sheet warehouses
	********************************************************************************************************************************************************************************************/
	
	$sheet_post_datagrid_rows = array();
	$sheet_post_datagrid_rows_separator = 0;
	
	if ($show_extended_rows) {
	
		$sheet_post_datagrid_rows[0] = "Warehouses";
	
		// load all active company warehouses
		$company_warehouses = $ordersheet->company()->warehouse()->load();
		$company_warehouses = _array::datagrid($company_warehouses);
	
		// load all ordersheet registred warehouses
		$ordersheet_warehouses = $ordersheet->warehouse()->load();
	
		if ($ordersheet_warehouses) {
	
			$i = 1;
	
			foreach ($ordersheet_warehouses as $row) {
	
				$warehouse = $row['mps_ordersheet_warehouse_address_warehouse_id'];
	
				if (!$row['mps_ordersheet_warehouse_stock_reserve']) {
					$sheet_post_datagrid_rows[$i]['id'] = $row['mps_ordersheet_warehouse_id'];
					$sheet_post_datagrid_rows[$i]['name'] = $company_warehouses[$warehouse]['address_warehouse_name'];
					$i++;
				}
			}
		}
	}
	
	$sheet_post_datagrid_rows_total = ($sheet_post_datagrid_rows) ? count($sheet_post_datagrid_rows) + $sheet_post_datagrid_rows_separator : 0;
	
	// row limits
	if ($sheet_post_datagrid_rows_total) {
		$sheet_post_datagrid_rows_first = $total_fixed_rows + $sheet_calculation_rows_total + $sheet_stock_reserve_total + $sheet_datagrid_rows_total + 1;
		$sheet_post_datagrid_rows_last = $sheet_post_datagrid_rows_first + $sheet_post_datagrid_rows_total - 1;
	}
	
/*******************************************************************************************************************************************************************************************
	PHPExcel BUILDER
********************************************************************************************************************************************************************************************/
	
	
	require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
	require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle($pagetitle);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	
	$styleBorder = array(
		'borders' => array( 
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);

	
	$stylePlanningType = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 14, 
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'EFEFEF')
		)
	);
	
	$styleCollection = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12, 
			'bold'=>true,
			'color' => array(
				'rgb' => '666666'
			)
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'EFEFEF')
		)
	);
	
	$styleVerticalText = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_BOTTOM,
			'rotation' => 90,
			'wrap'=> true
		)
	);
	
	$styleCalculationBanner = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12,
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		)
	);
	
	$stylePosType = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12,
			'bold'=>true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'EFEFEF')
		)
	);
	
	$stylePlanned = array(
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'DBEFFF')
		)
	);
	
	$styleDelivered = array(
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'FFFAD6')
		)
	);
	

/*******************************************************************************************************************************************************************************************
	SPREADSHEED BUILDER
********************************************************************************************************************************************************************************************/
	
	
	$sheet_total_rows = $total_fixed_rows + $sheet_calculation_rows_total + $sheet_stock_reserve_total +$sheet_datagrid_rows_total + $sheet_post_datagrid_rows_total;
	$sheet_total_columns = $total_fixed_columns + $sheet_datagrid_columns_total + $total_extended_columns + $total_calcullated_columns;
	
	
	// datagrid
	if ($sheet_datagrid_rows_total && $sheet_datagrid_columns_total) {
		
		$export = true;
		
		$lastColumn = spreadsheet::key($total_spreadsheet_columns);
	
		for ($row=1; $row <= $sheet_total_rows; $row++) {
				
			for ($col=1; $col <= $sheet_total_columns; $col++) {
	
				$column = spreadsheet::key($col);
				$index = $column.$row;
				$step = ($step==$step_max) ? 1 : $step;

				switch ($row) {
					
					// TITLE
					case 1:
						if ($col==1) {
							
							// column dimension
							$objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth(50);
								
							// merge cells
							$objPHPExcel->getActiveSheet()->mergeCells("$index:$lastColumn$row");
								
							// content & styling
							$objPHPExcel->getActiveSheet()->setCellValue($index, $ordersheet->header());
							$objPHPExcel->getActiveSheet()->getStyle($index)->getFont()->setBold(true);
							$objPHPExcel->getActiveSheet()->getStyle($index)->getFont()->setSize(20);
						}
					
					break;
					
					// SEPARATOR
					case 2:
					
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->mergeCells("$index:$lastColumn$row");
							$value = $_FILTER_CAPTIONS ? 'Filter(s): '.join(', ', $_FILTER_CAPTIONS) : null;
							$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
						}
					
					break;
					
					// PLANNING TYPES
					case 3;
					
						if ($col==1) {
							
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(40);
							
							// filter labels
						}
						elseif ($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last) {
								
							// column dimension
							$column_width = ($col==$separator_key) ? 5 : 10;
							$objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth($column_width);
								
							$i = $col-$sheet_datagrid_columns_first;

							if ($planning_type_captions[$i]) {
								
								$caption = $planning_type_captions[$i]['planning'];
								$merge_length = $planning_type_captions[$i]['merge'];
						
								// merge planning type cell
								if ($merge_length > 1) {
									$merge_cell = spreadsheet::index($i+$merge_length);
									$objPHPExcel->getActiveSheet()->mergeCells($index.':'.$merge_cell.$row);
									$objPHPExcel->getActiveSheet()->getStyle($index.':'.$merge_cell.$row)->applyFromArray($styleBorder);
								}
									
								// content
								$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($stylePlanningType+$styleBorder, false);
						
								// next colgroup seperator
								$separator_key = $col + $merge_length;
							}
								
							// merge colgroup columns
							if ($col==$separator_key) {
								$objPHPExcel->getActiveSheet()->mergeCells("$index:$column$sheet_total_rows");
							}
						}
					
					break;
				
					// COLLECTION CATEGORIES
					case 4;
					
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
						}
						elseif ($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last) {
								
							$i = $col-$sheet_datagrid_columns_first;
							
							if ($planning_type_captions[$i]) {
						
								$caption = $planning_type_captions[$i]['collection'];
								$merge_length = $planning_type_captions[$i]['merge'];
									
								// merge planning type cell
								if ($merge_length > 1) {
									$merge_cell = spreadsheet::index($i+$merge_length);
									$objPHPExcel->getActiveSheet()->mergeCells($index.':'.$merge_cell.$row);
									$objPHPExcel->getActiveSheet()->getStyle($index.':'.$merge_cell.$row)->applyFromArray($styleBorder);
								}
									
								// content
								$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleCollection+$styleBorder, false);
							}
						}
					
					break;
					
					// ITEM CODE
					case 5:
					
						// row diemension
						if ($col==1) {
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(160);
						}
						// references
						elseif ($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last) {
					
							$i = $col - $sheet_datagrid_columns_first;

							if ($sheet_datagrid_columns[$i]) {
					
								$code = $sheet_datagrid_columns[$i]['code'];
								$item_id = $sheet_datagrid_columns[$i]['id'];
								$merge_length = $sheet_datagrid_columns[$i]['merge'];
					
								// merge item label
								if ($merge_length > 1) {
									$merge_cell = spreadsheet::index($col-$sheet_datagrid_columns_first+$merge_length);
									$objPHPExcel->getActiveSheet()->mergeCells($index.':'.$merge_cell.$row);
									$objPHPExcel->getActiveSheet()->getStyle($index.':'.$merge_cell.$row)->applyFromArray($styleBorder);
								}
					
								// content && styling
								$objPHPExcel->getActiveSheet()->setCellValue($index, '     '.$code);
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleVerticalText+$styleBorder, false);
							}
						}
					
					break;
										
					// CALCULLATION
					case $row_calculation:
					
						$i = $row-$sheet_calculation_rows_first;
						$j = $col-$sheet_datagrid_columns_first;
					
						if ($col==1) {
							$caption = $sheet_calculation_rows[$i];
							$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleCalculationBanner, false);
						}
						elseif ($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last) {
							
							if ($sheet_datagrid_columns[$j]) {
							
								$key = $sheet_datagrid_columns[$j]['key'];
								$next_key = $sheet_datagrid_columns[$j+1]['key'];
								$material = $sheet_datagrid_columns[$j]['id'];
							
								// column planning
								if ($key==COLUMN_PLANNING) {
									
									$quantity = $sum_planning_quantities[$material];
									$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
									
									$style = ($mod_distribution) ? $stylePlanned+$styleBorder : $styleBorder;
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($style, false);
								}
								// colum distribution
								elseif ($key==COLUMN_DISTRIBUTION) {
									
									$quantity = ($next_key==COLUMN_PARTIALY_CONFIRMED) ? $sheet_datagrid_columns[$j]['quantity_distributed'] : $sum_distribution_quantities[$material];
									$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
									
									$style = ($distrubuted_items[$material] || $version || $next_key==COLUMN_PARTIALY_CONFIRMED) ? $styleDelivered+$styleBorder : $styleBorder;
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($style, false);
								}
								// column partially distribution
								elseif ($key==COLUMN_PARTIALY_CONFIRMED) {
									$value = $sum_distribution_quantities_not_confirmed[$material];
									$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleBorder, false);
								}
							}
						}
					
							
					break;
		
					// QUANTITIES
					case $row_quantity:
						
						$i = $row-$sheet_calculation_rows_first;
						$j = $col-$sheet_datagrid_columns_first;
						
						if ($col==1) {
							$caption = $sheet_calculation_rows[$i];
							$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleCalculationBanner, false);
						}
						elseif ($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last) {
							
							if ($sheet_datagrid_columns[$j]) {
								
								$key = $sheet_datagrid_columns[$j]['key'];
								$next_key = $sheet_datagrid_columns[$j+1]['key'];
								$material = $sheet_datagrid_columns[$j]['id'];

								// column planning
								if ($key==COLUMN_PLANNING) {
									
									$quantity =  ($mod_distribution) ? $sheet_datagrid_columns[$j]['quantity_confirmed'] : $sheet_datagrid_columns[$j]['quantity_approved'];
									$quantity =  ($statment_on_approving) ? $sheet_datagrid_columns[$j]['quantity'] : $quantity;
									$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
									
									$style = ($mod_distribution) ? $stylePlanned+$styleBorder : $styleBorder;
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($style, false);
								} 
								// colum distribution
								elseif ($key==COLUMN_DISTRIBUTION && $next_key<>COLUMN_PARTIALY_CONFIRMED) {
									
									$quantity = $sheet_datagrid_columns[$j]['quantity_shipped'];
									$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
									
									$style = ($distrubuted_items[$material] || $version) ? $styleDelivered+$styleBorder : $styleBorder;
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($style, false);
								}
								// column partially distribution
								elseif ($key==COLUMN_PARTIALY_CONFIRMED) {
									$quantity = $sheet_datagrid_columns[$j]['quantity_shipped'];
									$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleBorder, false);
								}
																				
							}
						}
						
					break;
					
					// STOCK RESERVE
					case $row_reserve;
					
						$i = $row-$sheet_stock_reserve_first;
						$j = $col-$sheet_datagrid_columns_first;
					
						if ($col==1) {
							$caption = $sheet_stock_reserve[$i];
							$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleCalculationBanner, false);
						}
						elseif($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last) {
							
							if ($sheet_datagrid_columns[$j]) {
								
								$key = $sheet_datagrid_columns[$j]['key'];
								$next_key = $sheet_datagrid_columns[$j+1]['key'];
								$material = $sheet_datagrid_columns[$j]['id'];
								
								// column planning
								if ($key==COLUMN_PLANNING) {
									
									$quantity =  ($mod_distribution) ? $sheet_datagrid_columns[$j]['quantity_confirmed'] : $sheet_datagrid_columns[$j]['quantity_approved'];
									$quantity =  ($stock_reserve_edit) ? $planning_reserve_quantities[$material]['quantity'] : $quantity - $sum_planning_quantities[$material];
									
									if ($quantity) {
										$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
									}
				
									$style = ($mod_distribution) ? $stylePlanned+$styleBorder : $styleBorder;
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($style, false);
								}
								// colum distribution
								elseif ($key==COLUMN_DISTRIBUTION && $next_key<>COLUMN_PARTIALY_CONFIRMED) {
									
									$quantity_shipped = $sheet_datagrid_columns[$j]['quantity_shipped'];
									$quantity_calculated = $sum_distribution_quantities_not_confirmed[$material];
									$quantity = $quantity_shipped - $quantity_calculated;

									if ($quantity) {
										$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
									}
									
									$style = ($distrubuted_items[$material]) ? $styleDelivered + $styleBorder : $styleBorder;
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($style, false);
								}
								// column partially distribution
								elseif ($key==COLUMN_PARTIALY_CONFIRMED) {

									$quantity_shipped = $sheet_datagrid_columns[$j]['quantity_shipped'];
									$quantity_calculated = $sum_distribution_quantities_not_confirmed[$material];
									$quantity_distributed = $sum_distribution_quantities[$material];
									$quantity = $quantity_shipped - $quantity_calculated-$quantity_distributed;
									
									if ($quantity) {
										$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
									}
									
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleBorder, false);
								}
							} 
						}
						 
					break;
					
					// REFERENCES
					case $row >= $sheet_datagrid_rows_first && $row <= $sheet_datagrid_rows_last :
						
						if ($col==1) {
							
							$i = $row-$sheet_datagrid_rows_first;
							$group = $sheet_datagrid_rows[$i]['group'];
							
							$caption = $sheet_datagrid_rows[$i]['caption'];
							$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
							
							$style = ($group) ? $stylePosType+$styleBorder : $styleBorder;
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($style, false);
						}
						elseif ($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last) {
							
							$i = $row - $sheet_datagrid_rows_first;
							$j = $col - $sheet_datagrid_columns_first;
							$group = $sheet_datagrid_rows[$i]['group'];
							$material = $sheet_datagrid_columns[$j]['id'];
							
							if (!$group && $material) {
									
								$key = $sheet_datagrid_columns[$j]['key'];
								$next_key = $sheet_datagrid_columns[$j+1]['key'];
								$pos = $sheet_datagrid_rows[$i]['pos'];
								
								$quantity = $sheet_datagrid_columns[$j]['data'][$pos][$material]['quantity'];
								$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
								
								$style = $styleBorder;
								
								if ($mod_distribution) {
									if ($key==COLUMN_PLANNING) $style = $stylePlanned+$style;
									elseif ($version && $key==COLUMN_DISTRIBUTION) $style = $styleDelivered+$style;
									elseif ($key==COLUMN_DISTRIBUTION && ($next_key==COLUMN_PARTIALY_CONFIRMED || $distrubuted_items[$material]) ) $style = $styleDelivered+$style;
								}
								
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($style, false);
							}
							else {
								/*	
								if ($planning_type_captions[$j]) {
									$merge_cell = $j+$planning_type_captions[$j]['merge'];
									$range = $index.':'.spreadsheet::index($merge_cell, $row);
									$objPHPExcel->getActiveSheet()->mergeCells($range);
									$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($stylePosType+$styleBorder, false);
								}
								*/
							}
						}
	
					break;
							
					// WAREHOUSE GROUP CAPTION
					case $sheet_post_datagrid_rows_first:
						
						if ($col==1) {
							
							$caption = $sheet_post_datagrid_rows[0];
							$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
							
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($stylePosType+$styleBorder, false);
						}
						
					break;
				
					// WAREHOUSES
					case $row > $sheet_post_datagrid_rows_first && $row <= $sheet_post_datagrid_rows_last :
						
						$i = $row-$sheet_post_datagrid_rows_first;
						$j = $col-$sheet_datagrid_columns_first;
						
						if ($col==1) {
							$caption = $sheet_post_datagrid_rows[$i]['name'];
							$objPHPExcel->getActiveSheet()->setCellValue($index, $caption);
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleBorder, false);
						}
						elseif ($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last && $sheet_post_datagrid_rows_total > 1) {
							
							if ($sheet_datagrid_columns[$j]) {
								
								$key = $sheet_datagrid_columns[$j]['key'];
								$next_key = $sheet_datagrid_columns[$j+1]['key'];
								$warehouse_id = $sheet_post_datagrid_rows[$i]['id'];
								$material = $sheet_datagrid_columns[$j]['id'];
								
								
								$quantity = ($key==COLUMN_DISTRIBUTION && $distrubuted_items[$material] && !$version) ? null : $sheet_datagrid_columns[$j]['warehouse'][$warehouse_id][$material]['quantity'];
								$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);

								$style = $styleBorder;
								
								if ($mod_distribution) {
									if ($key==COLUMN_PLANNING) $style = $stylePlanned+$style;
									elseif ($version && $key==COLUMN_DISTRIBUTION) $style = $styleDelivered+$style;
									elseif ($key==COLUMN_DISTRIBUTION && ($next_key==COLUMN_PARTIALY_CONFIRMED || $distrubuted_items[$material]) ) $style = $styleDelivered+$style;
								}
								
								$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($style, false);
							}
						}
						
					break;			
				}
			}
		}
	
		
		// merge cells
		$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
		
		if ($spreadsheet_column_distributed) {
				
			$row++;
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->applyFromArray($stylePlanned);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$row,"Planned Quantities");
				
			$row++;
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->applyFromArray($styleDelivered);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$row,"Delivered Quantities");
		}
	} 
	
		
/*******************************************************************************************************************************************************************************************
	EXPORT
********************************************************************************************************************************************************************************************/
	
	if ($export) {
		
		// file name
		$filename = 'ordersheet_planning_'.date('Ymd');
		
		// set active sheet
		$objPHPExcel->getActiveSheet()->setTitle($pagetitle);
		$objPHPExcel->setActiveSheetIndex(0);

		// phpexcel headers
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}
	else {
		message::empty_result();
		url::redirect("/$application/$controller$archived/$action/$id");
	}
	