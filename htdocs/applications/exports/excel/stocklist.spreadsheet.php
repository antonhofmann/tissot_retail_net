<?php 
/*
    Stock List Spreadsheet Export

    Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2011-10-11
    Modified by:    
    Date modified:  
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*/

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	ini_set('max_execution_time', 240);
	ini_set('memory_limit', '1024M');
	
	// request vars
	$id = $_REQUEST['id'];
	$current_year = $_REQUEST['year'];
	$first_week_in_week_range = $_REQUEST['start'];
	$last_week_in_week_range = $_REQUEST['stop'];
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$model = new Model(Connector::DB_CORE);
	
/************************************************************************************************************************************************
	Dataloaders
*************************************************************************************************************************************************/
	
	// stock list
	$stocklist = new Stocklist();
	$stocklist->read($id);

	// company
	$company = new Company();
	$company->read($stocklist->supplier_address);
	
	// currency
	$currency = new Currency();
	$currency->read($company->currency);
	$currency_symbol = $currency->symbol;
	$currency_exchange_rate = $currency->exchange_rate;
	$currency_factor = $currency->factor;
	

/************************************************************************************************************************************************
	Spreadsheet Utilities
*************************************************************************************************************************************************/
	
	$background_color_1 = 'f3f3f3';
	$background_color_2 = 'ffffff';
	
	// export file name
	$filename = strtolower(str_replace(' ','_', $company->company.'_'.$stocklist->title).'_'.date('Ymd'));
	
	// sheet name
	$sheet_name = 'Stock & Planning';
	
	
/************************************************************************************************************************************************
	Time Factory
*************************************************************************************************************************************************/
	
	// actual year
	$actuale_year = date('Y');
	
	// futured years
	$futured_years = 2;
	
	// current year
	$current_year = ($current_year) ? $current_year : $actuale_year;
	
	// current week
	$current_week = date('W');
	
	// onload week numbers
	$show_number_of_weeks = 8;

	
/************************************************************************************************************************************************
	Fixed Rows
*************************************************************************************************************************************************/
	
	$fixed_rows = array();
	
	$fixed_rows[] = array(
		'name' => 'separator',
		'caption' => 'Separator'
	);
	
	$fixed_rows[] = array(
		'name' => 'months',
		'caption' => 'Months'
	);
	
	$fixed_rows[] = array(
		'name' => 'weeks',
		'caption' => 'Weeks'
	);
	
	$fixed_rows[] = array(
		'name' => 'reference',
		'caption' => 'Reference'
	);
	
	
/************************************************************************************************************************************************
	Fixed Columns
*************************************************************************************************************************************************/

	$fixed_columns = array();
	
	$fixed_columns[] = array(
		'name' =>'counter',
		'caption' => ''		
	);
	
	$fixed_columns[] = array(
		'name' =>'reference',
		'caption' => 'Reference',
		'width' => 30,
		'height' => 220
	);
	
	$fixed_columns[] = array(
		'name' =>'actual_price',
		'caption' => "Actual Purchase Price in $currency_symbol",
		'width' => 15,
		'rotate' => 90
	);
	

/************************************************************************************************************************************************
	Extended Spreadsheet Columns
*************************************************************************************************************************************************/

	
	// additionals column week data
	$stocklist_additional_columns = unserialize($stocklist->columns);
	
	// caption for warehaus
	$caption = ($company->type == 8) ? 'In Transit' : 'Production order - not confirmed yet';
	
	$extended_columns = array();
	
	// extended column: Production Order - Not confirmett yet
	$extended_columns[] = array(
		'name' => 'scpps_week_not_confirmed_yet',
		'caption' => $caption,
		'width' => 10,
		'rotate' => 90,
		'bg' => 'fff000'
	);
	
	// extended column: Available after deduction of projects in pipeline
	$extended_columns[] = array(
		'name' => 'scpps_week_available_after_deduction',
		'caption' => "Available after deduction\nof projects in pipeline",
		'width' => 10,
		'rotate' => 90,
		'color' => 'ff0000'
	);
	
	// extended columns: push additional columns in extenden columns
	if ($stocklist_additional_columns) {
		foreach ($stocklist_additional_columns as $name => $caption) {
			$extended_columns[] = array(
				'name' => $name,
				'caption' => $caption,
				'width' => 10,
				'rotate' => 90
			);
		}
	}
	
/************************************************************************************************************************************************
	 Calculated Spreadsheet Rows
*************************************************************************************************************************************************/	
	
	
	$calculated_rows = array();
	
	// goods - propterty of swatch
	$calculated_rows[] = array(
		'name' => 'goods',
		'caption' => $translate->goods_propterty_of_swatch,
		'bg' => 'ff0000',
		'color' => 'ffffff'
	);
	
	// production ordernot confirmed yet
	$calculated_rows[] = array(
		'name' => 'production_order_not_confirmed',
		'caption' => 'Production order not confirmed yet',
		'bg' => 'fff000',
		'color' => '000000'
	);
	
	// new production order
	$calculated_rows[] = array(
		'name' => 'new_production_order',
		'caption' => 'New Production Order',
		'bg' => '00FFFF',
		'color' => '000000'
	);
	
	// no movement of stock!
	$calculated_rows[] = array(
		'name' => 'no_movement_of_stock!',
		'caption' => 'No movement of stock!',
		'bg' => 'FFC0CB',
		'color' => '000000'
	);
	
	// out of stock
	$calculated_rows[] = array(
		'name' => 'out_of_stock',
		'caption' => 'Out of Stock',
		'bg' => '00FF00',
		'color' => '000000'
	);

	
/************************************************************************************************************************************************
	Calculated Spreadsheet Columns
*************************************************************************************************************************************************/
	
	
	$calculated_columns = array();
	
	// TOTAL CONSUMPTION UP to DATE (units)
	$calculated_columns[] = array(
		'name' => 'consumption_units',
		'caption' => "TOTAL CONSUMPTION\nUP to DATE (units)",
		'width' => 25,
		'rotate' => 90,
		'amount' => true 
	);
	
	// TOTAL CONSUMPTION UP to DATE (€ / CHF)
	$calculated_columns[] = array(
		'name' => 'consumption_price',
		'caption' => "TOTAL CONSUMPTION\nUP to DATE (€ / CHF)",
		'width' => 25,
		'rotate' => 90,
		'amount' => true
	);
	
	// Average per Week
	$calculated_columns[] = array(
		'name' => 'average_week',
		'caption' => "AVERAGE PER WEEK $current_year",
		'width' => 10,
		'rotate' => 90
	);
	
	// Week Left
	$calculated_columns[] = array(
		'name' => 'week_left',
		'caption' => "WEEK LEFT\n(Stock divided by average per week)",
		'width' => 25,
		'rotate' => 90,
		'amount' => true
	);
	
	// Reproduction time
	$calculated_columns[] = array(
		'name' => 'reproduction',
		'caption' => "REPRODUCTION TIME",
		'width' => 10,
		'rotate' => 90
	);
	
	// Total Value of Stock
	$calculated_columns[] = array(
		'name' => 'value_stock',
		'caption' => "TOTAL VALUE OF STOCK",
		'width' => 25,
		'rotate' => 90,
		'amount' => true
	);
	
	// % total total value of Stock 
	$calculated_columns[] = array(
		'name' => 'percent_value_stock',
		'caption' => "%",
		'width' => 8,
		'rotate' => 90
	);
	
	// total poduction order
	$calculated_columns[] = array(
		'name' => 'total_production_order',
		'caption' => "TOTAL INCL. PRODUCTION ORDER",
		'width' => 25,
		'rotate' => 90,
		'bg' => '00bfff',
		'color' => 'ffffff',
		'amount' => true
	);
	
	// % total poduction order
	$calculated_columns[] = array(
		'name' => 'percent_production_order',
		'caption' => "%",
		'width' => 8,
		'rotate' => 90
	);	
	
	
	
/************************************************************************************************************************************************
	Movement of Stock
*************************************************************************************************************************************************/

	
	// Stock 
	$movement_of_stock[] = array(
		'name' => 'movement_stock',
		'caption' => 'Stock',
		'width' => 10,
		'rotate' => 90
	);
	
	// Stock Production
	$movement_of_stock[] = array(
		'name' => 'movement_production',
		'caption' => 'Production Order',
		'width' => 10,
		'rotate' => 90
	);
	
	// caption for addresstype warehaus
	$movement_caption = ($company->type == 8) ? "Transfer to Warehouse" : "Movement of stock";
	
	// Movement of stock
	$movement_of_stock[] = array(
		'name' => 'movement_movement',
		'caption' => $movement_caption,
		'width' => 10,
		'rotate' => 90
	);
	
	
/************************************************************************************************************************************************
	Stock List Items & Items Details
*************************************************************************************************************************************************/
	
	
	$supplier_items = $model->query("
		SELECT DISTINCT
			item_id,
			item_code,
			item_category_id,
			item_category_name
		FROM scpps_stocklist_items
	")
	->bind(Stocklist_Item::DB_BIND_ITEMS)
	->bind(Item::DB_BIND_ITEM_CATEGORIES)
	->filter('stocklist', "scpps_stocklist_item_stocklist_id = $id")
	->order('item_category_name')
	->order('item_code')
	->fetchAll();
	
	$items_details = $model->query("
		SELECT DISTINCT
			supplier_item,
			supplier_item_price,
			store_reproduction_time_in_weeks
		FROM db_retailnet.suppliers
		LEFT JOIN db_retailnet.stores ON store_item = supplier_item
		WHERE supplier_address = ".$stocklist->supplier_address."
	")->fetchAll();
	
	
	$items_details = _array::datagrid($items_details);
	
	
/************************************************************************************************************************************************
	Stock List Items Weeks Data
*************************************************************************************************************************************************/
	
	
	// stocklist weeks
	$stocklist_weeks = unserialize($stocklist->weeks);
	
	// stocklist weeks items
	$stocklist_weeks_items = $model->query("
		SELECT *
		FROM scpps_weeks
	")
	->filter('stocklist', "scpps_week_stocklist_id = $id")
	->filter('year', "scpps_week_year = $current_year")
	->fetchAll();
	
	$stocklist_weeks_items = _array::datagrid($stocklist_weeks_items, 'scpps_week_id');
	
	if ($stocklist_weeks_items) {
		
		foreach ($stocklist_weeks_items as $key => $row) { 
			
			$item = $row['scpps_week_item'];
			$week = $row['scpps_week_week'];
			
			// is week visible for stock list
			if (!in_array($row['scpps_week_week'], $stocklist_weeks)) {
				array_push($stocklist_weeks, $row['scpps_week_week']);
			}
			
			// item week data
			$item_week_data[$item][$week] = $row;
		}
		
		if (is_array($stocklist_weeks)) {
			
			// unique array values
			$stocklist_weeks = array_unique($stocklist_weeks);
			
			// sort weeks
			sort($stocklist_weeks);
		}
	}
	
	
	
/************************************************************************************************************************************************
	Stock List Items Weeks Data
*************************************************************************************************************************************************/
	
	
	// stocklist weeks
	$stocklist_weeks = unserialize($stocklist->weeks);
	
	// stocklist weeks items
	$stocklist_weeks_items = $model
	->query("SELECT * FROM scpps_weeks")
	->filter('stocklist', "scpps_week_stocklist_id = $id")
	->filter('year', "scpps_week_year = $current_year")
	->fetchAll();
	
	$stocklist_weeks_items = _array::datagrid($stocklist_weeks_items, 'scpps_week_id');
	
	if ($stocklist_weeks_items) {
	
		foreach ($stocklist_weeks_items as $key => $row) {
				
			$item = $row['scpps_week_item'];
			$week = $row['scpps_week_week'];
				
			// is week visible for stock list
			if (!in_array($row['scpps_week_week'], $stocklist_weeks)) {
				array_push($stocklist_weeks, $row['scpps_week_week']);
			}
				
			// item week data
			$item_week_data[$item][$week] = $row;
		}
	
		if (is_array($stocklist_weeks)) {
				
			// unique array values
			$stocklist_weeks = array_unique($stocklist_weeks);
				
			// sort weeks
			sort($stocklist_weeks);
		}
	}
	

/***********************************************************************************************************************************************
	Define Weeks Range
		Set First and Last week in weeks range according with current week value
		Possible Cases:
		- Current week is greater then last week in weeks range
		- Current week is lesser then fisrt week in weeks range
		- Current week is greater or equal then first week AND lesser or equal then last week in weeks range
*************************************************************************************************************************************************/
	
	
	$total_stocklist_weeks = count($stocklist_weeks);
	
	
	if ($_REQUEST['start'] && $_REQUEST['stop']) {
		$first_week_in_week_range = $_REQUEST['start'];
		$last_week_in_week_range = $_REQUEST['stop'];
	}
	
	
	if ($stocklist_weeks && $first_week_in_week_range && $last_week_in_week_range ) {
		
		foreach ($stocklist_weeks as $week) {
			
			if (($week >= $first_week_in_week_range) && ($week <= $last_week_in_week_range)) {
				
				// week range
				$weeks_range[] = $week;
				
				// month range
				$month = date::month_number_from_week($week, $current_year);
				$months_range[$month][] = $week;
			}
		}
	}
	
	
/************************************************************************************************************************************************
	Spreadsheet Items Grid
*************************************************************************************************************************************************/
	

	if ($supplier_items && $weeks_range) {
	
		$i = 1;
	
		foreach ($supplier_items as $row) {
				
			$category = $row['item_category_id'];
			$item = $row['item_id'];

			// include item weeks data
			if ($item_week_data[$item]) {
				
				foreach ($item_week_data[$item] as $week => $itemdata) {
					
					if ($itemdata['scpps_week_additional_columns']) {
						$additional_data[$item][$week] = unserialize($itemdata['scpps_week_additional_columns']);
					}
					
					// is week in week range
					if (in_array($week, $weeks_range)) {
						
						// weeks data
						$row['weeks'][$week] = $itemdata;
						
						// define prices range
						if ($itemdata['scpps_week_item_supplier_price']) {
							$item_prices[$item] = $itemdata['scpps_week_item_supplier_price'];
							$calculatedWeek[$item] = $week;
						}
						
						// Additional columns
						if ($additional_data[$week]) {
							foreach ($additional_data[$week] as $name => $value) {
								$row['weeks'][$week][$name] = $value;
							}
						}
					}
				}
				
				// sort data per weeks 
				if ($row['weeks']) asort($row['weeks']);
			}
						
			$row['supplier_item_price'] = $items_details[$item]['supplier_item_price'];
			$row['store_reproduction_time_in_weeks'] = $items_details[$item]['store_reproduction_time_in_weeks'];
			
			// categories
			$categories[$category] = $row['item_category_name'];
			
			// datagrid
			$datagrid[$i] = $row;
			$i++;
		}
	}
	
	
	if ($additional_data) {
	
		$last_additional_data = array();
	
		foreach ($additional_data as $item => $add_weeks) {
				
			rsort($add_weeks);
				
			foreach ($add_weeks as $add_week => $add_columns) {
				foreach ($add_columns as $add_column => $add_value) {
					if ($add_value && !$last_additional_data[$add_column]) {
						$last_additional_data[$item][$add_column] = $add_value;
					}
				}
			}
		}
	}

	
/************************************************************************************************************************************************
	Datagrid
*************************************************************************************************************************************************/
	
	
	if ($datagrid) { 
		
		// build current year calendar
		$month_weeks_ranges = date::month_weeks($current_year);
		
		// calendar limits
		$first_week_in_week_range = $first_week_in_week_range;
		$last_week_in_week_range = $last_week_in_week_range;
		$total_weeks_range = count($weeks_range);
		
		// data limits
		$total_items = count($datagrid);
		$total_categories = count($categories);
		$columns_per_week = count($movement_of_stock);
		
		// separators
		$row_separators = 1;
		$column_separators = 0;
		
		// spreadsheet row limits
		$total_fixed_rows = count($fixed_rows);
		$total_items_rows = $total_items + $total_categories;
		$total_extended_rows = count($extended_rows);
		$total_calculated_rows = count($calculated_rows);
		$total_spreadsheet_rows = $total_fixed_rows + $total_items_rows + $total_extended_rows + $total_calculated_rows + $row_separators;
		
		// spreadsheet column limits
		$total_fixed_columns = count($fixed_columns);
		$total_weeks_columns = $total_weeks_range * $columns_per_week;
		$total_extended_columns = count($extended_columns);
		$total_calculated_columns = count($calculated_columns);
		$total_spreadsheet_columns = $total_fixed_columns + $total_weeks_columns + $total_extended_columns + $total_calculated_columns + $column_separators;
		
		// item rows limits
		if ($total_items_rows) {
			$first_items_row = $total_fixed_rows + 1;
			$last_items_row = $first_items_row + $total_items_rows - 1; 
		}
		
		// extended row limits
		if ($total_extended_rows) {
			$first_extended_row = $total_fixed_rows + $total_items_rows  + 1;
			$last_extended_row = $first_extended_row + $total_extended_rows - 1;
		}
		
		// row separators
		if ($row_separators) {
			$first_row_separator = 	$total_fixed_rows + $total_items_rows + $total_extended_rows + 1;
			$last_row_separator = $first_row_separator + $row_separators - 1;
		}
		
		// calculet row limits
		if ($total_calculated_rows) {
			$first_calculated_row = $total_fixed_rows + $total_items_rows + $total_extended_rows + $row_separators + 1;
			$last_calculated_row = $first_calculated_row + $total_calculated_rows - 1;
		}
		
		// weeks column limits
		if ($total_weeks_columns) {
			$first_weeks_column = $total_fixed_columns + 1;
			$last_weeks_column = $first_weeks_column + $total_weeks_columns - 1;
		}
		
		// first extended column index
		if ($total_extended_columns) {
			$first_extended_column = $total_fixed_columns + $total_weeks_columns + 1;
			$last_extended_column = $first_extended_column + $total_extended_columns - 1;
		}
		
		// row separators
		if ($column_separators) {
			$first_col_separator = 	$total_fixed_columns + $total_weeks_columns + $total_extended_columns + 1;
			$last_col_separator = $first_col_separator + $column_separators - 1;
		}
		
		// first calculated column
		if ($total_calculated_columns) {
			$first_calculated_column =  $total_fixed_columns + $total_weeks_columns + $total_extended_columns + $column_separators + 1;
			$last_calculated_column = $first_calculated_column + $total_calculated_columns - 1; 
		}
		
	
		// PHP Excel library
		require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";

	
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Retailnet")->setLastModifiedBy("Retailnet") ->setTitle($sheet_name);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		
		
		$borders = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		
		$styleSpreadsheetTitle = array(
			'font' => array(
				'name'=>'Arial',
				'size'=> 18, 
				'bold'=>true
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			)
		);
		
		$styleMonthName = array(
			'font' => array(
				'name'=>'Arial',
				'size'=> 12,
				'bold'=>true
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$styleWeekName = array(
			'font' => array(
				'name'=>'Arial',
				'size'=> 11
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			)
		);
		
		$styles['reference'] = array(
			'font' => array(
				'name'=>'Arial',
				'size'=> 14, 
				'bold'=>true
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			)
		);
		
		$styleVerticalText = array(
			'font' => array(
				'name'=>'Arial',
				'size'=> 12
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_BOTTOM,
				'rotation' => 90,
				'wrap'=> true
			)
		);
		
		
		// inserted categories
		$inserted_categories = array();
		
		$increment_category = 0;
		
		// conditional formating for production order not confirmed
		$conditional_production_order_notconfirmed = new PHPExcel_Style_Conditional();
		$conditional_production_order_notconfirmed->setConditionType(PHPExcel_Style_Conditional::CONDITION_CELLIS);
		$conditional_production_order_notconfirmed->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_GREATERTHAN);
		$conditional_production_order_notconfirmed->addCondition('0');
		$conditional_production_order_notconfirmed->getStyle()->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getEndColor()->setRGB('fff000');
		
		// conditional formating for new production order
		$conditional_new_production_order = new PHPExcel_Style_Conditional();
		$conditional_new_production_order->setConditionType(PHPExcel_Style_Conditional::CONDITION_CELLIS);
		$conditional_new_production_order->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_GREATERTHAN);
		$conditional_new_production_order->addCondition('0');
		$conditional_new_production_order->getStyle()->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getEndColor()->setRGB('00FFFF');
			
		// grid builder
		for ($row = 1; $row <= $total_spreadsheet_rows; $row++) {
				
			// datagrid key
			$key = $row-$total_fixed_rows-$increment_category;
			
			// item id
			$item_id = $datagrid[$key]['item_id'];
			
			// category id
			$category_id = $datagrid[$key]['item_category_id'];
			
			// item code
			$code = $datagrid[$key]['item_code'];
			
			// reproduction time
			$reproduction_time = $datagrid[$key]['store_reproduction_time_in_weeks'];
			
			// item week values
			$weeks = $datagrid[$key]['weeks'];
			
			// group items in categories
			// fore eache first item in group, set this row as category row
			$category_row = false;
			
			// actual item price 
			if ($last_week_in_week_range >= $current_week) $price = $datagrid[$key]['supplier_item_price'];
			else $price = ($item_prices[$item_id]) ? $item_prices[$item_id] : $datagrid[$key]['supplier_item_price'];
			
			// calculated week
			if ($last_week_in_week_range == $current_week) $calculated_week = $current_week;
			else $calculated_week = ($calculatedWeek[$item_id]) ?  $calculatedWeek[$item_id] : $current_week;
				
			// loop controll values
			$cls = $background_color_1;
			$trigger = $columns_per_week;
			$sum_movement_stock = array();
			$value_of_stock = array();
			
			// shift first array in weeks range fore
			$colWeeks = $weeks_range;
			$week = array_shift($colWeeks); 
			
			// set month number for week
			$month = date::month_number_from_week($week, $current_year);

			// column looping
			for ($col = 1; $col <= $total_spreadsheet_columns; $col++) { 
	
				// cel index
				$index = spreadsheet::key($col, $row);
				
				// stepfor movement of stock
				// reset after column per weeks
				$step = $col-$trigger;
	
				switch ($row) {
	
					// header
					case 1:
						
						if ($col == 1) { 
							
							$mergecel = spreadsheet::key($total_spreadsheet_columns,$row);

							$objPHPExcel->getActiveSheet()->getColumnDimension('a')->setWidth(5);
							$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(40);
							$objPHPExcel->getActiveSheet()->mergeCells("$index:$mergecel");
							$objPHPExcel->getActiveSheet()->setCellValue($index, $company->company.", ".$stocklist->title." ($current_year)");
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleSpreadsheetTitle);
						}
					break;
	
					// row 2: months columns
					case 2:
							
						switch ($col) {
							
							// merge columns to fixed column
							case 1:
								$mergecel = spreadsheet::key($total_fixed_columns, $row);
								$objPHPExcel->getActiveSheet()->mergeCells("$index:$mergecel"); 
								$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(25); 
								
							break;
								
							// month captions
							case $col > $total_fixed_columns && $col <= $last_weeks_column : 
								
								if (!$monthMerge[$month]) {
									
									$monthMerge[$month] = true;
									$cel = $col-1 + (count($months_range[$month])*$columns_per_week);
									$mergecel = spreadsheet::key($cel,$row);
									
									$objPHPExcel->getActiveSheet()->mergeCells("$index:$mergecel");
									$objPHPExcel->getActiveSheet()->setCellValue($index, date::month_name($month));
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleMonthName);
								}

							break;
							
							case $last_weeks_column+1:
								$mergecel = spreadsheet::key($total_spreadsheet_columns,$row);
								$objPHPExcel->getActiveSheet()->mergeCells("$index:$mergecel");
							break;	
						}
							
					break;
		
					// row 3: weeks columns
					case 3:
							
						switch ($col) {
							
							// merge columns to fixed column
							case 1:
								$cel = spreadsheet::key($total_fixed_columns, $row);
								$objPHPExcel->getActiveSheet()->mergeCells("$index:$cel");
								$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(40);
							break;
								
							// week captions
							case $col >= $first_weeks_column && $col <= $last_weeks_column : 
								
								if (in_array($week, $weeks_range) && $step==1) {
									$cel = $col-1 + $columns_per_week;
									$cel = spreadsheet::key($cel,$row);
									$objPHPExcel->getActiveSheet()->mergeCells("$index:$cel");
									$objPHPExcel->getActiveSheet()->setCellValue($index, "Week $week \n".$month_weeks_ranges[$month][$week]);
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleWeekName);
									$objPHPExcel->getActiveSheet()->getStyle($index)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($zebra_bg);
								}
								
							break;
							
							case $last_weeks_column+1:
								$mergecel = spreadsheet::key($total_spreadsheet_columns,$row);
								$objPHPExcel->getActiveSheet()->mergeCells("$index:$mergecel");
							break;
						}
							
					break;
					
					// row 4: referencen
					case 4:
	
						switch ($col) {
																	
							// references
							case $col <= $total_fixed_columns :
								
								$cel = spreadsheet::key($col);
								$data = $fixed_columns[$col-1];
								
								if ($data['caption']) {
									$objPHPExcel->getActiveSheet()->setCellValue($index, $data['caption']);
								}
								
								if ($data['height']) {
									$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight($data['height']);
								}
								
								if ($data['width']) {
									$objPHPExcel->getActiveSheet()->getColumnDimension($cel)->setWidth($data['width']);
								}
								
								if ($data['rotate']) {
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleVerticalText);
								}
								
								if ($styles[$data['name']]) {
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles[$data['name']]);
								}
								
							break;

							// movement of stock
							case $col >= $first_weeks_column && $col <= $last_weeks_column :
								
								if (in_array($week, $stocklist_weeks)) {
									
									$data = $movement_of_stock[$step-1];
									$objPHPExcel->getActiveSheet()->setCellValue($index, $data['caption']);
									
									if ($data['rotate']) {
										$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleVerticalText);
									}
									
									if ($data['width']) {
										$objPHPExcel->getActiveSheet()->getColumnDimension(spreadsheet::key($col))->setWidth($data['width']);
									}
									
									if ($zebra_bg) {
										$objPHPExcel->getActiveSheet()->getStyle($index)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($zebra_bg);
									}
								}
								
							break;
							
							// extended columns
							case $col >= $first_extended_column && $col <= $last_extended_column :

								$data = $extended_columns[$col-$first_extended_column];
								
								$objPHPExcel->getActiveSheet()->setCellValue($index, $data['caption']);
								
								if ($data['rotate']) {
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleVerticalText);
								}
								
								if ($data['width']) {
									$objPHPExcel->getActiveSheet()->getColumnDimension(spreadsheet::key($col))->setWidth($data['width']);
								}
								
								if ($data['bg']) {
									$objPHPExcel->getActiveSheet()->getStyle($index)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($data['bg']);
								}
								
								if ($data['color']) {
									$objPHPExcel->getActiveSheet()->getStyle($index)->getFont()->getColor()->setRGB($data['color']);
								}
							
							break;
							
							// calculated columns
							case $col >= $first_calculated_column && $col <= $last_calculated_column :
							
								$data = $calculated_columns[$col-$first_calculated_column];
							
								$objPHPExcel->getActiveSheet()->setCellValue($index, $data['caption']);
								
								if ($data['rotate']) {
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleVerticalText);
								}
								
								if ($data['width']) {
									$objPHPExcel->getActiveSheet()->getColumnDimension(spreadsheet::key($col))->setWidth($data['width']);
								}
								
								if ($data['bg']) {
									$objPHPExcel->getActiveSheet()->getStyle($index)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($data['bg']);
								}
								
								if ($data['color']) {
									$objPHPExcel->getActiveSheet()->getStyle($index)->getFont()->getColor()->setRGB($data['color']);
								}
							
							break;
							
						}
							
					break;

					// item production movement
					case $row >= $first_items_row && $row <= $last_items_row :
						
						// controll for category row
						if (!in_array($category_id, $inserted_categories)) {
							array_push($inserted_categories, $category_id);
							$increment_category++;
							$category_row = true;
						}
						
						
						// category name
						if ($category_row) {
						
							if($col==1) {
								
								$cel = spreadsheet::key($total_spreadsheet_columns,$row);
								$objPHPExcel->getActiveSheet()->mergeCells("$index:$cel");
								$objPHPExcel->getActiveSheet()->setCellValue("A$row", $categories[$category_id]);
								$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
								$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(12);
								$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CCCCCC');
							}
						}
						else {
	
							switch ($col) {
									
								// counter
								case 1:
									$objPHPExcel->getActiveSheet()->setCellValue($index, $key);
								break;
					
									// item code
								case 2:
									$objPHPExcel->getActiveSheet()->setCellValue($index, $code);
								break;
								
									// item price
								case 3:
									$cel_price = $index;
									$objPHPExcel->getActiveSheet()->setCellValue($index, $price);
									$objPHPExcel->getActiveSheet()->getStyle($index)->getNumberFormat()->setFormatCode('0.00');
								break;
							
								// movement of stock
								case $col >= $first_weeks_column && $col <= $last_weeks_column : 
		
									switch ($step) {
											
										// stock value (week)
										case 1:
											
											$objPHPExcel->getActiveSheet()->setCellValue($index, $weeks[$week]['scpps_week_stock']);	
											
											$objConditional = new PHPExcel_Style_Conditional();
											$objConditional->setConditionType(PHPExcel_Style_Conditional::CONDITION_EXPRESSION);
											$objConditional->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_EQUAL);
											$objConditional->addCondition("AND(NOT(ISBLANK($index)),$index=0) ");
											$objConditional->getStyle()->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getEndColor()->setRGB('00FF00');
											$objPHPExcel->getActiveSheet()->getStyle($index)->setConditionalStyles(array($objConditional));
											$objPHPExcel->getActiveSheet()->getStyle($index)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
											
											$value_of_stock[] = $index;
										
										break;
												
										// production order (week)
										case 2:
											
											$objPHPExcel->getActiveSheet()->setCellValue($index, $weeks[$week]['scpps_week_not_confirmed']);
											$objPHPExcel->getActiveSheet()->getStyle($index)->setConditionalStyles(array($conditional_new_production_order));
											$objPHPExcel->getActiveSheet()->getStyle($index)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
											
										break;
												
											// movement of stock (week)
										case 3 && $col <= $last_weeks_column:
											
											$cel1 = spreadsheet::key($col-2,$row);
											$cel2 = spreadsheet::key($col-1,$row);
											$cel3 = spreadsheet::key($col+1,$row);
											
											if ($col == $last_weeks_column) $objPHPExcel->getActiveSheet()->setCellValue($index, "");
											else $objPHPExcel->getActiveSheet()->setCellValue($index, "=IF($cel3 > 0,$cel3-$cel2-$cel1,\"\")");
											
											$objConditional = new PHPExcel_Style_Conditional();
											$objConditional->setConditionType(PHPExcel_Style_Conditional::CONDITION_EXPRESSION);
											$objConditional->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_EQUAL);
											$objConditional->addCondition("AND(NOT(ISBLANK($index)),$index=0) ");
											$objConditional->getStyle()->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getEndColor()->setRGB('FFC0CB');
											$objPHPExcel->getActiveSheet()->getStyle($index)->setConditionalStyles(array($objConditional));
											
											$sum_movement_stock[] = $index;
											
										break;
	
									}
									
									$objPHPExcel->getActiveSheet()->getStyle($index)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($zebra_bg);
		
								break;
								
								// extended columns
								case $col >= $first_extended_column && $col <= $last_extended_column :
									
									
									$i = $col-$first_extended_column;
									$column_name = $extended_columns[$i]['name'];
									$value = $weeks[$calculated_week][$column_name];
									
									if (!$value && $i > 1) {
										$value = $last_additional_data[$item_id][$column_name];
									}
										
									$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
										
									// production order - not confirmed yet
									if ($i==0) {
										$conditionalStyles = $objPHPExcel->getActiveSheet()->getStyle($index)->getConditionalStyles();
										array_push($conditionalStyles, $conditional_production_order_notconfirmed);
										$objPHPExcel->getActiveSheet()->getStyle($index)->setConditionalStyles($conditionalStyles);
									}
									
									// available after deduction of projects in pipeline
									if ($i==1) {
										$cel_available_after_deduction = $index;
										$objPHPExcel->getActiveSheet()->getStyle($index)->getNumberFormat()->setFormatCode('[Red][>0];[Black][<0]');
									}
										
									$objPHPExcel->getActiveSheet()->getStyle($index)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
									$extended_column_totals[$column_name][] = $index;
									
								break;
								
								// calculated columns
								case $col >= $first_calculated_column && $col <= $last_calculated_column :
										
									$i = $col-$first_calculated_column;
									$column_name = $calculated_columns[$i]['name'];
									
									$calculated_column_totals[$column_name][] = $index;
										
									switch ($i) {
						
										case 0: // total consumtion up to date unit
											
											$cel_total_consumtion = $index;
											$value = ($sum_movement_stock) ? "=SUM(".join(',',$sum_movement_stock).")" : '0.00';
											$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
												
										break;
										
										case 1: // total consumtion up to date €/chf
											
											$objPHPExcel->getActiveSheet()->setCellValue($index, "=IF($cel_total_consumtion > 0, $cel_price*$cel_total_consumtion, 0.00)");
											$column_ranges['consumtion_currency'][] = $index;
											
										break;
									
										case 2: // average per week
											
											$cel_average_week = $index;
											$objPHPExcel->getActiveSheet()->setCellValue($index, "=IF($cel_total_consumtion > 0, $cel_total_consumtion / $total_weeks_range, 0.00)");
											
										break;
										
										case 3:// weeks left
											
											$objPHPExcel->getActiveSheet()->setCellValue($index, "=IF($cel_average_week > 0, $cel_available_after_deduction/$cel_average_week, 0.00)");
											
										break;
									
										case 4: // reproduction time
											
											$objPHPExcel->getActiveSheet()->setCellValue($index, $reproduction_time);
											
										break;
										
										case 5: // total values of stock
											
											$cel_total_values_of_stock = $index;
											$range = spreadsheet::key($first_weeks_column, $row)."_".spreadsheet::key($last_weeks_column, $row);
											$value = ($value_of_stock) ?  "=SUM(".join(',',$value_of_stock).")*$cel_price" : '0.00';
											$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
											
											
										break;
										
										case 6: // percent total value of stock
											
											$cel_sum_total_value_of_stock = spreadsheet::key($col-1, $first_calculated_row);
											$objPHPExcel->getActiveSheet()->setCellValue($index, "=IF($cel_sum_total_value_of_stock > 0, $cel_total_values_of_stock / $cel_sum_total_value_of_stock, 0.00)");
											
										break;
										
										case 7: // total inc. production order
											
											$cel_total_production_order = $index;
											$objPHPExcel->getActiveSheet()->setCellValue($index, "=$cel_available_after_deduction*$cel_price");
											
										break;
										
										
										case 8: // percent total production order
											
											$cel_sum_production_order = spreadsheet::key($col-1, $first_calculated_row);
											$objPHPExcel->getActiveSheet()->setCellValue($index, "=IF($cel_sum_production_order > 0, $cel_total_production_order / $cel_sum_production_order, 0.00)");
											
										break;
						
									}
									
									$objPHPExcel->getActiveSheet()->getStyle($index)->getNumberFormat()->setFormatCode("0.00");
										
								break;			
							}
						}
							
					break;
					
					// extended rows
					case $row >= $first_extended_row && $row <= $last_extended_row :
					
					break;
					
					// row separators
					case $row >= $first_row_separator && $row <= $last_row_separator:
						
						if ($col == 1) {
							$cel = spreadsheet::key($total_spreadsheet_columns, $row);
							$objPHPExcel->getActiveSheet()->mergeCells("$index:$cel");
						}
						
					break;
					
					// calculated rows
					case $row >= $first_calculated_row && $row <= $last_calculated_row :
					
						switch ($col) {
							
							// color descriptions
							case 2: 
							
								$i = $row - $first_calculated_row;
								$data = $calculated_rows[$i];
								
								$grid[$index]['text'] = $data['caption'];
								$grid[$index]['cls'] =  $data['class'].' '.$data['name'];
								$grid[$index]['readonly'] = "true";
								
								if ($data['caption']) {
									$objPHPExcel->getActiveSheet()->setCellValue($index, $data['caption']);
								}
								
								if ($data['rotate']) {
									$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styleVerticalText);
								}
								
								if ($data['width']) {
									$objPHPExcel->getActiveSheet()->getColumnDimension(spreadsheet::key($col))->setWidth($data['width']);
								}
								
								if ($data['bg']) {
									$objPHPExcel->getActiveSheet()->getStyle($index)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($data['bg']);
								}
								
								if ($data['color']) {
									$objPHPExcel->getActiveSheet()->getStyle($index)->getFont()->getColor()->setRGB($data['color']);
								}
								
							break;
							
							// merge empty cells
							// set for first and second calculated rows currency symboles
							case $col == $first_weeks_column:
								
								// currency symbole for EUR
								if ($row == $first_calculated_row) {
									$objPHPExcel->getActiveSheet()->setCellValue($index, 'TOTAL (EUR)');
									$objPHPExcel->getActiveSheet()->getStyle($index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
									$cel = spreadsheet::key($last_weeks_column,$row);
									$objPHPExcel->getActiveSheet()->mergeCells("$index:$cel");
								}
								// currency symbole for CHF
								elseif ($row == $first_calculated_row+1) {
									$objPHPExcel->getActiveSheet()->setCellValue($index, 'TOTAL (CHF)');
									$objPHPExcel->getActiveSheet()->getStyle($index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
									$cel = spreadsheet::key($last_weeks_column,$row);
									$objPHPExcel->getActiveSheet()->mergeCells("$index:$cel");
								}
								else {
									$cel = spreadsheet::key($total_spreadsheet_columns,$row);
									$objPHPExcel->getActiveSheet()->mergeCells("$index:$cel");
								}
								
							break;
							
							// total extended rows
							case $col >= $first_extended_column && $col <= $last_extended_column:
								
								$i = $col - $first_extended_column;
								$data = $extended_columns[$i];
								$column_name = $data['name'];
								
								// sum extended row
								if ($row==$first_calculated_row && $extended_column_totals[$column_name]) {
									$value = "=SUM(".join(',',$extended_column_totals[$column_name]).")";
									$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
									$objPHPExcel->getActiveSheet()->getStyle($index)->getNumberFormat()->setFormatCode("0.00");
								}
								
								// convert in CHF
								if ($row==$first_calculated_row+1) { 
									$cel = spreadsheet::key($col, $first_calculated_row);
									$objPHPExcel->getActiveSheet()->setCellValue($index, "=$cel*$currency_exchange_rate");
									$objPHPExcel->getActiveSheet()->getStyle($index)->getNumberFormat()->setFormatCode("0.00");
								}
								
								if ($row==$first_calculated_row || $row==$first_calculated_row+1) {
									$objPHPExcel->getActiveSheet()->getStyle($index)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($background_color_1);
								}
								
							break;
							
							// total calculated rows
							case $col >= $first_calculated_column && $col <= $last_calculated_column:
								
								$i = $col - $first_calculated_column;
								$data = $calculated_columns[$i];
								$column_name = $data['name'];
							
								// sum calculated row
								if ($row==$first_calculated_row && $calculated_column_totals[$column_name] ) {
									$value = "=SUM(".join(',',$calculated_column_totals[$column_name]).")";
									$objPHPExcel->getActiveSheet()->setCellValue($index, $value);
									$objPHPExcel->getActiveSheet()->getStyle($index)->getNumberFormat()->setFormatCode("0.00");
								}
								
								// convert in CHF
								if ($data['amount'] && $row==$first_calculated_row+1) {
									$cel = spreadsheet::key($col, $first_calculated_row);
									$objPHPExcel->getActiveSheet()->setCellValue($index, "=$cel*$currency_exchange_rate");
									$objPHPExcel->getActiveSheet()->getStyle($index)->getNumberFormat()->setFormatCode("0.00");
								}
								
								if ($row==$first_calculated_row || $row==$first_calculated_row+1) {
									$objPHPExcel->getActiveSheet()->getStyle($index)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($background_color_1);
								}
							
							break;
						}
						
					break;
				}
	
				// loop controll
				if ($col-$trigger == $columns_per_week) {
					$week = array_shift($colWeeks);
					$month = date::month_number_from_week($week, $current_year);
					$trigger = $col;
					$zebra_bg = ($zebra_bg==$background_color_1) ? $background_color_2 : $background_color_1;
				}
			}
		}
	

		$range = "a2:".spreadsheet::key($total_spreadsheet_columns, $total_spreadsheet_rows);
		$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($borders);
		
		// sheet security		
		$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
		
		// export
		$objPHPExcel->getActiveSheet()->setTitle($sheet_name);
		$objPHPExcel->setActiveSheetIndex(0);
		
		// redirect output to client browser
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		
		exit;
	}
	else {
		message::empty_result();
		$archived = ($archived) ? '/archived' : null;
		url::redirect("/$application/$controller$archived/$action/$id");
	}
		
	
