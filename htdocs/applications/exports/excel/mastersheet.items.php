<?php 
		
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	// execution time
	ini_set('max_execution_time', 120);
	ini_set('memory_limit', '1024M');
	
	$settings = Settings::init();
	$settings->load('data');
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$filter = $_REQUEST['filter'];
	$id = $_REQUEST['id'];
							 