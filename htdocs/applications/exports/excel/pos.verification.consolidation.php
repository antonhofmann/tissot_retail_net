<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
	require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');
	
	// execution time
	ini_set('max_execution_time', 600);
	ini_set('memory_limit', '102400M');
	
	$settings = Settings::init();
	$settings->load('data');
	$user = User::instance();
	$translate = Translate::instance();
	
	// request vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];

	$_REQUEST = session::filter($application, "$controller.$archived.$action");
	
	$period = $_REQUEST['periodes'];
	$region = $_REQUEST['regions'];
	$state = $_REQUEST['states'];
	$addresstype = $_REQUEST['addresstypes'];
	$legaltype = $_REQUEST['legaltypes'];
	$client = $_REQUEST['clients'];
	
	
	$pagetitle = 'POS Verifications';

	$model = new Model($application);

	
/************************************************************** filter captions *****************************************************************/
	
	$filterCaptions = $translate->period.": ".date('F Y', $period);
	
	if ($region) {
		
		$result = $model->query("
			SELECT DISTINCT
				region_id,
				region_name
			FROM db_retailnet.addresses
			INNER JOIN db_retailnet.countries ON country_id = address_country
			INNER JOIN db_retailnet.regions ON region_id = country_region
		")->fetchAll();
			
		$regions = _array::extract($result);
		$filterCaptions .= "\nRegion: ".$regions[$region];
	}
	
	if ($state) {
		
		$states = array(
			1 => 'Completed',
			2 => 'Uncompleted'
		);
		
		$filterCaptions .= "\nData Entry State: ".$states[$state];
	}
	
	if ($addresstype) {
		
		// client type: agents, subsidiaries, affiliates
		$result = $model->query("
			SELECT client_type_code
			FROM db_retailnet.client_types
			WHERE client_type_id = $addresstype
		")->fetch();
		
		$clientTypeName = $result['client_type_code'];
				
		$filterCaptions .= "\nAddress Type: $clientTypeName";
	}

	
/**************************************************************** actual numbers ******************************************************************/
	
	$filter = null;
	
	// filter: region
	if ($region) {
		$filter = " AND country_region = $region";
	}
	
	// filter: address type
	if ($addresstype) {
		$filter .= " AND clients.address_client_type = $addresstype AND ( clients.address_involved_in_planning = 1 OR clients.address_involved_in_planning_ff = 1 )";
	}
	
	$result = $model->query("
		SELECT DISTINCT
			posverification_data_id,
			posverification_data_distribution_channel_id,
			posverification_data_actual_number,
			posverification_address_id,
			addresses.address_id,
			addresses.address_parent,
			posverification_confirm_date
		FROM posverification_data	
		INNER JOIN posverifications ON posverification_id = posverification_data_posverification_id
		INNER JOIN db_retailnet.addresses ON address_id = posverification_address_id
		LEFT JOIN db_retailnet.posaddresses ON posaddress_client_id = posverification_address_id
		LEFT JOIN db_retailnet.addresses AS clients ON clients.address_id = posaddress_client_id
		LEFT JOIN db_retailnet.countries on country_id = addresses.address_country
		WHERE UNIX_TIMESTAMP(posverification_date) = '$period' $filter
	")->fetchAll();
	
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$key = $row['address_id'].$row['address_parent'];
			$channel = $row['posverification_data_distribution_channel_id'];
			$permitted = true;
			
			if ($state) {
				
				$permitted = false;
				
				// confirmed actual numbers
				if ($state==1 && $row['posverification_confirm_date']) {
					$permitted = true;
				}
				
				// unconfirmed actual numbers
				if ($state==2 && !$row['posverification_confirm_date']) {
					$permitted = true;
				}
			}
			
			if ($permitted) {
				
				// sum actual number over all companies
				$actualNumber[$channel] = $actualNumber[$channel] + $row['posverification_data_actual_number'];
				
				// sum actual number for each client
				$actualNumberCompany[$channel][$key] = $actualNumberCompany[$channel][$key] + $row['posverification_data_actual_number'];
			}
			
			// confirmed verification
			if ($row['posverification_confirm_date']) {
				$verifications_confirmed[$key] = true;
			}
		}
	}

	
/************************************************************** distribution channels ****************************************************************/
	
	// filter: address type
	$address_type_filter = "";
	$region_filter = "";
	$extended_filter = null;



	if ($region) {
		$region_filter = " AND country_region = $region ";
	}
	
	// filter: client type
	if ($addresstype) {
		$address_type_filter = " AND addresses.address_client_type = $addresstype ";
	} 
	else {
		
		$address_type_filter = " AND addresses.address_client_type IN (1, 2, 3) ";
		
		$extended_filter = "
		OR(
			parentaddresses.address_active = 1 
			AND parentaddresses.address_type = 1
			AND addresses.address_client_type = 3
			AND (
				posaddress_store_closingdate IS NULL
				OR posaddress_store_closingdate = '0000-00-00'
				OR posaddress_store_closingdate = ''
			)
			$region_filter
		)		
		";
	}
	
	$result = $model->query("
		SELECT DISTINCT
			posaddress_id,
			addresses.address_id,
			addresses.address_parent,
			posaddress_client_id,
			posaddress_distribution_channel
		FROM db_retailnet.posaddresses
		LEFT JOIN db_retailnet.addresses as addresses ON addresses.address_id = posaddress_client_id
		LEFT JOIN db_retailnet.addresses as parentaddresses ON parentaddresses.address_id = posaddress_franchisee_id
		LEFT JOIN db_retailnet.countries on country_id = addresses.address_country
		WHERE (
			addresses.address_active = 1 
			AND addresses.address_type = 1
			AND (
				addresses.address_involved_in_planning = 1 
				OR addresses.address_involved_in_planning_ff = 1
				
			)
			AND (
				posaddress_store_closingdate IS NULL
				OR posaddress_store_closingdate = '0000-00-00'
				OR posaddress_store_closingdate = ''
			)
			$address_type_filter
			$region_filter
		)
		$extended_filter	
	")->fetchAll();
	
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$key = $row['address_id'].$row['address_parent'];
			$channel = $row['posaddress_distribution_channel'];
			$permitted = true;
			
			if ($state) {
			
				$permitted = false;
			
				// confirmed actual numbers
				if ($state==1) {
					$permitted = ($verifications_confirmed[$key]) ? true : false;
				}
			
				// unconfirmed actual numbers
				if ($state==2) {
					$permitted = ($verifications_confirmed[$key]) ? false : true;
				}
			}
			
			if ($permitted) {
				
				if ($channel) {
					
					// count distribution channels over all companies
					$inPosIndex[$channel] = $inPosIndex[$channel] + 1;
					
					// count channels for each client
					$companyDistributionChannels[$channel][$key] = $companyDistributionChannels[$channel][$key] + 1;
					
				} 
				// pos without distributtion channels
				else {
					$grandCompaniesWithoutChannels = $grandCompaniesWithoutChannels + 1;
					$companiesWhithoutChannels[$key] = $companiesWhithoutChannels[$key] + 1;
				}
			}
		}
	}
	
	
/**************************************************************** clients **********************************************************************/
	
	// filter: address type
	$filter = $addresstype
		? " AND address_client_type = $addresstype"
		: " AND address_client_type IN (1, 2, 3)";
	
	// filter: region
	if ($region) {
		$filter .= " AND country_region = $region";
	}

	if ($client) {
		$filter .= " AND address_id = $client";
	}

	$result = $model->query("
		SELECT DISTINCT
			address_id,
			address_parent,
			address_company,
			country_name
		FROM db_retailnet.addresses
		LEFT JOIN db_retailnet.countries on country_id = address_country
		WHERE 
			address_active = 1
		AND address_type = 1
		AND (
			address_involved_in_planning = 1 
			OR address_involved_in_planning_ff = 1
		)
		$filter
		ORDER BY country_name
	")->fetchAll();

	if ($result) {
		
		$companies = array();
		
		foreach ($result as $row) {
			
			$key = $row['address_id'].$row['address_parent'];
			$permitted = true;
			
			// confirmed clients
			if ($state==1) {
				$permitted = ($verifications_confirmed[$key]) ? true : false;
			}
			
			// unconfirmed clients
			if ($state==2) {
				$permitted = ($verifications_confirmed[$key]) ? false : true;
			}
			
			if ($permitted) {
				$companies[$key]['country_name'] = $row['country_name'];
				$companies[$key]['address_company'] = $row['address_company'];
			}
		}
	}
	
/*********************************************************** distribution channels *************************************************************/

	$result = $model->query("
		SELECT DISTINCT
			mps_distchannel_id,
			mps_distchannel_group_name,
			mps_distchannel_code,
			mps_distchannel_name
		FROM db_retailnet.mps_distchannels
		INNER JOIN db_retailnet.mps_distchannel_groups ON mps_distchannels.mps_distchannel_group_id = mps_distchannel_groups.mps_distchannel_group_id	
		WHERE mps_distchannel_active = 1 
		ORDER BY mps_distchannel_group_name, mps_distchannel_name
	")->fetchAll();
	
	if ($result) {
		
		$distributionChannels = array();
		
		foreach ($result as $row) {
			
			$key = strtolower(str_replace(' ', '', $row['mps_distchannel_group_name']));
			
			// group name
			$distributionChannels[$key]['group'] = $row['mps_distchannel_group_name'];
			
			// distribution channel
			$distributionChannels[$key]['channels'][$row['mps_distchannel_id']] = array(
				'mps_distchannel_name' => $row['mps_distchannel_name'],
				'mps_distchannel_code' => $row['mps_distchannel_code']
			);
		}
	}
		
	
/************************************************************ spreadsheet data ***************************************************************/

	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator(settings::init()->project_name)->setLastModifiedBy(settings::init()->project_name) ->setTitle($pagetitle);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	
	$styles['borders'] = array(
		'borders' => array( 
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);
	
	$styles['align-left'] = array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		)
	);
	
	$styles['align-center'] = array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		)
	);
	
	$styles['small-caption'] = array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'ededed')
		),
		'font' => array(
			'name'=>'Arial',
			'size'=> 10, 
			'color' => array(
				'rgb' => '000000'
			)
		)
	);
	
	$styles['bg-total-group'] = array(
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'f3f7e4')
		)
	);
	
	$styles['bg-grand-total'] = array(
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'e4f5f7')
		)
	);
	
	$styles['confirmed'] = array(
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'C3FACE')
		)
	);

	$styles['caption'] = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 13
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'ededed')
		)
	);
	
	$styles['group'] = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'ededed')
		)
	);
	
	$styles['delta'] = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 12
		)
	);
		
	
	
	// total clients
	$totalClients = count($companies);
	
	// set last column
	// fixed columns: 4
	// seperator: 1
	// datagrid: clients*2 + total clients
	$totalColumns = 5 + $totalClients*2 + $totalClients;
	
	// last column
	$lastColumn = spreadsheet::key($totalColumns);
	
	// dimensions
	$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(40);
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(3);
	
	// header
	$row = 1;
	$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
	$objPHPExcel->getActiveSheet()->setCellValue("A$row", "$pagetitle");
	$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(20);

	// seperator
	$row++;
	$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
	$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: ".date('d.m.Y H:i:s'));
	
	$row++;
	$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(60);
	
	// filters cell
	$objPHPExcel->getActiveSheet()->setCellValue("A$row", $filterCaptions);
	$objPHPExcel->getActiveSheet()->getStyle("A$row")->getAlignment()->setWrapText(true);
	$objPHPExcel->getActiveSheet()->mergeCells("A$row:B$row");
	
	// over all company
	$objPHPExcel->getActiveSheet()->setCellValue("c$row", "Over all Countries");
	$objPHPExcel->getActiveSheet()->mergeCells("c$row:e$row");
	$objPHPExcel->getActiveSheet()->getStyle("c$row")->applyFromArray($styles['caption'], false);
	$objPHPExcel->getActiveSheet()->getStyle("c$row")->applyFromArray($styles['borders'], false);
	$objPHPExcel->getActiveSheet()->getStyle("d$row")->applyFromArray($styles['borders'], false);
	$objPHPExcel->getActiveSheet()->getStyle("e$row")->applyFromArray($styles['borders'], false);
	
		
	// company captions
	if ($companies) {

		$col = 7;
		$_row = $row + 1;

		foreach ($companies as $key => $value) {
			
			$cel = spreadsheet::key($col);
			$cel_next = spreadsheet::key($col+1);
			$cel_delta = spreadsheet::key($col+2);
			
			$index = $cel.$row;
			$index_next = $cel_next.$row;
			$index_delta = $cel_delta.$row;
			
			// cel dimension
			$objPHPExcel->getActiveSheet()->getColumnDimension($cel)->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension($cel_next)->setWidth(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension($cel_delta)->setWidth(12);
			
			// merge cels
			$objPHPExcel->getActiveSheet()->mergeCells("$index:$index_delta");
			
			// cel value
			$objPHPExcel->getActiveSheet()->setCellValue($index, $value['address_company']);
			//$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['caption']);

			$_index = $cel.$_row;
			$_index_next = $cel_next.$_row;
			$_index_delta = $cel_delta.$_row;

			$objPHPExcel->getActiveSheet()->setCellValue($_index, 'POS Index');
			$objPHPExcel->getActiveSheet()->setCellValue($_index_next, 'Actual');
			$objPHPExcel->getActiveSheet()->setCellValue($_index_delta, 'Delta');
			
			$objPHPExcel->getActiveSheet()->getStyle("$index:$index_delta")->applyFromArray($styles['caption'], false);
			$objPHPExcel->getActiveSheet()->getStyle("$_index:$_index_delta")->applyFromArray($styles['small-caption'], false);
			$objPHPExcel->getActiveSheet()->getStyle("$index:$_index_delta")->applyFromArray($styles['borders'], false);

			if ($verifications_confirmed[$key]) {
				$objPHPExcel->getActiveSheet()->getStyle("$index")->applyFromArray($styles['confirmed'], false);
			}

			// separator
			$col = $col + 3;
			$cel = spreadsheet::key($col);
			$objPHPExcel->getActiveSheet()->getColumnDimension($cel)->setWidth(3);

			$col++;
		}
	}
	
	
	// row 2: seperator
	$row = 4;
	//$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
	
	
	$row = 5;
	
	$grandTotal = array();
	

	if ($distributionChannels) {
		
		$export = true;

		foreach ($distributionChannels as $group => $data) {
			
			$col = 1;
				
			// distribution groups
			if ($data['group']) {
				
				$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
				
				// distribution group
				$index = spreadsheet::key($col,$row);
				$objPHPExcel->getActiveSheet()->setCellValue($index, $data['group']);
				
				// distribution code
				$col++;
				$index = spreadsheet::key($col,$row);
				$objPHPExcel->getActiveSheet()->setCellValue($index, translate::instance()->code);
				
				// in pos index (over all countries)
				$col++;
				$index = spreadsheet::key($col,$row);
				$objPHPExcel->getActiveSheet()->setCellValue($index, translate::instance()->in_pos_index);
				
				// actual number (over all countries)
				$col++;
				$index = spreadsheet::key($col,$row);
				$objPHPExcel->getActiveSheet()->setCellValue($index, translate::instance()->posverification_data_actual_number);
				
				// delta (over all countries)
				$col++;
				$index = spreadsheet::key($col,$row);
				$objPHPExcel->getActiveSheet()->setCellValue($index, translate::instance()->delta);
				
				// styling
				$objPHPExcel->getActiveSheet()->getStyle("A$row:E$row")->applyFromArray($styles['group']+$styles['borders'], false);
				$objPHPExcel->getActiveSheet()->getStyle("A$row:E$row")->applyFromArray($styles['align-left']);
				

				if ($companies) {

					$col = 7;

					foreach ($companies as $key => $value) {
						
						
						$index = spreadsheet::key($col,$row);
						$index_next = spreadsheet::key($col+1,$row);
						$index_delta = spreadsheet::key($col+2,$row);
						
						// merge cels
						$objPHPExcel->getActiveSheet()->mergeCells("$index:$index_delta");
							
						// cel value
						$objPHPExcel->getActiveSheet()->setCellValue($index, $value['country_name']);
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['group']);
						
						if ($verifications_confirmed[$key]) {
							$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['confirmed'], false);
						}
							
						// set border
						$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['borders'], false);
						$objPHPExcel->getActiveSheet()->getStyle($index_next)->applyFromArray($styles['borders'], false);
						$objPHPExcel->getActiveSheet()->getStyle($index_delta)->applyFromArray($styles['borders'], false);

						$col = $col+4;
					}
				}
				
				$row++;
			}

			// distribution channels
			if ($data['channels']) {

				$group_first_row = $row;
				$group_last_row = $row + count($data['channels']) - 1;

				foreach ($data['channels'] as $channel => $value) {
					
					$col = 1;
					
					// distribution channel name
					$index = spreadsheet::key($col,$row);
					$objPHPExcel->getActiveSheet()->setCellValue($index, $value['mps_distchannel_name']);
					
					// distribution channel code
					$col++;
					$index = spreadsheet::key($col,$row);
					$objPHPExcel->getActiveSheet()->setCellValue($index, $value['mps_distchannel_code']);
					
					// in pos index over all countries
					$col++;
					$index = spreadsheet::key($col,$row);
					$objPHPExcel->getActiveSheet()->setCellValue($index, $inPosIndex[$channel]);
					
					// actual number over all countries
					$col++;
					$index = spreadsheet::key($col,$row);
					$objPHPExcel->getActiveSheet()->setCellValue($index, $actualNumber[$channel]);
					
					// delta over all countries
					$col++;
					$delta = $actualNumber[$channel] - $inPosIndex[$channel];
					$index = spreadsheet::key($col,$row);
					$objPHPExcel->getActiveSheet()->setCellValue($index, $delta);
					
					if ($delta <> 0) {
						$objPHPExcel->getActiveSheet()->getStyle($index)->getFont()->getColor()->setRGB('FF0000');
					}

					// styling
					$objPHPExcel->getActiveSheet()->getStyle("A$row:E$row")->applyFromArray($styles['borders']);
					
					// sum groups
					$sumGroup[$group]['pos_index'] = $sumGroup[$group]['pos_index'] + $inPosIndex[$channel];
					$sumGroup[$group]['actual_number'] = $sumGroup[$group]['actual_number'] + $actualNumber[$channel];
					$sumGroup[$group]['delta'] = $sumGroup[$group]['delta'] + $delta;
					
					if ($companies) {

						$col = 7;

						foreach ($companies as $company => $value) {
							
							// in pos index
							$index = spreadsheet::key($col,$row);
							$objPHPExcel->getActiveSheet()->setCellValue($index, $companyDistributionChannels[$channel][$company]);
							$sumGroup[$group]['company'][$company]['pos_index'] = $sumGroup[$group]['company'][$company]['pos_index'] + $companyDistributionChannels[$channel][$company];
							
							// actual numbers
							$col++;
							$index_next = spreadsheet::key($col,$row);
							$objPHPExcel->getActiveSheet()->setCellValue($index_next, $actualNumberCompany[$channel][$company]);
							$objPHPExcel->getActiveSheet()->getStyle("$index:$index_next")->applyFromArray($styles['borders']);
							$sumGroup[$group]['company'][$company]['actual_number'] = $sumGroup[$group]['company'][$company]['actual_number'] + $actualNumberCompany[$channel][$company];
							
							// delta
							$col++;
							$index_delta = spreadsheet::key($col,$row);
							$delta = $actualNumberCompany[$channel][$company] - $companyDistributionChannels[$channel][$company];
							$objPHPExcel->getActiveSheet()->setCellValue($index_delta, $delta);
							$objPHPExcel->getActiveSheet()->getStyle("$index:$index_delta")->applyFromArray($styles['borders']);
							$sumGroup[$group]['company'][$company]['delta'] = $sumGroup[$group]['company'][$company]['delta'] + $delta;
							
							if ($delta <> 0) {
								$objPHPExcel->getActiveSheet()->getStyle($index_delta)->getFont()->getColor()->setRGB('FF0000');
							}
								
							$col = $col+2;
						}
					}
						
					$row++;
				}

				$col = 1;
				
				// caption total group
				$index = spreadsheet::key($col,$row);
				$objPHPExcel->getActiveSheet()->setCellValue($index, 'Total '.$data['group']);
				
				// total code
				$col++;
				
				// total group in pos inde (over all companies)
				$col++;
				$index = spreadsheet::key($col,$row);
				$objPHPExcel->getActiveSheet()->setCellValue($index, $sumGroup[$group]['pos_index']);
				
				// total group actual number (over all companies)
				$col++;
				$index = spreadsheet::key($col,$row);
				$objPHPExcel->getActiveSheet()->setCellValue($index, $sumGroup[$group]['actual_number']);
				
				// total delta (over all companies)
				$col++;
				$index = spreadsheet::key($col,$row);
				$objPHPExcel->getActiveSheet()->setCellValue($index, $sumGroup[$group]['delta']);
				
				if ($sumGroup[$group]['delta'] <> 0) {
					$objPHPExcel->getActiveSheet()->getStyle($index)->getFont()->getColor()->setRGB('FF0000');
				}
				
				
				// styling
				$objPHPExcel->getActiveSheet()->getStyle("A$row:E$row")->applyFromArray($styles['bg-total-group']+$styles['borders']);
				
				// grand total
				$grandTotal['pos_index'] = $grandTotal['pos_index'] + $sumGroup[$group]['pos_index'];
				$grandTotal['actual_number'] = $grandTotal['actual_number'] + $sumGroup[$group]['actual_number'];
				$grandTotal['delta'] = $grandTotal['delta'] + $sumGroup[$group]['delta'];

				// totals
				if ($companies) {

					$col = 7;

					foreach ($companies as $company => $value) {
						
						// total group in pos index
						$index = spreadsheet::key($col,$row);
						$objPHPExcel->getActiveSheet()->setCellValue($index, $sumGroup[$group]['company'][$company]['pos_index']);
						$grandTotal['company']['pos_index'][$company] = $grandTotal['company']['pos_index'][$company]  + $sumGroup[$group]['company'][$company]['pos_index'];

						// total group actual number
						$col++;
						$index_next = spreadsheet::key($col,$row);
						$objPHPExcel->getActiveSheet()->setCellValue($index_next, $sumGroup[$group]['company'][$company]['actual_number']);
						$objPHPExcel->getActiveSheet()->getStyle("$index:$index_next")->applyFromArray($styles['bg-total-group']+$styles['borders']);
						$grandTotal['company']['actual_number'][$company] = $grandTotal['company']['actual_number'][$company]  + $sumGroup[$group]['company'][$company]['actual_number'];

						// total group delta
						$col++;
						$index_delta = spreadsheet::key($col,$row);
						$objPHPExcel->getActiveSheet()->setCellValue($index_delta, $sumGroup[$group]['company'][$company]['delta']);
						$objPHPExcel->getActiveSheet()->getStyle("$index:$index_delta")->applyFromArray($styles['bg-total-group']+$styles['borders']);
						$grandTotal['company']['delta'][$company] = $grandTotal['company']['delta'][$company]  + $sumGroup[$group]['company'][$company]['delta'];
						
						if ($sumGroup[$group]['company'][$company]['delta'] <> 0) {
							$objPHPExcel->getActiveSheet()->getStyle($index_delta)->getFont()->getColor()->setRGB('FF0000');
						}
							
						$col = $col+2;
					}
				}

				$row++;
			}
				
			$row++;
		}
	}

	// grand totals
	if ($grandTotal) {
		
		$col = 1;
		
		// pos whithout distribution channels
		$index = spreadsheet::key($col,$row);
		$objPHPExcel->getActiveSheet()->setCellValue($index, "POS Locations whithout distribution channels");

		// code
		$col++;
		
		// grand total in pos inde
		$col++;
		$index = spreadsheet::key($col,$row);
		$objPHPExcel->getActiveSheet()->setCellValue($index, $grandCompaniesWithoutChannels);

		
		// styling
		$objPHPExcel->getActiveSheet()->getStyle("A$row:E$row")->applyFromArray($styles['bg-grand-total']+$styles['borders']);

		if ($companies) {

			$col = 7;

			foreach ($companies as $company => $value) {
				
				$index = spreadsheet::key($col,$row);
				$objPHPExcel->getActiveSheet()->setCellValue($index, $companiesWhithoutChannels[$company]);
				
				// styling
				$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['bg-grand-total']+$styles['borders']);

				$col = $col+4;
			}
		}
		
		$row++;
		$col = 1;
		
		// total group caption
		$index = spreadsheet::key($col,$row);
		$objPHPExcel->getActiveSheet()->setCellValue($index, "Grand Total of POS Locations");

		// code
		$col++;
		
		// grand total in pos inde
		$col++;
		$index = spreadsheet::key($col,$row);
		$objPHPExcel->getActiveSheet()->setCellValue($index, $grandTotal['pos_index']+$grandCompaniesWithoutChannels);
		

		// grand total: actual number
		$col++;
		$index = spreadsheet::key($col,$row);
		$objPHPExcel->getActiveSheet()->setCellValue($index, $grandTotal['actual_number']);

		
		// grand total: delta
		$col++;
		$index = spreadsheet::key($col,$row);
		$objPHPExcel->getActiveSheet()->setCellValue($index, $grandTotal['delta']);
		
		if ($grandTotal['delta'] <> 0) {
			$objPHPExcel->getActiveSheet()->getStyle($index)->getFont()->getColor()->setRGB('FF0000');
		}

		
		// styling
		$objPHPExcel->getActiveSheet()->getStyle("A$row:E$row")->applyFromArray($styles['bg-grand-total']+$styles['borders']);

		if ($companies) {

			$col = 7;

			foreach ($companies as $company => $value) {
				
				// grand total in pos index
				$index = spreadsheet::key($col,$row);
				$quantity = $grandTotal['company']['pos_index'][$company] + $companiesWhithoutChannels[$company];
				$objPHPExcel->getActiveSheet()->setCellValue($index, $quantity);
				
				// grand total actual number
				$col++;
				$index_next = spreadsheet::key($col,$row);
				$objPHPExcel->getActiveSheet()->setCellValue($index_next, $grandTotal['company']['actual_number'][$company]);
				$objPHPExcel->getActiveSheet()->getStyle("$index:$index_next")->applyFromArray($styles['bg-grand-total']+$styles['borders']);
				
				// grand total delta
				$col++;
				$index_delta = spreadsheet::key($col,$row);
				$objPHPExcel->getActiveSheet()->setCellValue($index_delta, $grandTotal['company']['delta'][$company]);
				$objPHPExcel->getActiveSheet()->getStyle("$index:$index_delta")->applyFromArray($styles['bg-grand-total']+$styles['borders']);
				
				if ($grandTotal['company']['delta'][$company] <> 0) {
					$objPHPExcel->getActiveSheet()->getStyle($index_delta)->getFont()->getColor()->setRGB('FF0000');
				}
				
				$col = $col+2;
			}
		}
	}	

		
/*********************************************************************** export ***************************************************************************/

	if ($export) {
		
		// file name
		$filename = 'pos.verification.consolidation.'.date('Ymd');
		
		// set active sheet
		$objPHPExcel->getActiveSheet()->setTitle($pagetitle);
		$objPHPExcel->setActiveSheetIndex(0);

		// phpexcel headers
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}
	else {
		message::empty_result();
		url::redirect("/$application/$controller$archived/$action/$id");
	}
