<?php
/*
    Excel Copmanies List

    Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2012-02-01
    Version:        1.1
    Updated: 		2012-10-19, Roger Bucher

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*/

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$model = new Model(Connector::DB_CORE);

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];

	// request execution time
	ini_set('max_execution_time', 120);

	// request
	$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

	$title = $application=='catalog' ? 'Item List' : 'Furniture';
	$fileName = "furniture.".date('Y.m.d');
	
	define('SHEET_NAME', $title);
	define('SHEET_FILE_NAME', $fileName);
	define('SHEET_VERSION', "Excel2007");


	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

	if ($_REQUEST['type']) {
		$filters['type'] = "item_type = ".$_REQUEST['type'];
	}
	elseif ($application=='catalog') {
		$filters['types'] = "item_type IN (1, 2, 3, 7)";
	}

	if ($application=='catalog') {
		
		// default sort
		$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "item_code, item_name"; 
		
		// default binds
		$binds['suppliers'] = "LEFT JOIN db_retailnet.suppliers ON supplier_item = item_id";
		$binds['currencies'] = "LEFT JOIN db_retailnet.currencies ON currency_id = supplier_item_currency";
		$binds['units'] = "LEFT JOIN db_retailnet.units ON items.item_unit = units.unit_id";
		$binds['packaging_types'] = "LEFT JOIN db_retailnet.packaging_types ON items.item_packaging_type = packaging_types.packaging_type_id";
	}
	elseif ($application == 'mps') {
		
		// default sort
		$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "item_category_name, item_code, item_name";
		
		// default filter
		$filters['mps'] = "item_visible_in_mps = 1";

		// default binds
		$binds['item_categories'] = "LEFT JOIN db_retailnet.item_categories ON item_category_id = item_category";
	}


	// filter active
	$active = isset($_REQUEST['active']) ? $_REQUEST['active'] : 1;
	$filters['active'] = "item_active = $active";

	// default filter
	$filters['default'] = "item_id > 0";


	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search ) {
		
		$keyword = $_REQUEST['search'];
	
		if ($application=='catalog') {
			
			$filters['search'] = "(
				item_code LIKE '%$keyword%' 
				OR item_name LIKE '%$keyword%' 
				OR item_description LIKE '%$keyword%'
				OR item_price LIKE '%$keyword%'
				OR supplier_item_price LIKE '%$keyword%'
				OR unit_name LIKE '%$keyword%'
				OR address_company LIKE '%$keyword%'
			)";

			$binds['addresses'] = "LEFT JOIN db_retailnet.addresses ON address_id = supplier_address";
		} 
		else {
			$filters['search'] = "(
				item_code LIKE '%$keyword%' 
				OR item_name LIKE '%$keyword%' 
				OR item_description LIKE '%$keyword%'
				OR item_category_name LIKE '%$keyword%'
			)";
		}

		$captions[] = $translate->search.": $keyword";
	}


	if ($application=='catalog') {

		$binds['item_categories'] = "LEFT JOIN db_retailnet.item_categories ON items.item_category = item_categories.item_category_id";
		$binds['item_subcategories'] = "LEFT JOIN db_retailnet.item_subcategories ON item_subcategory_id = items.item_subcategory";

		// filter: supplier
		if ($_REQUEST['suppliers']) {

			$filters['suppliers'] = "supplier_address = ".$_REQUEST['suppliers'];
			$binds['suppliers'] = "INNER JOIN db_retailnet.suppliers ON supplier_item = item_id";

			$company = new Company();
			$company->read($_REQUEST['suppliers']);
			$captions[] = "Supplier: $company->company";
		}

		// filter: item category
		if ($_REQUEST['item_categories']) {

			$filters['item_categories'] = "item_category_id = ".$_REQUEST['item_categories'];
			$binds['item_categories'] = "INNER JOIN db_retailnet.item_categories ON items.item_category = item_categories.item_category_id";

			$itemCategory = new Item_Category();
			$itemCategory->read($_REQUEST['item_categories']);
			$captions[] = "Item Category: $itemCategory->name";
		}

		// filter: product lines
		if ($_REQUEST['product_lines']) {
			
			$filters['product_lines'] = "category_product_line = ".$_REQUEST['product_lines'];
			$binds['category_items'] = "INNER JOIN db_retailnet.category_items ON items.item_id = category_items.category_item_item";
			$binds['categories'] = "INNER JOIN db_retailnet.categories ON category_items.category_item_category = categories.category_id";

			$productLine = new Product_Line();
			$productLine->read($_REQUEST['product_lines']);
			$captions[] = "Product Line: $productLine->name";
		}

		// filter: item types
		if ($_REQUEST['item_types']) {
			
			$filters['item_types'] = "item_type = ".$_REQUEST['item_types'];
			$binds['item_types'] = "INNER JOIN db_retailnet.item_types ON item_type = item_type_id";

			$itemType = new Item_Type();
			$itemType->read($_REQUEST['item_types']);
			$captions[] = "Item Type: $itemType->name";
		}

		$query = "
			SELECT DISTINCT
				item_id,
				item_code,
				item_name,
				(
					SELECT GROUP_CONCAT(supplying_group_name SEPARATOR ', ')
					FROM supplying_groups
					INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = supplying_group_id
					WHERE item_supplying_group_item_id = items.item_id
				) AS supplying_groups,
				item_category_name,
				item_subcategory_name,
				CONCAT('') AS item_suppleir,
				unit_name,
				supplier_item_price,
				item_price,
				item_width,
				item_height,
				item_length,
				item_width*item_height*item_length/1000000 as cbm,
				item_radius,
				item_gross_weight,
				item_net_weight,
				packaging_type_name,
				IF(item_stackable, 'Yes', 'No') AS item_stackable,
				IF(item_visible, 'Yes', 'No') as item_visible,
				IF(item_visible_in_production_order, 'Yes', 'No') as item_visible_in_production_order,
				IF(item_visible_in_mps, 'Yes', 'No') as item_visible_in_mps, 
				IF(item_stock_property_of_swatch, 'Yes', 'No') AS item_stock_property_of_swatch,
				IF(item_is_dr_swatch_furniture, 'Yes', 'No') AS item_is_dr_swatch_furniture,
				currency_symbol
			FROM db_retailnet.items	
		";

	} else {

		// filter: countries
		if ($_REQUEST['category']) {
			
			$filters['category'] = "item_category = ".$_REQUEST['category'];
			$binds['item_categories'] = "INNER JOIN db_retailnet.item_categories ON items.item_category = item_categories.item_category_id";
			
			$itemCategory = new Item_Category();
			$itemCategory->read($_REQUEST['category']);
			$captions[] = "Item Category: $itemCategory->name";
		}

		$query = "
			SELECT DISTINCT
				item_id,
				item_category_name,
				item_code,
				item_name,
				item_width,
				item_height,
				item_length,
				item_width*item_height*item_length/1000000 as cbm,
				item_radius,
				item_gross_weight,
				item_net_weight
			FROM db_retailnet.items
		";
	}
	
	$result = $model->query($query)
	->bind($binds)
	->filter($filters)
	->order($order, $direction)
	->fetchAll();

	//die($model->sql);

	if ($result) {
		
		require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
		require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

		$format = array(
			'item_code' => array(
				'width' => 20,
				'caption' => $translate->item_code
			),
			'item_name' => array(
				'width' => 60,
				'caption' => $translate->item_name
			),
			'item_suppleir' => array(
				'width' => 30,
				'caption' => $translate->item_suppleir
			),
			'item_price' => array(
				'width' => 18,
				'caption' => 'Sales Price CHF'
			),
			'item_visible' => array(
				'width' => 10,
				'caption' => 'Visible'
			),
			'item_visible_in_production_order' => array(
				'width' => 10,
				'caption' => 'Planning'
			),
			'item_visible_in_mps' => array(
				'width' => 16,
				'caption' => 'Merchandising'
			),
			'unit_name' => array(
				'width' => 10,
				'caption' => $translate->unit_name
			),
			'supplier_item_price' => array(
				'width' => 15,
				'caption' => $translate->item_price
			),
			'item_stock_property_of_swatch' => array(
				'width' => 12,
				'caption' => $translate->prop_swatch
			),
			'item_is_dr_swatch_furniture' => array(
				'width' => 12,
				'caption' => $translate->dr_swatch
			),
			'item_width' => array(
				'width' => 10,
				'caption' => $translate->item_width
			),
			'item_height' => array(
				'width' => 10,
				'caption' => $translate->item_height
			),
			'item_length' => array(
				'width' => 10,
				'caption' => $translate->item_length
			),
			'cbm' => array(
				'width' => 10,
				'caption' => $translate->cbm
			),
			'item_radius' => array(
				'width' => 10,
				'caption' => $translate->item_radius
			),
			'item_gross_weight' => array(
				'width' => 15,
				'caption' => $translate->item_gross_weight
			),
			'item_net_weight' => array(
				'width' => 15,
				'caption' => $translate->item_net_weight
			),
			'packaging_type_name' => array(
				'width' => 15,
				'caption' => $translate->packaging_type_name
			),
			'item_stackable' => array(
				'width' => 12,
				'caption' => $translate->item_stackable
			),
			'item_category_name' => array(
				'width' => 30,
				'caption' => $translate->item_category_name
			),
			'item_subcategory_name' => array(
				'width' => 30,
				'caption' => $translate->item_subcategory_name
			),
			'supplying_groups' => array(
				'width' => 30,
				'caption' => 'Supplying Groups'
			)
		);

		$datagrid = _array::datagrid($result);

		if ($application=='catalog') { 
	
			// toolbox suppliers
			$bindSuppliers = $binds;
			$bindSuppliers['suppliers'] = "INNER JOIN db_retailnet.suppliers ON supplier_item = item_id";
			$bindSuppliers['addresses'] = "INNER JOIN db_retailnet.addresses ON address_id = supplier_address";
		
			$result = $model->query("
				SELECT DISTINCT address_id,
					address_company, item_id
				FROM db_retailnet.items 
			")
			->bind($bindSuppliers)
			->filter($filters)
			->order('address_company')
			->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$item = $row['item_id'];
					$itemSuppliers[$item] = $itemSuppliers[$item] ? $itemSuppliers[$item].", ".$row['address_company'] : $row['address_company'];
				}
			}

			foreach ($datagrid as $item => $row) {
			
				// item supplier
				if ($itemSuppliers[$item]) {
					$datagrid[$item]['item_suppleir'] = $itemSuppliers[$item];
				}

				$currency = $row["currency_symbol"];
				unset($datagrid[$item]["currency_symbol"]);

				$itemPrice = number_format($row["supplier_item_price"], 2, ".", "'");

				$datagrid[$item]['supplier_item_price'] = "$itemPrice $currency";
			}
		}



		$columns = array_keys(end($datagrid));
		$totalColumns = count($columns);
		$lastColumn = PHPExcel_Cell::stringFromColumnIndex($totalColumns-1);

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle(SHEET_NAME);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

		// styling
		$styles['border'] = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);

		$styles['header'] = array(
			'font' => array(
				'name'=>'Arial',
				'size'=> 11, 
				'bold'=>false
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'wrap'=> true
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb'=>'ECECEC')
			)
		);

		// sheet header
		$row = 1;
		$range = "A$row:$lastColumn$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', SHEET_NAME);
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setSize(18);

		$row++;
		$range = "A$row:$lastColumn$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		$row++;
		$range = "A$row:$lastColumn$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: ".date('d.m.Y  H:i:s'));

		// filter captions
		if ($captions) {
			foreach ($captions as $value) {
				$row++;
				$range = "A$row:$lastColumn$row";
				$objPHPExcel->getActiveSheet()->mergeCells($range);
				$objPHPExcel->getActiveSheet()->setCellValue("A$row", $value);
				$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
			}
		}


		$row++;
		$range = "A$row:$lastColumn$row";
		$objPHPExcel->getActiveSheet()->mergeCells($range);

		// column captions
		$row++;
		$col = 0;
		$startBorderedCell = "A$row";

		foreach ($columns as $columnName) {

			$colLetter = spreadsheet::index($col);

			// caption
			$caption = $format[$columnName]['caption'] ?: $translate->$columnName;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $caption);

			// dimension
			if ($format[$columnName]['width']) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($colLetter)->setWidth($format[$columnName]['width']);
			}

			// style
			$objPHPExcel->getActiveSheet()->getStyle("$colLetter$row")->applyFromArray($styles['header']);

			$col++;
		}

		// table header styling
		$range = "A$row:$lastColumn$row";
		$objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setBold(true);

		// datagrid
		$row++;

		foreach ($datagrid as $i => $array) {

			$col = 0;
			foreach($array as $key=>$value) {
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
				$col++;
			}
			$row++;
		}

		// set borders
		$row = $row-1;
		$stopBorderCell = "$lastColumn$row";
		$objPHPExcel->getActiveSheet()->getStyle("$startBorderedCell:$stopBorderCell")->applyFromArray($styles['border'], false);

		// export
		$objPHPExcel->getActiveSheet()->setTitle(SHEET_NAME);
		$objPHPExcel->setActiveSheetIndex(0);

		// redirect output to client browser
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.SHEET_FILE_NAME.'.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		
		exit;
	}
	else {
		message::empty_result();
		$archived = ($archived) ? '/archived' : null;
		url::redirect("/$application/$controller$archived/$action/$id");
	}
	