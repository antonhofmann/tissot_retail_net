<?php
/*
    Excel Copmanies List

    Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2012-02-01
    Version:        1.1
    Updated: 		2012-10-19, Roger Bucher

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*/

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

$model = new Model(Connector::DB_CORE);

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

// request execution time
ini_set('max_execution_time', 120);

// request
$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

// request filters
$fCountry = $_REQUEST['countries'];
$fSerach =  $_REQUEST['search'] && $_REQUEST['search']<>$translate->search ? $_REQUEST['search'] : null;
$fAddressType = $_REQUEST['addresstypes'];

// sheet definition
$sheetname = ($archived) ? "Inactive Companies" :  "Company List";

// sql order
$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "country_name, address_type_name, address_company";
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";


// company permissiens
$permission_view = user::permission(Company::PERMISSION_VIEW);
$permission_view_limited = user::permission(Company::PERMISSION_VIEW_LIMITED);
$permission_edit = user::permission(Company::PERMISSION_EDIT);
$permission_edi_limited = user::permission(Company::PERMISSION_EDIT_LIMITED);
$permission_menager = user::permission(Company::PERMISSION_MANAGER);
$permission_catalog = user::permission(Company::PERMISSION_EDIT_CATALOG);

// has limited permission
$has_limited_permission = (!$permission_view &&!$permission_edit && !$permission_menager && !$permission_catalog) ? true : false;

// table fields
$table = new DB_Table();
$table->read_from_name('addresses');
$table_fields = ($table->export_fields) ? unserialize($table->export_fields) : array();

// user selected fields
if ($_REQUEST['columns'] && $_REQUEST['modal']) {
	
	$fields = array_keys($_REQUEST['columns']);
	$user_fields = serialize($fields);
	
	// read export list instance
	user::exportList()->read_from_table($table->id);
	
	if (user::exportList()->id) {
		user::exportList()->update(array(
			'user_export_list_fields' => $user_fields
		));
	} else {
		user::exportList()->create($table->id, $user_fields);
	}
} else {
	$fields = $table_fields;
}

// for limited view:
// get current company childs
// get country accesses
if ($has_limited_permission) {

	// filter access countries
	$countryAccess = User::getCountryAccess();	
	$filterCountryAccess = $countryAccess ? " OR address_country IN ($countryAccess)" : null;

	// regional access companies
	$regionalAccessCompanies = User::getRegionalAccessCompanies();
	$filterRegionalAccess = $regionalAccessCompanies ? " OR address_parent IN (".join(',', $regionalAccessCompanies).") " : null;

	$filters['limited'] = "(
		(address_id = $user->address OR address_parent = $user->address) 
		$filterCountryAccess 
		$filterRegionalAccess
	)";
}

// filter: country
if($fCountry) {
	$filters['countries'] = "address_country = $fCountry";
	$country = new Country();
	$country->read($fCountry);
	$captions[] = "Country: $country->name";
}

// filter: address types
if ($fAddressType) {

	switch ($_REQUEST['addresstypes']) {
		case 1:
			$filters['addresstypes'] = "address_client_type = 1";
			$captions[] = "Address Type: $translate->agents";
		break;
		case 2:
			$filters['addresstypes'] = "address_client_type = 2";
			$captions[] = "Address Type: $translate->subsidiaries";
		break;
		case 3:
			$filters['addresstypes'] = "address_client_type = 3";
			$captions[] = "Address Type: $translate->affiliates";
		break;
		case 4:
			$filters['addresstypes'] = "address_is_independent_retailer = 1";
			$captions[] = "Address Type: $translate->independent_retailers";
		break;
		case 5:
			$filters['addresstypes'] = "address_canbefranchisee = 1";
			$captions[] = "Address Type: $translate->franchisees";
		break;
	}
}

// filter: search
if ($fSerach) {

	$filters['search'] = "(
		address_company LIKE '%$fSerach%'
		OR address_street LIKE '%$fSerach%'
		OR country_name LIKE '%$fSerach%'
		OR province_canton LIKE '%$fSerach%'
		OR address_place LIKE '%$fSerach%'
		OR address_type_name LIKE '%$fSerach%'
		OR address_zip LIKE '%$fSerach%'
		OR address_mps_customernumber LIKE '%$fSerach%'
		OR address_mps_shipto LIKE '%$fSerach%'
	)";
	
	$captions[] = "Search: $fSerach";
}

// default filter
$active = ($archived) ? 0 : 1;
$filters['active'] = "address_active = $active";
$filter['default'] = "(address_type = 1 OR address_type = 7)";

// show in pos index
if ($application == 'mps') {
	$filters['showinpos'] = "address_showinposindex = 1";
}

//address_involved_in_planning
if ($_REQUEST['address_involved_in_planning']) {
	$filters['address_involved_in_planning'] = "address_involved_in_planning = 1";
}

$result = $model->query("
	SELECT DISTINCT
		address_id, 
		address_number, 
		address_sapnr, 
		address_mps_customernumber, 
		address_mps_shipto, 
		address_company, 
		address_company2, 
		address_address, 
		address_address2, 
		address_zip,  
		place_name AS address_place_id, 
		country_name AS address_country, 
		address_phone, 
		address_mobile_phone, 
		address_email, 
		address_website, 
		currency_symbol AS address_currency,  
		address_type_name AS address_type, 
		client_type_code AS address_client_type, 
		address_parent, 
		address_invoice_recipient, 
		address_contact, 
		address_active, 
		address_contact_name, 
		address_contact_email, 
		address_legnr, 
		address_do_data_export_from_mps_to_sap,
		IF (address_do_data_export_from_mps_to_sap, 'Yes', '') AS address_do_data_export_from_mps_to_sap, 
		IF (address_canbefranchisee, 'Yes', '') AS address_canbefranchisee, 
		IF (address_canbefranchisee_worldwide, 'Yes', '') AS address_canbefranchisee_worldwide, 
		IF (address_canbefranchisor, 'Yes', '') AS address_canbefranchisor, 
		IF (address_canbejointventure, 'Yes', '') AS address_canbejointventure, 
		IF (address_canbecooperation, 'Yes', '') AS address_canbecooperation, 
		IF (address_showinposindex, 'Yes', '') AS address_showinposindex, 
		IF (address_swatch_retailer, 'Yes', '') AS address_swatch_retailer, 
		IF (address_checked, 'Yes', '') AS address_checked,  
		IF (address_involved_in_planning, 'Yes', '') AS address_involved_in_planning, 
		IF (address_involved_in_planning_ff, 'Yes', '') AS address_involved_in_planning_ff, 
		IF (address_is_independent_retailer, 'Yes', '') AS address_is_independent_retailer, 
		IF (address_can_own_independent_retailers, 'Yes', '') AS address_can_own_independent_retailers, 
		IF (address_is_standard_invoice_address, 'Yes', '') AS address_is_standard_invoice_address, 
		IF (address_involved_in_red, 'Yes', '') AS address_involved_in_red, 
		IF (address_checkdate = '0000-00-00', '', DATE_FORMAT(address_checkdate,'%d.%m.%Y')) AS address_checkdate,
		IF (addresses.date_created='0000-00-00', '', DATE_FORMAT(addresses.date_created,'%d.%m.%Y')) AS date_created,
		IF(addresses.date_modified = '0000-00-00', '', DATE_FORMAT(addresses.date_modified,'%d.%m.%Y')) AS date_modified,
		addresses.user_created,
		addresses.user_modified,
		address_additional_customerno2,
	address_additional_customerno3,
	address_additional_customerno4,
	address_additional_customerno5,
	address_additional_shiptono2,
	address_additional_shiptono3,
	address_additional_shiptono4,
	address_additional_shiptono5,
	address_additional_lps_customerno1,
	address_additional_lps_customerno2,
	address_additional_lps_customerno3,
	address_additional_lps_customerno4,
	address_additional_lps_customerno5,
	address_additional_dummie_quantity
	FROM addresses
	LEFT JOIN address_additionals on address_additional_address = address_id 
	LEFT JOIN db_retailnet.address_types ON address_type_id = address_type
	INNER JOIN db_retailnet.countries ON address_country = country_id
	INNER JOIN db_retailnet.places ON place_id = address_place_id
	LEFT JOIN currencies ON currency_id = address_currency		
	LEFT JOIN client_types ON client_type_id = address_client_type
")
->filter($filters)
->order($order, $direction)
->fetchAll();
//die($model->sql);

if ($result) {

	$datagrid = _array::datagrid($result);
	$totalColumns = count($fields);
	$lastColumn = PHPExcel_Cell::stringFromColumnIndex($totalColumns-1);

	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle($sheetname);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

	// styling
	$border = array(
		'borders' => array( 
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);

	$headerString = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 10, 
			'bold'=> true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'EFEFEF')
		),
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);

	$headerNnumber = array(
		'font' => array(
			'name'=>'Arial',
			'size'=> 10,
			'bold' => true
		),
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'=> true
		),
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'color' => array('rgb'=>'EFEFEF')
		),
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);
	
	// column 
	$columns = array(
		address_id => array(
			'label' => 'ID',
			'width' => 5
		),
		address_number => array(
			'label' => $translate->address_number,
			'width' => 20
		),
		address_sapnr => array(
			'label' => $translate->address_sapnr,
			'width' => 20
		),
		address_mps_customernumber => array(
			'label' => $translate->address_mps_customernumber,
			'width' => 20
		),
		address_mps_shipto => array(
			'label' => $translate->address_mps_shipto,
			'width' => 20
		),
		address_legal_entity_name => array(
			'label' => $translate->address_legal_entity_name,
			'width' => 20
		),
		address_company => array(
			'label' => $translate->address_company,
			'width' => 40
		),
		address_company2 => array(
			'label' => $translate->address_company2,
			'width' => 20
		),
		address_address => array(
			'label' => $translate->address_address,
			'width' => 30
		),
		address_address2 => array(
			'label' => $translate->address_address2,
			'width' => 20
		),
		address_zip => array(
			'label' => $translate->address_zip,
			'width' => 10
		),
		address_place_id => array(
			'label' => $translate->address_place_id,
			'width' => 30
		),
		address_country => array(
			'label' => $translate->address_country,
			'width' => 30
		),
		address_phone => array(
			'label' => $translate->address_phone,
			'width' => 20
		),
		address_mobile_phone => array(
			'label' => $translate->address_mobile_phone,
			'width' => 20
		),
		address_email => array(
			'label' => $translate->address_email,
			'width' => 30
		),
		address_website => array(
			'label' => $translate->address_website,
			'width' => 30
		),
		address_currency => array(
			'label' => $translate->address_currency,
			'width' => 10
		),
		address_type => array(
			'label' => $translate->address_type,
			'width' => 30
		),
		address_client_type => array(
			'label' => $translate->address_client_type,
			'width' => 30
		),
		address_parent => array(
			'label' => $translate->address_parent,
			'width' => 50
		),
		address_invoice_recipient => array(
			'label' => $translate->address_invoice_recipient,
			'width' => 20
		),
		address_contact => array(
			'label' => $translate->address_active,
			'width' => 30
		),
		address_active => array(
			'label' => 'ACT',
			'width' => 5,
			'description' => $translate->address_active
		),
		address_contact_name => array(
			'label' => $translate->address_contact_name,
			'width' => 30
		),
		address_contact_email => array(
			'label' => $translate->address_contact_email,
			'width' => 30
		),
		address_legnr => array(
			'label' => $translate->address_legnr,
			'width' => 20
		),
		address_canbefranchisee => array(
			'label' => 'FRE',
			'width' => 5,
			'description' => $translate->address_canbefranchisee
		),
		address_canbefranchisee_worldwide => array(
			'label' => 'FRW',
			'width' => 5,
			'description' => $translate->address_canbefranchisee_worldwide
		),
		address_canbefranchisor => array(
			'label' => 'FRO',
			'width' => 5,
			'description' => $translate->address_canbefranchisor
		),
		address_canbejointventure => array(
			'label' => 'JVE',
			'width' => 5,
			'description' => $translate->address_canbejointventure
		),
		address_canbecooperation => array(
			'label' => 'COP',
			'width' => 5,
			'description' => $translate->address_canbecooperation
		),
		address_showinposindex => array(
			'label' => 'POS',
			'width' => 5,
			'description' => $translate->address_showinposindex
		),
		address_swatch_retailer => array(
			'label' => 'SR',
			'width' => 5,
			'description' => $translate->address_swatch_retailer
		),
		address_checkdate => array(
			'label' => 'CHK',
			'width' => 5,
			'description' => $translate->address_checkdate
		),
		address_involved_in_planning => array(
			'label' => 'OSM',
			'width' => 5,
			'description' => $translate->address_involved_in_planning
		),
		address_involved_in_planning_ff => array(
			'label' => 'OFM',
			'width' => 5,
			'description' => $translate->address_involved_in_planning_ff
		),
		address_is_independent_retailer => array(
			'label' => 'IR',
			'width' => 5,
			'description' => $translate->address_is_independent_retailer
		),
		address_can_own_independent_retailers => array(
			'label' => 'OIR',
			'width' => 5,
			'description' => $translate->address_can_own_independent_retailers
		),
		address_is_standard_invoice_address => array(
			'label' => 'SIA',
			'width' => 5,
			'description' => $translate->address_is_standard_invoice_address
		),
		address_involved_in_red => array(
			'label' => 'INPL',
			'width' => 5,
			'description' => $translate->address_involved_in_red
		),
		address_do_data_export_from_mps_to_sap => array(
			'label' => 'SAP',
			'width' => 5,
			'description' => $translate->address_do_data_export_from_mps_to_sap
		),
		user_created => array(
			'label' => $translate->user_created,
			'width' => 10
		),
		date_created => array(
			'label' => $translate->date_created,
			'width' => 10
		),
		user_modified => array(
			'label' => $translate->user_modified,
			'width' => 10
		),
		date_modified => array(
			'label' => $translate->date_modified,
			'width' => 10
		),
		address_additional_dummie_quantity => array(
			'label' => $translate->address_additional_dummie_quantity,
			'width' => 10
		)
	);

	// sheet header
	$row = 1;
	$header = ($_REQUEST['pagetitle']) ? $_REQUEST['pagetitle'] : $sheetname;
	$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
	$objPHPExcel->getActiveSheet()->setCellValue('A1', $header);
	$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A$row:$lastColumn$row")->getFont()->setSize(18);

	// separator
	$row++;
	$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");

	// current date
	$row++;
	$range = "A$row:$lastColumn$row";
	$objPHPExcel->getActiveSheet()->mergeCells($range);
	$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: ".date('d.m.Y  H:i:s'));

	// filter captions
	if ($captions) {
		foreach ($captions as $value) {
			$row++;
			$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
			$objPHPExcel->getActiveSheet()->setCellValue("A$row", $value);
			$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
		}
	}

	// separator
	$row++;
	$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");

	$row++;
	$col = 0;
	$has_description = false;
	
	// column captions
	foreach ($fields as $field) {
		$index = spreadsheet::index($col);
		$width = ($columns[$field]['width']) ? $columns[$field]['width'] : 20;
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $columns[$field]['label']);
		$objPHPExcel->getActiveSheet()->getColumnDimension($index)->setWidth($width);
		if (!$has_description && $columns[$field]['description']) $has_description = true;
		$col++;
	}

	// row style
	$objPHPExcel->getActiveSheet()->getStyle("A$row:$lastColumn$row")->applyFromArray($headerString, false);

	$row++;
	$first_datagrid_row = $row;
	
	// datagrid
	foreach ($datagrid as $i => $data) {

		$col = 0;
		
		foreach($fields as $field ) {
			if ($data[$field]) {
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $data[$field]);
			}
			$col++;
		}

		$row++;
	}
	
	// set borders
	$prev_row = $row-1;
	$objPHPExcel->getActiveSheet()->getStyle("A$first_datagrid_row:$lastColumn$prev_row")->applyFromArray($border, false);
	
	if ($has_description) {
		
		$row++;
		$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Shortcut Descriptions');
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
		
		$row++;
		
		// descriptions
		foreach ($fields as $field) {
			if ($columns[$field]['description']) {
				$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");
				$objPHPExcel->getActiveSheet()->setCellValue("A$row", $columns[$field]['label'].': '.$columns[$field]['description']);
				$row++;
			}
		}
	}

	// export
	$objPHPExcel->getActiveSheet()->setTitle($sheetname);
	$objPHPExcel->setActiveSheetIndex(0);

	// redirect output to client browser
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="companies_'.date('Y_m_d').'.xlsx"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	
	exit;
}
else {
	message::empty_result();
	$archived = ($archived) ? '/archived' : null;
	url::redirect("/$application/$controller$archived/$action/$id");
}