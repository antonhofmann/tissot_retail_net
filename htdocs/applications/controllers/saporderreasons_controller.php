<?php

class sapOrderReasons_Controller {

	public function __construct() {
				
		$this->user = User::instance();
		$this->request = request::instance();
		$this->translate = Translate::instance();
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}

	public function index() {

		$link = $this->request->query('add');
		$this->request->field('add', $link);

		$link = $this->request->query('data');
		$this->request->field('form', $link);

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'list.php');
		$tpl->data('url', '/applications/modules/sap/orderreason/list.php');
		$tpl->data('class', 'list-500');
		$this->view->setTemplate('orderreason', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js'
		));
	}

	public function add() {

		$data = array();
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['action'] = $this->request->action;
		$data['redirect'] = $this->request->query('data');

		$buttons = array();
		$buttons['save'] = true;
		$buttons['back'] = $this->request->query();

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'sap/orderreason/form.php');
		$tpl->data('data', $data);
		$tpl->data('buttons', $buttons);
		$this->view->setTemplate('orderreason', $tpl);

		// script integration
		Compiler::attach(array(
			'/public/scripts/validationEngine/css/validationEngine.jquery.css',
			'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
			'/public/scripts/validationEngine/js/jquery.validationEngine.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/js/form.validator.js'
		));
	}

	public function data() {

		$id = url::param();

		// modul: collection
		$type = new Modul();
		$type->setTable('sap_hq_order_reasons');

		$data = $type->read($id);
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['action'] = $this->request->action;

		// check access
		if (!$type->id) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$buttons = array();
		$buttons['save'] = true;
		$buttons['back'] = $this->request->query();
		$buttons['delete'] = $this->request->link('/applications/modules/sap/orderreason/delete.php', array('id'=>$id));

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'sap/orderreason/form.php');
		$tpl->data('data', $data);
		$tpl->data('buttons', $buttons);
		$this->view->setTemplate('collection', $tpl);

		// script integration
		Compiler::attach(array(
			'/public/scripts/validationEngine/css/validationEngine.jquery.css',
			'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
			'/public/scripts/validationEngine/js/jquery.validationEngine.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/js/form.validator.js'
		));
	}
}