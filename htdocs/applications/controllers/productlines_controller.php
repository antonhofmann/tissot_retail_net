<?php 

	class ProductLines_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() { 
			
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
			Compiler::attach(DIR_CSS."jquery.ui.css");

			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			if ($permission_edit) {
				$this->request->field('add',$this->request->query('add'));
			}
			
			if ($permission_edit || $permission_view) {
				$this->request->field('data',$this->request->query('data'));
			}
			
			// template: role list
			$this->view->productLines('pagecontent')
			->product('line.list')
			->data('buttons', $buttons);
		}

		public function add() {

			$permission_edit = user::permission('can_edit_catalog');
		
			if($this->request->archived || !$permission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			$data['product_line_active'] = 1;

			$model = new Model(Connector::DB_CORE);

			// dataloader: regions
			$result = $model->query("
				SELECT region_id, region_name
				FROM regions 
				ORDER BY region_name
			")->fetchAll();
			
			if ($result) {
				foreach ($result as $row) {
					$region = $row['region_id'];
					$dataloader['regions'][$region] = $row['region_name'];
				}
			}

			$result = $model->query("
				SELECT DISTINCT
					supplying_group_id,
					supplying_group_name,
					CASE WHEN product_line_supplying_group_group_id IS NOT NULL
						THEN 1
						ELSE 0
					END AS is_selected
				FROM supplying_groups
				LEFT JOIN product_line_supplying_groups ON product_line_supplying_group_group_id = supplying_group_id
				WHERE supplying_group_active = 1 OR product_line_supplying_group_line_id = '$id'
				ORDER BY supplying_group_name
			")->fetchAll();

			$dataloader['product_line_supplying_group'] = _array::extract($result);
				
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
		
			// template: application form
			$this->view->productLine('pagecontent')
			->product('line.form')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons);
		}
		
		public function data() {

			$id = url::param();

			$productLine = new Product_Line();
			$productLine->read($id);

			if (!$productLine->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// dataloader
			$dataloader = array();

			$data = $productLine->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;

			$model = new Model(Connector::DB_CORE);

			

			// data: regions
			$result = $model->query("
				SELECT productline_region_region
				FROM productline_regions 
				WHERE productline_region_productline = $id
			")->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$regions[] = $row['productline_region_region'];
				}
				$data['regions'] = serialize($regions);
			}

			


			// dataloader: regions
			if(count($regions) > 0)
			{
				$result = $model->query("
					SELECT region_id, region_name
					FROM regions
					WHERE region_active = 1 or region_id in (" . implode(', ', $regions) . ")
					ORDER BY region_name
				")->fetchAll();
			}
			else
			{
				$result = $model->query("
					SELECT region_id, region_name
					FROM regions
					ORDER BY region_name
				")->fetchAll();
			}
			
			if ($result) {
				foreach ($result as $row) {
					$region = $row['region_id'];
					$dataloader['regions'][$region] = $row['region_name'];
				}
			}

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			if ($permission_edit) {

				$buttons['save'] = true;

				$integrity = new Integrity();
				$integrity->set($id, 'product_lines', 'system');

				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/product.line.delete.php', array('id'=>$id));
				}
			}
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}

			$result = $model->query("
				SELECT DISTINCT
					supplying_group_id,
					supplying_group_name,
					CASE WHEN product_line_supplying_group_group_id IS NOT NULL
						THEN 1
						ELSE 0
					END AS is_selected
				FROM supplying_groups
				LEFT JOIN product_line_supplying_groups ON product_line_supplying_group_group_id = supplying_group_id
				WHERE supplying_group_active = 1 OR product_line_supplying_group_line_id = '$id'
				ORDER BY supplying_group_name
			")->fetchAll();

			$dataloader['product_line_supplying_group'] = _array::extract($result);

			$result = $model->query("
				SELECT GROUP_CONCAT(product_line_supplying_group_group_id) AS supplyings
				FROM product_line_supplying_groups
				WHERE product_line_supplying_group_line_id = $id
			")->fetch();

			$groups = $result['supplyings'] ? serialize(explode(',', $result['supplyings'])) : null;
			$data['product_line_supplying_group'] =  $groups;

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $productLine->name);
			$this->view->companytabs('pagecontent')->navigation('tab');
		
			// template: form
			$this->view->productLine('pagecontent')
			->product('line.form')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons);
		}


		public function items() {

			$id = url::param();

			$productLine = new Product_Line();
			$productLine->read($id);

			if (!$productLine->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			$db = Connector::get();

			$sth = $db->prepare("
				SELECT SQL_CALC_FOUND_ROWS DISTINCT
					item_id,
					item_code,
					item_name,
					item_price,
					supplying_group_id,
					supplying_group_name,
					item_category_id,
					item_category_name,
					item_subcategory_id,
					item_subcategory_name
				FROM items	
				INNER JOIN item_supplying_groups ON item_supplying_group_item_id = item_id
				INNER JOIN supplying_groups ON supplying_group_id = item_supplying_group_supplying_group_id
				INNER JOIN product_line_supplying_groups ON product_line_supplying_group_group_id = item_supplying_group_supplying_group_id
				INNER JOIN product_lines ON product_line_id = product_line_supplying_group_line_id
				INNER JOIN item_categories ON item_category_id = item_category
				LEFT JOIN item_subcategories ON item_subcategory_id = item_category
				WHERE product_line_id = ?
				ORDER BY supplying_group_name, item_category_name, item_subcategory_name, item_code
			");

			$sth->execute(array($id));
			$result = $sth->fetchAll();

			$datagrid = array();

			if ($result) {

				foreach ($result as $row) {
					
					$item = $row['item_id'];
					$key = $row['supplying_group_id'].'.'.$row['item_category_id'].'.'.$row['item_subcategory_id'];

					$datagrid[$key]['caption'] = join(', ', array_filter(array(
						$row['supplying_group_name'],
						$row['item_category_name'],
						$row['item_subcategory_name']
					)));

					$datagrid[$key]['data'][$item] = array(
						'item_code' => $row['item_code'],
						'item_name' => $row['item_name'],
						'item_price' => $row['item_price']
					);
				}
			}
			//echo "<pre>"; print_r($datagrid); echo "</pre>";
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $productLine->name);
			$this->view->companytabs('pagecontent')->navigation('tab');
		
			// template: form
			$this->view->productLine('pagecontent')
			->product('line.items')
			->data('datagrid', $datagrid)
			->data('buttons', $buttons);
		}

		public function designObjectiveGroups() {

			$id = url::param();

			$productLine = new Product_Line();
			$productLine->read($id);

			if (!$productLine->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$this->request->field('id', $id);
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");

			$data = $productLine->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;

			// buttons
			$buttons = array();
			$buttons['save'] = true;

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $productLine->name);
			$this->view->companytabs('pagecontent')->navigation('tab');
		
			// template: form
			$this->view->productLine('pagecontent')
			->product('line.desogn.objective.groups')
			->data('data', $data)
			->data('buttons', $buttons);
		}

		public function files() {

			$id = url::param();
			$file = url::param(1);

			$productLine = new Product_Line();
			$productLine->read($id);

			if (!$productLine->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$model = new Model(Connector::DB_CORE);

			// permissions
			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $productLine->name);
			$this->view->companytabs('pagecontent')->navigation('tab');

			if ($file) {
				
				Compiler::attach(DIR_SCRIPTS."ajaxuploader/ajaxupload.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				Compiler::attach(DIR_JS."form.validator.file.js");

				$file = (is_numeric($file)) ? $file : null;

				// button back
				$buttons['back'] = $this->request->query("files/$id");

				if ($file) {
					$productLineFile = new Product_Line_File();
					$data = $productLineFile->read($file);
					
				} else {
					$data['redirect'] = $this->request->query($this->request->action.'/'.$id);
				}

				if( !$this->request->archived && $permission_edit ) {

					$buttons['save'] = true;

					if ($file) {

						$integrity = new Integrity();
						$integrity->set($file, 'product_line_files', 'system');

						if ($integrity->check()) {
							$buttons['delete'] = $this->request->link('/applications/helpers/product.line.file.delete.php', array('id' => $file));
						}
					}

				}
				elseif($data) {
					foreach ($data as $key => $value) {
						$disabled[$key] = true;
					}
				}

				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;
				$data['action'] = $this->request->action;
				$data['product_line_file_product_line'] = $id;

				$dataloader = array();

				// file purpose
				$result = $model->query("
					SELECT 
						file_purpose_id, 
						file_purpose_name 
					FROM file_purposes 
					ORDER BY file_purpose_name
				")->fetchAll();

				$dataloader['product_line_file_purpose'] = _array::extract($result);

				// template: pos file form
				$this->view->posform('pagecontent')
				->product('line.file.form')
				->data('data', $data)
				->data('dataloader', $dataloader)
				->data('disabled', $disabled)
				->data('hidden', $hidden)
				->data('buttons', $buttons)
				->data('pos', $id)
				->data('id', $file);

			} else {
							
				$result = $model->query("
					SELECT 
						product_line_file_id AS id, 
						product_line_file_title AS title, 
						product_line_file_description AS description, 
						product_line_file_path AS path, 
						DATE_FORMAT(product_line_files.date_created, '%d.%m.%Y') AS date,
						file_type_name AS file_type,
						file_type_extension AS extension, 
						file_purpose_name AS purpose
					FROM product_line_files 
					INNER JOIN file_types ON file_type_id = product_line_file_type
					INNER JOIN file_purposes ON product_line_file_purpose = file_purpose_id
					WHERE product_line_file_product_line = $id
					ORDER BY product_line_file_title
				")->fetchAll();

				if ($result) {

					$files = array();

					foreach ($result as $row) {
						$i = $row['id'];
						$extension = file::extension($row['path']);
						$files[$i]['title'] = "{$row[title]}<span>{$row[description]}</span>";
						$files[$i]['date']= $row[date];
						$files[$i]['path'] = $row['path'];
						$files[$i]['file_purpose_name'] = $row['purpose'];
						$files[$i]['fileicon']  = "<span class='file-extension $extension modal' tag='{$row[path]}'></span>";
					}
				}
				
				// buttons
				$buttons = array();
				$buttons['back'] = $this->request->query();

				if ($permission_edit) {
					$buttons['add'] = $this->request->query("files/$id/add");
				}
			
				// template: form
				$this->view->productLine('pagecontent')
				->product('line.files')
				->data('files', $files)
				->data('id', $id)
				->data('buttons', $buttons);
			}
		}
	}