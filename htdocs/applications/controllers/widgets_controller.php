<?php

	class Widgets_Controller {

		public function __construct() {

			$this->user = User::instance();
			$this->request = request::instance();
			$this->translate = Translate::instance();

			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}

		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			$cen_edit = user::permission(Staff::PERMISSION_EDIT);
			$cen_edit_limited = user::permission(Staff::PERMISSION_EDIT_LIMITED);

			if (!$this->request->archived) {
				$this->request->field('add', $this->request->query('add'));
			}

			$this->request->field('data', $this->request->query('data'));

			$this->view->widgets('pagecontent')->widget('list');
		}

		public function add() {
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
			
			// form loader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] =  $this->request->query('data');

			$this->view->widgets('pagecontent')->widget('data')->data('data', $data);
		}

		public function data() {

			$id = url::param();

			$widget = new Widget();
			$widget->read($id);
			
			// check access to staff
			if (!$widget->id) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");

			// form dataloader
			$data = $widget->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;

			// button back
			$buttons['back'] = '/'.$this->request->application.'/'.$this->request->controller;

			if( !$this->request->archived ) {
				
				$buttons['save'] = true;

				$integrity = new Integrity();
				$integrity->set($id, 'ui_widgets', 'system');

				if ($integrity->check()) {
					$buttons['delete'] = url::build('/applications/helpers/widget.delete.php', array('id'=>$id));
				}
			}
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}
			
			$model = new Model(Connector::DB_CORE);
			
			$result = $model->query("
				SELECT 
					ui_widget_category_id,
					ui_widget_category_name
				FROM ui_widget_categories
				ORDER BY ui_widget_category_name		
			")->fetchAll();
			
			$dataloader['ui_widget_category_id'] = _array::extract($result);
			
			$result = $model->query("
				SELECT 
					ui_widget_theme_id,
					ui_widget_theme_name
				FROM ui_widget_themes
				ORDER BY ui_widget_theme_name		
			")->fetchAll();
			
			$dataloader['ui_widget_theme_id'] = _array::extract($result);

			// template: staff form
			$this->view->widgets('pagecontent')->widget('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}