<?php 

	class Planningtypes_Controller {
		
		public function __construct() {
			
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			
			// request
			$requestFields = array();
			$requestFields['form'] = $this->request->query('data');
			
			// permission: can edit material planning typers
			$can_edit = user::permission(Material_Planning_Type::PERMISSION_EDIT);
			
			if (!$this->request->archived && $can_edit) {
				$requestFields['add'] = $this->request->query('add');
			}

			// template: material planning types
			$this->view->plannings('pagecontent')
			->material('planning.type.list')
			->data('requestFields', $requestFields);
		}
		
		public function add() {
			
			// permissions
			$prmission_edit = user::permission(Material_Planning_Type::PERMISSION_EDIT);
		
			if($this->request->archived || !$prmission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
				
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
				
			// template: material planning type
			$this->view->materialPlanningType('pagecontent')
			->material('planning.type.data')
			->data('data', $data)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons);
		}
		
		public function data() {
				
			$id = url::param();
			
			// permissions
			$prmission_edit = user::permission(Material_Planning_Type::PERMISSION_EDIT);
			
			$materialPlanningType = new Material_Planning_Type();
			$materialPlanningType->read($id);
			
			// check access to material category
			if (!$materialPlanningType->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			} 
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");

			// form datalaoder
			$data = $materialPlanningType->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
				
			if( !$this->request->archived && $prmission_edit) {
				
				$buttons['save'] = true;
				
				$integrity = new Integrity();
				$integrity->set($id, 'mps_material_planning_types');
				
				if ($integrity->check()) {
					$buttons['delete'] = url::build('/applications/helpers/material.planning.type.delete.php', array(
						'application' => $this->request->application,
						'controller' => $this->request->controller,
						'action' => $this->request->action,
						'id' => $id
					));
				}
			}
			// disable all fields
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}
			
			// material planning type
			$this->view->materialPlanningTypeForm('pagecontent')
			->material('planning.type.data')
			->data('data', $data)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}