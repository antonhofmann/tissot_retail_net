<?php

class Settings_Controller {

	public function __construct() {
		Settings::init()->theme = 'swatch';
		$this->request = request::instance();
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
	}

	public function index() {

		switch ($this->request->application) {
			
			case 'news':

				$this->view->pageclass = 'theme-news';
				$this->view->userboxMenu('navigations')->navigation('userbox');
				$this->view->categories('navigations')->navigation('categories');
				$this->view->appTabs('navigations')->navigation('appnav');

				$model = new Model($this->request->application);

				$sth = $model->db->prepare("
					SELECT DISTINCT
						news_article_setting_id AS id,
						setting_property_name AS name,
						news_article_setting_field AS field,
						setting_property_title AS title,
						news_article_setting_value AS value
					FROM news_article_settings
					INNER JOIN db_retailnet.setting_properties ON setting_property_id = news_article_setting_property_id
				");

				$sth->execute();
				$articles = $sth->fetchAll();

				$sth = $model->db->prepare("
					SELECT DISTINCT
						newsletter_setting_id AS id,
						setting_property_name AS name,
						newsletter_setting_field AS field,
						setting_property_title AS title,
						newsletter_setting_value AS value
					FROM newsletter_settings
					INNER JOIN db_retailnet.setting_properties ON setting_property_id = newsletter_setting_property_id
				");

				$sth->execute();
				$newsletters = $sth->fetchAll();


				$sth = $model->db->prepare("
					SELECT 
						news_quicklink_id AS id,
						news_quicklink_title AS title,
						news_quicklink_link As link
					FROM news_quicklinks
					ORDER BY news_quicklink_order
				");

				$sth->execute();
				$quicklinks = $sth->fetchAll();


				// news picture
				$pictures = glob($_SERVER['DOCUMENT_ROOT']."/data/news/images/*.{jpg,png,gif}", GLOB_BRACE);

				// view template
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'news/settings/settings.php');
				$tpl->data('articles', $articles);
				$tpl->data('newsletters', $newsletters);
				$tpl->data('quicklinks', $quicklinks);
				$tpl->data('pictures', $pictures);
				$this->view->setTemplate('settings', $tpl);

				Compiler::attach(array(
					'/public/themes/swatch/css/news.css',
					'/public/scripts/form/maxlength.min.js',
					'/public/js/news.settings.js',
					'/public/scripts/bsdialog/bsdialog.min.css',
					'/public/scripts/bsdialog/bsdialog.min.js'
				));
					
			break;
		}
	}

	public function article() {

		switch ($this->request->application) {
			
			case 'news':

				$id = url::param();
				$id = $id=='add' ? null : $id;

				$setting = new Modul($this->request->application);
				$setting->setTable('news_article_settings');

				if ($id) {

					$setting->read($id);

					if (!$setting->id) {
						Message::access_denied();
						url::redirect($this->request->query());
					}
				}

				$this->view->pageclass = 'theme-news';
				$this->view->userboxMenu('navigations')->navigation('userbox');
				$this->view->categories('navigations')->navigation('categories');
				$this->view->appTabs('navigations')->navigation('appnav');

				$buttons = array();
				$buttons['save'] = "/applications/modules/news/settings/article.submit.php";
				$buttons['delete'] = $id ? "/applications/modules/news/settings/article.delete.php?id=$id" : false;

				$model = new Model($this->request->application);

				$sth = $model->db->prepare("
					SELECT DISTINCT
						setting_property_id AS value,
						setting_property_title AS name,
						IF (setting_property_id = ?, 'selected=selected', '') AS selected
					FROM db_retailnet.setting_properties
					ORDER BY setting_property_name
				");

				$sth->execute(array($setting->data['news_article_setting_property_id']));
				$settings = $sth->fetchAll();

				// view template
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'news/settings/article.php');
				$tpl->data('data', $setting->data);
				$tpl->data('buttons', $buttons);
				$tpl->data('settings', $settings);
				$this->view->setTemplate('setting', $tpl);

				Compiler::attach(array(
					'/public/themes/swatch/css/news.css',
					'/public/scripts/formvalidation/css/formValidation.css',
					'/public/scripts/formvalidation/js/formValidation.js',
					'/public/scripts/formvalidation/js/framework/bootstrap.min.js',
					'/public/scripts/bsdialog/bsdialog.min.css',
					'/public/scripts/bsdialog/bsdialog.min.js',
					'/public/js/news.setting.js'
				));
					
			break;
		}
	}

	public function newsletter() {

		switch ($this->request->application) {
			
			case 'news':

				$id = url::param();
				$id = $id=='add' ? null : $id;

				$setting = new Modul($this->request->application);
				$setting->setTable('newsletter_settings');

				if ($id) {

					$setting->read($id);

					if (!$setting->id) {
						Message::access_denied();
						url::redirect($this->request->query());
					}
				}

				$this->view->pageclass = 'theme-news';
				$this->view->userboxMenu('navigations')->navigation('userbox');
				$this->view->categories('navigations')->navigation('categories');
				$this->view->appTabs('navigations')->navigation('appnav');

				$buttons = array();
				$buttons['save'] = "/applications/modules/news/settings/newsletter.submit.php";
				$buttons['delete'] = $id ? "/applications/modules/news/settings/newsletter.delete.php?id=$id" : false;

				$model = new Model($this->request->application);

				$sth = $model->db->prepare("
					SELECT DISTINCT
						setting_property_id AS value,
						setting_property_title AS name,
						IF (setting_property_id = ?, 'selected=selected', '') AS selected
					FROM db_retailnet.setting_properties
					ORDER BY setting_property_name
				");

				$sth->execute(array($setting->data['newsletter_setting_property_id']));
				$settings = $sth->fetchAll();

				// view template
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'news/settings/newsletter.php');
				$tpl->data('data', $setting->data);
				$tpl->data('buttons', $buttons);
				$tpl->data('settings', $settings);
				$this->view->setTemplate('setting', $tpl);

				Compiler::attach(array(
					'/public/themes/swatch/css/news.css',
					'/public/scripts/formvalidation/css/formValidation.css',
					'/public/scripts/formvalidation/js/formValidation.js',
					'/public/scripts/formvalidation/js/framework/bootstrap.min.js',
					'/public/scripts/bsdialog/bsdialog.min.css',
					'/public/scripts/bsdialog/bsdialog.min.js',
					'/public/js/news.setting.js'
				));
					
			break;
		}
	}
}