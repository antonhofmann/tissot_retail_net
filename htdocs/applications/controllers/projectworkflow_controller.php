<?php 

/**
 * Controller for masterplan over all projects of a query
 */
class Projectworkflow_Controller {
  /**
   * @const string
   */
  const DATE_FORMAT_JS = 'Y/m/d';

  /**
   * @var translate
   */
  protected $Translate;

  /**
   * Setup controller
   */
  public function __construct() {
    if (!user::permission('can_view_project_masterplan')) {
      url::redirect("/messages/show/access_denied");
    }

    $this->user = User::instance();
    $this->request = request::instance();
    
    $this->view = new View();
    $this->view->pagetitle = $this->request->title;
    $this->view->usermenu('usermenu')->user('menu');
    $this->view->categories('pageleft')->navigation('tree');

    $this->Translate = translate::instance();

    // this page uses modular js
    $this->view->amd = true;

    Compiler::attach(Theme::$Default);
  }

  /**
   * List action
   * @param  integer $queryID
   * @return void
   */
  public function index($queryID) {
    $data = $this->assembleGanttData($queryID);

    // set data in view
    $this->view->projectmasterplanTemplates('pagecontent')
      ->project('masterplan.gantt')
      ->data('steps', json_encode($data['steps']))
      ->data('phases', $data['phases'])
      ->data('ganttModule', 'apps/admin/views/project_masterplan/projects_gantt')
      ->data('page', 'masterplan-gantt')
      ->data('projectID', $queryID)
      ->data('translate', translate::instance());
    Compiler::attach(DIR_CSS . 'popup.css');
    $this->view->master('popup.inc');
  }

  /**
   * Assemble data for gantt chart
   * @param  integer $queryID
   * @return array
   */
  protected function assembleGanttData($queryID) {
    $PmMapper = new Project_Masterplan_Datamapper;
    $ValueMapper = new Project_Masterplan_Values_Datamapper;
    $ValueMapper->setDateFormat($ValueMapper::DATE_JS);
    $ValueMapper->setDeliveryDateDoFallback(false);

    $projects = $this->loadProjects($queryID);
    $subphases = $PmMapper->getSubPhases();

    /**
     * add each project as a parent task, then add all it's (sub)phases as child tasks
     */
    $data = array();
    foreach ($projects as $project) {
      $values = $ValueMapper->loadProjectData($project['project_id'], true);

      $Project = new Project;
      $Project->read($project['project_id']);

      $phaseData = $this->projectToPhases($values, $subphases, $project['project_id']);
      list($start, $end) = $this->getProjectDuration($phaseData);
      
      $data[] = array(
        'id'       => $project['project_id'],
        'expanded' => true,
        'color'    => '6E7690',
        'type'     => 'project',
        'title'    => $Project->getPOSAddress(false),
        'number'   => $project['project_number'],
        'project'  => $project['project_id'],
        'start'    => date(self::DATE_FORMAT_JS, $start),
        'end'      => date(self::DATE_FORMAT_JS, $end),
        );
      $data = array_merge($data, $phaseData);
    }
    $phases = array();
    foreach ($subphases as $phase) {
      $phases[] = __::pick($phase, array('id', 'title'));
    }
    return array(
      'steps' => $data,
      'phases' => $phases,
      );
  }

  /**
   * Split a project into phases with aggregated date values
   *
   * What we want here is a gantt "step" for every phase that has values
   * for a project. We get the start/end values from the first and last
   * steps of the phase.
   * 
   * @param  array   Masterplan steps values for project
   * @param  array   Array of phases or subphases with steps
   * @param  integer Project ID of these "steps"
   * @return array
   */
  protected function projectToPhases(array $allValues, $phases, $parentID) {
    $data = array();
    foreach ($phases as $phase) {
      if ($phase->show_in_project_overview != '1') {
        continue;
      }
      $projectPhase = array(
        'id'       => rand(), // since all phases would have the same id, we generate one
        'phaseId'  => $phase->id,
        'title'    => $phase->title,
        'color'    => $phase->steps[0]['color'],
        'parentId' => $parentID,
        'type'     => 'phase',
        'state'    => 'completed',
        );

      $startDate = $phase->getStartDate($allValues);
      // if we have no first date -> skip this phase
      if (!$startDate) {
        continue;
      }
      $endDate   = $phase->getEndDate($allValues);
      if (!$endDate) {
        // if last date isn't set, project is still running
        $endDate = date(self::DATE_FORMAT_JS);
        $projectPhase['state'] = 'pending';
      }
      // if firstdate is same as lastdate, add +1 day to lastdate so it'll become a duration
      if ($startDate == $endDate) {
        $endDate = date(self::DATE_FORMAT_JS, strtotime($startDate . ' + 1 day'));
      }
      // if firstdate > lastdate, we have a phase with repeated steps
      // the quick n dirty handling of the is to set lastdate to today.
      // we'll need a better handling of this in the future
      else if (strtotime($startDate) > strtotime($endDate)) {
        $endDate = date(self::DATE_FORMAT_JS);
      }

      $projectPhase['start'] = $startDate;
      $projectPhase['end'] = $endDate;
      $data[] = $projectPhase;
    }
    return $data;
  }
  
  /**
   * Load projects from old retailnet query code 
   * @param  integer $queryID
   * @return array
   */
  protected function loadProjects($queryID) {
    // backup and reset compiler before and after the includes below or it'll mess up our layout
    $compilerBackup = Compiler::backup();

    require_once $_SERVER['DOCUMENT_ROOT'] . '/include/frame.php';
    //check_access('can_perform_queries');
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mis/include/query_get_functions.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mis/projects_query_filter_strings.php';
    $query_id = $queryID;
    require_once $_SERVER['DOCUMENT_ROOT'] . '/mis/include/parse_projects_filter.php';

    Compiler::reset();
    Compiler::attach($compilerBackup);

    // set page title to query_title
    // this should not be done in this method, but imo it's better than to read the mis filter stuff
    // again in another method just so "chain of command" is adhered to
    $this->view->pagetitle = $query_title;

    return $projects;
  }

  /**
   * Get the last date from an array of steps
   * @todo  this should be in a Phase class or something, not here
   * @param  array $steps
   * @param  array $values
   * @return string Date string
   */
  protected function getLastDate($steps, $values) {
    return $this->findDate(array_reverse($steps), $values, true);
  }

  /**
   * Get the first date from an array of steps
   * @todo  this should be in a Phase class or something, not here
   * @param  array $steps
   * @param  array $values
   * @return string Date string
   */
  protected function getFirstDate($steps, $values) {
    return $this->findDate($steps, $values, true); 
  }

  /**
   * Find the first valid date in an array of steps
   * @param  array $steps
   * @param  array $values
   * @param  boolean $greedy Abort on first try on continue until we find one
   * @return string Date string
   */
  protected function findDate($steps, $values, $greedy = false) {
    foreach ($steps as $step) {
      if ($step['type'] == 'info') {
        continue;
      }
      if (!array_key_exists($step['value'], $values)) {
        continue;
      }
      $value = $values[$step['value']];
      $date = !is_null($value) ? strtotime($value) : false;
      if ($date === false) {
        if ($greedy) {
          continue;
        }
        else {
          return false;
        }
      }
      return $value;
    }
    return false;
  }

  /**
   * Get project start and end dates from phase data
   * @param array $phaseData
   * @return array [0] = start [1] = end (timestamps)
   */
  protected function getProjectDuration(array $phaseData) {
    /*$isPending = false;
    foreach ($phaseData as $phase) {
      if ($phase['state'] == 'pending') {
        $isPending = true;
      }
    }*/
    $dates = $this->extractDates($phaseData);
    $startDate = array_shift($dates);
    $endDate = array_pop($dates);
    return array($startDate, $endDate);
  }

  /**
   * Extract all start and end dates from an array of phase data
   * @param array $phaseData
   * @return array Array of timestamps
   */
  protected function extractDates(array $phaseData) {
    $dates = array();
    foreach ($phaseData as $phase) {
      $dates[] = strtotime($phase['start']);
      $dates[] = strtotime($phase['end']);
    }
    sort($dates);
    return $dates;
  }
}