<?php 

	class Maintenance_Controller {
		
		public function __construct() {
				
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			$link = $this->request->query('add');
			$this->request->field('add', $link);
			
			$link = $this->request->query('data');
			$this->request->field('form', $link);
		
			$this->view->maintenance_list('pagecontent')->maintenance('list');
		}
		
		public function add() {
			
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
			Compiler::attach(DIR_CSS."jquery.ui.css");
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
				
			// form dataloader
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
				
			// buttons
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
				
			$this->view->maintenance('pagecontent')
			->maintenance('data')
			->data('data', $data)
			->data('buttons', $buttons);
		}
		
		public function data() {
			
			$id = url::param();
			
			$maintenance = new Maintenance();
			$data = $maintenance->read($id);

			// maintenance start date
			$start = strtotime($data['maintenance_window_start']);
			$data['maintenance_window_start'] = date('d.m.Y', $start);
			$data['maintenance_window_start_hours'] = date('H:i', $start);
			
			// maintenance stop date
			$stop = strtotime($data['maintenance_window_stop']);
			$data['maintenance_window_stop'] = date('d.m.Y', $stop);
			$data['maintenance_window_stop_hours'] = date('H:i', $stop);
			
			// maintenance warning date
			$warning = strtotime($data['maintenance_window_warning_start_date']);
			$data['maintenance_window_warning_start_date'] = date('d.m.Y', $warning);
			$data['maintenance_window_warning_start_date_hours'] = date('H:i', $warning);
			
			if (!$maintenance->id) {
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
			Compiler::attach(DIR_CSS."jquery.ui.css");
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
				
			// form dataloader
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
				
			// buttons
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
			
			$integrity = new Integrity();
			$integrity->set($id, 'maintenance_windows');
			
			if ($integrity->check()) {
				$buttons['delete'] = $this->request->link('/applications/helpers/maintenance.delete.php', array('id'=>$id));
			}
				
			$this->view->maintenance('pagecontent')
			->maintenance('data')
			->data('data', $data)
			->data('buttons', $buttons);
		}
	}