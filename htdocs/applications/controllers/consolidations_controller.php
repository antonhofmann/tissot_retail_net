<?php 

	class Consolidations_Controller {
		
		public function __construct() { 
			
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() { 
			
			switch ($this->request->application) {
				
				case 'mps':
					$permissionEdit = user::permission('can_edit_mps_master_sheets');
				    $permissionView = user::permission('can_view_consolidation');
				break;

				case 'lps':
					$permissionEdit = user::permission('can_edit_lps_master_sheets');
				break;
			}
			
			// check access te consolidation
			if (!$permissionEdit and !$permissionView) {
				message::access_denied();
				url::redirect('/messages/show/access_denied');
			}
			
			// form link
			$link = $this->request->query('items');
			$this->request->field('data', $link);
			
			// template: list
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'list.php');
			$tpl->data('url', '/applications/modules/mastersheet/list.php');
			$tpl->data('class', 'list-1000');

			// assign template to view
			$this->view->setTemplate('mastersheet', $tpl);
			
			Compiler::attach(array(
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js'
			));
		}
		
		public function items() {
			
			$id = url::param();
			$application = $this->request->application;

			switch ($application) {
				
				case 'mps':
					$permissionEdit = user::permission('can_edit_mps_master_sheets');
					$permissionView = user::permission('can_view_consolidation');

					$tableMasterSheet = 'mps_mastersheets';
					$urlPrintSplittingList = '/applications/exports/excel/mastersheet.splitting.list.php';

					$urlPrint = '/applications/exports/excel/mastersheet.consolidation.items.php';

					$mapMasterSheet = array(
						'mps_mastersheet_year' => 'mastersheet_year',
						'mps_mastersheet_name' => 'mastersheet_name',
						'mps_mastersheet_consolidated' => 'mastersheet_consolidated',
						'mps_mastersheet_archived' => 'mastersheet_archived'
					);

					$queryTotalMasterSheetItems = "
						SELECT COUNT(mps_ordersheet_item_id) AS total
						FROM mps_ordersheet_items
						INNER JOIN mps_ordersheets ON mps_ordersheet_item_ordersheet_id = mps_ordersheet_id
						WHERE mps_ordersheet_mastersheet_id = ?
					";

					$queryTotalMasterSheetItemsNotOrdered = "
						SELECT COUNT(mps_ordersheet_item_id) AS total
						FROM mps_ordersheet_items
						INNER JOIN mps_ordersheets ON mps_ordersheet_item_ordersheet_id = mps_ordersheet_id
						WHERE mps_ordersheet_mastersheet_id = ? AND (mps_ordersheet_item_order_date IS NULL OR mps_ordersheet_item_order_date = '')
					";

				break;

				case 'lps':
					$permissionEdit = user::permission('can_edit_lps_master_sheets');
					$tableMasterSheet = 'lps_mastersheets';
					$urlPrintSplittingList = '/applications/modules/mastersheet/print.detail.list.php';

					$urlPrint = '/applications/modules/mastersheet/print.launchplan.items.php';

					$mapMasterSheet = array(
						'lps_mastersheet_year' => 'mastersheet_year',
						'lps_mastersheet_name' => 'mastersheet_name',
						'lps_mastersheet_consolidated' => 'mastersheet_consolidated',
						'lps_mastersheet_archived' => 'mastersheet_archived',
						'lps_mastersheet_pos_date' => 'mastersheet_pos_date',
						'lps_mastersheet_phase_out_date' => 'mastersheet_phase_out_date',
						'lps_mastersheet_week_number_first' => 'mastersheet_week_number_first',
						'lps_mastersheet_weeks' => 'mastersheet_weeks',
						'lps_mastersheet_estimate_month' => 'mastersheet_estimate_month'
					);

					$queryTotalMasterSheetItems = "
						SELECT COUNT(lps_launchplan_item_id) AS total
						FROM lps_launchplan_items
						INNER JOIN lps_launchplans ON lps_launchplan_item_launchplan_id = lps_launchplan_id
						WHERE lps_launchplan_mastersheet_id = ?
					";

					$queryTotalMasterSheetItemsNotOrdered = "
						SELECT COUNT(lps_launchplan_item_id) AS total
						FROM lps_launchplan_items
						INNER JOIN lps_launchplans ON lps_launchplan_item_launchplan_id = lps_launchplan_id
						WHERE lps_launchplan_mastersheet_id = ? AND (lps_launchplan_item_order_date IS NULL OR lps_launchplan_item_order_date = '')
					";

				break;
			}
				
			// order sheet
			$mastersheet = new Modul($application);
			$mastersheet->setTable($tableMasterSheet);
			$mastersheet->setDataMap($mapMasterSheet);
			$mastersheet->read($id);
			
			// check access te consolidation
			if ((!$permissionEdit and !$permissionView ) || !$mastersheet->id) {
				message::access_denied();
				url::redirect($this->request->query());
			} elseif ($mastersheet->data['mastersheet_archived'] && !$this->request->archived) {
				url::redirect("/".$this->request->application."/mastersheets/archived/data/$id");
			}
			
			// buttons
			$buttons = array();
			
			// button: back
			$buttons['back'] = $this->request->query();
	
			// button: splitting list button
			$buttons['splitting'] = $this->request->link($urlPrintSplittingList, array('mastersheet'=>$id));
			
			// button: print items
			$buttons['print'] = $this->request->link($urlPrint, array('mastersheet'=>$id));
			
			// buuton: version
			if($permissionEdit)
			{
				if (!$mastersheet->data['mastersheet_consolidated']) {
					$buttons['consolidate'] = $this->request->link('/applications/modules/mastersheet/consolidation/consolidate.php', array('id'=>$id));
					$buttons['version'] = $this->request->link('/applications/modules/mastersheet/splitting/save.php', array('id'=>$id));
				}
				
				// button: unconsolidate
				if ($mastersheet->data['mastersheet_consolidated']) {
					
					$model = new Model($application);
					
					// total mastersheets items
					$sth = $model->db->prepare($queryTotalMasterSheetItems);
					$sth->execute(array($mastersheet->id));
					$result = $sth->fetch();
					$totalMasterSheetItems = $result['total'];
					
					// total mastersheet ordered items
					$sth = $model->db->prepare($queryTotalMasterSheetItemsNotOrdered);
					$sth->execute(array($mastersheet->id));
					$result = $sth->fetch();
					$totalMasterSheetItemsNotOrdered = $result['total'];

					// button: unconsolidate
					if ($totalMasterSheetItems && $totalMasterSheetItems==$totalMasterSheetItemsNotOrdered) {
						$buttons['unconsolidate'] = $this->request->link('/applications/modules/mastersheet/consolidation/unconsolidate.php', array('id'=>$id));
					}
				}
			}
			// header
			$header = $mastersheet->data['mastersheet_name'].', '.$mastersheet->data['mastersheet_year'];
			$this->view->header('pagecontent')->node('header')->data('title', $header);
			$this->view->tabs('pagecontent')->navigation('tab');

			switch ($application) {
				
				case 'mps':
					// template: master sheet consolidation ites
					$this->view->mastersheetItems('pagecontent')
					->mastersheet('consolidation.item.list')
					->data('buttons', $buttons)
					->data('id', $id);

					Compiler::attach(array(
						'/public/scripts/jquery/jquery.ui.css',
						'/public/css/jquery.ui.css',
						'/public/scripts/jquery/jquery.ui.js',
						'/public/scripts/validationEngine/css/validationEngine.jquery.css',
						'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
						'/public/scripts/validationEngine/js/jquery.validationEngine.js',
						'/public/scripts/qtip/qtip.css',
						'/public/scripts/qtip/qtip.js',
						'/public/scripts/dropdown/dropdown.css',
						'/public/scripts/dropdown/dropdown.js',
						'/public/scripts/table.loader.js'
					));

				break;

				case 'lps':
					$this->request->field('mastersheet', $id);
					$this->request->field('fixedNotSelectable', 1);

					$model = new Model($application);

					$sth = $model->db->prepare("
						SELECT 
							lps_launchplan_id AS value,
							CONCAT(country_name, ', ', address_company) as caption
						FROM lps_launchplans
						INNER JOIN db_retailnet.addresses ON address_id = lps_launchplan_address_id
						INNER join db_retailnet.countries ON country_id = address_country
						WHERE lps_launchplan_mastersheet_id = ? AND lps_launchplan_workflowstate_id IN (5,7)
						ORDER by caption
					");

					$sth->execute(array($id));
					$result = $sth->fetchAll();

					$dropdowns['launchplans'] = ui::dropdown($result, array(
						'id' => 'launchplan',
						'name' => 'launchplan',
						'caption' => 'All Launch Plans'
					));

					// ordersheet form
					$tpl = new Template('pagecontent');
					$tpl->template(PATH_TEMPLATES.'mastersheet/items.consolidation.php');
					$tpl->data('data', $data);
					$tpl->data('dropdowns', $dropdowns);
					$tpl->data('buttons', $buttons);

					// assign template to view
					$this->view->setTemplate('ordersheet', $tpl);

					Compiler::attach(array(
						'/public/scripts/spreadsheet/spreadsheet.css',
						'/public/scripts/spreadsheet/spreadsheet.js',
						'/public/scripts/qtip/qtip.css',
						'/public/scripts/qtip/qtip.js',
						'/public/js/mastersheet.items.consolidation.js',
						'/public/css/launchplan.items.css'
					));

				break;
			}
					
		}
		
		public function cost() {
			
			$id = url::param();
			$application = $this->request->application;

			switch ($application) {
				
				case 'mps':
					$permissionEdit = user::permission('can_edit_mps_master_sheets');

					$tableMasterSheet = 'mps_mastersheets';

					$mapMasterSheet = array(
						'mps_mastersheet_year' => 'mastersheet_year',
						'mps_mastersheet_name' => 'mastersheet_name',
						'mps_mastersheet_consolidated' => 'mastersheet_consolidated',
						'mps_mastersheet_archived' => 'mastersheet_archived'
					);

					$query = "
						SELECT
							mps_ordersheet_id AS id,
							mps_material_planning_type_id AS group_id,
							mps_material_planning_type_name AS group_name,
							mps_material_collection_category_id AS subgroup_id,
							mps_material_collection_category_code AS subgroup_name,
							mps_material_id AS reference_id,
							(mps_ordersheet_item_price * SUM(mps_ordersheet_item_quantity)) AS cost_quantity,
							(mps_ordersheet_item_price * SUM(mps_ordersheet_item_quantity_approved)) AS cost_quantity_approved
						FROM mps_ordersheet_items
						INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
						INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
						LEFT JOIN mps_material_collections ON mps_material_collection_id = mps_material_material_collection_id
						LEFT JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id
						LEFT JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id
						WHERE mps_ordersheet_mastersheet_id = ?
						GROUP BY mps_material_id
						ORDER BY
							mps_material_planning_type_name ASC,
							mps_material_collection_category_code ASC
					";

				break;

				case 'lps':
					$permissionEdit = user::permission('can_edit_lps_master_sheets');

					$tableMasterSheet = 'lps_mastersheets';

					$mapMasterSheet = array(
						'lps_mastersheet_year' => 'mastersheet_year',
						'lps_mastersheet_name' => 'mastersheet_name',
						'lps_mastersheet_consolidated' => 'mastersheet_consolidated',
						'lps_mastersheet_archived' => 'mastersheet_archived',
						'lps_mastersheet_pos_date' => 'mastersheet_pos_date',
						'lps_mastersheet_phase_out_date' => 'mastersheet_phase_out_date',
						'lps_mastersheet_week_number_first' => 'mastersheet_week_number_first',
						'lps_mastersheet_weeks' => 'mastersheet_weeks',
						'lps_mastersheet_estimate_month' => 'mastersheet_estimate_month'
					);

					$query = "
						SELECT
							lps_launchplan_id AS id,
							lps_product_group_id AS group_id,
							lps_product_group_name AS group_name,
							lps_reference_id AS reference_id,
							(lps_launchplan_item_price * SUM(lps_launchplan_item_quantity)) AS cost_quantity,
							(lps_launchplan_item_price * SUM(lps_launchplan_item_quantity_approved)) AS cost_quantity_approved,
							(lps_launchplan_item_price * SUM(lps_launchplan_item_quantity_estimate)) AS cost_quantity_estimate
						FROM lps_launchplan_items
						INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
						INNER JOIN lps_references ON lps_reference_id = lps_launchplan_item_reference_id
						INNER JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id
						WHERE lps_launchplan_mastersheet_id = ?
						GROUP BY lps_reference_id
						ORDER BY
							lps_product_group_name ASC
					";

				break;
			}

			// order sheet
			$mastersheet = new Modul($application);
			$mastersheet->setTable($tableMasterSheet);
			$mastersheet->setDataMap($mapMasterSheet);
			$mastersheet->read($id);
			
			// check access te consolidation
			if (!$permissionEdit || !$mastersheet->id) {
				message::access_denied();
				url::redirect($this->request->query());
			} elseif ($mastersheet->data['mastersheet_archived'] && !$this->request->archived) {
				url::redirect("/".$this->request->application."/mastersheets/archived/data/$id");
			}
				
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// header
			$header = $mastersheet->data['mastersheet_name'].', '.$mastersheet->data['mastersheet_year'];
			$this->view->header('pagecontent')->node('header')->data('title', $header);
			$this->view->tabs('pagecontent')->navigation('tab');

			$model = new Model($application);

			// get master sheet items
			$sth = $model->db->prepare($query);
			$sth->execute(array($mastersheet->id));
			$result = $sth->fetchAll();

			if ($result) {

				foreach ($result as $row) {
					
					$group = $row['group_id'];
					$subgroup = $row['subgroup_id'];
					$item = $row['reference_id'];

					$datagrid[$group]['caption'] = $row['group_name'];

					$data = array(
						'cost_quantity' => $row['cost_quantity'],
						'cost_quantity_approved' => $row['cost_quantity_approved'],
						'cost_quantity_estimate' => $row['cost_quantity_estimate']
					);

					if ($subgroup) {
						$datagrid[$group]['subgroups'][$subgroup]['caption'] = $row['subgroup_name'];
						$datagrid[$group]['subgroups'][$subgroup]['items'][$item] = $data;
					} else {
						$datagrid[$group]['items'][$item] = $data;
					}
				}
			}

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'mastersheet/costs.php');
			$tpl->data('buttons', $buttons);
			$tpl->data('datagrid', $datagrid);
			$this->view->setTemplate('mastersheet.cost', $tpl);
		}
		
		public function splittinglists() {
			
			$id = url::param();
			$param = url::param(1);
			$application = $this->request->application;

			switch ($application) {
				
				case 'mps':
					$permissionEdit = user::permission('can_edit_mps_master_sheets');
					$tableMasterSheet = 'mps_mastersheets';
					
					$urlPrint = '/applications/exports/excel/mastersheet.splitting.list.items.php';
					$urlPrintSplittingList = '/applications/exports/excel/mastersheet.splitting.list.version.php';

					$tableVersion = 'mps_mastersheet_splitting_lists';
					$mapVersion = array(
						'mps_mastersheet_splitting_list_name' => 'name',
						'mps_mastersheet_splitting_list_mastersheet_id' => 'mastersheet'
					);

					$mapMasterSheet = array(
						'mps_mastersheet_year' => 'mastersheet_year',
						'mps_mastersheet_name' => 'mastersheet_name',
						'mps_mastersheet_consolidated' => 'mastersheet_consolidated',
						'mps_mastersheet_archived' => 'mastersheet_archived'
					);

					$queryVersion = "
						SELECT
							mps_mastersheet_splitting_list_item_id AS id,
							mps_mastersheet_splitting_list_item_material_id AS reference_id,
							mps_mastersheet_splitting_list_item_price AS price,
							mps_mastersheet_splitting_list_item_total_quantity AS quantity,
							mps_mastersheet_splitting_list_item_total_quantity_approved AS quantity_approved,
							(mps_mastersheet_splitting_list_item_price * mps_mastersheet_splitting_list_item_total_quantity_approved) AS total_cost,
							mps_material_planning_type_id AS group_id,
							mps_material_planning_type_name AS group_name,
							mps_material_collection_category_id AS subgroup_id,
							mps_material_collection_category_code AS subgroup_name,
							mps_material_collection_code AS collection_code,
							mps_material_code AS reference_code,
							mps_material_name AS reference_name,
							mps_material_setof AS setof,
							mps_material_hsc,
							currency_symbol
						FROM mps_mastersheet_splitting_list_items
						INNER JOIN mps_materials ON mps_material_id = mps_mastersheet_splitting_list_item_material_id
						LEFT JOIN mps_material_collections ON mps_material_collection_id = mps_material_material_collection_id
						LEFT JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id
						LEFT JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id
						INNER JOIN db_retailnet.currencies ON currency_id = mps_material_currency_id
						WHERE mps_mastersheet_splitting_list_item_splittinglist_id = ?
						ORDER BY
							mps_material_planning_type_name ASC,
							mps_material_collection_category_code ASC,
							mps_material_code ASC,
							mps_material_name ASC
					";

				break;

				case 'lps':
					$permissionEdit = user::permission('can_edit_lps_master_sheets');
					$tableMasterSheet = 'lps_mastersheets';
					
					$urlPrint = '/applications/modules/mastersheet/version/print.launchplan.items.php';
					$urlPrintSplittingList = '/applications/modules/mastersheet/version/print.detail.list.php';

					$tableVersion = 'lps_mastersheet_detail_lists';
					$mapVersion = array(
						'lps_mastersheet_detail_list_name' => 'name',
						'lps_mastersheet_detail_list_mastersheet_id' => 'mastersheet'
					);

					$mapMasterSheet = array(
						'lps_mastersheet_year' => 'mastersheet_year',
						'lps_mastersheet_name' => 'mastersheet_name',
						'lps_mastersheet_consolidated' => 'mastersheet_consolidated',
						'lps_mastersheet_archived' => 'mastersheet_archived',
						'lps_mastersheet_pos_date' => 'mastersheet_pos_date',
						'lps_mastersheet_phase_out_date' => 'mastersheet_phase_out_date',
						'lps_mastersheet_week_number_first' => 'mastersheet_week_number_first',
						'lps_mastersheet_weeks' => 'mastersheet_weeks',
						'lps_mastersheet_estimate_month' => 'mastersheet_estimate_month'
					);

					$queryVersion = "
						SELECT
							lps_mastersheet_detail_list_item_id AS id,
							lps_mastersheet_detail_list_item_reference_id AS reference_id,
							lps_mastersheet_detail_list_item_price AS price,
							lps_mastersheet_detail_list_item_total_quantity AS quantity,
							lps_mastersheet_detail_list_item_total_quantity_approved AS quantity_approved,
							lps_mastersheet_detail_list_item_total_quantity_estimate AS quantity_estimate,
							(lps_mastersheet_detail_list_item_price * lps_mastersheet_detail_list_item_total_quantity_approved) AS total_cost,
							lps_product_group_id AS group_id,
							lps_product_group_name AS group_name,
							lps_collection_code AS collection_code,
							lps_reference_code AS reference_code,
							lps_reference_name AS reference_name,
							currency_symbol
						FROM lps_mastersheet_detail_list_items
						INNER JOIN lps_references ON lps_reference_id = lps_mastersheet_detail_list_item_reference_id
						LEFT JOIN lps_collections ON lps_collection_id = lps_reference_collection_id
						LEFT JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id
						INNER JOIN db_retailnet.currencies ON currency_id = lps_reference_currency_id
						WHERE lps_mastersheet_detail_list_item_list_id = ?
						ORDER BY
							lps_product_group_name ASC,
							lps_reference_code ASC
					";

				break;
			}

			// order sheet
			$mastersheet = new Modul($application);
			$mastersheet->setTable($tableMasterSheet);
			$mastersheet->setDataMap($mapMasterSheet);
			$mastersheet->read($id);
			
			// check access te consolidation
			if (!$permissionEdit || !$mastersheet->id) {
				message::access_denied();
				url::redirect($this->request->query());
			} elseif ($mastersheet->data['mastersheet_archived'] && !$this->request->archived) {
				url::redirect("/".$this->request->application."/mastersheets/archived/data/$id");
			}

			if ($param) {
				$version = new Modul($application);
				$version->setTable($tableVersion);
				$version->setDataMap($mapVersion);
				$version->read($param);
				$versionName = $version->data['name'];
			}

			// header
			$header = $mastersheet->data['mastersheet_name'].', '.$mastersheet->data['mastersheet_year'];
			$this->view->header('pagecontent')->node('header')->data('title', $header)->data('subtitle', $versionName);
			$this->view->tabs('pagecontent')->navigation('tab');
				
			// buttons
			$buttons = array();
			
			// splitting list details
			if ($param) {
				
				// db connector
				$model = new Model($application);

				// button: back
				$buttons['back'] = $this->request->query("splittinglists/$id");
	
				// filter: mastersheet
				$filters['mastersheet'] = "mps_mastersheet_splitting_list_item_splittinglist_id = $splittinglist";
				
				// get version items
				$sth = $model->db->prepare($queryVersion);
				$sth->execute(array($param));
				$result = $sth->fetchAll();
				
				if ($result) {
	
					foreach ($result as $row) {
						
						$group = $row['group_id'];
						$subgroup = $row['subgroup_id'];
						$key = $row['id'];
						
						$datagrid[$group]['caption'] = $row['group_name'];

						$item = array(
							'collection_code' => $row['collection_code'],
							'reference_code' => $row['reference_code'],
							'reference_name' => $row['reference_name'],
							'price' => $row['price'],
							'currency_symbol' => $row['currency_symbol'],
							'mps_material_hsc' => $row['mps_material_hsc'],
							'setof' => $row['setof'],
							'quantity' => $row['quantity'],
							'quantity_approved' => $row['quantity_approved'],
							'quantity_estimate' => $row['quantity_estimate'],
							'total_cost' => $row['total_cost']
						);
						
						if ($subgroup) {
							$datagrid[$group]['subgroups'][$subgroup]['caption'] = $row['subgroup_name'];
							$datagrid[$group]['subgroups'][$subgroup]['items'][$key] = $item;
						} else {
							$datagrid[$group]['items'][$key] = $item;
						}
					}
				}
				
				
				// button: splitting list dialog
				$buttons['splitting'] = $this->request->link($urlPrintSplittingList, array(
					'mastersheet' => $id,
					'version' => $param
				));
				
				// button: print item list
				$buttons['print'] = $this->request->link($urlPrint, array(
					'mastersheet' => $id,
					'version' => $param
				));
				
				// button: 
				if (!$mastersheet->consolidated) {
					$buttons['delete'] = $this->request->link('/applications/modules/mastersheet/splitting/delete.php', array(
						'mastersheet' => $id,
						'version' => $param
					));
				}
				
				$this->request->field('mastersheet', $id);
				$this->request->field('version', $param);

				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'mastersheet/version.php');
				$tpl->data('datagrid', $datagrid);
				$tpl->data('buttons', $buttons);
				$this->view->setTemplate('mastersheet.version', $tpl);
			}
			else {	
		
				// button: back
				$buttons['back'] = $this->request->query();
				
				// link to detail view
				$link = $this->request->query("splittinglists/$id");
				$this->request->field('form', $link);
				$this->request->field('id', $id);

				// template: list
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'list.php');
				$tpl->data('url', '/applications/modules/mastersheet/splitting/list.php');
				$tpl->data('class', 'list-700');

				// assign template to view
				$this->view->setTemplate('mastersheet', $tpl);

				Compiler::attach(array(
					'/public/scripts/dropdown/dropdown.css',
					'/public/scripts/dropdown/dropdown.js',
					'/public/scripts/table.loader.js'
				));
			}
		}
	}
	