<?php


class Articles_Controller { 

	static public $permissions = array();

	public function __construct() {
		
		Settings::init()->theme = 'swatch';
		$this->request = request::instance();
		$this->view = new View();
		$this->view->pageclass = 'theme-news news-article-list';
		$this->view->pagetitle = $this->request->title;
		$this->view->userboxMenu('navigations')->navigation('userbox');
		$this->view->categories('navigations')->navigation('categories');
		$this->view->appTabs('navigations')->navigation('appnav');

		static::$permissions = array(
			'view.all' => user::permission('can_view_all_news_articles'),
			'view.his' => user::permission('can_view_only_his_news_articles'),
			'edit.all' => user::permission('can_edit_all_news_articles'),
			'edit.his' => user::permission('can_edit_only_his_news_articles')
		);

		Compiler::attach('/public/themes/swatch/css/news.css');
	}

	public function index () {

		$application = $this->request->application;
		$controller = $this->request->controller;
		$archived = $this->request->archived;
		$action = $this->request->controller;
		
		$keyword = $archived ? 'articles.archive' : 'articles';
		$_REQUEST = session::filter($application, $keyword, true); 

		$filterSection = $_REQUEST['section'];
		$filterState = $_REQUEST['state'];
		$filterSort = $_REQUEST['sort'];
		$filterView = $_REQUEST['view'];
		$filterSearch = $_REQUEST['search'];
		$page = $_REQUEST['page'] ?: 1;
		
		// data
		$data = array();
		$data['application'] = $application;

		// buttons
		if (!$this->request->archived) {
			$buttons['add'] = static::$permissions['edit.all'] || static::$permissions['edit.his'] ? true : false;
		}

		$model = new Model($application);

		$stateFilter =  $archived ? "news_article_publish_state_id = 5" : "(news_article_multiple = 1 OR news_article_publish_state_id < 5)";

		// article sections
		$sth = $model->db->prepare("
			SELECT DISTINCT
				news_section_id AS id, 
				news_section_name AS name,
				CONCAT('.', LOWER(REPLACE(news_section_name, ' ', '-')) ) AS filter,
				IF (news_section_id = ?, 'active', '') AS active
			FROM news_articles 
			INNER JOIN news_categories ON news_articles.news_article_category_id = news_categories.news_category_id
			INNER JOIN news_sections ON news_categories.news_category_section_id = news_sections.news_section_id
			WHERE $stateFilter
			ORDER BY news_section_order, news_section_name
		");

		$sth->execute(array($filterSection));
		$sections = $sth->fetchAll();

		array_unshift($sections, array(
			'name' => 'All',
			'active' => $filterSection ? null : 'active'
		));

		if (!$archived) {

			// get workflow states
			$sth = $model->db->prepare("
				SELECT DISTINCT
					news_workflow_state_id AS id, 
					news_workflow_state_name AS name,
					CONCAT('.', LOWER(REPLACE(news_workflow_state_name, ' ', '-')) ) AS filter,
					IF (news_workflow_state_id = ?, 'active', '') AS active
				FROM news_articles 
				INNER JOIN news_workflow_states ON news_workflow_state_id = news_article_publish_state_id
				WHERE $stateFilter AND news_workflow_state_id <> 5
				ORDER BY news_workflow_state_order, news_workflow_state_name
			");

			$sth->execute(array($filterState));
			$states = $sth->fetchAll();

			array_unshift($states, array(
				'name' => 'All',
				'active' => $filterState ? null : 'active'
			));
		}

		// button sorts
		$sorts = array();

		// article title
		$sorts[] = array(
			'title' => 'Article Title',
			'filter' => 'title',
			'ascending' => true,
			'icon' => "<i class='fa fa-caret-down'></i>",
			'active' =>  $filterSort=='title' ? 'active' : null
		);

		// section
		$sorts[] = array(
			'title' => Translate::instance()->state,
			'filter' => 'state',
			'ascending' => true,
			'icon' => "<i class='fa fa-caret-down'></i>",
			'active' => $filterSort=='state' ? 'active' : null
		);

		// created on
		$sorts[] = array(
			'title' => 'Date Created',
			'filter' => 'created',
			'ascending' => true,
			'icon' => "<i class='fa fa-caret-down'></i>",
			'active' => $filterSort=='created' ? 'active' : null
		);

		// expiry on
		if ($archived) {
			$sorts[] = array(
				'title' => 'Archive Date',
				'filter' => 'expired',
				'ascending' => true,
				'icon' => "<i class='fa fa-caret-down'></i>",
				'active' => $filterSort=='expired' ? 'active' : null
			);
		}


		// button view
		$views = array();

		// thumbnails
		$views[] = array(
			'title' => '<i class="fa fa-th"></i>',
			'filter' => 'thumbnail-view',
			'active' => $filterView=='thumbnail-view' ? 'active' : null
		);

		// list
		$views[] = array(
			'title' => '<i class="fa fa-bars"></i>',
			'filter' => 'list-view',
			'active' => $filterView=='list-view' ? 'active' : null
		);


		$title = $this->request->archived ? $this->view->pagetitle : 'Workbench';

		// view template
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'news/article/list.php');
		$tpl->data('title', $title);
		$tpl->data('archived', $this->request->archived);
		$tpl->data('data', $data);
		$tpl->data('page', $page);
		$tpl->data('buttons', $buttons);
		$tpl->data('sections', $sections);
		$tpl->data('states', $states);
		$tpl->data('sorts', $sorts);
		$tpl->data('views', $views);
		$tpl->data('filterSearch', $filterSearch);
		$this->view->setTemplate('articles', $tpl);

		Compiler::attach(array(
			'/public/themes/swatch/css/news.articles.css',
			'/public/scripts/infinitescroll/infinitescroll.js',
			'/public/scripts/isotope/isotope.min.js',
			'/public/scripts/mustache/mustache.js',
			'/public/scripts/sticky/sticky.parent.min.js',
			'/public/js/news.articles.js'
		));
	}

	public function workbench() {
		url::redirect('/gazette/articles');
	}


	public function article() {

		$id = url::param();
		$application = $this->request->application;
		$user = User::instance();

		$_CAN_VIEW = static::$permissions['view.all'] || static::$permissions['view.his'] ? true : false;
		$_CAN_EDIT = static::$permissions['edit.all'] || static::$permissions['edit.his'] ? true : false;
		$_ACCESS_FULL = static::$permissions['view.all'] || static::$permissions['edit.all'] ? true : false;
		$_ACCESS_LIMITED = !$_ACCESS_FULL && (static::$permissions['view.his'] || static::$permissions['edit.his']) ? true : false;

		// no right to access or to view article
		if ( !$_CAN_VIEW && !$_CAN_EDIT ) { 
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$model = new Model($application);

		// add article if not exist
		if (!$id && $_CAN_EDIT) { 
			
			$sth = $model->db->prepare("
				INSERT INTO news_articles (
					news_article_active,
					news_article_publish_state_id,
					news_article_owner_user_id,
					user_created
				) VALUES (1, 1, ?, ?)
			");

			$sth->execute(array($user->id, $user->login));
			$id = $model->db->lastInsertId();

			if ($id) {
				
				// add teaser
				$sth = $model->db->prepare("
					INSERT INTO news_article_contents (
						news_article_content_article_id,
						news_article_content_template_id,
						news_article_content_order,
						user_created
					) VALUES (?,?,?,?)
				");

				$sth->execute(array($id, 1, 1, $user->login));

				// track article
				$sth = $model->db->prepare("
					INSERT INTO news_article_tracks (
						news_article_track_article_id,
						news_article_track_user_id,
						news_article_track_type_id
					)
					VALUES (?,?,?)
				");

				$sth->execute(array($id, $user->id, 31));
			}

			Url::redirect($this->request->query("article/$id"));
		}


		// read article
		$article = new Modul($application);
		$article->setTable('news_articles');
		$article->read($id);

		if ( (!$article->id) ) { 
			Message::error("Article not found.");
			url::redirect($this->request->query());
		}

		$_OWNER = $user->id == $article->data['news_article_owner_user_id'] ? true : false;
		$_CONFIRMER = $user->id == $article->data['news_article_confirmed_user_id'] ? true : false;

		$access = $_ACCESS_FULL;

		if ($_ACCESS_LIMITED) {
			
			$access = $_OWNER;

			if (!$access && $article->data['news_article_confirmed_user_id']) {
				$access = $user->id == $article->data['news_article_confirmed_user_id'] ? true : false;
			}
		}

		// article not found
		// or user is not owner or confirmer
		if ( !$article->id || !$access) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		// clean up unused templates
		$model->db->exec("
			DELETE FROM news_article_contents
			WHERE news_article_content_data IS NULL 
			AND news_article_content_template_id <> 1 
			AND news_article_owner_user_id = $user->id
		");

		// form data loader
		$data = $article->data;
		$data['id'] = $id;

		// get workflow states
		$result = $model->query("
			SELECT DISTINCT
				news_workflow_state_id AS id,
				news_workflow_state_name AS caption
			FROM news_workflow_states
			ORDER BY news_workflow_state_order, news_workflow_state_name
		")->fetchAll();

		$states = _array::extract($result);

		// get sections
		$result = $model->query("
			SELECT
				news_sections.news_section_id AS section,
				news_sections.news_section_name AS sectionName,
				news_categories.news_category_id AS category, 
				news_categories.news_category_name AS categoryName
			FROM news_categories 
			INNER JOIN news_sections ON news_categories.news_category_section_id = news_sections.news_section_id
			WHERE news_categories.news_category_active = 1
			ORDER BY 
				news_sections.news_section_order, 
				news_sections.news_section_name, 
				news_categories.news_category_order, 
				news_sections.news_section_name
		")->fetchAll();

		$sections = array();

		if ($result) {
			foreach ($result as $row) {
				$section = $row['section'];
				$category = $row['category'];
				$sections[$section]['name'] = $row['sectionName'];
				$sections[$section]['categories'][$category]['name'] = $row['categoryName'];
			}
		}

		$authors = array();

		// get retailnet authors
		$result = $model->query("
			SELECT DISTINCT 
				db_retailnet.users.user_id AS id, 
				CONCAT(db_retailnet.users.user_firstname, ' ', db_retailnet.users.user_name) AS name
			FROM db_retailnet.users 
			INNER JOIN db_retailnet.user_roles ON db_retailnet.users.user_id = db_retailnet.user_roles.user_role_user
			INNER JOIN db_retailnet.roles ON db_retailnet.roles.role_id = user_role_role
			INNER JOIN db_retailnet.role_permissions ON role_permission_role = user_role_role
			INNER JOIN db_retailnet.permissions ON permission_id = role_permission_permission
			WHERE db_retailnet.users.user_active = 1 
			AND (
				permissions.permission_name = 'can_edit_all_news_articles' 
				OR permissions.permission_name = 'can_edit_only_his_news_articles'
			)
			ORDER BY name
		")->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$author = $row['id'];
				$authors['retailnet']['name'] = 'Retail Net Authors';
				$authors['retailnet']['authors'][$author]['name'] = $row['name'];
			}
		}

		// get news authors
		$result = $model->query("
			SELECT DISTINCT 
				news_author_id AS id, 
				CONCAT(news_author_first_name, ' ', news_author_name) AS name
			FROM news_authors
			WHERE news_author_active = 1 OR news_author_id = '$newsAuthor'
			ORDER BY name
		")->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$author = 'n'.$row['id'];
				$authors['news']['name'] = 'News Authors';
				$authors['news']['authors'][$author]['name'] = $row['name'];
			}
		}
		

		// get user conrimators 
		$result = $model->query("
			SELECT DISTINCT 
				db_retailnet.users.user_id AS id, 
				CONCAT(db_retailnet.users.user_firstname, ' ', db_retailnet.users.user_name) AS name
			FROM db_retailnet.users 
			INNER JOIN db_retailnet.user_roles ON db_retailnet.users.user_id = db_retailnet.user_roles.user_role_user
			INNER JOIN db_retailnet.roles ON db_retailnet.roles.role_id = user_role_role
			INNER JOIN db_retailnet.role_permissions ON role_permission_role = user_role_role
			INNER JOIN db_retailnet.permissions ON permission_id = role_permission_permission
			WHERE db_retailnet.users.user_active = 1 AND permissions.permission_name = 'can_confirm_news_article'
			ORDER BY name
		")->fetchAll();

		$confirmators = _array::extract($result);

		// get company restrictions
		$result = $model->query("
			SELECT DISTINCT 
				db_retailnet.addresses.address_id AS company, 
				CONCAT(db_retailnet.countries.country_name, ', ', db_retailnet.addresses.address_company) AS name,
				IF (address_client_type=2, 'group-subsidiary', '') AS subsidiary,
				IF (address_client_type=1, 'group-agent', '') AS agent,
				IF (address_is_a_hq_address=1, 'group-hq', '') AS hq,
				salesregions.salesregion_id AS region,
				IF (db_retailnet.countries.country_salesregion>0, salesregions.salesregion_name, 'Other') AS region_name,
				IF((
					SELECT COUNT(DISTINCT news_article_address_id) as total
					FROM db_retailnet_news.news_article_addresses
					WHERE news_article_address_address_id = address_id AND news_article_address_article_id = $id
				), 'checked=checked', '') AS checked
			FROM db_retailnet.addresses
				INNER JOIN db_retailnet.countries ON country_id = address_country
				LEFT JOIN db_retailnet.salesregions ON salesregion_id = db_retailnet.countries.country_salesregion
				LEFT JOIN db_retailnet_news.news_article_addresses ON news_article_address_address_id = address_id
			WHERE (address_active = 1 AND (address_type = 1 OR address_is_a_hq_address = 1)) 
			OR (
				address_id = news_article_address_address_id 
				AND news_article_address_article_id = $id
			)
			ORDER BY region_name, name
		")->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				
				$region = $row['region'] ?: 0;
				$company = $row['company'];
				$companies[$region]['region'] = $row['region_name'];

				$classes = join(' ', array_filter(array(
					$row['subsidiary'], 
					$row['agent'], 
					$row['hq'], 
					"group-region-$region"
				)));
				
				$companies[$region]['companies'][$company] = array(
					'name' => $row['name'],
					'classes' => $classes,
					'checked' => $row['checked']
				);
			}
		}

		// get access controll
		$roles = $model->query("
			SELECT DISTINCT 
				roles.role_id AS id, 
				roles.role_name AS name,
				IF((
					SELECT COUNT(DISTINCT news_article_role_id) as total
					FROM db_retailnet_news.news_article_roles
					WHERE news_article_role_role_id = role_id AND news_article_role_article_id = $id
				), 'checked=checked', '') AS checked
			FROM db_retailnet.users 
			INNER JOIN db_retailnet.user_roles ON users.user_id = user_roles.user_role_user
			INNER JOIN db_retailnet.roles ON user_roles.user_role_role = roles.role_id
			INNER JOIN db_retailnet.role_permissions ON roles.role_id = role_permissions.role_permission_role
			INNER JOIN db_retailnet.permissions ON role_permissions.role_permission_permission = permissions.permission_id
			LEFT JOIN db_retailnet_news.news_article_roles ON news_article_role_role_id = role_id
			WHERE TRIM(permissions.permission_name) = 'can_read_news' 
			OR (
				role_id = news_article_role_role_id
				AND news_article_role_article_id = $id
			)
			ORDER BY roles.role_name ASC
		")->fetchAll(); 

		$stickers = $model->query("
			SELECT
				sticker_id AS id,
				sticker_title AS title,
				sticker_image AS image
			FROM stickers
			ORDER BY date_created DESC
		")->fetchAll(); 

		// view template
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'news/article/form.php');
		$tpl->data('data', $data);
		$tpl->data('sections', $sections);
		$tpl->data('authors', $authors);
		$tpl->data('confirmators', $confirmators);
		$tpl->data('states', $states);
		$tpl->data('roles', $roles);
		$tpl->data('companies', $companies);
		$tpl->data('stickers', $stickers);
		$this->view->setTemplate('articles', $tpl);

		Compiler::attach(array(
			'/public/css/switch.css',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/scripts/datapicker/css/bootstrap-select.min.css',
			'/public/scripts/select/css/bootstrap-select.min.css',
			'/public/scripts/select/js/bootstrap-select.min.js',
			'/public/scripts/croppic/croppic.css',
			'/public/scripts/croppic/croppic.js',
			'/public/scripts/handsontable/handsontable.full.min.css',
			'/public/scripts/handsontable/handsontable.full.min.js',
			'/public/scripts/bsdialog/bsdialog.min.css',
			'/public/scripts/bsdialog/bsdialog.min.js',
			'/public/themes/swatch/css/template.icons.css',
			'/public/themes/swatch/css/news.article.css',
			'/public/themes/swatch/css/sticker.css',
			'/public/scripts/sticky/jquery.sticky.js',
			'/public/js/news.article.js'
		));
	}


	public function preview() {

		$application = $this->request->application;
		$controller = $this->request->controller;
		$archived = $this->request->archived;
		$action = $this->request->controller;
		$id = url::param();
		$user = User::instance();

		$_CAN_VIEW = static::$permissions['view.all'] || static::$permissions['view.his'] ? true : false;
		$_CAN_EDIT = static::$permissions['edit.all'] || static::$permissions['edit.his'] ? true : false;
		$_ACCESS_FULL = static::$permissions['view.all'] || static::$permissions['edit.all'] ? true : false;
		$_ACCESS_LIMITED = !$_ACCESS_FULL && (static::$permissions['view.his'] || static::$permissions['edit.his']) ? true : false;

		// read article
		$article = new Modul($application);
		$article->setTable('news_articles');
		$article->read($id);

		// no right to access or to view article
		if ( (!$_CAN_VIEW && !$_CAN_EDIT) || !$article->id ) { 
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$model = new Model($application);

		$this->view->master('news.portal.php');
		$this->view->pageclass = 'theme-news page-news-preview';
		$this->view->pagetitle = Request::instance()->title;
		$this->view->areticle = $id;
		$this->view->logoLink = "/gazette/newsletters/preview/$id";


		$this->view->preview = true;
		$this->view->article = $article->id;

		$stickers = $model->query("
			SELECT
				sticker_id AS id,
				sticker_title AS title,
				sticker_image AS image
			FROM stickers
			ORDER BY date_created DESC
		")->fetchAll(); 

		$this->view->stickers = $stickers;

		Compiler::attach(array(
			'/public/themes/swatch/css/news.portal.css',
			'/public/css/news.content.css',
			'/public/scripts/sticky/jquery.sticky.js',
			'/public/js/news.portal.js',
			'/public/themes/swatch/css/sticker.css',
			'/public/js/article.sticker.js'
		));

	}
}