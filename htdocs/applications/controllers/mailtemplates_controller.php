<?php 

	class MailTemplates_Controller {
		
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			// request form: add link
			$field_add = $this->request->query('add');
			$this->request->field('add', $field_add);

			// request form: edit link
			$field_edit = $this->request->query('data');
			$this->request->field('form', $field_edit);
	
			// template: mail templates list
			$this->view->mailTemplates('pagecontent')->mail('template.list');
		}
		
		public function add() { 
			
			// permissions
			$permission_edit = user::permission(Mail_Template::PERMISSION_EDIT);
			
			if(!$permission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");

			$model = new Model();
			
			// form data loader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;

			// dropdown applications
			$result = $model->query("
				SELECT DISTINCT
					application_id,
					application_name
				FROM db_retailnet.applications
				ORDER BY application_name
			")->fetchAll();

			$dataloader['mail_template_application_id'] = _array::extract($result);
			
			// template: mail template form
			$this->view->mailTemplateForm('pagecontent')
			->mail('template.data')
			->data('buttons', $buttons)
			->data('dataloader', $dataloader)
			->data('data', $data);
		}
		
		public function data() {
			
			$id = url::param();

			$model = new Model();
			
			// mail template
			$mailTemplate = new Mail_Template();
			$mailTemplate->read($id);
			
			if (!$mailTemplate->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// permissions
			$permission_edit = user::permission(Mail_Template::PERMISSION_EDIT);
			
			// form datalaoder
			$data = $mailTemplate->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// button back
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// button: save
			if ($permission_edit) {
				
				$buttons['save'] = true;
				
				// db integrity
				$integrity = new Integrity();
				$integrity->set($id, 'mail_templates');
				
				// button delete
				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/mail.template.delete.php', array('id'=>$id));
				}
			}
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}

			// dropdown applications
			$result = $model->query("
				SELECT DISTINCT
					application_id,
					application_name
				FROM db_retailnet.applications
				ORDER BY application_name
			")->fetchAll();

			$dataloader['mail_template_application_id'] = _array::extract($result);
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $mailTemplate->code)
			->data('subtitle', $mailTemplate->purpose);

			// template: navigations tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');

			// template: mail template form
			$this->view->mailTemplateForm('pagecontent')
			->mail('template.data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function settings() {
			
			$id = url::param();
			
			// mail template
			$mailTemplate = new Mail_Template();
			$mailTemplate->read($id);
			
			if (!$mailTemplate->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// db model
			$model = new Model(Connector::DB_CORE);
			
			// permissions
			$permission_edit = user::permission(Mail_Template::PERMISSION_EDIT);
			
			// form datalaoder
			$data = $mailTemplate->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// button back
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// button: save
			if ($permission_edit) {
				$buttons['save'] = true;
			}
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}
			
			// standard recipients
			$result = $model->query("
				SELECT DISTINCT 
					user_id,
					CONCAT(user_firstname, ' ', user_name) as name
				FROM users
				WHERE user_active = 1 AND user_address = 13
				ORDER BY user_firstname, user_name
			")->fetchAll();
			
			$swatchUsers = _array::extract($result);
			
			$properties = array(
				'value' => $mailTemplate->sender_id,
				'id' => 'mail_template_standard_sender_select',
				'name' => 'mail_template_standard_sender_select'
			);
			
			if (!$mailTemplate->sender_id) {
				$properties['disabled'] = 'disabled';
			} else {
				$data['mail_template_sender_id'] = 1;
			}
			
			//dataloader: standard sender
			$dataloader['mail_template_sender_id'] = array(
				0 => 'Authenticated User',
				1 => html::select($swatchUsers, $properties)
			);
			
			// dataloader: action file
			$result = $model->query("
				SELECT 
					mail_template_action_id,
					mail_template_action_name
				FROM mail_template_actions
				ORDER BY mail_template_action_name		
			")->fetchAll();
			
			$dataloader['mail_template_action_id'] = _array::extract($result);
			
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $mailTemplate->code)
			->data('subtitle', $mailTemplate->purpose);

			// template: navigations tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');

			// template: mail template form
			$this->view->mailTemplateForm('pagecontent')
			->mail('template.settings')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function recipients() {
		
			$id = url::param();
			
			// mail template
			$mailTemplate = new Mail_Template();
			$mailTemplate->read($id);
			
			if (!$mailTemplate->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			$model = new Model(Connector::DB_CORE);
			
			// form datalaoder
			$data = $mailTemplate->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// button back
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// get selected users
			$result = $model->query("
				SELECT 
					mail_template_recipient_user_id AS user,
					mail_template_recipient_cc AS cc
				FROM mail_template_recipients
				WHERE mail_template_recipient_template_id = $id		
			")->fetchAll();

			if ($result) {
				
				foreach ($result as $row) {
					
					$user = $row['user'];
					
					if ($row['cc']) {
						$dataloader['cc'][$user] = true;
					} else {
						$dataloader['standard'][$user] = true;
					}
				}
			}
			
			// companis
			$result = $model->query("
				SELECT DISTINCT
					user_id,
					address_id,
					CONCAT(address_company, ', ', country_name) as company,
					CONCAT(user_firstname, ' ', user_name) as recipient
				FROM users
				INNER JOIN addresses ON address_id = user_address
				INNER JOIN countries ON country_id = address_country
				WHERE user_active = 1 AND (address_is_a_hq_address = 1 or address_is_internal_address = 1 )	
				ORDER BY address_company, country_name, user_firstname
			")->fetchAll();
			
			if ($result) {
				
				foreach ($result as $row) {
					
					$company = $row['address_id'];
					$recipient = $row['user_id'];
					
					$companies[$company]['company'] = $row['company'];
					
					$companies[$company]['recipients'][$recipient] = array(
						'standard' => $dataloader['standard'][$recipient],
						'cc' => $dataloader['cc'][$recipient],
						'recipient' => 	$row['recipient']
					);
				}
			}
	
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $mailTemplate->code)
			->data('subtitle', $mailTemplate->purpose);

			// template: navigations tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');

			$this->view->mailTemplateRecipients('pagecontent')
			->mail('template.recipients')
			->data('template', $id)
			->data('companies', $companies)
			->data('buttons', $buttons)
			->data('id', $id); 
		}
		
		public function roles() {
		
			$id = url::param();
			
			// mail template
			$mailTemplate = new Mail_Template();
			$mailTemplate->read($id);
			
			if (!$mailTemplate->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			$model = new Model(Connector::DB_CORE);

			// button back
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// get selected users
			$result = $model->query("
				SELECT
					mail_template_role_role_id AS role,
					mail_template_role_cc AS cc
				FROM mail_template_roles
				WHERE mail_template_role_template_id = $id		
			")->fetchAll();

			if ($result) {
				
				foreach ($result as $row) {
					
					$role = $row['role'];
					
					if ($row['cc']) {
						$dataloader['cc'][$role] = true;
					} else {
						$dataloader['recipient'][$role] = true;
					}
				}
			}

			// get selected users
			$result = $model->query("
				SELECT
					mail_template_other_recipient_name AS name,
					mail_template_other_recipient_value AS value
				FROM mail_template_other_recipients
				WHERE mail_template_other_recipient_template_id = $id		
			")->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$key = $row['name'];
					if (substr($key, -3)=='_cc') $dataloaderOthers['other_recipient_cc'][substr($key, 0, -3)] = $row['value'];
					else $dataloaderOthers['other_recipient'][$key] = $row['value'];
				}
			}
			
			$result = $model->query("
				SELECT DISTINCT
					role_id, 
					role_name, 
					IF (
						role_application IS NOT NULL, 
						IF (application_id, application_name, 'Retail Development'),
						'Uncategorized'
					) AS application_name
				FROM roles
				LEFT JOIN applications ON application_shortcut = role_application
				ORDER BY application_name, role_name			
			")->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$app = $row['application_name'];
					$roles[$app][$row['role_id']] = $row;
				}
			}

			$others = array();
			$others['regional_responsible'] = array(
				'other_name' => 'Regional Responsible'
			);

			//echo "<pre>"; print_r($roles); print_r($dataloader);  print_r($others); print_r($dataloaderOthers); die;
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $mailTemplate->code)
			->data('subtitle', $mailTemplate->purpose);

			// template: navigations tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');

			$this->view->mailTemplateRecipients('pagecontent')
			->mail('template.roles')
			->data('template', $id)
			->data('dataloader', $dataloader)
			->data('dataloaderOthers', $dataloaderOthers)
			->data('roles', $roles)
			->data('others', $others)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function filters() {
			
			$template = url::param();
			$filter = url::param(1);
			
			// mail template
			$mailTemplate = new Mail_Template();
			$mailTemplate->read($template);
			
			if (!$mailTemplate->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $mailTemplate->code)
			->data('subtitle', $mailTemplate->purpose);

			// template: navigations tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
		
			// button back
			$buttons = array();
			
			if ($filter) {
				
				$buttons['back'] = $this->request->query("filters/$template");
				$buttons['save'] = true;
				
				$filter = (is_numeric($filter)) ? $filter : null;
				
				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
				Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");

				if ($filter) {
					
					// DB model
					$model = new Model(Connector::DB_CORE);
					
					$data = $model->query("
						SELECT *
						FROM mail_template_filters
						WHERE mail_template_filter_id = $filter		
					")->fetch();
				}

				// template data
				$data['mail_template_filter_template_id'] = $template;
				
				$this->view->filterForm('pagecontent')
				->mail('template.filter.form')
				->data('data', $data)
				->data('buttons', $buttons);
								
			} else {
			
				Compiler::attach(DIR_SCRIPTS."table.loader.js");
				
				// request field: master sheet
				$this->request->field('template', $template);
				$this->request->field('add', $this->request->query("filters/$template/add"));
				
				$buttons['back'] = $this->request->query();
	
				// template: mail template form
				$this->view->filterList('pagecontent')
				->mail('template.filters')
				->data('buttons', $buttons);
			}
		}
	}