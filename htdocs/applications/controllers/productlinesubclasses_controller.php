<?php 

	class ProductLineSubclasses_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() { 
			

			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			if ($permission_edit) {
				$this->request->field('add',$this->request->query('add'));
			}
			
			if ($permission_edit || $permission_view) {
				$this->request->field('data',$this->request->query('data'));
			}
			
			// template: role list
			$this->view->productLines('pagecontent')
			->product('line.subclass.list')
			->data('buttons', $buttons);
		}

		public function add() {

			$permission_edit = user::permission('can_edit_catalog');
		
			if($this->request->archived || !$permission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
				
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();

			$model = new Model(Connector::DB_CORE);

			// dataloader: product lines
			$result = $model->query("
				SELECT product_line_id, product_line_name
				FROM product_lines 
				WHERE product_line_active = 1
				ORDER BY product_line_name
			")->fetchAll();
			
			$dataloader['productline_subclass_productline'] = _array::extract($result);
		
			// template: application form
			$this->view->productLine('pagecontent')
			->product('line.subclass.form')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons);
		}
		
		public function data() {

			$id = url::param();

			$productLineSubclass = new Product_Line_Subclass();
			$productLineSubclass->read($id);

			if (!$productLineSubclass->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// dataloader
			$dataloader = array();

			$data = $productLineSubclass->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;

			$model = new Model(Connector::DB_CORE);

			// dataloader: product lines
			

			
			$selected_product_lines = array();
			$result = $model->query("
				SELECT productline_subclass_productline_line_id
				FROM productline_subclass_productlines
				WHERE productline_subclass_productline_class_id = $id
			")->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$serialize[] = $row['productline_subclass_productline_line_id'];
					$selected_product_lines[] = $row['productline_subclass_productline_line_id'];
				}
				$data['productline_subclass_productline'] = serialize($serialize);
			}

			//product lines
			$selected_product_line_filter = "";
			if(count($selected_product_lines) > 0)
			{
				$selected_product_line_filter = ' or product_line_id in (' . implode(',', $selected_product_lines). ')';
			}
			$result = $model->query("
				SELECT product_line_id, product_line_name
				FROM product_lines 
				WHERE (product_line_active = 1 $selected_product_line_filter)
				ORDER BY product_line_name
			")->fetchAll();
			
			$dataloader['productline_subclass_productline'] = _array::extract($result);

			

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			if ($permission_edit) {
			
				$buttons['save'] = true;

				$integrity = new Integrity();
				$integrity->set($id, 'productline_subclasses', 'system');

				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/product.line.subclass.delete.php', array('id'=>$id));
				}
			}
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $productLineSubclass->name);
			$this->view->companytabs('pagecontent')->navigation('tab');
		
			// template: form
			$this->view->productLine('pagecontent')
			->product('line.subclass.form')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons);
		}
	}