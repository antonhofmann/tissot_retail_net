<?php 

	class Navigations_Controller {
		
		public function __construct() {
				
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
			Compiler::attach(DIR_CSS."jquery.ui.css");
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
			Compiler::attach(DIR_SCRIPTS."dynatree/src/skin/ui.dynatree.css");
			Compiler::attach(DIR_SCRIPTS."dynatree/src/jquery.dynatree.js");
			
			$requestFields = array();
			$requestFields['add'] = $this->request->query('add');
			$requestFields['form'] = $this->request->query('form');
		
			$this->view->navigationList('pagecontent')
			->navigation('list')
			->data('requestFields', $requestFields);
		}
		
		public function add() {
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."pop/pop.css");
			Compiler::attach(DIR_SCRIPTS."pop/pop.js");
			
			// form dataloader
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			
			// set default language english caption
			$language = new Language();
			$language->read(36);
			$data['navigation_language'] = $language->name;
			
			// dataloader: button parent
			$data['select_navigation_parent'] = "
				<span id=parentTrigger class='button' data='#menutree' >
					<span class='icon icon140'></span>
					<span class=label>Select Parent</span>
				</span>
			"; 
			
			// buttons
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
			
			// dataloader: applications
			$dataloader['navigation_application'] = Application::dropdown();
			$dataloader['navigation_controller'] = Navigation_Controller::dropdown();
			$dataloader['navigation_language'] = Language::loader();
			
			$menu = new menu();
			$tree = $menu->tree();
			
			$this->view->navigationForm('pagecontent')
			->navigation('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons)
			->data('menutree', $tree);
		}
		
		public function data() {
			
			$id = url::param();
			
			$navigation = new Navigation();
			$navigation->read($id);
			
			// check access to navigation
			if (!$navigation->id) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			Session::set('selected-navigation', $id);
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."pop/pop.css");
			Compiler::attach(DIR_SCRIPTS."pop/pop.js");
			
			// form dataloader
			$data = $navigation->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
			$buttons['delete'] = $this->request->link('/applications/helpers/navigation.delete.php', array('id'=>$id));

			// dataloader: applications
			$dataloader['navigation_application'] = Application::dropdown();
			$dataloader['navigation_controller'] = Navigation_Controller::dropdown();
			$dataloader['navigation_language'] = Language::loader();
			
			// navigation language
			$navigation_language = new Navigation_Language();
			
			// navigation parent
			$navigation_language->read_from_navigation($navigation->parent,36);
			$btnParent = $navigation_language->name;
			
			// navigation caption
			$navigation_language->read_from_navigation($navigation->id,36);
			$data['navigation_caption'] = $navigation_language->name;
				
			// set default language english caption
			$language = new Language();
			$language->read(36);
			$data['navigation_language'] = $language->name;
			
			// navigation url
			$url = explode('/',$navigation->url);

			// application
			$app = $url ? array_shift($url) : null; 
			$data['navigation_application'] = Application::is($app) ? $app : null;

			// controller
			$controller = $data['navigation_application'] ? array_shift($url) : $app;

			if (count($url)==1 && $app==$controller) {
				$controller = null;
			}
			
			if (file::isController($controller)) $data['navigation_controller'] = $controller;
			else $data['navigation_action'] = $controller;

			// action
			if (!$data['navigation_action']) {
				$action = array_shift($url);
				$archived = $action=='archived' ? 1 : null;
				$data['navigation_action'] = $archived ? array_shift($url) : $action;
				$data['navigation_archived'] = $archived;
			}

			// dataloader: button parent
			$data['select_navigation_parent'] = "
				<span id=parentTrigger class='button' data='#menutree' >
					<span class='icon icon140'></span>
					<span class=label>$btnParent</span>
				</span>
			";
			
			$menu = new menu();
			$tree = $menu->tree();
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $navigation_language->name);
			
			// template: tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			// template: navigation form	
			$this->view->navigationForm('pagecontent')
			->navigation('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons)
			->data('menutree', $tree)
			->data('id', $id);
		}
	}