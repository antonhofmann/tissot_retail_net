<?php

	class Projects_Controller {

		public function __construct() {
	
			$this->request = request::instance();
	
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}

		public function index() {
		
			switch ($this->request->application) {
	
				case 'red':
					
					// permissions
					$permissions_add_new = user::permission(Red_Project::PERMISSION_ADD_NEW_PROJECTS);
					$permission_view = user::permission(Red_Project::PERMISSION_VIEW_PROJECTS);
					$permission_view_limited = user::permission(Red_Project::PERMISSION_VIEW_LIMITED_PROJECTS);
					
					// request form: add button
					if (!$this->request->archived && $permissions_add_new) {
						$field = $this->request->query('add');
						$this->request->field('add',$field);
					}
						
					// request form: link to basic data
					$field = $this->request->query('data');
					$this->request->field('form', $field);
					
					// button: print
					if ($permission_view) {
						$field = $this->request->link('/applications/exports/excel/red_projects.php');
						$this->request->field('print', $field);
					}
			
					// template: projects list
					$this->view->projectsList('pagecontent')
					->red('project.list')
					->data('buttons', $buttons)
					->data('view', $view)
					->data('mode', 'active');
			
				break;
			}
		}

		public function add() {

			switch ($this->request->application) {
				
				case 'red':
					
					// permissions
					$permissions_add_new = user::permission(Red_Project::PERMISSION_ADD_NEW_PROJECTS);
					$permission_view = user::permission(Red_Project::PERMISSION_VIEW_PROJECTS);
					$permission_view_limited = user::permission(Red_Project::PERMISSION_VIEW_LIMITED_PROJECTS);
					
					$model = new Model($this->request->application);

					if ($this->request->archived || !$permissions_add_new) {
						message::access_denied();
						url::redirect($this->request->query());
					}
					
					// form dataloader
					$data = array();
					$data['application'] = $this->request->application;
					$data['redirect'] = $this->request->query('data');
					
					// buttons
					$button = array();
					$buttons['back'] = $this->request->query();
					$buttons['save'] = true;

					// generate new project name
					$project = new Red_Project();
					$data['red_project_projectnumber'] = $project->generateNumber();
					
					
					// dataloader: project types
					$connector = $this->request->application;
					$dataloader['red_project_projecttype_id'] = Red_Project_Type::dropdown_loader($connector);

					
					// dataloader: project states
					$connector = $this->request->application;
					$dataloader['red_project_projectstate_id'] = Red_Project_State::dropdown_loader($connector);
					
					// get leaders and assistances
					$model = new Model(Connector::DB_CORE);
					$result = $model->query("
						SELECT
							user_id,
							CONCAT(user_name, ' ', user_firstname)
						FROM users
					")
					->bind('LEFT JOIN user_roles ON user_role_user = user_id')
					->filter('user_active', 'user_active = 1')
					->filter('user_role', 'user_role_role IN (48,49)')
					->fetchAll();
					
					// dataloader: leaders and assitances
					$dataloader['red_project_leader_user_id'] = _array::extract($result);
					$dataloader['red_project_assistant_user_id'] = _array::extract($result);

					// template: project form
					$this->view->projectsForm('pagecontent')
					->red('project.form')
					->data('data', $data)
					->data('dataloader', $dataloader)
					->data('buttons', $buttons);
					
				break;
			}
		}

		public function data() {

			switch ($this->request->application) {

				case 'red':
					
					$id = url::param();
					
					// permissions
					$permission_view = user::permission(Red_Project::PERMISSION_VIEW_PROJECTS);
					$permission_view_limited = user::permission(Red_Project::PERMISSION_VIEW_LIMITED_PROJECTS);
					$permission_edit_basic_data = user::permission(Red_Project::PERMISSION_EDIT_BASIC_DATA);
					$permission_edit_project_sheet = user::permission(Red_Project::PERMISSION_EDIT_PROJECT_SHEET);
					$permission_view_partner_list = user::permission(Red_Project::PERMISSION_VIEW_PARTNERLIST);
					$permission_edit_partner_list = user::permission(Red_Project::PERMISSION_EDIT_PARTNERLIST);
					$permission_delete = user::permission(Red_Project::PERMISSION_DELETE_DATA);

					// load projects data
					$project = new Red_Project();
					$project->read($id);
					
					// project archived
					$project_archived = $project->is_archive();
					
					if (!$project->id || (!$permission_view && !$permission_view_limited)) {
						message::access_denied();
						url::redirect($this->request->query());
					}
					// if project is archived
					elseif (!$this->request->archived && $project_archived) {
						url::redirect($this->request->query("archived/data/$id"));
					}
					
					// db model
					$model = new Model(Connector::DB_CORE);
					
					// exclude project sheet tab
					if (!$permission_edit_project_sheet) {
						$this->request->exclude("red/projects/sheet");
					}
					
					// exclude tab: project partner list
					if (!$permission_view_partner_list && !$permission_edit_partner_list) {
						$this->request->exclude("red/projects/partners");
					}
					
					// form dataloader
					$data = $project->data;
					$data['application'] = $this->request->application;
					$data['controller'] = $this->request->controller;
						
					// buttons
					$button = array();
					$buttons['back'] = $this->request->query();

					// button: print
					if ($permission_edit_basic_data) {
						$buttons['print'] = $this->request->link('/applications/exports/pdf/red_projects.booklet.php', array('id'=>$id));
					}

					// button: save
					if (!$this->request->archived && $permission_edit_basic_data) {
						
						$buttons['save'] = true;
						
						// button: delete
						if ($permission_delete) {
						
							$integrity = new Integrity();
							$integrity->set($id, 'red_projects');
						
							if ($integrity->check()) {
								$buttons['delete'] = $this->request->link('/applications/helpers/red.project.delete.php', array('id' => $id));
							}
						}
					}
					elseif ($data) {
						
						foreach ($data as $field => $value) {
							$disabled[$field] = true;
						}
					}

					// enable save button for archive
					if ($this->request->archived && $permission_edit_basic_data) {
						$disabled['red_project_enddate'] = false;
						$buttons['save'] = true;
					}
					
					// dataloader: project types
					$connector = $this->request->application;
					$dataloader['red_project_projecttype_id'] = Red_Project_Type::dropdown_loader($connector);
					
					// dataloader: project states
					$connector = $this->request->application;
					$dataloader['red_project_projectstate_id'] = Red_Project_State::dropdown_loader($connector);
						
					// get project leaders
					$result = $model->query("
						SELECT
							user_id,
							CONCAT(user_name, ' ', user_firstname)
						FROM users
						LEFT JOIN user_roles ON user_role_user = user_id
					")
					->filter('user_active', '(user_active = 1 OR user_id = '.$project->leader_user_id.')')
					->filter('user_role', 'user_role_role IN (48,49)')
					->fetchAll();
						
					// dataloader: project leaders
					$dataloader['red_project_leader_user_id'] = _array::extract($result);
						
					// get project assistant
					$result = $model->query("
						SELECT
							user_id,
							CONCAT(user_name, ' ', user_firstname)
						FROM users
						LEFT JOIN user_roles ON user_role_user = user_id
					")
					->filter('user_active', '(user_active = 1 OR user_id = '.$project->assistant_user_id.')')
					->filter('user_role', 'user_role_role IN (48,49)' )
					->fetchAll();
						
					// dataloader: project assistants
					$dataloader['red_project_assistant_user_id'] = _array::extract($result);

					// template: header
					$this->view->header('pagecontent')
					->node('header')
					->data('title', $project->title);

					// template: navigation tabs
					$this->view->tabs('pagecontent')
					->navigation('tab');

					$this->view->projectsForm('pagecontent')
					->red('project.form')
					->data('data', $data)
					->data('dataloader', $dataloader)
					->data('disabled', $disabled)
					->data('buttons', $buttons)
					->data('id', $id);
					
				break;
			}
		}

		public function description() {
			
			switch ($this->request->application) {
			
				case 'red':
					
					$id = url::param();
					
					// permissions
					$permission_view = user::permission(Red_Project::PERMISSION_VIEW_PROJECTS);
					$permission_view_limited = user::permission(Red_Project::PERMISSION_VIEW_LIMITED_PROJECTS);
					$permission_edit_basic_data = user::permission(Red_Project::PERMISSION_EDIT_BASIC_DATA);
					$permission_edit_project_sheet = user::permission(Red_Project::PERMISSION_EDIT_PROJECT_SHEET);
					$permission_view_partner_list = user::permission(Red_Project::PERMISSION_VIEW_PARTNERLIST);
					$permission_edit_partner_list = user::permission(Red_Project::PERMISSION_EDIT_PARTNERLIST);
					$permission_delete = user::permission(Red_Project::PERMISSION_DELETE_DATA);
					$permission_edit_descriptions = user::permission(Red_Project::PERMISSION_EDIT_DESCRIPTION);
					
					// load projects data
					$project = new Red_Project();
					$project->read($id);
					
					// project archived
					$project_archived = $project->is_archive();
						
					if (!$project->id || (!$permission_view && !$permission_view_limited)) {
						message::access_denied();
						url::redirect($this->request->query());
					}
					// project is archived
					elseif (!$this->request->archived && $project_archived) {
						url::redirect($this->request->query("archived/description/$id"));
					}
					
					// exclude project sheet tab
					if (!$permission_edit_project_sheet) {
						$this->request->exclude("red/projects/sheet");
					}
						
					// exclude tab: project partner list
					if (!$permission_view_partner_list && !$permission_edit_partner_list) {
						$this->request->exclude("red/projects/partners");
					}
						
					// form dataloader
					$data = $project->data;
					$data['application'] = $this->request->application;
					$data['controller'] = $this->request->controller;
					
					// buttons
					$button = array();
					$buttons['back'] = $this->request->query();
					
					// button: save
					if (!$this->request->archived && $permission_edit_descriptions) {
						$buttons['save'] = true;
					}
					elseif ($data) {
						foreach ($data as $field => $value) {
							$disabled[$field] = true;
						}
					}
					
					// template: header
					$this->view->header('pagecontent')
					->node('header')
					->data('title', $project->title);

					// template: navigation tabs
					$this->view->tabs('pagecontent')
					->navigation('tab');
					
					$this->view->projectDescriptionForm('pagecontent')
					->red('project.description')
					->data('data', $data)
					->data('disabled', $disabled)
					->data('buttons', $buttons);
				
				break;
			}
		}

		public function partners() {
			
			switch ($this->request->application) {
			
				case 'red':
			
					$id = url::param();
					$param = url::param(1);
						
					// permissions
					$permission_project_view = user::permission(Red_Project::PERMISSION_VIEW_PROJECTS);
					$permission_project_view_limited = user::permission(Red_Project::PERMISSION_VIEW_LIMITED_PROJECTS);
					$permission_project_delete = user::permission(Red_Project::PERMISSION_DELETE_DATA);
					$permission_partner_list_view = user::permission(Red_Project::PERMISSION_VIEW_PARTNERLIST);
					$permission_partner_list_edit = user::permission(Red_Project::PERMISSION_EDIT_PARTNERLIST);
					
					// load projects data
					$project = new Red_Project();
					$project->read($id);
					
					// project archived
					$project_archived = $project->is_archive();
					
					if (!$project->id || (!$permission_project_view && !$permission_project_view_limited && !$permission_partner_list_view)) {
						message::access_denied();
						url::redirect($this->request->query());
					}
					// project is archived
					elseif (!$this->request->archived && $project_archived) {
						url::redirect($this->request->query("archived/partners/$id"));
					}
						
					// exclude project sheet tab
					if (!user::permission(Red_Project::PERMISSION_EDIT_PROJECT_SHEET)) {
						$this->request->exclude("red/projects/sheet");
					}
					
					// db model
					$model = new Model(Connector::DB_CORE);
					
					// buttons
					$button = array();
					
					// template: header
					$this->view->header('pagecontent')
					->node('header')
					->data('title', $project->title);

					// template: navigation tabs
					$this->view->tabs('pagecontent')
					->navigation('tab');

					
					// project partner form
					if ($param) {
						
						$param = (is_numeric($param)) ? $param : null;
						
						// button: back
						$buttons['back'] = $this->request->query("partners/$id");
						
						// get companies involved in red
						$result = $model->query("
							SELECT DISTINCT
								address_id,
								address_company
							FROM addresses
						")
						->bind(Red_Project::BIND_ADDRESS_TYPE)
						->filter('active', 'address_active = 1')
						->filter('application', 'address_involved_in_red = 1')
						->filter('type', 'address_type in (1,2,5,6,12)')
						->fetchAll();
						
						// dataloader: companies
						$dataloader['red_project_partner_address_id'] = _array::extract($result);

						// dataloader: partner types
						$connector = $this->request->application;
						$dataloader['red_project_partner_partnertype_id'] = Red_Partner_Type::dropdown_loader($connector);
						
						// edit partner
						if ($param) {
							
							$projectPartner = new Red_Project_Partner();
							$projectPartner->read($param);
							
							// form dataloader
							$data = $projectPartner->data;
							
							if (!$this->request->archived) {
								
								// button: save
								if ($permission_partner_list_edit) {
									$buttons['save'] = true;
								}
								
								// button: delete
								if ($permission_project_delete) {
									
									$integrity = new Integrity();
									$integrity->set($param, 'red_project_partners');
									
									if ($integrity->check()) {
										$buttons['delete'] = $this->request->link('/applications/helpers/red.project.partner.delete.php', array(
											'project' => $id,
											'partner' => $param		
										));
									}
								}
							}
							elseif ($data) {
							
								//disable all form fields
								foreach ($data as $field => $value) {
									$disabled[$field] = true;
								}
								
								// show user name
								$result = $model->query("
									SELECT CONCAT(user_name, ', ', user_firstname) as username
									FROM users
								")
								->filter('user', 'user_id = '.$projectPartner->user_id)
								->fetch();
								
								$data['red_project_partner_user_id'] = $result['username'];
							}
						}
						// add new parner
						elseif ($permission_partner_list_edit) {
							$buttons['save'] = true;
							$data['redirect'] = $this->request->query("partners/$id");
						} 
						// access denied, no edit permission
						else {
							message::access_denied();
							url::redirect($this->request->query());
						}
						
						// form project id
						$data['application'] = $this->request->application;
						$data['controller'] = $this->request->controller;
						$data['red_project_partner_project_id'] = $id;
						
						// template: project partner form
						$this->view->projectPartnerForm('pagecontent')
						->red('project.partner.form')
						->data('data', $data)
						->data('dataloader', $dataloader)
						->data('buttons', $buttons)
						->data('disabled', $disabled)
						->data('id', $id);
					}
					// project partner list
					else {
						
						// request form: add new button
						if (!$this->request->archived && $permission_partner_list_edit) {
							$field = $this->request->query("partners/$id/add");
							$this->request->field('add', $field);
						}
						
						// request form: show/edit partner data
						if ($permission_partner_list_view) {
							$field = $this->request->query("partners/$id");
							$this->request->field('form', $field);
						}
						
						// request form: project
						$this->request->field('id', $id);
						
						// button: back
						$buttons['back'] = $this->request->query();
						
						$this->view->projectPartnersList('pagecontent')
						->red('project.partner.list')
						->data('buttons', $buttons)
						->data('id', $id);
					}
										
				break;
			}
		}

		public function comments() {

			switch ($this->request->application) {
			
				case 'red':
					
					$id = url::param();
					$param = url::param(1);
						
					// permissions
					$permission_project_view = user::permission(Red_Project::PERMISSION_VIEW_PROJECTS);
					$permission_project_view_limited = user::permission(Red_Project::PERMISSION_VIEW_LIMITED_PROJECTS);
					$permission_project_delete = user::permission(Red_Project::PERMISSION_DELETE_DATA);
					$permission_comment_add = user::permission(Red_Project::PERMISSION_ADD_COMMENTS);
					$permission_comment_edit = user::permission(Red_Project::PERMISSION_EDIT_COMMENTS);
					
					// load projects data
					$project = new Red_Project();
					$project->read($id);
					
					// project archived
					$project_archived = $project->is_archive();
					
					if (!$project->id || (!$permission_project_view && !$permission_project_view_limited)) {
						message::access_denied();
						url::redirect($this->request->query());
					}
					// project is archived
					elseif (!$this->request->archived && $project_archived) {
						url::redirect($this->request->query("archived/comments/$id"));
					}
						
					// exclude project sheet tab
					if (!user::permission(Red_Project::PERMISSION_EDIT_PROJECT_SHEET)) {
						$this->request->exclude("red/projects/sheet");
					}
					
					// exclude tab: project partner list
					if (!user::permission(Red_Project::PERMISSION_VIEW_PARTNERLIST)) {
						$this->request->exclude("red/projects/partners");
					}
					
					// db model red
					$model = new Model(Connector::DB_RETAILNET_RED);
					
					// buttons
					$button = array();
					
					// current user
					$user = user::instance();
					
					// template: header
					$this->view->header('pagecontent')
					->node('header')
					->data('title', $project->title);

					// template: navigation tabs
					$this->view->tabs('pagecontent')
					->navigation('tab');
					
					// comment form
					if ($param) {
						
						$param = (is_numeric($param)) ? $param : null;
						
						// button: back
						$buttons['back'] = $this->request->query("comments/$id");
						
						// edit comment
						if ($param) {
							
							$projectComment = new Red_Comment();
							$projectComment->read($param);
							
							// form dataloader
							$data = $projectComment->data;
							
							// comment file id's
							$files = unserialize($projectComment->file_id); 
							$data['red_comment_file_id'] = (is_array($files)) ? join(',',$files) : null;
							
							// get comment access
							$result = $model->query("
								SELECT 
									red_comment_access_id,
									red_comment_access_project_partner_id
								FROM red_comment_accesses
							")
							->filter('comment', "red_comment_access_comment_id = $param")
							->fetchAll();
							
							// data: partners
							$data['partners'] = serialize(_array::extract($result));
							
							//user_can_view
							if ($permission_project_view || ($permission_project_view_limited && $projectComment->user_has_access($user->id))) {
								$user_can_view = true;
							}
							
							//user_can_edit
							if ($permission_comment_edit || $projectComment->user_is_owner($user->id)) {
								$user_can_edit = true;
							}
							
							// consequent actions
							if (!$user_can_view) {
								Message::access_denied();
								url::redirect($this->request->query("comments/$id"));
							}
							elseif ($this->request->archived || !$user_can_edit) { 
								
								foreach ($data as $field => $value) {
									$disabled[$field] = true;
								}
								
								$disabled['partners'] = true;
							}
							/*
							 * elseif ($user_can_view && !$user_can_edit) {
								$disabled['red_commentcategory_name'] = true;
								$disabled['red_comment_comment'] = true;
								$disabled['partners'] = true;
							}
							*/
							elseif ($user_can_view && $user_can_edit) {  
								
								$buttons['attach'] = true;
								$buttons['save'] = true;
								
								$integrity = new Integrity();
								$integrity->set($param, 'red_comments');
								
								if ($integrity->check()) {
									$buttons['delete'] = $this->request->link('/applications/helpers/red.project.comment.delete.php', array(
										'project' => $id,
										'comment' => $param		
									));
								}
							}
							
						}
						// add new for archive disabled
						elseif ($this->request->archived) {
							Message::access_denied();
							url::redirect($this->request->query("comments/$id"));
						}
						// add new comment
						elseif ($permission_comment_add) {
							
							$buttons['save'] = true;
							$buttons['attach'] = true;
							$data['redirect'] = $this->request->query("comments/$id");
							
							// prepare fields
							$data['red_comment_owner_user_id'] = $user->id;
							$data['red_file_owner_user_id'] = $user->id;
						}
						// access denied, no permission to add or edit
						else {
							message::access_denied();
							url::redirect($this->request->query());
						}
						
						// comment form dataloader
						$data['application'] = $this->request->application;
						$data['controller'] = $this->request->controller;
						$data['red_comment_project_id'] = $id;
						
						// file form dataloader
						$datafile = array();
						$datafile['red_file_project_id'] = $id;
						$datafile['red_file_owner_user_id'] = $user->id;
						$datafile['return_inserted_id'] = true;
						
						// get partners allocated to this project
						$result = $model->query("
							SELECT DISTINCT
								red_project_partner_id,
								CONCAT (address_company, ' (', user_name, ' ', user_firstname, ', ', red_partnertype_name, ')') as partner,
								red_project_partner_project_id,
								red_partnertype_name
							FROM red_project_partners
						")
						->bind(Red_Project::BIND_ADDRESSES)
						->bind(Red_Project::BIND_PARTNER_TYPE)
						->bind(Red_Project::BIND_USER_ID)
						->filter('project', "red_project_partner_project_id = $id")
						->fetchAll();
			
						// dataloader: partners
						$dataloader['partners'] = _array::extract($result);
						
						// hide partner list
						if (!user::permission(Red_Project::PERMISSION_VIEW_PARTNERLIST) ) {
							$hidden['partners'] = true;
						}
						
						// dataloader: comment categories
						$connector = $this->request->application;
						$dataloader['red_comment_category_id'] = Red_Comment_Category::dropdown_loader($connector);
						
						// dataloader: file categories
						$connector = $this->request->application;
						$dataloader['red_file_category_id'] = Red_File_Category::dropdown_loader($connector);
						
						// template: project partner form
						$this->view->projectPartnerForm('pagecontent')
						->red('project.comment.form')
						->data('data', $data)
						->data('datafile', $datafile)
						->data('dataloader', $dataloader)
						->data('buttons', $buttons)
						->data('disabled', $disabled)
						->data('id', $id);
					}
					// comments list
					else {
						
						// request form: add new button
						if (!$this->request->archived && $permission_comment_add) {
							$field = $this->request->query("comments/$id/add");
							$this->request->field('add', $field);
						}
						
						// request form: show/edit partner data
						if ($permission_project_view || $permission_project_view_limited) {
							$field = $this->request->query("comments/$id");
							$this->request->field('form', $field);
						}
						
						// request form: project
						$this->request->field('id', $id);
						
						// button: back
						$buttons['back'] = $this->request->query();
						
						$this->view->projectCommentsList('pagecontent')
						->red('project.comment.list')
						->data('buttons', $buttons)
						->data('id', $id);
					}
			
				break;
			
			}
		}

		public function files() {
			
			switch ($this->request->application) {
			
				case 'red':
					
					$id = url::param();
					$param = url::param(1);
						
					// permissions
					$permission_project_view = user::permission(Red_Project::PERMISSION_VIEW_PROJECTS);
					$permission_project_view_limited = user::permission(Red_Project::PERMISSION_VIEW_LIMITED_PROJECTS);
					$permission_project_delete = user::permission(Red_Project::PERMISSION_DELETE_DATA);
					$permission_file_add = user::permission(Red_Project::PERMISSION_ADD_FILES);
					$permission_file_edit = user::permission(Red_project::PERMISSION_EDIT_FILES);
					
					// load projects data
					$project = new Red_Project();
					$project->read($id);
					
					// project archived
					$project_archived = $project->is_archive();
					
					if (!$project->id || (!$permission_project_view && !$permission_project_view_limited)) {
						message::access_denied();
						url::redirect($this->request->query());
					}
					// project is archived
					elseif (!$this->request->archived && $project_archived) {
						url::redirect($this->request->query("archived/files/$id"));
					}
						
					// exclude project sheet tab
					if (!user::permission(Red_Project::PERMISSION_EDIT_PROJECT_SHEET)) {
						$this->request->exclude("red/projects/sheet");
					}
					
					// exclude tab: project partner list
					if (!user::permission(Red_Project::PERMISSION_VIEW_PARTNERLIST)) {
						$this->request->exclude("red/projects/partners");
					}
					
					// db model red
					$model = new Model(Connector::DB_RETAILNET_RED);
					
					// current user
					$user = user::instance();
					
					// buttons
					$button = array();
			
					// form hidden fields
					$hidden = array();
					
					// template: header
					$this->view->header('pagecontent')
					->node('header')
					->data('title', $project->title);

					// template: navigation tabs
					$this->view->tabs('pagecontent')
					->navigation('tab');
					
					// file form
					if ($param) {
						
						// is param file identificator
						$param = (is_numeric($param)) ? $param : null;
						
						// button: back
						$buttons['back'] = $this->request->query("files/$id");
						
						// partner list
						if (!user::permission(Red_Project::PERMISSION_VIEW_PARTNERLIST) && !user::permission(Red_Project::PERMISSION_EDIT_PARTNERLIST) ) {
							//$hidden['partners'] = true; 
						}
						
						// view/edit file form
						if ($param) {
							
							$projectFile = new Red_File();
							$projectFile->read($param);
							
							// form dataloader
							$data = $projectFile->data;
							
							// get all file accesses for this file
							$result = $model->query("
								SELECT
								 	red_file_access_id,
									red_file_access_project_partner_id
								FROM red_file_accesses
							")
							->filter('file_id', "red_file_access_file_id = $param")
							->fetchAll();
							
							// form dataloader: partners
							$data['partners'] = serialize(_array::extract($result));
							
							// user can view file
							if ( $permission_project_view || ($permission_project_view_limited && $projectFile->user_has_access($user->id)) ) {
								$user_can_view = true;
							}
							
							// user_can_edit file
							if ($permission_file_edit || $projectFile->user_is_owner($user->id)) {
								$user_can_edit = true;
							}
							
							if (!$user_can_view) {
								Message::access_denied();
								url::redirect($this->request->query("files/$id"));
							}
							elseif ($this->request->archived || !$user_can_edit) {
								
								foreach ($data as $field => $value) {
									$disabled[$field] = true;
								}
								
								$disabled['partners'] = true;
							}
							elseif ($user_can_edit) {
								
								$buttons['save'] = true;
								
								if ($permission_project_delete) {
									
									$integrity = new Integrity();
									$integrity->set($param, 'red_files');
									
									if ($integrity->check()) {
										$buttons['delete'] = $this->request->link('/applications/helpers/red.project.file.delete.php', array(
											'project' => $id,
											'file' => $param		
										));
									}
								}
							}
							
						} 
						// add new for archive is not permitted
						elseif ($this->request->archived) {
							Message::access_denied();
							url::redirect($this->request->query("files/$id"));
						}
						// add new fiel
						elseif ($permission_file_add) {
							$data['redirect'] = $this->request->query("files/$id");
							$data['red_file_owner_user_id'] = $user->id;
							$buttons['save'] = true;
						}
						else {
							message::access_denied();
							url::redirect($this->request->query());
						}
						
						$data['application'] = $this->request->application;
						$data['controller'] = $this->request->controller;
						$data['red_file_project_id'] = $id;
						
						// get partners allocated to this project
						$result = $model->query("
							SELECT DISTINCT
								red_project_partner_id,
								CONCAT (address_company, ' (', user_name, ' ', user_firstname, ', ', red_partnertype_name, ')') as partner,
								red_project_partner_project_id,
								red_partnertype_name
							FROM red_project_partners
						")
						->bind(Red_Project::BIND_ADDRESSES)
						->bind(Red_Project::BIND_PARTNER_TYPE)
						->bind(Red_Project::BIND_USER_ID)
						->filter('project', "red_project_partner_project_id = $id")
						->fetchAll();
			
						// dataloader: partners
						$dataloader['partners'] = _array::extract($result);
						
						// dataloader: file categories
						$connector = $this->request->application;
						$dataloader['red_file_category_id'] = Red_File_Category::dropdown_loader($connector);
						
						// template: project file form
						$this->view->projectFileForm('pagecontent')
						->red('project.file.form')
						->data('data', $data)
						->data('dataloader', $dataloader)
						->data('hidden', $hidden)
						->data('buttons', $buttons)
						->data('disabled', $disabled)
						->data('id', $id)
						->data('file', $param);
					}
					// files list
					else {
						
						// request form: add new button
						if (!$this->request->archived && $permission_file_add) {
							$field = $this->request->query("files/$id/add");
							$this->request->field('add', $field);
						}
						
						// request form: show/edit partner data
						if ($permission_project_view || $permission_project_view_limited) {
							$field = $this->request->query("files/$id");
							$this->request->field('form', $field);
						}
						
						// request form: project
						$this->request->field('id', $id);
						
						// button: back
						$buttons['back'] = $this->request->query();
						
						$this->view->projectCommentsList('pagecontent')
						->red('project.file.list')
						->data('buttons', $buttons)
						->data('id', $id);
					}
					
				break;
			}
		}
	
		public function sheet() {

			switch ($this->request->application) {
			
				case 'red':
					
					$id = url::param();
					$param = url::param(1);
						
					// permissions
					$permission_project_view = user::permission(Red_Project::PERMISSION_VIEW_PROJECTS);
					$permission_project_view_limited = user::permission(Red_Project::PERMISSION_VIEW_LIMITED_PROJECTS);
					$permission_project_delete = user::permission(Red_Project::PERMISSION_DELETE_DATA);
					$permission_file_add = user::permission(Red_Project::PERMISSION_ADD_FILES);
					$permission_sheet_edit = user::permission(Red_Project::PERMISSION_EDIT_PROJECT_SHEET);
					
					// load projects data
					$project = new Red_Project();
					$project->read($id);
					
					// project archived
					$project_archived = $project->is_archive();
					
					if (!$project->id || !$permission_sheet_edit || (!$permission_project_view && !$permission_project_view_limited)) {
						message::access_denied();
						url::redirect($this->request->query());
					}
					// project is archived
					elseif (!$this->request->archived && $project_archived) {
						url::redirect($this->request->query("archived/sheet/$id"));
					}
					
					// exclude tab: project partner list
					if (!user::permission(Red_Project::PERMISSION_VIEW_PARTNERLIST)) {
						$this->request->exclude("red/projects/partners");
					}
					
					// db model red
					$core = new Model(Connector::DB_CORE);
					$model = new Model(Connector::DB_RETAILNET_RED);
					
					// current user
					$user = user::instance();
					
					// buttons
					$buttons = array();
					$buttons['back'] = $this->request->query();

					// load projectsheet data
					$result = $model->query("
						SELECT SQL_CALC_FOUND_ROWS
						 	red_projectsheet_id,
							red_projectsheet_project_id
						FROM red_projectsheets
					")
					->filter("red_projectsheet_project_id = $id")
					->order('date_created', 'ASC')
					->fetchAll();
			
					$projectsheet_id = _array::extract_by_key($result, 'red_projectsheet_id');
			
					if (is_array($projectsheet_id)) {
						
						$param = array_shift(array_values($projectsheet_id));
						
						$projectsheet = new Red_Project_Sheet();
						$projectsheet->read($param);
						
						// form dataloader
						$data = $projectsheet->data;
						
						// button: print
						$buttons['sheet'] = $this->request->link('/applications/exports/pdf/red_projects.projectsheet.php', array('sheet_id'=>$id));
					}
					
					// form dataloader
					$data['application'] = $this->request->application;
					$data['controller'] = $this->request->controller;
					$data['red_projectsheet_project_id'] = $id;
					
					// button: save
					if (!$this->request->archived && $permission_sheet_edit) {
						$buttons['save'] = true;
					} else {
						foreach ($data as $field => $value) {
							$disabled[$field] = true;
						}
					}
					
					// button: delete
					if (!$this->request->archived && $permission_project_delete) {
						$buttons['delete'] = true;
					}
						
					// selected account number
					if (!$projectsheet->accountnumber_id) {
						
						$core = new Model(Connector::DB_CORE);
						
						$result = $core->query("
							SELECT account_number_id
							FROM account_numbers		
						")
						->filter('red', 'account_number_default_red = 1')
						->fetch();
					
						$data['red_projectsheet_accountnumber_id'] = $result['account_number_id'];
					} 
					
					// dataloader: currencies
					$dataloader['red_projectsheet_currency_id'] = Currency::loader();
					
					// business_units
					$result = $core->query("
						SELECT DISTINCT
							business_unit_id,
							business_unit_name
						FROM business_units
					")
					->filter('application', "business_unit_application IN ('all', 'red')")
					->order('business_unit_name')
					->fetchAll();
					
					// dataloader: business_units
					$dataloader['red_projectsheet_business_unit_id'] = _array::extract($result);
					
					// cost centers
					$result = $core->query("
						SELECT DISTINCT
							cost_center_id,
							cost_center_name
						FROM cost_centers
					")
					->order('cost_center_name')
					->fetchAll();
					
					// dataloader: cost centers
					$dataloader['red_projectsheet_costcenter_id'] = _array::extract($result);
					
					// account numbers
					$result = $core->query("
						SELECT DISTINCT
							account_number_id,
							account_number_name
						FROM account_numbers
					")
					->order('account_number_name')
					->fetchAll();

					// dataloader: account_numbers
					$dataloader['red_projectsheet_accountnumber_id'] = _array::extract($result);
					
					// project partners
					$result = $model->query("
						SELECT DISTINCT
							red_project_partner_id,
							CONCAT (address_company, ' (', user_name, ' ', user_firstname, ', ', red_partnertype_name, ')') as partner
						FROM red_project_partners
					")
					->bind(Red_project::BIND_ADDRESSES)
					->bind(Red_project::BIND_PARTNER_TYPE)
					->bind(Red_project::BIND_USER_ID)
					->filter('red_project_partner_project_id', 'red_project_partner_project_id = '.$data['red_project_id'])
					->order('address_company')
					->fetchAll();

					// dataloader: project partners
					$dataloader['red_projectsheet_mainsupplier_partner_id'] = _array::extract($result);
			
					// template: header
					$this->view->header('pagecontent')
					->node('header')
					->data('title', $project->title);

					// template: navigation tabs
					$this->view->tabs('pagecontent')
					->navigation('tab');
			
					// template: project sheet form
					$this->view->projectSheetForm('pagecontent')
					->red('project.sheet.form')
					->data('data', $data)
					->data('dataloader', $dataloader)
					->data('buttons', $buttons)
					->data('disabled', $disabled)
					->data('hidden', $hidden)
					->data('application', $this->request->application);
				
				break;
			}
		}
	}
