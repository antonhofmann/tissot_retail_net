<?php 

class SupplyingGroups_Controller {
	
	public function __construct() {
			
		$this->user = User::instance();
		$this->request = request::instance();
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}
	
	public function index() { 
		
		Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
		Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
		Compiler::attach(DIR_CSS."jquery.ui.css");
		Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
		Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
		Compiler::attach(DIR_SCRIPTS."table.loader.js");

		$permission_edit = user::permission('can_edit_catalog');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		
		if ($permission_edit) {
			$this->request->field('add',$this->request->query('add'));
		}
		
		if ($permission_edit || $permission_view) {
			$this->request->field('data',$this->request->query('data'));
		}
		
		// template: supplying groups
		$this->view->supplyingGroups('pagecontent')
		->supplying('groups')
		->data('buttons', $buttons);
	}

	public function add() {

		$permission_edit = user::permission('can_edit_catalog');
	
		if($this->request->archived || !$permission_edit) {
			Message::access_denied();
			url::redirect($this->request->query());
		}
		
		Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
		Compiler::attach(DIR_JS."form.validator.js");
		
		// form dataloader
		$data = array();
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['action'] = $this->request->action;
		$data['redirect'] = $this->request->query('data');
			
		// buttons
		$buttons = array();
		$buttons['save'] = true;
		$buttons['back'] = $this->request->query();

		$data['supplying_group_active'] = 1;
	
		// template: application form
		$this->view->productLine('pagecontent')
		->supplying('group.form')
		->data('data', $data)
		->data('buttons', $buttons);
	}
	
	public function data() {

		$id = url::param();
		$translate = Translate::instance();

		$supplyingGroup = new Supplying_Group();
		$supplyingGroup->read($id);

		if (!$supplyingGroup->id) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$permission_edit = user::permission('can_edit_catalog');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		
		Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
		Compiler::attach(DIR_JS."form.validator.js");
		
		$data = $supplyingGroup->data;
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;

		// buttons
		$buttons = array();
		$buttons['back'] = $this->request->query();

		if ($permission_edit) {
		
			$buttons['save'] = true;

			$integrity = new Integrity();
			$integrity->set($id, 'item_supplying_groups', 'system');

			if ($integrity->check()) {
				$buttons['delete'] = $this->request->link('/applications/helpers/supplying.group.delete.php', array('id'=>$id));
			}
		}
		elseif($data) {
			foreach ($data as $key => $value) {
				$disabled[$key] = true;
			}
		}

		// header
		$this->view->header('pagecontent')->node('header')->data('title', $translate->edit_supplying_group);
	
		// template: form
		$this->view->supplyingGroups('pagecontent')
		->supplying('group.form')
		->data('data', $data)
		->data('hidden', $hidden)
		->data('disabled', $disabled)
		->data('buttons', $buttons);
	}
}