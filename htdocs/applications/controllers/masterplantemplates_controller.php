<?php 

/**
 * Controller for project masterlan templates
 */
class Masterplantemplates_Controller {
  /**
   * Setup controller
   */
  public function __construct() {
    // user must be allowed to administrate system data
    if (!user::permission('can_administrate_system_data')) {
      url::redirect("/messages/show/access_denied");
    }

    $this->user = User::instance();
    $this->request = request::instance();
    
    $this->view = new View();
    $this->view->master('master.bootstrap.inc');
    $this->view->pagetitle = $this->request->title;
    $this->view->usermenu('usermenu')->user('menu');
    $this->view->categories('pageleft')->navigation('tree');

    // this page uses modular js
    $this->view->amd = true;

    Compiler::attach(Theme::$Default);
  }
  
  /**
   * Redirect to the steps page
   */
  public function index() {
    url::redirect('./masterplantemplates/steps');
  }

  /**
   * Masterplan template steps tab
   * @return void
   */
  public function steps() {
    Compiler::attach(DIR_ASSETS . '/colorpicker/evol.colorpicker.min.css');

    // tab based navigation
    $this->view->tabs('pagecontent')
      ->navigation('tab');

    // set view data
    $this->view->masterplanTemplates('pagecontent')
      ->masterplan('template.steps')
      ->data('viewData', $this->assembleViewData());
  }

  /**
   * Masterplan template phases tab
   * @return void
   */
  public function phases() {
    // user must be allowed to administrate system data
    if (!user::permission('can_administrate_system_data')) {
      url::redirect("/messages/show/access_denied");
    }

    $Translate = translate::instance();

    // tab based navigation
    $this->view->tabs('pagecontent')
      ->navigation('tab');  

    // get subphases
    $PmMapper = new Project_Masterplan_Datamapper();
    $phases = $PmMapper->getSubPhases();

    // get available values
    $valueMapper = new Project_Masterplan_Values_Datamapper();
    $availableValues = $valueMapper->getAvailableValues();

    $fieldTypes = array(
      'default_start_value',
      'fallback_start_value',
      'default_end_value',
      'fallback_end_value'
      );

    // set view data
    $this->view->masterplanTemplates('pagecontent')
      ->masterplan('template.phases')
      ->data('fieldTypes', $fieldTypes)
      ->data('translate', $Translate)
      ->data('phases', $phases)
      ->data('values', $availableValues);
  }

  /**
   * Save masterplan template
   * Called via ajax
   * @return void  Prints output directly
   */
  public function sync() {
    // user must be allowed to administrate system data
    if (!user::permission('can_administrate_system_data')) {
      url::redirect("/messages/show/access_denied");
    }
    $masterplan = json_decode($_POST['model']);

    if (is_array($masterplan->phases)) {
      $model = new Project_Masterplan_Phase_Model();
      $phases = array();
      foreach ($masterplan->phases as $phase) {
        $data = array(
          'project_masterplan_id' => $phase->masterplan_id,
          'title' => $phase->title,
          'parent_id' => $phase->parent_id > 0 ? $phase->parent_id : null,
          'pos' => $phase->pos,
          'steps' => $phase->steps,
        );

        // update
        if (isset($phase->id)) {
          $model->update($phase->id, $data);
        }
        // create
        else {
          $phase->id = $model->create($data);
        }

        $phases[] = $phase;
      }
    }

    $Translate = translate::instance();
    die(json_encode(array(
      'phases' => $phases,
      'content' => $Translate->message_request_saved,
      'properties' => array('theme' => 'message')
      )));
  }

  /**
   * Save a phase
   * This action is called via ajax
   * @return void
   */
  public function save_phase() {
    // user must be allowed to administrate system data
    if (!user::permission('can_administrate_system_data')) {
      url::redirect("/messages/show/access_denied");
    }

    $data = array();
    $data[$_POST['field']] = $_POST['value'];
    $model = new Project_Masterplan_Phase_Model();
    $success = $model->update($_POST['phaseID'], $data);

    $Translate = translate::instance();
    print json_encode(array(
    'content' => $success ? $Translate->message_request_saved : $Translate->message_request_failure,
    'properties' => array(
      'theme' => $success ? 'message' : 'error'
    )));
    exit;
  }

  /**
   * Handler for requests from step model
   * @param  integer $id
   * @return void  Prints json directly
   */
  public function step($id) {
    // user must be allowed to administrate system data
    if (!user::permission('can_administrate_system_data')) {
      url::redirect("/messages/show/access_denied");
    }

    if (isset($_POST['_method']) == 'delete') {
      $model = new Project_Masterplan_Step_Model();
      $model->delete($id);
      die(json_encode(true));
    }
  }

  /**
   * Assemble data for view
   * @return array Array with the various data parts needed by view
   */
  protected function assembleViewData() {
    $Translate = translate::instance();

    $PmMapper = new Project_Masterplan_Datamapper();
    $PosMapper = new POS_Datamapper();
    $ProjectMapper = new Project_Datamapper();
    $CountryMapper = new Country_Datamapper();
    $RoleMapper = new Role_Datamapper();
    $UserMapper = new User_Datamapper();

    // get regions and map their assigned countries to them
    $countries = $CountryMapper->getCountriesBySalesRegion();
    $regions = __::map($CountryMapper->getSalesRegions(), function($region) use($countries) {
      $region['countries'] = $countries[$region['id']];
      return $region;
    });

    // restructure values for view
    $valueMapper = new Project_Masterplan_Values_Datamapper();
    $availableValues = $valueMapper->getAvailableValues();
    
    $values = array();
    
    if ($availableValues) {
      foreach ($availableValues as $key => $value) {
        $values[] = array('alias' => $key, 'name' => $value);
      }
    }

    $stepTypes = array();
    $pmMapper = $PmMapper->getStepTypes();
    
    if ($pmMapper) {
      foreach ($PmMapper->getStepTypes() as $type) {
        $stepTypes[] = array('type' => $type, 'name' => $Translate->translate($type));
      }
    }

    $masterplan = $PmMapper->getMasterplanDataFlat(Project_Masterplan_Datamapper::DEFAULT_PROJECT_MASTERPLAN);
    return array(
      'viewData'           => array( 
        'projectCostTypes' => $ProjectMapper->getProjectCostTypes(),
        'posTypes'         => $PosMapper->getTypes(),
        'stepTypes'        => $stepTypes,
        'salesRegions'     => $regions,
        'roles'            => $RoleMapper->getMasterplanRoles(),
        'values'           => $values,
        'adminUsers'       => $UserMapper->getAdmins(),
        ),
      'masterplan' => $masterplan,
    );
  }
}
