<?php


class Newsletters_Controller {

	static public $permissions = array();

	public function __construct() {
		
		Settings::init()->theme = 'swatch';
		$this->request = request::instance();
		$this->view = new View();
		$this->view->pageclass = 'theme-news news-newsletters';
		$this->view->pagetitle = $this->request->title;
		$this->view->userboxMenu('navigations')->navigation('userbox');
		$this->view->categories('navigations')->navigation('categories');
		$this->view->appTabs('navigations')->navigation('appnav');

		static::$permissions = array(
			'edit' => user::permission('can_edit_newsletter'),
			'sent' => user::permission('can_sent_newsletter')
		);

		Compiler::attach('/public/themes/swatch/css/news.css');
	}

	public function index () {

		$application = $this->request->application;
		
		// session filetes
		$_REQUEST = session::filter($application, "newsletters", true);
		$filterState = $_REQUEST['state'];
		$filterSort = $_REQUEST['sort'] ?: 'created';
		$filterView = $_REQUEST['view'] ?: 'thumbnail-view';
		$filterSearch = $_REQUEST['search'];
		$filterYear = $_REQUEST['year'];
		$filterNewsletter = $_REQUEST['newsletter'];
		$filterPages = $_REQUEST['page'] ?: 1;

		// buttons
		$buttons['add'] = static::$permissions['edit'] ? true : false;

		$model = new Model($application);

		// get workflow states
		$sth = $model->db->prepare("
			SELECT DISTINCT
				newsletter_workflow_state_id AS id, 
				newsletter_workflow_state_name AS name,
				CONCAT('.', LOWER(REPLACE(newsletter_workflow_state_name, ' ', '-')) ) AS filter,
				IF (newsletter_workflow_state_id = ?, 'active', '') AS active
			FROM newsletters
			INNER JOIN newsletter_workflow_states ON newsletter_workflow_state_id = newsletter_publish_state_id
			ORDER BY newsletter_workflow_state_order, newsletter_workflow_state_name
		");

		$sth->execute(array($filterState));
		$states = $sth->fetchAll();

		array_unshift($states, array(
			'name' => 'All',
			'active' => $filterState ? null : 'active'
		));

		// newsletter years
		$sth = $model->db->prepare("
			SELECT DISTINCT
				YEAR(newsletter_publish_date) AS year,
				IF (YEAR(newsletter_publish_date) = ?, 'class=active', '') AS active
			FROM newsletters
			WHERE newsletter_publish_date > 0
			ORDER BY year DESC
		");

		$sth->execute(array($filterYear));
		$years = $sth->fetchAll();

		array_unshift($years, array(
			'year' => 'All Years',
			'active' => $filterYear ? null : 'active'
		));

		// newsletters
		$sth = $model->db->prepare("
			SELECT DISTINCT
				newsletter_id AS id,
				newsletter_number AS number,
				CONCAT('#', newsletter_number, ' ', newsletter_title) AS title,
				IF (YEAR(newsletter_id) = ?, 'class=active', '') AS active
			FROM newsletters
			WHERE newsletter_number > 0
			ORDER BY newsletter_number DESC
		");

		$sth->execute(array($filterNewsletter));
		$newsletters = $sth->fetchAll();

		array_unshift($newsletters, array(
			'title' => 'All Newsletters',
			'caption' => 'll Newsletters',
			'active' => $filterNewsletter ? null : 'active'
		));


		// button sorts
		$sorts = array();

		// published on
		$sorts[] = array(
			'title' => 'Date Created',
			'filter' => 'created',
			'ascending' => false,
			'icon' => "<i class='fa fa-caret-down'></i>",
			'active' => $filterSort=='created' ? 'active' : null
		);

		// published on
		$sorts[] = array(
			'title' => 'Date Publish',
			'filter' => 'published',
			'ascending' => false,
			'icon' => "<i class='fa fa-caret-down'></i>",
			'active' => $filterSort=='published' ? 'active' : null
		);

		// article title
		$sorts[] = array(
			'title' => 'Newsletter Title',
			'filter' => 'title',
			'ascending' => true,
			'icon' => "<i class='fa fa-caret-down'></i>",
			'active' => $filterSort=='title' ? 'active' : null
		);

		// section
		$sorts[] = array(
			'title' => 'Status',
			'filter' => 'state',
			'ascending' => true,
			'icon' => "<i class='fa fa-caret-down'></i>",
			'active' => $filterSort=='state' ? 'active' : null
		);

		
		// button view
		$views = array();

		// thumbnails
		$views[] = array(
			'title' => '<i class="fa fa-th"></i>',
			'filter' => 'thumbnail-view',
			'active' => $filterView=='thumbnail-view' ? 'active' : null
		);

		// list
		$views[] = array(
			'title' => '<i class="fa fa-bars"></i>',
			'filter' => 'list-view',
			'active' => $filterView=='list-view' ? 'active' : null
		);

		// view template
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'news/newsletter/list.php');
		$tpl->data('data', $data);
		$tpl->data('page', $page);
		$tpl->data('buttons', $buttons);
		$tpl->data('states', $states);
		$tpl->data('sorts', $sorts);
		$tpl->data('views', $views);
		$tpl->data('years', $years);
		$tpl->data('newsletters', $newsletters);
		$tpl->data('filterSearch', $filterSearch);
		$this->view->setTemplate('newsletter', $tpl);

		Compiler::attach(array(
			'/public/themes/swatch/css/newsletters.css',
			'/public/scripts/mustache/mustache.js',
			'/public/scripts/infinitescroll/infinitescroll.js',
			'/public/scripts/isotope/isotope.min.js',
			'/public/scripts/sticky/sticky.parent.min.js',
			'/public/js/newsletters.js'
		));
	}


	public function composer() {

		$id = url::param();
		$application = $this->request->application;
		$user = User::instance();

		$_CAN_EDIT = static::$permissions['edit'];
		$_CAN_SENT = static::$permissions['sent'];

		// no right to access or to view article
		if (!$_CAN_EDIT && !$_CAN_SENT && !$id ) { 
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$model = new Model($application);

		// add article if not exist
		if (!$id) { 
			
			$sth = $model->db->prepare("
				INSERT INTO newsletters (
					newsletter_publish_state_id, 
					user_created 
				) 
				VALUES (1, ?)
			");

			$sth->execute(array($user->login));
			$id = $model->db->lastInsertId();

			if ($id) {

				// track: create newsletter 
				$track = $model->db->prepare("
					INSERT INTO newsletter_tracks (
						newsletter_track_newsletter_id,
						newsletter_track_user_id,
						newsletter_track_type_id
					)
					VALUES (?,?,?)
				");

				$track->execute(array($id, $user->id, 1));

				url::redirect($this->request->query("composer/$id"));

			} else {
				Message::error("Error on add newsletter (PDO).");
				url::redirect($this->request->query());
			}
		}

		// read newsletter
		$newsletter = new Modul($application);
		$newsletter->setTable('newsletters');
		$newsletter->read($id);

		if (!$newsletter->id) {
			Message::error("Newsletter not found.");
			url::redirect($this->request->query());
		}

		// newsletter data
		$data = $newsletter->data;
		$data['id'] = $id;

		// tabs
		$menu = menu::instance();
		$tabs = $menu->getData($this->request->parent);
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'/navigation/tabs.bs.php');
		$tpl->data('tabs', $tabs);
		$tpl->data('style', 'navbar-nav');
		$tpl->data('params', array($id));
		$tabs = $tpl->render();

		// view template
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'news/newsletter/composer.php');
		$tpl->data('id', $id);
		$tpl->data('number', $newsletter->data['newsletter_number']);
		$tpl->data('tabs', $tabs);
		$tpl->data('data', $data);
		$this->view->setTemplate('articles', $tpl);

		Compiler::attach(array(
			'/public/scripts/croppic/croppic.css',
			'/public/scripts/croppic/croppic.js',
			'/public/scripts/bsdialog/bsdialog.min.css',
			'/public/scripts/bsdialog/bsdialog.min.js',
			'/public/scripts/mustache/mustache.js',
			'/public/js/newsletter.js',
			'/public/js/newsletter.composer.js',
			'/public/themes/swatch/css/newsletter.css'
		));
		
	}


	public function articles() {

		$id = url::param();
		$application = $this->request->application;
		$user = User::instance();

		$this->view->name = 'page-articles';

		$_CAN_EDIT = static::$permissions['edit'];
		$_CAN_SENT = static::$permissions['sent'];

		// no right to access or to view article
		if (!$_CAN_EDIT && !$_CAN_SENT) { 
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$model = new Model($application);

		// read newsletter
		$newsletter = new Modul($application);
		$newsletter->setTable('newsletters');
		$newsletter->read($id);

		if (!$newsletter->id) {
			Message::error("Newsletter not found.");
			url::redirect($this->request->query());
		}

		// newsletter data
		$data = $newsletter->data;
		$data['id'] = $id;

		// tabs
		$menu = menu::instance();
		$tabs = $menu->getData($this->request->parent);
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'/navigation/tabs.bs.php');
		$tpl->data('tabs', $tabs);
		$tpl->data('style', 'navbar-nav');
		$tpl->data('params', array($id));
		$tabs = $tpl->render();

		// view template
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'news/newsletter/articles.php');
		$tpl->data('id', $id);
		$tpl->data('number', $newsletter->data['newsletter_number']);
		$tpl->data('tabs', $tabs);
		$tpl->data('data', $data);
		$this->view->setTemplate('articles', $tpl);

		Compiler::attach(array(
			'/public/scripts/bsdialog/bsdialog.min.css',
			'/public/scripts/bsdialog/bsdialog.min.js',
			'/public/scripts/mustache/mustache.js',
			'/public/scripts/infinitescroll/infinitescroll.js',
			'/public/scripts/infinitescroll/behavior.local.js',
			'/public/js/newsletter.js',
			'/public/js/newsletter.articles.js',
			'/public/themes/swatch/css/newsletter.css',
			'/public/themes/swatch/css/tabs.flat.css'
		));
	}


	public function recipients() {

		$id = url::param();
		$application = $this->request->application;
		$user = User::instance();
		$this->view->name = 'page-recipients';

		$_CAN_EDIT = static::$permissions['edit'];
		$_CAN_SENT = static::$permissions['sent'];

		// no right to access or to view article
		if (!$_CAN_EDIT && !$_CAN_SENT) { 
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$model = new Model($application);

		// read newsletter
		$newsletter = new Modul($application);
		$newsletter->setTable('newsletters');
		$newsletter->read($id);

		if (!$newsletter->id) {
			Message::error("Newsletter not found.");
			url::redirect($this->request->query());
		}

		// newsletter data
		$data = $newsletter->data;
		$data['id'] = $id;

		// tabs
		$menu = menu::instance();
		$tabs = $menu->getData($this->request->parent);
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'/navigation/tabs.bs.php');
		$tpl->data('tabs', $tabs);
		$tpl->data('style', 'navbar-nav');
		$tpl->data('params', array($id));
		$tabs = $tpl->render();


		$swatchRecipients = array();
		$additionalUsers = array();
		$currentUsers = array();
		
		// get current recipients
		$sth = $model->db->prepare("
			SELECT 
				newsletter_recipient_role_id,
				newsletter_recipient_user_id,
				newsletter_recipient_additional_id,
				DATE_FORMAT(newsletter_recipient_sent_date, '%d.%m.%Y %H:%i') AS sent,
				newsletter_recipient_selected AS selected
			FROM newsletter_recipients
			WHERE newsletter_recipient_newsletter_id = ?
		");

		$sth->execute(array($id));
		$result = $sth->fetchAll();

		$involvedUsers = array();

		if ($result) {
			
			foreach ($result as $row) {
				
				$role = $row['newsletter_recipient_role_id'];
				$user = $row['newsletter_recipient_user_id'];
				$adit = $row['newsletter_recipient_additional_id'];
				
				if ($user && $role) {

						$involvedUsers[] = $user;

						$currentUsers['swatch'][$role][$user] = array(
						'sent' => $row['sent'],
						'selected' => $row['selected']
					);
				}
				
				if ($adit) {
					$currentUsers['aditional'][$adit] = array(
						'sent' => $row['sent'],
						'selected' => $row['selected']
					);
				}
			}
		}

		// filter involved users (timeout without indexes)
		//$filterInvolvedUsers = $involvedUsers ? ' OR user_id IN ('. join(',', $involvedUsers).') ' : null;

		// get all permitted users
		$sth = $model->db->prepare("
			SELECT DISTINCT
			    user_id,
			    user_email,
			    CONCAT(user_firstname, ' ', user_name) AS name,
				address_id,
				address_company AS company,
			    (
					SELECT GROUP_CONCAT(user_role_role)
			        FROM db_retailnet.user_roles
			        WHERE user_role_user = user_id
			    ) AS roles
			FROM db_retailnet.users
			INNER JOIN db_retailnet.addresses ON address_id = user_address
			INNER JOIN db_retailnet.user_roles ON user_role_user = user_id
			INNER JOIN db_retailnet.roles ON role_id = user_role_role
			INNER JOIN db_retailnet.role_permissions ON role_permission_role = role_id
			INNER JOIN db_retailnet.permissions ON role_permission_permission = permission_id
			WHERE user_active = 1 AND permission_name = 'can_read_news' $filterInvolvedUsers
			GROUP BY user_email
			ORDER BY name
		");

		$sth->execute();
		$users = $sth->fetchAll();

		if ($users) {

			$sth = $model->db->prepare("
				SELECT DISTINCT
					role_id AS id,
					role_name AS name
				FROM db_retailnet.users
				INNER JOIN db_retailnet.user_roles ON user_role_user = user_id
				INNER JOIN db_retailnet.roles ON role_id = user_role_role
				INNER JOIN db_retailnet.role_permissions ON role_permission_role = role_id
				INNER JOIN db_retailnet.permissions ON role_permission_permission = permission_id
				WHERE user_active = 1 AND permission_name = 'can_read_news'
				ORDER BY name
			");

			$sth->execute();
			$roles = $sth->fetchAll();

			if ($roles) {

				foreach ($roles as $role) {
					
					$roleID = $role['id'];

					foreach ($users as $user) {
						
						$userID =  $user['user_id'];
						$roles = explode(',', $user['roles']);

						if ( in_array($roleID, $roles) ) {

							$swatchRecipients[$roleID]['role'] = $roleID;
							$swatchRecipients[$roleID]['title'] = $role['name'];

							$swatchRecipients[$roleID]['users'][] = array(
								'user' => $userID,
								'name' => $user['name'],
								'company' => $user['company'],
								'checked' => $currentUsers['swatch'][$roleID] && $currentUsers['swatch'][$roleID][$userID]['selected']==1 ? 'checked' : null,
								'sent' => $currentUsers['swatch'][$roleID] ? $currentUsers['swatch'][$roleID][$userID]['sent'] : null
							);
						}

					}
				}
			}
		}

		// get all additional users
		$sth = $model->db->prepare("
			SELECT DISTINCT
				news_recipient_id,
				CONCAT(news_recipient_firstname, ' ', news_recipient_name) AS user
			FROM news_recipients
			WHERE news_recipient_active = 1
			ORDER BY user
		");

		$sth->execute();
		$result = $sth->fetchAll();

		if ($result) {

			foreach ($result as $row) {
				$user = $row['news_recipient_id'];
				$additionalUsers[] = array(
					'user' => $user,
					'name' => $row['user'],
					'checked' => $currentUsers['aditional'][$user]['selected']==1 ? 'checked' : null,
					'sent' => $currentUsers['aditional'][$user]['sent']
				);
			}
		}


		$tracks = array();

		// views tracks
		$sth = $model->db->prepare("
			SELECT DISTINCT
				newsletter_track_user_id AS user,
				COUNT(newsletter_track_newsletter_id) AS total
			FROM newsletter_tracks
			INNER JOIN db_retailnet.users ON user_id = newsletter_track_user_id
			WHERE newsletter_track_newsletter_id = ? AND newsletter_track_type_id = 19
			GROUP BY newsletter_track_user_id
			ORDER BY newsletter_tracks.date_created DESC
		");

		$sth->execute(array($id));
		$result = $sth->fetchAll();
		$tracks['views'] = _array::extract($result);

		// views from email
		$sth = $model->db->prepare("
			SELECT DISTINCT
				newsletter_track_user_id AS user,
				COUNT(newsletter_track_newsletter_id) AS total
			FROM newsletter_tracks
			INNER JOIN db_retailnet.users ON user_id = newsletter_track_user_id
			WHERE newsletter_track_newsletter_id = ? AND newsletter_track_type_id = 18
			GROUP BY newsletter_track_user_id
		");

		$sth->execute(array($id));
		$result = $sth->fetchAll();
		$tracks['eviews'] = _array::extract($result); 

		// views from email (additional)
		$sth = $model->db->prepare("
			SELECT DISTINCT
				newsletter_track_additional_user_id AS user,
				COUNT(newsletter_track_newsletter_id) AS total
			FROM newsletter_tracks
			INNER JOIN news_recipients ON news_recipient_id = newsletter_track_additional_user_id
			WHERE newsletter_track_newsletter_id = ? AND newsletter_track_type_id = 18
			GROUP BY newsletter_track_additional_user_id
		");

		$sth->execute(array($id));
		$result = $sth->fetchAll();
		$tracks['eviews-additional'] = _array::extract($result); 
		
		// view template
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'news/newsletter/recipients.php');
		$tpl->data('id', $id);
		$tpl->data('number', $newsletter->data['newsletter_number']);
		$tpl->data('tabs', $tabs);
		$tpl->data('swatchRecipients', $swatchRecipients);
		$tpl->data('additionalUsers', $additionalUsers);
		$tpl->data('info', $info);
		$tpl->data('data', $data);
		$tpl->data('tracks', $tracks);
		$this->view->setTemplate('recipients', $tpl);

		Compiler::attach(array(
			'/public/scripts/bsdialog/bsdialog.min.css',
			'/public/scripts/bsdialog/bsdialog.min.js',
			'/public/scripts/mustache/mustache.js',
			'/public/js/newsletter.js',
			'/public/js/newsletter.recipients.js',
			'/public/themes/swatch/css/newsletter.css',
			'/public/themes/swatch/css/tabs.flat.css'
		));

	}


	public function tracks() {

		$id = url::param();
		$application = $this->request->application;
		$user = User::instance();

		$this->view->name = 'page-tracks';

		$_CAN_EDIT = static::$permissions['edit'];
		$_CAN_SENT = static::$permissions['sent'];

		// no right to access or to view article
		if (!$_CAN_EDIT && !$_CAN_SENT) { 
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$model = new Model($application);

		// read newsletter
		$newsletter = new Modul($application);
		$newsletter->setTable('newsletters');
		$newsletter->read($id);

		if (!$newsletter->id) {
			Message::error("Newsletter not found.");
			url::redirect($this->request->query());
		}

		// newsletter data
		$data = $newsletter->data;
		$data['id'] = $id;

		// tabs
		$menu = menu::instance();
		$tabs = $menu->getData($this->request->parent);
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'/navigation/tabs.bs.php');
		$tpl->data('tabs', $tabs);
		$tpl->data('style', 'navbar-nav');
		$tpl->data('params', array($id));
		$tabs = $tpl->render();

		$tracks = array();
		$userTracks = array();

		// views tracks
		$sth = $model->db->prepare("
			SELECT DISTINCT
				newsletter_track_newsletter_id AS id,
				user_email AS email,
				CONCAT(user_firstname, ' ', user_name) AS name,
				CONCAT(address_company, ', ', country_name) AS company,
				DATE_FORMAT(newsletter_tracks.date_created, '%d.%m.%Y') AS date,
				DATE_FORMAT(newsletter_tracks.date_created, '%H:%i') AS time
			FROM newsletter_tracks
			INNER JOIN db_retailnet.users ON user_id = newsletter_track_user_id
			INNER JOIN db_retailnet.addresses ON address_id = user_address
			INNER JOIN db_retailnet.countries ON country_id = address_country
			WHERE newsletter_track_newsletter_id = ? AND newsletter_track_type_id = 19
			ORDER BY newsletter_tracks.date_created DESC
		");

		$sth->execute(array($id));
		$result = $sth->fetchAll();

		$tracks['views'] = array();
		$userTracks['views'] = array();

		if ($result) {

			foreach ($result as $row) {

				$mail = $row['email'];
				
				$tracks['views'][] = array(
					'name' => $row['name'],
					'company' => $row['company'],
					'date' => $row['date'],
					'time' => $row['time']
				);

				$userTracks['views'][$mail] = $userTracks['views'][$mail] + 1;
			}
		}

		// newsletter article downloaded files
		$sth = $model->db->prepare("
			SELECT
				CONCAT(user_firstname, ' ', user_name) AS name,
				CONCAT(address_company, ', ', country_name) AS company,
				DATE_FORMAT(news_article_tracks.date_created, '%d.%m.%Y') AS date,
				DATE_FORMAT(news_article_tracks.date_created, '%H:%i') AS time,
				news_article_track_entity AS file,
				UNIX_TIMESTAMP(news_article_tracks.date_created) AS sort
			FROM news_article_tracks
				INNER JOIN db_retailnet.users ON user_id = news_article_track_user_id
				INNER JOIN db_retailnet.addresses ON address_id = user_address
				INNER JOIN db_retailnet.countries ON country_id = address_country
				INNER JOIN news_articles ON news_article_id = news_article_track_article_id
				INNER JOIN newsletter_articles ON newsletter_article_article_id = news_article_id
			WHERE news_article_track_newsletter_id = :id AND news_article_track_type_id = 48
			UNION ALL
			SELECT 
				CONCAT(news_recipient_firstname, ' ', news_recipient_name) AS name,
				CONCAT('Additional Recipient') AS company,
				DATE_FORMAT(news_article_tracks.date_created, '%d.%m.%Y') AS date,
				DATE_FORMAT(news_article_tracks.date_created, '%H:%i') AS time,
				news_article_track_entity AS file,
				UNIX_TIMESTAMP(news_article_tracks.date_created) AS sort
			FROM news_article_tracks
				INNER JOIN news_recipients ON news_recipient_id = news_article_track_additional_user_id
				INNER JOIN news_articles ON news_article_id = news_article_track_article_id
				INNER JOIN newsletter_articles ON newsletter_article_article_id = news_article_id
			WHERE news_article_track_newsletter_id = :id AND news_article_track_type_id = 48
			ORDER BY sort DESC
		");

		$sth->execute(array('id' => $id));
		$tracks['downloads'] = $sth->fetchAll();

		// print tracks
		$sth = $model->db->prepare("
			SELECT DISTINCT
				UNIX_TIMESTAMP(newsletter_tracks.date_created) AS id,
				CONCAT(user_firstname, ' ', user_name) AS name,
				CONCAT(address_company, ', ', country_name) AS company,
				CONCAT('Whole print') AS content,
				DATE_FORMAT(newsletter_tracks.date_created, '%d.%m.%Y') AS date,
				DATE_FORMAT(newsletter_tracks.date_created, '%H:%i') AS time
			FROM newsletter_tracks
			INNER JOIN db_retailnet.users ON user_id = newsletter_track_user_id
			INNER JOIN db_retailnet.addresses ON address_id = user_address
			INNER JOIN db_retailnet.countries ON country_id = address_country
			WHERE newsletter_track_newsletter_id = :id AND newsletter_track_type_id = 21
			UNION ALL
			SELECT DISTINCT
				UNIX_TIMESTAMP(news_article_tracks.date_created) AS id,
				CONCAT(user_firstname, ' ', user_name) AS name,
				CONCAT(address_company, ', ', country_name) AS company,
				CONCAT('Partialy print ') AS content,
				DATE_FORMAT(news_article_tracks.date_created, '%d.%m.%Y') AS date,
				DATE_FORMAT(news_article_tracks.date_created, '%H:%i') AS time
			FROM news_article_tracks
				INNER JOIN db_retailnet.users ON user_id = news_article_track_user_id
				INNER JOIN db_retailnet.addresses ON address_id = user_address
				INNER JOIN db_retailnet.countries ON country_id = address_country
			WHERE news_article_track_newsletter_id = :id AND news_article_track_type_id = 49
			ORDER BY id DESC
		");

		$sth->execute(array('id'=>$id));
		$tracks['prints'] = $sth->fetchAll();

		// newsletter email recipients 
		$sth = $model->db->prepare("
			SELECT DISTINCT
				newsletter_track_id AS id,
				CONCAT(recipients.user_firstname, ' ', recipients.user_name) AS recipient,
				recipients.user_email AS recipient_mail,
				CONCAT(recipientsCompany.address_company, ', ', recipientsCountry.country_name) AS recipient_company,
				CONCAT(senders.user_firstname, ' ', senders.user_name) AS sender,
				senders.user_email AS sender_mail,
				CONCAT(sendersCompanies.address_company, ', ', sendersCountries.country_name) AS sender_company,
				DATE_FORMAT(newsletter_tracks.date_created, '%d.%m.%Y') AS date,
				DATE_FORMAT(newsletter_tracks.date_created, '%H:%i') AS time,
				UNIX_TIMESTAMP(newsletter_tracks.date_created) AS sort
			FROM newsletter_tracks
				INNER JOIN db_retailnet.users AS recipients ON recipients.user_id = newsletter_track_user_id
				INNER JOIN db_retailnet.addresses AS recipientsCompany ON recipientsCompany.address_id = recipients.user_address
				INNER JOIN db_retailnet.countries  AS recipientsCountry ON recipientsCountry.country_id = recipientsCompany.address_country
				INNER JOIN db_retailnet.users AS senders ON  senders.user_id = newsletter_track_entity
				INNER JOIN db_retailnet.addresses AS sendersCompanies ON sendersCompanies.address_id = senders.user_address
				INNER JOIN db_retailnet.countries  AS sendersCountries ON sendersCountries.country_id = sendersCompanies.address_country
			WHERE newsletter_track_newsletter_id = :id AND newsletter_track_type_id = 15
			UNION ALL
			SELECT DISTINCT
				newsletter_track_id AS id,
				CONCAT(recipients.news_recipient_firstname, ' ', recipients.news_recipient_name) AS recipient,
				recipients.news_recipient_email AS recipient_mail,
				CONCAT('Additonal Recipient') AS recipient_company,
				CONCAT(senders.user_firstname, ' ', senders.user_name) AS sender,
				senders.user_email AS sender_mail,
				CONCAT(sendersCompany.address_company, ', ', sendersCountry.country_name) AS sender_company,
				DATE_FORMAT(newsletter_tracks.date_created, '%d.%m.%Y') AS date,
				DATE_FORMAT(newsletter_tracks.date_created, '%H:%i') AS time,
				UNIX_TIMESTAMP(newsletter_tracks.date_created) AS sort
			FROM newsletter_tracks
				INNER JOIN news_recipients AS recipients ON news_recipient_id = newsletter_track_additional_user_id
				INNER JOIN db_retailnet.users AS senders ON user_id = newsletter_track_entity
				INNER JOIN db_retailnet.addresses AS sendersCompany ON address_id = user_address
				INNER JOIN db_retailnet.countries AS sendersCountry ON country_id = address_country
			WHERE newsletter_track_newsletter_id = :id AND newsletter_track_type_id = 30
			ORDER BY sort DESC
		");

		$sth->execute(array('id'=>$id));
		$result = $sth->fetchAll();
		
		$tracks['sendmail'] = $result;		

		// open newsletter from mail client app
		$sth = $model->db->prepare("
			SELECT DISTINCT
				newsletter_track_newsletter_id,
				user_email As email,
				CONCAT(user_firstname, ' ', user_name) AS name,
				DATE_FORMAT(newsletter_tracks.date_created, '%d.%m.%Y') AS date,
				DATE_FORMAT(newsletter_tracks.date_created, '%H:%i') AS time,
				CONCAT(address_company, ', ', country_name) AS company,
				UNIX_TIMESTAMP(newsletter_tracks.date_created) AS sort
			FROM newsletter_tracks
				INNER JOIN db_retailnet.users ON user_id = newsletter_track_user_id
				INNER JOIN db_retailnet.addresses ON address_id = user_address
				INNER JOIN db_retailnet.countries ON country_id = address_country
			WHERE newsletter_track_newsletter_id = :id AND newsletter_track_type_id = 18
			UNION ALL
			SELECT DISTINCT
				newsletter_track_newsletter_id,
				news_recipient_email AS email,
				CONCAT(news_recipient_firstname, ' ', news_recipient_name) AS name,
				DATE_FORMAT(newsletter_tracks.date_created, '%d.%m.%Y') AS date,
				DATE_FORMAT(newsletter_tracks.date_created, '%H:%i') AS time,
				CONCAT('Additional Recipient') AS company,
				UNIX_TIMESTAMP(newsletter_tracks.date_created) AS sort
			FROM newsletter_tracks
			INNER JOIN news_recipients ON news_recipient_id = newsletter_track_additional_user_id
			WHERE newsletter_track_newsletter_id = :id AND newsletter_track_type_id = 18
			ORDER BY sort DESC
		");

		$sth->execute(array('id'=>$id));
		$result = $sth->fetchAll();;

		if ($result) {
			foreach ($result as $row) {
				$tracks['views-client'][] = array(
					'name' => $row['name'],
					'email' => $row['email'],
					'date' => $row['date'],
					'time' => $row['time'],
					'company' => $row['company']
				);
			}
		}

		// view template
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'news/newsletter/tracks.php');
		$tpl->data('id', $id);
		$tpl->data('number', $newsletter->data['newsletter_number']);
		$tpl->data('tabs', $tabs);
		$tpl->data('data', $data);
		$tpl->data('tracks', $tracks);
		$this->view->setTemplate('articles', $tpl);

		Compiler::attach(array(
			'/public/scripts/bsdialog/bsdialog.min.css',
			'/public/scripts/bsdialog/bsdialog.min.js',
			'/public/scripts/mustache/mustache.js',
			'/public/scripts/infinitescroll/infinitescroll.js',
			'/public/scripts/infinitescroll/behavior.local.js',
			'/public/js/newsletter.js',
			'/public/js/newsletter.articles.js',
			'/public/themes/swatch/css/newsletter.css'
		));
	}


	public function preview () { 

		$id = url::param();
		$application = Request::instance()->application;

		$_CAN_EDIT = static::$permissions['edit'];
		$_CAN_SENT = static::$permissions['sent'];

		// no right to access or to view article
		if (!$_CAN_EDIT && !$_CAN_SENT) { 
			url::redirect("/messages/show/access_denied");
		}

		// reset portal filters
		session::resetFilter($application, 'portal-preview');

		$this->view->master('news.portal.php');
		$this->view->pageclass = 'theme-news news-newsletters';
		$this->view->pagetitle = Request::instance()->title;
		$this->view->preview = $id;
		$this->view->logoLink = "/gazette/newsletters/preview/$id";


		$model = new Model($application);

		// get last published newsletetrs
		$sth = $model->db->prepare("
			SELECT 
				newsletter_id AS id,
				newsletter_number As number,
				newsletter_title AS title,
				newsletter_image AS image,
				newsletter_text AS content,
				newsletter_background AS background,
				DATE_FORMAT(newsletter_publish_date, '%m/%d/%Y') AS date
			FROM newsletters
			WHERE newsletter_id = ?
		");
		
		$sth->execute(array($id));
		$newsletter = $sth->fetch();
		$newsletter['preview'] = true;
		$newsletter['background'] = $newsletter['background'] ? 'style="background-image: url('.$newsletter['background'].')"' : null; 

		// get newsletter number
		if (!$newsletter['number']) {
			$sth = $model->db->prepare("
				SELECT MAX(newsletter_number) AS number
				FROM newsletters
				WHERE newsletter_publish_state_id > 2
			");

			$sth->execute();
			$result = $sth->fetch();
			$newsletter['number'] = $result['number'] + 1;
		}

		if (!$newsletter['id']) {
			url::redirect("/messages/show/access_denied");
		}

		// get newsletters files
		$sth = $model->db->prepare("
			SELECT newsletter_file_path
			FROM newsletter_files
			WHERE newsletter_file_newsletter_id = ?
		");

		$sth->execute(array($newsletter['id']));
		$files = $sth->fetchAll();

		if ($files) {
			foreach ($files as $file) {
				if (file_exists($_SERVER['DOCUMENT_ROOT'].$file['newsletter_file_path'])) {
					$newsletter['files'][] = $file['newsletter_file_path'];
				}
			}
		}

		// assign newsletter data to view
		$this->view->newsletter = $newsletter;

		// get sections
		$sth = $model->db->prepare("
			SELECT DISTINCT
				news_category_id,
				news_category_name,
				news_section_id,
				news_section_name,
				(
					SELECT COUNT(DISTINCT news_article_id)
					FROM news_articles
					WHERE news_article_category_id = news_category_id 
					AND news_article_active = 1 
					AND news_article_publish_state_id IN (4,5)
				) AS total
			FROM newsletters
			INNER JOIN newsletter_articles ON newsletter_article_newsletter_id = newsletter_id
			INNER JOIN news_articles ON news_article_id = newsletter_article_article_id
			INNER JOIN news_categories ON news_article_category_id = news_category_id
			INNER JOIN news_sections ON news_section_id = news_category_section_id
			WHERE news_article_active = 1 
			AND news_article_publish_state_id IN (4,5) 
			AND newsletter_publish_state_id IN (3,4)
			ORDER BY news_section_order, news_section_name, news_category_order, news_category_name
		");

		$sth->execute();
		$result = $sth->fetchAll();

		if ($result) {
			
			foreach ($result as $row) {
				
				$section = $row['news_section_id'];
				$category = $row['news_category_id'];

				if ($row['total']) { 
					$categoriesMenu[$section]['name'] = $row['news_section_name'];
					$categoriesMenu[$section]['categories'][$category]['name'] = $row['news_category_name'];
					$categoriesMenu[$section]['categories'][$category]['total'] = $row['total'];
				}
			}
		}

		// assign sections data to view
		$this->view->categoriesMenu = $categoriesMenu;

		$this->view->searchSubmitAction = "/gazette/search/preview/$id";

		Compiler::attach(array(
			'/public/themes/swatch/css/news.portal.css',
			'/public/css/news.content.css',
			'/public/scripts/sticky/jquery.sticky.js',
			'/public/js/news.portal.js'
		));
	}

}