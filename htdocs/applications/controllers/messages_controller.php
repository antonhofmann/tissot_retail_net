<?php 

	class Messages_Controller {
		
		public function __construct() {
			
			$this->view = new View();
			$this->view->master('message.inc');
			$this->translate = Translate::instance();
			$this->view->usermenu('usermenu')->user('menu');
			
			if (session::get('user_id')) {
			
				$menu = menu::instance();
				$applicatios = $menu->data[0];
			
				if ($applicatios) {
					$this->view->categories('pageleft')->navigation('tree');
				}
			}

			Compiler::attach(Theme::$Default);
		}
		
		public function show() { 
			
			$this->view->pagetitle = $this->translate->message;
			
			$id = url::param(); 
			
			if (!$id) {
				$data = message::read_from_keyword('failure_id', Message::CATEGORY_MESSAGE);
			} else { 
				
				$data = message::read($id);
				
				if (is_numeric($id)) {
					$data = message::read($id);
				} else { 
					$param = url::param(1);
					$keyword = ($param) ? $param : $id;
					$category = ($param) ? $id : Message::CATEGORY_MESSAGE;
					$keyword = str_replace('-', '_', $keyword);
					$data = message::read_from_keyword($keyword, $category);
				}
			}
			
			// message title
			$keyword = $data['keyword'];
			$title = $this->translate->$keyword;
			
			// message content
			$content = $data['content'];
			$content = $this->render($content);
			
			$this->view
			->message('pagecontent')
			->message('show')
			->data('title', $title)
			->data('content', nl2br($content));	
		}
		
		public function error() { 
			
			$this->view->pagetitle = $this->translate->error;
			
			$id = url::param();
			
			if (is_numeric($id)) {
				$data = message::read($id);
			}
			else {
				$id = str_replace('-', '_', $id);
				$data = message::read_from_keyword($id, Message::CATEGORY_ERROR);
			}

			// message title
			$keyword = $data['keyword'];
			$title = $this->translate->$keyword;
				
			// message content
			$content = $data['content'];
			$content = $this->render($content);
				
			$this->view
			->message('pagecontent')
			->message('show')
			->data('title', $title)
			->data('content', nl2br($content));
			
		}
		
		protected function render($content) {
			
			$render =  array(
				'ip' => url::ip(),
				'password_forgotten' => '<a href="/security/password/forgotten">'.$this->translate->password_forgotten.'</a>',
				'password_reset' => 	'<a href="/security/password/reset">'.$this->translate->password_reset.'</a>',
				'login' => '<a href="/security/password/forgotten">'.$this->translate->login.'</a>',
				'mailto_administrator' => '<a href="mailto:'.$this->settings->settings.'">'.$this->translate->administrator.'</a>',
				'user_name' => Session::get('user_name'),
				'home_page' => '<a href="/">'.$this->translate->home_page.'</a>'
			);
			
			return content::render($content, $render);
		}


		public function getSession() {

			$messages = Session::get('messages'); 
			$messages = $messages ? unserialize($messages) : null;
			Session::remove('messages');

			header("Content-Type: text/json");
			echo json_encode($messages);
			exit;

		}
		
	}