<?php


class News_Controller {

	static public $permissions = array();

	public function __construct() {
		
		Settings::init()->theme = 'swatch';
		$this->view = new View();
		$this->view->master('news.portal.php');
		$this->view->pageclass = 'theme-news news-newsletters';
		$this->view->pagetitle = Request::instance()->title;
		//$this->view->userboxMenu('navigations')->navigation('userbox');
		//$this->view->categories('navigations')->navigation('categories');
		//$this->view->appTabs('navigations')->navigation('appnav');

		Compiler::attach('/public/themes/swatch/css/news.css');
	}

	public function index () {

		$user = User::instance();
		$request = Request::instance();
		$id = $request->param();
		$application = $request->application;

		$_PERMISSIONS = array(
			'view.all' => user::permission('can_view_all_news_articles'),
			'view.his' => user::permission('can_view_only_his_news_articles'),
			'edit.all' => user::permission('can_edit_all_news_articles'),
			'edit.his' => user::permission('can_edit_only_his_news_articles')
		);

		$_ACCESS_FULL = $_PERMISSIONS['view.all'] || $_PERMISSIONS['edit.all'] ? true : false;

		// reset portal filters
		session::resetFilter($application, 'portal');

		$certain = $id ? true : false;
		Session::setFilter($application, 'portal', 'certain', $certain);

		// newsletetr controlller
		$this->view->controller = 'news';

		$model = new Model($application);

		$filterNewsletter = "newsletter_publish_state_id = 3";

		if ($id) {
			
			$modul = new Modul($application);
			$modul->setTable('newsletters');
			$modul->read($id);
			
			if ($modul->id) {
				$filterNewsletter = "newsletter_id = $id AND newsletter_publish_state_id IN (3,4)";
				$this->view->logoLink = "/gazette/$modul->id";
			} else $filterNewsletter = "newsletter_publish_state_id = 3";
		}

		// get last published newsletetrs
		$sth = $model->db->prepare("
			SELECT 
				newsletter_id AS id,
				newsletter_number AS number,
				newsletter_title AS title,
				newsletter_image AS image,
				newsletter_text AS content,
				newsletter_publish_state_id AS state,
				newsletter_background AS background,
				DATE_FORMAT(newsletter_publish_date, '%m/%d/%Y') AS date
			FROM newsletters
			WHERE $filterNewsletter
			ORDER BY newsletter_publish_date DESC
			LIMIT 1
		");

		$sth->execute();
		$newsletter = $sth->fetch();
		$newsletter['preview'] = false;
		$newsletter['background'] = $newsletter['background'] ? 'style="background-image: url('.$newsletter['background'].')"' : null; 
		//echo "<pre>"; print_r($newsletter); die;

		// get newsletters files
		$sth = $model->db->prepare("
			SELECT newsletter_file_path, newsletter_file_title
			FROM newsletter_files
			WHERE newsletter_file_newsletter_id = ?
		");

		$sth->execute(array($newsletter['id']));
		$files = $sth->fetchAll();

		if ($files) {

			foreach ($files as $file) {
				
				if (file_exists($_SERVER['DOCUMENT_ROOT'].$file['newsletter_file_path'])) {
					//$extension = strtoupper(pathinfo($file['newsletter_file_path'], PATHINFO_EXTENSION));
					$hash = Encryption::url($file['newsletter_file_path']);
					$newsletter['files'][] = array(
						'file' => "/download/newsletter/$hash/".$newsletter['id'],
						'title' => $file['newsletter_file_title']
					);
				}
			}
		}

		// assign newsletter data to view
		$this->view->newsletter = $newsletter;

		$model->db->exec("SET GLOBAL group_concat_max_len = 1000000");

		// get sections
		$sth = $model->db->prepare("
			SELECT DISTINCT
				news_category_id,
				news_category_name,
				news_section_id,
				news_section_name,
				(
					SELECT GROUP_CONCAT(DISTINCT news_article_id)
					FROM news_articles
					WHERE news_article_category_id = news_category_id
					AND news_article_active = 1
					AND news_article_publish_state_id IN (4,5)
				) AS articles,
				(
					SELECT GROUP_CONCAT(DISTINCT news_article_id)
					FROM news_articles
					INNER JOIN news_article_addresses ON news_article_address_article_id = news_article_id
					WHERE news_article_category_id = news_category_id
					AND news_article_active = 1
					AND news_article_publish_state_id IN (4,5)
				) AS companyRestrictedArticles,
				(
					SELECT GROUP_CONCAT(DISTINCT news_article_id)
					FROM news_articles
					INNER JOIN news_article_addresses ON news_article_address_article_id = news_article_id
					WHERE news_article_category_id = news_category_id
					AND news_article_active = 1 AND news_article_address_address_id = ?
					AND news_article_publish_state_id IN (4,5)
				) AS companyRestrictedArticlesUser,
				(
					SELECT GROUP_CONCAT(DISTINCT news_article_id)
					FROM news_articles
					INNER JOIN news_article_roles ON news_article_role_article_id = news_article_id
					WHERE news_article_category_id = news_category_id
					AND news_article_active = 1
					AND news_article_publish_state_id IN (4,5)
				) AS roleRestrictedArticles,
				(
					SELECT GROUP_CONCAT(DISTINCT news_article_id)
					FROM news_articles
					INNER JOIN news_article_roles ON news_article_role_article_id = news_article_id
			        INNER JOIN db_retailnet.user_roles ON user_role_role = news_article_role_role_id
					WHERE news_article_category_id = news_category_id
					AND news_article_active = 1 AND user_role_user = ?
					AND news_article_publish_state_id IN (4,5)
				) AS roleRestrictedArticlesUser
			FROM news_articles
			INNER JOIN newsletter_articles ON news_article_id = newsletter_article_article_id
			INNER JOIN news_categories ON news_article_category_id = news_category_id
			INNER JOIN news_sections ON news_section_id = news_category_section_id
			WHERE news_article_active = 1 
			AND news_article_publish_state_id IN (4,5) 
			ORDER BY news_section_order, news_section_name, news_category_order, news_category_name
		");

		$sth->execute(array($user->address, $user->id));
		$result = $sth->fetchAll();

		if ($result) {

			$articleUserPermittedCompanies = array();
			$articleUserPermittedRoles = array();

			foreach ($result as $row) {
				
				$section = $row['news_section_id'];
				$category = $row['news_category_id'];
				
				$articles = explode(',',$row['articles']);
				$totalArticles = count($articles);

				// restrictions
				$companyRestrictedArticles = $row['companyRestrictedArticles'];
				$companyRestrictedArticlesUser = $row['companyRestrictedArticlesUser'];
				$roleRestrictedArticles = $row['roleRestrictedArticles'];
				$roleRestrictedArticlesUser = $row['roleRestrictedArticlesUser'];

				if ( !$_ACCESS_FULL && ($companyRestrictedArticles || $roleRestrictedArticles) ) {

					$totalArticles = 0;
					$companyNotPermittedArticles = array();
					$roleNotPermittedArticles = array();

					// company restrictions
					if ($companyRestrictedArticles) {
						
						$companyRestrictedArticles = explode(',', $companyRestrictedArticles);
						$userPermittedArticles = $companyRestrictedArticlesUser ? explode(',', $companyRestrictedArticlesUser) : array();

						// get only restricted articles from articles
						$restrictedArticles = array_intersect($articles, $companyRestrictedArticles);
		
						$companyNotPermittedArticles = array_diff($restrictedArticles, $userPermittedArticles);
					}

					// role restrictions
					if ($roleRestrictedArticles) {

						$roleRestrictedArticles = explode(',', $roleRestrictedArticles);
						$userPermittedArticles = $roleRestrictedArticlesUser ? explode(',', $roleRestrictedArticlesUser) : array();

						// get only restricted articles from articles
						$restrictedArticles = array_intersect($articles, $roleRestrictedArticles);
						
						$roleNotPermittedArticles = array_diff($restrictedArticles, $userPermittedArticles);
					}

					$notPermittedArticles = array_merge($companyNotPermittedArticles, $roleNotPermittedArticles);
					$totalArticles = count(array_diff($articles, array_unique($notPermittedArticles)));
				}
				
				if ($totalArticles) { 
					$categoriesMenu[$section]['name'] = $row['news_section_name'];
					$categoriesMenu[$section]['categories'][$category]['name'] = $row['news_category_name'];
					$categoriesMenu[$section]['categories'][$category]['total'] = $totalArticles;
				}
			}
		}

		// assign sections data to view
		$this->view->categoriesMenu = $categoriesMenu;

		// news quick links
		$sth = $model->db->prepare("
			SELECT 
				news_quicklink_title AS title,
				news_quicklink_link As link
			FROM news_quicklinks
			ORDER BY news_quicklink_order
		");

		$sth->execute();
		$quicklinks = $sth->fetchAll();
		$this->view->quicklinks = _array::partition($quicklinks, 3);

		// news archive
		$sth = $model->db->prepare("
			SELECT 
				newsletter_id AS id,
				CONCAT('Gazette #', newsletter_number) AS name
			FROM newsletters
			WHERE newsletter_publish_state_id = 4 AND newsletter_number > 0
			ORDER BY newsletter_publish_date DESC
		");

		$sth->execute();
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$id = $row['id'];
				$gazette[$id] = $row['name'];
			}

			$this->view->archive = $gazette;
		}

		$this->view->searchSubmitAction = "/gazette/search";

		Compiler::attach(array(
			'/public/themes/swatch/css/news.portal.css',
			'/public/css/news.content.css',
			'/public/scripts/sticky/jquery.sticky.js',
			'/public/js/news.portal.js'
		));
	}

}