<?php 

	class Socialmedias_Controller {
		
		public function __construct() {
			
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {

			// view collection
			$link = $this->request->query('data');
			$this->request->field('form', $link);

			$permission_edit = user::permission('can_edit_socialmedias');
		
			if(!$permission_edit) {
				Message::access_denied();
				url::redirect('/messages/show/access-denied');
			}

			// button: add new
			$link = $this->request->query('add');
			$this->request->field('add', $link);

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'list.php');
			$tpl->data('url', '/applications/modules/socialmedia/list.php');
			$tpl->data('class', 'list-600');
			$this->view->setTemplate('socialmedias', $tpl);

			Compiler::attach(array(
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js'
			));
		}
		
		public function add() {
			
			$permission_edit = user::permission('can_edit_socialmedias');
		
			if(!$permission_edit) {
				Message::access_denied();
				url::redirect('/messages/show/access-denied');
			}

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;


			//dataloaders brands
			$model = new Model(Connector::DB_CORE);
			$result = $model->query("
				SELECT DISTINCT
					brand_id,
					concat(language_name, ' ', brand_name) as name
				FROM brands
				LEFT JOIN languages on language_id = brand_language_id
				ORDER BY language_name, brand_name
			")->fetchAll();
			$dataloader['socialmedia_brand_id'] = _array::extract($result);

			
			//dataloaders social media channels
			$result = $model->query("
				SELECT DISTINCT
					socialmediachannel_id,
					socialmediachannel_name
				FROM socialmediachannels
				ORDER BY socialmediachannel_name
			")->fetchAll();
			$dataloader['socialmedia_channel_id'] = _array::extract($result);

			//dataloaders countries
			$dataloader['socialmedia_country_id'] = Country::loader();

			//dataloaders languages
			$result = $model->query("
				SELECT DISTINCT
					language_id,
					language_name
				FROM brands
				LEFT JOIN languages on language_id = brand_language_id
				ORDER BY language_name
			")->fetchAll();
			$dataloader['socialmedia_language_id'] = _array::extract($result);


			//dataloaders POS locations
			$dataloader['socialmedia_posaddress_id'] = array();
	
			// form hidden vars
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'socialmedia/form.php');
			$tpl->data('data', $data);
			$tpl->data('dataloader', $dataloader);
			$tpl->data('buttons', $buttons);

			$this->view->setTemplate('socialmedia', $tpl);

		
			// script integration
			Compiler::attach(array(
				'/public/scripts/validationEngine/css/validationEngine.jquery.css',
				'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
				'/public/scripts/validationEngine/js/jquery.validationEngine.js',
				'/public/scripts/qtip/qtip.css',
				'/public/scripts/qtip/qtip.js',
				'/public/js/form.validator.js'
			));

		}
		
		public function data() {
				
			$permission_edit = user::permission('can_edit_socialmedias');
		
			if(!$permission_edit) {
				Message::access_denied();
				url::redirect('/messages/show/access-denied');
			}
			
			
			$id = url::param();


			if (!$id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$application = $this->request->application;

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			// modul: brand
			$socialmedia = new Modul($application);
			$socialmedia->setTable('socialmedias');
			$data = $socialmedia->read($id);

			//dataloaders brands
			$model = new Model(Connector::DB_CORE);
			$result = $model->query("
				SELECT DISTINCT
					brand_id,
					concat(language_name, ' ', brand_name) as name
				FROM brands
				LEFT JOIN languages on language_id = brand_language_id
				ORDER BY language_name, brand_name
			")->fetchAll();
			$dataloader['socialmedia_brand_id'] = _array::extract($result);

			
			//dataloaders social media channels
			$result = $model->query("
				SELECT DISTINCT
					socialmediachannel_id,
					socialmediachannel_name
				FROM socialmediachannels
				ORDER BY socialmediachannel_name
			")->fetchAll();
			$dataloader['socialmedia_channel_id'] = _array::extract($result);

			//dataloaders countries
			$dataloader['socialmedia_country_id'] = Country::loader();

			//dataloaders languages
			$result = $model->query("
				SELECT DISTINCT
					language_id,
					language_name
				FROM brands
				LEFT JOIN languages on language_id = brand_language_id
				ORDER BY language_name
			")->fetchAll();
			$dataloader['socialmedia_language_id'] = _array::extract($result);

			//dataloaders POS locations
			$result = $model->query("
				SELECT DISTINCT
					posaddress_id,
					concat(place_name, ': ', posaddress_name) as posname
				FROM posaddresses
				LEFT JOIN places on place_id = posaddress_place_id
				WHERE (posaddress_id = '" . $data['socialmedia_posaddress_id'] . "' OR posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') 
				AND posaddress_country = '" . $data['socialmedia_country_id'] . 
				"' ORDER BY place_name, posaddress_name 
			")->fetchAll();
			$dataloader['socialmedia_posaddress_id'] = _array::extract($result);
		
			$buttons['save'] = true;
					
			// button: delete
			$buttons['delete'] = $this->request->link('/applications/modules/socialmedia/delete.php', array('id'=>$id));
			

			// request vars
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;


			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'socialmedia/form.php');
			$tpl->data('data', $data);
			$tpl->data('dataloader', $dataloader);
			$tpl->data('buttons', $buttons);
			
			$this->view->setTemplate('socialmedia', $tpl);

			// script integration
			Compiler::attach(array(
				'/public/scripts/validationEngine/css/validationEngine.jquery.css',
				'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
				'/public/scripts/validationEngine/js/jquery.validationEngine.js',
				'/public/scripts/qtip/qtip.css',
				'/public/scripts/qtip/qtip.js',
				'/public/js/form.validator.js'
			));

		}
	}