<?php 

	class DesignObjectiveGroups_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() { 
			

			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			if ($permission_edit) {
				$this->request->field('add',$this->request->query('add'));
			}
			
			if ($permission_edit || $permission_view) {
				$this->request->field('data',$this->request->query('data'));
			}
			
			// template: role list
			$this->view->productLines('pagecontent')
			->design('objective.group.list')
			->data('buttons', $buttons);
		}

		public function add() {

			$permission_edit = user::permission('can_edit_catalog');
		
			if($this->request->archived || !$permission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
				
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();

			$model = new Model(Connector::DB_CORE);

			// dataloader: product lines
			/*
			$result = $model->query("
				SELECT product_line_id, product_line_name
				FROM product_lines
				ORDER BY product_line_name
			")->fetchAll();
			
			$dataloader['design_objective_group_product_line'] = _array::extract($result);
			*/
		
			$hidden['design_objective_group_product_line'] = true;
			
			// dataloader: postypes
			$result = $model->query("
				SELECT postype_id, postype_name
				FROM postypes
				ORDER BY postype_name
			")->fetchAll();
			
			$dataloader['design_objective_group_postype'] = _array::extract($result);
		
			// template: application form
			$this->view->productLine('pagecontent')
			->design('objective.group.form')
			->data('data', $data)
			->data('hidden', $hidden)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons);
		}
		
		public function data() {

			$id = url::param();

			$designObjectiveGroup = new Design_Objective_Group();
			$designObjectiveGroup->read($id);

			if (!$designObjectiveGroup->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// dataloader
			$dataloader = array();

			$data = $designObjectiveGroup->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;

			$model = new Model(Connector::DB_CORE);

			// dataloader: product lines
			$result = $model->query("
				SELECT product_line_id, product_line_name
				FROM product_lines
				ORDER BY product_line_name
			")->fetchAll();
			
			$dataloader['design_objective_group_product_line'] = _array::extract($result);
			
			// dataloader: postypes
			$result = $model->query("
				SELECT postype_id, postype_name
				FROM postypes
				ORDER BY postype_name
			")->fetchAll();
			
			$dataloader['design_objective_group_postype'] = _array::extract($result);

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			if ($permission_edit) {
			
				$buttons['save'] = true;

				$integrity = new Integrity();
				$integrity->set($id, 'design_objective_groups', 'system');

				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/design.objective.group.delete.php', array('id'=>$id));
				}
			}
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}

			if ($data['design_objective_group_product_line']) {
				$disabled['design_objective_group_product_line'] = true;
			} else {
				$hidden['design_objective_group_product_line'] = true;
			}

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $designObjectiveGroup->name);
			$this->view->companytabs('pagecontent')->navigation('tab');
		
			// template: form
			$this->view->productLine('pagecontent')
			->design('objective.group.form')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons);
		}


		public function objectives() {

			$id = url::param();
			$param = url::param(1);

			$designObjectiveGroup = new Design_Objective_Group();
			$designObjectiveGroup->read($id);

			if (!$designObjectiveGroup->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			$model = new Model(Connector::DB_CORE);

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $designObjectiveGroup->name);
			$this->view->companytabs('pagecontent')->navigation('tab');
			
			// buttons
			$buttons = array();

			if ($param) {

				$param = (is_numeric($param)) ? $param : null;

				$buttons['back'] = $this->request->query("objectives/$id");

				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				Compiler::attach(DIR_JS."form.validator.js");

				$designObjectiveItem = new Design_Objective_Item();
				$designObjectiveItem->read($param);
				
				$data = $designObjectiveItem->data;
				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;
				$data['design_objective_item_group'] = $id;
				$data['redirect'] = $this->request->query("objectives/$id");

				if ($permission_edit) {
				
					$buttons['save'] = true;

					if ($designObjectiveItem->id) {
						
						$integrity = new Integrity();
						$integrity->set($designObjectiveItem->id, 'design_objective_items', 'system');

						if ($integrity->check()) {
							$buttons['delete'] = $this->request->link('/applications/helpers/design.objective.group.item.delete.php', array('id'=>$param));
						}
					}
				}
				elseif($data) {
					foreach ($data as $key => $value) {
						$disabled[$key] = true;
					}
				}

				// dataloader: product lines
				$result = $model->query("
					SELECT design_objective_group_id, design_objective_group_name
					FROM design_objective_groups
					ORDER BY design_objective_group_name
				")->fetchAll();
				
				$dataloader['design_objective_item_group'] = _array::extract($result);
			
				// template: form
				$this->view->productLine('pagecontent')
				->design('objective.group.item.form')
				->data('data', $data)
				->data('dataloader', $dataloader)
				->data('hidden', $hidden)
				->data('disabled', $disabled)
				->data('buttons', $buttons);
			} 
			else {

				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
				Compiler::attach(DIR_SCRIPTS."table.loader.js");

				if ($permission_edit) {
					$buttons['add'] = $this->request->query("objectives/$id/add");
				}

				$buttons['back'] = $this->request->query();

				$result = $model->query("
					SELECT 
						design_objective_item_id, 
						design_objective_item_name, 
						IF(design_objective_item_isstandard, 'Yes', 'No') AS design_objective_item_isstandard,
						postype_name
					FROM design_objective_items 
					INNER JOIN design_objective_groups ON design_objective_item_group = design_objective_group_id
					INNER JOIN postypes ON design_objective_group_postype = postypes.postype_id
					WHERE design_objective_item_group = $id
					ORDER BY 
						postypes.postype_name
				")->fetchAll();

				$items = _array::datagrid($result);

				// template: form
				$this->view->productLine('pagecontent')
				->design('objective.group.items')
				->data('items', $items)
				->data('id', $id)
				->data('buttons', $buttons);
			}
		}
	}