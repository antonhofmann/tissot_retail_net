<?php 

class Materials_Controller {
	
	public function __construct() {
			
		$this->user = User::instance();
		$this->request = request::instance();
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}
	
	public function index() { 
	
		// template: list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'list.php');

		switch ($this->request->application) {
			
			case 'catalog':

				$permissionEdit = user::permission('can_edit_catalog');
				$permissionEditCertificates = user::permission('can_edit_certificates');
				$permissionView = user::permission('can_browse_catalog_in_admin');

				$tpl->data('url', '/applications/modules/material/list.php');
				$tpl->data('class', 'list-500');

			break;

			case 'lps':
			
				$permissionEdit = user::permission('can_edit_lps_materials');
				$permissionView = user::permission('can_browse_catalog_in_admin');
				
				$tpl->data('url', '/applications/modules/reference/list.php');
				$tpl->data('class', 'list-1400');

			break;
		}

		// link: form
		if ($permissionEdit || $permissionEditCertificates || $permissionView) {
			$link = $this->request->query('data');
			$this->request->field('data', $link);
		}

		// button: add new
		if ($permissionEdit || $permissionEditCertificates) {
			$link = $this->request->query('add');
			$this->request->field('add', $link);
		}

		// button: print
		$link = $this->request->link('/applications/modules/reference/print.php');
		$this->request->field('print', $link);

		// assign template to view
		$this->view->setTemplate('collections', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js'
		));
	}

	public function add() {

		$permissionEdit = false;

		switch ($this->request->application) {
			
			case 'catalog':
				$permissionEdit = user::permission('can_edit_catalog');
				$permissionEditCertificates = user::permission('can_edit_certificates');
				$urlSubmit = '/applications/modules/material/submit.php';
			break;

			case 'lps':
				$permissionEdit = user::permission('can_edit_lps_materials');
				$urlSubmit = '/applications/modules/reference/submit.php';
			break;
		}

		$_CAN_EDIT = $permissionEdit || $permissionEditCertificates ? true : false;
	
		// check access
		if($this->request->archived || !$_CAN_EDIT) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		switch ($this->request->application) {
			
			case 'catalog':
				
				// template
				$template = PATH_TEMPLATES.'material/form.catalog.php';

				// form dataloader
				$data = array();
				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;
				$data['action'] = $this->request->action;
				$data['redirect'] = $this->request->query('data');

			break;

			case 'lps':
	
				// template
				$template = PATH_TEMPLATES.'reference/form.php';

				// db model
				$model = new Model($this->request->application);

				// dataloader: categories
				$categories = $model->query("
					SELECT 
						lps_collection_category_id, 
						lps_collection_category_code 
					FROM lps_collection_categories
					WHERE lps_collection_category_active = 1
					ORDER BY lps_collection_category_code
				")->fetchAll();

				// dataloader: collections
				$collections = $model->query("
					SELECT 
						lps_collection_id, 
						lps_collection_code 
					FROM lps_collections
					WHERE lps_collection_active = 1
					ORDER BY lps_collection_code
				")->fetchAll();

				// dataloader: product groups
				$productGroups = $model->query("
					SELECT 
						lps_product_group_id, 
						lps_product_group_name 
					FROM lps_product_groups
					WHERE lps_product_group_active = 1
					ORDER BY lps_product_group_name
				")->fetchAll();

				// dataloader: product types
				$productTypes = $model->query("
					SELECT 
						lps_product_type_id, 
						lps_product_type_name 
					FROM lps_product_types
					WHERE lps_product_type_active = 1
					ORDER BY lps_product_type_name
				")->fetchAll();

				// dataloader: currencies
				$currencies = $model->query("
					SELECT 
						currency_id, 
						CONCAT(currency_symbol,', ', currency_name) as name
					FROM db_retailnet.currencies
					ORDER BY currency_symbol,currency_name
				")->fetchAll();

				// form dataloader
				$data = array();
				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;
				$data['action'] = $this->request->action;
				$data['redirect'] = $this->request->query('data');
				$data['reference_active'] = 1;

				// dropdowns
				$dataloader = array();
				$dataloader['collection_code'] = _array::extract($collections);
				$dataloader['collection_category_code'] = _array::extract($categories);
				$dataloader['product_group_name'] = _array::extract($productGroups);
				$dataloader['product_type_name'] = _array::extract($productTypes);
				$dataloader['currency'] = _array::extract($currencies);

			break;
		}
		
		// buttons
		$buttons = array();
		$buttons['back'] = $this->request->query();
		$buttons['save'] = $urlSubmit;

		// load template
		$tpl = new Template('pagecontent');
		$tpl->template($template);
		$tpl->data('data', $data);
		$tpl->data('dataloader', $dataloader);
		$tpl->data('buttons', $buttons);

		// assign template to view
		$this->view->setTemplate('material', $tpl);

		// scripts
		Compiler::attach(array(
			'/public/scripts/chain/chained.select.js',
			'/public/scripts/validationEngine/css/validationEngine.jquery.css',
			'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
			'/public/scripts/validationEngine/js/jquery.validationEngine.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/js/form.validator.js'
		));
	}
	


	public function data() {

		$id = url::param();
		$permissionEdit = false;

		switch ($this->request->application) {
			
			case 'catalog':
				$tableName = 'catalog_materials';
				$permissionEdit = user::permission('can_edit_catalog');
				$permissionEditCertificates = user::permission('can_edit_certificates');
				$urlSubmit = '/applications/modules/material/submit.php';
				$urlDelete = '/applications/modules/material/delete.php';
			break;

			case 'lps':
				$tableName = 'lps_references';
				$permissionEdit = user::permission('can_edit_lps_materials');
				$urlSubmit = '/applications/modules/reference/submit.php';
				$urlDelete = '/applications/modules/reference/delete.php';
			break;
		}

		$_CAN_EDIT = $permissionEdit || $permissionEditCertificates ? true : false;

		// get reference
		$modul = new Modul($this->request->application);
		$modul->setTable($tableName);
		$modul->read($id);

		// check access
		if (!$modul->id) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		// buttons
		$buttons = array();
		$buttons['back'] = $this->request->query();
		
		// form data
		$data = array();

		$model = new Model($this->request->application);

		switch ($this->request->application) {
			
			case 'catalog':

				// template
				$template = PATH_TEMPLATES.'material/form.catalog.php';
				
				// form dataloader
				$data = $modul->data;
				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;

			break;

			case 'lps':

				$collection = $modul->data['lps_reference_collection_id'];
				$collectionCategory = $modul->data['lps_reference_collection_category_id'];
				$productGroup = $modul->data['lps_reference_product_group_id'];
				$productSubGroup = $modul->data['lps_reference_product_subgroup_id'];
				$productType = $modul->data['lps_reference_product_type_id'];

				// form data
				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;
				$data['reference_id'] = $modul->data['lps_reference_id'];
				$data['collection_code'] = $collection;
				$data['collection_category_code'] = $collectionCategory;
				$data['product_group_name'] = $productGroup;
				$data['product_subgroup_name'] = $productSubGroup;
				$data['product_type_name'] = $productType;
				$data['reference_ean_number'] = $modul->data['lps_reference_ean_number'];
				$data['reference_code'] = $modul->data['lps_reference_code'];
				$data['reference_name'] = $modul->data['lps_reference_name'];
				$data['reference_price_point'] = $modul->data['lps_reference_price_point'];
				$data['currency'] = $modul->data['lps_reference_currency_id'];
				$data['reference_description'] = $modul->data['lps_reference_description'];
				$data['reference_active'] = $modul->data['lps_reference_active'];

				// dataloaders
				$dataloader = array();

				// dataloader: categories
				$categories = $model->query("
					SELECT 
						lps_collection_category_id, 
						lps_collection_category_code 
					FROM lps_collection_categories
					WHERE lps_collection_category_active = 1 OR lps_collection_category_id = '$collectionCategory'
					ORDER BY lps_collection_category_code
				")->fetchAll();

				// dataloader: collections
				$collections = $model->query("
					SELECT 
						lps_collection_id, 
						lps_collection_code 
					FROM lps_collections
					WHERE lps_collection_active = 1 OR lps_collection_id = '$collection'
					ORDER BY lps_collection_code
				")->fetchAll();

				// dataloader: product groups
				$productGroups = $model->query("
					SELECT 
						lps_product_group_id, 
						lps_product_group_name 
					FROM lps_product_groups
					WHERE lps_product_group_active = 1 OR lps_product_group_id = '$productGroup'
					ORDER BY lps_product_group_name
				")->fetchAll();

				// dataloader: product types
				$productTypes = $model->query("
					SELECT 
						lps_product_type_id, 
						lps_product_type_name 
					FROM lps_product_types
					WHERE lps_product_type_active = 1 OR lps_product_type_id = '$productType'
					ORDER BY lps_product_type_name
				")->fetchAll();

				// dataloader: currencies
				$currencies = $model->query("
					SELECT 
						currency_id, 
						CONCAT(currency_symbol,', ', currency_name) as name
					FROM db_retailnet.currencies
					ORDER BY currency_symbol,currency_name
				")->fetchAll();

				$dataloader['collection_category_code'] = _array::extract($categories);
				$dataloader['collection_code'] = _array::extract($collections);
				$dataloader['product_group_name'] = _array::extract($productGroups);
				$dataloader['product_type_name'] = _array::extract($productTypes);
				$dataloader['currency'] = _array::extract($currencies);

				// header
				$header = $data['reference_code'].", ".$data['reference_name'];

				// template
				$template = PATH_TEMPLATES.'reference/form.php';

			break;
		}

		if (!$this->request->archived) { 
					
			if ($_CAN_EDIT) {
			
				$buttons['save'] = $urlSubmit;

				$integrity = new Integrity();
				$integrity->set($id, $tableName);

				if ($id && $integrity->check()) {
					$buttons['delete'] = $this->request->link($urlDelete, array('id'=>$id));
				}

			} else {
				$disabled = true;
			}

		} else {
			$disabled = true;
		}
		
		// headers
		$this->view->header('pagecontent')->node('header')->data('title', $header);
		$this->view->companytabs('pagecontent')->navigation('tab');
	
		$tpl = new Template('pagecontent');
		$tpl->template($template);
		$tpl->data('data', $data);
		$tpl->data('dataloader', $dataloader);
		$tpl->data('buttons', $buttons);

		// disabled fields
		if ($disabled) {
			$fields = array_keys($data);
			$tpl->data('disabled', array_fill_keys($fields, true));
		}

		$this->view->setTemplate('material', $tpl);

		Compiler::attach(array(
			'/public/scripts/chain/chained.select.js',
			'/public/scripts/validationEngine/css/validationEngine.jquery.css',
			'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
			'/public/scripts/validationEngine/js/jquery.validationEngine.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/js/form.validator.js'
		));
	}
	


	public function bulk() {

		$id = url::param();
		$permissionEdit = false;

		switch ($this->request->application) {

			case 'lps':
				$tableName = 'lps_references';
				$permissionEdit = user::permission('can_edit_lps_materials');
			break;
		}

		// get reference
		$modul = new Modul($this->request->application);
		$modul->setTable($tableName);
		$modul->read($id);

		// check access
		if (!$modul->id) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		// form data
		$data = array();
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['reference_id'] = $modul->data['lps_reference_id'];
		$data['reference_code'] = $modul->data['lps_reference_code'];
		$data['reference_name'] = $modul->data['lps_reference_name'];

		// buttons
		$buttons = array();
		$buttons['back'] = $this->request->query();

		// headers
		$header = $data['reference_code'].", ".$data['reference_name'];
		$this->view->header('pagecontent')->node('header')->data('title', $header);
		$this->view->companytabs('pagecontent')->navigation('tab');
	
		/*
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'reference/form.php');
		$tpl->data('data', $data);
		$tpl->data('dataloader', $dataloader);
		$tpl->data('buttons', $buttons);

		$this->view->setTemplate('reference', $tpl);
		*/

		Compiler::attach(array(
			'/public/scripts/chain/chained.select.js',
			'/public/scripts/validationEngine/css/validationEngine.jquery.css',
			'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
			'/public/scripts/validationEngine/js/jquery.validationEngine.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/js/form.validator.js'
		));
	}
}