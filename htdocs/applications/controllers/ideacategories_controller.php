<?php 

	class IdeaCategories_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			$this->translate = Translate::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() { 
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// request form: link add
			$field_add = $this->request->query('add');
			$this->request->field('add', $field_add);
			
			// request form: link edit
			$this->request->field('data', $this->request->query('data'));
		
			$this->view->ideas('pagecontent')->idea('category.list');
		}
		
		public function add() {
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
				
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
			
			// dataloader: dbs
			$dataloader['idea_category'] = array();
		
			// template: application form
			$this->view->ideacategory('pagecontent')
			->idea('category.data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons);
		}
		
		public function data() {
			
			$id = url::param();
			
			$ideaCategory = new IdeaCategory();
			$ideaCategory->read($id);
			
			if (!$ideaCategory->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// form datalaoder
			$data = $ideaCategory->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
			
			$integrity = new Integrity();
			$integrity->set($id, 'ideas');
			
			// button: delete
			if ($integrity->check()) {
				$buttons['delete'] = $this->request->link("/applications/helpers/idea.category.delete.php", array('id' => $id));
			}

			// dataloader: dbs
			$dataloader['idea_category_id'] = array();
			
			// template: header
			$this->view->header('pagecontent')->node('header')->data('title', $ideaCategory->name);
			$this->view->tabs('pagecontent')->navigation('tab');
			
			$this->view->applicationForm('pagecontent')
			->idea('category.data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}
	