<?php 

	class Roles_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() { 
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			// request form fields
			$this->request->field('add',$this->request->query('add'));
			$this->request->field('form',$this->request->query('role'));
			
			// template: role list
			$this->view->rols_list('pagecontent')
			->role('list')
			->data('buttons', $buttons);
		}
		
		public function add() {
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			$model = new Model(Connector::DB_CORE);
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['redirect'] = $this->request->query('role');

			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
			
			// order states
			$states = $model->query("
				SELECT DISTINCT
					order_state_id,
					CONCAT(order_state_code, ' ', order_state_name) AS orderstate
				FROM order_states
				LEFT JOIN order_state_groups ON order_state_group_id = order_state_group
				LEFT JOIN order_types ON order_type_id = order_state_group_order_type
				WHERE order_type_id=1
				ORDER BY order_state_code
			")->fetchAll();
			
			$states = _array::extract($states);
			
			// dataloaders
			$dataloader['role_order_state_visible_from'] = $states;
			$dataloader['role_order_state_visible_to'] = $states;
		
			// template: role form
			$this->view->role('pagecontent')
			->role('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons);
		}
		
		public function role() {

			$id = url::param();
			
			$role = new Role();
			$data = $role->read($id);
			
			// check access to staff
			if (!$role->id) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// form dataloader
			$data = $role->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			
			// buttons
			$butttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
			$buttons['delete'] = $this->request->link('/applications/helpers/role.delete.php', array('id'=>$id));
			
			// db model
			$model = new Model(Connector::DB_CORE);
			
			// order states
			$states = $model->query("
				SELECT DISTINCT
					order_state_id,
					CONCAT(order_state_code, ' ', order_state_name) AS orderstate
				FROM order_states
				LEFT JOIN order_state_groups ON order_state_group_id = order_state_group
				LEFT JOIN order_types ON order_type_id = order_state_group_order_type
				WHERE order_type_id=1
				ORDER BY order_state_code
			")->fetchAll();
			
			$states = _array::extract($states);
			
			// dataloaders
			$dataloader['role_order_state_visible_from'] = $states;
			$dataloader['role_order_state_visible_to'] = $states;
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $role->code.', '.$role->name);

			// template: navigation tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			// template: role form
			$this->view->role('pagecontent')
			->role('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function permissions() {
				
			$id = url::param();
			
			$role = new Role();
			$data = $role->read($id);
			
			// check access to staff
			if (!$role->id) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			// add role request form
			$this->request->field('id',$id);
			
			// buttons
			$butttons = array();
			$buttons['back'] = $this->request->query();
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $role->code.', '.$role->name);
			
			// template: navigation tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			// tempalate: role permissions list
			$this->view->rolePermissionsList('pagecontent')
			->role('permission.list')
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function navigations() {
		
			$id = url::param();
				
			$role = new Role();
			$data = $role->read($id);
			
			// check access to staff
			if (!$role->id) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
			Compiler::attach(DIR_CSS."jquery.ui.css");
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
			Compiler::attach(DIR_SCRIPTS."dynatree/src/skin/ui.dynatree.css");
			Compiler::attach(DIR_SCRIPTS."dynatree/src/jquery.dynatree.js");
			
			// add role request form
			$this->request->field('id',$id);
			
			// buttons
			$butttons = array();
			$buttons['back'] = $this->request->query();
				
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $role->code.', '.$role->name);
			
			// template: navigation tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
				
			$this->view->roleNavigationsList('pagecontent')
			->role('navigation.list')
			->data('buttons', $buttons)
			->data('id', $id);
		}
	
		public function applications() {
			
			$id = url::param();
				
			$role = new Role();
			$data = $role->read($id);
			
			// check access to staff
			if (!$role->id) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			// add role request form
			$this->request->field('id',$id);
			
			// buttons
			$butttons = array();
			$buttons['back'] = $this->request->query();
				
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $role->code.', '.$role->name);
			
			// template: navigation tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			// template: role applications list
			$this->view->roleApplicationsList('pagecontent')
			->role('application.list')
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}