<?php

	class Staff_Controller {

		public function __construct() {

			$this->user = User::instance();
			$this->request = request::instance();
			$this->translate = Translate::instance();

			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}

		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			
			$requestFields = array();
			$cen_edit = user::permission(Staff::PERMISSION_EDIT);
			$cen_edit_limited = user::permission(Staff::PERMISSION_EDIT_LIMITED);

			if (!$this->request->archived && ($cen_edit || $cen_edit_limited) ) {
				$requestFields['add'] = $this->request->query('add');
			}

			$requestFields['form'] = $this->request->query('data');
			$requestFields['export'] = $this->request->link('/applications/exports/excel/staff.php');

			// template: staff list
			$this->view->stafflist('pagecontent')->staff('list')->data('requestFields', $requestFields);
		}

		public function add() {
			
			// permissions
			$permission_edit = user::permission(Staff::PERMISSION_EDIT);
			$permissions_view = user::permission(Staff::PERMISSION_VIEW);
			$permission_edit_limited = user::permission(Staff::PERMISSION_EDIT_LIMITED);

			if( $this->request->archived || (!$permission_edit AND !$permission_edit_limited)) {
				Message::access_denied();
				url::redirect('/'.$this->request->application.'/'.$this->request->controller);
			}

			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
			
			// form loader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] =  $this->request->query('data');

			$client_filter = array(
				'address_type = 1',
				'address_active = 1'
			);

			if (!$permission_edit && !$permissions_view) {
				array_push($client_filter, '(address_parent = '.$this->user->address.' OR address_id = '.$this->user->address.')');
			}

			// dataloader: client
			$dataloader['mps_staff_address_id'] = company::loader($client_filter);

			// dataloader staff types
			$dataloader['mps_staff_staff_type_id'] = Staff_Type::loader($this->request->application);

			//dataloader: user sex
			$dataloader['mps_staff_sex'] = array(
				'm' => $this->translate->male,
				'f' => $this->translate->female
			);

			// template: staff form
			$this->view->staff_form('pagecontent')
			->staff('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons);
		}

		public function data() {

			$id = url::param();
			
			// permissions
			$permission_edit = user::permission(Staff::PERMISSION_EDIT);
			$permissions_view = user::permission(Staff::PERMISSION_VIEW);
			$permission_edit_limited = user::permission(Staff::PERMISSION_EDIT_LIMITED);

			$staff = new Staff();
			$staff->read($id);
			
			// check access to staff
			if (!$staff->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}

			$canEdit = $staff->canEdit();
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");

			// form dataloader
			$data = $staff->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;

			// button back
			$buttons['back'] = '/'.$this->request->application.'/'.$this->request->controller;

			if ( !$this->request->archived && $canEdit) { 
				
				$buttons['save'] = true;

				$integrity = new Integrity();
				$integrity->set($id, 'mps_stafs');

				if ($integrity->check()) {
					$buttons['delete'] = url::build('/applications/helpers/staff.delete.php', array('id'=>$id));
				}
				
			} else {
				$fields = array_keys($data);
				$disabled = array_fill_keys($fields, true);
			}

			// dataloader: clients
			$client_filter = array(
				'address_type = 1',
				'address_active = 1'
			);
			
			if (!$canEdit) {
				array_push($client_filter, '(address_parent = '.$this->user->address.' OR address_id = '.$this->user->address.')');
			}
			
			// dataloader: client
			$dataloader['mps_staff_address_id'] = company::loader($client_filter);
			$dataloader['mps_staff_staff_type_id'] = Staff_Type::loader($this->request->application);
			$dataloader['mps_staff_sex'] = array(
				'm' => $this->translate->male,
				'f' => $this->translate->female
			);

			// template: header
			$this->view->staff_header('pagecontent')->node('header')->data('title', $staff->header());

			// template: staff form
			$this->view->staffForm('pagecontent')
			->staff('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}