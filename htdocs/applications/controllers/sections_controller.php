<?php


class Sections_Controller {

	static public $permissions = array();

	public function __construct() {

		Settings::init()->theme = 'swatch';
		$this->request = request::instance();
		$this->view = new View();
		$this->view->pageclass = 'theme-news news-sections-list';
		$this->view->pagetitle = $this->request->title;
		$this->view->userboxMenu('navigations')->navigation('userbox');
		$this->view->categories('navigations')->navigation('categories');
		$this->view->appTabs('navigations')->navigation('appnav');

		static::$permissions = array(
			'edit' => user::permission('can_edit_news_system_data')
		);

		Compiler::attach('/public/themes/swatch/css/news.css');
	}


	public function index() {

		$application = $this->request->application;

		$model = new Model($application);

		switch ($application) {
			
			case 'news':

				// get sections
				$sth = $model->db->prepare("
					SELECT DISTINCT
						news_section_id AS id,
						news_section_name AS name,
						IF (news_section_confirm_user_id, CONCAT(user_firstname, ' ', user_name), '' ) AS confirm_person
					FROM news_sections
					LEFT JOIN db_retailnet.users on user_id = news_section_confirm_user_id
					ORDER BY news_section_order, news_section_name 
				");

				$sth->execute();
				$sections = $sth->fetchAll();

				// view template
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'news/section/list.php');
				$tpl->data('title', $this->request->title);
				$tpl->data('sections', $sections);
				$this->view->setTemplate('sections', $tpl);

				Compiler::attach(array(
					'/public/themes/swatch/css/news.sections.css',
					'/public/js/news.sections.js'
				));

			break;
		}
			
	}

	public function add() {

		$application = $this->request->application;

		$model = new Model($application);

		switch ($application) {
			
			case 'news':

				if (!static::$permissions['edit']) {
					Message::access_denied();
					url::redirect($this->request->query());
				}

				$buttons['save'] = true;

				$data = array();
				$data['application'] = $application;

				$users = $model->query("
					SELECT DISTINCT 
						db_retailnet.users.user_id AS id, 
						CONCAT(db_retailnet.users.user_firstname, ' ', db_retailnet.users.user_name) AS name
					FROM db_retailnet.users 
					INNER JOIN db_retailnet.user_roles ON db_retailnet.users.user_id = db_retailnet.user_roles.user_role_user
					INNER JOIN db_retailnet.roles ON db_retailnet.roles.role_id = user_role_role
					INNER JOIN db_retailnet.role_permissions ON role_permission_role = user_role_role
					INNER JOIN db_retailnet.permissions ON permission_id = role_permission_permission
					WHERE db_retailnet.users.user_active = 1 AND permissions.permission_name = 'can_confirm_news_article'
					ORDER BY name
				")->fetchAll();

				// view template
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'news/section/form.php');
				$tpl->data('buttons', $buttons);
				$tpl->data('data', $data);
				$tpl->data('title', $this->request->title);
				$tpl->data('users', $users);
				$this->view->setTemplate('section', $tpl);

				Compiler::attach(array(
					'/public/scripts/formvalidation/css/formValidation.css',
					'/public/scripts/formvalidation/js/formValidation.js',
					'/public/scripts/formvalidation/js/framework/bootstrap.min.js',
					'/public/js/news.section.js'
				));

			break;
		}
	}

	public function data() {

		$id = url::param();
		$application = $this->request->application;

		$model = new Model($application);

		switch ($application) {
			
			case 'news':

				$buttons = array();

				$section = new Modul($application);
				$section->setTable('news_sections');
				$section->read($id);

				if ( !$section->id ) {
					Message::access_denied();
					url::redirect($this->request->query());
				}

				$users = $model->query("
					SELECT DISTINCT 
						db_retailnet.users.user_id AS id, 
						CONCAT(db_retailnet.users.user_firstname, ' ', db_retailnet.users.user_name) AS name
					FROM db_retailnet.users 
					INNER JOIN db_retailnet.user_roles ON db_retailnet.users.user_id = db_retailnet.user_roles.user_role_user
					INNER JOIN db_retailnet.roles ON db_retailnet.roles.role_id = user_role_role
					INNER JOIN db_retailnet.role_permissions ON role_permission_role = user_role_role
					INNER JOIN db_retailnet.permissions ON permission_id = role_permission_permission
					WHERE db_retailnet.users.user_active = 1 AND permissions.permission_name = 'can_confirm_news_article'
					ORDER BY name
				")->fetchAll();

				if (static::$permissions['edit']) {
					
					$buttons['save'] = true;

					$integrity = new Integrity();
					$integrity->set($id, 'news_sections');
					
					if ($integrity->check()) {
						$buttons['delete'] = $this->request->link('/applications/modules/news/section/delete.php', array('id'=>$id));
					}
				}

				// view template
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'news/section/form.php');
				$tpl->data('id', $id);
				$tpl->data('buttons', $buttons);
				$tpl->data('title', $this->request->title);
				$tpl->data('data', $section->data);
				$tpl->data('users', $users);
				$this->view->setTemplate('section', $tpl);

				Compiler::attach(array(
					'/public/scripts/formvalidation/css/formValidation.min.css',
					'/public/scripts/formvalidation/js/formValidation.js',
					'/public/scripts/formvalidation/js/framework/bootstrap.min.js',
					'/public/scripts/bsdialog/bsdialog.min.css',
					'/public/scripts/bsdialog/bsdialog.min.js',
					'/public/js/news.section.js'
				));

			break;
		}
	}
}