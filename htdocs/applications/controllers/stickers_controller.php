<?php

class Stickers_Controller {

	public function __construct() {
		 
	}

	public function index() { 

		Settings::init()->theme = 'swatch';
		$this->request = request::instance();
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;

		$this->view->pageclass = 'theme-news';
		$this->view->userboxMenu('navigations')->navigation('userbox');
		$this->view->categories('navigations')->navigation('categories');
		$this->view->appTabs('navigations')->navigation('appnav');

		$model = new Model($this->request->application);

		$sth = $model->db->prepare("
			SELECT
				sticker_id AS id,
				sticker_title AS title,
				sticker_image AS image,
				CONCAT('/gazette/stickers/data/', sticker_id) AS url
			FROM stickers
			ORDER BY date_created DESC
		");

		$sth->execute();
		$stickers = $sth->fetchAll();

		$buttons = array();
		$buttons['add'] = $this->request->query("data/add");

		// view template
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'sticker/list.php');
		$tpl->data('stickers', $stickers);
		$tpl->data('buttons', $buttons);
		$this->view->setTemplate('stickers', $tpl);

		Compiler::attach(array(
			'/public/themes/swatch/css/news.css',
			'/public/scripts/bsdialog/bsdialog.min.css',
			'/public/scripts/bsdialog/bsdialog.min.js',
			'/public/themes/swatch/css/stickers.css',
			'/public/themes/swatch/css/sticker.css'
		));
	}

	public function data() {

		Settings::init()->theme = 'swatch';
		$this->request = request::instance();
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;

		$id = url::param();
		$id = $id=='add' ? null : $id;

		$sticker = new Modul($this->request->application);
		$sticker->setTable('stickers');
		$sticker->read($id);

		if ($id && !$sticker->id) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$this->view->pageclass = 'theme-news';
		$this->view->userboxMenu('navigations')->navigation('userbox');
		$this->view->categories('navigations')->navigation('categories');
		$this->view->appTabs('navigations')->navigation('appnav');

		$buttons = array();
		$buttons['back'] = $this->request->query();
		$buttons['save'] = $this->request->query("submit");
		$buttons['delete'] = $id ? $this->request->query("delete/$id") : false;

		$data = $sticker->data;
		$data['application'] = $this->request->application;
		$data['user'] = User::instance()->id;

		// view template
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'sticker/form.php');
		$tpl->data('title', $this->request->title);
		$tpl->data('data', $data);
		$tpl->data('buttons', $buttons);
		$this->view->setTemplate('sticker', $tpl);

		Compiler::attach(array(
			'/public/themes/swatch/css/news.css',
			'/public/themes/swatch/css/stickers.css',
			'/public/themes/swatch/css/sticker.css',
			'/public/scripts/formvalidation/css/formValidation.css',
			'/public/scripts/formvalidation/js/formValidation.js',
			'/public/scripts/formvalidation/js/framework/bootstrap.min.js',
			'/public/scripts/bsdialog/bsdialog.min.css',
			'/public/scripts/bsdialog/bsdialog.min.js',
			'/public/scripts/croppic/croppic.css',
			'/public/scripts/croppic/croppic.js',
			'/public/js/sticker.js'
		));
	}

	public function submit() {

		$response = array();
		$error = false;

		$request = Request::instance();
		$user = User::instance();
		$translate = Translate::instance();

		if (!Request::isAjax() || !$user->id) {
			$error = true;
			$response['notifications'][] = array(
				'type' => 'error',
				'title' => 'Error',
				'text' => 'Bad request.'
			);
		}

		$id = $_REQUEST['sticker_id'];
		$application = $_REQUEST['application'];
		$title = $_REQUEST['sticker_title'];
		$image = $_REQUEST['sticker_image'];
		$hasupload = $_REQUEST['hasupload'];

		$sticker = new Modul($application);
		$sticker->setTable('stickers');
		$sticker->read($id);

		if (!$error && $id && !$sticker->id) {
			$error = true;
			$response['notifications'][] = array(
				'type' => 'error',
				'title' => 'Error',
				'text' => 'Sticker not found'
			);
		}

		if (!$error) {

			if ($hasupload) {

				$image = Upload::move($image, '/data/stickers');

				// remove old sticker image
				if ($sticker->data['sticker_image']) {
					$oldSticker = $_SERVER['DOCUMENT_ROOT'].$sticker->data['sticker_image'];
				}
			}

			$data = array();
			$data['sticker_title'] = $title;
			$data['sticker_image'] = $image;

			if ($sticker->id) {
				$data['date_modified'] = date('Y-m-d H:i:s');
				$data['user_modified'] = $user->id;
				$response['success'] = $sticker->update($data);
				$message = $response['success'] ? $translate->message_request_updated : $translate->message_request_failure;
			} else {
				$data['user_created'] = $user->id;
				$response['success'] = $sticker->create($data);
				$message = $response['success'] ? $translate->message_request_inserted : $translate->message_request_failure;
				$response['redirect'] = '/'.$application.'/stickers/data/'.$sticker->id;
			}

			$response['notifications'][] = array(
				'type' => $response['success'] ? 'notice' : 'error',
				'text' => $message
			);

			if ($response['success'] && $hasupload) {
				
				$response['id'] = $sticker->id;
				$response['file'] = $image;

				if ($oldSticker && file_exists($oldSticker)) {
					//File::remove($oldSticker);
				}
			}
		}

		ob_clean();
		header('Content-Type: text/json');
		echo json_encode($response);
	}

	public function delete() {

		$response = array();
		$error = false;
		$id = Url::param();

		$request = Request::instance();
		$user = User::instance();
		$translate = Translate::instance();

		$sticker = new Modul($request->application);
		$sticker->setTable('stickers');
		$sticker->read($id);

		if (!Request::isAjax() || !$user->id || !$sticker->id) {
			$error = true;
			$response['notifications'][] = array(
				'type' => 'error',
				'title' => 'Error',
				'text' => 'Bad request.'
			);
		}

		if (!$error) {

			$file = $_SERVER['DOCUMENT_ROOT'].$sticker->data['sticker_image'];

			if (file_exists($file)) {
				File::remove($file);
			}

			$response['success'] = $sticker->delete();
			$response['redirect'] = $response['success'] ? '/'.$request->application.'/stickers' : null;

			$response['notifications'][] = array(
				'type' => $response['success'] ? 'notice' : 'error',
				'text' => $response['success'] ? $translate->message_request_deleted : $translate->message_request_failure
			);
		}

		ob_clean();
		header('Content-Type: text/json');
		echo json_encode($response);
	}

	public function update() {

		$response = array();
		$error = false;

		$application = $_REQUEST['application'];

		$request = Request::instance();
		$user = User::instance();
		$translate = Translate::instance();

		$article = new Modul($application);
		$article->setTable('news_articles');
		$article->read($_REQUEST['article']);

		if (!Request::isAjax() || !$user->id || !$article->id) {
			$error = true;
			$response['notifications'][] = array(
				'type' => 'error',
				'title' => 'Error',
				'text' => 'Bad request.'
			);
		}

		if (!$error) {
			
			$dataSticker = $article->data['news_article_sticker'];
			$dataSticker = $dataSticker ? unserialize($dataSticker) : array();

			if ($_REQUEST['sticker']) {
				
				$data['news_article_sticker'] = serialize(array_merge($dataSticker, array_filter(array(
					'id' => $_REQUEST['sticker'],
					'width' => $_REQUEST['width'],
					'height' => $_REQUEST['height'],
					'left' => $_REQUEST['left'],
					'oleft' => $_REQUEST['oleft'] ?: "0%",
					'top' => $_REQUEST['top'],
					'otop' => $_REQUEST['otop'] ?: "0%",
					'background-image' => $_REQUEST['background-image']
				))));

			} else {
				$data['news_article_sticker'] = null;
			}

			$response['success'] = $article->update($data);
		}

		ob_clean();
		header('Content-Type: text/json');
		echo json_encode($response);
	}
}