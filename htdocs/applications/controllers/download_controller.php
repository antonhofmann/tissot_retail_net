<?php


class Download_Controller {


	public function news() {

		$url = explode('/',url::get());  

		$file = base64_decode($url[2]); 
		$file = str_replace(' ', '_', $file);

		$newsletterID = $url[3];
		$userID = $url[4] ?: User::instance()->id;
		$additionalUserId = $url[5];
		
		// article id
		$path = explode('/',$file);
		$article = $path[4];
		
		if (!file_exists($_SERVER['DOCUMENT_ROOT'].$file)) { 
			url::redirect("/messages/show/access_denied");
		}

		if ($article) {

			$model = new Model('news');

			// track: news article download file
			$sth = $model->db->prepare("
				INSERT INTO news_article_tracks (
					news_article_track_article_id,
					news_article_track_newsletter_id,
					news_article_track_user_id,
					news_article_track_additional_user_id,
					news_article_track_type_id,
					news_article_track_entity
				)
				VALUES (?, ?, ?, ?, ?, ?)
			");

			$sth->execute(array($article, $newsletterID, $userID, $additionalUserId, 48, $file));
		}

		// full path
		$file = $_SERVER['DOCUMENT_ROOT'].$file;

		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		
		ob_start();
		readfile($file);
		
		exit;
	}

	public function newsletter() {

		$user = User::instance();

		$url = explode('/',url::get()); 

		// file
		$file = base64_decode($url[2]); 
		$file = str_replace(' ', '_', $file);

		if (!file_exists($_SERVER['DOCUMENT_ROOT'].$file)) { 
			url::redirect("/messages/show/access_denied");
		}

		// newsletter id
		$newsletter = $url[3];
		
		// article id
		$userId = $url[4] ?: User::instance()->id;
		$additionalUserId = $url[5];

		if ($newsletter && ($userId || $additionalUserId)) {

			$model = new Model('news');

			// track: news article download file
			$sth = $model->db->prepare("
				INSERT INTO newsletter_tracks (
					newsletter_track_newsletter_id,
					newsletter_track_user_id,
					newsletter_track_additional_user_id,
					newsletter_track_type_id,
					newsletter_track_entity
				)
				VALUES (?,?,?,?,?)
			");

			$sth->execute(array($newsletter, $userId, $additionalUserId, 22, $file ));
		}

		// full path
		$file = $_SERVER['DOCUMENT_ROOT'].$file;

		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		
		ob_start();
		readfile($file);
		
		exit;
	}

	public function tmp() {

		$url = explode('/',url::get());
		
		$file = base64_decode($url[2]);
		$file = str_replace(' ', '_', $file);
		$file = $_SERVER['DOCUMENT_ROOT'].$file;

		if (!file_exists($file)) {
			url::redirect("/messages/show/access_denied");
			return false;
		}

		$filesize = filesize($file);

		// check if temporary file exist
		if ($url[3]=='check') {

			header('Content-Type: text/json');

			echo json_encode(array(
				'success' => true,
				'file' => $file,
				'filesize' => $filesize
			));

			exit;
		}

		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Transfer-Encoding: binary');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . $filesize);
		
		ob_start();
		readfile($file);
		
		@chmod($file, 0666);
		@unlink($file);
		
		exit;
	}

	public function newsletterTrack() {

		$url = explode('/',url::get());
		
		$newsletter = $url[2];
		$user = $url[3];
		$type = $url[4];
		$additionalRecipient = $url[5];
		$entity = $url[6];

		if (!$newsletter || !$user || !$type) {
			return false;
		}

		$model = new Model('news');

		$sth = $model->db->prepare("
			INSERT INTO newsletter_tracks (
				newsletter_track_newsletter_id,
				newsletter_track_user_id,
				newsletter_track_additional_user_id,
				newsletter_track_type_id,
				newsletter_track_entity
			)
			VALUES (?,?,?,?,?)
		");

		$sth->execute(array($newsletter, $user, $additionalRecipient, $type, $entity));

		if ($type==18) {
			header('Content-Type: image/png');
			echo base64_decode('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII=');
			exit;
		}
	}


	public function data() {

		$url = url::get();
		$file = str_replace('download/data', '', $url);
		$file = $_SERVER['DOCUMENT_ROOT'].$file;

		if (User::instance()->id && file_exists($file) && strpos($file, 'index.php')==false) { 

			$type = mime_content_type($file);

			header("Content-Type: $type");
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			
			ob_start();
			readfile($file);
		}

		exit;
	}


	/**
	 * Load files for externe sources (email clients, streams, etc...)
	 * @return [type] [description]
	 */
	public function get() {

		$url = url::get();
		$file = str_replace('download/get/', '', $url);
		$file = base64_decode($file);
		$file = str_replace(' ', '_', $file);
		//$file = str_replace('http://retailnet.tissot.ch', '', $file);
		$file = str_replace('http://'.$_SERVER['SERVER_NAME'], null, $file);
		$file = $_SERVER['DOCUMENT_ROOT'].$file;

		if (file_exists($file) && strpos($file, 'index.php')==false) { 

			$type = mime_content_type($file);

			header("Content-Type: $type");
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			
			ob_start();
			readfile($file);
		}

		exit;
	}

}