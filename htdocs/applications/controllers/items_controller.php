<?php 

/**
 * Items conmtrolle
 */
class Items_Controller {
	
	public function __construct() {
			
		$this->user = User::instance();
		$this->request = request::instance();
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}
	
	public function index() {

		$auth = User::instance();
		$translate = Translate::instance(); 

		Compiler::attach(array(
			"/public/scripts/dropdown/dropdown.css",
			"/public/scripts/dropdown/dropdown.js",
			"/public/scripts/table.loader.js",
			"/public/scripts/nestable/nestable.min.css",
			"/public/scripts/nestable/nestable.min.js",
			"/public/css/printbox.css",
			"/public/js/printbox.js"
		));
        
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		$this->request->field('type', 1);
		
		if ($_CAN_EDIT) {
			$this->request->field('add',$this->request->query('add'));
		}
		
		$this->request->field('user_id', $auth->id);
		$this->request->field('data',$this->request->query('data'));
		$this->request->field('print', $this->request->link('/applications/exports/excel/items.php', array('type' => Item::TYPE_STANDARD)));

		// print box
		$table = new DB_Table();
		$table->read_from_name('items');
		$printFields = @unserialize($table->export_fields) ?: array();
		$printFields[] = 'supplying_groups';
		$printFields[] = 'supplier_item_price';
		$printFields[] = 'supplier_address';

		$fields = User::getPrintFields('items.print', $printFields);

		$printBox = ui::printBox($fields, array(
			'id' => 'printbox-items',
			'action' => $urlPrint
		));

		
		// template: role list
		$this->view->itemList('pagecontent')
		->item('list')
		->data('namespace', 'items')
		->data('printBox', $printBox)
		->data('buttons', $buttons);
	}

	public function standardSpecial() { 

		Compiler::attach(array(
			"/public/scripts/dropdown/dropdown.css",
			"/public/scripts/dropdown/dropdown.js",
			"/public/scripts/table.loader.js"
		));
        
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		$this->request->field('type', Item::TYPE_SPECIAL);
		
		if ($_CAN_EDIT) {
			$this->request->field('add',$this->request->query('add/'.Item::TYPE_SPECIAL));
		}
		
		if ($_CAN_EDIT || $permission_view) {
			$this->request->field('data',$this->request->query('data'));
			$this->request->field('print', $this->request->link('/applications/exports/excel/items.php', array('type' => Item::TYPE_SPECIAL)));
		}
		
		// template: role list
		$this->view->itemList('pagecontent')
		->item('list')
		->data('namespace', 'items table-xs')
		->data('buttons', $buttons);
	}

	public function  costEstimations() { 

		Compiler::attach(array(
			"/public/scripts/dropdown/dropdown.css",
			"/public/scripts/dropdown/dropdown.js",
			"/public/scripts/table.loader.js"
		));
        
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		$this->request->field('type', Item::TYPE_COST_ESTIMATION);
		
		if ($_CAN_EDIT) {
			$this->request->field('add',$this->request->query('add/'.Item::TYPE_COST_ESTIMATION));
		}
		
		if ($_CAN_EDIT || $permission_view) {
			$this->request->field('data',$this->request->query('data'));
			$this->request->field('print', $this->request->link('/applications/exports/excel/items.php', array('type' => Item::TYPE_COST_ESTIMATION)));
		}
		
		// template: role list
		$this->view->itemList('pagecontent')
		->item('list')
		->data('namespace', 'items table-xs')
		->data('buttons', $buttons);
	}

	public function add() {

		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;
	
		if($this->request->archived || !$_CAN_EDIT ) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$type = url::param() ?: Item::TYPE_STANDARD;
		
		Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
		Compiler::attach(DIR_SCRIPTS."chain/chained.select.js");
		Compiler::attach(DIR_JS."form.validator.js");
		Compiler::attach(DIR_JS."item.js");
		
		// form dataloader
		$data = array();
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['action'] = $this->request->action;
		$data['item_active'] = 1;
		$data['redirect'] = $this->request->query('data');

		if ($type) {

			$data['item_type'] = $type;
			$hidden['item_type'] = true;
		}

		if ($type==Item::TYPE_SPECIAL) {
			Menu::instance()->activate("catalog/items/standardSpecial");
			Menu::instance()->deactivate("catalog/items");
		}

		if ($item->type==Item::TYPE_COST_ESTIMATION) {
			Menu::instance()->activate("catalog/items/costEstimations");
			Menu::instance()->deactivate("catalog/items");
		}
			
		// buttons
		$buttons = array();
		$buttons['save'] = true;
		
		if ($type==Item::TYPE_COST_ESTIMATION) $action = 'costEstimations';
		elseif ($type==Item::TYPE_SPECIAL) $action = 'standardSpecial';
		
		$buttons['back'] = $this->goBack(null, $action);

		$model = new Model(Connector::DB_CORE);

		// item types
		if (!$type) {
			$result = $model->query("
				SELECT item_type_id, item_type_name 
				FROM item_types 
				WHERE item_type_id = 6 or item_type_id <= 3 
				ORDER BY item_type_id
			")->fetchAll();

			$dataloader['item_type'] = _array::extract($result); 
		}

		// item categories
		$result = $model->query("
			SELECT item_category_id, item_category_name 
			FROM item_categories
			WHERE item_category_active = 1
			ORDER BY item_category_name
		")->fetchAll();

		$dataloader['item_category'] = _array::extract($result); 
		
		// item cost group
		$result = $model->query("
			SELECT project_cost_groupname_id, project_cost_groupname_name 
			FROM project_cost_groupnames
			WHERE  project_cost_groupname_active = 1 
			ORDER BY project_cost_groupname_name
		")->fetchAll();

		$dataloader['item_cost_group'] = _array::extract($result); 						
		
		// item units
		$result = $model->query("
			SELECT unit_id, unit_name 
			FROM units 
			ORDER BY unit_name
		")->fetchAll();

		$dataloader['item_unit'] = _array::extract($result);


		// item packaging type
		$result = $model->query("
			SELECT packaging_type_id, packaging_type_name 
			FROM packaging_types 
			ORDER BY packaging_type_name
		")->fetchAll();

		$dataloader['item_packaging_type'] = _array::extract($result);

		// item slave
		$hidden['item_slave_item'] = true;

		// item price disabled
		$hidden['item_price'] = true;
		$hidden['supplier_item_price'] = true;

		if ($type==Item::TYPE_SPECIAL || $type==Item::TYPE_COST_ESTIMATION) {
			$hidden['item_watches_displayed'] = true;
			$hidden['item_watches_stored'] = true;
			$hidden['item_materials'] = true;
			$hidden['item_electrical_specifications'] = true;
			$hidden['item_install_requirements'] = true;
			$hidden['item_regulatory_approvals'] = true;
			$hidden['item_stock_property_of_swatch'] = true;
			$hidden['item_visible_in_production_order'] = true;
			$hidden['item_is_dr_swatch_furniture'] = true;
			$hidden['item_visible_in_mps'] = true;
			$hidden['item_addable_in_mps'] = true;
		}

		if ($type==Item::TYPE_COST_ESTIMATION) {
			$hidden['item_price'] = true;
		}
	
		// template: application form
		$this->view->productLine('pagecontent')
		->item('form')
		->data('data', $data)
		->data('hidden', $hidden)
		->data('disabled', $disabled)
		->data('dataloader', $dataloader)
		->data('materials', $materials)
		->data('buttons', $buttons);
	}
	
	public function data() {

		$id = url::param();

		$item = new Item();
		$item->read($id);

		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		if (!$item->id || (!$_CAN_EDIT && !$permission_view)) {
			Message::access_denied();
			url::redirect($this->request->query());
		}
		
		Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
		Compiler::attach(DIR_SCRIPTS."chain/chained.select.js");
		Compiler::attach(DIR_JS."form.validator.js");
		Compiler::attach(DIR_JS."item.js");

		$this->build($item);
		
		$data = $item->data;
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;

		$data["cbm"] = $item->width*$item->height*$item->length/1000000;

		// buttons
		$buttons = array();
		
		$buttons['back'] = $this->goBack($item);

		if ($_CAN_EDIT) {
		
			$buttons['save'] = true;

			$integrity = new Integrity();
			$integrity->set($id, 'items', 'system');

			if ($item->id && $integrity->check()) {
				$buttons['delete'] = $this->request->link('/applications/helpers/item.delete.php', array('id'=>$id));
			}
		}
		elseif($data) {
			foreach ($data as $key => $value) {
				$disabled[$key] = true;
			}
		}

		$model = new Model(Connector::DB_CORE);


	    //select availability for all countries as a default
		if ($_CAN_EDIT) {
			$result = $model->query("
				SELECT DISTINCT country_id, country_region
				FROM item_countries
				INNER JOIN countries ON country_id = item_country_country_id
				WHERE item_country_item_id = $id
			")->fetchAll();

			if ($result) {
				// no operation


			}
			else // no countries selected, add all countries as default
			{
				$user = User::instance();
				
				// get countries
				$result = $model->query("
					SELECT DISTINCT
						country_id,
						region_id,
						country_name,
						region_name
					FROM countries
					INNER JOIN regions ON region_id = country_region
					ORDER BY region_name, country_name
				")->fetchAll();	


				$countries = $result;
				
				
				foreach($countries as $key=>$country)
				{
					$sth = $model->db->prepare("
						INSERT INTO item_countries 
						   (item_country_item_id, item_country_country_id, user_created) VALUES (?, ?, ?)
						   ");

					$sth->execute(array($id, $country["country_id"], $user->login));
				}
			}
		}

		// item categories
		$result = $model->query("
			SELECT item_category_id, item_category_name 
			FROM item_categories
			WHERE item_category_active = 1 OR item_category_id = '$item->category'
			ORDER BY item_category_name
		")->fetchAll();

		$dataloader['item_category'] = _array::extract($result); 
		
		// item cost group
		$result = $model->query("
			SELECT project_cost_groupname_id, project_cost_groupname_name 
			FROM project_cost_groupnames
			WHERE  project_cost_groupname_active = 1 OR project_cost_groupname_id = '$item->cost_group'
			ORDER BY project_cost_groupname_name
		")->fetchAll();

		$dataloader['item_cost_group'] = _array::extract($result); 						
		
		// item units
		$result = $model->query("
			SELECT unit_id, unit_name 
			FROM units 
			ORDER BY unit_name
		")->fetchAll();

		$dataloader['item_unit'] = _array::extract($result);
		
		// item packaging type
		$result = $model->query("
			SELECT packaging_type_id, packaging_type_name 
			FROM packaging_types 
			ORDER BY packaging_type_name
		")->fetchAll();

		$dataloader['item_packaging_type'] = _array::extract($result);

		
		// dataloader: item materials
        $dataloader['item_materials'] = $model->query("
			SELECT item_material_id, item_material_name 
			FROM item_materials
			WHERE item_material_item_id = $id
		")->fetchAll();

		// dataloader: item electrical specifications
        $dataloader['item_electrical_specifications'] = $model->query("
			SELECT *
			FROM item_electricspecs
			WHERE item_electricspec_item_id = $id
		")->fetchAll();

		// dataloader: supplying groups
		$ItemSupplyingGroupMapper = new Item_Supplying_Groups_Datamapper();
		$supplyingGroups = $ItemSupplyingGroupMapper->getSupplyingGroupsForItem($id);
		$data['item_supplying_group'] = serialize($supplyingGroups['selected']);
		$dataloader['item_supplying_group'] = $supplyingGroups['groups'];

		// get item supplier
		$result = $model->query("
			SELECT supplier_address 
			FROM suppliers 
			where supplier_item = $item->id
		")->fetch();		

		if ($result['supplier_address']) {
			
			$supplier = $result['supplier_address'];
		
			// item slave
			$result = $model->query("
				SELECT item_id, CONCAT(item_code, ' - ', LEFT(item_name,55)) as name 
				FROM suppliers 
				LEFT JOIN items ON item_id = supplier_item 
				WHERE item_type = 1 and item_active = 1 
				AND supplier_address = $supplier and item_id <> $item->id 
				ORDER BY item_code
			")->fetchAll();

			$dataloader['item_slave_item'] = _array::extract($result); 
		} 
		else {
			$hidden['item_slave_item'] = true;
		}

		// get item certificate materials
		$result = $model->query("
			SELECT GROUP_CONCAT(DISTINCT catalog_material_name SEPARATOR ', ') AS materials
			FROM item_certificates 
			INNER JOIN certificates ON certificate_id = item_certificate_certificate_id
			INNER JOIN certificate_materials ON certificate_material_certificate_id = certificate_id
			INNER JOIN catalog_materials ON catalog_material_id = certificate_material_catalog_material_id
			WHERE item_certificates.item_certificate_item_id = $id
			ORDER BY catalog_material_name
		")->fetch();

		$data['item_certificate_materials'] = $result['materials'];

		// header
		$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
		$this->view->companytabs('pagecontent')->navigation('tab');

		// item price disabled
		$disabled['item_price'] = true;
		$disabled['supplier_item_price'] = true;

		$hidden['item_type'] = true;

		if ($item->type==Item::TYPE_SPECIAL) {
			$hidden['item_watches_displayed'] = true;
			$hidden['item_watches_stored'] = true;
			$hidden['item_materials'] = true;
			$hidden['item_electrical_specifications'] = true;
			$hidden['item_install_requirements'] = true;
			$hidden['item_regulatory_approvals'] = true;
			$hidden['item_stock_property_of_swatch'] = true;
			$hidden['item_visible_in_production_order'] = true;
			$hidden['item_is_dr_swatch_furniture'] = true;
			$hidden['item_visible_in_mps'] = true;
			$hidden['item_addable_in_mps'] = true;
		}

		if ($item->type==Item::TYPE_COST_ESTIMATION) {
			$hidden['item_type'] = true;
			$hidden['item_price'] = true;
			$hidden['item_watches_displayed'] = true;
			$hidden['item_watches_stored'] = true;
			$hidden['item_materials'] = true;
			$hidden['item_electrical_specifications'] = true;
			$hidden['item_install_requirements'] = true;
			$hidden['item_regulatory_approvals'] = true;
			$hidden['item_stock_property_of_swatch'] = true;
			$hidden['item_visible_in_production_order'] = true;
			$hidden['item_is_dr_swatch_furniture'] = true;
			$hidden['item_visible_in_mps'] = true;
			$hidden['item_addable_in_mps'] = true;
		}
	
		// template: form
		$this->view->productLine('pagecontent')
		->item('form')
		->data('dataloader', $dataloader)
		->data('data', $data)
		->data('hidden', $hidden)
		->data('disabled', $disabled)
		->data('buttons', $buttons);
	}

	public function logistic() {

		$id = url::param();

		$item = new Item();
		$item->read($id);

		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		if (!$item->id || (!$_CAN_EDIT && !$permission_view)) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$this->build($item);

		Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
		Compiler::attach(DIR_JS."form.validator.js");
		Compiler::attach(DIR_JS."item.js");
		
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;
		
		$data = $item->data;
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;

		//$data["cbm"] = $item->width*$item->height*$item->length/1000000;

		// buttons
		$buttons = array();
		$buttons['back'] = $this->goBack($item);

		if ($_CAN_EDIT) $buttons['save'] = true;
		elseif ($data) {
			foreach ($data as $key => $value) {
				$disabled[$key] = true;
			}
		}

		$model = new Model(Connector::DB_CORE);

		// item units
		$result = $model->query("
			SELECT unit_id, unit_name 
			FROM units 
			ORDER BY unit_name
		")->fetchAll();

		$dataloader['item_unit'] = _array::extract($result);
		
		// item packaging type
		$result = $model->query("
			SELECT packaging_type_id, packaging_type_name 
			FROM packaging_types 
			ORDER BY packaging_type_name
		")->fetchAll();

		$dataloader['item_packaging_type'] = _array::extract($result);

		// dataloader: item materials
        $dataloader['item_materials'] = $model->query("
			SELECT item_material_id, item_material_name 
			FROM item_materials
			ORDER BY item_material_item_id = $id
		")->fetchAll();

		// dataloader: item electrical specifications
        $dataloader['item_electrical_specifications'] = $model->query("
			SELECT 
				item_electricspec_id, 
				item_electricspec_name  AS name,
				item_electricspec_description  AS description
			FROM item_electricspecs
			ORDER BY item_electricspec_item_id = $id
		")->fetchAll();


		// package informations
		$dataloader['package_informations'] = $model->query("
			SELECT 
				item_packaging_id AS id, 
				item_packaging_number  AS number,
				item_packaging_unit_id  AS unit,
				item_packaging_packaging_id  AS packaging,
				item_packaging_number AS number,
				item_packaging_length AS length,
				item_packaging_width AS width,
				item_packaging_height AS height,
				item_packaging_weight_net AS weight_net,
				item_packaging_weight_gross AS weight_gross
			FROM item_packaging
			WHERE item_packaging_item_id = $id
		")->fetchAll();

		// header
		$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
		$this->view->companytabs('pagecontent')->navigation('tab');

		if (!$_CAN_EDIT) {
			$disabled['package_informations'] = true;
		}
	
		// template: form
		$this->view->productLine('pagecontent')
		->item('logistic')
		->data('dataloader', $dataloader)
		->data('data', $data)
		->data('hidden', $hidden)
		->data('disabled', $disabled)
		->data('buttons', $buttons);
	}

	public function materials() {

		$id = url::param();

		$item = new Item();
		$item->read($id);

		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		if (!$item->id || (!$_CAN_EDIT && !$permission_view)) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		
		Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
		Compiler::attach(DIR_SCRIPTS."chain/chained.select.js");
		Compiler::attach(DIR_JS."form.validator.js");
		Compiler::attach(DIR_JS."item.js");

		$this->build($item);
		
		$data = $item->data;
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;

		// buttons
		$buttons = array();
		
		$buttons['back'] = $this->goBack($item);

		if ($_CAN_EDIT) {
			$buttons['save'] = true;
		}
		elseif($data) {
			foreach ($data as $key => $value) {
				$disabled[$key] = true;
			}
		}

		$model = new Model(Connector::DB_CORE);

		// dataloader: catalog materials
		$result = $model->query("
			SELECT DISTINCT
				catalog_material_id,
				catalog_material_name
			FROM catalog_materials
			ORDER BY catalog_material_name
		")->fetchAll();

		$dataloader['materials'] = _array::extract($result);

		$result = $model->query("
			SELECT GROUP_CONCAT(item_catalog_material_material_id) AS materials
			FROM item_catalog_materials
			WHERE item_catalog_material_item_id = $id
		")->fetch();

		$data['materials'] = $result['materials'] ? serialize(explode(',', $result['materials'])) : null;

		// dataloader: item materials
        $dataloader['item_materials'] = $model->query("
			SELECT item_material_id, item_material_name 
			FROM item_materials
			WHERE item_material_item_id = $id
		")->fetchAll();

		// header
		$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
		$this->view->companytabs('pagecontent')->navigation('tab');

		// template: form
		$this->view->itemMaterials('pagecontent')
		->item('materials')
		->data('dataloader', $dataloader)
		->data('data', $data)
		->data('hidden', $hidden)
		->data('disabled', $disabled)
		->data('buttons', $buttons);
	}

	public function regions() {

		$id = url::param();

		$item = new Item();
		$item->read($id);

		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		if (!$item->id || (!$_CAN_EDIT && !$permission_view)) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$this->build($item);

		Compiler::attach(DIR_JS."item.regions.js");
		
		$data = $item->data;
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;

		// buttons
		$buttons = array();
		
		$buttons['back'] = $this->goBack($item);

		$model = new Model(Connector::DB_CORE);

		// get countries
		$result = $model->query("
			SELECT DISTINCT
				country_id,
				region_id,
				country_name,
				region_name
			FROM countries
			INNER JOIN regions ON region_id = country_region
			ORDER BY region_name, country_name
		")->fetchAll();	

		$countries = $result;

		$datagrid = array();

		if ($result) {
			foreach ($result as $row) {
				$region = $row['region_id'];
				$country = $row['country_id'];
				$datagrid[$region]['region'] = $row['region_name'];
				$datagrid[$region]['countries'][$country] = $row['country_name'];
			}
		}

		// data: countries
		$result = $model->query("
			SELECT DISTINCT country_id, country_region
			FROM item_countries
			INNER JOIN countries ON country_id = item_country_country_id
			WHERE item_country_item_id = $id
		")->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$region = $row['country_region'];
				$country = $row['country_id'];
				$selected[$region][$country] = true;
			}
		}
		else // no countries selected, add all countries as default
		{
			$user = User::instance();
			foreach($countries as $key=>$country)
			{
				$sth = $model->db->prepare("
					INSERT INTO item_countries 
					   (item_country_item_id, item_country_country_id, user_created) VALUES (?, ?, ?)
					   ");

				$sth->execute(array($id, $country["country_id"], $user->login));

				

			}

			$result = $model->query("
				SELECT DISTINCT country_id, country_region
				FROM item_countries
				INNER JOIN countries ON country_id = item_country_country_id
				WHERE item_country_item_id = $id
			")->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$region = $row['country_region'];
					$country = $row['country_id'];
					$selected[$region][$country] = true;
				}
			}
		}

		// header
		$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
		$this->view->companytabs('pagecontent')->navigation('tab');

		if (!$_CAN_EDIT) {
			$disabled['regions'] = true;
		}

		// template: form
		$this->view->itemRegions('pagecontent')
		->item('regions')
		->data('id', $id)
		->data('data', $data)
		->data('selected', $selected)
		->data('disabled', $disabled)
		->data('datagrid', $datagrid)
		->data('buttons', $buttons);
	}
	

	public function supplyingGroups() {

		$id = url::param();

		$item = new Item();
		$item->read($id);

		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		if (!$item->id || (!$_CAN_EDIT && !$permission_view)) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$this->build($item);

		// buttons
		$buttons = array();
		$buttons['back'] = $this->goBack($item);

		// header
		$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
		$this->view->companytabs('pagecontent')->navigation('tab');

		$this->request->field('item', $id);

		$db = Connector::get();

		$sth = $db->prepare("
			SELECT GROUP_CONCAT(item_supplying_group_supplying_group_id) AS groups
			FROM item_supplying_groups 
			WHERE item_supplying_group_item_id = ?
		");

		$sth->execute(array($id));
		$result = $sth->fetch();
		$groups = $result['groups'];

		if ($groups) {			
			
			$keys = explode(',', $groups);
			
			foreach ($keys as $key) {
				$dataloader['supplying_group_id'][$key] = $key;
			}

			$filterGroups = "OR supplying_group_id IN ($groups)";
		}

		$sth = $db->prepare("
			SELECT 
				supplying_group_id,
				supplying_group_name
			FROM supplying_groups 
			WHERE supplying_group_active = 1 $filterGroups
			ORDER BY supplying_group_name 
		");

		$sth->execute(array($id));
		$result = $sth->fetchAll();
		$datagrid = _array::datagrid($result);

		if (!$_CAN_EDIT && $datagrid) {
			
			foreach ($datagrid as $key => $row) {
				
				$img = $dataloader['supplying_group_id'] && $dataloader['supplying_group_id'][$key]
					? "<img src='/public/images/icon-checked.png' >" 
					: "<img src='/public/images/unchecked.png'>";
				
				$datagrid[$key]['supplying_group_id'] = $img;
			}
		}

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'item/supplying.groups.php');
		$tpl->data('id', $id);
		$tpl->data('disabled', !$_CAN_EDIT);
		$tpl->data('datagrid', $datagrid);
		$tpl->data('dataloader', $dataloader);
		$tpl->data('buttons', $buttons);
		$this->view->setTemplate('itemSupplyingGroups', $tpl);
	}

	public function files() {

		$id = url::param();
		$param = url::param(1);

		$item = new Item();
		$item->read($id);

		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$permission_edit_files = user::permission('can_edit_files_in_catalogue');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$_CAN_EDIT = $permission_edit || $permission_edit_items || $permission_edit_files ? true : false;

		if (!$item->id || (!$_CAN_EDIT && !$permission_view)) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$this->build($item);
		
		// db model
		$model = new Model(Connector::DB_CORE);

		// header
		$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
		$this->view->companytabs('pagecontent')->navigation('tab');

		// buttons
		$buttons = array();

		if ($param) {

			if (!$_CAN_EDIT) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$buttons['back'] = $this->request->query("files/$id");

			$param = (is_numeric($param)) ? $param : null;

			Compiler::attach(DIR_SCRIPTS."ajaxuploader/ajaxupload.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.file.js");

			// supplier data
			$itemFile = new Item_File();
			$itemFile->read($param);

			$data = $itemFile->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['item_file_item'] = $id;
			$data['redirect'] = $this->request->query("files/$id");
			
			$buttons['save'] = true;

			$integrity = new Integrity();
			$integrity->set($itemFile->id, 'item_files', 'system');

			if ($itemFile->id && $integrity->check()) {
				$buttons['delete'] = $this->request->link('/applications/helpers/item.file.delete.php', array('id'=>$param));
			}

			//check if an image for the cover sheet already exists
			$cover_sheet_filter = '';
			$result = $model->query("
				SELECT count(item_file_id) as num_recs
				FROM item_files
				where item_file_item = $id and item_file_purpose = 9
			")->fetchAll();

			if(!$param and array_key_exists(0, $result)) {
				$tmp = $result[0];

				if($tmp['num_recs'] > 0) {
					$cover_sheet_filter = " where file_purpose_id <> 9 ";
				}
			}
			
			$result = $model->query("
				SELECT 
					file_purposes.file_purpose_id AS purpose,
					file_purposes.file_purpose_name AS caption
				FROM file_purposes
				$cover_sheet_filter
				ORDER BY file_purpose_name
			")->fetchAll();

			$dataloader['item_file_purpose'] = _array::extract($result);

			// template: form
			$this->view->productLine('pagecontent')
			->item('file.form')
			->data('dataloader', $dataloader)
			->data('data', $data)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons);
		}
		else {

			$buttons['back'] = $this->goBack($item);

			if ($_CAN_EDIT) {
				$buttons['add'] = $this->request->query("files/$id/add");
			}

			$result = $model->query("
				SELECT 
					item_files.item_file_id AS item,  
					item_files.item_file_title AS title, 
					item_files.item_file_description AS description, 
					item_files.item_file_path AS path,
					file_purposes.file_purpose_id AS purpose,
					file_purposes.file_purpose_name AS caption, 
					DATE_FORMAT(item_files.date_created, '%d.%m.%Y') AS date
				FROM item_files 
				INNER JOIN file_types ON item_files.item_file_type = file_types.file_type_id
				INNER JOIN file_purposes ON item_files.item_file_purpose = file_purposes.file_purpose_id
				WHERE item_files.item_file_item = $id
				ORDER BY 
					file_purposes.file_purpose_name ASC, 
					item_files.item_file_title ASC
			")->fetchAll();

			$datagrid = array();

			if ($result) {

				$link = $this->request->query("files/$id");

				foreach ($result as $row) {
					
					$item = $row['item'];
					$purpose = $row['purpose'];
					$extension = file::extension($row['path']);

					$datagrid[$purpose]['caption'] = $row['caption'];
					$datagrid[$purpose]['items'][$item]['title'] = "<a href='$link/$item' >{$row[title]}</a><span>{$row[description]}</span>";
					$datagrid[$purpose]['items'][$item]['date']= "<span>Date:<br />{$row[date]}</span>";
					$datagrid[$purpose]['items'][$item]['path'] = $row['path'];
					$datagrid[$purpose]['items'][$item]['fileicon']  = "<span class='file-extension $extension modal' tag='{$row[path]}'></span>";
				}
			}


			$result = $model->query("
				SELECT 
					item_information_id,  
					item_information_name AS title, 
					item_information_file AS path,
					DATE_FORMAT(item_informations.date_created, '%d.%m.%Y') AS date
				FROM item_information_items 
				INNER JOIN item_informations on item_information_id = item_information_item_item_information_id 
				WHERE item_information_item_item_id = $id
				ORDER BY 
					item_information_name ASC
			")->fetchAll();


			if ($result) {

				

				foreach ($result as $row) {

					$link = "/catalog/iteminformation/data/";
					
					$item = $row['item_information_id'];
					$purpose = 'Additional Item Information';
					$extension = file::extension($row['path']);

					$datagrid[$purpose]['caption'] = $purpose;
					$datagrid[$purpose]['items'][$item]['title'] = "<a target='_blank' href='$link/$item' >{$row[title]}</a>";
					$datagrid[$purpose]['items'][$item]['date']= "<span>Date:<br />{$row[date]}</span>";
					$datagrid[$purpose]['items'][$item]['path'] = $row['path'];
					$datagrid[$purpose]['items'][$item]['fileicon']  = "<span class='file-extension $extension modal' tag='{$row[path]}'></span>";
				}
			}
		
			// template: form
			$this->view->productLine('pagecontent')
			->item('files')
			->data('datagrid', $datagrid)
			->data('buttons', $buttons)
			->data('link', $this->request->query("suppliers/$id"));
		}

	}


	public function suppliers() {

		$id = url::param();
		$param = url::param(1);

		$item = new Item();
		$item->read($id);

		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		if (!$item->id || (!$_CAN_EDIT && !$permission_view)) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$this->build($item);
		
		// db model
		$model = new Model(Connector::DB_CORE);

		// header
		$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
		$this->view->companytabs('pagecontent')->navigation('tab');

		// buttons
		$buttons = array();

		if ($param) {

			$param = (is_numeric($param)) ? $param : null;

			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");

			$count = $model->query("
				SELECT COUNT(supplier_id) AS total
				FROM suppliers 
				WHERE supplier_item = $id
			")->fetch();

			$buttons['back'] = !$param || $count['total'] > 1 ? $this->request->query("suppliers/$id") : $this->request->query();

			// supplier data
			$supplier = new Supplier();
			$supplier->read($param);

			$data = $supplier->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['supplier_item'] = $id;
			$data['redirect'] = $this->request->query("suppliers/$id");

			if (!$param) {
				$data['supplier_item_code'] = $item->code;
				$data['supplier_item_name'] = $item->name;
			}

			if ($_CAN_EDIT) {
			
				$buttons['save'] = true;

				$integrity = new Integrity();
				$integrity->set($supplier->id, 'suppliers', 'system');

				if ($supplier->id && $integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/item.supplier.delete.php', array('id'=>$param));
				}
			}
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}

			if ($supplier->address) {
				$filter = " OR address_id = $supplier->address";
			}

			// dataloader suppliers
			$result = $model->query("
				SELECT address_id, address_company 
				FROM addresses
				WHERE (address_active = 1 AND address_type in (2,5,8)) $filter
				ORDER BY address_company
			")->fetchAll();	

			$dataloader['supplier_address'] = _array::extract($result);
			
			// dataloader currencies
			$result = $model->query("
				SELECT currency_id, CONCAT(currency_symbol, ', ', currency_name) as currency
				FROM currencies 
				ORDER BY currency_symbol
			")->fetchAll();	

			$dataloader['supplier_item_currency'] = _array::extract($result);

			// stock data
			if ($supplier->id) {
				
				$result = $model->query("
					SELECT store_id 
					FROM items
					RIGHT JOIN suppliers ON item_id = supplier_item 
					LEFT JOIN stores ON item_id = store_item 
					WHERE supplier_item = store_item AND supplier_address = store_address AND supplier_id = $supplier->id
				")->fetch();

				$store = new Store();
				$store->read($result['store_id']);

				// join store to dataloader
				if ($store->id) {
					$data =  array_merge($data, $store->data);
					$data['store_stock_control_starting_date'] = date::system($store->data['store_stock_control_starting_date']);
				}
			}

			$data['store_item'] = $id;

			$disabled['store_last_global_order'] = true;
			$disabled['store_last_global_order_date'] = true;

			// template: form
			$this->view->productLine('pagecontent')
			->item('supplier.form')
			->data('dataloader', $dataloader)
			->data('data', $data)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons);
		}
		else {

			$buttons['back'] = $this->goBack($item);

			if ($_CAN_EDIT) {
				$buttons['add'] = $this->request->query("suppliers/$id/add");
			}

			$result = $model->query("
				SELECT 
					supplier_id,
					supplier_item_code,
					supplier_item_name,
					supplier_item_price,
					currency_symbol,
					address_company
				FROM suppliers 
				INNER JOIN addresses ON supplier_address = address_id
				INNER JOIN currencies ON currency_id = supplier_item_currency
				WHERE supplier_item = $id
				ORDER BY supplier_item_code
			")->fetchAll();

			$datagrid = _array::datagrid($result);

			if (count($datagrid)==1) {
				$key = key($datagrid);
				$url = $this->request->query("suppliers/$id/$key");
				url::redirect($url);
			}
		
			// template: form
			$this->view->productLine('pagecontent')
			->item('suppliers')
			->data('datagrid', $datagrid)
			->data('buttons', $buttons)
			->data('link', $this->request->query("suppliers/$id"));
		}

	}


	public function options() {

		$id = url::param();
		$param = url::param(1);

		$item = new Item();
		$item->read($id);

		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		if (!$item->id || (!$_CAN_EDIT && !$permission_view)) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$this->build($item);
		
		// db model
		$model = new Model(Connector::DB_CORE);

		// header
		$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
		$this->view->companytabs('pagecontent')->navigation('tab');

		// buttons
		$buttons = array();

		if ($param) {

			$buttons['back'] = $this->request->query("options/$id");

			$param = (is_numeric($param)) ? $param : null;

			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");

			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");

			$itemOption = new Item_Option();
			$itemOption->read($param);

			$data = $itemOption->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['item_option_parent'] = $id;
			$data['redirect'] = $this->request->query("options/$id");

			if ($_CAN_EDIT) {
			
				$buttons['save'] = true;
				
				if ($param) {
					$buttons['add_file'] = "/applications/templates/item.option.file.php?";
				}

				$integrity = new Integrity();
				$integrity->set($itemOption->id, 'item_options', 'system');

				if ($itemOption->id && $integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/item.options.delete.php', array('id'=>$param));
				}
			}
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}

			if ($itemOption->child) {
				$filter = "OR item_id = $itemOption->child";
			}

			// item slave
			$result = $model->query("
				SELECT item_id, CONCAT(item_code, ' - ', LEFT(item_name,45)) as name 
				FROM items
				WHERE item_active = 1 AND item_type < 3 AND item_id <> $id $filter
				ORDER BY item_code
			")->fetchAll();

			$dataloader['item_option_child'] = _array::extract($result);

			// template: form
			$this->view->productLine('pagecontent')
			->item('option.form')
			->data('dataloader', $dataloader)
			->data('data', $data)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons);
		}
		else {

			$buttons['back'] = $this->goBack($item);

			if ($_CAN_EDIT) {
				$buttons['add'] = $this->request->query("options/$id/add");
			}

			$result = $model->query("
				SELECT 
					item_options.item_option_id AS id, 
					item_options.item_option_name AS item_option_name,
					item_options.item_option_quantity AS item_option_quantity, 
					childs.item_code,
					childs.item_name
				FROM item_options
				INNER JOIN items AS parents ON parents.item_id = item_options.item_option_parent
				INNER JOIN items AS childs ON childs.item_id = item_options.item_option_child
				WHERE 	item_options.item_option_parent = $id
			")->fetchAll();

			$datagrid = _array::datagrid($result);
		
			// template: form
			$this->view->productLine('pagecontent')
			->item('options')
			->data('datagrid', $datagrid)
			->data('buttons', $buttons)
			->data('link', $this->request->query("options/$id"));
		}

	}


	public function composition() {

		$id = url::param();

		$item = new Item();
		$item->read($id);

		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		if (!$item->id || (!$_CAN_EDIT && !$permission_view)) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$this->build($item);

		Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
		Compiler::attach(DIR_JS."form.validator.js");

		// header
		$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
		$this->view->companytabs('pagecontent')->navigation('tab');

		// buttons
		$buttons = array();
		$buttons['back'] = $this->goBack($item);
		
		if ($_CAN_EDIT) {
			$buttons['save'] = true;
		}

		$data['item_composition_item'] = $id;

		$model = new Model(Connector::DB_CORE);

		// data: regions
		$result = $model->query("
			SELECT item_composition_group
			FROM item_compositions
			WHERE item_composition_item = $id
		")->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$serialize[] = $row['item_composition_group'];
			}
			$data['item_group_id'] = serialize($serialize);
		}

		$result = $model->query("
			SELECT item_group_id, item_group_name
			FROM item_groups
			ORDER BY item_group_name
		")->fetchAll();

		$dataloader['item_group_id'] = _array::extract($result);
	
		// template: form
		$this->view->productLine('pagecontent')
		->item('composition')
		->data('dataloader', $dataloader)
		->data('data', $data)
		->data('buttons', $buttons);
	}


	public function spareparts() {

		$id = url::param();

		$item = new Item();
		$item->read($id);

		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		if (!$item->id || (!$_CAN_EDIT && !$permission_view)) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$this->build($item);

		// header
		$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
		$this->view->companytabs('pagecontent')->navigation('tab');


		// buttons
		$buttons = array();
		$buttons['back'] = $this->goBack($item);

		$model = new Model(Connector::DB_CORE);

		$result = $result = $model->query("
			SELECT part_child, part_id
			FROM parts
			WHERE part_parent = $id
		")->fetchAll();

		$itemParts = _array::extract($result);

		if ($itemParts) {
			$selected = join(',', array_keys($itemParts));
			$filter = "OR item_id IN ($selected) ";
		}

		// get item supplier addres
		$result = $model->query("
			SELECT supplier_address AS address
			FROM suppliers
			WHERE supplier_item = $id
		")->fetch();

		$supplier = $result['address'];

		$result = $model->query("
			SELECT 
				item_id, 
				item_code, 
				item_name, 
				item_price,
				IF(item_active, '<img src=/public/images/icon-checked.png >', ' ') as active
			FROM suppliers
			INNER JOIN items ON item_id = supplier_item
			LEFT JOIN item_subcategories ON item_subcategory_id = item_subcategory
			WHERE (
				item_type = 1 
				AND item_id <> $id 
				AND item_active = 1 
				AND supplier_address = $supplier 
				AND item_subcategory > 0
				AND item_subcategory_spare_part = 1
			) $filter
			ORDER BY item_code, item_name
		")->fetchAll();

		if ($result) {

			$i = array();
			
			foreach ($result as $row) {
				
				$i = $row['item_id'];
				$items[$i] = $row;
				
				$checked = $itemParts[$i] ? 'checked=checked' : null;

				if ($_CAN_EDIT) {
					$items[$i]['checkbox'] = "<input type='checkbox' class='part' value='$i' $checked >";
				} elseif($checked) {
					$datagrid[$productline]['categories'][$category]['checkbox'] = "<i class='fa fa-check-circle' ></i>";
					$items[$i]['checkbox'] = "<i class='fa fa-check-circle' ></i>";
				}
			}
		}

		$this->request->field('parent', $id);
	
		// template: form
		$this->view->productLine('pagecontent')
		->item('spareparts')
		->data('items', $items)
		->data('buttons', $buttons);
	}


	public function finished() {

		$id = url::param();

		$item = new Item();
		$item->read($id);

		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		if (!$item->id || (!$_CAN_EDIT && !$permission_view)) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$this->build($item);

		// header
		$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
		$this->view->companytabs('pagecontent')->navigation('tab');


		// buttons
		$buttons = array();
		$buttons['back'] = $this->goBack($item);

		$model = new Model(Connector::DB_CORE);

		$result = $result = $model->query("
			SELECT part_parent, part_id
			FROM parts
			WHERE  part_child = $id
		")->fetchAll();

		$itemParts = _array::extract($result);

		if ($itemParts) {
			$selected = join(',', array_keys($itemParts));
			$filter = "OR item_id IN ($selected) ";
		}

		// get item supplier addres
		$result = $model->query("
			SELECT supplier_address AS address
			FROM suppliers
			WHERE supplier_item = $id
		")->fetch();

		$supplier = $result['address'];

		$result = $model->query("
			SELECT DISTINCT
				item_id, 
				item_code, 
				item_name, 
				item_price,
				item_category_id,
				item_category_name
			FROM suppliers
			INNER JOIN items ON item_id = supplier_item
			LEFT JOIN item_categories ON item_category = item_category_id
			LEFT JOIN item_subcategories ON item_subcategory_id = item_subcategory
			WHERE (
				item_type = 1 
				AND item_id <> $id 
				AND item_active = 1 
				AND supplier_address = $supplier 
				AND (item_subcategory_spare_part <> 1 OR item_subcategory_spare_part IS NULL)
			) $filter
			ORDER BY item_code, item_name
		")->fetchAll();

		if ($result) {

			$i = array();
			
			foreach ($result as $row) {
				
				$item = $row['item_id'];
				$category = $row['item_category_id'];
				
				$datagrid[$category]['caption'] = $row['item_category_name'];
				$datagrid[$category]['items'][$item]['item_code'] = $row['item_code'];
				$datagrid[$category]['items'][$item]['item_name'] = $row['item_name'];
				$datagrid[$category]['items'][$item]['item_price'] = $row['item_price'];
				
				$checked = $itemParts[$item] ? 'checked=checked' : null;

				if ($_CAN_EDIT) {
					$datagrid[$category]['checkall'] = "<input type='checkbox' class='category' value='$category' >";
					$datagrid[$category]['items'][$item]['box'] = "<input type='checkbox' class='part' value='$item' $checked >";
				} elseif($checked) {
					$datagrid[$category]['items'][$item]['box'] = "<i class='fa fa-check-circle' ></i>";
				}
			}
		}

		$this->request->field('parent', $id);

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'item/finished.php');
		$tpl->data('id', $id);
		$tpl->data('datagrid', $datagrid);
		$tpl->data('buttons', $buttons);
		
		$this->view->setTemplate('itemsFinished', $tpl);
	}


	public function addons() {

		$id = url::param();
		$param = url::param(1);

		$item = new Item();
		$item->read($id);

		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		if (!$item->id || (!$_CAN_EDIT && !$permission_view)) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$this->build($item);

		$model = new Model(Connector::DB_CORE);

		// header
		$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
		$this->view->companytabs('pagecontent')->navigation('tab');

		// buttons
		$buttons = array();

		if ($param) {

			$buttons['back'] = $this->request->query("addons/$id");

			$param = (is_numeric($param)) ? $param : null;

			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");

			$itemAddon = new Item_Addon();
			$itemAddon->read($param);

			$data = $itemAddon->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['addon_parent'] = $id;
			$data['redirect'] = $this->request->query("addons/$id");

			if ($_CAN_EDIT) {
			
				$buttons['save'] = true;

				$integrity = new Integrity();
				$integrity->set($itemAddon->id, 'addons', 'system');

				if ($itemAddon->id && $integrity->check()) { 
					$buttons['delete'] = $this->request->link('/applications/helpers/item.addon.delete.php', array('id'=>$param));
				}
			}
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}

			// selected items
			$items[] = $id;

			$result = $model->query("
				SELECT addon_child
				FROM addons
				WHERE addon_parent = $id
			")->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$items[] = $row['addon_child'];
				}
			}

			if ($itemAddon->child) {
				$key = array_search($itemAddon->child, $items);
				unset($items[$key]);
				$filter = "OR item_id = $itemAddon->child";
			}

			$items = join(',', array_filter(array_unique($items)));

			// item child
			$result = $model->query("
				SELECT item_id, CONCAT(item_code, ' - ', LEFT(item_name,45)) as name 
				FROM items
				WHERE item_id NOT IN ($items) AND item_active = 1 AND item_type < 3 $filter
				ORDER BY item_code
			")->fetchAll();

			$dataloader['addon_child'] = _array::extract($result);

			// template: form
			$this->view->productLine('pagecontent')
			->item('addon.form')
			->data('dataloader', $dataloader)
			->data('data', $data)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons);
		}
		else {

			$buttons['back'] = $this->goBack($item);

			if ($_CAN_EDIT) {
				$buttons['add'] = $this->request->query("addons/$id/add");
			}

			$result = $model->query("
				SELECT 
					addon_id, 
					item_code, 
					item_name, 
					addon_package_quantity,
					addon_min_packages,
					addon_max_packages
				FROM addons
				INNER JOIN items ON addon_child = item_id
				WHERE addon_parent = $id
				ORDER BY item_code, item_name
			")->fetchAll();

			$datagrid = _array::datagrid($result);

			if ($_CAN_EDIT) {
				$link =  $this->request->query("addons/$id");
			}

			// template: form
			$this->view->productLine('pagecontent')
			->item('addons')
			->data('datagrid', $datagrid)
			->data('link', $link)
			->data('buttons', $buttons);
		}
	}

	public function certificates() {

		$id = url::param();
		$param = url::param(1);

		$item = new Item();
		$item->read($id);

		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		if (!$item->id || (!$_CAN_EDIT && !$permission_view)) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$this->build($item);

		$model = new Model(Connector::DB_CORE);

		// header
		$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
		$this->view->companytabs('pagecontent')->navigation('tab');

		// buttons
		$buttons = array();
		$buttons['back'] = $this->goBack($item);

		$result = $model->query("
			SELECT DISTINCT
				certificate_files.certificate_file_id, 
				certificate_files.certificate_file_version, 
				DATE_FORMAT(certificate_files.certificate_file_expiry_date, '%d.%m.%Y') AS expiry_date, 
				certificate_files.certificate_file_file,
				certificates.certificate_id, 
				certificates.certificate_name, 
				addresses.address_company
			FROM addresses INNER JOIN certificates ON addresses.address_id = certificates.certificate_address_id
				 INNER JOIN certificate_files ON certificate_files.certificate_file_certificate_id = certificates.certificate_id
				 INNER JOIN item_certificates ON certificates.certificate_id = item_certificates.item_certificate_certificate_id
			WHERE item_certificates.item_certificate_item_id = $id
			ORDER BY certificate_name, certificate_file_expiry_date DESC
		")->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$certificate = $row['certificate_id'];
				$version = $row['certificate_file_id'];
				$certificates[$certificate]['name'] = $row['certificate_name'];
				$certificates[$certificate]['company'] = $row['address_company'];
				$certificates[$certificate]['versions'][$version]['name'] = $row['certificate_file_version'];
				$certificates[$certificate]['versions'][$version]['expiry'] = $row['expiry_date'];
				$certificates[$certificate]['versions'][$version]['file'] = $row['certificate_file_file'];
			}
		}

		// template: form
		$this->view->itemCertificates('pagecontent')
		->item('certificates')
		->data('certificates', $certificates)
		->data('buttons', $buttons);
	}

	protected function build($item) {

		if ($item && $item->id) {

			if ($item->type==Item::TYPE_SPECIAL) {
				Menu::instance()->activate("catalog/items/standardSpecial");
				Menu::instance()->deactivate("catalog/items");
				$this->request->exclude('catalog/items/materials');
			}

			if ($item->type==Item::TYPE_COST_ESTIMATION) {
				$this->request->exclude('catalog/items/logistic');
				$this->request->exclude('catalog/items/productlines');
				$this->request->exclude('catalog/items/files');
				$this->request->exclude('catalog/items/suppliers');
				$this->request->exclude('catalog/items/spareparts');
				$this->request->exclude('catalog/items/certificates');
				$this->request->exclude('catalog/items/materials');
				$this->request->exclude('catalog/items/supplyingGroups');
				$this->request->exclude('catalog/items/finished');
				Menu::instance()->activate("catalog/items/costEstimations");
				Menu::instance()->deactivate("catalog/items");
			}
		}
	}

	protected function goBack($item=null, $action=null) {

		if ($item && $item->id) {
			if ($item->type==Item::TYPE_COST_ESTIMATION) $action = 'costEstimations';
			elseif ($item->type==Item::TYPE_SPECIAL) $action = 'standardSpecial';
		}

		return $this->request->query($action);
	}
}