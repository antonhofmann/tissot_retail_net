<?php

class Recipients_Controller {

	static public $permissions = array();

	public function __construct() {

		Settings::init()->theme = 'swatch';
		$this->request = request::instance();
		$this->view = new View();
		$this->view->pageclass = 'theme-news news-recipients';
		$this->view->pagetitle = $this->request->title;
		$this->view->userboxMenu('navigations')->navigation('userbox');
		$this->view->categories('navigations')->navigation('categories');
		$this->view->appTabs('navigations')->navigation('appnav');

		static::$permissions = array(
			'edit' => user::permission('can_edit_news_system_data')
		);

		Compiler::attach('/public/themes/swatch/css/news.css');
	}


	public function index() {

		$application = $this->request->application;

		$model = new Model($application);

		switch ($application) {
			
			case 'news':

				$sth = $model->db->prepare("
					SELECT DISTINCT
						news_recipient_id AS id,
					    CONCAT(news_recipient_firstname, ' ', news_recipient_name) AS name,
					    news_recipient_email AS email,
					    news_recipient_active AS active
					FROM news_recipients
					ORDER BY name
				");

				$sth->execute();
				$recipients = $sth->fetchAll();

				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'news/recipient/list.php');
				$tpl->data('title', $this->request->title);
				$tpl->data('recipients', $recipients);
				$this->view->setTemplate('sections', $tpl);

			break;
		}
			
	}

	public function add() {

		$application = $this->request->application;

		$model = new Model($application);

		switch ($application) {
			
			case 'news':

				if (!static::$permissions['edit']) {
					Message::access_denied();
					url::redirect($this->request->query());
				}

				$buttons = array();
				$buttons['save'] = true;

				$data = array();
				$data['application'] = $application;
				$data['active'] = 'checked=checked';

				// view template
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'news/recipient/form.php');
				$tpl->data('id', $id);
				$tpl->data('data', $data);
				$tpl->data('buttons', $buttons);
				$tpl->data('title', $this->request->title);
				$this->view->setTemplate('section', $tpl);

				Compiler::attach(array(
					'/public/scripts/formvalidation/css/formValidation.min.css',
					'/public/scripts/formvalidation/js/formValidation.js',
					'/public/scripts/formvalidation/js/framework/bootstrap.min.js',
					'/public/scripts/bsdialog/bsdialog.min.css',
					'/public/scripts/bsdialog/bsdialog.min.js',
					'/public/js/news.recipient.js'
				));

			break;
		}
	}

	public function data() {

		$id = url::param();
		$application = $this->request->application;

		$model = new Model($application);

		switch ($application) {
			
			case 'news':

				$buttons = array();

				$recipient = new Modul($application);
				$recipient->setTable('news_recipients');
				$recipient->read($id);

				if ( !$recipient->id ) {
					Message::access_denied();
					url::redirect($this->request->query());
				}

				if (static::$permissions['edit']) {
					
					$buttons['save'] = true;

					$integrity = new Integrity();
					$integrity->set($id, 'news_recipients');
					
					if ($integrity->check()) {
						$buttons['delete'] = $this->request->link('/applications/modules/news/recipient/delete.php', array('id'=>$id));
					}
				}

				$recipient->data['active'] = $recipient->data['news_recipient_active'] ? 'checked=checked' : null;
				$recipient->data['application'] = $application;

				// view template
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'news/recipient/form.php');
				$tpl->data('id', $id);
				$tpl->data('buttons', $buttons);
				$tpl->data('title', $this->request->title);
				$tpl->data('data', $recipient->data);
				$this->view->setTemplate('section', $tpl);

				Compiler::attach(array(
					'/public/scripts/formvalidation/css/formValidation.min.css',
					'/public/scripts/formvalidation/js/formValidation.js',
					'/public/scripts/formvalidation/js/framework/bootstrap.min.js',
					'/public/scripts/bsdialog/bsdialog.min.css',
					'/public/scripts/bsdialog/bsdialog.min.js',
					'/public/js/news.recipient.js'
				));

			break;
		}
	}
}
