<?php 

	class Socialmediachannels_Controller {
		
		public function __construct() {
			
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {

			// view collection
			$link = $this->request->query('data');
			$this->request->field('form', $link);

			$permission_edit = user::permission('can_edit_socialmediachannels');
		
			if(!$permission_edit) {
				Message::access_denied();
				url::redirect('/messages/show/access-denied');
			}

			// button: add new
			$link = $this->request->query('add');
			$this->request->field('add', $link);

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'list.php');
			$tpl->data('url', '/applications/modules/socialmediachannel/list.php');
			$tpl->data('class', 'list-600');
			$this->view->setTemplate('socialmediachannels', $tpl);

			Compiler::attach(array(
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js'
			));
		}
		
		public function add() {
			
			$permission_edit = user::permission('can_edit_socialmediachannels');
		
			if(!$permission_edit) {
				Message::access_denied();
				url::redirect('/messages/show/access-denied');
			}

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
	
			// form hidden vars
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');

							
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'socialmediachannel/form.php');
			$tpl->data('data', $data);
			$tpl->data('buttons', $buttons);

			$this->view->setTemplate('socialmediachannel', $tpl);

		
			// script integration
			Compiler::attach(array(
				'/public/scripts/validationEngine/css/validationEngine.jquery.css',
				'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
				'/public/scripts/validationEngine/js/jquery.validationEngine.js',
				'/public/scripts/qtip/qtip.css',
				'/public/scripts/qtip/qtip.js',
				'/public/js/form.validator.js'
			));

		}
		
		public function data() {
				
			$permission_edit = user::permission('can_edit_socialmediachannels');
		
			if(!$permission_edit) {
				Message::access_denied();
				url::redirect('/messages/show/access-denied');
			}
			
			
			$id = url::param();


			if (!$id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$application = $this->request->application;

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			// modul: brand
			$socialmediachannel = new Modul($application);
			$socialmediachannel->setTable('socialmediachannels');
			$data = $socialmediachannel->read($id);

		
			$buttons['save'] = true;
					
			// button: delete
			/*
			if (user::permission('can_administrate_system_data')) {
				$buttons['delete'] = $this->request->link('/applications/modules/socialmediachannel/delete.php', array('id'=>$id));
			}
			*/
			

			// request vars
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;


			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'socialmediachannel/form.php');
			$tpl->data('data', $data);
			$tpl->data('buttons', $buttons);
			
			$this->view->setTemplate('socialmediachannel', $tpl);

			// script integration
			Compiler::attach(array(
				'/public/scripts/validationEngine/css/validationEngine.jquery.css',
				'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
				'/public/scripts/validationEngine/js/jquery.validationEngine.js',
				'/public/scripts/qtip/qtip.css',
				'/public/scripts/qtip/qtip.js',
				'/public/js/form.validator.js'
			));

		}
	}