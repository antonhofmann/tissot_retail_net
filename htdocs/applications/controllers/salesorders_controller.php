<?php 

class Salesorders_Controller {
	
	public function __construct() { 
		
		$this->request = request::instance();
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}
	
	public function index() {

		$permissionEdit = false;
		$application = $this->request->application;
		
		switch ($application) {
			
			case 'mps':
				$permissionEdit = user::permission(Mastersheet::PERMISSION_EDIT);

			break;

			case 'lps':
				$permissionEdit = user::permission('can_create_lps_sales_orders');

			break;
		}
		
		// check access te consolidation
		if (!$permissionEdit) {
			message::access_denied();
			url::redirect('/messages/show/access_denied');
		}
		
		// form link
		$link = $this->request->query('items');
		$this->request->field('data', $link);
		
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'list.php');
		$tpl->data('url', '/applications/modules/mastersheet/list.php');
		$tpl->data('class', 'list-1000');

		// assign template to view
		$this->view->setTemplate('mastersheet', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js'
		));
	}
	
	public function items() {
		
		$id = url::param();
		$permissionEdit = false;
		$application = $this->request->application;

		$model = new Model($applications);
		
		switch ($application) {
			
			case 'mps':
				$tableName = 'mps_mastersheets';
				$permissionEdit = user::permission(Mastersheet::PERMISSION_EDIT);
				$exportFile = '/applications/modules/mastersheet/salesorders/export.items.db.php';
				
				$printFile = '/applications/exports/excel/mastersheet.consolidation.items.php';

				$mapMasterSheet = array(
					'mps_mastersheet_year' => 'mastersheet_year',
					'mps_mastersheet_name' => 'mastersheet_name',
					'mps_mastersheet_consolidated' => 'mastersheet_consolidated',
					'mps_mastersheet_archived' => 'mastersheet_archived'
				);

				$result = $model->query("SELECT COUNT(sap_hq_order_type_id) AS total FROM db_retailnet.sap_hq_order_types")->fetch();
				$_HAS_SAP_ORDER_TYPES = $result['total'] > 0 ? true : false;

				$result = $model->query("SELECT COUNT(sap_hq_order_reason_id) AS total FROM db_retailnet.sap_hq_order_reasons")->fetch();
				$_HAS_SAP_ORDER_REASONS = $result['total'] > 0 ? true : false;

			break;

			case 'lps':
				$tableName = 'lps_mastersheets';
				$permissionEdit = user::permission('can_create_lps_sales_orders');
				$exportFile = '/applications/modules/mastersheet/salesorders/export.launchplan.xls.php';
				$printFile = '/applications/modules/mastersheet/print.launchplan.items.php';

				$mapMasterSheet = array(
					'lps_mastersheet_year' => 'mastersheet_year',
					'lps_mastersheet_name' => 'mastersheet_name',
					'lps_mastersheet_consolidated' => 'mastersheet_consolidated',
					'lps_mastersheet_archived' => 'mastersheet_archived',
					'lps_mastersheet_pos_date' => 'mastersheet_pos_date',
					'lps_mastersheet_phase_out_date' => 'mastersheet_phase_out_date',
					'lps_mastersheet_week_number_first' => 'mastersheet_week_number_first',
					'lps_mastersheet_weeks' => 'mastersheet_weeks',
					'lps_mastersheet_estimate_month' => 'mastersheet_estimate_month'
				);

			break;
		}
			
		$mastersheet = new Modul($application);
		$mastersheet->setTable($tableName);
		$mastersheet->setDataMap($mapMasterSheet);
		$mastersheet->read($id);

		$header = $mastersheet->data['mastersheet_name'].', '.$mastersheet->data['mastersheet_year'];
		
		// check access te consolidation
		if (!$permissionEdit || !$mastersheet->id) { 
			url::redirect('/messages/show/access_denied');
		} elseif ($mastersheet->archived && !$this->request->archived) {
			url::redirect("/".$this->request->application."/mastersheets/archived/data/$id");
		}

		$_CAN_EXPORT_ITEMS = true;
		$_EXPORT_ERRORS = array();

		if ($application=='mps') {

			if (!$_HAS_SAP_ORDER_TYPES) {
				$_CAN_EXPORT_ITEMS = false;
				$_EXPORT_ERRORS[] = "Missing SAP HQ Order Types";
			}

			if (!$_HAS_SAP_ORDER_REASONS) {
				$_CAN_EXPORT_ITEMS = false;
				$_EXPORT_ERRORS[] = "Missing SAP HQ Order Reasons";
			}
		}
		
		// buttons
		$buttons = array();
		
		// button: back
		$buttons['back'] = $this->request->query();

		// button: print items
		$buttons['print'] = $this->request->link($printFile, array('id'=>$id, 'mastersheet' => $id));

		// button: export
		if ($_CAN_EXPORT_ITEMS ) {
			$buttons['export'] = $this->request->link($exportFile, array('id'=>$id));
		} else {
			$_EXPORT_ERRORS[] = "Export items it's not possible";
		}
		
		// header
		$this->view->header('pagecontent')->node('header')->data('title', $header);
		$this->view->tabs('pagecontent')->navigation('tab');

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'mastersheet/sales.order.php');
		$tpl->data('buttons', $buttons);
		$tpl->data('id', $id);
		$tpl->data('header', $header);
		$tpl->data('errors', $_EXPORT_ERRORS);
		$this->view->setTemplate('mastersheet', $tpl);

		Compiler::attach(array(
			'/public/scripts/jquery/jquery.ui.css',
			'/public/scripts/jquery/jquery.ui.js',
			'/public/css/jquery.ui.css',
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/scripts/table.loader.js',
			'/public/js/mastersheet.sales.orders.js',
			'/public/css/switch.css'
		));
	}

	public function downloads() {

		$id = url::param();
		$permissionEdit = false;
		$application = $this->request->application;
		$permissionView = User::permission('can_download_lps_sales_orders');

		if (!$permissionView) {
			message::access_denied();
			url::redirect($this->request->query());
		}

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'list.php');
		$tpl->data('url', '/applications/modules/mastersheet/salesorders/download.list.php');
		$tpl->data('class', 'list-big list-1000');
		$this->view->setTemplate('salesorders', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js',
			'/public/js/sales.order.download.js',
		));
	}
}
