<?php 

	class CollectionCategories_Controller {
		
		public function __construct() {
			
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
				
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			$permissionEdit = false;

			switch ($this->request->application) {
				
				case 'mps':
					$permissionEdit = user::permission(Material_Collection_Category::PERMISSION_EDIT);
				break;

				case 'lps':
					$permissionEdit = user::permission('can_edit_lps_collection_categories');
				break;
			}

			// view collection
			$link = $this->request->query('data');
			$this->request->field('form', $link);

			// export collection
			$link = $this->request->link('/applications/modules/collection.category/print.php');
			$this->request->field('print', $link);

			// button: add new
			if (!$this->request->archived && $permissionEdit) {
				$link = $this->request->query('add');
				$this->request->field('add', $link);
			}

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'list.php');
			$tpl->data('url', '/applications/modules/collection.category/list.php');
			$tpl->data('class', 'list-700');
			$this->view->setTemplate('collection.category', $tpl);

			Compiler::attach(array(
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js'
			));
		}
		
		public function add() {
			
			$permissionEdit = false;

			switch ($this->request->application) {
				
				case 'mps':
					$permissionEdit = user::permission(Material_Collection_Category::PERMISSION_EDIT);
				break;

				case 'lps':
					$permissionEdit = user::permission('can_edit_lps_collection_categories');
				break;
			}

			// check access
			if( $this->request->archived || !$permissionEdit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
	
			// form hidden vars
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			$data['collection_category_active'] = true;

			Compiler::attach(array(
				'/public/scripts/validationEngine/css/validationEngine.jquery.css',
				'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
				'/public/scripts/validationEngine/js/jquery.validationEngine.js',
				'/public/scripts/qtip/qtip.css',
				'/public/scripts/qtip/qtip.js',
				'/public/js/form.validator.js'
			));
				
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'collection.category/form.php');
			$tpl->data('data', $data);
			$tpl->data('buttons', $buttons);

			$this->view->setTemplate('collection.category', $tpl);
		}
		
		public function data() {
				
			$id = url::param();
			$application = $this->request->application;
			$permissionEdit = false;

			switch ($application) {
				
				case 'mps':
					$permissionEdit = user::permission(Material_Collection_Category::PERMISSION_EDIT);
					$tableName = 'mps_material_collection_categories';
					$prefix = 'mps_material_';					
				break;

				case 'lps':
					$permissionEdit = user::permission('can_edit_lps_collection_categories');
					$tableName = 'lps_collection_categories';
					$prefix = 'lps_';

				break;
			}

			// modul: collection category
			$category = new Modul($application);
			$category->setTable($tableName);
			$data = $category->read($id);

			// check access to material collection
			if (!$category->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			// replace key prefix
			$data = _array::replaceKey($prefix, null, $data);

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			if (!$this->request->archived) {
		
				if ($permissionEdit) {
					
					$buttons['save'] = true;
					
					$integrity = new Integrity();
					$integrity->set($id, $tableName);
					
					if ($integrity->check()) {
						$buttons['delete'] = $this->request->link('/applications/modules/collection.category/delete.php', array('id'=>$id));
					}

				} else {
					$disabled = true;
				}

			} else {
				$disabled = true;
			}

			// headers
			$this->view->header('pagecontent')->node('header')->data('title', $data['collection_category_code']);
			$this->view->tabs('pagecontent')->navigation('tab');

			// request vars
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// script integration
			Compiler::attach(array(
				'/public/scripts/validationEngine/css/validationEngine.jquery.css',
				'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
				'/public/scripts/validationEngine/js/jquery.validationEngine.js',
				'/public/scripts/qtip/qtip.css',
				'/public/scripts/qtip/qtip.js',
				'/public/js/form.validator.js'
			));

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'collection.category/form.php');
			$tpl->data('data', $data);
			$tpl->data('buttons', $buttons);

			// disable form
			if ($disabled) {
				$fields = array_keys($data);
				$tpl->data('disabled', array_fill_keys($fields, true));
			}

			$this->view->setTemplate('collection.category', $tpl);
		}
		
		public function files() {
			
			$id = url::param();
			$param = url::param(1);
			$application = $this->request->application;
			$permissionEdit = false;

			switch ($application) {
				
				case 'mps':
					$tableName = 'mps_material_collection_categories';
					$tableNameFiles = 'mps_material_collection_category_files';
					$permissionEdit = user::permission(Material_Collection_Category::PERMISSION_EDIT);
					$prefix = 'mps_material_';
					$prefixFile = 'mps_material_collection_category_';
				break;


				case 'lps':
					$tableName = 'lps_collection_categories';
					$tableNameFiles = 'lps_collection_category_files';
					$permissionEdit = user::permission('can_edit_lps_collection_categories');
					$prefix = 'lps_';
					$prefixFile = 'lps_collection_category_';
				break;
			}

			// modul collection category
			$category = new Modul($application);
			$category->setTable($tableName);
			$data = $category->read($id);

			// check access
			if (!$category->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			// replace key prefix
			$data = _array::replaceKey($prefix, null, $data);
				
			// headers
			$this->view->header('pagecontent')->node('header')->data('title', $data['collection_category_code']);
			$this->view->tabs('pagecontent')->navigation('tab');
			

			// EDIT MODE
			if ($id && $param) { 
				
				$param = (is_numeric($param)) ? $param : null;
				
				// button: back
				$buttons['back'] = $this->request->query("files/$id");
				
				// button: save
				if (!$this->request->archived && $permissionEdit) {	
					$buttons['save'] = '/applications/modules/collection.category/submit.file.php';
				}
				
				if ($param) {

					$file = new Modul($application);
					$file->setTable($tableNameFiles);
					$fileData = $file->read($param);

					// replace key prefix
					$fileData = _array::replaceKey($prefixFile, null, $fileData);
					
					if ($buttons['save']) {
						
						$integrity = new Integrity();
						$integrity->set($param, $tableNameFiles);
						
						if ($integrity->check()) {
							$buttons['delete'] = $this->request->link('/applications/modules/collection.category/delete.file.php', array('id'=>$param));
						}
					}
					elseif ($data) {
						$disabled = true;
					}
				}
				else {
					$fileData['redirect'] = $this->request->query("files/$id");
					$fileData['collection_category_file_visible'] = $id;
				}
				
				// form dataloader
				$fileData['application'] = $this->request->application;
				$fileData['controller'] = $this->request->controller;
				$fileData['action'] = $this->request->action;
				$fileData['file_entity'] = $id;

				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'file/form.php');
				$tpl->data('data', $fileData);
				$tpl->data('buttons', $buttons);

				// disable form
				if ($disabled) {
					$fields = array_keys($fileData);
					$tpl->data('disabled', array_fill_keys($fields, true));
				}

				$this->view->setTemplate('collection.category.file', $tpl);

				// script integration
				Compiler::attach(array(
					'/public/scripts/ajaxuploader/ajaxupload.js',
					'/public/scripts/validationEngine/css/validationEngine.jquery.css',
					'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
					'/public/scripts/validationEngine/js/jquery.validationEngine.js',
					'/public/scripts/qtip/qtip.css',
					'/public/scripts/qtip/qtip.js',
					'/public/js/form.validator.file.js'
				));
			}
			// LIST MODE
			else {
				
				// button back
				$buttons['back'] = ui::button(array(
					'id' => 'back',
					'icon' => 'back',
					'href' => $this->request->query(),
					'label' => Translate::instance()->back
				));
				
				// button: add new file
				if (!$this->request->archived && $permissionEdit ) {
					$buttons['add'] = ui::button(array(
						'id' => 'add',
						'class' => 'add',
						'href' => $this->request->query("files/$id/add"),
						'label' => Translate::instance()->add_new
					));
				}

				$model = new Model($application);

				switch ($application) {
				
					case 'mps':
						$datagrid = $model->query("
							SELECT DISTINCT	
								mps_material_collection_category_file_id AS id,
								mps_material_collection_category_file_path AS path,
								mps_material_collection_category_file_description AS description,
								mps_material_collection_category_file_title as title,
								DATE_FORMAT(mps_material_collection_category_files.date_created, '%d.%m.%Y') AS date,
								file_type_id AS type,
								file_type_name AS type_name
							FROM mps_material_collection_category_files
							INNER JOIN db_retailnet.file_types ON file_type_id = mps_material_collection_category_file_type
							WHERE mps_material_collection_category_file_collection_id = $id
							ORDER BY file_type_name, mps_material_collection_category_file_title
						")->fetchAll();

					break;

					case 'lps':
						$datagrid = $model->query("
							SELECT DISTINCT	
								lps_collection_category_file_id AS id,
								lps_collection_category_file_path AS path,
								lps_collection_category_file_description AS description,
								lps_collection_category_file_title as title,
								DATE_FORMAT(lps_collection_category_files.date_created, '%d.%m.%Y') AS date,
								file_type_id AS type,
								file_type_name AS type_name
							FROM lps_collection_category_files
							INNER JOIN db_retailnet.file_types ON file_type_id = lps_collection_category_file_type
							WHERE lps_collection_category_file_collection_id = $id
							ORDER BY file_type_name, lps_collection_category_file_title
						")->fetchAll();

					break;
				}

				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'file/list.php');
				$tpl->data('data', $data);
				$tpl->data('datagrid', $datagrid);
				$tpl->data('buttons', $buttons);
				$tpl->data('link', $this->request->query("files/$id"));

				$this->view->setTemplate('collection.category.file', $tpl);

				Compiler::attach(array(
					'/public/scripts/dropdown/dropdown.css',
					'/public/scripts/dropdown/dropdown.js',
					'/public/scripts/table.loader.js'
				));
			}
		}
	}
	