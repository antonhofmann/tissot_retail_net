<?php


	class Announcements_Controller {

		public function __construct() {
			
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}

		public function index() {
	
			$permissionEdit = false;
			
			switch ($this->request->application) {
				
				case 'mps':
					$permissionEdit = user::permission('can_edit_mps_announcements');
				break;

				case 'lps':
					$permissionEdit = user::permission('can_edit_lps_announcements');
				break;
			}

			// link:: show announcement
			$link = $this->request->query('show');
			$this->request->field('show', $link);

			// button: add new
			if (!$this->request->archived && $permissionEdit) {
				$link = $this->request->query('add');
				$this->request->field('add', $link);
			}

			// template: list
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'list.php');
			$tpl->data('url', '/applications/modules/announcement/list.php');
			$tpl->data('class', 'list-800 annoucements-list');

			// assign template to view
			$this->view->setTemplate('announcements', $tpl);

			Compiler::attach(array(
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js'
			));
		}

		public function show() {
			
			$id = url::param();
			$permissionEdit = false;

			switch ($this->request->application) {
				
				case 'mps':
					$tableName = 'mps_announcements';
					$tablePrefix = 'mps_';
					$permissionEdit = user::permission('can_edit_mps_announcements');
				break;

				case 'lps':
					$tableName = 'lps_announcements';
					$tablePrefix = 'lps_';
					$permissionEdit = user::permission('can_edit_lps_announcements');
				break;
			}

			// get announcement
			$modul = new Modul($this->request->application);
			$modul->setTable($tableName);
			$modul->read($id);

			// check access
			if (!$modul->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			// dataloader
			$data = _array::replaceKey($tablePrefix, null, $modul->data);
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			// button edit
			if ($permissionEdit) {
				$buttons['edit'] = $this->request->query("edit/$id");
			}

			// template: list
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'announcement/show.php');
			$tpl->data('buttons', $buttons);
			$tpl->data('data', $data);

			// assign template to view
			$this->view->setTemplate('announcement', $tpl);
		}

		public function add() {

			$permissionEdit = false;

			switch ($this->request->application) {
				
				case 'mps':
					$permissionEdit = user::permission('can_edit_mps_announcements');
					$urlSubmit = '/applications/modules/announcement/submit.php';
				break;

				case 'lps':
					$permissionEdit = user::permission('can_edit_lps_announcements');
					$urlSubmit = '/applications/modules/announcement/submit.php';
				break;
			}

			if ($this->request->archived || !$permissionEdit) {
				message::invalid_permission();
				url::redirect($this->request->query());
			}
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('edit');
			
			// buttons
			$buttons = array();
			$buttons['back'] =  $this->request->query();
			$buttons['save'] = $urlSubmit;

			// template: list
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'announcement/form.php');
			$tpl->data('buttons', $buttons);
			$tpl->data('data', $data);

			// assign template to view
			$this->view->setTemplate('announcement', $tpl);

			Compiler::attach(array(
				'/public/scripts/jquery/jquery.ui.css',
				'/public/scripts/jquery/jquery.ui.js',
				'/public/css/jquery.ui.css',
				'/public/scripts/ajaxuploader/ajaxupload.js',
				'/public/scripts/validationEngine/css/validationEngine.jquery.css',
				'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
				'/public/scripts/validationEngine/js/jquery.validationEngine.js',
				'/public/scripts/qtip/qtip.css',
				'/public/scripts/qtip/qtip.js'
			));
		}

		public function edit() {
	
			$id = url::param();
			$permissionEdit = false;

			switch ($this->request->application) {
				
				case 'mps':
					$permissionEdit = user::permission('can_edit_mps_announcements');
					$urlSubmit = '/applications/modules/announcement/submit.php';
					$tableName = 'mps_announcements';
					$tablePrefix = 'mps_';
				break;

				case 'lps':
					$permissionEdit = user::permission('can_edit_lps_announcements');
					$urlSubmit = '/applications/modules/announcement/submit.php';
					$tableName = 'lps_announcements';
					$tablePrefix = 'lps_';
				break;
			}
			
			// get announcement
			$announcement = new Modul($this->request->application);
			$announcement->setTable($tableName);
			$announcement->read($id);

			// check access
			if (!$announcement->id || !$permissionEdit) {
				message::failure_id();
				url::redirect($this->request->query());
			}

			// form dataloader
			$data = _array::replaceKey($tablePrefix, null, $announcement->data);
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;

			// date format
			$data['announcement_date'] = date::system($data['announcement_date']);
			$data['announcement_expiry_date'] = date::system($data['announcement_expiry_date']);
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query("show/$id");
			$buttons['save'] = $urlSubmit;
			$buttons['delete'] = $this->request->link('/applications/modules/announcement/delete.php', array('id'=>$id));

			// template: list
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'announcement/form.php');
			$tpl->data('buttons', $buttons);
			$tpl->data('data', $data);

			// assign template to view
			$this->view->setTemplate('announcement', $tpl);

			Compiler::attach(array(
				'/public/scripts/jquery/jquery.ui.css',
				'/public/scripts/jquery/jquery.ui.js',
				'/public/css/jquery.ui.css',
				'/public/scripts/ajaxuploader/ajaxupload.js',
				'/public/scripts/validationEngine/css/validationEngine.jquery.css',
				'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
				'/public/scripts/validationEngine/js/jquery.validationEngine.js',
				'/public/scripts/qtip/qtip.css',
				'/public/scripts/qtip/qtip.js'
			));
		}
	}
	