<?php

	class Projectstates_Controller {

		public function __construct() {

			$this->request = request::instance();
	
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}

		public function index() {

			switch ($this->request->application) {
				
				case 'red':
					
					// permissions
					$permission_system_view = user::permission(RED::PERMISSION_VIEW_SYSTEM_DATA);
					$permission_system_edit = user::permission(RED::PERMISSION_EDIT_SYSTEM_DATA);

					// request form: add button
					if (!$this->request->archived && $permission_system_edit) {
						$field = $this->request->query('add');
						$this->request->field('add',$field);
					}
						
					// request form: link to basic data
					if ($permission_system_view) {
						$field = $this->request->query('data');
						$this->request->field('form', $field);
					}
		
					// template: project types
					$this->view->projectTypeList('pagecontent')->red('project.state.list');
				
				break;
			}
		}

		public function add() {

			switch ($this->request->application) {
				
				case 'red':

					// permissions
					$permission_system_view = user::permission(RED::PERMISSION_VIEW_SYSTEM_DATA);
					$permission_system_edit = user::permission(RED::PERMISSION_EDIT_SYSTEM_DATA);
					
					// check access to add partners
					if ($this->request->archived || !$permission_system_edit) {
						message::access_denied();
						url::redirect($this->request->query());
					}
		
					// form dataloader
					$data = array();
					$data['application'] = $this->request->application;
					$data['controller'] = $this->request->controller;
					$data['redirect'] = $this->request->query('data');
					
					// buttons
					$button = array();
					$buttons['back'] = $this->request->query();
					$buttons['save'] = true;
					
					// dataloader: icons
					$model = new Model($this->request->application);
					$result = $model->query("
				  		SELECT
						   	red_icon_id,
							red_icon_title,
							red_icon_path,
							red_icon_group
				  		FROM red_icons
						WHERE red_icon_group = 'projectstate'
					")->fetchAll();

					if ($result) {
						foreach ($result as $icon) {
							$attributes['red_projectstate_icon'][$icon['red_icon_id']]['data-iconurl'] = $icon['red_icon_path'];
							$dataloader['red_projectstate_icon'][$icon['red_icon_id']] = $icon['red_icon_title'];
						}
					}
		
					// template: project type
					$this->view->projectTypeForm('pagecontent')
					->red('project.state.form')
					->data('data', $data)
					->data('dataloader', $dataloader)
					->data('attributes', $attributes)
					->data('buttons', $buttons);
			
				break;
			}
		}

		public function data() {

			switch ($this->request->application) {
				
				case 'red':
					
					$id = url::param();
			
					// permissions
					$permission_system_view = user::permission(RED::PERMISSION_VIEW_SYSTEM_DATA);
					$permission_system_edit = user::permission(RED::PERMISSION_EDIT_SYSTEM_DATA);
					$permission_system_delete = user::permission(RED::PERMISSION_DELETE_SYSTEM_DATA);
					
					$projectState = new Red_Project_State();
					$projectState->read($id);
					
					// check access to add partners
					if (!$projectState->id || !$permission_system_view) {
						message::access_denied();
						url::redirect($this->request->query());
					}
		
					// form dataloader
					$data = $projectState->data;
					$data['application'] = $this->request->application;
					$data['controller'] = $this->request->controller;
					
					// buttons
					$button = array();
					$buttons['back'] = $this->request->query();

					if ($permission_system_edit) {
				
						// button: save
						$buttons['save'] = true;
			
						// db integrity
						if ($permission_system_delete) {
							
							$integrity = new Integrity();
							$integrity->set($id, 'red_projectstates');
							
							if ($integrity->check()) {
								$buttons['delete'] = $this->request->link('/applications/helpers/red.project.state.delete.php', array('id'=>$id));
							}
						}
					}
					else {
						foreach ($data as $field => $value) {
							$disabled[$field] = true;
						}
					}
					
					// dataloader: icons
					$model = new Model($this->request->application);
					$result = $model->query("
				  		SELECT
						   	red_icon_id,
							red_icon_title,
							red_icon_path,
							red_icon_group
				  		FROM red_icons
						WHERE red_icon_group = 'projectstate'
					")->fetchAll();

					if ($result) {
						foreach ($result as $icon) {
							$attributes['red_projectstate_icon'][$icon['red_icon_id']]['data-iconurl'] = $icon['red_icon_path'];
							$dataloader['red_projectstate_icon'][$icon['red_icon_id']] = $icon['red_icon_title'];
						}
					}

				
					// template: header
					$this->view->header('pagecontent')
					->node('header')
					->data('title', $projectState->name);
		
					// template: project type
					$this->view->projectsTypeForm('pagecontent')
					->red('project.state.form')
					->data('data', $data)
					->data('dataloader', $dataloader)
					->data('attributes', $attributes)
					->data('buttons', $buttons);
				
				break;
			}
		}
	}
