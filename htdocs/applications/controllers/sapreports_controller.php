<?php

	class sapReports_Controller {

		public function __construct() {

			$this->user = User::instance();
			$this->request = request::instance();
			$this->translate = Translate::instance();

			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}

		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			
			// permissions
			$permission_view = user::permission(Ordersheet::PERMISSION_VIEW);
			$permission_view_limited = user::permission(Ordersheet::PERMISSION_VIEW_LIMITED);
			$permission_edit = user::permission(Ordersheet::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Ordersheet::PERMISSION_EDIT_LIMITED);
			$permission_manage = user::permission(Ordersheet::PERMISSION_MANAGE);
			
			// filter: limited permission
			if(!$permission_manage && !$permission_edit && !$permission_view) { 
				$this->request->field('limited', true); 
			}

			$this->request->field('show', $this->request->query('show'));

			// template: staff list
			$this->view->raportList('pagecontent')->sap('raport.list');
		}

		public function show() {

			$id = url::param();
			
			// order sheet
			$ordersheet = new Ordersheet();
			$ordersheet->read($id);
			
			// check access to order sheet
			if (!$ordersheet->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			
			$model = new Model($this->request->application);
			$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);
			
			$datagrid = array();
			
			// get involved data
			$result = $model->query("
				SELECT
					mps_ordersheet_item_delivered_id AS id,
					mps_ordersheet_item_delivered_quantity AS quantity,
					DATE_FORMAT(
						mps_ordersheet_item_delivered_confirmed,
						'%d.%m.%Y'
					) AS confirmed_dete,
					mps_ordersheet_item_id AS item,
					mps_ordersheet_item_quantity_shipped AS item_shipped,
					mps_ordersheet_item_quantity_distributed AS item_distributed,
					CONCAT(
						mps_material_code,
						', ',
						mps_material_name
					) AS item_caption,
					mps_ordersheet_item_delivered_posaddress_id AS pos,
					CONCAT(
						posaddress_name,
						', ',
						posaddress_place
					) AS pos_caption,
					mps_ordersheet_item_delivered_warehouse_id AS warehouse,
					CONCAT(address_warehouse_address_id, '.', address_warehouse_id) AS warehouse_key,
					address_warehouse_name AS warehouse_caption
				FROM
					mps_ordersheet_item_delivered
				INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_delivered_ordersheet_item_id = mps_ordersheet_item_id
				INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
				LEFT JOIN db_retailnet.posaddresses ON posaddress_id = mps_ordersheet_item_delivered_posaddress_id
				LEFT JOIN mps_ordersheet_warehouses ON mps_ordersheet_warehouse_id = mps_ordersheet_item_delivered_warehouse_id
				LEFT JOIN db_retailnet.address_warehouses ON address_warehouse_id = mps_ordersheet_warehouse_address_warehouse_id
				WHERE
					mps_ordersheet_item_ordersheet_id = $ordersheet->id
				AND mps_ordersheet_item_delivered_confirmed IS NOT NULL
			")->fetchAll();
			
			if ($result) {
				
				foreach ($result as $row) {
					
					$key = $row['id'];
					$item = $row['item'];
					$pos = $row['pos'];
					$warehouse = $row['warehouse'];
					
					if ($pos) {
						$k = $pos;
						$caption = $row['pos_caption'];
					}
					
					if ($warehouse) {
						$k = $row['warehouse_key'];
						$caption = $row['warehouse_caption'];
					}
					
					$datagrid[$k]['caption'] = $caption;
					$datagrid[$k]['items'][$item]['caption'] = $row['item_caption'];
					$datagrid[$k]['items'][$item]['distributions'][$key]['date'] = $row['confirmed_dete'];
					
					$distributions[] = $key;
				}
			}
			
			// distribution keys
			$distributions = ($distributions) ? join(',', array_unique($distributions)) : null;
			
			
			// sales order items
			$result = $exchange->query("
				SELECT 
					sap_salesorder_item_mps_item_id AS item,
					sap_salesorder_item_sold_to AS sap,
					sap_salesorder_item_ship_to As ship,
					sap_salesorder_item_ean_number AS ean,
					sap_salesorder_item_mps_po_number AS pon,
					sap_salesorder_item_mps_po_line_number AS line,
					sap_salesorder_itemr_quantity AS quantity,
					DATE_FORMAT(sap_salesorder_item_fetched_by_sap, '%d.%m.%Y') AS date
				FROM sap_salesorder_items
				WHERE sap_salesorder_item_mps_item_id IN ($distributions)
			")->fetchAll();
			
			$data['orders'] = _array::datagrid($result);
			
			// confirmed items
			$result = $model->query("
				SELECT 
					sap_confirmed_item_mps_distribution_id AS item,
					sap_confirmed_item_sold_to AS sap,
					sap_confirmed_item_ship_to AS ship,
					sap_confirmed_item_mps_po_number AS pon,
					sap_confirmed_item_ean_number AS ean,
					sap_confirmed_item_confirmed_quantity AS quantity,
					sap_confirmed_item_mps_po_line_number AS line,
					sap_confirmed_item_status_code AS code,
					sap_confirmed_item_sap_order_number AS order_number,
					sap_confirmed_item_sap_order_line_number AS order_line_number
				FROM sap_confirmed_items
				WHERE sap_confirmed_item_mps_distribution_id IN ($distributions)
			")->fetchAll();
			
			$data['confirmed'] = _array::datagrid($result);
			
			
			// shipped items
			$result = $model->query("
				SELECT 
					sap_shipped_item_mps_distribution_id AS item,
					sap_shipped_item_shipped_to AS ship,
					sap_shipped_item_mps_po_number AS pon,
					sap_shipped_item_mps_po_line_number AS line,
					sap_shipped_item_ean_number AS ean,
					sap_shipped_item_quantity AS quantity,
					sap_shipped_item_status_code AS code,
					sap_shipped_item_sap_order_number AS order_number,
					sap_shipped_item_sap_order_line_number As order_line_number
				FROM sap_shipped_items
				WHERE sap_shipped_item_mps_distribution_id IN ($distributions)
			")->fetchAll();
			
			if ($result) {
				
				foreach ($result as $row) {
					
					$key = $row['item'];
					
					$data['shipped'][$key] = array(
						'ship' => $row['ship'],
						'pon' => $row['pon'],
						'line' => $row['line'],
						'ean' => $row['ean'],
						'code' => $row['code'],
						'order_number' => $row['order_number'],
						'order_line_number' => $row['order_line_number'],
						'quantity' => $row['quantity'] + $shippedOrders[$key]['quantity']
					);
				}
			}
			
			
			// button back
			$buttons['back'] = '/'.$this->request->application.'/'.$this->request->controller;

			// template: headers
			$this->view->header('pagecontent')->node('header')
			->data('title', $ordersheet->header())
			->data('subtitle', 'Workflow status: '.$ordersheet->workflowstate()->name);
			
			$this->view->sapRaport('pagecontent')->sap('raport.data')
			->data('data', $data)
			->data('datagrid', $datagrid)
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}