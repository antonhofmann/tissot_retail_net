<?php 

	class Integrity_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			$this->request->application = url::application();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			// request form: add button
			$field = $this->request->query('add');
			$this->request->field('add', $field);
			
			// request form: link data
			$field = $this->request->query('table');
			$this->request->field('form', $field);
			
			// template: integrity table list
			$this->view->dbTables('pagecontent')
			->db('table.list');
		}
		
		public function add() {
			
			// permissions
			$permission_edit = user::permission(DB_Table::PERMISSION_EDIT);
		
			if (!$permission_edit) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."chain/chained.select.js");
		
			// form datalaoder
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('table');
			
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
		
			// dataloader:  databases				
			$dataloader['db_table_db'] = db::loader();
			
			$this->view->dbTable('pagecontent')
			->db('table.data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons);
		}
		
		public function table() {
			
			$id = url::param();
			
			$db_tables = new DB_Table();
			$db_tables->read($id);
				
			if (!$db_tables->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."chain/chained.select.js");
			
			// permissions
			$permission_edit = user::permission(DB_Table::PERMISSION_EDIT);
			
			// form dataloader
			$data = $db_tables->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('table');
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// button: save
			if ($permission_edit) {
				
				$buttons['save'] = true;
				
				$references = $db_tables->get_references();
			
				// button: delete
				if (!$references) {
					$buttons['delete'] = $this->request->link('/applications/helpers/db.table.delete.php', array('id'=>$id));
				}
			}
			elseif ($data) {
				foreach ($data as $field => $value) {
					$disabled[$field] = true;
				}
			}
			
			// disabled fields
			$disabled['db_table_db'] = true;
			$disabled['db_table_table'] = true;
			
			$model = new Model(Connector::DB_CORE);
			
			$result = $model->query("
				SELECT db_table_id, db_table_table
				FROM db_tables
			")
			->filter('default', "db_table_id <> $id")
			->order('db_table_table')
			->fetchAll();
			
			// dataloader: table parents
			$dataloader['db_table_parent_table'] = _array::extract($result);
			
			// get table name
			$dataloader['db_table_table'][$db_tables->table] = $db_tables->description.' ('.$db_tables->table.')';
			
			// dataloader:  databases				
			$dataloader['db_table_db'] = db::loader();
			
			// template: headers
			$this->view->header('pagecontent')->node('header')->data('title', $db_tables->description);
			$this->view->tabs('pagecontent')->navigation('tab');
				
			// template: db table form
			$this->view->db_table('pagecontent')
			->db('table.data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function exports() {
			
			$id = url::param();
				
			$table = new DB_Table();
			$table->read($id);

			if (!$table->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
			Compiler::attach(DIR_CSS."jquery.ui.css");
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."chain/chained.select.js");
				
			// permissions
			$permission_edit = user::permission(DB_Table::PERMISSION_EDIT);
				
			// form dataloader
			$data = $table->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			
			// order array
			$orderArray = ($data['db_table_export_fields_order']) ? unserialize($data['db_table_export_fields_order']) : array();
			$data['db_table_export_fields_order'] = ($orderArray) ? join(',',$orderArray) : null;
				
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// translator
			$translate = translate::instance();
			
			// db
			$db = new DB();
			$db->read($table->db);
			
			// db model
			$model = new Model($db->name);
			
			// db fields
			if($table->data['db_table_table_join'] and $table->table == 'addresses' ) {

				$query = "select column_name as Field
						from information_schema.columns 
						where table_schema = '" . $db->name. "' and table_name in ('".$table->table."', '".$table->data['db_table_table_join'] ."');";

				
				$sth = $model->db->query($query);
			}
			else {
				$sth = $model->db->query("
					SHOW COLUMNS 
					FROM ".$table->table." 
					FROM ".$db->name."
				");
			}
			
			$result = ($sth) ? $sth->fetchAll() : null;
				
			if ($result) {
				foreach ($result as $row) {
					$field = $row['Field'];
					$label = $translate->$field;
					$dataloader['db_table_export_fields'][$field] = "$label <i class=gray >($field)</i>";
				}
			}
			
			//echo join(',<br>', array_keys($dataloader['db_table_export_fields']));
			
			$selected = ($table->export_fields) ? count(unserialize($table->export_fields)) : 0;
			$data['select_all'] = (count($dataloader['db_table_export_fields'])==$selected) ? 1 : 0;
			
			if ($orderArray) {
				$dataloader['db_table_export_fields'] = _array::sortByArray($dataloader['db_table_export_fields'], $orderArray);
			}
			
			// button: save
			if ($permission_edit) {
				$buttons['save'] = true;
			}
			elseif ($data) {
				foreach ($data as $field => $value) {
					$disabled[$field] = true;
				}
			}

			// template: headers
			$this->view->header('pagecontent')->node('header')->data('title', $table->description);
			$this->view->tabs('pagecontent')->navigation('tab');
			
			// template: db table form
			$this->view->db_table('pagecontent')
			->db('table.exports')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function references() {
				
			$id = url::param();
			$param = url::param(1);
			
			$db_tables = new DB_Table(Connector::DB_CORE);
			$db_tables->read($id);
				
			if (!$db_tables->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			// permissions
			$permission_edit = user::permission(DB_Table::PERMISSION_EDIT);
			
			// buttons
			$buttons = array();
				
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $db_tables->description);
			
			// template: navigation tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
	
			if ($param) {
				
				Compiler::attach(DIR_SCRIPTS."loader/loader.css");
				Compiler::attach(DIR_SCRIPTS."loader/loader.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
				Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
				Compiler::attach(DIR_SCRIPTS."chain/chained.select.js");
				
				// button: back
				$buttons['back'] = $this->request->query("references/$id");
				
				// button save
				if ($permission_edit) {
					$buttons['save'] = true;
				}
				
				$param = (is_numeric($param)) ? $param : null;
				
				if ($param) {
					
					$db_reference = new DB_Reference();
					$data = $db_reference->read($param);
					
					// get referenced db
					$table_reference = new DB_Table();
					$table_reference->read($db_reference->referenced_table_id);
					
					// form dataloader
					$data['db_table_db'] = $table_reference->db;
					
					// get referenced table name
					$table_reference = new DB_Table();;
					$table_reference->read($db_reference->referring_table);
					$dataloader['db_reference_referring_table'][$db_reference->referring_table] = $table_reference->description.' ('.$table_reference->table.')';
					
					// disabled fields
					$disabled['db_table_db'] = true;
					$disabled['db_reference_referring_table'] = true;
					
					// set forigne key
					$dataloader['db_reference_foreign_key'][$db_reference->foreign_key] = $db_reference->foreign_key;
					$disabled['db_reference_foreign_key'] = true;
					
					// button delete
					if ($permission_edit) {
						$buttons['delete'] =$this->request->link('/applications/helpers/db.reference.delete.php', array(
							'table' => $id,
							'id' => $param
						));
					}
					elseif ($data) {
						foreach ($data as $key => $value) {
							$disabled[$key] = true;
						}
					}
				}
				else {
					$data['redirect'] =  $this->request->query("references/$id");
				}
				
				// form dataoader	
				$data['db_reference_referenced_table_id'] = $id;
				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;
				$data['action'] = $this->request->action;
				
				// dataloader: databases
				$dataloader['db_table_db'] = db::loader();
					
				$this->view->dbTableRreferenceForm('pagecontent')
				->db('reference.data')
				->data('data', $data)
				->data('dataloader', $dataloader)
				->data('hidden', $hidden)
				->data('disabled', $disabled)
				->data('buttons', $buttons)
				->data('id', $id);
			}
			else {
				
				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
				Compiler::attach(DIR_SCRIPTS."table.loader.js");
				Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
				Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
				Compiler::attach(DIR_SCRIPTS."loader/loader.css");
				Compiler::attach(DIR_SCRIPTS."loader/loader.js");
				
				$buttons['back'] = $this->request->query();
				
				// request form: id
				$this->request->field('id', $id);
				
				// request form: add button
				$field = $this->request->query("references/$id/add");
				$this->request->field('add', $field);
				
				// request form: edit link
				$field = $this->request->query("references/$id");
				$this->request->field('form', $field);
					
				$this->view->dbTableReferencesList('pagecontent')
				->db('reference.list')
				->data('buttons', $buttons)
				->data('id', $id);
			}
		}
	}
