<?php 

	class ItemGroups_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() { 
			

			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			if ($permission_edit) {
				$this->request->field('add',$this->request->query('add'));
			}
			
			if ($permission_edit || $permission_view) {
				$this->request->field('data',$this->request->query('data'));
			}
			
			// template: role list
			$this->view->placeholder('pagecontent')
			->item('groups')
			->data('buttons', $buttons);
		}

		public function add() {

			$permission_edit = user::permission('can_edit_catalog');
		
			if($this->request->archived || !$permission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
				
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();

			$model = new Model(Connector::DB_CORE);

			$result = $model->query("
				SELECT 
					category_id, 
					CONCAT(product_line_name, ': ', category_name) AS categoryname
				FROM categories 
				LEFT JOIN product_lines ON product_line_id = category_product_line
				WHERE category_catalog = 1 AND (category_not_in_use = 0 OR category_not_in_use = '')
				ORDER BY product_line_name, category_name
			")->fetchAll();

			$dataloader['item_group_category'] = _array::extract($result); 
		
			// template: application form
			$this->view->productLine('pagecontent')
			->item('group.form')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons);
		}
		
		public function data() {

			$id = url::param();

			$itemGroup = new Item_Group();
			$itemGroup->read($id);

			if (!$itemGroup->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			$data = $itemGroup->data; 
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			if ($permission_edit) {
			
				$buttons['save'] = true;

				$integrity = new Integrity();
				$integrity->set($id, 'item_groups');

				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/item.group.delete.php', array('id'=>$id));
				}
			}
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}

			$model = new Model(Connector::DB_CORE);

			if ($itemGroup->category) {
				$filter = " OR  category_id = $itemGroup->category";
			}

			$result = $model->query("
				SELECT 
					category_id, 
					CONCAT(product_line_name, ': ', category_name) AS categoryname
				FROM categories 
				LEFT JOIN product_lines ON product_line_id = category_product_line
				WHERE category_catalog = 1 AND (category_not_in_use = 0 OR category_not_in_use = '') $filter
				ORDER BY product_line_name, category_name
			")->fetchAll();

			$dataloader['item_group_category'] = _array::extract($result); 

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $itemGroup->name);
			$this->view->companytabs('pagecontent')->navigation('tab');
		
			// template: form
			$this->view->productLine('pagecontent')
			->item('group.form')
			->data('dataloader', $dataloader)
			->data('data', $data)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons);
		}
		
		public function items() {

			$id = url::param();

			$itemGroup = new Item_Group();
			$itemGroup->read($id);

			if (!$itemGroup->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$this->request->field('group', $id);

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			$model = new Model(Connector::DB_CORE);

			$result = $model->query("
				SELECT 
					item_group_item_item,
					item_group_item_quantity
				FROM item_group_items
				WHERE item_group_item_group = $id
			")->fetchAll();

			$quantities = _array::extract($result);

			$result = $model->query("
				SELECT 
					item_id, 
					item_code, 
					item_name
				FROM items 
				INENR JOIN item_types ON item_type = item_type_id
				WHERE item_type = 1
				ORDER BY item_code
			")->fetchAll();

			$datagrid = array();

			if ($result) {
				
				foreach ($result as $row) {
					
					$k = $row['item_id'];
					$quantity = $quantities[$k];

					if ($permission_edit) {
						$datagrid[$k]['quantity'] = "<input type='text' value='$quantity' class='item-quantity' data-id='$k' >";
					} else {
						$datagrid[$k]['quantity'] = $quantity;
					}

					$datagrid[$k]['item_code'] = $row['item_code'];
					$datagrid[$k]['item_name'] = $row['item_name'];
				}
			}

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $itemGroup->name);
			$this->view->companytabs('pagecontent')->navigation('tab');
		
			// template: form
			$this->view->productLine('pagecontent')
			->item('group.items')
			->data('datagrid', $datagrid)
			->data('data', $data)
			->data('buttons', $buttons);
		}
		
		public function options() {

			$id = url::param();
			$param = url::param(1);

			$itemGroup = new Item_Group();
			$itemGroup->read($id);

			if (!$itemGroup->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $itemGroup->name);
			$this->view->companytabs('pagecontent')->navigation('tab');

			// buttons
			$buttons = array();

			$model = new Model(Connector::DB_CORE);

			if ($param) {

				$buttons['back'] = $this->request->query("options/$id");

				$param = (is_numeric($param)) ? $param : null;

				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
				Compiler::attach(DIR_SCRIPTS."table.loader.js");

				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				Compiler::attach(DIR_JS."form.validator.js");

				$itemGroupOption = new Item_Group_Option();
				$itemGroupOption->read($param);

				$data = $itemGroupOption->data;
				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;
				$data['action'] = $this->request->action;
				$data['item_group_option_group'] = $id;
				$data['redirect'] = $this->request->query("options/$id");
				$data['can_replace1'] = "can replace";
				$data['can_replace2'] = "can replace";

				if ($permission_edit) {
				
					$buttons['save'] = true;
					
					if ($itemGroupOption->id) {
						
						if ($param) {
							$buttons['add_file'] = "/applications/templates/item.group.option.file.php?";
						}

						$integrity = new Integrity();
						$integrity->set($itemGroupOption->id, 'item_group_options');

						if ($integrity->check()) {
							$buttons['delete'] = $this->request->link('/applications/helpers/item.group.options.delete.php', array('id'=>$param));
						}
					}

				} elseif($data) {
					
					foreach ($data as $key => $value) {
						$disabled[$key] = true;
					}
				}

				// item 1 filter
				$filter = $itemGroupOption->item1 ? "OR item_id = $itemGroupOption->item1" : null;

				// item 1
				$result = $model->query("
					SELECT 
						item_id, 
						CONCAT(item_code, ': ', LEFT(item_name, 50)) as itemname 
					FROM items  
					WHERE item_type = 1 $filter
					ORDER BY item_code, item_name
				")->fetchAll();

				$dataloader['item_group_option_item1'] = _array::extract($result);


				// item 2 filter
				$filter = $itemGroupOption->item2 ? "OR item_id = $itemGroupOption->item2" : null;

				// item 2
				$result = $model->query("
					SELECT 
						item_id, 
						CONCAT(item_code, ': ', LEFT(item_name, 50)) as itemname 
					FROM items  
					WHERE item_type = 1 $filter
					ORDER BY item_code, item_name
				")->fetchAll();

				$dataloader['item_group_option_item2'] = _array::extract($result);


				// item 3 filter
				$filter = $itemGroupOption->item3 ? "OR item_id = $itemGroupOption->item3" : null;

				// item 3
				$result = $model->query("
					SELECT 
						item_id, 
						CONCAT(item_code, ': ', LEFT(item_name, 50)) as itemname 
					FROM items  
					WHERE item_type = 1 $filter
					ORDER BY item_code, item_name
				")->fetchAll();

				$dataloader['item_group_option_item3'] = _array::extract($result);


				// item 4 filter
				$filter = $itemGroupOption->item4 ? "OR item_id = $itemGroupOption->item4" : null;

				// item 3
				$result = $model->query("
					SELECT 
						item_id, 
						CONCAT(item_code, ': ', LEFT(item_name, 50)) as itemname 
					FROM items  
					WHERE item_type = 1 $filter
					ORDER BY item_code, item_name
				")->fetchAll();

				$dataloader['item_group_option_item4'] = _array::extract($result);

				$dataloader['item_group_option_logical'] = array(
					0 => 'and',
					1 => 'or'
				);

				// template: form
				$this->view->productLine('pagecontent')
				->item('group.option.form')
				->data('dataloader', $dataloader)
				->data('data', $data)
				->data('hidden', $hidden)
				->data('disabled', $disabled)
				->data('buttons', $buttons);

			} else {

				$buttons['back'] = $this->request->query();

				if ($permission_edit) {
					$buttons['add'] = $this->request->query("options/$id/add");
				}

				$result = $model->query("
					SELECT 
						item_group_option_id, 
						item_group_option_name
					FROM item_group_options
					WHERE item_group_option_group = $id
					ORDER BY item_group_option_name
				")->fetchAll();

				$datagrid = _array::datagrid($result);
			
				// template: form
				$this->view->productLine('pagecontent')
				->item('group.option.list')
				->data('datagrid', $datagrid)
				->data('link', $this->request->query("options/$id"))
				->data('buttons', $buttons);
			}
		}
	}