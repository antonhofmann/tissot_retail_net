<?php 

	class Certificates_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
				
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {

			$permission_edit = user::permission('can_edit_catalog');
			$permission_edit_certificates = user::permission('can_edit_certificates');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			if ($permission_edit || $permission_edit_certificates) {
				$this->request->field('add',$this->request->query('add'));
			}
			
			$this->request->field('data',$this->request->query('data'));

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'certificate/list.php');
			$tpl->data('data', $data);
			$tpl->data('buttons', $buttons);
			$this->view->setTemplate('certificates', $tpl);

			Compiler::attach(array(
				DIR_SCRIPTS."dropdown/dropdown.css",
				DIR_SCRIPTS."dropdown/dropdown.js",
				DIR_SCRIPTS."table.loader.js"
			));
		}

		public function add() {

			$permission_edit = user::permission('can_edit_catalog');
			$permission_edit_certificates = user::permission('can_edit_certificates');
		
			if($this->request->archived || (!$permission_edit && !$permission_edit_certificates)) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
				
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();

			$model = new Model(Connector::DB_CORE);

			$result = $model->query("
				SELECT DISTINCT
					certificate_title_id,
					certificate_title_title
				FROM certificate_titles
				ORDER BY certificate_title_title
			")->fetchAll();

			$dataloader['certificate_title_id'] = _array::extract($result);

			$result = $model->query("
				SELECT DISTINCT
					address_id,
					address_company
				FROM suppliers
				INNER JOIN addresses ON supplier_address = address_id
				WHERE address_active = 1
				ORDER BY address_company
			")->fetchAll();

			$dataloader['certificate_address_id'] = _array::extract($result);
			
			$result = $model->query("
				SELECT DISTINCT
					catalog_material_id,
					catalog_material_name
				FROM catalog_materials
				ORDER BY catalog_material_name
			")->fetchAll();

			$dataloader['materials'] = _array::extract($result);

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'certificate/form.php');
			$tpl->data('data', $data);
			$tpl->data('dataloader', $dataloader);
			$tpl->data('buttons', $buttons);
			$tpl->data('hidden', $hidden);
			$tpl->data('disabled', $disabled);
			$this->view->setTemplate('certificate', $tpl);

			Compiler::attach(array(
				DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css",
				DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js",
				DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js",
				DIR_SCRIPTS."qtip/qtip.css",
				DIR_SCRIPTS."qtip/qtip.js",
				DIR_JS."form.validator.js"
			));
		}
		
		public function data() {

			$id = url::param();

			$certificate = new Certificate();
			$certificate->read($id);

			$model = new Model(Connector::DB_CORE);

			if (!$certificate->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$permission_edit = user::permission('can_edit_catalog');
			$permission_edit_certificates = user::permission('can_edit_certificates');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			$data = $certificate->data ?: array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			if ($permission_edit || $permission_edit_certificates) {
			
				$buttons['save'] = true;

				$integrity = new Integrity();
				$integrity->set($id, 'certificates', 'system');

				if ($id && $integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/modules/cerificates/delete.php', array('id'=>$id));
				}
			}

			$model = new Model(Connector::DB_CORE);

			$result = $model->query("
				SELECT DISTINCT
					certificate_title_id,
					certificate_title_title
				FROM certificate_titles
				ORDER BY certificate_title_title
			")->fetchAll();

			$dataloader['certificate_title_id'] = _array::extract($result);

			$result = $model->query("
				SELECT DISTINCT
					address_id,
					address_company
				FROM suppliers
				INNER JOIN addresses ON supplier_address = address_id
				WHERE address_active = 1
				ORDER BY address_company
			")->fetchAll();

			$dataloader['certificate_address_id'] = _array::extract($result);
			
			$result = $model->query("
				SELECT DISTINCT
					catalog_material_id,
					catalog_material_name
				FROM catalog_materials
				ORDER BY catalog_material_name
			")->fetchAll();

			$dataloader['materials'] = _array::extract($result);

			$result = $model->query("
				SELECT certificate_material_catalog_material_id
				FROM certificate_materials
				WHERE certificate_material_certificate_id = $id
			")->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$serialize[] = $row['certificate_material_catalog_material_id'];
				}
				$data['materials'] = serialize($serialize);
			}

			if ($this->request->archived || (!$permission_edit && !$permission_edit_certificates)) {
				$fields = array_keys($data);
				$disabled = array_fill_keys($fields, true);
				$disabled['materials'] = true;
			}

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $certificate->name);
			$this->view->companytabs('pagecontent')->navigation('tab');

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'certificate/form.php');
			$tpl->data('data', $data);
			$tpl->data('dataloader', $dataloader);
			$tpl->data('buttons', $buttons);
			$tpl->data('hidden', $hidden);
			$tpl->data('disabled', $disabled);
			
			$this->view->setTemplate('certificate', $tpl);

			Compiler::attach(array(
				DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css",
				DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js",
				DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js",
				DIR_SCRIPTS."qtip/qtip.css",
				DIR_SCRIPTS."qtip/qtip.js",
				DIR_JS."form.validator.js"
			));
		}


		public function items() {

			$id = url::param();

			$certificate = new Certificate();
			$certificate->read($id);

			$model = new Model(Connector::DB_CORE);

			if (!$certificate->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$permission_edit = user::permission('can_edit_catalog');
			$permission_edit_certificates = user::permission('can_edit_certificates');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			$data = $certificate->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			$model = new Model(Connector::DB_CORE);

			$result = $model->query("
				SELECT item_certificate_item_id
				FROM item_certificates
				WHERE item_certificate_certificate_id = $id
			")->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$item = $row['item_certificate_item_id'];
					$itemCertificates[$item] = true;
				}
			}

			// supplier item
			$result = $model->query("
				SELECT DISTINCT
					item_id,
					item_code,
					item_name
				FROM items
				INNER JOIN suppliers ON supplier_item = item_id
				WHERE supplier_address = $certificate->address_id
				ORDER BY item_code
			")->fetchAll();

			$disabled = $this->request->archived || (!$permission_edit && !$permission_edit_certificates) ? true : false;

			if ($result) {
				foreach ($result as $row) {
					
					$item = $row['item_id'];
					$items[$item]['item_code'] = $row['item_code'];
					$items[$item]['item_name'] = $row['item_name'];

					$checked = $itemCertificates[$item] ? "checked=checked" : null;

					if (!$disabled) {
						$items[$item]['checkbox'] = "<input type='checkbox' value='$item' class='item_certificate' $checked />";
					} elseif($checked) {
						$items[$item]['checkbox'] = "<i class='fa fa-check-circle' ></i>";
					}
				}
			}

			// set request id to form
			$this->request->field('id', $id);

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $certificate->name);
			$this->view->companytabs('pagecontent')->navigation('tab');


			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'certificate/item/list.php');
			$tpl->data('data', $data);
			$tpl->data('items', $items);
			$tpl->data('buttons', $buttons);
			$this->view->setTemplate('certificateItems', $tpl);

			Compiler::attach(array(
				DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css",
				DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js",
				DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js",
				DIR_SCRIPTS."qtip/qtip.css",
				DIR_SCRIPTS."qtip/qtip.js",
				DIR_JS."form.validator.js"
			));
		}


		public function versions() {

			$id = url::param();
			$param = url::param(1);

			$certificate = new Certificate();
			$certificate->read($id);

			$model = new Model(Connector::DB_CORE);

			if (!$certificate->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$permission_edit = user::permission('can_edit_catalog');
			$permission_edit_certificates = user::permission('can_edit_certificates');
			$permission_edit_files = user::permission('can_edit_files_in_catalogue');
			$permission_view = user::permission('can_browse_catalog_in_admin');

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $certificate->name);
			$this->view->companytabs('pagecontent')->navigation('tab');

			// buttons 
			$buttons = array();
			
			if ($param)  { 

				if (!$permission_edit && !$permission_edit_files && !$permission_edit_certificates) {
					Message::access_denied();
					url::redirect($this->request->query());
				}
				
				$buttons['back'] = $this->request->query("versions/$id");

				Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
				Compiler::attach(DIR_CSS."jquery.ui.css");
				Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
				Compiler::attach(DIR_SCRIPTS."ajaxuploader/ajaxupload.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				Compiler::attach(DIR_JS."form.validator.file.js");

				$param = (is_numeric($param)) ? $param : null;

				$certificateFile = new Certificate_File();
				$certificateFile->read($param);
				
				$data = $certificateFile->data;
				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;
				$data['certificate_file_certificate_id'] = $id;
				$data['redirect'] = $this->request->query("versions/$id");
	
				if ($data['certificate_file_expiry_date']) {
					$data['certificate_file_expiry_date'] = date::system($data['certificate_file_expiry_date']);
				}
				
				$buttons['save'] = true;

				$integrity = new Integrity();
				$integrity->set($id, 'certificate_files', 'system');

				if ($certificateFile->id && $integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/modules/cerificates/file/delete.php', array('id'=>$param));
				}

				if ($result) {
					foreach ($result as $row) {
						$serialize[] = $row['certificate_material_catalog_material_id'];
					}
					$data['materials'] = serialize($serialize);
				}

				// list view
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'certificate/file/form.php');
				$tpl->data('data', $data);
				$tpl->data('buttons', $buttons);
				$tpl->data('hidden', $hidden);
				$tpl->data('disabled', $disabled);
				$this->view->setTemplate('certificateFile', $tpl);

				Compiler::attach(array(
					DIR_SCRIPTS."jquery/jquery.ui.css",
					DIR_CSS."jquery.ui.css",
					DIR_SCRIPTS."jquery/jquery.ui.js",
					DIR_SCRIPTS."ajaxuploader/ajaxupload.js",
					DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css",
					DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js",
					DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js",
					DIR_SCRIPTS."qtip/qtip.css",
					DIR_SCRIPTS."qtip/qtip.js",
					DIR_JS."form.validator.file.js"
				));
			}
			else {

				$buttons['back'] = $this->request->query();

				if ($permission_edit || $permission_edit_files || $permission_edit_certificates) {
					$buttons['add'] = $this->request->query("versions/$id/add");
				}

				$result = $model->query("
					SELECT 
						certificate_file_id AS id,
						certificate_file_version AS title, 
						certificate_file_expiry_date AS date, 
						certificate_file_file AS file
					FROM certificate_files 
					WHERE certificate_file_certificate_id = $id
					ORDER BY certificate_file_expiry_date, certificate_file_version 
				")->fetchAll();

				if ($result) {

					$files = array();
					$link = $this->request->query("versions/$id");

					$today = date('U');

					foreach ($result as $row) {
						$i = $row['id'];
						$extension = file::extension($row['file']);
						$timestamp = strtotime($row['date']);
						$expired = strtotime($row['date']) < $today ? 'expired' : null;
						$files[$i]['certificate_file_version'] = "<a href='$link/$i' >{$row[title]}</a>";
						$files[$i]['certificate_file_expiry_date']= "<span class='$expired'>".date::system($row[date])."</span>";
						$files[$i]['fileicon']  = "<span class='file-extension $extension modal' tag='{$row[file]}'></span>";
					}
				}

				// list view
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'certificate/file/list.php');
				$tpl->data('files', $files);
				$tpl->data('buttons', $buttons);
				$this->view->setTemplate('certificateFiles', $tpl);
			}
		}

		public function titles() {

			$id = url::param();

			$permission_edit = user::permission('can_edit_catalog');
			$permission_edit_certificates = user::permission('can_edit_certificates');
			$permission_view = user::permission('can_browse_catalog_in_admin');

			if ($id) {

				$id = is_numeric($id) ? $id : null;

				$modul = new Modul();
				$modul->setTable('certificate_titles');
				$modul->read($id);

				if ( ($id && !$modul->id) || (!$permission_edit && !$permission_edit_certificates && !$permission_view) ) {
					Message::access_denied();
					url::redirect($this->request->query('titles'));
				}
				
				$data = $modul->data ?: array();
				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;

				if (!$id) {
					$data['redirect'] = $this->request->query('titles');
				}

				// buttons
				$buttons = array();
				$buttons['back'] = $this->request->query('titles');

				if ($permission_edit || $permission_edit_certificates) {
				
					$buttons['save'] = true;

					if ($id) {
						
						$integrity = new Integrity();
						$integrity->set($id, 'certificate_titles', 'system');

						if ($integrity->check()) {
							$buttons['delete'] = $this->request->link('/applications/modules/cerificates/title/delete.php', array('id'=>$id));
						}
					}
				}

				if ($this->request->archived || (!$permission_edit && !$permission_edit_certificates)) {
					$fields = array_keys($data);
					$disabled = array_fill_keys($fields, true);
				}

				// header
				$this->view->header('pagecontent')
				->node('header')
				->data('title', $data['certificate_title_title']);

				// list view
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'certificate/title/form.php');
				$tpl->data('data', $data);
				$tpl->data('buttons', $buttons);
				$tpl->data('disabled', $disabled);
				$this->view->setTemplate('certificateTitle', $tpl);

				Compiler::attach(array(
					DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css",
					DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js",
					DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js",
					DIR_SCRIPTS."qtip/qtip.css",
					DIR_SCRIPTS."qtip/qtip.js",
					DIR_JS."form.validator.js"
				));

			} else {
			
				if ($permission_edit || $permission_edit_certificates) {
					$this->request->field('add',$this->request->query('titles/add'));
				}
			
				$this->request->field('data',$this->request->query('titles'));

				// list view
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'list.php');
				$tpl->data('url', '/applications/modules/cerificates/title/list.php');
				$tpl->data('class', 'list-500');
				$this->view->setTemplate('certificateTitles', $tpl);

				Compiler::attach(array(
					'/public/scripts/dropdown/dropdown.css',
					'/public/scripts/dropdown/dropdown.js',
					'/public/scripts/table.loader.js'
				));
			}
		}
	}