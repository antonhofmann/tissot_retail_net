<?php 

	class Translationresponsibles_Controller {
		
		public function __construct() {
			
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {

			// view collection
			$link = $this->request->query('data');
			$this->request->field('form', $link);

			$permission_edit = user::permission('can_edit_catalog');
		
			if(!$permission_edit) {
				url::redirect('/messages/show/access-denied');
			}

			// button: add new
			$link = $this->request->query('add');
			$this->request->field('add', $link);

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'list.php');
			$tpl->data('url', '/applications/modules/translationresponsible/list.php');
			$tpl->data('class', 'list-600');
			$this->view->setTemplate('brands', $tpl);

			Compiler::attach(array(
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js'
			));
		}
		
		public function add() {
			
			$permission_edit = user::permission('can_edit_catalog');
		
			if(!$permission_edit) {
				Message::access_denied();
				url::redirect('/messages/show/access-denied');
			}

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
	
			// form hidden vars
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			

			$model = new Model(Connector::DB_CORE);
			
			//dataloader addresses
			$result = $model->query("
				SELECT DISTINCT
					address_id,
					concat(country_name, ' ', address_company) as address_company
				FROM addresses
				LEFT JOIN countries on country_id = address_country 
				WHERE address_type = 1 and address_active = 1
				ORDER BY address_company
			")->fetchAll();
			$dataloader['translationresponsible_address_id'] = _array::extract($result);

			//dataloader countries
			$result = $model->query("
				SELECT DISTINCT
					country_id,
					country_name
				FROM countries
				ORDER BY country_name
			")->fetchAll();
			$dataloader['translationresponsible_country_id'] = _array::extract($result);


			//dataloader roles
			$result = $model->query("
				SELECT DISTINCT
					role_id,
					role_name
				FROM roles
				ORDER BY role_name
			")->fetchAll();
			$dataloader['translationresponsible_role_id'] = _array::extract($result);


			//dataloader users
			$result = $model->query("
				SELECT DISTINCT
					user_id,
					concat(country_name , ' - ', user_name, ' ', user_firstname) as username
				FROM users
				Left join addresses on user_address = address_id
				left join countries on country_id = address_country
				WHERE user_active = 1
				ORDER BY username
			")->fetchAll();
			$dataloader['translationresponsible_user_id'] = _array::extract($result);


			//data loader languages
			$result = $model->query("
				SELECT DISTINCT
					language_id,
					concat(language_iso639_1 , ' - ', language_name) as language_name
				FROM languages
				ORDER BY language_name
			")->fetchAll();
			$dataloader['translationresponsible_language_id'] = _array::extract($result);

				
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'translationresponsible/form.php');
			$tpl->data('data', $data);
			$tpl->data('dataloader', $dataloader);
			$tpl->data('buttons', $buttons);

			$this->view->setTemplate('translationresponsible', $tpl);


			
			// script integration
			Compiler::attach(array(
					DIR_SCRIPTS."jquery/jquery.ui.css",
					DIR_CSS."jquery.ui.css",
					DIR_SCRIPTS."jquery/jquery.ui.js",
					DIR_SCRIPTS."ajaxuploader/ajaxupload.js",
					DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css",
					DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js",
					DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js",
					DIR_SCRIPTS."qtip/qtip.css",
					DIR_SCRIPTS."qtip/qtip.js",
					DIR_JS."form.validator.file.js"
				));

		}
		
		public function data() {
				
			$permission_edit = user::permission('can_edit_catalog');
		
			if(!$permission_edit) {
				url::redirect('/messages/show/access-denied');
			}
			
			
			$id = url::param();


			if (!$id) {
				url::redirect('/messages/show/access-denied');
			}

			$application = $this->request->application;


			// modul: brand
			$translationresponsible = new Modul(DB_CORE);
			$translationresponsible->setTable('translationresponsibles');
			$data = $translationresponsible->read($id);

			// request vars
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;

			
			$model = new Model(Connector::DB_CORE);
			
			//dataloader addresses
			$result = $model->query("
				SELECT DISTINCT
					address_id,
					concat(country_name, ' ', address_company) as address_company
				FROM addresses
				LEFT JOIN countries on country_id = address_country 
				WHERE address_type = 1 and address_active = 1
				ORDER BY address_company
			")->fetchAll();
			$dataloader['translationresponsible_address_id'] = _array::extract($result);


			//dataloader countries
			$result = $model->query("
				SELECT DISTINCT
					country_id,
					country_name
				FROM countries
				ORDER BY country_name
			")->fetchAll();
			$dataloader['translationresponsible_country_id'] = _array::extract($result);


			//dataloader roles
			$result = $model->query("
				SELECT DISTINCT
					role_id,
					role_name
				FROM roles
				ORDER BY role_name
			")->fetchAll();
			$dataloader['translationresponsible_role_id'] = _array::extract($result);


			//dataloader users
			$result = $model->query("
				SELECT DISTINCT
					user_id,
					concat(country_name , ' - ', user_name, ' ', user_firstname) as username
				FROM users
				Left join addresses on user_address = address_id
				left join countries on country_id = address_country
				WHERE user_active = 1
				ORDER BY username
			")->fetchAll();
			$dataloader['translationresponsible_user_id'] = _array::extract($result);


			//data loader languages
			$result = $model->query("
				SELECT DISTINCT
					language_id,
					concat(language_iso639_1 , ' - ', language_name) as language_name
				FROM languages
				ORDER BY language_name
			")->fetchAll();
			$dataloader['translationresponsible_language_id'] = _array::extract($result);


			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			$buttons['save'] = true;
					
			$buttons['delete'] = $this->request->link('/applications/modules/translationresponsible/delete.php', array('id'=>$id));
			

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'translationresponsible/form.php');
			$tpl->data('data', $data);
			$tpl->data('dataloader', $dataloader);
			$tpl->data('buttons', $buttons);
			
			$this->view->setTemplate('translationresponsible', $tpl);

			// script integration
			Compiler::attach(array(
					DIR_SCRIPTS."jquery/jquery.ui.css",
					DIR_CSS."jquery.ui.css",
					DIR_SCRIPTS."jquery/jquery.ui.js",
					DIR_SCRIPTS."ajaxuploader/ajaxupload.js",
					DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css",
					DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js",
					DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js",
					DIR_SCRIPTS."qtip/qtip.css",
					DIR_SCRIPTS."qtip/qtip.js",
					DIR_JS."form.validator.file.js"
				));

		}
		
	}