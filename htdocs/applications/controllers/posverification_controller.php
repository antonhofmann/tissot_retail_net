<?php 

	class POSverification_controller {
		
		public function __construct() {
		
			$this->request = request::instance();
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');
			
			Compiler::attach(Theme::$Default);
		}
		
		public function index() {

			$application = $this->request->application;
				
			// permission
			$permission_view = user::permission(Pos_Verification::PERMISSION_VIEW_ALL);
			$permission_edit = user::permission(Pos_Verification::PERMISSION_EDIT_ALL);
			$permission_view_limited = user::permission(Pos_Verification::PERMISSION_VIEW_LIMITED);
			$permission_edit_limited = user::permission(Pos_Verification::PERMISSION_EDIT_LIMITED);
			
			// exclude data view for company list
			$this->request->exclude("$application/posverification/data");
		
			// if user is client go to data entry view
			if (!$permission_view && !$permission_edit) {
				url::redirect($this->request->query('data'));
			}
		
			// button: edit company
			$link = $this->request->query('data');
			$this->request->field('form', $link);

			// list view
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'list.php');
			$tpl->data('url', '/applications/modules/pos/verification/data/list.php');
			$tpl->data('class', 'list-1000');
			$this->view->setTemplate('verifications', $tpl);

			Compiler::attach(array(
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js'
			));
		}
		
		public function data() { 
			
			$id = url::param();
			$period = url::param(1);
			$application = $this->request->application;
			$user = User::instance();

			$model = new Model($application);
			
			// permission
			$permission_view = user::permission(Pos_Verification::PERMISSION_VIEW_ALL);
			$permission_edit = user::permission(Pos_Verification::PERMISSION_EDIT_ALL);
			$permission_view_limited = user::permission(Pos_Verification::PERMISSION_VIEW_LIMITED);
			$permission_edit_limited = user::permission(Pos_Verification::PERMISSION_EDIT_LIMITED);
			
			// set company id for clients
			$id = ($id) ? $id : $user->address;
			
			$company = new Company();
			$company->read($id);
			
			$country = new Country();
			$country->read($company->country);
			
			$this->view->pagetitle = $this->request->title.": ".$company->company.', '.$country->name;

			if (!$permission_view && !$permission_edit) {
				if ($user->address <> $company->id) {
					url::redirect("/messages/show/access_denied");
				}
			}


			// period ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
			$d = new DateTime(date('Y-m-d'));
			
			// current month
			$d->modify('first day of this month');
			$currentMonthTimeStamp = $d->format('U'); 
			
			// last month
			$d->modify('first day of last month');
			$lastMonthTimeStamp = $d->format('U');
		
			// next month
			$d->modify('first day of next month'); 
			$d->modify('first day of next month');
			$nextMonthTimeStamp = $d->format('U');

			// periode
			$period = ($period) ? $period : $currentMonthTimeStamp;
			$periodDate = date('Y-m-d', $period);
			
			// request vars
			$this->request->field('period', $period);
			$this->request->field('url', $this->request->query("data/$id"));
			

			// get verification ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
			$posVerification = new Pos_Verification($application);
			$data = $posVerification->read_periode($id, $periodDate);

			$_VERIFICATION_CONFIRMED = $data['posverification_confirm_date'] ? true : false;
			$_POS_VERIFICATION_DELETED = $data['posverification_deleted'];
			
			// copy data from previous month to current month
			if (!$data['posverification_id']) {
				$data = $posVerification->setFromPreviousMonth($id, $period);
			}
			

			// dropdown periodes :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
			$result = $model->query("
				SELECT 
					UNIX_TIMESTAMP(posverification_date) AS date,
					DATE_FORMAT(posverification_date, '%M %Y') AS caption
				FROM posverifications
				INNER JOIN posverification_data ON posverification_data_posverification_id = posverification_id
				WHERE posverification_address_id = $id 
				ORDER BY posverification_date DESC		
			")->fetchAll();
			
			$periodes = _array::extract($result);
			$periodes[$currentMonthTimeStamp] = date('F Y');
			$periodes[$lastMonthTimeStamp] = date('F Y', $lastMonthTimeStamp);	
			//$periodes[$nextMonthTimeStamp] = date('F Y', $nextMonthTimeStamp);	
			krsort($periodes);	
			$dataloader['periodes'] = $periodes;


			// build datagrid ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
			$datagrid = array();
			$permittedDistChannels = array();

			$result = $model->query("
				SELECT DISTINCT
					mps_distchannel_id,
					mps_distchannel_groups.mps_distchannel_group_id,
					mps_distchannel_group_name,
					mps_distchannel_group_allow_for_agents,
					mps_distchannel_code,
					mps_distchannel_name
				FROM db_retailnet.mps_distchannels
				INNER JOIN db_retailnet.mps_distchannel_groups ON mps_distchannels.mps_distchannel_group_id = mps_distchannel_groups.mps_distchannel_group_id
				WHERE mps_distchannel_active = 1
				ORDER BY mps_distchannel_group_name, mps_distchannel_name
			")->fetchAll();
			
			if ($result) {
				
				foreach ($result as $row) {
					
					$permitted = true;
					
					// corporate pos allowed for agents
					if ($company->client_type==1 && $row['mps_distchannel_group_id']==1) {
						$permitted = $row['mps_distchannel_group_allow_for_agents'];
					}

					if ($permitted) {

						$key = strtolower(str_replace(' ', '', $row['mps_distchannel_group_name']));
						
						$i = $row['mps_distchannel_id'];
						$delta = $dataloader['posverification_data_actual_number'][$i] - $dataloader['in_pos_index'][$i];

						$permittedDistChannels[$i] = true;
						
						$datagrid[$key]['caption'] = $row['mps_distchannel_group_name'];
						
						$datagrid[$key]['data'][$row['mps_distchannel_id']] = array(
							'mps_distchannel_name' => $row['mps_distchannel_name'],
							'mps_distchannel_code' => $row['mps_distchannel_code'],
							'in_pos_index' => null,
							'posverification_data_actual_number' => null,
							'delta' => $delta
						);
					}
				}
			}
			

			// get verification data :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			if ($data['posverification_id']) {
				
				$result = $model->query("
					SELECT 
						posverification_data_distribution_channel_id,
						posverification_data_actual_number,
						posverification_data_in_posindex
					FROM posverification_data
					INNER JOIN db_retailnet.mps_distchannels ON mps_distchannel_id = posverification_data_distribution_channel_id
					WHERE posverification_data_posverification_id = ".$data['posverification_id']." AND mps_distchannel_active = 1
				")->fetchAll();
				
				if ($result) {
					
					foreach ($result as $row) {

						$channel = $row['posverification_data_distribution_channel_id'];

						if ($permittedDistChannels[$channel]) {
							
							$dataloader['posverification_data_actual_number'][$channel] = $row['posverification_data_actual_number'];
							
							if ( $row['posverification_data_in_posindex'] > 0 && ($_VERIFICATION_CONFIRMED || $currentMonthTimeStamp<>$period) ) {
								$dataloader['in_pos_index'][$channel] = $row['posverification_data_in_posindex'];
								
							}
						}
					}
				}
			}

			$data['pos_verification_deleted'] = $_POS_VERIFICATION_DELETED;
			
			
			// totals for confirmed  verifications :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
			if ($_VERIFICATION_CONFIRMED) { 

				// total pos locations without distirbution channels
				$data['pos-whithout-channels'] = $data['posverification_whithout_channels'];
				
				//echo "<pre>"; print_r($dataloader['in_pos_index']); die;
				// total pos locations
				$data['pos-total'] = $dataloader['in_pos_index'] 
					? array_sum(array_values($dataloader['in_pos_index'])) + $data['posverification_whithout_channels'] 
					: $data['posverification_whithout_channels'];

				// confirmed date
				$date = ($data['date_modiefied']) ? $data['date_modiefied'] : $data['posverification_confirm_date'];
				$d = new DateTime($date);
				$confirmedDate = $d->format("d.m.Y H:i:s");
				
				// conirmer
				$user = new User($data['posverification_confirm_user_id']);
				$data['submitted'] = "Submitted by $user->firstname $user->name, $confirmedDate";
			} 


			// totals for unconfirmed verifications ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
			$filterApplication = '';

			if (!$_VERIFICATION_CONFIRMED && $currentMonthTimeStamp==$period) { 
				
				$result = $model->query("
					SELECT
						posaddress_distribution_channel,
						COUNT(posaddress_id) as total
					FROM db_retailnet.posaddresses
					LEFT JOIN db_retailnet.addresses AS addresses ON addresses.address_id = posaddress_client_id
					LEFT JOIN db_retailnet.addresses AS parentaddresses ON parentaddresses.address_id = posaddress_franchisee_id
					LEFT JOIN db_retailnet.mps_distchannels ON mps_distchannel_id = posaddress_distribution_channel
					WHERE (
						posaddress_distribution_channel > 0 
						AND mps_distchannel_active = 1
						AND posaddress_client_id = $id
						AND addresses.address_active = 1 
						AND addresses.address_type = 1
						AND (
							addresses.address_involved_in_planning = 1 
							OR addresses.address_involved_in_planning_ff = 1
							
						)
						AND (
							posaddress_store_closingdate IS NULL
							OR posaddress_store_closingdate = '0000-00-00'
							OR posaddress_store_closingdate = ''
						)
						$filterApplication
					)
					OR (
						posaddress_distribution_channel > 0 
						AND mps_distchannel_active = 1
						AND parentaddresses.address_parent = $id
						AND parentaddresses.address_active = 1 
						AND parentaddresses.address_type = 1
						AND addresses.address_client_type = 3
						AND (
							posaddress_store_closingdate IS NULL
							OR posaddress_store_closingdate = '0000-00-00'
							OR posaddress_store_closingdate = ''
						)
						$filterApplication
					)
					GROUP BY posaddress_distribution_channel	
				")->fetchAll();
				
				// in pos index for each distribution channel
				$dataloader['in_pos_index'] = _array::extract($result); 

				// count all pos locations for current company
				$result = $model->query("
					SELECT 
						COUNT(DISTINCT posaddress_id) as total
					FROM db_retailnet.posaddresses
					LEFT JOIN db_retailnet.addresses as addresses ON addresses.address_id = posaddress_client_id
					LEFT JOIN db_retailnet.addresses as parentaddresses ON parentaddresses.address_id = posaddress_franchisee_id
					WHERE (
						posaddress_client_id = $id
						AND addresses.address_active = 1 
						AND addresses.address_type = 1
						AND (
							addresses.address_involved_in_planning = 1 
							OR addresses.address_involved_in_planning_ff = 1
							
						)
						AND (
							posaddress_store_closingdate IS NULL 
							OR posaddress_store_closingdate = '0000-00-00'
							OR posaddress_store_closingdate = ''
						)
						$filterApplication
					)
					OR (
						parentaddresses.address_parent = $id
						AND parentaddresses.address_active = 1 
						AND parentaddresses.address_type = 1
						AND addresses.address_client_type = 3
						AND (
							posaddress_store_closingdate IS NULL
							OR posaddress_store_closingdate = '0000-00-00'
							OR posaddress_store_closingdate = ''
						)
						$filterApplication
					)
				")->fetch();

				// total pos locations
				$data['pos-total'] = $result['total'];
				
				// total pos locations without channels
				$data['pos-whithout-channels'] = $dataloader['in_pos_index'] 
					? $data['pos-total'] - array_sum(array_values($dataloader['in_pos_index'])) 
					: $data['pos-total'];
			}


			// get pos index data from passt
			// if verfication is not confirmed
			if (!$_VERIFICATION_CONFIRMED && $currentMonthTimeStamp<>$period) {

				$periodYear = date('Y', $period);
				$periodMonth = date('m', $period);
				
				$result = $model->query("
					SELECT 
					    mps_distchannels.mps_distchannel_id,
					    COUNT(posaddresses.posaddress_id) AS total
					FROM db_retailnet.posaddresses
					LEFT JOIN db_retailnet.mps_distchannels ON mps_distchannels.mps_distchannel_id = posaddresses.posaddress_distribution_channel
					LEFT JOIN db_retailnet.countries ON posaddresses.posaddress_country = country_id
					LEFT JOIN db_retailnet.addresses ON posaddresses.posaddress_client_id = addresses.address_id
					WHERE
					addresses.address_id = $id
					AND (
						YEAR(posaddresses.posaddress_store_openingdate) < $periodYear
						OR ( 
							YEAR(posaddresses.posaddress_store_openingdate) = $periodYear 
							AND MONTH(posaddresses.posaddress_store_openingdate) <= $periodMonth
						)
					)
					AND (
						posaddresses.posaddress_store_closingdate IS NULL
						OR posaddresses.posaddress_store_closingdate = '000-00-00'
			            OR YEAR(posaddresses.posaddress_store_closingdate) > $periodYear
			            OR (
			            	YEAR(posaddresses.posaddress_store_closingdate) = $periodYear
							AND MONTH(posaddresses.posaddress_store_closingdate) > $periodMonth
						)
			        )
					$filterApplication
					GROUP BY addresses.address_id , mps_distchannels.mps_distchannel_id
					ORDER BY countries.country_name , addresses.address_company , mps_distchannels.mps_distchannel_id , mps_distchannels.mps_distchannel_name , mps_distchannels.mps_distchannel_code
				")->fetchAll();
				
				if ($result) {
					
					foreach ($result as $row) {
						
						$channel = $row['mps_distchannel_id'];

						if ($channel) {
							$dataloader['in_pos_index'][$channel] = $row['total'];
							$data['pos-total'] += $row['total'];
						} else $data['pos-whithout-channels'] += $row['total'];
					}
				}

				//$dataloader['in_pos_index'] = _array::extract($result);
			}
			
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['archived'] = $this->request->archived;
			$data['action'] = $this->request->action;


			// buttons :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
			$buttons = array();

			// back button
			$buttons['back'] = ui::button(array(
				'id' => 'back',
				'icon' => 'back',
				'href' => $this->request->query(),
				'caption' => translate::instance()->back
			));
				
			// button: print
			$buttons['print'] = ui::button(array(
				'id' => 'print',
				'href' => '/applications/modules/pos/verification/data/print.php',
				'icon' => 'print',
				'caption' => translate::instance()->print,
				'target' => '_blank'
			));
			
			if ($permission_edit || $permission_edit_limited) {
				
				$regionalAccessCompanies = User::getRegionalAccessCompanies();
				// confirm button
				if (!$data['posverification_confirm_date']) {
					$buttons['confirm_verification'] = ui::button(array(
						'id' => 'confirm_verification',
						'href' => '/applications/modules/pos/verification/data/confirm.php',
						'icon' => 'save',
						'class' => 'dialog action confirm',
						'caption' => translate::instance()->confirm_verification
					));
				} 
				// uncofirmed button
				elseif ($permission_edit or in_array($company->id, $regionalAccessCompanies)) {
					$buttons['delete_confirmation'] = ui::button(array(
						'id' => 'delete_confirmation',
						'href' => '/applications/modules/pos/verification/data/reset.php',
						'icon' => 'delete',
						'class' => 'dialog action',
						'caption' => translate::instance()->delete_confirmation
					));
				}
			}


			// output ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'pos/data/data.php');
			$tpl->data('id', $id);
			$tpl->data('client', $client);
			$tpl->data('period', $period);
			$tpl->data('data', $data);
			$tpl->data('dataloader', $dataloader);
			$tpl->data('datagrid', $datagrid);
			$tpl->data('buttons', $buttons);
			$this->view->setTemplate('verification', $tpl);

			Compiler::attach(array(
				'/public/css/pos.verification.data.entry.css',
				"/public/scripts/dropdown/dropdown.css",
				"/public/scripts/dropdown/dropdown.js",
				"/public/scripts/table.loader.js",
				'/public/js/pos.verification.js'
			));	
		}
		
		public function consolidated() {

			$application = $this->request->application;
			
			// permission
			$permission_view = user::permission(Pos_Verification::PERMISSION_VIEW_ALL);
			$permission_edit = user::permission(Pos_Verification::PERMISSION_EDIT_ALL);
			$permission_view_limited = user::permission(Pos_Verification::PERMISSION_VIEW_LIMITED);
			$permission_edit_limited = user::permission(Pos_Verification::PERMISSION_EDIT_LIMITED);
			
			// no access to consolidated views
			if (!$permission_view && !$permission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			$keyword = $this->request->controller.'.'.$this->request->archived.'.'.$this->request->action;
			$_REQUEST = session::filter($this->request->application, $keyword);
			
			// calendar vars
			$d = new DateTime(date('Y-m-d'));
				
			// current month
			$d->modify('first day of this month');
			$currentMonthTimeStamp = $d->format('U');
			
			// request vars
			$period = $_REQUEST['periodes'];
			$region = $_REQUEST['regions'];
			$state = $_REQUEST['states'];
			$miscellaneous = $_REQUEST['miscellaneous'];
			
			// periode
			$period = ($period) ? $period : $currentMonthTimeStamp;
			
			$model = new Model($application);
			
			// dropdown: period
			$result = $model->query("
				SELECT 
					UNIX_TIMESTAMP(posverification_date) AS date,
					DATE_FORMAT(posverification_date, '%M %Y') AS caption
				FROM posverifications
				ORDER BY posverification_date DESC		
			")->fetchAll();
			
			$periodes = _array::extract($result);
			$periodes[$currentMonthTimeStamp] = date('F Y');
			krsort($periodes);	
			$dataloader['periodes'] = $periodes;
			
			// dataloader: regions
			$result = $model->query("
				SELECT DISTINCT
					region_id,
					region_name
				FROM db_retailnet.addresses
				INNER JOIN db_retailnet.countries ON country_id = address_country
				INNER JOIN db_retailnet.regions ON region_id = country_region	
				WHERE address_type = 1 AND address_active = 1 AND (address_involved_in_planning = 1 or address_involved_in_planning_ff = 1) 	
				ORDER BY region_name
			")->fetchAll();
			
			$dataloader['regions'] = _array::extract($result);
			
			// dataloader states
			$dataloader['states'] = array(
				1 => 'Completed',
				2 => 'Uncompleted'
			);
			
			// period is not defined
			// set last foundet peroid
			if (!$period) { 
				reset($periodes);
				$period = key($periodes);
			}
			
			
			// client type: agents, subsidiaries, affiliates
			$result = $model->query("
				SELECT DISTINCT
					client_type_id,
					client_type_code
				FROM db_retailnet.addresses
				INNER JOIN db_retailnet.client_types ON client_type_id = address_client_type
				WHERE address_type = 1 AND address_active = 1 AND address_client_type IN(1, 2, 3)
				AND (
					address_involved_in_planning = 1
					OR address_involved_in_planning_ff = 1
				)
			")->fetchAll();
			
			$dataloader['addresstypes'] = _array::extract($result);


			// dataloader: regions
			$result = $model->query("
				SELECT DISTINCT
					address_id,
					CONCAT(country_name, ', ', address_company) AS client
				FROM db_retailnet.addresses
				INNER JOIN db_retailnet.countries ON country_id = address_country
				WHERE address_type = 1 AND address_active = 1 AND (address_involved_in_planning = 1 or address_involved_in_planning_ff = 1) 	
				ORDER BY client
			")->fetchAll();
			
			$dataloader['clients'] = _array::extract($result);
			
			
			// request vars
			$this->request->field('url', $this->request->query("consolidated/"));
			
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['archived'] = $this->request->archived;
			$data['action'] = $this->request->action;
			
			$data['addresstype'] = $_REQUEST['addresstypes'];
			$data['legaltype'] = $_REQUEST['legaltype'];
			
			$buttons = array();
				
			// button: print
			$buttons['print'] = ui::button(array(
				'id' => 'print',
				'class' => 'print',
				'icon' => 'print',
				'href' => $this->request->link('/applications/modules/pos/verification/consolidation/print.php'),
				'label' => translate::instance()->print
			));
				
			// button: pdf
			$buttons['pdf'] = ui::button(array(
				'id' => 'pdf',
				'class' => 'print',
				'icon' => 'icon55',
				'href' => $this->request->link('/applications/modules/pos/verification/consolidation/pdf.php'),
				'label' => 'Print PDF'
			));
			
			// has confirmed data
			$model->query("
				SELECT COUNT(posverification_id) as total
				FROM posverifications
			");
			
			$model->filter('period', "UNIX_TIMESTAMP(posverification_date) = '$period'");
			$model->filter('confirmed', "(posverification_confirm_date IS NOT NULL OR posverification_confirm_date != '')");
			
			if ($region) {
				$model->bind(Pos_Verification::DB_BIND_COMPANIES);
				$model->bind(Company::DB_BIND_COUNTRIES);
				$model->filter('region', "country_region = $region");
			}
			
			$result = $model->fetch();
			$totalConfirmed = $result['total'];

			$buttons['request_verification'] = ui::button(array(
				'id' => 'request_verification',
				'icon' => 'mail',
				'href' => $this->request->query('request'),
				'label' => "POS Verification Request"
			));
			
			// button: print
			if ($totalConfirmed) {
				$buttons['reset_verification'] = ui::button(array(
					'id' => 'reset_verification',
					'icon' => 'reload',
					'class' => 'dialog',
					'data-target' => '#reset_verification_dialog',
					'href' => '/applications/modules/pos/verification/consolidation/reset.php',
					'label' => translate::instance()->reset_verification
				));
			}
			
			// count all clients
			$model->query("SELECT COUNT(address_id) as total FROM db_retailnet.addresses")
			->bind(Company::DB_BIND_COUNTRIES)
			->filter('default', "address_type = 1 AND address_active = 1");
			
			if ($region) {
				$model->filter('region', "country_region = $region");
			}
			
			$result = $model->fetch();
			
			// button remind
			if ($result['total'] > $totalConfirmed) {
				$buttons['remind'] = ui::button(array(
					'id' => 'remind',
					'icon' => 'mail',
					'href' => $this->request->query('reminder'),
					'label' => translate::instance()->remind
				));
			}

			// spreadsheet view
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'pos/consolidation/spreadsheet.php');
			$tpl->data('period', $period);
			$tpl->data('region', $region);
			$tpl->data('state', $state);
			$tpl->data('miscellaneous', $miscellaneous);
			$tpl->data('data', $data);
			$tpl->data('dataloader', $dataloader);
			$tpl->data('buttons', $buttons);
			$this->view->setTemplate('verifications', $tpl);

			Compiler::attach(array(
				'/public/scripts/spreadsheet/spreadsheet.css',
				'/public/css/pos.verification.spreadsheet.css',
				'/public/scripts/spreadsheet/spreadsheet.js',
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js',
				'/public/js/pos.verification.consolidation.js'
			));
		}

		public function reminder() {

			// get request from session filters
			$application = $this->request->application;
			$controller = $this->request->controller;
			$archived = $this->request->archived;
			$_REQUEST = session::filter($application, "$controller.$archived.consolidated", false);

			// request fields
			$this->request->field('periodes', $_REQUEST['periodes']);
			$this->request->field('regions', $_REQUEST['regions']);
			$this->request->field('states', $_REQUEST['states']);
			$this->request->field('miscellaneous', $_REQUEST['miscellaneous']);
			$this->request->field('selected', '');

			$buttons = array();

			// button back
			$buttons['back'] = ui::button(array(
				'id' => 'back',
				'icon' => 'back',
				'label' => translate::instance()->back,
				'href' => $this->request->query('consolidated')
			));

			// button reminder
			$buttons['remider'] = ui::button(array(
				'id' => 'remind',
				'class' => 'submit-list sendmail',
				'icon' => 'save',
				'label' => translate::instance()->remind
			));

			// mail template
			$tpl = 14;
			$mail = new Mail_Template();
			$maildata = $mail->read($tpl);
			$maildata['mail_template_text'] = nl2br($maildata['mail_template_text']);

			// render
			$render = array(
				'sender_firstname' => User::instance()->firstname,
				'sender_name' => User::instance()->name,
				'periode' => $dataloader['periodes'][$period]
			);

			$maildata['mail_template_subject'] = Content::render($maildata['mail_template_subject'], $render);
			$maildata['mail_template_text'] = Content::render($maildata['mail_template_text'], $render);
			$maildata['application'] = $application;

			$modal = array();
			$modal['title'] = "POS Verification Reminder";
			$modal['subtitle'] = "Send E-mails to all not confirmed companies";

			// reminder list
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'pos/consolidation/reminder.php');
			$tpl->data('data', $data);
			$tpl->data('modal', $modal);
			$tpl->data('maildata', $maildata);
			$tpl->data('buttons', $buttons);
			$this->view->setTemplate('reminder', $tpl);

			Compiler::attach(array(
				'/public/scripts/table.loader.js',
				'/public/js/pos.verification.remider.js'
			));
		}

		public function request() {

			// get request from session filters
			$application = $this->request->application;
			$controller = $this->request->controller;
			$archived = $this->request->archived;
			$_REQUEST = session::filter($application, "$controller.$archived.consolidated", false);

			// request fields
			$this->request->field('periodes', $_REQUEST['periodes']);
			$this->request->field('regions', $_REQUEST['regions']);
			$this->request->field('states', $_REQUEST['states']);
			$this->request->field('miscellaneous', $_REQUEST['miscellaneous']);
			$this->request->field('selected', '');
			$this->request->field('states', 2);

			$buttons = array();

			// button back
			$buttons['back'] = ui::button(array(
				'id' => 'back',
				'icon' => 'back',
				'label' => translate::instance()->back,
				'href' => $this->request->query('consolidated')
			));

			// button reminder
			$buttons['remider'] = ui::button(array(
				'id' => 'remind',
				'class' => 'submit-list sendmail',
				'icon' => 'save',
				'label' => "Send Request"
			));

			// mail template
			$tpl = 42;
			$mail = new Mail_Template();
			$maildata = $mail->read($tpl);
			$maildata['mail_template_text'] = nl2br($maildata['mail_template_text']);

			// render
			$render = array(
				'sender_firstname' => User::instance()->firstname,
				'sender_name' => User::instance()->name,
				'periode' => $dataloader['periodes'][$period]
			);

			$maildata['mail_template_subject'] = Content::render($maildata['mail_template_subject'], $render);
			$maildata['mail_template_text'] = Content::render($maildata['mail_template_text'], $render);
			$maildata['application'] = $application;

			$modal = array();
			$modal['title'] = "POS Verification Request";

			// reminder list
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'pos/consolidation/reminder.php');
			$tpl->data('data', $data);
			$tpl->data('modal', $modal);
			$tpl->data('maildata', $maildata);
			$tpl->data('buttons', $buttons);
			$this->view->setTemplate('reminder', $tpl);

			Compiler::attach(array(
				'/public/scripts/table.loader.js',
				'/public/js/pos.verification.remider.js'
			));
		}
	}