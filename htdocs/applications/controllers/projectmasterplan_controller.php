<?php 

/**
 * Controller for project masterlans
 */
class Projectmasterplan_Controller {
  /**
   * @var translate
   */
  protected $Translate;
  
  /**
   * Setup controller
   */
  public function __construct() {
    if (!user::permission('can_view_project_masterplan')) {
      url::redirect("/messages/show/access_denied");
    }

    $this->user = User::instance();

	//check if preferences in table user_preferences is set, if not add record
	$prefs = $this->user->getPreferences('project.masterplan', 'excluded_phases');
	if(is_array($prefs) and !array_key_exists('project.masterplan', $prefs))
	{
		$success = $this->user->addPreference('project.masterplan', 'excluded_phases', '');
	}

	$this->request = request::instance();
    
    $this->view = new View();
    $this->view->pagetitle = $this->request->title;
    $this->view->usermenu('usermenu')->user('menu');
    $this->view->categories('pageleft')->navigation('tree');

    $this->Translate = translate::instance();

    // this page uses modular js
    $this->view->amd = true;

    Compiler::attach(Theme::$Default);
  }
  
  /**
   * Render table with all step dates for a project
   * @param  string ProjectID or project number
   * @return void
   */
  public function data($input) {
    if (!$input) {
		$input = url::param(); 
	}
	
	$projectID = $this->getProjectId($input);
    $this->view->pagetitle = $this->getPageTitle($projectID);

    $PmMapper = new Project_Masterplan_Datamapper();
    $masterplan = $PmMapper->getMasterplanDataTree($projectID);
    $responsibles = $PmMapper->getResponsibles($projectID);
    // add an array of steps without a responsible role
    $responsibles[0] = array();

    // tab based navigation
    $this->view->tabs('pagecontent')
      ->navigation('tab');

    // get project values (= dates)
    $ValueMapper = new Project_Masterplan_Values_Datamapper();
    $projectValues = $ValueMapper->loadProjectData($projectID);

    // calculate durations according to project values
    $projectSteps = $PmMapper->getProjectStepIterator($projectID);
    $projectDurations = $this->calculateDurations($projectSteps, $projectValues);

    // set data in view
    $this->view->projectmasterplanTemplates('pagecontent')
      ->project('masterplan')
      ->data('phases', $masterplan['phases'])
      ->data('values', $projectValues)
      ->data('projectID', $projectID)
      ->data('durations', $projectDurations)
      ->data('responsibles', $responsibles)
      ->data('translate', translate::instance());
    Compiler::attach(DIR_CSS . 'popup.css');
    Compiler::attach(DIR_SCRIPTS . 'spinner/spinner.css');
    $this->view->master('popup.inc');
  }

  /**
   * Render gantt diagram
   * @param  string ProjectID or project number
   * @return void
   */
  public function gantt($input) {

	if (!$input) {
		$input = url::param(); 
	}
    $projectID = $this->getProjectId($input);
    $this->view->pagetitle = $this->getPageTitle($projectID);
    $steps = $this->assembleGanttData($projectID);

    // tab based navigation
    $this->view->tabs('pagecontent')
      ->navigation('tab');

    // set data in view
    $this->view->projectmasterplanTemplates('pagecontent')
      ->project('masterplan.gantt')
      ->data('steps', json_encode($steps))
      ->data('ganttModule', 'apps/admin/views/project_masterplan/gantt')
      ->data('page', 'project-gantt')
      ->data('projectID', $projectID)
      ->data('translate', translate::instance());
    Compiler::attach(DIR_CSS . 'popup.css');
    $this->view->master('popup.inc');
  }

  /**
   * Render settings tab of project masterplan
   * @return void
   */
  public function settings() {
    $this->view->amd = true;
    // tab based navigation
    $this->view->tabs('pagecontent')
      ->navigation('tab');

    // get steps
    $PmMapper = new Project_Masterplan_Datamapper();

    $User = new User;
    $prefs = $User->getPreferences('project.masterplan');
    $excluded = array();
    if (isset($prefs['excluded_phases'])) {
      $excluded = explode(',', $prefs['excluded_phases']);
    }

    // render view
    $this->view->projectmasterplanTemplates('pagecontent')
      ->project('masterplan.settings')
      ->data('translate', translate::instance())
      ->data('phases', $PmMapper->getSubPhases(false))
      ->data('excluded', json_encode($excluded));
    Compiler::attach(DIR_CSS . 'popup.css');
    Compiler::attach(DIR_SCRIPTS . '/jgrowl/jgrowl.css');
    $this->view->master('popup.inc');
  }

  /**
   * Save a setting
   * This action is called via ajax 
   * @return void
   */
  public function save_settings() {
    if (!isset($_POST) || !isset($_POST['exclude'])) {
      exit;
    }

    // clean data, make sure it's unique
    $exclude = array();
    foreach ($_POST['exclude'] as $phase) {
      $phaseID = intval($phase);
      if ($phaseID > 0 && !in_array($phaseID, $exclude)) {
        $exclude[] = $phaseID;
      }
    }
    
    $User = new User;
    $success = $User->updatePreference('project.masterplan', 'excluded_phases', implode(',', $exclude));

    $Translate = translate::instance();
    print json_encode(array(
    'content' => $success ? $Translate->message_request_saved : $Translate->message_request_failure,
    'properties' => array(
      'theme' => $success ? 'message' : 'error'
    )));
    exit;
  }

  /**
   * Export PDF sent from masterplan component via POST
   * @return void
   */
  public function export() {
    if (!isset($_POST['base64']) || !isset($_POST['fileName'])) {
      url::header(400);
      die('Request is incomplete');
    }

    $this->Translate = translate::instance();

    // projectworkflow masterplan -> use query name in title
    if (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'projectworkflow') !== false) {
      $queryID = intval($_POST['fileName']);
      $Query = new MIS_Query();
      $Query->read($queryID);

      $filename  = $this->Translate->project_masterplan_filename(array(
        'identifier' => $Query->name, 
        'date' => date('YmdHi')
        ));
      $pageTitle = $this->Translate->masterplan_query_title(array('query_name' => $Query->name));
    }
    // project masterplan -> use project number in title
    else {
      $projectID = intval($_POST['fileName']);
      $Project = new Project();
      $Project->read($projectID);
      
      $filename  = $this->Translate->project_masterplan_filename(array(
        'identifier' => $Project->number, 
        'date' => date('YmdHi')
        ));
      $pageTitle = $this->getPageTitle($projectID);
    }

    $orientation = isset($_REQUEST['orientation']) ? $_REQUEST['orientation'] : 'landscape';

    try {
      // decode and temporarily save file
      $filepath = tempnam(PATH_DATA . '/files', 'pm_gantt_');
      if ($filepath === false) {
        throw new Exception('Could not save PDF');
      }
      file_put_contents($filepath, base64_decode($_POST['base64']));

      // create branded pdf and send to browser
      $pdf = new Project_Masterplan_PDF($filepath, $orientation);
      $pdf->SetTitle($pageTitle);
      $pdf->Output($filename, 'D');

      // remove temp file
      unlink($filepath);
      exit;
    }
    catch (Exception $e) {
      url::header(500);
      die('Could not save PDF');
    }
  }

  /**
   * Assemble masterplan data for gantt diagram
   * @param  integer $projectID
   * @return array
   */
  protected function assembleGanttData($projectID) {
    // get masterplan
    $PmMapper = new Project_Masterplan_Datamapper();
    $projectSteps = $PmMapper->getProjectStepIterator($projectID);

    // get values
    $ValueMapper = new Project_Masterplan_Values_Datamapper();
    $projectValues = $ValueMapper->loadProjectData($projectID);

    // calculate durations of steps
    $durations = $this->calculateDurations($projectSteps, $projectValues);

    $Project = new Project();
    $Project->read($projectID);
    $country = $Project->getCountry();
    $salesRegion = $Project->getSalesregion();
    $defaultDurations = $PmMapper->getAllStepDurationsForPlace($salesRegion['id'], $country->id);

    $steps = array();
    $i = 0;
    $previousDate = null;

    foreach ($projectSteps as $step) {
      // we don't want info steps in the gantt chart
      if ($step['step_type'] == 'info') {
        continue;
      }

      // assemble step data
      $steps[$i] = array(
        'id'          => $step['id'],
        'step'        => $step['step_nr'],
        'orderId'     => $i,
        'title'       => $step['title'],
        'color'       => $step['color'],
        'type'        => $step['step_type'],
        'nrOfDays'    => isset($durations[$step['value']]) ? $durations[$step['value']] : null,
        'overdueDays' => 0,
        ); 

      // calculate if and how much the step is overdue
      if (array_key_exists($step['id'], $defaultDurations)) {
        if (array_key_exists('country', $defaultDurations[$step['id']])) {
          $defaultDuration = $defaultDurations[$step['id']]['country'];
        }
        else if (array_key_exists('salesregion', $defaultDurations[$step['id']])) {
          $defaultDuration = $defaultDurations[$step['id']]['salesregion'];
        }
        
        $actualDuration  = $durations[$step['value']];
        if ($actualDuration > $defaultDuration) {
          $percent = 1 / $actualDuration * $defaultDuration;
          $steps[$i-1]['percentComplete'] = round($percent, 2);
          $steps[$i-1]['overdueDays'] = $actualDuration - $defaultDuration;
        }
      }

      // make sure we have a valid step value
      try {
        $startDate = is_null($projectValues[$step['value']])
          ? false
          : new Datetime($projectValues[$step['value']]);
      }
      catch (Exception $e) {
        $startDate = false;
      }

      if ($startDate instanceof Datetime) {
        // set date as startdate
        $steps[$i]['start'] = $startDate->format('Y/m/d');

        if ($step['step_type'] == 'duration') {
          // if previous step has a startdate and current start date is > previous
          // start date, set current startdate as enddate of previous step
          if ($i > 0
              && isset($steps[$i-1]['start'])
              && $steps[$i]['start'] > $steps[$i-1]['start']
             ) {
            $steps[$i-1]['end'] = $steps[$i]['start'];
          }
        }

        $previousDate = $startDate;
      }
      $i++;
    }
    return $steps;
  }

  /**
   * Calculate durations of all masterplan steps according to values and order of steps
   * @param Iterator $steps Step iterator
   * @param array $values step values
   * @return array Array with step value as keys and duration as values
   */
  protected function calculateDurations($steps, $values) {
    $durations = array();
    $previousDate = null;

    foreach ($steps as $step) {
      // filter out invalid dates
      if (is_null($values[$step['value']])) {
        continue;
      }
      try {
        $currentDate = new Datetime($values[$step['value']]);
      }
      catch (Exception $e) {
        continue;
      }

      // get interval between dates and save it if it's > 0
      if (!is_null($previousDate)) {
        $interval = $previousDate->diff($currentDate);
        $diff = (int) $interval->format('%R%a');
        if ($diff > 0) {
          $durations[$step['value']] = $diff;
        }
      }

      $previousDate = $currentDate;
    }

    return $durations;
  }

  /**
   * Get the page title for the actions of this controller
   * @return string
   */
  protected function getPageTitle($projectID) {
    $Project = new Project();
    $Project->read($projectID);
    return $this->Translate->project_masterplan_page_title(array(
      'project_number' => $Project->number, 
      'address' => $Project->getPOSAddress()
      ));
  }

  /**
   * Get project id from an input that is either project id or project number
   * @param  string $input
   * @return int ProjectID
   */
  protected function getProjectId($input) {
    if (strpos($input, '.')) {
      $Project = new Project;
      $data = $Project->readByProjectNumber($input);
      return (int) $data['project_id'];
    }
    else {
      return (int) $input;
    }
  }
}
