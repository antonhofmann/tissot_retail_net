<?php 

class Users_Controller {
	
	
	public function __construct() {
			
		$this->user = User::instance();
		$this->request = request::instance();
		$this->translate = Translate::instance();
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}
	
	public function index() {

		// for old request
		if (Request::param()=='users') {
			$company = Request::param(1);
			$user = Request::param(2);
			$action = $user ? 'user' : 'company';
			url::redirect($this->request->query("$action/$company/$user"));
		}
		
		// link to form
		$linkForm = $this->request->query('company');
		$this->request->field('form', $linkForm);
		
		// link to export
		$linkPrint = $this->request->link('/applications/exports/excel/companies.php', array('address_involved_in_planning'=>1));
		$this->request->field('export', $linkPrint);
		
		// list filter
		$this->request->field('filter', '(address_involved_in_planning=1 or address_is_a_hq_address = 1)');

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_VIEWS.'company.list.inc');
		$this->view->setTemplate('companList', $tpl);

		Compiler::attach(array(
			DIR_SCRIPTS."dropdown/dropdown.css",
			DIR_SCRIPTS."dropdown/dropdown.js",
			DIR_SCRIPTS."table.loader.js",
			DIR_SCRIPTS."loader/loader.css",
			DIR_SCRIPTS."loader/loader.js"
		));
	}
	
	public function company() {
			
		$_COMPANY = url::param();
		
		$company = new Company();
		$company->read($_COMPANY);
	
		// check acces
		if (!$company->access()) {
			message::access_denied();
			url::redirect($this->request->query());
		}
		
		// template: header
		$this->view->header('pagecontent')->node('header')->data('title', $company->header());
		$this->view->tabs('pagecontent')->navigation('tab');

		// button
		$buttons = array();
		
		// button: back
		$buttons['back'] = $this->request->query();
		
		// request form: company
		$this->request->field('id', $_COMPANY); 
		
		// request form: add field
		$linkAdd = $this->request->query("user/$_COMPANY/add");
		$this->request->field('add',$linkAdd);
		
		// request form: edit field
		$linkEdit = $this->request->query("user/$_COMPANY");
		$this->request->field('form', $linkEdit);
			
		// get planning applications
		$planningApplications = application::get_all_involved_in_planning();
			
		if ($planningApplications) {
			
			$planning_roles = array();
			
			foreach ($planningApplications as $app) {
				$planning_roles[] = $app['application_shortcut'];
			}
			
			$role_applications = join(',',$planning_roles);
			$this->request->field('role_applications',$role_applications);
		}

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_VIEWS.'user.list.inc');
		$tpl->data('role_applications', $role_applications);
		$tpl->data('buttons', $buttons);
		$tpl->data('id', $_COMPANY);
		$this->view->setTemplate('verifications', $tpl);

		Compiler::attach(array(
			DIR_SCRIPTS."dropdown/dropdown.css",
			DIR_SCRIPTS."dropdown/dropdown.js",
			DIR_SCRIPTS."table.loader.js",
			DIR_SCRIPTS."fancybox/fancybox.css",
			DIR_SCRIPTS."fancybox/fancybox.js",
			DIR_SCRIPTS."loader/loader.css",
			DIR_SCRIPTS."loader/loader.js"
		));
	}

	public function user() {

		$_COMPANY = url::param();

		// user id
		$_USER = url::param(1);
		$_USER = is_numeric($_USER) ? $_USER : null;
		
		$company = new Company();
		$company->read($_COMPANY);
	
		// check acces
		if (!$company->access()) {
			message::access_denied();
			url::redirect($this->request->query());
		}
		
		// permissions
		$permission_edit = user::permission(Company::PERMISSION_EDIT);
		$permission_edit_limited = user::permission(Company::PERMISSION_EDIT_LIMITED);
		$permission_catalog = user::permission(Company::PERMISSION_EDIT_CATALOG);
		$_CAN_EDIT = ($permission_edit || $permission_edit_limited || $permission_catalog) ? true : false;

		$model = new Model(Connector::DB_CORE);
		
		// template: header
		$this->view->header('pagecontent')->node('header')->data('title', $company->header());
		$this->view->tabs('pagecontent')->navigation('tab');
		
		// button
		$buttons = array();

		// button back
		$buttons['back'] = $this->request->query("company/$_COMPANY");

		// button save
		$buttons['save'] = $_CAN_EDIT;
		
		// get planning applications
		$planningApplications = application::get_all_involved_in_planning();
		
		if ($_USER) {
			
			$auth = User::instance();
			$authRoles = User::roles($auth->id);

			$user = new User($_USER);
			$userRoles = User::roles($_USER);

			$data = $user->data;
			$data['roles'] = serialize($userRoles);
			$data['lps_roles'] = $data['roles'];
			$data['news_roles'] = $data['roles'];
			
			if ($_CAN_EDIT) {
				
				$integrity = new Integrity();
				$integrity->set($_USER, 'users');

				// button delete
				if ($integrity->check()) {
					$buttons['delete'] = url::build('/applications/helpers/user.delete.php', array(
						'id' => $_COMPANY,
						'user' => $_USER
					));
				}
				
				// send user login data, modal email form
				if ($user->login && $user->password) {
					
					$buttons['send_login_data'] = true;
						
					$template = new Mail_Template();
					$loginData = $template->read(13);
						
					if ($template->id) { 

						$link = Settings::init()->http_protocol.'://'.$_SERVER['SERVER_NAME'];
						
						$roles = $user->roles($user->id);
							
						$apps = $model->query("
							SELECT DISTINCT navigation_url
							FROM navigations
							INNER JOIN role_navigations ON role_navigation_navigation = navigation_id
							WHERE navigation_active = 1
							AND navigation_parent = 0
							AND navigation_id != 1
							AND role_navigation_role IN (".join(', ', $roles).")
						")->fetchAll();
						
						if ($apps) { 
							foreach ($apps as $row) {
								$app = $row['navigation_url'];
								$permitted_applications .= "\n\n" . "<a href=\"" . $link . "/" . $app ."\">" . $link . "/" . $app ."</a>";
							}
						}
						
						$render = array(
							'recipient_firstname' => $user->firstname,
							'recipient_name' => $user->name,
							'user_name' => $user->login,
							'password' => $user->password,
							'sender_firstname' => $auth->firstname,
							'sender_name' => $auth->name,
							'uri_list_of_modules' => $permitted_applications
						);
						
						// content render
						$template->data['mail_template_subject'] = Content::render($template->subject, $render);
						$template->data['mail_template_text'] = nl2br(Content::render($template->text, $render));
						$template->data['mail_template_footer'] = Content::render($template->footer, $render);
						
						$loginData = $template->data;
						$loginData['user'] = $user->id;

						// set user creator as cc recipient if recipient get login data for the first time
						$res = $model->query("
							SELECT COUNT(mail_tracking_id) AS total
							FROM db_retailnet.mail_trackings
							WHERE mail_tracking_recipient_email = '$user->email' AND mail_tracking_mail_template_id = 13
						")->fetch();

						if (!$res['total']) {
							$createdFrom = $user->data['user_created'];
							$res = $model->query("SELECT * FROM db_retailnet.users WHERE user_login = '$createdFrom' ")->fetch();
							$loginData['login_data_cc'] = $user->email <> $res['user_email'] ? $res['user_email'] : null;
						}
					}
				}
			} 
			
			// disable all from fields
			if(!$_CAN_EDIT && $data) {
				foreach ($data as $field => $value) {
					$disabled[$field] = true;
				}
			}
			
			if ($planningApplications) {
				foreach ($planningApplications as $app) {
					if ($user->application($app['application_shortcut'])) {
						$belong_to_planning_system = true;
						break;
					}
				}
			}
			
			// for planning applications, hide folowing fields
			//if (!$belong_to_planning_system) {
				//$hidden['user_login'] = true;
				//$hidden['user_password'] = true;
			//}

			//do not allow people from MPS to see and edit passwords fro SWATCH AG General
			//they must not have access to admin accounts
			if (!in_array(1, $authRoles)) {
				
				if (in_array(1, $userRoles)) {
					$hidden['user_login'] = true;
					$hidden['user_password'] = true;
				} 
				elseif (!$_CAN_EDIT) {
					$disabled['user_login'] = true;
					$disabled['user_password'] = true;
				}

				/*
				if ($auth->data["user_address"] == $company->data["address_id"]) {
					$hidden['user_login'] = true;
					$hidden['user_password'] = true;
				}
				*/
			}
			
			// user other roles
			$result = $model->query("
				SELECT 
					role_id,
					role_name
				FROM roles
				INNER JOIN user_roles ON user_role_role = role_id
				WHERE 
					user_role_user = $param
				AND role_application NOT IN ('mps','lps')
			")->fetchAll();
			
			if ($result) {
				$dataloader['other_roles'] = _array::extract($result);
				$data['other_roles'] = serialize(array_keys($dataloader['other_roles']));
			}
			
		}
		else {
			$data['redirect'] = $this->request->query("user/$_COMPANY");
			$data['user_active'] = 1;
		}
		
		// dataloader: planning roles
		$mpsRoles = role::get_application_roles('mps') ?: array();
		$mpsRoles = $mpsRoles;
		$dataloader['roles'] = $mpsRoles ?: array();				
		
		// dataloader: lps roles
		$lpsRoles = role::get_application_roles('lps') ?: array();
		$lpsRoles = $lpsRoles;
		$dataloader['lps_roles'] = $lpsRoles ?: array();

		// dataloader: news roles
		$result = $model->query("
			SELECT role_id, role_name
			FROM roles
			WHERE role_application = 'news'
			ORDER BY role_name
		")->fetchAll();
		
		$dataloader['news_roles'] = $result ? _array::extract($result) : array();

		// send used roles with request
		$usedRoles = $dataloader['roles'] + $dataloader['lps_roles'] + $dataloader['news_roles'];
		$data['used_roles'] = $usedRoles ? serialize(array_keys($usedRoles)) : null;
		
		// dataloader: user sex
		$dataloader['user_sex'] = array(
			'm' => $this->translate->male,
			'f' => $this->translate->female
		);
		
		// dataloader: news roles
		$result = $model->query("
			SELECT 
				address_id, 
				CONCAT(address_company, ', ', place_name) as address_company 
			FROM addresses 
			LEFT JOIN places on place_id = address_place_id  
			WHERE address_id = $company->id
			OR (
				address_country = $company->country
				AND address_involved_in_planning = 1
				AND address_active = 1
			)  
			ORDER BY address_company
		")->fetchAll();
		
		$dataloader['company'] = _array::extract($result);
		$data['company'] = $user->address;

		// form url vars
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['action'] = $this->request->action;
		$data['user_address'] = $_COMPANY;

		$data['user_phone'] = $data['user_phone'];
		$data['user_mobile_phone'] = $data['user_mobile_phone'];

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_VIEWS.'user.data.inc');
		$tpl->data('data', $data);
		$tpl->data('loginData', $loginData);
		$tpl->data('dataloader', $dataloader);
		$tpl->data('disabled', $disabled);
		$tpl->data('hidden', $hidden);
		$tpl->data('buttons', $buttons);
		$tpl->data('id', $_COMPANY);
		
		$this->view->setTemplate('userForm', $tpl);

		Compiler::attach(array(
			DIR_SCRIPTS."fancybox/fancybox.css",
			DIR_SCRIPTS."fancybox/fancybox.js",
			DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css",
			DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js",
			DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js",
			DIR_SCRIPTS."qtip/qtip.css",
			DIR_SCRIPTS."qtip/qtip.js",
			DIR_SCRIPTS."chain/chained.select.js",
			DIR_JS."user.js"
		));
	}

	public function printroles() {

		$model = new Model(Connector::DB_CORE);

		// datagrid
		$result = $model->query("
			SELECT SQL_CALC_FOUND_ROWS DISTINCT
				role_id, 
				role_application,
				role_code, 
				role_name,
				IF(role_responsible_user_id > 0, CONCAT(user_firstname, ' ', user_name) , '') AS user
			FROM roles
			LEFT JOIN users ON user_id = role_responsible_user_id
			ORDER BY role_application, role_code, role_name
		")->fetchAll();

		$_DATAGRID = array();

		if ($result) {				
			foreach ($result as $row) {
				$id = $row['role_id'];
				$app = $row['role_application'];
				$_DATAGRID[$app][$id]['role_code'] = $row['role_code'];
				$_DATAGRID[$app][$id]['role_name'] = $row['role_name'];
				$_DATAGRID[$app][$id]['user'] = $row['user'];
			}
		}
		
		$result = $model->query("
			SELECT 
				client_type_id,
				client_type_code
			FROM client_types
			ORDER BY client_type_code
		")->fetchAll();

		$clienttypes = _array::extract($result);

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_VIEWS.'user.print.roles.inc');
		$tpl->data('clienttypes', $clienttypes);
		$tpl->data('datagrid', $_DATAGRID);

		$this->view->setTemplate('printRoles', $tpl);

		Compiler::attach(array(
			DIR_SCRIPTS."dropdown/dropdown.css",
			DIR_SCRIPTS."dropdown/dropdown.js",
			'/public/css/switch.css'
		));
	}
}