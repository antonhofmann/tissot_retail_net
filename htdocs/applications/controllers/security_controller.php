<?php 

	class Security_Controller {
		
		public $request;
		
		public $view;
		
		public function __construct() {
			
			$this->request = request::instance();
			$this->view = new View();

			Compiler::attach(Theme::$Default);
		}
	
		public function index() {
				 
			if (!session::get('user_id')) {
				url::redirect('/security/login');
			}
			
			// get navigations
			$result = Navigation::load();
			$navigations = Navigation::buildTree($result);
			
			$currentNavigationLevel = $navigations[$this->request->id];
			$currentNavigationLevel = array_shift($currentNavigationLevel);
			$redirect = $currentNavigationLevel['url'];

			if ($redirect) {
				url::redirect("/$redirect");
			} else {
				url::redirect('/security/maintenance/maintenance');
			}
		}
		
		public function password() {
			
			$translate = translate::instance();
			$this->view->pagetitle = $translate->password_forgotten;
			
			Compiler::attach('security.css');
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// template: password forgotten
			$this->view->passwordform('pagecontent')->security("password.forgotten");
		}
		
		public function reset() {
			
			$translate = translate::instance();
			$this->view->pagetitle = $translate->password_reset;
			
			Compiler::attach('security.css');
			
			$param = url::param();
			$id = Session::get('user_id');
			

			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			if ($param) {
				
				$user = User::identification(array(
					'user_password_reset_id' => $param
				));
				
				$id = $user['user_id'];
			}
			
			if (!$id) { 
				$id = Message::id('password_reset_link_expired');
				url::redirect("/messages/show/$id");
			}
			
			// template: password reset form
			$this->view->passwordform('pagecontent')
			->security("password.reset")
			->data('id', $id);
		}
				
		public function login() { 
			
			$translate = translate::instance();
			$this->view->pagetitle = $translate->login;
							
			$this->view->master("security.inc");
			$this->view->classname = "login";
			
			// template: welcome message
			$this->view->welcomBox('pageleft')->security('welcome');

			// template: system activity box
			$this->view->activityBox('pageleft')->security('activity');
			
			// template: login fomr
			$this->view->loginForm('pagecontent')
			->security('login')
			->data('maintenance', Maintenance::warnings());

			Compiler::attach(array(
				'/public/themes/default/css/security.css',
				'/public/scripts/validationEngine/css/validationEngine.jquery.css',
				'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
				'/public/scripts/validationEngine/js/jquery.validationEngine.js',
				'/public/scripts/qtip/qtip.css',
				'/public/scripts/qtip/qtip.js',
				'/public/js/form.validator.js'
			));
		}
		
		public function logout() {
			Session::destroy();
			Message::logout_success();
			url::redirect('/security');
		}

		public function lockedip() {

			if (!session::get('user_id')) {
				url::redirect('/security/login');
			}
			
			Compiler::attach('security.css');
		
			$this->user = User::instance();
			$this->request = request::instance();
			$this->view->pagetitle = $this->request->title;
			
			// template: menues
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');
			
			// template: locked IP's
			$this->view->lockedIPList('pagecontent')
			->security('ip.locked.list');
		}
		
		public function maintenance() {

			if (!session::get('user_id')) {
				url::redirect('/security/login');
			}
			
			$maintenance = Maintenance::getActives();
			
			if (!$maintenance) {
				url::redirect('/mps');
			}
			
			Compiler::attach('security.css');
			Compiler::attach('/public/scripts/countdown/jquery.countdown.css');
			Compiler::attach('/public/scripts/countdown/jquery.countdown.js');
			
			
			$translate = translate::instance();
			$this->view->pagetitle = $translate->maintenance;
			
			$this->view->master("security.inc");
			
			// template: system activity box
			$this->view->maintenance('pagecontent')->security('maintenance')->data('maintenance', $maintenance);
		}

		public function invitation() {

			$hash = url::param();
			$email = $hash ? base64_decode($hash) : null;

			if (!$email) { 
				url::redirect("/messages/show/access_denied");
			}

			$model = new Model();

			$result = $model->query("SELECT * FROM user_invitations WHERE user_invitation_email = '$email' ")->fetch();

			if (!$result['user_invitation_id']) {
				url::redirect("/messages/error/invitation_request_user_account");
			}
			
			$translate = translate::instance();
			$this->view->pagetitle = 'Request an account';

			Request::instance()->controller = 'mycompany';

			$translate = Translate::instance();
			
			Compiler::attach('security.css');
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."user.js");

			$model = new Model();
			
			$result = $model->query("
				SELECT role_id, role_name
				FROM roles	
				WHERE role_is_visible_in_request_for_an_account = 1
				ORDER BY role_name
			")->fetchAll();
			
			$dataloader['roles'] = _array::extract($result);

			$result = $model->query("
				SELECT 
					address_id, 
					LEFT(CONCAT(address_company, ', ', country_name), 75) AS company
				FROM addresses	
				INNER JOIN countries ON country_id = address_country
				WHERE address_active = 1 and address_is_a_hq_address = 1 
				ORDER BY company
			")->fetchAll();
			
			$dataloader['user_address'] = _array::extract($result);
			
			// send used roles with request
			if ($dataloader['roles']) {
				//$data['used_roles'] = serialize(array_keys($dataloader['roles']));
			}
			
			// dataloader: user sex
			$dataloader['user_sex'] = array(
				'm' => $translate->male,
				'f' => $translate->female
			);
			
			$buttons = array();
			$buttons['save'] = true;

			$data['controller'] = 'mycompany';
			$data['action'] = $this->request->action;
			$data['user_email'] = $email;
			$data['invitation_email'] = $email;
			$disabled['user_email'] = true;
			
			// template: password reset form
			$this->view->userForm('pagecontent')->user("data")
			->data('mycompany', true)
			->data('email', $email)
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons)
			->data('disabled', $disabled);
		}
	}
	