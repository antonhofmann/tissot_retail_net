<?php 

class ProductSubgroups_Controller {
	
	public function __construct() {
			
		$this->user = User::instance();
		$this->request = request::instance();
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}
	
	public function index() { 
	
		switch ($this->request->application) {
		
			case 'lps':
				$permission_edit = user::permission('can_edit_lps_product_groups');

				if ($permission_edit) {
					$this->request->field('add',$this->request->query('add'));
					$this->request->field('data',$this->request->query('data'));
				}

			break;
		}

		// button: print
		$link = $this->request->link('/applications/modules/product/subgroup/print.php');
		$this->request->field('print', $link);

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'list.php');
		$tpl->data('url', '/applications/modules/product/subgroup/list.php');
		$tpl->data('class', 'list-800');
		$this->view->setTemplate('producSubGroup', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js'
		));
	}

	public function add() {

		$application = $this->request->application;
		$permissionEdit = false;

		switch ($application) {

			case 'lps':
				$permissionEdit = user::permission('can_edit_lps_product_groups');

				$queryGroups = "
					SELECT 
						lps_product_group_id,
						lps_product_group_name
					FROM lps_product_groups
					WHERE lps_product_group_active = 1
					ORDER BY lps_product_group_name
				";

			break;
		}
	
		// check access
		if($this->request->archived || !$permissionEdit) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$model = new Model($application);
			
		// buttons
		$buttons = array();
		$buttons['save'] = true;
		$buttons['back'] = $this->request->query();

		// dataloader groups
		$result = $model->query($queryGroups)->fetchAll();
		$dataloader['product_subgroup_group_id'] = _array::extract($result);

		// form dataloader
		$data = array();
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['action'] = $this->request->action;
		$data['redirect'] = $this->request->query('data');
		$data['product_subgroup_active'] = 1;

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'product/subgroup/form.php');
		$tpl->data('data', $data);
		$tpl->data('dataloader', $dataloader);
		$tpl->data('buttons', $buttons);
		$this->view->setTemplate('productSubGroup', $tpl);

		Compiler::attach(array(
			'/public/scripts/validationEngine/css/validationEngine.jquery.css',
			'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
			'/public/scripts/validationEngine/js/jquery.validationEngine.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/js/form.validator.js'
		));
	}
	
	public function data() {

		$id = url::param();
		$application = $this->request->application;
		$permissionEdit = false;

		switch ($application) {

			case 'lps':
				$permissionEdit = user::permission('can_edit_lps_product_groups');
				
				$tableName = 'lps_product_subgroups';

				$mapFields = array(
					'lps_product_subgroup_id' => 'product_subgroup_id',
					'lps_product_subgroup_group_id' => 'product_subgroup_group_id',
					'lps_product_subgroup_name' => 'product_subgroup_name',
					'lps_product_subgroup_active' => 'product_subgroup_active'
				);

				$queryGroups = "
					SELECT DISTINCT
						lps_product_group_id,
						lps_product_group_name
					FROM lps_product_groups
					WHERE lps_product_group_active = 1 OR lps_product_group_id = ?
					ORDER BY lps_product_group_name
				";

			break;
		}

		// read modul
		$modul = new Modul($application);
		$modul->setTable($tableName);
		$modul->setDataMap($mapFields);
		$data = $modul->read($id);

		// check access
		if (!$modul->id) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		// db model
		$model = new Model($application);

		// dataloader: product groups
		$sth = $model->db->prepare($queryGroups);
		$sth->execute(array($modul->product_subgroup_group_id));
		$result = $sth->fetchAll();
		$dataloader['product_subgroup_group_id'] = _array::extract($result);

		// form hidden vars
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['action'] = $this->request->action;

		// buttons
		$buttons = array();
		$buttons['back'] = $this->request->query();

		if (!$this->request->archived) { 
			
			if ($permissionEdit) {
			
				$buttons['save'] = true;

				$integrity = new Integrity();
				$integrity->set($modul->id, $tableName);

				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/modules/product/subgroup/delete.php', array('id'=>$id));
				}

			} elseif($data) {
				$disabled = true;
			}

		} else {
			$disabled = true;
		}

		// header
		$this->view->header('pagecontent')->node('header')->data('title', $modul->product_subgroup_name);
		$this->view->companytabs('pagecontent')->navigation('tab');
			
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'product/subgroup/form.php');
		$tpl->data('data', $data);
		$tpl->data('dataloader', $dataloader);
		$tpl->data('buttons', $buttons);

		// disabled fields
		if ($disabled) {
			$fields = array_keys($data);
			$tpl->data('disabled', array_fill_keys($fields, true));
		}

		$this->view->setTemplate('productSubGroup', $tpl);

		Compiler::attach(array(
			'/public/scripts/validationEngine/css/validationEngine.jquery.css',
			'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
			'/public/scripts/validationEngine/js/jquery.validationEngine.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/js/form.validator.js'
		));
	}
}