<?php

	class Countries_Controller {

		public function __construct() {
			
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}

		public function index() {
			
			$requestFields = array();
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			
			$permission_manager = user::permission('can_administrate_mps');
			$permission_catalog = user::permission('can_edit_catalog');
			
			$this->view->pagetitle = $this->request->title; 

			if (!$this->request->archived && $permission_catalog) {
				
				$link = $this->request->query("add");
				$this->request->field('add', $link);
			}
			
			$link = $this->request->query('data');
			$this->request->field('data', $link);
			
			$this->view->countries('pagecontent')->country('list');
		}

		public function add() {

			// permission: edit announcement
			$permission_manager = user::permission('can_administrate_mps');
			$permission_catalog = user::permission('can_edit_catalog');

			if ($this->request->archived && !$permission_catalog) {
				Message::invalid_permission();
				url::redirect($this->request->query());
			}
			
			// css/js integration
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			
			$buttons = array();
			$buttons['back'] =  $this->request->query();
			$buttons['save'] = true;
			
			$model = new Model(Connector::DB_CORE);
			
			// dataloader: hemispheres
			$result = $model->query("
				SELECT 
					hemisphere_id,
					hemisphere_name 
				FROM hemispheres 
				ORDER BY hemisphere_name
			")->fetchAll();
			
			$dataloader['country_hemisphere_id'] = _array::extract($result);
			
			// dataloader: regions
			$result = $model->query("
				SELECT 
					region_id, 
					region_name
				FROM regions
				WHERE region_active = 1
				ORDER BY region_name
			")->fetchAll();
			
			$dataloader['country_region'] = _array::extract($result);
			
			// dataloader: sales regions
			$result = $model->query("
				SELECT 
					salesregion_id, 
					salesregion_name 
				FROM salesregions 
				ORDER BY salesregion_name
			")->fetchAll();
			
			$dataloader['country_salesregion'] = _array::extract($result);
			
			// dataloader: regions
			$result = $model->query("
				SELECT 
					currency_id,
					CONCAT(currency_symbol, ', ', currency_name) AS currency
				FROM currencies 
				ORDER BY currency_symbol
			")->fetchAll();
			
			$dataloader['country_currency'] = _array::extract($result);
			
			// dataloader: timeformat
			$dataloader['country_timeformat'] = array(
				24 => '24 hours', 
				12 => '12 hours'
			);

			$this->view->announcement('pagecontent')
			->country('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons);
		}

		public function data() {
			
			$id = url::param();
			
			$country = new Country();
			$country->read($id);
			
			if (!$country->id) {
				Message::failure_id();
				url::redirect($this->request->query());
			}
			
			$model = new Model(Connector::DB_CORE);
			
			// permission: edit announcement
			$permission_manager = user::permission('can_administrate_mps');
			$permission_catalog = user::permission('can_edit_catalog');
			$permission_uberall = user::permission('can_edit_socialmedias');
			
			// css/js integration
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// form loader
			$data = $country->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// dataloader: hemispheres
			$result = $model->query("
				SELECT 
					hemisphere_id,
					hemisphere_name 
				FROM hemispheres 
				ORDER BY hemisphere_name
			")->fetchAll();
			
			$dataloader['country_hemisphere_id'] = _array::extract($result);
			
			// dataloader: regions
			$result = $model->query("
				SELECT 
					region_id, 
					region_name
				FROM regions
				WHERE region_active = 1
				ORDER BY region_name
			")->fetchAll();
			
			$dataloader['country_region'] = _array::extract($result);
			
			// dataloader: sales regions
			$result = $model->query("
				SELECT 
					salesregion_id, 
					salesregion_name 
				FROM salesregions 
				ORDER BY salesregion_name
			")->fetchAll();
			
			$dataloader['country_salesregion'] = _array::extract($result);
			
			// dataloader: regions
			$result = $model->query("
				SELECT 
					currency_id,
					CONCAT(currency_symbol, ', ', currency_name) AS currency
				FROM currencies 
				ORDER BY currency_symbol
			")->fetchAll();
			
			$dataloader['country_currency'] = _array::extract($result);
			
			// dataloader: timeformat
			$dataloader['country_timeformat'] = array(
				24 => '24 hours', 
				12 => '12 hours'
			);
			
			if ($permission_manager || $permission_catalog || $permission_uberall) {
				
				$buttons['save'] = true;
				
				if ($permission_manager || $permission_catalog || $permission_uberall) {
					
					$integrity = new Integrity();
					$integrity->set($id, 'countries', 'system');
					
					// button: delete
					if ($integrity->check()) {
						$buttons['delete'] = $this->request->link('/applications/helpers/country.delete.php', array('id' >$id));
					}
					
				} else {
					
					foreach ($country->data as $field => $value) {
						$disabled[$field] = true;
					}
					
					unset($disabled['country_hemisphere_id']);
				}
				
			} else {
				foreach ($country->data as $field => $value) {
					$disabled[$field] = true;
				}
			}
			
			// template header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $country->name);

			$this->view->announcement('pagecontent')
			->country('data')
			->data('data', $data)
			->data('disabled', $disabled)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons);
		}


		public function localization() {
			
			$this->view->pagetitle = $this->request->title . " - " . translate::instance()->translation;

			// view countries
			$link = $this->request->query('data');
			$this->request->field('form', $link);
			$this->request->field('application', $this->request->application);

			
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'list.php');
			$tpl->data('url', '/applications/modules/country/localization/list.php');
			$tpl->data('class', 'list-600');
			$this->view->setTemplate('collections', $tpl);

			Compiler::attach(array(
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js',
				'/public/js/countries.localization.js'
			));
		}
	}
	