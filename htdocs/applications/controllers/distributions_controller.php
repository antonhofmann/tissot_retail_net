<?php 

	class Distributions_Controller {
		
		public function __construct() {
			
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			// permissions
			$permission_view = user::permission(Ordersheet::PERMISSION_VIEW);
			$permission_view_limited = user::permission(Ordersheet::PERMISSION_VIEW_LIMITED);
			$permission_edit = user::permission(Ordersheet::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Ordersheet::PERMISSION_EDIT_LIMITED);
			$permission_manage = user::permission(Ordersheet::PERMISSION_MANAGE);
			
			// access to order sheet
			if ($permission_view || $permission_view_limited || $permission_edit || $permission_edit_limited) {
				$field = $this->request->query('data');
				$this->request->field('form', $field);
			}
			
			// filter: limited permission
			if(!$permission_manage && !$permission_edit && !$permission_view) {
				$this->request->field('limited', true); 
			}
			
			// template: master sheets list
			$this->view->orderSheetsList('pagecontent')->ordersheet('list');
		}
		
		public function data() { 
			
			$id = url::param();
			
			// order sheet
			$ordersheet = new Ordersheet();
			$ordersheet->read($id);
			
			// check access to order sheet
			if (!$ordersheet->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			if (!$this->request->archived && $ordersheet->state()->isArchived()) {
				url::redirect($this->request->query("archived/data/$id"));
			}
			
			// order sheet editabled states
			$editabled_states = Ordersheet_State::loader('edit');
			
			// data: mastersheet
			$mastersheet = new Mastersheet();
			$mastersheet->read($ordersheet->mastersheet_id);
			
			// form dataloader
			$data = $ordersheet->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['redirect'] = $this->request->query("data/$id");
			$data['mps_mastersheet_year'] = $mastersheet->year;
			$data['mps_mastersheet_name'] = $mastersheet->name;		

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			foreach ($data as $key => $value) {
				$disabled[$key] = true;
			}
			
			// db application model
			$model = new Model($this->request->application);
			
			// workflow states
			$result = $model->query("
				SELECT mps_workflow_state_id, mps_workflow_state_name
				FROM mps_workflow_states
			")->fetchAll();
				
			// dataloader: workflow states
			$dataloader['mps_ordersheet_workflowstate_id'] = _array::extract($result);
			
			// order sheet tracking
			if ($ordersheet->workflowstate_id >= Workflow_State::STATE_COMPLETED) { 
				
				// planning tracking
				$result = $model->query("
					SELECT 
						user_tracking_id,
						DATE_FORMAT(user_trackings.date_created,'%d.%m.%Y') AS date,
						user_tracking_action,
						CONCAT(user_firstname,' ', user_name) AS user
					FROM user_trackings
				")
				->bind(User_Tracking::DB_BIND_USERS)
				->filter('entity', "user_tracking_entity = '".User_Tracking::ENTITY_ORDER_SHEETS."'")
				->filter('ordersheet', "user_tracking_entity_id = $id")
				->fetchAll();
				
				if ($result) {
					
					// datagrid	
					$trackings = _array::datagrid($result);
					
					// workflow state
					$workflow_state = new Workflow_State($this->request->application);
					
					foreach ($trackings as $key => $value) {
						
						// define workflow state from action label
						$state = ($value['user_tracking_action'] == User_Tracking::ACTION_WORKFLOW_STATE_COMPLETED) 
							? Workflow_State::STATE_COMPLETED 
							: Workflow_State::STATE_APPROVED;
						
						$workflow_state->read($state);
						$trackings[$key]['workflow_state'] = ucfirst($workflow_state->name);
					}					
				}
			}
			
			// template: headers
			$this->view->header('pagecontent')->node('header')->data('title', $ordersheet->header());
			$this->view->tabs('pagecontent')->navigation('tab');
			
			// template: order sheet form
			$this->view->ordersheet('pagecontent')
			->ordersheet('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('trackings', $trackings)
			->data('id', $id);
		}
		
		public function distribution() {
			
			$this->view->master('spreadsheet.inc.php');
			
			$id = url::param();
			
			// order sheet
			$ordersheet = new Ordersheet();
			$ordersheet->read($id);
			
			// check access to order sheet
			if (!$ordersheet->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			if (!$this->request->archived && $ordersheet->state()->isArchived()) {
				url::redirect($this->request->query("archived/versions/$id"));
			}
				
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// template: headers
			$this->view->header('pagecontent')->node('header')->data('title', $ordersheet->header());
			$this->view->tabs('pagecontent')->navigation('tab');
			
			// template: order sheet cost
			$this->view->orderSheetPlanning('pagecontent')
			->ordersheet('planning')
			->data('data', $data)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function items() {
			
			$id = url::param();
			
			// order sheet
			$ordersheet = new Ordersheet();
			$ordersheet->read($id);
			
			// check access to order sheet
			if (!$ordersheet->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			if (!$this->request->archived && $ordersheet->state()->isArchived()) {
				url::redirect($this->request->query("archived/items/$id"));
			}
			
			// set itemstabs as readonly
			$readonly = true;
			
			// block statments
			$statment_on_preparation = $ordersheet->state()->onPreparation();
			$statment_on_ordering = $ordersheet->state()->onConfirmation();
			$statment_on_distribution = $ordersheet->state()->onDistribution();
			
			// redirect link
			$data['redirect'] = $this->request->query("items/$id");
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// table columns
			$columns = array();
			
			// data: proposed and quantity for managers
			$columns['proposed'] = true;
			$columns['quantity'] = true;
			
			// data: approved
			if ($ordersheet->state()->manager || $ordersheet->state()->adminstrator) {
				$columns['approved'] = true;
			}
			elseif ($ordersheet->state()->isApproved() || ($statment_on_ordering || $statment_on_distribution)) {
				$columns['approved'] = true;
			}
			
			// data: pre ordered
			if($ordersheet->state()->manager && $statment_on_ordering) {
				$columns['ordered'] = true;
			}
			
			// data: distributed
			if($ordersheet->state()->manager|| ($statment_on_ordering || $statment_on_distribution)) {
				//$columns['delivered'] = true;
				//$buttons['save'] = true;
			}
			
			// template: headers
			$this->view->header('pagecontent')->node('header')->data('title', $ordersheet->header());
			$this->view->tabs('pagecontent')->navigation('tab');
				
			$this->view->ordersheetItems('pagecontent')
			->ordersheet('item.list')
			->data('data', $data)
			->data('columns', $columns)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function files() {
			
			$id = url::param();
			
			// order sheet
			$ordersheet = new Ordersheet();
			$ordersheet->read($id);
			
			// check access to order sheet
			if (!$ordersheet->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			if (!$this->request->archived && $ordersheet->state()->isArchived()) {
				url::redirect($this->request->query("archived/files/$id"));
			}
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// template: headers
			$this->view->header('pagecontent')->node('header')->data('title', $ordersheet->header());
			$this->view->tabs('pagecontent')->navigation('tab');
				
			$this->view->orderSheetFiles('pagecontent')
			->ordersheet('file.list')
			->data('data', $data)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function versions() {
			
			$id = url::param();
			$param = url::param(1);
			
			// order sheet
			$ordersheet = new Ordersheet();
			$ordersheet->read($id);
			
			// check access to order sheet
			if (!$ordersheet->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			if (!$this->request->archived && $ordersheet->state()->isArchived()) {
				url::redirect($this->request->query("archived/versions/$id"));
			}
			
			// buttons
			$buttons = array();
			
			// template: headers
			$this->view->header('pagecontent')->node('header')->data('title', $ordersheet->header());
			$this->view->tabs('pagecontent')->navigation('tab');
			
			if ($param) {
				
				$buttons['back'] = $this->request->query("versions/$id");
				
				// data: mastersheet
				$mastersheet = new Mastersheet();
				$mastersheet->read($ordersheet->mastersheet_id);
					
				$data = $ordersheet->data;
				$data['mps_mastersheet_year'] = $mastersheet->year;
				$data['mps_mastersheet_name'] = $mastersheet->name;
								
				// dataloader: workwlow states
				$dataloader['mps_ordersheet_workflowstate_id'] = Workflow_State::loader($this->request->application);
				
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
				
				// button: print
				$buttons['print'] = $this->request->link('/applications/exports/excel/ordersheet.items.version.php', array(
					'ordersheet' => $id,
					'version' => $param
				));
				
				// template: order sheet version detail view
				$this->view->orderSheetVersionForm('pagecontent')
				->ordersheet('version.data')
				->data('data', $data)
				->data('dataloader', $dataloader)
				->data('disabled', $disabled)
				->data('hidden', $hidden)
				->data('buttons', $buttons)
				->data('id', $id)
				->data('version', $param);
			}
			else {
				
				// request field id
				$this->request->field('id', $id);
				
				// request field fomr link
				$formlink = $this->request->query("versions/$id");
				$this->request->field('form', $formlink);
				
				$buttons['back'] = $this->request->query();
				
				// template: order sheet versions
				$this->view->orderSheetVersionsList('pagecontent')
				->ordersheet('version.list')
				->data('data', $data)
				->data('buttons', $buttons)
				->data('id', $id);
			}
		}
		
		public function cost() {
			
			$id = url::param();
			$param = url::param(1);
			
			// order sheet
			$ordersheet = new Ordersheet();
			$ordersheet->read($id);
			
			// check access to order sheet
			if (!$ordersheet->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			if (!$this->request->archived && $ordersheet->state()->isArchived()) {
				url::redirect($this->request->query("archived/versions/$id"));
			}
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// template: headers
			$this->view->header('pagecontent')->node('header')->data('title', $ordersheet->header());
			$this->view->tabs('pagecontent')->navigation('tab');
				
			// template: order sheet cost
			$this->view->orderSheetCost('pagecontent')
			->ordersheet('cost')
			->data('data', $data)
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}
	