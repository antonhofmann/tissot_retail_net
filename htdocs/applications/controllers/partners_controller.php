<?php
	
	class Partners_Controller {

		public function __construct() {

			$this->request = request::instance();
	
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');
			$this->translate = translate::instance();

			Compiler::attach(Theme::$Default);
		}

		public function index() {
			
			switch ($this->request->application) {

				case 'red':
					
					$permission_system_view = user::permission(RED::PERMISSION_VIEW_SYSTEM_DATA);
					$permission_system_edit = user::permission(RED::PERMISSION_EDIT_SYSTEM_DATA);
					
					// request form: add button
					if (!$this->request->archived && $permission_system_edit) {
						$field = $this->request->query('add');
						$this->request->field('add',$field);
					}
						
					// request form: link to basic data
					if ($permission_system_view) {
						$field = $this->request->query('address');
						$this->request->field('form', $field);
					}
					
					// template: partners list
					$this->view->red_partner('pagecontent')->red('partner.list');
				
				break;
			}
		}

		public function add() {

			switch ($this->request->application) {

				case 'red':
					
					// permissions
					$permission_system_view = user::permission(RED::PERMISSION_VIEW_SYSTEM_DATA);
					$permission_system_edit = user::permission(RED::PERMISSION_EDIT_SYSTEM_DATA);

					// check access to add partners
					if ($this->request->archived || !$permission_system_edit) {
						message::access_denied();
						url::redirect($this->request->query());
					}
					
					Compiler::attach(DIR_SCRIPTS."loader/loader.css");
					Compiler::attach(DIR_SCRIPTS."loader/loader.js");
					Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
					Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
					Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
					Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
					Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
					Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
					Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
					Compiler::attach(DIR_SCRIPTS."chain/chained.select.js");
					Compiler::attach(DIR_JS."user.js");
					
					// form dataloader
					$data = array();
					$data['application'] = $this->request->application;
					$data['redirect'] = $this->request->query('data');
					
					// buttons
					$button = array();
					$buttons['back'] = $this->request->query();
					$buttons['save'] = true;

					// hide unused fields
					$hidden['address_involved_in_planning'] = true;
					$hidden['address_is_independent_retailer'] = true;
					$hidden['address_can_own_independent_retailers'] = true;
					$hidden['address_parent'] = true;
					$hidden['address_mps_shipto'] = true;
					$hidden['address_mps_customernumber'] = true;
					$hidden['address_involved_in_planning_ff'] = true;
					$hidden['address_involved_in_lps'] = true;
					$hidden['create_pos_from_company'] = true;
					$hidden['address_do_data_export_from_mps_to_sap'] = true;
					$hidden['address_is_internal_address'] = true;

					// prefill fields
					$data['address_type'] = 12;
					$data['address_involved_in_red'] = 1;
					$data['address_active'] = 1;
					$data['address_showinposindex'] = 0;

					// dataloader: countries
					$dataloader['address_country'] = country::loader();

					// dataloader: provinces
					$dataloader['address_province_id'] = province::loader(array(
						'province_country = '.$company->country
					));

					// dataloader: places
					$dataloader['address_place_id'] = place::loader(array(
						'place_country = '.$company->country,
						'place_province = '.$company->province_id
					));

					// dataloader: currencies
					$dataloader['address_currency'] = currency::loader();

					// template: address from
					$this->view->partnerForm('pagecontent')
					->company('data')
					->data('data', $data)
					->data('dataloader', $dataloader)
					->data('disabled', $disabled)
					->data('hidden', $hidden)
					->data('buttons', $buttons);
				
				break;
			}
		}

		public function address() {

			switch ($this->request->application) {

				case 'red':
					
					$id = url::param();
					
					// permissions
					$permission_system_view = user::permission(RED::PERMISSION_VIEW_SYSTEM_DATA);
					$permission_system_edit = user::permission(RED::PERMISSION_EDIT_SYSTEM_DATA);
					$permission_system_delete = user::permission(RED::PERMISSION_DELETE_SYSTEM_DATA);

					$company = new Company();
					$company->read($id);
					
					// check access to add partners
					if (!$company->id || (!$permission_system_view && !$permission_system_edit)) {
						message::access_denied();
						url::redirect($this->request->query());
					}
					elseif ($this->request->archived) {
						url::redirect($this->request->query("archived/address/$id"));
					}
					
					Compiler::attach(DIR_SCRIPTS."loader/loader.css");
					Compiler::attach(DIR_SCRIPTS."loader/loader.js");
					Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
					Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
					Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
					Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
					Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
					Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
					Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
					Compiler::attach(DIR_SCRIPTS."chain/chained.select.js");

					// form dataloader
					$data = $company->data;
					$data['application'] = $this->request->application;
					$data['controller'] = $this->request->controller;
					$data['redirect'] = $this->request->query('address');
					
					// buttons
					$buttons = array();
					$buttons['back'] = $this->request->query();

					// only companies of address_type = 12 must be editable
					if ($data['address_type'] != 12) {
						
						foreach ($data as $field => $value) {
							$disabled[$field] = true;
						}
						
						$buttons['save'] = false;
					}
					elseif ($permission_system_edit) {
						
						// button: save
						$buttons['save'] = true;
					}

					// hide unused fields
					$hidden['address_involved_in_planning'] = true;
					$hidden['address_is_independent_retailer'] = true;
					$hidden['address_can_own_independent_retailers'] = true;
					$hidden['address_parent'] = true;
					$hidden['address_mps_shipto'] = true;
					$hidden['address_mps_customernumber'] = true;
					$hidden['address_involved_in_planning_ff'] = true;
					$hidden['address_involved_in_lps'] = true;
					$hidden['create_pos_from_company'] = true;
					$hidden['address_do_data_export_from_mps_to_sap'] = true;
					$hidden['address_is_internal_address'] = true;
					$hidden_fields['address_legnr'] = true;
					$disabeld_fields['address_legnr'] = true;
						
					// show legal number
					if ($company->client_type==2 || $company->client_type==3) {
						unset($hidden_fields['address_legnr']);
					}

					// dataloader: countries
					$dataloader['address_country'] = country::loader();

					// dataloader: provinces
					$dataloader['address_province_id'] = province::loader(array(
						'province_country = '.$company->country
					));

					// dataloader: places
					$dataloader['address_place_id'] = place::loader(array(
						'place_country = '.$company->country,
						'place_province = '.$company->province_id
					));

					// dataloader: currencies
					$dataloader['address_currency'] = currency::loader();
					
					// template header
					$this->view->header('pagecontent')
					->node('header')
					->data('title', $company->header());
						
					// template: tabs
					$this->view->companytabs('pagecontent')
					->navigation('tab');

					// template: company form
					$this->view->partnerForm('pagecontent')
					->company('data')
					->data('data', $data)
					->data('dataloader', $dataloader)
					->data('disabled', $disabled)
					->data('hidden', $hidden)
					->data('buttons', $buttons);
				
				break;
			}
		}

		public function users() {

			switch ($this->request->application) {

				case 'red':

					$id = url::param();
					$param = url::param(1);
					
					// permissions
					$permission_system_view = user::permission(RED::PERMISSION_VIEW_SYSTEM_DATA);
					$permission_system_edit = user::permission(RED::PERMISSION_EDIT_SYSTEM_DATA);

					$company = new Company();
					$company->read($id);
					
					// check access to add partners
					if (!$company->id || (!$permission_system_view && !$permission_system_edit)) {
						message::access_denied();
						url::redirect($this->request->query());
					}
					elseif ($this->request->archived) {
						url::redirect($this->request->query("archived/address/$id"));
					}
					
					Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
					Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
					Compiler::attach(DIR_SCRIPTS."table.loader.js");
					Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
					Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
					Compiler::attach(DIR_SCRIPTS."loader/loader.css");
					Compiler::attach(DIR_SCRIPTS."loader/loader.js");
					Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
					Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
					Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
					Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
					Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
					Compiler::attach(DIR_SCRIPTS."chain/chained.select.js");
					Compiler::attach(DIR_JS."user.js");

					// buttons
					$buttons = array();

					// template header
					$this->view->header('pagecontent')
					->node('header')
					->data('title', $company->header());
						
					// template: tabs
					$this->view->companytabs('pagecontent')
					->navigation('tab');
					
					// user form
					if ($param) {
						
						$param = (is_numeric($param)) ? $param : null;
						
						// button: back
						$buttons['back'] = $this->request->query("users/$id");

						if ($param) {
							
							$user = new User($param);
							$data = $user->data;
							
							// user roles
							$data['roles'] = serialize($user->roles($param));
							
							if ($permission_system_edit) {
								
								// button: save
								$buttons['save'] = true;

								// db integrity
								$integrity = new Integrity();
								$integrity->set($param, 'users');
	
								if ($integrity->check()) {
									$buttons['delete'] = $this->request->link('/applications/helpers/user.delete.php', array(
										'id' => $id,
										'user' => $param
									));
								}
							}
							
							// hide unused fields
							$hidden['user_login'] = true;
							$hidden['user_password'] = true;
						}
						// add new for archive is not permitted
						elseif ($this->request->archived) {
							message::access_denied();
							url::redirect($this->request->query("address/$id"));
						}
						elseif($permission_system_edit) {
							$buttons['save'] = true;
							$data['redirect'] = $this->request->query("users/$id");
							$data['user_active'] = 1;
						} else {
							message::access_denied();
							url::redirect($this->request->query());
						}

						// form dataloader
						$data['application'] = $this->request->application;
						$data['controller'] = $this->controller;
						$data['action'] = $this->action;
						$data['user_address'] = $id;

						// set mandatory fields
						$mandatory = array('user_phone');

						// dataloader: user sex
						$dataloader['user_sex'] = array(
							'm' => $this->translate->male,
							'f' => $this->translate->female
						);


						// dataloader: roles
						$dataloader['roles'] = role::get_application_roles($this->request->application);

						// send used roles with request
						if ($dataloader['roles']) {
							$data['used_roles'] = serialize(array_keys($dataloader['roles']));
						}

						//set mandatory fields
						$mandatory = array('user_phone', 'user_email');

						// template: company user
						$this->view->userForm('pagecontent')
						->user('data')
						->data('data', $data)
						->data('dataloader', $dataloader)
						->data('hidden', $hidden)
						->data('mandatory', $mandatory)
						->data('dataloader', $dataloader)
						->data('buttons', $buttons)
						->data('application', $this->request->application)
						->data('controller', $this->controller)
						->data('action', $this->action);
					}
					// user list
					else {

						// request form: company
						$this->request->field('id', $id); 
						
						// request form: add field
						$field_add = $this->request->query("users/$id/add");
						$this->request->field('add',$field_add);
						
						// request form: edit field
						$field_edit = $this->request->query("users/$id");
						$this->request->field('form',$field_edit);

						$this->view->useryList('pagecontent')
						->user('list')
						->data('buttons', $buttons)
						->data('id', $id);
					}

				break;
			}
		}
	}
