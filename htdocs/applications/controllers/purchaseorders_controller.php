<?php 

	class Purchaseorders_Controller {
		
		public function __construct() { 
			
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() { 
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			// permissions
			$permission_view = user::permission(Mastersheet::PERMISSION_VIEW);
			$permission_edit = user::permission(Mastersheet::PERMISSION_EDIT);
			
			// form link
			$link = $this->request->query('data');
			$this->request->field('form', $link);
			
			// template: master sheet list 
			$this->view->masterSheetList('pagecontent')->ordersheet('purchaseorder.list');
		}
		
		public function data() {
			
			$id = url::param();
			$ordernumber = url::param(1);
			$application = $this->request->application;
				
			$ordersheet = new Ordersheet($application);
			$ordersheet->read($id);
			
			// check access
			if (!$ordersheet->id) {
				//message::access_denied();
				//url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
			Compiler::attach(DIR_CSS."jquery.ui.css");
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
				
			
			// add requesr param order number
			$this->request->params[] = $ordernumber;
			
			// buttons: back
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			$model = new Model($this->request->application);
			
			// form data loader
			$data = $model->query("
				SELECT DISTINCT
					mps_ordersheet_id,
					mps_mastersheet_name,
					mps_mastersheet_year,
					address_company,
					address_mps_shipto,
					mps_ordersheet_item_purchase_order_number,
					DATE_FORMAT(mps_ordersheet_item_order_date, '%d.%m.%Y') AS mps_ordersheet_item_order_date,
					DATE_FORMAT(mps_ordersheet_item_desired_delivery_date, '%d.%m.%Y') AS mps_ordersheet_item_desired_delivery_date
				FROM mps_ordersheets
				INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_ordersheet_id = mps_ordersheet_id
				INNER JOIN mps_mastersheets ON mps_mastersheet_id = mps_ordersheet_mastersheet_id
				INNER JOIN db_retailnet.addresses ON mps_ordersheet_address_id = address_id
				WHERE mps_ordersheet_id = $id
			")->fetch();
			
			// disable all fields
			if ($data) {
				$fields = array_keys($data);
				$disabled = array_fill_keys($fields, true);
			}
			
			// headers
			$purchase_order = $this->translate->mps_ordersheet_item_purchase_order_number.': '.$ordernumber;
			$this->view->header('pagecontent')->node('header')->data('title', $ordersheet->header().$purchase_order);
			$this->view->tabs('pagecontent')->navigation('tab');
				
			$this->view->ordersheetItems('pagecontent')
			->ordersheet('purchaseorder.data')
			->data('data', $data)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id)
			->data('ordernumber', $ordernumber);
		}
		
		public function items() {
			
			$id = url::param();
			$ordernumber = url::param(1);
				
			$ordersheet = new Ordersheet();
			$ordersheet->read($id);
			
			// check access
			if (!$ordersheet->id) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			// group statments
			$statment_on_preparation = $ordersheet->state()->onPreparation();
			$statment_on_completing = $ordersheet->state()->onCompleting();
			$statment_on_revision = $ordersheet->state()->onCompleting();
			$statment_on_confirmation = $ordersheet->state()->onConfirmation();
			$statment_on_distribution = $ordersheet->state()->onDistribution();
			
			// buttons: back
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// table columns
			$columns = array();
			$columns['quantity']['show'] = true;
			$columns['approved']['show'] = true;
			$columns['confirmed']['show'] = true;
			$columns['shipped']['show'] = true;
			$columns['distributed']['show'] = true;
			
			// headers
			$purchase_order = $this->translate->mps_ordersheet_item_purchase_order_number.': '.$ordernumber;
			$this->view->header('pagecontent')->node('header')->data('title', $ordersheet->header().$purchase_order);
			$this->view->tabs('pagecontent')->navigation('tab');
				
			// template: order sheet items
			$this->view->ordersheetItems('pagecontent')
			->ordersheet('item.list')
			->data('data', $data)
			->data('columns', $columns)
			->data('buttons', $buttons)
			->data('id', $id)
			->data('ordernumber', $ordernumber);
		}

	}
	