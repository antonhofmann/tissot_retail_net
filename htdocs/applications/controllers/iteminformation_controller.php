<?php 

	class Iteminformation_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
				
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {

			$permission_edit = user::permission('can_edit_catalog');
			$permission_edit_item_information = user::permission('can_edit_item_information');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			if ($permission_edit || $permission_edit_item_information) {
				$this->request->field('add',$this->request->query('add'));
			}
			
			$this->request->field('data',$this->request->query('data'));

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'iteminformation/list.php');
			$tpl->data('data', $data);
			$tpl->data('buttons', $buttons);
			$this->view->setTemplate('iteminformation', $tpl);

			Compiler::attach(array(
				DIR_SCRIPTS."dropdown/dropdown.css",
				DIR_SCRIPTS."dropdown/dropdown.js",
				DIR_SCRIPTS."table.loader.js"
			));
		}

		public function add() {

			$permission_edit = user::permission('can_edit_catalog');
			$permission_edit_item_information = user::permission('can_edit_item_information');
		
			if($this->request->archived || (!$permission_edit && !$permission_edit_item_information)) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
				
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();

			$model = new Model(Connector::DB_CORE);

			$result = $model->query("
				SELECT DISTINCT
					item_information_title_id,
					item_information_title_title
				FROM item_information_titles
				ORDER BY item_information_title_title
			")->fetchAll();

			$dataloader['item_information_title_id'] = _array::extract($result);

			$result = $model->query("
				SELECT DISTINCT
					address_id,
					address_company
				FROM suppliers
				INNER JOIN addresses ON supplier_address = address_id
				WHERE address_active = 1
				ORDER BY address_company
			")->fetchAll();

			$dataloader['item_information_address_id'] = _array::extract($result);
			
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'iteminformation/form.php');
			$tpl->data('data', $data);
			$tpl->data('dataloader', $dataloader);
			$tpl->data('buttons', $buttons);
			$tpl->data('hidden', $hidden);
			$tpl->data('disabled', $disabled);
			$this->view->setTemplate('iteminformation', $tpl);

			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
				Compiler::attach(DIR_CSS."jquery.ui.css");
				Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
				Compiler::attach(DIR_SCRIPTS."ajaxuploader/ajaxupload.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				Compiler::attach(DIR_JS."form.validator.file.js");
		}
		
		public function data() {

			$id = url::param();

			$item_information = new Iteminformation();
			$item_information->read($id);

			if (!$item_information->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$permission_edit = user::permission('can_edit_catalog');
			$permission_edit_item_information = user::permission('can_edit_item_information');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			$data = $item_information->data ?: array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;

			if ($data['item_information_version_date']) {
				$data['item_information_version_date'] = date::system($data['item_information_version_date']);
			}

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			if ($permission_edit || $permission_edit_item_information) {
			
				$buttons['save'] = true;

				
				$integrity = new Integrity();
				$integrity->set($id, 'item_informations', 'system');

				if ($id && $integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/modules/iteminformation/delete.php', array('id'=>$id));
				}
				
			}

			$model = new Model(Connector::DB_CORE);

			$result = $model->query("
				SELECT DISTINCT
					item_information_title_id,
					item_information_title_title
				FROM item_information_titles
				ORDER BY item_information_title_title
			")->fetchAll();

			$dataloader['item_information_title_id'] = _array::extract($result);

			$result = $model->query("
				SELECT DISTINCT
					address_id,
					address_company
				FROM suppliers
				INNER JOIN addresses ON supplier_address = address_id
				WHERE address_active = 1
				ORDER BY address_company
			")->fetchAll();

			$dataloader['item_information_address_id'] = _array::extract($result);
			

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $item_information->name);
			$this->view->companytabs('pagecontent')->navigation('tab');

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'iteminformation/form.php');
			$tpl->data('data', $data);
			$tpl->data('dataloader', $dataloader);
			$tpl->data('buttons', $buttons);
			$tpl->data('hidden', $hidden);
			$tpl->data('disabled', $disabled);
			
			$this->view->setTemplate('iteminformation', $tpl);

			
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
				Compiler::attach(DIR_CSS."jquery.ui.css");
				Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
				Compiler::attach(DIR_SCRIPTS."ajaxuploader/ajaxupload.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				Compiler::attach(DIR_JS."form.validator.file.js");
		}


		public function items() {

			$id = url::param();

			$item_information = new Iteminformation();
			$item_information->read($id);

			$model = new Model(Connector::DB_CORE);

			if (!$item_information->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$permission_edit = user::permission('can_edit_catalog');
			$permission_edit_item_information = user::permission('can_edit_item_information');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			$data = $item_information->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			$model = new Model(Connector::DB_CORE);

			$result = $model->query("
				SELECT item_information_item_item_id
				FROM item_information_items
				WHERE item_information_item_item_information_id = $id
			")->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$item = $row['item_information_item_item_id'];
					$item_information_items[$item] = true;
				}
			}

			// supplier item
			$result = $model->query("
				SELECT DISTINCT
					item_id,
					item_code,
					item_name
				FROM items
				INNER JOIN suppliers ON supplier_item = item_id
				WHERE supplier_address = $item_information->address_id
				ORDER BY item_code
			")->fetchAll();


			if ($result) {
				foreach ($result as $row) {
					
					$item = $row['item_id'];
					$items[$item]['item_code'] = $row['item_code'];
					$items[$item]['item_name'] = $row['item_name'];

					$checked = $item_information_items[$item] ? "checked=checked" : null;

					if (!$disabled) {
						$items[$item]['checkbox'] = "<input type='checkbox' value='$item' class='item_item_information' $checked />";
					} elseif($checked) {
						$items[$item]['checkbox'] = "<i class='fa fa-check-circle' ></i>";
					}
				}
			}

			// set request id to form
			$this->request->field('id', $id);

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $item_information->name);
			$this->view->companytabs('pagecontent')->navigation('tab');


			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'iteminformation/item/list.php');
			$tpl->data('data', $data);
			$tpl->data('items', $items);
			$tpl->data('buttons', $buttons);
			$this->view->setTemplate('item_informationItems', $tpl);

			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
				Compiler::attach(DIR_CSS."jquery.ui.css");
				Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
				Compiler::attach(DIR_SCRIPTS."ajaxuploader/ajaxupload.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				Compiler::attach(DIR_JS."form.validator.file.js");
		}

		public function titles() {

			$id = url::param();

			$permission_edit = user::permission('can_edit_catalog');
			$permission_edit_item_information = user::permission('can_edit_item_information');
			$permission_view = user::permission('can_browse_catalog_in_admin');

			if ($id) {

				$id = is_numeric($id) ? $id : null;

				$modul = new Modul();
				$modul->setTable('item_information_titles');
				$modul->read($id);

				if ( ($id && !$modul->id) || (!$permission_edit && !$permission_edit_item_information && !$permission_view) ) {
					Message::access_denied();
					url::redirect($this->request->query('titles'));
				}
				
				$data = $modul->data ?: array();
				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;

				if (!$id) {
					$data['redirect'] = $this->request->query('titles');
				}

				// buttons
				$buttons = array();
				$buttons['back'] = $this->request->query('titles');

				if ($permission_edit || $permission_edit_item_information) {
				
					$buttons['save'] = true;

					if ($id) {
						
						$integrity = new Integrity();
						$integrity->set($id, 'item_information_titles', 'system');

						if ($integrity->check()) {
							$buttons['delete'] = $this->request->link('/applications/modules/iteminformation/title/delete.php', array('id'=>$id));
						}
					}
				}

				if ($this->request->archived || (!$permission_edit && !$permission_edit_item_information)) {
					$fields = array_keys($data);
					$disabled = array_fill_keys($fields, true);
				}

				// header
				$this->view->header('pagecontent')
				->node('header')
				->data('title', $data['item_information_title_title']);

				// list view
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'iteminformation/title/form.php');
				$tpl->data('data', $data);
				$tpl->data('buttons', $buttons);
				$tpl->data('disabled', $disabled);
				$this->view->setTemplate('iteminformationTitle', $tpl);

				Compiler::attach(array(
					DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css",
					DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js",
					DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js",
					DIR_SCRIPTS."qtip/qtip.css",
					DIR_SCRIPTS."qtip/qtip.js",
					DIR_JS."form.validator.js"
				));

			} else {
			
				if ($permission_edit || $permission_edit_item_information) {
					$this->request->field('add',$this->request->query('titles/add'));
				}
			
				$this->request->field('data',$this->request->query('titles'));
				// list view
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'list.php');
				$tpl->data('url', '/applications/modules/iteminformation/title/list.php');
				$tpl->data('class', 'list-500');
				$this->view->setTemplate('iteminformationTitles', $tpl);

				Compiler::attach(array(
					'/public/scripts/dropdown/dropdown.css',
					'/public/scripts/dropdown/dropdown.js',
					'/public/scripts/table.loader.js'
				));
			}
		}
	}