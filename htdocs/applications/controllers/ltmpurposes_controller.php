<?php 

	class LTMpurposes_Controller {
		
		public function __construct() {
			
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() { 
			
			// request
			$requestFields = array();
			$requestFields['form'] = $this->request->query('data');
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			
			// permissions: can edit material ppurposes
			$can_edit = user::permission(Material_Purpose::PERMISSION_EDIT);
			
			if (!$this->request->archived && $can_edit) {
				$requestFields['add'] = $this->request->query('add');
			}

			// template: material purposes
			$this->view->purposes('pagecontent')
			->material('purpose.list')
			->data('requestFields', $requestFields);
		}
		
		public function add() {
			
			// permissions
			$permission_edit = user::permission(Material_Purpose::PERMISSION_EDIT);
		
			if($this->request->archived && !$permission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
				
			// form datalaoder
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			
			// buttons
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
				
			// template: material purpose
			$this->view->materialPurpose('pagecontent')
			->material('purpose.data')
			->data('data', $data)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons);
		}
		
		public function data() {
				
			$id = url::param();
			
			// permissions
			$permission_edit = user::permission(Material_Purpose::PERMISSION_EDIT);
			
			$materialPurpose = new Material_Purpose();
			$materialPurpose->read($id);
			
			// check access to material purpose
			if (!$materialPurpose->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// form dataloader
			$data = $materialPurpose->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
				
			if(!$this->request->archived && $permission_edit) {
				
				$buttons['save'] = true;
				
				$integrity = new Integrity();
				$integrity->set($id, 'mps_material_purposes');
				
				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/material.purpose.delete.php', array('id'=>$id));
				}
			}
			// disable all fields
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}

			// template: material purpose form
			$this->view->materialPurpose('pagecontent')
			->material('purpose.data')
			->data('data', $data)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}