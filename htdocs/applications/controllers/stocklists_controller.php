<?php 

	class Stocklists_Controller {
		
		public function __construct() {
			
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->translate = Translate::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			// permissions
			$permission_edit = user::permission(Stocklist::PERMISSION_EDIT);
			
			// request form: add button
			if ( !$this->request->archived && $permission_edit) {
				$field = $this->request->query('add');
				$this->request->field('add', $field);
			}
			
			// request form: edit link
			$field = $this->request->query('data');
			$this->request->field('form', $field);
			
			// request form: speradsheet link
			$field = $this->request->query('planning');
			$this->request->field('planning', $field);
			
			// template: stokliist
			$this->view->stocklist('pagecontent')
			->stock('list');
		}
		
		public function add() {
			
			// permissions
			$permission_edit = user::permission(Stocklist::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Stocklist::PERMISSION_EDIT_LIMITED);

			if( $this->request->archived || (!$permission_edit && !$permission_edit_limited) ) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// form dataloader
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
				
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
			
			$model = new Model();
			
			// dataloader: suppliers
			$suppliers = $model->query("
				SELECT DISTINCT
					address_id,
					address_company
				FROM db_retailnet.suppliers
				LEFT JOIN db_retailnet.scpps_stocklists ON scpps_stocklist_supplier_address = supplier_address
			")
			->bind(Supplier::DB_BIND_COMPANIES)
			->filter('type', '(address_type = 2 OR address_type = 8)')
			->filter('active', '(address_active = 1 OR address_id = scpps_stocklist_supplier_address)')
			->order('address_company')
			->fetchAll();
			
			$dataloader['scpps_stocklist_supplier_address'] = _array::extract($suppliers);
			
			// weeks grid
			$data['weeks'] = $this->weeks_grid($weeks);
			
			$this->view->stokListFarm('pagecontent')
			->stock('list.data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons);	
		}
		
		public function data() {
			
			$id = url::param();
			
			$stocklist = new Stocklist();
			$stocklist->read($id);
			
			// check access to stocklist
			if (!$stocklist->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// permissions
			$permission_edit = user::permission(Stocklist::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Stocklist::PERMISSION_EDIT_LIMITED);
			
			// form dataloader
			$data = $stocklist->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			// company
			$company = new Company();
			$company->read($stocklist->supplier_address);
			
			// stock list items
			$items = $stocklist->item->load(); 

			// weeks grid
			$weeks = unserialize($stocklist->weeks);
			$data['weeks'] = $this->weeks_grid($weeks);


			// edit form
			if (!$this->request->archived && ($permission_edit || $permission_edit_limited ))  {
				
				// button: save
				$buttons['save'] = true;
				
				$integrity = new Integrity();
				$integrity->set($id, 'scpps_stocklists');
				
				// button: delete
				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/stock.list.delete.php', array('id' => $id));
				}
			}
			elseif ($data) {
				foreach ($data as $field => $value) {
					$disabled[$field] = true;
				}
			}
			
			$model = new Model();
			
			$suppliers = $model->query("
				SELECT DISTINCT
					address_id,
					address_company
				FROM db_retailnet.suppliers
				LEFT JOIN db_retailnet.scpps_stocklists ON scpps_stocklist_supplier_address = supplier_address
			")
			->bind(Supplier::DB_BIND_COMPANIES)
			->filter('type', '(address_type = 2 OR address_type = 8)')
			->filter('active', '(address_active = 1 OR address_id = scpps_stocklist_supplier_address)')
			->order('address_company')
			->fetchAll();
			
			// dataloader: suppliers
			$dataloader['scpps_stocklist_supplier_address'] = _array::extract($suppliers);
			
			// disable suppliers
			$disabled['scpps_stocklist_supplier_address'] = ($items) ? true : false;
			
			// template: header
			$this->view->staff_header('pagecontent')
			->node('header')
			->data('title', $company->company.', '.$stocklist->title);
			
			// template: navigation tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			// template: stock list form
			$this->view->stocklist('pagecontent')
			->stock('list.data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function items() {
			
			$id = url::param();
			
			$stocklist = new Stocklist();
			$stocklist->read($id);
			
			// check access to stocklist
			if (!$stocklist->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// permissions
			$permission_edit = user::permission(Stocklist::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Stocklist::PERMISSION_EDIT_LIMITED);
			$can_edit = ($permission_edit || $permission_edit_limited) ? true : false;
			
			// form dataloader
			$data = $stocklist->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// db model
			$model = new Model(Connector::DB_CORE);
			
			// remove all records from stock list weeks
			// where stock week values are null
			$is_week_null = $model->db->query("
				DELETE FROM scpps_weeks
				WHERE scpps_week_stocklist_id = $id
					AND scpps_week_stock IS NULL
					AND scpps_week_not_confirmed IS NULL
					AND	scpps_week_not_confirmed_yet IS NULL
					AND	scpps_week_available_after_deduction IS NULL
			");
			
			// company
			$company = new Company();
			$company->read($stocklist->supplier_address);
			
			// stock list items
			$stocklist_items = $stocklist->item->load();
			$stocklist_items = _array::datagrid($stocklist_items, 'scpps_stocklist_item_item_id');
			
			// stock list week items
			$stocklist_week_data = $stocklist->get_weeks_data();
			$stocklist_week_items = _array::datagrid($stocklist_week_data, 'scpps_week_item');
			
			// dataloader: items
			$items = $model->query("
				SELECT DISTINCT
					item_id,
					item_code,
					item_name,
					item_active
				FROM db_retailnet.suppliers
			")
			->bind(Supplier::DB_BIND_ITEMS)
			->filter('supplier', "supplier_address = ".$stocklist->supplier_address)
			->order('item_code')
			->order('item_name')
			->fetchAll();
	
			
			if ($items) {
				
				$visible_items = 0;
				$checked_items = 0;
				
				foreach ($items as $row) {
					
					$item = $row['item_id'];
					$code = $row['item_code'];
					$name = $row['item_name'];
					
					// item active
					$is_active = $row['item_active'];
					
					// item visible by stock list
					$is_visible = ($is_active || $stocklist_items[$item]) ? true : false;
					
					// can edit
					$checkbox = ($is_active) ? true : false;
					
					// is this item in stock list weeks?
					if ($stocklist_items[$item]) {
							
						$stockweek_data = $model->query("
							SELECT scpps_week_id
							FROM scpps_weeks
							WHERE scpps_week_stocklist_id = $id AND scpps_week_item = $item
						")->fetchAll();
						
						// if item is used in weeks planning
						// disable checkbox for this item
						if ($stockweek_data) {
							$checkbox = false;
						} 
						// ii item has is not used in weeks planning
						// and is no more active, remove this item from stocklist item
						elseif(!$is_active) {
							$stocklist->item->delete($item);
							$is_visible = false;
						}
					}
				
					
					if ($is_visible) {
						
						$visible_items++;
						
						$content .= "<div class='-element -checkbox vertical' >";
						
						if ($checkbox) {
							
							$editabled[] = $item;
							
							$checked_class = null;
							$checked = ($stocklist_items[$item]) ? "checked=checked" : null;
							
							if ($checked) {
								$checked_items++;
							}
							
							// checkbox
							$content .= "<input type=checkbox class=items  name=items[$item] value=$item $checked />";
						} 
						else {
							$checked_class = '-checked';
							$checked_items++;
						}
					
						$content .= "<span class='-caption $checked_class' index='$item'>$code, $name</span>";
						$content .= "</div>";
					}
				}
			}

			if ($editabled) {
				$checkall = ($visible_items==$checked_items) ? 'checked=checked' : null;
				$select_all = "<div class='-element -checkbox vertical' >";
				$select_all .= "<input type=checkbox class=checkall  name=checkall $checkall />";
				$select_all .= "<span class='-caption selectall' >".$this->translate->select_all."</span>";
				$select_all .= "</div>";
			}
			
			$data['stocklist_items'] = $select_all.$content;
			$data['editabled_fields'] = ($editabled) ? join(',', $editabled) : null;
			
				
			// button: save
			$buttons['save'] = ($can_edit && $editabled) ? true : false;
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $company->company.', '.$stocklist->title);
			
			// template: navigation tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			// template: stocklist items
			$this->view->stocklistItems('pagecontent')
			->stock('list.items')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function additionals() {
			
			$id = url::param();
			
			$stocklist = new Stocklist();
			$stocklist->read($id);
			
			// check access to stocklist
			if (!$stocklist->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// permissions
			$permission_edit = user::permission(Stocklist::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Stocklist::PERMISSION_EDIT_LIMITED);
			$can_edit = ($permission_edit || $permission_edit_limited) ? true : false;
			
			// form dataloader
			$data = $stocklist->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// db model
			$model = new Model(Connector::DB_CORE);
			
			// company
			$company = new Company();
			$company->read($stocklist->supplier_address);
			
			// dataloader: columns
			if ($stocklist->columns) {
				$dataloader['columns'] = unserialize($stocklist->columns);
			} else {
				$dataloader['columns']['column1'] = '';
			}
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $company->company.', '.$stocklist->title);
			
			// template: navigation tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			$this->view->stocklistAdditionals('pagecontent')
			->stock('list.additional')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function planning() {
			
			$this->view->master('spreadsheet.php'); 
			
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
			Compiler::attach(DIR_SCRIPTS."spreadsheet/spreadsheet.css");
			Compiler::attach(DIR_SCRIPTS."spreadsheet/spreadsheet.js");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."uislider/redmond/redmond.css");
			Compiler::attach(DIR_SCRIPTS."uislider/slider.css");
			Compiler::attach(DIR_SCRIPTS."uislider/slider.js");
			
			// db model
			$model = new Model($this->request->application);
		
			// stock list
			$id = url::param();
			
			// number of years in future
			$futured_years = 2;
			
			// actual year
			$actuale_year = date('Y');
			
			// current year
			$current_year = url::param(1);
			$current_year = ($current_year) ? $current_year : $actuale_year;
			
			// stock list
			$stocklist = new Stocklist();
			$data = $stocklist->read($id);
			
			// is stocklist has not weeks
			if (!$stocklist->weeks) {
				url::redirect('/'.$this->request->application.'/'.$this->request->controller);
			}

			// company
			$company = new Company();
			$company->read($stocklist->supplier_address);
			
			// button: back
			$buttons['back'] = $this->request->query();;
				
			// button: save
			$buttons['save'] = ($can_edit && $editabled) ? true : false;
			
			// button: print
			$buttons['print'] = '/applications/exports/excel/stocklist.spreadsheet.php';
			
			// buttons: year
			$buttons['year'] = true;
		
			// curent year in dropdown
			$stocklist_years[$actuale_year] = $actuale_year;
			
			// get saved years
			$result = $model->query("
				SELECT DISTINCT scpps_week_year
				FROM scpps_weeks
			")
			->filter('stocklist', "scpps_week_stocklist_id = $id")
			->filter('currentyear', "scpps_week_year <> $actuale_year")
			->fetchAll();
			
			if ($result) {
				foreach ($result as $row) {
					$year = $row['scpps_week_year'];
					$stocklist_years[$year] = $year;
				}
			}
			
			// set futured years
			if ($futured_years)	{			
				for ($i = 1; $i <= $futured_years; $i++) {
					$year = $actuale_year+$i;
					$stocklist_years[$year] = $year;
				}
			}
			
			$this->view->pagetitle = $stocklist->title.', '.$company->company;
			
			// sort years
			ksort($stocklist_years);
			
			// template: stock list spreadsheet
			$this->view->stocklist('pagecontent')
			->stock('list.spreadsheet')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('application', $this->request->application)
			->data('controller', $this->request->controller)
			->data('archived', $this->request->archived)
			->data('action', $this->request->action)
			->data('id', $id)
			->data('current_year', $current_year)
			->data('stocklist_years', $stocklist_years);
		
		}
		
		public function export() {
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
				
			$model = new Model(Connector::DB_CORE);
			
			// form dataloader
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// stock weeks years
			$result = $model->query("
				SELECT DISTINCT 
					scpps_week_year, scpps_week_year AS year 
				FROM scpps_weeks		
			")->fetchAll();
			
			// dataloader: years
			$dataloader['scpps_week_year'] = _array::extract($result);
			
			$model->query("
				SELECT DISTINCT 
					address_id, address_company
				FROM scpps_stocklists
			")
			->bind(Stocklist::DB_BIND_COMPANIES)
			->bind(Stocklist::DB_BIND_STOCKLIST_WEEKS)
			->order('address_company');
			
			if ($this->user->permission(Stocklist::PERMISSION_EDIT_LIMITED)) {
				$model->filter('limited', "supplier_address = ".$this->user->address);
			}
			
			$suppliers = $model->fetchAll();
			
			if ($suppliers) {
				$dataloader['suppliers'] = _array::extract($suppliers);
				$buttons['print'] = '/applications/exports/excel/stock.list.week.export.php';
				$hidden['no_result'] = true;
			} else {
				$hidden['scpps_week_year'] = true;
				$hidden['suppliers'] = true;
				$hidden['select_all'] = true;
				$data['no_result'] = $this->translate->nomatch;
			}
			
			$this->view->stocklistValues('pagecontent')
			->stock('list.export')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('application', $this->request->application)
			->data('controller', $this->request->controller)
			->data('archived', $this->request->archived)
			->data('action', $this->request->action);
		}
		
		protected function weeks_grid($weeks=array()) {
			
			// current year
			$current_year = date('Y'); 
			
			// get all Mondays of year
			$weeks_of_year = date::year_mondays($current_year); 
			
			// group weeks by months
			$weeks_of_month = date::month_weeks($current_year); 
			
			// set checkbox for weeks
			for ($week = 1; $week < count($weeks_of_year); $week++) {
			
				// month number
				$month = date::month_number_from_week($week, $current_year);
				
				// working days in week
				$weekRange = $this->translate->week." $week &nbsp;&nbsp;<i>".date::week_range($week, $current_year)."</i>";
			
				// checked month
				$checked = ($weeks && in_array($week, $weeks)) ? 'checked=checked' : null;
				
				if ($checked) {
					$checked_weeks[$month][] = $week;
				}
			
				$months_weeks[$month] .= "<span class='-week'><input type=checkbox class=weekbox name=weeks[$week] value=$week $checked /> $weekRange</span>";
			}
			
			for ($month = 1; $month <= 12; $month++) {
			
				// month name in English
				$month_name = date::month_name($month);
				
				// month checked
				$checked = (count($checked_weeks[$month]) == count($weeks_of_month[$month])) ? "checked=checked" : null;
				
				// month weeks
				$month_weeks = $months_weeks[$month];
			
				$content[$month] = "
					<div class='-row'>
						<span class=-month ><input type=checkbox class=monthbox name=months[$month] value=$month $checked />&nbsp;$month_name</span>
						$month_weeks
					</div>
				";
				
				if ($checked) {
					$checked_months++;
				}
			}
			
			if ($content) {
				$checkall = ($checked_months == 12) ? 'checked=checked' : null;
				$content = "<div class=-selectall ><input type=checkbox name=checkall class=selectall $checkall /> ".$this->translate->select_all."</div>".join($content);
			}
			
			return $content;
		}
	}
	