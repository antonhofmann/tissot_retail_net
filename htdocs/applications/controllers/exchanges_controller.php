<?php 

	class Exchanges_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			$this->request->application = url::application();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');	
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			url::redirect($this->request->query('simac'));
		}
		
		public function simac() {

			$application = $this->request->application;

			switch ($application) {
				
				case 'mps':
					$file = '/cronjobs/exchange.material.php';
				break;

				
				case 'lps':
					$file = '/cronjobs/exchange.references.php';
				break;
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			
			
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			$buttons['save'] = true;
			
			// template: import form
			$this->view->filetypes('pagecontent')
			->exchange('simac')
			->data('data', $data)
			->data('file', $file)
			->data('buttons', $buttons);
		}

	}