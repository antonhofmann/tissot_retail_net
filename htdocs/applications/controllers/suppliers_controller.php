<?php 

class Suppliers_Controller {
	
	public function __construct() {
			
		$this->user = User::instance();
		$this->request = request::instance();
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}
	
	public function index() { 
        
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		$this->request->field('form', $this->request->query('regions'));

		// list view
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'list.php');
		$tpl->data('url', '/applications/modules/supplier/list.php');
		$tpl->data('class', 'list-1000');
		$this->view->setTemplate('suppliers', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js'
		));
	}

	public function regions() {

		$id = url::param();

		$company = new Company();
		$company->read($id);

		$permission_edit = user::permission('can_edit_catalog');
		$permission_edit_items = user::permission('can_edit_items_in_catalogue');
		$permission_view = user::permission('can_browse_catalog_in_admin');
		$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

		if (!$company->id || (!$_CAN_EDIT && !$permission_view)) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		$model = new Model(Connector::DB_CORE);

		Compiler::attach(DIR_JS."supplier.regions.js");
		
		$data = array();
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;

		// buttons
		$buttons = array();
		$buttons['back'] = $this->request->query();

		// get supplier items
		$result = $model->query("
			SELECT COUNT(DISTINCT item_id) AS totalItems
			FROM suppliers
			INNER JOIN items ON item_id = supplier_item
			WHERE item_active = 1 AND supplier_address = $company->id
		")->fetch();

		$totalSupplierItems = $result['totalItems'];

		// get countries
		$result = $model->query("
			SELECT DISTINCT
				country_id,
				region_id,
				country_name,
				region_name
			FROM countries
			INNER JOIN regions ON region_id = country_region
			ORDER BY region_name, country_name
		")->fetchAll();	

		$datagrid = array();

		if ($result) {
			foreach ($result as $row) {
				$region = $row['region_id'];
				$country = $row['country_id'];
				$datagrid[$region]['region'] = $row['region_name'];
				$datagrid[$region]['countries'][$country] = $row['country_name'];
			}
		}

		// data: countries
		$result = $model->query("
			SELECT DISTINCT item_country_item_id, country_region, item_country_country_id
			FROM item_countries
			INNER JOIN items ON item_id = item_country_item_id
			INNER JOIN suppliers ON supplier_item = item_id
			INNER JOIN countries ON country_id = item_country_country_id
			WHERE item_active = 1 AND supplier_address = $company->id
		")->fetchAll();

		if ($result) {

			$group = array();
			
			foreach ($result as $row) {
				$region = $row['country_region'];
				$country = $row['item_country_country_id'];
				$group[$region][$country][] = $row['item_country_item_id'];
			}

			foreach ($group as $region => $countries) {
				foreach ($countries as $country => $items) {
					$selected[$region][$country] = $totalSupplierItems == count($items) ? true : false;
				}
			}
		}

		// header
		$this->view->header('pagecontent')->node('header')
		->data('title', $company->company)
		->data('subtitle', 'Please indicate the supplied region for all active items');
		
		$this->view->companytabs('pagecontent')->navigation('tab');

		if (!$_CAN_EDIT) {
			$disabled['regions'] = true;
		}

		// template: form
		$this->view->itemRegions('pagecontent')
		->item('regions')
		->data('id', $id)
		->data('data', $data)
		->data('selected', $selected)
		->data('disabled', $disabled)
		->data('datagrid', $datagrid)
		->data('buttons', $buttons);
	}
}