<?php

	class Pos_Controller {

		public function __construct() {

			$this->request = request::instance();
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}

		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."pop/pop.css");
			Compiler::attach(DIR_SCRIPTS."pop/pop.js");
			
			// permission
			$permission_edit = user::permission(Pos::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Pos::PERMISSION_EDIT_LIMITED);

			if (!$this->request->archived && (user::permission(Pos::PERMISSION_EDIT) || user::permission(Pos::PERMISSION_EDIT_LIMITED)) ) {
				$field = $this->request->query('add');
				$this->request->field('add', $field);
			}

			// button: edit company
			$field = $this->request->query('address');
			$this->request->field('form', $field);

			// button: pos sheet
			$field = $this->request->link('/applications/exports/pdf/pos.php');;;
			$this->request->field('pdf', $field);

			// button: pos excel sheet
			$field = $this->request->link('/applications/exports/excel/pos.list.php');
			$this->request->field('printlist', $field);
			
			// button: pos excel sheet
			$field = $this->request->link('/applications/exports/excel/pos.list.detail.php');
			$this->request->field('printdetails', $field);

			// button: pos furnitures
			$field = $this->request->link('/applications/exports/excel/pos.list.furniture.php');
			$this->request->field('printfurnite', $field);

			// button: pos material
			$field = $this->request->link('/applications/exports/excel/pos.list.material.php');
			$this->request->field('printmaterials', $field);
			
			// include template
			$this->view->posindex('pagecontent')->pos('list');
			
			// remove company referer
			session::remove('pos_referer');
		}

		public function add() {
			
			// permissions
			$permission_edit = user::permission(Pos::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Pos::PERMISSION_EDIT_LIMITED);

			if ($this->request->archived || (!$permission_edit && !$permission_edit_limited) ) {
				message::access_denied();
				url::redirect($this->request->query());
			}

			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
			Compiler::attach(DIR_CSS."jquery.ui.css");
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			

			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
			
			// create pos from company
			$company_id = url::param();

			if ($company_id) {

				$company = new Company();
				$company->read($company_id);

				$data = array(
					'add_from_company' => $company->id,
					'posaddress_client_id' => $company->parent,
					'posaddress_franchisee_id' => $company->id,
					'posaddress_identical_to_address' => 1,
					'posaddress_name' => $company->company,
					'posaddress_name2' => $company->company2,
					'posaddress_address' => $company->address,
					'posaddress_street' => $company->street,
					'posaddress_street_number' => $company->streetnumber,
					'posaddress_address2' => $company->address2,
					'posaddress_country' => $company->country,
					'posaddress_place_id' => $company->place_id,
					'posaddress_zip' => $company->zip,
					'posaddress_phone' => $company->phone,
					'posaddress_phone_country' => $company->phone_country,
					'posaddress_phone_area' => $company->phone_area,
					'posaddress_phone_number' => $company->phone_number,
					'posaddress_mobile_phone' => $company->mobile_phone,
					'posaddress_mobile_phone_country' => $company->mobile_phone_country,
					'posaddress_mobile_phone_area' => $company->mobile_phone_area,
					'posaddress_mobile_phone_number' => $company->mobile_phone_number,
					'posaddress_email' => $company->email,
					'posaddress_website' => $company->website,
					'posaddress_contact_name' => $company->contact_name,
					'posaddress_contact_email' => $company->contact_email
				);
			}

			$data['redirect'] = $this->request->query('address');
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;

			$model = new Model(Connector::DB_CORE);

			// dataloader: distribution channels
			if ($permission_edit) {

				$result = $model->query("
					SELECT DISTINCT
					mps_distchannel_id, 
					CONCAT(mps_distchannel_group_name, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) AS channel
				FROM mps_distchannels
				INNER JOIN mps_distchannel_groups ON mps_distchannels.mps_distchannel_group_id = mps_distchannel_groups.mps_distchannel_group_id
				WHERE mps_distchannel_active = 1
				ORDER BY 
					mps_distchannel_group_name,
					mps_distchannels.mps_distchannel_name,
					mps_distchannels.mps_distchannel_code	
				")->fetchAll();

				$dataloader['posaddress_distribution_channel'] = _array::extract($result);

			} else {
				
				$result = $model->query("
					SELECT DISTINCT
						mps_distchannels.mps_distchannel_id,
						CONCAT(mps_distchannel_group_name, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) AS distributionchannel
					FROM mps_distchannels
					LEFT JOIN mps_distchannel_groups ON mps_distchannel_groups.mps_distchannel_group_id = mps_distchannels.mps_distchannel_group_id
					WHERE
						mps_distchannel_selectable_by_clients_in_mps = 1
					ORDER BY
						mps_distchannel_group_name,
						mps_distchannels.mps_distchannel_name,
						mps_distchannels.mps_distchannel_code		
				")->fetchAll();
		
				
				$dataloader['posaddress_distribution_channel'] = _array::extract($result);
			}

			// dataloader: countries
			$dataloader['posaddress_country'] = country::loader();

			if (!$permission_edit && $permission_edit_limited) {
				$user = user::instance();
				$clinets_filter[] = '( address_id = '.$user->address.' OR address_parent = '.$user->address.' )';
			}

			// dataloader: clients
			$clinets_filter[] = 'address_type = 1';
			$dataloader['posaddress_client_id'] = Company::loader($clinets_filter);

			// template: pos form
			$this->view->posindex('pagecontent')
			->pos('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons);
		}

		public function address() {
			
			$id = url::param();
			
			// get pos data
			$pos = new Pos();
			$pos->read($id);
			
			// check access to pos
			if (!$pos->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			elseif (!$this->request->archived && ($pos->store_closingdate && $pos->store_closingdate <> '0000-00-00')) {
				url::redirect($this->request->query("archived/address/$id"));
			}
			
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
			Compiler::attach(DIR_CSS."jquery.ui.css");
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			
			// permissions
			$permission_edit = user::permission(Pos::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Pos::PERMISSION_EDIT_LIMITED);
			$permission_view = user::permission('can_view_posindex');
			$permission_view_limited = user::permission('can_view_his_posindex');
			$permission_administrate = user::permission('can_administrate_posindex');

			$model = new Model(Connector::DB_CORE);

			// can edit pos
			$canEditPos = $pos->canEdit();

			// form data
			$data = $pos->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			//echo "<pre>"; print_r($data); die;
			

			$model = new Model();

			$result = $model->query("
				SELECT 
					IF (
						posaddresses.user_created IS NOT NULL,
						CONCAT(creators.user_firstname, ' ', creators.user_name, ', ', DATE_FORMAT(posaddresses.date_created, '%d.%m.%Y %H:%i')),
				        DATE_FORMAT(posaddresses.date_created, '%d.%m.%Y %H:%i')
					) AS created_by,
				    IF (
						posaddresses.user_modified IS NOT NULL,
				        CONCAT(modified.user_firstname, ' ', modified.user_name, ', ', DATE_FORMAT(posaddresses.date_modified, '%d.%m.%Y %H:%i')),
				        DATE_FORMAT(posaddresses.date_modified, '%d.%m.%Y %H:%i')
					) AS modified_by
				FROM db_retailnet.posaddresses
				LEFT JOIN db_retailnet.users AS creators ON creators.user_login = posaddresses.user_created
				LEFT JOIN db_retailnet.users AS modified ON modified.user_login = posaddresses.user_modified
				WHERE posaddress_id = $id
			")->fetch();

			$data['created_by'] = $result['created_by'];
			$data['modified_by'] = $result['modified_by'];

			$data['posaddress_phone'] = $data['posaddress_phone'];
			$data['posaddress_mobile_phone'] = $data['posaddress_mobile_phone'];

			// disable all fields
			$fields = array_keys($data);
			$disabled = array_fill_keys($fields, true);
			
			// buttona
			$buttons = array();
			
			// button: back
			$referer = session::referer('back');
			$back = session::get('pos_referer');
			
			if (!$back) {
				session::set('pos_referer', $referer);
				$back = $referer;
			}
			
			$back = ($back) ? $back : $this->request->query();
			$buttons['back'] = $back;

			
			// pos type is not independent retailer
			if ($pos->store_postype <> 4) {
				$hidden['posaddress_identical_to_address'] = true;
			}

			// pos is independent retailer
			if (!$this->request->archived) {

				if ($canEditPos) {
					
					$buttons['save'] = true;

					if ($pos->store_postype==4) {
						$disabled = false;	
					} else {
						$disabled["posaddress_distribution_channel"] = false;
						$disabled['posaddress_name2'] = false;
						$disabled['posaddress_address'] = false;
						$disabled['posaddress_address2'] = false;
						$disabled['posaddress_zip'] = false;
						$disabled['posaddress_sapnumber'] = false;
						$disabled['posaddress_sap_shipto_number'] = false;
						$disabled['posaddress_export_to_web'] = false;
						$disabled['posaddress_email_on_web'] = false;
						$disabled['posaddress_offers_free_battery'] = false;
						$disabled['posaddress_store_closingdate'] = false;
						$disabled['posaddress_store_planned_closingdate'] = false;
						$disabled['posaddress_phone'] = false;
						$disabled['posaddress_mobile_phone'] = false;
						$disabled['posaddress_email'] = false;
						$disabled['posaddress_website'] = false;
						$disabled['posaddress_contact_name'] = false;
						$disabled['posaddress_contact_email'] = false;
					}
				}

			} elseif ($canEditPos) {
				$buttons['save'] = true;
				$disabled['posaddress_store_closingdate'] = false;
			}

			// dataloader: distribution channels
			if ($permission_edit) {

				$result = $model->query("
					SELECT DISTINCT
					mps_distchannel_id, 
					CONCAT(mps_distchannel_group_name, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) AS channel
				FROM mps_distchannels
				INNER JOIN mps_distchannel_groups ON mps_distchannels.mps_distchannel_group_id = mps_distchannel_groups.mps_distchannel_group_id
				WHERE mps_distchannel_active = 1 OR mps_distchannel_id = '$pos->distribution_channel'
				ORDER BY 
					mps_distchannel_group_name,
					mps_distchannels.mps_distchannel_name,
					mps_distchannels.mps_distchannel_code	
				")->fetchAll();

				$dataloader['posaddress_distribution_channel'] = _array::extract($result);

			} else {
				
				$result = $model->query("
					SELECT DISTINCT
						mps_distchannels.mps_distchannel_id,
						CONCAT(mps_distchannel_group_name, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) AS distributionchannel
					FROM mps_distchannels
					LEFT JOIN mps_distchannel_groups ON mps_distchannel_groups.mps_distchannel_group_id = mps_distchannels.mps_distchannel_group_id
					LEFT JOIN posdistributionchannels ON mps_distchannels.mps_distchannel_id = posdistributionchannels.posdistributionchannel_dchannel_id
					WHERE
						(
							posdistributionchannels.posdistributionchannel_postype_id = '$pos->store_postype'
							AND (
								posdistributionchannels.posdistributionchannel_possubclass_id = '$pos->store_subclass'
								OR posdistributionchannels.posdistributionchannel_possubclass_id = 0
							)
							AND posdistributionchannels.posdistributionchannel_legaltype_id = '$pos->ownertype'
							AND mps_distchannel_active = 1
						)
						OR mps_distchannels.mps_distchannel_id = '$pos->distribution_channel'
						OR (
							posdistributionchannels.posdistributionchannel_legaltype_id = '$pos->ownertype'
							AND posdistributionchannels.posdistributionchannel_postype_id = '$pos->store_postype'
							AND posdistributionchannel_assignable_by_user = 1
							AND mps_distchannel_active = 1
						)
					ORDER BY
						mps_distchannel_group_name,
						mps_distchannels.mps_distchannel_name,
						mps_distchannels.mps_distchannel_code		
				")->fetchAll();
				
				
				$dataloader['posaddress_distribution_channel'] = _array::extract($result);
			}

			// dataloader: countries
			$dataloader['posaddress_country'] = country::loader();
			
			// dataloader: provinces
			$dataloader['posaddress_province_id'] = province::loader(array(
				'province_country' => $pos->country		
			));
			
			// dataloader: places
			$dataloader['posaddress_place_id'] = place::loader(array(
				'place_country' => $pos->country,
				'place_province' => $pos->province_id
			));
			
			if (!$permission_edit && $permission_edit_limited) {
				$user = user::instance();
				$clinets_filter[] = '( address_id = '.$user->address.' OR address_id = '.$pos->client_id.' OR address_parent = '.$user->address.' )';
			}

			// dataloader: clients
			$clinets_filter[] = 'address_type = 1';
			$dataloader['posaddress_client_id'] = Company::loader($clinets_filter);
			
			// planned closing date
			if ($data['posaddress_store_planned_closingdate']) {
				if ($data['posaddress_store_planned_closingdate']=='0000-00-00') {
					$data['posaddress_store_planned_closingdate'] = null;
				} else $data['posaddress_store_planned_closingdate'] = date::system($data['posaddress_store_planned_closingdate']);
			}
			
			// dataloader: owners
			if ($disabled['posaddress_franchisee_id']) {
				$company = new Company();
				$company->read($pos->franchisee_id);
				$dataloader['posaddress_franchisee_id'][$pos->franchisee_id] = $company->header();
			}


			if ($permission_edit || $permission_edit_limited || $permission_view || $permission_view_limited || $permission_administrate) {
				$buttons['accessPosIndex'] = "/pos/posindex_pos.php?pos_id=$id&country=$pos->country";
			}

			// special checkboxes
			$canSetVipService = User::permission('can_set_vip_service_checkbox');
			$canSetBirthdayVaucher = User::permission('can_set_birthday_vaucher_checkbox');
			$canSetCollectorCard = User::permission('can_set_collector_card_checkbox');
			

			if ($canSetVipService) {
				$disabled['posaddress_vipservice_on_web'] = false;
				$hidden['posaddress_vipservice_on_web'] = false;
				$buttons['save'] = true;
			}
			else {
				$disabled['posaddress_vipservice_on_web'] = true;
			}


			if ($canSetBirthdayVaucher) {
				$disabled['posaddress_birthday_vaucher_on_web'] = true;
				$hidden['posaddress_birthday_vaucher_on_web'] = true;
				$buttons['save'] = true;
			}
			else {
				$disabled['posaddress_birthday_vaucher_on_web'] = true;
				$hidden['posaddress_birthday_vaucher_on_web'] = true;
			}

			if ($canSetCollectorCard) {
				$disabled['posaddress_show_in_club_jublee_stores'] = false;
				$hidden['posaddress_show_in_club_jublee_stores'] = false;
				$buttons['save'] = true;
			}
			else {
				$disabled['posaddress_show_in_club_jublee_stores'] = true;
			}
			

			// template: header
			$this->view->header('pagecontent')->node('header')->data('title', $pos->header());
			$this->view->tabs('pagecontent')->navigation('tab');

			// template: pos form
			$this->view->posForm('pagecontent')
			->pos('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}

		public function store() {

			$id = url::param();
			
			$pos = new Pos();
			$pos->read($id);
			
			// check access to pos
			if (!$pos->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			elseif (!$this->request->archived && ($pos->store_closingdate && $pos->store_closingdate <> '0000-00-00')) {
				url::redirect($this->request->query("archived/store/$id"));
			}
			
			Compiler::attach(array(
				DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css",
				DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js",
				DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js",
				DIR_SCRIPTS."qtip/qtip.css",
				DIR_SCRIPTS."qtip/qtip.js",
				DIR_SCRIPTS."chain/chained.select.js",
				DIR_SCRIPTS."raty/raty.js",
				"/public/js/pos.store.js"
			));


			// can edit pos
			$canEditPos = $pos->canEdit();
			
			// db model
			$model = new Model(Connector::DB_CORE);

			// form dataloader
			$data = $pos->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;

			// button
			$buttons = array();
			
			// button: back
			$back = session::get('pos_referer');
			$back = ($back) ? $back : $this->request->query();
			$buttons['back'] = $back;

			// dataloader: posareas
			$result = $model->query("
				SELECT posarea_area
				FROM posareas
				WHERE posarea_posaddress = $id
			")->fetchAll();
			
			if ($result) {
				
				foreach ($result as $row) {
					$locations[] = $row['posarea_area'];
				}

				$data['locations'] = serialize($locations);
			}

			if ($this->request->archived) $readonly = true;
			else $readonly = $pos->store_postype==4 && $canEditPos ? false : true;


			// disable all fields
			$fields = array_keys($data);
			$disabled = array_fill_keys($fields, true);
			$disabled['locations'] = true;

			if (!$this->request->archived && $canEditPos) {
				
				if ($pos->store_postype == 4) {
					$disabled['locations'] = false;
					$disabled['posaddress_perc_class'] = false;
					$disabled['posaddress_perc_tourist'] = false;
					$disabled['posaddress_perc_transport'] = false; 
					$disabled['posaddress_perc_people'] = false; 
					$disabled['posaddress_perc_parking'] = false; 
					$disabled['posaddress_perc_visibility1'] = false;
					$disabled['posaddress_perc_visibility2'] = false;
					$disabled['posaddress_store_subclass'] = false;
					$disabled['posaddress_store_floor'] = false;
					$buttons['save'] = true;
				}
			}
			
			// dataloader: pos types
			$dataloader['posaddress_store_postype'] = Pos_Type::loader();

			// dataloader: product types
			$dataloader['posaddress_store_furniture'] = Product_Line::loader(array(
				'product_line_posindex = 1'
			));

			// dataloader: product line subclasses
			$dataloader['posaddress_store_furniture_subclass'] = Product_Line_Subclass::loader(array(
				"productline_subclass_productline = $pos->store_furniture",
				"(productline_subclass_active = 1 OR productline_subclass_id = '$pos->store_furniture_subclass' )"
			));

			// pos area locations
			$result = $model->query("
				SELECT posareatype_id, posareatype_name
				FROM posareatypes
				ORDER BY posareatype_name		
			")->fetchAll();
			
			if ($result) {
				foreach ($result as $row) {
					$i = $row['posareatype_id'];
					$dataloader['locations'][$i] = array(
						'caption' => $row['posareatype_name']
					);
				}
			}

			// legal type
			$result = $model->query("
				SELECT posowner_type_id, posowner_type_name
				FROM posowner_types
				ORDER BY posowner_type_name
			")->fetchAll();

			$dataloader['posaddress_ownertype'] = _array::extract($result);
			$disabled['posaddress_ownertype'] = true;

			// owner company
			$result = $model->query("
				SELECT CONCAT(country_name, ': ', address_company) as caption
				FROM addresses
				INNER JOIN countries ON country_id = address_country
				WHERE address_id = $pos->franchisee_id
			")->fetch();

			$dataloader['posaddress_franchisee_id'][$pos->franchisee_id] = $result['caption'];
			$disabled['posaddress_franchisee_id'] = true;
			
			// dataloader: pos subclass
			$result = $model->query("
				SELECT possubclass_id, possubclass_name 
				FROM possubclasses 
				WHERE possubclass_selectable_in_mps = 1 OR possubclass_id = '{$data[posaddress_store_subclass]}'
				ORDER BY possubclass_name
			")->fetchAll();
				
			$dataloader['posaddress_store_subclass'] = _array::extract($result);


			// template: header
			$this->view->header('pagecontent')->node('header')->data('title', $pos->header());
			$this->view->tabs('pagecontent')->navigation('tab');

			// template: form
			$this->view->posform('pagecontent')
			->pos('store.data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('readonly', $readonly)
			->data('id', $id);
		}

		public function sales() {

			$id = url::param();

			$pos = new Pos();
			$pos->read($id);
			
			// check access to pos
			if (!$pos->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			elseif (!$this->request->archived && ($pos->store_closingdate && $pos->store_closingdate <> '0000-00-00')) {
				url::redirect($this->request->query("archived/sales/$id"));
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");

			// can edit pos
			$canEditPos = $pos->canEdit();

			// get company data
			$company = new Company();
			$company->read($pos->franchisee_id);

			// form dataloader
			$data = $pos->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;

			// disable all fields
			$fields = array_keys($data);
			$disabled = array_fill_keys($fields, true);

			// button: back
			$back = session::get('pos_referer');
			$back = ($back) ? $back : $this->request->query();
			$buttons['back'] = $back;

			if (!$this->request->archived && $canEditPos ) {
				
				$buttons['save'] = true;
				
				$disabled['posaddress_sales_representative'] = false; 
				$disabled['posaddress_decoration_person'] = false;
				$disabled['posaddress_turnoverclass_watches'] = false;
				$disabled['posaddress_turnoverclass_bijoux'] = false;
				$disabled['posaddress_turnover_type'] = false; 
				$disabled['posaddress_sales_classification01'] = false; 
				$disabled['posaddress_sales_classification02'] = false; 
				$disabled['posaddress_sales_classification03'] = false; 
				$disabled['posaddress_sales_classification04'] = false; 
				$disabled['posaddress_sales_classification05'] = false;
				$disabled['posaddress_selling_bijoux'] = false; 
				$disabled['posaddress_max_watches'] = false; 
				$disabled['posaddress_max_bijoux'] = false; 

				if ($company->type==7 AND $pos->store_postype==4) {
					$disabled['posaddress_overall_sqms'] = false;
					$disabled['posaddress_dedicated_sqms_wall'] = false;
					$disabled['posaddress_dedicated_sqms_free'] = false;
				}
			}

			// hidden fields
			if ($company->type==7 AND $pos->store_postype==4) {				
				$hidden['posaddress_store_totalsurface'] = true;
				$hidden['posaddress_store_backoffice'] = true;
				$hidden['posaddress_store_retailarea'] = true;
				$hidden['posaddress_store_numfloors'] = true;
				$hidden['posaddress_store_floorsurface1'] = true;
				$hidden['posaddress_store_floorsurface2'] = true;
				$hidden['posaddress_store_floorsurface3'] = true;
			}
			else {
				$hidden['posaddress_overall_sqms'] = true;
				$hidden['posaddress_dedicated_sqms_wall'] = true;
				$hidden['posaddress_dedicated_sqms_free'] = true;
			}

			// dataloader: sales representative
			$dataloader['posaddress_sales_representative'] = Staff::loader($this->request->application, array(
				'mps_staff_staff_type_id = 1',
				'mps_staff_address_id = '.$pos->client_id
			));

			// dataloader: decoration persons
			$dataloader['posaddress_decoration_person'] = Staff::loader($this->request->application, array(
				'mps_staff_staff_type_id = 2',
				'mps_staff_address_id = '.$pos->client_id
			));

			// dataloader: turnover class watches
			$dataloader['posaddress_turnoverclass_watches'] = TurnoverClass::loader($this->request->application, array(
				'mps_turnoverclass_group_id = 1'
			));

			// dataloader: turnover class watches
			$dataloader['posaddress_turnoverclass_bijoux'] = TurnoverClass::loader($this->request->application, array(
				'mps_turnoverclass_group_id = 2'
			));

			// template: header
			$this->view->posheader('pagecontent')->node('header')->data('title', $pos->header());
			$this->view->tabs('pagecontent')->navigation('tab');

			// template: form
			$this->view->posform('pagecontent')
			->pos('sale.data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}

		public function materials() {

			$id = url::param();
			$param = url::param(1);

			$pos = new Pos();
			$pos->read($id);
			
			// check access to pos
			if (!$pos->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			elseif (!$this->request->archived && ($pos->store_closingdate && $pos->store_closingdate <> '0000-00-00')) {
				url::redirect($this->request->query("archived/materials/$id"));
			}

			// can edit pos
			$canEditPos = $pos->canEdit();

			// template: header
			$this->view->header('pagecontent')->node('header')->data('title', $pos->header());
			$this->view->tabs('pagecontent')->navigation('tab');

			if ($param) {

				$param = (is_numeric($param)) ? $param : null;
				
				// button back
				$buttons['back'] = $this->request->query($this->request->action.'/'.$id);
				
				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");

				if ($param) {

					$pos_material = new Pos_Material();
					$data = $pos_material->read($param);
					$data['mps_material_category_id'] = $pos_material->material()->material_category_id;

				} else {
					$data['redirect'] = $this->request->query($this->request->action.'/'.$id);
				}

				$data['mps_pos_material_posaddress'] = $id;
				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;
				$data['action'] = $this->request->action;

				if ($this->request->archived || !$canEditPos) {
					$fields = array_keys($data);
					$disabled = array_fill_keys($fields, true);
					$disabled['mps_material_category_name'] = true;
				}

				// buttons
				if( !$this->request->archived && $canEditPos) {
					
					$buttons['save'] = true;

					if ($param) {
						$integrity = new Integrity();
						$integrity->set($param, 'mps_pos_materials');
						$buttons['delete'] = $integrity->check() ? $this->request->link('/applications/helpers/pos.material.delete.php', array('pos'=>$id,'id'=>$param)) : false;
					}
				}

				// dataloader material categories
				$dataloader['mps_material_category_id'] = Material_Category::loader($this->request->application, array(
					'mps_material_category_active=1'
				));

				// template: material form
				$this->view->materialForm('pagecontent')
				->pos('material.data')
				->data('data', $data)
				->data('dataloader', $dataloader)
				->data('disabled', $disabled)
				->data('hidden', $hidden)
				->data('buttons', $buttons)
				->data('id', $id)
				->data('material', $param);
			}
			else {
				
				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
				Compiler::attach(DIR_SCRIPTS."table.loader.js");
				
				// button: back
				$back = session::get('pos_referer');
				$back = ($back) ? $back : $this->request->query();
				$buttons['back'] = $back;
				
				$requestFields = array();
				$requestFields['id'] = $id; 
				$requestFields['form'] = $this->request->query($this->request->action.'/'.$id);

				if (!$this->request->archived && $canEditPos ) {
					$requestFields['add'] = $this->request->query($this->request->action.'/'.$id.'/add');
				}

				// template: material list
				$this->view->materialList('pagecontent')
				->pos('material.list')
				->data('buttons', $buttons)
				->data('requestFields', $requestFields)
				->data('id', $id);
			}
		}

		public function furniture() {

			$id = url::param();
			$param = url::param(1);
			$section = url::param(2);

			$pos = new Pos();
			$pos->read($id);

			// check access to pos
			if (!$pos->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			elseif (!$this->request->archived && ($pos->store_closingdate && $pos->store_closingdate <> '0000-00-00')) {
				url::redirect($this->request->query("archived/furniture/$id"));
			}

			// can edit pos
			$canEditPos = $pos->canEdit();

			$model = new Model(Connector::DB_CORE);

			// template: header
			$this->view->header('pagecontent')->node('header')->data('title', $pos->header());
			$this->view->tabs('pagecontent')->navigation('tab');

			if ($param) {

				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				
				$param = (is_numeric($param)) ? $param : null;

				if ($param) {
					
					$pos_furniture = new Pos_Furniture();
					$data = $pos_furniture->read($param);
					
					$section = ($pos_furniture->item_id) ? 'standard' : 'customized';

					// check is item old
					$result = $model->query("
						SELECT product_line_id
						FROM projects
						LEFT JOIN order_items ON order_item_order = project_order
						LEFT JOIN items ON item_id = order_item_item
						LEFT JOIN product_lines ON product_line_id = project_product_line
						WHERE item_id = {$data[mps_pos_furniture_item_id]}
					")->fetch();

					if (!$result['product_line_id']) {
						$data['old_items'] = $data['mps_pos_furniture_item_id'];
						$data['mps_pos_furniture_item_id'] = 'new';
					}
					
				} else {
					$data['redirect'] = $this->request->query($this->request->action.'/'.$id);
				}

				// button back
				$buttons['back'] = $this->request->query($this->request->action.'/'.$id);

				if( !$this->request->archived && $canEditPos) {

					$buttons['save'] = true;

					if ($param) {

						if ($section=='standard') {
							$integrity = new Integrity();
							$integrity->set($param, 'mps_pos_furniture');
							$canBeDeleted = $integrity->check();
						} else {
							$canBeDeleted = true;
						}

						if ($canBeDeleted) {
							$buttons['delete'] = $this->request->link('/applications/helpers/pos.furniture.delete.php', array(
								'pos' => $id,
								'id' => $param
							));
						}
					}

				}

				$result = $model->query("
					select distinct project_product_line, product_line_name
					from projects
					left join order_items on order_item_order = project_order
					left join items on item_id = order_item_item
					left join product_lines on product_line_id = project_product_line
					where item_addable_in_mps = 1
					order by product_line_name
				")->fetchAll();

				// dataloader: product types
				$dataloader['mps_pos_furniture_product_line_id'] = _array::extract($result);

				$data['mps_pos_furniture_posaddress'] = $id;
				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;
				$data['action'] = $this->request->action;

				if ($this->request->archived || !$canEditPos) {
					$fields = array_keys($data);
					$disabled = array_fill_keys($fields, true);
				}

				// template: furniture form
				$this->view->furinureForm('pagecontent')
				->pos('furniture.data')
				->data('data', $data)
				->data('dataloader', $dataloader)
				->data('disabled', $disabled)
				->data('hidden', $hidden)
				->data('buttons', $buttons)
				->data('pos', $id)
				->data('id', $param)
				->data('section', $section);
			}
			else {
				
				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
				Compiler::attach(DIR_SCRIPTS."table.loader.js");
				
				// button: back
				$back = session::get('pos_referer');
				$back = ($back) ? $back : $this->request->query();
				$buttons['back'] = $back;
				
				$requestFields = array();
				$requestFields['id'] = $id;
				$requestFields['form'] = $this->request->query($this->request->action.'/'.$id);

				if ( !$this->request->archived && $canEditPos ) {
					$requestFields['add-standard'] = $this->request->query($this->request->action."/$id/add/standard");
					$requestFields['add-customized'] = $this->request->query($this->request->action."/$id/add/customized");
				}

				// template furniture form
				$this->view->furnitureList('pagecontent')
				->pos('furniture.list')
				->data('buttons', $buttons)
				->data('requestFields', $requestFields)
				->data('id', $id);
			}
		}
		
		public function files() {

			$id = url::param();
			$param = url::param(1);

			$pos = new Pos();
			$pos->read($id);
			
			// check access to pos
			if (!$pos->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			elseif (!$this->request->archived && ($pos->store_closingdate && $pos->store_closingdate <> '0000-00-00')) {
				url::redirect($this->request->query("archived/files/$id"));
			}

			// can edit pos
			$canEditPos = $pos->canEdit();

			// template: header
			$this->view->posheader('pagecontent')->node('header')->data('title', $pos->header());
			$this->view->tabs('pagecontent')->navigation('tab');

			if ($param) {
				
				Compiler::attach(DIR_SCRIPTS."ajaxuploader/ajaxupload.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				Compiler::attach(DIR_JS."form.validator.file.js");
				

				$param = (is_numeric($param)) ? $param : null;

				// button back
				$buttons['back'] = $this->request->query("files/$id");

				if ($param) {
					$pos_file = new Pos_File(Connector::DB_CORE);
					$data = $pos_file->read($param);
					
				} else {
					$data['redirect'] = $this->request->query($this->request->action.'/'.$id);
				}

				if( !$this->request->archived && $canEditPos) {

					$buttons['save'] = true;

					if ($param) {

						$integrity = new Integrity();
						$integrity->set($param, 'posfiles');

						if ($integrity->check()) {
							$buttons['delete'] = $this->request->link('/applications/helpers/pos.file.delete.php', array(
								'pos' => $pos,
								'id' => $param
							));
						}
					}

				}

				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;
				$data['action'] = $this->request->action;
				$data['posfile_posaddress'] = $id;

				if ($this->request->archived || !$canEditPos) {
					$fields = array_keys($data);
					$disabled = array_fill_keys($fields, true);
				}

				// dataloader: pos file groups
				$dataloader['posfile_filegroup'] = Pos_File_Group::loader();

				// template: pos file form
				$this->view->posform('pagecontent')
				->pos('file.data')
				->data('data', $data)
				->data('dataloader', $dataloader)
				->data('disabled', $disabled)
				->data('hidden', $hidden)
				->data('buttons', $buttons)
				->data('pos', $id)
				->data('id', $param);
			}
			else {
				
				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
				Compiler::attach(DIR_SCRIPTS."table.loader.js");
				
				// button: back
				$back = session::get('pos_referer');
				$back = ($back) ? $back : $this->request->query();
				$buttons['back'] = $back;
				
				$requestFields = array();
				$requestFields['id'] = $id;
				$requestFields['form'] = $this->request->query("files/$id");

				if ( !$this->request->archived && $canEditPos) {
					$requestFields['add'] = $this->request->query($this->request->action.'/'.$id.'/add');
				}
				
				// template: pos file list
				$this->view->posfiles('pagecontent')
				->pos('file.list')
				->data('buttons', $buttons)
				->data('requestFields', $requestFields)
				->data('id', $id);
			}
		}

		public function map() {

			$id = url::param();

			$pos = new Pos();
			$pos->read($id);

			// check access to pos
			if (!$pos->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			elseif (!$this->request->archived && ($pos->store_closingdate && $pos->store_closingdate <> '0000-00-00')) {
				url::redirect($this->request->query("archived/map/$id"));
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			
			// permissions
			$permission_edit = user::permission(Pos::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Pos::PERMISSION_EDIT_LIMITED);
			$permission_edit_map = user::permission('can_edit_google_map_in_posindex');

			// can edit pos
			$canEditPos = $pos->canEdit();
			
			$canEdit = ($canEditPos || $permission_edit_map) ? true : false;

			// button: back
			$back = session::get('pos_referer');
			$back = ($back) ? $back : $this->request->query();
			$buttons['back'] = $back;

			// form dataloader
			$data = $pos->data;
			$data['mapzoom'] = ($data['posaddress_google_lat']) ? 16 : 2;
			$data['draggable'] = (!$this->request->archived && $canEdit) ? true : false;
			$data['application'] = $this->request->application;

			// button: save
			if( !$this->request->archived && $canEdit) {
				$buttons['save'] = true;
			} else {
				$fields = array_keys($data);
				$disabled = array_fill_keys($fields, true);
			}

			// tempplate: header
			$this->view->posheader('pagecontent')->node('header')->data('title', $pos->header());
			$this->view->tabs('pagecontent')->navigation('tab');

			// template: pos map
			$this->view->posmap('pagecontent')
			->pos('map.data')
			->data('data', $data)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function openinghours() {
			
			$id = url::param();
			
			$pos = new Pos();
			$pos->read($id);
			
			// check access to pos
			if (!$pos->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			elseif (!$this->request->archived && ($pos->store_closingdate && $pos->store_closingdate <> '0000-00-00')) {
				url::redirect($this->request->query("archived/openinghours/$id"));
			}

			$model = new Model(Connector::DB_CORE);

			// can edit pos
			$canEditPos = $pos->canEdit();
			
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.blue.css");
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
			Compiler::attach(DIR_SCRIPTS."weekcalendar/weekcalendar.css");
			Compiler::attach(DIR_SCRIPTS."weekcalendar/weekcalendar.js");
			Compiler::attach(DIR_SCRIPTS."pop/pop.css");
			Compiler::attach(DIR_SCRIPTS."pop/pop.js");
			
			// button: back
			$back = session::get('pos_referer');
			$back = ($back) ? $back : $this->request->query();
			$buttons['back'] = $back;

			if ($canEditPos) {
				$buttons['save'] = true;
			}
			
			// template: header
			$this->view->header('pagecontent')->node('header')->data('title', $pos->header());
			$this->view->tabs('pagecontent')->navigation('tab');
			
			// get pos times
			$result = $model->query("SHOW TABLE STATUS LIKE 'posopeninghrs'")->fetch();
			$increment = $result['Auto_increment'];
			
			// get pos closed days
			$result = $model->query("
				SELECT posclosinghr_text 
				FROM posclosinghrs 
				WHERE posclosinghr_posaddress_id = $id
			")->fetch();
			
			$closed_days = $result['posclosinghr_text'];
			
			// country
			$country = new Country();
			$country->read($pos->country);
			$timeformat = $country->timeformat;
			$readonly = (user::permission('can_edit_pos_opening_hours')) ? false : true;
			
			$this->view
			->posform('pagecontent')
			->pos('openinghours')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id)
			->data('timeformat', $timeformat)
			->data('increment', $increment)
			->data('closed_days', $closed_days)		 
			->data('readonly', $readonly)		 
			->data('pos_country', $pos->country)		 
			->data('pos_place', $pos->place_id)		 
			->data('pos_ownertype', $pos->ownertype);	
		}
	
		public function services() {
			
			$id = url::param();
				
			$pos = new Pos();
			$pos->read($id);
				
			// check access to pos
			if (!$pos->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			elseif (!$this->request->archived && ($pos->store_closingdate && $pos->store_closingdate <> '0000-00-00')) {
				url::redirect($this->request->query("archived/store/$id"));
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			
			
			// can edit pos
			$canEditPos = $pos->canEdit();
				
			// db model
			$model = new Model(Connector::DB_CORE);
			
			// form dataloader
			$data = $pos->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['posaddress_customerservice_posaddress_id'] = $id;
			
			// button
			$buttons = array();
				
			// button: back
			$back = session::get('pos_referer');
			$back = ($back) ? $back : $this->request->query();
			$buttons['back'] = $back;
			
			// dataloader: posareas
			$result = $model->query("
				SELECT posaddress_customerservice_customerservice_id AS id
				FROM posaddress_customerservices
				WHERE posaddress_customerservice_posaddress_id = $id
			")->fetchAll();
				
			if ($result) {
			
				foreach ($result as $row) {
					$services[] = $row['id'];
				}
			
				$data['services'] = serialize($services);
			}

			if (!$this->request->archived && ($canEditPos || user::permission('can_edit_customerservices'))) {
				$buttons['save'] = true;
			} else {
				$fields = array_keys($data);
				$disabled = array_fill_keys($fields, true);
				$disabled['services'] = true;
			}
			
			// dataloader: posareas
			$result = $model->query("
				SELECT customerservice_id, customerservice_text
				FROM customerservices
				ORDER BY customerservice_text
			")->fetchAll();
			
			$dataloader['services'] = _array::extract($result);
			
			// template: headers
			$this->view->header('pagecontent')->node('header')->data('title', $pos->header());
			$this->view->tabs('pagecontent')->navigation('tab');
			
			// template: form
			$this->view->posform('pagecontent')
			->pos('customer.services')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
			
		}


		public function localization() {
			
			$this->view->pagetitle = $this->request->title . " - " . translate::instance()->translation;

			// view countries
			$link = $this->request->query('data');
			$this->request->field('form', $link);
			$this->request->field('application', $this->request->application);

			
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'list.php');
			$tpl->data('url', '/applications/modules/pos/localization/list.php');
			$tpl->data('class', 'list-600');
			$this->view->setTemplate('collections', $tpl);

			Compiler::attach(array(
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js',
				'/public/js/pos.localization.js'
			));
		}
	}