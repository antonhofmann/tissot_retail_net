<?php

class SapDistributors_Controller {

	public function __construct() {
				
		$this->user = User::instance();
		$this->request = request::instance();
		$this->translate = Translate::instance();
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}

	public function index() {

		/*
		$_CAN_EDIT_ALL = user::permission('can_edit_all_sap_order_matrix');
		$_CAN_EDIT_HIS = user::permission('can_edit_his_sap_order_matrix');

		$model = new Model();

		if ($_CAN_EDIT_ALL) {
			
			$sth= $model->db->prepare("
				SELECT 
					address_id AS id, 
					CONCAT(country_name, ': ', address_company) AS name
				FROM addresses
				INNER JOIN countries ON country_id = address_country
				WHERE address_involved_in_planning = 1 AND address_active = 1 AND address_do_data_export_from_mps_to_sap = 1
				ORDER BY country_name, address_company
			");

			$sth->execute(array($_CLIENT));
			$result = $sth->fetchAll();
			$clients = _array::extract($result);
		}

		// template: list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'sap/distributor/list.php');
		$tpl->data('clients', $clients);
		$this->view->setTemplate('mastersheet', $tpl);

		Compiler::attach(array(
			'/public/css/sap.matrix.css',
			'/public/scripts/handsontable/handsontable.full.min.css',
			'/public/scripts/handsontable/handsontable.bootstrap.css',
			'/public/scripts/handsontable/handsontable.full.min.js',
			'/public/js/sap.matrix.list.js'
		));
		*/
	}

	public function channels() {

		$_CAN_EDIT_ALL = user::permission('can_edit_all_sap_order_matrix');
		$_CAN_EDIT_HIS = user::permission('can_edit_his_sap_order_matrix');

		// template: list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'sap/matrix/channels.php');
		$this->view->setTemplate('noname', $tpl);

		Compiler::attach(array(
			'/public/css/sap.matrix.css',
			'/public/scripts/handsontable/handsontable.full.min.css',
			'/public/scripts/handsontable/handsontable.bootstrap.css',
			'/public/scripts/handsontable/handsontable.full.min.js',
			'/public/js/sap.matrix.channels.js'
		));
	}

	public function ordertypes() {

		$_CAN_EDIT_ALL = user::permission('can_edit_all_sap_order_matrix');
		$_CAN_EDIT_HIS = user::permission('can_edit_his_sap_order_matrix');

		// template: list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'sap/matrix/order.types.php');
		$this->view->setTemplate('noname', $tpl);

		Compiler::attach(array(
			'/public/css/sap.matrix.css',
			'/public/scripts/handsontable/handsontable.full.min.css',
			'/public/scripts/handsontable/handsontable.bootstrap.css',
			'/public/scripts/handsontable/handsontable.full.min.js',
			'/public/js/sap.matrix.ordertypes.js'
		));
	}
}