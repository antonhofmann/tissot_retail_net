<?php 

class Mail_Controller {
	
	
	public function __construct() {
			
		$this->user = User::instance();
		$this->request = request::instance();
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}

	public function invitationMail() {

		$mail = new Mail_Template();
		$data = $mail->read(44);
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		
		// buttons
		$buttons = array();
		//$buttons['back'] = $this->request->query();
		$buttons['send'] = '/applications/modules/user/invitation.sendmail.php';
		$buttons['test'] = '/applications/modules/user/invitation.sendmail.php';
		
		// view template
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'user/invitation.php');
		$tpl->data('data', $data);
		$tpl->data('buttons', $buttons);
		$this->view->setTemplate('articles', $tpl);

		Compiler::attach('/public/js/user.invitation.mail.js');
		Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
		Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
		Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
	}
}