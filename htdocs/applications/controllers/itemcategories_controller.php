<?php 

	class ItemCategories_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() { 
			
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
			Compiler::attach(DIR_CSS."jquery.ui.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			if ($permission_edit) {
				$this->request->field('add',$this->request->query('add'));
			}
			
			if ($permission_edit || $permission_view) {
				$this->request->field('data',$this->request->query('data'));
			}
			
			// template: role list
			$this->view->productLines('pagecontent')
			->item('categories')
			->data('buttons', $buttons);
		}

		public function add() {

			$permission_edit = user::permission('can_edit_catalog');
		
			if($this->request->archived || !$permission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
				
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();

			$data['item_category_active'] = 1;
		
			// template: application form
			$this->view->productLine('pagecontent')
			->item('category.form')
			->data('data', $data)
			->data('buttons', $buttons);
		}
		
		public function data() {

			$id = url::param();

			$itemCategory = new Item_Category();
			$itemCategory->read($id);

			if (!$itemCategory->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			$data = $itemCategory->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			if ($permission_edit) {
			
				$buttons['save'] = true;

				$integrity = new Integrity();
				$integrity->set($id, 'item_categories', 'system');

				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/item.category.delete.php', array('id'=>$id));
				}
			}
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $itemCategory->name);
			$this->view->companytabs('pagecontent')->navigation('tab');
		
			// template: form
			$this->view->productLine('pagecontent')
			->item('category.form')
			->data('data', $data)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons);
		}
		
		public function subcategories() {

			$id = url::param();
			$subcategory = url::param(1);

			$itemCategory = new Item_Category();
			$itemCategory->read($id);

			if (!$itemCategory->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$model = new Model(Connector::DB_CORE);

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");

			$itemSubcategory = new Item_Subcategory();
			$data = $itemSubcategory->read($subcategory);

			// buttons
			$buttons = array();

			// header
			$this->view->header('pagecontent')->node('header')->data(array(
				'title' => $itemCategory->name,
				'subtitle' => $itemSubcategory->name ?: 'Add New Subcategory'
			));
			
			$this->view->companytabs('pagecontent')->navigation('tab');

			if ($subcategory) {

				$subcategory = is_numeric($subcategory) ? $subcategory : null;

				$buttons['back'] = $this->request->query("subcategories/$id");
				$buttons['save'] = $permission_edit;

				if (!$itemSubcategory->id) {
					$data['item_subcategory_active'] = 1;
					$data['redirect'] = $this->request->query("subcategories/$id");
				}

				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;
				$data['item_subcategory_category_id'] = $id;

				if ($permission_edit && $subcategory) {

					$integrity = new Integrity();
					$integrity->set($subcategory, 'item_subcategories', 'system');

					if ($integrity->check()) {
						$buttons['delete'] = $this->request->link('/applications/modules/item/subcategory/delete.php', array(
							'category'=>$id,
							'id'=>$subcategory,
						));
					}
				}
				elseif($data) {
					foreach ($data as $key => $value) {
						$disabled[$key] = true;
					}
				}

				// template: form
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'item/subcategory/form.php');
				$tpl->data('actions', $actions);
				$tpl->data('buttons', $buttons);
				$tpl->data('data', $data);
				$this->view->setTemplate('subcategories', $tpl);

			} else {

				$buttons['back'] = Ui::button(array(
					'icon' => 'back',
					'caption' => 'Back',
					'href' => $this->request->query()
				));

				if ($permission_edit) {
					$actions[] = Ui::button(array(
						'icon' => 'add',
						'caption' => 'Add New',
						'href' => $this->request->query("subcategories/$id/add")
					));
				}

				$subcategories = array();

				$result = $model->query("
					SELECT * 
					FROM item_subcategories
					WHERE item_subcategory_category_id = $id
				")->fetchAll();

				if ($result) {

					$link = $this->request->query("subcategories/$id");
					
					foreach ($result as $row) {
						
						$id = $row['item_subcategory_id'];

						$subcategories[$id] = array(
							'item_subcategory_name' => "<a href='$link/$id' >{$row[item_subcategory_name]}</a>",
							'active' => $row['item_subcategory_active'] ? "<img src='/public/images/icon-checked.png' />" : null
						);
					}
				}

				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'item/subcategory/list.php');
				$tpl->data('actions', $actions);
				$tpl->data('buttons', $buttons);
				$tpl->data('subcategories', $subcategories);
				$this->view->setTemplate('subcategories', $tpl);
			}

				
		}
	}