<?php 

	class Languages_Controller {
		
		public function __construct() {
				
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			// request
			$requestFields = array();
			$requestFields['add'] = $this->request->query('add');
			$requestFields['form'] = $this->request->query('data');
		
			// template: language list
			$this->view->languageList('pagecontent')
			->language('list')
			->data('requestFields', $requestFields);
		}
		
		public function add() {
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
				
			// form dataloadef
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
		
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
		
			$this->view->language('pagecontent')
			->language('data')
			->data('data', $data)
			->data('buttons', $buttons);
		}
		
		public function data() {
				
			$id = url::param();
			
			$language = new Language();
			$language->read($id);
				
			// check access to language
			if (!$language->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// form datalaoder
			$data = $language->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
				
			$integrity = new Integrity();
			$integrity->set($id, 'languages');
				
			if ($integrity->check()) {
				$buttons['delete'] = $this->request->link('/applications/helpers/language.delete.php', array('id'=>$id));
			}
				
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $language->name);
		
			// template: navigation tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
				
			// template: language form
			$this->view->language('pagecontent')
			->language('data')
			->data('data', $data)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function translations() {
		
			$id = url::param();
			$param = url::param(1);
			
			$language = new Language();
			$language->read($id);
				
			// check access to language
			if (!$language->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			// request
			$requestFields = array();
			$requestFields['id'] = $id;
			$requestFields['add'] = $this->request->query("translations/$id/add");
			$requestFields['form'] = $this->request->query("translations/$id");
			
			// buttons
			$buttons = array();

			// template: headers
			$this->view->header('pagecontent')->node('header')->data('title', $language->name);
			$this->view->tabs('pagecontent')->navigation('tab');
			
			if ($param) {
				
				Compiler::attach(DIR_SCRIPTS."loader/loader.css");
				Compiler::attach(DIR_SCRIPTS."loader/loader.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
				Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
				
				$model = new Model();
				$param = (is_numeric($param)) ? $param : null;
				
				// button back
				$buttons['back'] = $this->request->query("translations/$id");
				
				// button save
				$buttons['save'] = true;

				if ($param) {
					
					// form dataloader
					$data['application'] = $this->request->application;
					$data['controller'] = $this->request->controller;
					$data['action'] = $this->request->action;
					
					// ste keyword id
					$data['translation_keyword'] = $param;
					
					// heide keyword field
					$hidden['keyword'] = true;
					$hidden['content'] = true;
					
					// translation keyword
					$result = $model->query("
						SELECT translation_keyword_name 
						FROM translation_keywords
						WHERE translation_keyword_id = $param
					")->fetch();
					
					$keyword = $result['translation_keyword_name'];
					
					// translation contents
					$result = $model->query("
						SELECT translation_category, translation_content
						FROM translations
						WHERE translation_keyword = $param AND translation_language = $id
					")->fetchAll();
					
					$translations = _array::extract($result);
					
					// translation categories
					$categories = $model->query("
						SELECT *
						FROM translation_categories
					")->fetchAll();
					
					// build translation fields
					if ($categories) {
						foreach ($categories as $row) {
							$category = $row['translation_category_id'];
							$key = $row['translation_category_name'].'_'.$keyword;
							$data[$key] = $translations[$category];
							$fields[$key] = ($category == 1)  
								? $row['translation_category_caption']."<em>$keyword</em>" 
								: $row['translation_category_caption'].'<em>'.$row['translation_category_name']."_$keyword</em>";
						}
					}
					
					// button: delete
					$buttons['delete'] = $this->request->link('/applications/helpers/language.translation.delete.php', array(
						'language' => $id,
						'id' => $param
					));
				} 
				else {
					$data['redirect'] =  $this->request->query("translations/$id");
				}
				
				// translation language
				$data['translation_language'] = $id;

				$this->view->translation('pagecontent')
				->language('translation.data')
				->data('data', $data)
				->data('fields', $fields)
				->data('dataloader', $dataloader)
				->data('disabled', $disabled)
				->data('hidden', $hidden)
				->data('buttons', $buttons)
				->data('keyword', $keyword);
			}
			else {
				
				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
				Compiler::attach(DIR_SCRIPTS."table.loader.js");
				Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
				Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
				Compiler::attach(DIR_SCRIPTS."loader/loader.css");
				Compiler::attach(DIR_SCRIPTS."loader/loader.js");
				
				$requestFields = array();
				$requestFields['id'] = $id;
				$requestFields['add'] = $this->request->query("translations/$id/add");
				$requestFields['form'] = $this->request->query("translations/$id");
				
				// button back
				$buttons['back'] = $this->request->query();
				
				$this->view->languages('pagecontent')
				->language('translation.list')
				->data('buttons', $buttons)
				->data('requestFields', $requestFields)
				->data('id', $id);
			}
		}
		
		public function applications() {
		
			$id = url::param();
				
			$language = new Language();
			$language->read($id);
				
			// check access to language
			if (!$language->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			// request
			$requestFields = array();
			$requestFields['id'] = $id;
				
			// form datalaoder
			$data = $language->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
		
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $language->name);
		
			// template: navigation tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			// template: language used in applications
			$this->view->languages('pagecontent')
			->language('application.list')
			->data('buttons', $buttons)
			->data('requestFields', $requestFields)
			->data('id', $id);
		}
	}