<?php

class SapDistChannels_Controller {

	public function __construct() {
				
		$this->user = User::instance();
		$this->request = request::instance();
		$this->translate = Translate::instance();
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}

	public function index() {

		// request form: link edit
		$this->request->field('form', $this->request->query('data'));
		$this->request->field('add', $this->request->query('add'));

		// template: list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'list.php');
		$tpl->data('url', '/applications/modules/sap/distchannel/list.php');
		$tpl->data('class', 'list-600');
		$this->view->setTemplate('distributors', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js'
		));
	}

	public function add() {
			
		$id = url::param();
		
		// form datalaoder
		$data = $distributor->data;
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['action'] = $this->request->action;
		$data['redirect'] = $this->request->query('data');
		
		// buttons
		$buttons = array();
		$buttons['save'] = true;
		$buttons['back'] = $this->request->query();
		
		// template: list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'sap/distchannel/form.php');
		$tpl->data('data', $data);
		$tpl->data('buttons', $buttons);
		$this->view->setTemplate('distributor', $tpl);

		Compiler::attach(array(
			'/public/scripts/validationEngine/css/validationEngine.jquery.css',
			'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
			'/public/scripts/validationEngine/js/jquery.validationEngine.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/js/form.validator.js'
		));
	}

	public function data() {
			
		$id = url::param();
		
		$distributor = new Modul($application);
		$distributor->setTable('sap_distribution_channels');
		$distributor->read($id);
		
		if (!$distributor->id) {
			Message::access_denied();
			url::redirect($this->request->query());
		}
		
		// form datalaoder
		$data = $distributor->data;
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['action'] = $this->request->action;
		
		// buttons
		$buttons = array();
		$buttons['save'] = true;
		$buttons['back'] = $this->request->query();
		
		// button: delete
		if (user::permission('can_administrate_system_data')) {
		
			$integrity = new Integrity();
			$integrity->set($id, 'sap_distribution_channels', 'system');
		
			if ($integrity->check()) {
				$buttons['delete'] = $this->request->link("/applications/modules/sap/distchannel/delete.php", array('id'=>$id));
			}
			
		} else {
			$fields = array_keys($data);
			$disabled = array_fill_keys($fields, true);
		}
		
		// template:: header
		$header = $data['sap_distribution_channel_code'].', '.$data['sap_distribution_channel_name'];
		$this->view->header('pagecontent')->node('header')->data('title', $header);
		
		// template: list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'sap/distchannel/form.php');
		$tpl->data('data', $data);
		$tpl->data('buttons', $buttons);
		$tpl->data('disabled', $disabled);
		$this->view->setTemplate('distributor', $tpl);

		Compiler::attach(array(
			'/public/scripts/validationEngine/css/validationEngine.jquery.css',
			'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
			'/public/scripts/validationEngine/js/jquery.validationEngine.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/js/form.validator.js'
		));
	}
}