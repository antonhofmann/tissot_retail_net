<?php 

class Launchplans_Controller { 

	// permissions
	static public $permissions;
	
	public function __construct() {
		
		$this->request = request::instance();

		static::$permissions =  array(
			'view' => user::permission('can_view_all_lps_sheets'),
			'view.limited' => user::permission('can_view_only_his_lps_sheets'),
			'edit' => user::permission('can_edit_all_lps_sheets'),
			'edit.limited' => user::permission('can_edit_only_his_lps_sheets'),
			'manage' => user::permission('can_manage_lps_sheets')
		);
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}
	
	public function index() {
					
		// dropdown actions
		if (!$this->request->archived && static::$permissions['manage']) {
			$this->request->field('manage', $this->request->query('manage'));
			$this->request->field('submit', $this->request->query('submit'));
			$this->request->field('remind', $this->request->query('remind'));
		}

		// dropdown action import items from ramco
		if ($this->request->archived && static::$permissions['manage']) {
			$this->request->field('exchange_ordersheet_items', $this->request->link('/cronjobs/exchange.launchplans.php'));
		}
		
		// form link
		$field = $this->request->query('data');
		$this->request->field('form', $field);
		
		// template: list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'launchplan/list.php');
		$tpl->data('dataloader', $dataloader);
		$this->view->setTemplate('mastersheet', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js'
		));
	}
	
	public function data() { 
		
		$id = url::param();
		$application = $this->request->application;
		
		// order sheet
		$launchplan = new Modul($application);
		$launchplan->setTable('lps_launchplans');
		$data = $launchplan->read($id);

		// state handler
		$state = new State($data['lps_launchplan_workflowstate_id']); 
		$state->setOwner($data['lps_launchplan_address_id']);
		$state->setDateIntervals($data['lps_launchplan_openingdate'], $data['lps_launchplan_closingdate']);
		
		// check access to order sheet
		$this->checkAccess($launchplan);
		$this->buildHeader($launchplan);
		$this->buildTabs($launchplan, $state);

		// archived state
		if (!$this->request->archived && $state->isExported()) {
			url::redirect($this->request->query("archived/data/$id"));
		}
		
		// db model
		$model = new Model($application);

		// form dataloader
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['archived'] = $this->request->archived;
		$data['action'] = $this->request->action;
		$data['redirect'] = $this->request->query("data/$id");
		$data['lps_launchplan_openingdate'] = date::system($data['lps_launchplan_openingdate']);
		$data['lps_launchplan_closingdate'] = date::system($data['lps_launchplan_closingdate']);

		// buttons
		$buttons = array();
		$buttons['back'] = $this->request->query();

		// get mastersheet
		$mastersheet = new Modul($application);
		$mastersheet->setTable('lps_mastersheets');
		$mastersheet->read($data['lps_launchplan_mastersheet_id']);
		
		$data['lps_mastersheet_year'] = $mastersheet->data['lps_mastersheet_year'];
		$data['lps_mastersheet_name'] = $mastersheet->data['lps_mastersheet_name'];

		
		// for administrators
		if (!$state->isExported() && $state->canAdministrate() && $state->onPreparation() ) {
				
			// button: save
			$buttons['save'] = true;
			
			// button: delete
			if ($state->canDelete()) {
				$integrity = new Integrity();
				$integrity->set($id, $tableOrderSheet);
			}
			
			// not preparation mode
			if ($state->isApproved()) {
				$disabled['ordersheet_comment'] = true;
				$disabled['ordersheet_openingdate'] = true;
				$disabled['ordersheet_closingdate'] = true;
			}
		} 
		else {
			$fields = array_keys($data);
			$disabled = array_fill_keys($fields, true);
		}
		
		// allow clients to delete standard order sheets
		if (!$buttons['delete'] && $state->canEdit() && $data['lps_launchplan_workflowstate_id'] == 2) {
			
			if (substr($data['lps_mastersheet_name'], 0, strlen('Standard')) === 'Standard') {
				$buttons['delete'] = $this->request->link('/applications/modules/launchplan/delete.php', array('id'=>$id));
			}
		}
		
		// button: create version
		if (!$state->isExported() && $state->canEdit()) {
			$buttons['version'] = $this->request->link('/applications/modules/launchplan/version.submit.php', array('id'=>$id));
		}

		// button: print
		$buttons['print'] = $this->request->link('/applications/modules/launchplan/print.items.php', array('id'=>$id)); 
		

		// workflow states
		$allowedStates = array();

		switch ($state->state) {
			case 1: $allowedStates = array(1,2); break; // in preparation
			case 2: $allowedStates = array(1,2); break; // open
			case 3: $allowedStates = array(1,2,3); break; // in process
			case 4: $allowedStates = array(1,2,3,4); break; // completed
			case 5: $allowedStates = array(1,2,3,4,5); break; // approved
			case 6: $allowedStates = array(6); break; // revision
			default: $allowedStates = array($state->state); break;
		}

		$states = join(',', $allowedStates);

		// get workflow statess
		$sth = $model->db->prepare("
			SELECT lps_workflow_state_id, lps_workflow_state_name
			FROM lps_workflow_states
			WHERE lps_workflow_state_id IN ($states)
			ORDER BY lps_workflow_state_code
		");

		$sth->execute();
		$result = $sth->fetchAll();
		$dataloader['lps_launchplan_workflowstate_id'] = _array::extract($result);

		if (!$state->canAdministrate() || $state->state > 5) {
			$disabled['lps_launchplan_workflowstate_id'] = true;
		}

		// trackings
		$sth = $model->db->prepare("
			SELECT
				user_tracking_id AS id,
				DATE_FORMAT(user_trackings.date_created,'%d.%m.%Y') AS date,
				user_tracking_action AS action,
				CONCAT(user_firstname,' ', user_name) AS user, 
				user_trackings.user_created as user2
			FROM user_trackings
			INNER JOIN db_retailnet.users ON user_id = user_tracking_user_id
			WHERE user_tracking_entity = 'launch plan' AND user_tracking_entity_id = ?
		");

		$sth->execute(array($launchplan->id));
		$result = $sth->fetchAll();

		if ($result) {	
			foreach ($result as $row) {
				$trackings[] = array(
					'date' => $row['date'],
					'workflow_state' => ucfirst($row['action']),
					'user' => ($row['user']) ? $row['user'] : $row['user2']
				);
			}
		}
		
		// ordersheet form
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'launchplan/form.php');
		$tpl->data('data', $data);
		$tpl->data('dataloader', $dataloader);
		$tpl->data('disabled', $disabled);
		$tpl->data('hidden', $hidden);
		$tpl->data('buttons', $buttons);
		$tpl->data('trackings', $trackings);

		// assign template to view
		$this->view->setTemplate('ordersheet', $tpl);

		Compiler::attach(array(
			'/public/scripts/jquery/jquery.ui.css',
			'/public/scripts/jquery/jquery.ui.js',
			'/public/css/jquery.ui.css',
			'/public/scripts/validationEngine/css/validationEngine.jquery.css',
			'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
			'/public/scripts/validationEngine/js/jquery.validationEngine.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/js/form.validator.js',
			'/public/js/launchplan.form.js'
		));
	}
	
	public function items() {
		
		$id = url::param();
		$application = $this->request->application;
		
		// order sheet
		$launchplan = new Modul($application);
		$launchplan->setTable('lps_launchplans');
		$data = $launchplan->read($id);

		// state handler
		$state = new State($data['lps_launchplan_workflowstate_id']); 
		$state->setOwner($data['lps_launchplan_address_id']);
		$state->setDateIntervals($data['lps_launchplan_openingdate'], $data['lps_launchplan_closingdate']);
		
		// check access to order sheet
		$this->checkAccess($launchplan);
		$this->buildHeader($launchplan);
		$this->buildTabs($launchplan, $state);

		// archived state
		if (!$this->request->archived && $state->isExported()) {
			url::redirect($this->request->query("archived/data/$id"));
		}
		
		// db model
		$model = new Model($application);

		// framework vars
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['archived'] = $this->request->archived;
		$data['action'] = $this->request->action;

		// buttons
		$buttons = array();
		
		// button back
		$buttons['back'] = ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $this->request->query(),
			'label' => translate::instance()->back
		));

		if ($state->onPreparation() && $state->canEdit()) {

			// approvment buttons
			if ($state->canApprov()) {

				$buttons['approve'] = ui::button(array(
					'id' => 'approve',
					'icon' => 'save',
					'class' => 'submit-items',
					'href' => $this->request->link('/applications/modules/launchplan/item.approve.php',array('id'=>$id)),
					'label' => translate::instance()->approve
				));

				$buttons['approve_sendmail'] = ui::button(array(
					'id' => 'approve_sendmail',
					'icon' => 'save',
					'class' => 'submit-items sendmail',
					'href' => $this->request->link('/applications/modules/launchplan/item.approve.php',array('id'=>$id)),
					'label' => translate::instance()->approve." & Send Mail",
					'data-template' => 23
				));
			}

			// revision buttons
			if ($state->canSetRevisions()) {

				$buttons['revision'] = ui::button(array(
					'id' => 'revision',
					'icon' => 'reload',
					'class' => 'submit-items sendmail',
					'href' => $this->request->link('/applications/modules/launchplan/item.revision.php',array('id'=>$id)),
					'label' => translate::instance()->revision,
					'data-template' => 25
				));
			}
			
			// submit button
			if ($state->canSubmit()) {
				$buttons['submit'] = ui::button(array(
					'id' => 'submit',
					'icon' => 'save',
					'class' => 'submit-items',
					'href' => $this->request->link('/applications/modules/launchplan/item.submit.php',array('id'=>$id)),
					'label' => translate::instance()->submit
				));
			}	
		}

		// button: print
		$buttons['print'] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'href' => $this->request->link('/applications/modules/launchplan/print.items.php', array('id'=>$id)),
			'label' => translate::instance()->print
		));

		$this->request->field('launchplan', $id);
		$this->request->field('fixedNotSelectable', 1);

		// ordersheet form
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'launchplan/items.php');
		$tpl->data('data', $data);
		$tpl->data('maildata', $maildata);
		$tpl->data('buttons', $buttons);

		// assign template to view
		$this->view->setTemplate('launchplan', $tpl);

		Compiler::attach(array(
			'/public/scripts/spreadsheet/spreadsheet.css',
			'/public/scripts/spreadsheet/spreadsheet.js',
			'/public/scripts/textarea.expander.js',
			'/public/js/launchplan.items.js',
			'/public/css/launchplan.items.css'
		));
	}
	
	public function files() {
		
		$id = url::param();
		$application = $this->request->application;
		
		// order sheet
		$launchplan = new Modul($application);
		$launchplan->setTable('lps_launchplans');
		$data = $launchplan->read($id);

		// state handler
		$state = new State($data['lps_launchplan_workflowstate_id']); 
		$state->setOwner($data['lps_launchplan_address_id']);
		$state->setDateIntervals($data['lps_launchplan_openingdate'], $data['lps_launchplan_closingdate']);
		
		// check access to order sheet
		$this->checkAccess($launchplan);
		$this->buildHeader($launchplan);
		$this->buildTabs($launchplan, $state);

		// archived state
		if (!$this->request->archived && $state->isExported()) {
			url::redirect($this->request->query("archived/data/$id"));
		}

		// db model
		$model = new Model($application);

		// framework vars
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['archived'] = $this->request->archived;
		$data['action'] = $this->request->action;

		// buttons
		$buttons = array();
		$buttons['back'] = ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $this->request->query(),
			'label' => translate::instance()->back
		));

		// collection category files
		$sth = $model->db->prepare("
			SELECT DISTINCT 
				lps_collection_category_file_id AS id, 
				lps_collection_category_file_title AS title, 
				lps_collection_category_file_description AS description, 
				lps_collection_category_file_path AS path,
				DATE_FORMAT(lps_collection_category_files.date_created, '%d.%m.%Y') AS date
			FROM lps_collection_category_files 
			INNER JOIN lps_references ON lps_collection_category_file_collection_id = lps_reference_collection_category_id
			INNER JOIN lps_launchplan_items ON lps_reference_id = lps_launchplan_item_reference_id
			WHERE lps_collection_category_file_visible = 1 AND lps_launchplan_item_launchplan_id = ?
			ORDER BY lps_collection_category_file_title
		");

		$sth->execute(array($launchplan->id));
		$collectionFiles = $sth->fetchAll();

		// mastersheet files
		$sth = $model->db->prepare("
			SELECT DISTINCT 
				lps_mastersheet_file_id AS id,
				lps_mastersheet_file_title AS title, 
				lps_mastersheet_file_description AS description, 
				lps_mastersheet_file_path AS path,
				DATE_FORMAT(lps_mastersheet_files.date_created, '%d.%m.%Y') AS date
			FROM lps_mastersheet_files 
			INNER JOIN lps_launchplans ON lps_launchplan_mastersheet_id = lps_mastersheet_file_mastersheet_id
			WHERE lps_mastersheet_file_visible = 1 AND lps_launchplan_id = ?
			ORDER BY lps_mastersheet_file_title
		");	

		$sth->execute(array($launchplan->id));
		$mastersheetFiles = $sth->fetchAll();

		$datagrid = array_merge($collectionFiles, $mastersheetFiles);

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'file/list.php');
		$tpl->data('data', $data);
		$tpl->data('datagrid', $datagrid);
		$tpl->data('buttons', $buttons);
		$this->view->setTemplate('ordersheet.file', $tpl);
		
	}
	
	public function versions() {
		
		$id = url::param();
		$param = url::param(1);
		$application = $this->request->application;
		
		// order sheet
		$launchplan = new Modul($application);
		$launchplan->setTable('lps_launchplans');
		$data = $launchplan->read($id);

		// state handler
		$state = new State($data['lps_launchplan_workflowstate_id']); 
		$state->setOwner($data['lps_launchplan_address_id']);
		$state->setDateIntervals($data['lps_launchplan_openingdate'], $data['lps_launchplan_closingdate']);

		if ($param) {

			$version = new Modul($application);
			$version->setTable('lps_launchplan_versions');
			$version->read($param);
			
			if (!$version->id) {
				message::error('Version not found');
				url::redirect($this->request->query("versions/$id"));
			}

			$versionName = 'Version: '.$version->data['lps_launchplan_version_title'];
		}
		
		// check access to order sheet
		$this->checkAccess($launchplan);
		$this->buildHeader($launchplan, true, $versionName);
		$this->buildTabs($launchplan, $state);

		// archived state
		if (!$this->request->archived && $state->isExported()) {
			url::redirect($this->request->query("archived/data/$id"));
		}	
		
		// db model
		$model = new Model($application);

		// framework vars
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['archived'] = $this->request->archived;
		$data['action'] = $this->request->action;

		// buttons
		$buttons = array();
		$buttons['back'] = ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $this->request->query(),
			'label' => translate::instance()->back
		));
		
		if ($param) {
			
			$buttons['back'] = $this->request->query("versions/$id");

			// get mastersheet
			$mastersheet = new Modul($application);
			$mastersheet->setTable('lps_mastersheets');
			$mastersheet->read($data['lps_launchplan_mastersheet_id']);

			// form dataloader
			$data['lps_launchplan_openingdate'] = date::system($data['lps_launchplan_openingdate']);
			$data['lps_launchplan_closingdate'] = date::system($data['lps_launchplan_closingdate']);
			$data['lps_mastersheet_year'] = $mastersheet->data['lps_mastersheet_year'];
			$data['lps_mastersheet_name'] = $mastersheet->data['lps_mastersheet_name'];
			$data['lps_mastersheet_weeks'] = $mastersheet->data['lps_mastersheet_weeks'];
			$data['lps_mastersheet_week_number_first'] = $mastersheet->data['lps_mastersheet_week_number_first'];
			$data['lps_mastersheet_estimate_month'] = $mastersheet->data['lps_mastersheet_estimate_month'];

			// dataloader: workwlow states
			$sth = $model->db->prepare("
				SELECT lps_workflow_state_id, lps_workflow_state_name
				FROM lps_workflow_states
				ORDER BY lps_workflow_state_code
			");

			$sth->execute();
			$result = $sth->fetchAll();
			$dataloader['lps_launchplan_workflowstate_id']  = _array::extract($result);
			
			$fields = array_keys($data);
			$disabled = array_fill_keys($fields, true);

			// has items
			$sth = $model->db->prepare("
				SELECT COUNT(lps_launchplan_version_item_id) AS total
				FROM lps_launchplan_version_items
				WHERE lps_launchplan_version_item_version_id = ?
			");

			$sth->execute(array($param));
			$items = $sth->fetch();

			$buttons['items'] = $items['total'] ? true : false;
			
			// button: delete
			if (!$this->request->archived && $state->canAdministrate()) {
				$buttons['delete'] = $this->request->link('/applications/modules/launchplan/version.delete.php', array('id' => $param));
			}
			
			// button: print
			 $buttons['print'] = $this->request->link('/applications/modules/launchplan/print.items.version.php', array(
				'ordersheet' => $id,
				'version' => $param
			));
		
			// get version items
			$sth = $model->db->prepare("
				SELECT DISTINCT
					lps_launchplan_version_item_id AS id, 
					lps_launchplan_version_item_price AS price, 
					lps_launchplan_version_item_exchangrate AS exchangrate, 
					lps_launchplan_version_item_factor AS factor, 
					lps_launchplan_version_item_quantity AS quantity, 
					lps_launchplan_version_item_quantity_estimate AS quantity_estimate, 
					lps_launchplan_version_item_quantity_approved AS quantity_approved, 
					lps_launchplan_version_item_order_date AS order_date, 
					lps_launchplan_version_item_customernumber AS customernumber, 
					lps_launchplan_version_item_shipto AS shipto, 
					lps_launchplan_version_item_desired_delivery_date AS desired_delivery_date, 
					lps_launchplan_version_item_purchase_order_number AS purchase_order_number, 
					lps_launchplan_version_item_comment AS reference_description,
					currency_symbol AS currency, 
					lps_collection_code AS collection_code,
					lps_reference_code AS reference_code, 
					lps_reference_name AS reference_name, 
					lps_product_group_name AS group_name, 
					lps_product_group_id AS group_id
				FROM lps_launchplan_version_items 
				INNER JOIN lps_references ON lps_launchplan_version_item_reference_id = lps_reference_id
				INNER JOIN lps_product_groups ON lps_reference_product_group_id = lps_product_group_id
				INNER JOIN lps_collections ON lps_reference_collection_id = lps_collection_id
				INNER JOIN lps_launchplan_versions ON lps_launchplan_version_item_version_id = lps_launchplan_version_id
				INNER JOIN db_retailnet.currencies ON currency_id = lps_launchplan_version_item_currency
				WHERE lps_launchplan_version_item_version_id = ?
			");

			$sth->execute(array($param));
			$items = $sth->fetchAll();

			// get item week quantities
			$sth = $model->db->prepare("
				SELECT DISTINCT
					lps_launchplan_version_item_week_quantity_id AS id, 
					lps_launchplan_version_item_week_quantity_item_id AS item,
					lps_launchplan_version_item_week_quantity_week AS week, 
					lps_launchplan_version_item_week_quantity_quantity AS quantity, 
					lps_launchplan_version_item_week_quantity_approved AS quantity_approved
				FROM lps_launchplan_version_item_week_quantities  
				INNER JOIN lps_launchplan_version_items ON lps_launchplan_version_item_id = lps_launchplan_version_item_week_quantity_item_id
				WHERE lps_launchplan_version_item_version_id = ?
			");

			$sth->execute(array($param));
			$result = $sth->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$item = $row['item'];
					$week = $row['week'];
					$itemWeekQuantities[$item][$week] = array(
						'quantity' => $row['quantity'],
						'quantity_approved' => $row['quantity_approved']
					);
				}
			}

			if ($state->canAdministrate()) $showApprovment = true;
			else $showApprovment = $state->state >= 5 && $state->state <> 6 ? true : false;
		
			// template: list
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'launchplan/version.items.php');
			$tpl->data('data', $data);
			$tpl->data('dataloader', $dataloader);
			$tpl->data('disabled', $disabled);
			$tpl->data('buttons', $buttons);
			$tpl->data('items', $items);
			$tpl->data('itemWeekQuantities', $itemWeekQuantities);
			$tpl->data('showApprovment', $showApprovment);
			$this->view->setTemplate('launchplan.version', $tpl);
		}
		else {
			
			// request field id
			$this->request->field('id', $id);
			
			// request field fomr link
			$link = $this->request->query("versions/$id");
			$this->request->field('form', $link);

			// template: list
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'list.php');
			$tpl->data('url', '/applications/modules/launchplan/version.list.php');
			$tpl->data('class', 'list-700');
			$this->view->setTemplate('launchplan.versions', $tpl);

			Compiler::attach(array(
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js'
			));
		}
	}
	
	public function cost() {
		
		$id = url::param();
		$param = url::param(1);
		$application = $this->request->application;
		
		// order sheet
		$launchplan = new Modul($application);
		$launchplan->setTable('lps_launchplans');
		$data = $launchplan->read($id);

		// state handler
		$state = new State($data['lps_launchplan_workflowstate_id']); 
		$state->setOwner($data['lps_launchplan_address_id']);
		$state->setDateIntervals($data['lps_launchplan_openingdate'], $data['lps_launchplan_closingdate']);
		
		// check access to order sheet
		$this->checkAccess($launchplan);
		$this->buildHeader($launchplan);
		$this->buildTabs($launchplan, $state);

		// archived state
		if (!$this->request->archived && $state->isExported()) {
			url::redirect($this->request->query("archived/data/$id"));
		}
		
		// db model
		$model = new Model($application);

		// framework vars
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['archived'] = $this->request->archived;
		$data['action'] = $this->request->action;

		// buttons
		$buttons = array();

		$buttons['back'] = ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $this->request->query(),
			'label' => translate::instance()->back
		));


		if ($state->onCompleting()) {
			$sth = $model->db->prepare("
				SELECT 
					lps_launchplan_item_week_quantity_item_id AS id,
					SUM(lps_launchplan_item_week_quantity_quantity) as quantity,
					SUM(lps_launchplan_item_week_quantity_quantity_approved) AS quantity_approved,
					lps_launchplan_items.lps_launchplan_item_price AS price,
					lps_product_group_id AS group_id, 
					lps_product_group_name AS group_name
				FROM lps_launchplan_item_week_quantities 
				INNER JOIN lps_launchplan_items ON lps_launchplan_item_week_quantity_item_id = lps_launchplan_item_id
				INNER JOIN lps_references ON lps_reference_id = lps_launchplan_item_reference_id
				INNER JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id
				WHERE lps_launchplan_item_launchplan_id = ?
				GROUP BY lps_launchplan_item_week_quantity_item_id
			");
		} else {
			$sth = $model->db->prepare("
				SELECT DISTINCT
					lps_launchplan_item_id AS id,
					lps_launchplan_item_price AS price,
					lps_launchplan_item_quantity AS quantity,
					lps_launchplan_item_quantity_approved AS quantity_approved,
					lps_product_group_id AS group_id, 
					lps_product_group_name AS group_name
				FROM lps_launchplan_items
				INNER JOIN lps_references ON lps_reference_id = lps_launchplan_item_reference_id
				INNER JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id
				WHERE lps_launchplan_item_launchplan_id = ?
			");
		}

		$sth->execute(array($launchplan->id));
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				
				$group = $row['group_id'];
				$item = $row['id'];
				
				$datagrid[$group]['caption'] = $row['group_name'];
				$datagrid[$group]['data'][$item]['quantity'] = $row['quantity'];
				$datagrid[$group]['data'][$item]['quantity_approved'] = $row['quantity_approved'];
				$datagrid[$group]['data'][$item]['calc_quantity'] = $row['quantity']*$row['price']; 
				$datagrid[$group]['data'][$item]['calc_quantity_approved'] = $row['quantity_approved']*$row['price']; 
			}
		}
			
		// template
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'launchplan/cost.php');
		$tpl->data('datagrid', $datagrid);
		$this->view->setTemplate('ordersheet.versions', $tpl);
	}
	
	public function planning() { 
		
		$id = url::param();
		$version = url::param(1);
		$application = $this->request->application;
		
		// order sheet
		$launchplan = new Modul($application);
		$launchplan->setTable('lps_launchplans');
		$data = $launchplan->read($id);

		// state handler
		$state = new State($data['lps_launchplan_workflowstate_id']); 
		$state->setOwner($data['lps_launchplan_address_id']);
		$state->setDateIntervals($data['lps_launchplan_openingdate'], $data['lps_launchplan_closingdate']);

		$company = new Company();
		$company->read($launchplan->data['lps_launchplan_address_id']);

		$model = new Model($application);

		// filters
		$fDistChannel = $_REQUEST['distribution_channel'];
		$fTurnoverWatch = $_REQUEST['turnoverclass_watch'];
		$fSaleRepresentative = $_REQUEST['sale_representative'];
		$fDecorationPerson = $_REQUEST['decoration_person'];
		$fProvinces = $_REQUEST['province'];

		// form action
		$url = $this->request->query("planning/$id");
		$formAction = ($fVersion) ? "$url/$version" : $url;

		// pagetitle
		$header = $this->buildHeader($launchplan, false);
		$this->view->pagetitle = $header['title'].', '.$header['subtitle'];

		$filters = array();

		// filter: pos
		$filters['default'] = "posaddress_client_id = $company->id";

		// filter: distribution channels
		if ($fDistChannel) {
			$filters['distribution_channels'] = "posaddress_distribution_channel = $fDistChannel";
		}

		// filter: turnover classes
		if ($fTurnoverWatch) {
			$filters['turnoverclass_watches'] = "posaddress_turnoverclass_watches = $fTurnoverWatch";
		}

		// filter: sales represenatives
		if ($fSaleRepresentative) {
			$filters['sales_representative'] = "posaddress_sales_representative = $fSaleRepresentative";
		}

		// filter: decoration persons
		if ($fDecorationPerson) {
			$filters['decoration_person'] = "posaddress_decoration_person = $fDecorationPerson";
		}

		// provinces
		if ($fProvinces) {
			$filters['provinces'] = "province_id = $fProvinces";
		}

		// for archived ordersheets show only POS locations which has distributed quantities 
		if ($state->isExported()) {
			
			$sth = $model->db->prepare("
				SELECT DISTINCT mps_ordersheet_item_delivered_posaddress_id AS pos
				FROM mps_ordersheet_item_delivered	
			");

			$filters['active'] = "posaddress_id IN ($exportedPosLocations)";

		} else {
			$filters['active'] = "(posaddress_store_closingdate = '0000-00-00' OR posaddress_store_closingdate IS NULL OR posaddress_store_closingdate = '')";
		}

		// dropdown: dist channels
		$result = $model->query("
			SELECT DISTINCT
				mps_distchannel_id AS value,
				CONCAT(mps_distchannel_name, ' - ', mps_distchannel_code) AS caption
			FROM db_retailnet.posaddresses
			INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
			INNER JOIN db_retailnet.provinces ON province_id = place_province
			INNER JOIN db_retailnet.mps_distchannels ON mps_distchannel_id = posaddress_distribution_channel
		")
		->filter($filters)
		->exclude('distribution_channel')
		->order('caption')
		->fetchAll();

		if ($result) {				
			$dropdowns[] = ui::dropdown($result, array(
				'id' => 'distribution_channel',
				'name' => 'distribution_channel',
				'value' => $fDistChannel,
				'caption' => translate::instance()->all_distribution_channels
			));
		}

		// dropdown: turnover watches
		$result = $model->query("
			SELECT DISTINCT
				mps_turnoverclass_id AS value, 
				mps_turnoverclass_name AS caption
			FROM db_retailnet.posaddresses
			INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
			INNER JOIN db_retailnet.provinces ON province_id = place_province
			INNER JOIN db_retailnet.mps_turnoverclasses ON mps_turnoverclass_id = posaddress_turnoverclass_watches
		")
		->filter('company', "posaddress_franchisee_id = $company->id")
		->exclude('turnoverclass_watch')
		->order('caption')
		->fetchAll();

		if ($result) {				
			$dropdowns[] = ui::dropdown($result, array(
				'id' => 'turnoverclass_watch',
				'name' => 'turnoverclass_watch',
				'value' => $fTurnoverWatch,
				'caption' => translate::instance()->all_turnover_class_watches
			));
		}

		// dropdown: sales representatives
		$model->query("
			SELECT DISTINCT
				mps_staff_id As value,
				CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS caption
			FROM db_retailnet.posaddresses
			INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
			INNER JOIN db_retailnet.provinces ON province_id = place_province
			INNER JOIN db_retailnet_mps.mps_staffs ON mps_staff_id = posaddress_sales_representative
		")
		->filter($filters)
		->filter('type', 'mps_staff_staff_type_id = 1')
		->exclude('sale_representative')
		->order('mps_staff_name, mps_staff_firstname');
		
		if ($state->owner) {
			$address = User::instance()->address;
			$model->filter('limited', "mps_staff_address_id= $address");
		}
		
		$result = $model->fetchAll();

		if ($result) {
			$dropdowns[] = ui::dropdown($result, array(
				'name' => 'sale_representative',
				'id' => 'sale_representative',
				'value' => $fSaleRepresentative,
				'caption' => translate::instance()->all_sales_representatives
			));
		}

		// dropdown: decoration persons
		$model->query("
			SELECT DISTINCT
				mps_staff_id AS value,
				CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS caption
			FROM db_retailnet.posaddresses
			INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
			INNER JOIN db_retailnet.provinces ON province_id = place_province
			INNER JOIN db_retailnet_mps.mps_staffs ON mps_staff_id = posaddress_decoration_person
		")
		->filter($filters)
		->filter('staff_type', 'mps_staff_staff_type_id = 2')
		->exclude('decoration_person')
		->order('mps_staff_name, mps_staff_firstname');
		
		if ($state->owner) {
			$address = User::instance()->address;
			$model->filter('limited', "mps_staff_address_id= $address");
		}
		
		$result = $model->fetchAll();

		if ($result) {
			$dropdowns[] = ui::dropdown($result, array(
				'name' => 'decoration_person',
				'id' => 'decoration_person',
				'value' => $fDecorationPerson,
				'caption' => translate::instance()->all_decoration_persons
			));
		}

		// dropdown provinces
		$result = $model->query("
			SELECT DISTINCT
					province_id AS id, 
					province_canton AS caption
				FROM db_retailnet.posaddresses
				INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
				INNER JOIN db_retailnet.provinces ON province_id = place_province
		")
		->filter($filters)
		->exclude('province')
		->order('caption')
		->fetchAll();
		
		if ($result) {
			$dropdowns[] = ui::dropdown($result, array(
				'name' => 'province',
				'id' => 'province',
				'value' => $fProvinces,
				'caption' => translate::instance()->all_provinces
			));
		}


		// get all active company warehouses
		$sth = $model->db->prepare("
			SELECT 
				address_warehouse_id AS id,
				address_warehouse_name AS caption
			FROM db_retailnet.address_warehouses
			WHERE address_warehouse_active = 1 AND address_warehouse_stock_reserve IS NULL AND address_warehouse_address_id = ?
		");

		$sth->execute(array($company->id));
		$companyWarehouses = $sth->fetchAll();

		// add new warehouse 
		$warehouses = array('new' => 'Add New Warehouse');

		if ($companyWarehouses) {

			// get existed order sheet warehoses
			$sth = $model->db->prepare("
				SELECT 
					lps_launchplan_warehouse_address_warehouse_id AS warehouse
				FROM db_retailnet.address_warehouses
				INNER JOIN lps_launchplan_warehouses ON lps_launchplan_warehouse_address_warehouse_id = address_warehouse_id
				WHERE lps_launchplan_warehouse_launchplan_id = ? 
			");

			$sth->execute(array($launchplan->id));
			$result = $sth->fetchAll();

			$orderSheetWarehouses = array();

			if ($result) {
				foreach ($result as $row) $orderSheetWarehouses[] = $row['warehouse'];
			}
			
			foreach ($companyWarehouses as $row) {
				if (!in_array($row['id'], $orderSheetWarehouses)) {
					$warehouses[$row['id']] = $row['caption'];
				}
			}
		}
		
		// filters button
		$buttons['pop_filter'] = array(
			'icon' => 'direction-down',
			'data' => '#filters',
			'label' => 'Filters'
		);
			
		// button: print
		$buttons['print'] = array(
			'icon' => 'print',
			'class' => 'print-ajax',
			'href' => $this->request->link('/applications/modules/launchplan/planning/print.php', array('launchplan' => $id)),
			'label' => 'Print'
		);

		// planning mod
		$modPlanning = ($state->onPreparation() || $state->onConfirmation()) ? true : false;

		// distribution mod
		$modDistribution = ($state->onDistribution() || $state->isDistributed()) ? true : false;

		// request fields
		$this->request->field('launchplan', $id);
		$this->request->field('version', $version);
		$this->request->field('checkQuantities', $state->onApproving() ? 0 : 1);

		$this->view->master('spreadsheet.php');
		
		// template
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'launchplan/planning.php');
		$tpl->data('dropdowns', $dropdowns);
		$tpl->data('buttons', $buttons);
		$tpl->data('formAction', $formAction);
		$tpl->data('modeClass', $modPlanning ? 'planning' : 'distribution');
		$tpl->data('warehouses', $warehouses);
		$this->view->setTemplate('planning', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/spreadsheet/spreadsheet.css',
			'/public/scripts/spreadsheet/spreadsheet.js',
			'/public/scripts/tipsy/tipsy.css',
			'/public/scripts/tipsy/tipsy.js',
			'/public/scripts/pop/pop.css',
			'/public/scripts/pop/pop.js',
			'/public/css/launchplan.planning.css',
			'/public/js/launchplan.planning.js'
		));
	}
	
	public function manage() {

		$application = $this->request->application;

		$permission_manage = user::permission('can_manage_lps_sheets');

		// check access to page
		if (!$permission_manage || $this->request->archived) {
			message::access_denied();
			url::redirect($this->request->query());
		}

		// db model
		$model = new Model($this->request->application);

		// dataloader workflow states
		$sth = $model->db->prepare("
			SELECT DISTINCT lps_mastersheet_year, lps_mastersheet_year AS year 
			FROM lps_mastersheets
			WHERE lps_mastersheet_archived = 0 AND lps_mastersheet_consolidated = 0
			ORDER BY lps_mastersheet_year
		");

		$sth->execute();
		$result = $sth->fetchAll();
		$dataloader['mps_mastersheet_year'] = _array::extract($result);

		// dataloader workflow states
		$sth = $model->db->prepare("
			SELECT 
				lps_workflow_state_id,
				lps_workflow_state_name
			FROM lps_workflow_states
			WHERE lps_workflow_state_id IN (1,2)
		");

		$sth->execute();
		$result = $sth->fetchAll();
		$dataloader['mps_ordersheet_workflowstate_id'] = _array::extract($result);
		
		// buttons
		$buttons = array();
		$buttons['back'] = $this->request->query();
		$buttons['add'] = '/applications/modules/ordersheet/manage/create.php';
		$buttons['update'] = '/applications/modules/ordersheet/manage/update.php';
		$buttons['delete_ordersheets'] = '/applications/modules/ordersheet/manage/delete.php';
		$buttons['items'] = '/applications/modules/ordersheet/manage/items.save.php';
		$buttons['delete_items'] = '/applications/modules/ordersheet/manage/items.delete.php';
		
		// form dataloader
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['action'] = $this->request->action;
		$data['redirect'] = $this->request->url;
		$data['selected_tab'] = 'new_ordersheets';

		// template: list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'ordersheet/manage.php');
		$tpl->data('data', $data);
		$tpl->data('dataloader', $dataloader);
		$tpl->data('buttons', $buttons);

		// assign template to view
		$this->view->setTemplate('ordersheet.manage', $tpl);

		Compiler::attach(array(
			'/public/scripts/jquery/jquery.ui.css',
			'/public/scripts/jquery/jquery.ui.js',
			'/public/css/jquery.ui.css',
			'/public/scripts/validationEngine/css/validationEngine.jquery.css',
			'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
			'/public/scripts/validationEngine/js/jquery.validationEngine.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/scripts/chain/chained.select.js',
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js',
			'/public/js/ordersheet.manager.js'
		));
	}
	
	public function submit() {
		
		// request
		$application = $this->request->application;
		$permission_manage = user::permission('can_manage_lps_sheets');
		$state = 1;
		$mailTemplate = 27;
		
		// check access to page
		if (!$permission_manage || $this->request->archived) {
			message::access_denied();
			url::redirect($this->request->query());
		}
			
		// Mail Template
		$mail = new Mail_Template();
		$maildata = $mail->read($mailTemplate);
		$maildata['mail_template_text'] = nl2br($maildata['mail_template_text']);
		
		// button
		$buttons = array();
		$buttons['back'] = $this->request->query();
		$buttons['submit_sendmail'] = true;
		$buttons['submit'] = true;
		
		// template: submit list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'launchplan/submit.php');
		$tpl->data('dataloader', $dataloader);
		$tpl->data('buttons', $buttons);
		$tpl->data('data', $data);
		$tpl->data('modal_caption', 'Submit Launch Plans');
		$tpl->data('maildata', $maildata);
		$tpl->data('workflow_states', $state);
		
		// assign template to view
		$this->view->setTemplate('ordersheet.submit', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js',
			'/public/scripts/textarea.expander.js',
			'/public/js/launchplan.submit.js'
		));

	}
	
	public function remind() {

		// request
		$application = $this->request->application;
		$permission_manage = user::permission('can_manage_lps_sheets');
		$state = '2,3';
		$mailTemplate = 26;
		
		// check access to page
		if (!$permission_manage || $this->request->archived) {
			message::access_denied();
			url::redirect($this->request->query());
		}
			
		// Mail Template
		$mail = new Mail_Template();
		$maildata = $mail->read($mailTemplate);
		$maildata['mail_template_text'] = nl2br($maildata['mail_template_text']);
		
		// button
		$buttons = array();
		$buttons['back'] = $this->request->query();
		$buttons['remind'] = true;
		
		// template: remind list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'launchplan/submit.php');
		$tpl->data('dataloader', $dataloader);
		$tpl->data('buttons', $buttons);
		$tpl->data('data', $data);
		$tpl->data('modal_caption', 'Remind Launch Plans');
		$tpl->data('maildata', $maildata);
		$tpl->data('workflow_states', $state);
		
		// assign template to view
		$this->view->setTemplate('ordersheet.submit', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js',
			'/public/scripts/textarea.expander.js',
			'/public/js/launchplan.submit.js'
		));
	}

	private function buildTabs($launchplan, $state) {

		$showPlanningTab = false;

		$application = $this->request->application;
		$model = new Model($application);

		// has order sheet items
		$sth = $model->db->prepare("
			SELECT COUNT(lps_launchplan_item_id) as total
			FROM lps_launchplan_items 
			WHERE lps_launchplan_item_launchplan_id = ?
		");

		$sth->execute(array($launchplan->id));
		$items = $sth->fetch();
		
		if ($items['total']) {

			$showPlanningTab = true;
			
			/*
			if (!$state->onPreparation()) {

				// has order sheet approved items
				$sth = $model->db->prepare("
					SELECT COUNT(lps_launchplan_item_id) as total
					FROM lps_launchplan_items 
					WHERE lps_launchplan_item_launchplan_id = ? AND lps_launchplan_item_quantity_approved > 0
				");

				$sth->execute(array($launchplan->id));
				$result = $sth->fetch();

				$showPlanningTab = $result['total']>0 ? true : false; 
			}
			*/

		} else {
			$tab = substr($this->request->query('items'), 1);
			$this->request->exclude($tab);
		}
		
		// exclude items tabs
		if (!$showPlanningTab) { 
			$tab = substr($this->request->query('planning'), 1); 
			$this->request->exclude($tab);
		}

		// popup planning tab
		$planningTab = substr($this->request->query('planning'),1);
		
		$this->view
		->tabs('pagecontent')
		->navigation('tab')
		->data('data', array(
			$planningTab => array(
				'icon' => "<span class='icon icon46'></span>",
				'class' => 'popup',
				'caption' => ($state->isDistributed()) ? 'Export' : 'Planning',
				'name' => 'PlanningSpreadsheet'
			)
		));
		
	}

	private function buildHeader($launchplan, $view=true, $extendTitle=null) {

		$application = $this->request->application;
		$model = new Model($application);

		// template: headers
		$sth = $model->db->prepare("
			SELECT 
				CONCAT(address_company, ', ', country_name, ', ', lps_mastersheet_year, ', ', lps_mastersheet_name) AS caption,
				lps_workflow_state_name AS state,
				lps_launchplan_customer_number AS customer_number
			FROM lps_launchplans
			INNER JOIN lps_mastersheets ON lps_mastersheet_id = lps_launchplan_mastersheet_id
			INNER JOIN db_retailnet.addresses ON address_id = lps_launchplan_address_id
			INNER JOIN db_retailnet.countries ON country_id = address_country
			INNER JOIN lps_workflow_states ON lps_workflow_state_id = lps_launchplan_workflowstate_id
			WHERE lps_launchplan_id = ?
		");
		
		$sth->execute(array($launchplan->id));
		$header = $sth->fetch();

		$subtitle = join(', ', array_filter(array(
			'State: '.$header['state'],
			'Customer Number: '.$header['customer_number'],
			$extendTitle
		)));

		$data = array(
			'title' => $header['caption'],
			'subtitle' => $subtitle
		);

		if ($this->view && $view) {
			$this->view->header('pagecontent')->node('header')->data($data);
		}

		return $data;
	}

	private function checkAccess($launchplan) {

		$access = false;
		$user = User::instance(); 
		
		if ($launchplan->id && $user->id) {

			$client = $launchplan->data['lps_launchplan_address_id'];
	
			// administrator access
			if (static::$permissions['manage'] || static::$permissions['view'] || static::$permissions['edit']) {
				$access = true;
			} 
			// owner access edit.limited
			elseif (static::$permissions['view.limited'] || static::$permissions['edit.limited']) {
				$access = $client == $user->address ? true : false;
			}

			// country access
			if (!$access && $client) {

				$company = new Company();
				$company->read($client);

				$model = new Model(Connector::DB_CORE);
				
				$result = $model->query("
					SELECT COUNT(country_access_id) as total
					FROM country_access
					WHERE country_access_user = $user->id AND country_access_country = $company->country
				")->fetch();

				$access = $result['total'] > 0 ? true : false;
			}

			// regional access
			if (!$access) {

				// regional access companies
				$regionalAccessCompanies = User::getRegionalAccessCompanies();

				if ($regionalAccessCompanies) {
					foreach ($regionalAccessCompanies as $company) {
						if ($client==$company) {
							$access = true;
							break;
						}
					}
				}
			}			
		}

		if (!$access) {
			message::access_denied();
			url::redirect($this->request->query());
		}
	}

}
