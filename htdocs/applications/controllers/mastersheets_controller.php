<?php 

class Mastersheets_Controller {
	
	public function __construct() {
		
		$this->request = request::instance();
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}
	
	public function index() {
		
		$permissionEdit = false;
		
		switch ($this->request->application) {
			
			case 'mps':
					$permissionEdit = user::permission('can_edit_mps_master_sheets');
			break;

			case 'lps':
				$permissionEdit = user::permission('can_edit_lps_master_sheets');
			break;
		}

		// link:: show announcement
		$link = $this->request->query('data');
		$this->request->field('data', $link);

		// button: add new
		if (!$this->request->archived && $permissionEdit) {
			$link = $this->request->query('add');
			$this->request->field('add', $link);
		}

		// template: list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'list.php');
		$tpl->data('url', '/applications/modules/mastersheet/list.php');
		$tpl->data('class', 'list-1000');

		// assign template to view
		$this->view->setTemplate('mastersheet', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js'
		));
	}
	
	public function add() {
		
		$permissionEdit = false;

		switch ($this->request->application) {
			
			case 'mps':
				$permissionEdit = user::permission('can_edit_mps_master_sheets');
				$urlSubmit = '/applications/modules/mastersheet/submit.php';
			break;

			case 'lps':
				$permissionEdit = user::permission('can_edit_lps_master_sheets');
				$urlSubmit = '/applications/modules/mastersheet/submit.php';
			break;
		}
	
		// check access
		if($this->request->archived || !$permissionEdit) {
			Message::access_denied();
			url::redirect($this->request->query());
		}
		
		// form dataloader
		$data = array();
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['redirect'] = $this->request->query('data');
			
		// buttons
		$buttons = array();
		$buttons['back'] = $this->request->query();
		$buttons['save'] = $urlSubmit;

		// dataloader: week number first
		for ($i=1; $i < 53; $i++) { 
			$dataloader['mastersheet_week_number_first'][$i] = $i;
		}

		// load template
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'mastersheet/form.php');
		$tpl->data('data', $data);
		$tpl->data('dataloader', $dataloader);
		$tpl->data('buttons', $buttons);

		// assign template to view
		$this->view->setTemplate('mastersheet', $tpl);

		Compiler::attach(array(
			'/public/scripts/jquery/jquery.ui.css',
			'/public/scripts/jquery/jquery.ui.js',
			'/public/css/jquery.ui.css',
			'/public/scripts/validationEngine/css/validationEngine.jquery.css',
			'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
			'/public/scripts/validationEngine/js/jquery.validationEngine.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/js/form.validator.js'
		));
	}
	
	public function data() {
			
		$id = url::param();
		$permissionEdit = false;

		switch ($this->request->application) {
			
			case 'mps':
				$tableName = 'mps_mastersheets';
				$tablePrefix = 'mps_';
				$permissionEdit = user::permission('can_edit_mps_master_sheets');
				$urlSubmit = '/applications/modules/mastersheet/submit.php';
				$urlDelete = '/applications/modules/mastersheet/delete.php';
				$urlArchiv = '/applications/modules/mastersheet/archive.php';
				$urlExchange = '/applications/modules/mastersheet/exchange.reset.php';
				$urlPrint = '/applications/exports/excel/mastersheet.splitting.list.php';

				$queryPrintGroup = "
					SELECT DISTINCT
						mps_material_planning_type_id, 
					    mps_material_planning_type_name
					FROM mps_ordersheet_items
					INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
					INNER JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id
					INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
					WHERE mps_ordersheet_mastersheet_id = ? 
					ORDER BY mps_material_planning_type_name
				";

				$printOptions = array(
					'proposed_quantity' => 'Proposed Quantity',
					'desired_quantity' => 'Desired Quantity by Customer',
					'desired_quantity_total' => 'Total Cost of Desired Quantity',
					'approved_quantity' => 'Approved Quantity',
					'approved_quantity_total' => 'Total Cost of Approved Quantity',
					'ordered_quantity' => 'Pre Ordered Quantity',
					'ordered_quantity_total' => 'Total Cost of Pre Ordered Quantity'
				);

				$printGroupCaption = 'Splitting List';

			break;

			case 'lps':
				$tableName = 'lps_mastersheets';
				$tablePrefix = 'lps_';
				$permissionEdit = user::permission('can_edit_lps_master_sheets');
				$urlSubmit = '/applications/modules/mastersheet/submit.php';
				$urlDelete = '/applications/modules/mastersheet/delete.php';
				$urlArchiv = '/applications/modules/mastersheet/archive.php';
				$urlExchange = '/applications/modules/mastersheet/exchange.reset.php';
				$urlPrint = '/applications/modules/mastersheet/print.detail.list.php';

				$queryIsMasterSheetInProcces = "
					SELECT COUNT(lps_launchplan_id) as total
					FROM lps_launchplans
					WHERE lps_launchplan_mastersheet_id = ? AND lps_launchplan_workflowstate_id > 1
				";

				$queryPrintGroup = "
					SELECT DISTINCT
						lps_product_group_id,
					    lps_product_group_name
					FROM lps_launchplan_items
					INNER JOIN lps_references ON lps_reference_id = lps_launchplan_item_reference_id
					INNER JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id
					INNER JOIN lps_launchplans ON lps_launchplan_id = lps_launchplan_item_launchplan_id
					WHERE lps_launchplan_mastersheet_id = ?
					ORDER BY lps_product_group_name
				";

				$printOptions = array(
					'estimate_quantity' => 'Estimate Quantity',
					'desired_quantity' => 'Total Launch Quantity',
					'desired_quantity_total' => 'Total Launch Cost',
					'approved_quantity' => 'Approved Quantity',
					'approved_quantity_total' => 'Total Approved Cost'
				);

				$printGroupCaption = 'Detail List';

			break;
		}

		// get mastersheet
		$mastersheet = new Modul($this->request->application);
		$mastersheet->setTable($tableName);
		$mastersheet->read($id);
		$data = _array::replaceKey($tablePrefix, null, $mastersheet->data);

		// check access
		if (!$mastersheet->id) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		// if archived master sheet is in non-archived level
		if (!$this->request->archived && $data['mastersheet_archived']) {
			url::redirect($this->request->query("archived/data/$id"));
		}
		
		// db model
		$model = new Model($this->request->application);
		
		// form dataloader
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;

		$data['mastersheet_pos_date'] = date::system($data['mastersheet_pos_date']);
		$data['mastersheet_phase_out_date'] = date::system($data['mastersheet_phase_out_date']);
		
		// buttons
		$buttons = array();
		$buttons['back'] = $this->request->query();

		if (!$this->request->archived) { 
					
			if ($permissionEdit) {
			
				$buttons['save'] = $urlSubmit;

				$integrity = new Integrity();
				$integrity->set($id, $tableName);

				if ($id && $integrity->check()) {
					$buttons['delete'] = $this->request->link($urlDelete, array('id'=>$id));
				}

				switch ($this->request->application) {
			
					case 'mps':
						$ordersheets = $model->query("
							SELECT COUNT(mps_ordersheet_id) as total
							FROM mps_ordersheets
							WHERE mps_ordersheet_workflowstate_id NOT IN (6,11,12,13,14,15,16) AND mps_ordersheet_mastersheet_id = $mastersheet->id
						")->fetch();
					break;

					case 'lps':
						$ordersheets = $model->query("
							SELECT COUNT(lps_launchplan_id) as total
							FROM lps_launchplans
							WHERE lps_launchplan_workflowstate_id NOT IN (8,10) AND lps_launchplan_mastersheet_id = $mastersheet->id
						")->fetch();
						
					break;
				}
				
				// button: put in archive
				//if ($ordersheets['total']>0) {
					$buttons['archive'] = $this->request->link($urlArchiv, array('id'=>$id));	
				//}

			} else {
				$disabled = true;
			}

		} else {
			$disabled = true;
		}

		// button: reset
		if (Settings::init()->devmod) {
			//$buttons['reset_exchange_data'] = $this->request->link($urlExchange, array('id'=>$id));
		}

		// dataloader: week number first
		for ($i=1; $i < 53; $i++) { 
			$dataloader['mastersheet_week_number_first'][$i] = $i;
		}

		// disabled fields
		if ($disabled) {
			$fields = array_keys($data);
			$disabled = array_fill_keys($fields, true);
		}
		// has master sheet launch plans in progres
		elseif ($queryIsMasterSheetInProcces) {

			$sth = $model->db->prepare($queryIsMasterSheetInProcces);
			$sth->execute(array($mastersheet->id));
			$res = $sth->fetch();

			if ($res['total']) {
				$disabled['mastersheet_week_number_first'] = true;
				$disabled['mastersheet_weeks'] = true;
			}
		}

		// detail list
		if ($this->request->archived && $queryPrintGroup) {
			$sth = $model->db->prepare($queryPrintGroup);
			$sth->execute(array($id));
			$result = $sth->fetchAll();
			$printGroups = _array::extract($result);
			$buttons['detail-list'] = $printGroups ? $this->request->link($urlPrint, array('mastersheet'=>$id)) : false;
		}
		
		// headers
		$header = $data['mastersheet_name'].', '.$data['mastersheet_year'];
		$this->view->header('pagecontent')->node('header')->data('title', $header);
		$this->view->tabs('pagecontent')->navigation('tab');
		
		// load template
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'mastersheet/form.php');
		$tpl->data('data', $data);
		$tpl->data('printGroupCaption', $printGroupCaption);
		$tpl->data('printGroups', $printGroups);
		$tpl->data('printOptions', $printOptions);
		$tpl->data('dataloader', $dataloader);
		$tpl->data('disabled', $disabled);
		$tpl->data('buttons', $buttons);

		// assign template to view
		$this->view->setTemplate('mastersheet', $tpl);

		Compiler::attach(array(
			'/public/scripts/jquery/jquery.ui.css',
			'/public/scripts/jquery/jquery.ui.js',
			'/public/css/jquery.ui.css',
			'/public/scripts/validationEngine/css/validationEngine.jquery.css',
			'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
			'/public/scripts/validationEngine/js/jquery.validationEngine.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/js/form.validator.js',
			'/public/js/mastersheet.form.js'
		));
	}
	
	public function items() {
		
		$id = url::param();
		$permissionEdit = false;
		$application = $this->request->application;

		switch ($application) {
			
			case 'mps':
				$tableName = 'mps_mastersheets';
				$tablePrefix = 'mps_';
				$permissionEdit = user::permission('can_edit_mps_master_sheets');
				$urlScript = '/public/js/mastersheet.items.js';
			break;

			case 'lps':
				$tableName = 'lps_mastersheets';
				$tablePrefix = 'lps_';
				$permissionEdit = user::permission('can_edit_lps_master_sheets');
				$urlScript = '/public/js/mastersheet.reference.js';
			break;
		}
		
		// get mastersheet
		$mastersheet = new Modul($application);
		$mastersheet->setTable($tableName);
		$mastersheet->read($id);
		
		// check access
		if (!$mastersheet->id) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		// if archived master sheet is in non-archived level
		if (!$this->request->archived && $mastersheet->archived) {
			url::redirect($this->request->query("archived/data/$id"));
		}
		
		// db model
		$model = new Model($application);

		// form dataloader
		$data = _array::replaceKey($tablePrefix, null, $mastersheet->data);

		// request field: master sheet
		$this->request->field('id', $id);
		
		// buttons
		$buttons['back'] = $this->request->query();
		
		// header
		$header = $data['mastersheet_name'].', '.$data['mastersheet_year'];
		$this->view->header('pagecontent')->node('header')->data('title', $header);
		$this->view->tabs('pagecontent')->navigation('tab');
		
		if (!$this->request->archived && $permissionEdit) {
				
			// button: add new items
			$buttons['add'] = true;

			// request field: editabled mode
			$this->request->field('editabled', true);

			switch ($application) {
		
				case 'mps':
					$result = $model->query("
						SELECT DISTINCT
							mps_material_planning_type_id,	
							mps_material_planning_type_name
						FROM mps_materials	
						INNER JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id
						WHERE mps_material_active = 1
					")->fetchAll();
					
					$dataloader['group'] = _array::extract($result);

				break;

				case 'lps':
					$result = $model->query("
						SELECT DISTINCT
							lps_collection_id,	
							lps_collection_code
						FROM lps_references	
						INNER JOIN lps_collections ON lps_collection_id = lps_reference_collection_id
						WHERE lps_reference_active = 1
					")->fetchAll();
					
					$dataloader['group'] = _array::extract($result);

				break;
			}
		}

		// load template
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'mastersheet/items.php');
		$tpl->data('data', $data);
		$tpl->data('dataloader', $dataloader);
		$tpl->data('buttons', $buttons);

		// assign template to view
		$this->view->setTemplate('mastersheet.items', $tpl);

		Compiler::attach(array(
			'/public/scripts/jquery/jquery.ui.css',
			'/public/scripts/jquery/jquery.ui.js',
			'/public/css/jquery.ui.css',
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js',
			'/public/scripts/chain/chained.select.js',
			'/public/scripts/pop/pop.css',
			'/public/scripts/pop/pop.js',
			$urlScript
		));
	}
	
	public function files() {
		
		$id = url::param();
		$param = url::param(1);
		$param = url::param(1);
		$application = $this->request->application;
		$permissionEdit = false;

		switch ($this->request->application) {
			
			case 'mps':
				$tablePrefix = 'mps_';
				$tableName = 'mps_mastersheets';
				$tableNameFiles = 'mps_mastersheet_files';
				$permissionEdit = user::permission('can_edit_mps_master_sheets');
			break;

			case 'lps':
				$tablePrefix = 'lps_';
				$tableName = 'lps_mastersheets';
				$tableNameFiles = 'lps_mastersheet_files';
				$permissionEdit = user::permission('can_edit_lps_master_sheets');
			break;
		}

		// get mastersheet
		$mastersheet = new Modul($application);
		$mastersheet->setTable($tableName);
		$data = $mastersheet->read($id);

		// check access
		if (!$mastersheet->id) {
			Message::access_denied();
			url::redirect($this->request->query());
		}

		// if archived master sheet is in non-archived level
		if (!$this->request->archived && $mastersheet->archived) {
			url::redirect($this->request->query("archived/data/$id"));
		}
		
		// db model
		$model = new Model($this->request->application);
		
		// form dataloader
		$data = _array::replaceKey($tablePrefix, null, $data);
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		
		// headers
		$header = $data['mastersheet_name'].', '.$data['mastersheet_year'];
		$this->view->header('pagecontent')->node('header')->data('title', $header);
		$this->view->tabs('pagecontent')->navigation('tab');
		
		// buttons
		$buttons = array();
		
		// show file form
		if ($param) {
			
			// master sheet file
			$param = (is_numeric($param)) ? $param : null;
			
			// button: back
			$buttons['back'] = $this->request->query("files/$id");

			// button: save
			if (!$this->request->archived && $permissionEdit) {	
				$buttons['save'] = '/applications/modules/mastersheet/file.submit.php';
			}
			
			if ($param) {

				$file = new Modul($application);
				$file->setTable($tableNameFiles);
				$fileData = $file->read($param);

				$fileData = _array::replaceKey($tablePrefix, null, $fileData);

				// form dataloader
				$fileData['file_id'] = $fileData['mastersheet_file_id'];
				$fileData['file_path'] = $fileData['mastersheet_file_path'];
				$fileData['file_title'] = $fileData['mastersheet_file_title'];
				$fileData['file_description'] = $fileData['mastersheet_file_description'];
				$fileData['file_visible'] = $fileData['mastersheet_file_visible'];

				// replace key prefix
				$fileData = _array::replaceKey($tablePrefix, null, $fileData);
				
				if ($buttons['save']) {
					
					$integrity = new Integrity();
					$integrity->set($param, $tableNameFiles);
					
					if ($integrity->check()) {
						$buttons['delete'] = $this->request->link('/applications/modules/mastersheet/file.delete.php', array('id'=>$param));
					}
				}
				elseif ($data) {
					$disabled = true;
				}
			}
			else {
				$fileData['redirect'] = $this->request->query("files/$id");
				$fileData['file_visible'] = 1;
			}

			// form dataloader
			$fileData['application'] = $this->request->application;
			$fileData['controller'] = $this->request->controller;
			$fileData['action'] = $this->request->action;
			$fileData['file_entity'] = $mastersheet->id;
			
			// load template
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'file/form.php');
			$tpl->data('data', $fileData);
			$tpl->data('dataloader', $dataloader);
			$tpl->data('buttons', $buttons);

			// assign template to view
			$this->view->setTemplate('mastersheet.file', $tpl);

			// script integration
			Compiler::attach(array(
				'/public/scripts/ajaxuploader/ajaxupload.js',
				'/public/scripts/validationEngine/css/validationEngine.jquery.css',
				'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
				'/public/scripts/validationEngine/js/jquery.validationEngine.js',
				'/public/scripts/qtip/qtip.css',
				'/public/scripts/qtip/qtip.js',
				'/public/js/form.validator.file.js'
			));
		}
		// show file list
		else {
			
			// button back
			$buttons['back'] = ui::button(array(
				'id' => 'back',
				'icon' => 'back',
				'href' => $this->request->query(),
				'label' => Translate::instance()->back
			));
			
			// button: add new file
			if (!$this->request->archived && $permissionEdit ) {
				$buttons['add'] = ui::button(array(
					'id' => 'add',
					'class' => 'add',
					'href' => $this->request->query("files/$id/add"),
					'label' => Translate::instance()->add_new
				));
			}
			
			// link: show/edit file
			if ($permission_view || $permission_edit) {
				$field = $this->request->query("files/$id");
				$this->request->field('form', $field);
			}

			
			$model = new Model($application);

			switch ($application) {
			
				case 'mps':
					$datagrid = $model->query("
						SELECT DISTINCT
							mps_mastersheet_file_id AS id,
							mps_mastersheet_file_path AS path,
							mps_mastersheet_file_title AS title,
							mps_mastersheet_file_description AS description,
							DATE_FORMAT(mps_mastersheet_files.date_created, '%d.%m.%Y') AS date,
							file_type_id AS type,
							file_type_name AS type_name
						FROM mps_mastersheet_files
						INNER JOIN db_retailnet.file_types ON file_type_id = mps_mastersheet_file_type
						WHERE mps_mastersheet_file_mastersheet_id = $id
						ORDER BY file_type_name, mps_mastersheet_file_title
					")->fetchAll();

				break;

				case 'lps':
					$datagrid = $model->query("
						SELECT DISTINCT
							lps_mastersheet_file_id AS id,
							lps_mastersheet_file_path AS path,
							lps_mastersheet_file_title AS title,
							lps_mastersheet_file_description AS description,
							DATE_FORMAT(lps_mastersheet_files.date_created, '%d.%m.%Y') AS date,
							file_type_id AS type,
							file_type_name AS type_name
						FROM lps_mastersheet_files
						INNER JOIN db_retailnet.file_types ON file_type_id = lps_mastersheet_file_type
						WHERE lps_mastersheet_file_mastersheet_id = $id
						ORDER BY file_type_name, lps_mastersheet_file_title
					")->fetchAll();

				break;
			}


			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'file/list.php');
			$tpl->data('data', $data);
			$tpl->data('datagrid', $datagrid);
			$tpl->data('buttons', $buttons);
			$tpl->data('link', $this->request->query("files/$id"));

			$this->view->setTemplate('mastersheet.file', $tpl);
		}
	}
	
	public function cost() {
		
		$id = url::param();
		
		// permissions
		$permission_view = user::permission(Mastersheet::PERMISSION_VIEW);
		$permission_edit = user::permission(Mastersheet::PERMISSION_EDIT);
			
		$mastersheet = new Mastersheet();
		$mastersheet->read($id);
		
		// check access te consolidation
		if ((!$permission_view && !$permission_edit) || !$mastersheet->id) {
			url::redirect('/messages/show/access_denied');
		}
		elseif (!$mastersheet->archived || !$this->request->archived) {
			message::access_denied();
			url::redirect($this->request->query());
		}
			
		// buttons
		$buttons = array();
		
		// button: back
		$buttons['back'] = $this->request->query();
		
		// button print
		//$buttons['print'] = $this->request->link('/applications/exports/excel/mastersheet.consolidation.cost.php', array('id'=>$id));
		
		// header
		$this->view->header('pagecontent')->node('header')->data('title', $mastersheet->header());
		$this->view->tabs('pagecontent')->navigation('tab');
			
		// template: master sheet consolidation costs
		$this->view->consolidationCosts('pagecontent')
		->mastersheet('consolidation.cost')
		->data('buttons', $buttons)
		->data('id', $id);
	}
	
	public function splittinglists() {
		
		$id = url::param();
		$param = url::param(1);
		$application = $this->request->application;

		switch ($application) {
			
			case 'mps':
				$permissionEdit = user::permission('can_edit_mps_master_sheets');
				$tableMasterSheet = 'mps_mastersheets';
				
				$urlPrint = '/applications/exports/excel/mastersheet.splitting.list.items.php';
				$urlPrintSplittingList = '/applications/exports/excel/mastersheet.splitting.list.version.php';

				$tableVersion = 'mps_mastersheet_splitting_lists';
				$mapVersion = array(
					'mps_mastersheet_splitting_list_name' => 'name',
					'mps_mastersheet_splitting_list_mastersheet_id' => 'mastersheet'
				);

				$mapMasterSheet = array(
					'mps_mastersheet_year' => 'mastersheet_year',
					'mps_mastersheet_name' => 'mastersheet_name',
					'mps_mastersheet_consolidated' => 'mastersheet_consolidated',
					'mps_mastersheet_archived' => 'mastersheet_archived'
				);

				$queryVersion = "
					SELECT
						mps_mastersheet_splitting_list_item_id AS id,
						mps_mastersheet_splitting_list_item_material_id AS reference_id,
						mps_mastersheet_splitting_list_item_price AS price,
						mps_mastersheet_splitting_list_item_total_quantity AS quantity,
						mps_mastersheet_splitting_list_item_total_quantity_approved AS quantity_approved,
						(mps_mastersheet_splitting_list_item_price * mps_mastersheet_splitting_list_item_total_quantity_approved) AS total_cost,
						mps_material_planning_type_id AS group_id,
						mps_material_planning_type_name AS group_name,
						mps_material_collection_category_id AS subgroup_id,
						mps_material_collection_category_code AS subgroup_name,
						mps_material_collection_code AS collection_code,
						mps_material_code AS reference_code,
						mps_material_name AS reference_name,
						mps_material_setof AS setof,
						currency_symbol
					FROM mps_mastersheet_splitting_list_items
					INNER JOIN mps_materials ON mps_material_id = mps_mastersheet_splitting_list_item_material_id
					LEFT JOIN mps_material_collections ON mps_material_collection_id = mps_material_material_collection_id
					LEFT JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id
					LEFT JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id
					INNER JOIN db_retailnet.currencies ON currency_id = mps_material_currency_id
					WHERE mps_mastersheet_splitting_list_item_splittinglist_id = ?
					ORDER BY
						mps_material_planning_type_name ASC,
						mps_material_collection_category_code ASC,
						mps_material_code ASC,
						mps_material_name ASC
				";

			break;

			case 'lps':
				$permissionEdit = user::permission('can_edit_lps_master_sheets');
				$tableMasterSheet = 'lps_mastersheets';
				
				$urlPrint = '/applications/modules/mastersheet/version/print.launchplan.items.php';
				$urlPrintSplittingList = '/applications/modules/mastersheet/version/print.detail.list.php';

				$tableVersion = 'lps_mastersheet_detail_lists';
				$mapVersion = array(
					'lps_mastersheet_detail_list_name' => 'name',
					'lps_mastersheet_detail_list_mastersheet_id' => 'mastersheet'
				);

				$mapMasterSheet = array(
					'lps_mastersheet_year' => 'mastersheet_year',
					'lps_mastersheet_name' => 'mastersheet_name',
					'lps_mastersheet_consolidated' => 'mastersheet_consolidated',
					'lps_mastersheet_archived' => 'mastersheet_archived',
					'lps_mastersheet_pos_date' => 'mastersheet_pos_date',
					'lps_mastersheet_phase_out_date' => 'mastersheet_phase_out_date',
					'lps_mastersheet_week_number_first' => 'mastersheet_week_number_first',
					'lps_mastersheet_weeks' => 'mastersheet_weeks',
					'lps_mastersheet_estimate_month' => 'mastersheet_estimate_month'
				);

				$queryVersion = "
					SELECT
						lps_mastersheet_detail_list_item_id AS id,
						lps_mastersheet_detail_list_item_reference_id AS reference_id,
						lps_mastersheet_detail_list_item_price AS price,
						lps_mastersheet_detail_list_item_total_quantity AS quantity,
						lps_mastersheet_detail_list_item_total_quantity_approved AS quantity_approved,
						lps_mastersheet_detail_list_item_total_quantity_estimate AS quantity_estimate,
						(lps_mastersheet_detail_list_item_price * lps_mastersheet_detail_list_item_total_quantity_approved) AS total_cost,
						lps_product_group_id AS group_id,
						lps_product_group_name AS group_name,
						lps_collection_code AS collection_code,
						lps_reference_code AS reference_code,
						lps_reference_name AS reference_name,
						currency_symbol
					FROM lps_mastersheet_detail_list_items
					INNER JOIN lps_references ON lps_reference_id = lps_mastersheet_detail_list_item_reference_id
					LEFT JOIN lps_collections ON lps_collection_id = lps_reference_collection_id
					LEFT JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id
					INNER JOIN db_retailnet.currencies ON currency_id = lps_reference_currency_id
					WHERE lps_mastersheet_detail_list_item_list_id = ?
					ORDER BY
						lps_product_group_name ASC,
						lps_reference_code ASC
				";

			break;
		}

		// order sheet
		$mastersheet = new Modul($application);
		$mastersheet->setTable($tableMasterSheet);
		$mastersheet->setDataMap($mapMasterSheet);
		$mastersheet->read($id);
		
		// check access te consolidation
		if (!$mastersheet->id) {
			message::access_denied();
			url::redirect($this->request->query());
		} elseif ($mastersheet->data['mastersheet_archived'] && !$this->request->archived) {
			url::redirect("/".$this->request->application."/mastersheets/archived/data/$id");
		}

		if ($param) {
			$version = new Modul($application);
			$version->setTable($tableVersion);
			$version->setDataMap($mapVersion);
			$version->read($param);
			$versionName = $version->data['name'];
		}
		
		// header
		$header = $mastersheet->data['mastersheet_name'].', '.$mastersheet->data['mastersheet_year'];
		$this->view->header('pagecontent')->node('header')->data('title', $header)->data('subtitle', $versionName);
		$this->view->tabs('pagecontent')->navigation('tab');
			
		// buttons
		$buttons = array();
		
		// splitting list details
		if ($param) {
		
			// db connector
			$model = new Model($application);

			// button: back
			$buttons['back'] = $this->request->query("splittinglists/$id");

			// filter: mastersheet
			$filters['mastersheet'] = "mps_mastersheet_splitting_list_item_splittinglist_id = $splittinglist";
			
			// get version items
			$sth = $model->db->prepare($queryVersion);
			$sth->execute(array($param));
			$result = $sth->fetchAll();
			
			if ($result) {

				foreach ($result as $row) {
					
					$group = $row['group_id'];
					$subgroup = $row['subgroup_id'];
					$key = $row['id'];
					
					$datagrid[$group]['caption'] = $row['group_name'];

					$item = array(
						'collection_code' => $row['collection_code'],
						'reference_code' => $row['reference_code'],
						'reference_name' => $row['reference_name'],
						'price' => $row['price'],
						'currency_symbol' => $row['currency_symbol'],
						'setof' => $row['setof'],
						'quantity' => $row['quantity'],
						'quantity_approved' => $row['quantity_approved'],
						'quantity_estimate' => $row['quantity_estimate'],
						'total_cost' => $row['total_cost']
					);
					
					if ($subgroup) {
						$datagrid[$group]['subgroups'][$subgroup]['caption'] = $row['subgroup_name'];
						$datagrid[$group]['subgroups'][$subgroup]['items'][$key] = $item;
					} else {
						$datagrid[$group]['items'][$key] = $item;
					}
				}
			}
			
			
			// button: splitting list dialog
			$buttons['splitting'] = $this->request->link($urlPrintSplittingList, array(
				'mastersheet' => $id,
				'version' => $param
			));
			
			// button: print item list
			$buttons['print'] = $this->request->link($urlPrint, array(
				'mastersheet' => $id,
				'version' => $param
			));
			
			// button: 
			if (!$mastersheet->consolidated) {
				$buttons['delete'] = $this->request->link('/applications/modules/mastersheet/splitting/delete.php', array(
					'mastersheet' => $id,
					'version' => $param
				));
			}
			
			$this->request->field('mastersheet', $id);
			$this->request->field('version', $param);

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'mastersheet/version.php');
			$tpl->data('datagrid', $datagrid);
			$tpl->data('buttons', $buttons);
			$this->view->setTemplate('mastersheet.version', $tpl);
		}
		else {	
		
			// button: back
			$buttons['back'] = $this->request->query();
			
			// link to detail view
			$link = $this->request->query("splittinglists/$id");
			$this->request->field('form', $link);
			$this->request->field('id', $id);

			// template: list
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'list.php');
			$tpl->data('url', '/applications/modules/mastersheet/splitting/list.php');
			$tpl->data('class', 'list-700');

			// assign template to view
			$this->view->setTemplate('mastersheet', $tpl);

			Compiler::attach(array(
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js'
			));
		}
	}
}