<?php 

	class FileTypes_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			$this->request->application = url::application();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			// permissions
			$permission_edit = user::permission(File_Type::PERMISSION_EDIT);
			
			// request form: add button
			if ($permission_edit) {
				$field = $this->request->query('add');
				$this->request->field('add', $field);
			}
			
			// request form: edit link
			$field = $this->request->query('data');
			$this->request->field('form', $field);
			
			$this->view->fileTypes('pagecontent')
			->file('type.list');
		}
		
		public function add() {
			
			// permissions
			$permission_edit = user::permission(File_Type::PERMISSION_EDIT);
				
			if (!$permission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
		
			
			// template: file type form
			$this->view->fileTypeForm('pagecontent')
			->file('type.data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function data() {
			
			$id = url::param();
			
			$fileType = new File_Type();
			$fileType->read($id);
				
			if (!$fileType->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// permissions
			$permission_edit = user::permission(File_Type::PERMISSION_EDIT);
			
			// form dataloader
			$data = $fileType->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// button: save
			if ($permission_edit) {
				
				$buttons['save'] = true;
				
				$integrity = new Integrity();
				$integrity->set($id, 'file_types');
			
				// button: delete
				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/file.type.delete.php', array('id' => $id));
				}
			}
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}

			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $fileType->name);
			
			// template: navigation tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');

			// template: file type form				
			$this->view->fileType('pagecontent')
			->file('type.data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function applications() {
			
			$id = url::param();
			
			$fileType = new File_Type();
			$fileType->read($id);
				
			if (!$fileType->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			// permissions
			$permission_edit = user::permission(File_Type::PERMISSION_EDIT);
			
			// form dataloader
			$data = $fileType->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $fileType->name);
			
			// template: navigation tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			$this->view->fileTypeApplications('pagecontent')
			->file('type.application.list')
			->data('buttons', $buttons)
			->data('id', $id);
		}

	}