<?php

class ExchangeOrphans_Controller {

	public function __construct() {
		
		$this->request = request::instance();
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}


	public function index() {

		$permissionView = false;
		$permissionEdit = false;
			
		switch ($this->request->application) {
			
			case 'mps':
				$permissionView = user::permission('can_view_all_mps_exchange_orphans');
				$permissionEdit = user::permission('can_edit_all_mps_exchange_orphans');
			break;
		}

		if (!$permissionView && !$permissionEdit) {
			message::invalid_permission();
			url::redirect("/mps");
		}

		$link = $this->request->query('data');
		$this->request->field('data', $link);

		// template: list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'list.php');
		$tpl->data('url', '/applications/modules/ordersheet/orphans/list.php');
		$tpl->data('class', 'list-500 annoucements-list');

		// assign template to view
		$this->view->setTemplate('orphans', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js'
		));
	}

	public function data() {

		$id = url::param();
		$application = $this->request->application;
		$permissionView = user::permission('can_view_all_mps_exchange_orphans');
		$permissionEdit = user::permission('can_edit_all_mps_exchange_orphans');

		if (!$permissionView && !$permissionEdit) {
			message::invalid_permission();
			url::redirect("/mps");
		}

		// db models
		$model = new Model($this->request->application);
		$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);

		$exchangeItems = array();
		$materials = array();

		// buttons
		$buttons = array();
		$buttons['back'] = $this->request->query();

		// get confirmed orphans
		$result = $exchange->query("
			SELECT 
				mps_salesorder_ponumber AS pon,
				REPLACE(LOWER(mps_salesorderitem_material_code), ' ', '') AS item,
				TRIM(mps_salesorderitem_material_code) AS code,
				TRIM(mps_salesorder_customernumber) AS customer,
				TRIM(ramco_shipto_number) AS shipto,
				TRIM(ramco_sold_to_sapnr) AS sap,
				TRIM(ramco_confirmed_quantity) AS quantity
			FROM ramco_confirmed_items
			WHERE REPLACE(LOWER(mps_salesorder_ponumber), ' ', '') = '$id'
		")->fetchAll();

		if ($result) {
			
			foreach ($result as $row) {
				$pon = $pon ?: $row['pon'];
				$item = $row['item'];
				$materials[$item] = $row['code'];
				$exchangeItems[$item]['code'] = $row['code'];
				$exchangeItems[$item]['shipto'] = $row['shipto'];
				$exchangeItems[$item]['sap'] = $row['sap'];
				$exchangeItems[$item]['shipped'] = $row['quantity'];
				$exchangeItems[$item]['confirmed'] = $row['quantity'];
			}
		}

		// get shipped orphans
		$result = $exchange->query("
			SELECT 
				mps_salesorderitem_ponumber AS pon,
				REPLACE(LOWER(mps_salesorderitem_material_code), ' ', '') AS item,
				TRIM(mps_salesorderitem_material_code) AS code,
				TRIM(mps_salesorder_customernumber) AS customer,
				TRIM(ramco_shipto_number) AS shipto,
				TRIM(ramco_sold_to_sapnr) AS sap,
				TRIM(ramco_shipped_quantity) AS quantity
			FROM ramco_shipped_items
			WHERE REPLACE(LOWER(mps_salesorderitem_ponumber), ' ', '') = '$id'
		")->fetchAll();

		if ($result) {
			
			foreach ($result as $row) {
				$pon = $pon ?: $row['pon'];
				$item = $row['item'];
				$materials[$item] = $row['code'];
				$exchangeItems[$item]['code'] = $row['code'];
				$exchangeItems[$item]['customer'] = $row['customer'];
				$exchangeItems[$item]['shipto'] = $row['shipto'];
				$exchangeItems[$item]['sap'] = $row['sap'];
				$exchangeItems[$item]['shipped'] = $row['quantity'];
			}
		}

		if ($materials) {

			$materials = "'".join("' , '", array_values($materials))."'";

			$result = $model->query("
				SELECT 
					mps_ordersheet_id AS id,
					CONCAT(country_name, ': ', address_company, ', ', mps_mastersheet_name) AS caption,
					mps_ordersheet_item_customernumber As customer,
					mps_ordersheet_item_shipto AS shipto,
					REPLACE(LOWER(mps_material_code), ' ', '') AS item
				FROM mps_ordersheet_items
				INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
				INNER JOIN db_retailnet.addresses ON address_id = mps_ordersheet_address_id
				INNER JOIN db_retailnet.countries ON country_id = address_country
				INNER JOIN mps_mastersheets ON mps_mastersheet_id = mps_ordersheet_mastersheet_id
				INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
				WHERE mps_ordersheet_workflowstate_id IN (6,9,10,11,12,13,14,15)
				AND mps_ordersheet_item_quantity_approved > 0
				AND mps_material_code IN ($materials)
				ORDER BY country_name, address_company
			")->fetchAll();

			if ($result) {
				
				foreach ($result as $row) {
					
					$key = $row['id'];
					$item = $row['item'];
					$customer = $row['customer'];
					$shipto = $row['shipto'];
					$ordersheets[$key]['caption'] = $row['caption'];

					if ($exchangeItems[$item]['customer'] && $customer==$exchangeItems[$item]['customer']) {
						$ordersheets[$key]['class'] = 'sensitive';
					}
				}
			}
		}

		if ($permissionEdit) { 
			$buttons['import'] = true;
			$buttons['ignore'] = true;
		}

		ksort($exchangeItems);

		// template: list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'ordersheet/orphan.php');
		$tpl->data('id', $id );
		$tpl->data('application', $application );
		$tpl->data('buttons', $buttons );
		$tpl->data('pon', $pon );
		$tpl->data('ordersheets', $ordersheets );
		$tpl->data('exchangeItems', $exchangeItems );

		// assign template to view
		$this->view->setTemplate('orphan', $tpl);
	}
}