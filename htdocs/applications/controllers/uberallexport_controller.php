<?php 

	class Uberallexport_Controller {
		
		public function __construct() {
			
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {

			// view collection
			
			$permission_edit = user::permission('can_export_xml_poslocations');
		
			if(!$permission_edit) {
				Message::access_denied();
				url::redirect('/messages/show/access-denied');
			}

			
			// template: list
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'uberallexport/uberallexport.php');

			// assign template to view
			$this->view->setTemplate('uberallexport', $tpl);

		}
	}