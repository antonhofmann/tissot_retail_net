<?php 

	class Visuals_Controller {
		
		public function __construct() {
			
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			
			// request
			$requestFields = array();
			$requestFields['form'] = $this->request->query('data');
			
			// permission: can edit visuals
			$can_edit = user::permission(Visual::PERMISSION_EDIT);
			
			if (!$this->request->archived && $can_edit) {
				$requestFields['add'] = $this->request->query('add');
			}
			
			// template: visuals
			$this->view->visuals('pagecontent')
			->visual('list')
			->data('requestFields', $requestFields);
		}
		
		public function add() {
			
			// permissions
			$permission_edit = user::permission(Visual::PERMISSION_EDIT);
			
			if( $this->request->archived || !$permission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
				
			$this->view->visualForm('pagecontent')
			->visual('data')
			->data('data', $data)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons);
		}
		
		public function data() {
				
			$id = url::param();
			
			// permissions
			$permission_edit = user::permission(Visual::PERMISSION_EDIT);
			
			$visual = new Visual($this->request->application);
			$visual->read($id);
			
			// check access to material category
			if (!$visual->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");

			// form dataloader
			$data = $visual->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
				
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
				
			if (!$this->request->archived && $permission_edit) {
				
				$buttons['save'] = true;
				
				$integrity = new Integrity();
				$integrity->set($id, 'mps_visuals');
				
				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/visual.delete.php', array('id'=>$id));
				}
			}
			// disable all fields
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $visual->header());

			$this->view->visualForm('pagecontent')
			->visual('data')
			->data('data', $data)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}