<?php

	class Partnertypes_Controller {

		public function __construct() {

			$this->request = request::instance();
	
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');
			Compiler::attach(Theme::$Default);
		}

		public function index() {

			switch ($this->request->application) {
				
				case 'red':

					// permissions
					$permission_system_view = user::permission(RED::PERMISSION_VIEW_SYSTEM_DATA);
					$permission_system_edit = user::permission(RED::PERMISSION_EDIT_SYSTEM_DATA);
						
					// request form: add button
					if (!$this->request->archived && $permission_system_edit) {
						$field = $this->request->query('add');
						$this->request->field('add',$field);
					}
					
					// request form: link to basic data
					if ($permission_system_view) {
						$field = $this->request->query('data');
						$this->request->field('form', $field);
					}
					
					// template: project types
					$this->view->projectPartnerTypeList('pagecontent')->red('partner.type.list');
					
				break;
			}
		}

		public function add() {

			switch ($this->request->application) {
				
				case 'red':
					
					// permissions
					$permission_system_view = user::permission(RED::PERMISSION_VIEW_SYSTEM_DATA);
					$permission_system_edit = user::permission(RED::PERMISSION_EDIT_SYSTEM_DATA);
						
					// check access to add partners
					if ($this->request->archived || !$permission_system_edit) {
						message::access_denied();
						url::redirect($this->request->query());
					}
					
					// form dataloader
					$data = array();
					$data['application'] = $this->request->application;
					$data['controller'] = $this->request->controller;
					$data['redirect'] = $this->request->query('data');
						
					// buttons
					$button = array();
					$buttons['back'] = $this->request->query();
					$buttons['save'] = true;
					
					// template: project type
					$this->view->projectTypeForm('pagecontent')
					->red('partner.type.form')
					->data('data', $data)
					->data('buttons', $buttons);
					
				break;
			}
		}

		public function data() {
		
			switch ($this->request->application) {
		
				case 'red':
						
					$id = url::param();
		
					// permissions
					$permission_system_view = user::permission(RED::PERMISSION_VIEW_SYSTEM_DATA);
					$permission_system_edit = user::permission(RED::PERMISSION_EDIT_SYSTEM_DATA);
					$permission_system_delete = user::permission(RED::PERMISSION_DELETE_SYSTEM_DATA);
		
					$partnerType = new Red_Partner_Type();
					$partnerType->read($id);
		
					// check access to add partners
					if (!$partnerType->id || !$permission_system_view) {
						message::access_denied();
						url::redirect($this->request->query());
					}
						
					// form dataloader
					$data = $partnerType->data;
					$data['application'] = $this->request->application;
					$data['controller'] = $this->request->controller;
		
					// buttons
					$button = array();
					$buttons['back'] = $this->request->query();
		
					if ($permission_system_edit) {
							
						// button: save
						$buttons['save'] = true;
							
						// db integrity
						if ($permission_system_delete) {
		
							$integrity = new Integrity();
							$integrity->set($id, 'red_partnertypes');
		
							if ($integrity->check()) {
								$buttons['delete'] = $this->request->link('/applications/helpers/red.partner.type.delete.php', array('id'=>$id));
							}
						}
					}
					else {
						foreach ($data as $field => $value) {
							$disabled[$field] = true;
						}
					}
		
					// template: header
					$this->view->header('pagecontent')
					->node('header')
					->data('title', $partnerType->name);
						
					// template: project type
					$this->view->projectsTypeForm('pagecontent')
					->red('partner.type.form')
					->data('data', $data)
					->data('buttons', $buttons);
		
				break;
			}
		}
	}
