<?php 

	class Stringtranslations_Controller {
		
		public function __construct() {
			
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {

			// view collection
			$link = $this->request->query('data');
			$this->request->field('form', $link);

			$permission_edit = user::permission('can_edit_stringtranslations');
		
			if(!$permission_edit) {
				Message::access_denied();
				url::redirect('/messages/show/access-denied');
			}

			// button: add new
			$link = $this->request->query('add');
			$this->request->field('add', $link);

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'list.php');
			$tpl->data('url', '/applications/modules/stringtranslation/list.php');
			$tpl->data('class', 'list-600');
			$this->view->setTemplate('stringtranslation', $tpl);

			Compiler::attach(array(
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js'
			));
		}
		
		public function add() {
			
			$permission_edit = user::permission('can_edit_stringtranslations');
		
			if(!$permission_edit) {
				Message::access_denied();
				url::redirect('/messages/show/access-denied');
			}

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
	
			// form hidden vars
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');


			//get all languages
			$dataloader['string_translation_language_id'] = Language::loader();
				
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'stringtranslation/form.php');
			$tpl->data('data', $data);
			$tpl->data('dataloader', $dataloader);
			$tpl->data('buttons', $buttons);

			$this->view->setTemplate('stringtranslation', $tpl);


			
			// script integration
			Compiler::attach(array(
					DIR_SCRIPTS."jquery/jquery.ui.css",
					DIR_CSS."jquery.ui.css",
					DIR_SCRIPTS."jquery/jquery.ui.js",
					DIR_SCRIPTS."ajaxuploader/ajaxupload.js",
					DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css",
					DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js",
					DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js",
					DIR_SCRIPTS."qtip/qtip.css",
					DIR_SCRIPTS."qtip/qtip.js",
					DIR_JS."form.validator.file.js"
				));

		}
		
		public function data() {
				
			$permission_edit = user::permission('can_edit_stringtranslations');
		
			if(!$permission_edit) {
				Message::access_denied();
				url::redirect('/messages/show/access-denied');
			}
			
			
			$id = url::param();


			if (!$id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$application = $this->request->application;

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			// modul: stringtranslation
			$stringtranslation = new Modul($application);
			$stringtranslation->setTable('string_translations');
			$data = $stringtranslation->read($id);

		
			$buttons['save'] = true;
					
			// button: delete
			
			if (user::permission('can_administrate_system_data')) {
				$buttons['delete'] = $this->request->link('/applications/modules/stringtranslation/delete.php', array('id'=>$id));
			}
			
			

			// request vars
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;

			
			//get all languages
			$dataloader['string_translation_language_id'] = Language::loader();
			
			

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'stringtranslation/form.php');
			$tpl->data('data', $data);
			$tpl->data('dataloader', $dataloader);
			$tpl->data('buttons', $buttons);
			
			$this->view->setTemplate('stringtranslation', $tpl);

			// script integration
			Compiler::attach(array(
					DIR_SCRIPTS."jquery/jquery.ui.css",
					DIR_CSS."jquery.ui.css",
					DIR_SCRIPTS."jquery/jquery.ui.js",
					DIR_SCRIPTS."ajaxuploader/ajaxupload.js",
					DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css",
					DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js",
					DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js",
					DIR_SCRIPTS."qtip/qtip.css",
					DIR_SCRIPTS."qtip/qtip.js",
					DIR_JS."form.validator.file.js"
				));

		}
		
	}