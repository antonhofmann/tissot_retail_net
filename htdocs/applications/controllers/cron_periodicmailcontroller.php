<?php

/**
 * Mail controller for mails that we send periodically
 * 
 * This covers all mails previously sent on-login. The original mail functions were
 * in shared/func_mailalerts.php
 *
 * All mails:
 * new.project.planned.take.over.date
 * new.project.planned.lease.starting.date
 * new.project.realistic.pos.opening.date
 * project.actual.takeover.date
 * projects.actual.lease.starting.date
 * project.takeover.date.past.or.missing
 * project.lease.starting.date.past.or.missing
 * project.pos.opening.date.past.or.missing
 * project.agreed.pos.opening.date
 * lease.contract.extension.second.reminder
 * lease.resignation.deadline
 * supplier.order.confirmation.reminder
 * call.to.enter.item.pickup.dates
 * pos.call.to.enter.closing.date
 * pos.missing.closing.date.temporary.pos
 * pos.call.for.entering.opening.hours
 * pos.reminder.to.close.popup
 * call.for.entering.dr.swatch.services
 * call.for.pos.translations
 */
class Cron_PeriodicMailController extends Cron_AbstractMailController {
	/**
	 * @var Project_Mails_Datamapper
	 */
	protected $ProjectMapper;

	/**
	 * @var OrderModel
	 */
	protected $OrderModel;

	/**
	 * Setup
	 */
	public function __construct() {
		$this->ProjectMapper = new Project_Mails_Datamapper();
		$this->OrderModel = new OrderModel();
	}

	/**
	 * Send all mails
	 * @return void
	 */
	public function run() {

		$this->sendInitialNoticeForMissingRealisticDate();
		$this->sendInitialNoticeForOverdueRealisticDate();
		$this->sendOpeningDateImminentNotice();
		$this->sendActualOpeningDateNotice();
		$this->sendLeaseRenewalNotices(new POS_Datamapper);
		$this->sendLeaseResignationNotices(new POS_Datamapper);
		$this->sendPlannedPOSClosingNotice(new Pos_Model);
		$this->sendUnclosedTemporaryPOSNotice(new User_DataMapper);		
		$this->sendUnclosedPopupPOSNotice();
		$this->sendDataIntegrityAlert();
		
		//$this->sendExpiringCertificatesNotice(new Certificate_File_Model);
		//$this->sendPosAddressNoticeForCustomerService(new Pos_Model, new User_DataMapper);
		//$this->sendMissingOpeningHoursNotice(new Pos_Model, new User_DataMapper);
		//$this->sendUnconfirmedOrderItemNotice(new Order_Item_Datamapper);
		//$this->sendOrderConfirmationReminders();
		//$this->sendItemArrivalConfirmationReminders(new Order_Item_Datamapper);
		//$this->sendCallForPOSTranslations(new POS_Datamapper);
	}

	/**
	 * Send initial notice to enter realistic date for projects where date is missing
	 * Refactored from ma_enter_realistic_sopd()
	 * @return void
	 */
	public function sendInitialNoticeForMissingRealisticDate() {
		$projects = $this->ProjectMapper->getNewProjectsForMailAlerts(
			Project_Mails_Datamapper::DATE_MISSING
		);

		foreach ($projects as $project) {
			
			switch ($project['project_projectkind']) {
				// takeover
				case 4:
					$Mail = new ActionMail('new.project.planned.take.over.date');
					break;

				// lease renewal
				case 5:
					$Mail = new ActionMail('new.project.planned.lease.starting.date');
					break;

				// regular project
				default:
					$Mail = new ActionMail('new.project.realistic.pos.opening.date');
					break;
			}

			// check if mail was already sent
			$isSent = $this->OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['project_retail_coordinator'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				$Mail->setParam('projectID', $project['project_id']);
				$Mail->send();
			}
		}
	}

	/**
	 * Send a notice 1 week in advance that the agreed opening date is coming up
	 * This mail is followed up after agreed opening date passes by project.agreed.pos.opening.date
	 * @return void
	 */
	public function sendOpeningDateImminentNotice() {
		
		$projects = $this->ProjectMapper->getNewProjectsForMailAlerts(
			Project_Mails_Datamapper::DATE_IMMINENT
		);

		foreach ($projects as $project) {
			
			$Mail = new ActionMail('project.agreed.opening.date.imminent');

			// check if mail was already sent
			$isSent = $this->OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['project_retail_coordinator'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				$Mail->setParam('projectID', $project['project_id']);
				$Mail->send();
			}
		}
	}

	/**
	 * Send initial notice to enter realistic date for projects where date is already past
	 * Refactored from ma_enter_realistic_sopd()
	 * @return void
	 */
	public function sendInitialNoticeForOverdueRealisticDate() {
		
		$projects = $this->ProjectMapper->getNewProjectsForMailAlerts(
			Project_Mails_Datamapper::DATE_OVERDUE
		);

		foreach ($projects as $project) {
			
			switch ($project['project_projectkind']) {
				// takeover2
				case 4:
					continue;
					$Mail = new ActionMail('project.actual.takeover.date');
					break;

				// lease renewal
				case 5:
					continue;
					$Mail = new ActionMail('project.actual.lease.starting.date');
					break;

				// regular project
				default:
					$Mail = new ActionMail('project.agreed.pos.opening.date');
					break;
			}

			// check if mail was already sent
			$isSent = $this->OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['project_retail_coordinator'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				$Mail->setParam('projectID', $project['project_id']);
				$Mail->send();
			}
		}
	}

	/**
	 * Send initial notice that opening date is missing
	 * Refactored from ma_enter_actual_sopd()
	 * @return void
	 */
	public function sendActualOpeningDateNotice() {
		
		$projects = array_merge(
			// lease renewal / take over projects
			$this->ProjectMapper->getCompletedToLrProjectsWithoutOpeningDate(),
			// all other projects
			$this->ProjectMapper->getCompletedProjectsWithoutOpeningDate()
		);

		foreach ($projects as $project) {
			
			// retail coordinator is the recipient, if there's none set, don't try to send
			if (!$project['project_retail_coordinator']) {
				continue;
			}

			switch ($project['project_projectkind']) {
				// takeover2
				case 4:
					$Mail = new ActionMail('project.takeover.date.past.or.missing');
					break;

				// lease renewal
				case 5:
					$Mail = new ActionMail('project.lease.starting.date.past.or.missing');
					break;

				// regular project
				default:
					$Mail = new ActionMail('project.pos.opening.date.past.or.missing');
					break;
			}

			// has the mail already been sent? if not -> send it
			
			$isSent = $this->OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['project_retail_coordinator'],
				$Mail->getTemplateId()
			);
			

			if ($isSent === false) {
				$Mail->setParam('projectID', $project['project_id']);
				$Mail->send();
			}
		}
	}

	/**
	 * Send lease renewal notices
	 * Refactored from ma_resignation_deadline()
	 * @return void
	 */
	public function sendLeaseRenewalNotices(POS_Datamapper $PosMapper) {
		
		$leases = $this->ProjectMapper->getPOSLeasesForRenewal();
		
		foreach ($leases as $leaseInfo) {
			
			if ($leaseInfo['posowner_type'] == 'franchisee') {
				$Mail = new ActionMail('lease.contract.extension.reminder.franchisee');
			}
			else {
				$Mail = new ActionMail('lease.contract.extension.reminder.corporate');
			}

			$Mail->setParam('posAddressClientID', $leaseInfo['posaddress_client_id']);
			$Mail->setParam('posAddressID', $leaseInfo['posaddress_id']);
			$Mail->setParam('posLocation', $leaseInfo['posaddress_name'].' '.$leaseInfo['country_name']);

			$Mail->setDataloader(array(
				'expiry_date'          => date::system($leaseInfo['expiry_date']),
				'termination_deadline' => date::system($leaseInfo['termination_deadline']),
				'extension_date'       => date::system($leaseInfo['extension_date']),
			));

			$Mail->send();
			if ($Mail->isSuccess()) {
				
				$recipient = $Mail->getFirstRecipient();

				if (!$recipient->id) return;

				$PosMapper->updatePOSLease($leaseInfo['poslease_id'], array(
					'poslease_mailalert2' => $recipient->getContent()
				));
			}
		}
	}

	/**
	 * Send lease resignation notices
	 * Refactored from ma_resignation_deadline()
	 * @return void
	 */
	public function sendLeaseResignationNotices(POS_Datamapper $PosMapper) {
		
		$leases = $this->ProjectMapper->getExpiringPOSLeases();

		foreach ($leases as $leaseInfo) {
			
			if ($leaseInfo['posowner_type'] == 'franchisee') {
				$Mail = new ActionMail('lease.resignation.deadline.franchisee');
			}
			else {
				$Mail = new ActionMail('lease.resignation.deadline.corporate');
			}
			
			$Mail->setParam('posAddressClientID', $leaseInfo['posaddress_client_id']);
			$Mail->setParam('posAddressID', $leaseInfo['posaddress_id']);
			$Mail->setParam('posLocation', $leaseInfo['posaddress_name'].' '.$leaseInfo['country_name']);

			$Mail->setDataloader(array(
				'expiry_date'          => date::system($leaseInfo['expiry_date']),
				'termination_deadline' => date::system($leaseInfo['termination_deadline']),
			));

			$Mail->send();
			
			if ($Mail->isSuccess()) {

				$recipient = $Mail->getFirstRecipient();

				if (!$recipient->id) return;
				
				$PosMapper->updatePOSLease($leaseInfo['poslease_id'], array(
					'poslease_mailalert' => $recipient->getContent()
				));
			}
		}
	}

	/**
	 * Send reminders to confirm orders
	 * Refactored from send_reminder_for_orders_to_suppliers()
	 * @return void
	 */
	public function sendOrderConfirmationReminders() {
		
		$tasks = $this->ProjectMapper->getUnsentOrderTaskReminders();
		$Translate = Translate::instance();

		

		foreach ($tasks as $task) {
			
			$data = array();

			// Projects
			if ($task['order_type'] == 1) {
				$data['entity'] = $Translate->project;
				$data['link'] = URLBuilder::projectTaskCenter($task['project_id']);
			}
			// Orders
			else {
				$data['entity'] = $Translate->order;	
				$data['link'] = URLBuilder::orderTaskCenter($task['order_id']);
			}

			// assemble mail and data
			$Mail = new ActionMail('supplier.order.confirmation.reminder');
			
			$recipient = $Mail->addRecipient($task['task_user']);
			
			//$sender = $Mail->setSender($task['task_from_user']);

			$sender = new User;
			$sender->read($task['task_from_user']);

			$data['order_number']   = $task['order_number'];
			$data['recipient_name'] = $recipient->getName();
			$data['sender_name']    = $sender->firstname.' '.$sender->name;
			
			$Mail->setDataloader($data);
			$Mail->send();

			// mark when we sent reminder on task
			if ($Mail->isSuccess()) {
				$Task = new Task();
				$Task->read($task['task_id']);
				$Task->update(array('task_reminder_sent' => date('d.m.Y')));
			}
		}
	}

	/**
	 * Send reminders to confirm item arrival
	 * Refactored from send_reminder_for_orders_to_forwarders()
	 * @param  Order_Item_Datamapper $OrderItemMapper
	 * @return void
	 */
	public function sendItemArrivalConfirmationReminders(Order_Item_Datamapper $OrderItemMapper) {
		
		$orders = $this->ProjectMapper->getUnsentArrivalConfirmationReminders();
		$Translate = translate::instance();

		foreach ($orders as $order) {
		
			$data = array();
			// Projects
			if ($order['order_type'] == 1) {
				$data['entity'] = $Translate->project;
				$data['link'] = URLBuilder::editProjectTrafficData($order['project_id']);
			}
			// Orders
			else {
				$data['entity'] = $Translate->order;	
				$data['link'] = URLBuilder::editOrderTrafficData($order['order_id']);
			}

			// assemble mail and data
			$Mail = new ActionMail('forwarder.arrival.confirmation.reminder');
			
			$Recipient = new User;
			$Recipient->readByAddress($order['order_item_forwarder_address']);
			$recipient = $Mail->addRecipient($Recipient);

			//$sender = $Mail->setSender($order['order_retail_operator']);

			$sender = new User;
			$sender->read($order['order_retail_operator']);

			$data['order_number']   = $order['order_number'];
			$data['recipient_name'] = $recipient->getName();
			$data['sender_name']    = $sender->firstname.' '.$sender->name;
			
			$Mail->setDataloader($data);

			$Mail->send();

			// mark when we sent reminder
			if ($Mail->isSuccess()) {
				$OrderItemMapper->updateOrderItemByAddressAndOrderID(
					$order['order_item_forwarder_address'],
					'forwarder',
					$order['order_id'],
					array('order_item_reminder_sent_to_forwarder' => date::sql())
				);
			}
		}
	}

	/**
	 * Send notice to confirm arrival of order items
	 * Refactored from ma_reminder_to_enter_pickup_dates()
	 * @param  Order_Item_Datamapper $OrderItemMapper
	 * @return void
	 */
	public function sendUnconfirmedOrderItemNotice(Order_Item_Datamapper $OrderItemMapper) {
		
		$orders = $this->ProjectMapper->getOrdersWithUncorfirmedPickup();
		$Translate = translate::instance();

		foreach ($orders as $order) {
			
			$Mail = new ActionMail('call.to.enter.item.pickup.dates');
			
			$data = array(
				'order_number' => $order['order_number'],
				'project' => $order['order_number'],
			);

			// Projects
			if ($order['order_type'] == 1) {
				$data['entity'] = $Translate->project;
				$data['link'] = URLBuilder::editProjectSupplierData($order['project_id']);
			}
			// Orders
			else {
				$data['entity'] = $Translate->order;	
				$data['link'] = URLBuilder::editOrderSupplierData($order['order_id']);
			}

			// recipient	
			$Recipient = new User;
			$Recipient->readByAddress($order['order_item_supplier_address']);
			$recipient = $Mail->addRecipient($Recipient);
			$data['name'] = $recipient->getName();
			
			$Mail->setSender($order['order_retail_operator']);
			$Mail->setDataloader($data);

			$Mail->send();
			
			if ($Mail->isSuccess()) {
				$OrderItemMapper->updateOrderItemByAddressAndOrderID(
					$order['order_item_supplier_address'],
					'supplier',
					$order['order_id'],
					array('order_item_reminder_sent_to_supplier' => date::sql())
				);
			}
		}
	}

	/**
	 * Send notice to enter closing date when planned closing date is missed
	 * Refactored from ma_planned_closing_date()
	 * @param  Pos_Model $PosModel
	 * @return void
	 */
	public function sendPlannedPOSClosingNotice(Pos_Model $PosModel) {
		
		$poses = $this->ProjectMapper->getClosingPOSWithoutClosingDate();

		/*
		foreach ($poses as $pos) {
			
			$Mail = new ActionMail('pos.call.to.enter.closing.date');
			
			// mail was already sent -> skip
			if ($PosModel->isPosMailSent($pos['posaddress_id'], $Mail->getTemplateId())) {
				continue;
			}
			
			$Mail->setParam('posAddressClientID', $pos['posaddress_client_id']);
			$Mail->setParam('posAddressID', $pos['posaddress_id']);
			$Mail->setParam('addProjectOwner', true);
			
			$Mail->setParam('posLocation', sprintf('%s, %s, %s',
				$pos['country_name'],
				$pos['place_name'],
				$pos['posaddress_name'])
			);
			
			// onyl set retail managers as recipients for corporate projects
			$Mail->setParam('noRetailManager', $row['posaddress_ownertype'] != 1);
			
			// load additional data
			$Mail->setDataloader(array('
				pos_name' => $pos['posaddress_name']
			));
			
			// set pos for tracking pos mails
			$Pos = new Pos;
			$Pos->read($pos['posaddress_id']);

			$Mail->send();
			
			// track pos mails
			if ($Mail->isSuccess()) {
				$Pos->posMailTracks($Mail);
			}
		}
		*/

		foreach ($poses as $pos) {
			
			$Mail = new ActionMail('pos.call.to.enter.closing.date');

			// mail was already sent -> skip
			if ($PosModel->isPosMailSent($pos['posaddress_id'], $Mail->getTemplateId())) {
				continue;
			}

			// get project owner
			$ProjectOwner = $this->ProjectMapper->getOwnerOfLatestProject(
				$pos['posaddress_id'],
				$pos['posaddress_client_id']
			);


			// get brand manager country
			$BrandManager = $this->ProjectMapper->getBrandManager(
				$pos['posaddress_id'],
				$pos['posaddress_client_id']
			);


			// get local retail coordinator
			$LocalRetailCoordinator = $this->ProjectMapper->getlocalRetailCoordinator(
				$pos['posaddress_id'],
				$pos['posaddress_client_id']
			);

			// get retail manager country
			$RetailManagerCountry = $this->ProjectMapper->getRetailManagerCountry(
				$pos['posaddress_id'],
				$pos['posaddress_client_id']
			);
			
			// assemble data
			$data = array(
				'pos_location' => sprintf('%s, %s, %s',
					$pos['country_name'],
					$pos['place_name'],
					$pos['posaddress_name']
				),
				'pos_name' => $pos['posaddress_name'],
				'link' => htmlspecialchars(URLBuilder::mpsPOSAddress($pos['posaddress_id'])),
				'noRetailManager', $row['posaddress_ownertype'] != 1
			);

			if($BrandManager) {
				$recipient = $Mail->addRecipient($BrandManager);
			
			}
			else {
				$recipient = $Mail->addRecipient($ProjectOwner);
			}

			$data['name'] = $recipient->getName();
			
			if($LocalRetailCoordinator) {
				$Mail->addCCrecipient($LocalRetailCoordinator->email);
			}

			if($RetailManagerCountry) {
				$Mail->addCCrecipient($RetailManagerCountry->email);
			}

			$Mail->setDataloader($data);

			$Mail->send();

			// send mail
			$Pos = new Pos;
			$Pos->read($pos['posaddress_id']);
			
			if ($Pos->id && $Mail->isSuccess()) {
				$Pos->posMailTracks($Mail);
			}
		}

		
	}

	/**
	 * Send a notice to enter the actual closing date of a temporary POS
	 * Refactored from ma_close_temporary_pos_locations()
	 * @return void
	 */
	public function sendUnclosedTemporaryPOSNotice(User_Datamapper $UserMapper) {
		
		$projects = $this->ProjectMapper->getUnclosedTemporaryPOS();

		foreach ($projects as $project) {
			
			$Project = new Project();
			$Project->read($project['project_id']);
			

			$Order = new Order;
			$Order->read($project['order_id']);

			
			$Mail = new ActionMail('pos.missing.closing.date.temporary.pos');

			$isSent = $this->OrderModel->isOrderMailSent(
				$project['order_id'], 
				$project['order_user'],
				$Mail->getTemplateId()
			);

			if ($isSent) continue;

			// set recipient (client)
			$Recipient = new User;
			$Recipient->read($project['order_user']);
			$recipient = $Mail->addRecipient($Recipient);

			// get brand manager
			$BrandManager = $UserMapper->getBrandManager($project['order_client_address']);
			
			if ($BrandManager === false) {
				$BrandManager = $UserMapper->getAffiliateBrandManager($project['order_client_address']);
			}
			// add brand manager
			if ($BrandManager instanceof User && $Mail->checkRole( Role::BRAND_MANAGER)) {
				$Mail->addCCrecipient($BrandManager->id);
			}
			// add local project leader
			if ($project['project_local_retail_coordinator'] && $Mail->checkRole( Role::PROJECT_LEADER)) {
				$Mail->addCCrecipient($project['project_local_retail_coordinator']);
			}

			// load data
			$data = array(
				'link'           => URLBuilder::mpsPOSAddress($project['posorder_posaddress']),
				'recipient_name' => $recipient->getName(),
				'date'           => date::system($project['project_planned_closing_date']),
				'shop_address'   => $Order->shop_address_company,
				'country_name'   => $project['country_name'],
				'project_number' => $project['project_number'],
			);
			
			$Mail->setDataloader($data);
			
			$Mail->send();


			if ($Order->id && $Mail->isSuccess()) {
				$Order->orderMailTracks($Mail);
			}
		}
	}

	/**
	 * Send notice when POS is missing opening hours
	 * Refactored from ma_opening_hours()
	 * @param  Pos_Model $PosModel
	 * @param  User_Datamapper $UserMapper
	 * @return void
	 */
	public function sendMissingOpeningHoursNotice(Pos_Model $PosModel, User_Datamapper $UserMapper) {
		
		$poses = $this->ProjectMapper->getPosWithoutOpeningHours();

		foreach ($poses as $pos) {
			
			$Mail = new ActionMail('pos.call.for.entering.opening.hours');

			// mail was already sent -> skip
			if ($PosModel->isPosMailSent($pos['posaddress_id'], $Mail->getTemplateId())) {
				continue;
			}

			// get project owner
			$ProjectOwner = $this->ProjectMapper->getOwnerOfLatestProject(
				$pos['posaddress_id'],
				$pos['posaddress_client_id']
			);
			
			// fallback to project creator
			if (!$ProjectOwner) {
				$ProjectOwner = $UserMapper->getProjectCreator($pos['posaddress_id']);
			}
			
			if (!($ProjectOwner instanceof User) || !$ProjectOwner->email) {
				continue;
			}

			// get brand manager
			$BrandManager = $UserMapper->getBrandManager($project['order_client_address']);
			
			if ($BrandManager === false) {
				$BrandManager = $UserMapper->getAffiliateBrandManager($project['order_client_address']);
			}
			
			// add brand manager
			if ($BrandManager instanceof User && $Mail->checkRole(Role::BRAND_MANAGER)) {
				$Mail->addCCrecipient($BrandManager->id);
			}

			// get retail managers
			$retailManagers = $UserMapper->getRetailManager($pos['posaddress_id']);
			
			foreach ($retailManagers as $RetailManager) {
				if ($Mail->checkRole(Role::RETAIL_MANAGER)) {
					$Mail->addCCrecipient($RetailManager->id);
				}
			}

			// add local project leader
			if ($project['project_local_retail_coordinator'] && $Mail->checkRole(Role::PROJECT_LEADER)) {
				$Mail->addCCrecipient($project['project_local_retail_coordinator']);
			}

			// assemble data
			$data = array(
				'pos_location' => sprintf('%s, %s, %s',
					$pos['country_name'],
					$pos['place_name'],
					$pos['posaddress_name']
				),
				'pos_name' => $pos['posaddress_name'],
				'link' => htmlspecialchars(URLBuilder::editPOSOpeningHours($pos['posaddress_country'], $pos['posaddress_id'])),
			);

			$recipient = $Mail->addRecipient($ProjectOwner);

			$data['name'] = $recipient->getName();

			$Mail->setDataloader($data);

			$Mail->send();

			// send mail
			$Pos = new Pos;
			$Pos->read($pos['posaddress_id']);
			
			if ($Pos->id && $Mail->isSuccess()) {
				$Pos->posMailTracks($Mail);
			}
		}
	}

	/**
	 * Send notice to enter closing date for popup POS
	 * Refactored from ma_close_popup_locations()
	 * @return void
	 */
	public function sendUnclosedPopupPOSNotice() {
		
		$projects = $this->ProjectMapper->getUnclosedPopupPOS();

		foreach ($projects as $project) {
			
			$Mail = new ActionMail('pos.reminder.to.close.popup');
			
			// check if mail was already sent
			$isSent = $this->OrderModel->isOrderMailSent(
				$project['order_id'], 
				$project['project_retail_coordinator'],
				$Mail->getTemplateId()
			);
			
			if ($isSent) continue;

			$Order = new Order;
			$Order->read($project['order_id']);

			// setup mail
			$recipient = $Mail->addRecipient($project['project_retail_coordinator']);

			$Mail->setDataloader(array(
				'pos_location' => sprintf('%s, %s, %s',
					$project['order_number'],
					$project['country_name'],
					$project['order_shop_address_company']
				),
				'recipient_name' => $recipient->getName(),
				'date' => date::system($project['project_planned_closing_date']),
				'link' => URLBuilder::editPOSData($project['project_id']),
			));
			
			$Mail->send();

			if ($Order->id && $Mail->isSuccess()) {
				$Order->orderMailTracks($Mail);
			}
		}
		
	}

	/**
	 * Send notice to pos addresses without customer services
	 * Refactored from ma_customer_services()
	 * @param  Pos_Model $PosModel
	 * @param  User_Datamapper $UserMapper
	 * @return void
	 */
	public function sendPosAddressNoticeForCustomerService(Pos_Model $PosModel, User_Datamapper $UserMapper) {
		
		$poses = $this->ProjectMapper->getPOSAddressesWithoutCustomerService();

		foreach ($poses as $pos) {
			
			$Mail = new ActionMail('call.for.entering.dr.swatch.services');

			// mail was already sent -> skip
			if ($PosModel->isPosMailSent($pos['posaddress_id'], $Mail->getTemplateId())) {
				continue;
			}

			// get project owner
			$ProjectOwner = $this->ProjectMapper->getOwnerOfLatestProject(
				$pos['posaddress_id'],
				$pos['posaddress_client_id']
			);
			
			// fallback to project creator
			if (!$ProjectOwner) {
				$ProjectOwner = $UserMapper->getProjectCreator($pos['posaddress_id']);
			}
			
			if (!($ProjectOwner instanceof User) || !$ProjectOwner->email) {
				continue;
			}

			// get brand manager
			$BrandManager = $UserMapper->getBrandManager($project['order_client_address']);
			
			if ($BrandManager === false) {
				$BrandManager = $UserMapper->getAffiliateBrandManager($project['order_client_address']);
			}
			
			// add brand manager
			if ($BrandManager instanceof User && $Mail->checkRole(Role::BRAND_MANAGER)) {
				$Mail->addCCrecipient($BrandManager->id);
			}

			// get retail managers
			$retailManagers = $UserMapper->getRetailManager($pos['posaddress_id']);
			
			foreach ($retailManagers as $RetailManager) {
				if ($Mail->checkRole(Role::RETAIL_MANAGER)) {
					$Mail->addCCrecipient($RetailManager->id);
				}
			}

			// add local project leader
			if ($project['project_local_retail_coordinator'] && $Mail->checkRole(Role::PROJECT_LEADER)) {
				$Mail->addCCrecipient($project['project_local_retail_coordinator']);
			}

			// assemble data
			$data = array(
				'pos_location' => sprintf('%s, %s, %s',
					$pos['country_name'],
					$pos['place_name'],
					$pos['posaddress_name']
				),
				'pos_name' => $pos['posaddress_name'],
				'link' => htmlspecialchars(URLBuilder::customerService($pos['posaddress_id'], $pos['posaddress_country'])),
			);

			$recipient = $Mail->addRecipient($ProjectOwner);

			$data['name'] = $recipient->getName();

			$Mail->setDataloader($data);

			$Mail->send();

			// send mail
			$Pos = new Pos;
			$Pos->read($pos['posaddress_id']);

			if ($Pos->id && $Mail->isSuccess()) {
				$Pos->posMailTracks($Mail);
			}
		}
	}

	/**
	 * Send a notice when there are certificates that will expire within the next 6 months
	 * 1 mail is sent with all the certificates, not 1 mail per certificate
	 * @param Certificate_File_Model $CertificateFileModel
	 * @return void
	 */
	public function sendExpiringCertificatesNotice(Certificate_File_Model $CertificateFileModel) {
		
		$certs = $this->ProjectMapper->getExpiringCertificates();
		
		$Mail = new ActionMail('expiring.certificates.notice');
		
		$Translate = translate::instance();
		
		// no cert files expiring or they were already notified
		if (count($certs) < 1) return;

		// build certificate list
		$certificateList = array();
		
		foreach ($certs as $cert) {
			
			$Certificate = new Certificate();
			$Certificate->read($cert['id']);

			$certificateList[] = sprintf("%s\n%s\n%s: %s\n%s", 
				$Certificate->name, 
				$Certificate->getSupplierCompany(),
				$Translate->expires_on,
				date::system($cert['expiry_date']),
				URLBuilder::editCertificate($cert['id'])
			);
			
			$CertificateFileModel->update($cert['file_id'], array('certificate_file_expiry_notice_sent' => date::sql()));
		}

		$Mail->setDataloader(array(
			'certificates' => join("\n--\n", $certificateList)
		));

		$Mail->send();
	}

	/**
	 * Send Send Data Integrity Alert
	 * @return void
	 */
	public function sendDataIntegrityAlert() {
		$data = $this->ProjectMapper->getDataIntegrityIssue();

		$item_list = '';
		foreach ($data as $group=>$items) {
			
			$item_list .= '<strong>' . $group . '</strong><br><table>';
			foreach($items as $key=>$item) {
				
				$item_list .= '<tr>';
				$link = url::host() . $item['link'];
				$item_list .= '<td>' . $item['uname'] . '</td><td><a href="' . $link. '">' . $item['order_number'] . '</a></td>';
				$item_list .= '</tr>';
			}
			$item_list .= '</table><br><br>';
		}



		if($item_list) {
			$Mail = new ActionMail('data_integrity_alert');
			
			$Mail->setDataloader(array(
				'item_list' => join("\n--\n", array($item_list))
			));
			$Mail->send();
		}
	}


	/**
	 * Send Send Data Integrity Alert
	 * @return void
	 */
	public function sendCallForPOSTranslations(POS_Datamapper $PosMapper) {
		
		$poslocations = $PosMapper->getUntranslatedPOSLocations();
		
		
		foreach ($leases as $leaseInfo) {
			
			if ($leaseInfo['posowner_type'] == 'franchisee') {
				$Mail = new ActionMail('lease.contract.extension.reminder.franchisee');
			}
			else {
				$Mail = new ActionMail('lease.contract.extension.reminder.corporate');
			}

			$Mail->setParam('posAddressClientID', $leaseInfo['posaddress_client_id']);
			$Mail->setParam('posAddressID', $leaseInfo['posaddress_id']);
			$Mail->setParam('posLocation', $leaseInfo['posaddress_name'].' '.$leaseInfo['country_name']);

			$Mail->setDataloader(array(
				'expiry_date'          => date::system($leaseInfo['expiry_date']),
				'termination_deadline' => date::system($leaseInfo['termination_deadline']),
				'extension_date'       => date::system($leaseInfo['extension_date']),
			));

			$Mail->send();
			if ($Mail->isSuccess()) {
				
				$recipient = $Mail->getFirstRecipient();

				if (!$recipient->id) return;

				$PosMapper->updatePOSLease($leaseInfo['poslease_id'], array(
					'poslease_mailalert2' => $recipient->getContent()
				));
			}
		}
	}
}