<?php 

	class Applications_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			$this->translate = Translate::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			// request form: link add
			$field_add = $this->request->query('add');
			$this->request->field('add', $field_add);
			
			// request form: link edit
			$field_edit = $this->request->query('data');
			$this->request->field('form', $field_edit);
		
			$this->view->applicationsList('pagecontent')
			->application('list');
		}
		
		public function add() {
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
				
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
			
			// dataloader: dbs
			$dataloader['application_db'] = db::loader();
		
			// template: application form
			$this->view->applicationForm('pagecontent')
			->application('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons);
		}
		
		public function data() {
			
			$id = url::param();
			
			$application = new Application();
			$application->read($id);
			
			if (!$application->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// form datalaoder
			$data = $application->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
			
			$integrity = new Integrity();
			$integrity->set($id, 'applications');
			
			// button: delete
			if ($integrity->check()) {
				$buttons['delete'] = $this->request->link('/applications/helpers/application.delete.php', array('id' >$id));
			}

			// dataloader: dbs
			$dataloader['application_db'] = db::loader();
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $application->name);
				
			// template: navigation header
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			$this->view->applicationForm('pagecontent')
			->application('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function roles() {
			
			$id = url::param();
			
			$application = new Application();
			$application->read($id);
			
			if (!$application->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			// request form: crrent id
			$this->request->field('id',$id);
			
			// form datalaoder
			$data = $application->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $application->name);
				
			// template: navigation header
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			// template: application roles list
			$this->view->applicationsRolesList('pagecontent')
			->application('role.list')
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function languages() {
			
			$id = url::param();
			
			$application = new Application();
			$application->read($id);
			
			if (!$application->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			// request form: crrent id
			$this->request->field('id',$id);
			
			// form datalaoder
			$data = $application->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();;
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $application->name);
				
			// template: navigation header
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			$this->view->applicationLanguagesList('pagecontent')
			->application('language.list')
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}
	