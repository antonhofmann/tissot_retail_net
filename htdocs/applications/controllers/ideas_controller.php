<?php 

	class Ideas_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			$this->translate = Translate::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() { 
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// request form: link edit
			$this->request->field('data', $this->request->query('data'));
		
			$this->view->ideas('pagecontent')->idea('list');
		}
		
		public function add() {
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
				
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
			
			// dataloader: categories
			$dataloader['idea_idea_category_id'] = IdeaCategory::loader();
		
			// template: application form
			$this->view->idea('pagecontent')
			->idea('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons);
		}
		
		public function data() {
			
			$id = url::param();
			
			$idea = new Idea();
			$idea->read($id);
			
			if (!$idea->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// form datalaoder
			$data = $idea->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
			
			// button: delete
			if (Idea::PERMISSION_EDIT) {
				
				$integrity = new Integrity();
				$integrity->set($id, 'ideas');
				
				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/idea.actions.php', array(
						'id' => $id,
						'section' => 'delete-idea'
					));
				}
			} else {
				foreach ($data as $field => $value) {
					$disabled[$field] = true;
				}
			}

			// dataloader: categories
			$dataloader['idea_idea_category_id'] = IdeaCategory::loader();
			
			$user = new User($idea->user);
			
			// template: header
			$this->view->header('pagecontent')->node('header')
			->data('title', "Submitted by: $user->firstname $user->name")
			->data('subtitle', "Date: ".$idea->data['date_created']);
			
			$this->view->tabs('pagecontent')->navigation('tab');
			
			$model = new Model();
			
			// get idea files				
			$result = $model->query("
				SELECT DISTINCT
					idea_file_id AS id,
					idea_file_file AS file
				FROM idea_files
				WHERE idea_file_idea_id = $id
			")->fetchAll();
			
			if ($result) {
				
				foreach ($result as $row) {
					
					$i = $row['id'];
					$file = pathinfo($row['file']);
					
					$dirname = $file['dirname'];
					$filename = $file['basename'];
					$thumb = "$dirname/thumbnail/$filename";
					
					if (!file_exists($_SERVER['DOCUMENT_ROOT'].$thumb)) {
						$extension = $file['extension'];
						$icon = "/public/images/icons/file/48px/$extension.png";
						$thumb = file_exists($_SERVER['DOCUMENT_ROOT'].$icon) ? $icon : "/public/images/icons/file/48px/_blank.png";
					}
					
					$dataloader['files'][$i] = array(
						'thumb' => $thumb,
						'download' => $row['file']
					);
					
					if (Idea::PERMISSION_EDIT) {
						$dataloader['files'][$i]['delete'] = $this->request->link('/applications/helpers/idea.actions.php', array(
							'id' => $i,
							'section' => 'delete-file'
						));
					}
				}
			}
			
			$dataloader['idea_priority'] = range(0,5);
			unset($dataloader['idea_priority'][0]);
			
			$this->view->applicationForm('pagecontent')
			->idea('data')
			->data('data', $data)
			->data('disabled', $disabled)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}
	