<?php 

	class Openinghours_Controller {
		
		public function __construct() {
			
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}


		public function index() {

		}
		
		public function localization() {

			$this->view->pagetitle = $this->request->title . " - " . translate::instance()->translation;

			// view provinces
			$link = $this->request->query('data');
			$this->request->field('form', $link);
			$this->request->field('application', $this->request->application);

			
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'list.php');
			$tpl->data('url', '/applications/modules/openinghour/localization/list.php');
			$tpl->data('class', 'list-600');
			$this->view->setTemplate('collections', $tpl);

			Compiler::attach(array(
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js',
				'/public/js/openinghours.localization.js'
			));
		}
	}