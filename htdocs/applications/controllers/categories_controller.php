<?php

class Categories_Controller {

	static public $permissions = array();

	public function __construct() {

		Settings::init()->theme = 'swatch';
		$this->request = request::instance();
		$this->view = new View();
		$this->view->pageclass = 'theme-news news-categories-list';
		$this->view->pagetitle = $this->request->title;
		$this->view->userboxMenu('navigations')->navigation('userbox');
		$this->view->categories('navigations')->navigation('categories');
		$this->view->appTabs('navigations')->navigation('appnav');

		static::$permissions = array(
			'edit' => user::permission('can_edit_news_system_data')
		);

		Compiler::attach('/public/themes/swatch/css/news.css');
	}


	public function index() {

		$application = $this->request->application;

		$model = new Model($application);

		switch ($application) {
			
			case 'news':

				// get categories
				$sth = $model->db->prepare("
					SELECT DISTINCT
						news_category_id AS id,
						news_category_name AS name,
						news_section_name AS section
					FROM news_categories
					LEFT JOIN news_sections ON news_section_id = news_category_section_id
					ORDER BY news_category_order, news_category_name 
				");

				$sth->execute();
				$categories = $sth->fetchAll();

				// view template
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'news/category/list.php');
				$tpl->data('title', $this->request->title);
				$tpl->data('categories', $categories);
				$this->view->setTemplate('sections', $tpl);

				Compiler::attach(array(
					'/public/themes/swatch/css/news.categories.css',
					'/public/js/news.categories.js'
				));

			break;
		}
			
	}

	public function add() {

		$application = $this->request->application;

		$model = new Model($application);

		switch ($application) {
			
			case 'news':

				if (!static::$permissions['edit']) {
					Message::access_denied();
					url::redirect($this->request->query());
				}

				$buttons = array();
				$buttons['save'] = true;

				$data = array();
				$data['application'] = $application;

				$categories = $model->query("
					SELECT DISTINCT 
						news_section_id AS id, 
						news_section_name As name
					FROM news_sections
					ORDER BY news_section_order, news_section_name
				")->fetchAll();

				// view template
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'news/category/form.php');
				$tpl->data('id', $id);
				$tpl->data('data', $data);
				$tpl->data('buttons', $buttons);
				$tpl->data('title', $this->request->title);
				$tpl->data('categories', $categories);
				$this->view->setTemplate('section', $tpl);

				Compiler::attach(array(
					'/public/scripts/formvalidation/css/formValidation.min.css',
					'/public/scripts/formvalidation/js/formValidation.js',
					'/public/scripts/formvalidation/js/framework/bootstrap.min.js',
					'/public/scripts/bsdialog/bsdialog.min.css',
					'/public/scripts/bsdialog/bsdialog.min.js',
					'/public/js/news.category.js'
				));

			break;
		}
	}

	public function data() {

		$id = url::param();
		$application = $this->request->application;

		$model = new Model($application);

		switch ($application) {
			
			case 'news':

				$buttons = array();

				$section = new Modul($application);
				$section->setTable('news_categories');
				$section->read($id);

				if ( !$section->id ) {
					Message::access_denied();
					url::redirect($this->request->query());
				}

				$categories = $model->query("
					SELECT DISTINCT 
						news_section_id AS id, 
						news_section_name As name
					FROM news_sections
					ORDER BY news_section_order, news_section_name
				")->fetchAll();

				if (static::$permissions['edit']) {
					
					$buttons['save'] = true;

					$integrity = new Integrity();
					$integrity->set($id, 'news_sections');
					
					if ($integrity->check()) {
						$buttons['delete'] = $this->request->link('/applications/modules/news/category/delete.php', array('id'=>$id));
					}
				}

				// view template
				$tpl = new Template('pagecontent');
				$tpl->template(PATH_TEMPLATES.'news/category/form.php');
				$tpl->data('id', $id);
				$tpl->data('buttons', $buttons);
				$tpl->data('title', $this->request->title);
				$tpl->data('data', $section->data);
				$tpl->data('categories', $categories);
				$this->view->setTemplate('section', $tpl);

				Compiler::attach(array(
					'/public/scripts/formvalidation/css/formValidation.min.css',
					'/public/scripts/formvalidation/js/formValidation.js',
					'/public/scripts/formvalidation/js/framework/bootstrap.min.js',
					'/public/scripts/bsdialog/bsdialog.min.css',
					'/public/scripts/bsdialog/bsdialog.min.js',
					'/public/js/news.category.js'
				));

			break;
		}
	}
}
