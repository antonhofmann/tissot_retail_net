<?php 

	class MyCompany_Controller {
	
		public function __construct() {
			
			$this->request = request::instance();
			$this->view = new View();
			$this->translate = translate::instance();
			
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_CSS."mycompany.css");

			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."user.js");
			
			
			$this->view->pagetitle = $this->translate->my_company;
			$user = user::instance();
			
			$id = $user->address;

			$company = new Company();
			$data = $company->read($id);
			
			$data['authenticated_user'] = $user->id;
			$data['company'] = $user->address;
			
			$place = new Place();
			$place->read($company->place_id);
			$data['place_name'] = $place->name;
			
			$country = new Country();
			$country->read($company->country);
			$data['country_name'] = $country->name;
			
			$this->request->field('id', $id);
			$this->request->field('link', "/mycompany/user");
			
			if ($this->request->controller=='mycompany') {
				$access = (user::permission('can_request_account_closing_opening')) ? true : false;
			} else {
				$access = true;
			}
			
			if ($access) { 
				$this->request->field('add', $this->request->query('user'));
				$this->request->field('form', $this->request->query('user'));
			}

			$this->view->usersList('pagecontent')
			->mycompany('list')
			->data('data', $data)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		
		public function user() { 
			
			$this->view->pagetitle = $this->translate->my_company;
			
			$id = url::param();
			
			$auth = User::instance();
			$user = new User($id);

			$company = new Company();
			$company->read($auth->address);

			if ($id && $user->id) {
				$fields = array_keys($user->data);
				$disabled = array_fill_keys($fields, true);
				$disabled['roles'] = true;
				unset($disabled['user_email_cc']);
				unset($disabled['user_email_deputy']);
				$data = $user->data;
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_CSS."mycompany.css");

			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_JS."user.js");
			
			$model = new Model();
			
			$model->query("SELECT role_id, role_name FROM roles	");
			
			if ($auth->can_set_swatch_roles_in_my_company) {
				$model->filter('roles', 'role_active = 1 and role_is_visible_to_swatch_in_my_company = 1');
			} else {
				$model->filter('roles', 'role_active = 1 and role_is_visible_to_clients_in_my_company = 1');
			}

			// show role news readers HQ
			// only for HQ addresses 
			if (!$company->is_a_hq_address) {
				$model->filter('readers_hq', 'role_id != 77');
			}
			
			$model->order('role_application_number, role_name');
			$result = $model->fetchAll();
			
			$dataloader['roles'] = _array::extract($result);
			
			// send used roles with request
			$userRoles = User::roles($id);
			$data['roles'] = serialize($userRoles); echo $data['used_roles'];
			
			// dataloader: user sex
			$dataloader['user_sex'] = array(
				'm' => $this->translate->male,
				'f' => $this->translate->female
			);
			
			$data['user_active'] = 1;			
			$data['controller'] = $this->request->controller;
			$data['redirect'] = '/'.$this->request->controller;
			$data['user_address'] = $auth->address;

			$data['user_phone'] = $data['user_phone'];
			$data['user_mobile_phone'] = $data['user_mobile_phone'];
			
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			if ($user->active) {
				$buttons['save'] = true;
			}
			
			$this->view->header('pagecontent')->node('header')
			->data('title', $header)
			->data('subtitle', $subheader);
			
			$this->view->usersList('pagecontent')->user('data')
			->data('mycompany', true)
			->data('sendmail', true)
			->data('disabled', $disabled)
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons);
			
		}
	}