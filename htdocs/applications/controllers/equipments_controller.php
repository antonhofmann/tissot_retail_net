<?php 

	class Equipments_controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			$this->translate = Translate::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			url::redirect($this->request->query('materials'));
		}
		
		public function materials() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			
			// template: tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			// template: material list
			$this->view->materials('pagecontent')
			->pos('equipment.material.list')
			->data('buttons', $buttons);
		}
		
		public function furniture() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
				
			// template: tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			// template: furniture list
			$this->view->materials('pagecontent')
			->pos('equipment.furniture.list')
			->data('buttons', $buttons);
		}
	}