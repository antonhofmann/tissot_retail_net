<?php

/**
 * Abstract class for cron Mail controllers
 */
class Cron_AbstractMailController {
	/**
	 * Set the ActionMail factory class
	 * @param  string $Factory
	 * @return void
	 */
	public function setMailFactory($Factory) {
		$this->MailFactory = $Factory;
	}

	/**
	 * Get the ActionMail factory class
	 * @return ActionMail
	 */
	public function getMailFactory() {
		if (!$this->MailFactory) {
			$this->setMailFactory(new ActionMailFactory());
		}
		return $this->MailFactory;
	}
}


/**
 * Small helper class to enable dependency injection of Mail class in Cron_CMSMailController
 * I don't like having two classes in the same file and we should move this asap
 * but currently there isn't really a good place for it
 */
class ActionMailFactory {
	/**
	 * Get an ActionMail instance
	 * @param  string $templateName
	 * @return ActionMail
	 */
	public function buildActionMail($templateName) {
		return new ActionMail($templateName);
	}
}