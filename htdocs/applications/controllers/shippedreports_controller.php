<?php

class ShippedReports_controller {

	public function __construct() {
				
		$this->user = User::instance();
		$this->request = request::instance();
		$this->translate = Translate::instance();
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}

	public function index() {

		// template: list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'ordersheet/shipped.reports.php');
		$this->view->setTemplate('reports', $tpl);

		$this->request->field('ordersheets', 0);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js',
			'/public/js/ordersheet.item.shipped.reports.js'
		));
	}
}