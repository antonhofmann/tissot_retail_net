<?php 

class Ordersheets_Controller {
	
	public function __construct() {

		$id = url::param();
		$this->request = request::instance();

		if ($id) {
			$ordersheet = new Ordersheet();
			$data = $ordersheet->read($id);
			Menu::instance($data);
		}
		
		$this->view = new View();
		$this->view->pagetitle = $this->request->title;
		$this->view->usermenu('usermenu')->user('menu');
		$this->view->categories('pageleft')->navigation('tree');

		Compiler::attach(Theme::$Default);
	}
	
	public function index() {
		
		$application = $this->request->application;
		$permission_view = user::permission(Ordersheet::PERMISSION_VIEW);
		$permission_view_limited = user::permission(Ordersheet::PERMISSION_VIEW_LIMITED);
		$permission_edit = user::permission(Ordersheet::PERMISSION_EDIT);
		$permission_edit_limited = user::permission(Ordersheet::PERMISSION_EDIT_LIMITED);
		$permission_manage = user::permission(Ordersheet::PERMISSION_MANAGE);

		if (!$this->request->archived && ($permission_manage || $permission_edit || $permission_edit_limited)) {
	
			$model = new Model($this->request->application);
			
			$user = user::instance();
			$address = $user->address;
			
			// for order sheet owners
			if (!$permission_manage && !$permission_edit) {
				$filter = " AND (address_id = $address OR address_parent = $address)";
			}
			
			$result = $model->query("
				SELECT DISTINCT
					address_id,
					CONCAT(country_name,', ',address_company) as name
				FROM
					mps_material_addresses
				INNER JOIN mps_materials ON mps_material_address_material_id = mps_material_id
				INNER JOIN db_retailnet.addresses ON address_id = mps_material_address_address_id
				INNER JOIN db_retailnet.countries ON country_id = address_country
				WHERE
					mps_material_active = 1
				AND address_active = 1
				AND mps_material_material_planning_type_id IS NOT NULL
				AND mps_material_material_collection_category_id IS NOT NULL
				AND IF (
					mps_material_standard_order_date IS NOT NULL, 
					IF (mps_material_standard_order_date >= CURRENT_DATE, 1, 0), 
					1
				) = 1
				$filter
				ORDER BY country_name, address_company
			")->fetchAll();
			
			if (count($result) > 0) { 
				
				// button: add standard order sheet
				$submit_link = $this->request->link('/applications/helpers/ordersheet.standard.php');
				$this->request->field('submit_standard_ordersheet', $submit_link);
				
				// dataloader companies
				$dataloader['companies'] = _array::extract($result);

			} else {
				$this->request->field('submit_standard_ordersheet', false);
			}
		}
		
		// access for bulk actions
		if (!$this->request->archived && $permission_manage) {
			$this->request->field('manage', $this->request->query('manage'));
			$this->request->field('submit', $this->request->query('submit'));
			$this->request->field('remind', $this->request->query('remind'));
			$this->request->field('exchange_ordersheet_items', $this->request->link('/cronjobs/exchange.ordersheet.items.php'));
			$this->request->field('exchange_sap', $this->request->link('/cronjobs/sap.import.php'));
		}
		
		// access to master sheet
		$field = $this->request->query('data');
		$this->request->field('form', $field);
		
		// template: list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'ordersheet/list.php');
		$tpl->data('dataloader', $dataloader);

		// assign template to view
		$this->view->setTemplate('mastersheet', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js'
		));
	}
	
	public function data() { 
		
		$id = url::param();
		$application = $this->request->application;
		
		// order sheet
		$ordersheet = new Ordersheet();
		$data = $ordersheet->read($id);

		// state handler
		$state = new State($ordersheet->workflowstate_id); 
		$state->setOwner($ordersheet->address_id);
		$state->setDateIntervals($ordersheet->openingdate, $ordersheet->closingdate);

		// check access and build view
		$this->checkAccess($ordersheet, $state);
		$this->buildHeader($ordersheet);
		$this->buildTabs($ordersheet, $state);

		// mastersheet
		$mastersheet = new Mastersheet();
		$mastersheet->read($ordersheet->mastersheet_id);
		
		// form dataloader
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['redirect'] = $this->request->query("data/$id");
		$data['mps_mastersheet_year'] = $mastersheet->year;
		$data['mps_mastersheet_name'] = $mastersheet->name;		

		// buttons
		$buttons = array();
		
		// button: back
		$buttons['back'] = $this->request->query();

		// for administrators
		if ($state->canAdministrate() && $state->onPreparation()) {
				
			// button: save
			$buttons['save'] = true;
			
			// button: delete
			if ($state->canDelete()) {
			
				$integrity = new Integrity();
				$integrity->set($id, 'mps_ordersheets');
				
				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/ordersheet.delete.php', array('id'=>$id));
				}
			}
			
			// not preparation mode
			if ($state->isApproved()) {
				$disabled['mps_ordersheet_comment'] = true;
				$disabled['mps_ordersheet_openingdate'] = true;
				$disabled['mps_ordersheet_closingdate'] = true;
			}

		} else {
			$fields = array_keys($data);
			$disabled = array_fill_keys($fields, true);
		}
		
		
		// allow clients to delete standard order sheets
		if (!$buttons['delete'] && ($state->isOwner() || $state->canAdministrate()) && $ordersheet->workflowstate_id == Workflow_State::STATE_OPEN) {
			if (substr($mastersheet->name, 0, strlen('Standard')) == 'Standard') {
				$buttons['delete'] = $this->request->link('/applications/modules/ordersheet/delete.php', array('id'=>$id,'standard' => $mastersheet->id));
			}
		}
		
		// button: create version
		if ($state->canEdit(false) ) {
			$buttons['version'] = $this->request->link('/applications/helpers/ordersheet.version.save.php', array('id'=>$id));
		}
		
		// button: print
		$buttons['print'] = $this->request->link('/applications/exports/excel/ordersheet.items.php',array('id'=>$id));

		if ($state->canAdministrate() && $state->isOrderConfirmed()) {
			$buttons['shipOrdersheet'] = $this->request->link('/applications/helpers/ordersheet.setToShipped.php', array('id'=>$id));
		}

		if ($state->canAdministrate() && ($state->isArchived())) {
			$buttons['unarchive'] = $this->request->link('/applications/helpers/ordersheet.unarchive.php', array('id'=>$id));
		}

		// workflo states
		$allowedStates = array();
		
		switch ($state->state) {
			case 1: $allowedStates = array(1,7); break; // open
			case 2: $allowedStates = array(1,2,7); break; // in proggress
			case 3: $allowedStates = array(1,2,3,7); break; // completed
			case 4: $allowedStates = array(1,2,3,4,7); break; // approved
			case 7: $allowedStates = array(1,7); break; // preparation
			case 8: $allowedStates = array(8); break; // revision
			default: $allowedStates = array($state->state); break;
		}	

		$states = join(',', $allowedStates);
		$filters = "mps_workflow_state_id IN ($states)";
		$dataloader['mps_ordersheet_workflowstate_id']  = Workflow_State::dropdown($this->request->application, $filters);

		if (!$state->canAdministrate() || ($state->state>4 && $state->state<>7)) {
			$disabled['mps_ordersheet_workflowstate_id'] = true;
		}

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'ordersheet/form.php');
		$tpl->data('data', $data);
		$tpl->data('dataloader', $dataloader);
		$tpl->data('disabled', $disabled);
		$tpl->data('hidden', $hidden);
		$tpl->data('buttons', $buttons);
		$tpl->data('trackings', $ordersheet->trackings());
		$tpl->data('id', $id);

		// assign template to view
		$this->view->setTemplate('ordersheet', $tpl);

		Compiler::attach(array(
			'/public/scripts/jquery/jquery.ui.css',
			'/public/scripts/jquery/jquery.ui.js',
			'/public/css/jquery.ui.css',
			'/public/scripts/validationEngine/css/validationEngine.jquery.css',
			'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
			'/public/scripts/validationEngine/js/jquery.validationEngine.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/js/form.validator.js',
			'/public/js/ordersheet.form.js'
		));
	}
	
	public function items() {
		
		$id = url::param();
		$application = $this->request->application;
		
		// order sheet
		$ordersheet = new Ordersheet();
		$data = $ordersheet->read($id);

		// state handler
		$state = new State($ordersheet->workflowstate_id); 
		$state->setOwner($ordersheet->address_id);
		$state->setDateIntervals($ordersheet->openingdate, $ordersheet->closingdate);

		// check access and build view
		$this->checkAccess($ordersheet, $state);
		$this->buildHeader($ordersheet);
		$this->buildTabs($ordersheet, $state);

		// mastersheet
		$mastersheet = new Mastersheet();
		$mastersheet->read($ordersheet->mastersheet_id);

		$model = new Model($application);

		if (!$state->canAdministrate()) {
			
			$res = $model->query("
				SELECT mps_material_planning_type_id, mps_material_planning_is_dummy
				FROM mps_material_planning_types
				WHERE mps_material_planning_is_dummy = 1
			")->fetchAll();

			$dummyPlanningTypes = _array::extract($res);
		}

		
		$data['redirect'] = $this->request->query("items/$id");

		// buttons
		$buttons = array();

		// table columns
		$columns = array();
		
		// button back
		$buttons['back'] = ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $this->request->query(),
			'label' => translate::instance()->back
		));

		if ($state->canApprov()) {

			$buttons['approve_sendmail'] = ui::button(array(
				'id' => 'approve_sendmail',
				'icon' => 'save',
				'class' => 'submit-items sendmail',
				'href' => $this->request->link('/applications/modules/ordersheet/items.approve.php',array('id'=>$id)),
				'label' => translate::instance()->approve." & Send Mail",
				'data-template' => 2
			));

			$buttons['approve'] = ui::button(array(
				'id' => 'approve',
				'icon' => 'save',
				'class' => 'submit-items',
				'href' => $this->request->link('/applications/modules/ordersheet/items.approve.php',array('id'=>$id)),
				'label' => translate::instance()->approve
			));
		}

		// revision buttons
		if ($state->canSetRevisions()) { 
			$buttons['revision'] = ui::button(array(
				'id' => 'revision',
				'icon' => 'reload',
				'class' => 'submit-items sendmail',
				'href' => $this->request->link('/applications/modules/ordersheet/items.revision.php',array('id'=>$id)),
				'label' => translate::instance()->revision,
				'data-template' => 3
			));
		}

		// submit button
		if ($state->canSubmit()) { 
			$buttons['submit'] = ui::button(array(
				'id' => 'submit',
				'icon' => 'save',
				'class' => 'submit-items',
				'href' => $this->request->link('/applications/modules/ordersheet/items.submit.php',array('id'=>$id)),
				'label' => translate::instance()->submit
			));
		}

		// button submit
		if ($state->canEdit()) {
			if ( ($state->canAdministrate() && $state->onPreparation()) || ($state->isOwner() && $state->onCompleting())) {
				$buttons['save'] = ui::button(array(
					'id' => 'save',
					'icon' => 'save',
					'class' => 'submit-items',
					'href' => $this->request->link('/applications/modules/ordersheet/items.save.php',array('id'=>$id)),
					'label' => translate::instance()->save
				));
			}
		}

		// columns on preparation state
		if ($state->onPreparation()) {

			if ($state->canAdministrate()) {
				$columns['proposed']['show'] = true;
				$columns['quantity']['show'] = true;
				$columns['approved']['show'] = true;
				$columns['proposed']['edit'] = $state->canEdit();
				$columns['quantity']['edit'] = $state->canEdit();
				$columns['approved']['edit'] = $state->canEdit();
			}
			
			if ($state->isOwner() ) {
				$columns['proposed']['show'] = true;
				$columns['quantity']['show'] = true;
				$columns['approved']['show'] = $state->isApproved();
				$columns['quantity']['edit'] = $state->canEdit() && $state->onCompleting() && !$state->isApproved() ? true : false;
			}
		}
		
		// columns on confirmation state
		if ($state->onConfirmation())  {
			$columns['quantity']['show'] = true;
			$columns['approved']['show'] = true;
			$columns['confirmed']['show'] = $state->isConsolidated() ? false : true;
		}
		
		// columns on distribution
		if ($state->onDistribution() || $state->isDistributed() || $state->isArchived()) {
			$columns['approved']['show'] = true;
			$columns['confirmed']['show'] = true;
			$columns['shipped']['show'] = true;
			$columns['distributed']['show'] = true;
		}

		// unset save button on revision states
		if ($state->isCompleted() || $state->isRevision()) {
			unset($buttons['save']);
		}

		// check revision state
		if ($state->isRevision() && $state->isOwner()) {
		
			$items = $ordersheet->item()->load(array('mps_ordersheet_item_status=1'));
			
			if ($items) $columns['quantity']['edit'] = true;
			else unset($buttons['submit']);
		}

		// ordersheet form
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'ordersheet/items.php');
		$tpl->data('data', $data);
		$tpl->data('dummyPlanningTypes', $dummyPlanningTypes);
		$tpl->data('columns', $columns);
		$tpl->data('buttons', $buttons);
		$tpl->data('id', $id);

		$this->request->field('ordersheet', $id);
		$this->request->field('ordersheets', $id);
		$this->request->field('sendmail', false);
		
		// assign template to view
		$this->view->setTemplate('ordersheet.items', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js',
			'/public/js/ordersheet.items.js',
		));
	}
	
	public function files() {
		
		$id = url::param();
		$application = $this->request->application;
		
		// order sheet
		$ordersheet = new Ordersheet();
		$data = $ordersheet->read($id);

		// state handler
		$state = new State($ordersheet->workflowstate_id); 
		$state->setOwner($ordersheet->address_id);
		$state->setDateIntervals($ordersheet->openingdate, $ordersheet->closingdate);

		// check access and build view
		$this->checkAccess($ordersheet, $state);
		$this->buildHeader($ordersheet);
		$this->buildTabs($ordersheet, $state);

		// mastersheet
		$mastersheet = new Mastersheet();
		$mastersheet->read($ordersheet->mastersheet_id);

		// buttons
		$buttons = array();
		
		// button back
		$buttons['back'] = ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $this->request->query(),
			'label' => translate::instance()->back
		));

		$model = new Model($application);

		$result = $model->query("
			SELECT DISTINCT
				mps_material_collection_category_file_id AS id,
				mps_material_collection_category_file_title AS title,
				mps_material_collection_category_file_description AS description,
				mps_material_collection_category_file_path AS path,
				DATE_FORMAT(mps_material_collection_category_files.date_created,'%d.%m.%Y') AS date
			FROM mps_ordersheet_items
			INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
			LEFT JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id
			INNER JOIN mps_material_collection_category_files ON mps_material_collection_category_file_collection_id = mps_material_collection_category_id
			WHERE mps_material_collection_category_file_visible = 1 AND mps_ordersheet_item_ordersheet_id = $id
			ORDER BY mps_material_collection_category_file_title ASC
		")->fetchAll();

		$collectionFiles = $result ?: array();

		// mastersheet files
		$result = $model->query("
			SELECT DISTINCT
				mps_mastersheet_file_id AS id,
				mps_mastersheet_file_title AS title,
				mps_mastersheet_file_description AS desciption,
				mps_mastersheet_file_path AS path,
				DATE_FORMAT(mps_mastersheet_files.date_created,'%d.%m.%Y') AS date
			FROM mps_mastersheet_files
			INNER JOIN db_retailnet.file_types ON file_type_id = mps_mastersheet_file_type
			INNER JOIN mps_ordersheets ON mps_ordersheet_mastersheet_id = mps_mastersheet_file_mastersheet_id
			WHERE mps_mastersheet_file_visible = 1 AND mps_ordersheet_id = $id
			ORDER BY file_type_name ASC, mps_mastersheet_file_title ASC
		")->fetchAll();

		$mastersheetFiles = $result ?: array();
		
		$datagrid = array_merge($collectionFiles, $mastersheetFiles);
		
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'file/list.php');
		$tpl->data('data', $data);
		$tpl->data('datagrid', $datagrid);
		$tpl->data('buttons', $buttons);
		$this->view->setTemplate('ordersheet.file', $tpl);
	}
	
	public function versions() {
		
		$id = url::param();
		$version = url::param(1);
		$application = $this->request->application;
		
		// order sheet
		$ordersheet = new Ordersheet();
		$data = $ordersheet->read($id);

		// state handler
		$state = new State($ordersheet->workflowstate_id); 
		$state->setOwner($ordersheet->address_id);
		$state->setDateIntervals($ordersheet->openingdate, $ordersheet->closingdate);

		// check access and build view
		$this->checkAccess($ordersheet, $state);
		$this->buildHeader($ordersheet);
		$this->buildTabs($ordersheet, $state);

		// mastersheet
		$mastersheet = new Mastersheet();
		$mastersheet->read($ordersheet->mastersheet_id);

		// buttons
		$buttons = array();
		
		if ($version) {
			
			$buttons['back'] = $this->request->query("versions/$id");
				
			$data = $ordersheet->data;
			$data['mps_mastersheet_year'] = $mastersheet->year;
			$data['mps_mastersheet_name'] = $mastersheet->name;
							
			// dataloader: workwlow states
			$dataloader['mps_ordersheet_workflowstate_id'] = Workflow_State::loader($this->request->application);
			
			// disable all fields
			$fields = array_keys($data);
			$disabled = array_fill_keys($fields, true);
			
			// button: delete
			if ($state->canEdit(false) ) {
				$buttons['delete'] = $this->request->link('/applications/modules/ordersheet/delete.version.php', array('id' => $version));
			}
			
			// button: print
			$buttons['print'] = $this->request->link('/applications/exports/excel/ordersheet.items.version.php', array('ordersheet' => $id,'version' => $version));
			
			$columns = array();
			
			// order sheet preparation
			if ($state->onPreparation()) {
				$columns['proposed']['show'] = true;
				$columns['quantity']['show'] = true;
				$columns['approved']['show'] = ($state->owner && !$state->isApproved()) ? false : true;
			}
			
			// order sheet confirmation
			if ($state->onConfirmation())  {
				$columns['quantity']['show'] = true;
				$columns['approved']['show'] = true;
				$columns['confirmed']['show'] = ($state->isConsolidated()) ? false : true;
			}
			
			// order sheet distribution
			if ($state->onDistribution()) {
				$columns['quantity']['show'] = true;
				$columns['approved']['show'] = true;
				$columns['confirmed']['show'] = true;
				$columns['shipped']['show'] = true;
				$columns['distributed']['show'] = true;
			}
				
			// order sheet archive
			if ($state->isDistributed()) {
				$columns['quantity']['show'] = true;
				$columns['approved']['show'] = true;
				$columns['confirmed']['show'] = true;
				$columns['shipped']['show'] = true;
				$columns['distributed']['show'] = true;
			}
			
			// template: order sheet version detail view
			$this->view->orderSheetVersionForm('pagecontent')
			->ordersheet('version.data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('columns', $columns)
			->data('id', $id)
			->data('version', $version);
		}
		else {

			$buttons['back'] = $this->request->query();
			
			// request field id
			$this->request->field('id', $id);
			
			// request field fomr link
			$link = $this->request->query("versions/$id");
			$this->request->field('form', $link);
			
			// template: order sheet versions
			$this->view->orderSheetVersionsList('pagecontent')
			->ordersheet('version.list')
			->data('data', $data)
			->data('buttons', $buttons)
			->data('id', $id);

			Compiler::attach(array(
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js'
			));
		}
	}
	
	public function cost() {
		
		$id = url::param();
		$application = $this->request->application;
		
		// order sheet
		$ordersheet = new Ordersheet();
		$data = $ordersheet->read($id);

		// state handler
		$state = new State($ordersheet->workflowstate_id); 
		$state->setOwner($ordersheet->address_id);
		$state->setDateIntervals($ordersheet->openingdate, $ordersheet->closingdate);

		// check access and build view
		$this->checkAccess($ordersheet, $state);
		$this->buildHeader($ordersheet);
		$this->buildTabs($ordersheet, $state);

		// mastersheet
		$mastersheet = new Mastersheet();
		$mastersheet->read($ordersheet->mastersheet_id);

		// buttons
		$buttons = array();
		$buttons['back'] = $this->request->query();
			
		// template: order sheet cost
		$this->view->orderSheetCost('pagecontent')
		->ordersheet('cost')
		->data('data', $data)
		->data('buttons', $buttons)
		->data('id', $id);
	}
	
	public function planning() {
		
		$id = url::param();
		$version = url::param(1);
		$application = $this->request->application;
		$translate = translate::instance();
		$user = User::instance();

		// filters
		$_DISTRIBUTION_CHANNEL = $_REQUEST['distribution_channels'];
		$_TURNOVER_CLASS_WATCH = $_REQUEST['turnoverclass_watches'];
		$_SALES_REPRESENTATIVE = $_REQUEST['sales_representative'];
		$_DECORATION_PERSON = $_REQUEST['decoration_person'];
		$_PROVINCE = $_REQUEST['provinces'];
		$_FILTER_ACTIVE = null;
		
		// order sheet
		$ordersheet = new Ordersheet();
		$ordersheet->read($id);

		// state handler
		$state = new State($ordersheet->workflowstate_id); 
		$state->setOwner($ordersheet->address_id);
		$state->setDateIntervals($ordersheet->openingdate, $ordersheet->closingdate);

		$company = new Company();
		$company->read($ordersheet->address_id);
		$companyExportSap = $company->do_data_export_from_mps_to_sap;

		$model = new Model($application);

		// manually archived order sheet
		$manually_archived = $ordersheet->isManuallyArchived();
		
		// add stock reserve warehouse if not exist
		$ordersheet->warehouse()->addStockReserve();
		
		// page title
		$this->view->pagetitle = $ordersheet->header().' ('.$ordersheet->workflowstate()->name.')';

		// filters
		$_FILTERS = array();
		$_FIELDS = array();
		$_BUTTONS = array();

		// filter: pos
		$_FILTERS['default'] = "posaddress_client_id = $ordersheet->address_id";

		// filter: distribution channels
		if ($_DISTRIBUTION_CHANNEL) {
			$_FILTERS['distribution_channels'] = "posaddress_distribution_channel = $_DISTRIBUTION_CHANNEL";
			$_FILTER_ACTIVE = 'active';
		}

		// filter: turnover classes
		if ($_TURNOVER_CLASS_WATCH) {
			$_FILTERS['turnoverclass_watches'] = "posaddress_turnoverclass_watches = $_TURNOVER_CLASS_WATCH";
			$_FILTER_ACTIVE = 'active';
		}

		// filter: sales represenatives
		if ($_SALES_REPRESENTATIVE) {
			$_FILTERS['sales_representative'] = "posaddress_sales_representative = $_SALES_REPRESENTATIVE";
			$_FILTER_ACTIVE = 'active';
		}

		// filter: decoration persons
		if ($_DECORATION_PERSON) {
			$_FILTERS['decoration_person'] = "posaddress_decoration_person = $_DECORATION_PERSON";
			$_FILTER_ACTIVE = 'active';
		}

		// provinces
		if ($_PROVINCE) {
			$_FILTERS['provinces'] = "province_id = $_PROVINCE";
			$_FILTER_ACTIVE = 'active';
		}

		// for archived ordersheets show only POS locations which has distributed quantities 
		if ($state->isDistributed() and false) {
			
			$result = $model->query("
				SELECT GROUP_CONCAT(DISTINCT mps_ordersheet_item_delivered_posaddress_id) AS pos
				FROM mps_ordersheet_item_delivered
				INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id
				WHERE mps_ordersheet_item_ordersheet_id = $ordersheet->id AND mps_ordersheet_item_delivered_posaddress_id > 0 $_FILTERS
			")->fetchAll();

			$_FILTERS['active'] = "posaddress_id IN ({$result[pos]})";

		} else {
			$_FILTERS['active'] = "(
				posaddress_store_closingdate = '0000-00-00'
				OR posaddress_store_closingdate IS NULL
				OR posaddress_store_closingdate = ''
			)";
		}
		
		// button: print
		$_BUTTONS['print'] = array(
			'icon' => 'print',
			'href' => $this->request->link('/applications/exports/excel/ordersheet.planning.php', array('ordersheet'=>$id)),
			'label' => $translate->print
		);
		
		// filters button
		$_BUTTONS['pop_filter'] = array(
			'icon' => 'direction-down',
			'data' => '#filters',
			'label' => 'Filters'
		);
		
		// button: update order sheet items
		if ($state->canEdit() && $state->onCompleting() ) {
			$_BUTTONS['update'] = array(
				'icon' => 'save',
				'href' => $this->request->link('/applications/modules/ordersheet/planning/update.items.php', array('ordersheet'=>$id)),
				'label' => $translate->update_ordersheet,
				'class' => 'submit popup_close'
			);
		}

		// button: update from planning
		if ($state->canEdit(false) && $state->isShipped() || $state->isPartiallyShipped()) {

			if ($ordersheet->hasShippedQuantities() && $ordersheet->item()->hasPlannedQuantities()) {
				$_BUTTONS['update_from_planning'] = array(
					'icon' => 'save',
					'href' => $this->request->link('/applications/modules/ordersheet/planning/update.items.php', array('ordersheet' => $id)),
					'label' => $translate->update_ordersheet_from_planning,
					'class' => 'submit'
				);
			}
		}
		
		// button: confirm distribution
		if ($state->canEdit(false) && ($state->isShipped() || $state->onDistribution() || $state->isManuallyArchived()) ) {
			
			$_BUTTONS['confirm'] = array(
				'icon' => 'like',
				'href' => $this->request->link('/applications/modules/ordersheet/planning/confirm.php', array('ordersheet' => $id)),
				'label' => $translate->confirm_distribution,
				'class' => 'dialog'
			);
		}

		// button: put order sheet to archive
		if ($state->canEdit(false) && !$state->isDistributed() && !$state->isManuallyArchived() && ($state->isShipped() || $state->onDistribution())) {
			$_BUTTONS['partially_distribution'] = array(
				'icon' => 'save',
				'href' => $this->request->link('/applications/helpers/ordersheet.item.planning.partially.php', array('ordersheet' => $id)),
				'label' => 'Put Order Sheet to Archive',
				'class' => 'dialog'
			);
		}

		// button: activate order sheet
		if ( ($state->isOwner() || $state->canAdministrate()) && ( $state->isManuallyArchived() || ($state->isDistributed() && $ordersheet->hasDistributedQuantities() )) ) {
			$_BUTTONS['activate'] = array(
				'class' => 'blue',
				'href' => $this->request->link('/applications/modules/ordersheet/planning/activate.php', array('ordersheet' => $id)),
				'label' => 'Activate Order Sheet'
			);
		}

		// load distribution channels
		$result = $model->query("
			SELECT DISTINCT
				mps_distchannel_id,
				CONCAT(mps_distchannel_name, ' - ', mps_distchannel_code) AS distribution_channel
			FROM db_retailnet.posaddresses
		")
		->bind(Pos::DB_BIND_PLACES)
		->bind(Place::DB_BIND_PROVINCES)
		->bind(Pos::DB_BIND_DISTRIBUTION_CHANNELS)
		->filter($_FILTERS)
		->exclude('distribution_channels')
		->order('mps_distchannel_name, mps_distchannel_code')
		->fetchAll();
		
		$_FIELDS['distribution_channels'] = $result ? ui::dropdown($result, array(
			'id' => 'distribution_channels',
			'name' => 'distribution_channels',
			'value' => $_DISTRIBUTION_CHANNEL,
			'caption' => $translate->all_distribution_channels
		)) : null;
		

		// get turnover class watches
		$result = $model->query("
			SELECT DISTINCT
				mps_turnoverclass_id, mps_turnoverclass_name
			FROM db_retailnet.posaddresses
		")
		->bind(Pos::DB_BIND_PLACES)
		->bind(Place::DB_BIND_PROVINCES)
		->bind(Pos::DB_BIND_TURNOVER_CLASS_WATCHES)
		->filter('company', "posaddress_franchisee_id = $ordersheet->address_id")
		->order('mps_turnoverclass_name')
		->fetchAll();
		
		$_FIELDS['turnoverclass_watches'] = $result ? ui::dropdown($result, array(
			'name' => 'turnoverclass_watches',
			'id' => 'turnoverclass_watches',
			'value' => $_TURNOVER_CLASS_WATCH,
			'caption' => $translate->all_turnover_class_watches
		)) : null;


		// get sales representatives
		$result = $model->query("
			SELECT DISTINCT
				mps_staff_id,
				CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS name
			FROM db_retailnet.posaddresses
		")
		->bind(Pos::DB_BIND_PLACES)
		->bind(Place::DB_BIND_PROVINCES)
		->bind(Pos::DB_BIND_MPS_STAFF_SALES_REPRESENTATIVES)
		->filter($_FILTERS)
		->filter('staff_type', 'mps_staff_staff_type_id = 1')
		->filter('limited', $state->isOwner() ? "mps_staff_address_id = $user->address" : "mps_staff_address_id > 0" )
		->exclude('sales_representative')
		->order('mps_staff_name, mps_staff_firstname')
		->fetchAll();
			
		$_FIELDS['sales_representative'] = $result ? ui::dropdown($result, array(
			'name' => 'sales_representative',
			'id' => 'sales_representative',
			'value' => $_SALES_REPRESENTATIVE,
			'caption' => $translate->all_sales_representatives
		)) : array();
		
		// decorators
		$result = $model->query("
			SELECT DISTINCT
				mps_staff_id,
				CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS name
			FROM db_retailnet.posaddresses
		")
		->bind(Pos::DB_BIND_PLACES)
		->bind(Place::DB_BIND_PROVINCES)
		->bind(Pos::DB_BIND_MPS_STAFF_DECORATOR_PERSONS)
		->filter($_FILTERS)
		->filter('staff_type', 'mps_staff_staff_type_id = 2')
		->filter('limited', $state->isOwner() ?  "mps_staff_address_id = $user->address" : 'mps_staff_address_id > 0')
		->exclude('decoration_person')
		->order('mps_staff_name, mps_staff_firstname')
		->fetchAll();

		$_FIELDS['decoration_person'] = $result ? ui::dropdown($result, array(
			'name' => 'decoration_person',
			'id' => 'decoration_person',
			'value' => $_DECORATION_PERSON,
			'caption' => $translate->all_decoration_persons
		)) : null;
		
		$result = $model->query("
			SELECT DISTINCT
				province_id, province_canton
			FROM db_retailnet.posaddresses")
		->bind(Pos::DB_BIND_PLACES)
		->bind(Place::DB_BIND_PROVINCES)
		->filter($_FILTERS)
		->exclude('provinces')
		->order('province_canton')
		->fetchAll();
		
		$_FIELDS['provinces'] = $result ? ui::dropdown($result, array(
			'name' => 'provinces',
			'id' => 'provinces',
			'value' => $_PROVINCE,
			'caption' => $translate->all_provinces
		)) : null;


		$this->view->master('spreadsheet.php');

		$_CLASS_PLANNING = $state->onPreparation() || $state->onConfirmation() ? 'planning' : null;

		// template: order sheet cost
		$this->view->orderSheetPlanning('pagecontent')->ordersheet('planning')
		->data('manually_archived', $manually_archived)
		->data('buttons', $_BUTTONS)
		->data('formFilter', $_FIELDS)
		->data('companyExportSap', $companyExportSap)
		->data('filter_active', $_FILTER_ACTIVE)
		->data('class_planning', $_CLASS_PLANNING);

		// request fields
		$this->request->field('ordersheet', $id);
		$this->request->field('version', $version);
		$this->request->field('checkQuantitiy', $state->onApproving() ? false : true);
		$this->request->field('url', $this->request->query("planning/$id"));

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/spreadsheet/spreadsheet.css',
			'/public/scripts/spreadsheet/spreadsheet.js',
			'/public/scripts/tipsy/tipsy.css',
			'/public/scripts/tipsy/tipsy.js',
			'/public/scripts/pop/pop.css',
			'/public/scripts/pop/pop.js',
			'/public/css/ordersheet.planning.css',
			'/public/js/ordersheet.planning.js'
		));
	}
	
	public function sap() {

		$id = url::param();
		$version = url::param(1);
		$application = $this->request->application;
		$translate = translate::instance();
		$_START_DATE = '2014-11-01';
		
		// order sheet
		$ordersheet = new Ordersheet();
		$ordersheet->read($id);

		// state handler
		$state = new State($ordersheet->workflowstate_id); 
		$state->setOwner($ordersheet->address_id);
		$state->setDateIntervals($ordersheet->openingdate, $ordersheet->closingdate);

		// page title
		$this->view->pagetitle = $ordersheet->header().' ('.$ordersheet->workflowstate()->name.')';
		
		// button: back
		$buttons = array();
		
		// button: print
		$buttons['print'] = array(
			'icon' => 'print',
			'href' => $this->request->link('/applications/exports/excel/ordersheet.sap.php', array('ordersheet'=>$id)),
			'label' => $translate->print
		);
		
		// filters button
		$buttons['pop_filter'] = array(
			'icon' => 'direction-down',
			'data' => '#filters',
			'label' => 'Filters'
		);

		$model = new Model($application);
		$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);

		$result = $model->query("
			SELECT 
				mps_ordersheet_item_delivered_id AS distribution,
				mps_ordersheet_item_id AS item, 
				mps_ordersheet_item_delivered_sap_purchase_order_number AS pon,
				mps_ordersheet_item_delivered_confirmed AS confirmed_date,
				sap_confirmed_items.sap_confirmed_item_id AS confirmed
			FROM mps_ordersheet_item_delivered 
			INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_delivered_ordersheet_item_id = mps_ordersheet_item_id
			LEFT JOIN sap_confirmed_items ON mps_ordersheet_item_delivered_id = sap_confirmed_item_mps_distribution_id
			WHERE mps_ordersheet_item_delivered_confirmed IS NOT NULL 
			AND mps_ordersheet_item_delivered_sap_purchase_order_number IS NOT NULL 
			AND mps_ordersheet_item_delivered_confirmed >= '$_START_DATE' AND mps_ordersheet_item_ordersheet_id = $id
		")->fetchAll();

		$items = array();
		$pons = array();
		$failures = array();

		if ($result) {

			foreach ($result as $row) {
				if (!$row['confirmed']) {
					$item = $row['distribution'];
					$items[$item] = $row['item'];
					$pons[] = $row['pon'];
				}
			}

			$pons = array_unique(array_filter($pons));
		}

		if ($pons) { 

			$pons = "'".join("', '", $pons)."'";

			$result = $exchange->query("
				SELECT sap_salesorder_item_id AS id, sap_salesorder_item_mps_item_id AS item
				FROM sap_salesorder_items
				WHERE sap_salesorder_item_fetched_by_sap IS NOT NULL 
				AND sap_salesorder_item_mps_po_number IN ($pons)
			")->fetchAll();

			if ($result) {

				foreach ($result as $row) {
					$item = $row['item'];
					if ($items[$item]) {
						$failures = true;
						break;
					}
				}
			}
		}

		if ($failures) {
			$buttons['failures'] = array(
				'icon' => 'reload',
				'href' => $this->request->link('/applications/modules/ordersheet/sap/reorder.items.php', array('ordersheet'=>$id)),
				'label' => 'SAP Reorder Items'
			);
		}

		// template: order sheet cost
		$this->view->orderSheetPlanning('pagecontent')
		->ordersheet('sap.import')
		->data('buttons', $buttons);

		// request fields
		$this->request->field('ordersheet', $id);
		$this->request->field('version', $version);
		$this->request->field('url', $this->request->query("sap/$id"));

		$this->view->master('spreadsheet.php');

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/spreadsheet/spreadsheet.css',
			'/public/scripts/spreadsheet/spreadsheet.js',
			'/public/scripts/tipsy/tipsy.css',
			'/public/scripts/tipsy/tipsy.js',
			'/public/scripts/pop/pop.css',
			'/public/scripts/pop/pop.js',
			'/public/css/ordersheet.planning.css',
			'/public/js/ordersheet.sap.js'
		));
	}

	public function sapFailures() {

		// access to master sheet
		$application = $this->request->application;
		$link = "/$application/ordersheets/data";
		$this->request->field('form', $link);

		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'list.php');
		$tpl->data('url', '/applications/modules/ordersheet/sap/failures.php');
		$tpl->data('class', 'list-600');
		$this->view->setTemplate('sapFailures', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js'
		));
	}
	
	public function manage() {

		$application = $this->request->application;

		// check access to page
		if (!user::permission(Ordersheet::PERMISSION_MANAGE) || $this->request->archived) {
			message::access_denied();
			url::redirect($this->request->query());
		}

		// db model
		$model = new Model($application);

		// dataloader workflow states
		$sth = $model->db->prepare("
			SELECT DISTINCT mps_mastersheet_year, mps_mastersheet_year AS year 
			FROM mps_mastersheets
			WHERE mps_mastersheet_archived = 0 AND mps_mastersheet_consolidated = 0
			ORDER BY mps_mastersheet_year
		");

		$sth->execute();
		$result = $sth->fetchAll();
		$dataloader['mps_mastersheet_year'] = _array::extract($result);

		// dataloader workflow states
		$sth = $model->db->prepare("
			SELECT 
				mps_workflow_state_id,
				mps_workflow_state_name
			FROM mps_workflow_states
			WHERE mps_workflow_state_id IN (1,7)
		");

		$sth->execute();
		$result = $sth->fetchAll();
		$dataloader['mps_ordersheet_workflowstate_id'] = _array::extract($result);
		
		// buttons
		$buttons = array();
		$buttons['back'] = $this->request->query();
		$buttons['add'] = '/applications/modules/ordersheet/manage/create.php';
		$buttons['update'] = '/applications/modules/ordersheet/manage/update.php';
		$buttons['delete_ordersheets'] = '/applications/modules/ordersheet/manage/delete.php';
		$buttons['items'] = '/applications/modules/ordersheet/manage/items.save.php';
		$buttons['delete_items'] = '/applications/modules/ordersheet/manage/items.delete.php';
		
		// form dataloader
		$data['application'] = $this->request->application;
		$data['controller'] = $this->request->controller;
		$data['action'] = $this->request->action;
		$data['redirect'] = $this->request->url;
		$data['selected_tab'] = 'new_ordersheets';

		// template: list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'ordersheet/manage.php');
		$tpl->data('data', $data);
		$tpl->data('dataloader', $dataloader);
		$tpl->data('buttons', $buttons);

		// assign template to view
		$this->view->setTemplate('ordersheet.manage', $tpl);

		Compiler::attach(array(
			'/public/scripts/jquery/jquery.ui.css',
			'/public/scripts/jquery/jquery.ui.js',
			'/public/css/jquery.ui.css',
			'/public/scripts/validationEngine/css/validationEngine.jquery.css',
			'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
			'/public/scripts/validationEngine/js/jquery.validationEngine.js',
			'/public/scripts/qtip/qtip.css',
			'/public/scripts/qtip/qtip.js',
			'/public/scripts/chain/chained.select.js',
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js',
			'/public/js/ordersheet.manager.js'
		));
	}
	
	public function submit() {
		
		$state = 7;
		$application = $this->request->application;
		
		// check access to page
		if (!user::permission(Ordersheet::PERMISSION_MANAGE) || $this->request->archived) {
			message::access_denied();
			url::redirect($this->request->query());
		}
			
		// Mail Template
		$mail = new Mail_Template();
		$maildata = $mail->read(1);
		$maildata['mail_template_text'] = nl2br($maildata['mail_template_text']);
		
		// button
		$buttons = array();
		$buttons['back'] = $this->request->query();
		$buttons['submit_sendmail'] = true;
		$buttons['submit'] = true;
		
		// template: submit list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'ordersheet/submit.php');
		$tpl->data('dataloader', $dataloader);
		$tpl->data('buttons', $buttons);
		$tpl->data('data', $data);
		$tpl->data('modal_caption', 'Submit Order Sheets');
		$tpl->data('maildata', $maildata);
		$tpl->data('workflow_states', $state);
		
		// assign template to view
		$this->view->setTemplate('ordersheet.submit', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js',
			'/public/scripts/textarea.expander.js',
			'/public/js/ordersheet.submit.js'
		));

	}
	
	public function remind() {

		$state = '1,2';
		$application = $this->request->application;

		// check access to page
		if (!user::permission(Ordersheet::PERMISSION_MANAGE) || $this->request->archived) {
			message::access_denied();
			url::redirect($this->request->query());
		}
			
		// Mail Template
		$mail = new Mail_Template();
		$maildata = $mail->read(5);
		$maildata['mail_template_text'] = nl2br($maildata['mail_template_text']);
		
		// button
		$buttons = array();
		$buttons['back'] = $this->request->query();
		$buttons['remind'] = true;
		
		// template: remind list
		$tpl = new Template('pagecontent');
		$tpl->template(PATH_TEMPLATES.'ordersheet/submit.php');
		$tpl->data('dataloader', $dataloader);
		$tpl->data('buttons', $buttons);
		$tpl->data('data', $data);
		$tpl->data('modal_caption', 'Remind Order Sheets');
		$tpl->data('maildata', $maildata);
		$tpl->data('workflow_states', $state);
		
		// assign template to view
		$this->view->setTemplate('ordersheet.submit', $tpl);

		Compiler::attach(array(
			'/public/scripts/dropdown/dropdown.css',
			'/public/scripts/dropdown/dropdown.js',
			'/public/scripts/table.loader.js',
			'/public/scripts/textarea.expander.js',
			'/public/js/ordersheet.submit.js'
		));
	}	

	private function buildTabs($ordersheet, $state) {

		$showPlanningTab = false;
		$showSAPtab = false;

		$items = $ordersheet->item()->load();
		
		if ($items) {
			
			$shippedItems = $ordersheet->hasShippedQuantities();
			$approvedItems = $ordersheet->hasApprovedQuantities();
			
			if ($state->onPreparation()) {
				$showPlanningTab = $state->isApproved() && !$approvedItems ? false : true;
			}elseif ($state->onConfirmation()) {
				$showPlanningTab = $approvedItems ? true : false;
			}else {
				$showPlanningTab = $shippedItems ? true : false;
			}
			
			if ( ($state->onDistribution() || $this->request->archived) && $shippedItems) {
				$company = new Company();
				$company->read($ordersheet->address_id);
				$showSAPtab = $company->do_data_export_from_mps_to_sap;
			}
			
		} else {
			//$tab = substr($this->request->query('items'), 1);
			//$this->request->exclude($tab);
		}

		$tabs = array();
		
		if ($showPlanningTab) { 
			$planningTab = substr($this->request->query('planning'),1);
			$tabs[$planningTab] = array(
				'icon' => "<span class='icon icon46'></span>",
				'class' => 'popup',
				'caption' => $state->onDistribution() || $state->isDistributed() ? 'Distribution' : 'Planning',
				'name' => 'PlanningSpreadsheet'
			);

		} else {
			$tab = substr($this->request->query('planning'), 1); 
			$this->request->exclude($tab);
		}
		
		if ($showSAPtab) { 
			$sapTab = substr($this->request->query('sap'),1);
			$tabs[$sapTab] = array(
				'icon' => "<span class='icon icon46'></span>",
				'class' => 'popup',
				'name' => 'SAPSpreadsheet'
			);

		} else {
			$tab = substr($this->request->query('sap'), 1); 
			$this->request->exclude($tab);
		}

		$this->view->tabs('pagecontent')->navigation('tab')->data('data', $tabs);

		return $items;
	}


	private function buildHeader($ordersheet, $view=true) {

		if ($view) {
			$this->view->header('pagecontent')->node('header')
			->data('title', $ordersheet->header())
			->data('subtitle', 'Workflow status: '.$ordersheet->workflowstate()->name);
		} else {
			return array(
				'title' => $ordersheet->header(),
				'subtitle' => $ordersheet->workflowstate()->name
			);
		}
	}


	private function checkAccess($ordersheet, $state) {

		if (!$ordersheet->access()) {
			message::access_denied();
			url::redirect($this->request->query());
		}

		// show archived order sheet only in archive
		if (!$this->request->archived && $state->isDistributed()) {
			url::redirect($this->request->query("archived/data/$id"));
		}
	}
}
