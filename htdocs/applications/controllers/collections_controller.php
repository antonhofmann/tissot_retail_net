<?php 

	class Collections_Controller {
		
		public function __construct() {
			
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {

			// view collection
			$link = $this->request->query('data');
			$this->request->field('form', $link);

			// expor collections
			$link = $this->request->link('/applications/exports/excel/material.collections.php');
			$this->request->field('export', $link);

			// app vars
			switch ($this->request->application) {
				
				case 'mps':
					$permissionEdit = user::permission(Material_Collection::PERMISSION_EDIT);
				break;

				case 'lps':
					$permissionEdit = user::permission('can_edit_lps_collections');
				break;
			}

			// button: add new
			if (!$this->request->archived && $permissionEdit) {
				$link = $this->request->query('add');
				$this->request->field('add', $link);
			}
			
			// button: print
			$link = $this->request->link('/applications/modules/collection/print.php');
			$this->request->field('print', $link);

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'list.php');
			$tpl->data('url', '/applications/modules/collection/list.php');
			$tpl->data('class', 'list-600');
			$this->view->setTemplate('collections', $tpl);

			Compiler::attach(array(
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js'
			));
		}
		
		public function add() {
			
			$permissionEdit = false;

			switch ($this->request->application) {
				
				case 'mps':
					$permissionEdit = user::permission(Material_Collection::PERMISSION_EDIT);
				break;

				case 'lps':
					$permissionEdit = user::permission('can_edit_lps_collections');
				break;
			}

			// check access
			if($this->request->archived || !$permissionEdit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
	
			// form hidden vars
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			$data['collection_active'] = true;

			Compiler::attach(array(
				'/public/scripts/validationEngine/css/validationEngine.jquery.css',
				'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
				'/public/scripts/validationEngine/js/jquery.validationEngine.js',
				'/public/scripts/qtip/qtip.css',
				'/public/scripts/qtip/qtip.js',
				'/public/js/form.validator.js'
			));
				
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'collection/form.php');
			$tpl->data('data', $data);
			$tpl->data('buttons', $buttons);

			$this->view->setTemplate('collection', $tpl);
		}
		
		public function data() {
				
			$id = url::param();
			$application = $this->request->application;
			$permissionEdit = false;

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			// app vars
			switch ($application) {
				
				case 'mps':
					$permissionEdit = Material_Collection::PERMISSION_EDIT;
					$tableName = 'mps_material_collections';
					$prefix = 'mps_material_';
				break;

				case 'lps':
					$permissionEdit = 'can_edit_lps_collections';
					$tableName = 'lps_collections';
					$prefix = 'lps_';
				break;
			}

			// modul: collection
			$collection = new Modul($application);
			$collection->setTable($tableName);
			$data = $collection->read($id);

			// check access
			if (!$collection->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			} 

			// replace key prefix
			$data = _array::replaceKey($prefix, null, $data);

			if (!$this->request->archived) {
		
				if (user::permission(Material_Collection::PERMISSION_EDIT)) {
					
					$buttons['save'] = true;
					
					$integrity = new Integrity();
					$integrity->set($id, $tableName);
					
					if ($integrity->check()) {
						$buttons['delete'] = $this->request->link('/applications/modules/collection/delete.php', array('id'=>$id));
					}

				} else {
					$disabled = true;
				}

			} else {
				$disabled = true;
			}

			// request vars
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// script integration
			Compiler::attach(array(
				'/public/scripts/validationEngine/css/validationEngine.jquery.css',
				'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
				'/public/scripts/validationEngine/js/jquery.validationEngine.js',
				'/public/scripts/qtip/qtip.css',
				'/public/scripts/qtip/qtip.js',
				'/public/js/form.validator.js'
			));

			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'collection/form.php');
			$tpl->data('data', $data);
			$tpl->data('buttons', $buttons);

			// disable form
			if ($disabled) {
				$fields = array_keys($data);
				$tpl->data('disabled', array_fill_keys($fields, true));
			}

			$this->view->setTemplate('collection', $tpl);
		}
	}