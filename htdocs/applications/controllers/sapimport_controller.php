<?php 

	class SapImport_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			$this->translate = Translate::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() { 
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			
			// request form: link edit
			$this->request->field('data', $this->request->query('data'));
		
			$this->view->sapImport('pagecontent')->pos('sap.import.list');
		}
		
		public function data() {
			
			$id = url::param();
			
			$model = new Model(Connector::DB_CORE);
			
			$data = $model->query("
				SELECT *, 
					DATE_FORMAT(sap_imported_posaddresses.date_created, '%d.%m.%Y') AS version
				FROM sap_imported_posaddresses
				LEFT JOIN sap_countries ON sap_country_sap_code = sap_imported_posaddress_country
				WHERE sap_imported_posaddress_id = $id	
			")->fetch();

			if (!$data['sap_imported_posaddress_id']) {
				Message::error('POS not found.');
				url::redirect($this->request->query());
			}

			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");

			// form fileds
			$fields = $data;
			
			// header
			if (strlen($data['sap_country_country_name'])) {
				$header = $data['sap_country_country_name'];	
			}
			
			if ($data['sap_imported_posaddress_city']) {
				$header .= ', '.$data['sap_imported_posaddress_city'];
			}
				
			if ($data['sap_imported_posaddress_name1']) {
				$header .= ', '.$data['sap_imported_posaddress_name1'];
			}

			if ($data['version']) {
				$header .= ', '.$data['version'];
			}


			// import country
			$country = $data['sap_county_retailnet_country_id'];
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// dataloader
			$dataloader = array();

			$posaddresses = array();

			$posData = array();

			// get pos locations from SAP number
			$result = $model->query("
				SELECT *
				FROM posaddresses
				WHERE posaddress_sapnumber = '{$data[sap_imported_posaddress_sapnr]}'
			")->fetchAll();


			if (count($result) > 1) {
				
				$sap = trim($data['sap_imported_posaddress_sapnr']);
				$zip = trim($data['sap_imported_posaddress_zip']);

				foreach ($result as $row) {
					
					if (!$posData && $sap==trim($row['posaddress_sapnumber']) && $zip==trim($row['posaddress_zip'])) {
						$posData = $row; 
					}

					if (!$selected) {
						$selected = $row['posaddress_id']==$posData['posaddress_id'] ? true : false;
					}

					$posaddresses[$row['posaddress_id']] = $row['posaddress_name'];
				}
			} 
			elseif($result[0]) {
				$posData = $result[0];
				$posaddresses[$posData['posaddress_id']] = $posData['posaddress_name'];
			}

			$properties = array(
				'id' => 'posaddress_id',
				'name' => 'posaddress_id',
				'value' => $posData['posaddress_id']
			);

			if ($selected) {
				$properties['selected'] = 'selected';
			}

			// pos location dropdown
			$dataloader['posaddresses'] = ui::dropdown($posaddresses, $properties);

			// pos builder


			$pos = new Pos();
			if(count($posData) == 0) {
				$pos->read(0);
			}
			else
			{
				$pos->read($posData['posaddress_id']);
			}

			// form datalaoder
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;

			// pos builder
			if ($pos->id) {

				
				$company = new Company();
				$company->read($pos->franchisee_id);
				
				if ($company->company) {
					$data['owner'] = "<small id='owner' data-id='$company->id' >Owner: $company->company</small><br>";
				}
			}

			// permissions			
			$permission_import_all_data = user::permission('can_import_all_data');
			$permission_all_data_except_pos_name = user::permission('can_import_all_data_except_pos_name');
			$permission_only_sap_number_all_pos = user::permission('can_import_only_sap_number_all_pos');

			$keys = array_keys($fields);
			$disabled = array_fill_keys($keys,true); 

			if ($permission_import_all_data) { 
				$buttons['update'] = true;
				$buttons['ignore'] = true;
				$disabled = array_fill_keys($keys,false);
			}
			elseif ($permission_all_data_except_pos_name) {
				$buttons['update'] = true;
				$buttons['ignore'] = true;
				$disabled = array_fill_keys($keys,false);
				$disabled['sap_imported_posaddress_name1'] = true;
			} 
			elseif ($permission_only_sap_number_all_pos) {
				$buttons['update'] = true;
				$buttons['ignore'] = true;
				$disabled['sap_imported_posaddress_sapnr'] = false;
			}

			// hide update button on not matching pos locations
			if (!$pos->id && !$dataloader['posaddresses'] ) { 
				$buttons['update'] = false;
			}

			// transfer triggers
			$transferer = "<i class='fa fa-chevron-circle-right transfer'></i>";
			$transfererLetterControl = "<i class='fa fa-chevron-circle-right transfer letter-control'></i>";
			$disabledTransferer = "<i class='transfer-disabled'>&nbsp;</i>";

			// form files
			$fields = array();

			// sap number ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			$sourceName = 'sap_imported_posaddress_sapnr';
			$targetName = 'posaddress_sapnumber';
			$sourceValue = $data[$sourceName];
			$targetValue = $posData[$targetName];
			$sourcePlaceHolder = Translate::instance()->$sourceName ;
			$targetPlaceHolder = $posData[$targetName] && !$targetValue ? $posData[$targetName] : Translate::instance()->$targetName;
			$sourceClassName = $targetValue ? 'success' : 'danger';
			$sourceClassName = $posData[$targetName] ? $sourceClassName  : null;

			if ($disabled[$sourceName]) {
				$sourceValue = $sourceValue ?: "<i>$sourcePlaceHolder</i>";
				$targetValue = $targetValue ?: "<i>$targetPlaceHolder</i>";
				$fields[$sourceName]['source'] = "<span id='$sourceName ' class='input-data-placeholder' >$sourceValue</span>";
				$fields[$sourceName]['target'] = "<span id='$targetName' class='input-data-placeholder' >$targetValue</span>";
				$fields[$sourceName]['transferer'] = $disabledTransferer;
			} else {
				
				$fields[$sourceName]['source'] = html::input(array(
					'id' => $sourceName ,
					'value' => $sourceValue,
					'class' => $sourceClassName,
					'placeholder' => $sourcePlaceHolder,
					'readonly' => 'readonly'
				));
				
				$fields[$sourceName]['target'] = html::input(array(
					'id' => $targetName,
					'name' => $targetName,
					'class' => 'field-transfer',
					'value' => $targetValue,
					'placeholder' => $targetPlaceHolder
				));

				$fields[$sourceName]['transferer'] = $sourceValue ? $transferer : $disabledTransferer;
			}			

			// pos name ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			$sourceName = 'sap_imported_posaddress_name1';
			$targetName = 'posaddress_name';
			$sourceValue = $data[$sourceName];
			$targetValue = $posData[$targetName];
			$sourcePlaceHolder = Translate::instance()->$sourceName;
			$targetPlaceHolder = $posData[$targetName] && !$targetValue ? $posData[$targetName] : Translate::instance()->$targetName;
			$sourceClassName = $sourceValue && str_replace(' ', '', strtolower($sourceValue))==str_replace(' ', '', strtolower($posData[$targetName])) ? 'success' : 'danger';
			$sourceClassName = $posData[$targetName] ? $sourceClassName  : null;

			if ($disabled[$sourceName]) {
				$sourceValue = $sourceValue ?: "<i>$sourcePlaceHolder</i>";
				$targetValue = $targetValue ?: "<i>$targetPlaceHolder</i>";
				$fields[$sourceName]['source'] = "<span id='$sourceName' class='input-data-placeholder $sourceClassName' >$sourceValue</span>";
				$fields[$sourceName]['target'] = "<span id='$targetName' class='input-data-placeholder' >$targetValue</span>";
				$fields[$sourceName]['transferer'] = $disabledTransferer;
			} else {
				
				$fields[$sourceName]['source'] = html::input(array(
					'id' => $sourceName,
					'value' => $sourceValue,
					'class' => $sourceClassName,
					'placeholder' => $sourcePlaceHolder,
					'readonly' => 'readonly'
				));
				
				$fields[$sourceName]['target'] = html::input(array(
					'id' => $targetName,
					'name' => $targetName,
					'class' => 'field-transfer',
					'value' => $targetValue,
					'placeholder' => $targetPlaceHolder
				));

				$fields[$sourceName]['transferer'] = $sourceValue ? $transfererLetterControl : $disabledTransferer;
			}			


			// pos name 2 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			$sourceName = 'sap_imported_posaddress_name2';
			$targetName = 'posaddress_name2';
			$sourceValue = $data[$sourceName];
			$targetValue = $posData[$targetName];
			$sourcePlaceHolder = Translate::instance()->$sourceName;
			$targetPlaceHolder = $posData[$targetName] && !$targetValue ? $posData[$targetName] : Translate::instance()->$targetName;
			$sourceClassName = $sourceValue && str_replace(' ', '', strtolower($sourceValue))==str_replace(' ', '', strtolower($posData[$targetName])) ? 'success' : 'danger';
			$sourceClassName = $posData[$targetName] ? $sourceClassName  : null;

			if ($disabled[$sourceName]) {
				$sourceValue = $sourceValue ?: "<i>$sourcePlaceHolder</i>";
				$targetValue = $targetValue ?: "<i>$targetPlaceHolder</i>";
				$fields[$sourceName]['source'] = "<span id='$sourceName' class='input-data-placeholder $sourceClassName' >$sourceValue</span>";
				$fields[$sourceName]['target'] = "<span id='$targetName' class='input-data-placeholder' >$targetValue</span>";
				$fields[$sourceName]['transferer'] = $disabledTransferer;
			} else {
				
				$fields[$sourceName]['source'] = html::input(array(
					'id' => $sourceName,
					'value' => $sourceValue,
					'class' => $sourceClassName,
					'placeholder' => $sourcePlaceHolder,
					'readonly' => 'readonly'
				));
				
				$fields[$sourceName]['target'] = html::input(array(
					'id' => $targetName,
					'name' => $targetName,
					'class' => 'field-transfer',
					'value' => $targetValue,
					'placeholder' => $targetPlaceHolder
				));

				$fields[$sourceName]['transferer'] = $sourceValue ? $transfererLetterControl : $disabledTransferer;
			}		


			// pos addresse ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			$sourceName = 'sap_imported_posaddress_address';
			$targetName = 'posaddress_address';
			$sourceValue = $data[$sourceName];
			$targetValue = $posData[$targetName];
			$sourcePlaceHolder = Translate::instance()->$sourceName;
			$targetPlaceHolder = $posData[$targetName] && !$targetValue ? $posData[$targetName] : Translate::instance()->$targetName;
			$sourceClassName = $sourceValue && str_replace(' ', '', strtolower($sourceValue))==str_replace(' ', '', strtolower($posData[$targetName])) ? 'success' : 'danger';
			$sourceClassName = $posData[$targetName] ? $sourceClassName  : null;

			if ($disabled[$sourceName]) {
				$sourceValue = $sourceValue ?: "<i>$sourcePlaceHolder</i>";
				$targetValue = $targetValue ?: "<i>$targetPlaceHolder</i>";
				$fields[$sourceName]['source'] = "<span id='$sourceName' class='input-data-placeholder $sourceClassName' >$sourceValue</span>";
				$fields[$sourceName]['target'] = "<span id='$targetName' class='input-data-placeholder' >$targetValue</span>";
				$fields[$sourceName]['transferer'] = $disabledTransferer;
			} else {
				
				$fields[$sourceName]['source'] = html::input(array(
					'id' => $sourceName,
					'value' => $sourceValue,
					'class' => $sourceClassName,
					'placeholder' => $sourcePlaceHolder,
					'readonly' => 'readonly'
				));
				
				$fields[$sourceName]['target'] = html::input(array(
					'id' => $targetName,
					'name' => $targetName,
					'class' => 'field-transfer',
					'value' => $targetValue,
					'placeholder' => $targetPlaceHolder
				));

				$fields[$sourceName]['transferer'] = $sourceValue ? $transfererLetterControl : $disabledTransferer;
			}	


			// pos address 2 :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			$sourceName = 'sap_imported_posaddress_name2';
			$targetName = 'posaddress_address2';
			$sourceValue = $data[$sourceName];
			$targetValue = $posData[$targetName];
			$sourcePlaceHolder = Translate::instance()->$sourceName;
			$targetPlaceHolder = $posData[$targetName] && !$targetValue ? $posData[$targetName] : Translate::instance()->$targetName;
			$sourceClassName = $sourceValue && str_replace(' ', '', strtolower($sourceValue))==str_replace(' ', '', strtolower($posData[$targetName])) ? 'success' : 'danger';
			$sourceClassName = $posData[$targetName] ? $sourceClassName  : null;

			if ($disabled[$sourceName]) {
				$sourceValue = $sourceValue ?: "<i>$sourcePlaceHolder</i>";
				$targetValue = $targetValue ?: "<i>$targetPlaceHolder</i>";
				$fields['sap_imported_posaddress_address2']['source'] = "<span id='#sap_imported_posaddress_address2' class='input-data-placeholder $sourceClassName' >$sourceValue</span>";
				$fields['sap_imported_posaddress_address2']['target'] = "<span id='$targetName' class='input-data-placeholder' >$targetValue</span>";
				$fields['sap_imported_posaddress_address2']['transferer'] = $disabledTransferer;
			} else {
				
				$fields['sap_imported_posaddress_address2']['source'] = html::input(array(
					'id' => '#sap_imported_posaddress_address2',
					'value' => $sourceValue,
					'class' => $sourceClassName,
					'placeholder' => $sourcePlaceHolder,
					'readonly' => 'readonly'
				));
				
				$fields['sap_imported_posaddress_address2']['target'] = html::input(array(
					'id' => $targetName,
					'name' => $targetName,
					'class' => 'field-transfer',
					'value' => $targetValue,
					'placeholder' => $targetPlaceHolder
				));

				$fields['sap_imported_posaddress_address2']['transferer'] = $sourceValue ? $transfererLetterControl : $disabledTransferer;
			}			


			// zip code ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			$sourceName = 'sap_imported_posaddress_zip';
			$targetName = 'posaddress_zip';
			$sourceValue = $data[$sourceName];
			$targetValue =  $posData[$targetName];
			$sourcePlaceHolder = Translate::instance()->$sourceName;
			$targetPlaceHolder = $posData[$targetName] && !$targetValue ? $posData[$targetName] : Translate::instance()->$targetName;
			$sourceClassName = $sourceValue && str_replace(' ', '', strtolower($sourceValue))==str_replace(' ', '', strtolower($posData[$targetName])) ? 'success' : 'danger';
			$sourceClassName = $posData[$targetName] ? $sourceClassName  : null;

			if ($disabled[$sourceName]) {
				$sourceValue = $sourceValue ?: "<i>$sourcePlaceHolder</i>";
				$targetValue = $targetValue ?: "<i>$targetPlaceHolder</i>";
				$fields[$sourceName]['source'] = "<span id='$sourceName' class='input-data-placeholder $sourceClassName' >$sourceValue</span>";
				$fields[$sourceName]['target'] = "<span id='$targetName' class='input-data-placeholder' >$targetValue</span>";
				$fields[$sourceName]['transferer'] = $disabledTransferer;
			} else {
				
				$fields[$sourceName]['source'] = html::input(array(
					'id' => $sourceName,
					'value' => $sourceValue,
					'class' => $sourceClassName,
					'placeholder' => $sourcePlaceHolder,
					'readonly' => 'readonly'
				));
				
				$fields[$sourceName]['target'] = html::input(array(
					'id' => $targetName,
					'name' => $targetName,
					'class' => 'field-transfer',
					'value' => $targetValue,
					'placeholder' => $targetPlaceHolder
				));

				$fields[$sourceName]['transferer'] = $sourceValue ? $transferer : $disabledTransferer;
			}			


			// get places ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			$result = $model->query("
				SELECT place_id, place_name
				FROM places
				WHERE place_country = $country 
				ORDER BY place_name
			")->fetchAll();

			$dropdown = _array::extract($result); 


			// place :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			$sourceName = 'sap_imported_posaddress_city';
			$targetName = 'posaddress_place_id';
			$sourceValue = $data[$sourceName]; 
			$targetValue = $posData[$targetName];
			$sourcePlaceHolder = Translate::instance()->$sourceName;
			$targetPlaceHolder = Translate::instance()->$targetName;
			$sourceClassName = $sourceValue && str_replace(' ', '', strtolower($sourceValue))==str_replace(' ', '', strtolower($dropdown[$targetValue])) ? 'success' : 'danger';
			$sourceClassName = $posData[$targetName] ? $sourceClassName  : null;

			if ($disabled[$sourceName]) {

				if ($dropdown) {
					$array = $dropdown;
					$arrayKey = strtolower(str_replace(' ', '', $sourceValue));

					array_walk($array, function(&$key, $value) use (&$array) {
						$key = strtolower(str_replace(' ', '', $key));
					});
					
					$array = array_flip($array);
					$key = $array[$arrayKey];
				}

				$sourceValue = $sourceValue ?: "<i>$sourcePlaceHolder</i>";
				$targetValue = $dropdown[$key] ? $dropdown[$key] : "<i>$targetPlaceHolder</i>";
				$fields[$sourceName]['source'] = "<span id='$sourceName' class='input-data-placeholder  $sourceClassName' >$sourceValue</span>";
				$fields[$sourceName]['target'] = "<span id='$targetName' class='input-data-placeholder' >$targetValue</span>";
				$fields[$sourceName]['transferer'] = $disabledTransferer;

			} else {
				
				$fields[$sourceName]['source'] = html::input(array(
					'id' => $sourceName,
					'value' => $sourceValue,
					'class' => $sourceClassName,
					'placeholder' => $sourcePlaceHolder,
					'readonly' => 'readonly',
					'data-id' => $targetValue
				));
				
				$fields[$sourceName]['target'] = ui::dropdown($dropdown, array(
					'id' => $targetName,
					'name' => $targetName,
					'class' => 'field-transfer',
					'value' => $targetValue,
					'placeholder' => $targetPlaceHolder
				));

				$fields[$sourceName]['transferer'] = $disabledTransferer;
			}
	
			
			// phone :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			$sourceName = 'sap_imported_posaddress_phone';
			$targetName = 'posaddress_phone';
			$sourceValue = $data[$sourceName];
			$targetValue = $posData[$targetName];
			$sourcePlaceHolder = Translate::instance()->$sourceName;
			$targetPlaceHolder = $posData[$targetName] && !$targetValue ? $posData[$targetName] : Translate::instance()->$targetName;
			$sourceClassName = str_replace(' ', '', strtolower(preg_replace("/[^A-Za-z0-9 ]/", '', $sourceValue)))==str_replace(' ', '', strtolower(preg_replace("/[^A-Za-z0-9 ]/", '', $targetValue))) ? 'success' : 'danger';
			$sourceClassName = $posData[$targetName] ? $sourceClassName  : null;

			if ($disabled[$sourceName]) {
				$sourceValue = $sourceValue ?: "<i>$sourcePlaceHolder</i>";
				$targetValue = $targetValue ?: "<i>$targetPlaceHolder</i>";
				$fields[$sourceName]['source'] = "<span id='$sourceName' class='input-data-placeholder $sourceClassName' >$sourceValue</span>";
				$fields[$sourceName]['target'] = "<span id='$targetName' class='input-data-placeholder' >$targetValue</span>";
				$fields[$sourceName]['transferer'] = $disabledTransferer;
			} else {
				
				$fields[$sourceName]['source'] = html::input(array(
					'id' => $sourceName,
					'value' => $sourceValue,
					'class' => $sourceClassName,
					'placeholder' => $sourcePlaceHolder,
					'readonly' => 'readonly'
				));
				
				$fields[$sourceName]['target'] = html::input(array(
					'id' => $targetName,
					'name' => $targetName,
					'class' => 'field-transfer',
					'value' => $targetValue,
					'placeholder' => $targetPlaceHolder
				));

				$fields[$sourceName]['transferer'] = $sourceValue ? $transferer : $disabledTransferer;
			}
	
			
			

			// distribution channel ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			$distCodes = array();
			$dropdown = array();

			// get distribution channels
			$result = $model->query("
				SELECT 
					mps_distchannel_id AS id, 
					mps_distchannel_code AS code,
					CONCAT(mps_distchannel_group_name, ' - ', mps_distchannel_name, '-', mps_distchannel_code) as caption
				FROM mps_distchannels 
				INNER JOIN mps_distchannel_groups ON mps_distchannel_groups.mps_distchannel_group_id =mps_distchannels.mps_distchannel_group_id
				ORDER BY mps_distchannel_group_name, mps_distchannel_name, mps_distchannel_code
			")->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$dropdown[$row['id']] = $row['caption'];
					
					$code = str_replace(' ', '', $row['code']);
					$distCodes[$row['id']] = str_replace('x', '', $code);
				}
			}

			// sap distribution channel
			$sourceName = 'sap_imported_posaddress_distribution_channel';
			$sourceValue = str_replace(' ', '', $data[$sourceName]);
			$sourceValue = str_replace('x', '', $sourceValue);
			
			// mps distribution channel
			$targetName = 'posaddress_distribution_channel';
			$targetValue = str_replace(' ', '', strtolower($posData[$targetName]) );
			
			// placeholders
			$sourcePlaceHolder = Translate::instance()->$sourceName;
			$targetPlaceHolder = Translate::instance()->$targetName;

			$sourceClassName = $sourceValue && $sourceValue==$distCodes[$targetValue] ? 'success' : 'danger';
			$sourceClassName = $posData[$targetName] ? $sourceClassName  : null;

			if ($disabled[$sourceName]) {

				if ($dropdown) {
					
					$array = $dropdown;

					array_walk($array, function(&$key, $value) use (&$array) {
						$key = strtolower(str_replace(' ', '', $key));
					});
					
					$array = array_flip($array);
					$key = $array[$sourceValue];
				}

				$sourceValue = $sourceValue ?: "<i>$sourcePlaceHolder</i>";
				$targetValue = $dropdown[$key] ? $dropdown[$key] : "<i>$targetPlaceHolder</i>";
				$fields[$sourceName]['source'] = "<span id='$sourceName' class='input-data-placeholder $sourceClassName' >$sourceValue</span>";
				$fields[$sourceName]['target'] = "<span id='$targetName' class='input-data-placeholder' >$targetValue</span>";
				$fields[$sourceName]['transferer'] = $disabledTransferer;

			} else {
				
				$fields[$sourceName]['source'] = html::input(array(
					'id' => $sourceName,
					'value' => $sourceValue,
					'class' => $sourceClassName,
					'placeholder' => $sourcePlaceHolder,
					'readonly' => 'readonly'
				));
				
				$fields[$sourceName]['target'] = ui::dropdown($dropdown, array(
					'id' => $targetName,
					'name' => $targetName,
					'class' => 'field-transfer',
					'style' => 'width:440px;',
					'value' => $targetValue,
					'placeholder' => $targetPlaceHolder,
					'caption' => 'Select Distribution Channel'
				));

				$fields[$sourceName]['transferer'] = $disabledTransferer;
			}


			// store closing date ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			$sourceName = 'sap_imported_posaddress_deletion_indicator';
			$targetName = 'posaddress_store_closingdate';
			$sourceValue = $data[$sourceName];
			$targetValue = $posData[$targetName] <> '0000-00-00' ? $posData[$targetName] : null;;
			$sourcePlaceHolder = Translate::instance()->$sourceName;
			$targetPlaceHolder = $posData[$targetName] ? $posData[$targetName] : Translate::instance()->$targetName;
			$sourceClassName = str_replace(' ', '', strtolower($data[$sourceName]))==str_replace(' ', '', strtolower($posData[$targetName])) ? 'success' : 'danger';
			$sourceClassName = $posData[$targetName] ? $sourceClassName  : null;

			if ($disabled[$sourceName]) {
				$sourceValue = $sourceValue ?: "<i>$sourcePlaceHolder</i>";
				$targetValue = $targetValue ?: "<i>$targetPlaceHolder</i>";
				$fields[$sourceName]['source'] = "<span id='$sourceName' class='input-data-placeholder' >$sourceValue</span>";
				$fields[$sourceName]['target'] = "<span id='$targetName' class='input-data-placeholder' >$targetValue</span>";
				$fields[$sourceName]['transferer'] = $disabledTransferer;
			} else {
				
				$fields[$sourceName]['source'] = html::input(array(
					'id' => $sourceName,
					'value' => $sourceValue,
					'class' => $sourceClassName,
					'placeholder' => $sourcePlaceHolder,
					'readonly' => 'readonly'
				));
				
				$fields[$sourceName]['target'] =  "<span id='$targetName' class='input-data-placeholder' >$targetValue</span>";
				$fields[$sourceName]['transferer'] = $disabledTransferer;
			}
			

			// get provinces
			$result = $model->query("
				SELECT province_id, province_canton
				FROM provinces
				WHERE province_country = $country	
				ORDER BY province_canton
			")->fetchAll();
			
			if ($result) {
				$provinces = "<option value=''>Select</option>";
				foreach ($result as $row) {
					$provinces .= "<option value='{$row['province_id']}' >{$row['province_canton']}</option>";
				}
			}
			
			// template:: header
			$this->view->header('pagecontent')->node('header')->data('title', $header);
			
			$this->view->sapCountry('pagecontent')->pos('sap.import.data')
			->data('data', $data)
			->data('posData', $posData)
			->data('dataloader', $dataloader)
			->data('provinces', $provinces)
			->data('buttons', $buttons)
			->data('country', $country)
			->data('disabled', $disabled)
			->data('fields', $fields)
			->data('id', $pos->id)
			->data('import_id', $id);
		}
	}
	