<?php 

	class Assistances_Controller {
		
		public function __construct() {
				
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			

			Compiler::attach(array(
				'/public/scripts/jquery/jquery.ui.css',
				'/public/scripts/jquery/jquery.ui.js',
				'/public/css/jquery.ui.css',
				'/public/scripts/dropdown/dropdown.css',
				'/public/scripts/dropdown/dropdown.js',
				'/public/scripts/table.loader.js',
				'/public/js/assistances.js'
			));
			
			$link = $this->request->query('add');
			$this->request->field('add', $link);
			
			$link = $this->request->query('data');
			$this->request->field('form', $link);
		
			$this->view->assistances('pagecontent')->assistance('list');
		}
		
		public function add() {
			
			$id = url::param();
			
			if ($id) {
				$assistance = new Assistance();
				$data = $assistance->read($id);
			}
			
			// file manager
			Session::set('isLoggedIn', true);
			Session::set('moxiemanager.filesystem.rootpath', $_SERVER['DOCUMENT_ROOT']."/public/data/files/help");
			

			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
				
			// form dataloader
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			if ($id) {
				
				$this->view->pagetitle = 'Section';
				
				$hidden['assistance_section_content'] = true;
				$hidden['assistance_section_title'] = true;
				$hidden['assistance_section_published'] = true;
				
				// section mod
				$data['section'] = true;
				
				$integrity = new Integrity();
				$integrity->set($id, 'assistances');
					
				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/assistance.delete.php', array(
						'topic' => $id
					));
				}
			}
			
			$dataloader['assistance_application_id'] = Application::loader();
				
			// buttons
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
				
			$this->view->maintenance('pagecontent')
			->assistance('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('hidden', $hidden)
			->data('buttons', $buttons);
		}
		
		public function data() {
			
			$id = url::param();
			$section = url::param(1);
			
			$assistance = new Assistance();
			$assistance->read($id);
			
			if (!$assistance->id) {
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			$model = new Model(Connector::DB_CORE);
			
			// file manager
			Session::set('isLoggedIn', true);
			Session::set('moxiemanager.filesystem.rootpath', $_SERVER['DOCUMENT_ROOT']."/public/data/files/help");
			
			// set active sections
			session::set('assistance.bundle.active', $id);
			session::set('assistance.application.active', $assistance->application_id);
			
			// section
			if ($section) {
				$data = $assistance->section()->read($section);
			}
			
			// dataloader
			$data['assistance_id'] = $id;
			$data['assistance_title'] = $assistance->title;
			$data['assistance_application_id'] = $assistance->application_id;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;

			
			if ($id && $section) {
				
				$integrity = new Integrity();
				$integrity->set($section, 'assistance_sections');
				
				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/assistance.delete.php', array(
						'topic' => $id,
						'section' => $section
					));
				}
				
				// hidde copy links field
				$hidden['assistance_section_copy_links'] = true;
				
			} else {
				
				$links = $model->query("
					SELECT COUNT(assistance_section_link_link) AS total
					FROM assistances
					INNER JOIN assistance_sections ON assistance_id = assistance_section_assistance_id
					INNER JOIN assistance_section_links ON assistance_section_id = assistance_section_link_section_id
					WHERE assistance_id = $assistance->id
				")->fetch();
				
				if ($links['total']>0) {
					$data['assistance_section_copy_links'] = 1;
				} else {
					$hidden['assistance_section_copy_links'] = true;
				}
			}
			
			if (!$assistance->application_id) {
				$hidden['assistance_application_id'] = true;
			}
			
			$disabled['assistance_title'] = true;
			$disabled['assistance_application_id'] = true;
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $assistance->title)
			->data('subtitle', $assistance->section()->title);
			
			// dataloader: applications
			$dataloader['assistance_application_id'] = Application::loader();
			
			// template: navigation tabs
			$this->view->tabs('pagecontent')->navigation('tab')->data('queryParam', $section);
				
			$this->view->assistance('pagecontent')
			->assistance('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons);
		}
		
		public function links() {
			
			$id = url::param();
			$section = url::param(1);
			
			$assistance = new Assistance();
			$assistance->read($id);
			$assistance->section()->read($section);
			
			
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
				
			// form dataloader
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
				
			// buttons
			$buttons['back'] = ui::button(array(
				'id' => 'back',
				'href' => $this->request->query(),
				'icon' => 'back',
				'caption' => translate::instance()->back
			));
			
			$this->request->field('id', $id);
			$this->request->field('section', $section);
			$this->request->field('add', true);
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $assistance->title)
			->data('subtitle', $assistance->section()->title);
			
			// template: navigation tabs
			$this->view->tabs('pagecontent')->navigation('tab')->data('queryParam', $section);
				
			$this->view->assistanceURL('pagecontent')
			->assistance('links')
			->data('data', $data)
			->data('buttons', $buttons);
		}
		
	}