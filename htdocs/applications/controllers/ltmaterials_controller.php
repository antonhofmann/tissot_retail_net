<?php 

	class LTmaterials_Controller {
		
		public function __construct() {
			
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			// request
			$requestFields = array();
			$requestFields['form'] = $this->request->query('data');
			$requestFields['export'] = $this->request->link('/applications/exports/excel/material.ltm.php');
			$requestFields['filter'] = 'ltm';
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			
			// permission: can edit MPS material
			$can_edit = user::permission(Material::PERMISSION_LTM_EDIT);
			
			if (!$this->request->archived && $can_edit) {
				$requestFields['add'] = $this->request->query('add');
			}

			// template: material list
			$this->view->materials('pagecontent')
			->material('list')
			->data('requestFields', $requestFields);
		}
		
		public function add() {
			
			// permissions
			$permission_edit = user::permission(Material::PERMISSION_LTM_EDIT);
		
			if($this->request->archived || !$permission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// form datalaoder
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			$data['mps_material_islongterm'] = true;
			$data['mps_material_material_planning_type_id'] = 4;
			$data['mps_material_active'] = 1;
				
			// buttons
			$buttons = array();
			$buttons['back'] = '/'.$this->request->application.'/'.$this->request->controller;
			$buttons['save'] = true;
			
			// dataloader collections
			$dataloader['mps_material_material_collection_id'] = Material_Collection::loader($this->request->application, array(
				'mps_material_collection_active=1'
			));
			
			// dataloader : collection categories
			$dataloader['mps_material_material_collection_category_id'] = Material_Collection_Category::loader($this->request->application, array(
				'mps_material_collection_category_active=1'
			));
			
			// dataloader : planning types
			$dataloader['mps_material_material_planning_type_id'] = Material_Planning_Type::loader($this->request->application);
			
			// dataloader : material categories
			$dataloader['mps_material_material_category_id'] = Material_Category::loader($this->request->application, array(
				'mps_material_category_active=1'
			));
			
			// dataloader : purposes
			$dataloader['mps_material_material_purpose_id'] = Material_Purpose::loader($this->request->application, array(
				'mps_material_purpose_active=1'
			));
		
			// dataloader: currencies
			$dataloader['mps_material_currency_id'] = Currency::loader(array(
				'currency_id=1'
			));

			// template: material form
			$this->view->materialForm('pagecontent')
			->material('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('longterm', true);
		}
		
		public function data() {
				
			$id = url::param();
			
			// permissions
			$permission_edit = user::permission(Material::PERMISSION_LTM_EDIT);
			
			$material = new Material();
			$material->read($id);
			
			// check access to material
			if(!$material->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			} 
			// if material is archived, go to material archive
			elseif (!$this->request->archived && !$material->active) {
				url::redirect($this->request->query('archived/data'));
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// hide bulk operations tab for not permitted users or for archived materials
			if (!$permission_edit || $this->request->archived) {
				$this->request->exclude($this->request->query('bulk'));
			}
			
			// form dataloader
			$data = $material->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['mps_material_islongterm'] = true;
			$data['mps_material_material_planning_type_id'] = 4;
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// button: save
			if ($permission_edit) {
				$buttons['save'] = true;
			}
			
			if (!$this->request->archived && $permission_edit) {
				
				$integrity = new Integrity();
				$integrity->set($id, 'mps_materials');
				
				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/material.delete.php', array('id'=> $id));
				}
			}
			elseif ($data) {
				
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
				
				$disabled['mps_material_active'] = false;
			}
			
			// dataloader collections
			$dataloader['mps_material_material_collection_id'] = Material_Collection::loader($this->request->application, array(
				'mps_material_collection_active=1'
			));
			
			// dataloader : collection categories
			$dataloader['mps_material_material_collection_category_id'] = Material_Collection_Category::loader($this->request->application, array(
				'mps_material_collection_category_active=1'
			));
			
			// dataloader : planning types
			$dataloader['mps_material_material_planning_type_id'] = Material_Planning_Type::loader($this->request->application);
			
			// dataloader : material categories
			$dataloader['mps_material_material_category_id'] = Material_Category::loader($this->request->application, array(
				'mps_material_category_active=1'
			));
			
			// dataloader : purposes
			$dataloader['mps_material_material_purpose_id'] = Material_Purpose::loader($this->request->application, array(
				'mps_material_purpose_active=1'
			));
		
			// dataloader: currencies
			$dataloader['mps_material_currency_id'] = Currency::loader(array(
				'currency_id=1'
			));
			
			// template: material header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $material->header());
			
			// template: navigation tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			// template: material form
			$this->view->materialForm('pagecontent')
			->material('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function standard() {
			
			$id = url::param();
			
			// permissions
			$permission_edit = user::permission(Material::PERMISSION_LTM_EDIT);
				
			$material = new Material();
			$material->read($id);
				
			// check access to material
			if(!$material->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			// if material is archived, go to material archive
			elseif (!$this->request->archived && !$material->active) {
				url::redirect($this->request->query('archived/standard'));
			}
			
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.css");
			Compiler::attach(DIR_SCRIPTS."jquery/jquery.ui.js");
			Compiler::attach(DIR_CSS."jquery.ui.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				
			// hide bulk operations tab for not permitted users or for archived materials
			if (!$permission_edit || $this->request->archived) {
				$this->request->exclude($this->request->query('bulk'));
			}
			
			// form dataloader
			$data = $material->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
				
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			// db application model
			$model = new Model($this->request->application);
			
			
			// get all selected countires
			$result = $model->query("
				SELECT mps_material_address_address_id
				FROM mps_material_addresses
				WHERE mps_material_address_material_id = $id		
			")->fetchAll();
				
			if ($result) {
				
				$companies= array();
				
				foreach ($result as $row) {
					$companies[] = $row['mps_material_address_address_id'];
				}
				
				$data['companies'] = serialize($companies);
			}
			
			
			// get all countries involved in planning
			$result = $model->query("
				SELECT DISTINCT	
					address_id,
					CONCAT(country_name,', ', address_company) as name 
				FROM db_retailnet.addresses
			")
			->bind(Company::DB_BIND_COUNTRIES)
			->filter('planning', 'address_active = 1 AND address_involved_in_planning = 1')
			->order('country_name, address_company')
			->fetchAll();
			
			// dataloader: countries
			$dataloader['companies'] = _array::extract($result);
			
			
			
			// company languages
			if ($dataloader['companies']) { 
				
				$data['selectors'] = ui::button(array(
					'data' => 'all',
					'class' => 'selector all',
					'label' => translate::instance()->select_all
				));
				
				$result = $model->query("
					SELECT DISTINCT
						language_id AS id,
						UPPER(language_iso639_1) AS language,
						language_name
					FROM db_retailnet.address_languages
					INNER JOIN db_retailnet.languages ON address_language_language = language_id
					INNER JOIN db_retailnet.addresses ON address_id = address_language_address
					WHERE address_active = 1 AND address_involved_in_planning = 1
					ORDER BY language
				")->fetchAll();
				
				if ($result) {
					
					$total = count($result);
					
					for ($i=0; $i < $total; $i++) {
						
						if ($i==0) $class='left';
						else $class = ($i==$total-1) ? 'right' : 'middle';
						
						$data['selectors'] .= ui::button(array(
							'data' => $result[$i]['id'],
							'label' => $result[$i]['language'],
							'class' => "selector $class",
							'title' => $result[$i]['language_name'],
							'section' => 'companies.language'
						));
					}
				}
				
				$data['selectors'] .= ui::button(array(
					'data' => '1',
					'class' => 'selector left sh',
					'label' => 'SH',
					'title' => 'Southern Hemisphere',
					'section' => 'companies.hemisphere'
				));
				
				$data['selectors'] .= ui::button(array(
					'data' => '5',
					'class' => 'selector middle mi',
					'label' => 'ME',
					'title' => 'Middle East',
					'section' => 'companies.regions'
				));
				
				$data['selectors'] .= ui::button(array(
					'data' => 'internal',
					'class' => 'selector right internal',
					'label' => 'INTERNAL',
					'title' => 'Internal Departments',
					'section' => 'companies.intern'
				));
				
			}
			
			
			// form editabled
			if (!$this->request->archived && $permission_edit) {
				$buttons['save'] = true;
			} else {
				foreach ($data as $field => $value) {
					$disabled[$field] = true;
				}
				$disabled['companies'] = true;
			}
			
			// template: material header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $material->header());
				
			// template: navigation tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
				
			// template: material form
			$this->view->materialForm('pagecontent')
			->material('standard')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function bulk() {
				
			$id = url::param();
			
			// permissions
			$permission_edit = user::permission(Material::PERMISSION_LTM_EDIT);
			
			$material = new Material();
			$material->read($id);
			
			// check access to material
			if(!$material->id || $this->request->archived || !$permission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			
			// database model
			$model = new Model($this->request->application);
			
			// form dataloader
			$data = $material->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['mps_material_islongterm'] = true;
			$data['mps_material_material_planning_type_id'] = 4;
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['update'] = true;

			
			// dataloader: acions
			$actions = dataloader::material('bulk');
			$dataloader['action'] = _array::extract($actions);

			
			// template: material header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $material->code.', '.$material->name);
				
			// template: navigation tabs
			$this->view->tabs('pagecontent')
			->navigation('tab');
			
			// template: material bulk form
			$this->view->materialBulkForm('pagecontent')
			->material('bulk.form')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}