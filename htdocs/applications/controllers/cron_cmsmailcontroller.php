<?php

/**
 * This class handles sending cms mails for cronjob
 * Sends cost monitoring sheet notices and reminders to logistics coordinators
 * and clients.
 */
class Cron_CMSMailController extends Cron_AbstractMailController {
	/**
	 * @const integer
	 */
	const FIRST_REMINDER_AFTER_MONTHS = 2;

	/**
	 * @const integer
	 */
	const SECOND_REMINDER_AFTER_MONTHS = 3;

	/**
	 * @const integer
	 */
	const THIRD_REMINDER_AFTER_MONTHS = 4;

	/**
	 * @var Project_Mails_Datamapper
	 */
	protected $ProjectMapper;

	/**
	 * Setup controller
	 */
	public function __construct() {
		$this->ProjectMapper = new Project_Mails_Datamapper();
	}

	/**
	 * Run all tasks
	 * @return void
	 */
	public function run() {
		// update cms due date on completed projects
		$this->ProjectMapper->updateCMSDueDates();

		$OrderModel = new OrderModel;
		//$this->sendMailsToLogisticsCoordinators($OrderModel);
		//$this->sendFirstReminderToLogisticsCoordinators($OrderModel);
		//$this->sendSecondReminderToLogisticsCoordinators($OrderModel);
		//$this->sendThirdReminderToLogisticsCoordinators($OrderModel);
		$this->sendMailsToProjectOwners($OrderModel);
		$this->sendFirstReminderToProjectOwners($OrderModel);
		$this->sendSecondReminderToProjectOwners($OrderModel);
		$this->sendThirdReminderToProjectOwners($OrderModel);
	}

	/**
	 * Send mails to logistics coordinators for cms completion
	 * @param  OrderModel $OrderModel
	 * @return void
	 */
	public function sendMailsToLogisticsCoordinators(OrderModel $OrderModel) {
		
		// get non corporate projects with incomplete cms
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMS(
			Project_Mails_Datamapper::PROJECT_OPENS_IN_FUTURE, 'noncorporate'
		);

		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.call.to.logistics.for.cms.completion_ncp');

			// has the mail already been sent? if so, skip this project
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_retail_operator'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				
				$Mail->setParam('recipient', $project['order_retail_operator']);
				$Mail->setParam('recipientRole', Role::LOGISTICS_COORDINATOR);
				$Mail->setParam('projectID', $project['project_id']);

				$Mail->send();

				$sender = $Mail->getSender();

				$orderRetailOperator = $Mail->getRecipient($project['order_retail_operator']);

				// create a task for recipient
				if ($orderRetailOperator->id) {
					$this->createTask(array(
						'task_order_state' => 88,
						'task_order'       => $project['project_order'],
						'task_user'        => $project['order_retail_operator'],
						'task_from_user'   => $sender->id,
						'task_text'        => $orderRetailOperator->getContent(),
						'task_url'         => URLBuilder::projectTaskCenter($project['project_id']),
						'task_due_date'    => $project['project_cost_cms_completion_due_date'],
						'date_created'		 => date('Y-m-d H:i:s'),
					));
				}
			}
		}

		// get  corporate projects with incomplete cms
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMS(
			Project_Mails_Datamapper::PROJECT_OPENS_IN_FUTURE, 'corporate'
		);

		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.call.to.logistics.for.cms.completion_cp');

			// has the mail already been sent? if so, skip this project
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_retail_operator'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				
				$Mail->setParam('recipient', $project['order_retail_operator']);
				$Mail->setParam('recipientRole', Role::LOGISTICS_COORDINATOR);
				$Mail->setParam('projectID', $project['project_id']);

				$Mail->send();

				$sender = $Mail->getSender();

				$orderRetailOperator = $Mail->getRecipient($project['order_retail_operator']);

				// create a task for recipient
				if ($orderRetailOperator->id) {
					$this->createTask(array(
						'task_order_state' => 88,
						'task_order'       => $project['project_order'],
						'task_user'        => $project['order_retail_operator'],
						'task_from_user'   => $sender->id,
						'task_text'        => $orderRetailOperator->getContent(),
						'task_url'         => URLBuilder::projectTaskCenter($project['project_id']),
						'task_due_date'    => $project['project_cost_cms_completion_due_date'],
						'date_created'		 => date('Y-m-d H:i:s'),
					));
				}
			}
		}
	}

	/**
	 * Send first reminder to logistics coordinators for cms completion
	 * @param  OrderModel $OrderModel
	 * @return void
	 */
	public function sendFirstReminderToLogisticsCoordinators(OrderModel $OrderModel) {
		
		//get non corporate projects
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMS(self::FIRST_REMINDER_AFTER_MONTHS, 'noncorporate');
		
		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.first.reminder.logistics.for.cms.completion_ncp');

			// has the mail already been sent? if not -> send it
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_retail_operator'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				$Mail->setParam('recipient', $project['order_retail_operator']);
				$Mail->setParam('recipientRole', Role::LOGISTICS_COORDINATOR);
				$Mail->setParam('projectID', $project['project_id']);

				$Mail->send();

			}	
		}

		//get  corporate projects
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMS(self::FIRST_REMINDER_AFTER_MONTHS, 'corporate');
		
		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.first.reminder.logistics.for.cms.completion_cp');

			// has the mail already been sent? if not -> send it
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_retail_operator'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				
				$Mail->setParam('recipient', $project['order_retail_operator']);
				$Mail->setParam('recipientRole', Role::LOGISTICS_COORDINATOR);
				$Mail->setParam('projectID', $project['project_id']);

				$Mail->send();
			}	
		}
	}

	/**
	 * Send second reminder to logistics coordinators for cms completion
	 * @param  OrderModel $OrderModel
	 * @return void
	 */
	public function sendSecondReminderToLogisticsCoordinators(OrderModel $OrderModel) {
		
		//get non corporate projects
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMS(self::SECOND_REMINDER_AFTER_MONTHS, 'noncorporate');
		
		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.second.reminder.logistics.for.cms.completion_ncp');

			// has the mail already been sent? if not -> send it
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_retail_operator'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {

				$Mail->setParam('recipient', $project['order_retail_operator']);
				$Mail->setParam('recipientRole', Role::LOGISTICS_COORDINATOR);
				$Mail->setParam('projectID', $project['project_id']);

				$Mail->send();
			}
		}

		//get  corporate projects
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMS(self::SECOND_REMINDER_AFTER_MONTHS, 'corporate');
		
		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.second.reminder.logistics.for.cms.completion_cp');

			// has the mail already been sent? if not -> send it
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_retail_operator'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {

				$Mail->setParam('recipient', $project['order_retail_operator']);
				$Mail->setParam('recipientRole', Role::LOGISTICS_COORDINATOR);
				$Mail->setParam('projectID', $project['project_id']);

				$Mail->send();
			}
		}
	}

	/**
	 * Send third reminder to logistics coordinators for cms completion
	 * @param  OrderModel $OrderModel
	 * @return void
	 */
	public function sendThirdReminderToLogisticsCoordinators(OrderModel $OrderModel) {
		
		//get non corporate projects
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMS(self::SECOND_REMINDER_AFTER_MONTHS, 'noncorporate');

		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMS(
			self::THIRD_REMINDER_AFTER_MONTHS, 'noncorporate'
		);
		
		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.third.reminder.logistics.for.cms.completion_ncp');

			// has the mail already been sent? if not -> send it
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_retail_operator'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				
				$Mail->setParam('recipient', $project['order_retail_operator']);
				$Mail->setParam('recipientRole', Role::LOGISTICS_COORDINATOR);
				$Mail->setParam('projectID', $project['project_id']);

				$Mail->send();
			}	
		}

		//get  corporate projects
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMS(
			self::THIRD_REMINDER_AFTER_MONTHS, 'corporate'
		);
		
		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.third.reminder.logistics.for.cms.completion_cp');

			// has the mail already been sent? if not -> send it
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_retail_operator'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				
				$Mail->setParam('recipient', $project['order_retail_operator']);
				$Mail->setParam('recipientRole', Role::LOGISTICS_COORDINATOR);
				$Mail->setParam('projectID', $project['project_id']);

				$Mail->send();
			}	
		}
	}

	/**
	 * Send mails to project owners for cms completion
	 * @param  OrderModel $OrderModel
	 * @return void
	 */	
	public function sendMailsToProjectOwners(OrderModel $OrderModel) {
		
		//get non corporate projects
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMSForProductOwners(
			Project_Mails_Datamapper::PROJECT_OPENS_IN_FUTURE, 'noncorporate'
		);

		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.call.to.client.for.cms.completion_ncp');
			
			// has the mail already been sent? if not -> send it
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_user'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				
				$Mail->setParam('recipient', $project['order_user']);
				$Mail->setParam('recipientRole', Role::STANDARD_USER);
				$Mail->setParam('projectID', $project['project_id']);
				
				$Mail->send();

				$sender = $Mail->getSender();

				$orderUser = $Mail->getRecipient($project['order_user']);

				// create a task for recipient
				if ($orderUser->id) {
					$this->createTask(array(
						'task_order_state' => 89,
						'task_order'       => $project['project_order'],
						'task_user'        => $project['order_user'],
						'task_from_user'   => $sender->id,
						'task_text'        => $orderUser->getContent(),
						'task_url'         => URLBuilder::projectTaskCenter($project['project_id']),
						'task_due_date'    => $project['project_cost_cms_completion_due_date'],
						'date_created'		 => date('Y-m-d H:i:s'),
					));
				}
			}
		}

		//get  corporate projects
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMSForProductOwners(
			Project_Mails_Datamapper::PROJECT_OPENS_IN_FUTURE, 'corporate'
		);

		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.call.to.client.for.cms.completion_cp');
			
			// has the mail already been sent? if not -> send it
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_user'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				
				$Mail->setParam('recipient', $project['order_user']);
				$Mail->setParam('recipientRole', Role::STANDARD_USER);
				$Mail->setParam('projectID', $project['project_id']);
				
				$Mail->send();

				$sender = $Mail->getSender();

				$orderUser = $Mail->getRecipient($project['order_user']);

				// create a task for recipient
				if ($orderUser->id) {
					$this->createTask(array(
						'task_order_state' => 89,
						'task_order'       => $project['project_order'],
						'task_user'        => $project['order_user'],
						'task_from_user'   => $sender->id,
						'task_text'        => $orderUser->getContent(),
						'task_url'         => URLBuilder::projectTaskCenter($project['project_id']),
						'task_due_date'    => $project['project_cost_cms_completion_due_date'],
						'date_created'		 => date('Y-m-d H:i:s'),
					));
				}
			}
		}
	}

	/**
	 * Send first reminder to project owners for cms completion
	 * @param  OrderModel $OrderModel
	 * @return void
	 */
	public function sendFirstReminderToProjectOwners(OrderModel $OrderModel) {
		
		//get non corporate projects
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMSForProductOwners(
			self::FIRST_REMINDER_AFTER_MONTHS, 'noncorporate'
		);
		
		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.first.reminder.client.for.cms.completion_ncp');

			// has the mail already been sent? if not -> send it
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_user'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				
				$Mail->setParam('recipient', $project['order_user']);
				$Mail->setParam('recipientRole', Role::STANDARD_USER);
				$Mail->setParam('projectID', $project['project_id']);

				$Mail->send();
			}	
		}

		//get  corporate projects
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMSForProductOwners(
			self::FIRST_REMINDER_AFTER_MONTHS, 'corporate'
		);
		
		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.first.reminder.client.for.cms.completion_cp');

			// has the mail already been sent? if not -> send it
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_user'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				
				$Mail->setParam('recipient', $project['order_user']);
				$Mail->setParam('recipientRole', Role::STANDARD_USER);
				$Mail->setParam('projectID', $project['project_id']);

				$Mail->send();
			}	
		}
	}

	/**
	 * Send second reminder to project owners for cms completion
	 * @param  OrderModel $OrderModel
	 * @return void
	 */
	public function sendSecondReminderToProjectOwners(OrderModel $OrderModel) {
		
		//get non corporate projects
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMSForProductOwners(
			self::SECOND_REMINDER_AFTER_MONTHS, 'noncorporate'
		);

		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.second.reminder.client.for.cms.completion_ncp');

			// has the mail already been sent? if not -> send it
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_user'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				
				$Mail->setParam('recipient', $project['order_user']);
				$Mail->setParam('recipientRole', Role::STANDARD_USER);
				$Mail->setParam('projectID', $project['project_id']);

				$Mail->send();
			}
		}

		//get  corporate projects
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMSForProductOwners(
			self::SECOND_REMINDER_AFTER_MONTHS, 'corporate'
		);

		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.second.reminder.client.for.cms.completion_cp');

			// has the mail already been sent? if not -> send it
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_user'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				
				$Mail->setParam('recipient', $project['order_user']);
				$Mail->setParam('recipientRole', Role::STANDARD_USER);
				$Mail->setParam('projectID', $project['project_id']);

				$Mail->send();
			}
		}
	}

	/**
	 * Send third reminder to project owners for cms completion
	 * @param  OrderModel $OrderModel
	 * @return void
	 */
	public function sendThirdReminderToProjectOwners(OrderModel $OrderModel) {
		
		//get non corporate projects
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMSForProductOwners(
			self::THIRD_REMINDER_AFTER_MONTHS, 'noncorporate'
		);

		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.third.reminder.client.for.cms.completion_ncp');

			// has the mail already been sent? if not -> send it
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_user'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				
				$Mail->setParam('recipient', $project['order_user']);
				$Mail->setParam('recipientRole', Role::STANDARD_USER);
				$Mail->setParam('projectID', $project['project_id']);

				$Mail->send();
			}	
		}

		//get  corporate projects
		$projects = $this->ProjectMapper->getCompletedProjectsWithIncompleteCMSForProductOwners(
			self::THIRD_REMINDER_AFTER_MONTHS, 'corporate'
		);

		foreach ($projects as $project) {
			
			$Mail = $this->getMailFactory()->buildActionMail('project.third.reminder.client.for.cms.completion_cp');

			// has the mail already been sent? if not -> send it
			$isSent = $OrderModel->isOrderMailSent(
				$project['project_order'], 
				$project['order_user'],
				$Mail->getTemplateId()
			);

			if ($isSent === false) {
				
				$Mail->setParam('recipient', $project['order_user']);
				$Mail->setParam('recipientRole', Role::STANDARD_USER);
				$Mail->setParam('projectID', $project['project_id']);

				$Mail->send();
			}	
		}
	}

	/**
	 * Create a task for a user
	 * @param  array $task  Array of task data
	 * @return void
	 */
	protected function createTask($task) {
		$TaskModel = new Task_Model();
		$TaskModel->create($task);
	}
}