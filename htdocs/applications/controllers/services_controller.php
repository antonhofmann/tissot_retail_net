<?php 

	class Services_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() { 
			

			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			if ($permission_edit) {
				$this->request->field('add',$this->request->query('add'));
			}
			
			if ($permission_edit || $permission_view) {
				$this->request->field('data',$this->request->query('data'));
			}
			
			// template: role list
			$this->view->productLines('pagecontent')
			->item('services')
			->data('buttons', $buttons);
		}

		public function add() {

			$permission_edit = user::permission('can_edit_catalog');
		
			if($this->request->archived || !$permission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			$data['item_type'] = 7;
			$data['item_active'] = 1;
				
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();

			$model = new Model(Connector::DB_CORE);

			// item categories
			$result = $model->query("
				SELECT item_category_id, item_category_name 
				FROM item_categories
				ORDER BY item_category_name
			")->fetchAll();

			$dataloader['item_category'] = _array::extract($result); 

			// item cost group
			$result = $model->query("
				SELECT project_cost_groupname_id, project_cost_groupname_name 
				FROM project_cost_groupnames
				WHERE  project_cost_groupname_active = 1 
				ORDER BY project_cost_groupname_name
			")->fetchAll();

			$dataloader['item_cost_group'] = _array::extract($result); 			

			// item regions
			$result = $model->query("
				SELECT region_id, region_name 
				FROM regions 
				ORDER BY region_name
			")->fetchAll();	

			$dataloader['regions'] = _array::extract($result);

			// item available in procject types
			$result = $model->query("
				SELECT postype_id, postype_name 
				FROM postypes 
				ORDER BY postype_name
			")->fetchAll();	

			$dataloader['item_available_in_project_types'] = _array::extract($result);
		
			// item available in product lines
			$result = $model->query("
				SELECT product_line_id, product_line_name 
				FROM product_lines 
				WHERE product_line_budget = 1 
				ORDER BY product_line_name
			")->fetchAll();	

			$dataloader['item_available_in_product_lines'] = _array::extract($result);

			$hidden['item_visible'] = true;
			$hidden['item_price_adjustable'] = true;
		
			// template: application form
			$this->view->productLine('pagecontent')
			->item('service.form')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('hidden', $hidden)
			->data('buttons', $buttons);
		}
		
		public function data() {

			$id = url::param();

			$item = new Item();
			$item->read($id);

			if (!$item->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			$data = $item->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['item_type'] = 7;

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			if ($permission_edit) {
			
				$buttons['save'] = true;

				$integrity = new Integrity();
				$integrity->set($id, 'items', 'system');

				if ($item->id && $integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/helpers/item.delete.php', array('id'=>$id));
				}
			}
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}

			$model = new Model(Connector::DB_CORE);

			// item categories
			$result = $model->query("
				SELECT item_category_id, item_category_name 
				FROM item_categories
				ORDER BY item_category_name
			")->fetchAll();

			$dataloader['item_category'] = _array::extract($result); 

			// item cost group
			$result = $model->query("
				SELECT project_cost_groupname_id, project_cost_groupname_name 
				FROM project_cost_groupnames
				WHERE  project_cost_groupname_active = 1 
				ORDER BY project_cost_groupname_name
			")->fetchAll();

			$dataloader['item_cost_group'] = _array::extract($result); 			

			// item available in procject types
			$result = $model->query("
				SELECT postype_id, postype_name 
				FROM postypes 
				ORDER BY postype_name
			")->fetchAll();	

			$projectTypes = _array::extract($result);
		

			// item available in product lines
			$result = $model->query("
				SELECT product_line_id, product_line_name 
				FROM product_lines 
				WHERE product_line_budget = 1 
				ORDER BY product_line_name
			")->fetchAll();	

			$productLines = _array::extract($result);

			// get selected data
			$result = $model->query("
				SELECT 
					item_pos_type_pos_type,
					item_pos_type_product_line
				FROM item_pos_types
				WHERE item_pos_type_item = $id
			")->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$type = $row['item_pos_type_pos_type'];
					$line = $row['item_pos_type_product_line'];
					$selected[$line][$type] = true;
				}
			}

			if ($productLines && $projectTypes) {
				
				foreach ($productLines as $line => $lineName) {
					
					$datagrid[$line]['productline'] = $lineName;

					foreach ($projectTypes as $type => $typeName) {

						$columns["col_$type"] = $typeName;

						$checked = $selected[$line][$type] ? "checked=checked": null;
						
						if ($permission_edit) {
							$datagrid[$line]["col_$type"] = "<input type='checkbox' name='project_type_product_line[$line-$type]' value='1' $checked >";
						} elseif($checked) {
							$datagrid[$line]["col_$type"] = "<i class='fa fa-check-circle' ></i>";
						}
						
					}
				}
			}


			// data: available in project types
			// data: available in product lines
			$result = $model->query("
				SELECT * 
				FROM item_pos_types 
				WHERE item_pos_type_item = $item->id
			")->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$postypes[] = $row['item_pos_type_pos_type'];
					$productlines[] = $row['item_pos_type_product_line'];
				}
				$data['item_available_in_project_types'] = serialize($postypes);
				$data['item_available_in_product_lines'] = serialize($productlines);
			}


			$table = new Table(array('class' => 'listing'));
			$table->datagrid = $datagrid;
			
			$table->productline();
			$table->caption('productline', '<font color=black>Product Line / Project Type</font>');	

			if ($columns) {
				foreach ($columns as $key => $caption) {
					$table->$key('width=15%', 'align=center');
					$table->caption($key, '<font color=black>' . $caption . '</font>');
				}
			}

			$data['item_projecttype_productline'] = $table->render();

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
			$this->view->companytabs('pagecontent')->navigation('tab');


			$hidden['item_visible'] = true;
			$hidden['item_price_adjustable'] = true;
		
			// template: form
			$this->view->productLine('pagecontent')
			->item('service.form')
			->data('dataloader', $dataloader)
			->data('data', $data)
			->data('hidden', $hidden)
			->data('disabled', $disabled)
			->data('buttons', $buttons);
		}		



		public function suppliers() {

			$id = url::param();
			$param = url::param(1);

			$item = new Item();
			$item->read($id);

			if (!$item->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			$permission_edit = user::permission('can_edit_catalog');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			
			// db model
			$model = new Model(Connector::DB_CORE);

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
			$this->view->companytabs('pagecontent')->navigation('tab');

			// buttons
			$buttons = array();

			if ($param) {

				$param = (is_numeric($param)) ? $param : null;

				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				Compiler::attach(DIR_JS."form.validator.js");

				$buttons['back'] = $this->request->query("suppliers/$id");

				// supplier data
				$supplier = new Supplier();
				$supplier->read($param);

				$data = $supplier->data;
				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;
				$data['action'] = $this->request->action;
				$data['supplier_item'] = $id;
				$data['redirect'] = $this->request->query("suppliers/$id");

				if ($permission_edit) {
				
					$buttons['save'] = true;

					$integrity = new Integrity();
					$integrity->set($supplier->id, 'suppliers', 'system');

					if ($supplier->id && $integrity->check()) {
						$buttons['delete'] = $this->request->link('/applications/helpers/item.supplier.delete.php', array('id'=>$param));
					}
				}
				elseif($data) {
					foreach ($data as $key => $value) {
						$disabled[$key] = true;
					}
				}

				// dataloader suppliers
				$result = $model->query("
					SELECT address_id, address_company 
					FROM addresses left join address_types on address_type = address_type_id 
					WHERE address_active = 1 AND address_type_code IN('DECO', 'SUPP') 
					ORDER BY address_company
				")->fetchAll();	

				$dataloader['supplier_address'] = _array::extract($result);
				
				// dataloader currencies
				$result = $model->query("
					SELECT currency_id, CONCAT(currency_symbol, ', ', currency_name) as currency
					FROM currencies 
					ORDER BY currency_symbol
				")->fetchAll();	

				$dataloader['supplier_item_currency'] = _array::extract($result);

				// template: form
				$this->view->productLine('pagecontent')
				->item('supplier.form')
				->data('dataloader', $dataloader)
				->data('data', $data)
				->data('hidden', $hidden)
				->data('disabled', $disabled)
				->data('buttons', $buttons);
			}
			else {

				$buttons['back'] = $this->request->query();

				if ($permission_edit) {
					$buttons['add'] = $this->request->query("suppliers/$id/add");
				}

				$result = $model->query("
					SELECT 
						supplier_id,
						supplier_item_code,
						supplier_item_name,
						supplier_item_price,
						currency_symbol,
						address_company
					FROM suppliers 
					INNER JOIN addresses ON supplier_address = address_id
					INNER JOIN currencies ON currency_id = supplier_item_currency
					WHERE supplier_item = $id
					ORDER BY supplier_item_code
				")->fetchAll();

				$datagrid = _array::datagrid($result);
			
				// template: form
				$this->view->productLine('pagecontent')
				->item('suppliers')
				->data('datagrid', $datagrid)
				->data('buttons', $buttons)
				->data('link', $this->request->query("suppliers/$id"));
			}

		}

		public function regions() {

			$id = url::param();

			$item = new Item();
			$item->read($id);

			$permission_edit = user::permission('can_edit_catalog');
			$permission_edit_items = user::permission('can_edit_items_in_catalogue');
			$permission_view = user::permission('can_browse_catalog_in_admin');
			$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

			if (!$item->id || (!$_CAN_EDIT && !$permission_view)) {
				Message::access_denied();
				url::redirect($this->request->query());
			}

			Compiler::attach(DIR_JS."item.regions.js");
			
			$data = $item->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();

			$model = new Model(Connector::DB_CORE);

			// get countries
			$result = $model->query("
				SELECT DISTINCT
					country_id,
					region_id,
					country_name,
					region_name
				FROM countries
				INNER JOIN regions ON region_id = country_region
				ORDER BY region_name, country_name
			")->fetchAll();	

			$datagrid = array();

			if ($result) {
				foreach ($result as $row) {
					$region = $row['region_id'];
					$country = $row['country_id'];
					$datagrid[$region]['region'] = $row['region_name'];
					$datagrid[$region]['countries'][$country] = $row['country_name'];
				}
			}

			// data: countries
			$result = $model->query("
				SELECT DISTINCT country_id, country_region
				FROM item_countries
				INNER JOIN countries ON country_id = item_country_country_id
				WHERE item_country_item_id = $id
			")->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$region = $row['country_region'];
					$country = $row['country_id'];
					$selected[$region][$country] = true;
				}
			}

			// header
			$this->view->header('pagecontent')->node('header')->data('title', $item->code.', '.$item->name);
			$this->view->companytabs('pagecontent')->navigation('tab');

			if (!$_CAN_EDIT) {
				$disabled['regions'] = true;
			}

			// template: form
			$this->view->itemRegions('pagecontent')
			->item('regions')
			->data('id', $id)
			->data('data', $data)
			->data('selected', $selected)
			->data('disabled', $disabled)
			->data('datagrid', $datagrid)
			->data('buttons', $buttons);
		}
	}