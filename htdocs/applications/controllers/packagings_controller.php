<?php

	class Packagings_Controller {

		public function __construct() {
			
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}

		public function index() {
			
			$permission_catalog = user::permission('can_edit_catalog');
			
			if (!$this->request->archived && $permission_catalog) {
				$link = $this->request->query("add");
				$this->request->field('add', $link);
			}
			
			$link = $this->request->query('data');
			$this->request->field('data', $link);

			// view template
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'list.php');
			$tpl->data('url', '/applications/modules/packaging/type/list.php');
			$tpl->data('class', 'list-600');
			$this->view->setTemplate('packaging', $tpl);

			Compiler::attach(array(
				"/public/scripts/dropdown/dropdown.css",
				"/public/scripts/dropdown/dropdown.js",
				"/public/scripts/table.loader.js"
			));
		}

		public function add() {

			$permission_catalog = user::permission('can_edit_catalog');

			if ($this->request->archived && !$permission_catalog) {
				Message::invalid_permission();
				url::redirect($this->request->query());
			}
			
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			
			$buttons = array();
			$buttons['back'] =  $this->request->query();
			$buttons['save'] = true;

			// view template
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'packaging/type/form.php');
			$tpl->data('data', $data);
			$tpl->data('buttons', $buttons);
			$this->view->setTemplate('packaging', $tpl);

			Compiler::attach(array(
				'/public/scripts/validationEngine/css/validationEngine.jquery.css',
				'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
				'/public/scripts/validationEngine/js/jquery.validationEngine.js',
				'/public/scripts/qtip/qtip.css',
				'/public/scripts/qtip/qtip.js',
				'/public/js/form.validator.js'
			));
		}

		public function data() {
			
			$id = url::param();
			$permission_catalog = user::permission('can_edit_catalog');

			$type = new Modul(Connector::DB_CORE);
			$type->setTable('packaging_types');
			$data = $type->read($id);

			if (!$type->id) {
				Message::failure_id();
				url::redirect($this->request->query());
			}
			
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			
			$buttons = array();
			$buttons['back'] =  $this->request->query();
			
			if (!$this->request->archived && $permission_catalog) {

				$buttons['save'] = true;

				$integrity = new Integrity();
				$integrity->set($id, 'packaging_types', 'system');
				
				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link('/applications/modules/packaging/type/delete.php', array('id'=>$id));
				}
			}
			
			// view template
			$tpl = new Template('pagecontent');
			$tpl->template(PATH_TEMPLATES.'packaging/type/form.php');
			$tpl->data('data', $data);
			$tpl->data('buttons', $buttons);
			$this->view->setTemplate('packaging', $tpl);

			Compiler::attach(array(
				'/public/scripts/validationEngine/css/validationEngine.jquery.css',
				'/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js',
				'/public/scripts/validationEngine/js/jquery.validationEngine.js',
				'/public/scripts/qtip/qtip.css',
				'/public/scripts/qtip/qtip.js',
				'/public/js/form.validator.js'
			));
		}
	}
	