<?php 

	class Companies_Controller {
	
		public function __construct() {
			
			$this->request = request::instance();
			$this->view = new View();
			$this->translate = Translate::instance();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() { 
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			
			
			$requestFields = array();
			$can_edit_companies = user::permission(Company::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Company::PERMISSION_EDIT_LIMITED);
			
			// button: add new company
			if (!$this->request->archived && ($can_edit_companies || $permission_edit_limited)) {
				$link = $this->request->query('add');
				$this->request->field('add',$link);
			}
			
			// link: edit company
			$link = $this->request->query('address');
			$this->request->field('form',$link);
			
			// link: print company sheet
			$link = $this->request->link('/applications/exports/pdf/company.php');
			$this->request->field('pdf',$link);
			
			// button: print companies
			$link = $this->request->link('/applications/exports/excel/companies.php');
			$this->request->field('export', $link);
						
			// template: companies list
			$this->view->companies('pagecontent')->company('list');
			
			$table = new DB_Table();
			$table->read_from_name('addresses');
			$table_fields = ($table->export_list) ? unserialize($table->export_list) : array();
		}
	
		public function add() {
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."chain/chained.select.js");
		
			$user = user::instance();
			
			// permissions
			$permission_edit = user::permission(Company::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Company::PERMISSION_EDIT_LIMITED);
			$permission_manager = user::permission(Company::PERMISSION_MANAGER);
			$permission_catalog = user::permission(Company::PERMISSION_EDIT_CATALOG);
			
			if ($this->request->archived || (!$permission_edit && !$permission_edit_limited) ) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['address_active'] = 1;
			$data['redirect'] = $this->request->query('address');
			$data['address_showinposindex'] = 1;
			
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			if (application::involved_in_planning($this->request->application)) {
				$data['address_type'] = 7;
			}
			
			if ($permission_edit) {
				$buttons['save'] = true;
			}
			elseif ($permission_edit_limited) {
				$buttons['save'] = true;
				$hidden['address_parent'] = true;
				$hidden['address_involved_in_planning'] = true;
				$hidden['address_involved_in_planning_ff'] = true;
				$hidden['address_is_independent_retailer'] = true;
				$hidden['address_involved_in_lps'] = true;
				$hidden['address_involved_in_lps_ff'] = true;
				
			}
			
			// for non-manages disable customer number and shipto number
			if (!$permission_manager) {
				$hidden['address_mps_customernumber'] = true;
				$hidden['address_mps_shipto'] = true;
				$hidden['address_can_own_independent_retailers'] = true;
			}
			
			// hide red fields
			$hidden['address_involved_in_red'] = true;
			$hidden['address_legal_entity_name'] = true;
			$hidden['address_currency'] = true;

			// dataloader: countries
			$dataloader['address_country'] = country::loader();
			
			// dataloader: currencies
			$dataloader['address_currency'] = currency::loader();
				
			// dataloader: clients
			if (!$permission_edit && $permission_edit_limited) {
				$clinets_filter[] = '( address_id = '.$user->address.' OR address_parent = '.$user->address.' )';
			}
			
			$clinets_filter[] = 'address_type = 1';
			$dataloader['address_parent'] = Company::loader($clinets_filter);

			// hide non-manager fields
			if (!$permission_manager && !$permission_catalog) {
				
				// Export MPS Distrubution Data to SAP
				$hidden['address_do_data_export_from_mps_to_sap'] = true;
				
				// internal address
				$hidden['address_is_internal_address'] = true;
			} 
				
			// template: company form
			$this->view->addressForm('pagecontent')
			->company('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons);
		}
		
		public function address() {
			
			$id = url::param();
			$user = user::instance();
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."chain/chained.select.js");
			
			// copmany
			$company = new Company();
			$company->read($id); 


			
			// check acces
			if (!$company->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}

			// can edit company
			$canEdit = $company->canEdit();
			
			// permissions
			$permission_edit = user::permission(Company::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Company::PERMISSION_EDIT_LIMITED);
			$permission_manager = user::permission(Company::PERMISSION_MANAGER);
			$permission_catalog = user::permission(Company::PERMISSION_EDIT_CATALOG);
		
			// form dara
			$data = $company->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['archived'] = $this->request->archived;
			$data['action'] = $this->request->action;
			
			// button back
			$buttons = array();
			$buttons['back'] = $this->request->query();

			
			if ($permission_catalog and $company->data["address_type"] == 7) {
				$buttons['accessAddress3rd'] = "/admin/address3rd.php?country=" . $company->data["address_type"] . "&type=1&searchterm=&id=$id";
			}
			elseif ($permission_catalog) {
				$buttons['accessAddress'] = "/admin/address.php?id=$id";
			}
			

			// company languages tab show only for clients
			if (!$company->involved_in_planning) {
				$this->request->exclude($this->request->application.'/'.$this->request->controller.'/languages');
			}
			
			// form fields
			if ($company->data) {
				foreach ($company->data as $field => $value) {
					$fields[$field] = true;
				}
			}
			
			// for archived companies
			// disable all form fields
			if ($this->request->archived) {
				$disabeld_fields = $fields;
				$hidden_fields['address_can_own_independent_retailers'] = ($company->can_own_independent_retailers) ? false : true;
				$hidden_fields['address_involved_in_planning'] = ($company->involved_in_planning) ? false : true;
				$hidden_fields['address_involved_in_planning_ff'] = ($company->involved_in_planning_ff) ? false : true;
				$hidden_fields['address_is_independent_retailer'] = ($company->is_independent_retailer) ? false : true;

				$hidden_fields['address_involved_in_lps'] = ($company->involved_in_planning) ? false : true;
				$hidden_fields['address_involved_in_lps_ff'] = ($company->involved_in_planning_ff) ? false : true;

			}
			// for client companies
			elseif ($company->type==1) { 
				
				
				// for catalog users
				if ($permission_catalog) { 
					$buttons['save'] = true;
				}
				else { 
					
					$disabeld_fields = $fields;
					
					$hidden_fields['address_is_independent_retailer'] = true;
					
					if ($canEdit) {
						
						if ($permission_edit) {
							$buttons['save'] = true;
							unset($disabeld_fields['address_can_own_independent_retailers']);
							unset($disabeld_fields['address_involved_in_planning']);
							unset($disabeld_fields['address_involved_in_planning_ff']);	

							unset($disabeld_fields['address_involved_in_lps']);
							unset($disabeld_fields['address_involved_in_lps_ff']);	
						}
						elseif($permission_edit_limited) {
							$hidden_fields['address_involved_in_planning'] = true;
							$hidden_fields['address_involved_in_planning_ff'] = true;

							$hidden_fields['address_involved_in_lps'] = true;
							$hidden_fields['address_involved_in_lps_ff'] = true;
						}
						
						if ($permission_manager) {
							$buttons['save'] = true;
							unset($disabeld_fields['address_mps_customernumber']);
							unset($disabeld_fields['address_mps_shipto']);
						}
					}
				}
				
				$disabeld_fields['address_number'] = true;
			}
			// for third party companies
			elseif ($company->type==7) { 

				if ($canEdit) {
					$buttons['save'] = true;
				}

				if (!$permission_catalog && !$permission_edit) {
					$disabeld_fields['address_parent'] = true;
					$disabeld_fields['address_involved_in_lps'] = true;
				}

				if ($permission_edit || $permission_manager) {  
					$hidden_fields['address_is_independent_retailer'] = true;
				} 

				if (!$permission_edit && !$permission_catalog) { 
					$hidden_fields['address_involved_in_planning'] = true;
					$hidden_fields['address_involved_in_planning_ff'] = true;
					$hidden_fields['address_is_independent_retailer'] = true;

					$hidden_fields['address_involved_in_lps'] = true;
					$hidden_fields['address_involved_in_lps_ff'] = true;
				}

				if($permission_edit_limited && $company->is_independent_retailer) { 
					$hidden_fields['address_is_independent_retailer'] = true;
				}

				/*
				// franchisee
				if ($permission_catalog) { 
					$buttons['save'] = true;
				}
				elseif ($company->canbefranchisee || $company->canbefranchisee_worldwide) { 

					$disabeld_fields = $fields;
					
					if ($canEdit) {

						$buttons['save'] = true;

						if ($permission_edit) {  
							$buttons['save'] = true;
							unset($disabeld_fields['address_can_own_independent_retailers']);
							unset($disabeld_fields['address_involved_in_planning']);
							unset($disabeld_fields['address_involved_in_planning_ff']);
							$hidden_fields['address_is_independent_retailer'] = true;
						}
						elseif ($permission_edit_limited) { 
							$hidden_fields['address_involved_in_planning'] = treu;
							$hidden_fields['address_involved_in_planning_ff'] = true;
							$hidden_fields['address_is_independent_retailer'] = true;
						}
						
						if ($permission_manager) { 
							$buttons['save'] = true;
							unset($disabeld_fields['address_mps_customernumber']);
							unset($disabeld_fields['address_mps_shipto']);
							$hidden_fields['address_is_independent_retailer'] = true;
						}
					}
				}
				// independent retailer
				elseif($company->is_independent_retailer) {  
				
					
					$disabeld_fields = $fields;
					
					if ($canEdit) {

						$buttons['save'] = true;
						
						if ($permission_edit) {
							$buttons['save'] = true;
						}
						elseif ($permission_edit_limited) {
							$buttons['save'] = true;
							$hidden_fields['address_is_independent_retailer'] = true;
						}
						
						if (!$permission_manager) {
							$buttons['save'] = true;
							$disabeld_fields['address_mps_customernumber'] = true;
							$disabeld_fields['address_mps_shipto'] = true;
						}
						else {
							if ($company->involved_in_planning || $company->involved_in_planning_ff) {
								unset($disabeld_fields['address_involved_in_planning']); 
							}
						}
					}
				
					$hidden_fields['address_involved_in_planning'] = true;
					$hidden_fields['address_involved_in_planning_ff'] = true;
				}
				else {
					
					$disabeld_fields = $fields;
					$hidden_fields['address_can_own_independent_retailers'] = ($company->can_own_independent_retailers) ? false : true;
					$hidden_fields['address_involved_in_planning'] = ($company->involved_in_planning) ? false : true;
					$hidden_fields['address_involved_in_planning_ff'] = ($company->involved_in_planning_ff) ? false : true;
					$hidden_fields['address_is_independent_retailer'] = ($company->is_independent_retailer) ? false : true;
				}
				
				if ($canEdit && $permission_manager && ($company->involved_in_planning || $company->involved_in_planning_ff)) { 
					$buttons['save'] = true;
					unset($disabeld_fields['address_involved_in_planning']);
					unset($disabeld_fields['address_involved_in_planning_ff']);
				}
				*/
				
				$hidden_fields['address_number'] = true;
			}
			else {
				$disabeld_fields = $fields;
				$hidden_fields['address_can_own_independent_retailers'] = ($company->can_own_independent_retailers) ? false : true;
				$hidden_fields['address_involved_in_planning'] = ($company->involved_in_planning) ? false : true;
				$hidden_fields['address_involved_in_planning_ff'] = ($company->involved_in_planning_ff) ? false : true;
				$hidden_fields['address_is_independent_retailer'] = ($company->is_independent_retailer) ? false : true;

				$hidden_fields['address_involved_in_lps'] = ($company->involved_in_planning) ? false : true;
				$hidden_fields['address_involved_in_lps_ff'] = ($company->involved_in_planning_ff) ? false : true;
			}
			
			// SAP number
			if (!$this->request->archived && $canEdit) {
				unset($disabeld_fields['address_sapnr']);
				unset($hidden_fields['address_sapnr']);
				unset($disabeld_fields['address_sap_salesorganization']);
				unset($hidden_fields['address_sap_salesorganization']);
				$buttons['save'] = true;
			}
			
			
			// hide non-manager fields
			if (!$permission_manager && !$permission_catalog) {
				$hidden_fields['address_do_data_export_from_mps_to_sap'] = true;
				$hidden_fields['address_is_internal_address'] = true;
			} 
			elseif (!$this->request->archived && $canEdit) {
				unset($disabeld_fields['address_do_data_export_from_mps_to_sap']);
				unset($disabeld_fields['address_is_internal_address']);
				$buttons['save'] = true;
			}
			
			
			// mps application: hide red fields
			$hidden_fields['address_involved_in_red'] = true;
			$hidden_fields['create_pos_from_company'] = true;
			$hidden_fields['address_currency'] = true;
			$hidden_fields['address_legal_entity_name'] = true;
			$hidden_fields['address_legnr'] = true;
			$disabeld_fields['address_legnr'] = true;
			
			// show legal number
			if ($company->client_type==2 || $company->client_type==3) {
				unset($hidden_fields['address_legnr']);
			}
			
			// mps customer and shipto number
			if (!$this->request->archived && $permission_manager && $canEdit) {
				unset($disabeld_fields['address_mps_customernumber']);
				unset($hidden_fields['address_mps_customernumber']);
				unset($disabeld_fields['address_mps_shipto']);
				unset($hidden_fields['address_mps_shipto']);
				$buttons['save'] = true;
			}
			
			// dataloader: countries
			$dataloader['address_country'] = country::loader();
			
			// dataloader: provinces
			$dataloader['address_province_id'] = province::loader(array(
				'province_country = '.$company->country
			));
			
			// dataloader: places
			$dataloader['address_place_id'] = place::loader(array(
				'place_country = '.$company->country, 
				'place_province = '.$company->province_id
			));
			
			// dataloader: currencies
			$dataloader['address_currency'] = currency::loader();
				
			// dataloader: clients
			if (!user::permission(Company::PERMISSION_EDIT) && user::permission(Company::PERMISSION_EDIT_LIMITED)) {
				$clinets_filter[] = '( address_id = '.$user->address.' OR address_parent = '.$user->address.' )';
			}
			
			$clinets_filter[] = 'address_type = 1';
			$dataloader['address_parent'] = Company::loader($clinets_filter);


			$data['address_phone'] = $data['address_phone'];
			$data['address_mobile_phone'] = $data['address_mobile_phone'];
			
			// template header
			$this->view->header('pagecontent')->node('header')->data('title', $company->header());
			$this->view->companytabs('pagecontent')->navigation('tab');
			
			// template: company form
			$this->view->addressForm('pagecontent')
			->company('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabeld_fields)
			->data('hidden', $hidden_fields)
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function additionals() {
			
			$id = url::param();
			
			$user = user::instance();
			
			// get company
			$company = new Company();
			$company->read($id);
			
			// check acces
			if (!$company->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}

			// can edit company
			$canEdit = $company->canEdit();
			
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			
			// cuttons back
			$buttons['back'] = $this->request->query();
						
			// additionals
			$company_additional = new Company_Additional();
			$data = $company_additional->read($id);
			
			if (!$data) {
				$data['redirect'] = $this->request->query("additionals/$id");
			}
			
			// company languages tab show only for clients
			if (!$company->involved_in_planning) {
				$this->request->exclude($this->request->application."/companies/languages");
			}
			
			$data['address_additional_address'] = $id;
			$data['address_mps_customernumber'] = $company->mps_customernumber;
			$data['address_mps_shipto'] = $company->mps_shipto;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] =$this->request->action;
			
			$disabled['address_mps_customernumber'] = true;
			$disabled['address_mps_shipto'] = true;
				
			if (!$this->request->archived && $canEdit) {
				$buttons['save'] = true;
			} else {
				$disabled['address_additional_customerno2'] = true;
				$disabled['address_additional_customerno3'] = true;
				$disabled['address_additional_customerno4'] = true;
				$disabled['address_additional_customerno5'] = true;
				$disabled['address_additional_shiptono2'] = true;
				$disabled['address_additional_shiptono3'] = true;
				$disabled['address_additional_shiptono4'] = true;
				$disabled['address_additional_shiptono5'] = true;
			}
			
			// template: header
			$this->view->header('pagecontent')->node('header')->data('title', $company->header());
			$this->view->companyTabs('pagecontent')->navigation('tab');
			
			// template: company form
			$this->view->companyForm('pagecontent')
			->company('additional.data')
			->data('data', $data)
			->data('disabled', $disabled)
			->data('buttons', $buttons)
			->data('id', $id);
		}


		public function mps_sap_hq_ordertypes() { 
			
			// permissions
			$permission_edit = user::permission(Company::PERMISSION_EDIT);
			
			$id = url::param();
			$param = url::param(1);
			$user = user::instance();
			
			// company
			$company = new Company();
			$company->read($id);
			
			// check acces
			if (!$company->access() or !$permission_edit) {
				message::access_denied();
				url::redirect($this->request->query());
			}
						
			// Mps_sap_hq_ordertype
			$mps_sap_hq_ordertype = new Company_Mps_sap_hq_ordertype();
			
			if ($param) {
				$data = $mps_sap_hq_ordertype->read($param);
			}


			
			// buttons
			$buttons = array();
			
			// template: headers
			$this->view->header('pagecontent')->node('header')->data('title', $company->header());
			$this->view->tabs('pagecontent')->navigation('tab');
			
			if ($param) {
				
				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				Compiler::attach(DIR_JS."form.validator.js");
				
				$param = (is_numeric($param)) ? $param : null;
				
				// button back
				$buttons['back'] = $this->request->query($this->request->action.'/'.$id);
				
				if ($param) {
					
					$mps_sap_hq_ordertype = new Company_Mps_sap_hq_ordertype();
					$data = $mps_sap_hq_ordertype->read($param);	
					
				} else {
					$data['redirect'] = $this->request->query($this->request->action.'/'.$id);
				}
		
				// form dataloader
				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;
				$data['action'] = $this->request->action;

				$data['mps_sap_hq_ordertype_address_id'] = $id;

				// dataloader: SAP HQ Order Types
				$model = new Sap_hq_order_type_Model(Connector::DB_CORE);
				$result = $model->loader();
				$dataloader['mps_sap_hq_ordertype_sap_hq_ordertype_id'] = _array::extract($result);

				// dataloader: SAP HQ Order Resons
				$model = new Sap_hq_order_reason_Model(Connector::DB_CORE);
				$result = $model->loader();
				$dataloader['mps_sap_hq_ordertype_sap_hq_orderreason_id'] = _array::extract($result);
				
				if( !$this->request->archived && $permission_edit) {
				
					$buttons['save'] = true;
				
					if ($param) {
				
						
						$query = array('id'=>$param);
						$buttons['delete'] = $this->request->link('/applications/helpers/company.mps_sap_hq_ordertype.delete.php', $query);
					}
				} else {
					$fields = array_keys($data);
					$disabled = array_fill_keys($fields, true);
				}

				// template: material form
				$this->view->materialForm('pagecontent')
				->company('mps_sap_hq_ordertype.form')
				->data('data', $data)
				->data('dataloader', $dataloader)
				->data('disabled', $disabled)
				->data('hidden', $hidden)
				->data('buttons', $buttons)
				->data('mps_sap_hq_ordertype', $param);
				
			} else {
				
				Compiler::attach(DIR_SCRIPTS."table.loader.js");
				
				// button: back
				$buttons['back'] = $this->request->query();
				
				// request field: id
				$this->request->field('id', $id);
				
				// request field: edit
				if (!$this->request->archived && $permission_edit) {
					$link = $this->request->query("mps_sap_hq_ordertypes/$id/add");
					$this->request->field('add', $link);
				}

				// form link
				$link = $this->request->query("mps_sap_hq_ordertypes/$id");
				$this->request->field('form', $link);
				
				// template: company mps_sap_hq_ordertypes
				$this->view->mps_sap_hq_ordertypes('pagecontent')
				->company('mps_sap_hq_ordertypes.list')
				->data('buttons', $buttons)
				->data('id', $id);
			}
		}
		
		public function pos() { 
			
			$id = url::param();
			
			$user = user::instance();
			
			session::remove('pos_referer');
			
			// company
			$company = new Company();
			$company->read($id);
			
			// check acces
			if (!$company->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}

			// can edit company
			$canEdit = $company->canEdit();
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			
			// db model
			$model = new Model(Connector::DB_CORE);
			
			// button back
			$buttons['back'] = $this->request->query();
			
			// company languages tab show only for clients
			if (!$company->involved_in_planning) {
				$this->request->exclude($this->request->application."/companies/languages");
			}
			
			// template: headers
			$this->view->header('pagecontent')->node('header')->data('title', $company->header());
			$this->view->tabs('pagecontent')->navigation('tab');
			
			$this->request->field('id', $id);
				
			// template: company pos locations
			$this->view->poslocations('pagecontent')
			->company('pos')
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function languages() {
			
			$id = url::param();
			
			$user = user::instance();
			
			// company
			$company = new Company();
			$company->read($id);
			
			// check acces
			if (!$company->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}

			// can edit company
			$canEdit = $company->canEdit();
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			
			// permissions
			$permission_edit = user::permission(Company::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Company::PERMISSION_EDIT_LIMITED);
			
			$buttons = array();
			$buttons['back'] = $this->request->query();
				
			// template: headers
			$this->view->header('pagecontent')->node('header')->data('title', $company->header());
			$this->view->tabs('pagecontent')->navigation('tab');
			
			// template: company languages
			$this->view->companyLanguages('pagecontent')
			->company('language.list')
			->data('buttons', $buttons)
			->data('id', $id);
		}
		
		public function warehouses() {
			
			// permissions
			$permission_edit = user::permission(Company::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Company::PERMISSION_EDIT_LIMITED);
			
			$id = url::param();
			$param = url::param(1);
			$user = user::instance();
			
			// company
			$company = new Company();
			$company->read($id);
			
			// check acces
			if (!$company->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			// can edit company
			$canEdit = $company->canEdit();

			if ($permission_edit_limited) {
				$canEdit = false;
			}
			
			// warehouse
			$warehouse = new Company_Warehouse();
			
			if ($param) {
				$data = $warehouse->read($param);
			}
			
			// buttons
			$buttons = array();
			
			// template: headers
			$this->view->header('pagecontent')->node('header')->data('title', $company->header());
			$this->view->tabs('pagecontent')->navigation('tab');
			
			if ($param) {
				
				Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
				Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
				Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
				Compiler::attach(DIR_JS."form.validator.js");
				
				$param = (is_numeric($param)) ? $param : null;
				
				// button back
				$buttons['back'] = $this->request->query($this->request->action.'/'.$id);
				
				if ($param) {
					
					$warehouse = new Company_Warehouse();
					$data = $warehouse->read($param);	
					
				} else {
					$data['redirect'] = $this->request->query($this->request->action.'/'.$id);
				}

				// form dataloader
				$data['address_warehouse_address_id'] = $id;
				$data['application'] = $this->request->application;
				$data['controller'] = $this->request->controller;
				$data['action'] = $this->request->action;
				$data['export'] = $company->do_data_export_from_mps_to_sap;
				
				if( !$this->request->archived && !$warehouse->stock_reserve && $canEdit) {
				
					$buttons['save'] = true;
				
					if ($param) {
				
						$integrity = new Integrity();
						$integrity->set($param, 'address_warehouses', 'administration');
				
						if ($integrity->check()) {
							$query = array('id'=>$param);
							$buttons['delete'] = $this->request->link('/applications/helpers/company.warehouse.delete.php', $query);
						} else {
							$disabled['address_warehouse_active'] = true;
						}
					}
				} else {
					$fields = array_keys($data);
					$disabled = array_fill_keys($fields, true);
				}
				
				// template: material form
				$this->view->materialForm('pagecontent')
				->company('warehouse.form')
				->data('data', $data)
				->data('disabled', $disabled)
				->data('hidden', $hidden)
				->data('buttons', $buttons)
				->data('company', $id)
				->data('warehouse', $param);
				
			} else {
				
				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
				Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
				Compiler::attach(DIR_SCRIPTS."table.loader.js");
				
				// button: back
				$buttons['back'] = $this->request->query();
				
				// request field: id
				$this->request->field('id', $id);
				
				// request field: edit
				if (!$this->request->archived && $permission_edit) {
					$link = $this->request->query("warehouses/$id/add");
					$this->request->field('add', $link);
				}

				// form link
				$link = $this->request->query("warehouses/$id");
				$this->request->field('form', $link);
				
				// template: company languages
				$this->view->companyLanguages('pagecontent')
				->company('warehouse.list')
				->data('buttons', $buttons)
				->data('id', $id);
			}
		}
		
		public function users() {
			
			$id = url::param();
			$param = url::param(1);
			
			$company = new Company();
			$company->read($id);
			
			// check acces
			if (!$company->access()) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."loader/loader.css");
			Compiler::attach(DIR_SCRIPTS."loader/loader.js");

			// can edit company
			$canEdit = $company->canEdit();
			
			// permissions
			$permission_edit = user::permission(Company::PERMISSION_EDIT);
			$permission_edit_limited = user::permission(Company::PERMISSION_EDIT_LIMITED);
			$permission_catalog = user::permission(Company::PERMISSION_EDIT_CATALOG);
			
			// template: headers
			$this->view->header('pagecontent')->node('header')->data('title', $company->header());
			$this->view->tabs('pagecontent')->navigation('tab');
			
			// button
			$buttons = array();
			
			// button: back
			$buttons['back'] = $this->request->query();
			
			// get planning applications
			$planning_applications = application::get_all_involved_in_planning();
			
			// request form: company
			$this->request->field('id', $id);
			$this->request->field('active', 'active');
				
			if ($planning_applications) {
				
				$planning_roles = array();
				
				foreach ($planning_applications as $app) {
					$planning_roles[] = $app['application_shortcut'];
				}
				
				$role_applications = join(',',$planning_roles);
				$this->request->field('role_applications',$role_applications);
			}
				
			$this->view->usersList('pagecontent')
			->user('list')
			->data('role_applications', $role_applications)
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}