<?php 

	class SapBlockcodes_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			$this->translate = Translate::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() { 
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// request form: link edit
			$this->request->field('data', $this->request->query('data'));
			$this->request->field('add', $this->request->query('add'));
		
			$this->view->sapBlockCode('pagecontent')->sapblockcode('list');
		}
		
		public function add() {
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
				
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
		
			// template: application form
			$this->view->sapBlockCode('pagecontent')
			->sapblockcode('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons);
		}
		
		public function data() {
			
			$id = url::param();
			
			$sapBlockCode = new SapBlockCode();
			$sapBlockCode->read($id);
			
			if (!$sapBlockCode->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.css");
			Compiler::attach(DIR_SCRIPTS."fancybox/fancybox.js");
			
			// form datalaoder
			$data = $sapBlockCode->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['save'] = true;
			$buttons['back'] = $this->request->query();
			
			// button: delete
			if (user::permission('can_administrate_system_data')) {
					
				$integrity = new Integrity();
				$integrity->set($id, 'sap_blockcodes', 'system');
					
				if ($integrity->check()) {
					$buttons['delete'] = $this->request->link("/applications/helpers/sap.blockcode.delete.php", array('id'=>$id));
				}
					
			} else {
				foreach ($data as $field => $value) {
					$disabled[$field] = true;
				}
			}
			
			// template:: header
			$this->view->header('pagecontent')->node('header')->data('title', $sapBlockCode->text);
			
			$this->view->sapBlockCode('pagecontent')
			->sapblockcode('data')
			->data('data', $data)
			->data('disabled', $disabled)
			->data('dataloader', $dataloader)
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}
	