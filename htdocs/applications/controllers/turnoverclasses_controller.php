<?php

	class TurnOverClasses_Controller {

		public function __construct() {

			$this->user = User::instance();
			$this->request = request::instance();

			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}

		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			

			$this->request->field('print', $this->request->link('/applications/exports/excel/turnover.classes.php'));
			
			// permission: can edit turnover classes
			$can_edit = user::permission(TurnoverClass::PERMISSION_EDIT);

			if (!$this->request->archived) {
				$this->request->field('form', $this->request->query('data'));
			}

			// template: turnover class
			$this->view->turnoverClass('pagecontent')->turnover('class.list');
		}

		public function add() {
			
			// permissions 
			$permission_edit = user::permission(TurnoverClass::PERMISSION_EDIT);

			if( $this->request->archived || !$permission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;

			// dataloader: turnover groups
			$dataloader['mps_turnoverclass_group_id'] = TurnoverGroup::loader($this->request->application);

			$this->view->turnoverClass('pagecontent')
			->turnover('class.data')
			->data('dataloader', $dataloader)
			->data('data', $data)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons);
		}

		public function data() {

			$id = url::param();
			
			// permissions 
			$permission_edit = user::permission(TurnoverClass::PERMISSION_EDIT);

			$turnoverClass = new TurnoverClass();
			$turnoverClass->read($id);
			
			// check access to material category
			if (!$turnoverClass->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");

			// form dataloader
			$data = $turnoverClass->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;

			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
				
			if (!$this->request->archived && $permission_edit) {
				
				$buttons['save'] = true;
				
				$integrity = new Integrity();
				$integrity->set($id, 'mps_turnoverclasses');

				if ($integrity->check()) {
					$buttons['delete'] = url::build('/applications/helpers/turnover.class.delete.php', array(
						'application' => $this->request->application,
						'controller' => $this->request->controller,
						'action' => $this->request->action,
						'id' => $id
					));
				}
			}
			// disable all fields
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}

			// dataloader: turnover groups
			$dataloader['mps_turnoverclass_group_id'] = TurnoverGroup::loader($this->request->application);

			// template: heafer
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $turnoverClass->header());

			$this->view->turnoverClass('pagecontent')
			->turnover('class.data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}