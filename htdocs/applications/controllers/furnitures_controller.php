<?php 

	class Furnitures_Controller {
		
		public function __construct() {
				
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			// request
			$requestFields = array();
			
			// form link
			$link = $this->request->query('data');
			$this->request->field('data', $link);
			
			// print link
			$link = $this->request->link('/applications/exports/excel/items.php');
			$this->request->field('print', $link);
			
			// template: furnitures
			$this->view->furnitures('pagecontent')
			->item('list')
			->data('namespace', 'furniture');

			Compiler::attach(array(
				"/public/scripts/dropdown/dropdown.css",
				"/public/scripts/dropdown/dropdown.js",
				"/public/scripts/table.loader.js"
			));
		}
		
		public function data() { 
				
			$id = url::param();
			
			$permission_edit = user::permission(Item::PERMISSION_EDIT_MPS_FURNITURE);
			
			$item = new Item();
			$item->read($id);
			
			// check access to item
			if (!$item->id) {
				message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			
			// form dataloader
			$data = $item->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;

			$data["cbm"] = $item->width*$item->height*$item->length/1000000;
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			
			if( !$this->request->archived && $permission_edit) {
				$buttons['save'] = true;
			}
			elseif($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}
			
			if (Application::involved_in_planning($this->request->application)) {
				$disabled['item_category']= true;
				$disabled['item_code']= true;
				$disabled['item_name']= true;
			}
			
			// dataloader: item categories
			$dataloader['item_category'] = Item_Category::loader();
			
			// template:: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $item->header() );
			
			// template: item form
			$this->view->staff_form('pagecontent')
			->item('data')
			->data('data', $data)
			->data('dataloader', $dataloader)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}