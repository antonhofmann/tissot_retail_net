<?php 

	class DistributionGroups_Controller {
		
		public function __construct() {
			
			$this->user = User::instance();
			$this->request = request::instance();
			
			$this->view = new View();
			$this->view->pagetitle = $this->request->title;
			$this->view->usermenu('usermenu')->user('menu');
			$this->view->categories('pageleft')->navigation('tree');

			Compiler::attach(Theme::$Default);
		}
		
		public function index() {
			
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.css");
			Compiler::attach(DIR_SCRIPTS."dropdown/dropdown.js");
			Compiler::attach(DIR_SCRIPTS."table.loader.js");
			
			// permissions
			$permission_edit = user::permission(DistChannel::PERMISSION_EDIT);
			
			// form link
			$link = $this->request->query('data');
			$this->request->field('form', $link);
			
			// print link
			$link = $this->request->link('/applications/exports/excel/distribution.groups.php');
			$this->request->field('print', $link);
			
			if (!$this->request->archived && $permission_edit) {
				$link = $this->request->query('add');
				$this->request->field('add', $link);
			}

			// template: distribution channels list
			$this->view->distributionGroups('pagecontent')->distribution('group.list');
		}
		
		public function add() {
			
			// permissions
			$permission_edit = user::permission(DistChannel::PERMISSION_EDIT);
		
			if( $this->request->archived || !$permission_edit) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// form dataloader
			$data = array();
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			$data['redirect'] = $this->request->query('data');
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
			$buttons['save'] = true;
				
			$this->view->distributionGroup('pagecontent')
			->distribution('group.form')
			->data('data', $data)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons);
		}
		
		public function data() {
				
			$id = url::param();
			
			// permissions
			$permission_edit = user::permission(DistChannel::PERMISSION_EDIT);
			
			$distributionGroup = new DistChannelGroup();
			$distributionGroup->read($id);
			
			// check access to material category
			if (!$distributionGroup->id) {
				Message::access_denied();
				url::redirect($this->request->query());
			}
			
			Compiler::attach(DIR_SCRIPTS."validationEngine/css/validationEngine.jquery.css");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/languages/jquery.validationEngine-en.js");
			Compiler::attach(DIR_SCRIPTS."validationEngine/js/jquery.validationEngine.js");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.css");
			Compiler::attach(DIR_SCRIPTS."qtip/qtip.js");
			Compiler::attach(DIR_JS."form.validator.js");
			
			// form dataloader
			$data = $distributionGroup->data;
			$data['application'] = $this->request->application;
			$data['controller'] = $this->request->controller;
			$data['action'] = $this->request->action;
			
			// buttons
			$buttons = array();
			$buttons['back'] = $this->request->query();
				
			if (!$this->request->archived && $permission_edit) {
				
				$buttons['save'] = true;
				
				$integrity = new Integrity();
				$integrity->set($id, 'mps_distchannel_groups', 'system');
				
				if ($integrity->check()) {
					$buttons['delete'] = url::build('/applications/helpers/distribution.group.delete.php', array(
						'application' => $this->request->application,
						'controller' => $this->request->controller,
						'action' => $this->request->action,
						'id' => $id
					));
				}
			}
			// disable all from fields
			elseif ($data) {
				foreach ($data as $key => $value) {
					$disabled[$key] = true;
				}
			}
			
			// template: header
			$this->view->header('pagecontent')
			->node('header')
			->data('title', $distributionGroup->name);

			// template: distribution channel
			$this->view->distributionGroup('pagecontent')
			->distribution('group.form')
			->data('data', $data)
			->data('disabled', $disabled)
			->data('hidden', $hidden)
			->data('buttons', $buttons)
			->data('id', $id);
		}
	}