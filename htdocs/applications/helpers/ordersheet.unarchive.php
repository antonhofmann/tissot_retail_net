<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];

	
	if ($id) {
		
		$model = new Model($application);
		$tracker = new User_Tracking($application);
		$workflowState = new Workflow_State($application);
		$workflowState->read(Workflow_State::STATE_SHIPPED);
		
		// ordersheet
		$ordersheet = new Ordersheet($application);
		$ordersheet->read($id);
		
		
		//get tracker info of state before last state
		$trackings = $ordersheet->trackings();
		
		if(count($trackings) > 1) {
			$index = count($trackings) - 2;
			
			$row = $trackings[$index];
			$state = $row["workflow_state"];
			
			if($state == 'Order confirmed') {
				$data['mps_ordersheet_workflowstate_id'] = Workflow_State::STATE_ORDER_CONFIRMED;
				$data['user_modified'] = $user->login;

				$response = $ordersheet->update($data);

				//update tracking
		
				$workflowState->read(Workflow_State::STATE_ORDER_CONFIRMED);
				$tracker->create(array(
						'user_tracking_entity' => 'order sheet',
						'user_tracking_entity_id' => $id,
						'user_tracking_user_id' => $user->id,
						'user_tracking_action' => $workflowState->name,
						'user_created' => $user->login,
						'date_created' => date('Y-m-d H:i:s')
					));
			}
			elseif($state == 'Shipped') {

				$data['mps_ordersheet_workflowstate_id'] = Workflow_State::STATE_SHIPPED;
				$data['user_modified'] = $user->login;

				$response = $ordersheet->update($data);

				//update tracking
				$workflowState->read(Workflow_State::STATE_SHIPPED);
				$tracker->create(array(
						'user_tracking_entity' => 'order sheet',
						'user_tracking_entity_id' => $id,
						'user_tracking_user_id' => $user->id,
						'user_tracking_action' => $workflowState->name,
						'user_created' => $user->login,
						'date_created' => date('Y-m-d H:i:s')
					));
			}

			
		}
		
	}
	url::redirect("/$application/$controller/$action/$id");
	