<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	// product line
	$id = $_REQUEST['design_objective_item_id'];
	
	$data = array();
	
	if (in_array('design_objective_item_group', $fields)) {
		$data['design_objective_item_group'] = $_REQUEST['design_objective_item_group'] ? $_REQUEST['design_objective_item_group'] : null;
	}	
	
	if (in_array('design_objective_item_name', $fields)) {
		$data['design_objective_item_name'] = $_REQUEST['design_objective_item_name'];
	}	

	if (in_array('design_objective_item_isstandard', $fields)) {
		$data['design_objective_item_isstandard'] = $_REQUEST['design_objective_item_isstandard'] ? 1 : 0;
	}

	if (in_array('design_objective_item_active', $fields)) {
		$data['design_objective_item_active'] = $_REQUEST['design_objective_item_active'] ? 1 : 0;
	}
	
	if ($data) {
	
		$designObjectiveItem = new Design_Objective_Item();
		$designObjectiveItem->read($id);
	
		if ($designObjectiveItem->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $designObjectiveItem->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $designObjectiveItem->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	