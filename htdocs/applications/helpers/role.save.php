<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$id = $_REQUEST['role_id'];
	$application = $_REQUEST['application'];
	
	$data = array();
	
	if ($_REQUEST['role_code']) {
		$data['role_code'] = $_REQUEST['role_code'];
	}
	
	if ($_REQUEST['role_name']) {
		$data['role_name'] = $_REQUEST['role_name'];
	}
	
	if (isset($_REQUEST['role_order_state_visible_from'])) {
		$data['role_order_state_visible_from'] = ($_REQUEST['role_order_state_visible_from']) ? $_REQUEST['role_order_state_visible_from'] : null;
	}
	
	if (isset($_REQUEST['role_order_state_visible_to'])) {
		$data['role_order_state_visible_to'] = ($_REQUEST['role_order_state_visible_to']) ? $_REQUEST['role_order_state_visible_to'] : null;
	}
	
	if (in_array('role_used_in_mps', $fields)) {
		$data['role_used_in_mps'] = $_REQUEST['role_used_in_mps'];
	}
	
	if (in_array('role_used_in_red', $fields)) {
		$data['role_used_in_red'] = $_REQUEST['role_used_in_red'];
	}
	
	if ($data) {
	
		$role = new Role();
		$role->read($id);
	
		if ($id) {
			$data['user_modified'] = $user->login;
			$action = $role->update($data);
			$message = ($action) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$action = $id = $role->create($data);
			$message = ($action) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect'].$id;
		}
		
		$response = ($action) ? true : false;
		$redirect = ($redirect) ? $redirect : null;
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));
	