<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['item_id'];

$model = new Model(Connector::DB_CORE);

$data = array();

if (in_array('item_code', $fields)) {
	$data['item_code'] = ($_REQUEST['item_code']) ? $_REQUEST['item_code'] : null;
}

if (in_array('item_name', $fields)) {
	$data['item_name'] = ($_REQUEST['item_name']) ? $_REQUEST['item_name'] : null;
}

if (in_array('item_type', $fields)) {
	$data['item_type'] = ($_REQUEST['item_type']) ? $_REQUEST['item_type'] : null;
}

if (in_array('item_price', $fields)) {
	$data['item_price'] = ($_REQUEST['item_price']) ? $_REQUEST['item_price'] : null;
}

if (in_array('item_category', $fields)) {
	$data['item_category'] = ($_REQUEST['item_category']) ? $_REQUEST['item_category'] : null;
}

if (in_array('item_subcategory', $fields)) {
	$data['item_subcategory'] = intval($_REQUEST['item_subcategory']);
}

if (in_array('item_description', $fields)) {
	$data['item_description'] = ($_REQUEST['item_description']) ? $_REQUEST['item_description'] : null;
}

if (in_array('item_cost_group', $fields)) {
	$data['item_cost_group'] = ($_REQUEST['item_cost_group']) ? $_REQUEST['item_cost_group'] : null;
}

if (in_array('item_slave_item', $fields)) {
	$data['item_slave_item'] = ($_REQUEST['item_slave_item']) ? $_REQUEST['item_slave_item'] : null;
}

if (in_array('item_watches_displayed', $fields)) {
	$data['item_watches_displayed'] = ($_REQUEST['item_watches_displayed']) ? $_REQUEST['item_watches_displayed'] : null;
}

if (in_array('item_watches_stored', $fields)) {
	$data['item_watches_stored'] = ($_REQUEST['item_watches_stored']) ? $_REQUEST['item_watches_stored'] : null;
}

if (in_array('item_packed_size', $fields)) {
	$data['item_packed_size'] = ($_REQUEST['item_packed_size']) ? $_REQUEST['item_packed_size'] : null;
}

if (in_array('item_packed_weight', $fields)) {
	$data['item_packed_weight'] = ($_REQUEST['item_packed_weight']) ? $_REQUEST['item_packed_weight'] : null;
}

if (in_array('item_materials', $fields)) {
	$data['item_materials'] = ($_REQUEST['item_materials']) ? $_REQUEST['item_materials'] : null;
}

if (in_array('item_electrical_specifications', $fields)) {
	$data['item_electrical_specifications'] = ($_REQUEST['item_electrical_specifications']) ? $_REQUEST['item_electrical_specifications'] : null;
}

if (in_array('item_lights_used', $fields)) {
	$data['item_lights_used'] = ($_REQUEST['item_lights_used']) ? $_REQUEST['item_lights_used'] : null;
}

if (in_array('item_regulatory_approvals', $fields)) {
	$data['item_regulatory_approvals'] = ($_REQUEST['item_regulatory_approvals']) ? $_REQUEST['item_regulatory_approvals'] : null;
}

if (in_array('item_install_requirements', $fields)) {
	$data['item_install_requirements'] = ($_REQUEST['item_install_requirements']) ? $_REQUEST['item_install_requirements'] : null;
}


if (in_array('item_unit', $fields)) {
	$data['item_unit'] = ($_REQUEST['item_unit']) ? $_REQUEST['item_unit'] : null;
}

if (in_array('item_packaging_type', $fields)) {
	$data['item_packaging_type'] = ($_REQUEST['item_packaging_type']) ? $_REQUEST['item_packaging_type'] : null;
}

if (in_array('item_width', $fields)) {
	$data['item_width'] = ($_REQUEST['item_width']) ? $_REQUEST['item_width'] : null;
}

if (in_array('item_height', $fields)) {
	$data['item_height'] = ($_REQUEST['item_height']) ? $_REQUEST['item_height'] : null;
}

if (in_array('item_length', $fields)) {
	$data['item_length'] = ($_REQUEST['item_length']) ? $_REQUEST['item_length'] : null;
}

if (in_array('item_gross_weight', $fields)) {
	$data['item_gross_weight'] = ($_REQUEST['item_gross_weight']) ? $_REQUEST['item_gross_weight'] : null;
}

if (in_array('item_net_weight', $fields)) {
	$data['item_net_weight'] = ($_REQUEST['item_net_weight']) ? $_REQUEST['item_net_weight'] : null;
}

if (in_array('item_radius', $fields)) {
	$data['item_radius'] = ($_REQUEST['item_radius']) ? $_REQUEST['item_radius'] : null;
}

if (in_array('item_stock_property_of_swatch', $fields)) {
	$data['item_stock_property_of_swatch'] = ($_REQUEST['item_stock_property_of_swatch']) ? 1 : 0;
}

if (in_array('item_is_dr_swatch_furniture', $fields)) {
	$data['item_is_dr_swatch_furniture'] = ($_REQUEST['item_is_dr_swatch_furniture']) ? 1 : 0;
}

if (in_array('item_visible', $fields)) {
	$data['item_visible'] = ($_REQUEST['item_visible']) ? 1 : 0;
}

if (in_array('item_visible_in_projects', $fields)) {
	$data['item_visible_in_projects'] = ($_REQUEST['item_visible_in_projects']) ? 1 : 0;
}

if (in_array('item_visible_in_production_order', $fields)) {
	$data['item_visible_in_production_order'] = ($_REQUEST['item_visible_in_production_order']) ? 1 : 0;
}

if (in_array('item_price_adjustable', $fields)) {
	$data['item_price_adjustable'] = ($_REQUEST['item_price_adjustable']) ? 1 : 0;
}

if (in_array('item_active', $fields)) {
	$data['item_active'] = ($_REQUEST['item_active']) ? 1 : 0;
}

if (in_array('item_visible_in_mps', $fields)) {
	$data['item_visible_in_mps'] = ($_REQUEST['item_visible_in_mps']) ? 1 : 0;
}

if (in_array('item_addable_in_mps', $fields)) {
	$data['item_addable_in_mps'] = ($_REQUEST['item_addable_in_mps']) ? 1 : 0;
}

if (in_array('item_stackable', $fields)) {
	$data['item_stackable'] = ($_REQUEST['item_stackable']) ? 1 : 0;
}

if ($data) {
	
	$item = new Item();
	$item->read($id);
	
	if ($item->id) {
		
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');

		$response = $item->update($data);

		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		
		$response = $id = $item->create($data);
		
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		
		$redirect = $id && $_REQUEST['redirect'] ? $_REQUEST['redirect']."/$id" : null;
	}
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}


if ($id && $response) {
	$item->read($id);
}

// item material list
if ($id && $response && isset($_REQUEST['item_material_name'])) {

	// remove materials for current item
	$sth = $model->db->prepare("
		DELETE FROM item_materials 
		WHERE item_material_item_id = ?
	");

	$sth->execute(array($id));

	$sth = $model->db->prepare("
		INSERT INTO item_materials (
			item_material_item_id,
			item_material_name,
			user_created
		)
		VALUES (?,?,?)
	");

	// update material data
	foreach ($_REQUEST['item_material_name'] as $i => $value) {

		// null value fields
		if (!$value) {
			$removeFields['#itemMaterialList'][] = $i;
			continue;
		}

		$sth->execute(array($id, $value, $user->login));
	}
}

// item electric list
if ($id && $response && isset($_REQUEST['item_electricspec_name'])) {

	// remove item electric specifications
	$sth = $model->db->prepare("
		DELETE FROM item_electricspecs 
		WHERE item_electricspec_item_id = ?
	");

	$sth->execute(array($id));

	$sth = $model->db->prepare("
		INSERT INTO item_electricspecs (
			item_electricspec_item_id,
			item_electricspec_name,
			item_electricspec_description,
			user_created
		)
		VALUES (?,?,?,?)
	");

	foreach ($_REQUEST['item_electricspec_name'] as $i => $value) {

		$name = $value;
		$description = $_REQUEST['item_electricspec_description'][$i];
		
		if (!$name && !$description) {
			$removeFields['#itemElectricalList'][] = $i;
			continue;
		}

		$sth->execute(array($id, $name, $description, $user->login));
	}
}

// item product lines
if ($id && $response && in_array('item_projecttype_productline', $fields)) {
	
	// pos area locations
	$model->db->exec("
		DELETE FROM item_pos_types 
		WHERE item_pos_type_item = $id
	");

	if ($_REQUEST['project_type_product_line']) {

		$sth = $model->db->prepare("
			INSERT INTO item_pos_types (
				item_pos_type_pos_type,
				item_pos_type_product_line,
				item_pos_type_item,
				user_created,
				date_created
			)
			VALUES (:type, :line, :item, :user, NOW())
		");
		
		foreach ($_REQUEST['project_type_product_line'] as $key => $value) {
			
			$key = explode('-', $key);

			$sth->execute(array(
				'type' => $key[1],
				'line' => $key[0],
				'item' => $id,
				'user' => $user->login
			));
		}
	}

	if (!$message) {
		$response = true;
		$translate->message_request_submitted;
	}
}

// update orders data
if ($id && $response) {

	$sth = $model->db->prepare("
		UPDATE order_items SET 
			order_item_unit_id = ?,
			order_item_width = ?,
			order_item_height = ?,
			order_item_length = ?,
			order_item_gross_weight = ?,
			order_item_packaging_type_id = ?,
			order_item_stackable = ?
		WHERE order_item_item = ?
	");
	
	$sth->execute(array(
		$item->unit, 
		$item->width, 
		$item->height, 
		$item->length, 
		$item->gross_weight, 
		$item->packaging_type, 
		$item->stackable, 
		$item->id
	)); 
}


BLOCK_RESPONSE:

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'errors' => $errors,
	'redirect' => $redirect,
	'data' => $data,
	'fields' => $updateFields,
	'remove' => $removeFields
));
	