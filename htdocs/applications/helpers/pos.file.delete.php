<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$appliction = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$posFile = new Pos_File(Connector::DB_CORE);
$posFile->read($id);

if ($posFile->id && (user::permission(Pos::PERMISSION_EDIT) OR user::permission(Pos::PERMISSION_EDIT_LIMITED)) ) {
	
	$pos = $posFile->posaddress;

	$delete = $posFile->delete();
	
	if ($delete) {
		file::remove($posFile->path);			
		Message::request_deleted();
		url::redirect("/$appliction/$controller/$action/$pos");
	} else {
		Message::request_failure();
		url::redirect("/$appliction/$controller/$action/$pos/$id");
	}
} else {
	Message::request_failure();
	url::redirect("/$appliction/$controller");
}