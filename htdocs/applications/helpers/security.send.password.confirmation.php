<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	require_once PATH_LIBRARIES.'phpmailer/class.phpmailer.php';
	
	// confirmation link
	$md5 = md5(date("Y-m-d H:i:s")."_PasswordReset__");

	// project settings
	$settings = Settings::init();
	
	// user
	$user = new User($_REQUEST['id']);
	
	// template
	$mail_template = new Mail_Template(Connector::DB_CORE);
	$mail_template->read_from_shortcut('password.forgotten');
	
	// subject
	$subject = $mail_template->subject;
	
	$user->data['link'] = Settings::init()->http_protocol."://".$_SERVER['SERVER_NAME']."/security/reset/$md5";
	
	// content
	$content = content::render($mail_template->text, $user->data);
	
	// recipient
	$recipient = ucfirst($user->firstname).' '.ucfirst($user->name);

	$mail = new PHPMailer();
	$mail->SetFrom($settings->mail_noreply, $settings->project_name);
	$mail->AddAddress($user->email, $recipient);
	$mail->Subject = $subject;
	$mail->Body = $content;
	
	$response = $mail->Send();

	
	if ($response) {
		$user->resetPassword($md5);
		Message::password_reset_confirmation();
		$response = true;
	}
	else {
		$translate = Translate::instance();
		$response = false;
		$message = $translate->message_sendmail_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	