<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$appliction = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$localWorkGroup = new Local_Work_Group();
$localWorkGroup->read($id);

if ($localWorkGroup->id && user::permission('can_edit_catalog')) {
	
	$delete = $localWorkGroup->delete();
	
	if ($delete) {
		Message::request_deleted();
		url::redirect("/$appliction/$controller");
	} else {
		Message::request_failure();
		url::redirect("/$appliction/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$appliction/$controller");
}