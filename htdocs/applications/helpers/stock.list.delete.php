<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	if ($id && ($user->permission(Stocklist::PERMISSION_EDIT) OR $user->permission(Stocklist::PERMISSION_EDIT_LIMITED))) {
		
		$stocklist = new Stocklist();
		$stocklist->read($id);
		
		$delete = $stocklist->delete();
		
		if ($delete) {
			Message::request_deleted();
			url::redirect("/$application/$controller");
		} else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$id");
		}
	}
	else {
		Message::request_failure();
		url::redirect("/$application/$controller");
	}