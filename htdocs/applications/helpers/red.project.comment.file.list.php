<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$project_id = $_REQUEST['project_id'];
	$comment_id = $_REQUEST['comment_id'];
	$view = $_REQUEST['view'];
	$application = $_REQUEST['application'];
	
	$model = new Model($application);
	/*
	$model = new Model($application);

	// Get files allocated to this comment
	$files = $model->query("
	  SELECT SQL_CALC_FOUND_ROWS
	   red_file_id,
	   red_file_project_id,
	   red_file_category_id,
	   red_filecategory_name,
	   red_file_owner_user_id,
	   red_file_title,
	   red_file_path,
	   user_firstname,
	   user_name,
	   LEFT(red_file_description,120) AS red_file_description,
	   DATE_FORMAT(red_files.date_created, '%d.%m.%Y') AS date_created
	  FROM red_files
	 ")
	->bind(Red_file::BIND_USER_ID)
	->bind(Red_file::BIND_FILE_CATEGORY)
	->bind('INNER JOIN db_retailnet_red.red_comments ON red_file_id IN ('.$comment_id.')')
	->filter('red_file_id', 'red_file_project_id='.$project_id)
	->order('red_files.date_created', 'DESC')
	->fetchAll();
	*/
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS
			red_file_id,
			red_file_project_id,
			red_file_category_id,
			red_filecategory_name,
			red_file_owner_user_id,
			red_file_title,
			red_file_path,
			user_firstname,
			user_name,
			LEFT(red_file_description,120) AS red_file_description,
			DATE_FORMAT(red_files.date_created, '%d.%m.%Y') AS date_created
		FROM red_files
	")
	->bind(Red_file::BIND_USER_ID)
	->bind(Red_file::BIND_FILE_CATEGORY)
	->bind('file_ids', 'INNER JOIN db_retailnet_red.red_comments ON red_file_id IN ('.$comment_id.')')
	->filter('red_file_id', 'red_file_project_id='.$project_id)
	->order('red_files.date_created', 'DESC')
	->fetchAll();

	//Limited view mode (only his)
	if ($view == 'limited' && is_array($files)) {
		$file = new Red_file('red');
		foreach ($files as $row) {
			$file->read($row[red_file_id]);
			if ($file->user_has_access($user->id)) {

				$filtered_files[] = $row;
			}
		}
		$files = $filtered_files;
	}

	if ($files) {
		foreach ($files as $row) {
			extract($row);
			$file_extension = file::extension($red_file_path);
			$datagrid[$red_file_category_id]['caption'] = $red_filecategory_name;
			$datagrid[$red_file_category_id]['files'][$red_file_id]['title'] = "$red_file_title<span>".nl2br($red_file_description)."</span>";
			$datagrid[$red_file_category_id]['files'][$red_file_id]['date']= "<span>$user_name $user_firstname<br />$date_created</span>";
			$datagrid[$red_file_category_id]['files'][$red_file_id]['path'] = $red_file_path;
			$datagrid[$red_file_category_id]['files'][$red_file_id]['fileicon']  = "<span class='-file-extension $file_extension modal' tag='$red_file_path'></span>";
		}
	}

	if ($datagrid) {

		foreach ($datagrid as $key => $row) {

			$table = new Table(array(
				'thead' => false
			));

			$table->datagrid = $row['files'];
			$table->fileicon();
			$table->title("href=/$application/projects/files/$project_id/edit");
			$table->date();

			$render_table .= "<h6>".$row['caption']."</h6>";
			$render_table .= $table->render();
		}
	}

	echo $render_table;

