<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$user = User::instance();
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$company = new Company(Connector::DB_CORE);
	$company->read($id);

	if ($company->id && $user->permission(Red_Project::PERMISSION_DELETE_DATA)) {

		$integrity = new Integrity();
		$integrity->set($id, 'addresses');
		
		if ($integrity->check() && $company->type==12) {
			
			$delete = $company->delete();
			
			if ($delete) {
				Message::request_deleted();
				url::redirect("/$application/$controller");
			} else {
				Message::request_failure();
				url::redirect("/$application/$controller/$action/$id");
			}
		}
		else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$id");
		}
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller");
	}