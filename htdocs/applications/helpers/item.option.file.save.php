<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

// url vars
$application = $_REQUEST['application'];
$file = $_REQUEST['item_option_file_id'];
$option = $_REQUEST['item_option_file_option'];

$upload_path = "/files/items-options/$option";

$data = array();

if (in_array('item_option_file_title', $fields)) {
	$data['item_option_file_title'] = $_REQUEST['item_option_file_title'];
}

if (in_array('item_option_file_description', $fields)) {
	$data['item_option_file_description'] = $_REQUEST['item_option_file_description'];
}

if (in_array('item_option_file_purpose', $fields)) {
	$data['item_option_file_purpose'] = $_REQUEST['item_option_file_purpose'];
}

if ($_REQUEST['has_upload'] && $_REQUEST['item_option_file_path']) {
	
	$path = upload::move($_REQUEST['item_option_file_path'], $upload_path);
	$data['item_option_file_path'] = $path;

	$extension = file::extension($path);
	$data['item_option_file_type'] = File_Type::get_id_from_extension($extension);
}


if($option && $data) { 
	
	$data['item_option_file_option'] = $option;
	
	$itemOptionFile = new Item_Option_File();
	$itemOptionFile->read($file);

	if ($itemOptionFile->id) {
		
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		
		$response = $itemOptionFile->update($data);
		
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		
		$response = $id = $itemOptionFile->create($data);
		
		$message = ($response) ? $translate->message_request_inserted : $translate->message_request_failure;
	}	
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}

header('Content-Type: text/json');
echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'path' => $path
));
