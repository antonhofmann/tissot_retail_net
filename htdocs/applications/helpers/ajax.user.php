<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$auth = User::instance();
	$user = new User($_REQUEST['id']);
	$translate = Translate::instance();
	
	if ($user->id || $_REQUEST['role']) {
		
		switch ($_REQUEST['section']) {
			
			case 'header':
				$json['header'] = $user->name.', '.$user->firstname;
			break;
			
			case 'header-roles':
				
				$model = new Model();
				
				$result = $model->query("
					SELECT role_name
					FROM roles
					INNER JOIN user_roles ON user_role_role = role_id
					WHERE user_role_user = $user->id
				")->fetchAll();
				
				if ($result) {
					foreach ($result as $row) {
						$roles[] = $row['role_name'];
					}
				}
				
				$json['header'] = ($roles) ? join(', ', $roles) : null;
					
			break;
			
			case 'role-responsibile':
				
				$model = new Model();
				
				// get user roles
				$roles = $user->roles($_REQUEST['id']);
				$roles = join(',', $roles);
				
				$result = $model->query("
					SELECT DISTINCT
						user_id,
						user_firstname,
						user_name,
						user_email,
						role_application
					FROM roles
					INNER JOIN users ON role_responsible_user_id = user_id
					WHERE role_id IN ($roles)
				")->fetchAll();
				
				if (!$result) {
					
					$result = $model->query("
						SELECT DISTINCT
							user_id,
							user_firstname,
							user_name,
							user_email
						FROM users
						INNER JOIN user_roles ON user_role_user = user_id
						WHERE user_role_role = 1 AND user_active = 1
					")->fetchAll();
				}
				
				if ($result) {
					
					foreach ($result as $row) {
	
						if (!$responsible) {
							$responsible = $row;
						}
						
						if (!$retailnet && $row['role_application'] == 'retailnet') {
							$retailnet = $row;
							break;
						}
					}
					
					$data = ($retailnet) ? $retailnet : $responsible;
					
					foreach ($data as $key => $value) {
						$key = str_replace('user_', 'recipient_', $key);
						$json[$key] = $value;
					}
				}
				
			break;
			
			case 'role-responsibile-role':
				
				$model = new Model();
				
				$json = $model->query("
					SELECT DISTINCT
						user_id,
						user_firstname,
						user_name,
						user_email,
						role_application
					FROM roles
					INNER JOIN users ON role_responsible_user_id = user_id
					WHERE role_id = ".$_REQUEST['role']."
				")->fetch();
				
			break;
			
			case 'user-roles':
				
				$roles = $user->roles($_REQUEST['id']);
				
				$json = array(
					'response' => true,
					'roles' => ($roles) ? serialize($roles) : null
				);
				
			break;
			
			case 'sender':
				
				$data = $auth->data;
				
				if ($data) {
					
					foreach ($data as $key => $value) {
						$key = str_replace('user_', 'sender_', $key);
						$json[$key] = $value;
					}
				}
				
			break;
			
			default:
				
				$json = $user->data;
				
			break;
		}
	}
	
	if (is_array($json)) {
		header('Content-Type: text/json');
		echo json_encode($json);
	}
	