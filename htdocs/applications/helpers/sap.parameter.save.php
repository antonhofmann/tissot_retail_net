<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$user = User::instance();
	$translate = Translate::instance();
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$id = $_REQUEST['sap_systemparameter_id'];
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$data = array();
	
	if ($_REQUEST['sap_systemparameter_name']) {
		$data['sap_systemparameter_name'] = $_REQUEST['sap_systemparameter_name'];
	}
	
	if ($_REQUEST['sap_systemparameter_key']) {
		$data['sap_systemparameter_key'] = $_REQUEST['sap_systemparameter_key'];
	}
	
	if ($_REQUEST['sap_systemparameter_order_reason']) {
		$data['sap_systemparameter_order_reason'] = $_REQUEST['sap_systemparameter_order_reason'];
	}
	
	
	if ($_REQUEST['sap_systemparameter_order_type']) {
		$data['sap_systemparameter_order_type'] = $_REQUEST['sap_systemparameter_order_type'];
	}
	
	if ($_REQUEST['sap_systemparameter_distributor_id']) {
		$data['sap_systemparameter_distributor_id'] = $_REQUEST['sap_systemparameter_distributor_id'];
	}
	
	if ($_REQUEST['sap_systemparameter_division_name']) {
		$data['sap_systemparameter_division_name'] = $_REQUEST['sap_systemparameter_division_name'];
	}
	
	if ($_REQUEST['sap_systemparameter_division_code']) {
		$data['sap_systemparameter_division_code'] = $_REQUEST['sap_systemparameter_division_code'];
	}
	
	if ($_REQUEST['sap_systemparameter_dicount_code']) {
		$data['sap_systemparameter_dicount_code'] = $_REQUEST['sap_systemparameter_dicount_code'];
	}
	
	if ($_REQUEST['sap_systemparameter_dicount_percentage']) {
		$data['sap_systemparameter_dicount_percentage'] = $_REQUEST['sap_systemparameter_dicount_percentage'];
	}
	
	if ($data) {
	
		$sapParameter = new SAP_Parameter();
		$sapParameter->read($id);
	
		if ($sapParameter->id) {
			
			$data['sap_systemparameter_user_modified'] = $user->login;
			
			$response = $sapParameter->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['sap_systemparameter_user_created'] = $user->login;
			$data['sap_systemparameter_date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $sapParameter->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			if ($_REQUEST['redirect']) {
				$redirect = $_REQUEST['redirect']."/$id";
			}
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	