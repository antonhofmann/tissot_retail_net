<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$project = $_REQUEST['project'];
	$partner = $_REQUEST['partner'];
	
	$projectPartner = new Red_Project_Partner($application);
	$projectPartner->read($partner);

	if ($projectPartner->id && user::permission(Red_Project::PERMISSION_DELETE_DATA)) {

		$integrity = new Integrity();
		$integrity->set($partner, 'red_project_partners', $application);

		if ($integrity->check()) {

			$delete = $projectPartner->delete();

			if ($delete) {
				Message::request_deleted();
				url::redirect("/$application/$controller/$action/$project");
			} else {
				Message::request_failure();
				url::redirect("/$application/$controller/$action/$project/$partner");
			}
		}
		else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$project/$partner");
		}
	} else {
		Message::access_denied();
		url::redirect("/$application/$controller");
	}