<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", false);
	
	$model = new Model(Connector::DB_CORE);

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'language_name';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// sql pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$offset = ($page-1) * $settings->limit_pager_rows;
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "( 
			language_name LIKE \"%$keyword%\" 
		)";
	}
	
	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS
			language_id,
			language_name
		FROM languages
	")
	->filter($filters)
	->order($sort, $direction)
	->offset($offset)
	->fetchAll();

	if ($result) {
	
		$count = 0;
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		
		foreach ($datagrid as $key => $row) {
			
			$result = $model->query("
				SELECT application_language_id 
				FROM application_languages
				WHERE application_language_application = $id AND application_language_language = $key
			")->fetchAll();
			
			$dataloader['language'][$key] = ($result) ? 1 : 0;
			$datagrid[$key]['language'] = $key;
			if ($result) $count++;
		}
		
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
		
		$checkall = ($settings->limit_pager_rows == $count) ? 'checked=checked' : null;
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";

	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->caption('language', "<input type=checkbox class=checkall $checkall />");
	
	$table->datagrid = $datagrid;
	$table->dataloader($dataloader);
	
	$table->language_name(
		Table::ATTRIBUTE_NOWRAP
	);
	
	$table->language(
		Table::DATA_TYPE_CHECKBOX,
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=1%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	