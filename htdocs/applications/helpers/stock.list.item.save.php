<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['scpps_stocklist_id'];
	
	// editabled stocklist items
	$editabled_items = ($_REQUEST['editabled_fields']) ? explode(',', $_REQUEST['editabled_fields']) : array();
	
	// selected items
	$selected_items = $_REQUEST['items'];

	// stock list
	$stocklist = new Stocklist();
	$stocklist->read($id);
	
	// can edit stock list
	$can_edit = ($user->permission(Stocklist::PERMISSION_EDIT) || $user->permission(Stocklist::PERMISSION_EDIT_LIMITED)) ? true : false;
	
	if ($stocklist->id && $editabled_items) {

		// remove stocklist items
		foreach ($editabled_items as $item) {
			$stocklist->item->delete($item);				
		}

		// insert new items
		if ($selected_items) {
			foreach ($selected_items as $item => $value) {
				$stocklist->item->create($item);
			}
		}
		
		$response = true;
		$message = $translate->message_request_updated;
		
	} else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));