<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
require_once PATH_LIBRARIES."phpexcel/Classes/PHPExcel.php";
require_once(PATH_LIBRARIES.'phpexcel/Classes/PHPExcel/Writer/Excel2007.php');

// execution time
ini_set('max_execution_time', 240);
ini_set('memory_limit', '1024M');

$fileName = 'references-'.date('Y-m-d');

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// db model   
$model = new Model($application);

$_FILTERS = array();

// role filter
if ($_REQUEST['roles']) {
	$roles = join(',', array_keys($_REQUEST['roles']));
	$_FILTERS['roles'] = "user_role_role IN ($roles) ";
}

// role filter
if ($_REQUEST['clienttypes']) {
	
	$clienttypes = join(',', array_keys($_REQUEST['clienttypes']));
	$_FILTERS['clienttypes'] = "address_client_type IN ($clienttypes) ";

	$result = $model->query("
		SELECT GROUP_CONCAT(client_type_code) AS caption
		FROM client_types
		WHERE client_type_id IN ($clienttypes)
	")->fetch();

	$_CAPTIONS[] = "Client Type(s): ".$result['caption'];
}

$_FILTERS['active'] = "user_active = 1";

// get users
$users = $model->query("
	SELECT DISTINCT 
		country_name,
		user_id,
		CONCAT(user_name, ' ', user_firstname) AS user, 
		user_email, 
		address_type_name, 
		address_company,
		(
			SELECT  GROUP_CONCAT(user_role_role)
            FROM user_roles
            WHERE user_role_user = user_id
        ) AS roles
	FROM users
	LEFT JOIN user_roles on user_role_user = user_id
	LEFT JOIN addresses on address_id = user_address
	LEFT JOIN countries on country_id = address_country
	LEFT JOIN address_types on address_type_id = address_type

")
->filter($_FILTERS)
->order('country_name, address_type_name, address_company, user_name, user_firstname')
->fetchAll();


if ($_REQUEST['roles']) {
	$roles = join(',', array_keys($_REQUEST['roles']));
	$filterRoles['roles'] = "role_id IN ($roles) ";
}

// get roles
$roles = $model->query("
	SELECT role_id, role_name
	FROM roles
")
->filter($filterRoles)
->order('role_name')
->fetchAll();

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator($settings->project_name)->setLastModifiedBy($settings->project_name) ->setTitle($sheetName);
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

$_STYLES['borders'] = array(
	'borders' => array( 
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);

$_STYLES['align-left'] = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
	)
);

$_STYLES['align-center'] = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_CENTER
	)
);

$_STYLES['caption'] = array(
	'font' => array(
		'name'=>'Arial',
		'size'=> 12
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical'=> PHPExcel_Style_Alignment::VERTICAL_BOTTOM,
		'wrap'=> true
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb'=>'efefef')
	)
);


$_TOTAL_COLUMNS = 5+count($roles);
$_LAST_CEL_INDEX = spreadsheet::key($_TOTAL_COLUMNS);

// column dimensions
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);	// country name
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);	// client type
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);	// company
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);	// user name
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);	// email


// sheet header ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$row=1; $col=0;
$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(40);
$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col, $row, $_TOTAL_COLUMNS-1, $row);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "User and Roles");
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getFont()->setSize(20);

// print date
$row++;
$objPHPExcel->getActiveSheet()->mergeCells("A$row:$_LAST_CEL_INDEX$row");
$objPHPExcel->getActiveSheet()->setCellValue("A$row", "Date: ".date('d.m.Y  H:i:s'));

// separator
$row++;
$range = "A$row:".$_LAST_CEL_INDEX.$row;
$objPHPExcel->getActiveSheet()->mergeCells($range);

// filter captions
if ($_CAPTIONS) {

	foreach ($_CAPTIONS as $caption) {
		$row++;
		$objPHPExcel->getActiveSheet()->mergeCells("A$row:$_LAST_CEL_INDEX$row");
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", $caption);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setSize(11);
	}
}

// role captions
$row++;
$col = 0;

$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(140);
$objPHPExcel->getActiveSheet()->mergeCells("A$row:E$row");

$col=4;

foreach ($roles as $role) {
	$col++;
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $role['role_name']);
	$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setTextRotation(90);
	$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setWrapText(true);
}

$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':'.$_LAST_CEL_INDEX.$row)->applyFromArray($_STYLES['caption'], false);
$objPHPExcel->getActiveSheet()->getStyle('F'.$row.':'.$_LAST_CEL_INDEX.$row)->applyFromArray($_STYLES['borders'], false);

$rowStart = $row+1;

foreach ($users as $user) {
	
	$row++; 

	// country name
	$col=0;
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $user['country_name']);

	// client type
	$col++;
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $user['address_type_name']);

	// client type
	$col++;
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $user['address_company']);
	
	// user
	$col++;
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $user['user']);
	
	// user email
	$col++;
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $user['user_email']);

	$userRoles = $user['roles'] ? explode(',', $user['roles']) : array();

	foreach ($roles as $role) {

		$col++;
		
		if (in_array($role['role_id'], $userRoles)) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, 'x');
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($_STYLES['align-center'], false);
		}
	}
}

$objPHPExcel->getActiveSheet()->getStyle('A'.$rowStart.':'.$_LAST_CEL_INDEX.$row)->applyFromArray($_STYLES['borders'], false);


/*
foreach ($roles as $value) {
	$index = spreadsheet::index($col, $row);
	$objPHPExcel->getActiveSheet()->getStyle($index)->applyFromArray($styles['header']);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row, $translate->$value);
	$col++;
}

foreach ($datagrid as $i => $array) {
	
	$row++; $col = 0;
	
	foreach($array as $key => $value) {
		$index = spreadsheet::index($col, $row);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
		$col++;
    }

    $objPHPExcel->getActiveSheet()->getStyle("A$row:$lastColumn$row")->applyFromArray($styles['borders']);
}

$row++;
$objPHPExcel->getActiveSheet()->mergeCells("A$row:$lastColumn$row");

$objPHPExcel->getActiveSheet()->setTitle($sheetName);
$objPHPExcel->setActiveSheetIndex(0);
*/


if (!$_ERRORS) {

	$_SUCCESS = true;
	$stamp = $_SERVER['REQUEST_TIME'];
	$file = "/data/tmp/print.user.roles.$stamp.xlsx";
	$hash = Encryption::url($file);
	$_FILE_URL = "/download/tmp/$hash";
	
	// set active sheet
	$objPHPExcel->getActiveSheet()->setTitle('Users and Roles');
	$objPHPExcel->setActiveSheetIndex(0);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save($_SERVER['DOCUMENT_ROOT'].$file);
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


ob_end_clean();

header('Content-Type: text/json');
echo json_encode(array(
	'success' => $_SUCCESS,
	'file' => $_FILE_URL
));
	