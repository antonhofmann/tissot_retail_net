<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$template = $_REQUEST['template'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true); 
	
	$model = new Model(Connector::DB_CORE); 
	
	// list order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "mail_template_filter_name";
	
	// order direction
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	
	// pger offset
	$offset = ($page-1) * $settings->limit_pager_rows;

	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			mail_template_filter_name LIKE \"%$keyword%\"
			OR mail_template_filter_description LIKE \"%$keyword%\"
		)";
	}
	
	$filters['default'] = "mail_template_filter_template_id = $template";
	
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			mail_template_filter_id AS id,
			mail_template_filter_name AS name,
			mail_template_filter_description AS description
		FROM mail_template_filters
	")
	->filter($filters)
	->order($sort, $direction)
	->offset($offset)
	->fetchAll();
	
	if ($result) { 
		
		$totalrows = $model->totalRows();
		
		foreach ($result as $row) {
			
			$key = $row['id'];
			$name = $row['name'];
			$description = $row['description'];
			
			$datagrid[$key]['mail_template_filter_name'] = "
				<a href='/$application/$controller/$action/$template/$key' >$name</a>
				<span>$description</span>
			";
		}
		
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));
		
		$pageIndex = $pager->index();
		$pageControlls = $pager->controlls();
	}
	
	// toolbox: hide utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	
	// toolbox: button add
	if ($_REQUEST['add']) {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();

	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array(
			'column' => $sort,
			'direction' => $direction
		)
	));
	
	$table->datagrid = $datagrid;
	
	$table->mail_template_filter_name(
		Table::PARAM_SORT,
		'valign=top'
	);

	$table->footer($pageIndex);
	$table->footer($pageControlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	