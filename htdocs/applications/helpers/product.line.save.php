<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$db = Connector::get(Connector::DB_CORE);
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	// product line
	$id = $_REQUEST['product_line_id'];
	
	$data = array();
	
	if (in_array('product_line_name', $fields)) {
		$data['product_line_name'] = $_REQUEST['product_line_name'];
	}
	
	if (in_array('product_line_description', $fields)) {
		$data['product_line_description'] = $_REQUEST['product_line_description'];
	}
	
	if (in_array('product_line_visible', $fields)) {
		$data['product_line_visible'] = ($_REQUEST['product_line_visible']) ? 1 : 0;
	}
	
	if (in_array('product_line_budget', $fields)) {
		$data['product_line_budget'] = ($_REQUEST['product_line_budget']) ? 1 : 0;
	}
		
	if (in_array('product_line_clients', $fields)) {
		$data['product_line_clients'] = ($_REQUEST['product_line_clients']) ? 1 : 0;
	}
		
	if (in_array('product_line_posindex', $fields)) {
		$data['product_line_posindex'] = ($_REQUEST['product_line_posindex']) ? 1 : 0;
	}

	if (in_array('product_line_mis', $fields)) {
		$data['product_line_mis'] = ($_REQUEST['product_line_mis']) ? 1 : 0;
	}
	
	if (in_array('product_line_active', $fields)) {
		$data['product_line_active'] = ($_REQUEST['product_line_active']) ? 1 : 0;
	}
	
	if ($data) {
	
		$productLine = new Product_Line();
		$productLine->read($id);
	
		if ($productLine->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $productLine->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $productLine->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}


	// product line regions
	if( $id && in_array('regions', $fields)) {
		
		// pos area locations
		$db->exec("
			DELETE FROM productline_regions 
			WHERE productline_region_productline = $id
		");
	
		if ($_REQUEST['regions']) {

			$sth = $db->prepare("
				INSERT INTO productline_regions (
					productline_region_productline,
					productline_region_region,
					user_created,
					date_created
				)
				VALUES (:line, :region, :user, NOW() )
			");
			
			foreach ($_REQUEST['regions'] as $region => $value) {
				$sth->execute(array(
					'line' => $id,
					'region' => $region,
					'user' => $user->login
				));
			}
		}

		if (!$message) {
			$response = true;
			$translate->message_request_submitted;
		}
	}

	if ($id && $response && in_array('product_line_supplying_group', $fields)) {

		// pos area locations
		$db->exec("
			DELETE FROM product_line_supplying_groups 
			WHERE product_line_supplying_group_line_id = $id
		");
	
		if ($_REQUEST['product_line_supplying_group']) {

			$sth = $db->prepare("
				INSERT INTO product_line_supplying_groups (
					product_line_supplying_group_line_id,
					product_line_supplying_group_group_id,
					user_created
				)
				VALUES (?, ?, ?)
			");
			
			foreach ($_REQUEST['product_line_supplying_group'] as $group => $checked) {
				$sth->execute(array($id, $group, $user->login));
			}
		}

	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	