<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$user = User::instance();
	$translate = Translate::instance();
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$id = $_REQUEST['sap_blockcode_id'];
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$data = array();
	
	if ($_REQUEST['sap_blockcode_code']) {
		$data['sap_blockcode_code'] = $_REQUEST['sap_blockcode_code'];
	}
	
	if ($_REQUEST['sap_blockcode_text']) {
		$data['sap_blockcode_text'] = $_REQUEST['sap_blockcode_text'];
	}
	
	if ($data) {
	
		$sapBlockCode = new SapBlockCode();
		$sapBlockCode->read($id);
	
		if ($sapBlockCode->id) {
			
			$data['user_modified'] = $user->login;
			
			$response = $sapBlockCode->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $sapBlockCode->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			if ($_REQUEST['redirect']) {
				$redirect = $_REQUEST['redirect']."/$id";
			}
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	