<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$path = $_REQUEST['file'];

// send $file to browser
$finfo = finfo_open(FILEINFO_MIME_TYPE);
$mimeType = finfo_file($finfo, $path);
$size = filesize($path);
$name = basename($path);

ignore_user_abort(true);

header('Cache-Control: private, max-age=120, must-revalidate');
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
header("Content-Type: " . $mimeType);
header('Content-Disposition: attachment; filename="' . $name . '";');
header("Accept-Ranges: bytes");
header('Content-Length: ' . $size);

$context = stream_context_create();
$file = fopen($path, 'rb', FALSE, $context);

while(!feof($file)) {
    echo stream_get_contents($file, 2014);
}

fclose($file);
flush();

if (file_exists($path)) {
    unlink( $path );
}

exit;