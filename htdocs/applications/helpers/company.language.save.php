<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$id = $_REQUEST['id'];
	$language = $_REQUEST['language'];
	
	if ($id && $language) {
		
		$company_language = new Company_Language();
		$company_language->id($id);
		
		if ($_REQUEST['checked']) {
			$response = $company_language->create($language);
			$message = ($response) ? $translate->message_request_inserted : $translate->message_request_failure;
		}
		else {
			$response = $company_language->delete($language);
			$message = ($response) ? $translate->message_request_deleted : $translate->message_request_failure;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));