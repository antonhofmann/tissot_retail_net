<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	if (array_key_exists('address_id', $_POST)) {
		
		$result = array();

		$db = Connector::get(Connector::DB_CORE);
		
		
		
		if(array_key_exists('freegoods', $_POST)) {

			$sth = $db->prepare("SELECT mps_sap_hq_ordertype_id,
					mps_sap_hq_ordertype_address_id ,
					mps_sap_hq_ordertype_customer_number ,
					mps_sap_hq_ordertype_shipto_number ,
					sap_hq_order_type_name ,
					sap_hq_order_reason_name ,
					mps_sap_hq_ordertype_is_default,
					mps_sap_hq_ordertype_is_free_goods 
				FROM db_retailnet.mps_sap_hq_ordertypes
				LEFT JOIN db_retailnet.sap_hq_order_types ON sap_hq_order_type_id = mps_sap_hq_ordertype_sap_hq_ordertype_id
				LEFT JOIN db_retailnet.sap_hq_order_reasons ON  sap_hq_order_reason_id = mps_sap_hq_ordertype_sap_hq_orderreason_id where mps_sap_hq_ordertype_address_id = ?
				and mps_sap_hq_ordertype_is_free_goods = 1");

			$sth->execute(array($_POST['address_id']));
		
		}
		elseif(array_key_exists('default', $_POST)) {

			$sth = $db->prepare("SELECT mps_sap_hq_ordertype_id,
					mps_sap_hq_ordertype_address_id ,
					mps_sap_hq_ordertype_customer_number ,
					mps_sap_hq_ordertype_shipto_number ,
					sap_hq_order_type_name ,
					sap_hq_order_reason_name ,
					mps_sap_hq_ordertype_is_default,
					mps_sap_hq_ordertype_is_free_goods 
				FROM db_retailnet.mps_sap_hq_ordertypes
				LEFT JOIN db_retailnet.sap_hq_order_types ON sap_hq_order_type_id = mps_sap_hq_ordertype_sap_hq_ordertype_id
				LEFT JOIN db_retailnet.sap_hq_order_reasons ON  sap_hq_order_reason_id = mps_sap_hq_ordertype_sap_hq_orderreason_id where mps_sap_hq_ordertype_address_id = ?
				and mps_sap_hq_ordertype_is_default = 1");

			$sth->execute(array($_POST['address_id']));
		
		}
		else {

			$sth = $db->prepare("SELECT mps_sap_hq_ordertype_id,
				mps_sap_hq_ordertype_address_id ,
				mps_sap_hq_ordertype_customer_number ,
				mps_sap_hq_ordertype_shipto_number ,
				sap_hq_order_type_name ,
				sap_hq_order_reason_name ,
				mps_sap_hq_ordertype_is_default,
				mps_sap_hq_ordertype_is_free_goods 
			FROM db_retailnet.mps_sap_hq_ordertypes
			LEFT JOIN db_retailnet.sap_hq_order_types ON sap_hq_order_type_id = mps_sap_hq_ordertype_sap_hq_ordertype_id
            LEFT JOIN db_retailnet.sap_hq_order_reasons ON  sap_hq_order_reason_id = mps_sap_hq_ordertype_sap_hq_orderreason_id where mps_sap_hq_ordertype_address_id = ?");

			$sth->execute(array($_POST['address_id']));
		}
		
		
		$result = $sth->fetchall();
		

		header('Content-Type: text/json');
		
		if(count(result) > 0) {
			echo json_encode($result);
		}
		else {
			echo '';
		}
	}

?>
	