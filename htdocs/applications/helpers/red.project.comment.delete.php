<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$project = $_REQUEST['project'];
	$comment = $_REQUEST['comment'];
	
	$projectComment = new Red_Comment($application);
	$projectComment->read($comment);
	
	if ($projectComment->id && user::permission(Red_Project::PERMISSION_DELETE_DATA)) {

		$integrity = new Integrity();
		$integrity->set($comment, 'red_comments', $application);

		if ($integrity->check()) {
			
			$files = unserialize($projectComment->file_id);
			$response = $projectComment->delete();
			
			if ($response) {
				
				$commentAccess = new Red_Comment_Access($application);
				$commentAccess->comment($comment);
				$commentAccess->deleteAll();

				if (is_array($files)) {
					
					$file = new Red_file($application);
					
					foreach ($files as $id) {
						
						$file->read($id);
						$file->delete_all_accesses();
						
						file::remove($file->path);
						
						$file->delete();
					}
				}
			}

			if ($response) {
				Message::request_deleted();
				url::redirect("/$application/$controller/$action/$project");
			} else {
				Message::request_failure();
				url::redirect("/$application/$controller/$action/$project/$comment");
			}
		}
		else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$project/$comment");
		}

	} else {
		Message::request_failure();
		url::redirect("/$application/$controller");
	}