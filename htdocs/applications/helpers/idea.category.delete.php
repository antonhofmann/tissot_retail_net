<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url request
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

if ($id && Idea::PERMISSION_EDIT) {
	
	$ideaCategory = new IdeaCategory();
	$ideaCategory->read($id);
	
	if ($ideaCategory->id) {
		
		$response = $ideaCategory->delete();
		
		if ($response) {
			
			Message::request_deleted();
			$redirect = "/$application/$controller";
			
		} else {
			
			$message = Translate::instance()->message_request_failure;
		}
		
	} else {
		
		$message = Translate::instance()->message_request_failure;
	}
}
else {
	
	$message = Translate::instance()->message_request_failure;
}

header('Content-Type: text/json');
echo json_encode(array(
	'response' => $response,
	'redirect' => $redirect,
	'message' => $message
));