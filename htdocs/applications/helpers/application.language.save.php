<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$language = $_REQUEST['language'];
	$application = $_REQUEST['application'];
	
	if ($language && $application) {
	
		$model = new Model(Connector::DB_CORE);
	
		if ($_REQUEST['action']) {
				
			$response = $model->db->query("
				INSERT INTO application_languages (
					application_language_language, 
					application_language_application, 
					user_created
				)
				VALUES ($language, $application, '".$user->login."' )
			");
				
			$message = ($response) ? $translate->message_request_inserted : $translate->message_request_failure;
		}
		else {
				
			$response = $model->db->query("
				DELETE FROM application_languages
				WHERE application_language_language = $language && application_language_application = $application
			");
						
			$message = ($response) ? $translate->message_request_deleted : $translate->message_request_failure;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));
	