<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

// request
$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
$fSearch = $_REQUEST['search'] && $_REQUEST['search']<>$translate->search ? $_REQUEST['search'] : null;
$fCountry = $_REQUEST['countries'];
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "country_name, address_company, mps_staff_name";
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

// database model
$model = new Model($application);
	
// bind list external sources
$binds = array(
	Staff::DB_BIND_COMPANIES,
	Staff::DB_BIND_STAFF_TYPES,
	Company::DB_BIND_COUNTRIES
);

// limited view
if( !$user->permission(Staff::PERMISSION_EDIT) && !$user->permission(Staff::PERMISSION_VIEW)) {

	// country access filter
	$countryAccess = User::getCountryAccess();
	$filterCountryAccess = $countryAccess ? " OR address_country IN ( $countryAccess )" : null;

	// regional access filter
	$regionalAccessCompanies = User::getRegionalAccessCompanies();
	$filterRegionalAccess = $regionalAccessCompanies ? " OR address_id IN (".join(',', $regionalAccessCompanies).") " : null;

	$filters['limited'] = "(
		mps_staff_address_id = $user->address
		$filterCountryAccess
		$filterRegionalAccess
	)";
}

// filter: countries
if ($fCountry) {
	$filters['countries'] = "address_country = $fCountry";
}

// filter: full text search
if ($fSearch) {
	$filters['search'] = "(
		country_name LIKE \"%$fSearch%\"
		OR address_company LIKE \"%$fSearch%\"
		OR mps_staff_type_name LIKE \"%$fSearch%\"
		OR mps_staff_name LIKE \"%$fSearch%\"
		OR mps_staff_firstname LIKE \"%$fSearch%\"
		OR mps_staff_email LIKE \"%$fSearch%\"
	)";
}

$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		mps_staff_id,
		country_name,
		address_company,
		mps_staff_type_name,
		mps_staff_name,
		mps_staff_firstname,
		mps_staff_email
	FROM mps_staffs
")
->bind($binds)
->filter($filters)
->order($sort, $direction)
->offset($offset, $rowsPerPage)
->fetchAll(); 
	
$totalrows = $model->totalRows();

if ($result) {

	$datagrid = _array::datagrid($result);

	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// toolbox: button print
if ($datagrid && $_REQUEST['export']) {
	$toolbox[] = ui::button(array(
		'id' => 'print',
		'icon' => 'print',
		'href' => $_REQUEST['export'],
		'label' => $translate->print
	));
}

// toolbox: add
if ($_REQUEST['add'])  {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'label' => $translate->add_new,
		'href' => $_REQUEST['add']
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();

// toolbox: countries
$result = $model->query("SELECT DISTINCT country_id, country_name FROM mps_staffs")
->bind($binds)->filter($filters)->exclude('countries')->order('country_name')->fetchAll();
	
if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'countries',
		'id' => 'countries',
		'class' => 'submit',
		'value' => $_REQUEST['countries'],
		'caption' => $translate->all_countries
	));
}



if ($toolbox) {
	$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
}

$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction
	)
));

$table->datagrid = $datagrid;

$table->country_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=15%'
);

$table->address_company(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=15%'
);

$table->mps_staff_type_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=15%'
);

$table->mps_staff_firstname(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	Table::DATA_TYPE_LINK,
	"href=".$_REQUEST['form']
);

$table->mps_staff_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	Table::DATA_TYPE_LINK,
	"href=".$_REQUEST['form']
);

$table->mps_staff_email(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=15%'
);

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;
