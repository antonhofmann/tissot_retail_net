<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	switch ($_REQUEST['section']) {
				
		case 'tables': 
			
			$json[][] = "Select";
			
			if ($_REQUEST['value']) {
				
				// get db data
				$db = new DB();
				$db->read($_REQUEST['value']);
				
				// load db tables
				$model = new Model($db->name);
				$sth = $model->db->query("show tables");
				$result = ($sth) ? $sth->fetchAll() : null;
			}
			
			if ($result) {
				
				// get registred tables
				$model = new Model(Connector::DB_CORE);
				$sth = $model->db->query("
					SELECT db_table_table, db_table_id 
					FROM db_tables
					WHERE db_table_db = ".$_REQUEST['value']." 
				");
					
				$tables = ($sth) ? $sth->fetchAll() : null;
				$tables = _array::extract($tables);
				
				// include current table
				if ($_REQUEST['id'] && !$_REQUEST['get_only_registred']) {
					$db_table = new DB_Table();
					$db_table->read($_REQUEST['id']);
					unset($tables[$db_table->table]);
				}
			
				foreach ($result as $row) {
					
					$key = key($row);
					$name = $row[$key];
					
					// on add new reference, get only registred tables
					if ($_REQUEST['get_only_registred']) {
						if ($tables[$name]) $json[][$name] = $name;
					}
					// on add new table, get only not registred tables
					else{
						if (!$tables[$name]) $json[][$name] = $name;
					}
				}
			}
				
		break;
		
		case 'fields':

			$json[][] = "Select";

			if ($_REQUEST['db'] && $_REQUEST['value']) {
				$db = new DB();
				$db->read($_REQUEST['db']);
				$model = new Model($db->name);
				$sth = $model->db->query("SHOW COLUMNS FROM ".$_REQUEST['value']." FROM ".$db->name);
				$result = ($sth) ? $sth->fetchAll() : null;
			}
				
			if ($result) {
				foreach ($result as $row) {
					$name = $row['Field'];
					$json[][$name] = $name;
				}
			}
		
		break;
		
		case 'primary_key':
		
			if ($_REQUEST['db'] && $_REQUEST['table']) {
				
				// get db data
				$db = new DB();
				$db->read($_REQUEST['db']);
				
				$model = new Model($db->name);
				$sth = $model->db->query("show index from ".$_REQUEST['table']." where Key_name = 'PRIMARY' ");
				$result = ($sth) ? $sth->fetch() : null;
				$json['key'] = $result['Column_name'];
			}
		
		break;
	}
	
	echo json_encode($json);
	