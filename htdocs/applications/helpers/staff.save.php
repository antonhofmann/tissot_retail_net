<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['mps_staff_id'];
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$data = array();
	
	if ($_REQUEST['mps_staff_address_id']) {
		$data['mps_staff_address_id'] = $_REQUEST['mps_staff_address_id'];
	}
	
	if ($_REQUEST['mps_staff_staff_type_id']) {
		$data['mps_staff_staff_type_id'] = $_REQUEST['mps_staff_staff_type_id'];
	}
	
	if ($_REQUEST['mps_staff_firstname']) {
		$data['mps_staff_firstname'] = $_REQUEST['mps_staff_firstname'];
	}
	
	if ($_REQUEST['mps_staff_name']) {
		$data['mps_staff_name'] = $_REQUEST['mps_staff_name'];
	}
	
	if ($_REQUEST['mps_staff_sex']) {
		$data['mps_staff_sex'] = $_REQUEST['mps_staff_sex'];
	}
	
	if(in_array('mps_staff_active', $fields)) {
		$data['mps_staff_active'] = ($_REQUEST['mps_staff_active']) ? 1 : 0;
	}
	
	if (isset($_REQUEST['mps_staff_email'])) {
		$data['mps_staff_email'] = $_REQUEST['mps_staff_email'];
	}
	
	if (isset($_REQUEST['mps_staff_phone'])) {
		$data['mps_staff_phone'] = $_REQUEST['mps_staff_phone'];
	}
	
	if ($data) {
		
		$staff = new Staff($application);
		$staff->read($id);
		
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $staff->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $staff->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));
	