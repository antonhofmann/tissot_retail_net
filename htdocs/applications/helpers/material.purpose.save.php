<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['mps_material_purpose_id'];
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	$data = array();
	
	if ($_REQUEST['mps_material_purpose_name']) {
		$data['mps_material_purpose_name'] = $_REQUEST['mps_material_purpose_name'];
	}
	
	if(in_array('mps_material_purpose_active',$fields)) {
		$data['mps_material_purpose_active'] =($_REQUEST['mps_material_purpose_active']) ? 1 : 0;
	}
	
	if ($data) {
		
		$material_purpose = new Material_Purpose($application);
		$material_purpose->read($id);
		
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $material_purpose->update($data);
			$message = ($action) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $material_purpose->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));