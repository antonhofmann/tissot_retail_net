<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];

	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	$model = new Model(Connector::DB_CORE);

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'mps_turnovergroup_name, mps_turnoverclass_code';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;

	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			mps_turnovergroup_name LIKE \"%$keyword%\"
			OR mps_turnoverclass_code LIKE \"%$keyword%\"
			OR mps_turnoverclass_name LIKE \"%$keyword%\"
		)";
	}

	// filter: turnover groups
	if ($_REQUEST['turnover_groups']) {
		$filters['turnover_groups'] = "mps_turnovergroup_id = ".$_REQUEST['turnover_groups'];
	}

	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			mps_turnoverclass_id,
			mps_turnovergroup_name,
			mps_turnoverclass_code,
			mps_turnoverclass_name,
			mps_turnoverclass_active
		FROM mps_turnoverclasses
	")
	->bind(TurnoverClass::DB_BIND_TURNOVER_GROUPS)
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();
	
	if ($result) {

		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		$icon_checked = ui::icon('checked');
		$icon_unchecked = ui::icon('unchecked');

		foreach ($datagrid as $key => $row) {
			$dataloader['mps_turnoverclass_active'][$key] = ($row['mps_turnoverclass_active']) ? $icon_checked : $icon_unchecked;
		}

		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));

		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}

	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

	// toolbox: button print
	if ($datagrid && $_REQUEST['print']) {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'href' => $_REQUEST['print'],
			'label' => $translate->print,
		));
	}
	
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}

	// toolbox: serach full text
	$toolbox[] = ui::searchbox();

	// toolbox: turnover groups
	$result = $model->query("
		SELECT DISTINCT mps_turnovergroup_id, mps_turnovergroup_name 
		FROM mps_turnoverclasses
	")
	->bind(TurnoverClass::DB_BIND_TURNOVER_GROUPS)
	->filter($filters)
	->exclude('turnover_groups')
	->order('mps_turnovergroup_name')
	->fetchAll();

	$toolbox[] = ui::dropdown($result, array(
		'id' => 'turnover_groups',
		'name' => 'turnover_groups',
		'class' => 'submit',
		'value' => $_REQUEST['turnover_groups'],
		'caption' => $translate->all_turnovergroups
	));

	

	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}

	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));

	$table->datagrid = $datagrid;
	$table->dataloader($dataloader);

	$table->mps_turnovergroup_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->mps_turnoverclass_code(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->mps_turnoverclass_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		"href=".$_REQUEST['form']
	);

	$table->mps_turnoverclass_active(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		Table::DATA_TYPE_IMAGE,
		'width=5%'
	);

	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();

	echo $toolbox.$table;
