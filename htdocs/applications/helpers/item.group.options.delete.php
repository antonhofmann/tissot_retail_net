<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$appliction = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$itemGroupOption = new Item_Group_Option();
$itemGroupOption->read($id);

$permission_edit = user::permission('can_edit_catalog');
$permission_edit_items = user::permission('can_edit_items_in_catalogue');
$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

if ($itemGroupOption->id && $_CAN_EDIT) {
	
	$group = $itemGroupOption->group;
	$delete = $itemGroupOption->delete();
	
	if ($delete) {

		$model = new Model();

		// remove materials
		$model->db->exec("
			DELETE FROM item_group_files
			WHERE item_group_file_group_option = $id
		");

		// remove files
		dir::remove("/files/items_group_option_files/$id");

		Message::request_deleted();
		url::redirect("/$appliction/$controller/$action/$group");

	} else { 
		Message::request_failure();
		url::redirect("/$appliction/$controller/$action/$group/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$appliction/$controller");
}