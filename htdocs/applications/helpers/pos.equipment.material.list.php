<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// permissions
	$permission_view = User::permission(Pos::PERMISSION_VIEW);
	$permission_view_limited = User::permission(Pos::PERMISSION_VIEW_LIMITED);
	$permission_edit = User::permission(Pos::PERMISSION_EDIT);
	$permission_edit_limited = User::permission(Pos::PERMISSION_EDIT_LIMITED);
	$permission_travelling = User::permission('has_access_to_all_travalling_retail_data');

	// has limited permission
	$hasLimitedPermission = (!$permission_view && !$permission_edit) ? true : false;

	// url vars
	$application = trim($_REQUEST['application']);
	$controller = trim($_REQUEST['controller']);
	$action = trim($_REQUEST['action']);
	$archived = trim($_REQUEST['archived']);

	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

	// list order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "mps_material_code, mps_material_purpose_name, mps_material_category_name";
	
	// order direction
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
	
	// list vars
	$search = $_REQUEST['search'];
	$categories = $_REQUEST['categories'];
	$collections = $_REQUEST['collections'];
	$collection_categories = $_REQUEST['collection_categories'];
	
	// db connector
	$model = new Model($application);
	
	// filter: search
	if ($search && $search <> $translate->search) {
		
		$filters['search'] = "(
			mps_material_collection_category_code LIKE '%$search%' 
			OR mps_material_collection_code LIKE '%$search%' 
			OR mps_material_category_name LIKE '%$search%' 
			OR mps_material_purpose_name LIKE '%$search%' 
			OR mps_material_code LIKE '%$search%' 
			OR mps_material_name LIKE '%$search%'
		)";
	}
	
	// filter: category
	if ($categories) {
		$filters['categories'] = "mps_material_material_category_id = $categories";
	}
	
	// filter: collections
	if ($collections) {
		$filters['collections'] = "mps_material_material_collection_id = $collections";
	}
	
	// filter: collection category
	if ($collection_categories) {
		$filters['collection_categories'] = "mps_material_material_collection_category_id = $collection_categories";
	}
	
	// filter: long term materials
	$filters['long_term'] = "mps_material_islongterm = 1";
	
	// filter: active materials
	$filters['active'] = "mps_material_active = 1";
	
	// filter: only opened shops
	$filters['opened'] = "
		posaddress_store_openingdate IS NOT NULL 
		AND posaddress_store_openingdate <> '0000-00-00'
		AND (
			posaddress_store_closingdate = '0000-00-00'
			OR posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = ''
		)
	";

	// binds
	$binds = array(
		Pos_Material::DB_BIND_POSADDRESSES,
		Pos_Material::DB_BIND_MATERIALS,
		'LEFT JOIN mps_material_categories ON mps_material_category_id = mps_material_material_category_id',
		'LEFT JOIN mps_material_purposes ON mps_material_purpose_id = mps_material_material_purpose_id',
		'LEFT JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id',
		'LEFT JOIN mps_material_collections ON mps_material_collection_id = mps_material_material_collection_id',
		'LEFT JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id'
	);

	// for limited view
	if ($hasLimitedPermission) {

		$countryAccess = User::getCountryAccess();

		// filter access countries
		$filterCountryAccess = $countryAccess ? " OR posaddress_country IN ($countryAccess)" : null;

		// regional access companies
		$regionalAccessCompanies = User::getRegionalAccessPos();
		$filterRegionalAccess = $regionalAccessCompanies ? " OR $regionalAccessCompanies " : null;

		// travelling
		if ($permission_travelling) { 
			$binds[] = "LEFT JOIN db_retailnet.posareas ON posarea_posaddress = posaddress_id";
			$travelling = " OR posarea_area IN (4,5) OR posaddress_store_subclass IN(17) "; 
		}

		$filters['limited'] = "(
			posaddress_client_id = $user->address
			$travelling
			$filterCountryAccess 
			$filterRegionalAccess
		)";
	}
	
	// datagrid
	$result  = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT 
			mps_material_id,
			mps_material_collection_category_code,
			mps_material_collection_code,
			mps_material_code,
			mps_material_name,
			mps_material_category_name,
			mps_material_purpose_name
		FROM mps_pos_materials
	")
	->bind($binds)
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();
	
	$totalrows = $model->totalRows();
	

	if ($result) {
	
		$datagrid = _array::datagrid($result);
		
		foreach ($datagrid as $key => $row) {
			$datagrid[$key]['mps_material_name'] = "<a id=$key href=/$application/$controller/materials/$key >".$row['mps_material_name']."</a>";
		}
	
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	// toolbox: collection categories
	$result = $model->query("
		SELECT DISTINCT 
			mps_material_collection_category_id,
			mps_material_collection_category_code
		FROM mps_pos_materials"
	)
	->bind($binds)
	->filter($filters)
	->filter('emptys', 'mps_material_collection_category_id > 0')
	->exclude('collection_categories')
	->order('mps_material_collection_category_code')
	->fetchAll();
	
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'collection_categories',
		'id' => 'collection_categories',
		'class' => 'submit',
		'value' => $collection_categories,
		'caption' => $translate->all_collection_categories
	));
	
	// toolbox: collections
	$result = $model->query("
		SELECT DISTINCT
			mps_material_collection_id,
			mps_material_collection_code
		FROM mps_pos_materials 
	")
	->bind($binds)
	->filter($filters)
	->filter('emptys', 'mps_material_collection_id > 0')
	->exclude('collections')
	->order('mps_material_collection_code')
	->fetchAll();
	
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'collections',
		'id' => 'collections',
		'class' => 'submit',
		'value' => $collections,
		'caption' => $translate->all_collections
	));
	
	// toolbox: categories
	$result = $model->query("
		SELECT DISTINCT
			mps_material_category_id, 
			mps_material_category_name 
		FROM mps_pos_materials 
	")
	->bind($binds)
	->filter($filters)
	->filter('emptys', 'mps_material_category_id > 0')
	->exclude('categories')
	->order('mps_material_category_name')
	->fetchAll();
	
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'categories',
		'id' => 'categories',
		'class' => 'submit',
		'value' => $categories,
		'caption' => $translate->all_categories
	));
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>
					".join($toolbox)."
				</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	$table->dataloader($dataloader);
	
	$table->mps_material_collection_category_code(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->mps_material_collection_code(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->mps_material_code(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=15%'
	);
	
	$table->mps_material_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);
	
	$table->mps_material_category_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->mps_material_purpose_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	
	