<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$id = $_REQUEST['id'];

$model = new Model($application);

$active = isset($_REQUEST['active']) ? $_REQUEST['active'] : 1;


$result = $model->query("
	SELECT 
		design_objective_group_id, 
		design_objective_group_name,
		IF(design_objective_group_multiple, 'Yes', 'No') AS design_objective_group_multiple
	FROM design_objective_groups 
	WHERE design_objective_group_product_line = $id AND design_objective_group_active = $active
	ORDER BY 
		design_objective_groups.design_objective_group_priority, 
		design_objective_groups.design_objective_group_name
")->fetchAll();

$datagrid = _array::datagrid($result);

$actives = array(
	1 => 'Active',
	0 => 'Inactive'
);

$toolbox[] = ui::dropdown($actives, array(
	'name' => 'active',
	'id' => 'active',
	'class' => 'submit',
	'value' => $active,
	'caption' => false
));


if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

$table = new Table();

$table->datagrid = $datagrid;

$table->design_objective_group_name(
	Table::ATTRIBUTE_NOWRAP
);

$table->design_objective_group_multiple(
	Table::ATTRIBUTE_NOWRAP,
	'width=10%'
);

echo $toolbox.$table->render();


