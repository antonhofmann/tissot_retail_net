<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$value = $_REQUEST['value'];
	
	switch ($_REQUEST['section']) {
		
		case 'get':
			
			$company = new Company();
			$company->read($_REQUEST['id']);
			$json = $company->data;
			
		break;
		
		case 'letter_cases':
			
			if(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $value)) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $value)) && strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $value)) > 10 ) {
				$error = true;
				$message = $translate->error_letter_cases(array(
					'modul' => $translate->company		
				));
			} else {
				$error = false;
			}
			
			$json = array(
				'response' => true,
				'error' => $error,
				'message' => $message
			);
			
		break;
		
		case 'get_currency_from_country':
			
			$model = new Model(Connector::DB_CORE);
			
			$result = $model
			->query("SELECT country_currency FROM countries")
			->filter('country', "country_id = $value")
			->fetch();
			
			if ($result) {
				$json = array(
					'response' => true,
					'currency' => $result['country_currency']		
				);
			}
			
		break;
		
		// get all companies selected from language
		case 'companies.language':
			
			$model = new Model(Connector::DB_CORE);
			
			$result = $model->query("
				SELECT DISTINCT
					address_id as id,
					address_company as name
				FROM address_languages
				INNER JOIN languages ON address_language_language = language_id 
				INNER JOIN addresses ON address_id = address_language_address
				WHERE address_active = 1 AND language_id = $value		
			")->fetchAll();
			
			$json['companies'] = _array::extract($result);
			
		break;

		// get all companies selected from hemisphere
		case 'companies.hemisphere':
				
			$model = new Model(Connector::DB_CORE);
			
			$result = $model->query("
				SELECT DISTINCT
					address_id as id,
					address_company as name
				FROM addresses
				INNER JOIN countries ON country_id = address_country
				WHERE address_active = 1 AND country_hemisphere_id = $value		
			")->fetchAll();
			
			$json['companies'] = _array::extract($result);
			
		break;
		
		// get all interne swatch companies
		case 'companies.regions':
		
			$model = new Model(Connector::DB_CORE);
			
			$result = $model->query("
				SELECT DISTINCT
					address_id,
					address_company
				FROM addresses
				INNER JOIN countries ON country_id = address_country
				WHERE address_active = 1 AND country_region = $value	
			")->fetchAll();
			
			$json['companies'] = _array::extract($result);
			
		break;
		
		// get all interne swatch companies
		case 'companies.intern':
		
			$model = new Model(Connector::DB_CORE);
			
			$result = $model->query("
				SELECT DISTINCT
					address_id,
					address_company
				FROM addresses
				WHERE address_active = 1 AND address_is_internal_address = 1
			")->fetchAll();
			
			$json['companies'] = _array::extract($result);
			
		break;
	}
	
	
	header('Content-Type: text/json');
	echo json_encode($json);
	