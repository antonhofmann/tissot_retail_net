<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$period = $_REQUEST['periodes'];
$region = $_REQUEST['regions'];
$addresstype = $_REQUEST['addresstypes'];
$client = $_REQUEST['clients'];
$selected = $_REQUEST['selected'];

// set template as integer
$template = $_REQUEST['mail_template_id'];


if ($template) { 
	
	settype($template, "integer");
		
	// action mail
	$actionmail = new ActionMail($template);
	
	// assign filters 
	$actionmail->param()->application = $application;
	$actionmail->param()->period = $period;
	$actionmail->param()->region = $region;
	$actionmail->param()->addresstype = $addresstype;
	$actionmail->param()->client = $client;
	$actionmail->param()->selected = $selected;
	
	// set mail template data
	if ($_REQUEST['mail_template_view_modal']) {
		$actionmail->content()->subject = $_REQUEST['mail_template_subject'];
		$actionmail->content()->body = $_REQUEST['mail_template_text'];
	}

	// test mail
	if ($_REQUEST['test_recipient']) {
		$actionmail->param()->testmail = true;
		$actionmail->settings()->devemail = $_REQUEST['test_recipient'];
		$actionmail->console('Set Test Mail: '.$_REQUEST['test_recipient']);
	}
	
	// send mails
	$actionmail->send();
	
	// console messages
	$json['console'] = $actionmail->response()->console;
	
	// total send mails
	$totalSendMails = count($actionmail->response()->sendmails);

	$json['response'] = true;
	$json['notification'] = "Total send $totalSendMails E-mails.";
	
} else {
	$json['response'] = false;
	$json['notification'] = "ERROR: check form data.";
}

	
if (headers_sent()) {
	header('Content-Type: text/json');
}

echo json_encode($json);

