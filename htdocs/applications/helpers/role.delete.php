<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	if ($id) {
		
		$role = new Role();
		$role->read($id);
		$delete = $role->delete();
		
		if ($delete) {
			
			$model = new Model(Connector::DB_CORE);
			
			$model->db->query("
				DELETE FROM role_permissions
				WHERE role_permission_role = $id
			");

			Message::request_deleted();
			url::redirect("/$appliction/$controller");
			
		} else {
			Message::request_failure();
			url::redirect("/$appliction/$controller/$action/$id");
		}
		
	}
	else {
		Message::access_denied();
		url::redirect("/$appliction/$controller");
	}