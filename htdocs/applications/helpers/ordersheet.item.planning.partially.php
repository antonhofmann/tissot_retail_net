<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$id = $_REQUEST['ordersheet'];
	
	// ordersheet
	$ordersheet = new Ordersheet($application);
	$ordersheet->read($id);
	
	// ordering states
	$statment_on_distribution = $ordersheet->state()->onDistribution();
	
	if ($ordersheet->id && $statment_on_distribution) {
		
		$response = $ordersheet->update(array(
			'mps_ordersheet_workflowstate_id' => Workflow_State::STATE_MANUALLY_ARCHIVED,
			'user_modified' => $user->login
		));
		
		$workflowState = new Workflow_State($application);
		$workflowState->read(Workflow_State::STATE_MANUALLY_ARCHIVED);
		
		// user tracking
		$user_tracking = new User_Tracking($application);
		$user_tracking->create(array(
			'user_tracking_entity' => 'order sheet',
			'user_tracking_entity_id' => $ordersheet->id,
			'user_tracking_user_id' => $user->id,
			'user_tracking_action' => $workflowState->name,
			'user_created' => $user->login,
			'date_created' => date('Y-m-d H:i:s')
		));
		
		$response = true;
		$message = $translate->message_ordersheet_partially_distribution;
				
	} else {
		$response = false;
		$message = $translate->error_ordersheet_partially_distribution;
	}
	
	// response with json header
	header('Content-Type: text/json');
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));