<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// visible fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	$id = $_REQUEST['address_additional_id'];
	
	$data = array();
	$data['address_additional_address'] = $_REQUEST['address_additional_address'];
	$data['address_additional_customerno2'] = $_REQUEST['address_additional_customerno2'];
	$data['address_additional_customerno3'] = $_REQUEST['address_additional_customerno3'];
	$data['address_additional_customerno4'] = $_REQUEST['address_additional_customerno4'];
	$data['address_additional_customerno5'] = $_REQUEST['address_additional_customerno5'];
	$data['address_additional_shiptono2'] = $_REQUEST['address_additional_shiptono2'];
	$data['address_additional_shiptono3'] = $_REQUEST['address_additional_shiptono3'];
	$data['address_additional_shiptono4'] = $_REQUEST['address_additional_shiptono4'];
	$data['address_additional_shiptono5'] = $_REQUEST['address_additional_shiptono5'];
	$data['address_additional_lps_customerno1'] = $_REQUEST['address_additional_lps_customerno1'];
	$data['address_additional_lps_customerno2'] = $_REQUEST['address_additional_lps_customerno2'];
	$data['address_additional_lps_customerno3'] = $_REQUEST['address_additional_lps_customerno3'];
	$data['address_additional_lps_customerno4'] = $_REQUEST['address_additional_lps_customerno4'];
	$data['address_additional_lps_customerno5'] = $_REQUEST['address_additional_lps_customerno5'];
	
	
	// update company customer and shipto number
	if (in_array('address_mps_customernumber', $fields) || in_array('address_mps_shipto', $fields)) {
		
		$company = new Company();
		$company->read($_REQUEST['address_additional_address']);
		
		$companyData = array();
		
		if (in_array('address_mps_customernumber', $fields)) {
			$companyData['address_mps_customernumber'] = $_REQUEST['address_mps_customernumber'];
		}
		
		if (in_array('address_mps_shipto', $fields)) {
			$companyData['address_mps_shipto'] = $_REQUEST['address_mps_shipto'];
		}
		
		$company->update($companyData);
	}

	if (isset($_REQUEST['address_additional_dummie_quantity'])) {
		$data['address_additional_dummie_quantity'] = $_REQUEST['address_additional_dummie_quantity'];
	}
	
	
	if ($_REQUEST['address_additional_address']) {
		
		$company_additional = new Company_Additional();
		$company_additional->read($data['address_additional_address']);
		
		if ($id) { 
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $company_additional->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $company_additional->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect'] : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	