<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$model = new Model();
	
	switch ($_REQUEST['section']) {
		
		case 'check_keyword':
			
			$keyword = strtolower(str_replace(' ', '_', $_REQUEST['keyword']));
			
			$result = $model->query("
				SELECT translation_keyword_id
				FROM translation_keywords
				WHERE translation_keyword_name = '$keyword'
			")->fetch();
			
			$response = ($result) ? true : false;
			$message = ($result) ? $translate->message_translation_keyword_exists : $translate->message_translation_not_keyword_exists;
			
		break;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));