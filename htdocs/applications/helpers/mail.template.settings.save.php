<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$id = $_REQUEST['mail_template_id'];
	
	$mail_template = new Mail_Template();
	$mail_template->read($id);
	
	$data = array();
	
	if (in_array('mail_template_action_id', $fields)) {
		$data['mail_template_action_id'] = $_REQUEST['mail_template_action_id'];
		if (empty($data['mail_template_action_id'])) {
			$data['mail_template_action_id'] = null;
		}
	}
	
	if (in_array('mail_template_sender_id', $fields)) {
		$data['mail_template_sender_id'] = ($_REQUEST['mail_template_standard_sender_select']) ? $_REQUEST['mail_template_standard_sender_select'] : null;
	}
	
	if (in_array('mail_template_cc', $fields)) {
		$data['mail_template_cc'] = ($_REQUEST['mail_template_cc']) ? 1 : 0;
	}
	
	if (in_array('mail_template_bcc', $fields)) {
		$data['mail_template_bcc'] = ($_REQUEST['mail_template_bcc']) ? 1 : 0;
	}
	
	if (in_array('mail_template_debugger', $fields)) {
		$data['mail_template_debugger'] = ($_REQUEST['mail_template_debugger']) ? 1 : 0;
	}
	
	if (in_array('mail_template_view_modal', $fields)) {
		$data['mail_template_view_modal'] = ($_REQUEST['mail_template_view_modal']) ? 1 : 0;
	}
	
	if (in_array('mail_template_track', $fields)) {
		$data['mail_template_track'] = ($_REQUEST['mail_template_track']) ? 1 : 0;
	}
	
	if (in_array('mail_template_dev_email', $fields)) {
		$data['mail_template_dev_email'] = ($_REQUEST['mail_template_dev_email']) ? $_REQUEST['mail_template_dev_email'] : null;
	}

	if (in_array('mail_template_group', $fields)) {
		$data['mail_template_group'] = ($_REQUEST['mail_template_group']) ? 1 : 0;
	}
	
	if (in_array('mail_template_group_salutation', $fields)) {
		$data['mail_template_group_salutation'] = ($_REQUEST['mail_template_group_salutation']) ? $_REQUEST['mail_template_group_salutation'] : null;
	}
	
	if ($mail_template->id && $data) {
	
		$data['user_modified'] = $user->login;
		
		$response = $mail_template->update($data);
		
		$message = ($response) ? Translate::instance()->message_request_updated : Translate::instance()->message_request_failure;
	}
	else {
		$response = false;
		$message = Translate::instance()->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));
	