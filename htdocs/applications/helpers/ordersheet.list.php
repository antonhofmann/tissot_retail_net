<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

// request
$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
$fSearch = $_REQUEST['search'] && $_REQUEST['search']<>$translate->search ? $_REQUEST['search'] : null;
$fYear = $_REQUEST['mastersheet_years'];
$fCountry = $_REQUEST['countries'];
$fMasterSheet = $_REQUEST['mastersheets'];
$fWorkflowState = $_REQUEST['workflowstates'];
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'country_name, address_company, mps_mastersheet_name';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$add_button = $_REQUEST['submit_standard_ordersheet'];

$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

// permissions
$permission_view = user::permission(Ordersheet::PERMISSION_VIEW);
$permission_view_limited = user::permission(Ordersheet::PERMISSION_VIEW_LIMITED);
$permission_edit = user::permission(Ordersheet::PERMISSION_EDIT);
$permission_edit_limited = user::permission(Ordersheet::PERMISSION_EDIT_LIMITED);
$permission_manage = user::permission(Ordersheet::PERMISSION_MANAGE);

$limitedAccess = !$permission_manage && !$permission_edit ? true : false;

// db model
$model = new Model($application);

// filter: limited permission
if ($limitedAccess) { 
	
	// country access filter
	$countryAccess = User::getCountryAccess();
	$filterCountryAccess = $countryAccess ? " OR address_country IN ( $countryAccess )" : null;

	// regional access filter
	$regionalAccessCompanies = User::getRegionalAccessCompanies();
	$filterRegionalAccess = $regionalAccessCompanies ? " OR address_id IN (".join(',', $regionalAccessCompanies).") " : null;
	
	$filters['limited'] = "(
		mps_ordersheet_address_id = $user->address 
		AND mps_ordersheet_workflowstate_id <> 7 
		$filterCountryAccess
		$filterRegionalAccess
	)";
}
	
// filter: full text search
if ($fSearch) {	
	$filters['search'] = "(
		address_company LIKE \"%$fSearch%\" 
		OR country_name LIKE \"%$fSearch%\" 
		OR mps_mastersheet_name LIKE \"%$fSearch%\" 
		OR mps_workflow_state_name LIKE \"%$fSearch%\" 
		OR place_name LIKE \"%$fSearch%\"
	)";
}

// filter: mastersheet year
if ($fYear) {
	$filters['mastersheet_years'] = "mps_mastersheet_year = $fYear";
}

// filer: countries
if ($fCountry) {
	$filters['countries'] = "country_id = $fCountry";
}

// filer: mastersheet
if ($fMasterSheet) {
	$filters['mastersheets'] = "mps_ordersheet_mastersheet_id = $fMasterSheet";
}

// filer: workflowstate
if ($fWorkflowState) {
	$filters['workflowstates'] = "mps_ordersheet_workflowstate_id = $fWorkflowState";
}

$controllStates = "
	".Workflow_State::STATE_DISTRIBUTED.",
	".Workflow_State::STATE_MANUALLY_ARCHIVED.",
	".Workflow_State::STATE_ARCHIVED."
";

if ($archived) $filters['visibility'] = " mps_ordersheet_workflowstate_id IN ($controllStates) ";
else $filters['visibility'] = " mps_ordersheet_workflowstate_id NOT IN ($controllStates) ";

$binds = array(
	Ordersheet::DB_BIND_COMPANIES,
	Ordersheet::DB_BIND_MASTERSHEETS,
	Ordersheet::DB_BIND_WORKFLOW_STATES,
	Company::DB_BIND_COUNTRIES,
	Company::DB_BIND_PLACES
);

// datagrid
$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT	
		mps_ordersheet_id,
		mps_ordersheet_workflowstate_id,
		address_company,
		country_name,
		mps_mastersheet_name,
		mps_workflow_state_name,
		DATE_FORMAT(mps_ordersheet_openingdate,'%d.%m.%Y') AS openingdate,
		DATE_FORMAT(mps_ordersheet_closingdate,'%d.%m.%Y') AS closingdate
	FROM mps_ordersheets
")
->bind($binds)
->filter($filters)
->extend($extendedFilter)
->order($sort, $direction)
->offset($offset, $rowsPerPage)
->fetchAll();

if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);
	$timestamp = date::timestamp();
	$icon_warning = ui::icon('warrning');
	
	foreach ($datagrid as $key => $row) {
		
		// dates
		$datagrid[$key]['mps_ordersheet_openingdate'] = $row['openingdate'];
		$datagrid[$key]['mps_ordersheet_closingdate'] = $row['closingdate'];
		
		// expired closing dates
		if ( ($timestamp > date::timestamp($row['closingdate'])) AND in_array($row['mps_ordersheet_workflowstate_id'], array(1,2,7,8))) {
			$datagrid[$key]['mps_ordersheet_closingdate'] = "<span class=error>".$row['closingdate']." !!!</span>";
		}
		
		$result = $model->query("
			SELECT mps_ordersheet_item_id
			FROM mps_ordersheet_items
			WHERE mps_ordersheet_item_ordersheet_id = $key
		")->fetchAll();
		
		if (count($result) < 1) {
			$datagrid[$key]['warningicon'] = $icon_warning;
		}
	}

	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// toolbox: button print
if ($datagrid && $_REQUEST['print']) {
	$toolbox[] = ui::button(array(
		'id' => 'print',
		'icon' => 'print',
		'href' => $_REQUEST['print'],
		'label' => $translate->print
	));
}

if ($add_button) {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'label' => $translate->add_new
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();
	
if($_REQUEST['manage'] || $_REQUEST['submit'] || $_REQUEST['remind'] || $_REQUEST['exchange_ordersheet_items_confirmed'] || $_REQUEST['exchange_ordersheet_items_shipped']) {

	$dropdown = array();
	
	if ($_REQUEST['manage']) {
		array_push($dropdown, array(
			'value' => $_REQUEST['manage'],
			'caption' => $translate->bulk_operations
		));
	}
	
	if ($_REQUEST['submit']) {
		array_push($dropdown, array(
			'value' => $_REQUEST['submit'],
			'caption' => $translate->submit_order_sheet
		));
	}
	
	if ($_REQUEST['remind']) {
		array_push($dropdown, array(
			'value' => $_REQUEST['remind'],
			'caption' => $translate->remind_order_sheet
		));
	}
	
	if ($_REQUEST['exchange_ordersheet_items']) {
		array_push($dropdown, array(
			'value' => $_REQUEST['exchange_ordersheet_items'],
			'caption' => $translate->import_items,
			'class' => 'request_modal'
		));
	}
	
	if ($_REQUEST['exchange_sap']) {
		array_push($dropdown, array(
			'value' => $_REQUEST['exchange_sap'],
			'caption' => 'SAP Import',
			'class' => 'request_modal'
		));
	}
	
	$toolbox[] = ui::dropdown($dropdown, array(
		'name' => 'actions',
		'id' => 'actions',
		'caption' => $translate->actions
	));
}




// toolbox: mastersheet years :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$query = "SELECT DISTINCT mps_mastersheet_year, mps_mastersheet_year AS year FROM mps_ordersheets";
$result = $model->query($query)->bind($binds)->filter($filters)->exclude('mastersheet_years')->order('mps_mastersheet_year')->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'mastersheet_years',
		'id' => 'mastersheet_years',
		'class' => 'submit',
		'value' => $fYear,
		'caption' => $translate->all_years
	));
}

// toolbox: countries ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$query = "SELECT DISTINCT country_id, country_name FROM mps_ordersheets";
$result = $model->query($query)->bind($binds)->filter($filters)->exclude('countries')->order('country_name')->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'countries',
		'id' => 'countries',
		'class' => 'submit',
		'value' => $fCountry,
		'caption' => $translate->all_countries
	));
}


$extendedFilter = null;
$basicFiletrs = $filters;
unset($basicFiletrs['workflowstates']);

// filter country access
if ($accessCountries) {

	$filter = $basicFiletrs;
	unset($filter['limited']);

	if ($filter) {
		$filter = join(' AND ', $filter);
		$extendedFilter = " OR ($filter AND address_country IN ($accessCountriesFilter) )";
	} else {
		$basicFiletrs['countries'] = "address_country IN ($accessCountriesFilter)";
	}
}

// filter regional access
if ($regionalAccessFilter) {

	$filter = $basicFiletrs;
	unset($filter['limited']);

	if ($filter) {
		$filter = join(' AND ', $filter);
		$extendedFilter .= " OR ($filter  AND address_id IN ($regionalAccessFilter))";	
	} else {
		$basicFiletrs['regionals'] = "address_id IN ($regionalAccessFilter)";
	}
}

// toolbox: workflow states ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$query = "SELECT DISTINCT mps_workflow_state_id, mps_workflow_state_name FROM mps_ordersheets";
$result = $model->query($query)->bind($binds)->filter($filters)->exclude('workflowstates')->order('mps_workflow_state_code')->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'workflowstates',
		'id' => 'workflowstates',
		'class' => 'submit',
		'value' => $fWorkflowState,
		'caption' => $translate->all_workflow_states
	));
}


// toolbox: mastersheets ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$query = "SELECT DISTINCT mps_mastersheet_id, mps_mastersheet_name FROM mps_ordersheets";
$result = $model->query($query)->bind($binds)->filter($filters)->exclude('mastersheets')->order('mps_mastersheet_name')->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'mastersheets',
		'id' => 'mastersheets',
		'class' => 'submit',
		'value' => $fMasterSheet,
		'caption' => $translate->all_mastersheets
	));
}

// toolbox: form
if ($toolbox) {
	$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
}

$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

$table->datagrid = $datagrid;

$table->country_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=15%'
);

$table->address_company(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=20%'
);

$table->mps_mastersheet_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	"href=".$_REQUEST['form']
);

$table->mps_ordersheet_openingdate(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

$table->mps_ordersheet_closingdate(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

$table->mps_workflow_state_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

if (!$archived) {
	
	
	if ( $datagrid && _array::key_exists('warningicon', $datagrid)) {
		$table->warningicon(
			'width=20px'
		);
	}
}

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;
