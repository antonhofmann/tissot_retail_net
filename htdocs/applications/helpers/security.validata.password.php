<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	// request
	$field = $_REQUEST['fieldId'];
	$password = $_REQUEST['fieldValue'];
	
	// open user session
	if (!Session::get('user_id')) {
		Session::init();
		Session::set('user_id', $_REQUEST['id']);
	}
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// remove session
	$removeSession = ($user->password_reset_id) ? true : false;
	
	// used password
	$usedPasswords = $user->getUsedPasswords(); 
	
	// render vars
	$render = array(
		'field' => $translate->new_password,
		'password_length' => $settings->limit_password_length
	);
	
	// password length
	if (strlen($password) < $settings->limit_password_length) {
		$error = true;
		$errorMessage = $translate->error_password_length($render);
	}
	
	// password contain part of username
	if (!$error) {

		$usedNames = array(
			$user->firstname,
			$user->name,
			$user->login
		);
		
		foreach ($usedNames as $param) {
			$param = strtolower($param);
			$checkpassword = strtolower($password);
			if ($password==$param || preg_match("/$param/",$checkpassword)) {
				$error = true;
				$errorMessage = $translate->error_password_contain_username($render);
				break;
			}
		}
	}
	
	// is alphanumeric
	if (!$error AND (!ctype_alnum($password) || !preg_match("/[0-9]/",$password) || !preg_match("/[a-zA-Z]/",$password))) {
		$error = true;
		$errorMessage = $translate->error_password_alphanumeric($render);
	}
	
	// password is used
	if (!$error && $usedPasswords && in_array($password, $usedPasswords)) {
		$error = true;
		$errorMessage = $translate->error_password_already_used($render);
	}
	
	// resonse vars
	$success = ($error) ? false : true;
	$message = ($success) ? $translate->message_password_validata_success($render) : $errorMessage;
	
	// response
	$response[0] = $field;
	$response[1] = $success;
	$response[2] = $message;
	echo json_encode($response);
	
	if ($removeSession) {
		Session::remove('user_id');
	}
	