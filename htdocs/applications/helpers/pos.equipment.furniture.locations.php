<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = trim($_REQUEST['application']);
	$controller = trim($_REQUEST['controller']);
	$action = trim($_REQUEST['action']);
	$archived = trim($_REQUEST['archived']);
	$id = $_REQUEST['id'];
	
	// db connector
	$model = new Model($application);
	
	// limited users
	if(!$user->permission(Pos::PERMISSION_VIEW) && !$user->permission(Pos::PERMISSION_EDIT)) {
		$limited_filter = " AND posaddress_client_id = ".$user->address;
	}

	// extended country access
	$accessCountries = array();
		
	$result = $model->query("
		SELECT DISTINCT country_access_country
		FROM db_retailnet.country_access
		WHERE country_access_user = $user->id
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			$accessCountries[] = $row['country_access_country'];
		}
		
		$accessCountriesFilter = join(',', $accessCountries);
	}

	// filter: country access
	if ($accessCountriesFilter) {
		$extendedFilter = "posaddress_country IN ($accessCountriesFilter)";
	}

	// refional access companies
	$regionalAccessFilter = User::getRegionalAccessPos();

	// filter: regional access
	if ($regionalAccessFilter) {
		$extendedFilter .= " $regionalAccessFilter";
	}

	$mainFilter = "(
		posorders.posorder_type = 1
		AND posaddress_store_openingdate IS NOT NULL
		AND posaddress_store_openingdate <> '0000-00-00'
		AND(
			posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = '0000-00-00'
		)
		AND posorders.posorder_order > 0
		AND order_actual_order_state_code <> 900
		AND posorder_project_kind IN (1,2,3,4)
	)";

	if ($extendedFilter) {
		$extendedFilter = " OR ( $mainFilter AND $extendedFilter )";
	}
	
	// get last project orders
	$ordernumbers = $model->query("
		SELECT
			posaddress_id,
			max(posorders.posorder_order) as ordernumber
		FROM db_retailnet.posorders
			LEFT JOIN db_retailnet.posaddresses ON posaddress_id = posorder_posaddress
			LEFT JOIN db_retailnet.orders ON order_id = posorders.posorder_order
		WHERE
			($mainFilter $limited_filter) $extendedFilter
		GROUP BY
			posaddress_id		
	")->fetchAll();
	
	// filter: order numbers
	if ($ordernumbers) {
	
		$orders = array();
	
		foreach ($ordernumbers as $row) {
			$orders[] = $row['ordernumber'];
		}
			
		$ordernumbers = join(', ',$orders);
		$filters['order_numbers'] = "posorder_order IN ( $ordernumbers )";
	}
	
	
	// filter: only opened shops
	$filters['opened'] = "
		posaddress_store_openingdate IS NOT NULL 
		AND posaddress_store_openingdate <> '0000-00-00'
		AND (
			posaddress_store_closingdate = '0000-00-00'
			OR posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = ''
		)
	";
	
	// filter: project kind
	$filters['project_kind'] = 'posorder_project_kind IN (1,2,3,4)';
	
	// filter: item
	$filters['item'] = "item_id = $id";
	
	// filter: item type
	$filters['item_type'] = 'item_type = 1';
	
	// filter: used in mps
	$filters['mps'] = 'item_visible_in_mps = 1';
	
	// datagrid
	$result = $model->query("
		SELECT DISTINCT
			posaddress_id,
			country_name,
			place_name,
			postype_name,
			posaddress_name,
			posaddress_address,
			posaddress_zip,
			FORMAT(order_item_quantity,0) AS quantity
		FROM db_retailnet.posaddresses
	")
	->bind(Pos::DB_BIND_COUNTRIES)
	->bind(Pos::DB_BIND_PLACES)
	->bind(Pos::DB_BIND_POSTYPES)
	->bind('INNER JOIN db_retailnet.posorders ON posaddress_id = posorder_posaddress')
	->bind('INNER JOIN db_retailnet.order_items ON posorder_order = order_item_order')
	->bind('INNER JOIN db_retailnet.items ON order_item_item = item_id')
	->bind('LEFT JOIN mps_pos_furniture ON mps_pos_furniture_item_id = item_id')
	->filter($filters)
	->order('country_name')
	->order('posaddress_name')
	->order('place_name')
	->fetchAll();
	
	$datagrid = _array::datagrid($result);
	
	$table = new Table();
	
	$table->datagrid = $datagrid;
	
	$table->caption('quantity', $translate->quantity, array(
		'class' => '-numbers'		
	));
	
	$table->country_name(
		Table::ATTRIBUTE_NOWRAP,
		'width=10%'
	);
	
	$table->posaddress_name("href=/$application/pos/furniture");
	$table->attributes('posaddress_name', array('target'=>'_blank'));
	
	$table->place_name(
		Table::ATTRIBUTE_NOWRAP,
		'width=10%'
	);
	
	$table->postype_name(
		Table::ATTRIBUTE_NOWRAP,
		'width=10%'
	);
	
	$table->quantity(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_RIGHT,
		'width=10%'
	);
	
	echo $table->render();
	
	
	