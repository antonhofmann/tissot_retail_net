<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$workflow_states = $_REQUEST['workflow_states'];
	
	$model = new Model($application);

	// selected clients
	$ordersheets = ($_REQUEST['ordersheets']) ? explode(',',$_REQUEST['ordersheets']) : array();

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'country_name, address_company, mps_mastersheet_name';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// sql offset
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rows = $settings->limit_pager_rows;
	$offset = ($page-1) * $settings->limit_pager_rows;
		
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			address_company LIKE \"%$keyword%\" 
			OR country_name LIKE \"%$keyword%\" 
			OR mps_mastersheet_name LIKE \"%$keyword%\" 
			OR mps_workflow_state_name LIKE \"%$keyword%\" 
			OR place_name LIKE \"%$keyword%\"
		)";
	}
	
	// filer: mastersheet year
	if ($_REQUEST['mastersheet_years']) {
		$filters['mastersheet_years'] = "mps_mastersheet_year = ".$_REQUEST['mastersheet_years'];
	}
	
	// filer: countries
	if ($_REQUEST['countries']) {
		$filters['countries'] = "country_id = ".$_REQUEST['countries'];
	}
	
	// filer: mastersheet
	if ($_REQUEST['mastersheets']) {
		$filters['mastersheets'] = "mps_ordersheet_mastersheet_id = ".$_REQUEST['mastersheets'];
	}
	
	// default filter
	$filters['state'] = "mps_ordersheet_workflowstate_id IN ($workflow_states)";
	
	$binds = array(
		Ordersheet::DB_BIND_COMPANIES,
		Ordersheet::DB_BIND_MASTERSHEETS,
		Ordersheet::DB_BIND_WORKFLOW_STATES,
		Company::DB_BIND_COUNTRIES,
		Company::DB_BIND_PLACES
	);
	
	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT	
			mps_ordersheet_id,
			mps_ordersheet_workflowstate_id,
			address_company,
			country_name,
			mps_mastersheet_name,
			mps_workflow_state_name,
			DATE_FORMAT(mps_ordersheet_openingdate,'%d.%m.%Y') AS mps_ordersheet_openingdate,
			DATE_FORMAT(mps_ordersheet_closingdate,'%d.%m.%Y') AS mps_ordersheet_closingdate
		FROM mps_ordersheets
	")
	->bind($binds)
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rows)
	->fetchAll();
	
	if ($result) {
	
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		$count = 0;
		
		foreach ($datagrid as $key => $row) {
			$checked = (in_array($key, $ordersheets)) ? 'checked=checked' : null;
			$datagrid[$key]['box'] = "<input type=checkbox class=ordersheetbox name=ordersheetbox value=$key $checked>";
			if ($checked) $count++;
		}
	
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
		$checkall = (count($datagrid) == $count) ? 'checked=checked' : null;
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=clients name=clients value='".join(',',$ordersheets)."' />";
		
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	// data: mastersheet years
	$result = $model->query("
		SELECT DISTINCT mps_mastersheet_year, mps_mastersheet_year AS year 
		FROM mps_ordersheets
	")
	->bind($binds)
	->filter($filters)
	->exclude('mastersheet_years')
	->order('mps_mastersheet_year')
	->fetchAll();
	
	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'mastersheet_years',
			'id' => 'mastersheet_years',
			'class' => 'submit',
			'value' => $_REQUEST['mastersheet_years'],
			'label' => $translate->all_years
		));
	}
	
	// data: countries
	$result = $model->query("
		SELECT DISTINCT country_id, country_name 
		FROM mps_ordersheets
	")
	->bind($binds)
	->filter($filters)
	->exclude('countries')
	->order('country_name')
	->fetchAll();
	
	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'countries',
			'id' => 'countries',
			'class' => 'submit',
			'value' => $_REQUEST['countries'],
			'label' => $translate->all_countries
		));
	}
	
	// data: mastersheets
	$result = $model->query("
		SELECT DISTINCT mps_mastersheet_id, mps_mastersheet_name 
		FROM mps_ordersheets
	")
	->bind($binds)
	->filter($filters)
	->exclude('mastersheets')
	->order('mps_mastersheet_name')
	->fetchAll();
	
	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'mastersheets',
			'id' => 'mastersheets',
			'class' => 'submit',
			'value' => $_REQUEST['mastersheets'],
			'caption' => $translate->all_mastersheets
		));
	}
	
	// toolbox: form
	if ($toolbox) {
		$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	
	if ($datagrid) {
		$table->caption('box', "<input type=checkbox name=checkall class=checkall $checkall>");
	}
	
	$table->country_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=15%'
	);
	
	$table->address_company(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=20%'
	);
	
	$table->mps_mastersheet_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		"href=".$_REQUEST['button']['form']
	);
	
	$table->mps_ordersheet_openingdate(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->mps_ordersheet_closingdate(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->box(
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=20px'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	