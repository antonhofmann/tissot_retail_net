<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// application
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$model = new Model($application);
	
	// datagrid
	$result = $model->query("
		SELECT DISTINCT	
			mps_material_collection_category_file_id AS id,
			mps_material_collection_category_file_path AS path,
			mps_material_collection_category_file_description AS description,
			mps_material_collection_category_file_title as title,
			DATE_FORMAT(mps_material_collection_category_files.date_created, '%d.%m.%Y') AS date,
			file_type_id AS type,
			file_type_name AS type_name
		FROM mps_material_collection_category_files
	")
	->bind(Material_Collection_Category_File::DB_BIND_FILES_TYPES)
	->filter('default', "mps_material_collection_category_file_collection_id = $id")
	->order('file_type_name')
	->order('mps_material_collection_category_file_title')
	->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$file_extension = file::extension($row['path']);
			$row['description'] = ($row['description']) ? substr($row['description'], 0, 80).".." : null;
			
			$datagrid[$row['type']]['caption'] = $row['type_name'];
			$datagrid[$row['type']]['files'][$row['id']]['title'] = $row['title']."<span>".$row['description']."</span>";
			$datagrid[$row['type']]['files'][$row['id']]['date']= "<span>".$translate->date_created.":<br />".$row['date']."</span>";
			$datagrid[$row['type']]['files'][$row['id']]['path'] = $row['path'];
			$datagrid[$row['type']]['files'][$row['id']]['fileicon']  = "<span class='file-extension $file_extension modal' tag='".$row['path']."'></span>";
		}
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'class' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	if ($datagrid) {
		
		foreach ($datagrid as $key => $row) {
			$table = new Table(array('thead' => false));
			$table->datagrid = $row['files'];
			$table->fileicon();
			$table->title("href=".$_REQUEST['form']);
			$table->date();
			$files .= "<h6>".$row['caption']."</h6>";
			$files .= $table->render();
		}
	}
	
	echo $toolbox.$files;
	