<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$application = $_REQUEST['application'];
	$id = $_REQUEST['id'];
	
	$permission_delete = user::permission(Red_project::PERMISSION_DELETE_DATA);

	if ($id && $permission_delete) {

		$integrity = new Integrity();
		$integrity->set($id, 'red_projects', $application);
		
		if ($integrity->check()) {
	
			$project = new Red_Project($application);
			$project->read($id);
			$delete = $project->delete();
	
			if ($delete) {
				Message::request_deleted();
				url::redirect("/$application/projects");
			} else {
				Message::request_failure();
				url::redirect("/$application/projects/data/$id");
			}
		}
		else {
			Message::request_failure();
			url::redirect("/$application/projects");
		}
	}