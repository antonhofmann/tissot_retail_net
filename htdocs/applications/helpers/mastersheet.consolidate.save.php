<?php 
/**
 * 	Master Sheet Consolidation
 * 
 * 	Consolidate all master sheet items.
 * 	Chack whether all order sheets are approved.
 * 	Set order sheet workflow state as consolidated
 * 	Reset all item NULL values to Zero.
 * 	
 * 	@author: admir.serifi@mediaparx.ch
 * 	@copyright (c) Swatch AG
 *  @version 1.1
 * 
 */
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	// db application
	$model = new Model($application);
	
	
	// total mastersheet order sheets
	$result = $model->query("
		SELECT COUNT(mps_ordersheet_id) AS total
		FROM mps_ordersheets
		WHERE mps_ordersheet_mastersheet_id = $id	
	")->fetch();
	
	$totalMastersheetOrdersheets = $result['total'];
	
	
	// Get all approved, completed or expired order sheets
	$ordersheets = $model->query("
		SELECT DISTINCT 
			mps_ordersheet_id
		FROM mps_ordersheets
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_ordersheet_id = mps_ordersheet_id
		WHERE 
			mps_ordersheet_mastersheet_id = $id
			AND (
				mps_ordersheet_workflowstate_id IN (3,4,5)
				OR (
					mps_ordersheet_workflowstate_id = 2
					AND mps_ordersheet_closingdate < CURRENT_DATE
				)
				OR (
					mps_ordersheet_workflowstate_id = 1
					AND mps_ordersheet_closingdate < CURRENT_DATE
					AND ( 
						mps_ordersheet_item_quantity IS NULL 
						OR  mps_ordersheet_item_quantity = ''
						OR  mps_ordersheet_item_quantity = 0
					)
				)
			)
	")->fetchAll();

	
	// if number of conditional fetched orde sheets
	// is equal whith number of total order sheets
	if (count($ordersheets) == $totalMastersheetOrdersheets) {
		
		// master sheet
		$mastersheet = new Mastersheet($application);
		$mastersheet->read($id);
		
		
		// controll arrays
		$ordersheetHasQuantities = array();
		$archiveOrderesheets = array();
		$consolidatedOrdersheets = array();
		$updateItems = array();
		
		// workflow state
		$workflowState = new Workflow_State($application);
		
		// state consolidated
		$workflowState->read(Workflow_State::STATE_CONSOLIDATED);
		$stateConsolidated = $workflowState->id;
		$stateConsolidatedCaption = $workflowState->name;
		
		// state archived
		$workflowState->read(Workflow_State::STATE_ARCHIVED);
		$stateArchived = $workflowState->id;
		$stateArchivedCaption = $workflowState->name;
		
		// traking
		$tracking = new User_Tracking($application);
		
		// get all order sheets items
		$result = $model->query("
			SELECT 
				mps_ordersheet_id,
				mps_ordersheet_workflowstate_id,
				mps_ordersheet_item_id,
				mps_ordersheet_item_quantity,
				mps_ordersheet_item_quantity_approved,
				address_mps_customernumber,
				address_mps_shipto
			FROM mps_ordersheets
			INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_ordersheet_id = mps_ordersheet_id
			INNER JOIN db_retailnet.addresses ON address_id = mps_ordersheet_address_id
			WHERE 
				mps_ordersheet_mastersheet_id = $id
		")->fetchAll();
		
		// data analyse
		if ($result) {
			
			// set all clinet quantites to approved where approved quantity is not responded
			// set all customer/shipping numbers to all not ordered items
			foreach ($result as $row) {
				
				$idOrdersheet = $row['mps_ordersheet_id'];
				$idItem = $row['mps_ordersheet_item_id'];
				$quantity = $row['mps_ordersheet_item_quantity'];
				$approvedQuantity = $row['mps_ordersheet_item_quantity_approved'];
				
				if ($approvedQuantity || ($quantity && is_null($approvedQuantity)) ) {
					
					// set ordersheet as archived
					$ordersheetHasQuantities[$idOrdersheet] = true;
					
					$approvedQuantity = $approvedQuantity ?: $quantity;
					
					$updateItems[$idItem] = array(
						'mps_ordersheet_item_quantity_approved' => $approvedQuantity
					);
					
				} else {
					
					 $updateItems[$idItem] = array(
				 		'mps_ordersheet_item_quantity_approved' => 0,
				 		'mps_ordersheet_item_desired_delivery_date' =>  null,
				 		'mps_ordersheet_item_order_date' => null,
				 		'mps_ordersheet_item_purchase_order_number' => $_SERVER['REQUEST_TIME'],
				 		'mps_ordersheet_item_shipto' => $row['address_mps_shipto'],
				 		'mps_ordersheet_item_customernumber' => $row['address_mps_customernumber']
					 );
				}
			}
		}
		
		
		// set ordersheets new workflow states
		foreach ($ordersheets as $row) {
			
			$key = $row['mps_ordersheet_id'];
			
			// set ordersheet as consolidated
			if ($ordersheetHasQuantities[$key]) {
				$consolidatedOrdersheets[$key] = $consolidatedOrdersheets[$key] + 1;
			} 
			// set ordersheet as archived
			else {
				$archiveOrderesheets[$key] = $archiveOrderesheets[$key] + 1;
			}
		}
		
		
		// total consolidated and archived
		$totalConsolidatedOrdersheets = count($consolidatedOrdersheets);
		$totalArchivedOrdersheets = count($archiveOrderesheets);
		
		
		// consolidate master sheet
		if ($totalMastersheetOrdersheets == $totalConsolidatedOrdersheets + $totalArchivedOrdersheets) {
			
			$response = false;
			
			// ordersheet item builder
			$item = new Ordersheet_Item($application);
			
			// update items
			if ($updateItems) {
				foreach ($updateItems as $key => $data) {
					$item->read($key);
					$item->update($data);
				}
			}
			
			// change order sheet states
			foreach ($ordersheets as $row) {
				
				$key = $row['mps_ordersheet_id'];
				
				// set order sheet in archive
				if ($archiveOrderesheets[$key]) {
					
					 $response = $model->db->exec("
						UPDATE mps_ordersheets SET
							mps_ordersheet_workflowstate_id = $stateArchived,
							user_modified = '$user->login',
							date_modified = '".date('Y-m-d H:i:s')."'
						WHERE mps_ordersheet_id = $key
					");

					$tracking->create(array(
						'user_tracking_entity' => 'order sheet',
						'user_tracking_entity_id' => $key,
						'user_tracking_user_id' => $user->id,
						'user_tracking_action' => $stateArchivedCaption,
						'user_created' => $user->login,
						'date_created' => date('Y-m-d H:i:s')
					));
				}
				// set order sheet as consolidated
				else {
					
					$response = $model->db->exec("
						UPDATE mps_ordersheets SET
							mps_ordersheet_workflowstate_id = $stateConsolidated,
							user_modified = '$user->login',
							date_modified = '".date('Y-m-d H:i:s')."'
						WHERE mps_ordersheet_id = $key
					");
			
					$tracking->create(array(
						'user_tracking_entity' => 'order sheet',
						'user_tracking_entity_id' => $key,
						'user_tracking_user_id' => $user->id,
						'user_tracking_action' => $stateConsolidatedCaption,
						'user_created' => $user->login,
						'date_created' => date('Y-m-d H:i:s')
					));
				}
			}
			
			// set mastersheet as consolidated
			$response = $mastersheet->update(array(
				'mps_mastersheet_consolidated' => 1,
				'user_modified' => $user->login
			));
			
			// set master sheet as archived if all ordersheets are archived
			if ($totalMastersheetOrdersheets == $totalArchivedOrdersheets) {
				
				// set mastersheet as consolidated
				$mastersheet->update(array(
					'mps_mastersheet_archived' => 1,
					'user_modified' => $user->login
				));
			}
		} 
		else { 
			
			$response = false;
		}
	}
	else {
		
		$response = false;
	}

	if ($response) {
		Message::add(array(
			'type' => 'message',
			'keyword' => 'message_mastersheet_consolidate',
			'data' => array('mps_mastersheet_name' => $mastersheet->name)
		));
	} else {
		Message::add(array(
			'type' => 'error',
			'keyword' => 'error_mastersheet_consolidate',
			'sticky' => true
		));
	}
	
	url::redirect("/$application/$controller/$action/$id");
	
	