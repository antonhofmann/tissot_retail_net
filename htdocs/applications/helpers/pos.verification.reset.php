<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$period = $_REQUEST['periodes'];
$region = $_REQUEST['regions'];
$state = $_REQUEST['states'];

$model = new Model($application);


$response['response'] = true;

// get confirmed verifications
$result = $model->query("
	SELECT 
		posverification_address_id as company,
		posverification_id
	FROM posverifications
	WHERE UNIX_TIMESTAMP(posverification_date) = '$period'
	AND (posverification_confirm_date IS NOT NULL OR posverification_confirm_date != '')
")->fetchAll();

$verifications_confirmed = _array::extract($result);

$model->query("
	SELECT DISTINCT
		address_id,
		address_company
	FROM db_retailnet.addresses
")
->bind(Company::DB_BIND_COUNTRIES)
->filter('default', "address_type = 1 AND address_active = 1")
->order('country_name');

if ($region) {
	$model->filter('region', "country_region = $region");
}

$result = $model->fetchAll();

if ($result) {
	
	$posVerification = new Pos_Verification();
	
	foreach ($result as $row) {
			
		$company = $row['address_id'];
		
		if ($verifications_confirmed[$company]) {
			
			$posVerification->read($verifications_confirmed[$company]);
			
			if ($posVerification->id) {
				$response = $posVerification->update(array(
					'posverification_confirm_date' => null,
					'posverification_confirm_user_id' => null,
					'user_modiefied'=> user::instance()->id,
					'date_modiefied'=> date('Y-m-d H:i:s')
				));	
			}
		}
	}

	$json['reload'] = $response;
	
	if ($response) {
		Message::success(Translate::instance()->message_reset_verification);
	} else {
		$json['notification'] = Translate::instance()->error_reset_verification;
	}
} 
else {
	$json['notification'] = Translate::instance()->warning_reset_verification;
}

if (headers_sent()) {
	header('Content-Type: text/json');
}

echo json_encode($json);
