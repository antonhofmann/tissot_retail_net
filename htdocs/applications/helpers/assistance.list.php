<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	// active section
	$activeBundle = Session::get('assistance.bundle.active');
	Session::remove('assistance.bundle.active');
	
	// active application
	$activeApplication = Session::get('assistance.application.active');
	Session::remove('assistance.application.active');

	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	if(array_key_exists('group', $_REQUEST)) $group = $_REQUEST['group'];
	else $group = 2;
	
	$model = new Model(Connector::DB_CORE);

	
	if ($group) {
		
		$order = ($group==1) 
			? ' ORDER BY assistance_section_link_link, assistance_title, assistance_section_order, assistance_section_title' 
			: ' ORDER BY application_name, assistance_title, assistance_section_order, assistance_section_title';
		
	} else {
		
		$order = ' ORDER BY assistance_title, assistance_section_order, assistance_section_title';
	}

	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filter = " WHERE (";
		
		$filter .= "
			assistance_title LIKE \"%$keyword%\" 
			OR assistance_section_title LIKE \"%$keyword%\"
		";
		
		if ($group==1) {
			$filter .= " OR assistance_section_link_link LIKE \"%$keyword%\" ";
		}
		elseif ($group==2) {
			$filter .= " OR application_name LIKE \"%$keyword%\" ";
		}
		
		$filter .= " )";
	}
	
	// group by pages
	if ($group==1) {
		
		$query = "
			SELECT
				assistance_id,
				assistance_title,
				assistance_section_id,
				assistance_section_title,
				assistance_section_published,
				assistance_section_link_id,
				assistance_section_link_link
			FROM assistance_sections
			INNER JOIN assistances ON assistance_id = assistance_section_assistance_id
			LEFT JOIN assistance_section_links ON assistance_section_link_section_id = assistance_section_id
		";
	} else {
		
		$query = "
			SELECT
				assistance_id,
				assistance_title,
				assistance_section_id, 
				assistance_section_title,
				assistance_section_published,
				application_id,
				application_name
			FROM assistances
			LEFT JOIN assistance_sections ON assistance_id = assistance_section_assistance_id
			LEFT JOIN applications ON application_id = assistance_application_id		
		";
	}
	

	$result = $model->query($query.$filter.$order)->fetchAll();

	if ($result) {
		
		foreach ($result as $row) {
				
			$assistance = $row['assistance_id'];
			$section = $row['assistance_section_id'];
			
			if ($group) {
				
				$index = ($group==1) ? $row['assistance_section_link_link'] : $row['application_id'];
				$index = ($index) ? $index : 0;
				
				// bundle caption
				if (!$assistances[$index]['caption']) {
					$title = ($group==1) ? $row['assistance_section_link_link'] : $row['application_name'];
					$assistances[$index]['caption'] = ($title) ? $title : 'Unorganized';
				}
				
				// bundle title
				if (!$assistances[$index]['bundles'][$assistance]['book']) {
					$assistances[$index]['bundles'][$assistance]['book'] = nl2br($row['assistance_title']);
					$assistances[$index]['bundles'][$assistance]['button'] = true;
				}
				
				// bundle sections
				if ($row['assistance_section_title']) {
					$assistances[$index]['bundles'][$assistance]['sections'][$section]['title'] = nl2br($row['assistance_section_title']);
					$assistances[$index]['bundles'][$assistance]['sections'][$section]['published'] = $row['assistance_section_published'];
				}
				
			}
			// group by content
			else {	
				
				// book caption
				if (!$assistances[$assistance]['book']) {
					$assistances[$assistance]['caption'] = $row['assistance_title'];
					$assistances[$assistance]['flag'] = $row['application_name'];
					$assistances[$assistance]['button'] = true;
				}
				
				// book sections
				if ($row['assistance_section_title']) {
					$assistances[$assistance]['sections'][$section]['title'] = nl2br($row['assistance_section_title']);
					$assistances[$assistance]['sections'][$section]['published'] = $row['assistance_section_published'];
				}
			}	
		}
	}
	
	
	// toolbox: add
	if ($_REQUEST['add'])  {
		
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => 'Add Section'
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	if ($assistances) {
		
		$groups = array(
			1 => 'Page Link',
			2 => 'Application Name'
		);
		
		$groups = html::select($groups, array(
			'name' => 'group',
			'id' => 'group',
			'class' => 'submit',
			'value' => $_REQUEST['group'],
			'caption' => 'Topic Section'
		));
		
		$toolbox[].= "<span class='groups'><span class='filter-label'>Group by: </span>$groups</span>";
	}
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	//echo "<pre>"; print_r($assistances); echo "</pre>";
	echo $toolbox;
	
	if ($assistances) {

		foreach ($assistances as $assistance => $data) {
			
			$selectedPanel = $activeApplication==$assistance ? 'selected' : null;
			
			echo "<div class='panel $selectedPanel'>";
			
			// caption
			echo "<div class='title'>";
			
				$caption = $data['caption'];
			
				// button add section
				if ($data['button']) {
					echo "<a class='section-button' href='/$application/$controller/data/$assistance'><i class='fa fa-plus' ></i></a>";
				}
			
				// caption
				echo (!$group) 
					? "<span class='section-title' >$caption</span><a class='section-edit' href='/$application/$controller/add/$assistance' class='section' >[edit]</a>"
					: "<span class='section-title' >$caption</span>";
				
				// caption flag
				if ($data['flag']) {
					//echo "<i class='section-flag'>".$data['flag']."</i>";
				}
				
				// content toggler
				if ($data['sections'] || $data['bundles']) {
					echo "<i class=\"fa fa-chevron-circle-down toggler panel-toggler\" ></i>";
				}
			
			echo "</div>";
			
			echo "<div class='content'>";
			
			
			// sections
			// group by section titles
			if ($data['sections']) {
				
				echo "<ul>";
				
				foreach ($data['sections'] as $section => $book) {
					
					$title = $book['title'];
					$published = ($book['published']) ? 'published' : null;
						
					echo "<li>";
					echo "<i class='fa fa-check-circle $published'></i> ";
					echo "<a href='/$application/$controller/data/$assistance/$section'>$title</a>";
					echo "</li>";
				}
				
				echo "</ul>";
			}
			
			
			// bundles
			// group by applications or links
			if ($data['bundles']) {
				
				echo "<ul class=\"bundel-topics\" >";

				foreach ($data['bundles'] as $assistance => $bundle) {
					
					// button add section
					$button = ($bundle['button']) ? "<a href='/$application/$controller/data/$assistance'><i class='fa fa-plus' ></i></a>" : null;
					
					$selectedBundle = $activeBundle==$assistance ? 'selected' : null;
	
					// assistance title
					echo "<li class='bundle' id=\"bundle-$assistance\">";
					echo $button;
					echo "<span class='section-title' >".$bundle['book']."</span>";
					echo "<a class='section-edit' href='/$application/$controller/add/$assistance' class='section'>[edit]</a>";
					echo "<i class=\"fa fa-chevron-circle-down toggler bundle-toggler\" ></i>";
					echo "</li>";
					
					echo "<ul class=\"bundel-sections $selectedBundle\" data-id='$assistance' >";
					
					if ($bundle['sections']) {
							
						foreach ($bundle['sections'] as $section => $book) {
							
							$title = $book['title'];
							$published = ($book['published']) ? 'published' : null;
							
							echo "<li id=\"section-$section\" data-id='$section' >";	
							echo "<i class='fa fa-check-circle $published'></i> ";
							echo "<a href='/$application/$controller/data/$assistance/$section'>$title</a>";
							echo "<i class='fa fa-bars orderer pull-right'></i> ";
							echo "</li>";
						}
					}
						
					echo "</ul>";
					echo "</li>";
				}
				
				echo "</ul>";
			}
			
			echo "</div>";
			echo "</div>";
		}
	}
	