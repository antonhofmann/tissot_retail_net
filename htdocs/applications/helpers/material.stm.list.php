<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];

	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	$model = new Model($application);
	
	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : '
		mps_material_collection_code, 
		mps_material_collection_category_code, 
		mps_material_planning_type_name, 
		mps_material_code, 
		mps_material_name
	';
	
	// sql order
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
	
	// editing
	$canedit = ($user->permission(Material::PERMISSION_STM_EDIT)) ? true : false;
	
	// query join tables
	$binds = array(
		Material::DB_BIND_PLANNING_TYPES,
		Material::DB_BIND_COLLECTIONS,
		Material::DB_BIND_COLLECTION_CATEGORIES
	);

	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			mps_material_collection_code LIKE \"%$keyword%\" 
			OR mps_material_collection_category_code LIKE \"%$keyword%\" 
			OR mps_material_planning_type_name LIKE \"%$keyword%\" 
			OR mps_material_code LIKE \"%$keyword%\" 
			OR mps_material_name LIKE \"%$keyword%\"
			OR mps_material_hsc LIKE \"%$keyword%\"
		)";
	}
	
	// filter: category
	if ($_REQUEST['planning_types']) {
		$filters['planning_types'] = "mps_material_material_planning_type_id = ".$_REQUEST['planning_types'];
	}
	
	// filter: collections
	if ($_REQUEST['collections']) {
		$filters['collections'] = "mps_material_material_collection_id = ".$_REQUEST['collections'];
	}
	
	// filter: collection category
	if ($_REQUEST['collection_categories']) {
		$filters['collection_categories'] = "mps_material_material_collection_category_id = ".$_REQUEST['collection_categories'];
	}
	
	// filter: active
	$active = ($archived) ? 0 : 1;
	$filters['active'] = "mps_material_active = $active";
	
	// filter: default
	$filters['stm'] = "(
		mps_material_islongterm = 0 
		OR mps_material_islongterm IS NULL 
		OR mps_material_islongterm = ''
	)";

	$filters['dummies'] = "(
		mps_material_planning_is_dummy IS NULL 
		OR mps_material_planning_is_dummy = ''
		OR mps_material_planning_is_dummy = 0
	)";
	
	
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			mps_material_id,
			mps_material_collection_code,
			mps_material_collection_category_code,
			mps_material_planning_type_name,
			mps_material_hsc,
			mps_material_code,
			mps_material_name,
			mps_material_width,
			mps_material_height,
			mps_material_length,
			mps_material_radius
		FROM mps_materials
	")
	->bind($binds)
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();
	
	if ($result) {
	
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
	
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
	
	// toolbox: utton print
	if ($datagrid && $_REQUEST['export']) {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'label' => $translate->print,
			'href' => $_REQUEST['export']
		));
	}
	
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	// dataloader: planning types
	$result = $model->query("
		SELECT DISTINCT 
			mps_material_planning_type_id, 
			mps_material_planning_type_name 
		FROM mps_materials
	")
	->bind($binds)
	->filter($filters)
	->filter('default', 'mps_material_planning_type_id > 0')
	->exclude('planning_types')
	->order('mps_material_planning_type_name')
	->fetchAll();
	
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'planning_types',
		'id' => 'planning_types',
		'class' => 'submit',
		'value' => $_REQUEST['planning_types'],
		'caption' => $translate->all_planning_types
	));

	// dataloader: collections
	$result = $model->query("
		SELECT DISTINCT 
			mps_material_collection_id, 
			mps_material_collection_code 
		FROM mps_materials
	")
	->bind($binds)
	->filter($filters)
	->filter('default', 'mps_material_collection_id > 0')
	->exclude('collections')
	->order('mps_material_collection_code')
	->fetchAll();
	
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'collections',
		'id' => 'collections',
		'class' => 'submit',
		'value' => $_REQUEST['collections'],
		'caption' => $translate->all_collections
	));
	
	// dataloader: collection categories
	$result = $model->query("
		SELECT DISTINCT 
			mps_material_collection_category_id, 
			mps_material_collection_category_code 
		FROM mps_materials
	")
	->bind($binds)
	->filter($filters)
	->filter('default', 'mps_material_collection_category_id > 0')
	->exclude('collection_categories')
	->order('mps_material_collection_category_code')
	->fetchAll();
	
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'collection_categories',
		'id' => 'collection_categories',
		'class' => 'submit',
		'value' => $_REQUEST['collection_categories'],
		'caption' => $translate->all_collection_categories
	));

	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	// table builder
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	
	$table->mps_material_collection_category_code(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=5%'
	);
	
	$table->mps_material_collection_code(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=5%'
	);
	
	$table->mps_material_planning_type_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=5%'
	);
	
	$table->mps_material_code(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=5%'
	);
	
	$table->mps_material_name(
		Table::PARAM_SORT,
		"href=".$_REQUEST['form']
	);

	$table->mps_material_hsc(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=5%'
	);
	
	$table->mps_material_width(
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=5%'
	);
	
	$table->mps_material_height(
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=5%'
	);
	
	$table->mps_material_length(
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=5%'
	);
	
	$table->mps_material_radius(
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=5%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	
	