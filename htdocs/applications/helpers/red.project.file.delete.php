<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$project = $_REQUEST['project'];
	$file = $_REQUEST['file'];
	
	$projectFile = new Red_File($application);
	$projectFile->read($file);

	if ($projectFile->id && $project && user::permission(Red_Project::PERMISSION_DELETE_DATA)) {

		$integrity = new Integrity();
		$integrity->set($file, 'red_files', $application);

		if ($integrity->check()) {

			$filename = $_SERVER['DOCUMENT_ROOT'].$file->path;
			
			// delete file
			$response = $projectFile->delete();
			
			// file accesses
			if ($response) {
				
				//delete the file on server
				file::remove($filename);
				
				$fileAccess = new Red_Project_File_Access($application);
				$fileAccess->file($file);
				$fileAccess->deleteAll();

				message::request_deleted();
				url::redirect("/$application/$controller/$action/$project");
			} else {
				message::request_failure();
				url::redirect("/$application/$controller/$action/$project/$file");
			}
		}
		else {
			message::request_failure();
			url::redirect("/$application/$controller/$action/$project/$file");
		}
		
	} else {
		message::access_denied();
		url::redirect("/$application/$controller");
	}