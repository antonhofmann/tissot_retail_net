<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$auth = User::instance();
	$translate = Translate::instance();

	$controller = $_REQUEST['controller'];
	$id = $_REQUEST['mail_user'];
	
	$user = new User($id);
	
	if ($user->id) {
	
		$model = new Model(Connector::DB_CORE);
		
		// user roles
		$roles = $user->roles($id);
		
		// get all role responsible persons
		$result = $model->query("
			SELECT DISTINCT
				user_id,
				user_firstname,
				user_name,
				user_email,
				user_email_cc,
				user_email_deputy,
				role_id,
				role_application
			FROM roles
			INNER JOIN users ON role_responsible_user_id = user_id
			WHERE user_active = 1	
		")->fetchAll();
		
		// role responsible person not found!
		// get all system administrators
		if (!$result) {
			$result = $model->query("
				SELECT DISTINCT
					user_id,
					user_firstname,
					user_name,
					user_email,
					user_email_cc,
					user_email_deputy,
					user_role_role AS role_id
				FROM users
				INNER JOIN user_roles ON user_role_user = user_id
				WHERE user_role_role = 1 AND user_active = 1
			")->fetchAll();
		}
		
		$recipient = null;
		$recipients = array();
		
		if ($result) {
			
			$recipients = array();
			
			foreach ($result as $row) {
				
				if (!$recipient && in_array($row['role_id'], $roles)) {
					$recipient = $row;
				} elseif ($row['user_email'] <> $recipient['user_email']) {
					$recipients[] = $row['user_email'];
				}

				if ($row['user_email_cc'] <> $recipient['user_email']) {
					$recipients[] = $row['user_email_cc'];
				}

				if ($row['user_email_deputy'] <> $recipient['user_email']) {
					$recipients[] = $row['user_email_deputy'];
				}
			}
				
			if ($recipients) {
				
				$recipients = array_filter(array_unique($recipients));
				
				// find default recipient in cc recipients
				$key = array_search($recipient['user_email'], $recipients); 
				
				if (false !== $key) {
					unset($recipients[$key]);
				}
			}
		}
		
		if ($recipient) {
			
			$sendmail = new Send_Mail();
			
			$company = new Company();
			$company->read($user->address);
			
			// mail data
			$template = new Mail_Template();
			$template->read_from_shortcut($_REQUEST['mail_template']);
			$subject = ($_REQUEST['mail_subject']) ? $_REQUEST['mail_subject'] : $template->subject;
			$content = ($_REQUEST['mail_content']) ? $_REQUEST['mail_content'] : $template->text;

			if ($_REQUEST['close_account_reason']) {
				$content .= "\r\n\nThe reaseon for closing the account is:";
				$content .= "\r\n\n".$_REQUEST['close_account_reason'];
			}

			// link access to user data
			$protocol = $settings->http_protocol.'://';
			$server = $_SERVER['SERVER_NAME'];
			
			$check = $model->query("
				SELECT user_role_user
				FROM user_roles
				INNER JOIN role_permissions ON role_permission_role = user_role_role
				INNER JOIN permissions ON permission_id = role_permission_permission
				WHERE user_role_user = {$recipient[user_id]} AND permission_name = 'can_edit_catalog'
			")->fetch();
			
			$link  = $check['user_role_user'] > 0
				? $protocol.$server."/admin/user.php?id=$id"
				: $protocol.$server."/administration/users/users/$user->address/$id";

			
			$render = array(
				'recipient_firstname' => $row['user_firstname'],
				'user_firstname' => $user->firstname,
				'user_name' => $user->name,
				'link' => $protocol.$server.$link,
				'sender_firstname' => $auth->firstname,
				'sender_name' => $auth->name,
				'company_name' => $company->company
			);
			
			$mail_subject = content::render($subject, $render);
			$mail_content = content::render($content, $render);
			
			if ($template->footer) {
				$mail_content .= "\r\n\n".content::render($template->footer, $render);
			}
			
			// devmod recipients
			if ($template->devmod && $template->devmod_email) {
			
				// cc recipient(s)
				$cc_recipients = ($recipients) ? ', CC: '.join(', ', $recipients) : null;
				
				$mail_subject = "$mail_subject (DEVMOD)";
			
				// devmode signature
				$mail_content .= "\r\n\nIMPORTANT: Sendmail is in development mode. This email is dedicated for recipient(s): ".$recipient['user_email'].$cc_recipients;
			
				// replace user email to devmode email
				$recipient['user_email'] = $template->devmod_email;
			}
			
			if ($recipient['user_email']) {
				
				$sendmail->subject($recipient['user_id'], $mail_subject);
				$sendmail->content($recipient['user_id'], $mail_content);
				$sendmail->recipient($recipient['user_id'], $recipient['user_firstname']." ".$recipient['user_name']);
				$sendmail->email($recipient['user_id'], $recipient['user_email']);
				
				if ($recipients && !$template->devmod) {
					foreach ($recipients as $cc) {
						if ($cc <> $recipient['user_email']) {
							$sendmail->cc($recipient['user_id'], $cc);
						}
					}
				}
			}
			
			// addition recipients
			$additional_recipients = $template->get_additional_recipients();
			
			// add additional recipients
			if ($additional_recipients) {
				foreach ($additional_recipients as $email) {
					$sendmail->additionalRecipient($email);
				}
			}
			
			// send mails
			$sendmail->template($template->id);
			$response = $sendmail->send();
			$redirect = $_REQUEST['mail_redirect'];
			
			if ($_REQUEST['mail_template']=='user_request_close_account') {
				
				// remove user roles
				$model->db->exec("
					DELETE FROM user_roles
					WHERE user_role_user = $user->id		
				");
				
				// deactivate user account
				$user->update(array(
					'user_active' => '0',
					'user_modified' => $auth->login,
					'date_modified' => date('')
				));
				
				$message_key = 'message_request_close_account';
				$message = $translate->$message_key;
				
			} else {
				$message_key = 'message_request_open_account';
				$message = $translate->$message_key;
			}

			if (url::isAjax()) {
				Message::add(array(
					'type' => 'message',
					'keyword' => $message_key,
					'life' => 5000
				));
			}
		}
	}

	echo json_encode(array(
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message
	));
