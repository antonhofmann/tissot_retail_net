<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['mps_visual_id'];
	$redirect = $_REQUEST['redirect'];
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	$data = array();
	
	if ($_REQUEST['mps_visual_code']) {
		$data['mps_visual_code'] = $_REQUEST['mps_visual_code'];
	}
	
	if ($_REQUEST['mps_visual_name']) {
		$data['mps_visual_name'] = $_REQUEST['mps_visual_name'];
	}
	
	if (in_array('mps_visual_active', $fields)) {
		$data['mps_visual_active'] = ($_REQUEST['mps_visual_active']) ? 1 : 0;
	}
	
	if (isset($_REQUEST['mps_visual_width'])) {
		$data['mps_visual_width'] = ($_REQUEST['mps_visual_width'])? $_REQUEST['mps_visual_width'] : null;
	}
	
	if (isset($_REQUEST['mps_visual_height'])) {
		$data['mps_visual_height'] = ($_REQUEST['mps_visual_height'])? $_REQUEST['mps_visual_height'] : null;
	}
	
	if (isset($_REQUEST['mps_visual_length'])) {
		$data['mps_visual_length'] = ($_REQUEST['mps_visual_length'])? $_REQUEST['mps_visual_length'] : null;
	}
	
	if (isset($_REQUEST['mps_visual_radius'])) {
		$data['mps_visual_radius'] = ($_REQUEST['mps_visual_radius'])? $_REQUEST['mps_visual_radius'] : null;
	}
	
	if ($data) {
		
		$visual = new Visual($application);
		$visual->read($id);
		
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $visual->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $visual->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = "$redirect/$id";
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));