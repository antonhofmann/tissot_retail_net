<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// framework vars
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$model = new Model(Connector::DB_CORE);

// build request fileds (join values from session)
$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

// filters
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "user_name, user_firstname";
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

$_REQUEST['active'] = (isset($_REQUEST['active'])) ? $_REQUEST['active'] : 'active';

// filter: active
if ($_REQUEST['active']) {
	$active = ($_REQUEST['active'] == 'active') ? 1 : 0;
	$filters['active'] = "user_active = $active";
}

if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {

	$keyword = $_REQUEST['search'];

	$filters['search'] = "(
		user_firstname LIKE \"%$keyword%\"
		OR user_name LIKE \"%$keyword%\"
		OR user_email LIKE \"%$keyword%\"
	)";
}

if ($_REQUEST['id']) {
	$filters['companies'] = "address_id = ".$_REQUEST['id'];
	$bind = array(User::DB_BIND_COMPANIES);
}

// data: companies
$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		user_id,
		user_firstname,
		user_name,
		user_email,
		user_email_cc,
		user_email_deputy,
		user_phone,
		user_mobile_phone,
		user_active
	FROM db_retailnet.users
")
->bind($bind)
->filter($filters)
->order($sort, $direction)
->offset($offset, $rowsPerPage)
->fetchAll();

if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);
	
	// role applications
	// if is required to split planning roles from other roles
	$role_applications = ($_REQUEST['role_applications']) ? explode(',', $_REQUEST['role_applications']) : array();
	
	
	foreach ($datagrid as $key => $row) {
	
		$user_roles = array();
		$user_planning_roles = array();
		
		$roles = $model->query("
			SELECT DISTINCT
				role_name,
				application_shortcut
			FROM db_retailnet.roles
				INNER JOIN db_retailnet.user_roles ON user_role_role = role_id
				LEFT JOIN db_retailnet.application_roles ON application_role_role = role_id
				LEFT JOIN db_retailnet.applications ON application_role_application = application_id
			WHERE user_role_user = $key
			ORDER BY role_name
		")->fetchAll();
					
		if ($roles) { 
			
			foreach ($roles as $role) {
				
				if ($role['application_shortcut'] && in_array($role['application_shortcut'], $role_applications)) {
					$user_planning_roles[] = "<span>".$role['role_name']."</span>";
				} else {
					$user_roles[] = "<span>".$role['role_name']."</span>";
				}
			}
			
			if ($user_planning_roles) $datagrid[$key]['planning_roles'] = join($user_planning_roles);
			if ($user_roles) $datagrid[$key]['other_roles'] = join($user_roles);
		}
		
		if ($_REQUEST['form']) {
			
			$link = $_REQUEST['form'];
			
			if ($controller=='mycompany') {
				$datagrid[$key]['close_account'] = ($row['user_active']) ? "<a href='$link/$key' data-id='$key' class='close-account' >".$translate->close_account."</a>" : null;
			} 
		}
	}

	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$pageIndex = $pager->index();
	$pageControlls = $pager->controlls();
}

// toolbox: hide utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// toolbox: button add
if ($_REQUEST['add']) {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();

// toolbox: user actives
$users = array();
$users[1]['value'] = 'active';
$users[1]['caption'] = $translate->active_users;
$users[0]['value'] = 'inactive';
$users[0]['caption'] = $translate->inactive_users;

$toolbox[] = ui::dropdown($users, array(
	'name' => 'active',
	'id' => 'active',
	'class' => 'submit',
	'value' => $_REQUEST['active'],
	'caption' => $translate->all_users
));

// toolbox: utton print
if ($datagrid && $_REQUEST['export']) {
	$toolbox[] = ui::button(array(
		'id' => 'print',
		'icon' => 'print',
		'href' => $_REQUEST['print'],
		'label' => $translate->print
	));
}

if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

$table = new Table(array(
	'sort' => array(
		'column' => $sort,
		'direction' => $direction
	)
));

$table->datagrid = $datagrid;

$table->user_firstname(
	'width=20%',
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);

$table->user_name(
	Table::PARAM_SORT,
	'width=20%'
);

$table->user_email(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=20%'
);

if ($controller=='mycompany' && $_REQUEST['link']) {
	$table->user_name("href=".$_REQUEST['link']);
}

if ($controller=='mycompany') {
	
	$table->user_email_deputy(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->user_email_cc(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
}

$table->user_phone(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

$table->user_mobile_phone(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=10%'
);

if ($datagrid && $_REQUEST['role_applications'] && _array::key_exists('planning_roles', $datagrid)) {
	$table->planning_roles(Table::ATTRIBUTE_NOWRAP);
}

if ($datagrid && _array::key_exists('other_roles', $datagrid)) {
	
	$caption = (_array::key_exists('planning_roles', $datagrid)) ? $translate->other_roles : $translate->roles;
	$table->caption('other_roles', $caption);
	
	$table->other_roles(
		Table::ATTRIBUTE_NOWRAP
	);
}

if ($controller<>'mycompany') {
	$table->user_name('href='.$_REQUEST['form']);
} elseif (user::permission('can_request_account_closing_opening') && $_REQUEST['form']) {
	$table->close_account(
		'width=10%',
		Table::ATTRIBUTE_NOWRAP
	);
}

$table->footer($pageIndex);
$table->footer($pageControlls);
$table = $table->render();

echo $toolbox.$table;
