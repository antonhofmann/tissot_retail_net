<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$settings->load('data');
	$user = User::instance();
	$translate = Translate::instance();
	$model = new Model(Connector::DB_CORE);

	$selected = Session::get('selected-navigation');
		
	switch ($_REQUEST['section']) {
				
		case 'json':
			
			function navigation_json($param) {
					
				global $navigations, $parents;
					
				if ($navigations[$param]) {
			
					foreach ($navigations[$param] as $id => $row) {
							
						$json['key'] = $row['id'];
						$json['parent'] = $row['parent'];
						$json['title'] = $row['caption'];
						$json['href'] = '/administration/navigations/data/'.$row['id'];
						$json['order'] = $row['order'];
						$json['expand'] = false;
						$json['isFolder'] = false;
						$json['children'] = null;
						$json['select'] = ($row['active']) ? true : false;
							
						if ($navigations[$row['id']]) {
							$json['isFolder'] = true;
							$json['expand'] = (in_array($row['id'], $parents)) ? true : false;
							$json['children'] = navigation_json($row['id']);
						}
							
						$return[] = $json;
					}
				}
					
				return $return;
			}
			
			$result = Navigation::load();	
			$navigations = Navigation::buildTree($result);

			$parents = array();

			if ($selected) {
				$nav = new Navigation();
				$parents = $nav->getParents($selected);
			}

			$json = navigation_json(0);

			echo json_encode($json);
			
		break;
		
		case 'json_permissions':
				
			function _navigation_json($param) {
					
				global $navigations, $roles;
					
				if ($navigations[$param]) {
						
					foreach ($navigations[$param] as $id => $row) {
							
						$json['key'] = $id;
						$json['title'] = $row['caption'];
						$json['select'] = ($roles[$id]) ? true : false;
							
						if ($navigations[$id]) {
							$json['isFolder'] = true;
							if ($row['parent']==0) $json['expand'] = true;
							$json['children'] = _navigation_json($id);
						}
						else {
							unset($json['children']);
							$json['isFolder'] = false;
						}
							
						$return[] = $json;
					}
				}
					
				return $return;
			}

			$result = $model->query("
				SELECT DISTINCT
					role_navigation_navigation,
					role_navigation_role
				FROM role_navigations
			")
			->filter('role', 'role_navigation_role = '.$_REQUEST['role'])
			->fetchAll();
			
			$roles = _array::extract($result);
			
			$result = Navigation::load();	
			$navigations = Navigation::buildTree($result);
			$json = _navigation_json(0);
			
			echo json_encode($json);
				
		break;
		
		case 'check_url': 
			
			$url = trim($_REQUEST['url'], '/');
			$navigation = new Navigation();
			
			if ($_REQUEST['id']) {
				
				$result = $navigation->read($_REQUEST['id']);
				
				if ($result['navigation_url']==$url) {
					$response = true;
					$id = $result['navigation_id'];
				}
				else {
					$result = $navigation->read_from_request($url);
					$response = ($result) ? true : false;
				}
			}
			else {
				$result = $navigation->read_from_request($url);
				$response = ($result) ? true : false;
			}
			
			echo json_encode(array(
				'response' => $response,
				'id' => $id
			));
			
		break;
	}
