<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	
	$model = new Model(Connector::DB_CORE);

	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "address_country, address_company";
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;

	$bindTables = array(
		Company::DB_BIND_ADDRESS_TYPES,
		Company::DB_BIND_COUNTRIES,
		Company::DB_BIND_PLACES,
		Company::DB_BIND_PROVINCES
	);


	// filter: search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search ) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			address_company LIKE \"%$keyword%\"
			OR address_number LIKE \"%$keyword%\"
			OR address_mps_customernumber LIKE \"%$keyword%\"
			OR address_mps_shipto LIKE \"%$keyword%\"
			OR country_name LIKE \"%$keyword%\"
			OR province_canton LIKE \"%$keyword%\"
			OR address_zip LIKE \"%$keyword%\"
		)";
	}

	// filter address types
	if ($_REQUEST['address_types']) {
		$filters['address_types'] = "address_type = ".$_REQUEST['address_types'];
	}
	
	// filter: countries
	if ($_REQUEST['countries']) {
		$filters['countries'] = "address_country = ".$_REQUEST['countries'];
	}
	
	// filter: for limited view, show only projects where user is partner of it
	if (!user::permission(Red_Project::PERMISSION_VIEW_PROJECTS) && user::permission(Red_Project::PERMISSION_VIEW_LIMITED_PROJECTS)) {
		array_push($bindTables, Red_Project::BIND_PROJECT_PARTNER);
		array_push($bindTables, Red_Project::BIND_USER_ID);
		$filters['limited'] = "user_id = ".$user->data['user_id'];
	}


	// show only addresses of type 1,2,6 and AND where address_involded_in_red is true
	$filters['address_type'] = "address_type in (1,2,5,6,12)";
	$filters['involved_in_red'] = "address_involved_in_red = 1";
	$filters['address_active'] = "address_active = 1";

 	// partners
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS
			address_id,
			address_number,
			address_mps_customernumber,
			address_mps_shipto,
	 		address_company,
			address_zip,
			address_place_id,
			address_country,
			address_involved_in_red,
			country_name,
			province_canton,
			place_name,
			address_type_name
		FROM addresses
	")
	->bind($bindTables)
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();

	if ($result) {
		
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));

		$pageIndex = $pager->index();
		$pageControlls = $pager->controlls();
	}

	// hidden toolbox fields
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

	// toolbox: button add
	if ($_REQUEST['add']) {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'label' => $translate->add_new,
			'href' => 	$_REQUEST['add']	
		));
	}
	
	// toolbox: search full text
	$toolbox[] = ui::searchbox();

	// toolbox: Countries
	$types = $model->query("
		SELECT DISTINCT
		   	country_id,
			country_name
		FROM addresses
	")
	->bind($bindTables)
	->filter($filters)
	->order('country_name', 'asc')
	->exclude('countries')
	->fetchAll();

	$toolbox[] = ui::dropdown($types, array(
		'name' => 'countries',
		'id' => 'countries',
		'value' => $_REQUEST['countries'],
		'class' => 'submit',
		'label' => $translate->all_countries
	));

	// toolbox: Address types
	$types = $model->query("
		SELECT DISTINCT 
		   	address_type_id,
			address_type_name,
			address_type_code
		FROM addresses
	")
	->bind($bindTables)
	->filter($filters)
	->order('address_type_name', 'asc')
	->exclude('address_types')
	->fetchAll();

	$toolbox[] = ui::dropdown($types, array(
		'name' => 'address_types',
		'id' => 'address_types',
		'value' => $_REQUEST['address_types'],
		'class' => 'submit',
		'caption' => $translate->address_types
	));


	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}

	$table = new Table(array(
		'sort' => array(
			'column' => $sort,
			'direction' => $direction
		)
	));

	$table->datagrid = $datagrid;


	$table->country_name(
		Table::PARAM_SORT
	);

	$table->address_company(
		'href='.$_REQUEST['form'],
		Table::PARAM_SORT
	);

	$table->address_type_name(
		Table::PARAM_SORT
	);

	$table->address_mps_customernumber(
		Table::PARAM_SORT
	);

	$table->address_mps_shipto(
		Table::PARAM_SORT
	);

	$table->province_canton(
		Table::PARAM_SORT
	);


	$table->place_name(
		Table::PARAM_SORT
	);

	$table->address_zip(
		Table::PARAM_SORT
	);


	$table->footer($pageIndex);
	$table->footer($pageControlls);
	$table = $table->render();

	echo $toolbox.$table;
