<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['mps_announcement_id'];
	
	// anouncements file path
	$upload_path = "/public/data/files/$application/announcements";
	
	$data = array();
	
	if ($_REQUEST['mps_announcement_date']) {
		$data['mps_announcement_date'] = date::sql($_REQUEST['mps_announcement_date']);
	}
	
	if ($_REQUEST['mps_announcement_expiry_date']) {
		$data['mps_announcement_expiry_date'] = date::sql($_REQUEST['mps_announcement_expiry_date']);
	}
	
	if ($_REQUEST['mps_announcement_text']) {
		$data['mps_announcement_text'] = $_REQUEST['mps_announcement_text'];
	}
	
	if ($_REQUEST['mps_announcement_title']) {
		$data['mps_announcement_title'] = $_REQUEST['mps_announcement_title'];
	}
	
	if (in_array('mps_announcement_important', $fields)) {
		$data['mps_announcement_important'] = ($_REQUEST['mps_announcement_important']) ? 1 : 0;
	}
	
	if ($_REQUEST['mps_announcement_file_title']) {
		$data['mps_announcement_file_title'] = $_REQUEST['mps_announcement_file_title'];
	}
	
	if ($_REQUEST['has_upload'] && $_REQUEST['mps_announcement_file']) {
		$data['mps_announcement_file'] = upload::move($_REQUEST['mps_announcement_file'], $upload_path);
	}
	
	if($data) { 
		
		$announcement = new Announcement($application);
		$announcement->read($id);
	
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $announcement->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $announcement->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			if ($_REQUEST['redirect']) {
				$redirect = $_REQUEST['redirect'].'/'.$id;
			}
		}

		if ($response && $_REQUEST['has_upload']) {
			$path = $data['mps_announcement_file'];
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}

	echo json_encode(array(
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message,
		'path' => $path
	));
