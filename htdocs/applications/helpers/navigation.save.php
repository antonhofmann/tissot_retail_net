<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// form visible fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// mvc vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$redirect = $_REQUEST['redirect'];
	
	$id = $_REQUEST['navigation_id'];
	$language = 36;
	$caption = $_REQUEST['navigation_caption'];
	
	// db model
	$model = new Model(Connector::DB_CORE);
	
	$data = array();
	
	if (isset($_REQUEST['navigation_parent'])) {
		$data['navigation_parent'] = $_REQUEST['navigation_parent'];
	}
	
	if ($_REQUEST['navigation_url']) {
		$data['navigation_url'] = trim($_REQUEST['navigation_url'], '/');
	}
	
	if (in_array('navigation_tab', $fields)) {
		$data['navigation_tab'] = (isset($_REQUEST['navigation_tab'])) ? 1 : 0;
	}
	
	if (in_array('navigation_active', $fields)) {
		$data['navigation_active'] = (isset($_REQUEST['navigation_active'])) ? 1 : 0;
	}
	
	
	if ($data && $language && $caption) {
		
		$navigation = new Navigation();
		
		if ($id) {
			
			$navigation->read($id);
			$response = $navigation->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$result = $model->query("
				SELECT COUNT(navigation_id) AS total
				FROM navigations
				WHERE navigation_parent = ".$_REQUEST['navigation_parent']."
			")->fetch();
				
			$position = ($result['total']) ? $result['total'] : 0;
			
			$data['navigation_order'] = $position++;
			
			$response = $id = $navigation->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = ($redirect) ? "$redirect/$id" : null;
			
			// insert system administrator role
			if ($response) {
				$model->db->query("
					INSERT INTO role_navigations (role_navigation_role, role_navigation_navigation)
					VALUES (1,$id)
				");
			}
		}
		
		if ($response) {
			
			$navigation_language = new Navigation_Language();
			$navigation_language->read_from_navigation($id, $language);
			
			$data = array(
				'navigation_language_language' => $language,
				'navigation_language_navigation' => $id,
				'navigation_language_name' => $caption
			);
			
			if ($navigation_language->id) {
				$navigation_language->update($data);
			} else {
				$navigation_language->create($data);
			}
		}
	}
	else {
		$response = false;
		$message = ($errors) ? join('<br />', $errors) : $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	