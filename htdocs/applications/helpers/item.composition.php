<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// disabled fields
$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

// url request
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];

// product line
$item = $_REQUEST['item_composition_item'];

if ($item) {

	$model = new Model();

	// remove all groups
	$model->db->exec("
		DELETE FROM item_compositions
		WHERE item_composition_item = $item
	");

	if ($_REQUEST['item_group_id']) {

		$userlogin = User::instance()->login;

		foreach ($_REQUEST['item_group_id'] as $group => $value) {
			$model->db->exec("
				INSERT INTO item_compositions (
					item_composition_item,
					item_composition_group,
					user_created,
					date_created
				)
				VALUES ( $item, $group, '$userlogin', NOW() )
			");
		}
	}

	$response = true;
	$message = $translate->message_request_submitted;
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message
));
	