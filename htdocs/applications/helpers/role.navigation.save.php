<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$role = $_REQUEST['role'];
	$id = $_REQUEST['navigation'];
	$selected = $_REQUEST['selected'];
	
	if ($role && $id) {
		
		$model = new Model(Connector::DB_CORE);
		
		if ($selected) {
			
			$sth = $model->db->query("
				INSERT INTO role_navigations (
					role_navigation_role, 
					role_navigation_navigation
				)
				VALUES ($role, $id)
			");
			
			$response = ($sth) ? true : false;
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		} 
		else {
			
			$navigation = new Navigation();
			$navigation->read($id);
			$navigation->get_childs();
			$childs = $navigation->childs;
			
			$sth = $model->db->query("
				DELETE FROM role_navigations
				WHERE role_navigation_role = $role AND role_navigation_navigation = $id
			");
			
			if ($sth && $childs && $id<>1 )  {
				foreach ($childs as $key) {
					$model->db->query("
						DELETE FROM role_navigations
						WHERE role_navigation_navigation = $key AND role_navigation_role = $role
					");
				}
			}

			$response = ($sth) ? true : false;
			$message = ($response) ? $translate->message_request_deleted : $translate->message_request_failure;
			$select = false;
		}

		if ($response) {
			
			// user pemissions
			$permissions = permission::loader(null,true);
			$array = array();
			
			if ($permissions) {
				foreach ($permissions as $key => $permission) {
					$array[] = $permission;
				}
				session::set('user_permissions', join(' ', $array));
			}
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'childs' => $childs,
		'select' => $select
	));
	