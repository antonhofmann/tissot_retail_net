<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	$mps_sap_hq_ordertype_id = $_REQUEST['mps_sap_hq_ordertype_id'];
	
	$data = array();
	
	if($mps_sap_hq_ordertype_id > 0) {
		$data['mps_sap_hq_ordertype_id'] = $mps_sap_hq_ordertype_id;
	}
		
	
	if (in_array('mps_sap_hq_ordertype_address_id', $fields)) {
		$data['mps_sap_hq_ordertype_address_id'] = $_REQUEST['mps_sap_hq_ordertype_address_id'];
	}

	if (in_array('mps_sap_hq_ordertype_customer_number', $fields)) {
		$data['mps_sap_hq_ordertype_customer_number'] = $_REQUEST['mps_sap_hq_ordertype_customer_number'];
	}
	
	if (in_array('mps_sap_hq_ordertype_shipto_number', $fields)) {
		$data['mps_sap_hq_ordertype_shipto_number'] = $_REQUEST['mps_sap_hq_ordertype_shipto_number'];
	}

	if (in_array('mps_sap_hq_ordertype_sap_hq_ordertype_id', $fields)) {
		$data['mps_sap_hq_ordertype_sap_hq_ordertype_id'] = $_REQUEST['mps_sap_hq_ordertype_sap_hq_ordertype_id'];
	}
	

	$data['mps_sap_hq_ordertype_sap_hq_orderreason_id'] = 0; //epmty drop down value
	if (in_array('mps_sap_hq_ordertype_sap_hq_orderreason_id', $fields) 
		and $_REQUEST['mps_sap_hq_ordertype_sap_hq_orderreason_id'] > 0) {
		$data['mps_sap_hq_ordertype_sap_hq_orderreason_id'] = $_REQUEST['mps_sap_hq_ordertype_sap_hq_orderreason_id'];
	}

	if (in_array('mps_sap_hq_ordertype_is_default', $fields)) {
		$data['mps_sap_hq_ordertype_is_default'] = $_REQUEST['mps_sap_hq_ordertype_is_default'];
	}

	if (in_array('mps_sap_hq_ordertype_is_free_goods', $fields)) {
		$data['mps_sap_hq_ordertype_is_free_goods'] = $_REQUEST['mps_sap_hq_ordertype_is_free_goods'];
	}

	if (in_array('mps_sap_hq_ordertype_is_for_dummies', $fields)) {
		$data['mps_sap_hq_ordertype_is_for_dummies'] = $_REQUEST['mps_sap_hq_ordertype_is_for_dummies'];
	}

	$mps_sap_hq_ordertype = new Company_Mps_sap_hq_ordertype();

	if ($mps_sap_hq_ordertype_id > 0 && $data) {
	
			$mps_sap_hq_ordertype->id = $mps_sap_hq_ordertype_id;
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $mps_sap_hq_ordertype->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	elseif($data) {
		
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		
		$response = $id = $mps_sap_hq_ordertype->create($data);
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		
		if ($response && $_REQUEST['redirect']) {
			$redirect = ($_REQUEST['controller']=='mps_sap_hq_ordertypes') ? $_REQUEST['redirect']."/$id" : $_REQUEST['redirect'];
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	