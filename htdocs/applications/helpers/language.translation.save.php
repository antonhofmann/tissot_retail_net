<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['translation_keyword'];
	
	// translation language
	$language = $_REQUEST['translation_language'];
	
	// model
	$model = new Model();
	
	$translation = new Translation();
	
	// add new keyword
	if ($language && $_REQUEST['keyword'] && isset($_REQUEST['content'])) {
	
		$keyword = strtolower(str_replace(' ', '_', $_REQUEST['keyword']));
	
		$sth = $model->db->query("
			INSERT INTO translation_keywords (translation_keyword_name, user_created)
			VALUES ('$keyword', '".$user->login."')
		");
	
		$id = $model->db->lastInsertId();
		
		if ($id) {
			
			// add keyword translation
			$response = $translation->create(array(
				'translation_language' => $language,
				'translation_category' => 1,
				'translation_keyword' => $id,
				'translation_content' => $_REQUEST['content'],
				'user_created' => $user->login
			));
		}
		
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		$redirect = ($response && $_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : '';
	} 
	// update keyword translations
	elseif ($language && $id) {
		
		// translation keyword
		$result = $model->query("
			SELECT translation_keyword_name
			FROM translation_keywords
			WHERE translation_keyword_id = $id
		")->fetch();
			
		$keyword = $result['translation_keyword_name'];
		
		// translation categories
		$categories = $model->query("
			SELECT *
			FROM translation_categories
		")->fetchAll();
		
		// get translation categories
		if ($categories) {
			foreach ($categories as $row) {
				$key = $row['translation_category_id'];
				$translation_fields[$key] = $row['translation_category_name'].'_'.$keyword;
			}
		}

		if ($translation_fields) {

			// remove all keyword translations
			$model->db->query("
				DELETE FROM translations
				WHERE translation_keyword = $id
			");

			foreach ($translation_fields as $category => $field) {

				// insert new translation
				if ($_REQUEST[$field]) {
					$response = $translation->create(array(
						'translation_language' => $language,
						'translation_category' => $category,
						'translation_keyword' => $id,
						'translation_content' => $_REQUEST[$field],
						'user_created' => $user->login
					));
				}
			}
		}
		
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	