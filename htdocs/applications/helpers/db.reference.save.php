<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['db_reference_id'];
	
	$data = array();
	
	if ($_REQUEST['db_reference_referenced_table_id']) {
		$data['db_reference_referenced_table_id'] = $_REQUEST['db_reference_referenced_table_id'];
	}
	
	if ($_REQUEST['db_table_db'] && $_REQUEST['db_reference_referring_table']) {
		
		$model = new Model(Connector::DB_CORE);
		
		$sth = $model->db->query("
			SELECT db_table_id
			FROM db_tables
			WHERE db_table_db = ".$_REQUEST['db_table_db']." AND db_table_table = '".$_REQUEST['db_reference_referring_table']."'
		");
		
		$result = ($sth) ? $sth->fetch() : null;
		$data['db_reference_referring_table'] = $result['db_table_id'];
	}
	
	if ($_REQUEST['db_reference_foreign_key']) {
		$data['db_reference_foreign_key'] = $_REQUEST['db_reference_foreign_key'];
	}
	
	if ($data && $_REQUEST['db_reference_referenced_table_id']) {
		
		$db_reference = new DB_Reference();
		$db_reference->read($id);
	
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $db_reference->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $db_reference->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));
	