<?php 
/**
 * 	Master Sheet Consolidated Version
 * 
 * 	Save all master sheet items as consolidated splitting list.
 * 	
 * 	@author: admir.serifi@mediaparx.ch
 * 	@copyright (c) Mediaparx Ag
 *  @version 1.1
 */

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	$title = $_REQUEST['title'];
	
	if ($id && $title) {
		
		$model = new Model($application);
		
		// master sheet
		$mastersheet = new Mastersheet($application);
		$mastersheet->read($id);

		// create master sheet version		
		$inserted = $mastersheet->splittinglist()->create(array(
			'mps_mastersheet_splitting_list_name' => $title,
			'mps_mastersheet_splitting_list_mastersheet_id' => $id,
			'user_created' => $user->login,
			'date_created'	=> date('Y-m-d H:i:s')
		));
		
		if ($inserted) {
			
			// get mastersheet items
			// ordersheet items
			$mastersheet_items = $model->query("
				SELECT DISTINCT
					mps_ordersheet_item_id,
					mps_ordersheet_item_material_id,
					mps_ordersheet_item_price,
					SUM(mps_ordersheet_item_quantity) AS quantity,
					SUM(mps_ordersheet_item_quantity_approved) AS approved,
					mps_ordersheet_item_currency,
					mps_ordersheet_item_exchangrate,
					mps_ordersheet_item_factor
				FROM mps_ordersheet_items
			")
			->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
			->filter('mastersheet', "mps_ordersheet_mastersheet_id = $id")
			->group('mps_ordersheet_item_material_id')
			->fetchAll();
			
			if ($mastersheet_items) {
				
				// set instance for this splitting list
				$mastersheet->splittinglist()->read($inserted);
				
				foreach ($mastersheet_items as $item) {
					$mastersheet->splittinglist()->item()->create(array(
						'mps_mastersheet_splitting_list_item_splittinglist_id' => $inserted,
						'mps_mastersheet_splitting_list_item_material_id' => $item['mps_ordersheet_item_material_id'],
						'mps_mastersheet_splitting_list_item_price' => $item['mps_ordersheet_item_price'],
						'mps_mastersheet_splitting_list_item_total_quantity' => $item['quantity'],
						'mps_mastersheet_splitting_list_item_total_quantity_approved' => $item['approved'],
						'mps_mastersheet_splitting_list_item_currency' => $item['mps_ordersheet_item_currency'],
						'mps_mastersheet_splitting_list_item_exchangrate' => $item['mps_ordersheet_item_exchangrate'],
						'mps_mastersheet_splitting_list_item_factor' => $item['mps_ordersheet_item_factor'],
						'user_created' => $user->login,
						'date_created' => date('Y-m-d H:i:s')
					));
				}
			}
			
			$response = true;
			$message = $translate->message_request_inserted;
		}
	}
	else {
		$response = true;
		$message = $translate->error_request;
	}
	
	// action message
	//$inserted) ?  Message::request_inserted() : Message::request_failure();
	//url::redirect("/$application/$controller/$action/$id");
	
	// response with json header
	header('Content-Type: text/json');
	echo json_encode(array(
		'success' => $response,
		'message' => $message		
	));
	
	