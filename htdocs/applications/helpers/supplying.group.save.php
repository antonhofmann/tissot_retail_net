<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// disabled fields
$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

// url request
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];

// product line
$id = $_REQUEST['supplying_group_id'];

$data = array();	

if (in_array('supplying_group_name', $fields)) {
	$data['supplying_group_name'] = $_REQUEST['supplying_group_name'];
}	

if (in_array('supplying_group_active', $fields)) {
	$data['supplying_group_active'] = $_REQUEST['supplying_group_active'];
}	

if ($data) {

	$supplyingGroup = new Supplying_Group();
	$supplyingGroup->read($id);

	if ($supplyingGroup->id) {
		
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		
		$response = $supplyingGroup->update($data);
		
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		
		$response = $id = $supplyingGroup->create($data);
		
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		
		$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
	}
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect
));
