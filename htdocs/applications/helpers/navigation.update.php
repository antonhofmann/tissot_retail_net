<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$id = $_REQUEST['id'];
	$parent = $_REQUEST['parent'];
	$order = $_REQUEST['order'];
	
	if ($id && $order) { 
		
		$model = new Model(Connector::DB_CORE);
		
		$model->db->query("
			UPDATE navigations SET navigation_parent = $parent
			WHERE navigation_id = $id	
		");
		
		foreach ($order as $key => $row) {
			$model->db->query("
				UPDATE navigations SET navigation_order = ".$row['order']."
				WHERE navigation_id = ".$row['id']."
			");
		}
		
		$response = true;
		$message = $translate->message_request_updated;
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));