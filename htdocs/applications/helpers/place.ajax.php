<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$model = new Model(Connector::DB_CORE);
	
	$id = $_REQUEST['id'];
	$country = $_REQUEST['country'];
	$province = $_REQUEST['province'];
	$name = $_REQUEST['name'];
	
	switch ($_REQUEST['section']) {
		
		case 'get_from_name':

			$json = $model->query("
				SELECT place_id
				FROM places
				WHERE place_province = $province AND place_name = '$name'	
			")->fetch();
		
		break;
		
		case 'add';
		
			if ($country && $province && $name) {
				
				$place = new Place();
				
				$place->create(array(
					'place_country' => $country,
					'place_province' => $province,
					'place_name' => $name,
					'user_created' => User::instance()->login,
					'date_created' => date('Y-m-d H:i:s')	
				));
				
				$json = $place->data;
			}
		
		break;
		
		case 'remove':
		
		break;
	}
	
	header('Content-Type: text/json');
	echo json_encode($json);