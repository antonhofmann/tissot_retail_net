<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$id = $_REQUEST['red_project_id'];
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$data = array();

	// red_project_context
	if (in_array('red_project_context', $fields)) {
		$data['red_project_context'] = $_REQUEST['red_project_context'];
	}

	// red_project_description
	if (in_array('red_project_description', $fields)) {
		$data['red_project_description'] = $_REQUEST['red_project_description'];
	}
	
	// red_project_restrictions
	if (in_array('red_project_restrictions', $fields)) {
		$data['red_project_restrictions'] = $_REQUEST['red_project_restrictions'];
	}
	
	// red_project_timeline
	if (in_array('red_project_timeline', $fields)) {
		$data['red_project_timeline'] = $_REQUEST['red_project_timeline'];
	}

	// red_project_enddate
	if (in_array('red_project_enddate', $fields)) {
		$data['red_project_enddate'] = ($_REQUEST['red_project_enddate']) ? date::sql($_REQUEST['red_project_enddate']) : null;
	}

	// red_project_external_partners
	if (in_array('red_project_external_partners', $fields)) {
		$data['red_project_external_partners'] = $_REQUEST['red_project_external_partners'];
	}

	// red_project_foreseen_enddate
	if (in_array('red_project_foreseen_enddate', $fields)) {
		$data['red_project_foreseen_enddate'] = ($_REQUEST['red_project_foreseen_enddate']) ? date::sql($_REQUEST['red_project_foreseen_enddate']) : null;
	}

	// red_project_projectnumber
	if (in_array('red_project_projectnumber', $fields)) {
		$data['red_project_projectnumber'] = $_REQUEST['red_project_projectnumber'];
	}

	// red_project_startdate
	if (in_array('red_project_startdate', $fields)) {
		$data['red_project_startdate'] = ($_REQUEST['red_project_startdate']) ? date::sql($_REQUEST['red_project_startdate']) : null;
	}

	// red_project_title
	if (in_array('red_project_title', $fields)) {
		$data['red_project_title'] = $_REQUEST['red_project_title'];
	}

	// red_projectstate_name
	if (in_array('red_project_projectstate_id', $fields)) {
		$data['red_project_projectstate_id'] = $_REQUEST['red_project_projectstate_id'];
	}

	// red_projecttype_name
	if (in_array('red_project_projecttype_id', $fields)) {
		$data['red_project_projecttype_id'] = $_REQUEST['red_project_projecttype_id'];
	}

	// red_project_leader_user_id
	if (in_array('red_project_leader_user_id', $fields)) {
		$data['red_project_leader_user_id'] = $_REQUEST['red_project_leader_user_id'];
	}
	
	// red_project_leader_user_id
	if (in_array('red_project_assistant_user_id', $fields)) {
		$data['red_project_assistant_user_id'] = $_REQUEST['red_project_assistant_user_id'];
	}

	if ($data) {
		
		$project = new Red_Project($application);
		$project->read($id);
		
		if ($id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $project->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $project->create($data);
			$message = ($response) ? $translate->message_request_inserted : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}

	echo json_encode(array(
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message
	));
