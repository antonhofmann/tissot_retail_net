<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	
if ($_REQUEST['ordersheets'] && $_REQUEST['mail_template_id']) {

	$user = User::instance();
	$translate = Translate::instance();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	// ordersheet(s)
	$ordersheets = is_array($_REQUEST['ordersheets']) ? array_keys($_REQUEST['ordersheets']) : explode(',', $_REQUEST['ordersheets']);
	
	// set template as integer
	$template = $_REQUEST['mail_template_id'];
	settype($template, "integer");
	
	// ajax responding
	$json['response'] = true;
		
	// action mail
	$actionmail = new ActionMail($template);
		
	// set main parameters
	$actionmail->param()->application = $application;
	$actionmail->param()->ordersheets = $ordersheets;
		
	// set mail template data
	if ($_REQUEST['mail_template_view_modal']) {
		$actionmail->content()->subject = $_REQUEST['mail_template_subject'];
		$actionmail->content()->body = $_REQUEST['mail_template_text'];
	}
		
	// send mails
	$actionmail->send();
	
	// console messages
	$json['console'] = $actionmail->response()->console;
		
	// total send mails
	$totalSendMails = count($actionmail->response()->sendmails);
		
	// notification
	if ($totalSendMails) {
		if ($json['reload']) {
			Message::add(array(
			'keyword' => "Total send $totalSendMails E-mails."
			));
		} else {
			$json['notification'] = array(
				'content' => "Total send $totalSendMails E-mails"
			);
		}
	}
	
} else {

	if (!$_REQUEST['ordersheets']) {
		$message = "Select at least one order sheet. ";
	}

	if (!$_REQUEST['mail_template_id']) {
		$message .= "E-mail template not found.";
	}
	
	$json = array(
		'response' => false,
		'notification' => array(
			'content' => "Bad request. $message",
			'properties' => array(
				'theme' => 'error'
			)
		)
	);
}
	
	
if (!headers_sent()) {
	header('Content-Type: text/json');
}


echo json_encode($json);

