<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$id = $_REQUEST['red_projectsheet_id'];
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	$red_projectsheet_project_id = $_REQUEST['red_projectsheet_project_id'];
	$upload_path = Red_Project::PROJECT_FILE_UPLOAD_PATH.$red_projectsheet_project_id."/sheets";

	$data = array();

	// red_projecttype_name
	if ($_REQUEST['red_projecttype_name']) 									$data['red_projecttype_name'] = $_REQUEST['red_projecttype_name'];
	if (isset($_REQUEST['red_projectsheet_project_id'])) 					$data['red_projectsheet_project_id'] 				= $_REQUEST['red_projectsheet_project_id'];
	if (isset($_REQUEST['red_projectsheet_business_unit_id'])) 				$data['red_projectsheet_business_unit_id'] 			= $_REQUEST['red_projectsheet_business_unit_id'];
	if (isset($_REQUEST['red_projectsheet_costcenter_id'])) 				$data['red_projectsheet_costcenter_id'] 			= $_REQUEST['red_projectsheet_costcenter_id'];
	if (isset($_REQUEST['red_projectsheet_accountnumber_id'])) 				$data['red_projectsheet_accountnumber_id']			 = $_REQUEST['red_projectsheet_accountnumber_id'];
	if (isset($_REQUEST['red_projectsheet_currency_id'])) 					$data['red_projectsheet_currency_id']				= $_REQUEST['red_projectsheet_currency_id'];
	if (isset($_REQUEST['red_projectsheet_openingdate'])) 					$data['red_projectsheet_openingdate'] 				= date::sql($_REQUEST['red_projectsheet_openingdate']);
	if (isset($_REQUEST['red_projectsheet_plannedamount_current_year'])) 	$data['red_projectsheet_plannedamount_current_year'] = $_REQUEST['red_projectsheet_plannedamount_current_year'];
	if (isset($_REQUEST['red_projectsheet_share_swatch'])) 					$data['red_projectsheet_share_swatch'] 				= $_REQUEST['red_projectsheet_share_swatch'];
	if (isset($_REQUEST['red_projectsheet_share_others'])) 					$data['red_projectsheet_share_others'] 				= $_REQUEST['red_projectsheet_share_others'];
	if (isset($_REQUEST['red_projectsheet_totalbudget']))					$data['red_projectsheet_totalbudget'] 				= $_REQUEST['red_projectsheet_totalbudget'];
	if (isset($_REQUEST['red_projectsheet_alreadycommitted'])) 				$data['red_projectsheet_alreadycommitted'] 			= $_REQUEST['red_projectsheet_alreadycommitted'];

	if (isset($_REQUEST['red_projectsheet_alreadyspent'])) 					$data['red_projectsheet_alreadyspent'] 				= $_REQUEST['red_projectsheet_alreadyspent'];
	if (isset($_REQUEST['red_projectsheet_approvalname01'])) 				$data['red_projectsheet_approvalname01'] 			= $_REQUEST['red_projectsheet_approvalname01'];
	if ($_REQUEST['red_projectsheet_approval_deadline01'])
		$data['red_projectsheet_approval_deadline01'] = date::sql($_REQUEST['red_projectsheet_approval_deadline01']);
	else
		$data['red_projectsheet_approval_deadline01'] = null;

	if (isset($_REQUEST['red_projectsheet_approvalname02'])) 				$data['red_projectsheet_approvalname02']			= $_REQUEST['red_projectsheet_approvalname02'];
	if ($_REQUEST['red_projectsheet_approval_deadline02'])
		$data['red_projectsheet_approval_deadline02'] 		= date::sql($_REQUEST['red_projectsheet_approval_deadline02']);
	else
		$data['red_projectsheet_approval_deadline02'] = null;

	if (isset($_REQUEST['red_projectsheet_approvalname03'])) 				$data['red_projectsheet_approvalname03'] 			= $_REQUEST['red_projectsheet_approvalname03'];
	if ($_REQUEST['red_projectsheet_approval_deadline03'])
		$data['red_projectsheet_approval_deadline03'] 		= date::sql($_REQUEST['red_projectsheet_approval_deadline03']);
	else
		$data['red_projectsheet_approval_deadline03'] = null;

	if (isset($_REQUEST['red_projectsheet_approvalname04'])) 				$data['red_projectsheet_approvalname04'] 			= $_REQUEST['red_projectsheet_approvalname04'];
	if ($_REQUEST['red_projectsheet_approval_deadline04'])
		$data['red_projectsheet_approval_deadline04']		= date::sql($_REQUEST['red_projectsheet_approval_deadline04']);
	else
		$data['red_projectsheet_approval_deadline04'] = null;

	if (isset($_REQUEST['red_projectsheet_approvalname05'])) 				$data['red_projectsheet_approvalname05'] 			= $_REQUEST['red_projectsheet_approvalname05'];
	if ($_REQUEST['red_projectsheet_approval_deadline05'])
		$data['red_projectsheet_approval_deadline05'] 		= date::sql($_REQUEST['red_projectsheet_approval_deadline05']);
	else
		$data['red_projectsheet_approval_deadline05'] = null;

	if (isset($_REQUEST['red_projectsheet_approvalname06']))				$data['red_projectsheet_approvalname06'] 			= $_REQUEST['red_projectsheet_approvalname06'];
	if ($_REQUEST['red_projectsheet_approval_deadline06'])
		$data['red_projectsheet_approval_deadline06'] 		= date::sql($_REQUEST['red_projectsheet_approval_deadline06']);
	else
		$data['red_projectsheet_approval_deadline06'] = null;


	if (isset($_REQUEST['red_projectsheet_financial_justification'])) 		$data['red_projectsheet_financial_justification'] 	= $_REQUEST['red_projectsheet_financial_justification'];


	if ($_REQUEST['red_projectsheet_mainsupplier_partner_id']) {
		$data['red_projectsheet_mainsupplier_partner_id'] = $_REQUEST['red_projectsheet_mainsupplier_partner_id'];
	}
	else {
		$data['red_projectsheet_mainsupplier_partner_id'] = '';
	}


	//has_upload
	if ($_REQUEST['has_upload'] && $_REQUEST['red_projectsheet_signed_file_path']) {
		$path = upload::move($_REQUEST['red_projectsheet_signed_file_path'], $upload_path);
		$data['red_projectsheet_signed_file_path'] = $path;
	}


	if ($data) {
		
		// project sheet
		$projectSheet = new Red_Project_Sheet($application);
		$projectSheet->read($id);
		
		if ($projectSheet->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $projectSheet->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $projectSheet->create($data);
			$message = ($response) ? message::request_inserted() : $translate->message_request_failure;
			$redirect = "/$application/$controller/sheet/$id";
		}
	}

	echo json_encode(array(
		'path' => $path,
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message
	));
