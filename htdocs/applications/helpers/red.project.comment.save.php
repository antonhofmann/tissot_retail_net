<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	$application = $_REQUEST['application'];
	$project = $_REQUEST['red_comment_project_id'];
	$comment = $_REQUEST['red_comment_id'];
	
	$data = array();

	if (in_array('red_comment_project_id', $fields)) {
		$data['red_comment_project_id'] = $_REQUEST['red_comment_project_id'];
	}

	if (in_array('red_comment_category_id', $fields)) {
		$data['red_comment_category_id'] = $_REQUEST['red_comment_category_id'];
	}

	if (in_array('red_comment_owner_user_id', $fields)) {
		$data['red_comment_owner_user_id'] = $_REQUEST['red_comment_owner_user_id'];
	}

	if (in_array('red_comment_comment', $fields)) {
		$data['red_comment_comment'] = $_REQUEST['red_comment_comment'];
	}

	if ($_REQUEST['red_comment_file_id']) {

		$file_list = explode(',', $_REQUEST['red_comment_file_id']);
		
		$file_array = array();
		
		$file = new Red_File($application);
		
		foreach ($file_list as $file_id) {
			
			$file->read($file_id);
			
			if ($file->id) {
				$file_array[] = $file_id;
			}
		}
		
		$data['red_comment_file_id'] = serialize($file_array);
	}

	if ($data && $project) { 
	
		$projectComment = new Red_Comment($application);
		$projectComment->read($comment);
		
		if ($projectComment->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $projectComment->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $comment = $projectComment->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$comment" : null;
		}
		
		// partners accesses
		if ($response) {
			
			$commentAcces = new Red_Comment_Access($application);
			$commentAcces->comment($comment);
			
			// delete all existing accesses
			$commentAcces->deleteAll();
			
			// add new partner accesses
			if ($_REQUEST['partners']) {
				foreach ($_REQUEST['partners'] as $partner) {
					$commentAcces->create($partner);
				}
			}
		}
	}

	echo json_encode(array(
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message
	));
