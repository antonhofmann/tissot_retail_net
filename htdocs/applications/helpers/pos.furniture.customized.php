<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$model = new Model($application);
	
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "mps_pos_furniture_description";
	
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rows = $settings->limit_pager_rows;
	$offset = ($page-1) * $rows;

	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			mps_pos_furniture_id, 
			IF(
				LENGTH(mps_pos_furniture_description) > 90,
				CONCAT(LEFT(mps_pos_furniture_description, 90), '..'),
				mps_pos_furniture_description
			) AS mps_pos_furniture_description,
			mps_pos_furniture_quantity_in_use, 
			mps_pos_furniture_quantity_on_stock, 
			mps_pos_furniture_installation_year, 
			mps_pos_furniture_deinstallation_year
		FROM mps_pos_furniture
	")
	->filter('customized', "(mps_pos_furniture_item_id IS NULL OR mps_pos_furniture_item_id = '')")
	->filter('pos', "mps_pos_furniture_posaddress = $id")
	->order($sort, $direction)
	->offset($offset, $rows)
	->fetchAll();

	if ($result) {
		
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));
		
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	
	// toolbox: add
	if ($_REQUEST['add-customized'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add-customized'],
			'label' => $translate->add_customized_furniture
		));
	}
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;

	
	$table->mps_pos_furniture_description(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		"href=".$_REQUEST['form']
	);
	
	$table->mps_pos_furniture_quantity_in_use(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		Table::PARAM_SORT,
		'width=5%'
	);
	
	$table->mps_pos_furniture_quantity_on_stock(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		Table::PARAM_SORT,
		'width=5%'
	);
	
	$table->mps_pos_furniture_installation_year(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->mps_pos_furniture_deinstallation_year(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->item_width(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=5%'
	);
	
	$table->item_height(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=5%'
	);
	
	$table->item_length(
		Table::ATTRIBUTE_NOWRAP,
		'width=5%'
	);
	
	$table->item_radius(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=5%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo "<h5>$translate->customized_furniture</h5>";
	echo $toolbox;
	echo $table;
	