<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['mps_material_planning_type_id'];
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	$data = array();
	
	if ($_REQUEST['mps_material_planning_type_name']) {
		$data['mps_material_planning_type_name'] = $_REQUEST['mps_material_planning_type_name'];
	}
	
	if (isset($_REQUEST['mps_material_planning_type_description'])) {
		$data['mps_material_planning_type_description'] = $_REQUEST['mps_material_planning_type_description'];
	}

	if (in_array('mps_material_planning_is_dummy', $fields)) {
		$data['mps_material_planning_is_dummy'] = $_REQUEST['mps_material_planning_is_dummy'] ? 1 : null;
	}

	if (in_array('mps_material_planning_ordersheet_standrad_dummy_quantity', $fields)) {
		$data['mps_material_planning_ordersheet_standrad_dummy_quantity'] = $_REQUEST['mps_material_planning_ordersheet_standrad_dummy_quantity'] ? 1 : null;
	}

	if (in_array('mps_material_planning_ordersheet_approvment_automatically', $fields)) {
		$data['mps_material_planning_ordersheet_approvment_automatically'] = $_REQUEST['mps_material_planning_ordersheet_approvment_automatically'] ? 1 : null;
	}

	if (in_array('mps_material_planning_ordersheet_distrubution_automatically', $fields)) {
		$data['mps_material_planning_ordersheet_distrubution_automatically'] = $_REQUEST['mps_material_planning_ordersheet_distrubution_automatically'] ? 1 : null;
	}
	
	if ($data) {
		
		$material_planning_type = new Material_Planning_Type($application);
		$material_planning_type->read($id);
		
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $material_planning_type->update($data);
			$message = ($action) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $material_planning_type->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));