<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];

	$certificate = new Certificate();
	$certificate->read($id);

	$_PERMISSION_CAN_EDIT_CATALOG = user::permission('can_edit_catalog');
	$_PERMISSION_CAN_EDIT_CERTIFICATES = user::permission('can_edit_certificates');
	
	if ($certificate->id && ($_PERMISSION_CAN_EDIT_CATALOG || $_PERMISSION_CAN_EDIT_CERTIFICATES) ) {
		
		$delete = $certificate->delete();
		
		if ($delete) {

			$model = new Model();

			// remove materials
			$model->db->exec("
				DELETE FROM certificate_materials
				WHERE certificate_material_certificate_id = $id
			");

			// remove materials
			$model->db->exec("
				DELETE FROM certificate_files
				WHERE certificate_file_certificate_id = $id
			");

			// remove files
			dir::remove("/public/data/files/$appliction/certificates/$id");

			Message::request_deleted();
			url::redirect("/$appliction/$controller");

		} else { 
			Message::request_failure();
			url::redirect("/$appliction/$controller/$action/$id");
		}
	}
	else {
		Message::access_denied();
		url::redirect("/$appliction/$controller");
	}