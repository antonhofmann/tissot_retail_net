<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['db_table_id'];
	
	$data = array();
	
	if ($_REQUEST['db_table_db']) {
		$data['db_table_db'] = $_REQUEST['db_table_db'];
	}
	
	if ($_REQUEST['db_table_table']) {
		$data['db_table_table'] = $_REQUEST['db_table_table'];
	}
	
	if ($_REQUEST['db_table_primary_key']) {
		$data['db_table_primary_key'] = $_REQUEST['db_table_primary_key'];
	}
	
	if ($_REQUEST['db_table_description']) {
		$data['db_table_description'] = $_REQUEST['db_table_description'];
	}
	
	if (isset($_REQUEST['db_table_parent_table'])) {
		$data['db_table_parent_table'] = $_REQUEST['db_table_parent_table'];
	}
	
	if (in_array('db_table_records_can_be_deleted', $fields)) {
		$data['db_table_records_can_be_deleted'] = (isset($_REQUEST['db_table_records_can_be_deleted'])) ? 1 : 0;
	}
	
	if (in_array('db_table_delete_cascade', $fields)) {
		$data['db_table_delete_cascade'] = (isset($_REQUEST['db_table_delete_cascade'])) ? 1 : 0;
	}
	
	if ($data) {
	
		$db_table = new DB_Table();
		$db_table->read($id);
	
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $db_table->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $db_table->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));
	