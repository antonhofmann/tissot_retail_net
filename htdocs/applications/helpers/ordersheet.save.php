<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$id = $_REQUEST['mps_ordersheet_id'];
	
	$ordersheet = new Ordersheet($application);
	$ordersheet->read($id);
	
	$data = array();
	
	if ($_REQUEST['mps_ordersheet_workflowstate_id']) {
		$data['mps_ordersheet_workflowstate_id'] = $_REQUEST['mps_ordersheet_workflowstate_id'];
	}
	
	if ($_REQUEST['mps_ordersheet_address_id']) {
		$data['mps_ordersheet_address_id'] = $_REQUEST['mps_ordersheet_address_id'];
	}
	
	if ($_REQUEST['mps_ordersheet_mastersheet_id']) {
		$data['mps_ordersheet_mastersheet_id'] = $_REQUEST['mps_ordersheet_mastersheet_id'];
	}
	
	if (isset($_REQUEST['mps_ordersheet_comment'])) {
		$data['mps_ordersheet_comment'] = $_REQUEST['mps_ordersheet_comment'];
	}
	
	if ($_REQUEST['mps_ordersheet_openingdate']) {
		$data['mps_ordersheet_openingdate'] = date::sql($_REQUEST['mps_ordersheet_openingdate']);
	}
	
	if ($_REQUEST['mps_ordersheet_closingdate']) {
		$data['mps_ordersheet_closingdate'] = date::sql($_REQUEST['mps_ordersheet_closingdate']);
	}
	
	if ($data && $ordersheet->id) {
		
		// workflow state
		$state = $_REQUEST['mps_ordersheet_workflowstate_id'];
		
		// track state change
		$track = ($state  && $state <> $ordersheet->workflowstate_id) ? true : false;
		
		// state before data update
		$stateAprovedBeforeUpdate = $ordersheet->state()->isApproved();
		
		// resetabled states
		$resetStates = array(
			Workflow_State::STATE_PREPARATION,
			Workflow_State::STATE_OPEN
		);
	
		$data['user_modified'] = $user->login;
		
		// update order shet data
		$response = $ordersheet->update($data);
		
		// responding
		$redirect = $_REQUEST['redirect'];
		($response) ? Message::request_saved() : Message::request_failure();
		
		
		// reset items on state change open
		if ($response && $state && $stateAprovedBeforeUpdate && in_array($state, $resetStates)) {
			
			// order sheet items
			$items = $ordersheet->item()->load();
			
			foreach ($items as $key => $item) {
				
				$ordersheet->item->id = $key;
				
				$ordersheet->item->update(array(
					'mps_ordersheet_item_quantity_approved' => null,
					'mps_ordersheet_item_quantity_confirmed' => null,
					'mps_ordersheet_item_quantity_shipped' => null,
					'mps_ordersheet_item_quantity_distributed' => null,
					'mps_ordersheet_item_customernumber' => null,
					'mps_ordersheet_item_shipto' => null,
					'mps_ordersheet_item_order_date' => null,
					'mps_ordersheet_item_purchase_order_number' => null
				));
			}
		}
		
		// track workflow state change
		if ($response && $track) {
				
			$workflowState = new Workflow_State($application);
			$workflowState->read($state);
				
			$user_tracking = new User_Tracking($application);
			$user_tracking->create(array(
				'user_tracking_entity' => 'order sheet',
				'user_tracking_entity_id' => $ordersheet->id,
				'user_tracking_user_id' => $user->id,
				'user_tracking_action' => $workflowState->name,
				'user_created' => $user->login,
				'date_created' => date('Y-m-d H:i:s')
			));
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	