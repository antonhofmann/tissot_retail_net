<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$id = $_REQUEST['lwgroup_id'];
	
	$data = array();
	
	if (in_array('lwgroup_group', $fields)) {
		$data['lwgroup_group'] = $_REQUEST['lwgroup_group'] ? $_REQUEST['lwgroup_group'] : null;
	}
		
	if (in_array('lwgroup_code', $fields)) {
		$data['lwgroup_code'] = $_REQUEST['lwgroup_code'] ? $_REQUEST['lwgroup_code'] : null;
	}		

	if (in_array('lwgroup_text', $fields)) {
		$data['lwgroup_text'] = $_REQUEST['lwgroup_text'] ? $_REQUEST['lwgroup_text'] : null;
	}	

	if (in_array('lwgroup_costgroup', $fields)) {
		$data['lwgroup_costgroup'] = $_REQUEST['lwgroup_costgroup'] ? $_REQUEST['lwgroup_costgroup'] : null;
	}	
	
	if ($data) {
	
		$localWorkGroup = new Local_Work_Group();
		$localWorkGroup->read($id);
	
		if ($localWorkGroup->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $localWorkGroup->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			
			$response = $id = $localWorkGroup->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect'].'/'.$id : null;
		}
	}
	else {
		
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	