<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$active = isset($_REQUEST['active']) ? $_REQUEST['active'] : 1;

$model = new Model($application);

$_REQUEST = session::filter($application, "$controller.$archived.$action");

// sql order
$order = $active ? 'item_category_sortorder' : 'item_category_name';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// sql pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rows = $settings->limit_pager_rows;
$offset = ($page-1) * $rows;

// filter: full text search
if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {

	$keyword = $_REQUEST['search'];

	$filters['search'] = "(
		item_categories LIKE \"%$keyword%\"
	)";
}

if ($active) $filters['visibility'] = "item_category_active = 1";
else {
	$filters['visibility'] = "(
		item_category_active = 0 
		OR item_category_active IS NULL
		OR item_category_active = ''
	)";
}

$result = $model->query("
	SELECT  SQL_CALC_FOUND_ROWS DISTINCT
		item_category_id, 
		item_category_name,
		CONCAT('<i class=\'row-order fa fa-bars\' data-id=\'', item_category_id, '\' ></i>') as sorter
	FROM item_categories
")
->filter($filters)
->order($order, $direction)
->fetchAll();

if ($result) {
	
	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);
	
	$pager = new Pager(array(
		'page' => $page,
		'totalrows' => $totalrows,
		'buttons' => true
	));
	
	$list_index = "Total rows: $totalrows";
	//$list_controlls = $pager->controlls();
}


// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";

// toolbox: add
if ($_REQUEST['add']) {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();

$checked = $activeCategories? "checked='checked'" : true;

// active product lines
$actives[1] = "Active Categories";
$actives[0] = "Inactive Categories";

$toolbox[] = ui::dropdown($actives, array(
	'name' => 'active',
	'id' => 'active',
	'class' => 'submit',
	'value' => $active,
	'caption' => false
));

// toolbox: form
if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

$table = new Table(array(
	'sort' => array('column'=>$order,'direction'=>$direction)
));

$table->datagrid = $datagrid;

if ($active) {
	$table->sorter('width=20px');
}

$table->item_category_name(
	Table::ATTRIBUTE_NOWRAP,
	"href=".$_REQUEST['data']
);

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;