<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$action = $_REQUEST['action'];
	
	$id = $_REQUEST['mail_template_filter_id'];
	
	// template
	$mailTemplate = new Mail_Template();
	$mailTemplate->read($_REQUEST['mail_template_filter_template_id']);
	
	$data = array();
	
	$data['mail_template_filter_template_id'] = $_REQUEST['mail_template_filter_template_id'];
	$data['mail_template_filter_name'] = $_REQUEST['mail_template_filter_name'];
	$data['mail_template_filter_description'] = $_REQUEST['mail_template_filter_description'];
	$data['mail_template_filter_data'] = $_REQUEST['mail_template_filter_data'];
	
	
	if ($mailTemplate->id && $data) {
	
		if ($id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$mailTemplate->filter()->read($id);

			$response = $mailTemplate->filter()->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $mailTemplate->filter()->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));
	