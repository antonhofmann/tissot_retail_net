<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$mail_template = new Mail_Template();

if ($_REQUEST['id']) {
	$mail_template->read($_REQUEST['id']);
}
elseif ($_REQUEST['template']) {
	$mail_template->read_from_shortcut($_REQUEST['template']);
}

if ($mail_template->data) {
	
	if ($_REQUEST['render']) {
		
		$data = $_REQUEST['render'];
		$data['sender_firstname'] = $user->firstname;
		$data['sender_name'] = $user->name;

		$subject = $mail_template->subject;
		$mail_template->data['mail_template_subject'] = Content::render($subject, $data);
		
		$content = $mail_template->text;
		$mail_template->data['mail_template_text'] = Content::render($content, $data);
	}

	$mail_template->data['mail_template_text'] = nl2br($mail_template->data['mail_template_text']);

	$json = $mail_template->data;

	$json['response'] = true;
}
else {
	$json['response'] = false;
	$json['message'] = $translate->message_mail_not_found;
}

if ($json) {
	header('Content-Type: text/json');
	echo json_encode($json);
}