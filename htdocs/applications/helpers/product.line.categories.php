<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$id = $_REQUEST['product_line_id'];

$active = isset($_REQUEST['active']) ? $_REQUEST['active'] : 0;

$model = new Model($application);

$result = $model->query("
	SELECT 
		categories.category_id,
		categories.category_name,
		category_cost_unit_number, 
		IF (category_budget, 'yes', 'no') as category_budget, 
		IF (category_catalog, 'yes', 'no') as category_catalog,
		items.item_id, 
		items.item_code, 
		items.item_name,
		items.item_price,
		items.item_active
	FROM categories 
	LEFT JOIN category_items ON categories.category_id = category_items.category_item_category
	LEFT JOIN items ON category_items.category_item_item = items.item_id
	WHERE categories.category_product_line = $id AND category_not_in_use = $active
	ORDER BY 
		categories.category_name,
		items.item_code
")->fetchAll();

if ($result) {

	foreach ($result as $row) {
		
		$category = $row['category_id'];
		$item = $row['item_id'];

		$datagrid[$category]['category_name'] = $row['category_name'];
		$datagrid[$category]['category_cost_unit_number'] = $row['category_cost_unit_number'];
		$datagrid[$category]['category_budget'] = $row['category_budget'];
		$datagrid[$category]['category_catalog'] = $row['category_catalog'];

		if ($item && $row['item_active']) {
			$datagrid[$category]['items'][$item]['item_code'] = $row['item_code'];
			$datagrid[$category]['items'][$item]['item_name'] = $row['item_name'];
			$datagrid[$category]['items'][$item]['item_price'] = $row['item_price'];
		}
	}
}


// toolbox: add
if ($_REQUEST['add']) {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
}


$actives = array(
	0 => 'Active',
	1 => 'Inactive'
);

$toolbox[] = ui::dropdown($actives, array(
	'name' => 'active',
	'id' => 'active',
	'class' => 'submit',
	'value' => $_REQUEST['active'],
	'caption' => false
));

// toolbox: form
if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

if ($datagrid) {

	$list = "<table class='default'>";

	$list .="
		<thead>
			<tr>
				<th width='20px'>&nbsp;</th>
				<th nowrap=nowrap ><strong>$translate->category_name</strong></th>
				<th nowrap=nowrap width='10%'><strong>$translate->category_cost_unit_number</strong></th>
				<th nowrap=nowrap width='10%'><strong>in Budget</strong></th>
				<th nowrap=nowrap width='10%'><strong>in Catalog</strong></th>
				<!--<th width='20px'>&nbsp;</th>-->
			</tr>
		</thead>
		<tbody>
	";

	foreach ($datagrid as $category => $row) {
		
		$rowClass = $rowClass=='-odd' ? '-even' : '-odd'; 
		$hasSubtabe = $row['items'] ? 'has-subtable' : null;
		$itemToggler = ($row['items']) ? "<i class='fa fa-chevron-circle-right' ></i>" : null;

		$list .= "<tr class='$rowClass $hasSubtabe' data-id='$category' >";
		$list .= "<td class='item-trigger' title='Items' >$itemToggler</td>";
		$list .= "<td><a href='/$application/$controller/$action/$id/$category'>{$row[category_name]}</a></td>";
		$list .= "<td>{$row[category_cost_unit_number]}</td>";
		$list .= "<td>{$row[category_budget]}</td>";
		$list .= "<td>{$row[category_catalog]}</td>";
		//$list .= "<td class='sorter'><i class='fa fa-bars row-order' data-id='$category' ></i></td>";
		$list .= "</tr>";

		if ($row['items']) {

			$list .= "<tr class='row-subtable $rowClass' id='subtable-$category'>";
			$list .= "<td colspan='6' class='cell-subtable '>";
			$list .= "<table class='subtable'>";
			$list .="
				<thead>
					<tr>
						<th nowrap=nowrap width='20%' ><strong>Item Code</strong></th>
						<th nowrap=nowrap ><strong>Item Name</strong></th>
						<th nowrap=nowrap width='10%' ><strong>Item Price</strong></th>
					</tr>
				</thead>
				<tbody>
			";

			foreach ($row['items'] as $item => $subrow) {
				$list .= "<tr>";
				$list .= "<td>{$subrow[item_code]}</td>";
				$list .= "<td>{$subrow[item_name]}</td>";
				$list .= "<td>{$subrow[item_price]}</td>";
				$list .= "</tr>";
			}

			$list .= "</tbody></table>";
			$list .= "</td>";
			$list .= "</tr>";
		}

	}

	$list .= "</tbody></table>";

}

echo $toolbox.$list;


