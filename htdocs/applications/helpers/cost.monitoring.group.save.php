<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$id = $_REQUEST['costmonitoringgroup_id'];
	
	$data = array();
	
	if (in_array('costmonitoringgroup_code', $fields)) {
		$data['costmonitoringgroup_code'] = $_REQUEST['costmonitoringgroup_code'];
	}
		
	if (in_array('costmonitoringgroup_text', $fields)) {
		$data['costmonitoringgroup_text'] = $_REQUEST['costmonitoringgroup_text'];
	}	

	if (in_array('costmonitoringgroup_include_in_masterplan', $fields)) {
		$data['costmonitoringgroup_include_in_masterplan'] = $_REQUEST['costmonitoringgroup_include_in_masterplan'] ? 1 : 0;
	}	
	
	if ($data) {
	
		$costMonitorinGroup = new Cost_Monitoring_Group();
		$costMonitorinGroup->read($id);
	
		if ($costMonitorinGroup->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $costMonitorinGroup->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			
			$response = $id = $costMonitorinGroup->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect'].'/'.$id : null;
		}
	}
	else {
		
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	