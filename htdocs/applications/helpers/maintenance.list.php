<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$model = new Model($application);
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	// sql order
	$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : '
		maintenance_window_start,
		maintenance_window_stop,
		maintenance_window_title
	';
	
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
	
		$keyword = $_REQUEST['search'];
	
		$filters['search'] = "(
		maintenance_window_start LIKE \"%$keyword%\"
		OR maintenance_window_stop LIKE \"%$keyword%\"
		OR maintenance_window_title LIKE \"%$keyword%\"
		OR maintenance_window_content LIKE \"%$keyword%\"
		)";
	}
	
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT 
			maintenance_window_id,
			DATE_FORMAT(maintenance_window_start, '%d.%m.%Y %H:%i') AS maintenance_window_start,
			DATE_FORMAT(maintenance_window_stop, '%d.%m.%Y %H:%i') AS maintenance_window_stop,
			maintenance_window_title
		FROM maintenance_windows		
	")
	->filter($filters)
	->offset($offset, $rowsPerPage)
	->order($order, $direction)
	->fetchAll();
	
	if ($result) {
		
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));
		
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
	
	// toolbox: add
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	// toolbox: form
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$order,'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	
	$table->maintenance_window_title(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		"width=70%",
		"href=".$_REQUEST['form']
	);
	
	$table->maintenance_window_start(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=8%'
	);
	
	$table->maintenance_window_stop(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);
	
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	
	
	