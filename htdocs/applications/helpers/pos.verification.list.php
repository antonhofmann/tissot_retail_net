<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];

	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	// sql order
	$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "country_name, address_company, province_canton, place_name";
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
	
	// search keyword
	$search_keyword = ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) ? $_REQUEST['search'] : null;

	$model = new Model($application);

	// filter: default
	$filters['default'] = "
		address_type = 1 
		AND address_active = 1 
		AND (
			address_involved_in_planning = 1 
			OR address_involved_in_planning_ff = 1
		)
	";

	if ($search_keyword) {

		$filters['search'] = "(
			address_company LIKE \"%$search_keyword%\"
			OR country_name LIKE \"%$search_keyword%\"
			OR province_canton LIKE \"%$search_keyword%\"
			OR address_place LIKE \"%$search_keyword%\"
			OR address_zip LIKE \"%$search_keyword%\"
		)";
	}

	if ($_REQUEST['countries']) {
		$filters['countries'] = "address_country=".$_REQUEST['countries'];
	}
	
	// join list sources
	$binds = array(
		Company::DB_BIND_COUNTRIES,
		Company::DB_BIND_PLACES,
		Company::DB_BIND_PROVINCES
	);

	// data: companies
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			address_id,
			address_company,
			country_name,
			province_canton,
			place_name,
			address_zip
		FROM db_retailnet.addresses
	")
	->bind($binds)
	->filter($filters)
	->order($order, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();

	if ($result) {

		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);

		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));

		$pageIndex = $pager->index();
		$pageControlls = $pager->controlls();

		foreach ($datagrid as $key => $row) {
			
			$result = $model->query("
				SELECT DATE_FORMAT(posverification_date, '%M %Y') AS date
				FROM posverifications
				WHERE posverification_confirm_date IS NOT NULL AND posverification_address_id = $key
				ORDER BY posverification_confirm_date DESC
				LIMIT 0,1
			")->fetch();

			// company sheet
			if ($result) {
				$datagrid[$key]['posverification_date'] = $result['date'];
			}
		}
	}

	// toolbox: hide utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$order' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

	// toolbox: serach full text
	$toolbox[] = ui::searchbox();

	// toolbox: countries
	$result = $model->query("
		SELECT DISTINCT
			country_id, country_name
		FROM db_retailnet.addresses
	")
	->bind($binds)
	->filter($filters)
	->exclude('countries')
	->order('country_name')
	->fetchAll();

	$toolbox[] = html::select($result, array(
		'name' => 'countries',
		'id' => 'countries',
		'class' => 'submit selectbox',
		'value' => $_REQUEST['countries'],
		'caption' => $translate->all_countries
	));

	if ($toolbox) {
		$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
	}

	$table = new Table(array(
		'sort' => array('column' => $order, 'direction' => $direction)
	));

	$table->datagrid = $datagrid;

	$table->country_name(
		'width=10%',
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->address_company(
		"href=".$_REQUEST['form'],
		Table::PARAM_SORT
	);

	$table->province_canton(
		'width=10%',
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->place_name(
		'width=10%',
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->posverification_date(
		'width=10%',
		Table::ATTRIBUTE_NOWRAP
	);

	$table->footer($pageIndex);
	$table->footer($pageControlls);
	$table = $table->render();

	echo $toolbox.$table;
