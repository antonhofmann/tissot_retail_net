<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	$application = $_REQUEST['application'];
	$id = $_REQUEST['red_projecttype_id'];
	
	$data = array();

	if ($_REQUEST['red_projecttype_name']) {
		$data['red_projecttype_name'] = $_REQUEST['red_projecttype_name'];
	}

	if ($data) {
		
		$projectType = new Red_Project_Type($application);
		$projectType->read($id);
		
		if ($projectType->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $projectType->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $projectType->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}

	echo json_encode(array(
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message
	));
