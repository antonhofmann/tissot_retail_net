<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// company permissiens
$permission_view = user::permission(Company::PERMISSION_VIEW);
$permission_view_limited = user::permission(Company::PERMISSION_VIEW_LIMITED);
$permission_edit = user::permission(Company::PERMISSION_EDIT);
$permission_edit_limited = user::permission(Company::PERMISSION_EDIT_LIMITED);
$permission_menager = user::permission(Company::PERMISSION_MANAGER);
$permission_catalog = user::permission(Company::PERMISSION_EDIT_CATALOG);

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

// request filters
$fCountry = $_REQUEST['countries'];
$fSerach =  $_REQUEST['search'] && $_REQUEST['search']<>$translate->search ? $_REQUEST['search'] : null;
$fAddressType = $_REQUEST['addresstypes'];

// sql order
$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "country_name, address_type_name, address_company";
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

// can edit companies
$can_edit_companies = ($permission_edit || $permission_edit_limited || $permission_menager || $permission_catalog) ? true : false;

// has limited permission
$has_limited_permission = (!$permission_view && !$permission_edit && !$permission_menager && !$permission_catalog) ? true : false;

$model = new Model(Connector::DB_CORE);

// country access
$accessCountries = array();

// join list sources
$binds = array(
	Company::DB_BIND_ADDRESS_TYPES,
	Company::DB_BIND_COUNTRIES,
	Company::DB_BIND_PLACES,
	Company::DB_BIND_PROVINCES
);

// in administration section, bind users table for text search option 
if ($application=='administration' && $fSerach) {
	array_push($binds, 'LEFT JOIN users ON user_address = address_id');
}

// active filters
$active = ($archived) ? 0 : 1;
$filters['active'] = "address_active = $active";

// show in pos index
$showinpos = ($_REQUEST['showinpos']) ? $_REQUEST['showinpos'] : 1;
$filters['showinposindex'] = "address_showinposindex = $showinpos";

if ($application=="mps") {
	$filters['application'] = "(address_type = 1 OR address_type = 7)";
}

if ($_REQUEST['filter']) {
	$filters['filter'] = $_REQUEST['filter'];
}

if ($fSerach) {

	if ($application=='administration') {
		$search_users = "
			OR user_name LIKE \"%$fSerach%\"
			OR user_firstname LIKE \"%$fSerach%\"		
		";
	}

	$filters['search'] = "(
		address_company LIKE \"%$fSerach%\"
		OR address_street LIKE \"%$fSerach%\"
		OR country_name LIKE \"%$fSerach%\"
		OR province_canton LIKE \"%$fSerach%\"
		OR address_place LIKE \"%$fSerach%\"
		OR address_type_name LIKE \"%$fSerach%\"
		OR address_zip LIKE \"%$fSerach%\"
		OR address_mps_customernumber LIKE \"%$fSerach%\"
		OR address_mps_shipto LIKE \"%$fSerach%\"
		$search_users
	)";
}

// filter: address type
switch ($fAddressType) {
	case 1: $filters['addresstypes'] = "address_client_type = 1"; break;
	case 2: $filters['addresstypes'] = "address_client_type = 2"; break;
	case 3: $filters['addresstypes'] = "address_client_type = 3"; break;
	case 4: $filters['addresstypes'] = "address_is_independent_retailer = 1"; break;
	case 5: $filters['addresstypes'] = "address_canbefranchisee = 1"; break;
}

// filter: country
if ($fCountry) {
	$filters['countries'] = "address_country=$fCountry";
}


// for limited view:
// get current company childs
// get country accesses
if ($has_limited_permission) {

	// filter access countries
	$countryAccess = User::getCountryAccess();	
	$filterCountryAccess = $countryAccess ? " OR address_country IN ($countryAccess)" : null;

	// regional access companies
	$regionalAccessCompanies = User::getRegionalAccessCompanies();
	$filterRegionalAccess = $regionalAccessCompanies ? " OR address_parent IN (".join(',', $regionalAccessCompanies).") " : null;

	$filters['limited'] = "(
		(address_id = $user->address OR address_parent = $user->address) 
		$filterCountryAccess 
		$filterRegionalAccess
	)";
}

// data: companies
$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		address_id,
		country_name,
		address_type_name,
		address_company,
		province_canton,
		place_name,
		address_zip,
		address_canbefranchisee,
		address_is_independent_retailer,
		address_mps_customernumber,
		address_mps_shipto
	FROM db_retailnet.addresses
")
->bind($binds)
->filter($filters)
->extend($extendedFilter)
->order($order, $direction)
->offset($offset, $rowsPerPage)
->fetchAll();
//echo $model->sql;

if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);

	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$pageIndex = $pager->index();
	$pageControlls = $pager->controlls();

	foreach ($datagrid as $key => $row) {

		// company sheet
		if ($_REQUEST['pdf']) {
			$request = $_REQUEST['pdf']."&id=$key";
			$datagrid[$key]['pdf'] = "<a href='$request' target='_blank'>".ui::extension('pdf', 'small')."</a>";
		}

		// franchisee
		$datagrid[$key]['franchisee'] = ($row['address_canbefranchisee']) ? ui::icon('checked') : null;

		// is retailer
		$datagrid[$key]['retailer'] = ($row['address_is_independent_retailer']) ? ui::icon('checked') : null;
	}
}

// toolbox: hide utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$order' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// toolbox: button print
if ($datagrid && $_REQUEST['export']) {
	$toolbox[] = ui::button(array(
		'id' => 'print',
		'icon' => 'print',
		'href' => $_REQUEST['export'],
		'caption' => $translate->print,
	));
}

// toolbox: button add
if ( $can_edit_companies && $_REQUEST['add'] && !$archived) {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'caption' => $translate->add_new
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();

// toolbox: countries
$query = "SELECT DISTINCT country_id, country_name FROM db_retailnet.addresses";
$result = $model->query($query)->bind($binds)->filter($filters)->exclude('countries')->order('country_name')->fetchAll();

if ($result) {
	$toolbox[] = html::select($result, array(
		'name' => 'countries',
		'id' => 'countries',
		'class' => 'submit',
		'value' => $fCountry,
		'caption' => $translate->all_countries
	));
}

$addressTypes = array();

// toolbox type: agents, subsidiaries, affiliates
$query = "SELECT DISTINCT client_type_id, client_type_code FROM db_retailnet.addresses";
$result = $model->query($query)->bind($binds)
->bind('INNER JOIN db_retailnet.client_types ON client_type_id = address_client_type')	
->filter($filters)->exclude('addresstypes')->fetchAll();
$addressTypes = _array::extract($result);

// toolbox type: independent retailer
$query = "SELECT COUNT(address_id) AS total FROM db_retailnet.addresses";
$result = $model->query($query)->bind($binds)
->filter($filters)->filter('def', "address_is_independent_retailer=1")
->exclude('addresstypes')->fetch();

if ($result['total']>0) {
	$addressTypes[4] = Translate::instance()->independent_retailers;
}

// toolbox type: franchisee
$query = "SELECT COUNT(address_id) AS total FROM db_retailnet.addresses";
$result = $model->query($query)->bind($binds)
->filter($filters)->filter('def', "address_canbefranchisee=1")
->exclude('addresstypes')->fetch();

if ($result['total']>0) {
	$addressTypes[5] = Translate::instance()->franchisees;
}

if ($addressTypes) {
	asort($addressTypes);
	$toolbox[] = html::select($addressTypes, array(
		'id' => 'addresstypes',
		'name' => 'addresstypes',
		'value' => $fAddressType,
		'class' => 'submit',
		'caption' => $translate->all_address_types
	));
}



if ($toolbox) {
	$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
}

$table = new Table(array(
	'sort' => array('column' => $order, 'direction' => $direction)
));

$table->datagrid = $datagrid;

$table->country_name(
	'width=10%',
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);

$table->address_company(
	"href=".$_REQUEST['form'],
	Table::PARAM_SORT
);

$table->address_type_name(
	'width=10%',
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);

if ($application == 'administration') {

	$table->address_mps_customernumber(
		'width=10%',
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->address_mps_shipto(
		'width=10%',
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);
}

$table->province_canton(
	'width=8%',
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);

$table->place_name(
	'width=8%',
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);

$table->address_zip(
	'width=5%',
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);

if (Application::involved_in_planning($application)) {

	$table->pdf(
		'width=2%',
		TABLE::ATTRIBUTE_ALIGN_CENTER
	);

	$table->franchisee(
		'width=2%',
		TABLE::ATTRIBUTE_ALIGN_CENTER
	);

	$table->retailer(
		'width=2%',
		TABLE::ATTRIBUTE_ALIGN_CENTER
	);
}

$table->footer($pageIndex);
$table->footer($pageControlls);
$table = $table->render();

echo $toolbox.$table;
