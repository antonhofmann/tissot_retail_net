<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['file_type_id'];
	
	$data = array();
	
	if ($_REQUEST['file_type_name']) {
		$data['file_type_name'] = $_REQUEST['file_type_name'];
	}
	
	if ($_REQUEST['file_type_mime_type']) {
		$data['file_type_mime_type'] = $_REQUEST['file_type_mime_type'];
	}
	
	if ($_REQUEST['file_type_extension']) {
		$data['file_type_extension'] = $_REQUEST['file_type_extension'];
	}
	
	if (isset($_REQUEST['file_type_description'])) {
		$data['file_type_description'] = $_REQUEST['file_type_description'];
	}
	
	if ($data) {
	
		$file_type = new File_Type();
		$file_type->read($id);
	
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $file_type->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $file_type->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect'].$id : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	