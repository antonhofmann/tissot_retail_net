<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$appliction = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$itemOption = new Item_Option();
$itemOption->read($id);

$permission_edit = user::permission('can_edit_catalog');
$permission_edit_items = user::permission('can_edit_items_in_catalogue');
$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

if ($itemOption->id && $_CAN_EDIT) {
	
	$item = $itemOption->parent;
	$delete = $itemOption->delete();
	
	if ($delete) {

		$model = new Model();

		// remove materials
		$model->db->exec("
			DELETE FROM item_option_files
			WHERE item_option_file_option = $id
		");

		// remove files
		dir::remove("/files/items-options/$id");

		Message::request_deleted();
		url::redirect("/$appliction/$controller/$action/$item");

	} else { 
		Message::request_failure();
		url::redirect("/$appliction/$controller/$action/$item/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$appliction/$controller");
}