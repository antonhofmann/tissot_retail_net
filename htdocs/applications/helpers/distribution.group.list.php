<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// application
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];

	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'mps_distchannel_group_name';
	
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(mps_distchannel_group_name LIKE \"%$keyword%\")";
	}
	
	$model = new Model(Connector::DB_CORE);
	
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			mps_distchannel_group_id,
			mps_distchannel_group_name,
			mps_distchannel_group_allow_for_agents
		FROM mps_distchannel_groups
	")
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();
	
	if ($result) {
	
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		$icon_checked = ui::icon('checked');
		$icon_unchecked = ui::icon('unchecked');
		
		foreach ($datagrid as $key => $row) {
			$dataloader['mps_distchannel_group_allow_for_agents'][$key] = ($row['mps_distchannel_group_allow_for_agents']) ? $icon_checked : $icon_unchecked;
		}
	
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
	
	// toolbox: utton print
	if ($datagrid && $_REQUEST['print']) {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'href' => $_REQUEST['print'],
			'label' => $translate->print
		));
	}
	
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	$table->dataloader($dataloader);
	
	$table->mps_distchannel_group_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'href='.$_REQUEST['form']
	);
	
	$table->mps_distchannel_group_allow_for_agents(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		Table::DATA_TYPE_IMAGE,
		'width=5%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	