<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$topic = $_REQUEST['topic'];
	$section = $_REQUEST['section'];
	
	$assistance = new Assistance();
	$assistance->read($topic);
	
	
	if ($assistance->id) {
		
		$integrity = new Integrity();
		
		// delete help topic section
		if ($section) {
			
			$assistance->section()->read($section);
			
			$integrity->set($section, 'assistance_sections', 'system');
			
			if ($assistance->section()->id && $integrity->check()) {
				
				$response = $assistance->section()->delete();
			}
		}
		// delete help topic
		else {
			
			$integrity->set($topic, 'assistances', 'system');
			
			if ($integrity->check()) {
				
				$response = $assistance->delete();
			}
		}
	
		
		if ($response) {
			Message::request_deleted();
			url::redirect("/$application/$controller");
		} else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$id");
		}
	}
	else {
		Message::access_denied();
		url::redirect("/$application/$controller");
	}