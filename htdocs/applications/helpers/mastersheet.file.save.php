<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$mastersheet = $_REQUEST['mps_mastersheet_file_mastersheet_id'];
	$id = $_REQUEST['mps_mastersheet_file_id'];
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	$upload_path = "/public/data/files/$application/mastersheets/$mastersheet";
	
	$data = array();
	
	if ($_REQUEST['mps_mastersheet_file_title']) {
		$data['mps_mastersheet_file_title'] = $_REQUEST['mps_mastersheet_file_title'];
	}
	
	if ($_REQUEST['has_upload'] && $_REQUEST['mps_mastersheet_file_path']) {
		
		$path = upload::move($_REQUEST['mps_mastersheet_file_path'], $upload_path);
		$extension = file::extension($path);
		
		$file_type = new File_Type();
		$data['mps_mastersheet_file_path'] = $path;
		$data['mps_mastersheet_file_type'] = $file_type->get_id_from_extension($extension);
	}
	
	if (isset($_REQUEST['mps_mastersheet_file_description'])) {
		$data['mps_mastersheet_file_description'] = $_REQUEST['mps_mastersheet_file_description'];
	}
	
	if (in_array('mps_mastersheet_file_visible', $fields)) {
		$data['mps_mastersheet_file_visible'] = ($_REQUEST['mps_mastersheet_file_visible']) ? 1 : 0;
	}
	
	if ($mastersheet && $data) {
		
		$data['mps_mastersheet_file_mastersheet_id'] = $mastersheet;
	
		$mastersheetFile = new Mastersheet_File($application);
		$mastersheetFile->read($id);
	
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $mastersheetFile->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $mastersheetFile->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'path' => $path
	));
	