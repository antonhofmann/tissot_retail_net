<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// request vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	// mastersheet vars
	$mastersheet = $_REQUEST['mps_mastersheet_id'];
	$ordersheet_description = $_REQUEST['mps_ordersheet_comment'];
	$ordersheet_openingdate = $_REQUEST['mps_ordersheet_openingdate'];
	$ordersheet_closingdate = $_REQUEST['mps_ordersheet_closingdate'];
	$ordersheet_workflowstate = $_REQUEST['mps_ordersheet_workflowstate_id'];
	$selected_companies = $_REQUEST['selected_companies'];

	
	if ($selected_companies) {
		
		//$companies = explode(',', $selected_companies);
		$companies = array_filter(explode(',', $selected_companies));
		
		// ordersheet builder
		$ordersheet = new Ordersheet($application);

		// get mastersheet items
		$model = new Model($application);
		
		// master sheet items
		$mastersheet_items = $model->query("
			SELECT DISTINCT
				mps_material_id,
				mps_material_price, 
				currency_id, 
				currency_exchange_rate, 
				currency_factor
			FROM mps_mastersheet_items
		")
		->bind(Mastersheet_Item::DB_BIND_MATERIALS)
		->bind(Material::DB_BIND_CURRENCIES)
		->filter('mastersheet', "mps_mastersheet_item_mastersheet_id = $mastersheet")
		->fetchAll();
		
		// for each selected companie,
		// create new order sheet and new order sheet items set
		foreach ($companies as $company) {
			
			if ($company) {
				$new_ordersheet = $ordersheet->create(array(
					'mps_ordersheet_address_id' => $company,
					'mps_ordersheet_mastersheet_id' => $mastersheet,
					'mps_ordersheet_workflowstate_id' => 7,
					'mps_ordersheet_openingdate' => date::sql($ordersheet_openingdate),
					'mps_ordersheet_closingdate' => date::sql($ordersheet_closingdate),
					'mps_ordersheet_comment' => $ordersheet_description,
					'user_created' => $user->login,
					'date_created' => date('Y-m-d H:i:s')
				));
			}
			
			// if new order sheet is created
			// create new items set from master sheet items
			if ($new_ordersheet && $mastersheet_items) {
				
				// read order sheet instance
				$ordersheet->read($new_ordersheet);
				
				foreach ($mastersheet_items as $item) {
					$ordersheet->item()->create(array(
						'mps_ordersheet_item_ordersheet_id' => $new_ordersheet,
						'mps_ordersheet_item_material_id' => $item['mps_material_id'],
						'mps_ordersheet_item_price' => $item['mps_material_price'],
						'mps_ordersheet_item_currency' => $item['currency_id'],
						'mps_ordersheet_item_exchangrate' => $item['currency_exchange_rate'],
						'mps_ordersheet_item_factor' => $item['currency_factor'],
						'user_created' => $user->login,
						'date_created' => date('Y-m-d H:i:s')
					));
				}
			}
		}
		
		$response = true;
		$message = $translate->message_request_inserted;
		$tab = 'existing_ordersheets';
	} 
	else {
		$response = false;
		$message =$translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'tab' => $tab,
		'reset' => true
	));
	