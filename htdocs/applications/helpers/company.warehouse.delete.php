<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$warehouse = new Company_Warehouse();
	$warehouse->read($id);
	
	if ($warehouse->id && (user::permission(Company::PERMISSION_EDIT) OR user::permission(Company::PERMISSION_EDIT_LIMITED) )) {
		
		$company = $warehouse->address_id;
		$response = $warehouse->delete();
		
		if ($response) {
			Message::request_deleted();
			url::redirect("/$application/$controller/$action/$company");
		} else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$company/$id");
		}
		
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller");
	}