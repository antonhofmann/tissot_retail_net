<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$ordersheet_id = $_REQUEST['ordersheet'];
	
	$section = $_REQUEST['section'];
	$id = $_REQUEST['id'];
	$referer = $_REQUEST['referer'];
	$material = $_REQUEST['material'];
	$quantity = $_REQUEST['quantity'];
	
	// order sheet
	$ordersheet = new Ordersheet($application);
	$ordersheet->read($ordersheet_id);
	
	// read item instance from material
	$ordersheet->item()->read_from_material($material);
	
	// if order sheet and ordersheet item exist
	if ($ordersheet->id && $ordersheet->item()->id) {
		
		
		switch ($section) {
			
			// edit planned quantity
			case 'planned':
				
				$ordersheet->item()->planned()->read($id);
				
				if ($quantity>0) {				
					if ($ordersheet->item()->planned()->id) {
						$ordersheet->item()->planned()->update($quantity);
					}
					else {						
						$id = $ordersheet->item()->planned()->add_pos($referer, $quantity);
					}
				}
				elseif ($ordersheet->item()->planned()->id) {
					$ordersheet->item()->planned()->delete();
					unset($id);
				}
			
			break;
			
			// edit delivered quantity
			case 'delivered': 
				
				$ordersheet->item()->delivered()->read($id);
				
				if ($quantity>0) {				
					if ($ordersheet->item()->delivered()->id) {
						$ordersheet->item()->delivered()->update($quantity);
					} 
					else {						
						$id = $ordersheet->item()->delivered()->add_pos($referer, $quantity);
					}
				}
				elseif ($ordersheet->item()->delivered()->id) {
					$ordersheet->item()->delivered()->delete();
					unset($id);
				}
			
			break;
			
			// edit partiale deliveret quantity
			case 'partial': 
				
				$ordersheet->item()->delivered()->read($id);
				
				if ($quantity>0) {
					if ($ordersheet->item()->delivered()->id) {
						$ordersheet->item()->delivered()->update($quantity);
					}
					else {
						$id = $ordersheet->item()->delivered()->add_pos($referer, $quantity);
					}
				}
				elseif ($ordersheet->item()->delivered()->id) {
					$ordersheet->item()->delivered()->delete();
					unset($id);
				}
				
			break;
			
			// edit reserve planned quantity
			case 'reserve-planned':
				
				$ordersheet->item()->planned()->read($id);
				
				if ($quantity>0) {				
					if ($ordersheet->item()->planned()->id) {
						$ordersheet->item()->planned()->update($quantity);
					}
					else {						
						$id = $ordersheet->item()->planned()->add_warehouse($referer, $quantity);
					}
				}
				elseif ($ordersheet->item()->planned()->id) {
					$ordersheet->item()->planned()->delete();
					unset($id);
				}
				
			break;
			
			// edit reserve delivered quantity
			case 'reserve-delivered': 
				
				$ordersheet->item()->delivered()->read($id);
				
				if ($quantity>0) {				
					if ($ordersheet->item()->delivered()->id) {
						$ordersheet->item()->delivered()->update($quantity);
					} 
					else {						
						$id = $ordersheet->item()->delivered()->add_warehouse($referer, $quantity);
					}
				}
				elseif ($ordersheet->item()->delivered()->id) {
					$ordersheet->item()->delivered()->delete();
					unset($id);
				}
				
			break;
			
			// edit reserve partiale delivered quantity
			case 'reserve-partial':
				
				$ordersheet->item()->delivered()->read($id);
				
				if ($quantity>0) {
					if ($ordersheet->item()->delivered()->id) {
						$ordersheet->item()->delivered()->update($quantity);
					}
					else {
						$id = $ordersheet->item()->delivered()->add_warehouse($referer, $quantity);
					}
				}
				elseif ($ordersheet->item()->delivered()->id) {
					$ordersheet->item()->delivered()->delete();
					unset($id);
				}
				
			break;
			
			// stock reserve
			case 'stock-reserve':
				
				$warehouse = new Ordersheet_Warehouse($application);
				$warehouse->ordersheet = $ordersheet_id;
					
				// get order sheet
				$data = $warehouse->getStockReserve();
				
				// get stock reserve id
				$model = new Model($application);
				$res = $model->query("
					SELECT mps_ordersheet_item_planned_id
					FROM mps_ordersheet_item_planned
					WHERE mps_ordersheet_item_planned_ordersheet_item_id = ".$ordersheet->item()->id."
					AND mps_ordersheet_item_planned_warehouse_id = ".$data['mps_ordersheet_warehouse_id']."
				")->fetch();
				
				$id = $res['mps_ordersheet_item_planned_id'];

				if ($id && $quantity >= 0) {

					$id = $ordersheet->item()->planned()->read($id);
					
					
					$ordersheet->item()->planned()->update($quantity);
				}
				
			break;

			case 'adjusted':

				$model = new Model($application);

				$tracker = new Tracker($application);
				$tracker->setEntity('mps_ordersheet_items_adjusted');

				$sth = $model->db->prepare("
					SELECT *
					FROM mps_ordersheet_items_adjusted
					WHERE mps_ordersheet_items_adjusted_id = ?
				");

				$sth->execute(array($id));
				$adjusted = $sth->fetch();

				$id = $adjusted['mps_ordersheet_items_adjusted_id'];
				$item = $ordersheet->item()->id;

				if ($id) {

					$sth = $model->db->prepare("
						UPDATE mps_ordersheet_items_adjusted SET
							mps_ordersheet_items_adjusted_quantity = ?,
							user_modified = ?,
							date_modified = NOW()
						WHERE mps_ordersheet_items_adjusted_id = ?
					");

					$response = $sth->execute(array($quantity, $user->login, $id));

					if ($response) {
						$tracker->onChange("Update adjusted quantitiy from $oldQuantity to $quantity", $item, $id);
					}

				} else {

					$sth = $model->db->prepare("
						INSERT INTO mps_ordersheet_items_adjusted (
							mps_ordersheet_items_adjusted_item_id,
							mps_ordersheet_items_adjusted_quantity,
							user_created
						)
						VALUES (?,?,?)
					");

					$response = $sth->execute(array($item, $quantity, $user->login));
					$id = $model->db->lastInsertId();

					if ($response) {
						$tracker->onCreate("Add $quantity adjusted quantities", $item, $id);
					}
				}

			break;
			
		}
	}
	
	// response with json header
	header('Content-Type: text/json');
	echo json_encode(array(
		'id' => $id,
		'error' => $error
	));
	