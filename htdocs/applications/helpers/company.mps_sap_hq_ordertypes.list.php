<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// request vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];

	// permissions
	$permission_edit = user::permission(Company::PERMISSION_EDIT);
	$permission_edit_limited = user::permission(Company::PERMISSION_EDIT_LIMITED);
	
	// db application model
	$model = new Model($application);

	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

	// sql order
	$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "address_company, mps_sap_hq_ordertype_customer_number";
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rows = $settings->limit_pager_rows;
	$offset = ($page-1) * $rows;

	// can edit companies
	$can_edit_companies = (user::permission(Company::PERMISSION_EDIT) OR user::permission(Company::PERMISSION_EDIT_LIMITED)) ? true : false;

	// has limited permission
	$has_limited_permission = (!user::permission(Company::PERMISSION_EDIT) AND user::permission(Company::PERMISSION_EDIT_LIMITED)) ? true : false;

	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		$keyword = $_REQUEST['search'];
		$filters['search'] = "(
			address_company LIKE \"%$keyword%\"
		)";
	}


	
	// company filter
	$filters['company'] = "mps_sap_hq_ordertype_address_id = $id";

	// data: companies
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			mps_sap_hq_ordertype_id,
			address_company, sap_hq_order_type_name,
			sap_hq_order_reason_name, 
			mps_sap_hq_ordertype_customer_number,
			mps_sap_hq_ordertype_shipto_number, 
			IF(mps_sap_hq_ordertype_is_default, '<img src=/public/images/icon-checked.png >', ' ') AS mps_sap_hq_ordertype_is_default,
			IF(mps_sap_hq_ordertype_is_free_goods, '<img src=/public/images/icon-checked.png >', ' ') AS mps_sap_hq_ordertype_is_free_goods,

			IF(mps_sap_hq_ordertype_is_for_dummies, '<img src=/public/images/icon-checked.png >', ' ') AS mps_sap_hq_ordertype_is_for_dummies
			
		FROM db_retailnet.mps_sap_hq_ordertypes
	")
	->bind(Company_Mps_sap_hq_ordertype::DB_BIND_COMPANIES)
	->bind(Company_Mps_sap_hq_ordertype::DB_BIND_SAP_HQ_ORDER_TYPES)
	->bind(Company_Mps_sap_hq_ordertype::DB_BIND_SAP_HQ_ORDER_REASONS)
	->filter($filters)
	->order($order, $direction)
	->offset($offset, $rows)
	->fetchAll();

	if ($result) {

		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);

		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));

		$pageIndex = $pager->index();
		$pageControlls = $pager->controlls();
	}

	// toolbox: hide utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$order' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";

		
	// toolbox: button add
	if ( $can_edit_companies && $_REQUEST['add'] && !$archived) {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'caption' => $translate->add_new
		));
	}

	// toolbox: serach full text
	$toolbox[] = ui::searchbox();

	

	if ($toolbox) {
		$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
	}

	$table = new Table(array(
		'sort' => array('column' => $order, 'direction' => $direction)
	));

	$table->datagrid = $datagrid;

	$table->address_company(
		"href=".$_REQUEST['form'],
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->mps_sap_hq_ordertype_customer_number(
		'width=20%',
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_LEFT
	);

	$table->mps_sap_hq_ordertype_shipto_number(
		'width=20%',
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_LEFT
	);


	$table->sap_hq_order_type_name(
		'width=20%',
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_LEFT
	);


	$table->sap_hq_order_reason_name(
		'width=20%',
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_LEFT
	);


	$table->mps_sap_hq_ordertype_is_default(
		Table::ATTRIBUTE_NOWRAP,
		'width=5%',
		"align=center"
	);

	$table->mps_sap_hq_ordertype_is_free_goods(
		Table::ATTRIBUTE_NOWRAP,
		'width=5%',
		"align=center"
	);

	$table->mps_sap_hq_ordertype_is_for_dummies(
		Table::ATTRIBUTE_NOWRAP,
		'width=5%',
		"align=center"
	);


	


	$table->footer($pageIndex);
	$table->footer($pageControlls);
	$table = $table->render();

	echo $toolbox.$table;
