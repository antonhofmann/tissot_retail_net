<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$user_modified = $user->login;
	
	$mastersheet = new Mastersheet($application);
	$mastersheet->read($id);
	
	if ($mastersheet->id) {
				
		// application model
		$model = new Model($application);
		
		// exchange model
		$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);
		
		// master sheet order sheets
		$ordersheets = $model->query("
			SELECT mps_ordersheet_id 
			FROM mps_ordersheets
			WHERE mps_ordersheet_mastersheet_id = $id
		")->fetchAll();
		
		if ($ordersheets) {
			
			foreach ($ordersheets as $i => $row) {
				
				$ordersheet = $row['mps_ordersheet_id'];
				
				// get ordersheet items
				$items = $model->query("
					SELECT 
						mps_ordersheet_item_id,
						mps_ordersheet_item_purchase_order_number
					FROM mps_ordersheet_items
					WHERE mps_ordersheet_item_ordersheet_id = $ordersheet		
				")->fetchAll();
				
				if ($items) {
					
					foreach ($items as $i => $item) {
						
						$item_id = $item['mps_ordersheet_item_id'];
						$po_number = $item['mps_ordersheet_item_purchase_order_number'];
						
						// remove all planned quantitites
						$model->db->exec("
							DELETE FROM mps_ordersheet_item_planned
							WHERE mps_ordersheet_item_planned_ordersheet_item_id = $item_id
						");
						
						// remove all delivered items
						$model->db->exec("
							DELETE FROM mps_ordersheet_item_delivered
							WHERE mps_ordersheet_item_delivered_ordersheet_item_id = $item_id
						");
						
						// remove all confirmed items
						$model->db->exec("
							DELETE FROM mps_ordersheet_item_confirmed
							WHERE mps_ordersheet_item_confirmed_ordersheet_item_id = $item_id
						");
						
						// remove all shipped items
						$model->db->exec("
							DELETE FROM mps_ordersheet_item_shipped
							WHERE mps_ordersheet_item_shipped_ordersheet_item_id = $item_id
						");
						
						// remove all orders
						$model->db->exec("
							DELETE FROM mps_ramco_orders
							WHERE mps_ramco_order_po_number = '$po_number'		
						");
						
						// remove sales orders
						$exchange->db->exec("
							DELETE FROM mps_salesorders
							WHERE mps_salesorder_ponumber = '$po_number'		
						");
						
						// remove sales order items
						$exchange->db->exec("
							DELETE FROM mps_salesorderitems
							WHERE mps_salesorderitem_ponumber = '$po_number'		
						");
						
						// reset confirmed orders
						$exchange->db->exec("
							DELETE FROM ramco_orders
							WHERE mps_salesorder_ponumber = '$po_number'
						");
						
						// reset confirmed items
						$exchange->db->exec("
							DELETE FROM ramco_confirmed_items
							WHERE mps_salesorder_ponumber = '$po_number'
						");
						
						// reset shipped items
						$exchange->db->exec("
							DELETE FROM ramco_shipped_items
							WHERE mps_salesorderitem_ponumber = '$po_number'
						");
					}
					
					// reset ordersheet items
					$model->db->exec("
						UPDATE mps_ordersheet_items SET
							mps_ordersheet_item_quantity = NULL,
							mps_ordersheet_item_quantity_proposed = NULL,
							mps_ordersheet_item_quantity_approved = NULL,
							mps_ordersheet_item_quantity_confirmed = NULL,
							mps_ordersheet_item_quantity_shipped = NULL,
							mps_ordersheet_item_quantity_distributed = NULL,
							mps_ordersheet_item_order_date = NULL,
							mps_ordersheet_item_customernumber = NULL,
							mps_ordersheet_item_shipto = NULL,
							mps_ordersheet_item_desired_delivery_date = NULL,
							mps_ordersheet_item_purchase_order_number = NULL,
							user_modified = '$user_modified'
						WHERE mps_ordersheet_item_ordersheet_id = $ordersheet
					");
					
					// get order sheet versions
					$versions = $model->query("
						SELECT mps_ordersheet_version_id
						FROM mps_ordersheet_versions
						WHERE mps_ordersheet_version_ordersheet_id = $ordersheet
					")->fetchAll();
					
					// remove order sheet versions
					// and version items
					if ($versions) {
						
						foreach ($versions as $i => $value) {
							$model->db->exec("
								DELETE FROM mps_ordersheet_version_items
								WHERE mps_ordersheet_version_item_ordersheetversion_id = ".$value['mps_ordersheet_version_id']."		
							");
						}
						
						$model->db->exec("
							DELETE FROM mps_ordersheet_versions
							WHERE mps_ordersheet_version_ordersheet_id = $ordersheet	
						");
					}
					
					
					// remove order sheet ware houses
					$model->db->exec("
						DELETE FROM mps_ordersheet_warehouses
						WHERE mps_ordersheet_warehouse_ordersheet_id = $ordersheet		
					");
					
					// delete trackings
					$model->db->exec("
						DELETE FROM user_trackings
						WHERE user_tracking_entity_id = $ordersheet AND user_tracking_entity = 'order sheet'
					");
					
					// reset ordersheet
					$model->db->exec("
						UPDATE mps_ordersheets SET
							mps_ordersheet_workflowstate_id = 7,
							user_modified = '$user_modified'
						WHERE mps_ordersheet_id = $ordersheet
					");
				}	
			}
			
			// master sheet splitting lists
			$splitting_lists = $model->query("
				SELECT mps_mastersheet_splitting_list_id
				FROM mps_mastersheet_splitting_lists
				WHERE mps_mastersheet_splitting_list_mastersheet_id = $id
			")->fetchAll();
			
			// remove splitting lists
			if ($splitting_lists) {
				
				foreach ($splitting_lists as $i => $value) {
					$model->db->exec("
						DELETE FROM mps_mastersheet_splitting_list_items
						WHERE mps_mastersheet_splitting_list_item_splittinglist_id = ".$value['mps_mastersheet_splitting_list_id']."		
					");
				}
				
				$model->db->exec("
					DELETE FROM mps_mastersheet_splitting_lists
					WHERE mps_mastersheet_splitting_list_mastersheet_id = $id	
				");
			}
			
			// reset master sheet
			$mastersheet->update(array(
				'mps_mastersheet_consolidated' => 0,
				'mps_mastersheet_archived' => 0,
				'user_modified' => $user_modified
			));
			
			message::add(array(
				'type'=>'message',
				'keyword'=>'message_reset_exchange_data',
				'life'=>5000
			));
		}
		else {
			
			message::add(array(
				'type'=>'warning',
				'keyword'=>'warning_reset_exchange_data',
				'life'=>5000
			));
		}
		
	} else {
		
		message::add(array(
			'type'=>'error',
			'keyword'=>'error_reset_exchange_data',
			'life'=>5000
		));
	}
	
	$archived = ($_REQUEST['archived']) ? '/archived' : null;
	url::redirect("/$application/$controller$archived/$action/$id");
	
	