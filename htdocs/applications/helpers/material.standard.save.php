<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['mps_material_id'];
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	// material
	$material = new Material($application);
	$material->read($id);

	$data = array();
	
	if (in_array('mps_material_standard_order', $fields)) {
		$data['mps_material_standard_order'] = ($_REQUEST['mps_material_standard_order']) ? 1 : 0;
	}
	
	if (in_array('mps_material_standard_order_date', $fields)) {
		$data['mps_material_standard_order_date'] = ($_REQUEST['mps_material_standard_order_date']) ? date::sql($_REQUEST['mps_material_standard_order_date']) : null;
	}
	
	if (in_array('mps_material_standard_order_quantity', $fields)) {
		$data['mps_material_standard_order_quantity'] = ($_REQUEST['mps_material_standard_order_quantity']) ? $_REQUEST['mps_material_standard_order_quantity'] : null;
	}
	
	// update material data
	if ($data && $material->id) {
		$data['user_modified'] = $user->login;
		$response = $material->update($data);
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	
	// save selected companies
	if ($response && in_array('companies', $fields)) {
		
		$model = new Model($application);
		
		$model->db->query("
			DELETE FROM mps_material_addresses
			WHERE mps_material_address_material_id = $id		
		");
		
		if ($_REQUEST['companies'] && $_REQUEST['mps_material_standard_order']) {
			
			$created = $user->login;
			
			foreach ($_REQUEST['companies'] as $company => $value) {
				$model->db->query("
					INSERT INTO mps_material_addresses (
						mps_material_address_material_id,
						mps_material_address_address_id,
						user_created	
					) VALUES (
						$id, $company, '$created'	
					)		
				");
			}
		}
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	