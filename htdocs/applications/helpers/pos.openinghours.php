<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	date_default_timezone_set ('Europe/Zurich');
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$firstDay = 1325376000; // Sun, 01 Jan 2012 00:00:00 GMT
	$pos = $_REQUEST['pos'];
	$id = $_REQUEST['id'];
	
	// date time object
	$dateTime = new DateTime();
	$dateTime->setTimezone(new DateTimeZone('Europe/London'));
	$dateTime->setTimestamp($firstDay);

	if ($pos) {
		
		$model = new Model(Connector::DB_CORE);
		
		$storelocator = new Store_Locator();
		
		$data = array();
		
		switch ($_REQUEST['action']) {
				
			case 'load':
				
				// get opening hours
				$result = $model->query("
					SELECT openinghr_id, openinghr_time_24 
					FROM openinghrs
				")->fetchAll();
				
				$openning_hours = _array::datagrid($result);
				
				$referer = $dateTime->format('U');
				
				// load data
				$pos_opening_hour = new Pos_Opening_Hour(Connector::DB_CORE);
				$result = $pos_opening_hour->load($pos);
				
				if ($result) {
					
					foreach ($result as $key => $row) {
						
						$day = $row['posopeninghr_weekday_id'];
						$from = $openning_hours[$row['posopeninghr_from_openinghr_id']]['openinghr_time_24'];
						$to = $openning_hours[$row['posopeninghr_to_openinghr_id']]['openinghr_time_24'];
						
						list($start_hour, $start_minutes) = explode(':', $from); 
						list($end_hour, $end_minutes) = explode(':', $to); 
						
						// set time in milisecons
						$start = strtotime("+$day day $start_hour hours $start_minutes minutes", $referer)*1000;
						
						if ($row['posopeninghr_to_openinghr_id']==1) $end = strtotime("+$day day 24 hours", $referer)*1000;
						else $end = strtotime("+$day day $end_hour hours $end_minutes minutes", $referer)*1000;
						
						$data[] = array(
							'id' => $key,
							'start' => $start,
							'end' => $end
						);
					}
				}
			
			break;
			
			case 'save':
				
				// start time
				$dateTime->setTimestamp($_REQUEST['start']);
				$start = $dateTime->format('H:i');
				
				// week day
				$week_day = $dateTime->format('N');
				
				// end time
				$dateTime->setTimestamp($_REQUEST['end']);
				$end = $dateTime->format('H:i');

				// user
				$user_login = $user->login;
				
				// get opening hours
				$result = $model->query("
					SELECT 
						openinghr_time_24, 
						openinghr_id
					FROM openinghrs
				")->fetchAll();
				
				$openning_hours = _array::datagrid($result);
				$from = $openning_hours[$start]['openinghr_id'];
				$to = $openning_hours[$end]['openinghr_id'];
				$to = $from > $to ? 1 : $to;

				list($start_hour, $start_minutes) = explode(':', $start);
				list($end_hour, $end_minutes) = explode(':', $end);
				
				
				// get openening time
				$pos_opening_hour = new Pos_Opening_Hour(Connector::DB_CORE);
				$pos_opening_hour->read($id);
				
				if ($pos_opening_hour->id) {
					
					$currentFrom = $pos_opening_hour->from_openinghr_id;
					$currentTo = $pos_opening_hour->to_openinghr_id;
					
					$action = $pos_opening_hour->update(array(
						'posopeninghr_weekday_id' => $week_day,
						'posopeninghr_from_openinghr_id' => $from,
						'posopeninghr_to_openinghr_id' => $to,
						'user_modified' => $user_login,
						'date_modified' => date('Y-m-d H:i:s')
					));
					
					$data['message'] = $translate->message_request_updated;
					
				} else { 
					
					$action = $id = $pos_opening_hour->create(array(
						'posopeninghr_posaddress_id' => $pos,
						'posopeninghr_weekday_id' => $week_day,
						'posopeninghr_from_openinghr_id' => $from,
						'posopeninghr_to_openinghr_id' => $to,
						'user_created' => $user_login,
						'date_created' => date('Y-m-d H:i:s')
					));
					
					$data['message'] = $translate->message_request_inserted;
				}
			
				
				// referer
				$dateTime->setTimestamp($firstDay);
				$referer = $dateTime->format('U');

				
				// apply this timing for another days
				if ($_REQUEST['day']) {
					
					foreach ($_REQUEST['day'] as $day => $value) {
						
						if ($day <> $week_day) { 
							
							// read from range
							$pos_opening_hour->id = null;
							$pos_opening_hour->read_from_range($pos, $day, $from, $to);
							
							if ($pos_opening_hour->id) { 
								$response = $pos_opening_hour->update(array(
									'posopeninghr_weekday_id' => $day,
									'posopeninghr_from_openinghr_id' => $from,
									'posopeninghr_to_openinghr_id' => $to,
									'user_modified' => $user_login,
									'date_modified' => date('Y-m-d H:i:s')
								));
							} else { 
								$response = $pos_opening_hour->create(array(
									'posopeninghr_posaddress_id' => $pos,
									'posopeninghr_weekday_id' => $day,
									'posopeninghr_from_openinghr_id' => $from,
									'posopeninghr_to_openinghr_id' => $to,
									'user_created' => $user_login,
									'date_created' => date('Y-m-d H:i:s')
								));
							}
							
							if ($response) {
								
								// start time
								$start_time = strtotime("+$day day $start_hour hours $start_minutes minutes", $referer)*1000;
								
								// end time
								if ($to==1) $end_time = strtotime("+$day day 59 hours 59 minutes 59 seconds", $referer)*1000;
								else $end_time = strtotime("+$day day $end_hour hours $end_minutes minutes", $referer)*1000;
								
								$data['events'][] = array(
									'id' => $pos_opening_hour->id,
									'start' => $start_time,
									'end' => $end_time
								);
							}
						}
					}
				}

				// refresh store locator
				$storelocator->update($pos);
			
			break;
			
			case 'delete':
			
				$pos_opening_hour = new Pos_Opening_Hour(Connector::DB_CORE);
				$pos_opening_hour->read($id);
				$action = $pos_opening_hour->delete();
				
				$pos_opening_hour = new Pos_Opening_Hour(Connector::DB_RETAILNET_STORE_LOCATOR);
				$pos_opening_hour->read($id);
				$action = $pos_opening_hour->delete();
				
				$storelocator->update($pos);
				
				$data['message'] = $translate->message_request_deleted;
				
			break;
			
			case 'getdays':
				
				$data = array();
				
				// start time
				$dateTime->setTimestamp($_REQUEST['start']);
				$start = $dateTime->format('H:i');
				
				// week day
				$week_day = $dateTime->format('N');
				
				// end time
				$dateTime->setTimestamp($_REQUEST['end']);
				$end = $dateTime->format('H:i');
				
				// get opening hours
				$result = $model->query("
					SELECT 
						openinghr_time_24, 
						openinghr_id
					FROM openinghrs
				")->fetchAll();
				
				$openning_hours = _array::datagrid($result);
				$count = count($openning_hours);
				
				$from = $openning_hours[$start]['openinghr_id'];
				$to = ($end==1) ? $openning_hours[$count-1]['openinghr_id'] : $openning_hours[$end]['openinghr_id'];
				
				// get opening hours
				$result = $model->query("
					SELECT openinghr_id, openinghr_time_24 
					FROM openinghrs
				")->fetchAll();
				
				$openning_hours = _array::datagrid($result);
				
				// pos hours
				$pos_opening_hour = new Pos_Opening_Hour(Connector::DB_CORE);
				
				$result = $pos_opening_hour->load($pos);
				
				// current reange pos data
				$posRangeData = array();
				
				if ($result) {
					
					foreach ($result as $key => $row) {
						
						$day = $row['posopeninghr_weekday_id'];
						$start = ($row['posopeninghr_from_openinghr_id']==1) ? $row['posopeninghr_from_openinghr_id'] : $row['posopeninghr_from_openinghr_id']+1;
						$end = ($row['posopeninghr_to_openinghr_id']==1) ? $count-1 : $row['posopeninghr_to_openinghr_id']-1;
						
						$range = range($from,$to);
						//$range = range($start,$end);
						//$overRange = ($from < $start && $to > $end) ? true : false;
						//$inRange = (in_array($from, $range) || in_array($to, $range)) ? true : false;
						
						// find in range
						if (in_array($start, $range) || in_array($end, $range) || ($start<$from && $end>$to)) {
							$posRangeData[$day] = true;
						}
						
						
						if (!$data[$day] && ($overRange || $inRange)) {
							$data[$day]['checked'] = false;
							$data[$day]['disabled'] = true;
						}
						
					}
				}
				
				for ($i=1; $i <= 7; $i++) {
					
					if ($_REQUEST['mod']=='edit') {	
						if ($posRangeData[$i]) {
							$data[$i]['checked'] = true;
							$data[$i]['disabled'] = false;
						} else {
							$data[$i]['checked'] = false;
							$data[$i]['disabled'] = true;
						}			
					} else {
						if ($posRangeData[$i]) {
							$data[$i]['checked'] = false;
							$data[$i]['disabled'] = true;
						} else {
							$data[$i]['checked'] = 'checked';
							$data[$i]['disabled'] = false;
						}
					}
					
					if ($i==$week_day) {
						$data[$i]['disabled'] = true;
						$data[$i]['checked'] = 'checked';
					}
				}
							
			break;
			
			case 'copy' :
				
				// load existing hours
				$pos_opening_hour = new Pos_Opening_Hour(Connector::DB_CORE);
				$result = $pos_opening_hour->load($id);
				
				if ($result) {
					
					$model->db->query("
						DELETE FROM posopeninghrs
						WHERE posopeninghr_posaddress_id = $pos
					");
					
					$openning_hours = $model->query("
						SELECT openinghr_id, openinghr_time_24 
						FROM openinghrs
					")->fetchAll();
				
					$openning_hours = _array::datagrid($openning_hours);
					
					$referer = $dateTime->format('U');
					
					foreach ($result as $key => $row) {
							
						$inserted = $pos_opening_hour->create(array(
							'posopeninghr_posaddress_id' => $pos,
							'posopeninghr_weekday_id' => $row['posopeninghr_weekday_id'],
							'posopeninghr_from_openinghr_id' => $row['posopeninghr_from_openinghr_id'],
							'posopeninghr_to_openinghr_id' => $row['posopeninghr_to_openinghr_id'],
							'user_created' => $user->login,
							'date_created' => date('Y-m-d H:i:s')
						));
						
						if ($inserted) {
							
							$action = true;
							
							$day = $row['posopeninghr_weekday_id'];
							$from = $openning_hours[$row['posopeninghr_from_openinghr_id']]['openinghr_time_24'];
							$to = $openning_hours[$row['posopeninghr_to_openinghr_id']]['openinghr_time_24'];
							
							list($start_hour, $start_minutes) = explode(':', $from); 
							list($end_hour, $end_minutes) = explode(':', $to); 
							
							// set time in milisecons
							//$start = strtotime("+$day day $start_hour hours $start_minutes minutes", $referer)*1000;
							//$end = strtotime("+$day day $end_hour hours $end_minutes minutes", $referer)*1000;
							
							// start time
							$start_time = strtotime("+$day day $start_hour hours $start_minutes minutes", $referer)*1000;
							
							// end time
							if ($row['posopeninghr_to_openinghr_id']==1) $end_time = strtotime("+$day day 59 hours 59 minutes 59 seconds", $referer)*1000;
							else $end_time = strtotime("+$day day $end_hour hours $end_minutes minutes", $referer)*1000;
							
							$data['events'][] = array(
								'id' => $inserted,
								'start' => $start_time,
								'end' => $end_time
							);
						}
					}

					if ($inserted) {
						$result = $model->query("SHOW TABLE STATUS LIKE 'posopeninghrs'")->fetch();
						$data['last_id'] = $result['Auto_increment'];
					}
				}


				// pos closing days
				$pos_closing_day = new Pos_Closing_Day(Connector::DB_CORE);
				$result = $pos_closing_day->read_from_pos($id);
				
				$model->db->query("
					DELETE FROM posclosinghrs
					WHERE posclosinghr_posaddress_id = $pos
				");
				
				if ($result) {

					$inserted = $pos_closing_day->create(array(
						'posclosinghr_posaddress_id' => $pos,
						'posclosinghr_text' => $result['posclosinghr_text']
					));
					
					$data['closing_days'] = $result['posclosinghr_text'];
				}
				
				$data['message'] = $translate->message_request_saved;

				$storelocator->update($pos);
				
			break;
			
			case 'deleteall':
				
				$model->db->query("
					DELETE FROM posopeninghrs
					WHERE posopeninghr_posaddress_id = $pos
				");
				
				$model->db->query("
					DELETE FROM posclosinghrs
					WHERE posclosinghr_posaddress_id = $pos
				");
				
				$result = $model->query("SHOW TABLE STATUS LIKE 'posopeninghrs'")->fetch();
				
				$data['last_id'] = $result['Auto_increment'];
				$data['message'] = $translate->message_request_deleted;
				
				$storelocator->update($pos);
				
			break;
		}

		
	}

	header('Content-Type: text/json');
	echo json_encode($data);

