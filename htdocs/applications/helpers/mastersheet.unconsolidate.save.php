<?php 
/**
 * 	Master Sheet unconsolidation
 * 
 * 	This process is possible only if master sheet has not generated sales orders.
 * 	Reset Master sheet state as unconsolidated.
 * 	Reset all consolidateed order sheets as approved.
 * 	Reste all consolidated not ordered order sheet items as null value.
 * 	
 * 	@author: admir.serifi@mediaparx.ch
 * 	@copyright (c) Swatch AG
 *  @version 1.1
 * 
 */
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	// db connector
	$model = new Model($application);
	
	$mastersheet = new Mastersheet($application);
	$mastersheet->read($id);
	
	
	// tcount all mastersheets items
	$result = $model->query("
		SELECT 
			COUNT(mps_ordersheet_item_id) AS total
		FROM mps_ordersheet_items	
		INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_ordersheet_id	
		WHERE 
			mps_ordersheet_mastersheet_id = $id
	")->fetch();
	
	$totalMastersheetItems = $result['total'];
	
	
	// count all master sheet items not ordered
	$result = $model->query("
		SELECT 
			COUNT(mps_ordersheet_item_id) AS total
		FROM mps_ordersheet_items		
		INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
		WHERE 
			mps_ordersheet_mastersheet_id = $id
			AND ( 
				mps_ordersheet_item_order_date IS NULL 
				OR mps_ordersheet_item_order_date = '0000-00-00' 
				OR mps_ordersheet_item_order_date = '' 
			)
	")->fetch();
	
	$totalMastersheetItemsNotOrdered = $result['total'];

	if ($mastersheet->consolidated && $totalMastersheetItems == $totalMastersheetItemsNotOrdered) {
		
		// set mastersheet as unconsolidated
		$mastersheet->update(array(
			'mps_mastersheet_consolidated' => 0,
			'mps_mastersheet_archived' => 0,
			'user_modified' => $user->login
		));
		
		
		// set all order sheets as approved
		$result = $model->db->query("
			UPDATE mps_ordersheets SET 
				mps_ordersheet_workflowstate_id = ".Workflow_State::STATE_APPROVED."
			WHERE mps_ordersheet_mastersheet_id = $id
		");
		
		
		// get all not ordered items
		$items = $model->query("
			SELECT 
				mps_ordersheet_item_id
			FROM mps_ordersheet_items
			INNER JOIN mps_ordersheets ON  mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
			WHERE 
				mps_ordersheet_mastersheet_id = $id
				AND ( 
					mps_ordersheet_item_quantity_approved IS NULL 
					OR mps_ordersheet_item_quantity_approved = '' 
					OR mps_ordersheet_item_quantity_approved = 0 
				)
		")->fetchAll();
		
		// reset all export fields
		if ($items) {
			
			$item = new Ordersheet_Item($application);
			
			foreach ($items as $row) {
				
				$item->read($row['mps_ordersheet_item_id']);
				
				$item->update(array(
					'mps_ordersheet_item_desired_delivery_date' => null,
					'mps_ordersheet_item_order_date' => null,
					'mps_ordersheet_item_purchase_order_number' => null,
					'mps_ordersheet_item_shipto' => null,
					'mps_ordersheet_item_customernumber' => null,
					'user_modified' => $user->login,
					'date_modified' => date('Y-m-d H:i:s')
				));
			}
		}
		
		Message::add(array(
			'type' => 'message',
			'keyword' => 'message_mastersheet_unconsolidate',
			'data' => array('mps_mastersheet_name' => $mastersheet->name)
		));
	}
	else {
		
		message::add(array(
			'type' => 'error',
			'keyword' => 'error_mastersheet_unconsolidate',
			'sticky' => true
		));
	}
	
	url::redirect("/$application/$controller/$action/$id");
	
	