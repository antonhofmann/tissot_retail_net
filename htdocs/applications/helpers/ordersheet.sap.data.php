<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['ordersheet'];
	$version = $_REQUEST['version'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	// model
	$model = new Model($application);
	
	// current order sheet	
	$ordersheet = new Ordersheet($application);
	$ordersheet->read($id);
	
	// is archived
	$isDistributed = ($ordersheet->state()->isDistributed() && $ordersheet->isDistributed()) ? true : false;
	
	// company
	$company = new Company();
	$company->read($ordersheet->address_id);
	
	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Global Properties
	Set editabled and visibility vars
********************************************************************************************************************************************************************************************/
	
	// planning column keyword
	define(COLUMN_DISTRIBUTED, 'distributed');
	
	// distribution column keyword
	define(COLUMN_SAP_CONFIRMED, 'sap-confirmed');
	
	// partially confirmed distribution keyword
	define(COLUMN_SAP_SHIPPED, 'sap-shipped');
	
	// spreadsheet readonly
	define('CELL_READONLY', 'true');
	
	// spreadsheet attribute readonly
	$spreadsheet_attribute_readonly =  'true';
	
	// calculation area (first in calculation set)
	$show_calculation_area = true;

	// confirmed wanings
	$warningMessages['confirmed'] = array(
		1 => "Sales organization unknown",
		2 => "SAP number sold to unknown",
		3 => "SAP number ship  to unknown",
		4 => "PO-number unknown",
		5 => "PO-line number unknown",
		6 => "EAN number unknown",
		7 => "No stock available at a moment"
	);

	// shipped warnings
	$warningMessages['shipped'] = array(
		1 => "SAP order number unknown",
		2 => "SAP order line number unknown",
		3 => "EAN number unknown",
		4 => "PO-number unknown",
		5 => "SAP number sold to unknown"
	);
	
/*******************************************************************************************************************************************************************************************
	Request Filters
********************************************************************************************************************************************************************************************/
	
	$filters = array();
	$filer_labels = array();
	
	// filter: pos
	$filters['default'] = "(
		posaddress_client_id = $ordersheet->address_id 
		OR (
			address_parent = $ordersheet->address_id 
			AND address_client_type = 3
		)  
	)";
	
	// filter: distribution channels
	if ($_REQUEST['distribution_channels']) {
		$filters['distribution_channels'] = 'posaddress_distribution_channel = '.$_REQUEST['distribution_channels'];
		$filter_active = 'active';
	}
	
	// filter: turnover classes
	if ($_REQUEST['turnoverclass_watches']) {
		$filters['turnoverclass_watches'] = 'posaddress_turnoverclass_watches = '.$_REQUEST['turnoverclass_watches'];
		$filter_active = 'active';
	}
	
	// filter: sales represenatives
	if ($_REQUEST['sales_representative']) {
		$filters['sales_representative'] = 'posaddress_sales_representative = '.$_REQUEST['sales_representative'];
		$filter_active = 'active';
	}
	
	// filter: decoration persons
	if ($_REQUEST['decoration_person']) {
		$filters['decoration_person'] = 'posaddress_decoration_person = '.$_REQUEST['decoration_person'];
		$filter_active = 'active';
	}
	
	// provinces
	if ($_REQUEST['provinces']) {
		$filters['provinces'] = 'province_id = '.$_REQUEST['provinces'];
		$filter_active = 'active';
	}
	
	// for archived ordersheets show only POS locations which has distributed quantities 
	if ($isDistributed) {
		
		$result = $model->query("
			SELECT DISTINCT 
				mps_ordersheet_item_delivered_posaddress_id AS pos
			FROM mps_ordersheet_item_delivered	
			INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id
			WHERE mps_ordersheet_item_ordersheet_id = $ordersheet->id AND mps_ordersheet_item_delivered_posaddress_id > 0
		")->fetchAll();
		
		if ($result) {
			
			$array = array();
			
			foreach ($result as $row) {
				$array[] = $row['pos'];
			}

			$filters['active'] = 'posaddress_id IN ('.join(',', $array).')';
		}
	}
	// otherwise show all active pos locations
	else {
		$filters['active'] = "(
			posaddress_store_closingdate = '0000-00-00'
			OR posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = ''
		)";
	}
	
	
/*******************************************************************************************************************************************************************************************
	Dataloader
	POS Locations
********************************************************************************************************************************************************************************************/
	
	$posLocations = array();
	
	$pos_filter = $filters ? join(' AND ', array_values($filters)) : null;
	
	$result = $model->query("
		SELECT DISTINCT
			posaddress_id,
			posaddress_name,
			posaddress_place,
			postype_id,
			postype_name
		FROM db_retailnet.posaddresses
		INNER JOIN db_retailnet.addresses ON address_id = posaddress_client_id
		INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
		INNER JOIN db_retailnet.provinces ON province_id = place_province
		INNER JOIN db_retailnet.postypes ON postype_id = posaddress_store_postype
		WHERE $pos_filter
		ORDER BY posaddress_name, posaddress_place
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$pos = $row['posaddress_id'];
			$type = $row['postype_id'];
			
			$posLocations[$type]['caption'] = $row['postype_name'];
			$posLocations[$type]['pos'][$pos]['name'] = $row['posaddress_name'];
			$posLocations[$type]['pos'][$pos]['place'] = $row['posaddress_place'];
		}
	}
	
/*******************************************************************************************************************************************************************************************
	Dataloader
	Order Sheet Items
********************************************************************************************************************************************************************************************/
	
	$ordersheetItems = array();
	
	$result = $model->query("
		SELECT
			mps_ordersheet_item_id,
			mps_ordersheet_item_material_id,
			mps_ordersheet_item_quantity_distributed,
			mps_ordersheet_item_purchase_order_number,
			mps_material_planning_type_id,
			mps_material_planning_type_name,
			mps_material_collection_category_id,
			mps_material_collection_category_code,
			mps_material_id,
			mps_material_code,
			mps_material_name,
			mps_material_ean_number
		FROM mps_ordersheet_items
		INNER JOIN mps_ordersheets ON  mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
		INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
		LEFT JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id
		LEFT JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id
		WHERE 
			mps_ordersheet_id = $ordersheet->id
			AND mps_ordersheet_item_quantity_shipped > 0
		ORDER BY
			mps_material_planning_type_name,
			mps_material_collection_category_code,
			mps_material_code,
			mps_material_name
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$item = $row['mps_ordersheet_item_id'];
			$material = $row['mps_ordersheet_item_material_id'];
			$planning = $row['mps_material_planning_type_id'];
			$collection = $row['mps_material_collection_category_id'];
			
			$ordersheetItems[$planning]['caption'] = $row['mps_material_planning_type_name'];
			$ordersheetItems[$planning]['data'][$collection]['caption'] = $row['mps_material_collection_category_code'];
			$ordersheetItems[$planning]['data'][$collection]['data'][$item]['id'] = $row['mps_material_id'];
			$ordersheetItems[$planning]['data'][$collection]['data'][$item]['code'] = $row['mps_material_code'];
			$ordersheetItems[$planning]['data'][$collection]['data'][$item]['name'] = $row['mps_material_name'];
			$ordersheetItems[$planning]['data'][$collection]['data'][$item]['quantity'] = $row['mps_ordersheet_item_quantity_distributed'];
		}
	}
	

/*******************************************************************************************************************************************************************************************
	Dataloader
	Order Sheet Items Delivered Quantities
********************************************************************************************************************************************************************************************/

	$distributedItems = array();
	$distributedPosItems = array();
	$distributedWarehouseItems = array();
	
	$result = $model->query("
		SELECT DISTINCT
			mps_ordersheet_item_delivered_id,
			mps_ordersheet_item_delivered_posaddress_id,
			mps_ordersheet_item_delivered_warehouse_id,
			mps_ordersheet_item_delivered_quantity,
			mps_ordersheet_item_delivered_confirmed,
			mps_ordersheet_item_id,
			mps_ordersheet_item_material_id,
			mps_material_material_planning_type_id,
			mps_material_material_collection_category_id,
			mps_ordersheet_warehouse_stock_reserve,
			posaddress_sapnumber,
			address_warehouse_sap_customer_number
		FROM mps_ordersheet_item_delivered
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id
		INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
		LEFT JOIN mps_ordersheet_warehouses ON mps_ordersheet_warehouse_id = mps_ordersheet_item_delivered_warehouse_id
		LEFT JOIN db_retailnet.posaddresses ON posaddress_id = mps_ordersheet_item_delivered_posaddress_id
		LEFT JOIN db_retailnet.address_warehouses ON address_warehouse_id = mps_ordersheet_warehouse_address_warehouse_id
		WHERE
			mps_ordersheet_item_ordersheet_id = $ordersheet->id 
			AND mps_ordersheet_item_delivered_confirmed IS NOT NULL
		ORDER BY 
			mps_ordersheet_item_delivered_confirmed DESC,
			mps_ordersheet_item_delivered.date_created DESC
	")->fetchAll();
	
	if ($result) { 

		foreach ($result as $row) {

			$id = $row['mps_ordersheet_item_delivered_id'];
			$material = $row['mps_ordersheet_item_material_id'];
			$item = $row['mps_ordersheet_item_id'];
			$pos = $row['mps_ordersheet_item_delivered_posaddress_id'];
			$warehouse = $row['mps_ordersheet_item_delivered_warehouse_id'];
			$quantity = $row['mps_ordersheet_item_delivered_quantity'];
			
			$distributedItems[$id] = $row;
			
			// pos quantities
			if ($pos) {
				$distributedPosItems[$item][$pos] = $distributedPosItems[$item][$pos] + $quantity;				
			}
			
			// warehouse quantities
			if ($warehouse && !$row['mps_ordersheet_warehouse_stock_reserve']) {
				$distributedWarehouseItems[$item][$warehouse] = $distributedWarehouseItems[$item][$warehouse] + $quantity;
			}
		}
	}
	
	// all involved distributions
	$involvedDistributedItems = $distributedItems ? join(',', array_filter(array_keys($distributedItems))) : null;
	
	
/*******************************************************************************************************************************************************************************************
	SAP confirmed items
********************************************************************************************************************************************************************************************/
	
	$sapPosConfirmedItems = array();
	$sapWarehouseConfirmedItems = array();
	
	$sapPosConfirmedWarnings = array();
	$sapWarehouseConfirmedWarnings = array();
	
	$result = $model->query("
		SELECT * 
		FROM sap_confirmed_items
		WHERE sap_confirmed_item_mps_distribution_id IN ($involvedDistributedItems)
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$id = $row['sap_confirmed_item_mps_distribution_id'];
			$item = $distributedItems[$id]['mps_ordersheet_item_id'];
			$pos = $distributedItems[$id]['mps_ordersheet_item_delivered_posaddress_id'];
			$warehouse = $distributedItems[$id]['mps_ordersheet_item_delivered_warehouse_id'];
			$quantity = $row['sap_confirmed_item_confirmed_quantity'];
			$warning = $row['sap_confirmed_item_status_code'];
			
			if ($pos) {
				
				$sapPosConfirmedItems[$item][$pos] = $sapPosConfirmedItems[$item][$pos] + $quantity;
				
				// import warning message
				if ($warning && $warningMessages['confirmed'][$warning]) {
					$sapPosConfirmedWarnings[$item][$pos] = $warningMessages['confirmed'][$warning];
				} elseif (!$quantity) {
					$sapPosConfirmedWarnings[$item][$pos] = $warningMessages['confirmed'][7];
				}
				
			} elseif($warehouse) {
				
				$sapWarehouseConfirmedItems[$item][$warehouse] = $sapWarehouseConfirmedItems[$item][$warehouse] + $quantity;
				
				// import warning message
				if ($warning && $warningMessages['confirmed'][$warning]) {
					$sapWarehouseConfirmedWarnings[$item][$warehouse] = $warningMessages['confirmed'][$warning];
				}
				elseif (!$quantity) {
					$sapWarehouseConfirmedWarnings[$item][$warehouse] = $warningMessages['confirmed'][7];
				}
			}
		}
	}
	
	
/*******************************************************************************************************************************************************************************************
	SAP shipped items
********************************************************************************************************************************************************************************************/
	
	$sapPosShippedItems = array();
	$sapWarehouseShippedItems = array();
	$sapPosShippedWarnings = array();
	$sapWarehouseShippedWarnings = array();
	
	$result = $model->query("
		SELECT * 
		FROM sap_shipped_items
		WHERE sap_shipped_item_mps_distribution_id IN ($involvedDistributedItems)
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$id = $row['sap_shipped_item_mps_distribution_id'];
			$item = $distributedItems[$id]['mps_ordersheet_item_id'];
			$pos = $distributedItems[$id]['mps_ordersheet_item_delivered_posaddress_id'];
			$warehouse = $distributedItems[$id]['mps_ordersheet_item_delivered_warehouse_id'];
			$quantity = $row['sap_shipped_item_quantity'];
			$warning = $row['sap_shipped_item_status_code'];
			
			if ($pos) {
				
				$sapPosShippedItems[$item][$pos] = $sapPosShippedItems[$item][$pos] + $quantity;
				
				// import warning messages
				if ($warning && $warningMessages['shipped'][$warning]) {
					$sapPosShippedWarnings[$item][$pos] = $warningMessages['shipped'][$warning];
				}
				
			} elseif ($warehouse) {
				
				$sapWarehouseShippedItems[$item][$warehouse] = $sapWarehouseShippedItems[$item][$warehouse] + $quantity;
				
				// import warning messages
				if ($warning && $warningMessages['shipped'][$warning]) {
					$sapWarehouseShippedWarnings[$item][$warehouse] = $warningMessages['shipped'][$warning];
				}
			}
		}
	}

	$console['warnings.pos.shipped'] = $sapPosShippedWarnings;
	$console['warnings.warehouse.shipped'] = $sapWarehouseShippedWarnings;
/*******************************************************************************************************************************************************************************************
	Fixed Area
********************************************************************************************************************************************************************************************/
	
	$rowFixedFirst = 1;
	$rowFixedLast = 3;
	$rowFixedSeparator = 0;
	$rowFixedTotal = 3 + $rowFixedSeparator;

	$colFixedFirst = 1;
	$colFixedLast = 2;
	$colFixedSeperator = 1;
	$colFiexedTotal = 2 + $colFixedSeperator;
	
/*******************************************************************************************************************************************************************************************
	Calculated Area
********************************************************************************************************************************************************************************************/
	
	$rowCalculated = array();
	
	// totals
	$rowCalculated[0] = "Exported / SAP Confirmed / SAP Shipped";
	
	// total calculated rows
	$rowCalculatedSeperator = 1;
	$rowCalculatedTotal = ($rowCalculated) ? count($rowCalculated) + $rowCalculatedSeperator : 0;
	
	// calcullated rows
	if ($rowCalculatedTotal) {
		$rowCalculatedFirst = $rowFixedTotal + 1;
		$rowCalculatedLast = $rowCalculatedFirst + $rowCalculatedTotal - 1;
	}

/*******************************************************************************************************************************************************************************************
	Spreadsheet datagrid rows
********************************************************************************************************************************************************************************************/
	
	$i=0;
	
	$rowDataGrid = array();
	$rowDataGridSeparator = 0;
	$group = array();
	
	// row group separators
	$rowgroup_separator = 0;
	$total_rowgroup_separators = 0;
	
	if ($posLocations) {
	
		foreach ($posLocations as $type => $data) {
	
			$rowDataGrid[$i]['type'] = $type;
			$rowDataGrid[$i]['caption'] = htmlentities($data['caption'], ENT_QUOTES);
			$rowDataGrid[$i]['group'] = true;
	
			$j = $i+1;
	
			foreach ($data['pos'] as $pos => $value) {
	
				$rowDataGrid[$j]['type'] = $type;
				$rowDataGrid[$j]['pos'] = $pos;
	
				$caption = $value['name'].' ('.$value['place'].')';
				$caption = str_replace("'", "", $caption);
				$rowDataGrid[$j]['caption'] = (strlen($caption) > 50) ? substr($caption, 0, 50).'..' : $caption;
				$j++;
			}
	
			$i = $j + $rowgroup_separator;
	
			$total_rowgroup_separators = $total_rowgroup_separators + $rowgroup_separator;
		}
	
		// remove last seperator
		if ($total_rowgroup_separators) $total_rowgroup_separators = $total_rowgroup_separators - 1;
	}
	

/*******************************************************************************************************************************************************************************************
	Spreadsheet datagrid columns
********************************************************************************************************************************************************************************************/
	
	$i=0;
	
	$colDataGrid = array();
	$captionPlanningType = array();
	$planning_column_index = 0;
	$item_colum_index = 0;
	
	// row references separator
	$referenced_column_separator = 0;
	
	// column group separators
	$colgroup_separator = 1;
	$total_colgroup_separators = 0;
	$total_item_columns = 3;
	
	if ($ordersheetItems) {
	
		foreach ($ordersheetItems as $planning => $data) {
				
			foreach ($data['data'] as $collection => $value) {
				
				// planning type caption
				$captionPlanningType[$i]['merge'] = count($value['data']) * $total_item_columns;
					
				// planning type caption
				$caption_planning = htmlentities($data['caption'], ENT_QUOTES);
				$caption_collection = htmlentities($value['caption'], ENT_QUOTES);
				$captionPlanningType[$i]['caption'] = "<strong>$caption_planning</strong><span>$caption_collection</span>";
	
				foreach ($value['data'] as $item => $row) {
						
					// skip separator columns
					if ($i==$item_colum_index) {
													
						// merge length for item caption
						$merge_length = 3;
							
						// item caption
						$colDataGrid[$i]['merge'] = $merge_length;
						$colDataGrid[$i]['code'] = $row['code'];
							
						// next item index
						$item_colum_index = $i+$merge_length;
					
						$sumPos = is_array($distributedPosItems[$item]) ? array_sum($distributedPosItems[$item]) : 0;
						$sumWarehouse = is_array($distributedWarehouseItems[$item]) ? array_sum($distributedWarehouseItems[$item]) : 0;
						
						// distributed quantites
						$colDataGrid[$i]['key'] = COLUMN_DISTRIBUTED;
						$colDataGrid[$i]['id'] = $item;
						$colDataGrid[$i]['material'] = $row['id'];
						$colDataGrid[$i]['quantity'] = $sumPos+$sumWarehouse ?: null;
						$i++;
		
						$sumPos = is_array($sapPosConfirmedItems[$item]) ? array_sum($sapPosConfirmedItems[$item]) : 0;
						$sumWarehouse = is_array($sapWarehouseConfirmedItems[$item]) ? array_sum($sapWarehouseConfirmedItems[$item]) : 0;
	
						// sap confirmed quantite
						$colDataGrid[$i]['key'] = COLUMN_SAP_CONFIRMED;
						$colDataGrid[$i]['id'] = $item;
						$colDataGrid[$i]['quantity'] = $sumPos+$sumWarehouse ?: null;
						$i++;
						
						$sumPos = is_array($sapPosShippedItems[$item]) ? array_sum($sapPosShippedItems[$item]) : 0;
						$sumWarehouse = is_array($sapWarehouseShippedItems[$item]) ? array_sum($sapWarehouseShippedItems[$item]) : 0;
	
						// sap shipped items
						$colDataGrid[$i]['key'] = COLUMN_SAP_SHIPPED;
						$colDataGrid[$i]['id'] = $item;
						$colDataGrid[$i]['quantity'] =  $sumPos+$sumWarehouse ?: null;
						$i++;
					}
						
					// next first item index
					$item_colum_index = $i;
				}
	
				// increment
				$i = $i+$colgroup_separator;
	
				// next first item index
				$item_colum_index = $i;
	
				// colgroup separator
				$total_colgroup_separators = $total_colgroup_separators + $colgroup_separator;
			}
		}
	
		// remove last seperator
		if ($total_colgroup_separators) {
			$total_colgroup_separators = $total_colgroup_separators - 1;
		}
	}
	
	// total row references
	$rowDataGridTotal = ($rowDataGrid) ? count($rowDataGrid) + $total_rowgroup_separators + $rowDataGridSeparator : 0;
	
	// totall column references
	$colDataGridTotal = ($colDataGrid) ? count($colDataGrid) + $total_colgroup_separators + $referenced_column_separator : 0;
	
	// referenced rows
	if ($rowDataGridTotal) {
		$rowDataGridFirst = $rowFixedTotal + $rowCalculatedTotal + 1;
		$rowDataGridLast = $rowDataGridFirst + $rowDataGridTotal - 1;
	}
	
	// referenced columns
	if ($colDataGridTotal) {
		$colDataGridFirst = $colFiexedTotal + $total_calcullated_columns + 1;
		$colDataGridLast = $colDataGridFirst + $colDataGridTotal - 1;
	}
	
/*******************************************************************************************************************************************************************************************
	Extended Rows
	Order sheet warehouses
********************************************************************************************************************************************************************************************/
	
	$rowExtended = array();
	$rowExtendedSeparator = 0;
	
	// load all ordersheet registred warehouses
	$ordersheetWarehouses = $ordersheet->warehouse()->load();
	
	if ($ordersheetWarehouses) {
		
		$i=0;
		
		$rowExtended[$i] = "Warehouses";
		
		// load all active company warehouses
		$companyWarehouses = $ordersheet->company()->warehouse()->load();
		$companyWarehouses = _array::datagrid($companyWarehouses);
		
		foreach ($ordersheetWarehouses as $row) {
			
			$warehouse = $row['mps_ordersheet_warehouse_address_warehouse_id'];

			if (!$row['mps_ordersheet_warehouse_stock_reserve']) {
				$i++;
				$rowExtended[$i]['id'] = $row['mps_ordersheet_warehouse_id'];
				$rowExtended[$i]['warehouse'] = $warehouse;
				$rowExtended[$i]['name'] = $companyWarehouses[$warehouse]['address_warehouse_name'];
			}
		}
	}
	
	$rowExtendedTotal = ($rowExtended) ? count($rowExtended) + $rowExtendedSeparator : 0;

	// row limits
	if ($rowExtendedTotal) {
		$rowExtendedFirst = $rowFixedTotal + $rowCalculatedTotal + $rowDataGridTotal + 1;
		$rowExtendedLast = $rowExtendedFirst + $rowExtendedTotal - 1;
	}
	
	
/*******************************************************************************************************************************************************************************************
	SPREADSHEED BUILDER
********************************************************************************************************************************************************************************************/

	$rowSpreadSheetTotal = $rowFixedTotal + $rowCalculatedTotal + $rowDataGridTotal + $rowExtendedTotal;
	$colSpreadSheetTotal = $colFiexedTotal + $colDataGridTotal + $total_extended_columns + $total_calcullated_columns;
	
	$spreadsheet_fixed_rows = $rowFixedTotal + $rowCalculatedTotal;
	$spreadsheet_fixed_columns = $colFiexedTotal;
	
	// datagrid
	if ($rowDataGridTotal && $colDataGridTotal) {
	
		for ($row=1; $row <= $rowSpreadSheetTotal; $row++) {
				
			for ($col=1; $col <= $colSpreadSheetTotal; $col++) {
	
				$index = spreadsheet::key($col, $row);

				switch ($row) {
					
					case 1;
						if ($col >= $colDataGridFirst) {
							$j = $col-$colDataGridFirst;
							$grid[$index]['cls'] = ($colDataGrid[$j]) ? 'row-separator' : 'row-separator col-separator';
						}
						
						$grid[$index]['html'] = "&nbsp;";
						$grid[$index]['readonly'] = CELL_READONLY;
						
					break;
	
					// CAPTIONS
					case 2:
	
						if ($col==2) {
							$mergecel[$index] = $colFiexedTotal;
							$grid[$index]['html'] = ($filer_labels) ? 'Filter(s): '.join(', ', $filer_labels) : null;
							$grid[$index]['cls'] = "filters";
						}
						elseif ($col >= $colDataGridFirst && $col <= $colDataGridLast) {
							
							$i = $col-$colDataGridFirst;
							$caption = $captionPlanningType[$i]['caption'];
							$class_name = ($captionPlanningType[$i]['caption']) ? 'col-caption cel' : 'col-separator';
							
							if ($caption) {
								$mergecel[$index] = $captionPlanningType[$i]['merge'];
								$grid[$index]['html'] = $caption;
							}
							
							$grid[$index]['cls'] = $class_name;
						}
						
						$grid[$index]['readonly']  = CELL_READONLY;
							
					break;
	
					// ITEM CODE
					case 3: 
						
						if ($col==1) {
							$mergecel[$index] = $fixed_columns;
						}
						elseif ($col >= $colDataGridFirst && $col <= $colDataGridLast) {
							
							$i = $col-$colDataGridFirst;
							
							$code = $colDataGrid[$i]['code'];
							$material = $colDataGrid[$i]['material'];
							$merge_length = $colDataGrid[$i]['merge'];
							
							if ($code) {
					
								$icon_help = "<span class=icon-holder ><span class=\"icon info-full-circle infotip \" rel=\"material-info\" data=\"$material\" ></span></span>";
							
								$grid[$index]['html'] = "$icon_help<span class=\"code\">$code</span>";
								$grid[$index]['cls'] = "cel item-caption";
									
								// merge item label
								if ($merge_length>1) {
									$mergecel[$index] = $merge_length;
								}
							}
							
							if (!$colDataGrid[$i]) {
								$grid[$index]['cls'] = 'col-separator';
								$separated_active = true;
							}
						}
						
						$grid[$index]['readonly']  = CELL_READONLY;
	
					break;
		
					// TOTAL QUANTITIES
					case $rowCalculatedFirst:
						
						$i = $row-$rowCalculatedFirst;
						$j = $col-$colDataGridFirst;
						
						if ($col==2) {
							$grid[$index]['html'] = $rowCalculated[$i];
							$grid[$index]['cls'] = "calcullated-caption";
						}
						elseif ($col >= $colDataGridFirst && $col <= $colDataGridLast) {
							
							if ($colDataGrid[$j]) {
								$key = $colDataGrid[$j]['key'];
								$grid[$index]['text']  = $colDataGrid[$j]['quantity'];
								$grid[$index]['cls'] = "cel $key";
							} else {
								$grid[$index]['cls'] = "col-separator";
							}
						}
						
						$grid[$index]['readonly']  = CELL_READONLY;
						
					break;
			
				
					// REFERENCES
					case $row >= $rowDataGridFirst && $row <= $rowDataGridLast :
						
						// icon row
						if ($col==1) {
							
							$i = $row-$rowDataGridFirst;
							$type = $rowDataGrid[$i]['type'];
							$pos = $rowDataGrid[$i]['pos'];
							$group = $rowDataGrid[$i]['group'];
							$caption = $rowDataGrid[$i]['caption'];
							
							// pos type caption
							if ($group) {
								$grid[$index]['html'] = "<span class=\"arrow arrow_$type arrow-down type_$type\"  type=\"$type\" row=\"$row\" ></span>";
								$grid[$index]['cls'] = "cel postype row-caption";
							} 
							// pos info bouble box
							else {
								$grid[$index]['html'] = "<span class=\"icon info-full-circle infotip \" rel=\"pos-info\" data=\"$pos\" ></span>";
								$grid[$index]['cls'] = "cel";
							}
						}
						// pos caption
						elseif ($col==2) {
							
							$i = $row-$rowDataGridFirst;
							$type = $rowDataGrid[$i]['type'];
							$pos = $rowDataGrid[$i]['pos'];
							$caption = $rowDataGrid[$i]['caption'];
							$group = $rowDataGrid[$i]['group'];
							$data = ($group) ? $rowDataGrid[$i]['group'] : $pos;
							
							// grid properties
							if ($group) {
								$grid[$index]['html'] = "<span class=\"caption type_$type\" type=\"$type\" row=\"$row\" >$caption</span>";
								$grid[$index]['cls'] = "cel postype row-caption";
							} else {
								$grid[$index]['html'] = "<span class=\"caption type_$type\" type=\"$type\" row=\"$row\" >$caption</span>";
								$grid[$index]['cls'] = "cel postype";
							}
						}
						// datagrid
						elseif ($col >= $colDataGridFirst && $col <= $colDataGridLast) {
							
							$i = $row - $rowDataGridFirst;
							$j = $col - $colDataGridFirst;
							$group = $rowDataGrid[$i]['group'];
							
							if ($colDataGrid[$j]) {
								
								$key = $colDataGrid[$j]['key'];
								$pos = $rowDataGrid[$i]['pos'];
								$item = $colDataGrid[$j]['id'];
								
								// class cell
								$class_cell = ($group) ? 'row-separator' : "cel $key";
								$class_cell_extended = null;
								
								if ($key==COLUMN_DISTRIBUTED) {
									
									$quantity = $distributedPosItems[$item][$pos];
									
								} elseif ($key==COLUMN_SAP_CONFIRMED) {
									
									$quantity = $sapPosConfirmedItems[$item][$pos];
									
									if ($sapPosConfirmedWarnings[$item][$pos]) {
										$class_cell_extended = 'warning';
										$grid[$index]['tag'] = $sapPosConfirmedWarnings[$item][$pos];
									}
								} elseif ($key==COLUMN_SAP_SHIPPED) {
									
									$quantity = $sapPosShippedItems[$item][$pos];
									
									if ($sapPosShippedWarnings[$item][$pos]) {
										$class_cell_extended = 'warning';
										$grid[$index]['tag'] = $sapPosShippedWarnings[$item][$pos];
									}
								}
								else $quantity = null;
													
								$grid[$index]['text'] = $quantity;
								$grid[$index]['hiddenAsNull'] = 'true';
								$grid[$index]['numeric'] = 'true';
							}
							else {
								
								$class_cell = ($group) ? 'row-separator col-separator' : 'col-separator';
								if ($group && $class_cell) $grid[$index]['html'] = '&nbsp;';
							}
							
							$grid[$index]['cls'] = "$class_cell $class_cell_extended";
						}
						
						$grid[$index]['readonly']  = CELL_READONLY;
	
					break;
	
					// WAREHOUSE GROUP CAPTION
					case $rowExtendedFirst:
						
						if ($col==1) {
							$grid[$index]['html'] = $rowExtended[0];
							$grid[$index]['cls'] = "cel row-caption";
							$mergecel[$index] = 2;
						}
						elseif ($col >= $colDataGridFirst && $col <= $colDataGridLast) {
							
							$i = $col - $colDataGridFirst;
							$material = $colDataGrid[$i]['id'];
							
							$row_separator = ($rowExtendedTotal > 1) ? 'row-separator' : null;
							$col_separator = ($row_separator) ? 'col-separator' : null; 
							
							$grid[$index]['cls'] = ($material) ? $row_separator : "$row_separator $col_separator";
							
							if (!$material) {
								$grid[$index]['html'] = '&nbsp;';
							}
						}
						
						$grid[$index]['readonly']  = CELL_READONLY;
						
					break;
					
					// WAREHOUSES
					case $row > $rowExtendedFirst && $row <= $rowExtendedLast && $rowExtendedTotal > 1 :
						
						$i = $row-$rowExtendedFirst;
						$j = $col-$colDataGridFirst;
						
						$warehouse = $rowExtended[$i]['warehouse'];
						
						if ($col==1) {
							$grid[$index]['html'] = "<span class=\"icon info-full-circle infotip \" rel=\"warehouse-info\" data=\"$warehouse\" ></span>";
							$grid[$index]['cls'] = "cel";
						}
						elseif ($col==2) {
							$grid[$index]['html'] = $rowExtended[$i]['name'];
							$grid[$index]['cls'] = "cel warehouse";
						}
						elseif ($col >= $colDataGridFirst && $col <= $colDataGridLast) {
							
							$class_cell_extended = null;
							
							if ($colDataGrid[$j]) {

								$key = $colDataGrid[$j]['key'];
								$item = $colDataGrid[$j]['id'];
								$warehouse = $rowExtended[$i]['id'];
								
								$class_cell = "cel $key";			
																														
								if ($key==COLUMN_DISTRIBUTED) {
									
									$quantity = $distributedWarehouseItems[$item][$warehouse];
									
								} elseif ($key==COLUMN_SAP_CONFIRMED) {
									
									$quantity = $sapWarehouseConfirmedItems[$item][$warehouse];
									
									if ($sapWarehouseConfirmedWarnings[$item][$warehouse]) {
										$class_cell_extended = 'warning';
										$grid[$index]['tag'] = $sapWarehouseConfirmedWarnings[$item][$warehouse];
									}
								} elseif ($key==COLUMN_SAP_SHIPPED) {
									
									$quantity = $sapWarehouseShippedItems[$item][$warehouse];
									
									if ($sapWarehouseShippedWarnings[$item][$warehouse]) {
										$class_cell_extended = 'warning';
										$grid[$index]['tag'] = $sapWarehouseShippedWarnings[$item][$warehouse];
									}
								}
								else $quantity = null;
								
								$grid[$index]['text'] = $quantity;
								$grid[$index]['hiddenAsNull'] = 'true';
								$grid[$index]['numeric'] = 'true';
							}
							else {
								$class_cell = "separator";
								$grid[$index]['html'] = "&nbsp;";
							}
							
							$grid[$index]['cls'] = "$class_cell $class_cell_extended";
						}
						
						$grid[$index]['readonly']  = CELL_READONLY;
						
					break;			

					default:
						$grid[$index]['readonly']  = CELL_READONLY;
						$grid[$index]['html'] = "&nbsp;";
					break;
				}
			}
		}	
	}

	
	echo json_encode(array(
		'data' => $grid,
		'merge' => $mergecel,
		'top' => $spreadsheet_fixed_rows,
		'left' => $spreadsheet_fixed_columns,
		'console' => $console
	));
