<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['mps_mastersheet_id'];
	
	$data = array();
	
	if ($_REQUEST['mps_mastersheet_year']) {
		$data['mps_mastersheet_year'] = $_REQUEST['mps_mastersheet_year'];
	}
	
	if ($_REQUEST['mps_mastersheet_name']) {
		$data['mps_mastersheet_name'] = $_REQUEST['mps_mastersheet_name'];
	}
	
	if ($data) {
	
		$mastersheet = new Mastersheet($application);
		$mastersheet->read($id);
	
		if ($mastersheet->id) {
			
			$data['user_modified'] = $user->login;
			
			$response = $mastersheet->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $mastersheet->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));
	