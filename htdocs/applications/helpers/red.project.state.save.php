<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$id = $_REQUEST['red_projectstate_id'];
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();


	$data = array();


	if ($_REQUEST['red_projectstate_name']) {
		$data['red_projectstate_name'] = $_REQUEST['red_projectstate_name'];
	}

	// red_projectstate_name
	if ($_REQUEST['red_projectstate_icon']) {
		$data['red_projectstate_icon'] = $_REQUEST['red_projectstate_icon'];
	}

	// submit request
	if ($data) {
		
		$projectState = new Red_Project_State($application);
		$projectState->read($id);
	
		if ($projectState->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $projectState->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response =  $id = $projectState->create($data);
			$message = ($response) ? message::request_inserted() : $translate->message_request_failure;
			$redirect = ($_REQUEST['id']) ? $_REQUEST['id']."/$id" : null;
		}
	}

	echo json_encode(array(
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message
	));
