<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$translate = Translate::instance();
	$application = $_REQUEST['application'];
	$country = $_REQUEST['country'];
		
		
	$data =array();
	$data[] = array('posaddress_id'=>0, 'posname'=>$translate->select);
	
	if ($_REQUEST['country']) {
		
		$model = new Model($application);

		$filter = null;
		
		$poslocations = $model->query("
				SELECT DISTINCT
					posaddress_id,
					concat(place_name, ': ', posaddress_name) as posname
				FROM posaddresses
				LEFT JOIN places on place_id = posaddress_place_id
				WHERE (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') 
				AND posaddress_country = '" . $_REQUEST['country'] . 
				"' ORDER BY place_name, posaddress_name 
			")->fetchAll();
		 
		
		if ($poslocations) {
			foreach ($poslocations as $row) {
				$data[] = array('posaddress_id'=>$row['posaddress_id'], 'posname'=>$row['posname']);
			}
		}
	}

	// response with json header
	header('Content-Type: text/json');
	echo json_encode($data);
	