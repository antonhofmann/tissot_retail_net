<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", false);
	
	$model = new Model(Connector::DB_CORE);

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'role_code, role_name';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// sql pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$offset = ($page-1) * $settings->limit_pager_rows;
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "( 
			role_code LIKE \"%$keyword%\" 
			OR role_name LIKE \"%$keyword%\" 
		)";
	}
	
	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS
			role_id,
			role_code,
			role_name
		FROM db_retailnet.roles
	")
	->filter($filters)
	->order($sort, $direction)
	->fetchAll();
	
	if ($result) {
	
		$count = 0;
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		
		foreach ($datagrid as $key => $row) {
			
			$result = $model->query("
				SELECT application_role_id
				FROM db_retailnet.application_roles
				WHERE application_role_role = $key AND application_role_application = $id	
			")->fetchAll();
			
			$dataloader['role'][$key] = ($result) ? 1 : 0;
			$datagrid[$key]['role'] = $key;
			
			if ($result) $count++;
		}
		
		$checkall = (count($datagrid)==$count) ? 'checked=checked' : null;
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";

	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->caption('role', "<input type=checkbox class=checkall $checkall />");
	
	$table->datagrid = $datagrid;
	$table->dataloader($dataloader);
	
	$table->role_code(
		Table::ATTRIBUTE_NOWRAP,
		'width=20%'
	);
	
	$table->role_name(
		Table::ATTRIBUTE_NOWRAP
	);
	
	$table->role(
		Table::DATA_TYPE_CHECKBOX,
		'width=2%'
	);
	
	$table->footer($translate->total_rows.": $totalrows");
	$table = $table->render();
	
	echo $toolbox.$table;
	