<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$model = new Model($application);

$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

// sql order
$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'product_line_priority, product_line_name';

$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// sql pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rows = $settings->limit_pager_rows;
$offset = ($page-1) * $rows;

// filter: full text search
if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {

	$keyword = $_REQUEST['search'];

	$filters['search'] = "(
		product_line_name LIKE \"%$keyword%\"
	)";
}

// filter active
$active = isset($_REQUEST['active']) ? $_REQUEST['active'] : 1;
$filters['active'] = "product_line_active = $active";

$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT 
		product_line_id, 
		product_line_name, 
		product_line_priority, 
		IF (product_line_visible, 'yes', 'no') as product_line_visible, 
		IF (product_line_budget, 'yes', 'no') as product_line_budget, 
		IF (product_line_clients, 'yes', 'no') as product_line_clients, 
		IF (product_line_posindex, 'yes', 'no') as product_line_posindex, 
		IF (product_line_mis, 'yes', 'no') as product_line_mis,
		IF(product_line_active, '<img src=/public/images/icon-checked.png >', ' ') AS product_line_active,
		CONCAT('<i data-id=\'', product_line_id ,'\' class=\'row-order fa fa-bars\' ></i>') as sorter
	FROM product_lines	
")
->filter($filters)
->order($order, $direction)
->fetchAll();

if ($result) {
	
	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);
	
	$pager = new Pager(array(
		'page' => $page,
		'totalrows' => $totalrows,
		'buttons' => true
	));
	
	$list_index = "Total rows $totalrows";
	//$list_controlls = $pager->controlls();
}


// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";

// toolbox: add
if ($_REQUEST['add']) {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();

// active product lines
$actives[1] = "Active Product Lines";
$actives[0] = "Inactive Product Lines";

$toolbox[] = ui::dropdown($actives, array(
	'name' => 'active',
	'id' => 'active',
	'class' => 'submit',
	'value' => $active,
	'caption' => false
));

// toolbox: form
if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

$table = new Table(array(
	'sort' => array('column'=>$order,'direction'=>$direction)
));

$table->datagrid = $datagrid;

$table->sorter(
	Table::ATTRIBUTE_NOWRAP,
	'width=20px'
);

$table->product_line_name(
	Table::ATTRIBUTE_NOWRAP,
	"href=".$_REQUEST['data']
);

$table->product_line_visible(
	Table::ATTRIBUTE_NOWRAP,
	'width=5%'
);

$table->product_line_budget(
	Table::ATTRIBUTE_NOWRAP,
	'width=5%'
);

$table->product_line_clients(
	Table::ATTRIBUTE_NOWRAP,
	'width=5%'
);

$table->product_line_posindex(
	Table::ATTRIBUTE_NOWRAP,
	'width=5%'
);

$table->product_line_mis(
	Table::ATTRIBUTE_NOWRAP,
	'width=5%'
);

$table->product_line_active(
	Table::ATTRIBUTE_NOWRAP,
	'width=5%',
	"align=center"
);

$table->caption('product_line_visible', 'in Catalog');
$table->caption('product_line_budget', 'in Projects');
$table->caption('product_line_clients', 'to Clients');
$table->caption('product_line_posindex', 'in POS');
$table->caption('product_line_mis', 'in MIS');

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;


