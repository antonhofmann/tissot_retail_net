<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$role = $_REQUEST['role'];
	$permission = $_REQUEST['permission'];
	
	if ($role && $permission) {
		
		$model = new Model(Connector::DB_CORE);
		
		if ($_REQUEST['action']) {
			
			$sth = $model->db->query("
				INSERT INTO role_permissions (
					role_permission_role, 
					role_permission_permission
				)
				VALUES($role, $permission)
			");
			
			$action = ($sth) ? true : false;
			$message = ($action) ? $translate->message_request_inserted : $translate->message_request_failure;
		}
		else {
			
			$sth = $model->db->query("
				DELETE FROM role_permissions
				WHERE role_permission_role = $role AND  role_permission_permission = $permission
			");
			
			$action = ($sth) ? true : false;
			$message = ($action) ? $translate->message_request_deleted : $translate->message_request_failure;
		}

		if ($action) {

			$permissions = Permission::loader();
			$array = array();
			
			if ($permissions) {
				foreach ($permissions as $key => $permission) {
					$array[] = $permission;
				}
				session::set('permissions', join(' ', $array));
			}
			
			// user pemissions
			$permissions = Permission::loader(null,true);
			$array = array();
			
			if ($permissions) {
				foreach ($permissions as $key => $permission) {
					$array[] = $permission;
				}
				session::set('user_permissions', join(' ', $array));
			}
		}
		
		$response = ($action) ? true : false;
		$message = ($action) ? $message : $translate->message_request_failure;
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));
	