<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	$period = $_REQUEST['periodes'];
	$region = $_REQUEST['regions'];
	$state = $_REQUEST['states'];
	$addresstype = $_REQUEST['addresstypes'];
	$legaltype = $_REQUEST['legaltypes'];
	$client = $_REQUEST['clients'];
	
	// db model
	$model = new Model($application);
	
	
/**************************************************************** actual numbers ******************************************************************/
	
	$filter = null;
	
	// filter: region
	if ($region) {
		$filter = " AND country_region = $region";
	}
	
	// filter: address type
	if ($addresstype) {
		$filter .= " AND clients.address_client_type = $addresstype AND ( clients.address_involved_in_planning = 1 OR clients.address_involved_in_planning_ff = 1 )";
	}
	
	$result = $model->query("
		SELECT DISTINCT
			posverification_data_id,
			posverification_data_distribution_channel_id,
			posverification_data_actual_number,
			posverification_address_id,
			addresses.address_id,
			addresses.address_parent,
			posverification_confirm_date
		FROM posverification_data	
		INNER JOIN posverifications ON posverification_id = posverification_data_posverification_id
		INNER JOIN db_retailnet.addresses ON address_id = posverification_address_id
		LEFT JOIN db_retailnet.posaddresses ON posaddress_client_id = posverification_address_id
		LEFT JOIN db_retailnet.addresses AS clients ON clients.address_id = posaddress_client_id
		LEFT JOIN db_retailnet.countries on country_id = addresses.address_country
		WHERE UNIX_TIMESTAMP(posverification_date) = '$period' $filter
	")->fetchAll();
	

	if ($result) {
		
		foreach ($result as $row) {
			
			$key = $row['address_id'].$row['address_parent'];
			$channel = $row['posverification_data_distribution_channel_id'];
			$permitted = true;
			
			if ($state) {
				
				$permitted = false;
				
				// confirmed actual numbers
				if ($state==1 && $row['posverification_confirm_date']) {
					$permitted = true;
				}
				
				// unconfirmed actual numbers
				if ($state==2 && !$row['posverification_confirm_date']) {
					$permitted = true;
				}
			}
			
			if ($permitted) {
				
				// sum actual number over all companies
				$actualNumber[$channel] = $actualNumber[$channel] + $row['posverification_data_actual_number'];
				
				// sum actual number for each client
				$actualNumberCompany[$channel][$key] = $actualNumberCompany[$channel][$key] + $row['posverification_data_actual_number'];
			}
			
			// confirmed verification
			if ($row['posverification_confirm_date']) {
				$verifications_confirmed[$key] = true;
			}
		}
	}

	
/****************************************** distribution channels ****************************************************/
	
	// filter: address type
	$address_type_filter = "";
	$region_filter = "";
	$extended_filter = null;
	
	
	if ($region) {
		$region_filter = " AND country_region = $region ";
	}
	
	// filter: client type
	if ($addresstype) {
		$address_type_filter = " AND addresses.address_client_type = $addresstype ";
	} 
	else {
		
		$address_type_filter = " AND addresses.address_client_type IN (1, 2, 3) ";
		
		$extended_filter = "
		OR(
			parentaddresses.address_active = 1 
			AND parentaddresses.address_type = 1
			AND addresses.address_client_type = 3
			AND (
				posaddress_store_closingdate IS NULL
				OR posaddress_store_closingdate = '0000-00-00'
				OR posaddress_store_closingdate = ''
			)
			$region_filter
		)		
		";
	}
	
	$result = $model->query("
		SELECT DISTINCT
			posaddress_id,
			addresses.address_id,
			addresses.address_parent,
			posaddress_client_id,
			posaddress_distribution_channel
		FROM db_retailnet.posaddresses
		LEFT JOIN db_retailnet.addresses as addresses ON addresses.address_id = posaddress_client_id
		LEFT JOIN db_retailnet.addresses as parentaddresses ON parentaddresses.address_id = posaddress_franchisee_id
		LEFT JOIN db_retailnet.countries on country_id = addresses.address_country
		WHERE (
			addresses.address_active = 1 
			AND addresses.address_type = 1
			AND (
				addresses.address_involved_in_planning = 1 
				OR addresses.address_involved_in_planning_ff = 1
				
			)
			AND (
				posaddress_store_closingdate IS NULL
				OR posaddress_store_closingdate = '0000-00-00'
				OR posaddress_store_closingdate = ''
			)
			$address_type_filter
			$region_filter
		)
		$extended_filter	
	")->fetchAll();
	
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$key = $row['address_id'].$row['address_parent'];
			$channel = $row['posaddress_distribution_channel'];
			$permitted = true;
			
			if ($state) {
			
				$permitted = false;
			
				// confirmed actual numbers
				if ($state==1) {
					$permitted = ($verifications_confirmed[$key]) ? true : false;
				}
			
				// unconfirmed actual numbers
				if ($state==2) {
					$permitted = ($verifications_confirmed[$key]) ? false : true;
				}
			}
			
			if ($permitted) {
				
				if ($channel) {
					
					// count distribution channels over all companies
					$inPosIndex[$channel] = $inPosIndex[$channel] + 1;
					
					// count channels for each client
					$companyDistributionChannels[$channel][$key] = $companyDistributionChannels[$channel][$key] + 1;
					
				} 
				// pos without distributtion channels
				else {
					$grandCompaniesWithoutChannels = $grandCompaniesWithoutChannels + 1;
					$companiesWhithoutChannels[$key] = $companiesWhithoutChannels[$key] + 1;
				}
			}
		}
	}
	
	
/**************************************************************** clients **********************************************************************/
	
	// filter: address type
	$filter = $addresstype
		? " AND address_client_type = $addresstype"
		: " AND address_client_type IN (1, 2, 3)";
	
	// filter: region
	if ($region) {
		$filter .= " AND country_region = $region";
	}

	if ($client) {
		$filter .= " AND address_id = $client";
	}
	

	$result = $model->query("
		SELECT DISTINCT
			address_id,
			address_parent,
			address_company,
			country_name
		FROM db_retailnet.addresses
		LEFT JOIN db_retailnet.countries on country_id = address_country
		WHERE 
			address_active = 1
		AND address_type = 1
		AND (
			address_involved_in_planning = 1 
			OR address_involved_in_planning_ff = 1
		)
		$filter
		ORDER BY country_name
	")->fetchAll();

	if ($result) {
		
		$companies = array();
		
		foreach ($result as $row) {
			
			$key = $row['address_id'].$row['address_parent'];
			$permitted = true;
			
			// confirmed clients
			if ($state==1) {
				$permitted = ($verifications_confirmed[$key]) ? true : false;
			}
			
			// unconfirmed clients
			if ($state==2) {
				$permitted = ($verifications_confirmed[$key]) ? false : true;
			}
			
			if ($permitted) { 
				$companies[$key]['country_name'] = $row['country_name'];
				$companies[$key]['address_company'] = $row['address_company'];
			}
		}
	}
	
/*********************************************************** distribution channels *************************************************************/

	$result = $model->query("
		SELECT DISTINCT
			mps_distchannel_id,
			mps_distchannel_group_name,
			mps_distchannel_code,
			mps_distchannel_name
		FROM db_retailnet.mps_distchannels
		INNER JOIN db_retailnet.mps_distchannel_groups ON mps_distchannels.mps_distchannel_group_id = mps_distchannel_groups.mps_distchannel_group_id	
		WHERE mps_distchannel_active = 1 
		ORDER BY mps_distchannel_group_name, mps_distchannel_name
	")->fetchAll();
	
	if ($result) {
		
		$distributionChannels = array();
		
		foreach ($result as $row) {
			
			$key = strtolower(str_replace(' ', '', $row['mps_distchannel_group_name']));
			
			// group name
			$distributionChannels[$key]['group'] = $row['mps_distchannel_group_name'];
			
			// distribution channel
			$distributionChannels[$key]['channels'][$row['mps_distchannel_id']] = array(
				'mps_distchannel_name' => $row['mps_distchannel_name'],
				'mps_distchannel_code' => $row['mps_distchannel_code']
			);
		}
	}
	
/****************************************************** spreadsheet data *******************************************************/
	
	$datagrid = array();
	
	// rows: first blank row, countries, company captions
	$fixed_rows = 2;
	$first_fixed_row = 1;
	$last_fixed_row = 3;
	$fixed_rows_separator = 1;
	
	// columns: channel captions, separator
	$fixed_columns = 5;
	$first_fixed_column = 4;
	$last_fixed_column = 2;
	$fixed_column_separator = 1;
	
	// totals
	$total_fixed_rows = $fixed_rows + $fixed_rows_separator;
	$total_fixed_columns = $fixed_columns + $fixed_column_separator;

	$countryColumns = 3;
	
	// column: over all countries
	$datagrid[2][3] = array(
		'text' => "Over all Countries",
		'colspan' => 3,
		'cls' => 'cel country all-countries'
	);
	
	
	// column: actual number (over all countries)
	$datagrid[3][6] = array(
		'cls' => 'sep'
	);

/***************************************************** company captions *******************************************************/
	
	if ($companies) {
		
		$j = $total_fixed_columns+1;
		
		foreach ($companies as $key => $row) {
			
			$confirmed = ($verifications_confirmed[$key]) ? 'confirmed' : null;
			
			// row countries
			$datagrid[2][$j] = array(
				'text' => $row['address_company'],
				'cls' => "cel country $confirmed",
				'colspan' => $countryColumns
			);

			$datagrid[1][$j+$countryColumns] = array(
				'cls' => 'sep'
			);
			
			$datagrid[2][$j+3] = array(
				'cls' => 'sep'
			);

			$j = $j+$countryColumns+1;
		}
	}	
	
	
/******************************************************** datagrid ************************************************************/
	

	if ($distributionChannels) {
		
		$grandTotal = array();
		
		$i = $total_fixed_rows + 1;
		$first_grid_area = 1;
		
		foreach ($distributionChannels as $group => $row) {
			
			if ($row['group']) {
				
				$datagrid[$i][1] = array(
					'id' => $group,
					'text' => $row['group'],
					'cls' => 'cel group'
				);
				
				// column: distribution code
				$datagrid[$i][2] = array(
					'text' => translate::instance()->code,
					'cls' => 'cel group'
				);
				
				// column: in pos index (over all countries)
				$datagrid[$i][3] = array(
					'text' => translate::instance()->in_pos_index,
					'cls' => 'cel group text-center'
				);
				
				// column: actual number (over all countries)
				$datagrid[$i][4] = array(
					'text' => translate::instance()->posverification_data_actual_number,
					'cls' => 'cel group text-center'
				);
				
				// column: delta (over all countries)
				$datagrid[$i][5] = array(
					'text' => translate::instance()->delta,
					'cls' => 'cel group text-center'
				);
				
				if ($companies) {
				
					$j = $total_fixed_columns+1;
				
					foreach ($companies as $company => $data) {
							
						// total company distribution channels
						$datagrid[$i][$j] = array(
							'text' => $data['country_name'],
							'cls' => 'cel group country',
							'colspan' => $countryColumns
						);
							
						$j = $j+$countryColumns+1; // country group separator
					}
				}
				
				$i++;
			}
			
			if ($row['channels']) {
				
				$group_first_row = $i;
				$group_last_row = $i + count($row['channels']) - 1;
				
				foreach ($row['channels'] as $channel => $subrow) {
					
					$datagrid[$i][1] = array(
						'id' => $channel,
						'text' => $subrow['mps_distchannel_name'],
						'cls' => 'cel channel'
					);
					
					$datagrid[$i][2] = array(
						'text' => $subrow['mps_distchannel_code'],
						'cls' => 'cel'
					);
					
					$datagrid[$i][3] = array(
						'text' => $inPosIndex[$channel],
						'cls' => 'cel'
					);
					
					$datagrid[$i][4] = array(
						'text' => $actualNumber[$channel],
						'cls' => 'cel'
					);
					
					$datagrid[$i][5] = array(
						'text' => $actualNumber[$channel]-$inPosIndex[$channel],
						'cls' => "function(value) { return value==0 ? 'cel' : 'cel alert' }"
					);
							
					if ($companies) {
						
						$j = $total_fixed_columns+1;
						
						foreach ($companies as $key => $data) {
							
							// total company distribution channels
							$datagrid[$i][$j] = array(
								'text' => $companyDistributionChannels[$channel][$key],
								'cls' => 'cel'
							);
							
							$j = $j+1;
							
							// total company actual numbers
							$datagrid[$i][$j] = array(
								'text' => $actualNumberCompany[$channel][$key],
								'cls' => 'cel'
							);
							
							$j = $j+1;
							
							// total company actual numbers
							$datagrid[$i][$j] = array(
								'text' => $actualNumberCompany[$channel][$key] - $companyDistributionChannels[$channel][$key],
								'cls' => "function(value) { return value==0 ? 'cel' : 'cel alert' }"
							);
							
							$j = $j+2; // country group separator
						}
					}
					
					$i++;
				}

				// total group caption
				$datagrid[$i][1] = array(
					'text' => "Total ".$row['group'],
					'cls' => "cel total-group",
					'colspan' => 2
				);
				
				
				// grand total: in pos index
				$grandTotal[3][] = spreadsheet::key(3,$i);
				
				// group range: in pos index over all countries
				$range = spreadsheet::key(3,$group_first_row).'_'.spreadsheet::key(3,$group_last_row);
				
				// total group in pos index
				$datagrid[$i][3] = array(
					'text' => "function($range) { return sum($range) || ''; }",
					'cls' => 'cel total-group'
				);
				
				
				// grand total: actual number
				$grandTotal[4][] = spreadsheet::key(4,$i);
				
				// group range: actual number over all countries
				$range = spreadsheet::key(4,$group_first_row).'_'.spreadsheet::key(4,$group_last_row);
				
				// total group in pos index
				$datagrid[$i][4] = array(
					'text' => "function($range) { return sum($range) || ''; }",
					'cls' => 'cel total-group'
				);
				
				
				// grand total: actual number
				$grandTotal[5][] = spreadsheet::key(5,$i);
				
				// group range: actual number over all countries
				$range = spreadsheet::key(5,$group_first_row).'_'.spreadsheet::key(5,$group_last_row);
				
				// total group in pos index
				$datagrid[$i][5] = array(
					'text' => "function($range) { return sum($range) || ''; }",
					'cls' => 'function(value) { return value==0 ? "cel total-group" : "cel total-group alert" }'
				);
				
				
				// totals
				if ($companies) {
				
					$j = $total_fixed_columns+1;
				
					foreach ($companies as $key => $data) {
							
						$range = spreadsheet::key($j,$group_first_row).'_'.spreadsheet::key($j,$group_last_row);
						
						// total group in pos index for each client
						$datagrid[$i][$j] = array(
							'text' => "function($range) { return sum($range) || ''; }",
							'cls' => 'cel total-group'
						);
						
						// grand total: client pos index
						$grandTotal[$j][] = spreadsheet::key($j,$i);
							
						
						$j = $j+1;
						$range = spreadsheet::key($j,$group_first_row).'_'.spreadsheet::key($j,$group_last_row);
							
						// total group actual numbers for each client
						$datagrid[$i][$j] = array(
							'text' => "function($range) { return sum($range) || ''; }",
							'cls' => 'cel total-group'
						);
						
						// grand total: client actual number
						$grandTotal[$j][] = spreadsheet::key($j,$i);
						
							
						
						$j = $j+1;
						$range = spreadsheet::key($j,$group_first_row).'_'.spreadsheet::key($j,$group_last_row);
							
						// total group delta for each client
						$datagrid[$i][$j] = array(
							'text' => "function($range) { return sum($range) || ''; }",
							'cls' => "function(value) { return value==0 ? 'cel total-group' : 'cel total-group alert' }"
						);
						
						// grand total: client actual number
						$grandTotal[$j][] = spreadsheet::key($j,$i);
						
							
						$j = $j+2; // country group separator
					}
				}
				
				$i++;
			}
			
			$i++; // channel group separator
		}
	}
	
/******************************************************** calcullations ***************************************************************/
	
	if ($grandTotal) {
		
		// pos whithout distribution channels
		// total group caption
		$datagrid[$i][1] = array(
			'text' => "POS Locations whithout distribution channels",
			'cls' => "cel grand-total",
			'colspan' => 2
		);
		
		// total group in pos index
		$grandTotal[3][] = spreadsheet::key(3,$i);
		$datagrid[$i][3] = array(
			'text' => $grandCompaniesWithoutChannels,
			'cls' => 'cel grand-total'
		);
		
		if ($companies) {
		
			$j = $total_fixed_columns+1;
		
			foreach ($companies as $key => $data) {
				
				// grand total: client distribution channels
				$datagrid[$i][$j] = array(
					'text' => $companiesWhithoutChannels[$key],
					'cls' => 'cel grand-total'
				);
				
				$grandTotal[$j][] = spreadsheet::key($j,$i);
				
				$j = $j+$countryColumns+1;
			}
		}
		
		
		
		
		$i++; // grand total row
		
		// total group caption
		$datagrid[$i][1] = array(
			'text' => "Grand Total of POS Locations",
			'cls' => "cel grand-total",
			'colspan' => 2
		);
	
		// grand total: in pos index
		if ($grandTotal[3]) {
		
			$arg = join(',',$grandTotal[3]);
			$sum = join('+',$grandTotal[3]);
		
			// total group in pos index
			$datagrid[$i][3] = array(
				'text' => "function($arg) { return $sum; }",
				'cls' => 'cel grand-total'
			);
		}
		
		// grand total: actual number
		if ($grandTotal[4]) {
			
			$arg = join(',',$grandTotal[4]);
			$sum = join('+',$grandTotal[4]);
			
			$datagrid[$i][4] = array(
				'text' => "function($arg) { return $sum; }",
				'cls' => 'cel grand-total'
			);
		}
		
		// grand total: delta
		if ($grandTotal[5]) {
			
			$arg = join(',',$grandTotal[5]);
			$sum = join('+',$grandTotal[5]);
			
			$datagrid[$i][5] = array(
				'text' => "function($arg) { return $sum; }",
				'cls' => 'function(value) { return value==0 ? "cel grand-total" : "cel grand-total alert" }'
			);
		}
		
		if ($companies) {
		
			$j = $total_fixed_columns+1;
		
			foreach ($companies as $key => $data) {
		
				// grand total: client distribution channels
				if ($grandTotal[$j]) {
	
					$arg = join(',',$grandTotal[$j]);
					$sum = join('+',$grandTotal[$j]);
						
					$datagrid[$i][$j] = array(
						'text' => "function($arg) { return $sum || ''; }",
						'cls' => 'cel grand-total'
					);
				}
					
				$j = $j+1;
					
				// grand total: client actual number
				if ($grandTotal[$j]) {
	
					$arg = join(',',$grandTotal[$j]);
					$sum = join('+',$grandTotal[$j]);
						
					$datagrid[$i][$j] = array(
						'text' => "function($arg) { return $sum || ''; }",
						'cls' => 'cel grand-total'
					);
				}
					
				$j = $j+1;
					
				// grand total: delta
				if ($grandTotal[$j]) {
	
					$arg = join(',',$grandTotal[$j]);
					$sum = join('+',$grandTotal[$j]);
						
					$datagrid[$i][$j] = array(
						'text' => "function($arg) { return $sum || ''; }",
						'cls' => "function(value) { return value==0 ? 'cel grand-total' : 'cel grand-total alert' }"
					);
				}
					
				$j = $j+2;
			}
		}
	}
	
	
/******************************************************** responding ***************************************************************/
	
	if ($datagrid) {
		
		foreach ($datagrid as $i => $row) {
			foreach ($row as $j => $cel) {
				
				$index = spreadsheet::key($j,$i);
				
				$cel['readonly'] = 'true';
				
				// cell attribute tag
				if ($cel['colspan']) {
					$colspans[$index] = $cel['colspan'];
					unset($cel['colspan']);
				}
				
				$grid[$index] = $cel;
			}
		}
	}

	header('Content-Type: text/json');
	echo json_encode(array(
		'data' => $grid,
		'merge' => $colspans,
		'top' => $total_fixed_rows,
		'left' => $total_fixed_columns
	));
	