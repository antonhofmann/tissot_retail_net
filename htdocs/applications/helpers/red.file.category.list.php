<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	
	$model = new Model($application);

	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "red_filecategory_name";
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;

	// filter: search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			red_filecategory_name LIKE \"%$keyword%\"
		)";
	}

	// file categories
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS
			red_filecategory_id,
			red_filecategory_name
		FROM red_filecategories
	")
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();

	if ($result) {
		
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));

		$pageIndex = $pager->index();
		$pageControlls = $pager->controlls();
	}

	// hidden fields
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

	// toolbox: button add
	if ($_REQUEST['add']) {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'label' => $translate->add_new,
			'href' => 	$_REQUEST['add']	
		));
	}
	
	// toolbox: search full text
	$toolbox[] = ui::searchbox();

	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}

	$table = new Table(array(
		'sort' => array(
			'column' => $sort,
			'direction' => $direction
		)
	));

	$table->datagrid = $datagrid;

	$table->red_filecategory_name(
		'width=100%',
		'href='.$_REQUEST['form'],
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->footer($pageIndex);
	$table->footer($pageControlls);
	$table = $table->render();

	echo $toolbox.$table;
