<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	// product line
	$id = $_REQUEST['category_id'];
	$productLine = $_REQUEST['category_product_line'];
	
	$data = array();
	
	if (in_array('category_name', $fields)) {
		$data['category_name'] = $_REQUEST['category_name'];
	}
	
	if (in_array('category_cost_unit_number', $fields)) {
		$data['category_cost_unit_number'] = $_REQUEST['category_cost_unit_number'];
	}
	
	if (in_array('category_budget', $fields)) {
		$data['category_budget'] = ($_REQUEST['category_budget']) ? 1 : 0;
	}
	
	if (in_array('category_catalog', $fields)) {
		$data['category_catalog'] = ($_REQUEST['category_catalog']) ? 1 : 0;
	}
		
	if (in_array('category_useconfigurator', $fields)) {
		$data['category_useconfigurator'] = ($_REQUEST['category_useconfigurator']) ? 1 : 0;
	}
		
	if (in_array('category_not_in_use', $fields)) {
		$data['category_not_in_use'] = ($_REQUEST['category_not_in_use']) ? 1 : 0;
	}
	
	if ($data && $productLine) {

		$data['category_product_line'] = $productLine;

		$category = new Category();
		$category->read($id);
	
		if ($category->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $category->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $category->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	