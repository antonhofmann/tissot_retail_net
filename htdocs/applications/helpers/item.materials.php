<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['item_id'];

$model = new Model(Connector::DB_CORE);

$item = new Item();
$item->read($id);

$response = true;

// item catalog materials ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$itemCatalogMaterials = array();

if ($item->id && in_array('materials', $fields)) {

	$sth = $model->db->prepare("
		DELETE FROM item_catalog_materials 
		WHERE item_catalog_material_item_id = ?
	");

	$sth->execute(array($item->id));

	$sth = $model->db->prepare("
		INSERT INTO item_catalog_materials (
			item_catalog_material_item_id,
			item_catalog_material_material_id,
			user_created
		)
		VALUES (?,?,?)
	");

	if ($_REQUEST['materials']) {
		
		foreach ($_REQUEST['materials'] as $i => $value) {
			$response = $sth->execute(array($item->id, $value, $user->login));
			if ($response) $itemCatalogMaterials[] = $value;
			else $errors[] = $sth->errorInfo();
		}
	}
}

// item material list ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($item->id && in_array('item_materials', $fields)) {

	$sth = $model->db->prepare("
		DELETE FROM item_materials 
		WHERE item_material_item_id = ?
	");

	$sth->execute(array($item->id));

	$sth = $model->db->prepare("
		INSERT INTO item_materials (
			item_material_item_id,
			item_material_name,
			user_created
		)
		VALUES (?,?,?)
	");

	if ($_REQUEST['item_material_name']) {
		
		foreach ($_REQUEST['item_material_name'] as $i => $value) {

			// null value fields
			if (!$value) {
				$removeFields['#itemMaterialList'][] = $i;
				continue;
			}

			$sth->execute(array($item->id, $value, $user->login));
		}
	}
}

// update certificate items ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$supplierCertificates = array();
$existingCertificates = array();

if ($itemCatalogMaterials) {

	$involvedMaterials = join(',', $itemCatalogMaterials);
	
	// get item suppliers
	$sth = $model->db->prepare("
		SELECT address_id
		FROM suppliers 
		INNER JOIN addresses ON supplier_address = address_id
		WHERE supplier_item = ?
	");

	$sth->execute(array($item->id));
	$result = $sth->fetchAll();

	if ($result) {

		// get supplier certificates
		$sth_e = $model->db->prepare("
			SELECT certificate_id
			FROM certificates
			WHERE certificate_address_id = ?
		");

		
		$sth = $model->db->prepare("
			SELECT DISTINCT certificate_id
			FROM certificates
			INNER JOIN certificate_materials ON certificate_material_certificate_id = certificate_id
			WHERE certificate_address_id = ? AND certificate_material_catalog_material_id IN ($involvedMaterials)
		");

		foreach ($result as $row) {
			
			$sth_e->execute(array($row['address_id']));
			$existingCertificates = $sth_e->fetchAll();

			$sth->execute(array($row['address_id']));
			$result = $sth->fetchAll();



			if ($result) {
				foreach ($result as $row) {
					$certificate = $row['certificate_id'];
					$supplierCertificates[$certificate] = $row['items'] ? explode(',', $row['items']) : array();
				}
			}
		}
	}
}
//delete all existing certificates
if(count($existingCertificates) > 0)
{
	$sth = $model->db->prepare("
		DELETE from item_certificates where item_certificate_certificate_id = ? and item_certificate_item_id = ?
	");

	foreach($existingCertificates as $key=>$existingCertificate)
	{
		$sth->execute(array($existingCertificate["certificate_id"], $item->id));
	}
}

if ($supplierCertificates) {
	
	$sth = $model->db->prepare("
		INSERT INTO item_certificates (
			item_certificate_certificate_id,
			item_certificate_item_id,
			user_created
		) VALUES (?, ?, ?)
	");

	foreach ($supplierCertificates as $certificate => $items) {
		if (!in_array($item->id, $items)) {
			$sth->execute(array($certificate, $item->id, $user->login));
		}
	}

}

// response ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


if ($response) {
	$message = $translate->message_request_submitted;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'errors' => $errors,
	'fields' => $updateFields,
	'remove' => $removeFields,
	'supplierCertificates' => $supplierCertificates
));
	