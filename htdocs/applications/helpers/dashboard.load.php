<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	
	$dashboard = $_REQUEST['dashboard'] ?: Session::get('dashboard-active');
	$active = $dashboard ?: 1;
	
	Session::remove('dashboard-active');
	
	$model = new Model(Connector::DB_CORE);
	

/************************************************************* dashboards ************************************************************/
	
	// standard dashboards
	$standardDashboards = array();
	
	$result = $model->query("
		SELECT * 
		FROM ui_dashboards
	")->fetchAll();
	
	if ($result) {
		foreach ($result as $row) {
			$key = $row['ui_dashboard_id'];
			$standardDashboards[$key]['title'] = $row['ui_dashboard_title'];
		}
	}
	
	
	
	// user dashboards
	$dashboards = array();
	
	
	$result = $model->query("
		SELECT *
		FROM ui_user_dashboards
		WHERE ui_user_dashboard_user_id = $user->id		
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$key = $row['ui_user_dashboard_id'];
			$dash = $row['ui_user_dashboard_dashboard_id'];
			
			$dashboards[$key]['title'] = $row['ui_user_dashboard_title'];
			
			if ($standardDashboards[$dash]) {
				$dashboards[$key]['standard'] = 1;
			}
		}
		
		// dashboard tabs
		echo "<ul class=\"nav nav-tabs\" id=\"dashtabs\" >";
		
		foreach ($dashboards as $key => $row) {
				
			$title = $row['title'];
				
			$selected = $active==$key ? 'class=active' : null;
				
			$settings = $row['standard'] ? null : " <i class='fa fa-cog dashboard-settings' data-id='$key' ></i>";
				
			echo "<li id='dashtab-$key' $selected><a href=\"#dash$key\" data-toggle=\"tab\"><b>$title</b>$settings</a></li>";
		}
		
		echo "</ul>";
	}
	
	
/************************************************************* widgets ************************************************************/
	
	$widgets = array();
	
	$result = $model->query("
		SELECT DISTINCT
			ui_widget_id, 
			ui_user_widget_dashboard_id,
			ui_widget_title, 
			ui_widget_resizable, 
			ui_widget_draggable, 
			ui_user_widget_cord_y, 
			ui_user_widget_cord_x, 
			ui_user_widget_width, 
			ui_user_widget_height, 
			ui_widget_theme_name
		FROM ui_user_widgets
			INNER JOIN ui_widgets ON ui_widget_id = ui_user_widget_widget_id
			INNER JOIN ui_widget_themes ON ui_widget_themes.ui_widget_theme_id = ui_user_widget_theme_id
		WHERE	
			ui_user_widget_user_id = $user->id
	")->fetchAll();
	
	if ($dashboards && $result) {
	
		foreach ($result as $row) {
			
			$widget = $row['ui_widget_id'];
			$key = $row['ui_user_widget_dashboard_id'];
			
			$widgets[$key][$widget]['title'] = $row['ui_widget_title'];
			$widgets[$key][$widget]['data-resizable'] = $row['ui_widget_resizable'];
			$widgets[$key][$widget]['data-draggable'] = $row['ui_widget_draggable'];
			$widgets[$key][$widget]['row'] = $row['ui_user_widget_cord_y'];
			$widgets[$key][$widget]['col'] = $row['ui_user_widget_cord_x'];
			$widgets[$key][$widget]['size_x'] = ceil($row['ui_user_widget_width'] / 50);
			$widgets[$key][$widget]['size_y'] = ceil($row['ui_user_widget_height'] / 50);
			$widgets[$key][$widget]['theme'] = $row['ui_widget_theme_name'];
		}
		
		// dashboard widgets
		echo "<div class=\"tab-content\" id=\"dashtab-contents\">";
		
		foreach ($dashboards as $dashboard => $data) {
			
			$selected = $active==$dashboard ? 'active' : null;
			
			echo "<div class='gridster tab-pane fade in $selected' id='dash$dashboard' data-id='$dashboard' >";
			
			if ($widgets[$dashboard]) {
				
				echo "<ul class='gridster-items' id='dashboard-$dashboard' data-dashboard='$dashboard' >";
				
				foreach ($widgets[$dashboard] as $key => $widget) {
					
					$row = $widget['row'];
					$col = $widget['col'];
					$x = $widget['size_x'];
					$y = $widget['size_y'];
					$resizable = $widget['data-resizable'];
					$draggable = $widget['data-draggable'];
					$theme = $widget['theme'];
					$title = $widget['title'];
					
					echo "<li id='widget-$key' data-id='$key' data-row='$row' data-col='$col' data-sizex='$x' data-sizey='$y' data-resizable='$resizable' data-draggable='$draggable' >";
					
						// widget theme
						echo "<div class='panel $theme'>";
						
							echo "
								<div class='panel-heading'>
									<strong>$title</strong>
									<span class='pull-right panel-actions'>
										<i class='fa fa-cog widget-theme'></i>
										<i class='fa fa-times-circle widget-remover'></i>
										<!--<i class='fa fa-angle-up widget-toggler'></i>-->
									</span>
								</div>
								<div class='panel-body'></div>
							";
					
						echo "</div>";
					
					echo "</li>";
				}

				echo "</ul>";
			}
			
			echo "</div>";
		}
		
		echo "</div>";
	}