<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

switch ($_REQUEST['section']) {
	
	case 'delete-idea':
	
		if ($_REQUEST['id']) {
			
			$id = $_REQUEST['id'];
			
			$idea = new Idea();
			$idea->read($id);
			
			if ($idea->id) {
				
				$response['response'] = $idea->delete();
				
				if ($response['response']) {
					
					$response['redirect'] = "/administration/ideas";
					
					// message
					Message::request_deleted();
					
					// remove files
					$model = new Model(Connector::DB_CORE);
					$model->db->exec("DELETE FROM idea_files WHERE idea_file_idea_id = $id");
					dir::remove("/public/data/files/ideas/$id", true);
					
				} else {
					
					$response['message'] = Translate::instance()->message_request_failure;
				}
				
			} else {
				
				$response['message'] = Translate::instance()->warning_failure_id;
			}
			
		} else {
			
			$response['message'] = Translate::instance()->warning_failure_id;
		}
		
	break;
	
	case 'delete-file':
		
		if ($_REQUEST['id']) {
			
			$id = $_REQUEST['id'];
			
			$ideaFile = new IdeaFile();
			$ideaFile->read($id);
		
			if ($ideaFile->id) {
				
				$file = $ideaFile->file;
				
				$response['response'] = $ideaFile->delete();
				
				if ($response['response']) {
					
					$response['remove'] = $id;
					
					// remove in db
					$response['message'] = Translate::instance()->message_request_deleted;
					
					// remove in files
					file::remove($file);
					
					$file = pathinfo($file);
					$dirname = $file['dirname'];
					$filename = $file['basename'];
					$thumb = "$dirname/thumbnail/$filename";
					file::remove($thumb);
					
				} else {
					
					$response['message'] = Translate::instance()->message_request_failure;
				}
				
			} else {
				
				$response['message'] = Translate::instance()->warning_failure_id;
			}
			
		} else {
			$response['message'] = Translate::instance()->message_request_failure;
		}
		
	break;
}

header('Content-Type: text/json');
echo json_encode($response);
