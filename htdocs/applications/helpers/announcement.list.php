<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	$model = new Model($application);
	
	// sql order
	$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'mps_announcement_date';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "desc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		$keyword = $_REQUEST['search'];
		$filters['search'] = "(
			mps_announcement_title LIKE \"%$keyword%\"
			OR mps_announcement_text LIKE \"%$keyword%\"
			OR mps_announcement_date LIKE \"%$keyword%\"
		)";
	}
	
	$filters['archived'] = ($archived)
		? "(mps_announcement_expiry_date < CURDATE() AND mps_announcement_expiry_date <> '' AND mps_announcement_expiry_date IS NOT NULL)"
		: "(mps_announcement_expiry_date >= CURDATE() OR mps_announcement_expiry_date = '' OR mps_announcement_expiry_date IS NULL)";

	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS
			mps_announcement_id,
			mps_announcement_important,
			mps_announcement_title,
			LEFT(mps_announcement_text, 100) as text,
			DATE_FORMAT(mps_announcement_date, '%d.%m.%Y') as date
		FROM mps_announcements
	")
	->filter($filters)
	->order($order, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();

	$totalrows = $model->totalRows();
	
	if ($result) {
		
		$datagrid = _array::datagrid($result);
		
		foreach ($datagrid as $key => $row) {
			
			if ($row['mps_announcement_important']) {
				$datagrid[$key]['important'] = ui::icon('star');
			}
			
			$title = $row['mps_announcement_title'];
			$content = strip_tags($row['text']);
			$datagrid[$key]['mps_announcement_title'] = "<a href='".$_REQUEST['show']."/$key'>$title</a><span>$content..</span>";
			
			// date
			$datagrid[$key]['mps_announcement_date'] = $row['date'];
		}
		
		$pager = new Pager(array(
			'page' => $_REQUEST['page'],
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));
		
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	$table = new Table(array(
		'sort' => array('column' => $order, 'direction' => $direction)
	));
	
	$table->datagrid = $datagrid;
	
	$table->caption('important', null);
	
	if ($datagrid && _array::key_exists('important', $datagrid)) {
		$table->important();
	}
	
	$table->mps_announcement_date(
		Table::PARAM_SORT,
		'width=20%'
	);
	
	$table->mps_announcement_title(
		Table::PARAM_SORT
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	
	$table = $table->render();
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
	
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'caption' => $translate->add_new
		));
	}
	
	$toolbox[] = ui::searchbox();
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	echo $toolbox.$table;
	