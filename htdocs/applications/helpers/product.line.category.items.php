<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$category = $_REQUEST['category_id'];
$productLine = $_REQUEST['category_product_line'];
$assignItems = $_REQUEST['assign_items'];

$model = new Model($application);

if ($assignItems) {
	$query = "
		SELECT 
			item_types.item_type_id,
			item_types.item_type_name, 
			items.item_id, 
			items.item_code, 
			items.item_name,
			items.item_price,
			item_categories.item_category_id,
			item_categories.item_category_name
		FROM items 
		LEFT JOIN item_types ON item_types.item_type_id = items.item_type
		LEFT JOIN item_categories ON items.item_category = item_categories.item_category_id
		WHERE item_active = 1 AND item_type IN (1,7)
		ORDER BY 
			item_categories.item_category_name, 
			items.item_code
	";
} else {
	$query = "
		SELECT 
			item_types.item_type_id,
			item_types.item_type_name, 
			items.item_id, 
			items.item_code, 
			items.item_name,
			items.item_price,
			item_categories.item_category_id,
			item_categories.item_category_name
		FROM items 
		INNER JOIN item_types ON item_types.item_type_id = items.item_type
		INNER JOIN item_categories ON items.item_category = item_categories.item_category_id
		INNER JOIN category_items ON items.item_id = category_items.category_item_item
		LEFT JOIN categories ON categories.category_id = category_items.category_item_category
		WHERE item_active = 1 AND item_type IN (1,7) AND category_items.category_item_category = $category AND categories.category_product_line = $productLine
		ORDER BY 
			item_categories.item_category_name, 
			items.item_code
	";
}

$result = $model->query($query)->fetchAll();

if ($result) {

	if ($assignItems) {

		$categoryItems = $model->query("
			SELECT 
				category_item_item,
				category_item_category
			FROM category_items
			INNER JOIN categories ON categories.category_id = category_items.category_item_category
			WHERE categories.category_product_line = $productLine
		")->fetchAll();

		$itemCategories = _array::extract($categoryItems);
	}

	foreach ($result as $row) {
		
		$type = $row['item_category_id'];
		$item = $row['item_id'];

		$datagrid[$type]['caption'] = $row['item_category_name'];

		if ($item) {

			if ($assignItems) {
				
				if ($itemCategories[$item]==$category) {
					$datagrid[$type]['items'][$item]['assign'] = "<input type=checkbox name='category-item' data-id='$item' checked=checked >";
				}
				elseif (!$datagrid[$type]['items'][$item]['assign']) {
					$datagrid[$type]['items'][$item]['assign'] = "<input type=checkbox name='category-item' data-id='$item' >";
				}
			}

			$datagrid[$type]['items'][$item]['item_code'] = $row['item_code'];
			$datagrid[$type]['items'][$item]['item_name'] = $row['item_name'];
			$datagrid[$type]['items'][$item]['item_price'] = $row['item_price'];
		}
	}
}

if ($datagrid) {

	if (!$assignItems) {
		$list = "<h4>Category Items</h4>";
	}

	foreach ($datagrid as $cat => $row) {
		
		$list .= "<h5><i class='fa fa-chevron-circle-right'></i> {$row[caption]}</h5>";

		if ($row['items']) {

			$table = new Table();
			$table->datagrid = $row['items'];

			if ($assignItems) {
				$table->assign('width=20px');
			}

			$table->item_code('width=20%');
			$table->item_name();
			$table->item_price('width=10%');

			$list .= "<div class='items'>".$table->render()."</div>";
		}

	}
}

echo $list;


