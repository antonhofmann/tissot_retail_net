<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$model = new Model();

	// request
	$_REQUEST = session::filter($application, 'role.application.list', false);
	
	// framework application
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'application_name';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// sql pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$offset = ($page-1) * $settings->limit_pager_rows;
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "( 
			application_name LIKE \"%$keyword%\" 
		)";
	}
	
	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS
			application_id,
			application_name	
		FROM applications
	")
	->order($sort, $direction)
	->filter($filters)
	->offset($offset)
	->fetchAll();
	
	if ($result) {
	
		$datagrid = _array::datagrid($result);
		$totalrows = $model->totalRows();
		
		foreach ($datagrid as $key => $row) {
			
			$result = $model->query("
				SELECT application_role_id
				FROM application_roles
				WHERE application_role_application = $key AND application_role_role = $id	
			")->fetchAll();
			
			$dataloader['application'][$key] = ($result) ? 1 : 0;
			$datagrid[$key]['application'] = $key;
		}
	
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";

	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	$table->dataloader($dataloader);
	$table->caption('application', '');
	
	$table->application_name(
		Table::ATTRIBUTE_NOWRAP
	);
	
	$table->application(
		Table::DATA_TYPE_CHECKBOX,
		'width=2%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
