<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// mvc vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['address_id'];

	$company = new Company();
	$company->read($id);

	// visible fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// geo data
	$country = ($_REQUEST['address_country']) ? $_REQUEST['address_country'] : $company->country;
	$province = ($_REQUEST['province']) ? $_REQUEST['province'] : $company->province_id;
	$place = ($_REQUEST['address_place_id']) ? $_REQUEST['address_place_id'] : $company->place_id;

	$data = array();

	// copmany type
	if ($_REQUEST['address_type']) {
		$data['address_type'] = $_REQUEST['address_type'];
	}
	if (!$id && $application=="mps") {
		$data['address_type'] = 7;
	}

	// show in posindex
	if (!$id && $application == 'mps') {
		$data['address_showinposindex'] = 1;
	}

	// company internal
	if (in_array('address_is_internal_address', $fields)) {
		$data['address_is_internal_address'] = (isset($_REQUEST['address_is_internal_address'])) ? 1 : 0;
	}

	// own independent retailer
	if (in_array('address_can_own_independent_retailers', $fields)) {
		$data['address_can_own_independent_retailers'] = (isset($_REQUEST['address_can_own_independent_retailers'])) ? 1 : 0;
	}

	// independent retailer
	if (in_array('address_is_independent_retailer', $fields)) {
		$data['address_is_independent_retailer'] = (isset($_REQUEST['address_is_independent_retailer'])) ? 1 : 0;
	}
	elseif (!$id) {
		$data['address_is_independent_retailer'] = ($user->permission(Company::PERMISSION_EDIT_LIMITED)) ? 1 : 0;
	}

	// copmany parent
	if (in_array('address_parent', $fields)) {
		$data['address_parent'] = $_REQUEST['address_parent'];
	}
	elseif(!$id && $user->permission(Company::PERMISSION_EDIT_LIMITED)) {
		$data['address_parent'] = $user->address;
	}

	// company involved in planning
	if (in_array('address_involved_in_planning', $fields)) {
		$data['address_involved_in_planning'] = (isset($_REQUEST['address_involved_in_planning'])) ? 1 : 0;
	}
	
	// company involved in planning
	if (in_array('address_involved_in_planning_ff', $fields)) {
		$data['address_involved_in_planning_ff'] = (isset($_REQUEST['address_involved_in_planning_ff'])) ? 1 : 0;
	}


	// company involved in planning
	if (in_array('address_involved_in_lps', $fields)) {
		$data['address_involved_in_lps'] = (isset($_REQUEST['address_involved_in_lps'])) ? 1 : 0;
	}
	
	// company involved in planning
	if (in_array('address_involved_in_lps_ff', $fields)) {
		$data['address_involved_in_lps_ff'] = (isset($_REQUEST['address_involved_in_lps_ff'])) ? 1 : 0;
	}
	
	// Export MPS Distrubution Data to SAP
	if (in_array('address_do_data_export_from_mps_to_sap', $fields)) {
		$data['address_do_data_export_from_mps_to_sap'] = (isset($_REQUEST['address_do_data_export_from_mps_to_sap'])) ? 1 : 0;
	}

	// company involved in RED
	if (in_array('address_involved_in_red', $fields)) {
		$data['address_involved_in_red'] = (isset($_REQUEST['address_involved_in_red'])) ? 1 : 0;
	}

	// address_type
	if (isset($_REQUEST['address_type'])) {
		$data['address_type'] = $_REQUEST['address_type'];
	}
	// address_number
	if (isset($_REQUEST['address_number'])) {
		$data['address_number'] = $_REQUEST['address_number'];
	}
	// address_legnr
	if (isset($_REQUEST['address_legnr'])) {
		$data['address_legnr'] = $_REQUEST['address_legnr'];
	}

	// customer number
	if (isset($_REQUEST['address_mps_customernumber'])) {
		$data['address_mps_customernumber'] = $_REQUEST['address_mps_customernumber'];
	}

	// shipto number
	if (isset($_REQUEST['address_mps_shipto'])) {
		$data['address_mps_shipto'] = $_REQUEST['address_mps_shipto'];
	}

	// SNR number
	if (isset($_REQUEST['address_sapnr'])) {
		$data['address_sapnr'] = $_REQUEST['address_sapnr'];
	}
	

	// SNR number
	if (isset($_REQUEST['address_sap_salesorganization'])) {
		$data['address_sap_salesorganization'] = $_REQUEST['address_sap_salesorganization'];
	}

	// legal entity name
	if (isset($_REQUEST['address_legal_entity_name'])) {
		$data['address_legal_entity_name'] = $_REQUEST['address_legal_entity_name'];
	}

	// company name
	if (isset($_REQUEST['address_company'])) {
		$data['address_company'] = $_REQUEST['address_company'];
	}

	// second company name
	if (isset($_REQUEST['address_company2'])) {
		$data['address_company2'] = $_REQUEST['address_company2'];
	}

	// company address street
	if (isset($_REQUEST['address_street'])) {
		$data['address_street'] = trim($_REQUEST['address_street']);
	}

	// company address street number
	if (isset($_REQUEST['address_streetnumber'])) {
		$data['address_streetnumber'] = trim($_REQUEST['address_streetnumber']);
	}

	// company address
	$countryData = new Country();
	$tmp = $countryData->read($country);
	
	if (isset($_REQUEST['address_street'])) {
		
		if(trim($_REQUEST['address_streetnumber'])) {
			
			if($tmp['country_nr_before_streename'] == 1) {
				$data['address_address'] = trim($_REQUEST['address_streetnumber']) . ', ' . $_REQUEST['address_street'];
			}
			else {
				$data['address_address'] = $_REQUEST['address_street'] . ' ' . trim($_REQUEST['address_streetnumber']);
			}
		}
		else {
			$data['address_address'] = $_REQUEST['address_street'];
		}
	}

	// second company address
	if (isset($_REQUEST['address_address2'])) {
		$data['address_address2'] = $_REQUEST['address_address2'];
	}

	// second company address
	if (isset($_REQUEST['address_zip'])) {
		$data['address_zip'] = $_REQUEST['address_zip'];
	}

	// company country
	if (isset($_REQUEST['address_country'])) {

		$data['address_country'] = $_REQUEST['address_country'];

		if (!isset($_REQUEST['address_currency'])) {
			$Country = new Country();
			$Country->read($_REQUEST['address_country']);
			$data['address_currency'] = $Country->currency;
		}
	}

	// company province
	if ($country && $_REQUEST['new_province'] && $_REQUEST['new_place']) {

		$Province = new Province();

		$province = $Province->create(array(
			'province_country' => $country,
			'province_canton' => $_REQUEST['new_province'],
			'user_created' => $user->login,
			'date_created' => date('Y-m-d H:i:s')
		));

		$insertedProvince = true;
	}

	// company place
	if ($country && $_REQUEST['new_place']) {
		
		$Place = new Place();

		$place = $Place->create(array(
			'place_country' => $country,
			'place_province' => $province,
			'place_name' => $_REQUEST['new_place'],
			'user_created' => $user->login,
			'date_created' => date('Y-m-d H:i:s')
		));

		$insertedPlace = true;
		
		$data['address_place_id'] = $place;
	}
	elseif ($_REQUEST['address_place_id']) {
		$data['address_place_id'] = $_REQUEST['address_place_id'];
	}

	// place name
	if ($_REQUEST['address_place_id'] || $_REQUEST['new_place']) {

		$Place = new Place();
		$Place->read($place);

		$data['address_place'] = $Place->name;
	}

	
	if (isset($_REQUEST['address_phone_number'])) {
		$_REQUEST['address_phone_number'] = str_replace(' ', '', $_REQUEST['address_phone_number']);
	}

	if (isset($_REQUEST['address_phone_area'])) {
		$_REQUEST['address_phone_area'] = str_replace(' ', '', $_REQUEST['address_phone_area']);
	}

	if (isset($_REQUEST['address_phone_country'])) {
		$_REQUEST['address_phone_country'] = str_replace(' ', '', $_REQUEST['address_phone_country']);
	}
	
	if(isset($_REQUEST['address_phone_number'])) {
		// company phone country code
		if (isset($_REQUEST['address_phone_country'])) {
			$data['address_phone_country'] = $_REQUEST['address_phone_country'];
		}

		// company phone area code
		if (isset($_REQUEST['address_phone_area'])) {
			$data['address_phone_area'] = $_REQUEST['address_phone_area'];
		}

		// company phone number
		if (isset($_REQUEST['address_phone_number'])) {
			$data['address_phone_number'] = $_REQUEST['address_phone_number'];
		}

		// company phone
		if (isset($_REQUEST['address_phone_country']) && isset($_REQUEST['address_phone_area']) && isset($_REQUEST['address_phone_number'])) {
			
			if($_REQUEST['address_phone_area']) {
				$data['address_phone'] = $_REQUEST['address_phone_country']. ' '  . $_REQUEST['address_phone_area']. ' ' .  $_REQUEST['address_phone_number'];
			}
			else {
				$data['address_phone'] = $_REQUEST['address_phone_country']. ' ' .  $_REQUEST['address_phone_number'];
			}
		}
	}

	
	if (isset($_REQUEST['address_mobile_phone_number'])) {
	$_REQUEST['address_mobile_phone_number'] = str_replace(' ', '', $_REQUEST['address_mobile_phone_number']);
	}

	if (isset($_REQUEST['address_mobile_phone_area'])) {
		$_REQUEST['address_mobile_phone_area'] = str_replace(' ', '', $_REQUEST['address_mobile_phone_area']);
	}

	if (isset($_REQUEST['address_mobile_phone_country'])) {
		$_REQUEST['address_mobile_phone_country'] = str_replace(' ', '', $_REQUEST['address_mobile_phone_country']);
	}
	
	if(isset($_REQUEST['address_mobile_phone_number'])) {
		// company mobile_phone country code
		if (isset($_REQUEST['address_mobile_phone_country'])) {
			$data['address_mobile_phone_country'] = $_REQUEST['address_mobile_phone_country'];
		}

		// company mobile_phone area code
		if (isset($_REQUEST['address_mobile_phone_area'])) {
			$data['address_mobile_phone_area'] = $_REQUEST['address_mobile_phone_area'];
		}

		// company mobile_phone number
		if (isset($_REQUEST['address_mobile_phone_number'])) {
			$data['address_mobile_phone_number'] = $_REQUEST['address_mobile_phone_number'];
		}

		// company mobile_phone
		if (isset($_REQUEST['address_mobile_phone_country']) && isset($_REQUEST['address_mobile_phone_area']) && isset($_REQUEST['address_mobile_phone_number'])) {
			
			if($_REQUEST['address_mobile_phone_area']) {
				$data['address_mobile_phone'] = $_REQUEST['address_mobile_phone_country']. ' ' . $_REQUEST['address_mobile_phone_area']. ' '  . $_REQUEST['address_mobile_phone_number'];
			}
			elseif($_REQUEST['address_mobile_phone_number']) {
				$data['address_mobile_phone'] = $_REQUEST['address_mobile_phone_country']. ' '  . $_REQUEST['address_mobile_phone_number'];
			}
		}
	}

	// company email
	if (isset($_REQUEST['address_email'])) {
		$data['address_email'] = $_REQUEST['address_email'];
	}

	// company website
	if (isset($_REQUEST['address_website'])) {
		$data['address_website'] = $_REQUEST['address_website'];
	}

	// company contact
	if (isset($_REQUEST['address_contact'])) {
		$data['address_contact'] = $_REQUEST['address_contact'];
	}

	// company contact name
	if (isset($_REQUEST['address_contact_name'])) {
		$data['address_contact_name'] = $_REQUEST['address_contact_name'];
	}

	// company contact email
	if (isset($_REQUEST['address_contact_email'])) {
		$data['address_contact_email'] = $_REQUEST['address_contact_email'];
	}
	
	// company currency
	if (isset($_REQUEST['address_currency'])) {
		$data['address_currency'] = $_REQUEST['address_currency'];
	}

	// company active
	if (in_array('address_active', $fields)) {

		$data['address_active'] = (isset($_REQUEST['address_active'])) ? 1 : 0;

		if ($company->id && $company->active && !$_REQUEST['address_active']) {
			$setUsersInactive = true;
		}
	}

	// submit request
	if ($data) {

		if ($id) {
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			

			if($data['address_active'] == 1) {
				$data['address_last_modified_by'] = $user->id;
				$data['address_last_modified'] = date('Y-m-d H:i:s');
			}
			else {
				
				$data['address_set_to_inactive_by'] = $user->id;
				$data['address_set_to_inactive_date'] = date('Y-m-d H:i:s');
			}

			
			$response = $company->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
			//do redirect if new province or new place has been added
			if ($_REQUEST['new_address_province_id'] || $_REQUEST['new_address_place_id']) {
				$redirect = $_REQUEST['redirect']."/$id";
			}
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$data['address_added_by'] = $user->id;
			
			$response = $company->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$id = $response;
			
			if ($_REQUEST['create_pos_from_company']) {
				$redirect = "/$application/pos/add/$id";
			} else {
				$redirect = $_REQUEST['redirect']."/$id";
			}
		}

		$company->read($id);
	}

	// set user company as inactive
	if ($response && $company->id && $setUsersInactive) {

		$model = new Model();

		$model->db->exec("
			UPDATE users SET user_active = 0
			WHERE user_address = $company->id
		");
	}

	// synchronisation: company <-> pos location
	// update all pos locations with the same address
	if ($response) {

		$company_pos = $company->get_pos();
		$data = $company->data;

		if ($company_pos && $data) {

			$pos = new Pos();
			$pos->read($company_pos);

			$pos->update(array(
				//'posaddress_name' => $data['address_company'],
				//'posaddress_name2' => $data['address_company2'],
				'posaddress_address' => $data['address_street'].' '.$data['address_streetnumber'],
				'posaddress_street' => $data['address_street'],
				'posaddress_street_number' => $data['address_streetnumber'],
				'posaddress_address2' => $data['address_address2'],
				'posaddress_country' => $data['address_country'],
				'posaddress_place_id' => $data['address_place_id'],
				'posaddress_zip' => $data['address_zip'],
				'posaddress_phone' => $data['address_phone'],
				'posaddress_phone_country' => $data['address_phone_country'],
				'posaddress_phone_area' => $data['address_phone_area'],
				'posaddress_phone_number' => $data['address_phone_number'],
				'posaddress_mobile_phone' => $data['address_mobile_phone'],
				'posaddress_mobile_phone_country' => $data['address_mobile_phone_country'],
				'posaddress_mobile_phone_area' => $data['address_mobile_phone_area'],
				'posaddress_mobile_phone_number' => $data['address_mobile_phone_number'],
				'posaddress_email' => $data['address_email'],
				'posaddress_website' => $data['address_website'],
				'posaddress_contact_name' => $data['address_contact_name'],
				'posaddress_contact_email' => $data['address_contact_email']
			));
		}
	}

	// insert new province and new place in store locator
	if ($response && ($insertedProvince || $insertedPlace)) {

		$Country = new Country();
		$Country->read($country);
		$storelocator_country = $Country->store_locator_id;

		$storelocator = new Store_Locator();

		if ($insertedProvince) {
			$storelocator->add_province(array(
				'province_id' => $province,
				'province_country_id' => $storelocator_country,
				'province_canton' => $_REQUEST['new_province']
			));
		}

		if ($insertedPlace) {
			$storelocator->add_place(array(
				'place_id' => $place,
				'place_country_id' => $storelocator_country,
				'place_province_id' => $province,
				'place_name' => $_REQUEST['new_place']
			));
		}
	}

	//update franchisee address of all ongoing projects
	if ($response && $company->id) {

		$fields = array();
		$fields['order_franchisee_address_id'] =  $company->id;
		$fields['order_franchisee_address_company'] =  $company->company;
		$fields['order_franchisee_address_company2'] =  $company->company2;
		$fields['order_franchisee_address_address'] =  $company->address;
		$fields['order_franchisee_address_address2'] =  $company->address2;
		$fields['order_franchisee_address_zip'] =  $company->zip;
		$fields['order_franchisee_address_place'] =  $company->place;
		$fields['order_franchisee_address_country'] =  $company->country;
		$fields['order_franchisee_address_phone'] =  $company->phone;
		$fields['order_franchisee_address_mobile_phone'] =  $company->mobile_phone;
		$fields['order_franchisee_address_email'] =  $company->email;
		$fields['order_franchisee_address_contact'] =  $company->contact_name;
		$fields['order_franchisee_address_website'] =  $company->website;
		$fields['user_modified'] =  $user->login;


		$model = Connector::get(Connector::DB_CORE);
		
		$sth = $model->prepare("
			UPDATE orders SET
				order_franchisee_address_company = :order_franchisee_address_company,
				order_franchisee_address_company2 = :order_franchisee_address_company2,
				order_franchisee_address_address = :order_franchisee_address_address,
				order_franchisee_address_address2 = :order_franchisee_address_address2,
				order_franchisee_address_zip = :order_franchisee_address_zip,
				order_franchisee_address_place = :order_franchisee_address_place,
				order_franchisee_address_country = :order_franchisee_address_country,
				order_franchisee_address_phone = :order_franchisee_address_phone,
				order_franchisee_address_mobile_phone = :order_franchisee_address_mobile_phone,
				order_franchisee_address_email = :order_franchisee_address_email,
				order_franchisee_address_contact = :order_franchisee_address_contact,
				order_franchisee_address_website = :order_franchisee_address_website,
				user_modified = :user_modified,
				date_modified = NOW()
			WHERE order_type = 1 
			AND (order_archive_date IS NULL OR order_archive_date = '0000-00-00') 
			AND order_franchisee_address_id = :order_franchisee_address_id 
		");

		$sth->execute($fields);
	}

	echo json_encode(array(
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message
	));
