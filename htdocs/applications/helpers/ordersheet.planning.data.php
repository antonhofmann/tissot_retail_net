<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc'; 

	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['ordersheet'];
	$version = $_REQUEST['version'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", false); 

	// model
	$model = new Model($application);
	
	// current order sheet	
	$ordersheet = new Ordersheet($application);
	$ordersheet->read($id);
	
	// is archived
	$isDistributed = ($ordersheet->state()->isDistributed() && $ordersheet->isDistributed()) ? true : false;
	
	// can edit order sheet
	$canEdit = (user::permission(Ordersheet::PERMISSION_EDIT) || user::permission(Ordersheet::PERMISSION_EDIT_LIMITED)) ? true : false;
	
	// company
	$company = new Company();
	$company->read($ordersheet->address_id);
	
	
	
/*******************************************************************************************************************************************************************************************
	Ordersheet States
********************************************************************************************************************************************************************************************/
	
	// group statments
	$states_preparation = Ordersheet_State::loader('preparation');
	$states_complete = Ordersheet_State::loader('complete');
	$states_revision = Ordersheet_State::loader('revision');
	$states_confirmation = Ordersheet_State::loader('confirmation');
	$states_distribution = Ordersheet_State::loader('distribution');
	
	// is in group state
	$statment_on_preparation = $ordersheet->state()->onPreparation();
	$statment_on_completing = $ordersheet->state()->onCompleting();
	$statment_on_approving = $ordersheet->state()->onApproving();
	$statment_on_revision = $ordersheet->state()->onCompleting();
	$statment_on_confirmation = $ordersheet->state()->onConfirmation();
	$statment_on_distribution = $ordersheet->state()->onDistribution();
	
	// expired ordersheets on planning phase
	if ($ordersheet->state()->owner) {
		$canEdit = ($statment_on_preparation && $ordersheet->state()->expired) ? false : $canEdit;
	}
	
	$editabled_states = array();

	// order sheet editabled states
	if ($ordersheet->state()->owner) {
		$editabled_states = array_merge($states_complete, $states_revision, $states_confirmation, $states_distribution);
	}
	elseif ($ordersheet->state()->canAdministrate()) {
		$editabled_states = array_merge($states_preparation, $states_confirmation, $states_distribution);
	}

	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Global Properties
	Set editabled and visibility vars
********************************************************************************************************************************************************************************************/
	
	// planning column keyword
	define(COLUMN_PLANNING, 'planned');
	
	// distribution column keyword
	define(COLUMN_DISTRIBUTION, 'delivered');
	
	// partially confirmed distribution keyword
	define(COLUMN_PARTIALY_CONFIRMED, 'partial');
	
	// spreadsheet readonly
	define('CELL_READONLY', 'true');
	
	// readonly mod
	if ($version || !$canEdit) $mod_readonly = true;
	elseif($ordersheet->state()->isManuallyArchived()) $mod_readonly = false;
	else $mod_readonly =  ($ordersheet->state()->isDistributed() || !in_array($ordersheet->workflowstate_id, $editabled_states)) ? true: false;
	
	// planning mod
	$mod_planning = ($statment_on_preparation || $statment_on_confirmation) ? true : false;
	
	// distribution mod
	$mod_distribution = (in_array($ordersheet->workflowstate_id, $states_distribution) || $ordersheet->state()->isDistributed()) ? true : false;
	
	// spreadsheet attribute readonly
	$spreadsheet_attribute_readonly =  ($mod_readonly) ? 'true' : 'false';
	
	// planning columns
	$show_planning_columns = true;
	
	// distribution columns
	$show_distribution_columns = ($mod_distribution) ? true : false;
	
	// extended rows (warehouses)
	$show_extended_rows = (!$isDistributed || $version) ? true : false;
	
	// stock reserve edit
	if ($canEdit) {
		if ($ordersheet->state()->canAdministrate()) {
			$stock_reserve_edit = ($ordersheet->state()->onApproving()) ? true : false;
		} else {
			$stock_reserve_edit = ($ordersheet->state()->onCompleting()) ? true : false;
		}
	}
	
	// stock reserve id
	if ($stock_reserve_edit) {
		$result = $ordersheet->warehouse()->getStockReserve();
		$stock_rserve_id = $result['mps_ordersheet_warehouse_id'];
	}

	// show reserve row
	if ($isDistributed) {
		$show_reserve_row = ($version) ? true : false;
	} else {
		$show_reserve_row = true;
	}

	// calculation area (first in calculation set)
	$show_calculation_area = true;
	
	// planning class name
	$class_planning = ($mod_planning) ? 'planning' : null;

	
/*******************************************************************************************************************************************************************************************
	Request Filters
********************************************************************************************************************************************************************************************/
	
	$filters = array();
	$filer_labels = array();
	
	// filter: pos
	$filters['default'] = "(
		posaddress_client_id = $ordersheet->address_id 
		OR (
			address_parent = $ordersheet->address_id 
			AND address_client_type = 3
		)  
	)";
	
	// filter: distribution channels
	if ($_REQUEST['distribution_channels']) {
		$filters['distribution_channels'] = 'posaddress_distribution_channel = '.$_REQUEST['distribution_channels'];
		$filter_active = 'active';
	}
	
	// filter: turnover classes
	if ($_REQUEST['turnoverclass_watches']) {
		$filters['turnoverclass_watches'] = 'posaddress_turnoverclass_watches = '.$_REQUEST['turnoverclass_watches'];
		$filter_active = 'active';
	}
	
	// filter: sales represenatives
	if ($_REQUEST['sales_representative']) {
		$filters['sales_representative'] = 'posaddress_sales_representative = '.$_REQUEST['sales_representative'];
		$filter_active = 'active';
	}
	
	// filter: decoration persons
	if ($_REQUEST['decoration_person']) {
		$filters['decoration_person'] = 'posaddress_decoration_person = '.$_REQUEST['decoration_person'];
		$filter_active = 'active';
	}
	
	// provinces
	if ($_REQUEST['provinces']) {
		$filters['provinces'] = 'province_id = '.$_REQUEST['provinces'];
		$filter_active = 'active';
	}
	
	// for archived ordersheets show only POS locations which has distributed quantities 
	if ($isDistributed) {
		
		$result = $model->query("
			SELECT DISTINCT 
				mps_ordersheet_item_delivered_posaddress_id AS pos
			FROM mps_ordersheet_item_delivered		
		")
		->bind(Ordersheet_Item_Delivered::DB_BIND_ORDERSHEET_ITEMS)
		->filter('ordersheet', 'mps_ordersheet_item_ordersheet_id = '.$ordersheet->id)
		->filter('null', 'mps_ordersheet_item_delivered_posaddress_id > 0')
		->fetchAll();
		
		if ($result) {
			
			$array = array();
			
			foreach ($result as $row) {
				$array[] = $row['pos'];
			}

			$filters['active'] = 'posaddress_id IN ('.join(',', $array).')';
		}
	}
	// otherwise show all active pos locations
	else {
		$filters['active'] = "(
			posaddress_store_closingdate = '0000-00-00'
			OR posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = ''
		)";
	}
	
	
/*******************************************************************************************************************************************************************************************
	Dataloader
	POS Locations
********************************************************************************************************************************************************************************************/
	
	$pos_locations = array();
	
	$pos_filter = $filters ? join(' AND ', array_values($filters)) : null;
	
	$result = $model->query("
		SELECT DISTINCT
			posaddress_id,
			posaddress_name,
			posaddress_place,
			postype_id,
			postype_name
		FROM db_retailnet.posaddresses
		INNER JOIN db_retailnet.addresses ON address_id = posaddress_client_id
		INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
		INNER JOIN db_retailnet.provinces ON province_id = place_province
		INNER JOIN db_retailnet.postypes ON postype_id = posaddress_store_postype
		WHERE $pos_filter
		ORDER BY posaddress_name, posaddress_place
	")->fetchAll();


	
	if ($result) {
		foreach ($result as $row) {
			$pos = $row['posaddress_id'];
			$type = $row['postype_id'];
			$pos_locations[$type]['caption'] = $row['postype_name'];
			$pos_locations[$type]['pos'][$pos]['name'] = $row['posaddress_name'];
			$pos_locations[$type]['pos'][$pos]['place'] = $row['posaddress_place'];
			
			$sapInvolvedPos[$pos] = true;
		}
	}
	
/*******************************************************************************************************************************************************************************************
	Dataloader
	Order Sheet Items
********************************************************************************************************************************************************************************************/
	
	$ordersheet_items = array();
	$item_partially_columns = array();
	$distrubuted_items = array();
	
	$result = $model->query("
		SELECT
			mps_ordersheet_item_id,
			mps_ordersheet_item_material_id,
			mps_ordersheet_item_quantity,
			mps_ordersheet_item_quantity_approved,
			mps_ordersheet_item_quantity_confirmed,
			mps_ordersheet_item_quantity_shipped,
			mps_ordersheet_item_quantity_distributed,
			mps_material_planning_type_id,
			mps_material_planning_type_name,
			mps_material_collection_category_id,
			mps_material_collection_category_code,
			mps_material_id,
			mps_material_code,
			mps_material_name
		FROM mps_ordersheet_items
	")
	->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
	->bind(Ordersheet_Item::DB_BIND_MATERIALS)
	->bind(Material::DB_BIND_PLANNING_TYPES)
	->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
	->filter('ordersheets', "mps_ordersheet_id = ".$ordersheet->id)
	->order('mps_material_planning_type_name')
	->order('mps_material_collection_category_code')
	->order('mps_material_code')
	->order('mps_material_name')
	->fetchAll();

	
	if ($result) {
		
		// check if delivered quantities are once confirmed
		$has_confirmation = $model->query("
			SELECT COUNT(mps_ordersheet_item_delivered_id) AS total
			FROM mps_ordersheet_item_delivered	
		")
		->bind(Ordersheet_Item_Delivered::DB_BIND_ORDERSHEET_ITEMS)
		->filter('ordersheet', 'mps_ordersheet_item_ordersheet_id = '.$ordersheet->id)
		->filter('confirmed', 'mps_ordersheet_item_delivered_confirmed IS NOT NULL')
		->fetch();
		
		foreach ($result as $row) {
			
			$approved_quantity = $row['mps_ordersheet_item_quantity_approved'];
			$confirmed_quantity = $row['mps_ordersheet_item_quantity_confirmed'];
			
			// show item in spreadsheet
			// for mod planning: quantity approved greater then zero or null
			// for mod distribution: quantity confirmed greater then zero
			if ($mod_distribution) $show_item = ($confirmed_quantity > 0) ? true : false;
			else $show_item = ($approved_quantity > 0 || is_null($approved_quantity)) ? true : false;
			
			if ($show_item) {
				
				$item = $row['mps_ordersheet_item_id'];
				$material = $row['mps_ordersheet_item_material_id'];
				$planning = $row['mps_material_planning_type_id'];
				$collection = $row['mps_material_collection_category_id'];
				$ordersheet_items[$planning]['caption'] = $row['mps_material_planning_type_name'];
				$ordersheet_items[$planning]['data'][$collection]['caption'] = $row['mps_material_collection_category_code'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['id'] = $row['mps_material_id'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['code'] = $row['mps_material_code'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['name'] = $row['mps_material_name'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity'] = $row['mps_ordersheet_item_quantity'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity_approved'] = $row['mps_ordersheet_item_quantity_approved'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity_confirmed'] = $row['mps_ordersheet_item_quantity_confirmed'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity_shipped'] = $row['mps_ordersheet_item_quantity_shipped'];
				$ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity_distributed'] = $row['mps_ordersheet_item_quantity_distributed'];
				
				if ($mod_distribution && $has_confirmation['total'] && $row['mps_ordersheet_item_quantity_distributed']) {
					
					// items for partialy distribution
					if ($row['mps_ordersheet_item_quantity_shipped']-$row['mps_ordersheet_item_quantity_distributed'] > 0) {
						$item_partially_columns[$planning][$collection][$material] = true;
					} 
					// delivered items
					else {
						$distrubuted_items[$material] = true;
					}
				}
			}
		}

		// if all items are delivered hide unused row
		if (!$version && $distrubuted_items[$material] && !$item_partially_columns) {
			$spreadsheet_row_unused_quantity = false;
		}
	}


/*******************************************************************************************************************************************************************************************
	Dataloader
	Order Sheet Items Planned Quantities
********************************************************************************************************************************************************************************************/
	
	$planning_pos_quantities = array();
	$planning_warehouse_quantities = array();
	$planning_reserve_quantities = array();
	
	$sum_planning_quantities = array();
	
	if ($show_planning_columns) {
		
		$result = $model->query("
			SELECT
				mps_ordersheet_warehouse_id
			FROM
				mps_ordersheet_warehouses
			WHERE
				mps_ordersheet_warehouse_stock_reserve = 1
			AND mps_ordersheet_warehouse_ordersheet_id = $ordersheet->id
		")->fetch();
		
		$stockReserveWarehouseID = $result['mps_ordersheet_warehouse_id'];
		
		$result = $model->query("
			SELECT DISTINCT
				mps_ordersheet_item_planned_id,
				mps_ordersheet_item_planned_posaddress_id,
				mps_ordersheet_item_planned_warehouse_id,
				mps_ordersheet_item_planned_quantity,
				mps_ordersheet_item_material_id,
				mps_ordersheet_warehouse_stock_reserve
			FROM mps_ordersheet_item_planned
		")
		->bind(Ordersheet_Item_Planned::DB_BIND_ORDERSHEET_ITEMS)
		->bind('LEFT JOIN mps_ordersheet_warehouses ON mps_ordersheet_warehouse_id = mps_ordersheet_item_planned_warehouse_id')
		->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = ".$ordersheet->id)
		->fetchAll();
		
		if ($result) {
			
			foreach ($result as $row) {
				
				$material = $row['mps_ordersheet_item_material_id'];
				
				// planned quantities
				if ($row['mps_ordersheet_item_planned_posaddress_id']) {
					$pos = $row['mps_ordersheet_item_planned_posaddress_id'];
					$planning_pos_quantities[$pos][$material]['id'] = $row['mps_ordersheet_item_planned_id'];
					$planning_pos_quantities[$pos][$material]['quantity'] = $row['mps_ordersheet_item_planned_quantity'];
					$sum_planning_quantities[$material] = $sum_planning_quantities[$material] + $row['mps_ordersheet_item_planned_quantity'];
				}
				
				// planned warehouse quantities
				if ($row['mps_ordersheet_item_planned_warehouse_id']) {
					
					$warehouse = $row['mps_ordersheet_item_planned_warehouse_id'];
					
					if ($row['mps_ordersheet_warehouse_stock_reserve']) {
						$planning_reserve_quantities[$material]['id'] = $row['mps_ordersheet_item_planned_id'];
						$planning_reserve_quantities[$material]['warehouse'] = $warehouse;
						$planning_reserve_quantities[$material]['quantity'] = $row['mps_ordersheet_item_planned_quantity'];
					} else {
						
						$planning_warehouse_quantities[$warehouse][$material]['id'] = $row['mps_ordersheet_item_planned_id'];
						$planning_warehouse_quantities[$warehouse][$material]['quantity'] = $row['mps_ordersheet_item_planned_quantity'];
						
						if ($show_extended_rows) {
							$sum_planning_quantities[$material] = $sum_planning_quantities[$material] + $row['mps_ordersheet_item_planned_quantity'];
						}
					}
				}
			}
		}
	}
	
	
/*******************************************************************************************************************************************************************************************
	Dataloader
	Order Sheet Items Delivered Quantities
********************************************************************************************************************************************************************************************/

	$distribution_pos_quantities = array();
	$distribution_pos_quantities_not_confirmed = array();
	$distribution_warehouse_quantities = array();
	$distribution_warehouse_quantities_not_confirmed = array();
	$distribution_reserve_quantities = array();
	$distribution_reserve_quantities_not_confirmed = array();
	$distribution_confirmation_dates = array();
	
	$sum_distribution_quantities = array();
	$sum_distribution_quantities_not_confirmed = array();
	
	if ($show_distribution_columns) { 
	
		$result = $model->query("
			SELECT DISTINCT
				mps_ordersheet_item_delivered_id,
				mps_ordersheet_item_delivered_posaddress_id,
				mps_ordersheet_item_delivered_warehouse_id,
				mps_ordersheet_item_delivered_quantity,
				mps_ordersheet_item_delivered_confirmed,
				DATE_FORMAT(mps_ordersheet_item_delivered_confirmed,'%d.%m.%Y') AS confirmed_date,
				mps_ordersheet_item_id,
				mps_ordersheet_item_material_id,
				mps_material_material_planning_type_id,
				mps_material_material_collection_category_id,
				mps_ordersheet_warehouse_stock_reserve
			FROM mps_ordersheet_item_delivered
		")
		->bind(Ordersheet_Item_Delivered::DB_BIND_ORDERSHEET_ITEMS)
		->bind(Ordersheet_Item::DB_BIND_MATERIALS)
		->bind('LEFT JOIN mps_ordersheet_warehouses ON mps_ordersheet_warehouse_id = mps_ordersheet_item_delivered_warehouse_id')
		->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = ".$ordersheet->id)
		->order('mps_ordersheet_item_delivered_confirmed', 'DESC')
		->order('mps_ordersheet_item_delivered.date_created', 'DESC')
		->fetchAll();
	
		if ($result) { 
			
			foreach ($result as $row) {
	
				$id = $row['mps_ordersheet_item_delivered_id'];
				$planning = $row['mps_material_material_planning_type_id'];
				$collection = $row['mps_material_material_collection_category_id'];
				$material = $row['mps_ordersheet_item_material_id'];
				$item = $row['mps_ordersheet_item_id'];
				$pos = $row['mps_ordersheet_item_delivered_posaddress_id'];
				$warehouse = $row['mps_ordersheet_item_delivered_warehouse_id'];
				$quantity = $row['mps_ordersheet_item_delivered_quantity'];
				$timestamp = ($row['confirmed_date']) ? date::timestamp($row['confirmed_date']) : 0;
				
				// build versions data
				if ($timestamp) {
					$distribution_confirmation_dates[$timestamp] = $row['confirmed_date'];
				}
				
				// show quantities on required confirmed date
				if ($version) {
				
					if ($version==$timestamp) {
						
						// pos delivered quantities
						if ($pos) {
							$distribution_pos_quantities[$pos][$material]['id'] = $id;
							$distribution_pos_quantities[$pos][$material]['quantity'] = $quantity;
							$sum_distribution_quantities[$material] = $sum_distribution_quantities[$material] + $quantity;
						}
						
						// warehouse delivered quantities
						if ($warehouse) {
							
							if ($row['mps_ordersheet_warehouse_stock_reserve']) {
								$distribution_reserve_quantities[$material]['id'] = $id;
								$distribution_reserve_quantities[$material]['warehouse'] = $warehouse;
								$distribution_reserve_quantities[$material]['quantity'] = $quantity;
							} else {
								$distribution_warehouse_quantities[$warehouse][$material]['id'] = $id;
								$distribution_warehouse_quantities[$warehouse][$material]['quantity'] = $quantity;
								$sum_distribution_quantities[$material] = $sum_distribution_quantities[$material] + $quantity;
							}
						}
					}
				} 
				else {
					
					// delivered quantities are once confirmed
					// sum all confirmed quantities for and set not confiremd quantities as not confirmed
					if ($ordersheet_items[$planning]['data'][$collection]['data'][$item]['quantity_distributed']) {
							
						if ($row['mps_ordersheet_item_delivered_confirmed']) {
							
							// pos quantities
							if ($pos) {
								$distribution_pos_quantities[$pos][$material]['id'] = $id;
								$distribution_pos_quantities[$pos][$material]['quantity'] = $distribution_pos_quantities[$pos][$material]['quantity'] + $quantity;
								$sum_distribution_quantities[$material] = $sum_distribution_quantities[$material] + $quantity;
							}
							
							// warehouse quantities, only last insertetd
							if ($warehouse && $quantity) {
								if ($row['mps_ordersheet_warehouse_stock_reserve']) {
									$distribution_reserve_quantities[$material]['id'] = $id;
									$distribution_reserve_quantities[$material]['warehouse'] = $warehouse;
									$distribution_reserve_quantities[$material]['quantity'] = $quantity;
								} elseif (!$distribution_warehouse_quantities[$warehouse][$material]) {
									
									$distribution_warehouse_quantities[$warehouse][$material]['id'] = $id;
									$distribution_warehouse_quantities[$warehouse][$material]['quantity'] = $quantity;
									
									if ($show_extended_rows) {
										$sum_distribution_quantities[$material] = $sum_distribution_quantities[$material] + $quantity;
									}
								}
							}

						} else {
							
							// pos quantities
							if ($pos) {
								$distribution_pos_quantities_not_confirmed[$pos][$material]['id'] = $id;
								$distribution_pos_quantities_not_confirmed[$pos][$material]['quantity'] = $quantity;
								$sum_distribution_quantities_not_confirmed[$material] = $sum_distribution_quantities_not_confirmed[$material] + $quantity;
							}
							
							// warehouse quantities
							if ($warehouse) {
								if ($row['mps_ordersheet_warehouse_stock_reserve']) {
									$distribution_reserve_quantities_not_confirmed[$material]['id'] = $id;
									$distribution_reserve_quantities_not_confirmed[$material]['warehouse'] = $warehouse;
									$distribution_reserve_quantities_not_confirmed[$material]['quantity'] = $quantity;
								} elseif (!$distribution_warehouse_quantities[$warehouse][$material]) {
									$distribution_warehouse_quantities_not_confirmed[$warehouse][$material]['id'] = $id;
									$distribution_warehouse_quantities_not_confirmed[$warehouse][$material]['quantity'] = $quantity;
									$sum_distribution_quantities_not_confirmed[$material] = $sum_distribution_quantities_not_confirmed[$material] + $quantity;
								}
							}
						}
					}
					// not confirmed quantities
					else {
						
						// pos quantities
						if ($pos) {
							$distribution_pos_quantities[$pos][$material]['id'] = $id;
							$distribution_pos_quantities[$pos][$material]['quantity'] = $quantity;
							$sum_distribution_quantities[$material] = $sum_distribution_quantities[$material] + $quantity;
						}
						
						// warehouse quantities
						if ($warehouse) {
							if ($row['mps_ordersheet_warehouse_stock_reserve']) {
								$distribution_reserve_quantities[$material]['id'] = $id;
								$distribution_reserve_quantities[$material]['warehouse'] = $warehouse;
								$distribution_reserve_quantities[$material]['quantity'] = $quantity;
							} elseif (!$distribution_warehouse_quantities[$warehouse][$material]) {
								$distribution_warehouse_quantities[$warehouse][$material]['id'] = $id;
								$distribution_warehouse_quantities[$warehouse][$material]['quantity'] = $quantity;
								$sum_distribution_quantities[$material] = $sum_distribution_quantities[$material] + $quantity;
							}
						}
					}
				}
			}
		}
	}
	
/*	
	// confirmed date versions dropdown
	if ($mod_distribution && count($distribution_confirmation_dates) > 1) {
		array_unshift($formFilter, ui::dropdown($distribution_confirmation_dates, array(
			'name' => 'version',
			'id' => 'version',
			'value' => $version,
			'caption' => 'All Delivery Dates'
		)));
	}
*/

	
/*******************************************************************************************************************************************************************************************
	Dataloader
	SAP export warning messages
********************************************************************************************************************************************************************************************/


	// missing
	if ($statment_on_distribution && $sapInvolvedPos && $company->do_data_export_from_mps_to_sap) {
		
		$sapInvolvedPos = join(',',array_keys($sapInvolvedPos));
		
		$result = $model->query("
			SELECT DISTINCT
				posaddress_id,
				posaddress_sapnumber,
				posaddress_sap_shipto_number,
				address_sap_salesorganization
			FROM db_retailnet.posaddresses
			INNER JOIN db_retailnet.addresses ON address_id = posaddress_client_id
			WHERE posaddress_id IN($sapInvolvedPos)
		")->fetchAll();
		
		if ($result) {
			
			foreach ($result as $row) {
					
				$message = null;
				$pos = $row['posaddress_id'];
		
				if (!$row['posaddress_sapnumber']) {
					$message .= "The SAP customer code for this POS location is missing.";
				}
		
				if (!$row['address_sap_salesorganization']) {
					$message .= "<br \>The Sales Organisation for this Client Company is missing.";
				}
					
				if ($message) {
					$message .= "<br \>Export to SAP is not possible.";
					$sapWarningPOS[$pos] = $message;
				}
			}
		}
		
		$result = $model->query("
			SELECT DISTINCT
				posaddress_id,
				sap_imported_posaddress_order_block,
				sap_imported_posaddress_order_block_sales_area,
				sap_imported_posaddress_deletion_indicator,
				sap_imported_posaddress_deletion_sales_area_indicator
			FROM db_retailnet.sap_imported_posaddresses
			INNER JOIN db_retailnet.posaddresses ON posaddress_sapnumber = sap_imported_posaddress_sapnr
			WHERE posaddress_id IN($sapInvolvedPos)
			ORDER BY posaddress_id DESC
		")->fetchAll();
		
		if ($result) {

			$controlled = array();

			foreach ($result as $row) {
				
				$pos = $row['posaddress_id'];				
				
				if (!$controlled[$pos]) {
					
					$controlled[$pos] = true;

					if ($row['sap_imported_posaddress_order_block'] || $row['sap_imported_posaddress_order_block_sales_area'] ) {
						$sapBlockedPOS[$pos] = 'The POS is blocked for orders in SAP.<br \>Export to SAP is not possible.';
					}
					
					if ($row['sap_imported_posaddress_deletion_indicator'] || $row['sap_imported_posaddress_deletion_sales_area_indicator'] ) {
						$sapDeletionPOS[$pos] = 'This POS is closed in SAP..<br \>Export to SAP is not possible.';
					}
				}
			}
		}
		
	}
	
	
	// missing warehouse sap numbers
	if ($statment_on_distribution && $company->do_data_export_from_mps_to_sap) {
		
		$result = $model->query("
			SELECT DISTINCT
				mps_ordersheet_warehouse_id,
				CONCAT('The SAP customer code for warehouse <b>', address_warehouse_name,'</b> is missing.<br>Export to SAP is not possible.') as message
			FROM mps_ordersheet_warehouses
			INNER JOIN db_retailnet.address_warehouses ON address_warehouse_id = mps_ordersheet_warehouse_address_warehouse_id
			WHERE address_warehouse_address_id = $company->id
			AND mps_ordersheet_warehouse_stock_reserve <> 1 
			AND (address_warehouse_sap_customer_number IS NULL OR address_warehouse_sap_customer_number = '')
		")->fetchAll();
		
		$sapWarningWarehouse = _array::extract($result);
	}
	
	
/*******************************************************************************************************************************************************************************************
	SHEET FIXED AREA
	First blank row, planning type captions, material code
********************************************************************************************************************************************************************************************/
	
	$fixed_rows = 3;
	$first_fixed_row = 1;
	$last_fixed_row = 3;
	$fixed_rows_separator = 0;
	$fixed_columns = 2;
	$first_fixed_column = 1;
	$last_fixed_column = 2;
	$fixed_column_separator = 1;
	
	$total_fixed_rows = $fixed_rows + $fixed_rows_separator;
	$total_fixed_columns = $fixed_columns + $fixed_column_separator;
	
/*******************************************************************************************************************************************************************************************
	SHEET CALCULATION AREA
	Total column calculation, item quantities
********************************************************************************************************************************************************************************************/
	
	$sheet_calculation_rows = array();
	$sheet_calculation_rows_separator = ($stock_reserve_edit) ? 1 : 0;
	
	if ($show_calculation_area) {
		
		$sheet_calculation_rows[0] = ($mod_planning) ? "Total Quantity" : 'Total Planned';
		$sheet_calculation_rows[1] = ($mod_planning) ? "Total Planned" : "Total Shipped";

		if ($mod_distribution) {
			$sheet_calculation_rows[2] = "Distributed";
		}
	}
	
	// total calculated rows
	$sheet_calculation_rows_total = ($sheet_calculation_rows) ? count($sheet_calculation_rows) + $sheet_calculation_rows_separator : 0;
	
	// calcullated rows
	if ($sheet_calculation_rows_total) {
		$sheet_calculation_rows_first = $total_fixed_rows + 1;
		$sheet_calculation_rows_last = $sheet_calculation_rows_first + $sheet_calculation_rows_total - 1;
	}
	
	// total planned calculated row
	$rowTotalQuantities = $sheet_calculation_rows_first;
	
	// item quantity row
	$rowPlannedQuantities = $sheet_calculation_rows_first + 1;

	// row distributed
	if ($mod_distribution) {
		$rowDistributedQuantities = $rowPlannedQuantities + 1;
	}
	
	$row_sheet_calculation_rows_separator = ($sheet_calculation_rows_separator) ? $total_fixed_rows + $sheet_calculation_rows_total : null;
	
/*******************************************************************************************************************************************************************************************
	SHEET STOCK RESERVE
	Total column calculation, item quantities
********************************************************************************************************************************************************************************************/
	
	$sheet_stock_reserve = array();
	
	if ($show_reserve_row) {
		
		$sheet_stock_reserve[0] = 'Stock Reserve';
		
		$sheet_stock_reserve_separator = 0;
		$sheet_stock_reserve_total = 1 + $sheet_stock_reserve_separator;
		$sheet_stock_reserve_first = $total_fixed_rows + $sheet_calculation_rows_total + 1;
		$sheet_stock_reserve_last = $sheet_stock_reserve_first + $sheet_stock_reserve_total - 1;
		
		$rowStockReserve = $sheet_stock_reserve_first;
	}
	
/*******************************************************************************************************************************************************************************************
	SPREADSHEED DATAGRID AREA
	POS locations, group by POS types
	Order sheet items, group by material planning types
********************************************************************************************************************************************************************************************/
	
	$i=0;
	
	$sheet_datagrid_rows = array();
	$sheet_datagrid_rows_separator = 0;
	$group = array();
	
	// row group separators
	$rowgroup_separator = 0;
	$total_rowgroup_separators = 0;
	
	if ($pos_locations) {
	
		foreach ($pos_locations as $type => $data) {
	
			$sheet_datagrid_rows[$i]['type'] = $type;
			$sheet_datagrid_rows[$i]['caption'] = htmlspecialchars($data['caption'], ENT_QUOTES);
			$sheet_datagrid_rows[$i]['group'] = true;
	
			$j = $i+1;
	
			foreach ($data['pos'] as $pos => $value) {
	
				$sheet_datagrid_rows[$j]['type'] = $type;
				$sheet_datagrid_rows[$j]['pos'] = $pos;
	
				$caption = $value['name'].' ('.$value['place'].')';
				$caption = str_replace("'", "", $caption);
				$sheet_datagrid_rows[$j]['caption'] = (strlen($caption) > 50) ? substr($caption, 0, 50).'..' : $caption;
				$j++;
			}
	
			$i = $j + $rowgroup_separator;
	
			$total_rowgroup_separators = $total_rowgroup_separators + $rowgroup_separator;
		}
	
		// remove last seperator
		if ($total_rowgroup_separators) $total_rowgroup_separators = $total_rowgroup_separators - 1;
	}
	
	$i=0;
	
	$sheet_datagrid_columns = array();
	$planning_type_captions = array();
	$planning_column_index = 0;
	$item_colum_index = 0;
	
	// row references separator
	$referenced_column_separator = 0;
	
	// column group separators
	$colgroup_separator = 1;
	$total_colgroup_separators = 0;
	
	// column multiplicator
	$multiple = ($mod_distribution) ? 2 : 1;
	
	if ($ordersheet_items && ($show_planning_columns || $mod_distribution)) {
	
		foreach ($ordersheet_items as $planning => $data) {
				
			foreach ($data['data'] as $collection => $value) {
	
				// merge length for planning caption
				$total_item_columns = count($value['data']) * $multiple;
				$total_partial_columns = (!$version && $item_partially_columns[$planning][$collection]) ? count($item_partially_columns[$planning][$collection]) : 0;
				$planning_type_captions[$i]['merge'] = $total_item_columns + $total_partial_columns;
					
				// planning type caption
				$caption_planning = htmlspecialchars($data['caption'], ENT_QUOTES);
				$caption_collection = htmlspecialchars($value['caption'], ENT_QUOTES);
				$planning_type_captions[$i]['caption'] = "<strong>$caption_planning</strong><span>$caption_collection</span>";
	
				foreach ($value['data'] as $item => $row) {
						
					// skip separator columns
					if ($i==$item_colum_index) {
							
						// merge length for item caption
						$merge_length = 1;
						$merge_length = ($mod_distribution) ? 2 : $merge_length;
	
						if (!$version) {
							$merge_length = ($item_partially_columns[$planning][$collection][$row['id']] && $row['quantity_distributed']) ? 3 : $merge_length;
						}
							
						// item caption
						$sheet_datagrid_columns[$i]['merge'] = $merge_length;
						$sheet_datagrid_columns[$i]['code'] = $row['code'];
							
						// next item index
						$item_colum_index = $i+$merge_length;
	
						// planned column
						if ($show_planning_columns) {
							$sheet_datagrid_columns[$i]['key'] = COLUMN_PLANNING;
							$sheet_datagrid_columns[$i]['id'] = $row['id'];
							$sheet_datagrid_columns[$i]['quantity'] = $row['quantity'];
							$sheet_datagrid_columns[$i]['quantity_approved'] = $row['quantity_approved'];
							$sheet_datagrid_columns[$i]['quantity_confirmed'] = $row['quantity_confirmed'];
							$sheet_datagrid_columns[$i]['quantity_shipped'] = $row['quantity_shipped'];
							$sheet_datagrid_columns[$i]['data'] = $planning_pos_quantities;
							$sheet_datagrid_columns[$i]['warehouse'] = $planning_warehouse_quantities;
							$i++;
						}
	
						// delivered column
						if ($mod_distribution) {
							$sheet_datagrid_columns[$i]['key'] = COLUMN_DISTRIBUTION;
							$sheet_datagrid_columns[$i]['id'] = $row['id'];
							$sheet_datagrid_columns[$i]['quantity_approved'] = $row['quantity_approved'];
							$sheet_datagrid_columns[$i]['quantity_confirmed'] = $row['quantity_confirmed'];
							$sheet_datagrid_columns[$i]['quantity_shipped'] = $row['quantity_shipped'];
							$sheet_datagrid_columns[$i]['quantity_distributed'] = $row['quantity_distributed'];
							$sheet_datagrid_columns[$i]['data'] = $distribution_pos_quantities;
							$sheet_datagrid_columns[$i]['warehouse'] = $distribution_warehouse_quantities;
							$i++;
						}
	
						// partiale delivered column
						if (!$version && $item_partially_columns[$planning][$collection][$row['id']]) {
							$sheet_datagrid_columns[$i]['key'] = COLUMN_PARTIALY_CONFIRMED;
							$sheet_datagrid_columns[$i]['id'] = $row['id'];
							$sheet_datagrid_columns[$i]['data'] = $distribution_pos_quantities_not_confirmed;
							$sheet_datagrid_columns[$i]['quantity_confirmed'] = $row['quantity_confirmed'];
							$sheet_datagrid_columns[$i]['quantity_shipped'] = $row['quantity_shipped'];
							$sheet_datagrid_columns[$i]['quantity_distributed'] = $row['quantity_distributed'];
							$sheet_datagrid_columns[$i]['quantity_notused'] = $row['quantity_shipped'] - $row['quantity_distributed'];
							$sheet_datagrid_columns[$i]['warehouse'] = $distribution_warehouse_quantities_not_confirmed;
							$i++;
						}
					}
						
					// next first item index
					$item_colum_index = $i;
				}
	
				// increment
				$i = $i+$colgroup_separator;
	
				// next first item index
				$item_colum_index = $i;
	
				// colgroup separator
				$total_colgroup_separators = $total_colgroup_separators + $colgroup_separator;
			}
		}
	
		// remove last seperator
		if ($total_colgroup_separators) $total_colgroup_separators = $total_colgroup_separators - 1;
	}
	
	// total row references
	$sheet_datagrid_rows_total = ($sheet_datagrid_rows) ? count($sheet_datagrid_rows) + $total_rowgroup_separators + $sheet_datagrid_rows_separator : 0;
	
	// totall column references
	$sheet_datagrid_columns_total = ($sheet_datagrid_columns) ? count($sheet_datagrid_columns) + $total_colgroup_separators + $referenced_column_separator : 0;
	
	// referenced rows
	if ($sheet_datagrid_rows_total) {
		$sheet_datagrid_rows_first = $total_fixed_rows + $sheet_calculation_rows_total + $sheet_stock_reserve_total + 1;
		$sheet_datagrid_rows_last = $sheet_datagrid_rows_first + $sheet_datagrid_rows_total - 1;
	}
	
	// referenced columns
	if ($sheet_datagrid_columns_total) {
		$sheet_datagrid_columns_first = $total_fixed_columns + $total_calcullated_columns + 1;
		$sheet_datagrid_columns_last = $sheet_datagrid_columns_first + $sheet_datagrid_columns_total - 1;
	}
	
/*******************************************************************************************************************************************************************************************
	SHEET POST DATAGRID AREA
	Order sheet warehouses
********************************************************************************************************************************************************************************************/
	
	$sheet_post_datagrid_rows = array();
	$sheet_post_datagrid_rows_separator = 0;
	
	if ($show_extended_rows) {
		
		$sheet_post_datagrid_rows[0] = "Warehouses";
		
		// load all active company warehouses
		$company_warehouses = $ordersheet->company()->warehouse()->load();
		$company_warehouses = _array::datagrid($company_warehouses);
		
		// load all ordersheet registred warehouses
		$ordersheet_warehouses = $ordersheet->warehouse()->load();
		
		if ($ordersheet_warehouses) {
		
			$i = 1;
		
			foreach ($ordersheet_warehouses as $row) {
				
				$warehouse = $row['mps_ordersheet_warehouse_address_warehouse_id'];

				if (!$row['mps_ordersheet_warehouse_stock_reserve']) {
					$sheet_post_datagrid_rows[$i]['id'] = $row['mps_ordersheet_warehouse_id'];
					$sheet_post_datagrid_rows[$i]['name'] = $company_warehouses[$warehouse]['address_warehouse_name'];
					$i++;
				}
			}
		}
	}
	
	$sheet_post_datagrid_rows_total = ($sheet_post_datagrid_rows) ? count($sheet_post_datagrid_rows) + $sheet_post_datagrid_rows_separator : 0;

	// row limits
	if ($sheet_post_datagrid_rows_total) {
		$sheet_post_datagrid_rows_first = $total_fixed_rows + $sheet_calculation_rows_total + $sheet_stock_reserve_total + $sheet_datagrid_rows_total + 1;
		$sheet_post_datagrid_rows_last = $sheet_post_datagrid_rows_first + $sheet_post_datagrid_rows_total - 1;
	}
	
	
/*******************************************************************************************************************************************************************************************
	SPREADSHEED BUILDER
********************************************************************************************************************************************************************************************/

	$sheet_total_rows = $total_fixed_rows + $sheet_calculation_rows_total + $sheet_stock_reserve_total +$sheet_datagrid_rows_total + $sheet_post_datagrid_rows_total;
	$sheet_total_columns = $total_fixed_columns + $sheet_datagrid_columns_total + $total_extended_columns + $total_calcullated_columns;
	
	// spreadsheet fixed area
	if ($stock_reserve_edit) {
		$spreadsheet_fixed_rows = $total_fixed_rows + $total_ + $sheet_calculation_rows_total;
	} else {
		$spreadsheet_fixed_rows = $total_fixed_rows + $total_ + $sheet_calculation_rows_total + $sheet_stock_reserve_total;
	}
	
	$spreadsheet_fixed_columns = ($total_fixed_columns) ? $total_fixed_columns : 1;
	
	// datagrid
	if ($sheet_datagrid_rows_total && $sheet_datagrid_columns_total) {
	
		for ($row=1; $row <= $sheet_total_rows; $row++) {
				
			for ($col=1; $col <= $sheet_total_columns; $col++) {
	
				$index = spreadsheet::key($col, $row);

				switch ($row) {
					
					case 1;
						if ($col >= $sheet_datagrid_columns_first) {
							$j = $col-$sheet_datagrid_columns_first;
							$grid[$index]['cls'] = ($sheet_datagrid_columns[$j]) ? 'row-separator' : 'row-separator col-separator';
						}
						
						$grid[$index]['html'] = "&nbsp;";
						$grid[$index]['readonly'] = CELL_READONLY;
					break;
					
					// CAPTIONS
					case 2:
	
						if ($col==2) {
							$mergecel[$index] = $total_fixed_columns;
							$grid[$index]['html'] = ($filer_labels) ? 'Filter(s): '.join(', ', $filer_labels) : null;
							$grid[$index]['cls'] = "filters";
						}
						elseif ($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last) {
							
							$i = $col-$sheet_datagrid_columns_first;
							$caption = $planning_type_captions[$i]['caption'];
							$class_name = ($planning_type_captions[$i]['caption']) ? 'col-caption cel' : 'col-separator';
							
							if ($caption) {
								$mergecel[$index] = $planning_type_captions[$i]['merge'];
								$grid[$index]['html'] = $caption;
							}
							
							$grid[$index]['cls'] = $class_name;
						}
						
						$grid[$index]['readonly']  = CELL_READONLY;
							
					break;
					
					// ITEM CODE
					case 3: 
						
						if ($col==1) {
							$mergecel[$index] = $fixed_columns;
						}
						elseif ($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last) {
							
							$i = $col - $sheet_datagrid_columns_first;
							
							$code = $sheet_datagrid_columns[$i]['code'];
							$item_id = $sheet_datagrid_columns[$i]['id'];
							$merge_length = $sheet_datagrid_columns[$i]['merge'];
							
							if ($code) {
					
								$icon_help = "<span class=icon-holder ><span class=\"icon info-full-circle infotip \" rel=\"material-info\" data=\"$item_id\" ></span></span>";
							
								$grid[$index]['html'] = "$icon_help<span class=\"code\">$code</span>";
								$grid[$index]['cls'] = "cel item-caption";
									
								// merge item label
								if ($merge_length>1) $mergecel[$index] = $merge_length;
							
							}
							
							if (!$sheet_datagrid_columns[$i]) {
								$grid[$index]['cls'] = 'col-separator';
								$separated_active = true;
							}
						}
						
						$grid[$index]['readonly']  = CELL_READONLY;
	
					break;
					
					// TOTAL QUANTITY
					case $rowTotalQuantities:
						
						$i = $row-$sheet_calculation_rows_first;
						$j = $col-$sheet_datagrid_columns_first;
						
						if ($col==2) {
							
							$caption = $sheet_calculation_rows[$i];
							$class_cell_extended = ($mod_distribution) ? 'planned-readonly' : null;

							$grid[$index]['html'] = $caption;
							$grid[$index]['cls'] = "calcullated-caption $class_cell_extended";
						}
						elseif ($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last) {
							
							if ($sheet_datagrid_columns[$j]) {
								
								$key = $sheet_datagrid_columns[$j]['key'];
								$next_key = $sheet_datagrid_columns[$j+1]['key'];
								$material = $sheet_datagrid_columns[$j]['id'];
								

								// column planning
								if ($key==COLUMN_PLANNING) {
									
									if ($mod_distribution) {
										
										$class_cell = 'cel';
										$class_cell_extended = 'planned-readonly';

										// cel value: total submittetd quantity before shipping
										$grid[$index]['text']  = $sheet_datagrid_columns[$j]['quantity_approved'];

									} else {

										// cel value: total planned + stock reserve
										$celTotalPlanned = spreadsheet::key($col, $rowPlannedQuantities);
										$celStockReserve = spreadsheet::key($col, $rowStockReserve);
										$grid[$index]['text'] = "function($celTotalPlanned, $celStockReserve) { return $celTotalPlanned+$celStockReserve || null; }";
										
										$class_cell = 'cel';
										$class_cell_extended = 'calculated';
									}
								}								
								// colum distribution
								elseif ($key==COLUMN_DISTRIBUTION) {

									$class_cell = null;
									
									// delivered quantities
									if ($distrubuted_items[$material] || $version || $next_key==COLUMN_PARTIALY_CONFIRMED) {
										$class_cell_extended = null;
										$grid[$index]['text'] = $sheet_datagrid_columns[$j]['quantity_distributed'];
									} 
									// sum distributed quantites (pos + warehouses)
									else {
										$range = spreadsheet::key($col,$sheet_datagrid_rows_first).'_'.spreadsheet::key($col,$sheet_post_datagrid_rows_last);
										$grid[$index]['text']  = "function($range) { total = sum($range); return total ? total : null; }";
										$class_cell_extended = 'calculated';
									}

									$class_cell_extended = $class_cell_extended.' invisible';
								}
								// column partially distribution
								elseif ($key==COLUMN_PARTIALY_CONFIRMED) {
									
									$class_cell = null;
									
									// sum distributed quantities (pos + warehouses)
									$range = spreadsheet::key($col,$sheet_datagrid_rows_first).'_'.spreadsheet::key($col,$sheet_post_datagrid_rows_last);
									$grid[$index]['text']  = "function($range) { total = sum($range); return total ? total : null; }";
									$class_cell_extended = 'calculated invisible';
								}

							} else {
								$class_cell = 'col-separator';
								$class_cell_extended = null;
							}

							$grid[$index]['cls'] = "$class_cell $class_cell_extended";
						}
						
						$grid[$index]['readonly']  = CELL_READONLY;
						
					break;
					
					// PLANNED/SHIPPED QUANTITIES
					case $rowPlannedQuantities:
						
						$i = $row-$sheet_calculation_rows_first;
						$j = $col-$sheet_datagrid_columns_first;
						
						if ($col==2) {
							$caption = $sheet_calculation_rows[$i];
							$grid[$index]['html'] = $caption;
							$grid[$index]['cls'] = 'calcullated-caption';
						}
						elseif ($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last) {
							
							if ($sheet_datagrid_columns[$j]) {
								
								$key = $sheet_datagrid_columns[$j]['key'];
								$next_key = $sheet_datagrid_columns[$j+1]['key'];
								$material = $sheet_datagrid_columns[$j]['id'];
								
								$class_cell_extended = null;
								$class_cell = null;

								// column planning
								if ($key==COLUMN_PLANNING) {
									
									$class_cell = 'cel';
									
									if ($mod_distribution) {
										
										// shiped quantities
										$grid[$index]['text'] = $sheet_datagrid_columns[$j]['quantity_shipped'];
										
										$class_cell_extended = 'planned-readonly';
									} else {
										
										$class_cell_extended = 'quantity';

										// sum planned quantities (pos + warehouses)
										$range = spreadsheet::key($col,$sheet_datagrid_rows_first).'_'.spreadsheet::key($col,$sheet_post_datagrid_rows_last);
										$grid[$index]['text']  = "function($range) { return sum($range) || null }";
									}
								} 
								else {
									
									// ivisible sum planned quantities (pos+warehouse)
									if ($key==COLUMN_DISTRIBUTION) {
										$range = spreadsheet::key($col,$sheet_datagrid_rows_first).'_'.spreadsheet::key($col,$sheet_post_datagrid_rows_last);
										$grid[$index]['text']  = "function($range) { return sum($range) || null }";
									} else {
										$celShippedQuantities = spreadsheet::key($col-2,$rowPlannedQuantities);
										$celDeliveredQuantities = spreadsheet::key($col-1,$rowDistributedQuantities);
										$grid[$index]['text']  = "function($celShippedQuantities, $celDeliveredQuantities) { return $celShippedQuantities-$celDeliveredQuantities|| null }";
									}
									
									$class_cell = null;
									$class_cell_extended = 'invisible';
								}
																				
							} else {
								$class_cell = 'col-separator';
								$class_cell_extended = null;
							}
							
							$grid[$index]['cls'] = "$class_cell $class_cell_extended";
						}
						
						$grid[$index]['readonly']  = CELL_READONLY;
						
					break;

					// DISTRIBUTED QUANTITIES
					case $rowDistributedQuantities:

						$i = $row-$sheet_calculation_rows_first;
						$j = $col-$sheet_datagrid_columns_first;
						
						if ($col==2) {
							$caption = $sheet_calculation_rows[$i];
							$grid[$index]['html'] = $caption;
							$grid[$index]['cls'] = 'calcullated-caption';
						}
						elseif ($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last) {

							if ($sheet_datagrid_columns[$j]) {
								
								$key = $sheet_datagrid_columns[$j]['key'];
								$next_key = $sheet_datagrid_columns[$j+1]['key'];
								$material = $sheet_datagrid_columns[$j]['id'];
								
								$class_cell_extended = null;
								$class_cell = null;

								if ($key==COLUMN_DISTRIBUTION && $sheet_datagrid_columns[$j]['quantity_distributed']) {
									$class_cell = 'cel';
									$class_cell_extended = 'delivered-readonly';
									$grid[$index]['text'] = $sheet_datagrid_columns[$j]['quantity_distributed'];
								} else {
									$class_cell = null;
									$class_cell_extended = null;
								}							
							}
							else {
								$class_cell = 'col-separator';
								$class_cell_extended = null;
							}

							$grid[$index]['cls'] = "$class_cell $class_cell_extended";
						}

						$grid[$index]['readonly']  = CELL_READONLY;

					break;
					
					// STOCK RESERVE
					case $rowStockReserve;
					
						$i = $row-$sheet_stock_reserve_first;
						$j = $col-$sheet_datagrid_columns_first;
					
						if ($col==2) {
							$caption = $sheet_stock_reserve[$i];
							$grid[$index]['html'] = $caption;
							$grid[$index]['cls'] = "calcullated-caption";
							$grid[$index]['readonly']  = CELL_READONLY;
						}
						elseif($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last) {
							
							if ($sheet_datagrid_columns[$j]) {
								
								$key = $sheet_datagrid_columns[$j]['key'];
								$next_key = $sheet_datagrid_columns[$j+1]['key'];
								$material = $sheet_datagrid_columns[$j]['id'];
								
								$class_cell = null;
								$class_cell_extended = null;
								
								// column planning
								if ($key==COLUMN_PLANNING) {
									
									$class_cell = 'cel';
									
									if ($mod_distribution) {
										
										// ???
										$grid[$index]['text'] = $planning_reserve_quantities[$material]['quantity'];
										$class_cell_extended = 'planned-readonly';
									}
									else {
										
										$id = $planning_reserve_quantities[$material]['id'];
										
										// edit stock reserve
										if ($stock_reserve_edit) {
											
											$warehouse = ($planning_reserve_quantities[$material]['warehouse']) ? $planning_reserve_quantities[$material]['warehouse'] : $stockReserveWarehouseID;
											$quantity = $planning_reserve_quantities[$material]['quantity'];
											$grid[$index]['text'] = $quantity;
											$grid[$index]['tag'] = "reserve-planned;$id;$warehouse;$material";
										} 
										// stock reserve: totalQuantities - plannedQuantities
										else {
											
											$grid[$index]['text'] = $planning_reserve_quantities[$material]['quantity'];
										}
									
										$class_cell_extended = ($mod_distribution) ? 'planned-readonly' : 'reserve';
									}
								}
								// colum distribution
								elseif ($key==COLUMN_DISTRIBUTION && $next_key<>COLUMN_PARTIALY_CONFIRMED) {
									
									if (!$distrubuted_items[$material]) {
										$class_cell = 'cel';
										$class_cell_extended = ($distrubuted_items[$material]) ? 'delivered-readonly' : null;
									}

									$celTotalQuantities = spreadsheet::key($col,$rowTotalQuantities);
									$celShippedQuantities = spreadsheet::key($col-1,$rowPlannedQuantities);
									$grid[$index]['text']  = "function($celShippedQuantities, $celTotalQuantities) { return $celShippedQuantities-$celTotalQuantities || null } ";
								}
								// column partially distribution
								elseif ($key==COLUMN_PARTIALY_CONFIRMED) {
									$class_cell = 'cel';
									$class_cell_extended = 'reserve';
									$celTotalQuantities = spreadsheet::key($col,$rowTotalQuantities);
									$celShippedQuantities = spreadsheet::key($col-2,$rowPlannedQuantities);
									$celDistributedQuantities = spreadsheet::key($col-1,$rowDistributedQuantities);
									$grid[$index]['text']  = "function($celShippedQuantities, $celDistributedQuantities, $celTotalQuantities) { return $celShippedQuantities-$celDistributedQuantities-$celTotalQuantities || null; } ";
								}
								
								if (!$stock_reserve_edit) {
									// IMPORTANT: don't set stock reserve as readonly, this sel will be reduced from planning quantities (readonly cel can't be changed) 
									//$grid[$index]['readonly']  = CELL_READONLY;
								}
								
							} else {
								$class_cell = 'row-seperator col-separator';
								$class_cell_extended = null;
								$grid[$index]['readonly']  = CELL_READONLY;
							}
							
							$grid[$index]['cls'] = "$class_cell $class_cell_extended";
							
						} else {
							$grid[$index]['readonly']  = CELL_READONLY;
						}
						
					break;
					
					// REFERENCES
					case $row >= $sheet_datagrid_rows_first && $row <= $sheet_datagrid_rows_last :
						
						// icon row
						if ($col==1) {
							
							$i = $row-$sheet_datagrid_rows_first;
							$type = $sheet_datagrid_rows[$i]['type'];
							$pos = $sheet_datagrid_rows[$i]['pos'];
							$group = $sheet_datagrid_rows[$i]['group'];
							$caption = htmlspecialchars($sheet_datagrid_rows[$i]['caption']);
							
							// pos type caption
							if ($group) {
								$grid[$index]['html'] = "<span class=\"arrow arrow_$type arrow-down type_$type\"  type=\"$type\" row=\"$row\" ></span>";
								$grid[$index]['cls'] = "cel postype row-caption";
							} 
							// pos info bouble box
							else {
								$grid[$index]['html'] = "<span class=\"icon info-full-circle infotip \" rel=\"pos-info\" data=\"$pos\" ></span>";
								$grid[$index]['cls'] = "cel";
							}

							$grid[$index]['readonly']  = CELL_READONLY;
						}
						// pos caption
						elseif ($col==2) {
							
							$i = $row-$sheet_datagrid_rows_first;
							$type = $sheet_datagrid_rows[$i]['type'];
							$pos = $sheet_datagrid_rows[$i]['pos'];
							$caption = htmlspecialchars($sheet_datagrid_rows[$i]['caption']);
							$group = $sheet_datagrid_rows[$i]['group'];
							$data = ($group) ? $sheet_datagrid_rows[$i]['group'] : $pos;
							
							if ($sapDeletionPOS[$pos]) $warningIcon = "<i class=\"fa fa-ban infotip warrning\" data-title=\"".$sapDeletionPOS[$pos]."\"></i>";
							elseif ($sapBlockedPOS[$pos]) $warningIcon = "<i class=\"fa fa-ban infotip warrning\" data-title=\"".$sapBlockedPOS[$pos]."\"></i>";
							else $warningIcon = ($sapWarningPOS[$pos]) ? "<i class=\"fa fa-exclamation-triangle infotip warrning\" data-title=\"".$sapWarningPOS[$pos]."\"></i>" : null;
							
							// grid properties
							if ($group) {
								$grid[$index]['html'] = "<span class=\"caption type_$type\" type=\"$type\" row=\"$row\" >$caption</span>";
								$grid[$index]['cls'] = "cel postype row-caption";
							} else {
								$grid[$index]['html'] = "<span class=\"caption type_$type\" type=\"$type\" row=\"$row\" >$caption $warningIcon</span>";
								$grid[$index]['cls'] = "cel postype";
							}

							$grid[$index]['readonly']  = CELL_READONLY;
						}
						// datagrid
						elseif ($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last) {
							
							$i = $row - $sheet_datagrid_rows_first;
							$j = $col - $sheet_datagrid_columns_first;
							$group = $sheet_datagrid_rows[$i]['group'];

							if ($sheet_datagrid_columns[$j]) {
								
								$key = $sheet_datagrid_columns[$j]['key'];
								$next_key = $sheet_datagrid_columns[$j+1]['key'];
								$pos = $sheet_datagrid_rows[$i]['pos'];
								$material = $sheet_datagrid_columns[$j]['id'];
								
								// class cell
								$class_cell = ($group) ? 'row-separator' : 'cel';
																										
								if ($mod_distribution) {
									
									if ($key==COLUMN_PLANNING) $class_cell_extended = 'planned-readonly';
									elseif($version && $key==COLUMN_DISTRIBUTION) $class_cell_extended = 'delivered-readonly';
									else $class_cell_extended = ( $key==COLUMN_DISTRIBUTION && ($next_key==COLUMN_PARTIALY_CONFIRMED || $distrubuted_items[$material]) ) ? 'delivered-readonly' : null;
	
									$cel_editabled = ($class_cell_extended) ? false : true;
									
								} else {
									$class_cell_extended = null;
									$cel_editabled = true;
								}

								$grid[$index]['text'] = $sheet_datagrid_columns[$j]['data'][$pos][$material]['quantity'];
								$grid[$index]['hiddenAsNull'] = 'true';
								$grid[$index]['numeric'] = 'true';
							
								// onChange cel control
								if (!$group && $cel_editabled) {
									
									$id = $sheet_datagrid_columns[$j]['data'][$pos][$material]['id'];
									
									if ($key==COLUMN_DISTRIBUTION) {
										$celTotal = spreadsheet::key($col-1, $rowPlannedQuantities);
										$celPlanned = spreadsheet::key($col, $rowPlannedQuantities);
									} else {
										
										$celTotal = spreadsheet::key($col, $rowTotalQuantities);
										$celPlanned = spreadsheet::key($col, $rowPlannedQuantities);
									}
									
									$celReserve = ($key==COLUMN_PLANNING && !$stock_reserve_edit) ? spreadsheet::key($col, $rowStockReserve) : null;
									
									// TAG: [section] [item] [pos] [material] [total] [planned] [reserve]
									$grid[$index]['tag'] = "$key;$id;$pos;$material;$celTotal;$celPlanned;$celReserve";
								}
							}
							else {
								$class_cell = ($group) ? 'row-separator col-separator' : 'col-separator';
								if ($group && $class_cell) $grid[$index]['html'] = '&nbsp;';
							}
							
							$grid[$index]['cls'] = "$class_cell $class_cell_extended";
							$grid[$index]['readonly'] = ($cel_editabled && !$group) ? $spreadsheet_attribute_readonly : 'true';
						}
						else {
							$grid[$index]['readonly']  = CELL_READONLY;
						}
	
					break;
					
							
					// WAREHOUSE GROUP CAPTION
					case $sheet_post_datagrid_rows_first:
						
						if ($col==1) {
							if ($mod_readonly) {
								$grid[$index]['html'] = htmlspecialchars($sheet_post_datagrid_rows[0]);
								$mergecel[$index] = 2;
							} else {
								$grid[$index]['html'] = "<a id=\"add_warehouse\" class=\"icon add modal-box\" href=\"#add_warehouse_dialog\" ></a>";
							}
							
							$grid[$index]['cls'] = "cel row-caption";
						}
						elseif (!$mod_readonly && $col==2) {
							$i = $col - $sheet_datagrid_columns_first;
							$grid[$index]['html'] = htmlspecialchars($sheet_post_datagrid_rows[0]);
							$grid[$index]['cls'] = ($material) ? 'cel row-caption' : null;
						}
						elseif ($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last) {
							
							$i = $col - $sheet_datagrid_columns_first;
							$material = $sheet_datagrid_columns[$i]['id'];
							
							$row_separator = ($sheet_post_datagrid_rows_total > 1) ? 'row-separator' : null;
							$col_separator = ($row_separator) ? 'col-separator' : null; 
							$grid[$index]['cls'] = ($material) ? $row_separator : "$row_separator $col_separator";
							
							if (!$material) $grid[$index]['html'] = '&nbsp;';
						}
						
						$grid[$index]['readonly']  = CELL_READONLY;
						
					break;
					
					
					// WAREHOUSES
					case $row > $sheet_post_datagrid_rows_first && $row <= $sheet_post_datagrid_rows_last :
						
						$i = $row-$sheet_post_datagrid_rows_first;
						$j = $col-$sheet_datagrid_columns_first;
						
						if ($col==1) {
							if ($mod_readonly) {
								$grid[$index]['html'] = htmlspecialchars($sheet_post_datagrid_rows[$i]['name']);
								$mergecel[$index] = 2;
							} else {
								
								// stock reserve cannot be deleted
								if (!$sheet_post_datagrid_rows[$i]['reserve']) {
									$warehose_id = $sheet_post_datagrid_rows[$i]['id'];
									$grid[$index]['html'] = "<span class=\"icon cancel remove-warehouse\" row=$row data=\"$warehose_id\"  ></span>";
								}
							}
							
							$grid[$index]['cls'] = "cel warehouse";
							$grid[$index]['readonly']  = CELL_READONLY;
						}
						elseif (!$mod_readonly && $col==2) {			
							
							$warehouse_id = $sheet_post_datagrid_rows[$i]['id'];
							$warningIcon = ($sapWarningWarehouse[$warehouse_id]) ? "<i class=\"fa fa-exclamation-triangle infotip warrning\" data-title=\"".$sapWarningWarehouse[$warehouse_id]."\"></i>" : null;
							
							$grid[$index]['html'] = htmlspecialchars($sheet_post_datagrid_rows[$i]['name']).$warningIcon; 
							$grid[$index]['cls'] = "cel warehouse";
							$grid[$index]['readonly']  = CELL_READONLY;
						}
						elseif ($col >= $sheet_datagrid_columns_first && $col <= $sheet_datagrid_columns_last && $sheet_post_datagrid_rows_total > 1) {
							
							if ($sheet_datagrid_columns[$j]) {
								
								$warehouse_id = $sheet_post_datagrid_rows[$i]['id'];
								$material = $sheet_datagrid_columns[$j]['id'];
								$key = $sheet_datagrid_columns[$j]['key'];
								$next_key = $sheet_datagrid_columns[$j+1]['key'];
																										
								if ($mod_distribution) {
									
									if ($key==COLUMN_PLANNING) $class_cell_extended = 'planned-readonly';
									elseif( ($version && $key==COLUMN_DISTRIBUTION) || $distrubuted_items[$material] ) $class_cell_extended = 'delivered-readonly';
									else $class_cell_extended = ( $key==COLUMN_DISTRIBUTION && ($next_key==COLUMN_PARTIALY_CONFIRMED || $distrubuted_items[$material]) ) ? 'delivered-readonly' : null;
									
									$cel_editabled = ($class_cell_extended) ? false : true;
								} else {
									$cel_editabled = true;
								}
								
								$class_cell = ($cel_editabled) ? 'cel reserve' : 'cel';
								
								$grid[$index]['text'] = ($key==COLUMN_DISTRIBUTION && $distrubuted_items[$material] && !$version) ? null : $sheet_datagrid_columns[$j]['warehouse'][$warehouse_id][$material]['quantity'];
								$grid[$index]['hiddenAsNull'] = 'true';
								$grid[$index]['numeric'] = 'true';
							
								if ($cel_editabled ) {
									
									$id = $sheet_datagrid_columns[$j]['warehouse'][$warehouse_id][$material]['id'];
									
									if ($key==COLUMN_DISTRIBUTION) {
										$celShippedQuantities = spreadsheet::key($col-1, $rowPlannedQuantities);
										$celPlannedQuantities = spreadsheet::key($col, $rowPlannedQuantities);
									} else {
										$celShippedQuantities = spreadsheet::key($col, $rowPlannedQuantities);
										$celPlannedQuantities = spreadsheet::key($col, $rowTotalQuantities);
									}
									
									$grid[$index]['tag'] = "reserve-$key;$id;$warehouse_id;$material;$celShippedQuantities;$celPlannedQuantities";
								}
							}
							else {
								$class_cell = 'col-separator';
							}
							
							$grid[$index]['cls'] = "$class_cell $class_cell_extended";
							$grid[$index]['readonly'] = ($sheet_datagrid_columns[$j] && $cel_editabled) ? $spreadsheet_attribute_readonly : 'true';
						}
						else {
							$grid[$index]['readonly']  = CELL_READONLY;
						}
						
					break;
					
					default:
						$grid[$index]['readonly']  = CELL_READONLY;
						//$grid[$index]['html'] = "&nbsp;";
					break;

				}
			}
		}	
	}

	if (!headers_sent()) {
		header('Content-Type: text/json');
	}

	echo json_encode(array(
		'response' => true,
		'data' => $grid,
		'test' => $test,
		'merge' => $mergecel,
		'top' => $spreadsheet_fixed_rows,
		'left' => $spreadsheet_fixed_columns
	));
