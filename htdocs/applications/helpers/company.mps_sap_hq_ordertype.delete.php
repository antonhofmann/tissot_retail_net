<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];


	$mps_sap_hq_ordertypes = new Company_Mps_sap_hq_ordertype();
	$mps_sap_hq_ordertype = $mps_sap_hq_ordertypes->read($id);

		
	if ($mps_sap_hq_ordertype["mps_sap_hq_ordertype_id"] && (user::permission(Company::PERMISSION_EDIT))) {
		
		$company = $mps_sap_hq_ordertype["mps_sap_hq_ordertype_address_id"];
		$response = $mps_sap_hq_ordertypes->delete();
		
		if ($response) {
			Message::request_deleted();
			url::redirect("/$application/$controller/$action/$company");
		} else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$company/$id");
		}
		
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller");
	}