<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// request vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	// mastersheet vars
	$mastersheet = $_REQUEST['mps_mastersheet_id'];
	$selected_ordersheets = $_REQUEST['selected_companies'];
	$selected_items = $_REQUEST['selected_items'];
	$proposed_quantities = $_REQUEST['proposed'];

	if ($selected_ordersheets && $selected_items) {
		
		$ordersheets = array_filter(explode(',', $selected_ordersheets));
		
		$items = array_filter(explode(',', $selected_items));
		
		// get mastersheet items
		$model = new Model($application);
		
		// master sheet items
		$mastersheet_items = $model->query("
			SELECT DISTINCT
				mps_material_id,
				mps_material_price, 
				currency_id, 
				currency_exchange_rate, 
				currency_factor
			FROM mps_mastersheet_items
		")
		->bind(Mastersheet_Item::DB_BIND_MATERIALS)
		->bind(Material::DB_BIND_CURRENCIES)
		->filter('mastersheet', "mps_mastersheet_item_mastersheet_id = $mastersheet")
		->fetchAll();
		
		$data = array();
		
		if ($mastersheet_items) {
			
			// set order sheet in revision state
			$set_in_revision = false;
			
			// ordersheet builder
			$ordersheet = new Ordersheet($application);
			
			// for each selected order sheet
			// create or update items
			foreach ($ordersheets as $id) {
				
				$material_keys = array();
				
				// read order sheet instance
				$ordersheet->read($id);
				
				// get order sheet items
				$ordersheet_items = $ordersheet->item()->load();
				
				// gruop material keys
				if ($ordersheet_items) {
					foreach ($ordersheet_items as $key => $value) {
						$material = $value['mps_ordersheet_item_material_id'];
						$material_keys[$material] = $key;
					}
				}
				
				// get order sheet materials ids
				$ordersheet_materials = _array::extract_by_key($ordersheet_items, 'mps_ordersheet_item_material_id');
				
				// for eache master sheet item
				// save item in order sheet items
				foreach ($mastersheet_items as $item) { 
					
					$material = $item['mps_material_id']; 
					
					// is this item selected from user
					if (in_array($material, $items)) { 
						
						// item exist in order sheet
						if ($material_keys[$material]) { 
							
							// if proposed quantiti is not null update this item
							if ($proposed_quantities[$material]) {
								
								// read item instance
								$ordersheet->item()->read($material_keys[$material]);
								
								$ordersheet->item()->update(array(
									'mps_ordersheet_item_quantity_proposed' => $proposed_quantities[$material],
									'user_modified' => $user->login,
									'date_modified' => date('Y-m-d H:i:s')
								));
							}
						}
						// create new item for this order sheet
						else {
							
							$ordersheet->item()->create(array(
								'mps_ordersheet_item_ordersheet_id' => $id,
								'mps_ordersheet_item_material_id' => $material,
								'mps_ordersheet_item_price' => $item['mps_material_price'],
								'mps_ordersheet_item_currency' => $item['currency_id'],
								'mps_ordersheet_item_exchangrate' => $item['currency_exchange_rate'],
								'mps_ordersheet_item_factor' => $item['currency_factor'],
								'mps_ordersheet_item_quantity_proposed' => $proposed_quantities[$material] ?: null,
								'mps_ordersheet_item_status' => 1,
								'user_created' => $user->login,
								'date_created' => date('Y-m-d H:i:s')
							));
							
							$set_in_revision = true;
						}
					}
				}
				
				// if order sheet item structure is changed
				// and order sheet is approved or completed
				// set this order sheet in revision statemant
				if ($set_in_revision && ($ordersheet->state()->isApproved() || $ordersheet->state()->isCompleted()) ) {
					$ordersheet->update(array(
						'mps_ordersheet_workflowstate_id' => Workflow_State::STATE_REVISION		
					));
				}
			}
		}
		
		$response = true;
		$message = $translate->message_request_saved;
	} 
	else {
		$response = false;
		$message =$translate->message_request_failure;
	}
	
	$test = ($test) ? join(',',$test) : 'No action';
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'tab_items' => true,
		'reset' => false
	));
	