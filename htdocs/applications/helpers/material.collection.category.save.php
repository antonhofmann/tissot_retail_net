<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['mps_material_collection_category_id'];
	
	$data = array();
	
	if ($_REQUEST['mps_material_collection_category_code']) {
		$data['mps_material_collection_category_code'] = $_REQUEST['mps_material_collection_category_code'];
	}
	
	if (isset($_REQUEST['mps_material_collection_category_name'])) {
		$data['mps_material_collection_category_name'] = $_REQUEST['mps_material_collection_category_name'];
	}
	
	if (in_array('mps_material_collection_category_active', $fields)) {
		$data['mps_material_collection_category_active'] = ($_REQUEST['mps_material_collection_category_active']) ? 1 : 0;
	}
	
	if ($data) {
		
		$material_collection_category = new Material_Collection_Category($application);
		$material_collection_category->read($id);
		
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $material_collection_category->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $material_collection_category->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));