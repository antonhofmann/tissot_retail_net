<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['ordersheet'];
	$version = $_REQUEST['version'];
	
	// order sheet
	$ordersheet = new Ordersheet($application);
	$ordersheet->read($id);
	
	// order sheet version
	$ordersheet->version()->read($version);
	
	if ($ordersheet->version()->id && ($ordersheet->state()->mannager || $ordersheet->state()->administrator)) {
		
		// delete order sheet version
		$response = $ordersheet->version()->delete();
		
		if ($response) {
			
			// delete order sheet version items
			$ordersheet->version()->item()->deleteAll();

			message::request_deleted();
			url::redirect("/$application/$controller/$action/$id");
			
		} else {
			message::request_failure();
			url::redirect("/$application/$controller/$action/$id/$version");
		}
		
	}
	else {
		message::access_denied();
		url::redirect("/$application/$controller");
	}