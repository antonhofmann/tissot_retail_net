<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

	$permission_view = user::permission(Red_Project::PERMISSION_VIEW_PROJECTS);
	$permission_view_limited = user::permission(Red_Project::PERMISSION_VIEW_LIMITED_PROJECTS);

	// db model
	$model = new Model($application);

	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "red_project_title";
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;

	$bindTables = array(
		Red_Project::BIND_PROJECT_TYPES,
		Red_Project::BIND_PROJECT_STATES,
		'INNER JOIN db_retailnet.users leaders ON leaders.user_id = red_projects.red_project_leader_user_id',
		'LEFT JOIN db_retailnet.users assistants ON assistants.user_id = red_projects.red_project_assistant_user_id'
	);

	// define filters
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search ) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			red_project_title LIKE \"%$keyword%\"
			OR red_project_projectnumber LIKE \"%$keyword%\"
		)";
	}

	if ($_REQUEST['project_types']) {
		$filters['project_types'] = 'red_project_projecttype_id = '.$_REQUEST['project_types'];
	}
	
	if ($_REQUEST['project_states']) {
		$filters['project_states'] = 'red_project_projectstate_id = '.$_REQUEST['project_states'];
	}

	// show only active projects or archived projects resp.
	if (!$archived) {
		$filters['red_project_enddate'] = "(
			red_project_enddate = '0000-00-00' 
			OR red_project_enddate = '' 
			OR red_project_enddate IS NULL
		)";
	}
	else {
		$filters['red_project_enddate'] = "(
			red_project_enddate <> '0000-00-00' 
			AND red_project_enddate <> '' 
			AND red_project_enddate IS NOT NULL
		)";
	}
	
	// datagrid
	$result = $model->query("
	  SELECT SQL_CALC_FOUND_ROWS
		red_project_id,
		red_project_title,
		red_project_projectnumber,
		red_project_projecttype_id,
		red_project_projectstate_id,
		DATE_FORMAT(red_project_startdate, '%d.%m.%Y') as red_project_startdate,
		DATE_FORMAT(red_project_foreseen_enddate, '%d.%m.%Y') as red_project_foreseen_enddate,
		red_project_enddate,
		red_projecttype_name,
		red_projectstate_name,
		CONCAT(leaders.user_name, ', ', leaders.user_firstname) AS red_project_leader_user_id,
		CONCAT(assistants.user_name, ', ', assistants.user_firstname) AS red_project_assistant_user_id,
		red_projectstate_icon
	  FROM red_projects
	")
	->bind($bindTables)
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();

	$totalrows = $model->totalRows();

	// show only projects where user is partner of it
	if ($result && (!$permission_view && $permission_view_limited)) {
		
		foreach ($result as $row) {
			
			$project = new Red_Project($application);
			$project->read($row['red_project_id']);

			if ($project->user_is_partner($user->data['user_id'])) {
				$filtered_results[] = $row;
			}
		}
		
		$result = $filtered_results;
	}

	if ($result) {
		
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));

		$pageIndex = $pager->index();
		$pageControlls = $pager->controlls();

		foreach ($result as $row) {
			$id = $row['red_project_id'];
			$datagrid[$id] = $row;
		}
	}

	// toolbox hidden fields
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

	// toolbox: button print
	if ($datagrid && $_REQUEST['print']) {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'label' => $translate->print,
			'href' => 	$_REQUEST['print']	
		));
	} 
	
	// toolbox: button add
	if ($_REQUEST['add']) {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'label' => $translate->add_new,
			'href' => 	$_REQUEST['add']	
		));
	}

	// toolbox: search full text
	$toolbox[] = ui::searchbox();

	// toolbox: Project Types
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS
			red_projecttype_id,
			red_projecttype_name
		FROM red_projecttypes
	")->fetchAll();

	$toolbox[] = ui::dropdown($result, array(
		'name' => 'project_types',
		'id' => 'projecttypes',
		'value' => $_REQUEST['project_types'],
		'class' => 'submit',
		'caption' => 'All Types'
	));
	
	// toolbox: Project States
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS
			red_projectstate_id,
			red_projectstate_name
		FROM red_projectstates
	")->fetchAll();

	$toolbox[] = ui::dropdown($result, array(
		'name' => 'project_states',
		'id' => 'projectstates',
		'value' => $_REQUEST['project_states'],
		'class' => 'submit',
		'caption' => 'All States'
	));


	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}

	$table = new Table(array(
		'sort' => array(
			'column' => $sort,
			'direction' => $direction
		)
	));

	if (is_array($datagrid)) {
		foreach ($datagrid as $key => $value) {
			$icon = new Red_Icon($application);
			$icon->read($value[red_projectstate_icon]);
			$path = $icon->data[red_icon_path];
			$datagrid[$key][red_projectstate_icon] = "<span class='icon' style='background: url($path) no-repeat;'></span>";
		}
	}
	
	$table->datagrid = $datagrid;

	$table->red_project_title(
		'width=25%',
		'href='.$_REQUEST['form'],
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->red_project_projectnumber (
		'width=15%',
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->red_project_leader_user_id (
		'width=15%',
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);
	
	$table->red_project_assistant_user_id (
		'width=15%',
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->red_projecttype_name (
		'width=15%',
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->red_projectstate_icon(
		'width=5%',
		Table::ATTRIBUTE_NOWRAP
	);

	$table->red_projectstate_name(
		'width=10%',
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->red_project_startdate(
		'width=15%',
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->red_project_foreseen_enddate(
		'width=15%',
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->footer($pageIndex);
	$table->footer($pageControlls);
	$table = $table->render();

	echo $toolbox.$table;

