<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$id = $_REQUEST['assistance_id'];
	$section = $_REQUEST['assistance_section_id'];
	
	
	// book
	$data = array();
	
	if (in_array('assistance_title', $fields)) {
		$data['assistance_title'] = $_REQUEST['assistance_title'];
	}
	
	if (in_array('assistance_application_id', $fields)) {
		$data['assistance_application_id'] = ($_REQUEST['assistance_application_id']) ? $_REQUEST['assistance_application_id'] : 0;
	}
	
	if ($data) {
		
		$assistance = new Assistance();
		
		if ($id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$assistance->read($id); 
			
			$response = $assistance->update($data); 
			
		} else {
			
			$insert = true;
			
			$data['user_created'] = $user->login;
			
			$response = $id = $assistance->create($data);	
		}
		
		// section mod
		if (!$_REQUEST['section']) {
			if ($response) {
				($insert) ? Message::request_inserted() : Message::request_updated();
			} else {
				Message::request_failure();
			}
		}
	}
	
	// section
	$data = array();

	if (in_array('assistance_section_title', $fields)) {
		$data['assistance_section_title'] = $_REQUEST['assistance_section_title'];
	}
	
	if ($_REQUEST['assistance_section_content']) {
		$data['assistance_section_content'] = $_REQUEST['assistance_section_content'];
	}
	
	if (in_array('assistance_section_published', $fields)) {
		$data['assistance_section_published'] = ($_REQUEST['assistance_section_published']) ? 1 : 0;
	}

	if ($id && $data) {
		
		$assistance = new Assistance();
		$assistance->read($id);
		
		$data['assistance_section_assistance_id'] = $id;

		if ($section) {
		
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
		
			$assistance->section()->read($section);
		
			$response = $assistance->section()->update($data);
		
			($response) ? Message::request_updated() : Message::request_failure();
		
		} else {
		
			$data['user_created'] = $user->login;
		
			$section = $response = $assistance->section()->create($data);
		
			($response) ? Message::request_inserted() : Message::request_failure();
		
			$redirect = "/$application/$controller/data/$id/$section";
			
			$create = true;
		}
		
		// add all assistance links
		if ($_REQUEST['assistance_section_copy_links'] && $create) {
		
			$model = new Model(Connector::DB_CORE);
		
			$result = $model->query("
				SELECT DISTINCT
					assistance_section_link_link
				FROM assistances
				INNER JOIN assistance_sections ON assistance_id = assistance_section_assistance_id
				INNER JOIN assistance_section_links ON assistance_section_id = assistance_section_link_section_id
				WHERE assistance_id = $assistance->id
			")->fetchAll();
		
			if ($result) {
				foreach ($result as $row) {
					$assistance->section()->link()->create($row['assistance_section_link_link']);
				}
			}
		}
	}
	
	echo json_encode(array(
		'response' => $response,
		'redirect' => $redirect
	));
	