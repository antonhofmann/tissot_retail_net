<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$model = new Model(Connector::DB_CORE);
	
	$response = true;
	
	// update pos
	if ($_REQUEST['id'] && $_REQUEST['import_id'] && !$_REQUEST['ignore']) {
		
		$pos = new Pos();
		$pos->read($_REQUEST['id']);
		
		if ($pos->id) {
			
			$data = array();
			
			// sap number
			if ($_REQUEST['posaddress_sap']) {
				$data['posaddress_sap'] = $_REQUEST['posaddress_sap'];
			}
			
			// pos name
			if ($_REQUEST['posaddress_name']) {
				$data['posaddress_name'] = $_REQUEST['posaddress_name'];
			}
			
			// pos name 2
			if ($_REQUEST['posaddress_name2']) {
				$data['posaddress_name2'] = $_REQUEST['posaddress_name2'];
			}

			// posaddress street
			if (isset($_REQUEST['posaddress_street'])) {
				$data['posaddress_street'] = $_REQUEST['posaddress_street'];
			}

			// posaddress street number
			if (isset($_REQUEST['posaddress_street_number'])) {
				$data['posaddress_street_number'] = $_REQUEST['posaddress_street_number'];
			}

			// pos address
			if (isset($_REQUEST['posaddress_street'])) {
				
				$countryData = new Country();
				$tmp = $countryData->read($_REQUEST['posaddress_phone_country']);
				
				if($_REQUEST['posaddress_street_number']) {
					
					if($tmp['country_nr_before_streename'] == 1) {
						$data['posaddress_address'] = $_REQUEST['posaddress_street_number'] . ', ' . $_REQUEST['posaddress_street'];
					}
					else {
						$data['posaddress_address'] = $_REQUEST['posaddress_street'].' '.$_REQUEST['posaddress_street_number'];
					}
				}
				else {
					$data['posaddress_address'] = $_REQUEST['posaddress_street'];
				}
			}
			
			// pos address
			if ($_REQUEST['posaddress_address']) {
				$data['posaddress_address'] = $_REQUEST['posaddress_address'];
			}
			
			// pos address
			if ($_REQUEST['posaddress_address2']) {
				$data['posaddress_address2'] = $_REQUEST['posaddress_address2'];
			}
			
			// pos zip
			if ($_REQUEST['posaddress_zip']) {
				$data['posaddress_zip'] = $_REQUEST['posaddress_zip'];
			}
			
			// pos place
			if ($_REQUEST['posaddress_place_id'] && $_REQUEST['posaddress_place_id'] <> $pos->place_id) {
				
				$place = $_REQUEST['posaddress_place_id'];
				$place = $model->query("SELECT * FROM places WHERE place_id = $place ")->fetch();
				
				if ($place['place_id']) {
					$data['posaddress_place_id'] = $place['place_id'];
					$data['posaddress_place'] = $place['place_name'];
				}
			}
			
			// pos phone country code
			if (isset($_REQUEST['posaddress_phone_country'])) {
				$data['posaddress_phone_country'] = $_REQUEST['posaddress_phone_country'];
			}

			// pos phone area code
			if (isset($_REQUEST['posaddress_phone_area'])) {
				$data['posaddress_phone_area'] = $_REQUEST['posaddress_phone_area'];
			}

			// pos phone number
			if (isset($_REQUEST['posaddress_phone_number'])) {
				$data['posaddress_phone_number'] = $_REQUEST['posaddress_phone_number'];
			}

			// pos phone
			if (isset($_REQUEST['posaddress_phone_country']) && isset($_REQUEST['posaddress_phone_area']) && isset($_REQUEST['posaddress_phone_number'])) {
				
				if($_REQUEST['posaddress_phone_area']) {
					$data['posaddress_phone'] = $_REQUEST['posaddress_phone_country']. ' ' . $_REQUEST['posaddress_phone_area']. ' ' . $_REQUEST['posaddress_phone_number'];
				}
				else {
					$data['posaddress_phone'] = $_REQUEST['posaddress_phone_country']. ' ' . $_REQUEST['posaddress_phone_number'];
				}
			}

						
			// pos distribution channel
			if ($_REQUEST['posaddress_distribution_channel'] && $_REQUEST['posaddress_distribution_channel'] <> $pos->distribution_channel) {
				$data['posaddress_distribution_channel'] = $_REQUEST['posaddress_distribution_channel'];
			}
			
			if ($data) {
				
				$data['user_modified'] = $user->login;
				$data['date_modified'] = date('Y-m-d H:i:s');
				
				// update pos
				$update = $pos->update($data);
				
				if ($update) {
					
					$message = true;
					Message::request_submitted();
					
					// update store locator
					//$storelocator = new Store_Locator();
					//$storelocator->update($pos->id);
					
					// pos identical whith company
					if ($pos->identical_to_address) {
							
						$company = new Company();
						$company->read($pos->franchisee_id);
						
						$company->update(array(
							'address_address' => $pos->address,
							'address_street' => $pos->street,
							'address_streetnumber' => $pos->street_number,
							'address_address2' => $pos->address2,
							'address_place_id' => $pos->place_id,
							'address_zip' => $pos->zip,
							'address_phone' => $pos->phone,
							'address_phone_country' => $pos->phone_country,
							'address_phone_area' => $pos->phone_area,
							'address_phone_number' => $pos->phone_number
						));
					}
					
				} else {
					$response = false;
					$message = translate::instance()->message_request_failure;
				}
			}
		} 
		else {
			$response = false;
			$message = "POS not found.";
		}

		// redirect on
		$redirect = '/mps/sapimport';
			
		if (!$message) {
			$message = true;
			Message::request_submitted();
		}
			
		// set import as checked
		if ($response) {
			$_REQUEST['ignore'] = $_REQUEST['import_id'];
		}
	}
	
	
	// update imported pos
	if ($response && $_REQUEST['ignore']) {
	
		$model->db->exec("
			UPDATE sap_imported_posaddresses SET
				sap_imported_posaddress_date_checked = CURDATE(),
				user_modified = '$user->login',
				date_modified = NOW()
			WHERE sap_imported_posaddress_id = ".$_REQUEST['ignore']."
		");
	
		if (!$message) {
			
			$message = Translate::instance()->request_submitted; 

			if (!$_REQUEST['list']) {
				Message::request_submitted();
				$redirect = '/mps/sapimport';
			}
		}
	}
	
	echo json_encode(array(
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message,
		'console' => $console
	));
	