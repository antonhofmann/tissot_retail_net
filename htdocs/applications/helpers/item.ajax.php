<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$response = array();

switch ($_REQUEST['section']) {

	case 'code.unique':

		$id = $_REQUEST['id'];
		$code = str_replace(' ', '', strtolower($_REQUEST['code']));

		$model = new Model();

		$sql = $id 
			? "SELECT item_id FROM items WHERE item_id != $id AND LOWER(REPLACE(item_code, ' ', '')) = '$code' "
			: "SELECT item_id FROM items WHERE LOWER(REPLACE(item_code, ' ', '')) = '$code' ";

		$result = $model->query($sql)->fetch();
		$response['success'] = $result ? false : true;

	break;

	case 'category.order':

		if ($_REQUEST['orders']) {

			$model = new Model();

			$sth = $model->db->prepare("
				UPDATE item_categories SET 
					item_category_sortorder = :order
				WHERE item_category_id = :id
			");

			$order=1;
			$orders = explode(',',$_REQUEST['orders']);
			
			foreach ($orders as $id) {
				$sth->execute(array(
					'order' => $order,
					'id' => $id
				));
				$order++;
			}

			$response['success'] = true;
			$response['message'] = $translate->message_request_updated;
		} 
		else {
			$response['message'] = $translate->message_request_failure;
		}

	break;

	case 'supplying.groups':

		$item = $_REQUEST['item'];
		$groups = $_REQUEST['groups'];

		if (!$user->id || !$item) {
			$response['error'] = true;
			$response['message'] = 'Bad request';
			break;
		}

		$response['success'] = true;

		$db = Connector::get();

		$db->exec("
			DELETE FROM item_supplying_groups
			WHERE item_supplying_group_item_id = $item
		");

		if ($groups) {

			$sth = $db->prepare("
				INSERT INTO item_supplying_groups (
					item_supplying_group_item_id,
					item_supplying_group_supplying_group_id,
					user_created,
					date_created
				)
				VALUES (?, ?, ?, NOW())
			");

			foreach ($groups as $i => $key) {
				$sth->execute(array($item, $key, $user->login));
			}
		}

		$response['message'] = $translate->message_request_submitted;

	break;

	case 'item.option.file.get':

		$id = $_REQUEST['id'];

		if ($id) {

			$model = new Model(Connector::DB_CORE);

			$response = $model->query("
				SELECT * 
				FROM item_option_files
				WHERE item_option_file_id = $id
			")->fetch();
		}

	break;

	case 'item.part':

		$parent = $_REQUEST['parent'];
		$child = $_REQUEST['child'];

		if ($parent && $child) {

			$model = new Model(Connector::DB_CORE);

			if ($_REQUEST['checked']) {
				
				$userlogin = User::instance()->login;

				$response['success'] = $model->db->exec("
					INSERT INTO parts (
						part_parent,
						part_child,
						user_created,
						date_created
					)
					VALUES ( $child, $parent, '$userlogin', NOW() )
				");

			} else {
				$response['success'] = $model->db->exec("
					DELETE FROM parts
					WHERE part_child = $parent AND part_parent = $child
				");
			}
			
			$response['message'] = $response['success'] ? $translate->message_request_submitted : $translate->message_request_failure;
		}
		else {
			$response['message'] = $translate->message_request_failure;
		}

	break;

	case 'item.group.item':

		$item = $_REQUEST['item'];
		$group = $_REQUEST['group'];
		$quantity = $_REQUEST['quantity'];

		if ($group && $item) {

			$model = new Model(Connector::DB_CORE);

			if ($quantity) {

				$result = $model->query("
					SELECT item_group_item_id
					FROM item_group_items
					WHERE item_group_item_group = $group AND item_group_item_item = $item
				")->fetch();

				$userlogin = User::instance()->login;

				if ($result['item_group_item_id']) {

					$response['success'] = $model->db->exec("
						UPDATE item_group_items SET 
							item_group_item_quantity = $quantity,
							user_modified = '$userlogin',
							date_modified = NOW()
						WHERE item_group_item_id = {$result[item_group_item_id]}
					");
				} else {
					
					$response['success'] = $model->db->exec("
						INSERT INTO item_group_items (
							item_group_item_group,
							item_group_item_item,
							item_group_item_quantity,
							user_created,
							date_created
						)
						VALUES ( $group, $item, $quantity, '$userlogin', NOW() )
					");
				}
			}
			else {

				$response['success'] = $model->db->exec("
					DELETE FROM item_group_items
					WHERE item_group_item_group = $group AND item_group_item_item = $item
				");
			}
			
			$response['message'] = $response['success'] ? $translate->message_request_submitted : $translate->message_request_failure;
		}
		else {
			$response['message'] = $translate->message_request_failure;
		}

	break;

	case 'update.price':

		if (!$_REQUEST['id']) {
			$response['errors'][] = "Bad request.";
			break;
		}

		if ($_REQUEST['value']) {
			$price = $_REQUEST['value']; 
			$price = ceil($price/0.05)*0.05; 
		}

		$model = new Model(Connector::DB_CORE);

		$sth = $model->db->prepare("
			UPDATE items SET 
				item_price = ?,
				user_modified = ?,
				date_modified = NOW()
			WHERE item_id = ?
		");

		$success = $sth->execute(array($price, $user->login, $_REQUEST['id']));
		$response['success'] = $success;

	break;

	case 'update.supplier.price':

		if (!$_REQUEST['id']) {
			$response['errors'][] = "Bad request.";
			break;
		}

		// update supplier price
		$supplier = new Supplier();
		$supplier->getFromItem($_REQUEST['id']);
		$success = $supplier->updatePrice($_REQUEST['value']);

		if (!$success) break;
	
		// update item price
		$item = new Item();
		$item->read($_REQUEST['id']);
		$success = $item->updatePrice($_REQUEST['value']);

		if ($success) {
			$response['orders'] = $item->updateOrderPrices();
		}

		$response['success'] = $success;
		$response['price'] = number_format($supplier->item_price, 2, ".", "");
		$response['item_price'] = number_format($item->price, 2, ".", "");

	break;
}

// response with json header
header('Content-Type: text/json');
echo json_encode($response);
