<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	if ($id && (user::permission('can_edit_catalog') || user::permission('can_edit_files_in_catalogue')) ) {
		
		$certificateFile = new Certificate_File();
		$certificateFile->read($id);
		
		$certificate = $certificateFile->certificate_id;
		$file = $certificateFile->file;
		
		$delete = $certificateFile->delete();
		
		if ($delete) {
			file::remove($file);	
			Message::request_deleted();
			url::redirect("/$appliction/$controller/$action/$certificate");
		} else { 
			Message::request_failure();
			url::redirect("/$appliction/$controller/$action/$certificate/$id");
		}
	}
	else {
		Message::access_denied();
		url::redirect("/$appliction/$controller");
	}