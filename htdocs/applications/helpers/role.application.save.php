<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$role = $_REQUEST['role'];
	$application = $_REQUEST['application'];
	
	if ($role && $application) {
	
		$model = new Model(Connector::DB_CORE);
	
		if ($_REQUEST['action']) {
				
			$action = $model->db->query("
				INSERT INTO application_roles (
					application_role_role, 
					application_role_application, 
					user_created
				)
				VALUES ($role, $application, '".$user->login."' )
			");
				
			$message = ($action) ? $translate->message_request_inserted : $translate->message_request_failure;
		}
		else {
				
			$action = $model->db->query("
				DELETE FROM application_roles
				WHERE application_role_role = $role && application_role_application = $application
			");
						
			$message = ($action) ? $translate->message_request_deleted : $translate->message_request_failure;
		}
	
		$response = ($action) ? true : false;
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
			'response' => $response,
			'message' => $message
	));
	