<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// request vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	// mastersheet vars
	$mastersheet = $_REQUEST['mps_mastersheet_id'];
	$selected_companies = $_REQUEST['selected_companies'];
	$selected_items = $_REQUEST['selected_items'];

	// order sheet deletable statments
	$deletable_statments = Ordersheet_State::loader('delete');
	
	if ($selected_companies && $selected_items) {
		
		$deleted = array();
		$not_deleted = array();
		$return_items = array();
		
		$ordersheets = explode(',', $selected_companies);
		
		$items = explode(',', $selected_items);
		
		// ordersheet
		$ordersheet = new Ordersheet($application);
	
		// fore eache selected order sheets
		// remove selected items
		foreach ($ordersheets as $id) {
			
			// set order sheet in revision state
			$set_in_revision = false;
			
			// read order sheet instance
			$ordersheet->read($id);
			
			// get order sheet items
			$ordersheet_items = $ordersheet->item()->load();
			
			if ($ordersheet_items && $items) {
				
				foreach ($ordersheet_items as $item => $data) {
					
					$material = $data['mps_ordersheet_item_material_id'];
					
					// if item is selected
					if (in_array($material, $items)) {
						
						// is order sheet in deletable state
						if (in_array($ordersheet->workflowstate_id, $deletable_statments)){
							
							// read item instance
							$ordersheet->item()->read($item);
							
							// remove item from order sheet
							$ordersheet->item()->delete();
							
							// set order sheet in revision state
							$set_in_revision = true;
							
							// store delete items
							array_push($deleted, $material);
						}
						else {
							
							// store not delete items
							array_push($not_deleted, $material);
						}
					} 
					else {
						
						// store returned item (not deleted)
						array_push($return_items, $material);
					}
				}
			}
			
			// if order sheet item structure is changed
			// and order sheet is approved or completed
			// set this order sheet in revision statemant
			if ($set_in_revision && ($ordersheet->state()->isApproved() || $ordersheet->state()->isCompleted()) ) {
				$ordersheet->update(array(
					'mps_ordersheet_workflowstate_id' => Workflow_State::STATE_REVISION
				));
			}
		}
		
		$response = true;
		$message = $translate->message_request_deleted;
		
		if ($return_items) {
			$return_items = array_unique($return_items);
			$selected = join(',', $return_items);
		}
		
		if ($not_deleted) {			
			$message .= "<br /><br />";
			$message .= "Only order sheets with the one of the following workflow states can be deleted: ";
			$message .= "<b><b>in preparation</b>, <b>open</b>,, in progress</b>, and <b>in revision</b>. ";
		}
	} 
	else {
		$response = false;
		$message =$translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'tab_items' => true,
		'tab' => 'ordersheet_items',
		'selected_items' => $selected,
		'reset' => false
	));
	