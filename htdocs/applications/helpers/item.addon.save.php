<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	// product line
	$id = $_REQUEST['addon_id'];
	
	$data = array();	
	
	if (in_array('addon_parent', $fields)) {
		$data['addon_parent'] = $_REQUEST['addon_parent'];
	}	
	
	if (in_array('addon_child', $fields)) {
		$data['addon_child'] = $_REQUEST['addon_child'];
	}
		
	if (in_array('addon_package_quantity', $fields)) {
		$data['addon_package_quantity'] = $_REQUEST['addon_package_quantity'] ? $_REQUEST['addon_package_quantity'] : null;
	}
			
	if (in_array('addon_min_packages', $fields)) {
		$data['addon_min_packages'] = $_REQUEST['addon_min_packages'] ? $_REQUEST['addon_min_packages'] : null;
	}	
			
	if (in_array('addon_max_packages', $fields)) {
		$data['addon_max_packages'] = $_REQUEST['addon_max_packages'] ? $_REQUEST['addon_max_packages'] : null;
	}
	
	if ($data) {
	
		$itemAddon = new Item_Addon();
		$itemAddon->read($id);
	
		if ($itemAddon->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $itemAddon->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $itemAddon->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			if ($response) {
				$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
			}
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	