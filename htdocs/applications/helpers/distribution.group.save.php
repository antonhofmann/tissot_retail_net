<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$id = $_REQUEST['mps_distchannel_group_id'];
	
	$data = array();
	
	if ($_REQUEST['mps_distchannel_group_name']) {
		$data['mps_distchannel_group_name'] = $_REQUEST['mps_distchannel_group_name'];
	}
	
	if (in_array('mps_distchannel_group_allow_for_agents', $fields)) {
		$data['mps_distchannel_group_allow_for_agents'] = ($_REQUEST['mps_distchannel_group_allow_for_agents']) ? 1 : 0;
	}
	
	if ($data) {
		
		$distributionGroup = new DistChannelGroup();
		$distributionGroup->read($id);
		
		if ($id) {
			
			$data['user_modified'] = $user->login;
			
			$response = $distributionGroup->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $distributionGroup->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));