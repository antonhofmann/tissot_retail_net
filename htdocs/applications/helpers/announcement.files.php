<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$application = $_REQUEST['application'];
$id = $_REQUEST['id'];

switch ($_REQUEST['section']) {
	
	case 'save':
	
		if ($id && $_REQUEST['file']) {
			
			$model = new Model($application);
			
			foreach ($_REQUEST['file'] as $key => $file) {
				
				// file exist in db
				$result = $model->query("
					SELECT * 
					FROM mps_announcement_files	
					WHERE mps_announcement_file_id = $key
				")->fetch();
				
				// update file
				if ($result['mps_announcement_file_id']) {
					
					$sth = $model->db->prepare("
						UPDATE mps_announcement_files SET
							mps_announcement_file_title = :title
						WHERE mps_announcement_file_id = :id
					");
					
					$action = $sth->execute(array(
						'title' => $_REQUEST['title'][$key],
						'id' => $key
					));
					
				} 
				// insert new one
				else {
					
					// announcement dir
					$dir = "/public/data/files/mps/announcements/$id";
					$filename = basename($file);
					$copy = Upload::moveTemporaryFile($file, $dir);
					
					if ($copy) {
							
						// save file in db
						$sth = $model->db->prepare("
							INSERT INTO mps_announcement_files (
								mps_announcement_file_announcement_id,
								mps_announcement_file_title,
								mps_announcement_file_path,
								user_created
							) VALUES (
								:id, :title, :file, :user
							)
						");
							
						$action = $sth->execute(array(
							'id' => $id,
							'title' => $_REQUEST['title'][$key],
							'file' => "$dir/$filename",
							'user' => User::instance()->login
						));
					}
				}
			}
			
			$response = array(
				'response' => $action,
				'reload' => true
			);
		}
		
	break;
	
	case 'delete':
		
		if ($id) {
			
			$model = new Model($application);
			
			$file = $model->query("
				SELECT *
				FROM mps_announcement_files
				WHERE mps_announcement_file_id = $id
			")->fetch();
			
			if (file_exists($_SERVER['DOCUMENT_ROOT'].$file['mps_announcement_file_path'])) {
				
				$filename = basename($file['mps_announcement_file_path']);
				$dirpath = file::dirpath($file['mps_announcement_file_path']);
				
				file::remove($file['mps_announcement_file_path']);
				file::remove("$dirpath/thumbnail/$filename");
				
				$delete = $model->db->exec("
					DELETE FROM mps_announcement_files
					WHERE mps_announcement_file_id = $id
				");
				
				$response = array($filename => $delete);
			}
		}
	
	break;
	
	default:
		
		$model = new Model($application);
		
		$result = $model->query("
			SELECT 
				mps_announcement_file_id AS id,
				mps_announcement_file_title AS title,
				mps_announcement_file_description AS description,
				mps_announcement_file_path AS file
			FROM mps_announcement_files
			WHERE mps_announcement_file_announcement_id = $id		
		")->fetchAll();
		
		$response = Upload::get($result, $application);
	
	break;
}

header('Content-Type: text/json');
echo json_encode($response);
