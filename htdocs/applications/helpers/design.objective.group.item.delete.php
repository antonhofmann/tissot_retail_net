<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	if ($id && $user->permission('can_edit_catalog')) {
		
		$designObjectiveItem = new Design_Objective_Item();
		$designObjectiveItem->read($id);

		$group = $designObjectiveItem->group;
		
		$delete = $designObjectiveItem->delete();
		
		if ($delete) {
			Message::request_deleted();
			url::redirect("/$appliction/$controller/$action/$group");
		} else {
			Message::request_failure();
			url::redirect("/$appliction/$controller/$action/$group/$id");
		}
	}
	else {
		Message::access_denied();
		url::redirect("/$appliction/$controller");
	}