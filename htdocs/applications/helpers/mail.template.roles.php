<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$id = $_REQUEST['template'];
$user = User::instance();

if ($id) { 
	
	$model = new Model(Connector::DB_CORE);

	$response = true;
	
	// remove all recipients
	$model->db->exec("
		DELETE FROM mail_template_roles	
		WHERE mail_template_role_template_id = $id
	");
	
	$roles = array();
	
	if ($_REQUEST['recipient']) {
		$roles = array_keys($_REQUEST['recipient']);
	}
	
	if ($_REQUEST['cc']) {
		$roles = array_merge($roles, array_keys($_REQUEST['cc']));
	}

	if (is_array($roles)) {
		
		$roles = array_unique($roles);
		
		$sth = $model->db->prepare("
			INSERT INTO mail_template_roles (
				mail_template_role_template_id,
				mail_template_role_role_id,
				mail_template_role_cc,
				user_created
			) VALUES (
				:template, :role, :cc, :user
			)
		");
		
		foreach ($roles as $role) {
			$sth->execute(array(
				'template' => $id,
				'role' => $role,
				'cc' => isset($_REQUEST['cc'][$role]) ? 1 : 0,
				'user' => $user->login
			));
		}
	}

	// remove all other recipients
	$model->db->exec("
		DELETE FROM mail_template_other_recipients	
		WHERE mail_template_other_recipient_template_id = $id
	");

	$others = array();

	if ($_REQUEST['other_recipient']) {
		$others = array_keys($_REQUEST['other_recipient']);
	}
	
	if ($_REQUEST['other_recipient_cc']) {
		$others = array_merge($others, array_keys($_REQUEST['other_recipient_cc']));
	}

	if ($others) {

		$sthRegionalResponsible = $model->db->prepare("
			INSERT INTO mail_template_other_recipients (
				mail_template_other_recipient_template_id,
				mail_template_other_recipient_name,
				mail_template_other_recipient_value,
				user_created
			) VALUES (?,?,?,?)
		");

		foreach ($others as $name) {
			
			if ($name=='regional_responsible') {
				
				$name = $_REQUEST['other_recipient_cc'] && in_array('regional_responsible', array_keys($_REQUEST['other_recipient_cc']))
					? 'regional_responsible_cc'
					: 'regional_responsible';

				$sthRegionalResponsible->execute(array($id, $name, 1, $user->login));

			}

		}
	}

	$message = Translate::instance()->message_request_saved;
}
else {
	$response = false;
	$message = Translate::instance()->message_request_failure;
}

// responding
header('Content-Type: text/json');
echo json_encode(array(
	'response' => $response,
	'notification' => array(
		'content' => $message,
		'properties' => array(
			'theme' => $response ? 'message' : 'error'
		)
	)
));
	