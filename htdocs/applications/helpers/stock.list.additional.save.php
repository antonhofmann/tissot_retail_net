<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['scpps_stocklist_id'];
	$columns = $_REQUEST['columns'];
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	if ($id) {
	
		$stocklist = new Stocklist();
		$stocklist->read($id);
		
		// reset additional columns
		if (in_array('columns', $fields)) {
			$response = $stocklist->update(array(
				'scpps_stocklist_columns' => null
			));
		}
	
		// update additional columns
		if ($columns) {
			$response = $stocklist->update(array(
				'scpps_stocklist_columns' => serialize($columns),
				'user_modified' => $user->login
			));
		}
		
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	