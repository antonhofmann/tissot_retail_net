<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$archived = $_REQUEST['archived'];
	$id = $_REQUEST['id'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", false);
	$_REQUEST['status'] = (isset($_REQUEST['status'])) ? $_REQUEST['status'] : 1;
	
	$model = new Model(Connector::DB_CORE);

	// sql order
	$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'country_name, posaddress_name';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// sql pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$offset = ($page-1) * $settings->limit_pager_rows;
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "( 
			posaddress_name LIKE \"%$keyword%\" 
			OR posaddress_zip LIKE \"%$keyword%\" 
			OR posaddress_place LIKE \"%$keyword%\" 
			OR country_name LIKE \"%$keyword%\" 
			OR postype_name LIKE \"%$keyword%\" 
		)";
	}
	
	// filter: show opened pos locations
	if ($_REQUEST['status']==1) {
		$filters['status'] = "(
			posaddress_store_closingdate = '0000-00-00'
			OR posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = ''
		)";
	}
	
	// filter: show closed pos locations
	if ($_REQUEST['status']==2) {
		$filters['status'] = "(
			posaddress_store_closingdate <> '0000-00-00'
			OR posaddress_store_closingdate <> NULL
		)";
	}

	// filter: current company
	$filters['company'] = "posaddress_franchisee_id = $id";
	
	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			posaddress_id,
			posaddress_name,
			posaddress_zip,
			posaddress_place,
			country_name,
			posaddress_store_closingdate,
			postype_name
		FROM posaddresses
	")
	->bind(Pos::DB_BIND_COUNTRIES)
	->bind(Pos::DB_BIND_POSTYPES)
	->filter($filters)
	->order($order, $direction)
	->fetchAll();
	
	if ($result) {
		
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		
		$icon_lock = ui::icon('lock');
		
		foreach ($datagrid as $key => $row) {
			
			if ($row['posaddress_store_closingdate'] && $row['posaddress_store_closingdate'] <> '0000-00-00') {
				$row['closed'] = $icon_lock;
				$link = "/$application/pos/archived/address/$key";
				$row['posaddress_name'] = "<a href='$link'>".$row['posaddress_name']."</a>";
			} else {
				$link = "/$application/pos/address/$key";
				$row['posaddress_name'] = "<a href='$link'>".$row['posaddress_name']."</a>";
			}
				
			$datagrid[$key] = $row;
		}
		
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));
		
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";

	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	// toolbox: user actives
	$status_data = array(
		1 => 'Operating POS Locations',
		2 => 'Closed POS Locations'
	);
	
	$toolbox[] = ui::dropdown($status_data, array(
		'name' => 'status',
		'id' => 'status',
		'class' => 'submit',
		'value' => $_REQUEST['status'],
		'label' => 'All POS Locations'
	));
	
	if ($toolbox) {
		$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$order, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	$table->dataloader($dataloader);
	
	$table->country_name(
		'width=20%',
		Table::ATTRIBUTE_NOWRAP
	);
	
	$table->posaddress_name();
	
	$table->postype_name(
		'width=20%',
		Table::ATTRIBUTE_NOWRAP
	);
	
	$table->posaddress_place(
		'width=20%',
		Table::ATTRIBUTE_NOWRAP
	);
	
	$table->posaddress_zip(
		'width=5%',
		Table::ATTRIBUTE_NOWRAP
	);
	
	if ($datagrid && _array::key_exists('closed', $datagrid)) {
		$table->closed(
			'width=40px',
			TABLE::ATTRIBUTE_ALIGN_CENTER
		);
	}
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	