<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$appliction = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$permission_edit = user::permission('can_edit_catalog');
$permission_edit_items = user::permission('can_edit_items_in_catalogue');
$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

if ($id && $_CAN_EDIT) {
	
	$item = new Item();
	$item->read($id);
	
	$delete = $item->delete();

	
	if ($delete) {

		if ($item->type==Item::TYPE_COST_ESTIMATION) $target = 'costEstimations';
		elseif ($item->type==Item::TYPE_SPECIAL) $target = 'standardSpecial';

		Message::request_deleted();
		
		url::redirect("/$appliction/$controller/$target");

	} else { 
		Message::request_failure();
		url::redirect("/$appliction/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$appliction/$controller");
}