<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];

	$option = $_REQUEST['item_option_id'];
	
	$model = new Model(Connector::DB_CORE);
	
	// datagrid
	$result = $model->query("
		SELECT 
			item_option_file_id As id,
			item_option_file_title As title, 
			item_option_file_path AS path, 
			item_option_file_description AS description,
			DATE_FORMAT(date_created, '%d.%m.%Y') as date
		FROM item_option_files
		WHERE item_option_file_option = $option
		ORDER BY item_option_file_title
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {	
			$item = $row['id'];
			$extension = file::extension($row['path']);
			$datagrid[$item]['title'] = "<a href='/applications/templates/item.option.file.php?id=$item' class='filemodal' >{$row[title]}</a><span>{$row[description]}</span>";
			$datagrid[$item]['fileicon']  = "<span class='file-extension $extension modal' tag='{$row[path]}'></span>";
			$datagrid[$item]['date']= "<span>Date:<br />{$row[date]}</span>";
		}

		$table = new Table();
		$table->datagrid = $datagrid;
		$table->fileicon('width=32px');
		$table->title();
		$table->date('width=150px');
		
		echo "<h4>Files</h4>";
		echo $table->render();
	}
