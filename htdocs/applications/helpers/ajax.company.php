<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$company = new Company();
	$company->read($_REQUEST['id']);
	
	if ($company->id) {
		
		switch ($_REQUEST['action']) {
			
			case 'header':
				$json['header'] = $company->header();
			break;
			
			default:
				$json = $company->data;
			break;
		}
	}
	
	if ($json) {
		header('Content-Type: text/json');
		echo json_encode($json);
	}
	