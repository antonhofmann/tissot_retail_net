<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	$model = new Model(Connector::DB_CORE);

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'db_description, db_table_description';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			db_description LIKE \"%$keyword%\" 
			OR db_table_table LIKE \"%$keyword%\" 
			OR db_table_description LIKE \"%$keyword%\" 
		)";
	}
	
	if ($_REQUEST['database']) {
		$filters['database'] = 'db_table_db = '.$_REQUEST['database'];
	}
	
	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS
			db_table_id,
			db_table_description,
			db_description,
			(
				SELECT COUNT(db_reference_id) AS total 
				FROM db_references WHERE db_table_id = db_reference_referenced_table_id
			) AS reference
		FROM db_tables
	")
	->bind('INNER JOIN dbs ON db_id = db_table_db')
	->order($sort, $direction)
	->filter($filters)
	->offset($offset, $rowsPerPage)
	->fetchAll();
	
	if ($result) {
		
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
	
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
	
	// toolbox: button print
	if ($datagrid && $_REQUEST['export'])  {
		$toolbox[] = html::linkbutton(array(
			'id' => 'print',
			'icon' => 'print',
			'href' => $_REQUEST['export'],
			'label' => $translate->print
		));
	}
	
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	// toolbox: databases
	$result = $model->query("
		SELECT DISTINCT
			db_id,
			db_description
		FROM db_tables
	")
	->bind('INNER JOIN dbs ON db_id = db_table_db')
	->filter($filters)
	->exclude('database')
	->order('db_description')
	->fetchAll();
	
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'database',
		'id' => 'database',
		'class' => 'submit',
		'value' => $_REQUEST['database'],
		'caption' => $translate->all_databases
	));
	
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	$table->dataloader($dataloader);
	
	$table->db_description(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=20%'
	);
	
	$table->db_table_description(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		"href=".$_REQUEST['form']
	);
	
	$table->reference(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=10%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	