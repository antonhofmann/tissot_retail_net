<?php 
/*
 	Put Master Sheet in archiv
 	
	Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2012-02-01
    Version:        1.1
    Updated: 		2012-10-19, Roger Bucher

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
 */

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$model = new Model($application);
	
	$mastersheet = new Mastersheet($application);
	$mastersheet->read($id);
	
	if ($mastersheet->id) {

		if (!$mastersheet->hasExportedItems()) {
		
			// archive mastersheets
			$response = $model->db->query("
				UPDATE mps_mastersheets SET 
					mps_mastersheet_consolidated = 1, 
					mps_mastersheet_archived = 1 
				WHERE mps_mastersheet_id = $id
			");
			
			if ($response) {
				
				// set all ordersheets to archived
				$model->db->query("
					UPDATE mps_ordersheets SET 
						mps_ordersheet_workflowstate_id = 6
					WHERE mps_ordersheet_mastersheet_id = $id
				");
				
				// get all ordersheet items
				$result = $model->query("
					SELECT 
						mps_ordersheet_item_id,
						mps_ordersheet_item_quantity,
						mps_ordersheet_item_quantity_approved,
						mps_ordersheet_item_quantity_confirmed,
						mps_ordersheet_item_desired_delivery_date,
						address_mps_customernumber,
						address_mps_shipto
					FROM mps_ordersheet_items	
					INNER JOIN mps_ordersheets ON mps_ordersheet_ordersheet_id = mps_ordersheet_id
					INNER JOIN db_retailnet.addresses ON address_id = mps_ordersheet_address_id
					WHERE mps_ordersheet_mastersheet_id = $id")
				->fetchAll();
				
				if ($result) {
					
					$ordersheet_item = new Ordersheet_Item($application);
					
					foreach ($result as $ordersheet) {
						
						$quantity = ($ordersheet['mps_ordersheet_item_quantity_approved']) ? $ordersheet['mps_ordersheet_item_quantity_approved'] : $ordersheet['mps_ordersheet_item_quantity'];
						$quantity = ($quantity) ? $quantity : 0;
						
						$ordersheet_item->id = $ordersheet['mps_ordersheet_item_id'];
						$ordersheet_item->update(array(
							'mps_ordersheet_item_quantity_approved' => $quantity,
							'mps_ordersheet_item_quantity_confirmed' => 0,
							'mps_ordersheet_item_customernumber' => $ordersheet['address_mps_customernumber'],
							'mps_ordersheet_item_shipto' => $ordersheet['address_mps_shipto']
						));
					}
				}

				Message::add(array(
					'type' => 'message',
					'keyword' => 'message_mastersheet_put_to_archive',
					'life' => 5000
				));
			}
			else {
				Message::add(array(
					'type' => 'message',
					'keyword' => 'error_mastersheet_put_to_archive',
					'life' => 5000
				));
			}
		}
		else {
			Message::add(array(
				'type' => 'warning',
				'keyword' => 'warning_mastersheet_put_to_archive',
				'life' => 5000
			));
		}
	}
	else {

		Message::request_failure();
	}

	url::redirect("/$application/$controller/$action/$id");
	
