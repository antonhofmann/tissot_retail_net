<?php 	
/**
 * 	Master Sheet Items
 * 
 * 	Load all master sheet items. Outcome will depend on the controller.
 * 	
 * 	This file should be used for:
 * 		- Master Sheet Consolidation Items
 * 		- Generate Sales Orders
 * 		- Show Purchase Orders
 * 	
 * 	@author: admir.serifi@mediaparx.ch
 * 	@copyright (c) Swatch AG
 *  @version 1.1
 * 
 */
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$search = $_REQUEST['search'];
	$fields = $_REQUEST['fields'];
	$id = $_REQUEST['id'];
	
	// db connector
	$model = new Model($application);
	
	// request
	$_REQUEST = session::filter($application, $application.$controller.$archived.$action, false);
	
	// filter: clients
	if ($_REQUEST['ordersheets']) {
		$filters['ordersheets'] = "mps_ordersheet_id = ".$_REQUEST['clients'];
	}
	
	// filter workflow states
	if ($_REQUEST['workflowstates']) {
		$filters['workflowstates'] = "mps_ordersheet_workflowstate_id = ".$_REQUEST['workflowstates'];
	}
	
	// filter: mastersheet
	$filters['mastersheet'] = "mps_ordersheet_mastersheet_id = $id";
	
	// ordersheet items
	$result = $model->query("
		SELECT
			mps_ordersheet_id,
			mps_material_planning_type_id,
			mps_material_planning_type_name,
			mps_material_collection_category_id,
			mps_material_collection_category_code,
			mps_material_collection_code,
			mps_material_hsc,
			mps_material_id,
			mps_material_code,
			mps_material_name,
			mps_material_setof,
			mps_material_locally_provided,
			mps_ordersheet_item_price,
			currency_symbol,
			mps_ordersheet_item_desired_delivery_date,
			mps_ordersheet_item_order_date,
			SUM(mps_ordersheet_item_quantity) AS mps_ordersheet_item_quantity,
			SUM(mps_ordersheet_item_quantity_proposed) AS mps_ordersheet_item_quantity_proposed,
			SUM(mps_ordersheet_item_quantity_approved) AS mps_ordersheet_item_quantity_approved,
			SUM(mps_ordersheet_item_quantity_confirmed) AS mps_ordersheet_item_quantity_confirmed,
			(mps_ordersheet_item_price * Sum(mps_ordersheet_item_quantity_approved)) AS total_cost,
			(mps_ordersheet_item_price * Sum(mps_ordersheet_item_quantity_confirmed)) as total_cost_ordered
		FROM mps_ordersheet_items
	")
	->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
	->bind(Ordersheet_Item::DB_BIND_MATERIALS)
	->bind(Material::DB_BIND_COLLECTIONS)
	->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
	->bind(Material::DB_BIND_PLANNING_TYPES)
	->bind(Material::DB_BIND_CURRENCIES)
	->filter($filters)
	->group('mps_material_id')
	->order('mps_material_planning_type_name')
	->order('mps_material_collection_category_code')
	->order('mps_material_code')
	->order('mps_material_name')
	->fetchAll();
	
	if ($result) {
	
		foreach ($result as $row) {
			
			$planning = $row['mps_material_planning_type_id'];
			$collection = $row['mps_material_collection_category_id'];
			$item = $row['mps_material_id'];
			
			// proposed quantity
			$proposed_quantity = $row['mps_ordersheet_item_quantity_proposed'];
			$proposed_quantity = ($proposed_quantity) ? $proposed_quantity : 0;
			
			// quantity
			$quantity = $row['mps_ordersheet_item_quantity'];
			$quantity = ($quantity) ? $quantity : 0;
			
			// approved quantity
			$approved_quantity = $row['mps_ordersheet_item_quantity_approved'];
			$approved_quantity = ($approved_quantity) ? $approved_quantity : 0;
			
			// ordered quantity
			$ordered_quantity = $row['mps_ordersheet_item_quantity_confirmed'];
			$ordered_quantity = ($ordered_quantity) ? $ordered_quantity : 0;
			
			// total cost
			$total_cost = number_format($row['total_cost'], $settings->decimal_places, '.', '') ;
			
			// total cost ordered
			$total_cost_ordered = number_format($row['total_cost_ordered'], $settings->decimal_places, '.', '') ;
			
			$datagrid[$planning]['caption'] = $row['mps_material_planning_type_name'];
			$datagrid[$planning]['collections'][$collection]['caption'] = $row['mps_material_collection_category_code'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_collection_code'] = $row['mps_material_collection_code'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_hsc'] = $row['mps_material_hsc'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_code'] = $row['mps_material_code'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_name'] = $row['mps_material_name'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_item_price'] = $row['mps_ordersheet_item_price'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['currency_symbol'] = $row['currency_symbol'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_setof'] = $row['mps_material_setof'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_item_quantity_proposed'] = $proposed_quantity;
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_item_quantity'] = $quantity;
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_item_quantity_approved'] = $approved_quantity;
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_item_quantity_confirmed'] = $ordered_quantity;
			$datagrid[$planning]['collections'][$collection]['items'][$item]['total_cost'] = $total_cost;
			$datagrid[$planning]['collections'][$collection]['items'][$item]['total_cost_ordered'] = $total_cost_ordered;
			
			
			// SALES ORDERS: check ordered state
			// if item is not ordered then display checkbox as order option
			// for localy provided items, build hidden filds
			if ($controller=='salesorders') { 
				
				$check = $model->query("
					SELECT mps_ordersheet_item_id
					FROM mps_ordersheet_items		
				")
				->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
				->bind(Ordersheet_Item::DB_BIND_MATERIALS)
				->filter('mastersheet', "mps_ordersheet_mastersheet_id = $id")
				->filter('not_ordered', "( mps_ordersheet_item_purchase_order_number IS NULL OR mps_ordersheet_item_purchase_order_number = '') ")
				->filter('material', "mps_ordersheet_item_material_id = $item")
				->fetchAll();
				
				if ($check) { 
					
					// item is locally provided
					if ($row['mps_material_locally_provided']) { 
						$hidden_fields .= "<input type=hidden name=item[$item] value=$item  class=locally_provided >";
					} 
					// show order box
					else {
						$datagrid[$planning]['collections'][$collection]['items'][$item]['checkbox'] = "<input type=checkbox name=item[$item] value=$item class=checkbox  checked=checked >";
					}
					
					$checkAll = 'checked=checked';
				}
			}
		}
	
	
		if ($datagrid) {
	
			foreach ($datagrid as $key => $row) {
	
				$list .= "<h5 data=$key >".$row['caption']."</h5>";
	
				foreach ($row['collections'] as $subkey => $value) {
						
					$list .= "<h6 data=$subkey >".$value['caption']."</h6>";
	
					$totalprice = 0;
					$tableKey = $key."-".$subkey;
						
					$table = new Table(array(
						'id' => $tableKey
					));
	
					$table->datagrid = $value['items'];
					$table->dataloader($dataloader);

					$table->mps_material_collection_code(
						Table::ATTRIBUTE_NOWRAP,
						'width=10%'
					);

					$table->mps_material_code(
						Table::ATTRIBUTE_NOWRAP,
						'width=10%'
					);

					$table->mps_material_name();

					$table->mps_material_hsc(
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);

					$table->mps_material_setof(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);

					$table->mps_ordersheet_item_price(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);

					$table->currency_symbol(
						Table::ATTRIBUTE_NOWRAP,
						'width=20px'
					);
						
					$table->mps_ordersheet_item_quantity_proposed(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);

					$table->mps_ordersheet_item_quantity(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						Table::PARAM_GET_FROM_LOADER,
						'width=5%'
					);
	
					$table->mps_ordersheet_item_quantity_approved(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);
	
					$table->total_cost(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);
					
					if ($archived) {
						$table->mps_ordersheet_item_quantity_confirmed(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							'width=5%'
						);
					}
					
					if ($archived) {
						$table->total_cost_ordered(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							'width=5%'
						);
					} 
					
					// items in revision
					if (_array::key_exists('checkbox', $table->datagrid)) {
						$table->checkbox('width=20px');
						$table->caption('checkbox', "<input type=checkbox class=checkall $checkAll />");
					}
	
					// total collection price
					foreach ($value['items'] as $key => $array) {
						$totalprice = $totalprice+$array['total_cost'];
					}
	
					$totalprice = number_format($totalprice, $settings->decimal_places, '.', '');
						
					$list .= $table->render(array('order'=>true));
					
					$list .= "
						<div class='totoal-collection'>
							".$translate->total_cost." (<em>".$value['caption']."</em>): 
							<b class='total-items $tableKey'>$totalprice</b>
						</div>";
				}
			}
		}
	}
	else {
		
		$list = html::emptybox($translate->empty_result);
	}
	
	if ($hidden_fields) {
		$list .= "<div style='display:none' class=hidden_fields >$hidden_fields</div>";
	}
	
	echo $list;
	
	