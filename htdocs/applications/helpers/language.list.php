<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	$model = new Model(Connector::DB_CORE);

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'language_name';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// sql pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$offset = ($page-1) * $settings->limit_pager_rows;
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search']; 
		
		$filters['search'] = "( 
			language_name LIKE \"%$keyword%\" 
			OR language_iso639_1 LIKE \"%$keyword%\" 
		)";
	}
	
	$filters['mps'] = 'language_used_in_mps = 1';
	
	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS	
			language_id,
			language_name,
			language_iso639_1	
		FROM languages
	")
	->order($sort, $direction)
	->filter($filters)
	->offset($offset)
	->fetchAll();
	
	if ($result) {
		
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
	
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	
	// toolbox: button print
	if ($datagrid && $_REQUEST['export'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['export'],
			'caption' => $translate->add_new
		));
	}
	
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'caption' => $translate->add_new
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	$table->dataloader($dataloader);
	
	$table->language_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		"href=".$_REQUEST['form']
	);
	
	$table->language_iso639_1(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=20%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	