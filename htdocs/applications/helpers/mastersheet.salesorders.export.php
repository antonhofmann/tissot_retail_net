<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$mastersheet = $_REQUEST['id'];
	$customerNumbers = $_REQUEST['customernumber'];
	$shiptoNumbers = $_REQUEST['shiptonumber'];
	$deliverDates = $_REQUEST['desire'];
	$orderNumber = $_SERVER['REQUEST_TIME'];
	
	if ($mastersheet && $customerNumbers && $shiptoNumbers && $deliverDates) {
		
		$model = new Model($application);
		
		// export ramco array
		$export = array();
		$totalOrderedsheetItems = array();
		
		// get all ordersheets and ordersheet items number for selected mastersheet
		$ordersheets = $model->query("
			SELECT 
				mps_ordersheet_id,
				mps_ordersheet_address_id
			FROM mps_ordersheets
		")
		->bind(Ordersheet::DB_BIND_COMPANIES)
		->filter('mastersheet', "mps_ordersheet_mastersheet_id = $mastersheet")
		->fetchAll();

		if ($ordersheets) {
			
			$totalOrderSheets = count($ordersheets);
		
			foreach ($ordersheets as $row) {
				
				$ordersheet = $row['mps_ordersheet_id'];
				$company = $row['mps_ordersheet_address_id'];
					
				// get ordersheet item
				$result = $model->query("
					SELECT
						mps_ordersheet_item_id,
						mps_material_code,
						mps_material_ean_number,
						mps_material_locally_provided,
						mps_ordersheet_item_quantity_approved
					FROM mps_ordersheet_items
				")
				->bind(Ordersheet_Item::DB_BIND_MATERIALS)
				->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = $ordersheet")
				->fetchAll();
				
				// total ordersheet items
				$totalOrderedsheetItems[$ordersheet] = count($result);

				if ($result) {
					
					// group items dependet from customer number and shiping number
					// for each diferent shiping number, produce one CSV export file
					foreach ($result as $item) {
						
						$id = $item['mps_ordersheet_item_id'];
						
						if ($customerNumbers[$id] && $shiptoNumbers[$id]) {
							
							$quantity = ($item['mps_material_locally_provided']) ? 0 : $item['mps_ordersheet_item_quantity_approved'];
							
							$keyCustomerNumber = $customerNumbers[$id];
							$keyShipNumber = $shiptoNumbers[$id];
							$groupItems[$company][$keyCustomerNumber][$keyShipNumber][$id]['code'] = $item['mps_material_code'];
							$groupItems[$company][$keyCustomerNumber][$keyShipNumber][$id]['ean'] = $item['mps_material_ean_number'];
							$groupItems[$company][$keyCustomerNumber][$keyShipNumber][$id]['quantity'] = $quantity;
							$groupItems[$company][$keyCustomerNumber][$keyShipNumber][$id]['delivery_date'] = $deliverDates[$id];
							$groupItems[$company][$keyCustomerNumber][$keyShipNumber][$id]['locally'] = $item['mps_material_locally_provided'];
							
							// sum ordersheet approved quantities
							if ($quantity) {
								$totalOrdersheetApprovedQuantites[$ordersheet] = $totalOrdersheetApprovedQuantites[$ordersheet] + $quantity;
							}
						}
					}
				}
			}
		}
		else {
			$response = false;
			$message = $translate->error_request;
		}
		
		// create export array
		if (!empty($groupItems)) {
			
			$ordersheet_item = new Ordersheet_Item($application);
			
			foreach ($groupItems as $company => $customberNumbers) {
				foreach ($customberNumbers as $customerNumber => $shiptoNumbers) {
					foreach ($shiptoNumbers as $shipNumber => $itemData) {
						
						$orderNumber++;
						
						// check ordernumber
						$checkOrderNumber = $model->query("
							SELECT mps_ordersheet_item_purchase_order_number
							FROM mps_ordersheet_items
							WHERE mps_ordersheet_item_purchase_order_number = '$orderNumber'
						")->fetchAll();
						
						if ($checkOrderNumber) {
							$orderNumber++;
						}
						
						foreach ($itemData as $id => $item) {
							
							// update ordersheet item
							$ordersheet_item->id = $id;
							$ordersheet_item->update(array(
								'mps_ordersheet_item_desired_delivery_date' => date::sql($item['delivery_date']),
								'mps_ordersheet_item_order_date' => date('Y-m-d H:i:s'),
								'mps_ordersheet_item_purchase_order_number' => $orderNumber,
								'mps_ordersheet_item_shipto' => $shipNumber,
								'mps_ordersheet_item_customernumber' => $customerNumber
							));
								
							// export ramco array
							$export[$orderNumber]['items'][$id]['code'] = $item['code'];
							$export[$orderNumber]['items'][$id]['ean'] = $item['ean'];
							$export[$orderNumber]['items'][$id]['quantity'] = $item['quantity'];
							$export[$orderNumber]['items'][$id]['locally'] = $item['locally'];
							$export[$orderNumber]['items'][$id]['delivery_date'] = date('Y-m-d', date::timestamp($item['delivery_date']));
						}
			
						// export ramco array
						$export[$orderNumber]['customernumber'] = $customerNumber;
						$export[$orderNumber]['shipto'] = $shipNumber;
					}
				}
			}
		}
		
		
		// change order sheet state as sales order submitted
		if ($ordersheets) {
			
			$totalArchivedOrdersheets = 0;
			
			$workflowState = new Workflow_State($application);
			
			$user_tracking = new User_Tracking($application);
			
			foreach ($ordersheets as $row) {
				
				$ordersheet = $row['mps_ordersheet_id'];
				
				$orderedItems = $model->query("
					SELECT COUNT(mps_ordersheet_item_id) AS total
					FROM mps_ordersheet_items
					WHERE mps_ordersheet_item_ordersheet_id = $ordersheet AND mps_ordersheet_item_purchase_order_number > 0
				")->fetch();
				
				// if all order sheet items are ordered
				// set order sheet workflow state to sales order submitted
				if ($totalOrderedsheetItems[$ordersheet] == $orderedItems['total']) {
					
					// if order sheet has approved quantities set as sales order shubmittet
					// otherwise set as distributed
					$state = ($totalOrdersheetApprovedQuantites[$ordersheet] > 0) 
						? Workflow_State::STATE_SALES_ORDER_SUBITTED 
						: Workflow_State::STATE_DISTRIBUTED;
					
					
					// count archived ordersheets
					if ($state == Workflow_State::STATE_DISTRIBUTED) {
						$totalArchivedOrdersheets++;
					}
					
					$model->db->exec("
						UPDATE mps_ordersheets SET 
							mps_ordersheet_workflowstate_id = $state
						WHERE mps_ordersheet_id = $ordersheet
					");
					
					// tracking
					$workflowState->read($state);
					$user_tracking->create(array(
						'user_tracking_entity' => 'order sheet',
						'user_tracking_entity_id' => $ordersheet,
						'user_tracking_user_id' => $user->id,
						'user_tracking_action' => $workflowState->name,
						'user_created' => $user->login,
						'date_created' => date('Y-m-d H:i:s')
					));
				}
			}
			
			// if all ordersheets are archived
			// set mastersheets as aarchived
			if ($totalOrderSheets == $totalArchivedOrdersheets) {
				$model->db->exec("
					UPDATE mps_mastersheets SET
					mps_mastersheet_archived = 1
					WHERE mps_mastersheet_id = $mastersheet
				");
			}
		}

		
		/*
		 * Export ordered items in exchange system
		 */
		if ($export) {
			
			$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);
			
			$division = ($application=='mps') ? '08' : '09';
			
			// salesorders builder
			$salesorder = new Salesorders();
			
			// salesorder item  builder
			$salesorder_item = new Salesorders_Item();
			
			$salesorder_company = 'tissot';
			
			// set export data structure
			foreach ($export as $order => $row) {
				
				$increment = 1;
				$items = array();

				// order items
				foreach ($row['items'] as $id => $item) {
					
					if ($item['quantity'] && !$item['locally']) {
												
						$items[] = array(
							'mps_salesorderitem_ponumber' => $order,
							'mps_salesorderitem_linenumber' => $increment,
							'mps_salesorderitem_material_code' => $item['code'],
							'mps_salesorderitem_eannumber' => $item['ean'],
							'mps_salesorderitem_quantity' => $item['quantity'],
							'mps_salesorderitem_deliverydate' => $item['delivery_date']
						);
						
						$increment++;
					}
					
				}
				
				// export file
				if ($items) {
					
					$insert = $salesorder->create(array(
						'mps_salesorder_ponumber' => $order,
						'mps_salesorder_customernumber' => $row['customernumber'],
						'mps_salesorder_shipto' => $row['shipto'],
						'mps_salesorder_company' => $salesorder_company,
						'mps_salesorder_orderdate' => date('Y-m-d')
					));
					
					if ($insert) {
						
						// only for development server
						// insert orders as confirmed ramco orders
						if ($settings->devmod) {
							
							$exchange->db->exec("
								INSERT INTO ramco_orders (
									ramco_order_type,
									mps_salesorder_customernumber,
									mps_salesorder_ponumber,
									ramco_po_number,
									ramco_shipto_number,
									ramco_order_confirmation_date,
									ramco_division,
									ramco_order_deleted,
									mps_order_completely_imported
								)
								VALUES (
									'ZTA',
									'".$row['customernumber']."',
									'$order',
									'$order',
									'".$row['shipto']."',
									CURRENT_DATE,
									'$division',
									0,
									0
								)	
							");
						}
						
						foreach ($items as $item) {
							
							$salesorder_item->create(array(
								'mps_salesorderitem_salesorder_id' => $insert,
								'mps_salesorderitem_ponumber' => $item['mps_salesorderitem_ponumber'],
								'mps_salesorderitem_linenumber' => $item['mps_salesorderitem_linenumber'],
								'mps_salesorderitem_material_code' => $item['mps_salesorderitem_material_code'],
								'mps_salesorderitem_eannumber' => $item['mps_salesorderitem_eannumber'],
								'mps_salesorderitem_quantity' => $item['mps_salesorderitem_quantity'],
								'mps_salesorderitem_deliverydate' => $item['mps_salesorderitem_deliverydate']
							));
							
							// only for development server
							// insert order items in exchange system
							if ($settings->devmod) {
								
								if ($item['mps_salesorderitem_quantity']) {
									if ($item['mps_salesorderitem_quantity'] <= 50) {
										$quantity = $item['mps_salesorderitem_quantity'];
										$shortclosed = 0;
									} 
									elseif ($item['mps_salesorderitem_quantity'] > 100) {
										$quantity = 0;
										$shortclosed = 0;
									}
									else {
										$quantity = ceil($item['mps_salesorderitem_quantity']/2);
										$shortclosed = 1;
									} 
								} else {
									$quantity = 0;
									$shortclosed = 0;
								}
								
								// insert confirmed items
								$exchange->db->exec("
									INSERT INTO ramco_confirmed_items (
										mps_salesorder_ponumber,
										mps_salesorderitem_linenumber,
										mps_salesorderitem_material_code,
										ramco_eannumber,
										ramco_confirmed_quantity,
										ramco_order_state,
										mps_item_completely_imported
									)
									VALUES (
										'".$item['mps_salesorderitem_ponumber']."',
										'".$item['mps_salesorderitem_linenumber']."',
										'".$item['mps_salesorderitem_material_code']."',
										'".$item['mps_salesorderitem_eannumber']."',
										'$quantity',
										'00',
										0
									)
								");
								
								// insert shipped items
								$exchange->db->exec("
									INSERT INTO ramco_shipped_items (
										mps_salesorderitem_ponumber,
										mps_salesorderitem_linenumber,
										mps_salesorderitem_material_code,
										ramco_shipping_date,
										ramco_shipped_quantity,
										ramco_order_state,
										ramco_order_shortclosed,
										mps_item_completely_imported
									)
									VALUES (
										'".$item['mps_salesorderitem_ponumber']."',
										'".$item['mps_salesorderitem_linenumber']."',
										'".$item['mps_salesorderitem_material_code']."',
										CURRENT_DATE,
										'$quantity',
										'03',
										$shortclosed,
										0
									)
								");
							}
						}
					}
				}
			}
		}

		message::request_saved();
		$response = true;
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	// response with json header
	header('Content-Type: text/json');
	echo json_encode(array(
		'success' => $response,
		'message' => $message,
		'redirect' => $redirect		
	));
	