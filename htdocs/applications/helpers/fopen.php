<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
$user = User::instance();

$file = trim($_REQUEST['file']);
$filepath = $_SERVER['DOCUMENT_ROOT']."/$file";

if ($user->id) {
	
	if(file_exists($filepath)) {
		$mimetype = mime_content_type($filepath);
		header("Content-Type: $mimetype");
		header("Cache-Control: public");
		header('Pragma: no-cache');
		header('Content-Transfer-Encoding: binary');
		header('Accept-Ranges: bytes');
		echo file_get_contents($filepath);
	} else {
		header('HTTP/1.0 404 Not Found');
	    echo "<h1>404 Not Found</h1>";
	    echo "The page that you have requested could not be found.";
	}

	exit;
}
else {
    url::redirect('/security/login');
}