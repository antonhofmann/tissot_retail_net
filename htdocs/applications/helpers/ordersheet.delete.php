<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$ordersheet = new Ordersheet($appliction);
	$ordersheet->read($id);
	
	// delete permissions for manager
	$manager = ($user->permission(Ordersheet::PERMISSION_MANAGE) && $ordersheet->workflowstate_id == Workflow_State::STATE_PREPARATION) ? true : false;
	
	// delete permissions for standard master sheet
	$standard = ($_REQUEST['standard']==$ordersheet->mastersheet_id) ? true : false;
	
	if ($id && ($manager || $standard) ) {

		$delete = $ordersheet->delete();
		
		if ($delete) {
			
			$model = new Model($appliction);
			
			// delete ordershet items
			$model->db->query("
				DELETE FROM mps_ordersheet_items
				WHERE mps_ordersheet_item_ordersheet_id = $id		
			");
			
			// get ordersheet versions
			$result = $model->query("
				SELECT mps_ordersheet_version_id
				FROM mps_ordersheet_versions
				WHERE mps_ordersheet_version_ordersheet_id = $id
			")->fetchAll();
			
			if ($result) {
				
				foreach ($result as $row) {
					
					$version = $row['mps_ordersheet_version_id'];
					
					$model->db->query("
						DELETE FROM mps_ordersheet_versions
						WHERE mps_ordersheet_version_id = $version
					");
					
					$model->db->query("
						DELETE FROM mps_ordersheet_version_items
						WHERE mps_ordersheet_version_item_ordersheetversion_id = $version
					");
				}
			}
			
			Message::request_deleted();
			url::redirect("/$appliction/$controller");
			
		} else {
			Message::request_failure();
			url::redirect("/$appliction/$controller/$action/$id");
		}
		
	}
	else {
		Message::access_denied();
		url::redirect("/$appliction/$controller");
	}