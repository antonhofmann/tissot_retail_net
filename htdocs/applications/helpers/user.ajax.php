<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$translate = Translate::instance();

$auth = User::instance();
$db = Connector::get();

switch ($_REQUEST['section']) {
	
	case 'submit.preference':

		$sth = $db->prepare("
			DELETE FROM user_preferences
			WHERE user_preference_user_id = ?
			AND user_preference_entity = ?
			AND user_preference_name = ?
		");

		$sth->execute(array($auth->id, $_REQUEST['entity'], $_REQUEST['name']));

		if (!$_REQUEST['value']) break;

		$sth = $db->prepare("
			INSERT INTO user_preferences (
				user_preference_user_id,
				user_preference_entity,
				user_preference_name,
				user_preference_value
			)
			VALUES (?,?,?,?)
		");

		$json['success'] = $sth->execute(array(
			$auth->id, 
			$_REQUEST['entity'], 
			$_REQUEST['name'], 
			serialize($_REQUEST['value'])
		));

	break;
}

// response with json header
header('Content-Type: text/json');
echo json_encode($json);
