<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$redirect = $_REQUEST['redirect'];
	$pos = $_REQUEST['mps_pos_furniture_posaddress'];
	$item = $_REQUEST['mps_pos_furniture_id'];
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$data = array();
	$data['mps_pos_furniture_posaddress'] = $pos;
	$data['mps_pos_furniture_product_line_id'] = $_REQUEST['mps_pos_furniture_product_line_id'];
	$data['mps_pos_furniture_item_id'] = $_REQUEST['mps_pos_furniture_item_id'];
	$data['mps_pos_furniture_quantity_in_use'] = $_REQUEST['mps_pos_furniture_quantity_in_use'];
	$data['mps_pos_furniture_description'] = $_REQUEST['mps_pos_furniture_description'];

	if ($_REQUEST['mps_pos_furniture_item_id']=='new' && $_REQUEST['old_items']) {
		$data['mps_pos_furniture_item_id'] = $_REQUEST['old_items'];
	}
	
	if (isset($_REQUEST['mps_pos_furniture_quantity_on_stock'])) {
		$data['mps_pos_furniture_quantity_on_stock'] = ($_REQUEST['mps_pos_furniture_quantity_on_stock'])
			? $_REQUEST['mps_pos_furniture_quantity_on_stock']
			: null;
	}
	
	if (isset($_REQUEST['mps_pos_furniture_installation_year'])) {
		$data['mps_pos_furniture_installation_year'] = ($_REQUEST['mps_pos_furniture_installation_year'])
			? $_REQUEST['mps_pos_furniture_installation_year']
			: null;
	}
	
	if (isset($_REQUEST['mps_pos_furniture_deinstallation_year'])) {
		$data['mps_pos_furniture_deinstallation_year'] = ($_REQUEST['mps_pos_furniture_deinstallation_year'])
			? $_REQUEST['mps_pos_furniture_deinstallation_year']
			: null;
	}
	
	if ($pos && $data) { 
		
		$pos_furniture = new Pos_Furniture($application);
		$pos_furniture->read($item);
		
		if ($item) {
			$data['user_modified'] = $user->login;
			$response = $pos_furniture->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $item = $pos_furniture->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = "$redirect/$item";
		}		
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	