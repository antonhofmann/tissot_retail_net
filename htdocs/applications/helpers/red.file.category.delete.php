<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$fileCategory = new Red_File_Category($application);
	$fileCategory->read($id);

	if ($fileCategory->id && user::permission(RED::PERMISSION_DELETE_SYSTEM_DATA)) {
		
		$integrity = new Integrity();
		$integrity->set($id, 'red_filecategories', $application);

		if ($integrity->check()) {

			$response = $fileCategory->delete();

			if ($response) {
				message::request_deleted();
				url::redirect("/$application/$controller");
			} else {
				message::request_failure();
				url::redirect("/$application/$controller/$action/$id");
			}
		}
		else {
			message::request_failure();
			url::redirect("/$application/$controller/$action/$id");
		}

	} else {
		message::request_failure();
		url::redirect("/$application/$controller");
	}