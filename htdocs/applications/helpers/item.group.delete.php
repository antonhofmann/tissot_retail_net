<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$appliction = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$itemGroup = new Item_Group();
$itemGroup->read($id);

$permission_edit = user::permission('can_edit_catalog');
$permission_edit_items = user::permission('can_edit_items_in_catalogue');
$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

if ($itemGroup->id && $_CAN_EDIT) {
	
	$delete = $itemGroup->delete();
	
	if ($delete) {
		Message::request_deleted();
		url::redirect("/$appliction/$controller");
	} else {
		Message::request_failure();
		url::redirect("/$appliction/$controller/$action/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$appliction/$controller");
}