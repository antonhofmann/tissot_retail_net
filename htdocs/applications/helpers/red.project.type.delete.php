<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$projectType = new Red_Project_Type($application);
	$projectType->read($id);

	if ($projectType->id && user::permission(RED::PERMISSION_DELETE_SYSTEM_DATA)) {
		
		$integrity = new Integrity();
		$integrity->set($id, 'red_projecttypes', $application);

		if ($integrity->check()) {

			$response = $projectType->delete();

			if ($response) {
				Message::request_deleted();
				url::redirect("/$application/$controller");
			} else {
				Message::request_failure();
				url::redirect("/$application/$controller/$action/$id");
			}
		}
		else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$id");
		}

	} else {
		Message::request_failure();
		url::redirect("/$application/$controller");
	}