<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$translate = Translate::instance();
	$application = $_REQUEST['application'];
	$section = $_REQUEST['section'];
	$user = User::instance();
	
	$sessionFilter = session::filter($application, "pos.list.");
	
	$model = new Model(Connector::DB_CORE);
	
	$binds = array(
		Pos_Opening_Hour::DB_BIND_POSADDRESSES,
		Pos::DB_BIND_COUNTRIES,
		Pos::DB_BIND_PLACES,
		Pos::DB_BIND_PROVINCES,
		Pos::DB_BIND_POSOWNER_TYPES,
		Pos::DB_BIND_POSTYPES
	);
	
	if( !$user->permission(Pos::PERMISSION_EDIT) && !$user->permission(Pos::PERMISSION_VIEW)) {
		$filters['limited'] = "posaddress_client_id = ".$user->address;
	}
	
	if ($_REQUEST['country']) {
		$filters['country'] = 'posaddress_country = '.$_REQUEST['country'];
	}
	
	if ($_REQUEST['place']) {
		$filters['place'] = 'posaddress_place_id = '.$_REQUEST['place'];
	}
	
	if ($sessionFilter['search'] && $sessionFilter['search'] <> $translate->search) {

		$keyword = $sessionFilter['search'];

		$filters['search'] = "(
			posaddress_name LIKE \"%$keyword%\"
			OR country_name LIKE \"%$keyword%\"
			OR province_canton LIKE \"%$keyword%\"
			OR place_name LIKE \"%$keyword%\"
			OR posaddress_zip LIKE \"%$keyword%\"
			OR postype_name LIKE \"%$keyword%\"
		)";
	}
	
	if ($_REQUEST['ownertype']) {
		$filters['posowner_types'] = 'posaddress_ownertype = '.$_REQUEST['ownertype'];
	} 
	elseif ($sessionFilter['posowner_types']) {
		foreach ($sessionFilter['posowner_types'] as $key => $value) {
			$legaltypes[] = "posaddress_ownertype=$key";
		}
		$filters['posowner_types'] = "(".join(' OR ', $legaltypes).")";
	}
	
	// filter: pos type
	if ($sessionFilter['pos_types']) 	{
		$filters['pos_types'] = "posaddress_store_postype = ".$sessionFilter['pos_types'];
	}

	// filter: distribution channel
	if ($sessionFilter['distribution_channels']) {
		$filters['distribution_channels'] = "posaddress_distribution_channel=".$sessionFilter['distribution_channels'];
	}

	// filter: sales representative
	if ($sessionFilter['sales_representative']) {
		$filters['sales_representative'] = "posaddress_sales_representative=".$sessionFilter['sales_representative'];
	}

	// filter: decoration person
	if ($sessionFilter['decoration_person']) {
		$filters['decoration_person'] = "posaddress_decoration_person=".$sessionFilter['decoration_person'];
	}

	// filter: turnoverclass watches
	if ($sessionFilter['turnoverclass_watches']) {
		$filters['turnoverclass_watches'] = "posaddress_turnoverclass_watches=".$sessionFilter['turnoverclass_watches'];
	}

	// filter: turnoverclass bijoux
	if ($sessionFilter['turnoverclass_bijoux']) {
		$filters['turnoverclass_bijoux'] = "posaddress_turnoverclass_bijoux=".$sessionFilter['turnoverclass_bijoux'];
	}

	// filter: product lines
	if ($sessionFilter['product_lines']) {
		foreach ($sessionFilter['product_lines'] as $key => $value) {
			$productlines[] = "posaddress_store_furniture=$key";
		}
		$filters['product_lines'] = "(".join(' OR ', $productlines).")";
	}
	
	$filters['id'] = "posaddress_id != ".$_REQUEST['id'];
		
	switch ($section) {
		
		case 'place':
			
			$json[][''] = 'All Cities';
			
			$result = $model->query("
				SELECT DISTINCT 
					place_id, place_name
				FROM posopeninghrs
			")
			->bind($binds)
			->filter($filters)
			->fetchAll();
	
			if ($result) {
				foreach ($result as $row) {
					$json[][$row['place_id']] = $row['place_name'];
				}
			}
			
		break;
		
		case 'ownertype':
		
			$json[][''] = 'All POS Owner Types';
			
			$result = $model->query("
				SELECT DISTINCT 
					posowner_type_id, posowner_type_name
				FROM posopeninghrs
			")
			->bind($binds)
			->filter($filters)
			->fetchAll();
	
			if ($result) {
				foreach ($result as $row) {
					$json[][$row['posowner_type_id']] = $row['posowner_type_name'];
				}
			}
		
		break;
		
		case 'pos':
			
			$json[][''] = 'All POS';
			
			$binds[] = 'INNER JOIN addresses ON posaddress_client_id = address_id';
				
			$result = $model->query("
				SELECT DISTINCT 
					posaddress_id, CONCAT(address_company, ' - ', posaddress_name) AS name
				FROM posopeninghrs
			")
			->bind($binds)
			->filter($filters)
			->fetchAll();
	
			if ($result) {
				foreach ($result as $row) {
					$json[][$row['posaddress_id']] = $row['name'];
				}
			}
			
		break;
	}
	
	header('Content-Type: text/json');
	echo json_encode($json);