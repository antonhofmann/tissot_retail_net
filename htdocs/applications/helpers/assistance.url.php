<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$id = $_REQUEST['id'];
	$section = $_REQUEST['section'];
	$link = $_REQUEST['url'];
	$overAllSections = $_REQUEST['overAllSections'];

	switch ($_REQUEST['action']) {
		
		case 'save':

			$assistance = new Assistance();
			$assistance->read($id);
			$assistance->section()->read($section);
			
			if ($assistance->section()->id) {
				
				// assistance found
				$response = true;
				
				// read assistance URL
				$assistance->section()->link()->read($link);
				
				// if URL exist, show warning mesage
				if ($assistance->section()->link()->id) {
					
					$notification['properties'] = array('theme' => 'error');
					$notification['content'] = Translate::instance()->warning_assistance_section_link_link;
					
				} else {
					
					// insert URL for this assistance
					$success = $assistance->section()->link()->create($link);
					
					if ($assistance->section()->link()->id) {
						
						// add link to all sections
						if ($overAllSections) {
							
							$sections = $assistance->getAllSections();
							
							if ($sections) {
								
								foreach ($sections as $row) {
									
									$key = $row['assistance_section_id'];
									
									if ($key <> $section) {
									
										$assistance->section()->link()->id = null;
										$assistance->section()->read($key);
										$assistance->section()->link()->read($link);
										
										// insert linkt to all assistance sections
										if (!$assistance->section()->link()->id) {
											$success = $assistance->section()->link()->create($link);
										}
									}
								}
							}
						}
						
						$notification['content'] = Translate::instance()->message_request_submitted;
						
					} else {
						
						$notification['properties'] = array('theme' => 'error');
						$notification['content'] = Translate::instance()->message_request_failure;
					}
				} 
				
			} else {
				
				$notification['properties'] = array('theme' => 'error');
				$notification['content'] = Translate::instance()->message_request_failure;	
			}
			
		break;
		
		case 'remove' :
			
			$assistance = new Assistance();
			$assistance->read($id);
			$assistance->section()->read($section);
				
			if ($assistance->section()->id) {
			
				// assistance found
				$response = true;
			
				// read assistance URL
				$assistance->section()->link()->read($link);
				
				// if URL exist, show warning mesage
				if ($assistance->section()->link()->id) {
					
					// remove url
					$success = $assistance->section()->link()->delete($link);
					
					if ($success) {
						
						$notification['content'] = Translate::instance()->message_request_deleted;
						
					} else {
						
						$notification['properties'] = array('theme' => 'error');
						$notification['content'] = Translate::instance()->message_request_failure;
					}
					
				} else {
					
					$notification['properties'] = array('theme' => 'error');
					$notification['content'] = Translate::instance()->message_request_failure;				
				}
			} else {
				
				$notification['properties'] = array('theme' => 'error');
				$notification['content'] = Translate::instance()->message_request_failure;	
			}
			
		break;
	}
	
	header('Content-Type: text/json');
	
	echo json_encode(array(
		'response' => $response,
		'success' => $success,
		'notification' => $notification
	));