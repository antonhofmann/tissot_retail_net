<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$id = $_REQUEST['mps_material_id'];
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$data = array();
	
	if (in_array('mps_material_material_collection_id', $fields)) {
		$data['mps_material_material_collection_id'] = $_REQUEST['mps_material_material_collection_id'];
	}
	
	if (in_array('mps_material_material_collection_category_id', $fields)) {
		$data['mps_material_material_collection_category_id'] = $_REQUEST['mps_material_material_collection_category_id'];
	}
	
	if (in_array('mps_material_material_category_id', $fields)) {
		$data['mps_material_material_category_id'] = $_REQUEST['mps_material_material_category_id'];
	}
	
	if (in_array('mps_material_material_planning_type_id', $fields)) {
		$data['mps_material_material_planning_type_id'] = $_REQUEST['mps_material_material_planning_type_id'];
	}
	
	if (in_array('mps_material_material_purpose_id', $fields)) {
		$data['mps_material_material_purpose_id'] = $_REQUEST['mps_material_material_purpose_id'];
	}
	
	if (in_array('mps_material_ean_number', $fields)) {
		$data['mps_material_ean_number'] = $_REQUEST['mps_material_ean_number'];
	}
	
	if (in_array('mps_material_hsc', $fields)) {
		$data['mps_material_hsc'] = $_REQUEST['mps_material_hsc'];
	}
	
	if (in_array('mps_material_code', $fields)) {
		$data['mps_material_code'] = $_REQUEST['mps_material_code'];
	}
	
	if (in_array('mps_material_name', $fields)) {
		$data['mps_material_name'] = $_REQUEST['mps_material_name'];
	}
	
	if (in_array('mps_material_price', $fields)) {
		$data['mps_material_price'] = $_REQUEST['mps_material_price'];
	}
	
	if (in_array('mps_material_currency_id', $fields)) {
		$data['mps_material_currency_id'] = $_REQUEST['mps_material_currency_id'];
	}
	
	if (in_array('mps_material_setof', $fields)) {
		$data['mps_material_setof'] = $_REQUEST['mps_material_setof'] ?: 1;
	}
	
	if (in_array('mps_material_description', $fields)) {
		$data['mps_material_description'] = $_REQUEST['mps_material_description'] ?: null;
	}
	
	if (in_array('mps_material_islongterm', $fields)) {
		$data['mps_material_islongterm'] = ($_REQUEST['mps_material_islongterm']) ? 1 : 0;
	}
	
	if(in_array('mps_material_active', $fields)) {
		$data['mps_material_active'] =($_REQUEST['mps_material_active']) ? 1 : 0;
	}
	
	if (in_array('mps_material_width', $fields)) {
		$data['mps_material_width'] = $_REQUEST['mps_material_width'] ?: null;
	}
		
	if (in_array('mps_material_height', $fields)) {
		$data['mps_material_height'] = $_REQUEST['mps_material_height'] ?: null;
	}
	
	if (in_array('mps_material_length', $fields)) {
		$data['mps_material_length'] = $_REQUEST['mps_material_length'] ?: null;
	}
	
	if (in_array('mps_material_radius', $fields)) {
		$data['mps_material_radius'] = $_REQUEST['mps_material_radius'] ?: null;
	}

	if ($data) {
		
		// material builder
		$material = new Material($application);
		
		// update existed material
		if ($id) {
			
			$material->read($id);
			
			$data['user_modified'] = $user->login;
			
			$response = $material->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;

			if ($response) {
				
				$model = new Model($application);
				
				$stateArchived = Workflow_State::STATE_DISTRIBUTED;
				
				// get all no archived order sheets
				$result = $model->query("
					SELECT 
						mps_ordersheet_item_id,
						mps_ordersheet_item_purchase_order_number
					FROM mps_ordersheet_items
					INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
					INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
					WHERE
						mps_ordersheet_item_material_id = $id
						AND mps_ordersheet_workflowstate_id != $stateArchived
				")->fetchAll();
				
				// update all order sheets price
				if ($result) {
					
					$ordersheetItem = new Ordersheet_Item($application);
					
					foreach ($result as $row) {
						
						if (!$row['mps_ordersheet_item_purchase_order_number']) { 
							
							$ordersheetItem->read($row['mps_ordersheet_item_id']);
							
							$ordersheetItem->update(array(
								'mps_ordersheet_item_price'	=> $data['mps_material_price'],
								'user_modified' => $user->login,
								'date_modified' => date('Y-m-d H:i:s')
							));
						}
					}
				}
			}
		}
		// add new material
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $material->create($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		
		// forced redirect form
		if ($response) {
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
			($data['date_created']) ? Message::request_inserted() : Message::request_updated();
		}
	}
	else {
		
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));
	