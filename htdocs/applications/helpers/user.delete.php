<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
		
	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	$user = $_REQUEST['user'];
	
	if ($id && User::PERMISSION_EDIT) {
	
		$User = new User($user);
		$delete = $User->delete();
	
		if ($delete) {
			Message::request_deleted();
			url::redirect("/$application/$controller/$id");
		} else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$id/$user");
		}
	}
	else {
		Message::access_denied();
		url::redirect("/$application/$controller/$id");
	}