<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	if ($id && $user->permission(Mastersheet::PERMISSION_EDIT)) {
		
		$mastersheet_file = new Mastersheet_File($appliction);
		$mastersheet_file->read($id);
		$mastersheet = $mastersheet_file->mastersheet_id;
		
		$delete = $mastersheet_file->delete();
		
		if ($delete) {
			file::remove($mastersheet_file->path);
			Message::request_deleted();
			url::redirect("/$appliction/$controller/$action/$mastersheet");
		} else {
			Message::request_failure();
			url::redirect("/$appliction/$controller/$action/$mastersheet/$id");
		}
	}
	else {
		Message::access_denied();
		url::redirect("/$appliction/$controller/$action");
	}