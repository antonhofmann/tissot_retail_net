<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$model = new Model($application);
	
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			posfile_id,
			posfile_title,
			posfile_path,
			LEFT(posfile_description,120) AS posfile_description,
			DATE_FORMAT(posfiles.date_created, '%d.%m.%Y') AS created_on,
			posfilegroup_id,
			posfilegroup_name,
			posfile_posorder,
			posorder_ordernumber
		FROM db_retailnet.posfiles
		INNER JOIN db_retailnet.posfilegroups ON posfilegroup_id = posfile_filegroup
		LEFT JOIN db_retailnet.posorders ON posorder_id = posfile_posorder
		WHERE posfile_posaddress = $id
		ORDER BY posfilegroup_name
	")->fetchAll();

	$_FILES = array();
	$_ORDER_FILES = array();

	if ($result) {
		
		foreach ($result as $row) {

			$group = $row['posfilegroup_id'];
			$file = $row['posfile_id'];
			$order = $row['posfile_posorder'];
			$extension = file::extension($row['posfile_path']);

			if ($order) {
				$_ORDER_FILES[$order]['caption'] = $row['posorder_ordernumber'];
				$_ORDER_FILES[$order]['groups'][$group]['caption'] = $row['posfilegroup_name'];
				$_ORDER_FILES[$order]['groups'][$group]['files'][$file]['title'] = $row['posfile_title']."<span>{$row[posfile_description]}</span>";
				$_ORDER_FILES[$order]['groups'][$group]['files'][$file]['date']= "<span>$translate->date_created :<br />{$row[created_on]}</span>";
				$_ORDER_FILES[$order]['groups'][$group]['files'][$file]['path'] = $row['posfile_path'];
				$_ORDER_FILES[$order]['groups'][$group]['files'][$file]['fileicon']  = "<span class='file-extension $extension modal' tag='{$row[posfile_path]}'></span>";
			} else {
				$_FILES[$group]['caption'] = $row['posfilegroup_name'];
				$_FILES[$group]['files'][$file]['title'] = $row['posfile_title']."<span>{$row[posfile_description]}</span>";
				$_FILES[$group]['files'][$file]['date']= "<span>$translate->date_created :<br />{$row[created_on]}</span>";
				$_FILES[$group]['files'][$file]['path'] = $row['posfile_path'];
				$_FILES[$group]['files'][$file]['fileicon']  = "<span class='file-extension $extension modal' tag='{$row[posfile_path]}'></span>";
			}
		}
	}
	
	if ($_FILES) {
		foreach ($_FILES as $key => $row) {
			$table = new Table(array('thead' => false));
			$table->datagrid = $row['files'];
			$table->fileicon();
			$table->title("href=".$_REQUEST['form']);
			$table->date();
			$posfiles .= "<h6>".$row['caption']."</h6>";
			$posfiles .= $table->render();
		}
	}

	if ($_ORDER_FILES) {
		
		$posfiles .= "<br /><br />";

		foreach ($_ORDER_FILES as $order => $groups) {

			$posfiles .= "<h4>Files of Project {$groups[caption]}</h4>";

			foreach ($groups['groups'] as $group => $row) {
				$table = new Table(array('thead' => false));
				$table->datagrid = $row['files'];
				$table->fileicon();
				$table->title();
				$table->date();
				$posfiles .= "<h6>".$row['caption']."</h6>";
				$posfiles .= $table->render();
			}
		}
	}
	
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}
	
	if ($toolbox) {
		$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
	}
	
	echo $toolbox;
	echo $posfiles;
	
	
	