<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$pos = $_REQUEST['mps_pos_material_posaddress'];
	$material_id = $_REQUEST['mps_pos_material_id'];
	$redirect = $_REQUEST['redirect'];
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$data = array();
	$data['mps_pos_material_posaddress'] = $pos;
	$data['mps_pos_material_material_id'] = $_REQUEST['mps_pos_material_material_id'];
	$data['mps_pos_material_quantity_in_use'] = $_REQUEST['mps_pos_material_quantity_in_use'];
	
	if (isset($_REQUEST['mps_pos_material_quantity_on_stock'])) {
		$data['mps_pos_material_quantity_on_stock'] = ($_REQUEST['mps_pos_material_quantity_on_stock'])
			? $_REQUEST['mps_pos_material_quantity_on_stock']
			: null;
	}
	
	if (isset($_REQUEST['mps_pos_material_installation_year'])) {
		$data['mps_pos_material_installation_year'] = ($_REQUEST['mps_pos_material_installation_year'])
			? $_REQUEST['mps_pos_material_installation_year']
			: null;
	}
	
	if (isset($_REQUEST['mps_pos_material_deinstallation_year'])) {
		$data['mps_pos_material_deinstallation_year'] = ($_REQUEST['mps_pos_material_deinstallation_year'])
			? $_REQUEST['mps_pos_material_deinstallation_year']
			: null;
	}
	
	if ($pos) {
		
		$pos_material = new Pos_Material($application);
		$pos_material->read($material_id);
		
		if ($material_id) {
			$data['user_modified'] = $user->login;
			$response = $pos_material->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $material_id = $pos_material->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = "$redirect/$material_id";
		}		
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	