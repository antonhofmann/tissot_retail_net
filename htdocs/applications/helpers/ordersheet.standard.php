<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$companies = $_REQUEST['companies'];
	
	if ($companies) {
		
		$model = new Model($application);
		
		// load standard items
		$result = $model->query("
			SELECT DISTINCT
				mps_material_address_id,
				mps_material_id,
				mps_material_material_collection_category_id,
				mps_material_material_planning_type_id,
				mps_material_price,
				mps_material_standard_order_quantity,
				currency_id,
				currency_exchange_rate,
				currency_factor,
				address_id,
				address_company,
				IF (
					mps_material_standard_order_date, 
					DATE_FORMAT(mps_material_standard_order_date,'%d.%m.%Y'),
					''
				) AS mps_material_standard_order_date
			FROM mps_material_addresses
			INNER JOIN mps_materials ON mps_material_address_material_id = mps_material_id
			INNER JOIN db_retailnet.addresses ON address_id = mps_material_address_address_id 
			INNER JOIN db_retailnet.currencies ON currency_id = mps_material_currency_id 
			WHERE mps_material_active = 1 AND address_active = 1
		")->fetchAll();
		
		if ($result) {
			
			$mastersheet = new Mastersheet($application);
			$ordersheet = new Ordersheet($application);
			
			$data = array();
			
			// current date
			$current_date = date('Y.m.d');
			
			// delivery date
			$days = $settings->ordersheet_standard_delivery_days;
			$delivery_date = date('Y-m-d', strtotime("+$days day"));
			
			// get all standard active items
			foreach ($result as $row) {
				
				$company = $row['address_id'];
				
				if ($companies[$company]) {
					
					$planning = $row['mps_material_material_planning_type_id'];
					$collection = $row['mps_material_material_collection_category_id'];
					$material = $row['mps_material_id'];
					
					// item date available until
					if ($row['mps_material_standard_order_date']) {
						$permitted = (date::timestamp() <= date::timestamp($row['mps_material_standard_order_date'])) ? true : false;
					} else {
						$permitted = true;
					}
					
					if ($permitted && $planning && $collection) {
						
						// master sheet items
						$data[$company]['items'][$material] = array(
							'price' => $row['mps_material_price'],
							'currency' => $row['currency_id'],
							'rate' => $row['currency_exchange_rate'],
							'factor' => $row['currency_factor'],
							'proposed' => $row['mps_material_standard_order_quantity']
						);
						
						// master sheet name
						if (!$data[$company]['name']) {
							$company_name = $row['address_company'];
							$mastersheet_name = "Standard Orders $company_name $current_date";
							$data[$company]['name'] = $mastersheet_name;
						}
						
						// master sheet delivery dates
						if (!$data[$company]['date'][$planning][$collection]) {
							$data[$company]['deliveries'][$planning][$collection] = $delivery_date;
						}
					}
				}
			}
			// print_r($data); die();
			foreach ($companies as $company => $value) {					
					
				if ($data[$company]) {
				
					$mastersheet_name = $data[$company]['name'];
					$items = $data[$company]['items'];
					$delivery_dates = $data[$company]['deliveries'];
					
					// check master sheet name
					$check = $model->query("
						SELECT COUNT(mps_mastersheet_id) AS total
						FROM mps_mastersheets
						WHERE mps_mastersheet_name LIKE '$mastersheet_name%'		
					")->fetch();
					
					if ($check['total']>0) {
						$version = ($check['total']>0) ? $check['total']+1 : null;
						$mastersheet_name = $mastersheet_name.' '.$version;
					}
					
					// create master sheet
					$mastersheet->create(array(
						'mps_mastersheet_year' => date('Y'),
						'mps_mastersheet_name' => $mastersheet_name,
						'user_created' => $user->login,
						'date_created' => date('Y-m-d H:i:s')
					));
					
					if ($mastersheet->id) {
						
						// create master sheet items
						if ($items) {
							foreach ($items as $item => $row) {
								$mastersheet->item()->create($item);
							}
						}
						
						// create delivery dates
						if ($delivery_dates) {
							foreach ($delivery_dates as $planning => $collections) {
								foreach ($collections as $collection => $date) {
									$mastersheet->deliveryDate()->create($planning, $collection, $date);
								}
							}
						}
						
						// create order sheet
						$id = $ordersheet->create(array(
							'mps_ordersheet_workflowstate_id' => Workflow_State::STATE_OPEN,
							'mps_ordersheet_address_id' => $company,
							'mps_ordersheet_mastersheet_id' => $mastersheet->id,
							'mps_ordersheet_openingdate' => date('Y-m-d'),
							'mps_ordersheet_closingdate' => $delivery_date,
							'user_created' => $user->login,
							'date_created' => date('Y-m-d H:i:s')
						));
						
						// create ordersheet items
						if ($ordersheet->id && $items) {
							foreach ($items as $item => $row) {
								$ordersheet->item()->create(array(
									'mps_ordersheet_item_ordersheet_id' => $ordersheet->id,
									'mps_ordersheet_item_material_id' => $item,
									'mps_ordersheet_item_price' => $row['price'],
									'mps_ordersheet_item_currency' => $row['currency'],
									'mps_ordersheet_item_exchangrate' => $row['rate'],
									'mps_ordersheet_item_factor' => $row['factor'],
									'mps_ordersheet_item_quantity_proposed' => $row['proposed'],
									'user_created' => $user->login,
									'date_created' => date('Y-m-d H:i:s')
								));
							}
						}
					}
				}
			}
		}
		
		$response = ($data) ? true : false;
		$redirect = ($response && count($data)==1) ? "/$application/ordersheets/items/$id" : null;
		$message = ($response) ? 'Standard Order Sheet was created successfully' : $translate->message_request_failure;
		
		// jgrowl message for clients
		if ($response && $redirect) message::add(array(
			'type' => 'message',
			'keyword' => 'Standard Order Sheet was created successfully',
			'life' => 5000
		));
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}

	// response with json header
	header('Content-Type: text/json');
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect		
	));
	