<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$search = $_REQUEST['search'];
	$fields = $_REQUEST['fields'];
	$mastersheet = $_REQUEST['mastersheet'];
	$splittinglist = $_REQUEST['splittinglist'];
	
	// db connector
	$model = new Model($application);
	
	// filter: mastersheet
	$filters['mastersheet'] = "mps_mastersheet_splitting_list_item_splittinglist_id = $splittinglist";
	
	// ordersheet items
	$result = $model->query("
		SELECT 
			mps_mastersheet_splitting_list_item_id,
			mps_mastersheet_splitting_list_item_material_id,
			mps_mastersheet_splitting_list_item_price,
			mps_mastersheet_splitting_list_item_total_quantity,
			mps_mastersheet_splitting_list_item_total_quantity_approved,
			(mps_mastersheet_splitting_list_item_price * mps_mastersheet_splitting_list_item_total_quantity_approved) as total_cost,
			mps_material_planning_type_id,
			mps_material_planning_type_name,
			mps_material_collection_category_id,
			mps_material_collection_category_code,
			mps_material_collection_code,
			mps_material_code,
			mps_material_name,
			mps_material_setof,
			currency_symbol
		FROM mps_mastersheet_splitting_list_items
	")
	->bind(Mastersheet_Splitting_List_Item::DB_BIND_MATERIALS)
	->bind(Material::DB_BIND_COLLECTIONS)
	->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
	->bind(Material::DB_BIND_PLANNING_TYPES)
	->bind(Material::DB_BIND_CURRENCIES)
	->filter($filters)
	->order('mps_material_planning_type_name')
	->order('mps_material_collection_category_code')
	->order('mps_material_code')
	->order('mps_material_name')
	->fetchAll();
	
	if ($result) {
		
		$datagrid = array();
	
		
		// group items by material planning types
		// and material collection categories
		foreach ($result as $row) {
			
			$planning = $row['mps_material_planning_type_id'];
			$category = $row['mps_material_collection_category_id'];
			$item = $row['mps_mastersheet_splitting_list_item_id'];
			
			$datagrid[$planning]['caption'] = $row['mps_material_planning_type_name'];
			$datagrid[$planning]['data'][$category]['caption'] = $row['mps_material_collection_category_code'];
			$datagrid[$planning]['data'][$category]['data'][$item]['mps_material_collection_code'] = $row['mps_material_collection_code'];
			$datagrid[$planning]['data'][$category]['data'][$item]['mps_material_code'] = $row['mps_material_code'];
			$datagrid[$planning]['data'][$category]['data'][$item]['mps_material_name'] = $row['mps_material_name'];
			$datagrid[$planning]['data'][$category]['data'][$item]['mps_mastersheet_splitting_list_item_price'] = $row['mps_mastersheet_splitting_list_item_price'];
			$datagrid[$planning]['data'][$category]['data'][$item]['currency_symbol'] = $row['currency_symbol'];
			$datagrid[$planning]['data'][$category]['data'][$item]['mps_material_setof'] = $row['mps_material_setof'];
			$datagrid[$planning]['data'][$category]['data'][$item]['mps_mastersheet_splitting_list_item_total_quantity'] = ($row['mps_mastersheet_splitting_list_item_total_quantity']) ? $row['mps_mastersheet_splitting_list_item_total_quantity'] : 0;
			$datagrid[$planning]['data'][$category]['data'][$item]['mps_mastersheet_splitting_list_item_total_quantity_approved'] = ($row['mps_mastersheet_splitting_list_item_total_quantity_approved']) ? $row['mps_mastersheet_splitting_list_item_total_quantity_approved'] : 0;
			$datagrid[$planning]['data'][$category]['data'][$item]['total_cost'] = $row['total_cost'];
		}
	
			
		// visible fields
		foreach ($datagrid as $key => $row) {

			$list .= "<h5 data=$key >".$row['caption']."</h5>";

			foreach ($row['data'] as $subkey => $subrow) {
					
				$list .= "<h6 data=$key >".$subrow['caption']."</h6>";

				$totalprice = 0;
				$tableKey = $key."-".$subkey;
					
				$table = new Table(array(
					'id' => $tableKey
				));

				$table->datagrid = $subrow['data'];
				$table->dataloader($dataloader);

				$table->mps_material_collection_code(
					Table::ATTRIBUTE_NOWRAP,
					'width=10%'
				);

				$table->mps_material_code(
					Table::ATTRIBUTE_NOWRAP,
					'width=10%'
				);

				$table->mps_material_name();

				$table->mps_material_setof(
					Table::ATTRIBUTE_ALIGN_RIGHT,
					Table::ATTRIBUTE_NOWRAP,
					'width=5%'
				);

				$table->mps_mastersheet_splitting_list_item_price(
					Table::ATTRIBUTE_ALIGN_RIGHT,
					Table::ATTRIBUTE_NOWRAP,
					'width=5%'
				);

				$table->currency_symbol(
					Table::ATTRIBUTE_NOWRAP,
					'width=20px'
				);

				$table->mps_mastersheet_splitting_list_item_total_quantity(
					Table::ATTRIBUTE_ALIGN_RIGHT,
					Table::ATTRIBUTE_NOWRAP,
					Table::PARAM_GET_FROM_LOADER,
					'width=5%'
				);

				$table->mps_mastersheet_splitting_list_item_total_quantity_approved(
					Table::ATTRIBUTE_ALIGN_RIGHT,
					Table::ATTRIBUTE_NOWRAP,
					'width=5%'
				);
			
				$table->total_cost(
					Table::ATTRIBUTE_ALIGN_RIGHT,
					Table::ATTRIBUTE_NOWRAP,
					'width=5%'
				);

				// total collection price
				foreach ($subrow['data'] as $key => $array) {
					$totalprice = $totalprice+$array['total_cost'];
				}

				$totalprice = number_format($totalprice, $settings->decimal_places, '.', '');
					
				$list .= $table->render(array('order'=>true));
				$list .= "
					<div class='totoal-collection'>
						".$translate->total_cost." (<em>".$subrow['caption']."</em>): 
						<b class='total-items $tableKey'>$totalprice</b>
				</div>";
			}
		}
	}
	else {
		$list = html::emptybox($translate->empty_result);
	}
	
	echo $list;
	
	