<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$response = array();

switch ($_REQUEST['section']) {

	case 'get':

		$distributionChannel = new DistChannel();
		$distributionChannel->read($_REQUEST['id']);
		$response = $distributionChannel->data;

	break;
}

// response with json header
header('Content-Type: text/json');
echo json_encode($response);
