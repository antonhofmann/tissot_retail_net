<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// request vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	// mastersheet vars
	$mastersheet = $_REQUEST['mps_mastersheet_id'];
	$selected = $_REQUEST['selected_companies'];

	// deletable statments
	$deletable_statments = Ordersheet_State::loader('delete');
	
	if ($selected) {
		
		$deleted = array();
		$not_deleted = array();
		
		$ordersheets = explode(',', $selected);
		
		// ordersheet
		$ordersheet = new Ordersheet($application);
		
		// ordersheet version builder
		$ordersheet_version = new Ordersheet_Version($application);
	
		foreach ($ordersheets as $id) {
			
			// get ordershet data
			$ordersheet->read($id);
			
			// order sheet workflow state is deletable
			if (in_array($ordersheet->workflowstate_id, $deletable_statments)) {
					
				// delete order sheet
				$response = $ordersheet->delete();
				
				if ($response) {
					
					// get order sheet versions
					$versions = $ordersheet->version()->load();

					// remove all order sheet versions and
					// order sheet version items
					if ($versions) {
						foreach ($versions as $version => $data) {
						
							// read version instance
							$ordersheet->version()->read($version);
							
							// delete order sheet version
							$ordersheet->version()->delete();
							
							// delete all order sheet version items
							$ordersheet->version()->item()->deleteAll();
						}
					}
					
					// get all order sheet items
					$items = $ordersheet->item()->load();
					
					// remove all order sheet items and
					// order sheet items quantity planned and
					// order sheet items quantity confirmed and
					// order sheet items quantity shipped and
					// order sheet items quantity distributed
					if ($items) {
						foreach ($items as $key => $item) {
							
							$ordersheet->item()->read($key);
							
							$_response = $ordersheet->item()->delete();
							
							if ($_response) {
								$ordersheet->item->planned()->delete_all();
								$ordersheet->item()->confirmed()->delete_all();
								$ordersheet->item()->shipped()->delete_all();
								$ordersheet->item()->delivered()->delete_all();
							}
						}
					}
					
					// set  as deleted
					array_push($deleted, $id);
				}
				else {
					array_push($not_deleted, $id);
				}
				
			} 
			else {
				array_push($not_deleted, $id);
			}
		}
		
		// db application model
		$model = new Model($application);
		
		// check if master sheet has order sheets
		$has_ordersheets = $model->query("
			SELECT mps_ordersheet_id 
			FROM mps_ordersheets
			WHERE mps_ordersheet_workflowstate_id IN (".join(',', $deletable_statments).") AND mps_ordersheet_mastersheet_id = $mastersheet
		")->fetchAll();
		
		$response = true;
		$message = $translate->message_request_deleted;
		$tab = ($has_ordersheets) ? 'existing_ordersheets' : 'new_ordersheets';
		
		if ($not_deleted) {
			
			$selected = join(',', $not_deleted);
			$tab_items = true;
			
			$message .= "<br /><br />";
			$message .= "Only order sheets with the one of the following workflow states can be deleted: ";
			$message .= "<b><b>in preparation</b>, <b>open</b>,, in progress</b>, and <b>in revision</b>. ";
		}
	} 
	else {
		$response = false;
		$message =$translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'tab' => $tab,
		'tab_items' => $tab_items,
		'reset' => true,
		'selected_companies' => $selected
	));
	