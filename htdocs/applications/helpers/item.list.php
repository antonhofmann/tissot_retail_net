<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

$permission_edit = user::permission('can_edit_catalog');
$permission_edit_items = user::permission('can_edit_items_in_catalogue');
$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;

$model = new Model($application); 

if ($_REQUEST['type']) {
	$filters['type'] = "item_type = ".$_REQUEST['type'];
}
elseif ($application=='catalog') {
	$filters['types'] = "item_type IN (1, 2, 3, 7)";
}


if ($application=='catalog') {
	
	// default sort
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "item_code, item_name"; 
	
	// default filter
	//$filters['item_types'] = "item_type IN (1, 2, 3, 7)";
	
	// default binds
	$binds['suppliers'] = "LEFT JOIN db_retailnet.suppliers ON supplier_item = item_id";
	$binds['currencies'] = "LEFT JOIN db_retailnet.currencies ON currency_id = supplier_item_currency";
	$binds['units'] = "LEFT JOIN db_retailnet.units ON items.item_unit = units.unit_id";
	$binds['units'] = "LEFT JOIN db_retailnet.packaging_types ON items.item_packaging_type = packaging_types.packaging_type_id";
}

if ($application == 'mps' ) {
	
	// default sort
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "item_category_name, item_code, item_name";
	
	// default filter
	$filters['mps'] = "item_visible_in_mps = 1";

	// default binds
	$binds['item_categories'] = "LEFT JOIN db_retailnet.item_categories ON item_category_id = item_category";
}

$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

// filter: full text search
if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search ) {
	
	$keyword = $_REQUEST['search'];

	if ($application=='catalog') {
		
		$filters['search'] = "(
			item_code LIKE \"%$keyword%\" 
			OR item_name LIKE \"%$keyword%\" 
			OR item_description LIKE \"%$keyword%\"
			OR item_price LIKE \"%$keyword%\"
			OR supplier_item_price LIKE \"%$keyword%\"
			OR address_company LIKE \"%$keyword%\"
		)";

		$binds['addresses'] = "LEFT JOIN db_retailnet.addresses ON address_id = supplier_address";

	} 
	else {
		$filters['search'] = "(
			item_code LIKE \"%$keyword%\" 
			OR item_name LIKE \"%$keyword%\" 
			OR item_description LIKE \"%$keyword%\"
			OR item_category_name LIKE \"%$keyword%\"
		)";
	}
}

// filter active
$active = isset($_REQUEST['active']) ? $_REQUEST['active'] : 1;
$filters['active'] = "item_active = $active";

// default filter
$filters['default'] = "item_id > 0";

if ($application=='catalog') {

	// filter: supplier
	if ($_REQUEST['suppliers']) {
		$filters['suppliers'] = "supplier_address = ".$_REQUEST['suppliers'];
		$binds['suppliers'] = "LEFT JOIN db_retailnet.suppliers ON supplier_item = item_id";
	}
	else {
		$binds['suppliers'] = "LEFT JOIN db_retailnet.suppliers ON supplier_item = item_id";
	}

	// filter: item category
	if ($_REQUEST['item_categories']) {
		$filters['item_categories'] = "categories.item_category_id = ".$_REQUEST['item_categories'];
		$binds['item_categories'] = "INNER JOIN db_retailnet.item_categories AS categories ON items.item_category = categories.item_category_id";
	}

	// filter: item subcategory
	if ($_REQUEST['item_subcategories']) {
		$filters['item_subcategories'] = "item_subcategory = ".$_REQUEST['item_subcategories'];
		$binds['item_subcategories'] = "INNER JOIN db_retailnet.item_subcategories ON items.item_subcategory = item_subcategory_id";
	}

	// filter: product lines
	if ($_REQUEST['product_lines']) {
		
		$filters['product_lines'] = "product_line_supplying_group_line_id = ".$_REQUEST['product_lines'];

		$binds['item_supplying_groups'] = "INNER JOIN db_retailnet.item_supplying_groups ON item_supplying_group_item_id = item_id";
		$binds['supplying_groups'] = "INNER JOIN db_retailnet.supplying_groups ON supplying_group_id = item_supplying_group_supplying_group_id";
		$binds['product_line_supplying_groups'] = "INNER JOIN db_retailnet.product_line_supplying_groups ON product_line_supplying_group_group_id = supplying_group_id";
	}

	// filter: item types
	if ($_REQUEST['item_types']) {
		$filters['item_types'] = "item_type = ".$_REQUEST['item_types'];
		$binds['item_types'] = "INNER JOIN db_retailnet.item_types ON item_type = item_type_id";
	}

	$query = "
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			item_id,
			item_code,
			item_name,
			item_price,
			IF(item_visible, '<img src=/public/images/icon-checked.png >', ' ') as item_visible,
			IF(item_visible_in_production_order, '<img src=/public/images/icon-checked.png >', ' ') as item_visible_in_production_order,
			IF(item_visible_in_mps, '<img src=/public/images/icon-checked.png >', ' ') as item_visible_in_mps, 
			supplier_item_price,
			currency_symbol,
			currency_factor,
			currency_exchange_rate,
			IF(item_stock_property_of_swatch, '<img src=/pictures/stockproperty.gif >', '') AS item_stock_property_of_swatch,
			IF(item_is_dr_swatch_furniture, '<img src=/pictures/customerservice.png >', '') AS item_is_dr_swatch_furniture
		FROM db_retailnet.items	
	";
} else {

	// filter: item category
	if ($_REQUEST['item_categories']) {
		$filters['item_categories'] = "item_category_id = ".$_REQUEST['item_categories'];
	}

	$query = "
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			item_id,
			item_category_name,
			item_code,
			item_name,
			item_width,
			item_height,
			item_length,
			item_width*item_height*item_length/1000000 as cbm,
			item_radius,
			item_gross_weight,
			item_net_weight
		FROM db_retailnet.items
	";
}

$result = $model
->query($query)
->bind($binds)
->filter($filters)
->order($sort, $direction)
->offset($offset, $rowsPerPage)
->fetchAll();

//echo "<pre>$model->sql</pre>";

$datagrid = array();
$dropdown = array();

if ($result) {

	$totalrows = $model->totalRows(); 

	$files = $model->query("
		select DISTINCT
			item_file_item,
			item_file_id
		FROM item_files
	")->fetchAll();

	$files = _array::extract($files);

	//$pix = ui::icon('attachment');

	$editClass = $_CAN_EDIT ? ' edit' : null;

	foreach ($result as $row) {

		$item = $row['item_id'];

		// item purchase prices
		$price = number_format($row["item_price"], 2, ".", "");
		$row['item_price'] = $price;
		
		if($_REQUEST['type']<>Item::TYPE_COST_ESTIMATION)
		{
			$row['info'] = "<a class='item-files-modal' data-fancybox-type='iframe' href='/applications/templates/item.info.modal.php?id=$item' ><i class='fa fa-info-circle'></i></a>";
		}

		$datagrid[$item] = $row;
	}


	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));
	
	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=application name=application value='$application' />";
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
	

// toolbox: button print
if ($controller == 'furnitures') {
	$toolbox[] = ui::button(array(
		'id' => 'print',
		'icon' => 'print',
		'href' => $_REQUEST['print'],
		'label' => $translate->print
	));
}
elseif ($datagrid and $_REQUEST['type']<>Item::TYPE_COST_ESTIMATION) {

	$print = array();
	$print['/applications/modules/item/print.php'] = "List";
	$print['/applications/modules/item/print.available.countries.php'] = "Available in Countries";
	$print['booklet'] = "Booklet";
	
	$toolbox[] = ui::dropdown($print, array(
		'id' => 'selectPrint',
		'name' => 'selectPrint',
		'class' => 'selectbox',
		'caption' => $translate->print	
	));
}

// toolbox: add
if ($_REQUEST['add'])  {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();


// toolbox: item categories
if ($_REQUEST['type']<>Item::TYPE_COST_ESTIMATION) {
	
	$categoryBinds = $binds;
	$categoryBinds['item_categories'] = "INNER JOIN db_retailnet.item_categories AS categories ON categories.item_category_id = item_category";

	$result = $model->query("
		SELECT DISTINCT
			categories.item_category_id,
			categories.item_category_name
		FROM db_retailnet.items
	")
	->bind($categoryBinds)
	->filter($filters)
	->exclude('item_categories')
	->order('categories.item_category_name')
	->fetchAll();

	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'item_categories',
			'id' => 'item_categories',
			'class' => 'submit',
			'value' => $_REQUEST['item_categories'],
			'caption' => $translate->all_item_categories
		));
	}

	$subCategoryBinds = $binds;
	$subCategoryBinds['item_subcategories'] = "INNER JOIN db_retailnet.item_subcategories ON items.item_subcategory = item_subcategory_id";
	$subCategoryBinds['refCategories'] = "INNER JOIN db_retailnet.item_categories AS refCategories ON refCategories.item_category_id = item_subcategory_category_id";

	$result = $model->query("
		SELECT DISTINCT
			item_subcategory_id,
			item_subcategory_name,
			refCategories.item_category_id, 
			refCategories.item_category_name
		FROM db_retailnet.items
	")
	->bind($subCategoryBinds)
	->filter($filters)
	->exclude('item_subcategories')
	->order('item_subcategory_name')
	->fetchAll(); 

	if ($result) {

		$options = array();

		foreach ($result as $row) {
			$group = $row['item_category_id'];
			$subgroup = $row['item_subcategory_id'];
			$options[$group]['group'] = $row['item_category_name'];
			$options[$group]['options'][$subgroup] = $row['item_subcategory_name'];
		}
		
		$toolbox[] = ui::dropdown($options, array(
			'name' => 'item_subcategories',
			'id' => 'item_subcategories',
			'class' => 'submit',
			'value' => $_REQUEST['item_subcategories'],
			'caption' => "All Item Subcategories"
		));
	}
}


if ($application=='catalog') {

	// toolbox suppliers
	if ($_REQUEST['type']<>Item::TYPE_COST_ESTIMATION) {

		$bindSuppliers = $binds;
		$bindSuppliers['suppliers'] = "INNER JOIN db_retailnet.suppliers ON supplier_item = item_id";
		$bindSuppliers['addresses'] = "INNER JOIN db_retailnet.addresses ON address_id = supplier_address";

		$supplierFilters = $filters;

		$result = $model->query("
			SELECT DISTINCT
				address_id,
				address_company
			FROM db_retailnet.items
		")
		->bind($bindSuppliers)
		->filter($supplierFilters)
		->exclude('suppliers')
		->order('address_company')
		->fetchAll();

		if ($result) {
			$toolbox[] = ui::dropdown($result, array(
				'name' => 'suppliers',
				'id' => 'suppliers',
				'class' => 'submit',
				'value' => $_REQUEST['suppliers'],
				'caption' => $translate->all_suppliers
			));
		}
	}

	// toolbox: product lines
	if ($_REQUEST['type']<>Item::TYPE_COST_ESTIMATION) {
		
		$bindProductLines = $binds;

		$bindProductLines['item_supplying_groups'] = "INNER JOIN db_retailnet.item_supplying_groups ON item_supplying_group_item_id = item_id";
		$bindProductLines['supplying_groups'] = "INNER JOIN db_retailnet.supplying_groups ON supplying_group_id = item_supplying_group_supplying_group_id";
		$bindProductLines['product_line_supplying_groups'] = "INNER JOIN db_retailnet.product_line_supplying_groups ON product_line_supplying_group_group_id = supplying_group_id";
		$bindProductLines['product_lines'] = "INNER JOIN db_retailnet.product_lines ON product_line_id = product_line_supplying_group_line_id";

		$productLineFilters = $filters;
		$productLineFilters['active'] = "product_line_active=1";
		
		$result = $model->query("
			SELECT DISTINCT
				product_line_id,
				product_line_name
			FROM db_retailnet.items
		")
		->bind($bindProductLines)
		->filter($productLineFilters)
		->exclude('product_lines')
		->order('product_line_name')
		->fetchAll();

		if ($result) {
			$toolbox[] = ui::dropdown($result, array(
				'name' => 'product_lines',
				'id' => 'product_lines',
				'class' => 'submit',
				'value' => $_REQUEST['product_lines'],
				'caption' => $translate->all_product_lines
			));
		}
	}
}


// toolbox item active
$result = $model->query("
	SELECT COUNT(DISTINCT item_id) as total
	FROM db_retailnet.items
")
->bind($binds)
->filter($filters)
->filter('default_active', 'item_active = 1')
->exclude('active')
->fetch();

if ($result['total']) {
	$actives[1] = "Active Items";
}	

// toolbox item inactive
$result = $model->query("
	SELECT COUNT(DISTINCT item_id) as total
	FROM db_retailnet.items
")
->bind($binds)
->filter($filters)
->filter('default_active', '(item_active = 0 OR item_active = "" )')
->exclude('active')
->fetch();

if ($result['total']) {
	$actives[0] = "Inactive Items";
}

if ($actives) {
	$toolbox[] = ui::dropdown($actives, array(
		'name' => 'active',
		'id' => 'active',
		'class' => 'submit',
		'value' => $active,
		'caption' => false
	));
}

if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>";
}


// item suppliers
$itemSuppliers = array();

if ($application=='catalog') { 

	// toolbox suppliers
	$bindSuppliers = $binds;
	$bindSuppliers['suppliers'] = "INNER JOIN db_retailnet.suppliers ON supplier_item = item_id";
	$bindSuppliers['addresses'] = "INNER JOIN db_retailnet.addresses ON address_id = supplier_address";

	$result = $model->query("
		SELECT DISTINCT address_id,
			address_company, item_id
		FROM db_retailnet.items 
	")
	->bind($bindSuppliers)
	->filter($filters)
	->order('address_company')
	->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$item = $row['item_id'];
			$itemSuppliers[$item] = $itemSuppliers[$item] ? $itemSuppliers[$item].", ".$row['address_company'] : $row['address_company'];
		}
	}
}


$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

if ($datagrid) {
	foreach ($datagrid as $item => $row) {
		if ($itemSuppliers[$item]) {
			$datagrid[$item]['item_suppleir'] = $itemSuppliers[$item];
		}
	}
}

$table->datagrid = $datagrid;

if ($application == 'mps') {
	$table->item_category_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=15%'
	);
}

$table->item_code(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	"href=".$_REQUEST['data'],
	'width=10%'
);

if ($application=='catalog' and $_REQUEST['type']<>Item::TYPE_COST_ESTIMATION) {
	$table->info(
		Table::ATTRIBUTE_NOWRAP,
		"width=2%",
		"class=text-center"
	);
}

$table->item_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	Table::DATA_TYPE_LINK
);	

if ($application=='catalog') {
	
	if ($_REQUEST['type'] <> Item::TYPE_COST_ESTIMATION) {
		
		$table->item_suppleir(
			Table::ATTRIBUTE_NOWRAP,
			"width=10%"
		);

		$table->unit_name(
			Table::ATTRIBUTE_NOWRAP,
			"width=5%"
		);

		$table->supplier_item_price(
			Table::ATTRIBUTE_NOWRAP,
			"width=5%",
			"class=number"
		);

		$table->currency_symbol(
			Table::ATTRIBUTE_NOWRAP,
			"width=30",
			"caption="
		);
			
		/*
		$table->item_price(
			Table::ATTRIBUTE_NOWRAP,
			//Table::DATA_TYPE_TEXTBOX,
			"width=5%",
			"class=number"
		);	
		*/

		$table->caption('item_price', 'Sales Price CHF');
	}
	

	if ($_CAN_EDIT && $_REQUEST['type']<>Item::TYPE_COST_ESTIMATION) {
		$table->supplier_item_price(Table::DATA_TYPE_TEXTBOX);	
		$table->attributes('supplier_item_price', array('class' => 'edit-price', 'data-section' => 'update.supplier.price'));
	}

	
	if($_REQUEST['type']<>Item::TYPE_COST_ESTIMATION)
	{
		$table->item_visible(
			Table::ATTRIBUTE_NOWRAP,
			"width=2%",
			"class=text-center"
		);
	}

	$table->caption('item_visible', 'Visible');

	if ($_REQUEST['type'] <> Item::TYPE_COST_ESTIMATION) {
		
		$table->item_visible_in_production_order(
			Table::ATTRIBUTE_NOWRAP,
			"width=2%",
			"class=text-center"
		);

		$table->caption('item_visible_in_production_order', 'Planning');


		$table->item_visible_in_mps(
			Table::ATTRIBUTE_NOWRAP,
			"width=2%",
			"class=text-center"
		);

		$table->caption('item_visible_in_mps', 'Merchandising');

		$table->item_stock_property_of_swatch(
			Table::ATTRIBUTE_NOWRAP,
			"width=2%",
			"class=text-center"
		);

		$table->caption('item_stock_property_of_swatch', $translate->prop_swatch);

		$table->item_is_dr_swatch_furniture(
			Table::ATTRIBUTE_NOWRAP,
			"width=2%",
			"class=text-center"
		);

		$table->caption('item_is_dr_swatch_furniture', $translate->dr_swatch);
	}
}

if ($application == 'mps') {
	$table->item_width(Table::ATTRIBUTE_NOWRAP, 'width=5%');
	$table->item_height(Table::ATTRIBUTE_NOWRAP, 'width=5%');
	$table->item_length(Table::ATTRIBUTE_NOWRAP, 'width=5%');
	$table->cbm(Table::ATTRIBUTE_NOWRAP, 'width=5%');
	$table->item_radius(Table::ATTRIBUTE_NOWRAP, 'width=5%');
	$table->item_gross_weight(Table::ATTRIBUTE_NOWRAP, 'width=5%');
	$table->item_net_weight(Table::ATTRIBUTE_NOWRAP, 'width=5%');
}

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;
