<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['mps_turnoverclass_id'];
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	$data = array();
	
	if ($_REQUEST['mps_turnoverclass_group_id']) {
		$data['mps_turnoverclass_group_id'] = $_REQUEST['mps_turnoverclass_group_id'];
	}
	
	if (isset($_REQUEST['mps_turnoverclass_code'])) {
		$data['mps_turnoverclass_code'] = $_REQUEST['mps_turnoverclass_code'];
	}
	
	if (isset($_REQUEST['mps_turnoverclass_name'])) {
		$data['mps_turnoverclass_name'] = $_REQUEST['mps_turnoverclass_name'];
	}
	
	if (in_array('mps_turnoverclass_active', $fields)) {
		$data['mps_turnoverclass_active'] = ($_REQUEST['mps_turnoverclass_active']) ? 1 : 0;
	}
	
	if ($data) {
		
		$turnover_class = new TurnoverClass($application);
		$turnover_class->read($id);
		
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $turnover_class->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $turnover_class->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));