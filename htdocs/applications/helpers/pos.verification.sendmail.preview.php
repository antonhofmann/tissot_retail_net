<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$template = $_REQUEST['mail_template_id'];
$period = $_REQUEST['periodes'];
$region = $_REQUEST['regions'];
$addresstype = $_REQUEST['addresstypes'];
$client = $_REQUEST['clients'];
$selected = $_REQUEST['selected'];

if ($template) {

	settype($template, "integer");
		
	// action mail
	$actionmail = new ActionMail($template);
	
	// assign filters 
	$actionmail->param()->application = $application;
	$actionmail->param()->period = $period;
	$actionmail->param()->region = $region;
	$actionmail->param()->addresstype = $addresstype;
	$actionmail->param()->client = $client;
	$actionmail->param()->selected = $selected;
	
	// set mail template data
	if ($_REQUEST['mail_template_view_modal']) {
		$actionmail->content()->subject = $_REQUEST['mail_template_subject'];
		$actionmail->content()->body = $_REQUEST['mail_template_text'];
	}
		
	// preview mail
	$json = $actionmail->preview();	

	// console messages
	$json['console'] = $actionmail->response()->console;
	
} else {
	$json['response'] = false;
	$json['notification'] = "E-mail template not found.";
}
	
	
if (!headers_sent()) {
	header('Content-Type: text/json');
}

echo json_encode($json);
