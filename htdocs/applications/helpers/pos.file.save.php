<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	// url vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$pos = $_REQUEST['posfile_posaddress'];
	$id = $_REQUEST['posfile_id'];
	$dirpath = "/public/data/files/$application/posindex/$pos";
	
	if ($_REQUEST['posfile_filegroup']) {
		$data['posfile_filegroup'] = $_REQUEST['posfile_filegroup'];
	}
	
	if (isset($_REQUEST['posfile_title'])) {
		$data['posfile_title'] = $_REQUEST['posfile_title'];
	}

	if (isset($_REQUEST['posfile_description'])) {
		$data['posfile_description'] = $_REQUEST['posfile_description'];
	}
	
	if ($_REQUEST['has_upload'] && $_REQUEST['posfile_path']) {
		
		$path = upload::move($_REQUEST['posfile_path'], $dirpath);
		$extension = file::extension($path);
		
		$data['posfile_path'] = $path;
		$data['posfile_filetype'] = File_Type::get_id_from_extension($extension);
	}
	
	
	if($pos && $data) { 
		
		$data['posfile_posaddress'] = $pos;
		$data['posfile_owner'] = $user->id;
		
		$posFile = new Pos_File(Connector::DB_CORE);
		$posFile->read($id);
	
		if ($id) {
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			$response = $posFile->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $pos = $posFile->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect']."/$pos";
		}	
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}

	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'path' => $path
	));
