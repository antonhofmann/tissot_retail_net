<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$model = new Model(Connector::DB_CORE);
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];

	$filter = null;

	if (!user::permission("has_access_to_all_attachments_in_posindex")) {
		$filter = " AND (order_file_address_address = $user->address OR order_file_owner = $user->id )";
	}
	
	$result = $model->query("
		SELECT DISTINCT
			order_file_id, 
			posorder_ordernumber,
			order_file_title, 
			order_file_description, 
			order_file_path, 
			LEFT(order_file_description,120) AS order_file_description,
			DATE_FORMAT(order_files.date_created, '%d.%m.%Y') AS date_created,
			order_file_category_id,
			order_file_category_name
		FROM order_files
		INNER JOIN db_retailnet.posorders ON posorder_order = order_file_order
		INNER JOIN db_retailnet.order_file_categories ON order_file_category = order_file_category_id
		WHERE posorder_order > 0 
		AND posorder_type = 1
		AND posorder_posaddress = $id
		$filter
		ORDER BY posorder_order, order_file_title
	")->fetchAll();
	
	if ($result) { 
		foreach ($result as $row) {
	
			$file = $row['order_file_id'];
			$order = $row['posorder_ordernumber'];
			$group = $row['order_file_category_id'];
			$extension = file::extension($row['order_file_path']);

			$datagrid[$order]['caption'] = "Files of Project ".$row['posorder_ordernumber'];
			$datagrid[$order]['groups'][$group]['caption'] = $row['order_file_category_name'];
			$datagrid[$order]['groups'][$group]['files'][$file]['title'] = "<span class='order_title' >{$row[order_file_title]}</span><span>{$row[order_file_description]}</span>";
			$datagrid[$order]['groups'][$group]['files'][$file]['date']= "<span>$translate->date_created <br />{$row[date_created]}</span>";
			$datagrid[$order]['groups'][$group]['files'][$file]['path'] = $row['order_file_path'];
			$datagrid[$order]['groups'][$group]['files'][$file]['fileicon']  = "<span class='file-extension $extension modal' tag='{$row[order_file_path]}'></span>";
		}
	}
	
	if ($datagrid) {
	
		foreach ($datagrid as $order => $groups) {

			$filelist .= "<h4>{$groups[caption]}</h4>";

			foreach ($groups['groups'] as $group => $row) {
				$table = new Table(array('thead' => false));
				$table->datagrid = $row['files'];
				$table->fileicon();
				$table->title();
				$table->date();
				$filelist .= "<h6>{$row[caption]}</h6>";
				$filelist .= $table->render();
			}
		}
	}
	
	echo $filelist;
	
	
	