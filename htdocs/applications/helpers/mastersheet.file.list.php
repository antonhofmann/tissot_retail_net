<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	// db connector
	$model = new Model($application);
	
	// mastersheet
	$mastersheet = new Mastersheet($application);
	$mastersheet->read($id);
	
	// datagrid
	$result = $model->query("
		SELECT DISTINCT
			mps_mastersheet_file_id,
			mps_mastersheet_file_path,
			mps_mastersheet_file_title,
			DATE_FORMAT(mps_mastersheet_files.date_created, '%d.%m.%Y') AS date_created,
			file_type_id,
			file_type_name
		FROM mps_mastersheet_files
	")
	->bind(Mastersheet_File::DB_BIND_FILE_TYPES)
	->filter('mastersheet', "mps_mastersheet_file_mastersheet_id = $id")
	->order('file_type_name')
	->order('mps_mastersheet_file_title')
	->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$type = $row['file_type_id'];
			$typeName = $row['file_type_name'];
			$file = $row['mps_mastersheet_file_id'];
			$filePath = $row['mps_mastersheet_file_path'];
			$fileTitle = $row['mps_mastersheet_file_title'];
			$fileDate = $translate->date_created.': '.$row['date_created'];
			$file_extension = file::extension($filePath);
			
			$datagrid[$type]['caption'] = $typeName;
			$datagrid[$type]['files'][$file]['title'] = "<span class=title >$fileTitle</span><span class=description >$fileDate</span>";
			$datagrid[$type]['files'][$file]['fileicon']  = "<span class='file-extension $file_extension modal' data='$filePath'></span>";
		}
		
		foreach ($datagrid as $filetype => $files) {
			
			$table = new Table(array('thead' => false));
			$table->datagrid = $files['files'];
			$table->fileicon();
			$table->title("href=".$_REQUEST['form']);
			//$table->date();
			
			$list .= "<h6>".$files['caption']."</h6>";
			$list .= $table->render();
		}
	}
	else {
		$list = html::emptybox($translate->empty_result);
	}
	
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}
	
	if ($toolbox) {
		$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
	}

	echo $toolbox.$list;
	
	