<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['language_id'];
	
	$data = array();
	
	if ($_REQUEST['language_iso639_1']) {
		$data['language_iso639_1'] = $_REQUEST['language_iso639_1'];
	}
	
	if ($_REQUEST['language_name']) {
		$data['language_name'] = $_REQUEST['language_name'];
	}
	
	if (isset($_REQUEST['language_local_name'])) {
		$data['language_local_name'] = $_REQUEST['language_local_name'];
	}
	
	if ($data) {
	
		$language = new Language();
		$language->read($id);
	
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $language->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $language->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));
	