<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$field = $_REQUEST['fieldId'];
	$password = $_REQUEST['fieldValue'];
	
	// open user session
	if (!Session::get('user_id')) {
		Session::init();
		Session::set('user_id', $_REQUEST['id']);
	}
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// render vars
	$messageParams= array(
		'field' => $translate->old_password
	);
	
	if ($user->password <> $password) {
		$error = true;
		$errorMessage = $translate->message_password_old_error($messageParams);
	}

	// resonse vars
	$success = ($error) ? false : true;
	$message = ($success) ? $translate->message_password_old_success($messageParams) : $errorMessage;
	
	// response
	$response[0] = $field;
	$response[1] = $success;
	$response[2] = $message;
	echo json_encode($response);
	