<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// disabled fields
$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

// url request
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];

// product line
$id = $_REQUEST['item_category_id'];

$data = array();	

if (in_array('item_category_name', $fields)) {
	$data['item_category_name'] = $_REQUEST['item_category_name'];
}	

if (in_array('item_category_include_in_masterplan', $fields)) {
	$data['item_category_include_in_masterplan'] = $_REQUEST['item_category_include_in_masterplan'];
}	

if (in_array('item_category_active', $fields)) {
	$data['item_category_active'] = $_REQUEST['item_category_active'];
}	

if ($data) {

	$itemCategory = new Item_Category();
	$itemCategory->read($id);

	if ($itemCategory->id) {
		
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		
		$response = $itemCategory->update($data);
		
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		
		$response = $id = $itemCategory->create($data);
		
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		
		$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
	}
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect
));
