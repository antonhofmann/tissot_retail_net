<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// permissions
	$permission_view = User::permission(Pos::PERMISSION_VIEW);
	$permission_view_limited = User::permission(Pos::PERMISSION_VIEW_LIMITED);
	$permission_edit = User::permission(Pos::PERMISSION_EDIT);
	$permission_edit_limited = User::permission(Pos::PERMISSION_EDIT_LIMITED);
	$permission_travelling = User::permission('has_access_to_all_travalling_retail_data');

	// has limited permission
	$hasLimitedPermission = (!$permission_view && !$permission_edit) ? true : false;

	// url vars
	$application = trim($_REQUEST['application']);
	$controller = trim($_REQUEST['controller']);
	$action = trim($_REQUEST['action']);
	$archived = trim($_REQUEST['archived']);
	$material = $_REQUEST['material'];
	
	// db connector
	$model = new Model($application);
	
	// filter: material
	$filters['material'] = "mps_pos_material_material_id = $material";
	
	// filter: only opened shops
	$filters['opened'] = "
		posaddress_store_openingdate IS NOT NULL 
		AND posaddress_store_openingdate <> '0000-00-00'
		AND (
			posaddress_store_closingdate = '0000-00-00'
			OR posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = ''
		)
	";


	// extended country access
	$accessCountries = array();
		
	$result = $model->query("
		SELECT DISTINCT country_access_country
		FROM db_retailnet.country_access
		WHERE country_access_user = $user->id
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			$accessCountries[] = $row['country_access_country'];
		}
		
		$accessCountriesFilter = join(',', $accessCountries);
	}

	$binds = array(
		Pos_Material::DB_BIND_POSADDRESSES,
		Pos::DB_BIND_COUNTRIES,
		Pos::DB_BIND_PLACES,
		Pos::DB_BIND_POSTYPES
	);

	// for limited view
	if ($hasLimitedPermission) {

		$countryAccess = User::getCountryAccess();

		// filter access countries
		$filterCountryAccess = $countryAccess ? " OR posaddress_country IN ($countryAccess)" : null;

		// regional access companies
		$regionalAccessCompanies = User::getRegionalAccessPos();
		$filterRegionalAccess = $regionalAccessCompanies ? " OR $regionalAccessCompanies " : null;

		// travelling
		if ($permission_travelling) { 
			$binds[] = "LEFT JOIN db_retailnet.posareas ON posarea_posaddress = posaddress_id";
			$travelling = " OR posarea_area IN (4,5) OR posaddress_store_subclass IN(17) "; 
		}

		$filters['limited'] = "(
			posaddress_client_id = $user->address
			$travelling
			$filterCountryAccess 
			$filterRegionalAccess
		)";
	}
	
	// datagrid
	$result = $model->query("
		SELECT DISTINCT
			posaddress_id,
			posaddress_name,
			posaddress_address,
			posaddress_zip,
			country_name,
			place_name,
			postype_name,
			mps_pos_material_quantity_in_use
		FROM mps_pos_materials
	")
	->bind($binds)
	->filter($filters)
	//->extend($extendedFilter)
	->order('country_name')
	->order('posaddress_name')
	->order('place_name')
	->fetchAll();
	
	if ($result) {
	
		$datagrid = _array::datagrid($result);
	
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => count($result),
			'buttons' => true,
			'translate' => $translate
		));
	
		$list_index = $pager->index();
	}
	
	$table = new Table();
	
	$table->datagrid = $datagrid;
	
	$table->caption('mps_pos_material_quantity_in_use', $translate->quantity, array(
		'class' => '-numbers'		
	));
	
	$table->country_name(
		Table::ATTRIBUTE_NOWRAP,
		'width=10%'
	);
	
	$table->posaddress_name("href=/$application/pos/materials");
	$table->attributes('posaddress_name', array('target'=>'_blank'));
	
	$table->place_name(
		Table::ATTRIBUTE_NOWRAP,
		'width=10%'
	);
	
	$table->postype_name(
		Table::ATTRIBUTE_NOWRAP,
		'width=10%'
	);
	
	$table->mps_pos_material_quantity_in_use(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_RIGHT,
		'width=10%'
	);
	
	echo $table->render();
	
	
	