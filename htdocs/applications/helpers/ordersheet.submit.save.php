<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';


if ($_REQUEST['ordersheets']) {
	
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$ordersheets = explode(',', $_REQUEST['ordersheets']);
	
	$ordersheet = new Ordersheet($application);
		
	$workflowStateOpen = Workflow_State::STATE_OPEN;
		
	// set ordersheets in workflow state "open"
	foreach ($ordersheets as $id) {
			
		$ordersheet->read($id);
	
		$ordersheet->update(array(
			'mps_ordersheet_workflowstate_id' => $workflowStateOpen,
			'user_modified' => $user->login,
			'date_modified' => date('Y-m-d H:i:s')
		));
	}
	
	$json['response'] = true;
	
	// reload page
	if (!$_REQUEST['sendmail']) {
		$json['reload'] = true;
	}
	
	// onload message
	Message::request_submitted();
} 
else {

	$json = array(
		'response' => false,
		'notification' => array(
			'content' => 'Bad request. Check form data.',
			'properties' => array(
				'theme' => 'error'
			)
		)
	);
}

if (!headers_sent()) {
	header('Content-Type: text/json');
}

echo json_encode($json);
