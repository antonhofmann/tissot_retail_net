<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$filter = $_REQUEST['filter'];

	$model = new Model($application);
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : '
		mps_mastersheet_year desc,
		mps_mastersheet_name
	';

	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
		
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			mps_mastersheet_year LIKE \"%$keyword%\" 
			OR mps_mastersheet_name LIKE \"%$keyword%\"
		)";
	}
	
	// filer: collection year
	if ($_REQUEST['mastersheet_years']) {
		$filters['mastersheet_years'] = "mps_mastersheet_year = ".$_REQUEST['mastersheet_years'];
	}
	
	// filter: archived state
	$mastersheet_archived = ($archived) ? 1 : 0;
	
	// filter: consolidation
	if ($controller=='consolidations') {
		$filters['default'] = "
			mps_ordersheet_workflowstate_id IN (3,4,5)
			OR (
				mps_ordersheet_workflowstate_id = 2
				AND mps_ordersheet_closingdate < CURRENT_DATE
			)
		";
	} 
	elseif ($controller=='salesorders') {
		$filters['default'] = "mps_ordersheet_workflowstate_id = 5";
	}
	// filter: archived
	elseif ($archived) {
		$filters['default'] = "mps_mastersheet_archived = 1";
	}
	// filter: default
	else {
		$filters['default'] = "mps_mastersheet_archived = 0";
	}
	
	// datagrid
	if ($controller=='consolidations' || $controller=='salesorders' || $archived) {
		
		$result = $model->query("
			SELECT SQL_CALC_FOUND_ROWS DISTINCT
				mps_mastersheet_id, 
				mps_mastersheet_year, 
				mps_mastersheet_name,
				mps_mastersheet_consolidated,
				mps_mastersheet_archived
			FROM mps_ordersheets
		")
		->bind(Ordersheet::DB_BIND_MASTERSHEETS)
		->filter($filters)
		->order($sort, $direction)
		->offset($offset, $rowsPerPage)
		->fetchAll();
	}
	else {
		
		$result = $model->query("
			SELECT SQL_CALC_FOUND_ROWS DISTINCT
				mps_mastersheet_id, 
				mps_mastersheet_year, 
				mps_mastersheet_name,
				mps_mastersheet_consolidated,
				mps_mastersheet_archived
			FROM mps_mastersheets
		")
		->filter($filters)
		->order($sort, $direction)
		->offset($offset, $rows)
		->fetchAll();
	}
	
	if ($result) {
	
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		
		foreach ($datagrid as $key => $row) {
			
			if ($row['mps_mastersheet_archived']) {
				$datagrid[$key]['state'] = $translate->archived;
			}
			elseif ($row['mps_mastersheet_consolidated']) {
				$datagrid[$key]['state'] = $translate->consolidated;
			}
			
			if ($controller=='consolidations' || $controller=='salesorders' || $archived) {
				
				// total ordersheets
				$result = $model->query("
					SELECT COUNT(mps_ordersheet_id) AS total
					FROM mps_ordersheets
					WHERE mps_ordersheet_mastersheet_id = $key		
				")->fetch();
				
				$datagrid[$key]['ordersheets'] = $result['total'];
				
				// total ordersheets
				$result = $model->query("
					SELECT SUM(mps_ordersheet_item_price * mps_ordersheet_item_quantity_approved) AS total
					FROM mps_ordersheets
					INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_ordersheet_id = mps_ordersheet_id
					WHERE mps_ordersheet_mastersheet_id = $key
				")->fetch();
				
				$datagrid[$key]['total_cost'] = $result['total'];
			}
		}
	
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
	
	// toolbox: utton print
	if ($datagrid && $_REQUEST['print']) {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'href' => $_REQUEST['print'],
			'label' => $translate->print
		));
	}

	
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	// data: master sheet years
	$result = $model->query("
		SELECT DISTINCT
			mps_mastersheet_year, 
			mps_mastersheet_year AS year
		FROM mps_mastersheets
	")
	->filter($filters)
	->exclude('mastersheet_years')
	->order('mps_mastersheet_year')
	->fetchAll();
	
	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'mastersheet_years',
			'id' => 'mastersheet_years',
			'class' => 'submit',
			'value' => $_REQUEST['mastersheet_years'],
			'caption' => $translate->all_years
		));
	}

	
	// toolbox: form
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	
	$table->caption('mps_mastersheet_year', $translate->year);
	
	$table->mps_mastersheet_year(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=8%'
	);
	
	$table->mps_mastersheet_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		"href=".$_REQUEST['form']
	);

	// state column
	if ($datagrid && _array::key_exists('state', $datagrid)) {
		$table->state(
			Table::ATTRIBUTE_NOWRAP,
			'width=10%'
		);
	}
	
	// ordersheets column
	if ($datagrid && _array::key_exists('ordersheets', $datagrid)) {
		$table->attributes('ordersheets', array('class'=>'number'));
		$table->ordersheets(
			Table::ATTRIBUTE_NOWRAP,
			Table::FORMAT_NUMBER,
			'width=10%'
		);
	}
	
	// state column
	if ($datagrid && _array::key_exists('total_cost', $datagrid)) {
		$table->attributes('total_cost', array('class'=>'number'));
		$table->total_cost(
			Table::ATTRIBUTE_NOWRAP,
			Table::FORMAT_NUMBER,
			'width=10%'
		);
	}
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	
	