<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$commentCategory = new Red_Comment_Category($application);
	$commentCategory->read($id);

	if ($commentCategory->id && user::permission(RED::PERMISSION_DELETE_SYSTEM_DATA)) {
		
		$integrity = new Integrity();
		$integrity->set($id, 'red_commentcategories', $application);

		if ($integrity->check()) {
		
			$response = $commentCategory->delete();

			if ($response) {
				Message::request_deleted();
				url::redirect("/$application/$controller");
			} else {
				Message::request_failure();
				url::redirect("/$application/$controller/$action/$id");
			}
		}
		else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$id");
		}

	} else {
		Message::request_failure();
		url::redirect("/$application/$controller");
	}