<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$value = $_REQUEST['value'];
	
	switch ($_REQUEST['section']) {

		case 'company':

			$json[][''] = "Select";

			if ($_REQUEST['value']) {

				$model = new Model(Connector::DB_CORE);
				$result= $model->query("
					SELECT DISTINCT
						user_id,
						CONCAT(user_name, ', ', user_firstname) as name
					FROM users
				")
				->filter('user', "user_address = $value")
				->filter('active', "user_active = 1")
				->order('user_name, user_firstname')
				->fetchAll();

				if ($result) {
					foreach ($result as $row) {
						$json[][$row['user_id']] = $row['name'];
					}
				}
			}

			break;

	}

	header('Content-Type: text/json');
	echo json_encode($json);
