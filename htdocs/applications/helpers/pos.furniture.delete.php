<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$pos = $_REQUEST['pos'];
	$id = $_REQUEST['id'];
	
	if ($id && ($user->permission(Pos::PERMISSION_EDIT) OR $user->permission(Pos::PERMISSION_EDIT_LIMITED) )) {
		
		$pos_furniture = new Pos_Furniture($application);
		$pos_furniture->read($id);
		$delete = $pos_furniture->delete();
		
		if ($delete) {
			Message::request_deleted();
			url::redirect("/$application/$controller/$action/$pos");
		} else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$pos/$id");
		}
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$pos");
	}