<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['idea_id'];
	
	$data = array();
	
	if ($_REQUEST['idea_idea_category_id']) {
		$data['idea_idea_category_id'] = $_REQUEST['idea_idea_category_id'];
	}
	
	if ($_REQUEST['idea_title']) {
		$data['idea_title'] = $_REQUEST['idea_title'];
	}
	
	if ($_REQUEST['idea_text']) {
		$data['idea_text'] = $_REQUEST['idea_text'];
	}
	
	if (in_array('idea_priority', $fields)) {
		$data['idea_priority'] = $_REQUEST['idea_priority'];
	}
	
	if (in_array('idea_killed', $fields)) {
		$data['idea_killed'] = $_REQUEST['idea_killed'] ? 1 : 0;
	}
	
	if ($data) {
	
		$idea = new Idea();
		$idea->read($id);
	
		if ($idea->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $idea->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			
			$response = $id = $idea->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect'].'/'.$id : null;
		}
	}
	else {
		
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	