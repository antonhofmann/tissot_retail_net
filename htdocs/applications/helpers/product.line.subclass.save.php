<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	// product line
	$id = $_REQUEST['productline_subclass_id'];
	
	$data = array();
	
	if (in_array('productline_subclass_productline', $fields)) {
		$data['productline_subclass_productline'] = $_REQUEST['productline_subclass_productline'];
	}
	
	if (in_array('productline_subclass_name', $fields)) {
		$data['productline_subclass_name'] = $_REQUEST['productline_subclass_name'];
	}
	
	if (in_array('productline_subclass_active', $fields)) {
		$data['productline_subclass_active'] = $_REQUEST['productline_subclass_active'] ? 1 : 0;
	}
	
	if ($data) {
	
		$productLineSubclass = new Product_Line_Subclass();
		$productLineSubclass->read($id);
	
		if ($productLineSubclass->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $productLineSubclass->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $productLineSubclass->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}

	if ($response && in_array('productline_subclass_productline', $fields)) {
		
		$db = Connector::get();

		$db->exec("
			DELETE FROM productline_subclass_productlines
			WHERE productline_subclass_productline_class_id = $id
		");

		if ($_REQUEST['productline_subclass_productline']) {

			$sth = $db->prepare("
				INSERT INTO productline_subclass_productlines (
					productline_subclass_productline_class_id,
					productline_subclass_productline_line_id,
					user_created
				)
				VALUES (?, ?, ?)
			");

			foreach ($_REQUEST['productline_subclass_productline'] as $line => $value) {
				$sth->execute(array($id, $line, $user->login));
			}
		}

	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	