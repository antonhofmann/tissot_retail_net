<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	$title = $_REQUEST['title'];
	
	if ($id && $title) {
		
		$model = new Model($application);
		
		// ordersheet
		$ordersheet = new Ordersheet($application);
		$ordersheet->read($id);
		
		// ordersheet version 
		$version = $ordersheet->version()->create(array(
			'mps_ordersheet_version_ordersheet_id' => $id,
			'mps_ordersheet_version_workflowstate_id' => $ordersheet->workflowstate_id,
			'mps_ordersheet_version_address_id' => $ordersheet->address_id,
			'mps_ordersheet_version_mastersheet_id' => $ordersheet->mastersheet_id,
			'mps_ordersheet_version_title' => $title,
			'mps_ordersheet_version_openingdate' => date::sql($ordersheet->openingdate),
			'mps_ordersheet_version_closingdate' => date::sql($ordersheet->closingdate),
			'mps_ordersheet_version_comment' => $ordersheet->comment,
			'user_created' => $user->login,
			'date_created' => date('Y-m-d H:i:s')
		));
		
		// get ordersheet items
		$items = $ordersheet->item()->load();
		
		if ($version && $items) {
			
			$ordersheet->version()->read($version);
			
			foreach ($items as $row) {
				
				$ordersheet->version()->item()->create(array(
					'mps_ordersheet_version_item_ordersheetversion_id' => $version,
					'mps_ordersheet_version_item_material_id' => $row['mps_ordersheet_item_material_id'],
					'mps_ordersheet_version_item_price' => $row['mps_ordersheet_item_price'],
					'mps_ordersheet_version_item_currency' => $row['mps_ordersheet_item_currency'],
					'mps_ordersheet_version_item_exchangrate' => $row['mps_ordersheet_item_exchangrate'],
					'mps_ordersheet_version_item_factor' => $row['mps_ordersheet_item_factor'],
					'mps_ordersheet_version_item_quantity' => $row['mps_ordersheet_item_quantity'],
					'mps_ordersheet_version_item_quantity_proposed' => $row['mps_ordersheet_item_quantity_proposed'],
					'mps_ordersheet_version_item_quantity_approved' => $row['mps_ordersheet_item_quantity_approved'],
					'mps_ordersheet_version_item_quantity_confirmed' => $row['mps_ordersheet_item_quantity_confirmed'],
					'mps_ordersheet_version_item_quantity_shipped' => $row['mps_ordersheet_item_quantity_shipped'],
					'mps_ordersheet_version_item_quantity_distributed' => $row['mps_ordersheet_item_quantity_distributed'],
					'mps_ordersheet_version_item_status' => $row['mps_ordersheet_item_status'],
					'mps_ordersheet_version_item_shipto' => $row['mps_ordersheet_item_shipto'],
					'mps_ordersheet_version_item_desired_delivery_date' => $row['mps_ordersheet_item_desired_delivery_date'],
					'mps_ordersheet_version_item_order_date' => $row['mps_ordersheet_item_order_date'],
					'mps_ordersheet_version_item_purchase_order_number' => $row['mps_ordersheet_item_purchase_order_number'],
					'user_created' => $user->login,
					'date_created' => date('Y-m-d H:i:s')
				));
			}
			
			Message::ordersheet_version_created();
		}
	}
	
	url::redirect("/$application/$controller/$action/$id");
	