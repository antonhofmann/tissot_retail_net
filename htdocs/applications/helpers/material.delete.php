<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$material = new Material($appliction);
	$material->read($id);
	
	if ($material->islongterm) {
		$canedit = (user::permission(Material::PERMISSION_LTM_EDIT)) ? true : false;
	} else {
		$canedit = (user::permission(Material::PERMISSION_STM_EDIT)) ? true : false;
	}
	
	if ($canedit) {

		$delete = $material->delete();
		
		if ($delete) {
			Message::request_deleted();
			url::redirect("/$appliction/$controller");
		} else {
			Message::request_failure();
			url::redirect("/$appliction/$controller/$action/$id");
		}
	}
	else {
		Message::request_failure();
		url::redirect("/$appliction/$controller");
	}