<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['scpps_stocklist_id'];
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$data = array();
	
	
	if ($_REQUEST['scpps_stocklist_title']) {
		$data['scpps_stocklist_title'] = $_REQUEST['scpps_stocklist_title'];
	}
	
	if ($_REQUEST['scpps_stocklist_supplier_address']) {
		$data['scpps_stocklist_supplier_address'] = $_REQUEST['scpps_stocklist_supplier_address'];
	}
	
	if (in_array('weeks', $fields)) {
		$data['scpps_stocklist_weeks'] = ($_REQUEST['weeks']) ? serialize(array_keys($_REQUEST['weeks'])) : null;
	}
	
	if ($data) { 
		
		$stocklist = new Stocklist();
		$stocklist->read($id);
		
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $stocklist->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else { 
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $stocklist->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			if ($_REQUEST['redirect']) {
				$redirect = $_REQUEST['redirect']."/$id";
			}
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));