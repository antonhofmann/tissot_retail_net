<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	// url vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$id = $_REQUEST['certificate_file_id'];
	$certificate = $_REQUEST['certificate_file_certificate_id'];
	
	$upload_path = "/public/data/files/$application/certificates/$certificate";
	
	if ($_REQUEST['certificate_file_version']) {
		$data['certificate_file_version'] = $_REQUEST['certificate_file_version'];
	}	

	if ($_REQUEST['certificate_file_expiry_date']) {
		$data['certificate_file_expiry_date'] = date::sql($_REQUEST['certificate_file_expiry_date']);
	}
	
	if ($_REQUEST['has_upload'] && $_REQUEST['certificate_file_file']) {
		$path = upload::move($_REQUEST['certificate_file_file'], $upload_path);
		$data['certificate_file_file'] = $path;
	}
	
	
	if($certificate && $data) { 

		$data['certificate_file_certificate_id'] = $certificate;
		
		$certificateFile = new Certificate_File();
		$certificateFile->read($id);
	
		if ($certificateFile->id) {
			
			$data['user_modified'] = $user->login;
			$response = $certificateFile->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $certificateFile->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect'] ? $_REQUEST['redirect']."/$id" : null;
		}	
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}

	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'path' => $path
	));
