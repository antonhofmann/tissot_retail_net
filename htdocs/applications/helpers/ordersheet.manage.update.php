<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// request vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	// mastersheet vars
	$mastersheet = $_REQUEST['mps_mastersheet_id'];
	$ordersheet_description = $_REQUEST['mps_ordersheet_comment'];
	$ordersheet_openingdate = $_REQUEST['mps_ordersheet_openingdate'];
	$ordersheet_closingdate = $_REQUEST['mps_ordersheet_closingdate'];
	$ordersheet_workflowstate = $_REQUEST['mps_ordersheet_workflowstate_id'];
	$selected_companies = $_REQUEST['selected_companies'];

	if ($selected_companies) {
		
		$data = array();
		
		// order sheet opening date
		if ($ordersheet_openingdate) {
			$data['mps_ordersheet_openingdate'] = date::sql($ordersheet_openingdate);
		}
		
		// order sheet closing date
		if ($ordersheet_closingdate) {
			$data['mps_ordersheet_closingdate'] = date::sql($ordersheet_closingdate);
		}
		
		// order sheet description
		if ($ordersheet_description) {
			$data['mps_ordersheet_comment'] = $ordersheet_description;
		}
		
		// order sheet workflow state
		if ($ordersheet_workflowstate) {
			$data['mps_ordersheet_workflowstate_id'] = $ordersheet_workflowstate;
		}
		
		if ($data) {
			
			$ordersheets = explode(',', $selected_companies);
			
			// ordersheet builder
			$ordersheet = new Ordersheet($application);
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
		
			foreach ($ordersheets as $id) {
				$ordersheet->id = $id;
				$ordersheet->update($data);
			}
		}
		
		$response = true;
		$message = $translate->message_request_inserted;
		$tab = 'existing_ordersheets';
	} 
	else {
		$response = false;
		$message =$translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'tab' => $tab,
		'reset' => true,
		'selected_companies' => $selected_companies
	));
	