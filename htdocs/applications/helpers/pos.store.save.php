<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['posaddress_id'];
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$data = array();
	
	if ($_REQUEST['posaddress_store_postype']) {
		$data['posaddress_store_postype'] = $_REQUEST['posaddress_store_postype'];
	}

	if (isset($_REQUEST['posaddress_store_subclass'])) {
		$data['posaddress_store_subclass'] = $_REQUEST['posaddress_store_subclass'] ?: null;
	}
	
	if ($_REQUEST['posaddress_store_furniture']) {
		$data['posaddress_store_furniture'] = $_REQUEST['posaddress_store_furniture'];
	}
	
	if ($_REQUEST['posaddress_store_furniture_subclass']) {
		$data['posaddress_store_furniture_subclass'] = $_REQUEST['posaddress_store_furniture_subclass'];
	}
	

	$stars = 6;

	if (isset($_REQUEST['posaddress_perc_class'])) {
		$data['posaddress_perc_class'] = $_REQUEST['posaddress_perc_class'] ? $stars-$_REQUEST['posaddress_perc_class'] : null;
	}
	
	if (isset($_REQUEST['posaddress_perc_tourist'])) {
		$data['posaddress_perc_tourist'] = $_REQUEST['posaddress_perc_tourist'] ? $stars-$_REQUEST['posaddress_perc_tourist'] : null;
	}
	
	if (isset($_REQUEST['posaddress_perc_transport'])) {
		$data['posaddress_perc_transport'] = $_REQUEST['posaddress_perc_transport'] ? $stars-$_REQUEST['posaddress_perc_transport'] : null;
	}
	
	if (isset($_REQUEST['posaddress_perc_people'])) {
		$data['posaddress_perc_people'] = $_REQUEST['posaddress_perc_people'] ? $stars-$_REQUEST['posaddress_perc_people'] : null;
	}
	
	if (isset($_REQUEST['posaddress_perc_parking'])) {
		$data['posaddress_perc_parking'] = $_REQUEST['posaddress_perc_parking'] ? $stars-$_REQUEST['posaddress_perc_parking'] : null;
	}
	
	if (isset($_REQUEST['posaddress_perc_visibility1'])) {
		$data['posaddress_perc_visibility1'] = $_REQUEST['posaddress_perc_visibility1'] ? $stars-$_REQUEST['posaddress_perc_visibility1'] : null;
	}
	
	if (isset($_REQUEST['posaddress_perc_visibility2'])) {
		$data['posaddress_perc_visibility2'] = $_REQUEST['posaddress_perc_visibility2'] ? $stars-$_REQUEST['posaddress_perc_visibility2'] : null;
	}
	
		
	if (in_array('posaddress_store_floor', $fields)) {
		$data['posaddress_store_floor'] = $_REQUEST['posaddress_store_floor'];
	}
	
	if ($id) {
		
		$pos = new Pos();
		$pos->read($id);
		
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		
		$response = $pos->update($data);
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;

		if ($response) {
			$storelocator = new Store_Locator();
			$storelocator->update($id);
		}
	}
	
	echo json_encode(array(
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message
	));
	