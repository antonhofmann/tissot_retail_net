<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true); 
	
	$model = new Model($application); 
	
	// list order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "application_name, mail_template_code, mail_template_purpose";
	
	// order direction
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;

	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			mail_template_code LIKE \"%$keyword%\"
			OR mail_template_shortcut LIKE \"%$keyword%\"
			OR mail_template_purpose LIKE \"%$keyword%\"
			OR mail_template_dev_email LIKE \"%$keyword%\"
			OR application_name LIKE \"%$keyword%\"
		)";
	}

	if ($_REQUEST['applications']) {
		$filters['applications'] = "mail_template_application_id = ".$_REQUEST['applications'];
	}
	
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT 
			mail_template_id,
			mail_template_code,
			mail_template_purpose,
			mail_template_dev_email,
			mail_template_action_name,
			application_name
		FROM mail_templates
		LEFT JOIN mail_template_actions ON mail_template_actions.mail_template_action_id = mail_templates.mail_template_action_id
		LEFT JOIN applications ON mail_template_application_id = application_id
	")
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();
	
	if ($result) { 
		
		$totalrows = $model->totalRows();
		
		$link = $_REQUEST['form'];
		
		foreach ($result as $row) {
			
			$key = $row['mail_template_id'];
			$code = $row['mail_template_code'];
			$purpose = $row['mail_template_purpose'];
			
			// template
			$datagrid[$key]['mail_template_code'] = "<a href='$link/$key' >$code</a><span>$purpose</span>";
			
			// acion name
			$datagrid[$key]['mail_template_action_name'] = $row['mail_template_action_name'];
			
			$datagrid[$key]['application_name'] = $row['application_name'];
			
			// devmail
			if ($row['mail_template_dev_email']) {
				$devmod = true;
				$datagrid[$key]['mail_template_dev_email'] = $row['mail_template_dev_email'];
			}
			
		}
		
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));
		
		$pageIndex = $pager->index();
		$pageControlls = $pager->controlls();
	}

	
	// toolbox: hide utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
	
	// toolbox: button print
	if ($datagrid && $_REQUEST['export']) {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'href' => $_REQUEST['print'],
			'label' => $translate->print
		));
	}
	
	// toolbox: button add
	if ($_REQUEST['add']) {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();


	// dropdown applications
	$result = $model->query("
		SELECT DISTINCT
			application_id,
			application_name
		FROM db_retailnet.applications
		INNER JOIN db_retailnet.mail_templates ON mail_template_application_id = application_id
	")
	->filter($filters)
	->exclude('applications')
	->order('application_name')
	->fetchAll();

	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'applications',
			'id' => 'applications',
			'class' => 'submit',
			'value' => $_REQUEST['applications'],
			'caption' => $translate->all_applications
		));
	}
	
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";	
	}
	
	$table = new Table(array(
		'sort' => array(
			'column' => $sort,
			'direction' => $direction
		)
	));
	
	$table->datagrid = $datagrid;

	$table->application_name(
		Table::PARAM_SORT,
		Table::ATTRIBUTE_NOWRAP,
		'width=20%'
	);
	
	$table->caption('mail_template_code', 'Template');
	
	$table->mail_template_code(
		Table::PARAM_SORT
	);
	
	$table->mail_template_action_name(
		Table::PARAM_SORT,
		Table::ATTRIBUTE_NOWRAP,
		'width=20%'
	);
	
	if ($devmod) {
		$table->mail_template_dev_email(
			Table::ATTRIBUTE_NOWRAP,
			'width=20%'
		);
	}
	
	$table->footer($pageIndex);
	$table->footer($pageControlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	