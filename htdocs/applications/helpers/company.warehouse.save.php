<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	$id = $_REQUEST['address_warehouse_id'];
	$company = $_REQUEST['address_warehouse_address_id'];
	
	$data = array();
	$data['address_warehouse_address_id'] = $company;
	$data['address_warehouse_name'] = $_REQUEST['address_warehouse_name'];
	
	if ($_REQUEST['controller'] <> 'companies') {
		$data['address_warehouse_active'] = 1;
	}
	elseif (in_array('address_warehouse_active', $fields)) {
		$data['address_warehouse_active'] = ($_REQUEST['address_warehouse_active']) ? 1 : 0;
	}
	
	if (in_array('address_warehouse_sap_customer_number', $fields)) {
		$data['address_warehouse_sap_customer_number'] = $_REQUEST['address_warehouse_sap_customer_number'];
	}
	
	if (in_array('address_warehouse_sap_shipto_number', $fields)) {
		$data['address_warehouse_sap_shipto_number'] = $_REQUEST['address_warehouse_sap_shipto_number'];
	}
	
	if ($_REQUEST['address_warehouse_name'] && $data && $company) {
		
		$company_warehouse = new Company_Warehouse();
		$company_warehouse->read($id);
		
		if ($company_warehouse->id) { 
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $company_warehouse->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $company_warehouse->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			if ($response && $_REQUEST['redirect']) {
				$redirect = ($_REQUEST['controller']=='companies') ? $_REQUEST['redirect']."/$id" : $_REQUEST['redirect'];
			}
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	