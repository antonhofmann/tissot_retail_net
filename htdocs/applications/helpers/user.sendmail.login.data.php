<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$auth = User::instance();
	$translate = Translate::instance();
	
	// user
	$user = new User($_REQUEST['mail_template_user']);
	
	if ($user->login && $user->password) {
		
		// template
		$template = new Mail_Template();
		$template->read_from_shortcut('user_account_send_login_data');
		
		$subject = ($_REQUEST['login_data_subject']) ? $_REQUEST['login_data_subject'] : $template->subject;
		$content = ($_REQUEST['login_data_content']) ? $_REQUEST['login_data_content'] : $template->text;
		
			
		if ($user->email) {
			
			$model = new Model(Connector::DB_CORE);
			
			$cc = array();
			
			// sendmail
			$sendmail = new Send_Mail();
			
			// get user roles
			$roles = $user->roles($user->id);
			
			// cc recipients
			if ($roles) {
								
				$roles = join(',', $roles);

				$result = $model->query("
					SELECT DISTINCT 
						role_responsible_user_id,
						user_email,
						user_email_deputy
					FROM roles 
					INNER JOIN users ON user_id = role_responsible_user_id 
					WHERE role_id IN ($roles)		
				")->fetchAll();
				
				if ($result) {
					foreach ($result as $row) {
						
						if ($row['user_email'] <> $auth->email) {
							$cc[] = $row['user_email'];
						}
						
						if ($row['user_email_deputy'] <> $auth->email) {
							$cc[] = $row['user_email_deputy'];
						}
					}
				}
			}
			
			$ccRecipients = array();
			
			// form cc recipients
			if ($_REQUEST['login_data_cc']) {
	
				$ccRequest = trim($_POST['login_data_cc']);
				$ccRequest = explode("\n", $ccRequest);
				$ccRequest = array_filter($ccRequest, 'trim');
				
				foreach ($ccRequest as $row) {
					
					$comma = strpos($row, ',');
					
					if ($comma === false) {
						array_push($ccRecipients, trim($row));
					} else {
			
						$arr = explode(',', $row);
						$arr = array_filter($arr, 'trim');
						
						foreach ($arr as $email) {
							array_push($ccRecipients, trim($email));
						}
					}
				}
			}
			
			$cc = array_merge($ccRecipients, $cc);
			$cc = array_filter(array_unique($cc));
			
			// devmod recipients
			if ($template->devmod && $template->devmod_email) {
				
				// cc recipients
				if ($cc) {
					$cc_recipients = ' CC: ('.join(', ', $cc).')';
					unset($cc);
				}
				
				$subject = "$subject (DEVMOD)";
			
				// devmode signature
				$content .= "\r\n\nIMPORTANT: Sendmail is in development mode. This email is dedicated for recipient(s): $user->email $cc_recipients";
			
				// replace user email to devmode email
				$user->data['user_email'] = $template->devmod_email;
			}
			
			$sendmail->subject($user->id, $subject);
			$sendmail->content($user->id, $content);
			$sendmail->recipient($user->id, $user->firstname." ".$user->name);
			$sendmail->email($user->id, $user->email);
			
			if ($cc) {	
				foreach ($cc as $email) {
					$sendmail->cc($user->id, $email);
				}
			}
			
			$sendmail->template($template->id);
			$response = $sendmail->send();
			$message = $translate->message_send_login_data;
		}
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $_REQUEST['redirect']
	));
	