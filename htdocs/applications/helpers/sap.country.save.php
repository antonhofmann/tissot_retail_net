<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$user = User::instance();
	$translate = Translate::instance();
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$id = $_REQUEST['sap_country_id'];
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$data = array();
	
	if ($_REQUEST['sap_country_sap_code']) {
		$data['sap_country_sap_code'] = $_REQUEST['sap_country_sap_code'];
	}
	
	if ($_REQUEST['sap_country_country_name']) {
		$data['sap_country_country_name'] = $_REQUEST['sap_country_country_name'];
	}
	
	if ($_REQUEST['sap_county_retailnet_country_id']) {
		$data['sap_county_retailnet_country_id'] = $_REQUEST['sap_county_retailnet_country_id'];
	}
	
	if ($data) {
	
		$sapCountry = new SapCountry();
		$sapCountry->read($id);
	
		if ($sapCountry->id) {
			
			$data['user_modified'] = $user->login;
			
			$response = $sapCountry->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $sapCountry->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			if ($_REQUEST['redirect']) {
				$redirect = $_REQUEST['redirect']."/$id";
			}
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	