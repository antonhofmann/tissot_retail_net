<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];

	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	$model = new Model($application);

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'mps_material_planning_type_name';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			mps_material_planning_type_name LIKE \"%$keyword%\"
		)";
	}
	
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			mps_material_planning_type_id,
			mps_material_planning_type_name,
			IF(mps_material_planning_is_dummy, '<img src=\"/public/images/icon-checked.png\">' , NULL) AS mps_material_planning_is_dummy,
			IF(mps_material_planning_ordersheet_standrad_dummy_quantity, '<img src=\"/public/images/icon-checked.png\">' , NULL) AS dummy_quantity,
			IF(mps_material_planning_ordersheet_approvment_automatically, '<img src=\"/public/images/icon-checked.png\">' , NULL) AS approvment_automatically,
			IF(mps_material_planning_ordersheet_distrubution_automatically, '<img src=\"/public/images/icon-checked.png\">' , NULL) AS distrubution_automatically
		FROM mps_material_planning_types
	")
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();
	
	if ($result) {
	
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
	
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
	
	// toolbox: utton print
	if ($datagrid && $_REQUEST['export']) {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'href' => $_REQUEST['export'],
			'label' => $translate->add_new
		));
	}
	
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	
	$table->mps_material_planning_type_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		"href=".$_REQUEST['form']
	);
	
	$table->mps_material_planning_is_dummy(
		Table::ATTRIBUTE_NOWRAP,
		"width=10%",
		"align=center"
	);
	
	$table->dummy_quantity(
		Table::ATTRIBUTE_NOWRAP,
		"caption=Standard Quantities",
		"width=10%",
		"align=center"
	);
	
	$table->approvment_automatically(
		Table::ATTRIBUTE_NOWRAP,
		"caption=Auto Approvment",
		"width=10%",
		"align=center"
	);
	
	$table->distrubution_automatically(
		Table::ATTRIBUTE_NOWRAP,
		"caption=Auto Distribution",
		"width=10%",
		"align=center"
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	