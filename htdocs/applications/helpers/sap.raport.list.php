<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	$model = new Model($application);

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'country_name, address_company, mps_mastersheet_name';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// sql offset
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rows = $settings->limit_pager_rows;
	$offset = ($page-1) * $settings->limit_pager_rows;
	
	// filter: limited permission
	if($_REQUEST['limited']) {
		$filters['limited'] = "
			mps_ordersheet_address_id = ".$user->address." 
			AND mps_ordersheet_workflowstate_id <> 7 
		";
	}
		
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			address_company LIKE \"%$keyword%\" 
			OR country_name LIKE \"%$keyword%\" 
			OR mps_mastersheet_name LIKE \"%$keyword%\" 
			OR mps_workflow_state_name LIKE \"%$keyword%\" 
			OR place_name LIKE \"%$keyword%\"
		)";
	}
	
	// filer: mastersheet year
	if ($_REQUEST['mastersheet_years']) {
		$filters['mastersheet_years'] = "mps_mastersheet_year = ".$_REQUEST['mastersheet_years'];
	}
	
	// filer: countries
	if ($_REQUEST['countries']) {
		$filters['countries'] = "country_id = ".$_REQUEST['countries'];
	}
	
	// filer: mastersheet
	if ($_REQUEST['mastersheets']) {
		$filters['mastersheets'] = "mps_ordersheet_mastersheet_id = ".$_REQUEST['mastersheets'];
	}
	
	// filer: workflowstate
	if ($_REQUEST['workflowstates']) {
		$filters['workflowstates'] = "mps_ordersheet_workflowstate_id = ".$_REQUEST['workflowstates'];
	}
	
	$filters['default'] = "(sap_confirmed_item_status_code > 0 OR sap_shipped_item_status_code > 0)";
	
	$binds = array(
		Ordersheet::DB_BIND_COMPANIES,
		Ordersheet::DB_BIND_MASTERSHEETS,
		Ordersheet::DB_BIND_WORKFLOW_STATES,
		Company::DB_BIND_COUNTRIES,
		Company::DB_BIND_PLACES,
		Ordersheet::DB_BIND_ORDERSHEET_ITEMS,
		"INNER JOIN mps_ordersheet_item_delivered ON mps_ordersheet_item_delivered_ordersheet_item_id = mps_ordersheet_item_id",
		"LEFT JOIN sap_confirmed_items ON sap_confirmed_item_mps_distribution_id = mps_ordersheet_item_delivered_id",
		"LEFT JOIN sap_shipped_items ON sap_shipped_item_mps_distribution_id = mps_ordersheet_item_delivered_id"
	);
	
	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT	
			mps_ordersheet_id,
			mps_ordersheet_workflowstate_id,
			address_company,
			country_name,
			mps_mastersheet_name,
			mps_workflow_state_name,
			DATE_FORMAT(mps_ordersheet_openingdate,'%d.%m.%Y') AS openingdate,
			DATE_FORMAT(mps_ordersheet_closingdate,'%d.%m.%Y') AS closingdate
		FROM mps_ordersheets
	")
	->bind($binds)
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rows)
	->fetchAll();
	
	if ($result) {
	
		$totalrows = $model->totalRows();
		$timestamp = date::timestamp();
		$icon_warning = ui::icon('warrning');
		
		foreach ($result as $row) {
			$key = $row['mps_ordersheet_id'];
			$datagrid[$key] = $row;
			$datagrid[$key]['mps_ordersheet_openingdate'] = $row['openingdate'];
			$datagrid[$key]['mps_ordersheet_closingdate'] = $row['closingdate'];
		}
	
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	
	// toolbox: utton print
	if ($datagrid && $_REQUEST['print']) {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'href' => $_REQUEST['print'],
			'label' => $translate->print
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();

	// data: mastersheet years
	$model->query("
		SELECT DISTINCT mps_mastersheet_year, mps_mastersheet_year AS year 
		FROM mps_ordersheets
	")
	->bind($binds)
	->filter($filters)
	->exclude('mastersheet_years')
	->order('mps_mastersheet_year');
	
	if ($_REQUEST['countries']) {
		$model->extend(" OR mps_mastersheet_year = ".$_REQUEST['mastersheet_years']);
	}

	$result = $model->fetchAll();
	
	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'mastersheet_years',
			'id' => 'mastersheet_years',
			'class' => 'submit',
			'value' => $_REQUEST['mastersheet_years'],
			'caption' => $translate->all_years
		));
	}
	
	// data: countries
	$model->query("
		SELECT DISTINCT country_id, country_name 
		FROM mps_ordersheets
	")
	->bind($binds)
	->filter($filters)
	->exclude('countries')
	->order('country_name');
	
	if ($_REQUEST['countries']) {
		$model->extend(" OR country_id = ".$_REQUEST['countries']);
	}

	$result = $model->fetchAll();
	
	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'countries',
			'id' => 'countries',
			'class' => 'submit',
			'value' => $_REQUEST['countries'],
			'caption' => $translate->all_countries
		));
	}
	
	// data: workflow states
	$model->query("
		SELECT DISTINCT mps_workflow_state_id, mps_workflow_state_name 
		FROM mps_ordersheets
	")
	->bind($binds)
	->filter($filters)
	->exclude('workflowstates')
	->order('mps_workflow_state_code');

	if ($_REQUEST['workflowstates']) {
		$model->extend(" OR mps_workflow_state_id = ".$_REQUEST['workflowstates']);
	}
	
	$result = $model->fetchAll();
	
	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'workflowstates',
			'id' => 'workflowstates',
			'class' => 'submit',
			'value' => $_REQUEST['workflowstates'],
			'caption' => $translate->all_workflow_states
		));
	}
	
	// data: mastersheets
	$result = $model->query("
		SELECT DISTINCT mps_mastersheet_id, mps_mastersheet_name 
		FROM mps_ordersheets
	")
	->bind($binds)
	->filter($filters)
	->exclude('mastersheets')
	->order('mps_mastersheet_name');
	
	if ($_REQUEST['mastersheets']) {
		$model->extend(" OR mps_mastersheet_id = ".$_REQUEST['mastersheets']);
	}

	$result = $model->fetchAll();
	
	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'mastersheets',
			'id' => 'mastersheets',
			'class' => 'submit',
			'value' => $_REQUEST['mastersheets'],
			'caption' => $translate->all_mastersheets
		));
	}
	
	// toolbox: form
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	
	$table->country_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=15%'
	);
	
	$table->address_company(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=20%'
	);
	
	$table->mps_mastersheet_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		"href=".$_REQUEST['show']
	);
	
	$table->mps_ordersheet_openingdate(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->mps_ordersheet_closingdate(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->mps_workflow_state_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	if (!$archived) {
		
		
		if ( $datagrid && _array::key_exists('warningicon', $datagrid)) {
			$table->warningicon(
				'width=20px'
			);
		}
	}
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	