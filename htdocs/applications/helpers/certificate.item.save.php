<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$user = User::instance();
	$translate = Translate::instance();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	// product line
	$id = $_REQUEST['id'];
	$item = $_REQUEST['item'];
	
	$certificate = new Certificate();
	$certificate->read($id);
	
	if ($certificate->id && $item) {
	
		if ($_REQUEST['checked']) {
			$response = $certificate->item()->create($item);
		} else {
			$response = $certificate->item()->delete($item);
		}

		$message = ($response) ? $translate->message_request_submitted : $translate->message_request_failure;
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));
	