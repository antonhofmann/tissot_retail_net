<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['mps_material_collection_category_file_id'];
	$collection_category = $_REQUEST['mps_material_collection_category_file_collection_id'];

	$upload_path = "/public/data/files/$application/collectioncategories/$collection_category";
	
	$data = array();
	
	if ($_REQUEST['mps_material_collection_category_file_title']) {
		$data['mps_material_collection_category_file_title'] = $_REQUEST['mps_material_collection_category_file_title'];
	}
	
	if ($_REQUEST['has_upload'] && $_REQUEST['mps_material_collection_category_file_path']) {
		$path = $data['mps_material_collection_category_file_path'] = upload::move($_REQUEST['mps_material_collection_category_file_path'], $upload_path);
		$extension = file::extension($path);
		$file_type = new File_Type();
		$data['mps_material_collection_category_file_type'] = $file_type->get_id_from_extension($extension);
	}
	
	if (isset($_REQUEST['mps_material_collection_category_file_description'])) {
		$data['mps_material_collection_category_file_description'] = $_REQUEST['mps_material_collection_category_file_description'];
	}
	
	if (in_array('mps_material_collection_category_file_visible', $fields)) {
		$data['mps_material_collection_category_file_visible'] = ($_REQUEST['mps_material_collection_category_file_visible']) ? 1 : 0;
	}
	
	if ($collection_category && $data) {
		
		$data['mps_material_collection_category_file_collection_id'] = $collection_category;
	
		$collection_category_file = new Material_Collection_Category_File($application);
		$collection_category_file->read($id);
	
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $collection_category_file->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $collection_category_file->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'path' => $path
	));
	