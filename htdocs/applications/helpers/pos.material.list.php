<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$model = new Model($application);
	
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "
		mps_material_purpose_name,
		mps_material_category_name,
		mps_material_code,
		mps_material_name
	";
	
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rows = $settings->limit_pager_rows;
	$offset = ($page-1) * $rows;
		
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			mps_pos_material_id,
			mps_material_purpose_name,
			mps_material_category_name,
			mps_material_code,
			mps_material_name,
			mps_pos_material_quantity_in_use,
			mps_pos_material_quantity_on_stock,
			mps_pos_material_installation_year,
			mps_pos_material_deinstallation_year,
			mps_material_width,
			mps_material_height,
			mps_material_length,
			mps_material_radius
		FROM mps_pos_materials
	")
	->bind(array(
		Pos_Material::DB_BIND_MATERIALS,
		Material::DB_BIND_CATEGORIES,
		Material::DB_BIND_PURPOSES
	))
	->filter('pos', "mps_pos_material_posaddress = $id")
	->order($sort, $direction)
	->offset($offset, $rows)
	->fetchAll();
	
	if ($result) {
		
		$totalrows = $model->totalRows();
		
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));
		
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
		
		foreach ($result as $row) {
			$datagrid[$row['mps_pos_material_id']] = $row;
		}
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	
	$table->mps_material_purpose_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->mps_material_category_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->mps_material_code(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=15%'
	);
	
	$table->mps_material_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		"href=".$_REQUEST['form']
	);
	
	$table->mps_pos_material_quantity_in_use(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		Table::PARAM_SORT,
		'width=5%'
	);
	
	$table->mps_pos_material_quantity_on_stock(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		Table::PARAM_SORT,
		'width=5%'
	);
	
	$table->mps_pos_material_installation_year(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->mps_pos_material_deinstallation_year(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->mps_material_width(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=5%'
	);
	
	$table->mps_material_height(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=5%'
	);
	
	$table->mps_material_length(
		Table::ATTRIBUTE_NOWRAP,
		'width=5%'
	);
	
	$table->mps_material_radius(
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=5%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	