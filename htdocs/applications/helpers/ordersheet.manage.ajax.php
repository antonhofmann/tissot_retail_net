<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$application = $_REQUEST['application'];
	$section = $_REQUEST['section'];
	
	$model = new Model($application);
	
	switch ($section) {
		
		case 'mastersheets':
			
			$value = $_REQUEST['value'];
			
			$json[][] = "Select";

			$result = $model->query("
				SELECT mps_mastersheet_id, mps_mastersheet_name 
				FROM mps_mastersheets
				WHERE mps_mastersheet_archived = 0 AND mps_mastersheet_consolidated = 0 AND mps_mastersheet_year = $value
				ORDER BY mps_mastersheet_name
			")->fetchAll();
			
			if ($result) {
				foreach ($result as $row) {
					list($key,$value) = array_values($row);
					$json[][$key] = $value;
				}
			}
			
		break;
		
		case 'has_ordersheets':
				
			$mastersheet = $_REQUEST['mps_mastersheet_id'];
			$result = $model->query("SELECT * FROM mps_ordersheets WHERE mps_ordersheet_mastersheet_id = $mastersheet")->fetchAll();
			
			$json['response'] = ($result) ? true : false;
				
		break;
	}
	
	echo json_encode($json);
	