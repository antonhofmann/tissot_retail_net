<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "item_code,item_name";
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

$model = new Model($application);

$sth = $model->db->prepare("
	SELECT DISTINCT
		posorder_order
	FROM db_retailnet.posorders
	INNER JOIN db_retailnet.projects ON project_order = posorder_order
	INNER JOIN db_retailnet.orders ON order_id = posorder_order
	WHERE posorder_type = 1
		AND posorder_project_kind IN (1,2,3,4)
		AND (project_actual_opening_date <> '0000-00-00' AND project_actual_opening_date IS NOT NULL)
		AND (project_closing_date <> '0000-00-00' OR project_closing_date IS NULL)
		AND order_actual_order_state_code != '900'
		AND posorder_posaddress = ?
	ORDER BY posorder_order DESC
");

$sth->execute(array($id));
$projectorders = $sth->fetchAll();

if ($projectorders) {

	$sthOrderItems = $model->db->prepare("
		SELECT 
			item_id,
			FORMAT(order_item_quantity, 0) AS order_item_quantity,
			item_code,
			item_name,
			item_width,
			item_height,
			item_length,
			item_radius
		FROM db_retailnet.posorders
		INNER JOIN db_retailnet.orders ON order_id = posorder_order 
		INNER JOIN db_retailnet.order_items ON order_item_order = order_id 
		INNER JOIN db_retailnet.items ON item_id = order_item_item 
		WHERE item_visible_in_mps = 1  
			AND posorder_type = 1  
			AND posorder_order = ?  
		 ORDER BY item_code,item_name asc
	");

	$sthOrderCatalogItems = $model->db->prepare("
		SELECT DISTINCT
			orders.order_number,
			item_id,
			FORMAT(order_item_quantity, 0) AS order_item_quantity,
			item_code,
			item_name,
			item_width,
			item_height,
			item_length,
			item_radius
		FROM db_retailnet.order_items_in_projects
		INNER JOIN db_retailnet.projects ON order_items_in_projects.order_items_in_project_project_order_id = projects.project_order
		INNER JOIN db_retailnet.order_items ON order_items_in_projects.order_items_in_project_order_id = order_items.order_item_order
		INNER JOIN db_retailnet.items ON order_items.order_item_item = items.item_id
		INNER JOIN db_retailnet.orders ON order_items.order_item_order = orders.order_id
		WHERE items.item_visible_in_mps = 1 AND order_items_in_project_project_order_id = ?
		ORDER BY projects.project_number,items.item_code
	");

	$sthProjectHeader = $model->db->prepare("
		SELECT 
			CONCAT(posorder_ordernumber, ', ', product_line_name, ', ', projectkind_name, ' (Status ',order_actual_order_state_code, ')') AS caption
		FROM db_retailnet.posorders
		INNER JOIN db_retailnet.product_lines ON product_line_id = posorder_product_line
		INNER JOIN db_retailnet.projectkinds ON projectkind_id = posorder_project_kind 
		INNER JOIN db_retailnet.orders ON order_id = posorder_order 
		WHERE posorder_order = ?
	");
	
	foreach ($projectorders as $row) {
		
		$order = $row['posorder_order'];
		
		// get order items
		$sthOrderItems->execute(array($order));
		$result = $sthOrderItems->fetchAll();
			
		if ($result) {

			foreach ($result as $row) {
				$item = $row['item_id'];
				$_DATAGRID[$item] = $row;
			}

			// get order catalog items
			$sthOrderCatalogItems->execute(array($order));
			$result = $sthOrderCatalogItems->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					$item = $row['item_id'];
					$_DATAGRID[$item] = $row;
				}
			}

			// get order header
			$sthProjectHeader->execute(array($order));
			$header = $sthProjectHeader->fetch();
			$projectCaption = $header['caption'];

			break; // show only order from last order
		}
	}
}


if ($_DATAGRID) {
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='1' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $_DATAGRID;
	
	$table->item_code(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=15%'
	);
	
	$table->item_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);
	
	$table->order_item_quantity(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=5%'
	);
	
	$table->item_width(
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=5%'
	);
	
	$table->item_height(
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=5%'
	);
	
	$table->item_length(
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=5%'
	);
	
	$table->item_radius(
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=5%'
	);
	
	$index = $translate->total_rows.": ".count($_DATAGRID);
	$table->footer("<div class='table-index'>$index</div>");
	$table = $table->render();
	
	if ($toolbox) {
		$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
	}
	
	echo $toolbox;
	echo "<h5>$translate->project: $projectCaption</h5>";
	echo $table;
}
