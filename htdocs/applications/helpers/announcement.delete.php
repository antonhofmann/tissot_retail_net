<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	if ($id && user::permission(Announcement::PERMISSION_EDIT)) {
		
		$announcement = new Announcement($appliction);
		$announcement->read($id);
		$delete = $announcement->delete($id);
		
		if ($delete) {
			file::remove($announcement->file);
			Message::request_deleted();
			url::redirect("/$appliction/$controller");
		} else {
			Message::request_failure();
			url::redirect("/$appliction/$controller/$action/$id");
		}
	}
	else {
		Message::access_denied();
		url::redirect("/$appliction/$controller");
	}