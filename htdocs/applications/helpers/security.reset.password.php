<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	// open user session
	if (!Session::get('user_id')) {
		Session::init();
		Session::set('user_id', $_REQUEST['id']);
	}

	// default project settings
	$settings = Settings::init();
	$settings->load('data');
	
	// user
	$user = User::instance();
	
	// used psswords
	$usedPasswords = $user->getUsedPasswords();
	$usedPasswords = ($usedPasswords) ? $usedPasswords : array();
	
	// save only last 24 passwords
	if (count($usedPasswords) > 25) {
		array_shift($usedPasswords);
	}
	
	array_push($usedPasswords, $_REQUEST['new_password']);
	$user->updateUsedPasswords($usedPasswords);
	
	// update user	
	$response = $user->update(array(
		'user_password_reset_id' => null,
		'user_password' => $_REQUEST['new_password'],
		'user_password_reset' => date("Y-m-d")
	));
	
	//send email notification
	if($response && $_REQUEST['password_notification']) {
		
		require_once PATH_LIBRARIES.'phpmailer/class.phpmailer.php';
		
		// template
		$mail_template = new Mail_Template();
		$mail_template->read_from_shortcut('password.reset');
		
		// subject
		$subject = $mail_template->subject;
		
		$user->data['password'] = $_REQUEST['new_password'];
		
		// content
		$content = content::render($mail_template->text, $user->data);
		
		// recipient
		$recipient = ucfirst($user->firstname).' '.ucfirst($user->name);
		
		$mail = new PHPMailer();
		$mail->SetFrom($settings->mail_noreply, $settings->project_name);
		$mail->AddAddress($user->email, $recipient);
		$mail->Subject = $subject;
		$mail->Body = $content;
		$mail->Send();
		
	}
	
	if ($response) {		
		Message::password_reset_success();
	}
	else {
		$translate = Translate::instance();
		$message = $translate->message_sendmail_failure;
	}
	
	Session::remove('user_id');
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));
	