<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$id = $_REQUEST['id'];
	$section = $_REQUEST['section'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	$model = new Model(Connector::DB_CORE);

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'assistance_section_link_link';

	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "( 
			assistance_section_link_link LIKE \"%$keyword%\"
		)";
	}
	
	// datagrid
	$result = $model->query("
		SELECT 
			assistance_section_link_id,
			assistance_section_link_link
		FROM assistance_section_links
		WHERE assistance_section_link_section_id = $section
	")
	->filter($filters)
	->order($sort, $direction)
	->fetchAll();

	if ($result) {
	
		$datagrid = _array::datagrid($result);
		
		foreach ($datagrid as $key => $row) {
			$url = $row['assistance_section_link_link'];
			$datagrid[$key]['assistance_remove'] = "<img src='/public/images/icon-remover.png' height='16px' class='remover' data-url='$url' >";
		}
	}
	
	if ($toolbox) {
		$toolbox = "
			<form class=toolbox method=post>
				<input type=hidden id=sort name=sort value='$sort' class='submit' />
				<input type=hidden id=direction name=direction value='$direction' class='submit' />
			</form>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	$table->dataloader($dataloader);
	
	$table->assistance_section_link_link(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);
	
	$table->assistance_remove(
		'width=1%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	