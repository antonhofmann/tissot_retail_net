<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$id = $_REQUEST['stocklist'];
	
	$model = new Model();
	
	switch ($_REQUEST['section']) {
		
		case 'check_additional_column':
			
			$json['response'] = false;
			
			$result = $model->query("
				SELECT scpps_week_additional_columns
				FROM scpps_weeks
				WHERE scpps_week_stocklist_id = $id
			")->fetchAll();
			
			if ($result) {
				foreach ($result as $row) {
					
					$additionals = unserialize($row['scpps_week_additional_columns']);
					
					if ($additionals[$_REQUEST['field']]) {
						$json['response'] = true;
						break;
					}
				}
			}
			
		break;
		
		case 'save_additional_column':
			
			$column = $_REQUEST['field'];
			$caption = $_REQUEST['caption'];
			
			$stocklist = new Stocklist();
			$stocklist->read($id);
		
			// additional columns
			$columns = ($stocklist->columns) ? unserialize($stocklist->columns) : array();
			$columns[$column] = $caption;
 
			if ($columns[$column]) {
				$response = $stocklist->update(array(
					'scpps_stocklist_columns' => serialize($columns),
					'user_modified' => $user->login
				));
			}
			
			$message = ($response) ? $translate->message_request : $translate->error_request;
			
			$json = array(
				'response' => $response,
				'message' => $message
			);
			
		break;
		
		case 'remove_additional_column':
			
			$column = $_REQUEST['field'];
			$caption = $_REQUEST['caption'];
				
			// stock list
			$stocklist = new Stocklist();
			$stocklist->read($id);
			
			// additional columns
			$columns = ($stocklist->columns) ? unserialize($stocklist->columns) : array();
			
			if ($columns[$column]) {
						
				$result = $model->query("
					SELECT scpps_week_id, scpps_week_additional_columns
					FROM scpps_weeks
					WHERE scpps_week_stocklist_id = $id
				")->fetchAll();
					
				if ($result) {
					
					// stock list week
					$stocklist_week = new Stocklist_Week();
					
					foreach ($result as $row) {	
						
						$additionals = unserialize($row['scpps_week_additional_columns']);
						
						if ($additionals[$column]) {
							
							if ($_REQUEST['tag'] == 'cascading') {
								
								// load week data
								$stocklist_week->read($row['scpps_week_id']);
								
								// remove this column
								unset($additionals[$column]);
								$additionals = ($additionals) ? serialize($additionals) : null;
								
								// save week data
								$stocklist_week->update(array(
									'scpps_week_additional_columns'	=> $additionals
								));
								
							} else {
								$has_week_data = true;
								break;
							}
						}
					}
				}
			
				if (!$has_week_data) {
					
					unset($columns[$column]);
					$columns = ($columns) ? serialize($columns) : null;
					
					$response = $stocklist->update(array(
						'scpps_stocklist_columns' => $columns,
						'user_modified' => $user->login
					));
				}
			}
			
			$message = ($response) ? $translate->message_request : $translate->error_request;
				
			$json = array(
				'response' => $response,
				'message' => $message
			);
			
		break;
	}
	
	header('Content-Type: text/json');
	echo json_encode($json);
	