<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	switch ($_REQUEST['section']) {
			
		case 'material-info':
			
			$application = $_REQUEST['application'];
			$id = $_REQUEST['id'];
		
			$model = new Model($application);
			
			$result = $model->query("
				SELECT
					mps_material_code AS code,
					mps_material_name AS name,
					mps_material_hsc AS hsc,
					mps_material_price AS price,
					mps_material_planning_type_name AS planning,
					mps_material_collection_code AS collection,
					mps_material_collection_category_code AS category
				FROM mps_materials
			")
			->bind(Material::DB_BIND_COLLECTIONS)
			->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
			->bind(Material::DB_BIND_PLANNING_TYPES)
			->filter('material', "mps_material_id = $id")
			->fetch();
			
			if ($result) {
				
				extract($result);
			
				$json['content'] = "
					<table class=\"default tooltip\" >
						<tr>
							<td valign=top nowrap=nowrap >Planning Type: </td>
							<td valign=top >$planning</td>
						</tr>
						<tr>
							<td valign=top nowrap=nowrap >Collection Code: </td>
							<td valign=top >$collection</td>
						</tr>
						<tr>
							<td valign=top nowrap=nowrap >Category Code: </td>
							<td valign=top >$category</td>
						</tr>
						<tr>
							<td valign=top nowrap=nowrap >Item Code: </td>
							<td valign=top >$code</td>
						</tr>
						<tr>
							<td valign=top nowrap=nowrap >Item Name: </td>
							<td valign=top >$name</td>
						</tr>
						<tr>
							<td valign=top nowrap=nowrap >HSC: </td>
							<td valign=top >$hsc</td>
						</tr>
						<tr>
							<td valign=top nowrap=nowrap >Price: </td>
							<td valign=top >$price</td>
						</tr>
					</table>
				";
			}
			
		break;
		
		case 'pos-info':
			
			$application = $_REQUEST['application'];
			$id = $_REQUEST['id'];
		
			$model = new Model($application);
			
			$result = $model->query("
				SELECT
					posaddress_franchisee_id,
					posaddress_name AS name,
					posaddress_address AS address,
					CONCAT(posaddress_zip, ' ', country_name) AS country,
					CONCAT(mps_distchannel_group_name, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) AS distribution_channel, posaddress_sapnumber, 
					posowner_type_name AS owner_type,
					postype_name AS type,
					watches.mps_turnoverclass_code AS watch,
					bijoux.mps_turnoverclass_code AS bijoux
				FROM db_retailnet.posaddresses
			")
			->bind(Pos::DB_BIND_COUNTRIES)
			->bind(Pos::DB_BIND_POSOWNER_TYPES)
			->bind(Pos::DB_BIND_POSTYPES)
			->bind('LEFT JOIN db_retailnet.mps_distchannels ON mps_distchannel_id = posaddress_distribution_channel')
			->bind('LEFT JOIN db_retailnet.mps_distchannel_groups ON mps_distchannels.mps_distchannel_group_id = mps_distchannel_groups.mps_distchannel_group_id')
			->bind('LEFT JOIN db_retailnet.mps_turnoverclasses watches ON watches.mps_turnoverclass_id = posaddress_turnoverclass_watches')
			->bind('LEFT JOIN db_retailnet.mps_turnoverclasses bijoux ON bijoux.mps_turnoverclass_id = posaddress_turnoverclass_bijoux')
			->filter('pos', "posaddress_id = $id")
			->fetch();
			
			if ($result) {
				
				$materials = $model->query("
					SELECT DISTINCT
						mps_material_code,
						mps_material_name,
						mps_pos_material_quantity_in_use
					FROM mps_pos_materials
				")
				->bind(Pos_Material::DB_BIND_MATERIALS)
				->filter('pos', "mps_pos_material_posaddress = $id")
				->order('mps_material_code')
				->fetchAll();
				
				if ($materials) {
				
					$material_list = "<tr>
						<td valign=top >LTM in use:</td>
						<td valign=top >";
					
					foreach ($materials as $row) {
						$pieces = ($row['mps_pos_material_quantity_in_use']) ? "(".$row['mps_pos_material_quantity_in_use']." pcs.) " : null;
						$material_list .= "<span>".$row['mps_material_code']."  $pieces</span>";
					}
					
					$material_list .= "</td></tr>";
				}
				
				$company = new Company(Connector::DB_CORE);
				$company->read($result['posaddress_franchisee_id']);
				
				$json['content'] = "
					<table class=\"default tooltip\" >
						<tr>
							<td valign=top nowrap=nowrap width='20%' >POS Name: </td>
							<td valign=top >".$result['name']."</td>
						</tr>
						<tr>
							<td valign=top nowrap=nowrap >Address: </td>
							<td valign=top >".$result['address']."</td>
						</tr>
						<tr>
							<td valign=top nowrap=nowrap >ZIP Country: </td>
							<td valign=top >".$result['country']."</td>
						</tr>
						<tr>
							<td valign=top nowrap=nowrap >Distribution Channel: </td>
							<td valign=top >".$result['distribution_channel']."</td>
						</tr>
						<tr>
							<td valign=top nowrap=nowrap >Legal Type: </td>
							<td valign=top >".$result['owner_type']."</td>
						</tr>
						<tr>
							<td valign=top nowrap=nowrap >POS Type: </td>
							<td valign=top >".$result['type']."</td>
						</tr>
						<tr>
							<td valign=top nowrap=nowrap >SAP: </td>
							<td valign=top >".  $result['posaddress_sapnumber'] ."</td>
						</tr>
						$material_list
					</table>
				";
			}
			
		break;
		
		case 'warehouse-info':
			
			$application = $_REQUEST['application'];
			$id = $_REQUEST['id'];
			
			$companyWarehouse = new Company_Warehouse();
			$companyWarehouse->read($id);
			
			if ($companyWarehouse->id) {

				$json['content'] = "
					<table class=\"default tooltip\" >
						<tr>
							<td valign=top nowrap=nowrap width='20%' >Warehouse Name: </td>
							<td valign=top >$companyWarehouse->name</td>
						</tr>
						<tr>
							<td valign=top nowrap=nowrap >SAP Customer Code: </td>
							<td valign=top >$companyWarehouse->sap_customer_number</td>
						</tr>
						<tr>
							<td valign=top nowrap=nowrap >SAP Shipto NUmber: </td>
							<td valign=top >$companyWarehouse->sap_shipto_number</td>
						</tr>
					</table>
				";
			}
			
		break;

	}
	
	header('Content-Type: text/json');
	echo json_encode($json);
	