<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	$translate = Translate::instance();
	
	$ip = $_REQUEST['ip'];
	
	if ($ip) {
		
		$model = new Model(Connector::DB_CORE);
		
		$response = $model->db->exec("
			DELETE FROM sec_lockedips
			WHERE sec_lockedip_ip = '$ip'
		");
		
		if ($response) { 

			$model->db->exec("
				INSERT INTO user_tracking (
					user_tracking_user,
					user_tracking_track,
					date_created,
					user_created
				) VALUES (
					$user->id,
					'User hast unblocked IP $ip',
					NOW(),
					'$user->login'
				)	
			");
		}
		
		$message = ($response) ? $translate->message_request_deleted : $translate->message_request_failure;
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'query' => "INSERT INTO user_tracking (
					user_tracking_user,
					user_tracking_track,
					date_created,
					user_created
				) VALUES (
					$user->id,
					'Unlock IP: $ip',
					NOW(),
					$user->login
				)"
	));
	