<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$appliction = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$itemFile = new Item_File();
$itemFile->read($id);

$permission_edit = user::permission('can_edit_catalog');
$permission_edit_items = user::permission('can_edit_items_in_catalogue');
$permission_edit_files = user::permission('can_edit_files_in_catalogue');
$_CAN_EDIT = $permission_edit || $permission_edit_items || $permission_edit_files ? true : false;

if ($itemFile->id && $_CAN_EDIT) {
	
	$item = $itemFile->item;
	$file = $itemFile->path;
	
	$delete = $itemFile->delete();
	
	if ($delete) {
		file::remove($file);	
		Message::request_deleted();
		url::redirect("/$appliction/$controller/$action/$item");
	} else { 
		Message::request_failure();
		url::redirect("/$appliction/$controller/$action/$item/$id");
	}
}
else {
	Message::access_denied();
	url::redirect("/$appliction/$controller");
}