<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$appliction = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$sapCountry = new SapCountry();
$sapCountry->read($id);

if ($sapCountry->id && user::permission('can_administrate_system_data')) {
	
	$integrity = new Integrity();
	$integrity->set($id, 'sap_countries', 'system');

	if ($integrity->check()) {
	
		$action = $sapCountry->delete();
		
		if ($action) {
			
			$response['response'] = true;
			
			// message
			Message::request_deleted();
			
			$response['redirect'] = "/$appliction/$controller";
			
		} else {
			$response['message'] = Translate::instance()->message_request_failure;
		}
		
	} else {
		$response['message'] = Translate::instance()->warning_delete;
	}
	
}
else {
	$response['message'] = Translate::instance()->warning_failure_id;
}


header('Content-Type: text/json');
echo json_encode($response);