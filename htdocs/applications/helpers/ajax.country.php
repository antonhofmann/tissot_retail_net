<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$json = array();

switch ($_REQUEST['section']) {
	
	case 'country_phone_prefix':

		$db = Connector::get(Connector::DB_CORE);

		$sth = $db->prepare("SELECT country_id FROM countries WHERE country_phone_prefix = ?");
		$sth->execute(array($_REQUEST['value']));
		$result = $sth->fetch();

		$json['id'] = $result['country_id'];
		$json['success'] = $result['country_id'] ? true : false;
		
	break;

	case 'get_country_phone_prefix':

		$db = Connector::get(Connector::DB_CORE);
		$sth = $db->prepare("SELECT country_phone_prefix FROM countries WHERE country_id = ?");
		$sth->execute(array($_REQUEST['value']));
		$json = $sth->fetch();
		
	break;
}


header('Content-Type: text/json');
echo json_encode($json);
