<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$id = $_REQUEST['db_table_id'];
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	// material
	$table = new DB_Table();
	$table->read($id);

	if ($table->id && in_array('db_table_export_fields', $fields)) {
		
		// export fields
		$fields = ($_REQUEST['db_table_export_fields']) ? serialize(array_keys($_REQUEST['db_table_export_fields'])) : null;
		
		// export order
		if (!$fields) $order = null;
		else $order = ($_REQUEST['db_table_export_fields_order']) ? serialize(explode(',', $_REQUEST['db_table_export_fields_order'])) : null;
		
		$response = $table->update(array(
			'db_table_export_fields_selectable' => ($_REQUEST['db_table_export_fields_selectable']) ? 1 : null,
			'db_table_export_fields_order' => $order,
			'db_table_export_fields' => $fields,
			'user_modified' => $user->login
		));
		
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));
	