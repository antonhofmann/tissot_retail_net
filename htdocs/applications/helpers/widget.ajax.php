<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$translate = Translate::instance();
$user = User::instance();
$dashboard = $_REQUEST['dashboard'];
$widget = $_REQUEST['widget'];

$model = new Model(Connector::DB_CORE);

switch ($_REQUEST['section']) {
	
	case 'load':
	
		$file = $_SERVER['DOCUMENT_ROOT']."/applications/views/widgets/widget$widget.php";
		
		if (file_exists($file)) {
			$content = Template::load($file, array(
				'offset' => $_REQUEST['offset'],
				'limit' => $_REQUEST['limit']
			));
		}
		
	break;
	
	case 'add':
		
		if ($dashboard && $widget && $user->id) {
			
			$result = $model->query("
				SELECT 
					ui_widget_id,
					ui_widget_title, 
					ui_widget_resizable, 
					ui_widget_draggable, 
					ui_widget_deletable,
					ui_widget_width,
					ui_widget_height,
					ui_widget_auto_refresh,
					ui_widgets.ui_widget_theme_id,
					ui_widget_theme_name
				FROM ui_widgets
				LEFT JOIN ui_widget_themes ON ui_widget_themes.ui_widget_theme_id = ui_widgets.ui_widget_theme_id
				WHERE ui_widget_id = $widget	
			")->fetch();
			
			if ($result['ui_widget_id']) {
				
				$widget = $result;
				
				$widget['width'] =  ceil($widget['ui_widget_width']/50);
				$widget['height'] =  ceil($widget['ui_widget_height']/50);
				
				// get current widgets positions
				$result = $model->query("
					SELECT 
						ui_user_widget_cord_x,
						ui_user_widget_cord_y,
						ui_user_widget_width,
						ui_user_widget_height
					FROM ui_user_widgets
					WHERE ui_user_widget_dashboard_id = $dashboard AND ui_user_widget_user_id = $user->id	
				")->fetchAll();
				
				
				if ($result) {
					
					$axeX = range(1, 20);
					$axeY = range(1, 20);
					
					foreach ($result as $data) {
					
						$row = $data['ui_user_widget_cord_y'];
						$col = $data['ui_user_widget_cord_x'];
						
						$width = $col + ceil($data['ui_user_widget_width']/50) - 1;
						$height = $row + ceil($data['ui_user_widget_height']/50) - 1;

						for ($y=$row; $y <= $height; $y++) {
							for ($x=$col; $x <= $width; $x++) {
								$datagrid[$y][$x] = true;
							}
						}
					}

					// find free position for new widget
					for ($row=1; $row<=40; $row++) {
						for ($col=1; $col<=20; $col++) {
						
							if (!$datagrid[$row][$col]) {
								
								$width = $col+$widget['width']-1;
								$height = $row+$widget['height']-1;
								
								$free_cols = true;
								
								// check free column spaces
								for ($i=$col; $i<=$width; $i++) {
									if ($datagrid[$row][$i]) {
										$free_cols = false;
									}
								}
								
								$free_rows = true;
								
								// check free row spaces
								for ($i=$row; $i<=$height; $i++) {
									if ($datagrid[$i][$col]) {
										$free_rows = false;
									}
								}
								
								if ($free_rows && $free_cols &&  $width <= 20) {
									$cordX = $col;
									$cordY = $row;
									break;
								}
							}
						}
						
						if ($cordX) {
							break;
						}
					}
				} else {
					$cordX = 1;
					$cordY = 1;
				}
				
				$sth = $model->db->prepare("
					INSERT INTO ui_user_widgets (
						ui_user_widget_dashboard_id,
						ui_user_widget_user_id,
						ui_user_widget_widget_id,
						ui_user_widget_theme_id,
						ui_user_widget_cord_y,
						ui_user_widget_cord_x,
						ui_user_widget_width,
						ui_user_widget_height,
						user_created
					) VALUES (
						:dashboard,
						:user,
						:widget,
						:theme,
						:pos_x,
						:pos_y,
						:size_x,
						:size_y,
						:username
					)
				");
				
				$sth->execute(array(
					'dashboard' => $dashboard,
					'user' => $user->id,
					'widget' => $widget['ui_widget_id'],
					'theme' => $widget['ui_widget_theme_id'],
					'pos_y' => $cordX,
					'pos_x' => $cordY,
					'size_x' => $widget['ui_widget_width'],
					'size_y' => $widget['ui_widget_height'],
					'username' => $user->login
				));
				
				$response = $model->db->lastInsertId();
				
				$id = $widget['ui_widget_id'];
				
				if ($response) {
					
					$attributes = _array::rend_attributes(array(
						'id' 				=> "widget-$id",
						'class' 			=> 'gridster-widget',
						'data-id' 			=> $widget['ui_widget_id'],
						'data-resizable' 	=> $widget['ui_widget_resizable'],
						'data-draggable' 	=> $widget['ui_widget_draggable'],
						'data-refresh' 		=> $widget['ui_widget_auto_refresh']
					));
					
					$delete = $widget['ui_widget_deletable'] ? "<i class='fa fa-times-circle widget-remover' data-id='$id' ></i>" : null;
					
					$content = "
						<li $attributes >
							<div class='panel ".$widget['ui_widget_theme_name']."'>
								<div class='panel-heading'>
									<strong>".$widget['ui_widget_title']."</strong>
									<span class='pull-right panel-actions'>
										<i class='fa fa-cog widget-theme'></i>
										$delete
										<i class='fa fa-refresh widget-refresh' ></i>
									</span>
								</div>
							</div>
						</li>		
					";
					
					$data = array(
						'widget' => $widget['ui_widget_id'],
						'col' => $cordX,
						'row' => $cordY,
						'width' => $widget['width'],
						'height' => $widget['height']
					);
				}
			}
			
			$message = $response
				? $translate->message_request_inserted
				: $translate->error_request;
		} 
		else {
			
			$message = $translate->error_request;
		}
	
	break;
	
	case 'save':
		
		if ($user->id && $dashboard && $_REQUEST['widgets']) {
			
			foreach ($_REQUEST['widgets'] as $widget => $data) {
				
				$cords = json_decode($data, true);

				$pos_x = $cords['row'];
				$pos_y = $cords['col'];
				$size_x = $cords['size_x']*50;
				$size_y = $cords['size_y']*50;
				
				$response = $model->db->exec("
					UPDATE ui_user_widgets SET
						ui_user_widget_cord_y = $pos_x,
						ui_user_widget_cord_x = $pos_y,
						ui_user_widget_width = $size_x,
						ui_user_widget_height = $size_y,
						user_modified = '$user->login',
						date_modified = NOW()
					WHERE
						ui_user_widget_dashboard_id = $dashboard
						AND ui_user_widget_user_id = $user->id
						AND ui_user_widget_widget_id = $widget
				");
			}
		}
		
	break;
	
	case 'delete':
	
		if ($user->id && $_REQUEST['widget']) {
		
			$sth = $model->db->prepare("
				DELETE FROM ui_user_widgets
				WHERE ui_user_widget_user_id = :user AND ui_user_widget_widget_id = :widget
			");
		
			$response = $sth->execute(array(
				'user' => $user->id,
				'widget' => $_REQUEST['widget']
			));
		}
		
	break;
	
	case 'theme':
			
		if ($user->id && $widget && $_REQUEST['themename']) {
			
			$result = $model->query("
				SELECT ui_widget_theme_id AS theme
				FROM ui_widget_themes
				WHERE ui_widget_theme_name = '".$_REQUEST['themename']."'
			")->fetch();
			
			$sth = $model->db->prepare("
				UPDATE ui_user_widgets SET
					ui_user_widget_theme_id = :theme,
					user_modified = :username,
					date_modified = NOW()
				WHERE
					ui_user_widget_user_id = :user
					AND ui_user_widget_widget_id = :widget
			");
			
			$response = $sth->execute(array(
				'theme' => $result['theme'],
				'user' => $user->id,
				'username' => $user->login,
				'widget' => $_REQUEST['widget']
			));
		}
		
	break;
}


header('Content-Type: text/json');

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect,
	'reload' => $reload,
	'content' => $content,
	'data' => $data
));