<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$language = $_REQUEST['language'];
	$id = $_REQUEST['id'];
	
	if ($id && $language && Translation::PERMISSION_EDIT) {
		
		$model = new Model();
		
		$delete_translations = $model->db->query("
			DELETE FROM translations
			WHERE translation_keyword = $id AND translation_language = $language
		");
		
		$result = $model->query("
			SELECT translation_id
			FROM translations
			WHERE translation_keyword = $id
		")->fetchAll();
		
		if (!$result) {
			$delete_keyword = $model->db->query("
				DELETE FROM translation_keywords
				WHERE translation_keyword_id = $id			
			");
		}
		
		if ($delete_translations || $delete_keyword) {
			Message::request_deleted();
			url::redirect("/$application/$controller/$action/$language");
			
		} else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$language/$id");
		}
	}
	else {
		Message::access_denied();
		url::redirect("/$application/$controller");
	}