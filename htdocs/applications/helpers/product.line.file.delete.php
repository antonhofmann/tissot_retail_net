<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	if ($id && user::permission('can_edit_catalog')) {
		
		$productLineFile = new Product_Line_File();
		$productLineFile->read($id);
		
		$producline = $productLineFile->product_line;
		$path = $productLineFile->path;
		
		$delete = $productLineFile->delete();
		
		if ($delete) {
			file::remove($path);	
			Message::request_deleted();
			url::redirect("/$appliction/$controller/$action/$producline");
		} else { 
			Message::request_failure();
			url::redirect("/$appliction/$controller/$action/$producline/$id");
		}
	}
	else {
		Message::access_denied();
		url::redirect("/$appliction/$controller");
	}