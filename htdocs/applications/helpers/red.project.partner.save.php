<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$project = $_REQUEST['red_project_partner_project_id'];
	$partner = $_REQUEST['red_project_partner_id'];
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	$data = array();

	// address_company
	if (in_array('red_project_partner_address_id', $fields)) {
		$data['red_project_partner_address_id'] = $_REQUEST['red_project_partner_address_id'];
	}

	// red_partnertype_name
	if (in_array('red_project_partner_partnertype_id', $fields)) {
		$data['red_project_partner_partnertype_id'] = $_REQUEST['red_project_partner_partnertype_id'];
	}

	// user_name
	if (in_array('red_project_partner_user_id', $fields)) {
		$data['red_project_partner_user_id'] = $_REQUEST['red_project_partner_user_id'];
	}

	// red_project_partner_access_from
	if (in_array('red_project_partner_access_from', $fields)) {
		$data['red_project_partner_access_from'] = ($_REQUEST['red_project_partner_access_from']) ? date::sql($_REQUEST['red_project_partner_access_from']) : null;
	}

	// red_project_partner_access_until
	if (in_array('red_project_partner_access_until', $fields)) {
		$data['red_project_partner_access_until'] =  ($_REQUEST['red_project_partner_access_until']) ? date::sql($_REQUEST['red_project_partner_access_until']) : null;
	}

	if ($data && $project && $application) {
		
		// project id
		$data['red_project_partner_project_id'] = $project;
		
		$projectPartner = new Red_Project_Partner($application);
		$projectPartner->read($partner);
		
		if ($projectPartner->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $projectPartner->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $partner = $projectPartner->create($data);
			$message = ($response) ? $translate->message_request_inserted : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$partner" : null;
		}
	}

	echo json_encode(array(
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message
	));
