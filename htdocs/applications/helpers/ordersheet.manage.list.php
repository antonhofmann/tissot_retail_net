<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// request vars
	$application = $_REQUEST['application'];
	$year = $_REQUEST['mps_mastersheet_year'];
	$mastersheet = $_REQUEST['mps_mastersheet_id'];
	$year = $_REQUEST['mps_mastersheet_year'];
	$selected_companies = $_REQUEST['selected_companies'];
	$selected_items = $_REQUEST['selected_items'];
	$selected_tab = $_REQUEST['selected_tab'];
	$search = $_REQUEST['search'];
	$country = $_REQUEST['countries'];
	$counter = 0;
	
	// db connector
	$model = new Model($application);
	switch ($selected_tab) {
		
		case 'new_ordersheets':
			
			// selected companies
			$selected_companies = ($selected_companies) ? explode(',', $selected_companies) : array();
			
			// filter: active companies
			$filters['active'] = "address_active = 1";
			
			// filter: planning
			$filters['planning'] = "address_involved_in_planning = 1";
			
			// filter: full text search
			if ($search && $search <> $translate->search) {
				$filters['search'] = "(
					address_company LIKE '%$search%'
					OR country_name LIKE '%$search%'
				)";
			}
			
			// filer: countries
			if ($_REQUEST['countries']) {
				$filters['countries'] = "country_id = ".$_REQUEST['countries'];
			}
			
			// filter: exclude existing companies
			if ($mastersheet) {
				
				$ordersheets = $model->query("
					SELECT mps_ordersheet_id, mps_ordersheet_address_id 
					FROM mps_ordersheets
					WHERE mps_ordersheet_mastersheet_id = $mastersheet
				")->fetchAll();
					
				if ($ordersheets) {
					
					foreach ($ordersheets as $row) {
						$existingCompanies[] = $row['mps_ordersheet_address_id'];
					}
						
					$filters['excluded'] = "address_id NOT IN (".join(',', $existingCompanies).") ";
				}
			}
			
			// datagrid
			$result = $model->query("
				SELECT DISTINCT	
					address_id, 
					address_company, 
					country_name 
				FROM db_retailnet.addresses
			")
			->bind(Company::DB_BIND_COUNTRIES)
			->filter($filters)
			->order('country_name')
			->order('address_company')
			->fetchAll();
			
			if ($result) {
				
				$datagrid = _array::datagrid($result);
			
				foreach ($datagrid as $key => $row) {
					$checked = (in_array($key, $selected_companies)) ? 'checked=checked' : null;
					$datagrid[$key]['companies'] = "<input type=checkbox name=companies[$key] value=$key class='checkbox' $checked />";
					if ($checked) $counter++;
				}
				
				$checkedAll = (count($datagrid) == $counter) ? 'checked=checked' : null;
			}
			
			
			$table = new Table();
			$table->datagrid = $datagrid;
			
			$table->caption('companies', "<input type=checkbox class='checkall' $checkedAll />");
			
			$table->country_name(
				Table::ATTRIBUTE_NOWRAP,
				'width=30%'
			);
			
			$table->address_company(
				Table::ATTRIBUTE_NOWRAP
			);
			
			$table->companies(
				Table::ATTRIBUTE_NOWRAP,
				'width=20px'
			);

			$table = $table->render();
			
			// toolbox: serach full text
			$toolbox[] = ui::searchbox();
			
			// toolbox: countries
			$result = $model
			->query("SELECT DISTINCT country_id, country_name FROM db_retailnet.addresses")
			->bind(Company::DB_BIND_COUNTRIES)
			->filter($filters)
			->exclude('countries')
			->order('country_name')
			->fetchAll();
			
			$toolbox[] = ui::dropdown($result, array(
				'name' => 'countries',
				'id' => 'countries',
				'class' => 'submit',
				'value' => $_REQUEST['countries'],
				'caption' => $translate->all_countries
			));
		
		break;
		
		case 'existing_ordersheets' :
			
			// selected companies
			$selected_companies = ($selected_companies) ? explode(',', $selected_companies) : array();
			
			// filter: active companies
			$filters['active'] = "address_active = 1";
				
			// filter: planning
			$filters['planning'] = "address_involved_in_planning = 1";
		
			// filter: full text search
			if ($search && $search <> $translate->search) {
			
				$filters['search'] = "(
					address_company LIKE '%$search%'
					OR country_name LIKE '%$search%'
					OR mps_workflow_state_name LIKE '%$search%'
					OR mps_ordersheet_openingdate LIKE '%$search%'
					OR mps_ordersheet_closingdate LIKE '%$search%'
				)";
			}
			
			// filer: countries
			if ($_REQUEST['countries']) {
				$filters['countries'] = "country_id = ".$_REQUEST['countries'];
			}
			
			// filter: workflow state
			$filters['workflowstates'] = "mps_ordersheet_workflowstate_id IN (1,2,3,4,7,8)";
			
			// filter: mastersheet
			$filters['mastersheet'] = "mps_ordersheet_mastersheet_id = $mastersheet";
			
			// datagrid
			$result = $model->query("
				SELECT DISTINCT
					mps_ordersheet_id, 
					mps_ordersheet_workflowstate_id, 
					mps_workflow_state_name, 
					DATE_FORMAT(mps_ordersheet_openingdate,'%d.%m.%Y') AS mps_ordersheet_openingdate,
					address_company, 
					country_name
				FROM mps_ordersheets
			")
			->bind(Ordersheet::DB_BIND_COMPANIES)
			->bind(Company::DB_BIND_COUNTRIES)
			->bind(Ordersheet::DB_BIND_WORKFLOW_STATES)
			->filter($filters)
			->order('country_name')
			->order('address_company')
			->order('mps_ordersheet_openingdate')
			->fetchAll();
			
			if ($result) {
			
				$datagrid = _array::datagrid($result);
					
				foreach ($datagrid as $key => $row) {
					
					$checked = (in_array($key, $selected_companies)) ? 'checked=checked' : null;
					$datagrid[$key]['companies'] = "<input type=checkbox name=companies[$key] value=$key class='checkbox' $checked />";
					
					$ordersheet_items = $model->query("
						SELECT COUNT(mps_ordersheet_item_id) as total
						FROM mps_ordersheet_items 
						WHERE mps_ordersheet_item_ordersheet_id = $key		
					")->fetch();
					
					$datagrid[$key]['items'] = $ordersheet_items['total'];
					
					if ($checked) $counter++;
				}
				
				$checkedAll = (count($datagrid) == $counter) ? 'checked=checked' : null;
			}
			
			
			$table = new Table();
			$table->datagrid = $datagrid;
			
			$table->caption('companies', "<input type=checkbox class='checkall' $checkedAll />");
			
			$table->country_name(
				Table::ATTRIBUTE_NOWRAP,
				'width=10%'
			);
			
			$table->address_company();
			
			$table->mps_workflow_state_name(
				Table::ATTRIBUTE_NOWRAP,
				'width=5%'
			);
			
			$table->mps_ordersheet_openingdate(
				Table::ATTRIBUTE_NOWRAP,
				'width=5%'
			);
			
			$table->items(
				Table::ATTRIBUTE_NOWRAP,
				Table::ATTRIBUTE_ALIGN_CENTER,
				'width=5%'
			);
			
			$table->companies(
				Table::ATTRIBUTE_NOWRAP,
				'width=20px'
			);
			
			$table = $table->render();
			
			
			// toolbox: serach full text
			$toolbox[] = ui::searchbox();
			
			// toolbox: countries
			$result = $model
			->query("SELECT DISTINCT country_id, country_name FROM mps_ordersheets")
			->bind(Ordersheet::DB_BIND_COMPANIES)
			->bind(Company::DB_BIND_COUNTRIES)
			->bind(Ordersheet::DB_BIND_WORKFLOW_STATES)
			->filter($filters)
			->exclude('countries')
			->order('country_name')
			->fetchAll();
			
			$toolbox[] = ui::dropdown($result, array(
				'name' => 'countries',
				'id' => 'countries',
				'class' => 'submit',
				'value' => $_REQUEST['countries'],
				'caption' => $translate->all_countries
			));
			
		break;
		
		case 'ordersheet_items':
			
			// selected companies
			$selected_items = ($selected_items) ? explode(',', $selected_items) : array();
			
			// master sheet items
			$result = $model->query("
				SELECT DISTINCT
					mps_material_id,
					mps_material_code,
					mps_material_name,
					mps_material_planning_type_id,
					mps_material_planning_type_name,
					mps_material_collection_category_id,
					mps_material_collection_category_code,
					mps_material_collection_code
				FROM mps_mastersheet_items
			")
			->bind(Mastersheet_Item::DB_BIND_MATERIALS)
			->bind(Material::DB_BIND_PLANNING_TYPES)
			->bind(Material::DB_BIND_COLLECTIONS)
			->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
			->filter('mastersheet', "mps_mastersheet_item_mastersheet_id = $mastersheet")
			->filter('nullplanning', 'mps_material_material_planning_type_id > 0')
			->filter('nullcollection', 'mps_material_collection_category_id > 0')
			->order('mps_material_planning_type_name')
			->order('mps_material_collection_category_code')
			->order('mps_material_code')
			->order('mps_material_name')
			->fetchAll();

			$mastersheet_items = ($result) ? _array::datagrid($result) : array();
			
			// get existing items form selected companies
			$result = $model->query("
				SELECT DISTINCT
					mps_material_id,
					mps_material_code,
					mps_material_name,
					mps_material_planning_type_id,
					mps_material_planning_type_name,
					mps_material_collection_category_id,
					mps_material_collection_category_code,
					mps_material_collection_code
				FROM mps_ordersheet_items 	
			")
			->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
			->bind(Ordersheet_Item::DB_BIND_MATERIALS)
			->bind(Material::DB_BIND_PLANNING_TYPES)
			->bind(Material::DB_BIND_COLLECTIONS)
			->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
			->filter('ordersheet', "mps_ordersheet_item_ordersheet_id IN ($selected_companies)")
			->order('mps_material_planning_type_name')
			->order('mps_material_collection_category_code')
			->order('mps_material_code')
			->order('mps_material_name')
			->fetchAll();
			
			$ordersheet_items = ($result) ? _array::datagrid($result) : array();
			
			$items = $mastersheet_items + $ordersheet_items;
			
			if ($items) {

				foreach ($items as $item => $row) {
					
					$planning = $row['mps_material_planning_type_id'];
					$collection = $row['mps_material_collection_category_id'];
					
					//proposed textbox
					$textbox = "<input type='text' class='textbox'  name='proposed[$item]' index='$item' />";
					
					// item visibility
					$checked = ($ordersheet_items[$item]) ? 'checked=checked' : null;
					$checkbox = "<input type='checkbox' class='checkbox' name='item[$item]' value = '$item' $checked />";
					
					$datagrid[$planning]['caption'] = $row['mps_material_planning_type_name'];
					$datagrid[$planning]['collections'][$collection]['caption'] = $row['mps_material_collection_category_code'];
					$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_code'] = $row['mps_material_code'];
					$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_name'] = $row['mps_material_name'];
					$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_collection_code'] = $row['mps_material_collection_code'];
					$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_item_quantity_proposed'] = $textbox;
					$datagrid[$planning]['collections'][$collection]['items'][$item]['item'] = $checkbox;
					
					if ($checked) {
						$checkedCollectionx[$collection] = $checkedCollectionx[$collection] + 1;
					}
				}
				
			}
			
			if ($datagrid) { 
					
				foreach ($datagrid as $planning => $data) {
						
					$list  .= "<h5>".$data['caption']."</h5>";
			
					foreach ($data['collections'] as $collection => $items) {
			
						$list  .= "<h6>".$items['caption']."</h6>";
						$datagrid = $collectionSet['items'];
			
						$table = new Table();
						$table->datagrid = $items['items'];
						
						$checked = (count($table->datagrid) == $checkedCollectionx[$collection]) ? 'checked=checked' : null;
						$table->caption('item', "<input type=checkbox class='checkall' $checked />");
						
						$table->mps_material_collection_code(
							Table::ATTRIBUTE_NOWRAP, 
							'width=20%'
						);
						
						$table->mps_material_code(
							Table::ATTRIBUTE_NOWRAP, 
							'width=20%'
						);
						
						$table->mps_material_name();
						
						$table->mps_ordersheet_item_quantity_proposed(
							Table::ATTRIBUTE_NOWRAP, 
							Table::ATTRIBUTE_ALIGN_RIGHT, 
							'width=5%'
						);
						
						$table->item(
							Table::ATTRIBUTE_NOWRAP, 
							'width=20px'
						);
						
						$list .= $table->render();
					}
				}
				
				$table = $list;
			}
			else  $table = html::emptybox($translate->empty_result);
		
		break;
	}
	
	// toolbox: form
	if ($toolbox) {
		$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
	}
	
	echo $toolbox.$table;
	
	