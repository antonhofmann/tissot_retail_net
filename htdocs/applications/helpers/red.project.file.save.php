<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$project = $_REQUEST['red_file_project_id'];
	$file = $_REQUEST['red_file_id'];
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	$upload_path = Red_Project::PROJECT_FILE_UPLOAD_PATH.$project."/files";

	$data = array();

	if (in_array('red_file_project_id', $fields)) {
		$data['red_file_project_id'] = $_REQUEST['red_file_project_id'];
	}

	if (in_array('red_file_category_id', $fields)) {
		$data['red_file_category_id'] = $_REQUEST['red_file_category_id'];
	}

	if (in_array('red_file_owner_user_id', $fields)) {
		$data['red_file_owner_user_id'] = $_REQUEST['red_file_owner_user_id'];
	}

	if (in_array('red_file_title', $fields)) {
		$data['red_file_title'] = $_REQUEST['red_file_title'];
	}

	if (in_array('red_file_description', $fields)) {
		$data['red_file_description'] = $_REQUEST['red_file_description'];
	}

	if ($_REQUEST['has_upload'] && $_REQUEST['red_file_path']) {
		$path = upload::move($_REQUEST['red_file_path'], $upload_path);
		$data['red_file_path'] = $path;
	}


	if ($data) {
		
		$projectFile = new Red_File($application);
		$projectFile->read($file);
		
		if ($projectFile->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $projectFile->update($data);false;
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $file = $projectFile->create($data);
			$message = ($response) ? message::request_inserted() : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$file" : null;
		}
		
		if ($response) {
				
			// set file accesses
			$fileAccess = new Red_Project_File_Access($application);
			$fileAccess->file($file);
			
			// delete all existing accesses
			$fileAccess->deleteAll();
			
			if ($_REQUEST['partners']) {
				foreach ($_REQUEST['partners'] as $partner) {
					$fileAccess->create($partner);
				}
			}
			
			// IMPORTANT
			// for file inserting from comment form
			// return iserted id
			if ($_REQUEST['return_inserted_id']) {
				$inserted = $file;
			}
		}
	}


	echo json_encode(array(
		'id' => $inserted,
		'path' => $path,
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message
	));
