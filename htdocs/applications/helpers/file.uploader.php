<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

/*
 * jQuery File Upload Plugin PHP Example 5.14
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

// Turn off all error reporting
//error_reporting(0);

$connector = Connector::instance();
	

$user = User::instance()->id ?: 'fileuploader';


$path = $_REQUEST['path'] ?: "/public/data/tmp/$user/";
$options['path'] = $path;
$options['upload_dir'] = $_SERVER['DOCUMENT_ROOT'].$path;
$options['upload_url'] = '//'.$_SERVER['SERVER_NAME'].$path;
$upload_handler = new UploadHandler($options);
