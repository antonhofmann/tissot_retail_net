<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$id = $_REQUEST['stocklist'];
	$tag = $_REQUEST['tag'];
	$year = $_REQUEST['year'];
	$value = (is_numeric($_REQUEST['value'])) ? $_REQUEST['value'] : null;
	
	
	if ($id && $tag && $year) {
		
		$tags = explode('-', $tag);
		$item = $tags[0];
		$week = $tags[1];
		$request_column_name = $tags[3];
		
		//model
		$model = new Model(Connector::DB_CORE);
		
		// stock list
		$stocklist = new Stocklist();
		$stocklist->read($id);
		
		// stock list additional columns
		$stocklist_additional_columns = unserialize($stocklist->columns);
		
		// stocklist week modul
		$stocklist_week = new Stocklist_Week();
		
		// get week id
		$stock_week_data = $model
		->query("SELECT scpps_week_id FROM scpps_weeks")
		->filter('stocklist', "scpps_week_stocklist_id = $id")
		->filter('item', "scpps_week_item = $item")
		->filter('week', "scpps_week_week = $week")
		->filter('year', "scpps_week_year = $year")
		->fetch();
		
		// load stock week data
		$stocklist_week->read($stock_week_data['scpps_week_id']);
		
		// week additional columns
		$week_additional_columns = unserialize($stocklist_week->additional_columns);
		
		// week permitted columns
		$columns = array(
			'scpps_week_stock',
			'scpps_week_not_confirmed',
			'scpps_week_not_confirmed_yet',
			'scpps_week_available_after_deduction'
		);
		
		// add additional columns
		if (is_array($stocklist_additional_columns)) {
			
			$additional_data = array();
			
			foreach ($stocklist_additional_columns as $additional_column => $additional_caption) { 
				
				// add additional column to permitted columns
				array_push($columns, $additional_column);
				
				if ($additional_column == $request_column_name) {
					$is_additional = true;
					$additional_value = $value;
				} else {
					$additional_value = $week_additional_columns[$additional_column];
				}
				
				// build additional array
				$additional_data[$additional_column] = $additional_value;
			}
		}
		
		// if request column name is permited
		if (in_array($request_column_name, $columns)) {
			
			// serialize additional values
			if ($is_additional) {
				$column_name = 'scpps_week_additional_columns';
				$value = serialize($additional_data);
			}
			else {
				$column_name = $request_column_name;
			}
			
			// check additional data is empty
			if ($additional_data) {
				
				$empty_columns = 0;
			
				foreach ($additional_data as $additional_column => $additional_value) {
					if (!$additional_value) $empty_columns++;
				}
				
				if (count($additional_data) == $empty_columns) {
					$is_empty_additionals = true;
				}
				
			} else {
				$is_empty_additionals = true;
			}
			
			
			if ($stocklist_week->id) { 
	
				$stocklist_week->update(array(
					$column_name => $value,
					'user_modified' => $user->login,
					'date_modified' => date('Y-m-d H:i:s')
				));
				
				// remove empty records
				if (!$stocklist_week->stock && !$stocklist_week->not_confirmed && !$stocklist_week->not_confirmed_yet && !$stocklist_week->available_after_deduction && $is_empty_additionals) {
					$stocklist_week->delete();
				}			
			}
			elseif ($value) {
				
				// get supplier price
				$price = $model->query("
					SELECT 
						supplier_item_price, 
						supplier_item_currency, 
						currency_exchange_rate, 
						currency_factor	
					FROM suppliers 
				")
				->bind(Supplier::DB_BIND_CURRENCIES)
				->filter('item', "supplier_item = $item")
				->fetch();
				
				$stocklist_week->create(array(
					'scpps_week_stocklist_id' => $id,
					'scpps_week_item' => $item,
					'scpps_week_year' => $year,
					'scpps_week_week' => $week,
					$column_name => $value,
					'scpps_week_item_supplier_price' => $price['supplier_item_price'],
					'scpps_week_currency' => $price['supplier_item_currency'],
					'scpps_week_currency_exchange_rate' => $price['currency_exchange_rate'],
					'scpps_week_currency_factor' => $price['currency_factor'],
					'user_created' => $user->login,
					'date_created' => date('Y-m-d H:i:s')
				));
				
				echo $item."-".$week."-".$scpps_week_id."-".$column_name;
			}
		}
	}
	