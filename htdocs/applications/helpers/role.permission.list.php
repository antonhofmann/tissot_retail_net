<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$model = new Model();
	
	// framework application
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$role = $_REQUEST['id'];

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'permission_description';
	
	// sor direction
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// sql pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	
	// sql offset
	$offset = ($page-1) * $settings->limit_pager_rows;
		
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		$keyword = $_REQUEST['search'];
		$filters['search'] = "(
			permission_description LIKE \"%$keyword%\"
			OR permission_name LIKE \"%$keyword%\"
			OR permission_application LIKE \"%$keyword%\"
		)";
	}
	
	// filter: applications
	$_REQUEST['applications'] = (isset($_REQUEST['applications'])) ? $_REQUEST['applications'] : 'mps';
	
	if ($_REQUEST['applications']) {
		$filters['applications'] = "permission_application = '".$_REQUEST['applications']."'";
	}
	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			permission_id,
			permission_description
		FROM permissions
	")
	->filter($filters)
	->order($sort, $direction)
	->fetchAll();

	if ($result) {
	
		$counter = 0;
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		
		foreach ($datagrid as $key => $row) {
			
			$check = $model->query("
				SELECT *
				FROM role_permissions
				WHERE role_permission_role = $role AND role_permission_permission = $key
			")->fetch();
			
			$checked = ($check) ? "checked=checked" : null;
			$datagrid[$key]['checkboxs'] = "<input type='checkbox' class='permission_box' name='permission[$key]' value='$key' $checked />";
			
			if ($checked) {
				$counter++;
			}
		}
	
		$list_index = $index = $translate->page_index(array(
			'page' => 1,
			'pages' => 1,
			'totalrows' => $totalrows
		));
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	// toolbox: applications
	$result = $model->query("
		SELECT DISTINCT
			permission_application,
			UPPER(permission_application) AS name
		FROM permissions
	")
	->filter($filters)
	->exclude('applications')
	->order('permission_application')
	->fetchAll();
	
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'applications',
		'id' => 'applications',
		'class' => 'submit',
		'value' => $_REQUEST['applications'],
		'caption' => $translate->all_applications
	));
	
	// toolbox: form
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	
	$table->permission_description(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);
	
	$table->checkboxs(
		Table::ATTRIBUTE_NOWRAP,
		'width=20px'
	);
	
	$table->footer($list_index);
	$table = $table->render();
	
	echo $toolbox.$table;
	
	