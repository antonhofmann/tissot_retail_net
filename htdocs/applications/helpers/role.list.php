<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$model = new Model();

// framework application
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$link = $_REQUEST['form'];

$keyword = ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) ? $_REQUEST['search'] : null;

// request
$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

	
// filter: full text search
if ($keyword) {
	$filters['search'] = "(
		role_code LIKE \"%$keyword%\" 
		OR role_name LIKE \"%$keyword%\"
		OR role_application LIKE \"%$keyword%\"
		OR user_firstname LIKE \"%$keyword%\"
		OR user_name LIKE \"%$keyword%\"
	)";
}

$filter = $filters ? 'WHERE '.join(' AND ', array_values($filters)) : null;

// datagrid
$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		role_id, 
		role_application,
		role_code, 
		role_name,
		IF(role_responsible_user_id > 0, CONCAT(user_firstname, ' ', user_name) , '') AS user,
		IF(role_active = 1, 'yes', 'no') as status
	FROM roles
	LEFT JOIN users ON user_id = role_responsible_user_id
	$filter
	ORDER BY role_application, role_code, role_name
")->fetchAll();

$_DATAGRID = array();

if ($result) {

	$totalrows = $model->totalRows();
	
	foreach ($result as $row) {
		$id = $row['role_id'];
		$app = $row['role_application'];
		$_DATAGRID[$app][$id]['role_code'] = $row['role_code'];
		$_DATAGRID[$app][$id]['role_name'] = $row['role_name'];
		$_DATAGRID[$app][$id]['user'] = $row['user'];
		$_DATAGRID[$app][$id]['status'] = $row['status'];
	}

}

// toolbox: add
if ($_REQUEST['add']) {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();

// toolbox: form
if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

if ($_DATAGRID) {

	foreach ($_DATAGRID as $app => $roles) {

		$list .= "<h4>".strtoupper($app)."</h4>";

		$list .= "<table class='listing over-effects' >";

		$list .= "<tr>";
		$list .= "<th width='20%' >Code</th>";
		$list .= "<th>Name</th>";
		$list .= "<th width='30%' >Responsible</th>";
		$list .= "<th width='5%' >Active</th>";
		$list .= "</tr>";
		$list .= "<tbody>";

		foreach ($roles as $id => $role) {
			$list .= "<tr>";
			$list .= "<td>{$role[role_code]}</td>";
			$list .= "<td><a href='$link/$id' >{$role[role_name]}</a></td>";
			$list .= "<td>{$role[user]}</td>";
			$list .= "<td>{$role[status]}</td>";
			$list .= "</tr>";
		}

		$list .= "</tbody>";
		$list .= "</table>";
	}
}

echo $toolbox.$list;
	
	