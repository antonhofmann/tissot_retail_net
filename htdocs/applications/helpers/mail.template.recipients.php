<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$id = $_REQUEST['template'];
	
	if ($id) { 
		
		$model = new Model(Connector::DB_CORE);
		
		// remove all recipients
		$response = $model->db->exec("
			DELETE FROM mail_template_recipients	
			WHERE mail_template_recipient_template_id = $id
		");
		
		$recipients = array();
		
		if ($_REQUEST['standard']) {
			$recipients = array_keys($_REQUEST['standard']);
		}
		
		if ($_REQUEST['cc']) {
			$recipients = array_merge($recipients, array_keys($_REQUEST['cc']));
		}

		if (is_array($recipients)) {
			
			$recipients = array_unique($recipients);
			
			$sth = $model->db->prepare("
				INSERT INTO mail_template_recipients (
					mail_template_recipient_template_id,
					mail_template_recipient_user_id,
					mail_template_recipient_cc,
					user_created
				) VALUES (
					:template, :user, :cc, :created
				)
			");
			
			$user = User::instance()->login;
			
			foreach ($recipients as $recipient) {
				
				$response = $sth->execute(array(
					'template' => $id,
					'user' => $recipient,
					'cc' => isset($_REQUEST['cc'][$recipient]) ? 1 : 0,
					'created' => $user
				));
			}
		}

		$message = ($response) ? Translate::instance()->message_request_saved : Translate::instance()->message_request_failure;
	}
	else {
		$response = false;
		$message = Translate::instance()->message_request_failure;
	}
	
	// responding
	header('Content-Type: text/json');
	echo json_encode(array(
		'response' => $response,
		'notification' => array(
			'content' => $message,
			'properties' => array(
				'theme' => $response ? 'message' : 'error'
			)
		)
	));
	