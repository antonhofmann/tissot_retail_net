<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	if ($id && $user->permission(Material_Collection_Category::PERMISSION_EDIT) ) {
		
		$collection_category = new Material_Collection_Category($application);
		$collection_category->read($id);
		$delete = $collection_category->delete();
		
		if ($delete) {
			
			$model = new Model($application);
			$result = $model->query("
				SELECT 
					mps_material_collection_category_file_id,
					mps_material_collection_category_file_path
				FROM mps_material_collection_category_files
			")
			->filter('collection_category', "mps_material_collection_category_file_collection_id = $id")
			->fetchAll();
			
			if ($result) {
				
				$collection_category_file = new Material_Collection_Category_File($application);
				
				foreach ($result as $row) {
					
					list($file_id, $file_path) = array_values($row);
					
					$collection_category_file->id = $file_id;
					$collection_category_file->delete();
					
					file::remove($file_path);
				}
			}
			
			
			Message::request_deleted();
			url::redirect("/$application/$controller");
			
		} else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$id");
		}
	}
	else {
		Message::access_denied();
		url::redirect("/$application/$controller");
	}