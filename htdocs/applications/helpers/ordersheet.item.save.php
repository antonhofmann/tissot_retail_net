<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$redirect = $_REQUEST['redirect'];
	$id = $_REQUEST['id'];
	$state = $_REQUEST['state'];
	
	// order sheet
	$ordersheet = new Ordersheet($application);
	$ordersheet->read($id);
	
	// add stock reserve warehouse if not exist
	$ordersheet->warehouse()->addStockReserve();
	
	// order sheet items
	$items = $ordersheet->item()->load();

	if ($items) { 
		
		// workflow state
		$workflowState = new Workflow_State($application);
		
		// tracker
		$tracker = new User_Tracking($application);
		
		// block states
		$statement_on_approving = $ordersheet->state()->onApproving();
		
		// quantities
		$proposedQuantities = $_REQUEST['mps_ordersheet_item_quantity_proposed'];
		$clientQuantities = $_REQUEST['mps_ordersheet_item_quantity'];
		$approvedQuantities = $_REQUEST['mps_ordersheet_item_quantity_approved'];
		
		// revision
		$itemsInRevision = $_POST['mps_ordersheet_item_status'];
		
		
		// set ordersheet in progress when order has quantites
		if ($ordersheet->workflowstate_id==Workflow_State::STATE_OPEN) {
			
			$hasQuantitiy = false;
			
			$quantities = array_merge(
				$clientQuantities ?: array(), 
				$approvedQuantities ?: array()
			);
			
			foreach ($quantities as $key => $quantity) {
				if ($quantity) {
					$hasQuantitiy = true;
					break;
				}
			}
			
			if ($hasQuantitiy) {
				
				$workflowState->read(Workflow_State::STATE_PROGRESS);
				
				// set state in progress
				$inProgress = $ordersheet->update(array(
					'mps_ordersheet_workflowstate_id' => $workflowState->id,
					'user_modified' => $user->login,
					'date_modified' => date('Y-m-d H:i:s')
				));
				
				// trackering
				if ($inProgress) {
					$tracker->create(array(
						'user_tracking_entity' => 'order sheet',
						'user_tracking_entity_id' => $id,
						'user_tracking_user_id' => $user->id,
						'user_tracking_action' => $workflowState->name,
						'user_created' => $user->login,
						'date_created' => date('Y-m-d H:i:s')
					));
				}
			}
		}

		
		// change order sheet state
		// track eache change
		if ($state) {
			
			$workflowState->read($state);
			
			// change workflow state
			$stateChange = $ordersheet->update(array(
				'mps_ordersheet_workflowstate_id' => $workflowState->id,
				'user_modified' => $user->login,
				'date_modified' => date('Y-m-d H:i:s')
			));
			
			// user tracking
			if ($stateChange) {
				$tracker->create(array(
					'user_tracking_entity' => 'order sheet',
					'user_tracking_entity_id' => $id,
					'user_tracking_user_id' => $user->id,
					'user_tracking_action' => $workflowState->name,
					'user_created' => $user->login,
					'date_created' => date('Y-m-d H:i:s')
				));
			}
		}

		
		// order sheet items
		foreach ($items as $key => $item) {

			$data = array();

			// item in revision
			$data['mps_ordersheet_item_status'] = ($itemsInRevision[$key] && $state==Workflow_State::STATE_REVISION) ? 1 : null;

			// proposed quantity
			if (isset($proposedQuantities[$key])) {
				$data['mps_ordersheet_item_quantity_proposed'] = ($proposedQuantities[$key] <> null) ? $proposedQuantities[$key] : null;
			}
			
			// owner quantity
			if (isset($clientQuantities[$key])) {
				
				$data['mps_ordersheet_item_quantity'] = ($clientQuantities[$key] <> null) ? $clientQuantities[$key] : null;
				
				if ($statement_on_approving) {
					
					$ordersheet->item()->read($key);
					
					if ($clientQuantities[$key] > 0) { 
						$totalPlanned = $ordersheet->item()->planned()->sumQuantities();
						$stockReserve = $clientQuantities[$key] - $totalPlanned;
						$stockReserve = ($stockReserve > 0) ? $stockReserve : null;
						$ordersheet->item()->planned()->setStockReserve($stockReserve);
					} else {
						$ordersheet->item()->planned()->setStockReserve(null);
					}
				}
			}

			// approved quantity
			if ($ordersheet->state()->canAdministrate() && $state==Workflow_State::STATE_APPROVED) {
					
				if ($approvedQuantities[$key] <> null) $approved = $approvedQuantities[$key];
				elseif ($clientQuantities[$key] <> null) $approved = $clientQuantities[$key];
				elseif ($proposedQuantities[$key] <> null) $approved = $proposedQuantities[$key];
				else $approved = 0;

				$data['mps_ordersheet_item_quantity_approved'] = $approved;
				$data['mps_ordersheet_item_status'] = 0;
			}
			elseif(isset($approvedQuantities[$key])) {
				$data['mps_ordersheet_item_quantity_approved'] = ($approvedQuantities[$key] <> null) ? $approvedQuantities[$key] : null;
			}
			

			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');

			// update order sheet items
			$ordersheet->item()->read($key);
			$ordersheet->item()->update($data);
		}
		
		Message::request_saved();
	}
	else {
		
		Message::request_failure();
	}
	
	url::redirect($redirect);
