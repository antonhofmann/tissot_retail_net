<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", false);
	
	$model = new Model($application);
	
	// mastersheet
	$mastersheet = new Mastersheet($application);
	$mastersheet->read($id);

	// get collection categories from master sheet items
	$result = $model->query("
		SELECT DISTINCT
			mps_material_material_collection_category_id,
			mps_material_material_planning_type_id
		FROM mps_mastersheet_items
	")
	->bind(Mastersheet_Item::DB_BIND_MATERIALS)
	->filter('mastersheet', "mps_mastersheet_item_mastersheet_id = $id")
	->fetchAll();
	
	if ($result) {
		
		$datagrid = array();
		$current_items = array();
		$delivery_dates = array();
		
		// current master sheet items
		$mastersheet_items = $mastersheet->item()->load();
		
		if ($mastersheet_items) {
			foreach ($mastersheet_items as $key => $row) {
				$current_items[] = $row['mps_mastersheet_item_material_id'];
			}
		}
		
		
		// current master sheet delivery dates
		$mastersheet_delivery_dates = $mastersheet->deliveryDate()->load();
		
		if ($mastersheet_delivery_dates) {
			foreach ($mastersheet_delivery_dates as $key => $date) {
				$planning = $date['mps_mastersheet_delivery_date_material_planning_type_id'];
				$collection = $date['mps_mastersheet_delivery_date_material_collection_category_id'];
				$delivery_dates[$planning][$collection] = $date['mps_mastersheet_delivery_date_date'];
			}
		}

		foreach ($result as $row) {
			
			$items = $model->query("
				SELECT DISTINCT
					mps_material_id,
					mps_material_code,
					mps_material_name,
					mps_material_price,
					mps_material_setof,
					mps_material_hsc,
					mps_material_planning_type_id,
					mps_material_planning_type_name,
					mps_material_collection_id,
					mps_material_collection_code,
					mps_material_collection_category_id,
					mps_material_collection_category_code,
					currency_symbol
				FROM mps_materials
			")
			->bind(Material::DB_BIND_PLANNING_TYPES)
			->bind(Material::DB_BIND_COLLECTIONS)
			->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
			->bind(Material::DB_BIND_CURRENCIES)
			->filter('planning', "mps_material_material_planning_type_id = ".$row['mps_material_material_planning_type_id'])
			->filter('collection', "mps_material_collection_category_id = ".$row['mps_material_material_collection_category_id'])
			->filter('nullplanning', 'mps_material_material_planning_type_id > 0')
			->filter('nullcollection', 'mps_material_collection_category_id > 0')
			->order('mps_material_planning_type_name')
			->order('mps_material_collection_category_code')
			->order('mps_material_code')
			->order('mps_material_name')
			->fetchAll();
			
			if ($items) {
				
				foreach ($items as $item) {
					
					$planning = $item['mps_material_planning_type_id'];
					$collection = $item['mps_material_collection_category_id'];
					$material = $item['mps_material_id'];	
					
					$datagrid[$planning]['caption'] = $item['mps_material_planning_type_name'];
					$datagrid[$planning]['collections'][$collection]['caption'] = $item['mps_material_collection_category_code'];
					$datagrid[$planning]['collections'][$collection]['items'][$material]['mps_material_hsc'] = $item['mps_material_hsc'];
					$datagrid[$planning]['collections'][$collection]['items'][$material]['mps_material_collection_code'] = $item['mps_material_collection_code'];
					$datagrid[$planning]['collections'][$collection]['items'][$material]['mps_material_code'] = $item['mps_material_code'];
					$datagrid[$planning]['collections'][$collection]['items'][$material]['mps_material_name'] = $item['mps_material_name'];
					$datagrid[$planning]['collections'][$collection]['items'][$material]['mps_material_price'] = $item['mps_material_price'];
					$datagrid[$planning]['collections'][$collection]['items'][$material]['currency_symbol'] = $item['currency_symbol'];
					$datagrid[$planning]['collections'][$collection]['items'][$material]['set_of'] = $item['mps_material_setof'];
					
					if ($_REQUEST['editabled']) {
						
						// checkboxes
						$checked = (in_array($material, $current_items)) ? 'checked=checked' : null;
						$box = "<input type=checkbox name=material[$material] class=material value=$material $checked />";
						$datagrid[$planning]['collections'][$collection]['items'][$material]['box'] = $box;
						if ($checked) $checkall[$planning][$collection] = $checkall[$planning][$collection]+1;
					} 
					elseif (!$archived) {
						$box = (in_array($material, $current_items)) ? ui::icon('checked') : null;
						$datagrid[$planning]['collections'][$collection]['items'][$material]['box'] = $box;
					}
				}
			}
		}
	}
	
	if ($datagrid) {

		foreach ($datagrid as $planning => $data) {
				
			$list  .= "<h5>".$data['caption']."</h5>";
	
			foreach ($data['collections'] as $collection => $items) {
				
				$collection_delivery_date = $delivery_dates[$planning][$collection];
				
				if ($_REQUEST['editabled']) {
					
					// checkbox: check all
					$checked = ($checkall[$planning][$collection] == count($items['items'])) ? 'checked=checked' : null;
					$caption = "<input type=checkbox name=checkall[$collection] data=table_$planning$collection class=checkall value=1 $checked />";;
					
					// input: delivery date
					
					$delivery = "<span class=delivery >
						Delivery Date: <input type=text name=delivery[$collection] class='deliverydate' planning=$planning  category=$collection  value='$collection_delivery_date'/>
					</span>";
				}
				elseif ($collection_delivery_date) {
					$delivery = "<span class=delivery >Delivery Date: $collection_delivery_date</span>";
				}
				else {
					$delivery = null;
				}
	
				$list  .= "<h6>".$items['caption'].$delivery."</h6>";
	
				$table = new Table(array(
					'id' => 'table_'.$planning.$collection		
				));
				
				$table->datagrid = $items['items'];
				
				$table->mps_material_collection_code(
					Table::ATTRIBUTE_NOWRAP, 
					'width=10%'
				);
				
				$table->mps_material_hsc(
					Table::ATTRIBUTE_NOWRAP, 
					'width=10%'
				);
				
				$table->mps_material_code(
					Table::ATTRIBUTE_NOWRAP, 
					'width=15%'
				);
				
				$table->mps_material_name();
				
				$table->set_of(
					Table::ATTRIBUTE_NOWRAP,
					Table::ATTRIBUTE_ALIGN_RIGHT, 
					'width=5%'
				);
				
				$table->mps_material_price(
					Table::ATTRIBUTE_NOWRAP, 
					Table::ATTRIBUTE_ALIGN_RIGHT, 
					'width=5%'
				);
				
				$table->currency_symbol(
					Table::ATTRIBUTE_NOWRAP, 
					'width=20px'
				);
				
				if (_array::key_exists('box', $items['items'])) {
					
					$table->caption('box', $caption);
				
					$table->box(
						Table::ATTRIBUTE_NOWRAP, 
						'width=1%'
					);
				}
				
				$list .= $table->render();
			}
		}
	}
	else  {
		$list = html::emptybox($translate->empty_result);
	}

	
	echo $list;
	
	