<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = $_REQUEST['fields'] ? explode(',', $_REQUEST['fields']) : array();
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['supplier_id'];
	$company = $_REQUEST['supplier_address'];
	$_ITEM = $_REQUEST['supplier_item'];

	$db = Connector::get();

	// get current supplier ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$supplier = new Supplier();
	$supplier->read($id);

	$currenSupplierCompany = $supplier->address;
	$newSupplierAddress  = null;

	// update supplier data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$data = array();
	
	if (in_array('supplier_address', $fields)) {
		$data['supplier_address'] = $company ? $company : null;
		$newSupplierAddress = $company && $currenSupplierCompany && $company<>$currenSupplierCompany ? $data['supplier_address'] : null;
	}
	
	if (in_array('supplier_item', $fields)) {
		$data['supplier_item'] = ($_REQUEST['supplier_item']) ? $_REQUEST['supplier_item'] : null;
	}	

	if (in_array('supplier_item_code', $fields)) {
		$data['supplier_item_code'] = ($_REQUEST['supplier_item_code']) ? $_REQUEST['supplier_item_code'] : null;
	}
	
	if (in_array('supplier_item_name', $fields)) {
		$data['supplier_item_name'] = ($_REQUEST['supplier_item_name']) ? $_REQUEST['supplier_item_name'] : null;
	}
	
	if (in_array('supplier_item_price', $fields)) {
		$data['supplier_item_price'] = ($_REQUEST['supplier_item_price']) ? $_REQUEST['supplier_item_price'] : null;
	}

	if (in_array('supplier_item_currency', $fields)) {
		$data['supplier_item_currency'] = ($_REQUEST['supplier_item_currency']) ? $_REQUEST['supplier_item_currency'] : null;
	}
	
	if ($data) {
		
		if ($supplier->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');

			$response = $supplier->update($data);
			$message = $response ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $supplier->create($data);
			
			$message = $response ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = $id && $_REQUEST['redirect'] ? $_REQUEST['redirect']."/$id" : null;
		}
	}

	if (!$response) {
		$message = $translate->message_request_failure;
	}


	// update item price (calculate width exchange rate) :::::::::::::::::::::::::::::::::::::::::::	
	
	if ($supplier->id && $response && $supplier->item) {
	
		// update item price
		$item = new Item();
		$item->read($supplier->item);
		$success = $item->updatePrice($supplier->item_price);

		if ($success) {
			$item->updateOrderPrices();
			$updateFields['#supplier_item_price'] = number_format($supplier->item_price, 2, ".", "");
		}
	}

	// update store ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	if ($id && $response && isset($_REQUEST['store_id'])) {

		$data = array();

		if (in_array('store_global_order', $fields)) {
			$data['store_global_order'] = ($_REQUEST['store_global_order']) ? $_REQUEST['store_global_order'] : null;
		}

		if (in_array('store_last_global_order', $fields)) {
			$data['store_last_global_order'] = ($_REQUEST['store_last_global_order']) ? $_REQUEST['store_last_global_order'] : null;
		}

		if (in_array('store_last_global_order_date', $fields)) {
			$data['store_last_global_order_date'] = ($_REQUEST['store_last_global_order_date']) ? $_REQUEST['store_last_global_order_date'] : null;
		}

		if (in_array('store_show_in_stock_control', $fields)) {
			$data['store_show_in_stock_control'] = ($_REQUEST['store_show_in_stock_control']) ? 1 : 0;
		}

		if (in_array('store_stock_control_starting_date', $fields)) {
			$data['store_stock_control_starting_date'] = ($_REQUEST['store_stock_control_starting_date']) ? date::sql($_REQUEST['store_stock_control_starting_date']) : null;
		}

		if (in_array('store_consumed_upto_021101', $fields)) {
			$data['store_consumed_upto_021101'] = ($_REQUEST['store_consumed_upto_021101']) ? $_REQUEST['store_consumed_upto_021101'] : null;
		}

		if (in_array('store_minimum_order_quantity', $fields)) {
			$data['store_minimum_order_quantity'] = ($_REQUEST['store_minimum_order_quantity']) ? $_REQUEST['store_minimum_order_quantity'] : null;
		}

		if (in_array('store_reproduction_time_in_weeks', $fields)) {
			$data['store_reproduction_time_in_weeks'] = ($_REQUEST['store_reproduction_time_in_weeks']) ? $_REQUEST['store_reproduction_time_in_weeks'] : null;
		}

		if (in_array('store_physical_stock', $fields)) {
			$data['store_physical_stock'] = ($_REQUEST['store_physical_stock']) ? $_REQUEST['store_physical_stock'] : null;
		}

		if (in_array('store_minimum_stock', $fields)) {
			$data['store_minimum_stock'] = ($_REQUEST['store_minimum_stock']) ? $_REQUEST['store_minimum_stock'] : null;
		}

		if ($data) {

			$data['store_item'] = $_REQUEST['store_item'] ?: $_REQUEST['supplier_item'];
			$data['store_address'] = $_REQUEST['store_address'] ?: $_REQUEST['supplier_address'];
			
			$store = new Store();
			$store->read($_REQUEST['store_id']);

			if ($store->id) {
				$data['user_modified'] = $user->login;
				$data['date_modified'] = date('Y-m-d H:i:s');
				$store->update($data);
			} 
			else {
				$data['user_created'] = $user->login;
				$data['date_created'] = date('Y-m-d H:i:s');
				$store->create($data);
			}
		}
	}

	// on supplier change update item materials ::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$itemMaterials = array();
	$itemCertificates = array();

	if ($response && $newSupplierAddress) {

		// remove item catalog materials
		$db->exec("
			DELETE FROM item_catalog_materials
			WHERE item_catalog_material_item_id = $_ITEM
		");

		// get item certificates
		$sth = $db->prepare("
			SELECT certificate_id
			FROM certificates
			WHERE certificate_address_id = ?
		");

		$sth->execute(array($currenSupplierCompany));
		$result = $sth->fetchAll();

		if ($result) {

			// remove current item certificates
			$sth = $db->prepare("
				DELETE FROM item_certificates
				WHERE item_certificate_certificate_id = ? AND item_certificate_item_id = ?
			");

			foreach ($result as $row) {
				$sth->execute(array($row['certificate_id'], $_ITEM));
			}
		}

		// get new supplier ccertificate materials
		$sth = $db->prepare("
			SELECT certificate_id, certificate_material_catalog_material_id
			FROM certificates
			INNER JOIN certificate_materials ON certificate_material_certificate_id = certificate_id
			WHERE certificate_address_id = ?
		");

		$sth->execute(array($newSupplierAddress));
		$result = $sth->fetchAll();

		if ($result) {
			foreach ($result as $row) {
				$certificate = $row['certificate_id'];
				$material = $row['certificate_material_catalog_material_id'];
				$itemMaterials[$material] = $material;
				$itemCertificates[$certificate] = $certificate;
			}
		}
	}

	// add item catalog materials
	if ($response && $itemMaterials) {

		$sth = $db->prepare("
			INSERT INTO item_catalog_materials (
				item_catalog_material_item_id,
				item_catalog_material_material_id,
				user_created
			) VALUES (?, ?, ?)
		");

		foreach ($itemMaterials as $i => $material) {
			$sth->execute(array($_ITEM, $material, $user->login));
		}
	}

	// add item certificates
	if ($response && $itemMaterials && $itemCertificates) {

		$sth = $db->prepare("
			INSERT INTO item_certificates (
				item_certificate_certificate_id,
				item_certificate_item_id,
				user_created
			) VALUES (?, ?, ?)
		");

		foreach ($itemCertificates as $i => $certificate) {
			$sth->execute(array($certificate, $_ITEM, $user->login));
		}
	}

	// response ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'fields' => $updateFields,
		'newSupplierAddress' => $newSupplierAddress,
		'item' => $_ITEM
	));
	