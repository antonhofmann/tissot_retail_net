<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	$application = $_REQUEST['application'];
	$id = $_REQUEST['red_commentcategory_id'];

	$data = array();

	if ($_REQUEST['red_commentcategory_name']) {
		$data['red_commentcategory_name'] = $_REQUEST['red_commentcategory_name'];
	}

	if ($data) {
		
		$commentCategory = new Red_Comment_Category($application);
		$commentCategory->read($id);
		
		if ($commentCategory->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $commentCategory->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $commentCategory->create($data);
			$message = ($response) ? message::request_inserted() : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}

	echo json_encode(array(
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message
	));
