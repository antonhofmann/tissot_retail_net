<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$clients = $_REQUEST['clients'];

$_REQUEST = session::filter($application, "$controller.$archived.consolidated", false);
$_SELECTED = $_REQUEST['selected'] ? explode(',', $_REQUEST['selected']) : array();

$periode = $_REQUEST['periodes'];
$region = $_REQUEST['regions'];
$addresstype = $_REQUEST['addresstypes'];
$client = $_REQUEST['clients'];
$state = $_REQUEST['states'];

// database model
$model = new Model($application);

// default filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_FILTERS = array();

// standard filters
$_FILTERS['active'] = "address_active = 1";
$_FILTERS['addresstype'] = "address_type = 1";
$_FILTERS['planning'] = "(address_involved_in_planning = 1 OR address_involved_in_planning_ff = 1)";

// filter client type
$_FILTERS['clienttype'] = $addresstype ? "address_client_type = $addresstype" : "address_client_type IN (1, 2, 3)";

// filter region
if ($region) $_FILTERS['region'] = "country_region = $region";

// filter client
if ($client) $_FILTERS['client'] = "address_id = $client";

// get confirmed clients :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_CONFIRMED = array();
$filters = $_FILTERS;

if ($periode) {
	
	$filters['periode'] = "UNIX_TIMESTAMP(posverification_confirm_date) = '$periode' ";

	$result = $model->query("
		SELECT DISTINCT
			posverification_id As id,
			address_id AS company,
			address_parent As client,
			posverification_confirm_date AS confirmed
		FROM posverifications	
		INNER JOIN db_retailnet.addresses ON address_id = posverification_address_id
		LEFT JOIN db_retailnet.posaddresses ON posaddress_client_id = posverification_address_id
		LEFT JOIN db_retailnet.countries on country_id = addresses.address_country
	")
	->filter($filters)
	->fetchAll();

	if ($result) {

		foreach ($result as $row) {
			
			$company = $row['company'];
			$client = $row['client'];
			$confirmed = $row['confirmed'];

			if ($confirmed) {
				$_CONFIRMED[$company] = true;
				$_CONFIRMED[$client] = true;
			}
		}
	}
}


// get companies :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("
	SELECT DISTINCT
		address_id,
		address_parent,
		address_company,
		country_name
	FROM db_retailnet.addresses
	LEFT JOIN db_retailnet.countries on country_id = address_country
	LEFT JOIN posverifications ON posverification_address_id = address_id
")
->filter($_FILTERS)
->order('country_name, address_company')
->fetchAll();

if ($result) {

	$count = 0;
	$datagrid = array();

	foreach ($result as $row) {
		
		$id = $row['address_id'];
		$permitted = true;

		if ($periode && $state) {
			if ($state==1) $permitted = $_CONFIRMED[$id];
			elseif ($state==2) $permitted = !$_CONFIRMED[$id];
		}

		if ($permitted) {
			$checked = in_array($id, $_SELECTED) ? 'checked=checked' : null;
			$row['box'] = "<input type=checkbox class=client name=client value=$id $checked>";
			$datagrid[$id] = $row;
			$count = $checked ? $count+1 : $count;
		}
	}

	$totalRows = count($datagrid);

	$pager = new Pager(array(
		'page' => $page,
		'totalrows' => $totalRows,
		'buttons' => false
	));

	$list_index = "Total Rows: $totalRows";
	$checkall = $totalRows==$count ? 'checked=checked' : null;
}

// table :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$table = new Table();
$table->datagrid = $datagrid;

if ($datagrid) {
	$table->caption('box', "<input type=checkbox name=checkall class=checkall $checkall>");
}

$table->country_name(
	Table::ATTRIBUTE_NOWRAP,
	'width=200px'
);

$table->address_company(
	Table::ATTRIBUTE_NOWRAP,
	'width=500px'
);

$table->box(
	Table::ATTRIBUTE_ALIGN_CENTER,
	'width=20px'
);

$table->footer($list_index);
$table = $table->render();

echo $table;
	