<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$translate = Translate::instance();
	$user = User::instance();
	$model = new Model(Connector::DB_CORE);
	
	$dashboard = $_REQUEST['dashboard'];
	
	switch ($_REQUEST['section']) {
		
		case 'load':

			if ($user->id && $dashboard) {
				
				$content = "<ul class='gridster-items' id='dashboard-$dashboard' data-id='$dashboard' >";
				
				$result = $model->query("
					SELECT DISTINCT
						ui_widget_id,
						ui_widget_title, 
						ui_widget_resizable, 
						ui_widget_draggable, 
						ui_widget_deletable,
						ui_widget_auto_refresh,
						ui_user_widget_cord_y, 
						ui_user_widget_cord_x, 
						ui_user_widget_width, 
						ui_user_widget_height, 
						ui_widget_theme_name
					FROM ui_user_widgets
						INNER JOIN ui_widgets ON ui_widget_id = ui_user_widget_widget_id
						INNER JOIN ui_widget_themes ON ui_widget_themes.ui_widget_theme_id = ui_user_widget_theme_id
					WHERE	
						ui_user_widget_user_id = $user->id AND ui_user_widget_dashboard_id = $dashboard
				")->fetchAll();
				
				
				if ($result) {

					// widget markup
					foreach ($result as $row) {
						
						$title = $row['ui_widget_title'];
						$theme = $row['ui_widget_theme_name'];
						
						$widget = $row['ui_widget_id'];
						$userWidget = $row['ui_widget_deletable'];
						
						$attributes = _array::rend_attributes(array(
							'id' 				=> 'widget-'.$row['ui_widget_id'],
							'class' 			=> 'gridster-widget',
							'data-id' 			=> $row['ui_widget_id'],
							'data-row' 			=> $row['ui_user_widget_cord_y'],
							'data-col' 			=> $row['ui_user_widget_cord_x'],
							'data-sizex' 		=> ceil($row['ui_user_widget_width'] / 50),
							'data-sizey' 		=> ceil($row['ui_user_widget_height'] / 50),
							'data-resizable' 	=> $row['ui_widget_resizable'],
							'data-draggable' 	=> $row['ui_widget_draggable'],
							'data-refresh' 		=> $row['ui_widget_auto_refresh'],
							'data-limit' 		=> 0
						));
						
						$delete = $userWidget ? "<i class='fa fa-times-circle widget-remover' data-id='$widget' ></i>" : null;
						
						$content .= "
							<li $attributes >
								<div class='panel $theme'>
									<div class='panel-heading'>
										<strong>$title</strong>
										<span class='pull-right panel-actions'>
											<i class='fa fa-cog widget-theme'></i>
											$delete
											<i class='fa fa-refresh widget-refresh' ></i>
											<!--<i class='fa fa-angle-up widget-toggler'></i>-->
										</span>
									</div>
								</div>
							</li>
						";
					}
				}
				
				
				$content .= "</ul>";
				
			}
			
		break;
		
		case 'remove':
			
			if ($user->id && $dashboard) {

				// remove dashboard widgets
				$sth = $model->db->prepare("
					DELETE FROM ui_user_widgets
					WHERE ui_user_widget_user_id = :user AND ui_user_widget_dashboard_id = :dashboard
				");
				
				$response = $sth->execute(array(
					'user' => $user->id,
					'dashboard' => $dashboard
				));

				// remove dashboard widgets
				$sth = $model->db->prepare("
					DELETE FROM ui_user_dashboards
					WHERE ui_user_dashboard_id = :dashboard AND ui_user_dashboard_dashboard_id IS NULL
				");
				
				$response = $sth->execute(array(
					'dashboard' => $dashboard
				));
				
				$message = $response
					? $translate->message_request_deleted
					: $translate->error_request;
				
				//$reload = $response;
			}
			
		break;
		
		
		
		case 'save':

			if ($_REQUEST['title']) {
			
				if ($dashboard) {
					
					$sth = $model->db->prepare("
						UPDATE ui_user_dashboards SET
							ui_user_dashboard_title = :title,
							user_modified = :username,
							date_modified = NOW()
						WHERE ui_user_dashboard_id = :id		
					");
					
					$response = $sth->execute(array(
						'title' => $_REQUEST['title'],
						'id' => $dashboard,
						'username' => $user->login
					));
					
					$message = $response
						? $translate->message_request_updated
						: $translate->error_request;
					
					
					
				} else {
					
					$sth = $model->db->prepare("
						INSERT INTO ui_user_dashboards (
							ui_user_dashboard_user_id,
							ui_user_dashboard_title,
							user_created
						)
						VALUES (
							:user,
							:title,
							:username
						)
					");
					
					$response = $sth->execute(array(
						'user' => $user->id,
						'title' => $_REQUEST['title'],
						'username' => $user->login
					));
					
					if ($response) {
						
						$reload = true;
						
						Message::request_inserted();
						Session::set('dashboard-active', $model->db->lastInsertId());
						
					} else {
						$message = $translate->error_request;
					}
				}
				
			} else {
				
				$response = false;
				$message = $translate->error_request;
			}
			
		break;
	}
	
	header('Content-Type: text/json');
	
	echo json_encode(array(
		'response' => $response,
		'content' => $content,
		'dashboard' => $dashboard,
		'message' => $message,
		'redirect' => $redirect,
		'reload' => $reload
	));
	