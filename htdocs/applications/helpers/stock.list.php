<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$model = new Model(Connector::DB_CORE);
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "address_company, scpps_stocklist_title";
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
	
	// filter: address parents
	if( !$user->permission(Stocklist::PERMISSION_EDIT) && !$user->permission(Stocklist::PERMISSION_VIEW)) {
		$filters['limited'] = "supplier_address = ".$user->address;
	}
	
	// filter: countries
	if ($_REQUEST['suppliers']) {
		$filters['suppliers'] = "address_id = ".$_REQUEST['suppliers'];
	}
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			address_company LIKE \"%$keyword%\" 
			OR scpps_stocklist_title LIKE \"%$keyword%\"
		)";
	}
	
	$binds = array(
		Stocklist::DB_BIND_SUPPLIERS,
		Supplier::DB_BIND_COMPANIES
	);
	
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			scpps_stocklist_id,
			scpps_stocklist_title,
			address_company
		FROM scpps_stocklists
	")
	->bind($binds)
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();

	if ($result) {
		
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		$stocklist = new Stocklist(); 
		
		foreach ($datagrid as $key => $row) {
			
			$stocklist->read($key);
			$items = $stocklist->item->load();
			$link = $_REQUEST['planning']."/$key";
			
			$datagrid[$key]['scpps_stocklist_title'] = (count($items) > 0) 
				? "<a href='$link' class='popup' >".$row['scpps_stocklist_title']."</a>" 
				: $row['scpps_stocklist_title'];
		}
		
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));
		
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
	
	// toolbox: utton print
	if ($datagrid && $_REQUEST['print']) {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'href' => $_REQUEST['print'],
			'label' => $translate->print
		));
	}
	
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	$result = $model->query("
		SELECT DISTINCT address_id, address_company 
		FROM scpps_stocklists
	")
	->bind($binds)
	->filter($filters)
	->exclude('suppliers')
	->order('address_company')
	->fetchAll();

	$toolbox[] = ui::dropdown($result, array(
		'name' => 'suppliers',
		'id' => 'suppliers',
		'class' => 'submit',
		'value' => $_REQUEST['suppliers'],
		'label' => $translate->all_suppliers
	));
	
	
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	
	$table->address_company(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);
	
	$table->scpps_stocklist_title(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);
	
	if ($user->permission(Stocklist::PERMISSION_ACCESS_ALL_STOCK_DATA) && !$user->permission(Stocklist::PERMISSION_EDIT_LIMITED)) {
		$table->address_company('href='.$_REQUEST['form']);
	}

	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	