<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$_JSON = array();
$_ERRORS = array();

$_ID = $_REQUEST['id'];
$_DATA = $_REQUEST['data'];

$model = new Model();

// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ID) {
	$_ERRORS[] = "Helper identitifcator is not defined.";
	goto BLOCK_RESPONDING;
}

if (!$_DATA) {
	$_ERRORS[] = "Helper topics are not defined.";
	goto BLOCK_RESPONDING;
}

$sth = $model->db->prepare("
	UPDATE assistance_sections SET
		assistance_section_assistance_id = ?,
		assistance_section_order = ?,
		user_modified = ?,
		date_modified = NOW()
	WHERE assistance_section_id = ?
");

foreach ($_DATA as $i => $section) {
	$sth->execute(array($_ID, $i+1, $user->login, $section));
}

// resoinding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONDING:

header('Content-Type: text/json');
echo json_encode($_JSON);