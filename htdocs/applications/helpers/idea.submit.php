<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

if ($user->id && $_REQUEST['idea_idea_category_id'] && $_REQUEST['idea_title'] && $_REQUEST['idea_text']) {
	
	$idea = new Idea();
	
	$response = $idea->create(array(
		'idea_idea_category_id' => $_REQUEST['idea_idea_category_id'],
		'idea_title' => $_REQUEST['idea_title'],
		'idea_text' => $_REQUEST['idea_text'],
		'idea_user_id' => $user->id,
		'user_created' => $user->login
	));
	
	if ($idea->id) {
		
		$reload = true;
		
		// notification
		Message::request_inserted();
		
		if ($_REQUEST['file']) {
			
			foreach ($_REQUEST['file'] as $file) {
				
				$dir = "/public/data/files/ideas/$idea->id";
				$filename = basename($file);
						
				// move temporary file to target destination
				$copy = Upload::moveTemporaryFile($file, $dir);
				
				if ($copy) {
					$idea->file()->create(array(
						'idea_file_idea_id' => $idea->id,
						'idea_file_file' => "$dir/$filename",
						'user_created' => $user->login
					));
				}
			}
		}
		
	} else {
		
		$notification = array(
			'content' => $translate->message_request_failure,
			'properties' => array(
				'theme' => 'error',
				'sticky' => true			
			)
		);
	}
	
	
} else {
	
	$notification = array(
		'content' => $translate->message_request_failure,
		'properties' => array(
			'theme' => 'error',
			'sticky' => true			
		)
	);
}

header('Content-Type: text/json');
echo json_encode(array(
	'response' => $response,
	'notification' => $notification,
	'reload' => $reload
));