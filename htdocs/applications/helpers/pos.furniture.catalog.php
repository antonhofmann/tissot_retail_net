<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "item_code";
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	$model = new Model($application);
	
	$catalogorder = $model->query("
		SELECT DISTINCT
			project_opening_date
		FROM db_retailnet.posorders
	")
	->bind(Pos_Order::DB_BIND_PROJECTS)
	->bind(Pos_Order::DB_BIND_ORDERS)
	->filter('type', " posorder_type = 1 ")
	->filter('kind', " posorder_project_kind IN (1,2,3,4) ")
	->filter('opening', " project_actual_opening_date <> '0000-00-00' AND project_actual_opening_date IS NOT NULL ")
	->filter('closing', " (project_closing_date <> '0000-00-00' OR project_closing_date IS NULL) ")
	->filter('state', " order_actual_order_state_code != '900' ")
	->filter('pos', " posorder_posaddress = $id	 ")
	->order('posorder_order', 'DESC')
	->offset(0,1)
	->fetch();
	
	$opening_date = $catalogorder['project_opening_date'];
	
	if ($opening_date) {
		
		$result = $model->query("
			SELECT 
				item_id,
				item_code,
				item_name,
				item_width,
				item_height,
				item_length,
				item_radius,
				DATE_FORMAT(order_date, '%d.%m.%Y') AS order_date,
				FORMAT(order_item_quantity, 0) AS order_item_quantity,
				posorder_ordernumber
			FROM db_retailnet.posorders
		")
		->bind(Pos_Order::DB_BIND_ORDERS)
		->bind(Order::DB_BIND_ORDER_ITEMS)
		->bind(Order_Item::DB_BIND_ITEMS)
		->filter('mps', " item_visible_in_mps = 1 ")
		->filter('type', " posorder_type = 2 ")
		->filter('pos', " posorder_posaddress = $id ")
		->filter('date', " order_date >= $opening_date ")
		->order($sort, $direction)
		->fetchAll();
		
		$datagrid = _array::datagrid($result);
	}
	
	
	if ($datagrid) {
		
		// toolbox: hidden utilities
		$toolbox[] = "<input type=hidden id=page name=page value='1' class='submit' />";
		$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
		$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
		
		$table = new Table(array(
			'sort' => array('column'=>$sort, 'direction'=>$direction)
		));
		
		$table->datagrid = $datagrid;
		
		$table->order_date(
			Table::ATTRIBUTE_NOWRAP,
			Table::PARAM_SORT,
			'width=5%'
		);
		
		$table->item_code(
			Table::ATTRIBUTE_NOWRAP,
			Table::PARAM_SORT,
			'width=15%'
		);
		
		$table->item_name(
			Table::ATTRIBUTE_NOWRAP,
			Table::PARAM_SORT
		);
		
		$table->order_item_quantity(
			Table::ATTRIBUTE_NOWRAP,
			Table::PARAM_SORT,
			Table::ATTRIBUTE_ALIGN_CENTER,
			'width=5%'
		);
		
		$table->item_width(
			Table::ATTRIBUTE_ALIGN_CENTER,
			'width=5%'
		);
		
		$table->item_height(
			Table::ATTRIBUTE_ALIGN_CENTER,
			'width=5%'
		);
		
		$table->item_length(
			Table::ATTRIBUTE_ALIGN_CENTER,
			'width=5%'
		);
		
		$table->item_radius(
			Table::ATTRIBUTE_ALIGN_CENTER,
			'width=5%'
		);
		
		$index = $translate->total_rows.": ".count($datagrid);
		$table->footer("<div class='table-index'>$index</div>");
		$table = $table->render();
		
		if ($toolbox) {
			$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
		}
		
		echo $toolbox;
		echo "<h5>$translate->catalog_orders</h5>";
		echo $table;
	}
	