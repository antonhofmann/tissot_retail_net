<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$archived = $_REQUEST['archived'];
	$id = $_REQUEST['id'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", false);
	
	$model = new Model(Connector::DB_CORE);

	// sql order
	$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'language_name';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// sql pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$offset = ($page-1) * $settings->limit_pager_rows;


	$company = new Company();
	$company->read($id);
	$canEdit = $company->canEdit();
	
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		$keyword = $_REQUEST['search'];
		$filters['search'] = "( language_name LIKE \"%$keyword%\" )";
	}
	
	// application id
	$Application = new Application();
	$Application->read_from_shortcut($application);
	
	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			language_id, 
			language_name
		FROM application_languages
	")
	->bind(Application_Language::DB_BIND_LANGUAGES)
	->filter($filters)
	->filter('applications', "application_language_application = ".$Application->id)
	->order($order, $direction)
	->fetchAll();
	
	if ($result) {
		
		$count = 0;
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
	
		// get company languages
		$company_language = new Company_Language();
		$company_language->id($id);
		$languages = $company_language->read_all_languages(); 
		
		foreach ($datagrid as $key => $row) {
			$icon = ($languages[$key]) ?  ui::icon('checked') : ui::icon('unchecked');
			$checked = ($languages[$key]) ? 'checked=checked' : null;
			if ($checked) $count++;
			$datagrid[$key]['checkbox'] = ($canEdit && !$archived) ? "<input type=checkbox class=language value=$key $checked />" : $icon;
		}
		
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));
		
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
		
		$checkall = (count($datagrid) == $count) ? 'checked=checked' : null;
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";

	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	
	if ($toolbox) {
		$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$order, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	$table->dataloader($dataloader);
	
	$checkbox = ($archived) ? null : "<input type=checkbox class=checkall $checkall />";
	$table->caption('checkbox', $checkbox);
	
	$table->language_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);
	
	$table->checkbox(
		'width=2%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	