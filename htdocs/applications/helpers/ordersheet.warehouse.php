<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// request
	$application = $_REQUEST['application'];
	$id = $_REQUEST['ordersheet'];
	$warehouse = $_REQUEST['warehouse'];
	$new_warehouse = $_REQUEST['new_warehouse'];
	$address_warehouse = $_REQUEST['address_warehouse_id'];
	
	// ordersheet
	$ordersheet = new Ordersheet($application);
	$ordersheet->read($id);
	
	if ($ordersheet->id) {
		
		switch ($_REQUEST['section']) {
			
			
			/**
			 * Check if ordersheet has planned and delivered quantities
			 */
			case 'check':
				
				$model = new Model($application);
				
				// check planned quantities
				// planned quantities
				$planned_quantities = $model->query("
					SELECT *
					FROM mps_ordersheet_item_planned
				")
				->bind(Ordersheet_Item_Planned::DB_BIND_ORDERSHEET_ITEMS)
				->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = $id")
				->filter('warehouse', "mps_ordersheet_item_planned_warehouse_id = $warehouse")
				->filter('quantities', "mps_ordersheet_item_planned_quantity IS NOT NULL")
				->fetchAll();
				
				// delivered quantities
				$delivered_quantities = $model->query("
					SELECT *
					FROM mps_ordersheet_item_delivered
				")
				->bind(Ordersheet_Item_Delivered::DB_BIND_ORDERSHEET_ITEMS)
				->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = $id")
				->filter('warehouse', "mps_ordersheet_item_delivered_warehouse_id = $warehouse")
				->filter('quantities', "mps_ordersheet_item_delivered_quantity IS NOT NULL")
				->fetchAll();
				
				if (!$planned_quantities && !$delivered_quantities) {
					$response = true;
				} else {
					$response = false;
					$message = $translate->warning_warehouse;
				}
				
			break;
			
			/**
			 * Add new order sheet warehouse
			 * if warehouse is not by Company, add new warehouse to company
			 */
			case 'add':
				
				// add company warehouse to order sheet
				if ($address_warehouse && $address_warehouse <> 'new') {
					$response = $ordersheet->warehouse()->create($address_warehouse);
				}
				// create new company warehouse
				// and add this warehouse to order sheet
				else {
					
					$company_warehouse = new Company_Warehouse(Connector::DB_CORE);
					
					// add new company warehouse
					$id = $company_warehouse->create(array(
						'address_warehouse_address_id' => $ordersheet->address_id,
						'address_warehouse_name' => trim($_REQUEST['address_warehouse_name']),
						'address_warehouse_sap_customer_number' => trim($_REQUEST['address_warehouse_sap_customer_number']),
						'address_warehouse_sap_shipto_number' => trim($_REQUEST['address_warehouse_sap_shipto_number']),
						'address_warehouse_active' => 1,
						'user_created' => $user->login,
						'date_created' => date('Y-m-d H:i:s')
					));
					
					// add warehouse to order sheet
					if ($id) {
						$response = $ordersheet->warehouse()->create($id);
					}
				}
				
				$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			break;
			
			/**
			 * Remove warehose from order sheets
			 */
			case 'delete':
				
				$model = new Model($application);
				
				// planned quantities
				$planned_quantities = $model->query("
					SELECT * 
					FROM mps_ordersheet_item_planned
				")
				->bind(Ordersheet_Item_Planned::DB_BIND_ORDERSHEET_ITEMS)
				->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = $id")
				->filter('warehouse', "mps_ordersheet_item_planned_warehouse_id = $warehouse")
				->filter('quantities', "mps_ordersheet_item_planned_quantity IS NOT NULL")
				->fetchAll();
				
				// delivered quantities
				$delivered_quantities = $model->query("
					SELECT * 
					FROM mps_ordersheet_item_delivered
				")
				->bind(Ordersheet_Item_Delivered::DB_BIND_ORDERSHEET_ITEMS)
				->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = $id")
				->filter('warehouse', "mps_ordersheet_item_delivered_warehouse_id = $warehouse")
				->filter('quantities', "mps_ordersheet_item_delivered_quantity IS NOT NULL")
				->fetchAll();
				
				if (!$planned_quantities && !$delivered_quantities) {
					$ordersheet->warehouse()->read($warehouse);
					$response = $ordersheet->warehouse()->delete();
				}
				
				$message = ($response) ? Message::request_deleted() : $translate->message_request_failure;
			
			break;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	header('Content-Type: text/json');
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));
	