<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

Session::init();


$settings = Settings::init();

$_MAX_TRIALS = $settings->limit_maxtrials;

$_USERNAME = $_REQUEST['username'];
$_PASSWORD = $_REQUEST['password'];
$_REDIRECT = $_SESSION['redirect'];
$_ONLOAD_REDIRECT = $_REDIRECT;
$_IP = url::ip();


// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// check hijacking 
if (filter_var($_IP, FILTER_VALIDATE_IP)==false) {
	$_MESSAGE = "Something is not correct with your IP. Login is not possible.";
	goto BLOCK_RESPONDING;
}


// security controller
$security = new Security();
$user = new User();


//chck IP trials
$_TOTAL_TRIALS = $security->trials($_IP);


// unlock ip address
if ($_TOTAL_TRIALS >= $_MAX_TRIALS) {
	
	session_destroy();
	if ($security->isExcluded() || $user->canUnlockIP())  {
		$security->unlock($_IP);
	} else {
		$_REDIRECT = "/messages/error/locked_ip";
		$_RESPONSE = true;
		goto BLOCK_RESPONDING;
	}
}


// check identification
$user = User::identification(array(
	'user_login' => $_REQUEST['username'],
	'user_password' => $_REQUEST['password']
));

// user not found
if (!$user['user_id']) {

	// tracelogin failures
	$security->traceFailure($_IP, 'login', $_USERNAME, $_PASSWORD);
	
	// set locked ip address
	$security->lock();
	$maxTrials = $security->trials();
	
	// ecxlude address
	if ($maxTrials >= $settings->limit_maxtrials) {
		$_REDIRECT = "/messages/error/locked_ip";
		$_RESPONSE = true;
	}
	
	$translate = Translate::instance();
	$_MESSAGE = $translate->error_identification_failure;
	$_REDIRECT = null;
	$_RESPONSE = false;
	goto BLOCK_RESPONDING;
}

// inactive account
if (!$user['user_active']) {
	$_REDIRECT = "/messages/show/inactive_user_account";
	$_RESPONSE = true;
	goto BLOCK_RESPONDING;
}



// identification ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Session::set('user_id', $user['user_id']);

// reset expired password
if (!$_REDIRECT && !$user['user_active']) {
	$_REDIRECT = "/security/reset";
	message::password_expired();
	$_RESPONSE = true;
	goto BLOCK_RESPONDING;
}


// redirect ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	

if ($_REDIRECT) {

	$url = url::system_request($_REDIRECT);

	$_TEST = $url;

	$model = Connector::get();

	$sth = $model->prepare("
		SELECT DISTINCT navigation_id AS id
		FROM navigations
		INNER JOIN role_navigations ON navigations.navigation_id = role_navigations.role_navigation_navigation
		INNER JOIN user_roles ON role_navigations.role_navigation_role = user_roles.user_role_role
		WHERE user_role_user = ? AND navigation_url = ?
	");

	$sth->execute(array( $user['user_id'], $url));
	$result = $sth->fetch();

	if ($result['id']) $_REDIRECT = "/$_REDIRECT";
	else {
		unset($_REDIRECT);
		$error = $sth->errorInfo();
	}
}

if (!$_REDIRECT) {
				
	$applications = navigation::get_user_applications();

	if ($applications) {
		foreach ($applications as $row) {
			if ($row['navigation_url']<>'home' && $row['navigation_url']<>'index') {
				$_REDIRECT = '/'.$row['navigation_url'];
				break;
			}
		}
	}
}

$_REDIRECT = $_REDIRECT 
	? $settings->http_protocol.'://'.$_SERVER['SERVER_NAME'].$_REDIRECT.$_REQUEST['hash']
	: "/messages/show/no_access_application";


// authentification ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_RESPONSE = true;

Session::set('user_login', $user['user_login']);
Session::set('user_firstname', $user['user_firstname']);
Session::set('user_name', $user['user_name']);

$permissions = permission::loader();
$array = array();

if ($permissions) {
	foreach ($permissions as $key => $permission) $array[] = $permission;
	session::set('permissions', join(' ', $array));
}

// user pemissions
$permissions = permission::loader(null,true);
$array = array();

if ($permissions) {
	foreach ($permissions as $key => $permission) $array[] = $permission;
	session::set('user_permissions', join(' ', $array));
}

// session message
message::login_success();
session::remove('redirect');


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONDING:

header("Content-Type: text/json");
echo json_encode(array(
	'response' => $_RESPONSE,
	'redirect' => $_REDIRECT,
	'message' => $_MESSAGE,
	'maxtrials' => $_MAX_TRIALS,
	'totaltrials' => $_TOTAL_TRIALS,
	'test' => $_TEST
));
	