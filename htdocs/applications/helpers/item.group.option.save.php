<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	// item group option id
	$id = $_REQUEST['item_group_option_id'];
	
	$data = array();	
	
	if (in_array('item_group_option_group', $fields)) {
		$data['item_group_option_group'] = $_REQUEST['item_group_option_group'] ? $_REQUEST['item_group_option_group'] : null;
	}	
	
	if (in_array('item_group_option_name', $fields)) {
		$data['item_group_option_name'] = $_REQUEST['item_group_option_name'] ? $_REQUEST['item_group_option_name'] : null;
	}

	if (in_array('item_group_option_caption1', $fields)) {
		$data['item_group_option_caption1'] = $_REQUEST['item_group_option_caption1'] ? $_REQUEST['item_group_option_caption1'] : null;
	}
		
	if (in_array('item_group_option_description1', $fields)) {
		$data['item_group_option_description1'] = $_REQUEST['item_group_option_description1'] ? $_REQUEST['item_group_option_description1'] : null;
	}
		
	if (in_array('item_group_option_quantity1', $fields)) {
		$data['item_group_option_quantity1'] = $_REQUEST['item_group_option_quantity1'] ? $_REQUEST['item_group_option_quantity1'] : null;
	}
		
	if (in_array('item_group_option_item1', $fields)) {
		$data['item_group_option_item1'] = $_REQUEST['item_group_option_item1'] ? $_REQUEST['item_group_option_item1'] : null;
	}
		
	if (in_array('item_group_option_quantity2', $fields)) {
		$data['item_group_option_quantity2'] = $_REQUEST['item_group_option_quantity2'] ? $_REQUEST['item_group_option_quantity2'] : null;
	}
		
	if (in_array('item_group_option_item2', $fields)) {
		$data['item_group_option_item2'] = $_REQUEST['item_group_option_item2'] ? $_REQUEST['item_group_option_item2'] : null;
	}
		
	if (in_array('item_group_option_configcode1', $fields)) {
		$data['item_group_option_configcode1'] = $_REQUEST['item_group_option_configcode1'] ? $_REQUEST['item_group_option_configcode1'] : null;
	}
		
	if (in_array('item_group_option_info1', $fields)) {
		$data['item_group_option_info1'] = $_REQUEST['item_group_option_info1'] ? $_REQUEST['item_group_option_info1'] : null;
	}
		
	if (in_array('item_group_option_logical', $fields)) {
		$data['item_group_option_logical'] = $_REQUEST['item_group_option_logical'] ? $_REQUEST['item_group_option_logical'] : null;
	}
		
	if (in_array('item_group_option_caption2', $fields)) {
		$data['item_group_option_caption2'] = $_REQUEST['item_group_option_caption2'] ? $_REQUEST['item_group_option_caption2'] : null;
	}
		
	if (in_array('item_group_option_description2', $fields)) {
		$data['item_group_option_description2'] = $_REQUEST['item_group_option_description2'] ? $_REQUEST['item_group_option_description2'] : null;
	}
		
	if (in_array('item_group_option_quantity3', $fields)) {
		$data['item_group_option_quantity3'] = $_REQUEST['item_group_option_quantity3'] ? $_REQUEST['item_group_option_quantity3'] : null;
	}
		
	if (in_array('item_group_option_item3', $fields)) {
		$data['item_group_option_item3'] = $_REQUEST['item_group_option_item3'] ? $_REQUEST['item_group_option_item3'] : null;
	}
		
	if (in_array('item_group_option_quantity4', $fields)) {
		$data['item_group_option_quantity4'] = $_REQUEST['item_group_option_quantity4'] ? $_REQUEST['item_group_option_quantity4'] : null;
	}
		
	if (in_array('item_group_option_item4', $fields)) {
		$data['item_group_option_item4'] = $_REQUEST['item_group_option_item4'] ? $_REQUEST['item_group_option_item4'] : null;
	}
		
	if (in_array('item_group_option_configcode2', $fields)) {
		$data['item_group_option_configcode2'] = $_REQUEST['item_group_option_configcode2'] ? $_REQUEST['item_group_option_configcode2'] : null;
	}
		
	if (in_array('item_group_option_info2', $fields)) {
		$data['item_group_option_info2'] = $_REQUEST['item_group_option_info2'] ? $_REQUEST['item_group_option_info2'] : null;
	}
			
	if ($data) {
	
		$itemGroupOption = new Item_Group_Option();
		$itemGroupOption->read($id);
	
		if ($itemGroupOption->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $itemGroupOption->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $itemGroupOption->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	