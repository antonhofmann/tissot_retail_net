<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

// url vars
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];

$item = $_REQUEST['item_file_item'];
$file = $_REQUEST['item_file_id'];

$upload_path = "/files/items/$item";

if (in_array('item_file_title', $fields)) {
	$data['item_file_title'] = $_REQUEST['item_file_title'];
}

if (in_array('item_file_description', $fields)) {
	$data['item_file_description'] = $_REQUEST['item_file_description'];
}

if (in_array('item_file_purpose', $fields)) {
	$data['item_file_purpose'] = $_REQUEST['item_file_purpose'];
}

if (in_array('item_file_picture_config_code', $fields)) {
	$data['item_file_picture_config_code'] = $_REQUEST['item_file_picture_config_code'];
}

if ($_REQUEST['has_upload'] && $_REQUEST['item_file_path']) {
	
	$path = upload::move($_REQUEST['item_file_path'], $upload_path);
	$data['item_file_path'] = $path;

	$extension = file::extension($path);
	$data['item_file_type'] = File_Type::get_id_from_extension($extension);
}


if($item && $data) { 
	
	$data['item_file_item'] = $item;
	
	$itemFile = new Item_File();
	$itemFile->read($file);

	if ($itemFile->id) {
		
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		
		$response = $itemFile->update($data);
		
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {
		
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		
		$response = $id = $itemFile->create($data);
		
		$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
		$redirect = $_REQUEST['redirect'] ? $_REQUEST['redirect']."/$id" : null;
	}	
}
else {
	$response = false;
	$message = $translate->message_request_failure;
}

echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'redirect' => $redirect,
	'path' => $path
));
