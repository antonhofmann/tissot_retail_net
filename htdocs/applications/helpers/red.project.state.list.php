<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	
	$model = new Model($application);

	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "red_projectstate_name";
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$offset = ($page-1) * $settings->limit_pager_rows;


	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "
			(red_projectstate_name LIKE \"%$keyword%\")
		";
	}

	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS
			red_projectstate_id,
			red_projectstate_icon,
			red_projectstate_name
		FROM red_projectstates
	")
	->filter($filters)
	->order($sort, $direction)
	->offset($offset)
	->fetchAll();

	

	if ($result) {
		
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		
		$icon = new Red_Icon($application);
		
		foreach ($datagrid as $key => $value) {
			
			$icon->read($value['red_projectstate_icon']);
			$path = $icon->path;
			
			$datagrid[$key]['red_projectstate_icon'] = "<span class='icon' style='background: url($path) no-repeat;'></span>";
		}
		
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));

		$pageIndex = $pager->index();
		$pageControlls = $pager->controlls();
	}

	// hidden table fields
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";

	// toolbox: button add
	if ($_REQUEST['add']) {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'label' => $translate->add_new,
			'href' => 	$_REQUEST['add']	
		));
	}
	
	// toolbox: search full text
	$toolbox[] = ui::searchbox();

	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}

	$table = new Table(array(
		'sort' => array(
			'column' => $sort,
			'direction' => $direction
		)
	));

	$table->datagrid = $datagrid;

	$table->red_projectstate_icon(
		'width=5%',
		Table::ATTRIBUTE_NOWRAP
	);

	$table->red_projectstate_name(
		'href='.$_REQUEST['form'],
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);

	$table->footer($pageIndex);
	$table->footer($pageControlls);
	$table = $table->render();

	echo $toolbox.$table;
