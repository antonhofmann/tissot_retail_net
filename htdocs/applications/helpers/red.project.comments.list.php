<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$id = $_REQUEST['id'];
	
	// permissions 
	$permission_project_view = user::permission(Red_Project::PERMISSION_VIEW_PROJECTS);
	$permission_project_view_limited = user::permission(Red_Project::PERMISSION_VIEW_LIMITED_PROJECTS);
	$permission_comment_edit = user::permission(Red_Project::PERMISSION_EDIT_COMMENTS);

	// db model
	$model = new Model($application);
	
	// datagrid
	$comments = $model->query("
		SELECT SQL_CALC_FOUND_ROWS
			red_comment_id,
			red_comment_project_id,
			red_comment_file_id,
			red_comment_category_id,
			red_comment_owner_user_id,
			red_comment_comment,
			red_commentcategory_name,
			user_firstname,
			user_name,
			red_comment_comment,
			DATE_FORMAT(red_comments.date_created, '%d.%m.%Y %H:%i') AS date_created
		FROM red_comments
	 ")
	->bind(Red_Comment::BIND_USER_ID)
	->bind(Red_Comment::BIND_COMMENT_CATEGORY)
	->filter('project', "red_comment_project_id = $id")
	->order('red_comments.date_created', 'DESC')
	->fetchAll();


	if ($view == 'limited' && is_array($comments)) {
		
		$comment = new Red_Comment($application);
		
		foreach ($comments as $row) {

			$comment->read($row[red_comment_id]);
			
			if ($comment->user_has_access($user->id)) {
				$filtered_comments[] = $row;
			}
		}
		
		$comments = $filtered_comments;
	}

	if (is_array($comments)) {
		
		foreach ($comments as $row) {
			
			$category = $row['red_comment_category_id'];
			$comment = $row['red_comment_id'];
			$comment_file = $row['red_comment_file_id'];
			
			$datagrid[$category]['category'] = $row['red_commentcategory_name'];
			$datagrid[$category]['comments'][$comment]['content'] = nl2br($row['red_comment_comment']);
			$datagrid[$category]['comments'][$comment]['user'] = $row['user_name']." ".$row['user_firstname'];
			$datagrid[$category]['comments'][$comment]['date'] = $row['date_created'];
			
			
			$file_list = null;

			if ($comment_file) {
				
				$file_ids = unserialize($comment_file);
				
				rsort($file_ids);
				
				foreach ($file_ids as $file_id) {
					
					$file = new Red_File($application);
					$file->read($file_id);
					
					if ($permission_project_view || ($permission_project_view_limited && $file->user_has_access($user->id))) {
						
						if ($file->title) {
							
							$file_extension = file::extension($file->path);
							
							$file_list .= "
								<li class=modal path='".$file->path."' >
									<span class='file-extension small $file_extension'></span>
									<span class=label >".$file->title."</span>
								</li>
							";
						}
					}
				}
				
				if ($file_list) {
					$target = "comment_files_$comment";
					$datagrid[$category]['comments'][$comment]['files']  = "<span class='button filebox' data='#$target' ><span class='icon attachment'></span></span>";
					$popover_content .= "<div id=$target ><ul>$file_list</ul></div>";
				}
			}
		}
	}

	if ($datagrid) {
		
		$projectComment = new Red_Comment($application);
		$projectComment->read($id);
		
		//user_can_view
		if ($permission_project_view || $permission_project_view_limited) {
			$user_can_view = true;
		}
		
		$icon = ($archived) ? 'show' : 'edit';
		
		foreach ($datagrid as $key => $row) {
			
			$list .= "<h6>".$row['category']."</h6>";
			
			if ($row['comments']) {
				
				foreach ($row['comments'] as $key => $comment) {
					
					$content = $comment['content'];
					$user = $comment['user'];
					$date = $comment['date'];
					$files = $comment['files'];
					$link = $_REQUEST['form']."/$key";
					$bottom = ($_REQUEST['form'] && $user_can_view) ? "<a class='button' href='$link' ><span class='icon $icon'></span></a>" : null;
					
					$list .= "
						<div class='comment-box'>
							<div class=comment-header >
								<div class=header-left >
									<span class=user >$user</span>
									<span class=date >$date</span>
								</div>
								<div class=header-right >
									$bottom $files
								</div>
							</div>
							<div class=comment-content >$content</div>
						</div>		
					";
				}
			}
		}
	}

	if ($_REQUEST['print'])  {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'href' => $_REQUEST['print'],
			'label' => $translate->print
		));
	}

	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}

	

	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}

	echo $toolbox.$list.$popover_content;

