<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$item = $_REQUEST['item'];

$permission_edit = user::permission('can_edit_catalog');
$permission_edit_items = user::permission('can_edit_items_in_catalogue');
$_CAN_EDIT = $permission_edit || $permission_edit_items ? true : false;
$permission_view = user::permission('can_browse_catalog_in_admin');

$model = new Model($application);

$_REQUEST = session::filter($application, "$controller.$archived.$action", false);


// filter active
$active = isset($_REQUEST['active']) ? $_REQUEST['active'] : 1;
$filters = $active ? "(category_not_in_use = 0 AND product_line_active = 1)" : "product_line_active = 0";

// item categories
$result = $model->query("
	SELECT 
		category_item_category,
		category_product_line
	FROM category_items
	LEFT JOIN categories ON category_items.category_item_category = categories.category_id
	WHERE category_item_item = $item
")->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$category = $row['category_item_category'];
		$itemCategories[$category] = true;

		if ($row['category_product_line']) {
			$productlines[] = $row['category_product_line'];
		}
	}

	$productlines = array_unique($productlines);

	$filters .= " OR product_line_id IN (".join(',',$productlines).")";
}

$result = $model->query("
	SELECT 
		product_lines.product_line_id, 
		product_lines.product_line_name, 
		categories.category_id,
		categories.category_name
	FROM product_lines 
	INNER JOIN categories ON product_lines.product_line_id = categories.category_product_line
	WHERE $filters
	ORDER BY product_line_name, category_name 
")->fetchAll();

if ($result) {

	foreach ($result as $row) {
		
		$productline = $row['product_line_id'];
		$category = $row['category_id'];

		$datagrid[$productline]['product_line_name'] = $row['product_line_name'];
		$datagrid[$productline]['categories'][$category]['category_name'] = $row['category_name'];

		$checked = $itemCategories[$category] ? 'checked=checked' : null;

		if ($_CAN_EDIT) {
			$datagrid[$productline]['categories'][$category]['checkbox'] = "<input type='checkbox' data-line='$productline' value='$category' class='category' $checked >";
		} elseif($checked) {
			$datagrid[$productline]['categories'][$category]['checkbox'] = "<i class='fa fa-check-circle' ></i>";
		}
	}
}


// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";

$toolbox[] = ui::dropdown(array(
	1 => "Active",
	0 => "Inactive"
), array(
	'name' => 'active',
	'id' => 'active',
	'class' => 'submit',
	'value' => $active,
	'caption' => false
));

// toolbox: form
if ($toolbox) {
	echo "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
}

if ($datagrid) {

	foreach ($datagrid as $productline => $row) {

		if ($row['categories']) {

			$table = new Table();
			$table->datagrid = $row['categories'];

			$table->checkbox('width=20px');
			if ($canedit) $table->caption('checkbox',  "<input type='checkbox' class='productline' >");
			
			$table->category_name();
			$table->caption('category_name',  $row['product_line_name']);
			
			echo $table->render();
		}
	}
}


