<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$pos = $_REQUEST['pos'];
	$id = $_REQUEST['id'];
	
	if ($id && $pos && (user::permission(Pos::PERMISSION_EDIT) OR user::permission(Pos::PERMISSION_EDIT_LIMITED) )) {
		
		$pos_material = new Pos_Material($application);
		$pos_material->read($id);
		$delete = $pos_material->delete();
		
		if ($delete) {
			Message::request_deleted();
			url::redirect("/$application/$controller/$action/$pos");
		} else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$pos/$id");
		}
	} else {
		Message::request_failure();
		url::redirect("/$application/$controller/$action/$pos");
	}