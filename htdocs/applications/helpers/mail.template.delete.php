<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	// current user
	$user = User::instance();
	
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	if ($id &&  $user->permission(Mail_Template::PERMISSION_EDIT)) {
		
		$mail_template = new Mail_Template();
		$mail_template->read($id);
		$delete = $mail_template->delete();

		if ($delete) {
			Message::request_deleted();
			url::redirect("/$appliction/$controller");
		} else {
			Message::request_failure();
			url::redirect("/$appliction/$controller/$action/$id");
		}
	}
	else {
		Message::access_denied();
		url::redirect("/$appliction/$controller");
	}