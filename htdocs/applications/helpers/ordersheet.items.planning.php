<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc'; 

$user = User::instance();
$translate = Translate::instance();

//	request ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];
$id = $_REQUEST['ordersheet'];
$version = $_REQUEST['version'];
$filterDistributionChannel = $_REQUEST['distribution_channels'];
$filterTurnoverWatches = $_REQUEST['turnoverclass_watches'];
$filterSalesRepresentative = $_REQUEST['sales_representative'];
$filterDecorationPerson = $_REQUEST['decoration_person'];
$filterProvince = $_REQUEST['provinces'];

$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

//	order sheet ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// model
$model = new Model($application);

// current order sheet	
$ordersheet = new Ordersheet($application);
$ordersheet->read($id);

// company
$company = new Company();
$company->read($ordersheet->address_id);
	

//	spreadsheet settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

define(COLUMN_PLANNING, 'planned');					// column planning
define(COLUMN_DISTRIBUTION, 'delivered');			// column distribution
define(COLUMN_PARTIALY_CONFIRMED, 'partial');		// column partially distribution
define('CELL_READONLY', 'true');					// spreadsheet readonly attribute

// spreadsheet datagrid
$datagrid = array();

// some important states
$_STATE_EDIT = $ordersheet->state()->canEdit;
$_STATE_APPROVED = $ordersheet->state()->onApproving() ? false : true;
$_STATE_DISTRIBUTED = ($ordersheet->state()->isDistributed() && $ordersheet->isDistributed()) ? true : false;

// readonly mod
$_STATE_READONLY = false;

if ($version || $ordersheet->state()->isDistributed()) $_STATE_READONLY = true;
elseif ($ordersheet->state()->owner && $ordersheet->state()->isPreparation() ) $_STATE_READONLY = true;

// mode
$_MODE_PLANNING = ( $ordersheet->state()->onPreparation() || $ordersheet->state()->onConfirmation()) ? true : false;
$_MODE_DISTRIBUTION = ($ordersheet->state()->onDistribution() || $ordersheet->state()->isDistributed()) ? true : false;

// spreadsheet attribute readonly
$_ATTR_READONLY =  $_STATE_READONLY ? 'true' : 'false';

// extended rows (warehouses)
$_AREA_WAREHOUSE = ($_STATE_DISTRIBUTED || $version) ? false : true;

$_STOCK_RESERVE_EDIT = false;

// stock reserve edit
if ($_STATE_EDIT) {
	if ($ordersheet->state()->canAdministrate()) $_STOCK_RESERVE_EDIT = $_STATE_APPROVED ? false : true;
	else $_STOCK_RESERVE_EDIT = ($ordersheet->state()->onCompleting()) ? true : false;
}

// set item quantity validator
$_VALIDATA_TOTAL_ITEM_QUANTITY = $_STATE_APPROVED ? true : false;

// check if delivered quantities are once confirmed
if ($_MODE_DISTRIBUTION) {
	
	$result = $model->query("
		SELECT COUNT(mps_ordersheet_item_delivered_id) AS total
		FROM mps_ordersheet_item_delivered	
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id 
		WHERE mps_ordersheet_item_ordersheet_id = $ordersheet->id AND mps_ordersheet_item_delivered_confirmed IS NOT NULL
	")->fetch();

	$OrderSheetHasConfirmedItems = $result['total'];
}

// spreadsheet start references
$_FIRST_FIXED_ROW = 2;
$_FIRST_FIXED_COL = 1;
$_GROUP_SEPARATOR = 1;

// planning class name
$cssClassPlanning = ($_MODE_PLANNING) ? 'planning' : null;

$_ROW_BLANK 		= 1;
$_ROW_GROUP_CAPTION = $_FIRST_FIXED_ROW;
$_ROW_ITEM_CAPTION  = $_FIRST_FIXED_ROW + 1;
$_COL_POS_ICON 		= $_FIRST_FIXED_COL;
$_COL_POS_CAPTION 	= $_FIRST_FIXED_COL + 1;


//	filters ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$filters = array();
$filerCaptions = array();

// filter: pos
$filters['default'] = "(
	posaddress_client_id = $ordersheet->address_id 
	OR (address_parent = $ordersheet->address_id AND address_client_type = 3)
)";

// filter: distribution channels
if ($filterDistributionChannel) {
	$filters['distribution_channels'] = "posaddress_distribution_channel = $filterDistributionChannel";
	$isFilterActive = 'active';
}

// filter: turnover classes
if ($filterTurnoverWatches) {
	$filters['turnoverclass_watches'] = "posaddress_turnoverclass_watches = $filterTurnoverWatches";
	$isFilterActive = 'active';
}

// filter: sales represenatives
if ($filterSalesRepresentative) {
	$filters['sales_representative'] = "posaddress_sales_representative = $filterSalesRepresentative";
	$isFilterActive = 'active';
}

// filter: decoration persons
if ($filterDecorationPerson) {
	$filters['decoration_person'] = "posaddress_decoration_person = $filterDecorationPerson";
	$isFilterActive = 'active';
}

// provinces
if ($_REQUEST['provinces']) {
	$filters['provinces'] = "province_id = $filterProvince";
	$isFilterActive = 'active';
}

// for archived ordersheets show only POS locations which has distributed quantities 
if ($_STATE_DISTRIBUTED) {
	
	$result = $model->query("
		SELECT GROUP_CONCAT(DISTINCT mps_ordersheet_item_delivered_posaddress_id) AS pos
		FROM mps_ordersheet_item_delivered
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id 
		WHERE mps_ordersheet_item_ordersheet_id = $ordersheet->id AND mps_ordersheet_item_delivered_posaddress_id > 0
	")->fetch();
	
	if ($result) $filters['active'] = "posaddress_id IN ({$result[pos]})";

} else {
	$filters['active'] = "(
		posaddress_store_closingdate = '0000-00-00'
		OR posaddress_store_closingdate IS NULL
		OR posaddress_store_closingdate = ''
	)";
}

$filters = $filters ? join(' AND ', array_values($filters)) : null;
	
	
//	dataloader pos locations :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$posLocations = array();

$result = $model->query("
	SELECT DISTINCT
		posaddress_id,
		posaddress_name,
		posaddress_place,
		postype_id,
		postype_name
	FROM db_retailnet.posaddresses
	INNER JOIN db_retailnet.addresses ON address_id = posaddress_client_id
	INNER JOIN db_retailnet.places ON place_id = posaddress_place_id
	INNER JOIN db_retailnet.provinces ON province_id = place_province
	INNER JOIN db_retailnet.postypes ON postype_id = posaddress_store_postype
	WHERE $filters
	ORDER BY posaddress_name, posaddress_place
")->fetchAll();


if ($result) {
	
	foreach ($result as $row) {
		
		$pos = $row['posaddress_id'];
		$type = $row['postype_id'];
		
		// group pos locations by pos type
		$posLocations[$type]['name'] = $row['postype_name'];
		$posLocations[$type]['data'][$pos]['name'] = $row['posaddress_name'].', '.$row['posaddress_place'];
		
		$sapInvolvedPos[$pos] = true;
	}
}


//	dataloader order sheet items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$OrderSheetItems = array();
$orderSheetItemPartiallyDistributed = array();
$orderSheetItemDistributed = array();

$result = $model->query("
	SELECT
		mps_ordersheet_item_id,
		mps_ordersheet_item_material_id,
		mps_ordersheet_item_quantity,
		mps_ordersheet_item_quantity_approved,
		mps_ordersheet_item_quantity_confirmed,
		mps_ordersheet_item_quantity_shipped,
		mps_ordersheet_item_quantity_distributed,
		mps_material_planning_type_id,
		mps_material_planning_type_name,
		mps_material_collection_category_id,
		mps_material_collection_category_code,
		mps_material_id,
		mps_material_code,
		mps_material_name
	FROM mps_ordersheet_items
 	INNER JOIN mps_ordersheets ON  mps_ordersheet_id = mps_ordersheet_item_ordersheet_id 
 	INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id 
 	LEFT JOIN mps_material_planning_types ON mps_material_planning_type_id = mps_material_material_planning_type_id 
 	LEFT JOIN mps_material_collection_categories ON mps_material_collection_category_id = mps_material_material_collection_category_id 
 	WHERE mps_ordersheet_id = $ordersheet->id 
 	ORDER BY 
 		mps_material_planning_type_name asc, 
 		mps_material_collection_category_code asc, 
 		mps_material_code asc, mps_material_name asc
")->fetchAll();


if ($result) {

	$onApproving = $ordersheet->state()->onApproving();
	
	foreach ($result as $row) {
		
		$visible = true;

		$quantityApproved = $row['mps_ordersheet_item_quantity_approved'];
		$quantityShipped = $row['mps_ordersheet_item_quantity_shipped'];
		$quantityDistributed = $row['mps_ordersheet_item_quantity_distributed'];
		
		// show item in spreadsheet
		// for mod planning: quantity approved greater then zero or null
		// for mod distribution: quantity confirmed greater then zero
		if ($_MODE_DISTRIBUTION) $visible = $quantityShipped > 0 ? true : false;
		elseif (!$onApproving) $visible = $quantityApproved > 0 ? true : false;
		
		if ($visible) {
			
			$item = $row['mps_ordersheet_item_id'];
			$material = $row['mps_ordersheet_item_material_id'];
			$group = $row['mps_material_planning_type_id'];
			$subgroup = $row['mps_material_collection_category_id'];

			// group order sheet by planning type and collection category
			$OrderSheetItems[$group]['caption'] = $row['mps_material_planning_type_name'];
			$OrderSheetItems[$group]['data'][$subgroup]['caption'] = $row['mps_material_collection_category_code'];
			$OrderSheetItems[$group]['data'][$subgroup]['data'][$item]['item'] = $row['mps_material_id'];
			$OrderSheetItems[$group]['data'][$subgroup]['data'][$item]['code'] = $row['mps_material_code'];
			$OrderSheetItems[$group]['data'][$subgroup]['data'][$item]['name'] = $row['mps_material_name'];
			$OrderSheetItems[$group]['data'][$subgroup]['data'][$item]['quantity'] = $onApproving ? $row['mps_ordersheet_item_quantity'] : $row['mps_ordersheet_item_quantity_approved'];
			$OrderSheetItems[$group]['data'][$subgroup]['data'][$item]['shipped'] = $row['mps_ordersheet_item_quantity_shipped'];
			$OrderSheetItems[$group]['data'][$subgroup]['data'][$item]['distributed'] = $row['mps_ordersheet_item_quantity_distributed'];

			// global references
			$_REFERENCES[$item] = array(
				'material' => $material,
				'quantity' => $row['mps_ordersheet_item_quantity'],
				'approved' => $row['mps_ordersheet_item_quantity_approved'],
				'shipped' => $row['mps_ordersheet_item_quantity_shipped'],
				'distributed' => $row['mps_ordersheet_item_quantity_distributed']
			);
			
			// check confirmation
			if ($OrderSheetHasConfirmedItems && $row['mps_ordersheet_item_quantity_distributed']) {
				if ($quantityShipped == $quantityDistributed) $orderSheetItemDistributed[$item] = true;
				else $orderSheetItemPartiallyDistributed[$group][$subgroup][$item] = true;
			}
		}
	}
}


//	set warehouse stock reserve id :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("
	SELECT mps_ordersheet_warehouse_id
	FROM mps_ordersheet_warehouses
	WHERE mps_ordersheet_warehouse_stock_reserve = 1 AND mps_ordersheet_warehouse_ordersheet_id = $ordersheet->id
")->fetch();

$stockReserveWarehouseID = $result['mps_ordersheet_warehouse_id'];


//	dataloader order sheet items planned :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$posPlanningQuantities = array();
$warehousePlanningQuantites = array();
$stockReserveQuantites = array();

$result = $model->query("
	SELECT DISTINCT
		mps_ordersheet_item_planned_id AS id,
		mps_ordersheet_item_planned_ordersheet_item_id AS item,
		mps_ordersheet_item_planned_posaddress_id AS pos,
		mps_ordersheet_item_planned_warehouse_id AS warehouse,
		mps_ordersheet_item_planned_quantity AS quantity,
		mps_ordersheet_warehouse_stock_reserve AS reserve
	FROM mps_ordersheet_item_planned
	INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_planned_ordersheet_item_id 
	LEFT JOIN mps_ordersheet_warehouses ON mps_ordersheet_warehouse_id = mps_ordersheet_item_planned_warehouse_id 
	WHERE mps_ordersheet_item_ordersheet_id = $ordersheet->id
")->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		$pos = $row['pos'];
		$item = $row['item'];
		$warehouse = $row['warehouse'];

		$data = array(
			'id' => $row['id'],
			'quantity' => $row['quantity']
		);
		
		// planned quantities
		if ($pos) {
			$posPlanningQuantities[$item][$pos] = $data;
		}

		// planned warehouse quantities
		if ($warehouse) {
			if ($row['reserve']) {
				$stockReserveQuantites[$item][$warehouse] = $data;
			} else {
				$warehousePlanningQuantites[$item][$warehouse] = $data;
			}
		}
	}
}
	
	
//	dataloader order sheet items distributed :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$posDistributionQuantities = array();
$posDistributionQuantitiesNotConfirmed = array();
$warehouseDistributionQuantities = array();
$warehouseDistributionQuantitiesNotConfirmed = array();
$stockReserveDistributionQuantities = array();
$stockReserveDistributionQuantitiesNotConfirmed = array();
$distribution_confirmation_dates = array();

if ($_MODE_DISTRIBUTION) { 

	$result = $model->query("
		SELECT DISTINCT
			mps_ordersheet_item_delivered_id,
			mps_ordersheet_item_delivered_posaddress_id,
			mps_ordersheet_item_delivered_warehouse_id,
			mps_ordersheet_item_delivered_quantity,
			mps_ordersheet_item_delivered_confirmed,
			DATE_FORMAT(mps_ordersheet_item_delivered_confirmed,'%d.%m.%Y') AS confirmed_date,
			mps_ordersheet_item_id,
			mps_ordersheet_item_material_id,
			mps_material_material_planning_type_id,
			mps_material_material_collection_category_id,
			mps_ordersheet_warehouse_stock_reserve
		FROM mps_ordersheet_item_delivered
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id 
		INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id 
		LEFT JOIN mps_ordersheet_warehouses ON mps_ordersheet_warehouse_id = mps_ordersheet_item_delivered_warehouse_id 
		WHERE mps_ordersheet_item_ordersheet_id = $ordersheet->id 
		ORDER BY 
			mps_ordersheet_item_delivered_confirmed DESC, 
			mps_ordersheet_item_delivered.date_created DESC
	")->fetchAll();

	if ($result) { 
		
		foreach ($result as $row) {

			$id = $row['mps_ordersheet_item_delivered_id'];
			$planning = $row['mps_material_material_planning_type_id'];
			$collection = $row['mps_material_material_collection_category_id'];
			$item = $row['mps_ordersheet_item_material_id'];
			$item = $row['mps_ordersheet_item_id'];
			$pos = $row['mps_ordersheet_item_delivered_posaddress_id'];
			$warehouse = $row['mps_ordersheet_item_delivered_warehouse_id'];
			$stockreserve = $row['mps_ordersheet_warehouse_stock_reserve'];
			$quantity = $row['mps_ordersheet_item_delivered_quantity'];
			$timestamp = ($row['confirmed_date']) ? date::timestamp($row['confirmed_date']) : 0;
			
			// build versions data
			if ($timestamp) {
				$distribution_confirmation_dates[$timestamp] = $row['confirmed_date'];
			}

			$data = array(
				'id' => $id,
				'quantity' => $quantity
			);
			
			// show quantities on required confirmed date
			if ($version) {
			
				if ($version==$timestamp) {
					
					// pos delivered quantities
					if ($pos) {
						$posDistributionQuantities[$item][$pos] = $data;
					}
					
					// warehouse delivered quantities
					if ($warehouse) {
						if ($stockreserve) $stockReserveDistributionQuantities[$item][$warehouse] = $data;
						else $warehouseDistributionQuantities[$item][$warehouse] = $data;
					}
				}
			} 
			else {
				
				// delivered quantities are once confirmed
				// sum all confirmed quantities for and set not confiremd quantities as not confirmed
				if ($_REFERENCES[$item]['distributed']) {
						
					// confirmed quantity
					if ($row['mps_ordersheet_item_delivered_confirmed']) {
						
						// pos quantities
						if ($pos) {
							$posDistributionQuantities[$item][$pos]['id'] = $id;
							$posDistributionQuantities[$item][$pos]['quantity'] = $posDistributionQuantities[$item][$pos]['quantity'] + $quantity;
						}
						
						// warehouse quantities, only last insertetd
						if ($warehouse && $quantity) {
							if ($stockreserve) $stockReserveDistributionQuantities[$item][$warehouse] = $data;
							elseif (!$warehouseDistributionQuantities[$item][$warehouse]) $warehouseDistributionQuantities[$item][$warehouse] = $data;
						}

					} else {
						
						// pos quantities
						if ($pos) {
							$posDistributionQuantitiesNotConfirmed[$item][$pos] = $data;
						}
						
						// warehouse quantities
						if ($warehouse) {
							if ($stockreserve) $stockReserveDistributionQuantitiesNotConfirmed[$item][$warehouse] = $data;
							elseif (!$warehouseDistributionQuantitiesNotConfirmed[$item][$warehouse]) $warehouseDistributionQuantitiesNotConfirmed[$item][$warehouse] = $data;
						}
					}
				}
				// not confirmed quantities
				else {
					
					// pos quantities
					if ($pos) {
						$posDistributionQuantities[$item][$pos] = $data;
					}
					
					// warehouse quantities
					if ($warehouse) {
						if ($stockreserve) $stockReserveDistributionQuantities[$item][$warehouse] = $data;
						elseif (!$warehouseDistributionQuantities[$item][$warehouse]) $warehouseDistributionQuantities[$item][$warehouse] = $data;
					}
				}
			}
		}
	}
}


// 	sap export warning messages ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sapBlockedPOS = array();
$sapDeletionPOS = array();
$sapWarningWarehouse = array();

// missing pos sap numbers
if ($ordersheet->state()->onDistribution() && $sapInvolvedPos && $company->do_data_export_from_mps_to_sap) {
	
	$sapInvolvedPos = join(',',array_keys($sapInvolvedPos));
	
	$result = $model->query("
		SELECT DISTINCT
			posaddress_id,
			posaddress_sapnumber,
			posaddress_sap_shipto_number,
			address_sap_salesorganization
		FROM db_retailnet.posaddresses
		INNER JOIN db_retailnet.addresses ON address_id = posaddress_client_id
		WHERE posaddress_id IN($sapInvolvedPos)
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
				
			$message = null;
			$pos = $row['posaddress_id'];
	
			if (!$row['posaddress_sapnumber']) {
				$message .= "The SAP customer code for this POS location is missing.";
			}
	
			if (!$row['address_sap_salesorganization']) {
				$message .= "<br \>The Sales Organisation for this Client Company is missing.";
			}
				
			if ($message) {
				$message .= "<br \>Export to SAP is not possible.";
				$sapWarningPOS[$pos] = $message;
			}
		}
	}
	
	$result = $model->query("
		SELECT DISTINCT
			posaddress_id,
			sap_imported_posaddress_order_block AS order_block,
			sap_imported_posaddress_order_block_sales_area AS order_block_sales_area,
			sap_imported_posaddress_deletion_indicator AS deletion_indicator,
			sap_imported_posaddress_deletion_sales_area_indicator AS deletion_sales_area_indicator
		FROM db_retailnet.sap_imported_posaddresses
		INNER JOIN db_retailnet.posaddresses ON posaddress_sapnumber = sap_imported_posaddress_sapnr
		WHERE posaddress_id IN($sapInvolvedPos)
		ORDER BY posaddress_id DESC
	")->fetchAll();
	
	if ($result) {

		$controlled = array();

		foreach ($result as $row) {
			
			$pos = $row['posaddress_id'];				
			
			if (!$controlled[$pos]) {
				
				$controlled[$pos] = true;

				if ($row['order_block'] || $row['order_block_sales_area'] ) {
					$sapBlockedPOS[$pos] = 'The POS is blocked for orders in SAP.<br \>Export to SAP is not possible.';
				}
				
				if ($row['deletion_indicator'] || $row['deletion_sales_area_indicator'] ) {
					$sapDeletionPOS[$pos] = 'This POS is closed in SAP..<br \>Export to SAP is not possible.';
				}
			}
		}
	}
	
}
	
// missing warehouse sap numbers
if ($ordersheet->state()->onDistribution() && $company->do_data_export_from_mps_to_sap) {
	
	$result = $model->query("
		SELECT DISTINCT
			mps_ordersheet_warehouse_id,
			CONCAT('The SAP customer code for warehouse <b>', address_warehouse_name,'</b> is missing.<br>Export to SAP is not possible.') as message
		FROM mps_ordersheet_warehouses
		INNER JOIN db_retailnet.address_warehouses ON address_warehouse_id = mps_ordersheet_warehouse_address_warehouse_id
		WHERE address_warehouse_address_id = $company->id
		AND mps_ordersheet_warehouse_stock_reserve <> 1 
		AND (address_warehouse_sap_customer_number IS NULL OR address_warehouse_sap_customer_number = '')
	")->fetchAll();
	
	$sapWarningWarehouse = _array::extract($result);
}
	


//	spreadsheet set fixed rows :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ROW_ITEM_QUANTITY = 4;
$_ROW_PLANNED_QUANTITY = 5;
$_ROW_SHIPPED_QUANTITY = $_MODE_DISTRIBUTION ? 5 : null;
$_ROW_DISTRIBUTED_QUANTITY = $_MODE_DISTRIBUTION ? 6 : null;
$_ROW_STOCK_RESERVE  = $_STATE_APPROVED && $_MODE_PLANNING ? 6 : 7;

// total quantities / total approved 
if ($_ROW_ITEM_QUANTITY) {
	$datagrid[$_ROW_ITEM_QUANTITY][$_COL_POS_CAPTION] = array(
		'value' => $_MODE_DISTRIBUTION ? 'Total Planned' : 'Total Quantity',
		'cls' => 'calcullated-caption'
	);
}

// total planned/ total shipped [5,2]
if ($_ROW_PLANNED_QUANTITY) {
	$datagrid[$_ROW_PLANNED_QUANTITY][$_COL_POS_CAPTION] = array(
		'value' => $_MODE_DISTRIBUTION ? 'Total Shipped' : 'Total Planned',
		'cls' => 'calcullated-caption'
	);
}

// row distributed [6,2]
if ($_ROW_DISTRIBUTED_QUANTITY) {
	$datagrid[$_ROW_DISTRIBUTED_QUANTITY][$_COL_POS_CAPTION] = array(
		'value' => 'Distributed',
		'cls' => 'calcullated-caption'
	);	
}


// row stock reserve [7,2]
if ($_ROW_STOCK_RESERVE  && !$_STATE_DISTRIBUTED) {	
	$datagrid[$_ROW_STOCK_RESERVE ][$_COL_POS_CAPTION] = array(
		'value' => 'Stock reserve',
		'cls' => 'calcullated-caption'
	);
} else $_ROW_STOCK_RESERVE  = null;

$totalFixedRows =$_ROW_STOCK_RESERVE ;
$totalFixedCols = 3;
$_LAST_FIXED_ROW = $totalFixedRows;
$_LAST_FIXED_COL = $totalFixedCols;

//	spreadsheet items caption area :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$totalItemColumns = 0;

if ($OrderSheetItems) {
	
	$col = $totalFixedCols;
						
	foreach ($OrderSheetItems as $group_id => $group) {

		// group caption
		$groupCaption = htmlspecialchars($group['caption'], ENT_QUOTES);

		foreach ($group['data'] as $subgroup_id => $subgroup) {
			
			// count subgroup items
			$totalSubgroupColumns = 0;

			// subgroup caption
			$subgroupCaption = htmlspecialchars($subgroup['caption'], ENT_QUOTES);

			// group index
			$col++;
			$datagrid[$_ROW_GROUP_CAPTION][$col] = array(
				'id' => $group_id,
				'subgroup' => $subgroup_id,
				'value' => "<strong>$groupCaption</strong><span>$subgroupCaption</span>",
				'cls' => 'col-caption'
			);

			$index = $col;
			
			foreach ($subgroup['data'] as $id => $item) {
				
				// planned or approved quantities
				$totalSubgroupColumns++;

				$ico = "<span class=icon-holder ><span class=\"icon info-full-circle infotip \" rel=\"material-info\" data=\"{$item[item]}\" ></span></span>";

				// column planned quantties
				$datagrid[$_ROW_ITEM_CAPTION][$col] = array(
					'id' => $id,
					'value' => "$ico<span class=\"code\">{$item[code]}</span>",
					'cls' => 'item-caption',
					'column' => COLUMN_PLANNING
				);

				if ($_MODE_DISTRIBUTION) {
					
					$col++;
					$totalSubgroupColumns++;
					
					// column distributed quantitites
					$datagrid[$_ROW_ITEM_CAPTION][$col]['id'] = $id;
					$datagrid[$_ROW_ITEM_CAPTION][$col]['column'] = COLUMN_DISTRIBUTION;
					$datagrid[$_ROW_ITEM_CAPTION][$col-1]['merge'] = 2;
				
					if ($orderSheetItemPartiallyDistributed[$group_id][$subgroup_id][$id]) {
						
						$col++;
						$totalSubgroupColumns++;
						
						// column partially distributed quantities
						$datagrid[$_ROW_ITEM_CAPTION][$col]['id'] = $id;
						$datagrid[$_ROW_ITEM_CAPTION][$col]['column'] = COLUMN_PARTIALY_CONFIRMED;
						$datagrid[$_ROW_ITEM_CAPTION][$col-2]['merge'] = 3;
					}
				}

				$col++;
			}

			if ($totalSubgroupColumns > 1) {
				$datagrid[$_ROW_GROUP_CAPTION][$index]['merge'] = $totalSubgroupColumns;
			}

			$col--;
			$col = $col + $_GROUP_SEPARATOR;

			$totalItemColumns = $totalItemColumns  + $totalSubgroupColumns + $_GROUP_SEPARATOR;
		}
	}

	// set total spreadsheet columns
	$totalItemColumns = $_GROUP_SEPARATOR ? $totalItemColumns - $_GROUP_SEPARATOR : $totalItemColumns;
}

// items area triggers
$_FIRST_ITEM_COL = $totalFixedCols + 1;
$_LAST_ITEM_COL = $totalFixedCols + $totalItemColumns;


// spreadsheet pos locations area ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


$seprow = 0;
$totalPosRows = 0;

if ($posLocations) {

	$row = $totalFixedRows;

	foreach ($posLocations as $type_id => $types) {

		$row++;
		$totalPosRows++;

		$datagrid[$row][$_COL_POS_ICON] = array(
			'type' => $type_id,
			'value' => "<span class=\"arrow arrow_$type_id arrow-down type_$type_id\"  data-type=\"$type_id\" data-row=\"$row\" ></span>",
			'cls' => 'cel postype row-caption'
		);

		$datagrid[$row][$_COL_POS_CAPTION] = array(
			'type' => $type_id,
			'value' => $types['name'],
			'cls' => 'cel postype row-caption'
		);

		foreach ($types['data'] as $pos_id => $pos) {
			
			$row++;
			$totalPosRows++;

			$warningIcon = null;

			$datagrid[$row][$_COL_POS_ICON] = array(
				'id' => $pos_id,
				'value' => "<span class=\"icon info-full-circle infotip \" rel=\"pos-info\" data=\"$pos_id\" ></span>",
				'cls' => 'cel postype'
			);

			$caption = $pos['name'];
			$caption = strlen($caption)>50 ? substr($caption, 0, 50).'..' : $caption;
			$caption = htmlspecialchars($caption, ENT_QUOTES);

			if ($sapDeletionPOS[$pos_id]) $warningIcon = "<i class=\"fa fa-ban infotip warrning\" data-title=\"{$sapDeletionPOS[$pos_id]}\"></i>";
			elseif ($sapBlockedPOS[$pos_id]) $warningIcon = "<i class=\"fa fa-ban infotip warrning\" data-title=\"{$sapBlockedPOS[$pos_id]}\"></i>";
			elseif ($sapWarningPOS[$pos_id]) $warningIcon = "<i class=\"fa fa-exclamation-triangle infotip warrning\" data-title=\"{$sapWarningPOS[$pos_id]}\"></i>";

			$datagrid[$row][$_COL_POS_CAPTION] = array(
				'id' => $pos_id,
				'value' => "<span class=\"caption type_$type_id\" data-type=\"$type_id\" data-row=\"$row\" >$caption</span> $warningIcon",
				'cls' => 'cel postype'
			);
		}

		$row = $row + $seprow;
	}

	$row = $seprow ? $row-$seprow : $row;
}

// pos area triggers
$_FIRST_POS_ROW = $totalFixedRows+1;
$_LAST_POS_ROW = $row;


// spreadsheet warehouses rows :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$separea = 0;
$totalWarehouseRows = 0;

if ($_AREA_WAREHOUSE) {

	$row = $totalFixedRows + $totalPosRows + $separea + 1;
	
	$_ROW_WAREHOUSE_CAPTION = $row;

	$totalWarehouseRows++;

	if ($_STATE_EDIT) {

		$datagrid[$row][$_COL_POS_ICON] = array(
			'value' => "<a id=\"add_warehouse\" class=\"icon add\" href=\"#add_warehouse_dialog\" ></a>",
			'cls' => 'cel row-caption'
		);

		$datagrid[$row][$_COL_POS_CAPTION] = array(
			'value' => 'Warehouses',
			'cls' => 'cel row-caption'
		);

	} else {

		$datagrid[$row][$_COL_POS_ICON] = array(
			'value' => 'Warehouses',
			'merge' => 2,
			'cls' => 'cel row-caption'
		);
	}

	// load all active company warehouses
	$company_warehouses = $ordersheet->company()->warehouse()->load();
	$company_warehouses = _array::datagrid($company_warehouses);

	// load all ordersheet registred warehouses
	$ordersheet_warehouses = $ordersheet->warehouse()->load();

	if ($ordersheet_warehouses) {

		foreach ($ordersheet_warehouses as $value) {
			
			$warehouse = $value['mps_ordersheet_warehouse_address_warehouse_id'];

			if (!$value['mps_ordersheet_warehouse_stock_reserve']) {

				$row++;
				$totalWarehouseRows++;

				// first warehouse row
				if (!$_FIRST_WAREHOUSE_ROW) $_FIRST_WAREHOUSE_ROW = $row;

				$warehouseName = $company_warehouses[$warehouse]['address_warehouse_name'];
				$key = $value['mps_ordersheet_warehouse_id'];

				$warehouseName = str_replace("'", "", $company_warehouses[$warehouse]['address_warehouse_name']);
				$warehouseName = strlen($warehouseName)>50 ? substr($warehouseName, 0, 50).'..' : $warehouseName;

				$warningIcon = ($sapWarningWarehouse[$key]) ? "<i class=\"fa fa-exclamation-triangle infotip warrning\" data-title=\"{$sapWarningWarehouse[$key]}\"></i>" : null;

				if ($_STATE_EDIT) {

					$datagrid[$row][$_COL_POS_ICON] = array(
						'id' =>  $key,
						'value' => "<span class=\"icon cancel remove-warehouse\" data-row=\"$row\" data-warehouse=\"$key\"  ></span>",
						'cls' => 'cel warehouse'
					);

					$datagrid[$row][$_COL_POS_CAPTION] = array(
						'id' =>  $key,
						'value' => $warehouseName.$warningIcon,
						'cls' => 'cel warehouse'
					);

				} else {
					$datagrid[$row][$_COL_POS_ICON] = array(
						'id' =>  $key,
						'value' => $warehouseName.$warningIcon,
						'merge' => 2,
						'cls' => 'cel warehouse'
					);
				}
			}
		}

		// last warehouse row
		if ($_FIRST_WAREHOUSE_ROW) $_LAST_WAREHOUSE_ROW = $row;
	}
}
	
// spreadsheet builder :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$totalSpreadsheetRows = $totalFixedRows + $totalPosRows + $totalWarehouseRows;
$totalSpreadsheetCols = $totalFixedCols + $totalItemColumns;
$_LAST_SPREADSHEET_ROW = $totalSpreadsheetRows;
$_LAST_SPREADSHEET_COL = $totalSpreadsheetCols;
	

$grid = array();
$mergecel = array();

if ($totalSpreadsheetRows && $totalSpreadsheetCols) {

	for ($row=1; $row <= $totalSpreadsheetRows; $row++) {
			
		for ($col=1; $col <= $totalSpreadsheetCols; $col++) {

			// index
			$index = spreadsheet::key($col, $row);

			// merge cel index
			if ($datagrid[$row][$col]['merge']) {
				$mergecel[$index] = $datagrid[$row][$col]['merge'];
			}

			switch ($row) {
				
				case $_ROW_BLANK ;
					
					if ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {
						$grid[$index]['cls'] = $datagrid[$_ROW_ITEM_CAPTION][$col]['id'] ? null : 'col-separator';
					}
					
					$grid[$index]['html'] = "&nbsp;";

				break;
				
				case $_ROW_GROUP_CAPTION:		// PLANNING TYPE CAPTION
				case $_ROW_ITEM_CAPTION:		// MATERIAL CAPTION

					if ($col==$_COL_POS_CAPTION) {
						/*
						$mergecel[$index] = $total_fixed_columns;
						$grid[$index]['html'] = ($filerCaptions) ? 'Filter(s): '.join(', ', $filerCaptions) : null;
						$grid[$index]['cls'] = "filters";
						*/
					}
					elseif ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {
						
						$item = $datagrid[$_ROW_ITEM_CAPTION][$col]['id'];

						if ($item) {

							$cls = " cel";
							
							if ($datagrid[$row][$col]['value']) {
								$grid[$index]['html'] = $datagrid[$row][$col]['value'];
							}
						
							$grid[$index]['cls'] = $datagrid[$row][$col]['cls'].$cls;
						}
					}
						
				break;

				// ITEM QUANTITY OVER ALL POS
				case $_ROW_ITEM_QUANTITY:

					if ($col==$_COL_POS_CAPTION) {

						$grid[$index]['text'] = $datagrid[$row][$col]['value'];
						$grid[$index]['cls'] = $datagrid[$row][$col]['cls'];

					} elseif ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {

						$item = $datagrid[$_ROW_ITEM_CAPTION][$col]['id'];

						if ($item) {

							$column = $datagrid[$_ROW_ITEM_CAPTION][$col]['column'];

							// COLUMN PLANNING
							if ($column==COLUMN_PLANNING) {

								$cls = " cel";

								if ($_MODE_PLANNING) {
									$cls .= ' calculated';
									$celTotalPlanned = spreadsheet::key($col, $_ROW_PLANNED_QUANTITY);
									$celStockReserve = spreadsheet::key($col, $_ROW_STOCK_RESERVE);
									$grid[$index]['text'] = "function($celTotalPlanned, $celStockReserve) { return $celTotalPlanned+$celStockReserve || null; }"; // output: total planned + stock reserve
								}

								if ($_MODE_DISTRIBUTION) {
									$cls .= ' planned-readonly';
									$grid[$index]['text'] = $_REFERENCES[$item]['approved'];
								}

								$grid[$index]['cls'] = $datagrid[$row][$col]['cls'].$cls;

							} // END COLUMN PLANNING
						}
					}

				break;

				case $_ROW_PLANNED_QUANTITY : // mode planning: total planned quantity (function)

					if ($col==$_COL_POS_CAPTION) {
						$grid[$index]['text'] = $datagrid[$row][$col]['value'];
						$grid[$index]['cls'] = $datagrid[$row][$col]['cls'];
					}
					elseif ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {
						
						$item = $datagrid[$_ROW_ITEM_CAPTION][$col]['id'];

						if ($item) {

							$column = $datagrid[$_ROW_ITEM_CAPTION][$col]['column'];
							
							// COLUMN PLANNING
							if ($column==COLUMN_PLANNING) {

								$cls = " cel";
									
								if ($_MODE_PLANNING) {
									$range = spreadsheet::key($col,$_FIRST_POS_ROW).'_'.spreadsheet::key($col, $_LAST_SPREADSHEET_ROW);
									$grid[$index]['text']  = "function($range) { return sum($range) || null }"; // sum planned quantities (pos + warehouses)
								}

								if ($_MODE_DISTRIBUTION) {
									$cls .= ' planned-readonly';
									$grid[$index]['text'] = $_REFERENCES[$item]['shipped'];
								}

								$grid[$index]['cls'] = $datagrid[$row][$col]['cls'].$cls;

							} // END COLUMN PLANNING
						}
					}

				break;

				case $_ROW_DISTRIBUTED_QUANTITY:

					if ($col==$_COL_POS_CAPTION) {
						$grid[$index]['text'] = $datagrid[$row][$col]['value'];
						$grid[$index]['cls'] = $datagrid[$row][$col]['cls'];
					}
					elseif ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {
						
						$item = $datagrid[$_ROW_ITEM_CAPTION][$col]['id'];

						if ($item) {

							$cls = null; 
							$column = $datagrid[$_ROW_ITEM_CAPTION][$col]['column'];

							// COLUMN DISTRIBUTION
							if ($column==COLUMN_DISTRIBUTION) {
								
								$cls = " cel";

								$value = $_REFERENCES[$item]['distributed'];
								$grid[$index]['text'] = $_REFERENCES[$item]['distributed'];

								if ($_REFERENCES[$item]['distributed'] > 0) {
									$cls .= ' delivered-readonly';
								}

							} // END COLUMN DISTRIBUTION


							// COLUMN PARTIALLY DISTRIBUTION
							if ($column==COLUMN_PARTIALY_CONFIRMED) {
								
							} // END COLUMN PARTIALLY DISTRIBUTION

							$grid[$index]['cls'] = $datagrid[$row][$col]['cls'].$cls;
						}

					}
					
				break;

				case $_ROW_STOCK_RESERVE :

					// stock reserve caption
					if ($col==$_COL_POS_CAPTION) {
						$grid[$index]['text'] = $datagrid[$row][$col]['value'];
						$grid[$index]['cls'] = $datagrid[$row][$col]['cls'];
					}
					elseif ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {
						
						$item = $datagrid[$_ROW_ITEM_CAPTION][$col]['id'];

						if ($item) {

							// references
							$column = $datagrid[$_ROW_ITEM_CAPTION][$col]['column'];
							$warehouse = $stockReserveWarehouseID;
							$quantity = $stockReserveQuantites[$item][$warehouse]['quantity'] ?: null;

							$cls = null;

							// COLUMN PLANNING
							if ($column==COLUMN_PLANNING) {

								$cls = " cel";

								if ($_MODE_PLANNING) {

									$cls .= ' reserve';
	
									if ($_STOCK_RESERVE_EDIT) {;
										
										$grid[$index]['text'] = $quantity;

										// tag pattern: section;quantitty_id;warehouse_id;material_id
										$material = $_REFERENCES[$item]['material'];
										$id = $stockReserveQuantites[$item][$warehouse]['id'];
										$grid[$index]['tag'] = "reserve-planned;$id;$warehouse;$material";

									} else {

										if ($_STATE_APPROVED) {
											// stock reserve: total approved quantites - sum of planned pos quantities
											$approved = $_REFERENCES[$item]['approved'];
											$cel = spreadsheet::key($col,$_ROW_PLANNED_QUANTITY);
											$grid[$index]['text'] = "function($cel) { return $approved-$cel || null; }";
										} else {
											$grid[$index]['text'] = $quantity;
										}
										
										$grid[$index]['readonly'] = CELL_READONLY;
									}
								}

								if ($_MODE_DISTRIBUTION) {
									$cls .= " planned-readonly";
									$grid[$index]['text'] = $quantity;
								}

							} // END COLUMN PLANNING


							// COLUMN DISTRIBUTION
							if ($column == COLUMN_DISTRIBUTION && !$_REFERENCES[$item]['distributed']) {

								$cls = " cel";

								$shipped = $_REFERENCES[$item]['shipped'];
								$range = spreadsheet::key($col,$_FIRST_POS_ROW).'_'.spreadsheet::key($col, $_LAST_SPREADSHEET_ROW);
								$grid[$index]['text']  = "function($range) {return $shipped-sum($range) || null; } ";
								
							} // END COLUMN DISTRIBUTION


							// COLUMN PARTIALLY DISTRIBUTION
							if ($column == COLUMN_PARTIALY_CONFIRMED && $_REFERENCES[$item]['distributed']) {

								$cls = " cel";

								$shipped = $_REFERENCES[$item]['shipped'] - $_REFERENCES[$item]['distributed'];
								$range = spreadsheet::key($col,$_FIRST_POS_ROW).'_'.spreadsheet::key($col, $_LAST_SPREADSHEET_ROW);
								$grid[$index]['text']  = "function($range) {return $shipped-sum($range) || null; } ";

							} // END COLUMN PARTIALLY DISTRIBUTION

							$grid[$index]['cls'] = $datagrid[$row][$col]['cls'].$cls;

						}
					}

				break;

				case $row >= $_FIRST_POS_ROW && $row <= $_LAST_POS_ROW:

					if ($col < $_FIRST_ITEM_COL) {

						$grid[$index]['html'] = $datagrid[$row][$col]['value'];
						$grid[$index]['cls'] = $datagrid[$row][$col]['cls'];

						$readonly = CELL_READONLY;

					} elseif ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {
						
						$readonly = false;

						// references
						$item = $datagrid[$_ROW_ITEM_CAPTION][$col]['id'];
						$pos = $datagrid[$row][$_COL_POS_CAPTION]['id'];

						if ($item && $pos) {

							$column = $datagrid[$_ROW_ITEM_CAPTION][$col]['column'];
							$nextColumn = $datagrid[$_ROW_ITEM_CAPTION][$col+1]['column'];
							$material = $_REFERENCES[$item]['material'];
							
							$cls = " cel";

							$nextColumn = $datagrid[$_ROW_ITEM_CAPTION][$col+1]['column'];

							// cel attribtes
							$grid[$index]['hiddenAsNull'] = 'true';
							$grid[$index]['numeric'] = 'true';


							// COLUMN PLANNING
							if ($column == COLUMN_PLANNING) {

								$grid[$index]['text'] = $posPlanningQuantities[$item][$pos]['quantity'];

								if ($_MODE_PLANNING) { 

									if ($_STATE_EDIT) {

										if ($_VALIDATA_TOTAL_ITEM_QUANTITY) {
											$celReserve = spreadsheet::key($col, $_ROW_STOCK_RESERVE);
										}
										
										// TAG: [section] [item] [pos] [material] [total] [used]
										$id = $posPlanningQuantities[$item][$pos]['id'];
										$grid[$index]['tag'] = "$column;$id;$pos;$material;$celReserve";

									} else {
										$readonly = CELL_READONLY;
									}
								}

								if ($_MODE_DISTRIBUTION) {
									$readonly = CELL_READONLY;
									$cls .= ' planned-readonly';
								}

							} // END COLUMN PLANNING
							

							// COLUMN DISTRIBUTION
							if ($column == COLUMN_DISTRIBUTION) {

								if( $version || $orderSheetItemDistributed[$item]) {
									
									$cls .= ' delivered-readonly';
									$readonly = CELL_READONLY;
									
									$quantity = $version ? $DistributedVersionQuantity[$item][$pos] : $orderSheetItemDistributed[$item]['distributed'];
									$grid[$index]['text'] = $quantity;
								} 
								else {

									$grid[$index]['text'] = $posDistributionQuantities[$item][$pos]['quantity'];

									if ($_STATE_EDIT) {
										
										if ($_VALIDATA_TOTAL_ITEM_QUANTITY) {
											$celReserve = spreadsheet::key($col, $_ROW_STOCK_RESERVE);
										}
									
										// TAG: [section] [item] [pos] [material] [total] [planned] [reserve]
										$id = $posDistributionQuantities[$item][$pos]['id'];
										$grid[$index]['tag'] = "$column;$id;$pos;$material;$celReserve";

										$readonly = $datagrid[$_ROW_ITEM_CAPTION][$col+1]['column']==COLUMN_PARTIALY_CONFIRMED ? CELL_READONLY : false;

									} else {
										$readonly = CELL_READONLY;
									}

									 if ($nextColumn == COLUMN_PARTIALY_CONFIRMED) {
									 	$cls .= ' delivered-readonly';
									 }
								}

							} // END COLUMN DISTRIBUTION
							

							// COLUMN PARTIALLY DISTRIBUTION
							if ($column == COLUMN_PARTIALY_CONFIRMED) {

								$grid[$index]['text'] = $posDistributionQuantitiesNotConfirmed[$item][$pos]['quantity'];

								if ($_STATE_EDIT) {

									if ($_VALIDATA_TOTAL_ITEM_QUANTITY) {
										$celReserve = spreadsheet::key($col, $_ROW_STOCK_RESERVE);
									}
									
									// TAG: [section] [item] [pos] [material] [total] [planned] [reserve]
									$id = $posDistributionQuantitiesNotConfirmed[$item][$pos]['id'];
									$grid[$index]['tag'] = "$column;$id;$pos;$material;$celReserve";

								} else {
									$readonly = CELL_READONLY;
								}


							} // END PARTIALLY COLUMN DISTRIBUTION


							$grid[$index]['cls'] = $datagrid[$row][$col]['cls'].$cls;

						} // END ITEM CEL 
						else {
							$readonly = CELL_READONLY;
						}

						$grid[$index]['readonly'] = $readonly;
					}

				break;

				case $_ROW_WAREHOUSE_CAPTION:

					if ($datagrid[$row][$col]) {
						$grid[$index]['html'] = $datagrid[$row][$col]['value'];
						$grid[$index]['cls'] = $datagrid[$row][$col]['cls'];
					}

					$grid[$index]['readonly']  = CELL_READONLY;

				break;

				case $row >= $_FIRST_WAREHOUSE_ROW && $row <= $_LAST_WAREHOUSE_ROW:

					if ($col < $_FIRST_ITEM_COL) {
						$grid[$index]['html'] = $datagrid[$row][$col]['value'];
						$grid[$index]['cls'] = $datagrid[$row][$col]['cls'];
					}
					elseif ($col >= $_FIRST_ITEM_COL && $col <= $_LAST_ITEM_COL) {
						
						// references
						$readonly = false;
						$item = $datagrid[$_ROW_ITEM_CAPTION][$col]['id'];
						$warehouse = $datagrid[$row][$_COL_POS_CAPTION]['id'];

						if ($item && $warehouse) {

							// references
							$column = $datagrid[$_ROW_ITEM_CAPTION][$col]['column'];
							$nextColumn = $datagrid[$_ROW_ITEM_CAPTION][$col+1]['column'];
							$material = $_REFERENCES[$item]['material'];

							$cls = " cel";

							$grid[$index]['hiddenAsNull'] = 'true';
							$grid[$index]['numeric'] = 'true';
							
							// COLUMN PLANNING
							if ($column == COLUMN_PLANNING) {

								$grid[$index]['text'] = $warehousePlanningQuantites[$item][$warehouse]['quantity'];

								if ($_MODE_PLANNING) { 

									if ($_STATE_EDIT) {
										
										if ($_VALIDATA_TOTAL_ITEM_QUANTITY) {
											$celReserve = spreadsheet::key($col, $_ROW_STOCK_RESERVE);
										}

										// TAG: [section] [quantity_id] [warehouse] [material] [total] [used]
										$id = $warehousePlanningQuantites[$item][$warehouse]['id'];
										$grid[$index]['tag'] = "reserve-$column;$id;$warehouse;$material;$celReserve";

										$readonly = $datagrid[$_ROW_ITEM_CAPTION][$col+1]['column']==COLUMN_DISTRIBUTION ? CELL_READONLY : false;

									} else {
										$readonly = CELL_READONLY;
									}
								}

								if ($_MODE_DISTRIBUTION) {
									$cls .= ' planned-readonly';
									$readonly = CELL_READONLY;
								}

							} // END COLUMN PLANNING


							// COLUMN DISTRIBUTION
							if ($column == COLUMN_DISTRIBUTION) {

								$grid[$index]['text'] = $warehouseDistributionQuantities[$item][$warehouse]['quantity'];

								if ($_STATE_EDIT) {

									if ($_VALIDATA_TOTAL_ITEM_QUANTITY) {
										$celReserve = spreadsheet::key($col, $_ROW_STOCK_RESERVE);
									}

									// TAG: [section] [quantity_id] [warehouse] [material] [total] [used]
									$id = $warehouseDistributionQuantities[$item][$warehouse]['id'];
									$grid[$index]['tag'] = "reserve-$column;$id;$warehouse;$material;$celReserve";

									$readonly = $datagrid[$_ROW_ITEM_CAPTION][$col+1]['column']==COLUMN_PARTIALY_CONFIRMED ? CELL_READONLY : false;

								} else {
									$readonly = CELL_READONLY;
								}

								if ($nextColumn == COLUMN_PARTIALY_CONFIRMED) {
								 	$cls .= ' delivered-readonly';
								 }

							} // END COLUMN DISTRIBUTION


							// COLUMN PARTIALLY DISTRIBUTION
							if ($column == COLUMN_PARTIALY_CONFIRMED) {

								$grid[$index]['text'] = $warehouseDistributionQuantitiesNotConfirmed[$item][$warehouse]['quantity'];

								if ($_STATE_EDIT) {

									if ($_VALIDATA_TOTAL_ITEM_QUANTITY) {
										$celReserve = spreadsheet::key($col, $_ROW_STOCK_RESERVE);
									}

									// TAG: [section] [quantity_id] [warehouse] [material] [total] [used]
									$id = $warehouseDistributionQuantitiesNotConfirmed[$item][$warehouse]['id'];
									$grid[$index]['tag'] = "reserve-$column;$id;$warehouse;$material;$celReserve";

								} else {
									$readonly = CELL_READONLY;
								}

							} // END COLUMN PARTIALLY DISTRIBUTION


							$grid[$index]['cls'] = $datagrid[$row][$col]['cls'].$cls;

						} // END ITEM CEL 
						else {
							$readonly = CELL_READONLY;
						}

					} // END POS ROW

					$grid[$index]['readonly'] = $readonly;

				break;
								
				default:
					$grid[$index]['readonly']  = CELL_READONLY;
					//$grid[$index]['html'] = "&nbsp;";
				break;

			}
		}
	}	
}

// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!headers_sent()) {
	header('Content-Type: text/json');
}

echo json_encode(array(
	'response' => true,
	'data' => $grid,
	'datagrid' => $datagrid,
	'source' => $stockReserveQuantites,
	'merge' => $mergecel,
	'top' => $_MODE_DISTRIBUTION ? 7 : 6,
	'left' => 3
));
