<?php
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$auth = User::instance();
$translate = Translate::instance();

// request vars
$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['user_id'];

$User = new User($id);

$openAccount = $id ? false : true;
$closeAccount = $User->active && in_array('user_active', $fields) && !$_REQUEST['user_active'] ? true : false;
$sendMailChange = $openAccount || $closeAccount ? false : true;

$data = array();

// user_active
$user_set_to_inactive = false;
if (in_array('user_active', $fields)) {
	
	$data['user_active'] = (isset($_REQUEST['user_active'])) ? 1 : 0;
	
	if (!$_REQUEST['user_active']) {
		$data['user_set_to_inactive_by'] = $auth->id;
		$data['user_set_to_inactive_date'] = date('Y-m-d H:i:s');

		$user_set_to_inactive = true;
	}
}

// user address
if ($controller=='users' && $User->id && $_REQUEST['company'] ) {
	$company = $_REQUEST['company'];
	$data['user_address'] = $company;
	$redirect = $User->address <> $_REQUEST['company'] ? "/administration/users/users/$company/$id" : null;
}
elseif (isset($_REQUEST['user_address'])) {
	$data['user_address'] = $_REQUEST['user_address'];
}

// user_name
if (isset($_REQUEST['user_name'])) {
	$data['user_name'] = $_REQUEST['user_name'];
}
// user_firstname
if (isset($_REQUEST['user_firstname'])) {
	$data['user_firstname'] = $_REQUEST['user_firstname'];
}
// user_description
if (isset($_REQUEST['user_description'])) {
	$data['user_description'] = $_REQUEST['user_description'];
}
// user_sex
if (isset($_REQUEST['user_sex'])) {
	$data['user_sex'] = $_REQUEST['user_sex'];
}


if (isset($_REQUEST['user_phone_number'])) {
$_REQUEST['user_phone_number'] = str_replace(' ', '', $_REQUEST['user_phone_number']);
}

if (isset($_REQUEST['user_phone_area'])) {
	$_REQUEST['user_phone_area'] = str_replace(' ', '', $_REQUEST['user_phone_area']);
}

if (isset($_REQUEST['user_phone_country'])) {
	$_REQUEST['user_phone_country'] = str_replace(' ', '', $_REQUEST['user_phone_country']);
}

if(isset($_REQUEST['user_phone_number']) 
	and $_REQUEST['user_phone_number']) {
	// user phone country code
	if (isset($_REQUEST['user_phone_country'])) {
		$data['user_phone_country'] = $_REQUEST['user_phone_country'];
	}

	// user phone area code
	if (isset($_REQUEST['user_phone_area'])) {
		$data['user_phone_area'] = $_REQUEST['user_phone_area'];
	}

	// user phone number
	if (isset($_REQUEST['user_phone_number'])) {
		$data['user_phone_number'] = $_REQUEST['user_phone_number'];
	}

	// user phone
	if (isset($_REQUEST['user_phone_country']) && isset($_REQUEST['user_phone_area']) && isset($_REQUEST['user_phone_number'])) {
		
		if($_REQUEST['user_phone_area']) {
			$data['user_phone'] = $_REQUEST['user_phone_country']. ' ' . $_REQUEST['user_phone_area']. ' ' . $_REQUEST['user_phone_number'];
		}
		else {
			$data['user_phone'] = $_REQUEST['user_phone_country']. ' ' . $_REQUEST['user_phone_number'];
		}
	}
}

if (isset($_REQUEST['user_mobile_phone_number'])) {
$_REQUEST['user_mobile_phone_number'] = str_replace(' ', '', $_REQUEST['user_mobile_phone_number']);
}

if (isset($_REQUEST['user_mobile_phone_area'])) {
	$_REQUEST['user_mobile_phone_area'] = str_replace(' ', '', $_REQUEST['user_mobile_phone_area']);
}

if (isset($_REQUEST['user_mobile_phone_country'])) {
	$_REQUEST['user_mobile_phone_country'] = str_replace(' ', '', $_REQUEST['user_mobile_phone_country']);
}

if(isset($_REQUEST['user_mobile_phone_number']) 
	and $_REQUEST['user_mobile_phone_number']) {
	// user mobile phone country code
	if (isset($_REQUEST['user_mobile_phone_country'])) {
		$data['user_mobile_phone_country'] = $_REQUEST['user_mobile_phone_country'];
	}

	// user mobile phone area code
	if (isset($_REQUEST['user_mobile_phone_area'])) {
		$data['user_mobile_phone_area'] = $_REQUEST['user_mobile_phone_area'];
	}

	// user mobile phone number
	if (isset($_REQUEST['user_mobile_phone_number'])) {
		$data['user_mobile_phone_number'] = $_REQUEST['user_mobile_phone_number'];
	}

	// user mobile phone
	if (isset($_REQUEST['user_mobile_phone_country']) && isset($_REQUEST['user_mobile_phone_area']) && isset($_REQUEST['user_mobile_phone_number'])) {
		
		if($_REQUEST['user_mobile_phone_area']) {
			$data['user_mobile_phone'] = $_REQUEST['user_mobile_phone_country']. ' ' . $_REQUEST['user_mobile_phone_area']. ' ' . $_REQUEST['user_mobile_phone_number'];
		}
		else {
			$data['user_mobile_phone'] = $_REQUEST['user_mobile_phone_country']. ' ' . $_REQUEST['user_mobile_phone_number'];
		}
	}
}

// user_email
if (isset($_REQUEST['user_email'])) {
	$data['user_email'] = $_REQUEST['user_email'];
}

// user_email
if (isset($_REQUEST['invitation_email'])) {
	$data['user_email'] = $_REQUEST['invitation_email'];
}

// user_email_cc
if (isset($_REQUEST['user_email_cc'])) {
	
	$data['user_email_cc'] = $_REQUEST['user_email_cc'];
	
	if ($sendMailChange && ($User->email_cc || $data['user_email_cc'])) {
		$userCCMailChanged = $User->email_cc <> $data['user_email_cc'] ? true : false;
	}
}

// user_email_deputy
if (isset($_REQUEST['user_email_deputy'])) {
	
	$data['user_email_deputy'] = $_REQUEST['user_email_deputy'];
	
	if ($sendMailChange && ($User->email_deputy || $data['user_email_deputy'])) {
		$userDeputyMailChanged = $User->email_deputy <> $data['user_email_deputy'] ? true : false;
	}
}

// user_login
if (isset($_REQUEST['user_login'])) {
	$data['user_login'] = $_REQUEST['user_login'];
}

// user_phone
if (isset($_REQUEST['user_password'])) {
	$data['user_password'] = $_REQUEST['user_password'];
}

if ($action=='invitation') {

	$db = Connector::get();
	$email = $_REQUEST['invitation_email'];
	$sth = $db->prepare("SELECT * FROM user_invitations WHERE user_invitation_email = ? ");
	$sth->execute(array($email));
	$res = $sth->fetch();

	if (!$res) {
		$message = $translate->error_invitation_request_user_account;
		$response = false;
		$data = null;
	}
}

if ($data) {

	$canAddUserRoles = true;
	$canAddNewUser = $id ? false : true;
	
	if ($id) {
		$data['user_modified'] = $auth->login;
		$data['date_modified'] = date('Y-m-d H:i:s');

		$data['user_last_modified_by'] = $auth->id;
		$data['user_last_modified'] = date('Y-m-d H:i:s');

		$response = $User->update($data);
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}
	else {

		// invitation for request to open account
		if ($action=='invitation') {
			$db = Connector::get();
			$sth = $db->prepare("SELECT user_id FROM users WHERE user_email = ?");
			$sth->execute(array($data['user_email']));
			$res = $sth->fetch();
			$id = $res['user_id'];
			$canAddNewUser = $id ? false : true;
			$response = $canAddNewUser ? false : true;
			$canAddUserRoles = $id ? false : true;
		}
		
		if ($canAddNewUser) {
			$data['user_created'] = $auth->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$data['user_added_by'] = $auth->id;
			$response = $id = $User->create($data);
			$message = $response ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect'] ? $_REQUEST['redirect'].'/'.$id : null;
		}

		if ($response) {
			
			//$message = $response ? Message::request_inserted() : $translate->message_request_failure;
			if ($action=='invitation') $redirect = "/messages/show/invitation_request_user_account";
			else $redirect = $_REQUEST['redirect'] ? $_REQUEST['redirect'].'/'.$id : null;
		}
	}


	// selected roles in form
	$mpsRoles = $_REQUEST['roles'] ?: array();
	$lpsRoles = $_REQUEST['lps_roles'] ?: array();
	$newsRoles = $_REQUEST['news_roles'] ?: array();
	$selectedRoles = $mpsRoles + $lpsRoles + $newsRoles;

	
	if ($response && $canAddUserRoles) {
		
		$user_role = new User_Role();
		$user_role->user($id);
		
		// used roles in form
		$used_roles = unserialize($_REQUEST['used_roles']); 
		
		// remove all user roles used in form
		if ($used_roles &&  $action<>'invitation') {
			foreach ($used_roles as $role) {
				$user_role->delete($role);
			}
		}
			
		// add new roles
		if ($selectedRoles) {
			foreach ($selectedRoles as $role => $value) {
				$user_role->create($role);
			}
		}
	}

	if ($response && $action=='invitation') {

		$db = Connector::get();

		$email = $_REQUEST['invitation_email'];
		$db->exec("DELETE FROM user_invitations WHERE user_invitation_email = '$email' ");

		$mail = new ActionMail(12);

		$mail->setParam('id', $id);		
		$mail->setParam('invitation', true);		
		$mail->setParam('requestedRoles', $selectedRoles);	

		$mail->setSender($id);

		if (!$canAddNewUser) {

			$footerContent = "\r\n\n Attention, this user already has an active account.";

			if (!$canAddUserRoles && $selectedRoles) {
				
				$sth = $db->prepare("SELECT role_id, role_name FROM roles");
				$sth->execute();
				$res = $sth->fetchAll();

				$roles = array();

				if ($res) {
					foreach ($res as $row) {
						$i = $row['role_id'];
						if ($selectedRoles[$i]) $roles[] = $row['role_name'];
					}
				}

				$footerContent .= "\r\n\nPlease assign the requeste roles to that user.\r\n\nRoles: ".join(', ', $roles);
			}

			$mail->setFooter($footerContent);
		}
		
		// send mails
		$mail->send();

		$_RESPONSE = $mail->isSuccess();
		$_CONSOLE = $mail->getConsole();
		$totalSendMails = $mail->getTotalSentMails();
	}


	// change email cc und deputy
	if ($response && $controller=='mycompany' && ($userCCMailChanged || $userDeputyMailChanged)) {
		
		$protocol = Settings::init()->http_protocol.'://';
		$server = $_SERVER['SERVER_NAME'];

		$dataloader = array();

		foreach ($User->data as $key => $value) {
			$dataloader['submit_'.$key] = $value;
		}

		$dataloader['link'] = $protocol.$server."/admin/user.php?id=$id";

		$mail = new ActionMail(45);
		$mail->setParam('id', $id);		
		$mail->setSender($auth->id);
		$mail->setDataloader($dataloader);
		$mail->send();
	}
}

//remove all data where the user is reffered to
if($user_set_to_inactive == true) {
	$db = Connector::get();

	$sql = 'delete from user_roles where user_role_user = ' . $User->id;
	$db->exec($sql);

	$sql = "DELETE FROM user_invitations WHERE user_invitation_email = '$email' ";
	$db->exec($sql);

	$sql = 'update standardroles set standardrole_rtco_user = 0 where standardrole_rtco_user = ' . $User->id;
	$db->exec($sql);

	$sql = 'update standardroles set standardrole_dsup_user = 0 where standardrole_dsup_user = ' . $User->id;
	$db->exec($sql);

	$sql = 'update standardroles set standardrole_rtop_user = 0 where standardrole_rtop_user = ' . $User->id;
	$db->exec($sql);

	$sql = 'update standardroles set standardrole_cms_approver_user = 0 where standardrole_cms_approver_user = ' . $User->id;
	$db->exec($sql);

	$sql = 'update users set user_substitute = 0 where user_substitute = ' . $User->id;
	$db->exec($sql);

	$sql = 'update users set user_email_cc = "" where user_email_cc = "' . $email . '"';
	$db->exec($sql);

	$sql = 'update users set user_email_deputy = "" where user_email_deputy = "' . $email . '"';
	$db->exec($sql);

	$sql = 'delete from user_company_responsibles where user_company_responsible_user_id = ' .$User->id;
	$db->exec($sql);

	$sql = 'delete from mail_template_recipients where mail_template_recipient_user_id = ' . $User->id;
	$db->exec($sql);

	$sql = 'delete from user_company_responsibles where user_company_responsible_user_id = ' . $User->id;
	$db->exec($sql);
}

header('Content-Type: text/json');
echo json_encode(array(
	'response' => $response,
	'redirect' => $redirect,
	'message' => $message,
	'id' => $id
));
