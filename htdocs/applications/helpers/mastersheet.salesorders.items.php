<?php 
/**
 * 	Master Sheet Sales Orders Items List
 * 
 * 	Load all companies with customer numbers and shipto numbers and goup by items
 * 	Assign all master sheet material collection delivery dates as proposed delivery date for companies
 * 	For all companies with more then une customer or shipto number, build select dropdowns as opcion
 * 	
 * 	IMPORTANT: It's will be loaded only selectetd items from sales orders export page
 * 
 * 	@author admir.serifi@mediaparx.ch
 * 	@copyright Mediaparx AG
 * 	@version 1.1 (removed item collections)
 */
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$selected_items = $_REQUEST['selected_items'];
	$id = $_REQUEST['id'];
	
	// db application model
	$model = new Model($application);
	
	// get companies customer and ship to number
	// for selected items
	$result = $model->query("
		SELECT DISTINCT
			mps_ordersheet_id,
			address_company,
			address_mps_customernumber,
			address_mps_shipto
		FROM mps_ordersheet_items	
	")
	->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
	->bind(Ordersheet::DB_BIND_COMPANIES)
	->filter('mastersheet', "mps_ordersheet_mastersheet_id = $id")
	->filter('material', "mps_ordersheet_item_material_id IN ( $selected_items ) ")
	->fetchAll();
	
	if ($result) {
		
		// check has all ordersheet companies customer and shipto numbers 
		foreach ($result as $row) {
			if (!$row['address_mps_customernumber'] || !$row['address_mps_shipto']) {
				$companies_without_additionals[] = "<span class='error'>".$row['address_company']."</span>";
			}
		}
		
		if ($companies_without_additionals) {	
			$response = false;
			$disabled = 'disabled';
			$content = "<p class='error'>Operation is not possible since either the customer number or the ship to informatione for the following companies is missing:</p>";
			$content .= "<p>".join(' ', array_values($companies_without_additionals))."</p>";
		} else { 
			
			// master sheet
			$mastersheet = new Mastersheet($application);
			$mastersheet->read($id);
			
			// get company additinals
			$result = $model->query("
				SELECT *
				FROM db_retailnet.address_additionals
				INNER JOIN mps_ordersheets ON mps_ordersheet_address_id = address_additional_address
				WHERE mps_ordersheet_mastersheet_id = $id
			")->fetchAll();
			
			// build drop downs
			if ($result) {
				foreach ($result as $row) {
					
					$company = $row['address_additional_address'];
					
					// customer numbers
					if ($row['address_additional_customerno2']) $customerNumber[$company][]=$row['address_additional_customerno2'];
					if ($row['address_additional_customerno3']) $customerNumber[$company][]=$row['address_additional_customerno3'];
					if ($row['mps_address_shipto_customerno4']) $customerNumber[$company][]=$row['address_additional_customerno4'];
					if ($row['address_additional_customerno5']) $customerNumber[$company][]=$row['address_additional_customerno5'];
					
					// customer shipto numer
					if ($row['address_additional_shiptono2']) $shiptoNumber[$company][]=$row['address_additional_shiptono2'];
					if ($row['address_additional_shiptono3']) $shiptoNumber[$company][]=$row['address_additional_shiptono3'];
					if ($row['address_additional_shiptono4']) $shiptoNumber[$company][]=$row['address_additional_shiptono4'];
					if ($row['address_additional_shiptono5']) $shiptoNumber[$company][]=$row['address_additional_shiptono5'];
				}
			}
			
			// get master sheet delivery dates
			$mastersheet_delivery_dates = $mastersheet->deliveryDate()->load();
			
			// group delivery dates
			if ($mastersheet_delivery_dates) {
				
				$delivery_dates = array();
				
				foreach ($mastersheet_delivery_dates as $key => $date) {
					$planning = $date['mps_mastersheet_delivery_date_material_planning_type_id'];
					$collection = $date['mps_mastersheet_delivery_date_material_collection_category_id'];
					$delivery_dates[$planning][$collection] = $date['mps_mastersheet_delivery_date_date'];
				}
			}
			
			// get seleted items
			$result = $model->query("
				SELECT DISTINCT
					mps_ordersheet_id,
					mps_ordersheet_item_id,
					mps_material_id,
					mps_material_material_planning_type_id,
					mps_material_material_collection_category_id,
					CONCAT(mps_material_code, ', ', mps_material_name) AS item,
					mps_material_locally_provided,
					mps_ordersheet_item_quantity_approved,
					country_name, 
					mps_ordersheet_address_id,
					address_mps_customernumber,
					address_company,
					address_mps_shipto
				FROM mps_ordersheet_items	
			")
			->bind(Ordersheet_Item::DB_BIND_MATERIALS)
			->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
			->bind(Ordersheet::DB_BIND_COMPANIES)
			->bind(Company::DB_BIND_COUNTRIES)
			->filter('mastersheet', "mps_ordersheet_mastersheet_id = $id")
			->filter('not_ordered', "( mps_ordersheet_item_purchase_order_number IS NULL OR mps_ordersheet_item_purchase_order_number = '')")
			->order('mps_material_code')
			->order('mps_material_name')
			->order('country_name')
			->order('address_company')
			->fetchAll();
			
			if ($result) {  
				
				$selected_items = explode(',', $selected_items);
		
				foreach ($result as $row) { 
					
					$company = $row['mps_ordersheet_address_id'];
					$ordersheet = $row['mps_ordersheet_id'];
					$ordersheet_item = $row['mps_ordersheet_item_id'];
					$item = $row['mps_material_id'];
					$planning = $row['mps_material_material_planning_type_id'];
					$category = $row['mps_material_material_collection_category_id'];
					
					$delivery_date = $delivery_dates[$planning][$category];
					
					if (in_array($item, $selected_items)) { 
						
						// if company has more then one customer numbers
						// show this field as select dropdown
						if ($customerNumber[$company]) {
							
							$customerNumber[$company][]=$row['address_mps_customernumber'];
							
							$customerNumber[$company] = array_unique($customerNumber[$company]);
							sort($customerNumber[$company]);
							
							$company_customer_number = "<select name=customernumber[$ordersheet_item] >";
							
							foreach ($customerNumber[$company] as $value) {
								$selected = ($value==$row['address_mps_customernumber']) ? "selected=selected" : null;
								$company_customer_number .= "<option value='$value' $selected>$value</option>";
							}
							
							$company_customer_number .="</select>";
						}
						// show company customer number as text
						// and set customber number value as hidden field 
						else {
							$company_customer_number = $row['address_mps_customernumber'];
							$hiddenFields .= "<input type=hidden name=customernumber[$ordersheet_item] value='".$row['address_mps_customernumber']."'>";
						}
					
						
						// if company has more then one shipto numbers
						// show this field as drop down
						if ($shiptoNumber[$company]) {
						
							$shiptoNumber[$company][]=$row['address_mps_shipto'];
							
							$shiptoNumber[$company] = array_unique($shiptoNumber[$company]);
							sort($shiptoNumber[$company]);
						
							$company_shipto_number ="<select name=shiptonumber[$ordersheet_item] >";
							
							foreach ($shiptoNumber[$company] as $value) {
								$selected = ($value==$row['address_mps_shipto']) ? "selected=selected" : null;
								$company_shipto_number .= "<option value='$value' $selected>$value</option>";
							}
							
							$company_shipto_number .="</select>";
						}
						// show company shipto number as text
						// and set shipto number value as hidden field 
						else {
							$company_shipto_number = $row['address_mps_shipto'];
							$hiddenFields .= "<input type=hidden name=shiptonumber[$ordersheet_item] value='".$row['address_mps_shipto']."'>";
						}
	
						
						// item has approved quantites and is not localy provided, show it as list item
						if ($row['mps_ordersheet_item_quantity_approved'] && !$row['mps_material_locally_provided']) { 
							$source[$item]['name'] = $row['item'];
							$source[$item]['data'][$ordersheet]['country_name'] = $row['country_name'];
							$source[$item]['data'][$ordersheet]['address_company'] = $row['address_company'];;
							$source[$item]['data'][$ordersheet]['address_mps_customernumber'] = $company_customer_number;
							$source[$item]['data'][$ordersheet]['quantity'] = $row['mps_ordersheet_item_quantity_approved'];
							$source[$item]['data'][$ordersheet]['address_mps_shipto'] = $company_shipto_number;
							$source[$item]['data'][$ordersheet]['delivery_date'] = "<input type=text class=desire_date name=desire[$ordersheet_item] value='$delivery_date'>";
						}
						//  set items data (customer numer, shiptonumer and delivery date) as hidden field
						else {
							$date = ($row['delivery_date']) ? $row['delivery_date'] : date('d.m.Y');
							$hiddenFields .= "<input type=hidden name=customernumber[$ordersheet_item] value='".$row['address_mps_customernumber']."'>";
							$hiddenFields .= "<input type=hidden name=shiptonumber[$ordersheet_item] value='".$row['address_mps_shipto']."'>";
							$hiddenFields .= "<input type=hidden class=date name=desire[$ordersheet_item] value='$delivery_date'>";
						}
					}
				}
			}
			
			if ($source || $hiddenFields) {    
						
				if ($source) {
					foreach ($source as $item => $row) {
						
						$content  .= "<div class=content-caption >".$row['name']."</div>";
						
						$table = new Table(array(
							'class' => 'listing'		
						));
						
						$table->datagrid = $row['data'];
						$table->country_name('width=15%');
						$table->address_company();
						$table->quantity('width=5%', 'align=right');
						$table->address_mps_customernumber('width=20%');
						$table->address_mps_shipto('width=15%');
						$table->delivery_date('width=10%');
						$content .= $table->render();
					}
				}
				
				$content .= $hiddenFields;
			}
			
		}
	}

	echo json_encode(array(
		'response' => $response,
		'content' => $content,
		'disabled' => $disabled	
	));
	