<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$user = User::instance();
	$translate = Translate::instance();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$company = $_REQUEST['id'];
	$channel = $_REQUEST['channel'];
	$date = $_REQUEST['period'];
	$actualNumber = $_REQUEST['value'];

	$model = new Model($application);

	switch ($_REQUEST['section']) {
		
		case 'number':
			
			$posVerification = New Pos_Verification($application);
			$posVerification->read_periode($company, date('Y-m-d', $date));
			$verification = $posVerification->id;
			
			if (!$verification) {
				$verification = $posVerification->create(array(
					'posverification_address_id' => $company,
					'posverification_date' => date('Y-m-d', $date),
					'user_created' => $user->login
				));
			}
			
			$posVerificationData = new Pos_Verification_Data($application);
			$posVerificationData->read_verification($verification, $channel);
			
			if ($actualNumber) { 
				if ($posVerificationData->id) {
					$response = $posVerificationData->update(array(
						'posverification_data_actual_number' => $actualNumber,
						'user_modiefied' => $user->login,
						'date_modiefied'=> date('Y-m-d H:i:s')
					));
				} else {
					$response = $posVerificationData->create(array(
						'posverification_data_posverification_id' => $verification,
						'posverification_data_distribution_channel_id' => $channel,
						'posverification_data_actual_number' => $actualNumber,
						'user_created' => $user->login
					));
				}
			} elseif($posVerificationData->id) {
				
				$response = $posVerificationData->delete();
				
				// if no more verifications
				// remove pos verification
				if ($response && !$posVerification->verification()->load()) {
					$posVerification->delete();
				}
			}
			
		break;
		
		case 'confirm_verification':
			
			$posVerification = New Pos_Verification($application);
			$posVerification->read_periode($company, date('Y-m-d', $date));
			
			if ($posVerification->id) {
				$response = $posVerification->update(array(
					'posverification_confirm_date' => date('Y-m-d', $date),
					'posverification_confirm_user_id' => $user->id,
					'user_modiefied' => $user->login,
					'date_modiefied'=> date('Y-m-d H:i:s')
				));
			}
			
			Message::add(array(
				'type'=>'message',
				'keyword' => 'message_confirm_verification'
			));
			
		break;
		
		case 'delete_confirmation':
			
			$posVerification = New Pos_Verification($application);
			$posVerification->read_periode($company,date('Y-m-d', $date));
			
			if ($posVerification->id) {
				$response = $posVerification->update(array(
					'posverification_confirm_date' => null,
					'posverification_confirm_user_id' => null
				));
			}
			
			Message::add(array(
				'type'=>'message',
				'keyword' => 'message_delete_confirmation'
			));
			
		break;
		
		case 'reset':
			
		break;
	}
	
	if ($response) {
		$notification = array(
			'content' => $translate->message_request_saved,
			'properties' => array(
				'theme' => 'message'
			)
		);
	} else {
		$notification = array(
			'content' => $translate->error_request_saved,
			'properties' => array(
				'theme' => 'error'
			)
		);
	}
	
	header('Content-Type: text/json');
	echo json_encode(array(
		'response' => $response,
		'notification' => $notification
	));
	