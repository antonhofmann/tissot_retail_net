<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$search = $_REQUEST['search'];
	$id = $_REQUEST['id'];
	
	$model = new Model($application);

	$_REQUEST = session::filter($application, "$controller.$archived.$action", false);

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'date_created';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "desc";
	
	// sql pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rows = $settings->limit_pager_rows;
	$offset = ($page-1) * $rows;
		
	// filter: full text search
	if ($search && $search <> $translate->search) {
		
		$filters['search'] = "(
			mps_mastersheet_splitting_list_name LIKE '%$search%' 
			OR date_created LIKE '%$search%'
		)";
	}
	
	// filter: consolidation
	$filters['mastersheet'] = "mps_mastersheet_splitting_list_mastersheet_id = $id";
	
	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			mps_mastersheet_splitting_list_id,
			mps_mastersheet_splitting_list_name,
			DATE_FORMAT(date_created, '%d.%m.%Y') AS date_created 
		FROM mps_mastersheet_splitting_lists
	")
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rows)
	->fetchAll();

	if ($result) {
	
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
	
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	// toolbox: form
	if ($toolbox) {
		$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	
	$table->mps_mastersheet_splitting_list_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		"href=".$_REQUEST['form']
	);
	
	$table->date_created(
		Table::ATTRIBUTE_NOWRAP,
		'width=10%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	
	