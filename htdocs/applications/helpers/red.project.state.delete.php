<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$user = User::instance();
	$id = $_REQUEST['id'];
	$application = $_REQUEST['application'];

	if ($id && $user->permission(Red_project::PERMISSION_DELETE_DATA)) {
		$integrity = new Integrity();
		$integrity->set($id, 'red_projectstates', 'red');

		if ($integrity->check()) {
			$record = new Red_Project_State($application);
			$record->read($id);

			$delete = $record->delete();

			if ($delete) {
				Message::request_deleted();
				url::redirect("/$application/projectstates");
			} else {
				Message::request_failure();
				url::redirect("/$application/projectstates/edit/$id");
			}
		}
		else {
			Message::request_failure();
			url::redirect("/$application/projectstates/edit/$id");
		}

	} else {
		Message::request_failure();
		url::redirect("/$application/projectstates/edit/$id");
	}