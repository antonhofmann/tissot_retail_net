<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

Session::init();
$messages = Session::get('messages'); 
Session::remove('messages');
	
if ($messages) {
	
	$response = array();
	$messages = unserialize($messages);
	
	$translate = Translate::instance();
	
	foreach ($messages as $message) { 

		$theme = ($message['type']) ? $message['type'] : "message";
		$sticky = (isset($message['sticky'])) ? $message['sticky'] : "false";
		$life = ($message['life']) ? $message['life'] : 5000;
		
		$keyword = $message['keyword'];
		$data = $message['data'];

		if (is_array($data)) {
			$content = $translate->$keyword($data);
		} 
		elseif ($message['content']) {
			$content = $message['content'];
		}
		else {
			$content = $translate->$keyword ? $translate->$keyword : $keyword;
		}
		
		$response[] = array(
			'theme'  => $theme,
			'sticky' => $sticky,
			'life' => $life,
			'content' => $content
		);
	}

	header('Content-Type: text/json');
	echo json_encode($response);
}
	
