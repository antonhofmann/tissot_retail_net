<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// url vars
$application = $_REQUEST['application']; 
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

$model = new Model($application);

$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

// sql order
$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'design_objective_group_name';

$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

// filter: full text search
if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {

	$keyword = $_REQUEST['search'];

	$filters['search'] = "(
		design_objective_group_name LIKE \"%$keyword%\"
		OR product_line_name LIKE \"%$keyword%\"
		OR postype_name LIKE \"%$keyword%\"
	)";
}

if ($_REQUEST['active']) {
	
	$active = $_REQUEST['active'];

	if ($active==1) {
		$filters['active'] = "design_objective_group_active = 1 and design_objective_group_postype > 0";
	} 
	elseif($active==2) {
		$filters['active'] = "design_objective_group_active = 0 and design_objective_group_postype > 0";
	}
	elseif($active==3) {
		$filters['active'] = "(design_objective_group_postype IS NULL OR design_objective_group_postype = '')";
	}
} else {
	$active = 1;
	$filters['active'] = "design_objective_group_active = 1 and design_objective_group_postype > 0";
}

$result = $model->query("
	SELECT  SQL_CALC_FOUND_ROWS DISTINCT
		design_objective_groups.design_objective_group_id, 
		design_objective_groups.design_objective_group_name, 
		product_lines.product_line_name, 
		postypes.postype_name, 
		design_objective_groups.design_objective_group_multiple
	FROM design_objective_groups 
	LEFT JOIN product_lines ON design_objective_groups.design_objective_group_product_line = product_lines.product_line_id
	LEFT JOIN postypes ON design_objective_groups.design_objective_group_postype = postypes.postype_id
")
->filter($filters)
->order($order, $direction)
->offset($offset, $rowsPerPage)
->fetchAll();

if ($result) {
	
	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);
	
	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));
	
	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
}


// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// toolbox: add
if ($_REQUEST['add']) {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
}

// toolbox: serach full text
$toolbox[] = ui::searchbox();

$actives = array(
	1 => 'Active',
	2 => 'Inactive',
	3 => 'Used in old projects'
);

$toolbox[] = ui::dropdown($actives, array(
	'name' => 'active',
	'id' => 'active',
	'class' => 'submit',
	'value' => $active,
	'caption' => 'All Design Objective Groups'
));

// toolbox: form
if ($toolbox) {
	$toolbox = "
		<div class='table-toolbox'>
			<form class=toolbox method=post>".join($toolbox)."</form>
		</div>
	";
}

$table = new Table(array(
	'sort' => array('column'=>$order,'direction'=>$direction)
));

$table->datagrid = $datagrid;

$table->postype_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);

$table->design_objective_group_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	"href=".$_REQUEST['data']
);

/* not connected to a product line anymore
$table->product_line_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT
);
*/

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;


