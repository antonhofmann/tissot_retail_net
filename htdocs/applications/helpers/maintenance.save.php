<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['maintenance_window_id'];
	
	$data = array();
	
	// maintenance start date
	if (isset($_REQUEST['maintenance_window_start'])) {
		
		$d = new DateTime(date::sql($_REQUEST['maintenance_window_start']));
		
		if ($_REQUEST['maintenance_window_start_hours']) {
			$time = explode(':',$_REQUEST['maintenance_window_start_hours']);
			$hours = ($time[0]) ? $time[0] : 0;
			$minut = ($time[1]) ? $time[1] : 0;
			$d->modify("+$hours hours $minut minutes");
		}

		$data['maintenance_window_start'] = $d->format('Y-m-d H:i:s');
	}
	
	// maintenance stop date
	if ($_REQUEST['maintenance_window_stop']) {
		
		$d = new DateTime(date::sql($_REQUEST['maintenance_window_stop']));
		
		if ($_REQUEST['maintenance_window_stop_hours']) {
			$time = explode(':',$_REQUEST['maintenance_window_stop_hours']);
			$hours = ($time[0]) ? $time[0] : 0;
			$minut = ($time[1]) ? $time[1] : 0;
			$d->modify("+$hours hours $minut minutes");
		}
		
		$data['maintenance_window_stop'] = $d->format('Y-m-d H:i:s');
	}
	
	// maintenance warning date
	if (isset($_REQUEST['maintenance_window_warning_start_date'])) {
	
		$d = new DateTime(date::sql($_REQUEST['maintenance_window_warning_start_date']));
	
		if ($_REQUEST['maintenance_window_warning_start_date_hours']) {
			$time = explode(':',$_REQUEST['maintenance_window_warning_start_date_hours']);
			$hours = ($time[0]) ? $time[0] : 0;
			$minut = ($time[1]) ? $time[1] : 0;
			$d->modify("+$hours hours $minut minutes");
		}
	
		$data['maintenance_window_warning_start_date'] = $d->format('Y-m-d H:i:s');
	}
	
	// maintenance warning message content
	if (isset($_REQUEST['maintenance_window_warning_content'])) {
		$data['maintenance_window_warning_content'] = $_REQUEST['maintenance_window_warning_content'];
	}
	
	// maintenance message title
	if ($_REQUEST['maintenance_window_title']) {
		$data['maintenance_window_title'] = $_REQUEST['maintenance_window_title'];
	}
	
	// maintenance message content
	if (isset($_REQUEST['maintenance_window_content'])) {
		$data['maintenance_window_content'] = $_REQUEST['maintenance_window_content'];
	}
	
	if (in_array('maintenance_window_active', $fields)) {
		$data['maintenance_window_active'] = ($_REQUEST['maintenance_window_active']) ? 1 : 0;
	}
	
	if ($data) {
	
		$maintenance = new Maintenance($application);
		$maintenance->read($id);
		
		$model = new Model(Connector::DB_CORE);
		$model->db->exec("UPDATE maintenance_windows SET maintenance_window_active = 0");
	
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $maintenance->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $maintenance->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));
	