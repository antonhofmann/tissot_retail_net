<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['posaddress_id'];
	
	// request vars
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$data = array();
	
	if ($_REQUEST['posaddress_google_lat']) {
		$data['posaddress_google_lat'] = $_REQUEST['posaddress_google_lat'];
	}
	
	if ($_REQUEST['posaddress_google_long']) {
		$data['posaddress_google_long'] = $_REQUEST['posaddress_google_long'];
	}
	
	if($data && $id) {
		
		$pos = new Pos();
		$pos->read($id);
	
		$data['posaddress_google_precision'] = 1; 
		$data['user_modified'] = $user->login;
		
		$response = $pos->update($data);
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		
		if ($response) {
			$storeLocator = new Store_Locator();
			$storeLocator->geodata($id);
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));