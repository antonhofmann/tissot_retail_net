<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$appliction = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['id'];

$itemGroupOptionFile = new Item_Group_Option_File();
$itemGroupOptionFile->read($id);

$permission_edit = user::permission('can_edit_catalog');
$permission_edit_items = user::permission('can_edit_items_in_catalogue');
$permission_edit_files = user::permission('can_edit_files_in_catalogue');
$_CAN_EDIT = $permission_edit || $permission_edit_items || $permission_edit_files ? true : false;

if ($itemGroupOptionFile->id && $_CAN_EDIT) {
	
	$file = $itemGroupOptionFile->path;
	$json['response'] = $itemGroupOptionFile->delete();
	
	if ($json['response']) {
		file::remove($file);
		$json['message'] = Translate::instance()->message_request_deleted;
	} else {
		$json['message'] = Translate::instance()->message_request_failure;
	}
}
else {
	$json['response'] = false;
	$json['message'] = Translate::instance()->message_access_denied;
}

header('Content-Type: text/json');
echo json_encode($json);