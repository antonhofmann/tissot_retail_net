<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = trim($_REQUEST['application']);
	$controller = trim($_REQUEST['controller']);
	$action = trim($_REQUEST['action']);
	$archived = trim($_REQUEST['archived']);

	// list order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "item_code, item_name";
	
	// order direction
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
	
	// list vars
	$search = $_REQUEST['search'];
	$furniture = $_REQUEST['furniture'];
	
	// db connector
	$model = new Model($application);
	
	// filter: search
	if ($search && $search <> $translate->search) {
		$filters['search'] = "(
			item_code LIKE '%$search%' 
			OR item_name LIKE '%$search%'
		)";
	}
	
	// filter: item type
	$filters['item_type'] = 'item_type = 1';
	
	// filter: used in mps
	$filters['mps'] = 'item_visible_in_mps = 1';
	

	if(!$user->permission(Pos::PERMISSION_VIEW) && !$user->permission(Pos::PERMISSION_EDIT)) {
		$limited_filter = " AND posaddress_client_id = ".$user->address;
	}

	// extended country access
	$accessCountries = array();
		
	$result = $model->query("
		SELECT DISTINCT country_access_country
		FROM db_retailnet.country_access
		WHERE country_access_user = $user->id
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			$accessCountries[] = $row['country_access_country'];
		}
		
		$accessCountriesFilter = join(',', $accessCountries);
	}
	
	
	// filter: country access
	if ($accessCountriesFilter) {
		$extendedFilter = "posaddress_country IN ($accessCountriesFilter)";
	}

	// refional access companies
	$regionalAccessFilter = User::getRegionalAccessPos();

	// filter: regional access
	if ($regionalAccessFilter) {
		$extendedFilter .= $regionalAccessFilter;
	}

	$mainFilter = "
		posorders.posorder_type = 1
		AND posaddress_store_openingdate IS NOT NULL
		AND posaddress_store_openingdate <> '0000-00-00'
		AND(
			posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = '0000-00-00'
		)
		AND posorders.posorder_order > 0
		AND order_actual_order_state_code <> 900
		AND posorder_project_kind IN (1,2,3,4)
	";

	if ($extendedFilter) {
		$extendedFilter = " OR ( $mainFilter AND $extendedFilter )";
	}
	
	// get last project orders
	$ordernumbers = $model->query("
		SELECT
			posaddress_id,
			max(posorders.posorder_order) as ordernumber
		FROM db_retailnet.posorders
			LEFT JOIN db_retailnet.posaddresses ON posaddress_id = posorder_posaddress
			LEFT JOIN db_retailnet.orders ON order_id = posorders.posorder_order
		WHERE
			( $mainFilter $limited_filter ) $extendedFilter
		GROUP BY
			posaddress_id		
	")->fetchAll();
	
	
	// filter: order numbers
	if ($ordernumbers) {
		
		$orders = array();
		
		foreach ($ordernumbers as $row) {
			$orders[] = $row['ordernumber'];
		}
			
		$ordernumbers = join(', ',$orders);
		$filters['order_numbers'] = "order_item_order IN ( $ordernumbers )";
	}

	
	
	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT 
			item_id, 
			item_code,
			item_name
		FROM db_retailnet.order_items
	")
	->bind('INNER JOIN db_retailnet.items ON order_items.order_item_item = items.item_id')
	->bind('INNER JOIN db_retailnet.orders ON order_item_order = order_id')
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();

	
	if ($result && $ordernumbers) {
	
		$totalrows = $model->totalRows();
		
		$datagrid = _array::datagrid($result);
		
		foreach ($datagrid as $key => $row) {
			$datagrid[$key]['item_name'] = "<a id=$key href=/$application/$controller/$application/$key >".$row['item_name']."</a>";
		}
		
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true,
			'translate' => $translate
		));
		
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
	
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	
	$table->item_code(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=20%'
	);
	
	$table->item_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	
	