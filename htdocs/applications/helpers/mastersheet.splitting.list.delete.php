<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['mastersheet'];
	$version = $_REQUEST['version'];
	
	// master sheet
	$mastersheet = new Mastersheet($appliction);
	$mastersheet->read($id);
	
	// set splitting list instance
	$mastersheet->splittinglist()->read($version);
	
	if ($mastersheet->splittinglist()->id && user::permission(Mastersheet::PERMISSION_EDIT)) {
		
		// remove splitting list
		$response = $mastersheet->splittinglist()->delete();
		
		if ($response) {
			
			// remove all splitting list items
			$mastersheet->splittinglist()->item()->deleteAll();
			
			message::request_deleted();
			url::redirect("/$appliction/$controller/$action/$id");
			
		} else {
			message::request_failure();
			url::redirect("/$appliction/$controller/$action/$id/$version");
		}
		
	}
	else {
		Message::access_denied();
		url::redirect("/$appliction/$controller");
	}