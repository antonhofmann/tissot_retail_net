<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// permissions
$permission_view = User::permission(Pos::PERMISSION_VIEW);
$permission_view_limited = User::permission(Pos::PERMISSION_VIEW_LIMITED);
$permission_edit = User::permission(Pos::PERMISSION_EDIT);
$permission_edit_limited = User::permission(Pos::PERMISSION_EDIT_LIMITED);
$permission_travelling = User::permission('has_access_to_all_travalling_retail_data');

// has limited permission
$hasLimitedPermission = (!$permission_view && !$permission_edit) ? true : false;

$application = trim($_REQUEST['application']);
$controller = trim($_REQUEST['controller']);
$action = trim($_REQUEST['action']);
$archived = trim($_REQUEST['archived']);

$resetRequestVars = array();

// reset checkboxes
if ($_REQUEST['page']) {
	
	if (!$_REQUEST['posowner_types']) {
		$resetRequestVars[] = 'posowner_types';
	}
	
	if (!$_REQUEST['product_lines']) {
		$resetRequestVars[] = 'product_lines';
	}
	
	if (!$_REQUEST['possubclasses']) {
		$resetRequestVars[] = 'possubclasses';
	}
}

$_REQUEST = session::filter($application, "$controller.$archived.$action", $resetRequestVars);

// request filter
$fCountry = $_REQUEST['countries'];
$fSearch = $_REQUEST['search'] && $_REQUEST['search']<>$translate->search ? $_REQUEST['search'] : null;
$fPosType = $_REQUEST['pos_types'];
$fPosOwnerType = $_REQUEST['posowner_types'];
$fPosSubClass = $_REQUEST['possubclasses'];
$fDistributionChannel = $_REQUEST['distribution_channels'];
$fSalesRepresentative = $_REQUEST['sales_representative'];
$fDecorationPerson = $_REQUEST['decoration_person'];
$fTurnoverWatch = $_REQUEST['turnoverclass_watches'];
$fTuronoverBijoux = $_REQUEST['turnoverclass_bijoux'];
$fProductLines = $_REQUEST['product_lines'];
$fClient = $_REQUEST['client'];


// sql order
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "country_name, place_name, posaddress_name";
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;

$model = new Model($application);

// bind list external sources
$binds = array(
	Pos::DB_BIND_COUNTRIES,
	Pos::DB_BIND_PLACES,
	Pos::DB_BIND_PROVINCES,
	Pos::DB_BIND_POSOWNER_TYPES,
	Pos::DB_BIND_POSTYPES
);

// filter: countries
if ($fCountry) {
	$filters['countries'] = "posaddress_country = $fCountry";
} 

// filter: full text search
if ($fSearch) {
	$filters['search'] = "(
		posaddress_name LIKE \"%$fSearch%\"
		OR country_name LIKE \"%$fSearch%\"
		OR province_canton LIKE \"%$fSearch%\"
		OR place_name LIKE \"%$fSearch%\"
		OR posaddress_zip LIKE \"%$fSearch%\"
		OR postype_name LIKE \"%$fSearch%\"
		OR posaddress_sapnumber LIKE \"%$fSearch%\"
		OR posaddress_sap_shipto_number LIKE \"%$fSearch%\"
	)";
}

// filter: pos type
if ($fPosType) 	{
	$filters['pos_types'] = "posaddress_store_postype = $fPosType";
}

// filter: client
if ($fClient) 	{
	$filters['client'] = "posaddress_client_id = $fClient";
}

// filter: legal types
if ($fPosOwnerType) {
	$types = join(',', array_keys($fPosOwnerType));
	$filters['posowner_types'] = "posaddress_ownertype IN ($types)";
	$advancedActive = 'active';
}

// filter: postype subclass
if ($fPosSubClass) {
	$subclasses = join(',', array_keys($fPosSubClass));
	$filters['possubclasses'] = "posaddress_store_subclass IN ($subclasses)";
	$advancedActive = 'active';
}

// filter: distribution channel
if ($fDistributionChannel) {
	
	if ($fDistributionChannel=='null') $filters['distribution_channels'] = "(posaddress_distribution_channel IS NULL OR posaddress_distribution_channel='' OR posaddress_distribution_channel=0)";
	else $filters['distribution_channels'] = "posaddress_distribution_channel = $fDistributionChannel";
	
	$advancedActive = 'active';
}

// filter: sales representative
if ($fSalesRepresentative) {
	$filters['sales_representative'] = "posaddress_sales_representative = $fSalesRepresentative";
	$advancedActive = 'active';
}

// filter: decoration person
if ($fDecorationPerson) {
	$filters['decoration_person'] = "posaddress_decoration_person = $fDecorationPerson";
	$advancedActive = 'active';
}

// filter: turnoverclass watches
if ($fTurnoverWatch) {
	$filters['turnoverclass_watches'] = "posaddress_turnoverclass_watches = $fTurnoverWatch";
	$advancedActive = 'active';
}

// filter: turnoverclass bijoux
if ($fTuronoverBijoux) {
	$filters['turnoverclass_bijoux'] = "posaddress_turnoverclass_bijoux = $fTuronoverBijoux";
	$advancedActive = 'active';
}

// filter: product lines
if ($fProductLines) {
	$lines = join(',', array_keys($fProductLines));
	$filters['posowner_types'] = "posaddress_store_furniture IN ($lines)";
	$advancedActive = 'active';
}

// filter: actives
if ($archived) {
	$filters['archived'] = "(
		posaddress_store_closingdate <> '0000-00-00'
		OR posaddress_store_closingdate <> NULL
	)";
}
else {
	$filters['active'] = "(
		posaddress_store_closingdate = '0000-00-00'
		OR posaddress_store_closingdate IS NULL
		OR posaddress_store_closingdate = ''
	)";
}

// for limited view
if ($hasLimitedPermission) {

	$countryAccess = User::getCountryAccess();

	// filter access countries
	$filterCountryAccess = $countryAccess ? " OR posaddress_country IN($countryAccess)" : null;

	// regional access companies
	$regionalAccessCompanies = User::getRegionalAccessPos();
	$filterRegionalAccess = $regionalAccessCompanies ? " OR $regionalAccessCompanies " : null;

	// travelling
	if ($permission_travelling) { 
		$binds[] = "LEFT JOIN db_retailnet.posareas ON posarea_posaddress = posaddress_id";
		$travelling = " OR posarea_area IN (4,5) OR posaddress_store_subclass IN(17) "; 
	}

	$filters['limited'] = "(
		posaddress_client_id = $user->address
		$travelling
		$filterCountryAccess 
		$filterRegionalAccess
	)";
}


$sth = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		posaddress_id,
		IF(posaddress_name <> '', posaddress_name, 'n.a.') AS posname,
		posaddress_zip,
		place_name,
		country_name,
		posowner_type_name,
		postype_name,
		posaddress_google_precision,
		posaddress_store_closingdate,
		province_canton,
		posaddress_export_to_web,
		posaddress_google_lat,
		posaddress_google_long,
		posaddress_turnoverclass_watches,
		posaddress_turnoverclass_bijoux,
		posaddress_distribution_channel,
		posaddress_sapnumber,
		posaddress_store_subclass
	FROM db_retailnet.posaddresses
")
->bind($binds)
->filter($filters)
->order($sort, $direction)
->offset($offset, $rowsPerPage);

$sth->extend($extendedFilter);
$result = $sth->fetchAll();
$totalrows = $model->totalRows();

if ($result) {

	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	$list_index = $pager->index();
	$list_controlls = $pager->controlls();
	
	// distribution channel
	$distributionChannel = new DistChannel('mps');
	
	// turnover classes
	$turnoverClass = new TurnoverClass('mps');

	//$iconMap = ui::icon('map');
	$iconPDF = ui::extension('pdf','small');
	$iconWeb = "<i class='fa fa-globe fa-lg'></i>";
	$iconMap = "<span class='fa-stack'><i class='fa fa-circle fa-stack-2x'></i><i class='fa fa-map-marker fa-stack-1x fa-inverse'></i></span>";

	foreach ($result as $row) {

		$id = $row['posaddress_id'];

		// turnover watches
		if ($row['posaddress_turnoverclass_watches']) {
			$turnoverClass->read($row['posaddress_turnoverclass_watches']);
			$row['tc_watches'] = $turnoverClass->name;
		}

		// turnover bijoux
		if ($row['posaddress_turnoverclass_bijoux']) {
			$turnoverClass->read($row['posaddress_turnoverclass_bijoux']);
			$row['tc_bijoux'] = $turnoverClass->name;
		}

		// distributions channel
		if ($row['posaddress_distribution_channel']) {
			$distributionChannel->read($row['posaddress_distribution_channel']);
			$row['distribution_channel'] = $distributionChannel->name.' '.$distributionChannel->code;
		}

		// googlemap
		$link = "/applications/helpers/google.map.php?latitude=".$row['posaddress_google_lat']."&longitude=".$row['posaddress_google_long'];
		$row['map'] = "<a class='modal' data-fancybox-type='iframe' href='$link'>$iconMap</a>";

		// pdf sheet
		$link = $_REQUEST['pdf']."&id=$id";

		
		$row['pdf'] = "<a class='pdf' href='$link' target='_blank'>$iconPDF</a>";
		$row['web'] = $row['posaddress_export_to_web'] ? $iconWeb : null;

		// datagrid
		$datagrid[$id] = $row;
	}


	$involved_pos = join(',', array_keys($datagrid));

	// PupUp-Projekte
	$result = $model->query("
		SELECT 
			COUNT(posorder_id) as total,
			posorder_posaddress AS pos
		FROM db_retailnet.posorders
		WHERE posorder_posaddress IN ($involved_pos)
		AND posorder_project_kind = 8
		AND posorder_opening_date IS NOT NULL
		AND posorder_opening_date <> '0000-00-00'
		AND posorder_opening_date <> '0000-00-00'
		AND (posorder_popup_closingdate IS NULL OR posorder_popup_closingdate = '0000-00-00')
		GROUP BY posorder_posaddress
	")->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$id = $row['pos'];
			if ($datagrid[$id] && $row['total'] > 0) {
				$datagrid[$id]['popup_project'] = "<img alt=\"Has running PopUp project\" title=\"Has running PopUp project\"  src=\"/pictures/popup_store.png\" />";
			}
		}
	}
}

// toolbox: hidden utilities
$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
$toolbox[] = "<input type=hidden id=sort name=sort value='".$_REQUEST['sort']."' class='submit' />";
$toolbox[] = "<input type=hidden id=direction name=direction value='".$_REQUEST['direction']."' class='submit' />";
$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

// toolbox: dropdown print :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


if ($datagrid && ($_REQUEST['printlist'] || $_REQUEST['printdetails'] || $_REQUEST['printfurnite'] || $_REQUEST['printmaterials'])) { 
	
	$print = array();
	
	if ($_REQUEST['printlist']) $print['exportbox'] = 'Pos List';
	if ($_REQUEST['printfurnite']) $print[$_REQUEST['printfurnite']] = 'Pos Furniture List';
	if ($_REQUEST['printmaterials']) $print[$_REQUEST['printmaterials']] = 'Pos Material List';
	
	if ($print) {
		$toolbox[] = ui::dropdown($print, array(
			'id' => 'print',
			'name' => 'print',
			'class' => 'selectbox',
			'caption' => $translate->print	
		));
	}
}


// toolbox: add
if ($_REQUEST['add'])  {
	$toolbox[] = ui::button(array(
		'id' => 'add',
		'icon' => 'add',
		'href' => $_REQUEST['add'],
		'label' => $translate->add_new
	));
}


// toolbox: searchbox ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$toolbox[] = ui::searchbox();

// toolbox: countries ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$query = "SELECT DISTINCT country_id, country_name FROM db_retailnet.posaddresses";
$result = $model->query($query)->bind($binds)->filter($filters)->exclude('countries')->order('country_name')->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'countries',
		'id' => 'countries',
		'class' => 'submit selectbox',
		'value' => $fCountry,
		'caption' => $translate->all_countries
	));
}

// toolbox: legal types ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$query = "SELECT DISTINCT postype_id, postype_name FROM db_retailnet.posaddresses";
$result = $model->query($query)->bind($binds)->filter($filters)->exclude('pos_types')->order('postype_name')->fetchAll();

if ($result) {
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'pos_types',
		'id' => 'pos_types',
		'class' => 'submit selectbox',
		'value' => $fPosType,
		'caption' => $translate->all_pos_types
	));
}

// toolbox: clients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$hasLimitedPermission) {

	$result = $model->query("SELECT DISTINCT address_id, CONCAT(addressCountries.country_name, ': ', address_company) AS caption FROM db_retailnet.posaddresses")
	->bind($binds)
	->bind("INNER JOIN db_retailnet.addresses ON address_id = posaddress_client_id")
	->bind("INNER JOIN db_retailnet.countries addressCountries ON addressCountries.country_id = address_country")
	->filter($filters)->exclude('client')
	->order('addressCountries.country_name, address_company')
	->fetchAll();

	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'client',
			'id' => 'client',
			'class' => 'submit selectbox',
			'value' => $fClient,
			'caption' => "All Clients"
		));
	}
}

// toolbox: pos owners types :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$query = "SELECT DISTINCT posowner_type_id, posowner_type_name FROM db_retailnet.posaddresses";
$result = $model->query($query)->bind($binds)->filter($filters)->exclude('posowner_types')->order('posowner_type_name')->fetchAll();

if ($result) {

	$ownerTypes = '';

	foreach ($result as $row) {
		$type = $row['posowner_type_id'];
		$checked = ($fPosOwnerType[$type]) ? 'checked=checked' : null;
		$ownerTypes .= "<span class=poprow ><input type=checkbox name=posowner_types[$type] $checked /> {$row[posowner_type_name]}</span>";
	}
	
	$selected = $fPosOwnerType ? 'selected' : null;
	$arrow = $selected ? 'arrow-down' : 'arrow-right';

	$advanced[] = "
		<li>
			<span class='toggle-title'><span class='arrow $arrow'></span>Legal Types</span>
			<span class='toggle-content $selected'>$ownerTypes</span>
		</li>
	";
}

// sales details section
$saleDetails = array();

// toolbox: distribution chanels ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("
	SELECT DISTINCT 
		mps_distchannel_id,
		CONCAT(mps_distchannel_group_name, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) AS channel
	FROM db_retailnet.posaddresses
")
->bind($binds)
->bind(Pos::DB_BIND_DISTRIBUTION_CHANNELS)
->bind('LEFT JOIN db_retailnet.mps_distchannel_groups ON db_retailnet.mps_distchannels.mps_distchannel_group_id = db_retailnet.mps_distchannel_groups.mps_distchannel_group_id')
->filter($filters)
->exclude('distribution_channels')
->order('mps_distchannel_group_name, mps_distchannel_name, mps_distchannel_code')
->fetchAll();

if ($result) {
	
	array_unshift($result, array(
		'mps_distchannel_id' => 'null',
		'channel' => 'No Distribution Channel Assigned'
	));
	
	$dropdown = ui::dropdown($result, array(
		'name' => 'distribution_channels',
		'id' => 'distribution_channels',
		'value' => $fDistributionChannel,
		'caption' => $translate->all_distribution_channels
	));
	
	$saleDetails[] = "<span class=poprow >$dropdown</span>";
}

// toolbox: sales repesentative ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$model->query("
	SELECT DISTINCT
		mps_staff_id,
		CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS name
	FROM db_retailnet.posaddresses
")
->bind($binds)
->bind(Pos::DB_BIND_MPS_STAFF_SALES_REPRESENTATIVES)
->filter($filters)
->filter('staff_type', 'mps_staff_staff_type_id = 1')
->exclude('sales_representative')
->order('mps_staff_name, mps_staff_firstname');

if ($hasLimitedPermission) {
	$model->filter('staff_limited',  "mps_staff_address_id = $user->address");
}

$result = $model->fetchAll();

if ($result) {

	$dropdown = ui::dropdown($result, array(
		'name' => 'sales_representative',
		'id' => 'sales_representative',
		'value' => $fSalesRepresentative,
		'caption' => $translate->all_sales_representatives
	));
	
	$saleDetails[] = "<span class=poprow >$dropdown</span>";
}

// toolbox: decoration person :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$query = "SELECT DISTINCT mps_staff_id, CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS name FROM db_retailnet.posaddresses";
$model->query($query)
->bind($binds)->bind(Pos::DB_BIND_MPS_STAFF_DECORATOR_PERSONS)
->filter($filters)->filter('staff_type', 'mps_staff_staff_type_id = 2')
->exclude('decoration_person')->order('mps_staff_name, mps_staff_firstname')->fetchAll();

if ($hasLimitedPermission) {
	$model->filter('staff_limited',  "mps_staff_address_id = $user->address");
}

$result = $model->fetchAll();

if ($result) {

	$dropdown = ui::dropdown($result, array(
		'name' => 'decoration_person',
		'id' => 'decoration_person',
		'value' => $fDecorationPerson,
		'caption' => $translate->all_decoration_persons
	));
	
	$saleDetails[] = "<span class=poprow >$dropdown</span>";
}


// filter: toc watches
$extendedFilter = null;
$basicFiletrs = $filters;
$basicFiletrs['group'] = 'mps_turnoverclass_group_id=1';
unset($basicFiletrs['turnoverclass_watches']);

// toolbox: turnover class watches :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$query = "SELECT DISTINCT mps_turnoverclass_id, mps_turnoverclass_name FROM db_retailnet.posaddresses";
$result = $model->query($query)->bind($binds)->bind(Pos::DB_BIND_TURNOVER_CLASS_WATCHES)
->filter($filters)->filter('group', 'mps_turnoverclass_group_id=1')
->exclude('turnoverclass_watches')->order('mps_turnoverclass_name')->fetchAll();

if ($result) {

	$dropdown = ui::dropdown($result, array(
		'name' => 'turnoverclass_watches',
		'id' => 'turnoverclass_watches',
		'value' => $fTurnoverWatch,
		'caption' => $translate->all_turnover_class_watches
	));
	
	$saleDetails[] = "<span class=poprow >$dropdown</span>";
}

// toolboc: turnover class bijoux :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// filter: toc bijoux
$extendedFilter = null;
$basicFiletrs = $filters;
$basicFiletrs['group'] = 'mps_turnoverclass_group_id=2';
unset($basicFiletrs['turnoverclass_bijoux']);

$query = "SELECT DISTINCT mps_turnoverclass_id, mps_turnoverclass_name FROM db_retailnet.posaddresses";
$result = $model->query($query)->bind($binds)->bind(Pos::DB_BIND_TURNOVER_CLASS_BIJOUX)
->filter($filters)->filter('group', 'mps_turnoverclass_group_id=2')
->exclude('turnoverclass_bijoux')->order('mps_turnoverclass_name')->fetchAll();

if ($result) {
	
	$dropdown = ui::dropdown($result, array(
		'name' => 'turnoverclass_bijoux',
		'id' => 'turnoverclass_bijoux',
		'value' => $fTuronoverBijoux,
		'caption' => $translate->all_turnover_class_bijoux
	));
	
	$saleDetails[] = "<span class=poprow >$dropdown</span>";
}

if ($saleDetails) {
	
	$selected = $fDistributionChannel || $fSalesRepresentative || $fDecorationPerson || $fTurnoverWatch || $fTuronoverBijoux ? 'selected' : null;
	$arrow = ($selected) ? 'arrow-down' : 'arrow-right';
	
	$advanced[] = "
		<li>
			<span class='toggle-title'><span class='arrow $arrow'></span>Sales Details</span>
			<span class='toggle-content $selected'>".join($saleDetails)."</span>
		</li>
	";
}

$selected = null;


// filter product lines
$extendedFilter = null;
$basicFiletrs = $filters;
unset($basicFiletrs['product_lines']);



// toolbox: product lines :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$query = "SELECT DISTINCT product_line_id, product_line_name FROM db_retailnet.posaddresses";
$result = $model->query($query)->bind($binds)->bind(Pos::DB_BIND_PRODUCT_LINES)
->filter($filters)->exclude('product_lines')->order('product_line_name')->fetchAll();

if ($result) {

	$productLines = null;

	foreach ($result as $row) {
		$line = $row['product_line_id'];
		$checked = ($fProductLines[$line]) ? 'checked=checked' : null;
		$productLines .= "<span class=poprow ><input type=checkbox name=product_lines[$line] $checked /> {$row[product_line_name]}</span>";
	}
	
	$selected = $fProductLines ? 'selected' : null;
	$arrow = ($selected) ? 'arrow-down' : 'arrow-right';

	$advanced[] = "
		<li>
			<span class='toggle-title'><span class='arrow $arrow'></span>Product Lines</span>
			<span class='toggle-content $selected'>$productLines</span>
		</li>
	";
}

// toolbox: pos subclasses :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("
	SELECT DISTINCT 
		possubclass_id, possubclass_name 
	FROM db_retailnet.posaddresses
")
->bind($binds)
->bind("INNER JOIN db_retailnet.possubclasses ON possubclass_id = posaddress_store_subclass")
->filter($filters)
->exclude('possubclasses')
->order('possubclass_name')
->fetchAll();

if ($result) {

	$subClasses = '';

	foreach ($result as $row) {
		$k = $row['possubclass_id'];
		$checked = $fPosSubClass[$k] ? 'checked=checked' : null;
		$subClasses .= "<span class=poprow ><input type=checkbox name=possubclasses[$k] $checked /> {$row[possubclass_name]}</span>";
	}
	
	$selected = $fPosSubClass ? 'selected' : null;
	$arrow = $selected ? 'arrow-down' : 'arrow-right';

	$advanced[] = "
		<li>
			<span class='toggle-title'><span class='arrow $arrow'></span>POS Type Subclasses</span>
			<span class='toggle-content $selected'>$subClasses</span>
		</li>
	";
}

if ($advanced) {
	
	$btnCancel = ui::button(array(
		'id' => 'cancel',		
		'icon' => 'cancel',		
		'label' => $translate->cancel	
	));
	
	$btnApply = ui::button(array(
		'id' => 'apply',		
		'icon' => 'apply',		
		'label' => $translate->apply	
	));
	
	$toolbox[] = "
		<span id=advanced class='button $advancedActive' data='#filters' >
			<span class='icon direction-down'></span>
			<span class=label >Advanced Filter</span>
		</span>
	";
	
	$advancedBox = "
		<div id='filters'>
			<ul>".join($advanced)."</ul>
			<div class=actions >$btnCancel $btnApply</div>
		</div>	
	";
}

if ($toolbox) {
	$toolbox = "<div class='table-toolbox'><form class='toolbox default' method=post>".join($toolbox).$advancedBox."</form></div>";
}

$table = new Table(array(
	'sort' => array('column'=>$sort, 'direction'=>$direction)
));

$table->datagrid = $datagrid;

$table->caption('ff', 'FF');
$table->caption('se', 'SE');

$table->country_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=5%'
);

$table->province_canton(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=5%'
);

$table->place_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=5%'
);

if (_array::key_exists('popup_project', $datagrid)) {
	$table->popup_project(
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=20px'
	);
}

$table->posname(
	Table::PARAM_SORT,
	Table::DATA_TYPE_LINK,
	Table::ATTRIBUTE_NOWRAP,
	"href=".$_REQUEST['form']
);

$table->posaddress_sapnumber(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=5%'
);

$table->distribution_channel(
	Table::ATTRIBUTE_NOWRAP,
	'width=5%'
);

$table->postype_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=5%'
);

$table->posowner_type_name(
	Table::ATTRIBUTE_NOWRAP,
	Table::PARAM_SORT,
	'width=5%'
);

$table->ff(
	Table::ATTRIBUTE_ALIGN_CENTER,
	'width=20px'
);


$table->se(
	Table::ATTRIBUTE_ALIGN_CENTER,
	'width=20px'
);

$table->map(
	Table::ATTRIBUTE_ALIGN_CENTER,
	'width=20px'
);


$table->web(
	Table::ATTRIBUTE_ALIGN_CENTER,
	'width=20px'
);


$table->pdf(
	Table::ATTRIBUTE_ALIGN_CENTER,
	'width=20px'
);

$table->footer($list_index);
$table->footer($list_controlls);
$table = $table->render();

echo $toolbox.$table;
