<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$id = $_REQUEST['item_group_id'];
	
	$data = array();
	
	if (in_array('item_group_category', $fields)) {
		$data['item_group_category'] = $_REQUEST['item_group_category'] ? $_REQUEST['item_group_category'] : null;
	}
		
	if (in_array('item_group_name', $fields)) {
		$data['item_group_name'] = $_REQUEST['item_group_name'] ? $_REQUEST['item_group_name'] : null;
	}		

	if (in_array('item_group_description', $fields)) {
		$data['item_group_description'] = $_REQUEST['item_group_description'] ? $_REQUEST['item_group_description'] : null;
	}
	
	if ($data) {
	
		$itemGroup = new Item_Group();
		$itemGroup->read($id);
	
		if ($itemGroup->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $itemGroup->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			
			$response = $id = $itemGroup->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect'].'/'.$id : null;
		}
	}
	else {
		
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	