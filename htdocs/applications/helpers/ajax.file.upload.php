<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');

$canUpload = true;
$filename = $_FILES['userfile']['name'];

$user = User::instance();
$translate = Translate::instance();

// check file extensions
if ($_REQUEST['checkExtension'] && $_FILES['userfile']['name'] && $_REQUEST['application']) {
	
	$canUpload = false;
	
	// get application extensions
	$application = new Application();
	$application->read_from_shortcut($_REQUEST['application']);
	$result = $application->file_extensions();
	
	if ($result) {
		
		$extensions = array();
		
		foreach ($result as $key => $extension) {
			
			if (strpos($extension,';') !== false) {
				
				$array = explode(';', $extension);
				
				foreach ($array as $value) {
					$extensions[] = $value;
				}
			}
			else {
				$extensions[] = $extension;
			}
		}
		
		$extension = file::extension($_FILES['userfile']['name']);
			
		$canUpload = (in_array($extension, $extensions)) ? true : false;
			
		$message =  $button = $translate->message_file_uploaded_failure;
			
		$message .= $translate->error_file_type(array(
			'types' => '( .'.join(', .',$extensions).' )'
		));
	}
}

// upload sections
switch ($_REQUEST['section']) {
	
	case 'remove':
	
		if ($_REQUEST['file']) {
			$response = File::remove($_REQUEST['file']);
		} 
		
		$message = $response ? $translate->message_delete_file : $translate->error_delete_file;
		
	break;
	
	default:
		
		if ($canUpload) {
			
			$response = true;
			$path = upload::tmp('userfile');
			
			$message = $button = $path ? $translate->message_file_uploaded : $translate->message_file_uploaded_failure;
		}
		
	break;
}


echo json_encode(array(
	'response' => $response,
	'message' => $message,
	'path' => $path,
	'button' => $button
));
