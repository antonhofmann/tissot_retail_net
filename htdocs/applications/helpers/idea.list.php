<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	$model = new Model(Connector::DB_CORE);

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'ideas.date_created DESC, idea_category_name, idea_title';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			idea_category_name LIKE \"%$keyword%\" 
			OR user_firstname LIKE \"%$keyword%\" 
			OR user_name LIKE \"%$keyword%\"
			OR idea_title LIKE \"%$keyword%\"
			OR idea_text LIKE \"%$keyword%\"
		)";
	}
	
	// category
	if ($_REQUEST['category']) {
		$filters['category'] = "idea_idea_category_id = ".$_REQUEST['category'];
	}
	
	// category
	if ($_REQUEST['users']) {
		$filters['users'] = "idea_user_id = ".$_REQUEST['users'];
	}
	
	// killed
	if ($_REQUEST['killed'] == 1) {
		$filters['killed'] = "idea_killed = 1";
	} else {
		$filters['killed'] = "(idea_killed IS NULL OR idea_killed = '')";
	}
	
	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS
			idea_id,
			idea_category_name,
			CONCAT(user_firstname,' ', user_name) AS user,
			idea_title,
			idea_text,
			idea_priority,
			idea_killed,
			DATE_FORMAT(ideas.date_created, '%d.%m.%Y %H:%i') AS date
		FROM ideas
		INNER JOIN idea_categories ON idea_category_id = idea_idea_category_id
		INNER JOIN users ON user_id = idea_user_id
	")
	->order($sort, $direction)
	->filter($filters)
	->offset($offset, $rowsPerPage)
	->fetchAll();
	
	if ($result) {
		
		$link = $_REQUEST['data'];
		$totalrows = $model->totalRows();
		
		foreach ($result as $row) {
			
			$k = $row['idea_id'];
			
			if (strlen($row['idea_text']) > 120) {
				$row['idea_text'] = substr($row['idea_text'], 0, 120).'..';
			}
			
			$row['idea_title'] = "<a href='$link/$k'>".$row['idea_title']."</a><span>".$row['idea_text']."</span>";
			
			if ($row['idea_priority']) {
				$row['idea_priority'] = "<span class='fa fa-flag-checkered' ></span>";
			}
			
			if ($row['idea_killed']) {
				$row['idea_killed'] = "<span class='fa fa-thumbs-o-down killed' ></span>";
			}
			
			$datagrid[$k] = $row;
		}
	
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
	
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	// toolbox: categories
	$result = $model->query("
		SELECT DISTINCT
			idea_category_id, idea_category_name
		FROM ideas
		INNER JOIN idea_categories ON idea_category_id = idea_idea_category_id
	")
	->filter($filters)
	->exclude('category')
	->order('idea_category_name')
	->fetchAll();

	$toolbox[] = html::select($result, array(
		'name' => 'category',
		'id' => 'category',
		'class' => 'submit',
		'value' => $_REQUEST['category'],
		'caption' => $translate->all_categories
	));
	
	// toolbox: users
	$result = $model->query("
		SELECT DISTINCT
			user_id, CONCAT(user_firstname,' ', user_name) as user
		FROM ideas
		INNER JOIN users ON user_id = idea_user_id
	")
	->filter($filters)
	->exclude('users')
	->order('user_firstname, user_name')
	->fetchAll();

	$toolbox[] = html::select($result, array(
		'name' => 'users',
		'id' => 'users',
		'class' => 'submit',
		'value' => $_REQUEST['users'],
		'caption' => 'All Users'
	));

	$killed = array(
		'1'  => 'Killed Ideas'
	);
	
	$toolbox[] = html::select($killed, array(
		'name' => 'killed',
		'id' => 'killed',
		'class' => 'submit',
		'value' => $_REQUEST['killed'],
		'caption' => 'Not Killed Ideas'
	));
	
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	$table->dataloader($dataloader);
	
	$table->date(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=5%'
	);
	
	$table->idea_category_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->user(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->idea_title(
		Table::PARAM_SORT
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	