<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	if ($id && ($user->permission(TurnoverClass::PERMISSION_EDIT))) {
		
		$turnover_class = new TurnoverClass($application);
		$turnover_class->read($id);
		$delete = $turnover_class->delete();
		
		if ($delete) {
			Message::request_deleted();
			url::redirect("/$application/$controller");
		} else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$id");
		}
	}
	else {
		Message::access_denied();
		url::redirect("/$application/$controller");
	}