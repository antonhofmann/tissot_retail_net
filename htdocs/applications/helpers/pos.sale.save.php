<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$id = $_REQUEST['posaddress_id'];
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$data = array();
	
	if(in_array('posaddress_sales_representative', $fields)) {
		$data['posaddress_sales_representative'] = $_REQUEST['posaddress_sales_representative'] ?: null;
	}
	
	if(in_array('posaddress_decoration_person', $fields)) {
		$data['posaddress_decoration_person'] = $_REQUEST['posaddress_decoration_person'] ?: null;
	}
	
	if(in_array('posaddress_distribution_channel', $fields)) {
		$data['posaddress_distribution_channel'] = $_REQUEST['posaddress_distribution_channel'] ?: null;
	}
	
	if(in_array('posaddress_turnoverclass_watches', $fields)) {
		$data['posaddress_turnoverclass_watches'] = $_REQUEST['posaddress_turnoverclass_watches'] ?: null;
	}
	
	if(in_array('posaddress_turnoverclass_bijoux', $fields)) {
		$data['posaddress_turnoverclass_bijoux'] = $_REQUEST['posaddress_turnoverclass_bijoux'] ?: null;
	}
	
	if(in_array('posaddress_turnover_type', $fields)) {
		$data['posaddress_turnover_type'] = $_REQUEST['posaddress_turnover_type'] ?: null;
	}
	
	if(in_array('posaddress_sales_classification01', $fields)) {
		$data['posaddress_sales_classification01'] = $_REQUEST['posaddress_sales_classification01'] ?: null;
	}
	
	if(in_array('posaddress_sales_classification02', $fields)) {
		$data['posaddress_sales_classification02'] = $_REQUEST['posaddress_sales_classification02'] ?: null;
	}
	
	if(in_array('posaddress_sales_classification03', $fields)) {
		$data['posaddress_sales_classification03'] = $_REQUEST['posaddress_sales_classification03'] ?: null;
	}
	
	if(in_array('posaddress_sales_classification04', $fields)) {
		$data['posaddress_sales_classification04'] = $_REQUEST['posaddress_sales_classification4'] ?: null;
	}
	
	if(in_array('posaddress_sales_classification05', $fields)) {
		$data['posaddress_sales_classification05'] = $_REQUEST['posaddress_sales_classification05'] ?: null;
	}
	
	if(in_array('posaddress_selling_bijoux', $fields)) {
		$data['posaddress_selling_bijoux'] = ($_REQUEST['posaddress_selling_bijoux']) ? 1 : 0;
	}
	
	
	if(in_array('posaddress_max_watches', $fields)) {
		$data['posaddress_max_watches'] = $_REQUEST['posaddress_max_watches'] ?: null;
	}
	
	if(in_array('posaddress_max_bijoux', $fields)) {
		$data['posaddress_max_bijoux'] = $_REQUEST['posaddress_max_bijoux'] ?: null;
	}
	
	
	if(in_array('posaddress_overall_sqms', $fields)) {
		$data['posaddress_overall_sqms'] = $_REQUEST['posaddress_overall_sqms'] ?: null;
	}
	
	if(in_array('posaddress_dedicated_sqms_wall', $fields)) {
		$data['posaddress_dedicated_sqms_wall'] = $_REQUEST['posaddress_dedicated_sqms_wall'] ?: null;
	}
	
	if(in_array('posaddress_dedicated_sqms_free', $fields)) {
		$data['posaddress_dedicated_sqms_free'] = $_REQUEST['posaddress_dedicated_sqms_free'] ?: null;
	}
	
	if(in_array('posaddress_store_totalsurface', $fields)) {
		$data['posaddress_store_totalsurface'] = $_REQUEST['posaddress_store_totalsurface'] ?: null;
	}
	
	if(in_array('posaddress_store_retailarea', $fields)) {
		$data['posaddress_store_retailarea'] = $_REQUEST['posaddress_store_retailarea'] ?: null;
	}
	
	if(in_array('posaddress_store_backoffice', $fields)) {
		$data['posaddress_store_backoffice'] = $_REQUEST['posaddress_store_backoffice'] ?: null;
	}
	
	if(in_array('posaddress_store_numfloors', $fields)) {
		$data['posaddress_store_numfloors'] = $_REQUEST['posaddress_store_numfloors'] ?: null;
	}
	
	if(in_array('posaddress_store_floorsurface1', $fields)) {
		$data['posaddress_store_floorsurface1'] = $_REQUEST['posaddress_store_floorsurface1'] ?: null;
	}
	
	if(in_array('posaddress_store_floorsurface2', $fields)) {
		$data['posaddress_store_floorsurface2'] = $_REQUEST['posaddress_store_floorsurface2'] ?: null;
	}
	
	if(in_array('posaddress_store_floorsurface3', $fields)) {
		$data['posaddress_store_floorsurface3'] = $_REQUEST['posaddress_store_floorsurface3'] ?: null;
	}
	
	if ($data && $id) {
		
		$pos = new Pos();
		$pos->read($id);
		
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		
		$response = $pos->update($data);
		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
	}


	// update store locator
	if ($response) {
		$storelocator = new Store_Locator();
		$storelocator->update($id);
	}
	
	echo json_encode(array(
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message
	));
	