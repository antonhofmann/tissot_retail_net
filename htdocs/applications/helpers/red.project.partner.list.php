<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$model = new Model($application);
	
	// sql binder
	$bindTables = array(
		Red_Project::BIND_USER_ID,
		Red_Project::BIND_ADDRESSES,
		Red_Project::BIND_PARTNER_TYPE
	);
	
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "
		red_partnertype_name,
		address_company,
		address_place
	";
	
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rows = $settings->limit_pager_rows;
	$offset = ($page-1) * $rows;
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {

		$keyword = $_REQUEST['search'];

		$filters['search'] = "(
			red_partnertype_name LIKE \"%$keyword%\"
			OR address_company LIKE \"%$keyword%\"
			OR address_place LIKE \"%$keyword%\"
			OR user_name LIKE \"%$keyword%\"
			OR user_firstname LIKE \"%$keyword%\"
		)";
	}
	
	// filter: pos type
	if ($_REQUEST['partner_types']) 	{
		$filters['partner_types'] = "red_project_partner_partnertype_id = ".$_REQUEST['partner_types'];
	}
	
	// filter: project
	$filters['project'] = "red_project_partner_project_id = $id";

	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			red_project_partner_id,
			red_project_partner_address_id,
			red_project_partner_partnertype_id,
			red_project_partner_project_id,
			red_project_partner_user_id,
			DATE_FORMAT(red_project_partner_access_from, '%d.%m.%Y') as red_project_partner_access_from,
			DATE_FORMAT(red_project_partner_access_until, '%d.%m.%Y') as red_project_partner_access_until,
			user_firstname,
			user_name,
			address_company,
			address_place,
			red_partnertype_name
		FROM red_project_partners
	")
	->bind($bindTables)
	->filter($filters)
	->fetchAll();

	if ($result) {
		
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));

		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	
	// toolbox: button print
	if ($datagrid && $_REQUEST['export']) {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'href' => $_REQUEST['export'],
			'label' => $translate->print
		));
	}
	
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'label' => $translate->add_new,
			'href' => $_REQUEST['add']
		));
	}

	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	$result = $model->query("
		SELECT DISTINCT red_partnertype_id, red_partnertype_name 
		FROM red_project_partners
	")
	->bind($bindTables)
	->filter($filters)
	->exclude('partner_types')
	->order('red_partnertype_name')
	->fetchAll();

	$toolbox[] = ui::dropdown($result, array(
		'name' => 'partner_types',
		'id' => 'partner_types',
		'class' => 'submit',
		'value' => $_REQUEST['partner_types'],
		'caption' => $translate->all_partner_types
	));

	

	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));

	$table->datagrid = $datagrid;

	$table->red_partnertype_name(
		Table::ATTRIBUTE_NOWRAP
	);

	$table->address_company(
		Table::ATTRIBUTE_NOWRAP,
		'href='.$_REQUEST['form']
	);

	$table->address_place(
		Table::ATTRIBUTE_NOWRAP
	);

	$table->user_name(
		Table::ATTRIBUTE_NOWRAP
	);

	$table->user_firstname(
		Table::ATTRIBUTE_NOWRAP
	);

	$table->red_project_partner_access_from(
		Table::ATTRIBUTE_NOWRAP
	);

	$table->red_project_partner_access_until(
		Table::ATTRIBUTE_NOWRAP
	);

	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();

	echo $toolbox.$table;
	