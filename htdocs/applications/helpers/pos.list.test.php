<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$application = trim($_REQUEST['application']);
	$controller = trim($_REQUEST['controller']);
	$action = trim($_REQUEST['action']);
	$archived = trim($_REQUEST['archived']);
	
	$resetRequestVars = array(
		'posowner_types', 
		'product_lines'
	);

	$_REQUEST = session::filter($application, "pos.list.$archived", $resetRequestVars);

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "country_name, place_name, posaddress_name";
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

	// sql offset
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$offset = ($page-1) * $settings->limit_pager_rows;

	$model = new Model($application);

	// bind list external sources
	$binds = array(
		Pos::DB_BIND_COUNTRIES,
		Pos::DB_BIND_PLACES,
		Pos::DB_BIND_PROVINCES,
		Pos::DB_BIND_POSOWNER_TYPES,
		Pos::DB_BIND_POSTYPES
	);

	// filter: defaults
	if ($archived) {
		$filters['archived'] = "(
			posaddress_store_closingdate <> '0000-00-00'
			OR posaddress_store_closingdate <> NULL
		)";
	}
	else {
		$filters['active'] = "(
			posaddress_store_closingdate = '0000-00-00'
			OR posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = ''
		)";
	}

	// filter: address parents
	if( !$user->permission(Pos::PERMISSION_EDIT) && !$user->permission(Pos::PERMISSION_VIEW)) {
		$filters['limited'] = "posaddress_client_id = ".$user->address;
	}

	// filter: countries
	if ($_REQUEST['countries']) {
		$filters['countries'] = "posaddress_country=".$_REQUEST['countries'];
	}
	elseif ($user->permission(Pos::PERMISSION_EDIT_LIMITED)) {

		$result = $model->query("
			SELECT DISTINCT posaddress_country
			FROM db_retailnet.posaddresses
			WHERE posaddress_client_id = ".$user->address."
		")->fetchAll();

		if ($result) {

			foreach ($result as $row) {
				$countries[] = $row['posaddress_country'];
			}

			$user_countries = join(',', $countries);
			$filters['countries'] = "posaddress_country IN ($user_countries)";
		}
	}

	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {

		$keyword = $_REQUEST['search'];

		$filters['search'] = "(
			posaddress_name LIKE \"%$keyword%\"
			OR country_name LIKE \"%$keyword%\"
			OR province_canton LIKE \"%$keyword%\"
			OR place_name LIKE \"%$keyword%\"
			OR posaddress_zip LIKE \"%$keyword%\"
			OR postype_name LIKE \"%$keyword%\"
		)";
	}

	// filter: pos type
	if ($_REQUEST['pos_types']) 	{
		$filters['pos_types'] = "posaddress_store_postype = ".$_REQUEST['pos_types'];
	}

	// filter: legal types
	if ($_REQUEST['posowner_types']) {
		foreach ($_REQUEST['posowner_types'] as $key => $value) {
			$legaltypes[] = "posaddress_ownertype=$key";
		}
		$filters['posowner_types'] = "(".join(' OR ', $legaltypes).")";
	}

	// filter: distribution channel
	if ($_REQUEST['distribution_channels']) {
		$filters['distribution_channels'] = "posaddress_distribution_channel=".$_REQUEST['distribution_channels'];
	}

	// filter: sales representative
	if ($_REQUEST['sales_representative']) {
		$filters['sales_representative'] = "posaddress_sales_representative=".$_REQUEST['sales_representative'];
	}

	// filter: decoration person
	if ($_REQUEST['decoration_person']) {
		$filters['decoration_person'] = "posaddress_decoration_person=".$_REQUEST['decoration_person'];
	}

	// filter: turnoverclass watches
	if ($_REQUEST['turnoverclass_watches']) {
		$filters['turnoverclass_watches'] = "posaddress_turnoverclass_watches=".$_REQUEST['turnoverclass_watches'];
	}

	// filter: turnoverclass bijoux
	if ($_REQUEST['turnoverclass_bijoux']) {
		$filters['turnoverclass_bijoux'] = "posaddress_turnoverclass_bijoux=".$_REQUEST['turnoverclass_bijoux'];
	}

	// filter: product lines
	if ($_REQUEST['product_lines']) {
		foreach ($_REQUEST['product_lines'] as $key => $value) {
			$productlines[] = "posaddress_store_furniture=$key";
		}
		$filters['product_lines'] = "(".join(' OR ', $productlines).")";
	}

	// data: poslocations
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			posaddress_id,
			IF(posaddress_name <> '', posaddress_name, 'n.a.') AS posname,
			posaddress_zip,
			place_name,
			country_name,
			posowner_type_name,
			postype_name,
			posaddress_google_precision,
			posaddress_store_closingdate,
			province_canton,
			posaddress_export_to_web,
			posaddress_google_lat,
			posaddress_google_long,
			posaddress_turnoverclass_watches,
			posaddress_turnoverclass_bijoux,
			posaddress_distribution_channel
		FROM db_retailnet.posaddresses
	")
	->bind($binds)
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $settings->limit_pager_rows)
	->fetchAll();

	$totalrows = $model->totalRows();

	if ($result) {

		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));

		$list_index = $pager->index();
		$list_controlls = $pager->controlls();

		foreach ($result as $row) {

			$id = $row['posaddress_id'];

			// turnover watches
			if ($row['posaddress_turnoverclass_watches']) {

				$result = $model->query("
						SELECT mps_turnoverclass_name 
						FROM mps_turnoverclasses
				")
				->filter('default', 'mps_turnoverclass_id='.$row['posaddress_turnoverclass_watches'])
				->fetch();

				$row['tc_watches'] = $result['mps_turnoverclass_name'];
			}

			// turnover bijoux
			if ($row['posaddress_turnoverclass_bijoux']) {

				$result = $model->query("
					SELECT mps_turnoverclass_name 
					FROM mps_turnoverclasses
				")
				->filter('default', 'mps_turnoverclass_id='.$row['posaddress_turnoverclass_bijoux'])
				->fetch();

				$row['turnoverclass_bijoux'] = $result['mps_turnoverclass_name'];
			}

			// distributions channel
			if ($row['posaddress_distribution_channel']) {

				$result = $model->query("
					SELECT CONCAT(mps_distchannel_name, ' ', mps_distchannel_code) AS name 
					FROM mps_distchannels
				")
				->filter('default', 'mps_distchannel_id='.$row['posaddress_distribution_channel'])
				->fetch();

				$row['distribution_channel'] = $result['name'];
			}

			// googlemap
			$link = "/applications/helpers/google.map.php?latitude=".$row['posaddress_google_lat']."&longitude=".$row['posaddress_google_long'];
			$row['map'] = "<a class='modal' href='$link'>".ui::icon('map')."</a>";

			// has pos pictures
			$result = $model->query("
				SELECT order_file_id, order_file_path 
				FROM db_retailnet.order_files
			")
			->bind('INNER JOIN db_retailnet.posorders ON posorder_order=order_file_order')
			->filter('pos', "posorder_posaddress=$id")
			->filter('category', 'order_file_category=11')
			->fetch();

			// pos pictures
			if ($result) {
				$link = "/applications/helpers/picture.gallery.php?table=order_files&id=$id";
				$row['pic'] = "<a class='modal' href='$link'>".ui::icon('picture')."</a>";
			}

			// pdf sheet
			$link = $_REQUEST['pdf']."&id=$id";
			$row['pdf'] = "<a class='pdf' href='$link' target='_blank'>".ui::extension('pdf','small')."</a>";

			// datagrid
			$datagrid[$id] = $row;
		}
	}
/*
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";;

	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}

	// toolbox: serach full text
	$toolbox[] = ui::searchbox();

	// toolbox: countries
	$result = $model->query("
		SELECT DISTINCT
			country_id, country_name
		FROM db_retailnet.posaddresses")
	->bind($binds)
	->filter($filters)
	->exclude('countries')
	->order('country_name')
	->fetchAll();

	$toolbox[] = ui::dropdown($result, array(
		'name' => 'countries',
		'id' => 'countries',
		'class' => 'submit selectbox',
		'value' => $_REQUEST['countries'],
		'caption' => $translate->all_countries
	));

	// toolbox: postypes
	$result = $model->query("
		SELECT DISTINCT
			postype_id, postype_name
		FROM db_retailnet.posaddresses")
	->bind($binds)
	->filter($filters)
	->exclude('pos_types')
	->order('postype_name')
	->fetchAll();

	$toolbox[] = ui::dropdown($result, array(
		'name' => 'pos_types',
		'id' => 'pos_types',
		'class' => 'submit selectbox',
		'value' => $_REQUEST['pos_types'],
		'caption' => $translate->all_pos_types
	));


	// toolbox: pos owners types
	$result = $model->query("
		SELECT DISTINCT
			posowner_type_id, posowner_type_name
		FROM db_retailnet.posaddresses")
	->bind($binds)
	->filter($filters)
	->exclude('posowner_types')
	->order('posowner_type_name')
	->fetchAll();

	if ($result) {

		foreach ($result as $row) {
			list($id, $caption) = array_values($row);
			$checked = ($_REQUEST['posowner_types'][$id]) ? 'checked=checked' : null;
			$posowner_types .= "<span class=poprow ><input type=checkbox name=posowner_types[$id] $checked /> $caption</span>";
		}
		
		$selected = ($_REQUEST['posowner_types']) ? 'selected' : null;
		$arrow = ($selected) ? 'arrow-down' : 'arrow-right';

		$advanced[] = "
			<li>
				<span class='toggle-title'><span class='arrow $arrow'></span>Owner Types</span>
				<span class='toggle-content $selected'>$posowner_types</span>
			</li>
		";
	}
	
	// toolbox: distribution chanels
	$result = $model->query("
		SELECT DISTINCT
			mps_distchannel_id,
			CONCAT(mps_distchannel_name, ' ', mps_distchannel_code) AS distributionchannel
		FROM db_retailnet.posaddresses")
	->bind($binds)
	->bind(Pos::DB_BIND_DISTRIBUTION_CHANNELS)
	->filter($filters)
	->exclude('distribution_channels')
	->order('mps_distchannel_group')
	->order('mps_distchannel_name')
	->order('mps_distchannel_code')
	->fetchAll();

	if ($result) {
		
		$dropdown = ui::dropdown($result, array(
			'name' => 'distribution_channels',
			'id' => 'distribution_channels',
			'value' => $_REQUEST['distribution_channels'],
			'caption' => $translate->all_distribution_channels
		));
		
		$sales_details[] = "<span class=poprow >$dropdown</span>";
	}


	// toolbox: sales repesentative
	$model->query("
		SELECT DISTINCT
			mps_staff_id,
			CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS name
		FROM db_retailnet.posaddresses")
	->bind($binds)
	->bind(Pos::DB_BIND_MPS_STAFF_SALES_REPRESENTATIVES)
	->filter($filters)
	->filter('staff_type', 'mps_staff_type_id=1')
	->exclude('sales_representative')
	->order('mps_staff_name')
	->order('mps_staff_firstname');

	if (!$user->permission(!Staff::PERMISSION_EDIT)) {
		$model->filter('limited', 'mps_staff_address_id='.$user->address);
	}

	$result = $model->fetchAll();

	if ($result) {
		
		$dropdown = ui::dropdown($result, array(
			'name' => 'sales_representative',
			'id' => 'sales_representative',
			'value' => $_REQUEST['sales_representative'],
			'caption' => $translate->all_sales_representatives
		));
		
		$sales_details[] = "<span class=poprow >$dropdown</span>";
	}


	// toolbox: decoration person
	$model->query("
		SELECT DISTINCT
			mps_staff_id,
			CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS name
		FROM db_retailnet.posaddresses")
	->bind($binds)
	->bind(Pos::DB_BIND_MPS_STAFF_DECORATOR_PERSONS)
	->filter($filters)
	->filter('staff_type', 'mps_staff_type_id=2')
	->exclude('decoration_person')
	->order('mps_staff_name')
	->order('mps_staff_firstname');

	if (!$user->permission(!Staff::PERMISSION_EDIT)) {
		$model->filter('limited', 'mps_staff_address_id='.$user->address);
	}

	$result = $model->fetchAll();

	if ($result) {
		
		$dropdown = ui::dropdown($result, array(
			'name' => 'decoration_person',
			'id' => 'decoration_person',
			'value' => $_REQUEST['decoration_person'],
			'caption' => $translate->all_decoration_persons
		));
		
		$sales_details[] = "<span class=poprow >$dropdown</span>";
	}

	// toolbox: turnover class watches
	$result = $model->query("
		SELECT DISTINCT
			mps_turnoverclass_id, mps_turnoverclass_name
		FROM db_retailnet.posaddresses")
	->bind($binds)
	->bind(Pos::DB_BIND_TURNOVER_CLASS_WATCHES)
	->filter($filters)
	->filter('turnoverclass_group', 'mps_turnoverclass_group_id=1')
	->exclude('turnoverclass_watches')
	->order('mps_turnoverclass_name')
	->fetchAll();

	if ($result) {
		
		$dropdown = ui::dropdown($result, array(
			'name' => 'turnoverclass_watches',
			'id' => 'turnoverclass_watches',
			'value' => $_REQUEST['turnoverclass_watches'],
			'caption' => $translate->all_turnover_class_watches
		));
		
		$sales_details[] = "<span class=poprow >$dropdown</span>";
	}

	// toolbox: turnover class bijoux
	$result = $model->query("
		SELECT DISTINCT
			mps_turnoverclass_id, mps_turnoverclass_name
		FROM db_retailnet.posaddresses")
	->bind($binds)
	->bind(Pos::DB_BIND_TURNOVER_CLASS_WATCHES)
	->filter($filters)
	->filter('turnoverclass_group', 'mps_turnoverclass_group_id=2')
	->exclude('turnoverclass_bijoux')
	->order('mps_turnoverclass_name')
	->fetchAll();

	if ($result) {
		
		$dropdown = ui::dropdown($result, array(
			'name' => 'turnoverclass_bijoux',
			'id' => 'turnoverclass_bijoux',
			'value' => $_REQUEST['turnoverclass_bijoux'],
			'caption' => $translate->all_turnover_class_bijoux
		));
		
		$sales_details[] = "<span class=poprow >$dropdown</span>";
	}

	if ($sales_details) {
		
		$selected = (
			$_REQUEST['distribution_channels'] 
			|| $_REQUEST['sales_representative']
			|| $_REQUEST['decoration_person']
			|| $_REQUEST['turnoverclass_watches']
			|| $_REQUEST['turnoverclass_bijoux']
		) ? 'selected' : null;
		
		$arrow = ($selected) ? 'arrow-down' : 'arrow-right';
		
		$advanced[] = "
			<li>
				<span class='toggle-title'><span class='arrow $arrow'></span>Sales Details</span>
				<span class='toggle-content $selected'>".join($sales_details)."</span>
			</li>
		";
	}
	
	$selected = null;

	// toolbox: product lines
	$result = $model->query("
		SELECT DISTINCT
			product_line_id, product_line_name
		FROM db_retailnet.posaddresses")
	->bind($binds)
	->bind(Pos::DB_BIND_PRODUCT_LINES)
	->filter($filters)
	->exclude('product_lines')
	->order('product_line_name')
	->fetchAll();

	if ($result) {

		foreach ($result as $row) {
			list($id, $caption) = array_values($row);
			$checked = ($_REQUEST['product_lines'][$id]) ? 'checked=checked' : null;
			$product_lines .= "<span class=poprow ><input type=checkbox name=product_lines[$id] $checked /> $caption</span>";
		}
		
		$selected = ($_REQUEST['product_lines']) ? 'selected' : null;
		$arrow = ($selected) ? 'arrow-down' : 'arrow-right';

		$advanced[] = "
			<li>
				<span class='toggle-title'><span class='arrow $arrow'></span>Product Lines</span>
				<span class='toggle-content $selected'>$product_lines</span>
			</li>
		";
	}
	

	if ($advanced) {
		
		$btnCancel = ui::button(array(
			'id' => 'cancel',		
			'icon' => 'cancel',		
			'label' => $translate->cancel	
		));
		
		$btnApply = ui::button(array(
			'id' => 'apply',		
			'icon' => 'apply',		
			'label' => $translate->apply	
		));
		
		$toolbox[] = "
			<span id=advanced class='button' data='#filters' >
				<span class='icon direction-down'></span>
				<span class=label >Advanced Filter</span>
			</span>
		";
		
		$advancedBox = "
			<div id='filters'>
				<ul>".join($advanced)."</ul>
				<div class=actions >$btnCancel $btnApply</div>
			</div>	
		";
	}

	if ($datagrid && $_REQUEST['export']) {
		
		$toolbox[] = "
			<span id=print class='button' data='#printlist' >
				<span class='icon print'></span>
				<span class=label >Print</span>
			</span>
		";
		
		$printBox = "
			<div id='printlist'>
				<ul>
					<li><a href='".$_REQUEST['export']['list']."' >Pos List</a></li>
					<li><a href='".$_REQUEST['export']['details']."' >Pos Detail List</a></li>
					<li><a href='".$_REQUEST['export']['furniture']."' >Pos Furniture List</a></li>
					<li><a href='".$_REQUEST['export']['materials']."' >Pos Material List</a></li>
				</ul>
			</div>		
		";
	}

	if ($toolbox) {
		$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox).$advancedBox.$printBox."</form></div>";
	}
*/
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));

	$table->datagrid = $datagrid;

	$table->country_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);

	$table->province_canton(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);

	$table->place_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);

	$table->posname(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		Table::DATA_TYPE_LINK,
		"href=".$_REQUEST['form']
	);

	$table->distribution_channel(
		Table::ATTRIBUTE_NOWRAP,
		'width=10%'
	);

	$table->tc_watches(
		Table::ATTRIBUTE_NOWRAP,
		'width=10%'
	);

	$table->tc_bijoux(
		Table::ATTRIBUTE_NOWRAP,
		'width=10%'
	);

	$table->postype_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=5%'
	);

	$table->posowner_type_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=5%'
	);
	/*
	$table->pic(
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=20px'
	);
	*/
	$table->map(
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=20px'
	);

	$table->web(
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=20px'
	);

	$table->pdf(
		Table::ATTRIBUTE_ALIGN_CENTER,
		'width=20px'
	);

	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();

	echo $table;
