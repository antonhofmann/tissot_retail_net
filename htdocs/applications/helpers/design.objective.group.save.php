<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	// product line
	$id = $_REQUEST['design_objective_group_id'];
	
	$data = array();
	
	if (in_array('design_objective_group_postype', $fields)) {
		$data['design_objective_group_postype'] = $_REQUEST['design_objective_group_postype'] ? $_REQUEST['design_objective_group_postype'] : null;
	}
	
	if (in_array('design_objective_group_product_line', $fields)) {
		$data['design_objective_group_product_line'] = $_REQUEST['design_objective_group_product_line'] ? $_REQUEST['design_objective_group_product_line'] : null;
	}	
	
	if (in_array('design_objective_group_name', $fields)) {
		$data['design_objective_group_name'] = $_REQUEST['design_objective_group_name'];
	}	

	if (in_array('design_objective_group_multiple', $fields)) {
		$data['design_objective_group_multiple'] = $_REQUEST['design_objective_group_multiple'] ? 1 : 0;
	}

	if (in_array('design_objective_group_active', $fields)) {
		$data['design_objective_group_active'] = $_REQUEST['design_objective_group_active'] ? 1 : 0;
	}
	
	if ($data) {
	
		$designObjectiveGroup = new Design_Objective_Group();
		$designObjectiveGroup->read($id);
	
		if ($designObjectiveGroup->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $designObjectiveGroup->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $designObjectiveGroup->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	