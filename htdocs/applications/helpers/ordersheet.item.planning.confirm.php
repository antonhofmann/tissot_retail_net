<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

define(DEBUGGING_MODE, false);

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// request
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$id = $_REQUEST['ordersheet'];

// consoling
$_ERRORS = array();
$_MESSAGES = array();
$_NOTIFICATIONS = array();
$_DATA = array();

// item confirmation permission
$_CAN_CONFIRM_ITEMS = true;

// export to SAP permission
$_CAN_EXPORT_SAP = false;

// db model
$model = new Model($application);


// order sheet :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$ordersheet = new Ordersheet($application);
$ordersheet->read($id);

// company
$company = new Company();

if (!$ordersheet->id) {
	$_ERRORS[] = "Order sheet $id not found";
} else {

	$company->read($ordersheet->address_id);

	// export data to SAP
	$_CAN_EXPORT_SAP = $company->do_data_export_from_mps_to_sap;

	if ($_CAN_EXPORT_SAP) $_MESSAGES[] = "Company $company->company can export items in SAP";
	else $_MESSAGES[] = "Company $company->company can NOT export items in SAP";
}


// distributed quantities ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NOT_CONFIRMED_ITEMS = array();
$_TOTAL_ITEM_QUANTITIES = array();
$_SAP_INVOLVED_ITEMS = array();
$_SAP_INVOLVED_POS = array();
$_SAP_INVOLVED_WAREHOUSES = array();

if (!$_ERRORS) {

	$result = $model->query("
		SELECT 
			mps_ordersheet_item_delivered_id,
			mps_ordersheet_item_delivered_ordersheet_item_id,
			mps_ordersheet_item_delivered_posaddress_id,
			mps_ordersheet_item_delivered_warehouse_id,
			mps_ordersheet_item_delivered_quantity,
			mps_ordersheet_item_delivered_confirmed
		FROM mps_ordersheet_item_delivered
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id
		WHERE mps_ordersheet_item_ordersheet_id = $ordersheet->id
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
		
			$key = $row['mps_ordersheet_item_delivered_id'];
			$item = $row['mps_ordersheet_item_delivered_ordersheet_item_id'];
			$pos = $row['mps_ordersheet_item_delivered_posaddress_id'];
			$warehouse = $row['mps_ordersheet_item_delivered_warehouse_id'];
			$quantity = $row['mps_ordersheet_item_delivered_quantity'];

			// sum all distributed quantities
			$_TOTAL_ITEM_QUANTITIES[$item] = $_TOTAL_ITEM_QUANTITIES[$item] + $quantity;
		
			// quantity is not confirmed
			if (!$row['mps_ordersheet_item_delivered_confirmed']) {
					
				// distributed items
				$_NOT_CONFIRMED_ITEMS[$key]['item'] = $item;
				$_NOT_CONFIRMED_ITEMS[$key]['quantity'] = $quantity;
					
				// SAP sales order
				if ($_CAN_EXPORT_SAP) {
					
					$_SAP_INVOLVED_ITEMS[$item] = true;
					
					if ($pos) {
						$_NOT_CONFIRMED_ITEMS[$key]['pos'] = $pos;
						$_SAP_INVOLVED_POS[$pos] = true;
					}
					
					if ($warehouse) {
						$_NOT_CONFIRMED_ITEMS[$key]['warehouse'] = $warehouse;
						$_SAP_INVOLVED_WAREHOUSES[$warehouse] = true;
					}
				}

			}

		}
	}

	$_DATA['not.confirmed.items'] = $_NOT_CONFIRMED_ITEMS;
	$_DATA['sap.involved.items'] = $_SAP_INVOLVED_ITEMS;
	$_DATA['sap.involved.pos'] = $_SAP_INVOLVED_POS;
	$_DATA['sap.involved.warehouses'] = $_SAP_INVOLVED_WAREHOUSES;
}


// sap mps necessary data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_MATERIALS = array();	
$_ORDERSHEET_ITEM_SHIPPED = array();

if (!$_ERRORS && $_CAN_EXPORT_SAP && $_SAP_INVOLVED_ITEMS) {
		
	// involved order sheet items in sap export
	$keys = join(',',array_keys($_SAP_INVOLVED_ITEMS));
			
	// order sheet items shipped
	$result = $model->query("
		SELECT DISTINCT 
			mps_ordersheet_item_shipped_ordersheet_item_id AS item,
			mps_ordersheet_item_shipped_po_number AS pon,
			mps_ordersheet_item_shipped_line_number AS line
		FROM mps_ordersheet_item_shipped
		WHERE mps_ordersheet_item_shipped_ordersheet_item_id IN ($keys)		
		ORDER BY mps_ordersheet_item_shipped_date DESC
	")->fetchAll();
			
	$_ORDERSHEET_ITEM_SHIPPED = _array::datagrid($result);
	$_DATA['ordersheet.item.shipped'] = $_ORDERSHEET_ITEM_SHIPPED;		

	// materials
	$result = $model->query("
		SELECT DISTINCT
			mps_ordersheet_item_id AS id,
			mps_material_ean_number AS ean,
			mps_material_material_planning_type_id AS planning
		FROM mps_ordersheet_items
		INNER JOIN mps_materials ON mps_material_id = mps_ordersheet_item_material_id
		WHERE mps_ordersheet_item_id IN ($keys)		
	")->fetchAll();
			
	if ($result) {
		
		foreach ($result as $row) {
			$key = $row['id'];
			$_MATERIALS[$key]['ean'] = $row['ean'];	
			$_MATERIALS[$key]['planning'] = $row['planning'];	
		}
	}	

	$_DATA['materials'] = $_MATERIALS;	
}


// sap pos necessary data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_POS = array();

if (!$_ERRORS && $_CAN_EXPORT_SAP && $_SAP_INVOLVED_POS) {
			
	$keys = join(',',array_keys($_SAP_INVOLVED_POS));
	
	$result = $model->query("
		SELECT DISTINCT
			posaddress_id,
			posaddress_sapnumber,
			posaddress_sap_shipto_number,
			address_sap_salesorganization
		FROM db_retailnet.posaddresses
		INNER JOIN db_retailnet.addresses ON address_id = posaddress_client_id
		WHERE posaddress_id IN($keys)
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$key = $row['posaddress_id'];

			$_POS[$key] = array(
				'sap' => $row['posaddress_sapnumber'],
				'shipp' => $row['posaddress_sap_shipto_number'],
				'organization' => $row['address_sap_salesorganization']
			);
		}
	}

	$_DATA['pos'] = $_POS;	
}


// sap warehouse necessary data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_WAREHOUSES = array();

if (!$_ERRORS && $_CAN_EXPORT_SAP && $_SAP_INVOLVED_WAREHOUSES) {

	$keys = join(',',array_keys($_SAP_INVOLVED_WAREHOUSES));

	$result = $model->query("
		SELECT DISTINCT
			mps_ordersheet_warehouse_id,
			address_warehouse_sap_customer_number,
			address_warehouse_sap_shipto_number,
			address_sap_salesorganization
		FROM db_retailnet.address_warehouses
		INNER JOIN mps_ordersheet_warehouses ON mps_ordersheet_warehouse_address_warehouse_id = address_warehouse_id
		INNER JOIN db_retailnet.addresses ON address_id = address_warehouse_address_id
		WHERE mps_ordersheet_warehouse_id IN ($keys) AND mps_ordersheet_warehouse_stock_reserve <> 1		
	")->fetchAll();
	
	if ($result) {
		
		foreach ($result as $row) {
			
			$key = $row['mps_ordersheet_warehouse_id'];

			$_WAREHOUSES[$key] = array(
				'sap' => $row['address_warehouse_sap_customer_number'],
				'shipp' => $row['address_warehouse_sap_shipto_number'],
				'organization' => $row['address_sap_salesorganization']
			);
		}
	}

	$_DATA['warehouses'] = $_WAREHOUSES;	
}


// sap orders ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PARAMETERS = array();
$_DIVISIONS = array();
$_ORDERS = array();
$_USED_PONS = array();

if (!$_ERRORS && $_CAN_EXPORT_SAP && $_NOT_CONFIRMED_ITEMS) {

	// divisions
	$_DIVISIONS = array(
		'mps' => '08'
	);

	// get sap parameters
	$result = $model->query("
		SELECT
			sap_parameter_name AS param, 
			sap_parameter_value AS value
		FROM db_retailnet.sap_parameters
	")->fetchAll();

	$_PARAMETERS = _array::extract($result);
	$_DATA['parameters'] = $_PARAMETERS;

	// get order types dependent on planning types
	$result = $model->query("
		SELECT 
			sap_order_type_client_planning_type_id AS id,
			TRIM(sap_order_type_name) AS name
		FROM db_retailnet.sap_order_type_clients
		INNER JOIN db_retailnet.sap_order_types ON sap_order_type_id = sap_order_type_client_order_type_id
		WHERE sap_order_type_client_address_id = $ordersheet->address_id
	")->fetchAll();

	$_ORDER_TYPES = _array::extract($result);

			
	foreach ($_NOT_CONFIRMED_ITEMS as $key => $row) {
				
		$item = $row['item'];
		$pos = $row['pos'];
		$warehouse = $row['warehouse'];

		$planning = $_MATERIALS[$item]['planning'];
		$orderType = $_ORDER_TYPES[$planning] ?: $_PARAMETERS['order_type'];

		$order = array();

		// order sheet item
		$order['sap_salesorder_item_mps_item_id'] = $key; 

		// order division code
		if ($_DIVISIONS[$application]) $order['sap_salesorder_item_division'] = $_DIVISIONS[$application];
		else $_ERRORS[] = "Missing division code for application $application";
			
		// order sheet quantity
		if ($row['quantity']) $order['sap_salesorder_itemr_quantity'] = $row['quantity'];	
		else $_ERRORS[] = "Missing item quantiti ($key)";
		
		// material ean number
		if ($_MATERIALS[$item]['ean']) $order['sap_salesorder_item_ean_number'] = $_MATERIALS[$item]['ean'];
		else $_ERRORS[] = "Missing item EAN number ($item)";
		
		// shipped quantity orer line
		if ($_ORDERSHEET_ITEM_SHIPPED[$item]['line']) $order['sap_salesorder_item_mps_po_line_number'] = $_ORDERSHEET_ITEM_SHIPPED[$item]['line'];
		else $_ERRORS[] = "Missing order sheet item shipped order line ($key)";

		// sales orders organization
		if ($_POS[$pos]['organization']) $order['sap_salesorder_item_sales_organization'] = $_POS[$pos]['organization'];
		elseif($_WAREHOUSES[$warehouse]['organization']) $order['sap_salesorder_item_sales_organization'] = $_WAREHOUSES[$warehouse]['organization'];
		else $_ERRORS[] = "Missing sales order organization (pos: $pos, warehouse: $warehouse)";

		// sold to number
		if ($_POS[$pos]['sap']) $order['sap_salesorder_item_sold_to'] = $_POS[$pos]['sap'];
		elseif($_WAREHOUSES[$warehouse]['sap']) $order['sap_salesorder_item_sold_to'] = $_WAREHOUSES[$warehouse]['sap'];
		else $_ERRORS[] = "Missing soldto number (pos: $pos, warehouse: $warehouse)";

		// shipto number
		if ($_POS[$pos]['shipp']) $order['sap_salesorder_item_ship_to'] = $_POS[$pos]['shipp'];
		elseif($_WAREHOUSES[$warehouse]['shipp']) $order['sap_salesorder_item_ship_to'] = $_WAREHOUSES[$warehouse]['shipp'];
		else $_ERRORS[] = "Missing shipto number (pos: $pos, warehouse: $warehouse)";

		// order reason
		if ($_PARAMETERS['order_reason']) $order['sap_salesorder_item_order_reason'] = $_PARAMETERS['order_reason'];
		else $_ERRORS[] = "Missing sap parameter order reason";
		
		// order type
		if ($orderType) $order['sap_salesorder_item_order_type'] = $orderType;
		else $_ERRORS[] = "Missing sap parameter order type";
		
		// discount code
		if ($_PARAMETERS['discount_code']) $order['sap_salesorder_item_discount_code'] = $_PARAMETERS['discount_code'];
		else $_ERRORS[] = "Missing sap parameter discount code";
		
		// discount percentage
		if ($_PARAMETERS['discount_percentage']) $order['sap_salesorder_item_discount_percentage'] = $_PARAMETERS['discount_percentage'];
		else $_ERRORS[] = "Missing sap parameter discount percentage";
	

		if (!$_ERRORS) {
			$_ORDERS[$key] = $order;
		} else {

			$_CAN_EXPORT_SAP = false;
			$_CAN_CONFIRM_ITEMS = false;
			
			$notification = "Confirm Distribution not possible, please check followin data:<br>";
					
			if (!$order['sap_salesorder_item_sales_organization']) 	$notification .= "<br>- Company Sales Organization";
			if (!$order['sap_salesorder_item_sold_to']) 			$notification .= "<br>- POS SAP Number";
			if (!$order['sap_salesorder_item_mps_po_line_number']) 	$notification .= "<br>- Order Sheet Purchase Order Line Number";
			if (!$order['sap_salesorder_item_ean_number']) 			$notification .= "<br>- Item EAN Number";
			if (!$order['sap_salesorder_itemr_quantity']) 			$notification .= "<br>- Item Quantity";
			if (!$order['sap_salesorder_item_discount_code']) 		$notification .= "<br>- SAP Discount Code";
			if (!$order['sap_salesorder_item_discount_percentage']) $notification .= "<br>- SAP Discount Percentage";
			if (!$order['sap_salesorder_item_order_reason']) 		$notification .= "<br>- SAP order reason";
			if (!$order['sap_salesorder_item_order_type']) 			$notification .= "<br>- SAP order type";
			if (!$order['sap_salesorder_item_division'] ) 			$notification .= "<br>- SAP Division";

			$_NOTIFICATIONS[] = $message;
			
			break;
		}
	}

	$_DATA['orders'] = $_ORDERS;
}		


// confirm distributed items :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_CONFIRMED_ITEMS = array();
	
if (!$_ERRORS && $_CAN_CONFIRM_ITEMS && $_NOT_CONFIRMED_ITEMS) {

	$sth = $model->db->prepare("
		UPDATE mps_ordersheet_item_delivered SET
			mps_ordersheet_item_delivered_confirmed = CURRENT_DATE(),
			user_modified = ?,
			date_modified = NOW()
		WHERE mps_ordersheet_item_delivered_id = ?
	");

	$_MESSAGES[] = "<< START CONFIRMATION >>";
	
	foreach ($_NOT_CONFIRMED_ITEMS as $item => $row) {
		
		$update = true;

		if (!DEBUGGING_MODE) {
			$update = $sth->execute(array($user->login, $item));
		}

		if ($update) {
			$_CONFIRMED_ITEMS[$item] = $row;
			$_MESSAGES[] = "Item $item is confirmed";
		}
		else $_ERRORS[] = "Item $item is not confirmed;";
	}
	
	$_MESSAGES[] = "<< END CONFIRMATION >>";
}
		

// update order sheet distributed quantities :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
if (!$_ERRORS && $_CONFIRMED_ITEMS && $_TOTAL_ITEM_QUANTITIES) {
	
	$sth = $model->db->prepare("
		UPDATE mps_ordersheet_items SET
			mps_ordersheet_item_quantity_distributed = ?,
			user_modified = ?,
			date_modified = NOW()
		WHERE mps_ordersheet_item_id = ?
	");

	$_MESSAGES[] = "<< START UPDATE ORDER SHEET ITEM DISTRIBUTED QUANTITY >>";
	
	foreach ($_TOTAL_ITEM_QUANTITIES as $item => $quantity) {
		
		if (!DEBUGGING_MODE) {
			$sth->execute(array($quantity, $user->login, $item));
		}
		
		$_MESSAGES[] = "Update item $item with quantiy $quantity";
	}
	
	$_MESSAGES[] = "<< END UPDATE ORDER SHEET ITEM DISTRIBUTED QUANTITY >>";
}



// upgrade order sheet state :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && $_CONFIRMED_ITEMS) {

	$workflowState = new Workflow_State($application);
	$tracking = new User_Tracking($application);


	// set order sheet state in distribution
	if ($ordersheet->state()->isShipped() || $ordersheet->state()->isPartiallyShipped()) {

		$workflowState->read(Workflow_State::STATE_IN_DISTRIBUTION);

		if (!DEBUGGING_MODE) {
			
			$ordersheet->update(array(
				'mps_ordersheet_workflowstate_id' => Workflow_State::STATE_IN_DISTRIBUTION,
				'user_modified' => $user->login
			));
		
			$tracking->create(array(
				'user_tracking_entity' => 'order sheet',
				'user_tracking_entity_id' => $ordersheet->id,
				'user_tracking_user_id' => $user->id,
				'user_tracking_action' => $workflowState->name,
				'user_created' => $user->login,
				'date_created' => date('Y-m-d H:i:s')
			));
		}

		$_MESSAGES[] = "Set Order Sheet workflow state as $workflowState->name";
	}


	// get order sheet item quantities
	$items = $model->query("
		SELECT 
			mps_ordersheet_item_id AS id,
			mps_ordersheet_item_quantity_shipped AS shipped,
			mps_ordersheet_item_quantity_distributed AS distributed
		FROM mps_ordersheet_items
		WHERE mps_ordersheet_item_ordersheet_id = $ordersheet->id AND mps_ordersheet_item_quantity_shipped  > 0
	")->fetchAll();
		
	if ($items) {
			
		$distributed = 0;				
			
		// count distributed items
		foreach ($items as $row) {
			$item = $row['id'];
			if ($row['shipped']==$row['distributed']) $distributed++;
		}
			
		// if all items are distributed
		// set order sheet as distributed
		if (count($items)==$distributed) {

			$state = ($ordersheet->exchangeOrderCompleted()) ? Workflow_State::STATE_DISTRIBUTED : Workflow_State::STATE_PARTIALLY_DISTRIBUTED;
			$workflowState->read($state);
			
			if (!DEBUGGING_MODE) {
				
				// update order sheet state
				$ordersheet->update(array(
					'mps_ordersheet_workflowstate_id' => $state,
					'user_modified' => $user->login,
					'date_modified' => date('Y-m-d H:i:s')
				));

				// user tracking
				$tracking->create(array(
					'user_tracking_entity' => 'order sheet',
					'user_tracking_entity_id' => $ordersheet->id,
					'user_tracking_user_id' => $user->id,
					'user_tracking_action' => $workflowState->name,
					'user_created' => $user->login,
					'date_created' => date('Y-m-d H:i:s')
				));
			}

			$_MESSAGES[] = "Set ordersheet workflow state as $workflowState->name";
		}
	}
}


// upgrade master sheet states :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && $_CONFIRMED_ITEMS) {

	// get all master sheet orders
	$result = $model->query("
		SELECT 
			mps_ordersheet_id AS id, 
			mps_ordersheet_workflowstate_id As state
		FROM mps_ordersheets
		WHERE mps_ordersheet_mastersheet_id = $ordersheet->mastersheet_id
	")->fetchAll();

	if ($result) {

		$totalOrdersheets = count($result);
		$totalArchivedOrdersheets = 0;

		// sum archived order sheets
		foreach ($result as $row) {
			if ($row['state']==Workflow_State::STATE_DISTRIBUTED || $row['state']==Workflow_State::STATE_PARTIALLY_DISTRIBUTED) {
				$totalArchivedOrdersheets++;
			}
		}

		// if all order sheet are archived
		// send master sheeet also to archive
		if ($totalOrdersheets==$totalArchivedOrdersheets) {
			
			$mastersheet = new Mastersheet($application);
			$mastersheet->read($ordersheet->mastersheet_id);

			if (!DEBUGGING_MODE) {
				$mastersheet->update(array(
					'mps_mastersheet_archived' => 1,
					'user_modified' => $user->login,
					'date_modifeid' => date('Y-m-d H:i:s')
				));
			}
			
			$_MESSAGES[] = "Master Sheet $mastersheet->name is archived.";
		}
	}
}


// sales orders ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
$_SALES_ORDERS = array();

if (!$_ERRORS && $_CAN_EXPORT_SAP && $_ORDERS) {

	// get all planning type distributors
	$result = $model->query("
		SELECT 
			sap_distributor_planning_type_id AS type,
			sap_distribution_channel_code AS distributor
		FROM db_retailnet.sap_distribution_channels 
		INNER JOIN db_retailnet.sap_distributors ON sap_distribution_channels.sap_distribution_channel_id = sap_distributors.sap_distributor_channel_id
		WHERE sap_distributor_address_id = $ordersheet->address_id
	")->fetchAll();

	$distributors = _array::extract($result);
	$_DATA['distributors'] = $distributors;

	// get items planning types
	$result = $model->query("
		SELECT 
			mps_ordersheet_item_delivered_id AS id,
			mps_material_material_planning_type_id AS type
		FROM mps_ordersheet_item_delivered 
		INNER JOIN mps_ordersheet_items ON mps_ordersheet_item_id = mps_ordersheet_item_delivered_ordersheet_item_id
		INNER JOIN mps_materials ON mps_ordersheet_item_material_id = mps_materials.mps_material_id
		WHERE mps_ordersheet_item_quantity_shipped > 0 AND mps_ordersheet_item_ordersheet_id = $ordersheet->id
	")->fetchAll();

	$itemPlanningTypes = _array::extract($result);
	$_DATA['planning.types'] = $itemPlanningTypes;

	// group items by distributors
	foreach ($_ORDERS as $item => $order) {
		$type = $itemPlanningTypes[$item];
		$distributor = $distributors[$type] ?: '03';
		$_SALES_ORDERS[$distributor][$item] = $order;
	}

	$_DATA['sales.orders'] = $_SALES_ORDERS;
}


// generate sap export orders ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
$_EXPORTS = array();

if (!$_ERRORS && $_CAN_EXPORT_SAP && $_SALES_ORDERS) {

	// timestamp as purchase order number
	$pon = $_SERVER['REQUEST_TIME'];

	// get all existed purchase order numbers
	$result = $model->query("
		SELECT DISTINCT
			LEFT(mps_ordersheet_item_delivered_sap_purchase_order_number, 10) AS pon, 
			mps_ordersheet_item_delivered_id AS id
		FROM mps_ordersheet_item_delivered
		WHERE mps_ordersheet_item_delivered_sap_purchase_order_number IS NOT NULL 
		OR mps_ordersheet_item_delivered_sap_purchase_order_number != ''
	")->fetchAll();

	$pons = _array::extract($result);
	
	foreach ($_SALES_ORDERS as $distributor => $orders) {
		
		$pon++;
		if ($pons[$pon]) $pon++;

		foreach ($orders as $item => $order) {
			
			// purchase number
			$number  = array();
			$number[] = $pon;
			$number[] = $order['sap_salesorder_item_sold_to'];
			$number[] = $order['sap_salesorder_item_ship_to'];

			// export data
			$order['sap_salesorder_item_mps_po_number'] = join('.', array_filter($number));
			$order['sap_salesorder_item_distribution_by'] = $distributor;
			$_EXPORTS[] = $order;

			// register pon
			$pons[$pon] = $item;
		}
	}

	$_DATA['exports'] = $_EXPORTS;
}



// sap export orders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_NEW_ITEM_PONS = array();

if (!$_ERRORS && $_CAN_EXPORT_SAP && $_EXPORTS) {
		
	// db exchange model
	$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);
			
	$_MESSAGES[] = "START EXPORT ORDERS";
			
	// sales order statement
	$sth = $exchange->db->prepare("
		INSERT INTO sap_salesorder_items (
			sap_salesorder_item_mps_item_id,
			sap_salesorder_item_order_reason,
			sap_salesorder_item_order_type,
			sap_salesorder_item_sales_organization,
			sap_salesorder_item_distribution_by,
			sap_salesorder_item_division,
			sap_salesorder_item_sold_to,
			sap_salesorder_item_ship_to,
			sap_salesorder_item_mps_po_number,
			sap_salesorder_item_mps_po_line_number,
			sap_salesorder_item_ean_number,
			sap_salesorder_itemr_quantity,
			sap_salesorder_item_discount_code,
			sap_salesorder_item_discount_percentage
		) VALUES (
			:sap_salesorder_item_mps_item_id,
			:sap_salesorder_item_order_reason,
			:sap_salesorder_item_order_type,
			:sap_salesorder_item_sales_organization,
			:sap_salesorder_item_distribution_by,
			:sap_salesorder_item_division,
			:sap_salesorder_item_sold_to,
			:sap_salesorder_item_ship_to,
			:sap_salesorder_item_mps_po_number,
			:sap_salesorder_item_mps_po_line_number,
			:sap_salesorder_item_ean_number,
			:sap_salesorder_itemr_quantity,
			:sap_salesorder_item_discount_code,
			:sap_salesorder_item_discount_percentage
		)		
	");
			
	if ($settings->devmod) {
		
		// simulatet sap confirmed orders
		$sthConfirmed = $exchange->db->prepare("
			INSERT INTO sap_confirmed_items (
				sap_confirmed_item_action,
				sap_confirmed_item_order_type,
				sap_confirmed_item_sales_organization,
				sap_confirmed_item_distributed_by,
				sap_confirmed_item_division,
				sap_confirmed_item_sold_to,
				sap_confirmed_item_ship_to,
				sap_confirmed_item_sap_order_number,
				sap_confirmed_item_sap_order_line_number,
				sap_confirmed_item_mps_po_number,
				sap_confirmed_item_ean_number,
				sap_confirmed_item_quantity,
				sap_confirmed_item_confirmed_quantity,
				sap_confirmed_item_mps_po_line_number,
				sap_confirmed_item_date
			)
			VALUES (
				:sap_confirmed_item_action,
				:sap_confirmed_item_order_type,
				:sap_confirmed_item_sales_organization,
				:sap_confirmed_item_distributed_by,
				:sap_confirmed_item_division,
				:sap_confirmed_item_sold_to,
				:sap_confirmed_item_ship_to,
				:sap_confirmed_item_sap_order_number,
				:sap_confirmed_item_sap_order_line_number,
				:sap_confirmed_item_mps_po_number,
				:sap_confirmed_item_ean_number,
				:sap_confirmed_item_quantity,
				:sap_confirmed_item_confirmed_quantity,
				:sap_confirmed_item_mps_po_line_number,
				:sap_confirmed_item_date
			)
		");
		
		// simulatet sap shipped orders
		$sthShipped = $exchange->db->prepare("
			INSERT INTO sap_shipped_items (
				sap_shipped_item_sap_delivery_number,
				sap_shipped_item_date_shipped,
				sap_shipped_item_shipped_to,
				sap_shipped_item_sap_order_number,
				sap_shipped_item_sap_order_line_number,
				sap_shipped_item_mps_po_number,
				sap_shipped_item_mps_po_line_number,
				sap_shipped_item_ean_number,
				sap_shipped_item_quantity
			)
			VALUES (
				:sap_shipped_item_sap_delivery_number,
				:sap_shipped_item_date_shipped,
				:sap_shipped_item_shipped_to,
				:sap_shipped_item_sap_order_number,
				:sap_shipped_item_sap_order_line_number,
				:sap_shipped_item_mps_po_number,
				:sap_shipped_item_mps_po_line_number,
				:sap_shipped_item_ean_number,
				:sap_shipped_item_quantity
			)");
	}
			
	foreach ($_EXPORTS as $order) {	

		// export order to ramco
		$insert = DEBUGGING_MODE ? true : $sth->execute($order);
		
		if ($insert) {
			
			$item = $order['sap_salesorder_item_mps_item_id'];
			$pon = $order['sap_salesorder_item_mps_po_number'];
			$_NEW_ITEM_PONS[$item] = $pon;

			// inserted id
			$inserted = $exchange->db->lastInsertId();

			$_MESSAGES[] = "Export sales order $inserted";
			
			if ($settings->devmod && !DEBUGGING_MODE) {

				$confirmedQuantity = $order['sap_salesorder_itemr_quantity'];
				$shippedQuantity = $order['sap_salesorder_itemr_quantity'];
				
				if ($order['sap_salesorder_itemr_quantity'] > 100) {
					$confirmedQuantity = $order['sap_salesorder_itemr_quantity'];
					$shippedQuantity =  ceil($order['sap_salesorder_itemr_quantity']/2);
				}
				
				$sthConfirmed->execute(array(
					'sap_confirmed_item_action' => '004',
					'sap_confirmed_item_order_type' => $order['sap_salesorder_item_order_type'],
					'sap_confirmed_item_sales_organization' => $order['sap_salesorder_item_sales_organization'],
					'sap_confirmed_item_distributed_by' => $order['sap_salesorder_item_distribution_by'],
					'sap_confirmed_item_division' => $order['sap_salesorder_item_division'],
					'sap_confirmed_item_sold_to' => $order['sap_salesorder_item_sold_to'],
					'sap_confirmed_item_ship_to' => $order['sap_salesorder_item_ship_to'],
					'sap_confirmed_item_sap_order_number' => null,
					'sap_confirmed_item_sap_order_line_number' => null,
					'sap_confirmed_item_mps_po_number' => $order['sap_salesorder_item_mps_po_number'],
					'sap_confirmed_item_ean_number' => $order['sap_salesorder_item_ean_number'],
					'sap_confirmed_item_quantity' => $order['sap_salesorder_itemr_quantity'],
					'sap_confirmed_item_confirmed_quantity' => $confirmedQuantity,
					'sap_confirmed_item_mps_po_line_number' => $order['sap_salesorder_item_mps_po_line_number'],
					'sap_confirmed_item_date' => date('Y-m-d')
				));
				
				$sthShipped->execute(array(
					'sap_shipped_item_sap_delivery_number' => '004',
					'sap_shipped_item_date_shipped' => date('Y-m-d'),
					'sap_shipped_item_shipped_to' => $order['sap_salesorder_item_sold_to'],
					'sap_shipped_item_sap_order_number' => null,
					'sap_shipped_item_sap_order_line_number' => null,
					'sap_shipped_item_mps_po_number' => $order['sap_salesorder_item_mps_po_number'],
					'sap_shipped_item_mps_po_line_number' => $order['sap_salesorder_item_mps_po_line_number'],
					'sap_shipped_item_ean_number' => $order['sap_salesorder_item_ean_number'],
					'sap_shipped_item_quantity' => $shippedQuantity
				));
			}

		} else {
			$pon = $order['sap_salesorder_item_mps_po_number'];
			$_ERRORS[] = "Salse order $pon NOT exported";
		}
	}
	
	$_MESSAGES[] = "END EXPORT ORDERS";
}


// update item purchase order number :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_ERRORS && $_NEW_ITEM_PONS) {

	$_MESSAGES[] = "<< START UPDATE NEW PO NUMBERS";

	$sth = $model->db->prepare("
		UPDATE mps_ordersheet_item_delivered SET 
			mps_ordersheet_item_delivered_sap_purchase_order_number = ?
		WHERE mps_ordersheet_item_delivered_id = ?
	");

	foreach ($_NEW_ITEM_PONS as $item => $pon) {
		
		if (!DEBUGGING_MODE) {
			$sth->execute(array($pon, $item));
		}

		$_MESSAGES[] = "Update item $item whith po number $pon";
	}

	$_MESSAGES[] = "<< END UPDATE NEW PO NUMBERS";
}
		

// resonding :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
if (!DEBUGGING_MODE) {	
	
	$json['response'] = $_ERRORS ? false : $_CAN_CONFIRM_ITEMS;
			
	if (!$_ERRORS) {
		
		if ($_CONFIRMED_ITEMS) {
			message::request_submitted();
			$json['reload'] = true;
		} else {
			$message = $message ?: "Unconfirmed quantities not found";
			$json['message'] = $message;
			$json['response'] = true;
		}

	} else {
		$json['response'] = false;
		$json['message'] = $translate->message_request_failure;
	}

	$json['message'] .= $_NOTIFICATIONS ? join('<br />', $_NOTIFICATIONS) : null;

} else {
	//echo "<pre>";
	$json['errors'] = $_ERRORS;
	$json['messages'] = $_MESSAGES;
	$json['notifications'] = $_NOTIFICATIONS;
	$json['data'] = $_DATA;
	//print_r($json);
}

// response with json header
header('Content-Type: text/json');
echo json_encode($json);
	