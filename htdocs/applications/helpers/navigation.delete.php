<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	if ($id) {
		
		// load all navigations
		$result = Navigation::load();	
		$navigations = Navigation::buildTree($result);
			
		// current navigation
		$navigation = new Navigation();
		$navigation->read($id);
		
		// current navigations childs
		$navigation->get_childs();
		$childs = $navigation->childs;
		
		// remove navigation
		$action = $navigation->delete($id);
		
		if ($action) {

			$model = new Model(Connector::DB_CORE);
			
			$model->db->query("
				DELETE FROM navigation_languages
				WHERE navigation_language_navigation = $id
			");
				
			$model->db->query("
				DELETE FROM role_navigations
				WHERE role_navigation_navigation = $id
			");
			
			if ($childs)  {
				
				foreach ($childs as $key) {
					$model->db->query("
						DELETE FROM navigations
						WHERE navigation_id = $key
					");
					$model->db->query("
						DELETE FROM navigation_languages
						WHERE navigation_language_navigation = $key
					");
					$model->db->query("
						DELETE FROM role_navigations
						WHERE role_navigation_navigation = $key
					");
				}
			}
		}
		
		Message::request_deleted();
		url::redirect("/$appliction/$controller");
		
	} else {
		Message::request_failure();
		url::redirect("/$appliction/$controller/$action/$id");
	}
	