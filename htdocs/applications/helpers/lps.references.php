<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	$model = new Model($application);
	
	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : '
		lps_collection_code, 
		lps_collection_category_code, 
		lps_reference_code
	';
	
	// sql direction
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
	
	// editing
	$canedit = ($user->permission(Material::PERMISSION_LTM_EDIT)) ? true : false;
	
	// sql join tables
	$binds = array(
		'LEFT JOIN lps_product_groups ON lps_product_group_id = lps_reference_product_group_id',
		'LEFT JOIN lps_product_subgroups ON lps_product_subgroup_id = lps_reference_product_subgroup_id',
		'LEFT JOIN lps_product_types ON lps_product_type_id = lps_reference_product_type_id',
		'LEFT JOIN lps_collections ON lps_collection_id = lps_reference_collection_id',
		'LEFT JOIN lps_collection_categories ON lps_collection_category_id = lps_reference_collection_category_id'
	);

	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			lps_collection_code LIKE \"%$keyword%\" 
			OR lps_collection_category_code LIKE \"%$keyword%\" 
			OR lps_product_group_name LIKE \"%$keyword%\" 
			OR lps_product_subgroup_name LIKE \"%$keyword%\" 
			OR lps_product_type_name LIKE \"%$keyword%\" 
			OR lps_reference_code LIKE \"%$keyword%\" 
			OR lps_reference_name LIKE \"%$keyword%\"
		)";
	}
	
	// filter: product group
	if ($_REQUEST['product_group']) {
		$filters['product_group'] = "lps_reference_product_group_id = ".$_REQUEST['product_group'];
	}
	
	// filter: product subgrup
	if ($_REQUEST['product_subgroup']) {
		$filters['product_subgroup'] = "lps_reference_product_type_id = ".$_REQUEST['product_subgroup'];
	}
	
	// filter: product type
	if ($_REQUEST['product_type']) {
		$filters['product_type'] = "mps_material_material_collection_category_id = ".$_REQUEST['product_type'];
	}
	
	// filter: collection
	if ($_REQUEST['collection']) {
		$filters['collection'] = "lps_reference_collection_id = ".$_REQUEST['collection'];
	}
	
	// filter: collection
	if ($_REQUEST['collection_category']) {
		$filters['collection_category'] = "lps_reference_collection_category_id = ".$_REQUEST['collection_category'];
	}
	
	// filter: active
	$active = ($archived) ? 0 : 1;
	$filters['active'] = "lps_reference_active = $active";
	
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			lps_reference_id,
			lps_collection_code,
			lps_collection_category_code,
			lps_product_group_code,
			lps_reference_code,
			lps_reference_name,
			lps_reference_price_point
		FROM mps_references
	")
	->bind($binds)
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();
	
	if ($result) {
	
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
	
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
	
	// toolbox: utton print
	if ($datagrid && $_REQUEST['export']) {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'label' => $translate->print,
			'href' => $_REQUEST['export']
		));
	}
	
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();


	// dropdown: collections
	$result = $model->query("
		SELECT DISTINCT 
			lps_collection_id, 
			lps_collection_code 
		FROM lps_references
	")
	->bind($binds)
	->filter($filters)
	->filter('notempty', 'lps_collection_id > 0')
	->exclude('collection')
	->order('lps_collection_code')
	->fetchAll();

	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'collection',
			'id' => 'collection',
			'class' => 'submit',
			'value' => $_REQUEST['collection'],
			'caption' => $translate->all_collections
		));
	}

		
	// dropdown: collection categories
	$result = $model->query("
		SELECT DISTINCT 
			lps_collection_category_id, 
			lps_collection_category_code 
		FROM lps_references
	")
	->bind($binds)
	->filter($filters)
	->filter('notempty', 'lps_collection_category_id > 0')
	->exclude('collection_category')
	->order('lps_collection_category_code')
	->fetchAll();
	
	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'collection_category',
			'id' => 'collection_category',
			'class' => 'submit',
			'value' => $_REQUEST['collection_category'],
			'caption' => $translate->all_collection_categories
		));
	}
	

	// dropdown: product groups
	$result = $model->query("
		SELECT DISTINCT 
			lps_product_group_id, 
			lps_product_group_name 
		FROM lps_references
	")
	->bind($binds)
	->filter($filters)
	->filter('notempty', 'lps_reference_product_group_id > 0')
	->exclude('product_group')
	->order('lps_product_group_name')
	->fetchAll();
	
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'product_group',
		'id' => 'product_group',
		'class' => 'submit',
		'value' => $_REQUEST['product_group'],
		'caption' => 'All Product Groups'
	));
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	
	$table->lps_collection_code(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=5%'
	);
	
	$table->lps_collection_category_code(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=5%'
	);
	
	$table->lps_product_group_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=5%'
	);
	
	$table->lps_reference_code(
		Table::PARAM_SORT,
		"href=".$_REQUEST['form']
	);
	
	$table->lps_reference_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=5%'
	);
	
	$table->lps_reference_price_point(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=5%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	
	