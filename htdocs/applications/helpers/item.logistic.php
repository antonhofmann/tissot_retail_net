<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();
$db = Connector::get();

$item = $_REQUEST['item'];

if ($item && isset($_REQUEST['number'])) {

	$sth = $db->prepare("
		DELETE FROM item_packaging
		WHERE item_packaging_item_id = ?
	");

	$sth->execute(array($item));

	$sth = $db->prepare("
		INSERT INTO item_packaging (
			item_packaging_item_id,
			item_packaging_unit_id,
			item_packaging_packaging_id,
			item_packaging_number,
			item_packaging_length,
			item_packaging_width,
			item_packaging_height,
			item_packaging_weight_net,
			item_packaging_weight_gross,
			user_created
		) 
		VALUES (?,?,?,?,?,?,?,?,?,?)
	");

	foreach ($_REQUEST['number'] as $i => $value) {
		
		$number = $value;
		$unit = $_REQUEST['unit'][$i];
		$packaging = $_REQUEST['packaging'][$i];
		$length = $_REQUEST['length'][$i];
		$width = $_REQUEST['width'][$i];
		$height = $_REQUEST['height'][$i];
		$weightNet = $_REQUEST['weight_net'][$i];
		$weightGross = $_REQUEST['weight_gross'][$i];

		// check empty row
		if (!$number && !$unit && !$packaging && !$length && !$width && !$height && !$weightNet && !$weightGross) {
			$removeFields['#itemPackageInformations'][] = $i;
			continue;
		}

		$sth->execute(array($item, $unit, $packaging, $number, $length, $width, $height, $weightNet, $weightGross, $user->login));
	}
}


header('Content-Type: text/json');
echo json_encode(array(
	'response' => true,
	'message' => $message,
	'remove' => $removeFields
));
