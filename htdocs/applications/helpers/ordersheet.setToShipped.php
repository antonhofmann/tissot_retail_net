<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];

	
	if ($id) {
		
		$model = new Model($application);
		$tracker = new User_Tracking($application);
		$workflowState = new Workflow_State($application);
		$workflowState->read(Workflow_State::STATE_SHIPPED);
		
		// ordersheet
		$ordersheet = new Ordersheet($application);
		$ordersheet->read($id);
		
		
		//set workflow state to shipped

		$data['mps_ordersheet_workflowstate_id'] = Workflow_State::STATE_SHIPPED;
		$data['user_modified'] = $user->login;

		$response = $ordersheet->update($data);

		//update tracking
		
		$tracker->create(array(
				'user_tracking_entity' => 'order sheet',
				'user_tracking_entity_id' => $id,
				'user_tracking_user_id' => $user->id,
				'user_tracking_action' => $workflowState->name,
				'user_created' => $user->login,
				'date_created' => date('Y-m-d H:i:s')
			));
		
		//update items
		$model = new Model($application);

		$result = $model->query("
			SELECT *
			FROM mps_ordersheet_items
		")
		->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = $id")
		->fetchAll();
		
		if ($result) {
			foreach ($result as $row) {
			
				$item = $row["mps_ordersheet_item_id"];
				$ordersheet->item()->read($item);
				
				
				$ordersheet->item()->update(array(
					'mps_ordersheet_item_quantity_shipped' => $row["mps_ordersheet_item_quantity_confirmed"],
					'user_modified' => $user->login,
					'date_modified' => date('Y-m-d H:i:s')
				));
				

				//insert records inro table mps_ordersheet_item_shipped
				$result2 = $model->query("
					SELECT * 
					FROM mps_ordersheet_item_confirmed
					WHERE mps_ordersheet_item_confirmed_ordersheet_item_id = " . $row["mps_ordersheet_item_id"] . "
					ORDER BY mps_ordersheet_item_confirmed_id DESC
				")->fetchAll();

				if ($result2) {

					foreach ($result2 as $row2) {
						
						$addItem = $model->db->prepare("
							INSERT INTO mps_ordersheet_item_shipped (
								mps_ordersheet_item_shipped_ordersheet_item_id,
								mps_ordersheet_item_shipped_material_code,
								mps_ordersheet_item_shipped_po_number,
								mps_ordersheet_item_shipped_line_number,
								mps_ordersheet_item_shipped_date,
								mps_ordersheet_item_shipped_quantity,
								mps_ordersheet_item_shipped_order_state,
								mps_ordersheet_item_shortclosed,
								user_created,
								date_created
							) VALUES (?,?,?,?,?,?,?,?,?,NOW())
						");

						$data = array(
							$row2['mps_ordersheet_item_confirmed_ordersheet_item_id'],
							$row2['mps_ordersheet_item_confirmed_material_code'],
							$row2['mps_ordersheet_item_confirmed_po_number'],
							$row2['mps_ordersheet_item_confirmed_line_number'],
							date("Y-m-d"),
							$row2['mps_ordersheet_item_confirmed_quantity'],
							'03',
							'0',
							$user->login
						);
						
						$inserted = $addItem->execute($data);
						
					}
				}	
			}

		}
		
	}
	url::redirect("/$application/$controller/$action/$id");
	