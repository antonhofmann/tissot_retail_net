<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	if ($id && $user->permission(Material_Collection_Category::PERMISSION_EDIT) ) {
		
		$collection_category_file = new Material_Collection_Category_File($appliction);
		$collection_category_file->read($id);
		
		$collection = $collection_category_file->collection_id;
		$delete = $collection_category_file->delete();
		
		if ($delete) {
			file::remove($collection_category_file->path);
			Message::request_deleted();
			url::redirect("/$appliction/$controller/$action/$collection");
		} else {
			Message::request_failure();
			url::redirect("/$appliction/$controller/$action/$collection/$id");
		}
	}
	else {
		Message::access_denied();
		url::redirect("/$appliction/$controller");
	}