<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$translate = Translate::instance();
	$application = $_REQUEST['application'];
	$section = $_REQUEST['section'];
	$value = $_REQUEST['value'];
		
	switch ($section) {
		
		case 'countries':
				
			$json[][''] = $translate->select;
				
			// load countries
			$countries = Country::loader();
	
			if ($countries) {
				foreach ($countries as $key => $name) {
					$json[][$key] = $name;
				}
			}
				
		break;
		
		case 'provinces':
			
			// country
			$country = new Country();
			$country->read($value);
			
			$json[][''] = $translate->select;
			$json[]['new'] = $translate->other_province_not_listed_below;
			
			// if country provinces are not completed
			// show option for add new province
			if (!$country->provinces_complete) {
				$json[]['new'] = $translate->other_province_not_listed_below;
			}
			
			// provinces loader
			$provinces = Province::loader(array(
				"province_country = $value"
			));
			
			if ($provinces) {
				foreach ($provinces as $key => $name) {
					$json[][$key] = $name;
				}
			}
			
		break;
		
		case 'new_province':
			
			$country = $_REQUEST['country'];
			$value = $_REQUEST['value'];
			
			$result = Province::loader(array(
				"province_country = $country",
				"province_canton = '$value' "
			));
			
			$json = array(
				'response' => true,
				'error' => 	($result) ? true : false,
				'message' => ($result) ? "The province <b>$value</b> exist in our system" : null
			);
			
		break;
		
		case 'places':
			
			$country = $_REQUEST['country'];
			
			$json[][''] = $translate->select;
			$json[]['new'] = $translate->other_city_not_listed_below;
			
			$places = Place::loader(array(
				"place_country = $value"
			));
		
			if ($places) {
				foreach ($places as $key => $name) {
					$json[][$key] = $name;
				}
			}
		
		break;
		
		case 'new_place':
			
			$country = $_REQUEST['country'];
			$province = $_REQUEST['province'];
			$value = $_REQUEST['value'];
			
			if (is_numeric($_REQUEST['province'])) {
				$result = Place::loader(array(
					"place_country = $country",
					"place_province = $province",
					"place_name = '$value' "
				));
			}
			
			$json = array(
				'response' => true,
				'error' => 	($result) ? true : false,
				'message' => ($result) ? "The city <b>$value</b> exist in our system" : null
			);
			
		break;
		
		case 'company_owners':
			
			$json[][''] = $translate->select;
			
			if ($_REQUEST['value']) {
				
				$model = new Model($application);

				$filter = null;
				
				 // mps filter
				if (!$_REQUEST['mps']) {
					$filter = " 
					AND (
						(address_type=7 AND address_is_independent_retailer=1) 
						OR address_can_own_independent_retailers=1
					)";
				}
				
				$owners = $model->query("
					SELECT DISTINCT
						address_id,
						CONCAT(country_name, ': ', LEFT(address_company,40), ', ', LEFT(place_name,30)) AS company
					FROM db_retailnet.addresses
					INNER JOIN db_retailnet.countries ON address_country = country_id
					INNER JOIN db_retailnet.places ON place_id = address_place_id
					WHERE 
						(address_active = 1 AND address_parent = $value $filter) 
						OR ( 
							(address_id = $value OR address_parent = $value)
							$filter
						)
						OR ( address_active = 1 and address_canbefranchisee_worldwide = 1)
					ORDER BY country_name, address_company
				")->fetchAll();
				 
				
				if ($owners) {
					foreach ($owners as $row) {
						$json[][$row['address_id']] = $row['company'];
					}
				}
			}
			
		break;
		
		case 'product_line_subclasses':	
			
			$json[][''] = $translate->select;
			
			if ($value) {
				
				$model = new Model($application);
				
				$result = $model->query("
					SELECT DISTINCT 
						productline_subclass_id, 
						productline_subclass_name
					FROM db_retailnet.productline_subclasses"
				)
				->filter('default', "productline_subclass_productline = $value")
				->order('productline_subclass_name')
				->fetchAll();
				
				if ($result) {
					foreach ($result as $row) {
						$json[][$row['productline_subclass_id']] = $row['productline_subclass_name'];
					}
				}
			}
					
		break;
		
		case 'product_line_items':
				
			$json[][''] = $translate->select;
				
			if ($_REQUEST['value']) {
				
				$model = new Model(Connector::DB_CORE);
				
				$result = $model->query("
					SELECT 
						DISTINCT item_id,
						CONCAT(item_code, ' (', item_name, ')') AS item
					FROM projects
					LEFT JOIN order_items ON order_item_order = project_order
					LEFT JOIN items ON item_id = order_item_item
					LEFT JOIN product_lines ON product_line_id = project_product_line
					WHERE item_addable_in_mps = 1 AND product_line_id = $value
					ORDER BY item_code	
				")->fetchAll();

				$json[]['new'] = "Old item not listed below";
		
				if ($result) {

					foreach ($result as $row) {
						$json[][$row['item_id']] = $row['item'];
					}
				}
			}
				
		break;
		
		case 'product_line_old_items':
				
			$json[][''] = $translate->select;
				
			$model = new Model(Connector::DB_CORE);
			
			$result = $model->query("
				SELECT 
					DISTINCT item_id,
					CONCAT(item_code, ' (', item_name, ')') AS item
				FROM items
				INNER JOIN item_supplying_groups ON item_supplying_group_item_id = item_id
				INNER JOIN supplying_groups ON supplying_group_id = item_supplying_group_supplying_group_id
				INNER JOIN product_line_supplying_groups ON product_line_supplying_group_group_id = supplying_group_id
				INNER JOIN product_lines ON product_line_id = product_line_supplying_group_line_id
				WHERE item_addable_in_mps = 1 AND product_line_id IS NULL
				ORDER BY item_code, item_name
			")->fetchAll();
	
			if ($result) {
				foreach ($result as $row) {
					$json[][$row['item_id']] = $row['item'];
				}
			}
				
		break;
		
		case 'category_materials':
		
			$json[][''] = $translate->select;
		
			if ($_REQUEST['value']) {
		
				$model = new Model($application);
		
				$result = $model->query("
					SELECT DISTINCT
						mps_material_id,
						CONCAT(mps_material_code,' ', mps_material_name) AS name
					FROM mps_materials")
				->filter('active', 'mps_material_active = 1')
				->filter('default', "mps_material_material_category_id = $value")
				->order('mps_material_code')
				->order('mps_material_name')
				->fetchAll();
					
				if ($result) {
					foreach ($result as $row) {
						$json[][$row['mps_material_id']] = $row['name'];
					}
				}
			}
		
		break;
		
		case 'mastersheet.items.collection.categories':
		
			$json[][''] = $translate->all_collection_categories;
		
			if ($_REQUEST['value']) {
		
				$model = new Model($application);
		
				$result = $model->query("
					SELECT DISTINCT
						mps_material_collection_category_id AS id,
						mps_material_collection_category_code AS caption
					FROM mps_materials
				")
				->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
				->filter('active', 'mps_material_active = 1 AND mps_material_collection_category_id > 0')
				->filter('planning', "mps_material_material_planning_type_id = $value")
				->order('mps_material_collection_category_code')
				->fetchAll();
					
				if ($result) {
					foreach ($result as $row) {
						$json[][$row['id']] = $row['caption'];
					}
				}
			}
		
		break;
		
		case 'product.subgroups':
		
			$json[][''] = $translate->all_collection_categories;
		
			if ($value) {
		
				$model = new Model($application);
		
				$result = $model->query("
					SELECT DISTINCT
						lps_product_subgroup_id AS id, 
						lps_product_subgroup_name AS caption
					FROM lps_product_subgroups
					WHERE lps_product_subgroup_group_id = $value 
				")->fetchAll();
					
				if ($result) {
					foreach ($result as $row) {
						$json[][$row['id']] = $row['caption'];
					}
				}
			}
		
		break;
	}
	
	// response with json header
	header('Content-Type: text/json');
	echo json_encode($json);
	