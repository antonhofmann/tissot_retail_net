<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['mail_template_id'];
	
	$data = array();
	
	if (in_array('mail_template_application_id', $fields)) {
		$data['mail_template_application_id'] = $_REQUEST['mail_template_application_id'];
	}

	if ($_REQUEST['mail_template_shortcut']) {
		$data['mail_template_shortcut'] = str_replace(' ', '.', $_REQUEST['mail_template_shortcut']);
	}
	
	if ($_REQUEST['mail_template_code']) {
		$data['mail_template_code'] = $_REQUEST['mail_template_code'];
	}
	
	if ($_REQUEST['mail_template_purpose']) {
		$data['mail_template_purpose'] = $_REQUEST['mail_template_purpose'];
	}
	
	if ($_REQUEST['mail_template_subject']) {
		$data['mail_template_subject'] = $_REQUEST['mail_template_subject'];
	}
	
	if ($_REQUEST['mail_template_text']) {
		$data['mail_template_text'] = $_REQUEST['mail_template_text'];
	}
	
	if ($_REQUEST['mail_template_footer']) {
		$data['mail_template_footer'] = $_REQUEST['mail_template_footer'];
	}
	
	if ($data) {
	
		$mail_template = new Mail_Template();
		$mail_template->read($id);
	
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $mail_template->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $mail_template->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = "/$application/$controller/data/$id";
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	