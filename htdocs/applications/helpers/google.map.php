<?php 

/*
    Google Map V3

    Created by:     Admir Serifi (admir.serifi@mediaparx.com)
    Date created:   2011-05-12
    Modified by:    
    Date modified:  
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.
*/
	error_reporting(E_ERROR | E_WARNING);
	
	$latitude = ($_REQUEST['latitude']) ? $_REQUEST['latitude'] : 0;
	$longitude = ($_REQUEST['longitude']) ? $_REQUEST['longitude'] : 0;
	$zoom = ($_REQUEST['zoom']) ? $_REQUEST['zoom'] : 10;
	$draggable = ($_REQUEST['draggable']) ? $_REQUEST['draggable'] : 0;

?>
<!doctype html>
<!--[if lt IE 7]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"> <!--<![endif]-->
<head>
	<script src="/public/scripts/jquery/1.6.4.js" type="text/javascript"></script>
	<script src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyD2wMUs74upe6ZvEOflrrMDRCoPYABu1Qs" type="text/javascript" ></script>
	<script type="text/javascript">

		$(document).ready(function(){
			
			var latitude = $('#latitude').val(); 
			var longitude = $('#longitude').val();
			var draggable = ($('#draggable').val() == 1) ? true : false;
			//var map_zoom = $('#zoom').val();
			
			var StartPosition = new google.maps.LatLng(latitude, longitude);
			
			function SetPosition(Location, Viewport) {
			    Marker.setPosition(Location);
			    if(Viewport){
			        Map.fitBounds(Viewport);
			        Map.setZoom(map.getZoom() + 2);
			    }
			    else {
			        Map.panTo(Location);
			    }
			
			    $("#posaddress_google_lat").val(Location.lat().toFixed(15));
			    $("#posaddress_google_long").val(Location.lng().toFixed(15));
			}

			var MapOptions = {
			    zoom: 16,
			    center: StartPosition,
			    mapTypeId: google.maps.MapTypeId.ROADMAP,
			    mapTypeControl: true,
			    disableDoubleClickZoom: false,
			    streetViewControl: true
			};
			
			var MapView = $("#gmap");
			var Map = new google.maps.Map(MapView.get(0), MapOptions);
			
			var Marker = new google.maps.Marker({
			    position: StartPosition, 
			    map: Map, 
			    title: "Drag Me",
			    draggable: draggable
			});
			
			google.maps.event.addListener(Marker, "dragend", function(event) {
			    SetPosition(Marker.position);
			});
			
			SetPosition(Marker.position);
		});
		
	</script>
	<style type="text/css">
		#gmap {
			display: block; 
			width: 700px; 
			height: 700px;
			margin:0; 
			padding:0; 
			border:0;
		}
		
		.ie7 #gmap {
			height: 690px !important;
		}
		
	</style>
</head>
	<body>
		<div style="display:none">
			<form>
				<input type=hidden name=latitude id=latitude value="<?php echo $latitude; ?>" />
				<input type=hidden name=longitude id=longitude value="<?php echo $longitude; ?>" />
				<input type=hidden name=zoom id=zoom value="<?php echo $zoom; ?>" />
				<input type=hidden name=draggable id=draggable value="<?php echo $draggable; ?>" />
			</form>
		</div>
		<div id="gmap"></div>
	</body>
</html>
	