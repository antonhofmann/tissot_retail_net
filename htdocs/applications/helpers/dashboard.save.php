<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['ui_dashboard_id'];
	
	$data = array();
	
	if ($_REQUEST['ui_dashboard_title']) {
		$data['ui_dashboard_title'] = $_REQUEST['ui_dashboard_title'];
	}
	
	if ($data) {
	
		$dashboard = new Dashboard();
		$dashboard->read($id);
	
		if ($dashboard->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $dashboard->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			
			$response = $id = $dashboard->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
		
		// update all user dashboards
		$model = new Model(Connector::DB_CORE);
		
		$model->db->exec("
			UPDATE ui_user_dashboards SET
				ui_user_dashboard_title = '$dashboard->title'
			WHERE ui_user_dashboard_dashboard_id = $id		
		");
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	