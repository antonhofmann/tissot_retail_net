<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$model = new Model();

	// request
	$_REQUEST = session::filter($application, 'security.ip.locked.list', true);
	
	// framework application
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'sec_lockedip_date';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "desc";
	
	// sql pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$offset = ($page-1) * $settings->limit_pager_rows;
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "( 
			sec_lockedip_ip LIKE \"%$keyword%\"
			OR sec_lockedip_date LIKE \"%$keyword%\"
		)";
	}
	
	// datagrid
	$result = $model->query("
		SELECT DISTINCT
			sec_lockedip_ip AS id,
			sec_lockedip_ip,
			DATE_FORMAT(sec_lockedip_date, '%d.%m.%Y') AS sec_lockedip_date
		FROM sec_lockedips
	")
	->order($sort, $direction)
	->filter($filters)
	->fetchAll();
	
	if ($result) {
	
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		
		if (user::permission('can_unlock_ips')) {
			foreach ($datagrid as $key => $row) {
				$datagrid[$key]['unlock'] = "
					<span data='$key' class=button >
						<span class='icon unlock'></span>
					</span>
				";
			}
		}
	
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
		
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	$table->dataloader($dataloader);
	
	$table->caption('unlock', '');
	
	$table->sec_lockedip_ip(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);
	
	$table->sec_lockedip_date(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=20%'
	);
	
	if (user::permission('can_unlock_ips')) {
		$table->unlock(
			'width=5%'
		);
	}
	
	$table->footer("<div class='table-index'>".$translate->total_rows.": ".count($datagrid)."</div>");
	$table = $table->render();
	
	echo $toolbox.$table;
	