<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$translate = translate::instance();
	
	$application = $_REQUEST['application'];
	$id = $_REQUEST['mastersheet'];
	$planningtype = $_REQUEST['planningtype'];
	$collection = $_REQUEST['collection'];
	$item = $_REQUEST['item'];
	$checked = $_REQUEST['checked'];
	
	// mastersheet
	$mastersheet = new Mastersheet($application);
	$mastersheet->read($id);
	
	// add items from planning type and collection category
	if ($planningtype && $collection) {
		
		$model = new Model($application);
		
		$items = $model->query("
			SELECT DISTINCT mps_material_id
			FROM mps_materials
		")
		->bind(Material::DB_BIND_PLANNING_TYPES)
		->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
		->filter('planning', "mps_material_material_planning_type_id = $planningtype")
		->filter('collection', "mps_material_collection_category_id = $collection")
		->fetchAll();
		
		if ($items) {
			
			foreach ($items as $item) {
				
				$check = $mastersheet->item()->read($item['mps_material_id']);
				
				if (!$check) {
					$mastersheet->item()->create($item['mps_material_id']);
				}
			}
			
			$response = true;
			$message = $translate->message_request_inserted;
		}
	}
	// item or remove item from master sheet
	else {
		
		// item belong to master sheet
		$belong = $mastersheet->item()->read($item);
		 
		// add new item
		if ($checked && !$belong) {
			$response = $mastersheet->item()->create($item);
			$message = ($response) ? $translate->message_request_saved : $translate->message_request_failure;
		}	
		
		// remove item
		if (!$checked && $belong) { 
			$response = $mastersheet->item()->delete($item);
			$message = ($response) ? $translate->message_request_saved : $translate->message_request_failure;
		}
	}
	
	// response with json header
	header('Content-Type: text/json');
	echo json_encode(array(
		'response' => $response,
		'message' => $message	
	));
	