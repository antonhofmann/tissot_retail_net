<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	
	$user = User::identification(array(
		'user_firstname' => $_REQUEST['firstname'],
		'user_name' => $_REQUEST['lastname'],
		'user_email' => $_REQUEST['email'],
		'user_login' => $_REQUEST['username'],
		'user_active' => 1
	));
	
	if ($user) {
		$response = true;
		$message = $translate->message_request_submitted;
	}
	else {
		$response = false;
		$message = $translate->error_identification_failed;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'user' => $user['user_id']
	));