<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	require_once PATH_LIBRARIES."phpmailer/class.phpmailer.php";

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];

	$proposed_quantity = $_REQUEST['proposed_quantity'];
	$new_material = $_REQUEST['item_id'];
	
	// material
	$material = new Material($application);
	
	$material->read($_REQUEST['mps_material_id']);
	
	if ($material->id && $action) {
		
		$redirect = "/$application/$controller/bulk/$material->id";
		
		// master sheet
		$mastersheet = new Mastersheet($application);
		
		$mastersheet->read($_REQUEST['mps_mastersheet_id']);
		
		$model = new Model($application);
		
		// workflow state
		$workflowState = new Workflow_State($application);
		$workflowState->read(Workflow_State::STATE_REVISION);
		
		// tracker
		$tracker = new User_Tracking($application);
		
		// editabled workflow states
		$editabled_workflowstates = join(',', array(
			Workflow_State::STATE_PREPARATION,
			Workflow_State::STATE_OPEN,
			Workflow_State::STATE_PROGRESS,
			Workflow_State::STATE_COMPLETED,
			Workflow_State::STATE_APPROVED,
			Workflow_State::STATE_REVISION
		));
		
		// order sheet completetd states
		$ordersheet_completed_states = array(
			Workflow_State::STATE_COMPLETED,
			Workflow_State::STATE_APPROVED
		);
		
		switch ($action) {
				
			case 1: // Add item to master sheet and to order sheets items
				
				// master sheet items
				$mastersheet->item()->read($material->id);
				
				// add item to master sheet
				if (!$mastersheet->item()->id) {
					$response = $mastersheet->item()->create($material->id);
				}
				
				// add item to selected ordersheets
				if ($_REQUEST['ordersheets']) {
					
					$selected_ordersheets = join(',', array_keys($_REQUEST['ordersheets']));
				
					// get alleditabled ordersheets
					$ordersheets = $model->query("
						SELECT DISTINCT 
							mps_ordersheet_id
						FROM mps_ordersheet_items
						INNER JOIN mps_ordersheets ON  mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
						WHERE 
							mps_ordersheet_mastersheet_id = $mastersheet->id
							AND mps_ordersheet_item_material_id != $material->id
							AND mps_ordersheet_workflowstate_id IN ($editabled_workflowstates)
							AND mps_ordersheet_id IN ($selected_ordersheets)
					")->fetchAll();
					
					if ($ordersheets) {
						
						// ordersheet
						$ordersheet = new Ordersheet($application);
						
						// currency
						$currency = new Currency();
						$currency->read($material->currency_id);
						
						foreach ($ordersheets as $row) {
							
							$id = $row['mps_ordersheet_id'];
							
							$ordersheet->read($id);
							
							if ($ordersheet->id) {
								
								// if order sheet is completed or approved set it in revision
								$inRevision = (in_array($ordersheet->workflowstate_id, $ordersheet_completed_states)) ? 1 : 0;
								
								// add item te order sheet
								$response = $ordersheet->item()->create(array(
									'mps_ordersheet_item_ordersheet_id' => $ordersheet->id,
									'mps_ordersheet_item_material_id' => $material->id,
									'mps_ordersheet_item_price' => $material->price,
									'mps_ordersheet_item_currency' => $currency->id,
									'mps_ordersheet_item_exchangrate' => $currency->exchange_rate,
									'mps_ordersheet_item_factor' => $currency->factor,
									'mps_ordersheet_item_quantity_proposed' => $proposed_quantity ?: null,
									'mps_ordersheet_item_status' => $inRevision,
									'user_created' => $user->login,
									'date_created' => date('Y-m-d H:i:s')
								));
								
								// set order sheet in revision status
								if ($inRevision) {
									
									$ordersheet->update(array(
										'mps_ordersheet_workflowstate_id' => $workflowState->id,
										'user_modified' => $user->login,
										'date_modified' => date('Y-m-d H:i:s')
									));
									
									$tracker->create(array(
										'user_tracking_entity' => 'order sheet',
										'user_tracking_entity_id' => $id,
										'user_tracking_user_id' => $user->id,
										'user_tracking_action' => "$workflowState->name (material bulk operations: add)",
										'user_created' => $user->login,
										'date_created' => date('Y-m-d H:i:s')
									));
								}
							}
						}
					}
				}
				
				if ($response) {
					Message::request_saved();
				}
				else {
					$message = $translate->message_request_failure;
					$redirect = null;
				}
	
			break;
		
			
			case 2: // remove item from master sheet and from order sheets items
				
				// read master sheet item instance
				$mastersheet->item()->read($material->id);
				
				// remove item from mastersheet
				if ($mastersheet->item()->id) {
					$response = $mastersheet->item()->delete($material->id);
				}

				
				if ($_REQUEST['ordersheets']) {
					
					$selected_ordersheets = join(',', array_keys($_REQUEST['ordersheets']));
				
					// get all editabled ordersheets
					$ordersheets = $model->query("
						SELECT DISTINCT 
							mps_ordersheet_id, 
							mps_ordersheet_item_id
						FROM mps_ordersheet_items
						INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
						WHERE 
							mps_ordersheet_mastersheet_id = $mastersheet->id
							AND mps_ordersheet_item_material_id = $material->id	
							AND mps_ordersheet_id IN ($selected_ordersheets)
							AND mps_ordersheet_workflowstate_id IN ($editabled_workflowstates)
							AND (
								mps_ordersheet_item_purchase_order_number IS NULL 
								OR mps_ordersheet_item_purchase_order_number = ''
								OR mps_ordersheet_item_purchase_order_number = 0
							)
					")->fetchAll();
					
					if ($ordersheets) { 
						
						$ordersheet = new Ordersheet($application);
						
						foreach ($ordersheets as $row) {
							
							$id = $row['mps_ordersheet_id'];
							$item = $row['mps_ordersheet_item_id'];
							
							// read item
							$ordersheet->read($id);
							$ordersheet->item()->read($item);
								
							if ($ordersheet->item()->id) {
						
								// remove item from order sheet
								$ordersheet->item()->delete();
								
								// set order sheet in revision status
								if (in_array($ordersheet->workflowstate_id, $ordersheet_completed_states)) {
									
									$ordersheet->update(array(
										'mps_ordersheet_workflowstate_id' => $workflowState->id,
										'user_modified' => $user->login,
										'date_modified' => date('Y-m-d H:i:s')
									));
									
									$tracker->create(array(
										'user_tracking_entity' => 'order sheet',
										'user_tracking_entity_id' => $id,
										'user_tracking_user_id' => $user->id,
										'user_tracking_action' => "$workflowState->name (material bulk operations: delete)",
										'user_created' => $user->login,
										'date_created' => date('Y-m-d H:i:s')
									));
								}
							}
						}
					}
				}
				
				if ($response) {
					Message::request_saved();
				}
				else {
					$message = $translate->message_request_failure;
					$redirect = null;
				}
		
			break;
		
			case 3: // replace item in master sheet and in order sheet items
				
				
				// master items item
				$mastersheet->item()->read($material->id);
				
				if ($mastersheet->item()->id) {
					
					$response = $model->db->query("
						UPDATE mps_mastersheet_items SET
							mps_mastersheet_item_material_id = $new_material
						WHERE 
							mps_mastersheet_item_mastersheet_id = $mastersheet->id
							AND mps_mastersheet_item_material_id = $material->id	
					");
				}
				
				// replace items in order sheets
				if ($_REQUEST['ordersheets']) {
					
					$selected_ordersheets = join(',', array_keys($_REQUEST['ordersheets']));
					
					$ordersheets = $model->query("
						SELECT DISTINCT
							mps_ordersheet_id,
							mps_ordersheet_item_id
						FROM mps_ordersheet_items
						INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
						WHERE 
							mps_ordersheet_mastersheet_id = $mastersheet->id
							AND mps_ordersheet_id IN ($selected_ordersheets)
							AND mps_ordersheet_workflowstate_id IN ($editabled_workflowstates)
							AND mps_ordersheet_item_material_id = $material->id
							AND (
								mps_ordersheet_item_purchase_order_number IS NULL 
								OR mps_ordersheet_item_purchase_order_number = ''
								OR mps_ordersheet_item_purchase_order_number = 0
							)
					")->fetchAll();
					
					if ($ordersheets) {
						
						// new item
						$newMaterial = new Material($application);
						$newMaterial->read($new_material);
						
						// currency
						$currency = new Currency(Connector::DB_CORE);
						$currency->read($newMaterial->currency_id);
					
						// ordersheet
						$ordersheet = new Ordersheet($application);
						
						// new item
						$newOrdersheet = new Ordersheet($application);
						
						foreach ($ordersheets as $row) {
							
							$inRevision = false;
							$deleted = false;
							
							$id = $row['mps_ordersheet_id'];
							$item = $row['mps_ordersheet_item_id'];
								
							// read item
							$ordersheet->read($id);
							$ordersheet->item()->read($item);

							// old item exist
							if ($ordersheet->item()->id) {
								
								$newOrdersheet->read($id);
								$newOrdersheet->item()->read_from_material($new_material);
								
								$data = array(
									'mps_ordersheet_item_material_id' => $newMaterial->id,
									'mps_ordersheet_item_price' => $newMaterial->price,
									'mps_ordersheet_item_currency' => $currency->id,
									'mps_ordersheet_item_exchangrate' => $currency->exchange_rate,
									'mps_ordersheet_item_factor' => $currency->factor,
									'user_modified' => $user->login,
									'date_modified' => date('Y-m-d H:i:s')
								);
								
								
								// new item exist, update new item and delete old item
								if ($newOrdersheet->item()->id) {
									
									// item in revision
									$inRevision = (in_array($newOrdersheet->workflowstate_id, $ordersheet_completed_states)) ? 1 : 0;
									$data['mps_ordersheet_item_status'] = $inRevision;
									
									// client quantity
									$quantity = $ordersheet->item()->quantity + $newOrdersheet->item()->quantity;
									$data['mps_ordersheet_item_quantity'] = $quantity ?: null;
									
									// approved quantity
									$approved = $ordersheet->item()->quantity_approved + $newOrdersheet->item()->quantity_approved;
									$data['mps_ordersheet_item_quantity_approved'] = $approved ?: null;
									
									// remove old item
									$deleted = $ordersheet->item()->delete();
									
									if ($deleted) {
										
										$response = $newOrdersheet->item()->update($data);
									}
								} 
								// replace old item data with new item
								else {
									
									// item in revision
									$inRevision = (in_array($ordersheet->workflowstate_id, $ordersheet_completed_states)) ? 1 : 0;
									$data['mps_ordersheet_item_status'] = $inRevision;

									$response = $ordersheet->item()->update($data);
								}
							}
							
							// set order sheet in revision status
							if ($inRevision) {
								
								$data = array(
									'mps_ordersheet_workflowstate_id' => $workflowState->id,
									'user_modified' => $user->login,
									'date_modified' => date('Y-m-d H:i:s')
								);
								
								if ($deleted) {
									$newOrdersheet->update($data);
								} else {
									$ordersheet->update($data);
								}
								
								$tracker->create(array(
									'user_tracking_entity' => 'order sheet',
									'user_tracking_entity_id' => $id,
									'user_tracking_user_id' => $user->id,
									'user_tracking_action' => "$workflowState->name (material bulk operations: replace)",
									'user_created' => $user->login,
									'date_created' => date('Y-m-d H:i:s')
								));
							}
							
						} // end each order sheet
					}
				}
				
				if ($response) {
					Message::request_saved();
				}
				else {
					$message = $translate->message_request_failure;
					$redirect = null;
				}
		
			break;
		}
	}
	
	echo json_encode(array(
		'message' => $message,
		'response' => $response,
		'redirect' => $redirect
	));
	