<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['posaddress_customerservice_posaddress_id'];
	$services = $_REQUEST['services'];
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$data = array();
	
	
	if($id && in_array('services', $fields)) {
	
		$model = new Model(Connector::DB_CORE);
		
		$model->db->exec("
			DELETE FROM posaddress_customerservices
			WHERE posaddress_customerservice_posaddress_id = $id	
		");
		
		
		if ($_REQUEST['services']) {
			
			$sth = $model->db->prepare("
				INSERT INTO posaddress_customerservices (
					posaddress_customerservice_posaddress_id,
					posaddress_customerservice_customerservice_id,
					user_created,
					date_created
				)
				VALUES (:pos, :service, :user, :date)
			");
			
			foreach ($_REQUEST['services'] as $key => $value) {
				$sth->execute(array(
					':pos' => $id,
					':service' => $key,
					':user' => $user->login,
					':date' => date('Y-m-d H:i:s')
				));
			}
		}
		
		// update store locator
		$storelocator = new Store_Locator();
		$storelocator->update($id);
		
		$response = true;
		$message = $translate->message_request_updated;
		
	} else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message
	));
	