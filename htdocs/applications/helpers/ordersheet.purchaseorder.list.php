<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// url vars
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$filter = $_REQUEST['filter'];
	
	// permissions
	$permission_view = user::permission(Ordersheet::PERMISSION_VIEW);
	$permission_view_limited = user::permission(Ordersheet::PERMISSION_VIEW_LIMITED);
	$permission_edit = user::permission(Ordersheet::PERMISSION_EDIT);
	$permission_edit_limited = user::permission(Ordersheet::PERMISSION_EDIT_LIMITED);
	$permission_manage = user::permission(Ordersheet::PERMISSION_MANAGE);
	
	// db connector
	$model = new Model($application);

	// request
	$_REQUEST = session::filter($application, "$application.$controller.$archived", true);

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'country_name,address_company,mps_mastersheet_name,mps_ordersheet_item_purchase_order_number';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
	
	// filter: limited permission
	if(!$permission_manage && !$permission_edit && !$permission_view) {
		$filters['limited'] = "mps_ordersheet_address_id = ".$user->address;
	}
		
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "(
			mps_ordersheet_item_order_date LIKE \"%$keyword%\" 
			OR mps_ordersheet_item_purchase_order_number LIKE \"%$keyword%\"
			OR mps_ordersheet_item_desired_delivery_date LIKE \"%$keyword%\"
			OR mps_mastersheet_name LIKE \"%$keyword%\"
			OR address_mps_shipto LIKE \"%$keyword%\"
			OR address_mps_customernumber LIKE \"%$keyword%\"
			OR country_name LIKE \"%$keyword%\"
			OR address_company LIKE \"%$keyword%\"
		)";
	}
	
	// filer: mastersheet year
	if ($_REQUEST['mastersheet_years']) {
		$filters['mastersheet_years'] = "mps_mastersheet_year = ".$_REQUEST['mastersheet_years'];
	}
	
	// filer: countries
	if ($_REQUEST['countries']) {
		$filters['countries'] = "country_id = ".$_REQUEST['countries'];
	}
	
	// filer: mastersheet
	if ($_REQUEST['mastersheets']) {
		$filters['mastersheets'] = "mps_ordersheet_mastersheet_id = ".$_REQUEST['mastersheets'];
	}
	
	// filer: workflowstate
	if ($_REQUEST['workflowstates']) {
		$filters['workflowstates'] = "mps_ordersheet_workflowstate_id = ".$_REQUEST['workflowstates'];
	}
	
	if ($_REQUEST['ordernumbers']) {
		$filters['ordernumbers'] = "mps_ordersheet_item_purchase_order_number = ".$_REQUEST['ordernumbers'];
	}
	
	// default filter
	$filters['filter'] = "
		mps_ordersheet_workflowstate_id IN (5,6) 
		AND mps_ordersheet_item_purchase_order_number > 0 
		AND mps_ordersheet_item_quantity_confirmed > 0
	";

	// country access filter
	$accessCountries = array();

	$result = $model->query("
		SELECT DISTINCT country_access_country
		FROM db_retailnet.country_access
		WHERE country_access_user = $user->id
	")->fetchAll();

	if ($result) {
		
		foreach ($result as $row) {
			$accessCountries[] = $row['country_access_country'];
		}

		$accessCountriesFilter = join(',', $accessCountries);
	}

	// country access extended filter
	if ($accessCountriesFilter) {

		$basicFiletrs = $filters;
		unset($basicFiletrs['limited']);

		if ($basicFiletrs) {
			$filter = join(' AND ', $basicFiletrs);
			$extendedFilter = " OR ( $filter  AND address_country IN ($accessCountriesFilter))";	
		} else {
			$filters['countries'] = "address_country IN ($accessCountriesFilter)";
		}
	}

	// regional access companies
	$regionalAccessCompanies = User::getRegionalAccessCompanies();

	if ($regionalAccessCompanies) {
		$regionalAccessFilter = join(',', $regionalAccessCompanies);
	}

	if ($regionalAccessFilter) {

		$filter = $filters;
		unset($filter['limited']);

		if ($filter) {
			$filter = join(' AND ', $filter);
			$extendedFilter .= " OR ($filter  AND address_id IN ($regionalAccessFilter))";	
		} else {
			$filters['regionals'] = "address_id IN ($regionalAccessFilter)";
		}
	}
	
	$binds = array(
		Ordersheet::DB_BIND_ORDERSHEET_ITEMS,
		Ordersheet::DB_BIND_MASTERSHEETS,
		Ordersheet::DB_BIND_WORKFLOW_STATES,
		Ordersheet::DB_BIND_COMPANIES,
		Company::DB_BIND_COUNTRIES
	);
	
	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			mps_ordersheet_id,
			mps_mastersheet_name,
			address_mps_shipto,
			address_mps_customernumber,
			country_name,
			address_company,
			mps_ordersheet_item_purchase_order_number,
			DATE_FORMAT(mps_ordersheet_item_order_date, '%d.%m.%Y') AS mps_ordersheet_item_order_date,
			DATE_FORMAT(mps_ordersheet_item_desired_delivery_date, '%d.%m.%Y') AS mps_ordersheet_item_desired_delivery_date
		FROM mps_ordersheets
	")
	->bind($binds)
	->filter($filters)
	->extend($extendedFilter)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();
	
	if ($result) {
		
		$totalrows = $model->totalRows();
		
		// group result by ordersheet and order number
		foreach ($result as $row) {
			$ordersheet = $row['mps_ordersheet_id'];
			$ordernumber = $row['mps_ordersheet_item_purchase_order_number'];
			$source[$ordersheet][$ordernumber] = $row;
		}
		
		// build datagrid
		foreach ($source as $ordersheet => $array) {
			foreach ($array as $ordernumber => $row) {
				$link = ($_REQUEST['form']) ? '<a href="'.$_REQUEST['form'].'/'.$ordersheet.'/'.$ordernumber.'" >'.$row['mps_mastersheet_name'].'</a>' : $row['mps_mastersheet_name'];
				$row['mps_mastersheet_name'] = $link;
				$datagrid[] = $row;
			}
		}
	
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

	// toolbox: button print
	if ($datagrid && $_REQUEST['print']) {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'href' => $_REQUEST['print'],
			'label' => $translate->print
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();

	$extendedFilter = null;
	$basicFiletrs = $filters;
	unset($basicFiletrs['mastersheet_years']);
	
	// filter country access
	if ($accessCountries) {
	
		$filter = $basicFiletrs;
		unset($filter['limited']);
	
		if ($filter) {
			$filter = join(' AND ', $filter);
			$extendedFilter = " OR ($filter AND address_country IN ($accessCountriesFilter) )";
		} else {
			$basicFiletrs['countries'] = "address_country IN ($accessCountriesFilter)";
		}
	}

	// filter regional access
	if ($regionalAccessFilter) {

		$filter = $basicFiletrs;
		unset($filter['limited']);

		if ($filter) {
			$filter = join(' AND ', $filter);
			$extendedFilter .= " OR ($filter  AND address_id IN ($regionalAccessFilter))";	
		} else {
			$basicFiletrs['regionals'] = "address_id IN ($regionalAccessFilter)";
		}
	}
	
	// data: mastersheet years
	$result = $model->query("
		SELECT DISTINCT mps_mastersheet_year, mps_mastersheet_year AS year 
		FROM mps_ordersheets
	")
	->bind($binds)
	->filter($basicFiletrs)
	->extend($extendedFilter)
	->order('mps_mastersheet_year')
	->fetchAll();
	
	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'mastersheet_years',
			'id' => 'mastersheet_years',
			'class' => 'submit',
			'value' => $_REQUEST['mastersheet_years'],
			'caption' => $translate->all_years
		));
	}


	$extendedFilter = null;
	$basicFiletrs = $filters;
	unset($basicFiletrs['countries']);
	
	// filter country access
	if ($accessCountries) {
	
		$filter = $basicFiletrs;
		unset($filter['limited']);
	
		if ($filter) {
			$filter = join(' AND ', $filter);
			$extendedFilter = " OR ($filter AND address_country IN ($accessCountriesFilter) )";
		} else {
			$basicFiletrs['countries'] = "address_country IN ($accessCountriesFilter)";
		}
	}

	// filter regional access
	if ($regionalAccessFilter) {

		$filter = $basicFiletrs;
		unset($filter['limited']);

		if ($filter) {
			$filter = join(' AND ', $filter);
			$extendedFilter .= " OR ($filter  AND address_id IN ($regionalAccessFilter))";	
		} else {
			$basicFiletrs['regionals'] = "address_id IN ($regionalAccessFilter)";
		}
	}
	
	// data: countries
	$result = $model->query("
		SELECT DISTINCT country_id, country_name 
		FROM mps_ordersheets
	")
	->bind($binds)
	->filter($basicFiletrs)
	->extend($extendedFilter)
	->order('country_name')
	->fetchAll();
	
	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'countries',
			'id' => 'countries',
			'class' => 'submit',
			'value' => $_REQUEST['countries'],
			'caption' => $translate->all_countries
		));
	}
	


	$extendedFilter = null;
	$basicFiletrs = $filters;
	unset($basicFiletrs['mastersheets']);
	
	// filter country access
	if ($accessCountries) {
	
		$filter = $basicFiletrs;
		unset($filter['limited']);
	
		if ($filter) {
			$filter = join(' AND ', $filter);
			$extendedFilter = " OR ($filter AND address_country IN ($accessCountriesFilter) )";
		} else {
			$basicFiletrs['countries'] = "address_country IN ($accessCountriesFilter)";
		}
	}

	// filter regional access
	if ($regionalAccessFilter) {

		$filter = $basicFiletrs;
		unset($filter['limited']);

		if ($filter) {
			$filter = join(' AND ', $filter);
			$extendedFilter .= " OR ($filter  AND address_id IN ($regionalAccessFilter))";	
		} else {
			$basicFiletrs['regionals'] = "address_id IN ($regionalAccessFilter)";
		}
	}

	// data: mastersheets
	$result = $model->query("
		SELECT DISTINCT mps_mastersheet_id, mps_mastersheet_name 
		FROM mps_ordersheets
	")
	->bind($binds)
	->filter($basicFiletrs)
	->extend($extendedFilter)
	->order('mps_mastersheet_name')
	->fetchAll();
	
	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'mastersheets',
			'id' => 'mastersheets',
			'class' => 'submit',
			'value' => $_REQUEST['mastersheets'],
			'caption' => $translate->all_mastersheets
		));
	}




	$extendedFilter = null;
	$basicFiletrs = $filters;
	unset($basicFiletrs['ordernumbers']);
	
	// filter country access
	if ($accessCountries) {
	
		$filter = $basicFiletrs;
		unset($filter['limited']);
	
		if ($filter) {
			$filter = join(' AND ', $filter);
			$extendedFilter = " OR ($filter AND address_country IN ($accessCountriesFilter) )";
		} else {
			$basicFiletrs['countries'] = "address_country IN ($accessCountriesFilter)";
		}
	}

	// filter regional access
	if ($regionalAccessFilter) {

		$filter = $basicFiletrs;
		unset($filter['limited']);

		if ($filter) {
			$filter = join(' AND ', $filter);
			$extendedFilter .= " OR ($filter  AND address_id IN ($regionalAccessFilter))";	
		} else {
			$basicFiletrs['regionals'] = "address_id IN ($regionalAccessFilter)";
		}
	}
	
	// data: ordernumbers
	$result = $model->query("
		SELECT DISTINCT mps_ordersheet_item_purchase_order_number, mps_ordersheet_item_purchase_order_number AS number 
		FROM mps_ordersheets
	")
	->bind($binds)
	->filter($basicFiletrs)
	->extend($extendedFilter)
	->order('mps_ordersheet_item_purchase_order_number')
	->fetchAll();
	
	if ($result) {
		$toolbox[] = ui::dropdown($result, array(
			'name' => 'ordernumbers',
			'id' => 'ordernumbers',
			'class' => 'submit',
			'value' => $_REQUEST['ordernumbers'],
			'caption' => $translate->all_ordernumbers
		));
	}
	
	// toolbox: form
	if ($toolbox) {
		$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	
	$table->country_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=15%'
	);
	
	$table->address_company(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=20%'
	);
	
	$table->mps_mastersheet_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT
	);
	
	$table->address_mps_customernumber(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->address_mps_shipto(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->mps_ordersheet_item_order_date(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->mps_ordersheet_item_purchase_order_number(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->mps_ordersheet_item_desired_delivery_date(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	