<?php

	error_reporting(E_ERROR);
		
	$content = ($_REQUEST['caption']) ? $_REQUEST['caption'] : "GD Image";
	$width = ($_REQUEST['width']) ? $_REQUEST['width'] : 120;
	$height = ($_REQUEST['height']) ? $_REQUEST['height'] : 40;
	$space = ($_REQUEST['space']) ? $_REQUEST['space'] : 10;
	$fontsize = ($_REQUEST['fontsize']) ? $_REQUEST['fontsize'] : 11;
	$rotate = ($_REQUEST['rotate']) ? $_REQUEST['rotate'] : 0;
	$color= ($_REQUEST['color']) ? $_REQUEST['color'] : "000000";
	$bg = ($_REQUEST['bg']) ? $_REQUEST['bg'] : "ffffff";
	$font = ($_REQUEST['font']) ? $_REQUEST['font'] : "swatchCT.ttf";


	// font
	$fontpath = ( file_exists($_SERVER['DOCUMENT_ROOT']."/public/fonts/$font") )
		? $_SERVER['DOCUMENT_ROOT']."/public/fonts/$font"
		: $_SERVER['DOCUMENT_ROOT']."/public/fonts/arial.ttf";
	
	// content
	$txt = explode("\n", $content);
	$lines = count($txt);
	
	// image
	$image = imagecreatetruecolor($width, $height);
	
	// background
	$int = hexdec($bg);
	$bg = imagecolorallocate($image, 0xFF & ($int >> 0x10), 0xFF & ($int >> 0x8), 0xFF & $int);
	imagefill($image, 0, 0, $bg);
	
	// font color
	$int = hexdec($color);
	$color = imagecolorallocate($image, 0xFF & ($int >> 0x10), 0xFF & ($int >> 0x8), 0xFF & $int);
	
	// content
	$y = ($height-(2*$lines-1)*$fontsize + 2*$fontsize)/2;
	foreach ($txt as $text) {
		imagettftext ($image, $fontsize, 0, $space, $y, $color, $fontpath, $text);
		$y = $y+2*$fontsize;
	}
	
	// rotation
	if ($rotate) {
		$image = imagerotate($image, $rotate, $bg);
	}
	
	// export
	header('Content-type: image/png');
	imagepng($image);
	imagedestroy($image);
