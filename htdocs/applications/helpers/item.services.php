<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	$model = new Model($application); 

	if ($application=='catalog') {
		$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "item_code, item_name"; 
		$filters['item_types'] = "item_type = 7";
	}
	
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search ) {
		
		$keyword = $_REQUEST['search'];
	
		$filters['search'] = "(
			item_code LIKE \"%$keyword%\" 
			OR item_name LIKE \"%$keyword%\" 
			OR item_description LIKE \"%$keyword%\"
			OR item_category_name LIKE \"%$keyword%\"
		)";
	}

	// filter: actives
	if (isset($_REQUEST['active'])) {

		$active = $_REQUEST['active']==1 ? 1 : 0;

		if ($_REQUEST['active']) {
			$filters['active'] = "item_active = $active";
		}

	} else {
		$active = 1;
		$filters['active'] = "item_active = $active";
	}

	if ($application=='catalog') {

		// filter: item category
		if ($_REQUEST['category']) {
			$filters['category'] = "item_category_id = ".$_REQUEST['category'];
		}

		$query = "
			SELECT SQL_CALC_FOUND_ROWS DISTINCT
				item_id,
				item_category_name,
				item_code,
				item_name,
				item_price,
				IF(item_active, '<img src=/public/images/icon-checked.png >', ' ') as item_active
			FROM db_retailnet.items
			LEFT JOIN db_retailnet.item_categories ON item_category_id = item_category
		";
	}
	
	$result = $model->query($query)
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();
	
	//echo $model->sql;

	$datagrid = array();
	$dropdown = array();

	if ($result) {

		$totalrows = $model->totalRows();

		$datagrid = _array::datagrid($result);


		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));
		
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
		
	// toolbox: button print
	if ($datagrid && $_REQUEST['print']) {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'href' => $_REQUEST['print'],
			'label' => $translate->print
		));
	}
	
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}

	// toolbox: serach full text
	$toolbox[] = ui::searchbox();

	$actives = array(
		1 => 'Active Items',
		2 => 'Inactive Items'
	);

	$toolbox[] = ui::dropdown($actives, array(
		'name' => 'active',
		'id' => 'active',
		'class' => 'submit',
		'value' => $active,
		'caption' => 'All Items'
	));
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	if ($datagrid) {
		foreach ($datagrid as $item => $row) {
			
			// item supplier
			if ($itemSuppliers[$item]) {
				$datagrid[$item]['item_suppleir'] = $itemSuppliers[$item];
			}
		}
	}

	$table->datagrid = $datagrid;
	
	$table->item_code(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=15%'
	);
	
	$table->item_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		Table::DATA_TYPE_LINK,
		"href=".$_REQUEST['data']
	);	

	$table->item_category_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=15%'
	);

	$table->item_price(
		Table::ATTRIBUTE_NOWRAP,
		"width=10%"
	);

	$table->item_active(
		Table::ATTRIBUTE_NOWRAP,
		"width=2%",
		"class=text-center"
	);

	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	