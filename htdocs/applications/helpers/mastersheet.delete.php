<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$mastersheet = new Mastersheet($appliction);
	$mastersheet->read($id);
	
	if ($mastersheet->id && user::permission(Mastersheet::PERMISSION_EDIT) ) {
		
		// db integrity: record can be deleted
		if (dbIntegrity::check($id, 'mps_mastersheets', $application)) {
			
			$response = $mastersheet->delete();
			
			if ($response) {
				
				// remove all master sheet items
				$mastersheet->item()->deleteAll();
				
				// remove all master sheet deliver dates
				$mastersheet->deliveryDate()->deleteAll();
				
				// remove all master sheet files
				$mastersheet->file()->deleteAll();
	
				message::request_deleted();
				url::redirect("/$appliction/$controller");
				
			} else {
				message::request_failure();
				url::redirect("/$appliction/$controller/$action/$id");
			}
		} else {
			
			message::add(array(
				'type'=>'error',
				'keyword'=>'error_db_integrity',
				'life' => 3000
			));
			
			url::redirect("/$appliction/$controller/$action/$id");
		}
	}
	else {
		message::access_denied();
		url::redirect("/$appliction/$controller");
	}