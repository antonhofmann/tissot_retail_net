<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	// url vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$id = $_REQUEST['product_line_file_product_line'];
	$file = $_REQUEST['product_line_file_id'];
	
	$upload_path = "/public/data/files/$application/productlines/$id";
	
	if ($_REQUEST['product_line_file_title']) {
		$data['product_line_file_title'] = $_REQUEST['product_line_file_title'];
	}

	if (isset($_REQUEST['product_line_file_description'])) {
		$data['product_line_file_description'] = $_REQUEST['product_line_file_description'];
	}

	if ($_REQUEST['product_line_file_purpose']) {
		$data['product_line_file_purpose'] = $_REQUEST['product_line_file_purpose'];
	}
	
	if ($_REQUEST['has_upload'] && $_REQUEST['product_line_file_path']) {
		
		$path = upload::move($_REQUEST['product_line_file_path'], $upload_path);
		$data['product_line_file_path'] = $path;

		$extension = file::extension($path);
		$data['product_line_file_type'] = File_Type::get_id_from_extension($extension);
	}
	
	
	if($id && $data) { 
		
		$data['product_line_file_product_line'] = $id;
		
		$productLineFile = new Product_Line_File();
		$productLineFile->read($file);
	
		if ($productLineFile->id) {
			
			$data['user_modified'] = $user->login;
			$response = $productLineFile->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $productLineFile->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect'] ? $_REQUEST['redirect']."/$id" : null;
		}	
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}

	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'path' => $path
	));
