<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['application_id'];
	
	$data = array();
	
	if ($_REQUEST['application_db']) {
		$data['application_db'] = $_REQUEST['application_db'];
	}
	
	if ($_REQUEST['application_shortcut']) {
		$data['application_shortcut'] = $_REQUEST['application_shortcut'];
	}
	
	if ($_REQUEST['application_name']) {
		$data['application_name'] = $_REQUEST['application_name'];
	}
	
	if (isset($_REQUEST['application_description'])) {
		$data['application_description'] = $_REQUEST['application_description'];
	}
	
	if (in_array('application_active', $fields)) {
		$data['application_active'] = ($_REQUEST['application_active']) ? 1 : 0;
	}
	
	if (in_array('application_involved_in_planning', $fields)) {
		$data['application_involved_in_planning'] = ($_REQUEST['application_involved_in_planning']) ? 1 : 0;
	}
	
	if ($data) {
	
		$Application = new Application();
		$Application->read($id);
	
		if ($id) {
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			$response = $Application->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$response = $id = $Application->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	