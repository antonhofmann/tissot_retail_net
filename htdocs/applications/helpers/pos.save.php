<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init(); 
$user = User::instance();
$translate = Translate::instance();

$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];
$id = $_REQUEST['posaddress_id'];

$pos = new Pos();
$pos->read($id);

// request vars
$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

$country = ($_REQUEST['posaddress_country']) ? $_REQUEST['posaddress_country'] : $pos->country;
$province = ($_REQUEST['province']) ? $_REQUEST['province'] : $pos->province_id;
$place = ($_REQUEST['posaddress_place_id']) ? $_REQUEST['posaddress_place_id'] : $pos->place_id;

$data = array();
$errors = array();

if (!$id && $application=='mps') {
	$data['posaddress_ownertype'] = 6;
	$data['posaddress_store_postype'] = 4;
}

// company parent
if ($_REQUEST['posaddress_client_id']) {
	$data['posaddress_client_id'] = $_REQUEST['posaddress_client_id'];
}

// pos owner
if ($_REQUEST['posaddress_franchisee_id']) {
	$data['posaddress_franchisee_id'] = $_REQUEST['posaddress_franchisee_id'];
}

// pos name
if (isset($_REQUEST['posaddress_name'])) {
	$data['posaddress_name'] = $_REQUEST['posaddress_name'];
}

// pos name 2
if (isset($_REQUEST['posaddress_name2'])) {
	$data['posaddress_name2'] = $_REQUEST['posaddress_name2'];
}

// posaddress street
if (isset($_REQUEST['posaddress_street'])) {
	$data['posaddress_street'] = trim($_REQUEST['posaddress_street']);
}

// posaddress street number
if (isset($_REQUEST['posaddress_street_number'])) {
	$data['posaddress_street_number'] = trim($_REQUEST['posaddress_street_number']);
}

// pos address
if (isset($_REQUEST['posaddress_street'])) {
	
	$countryData = new Country();
	$tmp = $countryData->read($country);
	
	if($_REQUEST['posaddress_street_number']) {
		
		if($tmp['country_nr_before_streename'] == 1) {
			$data['posaddress_address'] = $_REQUEST['posaddress_street_number'] . ', ' . $_REQUEST['posaddress_street'];
		}
		else {
			$data['posaddress_address'] = $_REQUEST['posaddress_street'].' '.$_REQUEST['posaddress_street_number'];
		}
		
		
	}
	else {
		$data['posaddress_address'] = $_REQUEST['posaddress_street'];
	}
	
	// geodata request for address
	$geodata[1] = str_replace(" ", "+", $data['posaddress_address']);
}

// pos address 2
if (isset($_REQUEST['posaddress_address2'])) {
	$data['posaddress_address2'] = $_REQUEST['posaddress_address2'];
}

// pos address
if ($_REQUEST['posaddress_country']) {
	
	$data['posaddress_country'] = $_REQUEST['posaddress_country'];
	
	// geodata request for country
	$country_model = new Country();
	$geodata[3] = str_replace(" ", "+", $country_model->name);
}

// pos province
if ($country && $_REQUEST['new_province'] && $_REQUEST['new_place']) {

	$province_model = new Province();

	$province = $province_model->create(array(
		'province_country' => $country,
		'province_canton' => $_REQUEST['new_province'],
		'user_created' => $user->login,
		'date_created' => date('Y-m-d H:i:s')
	));

	$insertedProvince = true;
}

// pos place
if ($country && $_REQUEST['new_place']) {
	
	$place_model = new Place(); 
	
	$place = $place_model->create(array(
		'place_country' => $country,
		'place_province' => $province,
		'place_name' => $_REQUEST['new_place'],
		'user_created' => $user->login,
		'date_created' => date('Y-m-d H:i:s')
	));
	
	$insertedPlace = true;
	$data['posaddress_place_id'] = $place;
	$data['posaddress_place'] = $_REQUEST['new_place'];
	
	// geodata request for new place
	$geodata[2] = str_replace(" ", "+", $place_model->name);
} 
else if ($_REQUEST['posaddress_place_id']) {
	
	$data['posaddress_place_id'] = $_REQUEST['posaddress_place_id'];
	
	// geodata request for place
	$place_model = new Place();
	$place_model->read($_REQUEST['posaddress_place_id']);
	
	$data['posaddress_place'] = $place_model->name;
	
	$geodata[2] = str_replace(" ", "+", $place_model->name);
}

// sap number
if (isset($_REQUEST['posaddress_sapnumber'])) {
	$data['posaddress_sapnumber'] = $_REQUEST['posaddress_sapnumber'];
}

// shipto number
if (isset($_REQUEST['posaddress_sap_shipto_number'])) {
	$data['posaddress_sap_shipto_number'] = $_REQUEST['posaddress_sap_shipto_number'];
}	

// distribution channel
if(in_array('posaddress_distribution_channel',$fields)) {
	$data['posaddress_distribution_channel'] = $_REQUEST['posaddress_distribution_channel'] ?: null;
}

// pos zip
if (isset($_REQUEST['posaddress_zip'])) {
	$data['posaddress_zip'] = $_REQUEST['posaddress_zip'];
}


if (isset($_REQUEST['posaddress_phone_number'])) {
	$_REQUEST['posaddress_phone_number'] = str_replace(' ', '', $_REQUEST['posaddress_phone_number']);
}

if (isset($_REQUEST['posaddress_phone_area'])) {
	$_REQUEST['posaddress_phone_area'] = str_replace(' ', '', $_REQUEST['posaddress_phone_area']);
}

if (isset($_REQUEST['posaddress_phone_country'])) {
	$_REQUEST['posaddress_phone_country'] = str_replace(' ', '', $_REQUEST['posaddress_phone_country']);
}

if(isset($_REQUEST['posaddress_phone_number'])) {
	// pos phone country code
	if (isset($_REQUEST['posaddress_phone_country'])) {
		$data['posaddress_phone_country'] = $_REQUEST['posaddress_phone_country'];
	}

	// pos phone area code
	if (isset($_REQUEST['posaddress_phone_area'])) {
		$data['posaddress_phone_area'] = $_REQUEST['posaddress_phone_area'];
	}

	// pos phone number
	if (isset($_REQUEST['posaddress_phone_number'])) {
		$data['posaddress_phone_number'] = $_REQUEST['posaddress_phone_number'];
	}

	// pos phone
	if (isset($_REQUEST['posaddress_phone_country']) && isset($_REQUEST['posaddress_phone_area']) && isset($_REQUEST['posaddress_phone_number'])) {
		
		if($_REQUEST['posaddress_phone_area']) {
			$data['posaddress_phone'] = $_REQUEST['posaddress_phone_country']. ' ' . $_REQUEST['posaddress_phone_area'].' ' . $_REQUEST['posaddress_phone_number'];
		}
		else {
			$data['posaddress_phone'] = $_REQUEST['posaddress_phone_country']. ' ' . $_REQUEST['posaddress_phone_number'];
		}
	}
}

if (isset($_REQUEST['posaddress_mobile_phone_number'])) {
	$_REQUEST['posaddress_mobile_phone_number'] = str_replace(' ', '', $_REQUEST['posaddress_mobile_phone_number']);
}

if (isset($_REQUEST['posaddress_mobile_phone_area'])) {
	$_REQUEST['posaddress_mobile_phone_area'] = str_replace(' ', '', $_REQUEST['posaddress_mobile_phone_area']);
}

if (isset($_REQUEST['posaddress_mobile_phone_country'])) {
	$_REQUEST['posaddress_mobile_phone_country'] = str_replace(' ', '', $_REQUEST['posaddress_mobile_phone_country']);
}

if(isset($_REQUEST['posaddress_mobile_phone_number'])) {
	// pos mobile_phone country code
	if (isset($_REQUEST['posaddress_mobile_phone_country'])) {
		$data['posaddress_mobile_phone_country'] = $_REQUEST['posaddress_mobile_phone_country'];
	}

	// pos mobile_phone area code
	if (isset($_REQUEST['posaddress_mobile_phone_area'])) {
		$data['posaddress_mobile_phone_area'] = $_REQUEST['posaddress_mobile_phone_area'];
	}

	// pos mobile_phone number
	if (isset($_REQUEST['posaddress_mobile_phone_number'])) {
		$data['posaddress_mobile_phone_number'] = $_REQUEST['posaddress_mobile_phone_number'];
	}

	// pos mobile_phone
	if (isset($_REQUEST['posaddress_mobile_phone_country']) && isset($_REQUEST['posaddress_mobile_phone_area']) && isset($_REQUEST['posaddress_mobile_phone_number'])) {
		
		
		if($_REQUEST['posaddress_mobile_phone_area']) {
			$data['posaddress_mobile_phone'] = $_REQUEST['posaddress_mobile_phone_country']. ' ' .  $_REQUEST['posaddress_mobile_phone_area']. ' ' . $_REQUEST['posaddress_mobile_phone_number'];
		}
		elseif($_REQUEST['posaddress_mobile_phone_number'])
		{
			$data['posaddress_mobile_phone'] = $_REQUEST['posaddress_mobile_phone_country']. ' ' . $_REQUEST['posaddress_mobile_phone_number'];
		}
	}
}

// pos email
if (isset($_REQUEST['posaddress_email'])) {
	$data['posaddress_email'] = $_REQUEST['posaddress_email'];
}

// pos web
if (isset($_REQUEST['posaddress_website'])) {
	$data['posaddress_website'] = $_REQUEST['posaddress_website'];
}

// pos contact name
if (isset($_REQUEST['posaddress_contact_name'])) {
	$data['posaddress_contact_name'] = $_REQUEST['posaddress_contact_name'];
}

// pos contact email
if (isset($_REQUEST['posaddress_contact_email'])) {
	$data['posaddress_contact_email'] = $_REQUEST['posaddress_contact_email'];
}

// show email on store locator
if(in_array('posaddress_export_to_web', $fields)) {
	$data['posaddress_export_to_web'] = ($_REQUEST['posaddress_export_to_web']) ? 1 : 0;
}

// show web url on store locator
if(in_array('posaddress_email_on_web',$fields)) {
	$data['posaddress_email_on_web'] = ($_REQUEST['posaddress_email_on_web']) ? 1 : 0;
}

// offers free battery change
if(in_array('posaddress_offers_free_battery',$fields)) {
	$data['posaddress_offers_free_battery'] = ($_REQUEST['posaddress_offers_free_battery']) ? 1 : 0;
}

// show web url on jubilee store locator
if(in_array('posaddress_show_in_club_jublee_stores',$fields)) {
	$data['posaddress_show_in_club_jublee_stores'] = ($_REQUEST['posaddress_show_in_club_jublee_stores']) ? 1 : 0;
}

// POS offers VIP Service for Swatch Club Members
if(in_array('posaddress_vipservice_on_web',$fields)) {
	$data['posaddress_vipservice_on_web'] = ($_REQUEST['posaddress_vipservice_on_web']) ? 1 : 0;
}

// Show this POS in the Swatch Club Birthday Voucher Store Locator
if(in_array('posaddress_birthday_vaucher_on_web',$fields)) {
	$data['posaddress_birthday_vaucher_on_web'] = ($_REQUEST['posaddress_birthday_vaucher_on_web']) ? 1 : 0;
}

// pos is identical with company data
if(in_array('posaddress_identical_to_address',$fields)) {
	$data['posaddress_identical_to_address'] = ($_REQUEST['posaddress_identical_to_address']) ? 1 : 0;
}

if (in_array('posaddress_store_openingdate',$fields)) {
	$data['posaddress_store_openingdate'] = ($_REQUEST['posaddress_store_openingdate']) ? date::sql($_REQUEST['posaddress_store_openingdate']) : null;
}


if (in_array('posaddress_store_planned_closingdate',$fields)) {

	$data['posaddress_store_planned_closingdate'] = ($_REQUEST['posaddress_store_planned_closingdate']) ? date::sql($_REQUEST['posaddress_store_planned_closingdate']) : null;

	if ($pos->store_planned_closingdate && $_REQUEST['posaddress_store_planned_closingdate']) {

		$oldPlannedClosingDate = date::timestamp($pos->store_planned_closingdate);
		$newPlannedClosingDate = date::timestamp($_REQUEST['posaddress_store_planned_closingdate']);

		if ($newPlannedClosingDate > $oldPlannedClosingDate ) {
			$deletePlannedClosingDateMailAlert = true;
		}
	}


}

if (in_array('posaddress_store_closingdate',$fields)) {

	$closingdateChanged = false;

	// track chnage planned closing date
	$trackClosingDate = ( ($_REQUEST['posaddress_store_closingdate'] || $pos->store_closingdate) && $_REQUEST['posaddress_store_closingdate'] != $pos->store_closingdate ) ? true : false;
	$oldPosClosingDate = $pos->store_closingdate ? date::sql($pos->store_closingdate) : null;

	// check old closing date
	if ($pos->store_closingdate && $pos->store_closingdate<>'0000-00-00') {
		
		if ($_REQUEST['posaddress_store_closingdate']) {
			$closingdateChanged = $_REQUEST['posaddress_store_closingdate']!=$pos->store_closingdate ? true : false;
		} 
		// closing date is removed
		else {
			$closingdateChanged = true;
			$forcedRedirect = "/$application/$controller/$action/$id";
		}

		$closingDateSendmailTemplate = 19; // Change the closing date of a POS Location
	}
	// set new closing date
	elseif ($_REQUEST['posaddress_store_closingdate']) {
		$closingdateChanged = true;
		$closingDateSendmailTemplate = 21; // Enter the planned closing date of a POS Location
		$forcedRedirect = "/$application/$controller/archived/$action/$id";
	}
	
	$closing_date = date::timestamp($_REQUEST['posaddress_store_closingdate']);
	$aftertomorrow = date('U', strtotime(date("Y-m-d"). ' + 2 days'));
	
	if($closing_date > $aftertomorrow) {
		$errors[] = "The POS closing date must be a date in the past or can only be at maximum two days in the future!";
	} else {
		
		$data['posaddress_store_closingdate'] = ($_REQUEST['posaddress_store_closingdate']) ? date::sql($_REQUEST['posaddress_store_closingdate']) : null;
		
		$franchisee = ($_REQUEST['posaddress_franchisee_id']) ? $_REQUEST['posaddress_franchisee_id'] : $pos->franchisee_id;
		
		$company = new Company();
		$company->read($franchisee);
		
		if ($company->id) {
			
			if ($_REQUEST['posaddress_store_closingdate']) {
				
				$model = new Model(Connector::DB_CORE);
				
				// count active pos locations
				$result = $model->query("
					SELECT 
						COUNT(posaddress_id) as total
					FROM posaddresses
					WHERE 
						posaddress_id != $id
						AND posaddress_franchisee_id = $franchisee 
						AND (
							posaddress_store_closingdate IS NULL
							OR posaddress_store_closingdate = ''
							OR posaddress_store_closingdate = '0000-00-00'
						)	
				")->fetch();
				
				// set franchisee as inactive
				if ($result['total'] == 0 && $company->type != 1) {
					$company->update(array(
						'address_active' => 0
					));
				}
				
			} 
			// set franchisee as active
			elseif (!$company->active) {
				$company->update(array(
					'address_active' => 1
				));
			}
		}
	}

	// send mail for change closing date
	if (!$errors && $closingdateChanged) {
		
		$permittedStoreTypes = (in_array($pos->store_postype, array(1,2,3))) ? true : false;
		$currentClosingDate = $pos->store_closingdate ? date::timestamp($pos->store_closingdate) : 0;
		
		if ($permittedStoreTypes && $closing_date <> $currentClosingDate) {
			$sendmailClosingDate = true;	
		}

	} else {
		$sendmailClosingDate = false;
	}
}


if (in_array('posaddress_store_planned_closingdate',$fields)) {

	$plannedClosingdateChanged = false;

	// track chnage planned closing date
	$trackPlannedClosingDate = ( ($_REQUEST['posaddress_store_planned_closingdate'] || $pos->store_planned_closingdate) && $_REQUEST['posaddress_store_planned_closingdate'] != date::system($pos->store_planned_closingdate) ) ? true : false;
	$oldPosPlannedClosingDate = $pos->store_planned_closingdate;

	// check is planned closing date changed
	if ($pos->store_planned_closingdate && $pos->store_planned_closingdate <> '0000-00-00') {

		if ($_REQUEST['posaddress_store_planned_closingdate']) {
			$plannedClosingdateChanged = $_REQUEST['posaddress_store_planned_closingdate']!=date::system($pos->store_planned_closingdate) ? true : false;
		} else {
			$plannedClosingdateChanged = true;
		}

		$plannedClosingDateSendmailTemplate = 20; // Change the planned closing date of a POS Location
	}
	// set new planned closing date
	elseif ($_REQUEST['posaddress_store_planned_closingdate']) {
		$plannedClosingdateChanged = true;
		$plannedClosingDateSendmailTemplate = 22; // Enter the planned closing date of a POS Location
	}

	// send mail for change planned closing date
	if ($plannedClosingdateChanged) {
		
		$permittedStoreTypes = (in_array($pos->store_postype, array(1,2,3))) ? true : false;
		$currentPlannedClosingDate = $pos->store_planned_closingdate ? strtotime($pos->store_planned_closingdate) : 0;
		$plannedClosingDate = date::timestamp($_REQUEST['posaddress_store_planned_closingdate']);
		
		if ($permittedStoreTypes && $plannedClosingDate <> $currentPlannedClosingDate) {
			$sendmailPlannedClosingDate = true;
		}


		
		if($oldPosPlannedClosingDate != NULL and $oldPosPlannedClosingDate != '0000-00-00' and !$_REQUEST['planned_closing_change_comment']) {
			$errors[] = "Please indicate a reason for changing the planned closing date!";
		}
		
	} else {
		$sendmailPlannedClosingDate = false;
	}


}

if (in_array('posaddress_identical_to_address',$fields)) {
	$data['posaddress_identical_to_address'] = ($_REQUEST['posaddress_identical_to_address']) ? 1 : 0;
}

// get company geodatas
if ($geodata && !$pos->google_precision) {

	ksort($geodata);

	$geodata = join('+', $geodata);

	$host = $settings->shortcut;
	
	if (preg_match("/retailnet.$host/", $_SERVER["HTTP_HOST"])) {
		$opts	= array('http'	=> array('proxy'=>'proxy01.sharedit.ch:8082', 'request_fulluri'=>true));
		$context = stream_context_create($opts);
		$request = "https://maps.google.com/maps/api/geocode/json?address=$geodata&sensor=false&key=AIzaSyCT0qo4PQAkgZuT3A2niAE9Gvqp6fir0G0";
		$geocode = file_get_contents($request, false, $context);
	}
	else {
		$request = "https://maps.google.com/maps/api/geocode/json?address=$geodata&sensor=false&key=AIzaSyCT0qo4PQAkgZuT3A2niAE9Gvqp6fir0G0";
		$geocode = file_get_contents($request);
	}

	if ($geocode) {
		$output= json_decode($geocode);
		$data['posaddress_google_lat'] = $output->results[0]->geometry->location->lat;
		$data['posaddress_google_long'] = $output->results[0]->geometry->location->lng;
		$data['posaddress_google_precision'] = 1;
	}
}

if ($data && !$errors) {
	
	if ($id) {
		
		$data['user_modified'] = $user->login;
		$data['date_modified'] = date('Y-m-d H:i:s');
		
		$response = $pos->update($data);

		$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;

		if ($response && $forcedRedirect) {
			$redirect = $forcedRedirect;
		}
		
	} else {
		
		$data['user_created'] = $user->login;
		$data['date_created'] = date('Y-m-d H:i:s');
		
		$response = $id = $pos->create($data);

		$message = ($action) ? Message::request_inserted() : $translate->message_request_failure;
		
		$redirect = $_REQUEST['redirect']."/$id";
	}
	

	// update company data for identical pos address
	if ($response && $_REQUEST['posaddress_identical_to_address']) {
		
		$company = new Company();
		$company->read($pos->franchisee_id);
		$company->update(array(
			//'address_company' => $pos->name,
			//'address_company2' => $pos->name2,
			'address_address' => $pos->address,
			'address_street' => $pos->street,
			'address_streetnumber' => $pos->street_number,
			'address_address2' => $pos->address2,
			'address_country' => $pos->country,
			'address_place_id' => $pos->place_id,
			'address_zip' => $pos->zip,
			'address_phone' => $pos->phone,
			'address_phone_country' => $pos->phone_country,
			'address_phone_area' => $pos->phone_area,
			'address_phone_number' => $pos->phone_number,
			'address_mobile_phone' => $pos->mobile_phone,
			'address_mobile_phone_country' => $pos->mobile_phone_country,
			'address_mobile_phone_area' => $pos->mobile_phone_area,
			'address_mobile_phone_number' => $pos->mobile_phone_number,
			'address_email' => $pos->email,
			'address_website' => $pos->website,
			'address_contact_name' => $pos->contact_name,
			'address_contact_email' => $pos->contact_email
		));
	}

	// on change planned closing date
	// remove old mail alert from system
	if ($deletePlannedClosingDateMailAlert) {

		$model = new Model();

		$result = $model->query("
			SELECT posorder_order 
			FROM posorders 
			WHERE posorder_type = 1 
			AND posorder_posaddress = $pos->id 
			AND posorder_subclass = 27 
			AND (posorder_closing_date IS NULL OR posorder_closing_date = '0000-00-00')
		")->fetch();

		if ($result['posorder_order']) {
			$model->db->exec("
				DELETE FROM mail_alerts 
				WHERE mail_alert_type = 25 
				AND mail_alert_order = '{$result[posorder_order]}'
			");
		}
	}
	
	// sendmail closing date changed
	if ($response && $sendmailClosingDate && $closingDateSendmailTemplate) {
		
		$mail = new ActionMail($closingDateSendmailTemplate);
		
		$mail->setParam('id', $id);
		$mail->setParam('section', 'posaddress_store_closingdate');
		
		$model = new Model(Connector::DB_CORE);
		$dataloader = $model->query("
			SELECT *
			FROM posaddresses 
			LEFT JOIN countries ON country_id = posaddress_country
			LEFT JOIN posowner_types ON posowner_type_id = posaddress_ownertype
			LEFT JOIN postypes ON postype_id = posaddress_store_postype
			WHERE posaddress_id = $id
		")->fetch();
		
		// link
		$protocol = Settings::init()->http_protocol.'://';
		$server = $_SERVER['SERVER_NAME'];
		$dataloader['link'] = $protocol.$server."/pos/posindex_pos.php?id=$id";
		$dataloader['closing_date'] = $_REQUEST['posaddress_store_closingdate'];
		

		
		
		// dataloader
		$mail->setDataloader($dataloader);
		
		// send mails
		$mail->send();

		$_RESPONSE = $mail->isSuccess();
		$_CONSOLE = $mail->getConsole();
		$totalSendMails = $mail->getTotalSentMails();


		if($closingDateSendmailTemplate == 21 and $pos->data["posaddress_ownertype"] == 1) {
			$dchannel = 'not available';
			
			$model = new Model(Connector::DB_CORE);
			$row = $model->query("select concat(mps_distchannel_name, ' - ', mps_distchannel_code) as dchannel 
				   from mps_distchannels 
				   where mps_distchannel_id = '" . $pos->data["posaddress_distribution_channel"] . "'")->fetch();
			
			
			if ($row)
			{
				$dchannel = $row["dchannel"];
			}
			
			$Mail = new ActionMail('info.new.closings.corporate.pos');

			$data = array(
				'country' => $pos->data['country_name'],
				'pos_name' => $pos->data['posaddress_name'],
				'closing_date' => $_REQUEST['posaddress_store_closingdate'],
				'eprnr' => $pos->data['posaddress_eprepnr'] ? $pos->data['posaddress_eprepnr'] : "not available",
				'dchannel' => $dchannel,
				'sap_nr' => $pos->data['posaddress_sapnumber'] ? $pos->data['posaddress_sapnumber'] : 'not available',
				'sap_shipto_nr' => $pos->data['posaddress_sap_shipto_number'] ? $pos->data['posaddress_sap_shipto_number'] : 'not available'
			);
			
			
				
			$Mail->setDataloader($data);
			
			$Mail->send(true);

			$recipients = $Mail->getRecipients();


			
			$rcpts = array();
			$mail_text = '';
			$subject = '';
			foreach($recipients as $user_id=>$recipient_data)
			{
				$row = $model->query("select user_email from users where user_id = '" . $user_id . "'")->fetch();
				if ($row)
				{
					$rcpts[] = $row["user_email"];
				}
				$mail_text = $recipient_data->getBody(true);
				$subject = $recipient_data->getSubject(true);
			}


			//update mail history
			if($mail_text and count($rcpts) > 0) {
				$pos_mail_fields = array();
				$pos_mail_values = array();

				$pos_mail_fields[] = "posmail_posaddress_id";
				$pos_mail_values[] = '"' . $pos->data["posaddress_id"] . '"';

				$pos_mail_fields[] = "posmail_mail_template_id";
				$pos_mail_values[] = 153;;

				$pos_mail_fields[] = "posmail_sender_email";
				$pos_mail_values[] = '"' . $Mail->getSender()->email . '"';


				$pos_mail_fields[] = "posmail_recipeint_email";
				$pos_mail_values[] = '"' . implode(';', $rcpts) . '"';

				$pos_mail_fields[] = "posmail_subject";
				$pos_mail_values[] = '"' . $subject . '"';

				$pos_mail_fields[] = "posmail_text";
				$pos_mail_values[] = '"' . $mail_text . '"';

				$pos_mail_fields[] = "date_created";
				$pos_mail_values[] = "current_timestamp";

				$pos_mail_fields[] = "user_created";
				$pos_mail_values[] = '"' . $_SESSION["user_login"] . '"';


				$sql = "insert into posmails (" . join(", ", $pos_mail_fields) . ") values (" . join(", ", $pos_mail_values) . ")";

				
				$db = Connector::get(Connector::DB_CORE);

				$sth = $db->prepare($sql);
				$sth->execute();
				

			}
		}
	}
	
	// sendmail planned closing date changed
	if ($response && $sendmailPlannedClosingDate && $plannedClosingDateSendmailTemplate) {
		
		$mail = new ActionMail($plannedClosingDateSendmailTemplate);
		
		$mail->setParam('id', $id);
		$mail->setParam('section', 'posaddress_store_planned_closingdate');
		
		$model = new Model(Connector::DB_CORE);
		$dataloader = $model->query("
			SELECT *
			FROM posaddresses 
			LEFT JOIN countries ON country_id = posaddress_country
			LEFT JOIN posowner_types ON posowner_type_id = posaddress_ownertype
			LEFT JOIN postypes ON postype_id = posaddress_store_postype
			WHERE posaddress_id = $id
		")->fetch();
		
		// link
		$protocol = Settings::init()->http_protocol.'://';
		$server = $_SERVER['SERVER_NAME'];
		$dataloader['link'] = $protocol.$server."/pos/posindex_pos.php?id=$id";
		$dataloader['planned_closing_date'] = $_REQUEST['posaddress_store_planned_closingdate'];
		$dataloader['reason'] = $_REQUEST['planned_closing_change_comment'];
		
		// dataloader
		$mail->setDataloader($dataloader);
		
		// send mails
		$mail->send();
		
		$_RESPONSE = $mail->isSuccess();
		$_CONSOLE = $mail->getConsole();
		$totalSendMails = $mail->getTotalSentMails();
	}

	if ($totalSendMails) {
		$message .= "<br /><br />Total send $totalSendMails E-mails";
	}
	
	// update order data for non poPup Projects
	if ($closingdateChanged && $pos->id) {
		
		$model = new Model(Connector::DB_CORE);
		
		$closing_date = date::sql($pos->store_closingdate);
		
		$result = $model->query("
			SELECT 
				posorder_order 
			FROM posorders 
			LEFT JOIN orders ON order_id = posorder_order 
			WHERE (order_actual_order_state_code <> '900' OR order_actual_order_state_code IS NULL) 
			AND posorder_type = 1 
			AND posorder_posaddress = $pos->id
			AND posorder_opening_date IS NOT NULL 
			AND posorder_opening_date <> '0000-00-00' 
			AND posorder_project_kind <> 8  
			ORDER BY 
				posorder_year DESC, 
				posorder_opening_date DESC
		")->fetch();

		if ($result['posorder_order']) {
			
			
			if($closing_date)
			{
				$model->db->exec("
					UPDATE projects SET 
						project_state = 5, project_shop_closingdate = '$closing_date'
					WHERE project_order = ".$result["posorder_order"]."	
				");
			}
			else
			{
				$model->db->exec("
					UPDATE projects SET 
						project_state = 4, project_shop_closingdate = '$closing_date'
					WHERE project_order = ".$result["posorder_order"]."	
				");
			}
		}
		else {
			
			$result = $model->query("
				SELECT
					posorder_id
					FROM posorders
				WHERE posorder_type = 1
				AND posorder_posaddress = $pos->id
				ORDER BY
					posorder_year DESC,
					posorder_opening_date DESC
			")->fetch();

			if ($result['posorder_id']) {
				$model->db->exec("
					UPDATE posorders SET
						posorder_closing_date = '$closing_date'
					WHERE posorder_id = ".$result["posorder_id"]."
				");
			}
		}
	}


	//update planned clsoing date for non PopUp Projects
	if ($plannedClosingdateChanged && $pos->id)
	{
		
		$model = new Model(Connector::DB_CORE);
		$plannedClosing_date = $pos->store_planned_closingdate;
		
		$result = $model->query("
			SELECT 
				posorder_order 
			FROM posorders 
			LEFT JOIN orders ON order_id = posorder_order 
			WHERE (order_actual_order_state_code <> '900' OR order_actual_order_state_code IS NULL) 
			AND posorder_type = 1 
			AND posorder_posaddress = $pos->id
			AND posorder_opening_date IS NOT NULL 
			AND posorder_opening_date <> '0000-00-00' 
			AND posorder_project_kind <> 8  
			ORDER BY 
				posorder_year DESC, 
				posorder_opening_date DESC
		")->fetch();
		
		if ($result['posorder_order']) {
			$model->db->exec("
				UPDATE projects SET 
					project_planned_closing_date = '$plannedClosing_date'
				WHERE project_order = ".$result["posorder_order"]."	
			");
		}
	}

	// track planned closing date
	if($response && $trackPlannedClosingDate && $plannedClosingdateChanged) {
		$pos->track()->create(
			$user->id, 
			'posaddress_store_planned_closingdate',
			substr($oldPosPlannedClosingDate, 0,10),
			substr(date::sql($_REQUEST['posaddress_store_planned_closingdate']), 0, 10),
			$_REQUEST['planned_closing_change_comment'] ? $_REQUEST['planned_closing_change_comment']:'changed by user'
		);
	}

	// track closing date
	if($response && $trackClosingDate && $closingdateChanged) {
		$pos->track()->create(
			$user->id, 
			'posaddress_store_closingdate',
			substr($oldPosClosingDate, 0, 10),
			substr(date::sql($_REQUEST['posaddress_store_closingdate']), 0, 10),
			'changed by user'
		); 
	}
	
} else {
	
	$response = false;
	$message = $translate->message_request_failure;
	
	if ($errors) {
		foreach ($errors as $error) {
			$message .= "<br /><br />$error";
		}
	}
}

// update store locator
if ($response) {
	$storelocator = new Store_Locator();
	$storelocator->update($id);

}

echo json_encode(array(
	'response' => $response,
	'redirect' => $redirect,
	'message' => $message,
	'console' => $_CONSOLE,
	'dataloader' => $dataloader
));

