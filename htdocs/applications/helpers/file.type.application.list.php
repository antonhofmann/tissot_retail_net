<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", false);
	
	$model = new Model(Connector::DB_CORE);

	// sql order
	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'application_name';
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// sql pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$offset = ($page-1) * $settings->limit_pager_rows;
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		
		$keyword = $_REQUEST['search'];
		
		$filters['search'] = "( 
			application_name LIKE \"%$keyword%\" 
		)";
	}
	
	// datagrid
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS
			application_id,
			application_name
		FROM applications
	")
	->order($sort, $direction)
	->filter($filters)
	->offset($offset)
	->fetchAll();
	
	if ($result) {
		
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		$count = 0;
		
		foreach ($datagrid as $key => $row) {
			
			$result = $model->query("
				SELECT application_filetype_id
				FROM application_filetypes
				WHERE application_filetype_application = $key AND application_filetype_filetype = $id
			")->fetch();
			
			$dataloader['filetype'][$key] = ($result) ? 1 : 0;
			$datagrid[$key]['filetype'] = $key;
			if ($result) $count++;
		}
	
		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));
	
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
		
		$checkall = (count($datagrid) == $count) ? 'checked=checked' : null;
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";

	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	$table->dataloader($dataloader);
	
	$table->caption('filetype', "<input type=checkbox class=checkall $checkall />");
	
	$table->application_name(
		Table::ATTRIBUTE_NOWRAP
	);
	
	$table->filetype(
		Table::DATA_TYPE_CHECKBOX,
		'width=2%',
		'caption=&nbsp;'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	