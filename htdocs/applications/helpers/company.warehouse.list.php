<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	// request vars
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];

	// permissions
	$permission_edit = user::permission(Company::PERMISSION_EDIT);
	$permission_edit_limited = user::permission(Company::PERMISSION_EDIT_LIMITED);
	
	// db application model
	$model = new Model($application);

	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

	// sql order
	$order = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "address_warehouse_name";
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rows = $settings->limit_pager_rows;
	$offset = ($page-1) * $rows;

	// can edit companies
	$can_edit_companies = (user::permission(Company::PERMISSION_EDIT) OR user::permission(Company::PERMISSION_EDIT_LIMITED)) ? true : false;

	// has limited permission
	$has_limited_permission = (!user::permission(Company::PERMISSION_EDIT) AND user::permission(Company::PERMISSION_EDIT_LIMITED)) ? true : false;

	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
		$keyword = $_REQUEST['search'];
		$filters['search'] = "(
			address_warehouse_name LIKE \"%$keyword%\"
		)";
	}
	
	// company filter
	$filters['company'] = "address_warehouse_address_id = $id";
	$filters['reserve'] = "(address_warehouse_stock_reserve IS NULL OR address_warehouse_stock_reserve = '')";

	// data: companies
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			address_warehouse_id,
			address_warehouse_name,
			address_warehouse_active
		FROM db_retailnet.address_warehouses
	")
	->bind(Company_Warehouse::DB_BIND_COMPANIES)
	->filter($filters)
	->order($order, $direction)
	->offset($offset, $rows)
	->fetchAll();

	
	if ($result) {

		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);

		$pager = new Pager(array(
			'page' => $page,
			'totalrows' => $totalrows,
			'buttons' => true
		));

		$pageIndex = $pager->index();
		$pageControlls = $pager->controlls();
		$icon_checked = ui::icon('checked');

		foreach ($datagrid as $key => $row) {
			$datagrid[$key]['address_warehouse_active'] = ($row['address_warehouse_active']) ? $icon_checked : null;
		}
	}

	// toolbox: hide utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$order' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";

	// toolbox: button print
	if ($datagrid && $_REQUEST['export']) {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'href' => $_REQUEST['export'],
			'caption' => $translate->print,
		));
	}
	
	// toolbox: button add
	if ( $can_edit_companies && $_REQUEST['add'] && !$archived) {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'caption' => $translate->add_new
		));
	}

	// toolbox: serach full text
	$toolbox[] = ui::searchbox();

	

	if ($toolbox) {
		$toolbox = "<div class='table-toolbox'><form class=toolbox method=post>".join($toolbox)."</form></div>";
	}

	$table = new Table(array(
		'sort' => array('column' => $order, 'direction' => $direction)
	));

	$table->datagrid = $datagrid;

	$table->address_warehouse_name(
		"href=".$_REQUEST['form'],
		Table::PARAM_SORT
	);

	$table->address_warehouse_active(
		'width=10%',
		Table::ATTRIBUTE_NOWRAP,
		Table::ATTRIBUTE_ALIGN_CENTER
	);

	$table->footer($pageIndex);
	$table->footer($pageControlls);
	$table = $table->render();

	echo $toolbox.$table;
