<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$pos = $_REQUEST['pos'];
	$days = $_REQUEST['days'];
	
	if ($pos) {
			
		$pos_closing_day = new Pos_Closing_Day();
		$pos_closing_day->read_from_pos($pos);
		
		if ($_REQUEST['days']) {
			if ($pos_closing_day->id) { 
				$response = $pos_closing_day->update(array(
					'posclosinghr_text' => $days,
					'user_modified' => $user->login,
					'date_modified' => date('Y-m-d H:i:s')
				));
				
			} else { 
				$response = $pos_closing_day->create(array(
					'posclosinghr_posaddress_id' => $pos,
					'posclosinghr_text' => $days,
					'user_created' => $user->login,
					'date_created' => date('Y-m-d H:i:s')
				));
			}
		}
		else {
			$response = $pos_closing_day->delete($pos_closing_day->id);
		}

		$response = true;
		$message = $translate->message_request_updated;
		
		// update store locator
		$storelocator = new Store_Locator();
		$storelocator->update($pos);
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	header('Content-Type: text/json');
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));
