<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$url = $_REQUEST['url'];
$request = Request::instance($url);
$translator = Translate::instance();

if ($_REQUEST['mail_template']) {
	
	$mail = new ActionMail($_REQUEST['mail_template']);
	
	// mail subject
	if ($_REQUEST['mail_subject']) {
		$mail->setSubject($_REQUEST['mail_subject']);
	}
	
	// mail content
	if ($_REQUEST['mail_content']) {
		$mail->setBody($_REQUEST['mail_content']);
	}
	
	// action file entity
	if ($_REQUEST['entity']) {
		$mail->setParam('entity', $_REQUEST['entity']);
	}
	
	$mail->send();

	$json = array(
		'response' => true,
		'sendmail' => $mail->isSuccess(),
		'message' => $translator->message_mails
	);
} 
else {

	$json = array(
		'response' => false,
		'sendmail' => false,
		'message' => $translator->message_mails
	);
}

header('Content-Type: text/json');
echo json_encode($json);