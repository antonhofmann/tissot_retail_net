<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// request vars
	$application = $_REQUEST['application'];
	$section = $_REQUEST['section'];
	$action = $_REQUEST['action'];
	$mastersheet = $_REQUEST['mps_mastersheet_id'];
	$material = $_REQUEST['mps_material_id'];
	$new_material = $_REQUEST['item_id'];
	$disabled = $_REQUEST['disabled'];
	
	// editabled workflow states
	$editabled_workflowstates = join(',', array(
		Workflow_State::STATE_PREPARATION,
		Workflow_State::STATE_OPEN,
		Workflow_State::STATE_PROGRESS,
		Workflow_State::STATE_COMPLETED,
		Workflow_State::STATE_APPROVED,
		Workflow_State::STATE_REVISION
	));
	
	// database model
	$model = new Model($application);
	
	switch ($section) {

		
		// master sheets loader
		case 'mastersheets': 
			
			// filter master sheet items
			$filter = ($action==1)
				? "mps_mastersheet_item_material_id != $material"
				: "mps_mastersheet_item_material_id = $material";
			
			// get master sheets from items
			$result = $model->query("
				SELECT DISTINCT 
					mps_mastersheet_id, 
					mps_mastersheet_name
				FROM 
					mps_mastersheets
				LEFT JOIN mps_mastersheet_items ON mps_mastersheet_item_mastersheet_id = mps_mastersheet_id
				WHERE 
					mps_mastersheet_archived != 1
					AND mps_mastersheet_consolidated != 1
					AND $filter
				ORDER BY mps_mastersheet_name
			")->fetchAll();
				
			$mastersheets_from_items = ($result) ?  _array::extract($result) : array();
			

			// filter order sheet items
			$filter = ($action==1)
				? "mps_ordersheet_item_material_id != $material"
				: "mps_ordersheet_item_material_id = $material";
			
			
			// master sheet from order sheet items
			$result = $model->query("
				SELECT DISTINCT 
					mps_mastersheet_id, 
					mps_mastersheet_name
				FROM mps_ordersheet_items
					INNER JOIN mps_ordersheets ON  mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
					INNER JOIN mps_mastersheets ON mps_mastersheet_id = mps_ordersheet_mastersheet_id
					INNER JOIN db_retailnet.addresses ON mps_ordersheet_address_id = address_id
					INNER JOIN db_retailnet.countries ON address_country = country_id
				WHERE
					mps_ordersheet_workflowstate_id IN ($editabled_workflowstates)
					AND $filter
				ORDER BY mps_mastersheet_name
			")->fetchAll();
			
			$mastersheets_from_ordersheets = ($result) ? _array::extract($result) : array();
			
			
			
			// group master sheets
			$mastersheets = $mastersheets_from_items + $mastersheets_from_ordersheets;
			
			// sort master sheets
			uasort($mastersheets, function($a, $b) {
				if ($a == $b) {
					return 0;
				}
				return ($a < $b) ? -1 : 1;
			});
			

			if ($mastersheets) {
				
				$content = "<option value=''>$translate->select</option>";
			
				foreach ($mastersheets as $value => $name) {
					$content .= "<option value=$value >$name</option>";
				}
			
				$json['response'] = true;
				$json['content'] = $content;
			} 
			else {
				$json['response'] = false;
				$json['content'] = "<span class='mastersheet noresult'>No matching records found</span>";
			}
			
		break;
		
		
		// order sheets loader
		case 'ordersheets':
			
			// action filter
			$filter = ($action==1)
				? "mps_ordersheet_item_material_id != $material"
				: "mps_ordersheet_item_material_id  = $material";
			
			// get ordersheets
			$result = $model->query("
				SELECT DISTINCT
					mps_ordersheet_id,
					CONCAT(address_company,', ', country_name) AS caption
				FROM 
					mps_ordersheet_items
				INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
				INNER JOIN db_retailnet.addresses ON mps_ordersheet_address_id = address_id
				INNER JOIN db_retailnet.countries ON address_country = country_id
				WHERE 
					mps_ordersheet_mastersheet_id = $mastersheet
					AND mps_ordersheet_workflowstate_id IN ($editabled_workflowstates)
					AND $filter
				ORDER BY 
					address_company, 
					country_name
			")->fetchAll();
			
			$ordersheets = _array::extract($result);
			
			
			// get ordersheet items
			$result = $model->query("
				SELECT DISTINCT
					mps_ordersheet_item_id,
					mps_ordersheet_id,
					mps_ordersheet_item_material_id
				FROM mps_ordersheet_items
				INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
				WHERE 
					mps_ordersheet_mastersheet_id = $mastersheet 
					AND mps_ordersheet_workflowstate_id IN ($editabled_workflowstates)
			")->fetchAll();
			
			if ($result) {
				foreach ($result as $row) {
					$idOrdersheet = $row['mps_ordersheet_id'];
					$idMaterial = $row['mps_ordersheet_item_material_id'];
					$items[$idOrdersheet][$idMaterial] = $row['mps_ordersheet_item_id'];
				}
			}	
			
			
			if ($ordersheets) {
				
				foreach ($ordersheets as $ordersheet => $caption) {	

					if ($action==1) {
						$show = !$items[$ordersheet][$material] ? true : false;
					} 
					// action remove and replace
					else {
						$show = $items[$ordersheet][$material] ? true : false;
					}
					
					if ($show) {
						$content .= "
							<span class='ordersheet'>
								<input type=checkbox class=ordersheet name=ordersheets[$ordersheet] value=$ordersheet checked=checked />
								$caption
							</span>
						";
					}
				}	
			}
			
			if ($content) {
				$json['response'] = true;
				$json['content'] = $content;
			} else {
				$json['response'] = false;
				$json['content'] = "<span class='ordersheet noresult'>No matching records found</span>";
			}
				
		break;
	
		
		// load replacements items
		case 'items' :
			
			$Material = new Material($application);
			$Material->read($material);
			
			// select materials
			$result = $model->query("
				SELECT 
					mps_material_id,
					CONCAT(mps_material_code, ', ', LEFT(mps_material_name,60)) as caption
				FROM mps_materials
				WHERE
					mps_material_material_planning_type_id = $Material->material_planning_type_id
					AND mps_material_material_collection_category_id = $Material->material_collection_category_id
					AND mps_material_id != $material
				ORDER BY mps_material_code, mps_material_name
			")->fetchAll();
			
			$materials = ($result) ? _array::extract($result) : array();
			
			if ($materials) {
			
				$content = "<option value=''>".$translate->select."</option>";
				
				foreach ($materials as $value => $caption) {
					$content .= "<option value='$value' >$caption</option>";
				}
				
				$json['response'] = true;
				$json['content'] = $content;
				
			} else {
				$json['response'] = false;
				$json['content'] = "<span class='ordersheet noresult'>No matching records found</span>";
			}
				
		break;
		
		case 'info':
			
			$material = new Material($application);
			$material->read($_REQUEST['id']);
			
			$json['content'] = "
				<table class=default >
					<tr>
						<td nowrap=nowrap >Planning Type</td>
						<td nowrap=nowrap ></td>
					</tr>
					<tr>
						<td nowrap=nowrap >Collection Code</td>
						<td nowrap=nowrap ></td>
					</tr>
					<tr>
						<td nowrap=nowrap >Category Code</td>
						<td nowrap=nowrap ></td>
					</tr>
					<tr>
						<td nowrap=nowrap >Item Code</td>
						<td nowrap=nowrap >".$material->code."</td>
					</tr>
					<tr>
						<td nowrap=nowrap >Item Name</td>
						<td nowrap=nowrap >".$material->name."</td>
					</tr>
					<tr>
						<td nowrap=nowrap >Pricee</td>
						<td nowrap=nowrap ></td>
					</tr>
				</table>
			";
			
			$json['response'] = true;
			
		break;

	}
	
	header('Content-Type: text/json');
	echo json_encode($json);
	