<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$table = $_REQUEST['table'];
	$id = $_REQUEST['id'];
	
	if ($id && $table && DB_Reference::PERMISSION_EDIT) {
		
		$db_reference = new DB_Reference();
		$db_reference->read($id);
		$delete = $db_reference->delete();
		
		if ($delete) {
			Message::request_deleted();
			url::redirect("/$application/$controller/$action/$table");
		} else {
			Message::request_failure();
			url::redirect("/$application/$controller/$action/$table/$id");
		}
	}
	else {
		Message::access_denied();
		url::redirect("/$application/$controller");
	}