<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	$translate = Translate::instance();
	
	switch ($_REQUEST['section']) {
		
		case 'check-store-locator-id':
		
			$model = new Model(Connector::DB_CORE);
			
			$country = $_REQUEST['country'];
			$id = $_REQUEST['id'];
			
			$filter = $country ? "AND country_id != $country" : null;
			
			$result = $model->query("
				SELECT country_id
				FROM countries
				WHERE country_store_locator_id = $id $filter	
			")->fetch();
			
			$json['response'] = $result['country_id'] ? true : false;
			
			if ($json['response']) {
				
				$json['notification'] = array(
						'content' => $translate->error_country_store_locator_id,
						'properties' => array(
							'theme' => 'error',
							'sticky' => true
						)
				);
			}
			
		break;
	}

	
	header('Content-Type: text/json');
	echo json_encode($json);