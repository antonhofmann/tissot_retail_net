<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

  	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$project = $_REQUEST['id'];
	
	$permission_view = user::permission(Red_Project::PERMISSION_VIEW_PROJECTS);
	$permission_view_limited = user::permission(Red_Project::PERMISSION_VIEW_LIMITED_PROJECTS);
	
	// db model
	$model = new Model($application);
	
	// get files allocated to this project
	$files = $model->query("
		SELECT SQL_CALC_FOUND_ROWS
			red_file_id,
			red_file_project_id,
			red_file_category_id,
			red_filecategory_name,
			red_file_owner_user_id,
			red_file_title,
			red_file_path,
			user_firstname,
			user_name,
			LEFT(red_file_description,120) AS red_file_description,
			DATE_FORMAT(red_files.date_created, '%d.%m.%Y') AS date_created
		FROM red_files
  	")
	->bind(Red_file::BIND_USER_ID)
	->bind(Red_file::BIND_FILE_CATEGORY)
	->filter('project', "red_file_project_id = $project")
	->order('red_files.date_created', 'DESC')
	->fetchAll();

	if (!$permission_view && $permission_view_limited && is_array($files)) {
		
		$fileAccess = new Red_Project_File_Access($application);
		
		foreach ($files as $row) {
			$fileAccess->file($row['red_file_id']);
			$fileAccess->read($user->id);
   		
			if ($fileAccess->id) {
   				$filtered_files[] = $row;
			}
    	}
    	
    	$files = $filtered_files;
	}

	if (is_array($files)) {
		
		$datagrid = array();
		
		foreach ($files as $row) {
			
			$category = $row['red_file_category_id'];
			$file = $row['red_file_id'];
			$file_extension = file::extension($row['red_file_path']);
			$file_title = $row['red_file_title'];
			$file_owner = $row['user_firstname'].' '.$row['user_name'];
			$file_date = $row['date_created'];
			$file_path = $row['red_file_path'];
			$link = $_REQUEST['form']."/$file";
			
			$datagrid[$category]['category'] = $row['red_filecategory_name'];
			$datagrid[$category]['files'][$file]['title'] = "<a href='$link' >$file_title</a><span>$file_owner, $file_date</span>";
			$datagrid[$category]['files'][$file]['date']= "<span>$file_date</span>";
			$datagrid[$category]['files'][$file]['fileicon']  = "<span class='file-extension $file_extension modal' path='$file_path'></span>";
		}
	}

	if ($datagrid) {
	
		foreach ($datagrid as $key => $row) {

			$table = new Table(array(
				'thead' => false
			));
	
			$table->datagrid = $row['files'];
			$table->fileicon();
			$table->title();
	
			$file_list .= "<h6>".$row['category']."</h6>";
			$file_list .= $table->render();
		}
	}
	
	// toolbox: button add
	if ($_REQUEST['add']) {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'label' => $translate->add_new,
			'href' => 	$_REQUEST['add']	
		));
	}

	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}

  echo $toolbox.$file_list;

