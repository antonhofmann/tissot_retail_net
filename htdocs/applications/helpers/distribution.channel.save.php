<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['mps_distchannel_id'];
	
	$data = array();
	
	if ($_REQUEST['mps_distchannel_group_id']) {
		$data['mps_distchannel_group_id'] = $_REQUEST['mps_distchannel_group_id'];
	}
	
	if (isset($_REQUEST['mps_distchannel_code'])) {
		$data['mps_distchannel_code'] = $_REQUEST['mps_distchannel_code'];
	}
	
	if (isset($_REQUEST['mps_distchannel_name'])) {
		$data['mps_distchannel_name'] = $_REQUEST['mps_distchannel_name'];
	}
	
	if (in_array('mps_distchannel_active', $fields)) {
		$data['mps_distchannel_active'] = ($_REQUEST['mps_distchannel_active']) ? 1 : 0;
	}
	
	if (in_array('mps_distchannel_selectable_in_new_projects', $fields)) {
		$data['mps_distchannel_selectable_in_new_projects'] = ($_REQUEST['mps_distchannel_selectable_in_new_projects']) ? 1 : 0;
	}
	
	if (in_array('mps_distchannel_selectable_by_clients_in_mps', $fields)) {
		$data['mps_distchannel_selectable_by_clients_in_mps'] = ($_REQUEST['mps_distchannel_selectable_by_clients_in_mps']) ? 1 : 0;
	}
	
	if (in_array('mps_distchannel_use_for_warehouses_export_to_sap', $fields)) {
		$data['mps_distchannel_use_for_warehouses_export_to_sap'] = $_REQUEST['mps_distchannel_use_for_warehouses_export_to_sap'] ? 1 : 0;
	}
	
	if ($data) {
		
		$distribution_channel = new DistChannel();
		$distribution_channel->read($id);
		
		if ($id) {
			$data['user_modified'] = $user->login;
			$response = $distribution_channel->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			$response = $id = $distribution_channel->create($data);
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			$redirect = $_REQUEST['redirect']."/$id";
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect,
		'id' => $id
	));