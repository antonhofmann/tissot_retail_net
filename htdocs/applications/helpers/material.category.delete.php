<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$user = User::instance();
	
	$appliction = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	$id = $_REQUEST['id'];
	
	if ($id && $user->permission(Material_Category::PERMISSION_EDIT) ) {
		
		$material_category = new Material_Category($appliction);
		$material_category->read($id);
		$delete = $material_category->delete();
		
		if ($delete) {
			Message::request_deleted();
			url::redirect("/$appliction/$controller");
		} else {
			Message::request_failure();
			url::redirect("/$appliction/$controller/$action/$id");
		}
	}
	else {
		Message::access_denied();
		url::redirect("/$appliction/$controller");
	}