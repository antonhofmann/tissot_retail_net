<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	ini_set('memory_limit', '4096M');
	
	$application = $_REQUEST['application'] ?: 'mps';
	$_PON = $_REQUEST['pon'];
	
	$data = array();
	
	// model
	$model = new Model($application);
	$exchange = new Model(Connector::DB_RETAILNET_EXCHANGES);
	
	// application identificators
	$applications = array(
		'mps' => '08'
	);

	$filter = $_PON ? " AND REPLACE(LOWER(mps_salesorder_ponumber), ' ', '') = '$_PON' " : null;

	// get all non imported orders
	$result = $exchange->query("
		SELECT 
			TRIM(ramco_order_type) AS ramco_order_type,
			TRIM(mps_salesorder_customernumber) AS mps_salesorder_customernumber,
			TRIM(mps_salesorder_ponumber) AS mps_salesorder_ponumber,
			TRIM(ramco_sold_to_sapnr) AS ramco_sold_to_sapnr,
			TRIM(ramco_sales_order_number) AS ramco_sales_order_number,
			TRIM(ramco_shipto_number) AS ramco_shipto_number,
			TRIM(ramco_billto_number) AS ramco_billto_number,
			TRIM(ramco_order_confirmation_date) AS ramco_order_confirmation_date,
			TRIM(ramco_payment_condition) AS ramco_payment_condition,
			TRIM(ramco_shipment_mode) AS ramco_shipment_mode,
			TRIM(ramco_inco_terms) AS ramco_inco_terms,
			TRIM(ramco_delivered_by) AS ramco_delivered_by,
			TRIM(ramco_employee) AS ramco_employee,
			TRIM(ramco_distribution_code) AS ramco_distribution_code,
			TRIM(ramco_sales_organisation) AS ramco_sales_organisation,
			TRIM(ramco_division) AS ramco_division,
			TRIM(ramco_order_reason) AS ramco_order_reason,
			TRIM(ramco_order_deleted) AS ramco_order_deleted,
			TRIM(mps_order_completely_imported) AS mps_order_completely_imported
		FROM ramco_orders
		WHERE 
			TRIM(ramco_division) = '".$applications[$application]."' $filter
	")->fetchAll();

			
			
	if ($result) {
		
		$pons = array();

		foreach ($result as $row) {
			$pon = $row['mps_salesorder_ponumber'];
			if (is_numeric($pon)) {
				$data[$pon]['orders'][] = $row;
				$pons[] = $pon;
			}
		}
		
		$pons = join(',', array_unique($pons));
		
		// get all confirmed orders
		$result = $exchange->query("
			SELECT 
				TRIM(mps_salesorder_ponumber) AS mps_salesorder_ponumber,
				TRIM(mps_salesorderitem_linenumber) AS mps_salesorderitem_linenumber,
				TRIM(mps_salesorderitem_material_code) AS mps_salesorderitem_material_code,
				TRIM(ramco_eannumber) AS ramco_eannumber,
				TRIM(ramco_confirmed_quantity) AS ramco_confirmed_quantity,
				TRIM(ramco_confirmed_price) AS ramco_confirmed_price,
				TRIM(ramco_estimated_departure_date) AS ramco_estimated_departure_date,
				TRIM(ramco_requested_delivery_date) AS ramco_requested_delivery_date,
				TRIM(ramco_price_discount_code) AS ramco_price_discount_code,
				TRIM(ramco_price_discount_percent) AS ramco_price_discount_percent,
				TRIM(ramco_order_state) AS ramco_order_state,
				TRIM(mps_item_completely_imported) AS mps_item_completely_imported,
				LOWER(REPLACE(mps_salesorderitem_material_code, ' ', '')) AS code
			FROM ramco_confirmed_items
			WHERE TRIM(mps_salesorder_ponumber) IN ($pons) $filter
		")->fetchAll();
		
		if ($result) {
			foreach ($result as $row) {
				$pon = $row['mps_salesorder_ponumber'];
				$code = $row['code'];
				$data[$pon]['confirmed'][] = $row;
				$lastConfirmedQuantity[$pon][$code] = $row;
			}
		}
		

		// get shipped orders
		$result = $exchange->query("
			SELECT 
				TRIM(mps_salesorderitem_ponumber) AS mps_salesorderitem_ponumber,
				TRIM(mps_salesorderitem_linenumber) AS mps_salesorderitem_linenumber,
				TRIM(mps_salesorderitem_material_code) AS mps_salesorderitem_material_code,
				TRIM(ramco_shipping_date) AS ramco_shipping_date,
				TRIM(ramco_shipped_quantity) AS ramco_shipped_quantity,
				TRIM(ramco_order_state) AS ramco_order_state,
				TRIM(ramco_order_shortclosed) AS ramco_order_shortclosed,
				TRIM(mps_item_completely_imported) AS mps_item_completely_imported,
				LOWER(REPLACE(mps_salesorderitem_material_code, ' ', '')) AS code
			FROM ramco_shipped_items
			WHERE TRIM(mps_salesorderitem_ponumber) IN ($pons) $filter
		")->fetchAll();
		
		if ($result) {
			
			foreach ($result as $row) {
				$pon = $row['mps_salesorderitem_ponumber'];
				$data[$pon]['shipped'][] = $row;
				$code = $row['code'];
				$lastShippedQuantity[$pon][$code] = $row;
			}
		}
		
		$company = array(
			'mps' => 'tissot'
		);
		
		$result = $exchange->query("
			SELECT *,
				LOWER(REPLACE(mps_salesorderitem_material_code, ' ', '')) AS code
			FROM mps_salesorderitems
			INNER JOIN mps_salesorders ON mps_salesorder_id = mps_salesorderitem_salesorder_id
			WHERE 
				mps_salesorder_company = '".$company[$application]."'
		")->fetchAll();
		
		if ($result) {
			foreach ($result as $row) {
				$pon = $row['mps_salesorderitem_ponumber'];
				$data[$pon]['salesorder'][] = $row;
			}
		}
		
		
		
		$result = $model->query("
			select DISTINCT
				mps_ordersheet_items.mps_ordersheet_item_purchase_order_number as pon,
				CONCAT(db_retailnet.countries.country_name, ', ', db_retailnet.addresses.address_company, ', ', mps_workflow_state_name ) AS company
			from mps_ordersheet_items
			INNER JOIN mps_ordersheets ON mps_ordersheet_id = mps_ordersheet_item_ordersheet_id
			INNER JOIN mps_workflow_states ON mps_workflow_state_id = mps_ordersheet_workflowstate_id
			INNER JOIN db_retailnet.addresses ON address_id = mps_ordersheet_address_id
			INNER JOIN db_retailnet.countries ON country_id = address_country
			WHERE
				mps_ordersheet_item_quantity_approved > 0
				AND mps_ordersheet_item_purchase_order_number IS NOT NULL
			ORDER BY country_name, address_company		
		")->fetchAll();
		
		$companies = _array::extract($result);
	}
	
	
	
	
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Exchange Data Observer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="/public/scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../public/scripts/js/html5shiv.js"></script>
      <script src="../../public/scripts/js/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    
    	h1 {
    		margin: 50px 0 100px;
    	}
    	
    	h1 strong {
    		margin-left: 0px;
    		color: #999;
    	}
    
    	.row {
    		margin: 10px 0 80px;
    	}
    	
    	.tab-content {
    		border-width: 0 1px 1px;
    		border-style: solid;
    		border-color: silver;
    		padding: 20px;
    		display: block;
    	}
    	
    </style>
  </head>
  <body>
  	<div class="container">
 
  		
  		<h1><img src="/public/images/logo_small.jpg"> <strong>Exchange Data Observer</strong></h1>
  		
  		<?php 
  		
	  		if ($data) {
	  		
	  			foreach ($data as $pon => $sections) {
	  		
					echo "<div class='row'>";
					
						echo "<h4>".date('d.m.Y', $pon).", ".$companies[$pon]."</h4>";
					
						echo "<ul class='nav nav-tabs'>";
						
						echo "<li class='active'><a href='#items$pon' data-toggle='tab'>Items</a></li>";
						
						if ($sections['orders']) echo "<li><a href='#order$pon' data-toggle='tab'>Orders</a></li>";
						if ($sections['confirmed']) echo "<li><a href='#confirmed$pon' data-toggle='tab'>Confirmed Orders</a></li>";
						if ($sections['shipped']) echo "<li><a href='#shipped$pon' data-toggle='tab'>Shipped Orders</a></li>";
						
						echo "</ul>";
					
						echo "<div class='tab-content'>";
						
						
						
							// ITEMS
							echo "<div class='tab-pane active' id='items$pon'>";
							
								if ($sections['salesorder']) {
									
									echo "
										<table class='table table-condensed table-striped'>
									        <thead>
									          <tr>
									            <th>Order Number</th>
					 							<th>Confirmed State</th>
									            <th>Item</th>
									            <th>Approved</th>
									            <th>Confirmed</th>
									            <th>Shipped</th>
									            <th>Shortclosed</th>
									          </tr>
									        </thead>
									        <tbody>
									";
									
									foreach ($sections['salesorder'] as $row) {
										
										$code = $row['code'];
										
										$confirmedQuantity = $lastConfirmedQuantity[$pon][$code]['ramco_confirmed_quantity'];
										$confirmedState = $lastConfirmedQuantity[$pon][$code]['ramco_order_state'];
										
										$shippedQuantity = $lastShippedQuantity[$pon][$code]['ramco_shipped_quantity'];
										$shippedShortclosed = $lastShippedQuantity[$pon][$code]['ramco_order_shortclosed'];
										
										if ($lastShippedQuantity[$pon][$code]) {
											$class = ($shippedQuantity==0 && $lastShippedQuantity[$pon][$code]['ramco_order_shortclosed']==0) ? 'danger' : null;
										} else {
											$class = null;
										}

										echo "
											<tr class='$class'>
												<td>".$row['mps_salesorderitem_ponumber']."</td>
												<td>$confirmedState</td>
												<td>".$row['mps_salesorderitem_material_code']."</td>
												<td>".$row['mps_salesorderitem_quantity']."</td>
												<td>$confirmedQuantity</td>
												<td>$shippedQuantity</td>
												<td>$shippedShortclosed</td>
											</tr>
										";
									}
									
									echo "</tbody></table>";
								}
								else {
									echo "Items not found. Check this order.";
								}
							
							echo "</div>";
						
						
						
							// ORDERS
							if ($sections['orders']) {

								echo "<div class='tab-pane' id='order$pon'>";
								
								echo "
									<table class='table table-condensed table-striped'>
								        <thead>
								          <tr>
								            <th>Order Number</th>
								            <th>Customer Number</th>
								            <th>Shipto Number</th>
								            <th>Date</th>
								            <th>Imported</th>
								          </tr>
								        </thead>
								        <tbody>
								";
								
								foreach ($sections['orders'] as $row) {
									echo "
										<tr>
											<td>".$row['mps_salesorder_ponumber']."</td>
											<td>".$row['mps_salesorder_customernumber']."</td>
											<td>".$row['ramco_shipto_number']."</td>
											<td>".$row['ramco_order_confirmation_date']."</td>
											<td>".$row['mps_order_completely_imported']."</td>
										</tr>
									";
								}
								
								echo "</tbody></table>";
								echo "</div>";
							}
							
							
							// CONFIRMED ORDER
							if ($sections['confirmed']) {
									
								echo "<div class='tab-pane' id='confirmed$pon'>";
								
								echo "
									<table class='table table-condensed table-striped'>
								        <thead>
								          <tr>
								            <th>Order Number</th>
								            <th>Item</th>
								            <th>Quantity</th>
								            <th>Confirmed Date</th>
								            <th>State</th>
								            <th>Imported</th>
								          </tr>
								        </thead>
								        <tbody>
								";
							
								foreach ($sections['confirmed'] as $row) {
									echo "
										<tr>
											<td>".$row['mps_salesorder_ponumber']."</td>
											<td>".$row['mps_salesorderitem_material_code']."</td>
											<td>".$row['ramco_confirmed_quantity']."</td>
											<td>".$row['ramco_estimated_departure_date']."</td>
											<td>".$row['ramco_order_state']."</td>
											<td>".$row['mps_item_completely_imported']."</td>
										</tr>
									";
								}
							
								echo "</tbody></table>";
								echo "</div>";
							}

							
							
							// SHIPPED
							if ($sections['shipped']) {
							
								echo "<div class='tab-pane' id='shipped$pon'>";
								
								echo "
									<table class='table table-condensed table-striped'>
								        <thead>
								          <tr>
								            <th>Order Number</th>
								            <th>Item</th>
								            <th>Quantity</th>
								            <th>Shipping Date</th>
								            <th>State</th>
								            <th>Shortclosed</th>
								            <th>Imported</th>
								          </tr>
								        </thead>
								        <tbody>
								";
							
								foreach ($sections['shipped'] as $row) {
									echo "
										<tr>
											<td>".$row['mps_salesorderitem_ponumber']."</td>
											<td>".$row['mps_salesorderitem_material_code']."</td>
											<td>".$row['ramco_shipped_quantity']."</td>
											<td>".$row['ramco_shipping_date']."</td>
											<td>".$row['ramco_order_state']."</td>
											<td>".$row['ramco_order_shortclosed']."</td>
											<td>".$row['mps_item_completely_imported']."</td>
										</tr>
									";
								}
							
								echo "</tbody></table>";
								echo "</div>";
							}
						
						echo "</div>";

	  				echo "</div>";	
	  			}
	  		}
  		?>
 		
	</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/public/scripts/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
		$(document).ready(function() {

			if (location.hash !== ''){
		        $('a[href="' + location.hash + '"]').tab('show');
		    }
		    
		    $('a[data-toggle="tab"]').on('shown', function(e) {
		        location.hash = $(e.target).attr('href').substr(1);
		        $(this).focus();
		        return false;
		    });
		    
		    $(window).on("hashchange", function(){
		        $('a[href="' + location.hash + '"]').tab('show');
		    });
			
		});
    </script>
  </body>
</html>