<?php 
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// pos administrate permissions
$permission_edit = user::permission(Pos::PERMISSION_EDIT);
$permission_view = user::permission(Pos::PERMISSION_VIEW);
$permission_administrate = user::permission(Pos::PERMISSION_ADMINISTRATE);
$hasLimitedPermission = !$permission_edit && !$permission_view && !$permission_administrate ? true : false;

$model = new Model();

// framework application
$application = $_REQUEST['application'];
$controller = $_REQUEST['controller'];
$archived = $_REQUEST['archived'];
$action = $_REQUEST['action'];

// request
$_REQUEST = session::filter($application, "$controller.$archived.$action", true);

$state = $_REQUEST['state'];

// sql order
$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : 'sap_country_country_name, sap_imported_posaddress_name1';
$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";

// pager
$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
$offset = ($page-1) * $rowsPerPage;
	
// filter: full text search
if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search) {
	
	$keyword = $_REQUEST['search'];
	
	$filters['search'] = "(
		sap_imported_posaddress_sapnr LIKE \"%$keyword%\" 
		OR sap_imported_posaddress_name1 LIKE \"%$keyword%\"
		OR sap_imported_posaddress_name2 LIKE \"%$keyword%\"
		OR sap_imported_posaddress_address LIKE \"%$keyword%\"
		OR sap_imported_posaddress_zip LIKE \"%$keyword%\"
		OR sap_imported_posaddress_city LIKE \"%$keyword%\"
		OR sap_imported_posaddress_country LIKE \"%$keyword%\"
	)";
}

if ($_REQUEST['countries']) {
	$filters['countries'] = "sap_county_retailnet_country_id = ".$_REQUEST['countries'];
}

$filters['default'] = "(
	sap_imported_posaddress_date_checked IS NULL 
	OR sap_imported_posaddress_date_checked = ''
	OR sap_imported_posaddress_date_checked = '0000-00-00'
)";


// for limited view
if ($hasLimitedPermission) {

	// country access
	$countryAccess = User::getCountryAccess();
	$filterCountryAccess = $countryAccess ? " OR sap_county_retailnet_country_id IN ($countryAccess) " : null;

	// regional access
	$regionalAccessCountries = User::getRegionalAccessCountries();
	$regionalAccessCountries = $regionalAccessCountries ? join(',', $regionalAccessCountries) : null;
	$filterRegionalAccess = $regionalAccessCountries ? " OR sap_county_retailnet_country_id IN ($regionalAccessCountries) " : null;

	$company = new Company();
	$company->read($user->address);

	$filters['limited'] = "(
		sap_county_retailnet_country_id = $company->country
		$filterCountryAccess 
		$filterRegionalAccess
	)";
}


/*
// filter: limited


// country access filter
$accessCountries = array();

$result = $model->query("
	SELECT DISTINCT country_access_country
	FROM db_retailnet.country_access
	WHERE country_access_user = $user->id
")->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$accessCountries[] = $row['country_access_country'];
	}
}

// regional access countries
$regionalAccessCountries = User::getRegionalAccessCountries();

if ($regionalAccessCountries) {
	$accessCountries = array_merge($accessCountries, $regionalAccessCountries);
}

// filter acces countries
if ($accessCountries) {
	$accessCountriesFilter = join(',', array_unique($accessCountries));
}

// country access extended filter
if ($accessCountries) {

	$basicFiletrs = $filters;
	unset($basicFiletrs['limited']);

	if ($basicFiletrs) {
		$filter = join(' AND ', $basicFiletrs);
		$extendedFilter = " OR ($filter  AND sap_county_retailnet_country_id IN ($accessCountriesFilter) )";	
	} else {
		$extendedFilter = " OR sap_county_retailnet_country_id IN ($accessCountriesFilter)";
	}
}
*/


// datagrid
$model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		sap_imported_posaddress_id,
		sap_country_country_name,
		sap_imported_posaddress_sapnr,
		sap_imported_posaddress_name1,
		sap_imported_posaddress_city,
		sap_imported_posaddress_zip,
		REPLACE(LOWER(sap_imported_posaddress_distribution_channel), ' ', '') AS channel,
		DATE_FORMAT(sap_imported_posaddresses.date_created, '%d.%m.%Y') AS version
	FROM sap_imported_posaddresses
	LEFT JOIN sap_countries ON sap_country_sap_code = sap_imported_posaddress_country
")
->filter($filters)
->order($sort, $direction);

if (!$state) {
	$model->offset($offset, $rowsPerPage);
}

$result = $model->fetchAll();

if ($result) {

	$totalrows = $model->totalRows();
	$datagrid = _array::datagrid($result);
	
	// get pos sap numbers
	$posdata = $model->query("
		SELECT DISTINCT
			posaddress_id,
			posaddress_sapnumber,
			posaddress_zip,
			REPLACE(LOWER(mps_distchannel_code), ' ', '') AS channel
		FROM posaddresses
		LEFT JOIN mps_distchannels ON mps_distchannel_id = posaddress_distribution_channel
		WHERE posaddress_sapnumber > 0	
	")->fetchAll();
	
	if ($posdata) {
		foreach ($posdata as $row) {
			$pos = $row['posaddress_id'];
			$sap = $row['posaddress_sapnumber'];
			$zip = $row['posaddress_zip'];
			$k = str_replace(' ', '', strtolower("$sap.$zip"));
			$sapNumbers[$sap] = $row['posaddress_id'];
			$sapZipKey[$k] = $sap;
			
			if ($row['channel']) {
				//$posDistributionChannels[$pos] = str_replace('x', '', $row['channel']);
				$posDistributionChannels[$k] = str_replace('x', '', $row['channel']);
			}
		}
	}


	if ($datagrid) {

		foreach ($datagrid as $key => $row) {

			$sap = $row['sap_imported_posaddress_sapnr'];
			$zip = $row['sap_imported_posaddress_zip'];
			$sapChannel = str_replace('x', '', $row['channel']);

			$k = str_replace(' ', '', strtolower("$sap.$zip"));
			
			if ($sapNumbers[$sap]) $s = $sapZipKey[$k] ? "ok" : "maybe";
			else $s = "notok";

			$posID = $sapNumbers[$sap];
			//$mpsChannel = $posDistributionChannels[$posID];
			$mpsChannel = $posDistributionChannels[$k];
			
			if ($sapChannel && $sapChannel <> $mpsChannel) {
				$s = "notok";
			}

			if ($state && $state <> $s) {
				unset($datagrid[$key]);
			} else {

				$datagrid[$key]['ico'] = "<img src='/public/images/sap.import.$s.png' >";

				if ($s=='ok') {
					$hasMatchedPos = true;
					$datagrid[$key]['ign'] = "<span class='btn btn-ignore' data-id='$key' title='Ignore' ><span class='icon cancel'></span></span>";
				}
			}
		}
	}

	$pager = new Pager(array(
		'page' => $page,
		'rows' => $rowsPerPage,
		'totalrows' => $totalrows,
		'buttons' => true
	));

	if (!$state) {
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";

	// toolbox: add
	if ($hasMatchedPos && $permission_administrate) {
		$toolbox[] = ui::button(array(
			'id' => 'btn-ignore-matching',
			'icon' => 'cancel',
			'href' => '#',
			'label' => 'Ignore Matching'
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();


	// filter countries
	$extendedFilter = null;
	$basicFiletrs = $filters;
	unset($basicFiletrs['countries']);
	
	// country access filter
	if ($accessCountriesFilter) {
		
		$filter = $basicFiletrs;
		unset($filter['limited']);
		
		if ($filter) {
			$filter = join(' AND ', $filter);
			$extendedFilter = " OR ( $filter  AND sap_county_retailnet_country_id IN ($accessCountriesFilter) )";
		} else {
			$basicFiletrs['countries'] = "sap_county_retailnet_country_id IN ($accessCountriesFilter)";
		}
	}
	
	// toolbox: countries
	$result = $model->query("
		SELECT DISTINCT
			country_id, country_name
		FROM sap_imported_posaddresses
		INNER JOIN sap_countries ON sap_country_sap_code = sap_imported_posaddress_country
		INNER JOIN countries ON country_id = sap_county_retailnet_country_id 
	")
	->filter($basicFiletrs)
	->extend($extendedFilter)
	->order('country_name')
	->fetchAll();
	
	$toolbox[] = html::select($result, array(
		'name' => 'countries',
		'id' => 'countries',
		'class' => 'submit',
		'value' => $_REQUEST['countries'],
		'caption' => $translate->all_countries
	));

	$states = array(
		'ok' => 'Matching Data',
		'maybe' => 'Partially Matching Data',
		'notok' => 'Not Matching'
	);

	$toolbox[] = html::select($states, array(
		'name' => 'state',
		'id' => 'state',
		'class' => 'submit',
		'value' => $_REQUEST['state'],
		'caption' => "All States"
	));

	
	// toolbox: form
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>
		";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;

	$table->ico(
		Table::ATTRIBUTE_NOWRAP,
		'width=20px'
	);	

	if (_array::key_exists('ign', $datagrid)) {
		$table->ign(
			Table::ATTRIBUTE_NOWRAP,
			'width=20px'
		);	
	}

	$table->version(
		Table::ATTRIBUTE_NOWRAP,
		'width=10%'
	);
	
	$table->sap_country_country_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=20%'
	);
	
	$table->sap_imported_posaddress_sapnr(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);
	
	$table->sap_imported_posaddress_name1(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		"href=".$_REQUEST['data']
	);
	
	$table->sap_imported_posaddress_city(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=15%'
	);
	
	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	
	