<?php
	
require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$auth = User::instance();
$translate = Translate::instance();

$controller = $_REQUEST['controller'];
$id = $_REQUEST['user'];
$template = $_REQUEST['mail_template_id'];

$user = new User($id);
	

if ($user->id && $template) { 
	
	// set template as integer
	settype($template, "integer");
	
	// ajax responding
	$json['response'] = true;
	
	// action mail
	$actionmail = new ActionMail($template);

	$actionmail->sender($id);
	
	// set main parameters
	$actionmail->param()->id = $user->id;
	$actionmail->param()->cc = $_REQUEST['login_data_cc'];
	
	// mail subject
	if ($_REQUEST['mail_template_subject']) {
		$actionmail->content()->subject = $_REQUEST['mail_template_subject'];
	}
	
	// mail content
	if ($_REQUEST['mail_template_text']) {
		$actionmail->content()->body = $_REQUEST['mail_template_text'];
	}
	
	// mail content
	if ($_REQUEST['close_account_reason']) {
		$actionmail->content()->body .= "\r\n\nThe reaseon for closing the account is:";
		$actionmail->content()->body .= "\r\n\n".$_REQUEST['close_account_reason']."\r\n\n";
	}
	
	// send mails
	$actionmail->send();
	
	// console messages
	$json['console'] = $actionmail->response()->console;
	
	
	// close user account
	if ($actionmail->response()->sendmail) {
		
		// production mod
		if ($actionmail->settings()->devemail && $_REQUEST['close']) {
			
			$model = new Model(Connector::DB_CORE);
			
			// remove user roles
			$model->db->exec("
				DELETE FROM user_roles
				WHERE user_role_user = $user->id
			");
			
			// deactivate user account
			$user->update(array(
				'user_active' => '0',
				'user_modified' => $auth->login,
				'date_modified' => date('')
			));
			
			$message_key = 'message_request_close_account';
		}
		
		if ($_REQUEST['close']) {
			$message_key = 'message_request_close_account';
		} else {
			$message_key = ($template== 13) ? 'message_send_login_data' : 'message_request_open_account';
		}
		
		Message::add(array(
			'type' => 'message',
			'keyword' => $message_key,
			'life' => 5000
		));
		
		// reload page
		$json['reload'] = $_REQUEST['redirect'] ? false : true;
		$json['redirect'] = $_REQUEST['redirect'];
		
	} else {
		
		$json['notification'] = array(
			'content' => "ACTION MAIL: mail send failure. ",
			'properties' => array(
				'theme' => 'error'
			)
		);
	}

} else {

	if (!$_REQUEST['user']) {
		$message = "User not found.<br>";
	}

	if (!$_REQUEST['mail_template_id']) {
		$message .= "E-mail template not found.";
	}
	
	$json = array(
		'response' => false,
		'notification' => array(
			'content' => "Bad request. $message",
			'properties' => array(
				'theme' => 'error'
			)
		)
	);
}


if (!headers_sent()) {
	header('Content-Type: text/json');
}

echo json_encode($json);

