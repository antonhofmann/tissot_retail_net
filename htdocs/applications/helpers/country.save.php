<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	$id = $_REQUEST['country_id'];
	
	$data = array();
	
	if (in_array('country_code', $fields)) {
		$data['country_code'] = $_REQUEST['country_code'];
	}
	
	if (in_array('country_name', $fields)) {
		$data['country_name'] = $_REQUEST['country_name'];
	}
	
	if (in_array('country_hemisphere_id', $fields)) {
		$data['country_hemisphere_id'] = $_REQUEST['country_hemisphere_id'];
	}
	
	if (in_array('country_region', $fields)) {
		$data['country_region'] = $_REQUEST['country_region'];
	}
	
	if (in_array('country_salesregion', $fields)) {
		$data['country_salesregion'] = $_REQUEST['country_salesregion'];
	}
	
	if (in_array('country_currency', $fields)) {
		$data['country_currency'] = $_REQUEST['country_currency'];
	}
	
	if (in_array('country_store_locator_id', $fields)) {
		$data['country_store_locator_id'] = $_REQUEST['country_store_locator_id'];
	}
	
	if (in_array('country_provinces_complete', $fields)) {
		$data['country_provinces_complete'] = $_REQUEST['country_provinces_complete'] ? 1 : 0;
	}
	
	if (in_array('country_timeformat', $fields)) {
		$data['country_timeformat'] = $_REQUEST['country_timeformat'];
	}

	if (in_array('country_website_swatch', $fields)) {
		$data['country_website_swatch'] = $_REQUEST['country_website_swatch'];
	}
	
	if ($data) {
	
		$country = new Country();
		$country->read($id);
	
		if ($country->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $country->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $country->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	