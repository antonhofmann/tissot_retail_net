<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();
$translate = Translate::instance();

$region = $_REQUEST['region'];
$action = $_REQUEST['action'];

$db = Connector::get();

// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$response = array();
$error = false;

$item = new Item();
$item->read($_REQUEST['item']);

if (!Request::isAjax() || !$user->id || !$item->id || !$region) {
	
	$response['notifications'][] = array(
		'type' => 'error',
		'title' => 'Error',
		'text' => 'Bad request.'
	);

	goto BLOCK_RESPONSE;
}


// submitter :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

switch ($_REQUEST['section']) {
	
	case 'country':

		$country = $_REQUEST['country'];

		if ($action) {

			$sth = $db->prepare("
				INSERT INTO item_countries (
					item_country_item_id,
					item_country_country_id,
					user_created
				)
				VALUES (?,?,?)
			");

			$response['success'] = $sth->execute(array($item->id, $country, $user->login));

		} else {

			$sth = $db->prepare("
				DELETE FROM item_countries
				WHERE item_country_item_id = ? AND item_country_country_id = ?
			");

			$response['success'] = $sth->execute(array($item->id, $country));
		}

	break;

	case 'countries':

		$countries = $_REQUEST['countries'];

		// get region countries
		$sth = $db->prepare("
			SELECT country_id
			FROM countries
			WHERE country_region = ?
		");

		$sth->execute(array($region));
		$result = $sth->fetchAll();

		if ($result) {

			$sth = $db->prepare("
				DELETE FROM item_countries
				WHERE item_country_item_id = ? AND item_country_country_id = ?
			");

			foreach ($result as $row) {
				$sth->execute(array($item->id, $row['country_id']));
			}
		}

		if ($action && $countries) {

			$sth = $db->prepare("
				INSERT INTO item_countries (
					item_country_item_id,
					item_country_country_id,
					user_created
				)
				VALUES (?,?,?)
			");

			foreach ($countries as $country) {
				$sth->execute(array($item->id, $country, $user->login));
			}
		}

		$response['success'] = true;

	break;
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONSE:

ob_clean();
header('Content-Type: text/json');
echo json_encode($response);
