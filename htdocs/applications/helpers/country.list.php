<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$application = $_REQUEST['application']; 
	$controller = $_REQUEST['controller'];
	$archived = $_REQUEST['archived'];
	$action = $_REQUEST['action'];
	
	$_REQUEST = session::filter($application, "$controller.$archived.$action", true);
	
	$model = new Model(Connector::DB_CORE); 

	$sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : "country_name";
	$direction = ($_REQUEST['direction']) ? $_REQUEST['direction'] : "asc";
	
	// pager
	$page = ($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	$rowsPerPage =  user::rowsPerPage("$application.$controller.$action", $_REQUEST['rows_per_page']);
	$offset = ($page-1) * $rowsPerPage;
	
	
	if ($_REQUEST['regions']) {
		$filters['regions'] = "country_region = ".$_REQUEST['regions'];
	}
	
	if ($_REQUEST['salesregions']) {
		$filters['salesregions'] = "country_salesregion = ".$_REQUEST['salesregions'];
	}
	
	
	// filter: full text search
	if ($_REQUEST['search'] && $_REQUEST['search'] <> $translate->search ) {
		
		$keyword = $_REQUEST['search'];
	
		$filters['search'] = "(
			country_name LIKE \"%$keyword%\" 
			OR country_code LIKE \"%$keyword%\" 
			OR country_phone_prefix LIKE \"%$keyword%\" 
			OR region_name LIKE \"%$keyword%\"
			OR currency_symbol LIKE \"%$keyword%\"
			OR country_timeformat LIKE \"%$keyword%\"
			OR address_sapnr LIKE \"%$keyword%\"
			OR address_sap_salesorganization LIKE \"%$keyword%\"
		)";
	}
	
	$result = $model->query("
		SELECT SQL_CALC_FOUND_ROWS DISTINCT
			country_id, 
			country_code, 
			country_name, 
			region_name,
			salesregion_name, 
			currency_symbol, 
			CONCAT(country_timeformat, ' hours') as country_timeformat,
			country_website_swatch
		FROM countries 
		LEFT JOIN salesregions ON country_salesregion = salesregion_id 
		LEFT JOIN regions ON country_region = region_id 
		LEFT JOIN currencies ON currency_id = country_currency 
	")
	->filter($filters)
	->order($sort, $direction)
	->offset($offset, $rowsPerPage)
	->fetchAll();

	if ($result) {
		
		$totalrows = $model->totalRows();
		$datagrid = _array::datagrid($result);
		
		$pager = new Pager(array(
			'page' => $page,
			'rows' => $rowsPerPage,
			'totalrows' => $totalrows,
			'buttons' => true
		));
		
		$list_index = $pager->index();
		$list_controlls = $pager->controlls();
	}
	
	// toolbox: hidden utilities
	$toolbox[] = "<input type=hidden id=page name=page value='$page' class='submit' />";
	$toolbox[] = "<input type=hidden id=sort name=sort value='$sort' class='submit' />";
	$toolbox[] = "<input type=hidden id=direction name=direction value='$direction' class='submit' />";
	$toolbox[] = "<input type=hidden id=rows_per_page name=rows_per_page value='$rowsPerPage' class='submit' />";
		
	// toolbox: add
	if ($_REQUEST['add'])  {
		$toolbox[] = ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $_REQUEST['add'],
			'label' => $translate->add_new
		));
	}
	
	// toolbox: serach full text
	$toolbox[] = ui::searchbox();
	
	// toolbox: region
	$result = $model->query("
		SELECT DISTINCT
			region_id,
			region_name
		FROM countries
		INNER JOIN regions ON country_region = region_id 
		ORDER BY region_name
	")->fetchAll();
	
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'regions',
		'id' => 'regions',
		'class' => 'submit',
		'value' => $_REQUEST['regions'],
		'caption' => $translate->all_regions
	));
	
	// toolbox: countries
	$result = $model->query("
		SELECT DISTINCT
			salesregion_id,
			salesregion_name
		FROM countries
		INNER JOIN salesregions ON country_salesregion = salesregion_id
		ORDER BY salesregion_name
	")->fetchAll();
	
	$toolbox[] = ui::dropdown($result, array(
		'name' => 'salesregions',
		'id' => 'salesregions',
		'class' => 'submit',
		'value' => $_REQUEST['salesregions'],
		'caption' => $translate->all_salesregions
	));
	
	// toolbox: button print
	if ($datagrid && $_REQUEST['print']) {
		$toolbox[] = ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'href' => $_REQUEST['print'],
			'label' => $translate->print
		));
	}
	
	if ($toolbox) {
		$toolbox = "
			<div class='table-toolbox'>
				<form class=toolbox method=post>".join($toolbox)."</form>
			</div>";
	}
	
	$table = new Table(array(
		'sort' => array('column'=>$sort, 'direction'=>$direction)
	));
	
	$table->datagrid = $datagrid;
	
	$table->country_code(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=2%'
	);
	
	$table->country_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		Table::DATA_TYPE_LINK,
		"href=".$_REQUEST['data']
	);
	
	$table->region_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=20%'
	);

	$table->salesregion_name(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=30%'
	);

	$table->currency_symbol(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=5%'
	);

	$table->country_timeformat(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);


	$table->country_website_swatch(
		Table::ATTRIBUTE_NOWRAP,
		Table::PARAM_SORT,
		'width=10%'
	);

	

	$table->footer($list_index);
	$table->footer($list_controlls);
	$table = $table->render();
	
	echo $toolbox.$table;
	