<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

// pos administrate permissions
$permission_edit = user::permission(Pos::PERMISSION_EDIT);
$permission_view = user::permission(Pos::PERMISSION_VIEW);
$permission_administrate = user::permission(Pos::PERMISSION_ADMINISTRATE);
$hasLimitedPermission = !$permission_edit && !$permission_view && !$permission_administrate ? true : false;

$_APPLICATION = $_REQUEST['application'];
$_COUNTRY = trim($_REQUEST['countries']);
$_SEARCH = $_REQUEST['search'] && $_REQUEST['search']<>$translate->search ? trim($_REQUEST['search']) : null;

$model = new Model();

// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$permission_administrate) {
	$_ERRORS[] = "You don't have permission to execute this action";
	goto BLOCK_RESPONDING;
}


// filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_SEARCH) {
	$_FILTERS['search'] = "(
		sap_imported_posaddress_sapnr LIKE '%$_SEARCH%' 
		OR sap_imported_posaddress_name1 LIKE '%$_SEARCH%'
		OR sap_imported_posaddress_name2 LIKE '%$_SEARCH%'
		OR sap_imported_posaddress_address LIKE '%$_SEARCH%'
		OR sap_imported_posaddress_zip LIKE '%$_SEARCH%'
		OR sap_imported_posaddress_city LIKE '%$_SEARCH%'
		OR sap_imported_posaddress_country LIKE '%$_SEARCH%'
	)";
}

if ($_COUNTRY) {
	$_FILTERS['countries'] = "sap_county_retailnet_country_id = $_COUNTRY";
}

if ($hasLimitedPermission) {

	// country access
	$countryAccess = User::getCountryAccess();
	$filterCountryAccess = $countryAccess ? " OR sap_county_retailnet_country_id IN ($countryAccess) " : null;

	// regional access
	$regionalAccessCountries = User::getRegionalAccessCountries();
	$regionalAccessCountries = $regionalAccessCountries ? join(',', $regionalAccessCountries) : null;
	$filterRegionalAccess = $regionalAccessCountries ? " OR sap_county_retailnet_country_id IN ($regionalAccessCountries) " : null;

	$company = new Company();
	$company->read($user->address);

	$_FILTERS['limited'] = "(
		sap_county_retailnet_country_id = $company->country
		$filterCountryAccess 
		$filterRegionalAccess
	)";
}

$_FILTERS = $_FILTERS ? ' AND '.join(' AND  ', $_FILTERS) : null;


// dataloader ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$result = $model->query("
	SELECT DISTINCT 
		sap_imported_posaddress_id,
		sap_imported_posaddress_sapnr,
		sap_imported_posaddress_zip,
		REPLACE(LOWER(sap_imported_posaddress_distribution_channel), ' ', '') AS channel
	FROM sap_imported_posaddresses
	LEFT JOIN sap_countries ON sap_country_sap_code = sap_imported_posaddress_country
	WHERE (
		sap_imported_posaddress_date_checked IS NULL 
		OR sap_imported_posaddress_date_checked = ''
		OR sap_imported_posaddress_date_checked = '0000-00-00'
	)
	$_FILTERS
")->fetchAll();

if (!$result) {
	$_ERRORS[] = "No records found.";
	goto BLOCK_RESPONDING;
}
	
// get pos sap numbers
$posdata = $model->query("
	SELECT DISTINCT
		posaddress_id,
		posaddress_sapnumber,
		posaddress_zip,
		REPLACE(LOWER(mps_distchannel_code), ' ', '') AS channel
	FROM posaddresses
	LEFT JOIN mps_distchannels ON mps_distchannel_id = posaddress_distribution_channel
	WHERE posaddress_sapnumber > 0	
")->fetchAll();

if ($posdata) {
	
	foreach ($posdata as $row) {
		
		$pos = $row['posaddress_id'];
		$sap = $row['posaddress_sapnumber'];
		$zip = $row['posaddress_zip'];
		$k = str_replace(' ', '', strtolower("$sap.$zip"));
		
		$sapNumbers[$sap] = $row['posaddress_id'];
		$sapZipKey[$k] = $sap;
		
		if ($row['channel']) {
			$posDistributionChannels[$pos] = str_replace('x', '', $row['channel']);
		}
	}
}


// submitting ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_DATAGRID = array();

foreach ($result as $row) {

	$key = $row['sap_imported_posaddress_id'];
	$sap = $row['sap_imported_posaddress_sapnr'];
	$zip = $row['sap_imported_posaddress_zip'];
	
	$sapChannel = str_replace('x', '', $row['channel']);
	
	if ($sapNumbers[$sap]) {
		$k = str_replace(' ', '', strtolower("$sap.$zip"));
		$s = $sapZipKey[$k] ? "ok" : "maybe";
	} else {
		$s = "notok";
	}

	$posID = $sapNumbers[$sap];
	$mpsChannel = $posDistributionChannels[$posID];
	
	if ($sapChannel && $sapChannel <> $mpsChannel) {
		$s = "notok";
	}

	if ($s=='ok') {
		$_DATAGRID[$key] = $key;
	}
}

if ($_DATAGRID) {

	$sth = $model->db->prepare("
		UPDATE sap_imported_posaddresses SET
			sap_imported_posaddress_date_checked = CURDATE(),
			user_modified = ?,
			date_modified = NOW()
		WHERE sap_imported_posaddress_id = ?
	");

	foreach ($_DATAGRID as $key => $value) {
		$sth->execute(array($user->login, $key));
	}

	$_SUCCESS = true;
	$_MESSAGE = $translate->message_request_updated;
}


// responding ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

BLOCK_RESPONDING: 

header('Content-Type: text/json');
echo json_encode(array(
	'success' => $_SUCCESS,
	'message' => $_MESSAGE,
	'datagrid' => $_DATAGRID
));
