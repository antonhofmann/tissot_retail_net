<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$filetype = $_REQUEST['filetype'];
	$application = $_REQUEST['application'];
	
	if ($filetype && $application) {
		
		$model = new Model(Connector::DB_CORE);
		
		if ($_REQUEST['action']) {
			
			$response = $model->db->query("
				INSERT INTO application_filetypes (
					application_filetype_filetype, 
					application_filetype_application, 
					user_created, 
					date_created
				)	
				VALUES ($filetype, $application, '".$user->login."', NOW() )	
			");
			
			$message = ($response) ? $translate->message_request_inserted : $translate->message_request_failure;
		}
		else {
			
			$response = $model->db->query("
				DELETE FROM application_filetypes
				WHERE application_filetype_filetype = $filetype AND application_filetype_application = $application
			");
			
			$message = ($response) ? $translate->message_request_deleted : $translate->message_request_failure;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message
	));