<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	switch ($_REQUEST['section']) {
		
		case 'hasIdentical':
		
			if ($_REQUEST['franchisee']) {
				
				$model = new Model($_REQUEST['application']);
				
				$result = $model->query("
					SELECT posaddress_id
					FROM db_retailnet.posaddresses"
				)
				->filter('pos', 'posaddress_franchisee_id = '.$_REQUEST['franchisee'])
				->filter('default', 'posaddress_identical_to_address = 1')
				->fetch();
				
				if ($result) {
					$json['response'] = ($result['posaddress_id'] == $_REQUEST['pos']) ? true : false;
					$json['checked'] = ($json['response']) ? true : false;
				} else {
					$json['response'] = true;
					$json['checked'] = false;
				}
			}
				
		break;
		
		case 'getcompany':
		
			if ($_REQUEST['franchisee']) {
				
				$company = new Company();
				$company->read($_REQUEST['franchisee']);
				
				if ($_REQUEST['id']) {
					$pos = new Pos();
					$pos->read($_REQUEST['id']);
					$json['posaddress_name'] = $pos->name;
					$json['posaddress_name2'] = $pos->name2;
				} else {
					$json['posaddress_name'] = $company->company;
					$json['posaddress_name2'] = $company->company2;
				}
				
				$json['posaddress_address'] = $company->address;
				$json['posaddress_street'] = $company->street;
				$json['posaddress_street_number'] = $company->streetnumber;
				$json['posaddress_address2'] = $company->address2;
				$json['posaddress_country'] = $company->country;
				$json['posaddress_province_id'] = $company->province_id;
				$json['posaddress_place_id'] = $company->place_id;
				$json['posaddress_zip'] = $company->zip;
				$json['posaddress_phone'] = $company->phone;
				$json['posaddress_phone_country'] = $company->phone_country;
				$json['posaddress_phone_area'] = $company->phone_area;
				$json['posaddress_phone_number'] = $company->phone_number;
				$json['posaddress_mobile_phone'] = $company->mobile_phone;
				$json['posaddress_mobile_phone_country'] = $company->mobile_phone_country;
				$json['posaddress_mobile_phone_area'] = $company->mobile_phone_area;
				$json['posaddress_mobile_phone_number'] = $company->mobile_phone_number;
				$json['posaddress_email'] = $company->email;
				$json['posaddress_website'] = $company->website;
				$json['posaddress_contact_name'] = $company->contact_name;
				$json['posaddress_contact_email'] =  $company->contact_email;
				
			}
				
		break;
		
		case 'getpos':
		
			$pos = new Pos();
			$pos->read($_REQUEST['id']);
			
			$json['posaddress_name'] = $pos->name;
			$json['posaddress_name2'] = $pos->name2;
			$json['posaddress_address'] = $pos->address;
			$json['posaddress_street'] = $pos->street;
			$json['posaddress_street_number'] = $pos->street_number;
			$json['posaddress_address2'] = $pos->address2;
			$json['posaddress_country'] = $pos->country;
			$json['posaddress_province_id'] = $pos->province_id;
			$json['posaddress_place_id'] = $pos->place_id;
			$json['posaddress_zip'] = $pos->zip;
			$json['posaddress_phone'] = $pos->phone;
			$json['posaddress_phone_country'] = $pos->phone_country;
			$json['posaddress_phone_area'] = $pos->phone_area;
			$json['posaddress_phone_number'] = $pos->phone_number;
			$json['posaddress_mobile_phone'] = $pos->mobile_phone;
			$json['posaddress_mobile_phone_country'] = $pos->mobile_phone_country;
			$json['posaddress_mobile_phone_area'] = $pos->mobile_phone_area;
			$json['posaddress_mobile_phone_number'] = $pos->mobile_phone_number;
			$json['posaddress_email'] = $pos->email;
			$json['posaddress_website'] = $pos->website;
			$json['posaddress_contact_name'] = $pos->contact_name;
			$json['posaddress_contact_email'] =  $pos->contact_email;
				
		break;
		
		case 'pos':
		
			$pos = new Pos();
			$pos->read($_REQUEST['id']);
			$json = $pos->data;
				
		break;
		
		case 'letter_cases':
			
			$value = $_REQUEST['value'];
		
			if(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $value)) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $value)) && strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $value)) > 10 ) {
				$error = true;
				$message = $translate->error_letter_cases(array(
					'modul' => 'POS'
				));
			} else {
				$error = false;
			}
		
			$json = array(
				'response' => true,
				'error' => $error,
				'message' => $message
			);
		
		break;
	}
	
	header('Content-Type: text/json');
	echo json_encode($json);
	