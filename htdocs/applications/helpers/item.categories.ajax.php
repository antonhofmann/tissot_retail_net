<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();

$response = array();

switch ($_REQUEST['section']) {

	case 'order':

		if ($_REQUEST['orders']) {

			$model = new Model();

			$sth = $model->db->prepare("
				UPDATE item_categories SET 
					item_category_sortorder = :order
				WHERE item_category_id = :id
			");

			$order=1;
			$orders = explode(',',$_REQUEST['orders']);
			
			foreach ($orders as $id) {
				$sth->execute(array(
					'order' => $order,
					'id' => $id
				));
				$order++;
			}

			$response['success'] = true;
			$response['message'] = $translate->message_request_updated;
		} 
		else {
			$response['message'] = $translate->message_request_failure;
		}

	break;
}

// response with json header
header('Content-Type: text/json');
echo json_encode($response);