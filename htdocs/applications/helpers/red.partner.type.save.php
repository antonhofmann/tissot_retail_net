<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();
	
	$application = $_REQUEST['application'];
	$id = $_REQUEST['red_partnertype_id'];

	$data = array();

	if ($_REQUEST['red_partnertype_name']) {
		$data['red_partnertype_name'] = $_REQUEST['red_partnertype_name'];
	}

	if ($data) {
		
		$partnerType = new Red_Partner_Type($application);
		$partnerType->read($id);
		
		if ($partnerType->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $partnerType->update($data);
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $partnerType->create($data);
			$message = ($response) ? message::request_inserted() : $translate->message_request_failure;
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}

	echo json_encode(array(
		'response' => $response,
		'redirect' => $redirect,
		'message' => $message
	));
