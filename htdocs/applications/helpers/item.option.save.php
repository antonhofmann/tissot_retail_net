<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	// disabled fields
	$fields = ($_REQUEST['fields']) ? explode(',', $_REQUEST['fields']) : array();

	// url request
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];
	$action = $_REQUEST['action'];
	
	// product line
	$id = $_REQUEST['item_option_id'];
	
	$data = array();	
	
	if (in_array('item_option_parent', $fields)) {
		$data['item_option_parent'] = $_REQUEST['item_option_parent'];
	}	
	
	if (in_array('item_option_child', $fields)) {
		$data['item_option_child'] = $_REQUEST['item_option_child'];
	}
		
	if (in_array('item_option_name', $fields)) {
		$data['item_option_name'] = $_REQUEST['item_option_name'];
	}
			
	if (in_array('item_option_description', $fields)) {
		$data['item_option_description'] = $_REQUEST['item_option_description'];
	}	
			
	if (in_array('item_option_quantity', $fields)) {
		$data['item_option_quantity'] = $_REQUEST['item_option_quantity'];
	}		

	if (in_array('item_option_configcode', $fields)) {
		$data['item_option_configcode'] = $_REQUEST['item_option_configcode'];
	}	
	
	if ($data) {
	
		$itemOption = new Item_Option();
		$itemOption->read($id);
	
		if ($itemOption->id) {
			
			$data['user_modified'] = $user->login;
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			$response = $itemOption->update($data);
			
			$message = ($response) ? $translate->message_request_updated : $translate->message_request_failure;
		}
		else {
			
			$data['user_created'] = $user->login;
			$data['date_created'] = date('Y-m-d H:i:s');
			
			$response = $id = $itemOption->create($data);
			
			$message = ($response) ? Message::request_inserted() : $translate->message_request_failure;
			
			$redirect = ($_REQUEST['redirect']) ? $_REQUEST['redirect']."/$id" : null;
		}
	}
	else {
		$response = false;
		$message = $translate->message_request_failure;
	}
	
	echo json_encode(array(
		'response' => $response,
		'message' => $message,
		'redirect' => $redirect
	));
	