<style type="text/css">
	
	#sapForm {
		width: 960px;
	}
	
	.header-caption {
		width: 100%;
		display: table;
		background-color: #efefef;
		border-bottom: 1px solid silver;
	}
	
	.header-caption strong {
		width: 50%;
		display: table-cell;
		font-size: 14px;
		text-align: center;
		padding: 10px;
	}
	
	.header-caption strong:first-child {
		border-right: 1px solid silver;
	}
	
	form.default .-element {
		margin: 0 20px;
	}
	
	form.default .pos-row .-element {
		margin: 0 !important; 
		padding: 0 20px;
		width: 100%;
		text-align: right;
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
	}

	form.default .-element {
		vertical-align: top;
	}
	
	form.default input[type=text] {
		width: 428px;
	}

	.transfer {
		margin: 0 10px;
		cursor: pointer;
	}
	
	.transfer-disabled {
		display: inline-block;
		width: 14px;
		margin: 0 10px;
		vertical-align: top;
	}
	
	.transfer:hover {
		color: #0000ff;
	}
	
	.transfer.disabled,
	.transfer.disabled:hover {
		color: #eee;
		cursor: none;
	}
	
	#cancel {
		float: none;
	}

	.input-data-placeholder {
		display: inline-block;
		width: 420px;
		height: 24px;
		line-height: 24px;
		font-size: 12px;
		background-color: #f3f3f3;
		padding: 0 10px;
		vertical-align: top;
	}

	.input-data-placeholder i {
		font-style: normal;
		color: #999;
	}

	::-webkit-input-placeholder {
	   color: #999;
	}

	:-moz-placeholder { 
	   color: #999;  
	}

	::-moz-placeholder {
	   color: #999;  
	}

	:-ms-input-placeholder {  
	   color: #999;  
	}
	
	input.success { background-color: #e3f3fc !important }
	input.danger { background-color: #f7dede !important }	
	.input-data-placeholder.success { color: #000 !important }
	.input-data-placeholder.danger { color: #8a0e1f !important }

	#sapForm select {
		height: 24px;
		vertical-align: top;
	}

	#sapForm label {	
		width: auto !important;
	}

	.posaddress_phone, .posaddress_address { 
		position: relative; 
	}
	
	.posaddress_address .-element:not(.-disabled),
	.posaddress_phone .-element:not(.-disabled) { 
		display: none !important;
	}

	.input-mask-container { display: inline-block; margin-left: -5px; }
	.input-mask-container input {width: auto;}
	.phone-mask:nth-child(1) { width: 45px!important; }
	.phone-mask:nth-child(2) { width: 40px!important; }
	.phone-mask:nth-child(3) { width: 305px!important; }
	.address-mask:nth-child(1) { width: 350px!important; }
	.address-mask:nth-child(2) { width: 60px!important; }
	
</style>
<script type="text/javascript" src="/public/js/pos.sap.import.js"></script>
<form id="sapForm" action="/applications/helpers/pos.sap.import.update.php" method="post" class="default">
	<input type="hidden" name="id" id="id" value="<?php echo $id ?>" >
	<input type="hidden" name="import_id" id="import_id" value="<?php echo $import_id ?>" >
	<input type="hidden" name="import_country" id="import_country" value="<?php echo $country ?>" >
	<div class="-fieldset translation">
		<div class="header-caption">
			<strong>SAP</strong>
			<strong>RETAILNET</strong>
		</div>
		<div class="-row pos-row"> 
			<div class="-element -text">
				<?php 
					echo $data['owner']; 
					echo "<label>POS Location:</label>".$dataloader['posaddresses'];
				?>
			</div>   
		</div>
		<?php
			if ($fields) {
				foreach ($fields as $field => $row) {
					
					echo "<div class='-row'><div class='-element -text'>";

					echo $row['source'];
					
					if ($field=='sap_imported_posaddress_phone') {
						echo "<i class='transfer-disabled'>&nbsp;</i>";
						echo "
							<div class='input-mask-container phone-container'>
								<input type='text' value='".$posData['posaddress_phone_country']."' class='input-mask phone-mask' name='posaddress_phone_country' maxlength='6' placeholder='Country code'>
								<input type='text' value='".$posData['posaddress_phone_area']."' class='input-mask phone-mask' name='posaddress_phone_area'  maxlength='6' placeholder='Area'>
								<input type='text' value='".$posData['posaddress_phone_number']."' class='input-mask phone-mask' name='posaddress_phone_number'  maxlength='24' placeholder='Phone number'>
							</div>
						";
					} 
					elseif($field=='sap_imported_posaddress_address') {
						echo "<i class='transfer-disabled'>&nbsp;</i>";
						echo "
							<div class='input-mask-container address-container'>
								<input type='text' value='".$posData['posaddress_street']."' class='input-mask address-mask' name='posaddress_street' id='posaddress_street' placeholder='Street'>
								<input type='text' value='".$posData['posaddress_street_number']."' class='input-mask address-mask' name='posaddress_street_number' id='posaddress_street_number'  placeholder='Number'>
							</div>
						";
					}
					else {
						echo $row['transferer'];
						echo $row['target'];
					}
					
					echo "</div></div>";
				}
			}
		?>
	</div>
	<div class="actions">
		<a id="back" href="<?php echo $buttons['back']?>" class="button">
			<span class="icon back"></span>
			<span class="label">Back</span>
		</a>

		<?php if ($buttons['ignore']) : ?>
		<span id="ignore" class="button">
			<span class="icon cancel"></span>
			<span class="label">Ignore</span>
		</span>
		<?php endif; ?>

		<?php if ($buttons['update']) : ?>
		<span id="update" class="button">
			<span class="icon save"></span>
			<span class="label">Update</span>
		</span>
		<?php endif; ?>
	</div>
</form>

<?php 
	
	echo ui::dialogbox(array(
		'id' => 'addNewPlace',
		'title' => 'Add New Place',
		'content' => "<form class='default' ><select id='provinces' >$provinces</select><br /><br /><input type='text' id='new_place' placeholder='New Place Name' ></form>",
		'buttons' => array(
			'placeCancel' => array(
				'icon' => 'cancel',
				'label' => translate::instance()->cancel
			),
			'placeApply' => array(
				'icon' => 'apply',
				'label' => translate::instance()->add
			)
		)
	));
?>
