<?php 

	$settings = Settings::init();
	$translate = Translate::instance();
	$request = request::instance();
	
	$data['file'] = '
		<span id="showfile" class="button">
			<span class="icon download"></span>
			<span class="label">'.$translate->download_file.'</span>	
		</span>
		<span id="upload" class="button upl">
			<span class="icon upload"></span>
			<span class="label">'.$translate->upload_file.'</span>	
		</span>
		<span class="highlight" ></span>
		<input type=hidden name="mps_announcement_file" id="mps_announcement_file" value="'.$data['mps_announcement_file'].'" />
		<input type="hidden" name="has_upload" id="has_upload" />
	';
	
	$form = new Form(array(
		'id' => 'announcementform',
		'action' => '/applications/helpers/announcement.save.php',
		'method' => 'post'
	));
	
	$form->mps_announcement_id(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_announcement_date(
		Form::TYPE_TEXT, 
		Form::TOOLTIP_DATE,
		Validata::PARAM_REQUIRED,
		Validata::PARAM_DATE,
		"class=datepicker"
	);
	
	$form->mps_announcement_expiry_date(
		Form::TYPE_TEXT, 
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		"class=datepicker"
	);
	
	$form->mps_announcement_title(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_announcement_important(
		Form::TYPE_CHECKBOX
	);
	
	$form->mps_announcement_text(
		Form::TYPE_TEXTAREA,
		Validata::PARAM_REQUIRED,
		'class=tinymce'
	);
	
	$form->mps_announcement_file_title(
		Form::TYPE_TEXT
	);
	
	$form->file(
		Form::TYPE_AJAX,
		FORM::PARAM_SHOW_VALUE
	);

	$form->fieldset('announcement', array(
		'mps_announcement_date',
		'mps_announcement_expiry_date',
		'mps_announcement_title',
		'mps_announcement_important'
	));
	
	$form->fieldset('file', array(
		'mps_announcement_file_title',
		'file'
	));

	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete_announcement',
			'class' => 'dialog-trigger',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form = $form->render();
?>
<script type="text/javascript" src="/public/scripts/tinymce4/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/public/js/announcement.js"></script>
<style type="text/css">

	#announcementform {
		width: 700px;
	}
	#mps_announcement_text {
		width: 690px;
		height: 200px;
	}
	
	form .mps_announcement_date input,
	form .mps_announcement_expiry_date input {
		width:150px;
	}
	
	.error  {
		border: 1px solid red !important;
	}
	
	img.icon-ajax-loader {
		margin-left: 10px;
		position: relative;
		top: 5px;	
	}
	
</style>
<?php 
	
	echo $form;
	 
	echo ui::dialogbox(array(
		'id' => 'delete_announcement_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>