<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#collection_category_files').tableLoader({
		url: '/applications/helpers/material.collection.category.file.list.php',
		data: $('.request').serializeArray()
	});	

	$(document).delegate(".modal", "click", function() {
		
		var path = $(this).attr('tag');
		var title = $(this).parent().next().find('a').text();
		title = (title) ? title : '';
	
		if (isImage(path)) {
			retailnet.modal.show(path, {title: title});
		} else {
			window.open('http://'+location.host+path);
		}
	});
	
});
</script>
<style type="text/css">
	
	#collection_category_files, .-actions { 
		width: 700px; 
	}
	
	td span {
		display:block; 
		color:gray; 
		font-size:11px;
	}
	
	td.fileicon {width: 32px;}
	td.date {width: 100px;}
	
	h6 { 
		display:block; 
		margin: 40px 0 5px; 
		font-size: .825em;
	}

	table { 
		margin-bottom:20px; 
		border-style: solid; 
		border-color: silver; 
	}
	
	.file-extension {cursor: pointer;}
	span.order_title {color: black;}
	span.filename {display:block; font-weight:600; color:#0050CC; cursor:pointer; font-size: 11px;}
	span.filename:hover { color: black; }
	
</style>
<div id="collection_category_files"></div>
<div class='actions'>
<?php 

	echo $request->form($requestFields);
	
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>