<?php 
	
	$user = user::instance();
	$translate = Translate::instance();
	$request = request::instance();
	
	$idea = (user::permission(Idea::PERMISSION_NEW)) 
		? "<li><a href='/public/themes/default/idea.html' data-fancybox-type='iframe' id='btnIdea' ><i class='fa fa-comment'></i> $translate->idea</a></li>"
		: null; 

	if ($user->id) :  ?>
		<div class="usermenu">
			<ul class="navigation">
				<li class=nogo ><span class="icon icon4"></span><span class=label ><?php echo $translate->welcome.' '.$user->firstname; ?></span></li>
				<?php echo $idea; ?>
				<li><a href="/mycompany" ><span class="icon icon97"></span><span class=label ><?php echo $translate->my_company; ?></span></a></li>
				<li><a href="/security/logout"><span class="icon icon151"></span><span class=label ><?php echo $translate->logout; ?></span></a></li>
			</ul>
		</div>
<?php endif; ?>