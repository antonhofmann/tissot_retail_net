<div class='adomat modalbox-container'>
	<div id="<?php echo $id; ?>" class="modalbox <?php echo $class; ?>" >
		<div class=modalbox-header >
			<div class='title'><?php echo $title; ?></div>
			<div class='subtitle'><?php echo $subtitle; ?></div>
			<div class='modal-infotex'><?php echo $infotext; ?></div>
		</div>
		<div class=modalbox-content-container >
			<div class=modalbox-content><?php echo $content; ?></div>
		</div>
		<?php if (is_array($buttons)) : ?>
		<div class=modalbox-footer >
			<div class=modalbox-actions ><?php echo join($buttons); ?></div>
		</div>
		<?php endif; ?>
	</div>
</div>