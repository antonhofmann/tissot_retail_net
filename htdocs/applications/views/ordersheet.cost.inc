<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();
	
	$model = new Model($request->application);
	
	// ordersheet
	$ordersheet = new Ordersheet($request->application);
	$ordersheet->read($id);
	
	// get sheet collections
	$result = $model->query("
		SELECT  
			mps_ordersheet_item_id,
			mps_ordersheet_item_price,
			mps_ordersheet_item_quantity,
			(mps_ordersheet_item_price * mps_ordersheet_item_quantity) AS quantity, 
			(mps_ordersheet_item_price * mps_ordersheet_item_quantity_approved) AS quantity_approved,
			mps_material_planning_type_id AS planning, 
			mps_material_planning_type_name AS planning_name, 
			mps_material_collection_category_id AS category,
			mps_material_collection_category_code AS category_name 
		FROM mps_ordersheet_items
	")
	->bind(Ordersheet_Item::DB_BIND_MATERIALS)
	->bind(Material::DB_BIND_PLANNING_TYPES)
	->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
	->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = $id")
	->order('planning_name')
	->order('category_name')
	->fetchAll();

	if ($result) { 
	
		$datagrid = array();
		
		// group data
		foreach ($result as $row) {
			$planning = $row['planning'];
			$category = $row['category'];
			$item = $row['mps_ordersheet_item_id'];
			$datagrid[$planning]['caption'] = $row['planning_name'];
			$datagrid[$planning]['data'][$category]['caption'] = $row['category_name'];
			$datagrid[$planning]['data'][$category]['data'][$item]['quantity'] = $row['quantity'];
			$datagrid[$planning]['data'][$category]['data'][$item]['quantity_approved'] = $row['quantity_approved'];
		}
		
		// list
		$list = "
			<table class=listing ><thead>
				<tr>
					<td colspan=2 >&nbsp;</td>
					<td class=number >".$translate->quantity."</td>
					<td class=number >".$translate->approved."</td>
				</tr>
			</thead><tbody>
		";

		// datagrid
		foreach ($datagrid as $planning => $row) {
			
			$planning_quantity = 0;
			$planning_quantity_approved = 0;
			
			$list .= "<tr class=planning ><td colspan=4 >".$row['caption']."</td></tr>";

		
			foreach ($row['data'] as $category => $subrow) {
				
				$category_quantity = 0;
				$category_quantity_approved = 0;
				

				foreach ($subrow['data'] as $item => $value) {
					$category_quantity = $category_quantity + $value['quantity'];
					$category_quantity_approved = $category_quantity_approved + $value['quantity_approved'];	
				}
				
				// number format
				$category_quantity = number_format($category_quantity, $settings->decimal_places, '.', '');
				$category_quantity_approved = number_format($category_quantity_approved, $settings->decimal_places, '.', '');
				
				$list .= "
					<tr>
						<td class=category >".$subrow['caption']."</td>
						<td class=currency >CHF</td>
						<td class=number >$category_quantity</td>
						<td class=number >$category_quantity_approved</td>		
					</tr>
				";
				
				$planning_quantity = $planning_quantity + $category_quantity;
				$planning_quantity_approved = $planning_quantity_approved + $category_quantity_approved;
			}
			
			// number format
			$planning_quantity = number_format($planning_quantity, $settings->decimal_places, '.', '');
			$planning_quantity_approved = number_format($planning_quantity_approved, $settings->decimal_places, '.', '');
					
			$list .= "
				<tr class=subtotal >
					<td class=planning >".$translate->total." $planning_name</td>
					<td class=currency >CHF</td>
					<td class=number >$planning_quantity</td>
					<td class=number >$planning_quantity_approved</td>
				</tr>
			";
			
			$ordersheet_quantity = $ordersheet_quantity + $planning_quantity;
			$ordersheet_quantity_approved = $ordersheet_quantity_approved + $planning_quantity_approved;
		}
		
		// number format
		$ordersheet_quantity = number_format($ordersheet_quantity, $settings->decimal_places, '.', '');
		$ordersheet_quantity_approved = number_format($ordersheet_quantity_approved, $settings->decimal_places, '.', '');
		
		$list .= "
			<tr class=total >
				<td>Total Order Sheet</td>
				<td class=currency >CHF</td>
				<td class=number >$ordersheet_quantity</td>
				<td class=number >$ordersheet_quantity_approved</td>
			</tr>
			</tbody></table>
		";
	}	
	else {
		$list = "<div class=emptybox >".$translate->empty_costs."</div>";
	}		
?>
<style type="text/css">
	
	.costs { 
		display: block;
		width: 800px; 
	}
	
	.listing thead td {
		color: gray;
	}
	
	.listing td {
		border: 0;
	}
	
	.listing .planning td {
		padding-top: 30px;
		font-size: 14px;
		font-weight: 700;
		border-bottom: 1px solid silver;
	}
	
	.listing .subtotal td {
		color: gray;
		border-top: 1px solid #ddd;
		background-color: #f3f3f3;
		font-size: 12px;
		padding: 5px 10px;
	}
	
	.listing tr:first-child td {
		padding-top: 10px;	
	}
	
	.listing  .category {
		padding-left: 30px;
	}
	
	.listing  .number {
		text-align: right;
		width: 10%;
	}
	
	.listing .currency {
		text-align: right;
	}

	.listing .total td {
		padding-top: 40px;
		font-weight: 700;
		font-size: 18px;
	}
	
</style>
<div class=costs >
	<div class="-box">
		<div class=-content ><?php echo $list; ?></div>
	</div>
	<div class='actions'>
	<?php 
		echo ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => $translate->back
		));
	?>
	</div>
</div>