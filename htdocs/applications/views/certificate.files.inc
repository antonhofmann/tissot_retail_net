<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$(".modal").on("click", function() {
		
		var path = $(this).attr('tag');
		var title = $(this).parent().next().find('a').text();
	
		if (isImage(path)) {

			retailnet.modal.show(path, {
				title: (title) ? title : ''
			});
			
		} else {
			window.open('http://'+location.host+path);
		}
	});
});
</script>
<style type="text/css">

	#files { 
		width: 700px; 
	}

	.expired {
		color: red;
	}
	
</style>
<div id="files">
	<div class="table-toolbox">
		<form class=toolbox >
			<?php
				if ($buttons['add'])  {
					echo ui::button(array(
						'id' => 'add',
						'icon' => 'add',
						'href' => $buttons['add'],
						'label' => $translate->add_new
					));
				}
			?>
		</form>
	</div>
	<?php
		if ($files) {
			
			$table = new Table();
			$table->datagrid = $files;
			$table->fileicon('width=5%');
			$table->certificate_file_version();
			$table->certificate_file_expiry_date(Table::ATTRIBUTE_NOWRAP, 'width=20%');
			
			echo $table->render();
		}
	?>
</div>

<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>