<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();
	$request = Request::instance();
	
	$form = new Form(array(
		'id' => 'form',
		'action' => '/applications/helpers/item.supplier.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);	
	
	$form->supplier_id(
		Form::TYPE_HIDDEN
	);	
	
	$form->supplier_item(
		Form::TYPE_HIDDEN
	);

	$form->supplier_address(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->supplier_item_code(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);	

	$form->supplier_item_name(
		Form::TYPE_TEXT
	);

	$form->supplier_item_price(
		Form::TYPE_TEXT,
		Form::TOOLTIP_NUMERIC,
		Validata::PARAM_REQUIRED,
		Validata::PARAM_NUMBER
	);

	$form->supplier_item_currency(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->fieldset('Description', array(
		'supplier_address',
		'supplier_item_code',
		'supplier_item_name',
		'supplier_item_price',
		'supplier_item_currency'
	));	

	if ($request->controller == 'items') {

		$form->store_id(
			Form::TYPE_HIDDEN
		);

		$form->store_item(
			Form::TYPE_HIDDEN
		);
		
		$form->store_address(
			Form::TYPE_HIDDEN
		);

		$form->store_global_order(
			Form::TYPE_TEXT
		);
		
		$form->store_last_global_order(
			Form::TYPE_TEXT
		);

		$form->store_last_global_order_date(
			Form::TYPE_TEXT
		);

		$form->store_show_in_stock_control(
			Form::TYPE_CHECKBOX
		);

		$form->store_stock_control_starting_date(
			Form::TYPE_TEXT
		);

		$form->store_consumed_upto_021101(
			Form::TYPE_TEXT
		);

		$form->store_minimum_order_quantity(
			Form::TYPE_TEXT
		);

		$form->store_reproduction_time_in_weeks(
			Form::TYPE_TEXT
		);

		$form->store_physical_stock(
			Form::TYPE_TEXT
		);

		$form->store_minimum_stock(
			Form::TYPE_TEXT
		);

		$form->fieldset('Stock', array(
			'store_global_order',
			'store_last_global_order',
			'store_last_global_order_date',
			'store_show_in_stock_control',
			'store_stock_control_starting_date',
			'store_consumed_upto_021101',
			'store_minimum_order_quantity',
			'store_reproduction_time_in_weeks',
			'store_physical_stock',
			'store_minimum_stock'
		));	
	}

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>

<style type="text/css">

	#form { 
		width: 700px; 
	}
	
	#form textarea {
		width: 440px;
		height: 60px;
	}

	.Stock label {
		width: 260px !important;
	}

	.Stock input[type=text] {
		width: 100px !important;
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>