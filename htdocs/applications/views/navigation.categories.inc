<?php 

	$request = request::instance();
	$menu = menu::instance();
	$categories = $menu->data[0];
	
	if($categories) {

		echo "<ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dLabel\">";

		foreach ($categories as $id => $row) {
			
			if (!$row['tab']) {
				$active = $request->id && in_array($id, $request->parents) ? "class='active'" : null;
				$target = $active ? null : "target=_blank";
				echo "<li $active><a href=\"/{$row[url]}\" $active $target >{$row[caption]}</a></li>";
			}
		}

		echo "</ul>";

	}
?>