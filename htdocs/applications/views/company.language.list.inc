<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript" src="/public/js/company.languages.js"></script>
<style type="text/css">
	
	#company_languages, 
	.company_languages { 
		width: 500px; 
	}
	
	.company_languages { 
		font-size: .8em; 
		font-weight:700; 
		margin-bottom: 20px;
	}
	
</style>
<?php 
	echo $request->form(array(
		'id' => $id
	)); 
?>
<div class=company_languages>
	<?php echo $translate->company_order_languages; ?>
</div>
<div id="company_languages"></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'caption' => $translate->back
	));
?>
</div>