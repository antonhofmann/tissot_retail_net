<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	
});
</script>
<style type="text/css">

	#items { 
		width: 700px; 
	}
	
</style>
<div id="items">

	<div class="table-toolbox">
		<form class=toolbox >
			<?php
				if ($buttons['add'])  {
					echo ui::button(array(
						'id' => 'add',
						'icon' => 'add',
						'href' => $buttons['add'],
						'label' => $translate->add_new
					));
				}
			?>
		</form>
	</div>

	<?php

	if ($items) {

		$table = new Table();
		$table->datagrid = $items;
		
		$table->postype_name(Table::ATTRIBUTE_NOWRAP, 'width=20%');
		$table->design_objective_item_name(Table::ATTRIBUTE_NOWRAP, "href=/catalog/designobjectivegroups/objectives/$id");
		$table->design_objective_item_isstandard(Table::ATTRIBUTE_NOWRAP, 'width=5%');

		echo $table->render();
	}

	?>

	<div class='actions'>
	<?php 
		echo ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => Translate::instance()->back
		));
	?>
	</div>
</div>