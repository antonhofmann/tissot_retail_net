<script type="text/javascript">
$(document).ready(function() {

	$('#productLineDesignObjectives').tableLoader({
		url: '/applications/helpers/product.line.desogn.objective.groups.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">

	#productLineDesignObjectives { 
		width: 500px; 
	}

	.dropdown-placeholder  {
		margin-left:0;
	}
	
</style>
<div id="productLineDesignObjectives" ></div>
<?php echo Request::instance()->form(); ?> 