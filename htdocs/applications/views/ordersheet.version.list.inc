<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	var ordersheets = $('#ordersheet_versions').tableLoader({
		url: '/applications/helpers/ordersheet.version.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select', self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">
	
	#ordersheet_versions {
		width: 700px;
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="ordersheet_versions"></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>