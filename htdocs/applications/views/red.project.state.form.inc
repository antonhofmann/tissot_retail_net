<?php

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'projectState',
		'action' => '/applications/helpers/red.project.state.save.php',
		'method' => 'post'
	));


	$form->red_projectstate_id(
		Form::TYPE_HIDDEN
	);

	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);

	$form->red_projectstate_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);

	$form->red_projectstate_icon(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->fieldset('projectstate_details', array(
		'red_projectstate_name',
		'red_projectstate_icon'

	));

	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}

	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form->extended_attributes = $attributes;
	$form = $form->render();
?>
<link rel="stylesheet" type="text/css" href="/public/scripts/jquery/jquery.ui.css" />
<link rel="stylesheet" type="text/css" href="/public/scripts/selectboxit/selectboxit.css" />
<link rel="stylesheet" type="text/css" href="/public/scripts/selectboxit/theme.bootstrap.css" />
<script type="text/javascript" src="/public/scripts/jquery/ui.1.9.1.js" ></script>
<script type="text/javascript" src="/public/scripts/selectboxit/selectboxit.js" ></script>
<link rel="stylesheet" type="text/css" href="/public/scripts/loader/loader.css" />
<script type="text/javascript" src="/public/scripts/loader/loader.js"  ></script>
<link rel="stylesheet" href="/public/scripts/validationEngine/css/validationEngine.jquery.css" type="text/css"/>
<script src="/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="/public/scripts/validationEngine/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="/public/scripts/qtip/qtip.js"  ></script>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $('#projectState');

		$("#red_projectstate_icon").selectBoxIt({
			aggressiveChange: true,
			changed: function() {

			}
		});

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			onAjaxFormComplete: function(status, form, json, options) {

				retailnet.loader.hide();
				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

				if (json) {

					if (json.response && json.redirect) {
						window.location=json.redirect;
					}

					if (json.message) {
						$.jGrowl(json.message,{ 
							sticky: (json.response) ? false : true, 
							theme: (json.response) ? 'message' : 'error'
						});
					}
				}
			}
		});

		$("#save").click(function(event) {

			event.preventDefault();

			var errors = form.find('.error').length;
			var valid = form.validationEngine('validate');

			if (errors || !valid) {
				$.jGrowl('Please check red marked fields.',{ 
					sticky: true, 
					theme:'error'
				});
			}
			else {
				retailnet.loader.show();
				form.submit();
			}
		});


		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});

		$('.dialog').click(function(event) {

			event.preventDefault();

			var button = $(this);
			
			$('#apply, a.apply').attr('href', button.attr('href'));
			
			retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
		});

		$('#cancel').bind('click', function() {
			retailnet.modal.close();
			return false;
		});

	});
</script>
<style type="text/css">

	#projectState {
		width: 700px;
	}
	
	.selectboxit-container .selectboxit-options {
    	max-height: 300px;
	}

</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>