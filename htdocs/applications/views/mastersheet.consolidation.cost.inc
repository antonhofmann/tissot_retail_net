<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = Request::instance();
	
	// db application model
	$model = new Model($request->application);
	
	// request filters
	$filters = array();
	
	// filter: mastersheet
	$filters['mastersheet'] = "mps_ordersheet_mastersheet_id = $id";
	
	// ordersheet items
	$result = $model->query("
		SELECT
			mps_ordersheet_id,
			mps_material_planning_type_id,
			mps_material_planning_type_name,
			mps_material_collection_category_id,
			mps_material_collection_category_code,
			mps_material_id,
			(mps_ordersheet_item_price * SUM(mps_ordersheet_item_quantity)) as mps_ordersheet_item_quantity,
			(mps_ordersheet_item_price * SUM(mps_ordersheet_item_quantity_approved)) as mps_ordersheet_item_quantity_approved
		FROM mps_ordersheet_items
	")
	->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
	->bind(Ordersheet_Item::DB_BIND_MATERIALS)
	->bind(Material::DB_BIND_COLLECTIONS)
	->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
	->bind(Material::DB_BIND_PLANNING_TYPES)
	->filter($filters)
	->group('mps_material_id')
	->order('mps_material_planning_type_name')
	->order('mps_material_collection_category_code')
	->fetchAll();

	if ($result) { 
		
		// group items by planning types
		// and material collection categories
		foreach ($result as $row) {
			
			$planning = $row['mps_material_planning_type_id'];
			$category = $row['mps_material_collection_category_id'];
			$item = $row['mps_material_id'];
			
			$datagrid[$planning]['caption'] = $row['mps_material_planning_type_name'];
			$datagrid[$planning]['data'][$category]['caption'] = $row['mps_material_collection_category_code'];
			$datagrid[$planning]['data'][$category]['data'][$item]['quantity'] = $row['mps_ordersheet_item_quantity'];
			$datagrid[$planning]['data'][$category]['data'][$item]['quantity_approved'] = $row['mps_ordersheet_item_quantity_approved'];
		}
		
		// list
		$list = "
			<table class=listing ><thead>
				<tr>
					<td colspan=2 >&nbsp;</td>
					<td class=number >".$translate->quantity."</td>
					<td class=number >".$translate->approved."</td>
				</tr>
			</thead><tbody>
		";
		
		// datagrid
		foreach ($datagrid as $planning => $row) {
			
			$planning_quantity = 0;
			$planning_quantity_approved = 0;
			
			$list .= "<tr class=planning ><td colspan=4 >".$row['caption']."</td></tr>";
			
			// collection categories
			if ($row['data']) {
				
				foreach ($row['data'] as $category => $subrow) {
					
					$category_quantity = 0;
					$category_quantity_approved = 0;
					
					// order sheet items
					if ($subrow['data']) {
						
						$item_quantity = 0;
						$item_quantity_approved = 0;
						
						foreach ($subrow['data'] as $key => $item) {
							$item_quantity = $item_quantity + $item['quantity'];
							$item_quantity_approved = $item_quantity_approved + $item['quantity_approved'];
						}
						
						$category_quantity = $category_quantity + $item_quantity;
						$category_quantity_approved = $category_quantity_approved + $item_quantity_approved;
					}
					
					// number format
					$category_quantity = number_format($category_quantity, $settings->decimal_places, '.', '');
					$category_quantity_approved = number_format($category_quantity_approved, $settings->decimal_places, '.', '');
					
					$list .= "
						<tr>
							<td class=category >".$subrow['caption']."</td>
							<td class=currency >CHF</td>
							<td class=number >$category_quantity</td>
							<td class=number >$category_quantity_approved</td>		
						</tr>
					";
					
					$planning_quantity = $planning_quantity + $category_quantity;
					$planning_quantity_approved = $planning_quantity_approved + $category_quantity_approved;
				}
			}
			
			// number format
			$planning_quantity = number_format($planning_quantity, $settings->decimal_places, '.', '');
			$planning_quantity_approved = number_format($planning_quantity_approved, $settings->decimal_places, '.', '');
					
			$list .= "
				<tr class=subtotal >
					<td class=planning >".$translate->total." ".$row['caption']."</td>
					<td class=currency >CHF</td>
					<td class=number >$planning_quantity</td>
					<td class=number >$planning_quantity_approved</td>
				</tr>
			";
			
			$ordersheet_quantity = $ordersheet_quantity + $planning_quantity;
			$ordersheet_quantity_approved = $ordersheet_quantity_approved + $planning_quantity_approved;
		}
		
		// number format
		$ordersheet_quantity = number_format($ordersheet_quantity, $settings->decimal_places, '.', '');
		$ordersheet_quantity_approved = number_format($ordersheet_quantity_approved, $settings->decimal_places, '.', '');
		
		$list .= "
			<tr class=total >
				<td>Total Master Sheet</td>
				<td class=currency >CHF</td>
				<td class=number >$ordersheet_quantity</td>
				<td class=number >$ordersheet_quantity_approved</td>
			</tr>
			</tbody></table>
		";
	}
	else  {
		$costs = "<div class=emptybox >".$translate->empty_costs."</div>";
	}
	
?>
<style type="text/css">
	
	.costs { 
		display: block;
		width: 800px; 
	}
	
	.listing thead td {
		color: gray;
	}
	
	.listing td {
		border: 0;
	}
	
	.listing .planning td {
		padding-top: 30px;
		font-size: 14px;
		font-weight: 700;
		border-bottom: 1px solid silver;
	}
	
	.listing .subtotal td {
		color: gray;
		border-top: 1px solid #ddd;
		background-color: #f3f3f3;
		font-size: 12px;
		padding: 5px 10px;
	}
	
	.listing tr:first-child td {
		padding-top: 10px;	
	}
	
	.listing  .category {
		padding-left: 30px;
	}
	
	.listing  .number {
		text-align: right;
		width: 10%;
	}
	
	.listing .currency {
		text-align: right;
	}

	.listing .total td {
		padding-top: 40px;
		font-weight: 700;
		font-size: 18px;
	}
	
</style>
<div class=costs >
	<div class="-box">
		<div class=-content ><?php echo $list; ?></div>
	</div>
	<div class='actions'>
	<?php 
	
		if ($buttons['back']) {
			echo ui::button(array(
				'id' => 'back',
				'icon' => 'back',
				'href' => $buttons['back'],
				'label' => $translate->back
			));
		}
		
		if ($buttons['print']) {
			echo ui::button(array(
				'id' => 'print',
				'icon' => 'print',
				'href' => $buttons['print'],
				'label' => $translate->print
			));
		}
	?>
	</div>
</div>