<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'company_additionals',
		'action' => '/applications/helpers/company.additional.save.php',
		'method' => 'post'
	));
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->address_additional_address(
		Form::TYPE_HIDDEN
	);
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->address_additional_id(
		Form::TYPE_HIDDEN
	);
	
	$form->address_mps_customernumber(
		Form::TYPE_TEXT,
		FORM::PARAM_DISABLED
	);
	
	$form->address_additional_customerno2(
		Form::TYPE_TEXT
	);
	
	$form->address_additional_customerno3(
		Form::TYPE_TEXT
	);
	
	$form->address_additional_customerno4(
		Form::TYPE_TEXT
	);
	
	$form->address_additional_customerno5(
		Form::TYPE_TEXT
	);

	$form->fieldset('alternative_customer_numbers', array(
		'address_mps_customernumber',
		'address_additional_customerno2',
		'address_additional_customerno3',
		'address_additional_customerno4',
		'address_additional_customerno5'	
	));
	
	$form->address_mps_shipto(
		Form::TYPE_TEXT,
		Form::PARAM_DISABLED
	);
	
	$form->address_additional_shiptono2(
		Form::TYPE_TEXT
	);
	
	$form->address_additional_shiptono3(
		Form::TYPE_TEXT
	);
	
	$form->address_additional_shiptono4(
		Form::TYPE_TEXT
	);
	
	$form->address_additional_shiptono5(
		Form::TYPE_TEXT
	);
	
	$form->fieldset('alternative_shipto_numbers', array(
		'address_mps_shipto',
		'address_additional_shiptono2',
		'address_additional_shiptono3',
		'address_additional_shiptono4',
		'address_additional_shiptono5'
	));
	
	$form->address_additional_lps_customerno1(
		Form::TYPE_TEXT
	);
	
	$form->address_additional_lps_customerno2(
		Form::TYPE_TEXT
	);
	
	$form->address_additional_lps_customerno3(
		Form::TYPE_TEXT
	);
	
	$form->address_additional_lps_customerno4(
		Form::TYPE_TEXT
	);
	
	$form->address_additional_lps_customerno5(
		Form::TYPE_TEXT
	);
	
	$form->fieldset('LPS Customer Numbers', array(
		'address_additional_lps_customerno1',
		'address_additional_lps_customerno2',
		'address_additional_lps_customerno3',
		'address_additional_lps_customerno4',
		'address_additional_lps_customerno5'
	));
	
	$form->address_additional_dummie_quantity(
		Form::TYPE_TEXT
	);
	
	$form->fieldset('Standard order quantity for dummies', array(
		'address_additional_dummie_quantity'
	));
	
	$form->dataloader($data);
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form = $form->render();
?>
<script type="text/javascript" src="/public/js/company.additionals.js"></script>
<style type="text/css">
	
	#company_additionals {
		width: 700px;
	}

	#company_additionals label {
		width: 200px;
	}
	
</style>
<?php echo $form; ?>
