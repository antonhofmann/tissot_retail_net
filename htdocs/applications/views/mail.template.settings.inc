<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'mail_template',
		'action' => '/applications/helpers/mail.template.settings.save.php',
		'method' => 'post'
	));
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->mail_template_id(
		Form::TYPE_HIDDEN
	);
	
	
	
	$form->mail_template_action_id(
		Form::TYPE_SELECT
	);
	
	$form->fieldset('Action File', array(
		'mail_template_action_id',
	));
	
	
	$form->mail_template_sender_id(
		Form::TYPE_RADIO,
		Form::PARAM_NO_LABEL,
		'class=standard_sender',
		'direction=vertical'
	);
	
	$form->fieldset('Standard Sender', array(
		'mail_template_sender_id'
	));
	

	$form->mail_template_cc(
		Form::TYPE_CHECKBOX
	);

	$form->mail_template_bcc(
		Form::TYPE_CHECKBOX
	);
	
	$form->fieldset('Carbon Copies', array(
		'mail_template_cc',
		'mail_template_bcc'
	));
	
	$form->mail_template_group(
		Form::TYPE_CHECKBOX
	);

	$form->mail_template_group_salutation(
		Form::TYPE_TEXT
	);
	
	$form->fieldset('Group Mail', array(
		'mail_template_group',
		'mail_template_group_salutation'
	));
	
	$form->mail_template_debugger(
		Form::TYPE_CHECKBOX
	);
	
	$form->mail_template_view_modal(
		Form::TYPE_CHECKBOX
	);
	
	$form->mail_template_track(
		Form::TYPE_CHECKBOX
	);
	
	$form->mail_template_dev_email(
		Form::TYPE_TEXT,
		Validata::PARAM_EMAIL,
		Form::TOOLTIP_EMAIL		
	);
	
	
	$form->fieldset('miscellanous', array(
		'mail_template_debugger',
		'mail_template_view_modal',
		'mail_template_track',
		'mail_template_dev_email'
	));

	
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#mail_template");
		var id = $("#mail_template_id");

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			onAjaxFormComplete: function(status, form, json, options) {

				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

				if (json) {

					if (json.response && json.redirect) {
						window.location=json.redirect;
					}

					if (json.response && json.id) {
						id.val(json.id);
					}

					if (json.message) {
						$.jGrowl(json.message, { 
							sticky: (json.response) ? false : true,
							theme: (json.response) ? 'message' : 'error'
						});
					}
				}
			}
		});

		$('#mail_template_group').on('click', function(e) {
			if ($(this).is(':checked')) {
				$('#mail_template_group_salutation').focus();
			} else {
				$('#mail_template_group_salutation').val('');
			}
		})
		
		$("#save").click(function(event) { 

			event.preventDefault();
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

			form.find('input, textarea, select').removeClass('error');

			var error = false;

			if ($('#mail_template_group').is(':checked') && !$('#mail_template_group_salutation').val()) {
				$('#mail_template_group_salutation').addClass('error');
				$.jGrowl('Please set the salutation for multiple persons', { 
					sticky: true,
					theme: 'error'
				});

				error = true;
			}

			if ($('#mail_template_devmod').is(':checked') && !$('#mail_template_devmod_email').val()) {
				$.jGrowl('Please set e-mail address for recipient in development mode.', { 
					sticky: true,
					theme: 'error'
				});

				error = true;
			}

			if (!error) {
				form.submit();
			}
			
			return false;
		});

		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});

		$('.dialog').click(function(event) {

			event.preventDefault();

			var button = $(this);
			
			$('#apply, a.apply').attr('href', button.attr('href'));
			
			retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
		});

		$('#cancel').bind('click', function(event) {
			event.preventDefault();
			retailnet.modal.close();
			return false;
		});	


		$('#mail_template_standard_recipient_id').change(function() {
			var checked = $(this).val() ? true : false;
			$('#mail_template_standard_recipient_cc,#mail_template_standard_recipient_additionals').attr('checked', checked);
		});

		$('input.standard_sender').change(function() { 
			var checked = $(this).val()==0 ? true : false;
			$('#mail_template_standard_sender_select').attr('disabled', checked);
		});

		$('#mail_template_standard_sender_select').change(function() {

			var value = $(this).val();
			var disabled = value ? false : true;

			$(this).attr('disabled', disabled);

			if (!value) {
				$('input.standard_sender:first').attr('checked', true);
			}
		});

	});
</script>

<style type="text/css">

	#mail_template { 
		min-width: 740px; 
	}

	.elembox {
		margin-left: 194px;
	}
	
</style>
<?php 
	
	echo $form; 
?>