<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	var splittinglist = $('#splittinglist').tableLoader({
		url: '/applications/helpers/mastersheet.splitting.list.php',
		data: $('.request').serializeArray()
	});

});
</script>
<style type="text/css">
	#splittinglist { width: 600px; }
</style>
<?php 
	echo $request->form();
?>
<div id="splittinglist"></div>
<div class='actions'>
<?php 

	if ($buttons['back']) {
		echo ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => $translate->back
		));
	}
?>
</div>