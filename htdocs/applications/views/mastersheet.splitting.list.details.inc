<?php 
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	var buttonSplittingDialog = $('#splitting'),
		dialogSplitting = $('#splitting_dialog');

	$('#itemlist').tableLoader({
		url: '/applications/helpers/mastersheet.splitting.list.items.php',
		filters: $('.request')
	});

	// if button action required modal screen
	$('.modal-trigger').click(function(event) {

		event.preventDefault();
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
		
		var button = $(this);

		if (button.attr('id')==buttonSplittingDialog.attr('id')) { 

			// empty planning types place holder
			$('.planningtypes-placeholder', dialogSplitting).empty();
			
			$.each($('h5'), function(i, elem) {
	
				var id = $(this).attr('data');
				var caption = '<span>'+$(this).text()+'</span>';
				var input = '<input type=checkbox class=planning_type name=planning_type['+id+'] value=1 />';
	
				// insert input box 
				$('.planningtypes-placeholder', dialogSplitting).append('<p>'+input+caption+'</p>');
			});
	
			// set form action
			$('form', dialogSplitting).attr('action', buttonSplittingDialog.attr('href'));
			$('.submit', dialogSplitting).addClass('submit_splitting');
		}
			
		// show modal box
		var modal = '#'+button.attr('id')+'_dialog';
		$('a.submit', $(modal)).attr('href', button.attr('href'))
		retailnet.modal.show(modal);

		return false;
	});


	// close modal screen
	$('.cancel').bind('click', function(event) {
		event.preventDefault();
		retailnet.modal.close();
		return false;
	});	

	
	// submit modal screen
	$('.submit').bind('click', function(event) {

		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

		// if is disabled prevent action
		if ($(this).hasClass('disabled')) {
			return false;
		} 
		// submit splitting list
		else if ($(this).hasClass('submit_splitting')) {
			
			var selectedPlannings = $('input.planning_type:checked').length;
			var selectedOptions = $('input.option:checked').length;

			if (selectedPlannings && selectedOptions) {
				
				$('form', dialogSplitting).submit();
				$('input:checkbox', dialogSplitting).attr('checked', false);

				retailnet.modal.close();
				
			} else {

				retailnet.notification.error('Please, select at least one planning type and one print option.');
			}

			return false;
		}
	})
});
</script>
<style type="text/css">

	.pagecontent {
		width: 1200px;
	}
	
	.actions {
		width: 1160px;
	}

	h6 { 
		display: block; 
		margin-bottom: 5px; 
		font-size: .8em;
		font-weight: 500;
	}
	
	h5 { 
		display: block;
		margin: 40px 0 20px; 
		font-size: 1em;
		font-variant: small-caps;
		border-bottom: 1px solid silver;
		color: gray;
	}

	table { 
		margin-top: 20px; 
		border-style: solid; 
		border-color: silver; 
	}
	
	.totoal-collection {
		display: block;
		width: 100%;
		font-size: .75em;
		text-align: right;
		padding-top: 5px;
	}
	
	.totoal-collection b {
		padding-left: 5px;
	}
	
	#splitting_dialog {
		width: 400px;
	}
	
	#splitting_dialog .modalbox-content {
		max-height: 800px;
		overflow-y: auto;
	}
	
	.modalbox-content p span {
		display: inline-block;
		padding-left: 10px;
		font-size: 12px;
	}
	
	.modalbox-content strong {
		display: block;
		font-size: 14px;
		margin: 10px 0;
	}

</style>
<div id="itemlist"></div>	
<div class='actions'>
	<?php 
		
		if ($buttons['back']) {
			echo ui::button(array(
				'id' => 'back',
				'icon' => 'back',
				'href' => $buttons['back'],
				'label' => $translate->back
			));
		}

		if ($buttons['delete']) {
			echo ui::button(array(
				'id' => 'delete',
				'icon' => 'delete',
				'class' => 'modal-trigger',
				'label' => $translate->delete,
				'href' => $buttons['delete']
			));
		}
		
		if ($buttons['print']) {
			echo ui::button(array(
				'id' => 'btnprint',
				'icon' => 'print',
				'label' => $translate->print,
				'href' => $buttons['print']
			));
		}
		
		if ($buttons['splitting']) {
			echo ui::button(array(
				'id' => 'splitting',
				'class' => 'modal-trigger',
				'icon' => 'print',
				'label' => $translate->splitting_list,
				'href' => $buttons['splitting']
			));
		}
	?>
</div>
<?php 

	echo $request->form();
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'class' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'class' => 'submit',
				'label' => $translate->yes
			),
		)
	));
?>
<!-- Splitting Box: Print dialog form splitting list -->
<div class='modalbox-container'>
	<div id="splitting_dialog" class="modalbox modal">
		<div class=modalbox-header >
			<div class='title'>Select Export Options</div>
		</div>
		<div class=modalbox-content-container >
			<form method=post >
				<div class=modalbox-content >
					<strong>Planning Types</strong>
					<div class="modal-fieldset planningtypes-placeholder"></div>
					<strong>Options</strong>
					<div class=modal-fieldset >
						<p>
							<input type=checkbox class=option name='desired_quantity' value='1' /> 
							<span>Desired Quantity by Customer</span>
						</p>
						<p>
							<input type=checkbox class=option name='desired_quantity_total' value='1' /> 
							<span>Total Cost of Desired Quantity</span>
						</p>
						<p>
							<input type=checkbox class=option name='approved_quantity' value='1' /> 
							<span>Approved Quantity</span>
						</p>
						<p>
							<input type=checkbox class=option name='approved_quantity_total' value='1' /> 
							<span>Total Cost of Approved Quantity</span>
						</p>
						<p>
							<input type=checkbox class=option name='ordered_quantity' value='1' /> 
							<span>Pre Ordered Quantity</span>
						</p>
						<p>
							<input type=checkbox class=option name='ordered_quantity_total' value='1' /> 
							<span>Total Cost of Pre Ordered Quantity</span>
						</p>
					</div>
				</div>
			</form>
		</div>
		<div class=modalbox-footer >
			<div class=modalbox-actions >
				<a class='button cancel'>
					<span class="icon cancel"></span>
					<span class="label"><?php echo $translate->cancel ?></span>
				</a>
				<a class='button submit'>
					<span class="icon print"></span>
					<span class="label"><?php echo $translate->print ?></span>
				</a>
			</div>
		</div>
	</div>
</div>