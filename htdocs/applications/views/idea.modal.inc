<div class="modalbox-container">
	<div id="ideabox" class="adomat modalbox ideabox fileuploader <?php echo $class; ?>" <?php echo _array::rend_attributes($attributes) ?> >
		<div class="modalbox-header" >
			<div class="title"><?php echo translate::instance()->idea; ?></div>
			<div class="subtitle">Please give us your suggestion.</div>
		</div>
		<div class="modalbox-content-container" >
			<div class="modalbox-content">
				<form class="default" method="post" action="/applications/helpers/idea.submit.php">
				
					<?php 
						if ($fields) {
							foreach ($fields as $k => $v) {
								echo "<input type='hidden' name='$k' class='$k' value='$v' >";
							}
						}
					?>
				
					<div class="-filedset idea-content">
						<?php if ($categories) : ?>
						<div class="-row -odd">
							<label for="idea_idea_category_id">Category</label>
							<div class="-element -select">
								<select name="idea_idea_category_id" class="form-control required" >
									<option value=0 >Select Category</option>
									<?php 
										foreach ($categories as $k => $v) {
											echo "<option value='$k'>$v</option>";
										}
									?>
								</select>
							</div>
						</div>
						<?php endif; ?>
						<div class="-row -odd">
							<label for="idea_title">Title</label>
							<div class="-element -text"><input type="text" class="form-control required" name="idea_title" placeholder="Title"></div>
						</div>
						<div class="-row">
							<label for="idea_text">Content</label>
							<div class="-element -textarea"><textarea class="form-control required" name="idea_text" placeholder="Content" ></textarea></div>
						</div>
						<div class="-row">
							<label>Files<br /><small>max. 3 files</small></label>
							<div class="-element files bootstrap">
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="modalbox-footer" >
			<div class=modalbox-actions >
				<span class="button cancel"> 
					<span class="icon cancel"></span>
					<span class="label"><?php echo translate::instance()->cancel ?></span>
				</span>
				<span class="button fileinput-button"> 
					<span class="icon icon45"></span>
					<span class="label">Add Files</span>
					<input type="file" name="files[]" multiple>
				</span>
				<span class="button submit"> 
					<span class="icon save"></span>
					<span class="label"><?php echo translate::instance()->submit ?></span>
				</span>
			</div>
		</div>
	</div>
</div>
<!-- The template to display files available for upload -->
<script id="idea-template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
<div class="file-placeholder template-upload fade">
	<div class="preview">
		<img src="/public/scripts/fileuploader/img/loading.gif" style="max-width:80px">	
	</div>
	<div class="file-info">
		{% if (!i && !o.options.autoUpload) { %}
			<button class="btn btn-xs btn-primary start chankuploader" disabled>
				<i class="glyphicon glyphicon-upload"></i> <span>Upload</span>
			</button>
		{% } %}
		{% if (!i) { %}
			<br /><button class="btn btn-xs btn-warning cancel">
				<i class="fa fa-minus-circle"></i> <span>Cancel</span>
			</button><br />
		{% } %}
		<span class="name">{%=file.name%}</span><br />
		<span class="size">Processing...</span>
		<strong class="error text-danger"></strong>
		<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
			<div class="progress-bar progress-bar-success" style="width:0%;"></div>
		</div>
	</div>
</div>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="idea-template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
<div class="file-placeholder template-download fade">
	<input type="hidden" name="file[{%=file.id%}]" class="form-control input-sm required" value="{%=file.url%}">
	{% if (file.thumbnailUrl) { %}
	<div class="preview">
		<img src="{%=file.thumbnailUrl%}" style="max-width:80px">	
	</div>
	{% } %}
	<div class="file-info">
		<span class="filename name">
			{% if (file.url) { %}
				<a class="btn btn-xs btn-info" href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}">
					<i class="fa fa-file"></i> <span>Download</span>
				</a><br />
			{% } %}
			{% if (file.deleteUrl) { %}
 				<button class="btn btn-xs btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
					<i class="fa fa-times-circle"></i> <span>Delete</span>
				</button>
			{% } else { %}
				<button class="btn btn-xs btn-warning cancel">
					<i class="fa fa-minus-circle"></i> <span>Cancel</span>
				</button>
				{%=file.name%}
			{% } %}
			<br /><span class="size">{%=o.formatFileSize(file.size)%}</span>
		</span>
		{% if (file.error) { %}
			<br /><span class="label label-danger">Error</span> {%=file.error%}
		{% } %}
	</div>
</div>
{% } %}
</script>