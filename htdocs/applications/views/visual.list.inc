<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#visuals').tableLoader({
		url: '/applications/helpers/visual.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select', self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">

	#visuals { 
		width: 800px; 
	}
	
</style>
<?php echo $request->form($requestFields); ?>
<div id="visuals"></div>