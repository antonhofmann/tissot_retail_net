<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$data['file'] = '
		<span id="showfile" class="button">
			<span class="icon download"></span>
			<span class="label">'.$translate->download_file.'</span>	
		</span>
		<span id="upload" class="button">
			<span class="icon upload"></span>
			<span class="label">'.$translate->upload_file.'</span>	
		</span>
		<input type=hidden name="certificate_file_file" id="certificate_file_file" value="'.$data['certificate_file_file'].'" class="file_path required" />
		<input type="hidden" name="has_upload" id="has_upload" />
	';

	$form = new Form(array(
		'id' => 'posfile',
		'action' => '/applications/helpers/certificate.file.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->certificate_file_id(
		Form::TYPE_HIDDEN,
		'class=file_id'
	);	

	$form->certificate_file_certificate_id(
		Form::TYPE_HIDDEN
	);

	$form->certificate_file_version(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		'class=file_title'
	);

	$form->certificate_file_expiry_date(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		Validata::CLASS_DATAPICKER
	);
	
	$form->file(
		Form::TYPE_AJAX,
		FORM::PARAM_SHOW_VALUE
	);
	
	$form->fieldset('file', array(
		'certificate_file_version',
		'certificate_file_expiry_date',
		'file'
	));

	$form->items(
		Form::TYPE_CHECKBOX_GROUP
	);

	$form->fieldset('Supplier Items', array(
		'items'
	));	

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<style type="text/css">
	
	#posfile { 
		width: 700px;
	}
	
	form .equipment input { 
		width: 120px; 
	}
	
	#product_line_file_description { 
		width: 440px; 
		height: 100px; 
	}
	
</style>
<script type="text/javascript" src="/public/scripts/date.js"></script>
<script type="text/javascript">
	
	$(document).ready(function() {

		$('#certificate_file_expiry_date').change(function() {
		
			var self = $(this);
			var val = $(this).val();
			
			// close all notofications
			retailnet.notification.hide();
			self.removeClass('error');
			
			if (val) {
				
				if (Date.parse(val, "dd.mm.YYYY")) {
					$('#certificate_file_expiry_date').removeClass('error').next().removeClass('error');
				} else {
					self.addClass('error');
					retailnet.notification.error('Invalid date.');
				}
			}
		});
	});

</script>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete_file,
		'content' => $translate->dialog_delete_file,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>