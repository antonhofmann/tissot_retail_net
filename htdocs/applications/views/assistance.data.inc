<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();
	
	$form = new Form(array(
		'id' => 'assistanceForm',
		'action' => '/applications/helpers/assistance.save.php',
		'method' => 'post'
	));
	
	$form->section(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->assistance_id(
		Form::TYPE_HIDDEN
	);
	
	$form->assistance_section_id(
		Form::TYPE_HIDDEN
	);
	
	$form->assistance_application_id(
		Form::TYPE_SELECT
	);
	
	$form->assistance_title(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->assistance_section_title(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL,
		Validata::PARAM_REQUIRED
	);
	
	$form->assistance_section_published(
		Form::TYPE_CHECKBOX
	);
	
	$form->assistance_section_copy_links(
		Form::TYPE_CHECKBOX
	);
	
	$form->fieldset('Section', array(
		'assistance_application_id',
		'assistance_title',
		'assistance_section_title',
		'assistance_section_published',
		'assistance_section_copy_links'
	));
	
	$form->assistance_section_content(
		Form::TYPE_TEXTAREA,
		Validata::PARAM_REQUIRED,
		'class=tinymce'
	);
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/public/scripts/assistance/assistance.css" />
<script type="text/javascript" src="/public/scripts/assistance/assistance.js"  ></script>
<script type="text/javascript" src="/public/scripts/tinymce4/js/tinymce/tinymce.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#assistanceForm");

		tinymce.init({
		    selector: "#assistance_section_content",
		    plugins: [
				"advlist autolink lists link image charmap print preview hr anchor pagebreak",
				"searchreplace wordcount visualblocks visualchars code fullscreen",
				"insertdatetime media nonbreaking save table contextmenu directionality",
				"emoticons template paste textcolor moxiemanager"
		    ],
		    width: '100%',
		    height: 600,
		    menubar : false,
		    statusbar : false,
		    toolbar_items_size: 'small',
			autosave_restore_when_empty: false,
			relative_urls: false,
		    document_base_url: "/public/data/files/help/",
		    toolbar: "styleselect | fontsizeselect | bold italic | forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image insertfile print",
		    moxiemanager_leftpanel: false,
		    moxiemanager_title: 'File Manager',
		    moxiemanager_path: '/public/data/files/help/',
		    body_class: 'assistance assistance-body',
		    content_css : "/public/scripts/assistance/assistance.css"
		});

		// section mod
		if ($('#section').val()) {
			$('.assistance_section_content').hide();
			tinymce.DOM.hide('assistance_section_content');
		}


		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			beforeSend: function () {
				retailnet.loader.show();
			},
			onAjaxFormComplete: function(status, form, json, options) {

				retailnet.loader.hide();
				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

				if (json) {

					if (json.response && json.redirect) {
						window.location=json.redirect;
					} else {
						window.location.reload();
					}
				}
			}
		});
		
		$("#save").click(function(event) { 
			event.preventDefault();
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
			tinymce.activeEditor.save();
			form.submit();
			return false;
		});

		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});

		$('.dialog').click(function(event) {

			event.preventDefault();

			var button = $(this);
			
			$('#apply, a.apply').attr('href', button.attr('href'));
			
			retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
		});

		$('#cancel').bind('click', function(event) {
			event.preventDefault();
			retailnet.modal.close();
			return false;
		});	

	});
	
</script>

<style type="text/css">

	#assistanceForm { 
		width: 640px; 
	}
	
	#assistance_title {
		width: 400px;
	}
	
	#assistance_section_title {
		width: 400px;
		height: 40px;
	}
	
	#assistance_section_content {
		height: 600px;
	}
	
	
	.assistance_section_content {
		padding: 0 !important;
		margin-top: 20px;
	}

	.assistance_section_content .-element.-textarea {
		display: block;
	}
	
</style>
<?php 
	
	echo $form; 
	

	if ($data['assistance_id']) {

		$model = new Model(Connector::DB_CORE);
		
		$id = $data['assistance_id'];
	
		$result = $model->query("
			SELECT
				assistance_title,
				assistance_section_id,
				assistance_section_title,
				assistance_section_content
			FROM assistance_sections
			INNER JOIN assistances ON assistance_id = assistance_section_assistance_id
			WHERE assistance_id = $id
			ORDER BY
				assistance_section_order,
				assistance_title,
				assistance_section_title
		")->fetchAll();


		if ($result) {

			// build help content
			foreach ($result as $row) {
				$section = $row['assistance_section_id'];
				$caption = $row['assistance_title'];
				$sections[$section]['title'] = nl2br($row['assistance_section_title']);
				$sections[$section]['content'] = $row['assistance_section_content'];
			}

			// box container
			echo "<div class=\"assistance assistance-box\">";
		
			// box caption
			echo "<div class=\"assistance-box-caption\">";
			echo "<i class=\"fa fa-question-circle trigger\"></i>";
			echo "<strong>$caption</strong>";
			echo "<a href='/applications/exports/pdf/assistance.php?topic=$id' target=\"_blank\" class=\"fa fa-print\"></a>";
			echo "</div>";

			// box content
			echo "<div class=\"assistance-box-content\">";
			echo "<div class=\"topics\" >";

			foreach ($sections as $section => $row) {

				$title = $row['title'];
				$content = $row['content'];
					
				echo "
					<div class=\"assistance-section-title\">
						<i class=\"fa fa-caret-right caret\"></i>
						<strong>$title</strong>
						<a href=\"/applications/exports/pdf/assistance.php?topic=$id&section=$section\" target = \"blank\" class=\"fa fa-print\"></a>
						</div>
					<div class=\"assistance-section-content\">$content</div>
				";
			}

			echo "</div></div></div>";
		}
	}
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>