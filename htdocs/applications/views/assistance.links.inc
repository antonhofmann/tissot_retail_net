<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	var id = $('#id').val();
	var section = $('#section').val();
	var url = $('#assistance_url');

	
	var assistancesURL = $('#assistancesURL').tableLoader({
		url: '/applications/helpers/assistance.links.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			// remove url
			$('.remover').click(function(e) {

				e.preventDefault();
				
				var row = $(this).closest('tr');

				retailnet.ajax.json('/applications/helpers/assistance.url.php', {
					action: 'remove',
					id: id,
					section: section,
					url: $(this).attr('data-url')
				}).success(function(data) {

					// show notifications
					if (data && data.notification) {

						var content = data.notification.content;
						var properties = eval(data.notification.properties);

						retailnet.notification.show(content, properties);
					}

					// refresh list
					if (data && data.success) {

						if ($('tbody tr', self).length > 1) {
							row.remove();
						} else {
							self.showPage();
						}
					}
				});
			});
		}
	});

	// submit on enter
	url.keypress(function(e) {

		if (e.keyCode==13) {

			$(this).blur();

			return e.keyCode != 13;
		}
	});

	// add new url
	$('.icon.add').click(function(e) {

		e.preventDefault();

		url.trigger('change');
	});

	// save url
	url.change(function(e) {

		e.preventDefault();

		$(this).focusout();
		
		retailnet.notification.hide();

		var elem = $(this),
			url = elem.val(),
			overAllSections = $('#overAllSections').is(':checked') ? 1 : 0,
			query = null;

		var urlexp = new RegExp('(http|ftp|https)://[a-z0-9\-_]+(\.[a-z0-9\-_]+)+([a-z0-9\-\.,@\?^=%&;:/~\+#]*[a-z0-9\-@\?^=%&;/~\+#])?', 'i');

		// check url
		if (urlexp.test(url)) {
			
			var a = document.createElement('a');
		    a.href = url;
		    query = a.pathname.substring(1)+a.search;
		    
		} else {

			query = url;
		}
		
		if (query) {

			retailnet.ajax.json('/applications/helpers/assistance.url.php', {
				action: 'save',
				id: id,
				section: section,
				overAllSections: overAllSections,
				url: query
			}).success(function(data) {

				// refresh list
				if (data && data.success) {
					elem.val('').empty();
					assistancesURL.showPage();
				}
				
				// show notifications
				if (data && data.notification) {
					var content = data.notification.content;
					var properties = eval(data.notification.properties);
					retailnet.notification.show(content, properties);
				}
			});
		}
	});
	
});
</script>
<style type="text/css">
	
	#assistancesURL { 
		width: 680px; 
	}
	
	.form {
		display: block;
		margin-bottom: 20px;
		width: 640px; 
	}
	
	.searchbox {
		width: 625px;
	}
	
	.searchbox  input {
		width: 600px !important;
	}
	
	span.icon.delete {
		margin: 0 !important;
	}
	
	.row {
		display: block;
		width: 100%;
		height: 32px;
		line-height: 32px;
		font-size: 12px;
	}
	
</style>
<?php 
	echo $request->form();
?>
<div class='form'>
	<div class="row">
		<span class='button searchbox'>
			<input type=text name='assistance_url' id='assistance_url' placeholder='Add new URL' class=active />
			<span class="icon add"></span>
		</span>	
	</div>
	<div class="row">
		<input type="checkbox" name="overAllSections" id="overAllSections" value="">
		<span class="allsections">Insert URL to all help sections</span>
	</div>
</div>
<div id=assistancesURL></div>
<div class="actions">
<?php 
	if ($buttons) {
		echo join(array_values($buttons));
	}
?>
</div>
