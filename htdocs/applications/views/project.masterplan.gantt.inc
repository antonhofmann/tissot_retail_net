<link rel="stylesheet" href="/public/scripts/kendo-gantt/styles/kendo.common.min.css">
<link rel="stylesheet" href="/public/scripts/kendo-gantt/styles/kendo.default.min.css">
<link rel="stylesheet" href="/public/scripts/jqrange-slider/css/classic-min.css">
<link rel="stylesheet" href="/public/css/project.masterplan.gantt.css">
<script>
require([
  'jquery', 
  '<?php print $ganttModule?>'
  ], function($, GanttView) {
    $(document).ready(function() {
      var steps = <?php print $steps?>;
      var projectID = <?php print $projectID?>;
      var view = new GanttView({ 
        steps: steps,
        projectID: projectID
      });
      view.render();
  });
});
</script>

<div id="gantt-container" class="<?php print $page?>">
  <form class="default">

    <!--
      initially this started out with just the print button. in the meanwhile it's gotten to a be a toolbar/filter
      and should really be it's own view or at least it's own template. this should be done when we have the time.
    -->
    <div class="toolbar">
      <button id="btn-pdf-export" class="button">
        <span class="icon print"></span>
        <span class="label"><?php print $translate->print?></span>
      </button>
      
      <div class="controls">
        <?php if ($page == 'masterplan-gantt'): ?>
        <div class="control">
          <select id="gantt-detail-view" class="gantt-view">
              <option value="all_phases"><?php print $translate->all_phases_view?></option>
              <option value="projects_only"><?php print $translate->projects_only_view?></option>
              <option>---</option>

            <?php foreach ($phases as $phase): ?>
              <option value="<?php print $phase['id']?>"><?php print $translate->show_phase(array('phase' => $phase['title']))?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <?php endif; ?>

        <div class="control">
          <label for="cbx-show-details"><?php print $translate->show_details?></label>
          <input type="checkbox" id="cbx-show-details" checked="checked" />
        </div>

        <div class="control">
          <select id="gantt-time-view" class="gantt-view">
            <option value="month"><?php print $translate->month_view?></option>
            <option value="year"><?php print $translate->year_view?></option>
          </select>
        </div>

        <div class="control">
          <label><?php print $translate->date_range?></label>
          <div id="gantt-date-slider"></div>
        </div>
      </div>
    </div>

  </form>

  <!-- the gantt chart -->
  <div id="gantt-chart"></div>

  <!-- box with filtered out steps -->
  <div id="filtered-out" class="hidden">
    <h2><?php print $translate->not_shown_in_gantt?></h2>
    <table class="listing"></table>
  </div>
</div>