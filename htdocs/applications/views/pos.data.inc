<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$form = new Form(array(
		'id' => 'posform',
		'action' => '/applications/helpers/pos.save.php',
		'method' => 'post'
	));
	
	$form->posaddress_id(
		Form::TYPE_HIDDEN
	);
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->add_from_company(
		Form::TYPE_HIDDEN
	);
	
	$form->posaddress_province_id(
		Form::TYPE_HIDDEN
	);
	
	$form->posaddress_client_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->posaddress_franchisee_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED,
		Form::PARAM_AJAX
	);
	
	$form->posaddress_identical_to_address(
		Form::TYPE_CHECKBOX
	);
	
	$form->posaddress_sapnumber(
		Form::TYPE_TEXT
	);
	
	$form->posaddress_sap_shipto_number(
		Form::TYPE_TEXT
	);
	
	$form->posaddress_distribution_channel(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->posaddress_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->extended('posaddress_name', ' 
		<span class="fa-stack fa-md letter-control">
  			<i class="fa fa-square fa-stack-2x"></i>
			<i class="fa fa-font fa-stack-1x fa-inverse"></i>
		</span>
	');
	
	$form->posaddress_name2(
		Form::TYPE_TEXT
	);
	
	$form->extended('posaddress_name2', '
		<span class="fa-stack fa-md letter-control">
  			<i class="fa fa-square fa-stack-2x"></i>
			<i class="fa fa-font fa-stack-1x fa-inverse"></i>
		</span>
	');
	
	$form->posaddress_address(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	/*
	$form->extended('posaddress_address', '
		<span class="fa-stack fa-md letter-control">
  			<i class="fa fa-square fa-stack-2x"></i>
			<i class="fa fa-font fa-stack-1x fa-inverse"></i>
		</span>
	');
	*/

	if (!$disabled['posaddress_address']) {
		$form->extended('posaddress_address',"
			<div class='input-mask-container address-container'>
				<input type='text' value='".$data['posaddress_street']."' class='input-mask address-mask required' name='posaddress_street' placeholder='Street'>
				<input type='text' value='".$data['posaddress_street_number']."' class='input-mask address-mask' name='posaddress_street_number'  placeholder='Number'>
			</div>
			<span class='fa-stack fa-md letter-control'>
	  			<i class='fa fa-square fa-stack-2x'></i>
				<i class='fa fa-font fa-stack-1x fa-inverse'></i>
			</span>
		");
	}
	
	$form->posaddress_address2(
		Form::TYPE_TEXT
	);
	
	$form->extended('posaddress_address2', '
		<span class="fa-stack fa-md letter-control">
  			<i class="fa fa-square fa-stack-2x"></i>
			<i class="fa fa-font fa-stack-1x fa-inverse"></i>
		</span>
	');
	
	$form->posaddress_country(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->posaddress_place_id(
		Form::TYPE_SELECT,
		Form::PARAM_AJAX,
		Validata::PARAM_REQUIRED
	);
	
	$form->posaddress_zip(
		Form::TYPE_TEXT
	);
	
	$form->posaddress_phone(
		Form::TYPE_TEXT,
		FORM::TOOLTIP_PHONE,
		Validata::PARAM_PHONE
	);

	if (!$disabled['posaddress_phone']) {
		$form->extended('posaddress_phone',"
			<div class='input-mask-container phone-container'>
				<input type='text' value='".$data['posaddress_phone_country']."' class='input-mask phone-mask' name='posaddress_phone_country' id='posaddress_phone_country' maxlength='6' placeholder='Country code'>
				<input type='text' value='".$data['posaddress_phone_area']."' class='phone-mask' name='posaddress_phone_area'  id='posaddress_phone_area' maxlength='6' placeholder='Area'>
				<input type='text' value='".$data['posaddress_phone_number']."' class='input-mask phone-mask' name='posaddress_phone_number'  id='posaddress_phone_number' maxlength='20' placeholder='Phone number'>
			</div>
		");
	}
	
	$form->posaddress_mobile_phone(
		Form::TYPE_TEXT,
		FORM::TOOLTIP_PHONE,
		Validata::PARAM_PHONE
	);

	if (!$disabled['posaddress_mobile_phone']) {
		$form->extended('posaddress_mobile_phone',"
			<div class='input-mask-container phone-container'>
				<input type='text' value='".$data['posaddress_mobile_phone_country']."' class='input-mask phone-mask' name='posaddress_mobile_phone_country' id='posaddress_mobile_phone_country' maxlength='6' placeholder='Country code'>
				<input type='text' value='".$data['posaddress_mobile_phone_area']."' class='phone-mask' name='posaddress_mobile_phone_area'  id='posaddress_mobile_phone_area' maxlength='6' placeholder='Area'>
				<input type='text' value='".$data['posaddress_mobile_phone_number']."' class='input-mask phone-mask' name='posaddress_mobile_phone_number' id='posaddress_mobile_phone_number' maxlength='20' placeholder='Mobile Phone number'>
			</div>
		");
	}
	
	$form->posaddress_email(
		Form::TYPE_TEXT,
		FORM::TOOLTIP_EMAIL,
		Validata::PARAM_EMAIL
	);
	
	$form->posaddress_website(
		Form::TYPE_TEXT,
		FORM::TOOLTIP_URL,
		Validata::PARAM_URL
	);
	
	$form->posaddress_contact_name(
		Form::TYPE_TEXT
	);
	
	$form->posaddress_contact_email(
		Form::TYPE_TEXT,
		FORM::TOOLTIP_EMAIL,
		Validata::PARAM_EMAIL
	);
	
	$form->posaddress_store_openingdate(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		Validata::CLASS_DATAPICKER
	);

	if (!$id) {
		$form->posaddress_store_openingdate(Validata::PARAM_REQUIRED);
	}
	
	$form->posaddress_store_planned_closingdate(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		Validata::CLASS_DATAPICKER
	);

	
	if($data['posaddress_store_planned_closingdate'] != NULL
		and $data['posaddress_store_planned_closingdate'] != '0000-00-00') {

		$form->planned_closing_change_comment(
			Form::TYPE_TEXT
		);
	
	}
	else {
		$form->planned_closing_change_comment(
			Form::TYPE_HIDDEN
		);
	}
	
	
	
	$form->posaddress_store_closingdate(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		Validata::CLASS_DATAPICKER
	);
	
	$form->posaddress_export_to_web(
		Form::TYPE_CHECKBOX
	);

	$form->posaddress_email_on_web(
		Form::TYPE_CHECKBOX
	);

	$form->posaddress_offers_free_battery(
		Form::TYPE_CHECKBOX
	);

	$form->posaddress_show_in_club_jublee_stores(
		Form::TYPE_CHECKBOX
	);
	
	$form->posaddress_vipservice_on_web(
		Form::TYPE_CHECKBOX
	);
	
	$form->posaddress_birthday_vaucher_on_web(
		Form::TYPE_CHECKBOX
	);
	
	
	// extend contents
	if (!$disabled['posaddress_place_id'] ) {
		$form->extended('posaddress_place_id',"
			<div class='geobox'>
				<div class=row >	
					<span>Province</span>
					<select name=province alt='Province' id=province >
						
					</select>
					<input type=text alt='New Province' name=new_province id=new_province class='required geodata' />
				</div>
				<div class=row >
					<span>New City</span> 
					<input type=text alt='New City' name=new_place id=new_place class='required geodata' >
				</div>
			</div>
		");
	}
	
	// fieldset: client data
	$form->fieldset('client_data', array(
		'posaddress_client_id',
		'posaddress_franchisee_id',
		'posaddress_identical_to_address'
	));
	
	// fieldset: pos data
	$form->fieldset('name_address', array(
		'posaddress_sapnumber',
		'posaddress_sap_shipto_number',
		'posaddress_distribution_channel',
		'posaddress_name', 
		'posaddress_name2', 
		'posaddress_address', 
		'posaddress_address2', 
		'posaddress_country', 
		'posaddress_place_id', 
		'posaddress_zip'
	));
	
	// fieldset: communication
	$form->fieldset('communication', array(
		'posaddress_phone', 
		'posaddress_mobile_phone', 
		'posaddress_email', 
		'posaddress_website', 
		'posaddress_contact_name', 
		'posaddress_contact_email'
	));
	
	// pos dates
	//if (!$id) $fieldset_caption_dates = 'sales_agreement';
	//else $fieldset_caption_dates = ($data['posaddress_store_postype'] == 4) ? 'sales_agreement' : 'dates';
	
	
	if($data['posaddress_store_planned_closingdate'] != NULL
		and $data['posaddress_store_planned_closingdate'] != '0000-00-00') {


		$form->fieldset('dates', array(
			'posaddress_store_openingdate',
			'posaddress_store_planned_closingdate',
			'planned_closing_change_comment',
			'posaddress_store_closingdate'
		));
	
	}
	else {
		$form->fieldset('dates', array(
			'posaddress_store_openingdate',
			'posaddress_store_planned_closingdate',
			'posaddress_store_closingdate'
		));
	}
	
	
	// fieldset: miscellanous
	$form->fieldset('miscellanous', array(
		'posaddress_export_to_web',
		'posaddress_email_on_web',
		'posaddress_offers_free_battery',
		'posaddress_show_in_club_jublee_stores',
		'posaddress_vipservice_on_web',
		'posaddress_birthday_vaucher_on_web'
	));

	if ($data['posaddress_id']) {
		
		$form->created_by(
			Form::TYPE_PLACEHOLDER,
			Form::PARAM_SHOW_VALUE
		);

		$form->modified_by(
			Form::TYPE_PLACEHOLDER,
			Form::PARAM_SHOW_VALUE
		);

		$form->fieldset('Tracking Info', array(
			'created_by',
			'modified_by'
		));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
		
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}

	$form = $form->render();

?>
<script type="text/javascript" src="/public/js/pos.form.js"></script>
<script type="text/javascript" src="/public/scripts/chain/chained.select.js"></script>
<script type="text/javascript" src="/public/scripts/date.js"></script>
<style type="text/css">
	
	#posform {
		width: 700px;
	}
	
	#posaddress_zip {
		width: 100px !important;
	}
	
	#posaddress_store_openingdate,
	#posaddress_store_closingdate,
	#posaddress_store_planned_closingdate {
		width: 100px;
	}
	
	.geobox {
		display: block;
		margin: 10px 0 10px 190px;
		padding: 10px;
		background-color: #f3f3f3;
		width: 460px;
	}
	
	.geobox .row {
		display: block;
	}
	
	.geobox span {
		display: inline-block;
		width: 60px;
		font-size: .8em;
	}
	
	.geobox span:first-child {
		margin-bottom: 10px;
	}
	
	.geobox input {
		width: 150px !important;
	}
	
	.fa-stack.fa-md {
		width: 1.6em;
		height: 1.6em;
		line-height: 1.6em;
	}
	
	.fa-stack.fa-md .fa-stack-2x {
		font-size: 1.6em;
	}
	
	.fa-stack.fa-md .fa-stack-1x {
		font-size: 0.85em;
	}
	
	.letter-control {
		color: silver;
		cursor: pointer;
	}
	
	.letter-control:hover {
		color: gray;
	}

	.accessPosIndex {
		display: block;
		width: 700px;
		text-align: right;
		position: relative;
		bottom: -35px;
		margin-top: -35px;
	}

	.accessPosIndex a {
		font-size: 13px;
	}

	#new_province {
		display: block;
		margin: 0 0 5px 64px;
	}

	.posaddress_phone, .posaddress_mobile_phone, .posaddress_address { 
		position: relative; 
	}
	
	.posaddress_address .-element:not(.-disabled), 
	.posaddress_phone .-element:not(.-disabled), 
	.posaddress_mobile_phone .-element:not(.-disabled) { 
		display: none !important;
	}

	.posaddress_address .-tooltip, 
	.posaddress_phone .-tooltip, 
	.posaddress_mobile_phone .-tooltip {
		position: absolute; left: 500px;
	}

	.input-mask-container { display: inline-block; }
	.input-mask-container input {width: auto;}
	.phone-mask:nth-child(1) { width: 45px!important; }
	.phone-mask:nth-child(2) { width: 40px!important; }
	.phone-mask:nth-child(3) { width: 180px!important; }
	.address-mask:nth-child(1) { width: 240px!important; }
	.address-mask:nth-child(2) { width: 45px!important; }
	
</style>
<?php 
	
	if ($buttons['accessPosIndex']) {
		echo "<div class='accessPosIndex'><a href='{$buttons[accessPosIndex]}' target='_blank'>Access this POS in POS Index</a></div>";
	}

	echo $form; 
?>
	