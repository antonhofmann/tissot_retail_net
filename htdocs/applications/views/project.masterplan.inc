<style type="text/css">
  table.project_masterplan tr.subphase td {
    font-weight: bold;
  }
  table.project_masterplan tbody tr td.col_title {
    background-color: transparent;
  }
  table.project_masterplan td.step_nr {
    border-top: 1px solid #fff;
    text-align: center;
  }
  table.project_masterplan tr.subphase
  + tr td.step_nr {
    border-top-width: 0;
  }
  table.project_masterplan td.step_nr {
    width: 40px;
  }
  table.project_masterplan td.step_title {
    width: 500px;
  }
  table.project_masterplan td.step_value {
    width: 120px;
  }
  table.project_masterplan td.step_duration {
    width: 100px;
  }
  table.project_masterplan table tr td {
    background-color: inherit !important;
  }
  table.project_masterplan table td {
    padding: 5px 0;
  }
  table.project_masterplan table td.label {
    display: inline-block;
    margin-right: 20px;
  }
  
  /**
   * Print styles
   */
  table.project_masterplan.print {
    width: 190mm;
  }
  table.project_masterplan.print td.step_nr {
    width: 5mm;
  }
  table.project_masterplan.print td.step_title {
    width: 70mm;
  }
  table.project_masterplan.print td.step_value {
    width: 30mm;
  }
  table.project_masterplan.print td.step_duration {
    width: 10mm;
  }
  table.project_masterplan.print td,
  table.project_masterplan.print th {
    font-size: 9pt !important;
    padding-top: 5px;
    padding-bottom: 5px;
  }
  table.project_masterplan.print th {
    padding-top: 7px;
    padding-bottom: 7px;
  }

</style>

<form class="default">

  <div class="toolbar">

    <button id="btn-pdf-export" class="button">
      <span class="icon print"></span>
      <span class="label"><?php print $translate->print?></span>
    </button>

  </div>

</form>

<table class="listing project_masterplan">
<tr>
  <td colspan="2" class="col_title"></td>
  <td class="col_title"><?php print $translate->value?></td>
  <td class="col_title"><?php print $translate->duration?></td>
  <td class="col_title"><?php print $translate->responsible?></td>
</tr>
<!-- iterate phases -->
<?php foreach ($phases as $phase): ?>
  <tr><th colspan="5"><?php echo $phase->title?></th></tr>

  <!-- iterate subphases -->
  <?php foreach ($phase->subphases as $subphase): ?>
    <tr class="subphase"><td colspan="5"><?php echo $subphase->title?></td></tr>

    <!-- iterate steps -->
    <?php foreach ($subphase->steps as $step): ?>
      <tr>
        <td class="step_nr" style="background-color: #<?php echo $step['color']?>"><?php print $step['step_nr']; ?></td>
        <td class="step_title"><?php print $step['title']; ?></td>
        <td class="step_value"><?php print strlen($values[$step['value']]) ? $values[$step['value']] : '-'; ?></td>
        <td class="step_duration"><?php print strlen($durations[$step['value']]) ? $durations[$step['value']] : ''; ?></td>
        <td class="step_responsible">
        <?php 
          // this should be done via a subtemplate but we don't have a template engine :(
          if (is_array($responsibles[$step['responsible_role_id']])) {
            // the default case: 1 person is responsible
            if (isset($responsibles[$step['responsible_role_id']]['name'])) {
              print $responsibles[$step['responsible_role_id']]['name'];
            }
            // array of responsibility holders
            else {
              print '<table>';
              foreach ($responsibles[$step['responsible_role_id']] as $responsible) {
                printf('<tr><td class="label">%s</td><td>%s</td></tr>',
                  $responsible['date'],
                  $responsible['name']
                  );
              }
              print '</table>';
            }
          }
        ?>
        </td>
      </tr>
    <?php endforeach; ?>
  <?php endforeach; ?>
<?php endforeach; ?>
</table>


<script>
  require([
    'jquery',
    'pdf_printer',
    'views/spin'], 
    function($, PdfPrinter, Spin) {
      var projectID = <?php print $projectID?>;
      var paperSize = 'A4';
      // preload spinner
      var Spinner = new Spin({template: 'rotating.plane'});

      $('#btn-pdf-export').click(function(e) {
        e.preventDefault();
        Spinner.render().show();

        var $el = $('table.listing.project_masterplan')
          .addClass('print');

        // send masterplan table to server for pdf printing
        PdfPrinter.on('afterPrint', function() { 
          $el.removeClass('print');
          Spinner.hide(); 
        });
        PdfPrinter.print($el, {
          proxyURL: '/project/projectmasterplan/export',
          fileName: projectID,
          landscape: false,
          paperSize: 'A4'
        });
      });
  });
</script>