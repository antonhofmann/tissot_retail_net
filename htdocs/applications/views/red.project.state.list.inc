<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<link href="/public/scripts/dropdown/dropdown.css" rel="stylesheet" type="text/css" />
<script src="/public/scripts/dropdown/dropdown.js" type="text/javascript"  ></script>
<script type="text/javascript" src="/public/scripts/table.loader.js" ></script>
<script type="text/javascript">
$(document).ready(function() {

	$('#project_states').tableLoader({
		url: '/applications/helpers/red.project.state.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});

});
</script>
<style type="text/css">

	#project_states { 
		width: 500px; 
	}

	span.icon {
		display: block;
		width: 20px;
		height: 20px;
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="project_states"></div>