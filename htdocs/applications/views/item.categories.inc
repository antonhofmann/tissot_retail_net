<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#itemCategories').tableLoader({
		url: '/applications/helpers/item.categories.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			$('select',self).dropdown();

			$("tbody", self).sortable({
				placeholder: "ui-state-highlight",         
		        scroll: true,
		        handle: ".row-order",
		        items: "> tr",
		        update: function( event, ui ) {
		        	
		        	$("tr:odd", self).removeClass('-odd').removeClass('-even').addClass('-odd');
		        	$("tr:even", self).removeClass('-odd').removeClass('-even').addClass('-even');

		        	var orders = [];
					
					$('.row-order', self).each( function () {
						orders.push($(this).data('id'));
					});

					if (orders.length) {
						retailnet.ajax.json('/applications/helpers/item.categories.ajax.php', {
							section: 'order',
							orders: orders.join(',')
						}).done(function(xhr) {
							if (xhr && xhr.message) {
								if (xhr.success==1) retailnet.notification.success(xhr.message);
								else retailnet.notification.error(xhr.message);
							}
						});
					}
		        }
			});
		}
	});
	
});
</script>
<style type="text/css">

	#itemCategories { 
		width: 600px; 
	}

	.row-order {
		border-right: 0 !important;
		cursor: pointer;
	}

	.row-order:hover {
		color: blue;
	}

	.ui-state-highlight { background: #fffeee !important; }
	
</style>
<?php 
	echo $request->form();
?>
<div id="itemCategories"></div>