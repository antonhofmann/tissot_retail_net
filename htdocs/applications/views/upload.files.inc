<?php 

	if ($dataloader) {
		
		foreach ($dataloader as $file) {
			$rows .= '
				<div class="file-row">
					<div class="file-title">
						<input type="text" name="titles[]" class="title required validate[required]" placeholder="File Title" value="'.$file['title'].'" />
					</div>
					<div class="file-actions">
						<a class="button show-file active" target="_blank" href="'.$file['link'].'">
							<span class="icon picture"></span>
							<span class="label">Show</span>
						</a>
						<span class="button remove-file active">
							<span class="icon delete"></span>
							<span class="label">Remove</span>
						</span>
					</div>
				</div>
			';
		}
	}

?>
<form id="uplForm" enctype="multipart/form-data" method="post" class="<?php echo $class ?>" action="<?php echo $action ?>">
	<span class="btn-ajax-upload"></span>
	<input type="hidden" name="entity" id="entity" value="<?php echo $entity ?>" />
	<input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
	<input type="hidden" name="allowed_extensions" id="allowed_extensions" value="<?php echo $allowed_extensions ?>" />
	<?php echo $rows ?>
	<div class="file-row">
		<input type="hidden" name="files[]" class="file" />
		<div class="file-title">
			<input type="text" name="titles[]" class="title required validate[required]" placeholder="File Title" />
		</div>
		<div class="file-actions">
			<span class="button upload-file">
				<span class="icon upload"></span>
				<span class="label">Upload</span>
			</span>
			<a class="button show-file" target="_blank">
				<span class="icon picture"></span>
				<span class="label">Show</span>
			</a>
			<span class="button remove-file">
				<span class="icon delete"></span>
				<span class="label">Remove</span>
			</span>
		</div>
	</div>
</form>