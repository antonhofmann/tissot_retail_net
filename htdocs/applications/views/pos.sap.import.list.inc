<script type="text/javascript">
$(document).ready(function() {

	var sapImport = $('#saplist').tableLoader({
		url: '/applications/helpers/pos.sap.import.list.php',
		data: $('.request').serializeArray(),
		after: function(self, page) {

			$('select',self).dropdown();

			$('.btn-ignore').on('click', function() {

				var row = $(this).closest('tr');
				var id = $(this).data('id');

				retailnet.ajax.json('/applications/helpers/pos.sap.import.update.php', {
					ignore: id,
					list: 1
				}).done(function(xhr) {
					
					if (xhr && xhr.response) { 
						//row.remove(); 
						sapImport.showPage(page);
					}
					
					if (xhr && xhr.message) {
						if (xhr.response) retailnet.notification.success(xhr.message);
						else retailnet.notification.error(xhr.message);
					}
				});
			});

			$('#btn-ignore-matching').on('click', function(e) {

				e.preventDefault();

				var self = $(this);

				retailnet.ajax.json('/applications/helpers/pos.sap.ignore.matching.php', 
					$('form.request, form.toolbox').serializeArray() 
				).done(function(xhr) {
					
					if (xhr && xhr.success) { 
						$('#countries').val('');
						$('#search').val('');
						$('#state').val('');
						sapImport.showPage(1);
						self.remove();
					}
					
					if (xhr && xhr.message) {
						if (xhr.success) retailnet.notification.success(xhr.message);
						else retailnet.notification.error(xhr.message);
					}
				});

				return false;
			})
		}
	});

});
</script>
<style type="text/css">
	
	#saplist { width: 1200px; }

	

	.fa-stack {
		font-size: 14px !important;
	}

	.fa-circle {
		color: gray;
	}

	.fa-stack-1x {
		font-size: 16px;
		font-weight: 600;
	}

	.fa-frown-o {color: white; }
	.fa-smile-o {color: #00ff00 ;}	
	.fa-meh-o { color: #fff000; }
	
</style>
<div id="saplist"></div>
<?php 
	echo Request::instance()->form();
?>
