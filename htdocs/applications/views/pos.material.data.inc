<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'posltm',
		'action' => '/applications/helpers/pos.material.save.php',
		'method' => 'post'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_pos_material_posaddress(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_pos_material_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_material_category_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_pos_material_material_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED,
		Form::PARAM_AJAX
	);
	
	$form->mps_pos_material_quantity_in_use(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		Form::TOOLTIP_INTEGER,
		Validata::PARAM_ONLY_NUMBER
	);
	
	$form->mps_pos_material_quantity_on_stock(
		Form::TYPE_TEXT,
		Form::TOOLTIP_INTEGER,
		Validata::PARAM_ONLY_NUMBER
	);
	
	$form->mps_pos_material_installation_year(
		Form::TYPE_TEXT,
		Form::TOOLTIP_INTEGER, 
		Validata::PARAM_YEAR
	);
	
	$form->mps_pos_material_deinstallation_year(
		Form::TYPE_TEXT,
		Form::TOOLTIP_INTEGER,
		Validata::PARAM_YEAR
	);
	
	$form->fieldset('long_term_materials', array(
		'mps_material_category_id', 
		'mps_pos_material_material_id'
	));
	
	$form->fieldset('equipment', array(
		'mps_pos_material_quantity_in_use',
		'mps_pos_material_quantity_on_stock', 
		'mps_pos_material_installation_year',
		'mps_pos_material_deinstallation_year'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript" src="/public/scripts/chain/chained.select.js"></script>
<script type="text/javascript" src="/public/js/pos.material.js"></script>
<style type="text/css">
	
	#posltm { 
		width: 740px; 
	}
	
	form .equipment input { 
		width: 120px; 
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>