<div class="compare-list">
<?php 
	
		// confirmed wanings
		$warningConfirmedMessages = array(
			1 => "Sales organization unknown",
			2 => "SAP number sold to unknown",
			3 => "SAP number ship  to unknown",
			4 => "PO-number unknown",
			5 => "PO-line number unknown",
			6 => "EAN number unknown"
		);
	
		// shipped warnings
		$warningShippedMessages = array(
			1 => "SAP order number unknown",
			2 => "SAP order line number unknown",
			3 => "EAN number unknown",
			4 => "PO-number unknown",
			5 => "SAP number sold to unknown"
		);
			
	if ($datagrid) {
		
		foreach ($datagrid as $k => $row) {
			
			echo "<div class='compare-caption'>{$row['caption']}</div>";
			
			foreach ($row['items'] as $i => $item) {
				
				echo "<div class='compare-group'>";
				
				echo "<div class='compare-group-caption'>{$item['caption']}</div>";
				
				foreach ($item['distributions'] as $d => $distribution) {
					
					$orders = $data['orders'][$d];
					$confirmed = $data['confirmed'][$d];
					$shipped = $data['shipped'][$d];

					echo "<table>";
					
					echo "<thead>";
					echo "<th>Exported on: {$distribution['date']}</th>";
					echo "<th width='23%'>SAP Sales Order</th>";
					echo "<th width='23%'>SAP Confirmed Item</th>";
					echo "<th width='23%'>SAP Shipped Item</th>";
					echo "</thead>";
					
					echo "<tbody>";
					
					echo "<tr  class='odd'>";
						echo "<td>Quantity</td>";
						echo "<td>{$orders['quantity']}</td>";
						echo "<td>{$confirmed['quantity']}</td>";
						echo "<td>{$shipped['quantity']}</td>";
					echo "</tr>";
					
					echo "<tr>";
						echo "<td>SAP Number</td>";
						echo "<td>{$orders['sap']}</td>";
						echo "<td>{$confirmed['sap']}</td>";
						echo "<td>{$shipped['sap']}</td>";
					echo "</tr>";
					
					echo "<tr  class='odd'>";
						echo "<td>Shipto Number</td>";
						echo "<td>{$orders['ship']}</td>";
						echo "<td>{$confirmed['ship']}</td>";
						echo "<td>{$shipped['ship']}</td>";
					echo "</tr>";
					
					echo "<tr>";
						echo "<td>EAN Number</td>";
						echo "<td>{$orders['ean']}</td>";
						echo "<td>{$confirmed['ean']}</td>";
						echo "<td>{$shipped['ean']}</td>";
					echo "</tr>";
					
					echo "<tr class='odd'>";
						echo "<td>Purchase Order Number</td>";
						echo "<td>{$orders['pon']}</td>";
						echo "<td>{$confirmed['pon']}</td>";
						echo "<td>{$shipped['pon']}</td>";
					echo "</tr>";
					
					echo "<tr>";
						echo "<td>PO Line Number</td>";
						echo "<td>{$orders['line']}</td>";
						echo "<td>{$confirmed['line']}</td>";
						echo "<td>{$shipped['line']}</td>";
					echo "</tr>";
					
					echo "<tr class='odd'>";
						echo "<td>SAP Sales Order Number</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>{$confirmed['order_number']}</td>";
						echo "<td>{$shipped['order_number']}</td>";
					echo "</tr>";
					
					echo "<tr>";
						echo "<td>SAP Sales Order Position</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>{$confirmed['order_line_number']}</td>";
						echo "<td>{$shipped['order_line_number']}</td>";
					echo "</tr>";
					
					echo "<tr class='odd'>";
						echo "<td>Message</td>";
						echo "<td>Imported on: {$orders['date']}</td>";
						echo "<td class='compare-error'>{$warningConfirmedMessages[$confirmed['code']]}</td>";
						echo "<td class='compare-error'>{$warningShippedMessages[$shipped['code']]}</td>";
					echo "</tr>";
					
					echo "</tbody>";
					
					echo "</table>";
				}
				
				echo "</div>";
			}
		}
	}
?>
</div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => translate::instance()->back
	));
?>
</div>


