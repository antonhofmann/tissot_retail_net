<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();
	
	$form = new Form(array(
		'id' => 'maintenance',
		'action' => '/applications/helpers/maintenance.save.php',
		'method' => 'post'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->maintenance_window_id(
		Form::TYPE_HIDDEN
	);
	
	$form->maintenance_window_start(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		Validata::PARAM_DATE,
		"class=datepicker"
	);
	
	$form->extended('maintenance_window_start', "<input type=text name=maintenance_window_start_hours class='hours' value='".$data['maintenance_window_start_hours']."' >");
	
	$form->maintenance_window_stop(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		Validata::PARAM_DATE,
		"class=datepicker"
	);
	
	$form->maintenance_window_active(
		Form::TYPE_CHECKBOX
	);
	
	$form->extended('maintenance_window_stop', "<input type=text name=maintenance_window_stop_hours class='hours' value='".$data['maintenance_window_stop_hours']."' >");
	
	$form->fieldset('maintenance', array(
		'maintenance_window_start',
		'maintenance_window_stop',
		'maintenance_window_active'
	));
	
	$form->maintenance_window_warning_start_date(
		Form::TYPE_TEXT,
		Validata::PARAM_DATE,
		"class=datepicker"
	);
	
	$form->extended('maintenance_window_warning_start_date', "<input type=text name=maintenance_window_warning_start_date_hours class='hours' value='".$data['maintenance_window_warning_start_date_hours']."' >");
	
	$form->maintenance_window_warning_content(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);
	
	$form->fieldset('warning', array(
		'maintenance_window_warning_start_date',
		'maintenance_window_warning_content'
	));
	
	$form->maintenance_window_title(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->maintenance_window_content(
		Form::TYPE_TEXTAREA,
		Validata::PARAM_REQUIRED,
		Form::PARAM_LABEL
	);
	
	$form->fieldset('content', array(
		'maintenance_window_title',
		'maintenance_window_content'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'label' => $translate->delete,
			'href' => $buttons['delete']
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#maintenance");

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			onAjaxFormComplete: function(status, form, json, options) {

				retailnet.loader.hide();
				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

				if (json) {

					if (json.response && json.redirect) {
						window.location=json.redirect;
					}

					if (json.message) {
						$.jGrowl(json.message,{ 
							sticky: (json.response) ? false : true, 
							theme: (json.response) ? 'message' : 'error'
						});
					}
				}
			}
		});
		
		// button: submit form
		$("#save").click(function(event) {

			// form errors
			var errors = form.find('.error').length;
			var valid = form.validationEngine('validate');
			
			if (errors || !valid) {
				$.jGrowl('Check red marked fields.',{ 
					sticky: true, 
					theme: 'error'
				});
			}
			else {
				retailnet.loader.show();
				form.submit();
			}
			
		});

		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});

		// dialog confirmation
		$('.dialog').click(function(event) {
			
			event.preventDefault();

			var button = $(this);
			
			$('#apply, a.apply').attr('href', button.attr('href'));
			
			retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
		});

		$('#cancel, #versionCancel').bind('click', function(event) {
			event.preventDefault();
			retailnet.modal.close();
			return false;
		});	

		$('#apply, #versionApply').click(function(event) {
			if ($(this).hasClass('disabled')) {
				return false;
			}
		})

		$('.datepicker').datepicker({ 
			dateFormat: 'dd.mm.yy' ,
			showOtherMonths : true,
			firstDay: 1,
			onClose: function(dateText, inst) { 
				var error = (dateText && !isValidDate(dateText)) ? true : false;
				$(this).removeClass('error').toggleClass('error', error);
			}
		});
	});
</script>
<style type="text/css">

	#maintenance { 
		width: 700px; 
	}
	
	#maintenance textarea { 
		width:440px; 
		height: 160px;
	}
	
	form input.datapicker {
		width:100px !important;
	}
	
	input.hours {
		width: 40px !important;
		margin-left: 10px;
	}
	
</style>
<?php 
	
	echo $form; 

	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			)
		)
	));
?>