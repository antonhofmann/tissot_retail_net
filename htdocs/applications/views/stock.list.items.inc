<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'stocklist',
		'action' => '/applications/helpers/stock.list.item.save.php',
		'method' => 'post'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->scpps_stocklist_id(
		Form::TYPE_HIDDEN
	);

	$form->editabled_fields(
		Form::TYPE_HIDDEN
	);
	
	$form->stocklist_items(
		Form::TYPE_AJAX,
		FORM::PARAM_SHOW_VALUE
	);
	
	$form->fieldset('items', array(
		'stocklist_items'
	)); 

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#stocklist");
		var id = $('#scpps_stocklist_id');
		var checkall = $('input.checkall');
		var items = $('input.items');

		// checkbox: select all
		checkall.change(function() {
			var attr = $(this).is(':checked') ? 'checked' : false;
			items.attr('checked', attr);
		});
		
		// checkbox: items
		items.change(function() {
			var attr = (items.length == $('input.items:checked').length) ? 'checked' : false;
			checkall.attr('checked', attr);
		});
		
		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			beforeSend: function () {
				retailnet.loader.show();
			},
			onAjaxFormComplete: function(status, form, json, options) {

				retailnet.loader.hide();
				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

				if (json.response && json.redirect) {
					window.location=json.redirect;
				}
				
				if (json.message) {
					$.jGrowl( json.message, { 
						sticky: (json.response) ? false : true,
						theme: (json.response) ? 'message' : 'error'
					});	
				}
			}
		});
		
		$("#save").click(function(event) { 
			event.preventDefault();
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
			form.submit();
		});

		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});

		$('.dialog').click(function(event) {
			
			event.preventDefault();

			var button = $(this);
			
			$('#apply, a.apply').attr('href', button.attr('href'));
			
			retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
		});

		$('#cancel').bind('click', function(event) {
			event.preventDefault();
			retailnet.modal.close();
			return false;
		});	

	});
</script>

<style type="text/css">

	#stocklist { 
		width: 700px; 
	}
	
	.stocklist_items label {
		display: none !important;
	}
	
	.-placeholder {
		font-size: 1em !important;;
	}
	
	.selectall {
		font-weight: 600;
		margin-bottom: 20px;
	}
	
	form .stocklist_items .-checkbox {
		margin-left: 80px;
	}
	
	input.items {
		margin-left: 5px;
	}
	
</style>
<?php 
	
	echo $form;
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>