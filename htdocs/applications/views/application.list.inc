<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	var applications = $('#applications').tableLoader({
		url: '/applications/helpers/application.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">
	#applications { width: 800px; }
</style>
<?php 
	echo $request->form();
?>
<div id="applications"></div>