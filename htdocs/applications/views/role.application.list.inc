<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#role_applications').tableLoader({
		url: '/applications/helpers/role.application.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});

	$(document).delegate("input.application", "click", function(event) {

		event.stopPropagation();

		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

		var data = { 
			role: $('#id').val(),
			application: $(this).val(),
			action: ($(this).is(':checked')) ? 1 : 0
		};

		var request = $.getJSON('/applications/helpers/role.application.save.php', data, function(json) {
			if (json) {
				$.jGrowl(json.message,{ 
					sticky: false, 
					theme: (json.response) ? 'message' : 'error'
				});
			}
		});
	});
	
});
</script>
<style type="text/css">
	
	#role_applications { 
		width: 500px; 
	}
	
</style>
<?php 
	echo $request->form($requestFields);
?>
<div id="role_applications"></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>