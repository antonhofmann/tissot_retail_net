<?php 
	$translate = Translate::instance();
	$request = request::instance();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var poslist = $('#poslist').tableLoader({
			url: '/applications/helpers/pos.verification.list.php',
			data: $('.request').serializeArray(),
			after: function(self) {
				$('.selectbox',self).dropdown();
			}
		});
	});
</script>
<style type="text/css">
	
	#poslist {
		width: 1200px;
	}
	
</style>
<div id="poslist"></div>
<?php 
	echo $request->form();
?>