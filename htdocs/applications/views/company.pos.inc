<?php 

	$settings = Settings::init();
	$user = User::instance();
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#posLocations').tableLoader({
		url: '/applications/helpers/company.pos.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select', self).dropdown();
		}
	});

});
</script>
<style type="text/css">

	#posLocations { 
		width: 1000px; 
	}
	
	p.franchisee { 
		font-size: .8em; 
		font-weight:700; 
		margin: 40px 0;
	}
	
</style>
<?php 
	echo $request->form();
?>
<p class=franchisee>The Company is Franchisee (Owner) of the following POS Locations</p>
<div id="posLocations"></div>
<div class='actions'>
<?php 
	if ($buttons['back']) {
		echo ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => $translate->back
		));
	}
?>
</div>