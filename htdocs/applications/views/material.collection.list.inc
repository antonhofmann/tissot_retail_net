<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>

<script type="text/javascript">
$(document).ready(function() {

	$('#collections').tableLoader({
		url: '/applications/helpers/material.collection.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select', self).dropdown();
		}
	});	
	
});
</script>
<style type="text/css">

	#collections { 
		width: 800px; 
	}
	
</style>
<?php echo $request->form($requestFields); ?>
<div id="collections"></div>