<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();

	$form = new Form(array(
		'id' => 'form',
		'action' => '/applications/helpers/item.addon.save.php',
		'method' => 'post',
		'class' => 'validator'
	));

	$form->redirect(
		Form::TYPE_HIDDEN
	);

	$form->application(
		Form::TYPE_HIDDEN
	);

	$form->controller(
		Form::TYPE_HIDDEN
	);

	$form->action(
		Form::TYPE_HIDDEN
	);

	$form->addon_id(
		Form::TYPE_HIDDEN
	);	

	$form->addon_parent(
		Form::TYPE_HIDDEN
	);

	$form->addon_child(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->addon_package_quantity(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		Form::TOOLTIP_NUMERIC,
		Validata::PARAM_NUMBER,
		'class=numeric'
	);	

	$form->addon_min_packages(
		Form::TYPE_TEXT,
		Form::TOOLTIP_NUMERIC,
		Validata::PARAM_NUMBER,
		'class=numeric'
	);

	$form->addon_max_packages(
		Form::TYPE_TEXT,
		Form::TOOLTIP_NUMERIC,
		Validata::PARAM_NUMBER,
		'class=numeric'
	);

	$form->fieldset('Addon', array(
		'addon_child',
		'addon_package_quantity',
		'addon_min_packages',
		'addon_max_packages'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}

	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}

	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}

	$form->dataloader($data);
	$form->dataloader($dataloader);
	
	echo $form->render();


	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			)
		)
	));