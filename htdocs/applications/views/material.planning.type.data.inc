<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'planningtype',
		'action' => '/applications/helpers/material.planning.type.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_material_planning_type_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_material_planning_type_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_material_planning_type_description(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);

	$form->mps_material_planning_is_dummy(
		Form::TYPE_CHECKBOX
	);

	$form->mps_material_planning_ordersheet_standrad_dummy_quantity(
		Form::TYPE_CHECKBOX
	);

	$form->mps_material_planning_ordersheet_approvment_automatically(
		Form::TYPE_CHECKBOX
	);

	$form->mps_material_planning_ordersheet_distrubution_automatically(
		Form::TYPE_CHECKBOX
	);
	
	// fieldset: dimensions
	$form->fieldset('planning_type', array(
		'mps_material_planning_type_name',
		'mps_material_planning_type_description',
		'mps_material_planning_is_dummy',
		'mps_material_planning_ordersheet_standrad_dummy_quantity',
		'mps_material_planning_ordersheet_approvment_automatically',
		'mps_material_planning_ordersheet_distrubution_automatically'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<style type="text/css">

	#planningtype { 
		width: 700px; 
	}
	
	#mps_material_planning_type_description { 
		width: 440px; 
		height: 80px;
	}
	
</style>
<?php 

	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>