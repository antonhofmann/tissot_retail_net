<style type="text/css">

	.message {
		width: 800px;
		margin-bottom: 40px;
	}
	
	.message .-title {
		margin:0;
		color: black;
		font-size: 24px;
	}
	
	.message .-content {
		font-size: 14px;
		line-height: 20px;
	}
	
	.contdown {
		width: 800px;
		height: 18px;
		line-height: 18px;
		color: #444;
		font-size: 12px;
		font-weight: 300;
		margin-bottom: 40px;
		border: 0 !important;
		background: none;
	}

</style>
<script type="text/javascript">
$(function () {
	$('.contdown').each(function(i,elem) {

		var s = $(elem).attr('data-stop')*1000;
		var d = new Date(s); 
		
		$(elem).countdown({
			until: d, 
			compact: true, 
		    layout: 'Estimated Time Remaining: <b>{dn}{dl} {hnn}{sep}{mnn}{sep}{snn}</b>',
		    expiryUrl: '/mps'
		});
	});
});
</script>
<?php 

	if ($maintenance) {
		
		foreach ($maintenance as $row) {
			
			$title = $row['maintenance_window_title'];
			$content = nl2br($row['maintenance_window_content']);
			$stop = $row['stop_timestamp'];
			
			echo "
				<div class='message'>
					<div class='-title'>$title</div>
					<div class='contdown' data-stop='$stop' ></div>
					<div class='-content'>$content</div>
				</diV>		
			";
		}
	}