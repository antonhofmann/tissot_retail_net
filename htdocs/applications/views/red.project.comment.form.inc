<?php

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$form = new Form(array(
		'id' => 'commentForm',
		'action' => '/applications/helpers/red.project.comment.save.php',
		'method' => 'post'
	));
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);

	$form->red_comment_id(
		Form::TYPE_HIDDEN
	);
	
	$form->red_comment_project_id(
		Form::TYPE_HIDDEN
	);
	
	$form->red_comment_file_id(
		Form::TYPE_HIDDEN
	);

	$form->red_comment_owner_user_id(
		Form::TYPE_HIDDEN
	);

	$form->red_comment_category_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->red_comment_comment(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL,
		Validata::PARAM_REQUIRED
	);

	$form->partners(
		Form::TYPE_CHECKBOX_GROUP
	);

	$form->fieldset('comment', array(
		'red_comment_category_id',
		'red_comment_comment'
	));
	
	$form->fieldset('access', array(
		'partners'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}

	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}

	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['attach']) {
		$form->button(ui::button(array(
			'id' => 'attach',
			'icon' => 'attachment',
			'label' => "Attach File"
		)));
	}
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}

	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form =  $form->render();
	
?>
<link rel="stylesheet" href="/public/scripts/validationEngine/css/validationEngine.jquery.css" type="text/css"/>
<script src="/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="/public/scripts/validationEngine/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="/public/scripts/ajaxuploader/ajaxupload.js"  ></script>
<script type="text/javascript" src="/public/scripts/chain/chained.select.js"  ></script>
<link rel="stylesheet" type="text/css" href="/public/scripts/loader/loader.css" />
<script type="text/javascript" src="/public/scripts/loader/loader.js"  ></script>
<script type="text/javascript" src="/public/scripts/table.loader.js" ></script>
<script type="text/javascript">
$(document).ready(function() {

	var commentForm = $('#commentForm'),
		fileForm = $('#fileForm'),
		commentFiles = $('#red_comment_file_id'),
		filePath = $('#red_file_path'),
		fileTitle = $('#red_file_title'),
		btnUploadFile = $('#upload'),
		hasUpload = $('#has_upload'),
		application = $('#application'),
		highlight = $('.highlight').hide();

	
	// comment form validator
	commentForm.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, comment_form, json, options) {

			retailnet.loader.hide();
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

			if (json) {

				if (json.response && json.redirect) {
					window.location=json.redirect;
				}

				if (json.message) {
					$.jGrowl(json.message,{ 
						sticky: (json.response) ? false : true, 
						theme: (json.response) ? 'message' : 'error'
					});
				}
			}
		}
	});

	
	// file form validator
	fileForm.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, file_form, json, options) {

			retailnet.loader.hide();
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

			if (json.response) {
				
				// comment file identificators
				if (json.id) {
					var data = commentFiles.val().split(',') || [];
					data.push(json.id);
					commentFiles.val(data.join(','));
				}

				retailnet.modal.close();
				
				// save comment form
				commentForm.submit();
			}

			if (json.message) {
				$.jGrowl(json.message, { 
					sticky: (json.response) ? false : true, 
					theme: (json.response) ? 'message' : 'error'
				});
			}
		}
	});

	
	// button: submit comment form
	$("#save").click(function(event) {

		// form errors
		var errors = commentForm.find('.error').length;
		var valid = commentForm.validationEngine('validate');
		
		if (errors || !valid) {
			$.jGrowl('Check red marked fields.',{ 
				sticky: true, 
				theme: 'error'
			});
		}
		else {
			retailnet.loader.show();
			commentForm.submit();
		}
		
	});

	// submit fileform
	$('#applyModal').click(function(event) {

		event.preventDefault();

		// close modals
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
		retailnet.loader.hide();

		// form errors
		var errors = fileForm.find('.error').length;
		var valid = fileForm.validationEngine('validate');

		// check file path
		if (!filePath.val()) {
			btnUploadFile.addClass('error');
			errors = true;
		}

		if (errors || !valid) {
			$.jGrowl('Check red marked fields.',{ 
				sticky: true, 
				theme: 'error'
			});
		}
		else {
			retailnet.loader.show();
			fileForm.submit();
		}
	});

	
	// button: dialogbox and modalbox cancel
	$('#cancel, #cancelModal').click(function(event) {
		event.preventDefault();
		retailnet.modal.close();
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
		retailnet.loader.hide();
		fileForm.validationEngine('hideAll');
	});


	// button: dialogbox apply
	$('#apply').click(function(event) {
		event.preventDefault();
		window.location=$('#delete').attr('href');
	})

	
	// button: attach file form
	$('#attach').click(function(event) {

		event.preventDefault();

		var modalbox = $('#modalbox');
		var valid = commentForm.validationEngine('validate');

		if (valid) {

			fileForm.find('.error').removeClass('error');
			$('.label', btnUploadFile).text('Upload File');

			retailnet.modal.show('#modalbox', {
				autoSize	: false,
				closeClick	: false,
				closeBtn	: false,
				fitToView   : true,
			    width       : '90%',
			    height      : '90%',
			    maxWidth	: '700px',
			    maxHeight	: '700px',
				modal 		: true,
				afterLoad: function() {
					
					// reset file form partners
					fileForm.find('.partners input:checkbox').attr('checked', false);
					
					commentForm.find('.partners input:checkbox:checked').each(function() {
					    name = $(this).attr('name');
					    fileForm.find('.partners input:checkbox:[name="'+name+'"]').attr('checked', true);
					}); 
				},
				afterClose: function() {
					$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
					retailnet.loader.hide();
				}
			});
		}
	});

	
	// dialog box
	$('.dialog').click(function(event) {

		event.preventDefault();

		var button = $(this);
		
		$('#apply, a.apply').attr('href', button.attr('href'));
		
		retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
	});

	


	new AjaxUpload( btnUploadFile, {
		action: '/applications/helpers/ajax.file.upload.php',
		name: 'userfile',
		data: {
			application: application.val(),
			checkExtension: true
		},
		onSubmit: function(file, ext) {
			
		},
		onComplete: function(file, response) {

			var json = $.parseJSON(response);

			// close modal screens
			retailnet.loader.hide();
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
			highlight.hide();
			
			if(json.response) {
				
				var path = $("<div/>").html(json.path).text();
				
				filePath.val(path);
				hasUpload.val(1);
				btnUploadFile.removeClass('error');

				if (json.button) {
					btnUploadFile.text(json.button);
				}
			} 
			else if (json.message) {
				$.jGrowl(json.message, { 
					sticky: true, 
					theme:'error'
				});
			}
		}
	});

});
</script>
<style type="text/css">
	
	#commentForm { 
		width: 700px; 
	}

	#commentForm textarea {
		width: 480px;
		height: 120px;
	}

	#fileForm textarea {
		width: 380px;
	}


	
	.partners label {
		display: none !important;
	}

	.checkbox-group {
		margin-left: 20px;
	}
	
	.modalbox {
		width: 700px;
	}
	
	.modalbox-content {
		max-height: 500px;
		overflow-y: auto;
	}
	
	#cancelModal {
		float: left;
	}


</style>
<?php 
	
	echo $form; 
	
	// dialog box
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));

	// upload buttn
	$datafile['file'] = '
		<span id="upload" class="button">
			<span class="icon upload"></span>
			<span class="label">'.$translate->upload_file.'</span>	
		</span>
		<span class="highlight" ></span>
		<input type=hidden name="red_file_path" id="red_file_path" />
		<input type="hidden" name="has_upload" id="has_upload" />
	';

	
	// file form
	$form = new Form(array(
		'id' => 'fileForm',
		'action' => '/applications/helpers/red.project.file.save.php',
		'method' => 'post'
	));

	$form->return_inserted_id(
		Form::TYPE_HIDDEN
	);

	$form->red_file_project_id(
		Form::TYPE_HIDDEN
	);

	$form->red_file_owner_user_id(
		Form::TYPE_HIDDEN
	);

	$form->red_file_category_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->red_file_title(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);

	$form->red_file_description(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);

	$form->file(
		Form::TYPE_AJAX,
		FORM::PARAM_SHOW_VALUE
	);

	$form->partners(
		Form::TYPE_CHECKBOX_GROUP
	);

	// fieldset definition
	$form->fieldset('file', array(
		'red_file_category_id',
		'red_file_title',
		'red_file_description',
		'file'
	));

	$form->fieldset('access', array(
		'partners'
	));

	//Fill form with data (attach)
	if ($data['red_file_owner_user_id'] == '')
		$data['red_file_owner_user_id'] = $user->id;

	//Replace red_comment_file_id with ref_file _id of $table_data
	$ids = _array::extract_by_key($table_data, 'red_file_id');
	if ($ids != '') {
		$data['red_comment_file_id'] = implode(',', _array::extract_by_key($table_data, 'red_file_id'));
	}

	//Unserialize file ids and make comma sep list
	$id_array = unserialize($data['red_comment_file_id']);
	if (is_array($id_array)) {
		$data['red_comment_file_id'] = implode(',', $id_array);
	}
	else {
		$data['red_comment_file_id'] = '';
	}

	$form->dataloader($datafile);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<div class='modalbox-container'>
	<div id="modalbox" class="modalbox modal">
		<div class=modalbox-header >
			<div class='title'>Attach File</div>
			<div class='subtitle'><strong></strong></div>
		</div>
		<div class=modalbox-content-container >
			<div class=modalbox-content>
				<?php echo $form; ?>
			</div>
		</div>
		<div class=modalbox-footer >
			<div class=modalbox-actions >
				<a id=cancelModal class='button'>
					<span class="icon cancel"></span>
					<span class="label"><?php echo $translate->cancel ?></span>
				</a>
				<a id="applyModal" class='button'>
					<span class="icon save"></span>
					<span class="label"><?php echo $translate->save ?></span>
				</a>
			</div>
		</div>
	</div>
</div>