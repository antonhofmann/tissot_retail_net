<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	var section = $('#filter').val();

	$('#materials').tableLoader({
		url: '/applications/helpers/material.'+section+'.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">

	#materials { 
		width: 1400px; 
	}
	
</style>
<?php echo $request->form($requestFields); ?> 
<div id="materials" ></div>