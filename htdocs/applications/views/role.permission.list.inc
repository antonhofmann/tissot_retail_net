<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#role_permissions').tableLoader({
		url: '/applications/helpers/role.permission.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select', self).dropdown();
		}
	});

	$('#role_permissions').delegate('.permission_box', 'click', function(event) {

		event.stopPropagation();
		
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

		var role = $('#id').val();
		var permission = $(this).attr('value');
		var action = ($(this).is(':checked')) ? 1 : 0;

		var request = $.getJSON('/applications/helpers/role.permission.save.php', { 
			role: role,
			permission: permission,
			action: action
		});

		request.success(function(json){
			if (json.response) $.jGrowl(json.message,{ sticky: false, theme:'message'});
			else if (json.message) $.jGrowl(json.message,{ sticky: true, theme:'error'});
		});
	});

});
</script>
<style type="text/css">

	#role_permissions { 
		width: 600px; 
	}
	
</style>
<?php 
	echo $request->form($requestFields);
?>
<div id="role_permissions"></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>