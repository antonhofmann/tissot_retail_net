<?php

	$user = User::instance();
	$translate = Translate::instance();

	$form = new Form(array(
		'id' => 'companyform',
		'action' => '/applications/helpers/company.save.php',
		'method' => 'post'
	));

	$form->address_id(
		Form::TYPE_HIDDEN
	);

	$form->address_type(
		Form::TYPE_HIDDEN
	);

	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);

	$form->redirect(
		Form::TYPE_HIDDEN
	);

	$form->address_showinposindex(
		Form::TYPE_HIDDEN
	);
	
	$form->address_province_id(
		Form::TYPE_HIDDEN
	);

	$form->address_number(
		Form::TYPE_TEXT
	);
	
	$form->address_legnr(
		Form::TYPE_TEXT
	);
	
	$form->address_sapnr(
		Form::TYPE_TEXT
	);
	
	$form->address_sap_salesorganization(
		Form::TYPE_TEXT
	);

	$form->address_mps_customernumber(
		Form::TYPE_TEXT,
		'class=required_for_independet_retailer'
	);

	$form->address_mps_shipto(
		Form::TYPE_TEXT,
		'class=required_for_independet_retailer'
	);

	$form->address_legal_entity_name(
		Form::TYPE_TEXT
	);

	$form->address_company(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->extended('address_company', '
		<span class="fa-stack fa-md letter-control">
  			<i class="fa fa-square fa-stack-2x"></i>
			<i class="fa fa-font fa-stack-1x fa-inverse"></i>
		</span>
	');

	$form->address_company2(
		Form::TYPE_TEXT
	);
	
	$form->extended('address_company2', '
		<span class="fa-stack fa-md letter-control">
  			<i class="fa fa-square fa-stack-2x"></i>
			<i class="fa fa-font fa-stack-1x fa-inverse"></i>
		</span>
	');

	$form->address_address(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	/*
	$form->extended('address_address', '
		<span class="fa-stack fa-md letter-control">
  			<i class="fa fa-square fa-stack-2x"></i>
			<i class="fa fa-font fa-stack-1x fa-inverse"></i>
		</span>
	');
	*/

	if (!$disabled['address_address']) {
		$form->extended('address_address',"
			<div class='input-mask-container address-container'>
				<input type='text' value='".$data['address_street']."' class='input-mask address-mask required' name='address_street' placeholder='Street'>
				<input type='text' value='".$data['address_streetnumber']."' class='input-mask address-mask' name='address_streetnumber'  placeholder='Number'>
			</div>
			<span class='fa-stack fa-md letter-control'>
	  			<i class='fa fa-square fa-stack-2x'></i>
				<i class='fa fa-font fa-stack-1x fa-inverse'></i>
			</span>
		");
	}

	$form->address_address2(
		Form::TYPE_TEXT
	);
	
	$form->extended('address_address2', '
		<span class="fa-stack fa-md letter-control">
  			<i class="fa fa-square fa-stack-2x"></i>
			<i class="fa fa-font fa-stack-1x fa-inverse"></i>
		</span>
	');

	$form->address_country(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->address_place_id(
		Form::TYPE_SELECT,
		Form::PARAM_AJAX,
		Validata::PARAM_REQUIRED
	);
	
	// extend contents
	if (!$disabled['address_place_id'] ) {
		$form->extended('address_place_id',"
			<div class='geobox'>
				<div class=row >	
					<span>Province</span>
					<select name=province alt='Province' id=province ></select>
					<input type=text alt='New Province' name=new_province id=new_province class='required geodata' placeholder='Province Name' />
				</div>
				<div class=row >
					<span>New City</span> 
					<input type=text alt='New City' name=new_place id=new_place class='required geodata' placeholder='City Name' >
				</div>
			</div>
		");
	}

	$form->address_zip(
		Form::TYPE_TEXT
	);

	$form->address_phone(
		Form::TYPE_TEXT,
		FORM::TOOLTIP_PHONE,
		Validata::PARAM_PHONE
	);

	if (!$disabled['address_phone']) {
		$form->extended('address_phone',"
			<div class='input-mask-container phone-container'>
				<input type='text' value='".$data['address_phone_country']."' class='input-mask phone-mask' name='address_phone_country' id='address_phone_country' maxlength='6' placeholder='Country code'>
				<input type='text' value='".$data['address_phone_area']."' class='phone-mask' name='address_phone_area'  id='address_phone_area' maxlength='6' placeholder='Area'>
				<input type='text' value='".$data['address_phone_number']."' class='input-mask phone-mask' name='address_phone_number' id='address_phone_number' maxlength='20' placeholder='Phone number'>
			</div>
		");
	}

	$form->address_mobile_phone(
		Form::TYPE_TEXT,
		FORM::TOOLTIP_PHONE,
		Validata::PARAM_PHONE
	);

	if (!$disabled['address_mobile_phone']) {
		$form->extended('address_mobile_phone',"
			<div class='input-mask-container phone-container'>
				<input type='text' value='".$data['address_mobile_phone_country']."' class='input-mask phone-mask' name='address_mobile_phone_country' id='address_mobile_phone_country' maxlength='6' placeholder='Country code'>
				<input type='text' value='".$data['address_mobile_phone_area']."' class='phone-mask' name='address_mobile_phone_area'  id='address_mobile_phone_area' maxlength='6' placeholder='Area'>
				<input type='text' value='".$data['address_mobile_phone_number']."' class='input-mask phone-mask' name='address_mobile_phone_number' id='address_mobile_phone_number' maxlength='20' placeholder='Mobile phone number'>
			</div>
		");
	}

	$form->address_email(
		Form::TYPE_TEXT,
		FORM::TOOLTIP_EMAIL,
		Validata::PARAM_EMAIL
	);

	$form->address_website(
		Form::TYPE_TEXT,
		FORM::TOOLTIP_URL,
		Validata::PARAM_URL
	);

	$form->address_contact_name(
		Form::TYPE_TEXT
	);

	$form->address_contact_email(
		Form::TYPE_TEXT,
		FORM::TOOLTIP_EMAIL,
		Validata::PARAM_EMAIL
	);

	$form->address_currency(
		Form::TYPE_SELECT
	);

	$form->address_parent(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->address_can_own_independent_retailers(
		Form::TYPE_CHECKBOX
	);

	$form->address_involved_in_planning(
		Form::TYPE_CHECKBOX,
		"class=dependent_from_independent_retailer"
	);
	
	$form->address_involved_in_planning_ff(
		Form::TYPE_CHECKBOX,
		"class=dependent_from_independent_retailer"
	);

	$form->address_involved_in_lps(
		Form::TYPE_CHECKBOX
	);

	$form->address_involved_in_lps_ff(
		Form::TYPE_CHECKBOX
	);

	$form->address_involved_in_red(
		Form::TYPE_CHECKBOX
	);

	
	
	if ($hidden['address_is_independent_retailer'] && user::permission(Company::PERMISSION_EDIT_LIMITED)) {
		
		$form->address_is_independent_retailer(
			Form::TYPE_HIDDEN
		);
		
		$data['address_is_independent_retailer'] = 1; 
		unset($hidden['address_is_independent_retailer']);
		
	} else {
		$form->address_is_independent_retailer(
			Form::TYPE_CHECKBOX
		);
	}
	
	$form->address_is_internal_address(
			Form::TYPE_CHECKBOX
	);
	
	$form->address_do_data_export_from_mps_to_sap(
			Form::TYPE_CHECKBOX
	);
	
	$form->address_active(
		Form::TYPE_CHECKBOX
	);

	if (!$data['address_id']) {
		$form->create_pos_from_company(
			Form::TYPE_CHECKBOX
		);
	}

	// fieldset: company data
	$form->fieldset('name_address', array(
		'address_number',
		'address_legnr',
		'address_sapnr',
		'address_sap_salesorganization',
		'address_mps_customernumber',
		'address_mps_shipto',
		'address_legal_entity_name',
		'address_company',
		'address_company2',
		'address_address',
		'address_address2',
		'address_country',
		'address_place_id',
		'address_zip'
	));

	// fieldset: communication
	$form->fieldset('communication', array(
		'address_phone',
		'address_mobile_phone',
		'address_email',
		'address_website',
		'address_contact_name',
		'address_contact_email'
	));

	// fieldset: localisation
	$form->fieldset('localisation', array(
		'address_currency',
	));

	// fieldset: miscellanous
	$form->fieldset('miscellanous', array(
		'address_parent',
		'address_can_own_independent_retailers',
		'address_involved_in_planning',
		'address_involved_in_planning_ff',
		'address_involved_in_lps',
		'address_involved_in_lps_ff',
		'address_involved_in_red',
		'address_is_independent_retailer',
		'address_is_internal_address',
		'address_do_data_export_from_mps_to_sap',
		'address_active',
		'create_pos_from_company'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}

	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}

	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'mastersheet_delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}

	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript" src="/public/js/company.js"></script>
<style type="text/css">

	#companyform {
		width: 700px;
	}

	#address_mps_customernumber,
	#address_mps_shipto,
	#address_zip  {
		width: 120px;
	}

	.geobox {
		display: inline-block;
		*display: inline;
		zoom: 1;
		margin: 10px 0 0 190px;
		padding: 10px;
		background-color: white;
		width: 460px;
		border: 1px solid silver;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
	}
	
	.geobox .row {
		display: block;
	}
	
	.geobox span {
		display: inline-block;
		width: 60px;
		font-size: .8em;
	}
	
	.geobox span:first-child {
		margin-bottom: 10px;
	}
	
	.geobox input {
		width: 150px !important;
	}
	
	.fa-stack.fa-md {
		width: 1.6em;
		height: 1.6em;
		line-height: 1.6em;
	}
	
	.fa-stack.fa-md .fa-stack-2x {
		font-size: 1.6em;
	}
	
	.fa-stack.fa-md .fa-stack-1x {
		font-size: 0.85em;
	}
	
	.letter-control {
		color: silver;
		cursor: pointer;
	}
	
	.letter-control:hover {
		color: gray;
	}

	#new_province {
		margin: 5px 0 5px 64px;
	}


	.address_phone, .address_mobile_phone, .address_address { 
		position: relative; 
	}
	
	.address_address .-element:not(.-disabled), 
	.address_phone .-element:not(.-disabled), 
	.address_mobile_phone .-element:not(.-disabled) { 
		display: none !important;
	}

	.address_address .-tooltip, 
	.address_phone .-tooltip, 
	.address_mobile_phone .-tooltip {
		position: absolute; left: 500px;
	}

	.input-mask-container { display: inline-block; }
	.input-mask-container input {width: auto;}
	.phone-mask:nth-child(1) { width: 45px!important; }
	.phone-mask:nth-child(2) { width: 40px!important; }
	.phone-mask:nth-child(3) { width: 180px!important; }
	.address-mask:nth-child(1) { width: 240px!important; }
	.address-mask:nth-child(2) { width: 45px!important; }


</style>
<?php 
	
	if ($buttons['accessAddress']) {
		echo "<div class='accessAddress'><a href='{$buttons[accessAddress]}' target='_blank'>Access this Address in SysAdmin</a></div>";
	}
	elseif ($buttons['accessAddress3rd']) {
		echo "<div class='accessAddress'><a href='{$buttons[accessAddress3rd]}' target='_blank'>Access this Address in SysAdmin</a></div>";
	}
	echo $form; 
?>
