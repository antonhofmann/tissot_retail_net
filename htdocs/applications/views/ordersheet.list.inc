<?php 

	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript" src="/public/js/ordersheet.list.js" ></script>
<style type="text/css">

	#ordersheets {
		width: 1400px;
	}
	
	.dropdown-actions {
		margin: 0 10px 0 0;
	}
	
	.dropdown-actions .button {
		background: #f2f6f8;
		background: -moz-linear-gradient(top,  #f2f6f8 0%, #d8e1e7 50%, #b5c6d0 51%, #e0eff9 100%);
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f2f6f8), color-stop(50%,#d8e1e7), color-stop(51%,#b5c6d0), color-stop(100%,#e0eff9));
		background: -webkit-linear-gradient(top,  #f2f6f8 0%,#d8e1e7 50%,#b5c6d0 51%,#e0eff9 100%);
		background: -o-linear-gradient(top,  #f2f6f8 0%,#d8e1e7 50%,#b5c6d0 51%,#e0eff9 100%);
		background: -ms-linear-gradient(top,  #f2f6f8 0%,#d8e1e7 50%,#b5c6d0 51%,#e0eff9 100%);
		background: linear-gradient(to bottom,  #f2f6f8 0%,#d8e1e7 50%,#b5c6d0 51%,#e0eff9 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f6f8', endColorstr='#e0eff9',GradientType=0 );	
	}
	
	.dropdown-actions .button .label {
		color: maroon;
	}
	
	#workflowstates {
		text-transform:capitalize;
	}
	
	.companies label {
		display: none !important;
	}
	
	span.checkbox {
		display: block;
		width: 100%;
		margin-bottom: 10px;
	}
	
	span.caption {
		display: inline-block;
		*display: inline;
		zoom: 1;
		padding-left: 10px;
		font-size: 12px;
	}
	
	span.checkbox.checkall {
		font-weight: 700;
		margin-bottom: 20px;
	}
	
</style>
<div id="ordersheets"></div>
<?php  if ($dataloader['companies']) : ?>
<div class='modalbox-container'>
	<div id="modalbox" class="modalbox modal">
		<div class=modalbox-header >
			<div class='title'><?php echo $translate->standard_ordersheet; ?></div>
		</div>
		<div class=modalbox-content-container >
			<div class=modalbox-content> 
				<form id="standard_ordersheet" method="post" class="default" action="<?php echo $request->field('submit_standard_ordersheet'); ?>" >
					<span class="checkbox checkall"><input type=checkbox id=select_all name=select_all checked=checked ><span class="caption" data="Select All" >Deselect All</span></span>
					<?php 
						foreach ($dataloader['companies'] as $company => $name) {
							echo "<span class=checkbox ><input type=checkbox name='companies[$company]' class=companies checked=checked /><span class=caption >$name</span></span>";
						}
					?>
				</form>
			</div>
		</div>
		<div class=modalbox-footer >
			<div class=modalbox-actions >
				<a id=cancel class='button'>
					<span class="icon cancel"></span>
					<span class="label"><?php echo $translate->cancel ?></span>
				</a>
				<a id="apply" class='button'>
					<span class="icon add"></span>
					<span class="label"><?php echo $translate->create ?></span>
				</a>
			</div>
		</div>
	</div>
</div>
<?php  
	endif; 
	echo $request->form();
?>