<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();
	
	$form = new Form(array(
		'id' => 'form',
		'action' => '/applications/helpers/local.work.group.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->lwgroup_id(
		Form::TYPE_HIDDEN
	);	
	
	$form->lwgroup_group(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		Form::TOOLTIP_INTEGER,
		Validata::PARAM_ONLY_NUMBER,
		"class=numeric"
	);	
	
	$form->lwgroup_code(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);		
	
	$form->lwgroup_text(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);	

	$form->lwgroup_costgroup(
		Form::TYPE_SELECT
	);		
	
	$form->fieldset('Cost Monitorin Group', array(
		'lwgroup_group',
		'lwgroup_code',
		'lwgroup_text',
		'lwgroup_costgroup'
	));	
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>

<style type="text/css">

	#form { 
		width: 700px; 
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>