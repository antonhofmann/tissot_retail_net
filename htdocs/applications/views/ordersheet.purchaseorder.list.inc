<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	var ordersheets = $('#ordersheets').tableLoader({
		url: '/applications/helpers/ordersheet.purchaseorder.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			
			$('select',self).dropdown();

			$("#actions").change(function() {
				window.location = $(this).val();
			});
		}
	});
	
});
</script>
<style type="text/css">
	
	#ordersheets {
		width: 1400px;
	}
	
</style>
<div id="ordersheets"></div>
<?php 
	echo $request->form();
?>