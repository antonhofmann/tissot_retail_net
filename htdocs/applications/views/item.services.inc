<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<link href="/public/scripts/dropdown/dropdown.css" rel="stylesheet" type="text/css" />
<script src="/public/scripts/dropdown/dropdown.js" type="text/javascript"  ></script>
<script type="text/javascript" src="/public/scripts/table.loader.js" ></script>
<script type="text/javascript">
$(document).ready(function() {

	$('#items').tableLoader({
		url: '/applications/helpers/item.services.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			$('select',self).dropdown();

		}
	});

});
</script>
<style type="text/css">
	#items { width: 1000px; }
</style>
<div id="items"></div>
<?php echo $request->form(); ?>