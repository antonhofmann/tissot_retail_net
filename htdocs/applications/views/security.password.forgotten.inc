<?php 

	$translate = Translate::instance();

	$form = new Form(array(
		'id'=>'passwordform',
		'action'=>'/applications/helpers/security.check.user.php',
		'autocomplete' => 'off'
	));
	
	$form->firstname(
		Form::TYPE_TEXT, 
		Validata::PARAM_REQUIRED
	);
	
	$form->lastname(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->email(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		Validata::PARAM_EMAIL
	);
	
	$form->username(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->fieldset('personal_data', array(
		'firstname',
		'lastname',
		'email'
	));
	
	$form->fieldset('login_data', array(
		'username'
	));
	
	$form->button(ui::button(array(
		'id' => 'back',
		'href' => '/security',
		'icon' => 'back',
		'caption' => $translate->back
	)));
	
	$form->button(ui::button(array(
		'id' => 'submit',
		'class' => 'action blue',
		'caption' => $translate->submit_request
	)));
	
	$form = $form->render();

?>
<script type="text/javascript" src="/public/js/security.password.forgotten.js"></script>
<style type="text/css">
	#passwordform {
		width: 660px;
	}
</style>
<?php 
	echo $form; 
?>