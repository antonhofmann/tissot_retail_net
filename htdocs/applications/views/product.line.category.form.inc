<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();

	$form = new Form(array(
		'id' => 'productLineCateforyForm',
		'action' => '/applications/helpers/product.line.category.save.php',
		'method' => 'post',
		'class' => 'validator'
	));

	$form->redirect(
		Form::TYPE_HIDDEN
	);

	$form->application(
		Form::TYPE_HIDDEN
	);

	$form->controller(
		Form::TYPE_HIDDEN
	);

	$form->action(
		Form::TYPE_HIDDEN
	);

	$form->category_id(
		Form::TYPE_HIDDEN
	);	

	$form->category_product_line(
		Form::TYPE_HIDDEN
	);

	$form->category_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);	

	$form->category_cost_unit_number(
		Form::TYPE_TEXT
	);

	$form->category_budget(
		Form::TYPE_CHECKBOX
	);

	$form->category_catalog(
		Form::TYPE_CHECKBOX
	);	

	$form->category_useconfigurator(
		Form::TYPE_CHECKBOX
	);

	$form->category_not_in_use(
		Form::TYPE_CHECKBOX
	);

	$form->fieldset('Name', array(
		'category_name',
		'category_cost_unit_number'
	));	

	$form->fieldset('Visibility', array(
		'category_budget',
		'category_catalog',
		'category_useconfigurator',
		'category_not_in_use'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}

	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}

	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['items']) {
		$form->button(ui::button(array(
			'id' => 'assignItems',
			'icon' => 'icon120',
			'label' => 'Assign Items'
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}

	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<style></style>
<style type="text/css">

	#productLineCateforyForm,
	#categoryItems { 
		width: 700px; 
	}

	h4 { 
		display: block;
		margin: 40px 0 20px; 
		font-size: 1.4em;
		font-variant: small-caps;
		color: gray;
	}

	h5 { 
		display: block;
		margin: 40px 0 20px; 
		font-size: 1em;
		font-variant: small-caps;
		border-bottom: 1px solid silver;
		color: gray;
		padding-bottom: 5px;
		cursor: pointer;
	}

	#assignCategoryItems .modalbox-content h5:first-child {
		margin-top: 20px;
	}

	h5:hover { 
		color: #1b5584;
	}

	.xhr-placeholder {
		padding: 0;
	}

</style>
<script type="text/javascript">
	$(document).ready(function() {

		var categoryItems = $('#categoryItems').tableLoader({
			url: '/applications/helpers/product.line.category.items.php',
			data: $('#productLineCateforyForm').serializeArray(),
			after: function(self) {

				if ($('h5', self).length>1) {
					
					$('.items', self).hide();

					$('h5', self).click(function(e) {
						e.preventDefault();
						$(this).next().slideToggle();
						$('.fa', $(this)).toggleClass('fa-chevron-circle-right').toggleClass('fa-chevron-circle-down');
					});

					$('h5:first', self).trigger('click');

				} else {
					$('.fa', self).toggleClass('fa-chevron-circle-right').toggleClass('fa-chevron-circle-down');
				}
			}
		}); 


		$('#assignItems').click(function(e) {
			
			e.preventDefault();
		
			retailnet.modal.show('#assignCategoryItems', {
				autoSize	: false,
				closeClick	: false,
				closeBtn	: false,
				fitToView   : true,
			    width       : '90%',
			    height      : '90%',
			    maxWidth	: '820px',
			    maxHeight	: '820px',
				modal 		: true
			}); 

			var assignCategoryItems = $('#assignCategoryItems .modalbox-content').tableLoader({
				url: '/applications/helpers/product.line.category.items.php?assign_items=1',
				data: $('#productLineCateforyForm').serializeArray(),
				after: function(self) {

					if ($('h5', self).length>1) {
						
						$('.items', self).hide();

						$('h5', self).click(function(e) {
							e.preventDefault();
							$(this).next().slideToggle();
							$('.fa', $(this)).toggleClass('fa-chevron-circle-right').toggleClass('fa-chevron-circle-down');
						});
					} else {
						$('.fa', self).toggleClass('fa-chevron-circle-right').toggleClass('fa-chevron-circle-down');
					}

					$('[name=category-item]', self).change(function(e) {
							
						retailnet.notification.hide();

						var section = $(this).is(':checked') ? 'category.item.add' : 'category.item.remove';

						retailnet.ajax.json('/applications/helpers/product.line.ajax.php', {
							section: section,
							category: $('#category_id').val(),
							item: $(this).data('id')
						}).done(function(xhr) {
							if (xhr && xhr.message) {
								if (xhr.success==1) retailnet.notification.success(xhr.message);
								else retailnet.notification.error(xhr.message);
							}
						});
					});
				}
			});

			return false;
		});
		
		
		// cancel modal screen
		$('.close-modal-items').click(function(e) {
			e.preventDefault();
			retailnet.modal.hide();
			categoryItems.showPage(1);
			$('#assignCategoryItems .modalbox-content').empty();
			return false;
		});	
		
	});
</script>

<?php 

echo $form; 

echo ui::dialogbox(array(
	'id' => 'delete_dialog',
	'title' => $translate->delete,
	'content' => $translate->dialog_delete_record,
	'buttons' => array(
		'cancel' => array(
			'icon' => 'cancel',
			'label' => $translate->cancel
		),
		'apply' => array(
			'icon' => 'apply',
			'label' => $translate->yes
		),
	)
));
?>
<div id="categoryItems" ></div>
<div class="modalbox-container">
	<div id="assignCategoryItems" class="modalbox modal">
		<div class="modalbox-header" >
			<div class="title">Assign Items to Category</div>
		</div>
		<div class="modalbox-content-container" >
			<div class="modalbox-content"></div>
		</div>
		<div class="modalbox-footer" >
			<div class="modalbox-actions" >
				<a class="button close-modal-items">
					<span class="label">Close</span>
				</a>
			</div>
		</div>
	</div>
</div>