<?php

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$translate->file = $translate->red_projectsheet_signed_file_path;
	$uploadDisabled = ($buttons['save']) ? null : 'disabled';
	
	$data['file'] = '
		<span id="showfile" class="button">
			<span class="icon download"></span>
			<span class="label">'.$translate->download_file.'</span>	
		</span>
		<span id="upload" class="button '.$uploadDisabled.'">
			<span class="icon upload"></span>
			<span class="label">'.$translate->upload_file.'</span>	
		</span>
		<span class="highlight" ></span>
		<input type=hidden name="red_projectsheet_signed_file_path" id="red_projectsheet_signed_file_path" value="'.$data['red_projectsheet_signed_file_path'].'" />
		<input type="hidden" name="has_upload" id="has_upload" />
	';

	$form = new Form(array(
		'id' => 'projectsheet_form',
		'action' => '/applications/helpers/red.project.sheet.save.php',
		'method' => 'post'
	));
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);

	$form->red_projectsheet_id(
		Form::TYPE_HIDDEN
	);

	$form->red_projectsheet_project_id(
		Form::TYPE_HIDDEN
	);

	$form->red_projectsheet_business_unit_id (
		Form::TYPE_SELECT
	);

	$form->red_projectsheet_costcenter_id (
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->red_projectsheet_accountnumber_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->red_projectsheet_currency_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->red_projectsheet_openingdate(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		"class=datepicker"
	);

	$form->red_projectsheet_plannedamount_current_year(
		Form::TYPE_TEXT
	);

	$form->red_projectsheet_share_swatch(
		Form::TYPE_TEXT
	);


	$form->red_projectsheet_share_others(
		Form::TYPE_TEXT
	);

	$form->red_projectsheet_totalbudget(
		Form::TYPE_TEXT
	);

	$form->red_projectsheet_alreadycommitted(
		Form::TYPE_TEXT
	);

	$form->red_projectsheet_alreadyspent(
		Form::TYPE_TEXT
	);

	$form->budget_still_available(
		Form::TYPE_AJAX
	);

	$form->red_projectsheet_approvalname01(
		Form::TYPE_TEXT
	);

	$form->red_projectsheet_approval_deadline01(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		"class=datepicker"
	);
	$form->red_projectsheet_approvalname02(
		Form::TYPE_TEXT
	);
	$form->red_projectsheet_approval_deadline02(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		"class=datepicker"
	);
	$form->red_projectsheet_approvalname03(
		Form::TYPE_TEXT
	);
	$form->red_projectsheet_approval_deadline03(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		"class=datepicker"
	);
	$form->red_projectsheet_approvalname04(
		Form::TYPE_TEXT
	);
	$form->red_projectsheet_approval_deadline04(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		"class=datepicker"
	);
	$form->red_projectsheet_approvalname05(
		Form::TYPE_TEXT
	);
	$form->red_projectsheet_approval_deadline05(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		"class=datepicker"
	);
	$form->red_projectsheet_approvalname06(
		Form::TYPE_TEXT
	);

	$form->red_projectsheet_approval_deadline06(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		"class=datepicker"
	);

	$form->red_projectsheet_financial_justification(
		Form::TYPE_TEXTAREA,
		'param=label'
	);

	$form->red_projectsheet_mainsupplier_partner_id(
		Form::TYPE_SELECT
	);

	$form->file(
		Form::TYPE_AJAX,
		FORM::PARAM_SHOW_VALUE
	);

	// fieldset: Project Details
	$form->fieldset('project', array(
		'red_projectsheet_business_unit_id',
		'red_projectsheet_costcenter_id',
		'red_projectsheet_accountnumber_id',
		'red_projectsheet_currency_id',
		'red_projectsheet_openingdate',
		'red_projectsheet_plannedamount_current_year',
		'red_projectsheet_share_swatch',
		'red_projectsheet_share_others'
	));

	// fieldset: Budget Situation
	$form->fieldset('budget_situation', array(
		'red_projectsheet_totalbudget',
		'red_projectsheet_alreadycommitted',
		'red_projectsheet_alreadyspent',
		'budget_still_available'
	));

	// fieldset: Budget Situation
	$form->fieldset('approval', array(
		'red_projectsheet_approvalname01',
		'red_projectsheet_approval_deadline01',
		'red_projectsheet_approvalname02',
		'red_projectsheet_approval_deadline02',
		'red_projectsheet_approvalname03',
		'red_projectsheet_approval_deadline03',
		'red_projectsheet_approvalname04',
		'red_projectsheet_approval_deadline04',
		'red_projectsheet_approvalname05',
		'red_projectsheet_approval_deadline05',
		'red_projectsheet_approvalname06',
		'red_projectsheet_approval_deadline06'
	));

	// fieldset: Budget Situation
	$form->fieldset('miscellaneous', array(
		'red_projectsheet_financial_justification',
		'red_projectsheet_mainsupplier_partner_id',
		'file'
	));


	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}

	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['sheet']) {
		$form->button(ui::button(array(
			'id' => 'sheet',
			'icon' => 'print',
			'href' => $buttons['sheet'],
			'label' => $translate->print,
			'target' => '_blank'
		)));
	}
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}

	$form->dataloader($data);
	$form->dataloader($data_projectsheet);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<link rel="stylesheet" type="text/css" href="/public/scripts/jquery/jquery.ui.css" />
<link rel="stylesheet" type="text/css" href="/public/css/jquery.ui.css" />
<script type="text/javascript" src="/public/scripts/jquery/jquery.ui.js"  ></script>
<link rel="stylesheet" href="/public/scripts/validationEngine/css/validationEngine.jquery.css" type="text/css"/>
<script src="/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="/public/scripts/validationEngine/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="/public/css/jquery.ui.css" />
<script type="text/javascript" src="/public/scripts/chain/chained.select.js"  ></script>
<link rel="stylesheet" type="text/css" href="/public/scripts/loader/loader.css" />
<script type="text/javascript" src="/public/scripts/loader/loader.js"  ></script>
<script type="text/javascript" src="/public/scripts/qtip/qtip.js"  ></script>
<script type="text/javascript" src="/public/scripts/ajaxuploader/ajaxupload.js"  ></script>
<script type="text/javascript">
$(document).ready(function() {

	var form = $('#projectsheet_form');
	var filePath = $('#red_projectsheet_signed_file_path');
	var btnShowFile = $('#showfile');
	var btnUploadFile = $('#upload');
	var hasUpload = $('#has_upload');
	var application = $('#application');
	var highlight = $('.highlight').hide();
	var MAX_LINES = 13;


	new AjaxUpload( btnUploadFile, {
		action: '/applications/helpers/ajax.file.upload.php',
		name: 'userfile',
		data: {
			application: application.val(),
			checkExtension: true
		},
		onComplete: function(file, response) {
			
			if (response) {

				var json = $.parseJSON(response);
				
				retailnet.loader.hide();
				highlight.hide();
				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
				
				if(json.response) {

					var path = $("<div/>").html(json.path).text();
					filePath.val(path);
					
					btnUploadFile.removeClass('error');
					hasUpload.val(1);
					highlight.removeClass('error').addClass('success').text(json.button).slideToggle('slow');
					filePath.trigger('change');
				} 

				if (json.message) {
					$.jGrowl(json.message, { 
						sticky: (json.response) ? false : true, 
						theme: (json.response) ? 'message' : 'error'
					});
				}
			}
		}
	});

	filePath.change(function() {

		path = $(this).val(); 

		if (path) {

			btnShowFile.removeClass('disabled');

			if (isImage(path)) {
				$('.icon', btnShowFile).removeClass('download').addClass('picture');
				$('.label', btnShowFile).text('Show Picture');
			} else {
				$('.icon', btnShowFile).removeClass('picture').addClass('download');
				$('.label', btnShowFile).text('Download');
			}
			
		} else {
			btnShowFile.addClass('disabled');
		}
		
	}).trigger('change');
	

	btnShowFile.click(function(event) {

		event.preventDefault();

		if (!$(this).hasClass('disabled')) {

			path = filePath.val();

			if (isImage(path)) {

				retailnet.modal.show(path);
			}
			else {
				window.open('http://'+location.host+path);
			}
		}
	});


	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) {

			retailnet.loader.hide();
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
			
			if (json.response) { 
				
				if (json.redirect) {
					window.location=json.redirect;
				}
				
				if (json.message) {
					$.jGrowl(json.message,{ 
						sticky: (json.response) ? false : true, 
						theme: (json.response) ? 'message' : 'error'
					});
				}
				
				if (hasUpload.val() && json.path) {
					filePath.val(json.path);
					hasUpload.val('');
				}	
			}
		}
	});


	$("#save").click(function(event) {

		event.preventDefault();

		// validators
		var errors = form.find('.error').length;
		var valid = form.validationEngine('validate');

		// close loaders
		retailnet.loader.hide();
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
		highlight.removeAttr('class').hide();

		if (errors || !valid) {
			$.jGrowl('Check red marked fields.',{ 
				sticky: true, 
				theme: 'error'
			});
		}
		else {
			retailnet.loader.show();
			form.submit();
		}
	});

	$('.-tooltip[title]').qtip({
		position: {corner: {target:'topRight', tooltip:'leftTop'}},
		style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
	});

	$('.datepicker').datepicker({
		dateFormat: 'dd.mm.yy' ,
		showOtherMonths : true,
		firstDay: 1,
		onClose: function(dateText, inst) {
			//$(this).toggleClass('error', (dateText) ? false : true);
		}
	});

	// Line Number
	lineNumber = $('<span id="lineNumber" class="-label"></span>');
	textarea = $("div.red_projectsheet_financial_justification textarea");
	textarea.after(lineNumber);

	textarea.bind('change keydown keyup paste',function (){
		var text = textarea.val();
		var lines = text.split(/\r|\r\n|\n/);
		var count = lines.length;
		lineNumber.text(count + ' of '+ MAX_LINES + ' lines used');
	});

	textarea.trigger('change');

	// Calculate budget
	//$("#red_projectsheet_totalbudget, #red_projectsheet_alreadycommitted, #red_projectsheet_alreadyspent").numeric();
	$("#red_projectsheet_totalbudget, #red_projectsheet_alreadycommitted").bind("keyup paste", function(event) {
		calculateBudget();
	});

	function calculateBudget() {
		budget = $('#red_projectsheet_totalbudget').val() - $('#red_projectsheet_alreadycommitted').val();
		$('#budget_still_available').text(budget);
		if (budget < 0) $('#budget_still_available').addClass('negative');
		else $('#budget_still_available').removeClass('negative');
	}

	//Main
	calculateBudget();

	//Select CHF as default currency
	if ($('#red_projectsheet_currency_id').val() == '') {
		$('#red_projectsheet_currency_id option[value="1"]').attr('selected','selected');
	}

});
</script>
<style type="text/css">

	#projectsheet_form,
	#toolbox {
		width: 730px;
	}

	textarea {
		width: 500px;
		height: 260px;
		display: block;
	}

	#budget_still_available.negative {
		color: red;
	}

	#lineNumber {
		margin: 0;
	}
	
	#red_projectsheet_openingdate,
	#red_projectsheet_approval_deadline01,
	#red_projectsheet_approval_deadline02,
	#red_projectsheet_approval_deadline03,
	#red_projectsheet_approval_deadline04,
	#red_projectsheet_approval_deadline05,
	#red_projectsheet_approval_deadline06 {
		width: 100px;
	}
	
	#upload.disabled {
		display: none !important;
	}

</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete_file,
		'content' => $translate->dialog_delete_file,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>