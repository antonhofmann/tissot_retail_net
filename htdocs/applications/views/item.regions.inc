<?php 
	$user = User::instance();
	$translate = Translate::instance();
?>
<style type="text/css">
	
	.regions-container {
		width: 700px;
	}

	.regions-container + .regions-container {
		margin-top: 20px;
	}

	.table-regions th {
	    background-color: #FAFAFA;
	    border-top: 1px solid #dedede !important;
	    vertical-align: middle !important;
	    border-bottom-width: 1px !important;
	    padding-top: 10px !important;
	    padding-bottom: 10px  !important;
	}

	.table-regions th a {
		color: black;
		text-decoration: none;
		font-weight: 400;
	}

	.table-regions td {
		font-size: 12px;
	}

	.table-regions .checkbox {
		margin: 0;
	}

	.table-regions tbody {
		display: none;
	}

	.table-regions thead th.total-countries-caption,
	.table-regions thead th:last-child {
		color: #999;
		text-decoration: none;
		font-weight: 300;
		font-size: 12px;
	}

</style>
<?php
	
	echo "<input type='hidden' name='item' id='item' value='$id' >";

	if ($datagrid) {

		foreach ($datagrid as $region => $row) {

			$selectedRegion = null;

			$countries = $row['countries'];
			$regionName = $row['region'];

			if (!$countries) continue;

			if ($countries && $selected[$region]) {
				$selectedRegion = count($countries)==count($selected[$region]) ? 'checked=checked' : null;
			}
			
			$ico = $selectedRegion ? 
				"<img src='/public/images/icon-checked.png' >" 
				: "<img src='/public/images/unchecked.png'>";

			$el = $disabled['regions']
					? $ico
					: "<input type='checkbox' class='region' value='$region' $selectedRegion>";
			
			echo "<div class='bootstrap regions-container'>";
			echo "<table class='table table-condensed table-regions' data-region='$region' width='100%' >";
			
			echo "<thead>";
			echo "<tr>";
			echo "<th width='20px' >$el</th>";
			echo "<th><a data-toggle='collapse' href='#region-countries-$region' aria-expanded='true' aria-controls='region-countries-$region'>";
			echo "<i class='fa fa-caret-down arow-caret'></i> $regionName</a></th>";
			echo "<th nowrap='nowrap' align='right' width='10%' class='total-countries-caption' >Total selectd countries</th>";
			echo "<th nowrap='nowrap' align='right' width='5%'><span class='badge total-countries' >0</span></th>";
			echo "</tr>";
			echo "</thead>";

			echo "<tbody id='region-countries-$region'>";
			
			foreach ($countries as $country => $countryName) {

				$selectedCountry = $selected[$region] && $selected[$region][$country] ? 'checked=checked' : null;
				
				$ico = $selectedCountry 
					? "<img src='/public/images/icon-checked.png' class='checked' > " 
					: "<img src='/public/images/unchecked.png'> ";

				$el = $disabled['regions']
					? $ico.$countryName
					: "<div class='checkbox'><label><input type='checkbox' value='$country' class='country' $selectedCountry > $countryName</label></div>";
				
				echo "<tr>";
				echo "<td>&nbsp;</td>";
				echo "<td colspan='3'>$el</td>";
				echo "</tr>";
			}

			echo "</tbody>";

			echo "</table>";
			echo "</div>";

		}
	}

	echo "<div class='actions'>";
	if ($buttons['back']) {
		
		echo UI::button(array(
			'caption' => 'Back',
			'icon' => 'back',
			'href' => $buttons['back']
		));
	}
	echo "</div>";
?>