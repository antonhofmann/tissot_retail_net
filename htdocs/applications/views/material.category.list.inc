<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#material_categories').tableLoader({
		url: '/applications/helpers/material.category.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">
	
	#material_categories { 
		width: 800px; 
	}
	
</style>
<?php echo $request->form($requestFields);  ?>
<div id="material_categories"></div>