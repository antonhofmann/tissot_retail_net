<?php

	$translate = Translate::instance();

	$form = new Form(array(
		'id'=>'loginform',
		'action'=>'/applications/helpers/security.login.php',
		'class' => 'validator'
	));

	$form->hash(Form::TYPE_HIDDEN);

	$form->username(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);

	$form->password(
		Form::TYPE_PASSWORD,
		Validata::PARAM_REQUIRED
	);

	$form->button(ui::button(array(
		'id' => 'save',
		'class' => 'big',
		'icon' => 'lock',
		'caption' => $translate->login
	)));

	$form = $form->render();
?>
<script type="text/javascript" src="/public/scripts/form.js"  ></script>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $('#loginform');
		var hash = window.location.hash;

		$('#loginform').Form();
		$('#hash').val(hash);

		/*
		jQuery("#login").click(function(event) {
			event.preventDefault();
			$("#loginform").submit();
			return false;
		});

		form.validationEngine({
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			onAjaxFormComplete: function(status, form, json, options) { 

				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close'); 

				if (json.response && json.redirect) {
					window.location=json.redirect;
				} else if (json.message) {
					$.jGrowl(json.message, { 
						sticky : true, 
						theme :'error'
					});
				}
			}
		});
		*/
	});
</script>
<style type="text/css">
	
	.maintenance {
		width: 600px;
		padding: 10px;
		margin: 0 auto 20px;
		text-align: center;
		font-size: 14px;
		font-weight: 500;
		color: #F5092A;
		background-color: #cfe6f8;
		-moz-box-shadow: 0 0 5px #888;
		-webkit-box-shadow: 0 0 5px#888;
		box-shadow: 0 0 5px #888;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		line-height:140%;
	}
	
</style>
<?php 
	
	
	if($this->data['maintenance']) {
		echo '<div class="maintenance">';
		echo $this->data['maintenance'];
		echo '</div>';
	}

	echo $form;

	
?>