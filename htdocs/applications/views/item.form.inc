<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();
	
	$form = new Form(array(
		'action' => '/applications/helpers/item.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);	
	
	$form->item_id(
		Form::TYPE_HIDDEN
	);

	$form->item_type(
		Form::TYPE_HIDDEN
	);
	

	// Description 

	$form->item_code(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);	

	$form->item_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);

	$form->item_category(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->item_subcategory(
		Form::TYPE_SELECT,
		Form::PARAM_AJAX
	);

	/*
	$form->item_price(
		Form::TYPE_TEXT,
		Form::TOOLTIP_NUMERIC,
		Validata::PARAM_NUMBER
	);
	*/

	$form->item_description(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);

	$form->caption('item_price', 'Item Price in CHF');

	$form->fieldset('Description', array(
		'item_code',
		'item_name',
		'item_category',
		'item_subcategory',
		//'item_price',
		'item_description'
	));	

	// Cost Monitoring 
	
	$form->item_cost_group(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);		

	$form->fieldset('Cost Monitoring', array(
		'item_cost_group'
	));

	// Specification Data 

	$form->item_watches_displayed(
		Form::TYPE_TEXT,
		Form::TOOLTIP_INTEGER,
		Validata::PARAM_ONLY_NUMBER
	);

	$form->item_watches_stored(
		Form::TYPE_TEXT,
		Form::TOOLTIP_INTEGER,
		Validata::PARAM_ONLY_NUMBER
	);

	/*
	if ($data['item_certificate_materials']) {

		$form->item_certificate_materials(
			Form::TYPE_PLACEHOLDER,
			Form::PARAM_SHOW_VALUE,
			"label=Materials"
		);
	}

	// Item materials list
	
	$form->item_materials(
        Form::TYPE_PLACEHOLDER,
		Form::PARAM_SHOW_VALUE
	);

	if ($data['item_certificate_materials']) {
		$form->item_materials('label=Other Materials');
	}

	$table  = "<div class='form-list'>";
	$table .= "<table id='itemMaterialList' class='form-list item-materials-list' cellpadding='0' cellspacing='0' border='0' >";
	$table .= "<tbody>";

	if ($dataloader['item_materials']) { 

		foreach ($dataloader['item_materials'] as $row) {

			$btnItemMaterial = $disabled['item_materials'] 
				? null 
				: "<span class='btn-list-action btn-remove' ><i class='fa fa-minus-square'></i></span>";

			$fieldName = $disabled['item_materials']
				? $row['item_material_name']
				: "<input type='text' name='item_material_name[]' value='{$row[item_material_name]}' placeholder='Material description' >";

			$table .= "<tr>";
			$table .= "<td>".$fieldName."</td>";
			$table .= "<td>".$btnItemMaterial."</td>";
			$table .= "</tr>";
		}
	}
	
	if (!$disabled['item_materials']) {
		$table .= "<tr class='newrow'>";
		$table .= "<td><input type='text' name='item_material_name[]' placeholder='Material description' ></td>";
		$table .= "<td valign='bottom'><span class='btn-list-action btn-add' ><i class='fa fa-plus-square'></i></span></td>";
		$table .= "</tr>";
	}

	$table .= "</tbody>";
	$table .= "</table>";
	$table .= "</div>";

	unset($dataloader['item_materials']);
	$data['item_materials'] =  $table;
	*/

	// Electrical Specifications list
	
	$form->item_electrical_specifications(
		Form::TYPE_PLACEHOLDER,
		Form::PARAM_SHOW_VALUE
	);

	$table  = "<div class='form-list'>";
	$table .= "<table id='itemElectricalList' class='item-electrical-list' cellpadding='0' cellspacing='0' border='0' >";
	$table .= "<tbody>";

	if ($dataloader['item_electrical_specifications']) {

		$btnElectricalSpecifications = $disabled['item_electrical_specifications'] 
			? null 
			: "<span class='btn-list-action btn-remove' ><i class='fa fa-minus-square'></i></span>";
		
		foreach ($dataloader['item_electrical_specifications'] as $row) {

			$fieldName = $disabled['item_electrical_specifications']
				? $row['item_electricspec_name']
				: "<textarea name='item_electricspec_name[]' placeholder='Article' >{$row[item_electricspec_name]}</textarea>";
			
			$fieldDescription = $disabled['item_electrical_specifications']
				? $row['item_electricspec_description']
				: "<textarea name='item_electricspec_description[]' placeholder='Specification' >{$row[item_electricspec_description]}</textarea>";

			$table .= "<tr>";
			$table .= "<td>".$fieldName."</td>";
			$table .= "<td>".$fieldDescription."</td>";
			$table .= "<td>".$btnElectricalSpecifications."</td>";
			$table .= "</tr>";
		}
	} 

	if (!$disabled['item_electrical_specifications']) {
		$table .= "<tr class='newrow'>";
		$table .= "<td><textarea name='item_electricspec_name[]' placeholder='Article' ></textarea></td>";
		$table .= "<td><textarea name='item_electricspec_description[]' placeholder='Specification' ></textarea></td>";
		$table .= "<td valign='bottom'><span class='btn-list-action btn-add' ><i class='fa fa-plus-square'></i></span></td>";
		$table .= "</tr>";
	}

	$table .= "</tbody>";
	$table .= "</table>";
	$table .= "</div>";

	unset($dataloader['item_electrical_specifications']);
	$data['item_electrical_specifications'] = $table;

		
	$form->item_regulatory_approvals(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);

	$form->item_install_requirements(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);

	$form->fieldset('Specification Data', array(
		'item_watches_displayed',
		'item_watches_stored',
		'item_electrical_specifications',
		'item_regulatory_approvals',
		'item_install_requirements'
	));

	// Other Informations 

	$form->item_stock_property_of_swatch(
		Form::TYPE_CHECKBOX,
		"tooltip=true"
	);

	/*
	$form->item_visible(
		Form::TYPE_CHECKBOX,
		"tooltip=true"
	);
	*/

	$form->item_visible(
		Form::TYPE_HIDDEN
	);

	$form->item_visible_in_projects(
		Form::TYPE_CHECKBOX,
		"tooltip=true"
	);

	/*
	$form->item_visible_in_production_order(
		Form::TYPE_CHECKBOX,
		"tooltip=true"
	);
	*/

	$form->item_visible_in_production_order(
		Form::TYPE_HIDDEN
	);

	/*
	$form->item_price_adjustable(
		Form::TYPE_CHECKBOX,
		"tooltip=true"
	);
	*/

	$form->item_price_adjustable(
		Form::TYPE_HIDDEN
	);

	$form->item_active(
		Form::TYPE_CHECKBOX,
		"tooltip=true"
	);

	/*
	if (!$hidden['item_is_dr_swatch_furniture'] || !$hidden['item_visible_in_mps'] || !$hidden['item_addable_in_mps']) {
		$form->to_be_removed_later_time(
			Form::TYPE_PLACEHOLDER,
			Form::PARAM_SHOW_VALUE,
			'label=To be removed at a later time:'
		);
	}

	$form->item_is_dr_swatch_furniture(
		Form::TYPE_CHECKBOX,
		"tooltip=true"
	);

	$form->item_visible_in_mps(
		Form::TYPE_CHECKBOX,
		"tooltip=true"
	);

	$form->item_addable_in_mps(
		Form::TYPE_CHECKBOX,
		"tooltip=true"
	);
	*/

	$form->item_is_dr_swatch_furniture(
		Form::TYPE_HIDDEN
	);

	$form->item_visible_in_mps(
		Form::TYPE_HIDDEN
	);

	$form->item_addable_in_mps(
		Form::TYPE_HIDDEN
	);

	$form->fieldset('Other', array(
		'item_stock_property_of_swatch',
		'item_visible',
		'item_visible_in_projects',
		'item_visible_in_production_order',
		'item_price_adjustable',
		'item_active',
		'to_be_removed_later_time',
		'item_is_dr_swatch_furniture',
		'item_visible_in_mps',
		'item_addable_in_mps'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>

<style type="text/css">
	
	form textarea {
		width: 440px;
		height: 60px;
		outline: none;
	}

	.item_slave_item .desc {
		display: block;
		font-size: 12px;
		margin: 5px 10px 0 194px;
		color: gray;
	}

	
	#item_packed_weight,
	#item_lights_used,
	#item_regulatory_approvals,
	#item_install_requirements {
		height: 30px !important;
	}

	
	#item_packed_size {
		height: 120px !important;
	}

	.btn-list-action {
		font-size: 18px;
		color: #888;
		cursor: pointer;
	}

	.btn-list-action:hover {color: #000;}

	.btn-list-action.btn-add {
		display: none;
	}

	form.default .form-list input[type="text"] {
		width: auto;
	}

	form.default .form-list tr th,
	form.default .form-list tr td {
		font-weight: 400;
		text-align: left;
		padding: 5px;
	}

	form.default .form-list tr td:first-child {
		padding-left: 0;
	}

	form.default .form-list tr:last-child .add-btn-live {
		display: block;
	}

	form.default table.item-materials-list input[type="text"] {
		width: 300px;
	}

	form.default table.item-electrical-list td {
		vertical-align: bottom;
	}

	form.default table.item-electrical-list textarea {
		width: 200px;
		height: 64px;
		box-sizing: border-box;
		resize: none;
		outline: none;
	}

	form.default table.item-electrical-list tr td:last-child {
		padding-bottom: 10px;
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>