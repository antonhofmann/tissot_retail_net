<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'mastersheet',
		'action' => '/applications/helpers/mastersheet.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_mastersheet_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_mastersheet_year(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		Validata::PARAM_YEAR
	);
	
	$form->mps_mastersheet_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->fieldset('master_sheet', array(
		'mps_mastersheet_year',
		'mps_mastersheet_name'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['reset_exchange_data']) {
		$form->button(ui::button(array(
			'id' => 'reset_exchange_data',
			'class' => 'dialog',
			'icon' => 'reload',
			'href' => $buttons['reset_exchange_data'],
			'label' => $translate->reset_exchange_data
		)));
	}
	
	if ($buttons['archive']) {
		$form->button(ui::button(array(
			'id' => 'mastersheet_archive',
			'class' => 'dialog',
			'icon' => 'lock',
			'href' => $buttons['archive'],
			'label' => $translate->put_to_archive
		)));
	}
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<style type="text/css">

	#mastersheet { 
		width: 700px; 
	}
	
	#mps_mastersheet_year { 
		width: 100px; 
	} 
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel,
				'class' => 'cancel'
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes,
				'class' => 'apply'
			),
		)
	));

	echo ui::dialogbox(array(
		'id' => 'mastersheet_archive_dialog',
		'title' => $translate->put_to_archive,
		'content' => $translate->dialog_mastersheet_put_to_archive(array('mastersheet' => $data['mps_mastersheet_name'])),
		'buttons' => array(
			'cancelArchive' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel,
				'class' => 'cancel'
			),
			'applyArchive' => array(
				'icon' => 'apply',
				'label' => $translate->yes,
				'class' => 'apply'
			),
		)
	));

	echo ui::dialogbox(array(
		'id' => 'reset_exchange_data_dialog',
		'title' => $translate->reset_exchange_data,
		'content' => $translate->dialog_reset_exchange_data,
		'buttons' => array(
			'cancelReset' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel,
				'class' => 'cancel'
			),
			'applyReset' => array(
				'icon' => 'apply',
				'label' => $translate->yes,
				'class' => 'apply'
			),
		)
	));
?>