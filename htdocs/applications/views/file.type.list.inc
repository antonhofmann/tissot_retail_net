<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#filetypes').tableLoader({
		url: '/applications/helpers/file.type.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">

	#filetypes { 
		width: 500px; 
	}
</style>
<?php 
	echo $request->form();
?>
<div id="filetypes"></div>