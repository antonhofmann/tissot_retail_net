<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#purposes').tableLoader({
		url: '/applications/helpers/material.purpose.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
});
</script>
<style type="text/css">

	#purposes { 
		width: 800px; 
	}
	
</style>
<?php echo $request->form($requestFields); ?>
<div id="purposes"></div>
