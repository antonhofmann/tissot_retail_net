<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'furniture',
		'action' => '/applications/helpers/item.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->url(
		Form::TYPE_HIDDEN
	);
	
	$form->item_id(
		Form::TYPE_HIDDEN
	);
	
	$form->item_category(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->item_code(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->item_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->item_width(
		Form::TYPE_TEXT,
		Form::TOOLTIP_NUMBER_DECIMAL_2,
		Validata::PARAM_NUMBER_DECIMAL_2
	);
	
	$form->item_height(
		Form::TYPE_TEXT,
		Form::TOOLTIP_NUMBER_DECIMAL_2,
		Validata::PARAM_NUMBER_DECIMAL_2
	);
	
	$form->item_length(
		Form::TYPE_TEXT,
		Form::TOOLTIP_NUMBER_DECIMAL_2,
		Validata::PARAM_NUMBER_DECIMAL_2
	);
	
	$form->item_radius(
		Form::TYPE_TEXT,
		Form::TOOLTIP_NUMBER_DECIMAL_2,
		Validata::PARAM_NUMBER_DECIMAL_2
	);
	
	$form->fieldset('furniture', array(
		'item_category', 
		'item_code',
		'item_name'
	));
	
	$form->fieldset('dimensions', array(
		'item_width',
		'item_height',
		'item_length',
		'item_radius'
	));
	
	$form->fieldset('contact', array(
		'mps_staff_email',
		'mps_staff_phone'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>

<script type="text/javascript" src="/public/js/form.validator.js"></script>

<style type="text/css">

	#furniture { 
		width: 700px; 
	}
	
	form .dimensions input { 
		width: 120px; 
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>