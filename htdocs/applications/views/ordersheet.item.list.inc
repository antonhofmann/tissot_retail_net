<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();
	
	// db application model
	$model = new Model($request->application);

	// order sheet
	$ordersheet = new Ordersheet($application);
	$ordersheet->read($id);
	
	// ordersheet items
	$result = $model->query("
		SELECT 
			mps_ordersheet_item_id,
			mps_ordersheet_item_price,
			mps_ordersheet_item_quantity,
			mps_ordersheet_item_quantity_proposed,
			mps_ordersheet_item_quantity_approved,
			mps_ordersheet_item_quantity_distributed,
			mps_ordersheet_item_quantity_confirmed,
			mps_ordersheet_item_quantity_shipped,
			mps_ordersheet_item_status,
			mps_ordersheet_item_price,
			mps_ordersheet_item_order_date,
			mps_material_collection_code,
			mps_material_collection_category_id,
			mps_material_collection_category_code,
			mps_material_planning_type_id,
			mps_material_planning_type_name,
			mps_material_code,
			mps_material_name,
			mps_material_setof,
			mps_material_hsc,
			currency_symbol,
			(mps_ordersheet_item_price * mps_ordersheet_item_quantity_confirmed) AS total_cost_ordered
		FROM mps_ordersheet_items
	")
	->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
	->bind(Ordersheet_Item::DB_BIND_MATERIALS)
	->bind(Material::DB_BIND_PLANNING_TYPES)
	->bind(Material::DB_BIND_COLLECTIONS)
	->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
	->bind(Material::DB_BIND_CURRENCIES)
	->filter('ordersheets', "mps_ordersheet_id = $id")
	->order('mps_material_planning_type_name')
	->order('mps_material_collection_category_code')
	->order('mps_material_code')
	->order('mps_material_name')
	->fetchAll();
	
	if ($result) { 

		// workflow states
		$state_completed = $ordersheet->state()->isCompleted();
		$state_approved = $ordersheet->state()->isApproved();
		$state_revision = $ordersheet->state()->isRevision();
		$STATE_DISTRIBUTED = $ordersheet->state()->isDistributed();
		$state_expired = $ordersheet->state()->isExpired();
		$state_open = $ordersheet->state()->open;
		$icon_revision = ui::icon('warrning');
		
		// user role
		$administrator = $ordersheet->state()->canAdministrate();
		$user_owner = $ordersheet->state()->owner;

		// group statments
		$statment_on_preparation = $ordersheet->state()->onPreparation();
		$statment_on_completing = $ordersheet->state()->onCompleting();
		$statment_on_revision = $ordersheet->state()->onCompleting();
		$statment_on_confirmation = $ordersheet->state()->onConfirmation();
		$statment_on_distribution = $ordersheet->state()->onDistribution();
		
		foreach ($result as $row) {
			
			$item = $row['mps_ordersheet_item_id'];
			$planning = $row['mps_material_planning_type_id'];
			$collection = $row['mps_material_collection_category_id'];
			
			// quantities
			$quantity = $row['mps_ordersheet_item_quantity'];
			$quantity_proposed = $row['mps_ordersheet_item_quantity_proposed'];
			$quantity_approved = $row['mps_ordersheet_item_quantity_approved'];
			$quantity_confirmed = $row['mps_ordersheet_item_quantity_confirmed'];
			$quantity_shipped = $row['mps_ordersheet_item_quantity_shipped'];
			$quantity_distributed = $row['mps_ordersheet_item_quantity_distributed'];
			
			// if order sheet is archived
			// reset all NULL quantitis to Zero
			if ($STATE_DISTRIBUTED) {
				$quantity = ($quantity) ? $quantity : 0;
				$quantity_proposed = ($quantity_proposed) ? $quantity_proposed : 0;
				$quantity_approved = ($quantity_approved) ? $quantity_approved : 0;
			}
			
			// item prise
			$item_price = $row['mps_ordersheet_item_price'];
				
			// item ordered date
			$item_order_date = $row['mps_ordersheet_item_order_date'];
			
			// total prices
			if ($statment_on_preparation) {
				if ($state_approved) $totalprice = $item_price * $quantity_approved;
				elseif ($quantity_approved) $totalprice = $item_price*$quantity_approved;
				elseif ($quantity) $totalprice = $item_price*$quantity;
				elseif ($quantity_proposed) $totalprice = $item_price*$quantity_proposed;	
				else $totalprice = null;
			}
			elseif ($statment_on_confirmation) {				
				$totalprice = ($quantity_confirmed) ? $item_price*$quantity_confirmed : $item_price*$quantity_approved;
			} 
			else {
				if ($quantity_distributed) $totalprice = $item_price*$quantity_distributed;
				elseif($quantity_shipped) $totalprice = $item_price*$quantity_shipped;
				else $totalprice = ($quantity_confirmed) ? $item_price*$quantity_confirmed : $item_price*$quantity_approved;
			}
			
			// total costs 
			$total_cost = number_format($totalprice, $settings->decimal_places, '.', '');
				
			// datagrid
			$datagrid[$planning]['caption'] = $row['mps_material_planning_type_name'];
			$datagrid[$planning]['data'][$collection]['caption'] = $row['mps_material_collection_category_code'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_material_collection_code'] = $row['mps_material_collection_code'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_material_code'] = $row['mps_material_code'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_material_name'] = $row['mps_material_name'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_material_hsc'] = $row['mps_material_hsc'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_material_setof'] = $row['mps_material_setof'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_price'] = $row['mps_ordersheet_item_price'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['currency_symbol'] = strtoupper($row['currency_symbol']);
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_quantity_proposed'] = $quantity_proposed;
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_quantity'] = $quantity;
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_quantity_approved'] = $quantity_approved;
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_quantity_confirmed'] = $quantity_confirmed;
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_quantity_distributed'] = $quantity_distributed;
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_quantity_shipped'] = $quantity_shipped;
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_status'] = $row['mps_ordersheet_item_status'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['total_cost'] = $total_cost;
				
			// proposed quantities
			if ($columns['proposed']['edit']) {
				$dataloader['mps_ordersheet_item_quantity_proposed'][$item] = "<input type=text data=$item class=quantity_proposed name=mps_ordersheet_item_quantity_proposed[$item] value='$quantity_proposed' />";
			}
			
			// owner quantities
			if ($columns['quantity']['edit']) {
				
				if ($state_revision && $user_owner) {
					$quantitybox = ($row['mps_ordersheet_item_status']) ? true : false;
					$dataloader['mps_ordersheet_item_quantity'][$item] = ($row['mps_ordersheet_item_status']) ? "<input type=text data='$item' class=quantity name=mps_ordersheet_item_quantity[$item] value='$quantity' />" : $quantity;
				} else {
					$dataloader['mps_ordersheet_item_quantity'][$item] = "<input type=text data='$item' class=quantity name=mps_ordersheet_item_quantity[$item] value='$quantity' />";				
				}
			}
			
			// approved quantities
			if ($columns['approved']['edit']) {
				$dataloader['mps_ordersheet_item_quantity_approved'][$item] = "<input type=text data='$item' class=quantity_approved name=mps_ordersheet_item_quantity_approved[$item] value='$quantity_approved' />";
			}
			
			// confirmed quantities
			if ($columns['confirmed']['edit']) {
				$dataloader['mps_ordersheet_item_quantity_confirmed'][$item] = "<input type=text data='$item' class=quantity_confirmed name=mps_ordersheet_item_quantity_confirmed[$item] value='$quantity_confirmed' />";
			}
			
			// confirmed quantities
			if ($columns['shipped']['edit']) {
				$dataloader['mps_ordersheet_item_quantity_shipped'][$item] = "<input type=text data='$item' class=quantity_shipped name=mps_ordersheet_item_quantity_shipped[$item] value='$quantity_shipped' />";
			}
			
			// distributed quantities
			if ($columns['distributed']['edit']) {
				$dataloader['mps_ordersheet_item_quantity_distributed'][$item] = "<input type=text data='$item' class=quantity_distributed name=mps_ordersheet_item_quantity_distributed[$item] value='$quantity_distributed' />";
			}
			
			// dataloder: item in revision
			if ($administrator && $buttons['revision'] && !$item_order_date) {
				if ($state_completed) {
					$checked = ($row['mps_ordersheet_item_status']) ? "checked=checked" : null;
					$dataloader['mps_ordersheet_item_status'][$item] = "<input type=checkbox data='$item' class=item_revision $checked name=mps_ordersheet_item_status[$item] value=1 />";
				}
				else { 
					$dataloader['mps_ordersheet_item_status'][$item] = ($row['mps_ordersheet_item_status']) ? $icon_revision : null;
				}
			} 
		}
		
		
		if ($datagrid) { 
				
			foreach ($datagrid as $key => $row) {
				
				$planningType = $row['caption'];
				
				$list .= "<h5>$planningType</h5>";
				
				foreach ($row['data'] as $subkey => $value) {
					
					$collectionCode = $value['caption'];
					$list .= "<h6>$collectionCode</h6>";
						
					$totalprice = 0;
					$tableKey = $key."-".$subkey;
					
					$table = new Table(array(
						'id' => $tableKey
					));
						
					$table->datagrid = $value['data'];
					$table->dataloader($dataloader);

					$table->mps_material_collection_code(
						Table::ATTRIBUTE_NOWRAP,
						'width=10%'
					);
					
					$table->mps_material_code(
						Table::ATTRIBUTE_NOWRAP,
						'width=10%'
					);
					
					$table->mps_material_name();

					$table->mps_material_hsc(
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);
					
					$table->mps_material_setof(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);
					
					$table->mps_ordersheet_item_price(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);
					
					$table->currency_symbol(
						Table::ATTRIBUTE_NOWRAP,
						'width=20px'
					);
					
					// proposed quantites
					if ($columns['proposed']['show']) {
						
						$table->mps_ordersheet_item_quantity_proposed(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
						
						if ($columns['proposed']['edit']) {
							$table->mps_ordersheet_item_quantity_proposed(
								Table::PARAM_GET_FROM_LOADER
							);
						}
					}
					
					// owner quantites
					if ($columns['quantity']['show']) {
						
						$table->mps_ordersheet_item_quantity(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
						
						if ($columns['quantity']['edit']) {
							$table->mps_ordersheet_item_quantity(
								Table::PARAM_GET_FROM_LOADER
							);
						}
					}
				
					// approved quantities
					if($columns['approved']['show']) {
						
						$table->mps_ordersheet_item_quantity_approved(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
						
						if ($columns['approved']['edit']) {
							$table->mps_ordersheet_item_quantity_approved(
								Table::PARAM_GET_FROM_LOADER
							);
						}
					}
					
					// confirmed quantites
					if ($columns['confirmed']['show']) {
						
						$table->mps_ordersheet_item_quantity_confirmed(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
						
						if ($columns['confirmed']['edit']) {
							$table->mps_ordersheet_item_quantity_confirmed(
								Table::PARAM_GET_FROM_LOADER
							);
						}
					}
					
					// shipped quantites
					if ($columns['shipped']['show']) {
						
						$table->mps_ordersheet_item_quantity_shipped(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
						
						if ($columns['confirmed']['edit']) {
							$table->mps_ordersheet_item_quantity_shipped(
								Table::PARAM_GET_FROM_LOADER
							);
						}
					}
					
					// distributed quantites
					if ($columns['distributed']['show']) {
						
						$table->mps_ordersheet_item_quantity_distributed(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
						
						if ($columns['distributed']['edit']) {
							$table->mps_ordersheet_item_quantity_distributed(
								Table::PARAM_GET_FROM_LOADER
							);
						}
					}
					
					// total price
					$table->total_cost(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						Table::FORMAT_NUMBER,
						Table::FORMAT_NUMBER,
						'width=5%'
					);
					
					$has_item_in_revision = false;
					
					// total collection price
					foreach ($value['data'] as $key => $array) {
						
						$totalprice = $totalprice+$array['total_cost'];
						
						if (!$has_item_in_revision && $dataloader['mps_ordersheet_item_status'][$key]) {
							$has_item_in_revision = true;
						}
					}
						
					// items in revision
					if ($has_item_in_revision) {
						$table->mps_ordersheet_item_status(
							Table::PARAM_GET_FROM_LOADER,
							Table::ATTRIBUTE_ALIGN_CENTER,
							'width=20px'
						);
					}
					
					$total_cost = number_format($totalprice, $settings->decimal_places, '.', '');
					
					$list .= $table->render();
					$list .= "<div class='totoal-collection'>".$translate->total_cost." (<em>$collectionCode</em>): <b class='total-items $tableKey'>$total_cost</b></div>";
				}
			}
		}
	}
	else {
		$list = html::emptybox($translate->empty_result);
	}

?>
<script type="text/javascript" src="/public/scripts/textarea.expander.js"></script>
<script type="text/javascript" src="/public/js/ordersheet.item.list.js"></script>
<style type="text/css">

	#ordersheet_items {
		width: 1300px;
		padding-right: 40px;
	}
	
	h6 { 
		display: block; 
		margin-bottom: 5px; 
		font-size: .8em;
		font-weight: 500;
	}
	
	h5 { 
		display: block;
		margin: 40px 0 20px; 
		font-size: 1em;
		font-variant: small-caps;
		border-bottom: 1px solid silver;
		color: gray;
	}
	
	.totoal-collection {
		display: block;
		width: 100%;
		font-size: .75em;
		text-align: right;
		padding-top: 5px;
		margin-bottom: 20px;
	}
	
	.totoal-collection b {
		padding-left: 5px;
	}
	
	.ordersheet_items input[type=text] {
		width: 50px !important;
	}
	
	.modalbox {
		width: 800px;
	}
	
	.modalbox-content {
		max-height: 760px;
		overflow-y: auto;
	}
	
	.modalbox input[type=text] {
		width: 750px !important;
	}
	
	.modalbox textarea {
		width: 750px !important;
		height: 200px !important;
	}
	
	.listing td {
		border-bottom: 1px solid silver;
	}
	
</style>
<div id="ordersheet_items">
	<form class="ordersheet_items default" id="itemform" action="/applications/helpers/ordersheet.item.save.php" method=post >
		<input type="hidden" name="application" value="<?php echo $request->application; ?>" />
		<input type="hidden" name="id" value="<?php echo $ordersheet->id; ?>" />
		<input type="hidden" name="redirect" value="<?php echo $data['redirect']; ?>" />
		<input type="hidden" name="action" id="action" />
		<input type="hidden" name="state" id="state" />
		<?php 
			echo $list; 
		?>
	</form>
	<div class="actions">
	<?php 

		if ($buttons['back']) {
			echo ui::button(array(
				'id' => 'back',
				'icon' => 'back',
				'href' => $buttons['back'],
				'label' => $translate->back
			));
		}
			
		if ($buttons['approve_sendmail']) {
			echo ui::button(array(
				'id' => 'approve_sendmail',
				'class' => 'submit sendmail',
				'icon' => 'mail',
				'label' => $translate->approve.' & '.$translate->sendmail,
				'state' => Workflow_State::STATE_APPROVED,
				'href' => 'approve',
				'template' => 2
			));
		}
		
		if ($buttons['approve']) {
			echo ui::button(array(
				'id' => 'approve',
				'class' => 'submit',
				'icon' => 'like',
				'label' => $translate->approve,
				'state' => Workflow_State::STATE_APPROVED,
				'href' => 'approve'
			));
		}
		
		if ($buttons['revision']) {
			echo ui::button(array(
				'id' => 'revision',
				'class' => 'submit sendmail',
				'icon' => 'reload',
				'label' => $translate->revision,
				'state' => Workflow_State::STATE_REVISION,
				'href' => 'revision',
				'template' => 3
			));
		}
		
		if ($buttons['submit']) {
			echo ui::button(array(
				'id' => 'submit',
				'class' => 'submit',
				'icon' => 'like',
				'label' => $translate->submit,
				'state' => Workflow_State::STATE_COMPLETED,
				'href' => 'submit'
			));
		}
	
		if ($buttons['save']) {
			echo ui::button(array(
				'id' => 'save',
				'class' => 'submit',
				'icon' => 'save',
				'label' => $translate->save,
				'href' => 'save'
			));
		}
	?>
	</div>
</div>
<div class='modalbox-container'>
	<div id="modalbox" class="modalbox modal">
		<div class=modalbox-header >
			<div class='title'><?php echo $translate->sendmail; ?></div>
			<div class='subtitle'><?php echo $ordersheet->header(); ?>"<strong></strong></div>
		</div>
		<div class=modalbox-content-container >
			<div class=modalbox-content>
				<form id="mailform" action="/applications/helpers/ordersheet.sendmail.php">
					<input type="hidden" name="application" value="<?php echo $request->application; ?>" />
					<input type="hidden" name="controller" value="<?php echo $request->controller; ?>" />
					<input type="hidden" name="archived" value="<?php echo $request->archived; ?>" />
					<input type="hidden" name="action" value="<?php echo $request->action; ?>" />
					<input type="hidden" name="ordersheets" value="<?php echo $ordersheet->id; ?>" />
					<input type="hidden" name="mail_template_id" id="mail_template_id" />
					<div class="modal-input-container"><input type="text" name="mail_template_subject" id="mail_template_subject" class="required" /></div>
					<div class="modal-input-container"><textarea name="mail_template_tex" id="mail_template_text" class="required expand" ></textarea></div>
				</form>
			</div>
		</div>
		<div class=modalbox-footer >
			<div class=modalbox-actions >
				<a id=cancel class='button'>
					<span class="icon cancel"></span>
					<span class="label"><?php echo $translate->cancel ?></span>
				</a>
				<a id="sendmail" class='button'>
					<span class="icon mail"></span>
					<span class="label"><?php echo $translate->sendmail ?></span>
				</a>
			</div>
		</div>
	</div>
</div>