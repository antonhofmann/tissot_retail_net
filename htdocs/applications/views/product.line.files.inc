<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$(".modal").on("click", function() {
		
		var path = $(this).attr('tag');
		var title = $(this).parent().next().find('a').text();
	
		if (isImage(path)) {

			retailnet.modal.show(path, {
				title: (title) ? title : ''
			});
			
		} else {
			window.open('http://'+location.host+path);
		}
	});
});
</script>
<style type="text/css">

	#files { 
		width: 700px; 
	}
	
	td span {
		display:block; 
		color:gray; 
		font-size:11px;
	}
	
	td.fileicon {
		width: 32px;
	}
	
	td.date {
		width: 100px;
	}
	
	h6 { 
		display:block; 
		margin: 40px 0 5px; 
		font-size: .825em;
	}

	table { 
		margin-bottom:20px; 
		border-style: solid; 
		border-color: silver; 
	}
	
	.file-extension {
		cursor: pointer;
	}
	
	span.order_title {
		color: black;
	}

	span.filename {display:block; font-weight:600; color:#0050CC; cursor:pointer; font-size: 11px;}
	span.filename:hover { color: black; }
	
</style>
<div id="files">
	<div class="table-toolbox">
		<form class=toolbox >
			<?php
				if ($buttons['add'])  {
					echo ui::button(array(
						'id' => 'add',
						'icon' => 'add',
						'href' => $buttons['add'],
						'label' => $translate->add_new
					));
				}
			?>
		</form>
	</div>
	<?php
		if ($files) {
			
			$table = new Table();
			$table->datagrid = $files;
			$table->fileicon();
			$table->title("href=/catalog/productlines/files/$id");
			$table->file_purpose_name(Table::ATTRIBUTE_NOWRAP, 'width=20%');
			$table->date('width=80px');
			
			echo $table->render();
		}
	?>
</div>

<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>