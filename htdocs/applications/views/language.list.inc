<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	var languages = $('#languages').tableLoader({
		url: '/applications/helpers/language.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">

	#languages { 
		width: 500px; 
	}
	
</style>
<?php 
	echo $request->form($requestFields);
?>
<div id="languages"></div>