<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	
	$form = new Form(array(
		'id' => 'mps_sap_hq_ordertypeForm',
		'action' => '/applications/helpers/company.mps_sap_hq_ordertype.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_sap_hq_ordertype_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_sap_hq_ordertype_address_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_sap_hq_ordertype_customer_number(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	
	$form->mps_sap_hq_ordertype_shipto_number(
		Form::TYPE_TEXT
	);

	$form->mps_sap_hq_ordertype_sap_hq_ordertype_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->mps_sap_hq_ordertype_sap_hq_orderreason_id(
		Form::TYPE_SELECT
	);

	$form->mps_sap_hq_ordertype_is_default(
		Form::TYPE_CHECKBOX
	);

	$form->mps_sap_hq_ordertype_is_free_goods(
		Form::TYPE_CHECKBOX
	);

	$form->mps_sap_hq_ordertype_is_for_dummies(
		Form::TYPE_CHECKBOX
	);
	
		
		
	$form->fieldset('mps_sap_hq_ordertype', array(
		'mps_sap_hq_ordertype_customer_number',
		'mps_sap_hq_ordertype_shipto_number',
		'mps_sap_hq_ordertype_sap_hq_ordertype_id',
		'mps_sap_hq_ordertype_sap_hq_orderreason_id',
		'mps_sap_hq_ordertype_is_default',
		'mps_sap_hq_ordertype_is_free_goods',
		'mps_sap_hq_ordertype_is_for_dummies'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>

<style type="text/css">

	#mps_sap_hq_ordertypeForm { width: 700px; }
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>