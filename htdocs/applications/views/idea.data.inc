<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'idea_form',
		'action' => '/applications/helpers/idea.save.php',
		'method' => 'post'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->idea_id(
		Form::TYPE_HIDDEN
	);
	
	$form->idea_idea_category_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->idea_title(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->idea_text(
		Form::TYPE_TEXTAREA,
		Validata::PARAM_REQUIRED,
		FORM::PARAM_LABEL
	);
	
	$form->fieldset('Idea', array(
		'idea_idea_category_id',
		'idea_title',
		'idea_text'
	));
	
	$form->idea_priority(
		Form::TYPE_SELECT
	);
	
	$form->idea_killed(
		Form::TYPE_CHECKBOX
	);
	

	$form->fieldset('State', array(
		'idea_priority',
		'idea_killed'
	));
	
	if ($dataloader['files']) {
		

		$form->files(
			Form::TYPE_AJAX,
			Form::PARAM_SHOW_VALUE,
			Form::PARAM_NO_LABEL
		);
		
		$form->fieldset('Files', array('files'));
		
		foreach ($dataloader['files'] as $i => $file) {
			
			$btnDelete = $file['delete'] ? "&nbsp;&nbsp;|&nbsp;&nbsp;<a href='".$file['delete']."' id='delete_file' class='dialog'>Delete</a>" : null;
			
			$data['files'] .= "
				<div class='file-container' id='file-$i' >
					<span class='preview'><img src='".$file['thumb']."' /></span>
					<a href='".$file['download']."' target='_blank' >Download</a>
					$btnDelete
				</div>		
			";
		}
	}
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
$(document).ready(function() {

	var form = $("#idea_form");

	// loader instance
	retailnet.loader.init();

	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) {

			// close wait screen
			retailnet.loader.hide();

			if (json) {

				if (json.response && json.redirect) {
					window.location = json.redirect;
				}

				if (json.message) {
					retailnet.notification.show(json.message);
				}
			}
		}
	});


	// submit form
	$("#save").click(function(e) { 

		e.preventDefault();

		var validata = form.validationEngine('validate');

		// close all notofications
		retailnet.notification.hide();

		if (validata) {

			// show wait screen
			retailnet.loader.show();
			
			form.submit();
		}

		return false;
	});


	// tooltips
	$('.-tooltip[title]').qtip({
		position: {corner: {target:'topRight', tooltip:'leftTop'}},
		style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
	});


	// show dialog
	$('.dialog').click(function(e) {

		e.preventDefault();

		var button = $(this);
		var target = '#'+button.attr('id')+'_dialog';
		$('a.apply', $(target)).attr('href', button.attr('href'))
		retailnet.modal.show(target);
	});


	// cancel dialog action
	$('.cancel').click(function(e) {

		e.preventDefault();
		e.stopPropagation();
		
		retailnet.modal.hide();
		
		return false;
	});	


	// confirm dialog action
	$('.apply').bind('click', function(e) {

		e.preventDefault();

		var self = $(this);

		// close modal screen
		retailnet.modal.hide();

		// show wait screen
		retailnet.loader.show();
		
		retailnet.ajax.json(self.attr('href')).done(function(xhr) {

			if (xhr.redirect) {
				window.location = xhr.redirect;
			}

			if (xhr.message) {
				retailnet.notification.show(xhr.message);
			}

			// remove file container
			if (xhr.remove) {
				$('#file-'+xhr.remove).remove();
			}
			
		}).complete(function() {

			// close wait screen
			retailnet.loader.hide();
		});
	})
	

});
</script>

<style type="text/css">

	#idea_form { 
		width: 700px; 
	}

	#idea_text { 
		width: 440px;
		height: 200px; 
	}
	
	.file-container {
		display: block;
		width: 160px;
		float: left;
		text-align: center;
		margin: 0 30px;
	}
	
	
	.file-container .preview {
		display: block;
		width: 80px;
		height: 80px;
		margin: 0 auto 10px;
		text-align: center;
		vertical-align: center;
	}
	
	
	.file-container img {
		display: block;
		max-width: 80px;
		margin: auto;
	}
	
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'class' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'class' => 'apply',
				'label' => $translate->yes
			),
		)
	));
	
	echo ui::dialogbox(array(
		'id' => 'delete_file_dialog',
		'title' => $translate->delete_file,
		'content' => $translate->dialog_delete_file,
		'buttons' => array(
			'delete_file_cancel' => array(
				'icon' => 'cancel',
				'class' => 'cancel',
				'label' => $translate->cancel
			),
			'delete_file_apply' => array(
				'icon' => 'apply',
				'class' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>