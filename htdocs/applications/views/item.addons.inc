<?php 
	$user = User::instance();
	$translate = Translate::instance();
?>

<style type="text/css">

	#items { 
		width: 700px; 
	}
	
</style>
<script type="text/javascript">
	$(document).ready(function() {
		
	});
</script>
<div id="items">
	<div class="table-toolbox">
		<form class="toolbox" >
			<?php 
				if ($buttons['add'])  {
					echo ui::button(array(
						'id' => 'add',
						'icon' => 'add',
						'href' => $buttons['add'],
						'label' => $translate->add_new
					));
				}
			?>
		</form>
	</div>
<?php 

	if ($datagrid) {

		$table = new Table();
		$table->datagrid = $datagrid;
		$table->item_code(Table::ATTRIBUTE_NOWRAP, 'width=20%');
		$table->item_name("href=$link");
		$table->addon_package_quantity(Table::ATTRIBUTE_NOWRAP, 'width=10%');
		$table->addon_min_packages(Table::ATTRIBUTE_NOWRAP, 'width=10%');
		$table->addon_max_packages(Table::ATTRIBUTE_NOWRAP, 'width=10%');

		$table->caption('addon_min_packages', 'Min');
		$table->caption('addon_max_packages', 'Max');

		echo $table->render();
	}
?>
</div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>