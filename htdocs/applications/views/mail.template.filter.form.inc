<?php 
	
	$form = new Form(array(
		'id' => 'filterForm',
		'action' => '/applications/helpers/mail.template.filter.save.php',
		'method' => 'post'
	));
	
	$form->mail_template_filter_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mail_template_filter_template_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mail_template_filter_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mail_template_filter_description(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);
	
	$form->mail_template_filter_data(
		Form::TYPE_TEXTAREA,
		Validata::PARAM_REQUIRED
	);
	
	$form->fieldset('Filter Description', array(
		'mail_template_filter_name',
		'mail_template_filter_description',
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => Translate::instance()->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => Translate::instance()->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => Translate::instance()->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#filterForm");

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			onAjaxFormComplete: function(status, form, json, options) {

				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

				if (json) {

					if (json.response && json.redirect) {
						window.location=json.redirect;
					}

					if (json.response && json.id) {
						id.val(json.id);
					}

					if (json.message) {
						$.jGrowl(json.message, { 
							sticky: (json.response) ? false : true,
							theme: (json.response) ? 'message' : 'error'
						});
					}
				}
			}
		});
		
		$("#save").click(function(event) { 

			event.preventDefault();
			
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

			var validata = form.validationEngine('validate');

			if (validata) {
				form.submit();
			}
			
			return false;
		});

		$('.dialog').click(function(event) {

			event.preventDefault();

			var button = $(this);
			
			$('#apply, a.apply').attr('href', button.attr('href'));
			
			retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
		});

		$('#cancel').bind('click', function(event) {
			event.preventDefault();
			retailnet.modal.close();
			return false;
		});	
	});
</script>

<style type="text/css">

	#filterForm { 
		width: 700px; 
	}
	
	form .mail_template_code input {
		width: 440px;
	}
	
	form .mail_template_filter_description textarea {
		width: 440px;
		height: 50px;
	}
	
	form .mail_template_filter_data {
		margin: 20px 0 0 !important;
		padding: 0 !important;
		background-color: white;
	}
	
	form .mail_template_filter_data textarea {
		width: 690px;
		height: 160px;
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => Translate::instance()->delete,
		'content' => Translate::instance()->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => Translate::instance()->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => Translate::instance()->yes
			),
		)
	));
?>