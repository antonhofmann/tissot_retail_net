<?php 
	$translate = Translate::instance();
?>
<script type="text/javascript">

	jQuery(document).ready(function() {

		var file = $('a.show-file').attr('href');

		if (!isImage(file)) {
			$('.icon-ui').removeClass('picture').addClass('attachment');
		}

		$('a.show-file').click(function() {
			if (isImage(file)) {

				retailnet.modal.show(file, { 
					title: $('a.show-file').html(),
					closeBtn: true
				});
				
			} else {
				var id = new Date().getTime();
				window.open('http://'+location.host+file+'?id='+id);
			}

			return false;
		});
	});
	
</script>
<style type="text/css">

	.announcement {
		display: block;
		width: 800px;
	}
		
	.filerow {
		display: block;
		margin-top: 20px;
		line-height: 20px;
		padding-top: 20px;
	}
	
	.filerow a {
		padding-left: 5px;
		position: relative;
		top: -5px;
	}
	
	.ie7 .filerow a {
		top: -2px;
	}
	
</style>
<div class=announcement >
	<div class="-box-header">
		<strong><?php echo $data['mps_announcement_title']; ?></strong>
	</div>
	<div class="-box">
		<div class="-content">
			<?php 
				
				echo $data['mps_announcement_text'];
				$icon = (check::image($data['mps_announcement_file'])) ? ui::icon('picture') : ui::icon('attachment');
				
				if ($data['mps_announcement_file']) {
					echo "
						<p class='filerow'>
							$icon
							<a class='show-file' href='".$data['mps_announcement_file']."' >".$data['mps_announcement_file_title']."</a>
						</p>
					";
				} 
			?>
		</div>
	</div>
	<div class="actions">
		<?php 
		
			if ($buttons['back']) {
				echo ui::button(array(
					'id' => 'back',
					'icon' => 'back',
					'href' => $buttons['back'],
					'caption' => $translate->back
				));
			}
		
			if ($buttons['edit']) {
				echo ui::button(array(
					'id' => 'edit',
					'icon' => 'edit',
					'href' => $buttons['edit'],
					'caption' => $translate->edit
				));
			}
		?>
	</div>
</div>