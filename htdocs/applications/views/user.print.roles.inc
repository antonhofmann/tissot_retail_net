<?php


if ($datagrid) {

	foreach ($datagrid as $app => $roles) {

		$list .= "<h4>".strtoupper($app)."</h4>";

		$list .= "<table class='listing over-effects' >";

		$list .= "<tr>";
		$list .= "<th width='20' ><input type='checkbox' class='group-role' value='$app' /></th>";
		$list .= "<th width='10%' >Code</th>";
		$list .= "<th>Name</th>";
		$list .= "<th width='30%' >Responsible</th>";
		$list .= "</tr>";
		$list .= "<tbody>";

		foreach ($roles as $id => $role) {
			$list .= "<tr>";
			$list .= "<td width='20' ><input type='checkbox' class='role' name='roles[$id]' value='$id'  /></td>";
			$list .= "<td>{$role[role_code]}</td>";
			//$list .= "<td><a href='$link/$id' >{$role[role_name]}</a></td>";
			$list .= "<td>{$role[role_name]}</td>";
			$list .= "<td>{$role[user]}</td>";
			$list .= "</tr>";
		}

		$list .= "</tbody>";
		$list .= "</table>";
	}
}
?>
<style type="text/css">
	h4 { font-size: 14px; margin: 40px 0 10px; }
	.list-container h4:first-child {margin-top: 0; }

	.checkbox-filters { display: inline-block; vertical-align: top; padding-top: 2px;}
	.filter-caption {display: inline-block; vertical-align: top; padding: 7px 10px 0 12px; font-size: 12px; font-weight: 600;}
</style>
<script type="text/javascript">
	$(document).ready(function() {

		$(document).on('change', 'input.role', function(e) {
			var self = $(this);
			var group = self.closest('table');
			$('input.group-role', group).trigger('changed');
		})

		$(document).on('change', 'input.group-role', function(e) {
			var self = $(this);
			var group = self.closest('table');
			var checked = self.is(':checked');
			var checkboxes = [];
			$('input.role', group).prop('checked', checked);			
		})

		$(document).on('click', '#selectall', function(e) {

			e.stopPropagation();

			var self = $(this);
			var checked = self.hasClass('all') ? false : true;
			var caption = self.data('caption');
			var title = $('.label', self).text();

			self.data('caption', title);
			$('.label', self).text(caption);

			self.toggleClass('all');
			$('input.group-role').prop('checked', checked).trigger('change');

			return;
		})

		// total group recipients
		$('input.group-role').on('changed', function(e) {

			e.stopImmediatePropagation();

			var self = $(this);
			var group = self.closest('table'); 
			var totalRoles = $('input.role', group).length;
			var totalSelectedRoles = $('input.role:checked', group).length;
			
			self.prop('checked', totalRoles==totalSelectedRoles ? true : false)
		})

		$(document).on('click', '#print', function(e) {

			e.preventDefault();
			e.stopPropagation();

			retailnet.notification.hide();

			var self = $(this);
			var totalSelected = $('input.role:checked').length;

			if (!totalSelected) {
				retailnet.notification.error("Please, select at least one Role.");
				return;
			}

			retailnet.loader.show();
			
			retailnet.ajax.json(self.attr('href'), $('.toolbox, .print-roles').serialize()).done(function(xhr) {

				if (xhr && xhr.file) window.location = xhr.file;
				else retailnet.notification.error("System error, please contact system administrator.");

			}).complete(function() {
				retailnet.loader.hide();
			});

			return;
		})

	})
</script>
<div class="list-container">
	<div class='table-toolbox'>
		<form class="toolbox" method="post">
			<a id="print" href="/applications/helpers/user.print.roles.php" class="button">
				<span class="icon print"></span>
				<span class="label">Print</span>
			</a>
			<a id="selectall" href="#" class="button" data-caption="Deselect all">
				<span class="icon icon43"></span>
				<span class="label">Select All</span>
			</a>
			<span class="checkbox-filters">
			<?php 
				if ($clienttypes) {
					echo "<span class='filter-caption'>Client Types: </span>";
					foreach ($clienttypes as $id => $name) {
						echo "<label class='switch switch-green' style='width:95px'>
								<input type='checkbox' class='switch-input' name='clienttypes[$id]' value='$id' >
								<span class='switch-label' data-on='$name' data-off='$name'></span>
								<span class='switch-handle'></span>
							</label>
						";
					}
				}
			?>
			<span>
		</form>
	</div>
	<form class="print-roles"><?php echo $list; ?></form>
</div>