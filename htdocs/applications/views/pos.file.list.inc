<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#pos_files').tableLoader({
		url: '/applications/helpers/pos.file.individual.php',
		data: $('.request').serializeArray()
	});

	$('#project_files').tableLoader({
		url: '/applications/helpers/pos.file.projects.php',
		data: $('.request').serializeArray()
	});

	$(document).delegate(".modal", "click", function() {
		
		var path = $(this).attr('tag');
		var title = $(this).parent().next().find('a').text();
	
		if (isImage(path)) {

			retailnet.modal.show(path, {
				title: (title) ? title : ''
			});
			
		} else {

			window.open('http://'+location.host+path);
		}
	});
	
});
</script>
<style type="text/css">

	#pos_files,
	#project_files { 
		width: 700px; 
	}
	
	td span {
		display:block; 
		color:gray; 
		font-size:11px;
	}
	
	td.fileicon {
		width: 32px;
	}
	
	td.date {
		width: 100px;
	}
	
	h4 { 
		display:block; 
		margin: 40px 0 5px; 
		font-size: 1em;
		border-bottom: 1px solid silver;
	}
	
	h6 { 
		display:block; 
		margin: 40px 0 5px; 
		font-size: .825em;
	}

	#pos_files table,
	#project_files table { 
		width: 660px;
		margin-bottom:20px; 
		border-style: solid; 
		border-color: silver; 
	}
	
	.file-extension {
		cursor: pointer;
	}
	
	span.order_title {
		color: black;
	}

	span.filename {display:block; font-weight:600; color:#0050CC; cursor:pointer; font-size: 11px;}
	span.filename:hover { color: black; }
	
</style>
<?php 
	echo $request->form($requestFields);
?>
<div id="pos_files"></div>
<div id="project_files"></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>