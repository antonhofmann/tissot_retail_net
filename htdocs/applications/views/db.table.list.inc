<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	var fileTypes = $('#integrity_tables').tableLoader({
		url: '/applications/helpers/db.table.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">

	#integrity_tables { 
		width: 900px; 
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="integrity_tables"></div>