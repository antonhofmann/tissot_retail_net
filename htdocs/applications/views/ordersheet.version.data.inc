<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'ordersheet',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->mps_mastersheet_year(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE
	);
	
	$form->mps_mastersheet_name(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE
	);
	
	$form->mps_ordersheet_workflowstate_id(
		Form::TYPE_SELECT		
	);
	
	$form->mps_ordersheet_comment(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL	
	);
	
	$form->mps_ordersheet_openingdate(
		Form::TYPE_TEXT
	);
	
	$form->mps_ordersheet_closingdate(
		Form::TYPE_TEXT
	);
	
	$form->fieldset('ordersheet', array(
		'mps_mastersheet_year',
		'mps_mastersheet_name',
		'mps_ordersheet_workflowstate_id',
		'mps_ordersheet_comment',
		'mps_ordersheet_openingdate',
		'mps_ordersheet_closingdate'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['print']) {
		$form->button(ui::button(array(
			'id' => 'export',
			'icon' => 'print',
			'href' => $buttons['print'],
			'label' => $translate->print
		)));
	}
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'label' => $translate->delete,
			'href' => $buttons['delete']
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
	
	// db application model
	$model = new Model(Request::instance()->application);
	
	// ordersheet items
	$result = $model->query("
		SELECT 
			mps_ordersheet_version_item_id,
			mps_ordersheet_version_item_price,
			mps_ordersheet_version_item_quantity,
			mps_ordersheet_version_item_quantity_proposed,
			mps_ordersheet_version_item_quantity_approved,
			mps_ordersheet_version_item_quantity_confirmed,
			mps_ordersheet_version_item_quantity_shipped,
			mps_ordersheet_version_item_quantity_distributed,
			mps_ordersheet_version_item_order_date,
			mps_material_collection_code,
			mps_material_collection_category_id,
			mps_material_collection_category_code,
			mps_material_planning_type_id,
			mps_material_planning_type_name,
			mps_material_code,
			mps_material_name,
			mps_material_setof,
			mps_material_hsc,
			currency_symbol
		FROM mps_ordersheet_version_items
	")
	->bind(Ordersheet_Version_Item::DB_BIND_MATERIALS)
	->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
	->bind(Material::DB_BIND_COLLECTIONS)
	->bind(Material::DB_BIND_PLANNING_TYPES)
	->bind(Material::DB_BIND_CURRENCIES)
	->filter('ordersheets', "mps_ordersheet_version_item_ordersheetversion_id = $version")
	->order('mps_material_planning_type_name')
	->order('mps_material_collection_category_code')
	->order('mps_material_code')
	->order('mps_material_name')
	->fetchAll();

	
	if ($result) {
	
		$ordersheet = new Ordersheet($application);
		$ordersheet->read($id);
		
		// group statments
		$statment_on_preparation = $ordersheet->state()->onPreparation();
		$statment_on_completing = $ordersheet->state()->onCompleting();
		$statment_on_revision = $ordersheet->state()->onCompleting();
		$statment_on_confirmation = $ordersheet->state()->onConfirmation();
		$statment_on_distribution = $ordersheet->state()->onDistribution();
	
		foreach ($result as $row) {
				
			$item = $row['mps_ordersheet_version_item_id'];
			$planning = $row['mps_material_planning_type_id'];
			$collection = $row['mps_material_collection_category_id'];
			$quantity = $row['mps_ordersheet_version_item_quantity'];
			$quantity_proposed = $row['mps_ordersheet_version_item_quantity_proposed'];
			$quantity_approved = $row['mps_ordersheet_version_item_quantity_approved'];
			$quantity_ordered = $row['mps_ordersheet_version_item_quantity_confirmed'];
			$quantity_delivered = $row['mps_ordersheet_version_item_quantity_distributed'];
			$item_price = $row['mps_ordersheet_version_item_price'];
			$item_order_date = $row['mps_ordersheet_version_item_order_date'];

			// total prices
			if ($statment_on_preparation) {
				if ($state_approved) $totalprice = $item_price * $quantity_approved;
				elseif ($quantity_approved) $totalprice = $item_price*$quantity_approved;
				elseif ($quantity) $totalprice = $item_price*$quantity;
				elseif ($quantity_proposed) $totalprice = $item_price*$quantity_proposed;	
				else $totalprice = null;
			}
			elseif ($statment_on_confirmation) {				
				$totalprice = ($quantity_confirmed) ? $item_price*$quantity_confirmed : $item_price*$quantity_approved;
			} 
			else {
				if ($quantity_distributed) $totalprice = $item_price*$quantity_distributed;
				elseif($quantity_shipped) $totalprice = $item_price*$quantity_shipped;
				else $totalprice = ($quantity_confirmed) ? $item_price*$quantity_confirmed : $item_price*$quantity_approved;
			}
			
			// total costs 
			$total_cost = number_format($totalprice, $settings->decimal_places, '.', '');
	
			$datagrid[$planning]['caption'] = $row['mps_material_planning_type_name'];
			$datagrid[$planning]['collections'][$collection]['caption'] = $row['mps_material_collection_category_code'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_collection_code'] = $row['mps_material_collection_code'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_code'] = $row['mps_material_code'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_name'] = $row['mps_material_name'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_hsc'] = $row['mps_material_hsc'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_material_setof'] = $row['mps_material_setof'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_version_item_price'] = $row['mps_ordersheet_version_item_price'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['currency_symbol'] = strtoupper($row['currency_symbol']);
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_version_item_quantity_proposed'] = $row['mps_ordersheet_version_item_quantity_proposed'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_version_item_quantity'] = $row['mps_ordersheet_version_item_quantity'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_version_item_quantity_approved'] = $row['mps_ordersheet_version_item_quantity_approved'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_version_item_quantity_confirmed'] = $row['mps_ordersheet_version_item_quantity_confirmed'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_version_item_quantity_shipped'] = $row['mps_ordersheet_version_item_quantity_shipped'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_version_item_quantity_distributed'] = $row['mps_ordersheet_version_item_quantity_distributed'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['mps_ordersheet_version_item_status'] = $row['mps_ordersheet_version_item_status'];
			$datagrid[$planning]['collections'][$collection]['items'][$item]['total_price']  = $total_cost;
		}
	
	
		if ($datagrid) {
	
			foreach ($datagrid as $key => $row) {
	
				$planningType = $row['caption'];
				$list .= "<h5>$planningType</h5>";
	
				foreach ($row['collections'] as $subkey => $value) {
						
					$collectionCode = $value['caption'];
					$list .= "<h6>$collectionCode</h6>";
	
					$totalprice = 0;
					$tableKey = $key."-".$subkey;
						
					$table = new Table(array('id'=>$tableKey));
					$table->datagrid = $value['items'];
						
					$table->mps_material_collection_code(
					Table::ATTRIBUTE_NOWRAP,
						'width=10%'
					);
						
					$table->mps_material_code(
					Table::ATTRIBUTE_NOWRAP,
						'width=10%'
					);
						
					$table->mps_material_name(
						Table::ATTRIBUTE_NOWRAP
					);

					$table->mps_material_hsc(
					Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);
						
					$table->mps_material_setof(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);
						
					$table->mps_ordersheet_version_item_price(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);
						
					$table->currency_symbol(
						Table::ATTRIBUTE_NOWRAP,
						'width=20px'
					);
						
					if ($columns['proposed']['show']) {
						$table->mps_ordersheet_version_item_quantity_proposed(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
					}
					
					if ($columns['quantity']['show']) {
						$table->mps_ordersheet_version_item_quantity(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
					}
					
					// approved quantities
					if($columns['approved']['show']) {
					
						$table->mps_ordersheet_version_item_quantity_approved(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
					}
					
					if ($columns['confirmed']['show']) {
						$table->mps_ordersheet_version_item_quantity_confirmed(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
					}
					
					if ($columns['shipped']['show']) {
						$table->mps_ordersheet_version_item_quantity_shipped(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
					}
					
					if ($columns['distributed']['show']) {
						$table->mps_ordersheet_version_item_quantity_distributed(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
					}
	
					// approved total price
					$table->total_price(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);
	
					// total collection price
					foreach ($value['items'] as $key => $array) {
						$totalprice = $totalprice+$array['total_price'];
					}
	
					$totalprice = number_format($totalprice, $settings->decimal_places, '.', '');
						
					$list .= $table->render();
					
					$list .= "<div class='totoal-collection'>".$translate->total_cost." (<em>".$value['caption']."</em>): <b class='total-items $tableKey'>$totalprice</b></div>";
				}
			}
		}
	}

?>
<style type="text/css">


	#ordersheet {
		width: 700px;
	}
	
	.ordersheet_items {
		max-width: 1200px;
	}
	
	h6 { 
		display: block; 
		margin-bottom: 5px; 
		font-size: .8em;
		font-weight: 500;
	}
	
	h5 { 
		display: block;
		margin: 40px 0 20px; 
		font-size: 1em;
		font-variant: small-caps;
		border-bottom: 1px solid silver;
		color: gray;
	}
	
	.totoal-collection {
		display: block;
		width: 100%;
		font-size: .75em;
		text-align: right;
		padding-top: 5px;
		margin-bottom: 20px;
	}
	
	.totoal-collection b {
		padding-left: 5px;
	}

</style>
<?php 

	echo $form."<div class=ordersheet_items >$list</div>";
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			)
		)
	));
	
?>