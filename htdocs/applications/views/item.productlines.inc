<?php 
	$user = User::instance();
	$translate = Translate::instance();
?>

<style type="text/css">

	#items { 
		width: 500px; 
	}

	#items table {
		width: 500px;
	}

	h5 { 
		display: block;
		margin: 40px 0 20px; 
		font-size: 1em;
		font-variant: small-caps;
		border-bottom: 1px solid silver;
		color: gray;
		padding-bottom: 5px;
		cursor: pointer;
	}

	h5:hover { 
		color: #1b5584;
	}

	#items table {
		margin-bottom: 40px;
	}

	.dropdown-active {
		margin: 0 !important;
	}
	
</style>
<link href="/public/scripts/dropdown/dropdown.css" rel="stylesheet" type="text/css" />
<script src="/public/scripts/dropdown/dropdown.js" type="text/javascript"  ></script>
<script type="text/javascript" src="/public/scripts/table.loader.js" ></script>
<script type="text/javascript">
$(document).ready(function() {

	$('#items').tableLoader({
		url: '/applications/helpers/item.product.lines.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			$('select',self).dropdown();

			$('input.category', self).on('change', function() {
			
				var self = $(this);
				var table = self.closest('table');
				var checked = self.is(':checked') ? 1 : 0;
				var data = $('form.request').serializeArray();

				// add checked value
				data.push({ name: 'section',  value: 'category.item'});
				data.push({ name: 'checked',  value: checked});
				data.push({ name: 'category',  value: self.val() });

				// check controllr for checkall
				$('input.productline', table).trigger('checkall');
				
				retailnet.ajax.json('/applications/helpers/item.ajax.php', data).done(function(xhr) {
					if (xhr.message) {
						if (xhr.success) retailnet.notification.success(xhr.message);
						else retailnet.notification.error(xhr.message);
					}
				});
			});	

			$('input.productline', self).on({
				
				/**
				 * This trigger chack all subboxes and set checked status
				 * @return void
				 */
				checkall: function(e) {

					var table = $(this).closest('table');
					var boxes = $('input.category', table);
					var checkedBoxes = $('input.category:checked', table);

					$(this).attr('checked', (boxes.length==checkedBoxes.length) ? true : false);
				},
				
				change: function(e) {
					
					var table = $(this).closest('table');
					var boxes = $('input.category', table);
					var checked = $(this).is(':checked') ? true : false;

					boxes.each(function(i,el) {
						$(el).attr('checked', checked).trigger('change');
					});
				}
			});

			// trigger all checkall buttons
			$('input.productline', self).trigger('checkall');

		}
	});

});
</script>
<div id="items"></div>
<?php echo Request::instance()->form(); ?>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>