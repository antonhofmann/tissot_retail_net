<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#designGroups').tableLoader({
		url: '/applications/helpers/design.objective.groups.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">

	#designGroups { 
		width: 800px; 
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="designGroups"></div>