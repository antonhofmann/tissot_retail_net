<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();
	
	$form = new Form(array(
		'id' => 'countryForm',
		'action' => '/applications/helpers/country.save.php',
		'method' => 'post'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->country_id(
		Form::TYPE_HIDDEN
	);
	
	
	/*
	$form->country_code(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		Validata::maxsize(3)
	);
	
	$form->country_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->country_hemisphere_id(
		Form::TYPE_SELECT
	);
	
	$form->country_region(
		Form::TYPE_SELECT
	);
	
	$form->country_salesregion(
		Form::TYPE_SELECT
	);
	
	$form->country_currency(
		Form::TYPE_SELECT
	);
	
	$form->country_store_locator_id(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->country_provinces_complete(
		Form::TYPE_CHECKBOX
	);
	
	$form->country_timeformat(
		Form::TYPE_SELECT
	);



	

	
	$form->fieldset('Country', array(
		'country_code',
		'country_name',
		'country_hemisphere_id',
		'country_region',
		'country_salesregion',
		'country_currency',
		'country_store_locator_id',
		'country_provinces_complete',
		'country_timeformat'
	));
	*/


	$form->country_website_swatch(
		Form::TYPE_TEXT
	);

	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	
	/*
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	*/

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#countryForm");

		retailnet.loader.init();

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			onAjaxFormComplete: function(status, form, json, options) {

				retailnet.loader.hide();
				retailnet.notification.hide();

				if (json) {

					if (json.response && json.redirect) {
						window.location=json.redirect;
					}

					if (json.message) {
						$.jGrowl(json.message, { 
							sticky: (json.response) ? false : true,
							theme: (json.response) ? 'message' : 'error'
						});
					}
				}
			}
		});
		
		$("#save").click(function(event) { 
			
			event.preventDefault();

			retailnet.notification.hide();

			if ($('#country_store_locator_id').hasClass('error')) {

				retailnet.notification.show(
					'The Store Locator ID already exists - it is supposed to be unique.', 
					{
						theme: 'error',
						'sticky': true
					}
				);
				
			} else {

				retailnet.loader.show();

				form.submit();
			}

			return false;
		});

		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});

		$('.dialog').click(function(event) {

			event.preventDefault();

			var button = $(this);
			
			$('#apply, a.apply').attr('href', button.attr('href'));
			
			retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
		});

		$('#cancel').bind('click', function(event) {
			event.preventDefault();
			retailnet.modal.close();
			return false;
		});	

		$('#apply').bind('click', function(event) {
			event.preventDefault();
			window.location=$(this).attr('href');
		})


		$('#country_store_locator_id').change(function() {

			var self = $(this);
			var val  = self.val();

			self.removeClass('error');
			
			retailnet.notification.hide();
			
			retailnet.ajax.json('/applications/helpers/country.ajax.php', {
				section: 'check-store-locator-id',
				country: $('#country_id').val(),
				id: self.val()
			}).done(function(xhr) {
				
				if (xhr) {

					if (xhr.response) {
						self.addClass('error');
					}

					if (xhr.notification) {
						retailnet.notification.show(
							xhr.notification.content, 
							eval(xhr.notification.properties)
						);
					}
				}
			});
		});

	});
</script>

<style type="text/css">

	#countryForm { 
		width: 700px; 
	}
	
	#country_code,
	#country_store_locator_id {
		width: 60px !important;
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>