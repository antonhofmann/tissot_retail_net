<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#turnover_classes').tableLoader({
		url: '/applications/helpers/turnover.class.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select', self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">

	#turnover_classes { 
		width: 800px; 
	}
	
</style>
<?php echo $request->form(); ?>
<div id="turnover_classes"></div>