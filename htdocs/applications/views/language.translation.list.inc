<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#language_translations').tableLoader({
		url: '/applications/helpers/language.translation.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select', self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">
	#language_translations { width: 600px; }
</style>
<?php 
	echo $request->form($requestFields);
?>
<div id="language_translations"></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>