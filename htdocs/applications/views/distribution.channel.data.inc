<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	
	$form = new Form(array(
		'id' => 'distribution_channel',
		'action' => '/applications/helpers/distribution.channel.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_distchannel_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_distchannel_group_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_distchannel_code(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_distchannel_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_distchannel_active(
		Form::TYPE_CHECKBOX
	);
	
	$form->mps_distchannel_selectable_in_new_projects(
		Form::TYPE_CHECKBOX
	);	

	$form->mps_distchannel_selectable_by_clients_in_mps(
		Form::TYPE_CHECKBOX
	);

	$form->mps_distchannel_use_for_warehouses_export_to_sap(
		Form::TYPE_CHECKBOX
	);
	
	$form->fieldset('distribution_channel', array(
		'mps_distchannel_group_id',
		'mps_distchannel_code',
		'mps_distchannel_name',
		'mps_distchannel_active',
		'mps_distchannel_selectable_in_new_projects',
		'mps_distchannel_selectable_by_clients_in_mps',
		'mps_distchannel_use_for_warehouses_export_to_sap'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<style type="text/css">

	#distribution_channel { width: 700px; }
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>