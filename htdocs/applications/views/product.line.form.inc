<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();
	
	$form = new Form(array(
		'id' => 'applicationForm',
		'action' => '/applications/helpers/product.line.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->product_line_id(
		Form::TYPE_HIDDEN
	);
	
	$form->product_line_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->product_line_description(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);

	$form->product_line_supplying_group(
		Form::TYPE_CHECKBOX_GROUP
	);
	
	$form->product_line_visible(
		Form::TYPE_CHECKBOX
	);
	
	$form->product_line_budget(
		Form::TYPE_CHECKBOX
	);	

	$form->product_line_clients(
		Form::TYPE_CHECKBOX
	);

	$form->product_line_posindex(
		Form::TYPE_CHECKBOX
	);

	$form->product_line_mis(
		Form::TYPE_CHECKBOX
	);

	$form->product_line_active(
		Form::TYPE_CHECKBOX
	);

	$form->regions(
		Form::TYPE_CHECKBOX_GROUP
	);
	
	$form->fieldset('Name', array(
		'product_line_name',
		'product_line_description'
	));	

	$form->fieldset('Supplying Groups', array('product_line_supplying_group'));

	$form->fieldset('Visibility', array(
		'product_line_visible',
		'product_line_budget',
		'product_line_clients',
		'product_line_posindex',
		'product_line_mis',
		'product_line_active'
	));

	$form->fieldset('Available in the following supplying regions', array(
		'regions'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>

<style type="text/css">

	#applicationForm { 
		width: 700px; 
	}
	
	#applicationForm textarea {
		width: 440px;
		height: 60px;
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>