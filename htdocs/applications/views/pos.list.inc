<?php 
	$translate = Translate::instance();
	$request = request::instance();
?>
<script type="text/javascript" src="/public/js/pos.list.js"></script>
<style type="text/css">
	
	#posindex {
		width: 1500px;
	}
	
	.pdf { 
		cursor: pointer; 
	}
	
	select.selectbox {
		margin-left: 10px;
	}
	
	#filters {
		min-width: 220px;
	}
	
	.dropdown-print {
		float: left;
		display: inline-block;
		margin: 0 10px 0 0;
	}
	
	.modalbox-content p span {
		display: inline-block;
		padding-left: 10px;
		font-size: 12px;
	}
	
	.modalbox .content-caption {
		display: block;
		font-size: 16px;
		margin: 20px 0 10px;
		color: #444;
		font-weight: 600;
	}
	
	.select_all {
		margin-bottom: 10px;
		font-weight: 600;
	}
	
	#group {
		border: 1px solid silver;
		margin: 20px 0 0;
	}
	
</style>
<div id="posindex"></div>
<?php 

	echo $request->form();
	
	$group = ui::dropdown(array(
		'posaddress_distribution_channel' => $translate->posaddress_distribution_channel,
		'posaddress_store_postype' => $translate->posaddress_store_postype,
		'posaddress_ownertype' => $translate->posaddress_ownertype
	), array(
		'name' => 'group',
		'id' => 'group',
		'caption' => 'Group selected fields by'
	));
	
	// export box
	echo ui::exportbox($request->field('printlist'), 'posaddresses', null, "<p>$group</p>");
?>