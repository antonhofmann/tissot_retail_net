<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();

	$form = new Form(array(
		'id' => 'form',
		'action' => '/applications/helpers/item.option.save.php',
		'method' => 'post',
		'class' => 'validator'
	));

	$form->redirect(
		Form::TYPE_HIDDEN
	);

	$form->application(
		Form::TYPE_HIDDEN
	);

	$form->controller(
		Form::TYPE_HIDDEN
	);

	$form->action(
		Form::TYPE_HIDDEN
	);

	$form->item_option_id(
		Form::TYPE_HIDDEN
	);	

	$form->item_option_parent(
		Form::TYPE_HIDDEN
	);

	$form->item_option_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);

	$form->item_option_child(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);	

	$form->item_option_description(
		Form::TYPE_TEXTAREA,
		FORM::PARAM_LABEL,
		"class=description"
	);

	$form->item_option_quantity(
		Form::TYPE_TEXT,
		Form::TOOLTIP_NUMERIC,
		Validata::PARAM_NUMBER,
		'class=numeric'
	);

	$form->item_option_configcode(
		Form::TYPE_TEXT,
		'class=numeric',
		'maxlength=2'
	);	

	$form->fieldset('Option', array(
		'item_option_name',
		'item_option_child',
		'item_option_description',
		'item_option_quantity',
		'item_option_configcode'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}

	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}

	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['add_file']) {
		$form->button(ui::button(array(
			'id' => 'add_file',
			'icon' => 'add',
			'href' => $buttons['add_file'],
			'class' => 'filemodal',
			'label' => 'Add File'
		)));
	}

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}

	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<style></style>
<style type="text/css">

	#form, #files { 
		width: 700px; 
	}

	#optionFiles { 
		width: 700px; 
		margin-top: 40px;
	}

	.xhr-placeholder {
		padding: 0;
	}
	
	.file-extension {
		cursor: pointer;
	}

	textarea.description {
		width: 440px;
		height: 100px;
	}

	h4 { 
		display:block; 
		margin: 0 0 10px; 
		font-size: 18px;
		color: gray;
	}

	span.filename {display:block; font-weight:600; color:#0050CC; cursor:pointer; font-size: 11px;}
	span.filename:hover { color: black; }

	td.title span {
		display: block;
		color: gray;
		font-size: 12px;
		padding-top: 3px;
	}

</style>
<script type="text/javascript">
	$(document).ready(function() {

		// get item option files
		var fileList = $('#optionFiles').tableLoader({
			url: '/applications/helpers/item.option.files.php',
			data: $('#form').serializeArray(),
			after: function(self) {

				$(".modal").on("click", function() {
		
					var path = $(this).attr('tag');
					var title = $(this).parent().next().find('a').text();
				
					if (isImage(path)) {

						retailnet.modal.show(path, {
							title: (title) ? title : ''
						});
						
					} else {
						window.open('http://'+location.host+path);
					}
				});
			}
		});

		// modal file upload
		$(document).on('click', '.filemodal', function(e) {

			e.preventDefault();

			var url = $(this).attr('href');
			var option = $('#item_option_id').val();

			if (option) {
				$.fancybox({
					href		: url+'&option='+option,
					type 		: 'iframe',
					autoSize	: false,
					closeClick	: false,
					closeBtn	: false,
					fitToView   : true,
				    width       : '760px',
				    height      : '600px',
					modal 		: true,
					margin		: 0,
					padding		: 0,
					iframe		: {
						scrolling : 'auto',
						preload   : true
					},
			        afterClose	: function() {
			        	fileList.showPage(1);
			        }
				});
			} 
			else {
				retailnet.notification.error('Item option is not defined.');
			}

			return false;
		});

	});
</script>

<?php 

echo $form; 

echo "<div id='optionFiles' ></div>";

echo ui::dialogbox(array(
	'id' => 'delete_dialog',
	'title' => $translate->delete,
	'content' => $translate->dialog_delete_record,
	'buttons' => array(
		'cancel' => array(
			'icon' => 'cancel',
			'label' => $translate->cancel
		),
		'apply' => array(
			'icon' => 'apply',
			'label' => $translate->yes
		)
	)
));