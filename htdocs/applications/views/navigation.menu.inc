<?php 
	
	$request = request::instance(); 
	$navigation = Navigation::instance();
	$navigation->navigations();

	if($navigation) {
		$parent = ($navigation->request->parent==0) ? $navigation->id : $navigation->request->parent;
		echo "<div class='menu'>";
		echo $navigation->menu($parent,1);
		echo "</div>";
	}

?>