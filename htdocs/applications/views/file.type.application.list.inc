<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	var fileTypes = $('#filetype_applications').tableLoader({
		url: '/applications/helpers/file.type.application.list.php',
		data: $('.request').serializeArray()
	});

	$(document).delegate("input.filetype", "change", function() {

		var filetype = $('#id').val();
		var application = $(this).val();
		var checked = ($(this).is(':checked')) ? 1 : 0;

		if (!checked) {
			$('.checkall').removeAttr('checked');
		} else {
			var data = [];
			var checkbox = $('input.filetype');
			var count = 0;
			$.each(checkbox, function(e) {
				if ($(this).is(':checked')) data.push(1);
				count++;
			});
			if (count == data.length ) {
				$('.checkall').attr('checked', 'checked')
			}
		}
		
		var request = $.getJSON('/applications/helpers/application.file.type.save.php', { 
			filetype: filetype,
			application: application,
			action: checked
		});

		request.success(function(json) {
			if (json.message) {
				$.jGrowl(json.message, {
					life: 1000, 
					theme: (json.response) ? 'message' : 'error'
				});
			}
		});
	});

	$(document).delegate(".checkall", "click", function() {
		
		var checkbox = $('input.filetype');
		var checked = ($(this).is(':checked')) ? 'checked' : false;

		$.each(checkbox, function(e) {

			if (checked && !$(this).is(':checked')) {
				$(this).attr('checked', 'checked').trigger('change')
			}
			
			if (!checked && $(this).is(':checked')) {
				$(this).attr('checked', false).trigger('change')
			}
		});
	});
	
});
</script>
<style type="text/css">
	#filetype_applications { width: 500px; }
</style>
<form class='request'>
	<input type=hidden name=application value="<?php echo $request->application ?>" />
	<input type=hidden name=controller value="<?php echo $request->controller ?>" />
	<input type=hidden name=action value="<?php echo $request->action ?>" />
	<input type=hidden name=id id=id value="<?php echo $id ?>" />
</form>
<div id="filetype_applications"></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>