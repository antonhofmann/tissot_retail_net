<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	var roles = $('#roles').tableLoader({
		url: '/applications/helpers/role.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});

});
</script>
<style type="text/css">

	#roles { width: 900px; }

	#roles h4 {
		margin: 40px 0 10px;
		font-size: 16px;
	}

	#roles th {font-size: 14px;}
	#roles td {font-size: 13px;}

</style>
<?php 
	echo $request->form($requestFields);
?>
<div id="roles"></div>