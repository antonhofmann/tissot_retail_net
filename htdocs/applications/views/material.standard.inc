<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
		
	$form = new Form(array(
		'id' => 'material_standard_form',
		'action' => '/applications/helpers/material.standard.save.php',
		'method' => 'post'
	));
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_material_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_material_standard_order(
		Form::TYPE_CHECKBOX
	);
	
	$form->mps_material_standard_order_date(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		Validata::CLASS_DATAPICKER
	);
	
	$form->mps_material_standard_order_quantity(
		Form::TYPE_TEXT,
		Validata::PARAM_INTEGER	
	);
	
	$caption = ($data['select_all']) ? $translate->deselect_all : $translate->select_all;
	$recaption = ($data['select_all']) ? $translate->select_all : $translate->deselect_all;
	
	$form->selectors(
		Form::TYPE_AJAX,
		FORM::PARAM_SHOW_VALUE
	);
	
	$form->companies(
		Form::TYPE_CHECKBOX_GROUP
	);
	
	$form->fieldset('standard_ordersheet', array(
		'mps_material_standard_order',
		'mps_material_standard_order_date',
		'mps_material_standard_order_quantity'
	));
	
	// fieldset: dimensions
	$form->fieldset('companies', array(
		'selectors',
		'companies'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript" src="/public/js/material.standard.js"></script>

<style type="text/css">

	#material_standard_form { 
		width: 700px; 
	}
	
	#mps_material_ean_number,
	#mps_material_setof,
	#mps_material_price { 
		width:120px;
	}
	
	#mps_material_standard_order_date,
	#mps_material_standard_order_quantity { 
		width: 150px; 
	}
	
	.-row.companies label {
		display: none !important;
	}
	
	.selectors label {
		display: none !important;
	}
	
	form.default .-placeholder {
		width: 100%;
		display: block;
		text-align: center;
	}
	
	#selectors {
		margin: 0 auto;
		text-align: center;
	}
	
	.button.selector + .button.selector {
		margin: 0;
	}
	
	.button.selector.all {
		margin-right: 10px;
	}
	
	.button.selector.sh {
		margin-left: 10px !important;
	}
	
	.button.active .label {
		color: #0000FF !important;
	}
	
</style>
<?php 
	echo $form; 
?>