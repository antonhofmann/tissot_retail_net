<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	var id = $('#id').val();

	var assistancesURL = $('#assistancesURL').tableLoader({
		url: '/applications/helpers/assistance.urls.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			var url = $('#assistance_url');

			url.keypress(function(e) {
				if (e.keyCode==13) {
					$(this).blur();
					//$(this).trigger('change');
					return e.keyCode != 13;
				}
			});

			// add new url
			$('.icon.add').click(function(e) {

				e.preventDefault();

				if ($(this).hasClass('delete')) {
					url.val('');
					$(this).removeClass('delete');
				}

				url.trigger('change');
			});

			// remove url
			$('.remover').click(function(e) {

				e.preventDefault();
				var row = $(this).closest('tr');

				retailnet.ajax.json('/applications/helpers/assistance.url.php', {
					section: 'remove',
					id: id,
					url: $(this).attr('data-url')
				}).success(function(data) {

					// show notifications
					if (data && data.notification) {
						var content = data.notification.content;
						var properties = eval(data.notification.properties);
						retailnet.notification.show(content, properties);
					}

					// refresh list
					if (data && data.success) {
						row.remove();
					}
				});
			});

			// save url
			url.change(function(e) {

				e.preventDefault();
				$(this).focusout();
				
				retailnet.notification.hide();

				var elem = $(this),
					url = elem.val(),
					query = null;

				var urlexp = new RegExp('(http|ftp|https)://[a-z0-9\-_]+(\.[a-z0-9\-_]+)+([a-z0-9\-\.,@\?^=%&;:/~\+#]*[a-z0-9\-@\?^=%&;/~\+#])?', 'i');

				if (urlexp.test(url)) {
					var a = document.createElement('a');
				    a.href = url;
				    query = a.pathname.substring(1)+a.search
				} else {
					query = url;
				}
				
				if (query) {
					
					$('.icon.add').addClass('delete');

					retailnet.ajax.json('/applications/helpers/assistance.url.php', {
						section: 'save',
						id: id,
						url: query
					}).success(function(data) {

						elem.empty();

						// show notifications
						if (data && data.notification) {
							var content = data.notification.content;
							var properties = eval(data.notification.properties);
							retailnet.notification.show(content, properties);
						}

						// refresh list
						if (data && data.success) {
							self.showPage();
						}
					});
				}
			});
		}
	});
	
});
</script>
<style type="text/css">
	
	#assistancesURL { width: 680px; }
	
	.searchbox {
		width: 625px !important;
	}
	
	.searchbox  input {
		width: 600px !important;
	}
	
	span.icon.delete {
		margin: 0 !important;
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id=assistancesURL></div>