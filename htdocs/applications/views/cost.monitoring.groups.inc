<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#costMonitoringGroups').tableLoader({
		url: '/applications/helpers/cost.monitoring.groups.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">

	#costMonitoringGroups { 
		width: 800px; 
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="costMonitoringGroups"></div>