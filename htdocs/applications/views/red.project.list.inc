<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<link href="/public/scripts/dropdown/dropdown.css" rel="stylesheet" type="text/css" />
<script src="/public/scripts/dropdown/dropdown.js" type="text/javascript"  ></script>
<script type="text/javascript" src="/public/scripts/table.loader.js" ></script>
<script type="text/javascript">
$(document).ready(function() {

	$('#projects').tableLoader({
		url: '/applications/helpers/red.project.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			$('select',self).dropdown();

			printButton = $('#print_excel');

			if (printButton != undefined) {
				
				var href = printButton.attr('href');
				var searchValue =  printButton.parent('form').find('input#search').val();

				if (searchValue == 'Search') {
					searchValue = '';
				}
				
				filters = '?mode=' + printButton.attr('mode');
				filters += '&search=' + searchValue;
				filters += '&project_types=' + printButton.parent('form').find('select#projecttypes').val();
				filters += '&project_states=' + printButton.parent('form').find('select#projectstates').val();

				printButton.attr('href', href +  filters);
			}
		}
	});


});
</script>
<style type="text/css">

	#projects { 
		width: 1400px; 
	}

	span.icon {
		display: block;
		width: 20px;
		height: 20px;
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="projects"></div>