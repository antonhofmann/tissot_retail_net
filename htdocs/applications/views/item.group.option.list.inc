<?php 
	$user = User::instance();
	$translate = Translate::instance();
?>

<style type="text/css">

	#items { 
		width: 600px; 
	}
	
</style>
<script type="text/javascript">
	$(document).ready(function() {

	});
</script>
<div id="items">
	<div class="table-toolbox">
		<form class=toolbox >
			<?php
				if ($buttons['add'])  {
					echo ui::button(array(
						'id' => 'add',
						'icon' => 'add',
						'href' => $buttons['add'],
						'label' => $translate->add_new
					));
				}
			?>
		</form>
	</div>
<?php 
	if ($datagrid) {
		$table = new Table();
		$table->datagrid = $datagrid;
		$table->item_group_option_name("href=$link");
		echo $table->render();
	}
?>
</div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>