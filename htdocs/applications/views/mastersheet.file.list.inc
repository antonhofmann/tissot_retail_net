<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#mastersheet_files').tableLoader({
		url: '/applications/helpers/mastersheet.file.list.php',
		data: $('.request').serializeArray()
	});

	$(document).on("click", ".modal", function() {
		
		var path = $(this).attr('data');
		var title = $(this).parent().next().find('.title').text();
		title = (title) ? title : '';
	
		if (isImage(path)) {
			retailnet.modal.show(path, {title:title});
		} else {
			window.open('http://'+location.host+path);
		}
	});
	
});
</script>
<style type="text/css">

	#mastersheet_files { 
		width: 700px; 
	}
	
	span.date,
	span.description {
		display: block; 
		color: gray; 
		font-size: 11px;
	}
	
	td.fileicon {
		width: 32px;
	}
	
	td.date {
		width: 100px;
	}
	
	h6 { 
		display: block; 
		margin: 40px 0 5px; 
		font-size: .825em;
	}

	table { 
		margin-bottom:20px; 
		border-style: solid; 
		border-color: silver; 
	}
	
	.file-extension {
		cursor: pointer;
	}

	span.filename {display:block; font-weight:600; color:#0050CC; cursor:pointer; font-size: 11px;}
	span.filename:hover { color: black; }
	
</style>
<?php 

	echo $request->form();
?>
<div id="mastersheet_files"></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>