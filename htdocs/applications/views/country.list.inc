<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#countries').tableLoader({
		url: '/applications/helpers/country.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});

});
</script>
<style type="text/css">
	#countries { width: 1200px; }
</style>
<div id="countries"></div>
<?php echo $request->form(); ?>