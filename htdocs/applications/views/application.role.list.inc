<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#roles').tableLoader({
		url: '/applications/helpers/application.role.list.php',
		data: $('.request').serializeArray()
	});

	$(document).delegate("input.role", "change", function(event) {

		event.stopPropagation();

		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
		
		var checked = ($(this).is(':checked')) ? 1 : 0;
		var roles = $('input.role');
		var rolesChecked = $('input.role:checked');

		var data = {
			application: $('#id').val(),
			role: $(this).val(),
			action: checked
		};

		// checkall box
		var attribute = (roles.length == rolesChecked.length) ? 'checked' : false;
		$('.checkall').attr('checked', attribute);
		
		$.getJSON('/applications/helpers/application.role.save.php', data, function(json) {
			if (json) {
				$.jGrowl(json.message, {
					life: 1000, 
					theme: (json.response) ? 'message' : 'error'
				});
			}
		});
	});

	$(document).delegate(".checkall", "click", function() {
		
		var checked = ($(this).is(':checked')) ? 'checked' : false;

		$.each($('input.role'), function(e) {
			$(this).attr('checked', checked).trigger('change');
		});
	});
	
});
</script>
<style type="text/css">
	#roles { 
		width: 500px; 
	}
</style>
<?php 
	echo $request->form();
?>
<div id="roles"></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>