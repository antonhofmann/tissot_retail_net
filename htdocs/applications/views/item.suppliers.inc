<?php 
	$user = User::instance();
	$translate = Translate::instance();
?>

<style type="text/css">

	#items { 
		width: 800px; 
	}

	h5 { 
		display: block;
		margin: 40px 0 20px; 
		font-size: 1em;
		font-variant: small-caps;
		border-bottom: 1px solid silver;
		color: gray;
		padding-bottom: 5px;
		cursor: pointer;
	}

	h5:hover { 
		color: #1b5584;
	}
	
</style>
<script type="text/javascript">
	$(document).ready(function() {

	});
</script>
<div id="items">
<?php 

	if ($buttons['add']) {

		echo "<div class='table-toolbox'>";
		echo ui::button(array(
			'id' => 'add',
			'icon' => 'add',
			'href' => $buttons['add'],
			'label' => $translate->add
		));
		echo "<div>";
	}

	$table = new Table();
	$table->datagrid = $datagrid;
	$table->supplier_item_code(Table::ATTRIBUTE_NOWRAP, 'width=10%', "href=$link");
	$table->supplier_item_name();
	$table->currency_symbol(Table::ATTRIBUTE_NOWRAP, 'width=5%');
	$table->supplier_item_price(Table::ATTRIBUTE_NOWRAP, 'width=10%');
	$table->address_company(Table::ATTRIBUTE_NOWRAP);

	echo $table->render();
?>
</div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>