<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#companies').tableLoader({
		url: '/applications/helpers/company.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			var dropdown = $("select",self).dropdown();

			$('#print').click(function(event) {

				event.preventDefault();

				if ($('#addresses-modal').val()) {

					retailnet.modal.show('#exportbox-addresses', {
						autoSize	: false,
						closeClick	: false,
						closeBtn	: false,
						fitToView   : true,
					    width       : '90%',
					    height      : '90%',
					    maxWidth	: '520px',
					    maxHeight	: '520px',
						modal 		: true
					});
					
				} else {
					$('#exportbox-addresses-apply').trigger('click');
				}
			});

			$('#exportbox-addresses-apply').click(function(event) { 

				event.preventDefault();
				var error = false;

				if ($('#addresses-modal').val()) { 
					error = ($('#exportbox-addresses input[type=checkbox]:checked').length<1) ? true : false;
				}

				if (!error) {
					$('#exportbox-addresses form').submit();
					retailnet.modal.close();
				} else {
					$.jGrowl('Please, select at least one Column for export.', {
						life: 5000, 
						theme: 'error'
					});
				}
			
				return false;
			});	

			$('.cancel').click(function(event) {
				event.preventDefault();
				retailnet.modal.close();
				return false;
			});

			$('.select_all input').click(function() {
				var form = $(this).closest('form');
				var checked = ($(this).is(':checked')) ? true : false;
				$('input.export_field', form).attr('checked', checked);
				$('.select_all span', form).trigger('click');
			});

			$('.select_all span').click(function() { 
				var form = $(this).closest('form');
				var selectAll = $('.select_all input', form); 
				var checked = ($('input.export_field', form).length == $('input.export_field:checked', form).length) ? true : false;
				var caption = checked ? 'Deselect All' : 'Select All'
				selectAll.attr('checked', checked);
				$(this).text(caption);
			});

			$('input.export_field').click(function() {
				$('.select_all span').trigger('click');
			});	
		}
	});

});
</script>
<style type="text/css">

	#companies { 
		width: 1300px;
	}
	
	.pdf { 
		cursor: pointer; 
	}
	
	.modalbox-content p span {
		display: inline-block;
		padding-left: 10px;
		font-size: 12px;
	}
	
	.modalbox .content-caption {
		display: block;
		font-size: 16px;
		margin: 20px 0 10px;
		color: #444;
		font-weight: 600;
	}
	
	.select_all {
		margin-bottom: 10px;
		font-weight: 600;
	}
	
</style>
<div id="companies" ></div>
<?php 

	// refquest form 
	echo $request->form();

	// export box
	echo ui::exportbox($request->field('export'), 'addresses');
?>