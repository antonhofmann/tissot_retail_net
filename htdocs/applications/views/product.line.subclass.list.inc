<script type="text/javascript">
$(document).ready(function() {

	$('#productLineSubclasses').tableLoader({
		url: '/applications/helpers/product.line.subclass.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">

	#productLineSubclasses { 
		width: 700px; 
	}
	
</style>
<div id="productLineSubclasses" ></div>
<?php echo Request::instance()->form(); ?> 