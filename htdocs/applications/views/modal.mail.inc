<script type="text/javascript" src="/public/scripts/textarea.expander.js"></script>
<div class="modalbox-container">
	<div id="<?php echo $id; ?>" class="modalbox mail-template-modal <?php echo $class; ?>">
		<div class="modalbox-header" >
			<div class="title"><?php echo $title; ?></div>
			<div class="subtitle"><?php echo $subtitle; ?></div>
		</div>
		<div class="modalbox-content-container" >
			<div class="modalbox-content">
				<form action="<?php echo $action; ?>" >
					<input type="hidden" name="mail_template_id" class="mail_template_id" value="<?php echo $mail['mail_template_id'] ?>" />
					<input type="hidden" name="mail_template_view_modal" class="mail_template_view_modal" value="<?php echo $mail['mail_template_view_modal'] ?>" />
					<div class="modal-input-container"><input type=text name="mail_template_subject" class="mail_template_subject" class="required" value="<?php echo $mail['mail_template_subject'] ?>" /></div>
					<div class="modal-input-container"><textarea name="mail_template_text" class="mail_template_text required expand" ><?php echo $mail['mail_template_text'] ?></textarea></div>
				</form>
			</div>
		</div>
		<div class="modalbox-footer" >
			<div class=modalbox-actions >
				<span class="button cancel"> 
					<span class="icon cancel"></span>
					<span class="label"><?php echo translate::instance()->cancel ?></span>
				</span>
				<span class="button apply"> 
					<span class="icon mail"></span>
					<span class="label"><?php echo translate::instance()->sendmail ?></span>
				</span>
			</div>
		</div>
	</div>
</div>