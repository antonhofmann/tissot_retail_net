<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'posstore',
		'action' => '/applications/helpers/pos.store.save.php',
		'method' => 'post',
		'tag' => $readonly
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->posaddress_id(
		Form::TYPE_HIDDEN
	);
	
	$form->posaddress_store_postype(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);	
	
	$form->posaddress_store_subclass(
		Form::TYPE_SELECT
	);	
	
	$form->posaddress_is_flagship(
		Form::TYPE_CHECKBOX,
		Form::PARAM_SHOW_ONLY_SELECTED
	);	

	$form->posaddress_ownertype(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->posaddress_franchisee_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->posaddress_store_furniture(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->posaddress_store_furniture_subclass(
		Form::TYPE_SELECT,
		Form::PARAM_AJAX
	);
	
	$form->locations(
		Form::TYPE_CHECKBOX_GROUP,
		Form::PARAM_SHOW_ONLY_SELECTED
	);

	$form->posaddress_store_floor(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->posaddress_perc_class(
		Form::TYPE_AJAX,
		'tag='.$data['posaddress_perc_class']
	);
	
	$form->extended('posaddress_perc_class', "<span id='posaddress_perc_class_target' class='raty-target'></span>");
	
	$form->posaddress_perc_tourist(
		Form::TYPE_AJAX,
		'tag='.$data['posaddress_perc_tourist']
	);

	$form->extended('posaddress_perc_tourist', "<span id='posaddress_perc_tourist_target' class='raty-target'></span>");
	
	$form->posaddress_perc_transport(
		Form::TYPE_AJAX,
		'tag='.$data['posaddress_perc_transport']
	);

	$form->extended('posaddress_perc_transport', "<span id='posaddress_perc_transport_target' class='raty-target'></span>");
	
	$form->posaddress_perc_people(
		Form::TYPE_AJAX,
		'tag='.$data['posaddress_perc_people']
	);

	$form->extended('posaddress_perc_people', "<span id='posaddress_perc_people_target' class='raty-target'></span>");
	
	$form->posaddress_perc_parking(
		Form::TYPE_AJAX,
		'tag='.$data['posaddress_perc_parking']
	);

	$form->extended('posaddress_perc_parking', "<span id='posaddress_perc_parking_target' class='raty-target'></span>");
	
	$form->posaddress_perc_visibility1(
		Form::TYPE_AJAX,
		'tag='.$data['posaddress_perc_visibility1']
	);

	$form->extended('posaddress_perc_visibility1', "<span id='posaddress_perc_visibility1_target' class='raty-target'></span>");
	
	$form->posaddress_perc_visibility2(
		Form::TYPE_AJAX,
		'tag='.$data['posaddress_perc_visibility2']
	);

	$form->extended('posaddress_perc_visibility2', "<span id='posaddress_perc_visibility2_target' class='raty-target'></span>");
	
	$form->fieldset('classifications', array(
		'posaddress_store_postype',
		'posaddress_store_subclass',
		'posaddress_is_flagship',
		'posaddress_ownertype',
		'posaddress_franchisee_id',
		'posaddress_store_furniture',
		'posaddress_store_furniture_subclass'
	));
	
	$form->fieldset('locations', array(
		'locations'
	));


	$form->fieldset('posaddress_store_floor', array(
		'posaddress_store_floor'
	));

	$form->fieldset('area_perception', array(
		'posaddress_perc_class', 
		'posaddress_perc_tourist', 
		'posaddress_perc_transport', 
		'posaddress_perc_people', 
		'posaddress_perc_parking', 
		'posaddress_perc_visibility1',
		'posaddress_perc_visibility2'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
		
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<style type="text/css">
	
	#posstore {
		width: 700px;
	}
	
	#posaddress_store_openingdate,
	#posaddress_store_closingdate {
		width: 120px;
	}

	form.default .locations label {
		display: none;
	}
	
	.locations .checkbox-group {
		margin-left: 264px;
	}

	.area_perception label {
		width: 240px;
	}
	
	.locations .-caption {
		/*width: 400px;*/
	}

	

	.raty-target {
		display: block;
		position: relative;
		left: 104px;
		top: -24px;
		width: 100px;
		height: 16px;
		line-height: 18px;
		color: #666;
	}
	
</style>
<?php 
	echo $form; 
?>
	