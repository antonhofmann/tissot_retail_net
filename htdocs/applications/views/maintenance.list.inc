<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#maintenance').tableLoader({
		url: '/applications/helpers/maintenance.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select', self).dropdown();
		}
	});

});
</script>
<style type="text/css">
	#maintenance { 
		width: 900px; 
	}
</style>
<?php 
	echo $request->form();
?>
<div id="maintenance"></div>