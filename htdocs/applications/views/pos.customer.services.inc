<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'posCustomerServices',
		'action' => '/applications/helpers/pos.customer.services.save.php',
		'method' => 'post'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->posaddress_customerservice_posaddress_id(
		Form::TYPE_HIDDEN
	);

	
	$form->services(
		Form::TYPE_CHECKBOX_GROUP
	);

	
	$form->fieldset('customer_services', array(
		'services'
	));
	

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
		
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	if ($buttons['back']) {
		$form->button(ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => $translate->back
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript" src="/public/js/pos.customer.services.js"></script>
<style type="text/css">
	
	#posCustomerServices {
		width: 700px;
	}
	
	.services .-caption {
		width: 400px;
	}
	
</style>
<?php 
	echo $form; 
?>
	