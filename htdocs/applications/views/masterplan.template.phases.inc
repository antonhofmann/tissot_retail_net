<style>
.masterplan-phases .phase-title {
	width: 300px;
}
.masterplan-phases select.value {
	width: 100%;
}
.masterplan-phases .settings {
	width: 200px;
}
</style>
<form class="default" name="masterplan-phases">
	<table class="listing light masterplan-phases">
	<tr>
		<th><?php print $translate->phase_title?></th>
		<th colspan="2"><?php print $translate->start?></th>
		<th colspan="2"><?php print $translate->end?></th>
		<th colspan="3" class="settings"><?php print $translate->appears_in?></th>
	</tr>
	<tr class="strong no-bg">
		<td></td>
		<td><?php print $translate->default?></td>
		<td><?php print $translate->fallback?></td>
		<td><?php print $translate->default?></td>
		<td><?php print $translate->fallback?></td>
		<td><?php print $translate->gantt_project_masterplan?></td>
		<td><?php print $translate->gantt_project_overview?></td>
		<td><?php print $translate->excel_masterplan?></td>
	</tr>
	<?php foreach ($phases as $phase): ?>
		<tr>
			<td class="phase-title"><?php printf('%s (%s)', $phase->title, $phase->parent->title)?></td>
			<?php foreach ($fieldTypes as $type): ?>
			<td>
			<select 
				name="<?php print $type?>" 
				class="value"
				data-phase-id="<?php print $phase->id?>"
				data-selected="<?php if (isset($phase->$type)) { print $phase->$type; } ?>"
			>
				<option value="">--</option>
				<?php foreach ($values as $key => $value): ?>
				<option value="<?php print $key?>"><?php print $value?></option>
				<?php endforeach; ?>
			</select>
			</td>
			<?php endforeach; ?>
			<td>
				<input
					type="checkbox"
					<?php print $phase->show_in_project_masterplan == '1' ? 'checked' : ''?>
					name="show_in_project_masterplan"
					data-phase-id="<?php print $phase->id?>"
				></td>
			<td>
				<input
					type="checkbox"
					<?php print $phase->show_in_project_overview == '1' ? 'checked' : ''?>
					name="show_in_project_overview"
					data-phase-id="<?php print $phase->id?>"
				></td>
			<td>
				<input
					type="checkbox"
					<?php print $phase->show_in_excel_masterplan == '1' ? 'checked' : ''?>
					name="show_in_excel_masterplan"
					data-phase-id="<?php print $phase->id?>"
				></td>
		</tr>
	<?php endforeach; ?>
	</table>
</form>

<script>
require(['jquery', 'libs/retailnet'], function($, retailnet) {
	var url = '/masterplantemplates/save_phase',
			$steps = $('form[name=masterplan-phases] select.value'),
			$settings = $('form[name=masterplan-phases] input[type=checkbox]');

	/**
	 * start/end steps
	 */
	$steps
		// mark all dropdowns as selected that have a data attribute saying so
		.each(function() {
			$(this).val($(this).attr('data-selected'));
		})
		// auto-save steps to server when dropdowns are changed
		.change(function() {
			var data = {
				phaseID: $(this).attr('data-phase-id'),
				value: $(this).val(),
				field: this.name
			};
			$.post(url, data, function(response) { 
				retailnet.notification.show(response.content, response.properties);
			}, 'json');
	});

	/**
	 * settings
	 */
	$settings
		// auto-save steps to server when checkboxes are changed
		.change(function() {
			var data = {
				phaseID: $(this).attr('data-phase-id'),
				value: $(this).attr('checked') == 'checked' ? '1' : '0',
				field: this.name
			};
			$.post(url, data, function(response) { 
				retailnet.notification.show(response.content, response.properties);
			}, 'json');
	});
});
</script>