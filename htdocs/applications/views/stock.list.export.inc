<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'stockvalues',
		'action' => '/applications/helpers/stock.list.weeks.export.php',
		'method' => 'post'
	));
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->no_result(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE
	);
	
	$form->scpps_week_year(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->select_all(
		Form::TYPE_CHECKBOX
	);
	
	$form->suppliers(
		Form::TYPE_CHECKBOX_GROUP
	);
	
	$form->fieldset('generate_excel_sheet', array(
		'scpps_week_year', 
		'select_all',
		'suppliers',
		'no_result'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	if ($buttons['print']) {
		$form->button(ui::button(array(
			'id' => 'print',
			'icon' => 'print',
			'label' => $translate->print,
			'href' => $buttons['print']
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#stockvalues");	

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			beforeSend: function () {
				retailnet.loader.show();
			},
			onAjaxFormComplete: function(status, form, json, options) {

				retailnet.loader.hide();
				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

				if (json) {
					
					if (json.response && json.redirect) {
						window.location=json.redirect;
					}
					
					if (json.message) {
						$.jGrowl( json.message, { 
							sticky: (json.response) ? true : false, 
							theme: (json.response) ? 'message' : 'error'
						});	
					}
				}
			}
		});
		
		$("#print").click(function(event) { 

			event.preventDefault();
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

			form.validationEngine('validate');

			var error = false;
			var checkbox = $('input.suppliers:checked');

			if (!$('#scpps_week_year').val()) {
				error = true;
			}

			if (checkbox.length < 1) {

				$.jGrowl( 'Please, select at least one Supplier', { 
					sticky: true, 
					theme: 'error'
				});	

				error = true;
			} 

			if (!error) {
				var url = $(this).attr('href');
				var request = form.serialize();
				window.location = url+'?'+request;
			}
			
			return false;
		});


		$("#select_all").click(function(event) {
			var checked = $(this).is(':checked') ? 'checked' : false;
			var checkbox = $('input.suppliers');
			checkbox.attr('checked', checked);
		});
		

		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});

	});
</script>

<style type="text/css">

	#stockvalues { 
		width: 700px; 
	}
	
	.no_result .-label {
		display: none;
	}
	
	#no_result {
		width: 700px;
		display: block;
		font-size: 1.6em;
		font-weight: 500;
		color:  gray;
		text-align: center;
	}
	
</style>
<?php 
	echo $form;
?>