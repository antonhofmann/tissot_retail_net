<style>
table.settings .title { width: 450px; }
table.settings .checkbox { width: 140px; text-align: center; }
.save_box { margin-bottom: 1em; }
</style>

<form class="default" name="settings" action="/project/projectmasterplan/save_settings" method="post">
<div class="save_box">
  <button id="saveBtn" class="button align"><span class="icon save"></span><span class="label"><?php print $translate->save?></span></button>
</div>

<table class="listing settings reverse-zebra light">

	<tr>
		<th class="title"><?php print $translate->phase?></th>
		<th class="checkbox"><?php print $translate->hide_in_masterplan?></th>
		<th></th>
	</tr>
	
	<tbody>
		<!-- subphase name -->
		<?php foreach ($phases as $subphase): ?>
			<tr>
				<td class="title"><?php printf('%s (%s)', $subphase->title, $subphase->parent->title)?></td>
				<td class="checkbox">
					<input 
						type="checkbox"
						name="exclude[]"
						value="<?php print $subphase->id?>"
						data-phase-id="<?php print $subphase->id?>"
						data-selected="<?php if (isset($subphase->$type)) { print $subphase->$type; } ?>"
					>
				</td>
				<td></td>
			</tr>
			<?php endforeach; ?>
	</tbody>
</table>
</form>
<script>
require(['jquery', 'libs/retailnet'], function($, retailnet) {
   var url = '/project/projectmasterplan/save_settings';
   var $form = $('form[name=settings]');

   // handle saving via ajax
   $('#saveBtn').click(function() {
   	// make sure to also send when there are no excluded phases
   	var data = $form.serialize();
   	if (!data.length) {
   		data = 'exclude[]='
   	}
   	$.post(url, data, function(response) { 
   		retailnet.notification.show(response.content, response.properties);
   	}, 'json');
   	return false;
   });

   // set checkbox state based on initial data
   var excluded = <?php print $excluded?>;
   if (excluded.length > 0) {
   	$('table.settings input').each(function() {
   		if ($.inArray(this.value, excluded) >= 0) {
   			$(this).prop('checked', true);
   		}
   	});
   }
});
</script>