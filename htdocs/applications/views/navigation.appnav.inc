<?php 
	
	$request = request::instance();
	$submenu = isset($submenu) ? $submenu : true;
	$menu = menu::instance();
	$parents = $request->parents;
	$app = $parents ? array_pop($parents) : null;
	$navigations = $menu->data[$app];

	function submenu($id) {

		$menu = menu::instance();
		$request = request::instance();
 		
		if ($menu->data[$id]) { 

			foreach ($menu->data[$id] as $id => $row) {
			
				if (!$row['tab']) {
					
					$active = $request->id && in_array($id, $request->parents) ? "active" : null;
					
					$return .= "<li class='$active' >";
					$return .= "<a class='$active' href=\"/{$row[url]}\">{$row[caption]}</a>";

					if ($menu->data[$id]) {
						$return .= submenu($id);
					}
					
					$return .= "</li>";
				}
			}

			if ($return) {
				return "<ul class='submenu' >$return</ul>";
			}
		}
	}

	if ($navigations) {
		
		echo "<ul class=\"app-pages tab-dropper\">";
		
		foreach ($navigations as $id => $row) {
			
			if (!$row['tab']) {

				$submenuItems = $menu->data[$id] && $submenu ? submenu($id) : null;
				$submenuClass = $submenuItems ? 'has-submenu' : null;

				$active = $request->id==$id || in_array($id, $request->parents) ? "active" : null;
	
				echo "<li class='menu-$id $active $submenuClass' >";
				echo "<a class='$active' href=\"/{$row[url]}\">{$row[caption]}</a>";
				echo $submenuItems;
				echo "</li>";
			}
		}
		
		echo "</ul>"; 
	}