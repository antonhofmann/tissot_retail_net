<script type="text/javascript">
$(document).ready(function() {

	$('#productlinesCategories').tableLoader({
		url: '/applications/helpers/product.line.categories.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			$('select',self).dropdown();

			$('.item-trigger', self).click(function(e) {
				e.preventDefault();
				$(this).closest('tr').next().slideToggle();
				$('.fa', $(this)).toggleClass('fa-chevron-circle-right').toggleClass('fa-chevron-circle-down');
			});

			$("tbody", self).sortable({
				placeholder: "ui-state-highlight",         
		        scroll: true,
		        handle: ".row-order",
		        items: "> tr",
		        update: function( event, ui ) {
		        	
		        	$("tr:odd", self).filter(':not(.row-subtable)').removeClass('-odd').removeClass('-even').addClass('-odd');
		        	$("tr:even", self).filter(':not(.row-subtable)').removeClass('-odd').removeClass('-even').addClass('-even');

		        	var orders = [];
					
					$('.row-order', self).each( function () {
						orders.push($(this).data('id'));
					});

					if (orders.length) {
						retailnet.ajax.json('/applications/helpers/product.line.ajax.php', {
							section: 'category.order',
							id: $('#product_line_id').val(),
							orders: orders.join(',')
						}).done(function(xhr) {
							if (xhr && xhr.message) {
								if (xhr.success==1) retailnet.notification.success(xhr.message);
								else retailnet.notification.error(xhr.message);
							}
						});
					}

					// subtable cloner
					var elem = $(event.srcElement);
					var hendle = elem.data('id');
					var subtable = $('#subtable-'+hendle);
					subtable.clone(true).insertAfter(elem);
					subtable.remove();
		        }
			});
		}
	});
	
});
</script>
<style type="text/css">

	#productlinesCategories { 
		width: 1000px; 
	}

	.fa {
		cursor: pointer;
		font-size: 16px;
		color: #888;
	}

	.fa:hover {
		color: blue;
	}

	.item-trigger {
		border-right: 0 !important;
		padding-right: 0 !important;
	}

	.row-subtable {
		display:none;
	}

	table.default {
		border-bottom: 1px solid silver !important;
	}
	
</style>
<div id="productlinesCategories" ></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => Translate::instance()->back
	));
?>
</div>
<?php echo Request::instance()->form(); ?> 