<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'stocklist',
		'action' => '/applications/helpers/stock.list.save.php',
		'method' => 'post'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->scpps_stocklist_id(
		Form::TYPE_HIDDEN
	);
	
	$form->scpps_stocklist_title(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->scpps_stocklist_supplier_address(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->weeks(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE
	);
	
	$form->fieldset('supplier', array(
		'scpps_stocklist_title', 
		'scpps_stocklist_supplier_address'
	));
	
	$form->fieldset('weeks', array(
		'weeks'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#stocklist");
		var id = $('#scpps_stocklist_id');
		var selectAll = $('.selectall');
		var months = $('.monthbox');
		var weeks = $('.weekbox');

		// checkbox: select all
		selectAll.change(function() {
			var attr = $(this).is(':checked') ? 'checked' : false;
			months.attr('checked', attr).trigger('change');
		});

		// checkbox: months
		months.change(function() {
			
			var parent = $(this).closest('div');
			var checkbox = $('.weekbox', parent);
			
			var attr = $(this).is(':checked') ? 'checked' : false;
			checkbox.attr('checked', attr);

			var checkall = (months.length == $('.monthbox:checked').length) ? 'checked' : false;
			selectAll.attr('checked', checkall);
		});

		// checkbox: weeks
		weeks.change(function() {
			
			var parent = $(this).closest('div');
			
			var checked = ($('.weekbox', parent).length == $('.weekbox:checked', parent).length) ? 'checked' : false; 
			$('.monthbox', parent).attr('checked', checked);

			var checkall = (months.length == $('.monthbox:checked').length) ? 'checked' : false;
			selectAll.attr('checked', checkall);
		});
		

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			beforeSend: function () {
				retailnet.loader.show();
			},
			onAjaxFormComplete: function(status, form, json, options) {

				retailnet.loader.hide();
				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

				if (json) {
					if (json.response && json.redirect) {
						window.location=json.redirect;
					}
					
					if (json.message) {
						$.jGrowl( json.message, { 
							sticky: (json.response) ? true : false, 
							theme: (json.response) ? 'message' : 'error'
						});	
					}
				}
			}
		});
		
		$("#save").click(function(event) { 
			event.preventDefault();
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
			form.submit();
		});

		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});

		$('.dialog').click(function(event) {
			
			event.preventDefault();

			var button = $(this);
			
			$('#apply, a.apply').attr('href', button.attr('href'));
			
			retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
		});

		$('#cancel').bind('click', function(event) {
			event.preventDefault();
			retailnet.modal.close();
			return false;
		});

	});
</script>

<style type="text/css">

	#stocklist { 
		width: 700px; 
	}
	
	#weeks {
		display: block;
		float: left;
		font-size: 1em;
	}
		
	#weeks .-row {
		display: block;
		margin: 0;
		padding: 5px 0;
	}
	
	#weeks .-month {
		display: block;
		margin-bottom: 5px;
		font-weight: 600;
	}
	
	#weeks .-week {
		display: block;
		padding-left: 20px;
	}
	
	#weeks .-weekbox {
		padding-left: 20px;
	}
	
	#weeks i {
		font-style: normal;
		color: gray;
	}
	
	.weeks .-label {
		display: none;
	}
	
	.-selectall {
		font-weight: 600;
		margin-bottom: 20px;
	}
	
</style>
<?php 
	
	echo $form;
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>