<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();
	
	$form = new Form(array(
		'action' => '/applications/helpers/item.materials.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->item_id(
		Form::TYPE_HIDDEN
	);

	$form->materials(
		Form::TYPE_CHECKBOX_GROUP
	);

	$form->fieldset('Materials', array(
		'materials'
	));

	// Item materials list
	$form->item_materials(
        Form::TYPE_PLACEHOLDER,
		Form::PARAM_SHOW_VALUE
	);

	if ($data['item_certificate_materials']) {
		$form->item_materials('label=Other Materials');
	}

	$table  = "<div class='form-list'>";
	$table .= "<table id='itemMaterialList' class='form-list item-materials-list' cellpadding='0' cellspacing='0' border='0' >";
	$table .= "<tbody>";

	if ($dataloader['item_materials']) { 

		foreach ($dataloader['item_materials'] as $row) {

			$btnItemMaterial = $disabled['item_materials'] 
				? null 
				: "<span class='btn-list-action btn-remove' ><i class='fa fa-minus-square'></i></span>";

			$fieldName = $disabled['item_materials']
				? $row['item_material_name']
				: "<input type='text' name='item_material_name[]' value='{$row[item_material_name]}' placeholder='Material description' >";

			$table .= "<tr>";
			$table .= "<td>".$fieldName."</td>";
			$table .= "<td>".$btnItemMaterial."</td>";
			$table .= "</tr>";
		}
	}
	
	if (!$disabled['item_materials']) {
		$table .= "<tr class='newrow'>";
		$table .= "<td><input type='text' name='item_material_name[]' placeholder='Material description' ></td>";
		$table .= "<td valign='bottom'><span class='btn-list-action btn-add' ><i class='fa fa-plus-square'></i></span></td>";
		$table .= "</tr>";
	}

	$table .= "</tbody>";
	$table .= "</table>";
	$table .= "</div>";

	unset($dataloader['item_materials']);
	$data['item_materials'] =  $table;


	$form->fieldset('Other Materials', array(
		'item_materials',
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>

<style type="text/css">
	

	.btn-list-action {
		font-size: 18px;
		color: #888;
		cursor: pointer;
	}

	.btn-list-action:hover {color: #000;}

	.btn-list-action.btn-add {
		display: none;
	}

	form.default .form-list input[type="text"] {
		width: auto;
	}

	form.default .form-list tr th,
	form.default .form-list tr td {
		font-weight: 400;
		text-align: left;
		padding: 5px;
	}

	form.default .form-list tr td:first-child {
		padding-left: 0;
	}

	form.default .form-list tr:last-child .add-btn-live {
		display: block;
	}

	form.default table.item-materials-list input[type="text"] {
		width: 300px;
	}

	form.default table.item-electrical-list td {
		vertical-align: bottom;
	}

	form.default table.item-electrical-list textarea {
		width: 200px;
		height: 64px;
		box-sizing: border-box;
		resize: none;
		outline: none;
	}

	form.default table.item-electrical-list tr td:last-child {
		padding-bottom: 10px;
	}
	
</style>
<?php 
	
	echo $form; 
?>