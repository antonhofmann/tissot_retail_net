<?php 
	$user = User::instance();
	$translate = Translate::instance();
?>

<style type="text/css">

	#items { 
		width: 800px; 
	}

	input.item-quantity {
		width: 24px !important;
	}
	
</style>
<script type="text/javascript">
	$(document).ready(function() {

		//get number of text input element in a html document
		var inputs = $('input.item-quantity').length;

		$('input.item-quantity').keydown(function(e) {
			
			if(e.keyCode==13) {
				
				//get the next index of text input element
				var next = $('input.item-quantity').index(this)+1;

				if(inputs != next) {
					$('input.item-quantity:eq('+next+')').focus();
				}
			}
		});

		$('input.item-quantity').change(function(e) {
			
			var item = $(this).data('id');
			var quantity = $(this).val();
			var data = $('form.request').serializeArray();

			data.push({ name: 'section',  value: 'item.group.item' });
			data.push({ name: 'item',  value: item });
			data.push({ name: 'quantity',  value: quantity });

			retailnet.ajax.json('/applications/helpers/item.ajax.php', data).done(function(xhr) {
				if (xhr.message) {
					if (xhr.success) retailnet.notification.success(xhr.message);
					else retailnet.notification.error(xhr.message);
				}
			});
		});

	});
</script>
<div id="items">
<?php 

	if ($datagrid) {
		$table = new Table();
		$table->datagrid = $datagrid;
		$table->quantity('width=20px');
		$table->item_code('width=20%');
		$table->item_name();
		echo $table->render();
	}
?>
</div>
<div class='actions'>
<?php 
	
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>
<?php
	echo Request::instance()->form();
?>