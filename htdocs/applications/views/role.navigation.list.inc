<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();
?>
<style type="text/css">

	#navigations_list {
		width: 500px;
	}
  
	ul.dynatree-container {
		width: 480px;
		border: 1px solid silver;
		padding: 10px;
	}
  
	#treeToggler {
  		float: right;
	}
  
</style>
<script type="text/javascript">
$(function(){

	var tree = $("#tree");
	var role = $('#id').val();

	tree.dynatree({
		initAjax: {
			url: '/applications/helpers/navigation.ajax.php?section=json_permissions&role='+role
		},
		checkbox: true,
		onSelect: function(select, node) { 
			$.ajax({
				url: '/applications/helpers/role.navigation.save.php',
				dataType: 'json',
				data: {
					role: role,
					navigation: node.data.key,
					selected: (select) ? 1 : 0
				},
				success: function(json) {

					$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
					
					if (json) {

						if (json.message) {
							$.jGrowl(json.message,{ 
								sticky: false, 
								theme: (json.response) ? 'message' : 'error' 
							});
						}

						if (json.childs) {
							$.each(json.childs, function(i, key) { 
								var child = node.tree.getNodeByKey(key);
								child.select(json.select);
							});
						}
					}
				}
			});
		}
	});

	$("#treeToggler").click(function(){

		var collapse = ($(this).hasClass('collapse')) ? false : true;
		$(this).toggleClass('collapse');

		$("#tree").dynatree("getRoot").visit(function(node){
			node.expand(collapse);
		});

		return false;
	});
});
</script>
<?php 
	echo $request->form($requestFields);
?>
<div id="navigations_list">
	<div class='table-toolbox'>
		<form class=toolbox method=post>
		<?php 
			echo ui::button(array(
				'id' => 'treeToggler',
				'class' => '-expande',
				'icon' => 'direction-down',
				'label' => $translate->toggle_expand
			));
		?>
		</form>
	</div>
	<div id="tree"></div>
</div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>