<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
<script type="text/javascript">
$(document).ready(function() {

	var widgets = $('#widgets').tableLoader({
		url: '/applications/helpers/widgets.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">
	#widgets { width: 600px; }
	.fa { margin: 0 auto; }
</style>
<?php 
	echo Request::instance()->form();
?>
<div id="widgets"></div>