<?php 

	$translate = Translate::instance();

	$form = new Form(array(
		'id'=>'passwordform',
		'action'=>'/applications/helpers/security.check.user.php',
		'autocomplete' => 'off'
	));
	
	$form->id(
		Form::TYPE_HIDDEN,
		"value=".$id		
	);
	
	$form->firstname(
		Form::TYPE_TEXT, 
		Validata::PARAM_REQUIRED
	);
	
	$form->lastname(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->email(
		Form::TYPE_TEXT,
		Form::TOOLTIP_EMAIL,
		Validata::PARAM_REQUIRED,
		Validata::PARAM_EMAIL
	);
	
	$form->username(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	if (Session::get('user_id')) {
		$form->old_password(
			Form::TYPE_PASSWORD,
			Validata::PARAM_REQUIRED,
			Validata::ajax('oldPassword')
		);
	}
	
	$form->new_password(
		Form::TYPE_PASSWORD,
		Form::TOOLTIP_USE_LETETRS_NUMBERS,
		Form::TOOLTIP_LEAST_ONE_LETTER_NUMBER,
		Form::TOOLTIP_NOT_SPECIAL_CHARACTERS,
		Form::TOOLTIP_NOT_INTERNATIONAL_LETTERS,
		Form::TOOLTIP_LENGTH_EIGHT_CHARACTERS,
		Form::TOOLTIP_PASSWORD_NOT_PART_USERNAME,
		Validata::PARAM_REQUIRED,
		Validata::ajax('validataPassword')
	);
	
	$form->repeat_password(
		Form::TYPE_PASSWORD,
		Validata::PARAM_REQUIRED,
		Validata::equals('new_password')
	);
	
	$form->password_notification(
		Form::TYPE_CHECKBOX
	);
	
	$form->fieldset('personal_data', array(
		'firstname',
		'lastname',
		'email'
	));
	
	$form->fieldset('login_data', array(
		'username',
		'old_password'
	));

	$form->fieldset('new_password', array(
		'new_password',
		'repeat_password',
		'password_notification'
	));
	
	$form->button(ui::button(array(
		'id' => 'submitRequest',
		'class' => 'action blue',
		'caption' => $translate->submit_request
	)));
	
	$form = $form->render();

?>
<script type="text/javascript" src="/public/js/security.password.reset.js"></script>
<style type="text/css">
	#passwordform {
		width: 660px;
	}
</style>
<?php echo $form; ?>
