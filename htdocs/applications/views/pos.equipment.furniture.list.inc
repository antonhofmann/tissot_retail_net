<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript" src="/public/js/pos.equipment.furnitures.js"></script>
<style type="text/css">

	#furniture {
		width: 1000px;
	}
	
</style>
<?php 
	echo $request->form(array(
		'id' => ''
	));
?>
<div id="furniture"></div>
<div class='modalbox-container'>
	<div id="modalbox" class="modalbox modal">
		<div class=modalbox-header >
			<div class='title'>POS Locations</div>
			<div class='subtitle'>Equipment: <strong></strong></div>
		</div>
		<div class=modalbox-content-container >
			<div class=modalbox-content></div>
		</div>
		<div class=modalbox-footer >
			<div class=modalbox-actions >
				<a id=cancel class='button'>
					<span class="icon cancel"></span>
					<span class="label">Cancel</span>
				</a>
				<a id="print" class='button'>
					<span class="icon print"></span>
					<span class="label">Print</span>
				</a>
			</div>
		</div>
	</div>
</div>
