<?php 
	$user = User::instance();
	$translate = Translate::instance();
?>

<style type="text/css">

	#items { 
		width: 700px; 
	}

	h5 { 
		display: block;
		margin: 40px 0 20px; 
		font-size: 1em;
		font-variant: small-caps;
		border-bottom: 1px solid silver;
		color: gray;
		padding-bottom: 5px;
		cursor: pointer;
	}

	h5:hover { 
		color: #1b5584;
	}

	#items table {
		margin-bottom: 40px;
	}
	
</style>
<script type="text/javascript">
	$(document).ready(function() {
	
		$('input.part').on('change', function() {
			
			var self = $(this);
			var checked = self.is(':checked') ? 1 : 0;
			var data = $('form.request').serializeArray();

			// add checked value
			data.push({ name: 'section',  value: 'item.part'});
			data.push({ name: 'checked',  value: checked});
			data.push({ name: 'child',  value: self.val() });

			// check controllr for checkall
			// var table = self.closest('table');
			//$('input.partall', table).trigger('checkall');
			
			retailnet.ajax.json('/applications/helpers/item.ajax.php', data).done(function(xhr) {
				if (xhr.message) {
					if (xhr.success) retailnet.notification.success(xhr.message);
					else retailnet.notification.error(xhr.message);
				}
			});
		});	

		$('input.partall').on({
			
			/**
			 * This trigger chack all subboxes and set checked status
			 * @return void
			 */
			checkall: function(e) {

				var table = $(this).closest('table');
				var boxes = $('input.category', table);
				var checkedBoxes = $('input.category:checked', table);

				$(this).attr('checked', (boxes.length==checkedBoxes.length) ? true : false);
			},
			
			change: function(e) {
				
				var table = $(this).closest('table');
				var boxes = $('input.category', table);
				var checked = $(this).is(':checked') ? true : false;

				boxes.each(function(i,el) {
					$(el).attr('checked', checked).trigger('change');
				});
			}
		});

		// trigger all checkall buttons
		//$('input.productline').trigger('checkall');
		
	});
</script>
<div id="items">
<?php 
	if ($items) {

		$table = new Table();
		$table->datagrid = $items;
		$table->checkbox('width=20px');
		$table->item_code(Table::ATTRIBUTE_NOWRAP, 'width=20%');
		$table->item_name();
		$table->item_price(Table::ATTRIBUTE_NOWRAP);
		$table->active(Table::ATTRIBUTE_NOWRAP, 'align=center');
		$table->caption('active', $translate->item_active);

		echo $table->render();
	}
?>
</div>
<?php echo Request::instance()->form(); ?>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>