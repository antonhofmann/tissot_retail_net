<?php 

	$settings = Settings::init();
	$user = User::instance();
	$request = request::instance();
	$translate = Translate::instance();
?>
<style type="text/css">
	
	.company-header {
		display: block;
		width: 100%;
		min-height: 200px;
	}

	.company-header .box.small label {
		width: 180px;
	}
	
	.company-header .box {
		display: block;
		width: 500px;
		background-color: white;
		border: 1px solid silver;
		padding: 10px;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
		float: left;
		margin-right: 20px;
		min-height: 120px;
	}
	
	.company-header .box.small {
		width: 350px;
	}
	
	.company-header .box.medium {
		width: 400px;
	}
	
	.company-header .row-control {
		display: block;
		width: 100%;
		line-height: 25px;
		font-size: 13px;
		margin-bottom: 5px;
	}

	.row-control label {
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
	}

</style>

<div class="company-header">
	<div class="box">
		<div class="row-control">
			<label><?php echo $translate->address_company ?>:</label>
			<span><?php echo $data['address_company']; ?></span>
		</div>
		<?php if ($data['address_company2']) : ?>
		<div class="row-control">
			<label>&nbsp;</label>
			<span><?php echo $data['address_company2']; ?></span>
		</div>
		<?php endif; ?>
		<div class="row-control">
			<label><?php echo $translate->address_address ?>:</label>
			<span><?php echo $data['address_street'].' '.$data['address_streetnumber']; ?></span>
		</div>
		<?php if ($data['address_address2']) : ?>
		<div class="row-control">
			<label>&nbsp;</label>
			<span><?php echo $data['address_address2']; ?></span>
		</div>
		<?php endif; ?>
		<div class="row-control">
			<label>&nbsp;</label>
			<span><?php echo $data['address_zip'].' '.$data['place_name'].' '.$data['country_name']; ?></span>
		</div>
	</div>
	<div class=" box medium">
		<div class="row-control">
			<label><?php echo $translate->address_phone ?>:</label>
			<span><?php echo $data['address_phone']; ?></span>
		</div>
		<div class="row-control">
			<label><?php echo $translate->address_mobile_phone ?>:</label>
			<span><?php echo $data['address_mobile_phone']; ?></span>
		</div>
		<div class="row-control">
			<label><?php echo $translate->address_email ?>:</label>
			<span><?php echo $data['address_email']; ?></span>
		</div>
		<?php if ($data['address_website']) : ?>
		<div class="row-control">
			<label><?php echo $translate->address_website ?>:</label>
			<span><?php echo $data['address_website']; ?></span>
		</div>
		<?php endif; ?>
	</div>
	<div class=" box small">
		<div class="row-control">
			<label><?php echo $translate->address_number ?>:</label>
			<span><?php echo $data['address_number']; ?></span>
		</div>
		<div class="row-control">
			<label><?php echo $translate->address_sapnr ?>:</label>
			<span><?php echo $data['address_sapnr']; ?></span>
		</div>
		<div class="row-control">
			<label><?php echo $translate->address_mps_customernumber ?>:</label>
			<span><?php echo $data['address_mps_customernumber']; ?></span>
		</div>
		<div class="row-control">
			<label><?php echo $translate->address_mps_shipto ?>:</label>
			<span><?php echo $data['address_mps_shipto']; ?></span>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {

	var users = $('#users').tableLoader({
		url: '/applications/helpers/user.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			$('select', self).dropdown();

			$('.close-account', self).click(function(e) { 

				e.preventDefault();

				var id = $(this).attr('data-id');
				
				if (id) {
					$('input.user-id', $('#accountMailContainer') ).val(id).trigger('change');
				} else {
					retailnet.notification.error('Error: User not found.');
				}

				return false;
			});
		}
	});

});
</script>
<style type="text/css">

	#users { 
		width: 1400px;
	}
	
	.pdf { 
		cursor: pointer; }
	
	.planning_roles span,
	.other_roles span,
	.roles span {
		display: block; 
		padding: 0 0 2px;
	}
	
</style>
<div id="users" class="users-list"></div>
<?php 
	echo $request->form();
?>
<div class='actions'>
<?php 
	if ($buttons['back']) {
		echo ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => $translate->back
		));
	}
?>
</div>

<script type="text/javascript" src="/public/scripts/tinymce4/js/tinymce/tinymce.min.js"></script>

<!-- mail template modal -->
<div id="accountMailContainer" class="ado-modal ado-box">
	<div class="ado-modal-header">
		<div class="ado-title">Request to Close Account</div>
		<div class="ado-subtitle"></div>
	</div>
	<div class="ado-modal-body">
		<form method="post" action="/applications/modules/user/sendmail.php" >
			<input type="hidden" name="application" value="<?php echo $request->application ?>" />
			<input type="hidden" name="controller" value="<?php echo $request->controller ?>" />
			<input type="hidden" name="close" value="1" />
			<input type="hidden" name="user" class="user-id" />
			<input type="hidden" name="mail_template_id" class="mail-id" value="11" />
			<input type="hidden" name="mail_template_view_modal" id="modal-trigger" />
			<div class="ado-row">
				<input type=text name="mail_template_subject" class="ado-modal-input required mail-subject" />
			</div>
			<div class="ado-row">
				<textarea name="mail_template_text" id="mailAccountContent" class="ado-modal-input required mail-content" ></textarea>
			</div>
			<div class="ado-row">
				<p><small>Please indicate the reason why you want to close this account.</small></p>
				<textarea name="close_account_reason" id="close_account_reason" class="ado-modal-input mail-reason" rows="2" ></textarea>
			</div>
		</form>
	</div>
	<div class="ado-modal-footer" >
		<div class="ado-row ado-actions">
			<a class="button cancel ado-modal-close">
				<span class="icon cancel"></span>
				<span class="label"><?php echo $translate->cancel ?></span>
			</a>
			<a class="button test-mail" href="#">
				<span class="icon mail"></span>
				<span class="label">Test Mail</span>
			</a>
			<a class="button preview-mail" href="/applications/modules/user/sendmail.preview.php">
				<span class="icon icon84"></span>
				<span class="label">Preview</span>
			</a>
			<a class="button sendmail" href="#">
				<span class="icon mail"></span>
				<span class="label"><?php echo $translate->sendmail ?></span>
			</a>
		</div>
	</div>
</div>

<!-- preview mail modal -->
<div id="preview-mail-template" class="ado-modal ado-box">
	<div class="ado-modal-header">
		<span class="fa-stack ado-modal-close">
			<i class="fa fa-circle fa-stack-2x"></i>
			<i class="fa fa-times fa-stack-1x fa-inverse"></i>
		</span>
		<div class="ado-row ado-title mail-subject"></div>
		<div class="ado-row" >
			<span class="ado-label ado-width-30">To:</span>
			<span class="ado-label mail-address"></span>
		</div>			
		<div class="ado-row">
			<span class="ado-label ado-width-30">CC:</span>
			<span class="ado-label mail-cc-address"></span>
		</div>
		<div class="ado-notification-right-bottom">
			<span class="ado-label mail-date"></span>
		</div>
	</div>
	<div class="ado-modal-body ado-max-height-500 mail-content"></div>
	<div class="ado-modal-footer">
		<div class="mail-footer"></div>
	</div>
</div>

<!-- test mail modal dialog -->
<div id="test-mail-template"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" >Send Test E-mail an:</div>
	</div>
	<div class="ado-modal-body">
		<input type="text" name="test-mail-recipient" id="test-mail-recipient" class="ado-modal-input required" value="<?php echo $user->email ?>" placeholder="Recipient E-mail" />
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="/applications/modules/user/sendmail.php" >
				<span class="icon mail"></span>
				<span class="label" ><?php echo $translate->send ?></span>
			</a>
		</div>
	</div>
</div>
