<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#language_applications').tableLoader({
		url: '/applications/helpers/language.application.list.php',
		data: $('form.request').serializeArray()
	});

	$(document).delegate("input.application", "change", function() {

		var checked = ($(this).is(':checked')) ? 1 : 0;

		if (!checked) {
			$('.checkall').removeAttr('checked');
		} else {
			var data = [];
			var checkbox = $('input.application');
			var count = 0;
			$.each(checkbox, function(e) {
				if ($(this).is(':checked')) data.push(1);
				count++;
			});
			if (count == data.length ) {
				$('.checkall').attr('checked', 'checked')
			}
		}
		
		var request = $.getJSON('/applications/helpers/language.application.save.php', { 
			language: $('#id').val(),
			application: $(this).val(),
			action: checked
		});

		request.success(function(json) {
			if (json.message) {
				$.jGrowl(json.message, {
					life: 1000, 
					theme: (json.response) ? 'message' : 'error'
				});
			}
		});
	});

	$(document).delegate(".checkall", "click", function() {
		
		var checkbox = $('input.application');
		var checked = ($(this).is(':checked')) ? 'checked' : false;

		$.each(checkbox, function(e) {

			if (checked && !$(this).is(':checked')) {
				$(this).attr('checked', 'checked').trigger('change')
			}
			
			if (!checked && $(this).is(':checked')) {
				$(this).attr('checked', false).trigger('change')
			}
		});
	});
	
});
</script>
<style type="text/css">

	#language_applications { 
		width: 500px; 
	}
	
</style>
<?php 
	echo $request->form($requestFields);
?>
<div id="language_applications"></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>