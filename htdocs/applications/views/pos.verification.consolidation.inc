<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();
	echo $request->form(); 
?>
<div class="actions">
	<form class="filters">
		<?php 
		
			// display buttons
			if ($buttons) {
				echo join(array_values($buttons));
			}
	
			if ($dataloader['periodes']) {
				echo html::select($dataloader['periodes'], array(
					'name' => 'periodes',
					'id' => 'periodes',
					'class' => 'submit selectbox',
					'value' => $period,
					'caption' => $translate->period
				));
			}
		
			if ($dataloader['regions']) {
				echo html::select($dataloader['regions'], array(
					'name' => 'regions',
					'id' => 'regions',
					'class' => 'submit selectbox',
					'value' => $region,
					'caption' => $translate->all_regions
				));
			}
			
			if ($dataloader['states']) {
				echo html::select($dataloader['states'], array(
					'name' => 'states',
					'id' => 'states',
					'class' => 'submit selectbox',
					'value' => $state,
					'caption' => 'Data Entry Status'
				));
			}
			
			if ($dataloader['clients']) {
				echo html::select($dataloader['clients'], array(
					'id' => 'clients',
					'name' => 'clients',
					'value' => $data['clients'],
					'class' => 'submit selectbox',
					'caption' => 'All Clients'
				));	
			}
			
			if ($dataloader['addresstypes']) {
				echo html::select($dataloader['addresstypes'], array(
					'id' => 'addresstypes',
					'name' => 'addresstypes',
					'value' => $data['addresstype'],
					'class' => 'submit selectbox',
					'caption' => 'All Client Types'
				));	
			}
	
		?>
	</form>
</div>
<div id="spreadsheet"></div>
