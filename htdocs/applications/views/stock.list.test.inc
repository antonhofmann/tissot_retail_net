<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
?>
<link type='text/css' rel='stylesheet' href='/public/scripts/spreadsheet/spreadsheet.css' />
<script type='text/javascript' src='/public/scripts/spreadsheet/spreadsheet.js'></script>

<script type="text/javascript">

	$(document).ready(function() {


		var data = { 
			a1:{text:'Test'}, 
			c2:{text:'Test2'} 
		};
		
		var spreadsheet = new Spreadsheet('#spreadsheet', data, {
			fixTop: 2, 
			fixLeft: 2, 
			readonly: false
		});

	});
	
</script>

<style type="text/css">
	
	#spreadsheet {
		display: block;
		position: relative;
		width: 100%;
		top: 80px;
	    bottom: 10px;
	    border: 1px solid silver;
	    min-height: 600px;
	}}
	
	
</style>

<div id='spreadsheet'></div> 
				