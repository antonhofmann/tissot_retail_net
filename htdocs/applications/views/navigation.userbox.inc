<?php 
	
	$user = user::instance();
	$translate = Translate::instance();
	$request = request::instance();

	if ($user->id) :  ?>
		<a href="#" class="dropdown-toggle" id="user-menu" data-toggle="dropdown">
			<i class="fa fa-user"></i> 
			<strong><?php echo "$user->firstname $user->name"?></strong> 
			<b class="caret"></b>
		</a>
		<ul class="dropdown-menu" role="menu" aria-labelledby="user-menu">
			<li><a href="/home"> <i class="fa fa-tachometer"></i> Dashboard</a></li>
			<li><a href="/user/my_company.php"><i class="fa fa-users"></i> My Company</a></li>
			<?php echo user::permission(Idea::PERMISSION_NEW) ? "<li><a href='/public/themes/default/idea.html' data-fancybox-type='iframe' id='btnIdea' ><i class='fa fa-comment'></i> $translate->idea</a></li>": null; ?>
			<li class="divider"></li>
			<li><a href="/security/logout"><i class="fa fa-power-off"></i> Logout</a></li>
		</ul>
	<?php endif; ?>


		