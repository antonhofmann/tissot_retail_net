<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#language_applications').tableLoader({
		url: '/applications/helpers/application.language.list.php',
		data: $('.request').serializeArray()
	});

	$(document).delegate("input.language", "change", function(event) {

		event.stopPropagation();
		
		var checked = ($(this).is(':checked')) ? 1 : 0;
		var languages = $('input.language');
		var languagesChecked = $('input.language:checked');

		var data = { 
			application: $('#id').val(),
			language: $(this).val(),
			action: checked
		};
		
		// checkall box
		var attribute = (languages.length == languagesChecked.length) ? 'checked' : false;
		$('.checkall').attr('checked', attribute);
		
		var request = $.getJSON('/applications/helpers/application.language.save.php', data, function(json) {
			if (json) {
				$.jGrowl(json.message, {
					life: 1000, 
					theme: (json.response) ? 'message' : 'error'
				});
			}
		});
	});

	$(document).delegate(".checkall", "click", function() {
		
		var checked = ($(this).is(':checked')) ? 'checked' : false;

		$.each($('input.language'), function(e) {
			$(this).attr('checked', checked).trigger('change');
		});
	});
	
});
</script>
<style type="text/css">

	#language_applications { 
		width: 500px; 
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="language_applications"></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>