<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$data['file'] = '
		<span id="showfile" class="button">
			<span class="icon download"></span>
			<span class="label">'.$translate->download_file.'</span>	
		</span>
		<span id="upload" class="button">
			<span class="icon upload"></span>
			<span class="label">'.$translate->upload_file.'</span>	
		</span>
		<input type=hidden name="posfile_path" id="posfile_path" value="'.$data['posfile_path'].'" class="file_path" />
		<input type="hidden" name="has_upload" id="has_upload" />
	';

	$form = new Form(array(
		'id' => 'posfile',
		'action' => '/applications/helpers/pos.file.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->posfile_posaddress(
		Form::TYPE_HIDDEN
	);
	
	$form->posfile_id(
		Form::TYPE_HIDDEN,
		'class=file_id'
	);
	
	$form->posfile_filegroup(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->posfile_title(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		'class=file_title'
	);
	
	$form->file(
		Form::TYPE_AJAX,
		FORM::PARAM_SHOW_VALUE
	);
	
	$form->posfile_description(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);
	
	$form->fieldset('file', array(
		'posfile_filegroup',
		'posfile_title',
		'file', 
		'posfile_description'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<style type="text/css">
	
	#posfile { 
		width: 700px;
	}
	
	form .equipment input { 
		width: 120px; 
	}
	
	#posfile_description { 
		width: 440px; 
		height: 100px; 
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete_file,
		'content' => $translate->dialog_delete_file,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>