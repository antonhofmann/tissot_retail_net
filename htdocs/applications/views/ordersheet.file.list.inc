<?php 
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';
	
	// settings
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = Request::instance();
	
	$model = new Model($request->application);
	
	// collection category files
	$result = $model->query("
		SELECT DISTINCT
			mps_material_collection_category_file_id AS file, 
			mps_material_collection_category_file_title AS title,
			mps_material_collection_category_file_path AS path,
			DATE_FORMAT(mps_material_collection_category_files.date_created, '%d.%m.%Y') AS date,
			mps_material_collection_category_id AS category,
			mps_material_collection_category_code AS name
		FROM mps_ordersheet_items
	")
	->bind(Ordersheet_Item::DB_BIND_MATERIALS)
	->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
	->bind(Material_Collection_Category::DB_BIND_COLLECTION_CATEGORY_FILES)
	->filter('visible', "mps_material_collection_category_file_visible = 1")
	->filter('ordersheet', "mps_ordersheet_item_ordersheet_id = $id")
	->order('mps_material_collection_category_file_title')
	->fetchAll();
	
	if ($result) { 
		
		$datagrid = array();
		
		// froup files
		foreach ($result as $row) {
			$file = $row['file'];
			$category = $row['category'];
			$title = $row['title'];
			$path = $row['path'];
			$date = $translate->date_created.': '.$row['date'];
			$extension = file::extension($path);
			$datagrid[$category]['caption'] = $row['name'];
			$datagrid[$category]['files'][$file]['title'] = "<span class=title data='$path'>$title</span><span class=description >$date</span>";
			$datagrid[$category]['files'][$file]['fileicon']  = "<span class='file-extension $extension modal' data='$path'></span>";
		}
		
		$filelist .= "<h5>".$translate->collection_category_files."</h5>";

		foreach ($datagrid as $key => $row) {
			$table = new Table(array('thead' => false));
			$table->datagrid = $row['files'];
			$table->fileicon();
			$table->title();
			$filelist .= "<h6>".$row['caption']."</h6>";
			$filelist .= $table->render();
		}
	}
	
	// mastersheet files
	$result = $model->query("
		SELECT DISTINCT
			mps_mastersheet_file_id AS file,
			mps_mastersheet_file_path AS path,
			mps_mastersheet_file_title As title,
			DATE_FORMAT(mps_mastersheet_files.date_created, '%d.%m.%Y') AS date,
			file_type_id AS category,
			file_type_name AS name
		FROM mps_mastersheet_files
	")
	->bind(Mastersheet_File::DB_BIND_FILE_TYPES)
	->bind(Mastersheet_File::DB_BIND_ORDERSHEETS)
	->filter('visible', 'mps_mastersheet_file_visible = 1')
	->filter('mastersheet', "mps_ordersheet_id = $id")
	->order('file_type_name')
	->order('mps_mastersheet_file_title')
	->fetchAll();
	
	if ($result) {
		
		$datagrid = array();
		
		// froup files
		foreach ($result as $row) {
			$file = $row['file'];
			$category = $row['category'];
			$title = $row['title'];
			$path = $row['path'];
			$date = $translate->date_created.': '.$row['date'];
			$extension = file::extension($path);
			$datagrid[$category]['caption'] = $row['name'];
			$datagrid[$category]['files'][$file]['title'] = "<span class='title modal' data='$path'>$title</span><span class=description >$date</span>";
			$datagrid[$category]['files'][$file]['fileicon']  = "<span class='file-extension $extension modal' data='$path'></span>";
		}
		
		$filelist .= "<h5>".$translate->master_sheet_files."</h5>";
	
		foreach ($datagrid as $filetype => $files) {
			$table = new Table(array('thead' => false));
			$table->datagrid = $files['files'];
			$table->fileicon();
			$table->title();
			$filelist .= "<h6>".$files['caption']."</h6>";
			$filelist .= $table->render();
		}
	}
	
	// empty box
	if (!$filelist) {
		$filelist = html::emptybox($translate->empty_result);
	}
?>
<script type="text/javascript">
	$(document).ready(function() {

		$(".modal").click(function() {
			
			var path = $(this).attr('data');
			var title = ($(this).hasClass('title')) ? $(this).text() : $(this).parent().next().find('.title').text();
			title = (title) ? title : '';
		
			if (isImage(path)) {
				retailnet.modal.show(path, {title:title});
			} else {
				window.open('http://'+location.host+path);
			}
		});
		
	});
</script>
<style type="text/css">

	#ordersheet_files { 
		width: 700px; 
	}
	
	span.date,
	span.description {
		display: block; 
		color: gray; 
		font-size: 11px;
	}
	
	td.fileicon {
		width: 32px;
	}
	
	td.date {
		width: 100px;
	}
	
	h6 { 
		display: block; 
		margin-bottom: 5px; 
		font-size: .8em;
		font-weight: 500;
	}
	
	h5 { 
		display: block;
		margin: 40px 0 20px; 
		font-size: 1em;
		font-variant: small-caps;
		border-bottom: 1px solid silver;
		color: gray;
	}

	table { 
		margin-bottom:20px; 
		border-style: solid; 
		border-color: silver; 
	}
	
	.file-extension {
		cursor: pointer;
	}
	
	span.title {
		cursor: pointer;
		color: royalblue;
	}

	span.filename {display:block; font-weight:600; color:#0050CC; cursor:pointer; font-size: 11px;}
	span.filename:hover { color: black; }
	
</style>
<div id="ordersheet_files" >
	<?php echo $filelist; ?>
</div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>
