<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();
	
	$form = new Form(array(
		'id' => 'applicationForm',
		'action' => '/applications/helpers/application.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->application_id(
		Form::TYPE_HIDDEN
	);
	
	$form->application_db(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->application_shortcut(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->application_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->application_description(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);
	
	$form->application_active(
		Form::TYPE_CHECKBOX
	);
	
	$form->application_involved_in_planning(
		Form::TYPE_CHECKBOX
	);
	
	$form->fieldset('application', array(
		'application_db',
		'application_shortcut',
		'application_name',
		'application_description',
		'application_active',
		'application_involved_in_planning'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>

<style type="text/css">

	#applicationForm { 
		width: 700px; 
	}
	
	#applicationForm textarea {
		width: 440px;
		height: 60px;
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>