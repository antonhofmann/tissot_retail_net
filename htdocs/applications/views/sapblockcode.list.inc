<script type="text/javascript">
$(document).ready(function() {

	var sapCountries = $('#sapCountries').tableLoader({
		url: '/applications/helpers/sap.blockcode.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});

});
</script>
<style type="text/css">
	#sapCountries { width: 900px; }
</style>
<div id="sapCountries"></div>
<?php 
	echo Request::instance()->form();
?>
