<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<link href="/public/scripts/dropdown/dropdown.css" rel="stylesheet" type="text/css" />
<script src="/public/scripts/dropdown/dropdown.js" type="text/javascript"  ></script>
<script type="text/javascript" src="/public/scripts/table.loader.js" ></script>
<script type="text/javascript">
$(document).ready(function() {
	$('#projectPartners').tableLoader({
		url: '/applications/helpers/red.project.partner.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select', self).dropdown();
		}
	});
});
</script>

<style type="text/css"> 

	#projectPartners { 
		width: 1400px; 
	}
	
</style>
<?php 
	echo $request->form($requestFields); 
?>
<div id="projectPartners"></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>