<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();
	
	$material = new Material($request->application);
	$material->read($id);
	
	$form = new Form(array(
		'id' => 'bulkform',
		'action' => '/applications/helpers/material.bulk.save.php',
		'method' => 'post'
	));
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_material_id(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_mastersheet_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->caption('item_id', "Replace <b>".$data['mps_material_code']."</b> with:");
	
	$form->item_id(
		Form::TYPE_SELECT,
		FOrm::PARAM_AJAX,
		Validata::PARAM_REQUIRED
	);
	
	$form->select_all(
		Form::TYPE_CHECKBOX,
		'checked=checked',
		"caption=".$translate->deselect_all,
		"recaption=".$translate->select_all
	);
	
	$form->ordersheets(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE
	);
	
	$form->proposed_quantity(
		Form::TYPE_TEXT
	);
	
	$form->send_mail_clients(
		Form::TYPE_CHECKBOX
	);

	$form->fieldset('master_sheet', array(
		'action',
		'mps_mastersheet_id',
		'item_id', 
		'select_all',
		'ordersheets', 
		'proposed_quantity',
		'send_mail_clients'
	));

	$form->dataloader($data);
	$form->dataloader($dataloader);
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['update']) {
		$form->button(ui::button(array(
			'id' => 'update',
			'icon' => 'save',
			'label' => $translate->update
		)));
	}
	
	$bulkform = $form->render();
		
?>
<script type="text/javascript" src="/public/js/material.bulk.js"></script>
<style type="text/css">

	#bulkform { width: 840px; }
	#bulkform label { width: 220px !important; }
	
	#ordersheets input[type=checkbox] { margin-right: 5px; }
	#ordersheets strong { font-weight:500; }
	span.ordersheet { display: block; line-height: 25px; }
	.noresult { color: gray; font-size: 14px; }
	#proposed_quantity { width:150px !important; }
	

	
	#mailform label {
		margin-left: 0 !important;
	}
	
</style>
<?php 
	echo $bulkform; 
	
	// mail template: set
	$mailTemplate = new Mail_Template();
	$mailTemplate->read(4);
	
	// mail template: data loader
	$maildata = $mailTemplate->data;
	
	/*
	$form = new Form(array(
		'id' => 'mailform',
		'action' => '/applications/helpers/ordersheet.sendmail.php',
		'method' => 'post'
	));
	
	$form->mail_template_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mail_template_view_modal(
		Form::TYPE_HIDDEN
	);
	
	$form->mail_template_subject(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mail_template_text(
		Form::TYPE_TEXTAREA,
		Validata::PARAM_REQUIRED
	);
		
	$form->dataloader($maildata);
	
	$mailform = $form->render();
	*/

	$modal = new Template('modal.mail');
	$modal->data('id', 'mailform');
	$modal->data('action', '/applications/helpers/ordersheet.sendmail.php');
	$modal->data('title', $translate->sendmail);
	$modal->data('subtitle', 'Material: '.$material->header());
	$modal->data('mail', $maildata);

	echo $modal;
?>