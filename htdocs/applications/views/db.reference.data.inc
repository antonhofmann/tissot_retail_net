<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'db_reference',
		'action' => '/applications/helpers/db.reference.save.php',
		'method' => 'post'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->db_reference_id(
		Form::TYPE_HIDDEN
	);
	
	$form->db_reference_referenced_table_id(
		Form::TYPE_HIDDEN
	);
	
	$form->db_table_db(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->db_reference_referring_table(
		Form::TYPE_SELECT,
		Form::PARAM_AJAX,
		Validata::PARAM_REQUIRED
	);
	
	$form->db_reference_foreign_key(
		Form::TYPE_SELECT,
		Form::PARAM_AJAX,
		Validata::PARAM_REQUIRED
	);

	
	$form->fieldset('table', array(
		'db_table_db',
		'db_reference_referring_table',
		'db_reference_foreign_key'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#db_reference");
		var id = $("#db_reference_id");

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			beforeSend: function () {
				retailnet.loader.show();
			},
			onAjaxFormComplete: function(status, form, json, options) {

				retailnet.loader.hide();
				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

				if (json) {

					if (json.response && json.redirect) {
						window.location=json.redirect;
					}

					if (json.message) {
						$.jGrowl(json.message, { 
							sticky: (json.response) ? false : true,
							theme: (json.response) ? 'message' : 'error'
						});
					}
				}
			}
		});
		
		$("#save").click(function(event) { 

			var error = false;
			var tableName = $('#db_reference_referring_table');

			event.preventDefault();
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

			if (tableName.hasClass('error')) {
				$.jGrowl(tableName.attr('alt'), { 
					sticky: true, 
					theme: 'error'
				});
			} else {
				form.submit();
			}

			return false;
		});

		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});

		$('.dialog').click(function(event) {

			event.preventDefault();

			var button = $(this);
			
			$('#apply, a.apply').attr('href', button.attr('href'));
			
			retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
		});

		$('#cancel').bind('click', function(event) {
			event.preventDefault();
			retailnet.modal.close();
			return false;
		});	

		$('#apply').bind('click', function(event) {
			event.preventDefault();
			window.location=$(this).attr('href');
		})

		// chainde counries
		$('#db_table_db').chainedSelect('#db_reference_referring_table', {
			url : "/applications/helpers/db.ajax.php",
			parameters: {
	      		section: "tables",
	      		get_only_registred: 1
	      	}
		}).trigger('change');

		$('#db_reference_referring_table').chainedSelect('#db_reference_foreign_key', {
			url : "/applications/helpers/db.ajax.php",
			parameters: {
	      		section: "fields"
	      	},
			before: function (target, settings) {
				settings.parameters.db = $('#db_table_db').val();
	        }
		});

	});
</script>

<style type="text/css">

	#db_reference { 
		width: 700px; 
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>