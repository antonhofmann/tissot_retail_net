<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#localWorkGroups').tableLoader({
		url: '/applications/helpers/local.work.groups.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">

	#localWorkGroups { 
		width: 900px; 
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="localWorkGroups"></div>