<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#list').tableLoader({
		url: '/applications/helpers/lps.collections.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">
	#list { width: 500px; }
</style>
<div id="list"></div>
<?php echo $request->form(); ?>