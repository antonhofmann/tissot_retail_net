<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#stocklist').tableLoader({
		url: '/applications/helpers/stock.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			
			$('select',self).dropdown();

			 $('a.popup').click(function(event) {
					
				event.preventDefault();

				var width = $(window).width();
				var height = $(window).height();
				var href = $(this).attr('href');
				var name = $(this).attr('name') || 'popupWindow';
				
				width = width - Math.ceil((10/100)*width);
				height = height - Math.ceil((10/100)*height);

				var spreadsheet = window.open(href, name, "width="+width+",height="+height+",resizable=1,scrollbars=1,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0");
				spreadsheet.focus();
				
				return false;
				
			});
		}
	});
	
});
</script>
<style type="text/css">
	
	#stocklist { 
		width: 900px; 
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="stocklist"></div>