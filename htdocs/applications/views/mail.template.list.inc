<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#mail_templates').tableLoader({
		url: '/applications/helpers/mail.template.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});

});
</script>
<style type="text/css">
	
	#mail_templates { 
		width: 900px; 
	}
	
	#mail_templates td a {
		display: block;
	}
	
	#mail_templates td span {
		display: block !important;
		color: gray;
		padding-top: 5px;
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="mail_templates"></div>