<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<link href="/public/scripts/dropdown/dropdown.css" rel="stylesheet" type="text/css" />
<script src="/public/scripts/dropdown/dropdown.js" type="text/javascript"  ></script>
<script type="text/javascript" src="/public/scripts/table.loader.js" ></script>
<script type="text/javascript">
$(document).ready(function() {

	$('#partners').tableLoader({
		url: '/applications/helpers/red.partner.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});

});
</script>
<style type="text/css">

	#partners { 
		width: 1200px; 
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="partners"></div>
