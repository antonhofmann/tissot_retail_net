<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$request = request::instance();
	$translate = Translate::instance();
	
	$mastersheet = new Mastersheet($request->application);
	$mastersheet->read($id);
?>
<script type="text/javascript">
$(document).ready(function() {

	var buttonSplittingDialog = $('#splitting'),
		dialogSplitting = $('#splitting_dialog'),
		buttonExport = $('#export'),
		dialogExport = $('#export_dialog'),
		selectedItems = $('#selected_items'),
		mastersheet = $('#mastersheet').val();


	// spiner instance	
	retailnet.loader.init();

	// load master sheet items
	$('#itemlist').tableLoader({
		url: '/applications/helpers/mastersheet.ordersheets.items.php',
		filters: $('.request'),
		after: function(self) {

			$('select',self).dropdown();

			// items checkboxes
			var items = $('table input.checkbox', self);
			
			if (items.length) {

				buttonExport.show();

				setSelected();

				$('table input.checkbox', self).click(function() {  
					var table = $(this).closest('table'); 
					setSelected(table);
				});

				$('table input.checkall', self).click(function() { 
					var table = $(this).closest('table');
					var tablebox = $('.checkbox', table);
					var checked = ($(this).is(':checked')) ? 'checked' : false;
					tablebox.attr('checked', checked);
					setSelected(table);
				});
				
			} else {
				var items = $('input.locally_provided', self);
				if (items.length == 0) buttonExport.hide();
			}
		}
	});


	// if button action required modal screen
	$('.modal-trigger').click(function(event) {

		event.preventDefault();

		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
		
		var button = $(this);
		var id = button.attr('id');

		// if clicked button is spliting list
		// then for each planning type on items list
		// create checkbox button as planning option
		// and append in planning modal screen dialog
		if (id==buttonSplittingDialog.attr('id')) { 

			// empty planning types place holder
			$('.planningtypes-placeholder', dialogSplitting).empty();
			
			$.each($('h5'), function(i, elem) {

				var id = $(this).attr('data');
				var caption = '<span>'+$(this).text()+'</span>';
				var input = '<input type=checkbox class=planning_type name=planning_type['+id+'] value=1 />';

				// insert input box 
				$('.planningtypes-placeholder', dialogSplitting).append('<p>'+input+caption+'</p>');
			});
		}

		// show modal box
		var modal = '#'+button.attr('id')+'_dialog';
		retailnet.modal.show(modal, {
			autoSize	: false,
			closeClick	: false,
			closeBtn	: false,
			fitToView   : true,
		    width       : '90%',
		    height      : '90%',
		    maxWidth	: '500px',
		    maxHeight	: '600px',
			modal 		: true
		});

		return false;
	});

	// submit splitting list
	$('.submit_splitting').click(function(event) {

		event.preventDefault();
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
		
		if (!$(this).hasClass('disabled')) {

			var selectedPlannings = $('input.planning_type:checked').length;
			var selectedOptions = $('input.option:checked').length;
			
			if (selectedPlannings && selectedOptions) {
				retailnet.loader.show();
				$('form', dialogSplitting).submit();
				$('input:checkbox', dialogSplitting).attr('checked', false);
				retailnet.loader.hide();
				retailnet.modal.close();
			} else {
				$.jGrowl('Please, select at least one planning type and one print option.', { 
					sticky: true, 
					theme: 'error'
				});
			}
		}

		return false;
	});


	// export to ramco
	$('.submit_export').click(function(event) {

		event.preventDefault();
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
		
		if (!$(this).hasClass('disabled')) {

			var errors = $('form', dialogExport).find('.error').length;
			var emptyFields = $('input:text[value=""]', dialogExport).length; 
	
			if (errors > 0 || emptyFields > 0) {
				$.jGrowl('Please enter all Delivery Dates.', { 
					sticky: true, 
					theme:'error'
				});
			} 
			else {
	
				retailnet.loader.show();
	
				$.ajax({
					type: "post",
					url: $('form', dialogExport).attr('action'),
					data: $('form', dialogExport).serializeArray(),
					cache: false,
					dataType: 'json',
					success: function(response) {
	
						retailnet.loader.hide();
						retailnet.modal.close();
	
						if (response.success) {
							window.location.reload();
						} else if (response.message) {
							$.jGrowl(response.message, { 
								sticky: true, 
								theme:'error'
							});
						}
					}
				});
			}
		}

		return false;
	});
	
	// button: export to ramco
	// load all not ordered items into the modal screen
	$('#export').click(function(event) {

		event.preventDefault();

		retailnet.notification.hide();
		retailnet.loader.show();

		var self = $(this);
		var items = selectedItems.val();
		var locallyProvidedItems = $('input.locally_provided');

		if (selectedItems.val() || locallyProvidedItems.length > 0) {

			// load sales order items
			getSalsorderItems().done(function(json) {

				// toggle export button
				// set class identificator for apply button
				$('.submit', dialogExport).addClass('submit_export');//.toggle(json.response);

				// assign responsed content
				// to modal screen content placeholder
				if (json.content) {
					$('#modalItemList', dialogExport).html(json.content);
				}

				// statemant controll for delivery dates
				$('table input.desire_date', dialogExport).change(function() {
					var date = $(this).val();
					if (!date || !isValidDate(date)) $(this).addClass('error');
					else $(this).removeClass('error');
				});

				// set form action
				$('form', dialogExport).attr('action', buttonExport.attr('href'));

				// master sheet delivery date
				$('input.desire_date').datepicker({ 
					dateFormat: 'dd.mm.yy',
					showOtherMonths : true,
					firstDay: 1,
					onClose: function(dateText, inst) { 
						var error = (!dateText || !isValidDate(dateText)) ? true : false;
						$(this).removeClass('error').toggleClass('error', error);
					}
				});

				// show modal box
				retailnet.modal.show('#export_dialog', {
					autoSize	: false,
					closeClick	: false,
					closeBtn	: false,
					fitToView   : true,
				    width       : '90%',
				    height      : '90%',
				    maxWidth	: '1000px',
				    maxHeight	: '900px',
					modal 		: true
				});

				retailnet.loader.hide();
			});
		} else {

			// display error message
			$.jGrowl( 'Please, select at least one item', { 
				sticky: true,
				theme: 'error'
			});
		}
		
		return false;
	});

	// close modal screen
	$('.cancel').bind('click', function(e) {
		e.preventDefault();
		$('#version_name').val('');
		retailnet.modal.close();
		retailnet.notification.hide();
		return false;
	});	

	
	var setSelected = function(table) { 

		var count = 0;
		var selected, checkbox, data;

		selected = selectedItems;

		if (table) checkbox = $('input.checkbox', table); 
		else checkbox = $('table input.checkbox');
		
		data = selected.val().split(',') || []; 
		
		$.each(checkbox, function() {

			var value = $(this).attr('value'); 
			var index = $.inArray(value, data);

			if ($(this).is(':checked')) {
				data.push(value);
				count++;
			} 
			else if (index>-1) {
				data.splice(index, 1);
			}
		});
		
		selectedItems.val('');
		
		if (data.length>0) {

			data = $.grep(data, function(n) { 
				return (n); 
			});

			data = $.grep(data, function(v,k){
			    return $.inArray(v,data) === k;
			});
			
			selectedItems.val(data.join(','));
		}

		if (table) {
			var checked = (checkbox.length == count) ? 'checked' : false;
			$('input.checkall', table).attr('checked', checked);
		}
	}


	// load sales orders item
	var getSalsorderItems = function(data) {
		return $.ajax({
			url: '/applications/helpers/mastersheet.salesorders.items.php',
			data: $('.request').serialize(),
			cache: false,
			dataType: 'json'
		});
	}

	// button: create version
	$('#btnVersion').on('click', function(e) {
		e.preventDefault();
		retailnet.modal.dialog('#version_dialog');
		return false;
	});

	// button: apply version
	$('#versionApply').on('click', function(e) {

		e.preventDefault();
		retailnet.loader.hide();
		retailnet.notification.hide();

		var versionTitle = $('#version_name').val();

		retailnet.modal.dialog('#version_dialog');

		if (versionTitle) {

			retailnet.loader.show();

			retailnet.ajax.json($('#btnVersion').attr('href'), {
				id: mastersheet,
				title: versionTitle
			}).done(function(xhr) {

				retailnet.modal.close();

				$('#version_name').val('');

				// reload page
				if (xhr && xhr.reload) {
					window.location.reload();
	        	}

				if (xhr && xhr.message) {
					if (xhr.response) retailnet.notification.success(xhr.message);
					else retailnet.notification.error(xhr.message);
				}

			}).complete(function() {
				retailnet.loader.hide();	
			});

		} else {
			retailnet.notification.error('Please enter Splitting List Version Name.');
		}
		
		return false;
	});

});
</script>
<style type="text/css">

	.pagecontent {
		width: 1300px;
	}
	
	.actions {
		width: 1260px;
	}

	h6 { 
		display: block; 
		margin-bottom: 5px; 
		font-size: .8em;
		font-weight: 500;
	}
	
	h5 { 
		display: block;
		margin: 40px 0 20px; 
		font-size: 1em;
		font-variant: small-caps;
		border-bottom: 1px solid silver;
		color: gray;
	}

	table { 
		margin-top: 20px; 
		border-style: solid; 
		border-color: silver; 
	}
	
	.totoal-collection {
		display: block;
		width: 100%;
		font-size: .75em;
		text-align: right;
		padding-top: 5px;
	}
	
	.totoal-collection b {
		padding-left: 5px;
	}
	
	#version_name {
		width: 350px !important;
		border: 1px solid silver;
		padding: 5px;
		color: black;
		font-size: .9em;
	}
	
	#version_name.silver {
		color: silver;
	}
	
	#splitting_dialog {
		/*width: 400px;*/
		margin: 0 auto;
	}
	
	#splitting_dialog .modalbox-content {
		/*max-height: 800px;
		overflow-y: auto;
		*/
	}
	
	#export_dialog {
		/*width: 1000px;*/
	}
	
	#export_dialog .modalbox-content {
		/*max-height: 500px;
		overflow-y: auto;*/
	}
	
	#export_dialog form input[type=text] {
		width: 80px !important;
	}
	
	.modalbox-content p span {
		display: inline-block;
		padding-left: 10px;
		font-size: 12px;
	}
	
	#splitting_dialog .content-caption {
		display: block;
		font-size: 13px;
		margin: 10px 0;
	}
	
	#export_dialog .content-caption {
		display: block;
		font-size: 16px;
		margin-top: 60px;
		color: #444;
		font-weight: 600;
	}
	
	#export_dialog .content-caption:first-child {
		margin-top: 10px;
	}
	
	.modalbox-content .error {
		display: block;
		color: red;
		font-weight: 300;
	}
	
	.modalbox-content span.error {
		padding-left: 40px;
	}
	
	table.listing th,
	table.listing td {
		font-size: 12px;
	}
	
	table.listing td {
		border-bottom: 1px solid silver;
	}
	
</style>
<form class='request'>
	<input type=hidden name=application value="<?php echo $request->application ?>" />
	<input type=hidden name=controller value="<?php echo $request->controller ?>" />
	<input type=hidden name=archived value="<?php echo $request->archived ?>" />
	<input type=hidden name=action value="<?php echo $request->action ?>" />
	<input type=hidden name=id value="<?php echo $mastersheet->id ?>" />
	<input type=hidden name=mastersheet value="<?php echo $mastersheet->id ?>" />
	<input type=hidden name=selected_items id=selected_items >
</form>
<div id="itemlist"></div>	
<div class='actions list-actions'>
	<?php 
		
		echo ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => $translate->back
		));

		if ($buttons['print']) {
			echo ui::button(array(
				'id' => 'btnprint',
				'icon' => 'print',
				'label' => $translate->print,
				'href' => $buttons['print']
			));
		}
		
		if ($buttons['splitting']) {
			echo ui::button(array(
				'id' => 'splitting',
				'class' => 'modal-trigger',
				'icon' => 'print',
				'label' => $translate->splitting_list,
				'href' => $buttons['splitting']
			));
		}
		
		if ($buttons['version']) {
			echo ui::button(array(
				'id' => 'btnVersion',
				'icon' => 'add',
				'href' => $buttons['version'],
				'label' => $translate->create_version
			));
		}
		
		if ($buttons['consolidate']) {
			echo ui::button(array(
				'id' => 'consolidate',
				'icon' => 'save',
				'label' => $translate->consolidate,
				'href' => $buttons['consolidate']
			));
		}
		
		if ($buttons['unconsolidate']) {
			echo ui::button(array(
				'id' => 'unconsolidate',
				'icon' => 'save',
				'label' => $translate->mastersheet_unconsolidate,
				'href' => $buttons['unconsolidate']
			));
		}
		
		if ($buttons['export']) {
			echo ui::button(array(
				'id' => 'export',
				'class' => 'modal-trigger',
				'icon' => 'save',
				'label' => 'Export to RAMCO'
			));
		}
	?>
</div>
<?php 
	
	
	if ($buttons['export']) {
		echo $export_dialog;
	}
	
	if ($buttons['version']) {

		echo ui::dialogbox(array(
			'id' => 'version_dialog',
			'title' => $translate->create_version,
			'content' => "<input type=text name=version_name id=version_name placeholder='Version Title' />",
			'buttons' => array(
				'versionCancel' => array(
					'icon' => 'cancel',
					'class' => 'cancel',
					'label' => $translate->cancel
				),
				'versionApply' => array(
					'icon' => 'apply',
					'label' => $translate->create
				)
			)
		));
	}
?>
<!-- Splitting Box: Print dialog form splitting list -->
<div class='modalbox-container'>
	<div id="splitting_dialog" class="modalbox modal">
		<div class=modalbox-header >
			<div class='title'>Select Export Options</div>
		</div>
		<div class=modalbox-content-container >
			<div class=modalbox-content >
				<form method=post action="<?php echo $buttons['splitting'] ?>" >
					<div class=content-caption >Planning Types</div>
					<div class="modal-fieldset planningtypes-placeholder"></div>
					<div class=content-caption >Options</div>
					<div class=modal-fieldset >
						<p>
							<input type=checkbox class=option name='proposed_quantity' value='1' /> 
							<span>Proposed Quantity</span>
						</p>
						<p>
							<input type=checkbox class=option name='desired_quantity' value='1' /> 
							<span>Desired Quantity by Customer</span>
						</p>
						<p>
							<input type=checkbox class=option name='desired_quantity_total' value='1' /> 
							<span>Total Cost of Desired Quantity</span>
						</p>
						<p>
							<input type=checkbox class=option name='approved_quantity' value='1' /> 
							<span>Approved Quantity</span>
						</p>
						<p>
							<input type=checkbox class=option name='approved_quantity_total' value='1' /> 
							<span>Total Cost of Approved Quantity</span>
						</p>
						<p>
							<input type=checkbox class=option name='ordered_quantity' value='1' /> 
							<span>Pre Ordered Quantity</span>
						</p>
						<p>
							<input type=checkbox class=option name='ordered_quantity_total' value='1' /> 
							<span>Total Cost of Pre Ordered Quantity</span>
						</p>
					</div>
				</form>
			</div>
		</div>
		<div class=modalbox-footer >
			<div class=modalbox-actions >
				<span class='button cancel'>
					<span class="icon cancel"></span>
					<span class="label"><?php echo $translate->cancel ?></span>
				</span>
				<span class='button submit_splitting'>
					<span class="icon print"></span>
					<span class="label"><?php echo $translate->print ?></span>
				</span>
			</div>
		</div>
	</div>
</div>

<!-- Modal Box: Export to exchange server -->
<div class='modalbox-container'>
	<div id="export_dialog" class="modalbox modal">
		<div class=modalbox-header >
			<div class='title'>Export to RAMCO</div>
			<div class='subtitle'><?php echo $mastersheet->header(); ?></div>
		</div>
		<div class=modalbox-content-container >
			<div class=modalbox-content>
				<form method=post class="default" action="<?php echo $buttons['export'] ?>" id="modalItemList" ></form>
			</div>
		</div>
		<div class=modalbox-footer >
			<div class=modalbox-actions >
				<span class='button cancel'>
					<span class="icon cancel"></span>
					<span class="label"><?php echo $translate->cancel ?></span>
				</span>
				<span class='button submit_export'>
					<span class="icon reload"></span>
					<span class="label"><?php echo $translate->export ?></span>
				</span>
			</div>
		</div>
	</div>
</div>