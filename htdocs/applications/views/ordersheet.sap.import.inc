<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();
	
	// model
	$model = new Model($request->application);
	
	// current order sheet	
	$ordersheet = new Ordersheet($request->application);
	$ordersheet->read($request->field('ordersheet'));
	
	// company
	$company = new Company();
	$company->read($ordersheet->address_id);
	
	// is archived
	$isDistributed = ($ordersheet->state()->isDistributed() && $ordersheet->isDistributed()) ? true : false;

	// ordersheet version
	$version = $request->field('version');
	
/*******************************************************************************************************************************************************************************************
	Ordersheet States
********************************************************************************************************************************************************************************************/
	
	// group statments
	$states_preparation = Ordersheet_State::loader('preparation');
	$states_complete = Ordersheet_State::loader('complete');
	$states_revision = Ordersheet_State::loader('revision');
	$states_confirmation = Ordersheet_State::loader('confirmation');
	$states_distribution = Ordersheet_State::loader('distribution');
	
	// is in group state
	$statment_on_preparation = $ordersheet->state()->onPreparation();
	$statment_on_completing = $ordersheet->state()->onCompleting();
	$statment_on_approving = $ordersheet->state()->onApproving();
	$statment_on_revision = $ordersheet->state()->onCompleting();
	$statment_on_confirmation = $ordersheet->state()->onConfirmation();
	$statment_on_distribution = $ordersheet->state()->onDistribution();
	
	// order sheet editabled states
	if ($ordersheet->state()->owner) {
		$editabled_states = array_merge($states_complete, $states_revision, $states_confirmation, $states_distribution);
	}
	elseif ($ordersheet->state()->canAdministrate()) {
		$editabled_states = array_merge($states_preparation, $states_confirmation, $states_distribution);
	}

	
/*******************************************************************************************************************************************************************************************
	Spreadsheet Global Properties
	Set editabled and visibility vars
********************************************************************************************************************************************************************************************/
	

	// readonly mod
	$mod_readonly = true;
	
	// planning mod
	$mod_planning = false;
	
	// distribution mod
	$mod_distribution = true;
	
	// spreadsheet attribute readonly
	$spreadsheet_attribute_readonly =  'true';
	
	// planning columns
	$show_planning_columns = true;
	
	// distribution columns
	$show_distribution_columns = ($mod_distribution) ? true : false;
	
	// extended rows (warehouses)
	$show_extended_rows = (!$isDistributed) ? true : false;
	
	// calculation area (first in calculation set)
	$show_calculation_area = true;
	
	// spreadsheet request fields
	$request->field('readonly', $spreadsheet_attribute_readonly);
	$request->field('check', 0);
	$request->field('fixedNotSelectable', 1);
	$request->field('mode_planning', 0);
	$request->field('mode_distribution', 1);
	
	
/*******************************************************************************************************************************************************************************************
	Request Filters
********************************************************************************************************************************************************************************************/
	
	$filters = array();
	$filer_labels = array();
	
	// filter: pos
	$filters['default'] = "posaddress_client_id = ".$ordersheet->address_id;
	
	// filter: distribution channels
	if ($_REQUEST['distribution_channels']) {
		$filters['distribution_channels'] = 'posaddress_distribution_channel = '.$_REQUEST['distribution_channels'];
		$filter_active = 'active';
	}
	
	// filter: turnover classes
	if ($_REQUEST['turnoverclass_watches']) {
		$filters['turnoverclass_watches'] = 'posaddress_turnoverclass_watches = '.$_REQUEST['turnoverclass_watches'];
		$filter_active = 'active';
	}
	
	// filter: sales represenatives
	if ($_REQUEST['sales_representative']) {
		$filters['sales_representative'] = 'posaddress_sales_representative = '.$_REQUEST['sales_representative'];
		$filter_active = 'active';
	}
	
	// filter: decoration persons
	if ($_REQUEST['decoration_person']) {
		$filters['decoration_person'] = 'posaddress_decoration_person = '.$_REQUEST['decoration_person'];
		$filter_active = 'active';
	}
	
	// provinces
	if ($_REQUEST['provinces']) {
		$filters['provinces'] = 'province_id = '.$_REQUEST['provinces'];
		$filter_active = 'active';
	}
	
	// for archived ordersheets show only POS locations which has distributed quantities 
	if ($isDistributed) {
		
		$result = $model->query("
			SELECT DISTINCT 
				mps_ordersheet_item_delivered_posaddress_id AS pos
			FROM mps_ordersheet_item_delivered		
		")
		->bind(Ordersheet_Item_Delivered::DB_BIND_ORDERSHEET_ITEMS)
		->filter('ordersheet', 'mps_ordersheet_item_ordersheet_id = '.$ordersheet->id)
		->filter('null', 'mps_ordersheet_item_delivered_posaddress_id > 0')
		->fetchAll();
		
		if ($result) {
			
			$array = array();
			
			foreach ($result as $row) {
				$array[] = $row['pos'];
			}

			$filters['active'] = 'posaddress_id IN ('.join(',', $array).')';
		}
	}
	// otherwise show all active pos locations
	else {
		$filters['active'] = "(
			posaddress_store_closingdate = '0000-00-00'
			OR posaddress_store_closingdate IS NULL
			OR posaddress_store_closingdate = ''
		)";
	}
	
	
/*******************************************************************************************************************************************************************************************
	Dataloader
	Distribution Channels
********************************************************************************************************************************************************************************************/

	$formFilter = array();
	
	// load distribution channels
	$result = $model->query("
		SELECT DISTINCT
			mps_distchannel_id,
			CONCAT(mps_distchannel_name, ' - ', mps_distchannel_code) AS distribution_channel
		FROM db_retailnet.posaddresses
	")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->bind(Pos::DB_BIND_DISTRIBUTION_CHANNELS)
	->filter($filters)
	->exclude('distribution_channels')
	->order('mps_distchannel_name, mps_distchannel_code')
	->fetchAll();
	
	if ($result) {
		
		$result = _array::extract($result);
		
		$formFilter[] = ui::dropdown($result, array(
			'id' => 'distribution_channels',
			'name' => 'distribution_channels',
			'value' => $_REQUEST['distribution_channels'],
			'caption' => $translate->all_distribution_channels
		));
		
		// show active filter labels
		if ($_REQUEST['distribution_channels']) {
			$filer_labels[] = $result[$_REQUEST['distribution_channels']];	
		}
	}
	

/*******************************************************************************************************************************************************************************************
	Dataloader
	Turnover Class Watches
********************************************************************************************************************************************************************************************/
	
	$result = $model->query("
		SELECT DISTINCT
			mps_turnoverclass_id, mps_turnoverclass_name
		FROM db_retailnet.posaddresses
	")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->bind(Pos::DB_BIND_TURNOVER_CLASS_WATCHES)
	->filter('company', "posaddress_franchisee_id = ".$ordersheet->address_id)
	->order('mps_turnoverclass_name')
	->fetchAll();
	
	if ($result) {
		
		$result = _array::extract($result);
		
		$formFilter[] = ui::dropdown($result, array(
			'name' => 'turnoverclass_watches',
			'id' => 'turnoverclass_watches',
			'value' => $_REQUEST['turnoverclass_watches'],
			'caption' => $translate->all_turnover_class_watches
		));
		
		// show active filter labels
		if ($_REQUEST['turnoverclass_watches']) {
			$filer_labels[] = $result[$_REQUEST['turnoverclass_watches']];
		}
	}

	
/*******************************************************************************************************************************************************************************************
	Dataloader
	Sales Representative
********************************************************************************************************************************************************************************************/
	
	$model->query("
		SELECT DISTINCT
			mps_staff_id,
			CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS name
		FROM db_retailnet.posaddresses
	")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->bind(Pos::DB_BIND_MPS_STAFF_SALES_REPRESENTATIVES)
	->filter($filters)
	->filter('staff_type', 'mps_staff_staff_type_id = 1')
	->exclude('sales_representative')
	->order('mps_staff_name, mps_staff_firstname');
	
	if ($ordersheet->state()->owner) {
		$model->filter('limited', 'mps_staff_address_id='.$user->address);
	}
	
	$result = $model->fetchAll();

	if ($result) {
		
		$result = _array::extract($result);
		
		$formFilter[] = ui::dropdown($result, array(
			'name' => 'sales_representative',
			'id' => 'sales_representative',
			'value' => $_REQUEST['sales_representative'],
			'caption' => $translate->all_sales_representatives
		));
		
		// show active filter labels
		if ($_REQUEST['sales_representative']) {
			$filer_labels[] = $result[$_REQUEST['sales_representative']];
		}
	}
	

/*******************************************************************************************************************************************************************************************
	Dataloader
	Deecoration Persons
********************************************************************************************************************************************************************************************/
	
	$model->query("
		SELECT DISTINCT
			mps_staff_id,
			CONCAT(mps_staff_name, ', ', mps_staff_firstname) AS name
		FROM db_retailnet.posaddresses
	")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->bind(Pos::DB_BIND_MPS_STAFF_DECORATOR_PERSONS)
	->filter($filters)
	->filter('staff_type', 'mps_staff_staff_type_id = 2')
	->exclude('decoration_person')
	->order('mps_staff_name, mps_staff_firstname');
	
	if ($ordersheet->state()->owner) {
		$model->filter('limited', 'mps_staff_address_id='.$user->address);
	}
	
	$result = $model->fetchAll();

	if ($result) {
		
		$result = _array::extract($result);
		
		$formFilter[] = ui::dropdown($result, array(
			'name' => 'decoration_person',
			'id' => 'decoration_person',
			'value' => $_REQUEST['decoration_person'],
			'caption' => $translate->all_decoration_persons
		));
		
		// show active filter labels
		if ($_REQUEST['decoration_person']) {
			$filer_labels[] = $result[$_REQUEST['decoration_person']];
		}
	}
	
	
/*******************************************************************************************************************************************************************************************
	Dataloader
	Provinces
********************************************************************************************************************************************************************************************/
	
	$result = $model->query("
		SELECT DISTINCT
			province_id, province_canton
		FROM db_retailnet.posaddresses")
	->bind(Pos::DB_BIND_PLACES)
	->bind(Place::DB_BIND_PROVINCES)
	->filter($filters)
	->exclude('provinces')
	->order('province_canton')
	->fetchAll();
	
	if ($result) {
		
		$result = _array::extract($result);
		
		$formFilter[] = ui::dropdown($result, array(
			'name' => 'provinces',
			'id' => 'provinces',
			'value' => $_REQUEST['provinces'],
			'caption' => $translate->all_provinces
		));
		
		// show active filter labels
		if ($_REQUEST['provinces']) {
			$filer_labels[] = $result[$_REQUEST['provinces']];
		}
	}
	

?>
<div class="actions">	
	<?php 

		echo ui::button(array(
			'id' => 'close',
			'icon' => 'icon151',
			'caption' => 'Close'
		));
	
		if ($buttons) {
			foreach ($buttons as $name => $button) {
				$button['id'] = $name;
				if ($name=='pop_filter') $button['class'] = $filter_active;
				echo ui::button($button);
			}
		}
	
	?>
</div>
<div id='spreadsheet' class='<?php echo $class_planning; ?>'></div>
<div class="tooltips-container">
	<div class="image-loader" ><img src="/public/images/loader-quer.gif" /></div>
</div>
<?php
	
	echo $request->form();

	if ($formFilter) {
		$action = ($version) ? $request->field('url')."/$version" : $request->field('url');
		echo "<form id='filters' method=post action='$action' class=default  ><ul>";
		foreach ($formFilter as $value) echo "<li>$value</li>";
		echo "</ul></form>";
	}
	
?>
