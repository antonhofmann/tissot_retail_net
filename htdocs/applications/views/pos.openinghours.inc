<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>	
<script type='text/javascript' src="/public/js/pos.openinghours.js"></script>
<style type="text/css">

	form.default {
		width: 800px;
	}

	.oh-wrapper {
		width: 800px;
		margin: 20px 0;
		float: left;
	}

	#calendar {
		width: 800px;
		height: 700px;
		overflow: hidden;
	}
	.wc-time-header-cell,
	.wc-day-column-header {
		font-size: 12px;
	}
	
	.wc-scrollbar-shim {
		display: none;
	}
	
	.util {
		display: none;
	}
	
	#calendar table {
		border-style: solid;
		border-color: silver;
		border-width: 1px 1px 0;
	}
	
	#event_edit_container {
		display: none;
	}
	
	#event_edit_container label {
		display: block;
		margin-top: 1em;
		margin-bottom: 0.5em;
		font-size: 12px;
	}
	
	#event_edit_container ul {
		padding: 0.3em;
		list-style: none;
	}
	
	#event_edit_container select, 
	#event_edit_container input[type='text'] {
		width: 250px;
		padding: 3px;
		font-size: 12px;
		border: 1px solid silver;
	}
	
	#event_edit_container input[type='text'] {
		width: 245px;
		font-size: 12px;
	}
	
	.ui-dialog-title,
	.ui-button-text {
		font-size: 14px;	
	}
	
	.table-toolbox {
		text-align: right;
	}
	
	#popover {
		min-width: 200px;
		text-align: left;
	}
	
	#popover .row {
		display: block;
		margin-bottom: 20px;
	}
	
	#closing_days {
		display: block;
		margin: 20px 0;
	}
	
	#closing_days p {
		padding: 20px;
		font-size: 12px;
	}
	
	#days_closed {
		width: 520px;
		height: 60px;
	}
	
	.daybox {
		display: inline-block;
		*display: inline;
		zoom: 1;
		width: 20px;
		font-size: 12px;
		line-height: 18px;
		padding: 5px;
		margin: 0;
		background-color: #f2f2f2;
	}
	
	form#form_closing_days {
		width: 800px !important;
	}
	
	.popover .row:first-child {
		margin-top: 20px;
	}

</style>
<div class="oh-wrapper">
	<form id="toolbox" class="default">
		<input type="hidden" name="loaderTrigger" id="loaderTrigger" />
		<input type="hidden" name="application" id="application" value="<?php echo $request->application ?>" />
		<input type="hidden" name="country" id="country" value="<?php echo $pos_country ?>" />
		<input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
		<input type="hidden" name="readonly" id="readonly" value="<?php echo $readonly ?>" />
		<input type="hidden" name="increment" id="increment" value="<?php echo $increment ?>" />
		<input type="hidden" name="timeformat" id="timeformat" value="<?php echo $timeformat ?>" />
		<?php if (!$readonly) : ?>
		<div class='table-toolbox'>
			<span id="delete" class="button dialog">
				<span class="icon delete"></span>
				<span class=label >Delete All</span>
			</span>
			<span id="copyfrom" class="button" data="#popover">
				<span class="icon direction-down"></span>
				<span class=label >Copy From</span>
			</span>
			<div id="popover" >
				<div class="row">
					<select name="place" id="place" class="dropdown-loader"></select>
				</div>
				<div class="row">
					<select name="ownertype" id="ownertype" class="dropdown-loader"></select>
				</div>
				<div class="row">
					<select name="pos" id="pos" class="dropdown-loader"></select>
				</div>
			</div>
		</div>
		<?php  endif; ?>
	</form>
	<div id='calendar'></div>
	<div id="closing_days">
		<form id="form_closing_days" class="default" action="/applications/helpers/pos.closed.days.php" method="post">
			<div class="-legend"><b>Days Closed in General</b></div>
			<div class="-fieldset closeddays">
				<div class="-row description -odd">
					<p>Please indicate the days on which the POS is closed in general like e.g. Sundays, New Year, 24th of December to 3rd of January or similar information.</p>
				</div>
				<div class="-row days_closed -even">
					<label>Days Closed: </label> 
					<div class="-element -textarea -odd">
						<?php
							if ($buttons['save']) {
							 echo "<textarea name=\"days_closed\" id=\"days_closed\" tag=\"required\">$closed_days</textarea>";
							} else {
								echo $closed_days;
							}
						?>
					</div>   
				</div>
			</div>
			<div class="actions">
				<a class="button" id="back" href="<?php echo $buttons['back'] ?>"><span class='icon back'></span><span class="label button-back">Back</span></a>
				<?php
					if ($buttons['save']) {
						echo '<a class="button" id="save"><span class="icon save"></span><span class="label button-save">Save</span></a>';
					}
				?>
			</div>
		</form>
	</div>
</div>
<div id="event_edit_container">
	<form id="timeform">
		<input type="hidden" />
		<ul>
			<li class="util">
				<span>Date: </span><span class="date_holder"></span> 
			</li>
			<li  class="util">
				<label for="title">Title: </label><input type="text" name="title" />
			</li>
			<li>
				<label for="start">Start Time: </label><select name="start"><option value="">Select Start Time</option></select>
			</li>
			<li>
				<label for="end">End Time: </label><select name="end"><option value="">Select End Time</option></select>
			</li>
			<li class="dayboxes">
				<label for="start">Apply to: </label>
				<span class=daybox >Mo <input type=checkbox name=day[1] value=1 ></span>
				<span class=daybox >Tu <input type=checkbox name=day[2] value=2 ></span>
				<span class=daybox >We <input type=checkbox name=day[3] value=3 ></span>
				<span class=daybox >Th <input type=checkbox name=day[4] value=4 ></span>
				<span class=daybox >Fr <input type=checkbox name=day[5] value=5 ></span>
				<span class=daybox >Sa <input type=checkbox name=day[6] value=6 ></span>
				<span class=daybox >Su <input type=checkbox name=day[7] value=7 ></span>
			</li>
		</ul>
	</form>
</div>	

<?php 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => "Are you sure to delete all Opening Hours?",
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>
