<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();
	
	$form = new Form(array(
		'id' => 'applicationForm',
		'action' => '/applications/helpers/item.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);	
	
	$form->item_id(
		Form::TYPE_HIDDEN
	);

	$form->item_type(
		Form::TYPE_HIDDEN
	);
	
	$form->item_code(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);	

	$form->item_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);

	$form->item_category(
		Form::TYPE_SELECT
	);

	$form->item_description(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);

	$form->fieldset('Description', array(
		'item_code',
		'item_name',
		'item_category',
		'item_description'
	));	
	
	$form->item_cost_group(
		Form::TYPE_SELECT
	);		

	$form->fieldset('Cost Monitoring', array(
		'item_cost_group'
	));

	$form->item_projecttype_productline(
		Form::TYPE_AJAX,
		FORM::PARAM_SHOW_VALUE
	);

	$form->fieldset('Available in Project Types and Product Lines', array(
		'item_projecttype_productline'
	));

	$form->item_visible(
		Form::TYPE_CHECKBOX
	);

	$form->item_visible_in_projects(
		Form::TYPE_CHECKBOX
	);

	$form->item_price_adjustable(
		Form::TYPE_CHECKBOX
	);

	$form->item_active(
		Form::TYPE_CHECKBOX
	);

	$form->fieldset('Others', array(
		'item_visible',
		'item_visible_in_projects',
		'item_price_adjustable',
		'item_active'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}

	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
		
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>

<style type="text/css">

	#applicationForm { 
		width: 700px; 
	}
	
	#applicationForm textarea {
		width: 440px;
		height: 60px;
	}

	.item_slave_item .desc {
		display: block;
		font-size: 12px;
		margin: 5px 10px 0 194px;
		color: gray;
	}

	#item_packed_size,
	#item_packed_weight,
	#item_materials,
	#item_electrical_specifications,
	#item_lights_used,
	#item_regulatory_approvals,
	#item_install_requirements {
		height: 30px !important;
	}

	.item_projecttype_productline label {
		display: none !important;
	}

	#item_projecttype_productline {
		display: block;
		padding: 10px 20px;
	}

	#item_projecttype_productline table {
		width: 650px;
	}

	#item_projecttype_productline table th {
		text-align: center;
		background: transparent !important;
		border-bottom: 1px solid silver;
	}

	#item_projecttype_productline table tr th:first-child {
		text-align: left !important;
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>