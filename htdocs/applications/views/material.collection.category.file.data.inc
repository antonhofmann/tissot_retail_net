<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$data['file'] = '
		<span id="showfile" class="button">
			<span class="icon download"></span>
			<span class="label">'.$translate->download_file.'</span>	
		</span>
		<span id="upload" class="button">
			<span class="icon upload"></span>
			<span class="label">'.$translate->upload_file.'</span>	
		</span>
		<input type=hidden name="mps_material_collection_category_file_path" id="mps_material_collection_category_file_path" value="'.$data['mps_material_collection_category_file_path'].'" class="file_path" />
		<input type="hidden" name="has_upload" id="has_upload" />
	';
	
	$form = new Form(array(
		'id' => 'collection_category_file',
		'action' => '/applications/helpers/material.collection.category.file.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_material_collection_category_file_id(
		Form::TYPE_HIDDEN,
		'class=file_id'
	);
	
	$form->mps_material_collection_category_file_collection_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_material_collection_category_file_title(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		'class=file_title'
	);
	
	$form->file(
		Form::TYPE_AJAX,
		FORM::PARAM_SHOW_VALUE
	);
	
	$form->mps_material_collection_category_file_description(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);
	
	$form->mps_material_collection_category_file_visible(
		Form::TYPE_CHECKBOX
	);
	
	$form->fieldset('file_type', array( 
		'mps_material_collection_category_file_title',
		'file',
		'mps_material_collection_category_file_description',
		'mps_material_collection_category_file_visible'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<style type="text/css">
	
	#collection_category_file { 
		width: 700px; 
	}
	
	form .equipment input { 
		width: 120px; 
	}
	
	#mps_material_collection_category_file_description { 
		width: 440px; 
		height: 80px; 
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete_file,
		'content' => $translate->dialog_delete_file,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>