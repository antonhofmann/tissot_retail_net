<?php

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$form = new Form(array(
		'id' => 'projectsform',
		'action' => '/applications/helpers/red.project.save.php',
		'method' => 'post'
	));

	$form->redirect(
		Form::TYPE_HIDDEN
	);

	$form->red_project_id(
		Form::TYPE_HIDDEN
	);

	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);

	$form->red_project_title(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->red_project_projectnumber(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->red_project_projectstate_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->red_project_projecttype_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->red_project_leader_user_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->red_project_assistant_user_id(
		Form::TYPE_SELECT
	);

	$form->red_project_startdate(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		"class=datepicker"
	);
	$form->red_project_foreseen_enddate(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		"class=datepicker"
	);
	$form->red_project_enddate(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		"class=datepicker"
	);

	// fieldset: Project Details
	$form->fieldset('basic_data', array(
		'red_project_title',
		'red_project_projectnumber',
		'red_project_startdate',
		'red_project_foreseen_enddate',
		'red_project_projectstate_id',
		'red_project_leader_user_id',
		'red_project_assistant_user_id',
		'red_project_projecttype_id',
		'red_project_startdate',
		'red_project_foreseen_enddate',
		'red_project_enddate',
		'red_project_context',
		'red_project_description',
		'red_project_restrictions',
		'red_project_timeline',
		'red_project_external_partners',
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}

	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}

	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['print']) {
		$form->button(ui::button(array(
			'id' => 'print_booklet',
			'icon' => 'print',
			'href' => $buttons['print'],
			'label' => "Print Project Booklet"
		)));
	}
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}

	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
	
?>

<link rel="stylesheet" type="text/css" href="/public/scripts/jquery/jquery.ui.css" />
<link rel="stylesheet" type="text/css" href="/public/css/jquery.ui.css" />
<script type="text/javascript" src="/public/scripts/jquery/jquery.ui.js"  ></script>

<link rel="stylesheet" type="text/css" href="/public/scripts/loader/loader.css" />
<script type="text/javascript" src="/public/scripts/loader/loader.js"  ></script>
<link rel="stylesheet" type="text/css" href="/public/css/jquery.ui.css" />
<link rel="stylesheet" href="/public/scripts/validationEngine/css/validationEngine.jquery.css" type="text/css"/>
<script src="/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="/public/scripts/validationEngine/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="/public/scripts/qtip/qtip.js"  ></script>
<script type="text/javascript" src="/public/scripts/chain/chained.select.js"  ></script>
<script type="text/javascript">

	function isInt(n) {
		return n % 1 === 0;
	}

	$(document).ready(function() {

		var form = $('#projectsform');

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			onAjaxFormComplete: function(status, form, json, options) {

				retailnet.loader.hide();
				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

				if (json) {

					if (json.response && json.redirect) {
						window.location=json.redirect;
					}

					if (json.message) {
						$.jGrowl(json.message,{ 
							sticky: (json.response) ? false : true, 
							theme: (json.response) ? 'message' : 'error'
						});
					}
				}
			}
		});

		$("#save").click(function(event) {

			event.preventDefault();

			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

			var errors=0;
			var elements = $('input,textarea,select');
			var validator = form.validationEngine('validate');

			$.each(elements, function(index, elem) {
				if ($(elem).hasClass('error')) {
					errors++;
				}
			});

			if (errors || !validator) {
				$.jGrowl('Please check red marked fields.',{ 
					sticky: true, 
					theme:'error'
				});
			}
			else {
				retailnet.loader.show();
				form.submit();
			}
		});

		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});

		$('.datepicker').datepicker({
			dateFormat: 'dd.mm.yy',
			showOtherMonths : true,
			firstDay: 1
		});

		$('.dialog').click(function(event) {

			event.preventDefault();

			var button = $(this);
			
			$('#apply, a.apply').attr('href', button.attr('href'));
			
			retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
		});
		
		$('#cancel').bind('click', function() {
			retailnet.modal.close();
			return false;
		});

	});
</script>
<style type="text/css">

	#projectsform, .toolbox {
		width: 700px;
	}

	input.generated {
		width: 150px !important;
		margin-left: 20px;
	}
	
	#red_project_startdate {
		width: 100px;
	}
	
	#red_project_foreseen_enddate {
		width: 100px;
	}
	
	#red_project_enddate {
		width: 100px;
	}

</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>