<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#warehouses').tableLoader({
		url: '/applications/helpers/company.warehouse.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			var dropdown = $("select",self).dropdown();

		}
	});

});
</script>
<style type="text/css">

	#warehouses { 
		width: 600px;
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="warehouses" ></div>