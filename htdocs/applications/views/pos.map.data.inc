<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	if (!$disabled['posaddress_google_lat']) {
		$map_guide = "<div class='map_guide'>".$translate->map_guide."</div>";
	}
	
	$data['map_content'] = "$map_guide<div id='gmap'></div>";
	
	$form = new Form(array(
		'id' => 'posmap',
		'action' => '/applications/helpers/pos.map.save.php',
		'method' => 'post'
	));
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->posaddress_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mapzoom(
		Form::TYPE_HIDDEN
	);
	
	$form->draggable(
		Form::TYPE_HIDDEN
	);
	
	$form->posaddress_google_lat(
		Form::TYPE_TEXT,
		Form::PARAM_REQUIRED
	);
	
	$form->posaddress_google_long(
		Form::TYPE_TEXT,
		Form::PARAM_REQUIRED
	);
	
	$form->map_content(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE
	);
	
	$form->fieldset('geo_data', array(
		'posaddress_google_lat', 
		'posaddress_google_long'
	));
	
	$form->dataloader($data);
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form = $form->render();
?>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyD2wMUs74upe6ZvEOflrrMDRCoPYABu1Qs"></script>
<script type="text/javascript" src="/public/js/pos.map.js?t=<?php echo $_SERVER['REQUEST_TIME'] ?>"></script>
<style type="text/css">

	#posmap { 
		width: 700px; 
	}
	
	#gmap {
		display: block; 
		width: 700px; 
		height: 500px; 
		border: 1px solid gray; 
		margin: 10px 0 20px 0;
	}
	
	.map_content {
		padding: 0 !important;
	}
	
</style>
<?php 
	
	echo $form; 


	echo ui::dialogbox(array(
		'id' => 'delete_posfile',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>