<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#pos_materials').tableLoader({
		url: '/applications/helpers/pos.material.list.php',
		data: $('.request').serializeArray()
	});
	
});
</script>
<style type="text/css">

	#pos_materials { 
		width: 1200px; 
	}
	
</style>
<?php echo $request->form($requestFields); ?>
<div id="pos_materials"></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>