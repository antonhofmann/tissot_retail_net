<script type="text/javascript">
$(document).ready(function() {

	$('#productlines').tableLoader({
		url: '/applications/helpers/product.line.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			$('select',self).dropdown();

			$("tbody", self).sortable({
				placeholder: "ui-state-highlight",         
		        scroll: true,
		        handle: ".row-order",
		        items: "> tr",
		        update: function( event, ui ) {
		        	
		        	$("tr:odd", self).removeClass('-odd').removeClass('-even').addClass('-odd');
		        	$("tr:even", self).removeClass('-odd').removeClass('-even').addClass('-even');

		        	var orders = [];
					
					$('.row-order', self).each( function () {
						orders.push($(this).data('id'));
					});

					if (orders.length) {
						retailnet.ajax.json('/applications/helpers/product.line.ajax.php', {
							section: 'order',
							orders: orders.join(',')
						}).done(function(xhr) {
							if (xhr && xhr.message) {
								if (xhr.success==1) retailnet.notification.success(xhr.message);
								else retailnet.notification.error(xhr.message);
							}
						});
					}
		        }
			});
		}
	});
	
});
</script>
<style type="text/css">

	#productlines { 
		width: 600px; 
	}

	td.sorter {
		border-right: 0 !important;
		cursor: pointer;
	}

	td.sorter:hover {
		color: blue;
	}

	.ui-state-highlight { background: #fffeee !important; }
	
</style>
<div id="productlines" ></div>
<?php echo Request::instance()->form(); ?> 