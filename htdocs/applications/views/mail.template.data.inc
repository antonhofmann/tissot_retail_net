<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'mail_template',
		'action' => '/applications/helpers/mail.template.save.php',
		'method' => 'post'
	));

	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->mail_template_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mail_template_application_id(
		Form::TYPE_SELECT
	);
	
	$form->mail_template_shortcut(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mail_template_code(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mail_template_purpose(
		Form::TYPE_TEXTAREA,
		Validata::PARAM_REQUIRED,
		Form::PARAM_LABEL
	);
	
	$form->mail_template_subject(
		Form::TYPE_TEXTAREA,
		Validata::PARAM_REQUIRED,
		Form::PARAM_LABEL
	);
	
	$form->mail_template_text(
		Form::TYPE_TEXTAREA,
		Validata::PARAM_REQUIRED,
		Form::PARAM_LABEL
	);
	
	$form->mail_template_footer(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);
	
	$form->fieldset('mail', array(
		'mail_template_application_id',
		'mail_template_shortcut',
		'mail_template_code',
		'mail_template_purpose',
		'mail_template_subject',
		'mail_template_text',
		'mail_template_footer'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#mail_template");
		var id = $("#mail_template_id");

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			beforeSend: function () {
				retailnet.loader.show();
			},
			onAjaxFormComplete: function(status, form, json, options) {

				retailnet.loader.hide();
				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

				if (json) {

					if (json.response && json.redirect) {
						window.location=json.redirect;
					}

					if (json.message) {
						$.jGrowl(json.message, { 
							sticky: (json.response) ? false : true,
							theme: (json.response) ? 'message' : 'error'
						});
					}
				}
			}
		});
		
		$("#save").click(function(event) { 

			event.preventDefault();
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

			form.submit();
			
			return false;
		});

		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});

		$('.dialog').click(function(event) {

			event.preventDefault();

			var button = $(this);
			
			$('#apply, a.apply').attr('href', button.attr('href'));
			
			retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
		});

		$('#cancel').bind('click', function(event) {
			event.preventDefault();
			retailnet.modal.close();
			return false;
		});	

	});
</script>

<style type="text/css">

	#mail_template { 
		width: 700px; 
	}
	
	form .mail_template_code input {
		width: 440px;
	}
	
	form .mail_template_purpose textarea {
		width: 440px;
		height: 40px;
	}
	
	form .mail_template_subject textarea {
		width: 440px;
		height: 40px;
	}
	
	form .mail_template_text textarea {
		width: 440px;
		height: 200px;
	}
	
	form .mail_template_footer textarea {
		width: 440px;
		height: 100px;
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>