<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'import',
		'action' => $file,
		'method' => 'post'
	));
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	

	$form->update_existing_order_sheets(
		Form::TYPE_CHECKBOX
	);

	if (in_array($data['application'], array('lps'))) {
		$translate->update_existing_order_sheets = "update existing launch plans";
	}
	
	$form->fieldset('update_from_simac', array(
		'update_existing_order_sheets'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->update
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#import");
		
		// spiner instance	
		retailnet.loader.init();
		
		// tooltip instance	
		retailnet.tooltip.init()

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			onAjaxFormComplete: function(status, form, json, options) {

				json = json || {};

				retailnet.loader.hide();
				retailnet.notification.hide();

				if (json) {

					if (json.response && json.redirect) { 
						window.location=json.redirect;
					}

					if (json.message) {
						if (json.response) retailnet.notification.success(json.message);
						else retailnet.notification.error(json.message);
					}

					if (json.errors) {
						$('.errors .-content').html(json.errors);
						$('.errors').toggle();
					}

					if (json.success) {
						$('.success .-content').html(json.success);
						$('.success').toggle();
					}

					if (json.ordersheets) {
						$('.ordersheets .-content').html(json.ordersheets);
						$('.ordersheets').toggle();
					}
				}
			}
		});


		// submit from
		$("#save").click(function(event) { 
			
			event.preventDefault();
			retailnet.notification.hide();

			$('.-box').hide();
			$('.-content').empty();
			
			if (form.validationEngine('validate')) {
				retailnet.loader.show();
				form.submit();
			} else {
				retailnet.notification.error('Please check red marked fields.');
			}
		});
		

		$(".close").click(function(event) { 
			var parent = $(this).closest('.-box');
			parent.fadeOut("slow");
		});
	});
</script>

<style type="text/css">

	.pagebody {
		min-width: 1200px !important;
	}

	.pagecontent {
		width: 700px; 
		float: left;
		margin-left: 40px;
	}
	
	.-box {
		width: 680px; 
		display: none;
		margin-bottom: 20px;
		padding: 10px;
	}
	
	.-box-header {
		font-size: 14px;
	}
	
	.response {
		padding: 10px;
	}
	
	.icon.close {
		float: right;
		margin: 0 !important;
	}
	
</style>
<?php 
	echo $form; 
?>
<div class="-box errors">
	<div class="-box-header">Errors occured <span class="icon close"></span></div>
	<div class="-content"></div>
</div>
<div class="-box success">
	<div class="-box-header">Success <span class="icon close"></span></div>
	<div class="-content"></div>
</div>
<div class="-box ordersheets">
	<div class="-box-header">Order Sheet Update <span class="icon close"></span></div>
	<div class="-content"></div>
</div>