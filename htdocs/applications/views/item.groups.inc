<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#list').tableLoader({
		url: '/applications/helpers/item.groups.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">

	#list { 
		width: 900px; 
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="list"></div>