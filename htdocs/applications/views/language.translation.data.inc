<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();
	
	$form = new Form(array(
		'id' => 'translation',
		'action' => '/applications/helpers/language.translation.save.php',
		'method' => 'post'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->translation_keyword(
		Form::TYPE_HIDDEN
	);
	
	$form->translation_language(
		Form::TYPE_HIDDEN
	);
	
	$form->keyword(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	$fieldset[] = 'keyword';
	
	$form->content(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$fieldset[] = 'content';
	
	$form->translation_language(
		Form::TYPE_HIDDEN
	);
	
	if($fields) {
		
		foreach ($fields as $field => $caption) {
			
			$form->$field(
				Form::TYPE_TEXTAREA,
				Form::PARAM_LABEL
			);
			
			$form->caption($field, $caption);
			$fieldset[] = $field;
		}
	}
	
	$form->caption('translation', $keyword);
	$form->fieldset('translation', $fieldset);
	
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#translation");
		var id = $("#translation_id");

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			beforeSend: function () {
				retailnet.loader.show();
			},
			onAjaxFormComplete: function(status, form, json, options) {

				retailnet.loader.hide();
				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

				if (json) {

					if (json.response && json.redirect) {
						window.location=json.redirect;
					}

					if (json.message) {
						$.jGrowl(json.message,{ 
							sticky: (json.response) ? false : true, 
							theme: (json.response) ? 'message' : 'error'
						});
					}
				}
			}
		});
		
		$("#save").click(function(event) { 
			event.preventDefault();
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
			form.submit();
			return false;
		});

		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});

		$('.dialog').click(function(event) {

			event.preventDefault();

			var button = $(this);
			
			$('#apply, a.apply').attr('href', button.attr('href'));
			
			retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
		});

		$('#cancel').bind('click', function(event) {
			event.preventDefault();
			retailnet.modal.close();
			return false;
		});	


		$('#keyword').change(function() {
			
			var keyword = $(this).val();

			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
 			
			if (keyword) {
				
				var request = $.getJSON('/applications/helpers/language.ajax.php', { 
					section: 'check_keyword',
					keyword: keyword
				});

				request.success(function(json){

					if (json) {
						
						$('#keyword').toggleClass('error', json.response);

						if (json.message) {
							$.jGrowl(json.message, { 
								sticky: true, 
								theme:'error'
							});
						}
					}
				});
			}
		});

	});
</script>
<style type="text/css">

	#translation { 
		width: 700px; 
	}
	
	#translation textarea { 
		width: 440px; 
		height: 50px; 
	}
	
	form input.generated { 
		width: 150px !important; 
		margin-left: 10px; 
	}
	
	label em {
		display: block;
		font-style: normal;
		font-size: .8em;
		color: gray;
	}
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>