<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'idea_category_form',
		'action' => '/applications/helpers/idea.category.save.php',
		'method' => 'post'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->idea_category_id(
		Form::TYPE_HIDDEN
	);
	
	$form->idea_category_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->fieldset('Idea', array(
		'idea_category_name'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
$(document).ready(function() {

	var form = $("#idea_category_form");

	// loader instance
	retailnet.loader.init();

	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) {

			// close wait screen
			retailnet.loader.hide();

			if (json) {

				if (json.response && json.redirect) {
					window.location = json.redirect;
				}

				if (json.message) {
					retailnet.notification.show(json.message);
				}
			}
		}
	});
	
	$("#save").click(function(e) { 

		e.preventDefault();

		var validata = form.validationEngine('validate');

		// close all notofications
		retailnet.notification.hide();

		if (validata) {

			// show wait screen
			retailnet.loader.show();
			
			form.submit();
		}

		return false;
	});

	$('.-tooltip[title]').qtip({
		position: {corner: {target:'topRight', tooltip:'leftTop'}},
		style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
	});

	$('.dialog').click(function(e) {

		e.preventDefault();

		var button = $(this);
		var target = '#'+button.attr('id')+'_dialog';
		
		retailnet.modal.show(target, {
			'onComplete':	function() {
				$('#apply').attr('href', button.attr('href'))
			}
		});
	});

	$('#cancel').bind('click', function(e) {

		e.preventDefault();
		e.stopPropagation();
		
		retailnet.modal.hide();
		
		return false;
	});	

	$('#apply').bind('click', function(e) {

		e.preventDefault();

		var self = $(this);

		// show wait screen
		retailnet.loader.show();
		
		retailnet.ajax.json(self.attr('href')).done(function(xhr) {

			if (xhr.redirect) {
				window.location = xhr.redirect;
			}

			if (xhr.message) {
				retailnet.notification.show(xhr.message);
			}
			
		}).complete(function() {

			// close wait screen
			retailnet.loader.hide();
		});
	})

});
</script>

<style type="text/css">

	#idea_category_form { 
		width: 700px; 
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>