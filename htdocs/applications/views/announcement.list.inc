<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {
	
	$('#announcements').tableLoader({
		url: '/applications/helpers/announcement.list.php',
		data: $('.request').serializeArray()
	});
		
});
</script>
<style type="text/css">
	
	#announcements { width: 900px; }
	td.important {width: 20px !important;}
	
	.mps_announcement_title span {
		color: gray; 
		display: block; 
		font-size:11px;
	}
	
</style>
<?php 
	echo $request->form($requestFields);
?>
<div id="announcements"></div>