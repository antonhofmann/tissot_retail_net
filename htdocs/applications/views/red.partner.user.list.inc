<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<link href="/public/scripts/dropdown/dropdown.css" rel="stylesheet" type="text/css" />
<script src="/public/scripts/dropdown/dropdown.js" type="text/javascript"  ></script>
<script type="text/javascript" src="/public/scripts/table.loader.js" ></script>
<script type="text/javascript">
$(document).ready(function() {

	var partnerUsers = $('#partner_users').tableLoader({
		url: '/applications/helpers/red_partner.user.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});

});
</script>
<style type="text/css">
	#partner_users { width: 800px; }
</style>
<form class="request">
	<input type=hidden name=application value="<?php echo $request->application ?>" />
	<input type=hidden name=controller value="<?php echo $request->controller ?>" />
	<input type=hidden name=action value="<?php echo $request->action ?>" />
	<input type=hidden name=archived value="<?php echo $request->archived ?>" />
	<input type=hidden name=id value="<?php echo $request->id ?>" />
	<input type=hidden name=link[add] value="<?php echo $buttons['add'] ?>" />
	<input type=hidden name=link[form] value="<?php echo $buttons['form'] ?>" />
</form>
<div id="partner_users"></div>
