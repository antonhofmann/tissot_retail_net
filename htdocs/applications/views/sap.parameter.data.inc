<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'sapParameter',
		'action' => '/applications/helpers/sap.parameter.save.php',
		'method' => 'post'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->sap_systemparameter_id(
		Form::TYPE_HIDDEN
	);
	
	$form->sap_systemparameter_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->sap_systemparameter_key(
		Form::TYPE_TEXT
	);
	
	$form->sap_systemparameter_order_reason(
		Form::TYPE_TEXT
	);
	
	$form->sap_systemparameter_order_type(
		Form::TYPE_TEXT
	);
	
	$form->sap_systemparameter_distributor_id(
		Form::TYPE_TEXT
	);
	
	$form->sap_systemparameter_division_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->sap_systemparameter_division_code(
		Form::TYPE_TEXT
	);
	
	$form->sap_systemparameter_dicount_code(
		Form::TYPE_TEXT
	);
	
	$form->sap_systemparameter_dicount_percentage(
		Form::TYPE_TEXT
	);

	
	$form->fieldset('SAP System Parameters', array(
		'sap_systemparameter_name',
		'sap_systemparameter_key',
		'sap_systemparameter_order_reason',
		'sap_systemparameter_order_type',
		'sap_systemparameter_distributor_id',
		'sap_systemparameter_division_name',
		'sap_systemparameter_division_code',
		'sap_systemparameter_dicount_code',
		'sap_systemparameter_dicount_percentage'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#sapParameter");

		// loader instance
		retailnet.loader.init();

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			onAjaxFormComplete: function(status, form, json, options) {

				// hide wait screen
				retailnet.loader.hide();
				
				if (json && json.redirect) {
					window.location=json.redirect;
				}

				if (json && json.message) {
					retailnet.notification.show(json.message, {
						sticky: (json.response) ? false : true, 
						theme: (json.response) ? 'message' : 'error'
					});
				}
			}
		});
		
		$("#save").click(function(event) { 

			event.preventDefault();

			var validata = form.validationEngine('validate');

			// close all notofications
			retailnet.notification.hide();

			if (validata) {

				// show wait screen
				retailnet.loader.show();
				
				form.submit();
			}

			return false;
		});

		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});


		// show dialog
		$('.dialog').click(function(e) {

			e.preventDefault();

			var button = $(this);
			var target = '#'+button.attr('id')+'_dialog';
			
			retailnet.modal.show(target, {
				'onComplete':	function() {
					$('.modal-apply', $(target)).attr('href', button.attr('href'))
				}
			});
		});

		// cancel dialog action
		$('.modal-cancel').click(function(e) {

			e.preventDefault();
			e.stopPropagation();
			
			retailnet.modal.hide();
			
			return false;
		});	

		// confirm dialog action
		$('.modal-apply').bind('click', function(e) {

			e.preventDefault();

			var self = $(this);

			// close modal screen
			retailnet.modal.hide();

			// show wait screen
			retailnet.loader.show();
			
			retailnet.ajax.json(self.attr('href')).done(function(xhr) {

				if (xhr.redirect) {
					window.location = xhr.redirect;
				}

				if (xhr.message) {
					retailnet.notification.show(xhr.message);
				}

			}).complete(function() {

				// close wait screen
				retailnet.loader.hide();
			});
		})
	});
</script>

<style type="text/css">

	#sapParameter { 
		width: 700px; 
	}
	
	#sap_country_sap_code {
		width: 50px;
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'class' => 'modal-cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'class' => 'modal-apply',
				'label' => $translate->yes
			),
		)
	));
?>