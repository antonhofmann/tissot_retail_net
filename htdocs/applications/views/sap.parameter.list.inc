<script type="text/javascript">
$(document).ready(function() {

	var sapCountries = $('#saplist').tableLoader({
		url: '/applications/helpers/sap.parameters.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});

});
</script>
<style type="text/css">
	#saplist { width: 900px; }
</style>
<div id="saplist"></div>
<?php 
	echo Request::instance()->form();
?>
