<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();
	
	$form = new Form(array(
		'id' => 'form',
		'action' => '/applications/helpers/item.composition.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->item_composition_item(
		Form::TYPE_HIDDEN
	);

	$form->item_group_id(
		Form::TYPE_CHECKBOX_GROUP
	);

	$form->fieldset('Item Groups', array(
		'item_group_id'
	));
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}

	$form->dataloader($data);
	$form->dataloader($dataloader);

	echo  $form->render();
?>