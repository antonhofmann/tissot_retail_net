<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'db_table_exports',
		'action' => '/applications/helpers/db.table.exports.save.php',
		'method' => 'post'
	));
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->db_table_id(
		Form::TYPE_HIDDEN
	);
	
	$form->db_table_export_fields_order(
		Form::TYPE_HIDDEN
	);
	
	$form->db_table_export_fields_selectable(
		Form::TYPE_CHECKBOX
	);
	
	$caption = ($data['select_all']) ? $translate->deselect_all : $translate->select_all;
	$recaption = ($data['select_all']) ? $translate->select_all : $translate->deselect_all;
	
	$form->select_all(
		Form::TYPE_CHECKBOX,
		"caption=$caption",
		"recaption=$recaption"
	);
	
	$form->db_table_export_fields(
		Form::TYPE_CHECKBOX_GROUP
	);
	
	$form->fieldset('export_fields', array(
		'db_table_export_fields_selectable',
		'select_all',
		'db_table_export_fields'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	if ($buttons['back']) {
		$form->button(ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => $translate->back
		)));
	}

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'mastersheet_delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#db_table_exports");

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			beforeSend: function () {
				retailnet.loader.show();
			},
			onAjaxFormComplete: function(status, form, json, options) {

				retailnet.loader.hide();
				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

				if (json) {

					if (json.response && json.redirect) {
						window.location=json.redirect;
					}

					if (json.message) {
						$.jGrowl(json.message, { 
							sticky: (json.response) ? false : true,
							theme: (json.response) ? 'message' : 'error'
						});
					}
				}
			}
		});
		
		$("#save").click(function(event) { 
			event.preventDefault();
			form.submit();
			return false;
		});

		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});

		$('#select_all').change(function() {
			var self = $(this);
			var caption = self.next('.caption');
			var label = caption.attr('data');
			var checked = (self.is(':checked')) ? true  : false;
			$('input.db_table_export_fields').attr('checked', checked);
			caption.attr('data', caption.text()).text(label);
			updateOrderFields();
		});

		$('.select_all .caption').click(function() {
			$('#select_all').trigger('click');
		});

		$('input.db_table_export_fields').click(function() {
			var checked = ($('input.db_table_export_fields').length == $('input.db_table_export_fields:checked').length) ? true : false;
			$('#select_all').attr('checked', checked);
			updateOrderFields();
		});


		$(".db_table_export_fields .checkbox-group").sortable({
			placeholder: "ui-state-highlight",         
	        scroll:true,
	        update: function( event, ui ) {
	        	updateOrderFields();
			}
		});
	   
	});

	var updateOrderFields = function() {

		var order = [];
		
		$('input.db_table_export_fields').each( function () {
			if ($(this).is(':checked')) {
				order.push($(this).val());
			}
		});

		$('#db_table_export_fields_order').val(order.join(','));
	}
	
</script>

<style type="text/css">

	#db_table_exports { 
		width: 500px; 
	}
	
	form label {
		display: none !important;
	}
	
	.gray {
		color: #999999;
	}
	
	.checkbox-group {
		margin: 0 40px;
	}
	
	#select_all,
	#db_table_export_fields_selectable {
		margin-left: 50px;
	}
	
	
	.checkbox-group .-element {
		padding: 0 10px;
		background-color: #f4f4f4;
		border: 1px solid #ededed;
		margin-bottom: 5px;
		cursor: pointer !important;
	}
	
	form.default .checkbox-group .-caption {
		display: inline;
	}
	
	h6 {
		margin-top: 40px;
	}
	
	.ui-state-highlight { height: 24px; line-height: 24px; background: #fffeee !important; margin-bottom: 5px; }
	
</style>
<h6>Map fields that are allowed to export lists (Excel, PDF, etc..)</h6>
<?php 
	echo $form; 
?>