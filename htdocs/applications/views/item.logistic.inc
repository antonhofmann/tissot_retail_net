<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();
	
	$form = new Form(array(
		'action' => '/applications/helpers/item.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->item_id(
		Form::TYPE_HIDDEN
	);


	// Logistic Data

	$form->item_unit(
		Form::TYPE_SELECT
	);

	$form->item_length(
		Form::TYPE_TEXT,
		"tooltip=true",
		Validata::PARAM_NUMBER_DECIMAL_2
	);

	$form->item_width(
		Form::TYPE_TEXT,
		"tooltip=true",
		Validata::PARAM_NUMBER_DECIMAL_2
	);
	
	$form->item_height(
		Form::TYPE_TEXT,
		"tooltip=true",
		Validata::PARAM_NUMBER_DECIMAL_2
	);

	$form->cbm(
		Form::TYPE_PLACEHOLDER,
		Form::PARAM_SHOW_VALUE
	);
	
	$form->item_radius(
		Form::TYPE_TEXT,
		"tooltip=true",
		Validata::PARAM_NUMBER_DECIMAL_2
	);
	/*
	$form->item_gross_weight(
		Form::TYPE_TEXT,
		"tooltip=true",
		Validata::PARAM_NUMBER_DECIMAL_2
	);
	*/

	$form->item_net_weight(
		Form::TYPE_TEXT,
		"tooltip=true",
		Validata::PARAM_NUMBER_DECIMAL_2
	);

	$form->item_stackable(
		Form::TYPE_CHECKBOX
	);
/*	
	$form->package_informations_caption(
        Form::TYPE_PLACEHOLDER,
		Form::PARAM_SHOW_VALUE,
		'label=Packing Informations'
	);
	*/

	$form->package_informations(
		Form::TYPE_PLACEHOLDER,
		Form::PARAM_SHOW_VALUE,
		'label=Packing Information'
	);

	$table  = "<table id='itemPackageInformations' class='form-list package-informations-list' cellpadding='0' cellspacing='0' border='0' >";
	$table .= "<thead>";
	$table .= "<tr>";
	$table .= "<th nowrap='nowrap' width='5%'>Number</th>";
	$table .= "<th nowrap='nowrap' >Unit of</th>";
	$table .= "<th nowrap='nowrap' >Packaging</th>";
	$table .= "<th nowrap='nowrap' width='5%'>Length <i>cm</i></th>";
	$table .= "<th nowrap='nowrap' width='5%'>Width <i>cm</i></th>";
	$table .= "<th nowrap='nowrap' width='5%'>Height <i>cm</i></th>";
	$table .= "<th nowrap='nowrap' width='5%'>CBM</th>";
	$table .= "<th nowrap='nowrap' width='5%'>Net Weight <i>kg</i></th>";
	$table .= "<th nowrap='nowrap' width='5%'>Gross Weight <i>kg</i></th>";
	$table .= "<th nowrap='nowrap' width='1%'>&nbsp;</th>";
	$table .= "</tr>";
	$table .= "</thead>";
	$table .= "<tbody>";

	// dropdown: unit of measure
	$unitOfMeasure  = "<select name='unit[{id}]' data-value='{value}' >";
	$unitOfMeasure .= "<option value='' >Unit of measure</option>";

	if ($dataloader['item_unit']) {
		foreach ($dataloader['item_unit'] as $key => $value) {
			$unitOfMeasure .= "<option value='$key' >$value</option>";
		}
	}

	$unitOfMeasure .= "</select>";

	// dropdown: packaging type
	$packagingType  = "<select name='packaging[{id}]'  data-value='{value}'>";
	$packagingType .= "<option value='' >Packaging</option>";

	if ($dataloader['item_packaging_type']) {
		foreach ($dataloader['item_packaging_type'] as $key => $value) {
			$packagingType .= "<option value='$key' >$value</option>";
		}
	}

	$packagingType .= "</select>";

	$totalCbm = null;
	$totalWeightNet = null;
	$totalWeightGross = null;

	if ($dataloader['package_informations']) {

		foreach ($dataloader['package_informations'] as $row) {

			$cbm = $row['length'] && $row['width'] && $row['height'] ? number_format($row['length']*$row['width']*$row['height']/1000000, 4, '.', '') : '';

			$totalCbm += $cbm;
			$totalWeightNet += $row['weight_net'];
			$totalWeightGross += $row['weight_gross'];

			if ($disabled['package_informations']) {
				$fieldNumber = $row['number'];
				$fieldUnit = $dataloader['item_unit'] ? $dataloader['item_unit'][$row['unit']] : '';
				$fieldPackaging = $dataloader['item_packaging_type'] ? $dataloader['item_packaging_type'][$row['packaging']] : '';
				$fieldLength = $row['length'];
				$fieldWidth = $row['width'];
				$fieldHeight = $row['height'];
				$fieldWeightNet = $row['weight_net'];
				$fieldWeightGross = $row['weight_gross'];
			} else {
				$fieldNumber = "<input type='text' name='number[]' data-type='integer' value='{$row[number]}' />";
				$fieldUnit = Content::render($unitOfMeasure, array('value' => $row['unit']), true);
				$fieldPackaging = Content::render($packagingType, array('value' => $row['packaging']), true);
				$fieldLength = "<input type='text' name='length[]' data-type='decimal' value='{$row[length]}' />";
				$fieldWidth = "<input type='text'  name='width[]' data-type='decimal' value='{$row[width]}' />";
				$fieldHeight = "<input type='text'  name='height[]' data-type='decimal' value='{$row[height]}' />";
				$fieldWeightNet = "<input type='text'  name='weight_net[]' data-type='decimal' value='{$row[weight_net]}' />";
				$fieldWeightGross = "<input type='text'  name='weight_gross[]' data-type='decimal' value='{$row[weight_gross]}'/>";
				$btnRow = "<span class='btn-list-action btn-remove' ><i class='fa fa-minus-square'></i></span>";
			}
			
			$table .= "<tr>";
			$table .= "<td class='item-number'>$fieldNumber</td>";
			$table .= "<td class='item-unit'>$fieldUnit</td>";
			$table .= "<td class='item-packaging'>$fieldPackaging</td>";
			$table .= "<td class='item-length'>$fieldLength</td>";
			$table .= "<td class='item-width'>$fieldWidth</td>";
			$table .= "<td class='item-height'>$fieldHeight</td>";
			$table .= "<td class='item-cbm calculate'>$cbm</td>";
			$table .= "<td class='item-weight-net calculate'>$fieldWeightNet</td>";
			$table .= "<td class='item-weight-gross calculate'>$fieldWeightGross</td>";
			$table .= "<td>$btnRow</td>";
			$table .= "</tr>";
		}
	} 

	if (!$disabled['package_informations']) {
		$fieldUnit = Content::render($unitOfMeasure, array(), true);
		$fieldPackaging = Content::render($packagingType, array(), true);
		$table .= "<tr class='newrow'>";
		$table .= "<td class='item-number'><input type='text' name='number[]' data-type='integer' /></td>";
		$table .= "<td class='item-unit'>$fieldUnit</td>";
		$table .= "<td class='item-packaging'>$fieldPackaging</td>";
		$table .= "<td class='item-length'><input type='text'  name='length[]' data-type='decimal' /></td>";
		$table .= "<td class='item-width'><input type='text'  name='width[]' data-type='decimal' /></td>";
		$table .= "<td class='item-height'><input type='text'  name='height[]' data-type='decimal' /></td>";
		$table .= "<td class='item-cbm calculate'></td>";
		$table .= "<td class='item-weight-net calculate'><input type='text'  name='weight_net[]' data-type='decimal' /></td>";
		$table .= "<td class='item-weight-gross calculate'><input type='text'  name='weight_gross[]' data-type='decimal' /></td>";
		$table .= "<td><span class='btn-list-action btn-add' ><i class='fa fa-plus-square'></i></span></td>";
		$table .= "</tr>";
	}

	$table .= "</tbody>";

	if (!$disabled['package_informations'] || $dataloader['package_informations']) {
		$table .= "<tfoot>";
		$table .= "<tr>";
		$table .= "<td>&nbsp;</td>";
		$table .= "<td>&nbsp;</td>";
		$table .= "<td>&nbsp;</td>";
		$table .= "<td>&nbsp;</td>";
		$table .= "<td>&nbsp;</td>";
		$table .= "<td align='right' ><b>Total:</b></td>";
		$table .= "<td class='total-cbm' >$totalCbm</td>";
		$table .= "<td class='total-weight-net'>$totalWeightNet</td>";
		$table .= "<td class='total-weight-gross'>$totalWeightGross</td>";
		$table .= "<td>&nbsp;</td>";
		$table .= "</tr>";
		$table .= "<tfoot>";
	}

	$table .= "<input type='hidden' name='item' value='{$data[item_id]}' >";
	$table .= '</table>';

	unset($dataloader['package_informations']);
	$data['package_informations'] = $table;

	$form->fieldset('Logistic Data', array(
		'item_unit',
		'item_length',
		'item_width',
		'item_height',
		'cbm',
		'item_radius',
		//'item_gross_weight',
		'item_net_weight',
		'item_stackable',
		'package_informations'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>

<style type="text/css">
	
	form textarea {
		width: 440px;
		height: 60px;
		outline: none;
	}

	.item_slave_item .desc {
		display: block;
		font-size: 12px;
		margin: 5px 10px 0 194px;
		color: gray;
	}

	
	#item_packed_weight,
	#item_materials,
	#item_lights_used,
	#item_regulatory_approvals,
	#item_install_requirements {
		height: 30px !important;
	}

	
	#item_packed_size {
		height: 120px !important;
	}

	.btn-list-action {
		font-size: 18px;
		color: #888;
		cursor: pointer;
	}

	.btn-list-action.btn-add {
		display: none;
	}

	.btn-list-action:hover {color: #000;}


	form.default .form-list tr td {
		padding: 5px;
	}

	form.default .form-list input[type="text"] {
		width: auto;
	}

	form.default .package-informations-list {
		width: 660px;
		margin-left: 10px;
		margin-right: 10px;
	}

	form.default .package-informations-list tr th {
		font-weight: 600;
		font-size: 11px;
		text-align: left;
		padding: 5px;
		border-bottom: 1px solid #ccc;
	}

	form.default .package-informations-list tr th i {
		font-weight: 300;
		font-size: 10px;
	}

	form.default .package-informations-list tfoot td {
		border-top: 1px solid #ccc;
	}

	form.default .package-informations-list tr td input[type="text"] {
		max-width: 60px;
	}

	form.default .package-informations-list tr td:nth-child(1) input[type="text"] {
		width: 40px
	}

	form.default .package-informations-list tr th:last-child {
		width: 26px
	}

	form.default .package_informations label {
		display: block;
	}
	
</style>
<?php 
	echo $form; 
?>