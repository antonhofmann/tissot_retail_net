<script type="text/javascript">
	$(document).ready(function() {

		retailnet.loader.init();

		$('#save').click(function(e) {

			e.preventDefault();

			var form = $('#roles'),
				action = form.attr('action'),
				data = form.serializeArray();

			// show loader
			retailnet.loader.show();

			retailnet.ajax.json(action, data).done(function(xhr) {

				retailnet.loader.hide();
	    		
	    		if (xhr && xhr.reload) {
					window.location.reload();
	        	}
	    		
				if (xhr && xhr.notification) {
					retailnet.notification.show(
						xhr.notification.content, 
						eval(xhr.notification.properties
					));
				}
			});
			
		});

		// checkbox as radio
		$('table input').change(function() {
			$('input', $(this).closest('tr')).not(this).attr('checked', false);
		});
		
	});
</script>
<style type="text/css">
	
	#form {
		width: 500px;
		margin-top: 40px;
	}
	
	#form h4 {
		display: block;
		margin-top: 20px;
		margin-bottom: 10px;
		font-size: 16px;
		color: gray;
	}
	
	#form .container {
		display: block;
		background-color: white;
		border: 1px solid silver;
	}
	
	#form table th {
		font-size: 12px;
		color: black;
		background-color: #ddd !important;
	}
	
</style>
<?php 
	
	$form  = "<form id=roles method=post action='/applications/helpers/mail.template.roles.php' >";
	$form .= "<input type=hidden name=template id=template value=$template >";
	
	if ($roles) { 

		foreach ($roles as $app => $data) {
			
			$form .= "<h4>$app</h4>";
			$form .= "<div class='container' >";

			$table = new Table(array(
				'class'=>'listing'
			));
			
			$table->datagrid = $data;
			$table->dataloader($dataloader);
			
			$table->caption('recipient', 'Recipient');
			$table->caption('cc', 'CC');
			
			$table->recipient(
				Table::ATTRIBUTE_NOWRAP, 
				Table::DATA_TYPE_CHECKBOX,
				'width=5%'
			);

			$table->cc(
				Table::ATTRIBUTE_NOWRAP, 
				Table::DATA_TYPE_CHECKBOX,
				'width=5%'
			);

			$table->role_name(
				Table::ATTRIBUTE_NOWRAP
			);

			$form .= $table->render();

			$form .= "</div>";
		}
	}

	if ($others) {
		
		$form .= "<h4>Other</h4>";
		$form .= "<div class='container' >";

		$table = new Table(array(
			'class'=>'listing'
		));

		$table->datagrid = $others;
		$table->dataloader($dataloaderOthers);
		
		$table->other_recipient(
			Table::ATTRIBUTE_NOWRAP, 
			Table::DATA_TYPE_CHECKBOX,
			'width=5%',
			"caption=Recipient",
			"value=1"
		);

		$table->other_recipient_cc(
			Table::ATTRIBUTE_NOWRAP, 
			Table::DATA_TYPE_CHECKBOX,
			'width=5%',
			"caption=CC",
			"value=1"
		);

		$table->other_name(
			Table::ATTRIBUTE_NOWRAP,
			"caption=Name"
		);

		$form .= $table->render();
		$form .= "</div>";
	}

	$form .= "</form>";
	
	$actions = "<div class=actions >";
	
	$actions .= ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => Translate::instance()->back
	));

	$actions .= ui::button(array(
		'icon' => 'save',
		'id' => 'save',
		'label' => Translate::instance()->save
	));
	
	$actions .= "</div>";
	
	echo "<div id=form >$form $actions</div>";
?>
