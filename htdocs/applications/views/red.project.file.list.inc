<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<link href="/public/scripts/dropdown/dropdown.css" rel="stylesheet" type="text/css" />
<script src="/public/scripts/dropdown/dropdown.js" type="text/javascript"  ></script>
<script type="text/javascript" src="/public/scripts/table.loader.js" ></script>
<script type="text/javascript">
$(document).ready(function() {

	var projectFiles = $('#project_files').tableLoader({
		url: '/applications/helpers/red.project.files.list.php',
		data: $('.request').serializeArray(),
		after: function(self, settings) {
			$('h6').addClass('toggle');
		}
	});

	$(document).delegate(".modal", "click", function() {

		var path = $(this).attr('path');
		var title = $(this).parent().next().find('a').text();
		title = (title) ? title : '';

		if (isImage(path)) {

			retailnet.modal.show(path, {
				title: title
			});
		}
		else window.open('http://'+location.host+path);
	});

	$(document).delegate("h6", "click", function() {
		caption = $(this);
		$(this).next('table').first().toggle('slow', function() {
			if ($(this).is(":visible")) caption.removeClass('closed');
			else caption.addClass('closed');
        });
	});

});
</script>
<style type="text/css">

	#project_files {
		width: 700px;
	}

	td span {
		display:block;
		color: gray;
		font-size: 11px;
	}

	td.fileicon {
		width: 32px;
	}

	h6 {
		display:block;
		margin: 40px 0 20px;
		font-size: .95em;
		color: gray;
	}

	table {
		margin-bottom:20px;
		border-style: solid;
		border-color: silver;
	}

	.file-extension {
		cursor: pointer;
	}

</style>
<?php 
	echo $request->form(); 
?>
<div id="project_files"></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>