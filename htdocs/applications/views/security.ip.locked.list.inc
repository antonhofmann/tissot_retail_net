<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<link href="/public/scripts/dropdown/dropdown.css" rel="stylesheet" type="text/css" />
<script src="/public/scripts/dropdown/dropdown.js" type="text/javascript"  ></script>
<script type="text/javascript" src="/public/scripts/table.loader.js" ></script>
<script type="text/javascript">
$(document).ready(function() {

	$('#lockedips').tableLoader({
		url: '/applications/helpers/security.ip.locked.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});

	$('#lockedips').delegate('table .button', 'click', function(event) {

		var ip = $(this).attr('data');
		var tr = $(this).closest('tr');

		event.stopPropagation();
		
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
		
		$.ajax({
			type: "POST",
			url: '/applications/helpers/security.ip.unlock.php',
			data: { ip: ip },
			dataType: 'json',
			success: function(json) {
				if (json) {
					$.jGrowl(json.message,{ sticky: false, theme:'message'});
					tr.remove();
				}
			}
		});

		return false;
	});
	
});
</script>
<style type="text/css">

	#lockedips { 
		width: 500px; 
	}
	
	.icon {
		margin: 0 !important;
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="lockedips"></div>