<?php 
	$translate = Translate::instance();
	$request = request::instance();
?>
<script src="/public/scripts/pop/pop.js" type="text/javascript"></script>
<link href="/public/scripts/pop/pop.css" rel="stylesheet" type="text/css">
<link href="/public/scripts/dropdown/dropdown.css" rel="stylesheet" type="text/css" />
<script src="/public/scripts/dropdown/dropdown.js" type="text/javascript"  ></script>
<link rel="stylesheet" type="text/css" href="/public/scripts/loader/loader.css" />
<script type="text/javascript" src="/public/scripts/loader/loader.js"  ></script>
<script type="text/javascript" src="/public/scripts/table.loader.js" ></script>
<script type="text/javascript">
$(document).ready(function() {

	var projectComments = $('#project_comments').tableLoader({
		url: '/applications/helpers/red.project.comments.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			$('h6').addClass('toggle');

			$('.filebox').pop().click(function() {
				$('.icon',this).toggleClass('direction-up');
			});
		}
	});

	//Add toggle effect
	$(document).delegate("h6", "click", function() {
		caption = $(this);
		$(this).next('table').first().toggle('slow', function() {
			if ($(this).is(":visible")) caption.removeClass('closed');
			else caption.addClass('closed');
        });
	});


	$(document).delegate(".modal", "click", function() {

		var path = $(this).attr('path');
		var title = $('.label',this).text() || '';

		if (isImage(path)) {

			retailnet.modal.show(path, {
				title: title
			});
		}
		else window.open('http://'+location.host+path);
	});
});
</script>
<style type="text/css">

	#project_comments {
		width: 700px;
	}
	
	h6 {
		width: 100%;
		margin: 50px 0 20px;
		font: 16px normal sans-serif,arial;
		font-weight: 600;
		color: gray;
	}
	
	.table-conainer h6:first-child {
		margin-top: 0 !important;
	}

	.comment-box {
		display: block;
		background: white;
		border: 1px solid silver;
		border-radius: 4px;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		margin-bottom: 20px;
	}
	
	.comment-header {
		display: block;
		width: 100%;
		height: 30px;
		padding: 10px 0;
	}
	
	.header-left {
		float: left;
		margin-left: 10px;
	}
	
	.header-right {
		float: right;
		margin-right: 10px;
	}
	
	.comment-content {
		display: block;
		padding: 10px;
		color: black;
		font-size: 12px;
	}

	.user {
		display: block;
		color: black;
		font-size: 14px;
		font-weight: 600;
	}
	
	.date {
		display: block;
		color: gray;
		font-size: 11px;
	}
	
	.comment-box .icon {
		margin: 0 !important;
	}
	
	.popover li {
		cursor: pointer;
	}
	
	.file-extension {
		position: relative;
		top: 3px;
	}

</style>
<?php 
	echo $request->form();
?>
<div id="project_comments"></div>
<div class='actions'>
<?php
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>

