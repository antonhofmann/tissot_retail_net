<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#collection_categories').tableLoader({
		url: '/applications/helpers/material.collection.category.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">

	#collection_categories { 
		width: 800px; 
	}
	
</style>
<?php echo $request->form($requestFields);  ?>
<div id="collection_categories"></div>