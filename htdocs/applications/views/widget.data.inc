<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();
	
	$form = new Form(array(
		'id' => 'widgetForm',
		'action' => '/applications/helpers/widget.save.php',
		'method' => 'post'
	));
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->ui_widget_id(
		Form::TYPE_HIDDEN
	);
	
	$form->ui_widget_title(
		Form::TYPE_TEXT,
		Form::PARAM_REQUIRED
	);
	
	$form->ui_widget_desciption(
		Form::TYPE_TEXTAREA,
		Form::PARAM_REQUIRED,
		Form::PARAM_LABEL
	);
	
	$form->fieldset('Description', array(
		'ui_widget_title',
		'ui_widget_desciption'
	));
	
	$form->ui_widget_category_id(
		Form::TYPE_SELECT,
		Form::PARAM_REQUIRED
	);
	
	$form->ui_widget_theme_id(
		Form::TYPE_SELECT,
		Form::PARAM_REQUIRED
	);
	
	$form->ui_widget_icon(
		Form::TYPE_TEXT
	);
	
	$form->fieldset('Appearance', array(
		'ui_widget_category_id',
		'ui_widget_theme_id',
		'ui_widget_icon'
	));
	
	$form->ui_widget_width(
		Form::TYPE_SELECT,
		Form::PARAM_REQUIRED
	);
	
	$form->ui_widget_height(
		Form::TYPE_SELECT,
		Form::PARAM_REQUIRED
	);
	
	$form->ui_widget_auto_refresh(
		Form::TYPE_SELECT
	);
	
	$form->ui_widget_standard(
		Form::TYPE_CHECKBOX
	);
	
	$form->ui_widget_deletable(
		Form::TYPE_CHECKBOX
	);
	
	$form->ui_widget_resizable(
		Form::TYPE_CHECKBOX
	);
	
	$form->ui_widget_draggable(
		Form::TYPE_CHECKBOX
	);
	
	$form->ui_widget_active(
		Form::TYPE_CHECKBOX
	);
	
	$form->fieldset('Settings', array(
		'ui_widget_width',
		'ui_widget_height',
		'ui_widget_auto_refresh',
		'ui_widget_standard',
		'ui_widget_deletable',
		'ui_widget_resizable',
		'ui_widget_draggable',
		'ui_widget_active'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#widgetForm");

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			onAjaxFormComplete: function(status, form, json, options) {

				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

				if (json) {

					if (json.response && json.redirect) {
						window.location=json.redirect;
					}

					if (json.message) {
						$.jGrowl(json.message, { 
							sticky: (json.response) ? false : true,
							theme: (json.response) ? 'message' : 'error'
						});
					}
				}
			}
		});
		
		$("#save").click(function(event) { 
			event.preventDefault();
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
			form.submit();
			return false;
		});

		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});

		$('.dialog').click(function(event) {

			event.preventDefault();

			var button = $(this);
			
			$('#apply, a.apply').attr('href', button.attr('href'));
			
			retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
		});

		$('#cancel').bind('click', function(event) {
			event.preventDefault();
			retailnet.modal.close();
			return false;
		});	
	});
	
</script>

<style type="text/css">

	#widgetForm { 
		width: 700px; 
	}
	
	#ui_widget_desciption {
		width: 440px;
		height: 120px;
	}
	
</style>
<?php 
	
	echo $form; 
		
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>