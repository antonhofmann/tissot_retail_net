<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'visual',
		'action' => '/applications/helpers/visual.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_visual_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_visual_code(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_visual_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_visual_active(
		Form::TYPE_CHECKBOX
	);
	
	$form->mps_visual_width(
		Form::TYPE_TEXT,
		Form::TOOLTIP_INTEGER,
		Validata::PARAM_ONLY_NUMBER
	);
	
	$form->mps_visual_height(
		Form::TYPE_TEXT,
		Form::TOOLTIP_INTEGER,
		Validata::PARAM_ONLY_NUMBER
	);
	
	$form->mps_visual_length(
		Form::TYPE_TEXT,
		Form::TOOLTIP_INTEGER,
		Validata::PARAM_ONLY_NUMBER
	);
	
	$form->mps_visual_radius(
		Form::TYPE_TEXT,
		Form::TOOLTIP_INTEGER,
		Validata::PARAM_ONLY_NUMBER
	);
	
	$form->fieldset('visual', array(
		'mps_visual_code',
		'mps_visual_name',
		'mps_visual_active'
	));
	
	$form->fieldset('dimensions', array(
		'mps_visual_width',
		'mps_visual_height',
		'mps_visual_length',
		'mps_visual_radius'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<style type="text/css">

	#visual { 
		width: 700px; 
	}
	
	form .dimensions input { 
		width: 120px; 
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>