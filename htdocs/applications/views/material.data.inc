<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
		
	$form = new Form(array(
		'id' => 'material_form',
		'action' => '/applications/helpers/material.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_material_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_material_islongterm(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_material_material_collection_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_material_material_collection_category_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->mps_material_material_planning_type_id(
		($data['mps_material_islongterm']) ? Form::TYPE_HIDDEN : Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_material_material_category_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_material_material_purpose_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_material_hsc(
		Form::TYPE_TEXT,
		"label=Harmonized System Codes (HSC)"
	);
	
	$form->mps_material_ean_number(
		Form::TYPE_TEXT,
		Validata::minsize(13),
		"maxlength=13"
	);
	
	$form->mps_material_code(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_material_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_material_price(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		Validata::PARAM_NUMBER
	);
	
	$form->mps_material_currency_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_material_setof(
		Form::TYPE_TEXT,
		Validata::PARAM_ONLY_NUMBER,
		Form::TOOLTIP_INTEGER
	);
	
	$form->mps_material_description(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);
	
	$form->mps_material_active(
		Form::TYPE_CHECKBOX
	);
	
	$form->mps_material_width(
		Form::TYPE_TEXT,
		Validata::PARAM_ONLY_NUMBER,
		Form::TOOLTIP_INTEGER
	);
	
	$form->mps_material_height(
		Form::TYPE_TEXT,
		Validata::PARAM_ONLY_NUMBER,
		Form::TOOLTIP_INTEGER
	);
	
	$form->mps_material_length(
		Form::TYPE_TEXT,
		Validata::PARAM_ONLY_NUMBER,
		Form::TOOLTIP_INTEGER
	);
	
	$form->mps_material_radius(
		Form::TYPE_TEXT,
		Validata::PARAM_ONLY_NUMBER,
		Form::TOOLTIP_INTEGER
	);
	
	$form->fieldset('material', array(
		'mps_material_material_collection_id',
		'mps_material_material_collection_category_id',
		'mps_material_material_planning_type_id',
		'mps_material_material_category_id',
		'mps_material_material_purpose_id',
		'mps_material_hsc',
		'mps_material_ean_number',
		'mps_material_code',
		'mps_material_name',
		'mps_material_price',
		'mps_material_currency_id',
		'mps_material_setof',
		'mps_material_description',
		'mps_material_active'
	));
	
	// fieldset: dimensions
	$form->fieldset('dimenssions', array(
		'mps_material_width',
		'mps_material_height',
		'mps_material_length',
		'mps_material_radius'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<style type="text/css">

	#material_form { 
		width: 700px; 
	}

	#material_form label { width: 200px; }
	
	#mps_material_ean_number,
	#mps_material_setof,
	#mps_material_price { 
		width:120px;
	}
	
	form .dimenssions input { 
		width: 120px; 
	}
	
	#mps_material_description { 
		width: 440px; 
		height: 80px; 
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>