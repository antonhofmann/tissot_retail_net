<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'sale',
		'action' => '/applications/helpers/pos.sale.save.php',
		'method' => 'post'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->posaddress_id(
		Form::TYPE_HIDDEN
	);
	
	$form->posaddress_sales_representative(
		Form::TYPE_SELECT
	);
	
	$form->posaddress_decoration_person(
		Form::TYPE_SELECT
	);
	
	$form->posaddress_turnoverclass_watches(
		Form::TYPE_SELECT
	);
	
	$form->posaddress_turnoverclass_bijoux(
		Form::TYPE_SELECT
	);
	
	$form->posaddress_sales_classification01(
		Form::TYPE_TEXT,
		Form::TOOLTIP_PERSONAL_CLASSIFICATION
	);
	
	$form->posaddress_sales_classification02(
		Form::TYPE_TEXT,
		Form::TOOLTIP_PERSONAL_CLASSIFICATION
	);
	
	$form->posaddress_sales_classification03(
		Form::TYPE_TEXT,
		Form::TOOLTIP_PERSONAL_CLASSIFICATION
	);
	
	$form->posaddress_sales_classification04(
		Form::TYPE_TEXT,
		Form::TOOLTIP_PERSONAL_CLASSIFICATION
	);
	
	$form->posaddress_sales_classification05(
		Form::TYPE_TEXT,
		Form::TOOLTIP_PERSONAL_CLASSIFICATION
	);
	
	$form->posaddress_selling_bijoux(
		Form::TYPE_CHECKBOX
	);
	
	$form->posaddress_max_watches(
		Form::TYPE_TEXT,
		Form::TOOLTIP_INTEGER
	);
	
	$form->posaddress_max_bijoux(
		Form::TYPE_TEXT,
		Form::TOOLTIP_INTEGER
	);
	
	$form->posaddress_overall_sqms(
		Form::TYPE_TEXT
	);
	$form->posaddress_dedicated_sqms_wall(
		Form::TYPE_TEXT
	);
	
	$form->posaddress_dedicated_sqms_free(
		Form::TYPE_TEXT
	);
	
	$form->posaddress_store_totalsurface(
		Form::TYPE_TEXT
	);
	
	$form->posaddress_store_retailarea(
		Form::TYPE_TEXT
	);
	
	$form->posaddress_store_backoffice(
		Form::TYPE_TEXT
	);
	
	$form->posaddress_store_numfloors(
		Form::TYPE_TEXT
	);
	$form->posaddress_store_floorsurface1(
		Form::TYPE_TEXT
	);
	
	$form->posaddress_store_floorsurface2(
		Form::TYPE_TEXT
	);
	$form->posaddress_store_floorsurface3(
		Form::TYPE_TEXT
	);
	
	$form->fieldset('staff', array(
		'posaddress_sales_representative', 
		'posaddress_decoration_person'
	));
	
	$form->fieldset('classsification', array(
		'posaddress_turnoverclass_watches',
		'posaddress_turnoverclass_bijoux',
		'posaddress_turnover_type', 
		'posaddress_sales_classification01', 
		'posaddress_sales_classification02', 
		'posaddress_sales_classification03', 
		'posaddress_sales_classification04', 
		'posaddress_sales_classification05'
	));
	
	$form->fieldset('items_sold', array(
		'posaddress_selling_bijoux'
	));
	
	$form->fieldset('displayed_items', array(
		'posaddress_max_watches'
	));
	
	$form->fieldset('surfaces', array(
		'posaddress_overall_sqms', 
		'posaddress_dedicated_sqms_wall', 
		'posaddress_dedicated_sqms_free',
		'posaddress_store_totalsurface', 
		'posaddress_store_retailarea', 
		'posaddress_store_backoffice', 
		'posaddress_store_numfloors',
		'posaddress_store_floorsurface1', 
		'posaddress_store_floorsurface2', 
		'posaddress_store_floorsurface3'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
		
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript" src="/public/js/pos.sale.js"></script>
<style type="text/css">
	
	#sale { 
		width: 700px; 
	}
	
	form .displayed_items label,
	form .surfaces label { 
		width: 280px; 
	}
	
	form .displayed_items input,
	form .surfaces input { 
		width: 100px; 
	}
	
</style>
<?php 
	echo $form; 
?>
	