<?php
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	$form = new Form(array(
		'id' => 'projectPartner',
		'action' => '/applications/helpers/red.project.partner.save.php',
		'method' => 'post'
	));

	$form->red_project_partner_id(
		Form::TYPE_HIDDEN
	);
	
	$form->red_project_partner_project_id(
		Form::TYPE_HIDDEN
	);

	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);

	$form->red_project_partner_address_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->red_project_partner_user_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED,
		Form::PARAM_AJAX
	);

	$form->red_project_partner_partnertype_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->red_project_partner_access_from(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		"class=datepicker"
	);

	$form->red_project_partner_access_until(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		"class=datepicker"
	);

	$form->fieldset('partner', array(
		'red_project_partner_address_id',
		'red_project_partner_user_id',
		'red_project_partner_partnertype_id',
	));

	$form->fieldset('access', array(
		'red_project_partner_access_from',
		'red_project_partner_access_until'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}

	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}

	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<link rel="stylesheet" type="text/css" href="/public/scripts/jquery/jquery.ui.css" />
<link rel="stylesheet" type="text/css" href="/public/css/jquery.ui.css" />
<script type="text/javascript" src="/public/scripts/jquery/jquery.ui.js"  ></script>
<link rel="stylesheet" href="/public/scripts/validationEngine/css/validationEngine.jquery.css" type="text/css"/>
<script src="/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="/public/scripts/validationEngine/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="/public/scripts/chain/chained.select.js"  ></script>
<link rel="stylesheet" type="text/css" href="/public/scripts/loader/loader.css" />
<script type="text/javascript" src="/public/scripts/loader/loader.js"  ></script>
<script type="text/javascript" src="/public/scripts/qtip/qtip.js"  ></script>
<link rel="stylesheet" type="text/css" href="/public/css/jquery.ui.css" />
<style type="text/css">

	#projectPartner { 
		width: 600px; 
	}
	
	#red_project_partner_access_from {
		width: 100px;
	}
	
	#red_project_partner_access_until {
		width: 100px;
	}
	
</style>
<script type="text/javascript">
$(document).ready(function() {

	var form = $('#projectPartner');

	// chained select company
	$('#red_project_partner_address_id').chainedSelect('#red_project_partner_user_id', {
		url : "/applications/helpers/red.project.ajax.php",
		parameters: {
      		section: "company"
      	}
	}).trigger('change');

	// form submiter
	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) {

			retailnet.loader.hide();
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

			if (json.response) {
				$.jGrowl(json.message,{ sticky: false, theme:'message'});
				if (json.redirect) window.location=json.redirect;
			}
			else {
				$.jGrowl(json.message,{ sticky: true, theme:'error'});
			}
		}
	});

	$("#save").click(function(event) {

		event.preventDefault();

		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

		var errors=0;
		var elements = $('input,textarea,select');
		var validator = form.validationEngine('validate');

		$.each(elements, function(index, elem) {
			if ($(elem).hasClass('error')) {
				errors++;
			}
		});

		if (errors || !validator) {
			$.jGrowl('Please check red marked fields.', { 
				sticky: true, 
				theme: 'error'
			});
		}
		else {
			retailnet.loader.show();
			$(form).submit();
		}
	});

	$('.dialog').click(function(event) {

		event.preventDefault();

		var button = $(this);
		
		$('#apply, a.apply').attr('href', button.attr('href'));
		
		retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
	});

	// cancel action
	$('#cancel, .cancel').click(function(event) {
		
		event.preventDefault();
		
		retailnet.modal.close();
	
		return false;
	});


	$('.-tooltip[title]').qtip({
		position: {corner: {target:'topRight', tooltip:'leftTop'}},
		style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
	});


	$('.datepicker').datepicker({
		dateFormat: 'dd.mm.yy',
		showOtherMonths : true,
		firstDay: 1,
		onClose: function(dateText, inst) {
			//$(this).toggleClass('error', (dateText) ? false : true)
		}
	});
});
</script>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>
