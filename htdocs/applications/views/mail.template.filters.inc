<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#filters').tableLoader({
		url: '/applications/helpers/mail.template.filters.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			
		}
	});
});
</script>
<style type="text/css">
	
	#filters {
		width: 700px; 
	}
	
	table span {
		display: block;
		margin-top: 5px;
		color: gray;
	}
	
	table span i {
		display: inline-block;
		width: 36px;
		color: #999;
		font-style: normal;
	}
	
</style>
<div id="filters"></div>
<?php 	
	echo $request->form();
?>
