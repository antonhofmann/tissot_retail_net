<script type="text/javascript">
$(document).ready(function() {

	var dash = $('#dashboards').tableLoader({
		url: '/applications/helpers/dashboard.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">
	#dashboards { width: 600px; }
</style>
<?php 
	echo Request::instance()->form();
?>
<div id="dashboards"></div>