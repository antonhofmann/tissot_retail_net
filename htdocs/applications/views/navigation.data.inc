<?php 

	$settings = Settings::init(); 
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'navigation',
		'action' => '/applications/helpers/navigation.save.php',
		'method' => 'post'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->navigation_id(
		Form::TYPE_HIDDEN
	);
	
	$form->navigation_parent(
		Form::TYPE_HIDDEN
	);

	$form->navigation_url(
		Form::TYPE_HIDDEN
	);
	
	$form->select_navigation_parent(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE
	);
	
	$form->extended('select_navigation_parent', "<div id=menutree>$menutree</div>");
	
	$form->navigation_application(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED,
		'class=build-url'
	);
	
	$form->navigation_controller(
		Form::TYPE_SELECT,
		'class=build-url'
	);
	
	$form->navigation_action(
		Form::TYPE_TEXT,
		'class=build-url'
	);
	
	$form->navigation_path(
		Form::TYPE_AJAX
	);
	
	$form->navigation_archived(
		Form::TYPE_CHECKBOX,
		'class=build-url'
	);
	
	$form->navigation_tab(
		Form::TYPE_CHECKBOX
	);
	
	$form->navigation_active(
		Form::TYPE_CHECKBOX
	);
	
	$form->navigation_language(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE
	);
	
	$form->navigation_caption(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->fieldset('navigation', array(
		'select_navigation_parent',
		'navigation_application',
		'navigation_controller',
		'navigation_action',
		'navigation_path',
		'navigation_tab',
		'navigation_archived',
		'navigation_active'
	));
	
	$form->fieldset('navigation_language_english', array(
		'navigation_language',
		'navigation_caption'		
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>

<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#navigation"),
			navID = $('#navigation_id'),
			navParent = $('#navigation_parent'),
			navURL = $('#navigation_url'),
			navApplication = $('#navigation_application'),
			navController = $('#navigation_controller'),
			navAction = $('#navigation_action'),
			navArchived = $('#navigation_archived'),
			navPath = $('#navigation_path'),
			btnParent = $('#parentTrigger');

		btnParent.pop().click(function() {
			$('.icon',this).toggleClass('direction-up');
		});

		$('#menutree li').bind({
			click: function(event) {
				event.stopPropagation();
				$('ul:first',$(this)).slideToggle();
				$('.arrow:first',this).toggleClass('arrow-right').toggleClass('arrow-down');
				$('ul', $(this).siblings('li')).removeClass('arrow-down').slideUp();
			},
			dblclick: function(event) {

				event.stopPropagation();

				var id = $(this).attr('id');
				var title = $('.label:first',this).text();
				var url = $(this).attr('url').split('/');

				// reset tree menu
				$('#menutree li ul').slideUp();
				$('#menutree li').removeClass('selected');
				$('#menutree .arrow').removeClass('arrow-down').addClass('arrow-right');

				// close popup box
				$('.icon', btnParent).removeClass('direction-up');
				$('.label', btnParent).text(title).removeClass('error');
				$(this).addClass('selected');
				btnParent.pop('hide');

				// assign selected value
				navApplication.val(url[0]);
				navController.val(url[1]);
				navURL.trigger('change');
				navParent.val(id);
			}
		});

		$('.build-url').change(function() {
			navURL.trigger('change');
		});

		// build navigation path
		navURL.change(function() {

			var url = [];

			if (navApplication.val()) url.push(navApplication.val());
			if (navController.val()) url.push(navController.val());
			if (navArchived.is(':checked')) url.push('archived');
			if (navAction.val()) url.push(navAction.val());

			if (url.length > 0) {
				url = url.join('/');
				navURL.val(url);
				navPath.text(url);
			}
		}).trigger('change');

		form.validationEngine({
			autoHidePrompt: true,
			ajaxFormValidation: true,
			ajaxFormValidationMethod: 'post',
			beforeSend: function () {
				retailnet.loader.show();
			},
			onAjaxFormComplete: function(status, form, json, options) {

				retailnet.loader.hide();
				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

				if (json) {

					if (json.response && json.redirect) {
						window.location=json.redirect;
					}

					if (json.message) {
						$.jGrowl(json.message,{ 
							sticky: (json.response) ? false : true, 
							theme: (json.response) ? 'message' : 'error'
						});
					}
				}
			}
		});
		
		$("#save").click(function(event) { 

			event.preventDefault();

			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

			var url = navURL.val();
			var id = navID.val();

			if (!navParent.val()) {

				$('.label',btnParent).addClass('error');

				$.jGrowl('Please, select navigation parent.', { 
					sticky: true, 
					theme:'error'
				});
			}
			else if (url) {
				
				$.ajax({
					url: '/applications/helpers/navigation.ajax.php?section=check_url',
					data: { 
						id: id,
						url: url 
					},
					dataType: 'json',
					success: function(json) {
	
						$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
						navPath.removeClass('error');

						var error = false;
						var message = null;
						var validata = form.validationEngine('validate');
	
						if (json) { 

							if (json.response) { 
								if (json.id) { 
									error = ($('#navigation_id').val()==json.id) ? false : true;
								} else { 
									error = true;
								}
							}
	
							if (error || !validata) {

								if (error) {
									message = '<span>THis URL is already taken.</span>';
								}

								if (!validata) {
									message += '<span>Set all form required Fields.</span>';
								}
								
								navPath.addClass('error');

								$.jGrowl(message, { 
									sticky: true, 
									theme:'error'
								});
								
							} else {
								form.submit();
							}
						}
					}
				});
			} else { 
				$.jGrowl('Please, build your URL.', { 
					sticky: true, 
					theme:'error'
				});
			}
		});

		$('.-tooltip[title]').qtip({
			position: {corner: {target:'topRight', tooltip:'leftTop'}},
			style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
		});

		$('.dialog').click(function(event) {

			event.preventDefault();

			var button = $(this);
			
			$('#apply, a.apply').attr('href', button.attr('href'));
			
			retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
		});

		$('#cancel').bind('click', function(event) {
			event.preventDefault();
			retailnet.modal.close();
			return false;
		});	
	});
</script>

<style type="text/css">

	#navigation { width: 700px; }
	
	#navigation_path {
		font-weight: 500;
		color: royalblue;
	}
	
	#menutree {
		min-width: 220px;
	}
	
	#menutree li {
		cursor: pointer;
	}
	
	#menutree li ul {
		display: none;
	}
	
	#menutree li.selected .label {
		color: royalblue;
	}
	
	.button.pop {
		position: relative;
		left: 0;
	}
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>