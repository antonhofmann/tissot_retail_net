<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#mps_sap_hq_ordertypes').tableLoader({
		url: '/applications/helpers/company.mps_sap_hq_ordertypes.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			var dropdown = $("select",self).dropdown();

		}
	});

});
</script>
<style type="text/css">

	#mps_sap_hq_ordertypes { 
		width: 600px;
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="mps_sap_hq_ordertypes" ></div>