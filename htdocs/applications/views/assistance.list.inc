<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<style type="text/css">

	#assistances { 
		width: 800px;
	}
	
	.groups {
		display: inline-block;
		position: relative;
		top: 0; left: 180px;
	}
	
	.ie .groups {
		left: 174px;
	}
	
	.ie.ie7 .groups {
		left: 185px;
	}
	
	.fa {
		color: silver;
		margin-right: 8px;
	}

	.fa.published {
		color: green !important;
	}
	
	.fa.fa-plus {
		color: blue;
		margin-right: 10px;
	}
	
	.fa.fa-plus:hover {
		color: black;
	}

	.fa.orderer {
		cursor: pointer;
	}
	
	.panel .title .section-title {
		font-weight: 500;
		font-size: 14px;
		color: black;
	}
	
	.panel .title a.section-title:hover {
		color: red;
	}
	
	.panel .toggler {
		color: gray;
	}
	
	.panel .toggler:hover {
		color: black;
	}
	
	.panel .content,
	.panel .bundel-sections {
		display: none;
	}
	
	.panel.selected,
	.panel .bundel-sections.selected {
		display: block;
	}

	ul.bundel-sections {
		min-height: 20px;
	}

	.ui-state-highlight {
		background-color: #fffff0 !important;
	}
	
</style>
<?php 
	echo $request->form();
?>
<div id="assistances"></div>