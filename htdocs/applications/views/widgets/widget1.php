<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$user = User::instance();

$model = new Model();

$sth = $model->db->prepare("
	SELECT DISTINCT
		task_id, 
		order_number,  
		task_text, 
		task_url,
		CONCAT(
			DAYOFMONTH(task_due_date), '.', 
			MONTH(task_due_date), '.', 
			RIGHT(YEAR(task_due_date), 2)
		) AS date,
		CONCAT(
			order_number, ', ', 
			address_company, ', ', 
			country_name
		) AS head,
		CONCAT(
			users2.user_name, ' ', 
			users2.user_firstname
		) AS logistic_coordinator 
	FROM tasks 
		LEFT JOIN orders ON order_id = task_order 
		LEFT JOIN users AS users1 ON task_user = users1.user_id 
		LEFT JOIN users AS users2 ON order_retail_operator = users2.user_id 
		LEFT JOIN addresses ON order_client_address = address_id 
		LEFT JOIN countries ON address_country = countries.country_id 
	WHERE task_user = :user	
");

$sth->execute(array(
	'user' => 1050//$user->id
));


$result = $sth->fetchAll();


if ($result) {
	
	$datagrid = array();
	
	foreach ($result as $row) {
		$task = $row['task_id'];
		$order = $row['order_number'];
		$datagrid[$order]['caption'] = $row['head'];
		$datagrid[$order]['tasks'][$task]['date'] = $row['date'];
		$datagrid[$order]['tasks'][$task]['description'] = $row['task_text'];
		$datagrid[$order]['tasks'][$task]['link'] = $row['task_url'];
		$datagrid[$order]['tasks'][$task]['logistic_coordinator'] = $row['logistic_coordinator'];
	}
	
	
	// table
	
	foreach ($datagrid as $order => $group) {
		
		// header
		//echo "<tr><td valign='top' colspan='4' class='head' >".$group['caption']."</td></tr>";
		echo "<h5 class='copmany-header' >".$group['caption']."</h5>";
		
		echo "
			<table class='table table-hover table-dashboard'>
			<thead>
				<tr>
					<th valign='top' width='5%'>&nbsp;</th>
					<th valign='top' width='15%'>Due Date</th>
					<th valign='top' width='25%'>Logistics Coordinator</th>
					<th valign='top'>Description</th>
				</tr>
			</thead>
			<tbody>
		";

		foreach ($group['tasks'] as $task => $row) {
			
			echo "
				<tr>
					<td valign=top ><a href='/user/".$row['link']."' target='_blank'><i class='fa fa-tasks'></i></a></td>
					<td valign=top >".$row['date']."</td>
					<td valign=top >".$row['logistic_coordinator']."</td>
					<td valign=top >".$row['description']."</td>
				</tr>
			";
		}
		
		echo "</tbody></table>";
	}
}