<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$limit = $_REQUEST['limit'];
$offset = $_REQUEST['offset'];

$rows = 10;
$limit = $limit ?: 0;

// refresh content
if (isset($offset)) {
	$rows = $limit*$rows + $rows;
} 
// append content
else {
	$offset = $limit*$rows;
}

$user = User::instance();
$model = new Model();

$result = $model->query("
	SELECT SQL_CALC_FOUND_ROWS DISTINCT
		posaddress_id, 
		CONCAT(posaddress_name, ', ', place_name, ', ', country_name) as pos,
		DATE_FORMAT(posaddress_store_openingdate, '%d.%m.%Y') as date
	FROM posaddresses INNER JOIN addresses ON posaddress_client_id = address_id
	INNER JOIN countries ON country_id = posaddress_country
	INNER JOIN places ON place_id = posaddress_place_id
	WHERE 
		address_id = 265
		AND (
			posaddress_store_closingdate = NULL
			OR posaddress_store_closingdate = ''
			OR posaddress_store_closingdate = '0000-00-00'
		)
	ORDER BY posaddress_id DESC
	LIMIT $offset, $rows
")->fetchAll();

$totalRows = $model->totalRows();
$loadedRows = $offset ? $offset+$rows : $rows;

if ($result) {
	
	if ($offset==0) {
		echo "<table class='table table-hover table-dashboard'>";
		echo "<tbody class='appender-content'>";
	}
		
	foreach ($result as $row) {
		
		$id = $row['posaddress_id'];
		
		echo "
			<tr>
				<td valign=top>
					<h6><a href='/mps/pos/address/$id' target='_blank' >".$row['pos']."</a></h6>
					<span class='text-muted' >Opening Date: ".$row['date']."</span>
				</td>	
			</tr>		
		";
	}
		
	if ($offset==0) {
		echo "</tbody>";
		echo "</table>";
	}
	
}

// show more link
if (!$limit) {
	echo "<a href='#' class='appender btn btn-primary btn-xs btn-block'><i class='fa fa-caret-down'></i> more</a>";
}