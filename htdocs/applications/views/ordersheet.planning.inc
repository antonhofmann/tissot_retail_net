<?php 

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();
$request = request::instance();

// model
$model = new Model($request->application);

// current order sheet	
$ordersheet = new Ordersheet($request->application);
$ordersheet->read($request->field('ordersheet'));

// company
$company = new Company();
$company->read($ordersheet->address_id);

?>
<style type="text/css">
	
	.new-warehouse-container {
		display: none;
	}
	
	.new-warehouse-container input[type=text] {
		display: block;
		width: 354px;
		height: 20px;
		padding: 1px 2px;
		border: 1px solid silver;
	}
	
	.new-warehouse-container .error {
		border-color: red !important;
	}
	
	.new-warehouse-container small {
		display: block;
		margin: 10px 0 2px;
	}
	
	.warrning {
		color: #ff6000;
		float: right;
	}

	#lean_overlay {
	    position: fixed;
	    z-index:100;
	    top: 0px;
	    left: 0px;
	    height:100%;
	    width:100%;
	    background: #000;
	    display: none;
	}

	.addNewWarehouse { display: block; }

	.alert-spa {
		color: red;
		font-weight: 600;
		font-size: 12px;
		padding-top: 10px;
	}
	
</style>
<div class="actions">	
	<?php 

		echo ui::button(array(
			'id' => 'close',
			'icon' => 'icon151',
			'caption' => 'Close'
		));
	
		if ($buttons) {
			foreach ($buttons as $name => $button) {
				$button['id'] = $name;
				if ($name=='pop_filter') $button['class'] = $filter_active;
				echo ui::button($button);
			}
		}
	
	?>
</div>

<?php if ($companyExportSap) echo "<p class='alert-spa'>Please first check if prices for all items are defined in SAP. Wait confirming the distribution until all prices are available in SAP.</p>"; ?>
<div id='spreadsheet' class='<?php echo $class_planning; ?>'></div>
<div class="tooltips-container">
	<div class="image-loader" ><img src="/public/images/loader-quer.gif" /></div>
</div>
<?php
	
	echo $request->form();

	if ($formFilter) {
		$action = ($version) ? $request->field('url')."/$version" : $request->field('url');
		echo "<form id='filters' method=post action='$action' class=default  ><ul>";
		foreach ($formFilter as $filter => $value) {
			if ($value) echo "<li>$value</li>";
		}
		echo "</ul></form>";
	}
	
	// load all active company warehouses
	$company_warehouses = $ordersheet->company()->warehouse()->load();
	$company_warehouses = _array::datagrid($company_warehouses);
	
	// load all ordersheet registred warehouses
	$ordersheet_warehouses = $ordersheet->warehouse()->load();
	
	$warehouse_dropdown = array(
		'new' => 'Add New Warehouse'
	);

	// if ordersheet containt not all warehouses
	// show dropdown warehouses on modal screen
	if (count($company_warehouses) > count($ordersheet_warehouses)) {
		foreach ($company_warehouses as $key => $row) {
			$warehouse = $ordersheet->warehouse()->read_from_warehouse($key);
			if (!$warehouse) $warehouse_dropdown[$key] = $row['address_warehouse_name'];
		}
	}
	
	$select_warehouse = "<small>Select Warehouse</small>";
	$select_warehouse .= ui::dropdown($warehouse_dropdown, array(
		'id' => 'address_warehouse_id',
		'name' => 'address_warehouse_id',
		'caption' => 'Select Company Warehouse',
		'value' => ''
	));
	
	if ($company->do_data_export_from_mps_to_sap) {
		$sapFields = "
		<div class='sap-fields' >
			<small>SAP Customer Code Name *</small>				
			<input type=text name=address_warehouse_sap_customer_number class='silver required'  />
			<small>SAP Shipto Number</small>				
			<input type=text name=address_warehouse_sap_shipto_number class='silver'  />
		</div>
		";
	}

?>

<!-- partially distribution dialog -->
<div id="partially_distribution_dialog"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" ><?php echo $translate->ordersheet_partially_distribution; ?></div>
	</div>
	<div class="ado-modal-body">
		<?php echo nl2br($translate->dialog_ordersheet_partially_distribution); ?>
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="#" >
				<span class="icon apply"></span>
				<span class="label" ><?php echo $translate->yes ?></span>
			</a>
		</div>
	</div>
</div>

<!-- remove warehose dialog -->
<div id="remove_warehouse_dialog"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" ><?php echo $translate->warehouse; ?></div>
	</div>
	<div class="ado-modal-body">
		<?php echo nl2br($translate->dialog_warehouse); ?>
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="#" >
				<span class="icon apply"></span>
				<span class="label" ><?php echo $translate->delete ?></span>
			</a>
		</div>
	</div>
</div>

<!-- confirm quantities dialog -->
<div id="confirm_dialog"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" ><?php echo $translate->confirm_ordersheet_planning; ?></div>
	</div>
	<div class="ado-modal-body">
		<?php echo nl2br($translate->dialog_confirm_ordersheet_planning); ?>
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="#" >
				<span class="icon apply"></span>
				<span class="label" ><?php echo $translate->yes ?></span>
			</a>
		</div>
	</div>
</div>

<!-- activate order sheet dialog -->
<div id="activateDialog"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" ><?php echo $translate->activate_ordersheet; ?></div>
	</div>
	<div class="ado-modal-body">
		<?php echo nl2br($translate->dialog_activate_ordersheet); ?>
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="#" >
				<span class="icon apply"></span>
				<span class="label" ><?php echo $translate->yes ?></span>
			</a>
		</div>
	</div>
</div>

<!-- add new warehouse -->
<div id="add_warehouse_dialog"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" ><?php echo $translate->warehouse; ?></div>
	</div>
	<div class="ado-modal-body">
		<form method="post" action="/applications/helpers/ordersheet.warehouse.php" id="warehouseForm" >
			<?php echo  $select_warehouse ?>
			<span class="new-warehouse-container">
				<small>Warehouse Name *</small>
				<input type="text" name="address_warehouse_name" class="required" placeholder="Warehouse Name"  />
				<?php echo $sapFields ?>
			</span>
		</form>
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="#" >
				<span class="icon apply"></span>
				<span class="label" ><?php echo $translate->create ?></span>
			</a>
		</div>
	</div>
</div>

<!-- add new warehouse -->
<div id="export-reset"  class="ado-modal ado-box bootstrap">
	<div class="ado-modal-header">
		<div class="ado-title" >Reset Exported Orders</div>
		<div class="ado-subtitle"></div>
	</div>
	<div class="ado-modal-body"></div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a href="#" class="btn btn-sm btn-default pull-left sap-reset-close">
				Close
			</a>
			<a href="#" class="btn btn-sm btn-danger pull-right sap-reset sap-reset-all" data-action="all">
				<i class="fa fa-refresh"></i>
				Reset All
			</a>
		</div>
		<br class="clearfix" />
	</div>
</div>
