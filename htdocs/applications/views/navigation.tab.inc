<?php 

	$request = request::instance();
	$menu = menu::instance();
	$tabs = $menu->data[$request->parent];

	if (count($tabs) > 1) { 

		$systemRequest = url::system_request();
		$param = url::param();

		if ($queryParam) {
			$param = $param ? $param.'/'.$queryParam : $queryParam;
		}
	
		echo '<div class="tabs navigation"><ul>';
	
		foreach ($tabs as $id => $row) {
				
			if ($row['tab'] && $row['active']) {
				
				$url = $row['url'];
				$caption = $row['caption'];
				
				$visible = (in_array($url, $request->excluded)) ? false : true;
				$classname = str_replace('/', '-', $url);
				$active = $systemRequest==$url ? "active" : null;
						
				
				if ($visible) {
					$icon = $data[$url]['icon'];
					$class = $data[$url]['class'];
					$caption = ($data[$url]['caption']) ? $data[$url]['caption'] : $caption;
					$name = ($data[$url]['name']) ? 'name='.$data[$url]['name'] : null;
					echo "<li class='$active $classname'>";
					echo "<a href='/$url/$param' class='$active $class' $name >$icon<b>$caption</b></a>";
					echo "</li>";
				}
			}
		}
	
		echo "</ul></div>";
	}

?>