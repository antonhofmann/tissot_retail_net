<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$model = new Model($application);
	
	$form = new Form(array(
		'id' => 'ordersheet',
		'method' => 'post'
	));
	
	$form->address_company(
		Form::TYPE_TEXT,
		Form::PARAM_SHOW_VALUE
	);
	
	$form->mps_mastersheet_name(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE
	);
	
	$form->mps_ordersheet_item_order_date(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE		
	);
	
	$form->mps_ordersheet_item_purchase_order_number(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE		
	);
	
	$form->mps_ordersheet_item_desired_delivery_date(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE
	);
	
	$form->address_mps_shipto(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE
	);
	
	$form->fieldset('ordersheet', array(
		'address_company',
		'mps_mastersheet_name',
		'mps_ordersheet_item_order_date',
		'mps_ordersheet_item_purchase_order_number',
		'mps_ordersheet_item_desired_delivery_date',
		'address_mps_shipto'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['print']) {
		$form->button(ui::button(array(
			'id' => 'export',
			'icon' => 'print',
			'href' => $buttons['print'],
			'label' => $translate->print
		)));
	}
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'label' => $translate->delete,
			'href' => $buttons['delete']
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#ordersheet");

		// dialog confirmation
		$('.dialog').click(function(event) {
			
			event.preventDefault();

			var button = $(this);
			
			$('#apply, a.apply').attr('href', button.attr('href'));
			
			retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
		});

		$('#cancel, #versionCancel').bind('click', function(event) {
			event.preventDefault();
			retailnet.modal.close();
			return false;
		});	

		$('#apply, #versionApply').click(function(event) {
			if ($(this).hasClass('disabled')) {
				return false;
			}
		})
	});
</script>
<style type="text/css">

	#ordersheet { 
		width: 700px; 
		float: left;
	}

</style>
<?php 
	echo $form;
?>
