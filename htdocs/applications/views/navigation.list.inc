<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();
	$_REQUEST = session::filter($application, 'navigation', true);
?>
<style type="text/css">

	#navigations_list {
		width: 500px;
	}
  
	ul.dynatree-container {
		width: 480px;
		border: 1px solid silver;
		padding: 10px;
	}
  
	#treeToggler {
		float: right;
	}
  
</style>
<script type="text/javascript">
$(function(){

	var tree = $("#tree").dynatree({
		initAjax: {
			url: '/applications/helpers/navigation.ajax.php?section=json'
		},
		onActivate: function(node, event) { 
	        if( node.data.href ) {
	        	window.location = node.data.href;
			}
	    },
		dnd: {
			onDragStart: function(node) {
				/** This function MUST be defined to enable dragging for the tree.
				 *  Return false to cancel dragging of node.
				 */
				return true;
			},
			onDragStop: function(node) {
				// This function is optional.
			},
			autoExpandMS: 1000,
			preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
			onDragEnter: function(node, sourceNode) {
				/** sourceNode may be null for non-dynatree droppables.
				 *  Return false to disallow dropping on node. In this case
				 *  onDragOver and onDragLeave are not called.
				 *  Return 'over', 'before, or 'after' to force a hitMode.
				 *  Return ['before', 'after'] to restrict available hitModes.
				 *  Any other return value will calc the hitMode from the cursor position.
				 */
				return true;
			},
			onDragOver: function(node, sourceNode, hitMode) {

			},
			onDrop: function(node, sourceNode, hitMode, ui, draggable) { 
				
				var parent = node.getParent();
				var children = parent.getChildren();
				var i = 1;
				
				sourceNode.data.parent = node.data.parent;
				sourceNode.move(node, hitMode);
				sourceNode.expand(true);

				var order = {};
				var i = 1;
				
				if (children) {
					$.each(children, function(index, child) { 
						child.data.order = i;
						order[index] = { 
							id : child.data.key, 
							order: child.data.order
						}
						i++;
					});
				}
				
				$.ajax({
					url: '/applications/helpers/navigation.update.php',
					dataType: 'json',
					data: {
						id: sourceNode.data.key,
						parent: sourceNode.data.parent,
						order: order
					},
					success: function(json) { 
						$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
						if (json && json.message) {
							var theme = (json.response) ? 'message' : error;
							$.jGrowl(json.message, { 
								sticky: false, 
								theme: theme
							});
						}
					}
				});
			},
			onDragLeave: function(node, sourceNod) {
	
			}
		}
	});

	$("#treeToggler").click(function(){

		var collapse = ($(this).hasClass('collapse')) ? false : true;
		$(this).toggleClass('collapse');
		$('.icon',this).toggleClass('direction-up');

		$("#tree").dynatree("getRoot").visit(function(node){
			node.expand(collapse);
		});

		return false;
	});
});
</script>
<?php 
	echo $request->form($requestFields);
?>
<div id="navigations_list">
	<div class='table-toolbox'>
	<form class=toolbox method=post>
		<?php 
		
			echo ui::button(array(
				'id' => 'page-add',
				'icon' => 'icon120',
				'href' => $requestFields['add'],
				'label' => $translate->add_new
			));
			
			echo ui::button(array(
				'id' => 'treeToggler',
				'class' => '-add -expande',
				'icon' => 'direction-down',
				'label' => $translate->toggle_expand
			));
		?>
		</form>
	</div>
	<div id="tree"></div>
</div>