<?php 
	$request = request::instance();
?>
<script src="/public/scripts/pop/pop.js" type="text/javascript"></script>
<link href="/public/scripts/pop/pop.css" rel="stylesheet" type="text/css">
<link href="/public/scripts/dropdown/dropdown.css" rel="stylesheet" type="text/css" />
<script src="/public/scripts/dropdown/dropdown.js" type="text/javascript"  ></script>
<link rel="stylesheet" type="text/css" href="/public/scripts/fancybox/fancybox.css" />
<script type="text/javascript" src="/public/scripts/fancybox/fancybox.js"  ></script>
<link rel="stylesheet" type="text/css" href="/public/scripts/loader/loader.css" />
<script type="text/javascript" src="/public/scripts/loader/loader.js"  ></script>
<script type="text/javascript" src="/public/scripts/content.loader.js" ></script>
<script type="text/javascript" src="/public/scripts/search.box.js" ></script>
<script type="text/javascript">
$(document).ready(function() {
	
	var posList = $('#posindex').contentLoader({
		url: '/applications/helpers/pos.list.test.php',
		filters: '.request, #filters',
		after: function(self) {

			
		}
	});

	var filters = $('#filters').submit(function(event) {
		event.preventDefault();
		posList.load();
		return false;
	});

	$('.submiter').change(function() {
		filters.submit();
	});

	var search = $('.searchbox').searchBox();	
});
</script>
<style type="text/css">
	
	#posindex {
		max-width: 1800px;
	}
	
	.pdf { 
		cursor: pointer; 
	}
	
	select.selectbox {
		margin-left: 10px;
	}
	
	#filters {
		min-width: 220px;
	}
	
	#printlist {
		width: 180px;
	}
	
</style>
<div class="table-toolbox">
<form id="filters" name=filters >
<?php 
	echo ui::searchbox();
?>
</form>
</div>
<?php 
	echo $request->form($requestFields);
?>
<div id="posindex"></div>