<?php
	
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = Request::instance();

	$form = new Form(array(
		'id' => 'userform',
		'action' => '/applications/helpers/user.save.php',
		'method' => 'post',
		'class' => ($mycompany) ? 'mycompany' : null
	));
	
	$form->user_id(
		Form::TYPE_HIDDEN
	);

	if ($request->action=='invitation') {
		$form->invitation_email(
			Form::TYPE_HIDDEN
		);
	}

	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);

	$form->redirect(
		Form::TYPE_HIDDEN
	);

	$form->used_roles(
		Form::TYPE_HIDDEN
	);

	if ($dataloader['company'] && $data['controller']=='users' && $data['user_id']) {

		$form->company(
			Form::TYPE_SELECT,
			Validata::PARAM_REQUIRED,
			'label=Company'
		);

		$form->fieldset('Company', array('company'));
	}
	
	$form->user_address(
		$data['action']=='invitation' ? Form::TYPE_SELECT : Form::TYPE_HIDDEN,
		Validata::PARAM_REQUIRED,
		'label=Company'
	);


	$form->user_firstname(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);

	$form->user_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);

	$form->user_description(
		Form::TYPE_TEXTAREA,
		'param=label'
	);

	if ($mycompany) {
		$form->user_description(Validata::PARAM_REQUIRED);
	}

	$form->user_sex(
		Form::TYPE_RADIO,
		'param=label'
	);

	if ($mycompany) {
		$form->user_sex(Validata::PARAM_REQUIRED);
	}
	
	// fieldset: personal data
	$form->fieldset('personal_data', array(
		'user_address',
		'user_firstname',
		'user_name',
		'user_sex',
		'user_description'
	));

	$form->user_phone(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);

	if ($mycompany) {
		$form->user_phone(Validata::PARAM_REQUIRED);
	}

	if (!$disabled['user_phone']) {
		$form->extended('user_phone',"
			<div class='input-mask-container phone-container'>
				<input type='text' value='".$data['user_phone_country']."' class='input-mask phone-mask required' name='user_phone_country' id='user_phone_country' maxlength='6' placeholder='Country code'>
				<input type='text' value='".$data['user_phone_area']."' class='input-mask phone-mask required' name='user_phone_area'  id='user_phone_area' maxlength='6' placeholder='Area'>
				<input type='text' value='".$data['user_phone_number']."' class='input-mask phone-mask required' name='user_phone_number'  id='user_phone_number' maxlength='20' placeholder='Phone number'>
			</div>
		");
	}

	$form->user_mobile_phone(
		Form::TYPE_TEXT
	);

	if (!$disabled['user_mobile_phone']) {
		$form->extended('user_mobile_phone',"
			<div class='input-mask-container phone-container'>
				<input type='text' value='".$data['user_mobile_phone_country']."' class='input-mask phone-mask' name='user_mobile_phone_country' id='user_mobile_phone_country' maxlength='6' placeholder='Country code'>
				<input type='text' value='".$data['user_mobile_phone_area']."' class='phone-mask' name='user_mobile_phone_area'  id='user_mobile_phone_area' maxlength='6' placeholder='Area'>
				<input type='text' value='".$data['user_mobile_phone_number']."' class='input-mask phone-mask' name='user_mobile_phone_number'  id='user_mobile_phone_number' maxlength='20' placeholder='Mobile phone number'>
			</div>
		");
	}

		
	$form->user_email(
		Form::TYPE_TEXT,
		Form::TOOLTIP_EMAIL,
		Validata::PARAM_EMAIL,
		Validata::PARAM_REQUIRED
	);
	
	$form->user_email_cc(
		Form::TYPE_TEXT,
		Form::TOOLTIP_EMAIL,
		Validata::PARAM_EMAIL
	);
	$form->user_email_deputy(
		Form::TYPE_TEXT,
		Form::TOOLTIP_EMAIL,
		Validata::PARAM_EMAIL
	);
	
	// fieldset: Communication
	$form->fieldset('communication', array(
		'user_phone',
		'user_mobile_phone',
		'user_email',
		'user_email_cc',
		'user_email_deputy'
	));

	if (!$mycompany) {
		
		$form->user_login(
			Form::TYPE_TEXT,
			Validata::PARAM_REQUIRED
		);
		
		$form->user_password(
			Form::TYPE_TEXT,
			Validata::PARAM_REQUIRED
		);
		
		// fieldset: Security
		$form->fieldset('security', array(
			'user_login',
			'user_password'
		));
	}

	$form->roles(
		Form::TYPE_CHECKBOX_GROUP
	);
	
	// fieldset: Roles
	$legend = $request->action=='invitation' || $request->controller=='mycompany' ? 'Roles' : 'Roles';
	$form->fieldset($legend, array('roles'));

	$form->lps_roles(
		Form::TYPE_CHECKBOX_GROUP
	);
	
	// fieldset: Roles
	$form->fieldset('LPS Roles', array(
		'lps_roles'
	));

	$form->news_roles(
		Form::TYPE_CHECKBOX_GROUP
	);
	
	// fieldset: news roles
	$form->fieldset('Gazette', array(
		'news_roles'
	));

	if ($mycompany) {
		$form->user_active(
			Form::TYPE_HIDDEN
		);
	} else {
		
		if ($dataloader['other_roles']) {
			
			$form->other_roles(
				Form::TYPE_CHECKBOX_GROUP,
				Form::PARAM_DISABLED
			);
			
			// fieldset: Roles
			$form->fieldset('other_roles', array(
				'other_roles'
			));
		}

		$form->user_active(
			Form::TYPE_CHECKBOX
		);
		
		// fieldset: others
		$form->fieldset('state', array(
			'user_active'
		));
		
		if ($dataloader['other_roles']) {
			$form->user_active(
				Form::PARAM_DISABLED
			);
		}
	}

 	//set mandatory fields
 	if (is_array($mandatory) ){
 		foreach($mandatory as $field) {
			$form->validata[$field][] = 'required';
 		}
 	}

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}

	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}

	if ($buttons['back']) {
		$form->button(ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => $translate->back
		)));
	}
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['send_login_data']) {
		$form->button(ui::button(array(
			'icon' => 'mail',
			'id' => 'send_login_data',
			'label' => $translate->send_login_data
		)));
	}
	
	if ($buttons['save']) {
		
		$sendmail = null;
		
		if ($request->controller=='mycompany' && $request->action<>'invitation') {
			$sendmail = $data['user_id'] ? null : 'sendmail';
		}


		if ($request->controller=='mycompany') {
			$label = $data['user_id'] ? $translate->save : $translate->send_request;
		}
		else $label = $translate->save;

		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'user_save',
			'class' => $sendmail,
			'label' => $label
		)));
	}

	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();

?>
<style type="text/css">

	#user_description {
		width: 440px;
		height: 40px;

	}

	.user_phone, .user_mobile_phone { 
		position: relative; 
	}
	
	.user_phone .-element:not(.-disabled), 
	.user_mobile_phone .-element:not(.-disabled) { 
		display: none !important;
	}

	.user_phone .-tooltip, 
	.user_mobile_phone .-tooltip {
		position: absolute; left: 500px;
	}

	.input-mask-container { display: inline-block; }
	.input-mask-container input {width: auto;}
	.phone-mask:nth-child(1) { width: 45px!important; }
	.phone-mask:nth-child(2) { width: 40px!important; }
	.phone-mask:nth-child(3) { width: 180px!important; }

</style>
<?php 
	
	echo $form;
	
	if ($buttons['delete']) {
		echo ui::dialogbox(array(
			'id' => 'delete_dialog',
			'title' => $translate->delete,
			'content' => $translate->dialog_delete_record,
			'buttons' => array(
				'cancel' => array(
					'icon' => 'cancel',
					'label' => $translate->cancel
				),
				'apply' => array(
					'icon' => 'apply',
					'label' => $translate->yes
				),
			)
		));
	}
?>

<script type="text/javascript" src="/public/scripts/tinymce4/js/tinymce/tinymce.min.js"></script>

<!-- mail template open user account -->
<div id="accountMailContainer" class="ado-modal ado-box">
	<div class="ado-modal-header">
		<div class="ado-title">Request to Open Account</div>
		<div class="ado-subtitle"></div>
	</div>
	<div class="ado-modal-body">
		<form method="post" action="/applications/modules/user/sendmail.php" >
			<input type="hidden" name="application" value="<?php echo $request->application ?>" />
			<input type="hidden" name="controller" value="<?php echo $request->controller ?>" />
			<input type="hidden" name="user" class="user-id" />
			<input type="hidden" name="redirect" value="<?php echo $data['redirect'] ?>" />
			<input type="hidden" name="mail_template_id" value="12" class="mail-id" />
			<input type="hidden" name="mail_template_view_modal" class="modal-trigger" />
			<div class="ado-row">
				<input type=text name="mail_template_subject" class="ado-modal-input mail-subject required" />
			</div>
			<div class="ado-row">
				<textarea name="mail_template_text" id="mailAccountContent" class="ado-modal-input required mail-content" data-height="460"  ></textarea>
			</div>
		</form>
	</div>
	<div class="ado-modal-footer" >
		<div class="ado-row ado-actions">
			<a class="button cancel ado-modal-close">
				<span class="icon cancel"></span>
				<span class="label"><?php echo $translate->cancel ?></span>
			</a>
			<a class="button test-mail" href="#">
				<span class="icon mail"></span>
				<span class="label">Test Mail</span>
			</a>
			<a class="button preview-mail" href="/applications/modules/user/sendmail.preview.php">
				<span class="icon icon84"></span>
				<span class="label">Preview</span>
			</a>
			<a class="button sendmail" href="#">
				<span class="icon mail"></span>
				<span class="label"><?php echo $translate->sendmail ?></span>
			</a>
		</div>
	</div>
</div>

<!-- mail template modal -->
<div id="loginMailContainer" class="ado-modal ado-box">
	<div class="ado-modal-header">
		<div class="ado-title"><?php echo $translate->send_login_data; ?></div>
		<div class="ado-subtitle"><?php echo $data['user_firstname'].' '.$data['user_name']; ?></div>
	</div>
	<div class="ado-modal-body">
		<form method="post" id="loginMailForm" action="/applications/modules/user/sendmail.php" >
			<input type="hidden" name="application" value="<?php echo $request->application ?>" />
			<input type="hidden" name="controller" value="<?php echo $request->controller ?>" />
			<input type="hidden" name="redirect" value="<?php echo $loginData['redirect'] ?>" />
			<input type="hidden" name="user" class="user-id" value="<?php echo $data['user_id'] ?>" />
			<input type="hidden" name="mail_template_id" class="mail-id" value="<?php echo $loginData['mail_template_id'] ?>" />
			<input type="hidden" name="mail_template_view_modal" class="modal-trigger" value="<?php echo $loginData['mail_template_view_modal'] ?>" />
			<div class="ado-row">
				<input type=text name="mail_template_subject" class="ado-modal-input required mail-subject" value="<?php echo $loginData['mail_template_subject'] ?>" />
			</div>
			<div class="ado-row">
				<textarea name="login_data_cc" id="login_data_cc" class="cc ado-modal-input mail-cc" placeholder="cc recipients (comma separated):"><?php echo $loginData['login_data_cc'] ?></textarea>
			</div>
			<div class="ado-row">
				<textarea name="mail_template_text" id="mailLoginContent" class="ado-modal-input required mail-content" data-height="460" ><?php echo $loginData['mail_template_text'] ?></textarea>
			</div>
		</form>
	</div>
	<div class="ado-modal-footer" >
		<div class="ado-row ado-actions">
			<a class="button cancel ado-modal-close">
				<span class="icon cancel"></span>
				<span class="label"><?php echo $translate->cancel ?></span>
			</a>
			<a class="button test-mail" href="#">
				<span class="icon mail"></span>
				<span class="label">Test Mail</span>
			</a>
			<a class="button preview-mail" href="/applications/modules/user/sendmail.preview.php">
				<span class="icon icon84"></span>
				<span class="label">Preview</span>
			</a>
			<a class="button sendmail" href="#">
				<span class="icon mail"></span>
				<span class="label"><?php echo $translate->sendmail ?></span>
			</a>
		</div>
	</div>
</div>

<!-- preview mail modal -->
<div id="preview-mail-template" class="ado-modal ado-box">
	<div class="ado-modal-header">
		<span class="fa-stack ado-modal-close">
			<i class="fa fa-circle fa-stack-2x"></i>
			<i class="fa fa-times fa-stack-1x fa-inverse"></i>
		</span>
		<div class="ado-row ado-title mail-subject"></div>
		<div class="ado-row" >
			<span class="ado-label ado-width-30">To:</span>
			<span class="ado-label mail-address"></span>
		</div>			
		<div class="ado-row">
			<span class="ado-label ado-width-30">CC:</span>
			<span class="ado-label mail-cc-address"></span>
		</div>
		<div class="ado-notification-right-bottom">
			<span class="ado-label mail-date"></span>
		</div>
	</div>
	<div class="ado-modal-body ado-max-height-500 mail-content"></div>
	<div class="ado-modal-footer">
		<div class="mail-footer"></div>
	</div>
</div>

<!-- test mail modal dialog -->
<div id="test-mail-template"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" >Send Test E-mail an:</div>
	</div>
	<div class="ado-modal-body">
		<input type="text" name="test-mail-recipient" id="test-mail-recipient" class="ado-modal-input required" value="<?php echo $user->email ?>" placeholder="Recipient E-mail" />
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="/applications/modules/user/sendmail.php" >
				<span class="icon mail"></span>
				<span class="label" ><?php echo $translate->send ?></span>
			</a>
		</div>
	</div>
</div>