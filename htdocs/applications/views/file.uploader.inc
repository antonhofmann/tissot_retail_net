<div class="adomat modalbox-container">
	<div id="fileUploader" class="adomat modalbox bootstrap fileuploader <?php echo $class ?>"  <?php echo _array::rend_attributes($attributes) ?>  >
		<form method="POST" enctype="multipart/form-data" action="<?php echo $action ?>" >
			<?php 
				if ($fields) {
					foreach ($fields as $k => $v) {
						echo "<input type=hidden name='$k' class='$k' value='$v' >";
					}
				}
			?>	
			<div class=modalbox-header >
				<div class='title'><?php echo $title ?></div>
				<div class='subtitle'><?php echo $subtitle ?></div>
				<div class='modal-infotex'><?php echo $infotext ?></div>
			</div>
			<div class="modalbox-content-container" >
				<div class="modalbox-content" >
					<!-- The table listing the files available for upload/download -->
					<table role="presentation" class="table table-striped">
						<tbody class="files"></tbody>
					</table>
				</div>
			</div>
			<div class="modalbox-footer" >
				<div class="modalbox-actions fileupload-buttonbar" >
					<span class="btn btn-default btn-sm modal-close pull-left">
						<span>Close</span>
					</span>
					<span class="btn btn-sm btn-success fileinput-button">
						<i class="fa fa-plus"></i>
						<span>Add Files</span>
						<input type="file" name="files[]" multiple>
					</span>
					
					<button type="button" class="btn btn-sm btn-primary uploader">
						<i class="fa fa-arrow-circle-up"></i>
						<span>Submit</span>
					</button>	
				</div>
			</div>
	    </form>
	</div>
</div>
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
<!-- The template to display files available for upload -->
<script id="retailnet-template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td width="80px" align="center" >
            <span class="preview"></span>
        </td>
		<td>
			<input type="title[]" class="form-control input-sm required" value="{%=file.title%}" placeholder="File Title">
			<span class="help-block">
				{% if (!i && !o.options.autoUpload) { %}
                	<button class="btn btn-xs btn-primary start chankuploader" disabled>
                    	<i class="glyphicon glyphicon-upload"></i>
                    	<span>Upload</span>
                	</button>
            	{% } %}
				{% if (!i) { %}
                	<button class="btn btn-xs btn-warning cancel">
                    	<i class="fa fa-minus-circle"></i>
                    	<span>Cancel</span>
                	</button>&nbsp;
            	{% } %}
				<span class="name">{%=file.name%}</span>&nbsp;
				<span class="size">Processing...</span>
			</span>
			<strong class="error text-danger"></strong>
			<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
		</td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="retailnet-template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td width="80px" align=center >
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.title%}" download="{%=file.name%}" {%=file.gallery ? 'data-gallery':''%} ><img src="{%=file.thumbnailUrl%}" style="max-width:80px"></a>
                {% } %}
            </span>
        </td>
		<td>
			<div class="input-group input-group-sm">
				<span class="input-group-addon">File Title</span>
				<input type="text" name="title[{%=file.id%}]" class="form-control input-sm required file-title" value="{%=file.title%}" placeholder="File Title">
			</div>
			<input type="hidden" name="file[{%=file.id%}]" class="form-control input-sm required" value="{%=file.url%}">
			<span class="help-block">
				<span class="filename name">
					{% if (file.url) { %}
            			<a class="btn btn-xs btn-info" href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.gallery?'data-gallery':''%}>
							<i class="fa fa-file"></i>
                    		<span>{%=file.gallery ? 'Show File' : 'Download File' %}</span>
						</a>&nbsp;
            		{% } %}
					{% if (file.deleteUrl) { %}
                		<button class="btn btn-xs btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                	    	<i class="fa fa-times-circle"></i>
                 	   		<span>Delete</span>
                		</button>
          	  		{% } else { %}
                		<button class="btn btn-xs btn-warning cancel">
                    		<i class="fa fa-minus-circle"></i>
                    		<span>Cancel</span>
                		</button>
						{%=file.name%}
            		{% } %}
					&nbsp;<span class="size">{%=o.formatFileSize(file.size)%}</span>
				</span>
			</span>
			{% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
		</td>
    </tr>
{% } %}
</script>