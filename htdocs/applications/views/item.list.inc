<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#items').tableLoader({
		url: '/applications/helpers/item.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			$('select',self).dropdown();

			var dataindex = [];

			$("input.edit-price").each(function() {
				
				var index = $(this).attr('index');
				var value = $(this).val();
				var section = $(this).data('section');

				value = value ? Number(value) : '';
				
				if (dataindex[section]) dataindex[section][index] = value;
				else {
					dataindex[section] = [];
					dataindex[section][index] = value;
				}
				
			})

			$('input.edit-price').on('change', function(e) {

				e.stopPropagation();

				var self = $(this);
				var row = self.closest('tr');
				var value = Number(self.val());
				var id = self.attr('index');
				var section = self.data('section');
				var oldValue = dataindex[section] ? dataindex[section][id] : 0;
				var error = '';

				if (isNaN(value)) {
					error = "Please only integer numbers";
				}

				if (error) {

					if (!isNaN(oldValue)) {
						self.val(oldValue.toFixed(2));
					}
					
					retailnet.notification.error(error);
					
					return;
				}

				$.ajax({
					url: "/applications/helpers/item.ajax.php", 
					type: "POST",
					data : {
						section: section,
						id: id,
						value: value
					},
					dataType : "json",
					success : function (xhr) {

						if (xhr && xhr.success) {
							dataindex[section][id] = value;
							self.val(value.toFixed(2))
						}
						
						if (xhr && xhr.errors) {

							self.val(oldValue ? oldValue.toFixed(2) : '');

							$.each(xhr.errors, function(i, msg) {
								retailnet.notification.error(msg);
							})

							return;
						}

						// update item price
						if (xhr.item_price) {
							var itemPrice = $('.item_price', row);
							if (itemPrice.is('input')) itemPrice.val(xhr.item_price);
							else itemPrice.text(xhr.item_price);
						}

						if (xhr.price) {
							self.val(xhr.price)
						}
					}
				})

				return;
			})

			$('input.edit-price').on('keyup', function(e) {
				
				if (e.which == 13) {
					
					e.stopPropagation();

					var self = $(this);
					var id = self.attr('index');
					var section = self.data('section');
					var oldValue = dataindex[section] ? dataindex[section][id] : 0;
					
					if (self.val() != oldValue) {
						//self.trigger('change');
					}

					var nextRow = self.closest('tr').next('tr');
					var className = self.attr('class').split(' ')[0];
					$('input.'+className, nextRow).focus();
					
					return e.keyCode != 13;
				}
			})

			$('#selectPrint').on('change', function(e) {

				e.preventDefault();
				e.stopPropagation();

				var val = $(this).val();

				if (!val) return;

				switch (val) {

					case 'booklet':

						var data = $('form.toolbox').serializeArray();

						$.fancybox({
							type: 'iframe',
							href: '/applications/templates/item.booklet.php?'+$.param(data),
							iframe : {
						        preload: false
						    },
							autoSize	: false,
							fitToView   : true,
						    width       : '800px',
						    height      : '640px',
						    margin 		: 0,
						    padding 	: 0,
							modal 		: true
						})

					break;

					case '/applications/modules/item/print.php':

						if ($('input.export-field').length) {

							$('#modal-printbox-items .button.apply').data('url', val);
							
							retailnet.modal.show('#modal-printbox-items', {
								autoSize	: false,
								closeClick	: false,
								closeBtn	: false,
								fitToView   : true,
							    width       : '90%',
							    height      : '90%',
							    maxWidth	: '520px',
							    maxHeight	: '520px',
								modal 		: true
							});
							
						} else {
							printItems(val);
						}

					break;

					default:
						
						printItems(val);
						
					break;
				}

				$(this).val('').trigger('change');

				return;
			})

			var itemsExportbox = $('#modal-printbox-items');
			var selectAllFields = $('.select-all input', itemsExportbox);
			var exportFields = $('input.export-field', itemsExportbox);

			$('.button.apply', itemsExportbox).click(function(e) { 

				e.preventDefault();
				e.stopImmediatePropagation();

				var self = $(this);

				if (exportFields.filter(':checked').length > 0) {
					printItems(self.data('url'));
					$.fancybox.close();
				} else {
					retailnet.notification.error('Please, select at least one Column for export.');
				}
			
				return false;
			});	

			$('.button.cancel', itemsExportbox).click(function(e) {
				e.preventDefault();
				$.fancybox.close();
				return false;
			});

			selectAllFields.on('change', function(e, manually) {
				
				var self = $(this);
				
				if (manually) {
					var checked = exportFields.length == exportFields.filter(':checked').length ? true : false;
					self.prop('checked', checked);
				}
				else {
					var checked = self.is(':checked') ? true : false;
					exportFields.attr('checked', checked);
				}
				
				var caption = self.is(':checked') ? 'Deselect All' : 'Select All'; 
				self.next('span').text(caption);
			});

			exportFields.on('change', function(e) {
				selectAllFields.trigger('change', [true]);
			});	
		}
	})

	function printItems(val) {
		
		retailnet.loader.show();
		
		retailnet.ajax.json(val, $('form.toolbox, #modal-printbox-items form').serialize()).done(function(xhr) {
			
			xhr = xhr || {}

			if (xhr.errors && xhr.errors.length) {
				
				$.each(xhr.errors, function(i, message) {
					retailnet.notification.error(message);
				})
				
				return;
			}

			if (xhr.success && xhr.file) {
				
				// wait until file is generated
				setTimeout(function() {
					
					var check = retailnet.ajax.request(xhr.file+'/check');
					
					if (check.success && check.filesize > 0) {
						window.open(xhr.file);
						//window.close();
					}
					
				}, 2000);
			}
			
		}).complete(function(xhr, responseText) {
			setTimeout(function(){
        		retailnet.loader.hide();
        	}, 2000);
		});
	}
})
</script>
<style type="text/css">

	#items { width: 1600px; }
	#items.furniture { width: 1200px; }
	a.item-files-modal {font-size: 18px; color: #606060 !important;}
	a.item-files-modal:hover {color: #000 !important;}
	input.item_price {width: 80px; text-align:right; }
	td.supplier_item_price {border-right-width: 0 !important; padding-right: 0 !important; }
	input.supplier_item_price {width: 80px; text-align:right; border-right-width: 0; padding-right: 0; }
	.dropdown-selectPrint {margin:0;}

	#items { width: 1600px; }
	#items.furniture { width: 1200px; }
	a.item-files-modal {font-size: 18px; color: #606060 !important;}
	a.item-files-modal:hover {color: #000 !important;}
	input.item_price {width: 80px; text-align:right; }
	td.supplier_item_price {border-right-width: 0 !important; padding-right: 0 !important; }
	input.supplier_item_price {width: 80px; text-align:right; border-right-width: 0; padding-right: 0; }
	.dropdown-selectPrint {margin:0;}

	.modalbox-content p:not(.select_all)  {
		background-color: #f3f3f3;
		padding: 5px;
		margin: 0 0 5px 0;
	}

	.modalbox-content p span {
		display: inline-block;
		padding-left: 10px;
		font-size: 12px;
	}
	
	.modalbox .content-caption {
		display: block;
		font-size: 16px;
		margin: 20px 0 10px;
		color: #444;
		font-weight: 600;
	}
	
	.select_all {
		margin-bottom: 10px;
		font-weight: 600;
	}
</style>
<div id="items" class="<?php echo $namespace ?>"></div>
<?php 

	echo $request->form(); 

	// export box
	echo $printBox;
?>