<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<style type="text/css">
	
	#list {
		width: 1240px;
	}
	
	.actions {
		width: 1200px;
	}

	
</style>
<script type="text/javascript" src="/public/scripts/textarea.expander.js"></script>
<div id="list"></div>
<div class='actions'>
<?php 

		if ($buttons['back']) {
			
			echo ui::button(array(
				'id' => 'back',
				'icon' => 'back',
				'label' => $translate->back,
				'href' => $buttons['back']
			));
		}

		if ($buttons['submit_sendmail']) {
			echo ui::button(array(
				'id' => 'submit_sendmail',
				'class' => 'submit sendmail',
				'icon' => 'mail',
				'label' => $translate->submit.' & '.$translate->sendmail
			));
		}
	
		if ($buttons['submit']) {
			echo ui::button(array(
				'id' => 'submit',
				'class' => 'submit',
				'icon' => 'save',
				'label' => $translate->submit
			));
		}
		
		if ($buttons['remind']) {
			echo ui::button(array(
				'id' => 'remind',
				'class' => 'submit sendmail',
				'icon' => 'save',
				'label' => $translate->remind
			));
		}
?>
</div>
<form id="ordersheetsForm" class="request" method="post" action="/applications/helpers/ordersheet.submit.save.php">
	<input type="hidden" name="application" value="<?php echo $request->application; ?>" />
	<input type="hidden" name="controller" value="<?php echo $request->controller; ?>" />
	<input type="hidden" name="archived" value="<?php echo $request->archived; ?>" />
	<input type="hidden" name="action" id="action"  value="<?php echo $request->action; ?>" />
	<input type="hidden" name="ordersheets" id="ordersheets" />
	<input type="hidden" name="workflow_states" id="workflowStates" value="<?php echo $workflow_states ?>" />
	<input type="hidden" name="sendmail" id="sendmail" />
</form>
<div class='modalbox-container'>
	<div id="modalbox" class="modalbox modal">
		<div class=modalbox-header >
			<div class='title'><?php echo $translate->sendmail; ?></div>
			<div class='subtitle'><?php echo $modal_caption ?><strong></strong></div>
		</div>
		<div class=modalbox-content-container >
			<div class=modalbox-content>
				<form id="mailForm" action="/applications/helpers/ordersheet.sendmail.php" method="post" >
					<input type="hidden" name="mail_template_id" id="mail_template_id" value="<?php echo $maildata['mail_template_id'] ?>" >
					<input type="hidden" name="mail_template_view_modal" id="mail_template_view_modal" value="<?php echo $maildata['mail_template_view_modal'] ?>" >
					<div class="modal-input-container"><input type=text name="mail_template_subject" id="mail_template_subject" class="required" value="<?php echo $maildata['mail_template_subject'] ?>" /></div>
					<div class="modal-input-container"><textarea name="mail_template_text" id="mail_template_text" class="required expand" ><?php echo $maildata['mail_template_text'] ?></textarea></div>
				</form>
			</div>
		</div>
		<div class=modalbox-footer >
			<div class=modalbox-actions >
				<a class='button cancel'>
					<span class="icon cancel"></span>
					<span class="label"><?php echo $translate->cancel ?></span>
				</a>
				<a class='button submit-modal'>
					<span class="icon mail"></span>
					<span class="label"><?php echo $translate->sendmail ?></span>
				</a>
			</div>
		</div>
	</div>
</div>