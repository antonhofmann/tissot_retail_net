<?php 
	$user = User::instance();
	$translate = Translate::instance();
?>

<style type="text/css">

	#items { 
		width: 800px; 
	}

	#items h5 {
		margin: 25px 0 0;
		padding: 0 0 5px 0;
		color: #404040;
		font-size: 13px;
		cursor: pointer;
		border-bottom: double 3px #ccc;
	}

	#items .toggler { color: #404040; }
	
</style>
<script type="text/javascript">
	$(document).ready(function() {

		$('#items h5').on('click', function(e) {
			e.preventDefault();
			$(this).next().slideToggle();
			$('.toggler', $(this)).toggleClass('fa-caret-right').toggleClass('fa-caret-down');
		})
	})
</script>
<div id="items">
<?php 

	if ($datagrid) {

		foreach ($datagrid as $key => $row) {
			
			echo "<h5><i class='toggler fa fa-caret-down'></i> {$row[caption]}</h5>";

			$table = new Table(array('theme' => 'spreadsheet', 'class' => 'spreadsheet'));
			$table->datagrid = $row['data'];
			$table->item_code('width=20%');	
			$table->item_name();
			$table->item_price('width=10%');	
	
			echo $table->render();
		}
	}			
?>
</div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>