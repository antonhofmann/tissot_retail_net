<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#distribution_channels').tableLoader({
		url: '/applications/helpers/distribution.channel.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown()
		}
	});
	
});
</script>
<style type="text/css">

	#distribution_channels { 
		width: 800px; 
	}
	
</style>
<div id="distribution_channels"></div>
<?php echo $request->form(); ?>