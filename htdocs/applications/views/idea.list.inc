<script type="text/javascript">
$(document).ready(function() {

	$('#ideas').tableLoader({
		url: '/applications/helpers/idea.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
<style type="text/css">

	#ideas { 
		width: 1000px; 
	}
	
	.idea_title span {
		color: gray;
		display: block;
		font-size: 11px;
	}
	
	span.fa {
		margin-top: 5px;
	}
	
	span.killed {
		color: red;
	}
	
</style>
<?php 
	echo request::instance()->form();
?>
<div id="ideas"></div>