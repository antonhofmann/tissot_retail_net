<?php 

	$settings = Settings::init();
	$user = User::instance();
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	var users = $('#users').tableLoader({
		url: '/applications/helpers/user.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select', self).dropdown();
		}
	});

});
</script>
<style type="text/css">

	#users { 
		width: 1200px;
	}
	
	.pdf { 
		cursor: pointer; }
	
	.planning_roles span,
	.other_roles span,
	.roles span {
		display: block; 
		padding: 0 0 2px;
	}
	
</style>
<div id="users"></div>
<?php 
	echo $request->form();
?>
<div class='actions'>
<?php 
	if ($buttons['back']) {
		echo ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => $translate->back
		));
	}
?>
</div>