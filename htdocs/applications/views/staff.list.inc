<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('#staff').tableLoader({
		url: '/applications/helpers/staff.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">
	#staff { width: 1000px; }
</style>
<?php echo $request->form($requestFields); ?>
<div id="staff"></div>