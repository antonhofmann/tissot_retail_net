<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

});
</script>
<style type="text/css">

	#options { 
		width: 800px; 
	}
	
</style>
<div id="options">
	<div class="table-toolbox">
		<form class=toolbox >
			<?php
				if ($buttons['add'])  {
					echo ui::button(array(
						'id' => 'add',
						'icon' => 'add',
						'href' => $buttons['add'],
						'label' => $translate->add_new
					));
				}
			?>
		</form>
	</div>
	<?php
		if ($datagrid) {
			$table = new Table();
			$table->datagrid = $datagrid;
			$table->item_option_name("href=$link");
			$table->item_option_quantity();
			$table->item_code();
			$table->item_name();
			echo $table->render();
		}
	?>
</div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>