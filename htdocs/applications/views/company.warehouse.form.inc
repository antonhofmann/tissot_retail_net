<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	
	$form = new Form(array(
		'id' => 'warehouseForm',
		'action' => '/applications/helpers/company.warehouse.save.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->address_warehouse_id(
		Form::TYPE_HIDDEN
	);
	
	$form->address_warehouse_address_id(
		Form::TYPE_HIDDEN
	);
	
	$form->address_warehouse_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	
	$form->address_warehouse_sap_customer_number(
		Form::TYPE_TEXT
	);
	
	$form->address_warehouse_sap_shipto_number(
		Form::TYPE_TEXT
	);
	
	$form->address_warehouse_active(
		Form::TYPE_CHECKBOX
	);
	
	if ($data['export']) {
		$form->address_warehouse_sap_customer_number(Validata::PARAM_REQUIRED);
	}
	
	$form->fieldset('warehouse', array(
		'address_warehouse_name',
		'address_warehouse_sap_customer_number',
		'address_warehouse_sap_shipto_number',
		'address_warehouse_active'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>

<style type="text/css">

	#warehouseForm { width: 700px; }
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>