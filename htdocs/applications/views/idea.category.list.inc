<script type="text/javascript">
$(document).ready(function() {

	$('#idea_categories').tableLoader({
		url: '/applications/helpers/idea.category.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select',self).dropdown();
		}
	});
	
});
</script>
<style type="text/css">

	#idea_categories { 
		width: 400px; 
	}

</style>
<?php 
	echo request::instance()->form();
?>
<div id="idea_categories"></div>