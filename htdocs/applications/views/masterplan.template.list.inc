<script>
require([
  'jquery', 
  'apps/admin/views/masterplan_templates/masterplan',
  'apps/admin/models/masterplan_templates/masterplan'
  ], function($, MasterplanTemplatesView, Masterplan) {
    Backbone.emulateHTTP = true;
    Backbone.emulateJSON = true;

    $(document).ready(function() {
      var data = <?php print json_encode($this->viewData)?>;
      var view = new MasterplanTemplatesView({
        data: data.viewData,
        model: new Masterplan(data.masterplan)
      });
      view.render();
  });
});
</script>
<style>
#add_box .align,
#phases .phase .steps .step .colorpicker,
#phases .phase .steps .step .color,
.drag-handle,
#phases .phase .steps .step input,
#phases .phase .steps .step select { 
  vertical-align: middle; 
}

#phases .phase .steps .step select {
  height: 24px;
  width: 150px;
}

#phases abbr {
  cursor: help;
}

#phases, .subphases {
  padding: 1em 0;
}
.subphases {
  min-height: 50px;
}

#add_box .align {
  display: inline-block; 
}
#add_box input.align {
  padding-top: 4px;
  padding-bottom: 4px;
}
form.default {
  display: block;
}
.phase .head {
  font-size: .8em;
}
#phases {
  width: auto;
}
#phases .phase {
  border: 1px solid #333333;
  border-left-width: 0;
  border-right-width: 0;
  padding: 1em;
  background-color: #fafafa;
  margin-top: 1em;
}
#phases .phase.subphase {
  border-style: dotted;
}

#phases .toolbar {
  margin-top: 1em;
}
#phases .phase .steps {
  margin: 0 0 1em 0;
}
#masterplan_templates ul {
  list-style-type: none;
}
#phases .phase .steps .step {
  border-bottom: 1px dotted #848484;
  padding: .5em 0;
  min-width: 1000px;
}
#phases .phase .steps .step .step_title {
  width: 140px;
}
#phases .phase .steps .step .step_nr {
  width: 50px;
  margin-right: 10px;
}
#phases .phase .steps .step .step_type {
  width: 90px;
}
#phases .phase .steps .step .colorpicker,
#phases .phase .steps .step .color {
  width: 21px;
  height: 21px;
  display: inline-block;
}
.evo-colorind, 
.evo-colorind-ie, 
.evo-colorind-ff {
  width: 21px;
  height: 21px;
}
#phases .phase .steps .step .colorpicker {
  width: 45px;
}

.hidden { display: none; }

/* drag&drop styles */
body.dragging, body.dragging * {
  cursor: move !important;
}
.dragged {
  position: absolute;
  opacity: 0.5;
  z-index: 2000;
}
li.placeholder {
  position: relative;
  min-height: 30px;
  border: 2px dashed #B2C3DA;
}
li.placeholder:before {
  position: absolute;
}

form.default input.h2,
form.default input.h3 {
  border-color: transparent;
  font-size: 1.3em;
  height: auto;
  display: inline-block;
  /*width: 50%;*/
  vertical-align: middle;
  background-color: #fafafa;
}
form.default input.h3 {
  font-size: 1.1em;
}
form.default input.h2:hover,
form.default input.h3:hover {
  border-color: #cccccc;
}

.drag-handle {
  background: url('/public/images/drag-handle.gif') 3px 1px no-repeat;
  width: 15px;
  height: 20px;
  display: inline-block;
  visibility: hidden;
  cursor: move;
  opacity: .7;
}
.phase:hover > .drag-handle,
.step:hover .drag-handle {
  visibility: visible;
}
/* colorpicker */
.evo-pop.ui-widget {
  background-color: #fafafa;
  border: 1px solid #ccc;
  padding: 10px 3px 10px 3px;
}
.evo-pop.ui-widget .evo-palette tr:nth-last-child(2),
.evo-pop.ui-widget .evo-palette tr:first-child,
.evo-pop.ui-widget .evo-palette tr:last-child,
.evo-pop.ui-widget .evo-more {
  display: none;
}
.step.colored .colorpicker .evo-pointer {
  border: 1px solid #999999;
}

.phase .head { 
  margin: .5em 0; 
  min-width: 1000px;
}
.phase .head span { 
  display: inline-block; 
  margin-right: 20px;
}
.phase .head .spacer { width: 15px; margin-right: 0; }
.phase .step_nr { width: 54px; }
.phase .step_title { width: 140px; }
.phase .step_type { width: 70px; }
.phase .step_color { width: 20px; }
.phase .step_value { width: 150px; margin-right: 20px; }
.phase .step_role { width: 140px; }
.phase .step_apply_to abbr,
.phase .apply_to input { 
  margin-left: 10px; 
}
.phase .head .step_apply_to abbr {
  display: inline-block;
  min-width: 15px;
}
.phase .apply_to { margin-left: 10px; }
.phase .active { 
  display: inline-block;
  width: 20px;
}
form.default .phase input.step_duration {
  width: 25px;
}
.phase .duration {
  width: 75px;
  margin-left: 25px;
  display: inline-block;
}
.phase .duration .settings {
  vertical-align: middle;
  cursor: pointer;
  margin-left: 10px;
}
.phase .duration_unit {
  font-size: .8em;
}
.save_box {
  margin-top: 1em;
}
.step .icon.delete {
  cursor: pointer;
  zoom: .5;
  display: none;
  margin-left: 2em;
}
.step:hover .icon.delete {
  display: inline-block;
}

/**
 * Durations
 */
.durations-table .countries li {
  list-style-type: none;
  border-bottom: 1px solid #ccc;
  padding: .5em 0;
}
.durations-table .countries {
  margin-bottom: 1em;
}
form.default.durations-table .duration {
  float: right;
  width: 40px;
  text-align: right;
}
.durations-table .countries .duration { margin-right: 27px; }
.durations-table .caption .duration { margin-right: 10px; }
.durations-table .caption {
  font-size: 1.2em;
  border-bottom: 1px solid #333;
  font-weight: normal;
  display: block;
  padding: .5em 0;
  outline: 0;
}
.durations-table .caption .icon {
  float: right;
  cursor: pointer;
  margin-top: 3px;
}
form.default.responsible-user-picker {
  width: 100%;
  height: 100%;
  min-width: 0;
}
.responsible-user-picker select {
  width: 100%;
  height: 100%;
}
</style>

<div id="masterplan_templates">

  <div id="add_box">
    <form class="default">
      <input name="phase_title" type="text" alt="Phase title" placeholder="Phase" class="align validate[required]">
      <button id="addBtn" class="button align"><span class="icon add"></span><span class="label">Add phase</span></button>
    </form>
  </div>

  <div class="save_box">
    <button id="saveBtn" class="button align"><span class="icon save"></span><span class="label">Save</span></button>
  </div>

<form class="default">
<ul id="phases">
  
</ul>
</form>
  
</div>
