<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'staff_form',
		'action' => '/applications/helpers/staff.save.php',
		'method' => 'post'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_staff_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_staff_address_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_staff_staff_type_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_staff_firstname(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_staff_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_staff_sex(
		Form::TYPE_RADIO,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_staff_active(
		Form::TYPE_CHECKBOX
	);
	
	$form->mps_staff_email(
		Form::TYPE_TEXT,
		Form::TOOLTIP_EMAIL,
		Validata::PARAM_EMAIL
	);
	
	$form->mps_staff_phone(
		Form::TYPE_TEXT,
		Form::TOOLTIP_PHONE,
		Validata::PARAM_PHONE
	);
	
	$form->fieldset('company_data', array(
		'mps_staff_address_id', 
		'mps_staff_staff_type_id'
	));
	
	$form->fieldset('personal_data', array(
		'mps_staff_firstname',
		'mps_staff_name',
		'mps_staff_sex',
		'mps_staff_active'
	));
	
	$form->fieldset('contact', array(
		'mps_staff_email',
		'mps_staff_phone'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);  
	$form = $form->render();
?>
<script type="text/javascript" src="/public/js/staff.js"></script>

<style type="text/css">

	#staff_form { 
		width: 700px; 
	}
	
</style>
<?php 
	
	echo $form;
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>