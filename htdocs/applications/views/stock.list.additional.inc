<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'additionals',
		'action' => '/applications/helpers/stock.list.additional.save.php',
		'method' => 'post'
	));
	
	$form->scpps_stocklist_id(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->columns(
		Form::TYPE_TEXT_GROUP,
		Validata::PARAM_REQUIRED,
		'class=cloned'	
	);
	
	$form->caption('columns', $translate->caption);
	$form->fieldset('additional_columns', array('columns'));
	$form->dataloader($data);
	$form->dataloader($dataloader);
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form = $form->render();
?>
<script type="text/javascript">
	$(document).ready(function() {

		var form = $("#additionals");
		var btnAdd = '<span class="icon add insert"></span>';
		var btnRemove = '<span class="icon cancel remove"></span>';
		var stocklist = $('#scpps_stocklist_id').val();
		var application = $('#application').val();
		var request, json;

		form.submit(function(e) {
			e.preventDefault();
			return false;
		});
		
		var sendRequest = function(field, section) {

			json = $.ajax({
				url: '/applications/helpers/stock.list.ajax.php',
				data: {
					section : section,
					field : field.attr('id'),
					caption: field.val(),
					tag: field.attr('tag'),
					stocklist: stocklist,
					application: application
				},
				dataType: 'json',
				async: false
			}).responseText;

			return $.parseJSON(json);	
		}

		var showMessage = function(obj) {

			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
			
			if (obj.message) {
				$.jGrowl(obj.message,{ 
					sticky: (obj.response) ? false : true,
					life: 5000,
					theme: (obj.response) ? 'message' : 'error'
				});
			}
		}

		var createClone = function(e) {

			var row = $(e).closest('.-row');
			var field = $('.cloned', row);
			
			var num = new Number(field.attr('id').substring(6));
			var newNum = new Number(num+1);  

			var className = (num%2 == 0) ? '-odd' : '-even';
 
			// clone element
			var newElem = $('.column'+num).clone();
 			
			// manipulate the name/id values of the input inside the new element
			newElem
			.removeClass('column'+num)
			.removeClass('-even')
			.removeClass('-odd')
			.addClass('column'+newNum)
			.addClass(className)
			.find('.cloned').val('')
			.attr('id', 'column'+newNum)
			.attr('name', 'columns[column'+newNum+']');

			return newElem;
		}

		var removeClone = function(e) {

			var clones = $('.cloned').length;
			
			if (clones==1) {
				var newElem = createClone(e);
			}

			e.remove();
		
			if (newElem) {
				$('.additional_columns').append(newElem);
			}

			$('.cloned', $('.-row:last')).trigger('click').focus();
		}

		// controll textbox
		$(document).delegate('.cloned', {
			click: function(e) {

				var row = $(this).closest('.-row');
				var num = $('.cloned').length;
				var index = $(this).index('.cloned') + 1;

				// remove generated buttons
				$('.icon').remove();
				
				// insert add button
				$('.-element:last').after(btnAdd);

				// insert remove button
				if (num == index) $('.insert:last').after(btnRemove);
				else $('.-element', row).after(btnRemove);
				
			},
			change: function(e) {

				var field = $(this);
				
				if (field.val()) {
					$(this).removeClass('error');
					var request = sendRequest(field,'save_additional_column');
					if (request) showMessage(request);
					
				} else {
					$(this).addClass('error');
					showMessage({'response': false, 'message': 'Column Caption is required.'});
				}

				$('.remove').remove();
			}
		});

		// insert new element
		$(document).delegate('.insert', 'click', function() {
			var row = $(this).closest('.-row');
			var newElem = createClone(row);
			row.after(newElem);
			$('input.cloned', newElem).trigger('click').focus();

		});

		// remove generated elements
		$(document).delegate('.remove', 'click', function() {

			var row = $(this).closest('.-row');
			var field = $('.cloned', row);

			request = sendRequest(field,'check_additional_column');

			// if this field has data in weeks table
			// show dialog box
			if (request.response) {

				retailnet.modal.show('#delete_dialog', {
					closeBtn: false,
					afterClose: function() {
						$('#apply').removeAttr('href');
						$('.cloned').removeAttr('tag');
					}
				});
				
			} 
			// remove this field and set focus to last field
			else {
				request = sendRequest(field, 'remove_additional_column');
				if (request.response) removeClone(row);
				if (request) showMessage(request);
			}
		});

		$('#cancel').bind('click', function(event) {
			event.preventDefault();
			retailnet.modal.close();
			return false;
		});	

		$('#apply').bind('click', function(event) {

			event.preventDefault();

			var id = $(this).attr('href');
			var row = $('.'+id);
			var field = $('.cloned', row);
			field.attr('tag', 'cascading');

			request = sendRequest(field, 'remove_additional_column');

			if (request.response) removeClone(row);
			if (request) showMessage(request);

			retailnet.modal.close();
			showMessage(request);
		})

		// on page load trigger last field
		$('.cloned:last').trigger('click').focus();
		
	});
</script>
<style type="text/css">
	
	#additionals {
		width: 700px;
	}
	
	input.cloned {
		margin-right: 10px;
	}
	
</style>
<?php 
	
	echo $form;
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>