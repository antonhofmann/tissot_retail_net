<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'pos_furniture',
		'action' => '/applications/helpers/pos.furniture.save.php',
		'method' => 'post'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_pos_furniture_posaddress(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_pos_furniture_id(
		Form::TYPE_HIDDEN
	);
	
	if ($section=='customized') {
		
		$form->mps_pos_furniture_description(
			Form::TYPE_TEXTAREA,
			Form::PARAM_LABEL,
			Validata::PARAM_REQUIRED
		);
		
		$form->fieldset('furniture', array(
			'mps_pos_furniture_description'
		));
	} 
	else {
		
		$form->mps_pos_furniture_product_line_id(
			Form::TYPE_SELECT,
			Validata::PARAM_REQUIRED
		);
		
		$form->mps_pos_furniture_item_id(
			Form::TYPE_SELECT,
			Validata::PARAM_REQUIRED,
			Form::PARAM_AJAX
		);
		
		$form->old_items(
			Form::TYPE_SELECT,
			Form::PARAM_AJAX
		);
		
		$form->caption('old_items', "Old items");

		$form->fieldset('furniture', array(
			'mps_pos_furniture_product_line_id',
			'mps_pos_furniture_item_id',
			'old_items'
		));
	}
	
	$form->mps_pos_furniture_quantity_in_use(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		Form::TOOLTIP_INTEGER,
		Validata::PARAM_ONLY_NUMBER
	);
	
	$form->mps_pos_furniture_quantity_on_stock(
		Form::TYPE_TEXT,
		Form::TOOLTIP_INTEGER,
		Validata::PARAM_ONLY_NUMBER
	);
	
	$form->mps_pos_furniture_installation_year(
		Form::TYPE_TEXT,
		Form::TOOLTIP_INTEGER, 
		Validata::PARAM_YEAR
	);
	
	$form->mps_pos_furniture_deinstallation_year(
		Form::TYPE_TEXT,
		Form::TOOLTIP_INTEGER,
		Validata::PARAM_YEAR
	);
	
	$form->fieldset('equipment', array(
		'mps_pos_furniture_quantity_in_use',
		'mps_pos_furniture_quantity_on_stock', 
		'mps_pos_furniture_installation_year',
		'mps_pos_furniture_deinstallation_year'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<script type="text/javascript" src="/public/scripts/chain/chained.select.js"></script>
<script type="text/javascript" src="/public/js/pos.furniture.js"></script>
<style type="text/css">
	
	#pos_furniture { 
		width: 740px; 
	}
	
	form .equipment input { 
		width: 120px; 
	}
	
	#mps_pos_furniture_description {
		width: 500px;
		height: 60px;
	}
	
</style>
<?php 
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>