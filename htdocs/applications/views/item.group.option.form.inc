<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();

	$form = new Form(array(
		'id' => 'form',
		'action' => '/applications/helpers/item.group.option.save.php',
		'method' => 'post',
		'class' => 'validator'
	));

	$form->redirect(
		Form::TYPE_HIDDEN
	);

	$form->application(
		Form::TYPE_HIDDEN
	);

	$form->controller(
		Form::TYPE_HIDDEN
	);

	$form->action(
		Form::TYPE_HIDDEN
	);

	$form->item_group_option_id(
		Form::TYPE_HIDDEN
	);	

	$form->item_group_option_group(
		Form::TYPE_HIDDEN
	);

	$form->item_group_option_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);

	$form->item_group_option_caption1(
		Form::TYPE_TEXT
	);

	$form->item_group_option_description1(
		Form::TYPE_TEXTAREA,
		FORM::PARAM_LABEL,
		"class=description"
	);

	$form->item_group_option_quantity1(
		Form::TYPE_TEXT,
		Form::TOOLTIP_NUMERIC,
		Validata::PARAM_NUMBER,
		'class=numeric'
	);

	$form->item_group_option_item1(
		Form::TYPE_SELECT
	);

	$form->can_replace1(
		Form::TYPE_AJAX,
		FORM::PARAM_SHOW_VALUE
	);	

	$form->item_group_option_quantity2(
		Form::TYPE_TEXT,
		Form::TOOLTIP_NUMERIC,
		Validata::PARAM_NUMBER,
		'class=numeric'
	);

	$form->item_group_option_item2(
		Form::TYPE_SELECT
	);	

	$form->item_group_option_configcode1(
		Form::TYPE_TEXT
	);	

	$form->item_group_option_info1(
		Form::TYPE_TEXTAREA,
		FORM::PARAM_LABEL,
		"class=description"
	);

	$form->item_group_option_logical(
		Form::TYPE_RADIO,
		FORM::PARAM_HORIZONTAL
	);	

	$form->item_group_option_caption2(
		Form::TYPE_TEXT
	);
	
	$form->item_group_option_description2(
		Form::TYPE_TEXTAREA,
		FORM::PARAM_LABEL,
		"class=description"
	);

	$form->item_group_option_quantity3(
		Form::TYPE_TEXT,
		Form::TOOLTIP_NUMERIC,
		Validata::PARAM_NUMBER,
		'class=numeric'
	);

	$form->item_group_option_item3(
		Form::TYPE_SELECT
	);

	$form->can_replace2(
		Form::TYPE_AJAX,
		FORM::PARAM_SHOW_VALUE
	);	

	$form->item_group_option_quantity4(
		Form::TYPE_TEXT,
		Form::TOOLTIP_NUMERIC,
		Validata::PARAM_NUMBER,
		'class=numeric'
	);

	$form->item_group_option_item4(
		Form::TYPE_SELECT
	);

	$form->item_group_option_configcode2(
		Form::TYPE_TEXT
	);	

	$form->item_group_option_info2(
		Form::TYPE_TEXTAREA,
		FORM::PARAM_LABEL,
		"class=description"
	);

	$form->fieldset('Option', array(
		'item_group_option_name',
	));

	$form->fieldset('Items', array(
		'item_group_option_caption1',
		'item_group_option_description1',
		'item_group_option_quantity1',
		'item_group_option_item1',
		'can_replace1',
		'item_group_option_quantity2',
		'item_group_option_item2',
		'item_group_option_configcode1',
		'item_group_option_info1',
		'item_group_option_logical',
		'item_group_option_caption2',
		'item_group_option_description2',
		'item_group_option_quantity3',
		'item_group_option_item3',
		'can_replace2',
		'item_group_option_quantity4',
		'item_group_option_item4',
		'item_group_option_configcode2',
		'item_group_option_info2'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}

	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}

	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['add_file']) {
		$form->button(ui::button(array(
			'id' => 'add_file',
			'icon' => 'add',
			'href' => $buttons['add_file'],
			'class' => 'filemodal',
			'label' => 'Add File'
		)));
	}

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}

	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<style></style>
<style type="text/css">

	#form, #files { 
		width: 700px; 
	}

	.elembox {
		display: inline-block;
	}

	#optionFiles { 
		width: 700px; 
		margin-top: 40px;
	}

	.xhr-placeholder {
		padding: 0;
	}
	
	.file-extension {
		cursor: pointer;
	}

	textarea.description {
		width: 440px;
		height: 100px;
	}

	h4 { 
		display:block; 
		margin: 0 0 10px; 
		font-size: 18px;
		color: gray;
	}

	span.filename {display:block; font-weight:600; color:#0050CC; cursor:pointer; font-size: 11px;}
	span.filename:hover { color: black; }

	td.title span {
		display: block;
		color: gray;
		font-size: 12px;
		padding-top: 3px;
	}

	#can_replace1,
	#can_replace2 {
		font-weight: 600;
	}

</style>
<script type="text/javascript">
	$(document).ready(function() {

		// get item option files
		var fileList = $('#optionFiles').tableLoader({
			url: '/applications/helpers/item.group.option.files.php',
			data: $('#form').serializeArray(),
			after: function(self) {

				$(".modal").on("click", function() {
		
					var path = $(this).attr('tag');
					var title = $(this).parent().next().find('a').text();
				
					if (isImage(path)) {

						retailnet.modal.show(path, {
							title: (title) ? title : ''
						});
						
					} else {
						window.open('http://'+location.host+path);
					}
				});
			}
		});

		// modal file upload
		$(document).on('click', '.filemodal', function(e) {

			e.preventDefault();

			var url = $(this).attr('href');
			var option = $('#item_group_option_id').val();

			if (option) {
				$.fancybox({
					href		: url+'&option='+option,
					autoSize	: false,
					closeClick	: false,
					closeBtn	: false,
					fitToView   : true,
				    width       : '760px',
				    height      : '600px',
					modal 		: true,
					margin		: 0,
					padding		: 0,
					type 		: 'iframe',
					iframe		: {
						scrolling : 'auto',
						preload   : true
					},
			        afterClose	: function() {
			        	fileList.showPage(1);
			        }
				});
			} 
			else {
				retailnet.notification.error('Item option is not defined.');
			}

			return false;
		});

	});
</script>

<?php 

echo $form; 

echo "<div id='optionFiles' ></div>";

echo ui::dialogbox(array(
	'id' => 'delete_dialog',
	'title' => $translate->delete,
	'content' => $translate->dialog_delete_record,
	'buttons' => array(
		'cancel' => array(
			'icon' => 'cancel',
			'label' => $translate->cancel
		),
		'apply' => array(
			'icon' => 'apply',
			'label' => $translate->yes
		)
	)
));