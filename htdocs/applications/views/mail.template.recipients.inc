<script type="text/javascript">
	$(document).ready(function() {

		retailnet.loader.init();

		$('#save').click(function(e) {

			e.preventDefault();

			var form = $('#recipients'),
				action = form.attr('action'),
				data = form.serializeArray();

			// show loader
			retailnet.loader.show();

			retailnet.ajax.json(action, data).done(function(xhr) {

				retailnet.loader.hide();
	    		
	    		if (xhr && xhr.reload) {
					window.location.reload();
	        	}
	    		
				if (xhr && xhr.notification) {
					retailnet.notification.show(
						xhr.notification.content, 
						eval(xhr.notification.properties
					));
				}
			});
			
		});

		// checkbox as radio
		$('table input').change(function() {
			$('input', $(this).closest('tr')).not(this).attr('checked', false);
		});
		
	});
</script>
<style type="text/css">
	
	#form {
		width: 500px;
		margin-top: 20px;
	}
	
	#form h4 {
		display: block;
		margin: 40px 0 10px;
		font-size: 16px;
		color: gray;
	}
	
	#form h4:frist-child {
		margin-top: 0;
	}
	
	#form .container {
		display: block;
		background-color: white;
		border: 1px solid silver;
	}
	
	#form table th {
		font-size: 12px;
		color: black;
		background-color: #ddd !important;
	}
	
</style>
<?php 
	
	$form  = "<form id=recipients method=post action='/applications/helpers/mail.template.recipients.php' >";
	$form .= "<input type=hidden name=template id=template value=$template >";

	if($companies) {

		foreach ($companies as $key => $row) {
			
			$form .= "<h4>".$row['company']."</h4>";
			$form .= "<div class='container' >";
			
			$table = new Table(array(
				'class'=>'listing'
			));
			
			$table->datagrid = $row['recipients'];
			
			$table->caption('standard', 'Recipient');
			$table->caption('cc', 'CC');
			
			$table->standard(
				Table::ATTRIBUTE_NOWRAP, 
				Table::DATA_TYPE_CHECKBOX,
				'width=5%'
			);
		
			$table->cc(
				Table::ATTRIBUTE_NOWRAP, 
				Table::DATA_TYPE_CHECKBOX,
				'width=5%'
			);
		
			$table->recipient(
				Table::ATTRIBUTE_NOWRAP
			);
		
			$form .= $table->render();
			
			$form .= "</div>";
		}
		
	}
	
	$form .= "</form>";
	
	$actions = "<div class=actions >";
	
	$actions .= ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => Translate::instance()->back
	));

	$actions .= ui::button(array(
		'icon' => 'save',
		'id' => 'save',
		'label' => Translate::instance()->save
	));
	
	$actions .= "</div>";
	
	echo "<div id=form >$form $actions</div>";
?>
