<?php

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$uploadDisabled = ($buttons['save']) ? null : 'disabled';
	
	$data['file'] = '
		<span id="showfile" class="button">
			<span class="icon download"></span>
			<span class="label">'.$translate->download_file.'</span>	
		</span>
		<span id="upload" class="button '.$uploadDisabled.'">
			<span class="icon upload"></span>
			<span class="label">'.$translate->upload_file.'</span>	
		</span>
		<span class="highlight" ></span>
		<input type=hidden name="red_file_path" id="red_file_path" value="'.$data['red_file_path'].'" />
		<input type="hidden" name="has_upload" id="has_upload" />
	';

	//Define Form
	$form = new Form(array(
		'id' => 'fileForm',
		'action' => '/applications/helpers/red.project.file.save.php',
		'method' => 'post'
	));

	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);

	$form->red_file_id(
		Form::TYPE_HIDDEN
	);
	
	$form->red_file_project_id(
		Form::TYPE_HIDDEN
	);

	$form->red_file_owner_user_id(
		Form::TYPE_HIDDEN
	);

	$form->red_file_category_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->red_file_title(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);

	$form->red_file_description(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);

	$form->file(
		Form::TYPE_AJAX,
		FORM::PARAM_SHOW_VALUE
	);

	$form->partners(
		Form::TYPE_CHECKBOX_GROUP
	);

	$form->fieldset('file', array(
		'red_file_category_id',
		'red_file_title',
		'red_file_description',
		'file',
	));
	$form->fieldset('access', array(
		'partners'
	));

	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}

	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<link rel="stylesheet" href="/public/scripts/validationEngine/css/validationEngine.jquery.css" type="text/css"/>
<script src="/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="/public/scripts/validationEngine/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="/public/scripts/ajaxuploader/ajaxupload.js"  ></script>
<link rel="stylesheet" type="text/css" href="/public/scripts/loader/loader.css" />
<script type="text/javascript" src="/public/scripts/loader/loader.js"  ></script>
<script type="text/javascript">
$(document).ready(function() {

	var path;
	var application = $('#application');
	var form = $("#fileForm");
	var id  = $('#red_file_id');
	var btnShowFile = $('#showfile');
	var btnUploadFile = $('#upload');
	var filePath = $('#red_file_path');
	var fileTitle = $('#red_file_title');
	var hasUpload =  $('#has_upload');
	var highlight = $('.highlight').hide();

	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) { 

			retailnet.loader.hide();
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
			
			if (json.response) { 
				
				if (json.redirect) {
					window.location=json.redirect;
				}
				
				if (json.message) {
					$.jGrowl(json.message,{ 
						sticky: (json.response) ? false : true, 
						theme: (json.response) ? 'message' : 'error'
					});
				}
				
				if (hasUpload.val() && json.path) {
					filePath.val(json.path);
					hasUpload.val('');
				}	
			}
		}
	});

	new AjaxUpload( btnUploadFile, {
		action: '/applications/helpers/ajax.file.upload.php',
		name: 'userfile',
		data: {
			application: application.val(),
			checkExtension: true
		},
		onSubmit: function(file, ext) {

		},
		onComplete: function(file, response) {

			if (response) {

				var json = $.parseJSON(response);
				
				retailnet.loader.hide();
				highlight.hide();
				$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
				
				if(json.response) {

					var path = $("<div/>").html(json.path).text();
					filePath.val(path);
					btnUploadFile.removeClass('error');
					hasUpload.val(1);
					highlight.removeClass('error').addClass('success').text(json.button).slideToggle('slow');
					filePath.trigger('change');
				} 

				if (json.message) {
					$.jGrowl(json.message, { 
						sticky: (json.response) ? false : true, 
						theme: (json.response) ? 'message' : 'error'
					});
				}
			}
		}
	});

	filePath.change(function() {

		path = $(this).val(); 

		if (path) {

			btnShowFile.removeClass('disabled');

			if (isImage(path)) {
				$('.icon', btnShowFile).removeClass('download').addClass('picture');
				$('.label', btnShowFile).text('Show Picture');
			} else {
				$('.icon', btnShowFile).removeClass('picture').addClass('download');
				$('.label', btnShowFile).text('Download');
			}
			
		} else {
			btnShowFile.addClass('disabled');
		}
		
	}).trigger('change');
	

	btnShowFile.click(function(event) {

		event.preventDefault();

		if (!$(this).hasClass('disabled')) {

			path = filePath.val();

			if (isImage(path)) {

				retailnet.modal.show(path, {
					title: fileTitle.val()
				});
			}
			else {
				window.open('http://'+location.host+path);
			}
		}
	});
	
	$("#save").click(function(event) { 

		event.preventDefault();

		// check file path
		if (!filePath.val()) {
			btnUploadFile.addClass('error');
		}

		// validators
		var errors = form.find('.error').length;
		var valid = form.validationEngine('validate');

		// close loaders
		retailnet.loader.hide();
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
		highlight.removeAttr('class').hide();

		if (errors || !valid) {
			$.jGrowl('Check red marked fields.',{ 
				sticky: true, 
				theme: 'error'
			});
		}
		else {
			retailnet.loader.show();
			form.submit();
		}
	});

	$('.dialog').click(function(event) {
		
		event.preventDefault();

		var button = $(this);
		
		$('#apply, a.apply').attr('href', button.attr('href'));
		
		retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
	});
	
	$('#cancel').bind('click', function() {
		retailnet.modal.close();
		return false;
	});
});
</script>
<style type="text/css">

	#fileForm { 
		width: 700px; 
	}
	
	#fileForm textarea {
		width: 380px;
	}
	
	#upload.disabled {
		display: none;
	}
	
</style>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete_file,
		'content' => $translate->dialog_delete_file,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>