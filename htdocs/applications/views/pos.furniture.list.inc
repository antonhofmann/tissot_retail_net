<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	// standard furniture
	$('#standard_furniture').tableLoader({
		url: '/applications/helpers/pos.furniture.standard.php',
		data: $('.request').serializeArray()
	});
	
	// customized furniture
	$('#customized_furniture').tableLoader({
		url: '/applications/helpers/pos.furniture.customized.php',
		data: $('.request').serializeArray()
	});

	// project furnitures
	$('#project_furniture').tableLoader({
		url: '/applications/helpers/pos.furniture.project.php',
		data: $('.request').serializeArray()
	});

	// catalog furnitures
	$('#catalog_furniture').tableLoader({
		url: '/applications/helpers/pos.furniture.catalog.php',
		data: $('.request').serializeArray()
	});
	
});
</script>

<style type="text/css"> 

	.furnitures { 
		width: 1400px; 
	}
	
	h5 {
		display: block;
		margin: 20px 0;
	}
	
	
</style>
<?php 
	echo $request->form($requestFields); 
?>
<div id="standard_furniture" class="furnitures"></div>
<div id="customized_furniture" class="furnitures"></div>
<div id="project_furniture" class="furnitures"></div>
<div id="catalog_furniture" class="furnitures"></div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>