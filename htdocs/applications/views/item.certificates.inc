<style type="text/css">

	#certificates { 
		width: 700px; 
	}

	.item-trigger {
		border-right: 0 !important;
		padding-right: 0 !important;
	}

	.row-subtable {
		display:none;
	}

	table.certificates {
		width: 100%;
		border-bottom: 1px solid silver !important;
	}

	table.certificates small {
		display: block;
		padding-top: 2px;
	}
	
</style>
<script type="text/javascript">
$(document).ready(function() {
	$('.item-trigger').click(function(e) {
		e.preventDefault();
		$(this).closest('tr').next().slideToggle();
		$('.fa', $(this)).toggleClass('fa-chevron-circle-right').toggleClass('fa-chevron-circle-down');
	});	
})
</script>
<?php

if ($certificates) {

	$list ="
		<table class='default certificates'>
		<thead>
			<tr>
				<th width='20px'>&nbsp;</th>
				<th nowrap=nowrap ><strong>$translate->certificate</strong></th>
			</tr>
		</thead>
		<tbody>
	";

	$btnDownload = "<span class=\"fa-stack\"><i class=\"fa fa-circle fa-stack-2x\"></i><i class=\"fa fa-download fa-stack-1x fa-inverse\"></i></span>";

	foreach ($certificates as $id => $certificate) {
		
		$rowClass = $rowClass=='-odd' ? '-even' : '-odd'; 
		$hasSubtabe = $certificate['versions'] ? 'has-subtable' : null;
		$itemToggler = ($certificate['versions']) ? "<i class='fa fa-chevron-circle-right' ></i>" : null;

		$list .= "<tr class='$rowClass $hasSubtabe' data-id='$id' >";
		$list .= "<td class='item-trigger' title='Items' >$itemToggler</td>";
		$list .= "<td><a href='/catalog/certificates/data/$id' target='_blank'>{$certificate[name]}</a><small>{$certificate[company]}</small></td>";
		$list .= "</tr>";

		if ($certificate['versions']) {

			$list .= "<tr class='row-subtable $rowClass' id='subtable-$id'>";
			$list .= "<td colspan='6' class='cell-subtable '>";
			$list .= "<table class='subtable'>";
			$list .="<thead>";
			$list .="<tr>";
			$list .="<th nowrap=nowrap ><strong>Version Name</strong></th>";
			$list .="<th nowrap=nowrap width='100' ><strong>Expiry</strong></th>";
			$list .="<th nowrap=nowrap width='30' ><strong>Download</strong></th>";
			$list .="</tr>";
			$list .="</thead>";
			$list .="<tbody>";

			foreach ($certificate['versions'] as $i => $version) {
				$list .= "<tr>";
				$list .= "<td>{$version[name]}</td>";
				$list .= "<td>{$version[expiry]}</td>";
				$list .= "<td align=center ><a href=\"{$version[file]}\" target='_blank'>$btnDownload</a></td>";
				$list .= "</tr>";
			}

			$list .= "</tbody></table>";
			$list .= "</td>";
			$list .= "</tr>";
		}

	}

	$list .= "</tbody></table>";

}

echo "<div id='certificates'>$list</div>";