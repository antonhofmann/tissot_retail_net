<script type="text/javascript">
$(document).ready(function() {

	var list = $('#list');

	list.tableLoader({
		url: list.data('url'),
		data: $('.request').serializeArray(),
		after: function(self) {
			
			$('select', self).dropdown();
		}
	});	
	
});
</script>
<div id="list" class="list-loader <?php echo $class ?>" data-url="<?php echo $url ?>"></div>
<?php echo Request::instance()->form(); ?>