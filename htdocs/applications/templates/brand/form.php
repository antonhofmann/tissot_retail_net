<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'brand',
		'action' => '/applications/modules/brand/submit.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->brand_id(
		Form::TYPE_HIDDEN
	);
	
	$form->brand_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	
	$form->brand_language_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	

	$form->brand_short_description(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL,
		Validata::PARAM_REQUIRED
	);

	$form->brand_long_desicrption(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL,
		Validata::PARAM_REQUIRED
	);

	$form->brand_keywords(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL,
		Validata::PARAM_REQUIRED
	);

	$form->brand_uberal_category_ids(
		Form::TYPE_TEXT,
		Form::PARAM_LABEL,
		Validata::PARAM_REQUIRED
	);

	$form->brand_video_url(
		Form::TYPE_TEXT,
		Form::PARAM_LABEL,
		Validata::PARAM_REQUIRED
	);


	$form->brand_video_description(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL,
		Validata::PARAM_REQUIRED
	);


	
	
	// fieldset: dimensions
	$form->fieldset('Brand Details', array(
		'brand_name',
		'brand_language_id', 
		'brand_short_description',
		'brand_long_desicrption',
		'brand_keywords',
		'brand_uberal_category_ids',
		'brand_video_url',
		'brand_video_description'
	));


	
	//upload section

	$data['brand_logo'] = '
		<span id="showfile" class="button">
			<span class="icon download"></span>
			<span class="label">'.$translate->download_file.'</span>	
		</span>
		<span id="upload" class="button">
			<span class="icon upload"></span>
			<span class="label">'.$translate->upload_file.'</span>	
		</span>
		<input type=hidden name="brand_logo" id="brand_logo" value="'.$data['brand_logo'].'" class="file_path required" />
		<input type="hidden" name="has_upload" id="has_upload" />
	';


	

	$form->logo_id(
		Form::TYPE_HIDDEN,
		'class=file_id'
	);

	$form->logo_title(
		Form::TYPE_HIDDEN,
		'class=file_title'
	);


	$form->brand_logo(
		Form::TYPE_AJAX,
		FORM::PARAM_SHOW_VALUE
	);

	


	$form->fieldset('Logos', array(
		'brand_logo', 
		'brand_squared_logo'
	));

	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	
	/*
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	*/
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
	
?>

<style type="text/css">

	form textarea {
		width: 400px;
	}

	#brand_long_desicrption {
		height:200px;
	}

	#brand_language_id {
		width: 200px;
	}
	
	
</style>