<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();

	$form = new Form(array(
		'action' => $buttons['send'],
		'method' => 'post',
		'id' => 'user-invitation-form'
	));
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);

	$form->mail_template_id(
		Form::TYPE_HIDDEN
	);

	$form->mail_template_subject(
		Form::TYPE_TEXTAREA,
		Validata::PARAM_REQUIRED,
		Form::PARAM_LABEL
	);

	$form->mail_template_text(
		Form::TYPE_TEXTAREA,
		Validata::PARAM_REQUIRED,
		Form::PARAM_LABEL
	);

	$form->mail_template_recipients(
		Form::TYPE_TEXTAREA,
		Validata::PARAM_REQUIRED,
		Form::PARAM_LABEL,
		'label=Recipients<br />(add email addresses separated by a coma, a semicolon or by a line feed)'
	);

	$form->fieldset('mail', array(
		'mail_template_subject',
		'mail_template_text',
		'mail_template_recipients'
	));

	if ($buttons['back']) {
		$form->button(ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => $translate->back
		)));
	}

	if ($buttons['test']) {
		$form->button(ui::button(array(
			'id' => 'test',
			'href' => $buttons['test'],
			'icon' => 'mail',
			'label' => "Test Mail"
		)));
	}
	
	if ($buttons['send']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'mail',
			'label' => $translate->sendmail
		)));
	}

	$form->dataloader($data);

	echo $form->render();
?>
<style type="text/css">

	
	form .mail_template_code input {
		width: 440px;
	}
	
	form .mail_template_purpose textarea {
		width: 440px;
		height: 40px;
	}
	
	form .mail_template_subject textarea {
		width: 440px;
		height: 40px;
	}
	
	form .mail_template_recipients textarea,
	form .mail_template_text textarea {
		width: 440px;
		height: 200px;
	}
	
	form .mail_template_footer textarea {
		width: 440px;
		height: 100px;
	}

	form.default .mail_template_recipients label {
	  min-width: 160px;
	  max-width: 160px;
	  line-height: 24px;
	  min-height: 24px;
	}
	
</style>
<!-- test mail modal dialog -->
<div id="test-mail-template"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" >Send Test E-mail an:</div>
	</div>
	<div class="ado-modal-body">
		<input type="text" name="test-mail-recipient" id="test-mail-recipient" class="ado-modal-input required" value="<?php echo $user->email ?>" placeholder="Recipient E-mail" />
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="/applications/modules/ordersheet/sendmail.php" >
				<span class="icon mail"></span>
				<span class="label" ><?php echo $translate->send ?></span>
			</a>
		</div>
	</div>
</div>
<?php 
	
	
	 
	
?>