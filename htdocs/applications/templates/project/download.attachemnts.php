<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$_ID = $_REQUEST['pid'];

	$user = User::instance();

	$project = new Project();
	$project->read($_ID);

	$db = Connector::get();

	if (!$_REQUEST['frame']) {
		die("You don't have access to this page.");
		exit;
	}

	// project entity
	$_PID = $project->id && User::permission('can_view_attachments_in_projects') ? $_ID : null;

	// get user roels
	$userRoles = User::roles();
	$roles = $userRoles ? join($userRoles) : '';

	
	$_FILTERS = array();
	
	// default
	$_FILTERS[] = "order_file_order = $project->order";

	// filter file categories
	$sth = $db->prepare("
		SELECT GROUP_CONCAT(role_file_category_file_category_id) AS categories
		FROM role_file_categories 
		WHERE role_file_category_role_id IN ($roles)
	");

	$sth->execute();
	$result = $sth->fetch();

	if ($result['categories']) {
		$_FILTERS[] = "order_file_category IN ({$result[categories]})";
	}

	// restricted view
	if (!User::permission('has_access_to_all_attachments_in_projects')) {

		$db->exec("SET SESSION group_concat_max_len = 1000000");

		$sth = $db->prepare("
			SELECT GROUP_CONCAT(order_file_address_file) AS ids
			FROM order_file_addresses
			WHERE order_file_address_address = ?
		");

		$sth->execute(array($user->address));
		$result = $sth->fetch();
		$extendFilter = $result['ids'] ? "OR order_file_id IN ({$result[ids]})" : null;
		$_FILTERS[] = "(order_file_owner = $user->id $extendFilter)";
	}

	$_FILTERS = join(' AND ', $_FILTERS);

	$sth = $db->prepare("
		SELECT DISTINCT 
			order_file_id, 
			order_file_title, 
			order_file_path, 
			file_type_name,
			order_file_category_id,
			DATE_FORMAT(order_files.date_created, '%d.%m.%Y %H:%i') AS date,
			CONCAT(user_name, ' ', user_firstname) AS owner,
		 	CONCAT(order_file_category_priority, ' ', order_file_category_name) AS group_name
		FROM order_files 
		LEFT JOIN order_file_categories ON order_file_category_id = order_file_category
		LEFT JOIN users ON user_id = order_file_owner
		LEFT JOIN file_types ON order_file_type = file_type_id
		WHERE $_FILTERS
		ORDER BY group_name, order_file_category_priority, order_files.date_created DESC 
	");

	$sth->execute();
	$result = $sth->fetchAll();

	$_DATAGRID = array();

	if ($result) {
		foreach ($result as $row) {
			$file = $row['order_file_id'];
			$category = $row['order_file_category_id'];
			$_DATAGRID[$category]['name'] = $row['group_name'];
			$_DATAGRID[$category]['files'][$file]['owner'] = $row['owner'];
			$_DATAGRID[$category]['files'][$file]['title'] = $row['order_file_title'];
			$_DATAGRID[$category]['files'][$file]['path'] = $row['order_file_path'];
			$_DATAGRID[$category]['files'][$file]['type'] = $row['file_type_name'];
			$_DATAGRID[$category]['files'][$file]['date'] = $row['date'];
		}
	}

?>
<!doctype html>
<!--[if IE 7]>    <html class="ie ie7" lang="en"> <![endif]-->
<!--[if gt IE 7]>    <html class="ie" lang="en"> <![endif]-->
<!--[if !IE]><!--><html lang="en"> <!--<![endif]--> 
<head>
	<title>Download Project Attachments - Retail Net</title>
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link href="/public/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="/public/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/x-icon" />
	
	<script src="/public/scripts/jquery/1.9.1.js" type="text/javascript"></script>
	<script src="/public/scripts/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	
	<link rel="stylesheet" type="text/css" href="/public/scripts/fancybox/fancybox.css?v=2.1.5" />
	<script type="text/javascript" src="/public/scripts/fancybox/fancybox.js?v=2.1.5"></script>
	
	<script type="text/javascript" src="/public/scripts/jgrowl/jgrowl.js"></script>
	<link rel="stylesheet" type="text/css" href="/public/scripts/jgrowl/jgrowl.css" />
	
	<link href="/public/scripts/adomodal/adomodal.css" rel="stylesheet" type="text/css" />
	<script src="/public/scripts/adomodal/adomodal.js" type="text/javascript"></script> 
	<script type="text/javascript" src="/public/scripts/jquery.actual.min.js"></script>
	<link rel="stylesheet" href="/public/css/spinners.css">
	
	<script type="text/javascript" src="/public/scripts/message.js"></script>
	<script type="text/javascript" src="/public/scripts/retailnet.js"></script>

	<link rel="stylesheet" href="/public/scripts/bootstrap/css/bootstrap.compiled.css">
	<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
	
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/reset.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/layout.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/table.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/form.css" />
	<link rel="stylesheet" type="text/css" href="/public/css/gui.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/gui.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/modal.css?t=<?php echo date('U'); ?>" />

	<style type="text/css">
	
		body,
		body.adomat {
			background: #eeeeee !important;
			min-width: 400px !important;
		}

		.bootstrap {
			background: transparent;
		}
		
		.modalbox-container {
			display: block;
		}

		.modalbox-container {
			display: block;
			height: 640px;
		}

		.section + .section {
			margin-top: 40px;
		}

		.section-title {
			font-size: 14px;
			margin-bottom: 10px;
		}

		.section .table-striped tbody tr:last-child td {
			border-bottom: 1px solid #ddd;
		}

		.section tr th {
			font-weight: 500;
			font-size: 12px;
		}

		.section tbody td,
		.section tfoot td {
			font-size: 12px;
		}
		
	</style>

	<script type="text/javascript">
		$(document).ready(function() {

			var ID_ENTITY = $('#idEntity').val();
			var BTN_PDF = $("#btnPrintPdf");
			var BTN_ZIP = $("#btnPrintZip");

			if (!ID_ENTITY && parent) {
				
				parent.retailnet.notification.hide();
				
				parent.retailnet.notification.show("You don't have access to this page", { 
					live: 3000,
					sticky: false, 
					theme: 'error'
				});
				
				parent.retailnet.modal.hide();
			}

			retailnet.loader.init();

			// close modal screen
			$('.close', $('#modalbox')).click(function(e) {
				
				e.preventDefault();
				e.stopImmediatePropagation();
				
				parent.retailnet.modal.close();
				
				return false;
			})

			$('input.selct-group-files').on('change', function() {
				var self = $(this);
				$('input.file', self.closest('table')).prop('checked', self.is(':checked'))
				self.trigger('changed', [true]);
			})

			$('input.selct-group-files').on('changed', function(e, manuallyTriggered) { 
				
				var self = $(this);
				var list = self.closest('table');
				var files = $('input.file', list);
				var filesChecked = $('input.file:checked', list);

				if (!manuallyTriggered) {
					self.prop('checked', files.length==filesChecked.length ? true : false);
				}

				// for each change, check pdf button
				BTN_PDF.toggle($('input.nopdf:checked').length ? false : true);
			})

			$('input.file').on('change', function() {
				var self = $(this);
				$('.selct-group-files', self.closest('table')).trigger('changed');
				self.trigger('changed', [true]);
			})


			$('.btn-submit').on('click', function(e) {

				e.preventDefault();
				e.stopPropagation();

				parent.retailnet.notification.hide();

				if (!$('input.file:checked').length) {
					
					parent.retailnet.notification.show("Please select at least one file", { 
						live: 3000,
						sticky: false, 
						theme: 'error'
					})

					return false;
				}

				var self = $(this);
				var url = self.prop('href');
				var data = $('.modalbox-content :input').serialize();

				retailnet.loader.show();

				$.post(url, data, function(xhr) {

					retailnet.loader.hide();

					if (xhr && xhr.errors) {
						$.each(xhr.errors, function(i, msg) {
							parent.retailnet.notification.error(msg);
						})
						return;
					}

					if (xhr && xhr.file) {
						parent.window.location = xhr.file;
					}

				}, 'json')

				return false;
			})

		})
	</script>
	
</head>
<body class="adomat">
	<div class="modalbox-container">
		<div id="modalbox" class="adomat modalbox">
			<div class="modalbox-header">
				<div class="title">Download Attachments</div> 
				<div class="subtitle">Project Number: <?php echo $project->number ?></div>
				<span class="fa-stack ado-modal-close close">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-times fa-stack-1x fa-inverse"></i>
				</span>
			</div>
			<div class="modalbox-content-container">
				<div class="modalbox-content">
					<input type="hidden" id="idEntity" name="id" value="<?php echo $_PID ?>">
					<div class="bootstrap">
						<?php 

							$_PDF_PERMITTED_EXTENSIONS = array('pdf', 'gif', 'jpg', 'jpeg', 'png', 'bmp', 'tif');

							if ($_DATAGRID) {

								foreach ($_DATAGRID as $gid => $group) {
									
									echo "<div class=\"section\">";
									echo "<h4 class=\"section-title\">{$group[name]}</h4>";
									echo "<table class=\"table table-condensed table-striped\">";
									echo "<thead>";
									echo "<tr>";
									echo "<th width='20'><input type=\"checkbox\" class='selct-group-files'></th>";
									//echo "<th width=\"20%\" nowrap=\"nowrap\">Made by</th>";
									echo "<th nowrap=\"nowrap\">Title</th>";
									echo "<th width=\"20%\" nowrap=\"nowrap\">Type</th>";
									echo "<th width=\"5%\" nowrap=\"nowrap\">Date</th>";
									echo "</tr>";
									echo "</thead>";
									echo "<tbody>";

									foreach ($group['files'] as $id => $file) {
										
										$info = pathinfo($file['path']);
										$nopdf = in_array($info['extension'], $_PDF_PERMITTED_EXTENSIONS) ? null : 'nopdf';

										echo "<tr>";
										echo "<td width='20'><input type=\"checkbox\" name=\"files[$id]\" value=\"1\" class=\"file $nopdf\"></td>";
										//echo "<td width=\"20%\" nowrap=\"nowrap\">{$file[owner]}</td>";
										echo "<td>{$file[title]}</td>";
										echo "<td width=\"25%\" nowrap=\"nowrap\">{$file[type]}</td>";
										echo "<td width=\"5%\" nowrap=\"nowrap\">{$file[date]}</td>";
										echo "</tr>";
									}

									echo "</tbody>";
									echo "</table>";
									echo "</div>";
								}
							} else {
								echo "<p class=\"emptybox\">No matching records found</p>";
							}
						?>
					</div>
				</div>
			</div>
			<div class="modalbox-footer">
				<div class="modalbox-actions ado-actions">
					<div class="bootstrap print-menu">
						<a id="btnPrintPdf" role="button" class="btn btn-default btn-sm btn-submit" href="/applications/modules/project/download.attachments.pdf.php">
							<i class="fa fa-file-text"></i> 
							Download PDF
						</a>
						<a id="btnPrintZip" role="button" class="btn btn-default btn-sm btn-submit" href="/applications/modules/project/download.attachments.zip.php">
							<i class="fa fa-archive"></i> 
							Download ZIP
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>