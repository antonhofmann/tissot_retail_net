<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$user = User::instance();
	$translate = Translate::instance();

	// db model
	$model = new Model(Connector::DB_CORE);

	// user id
	$id = $_REQUEST['id'];
	$application = $_REQUEST['application'];
	$controller = $_REQUEST['controller'];

	// modal title
	$title = $id ? "Edit User" : "Add New User";

	// user roles
	$model->query("SELECT role_id, role_name FROM roles	");

	if ($user->can_set_swatch_roles_in_my_company) {
		$model->filter('roles', 'role_is_visible_to_swatch_in_my_company = 1');
	} else {
		$model->filter('roles', 'role_is_visible_to_clients_in_my_company = 1');
	}

	$model->order('role_application_number, role_name');
	$result = $model->fetchAll();

	$dataloader['roles'] = _array::extract($result);

	// dataloader: user sex
	$dataloader['user_sex'] = array(
		'm' => $translate->male,
		'f' => $translate->female
	);

	$data = array();
	$data['user_active'] = 1;
	$data['controller'] = 'mycompany';
	$data['user_address'] = $user->address;
	$data['used_roles'] = ($dataloader['roles']) ? serialize(array_keys($dataloader['roles'])) : null;


	// add new user form
	$form = Template::load('user.data', array(
		'mycompany' => true,
		'data' => $data,
		'dataloader' => $dataloader,
		'hidden' => array(
			'user_login' => true,
			'user_password' => true,
			'user_active' => true
		)
	));

?>
<!doctype html>
<html lang="en">
<head>
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link href="/public/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="/public/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/x-icon" />
	
	<script src="/public/scripts/jquery/1.7.2.js" type="text/javascript"></script>
	<script src="/public/scripts/jquery/migrate.min.js" type="text/javascript"></script>
	<script src="/public/scripts/jquery/jquery.ui.1.10.4.min.js"></script>
	<link type="text/css" href="/public/scripts/jquery/jquery.ui.1.10.4.min.css" rel="stylesheet" />
	
	<link rel="stylesheet" type="text/css" href="/public/scripts/fancybox/fancybox.css?v=2.1.5" />
	<script type="text/javascript" src="/public/scripts/fancybox/fancybox.js?v=2.1.5"></script>
	
	<script type="text/javascript" src="/public/scripts/jgrowl/jgrowl.js"></script>
	<link rel="stylesheet" type="text/css" href="/public/scripts/jgrowl/jgrowl.css" />
	
	<link href="/public/scripts/adomodal/adomodal.css" rel="stylesheet" type="text/css" />
	<script src="/public/scripts/adomodal/adomodal.js" type="text/javascript"></script> 
	<script type="text/javascript" src="/public/scripts/jquery.actual.min.js"></script>
	<link rel="stylesheet" href="/public/css/spinners.css">
	
	<script src="/public/scripts/master.js" type="text/javascript"></script>
	<script type="text/javascript" src="/public/scripts/message.js"></script>
	<script type="text/javascript" src="/public/scripts/retailnet.js"></script>

	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/modal.css" />
	<link rel="stylesheet" href="/public/scripts/bootstrap/css/bootstrap.compiled.css">
	<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
	
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/modal.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/reset.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/layout.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/table.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/form.css" />
	<link rel="stylesheet" type="text/css" href="/public/css/gui.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/gui.css" />


	<link rel="stylesheet" type="text/css" href="/public/scripts/validationEngine/css/validationEngine.jquery.css" />
	<script type="text/javascript" src="/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js"></script>
	<script type="text/javascript" src="/public/scripts/validationEngine/js/jquery.validationEngine.js"></script>

	<link rel="stylesheet" type="text/css" href="/public/scripts/qtip/qtip.css" />
	<script type="text/javascript" src="/public/scripts/qtip/qtip.js"></script>

	<script type="text/javascript" src="/public/js/user.js"></script>

	<style type="text/css">
	
		body,
		body.adomat {
			background: #eeeeee !important;
			min-width: 400px !important;
		}
		
		.modalbox-container {
			display: block;
		}
		
		.modalbox {
			display: block;
			width: 100%;
			margin: 0;
			padding: 0;
			-moz-box-shadow: none !important;
			-webkit-box-shadow: none !important;
			box-shadow: none !important;
		}
		
		.modal-content {
			max-width: 500px;
		}
		
		textarea.description {
			width: 440px;
			height: 100px;
		}
		
	</style>

	<script type="text/javascript">
		
		$(document).ready(function() {

			// close modal screen
			$('.close', $('#modalbox')).click(function(e) {
				
				e.preventDefault();
				e.stopImmediatePropagation();
				
				parent.retailnet.modal.close();
				
				return false;
			});
		});
	</script>
	
</head>
<body class="adomat modal-iframe">

	<div class="modalbox-container">
		<div id="modalbox" class="adomat modalbox modal-750">
			<div class="modalbox-header">
				<div class="title"><?php echo $title; ?></div>
			</div>
			<div class="modalbox-content-container">
				<div class="modalbox-content"><?php echo $form; ?></div>
			</div>
			<div class="modalbox-footer">
				<div class="modalbox-actions">
					
					<span class="button close cancel"> 
						<span class="icon cancel"></span>
						<span class="label">Close</span>
					</span>

					<?php
						echo ui::button(array(
							'id' => 'user_save',
							'class' => 'sendmail',
							'icon' => 'save',
							'caption' => $translate->save
						));
					?>
				</div>
			</div>
		</div>
	</div>
<?php
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			)
		)
	));
?>
</body>
</html>