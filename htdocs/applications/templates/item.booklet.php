<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$_ENTITY = $_REQUEST['entity'];
	$_ENTITY_VALUES = $_REQUEST['entity_values'];

	$currentActive = 1;

	$db = Connector::get();

	$_ITEM_CATEGORIES = array();


	if (!$_REQUEST['nofilters']) {
		
		$sth = $db->prepare("
			SELECT DISTINCT
				item_category_id AS id,
				item_category_name AS name
			FROM items
			INNER JOIN item_categories ON item_category_id = item_category
			WHERE item_type = 1
			ORDER BY item_category_name
		");

		$sth->execute();
		$_ITEM_CATEGORIES = $sth->fetchAll();
	}

	$_ITEM_SUBCATEGORIES = array();


	if (!$_REQUEST['nofilters']) {
		
		$sth = $db->prepare("
			SELECT DISTINCT
				item_subcategory_id AS id,
				CONCAT(item_category_name, ' - ', item_subcategory_name) AS name
			FROM items
			INNER JOIN item_subcategories ON item_subcategory_id = item_subcategory
			INNER JOIN item_categories ON item_category_id = item_subcategory_category_id
			ORDER BY name
		");

		$sth->execute();
		$_ITEM_SUBCATEGORIES = $sth->fetchAll();
	}

	$_PRODUCT_LINES = array();

	if (!$_REQUEST['nofilters']) {
		
		$sth = $db->prepare("
			SELECT DISTINCT
				product_line_id AS id,
				product_line_name AS name
			FROM items
			INNER JOIN item_supplying_groups ON item_supplying_group_item_id = item_id
			INNER JOIN supplying_groups ON supplying_group_id = item_supplying_group_supplying_group_id
			INNER JOIN product_line_supplying_groups ON product_line_supplying_group_group_id = supplying_group_id
			INNER JOIN product_lines ON product_line_id = product_line_supplying_group_line_id
			WHERE item_type = 1
			ORDER BY product_line_name
		");

		$sth->execute();
		$_PRODUCT_LINES = $sth->fetchAll();
	}

	// active item filter
	$_ACTIVE = array();
	$_ACTIVE[0] = "all items";
	$_ACTIVE[1] = "active items";
	$_ACTIVE[2] = "inactive items";

	Compiler::attach(array(
		'/public/themes/default/css/reset.css',
		'/public/themes/default/css/layout.css',
		'/public/themes/default/css/table.css',
		'/public/themes/default/css/form.css',
		'/public/css/gui.css',
		'/public/themes/default/css/gui.css',
		'/public/scripts/adomodal/adomodal.css',
		'/public/themes/default/css/modal.css'
	));

	Compiler::export();

?>
<!doctype html>
<!--[if IE 7]>    <html class="ie ie7" lang="en"> <![endif]-->
<!--[if gt IE 7]>    <html class="ie" lang="en"> <![endif]-->
<!--[if !IE]><!--><html lang="en"> <!--<![endif]--> 
<head>
	<title>Idea - Retail Net</title>
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link href="/public/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="/public/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/x-icon" />
	
	<link rel="stylesheet" type="text/css" href="/public/scripts/fancybox/fancybox.css?v=2.1.5" />
	<link rel="stylesheet" type="text/css" href="/public/scripts/jgrowl/jgrowl.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/modal.css" />
	<link rel="stylesheet" href="/public/scripts/bootstrap/css/bootstrap.compiled.css">
	<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/public/css/spinners.css">
	<?php echo Compiler::get('css') ?>

	<script src="/public/scripts/jquery/1.9.1.js" type="text/javascript"></script>
	<script src="/public/scripts/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="/public/scripts/fancybox/fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="/public/scripts/jgrowl/jgrowl.js"></script>
	<script src="/public/scripts/adomodal/adomodal.js" type="text/javascript"></script> 
	<script type="text/javascript" src="/public/scripts/jquery.actual.min.js"></script>
	<script type="text/javascript" src="/public/scripts/message.js"></script>
	<script type="text/javascript" src="/public/scripts/retailnet.js"></script>
	<?php echo Compiler::get('js') ?>

	<style type="text/css">
	
		body,
		body.adomat {
			background: #eeeeee !important;
			min-width: 400px !important;
		}

		.bootstrap {
			background-color: transparent !important;
		}
		
		.modalbox-container {
			display: block;
		}

		.modalbox-container {
			display: block;
			height: 640px;
		}

	
		.image-responsive {
			max-width: 760px;
		}

		.info-section + .info-section {
			margin-top: 30px;
		}

		.section-title {
			font: bold 14px arial;
			/*border-bottom: 1px solid #dedede;*/
			padding-bottom: 5px;
			margin-bottom: 10px;
		}

		.section-title .table-striped tbody tr:last-child td {
			border-bottom: 1px solid #ddd;
		}

		.cel-label {
			white-space: nowrap;
			font-weight: bold;
		}


		.info-section tr th {
			font-weight: 500;
			font-size: 12px;
		}

		.info-section small {
			font-weight: 500;
			font-size: 10px;
		}

		.info-section tbody td,
		.info-section tfoot td {
			font-size: 12px;
		}

		.cover-image {
			margin-bottom: 40px;
		}

		.print-menu li {
			text-align: left;
			font-size: 12px;
		}

		.print-menu li label {
			margin: 0;
		}

		.print-menu li:hover { color: #000; }

		.btn-caption {
			display: inline-block;
			font-size: 11px;
    		line-height: 1.5;
    		color: #888;
    		margin-right: 5px;
		}

		.modalbox-topbar .dropdown-menu {
			overflow-y: auto;
    		max-height: 300px;
		}

		.dropdown-menu label,
		.dropdown-menu a {
			font-weight: 400;
			margin: 0;
			font-size: 12px;
			cursor: pointer;
		}

		.total-selected {
			display: none;
			font-size: 10px;
			font-style: normal;
		}

		.sort-menu {
			min-width: 200px !important; 
		}

		.sort-menu a label {
			float: left;
		}

		.sort-menu a .clearfix {
			width: auto !important;
		}

		.sort-captions {
			font-size: 10px;
			font-style: normal;
		}

		#items {margin-top: 20px; font-size: 12px;}

		.fa.active {color: #16E600}
		.fa.inactive {color: #CCC}
		
	</style>

	<script type="text/javascript">
		$(document).ready(function() {

			retailnet.loader.init();

			// close modal screen
			$('.close', $('#modalbox')).click(function(e) {
				
				e.preventDefault();
				e.stopImmediatePropagation();
				
				parent.retailnet.modal.close();
				
				return false;
			});

			// checkbox in dropdown menu
			$('ul.dropdown-menu li').on('click', function (e) {

				e.stopImmediatePropagation();
				
				var self = $(this);
				var checkbox = $('input[type="checkbox"]', $(this).closest('li'));

				if (checkbox.length==0) return;

				e.preventDefault();
				e.stopPropagation();

				var list = $(this).closest('ul');

				if (checkbox.is(':disabled') || checkbox.prop('readonly')) {
					return false;
				}

				if (checkbox.hasClass('select-all')) {
					var checked = checkbox.is(':checked');
					$('input[type="checkbox"]:not(.select-all)', list).prop('checked', !checked);
					checkbox.trigger('changed', [true]);
				}

				var checked = $(e.target).is(':checkbox') 
					? checkbox.is(':checked') 
					: !checkbox.is(':checked');
				
				checkbox.prop('checked', checked);

				if (!checkbox.hasClass('select-all')) {
					$('.select-all', list).trigger('changed');
				}

				if (list.hasClass('sort-menu')) {
					self.trigger('sort', [true]);
				}

				if (!list.hasClass('print-menu')) {
					loadItems();
				}

				if (!list.hasClass('sort-menu')) return false;
			})

			$('ul.dropdown-menu li input[type="checkbox"]').on('click', function (e) {
				e.stopImmediatePropagation();
				$(this).prop('checked', !$(this).is(':checked'))
				$(this).closest('li').trigger('click');
			})

			$('ul.dropdown-menu li.radio').on('click', function (e) {
				e.stopImmediatePropagation();
				$(this).find('input[type="checkbox"]').trigger('click');
			})

			$('ul.dropdown-menu li input[type="radio"]').on('click', function (e) {
				e.stopImmediatePropagation();
				loadItems();
			})

			$('input[type="checkbox"].select-all').on('changed', function(e, isSelf) {

				var self = $(this);
				var list = self.closest('.dropdown-menu');
				var totalSelectedIndex = $('.total-selected', self.closest('.btn-group'));
				var checkboxes = $('input[type="checkbox"]:not(.select-all)', list);
				var totalChecked = $('input[type="checkbox"]:not(.select-all)', list).filter(':checked').length;

				totalSelectedIndex.text('('+totalChecked+') ').toggle(totalChecked ? true : false);

				if (!isSelf) {
					self.prop('checked', checkboxes.length==totalChecked ? true : false );
				}
			})

			$('ul.sort-menu li').on('sort', function (e, manuallyTriggered) {

				var self = $(this),
					list = self.closest('ul'),
					index = $('.badge', self),
					checkbox = $('input[type="checkbox"]', self),
					currentIndex=0, sort=[], captions=[];

				// get current index
				var currentIndex = index.length && index.text() ? parseInt(index.text()) : 0;

				// if checkbox is not checked, remove index value
				var resetCurrentIndex = checkbox.length && !checkbox.is(':checked') ? true : false;

				// reset index
				if (resetCurrentIndex) index.text('');
				
				// get max 
				var max = 0;
				
				// reset other indexes
				$('.badge', list).each( function(i, el) { 
					
					var val = $(el).text();

					val = val ? parseInt(val) : '';
					max = val ? Math.max(max, val) : max;  

					// reduce index
					if (resetCurrentIndex && val &&  val > currentIndex) {
						val--;
						$(el).text(val);
					}
				})
				
				// incres current index
				if (!resetCurrentIndex) {
					max++;
					index.text(max);
				}

				// build captions
				$('li', list).each(function(i, el) {

					var elIndexVal = $('.badge', $(el)).text();
					var elCheckbox = $('input[type="checkbox"]', $(el));

					if (elCheckbox.length && elCheckbox.is(':checked')) {
						var i = parseInt(elIndexVal);

						i--;
						sort[i] = elCheckbox.val();
						captions[i] = elCheckbox.prop('title');
					}
				})

				var sortValue = sort.length ? sort.join(',') : '';
				$('#sort').val(sortValue);

				var captionTitle = captions.length ? captions.join(', ') : 'Default';
				$('.sort-captions', self.closest('.btn-group')).text(captionTitle);

				if (!manuallyTriggered) {
					loadItems();
				}

			})

			$("#resetFilters").on('click', function(e) {

				e.preventDefault();
				e.stopPropagation();

				$('input[type="checkbox"]', $('.dropdown-menu:not(.sort-menu):not(.print-menu)')).prop('checked', false);
				$('.total-selected', $('.btn-group')).text('').hide();

				loadItems();

				return false;
			})

			$("#btnPrintItemBooklet").on('click', function(e) {

				e.preventDefault();
				e.stopPropagation();

				var self = $(this);

				retailnet.loader.show();

				$.ajax({
					url: self.prop('href'), 
					type: "POST",
					dataType : "json",
					data: $('.print-menu :input, #items :input').serialize(),
					success : function (xhr) {
						
						if (xhr && xhr.errors) {
							$.each(xhr.errors, function(i, msg) {
								parent.retailnet.notification.error(msg);
							})
							return;
						}

						if (xhr && xhr.file) {
							parent.window.location = xhr.file;
						}
					},
					complete: function() {
						retailnet.loader.hide();
					}
				})

				return false;
			})

			$('.selct-all-items').on('change', function() {
				var checked = $(this).is(':checked');
				var items = $('#items tbody input[type="checkbox"]');
				items.prop('checked', checked);			
			})

			$('.selct-all-items').on('changed', function(e, manuallyTriggered) {
				var items = $('#items tbody input[type="checkbox"]').length;
				var itemsSelected = $('#items tbody input[type="checkbox"]:checked').length;		
				$(this).prop('checked', items==itemsSelected ? true : false);	
			})

			$(document).on('change', '#items tbody input[type="checkbox"]', function(e) {
				$('.selct-all-items').trigger('changed');
			})

			var loadItems = function() {

				retailnet.loader.show();

				var data = $('.modalbox-content-container :input').serialize();
				var targetContainer = $('tbody', $('#items')).empty();

				$.ajax({
					url: '/applications/modules/item/items.booklet.list.php', 
					type: "POST",
					dataType : "json",
					data: data,
					success : function (data) {

						$('.selct-all-items').prop('disabled', data && data.length ? false : true);

						if (data) {
							
							for (var i in data) {

								var item = data[i];
								var tr = $("<tr />");
								var td;

								var input = $("<input />").prop({
									type: 'checkbox',
									name: 'items['+item.id+']',
									checked: item.checked,
									value: 1
								});

								td = $("<td />").append(input).appendTo(tr);
								td = $("<td />").append(item.code).appendTo(tr);
								td = $("<td />").append(item.name).appendTo(tr);
								td = $("<td />").prop('align', 'center').append(item.active).appendTo(tr);
								
								targetContainer.append(tr);
							}

							$('.selct-all-items').trigger('changed');

						} else {
							
							var tr = $("<tr />");
							
							var td = $("<td />").prop({
								colspan: 3,
								'class': '-emptybox'
							}).text('No matching records found')
							.appendTo(tr);

							targetContainer.append(tr);
						}
					},
					complete: function() {
						retailnet.loader.hide();
					}
				})
			}

			$(function() {

				// öoad entity items
				if ($('#entity').val()) {
					loadItems();
				}
			})

		})
	</script>
	
</head>
<body class="adomat">

	<div class="modalbox-container">
		<div id="modalbox" class="adomat modalbox">
			<div class="modalbox-header">
				<div class="title">Print Booklet</div>
				<span class="fa-stack ado-modal-close close">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-times fa-stack-1x fa-inverse"></i>
				</span>
			</div>
			<div class="modalbox-content-container">
				
				<div class="modalbox-topbar">
					<div class="bootstrap">
						<?php if (!$_REQUEST['nofilters']) : ?>
							<div class="btn-caption">Filters: </div>
						<?php endif; ?>
						<div class="btn-group btn-group-xs" role="group">
							
							<input type="hidden" name="entity" id="entity" value="<?php echo $_ENTITY ?>" >

							<?php 
								if ($_ENTITY_VALUES) {
									foreach ($_ENTITY_VALUES as $name => $value) {
										echo "<input type=\"hidden\" name=\"entity_values[$name]\" value=\"$value\" >";
									}
								}
							?>

							<?php if (!$_REQUEST['nofilters']) : ?>
								<button id="resetFilters" type="button" class="btn btn-default btn-xs">All Items</button>
							<?php endif; ?>
							
							<?php if ($_ITEM_CATEGORIES) : ?>
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
									Item Categories <i class="total-selected"></i><span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li class="dropdown-header"><label><input type="checkbox" class="select-all"> Select all categories</label></li>
									<?php 
										foreach ($_ITEM_CATEGORIES as $row) {
											echo "<li><a href=\"#\"><label><input type=\"checkbox\" name=\"categories[{$row[id]}]\" value=\"1\"> {$row[name]}</label></a></li>";
										}
									?>
								</ul>
							</div>
							<?php endif; ?>
							
							<?php if ($_ITEM_SUBCATEGORIES) : ?>
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
									Item Subcategories <i class="total-selected"></i><span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li class="dropdown-header"><label><input type="checkbox" class="select-all"> Select all subcategories</label></li>
									<?php 
										foreach ($_ITEM_SUBCATEGORIES as $row) {
											echo "<li><a href=\"#\"><label><input type=\"checkbox\" name=\"subcategories[{$row[id]}]\" value=\"1\"> {$row[name]}</label></a></li>";
										}
									?>
								</ul>
							</div>
							<?php endif; ?>

							<?php if ($_PRODUCT_LINES) : ?>
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Product Lines <i class="total-selected"></i><span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li class="dropdown-header"><label><input type="checkbox" class="select-all"> Select all product lines</label></li>
									<?php 
										foreach ($_PRODUCT_LINES as $row) {
											echo "<li><a href=\"#\"><label><input type=\"checkbox\" name=\"product_lines[{$row[id]}]\" value=\"1\"> {$row[name]}</label></a></li>";
										}
									?>
								</ul>
							</div>
							<?php endif; ?>

							<?php if(!$_REQUEST['nofilters'])
							{
							
								if ($_ACTIVE) : ?>
								<div class="btn-group">
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Visibility <small>(<?php echo $_ACTIVE[$currentActive] ?>)</small> <span class="caret"></span>
									</button>
									<ul class="dropdown-menu">
										<?php 
											foreach ($_ACTIVE as $k => $v) {
												$selected = $k==$currentActive ? "checked=checked" : null;
												echo "<li class='radio'><a href=\"#\"><label><input type=\"radio\" name=\"active\" value=\"$k\" $selected > $v</label></a></li>";
											}
										?>
									</ul>
								</div>
								<?php endif; 
							}
							?>


							<?php if ($_SUPPLIERS) : ?>
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Suppliers <i class="total-selected"></i> <span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li class="dropdown-header"><label><input type="checkbox" class="select-all"> Select all suppliers</label></li>
									<?php 
										foreach ($_SUPPLIERS as $row) {
											echo "<li><a href=\"#\"><label><input type=\"checkbox\" name=\"suppliers[{$row[id]}]\" value=\"1\"> {$row[name]}</label></a></li>";
										}
									?>
								</ul>
							</div>
							<?php endif; ?>

						</div>

						<div class="pull-right">
							<div class="btn-caption">Sort by: </div>
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Item Code">
									<span class="sort-captions">Code</span> <span class="caret"></span>
								</button>
								<ul class="dropdown-menu dropdown-menu-right sort-menu">
									<input type="hidden" name="sort" id="sort" >
									<li>
										<a href="#">
											<label><input type="checkbox" value="item_code" checked="checked" title="Code"> Code</label>
											<span class="badge pull-right">1</span>
											<div class="clearfix"></div>
										</a>
									</li>
									<li>
										<a href="#">
											<label><input type="checkbox" value="item_name" title="Name"> Name</label>
											<span class="badge pull-right"></span>
											<div class="clearfix"></div>
										</a>
									</li>
									<li>
										<a href="#">
											<label><input type="checkbox" value="item_category_name" title="Category"> Category</label>
											<span class="badge pull-right"></span>
											<div class="clearfix"></div>
										</a>
									</li>
									<li>
										<a href="#">
											<label><input type="checkbox" value="product_line_name" title="Product Line"> Product Line</label>
											<span class="badge pull-right"></span>
											<div class="clearfix"></div>
										</a>
									</li>
									<li>
										<a href="#">
											<label><input type="checkbox" value="address_company" title="Supplier"> Supplier</label>
											<span class="badge pull-right"></span>
											<div class="clearfix"></div>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="modalbox-content">
					<div class="bootstrap">
						<table id="items" class="table table-condensed table-striped">
							<thead>
								<tr>
									<th nowrap="nowrap" width="20"><input type="checkbox" class="selct-all-items" disabled="disabled"></th>
									<th nowrap="nowrap" width="20%">Item Code</th>
									<th nowrap="nowrap">Item Name</th>
									<th nowrap="nowrap" width="40px">Active</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="3" class="-emptybox">
										Please select filter
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modalbox-footer">
				<div class="modalbox-actions ado-actions">
					<div class="bootstrap print-menu">
						<input type="hidden" name="sections[cover]" value="1">
						<div class="btn-group dropup pull-right">
							<a id="btnPrintItemBooklet" role="button" class="btn btn-default btn-sm" href="/applications/modules/item/print.info.php">Print</a>
							<a role="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right print-menu" aria-labelledby="dLabel">
								<li class="dropdown-header">Print Sections</li>
								<li><a href="#"><label><input type="checkbox" disabled="disabled" checked="checked" value="1"> Cover</label></a></li>
								<li><a href="#"><label><input name="sections[properties]" type="checkbox" checked="checked" value="1"> Product Properties</label></a></li>
								<li><a href="#"><label><input name="sections[technical]" type="checkbox" checked="checked" value="1"> Technical Drawing</label></a></li> 
								<li><a href="#"><label><input name="sections[logistic]" type="checkbox" checked="checked" value="1"> Logistic Information</label></a></li>
								<li><a href="#"><label><input name="sections[remarks]" type="checkbox" checked="checked" value="1"> Remarks</label></a></li>
								<li><a href="#"><label><input name="sections[packing]" type="checkbox" checked="checked" value="1"> Packing Informations</label></a></li>
								<li><a href="#"><label><input name="sections[spareparts]" type="checkbox" checked="checked" value="1"> Spare Parts</label></a></li>
								<li role="separator" class="divider"></li>
								<li class="dropdown-header">Include</li>
								<li><a href="#"><label><input name="sections[files]" type="checkbox" value="1"> Files</label></a></li>
								<li><a href="#"><label><input name="sections[certificates]" type="checkbox" value="1"> Certificates</label></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>