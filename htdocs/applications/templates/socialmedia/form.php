<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'socialmedia',
		'action' => '/applications/modules/socialmedia/submit.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->socialmedia_id(
		Form::TYPE_HIDDEN
	);
	
	
	
	$form->socialmedia_brand_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->socialmedia_channel_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->socialmedia_language_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->socialmedia_country_id(
		Form::TYPE_SELECT
	);

	$form->socialmedia_posaddress_id(
		Form::TYPE_SELECT
	);

	$form->socialmedia_url(
		Form::TYPE_TEXT
	);
	
	$form->socialmedia_video_url(
		Form::TYPE_TEXT,
		Form::PARAM_LABEL
	);


	$form->socialmedia_video_description(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);
	
	
	// fieldset: dimensions
	$form->fieldset('Social Media', array(
		'socialmedia_brand_id',
		'socialmedia_channel_id',
		'socialmedia_language_id',
		'socialmedia_country_id',
		'socialmedia_posaddress_id',
		'socialmedia_url',
		'socialmedia_video_url',
		'socialmedia_video_description'
	));


		
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
	
?>

<style type="text/css">

	form textarea {
		width: 400px;
	}
</style>

<script language="javascript">

	 $("#socialmedia_country_id").change(function () {

		$('#socialmedia_posaddress_id').empty();
		$.ajax({
			url: '/applications/helpers/ajax.poslocations.php',
			type: 'POST',
			data: {country : $("#socialmedia_country_id").val()},
			success: function(poslocations) {
				
				$.each(poslocations,function(key, value_array) 
				{
					$("#socialmedia_posaddress_id").append('<option value=' + value_array['posaddress_id'] + '>' + value_array['posname'] + '</option>');
				});
			}
		});
	});

</script>