<div class="page-header">
	<h4 class="page-title">
		<?php 
			
			if ($buttons['add']) {
				echo "<a href='{$buttons[add]}' id='addNewSticker' class='btn btn-xs btn-primary'><i class='fa fa-plus'></i></a>";
			} 

			echo Request::instance()->title 
		?>
	</h4>
</div>
<div class="stickers">
	<?php 
		if ($stickers) {
			foreach ($stickers as $row) {
				echo "<div class='item'>";
				echo "<a href='{$row[url]}' class='sticker' style='background-image: url({$row[image]})'>";
				if ($row['title']) echo "<p>{$row[title]}</p>";
				echo "</a>";
				echo "</div>";
			}
		}
	?>
</div>