<div class="form-container form-container-xs">
	<div class="page-header">
		<h3 class="page-title"><?php echo $title ?></h3>
	</div>
	<form id="sticker" method="post" class="form-horizontal" action="<?php echo $form['submit']; ?>" >
		<input type="hidden" name="sticker_id" id="sticker_id" value="<?php echo $data['sticker_id'] ?>">
		<input type="hidden" name="application" id="application" value="<?php echo $data['application'] ?>">
		<input type="hidden" name="user" id="user" value="<?php echo $data['user'] ?>">
		<input type="hidden" name="hasupload" id="hasupload">
		<div class="form-group">
			<div class="col-sm-12">
				<div class="sticker-cropper-container">
					<input type="hidden" name="sticker_image" id="sticker_image" value="<?php echo $data['sticker_image'] ?>">
					<div id="stickerCropper" class="sticker-cropper"></div>
				</div>
				<input class="form-control" name="sticker_title" placeholder="Sticker Title" value="<?php echo $data['sticker_title'] ?>" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<?php if ($buttons['back']) : ?>
				<a href="<?php echo $buttons[back] ?>" class="btn btn-sm btn-default" ><i class="fa fa-chevron-left"></i> Back</a>
				<?php endif; ?>	
				<?php if ($buttons['save']) : ?>
					<button type="submit" data-url="<?php echo $buttons['save'] ?>" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Save</button>
				<?php endif; ?>				
				<?php if ($buttons['delete']) : ?>
					<button type="reset" data-url="<?php echo $buttons['delete'] ?>" class="btn btn-sm btn-danger" data-message="Are you sure to remove this Sticker?"><i class="fa fa-times"></i> Delete</button>
				<?php endif; ?>
			</div>
		</div>
	</form>
</div>