<article class="news-article" data-id="{id}">
	<div class="row article-title">
		<div class="col-xs-12"><h5>{title}</h5></div>
	</div>
	<div class="row article-teaser">{teaser}</div>
	<div class="row article-content" id="article-body-{id}">{templates}</div>
	<div class="row article-bottom">
		<div class="col-xs-12"><span class="badge"><i class="fa fa-book"></i></span></div>
	</div>
</article>