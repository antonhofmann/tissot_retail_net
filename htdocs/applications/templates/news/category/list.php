<link href="/public/scripts/jquery-ui/jquery-ui.min.css" rel="stylesheet"></link>
<script src="/public/scripts/jquery-ui/jquery-ui.min.js"></script>
<div class="page-header">
	<h3 class="page-title"><?php echo $this->title ?></h3>
</div>
<div class="categories-list">
	<table class='table table-hover table-striped' id='categories' >
		<thead>
			<tr>
				<th width='20px'><a href='/gazette/categories/add' class='btn btn-xs btn-primary' ><i class='fa fa-plus'></i></a></th>
				<th>Category Name</th>
				<th width='40%'>Section</th>
			</tr>
		</thead>
		<?php
			if ($categories) {

				echo "<tbody>";

				foreach ($categories as $category) {
					$id = $category['id'];
					echo "<tr data-id='$id'>";
					echo "<td class='section-order' ><i class='fa fa-bars'></i></td>";
					echo "<td><a href='/gazette/categories/data/$id'>{$category[name]}</a></td>";
					echo "<td>{$category[section]}</td>";
					echo "</tr>";
				}

				echo "</tbody>";
			}
		?>
	</table>
</div>