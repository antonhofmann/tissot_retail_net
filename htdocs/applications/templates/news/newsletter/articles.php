<link href="/public/scripts/jquery-ui/jquery-ui.min.css" rel="stylesheet"></link>
<script src="/public/scripts/jquery-ui/jquery-ui.min.js"></script>
<div class="container-fluid form-container">
	<input type="hidden" name="newsletter_id" id="newsletter_id" value="<?php echo $id ?>" >

	<!-- page header -->
	<div class="page-header">
		<h4 class="page-title">Newsletter #<?php echo $number ?></h4>
		<div id="newsletter-tabs" class="newsletter-tabs navbar-collapse collapse"><?php echo $tabs; ?></div>
		<button type="button" class="navbar-toggle collapsed btn btn-sm btn-default" data-toggle="collapse" data-target="#newsletter-tabs" aria-expanded="false" aria-controls="newsletter-tabs">
			<i class="fa fa-bars"></i>
		</button>
		<ul id="breadcrumps" class="process-staps"></ul>
		<input type="hidden" name="newsletter_id" id="newsletter_id" value="<?php echo $id ?>" >
	</div>

	<section class="articles">
		<div class="row">
			<div class="col-xs-12 newsletter-articles">
				<div class="flat-tabs">
					<ul class="nav nav-tabs">
						<li role="presentation" class="active">
							<a href="#articles" id="articles-tab" role="tab" data-toggle="tab" aria-controls="articles" aria-expanded="true">Newsletter Articles</a>
						</li>
						<li role="presentation">
							<a href="#unassigned" id="unassigned-tab" role="tab" data-toggle="tab" aria-controls="unassigned" aria-expanded="true">Unassigned Articles</a>
						</li>
						<li role="presentation">
							<a href="#other" id="other-tab" role="tab" data-toggle="tab" aria-controls="other" aria-expanded="true">Other Articles</a>
						</li>
					</ul>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade active in" id="articles" aria-labelledby="articles-tab">
							<div class="ajax-container">
								
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="unassigned" aria-labelledby="unassigned-tab">
							<div class="flex-container ajax-container"></div>
							<div class="pagination" class="col-xs-12">
								<a id="load-unassigned" href="/applications/modules/news/newsletter/newsletter.articles.php?page=1">Load</a>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="other" aria-labelledby="other-tab">
							<div class="flex-container ajax-container"></div>
							<div class="pagination" class="col-xs-12">
								<a id="load-other" href="/applications/modules/news/newsletter/newsletter.articles.php?page=1">Load</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<script id="teaser-template" type="text/template">
	<div class="article article-{{id}}" data-id="{{id}}">
		<div class="article-header">
			<h4 class="title">{{title}}</h4>
			<div class="article-actions">
				<span class="badge">{{state}}</span>
				{{#roles_restrictions}}
					<span class="badge roles article-restrictions has-popover" data-toggle="popover" data-trigger="click" data-content="{{roles_restrictions}}" data-placement="top">
						<i class="fa fa-user"></i>
					</span>
				{{/roles_restrictions}}
				{{#companies_restrictions}}
					<span class="badge companies article-restrictions has-popover" data-toggle="popover" data-original-title="Role Restrictions" data-trigger="click" data-content="{{companies_restrictions}}" data-placement="top">
						<i class="fa fa-home"></i>
					</span>
				{{/companies_restrictions}}
				<span class="badge article-action article-remove"><i class="fa fa-times"></i></span>
				<span class="badge article-action article-sort"><i class="fa fa-bars sort"></i></span>
			</div>
		</div>
		<div class="article-body">
			<div class="teaser-image"><img src='{{teaser.image}}' class='img-responsive'></div>
			<div class="teaser-text">
				<h5>{{&teaser.title}}</h5>
				{{&teaser.text}}
			</div>
		</div>
		<div class="article-footer">
			{{#info}}
				<span class="info">
					<label>{{caption}}:</label> {{value}}
				</span>
			{{/info}}
		</div>
	</div>
</script>

<!-- article thumb template -->
<script id="article-template" type="text/template">
	<div class="panel-box article-{{id}}" data-id="{{id}}" data-type="{{type}}" data-section="{{section}}" {{#teaser}}style="background-image: url({{teaser.image}}); "{{/teaser}} >
		<div class="front">
			<div class="panel">
				<div class="panel-footer">
					<h4>{{title}} </h4>
					<h5>{{{author}}}</h5>	
					{{#desired_publish_date}}<h6>Desired publishing date: {{desired_publish_date}}</h6>{{/desired_publish_date}}			
				</div>
				<mark class="mark-top-left">{{state}}</mark>
				<mark class="action article-add mark-top-right">
					<i class="fa fa-plus fa-2x"></i>
				</mark>
			</div>
		</div>
		<div class="back">
			<dl class="dl-horizontal">
				<dt>Author</dt><dd>{{{author}}}</dd>
				<dt>Status</dt><dd>{{state}}</dd>
				<dt>Section</dt><dd>{{section_name}}</dd>
				<dt>Category</dt><dd>{{category}}</dd>
				<dt>Featured</dt><dd>{{featured}}</dd>
				<dt>Created on</dt><dd>{{created_date}}</dd>
				<dt>Published on</dt><dd>{{published_date}}</dd>
				<dt>Expiry on</dt><dd>{{expiry_date}}</dd>
			</dl>
		</div>
		<mark class="back-toggle">Show Details</mark>
	</div>
</script>