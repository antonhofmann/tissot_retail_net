<link href="/public/scripts/jquery-ui/jquery-ui.min.css" rel="stylesheet"></link>
<script src="/public/scripts/jquery-ui/jquery-ui.min.js"></script>
<link href="/public/css/datepicker.css" rel="stylesheet"></link>
<div class="container-fluid form-container">
	<input type="hidden" name="newsletter_id" id="newsletter_id" value="<?php echo $id ?>" >

	<!-- page header -->
	<div class="page-header">
		<h4 class="page-title">Newsletter #<?php echo $number ?></h4>
		<div id="newsletter-tabs" class="newsletter-tabs navbar-collapse collapse"><?php echo $tabs; ?></div>
		<button type="button" class="navbar-toggle collapsed btn btn-sm btn-default" data-toggle="collapse" data-target="#newsletter-tabs" aria-expanded="false" aria-controls="newsletter-tabs">
			<i class="fa fa-bars"></i>
		</button>
		<ul id="breadcrumps" class="process-staps"></ul>
		<input type="hidden" name="newsletter_id" id="newsletter_id" value="<?php echo $id ?>" >
	</div>

	<section class="recipients">
		<div class="row">
			<div class="col-xs-12">
				<div class="flat-tabs">
					<ul class="nav nav-tabs">
						<li role="presentation" class="active">
							<a href="#swatch-recipients" id="tab-swatch-recipients" role="tab" data-toggle="tab" aria-controls="swatch-recipients" aria-expanded="true">Swatch Recipients <span class="badge">0</span></a>
						</li>
						<li role="presentation">
							<a href="#additional-recipients" id="tab-additional-recipients" role="tab" data-toggle="tab" aria-controls="additional-recipients" aria-expanded="true">Additional Recipients <span class="badge">0</span></a>
						</li>
						<li role="presentation">
							<a href="#send-newsletter" id="tab-send-newsletter" role="tab" data-toggle="tab" aria-controls="send-newsletter" aria-expanded="true">Send Newsletter</a>
						</li>
					</ul>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade active in" id="swatch-recipients" aria-labelledby="tab-additional-recipients">
							<?php
								if ($swatchRecipients) {
									
									$list = null;

									foreach ($swatchRecipients as $role => $row) {

										echo "<table class='table table-hover table-responsive table-recipients' data-section='standard-recipients' data-id='$role'>";
									
										echo "
											<thead>
												<tr>
													<th width='20px'><input type='checkbox' class='group-recipients' value='$role' ></th>
													<th>
														<a data-toggle='collapse' href='#body-$role' aria-expanded='true' aria-controls='body-$role'>
															{$row[title]}
															<span class='total-group-recipients badge'></span>
														</a>
													</th>
													<th width='10%'>Views</th>
													<th width='20%' nowrap='nowrap'>Views from E-mail</th>
													<th width='20%'>Date Sent</th>
												</tr>
											</thead>
										";

										echo "<tbody id='body-$role' class='collapse'>";

										foreach ($row['users'] as $user) {
											
											$i = $user['user'];
											$views = $tracks['views'] ? $tracks['views'][$i] : null;
											$eviews = $tracks['eviews'] ? $tracks['eviews'][$i] : null;

											echo "
												<tr>
													<td colspan='2'>
														<div class='checkbox'>
															<label><input type='checkbox' value='1' class='recipient' data-id='{$user[user]}' {$user[checked]} > {$user[name]}</label>
														</div>
													</td>
													<td nowrap='nowrap'>$views</td>
													<td nowrap='nowrap'>$eviews</td>
													<td nowrap='nowrap'>{$user[sent]}</td>
												</tr>
											";
										}

										echo "</tbody></table>";
									}
								}
							?>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="additional-recipients" aria-labelledby="tab-additional-recipients">
							<p><button id="btnAddAdditionalRecipient" class="btn btn-primary btn-sm" data-toggle='collapse' href='#addAdditionalRecipient' aria-expanded='true' aria-controls='addAdditionalRecipient'><i class="fa fa-plus"></i> Add Recipient</button></p>
							<form id="addAdditionalRecipient" class="collapse">
								<div class="well">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<div class="form-group">
												<input type="text" name="news_recipient_firstname" id="news_recipient_firstname" class="form-control required" placeholder="First Name" />
											</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="form-group">
												<input type="text" name="news_recipient_name" id="news_recipient_name" class="form-control required" placeholder="Name" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<div class="form-group">
												<input type="text" name="news_recipient_email" id="news_recipient_email" class="form-control required" placeholder="E-mail" />
											</div>
										</div>
									</div>
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</form>
							<table class="table table-hover table-responsive table-recipients" data-section="additional-recipients">
								<thead>
									<tr>
										<th width='20px'><input type='checkbox' class='group-recipients'></th>
										<th>
											<a href="#">All additional recipients</a>
											<span class='total-group-recipients badge'></span>
										</th>
										<th width='20%' nowrap='nowrap'>Views from E-mail</th>
										<th width="20%">Date Sent</th>
									</tr>
								</thead>
								<tbody id="body-additional">
									<?php 
										if ($additionalUsers) {
											foreach ($additionalUsers as $user) {
												
												$i = $user['user'];
												$eviews = $tracks['eviews-additional'] ? $tracks['eviews-additional'][$i] : null;

												echo "<tr>";
												echo "
													<td colspan='2'>
														<div class='checkbox'>
															<label><input type='checkbox' class='recipient additional-recipient' data-id='{$user[user]}' {$user[checked]} > {$user[name]}</label>
														</div>
													</td>
												";
												echo "<td>$eviews</td>";
												echo "<td>{$user[sent]}</td>";
												echo "</tr>";
											}
										}
									?>
								</tbody>
							</table>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="send-newsletter" aria-labelledby="tab-send-newsletter">
							<div class="sendmail-info-box">
								<table class="table">
									<tr>
										<td id="newsletter-total-articles"></td>
										<td>Articles</td>
									</tr>
									<tr>
										<td id="total-swatch-recipients"></td>
										<td>Standard Recipients</td>
									</tr>
									<tr>
										<td id="total-additional-recipients"></td>
										<td>Additional Recipients</td>
									</tr>
								</table>
								<?php if ( $data['newsletter_publish_state_id']==2 || $data['newsletter_publish_state_id']==3 ) : ?>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-calendar"></i> Send Date: </span>
										<input type="text" class="form-control datepicker datepicker-resize" name="newsletter_send_date" id="newsletter_send_date" readonly="readonly" placeholder="Newsletter to be sent on">
										<span class="input-group-addon remove-terminated-date"><i class="fa fa-times"></i></span>
									</div>
								</div>
								<?php endif; ?>
								<div class="form-group">
									<div class="btn-group btn-group-justified" id="actions">
										<a href="/applications/modules/news/newsletter/send.php" class="btn btn-primary" id="action-test-newsletter" ><i class="fa fa-envelope-o"></i> Test Mail</a>
										<a href="/gazette/newsletters/preview/<?php echo $id ?>" class="btn btn-info" id="action-preview-newsletter" target="_blank" ><i class="fa fa fa-external-link"></i> Preview</a>
									</div>
								</div>
								<div class="form-group">
									<a href="/applications/modules/news/newsletter/send.php?id=<?php echo $id ?>" class="btn btn-success btn-block btn-lg" id="action-send-newsletter"><i class="fa fa-envelope"></i> Send Newsletter</a>
								</div>
								<div id="newsletter-alerts"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<div id="template-test-mail" class="hide">
	<form id="testForm" action="/applications/modules/news/newsletter/send.php">
		<div class="form-group">
			<input type="hidden" name="id" value="<?php echo $id ?>" >
			<input type="text" class="form-control required" name="test" id="test" placeholder="Enter email" value="<?php echo User::instance()->email ?>" >
		</div>
	</form>
</div>
