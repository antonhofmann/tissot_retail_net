<div class="page-header">
	<h3 class="page-title">Newsletters <small id="filter-caption"></small></h3>
</div>
<ul class="list-filters">
	<?php
		if ($buttons['add']) {
			echo "<li><a href=\"/gazette/newsletters/composer\" class=\"btn btn-primary btn-sm\" ><i class=\"fa fa-plus\"></i> Add New</a></li>";
		}
	?>
	<!-- quick search -->
	<li>
		<div class="btn-group btn-group-sm filter-group group-search" role="group">
			<button type="button" class="btn btn-default disabled">Search</button>
			<input type="text" name="search" id="quicksearch" value="<?php echo $filterSearch; ?>" />
		</div>
	</li>
	<!-- filter years -->
	<?php if ($years) : ?>
	<li>
		<div id="group-years" class="btn-group btn-group-sm filter-group" role="group">
			<div class="dropdown filter-dropdown dropdown-years" data-filter-group="year">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownYears" data-toggle="dropdown" title="Years">
					<span class="btn-caption">Years</span> <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownYears">
					<?php 
						foreach ($years as $year) {
							$val = preg_replace( '/[^0-9]/', '', $year['year']) ? $year['year'] : null;
							echo "<li><a href='#' data-search='$val' data-caption='{$year[year]}' {$year[active]} >{$year[year]}</a></li>";
						}
					?>
				</ul>
			</div>
		</div>
	</li>
	<?php endif; ?>
	<!-- filter newsletters -->
	<?php if ($newsletters) : ?>
	<li>
		<div id="group-newsletters" class="btn-group btn-group-sm filter-group" role="group">
			<div class="dropdown filter-dropdown dropdown-newsletters" data-filter-group="number">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownNewsletters" data-toggle="dropdown" title="Gazette">
					<span class="btn-caption">Gazette</span> <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownNewsletters">
					<?php 
						foreach ($newsletters as $row) {
							$caption = preg_replace( '/[^0-9]/', '', $row['number']) ? 'Gazette '.$row['number'] : $row['title'];
							echo "<li><a href='#' data-search='{$row[number]}' data-caption='$caption' {$row[active]} >{$row[title]}</a></li>";
						}
					?>
				</ul>
			</div>
		</div>
	</li>
	<?php endif; ?>
	<!-- filter group states -->	
	<li>
		<div id="group-states" class="btn-group btn-group-sm filter-group" role="group" data-filter-group="state">
			<button type="button" class="btn btn-default disabled">States</button>
			<div class="bnt-group-responsive-caption">
				<label></label>
				<span class="caret-holder"><span class="caret"></span></span>
			</div>
			<div class="btn-group btn-group-sm bnt-group-responsive btn-filters" role="group">
				<?php 
					if ($states) {
						foreach ($states as $state) {
							echo "<button 
								type=\"button\" 
								class=\"btn btn-default filter {$state[active]}\" 
								data-id=\"{$state[id]}\" 
								data-filter=\"{$state[filter]}\">
								{$state[name]}
							</button>";
						}
					}
				?>
			</div>
		</div>
	</li>
	<!-- sort -->
	<li>
		<div id="group-sort" class="btn-group btn-group-sm filter-group" role="group" data-filter-group="sort">
			<button type="button" class="btn btn-default disabled">Sort</button>
			<div class="bnt-group-responsive-caption">
				<label></label>
				<span class="caret-holder"><span class="caret"></span></span>
			</div>
			<div class="btn-group btn-group-sm bnt-group-responsive" role="group">
				<?php
					if ($sorts) {
						foreach ($sorts as $button) {
							echo "<button 
								type=\"button\" 
								class=\"btn btn-default sort {$button[active]}\" 
								data-filter=\"{$button[filter]}\" 
								data-ascending=\"{$button[ascending]}\" >
								{$button[title]} {$button[icon]}
							</button>";
						}
					}
				?>
			</div>
		</div>
	</li>
	<li>
		<div id="group-view" class="btn-group btn-group-sm filter-group" role="group" data-filter-group="view">
			<button type="button" class="btn btn-default dropdown-toggle disabled">View</button>
			<?php
				if ($views) {
					foreach ($views as $button) {
						echo "<button 
							type=\"button\" 
							class=\"btn btn-default view {$button[active]}\" 
							data-filter=\"{$button[filter]}\">
							{$button[title]}
						</button>";
					}
				}
			?>
		</div>
	</li>
	<div class="clearfix"></div>
</ul>

<div id="newsletters-container">
	<div id="newsletters" class="row"></div>
	<div class="pagination">
		<a href="#" class="next" data-page="<?php echo $page ?>">Load</a>
	</div>
</div>

<script id="newsletter-template" type="text/template">
	<div id="newsletter-{{id}}" class="newsletter col-xs-12 col-sm-6 col-md-4 col-lg-3 {{filter}}"  data-id="{{id}}">
		<div class="panel newsletter-panel" {{#image}}style="background-image: url({{image}}); background-size: cover; "{{/image}}>
			<div class="panel-footer">
				<h4><a href="{{link}}" class="title">{{number}} {{title}}</a></h4>
				{{#published}}
				<h6 class="published" data-sort="{{published_stamp}}">{{published}}</h6>
				<h6 class="created hide" data-sort="{{created_date_timestamp}}">{{created_date}}</h6>
				{{/published}}
				{{^published}}
				<h6 class="published hide" data-sort="{{published_stamp}}">{{published}}</h6>
				<h6 class="created" data-sort="{{created_date_timestamp}}">{{created_date}}</h6>
				{{/published}}
			</div>
			<h6 class="state-caption state">{{state_name}}</h6>
		</div>
	</div>
</script>