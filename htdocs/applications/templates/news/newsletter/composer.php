<link href="/public/scripts/jquery-ui/jquery-ui.min.css" rel="stylesheet"></link>
<script src="/public/scripts/jquery-ui/jquery-ui.min.js"></script>
<link href="/public/css/datepicker.css" rel="stylesheet"></link>
<script type="text/javascript" src="/public/scripts/tinymce4/js/tinymce/tinymce.min.js"></script>
<link rel="stylesheet" href="/public/scripts/fileuploader/css/style.css">
<link rel="stylesheet" href="/public/scripts/fileuploader/css/jquery.fileupload.css">
<script src="/public/scripts/fileuploader/js/jquery.ui.widget.js"></script>
<script src="/public/scripts/fileuploader/js/jquery.iframe-transport.js"></script>
<script src="/public/scripts/fileuploader/js/jquery.fileupload.js"></script>
<!--[if (gte IE 8)&(lt IE 10)]><script src="/public/scripts/fileuploader/js/cors/jquery.xdr-transport.js"></script><![endif]-->

<div class="container-fluid form-container">

	<!-- page header -->
	<div class="page-header">
		<h4 class="page-title">Newsletter #<?php echo $number ?></h4>
		<div id="newsletter-tabs" class="newsletter-tabs navbar-collapse collapse"><?php echo $tabs; ?></div>
		<button type="button" class="navbar-toggle collapsed btn btn-sm btn-default" data-toggle="collapse" data-target="#newsletter-tabs" aria-expanded="false" aria-controls="newsletter-tabs">
			<i class="fa fa-bars"></i>
		</button>
		<ul id="breadcrumps" class="process-staps"></ul>
	</div>

	<form id="newsletter" action="/applications/modules/news/newsletter/newsletter.php">
		<input type="hidden" name="newsletter_id" id="newsletter_id" value="<?php echo $id ?>" >

		<!-- newsletter title -->
		<div class="row">
			<div class="col-xs-12 col-sm-7 newsletter-title">
				<div class="form-group">
					<input type="text" class="form-control field newsletter_title" name="newsletter_title" id="newsletter_title" placeholder="Title">
				</div>
			</div>
			<div class="col-xs-12 col-sm-5 newsletter-actions">
				<div class="btn-group btn-group-justified" id="actions"></div>
			</div>
		</div>

		<!-- newsletter image -->
		<div class="row">
			<div class="col-xs-12">
				<div class="image-cropper-container has-popover" data-toggle="popover" data-placement="top" data-content="Minimum image size 1200x520px.">
					<input type="hidden" name="newsletter_image" id="newsletter_image" class="field field-crop" >
					<div class="image-cropper" id="cropper" ></div>
				</div>
			</div>
		</div>

		<!-- newsletter content -->
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<textarea name="newsletter_text" id="newsletter_text" placeholder="Content" class="form-control field editor newsletter_text"></textarea>
				</div>
			</div>
		</div>

		<!-- newsletter files -->
		<div class="row">
			<div class="col-xs-12">
				<div id="newsletters-files" class="panel panel-default ajax-files-container">
					<div class="panel-heading">
						<span class="btn btn-success btn-xs fileinput-button">
							<i class="glyphicon glyphicon-plus"></i>
							<span>Select files...</span>
							<input id="newsletter_files" type="file" name="files[]" class="ajax-file" multiple >
						</span>
					</div>
					<div class="panel-body">
						<div class="progress">
							<div class="progress-bar progress-bar-success"></div>
						</div>
						 <div class="files-list"></div>
					</div>
				</div>
			</div>
		</div>

		<!-- newsletter background image -->
		<div class="row">
			<div class="col-xs-12">
				<div id="newsletters-background" class="panel panel-default ajax-files-container">
					<div class="panel-heading">
						<span class="btn btn-info btn-xs fileinput-button">
							<i class="glyphicon glyphicon-plus"></i>
							<span>Newsletter Background...</span>
							<input id="newsletter_background" type="file" name="newsletter_background" class="ajax-file" data-render="images" >
							<input type="hidden" name="extensions" value="jpg,jpeg,gif,png" >
						</span>
					</div>
					<div class="panel-body">
						<div class="progress">
							<div class="progress-bar progress-bar-success"></div>
						</div>
						 <div class="files-list"></div>
					</div>
				</div>
			</div>
		</div>

	</form>
</div>