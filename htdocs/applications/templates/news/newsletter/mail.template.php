<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Swatch Retail Net Gazette</title>
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<!--[if gte mso 9]><xml>
		 <o:OfficeDocumentSettings>
		  <o:AllowPNG/>
		  <o:PixelsPerInch>96</o:PixelsPerInch>
		 </o:OfficeDocumentSettings>
	</xml><![endif]-->
	<style type="text/css">
		
		html {
			font-size: 100%;
			font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
		}

		body {
			margin: 0;
			padding: 0;
			background-image: url({background_style});
			background-position: top center;
			background-repeat: repeat;
			width: 100%!important; 
		}

		table { 
			border: none; 
			mso-table-lspace: 0pt; 
			mso-table-rspace: 0pt; 
			border-collapse: collapse; 
			border-spacing: 0;
			background-color: #ffffff;
		}

		table td {
			font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
		}


		/**
		 * Main layout wrapper
		 */
		.wrapper {
			max-width: 750px;
			margin: auto;
			background-color: #ffffff;
		}

		/**
		 * Image responsive
		 */
		.img-responsive {
			max-width: 100%;
			height: auto;
		}

		.gazette {
			font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
			color: #000;
			font-size: 24px;
		}

		.gazette i {
			font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
			color: #000;
			font-size: 20px;
		}

		/**
		* Newsletter Title
		*/
		.newsletter-title {
			font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
			color: #000;
			font-size: 40px;
			line-height: 56px;
			padding: 20px 0 10px 0;
		}

		/**
		* Newsletter text
		*/
		.newsletter-text {
			font-size: 14px;
			font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
		}

		/**
		* Section Name
		*/
		.section {
			background-color: #E2001A;
			padding: 5px 10px 5px 10px;
			color: #ffffff;
			text-align: right;
			font-size: 28px;
			font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
		}

		/**
		* Article title
		*/
		.article-title {
			font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
			color: #A4A4A4;
			font-size: 24px;
			line-height: 34px;
			padding: 20px 0 10px 0;
		}

		/**
		* Article separator
		*/
		.article-separator {
			height: 10px;
			border-bottom: 1px dashed #a4a4a4;
		}

		/**
		* Teaser title
		*/
		.teaser-title, 
		.article-template-title {
			font-size: 16px;
			font-weight: 500;
			font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
			padding: 0;
			margin: 0;
		}

		/**
		* Teaser text
		*/
		.col-teaser-text,
		.article-template-text {
			font-size: 14px;
			font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif;
		}

		.col-teaser-text p {
			margin: 0;
		}

		.teaser-image img {
			max-height: 240px!important;
		}

		/**
		 * Teaser image
		 */
		.teaser-image {
			border: 1px solid #a6a6a6;
			display: block;
		}

		@media (max-width: 750px) {

			.gazette,
			.gazette i {
				font-size: 18px;
				text-align: right!important;
			}

			.gazzete-gap {
				width: 0px!important;
				display: none;
			}

			.wrapper {
				width: 100%;
				max-width: 100%;
				margin: auto;
			}

			.wrapper-row {
				padding: 15px 15px 15px 15px;
			}

			.footerlink, .footerlink2 {
				display: block !important;
			}

			.img-visual {
				width: 100% !important;
				max-width: 100% !important;
			}

			.col-teaser-image {
				width: 100%!important;
				max-width: 100% !important;
				display: block;
				height: 240px;
			}

			.col-teaser-image-sep {
				width: 100%!important;
				height: 15px!important;
				display: block;
			}

			.col-teaser-text {
				width: 100% !important;
				max-width: 100% !important;
				display: block;
			}

			.tbl-button {
				width: 330px!important;
			}
		}

	</style>
	<!--[if gte mso 9]><style type="text/css">

		.wrapper { 
			width: 750px; 
			max-width: 750px;
		}
		.img-visual { 
			width: 750px; 
			width: 750px !important; 
			max-width: 750px;
		}
		
	</style><![endif]-->
</head>
<body>
<center>
<table align="center" border="0" cellspacing="0" cellpadding="10" class="wrapper" bgcolor="#ffffff">
	<tr><td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td height="20">&nbsp;</td></tr>
			<tr>
				<td class="wrapper-row">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="wrapper">
						<tr>
							<td width="145" height="40">
								<a class="brand-logo" href="http://retailnet.tissot.ch/news" target="_blank">
									<img src="http://retailnet.tissot.ch/public/images/logo.jpg" width="109" height="17" />
								</a>
							</td>
							<td align="center" class="gazette">Gazette <i>No.{number}</i></td>
							<td width="145" height="40" class="gazzete-gap">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td height="20">&nbsp;</td></tr>
			<tr><td class="wrapper-row">{visual}</td></tr>
			<tr><td class="wrapper-row">{content}</td></tr>
			<tr>
				<td class="wrapper-row">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tbody>
							<tr><td height="20">&nbsp;</td></tr>
							<tr>
								<td align="center" style="font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif; font-size: 12px; color: #a4a4a4; text-align: center">
									<p style="padding-top: 10px; font-size: 20px; color: #a4a4a4">More about Swatch</p>
									<p style="line-height: 20px">
										<a class="footerlink2" style="display: inline-block; color: #a4a4a4; text-decoration: none; text-transform: uppercase" href="http://www.swatch.com/zz_en/store-locator.html" target="_blank">&nbsp;&nbsp;Store Locator&nbsp;&nbsp;</a>
										<a class="footerlink2" style="display: inline-block; color: #a4a4a4; text-decoration: none; text-transform: uppercase" href="http://www.swatch.com/zz_en/watches/finder.html" target="_blank">&nbsp;&nbsp;Swatch Finder&nbsp;&nbsp;</a>
										<a class="footerlink2" style="display: inline-block; color: #a4a4a4; text-decoration: none; text-transform: uppercase" href="http://swatch.tv" target="_blank">&nbsp;&nbsp;swatch.tv&nbsp;&nbsp;</a>
										<a class="footerlink2" style="display: inline-block; color: #a4a4a4; text-decoration: none; text-transform: uppercase" href="http://swatch.com" target="_blank">&nbsp;&nbsp;swatch.com&nbsp;&nbsp;</a>
									</p>
									<p style="padding-top: 10px; font-size: 20px; color: #a4a4a4">Follow us on</p>
									<p>
										<a style="display: inline-block;" href="http://www.facebook.com/Swatch" target="_blank">
											<img src="http://webr.emv2.com/swatch/template_responsive/facebook.png" width="29" height="29" border="0">
										</a>&nbsp;&nbsp;&nbsp;&nbsp;
										<a style="display: inline-block;" href="http://twitter.com/Swatch" target="_blank">
											<img src="http://webr.emv2.com/swatch/template_responsive/twitter.png" width="29" height="29" border="0">
										</a>&nbsp;&nbsp;&nbsp;&nbsp;
										<a style="display: inline-block;" href="http://www.youtube.com/user/swatch" target="_blank">
											<img src="http://webr.emv2.com/swatch/template_responsive/youtube.png" width="29" height="29" border="0">
										</a>&nbsp;&nbsp;&nbsp;&nbsp;
										<a style="display: inline-block;" href="http://pinterest.com/swatchwatches/" target="_blank">
											<img src="http://webr.emv2.com/swatch/template_responsive/pinterest.png" width="29" height="29" border="0">
										</a>&nbsp;&nbsp;&nbsp;&nbsp;
										<a style="display: inline-block;" href="http://instagram.com/swatch" target="_blank">
											<img src="http://webr.emv2.com/swatch/template_responsive/instagram.png" width="29" height="29" border="0">
										</a>&nbsp;&nbsp;&nbsp;&nbsp;
										<a style="display: inline-block;" href="http://plus.google.com/+swatch/posts" target="_blank">
											<img src="http://webr.emv2.com/swatch/template_responsive/googleplus.png" width="29" height="29" border="0">
										</a>
									</p>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td class="wrapper-row">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td data-editable="convert_to_text" colspan="4" align="center" style="font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif; font-size: 12px; color: #a4a4a4; text-align: center">
								<span style="font-size: 9px; color: #a4a4a4; text-transform: uppercase">©SWATCH LTD.2014 ALL RIGHTS RESERVED: SWISS WATCHES</span>
							</td>
						</tr>
						<tr><td height="40">&nbsp;</td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="wrapper-row">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td data-editable="convert_to_text" colspan="4" align="center" style="font-family: 'century gothic', CenturyGothic, STHeitiTC-Medium, Arial, helvetica, sans-serif; font-size: 12px; color: #a4a4a4; text-align: center">
								<br /><span style="font-size: 9px; color: #a4a4a4; text-transform: uppercase">DON'T FORGET THAT I AM AN INTERNAL TOOL WHICH MUST NOT BE SHARED OUTSIDE OF SWATCH OR LEFT BEHIND!</span>
							</td>
						</tr>
						<tr><td height="40">&nbsp;</td></tr>
					</table>
				</td>
			</tr>
		</table>
	</td></tr>
</table>
</center>