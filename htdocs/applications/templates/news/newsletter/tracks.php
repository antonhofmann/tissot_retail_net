<div class="container-fluid form-container">
	<input type="hidden" name="newsletter_id" id="newsletter_id" value="<?php echo $id ?>" >

	<!-- page header -->
	<div class="page-header">
		<h4 class="page-title">Newsletter #<?php echo $number ?></h4>
		<div id="newsletter-tabs" class="newsletter-tabs navbar-collapse collapse"><?php echo $tabs; ?></div>
		<button type="button" class="navbar-toggle collapsed btn btn-sm btn-default" data-toggle="collapse" data-target="#newsletter-tabs" aria-expanded="false" aria-controls="newsletter-tabs">
			<i class="fa fa-bars"></i>
		</button>
		<ul id="breadcrumps" class="process-staps"></ul>
	</div>

	<section class="tracks">
		<div class="row">
			<div class="col-xs-12">
				<div class="flat-tabs">
					<ul class="nav nav-tabs">
						<li role="presentation" class="active">
							<a href="#views" id="tab-views" role="tab" data-toggle="tab" aria-controls="views" aria-expanded="true">Views</a>
						</li>
						<li role="presentation">
							<a href="#downloads" id="tab-downloads" role="tab" data-toggle="tab" aria-controls="downloads" aria-expanded="true">File Downloads</a>
						</li>
						<li role="presentation">
							<a href="#print" id="tab-print" role="tab" data-toggle="tab" aria-controls="print" aria-expanded="true">Print</a>
						</li>
						<li role="presentation">
							<a href="#sendmail" id="tab-sendmail" role="tab" data-toggle="tab" aria-controls="sendmail" aria-expanded="true">Recipients</a>
						</li>
						<li role="presentation">
							<a href="#views-client" id="tab-views-client" role="tab" data-toggle="tab" aria-controls="views-client" aria-expanded="true">Views from E-mail</a>
						</li>
					</ul>
					<div class="tab-content">

						<div role="tabpanel" class="tab-pane fade active in" id="views" aria-labelledby="tab-views">
							<table class="table table-striped table-hover table-responsive">
								<thead>
									<tr>
										<td width="100px">Date</td>
										<td width="40px">&nbsp;</td>
										<td>User</td>
									</tr>
								</thead>
								<tbody>
								<?php 
									if ($tracks['views']) {
										foreach ($tracks['views'] as $i => $track) {
											echo "<tr>";
											echo "<td>{$track[date]}</td>";
											echo "<td>{$track[time]}</td>";
											echo "<td>{$track[name]}<br /><small>{$track[company]}</small></td>";
											echo "</tr>";
										}
									}
								?>
								</tbody>
							</table>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="downloads" aria-labelledby="tab-downloads">
							<table class="table table-striped table-hover table-responsive">
								<thead>
									<tr>
										<td width="100px">Date</td>
										<td width="40px">&nbsp;</td>
										<td>User</td>
										<td width="50%">File</td>
									</tr>
								</thead>
								<tbody>
								<?php 
									if ($tracks['downloads']) {
										foreach ($tracks['downloads'] as $track) {
											echo "<tr>";
											echo "<td>{$track[date]}</td>";
											echo "<td>{$track[time]}</td>";
											echo "<td>{$track[name]}<br /><small>{$track[company]}</small></td>";
											echo "<td><a href='{$track[file]}' class='download' target='_blank' ><small>".basename($track['file'])."</small></a></td>";
											echo "</tr>";
										}
									}
								?>
								</tbody>
							</table>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="print" aria-labelledby="tab-print">
							<table class="table table-striped table-hover table-responsive">
								<thead>
									<tr>
										<td width="100px">Date</td>
										<td width="40px">&nbsp;</td>
										<td>User</td>
									</tr>
								</thead>
								<tbody>
								<?php 
									if ($tracks['prints']) {
										foreach ($tracks['prints'] as $track) {
											echo "<tr>";
											echo "<td>{$track[date]}</td>";
											echo "<td>{$track[time]}</td>";
											echo "<td>
												{$track[name]}<br />
												<small>{$track[company]}</small><br />
												<small class=text-muted >{$track[content]}</small>
											</td>";
											echo "</tr>";
										}
									}
								?>
								</tbody>
							</table>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="sendmail" aria-labelledby="tab-sendmail">
							<table class="table table-striped table-hover table-responsive">
								<thead>
									<tr>
										<td width="100px">Date</td>
										<td width="40px">&nbsp;</td>
										<td width="40%">Recipient</td>
										<td>Sender</td>
									</tr>
								</thead>
								<tbody>
								<?php 
									if ($tracks['sendmail']) {
										foreach ($tracks['sendmail'] as $track) {
											
											echo "<tr>";
											
											echo "<td>{$track[date]}</td>";
											echo "<td>{$track[time]}</td>";
											
											echo "<td>";
											echo $track['recipient'];
											echo "<br /><small>{$track[recipient_mail]}</small>";
											if ($track['recipient_company']) echo "<br /><small>{$track[recipient_company]}</small>";
											echo "</td>";

											echo "<td>";
											echo $track['sender'];
											echo "<br /><small>{$track[sender_mail]}</small>";
											if ($track['sender_company']) echo "<br /><small>{$track[sender_company]}</small>";
											echo "</td>";
											
											echo "</tr>";
										}
									}
								?>
								</tbody>
							</table>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="views-client" aria-labelledby="tab-views-client">
							<table class="table table-striped table-hover table-responsive">
								<thead>
									<tr>
										<td width="100px">Date</td>
										<td width="40px">&nbsp;</td>
										<td>User</td>
									</tr>
								</thead>
								<tbody>
								<?php 
									if ($tracks['views-client']) {
										foreach ($tracks['views-client'] as $track) {
											echo "<tr>";
											echo "<td>{$track[date]}</td>";
											echo "<td>{$track[time]}</td>";
											echo "<td>";
											echo "{$track[name]}<br /><small>{$track[email]}</small>";
											if ($track['company']) echo "<br /><small>{$track[company]}</small>";
											echo "</td>";
											echo "</tr>";
										}
									}
								?>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>

</div>
