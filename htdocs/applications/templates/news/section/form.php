<div class="page-header">
	<h3 class="page-title"><?php echo $this->title ?></h3>
</div>
<div class="form-container form-container-xs">
	<form method="post" class="form-horizontal" id="sectionForm" action="/applications/modules/news/section/submit.php"
		data-fv-framework="bootstrap"
	    data-fv-icon-valid="glyphicon glyphicon-ok"
	    data-fv-icon-invalid="glyphicon glyphicon-remove"
	    data-fv-icon-validating="glyphicon glyphicon-refresh" >
		<input type="hidden" name="news_section_id" id="news_section_id" value="<?php echo $data['news_section_id'] ?>">
		<input type="hidden" name="application" id="application" value="<?php echo $data['application'] ?>">
		<div class="form-group">
			<label for="news_section_name" class="col-sm-4 control-label">Section Name</label>
			<div class="col-sm-8 has-feedback">
				<input type="text" class="form-control" name="news_section_name"
					placeholder="Section Name"
					data-fv-row=".col-xs-8"
                	data-fv-notempty="true"
                	data-fv-notempty-message="The section name is required"
                	value="<?php echo $data['news_section_name'] ?>" />
			</div>
		</div>
		<div class="form-group">
			<label for="news_section_confirm_user_id" class="col-sm-4 control-label">Standard Approver</label>
			<div class="col-sm-8">
				<select class="form-control" name="news_section_confirm_user_id" id="news_section_confirm_user_id" >
					<option value="">Select</option>
					<?php
						if ($users) {
							foreach ($users as $user) {
								$selected = $data['news_section_confirm_user_id']==$user['id'] ? 'selected' : null;
								echo "<option value='{$user[id]}' $selected >{$user[name]}</option>";
							}
						}
					?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-4 col-sm-8">
				<a href="/gazette/sections" class="btn btn-default"><i class="fa fa-chevron-left"></i> Back</a>
				<?php if ($buttons['save']) : ?>
					<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
				<?php endif; ?>				
				<?php if ($buttons['delete']) : ?>
					<a id="btnDelete" href="<?php echo $buttons['delete'] ?>" class="btn btn-danger"><i class="fa fa-times"></i> Delete</a>
				<?php endif; ?>

			</div>
		</div>
	</form>
</div>