<link href="/public/scripts/jquery-ui/jquery-ui.min.css" rel="stylesheet"></link>
<script src="/public/scripts/jquery-ui/jquery-ui.min.js"></script>
<div class="page-header">
	<h3 class="page-title"><?php echo $this->title ?></h3>
</div>
<div class="sections-list">
	<?php
		if ($sections) {

			echo "<table class='table table-hover table-striped' id='sections' >";
			echo "<thead>";
			echo "<th width='20px'><a href='/gazette/sections/add' class='btn btn-xs btn-primary' ><i class='fa fa-plus'></i></a></th>";
			echo "<th>Section</th>";
			echo "<th width='40%'>Standard Approver</th>";
			echo "</thead>";
			echo "<tbody>";

			foreach ($sections as $section) {
				$id = $section['id'];
				echo "<tr data-id='$id'>";
				echo "<td class='section-order' ><i class='fa fa-bars'></i></td>";
				echo "<td><a href='/gazette/sections/data/$id'>{$section[name]}</a></td>";
				echo "<td>{$section[confirm_person]}</td>";
				echo "</tr>";
			}

			echo "</tbody>";
			echo "</table>";
		}
	?>
</div>