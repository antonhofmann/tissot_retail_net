<form method="POST" action="/appcliactions/modules/news/article/author.submit.php">
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<div class="form-group">
				<input type="text" name="auchor_firstname" id="auchor_firstname" class="form-control" placeholder="Author first name" required />
			</div>
		</div>
		<div class="col-xs-12 col-sm-6">
			<div class="form-group">
				<input type="text" name="auchor_name" id="auchor_name" class="form-control" placeholder="Author name" required />
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="form-group">
				<input type="email" name="auchor_email" id="auchor_email" class="form-control" placeholder="Author email" required />
			</div>
		</div>
	</div>
</form>