<link href="/public/scripts/jquery-ui/jquery-ui.min.css" rel="stylesheet"></link> 
<link href="/public/scripts/jquery-ui/jquery-ui.theme.min.css" rel="stylesheet"></link>
<script src="/public/scripts/jquery-ui/jquery-ui.min.js"></script>
<link href="/public/css/datepicker.css" rel="stylesheet"></link>
<script type="text/javascript" src="/public/scripts/tinymce4/js/tinymce/tinymce.min.js"></script>
<link rel="stylesheet" href="/public/scripts/fileuploader/css/style.css">
<link rel="stylesheet" href="/public/scripts/fileuploader/css/jquery.fileupload.css">
<script src="/public/scripts/fileuploader/js/jquery.ui.widget.js"></script>
<script src="/public/scripts/fileuploader/js/jquery.iframe-transport.js"></script>
<script src="/public/scripts/fileuploader/js/jquery.fileupload.js"></script>
<!--[if (gte IE 8)&(lt IE 10)]><script src="/public/scripts/fileuploader/js/cors/jquery.xdr-transport.js"></script><![endif]-->

<div class="page-header"><h4 class="page-title"><?php echo Request::instance()->title ?></h4></div>
<div class="container-fluid form-container">
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-8">
			<ul id="breadcrumps" class="process-staps"></ul>
		</div>
		<div class="col-xs-12 col-sm-4 col-md-4">
			<div class="btn-group btn-group-justified" id="actions"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-8">

				<!-- field: news article id -->
				<input type="hidden" name="news_article_id" id="news_article_id" value="<?php echo $data['id'] ?>" >
				
				<!-- article -->
				<fieldset>
					<legend>Article</legend>
					<div class="form-group">
						<!-- field: news article title -->
						<input type="text" class="form-control article-field news_article_title" name="news_article_title" id="news_article_title" placeholder="Article Title">
					</div>
					<div class="row">
						<div class="form-group col-xs-12 col-sm-6">
							<div class="form-group">
								<!-- field: news article category -->
								<select name="news_article_category_id" id="news_article_category_id" data-size='12' class="form-control article-field selectpicker">
									<option value=''>Select Category</option>
									<?php 
										if ($sections) {
											foreach ($sections as $key => $section) {
												echo "<optgroup label='{$section[name]}' >";
												foreach ($section['categories'] as $value => $category) echo "<option value='$value'>{$category[name]}</option>"; 
												echo "</optgroup>";
											}
										} 
									?>
								</select>
							</div>
						</div>
						<div class="form-group col-xs-12 col-sm-6">
							<div class="form-group">
								<!-- field: news article category -->
								<select name="news_article_author_id" id="news_article_author_id" data-size='12' class="form-control article-field selectpicker">
									<option value="">Select Author</option>
									<option value="new">Add New Author</option>
									<?php 
										if ($authors) {
											foreach ($authors as $key => $group) {
												echo "<optgroup class='group-$key' label='{$group[name]}' >";
												foreach ($group['authors'] as $value => $author) {
													echo "<option value='$value'>{$author[name]}</option>"; 
												}
												echo "</optgroup>";
											}
										}
									?>
								</select>
							</div>
						</div>
					</div>
				</fieldset>

				<!-- article teasers -->
				<fieldset>
					<legend>Teaser</legend>
					<div id="teaser-container"></div>
				</fieldset>
				
				<!-- article templates -->
				<fieldset>
					<legend>Content</legend>
					<!-- templates container -->
					<div id="templates-container"></div>
				</fieldset>

				<!-- template icons -->
				<fieldset id="template-icons">
					<legend>Templates</legend>
					<div class="template-icons">
						<div class="row">
							<div class="tpl-icon tmp-2">
								<div class="inner">
									<span class="tmp-title">Title</span>
									<span class="tmp-content">Content</span>
								</div>
								<span class="fa fa-plus-circle tmp-add" data-id="2"></span>
							</div>
							<div class="tpl-icon tmp-3">
								<div class="inner">
									<span class="tmp-title">Title</span>
									<span class="tmp-image">&nbsp;</span>
								</div>
								<span class="fa fa-plus-circle tmp-add"  data-id="3"></span>
							</div>
							<div class="tpl-icon tmp-4">
								<div class="inner">
									<span class="tmp-image">&nbsp;</span>
									<span class="tmp-title">Title</span>
									<span class="tmp-content">Content</span>
								</div>
								<span class="fa fa-plus-circle tmp-add"  data-id="4"></span>
							</div>
							<div class="tpl-icon tmp-5">
								<div class="inner">
									<table>
										<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
									</table>
								</div>
								<span class="fa fa-plus-circle tmp-add"  data-id="5"></span>
							</div>
						</div>
					</div>
				</fieldset>

		</div>
		<div class="col-xs-12 col-sm-4 col-md-4 sidebar">
			<div class="panel-group" id="accordions" role="tablist" aria-multiselectable="true">
				
				<!-- publishing -->
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headerPublishing">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" href="#bodyPublishing" data-parent="#accordions" aria-expanded="true" aria-controls="bodyPublishing">Publishing</a>
						</h4>
					</div>
					<div id="bodyPublishing" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headerPublishing">
						<div class="panel-body">
							<table class="table">
								<tr id="row_date_created">
									<td width="50%" valign="top">Owner</td>
									<td valign="top">
										<input type="hidden" name="owner" id="owner" data-caption="#caption-owner" class="article-field" >
										<span class="field-static" id="caption-owner"></span>
									</td>
								</tr>
								<tr id="row_news_article_publish_state_id">
									<td valign="top">Status</td>
									<td valign="top">
										<select name="news_article_publish_state_id" id="news_article_publish_state_id" data-caption="#caption-state" class="article-field hidden">
											<?php  if ($states) foreach ($states as $value => $name) echo "<option value='$value' >$name</option>"; ?>
										</select>
										<span class="field-static" id="caption-state"></span>
									</td>
								</tr>
								<tr id="row_date_created">
									<td width="60%" valign="top">Created on</td>
									<td valign="top">
										<input type="hidden" name="date_created" id="date_created" data-caption="#caption-date-created" class="article-field" >
										<span class="field-static" id="caption-date-created"></span>
									</td>
								</tr>
								<tr id="row_news_article_publish_date">
									<td valign="top">Published on</td>
									<td valign="top">
										<input type="text" name="news_article_publish_date" id="news_article_publish_date" class="article-field" >
									</td>
								</tr>
								<tr id="row_news_article_desired_publish_date">
									<td valign="top">Desired publishing date</td>
									<td valign="top">
										<input type="text" name="news_article_desired_publish_date" id="news_article_desired_publish_date" class="article-field datepicker" >
									</td>
								</tr>
								<tr id="row_news_article_expiry_date">
									<td valign="top">Put to archive on</td>
									<td valign="top">
										<input type="text" name="news_article_expiry_date" id="news_article_expiry_date" class="article-field datepicker" >
									</td>
								</tr>
								<tr id="row_news_article_featured">
									<td valign="top">Featured</td>
									<td valign="top">
										<label class="switch switch-green">
											<input type="checkbox" name="news_article_featured" id="news_article_featured" class="article-field switch-input" >
											<span class="switch-label" data-on="Yes" data-off="No"></span>
											<span class="switch-handle"></span>
										</label>
									</td>
								</tr>
								<tr id="row_news_article_active">
									<td valign="top">In use</td>
									<td valign="top">
										<label class="switch switch-green">
											<!-- field: news article active -->
											<input type="checkbox" name="news_article_active" id="news_article_active" class="article-field switch-input" >
											<span class="switch-label" data-on="Yes" data-off="No"></span>
											<span class="switch-handle"></span>
										</label>
									</td>
								</tr>
								<tr id="row_news_article_multiple">
									<td valign="top">Multiple use</td>
									<td valign="top">
										<label class="switch switch-green">
											<!-- field: news article active -->
											<input type="checkbox" name="news_article_multiple" id="news_article_multiple" class="article-field switch-input" >
											<span class="switch-label" data-on="Yes" data-off="No"></span>
											<span class="switch-handle"></span>
										</label>
									</td>
								</tr>
								<tr id="row_news_article_new">
									<td valign="top">Is new</td>
									<td valign="top">
										<label class="switch switch-green">
											<!-- field: news article active -->
											<input type="checkbox" name="news_article_new" id="news_article_new" class="article-field switch-input" >
											<span class="switch-label" data-on="Yes" data-off="No"></span>
											<span class="switch-handle"></span>
										</label>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>

				<!-- confirmation -->
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headerConfirmation">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" href="#bodyConfirmation" aria-expanded="true" aria-controls="bodyConfirmation">Approval</a>
						</h4>
					</div>
					<div id="bodyConfirmation" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headerConfirmation">
						<div class="panel-body">
							<table class="table">
								<tr>
									<td width="60%" valign="top">Approval date</td>
									<td valign="top">
										<input type="hidden" name="news_article_confirmed_date" id="news_article_confirmed_date" data-caption="#caption-date-confirmed" class="article-field" >
										<span class="field-static" id="caption-date-confirmed"></span>
									</td>
								</tr>
								<tr>
									<td valign="top">To be approved by</td>
									<td valign="top">
										<select name="news_article_confirmed_user_id" id="news_article_confirmed_user_id" class="article-field" >
											<option value="">Select</option>
											<?php if ($confirmators) foreach ($confirmators as $value => $name) echo "<option value='$value' >$name</option>"; ?>
										</select>
									</td>
								</tr>							
							</table>
						</div>
					</div>
				</div>

				<!-- role access controll -->
				<?php if ($roles) : ?>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headerAccessController">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" href="#bodyAccessControll" aria-expanded="false" aria-controls="bodyAccessControll">
								Roles - Visibility restricted to
							</a>
							<span class="badge pull-right total-entity"></span>
						</h4>
					</div>
					<div id="bodyAccessControll" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headerAccessController">
						<div class="panel-body" data-section="article-access">
							<ul class="nav nav-list checkbox-list" data-group-name="article-access">
								<li class="checkbox chackall">
									<label class="group-container">
										<input type='checkbox' class='check-all'/>
										<span>Select All</span>
									</label>
								</li>
								<?php 
									foreach ($roles as $row) {
										echo "<li class='checkbox'>";
										echo "<label>";
										echo "<input type='checkbox' name='news_article_access_controlls[{$row[id]}]' data-id='{$row[id]}' class='article-checkbox' {$row[checked]} />";
										echo "<span>{$row[name]}</span>";
										echo "</label>";
										echo "</li>"; 
									}
								?>
							</ul>
						</div>
					</div>
				</div>
				<?php endif; ?>

				<!-- company access controll -->
				<?php if ($companies) : ?>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headerCompanyRestrictions">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" href="#bodyCompanyRestrictions" aria-expanded="false" aria-controls="bodyCompanyRestrictions">
								Companies - Visibility restricted to
							</a>
							<span class="badge pull-right total-entity"></span>
						</h4>
					</div>
					<div id="bodyCompanyRestrictions" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headerCompanyRestrictions">
						<div class="panel-body" data-section="company-access">
							<ul class="nav nav-list checkbox-list">
								<li class="checkbox chackall">
									<label class="group-container">
										<input type='checkbox' class='check-all'/>
										<span>Select All</span>
									</label>
									<br />
									<label class="group-container">
										<input type='checkbox' class='check-group' data-group="group-subsidiary" />
										<span>Select Subsidiaries <i class='total-group'></i></span>
									</label>
									<br />
									<label class="group-container">
										<input type='checkbox' class='check-group' data-group="group-agent" />
										<span>Select Agents <i class='total-group'></i></span>
									</label>
									<br />
									<label class="group-container">
										<input type='checkbox' class='check-group' data-group="group-hq" />
										<span>Select HQ Companies <i class='total-group'></i></span>
									</label>
								</li>
							</ul>
							<?php 

								foreach ($companies as $rid => $region) {

									echo "<ul class='nav nav-list checkbox-list checkbox-regions region-$rid' data-region='$rid' >";
									echo "
										<li class='checkbox chackall region-name group-container'>
											<label><input type='checkbox' class='check-group' data-group='group-region-$rid' /></label>
											<span>{$region[region]}</span>
											<i class='total-group'></i>
											<i class='fa fa-caret-down pull-right region-toggler'></i>
										<li>
									";

									foreach ($region['companies'] as $company => $row) {
										echo "<li class='checkbox'><label>";
										echo "<input type='checkbox' name='news_article_access_controlls[$company]' data-id='$company' class='article-checkbox {$row[classes]}' {$row[checked]} />";
										echo "<span>{$row[name]}</span>";
										echo "</label></li>"; 
									}

									echo "</ul>";
								}
							?>
						</div>
					</div>
				</div>
				<?php endif; ?>

				<!-- contact informations -->
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headerContactInformation">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" href="#bodyContactInformation" aria-expanded="false" aria-controls="bodyContactInformation">Contact information</a>
						</h4>
					</div>
					<div id="bodyContactInformation" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headerContactInformation">
						<div class="panel-body">
							<div class="form-group">
								<textarea name="news_article_contact_information" id="news_article_contact_information" rows="8" class="form-control article-field" ></textarea>
							</div>
						</div>
					</div>
				</div>

				<!-- article stickers -->
				<?php if ($stickers) : ?>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headerStickers">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" href="#bodyStickers" aria-expanded="false" aria-controls="bodyStickers">Stickers</a>
						</h4>
					</div>
					<div id="bodyStickers" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headerStickers">
						<div class="panel-body" id="stickers">
							<div class="stickers height-400">
							<?php 
								foreach ($stickers as $row) {
									echo "<div class='item'>";
									echo "<span data-id='{$row[id]}' class='sticker sticker-{$row[id]}' style='background-image: url({$row[image]})'>";
									echo "<span class='selected'><i class='fa fa-check-circle-o'></i></span>";
									if ($row['title']) echo "<p>{$row[title]}</p>";
									echo "</span>";
									echo "</div>";
								}
							?>
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>

				<!-- article tracks -->
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headerTracks">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" href="#bodyTracks" aria-expanded="false" aria-controls="bodyTracks">Tracks</a>
						</h4>
					</div>
					<div id="bodyTracks" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headerTracks">
						<div class="panel-body" id="articleTracks"></div>
					</div>
				</div>

				<?php if ($newsletters) : ?>
				<!-- newslterrs -->
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headerNewslleters">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" href="#bodyNewsletters" aria-expanded="false" aria-controls="bodyNewsletters">Assign to Newsletters</a>
						</h4>
					</div>
					<div id="bodyNewsletters" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headerNewslleters">
						<div class="panel-body">
							<ul class="nav nav-list checkbox-list">
							<?php 
								foreach ($newsletters as $row) {
									//echo "<li><input type='checkbox' name='news_article_company_restrict' data-id='{$row[id]}' class='company-access' {$row[checked]} /> {$row[name]}</li>"; 
									echo "<li class='checkbox'>";
									echo "<label>";
									echo "<input type='checkbox' name='news_article_access_controlls[{$row[id]}]' data-id='{$row[id]}' class='newsletter-access' {$row[checked]} />";
									echo "<span>{$row[name]}</span>";
									echo "</label>";
									echo "</li>"; 
								}
							?>
							</ul>
						</div>
					</div>
				</div>
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>

<!-- modal delete dialog 
<div id="delete-dialog"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" >Confirm</div>
	</div>
	<div class="ado-modal-body"><?php echo $translate->dialog_delete_record ?></div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="#" >
				<span class="icon save"></span>
				<span class="label" ><?php echo $translate->send ?></span>
			</a>
		</div>
	</div>
</div>
-->