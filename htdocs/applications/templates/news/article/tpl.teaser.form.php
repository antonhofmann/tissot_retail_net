<form class="news-article-template-container news-article article-teaser teaser tpl-{id}" data-article="{article}" data-id="{id}" id="article-template-{id}">
	<div class="col-left box-image">
		<div class="image-cropper-container has-popover" data-toggle="popover" data-placement="top" data-content="Min image size 320x240px.">
			<!-- field: news article teaser image -->
			<input type="hidden" name="image" id="image-{id}" class="template-field field-crop" value="{image}" data-filename="teaser" >
			<div class="image-cropper" id="croper-{id}" ></div>
		</div>
		<div id="teaser-media" class="form-group">
			<!-- field: news article teaser media -->
			<textarea name="media" id="media-{id}" placeholder="Media Link" class="form-control template-field teaser-media has-popover" rows="2"
				data-toggle="popover" data-content="Swatch TV ID or valid URL address (http://www.yourdomain.com/optionl)"  data-placement="right">{media}</textarea>
		</div>
	</div>
	<div class="col-right box-content">
		<div class="form-group">
			<!-- field: news article teaser title -->
			<input type="text" name="title" id="title-{id}" placeholder="Teaser Title" class="form-control template-field teaser-title news_article_teaser_title" value="{title}" />
		</div>
		<div class="form-group">
			<!-- field: news article teaser text -->
			<textarea name="text" id="text-{id}" placeholder="Teaser Text" class="form-control template-field teaser-text news_article_teaser_text ">{text}</textarea>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="template-files">
		<span class="btn btn-success btn-xs fileinput-button">
			<i class="glyphicon glyphicon-plus"></i>
			<span>Select files...</span>
			<input id="teaser-files-{id}" type="file" name="files[]" class="tpl-files" multiple >
		</span>
		<div class="progress">
			<div class="progress-bar progress-bar-success"></div>
		</div>
		<div class="row">
			<div class="col-xs-12">
		 		<div class="files">{files}</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</form>