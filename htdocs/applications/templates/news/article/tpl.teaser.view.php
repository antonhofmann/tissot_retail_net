<div class="row"> 
	<div class="col-xs-12 col-sm-4 col-md-4">
		{image}
		{media}
	</div>
	<div class="col-xs-12 col-sm-8 col-md-8 {class_no_image}">
		<h4 class="not-empty">{title}</h4>
		<p class="not-empty">{text}</p>
		<a href='#' class='blocklink read-more not-empty' data-caption='Show Less' data-target='#article-body-{article}' >{readmore}</a>
		<p class="date_and_author">{date}{author}</p>
		<p class="informations not-empty">{informations}</p>
		<p class="files not-empty">{files}</p>
	</div>
</div>