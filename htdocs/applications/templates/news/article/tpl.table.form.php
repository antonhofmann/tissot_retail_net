<section class="news-article-template-container tpl-{id}" data-article="{article}" data-id="{id}" id="article-template-{id}">
	<a href="#" class="template-control tpl-order"><i class="fa fa-bars"></i></a>
	<a href="#" class="template-control tpl-remover" ><i class="fa fa-times-circle"></i></a>
	<div class="template-content">
		<div id="table-handsontable-{id}" class="table-handsontable"></div>
	</div>
	<div class="template-files">
		<span class="btn btn-primary btn-xs fileinput-button">
			<i class="glyphicon glyphicon-plus"></i>
			<span>Select File(s)...</span>
			<input id="template-files-{id}" type="file" name="files[]" class="tpl-files" multiple >
		</span>
		<div class="progress">
			<div class="progress-bar progress-bar-success"></div>
		</div>
		<div class="row">
		 	<div class="col-xs-12">
		 		<div class="files">{files}</div>
		 	</div>
		</div>
	</div>
</section>