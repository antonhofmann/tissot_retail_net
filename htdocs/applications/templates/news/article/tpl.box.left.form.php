<section class="news-article-template-container tpl-{id}" data-article="{article}" data-id="{id}" id="article-template-{id}">
	<a href="#" class="template-control tpl-order"><i class="fa fa-bars"></i></a>
	<a href="#" class="template-control tpl-remover" ><i class="fa fa-times-circle"></i></a>
	<div class="col-left box-image image-cropper-container">
		<!-- field: template image -->
		<input type="hidden" name="image" id="image-{id}" class="template-field field-crop" value="{image}" >
		<div class="image-cropper" id="croper-{id}" ></div>
	</div>
	<div class="col-right box-content">
		<div class="form-group">
			<!-- field: template title -->
			<input type="text" name="title" id="title-{id}" placeholder="Title" class="form-control template-field" value="{title}">
		</div>
		<div class="form-group">
			<!-- field: template text -->
			<textarea name="text" id="text-{id}" placeholder="Text" class="form-control template-field" rows="10">{text}</textarea>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="template-files">
		<span class="btn btn-primary btn-xs fileinput-button">
			<i class="glyphicon glyphicon-plus"></i>
			<span>Select files...</span>
			<!-- field: template files -->
			<input id="tpl-files-{id}" type="file" name="files[]" class="tpl-files" multiple >
		</span>
		<div class="progress">
			<div class="progress-bar progress-bar-success"></div>
		</div>
		<div class="row">
		 	<div class="col-xs-12">
		 		<div class="files">{files}</div>
		 	</div>
		 </div>
	</div>
	<div class="clearfix"></div>
</section>