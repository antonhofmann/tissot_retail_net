<button class="btn btn-primary filter-toggle">
	<i class="fa fa-caret-down"></i> Filters
</button>
<div class="page-header">
	<h3 class="page-title"><?php echo $this->title ?></h3>
</div>
<ul class="list-filters">
	<?php
		if ($buttons['add']) {
			echo "<li><a href=\"/gazette/articles/article\" class=\"btn btn-primary btn-sm add-article\" ><i class=\"fa fa-plus\"></i> New Article</a></li>";
		}
	?>
	<!-- quick search -->
	<li>
		<div class="btn-group btn-group-sm filter-group group-search" role="group">
			<button type="button" class="btn btn-default disabled"><?php echo Translate::instance()->section ?></button>
			<input type="text" name="search" id="quicksearch" value="<?php echo $filterSearch; ?>" />
		</div>
	</li>
	<!-- filter group sections -->
	<li>
		<div id="group-sections" class="btn-group btn-group-sm filter-group" role="group" data-filter-group="section">
			<button type="button" class="btn btn-default disabled"><?php echo Translate::instance()->section ?></button>
			<div class="bnt-group-responsive-caption">
				<label></label>
				<span class="caret-holder"><span class="caret"></span></span>
			</div>
			<div class="btn-group btn-group-sm bnt-group-responsive btn-filters" role="group">
				<?php 
					if ($sections) {
						foreach ($sections as $section) {
							echo "<button 
								type=\"button\" 
								class=\"btn btn-default filter {$section[active]}\" 
								data-id=\"{$section[id]}\" 
								data-filter=\"{$section[filter]}\">
								{$section[name]}
							</button>";
						}
					}
				?>
			</div>
		</div>
	</li>
	<!-- filter group states -->	
	<?php if ($states) :  ?>
	<li>
		<div id="group-states" class="btn-group btn-group-sm filter-group" role="group" data-filter-group="state">
			<button type="button" class="btn btn-default disabled"><?php echo Translate::instance()->state ?></button>
			<div class="bnt-group-responsive-caption">
				<label></label>
				<span class="caret-holder"><span class="caret"></span></span>
			</div>
			<div class="btn-group btn-group-sm bnt-group-responsive btn-filters" role="group">
				<?php 
					foreach ($states as $state) {
						echo "<button 
							type=\"button\" 
							class=\"btn btn-default filter {$state[active]}\" 
							data-id=\"{$state[id]}\" 
							data-filter=\"{$state[filter]}\">
							{$state[name]}
						</button>";
					}
				?>
			</div>
		</div>
	</li>
<?php endif; ?>
	<!-- orders -->
	<li>
		<div id="group-sort" class="btn-group btn-group-sm filter-group" role="group" data-filter-group="sort">
			<button type="button" class="btn btn-default disabled"><?php echo Translate::instance()->sort ?></button>
			<div class="bnt-group-responsive-caption">
				<label></label>
				<span class="caret-holder"><span class="caret"></span></span>
			</div>
			<div class="btn-group btn-group-sm bnt-group-responsive" role="group">
				<?php
					if ($sorts) {
						foreach ($sorts as $button) {
							echo "<button 
								type=\"button\" 
								class=\"btn btn-default sort {$button[active]}\" 
								data-filter=\"{$button[filter]}\" 
								data-ascending=\"{$button[ascending]}\" >
								{$button[title]} {$button[icon]}
							</button>";
						}
					}
				?>
			</div>
		</div>
	</li>
	<li>
		<div id="group-view" class="btn-group btn-group-sm filter-group" role="group" data-filter-group="view">
			<button type="button" class="btn btn-default dropdown-toggle disabled"><?php echo Translate::instance()->view ?></button>
			<?php
				if ($views) {
					foreach ($views as $button) {
						echo "<button 
							type=\"button\" 
							class=\"btn btn-default view {$button[active]}\" 
							data-filter=\"{$button[filter]}\">
							{$button[title]}
						</button>";
					}
				}
			?>
		</div>
	</li>
	<div class="clearfix"></div>
</ul>
<div class="articles-container">
	<input type="hidden" name="archived" id="archived" value="<?php echo $archived; ?>">
	<div id="articles" class="row"></div>
	<div class="article-pagination">
		<a href="#" class="next" data-page="<?php echo $page ?>">Load</a>
	</div>
</div>

<script id="article-template" type="text/template">
	<div id="article-{{id}}" class=" article article-box {{filter}}" data-id="{{id}}" >
		<div class="panel {{type}}">
			<div class="panel-heading">
				<a href="{{link}}" class="panel-title title">{{title}}</a>
				{{#button}}
					<a href="#" data-url="{{url}}" class="btn btn-{{type}} btn-state" title="{{title}}" data-state="{{state}}" data-toggle="tooltip" data-placement="left" >
						<i class="fa fa-{{icon}}"></i>
					</a>
				{{/button}}
			</div>
			{{#teaser}}
			<div class="panel-body">
				{{#image}}
					<div class="teaser-image">
						<img src="{{image}}" class="img-responsive">
					</div>
				{{/image}}
				<div class="teaser-text">
					<div class="ellipsis"><p>{{&title}} {{&text}}</p></div>
				</div>
			</div>
			{{/teaser}}
			<div class="panel-footer">
				<div class="article-info">
				{{#info}}
					<span>
						<label>{{label}}</label>
						<span class="{{class}}" data-sort="{{sort}}">{{title}}</span>
					</span>
				{{/info}}
				</div>
			</div>
		</div>
	</div>
</script>