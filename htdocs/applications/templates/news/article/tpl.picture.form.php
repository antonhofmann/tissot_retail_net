<section class="news-article-template-container tpl-{id}" data-article="{article}" data-id="{id}" id="article-template-{id}">
	<a href="#" class="template-control tpl-order"><i class="fa fa-bars"></i></a>
	<a href="#" class="template-control tpl-remover" ><i class="fa fa-times-circle"></i></a>
	<div class="template-content">
		<div class="form-group">
			<textarea class="form-control template-field" id="text-{id}" name="text" placeholder="Description for picture(s)" rows="2">{text}</textarea>
		</div>
	</div>
	<div class="template-files">
		<span class="btn btn-primary btn-xs fileinput-button">
			<i class="glyphicon glyphicon-plus"></i>
			<span>Select Picture(s)...</span>
			<input id="template-files-{id}" type="file" name="files[]" class="tpl-files" data-render="images" multiple >
			<input id="files-extensions-{id}" type="hidden" name="extensions"  value="jpg|jpeg|png|gif" >
		</span>
		<div class="progress">
			<div class="progress-bar progress-bar-success"></div>
		</div>
		<div class="files row">{files}</div>
	</div>
</section>