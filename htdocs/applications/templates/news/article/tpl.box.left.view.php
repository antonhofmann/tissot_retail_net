<div class="row">
	<div class="col-xs-12 col-sm-4 col-md-4">
		<img class="img-responsive" src="{image}" >
	</div>
	<div class="col-xs-12 col-sm-8 col-md-8">
		<h4 class="not-empty">{title}</h4>
		<p class="not-empty">{text}</p>
		<p class="files not-empty">{files}</p>
	</div>
</div>