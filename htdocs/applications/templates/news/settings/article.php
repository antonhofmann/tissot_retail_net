<div class="page-header">
	<h4 class="page-title"><?php echo Request::instance()->title ?></h4>
</div>
<form id="formSetting" class="form-container-xs form-horizontal"
	data-fv-framework="bootstrap"
    data-fv-icon-valid="glyphicon glyphicon-ok"
    data-fv-icon-invalid="glyphicon glyphicon-remove"
    data-fv-icon-validating="glyphicon glyphicon-refresh" >
	<input type="hidden" name="news_article_setting_id" id="news_article_setting_id" value="<?php echo $data['news_article_setting_id'] ?>" >	
	<div class="form-group">
		<label for="news_article_setting_property_id" class="col-sm-3 control-label">Property</label>
		<div class="col-sm-9 has-feedback">
			<select class="form-control" 
				name="news_article_setting_property_id" 
				id="news_article_setting_property_id"
				data-fv-row=".col-xs-9"
            	data-fv-notempty="true"
            	data-fv-notempty-message="The setting property is required" >
				<option value="">Settings</option>
				<?php 
					if ($settings) {
						foreach ($settings as $row) {
							echo "<option value='{$row[value]}' {$row[selected]}>{$row[name]}</option>"; 
						}
					}
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="news_article_setting_field" class="col-sm-3 control-label">Name</label>
		<div class="col-sm-9 has-feedback">
			<input type="text" class="form-control" 
				name="news_article_setting_field" 
				id="news_article_setting_field" 
				placeholder="Name" 
				data-fv-row=".col-xs-9"
            	data-fv-notempty="true"
            	data-fv-notempty-message="The Fiel name is required"
				value="<?php echo $data['news_article_setting_field'] ?>" />
		</div>
	</div>
	<div class="form-group">
		<label for="news_article_setting_value" class="col-sm-3 control-label">Value</label>
		<div class="col-sm-9 has-feedback">
			<input type="text" class="form-control" 
				name="news_article_setting_value" 
				id="news_article_setting_value" 
				placeholder="Value" 
				data-fv-row=".col-xs-9"
            	data-fv-notempty="true"
            	data-fv-notempty-message="The value is required"
				value="<?php echo $data['news_article_setting_value'] ?>" />
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<a href="/gazette/settings" class="btn btn-default"><i class="fa fa-chevron-left"></i> Back</a>
			<?php if ($buttons['save']) : ?>
				<button type="submit" class="btn btn-primary" id="submit" data-url="<?php echo $buttons['save'] ?>">Save <i class="fa fa-check"></i></button>
			<?php endif; ?>
			<?php if ($buttons['delete']) : ?>
				<button type="submit" class="btn btn-danger"  id="delete" data-url="<?php echo $buttons['delete'] ?>">Delete <i class="fa fa-times"></i></button>
			<?php endif; ?>
		</div>
	</div>
</form>