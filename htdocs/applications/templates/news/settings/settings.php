<link href="/public/scripts/jquery-ui/jquery-ui.min.css" rel="stylesheet"></link>
<script src="/public/scripts/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="/public/scripts/fileuploader/css/style.css">
<link rel="stylesheet" href="/public/scripts/fileuploader/css/jquery.fileupload.css">
<script src="/public/scripts/fileuploader/js/jquery.ui.widget.js"></script>
<script src="/public/scripts/fileuploader/js/jquery.iframe-transport.js"></script>
<script src="/public/scripts/fileuploader/js/jquery.fileupload.js"></script>
<!--[if (gte IE 8)&(lt IE 10)]><script src="/public/scripts/fileuploader/js/cors/jquery.xdr-transport.js"></script><![endif]-->
<div class="page-header">
	<h4 class="page-title"><?php echo Request::instance()->title ?></h4>
</div>
<div role="tabpanel">
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#article" aria-controls="article" role="tab" data-toggle="tab">Article</a></li>
		<li role="presentation"><a href="#newsletter" aria-controls="newsletter" role="tab" data-toggle="tab">Newsletter</a></li>
		<li role="presentation"><a href="#quicklinks" aria-controls="quicklinks" role="tab" data-toggle="tab">Quick Links</a></li>
		<li role="presentation"><a href="#pictures" aria-controls="pictures" role="tab" data-toggle="tab">Template Pictures</a></li>
	</ul>
	<div class="clear-fix fix-40"></div>
	<div class="tab-content">
		<!-- article settings -->
		<div role="tabpanel" class="tab-pane active" id="article">
			<table class="table table-striped table-hover table-sm ajax-submitter">
				<thead>
					<tr>
						<th width="20">
							<a href="/gazette/settings/article/add" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></a>
						</th>
						<th>Name</th>
						<th width="25%">Property</th>
						<th width="25%">Value</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if ($articles) {
							foreach ($articles as $row) {
								$id = $row['id'];
								$field = $row['field'];
								$caption = translate::instance()->$field ?: $row['field'];
								echo "<tr>";
								echo "<td><a href='/gazette/settings/article/$id' class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o'></i></a></td>";
								echo "<td>$caption</td>";
								echo "<td>{$row[title]}</td>";
								echo "<td><input type='text' class='form-control input-sm' value='{$row[value]}' data-id='$id' /></td>";
								echo "</tr>";
							}
						}
					?>
				</tbody>
			</table>
		</div>
		<!-- newsletter settings -->
		<div role="tabpanel" class="tab-pane" id="newsletter">
			<table class="table table-striped table-hover table-sm ajax-submitter">
				<thead>
					<tr>
						<th width="20">
							<a href="/gazette/settings/newsletter/add" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></a>
						</th>
						<th>Name</th>
						<th width="25%">Property</th>
						<th width="25%">Value</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if ($newsletters) {
							foreach ($newsletters as $row) {
								$id = $row['id'];
								$field = $row['field'];
								$caption = translate::instance()->$field ?: $row['field'];
								echo "<tr>";
								echo "<td><a href='/gazette/settings/newsletter/$id' class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o'></i></a></td>";
								echo "<td>$caption</td>";
								echo "<td>{$row[title]}</td>";
								echo "<td><input type='text' class='form-control input-sm' value='{$row[value]}' data-id='$id' /></td>";
								echo "</tr>";
							}
						}
					?>
				</tbody>
			</table>
		</div>
		<!-- quick links -->
		<div role="tabpanel" class="tab-pane" id="quicklinks">
			<table class="table table-striped table-hover table-sm ajax-submitter">
				<thead>
					<tr>
						<th width="50%">Title</th>
						<th>Link</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if ($quicklinks) {
							foreach ($quicklinks as $row) {
								$id = $row['id'];
								$title = $row['title'];
								$link = $row['link'];
								echo "<tr>";
								echo "<td><input type='text' name='news_quicklink_title' class='form-control input-sm' value='{$row[title]}' data-id='$id' /></td>";
								echo "<td><input type='text' name='news_quicklink_link' class='form-control input-sm' value='{$row[link]}' data-id='$id' /></td>";
								echo "</tr>";
							}
						}
					?>
				</tbody>
			</table>
		</div>
		<!-- quick links -->
		<div role="tabpanel" class="tab-pane" id="pictures">
			<table class="table table-striped table-hover table-sm ajax-submitter">
				<thead>
					<tr>
						<th width="20">
							<a href="#" id="addImage" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></a>
						</th>
						<th width="50%">Name</th>
						<th>Picture</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if ($pictures) {
							foreach ($pictures as $img) {
								$name = "[".basename($img)."]";
								$file = str_replace($_SERVER['DOCUMENT_ROOT'], null, $img);
								echo "<tr>";
								echo "<td><a href='#' class='btn btn-danger btn-xs'><i class='fa fa-times'></i></a></td>";
								echo "<td>$name</td>";
								echo "<td><img src=\"$file\" /></td>";
								echo "</tr>";
							}
						}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- mail template modal -->
<div id="add-picture-modal" class="ado-modal ado-box">
	<div class="ado-modal-header">
		<div class="ado-title">Add New Picture</div>
		<div class="ado-subtitle">File types: jpg, jpeg, png, gif</div>
	</div>
	<div class="ado-modal-body">
		<div id="progress" class="progress">
			<div class="progress-bar progress-bar-success"></div>
		</div>
		<div id="files" class="files"></div>
	</div>
	<div class="ado-modal-footer" >
		<div class="ado-row ado-actions">
			<a class='btn btn-sm btn-default ado-modal-close'>
				<i class="fa fa-times"></i> Close
			</a>
			<a href="#" class="btn btn-sm btn-success fileinput-button">
				<i class="glyphicon glyphicon-plus"></i>
				<span>Add File</span>
				<!-- The file input field used as target for the file upload widget -->
				<input id="fileupload" type="file" name="files[]" multiple >
			</a>
			<a class='btn btn-sm btn-primary fileupload-submit'>
				<i class="fa fa-upload"></i> Upload
			</a>
		</div>
	</div>
</div>
