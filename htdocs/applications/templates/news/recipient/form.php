<div class="page-header">
	<h3 class="page-title"><?php echo $this->title ?></h3>
</div>
<div class="form-container form-container-xs">
	<form method="post" class="form-horizontal" id="newsRecipientForm" action="/applications/modules/news/recipient/submit.php"
		data-fv-framework="bootstrap"
	    data-fv-icon-valid="glyphicon glyphicon-ok"
	    data-fv-icon-invalid="glyphicon glyphicon-remove"
	    data-fv-icon-validating="glyphicon glyphicon-refresh" >
		<input type="hidden" name="news_recipient_id" id="news_recipient_id" value="<?php echo $data['news_recipient_id'] ?>">
		<input type="hidden" name="application" id="application" value="<?php echo $data['application'] ?>">
		<div class="form-group">
			<label for="news_recipient_firstname" class="col-sm-4 control-label">First Name</label>
			<div class="col-sm-8 has-feedback">
				<input type="text" class="form-control" name="news_recipient_firstname"
					placeholder="First Name"
					data-fv-row=".col-xs-8"
                	data-fv-notempty="true"
                	data-fv-notempty-message="First name is required"
                	value="<?php echo $data['news_recipient_firstname'] ?>" />
			</div>
		</div>
		<div class="form-group">
			<label for="news_recipient_name" class="col-sm-4 control-label">Name</label>
			<div class="col-sm-8 has-feedback">
				<input type="text" class="form-control" name="news_recipient_name"
					placeholder="Name"
					data-fv-row=".col-xs-8"
                	data-fv-notempty="true"
                	data-fv-notempty-message="Name is required"
                	value="<?php echo $data['news_recipient_name'] ?>" />
			</div>
		</div>
		<div class="form-group">
			<label for="news_recipient_email" class="col-sm-4 control-label">E-mail</label>
			<div class="col-sm-8 has-feedback">
				<input type="email" class="form-control" name="news_recipient_email"
					placeholder="E-mail"
					data-fv-row=".col-xs-8"
                	data-fv-notempty="true"
                	data-fv-notempty-message="E-mail is required"
                	value="<?php echo $data['news_recipient_email'] ?>" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-4 col-sm-8">
				<div class="checkbox">
					<label><input type="checkbox" name="news_recipient_active" id="news_recipient_active" value="1" <?php echo $data['active'] ?> /> is active</label>
					<input type="hidden" name="news_recipient_active_old" id="news_recipient_active_old" value="<?php echo $data['news_recipient_active'] ?>">
				</div>
			</div>
  		</div>
		<div class="form-group">
			<div class="col-sm-offset-4 col-sm-8">
				<a href="/gazette/recipients" class="btn btn-default"><i class="fa fa-chevron-left"></i> Back</a>
				<?php if ($buttons['save']) : ?>
					<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
				<?php endif; ?>				
				<?php if ($buttons['delete']) : ?>
					<a id="btnDelete" href="<?php echo $buttons['delete'] ?>" class="btn btn-danger"><i class="fa fa-times"></i> Delete</a>
				<?php endif; ?>

			</div>
		</div>
	</form>
</div>