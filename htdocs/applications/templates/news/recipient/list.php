<div class="page-header">
	<h3 class="page-title"><?php echo $this->title ?></h3>
</div>

<table class='table table-hover table-striped table-xs' id='categories' >
	<thead>
		<tr>
			<th width='20px'><a href='/gazette/recipients/add' class='btn btn-xs btn-primary' ><i class='fa fa-plus'></i></a></th>
			<th>Recipient</th>
			<th width='40%' >E-mail</th>
		</tr>
	</thead>
	<?php
		if ($recipients) {

			echo "<tbody>";

			foreach ($recipients as $recipient) {
				$id = $recipient['id'];
				$active = $recipient['active'] ? "<i class='fa fa-check-square'></i>" : "&nbsp;";
				echo "<tr data-id='$id'>";
				echo "<td>$active</td>";
				echo "<td nowrap=vowrap ><a href='/gazette/recipients/data/$id'>{$recipient[name]}</a></td>";
				echo "<td nowrap=vowrap >{$recipient[email]}</td>";
				echo "</tr>";
			}

			echo "</tbody>";
		}
	?>
</table>