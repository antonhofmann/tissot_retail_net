<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
		
	$form = new Form(array(
		'id' => 'material_form',
		'action' => $buttons['save'],
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->reference_id(
		Form::TYPE_HIDDEN
	);

	$form->product_group_name(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->product_subgroup_name(
		Form::TYPE_SELECT,
		Form::PARAM_AJAX
	);
	
	$form->collection_code(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED,
		'label=Collection'
	);
	
	$form->collection_category_code(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED,
		'label=Collection Collection'
	);
	
	$form->product_type_name(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->reference_ean_number(
		Form::TYPE_TEXT,
		Validata::minsize(13),
		"maxlength=13"
	);
	
	$form->reference_code(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->reference_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->reference_price_point(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		Validata::PARAM_NUMBER
	);
	
	$form->currency(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->reference_description(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL
	);
	
	$form->reference_active(
		Form::TYPE_CHECKBOX
	);
	
	$form->fieldset('Reference', array(
		'collection_code',
		'collection_category_code',
		'product_group_name',
		'product_subgroup_name',
		'product_type_name',
		'reference_ean_number',
		'reference_code',
		'reference_name',
		'reference_price_point',
		'currency',
		'reference_description',
		'reference_active'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<style type="text/css">

	#reference_ean_number,
	#reference_price_point { 
		width:120px;
	}
	
	#reference_description { 
		width: 440px; 
		height: 80px; 
	}
	
</style>
<script type="text/javascript">
	
	$(document).ready(function() {

		var productGroup = $('#product_group_name');

		// chainde counries
		productGroup.chainedSelect('#product_subgroup_name', {
			parameters: {
	      		section: "product.subgroups",
	      		application: $('#application').val()
	      	}
		});

		// trigger country onload
		if (productGroup.is('select')) {
			productGroup.trigger('change');
		}

	});

</script>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>