<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$_ID = $_REQUEST['id'];
	$item = new Item();
	$item->read($_ID);

	$_FILE_PURPOSE_COVER_IMAGE = 9;
	$_FILE_PURPOSE_TECHNICAL_DRAWING= 10;

	$model = new Model(Connector::DB_CORE);

	// get item units
	$result = $model->query("
		SELECT unit_id, unit_name 
		FROM units 
		ORDER BY unit_name
	")->fetchAll();

	$itemUnits = _array::extract($result);

	// get packaging_types
	$result = $model->query("
		SELECT packaging_type_id, packaging_type_name 
		FROM packaging_types 
		ORDER BY packaging_type_name
	")->fetchAll();

	$itemPackagingTypes = _array::extract($result);

	// get item files
	$result = $model->query("
		SELECT DISTINCT
			item_file_id,
			item_file_title,
			item_file_description,
			item_file_path,
			file_type_name,
			file_purpose_id,
			file_purpose_name,
			DATE_FORMAT(item_files.date_created, '%d.%m.%Y') AS date
		FROM item_files
		LEFT JOIN file_types ON item_file_type = file_type_id
		LEFT JOIN file_purposes ON file_purpose_id = item_file_purpose
		WHERE item_file_item = $_ID
		ORDER BY date, item_file_title
	")->fetchAll();

	$coverImage = array();
	$technicalDrawing = array();
	$pictures = array();
	$pdfDocuments = array();
	$otherFiles = array();

	// set cover image
	if ($result) {
		
		foreach ($result as $row) {

			$filepath = $_SERVER['DOCUMENT_ROOT'].$row['item_file_path'];
			$filePorpose = $row['file_purpose_id'];

			if (!file_exists($filepath)) {
				continue;
			}

			// cover image
			if (!$coverImage && $filePorpose==$_FILE_PURPOSE_COVER_IMAGE && check::image($filepath) ) {
				$coverImage = $row;
			} 
			// other files
			else {

				$otherFiles[] = $row;

				// technical drawing
				if ($filePorpose==$_FILE_PURPOSE_TECHNICAL_DRAWING && (check::image($filepath) || check::pdf($filepath))) {
					$technicalDrawing[] = $row;
				}
				else {
				
					// pictures
					if (check::image($filepath)) {
						$pictures[] = $row;
					}
					// pdf documents
					elseif (check::pdf($filepath)) {
						$pdfDocuments[] = $row;
					}
				}
			}
		}
	}


	$packingInformations = $model->query("
		SELECT 
			item_packaging_id AS id, 
			item_packaging_number  AS number,
			item_packaging_unit_id  AS unit,
			item_packaging_packaging_id  AS packaging,
			item_packaging_width AS width,
			item_packaging_height AS height,
			item_packaging_length AS length,
			item_packaging_weight_net AS weight_net,
			item_packaging_weight_gross AS weight_gross
		FROM item_packaging
		WHERE item_packaging_item_id = $_ID
	")->fetchAll();

	$spareParts = $result = $model->query("
		SELECT DISTINCT item_id, item_code, item_name
		FROM parts
		INNER JOIN items ON item_id = part_child
		WHERE part_parent = $_ID
	")->fetchAll();

	$result = $model->query("
		SELECT DISTINCT
			certificate_files.certificate_file_id, 
			certificate_files.certificate_file_version, 
			DATE_FORMAT(certificate_files.certificate_file_expiry_date, '%d.%m.%Y') AS expiry_date, 
			certificate_files.certificate_file_file,
			certificates.certificate_id, 
			certificates.certificate_name, 
			addresses.address_company
		FROM addresses 
		INNER JOIN certificates ON addresses.address_id = certificates.certificate_address_id
		INNER JOIN certificate_files ON certificate_files.certificate_file_certificate_id = certificates.certificate_id
		INNER JOIN item_certificates ON certificates.certificate_id = item_certificates.item_certificate_certificate_id
		WHERE item_certificates.item_certificate_item_id = $_ID AND certificate_file_expiry_date >= CURRENT_DATE
		ORDER BY certificate_file_expiry_date DESC
	")->fetchAll();

	$certificates = array();

	if ($result) {
		foreach ($result as $row) {
			if (file_exists($_SERVER['DOCUMENT_ROOT'].$row['certificate_file_file'])) {
				$certificates[] = $row;
			}
		}
	}

	//item information, additional fiels
	$result = $model->query("
		SELECT DISTINCT
			item_information_id, item_information_file
		FROM item_informations 
		INNER JOIN item_information_items ON item_information_item_item_information_id = item_information_id
		WHERE item_information_item_item_id = $_ID
	")->fetchAll();


	

	$iteminformation = array();

	if ($result) {
		foreach ($result as $row) {
			if (file_exists($_SERVER['DOCUMENT_ROOT'].$row['item_information_file'])) {
				$iteminformation[] = $row;
			}
		}
	}
	$itemMaterials = array();

	// get certificate materials
	$result = $model->query("
		SELECT DISTINCT catalog_material_name AS material
		FROM catalog_materials 
		INNER JOIN item_catalog_materials ON item_catalog_material_material_id = catalog_material_id
		WHERE item_catalog_material_item_id = $_ID
		ORDER BY catalog_material_name
	")->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$itemMaterials[] = $row['material'];
		}
	}

	// get item  materials
	$result = $model->query("
		SELECT DISTINCT item_material_name AS material
		FROM item_materials
		WHERE item_material_item_id = $_ID
		ORDER BY item_material_name
	")->fetchAll();


	if ($result) {
		foreach ($result as $row) {
			
			$itemMaterials[] = $row['material'];
		}
	}

	$itemMaterials = $itemMaterials ? join('<br />', array_unique($itemMaterials)) : null;

	// get item  materials
	$ElectricalSpecifications = $model->query("
		SELECT item_electricspec_name, item_electricspec_description
		FROM item_electricspecs
		WHERE item_electricspec_item_id = $_ID
	")->fetchAll();

	// project cost group name
	$result = $model->query("
		SELECT GROUP_CONCAT(DISTINCT project_cost_groupname_name) AS costGroup
		FROM project_cost_groupnames
		WHERE  project_cost_groupname_id = $item->cost_group
		ORDER BY project_cost_groupname_name
	")->fetch();

	$ProjectCostGroupName = $result['costGroup'];

	// item supplier
	$result = $model->query("
		SELECT CONCAT(address_company, ', ', country_name) AS company, supplier_item_price, currency_symbol
		FROM suppliers 
		INNER JOIN addresses ON supplier_address = address_id
		INNER JOIN countries ON country_id = address_country
		INNER JOIN currencies on currency_id = address_currency 
		WHERE supplier_item = $_ID
		LIMIT 1
	")->fetch();

	$ItemSupplier = $result['company'];
	$CurrencySymbol = $result['currency_symbol'];
	$SupplierPrice = $result['supplier_item_price'];

	Compiler::attach(array(
		'/public/themes/default/css/reset.css',
		'/public/themes/default/css/layout.css',
		'/public/themes/default/css/table.css',
		'/public/themes/default/css/form.css',
		'/public/css/gui.css',
		'/public/themes/default/css/gui.css',
		'/public/scripts/adomodal/adomodal.css',
		'/public/themes/default/css/modal.css'
	));

	Compiler::export();
?>
<!doctype html>
<!--[if IE 7]>    <html class="ie ie7" lang="en"> <![endif]-->
<!--[if gt IE 7]>    <html class="ie" lang="en"> <![endif]-->
<!--[if !IE]><!--><html lang="en"> <!--<![endif]--> 
<head>
	<title>Item Info Box (modal) - Retail Net</title>
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link href="/public/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="/public/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/x-icon" />
	
	<link rel="stylesheet" type="text/css" href="/public/scripts/fancybox/fancybox.css?v=2.1.5" />
	<link rel="stylesheet" type="text/css" href="/public/scripts/jgrowl/jgrowl.css" />
	<link rel="stylesheet" href="/public/css/spinners.css">
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/modal.css" />
	<link rel="stylesheet" href="/public/scripts/bootstrap/css/bootstrap.compiled.css">
	<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
	<?php echo Compiler::get('css') ?>

	<script src="/public/scripts/jquery/1.9.1.js" type="text/javascript"></script>
	<script src="/public/scripts/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="/public/scripts/message.js"></script>
	<script type="text/javascript" src="/public/scripts/retailnet.js"></script>
	<script type="text/javascript" src="/public/scripts/fancybox/fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="/public/scripts/jgrowl/jgrowl.js"></script>
	<script src="/public/scripts/adomodal/adomodal.js" type="text/javascript"></script> 
	<script type="text/javascript" src="/public/scripts/jquery.actual.min.js"></script>
	<?php echo Compiler::get('js') ?>

	<style type="text/css">
	
		body,
		body.adomat {
			background: #eeeeee !important;
			min-width: 400px !important;
		}
		
		.modalbox-container {
			display: block;
		}

		.modalbox-container {
			display: block;
			height: 640px;
		}

	
		.image-responsive {
			max-width: 760px;
		}

		.info-section + .info-section {
			margin-top: 30px;
		}

		.section-title {
			font: bold 14px arial;
			/*border-bottom: 1px solid #dedede;*/
			padding-bottom: 5px;
			margin-bottom: 10px;
		}

		.section-title .table-striped tbody tr:last-child td {
			border-bottom: 1px solid #ddd;
		}

		.cel-label {
			white-space: nowrap;
			font-weight: bold;
		}


		.info-section tr th {
			font-weight: 500;
			font-size: 12px;
		}

		.info-section small {
			font-weight: 500;
			font-size: 10px;
		}

		.info-section tbody td,
		.info-section tfoot td {
			font-size: 12px;
		}

		.cover-image {
			width: 100%;
			margin-bottom: 40px;
		}

		.cover-image img {
			margin: 0 auto;
		}

		.print-menu li {
			text-align: left;
			font-size: 12px;
		}

		.print-menu li label {
			margin: 0;
		}

		.print-menu li:hover { color: #000; }

		.bootstrap .table .table  {
			background-color: transparent !important;
			margin: 0;
		}

		.bootstrap .table .table td {
			background-color: transparent !important;
			border: 0 !important;
		}

		.bootstrap .table .table tr td:first-child {
			padding-left: 0;
		}
		
	</style>

	<script type="text/javascript">
		$(document).ready(function() {

			retailnet.loader.init();

			// close modal screen
			$('.close', $('#modalbox')).click(function(e) {
				
				e.preventDefault();
				e.stopImmediatePropagation();
				
				parent.retailnet.modal.close();
				
				return false;
			});

			$('.print-menu ul.dropdown-menu li a').on('click', function (e) {
				
				e.preventDefault();
				e.stopPropagation();
				
				var checkbox = $('input[type="checkbox"]', $(this).closest('li'));

				if (checkbox.is(':disabled') || checkbox.prop('readonly')) {
					return false;
				}

				var checked = $(e.target).is(':checkbox') 
					? checkbox.is(':checked') 
					: !checkbox.is(':checked');
				
				checkbox.prop('checked', checked);
				
				return false;
			})

			$('.print-menu ul.dropdown-menu li input[type="checkbox"]').on('click', function (e) {
				e.stopImmediatePropagation();
			})

			$("#btnPrintItemInfo").on('click', function(e) {

				e.preventDefault();
				e.stopPropagation();

				var self = $(this);

				retailnet.loader.show();

				$.ajax({
					url: self.prop('href'), 
					type: "POST",
					dataType : "json",
					data: $('.print-menu :input').serialize(),
					success : function (xhr) {
						
						if (xhr && xhr.errors) {
							$.each(xhr.errors, function(i, msg) {
								parent.retailnet.notification.error(msg);
							})
							return;
						}

						if (xhr && xhr.file) {
							parent.window.location = xhr.file;
						}
					},
					complete: function() {
						retailnet.loader.hide();
					}
				})

				return false;
			})

		})
	</script>
	
</head>
<body class="adomat">

	<div class="modalbox-container">
		<div id="modalbox" class="adomat modalbox">
			<div class="modalbox-header">
				<div class="title"><?php echo $item->code ?></div>
				<div class="subtitle"><?php echo $item->name ?></div>
				<span class="fa-stack ado-modal-close close">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-times fa-stack-1x fa-inverse"></i>
				</span>
			</div>
			<div class="modalbox-content-container">
				<div class="modalbox-content">

					<?php 

						if ($coverImage) {
							echo "<div class='cover-image bootstrap'>";
							echo "<img src='{$coverImage[item_file_path]}' class='img-responsive' />";
							echo "</div>";
						}	
					?>

					<!-- Product properties -->
					<div class="info-section bootstrap">
						<h4 class="section-title">Product Properties</h4>
						<table class="table table-condensed table-striped">
							<tbody>
								
								<?php if ($item->code) : ?>
								<tr>
									<td width="30%" class="cel-label">Product Code</td>
									<td><?php echo $item->code ?></td>
								</tr>
								<?php endif; ?>
								
								<?php if ($item->name) : ?>
								<tr>
									<td width="30%" class="cel-label">Product Name</td>
									<td><?php echo $item->name ?></td>
								</tr>
								<?php endif; ?>
								
								<?php if ($item->description) : ?>
								<tr>
									<td width="30%" class="cel-label">Description</td>
									<td><?php echo $item->description ?></td>
								</tr>
								<?php endif; ?>
								
								<?php 
									if ($SupplierPrice) { ?>
										<tr>
											<td width="30%" class="cel-label">Price</td>
											<td><?php echo $CurrencySymbol . " " .$SupplierPrice; ?></td>
										</tr>
								<?php 
									} 
								    elseif ($item->price) 
									{ 
								?>
										<tr>
											<td width="30%" class="cel-label">Price</td>
											<td><?php echo "CHF ".$item->price; ?></td>
										</tr>
								<?php 
									} ?>
								
								<?php if ($item->watches_displayed) : ?>
								<tr>
									<td width="30%" class="cel-label">Products Displayed</td>
									<td><?php echo $item->watches_displayed ?></td>
								</tr>
								<?php endif; ?>
								
								<?php if ($item->watches_stored) : ?>
								<tr>
									<td width="30%" class="cel-label">Products Stored</td>
									<td><?php echo $item->watches_stored ?></td>
								</tr>
								<?php endif; ?>
								
								<?php if ($itemMaterials) : ?>
								<tr>
									<td width="30%" class="cel-label">Materials</td>
									<td><?php echo $itemMaterials ?></td>
								</tr>
								<?php endif; ?>

								<?php if ($ElectricalSpecifications) : ?>
								<tr>
									<td width="30%" class="cel-label">Electrical specifications</td>
									<td>
									<?php 
										echo "<table class='table table-condensed'>";

										foreach ($ElectricalSpecifications as $row) {
											echo "<tr>";
											echo "<td width='50%'>{$row[item_electricspec_name]}</td>";
											echo "<td>{$row[item_electricspec_description]}</td>";
											echo "</tr>";
										}

										echo "</table>";
									?>
									</td>
								</tr>
								<?php endif; ?>
								
								<?php if ($item->regulatory_approvals) : ?>
								<tr>
									<td width="30%" class="cel-label">Regulatory Approvals</td>
									<td><?php echo $item->regulatory_approvals ?></td>
								</tr>
								<?php endif; ?>
								
								<?php if ($item->install_requirements) : ?>
								<tr>
									<td width="30%" class="cel-label">Installation Information</td>
									<td><?php echo $item->install_requirements ?></td>
								</tr>
								<?php endif; ?>
								
								<?php if ($ProjectCostGroupName) : ?>
								<tr>
									<td width="30%" class="cel-label">Cost Group</td>
									<td><?php echo $ProjectCostGroupName ?></td>
								</tr>
								<?php endif; ?>
								
								<?php if ($ItemSupplier) : ?>
								<tr>
									<td width="30%" class="cel-label">Supplier</td>
									<td><?php echo $ItemSupplier ?></td>
								</tr>
								<?php endif; ?>

							</tbody>
						</table>
					</div>

					<?php

						$showItemLength = $item->length && $item->length <> '0.00' ? true : false;
						$showItemWidth = $item->width && $item->width <> '0.00' ? true : false;
						$showItemHeight = $item->height && $item->height <> '0.00' ? true : false;
						$showNetWeight = $item->net_weight && $item->net_weight <> '0.00' ? true : false;
						$showUnit = $itemUnits[$item->unit] ? true : false;
						$showStackable = $item->stackable ? true : false;
						$showLogistic = array_filter(array($showItemLength,$showItemWidth,$showItemHeight,$showNetWeight,$showUnit,$showStackable));
					?>

					<!-- Logistic Information -->
					<?php if ($showLogistic) : ?>
					<div class="info-section bootstrap">
						<h4 class="section-title">Dimensions and Weight</h4>
						<table class="table table-condensed table-striped">
							<tbody>
								
								<?php if ($showItemLength) : ?>
								<tr>
									<td width="30%" class="cel-label">Length <small>in cm</small></td>
									<td><?php echo $item->length ?></td>
								</tr>
								<?php endif; ?>

								<?php if ($showItemWidth) : ?>
								<tr>
									<td width="30%" class="cel-label">Width <small>in cm</small></td>
									<td><?php echo $item->width ?></td>
								</tr>
								<?php endif; ?>

								<?php if ($showItemHeight) : ?>
								<tr>
									<td width="30%" class="cel-label">Height <small>in cm</small></td>
									<td><?php echo $item->height ?></td>
								</tr>
								<?php endif; ?>

								<?php if ($showNetWeight) : ?>
								<tr>
									<td width="30%" class="cel-label">Net weight <small>in kg</small></td>
									<td><?php echo $item->net_weight ?></td>
								</tr>
								<?php endif; ?>

								<?php if ($showUnit) : ?>
								<tr>
									<td width="30%" class="cel-label">Unit</td>
									<td><?php echo $itemUnits[$item->unit]; ?></td>
								</tr>
								<?php endif; ?>

								<?php if ($showStackable) : ?>
								<tr>
									<td width="30%" class="cel-label">Stackable</td>
									<td>yes</td>
								</tr>
								<?php endif; ?>

							</tbody>
						</table>
					</div>
					<?php endif; ?>

					<!-- Remarks -->
					<div class="info-section bootstrap">
						<h4 class="section-title">Remarks</h4>
						<table class="table table-condensed table-striped">
							<tbody>
								<tr>
									<td>Prices and technical data may change at any given time.</td>
								</tr>
							</tbody>
						</table>
					</div>

					<!-- Packing Informations -->
					<?php if ($packingInformations) :  ?>
					<div class="info-section bootstrap">
						<h4 class="section-title">Packing Information</h4>
						<table class="table table-condensed table-striped package-informations" >
							<thead>
								<th nowrap="nowrap" width="5%">Number</th>
								<th nowrap="nowrap" >Unit of</th>
								<th nowrap="nowrap" >Packaging</th>
								<th nowrap="nowrap" width="5%">Length <small>cm</small></th>
								<th nowrap="nowrap" width="5%">Width <small>cm</small></th>
								<th nowrap="nowrap" width="5%">Height <small>cm</small></th>
								<th nowrap="nowrap" width="5%">CBM</th>
								<th nowrap="nowrap" width="5%">Net Weight <small>kg</small></th>
								<th nowrap="nowrap" width="5%">Gross Weight <small>kg</small></th>
							</thead>
							<tbody>
							<?php 
				
								$totalCbm = 0;
								$totalWeightNet = 0;
								$totalWeightGross = 0;

								foreach ($packingInformations as $row) {

									$cbm = $row['length'] && $row['width'] && $row['height'] 
										? number_format($row['length']*$row['width']*$row['height']/1000000, 4, '.', '') 
										: 0;

									$totalCbm += $cbm;
									$totalWeightNet += $row['weight_net'];
									$totalWeightGross += $row['weight_gross'];

									$cbm = $cbm ?: null;

									$packagingUnit = $itemUnits[$row['unit']];
									$packagingType = $itemPackagingTypes[$row['packaging']];

									echo "<tr>";
									echo "<td nowrap=nowrap >{$row[number]}</td>";
									echo "<td nowrap=nowrap >$packagingUnit</td>";
									echo "<td nowrap=nowrap >$packagingType</td>";
									echo "<td nowrap=nowrap >{$row[length]}</td>";
									echo "<td nowrap=nowrap >{$row[width]}</td>";
									echo "<td nowrap=nowrap >{$row[height]}</td>";
									echo "<td nowrap=nowrap >$cbm</td>";
									echo "<td nowrap=nowrap >{$row[weight_net]}</td>";
									echo "<td nowrap=nowrap >{$row[weight_gross]}</td>";
									echo "</tr>";
								}
								$totalWeightNet = $totalWeightNet ?: null;
								$totalWeightGross = $totalWeightGross ?: null;

							?>
							</tbody>
							<?php if ($totalWeightNet || $totalWeightGross) : ?>
							<tfoot>
								<tr>
									<td colspan="6" align="right" >Total:</td>
									<td><?php echo $totalCbm ?></td>
									<td><?php echo number_format($totalWeightNet, 2, '.', '') ?></td>
									<td><?php echo number_format($totalWeightGross, 2, '.', '') ?></td>
								</tr>
							<tfoot>
						<?php endif; ?>
						</table>
					</div>
					<?php endif; ?>

					<!-- Spare Parts -->
					<?php if ($spareParts) : ?>
					<div class="info-section bootstrap">
						<h4 class="section-title">Spare Parts</h4>
						<table class="table table-condensed table-striped" >
							<thead>
								<th nowrap="nowrap" width="30%">Item Code</th>
								<th nowrap="nowrap" >Item Name</th>
							</thead>
							<tbody>
							<?php
								foreach ($spareParts as $row) {
									echo "<tr>";
									echo "<td>{$row[item_code]}</td>";
									echo "<td>{$row[item_name]}</td>";
									echo "</tr>";
								}
							?>
							</tbody>
						</table>
					</div>
					<?php endif; ?>

					<!-- attached files (other) -->
					<?php if ($otherFiles) : ?>
					<div class="info-section bootstrap">
						<h4 class="section-title">Attached Files</h4>
						<table class="table table-condensed table-striped" >
							<thead>
								<th nowrap="nowrap">File</th>
								<th nowrap="nowrap" width="15%">File Type</th>
								<th nowrap="nowrap" width="15%">File Purpose</th>
								<th nowrap="nowrap" width="5%" >Date</th>
							</thead>
							<tbody>
							<?php 
								foreach ($otherFiles as $row) {
									echo "<tr>";
									echo "<td>";
									echo "<a href='{$row[item_file_path]}' target='_blank' >{$row[item_file_title]}</a>";
									echo "</td>";
									echo "<td nowrap='nowrap'>{$row[file_type_name]}</td>";
									echo "<td nowrap='nowrap'>{$row[file_purpose_name]}</td>";
									echo "<td nowrap='nowrap' >{$row[date]}</td>";
									echo "</tr>";
								}
							?>
							</tbody>
						</table>
					</div>
					<?php endif; ?>
					
					<!-- File certificates -->
					<?php if ($certificates) : ?>
					<div class="info-section bootstrap">
						<h4 class="section-title">File Certificates</h4>
						<table class="table table-condensed table-striped" >
							<thead>
								<th nowrap="nowrap">Version Name</th>
								<th nowrap="nowrap" width="10%" >Expiry</th>
							</thead>
							<tbody>
							<?php 
								foreach ($certificates as $row) {
									if (file_exists($_SERVER['DOCUMENT_ROOT'].$row['certificate_file_file'])) {
										echo "<tr>";
										echo "<td>";
										echo "<a href='{$row[certificate_file_file]}' target='_blank' >{$row[certificate_name]}</a><br />";
										echo "<small>{$row[address_company]}, Version: {$row[certificate_file_version]}</small>";
										echo "</td>";
										echo "<td>{$row[expiry_date]}</td>";
										echo "</tr>";
									}
								}
							?>
							</tbody>
						</table>
					</div>
					<?php endif; ?>

				</div>
			</div>
			<div class="modalbox-footer">
				<div class="modalbox-actions ado-actions">
					<div class="bootstrap print-menu">
						<input type="hidden" name="id" value="<?php echo $_ID ?>">
						<input type="hidden" name="sections[cover]" value="1">
						<div class="btn-group dropup pull-right">
							<a id="btnPrintItemInfo" role="button" class="btn btn-default btn-sm" href="/applications/modules/item/print.info.php">Print</a>
							<a role="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dLabel">
								<li class="dropdown-header">Print Sections</li>
								<li><a href="#"><label><input type="checkbox" disabled="disabled" checked="checked" value="1"> Cover</label></a></li>
								<li><a href="#"><label><input name="sections[properties]" type="checkbox" checked="checked" value="1"> Product Properties</label></a></li>
								<?php if ($technicalDrawing) : ?>
								<li><a href="#"><label><input name="sections[technical]" type="checkbox" checked="checked" value="1"> Technical Drawing</label></a></li> 
								<?php endif; ?>
								<li><a href="#"><label><input name="sections[logistic]" type="checkbox" checked="checked" value="1"> Logistic Information</label></a></li>
								<li><a href="#"><label><input name="sections[remarks]" type="checkbox" checked="checked" value="1"> Remarks</label></a></li>
								<?php if ($packingInformations) : ?>
								<li><a href="#"><label><input name="sections[packing]" type="checkbox" checked="checked" value="1"> Packing Informations</label></a></li>
								<?php endif; ?>
								<?php if ($spareParts) : ?>
								<li><a href="#"><label><input name="sections[spareparts]" type="checkbox" checked="checked" value="1"> Spare Parts</label></a></li>
								<?php endif; ?>
								<?php if ($pictures || $pdfDocuments || $certificates || $iteminformation) : ?>
								<li role="separator" class="divider"></li>
								<li class="dropdown-header">Include</li>
								<?php endif; ?>
								<?php if ($pictures || $pdfDocuments) : ?>
								<li><a href="#"><label><input name="sections[files]" type="checkbox" value="1"> Files (PDF or Images)</label></a></li>
								<?php endif; ?>
								<?php if ($certificates) : ?>
								<li><a href="#"><label><input name="sections[certificates]" type="checkbox" value="1"> Certificates</label></a></li>
								<?php endif; ?>
								<?php if ($iteminformation) : ?>
								<li><a href="#"><label><input name="sections[iteminformation]" type="checkbox" value="1"> Additional Item Information</label></a></li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>