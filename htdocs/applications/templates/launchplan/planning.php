<style type="text/css">
	
	.new-warehouse-container {
		display: none;
	}
	
	.new-warehouse-container input[type=text] {
		display: block;
		width: 354px;
		height: 20px;
		padding: 1px 2px;
		border: 1px solid silver;
	}
	
	.new-warehouse-container .error {
		border-color: red !important;
	}
	
	.new-warehouse-container small {
		display: block;
		margin: 10px 0 2px;
	}
	
	.warrning {
		color: #ff6000;
		float: right;
	}
	
</style>
<div class="actions">	
	<?php 

		echo ui::button(array(
			'id' => 'close',
			'icon' => 'icon151',
			'caption' => 'Close'
		));
	
		if ($buttons) {
			foreach ($buttons as $name => $button) {
				$button['id'] = $name;
				if ($name=='pop_filter') $button['class'] = $filter_active;
				echo ui::button($button);
			}
		}
	
	?>
</div>
<div id='spreadsheet' class='<?php echo $modeClass; ?>'></div>
<div class="tooltips-container">
	<div class="image-loader" ><img src="/public/images/loader-quer.gif" /></div>
</div>
<?php

	$translate = Translate::instance();
	$request = request::instance();
	
	echo $request->form();

	if ($dropdowns) {
		echo "<form id='filters' method=post action='$formAction' class=default  ><ul>";
		foreach ($dropdowns as $dropdown) echo "<li>$dropdown</li>";
		echo "</ul></form>";
	}
	
	// dropdown warehouses
	$dropdownWarehouses .= ui::dropdown($warehouses, array(
		'id' => 'address_warehouse_id',
		'name' => 'address_warehouse_id',
		'caption' => 'Select Company Warehouse',
		'value' => ''
	));
	
	/*
	// add ordersheet warehouse
	echo ui::dialogbox(array(
		'id' => 'add_warehouse_dialog',
		'title' => $translate->warehouse,
		'content' => "
			<form method=post action='/applications/modules/launchplan/planning/warehouse.php' id='warehouseForm' >
				$dropdownWarehouses
				<span class='new-warehouse-container'>
					<small>Warehouse Name *</small>
					<input type=text name=address_warehouse_name class='silver required'  />
					$sapFields
				</span>
			</form>
		",
		'buttons' => array(
			'add_warehouse_cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'add_warehouse_apply' => array(
				'icon' => 'apply',
				'class' => 'apply-dialog disabled',
				'label' => $translate->create
			)
		)
	));
	

	// remove ordersheet warehouse
	echo ui::dialogbox(array(
		'id' => 'remove_warehouse_dialog',
		'title' => $translate->warehouse,
		'content' => $translate->dialog_warehouse,
		'buttons' => array(
			'remove_warehouse_cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel,
				'class' => 'cancel'
			),
			'remove_warehouse_apply' => array(
				'icon' => 'apply',
				'class' => 'apply-dialog',
				'label' => $translate->delete
			)
		)
	));


	// ordersheet delivering confirmation
	echo ui::dialogbox(array(
		'id' => 'confirm_dialog',
		'title' => $translate->confirm_ordersheet_planning,
		'content' => nl2br($translate->dialog_confirm_ordersheet_planning),
		'buttons' => array(
			'confirm_cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel,
				'class' => 'cancel'
			),
			'confirm_apply' => array(
				'icon' => 'save',
				'label' => $translate->yes,
				'class' => 'apply'
			)
		)
	));

	// ordersheet partially distribution 
	echo ui::dialogbox(array(
		'id' => 'partially_distribution_dialog',
		'title' => $translate->ordersheet_partially_distribution,
		'content' => nl2br($translate->dialog_ordersheet_partially_distribution),
		'buttons' => array(
			'partially_distribution_cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel,
				'class' => 'cancel'
			),
			'partially_distribution_apply' => array(
				'icon' => 'save',
				'label' => $translate->yes,
				'class' => 'apply'
			)
		)
	));
	*/
?>

<!-- partially distribution dialog -->
<div id="partially_distribution_dialog"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" ><?php echo $translate->ordersheet_partially_distribution; ?></div>
	</div>
	<div class="ado-modal-body">Are you sure to put the launch plan into archive?</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="#" >
				<span class="icon apply"></span>
				<span class="label" ><?php echo $translate->yes ?></span>
			</a>
		</div>
	</div>
</div>

<!-- remove warehose dialog -->
<div id="remove_warehouse_dialog"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" ><?php echo $translate->warehouse; ?></div>
	</div>
	<div class="ado-modal-body">
		<?php echo nl2br($translate->dialog_warehouse); ?>
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="#" >
				<span class="icon apply"></span>
				<span class="label" ><?php echo $translate->delete ?></span>
			</a>
		</div>
	</div>
</div>

<!-- confirm quantities dialog -->
<div id="confirm_dialog"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" >Confirm</div>
	</div>
	<div class="ado-modal-body">
		<?php echo nl2br($translate->dialog_confirm_ordersheet_planning); ?>
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="#" >
				<span class="icon apply"></span>
				<span class="label" ><?php echo $translate->yes ?></span>
			</a>
		</div>
	</div>
</div>

<!-- add new warehouse -->
<div id="add_warehouse_dialog"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" ><?php echo $translate->warehouse; ?></div>
	</div>
	<div class="ado-modal-body">
		<form method="post" action="/applications/modules/launchplan/planning/warehouse.php" >
			<small>Select Warehouse</small>
			<?php echo $dropdownWarehouses ?>
			<span class='new-warehouse-container'>
				<small>Warehouse Name *</small>
				<input type="text" name="address_warehouse_name" class='required'  />
				<?php echo $sapFields ?>
			</span>
		</form>
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="#" >
				<span class="icon apply"></span>
				<span class="label" ><?php echo $translate->create ?></span>
			</a>
		</div>
	</div>
</div>


