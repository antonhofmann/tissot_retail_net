<?php 

	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript" src="/public/js/launchplan.list.js" ></script>
<style type="text/css">

	#ordersheets {
		width: 1400px;
	}
	
	.dropdown-actions {
		margin: 0 10px 0 0;
	}
	
	.dropdown-actions .button {
		background: #f2f6f8;
		background: -moz-linear-gradient(top,  #f2f6f8 0%, #d8e1e7 50%, #b5c6d0 51%, #e0eff9 100%);
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f2f6f8), color-stop(50%,#d8e1e7), color-stop(51%,#b5c6d0), color-stop(100%,#e0eff9));
		background: -webkit-linear-gradient(top,  #f2f6f8 0%,#d8e1e7 50%,#b5c6d0 51%,#e0eff9 100%);
		background: -o-linear-gradient(top,  #f2f6f8 0%,#d8e1e7 50%,#b5c6d0 51%,#e0eff9 100%);
		background: -ms-linear-gradient(top,  #f2f6f8 0%,#d8e1e7 50%,#b5c6d0 51%,#e0eff9 100%);
		background: linear-gradient(to bottom,  #f2f6f8 0%,#d8e1e7 50%,#b5c6d0 51%,#e0eff9 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f6f8', endColorstr='#e0eff9',GradientType=0 );	
	}
	
	.dropdown-actions .button .label {
		color: maroon;
	}
	
</style>
<div id="ordersheets"></div>

<?php  
	echo $request->form();
?>