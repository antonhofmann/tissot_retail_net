<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();
	
	$form = new Form(array(
		'action' => '/applications/modules/launchplan/submit.php',
		'method' => 'post',
		'class' => 'launchplan validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->lps_launchplan_id(
		Form::TYPE_HIDDEN
	);
	
	$form->lps_mastersheet_year(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE,
		'label='.$translate->mastersheet_year	
	);
	
	$form->lps_mastersheet_name(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE,
		'label='.$translate->mastersheet_name
	);
	
	$form->lps_launchplan_workflowstate_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED,
		'label='.$translate->workflow_state		
	);
	
	$form->lps_launchplan_comment(
		Form::TYPE_WYSIWYG,
		Form::PARAM_LABEL,
		'class=tinymce',
		'label='.$translate->comment		
	);
	
	$form->lps_launchplan_openingdate(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_REQUIRED,
		Validata::PARAM_DATE,
		"class=datepicker",
		'label='.$translate->opening_date	
	);
	
	$form->lps_launchplan_closingdate(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::PARAM_REQUIRED,
		Validata::PARAM_DATE,
		"class=datepicker",
		'label='.$translate->closing_date	
	);
	
	$form->fieldset('Launch Plan', array(
		'lps_mastersheet_year',
		'lps_mastersheet_name',
		'lps_launchplan_workflowstate_id',
		'lps_launchplan_comment',
		'lps_launchplan_openingdate',
		'lps_launchplan_closingdate'
	));
	
	if($trackings) {
		
		$form->trackinglist(
			Form::TYPE_AJAX, 
			Form::PARAM_SHOW_VALUE, 
			Form::PARAM_NO_LABEL
		);
		
		$form->fieldset('tracking', array('trackinglist'));
		
		$table = new Table(array('class'=>'listing'));
		$table->datagrid = $trackings;
		$table->date(Table::ATTRIBUTE_NOWRAP, 'width=15%');
		$table->workflow_state(Table::ATTRIBUTE_NOWRAP, 'width=40%');
		$table->user(Table::ATTRIBUTE_NOWRAP); 
		$data['trackinglist'] = $table->render();	
	}
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['print']) {
		$form->button(ui::button(array(
			'id' => 'export',
			'icon' => 'print',
			'href' => $buttons['print'],
			'label' => $translate->print
		)));
	}
	
	if ($buttons['version']) {
		$form->button(ui::button(array(
			'id' => 'version',
			'class' => 'dialog',
			'icon' => 'add',
			'href' => $buttons['version'],
			'label' => $translate->create_version
		)));
	}
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'label' => $translate->delete,
			'href' => $buttons['delete']
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<style type="text/css">

	#ordersheet { 
		width: 700px; 
	}
	
	textarea#lps_launchplan_comment { 
		width:400px; height: 100px;
	}
	
	form input.datapicker {
		width:100px !important;
	}
	
	#version_name {
		width: 350px !important;
		border: 1px solid silver;
		padding: 5px;
		color: black;
		font-size: .9em;
	}
	
	#version_name.silver {
		color: silver;
	}
	
	#trackinglist {
		padding: 20px;
		width: 660px;
		float:left;
	}
	
	table.listing th,
	table.listing td{
		padding: 5px;
		border-bottom: 1px solid silver;
	}
	
</style>
<script type="text/javascript" src="/public/scripts/tinymce4/js/tinymce/tinymce.min.js"></script>
<?php 
	
	echo $form; 

	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			)
		)
	));

	$content = "
		<input type=text name=version_name id=version_name placeholder='".$translate->version_title."' />
		<input type=hidden name=version_url id=version_url value='".$buttons['version']."' />
	";

	echo ui::dialogbox(array(
		'id' => 'version_dialog',
		'title' => $translate->create_version,
		'content' => $content,
		'buttons' => array(
			'versionCancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'versionApply' => array(
				'icon' => 'apply',
				'class' => 'disabled',
				'label' => $translate->create
			)
		)
	));
?>