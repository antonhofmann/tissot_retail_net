<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$user = User::instance();
	$translate = Translate::instance();
	
	if ($datagrid) { 
	
		// list
		$list = "<table class=listing >";
		$list .= "<thead>";
		$list .= "<tr>";
		$list .= "<td>&nbsp;</td>";
		$list .= "<td class=currency>Currency</td>";
		$list .= "<td class=number >Quantities</td>";
		$list .= "<td class=number >Approved Quantities</td>";
		$list .= "</tr>";
		$list .= "</thead>";
		$list .= "<tbody>";

		foreach ($datagrid as $group => $row) {
			
			$groupTotalQuantity = 0;
			$groupTotalQuantityApproved = 0;
			
			$list .= "<tr class=group >";
			$list .= "<td>{$row[caption]}</td>";

			foreach ($row['data'] as $i => $item) {
				$groupTotalQuantity = $groupTotalQuantity+$item['calc_quantity'];
				$groupTotalQuantityApproved = $groupTotalQuantityApproved+$item['calc_quantity_approved'];
			}

			// number format
			$groupTotalQuantity = number_format($groupTotalQuantity, 2, '.', '');
			$groupTotalQuantityApproved = number_format($groupTotalQuantityApproved, 2, '.', '');

			$list .= "<td class=currency >CHF</td>";
			$list .= "<td class=number >$groupTotalQuantity</td>";
			$list .= "<td class=number >$groupTotalQuantityApproved</td>";
			$list .= "<tr>";
			
			$totalQuantities = $totalQuantities + $groupTotalQuantity;
			$totalQuantitiesApproved = $totalQuantitiesApproved + $groupTotalQuantityApproved;
		}
		
		// number format
		$totalQuantities = number_format($totalQuantities, 2, '.', '');
		$totalQuantitiesApproved = number_format($totalQuantitiesApproved, 2, '.', '');
		
		$list .= "<tr class=total >";
		$list .= "<td>Total Launch Plan</td>";
		$list .= "<td class=currency >&nbsp;</td>";
		$list .= "<td class=number >$totalQuantities</td>";
		$list .= "<td class=number >$totalQuantitiesApproved</td>";
		$list .= "</tr>";
		$list .= "</tbody></table>";

	} else {
		$list = "<div class=emptybox >".$translate->empty_costs."</div>";
	}		
?>
<style type="text/css">
	
	.costs { 
		display: block;
		width: 800px; 
	}
	
	.listing thead td {
		color: gray;
	}
	
	.listing td {
		border: 0;
	}
	
	.listing .group td {
		font-size: 13px;
	}
	
	.listing .subtotal td {
		color: gray;
		border-top: 1px solid #ddd;
		background-color: #f3f3f3;
		font-size: 12px;
		padding: 5px 10px;
	}
	
	.listing tr:first-child td {
		padding-top: 10px;	
	}
	
	.listing  .number {
		text-align: right;
		width: 20%;
	}
	
	.listing .currency {
		white-space: nowrap;
	}

	.listing .total td {
		padding-top: 40px;
		font-weight: 700;
		font-size: 18px;
	}
	
</style>
<div class=costs >
	<div class="-box">
		<div class=-content ><?php echo $list; ?></div>
	</div>
	<div class='actions'>
	<?php 
		echo ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => $translate->back
		));
	?>
	</div>
</div>