<script type="text/javascript">
	
	$(document).ready(function () {
	      
	    var $container = $("#logistics");
	    
	    $container.handsontable({
	        data: [
	            { 
	            	name: {
	            		first: 'Admir', 
	            		last: 'serifi'
	            	}, 
	            	address: {
	            		street: '513 Broadway',
	            		state: 'New York',
	            		zip: '10013'
	            	} 
	        	}
	        	/*
	            model('Chico', 'Marx', '513 Broadway', 'New York', '10013'),
	            model('Harpo', 'Marx', '3913 Michigan Ave', 'Chicago', '40123'),
	            model('Groucho', 'Marx', '334 Park Place', 'Atlantic City', '31513'),
	            model( 'Zeppo', 'Marx', '3135 Sunset Blvd', 'Los Angeles', '14123')
	            */
	        ],
	        startRows: 4,
	        startCols: 5,
	        colHeaders: ['First', 'Last', 'Street', 'State', 'Zip'],
	        rowaHeaders: false,
	        manualColumnResize: true,
	        columns: [
	            {data: 'name.first'},
	            {data: 'name.last'},
	            {data: 'address.street'},
	            {data: 'address.state'},
	            {data: 'address.zip'}
	        ],
	        afterRender: function () {
	             $container.find('thead').find('tr').before(
	                 '<tr id="header-grouping"><th colspan="2">Name</th>' + 
	                 '<th colspan="3">Address</th></tr>');
	        },
	        beforeRender: function() {
	            $('#header-grouping').remove();
	        }, 
	        modifyColWidth: function () {
	            $('#header-grouping').remove();
	        }
	      });
	});

</script>
<style type="text/css">
	
	#logistics {
	    font-size: 13px;
	}

	
	

</style>
<div id="logistics"></div>