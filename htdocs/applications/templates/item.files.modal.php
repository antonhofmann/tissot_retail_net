<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$item = new Item();
	$item->read($_REQUEST['id']);

	$pictures = array();
	$files = array();

	if ($item->id) {

		$model = new Model(Connector::DB_CORE);

		$result = $model->query("
			SELECT *
			FROM item_files
			INNER JOIN file_types ON item_file_type = file_type_id
			WHERE item_file_item = $item->id
		")->fetchAll();

		if ($result) {

			foreach ($result as $row) {
				
				$file = $row['item_file_id'];

				if (in_array($row['item_file_type'],array(1,2))) {
					$pictures[$file]['item_file_title'] = $row['item_file_title']; 
					$pictures[$file]['item_file_description'] = $row['item_file_description']; 
					$pictures[$file]['item_file_path'] = $row['item_file_path']; 
				} else {
					$files[$file]['item_file_title'] = "<a href='{$row[item_file_path]}' target='_blank' >{$row[item_file_title]}</a>";
					$files[$file]['item_file_type'] = $row['file_type_name'];
					$files[$file]['item_file_description'] = $row['item_file_description'];
				}
			}
		}
	}
?>
<!doctype html>
<!--[if IE 7]>    <html class="ie ie7" lang="en"> <![endif]-->
<!--[if gt IE 7]>    <html class="ie" lang="en"> <![endif]-->
<!--[if !IE]><!--><html lang="en"> <!--<![endif]--> 
<head>
	<title>Item Files - Retail Net</title>
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link href="/public/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="/public/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/x-icon" />
	
	<script src="/public/scripts/jquery/1.7.2.js" type="text/javascript"></script>
	<script src="/public/scripts/jquery/migrate.min.js" type="text/javascript"></script>
	<script src="/public/scripts/jquery/jquery.ui.1.10.4.min.js"></script>
	<link type="text/css" href="/public/scripts/jquery/jquery.ui.1.10.4.min.css" rel="stylesheet" />
	
	<link rel="stylesheet" type="text/css" href="/public/scripts/fancybox/fancybox.css?v=2.1.5" />
	<script type="text/javascript" src="/public/scripts/fancybox/fancybox.js?v=2.1.5"></script>
	
	<script type="text/javascript" src="/public/scripts/jgrowl/jgrowl.js"></script>
	<link rel="stylesheet" type="text/css" href="/public/scripts/jgrowl/jgrowl.css" />
	
	<link href="/public/scripts/adomodal/adomodal.css" rel="stylesheet" type="text/css" />
	<script src="/public/scripts/adomodal/adomodal.js" type="text/javascript"></script> 
	<script type="text/javascript" src="/public/scripts/jquery.actual.min.js"></script>
	<link rel="stylesheet" href="/public/css/spinners.css">
	
	<script type="text/javascript" src="/public/scripts/message.js"></script>
	<script type="text/javascript" src="/public/scripts/retailnet.js"></script>

	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/modal.css" />
	<link rel="stylesheet" href="/public/scripts/bootstrap/css/bootstrap.compiled.css">
	<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
	
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/modal.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/reset.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/layout.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/table.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/form.css" />
	<link rel="stylesheet" type="text/css" href="/public/css/gui.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/gui.css" />
	
	<link rel="stylesheet" href="/public/scripts/fileuploader/css/style.css">
	<link rel="stylesheet" href="/public/scripts/fileuploader/css/jquery.fileupload.css">
	<link rel="stylesheet" href="/public/scripts/fileuploader/css/jquery.fileupload-ui.css">

	<style type="text/css">
	
		body,
		body.adomat {
			background: #eeeeee !important;
			min-width: 400px !important;
		}
		
		.modalbox-container {
			display: block;
		}

		.modalbox-container {
			display: block;
			height: 540px;
		}
		
		.-element.files {
			min-height: 90px !important;
		}
		
		.ideabox .idea-content textarea {
			height: 160px !important;
		}

		.item-pictures {
			margin-bottom: 20px;
		}

		.image-title {
			disaply: block;
			margin: 20px 0;
			font-size: 16px;
		}

		.image-title span {
			display: block;
			margin-top: 5px;
			font-size: 13px;
			color: gray;
		}

	
		.image-responsive {
			max-width: 760px;
		}


		hr {
			margin: 10px 0 ;
		}
		
	</style>

	<script type="text/javascript">
		$(document).ready(function() {

			// close modal screen
			$('.close', $('#modalbox')).click(function(e) {
				
				e.preventDefault();
				e.stopImmediatePropagation();
				
				parent.retailnet.modal.close();
				
				return false;
			});
		});
	</script>
	
</head>
<body class="adomat">

	<div class="modalbox-container">
		<div id="modalbox" class="adomat modalbox">
			<div class="modalbox-header">
				<div class="title">Item Files</div> 
				<div class="subtitle"><?php echo $item->code, ', ', $item->name ?></div>
				<div class="modal-infotex">CHF <?php echo $item->price, ' | ', $item->description ?></div>
			</div>
			<div class="modalbox-content-container">
				<div class="modalbox-content">
					<div class="item-pictures">
					<?php
						if ($pictures) {
							foreach ($pictures as $i => $row) {
								echo "<div class=image-title >{$row[item_file_title]}<span>{$row[item_file_description]}</span></div>";
								echo "<img src='{$row[item_file_path]}' class='image-responsive' >";
								echo "<hr>";
							}
						}
					?>
					</div>
					<div class="item-files">
					<?php
						if ($files) {
							$table = new Table();
							$table->datagrid = $files;
							$table->item_file_title(Table::ATTRIBUTE_NOWRAP, "width=20%");
							$table->item_file_type(Table::ATTRIBUTE_NOWRAP, "width=20%");
							$table->item_file_description();
							
							echo "<h5>Attached Files</h5>";
							echo $table->render();
						}
					?>
					</div>
				</div>
			</div>
			<div class="modalbox-footer">
				<div class="modalbox-actions">
					<a class="button close"> 
						<span class="icon cancel"></span>
						<span class="label">Close</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>