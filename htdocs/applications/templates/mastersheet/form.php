<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'action' => $buttons['save'],
		'method' => 'post',
		'class' => 'mastersheet validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->mastersheet_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mastersheet_year(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		Validata::PARAM_YEAR,
		'class=numeric'
	);
	
	$form->mastersheet_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED
	);
	
	$form->fieldset('master_sheet', array(
		'mastersheet_year',
		'mastersheet_name'
	));

	if ($data['application']=='lps') {

		$form->mastersheet_pos_date(
			Form::TYPE_TEXT, 
			Form::TOOLTIP_DATE,
			Validata::PARAM_REQUIRED,
			Validata::PARAM_DATE,
			"class=datepicker required"
		);

		$form->mastersheet_phase_out_date(
			Form::TYPE_TEXT, 
			Form::TOOLTIP_DATE,
			Validata::PARAM_REQUIRED,
			Validata::PARAM_DATE,
			"class=datepicker required"
		);

		$form->mastersheet_week_number_first(
			Form::TYPE_SELECT,
			Validata::PARAM_REQUIRED,
			Validata::PARAM_INTEGER,
			'class=numeric'
		);

		$form->mastersheet_weeks(
			Form::TYPE_TEXT,
			Validata::PARAM_REQUIRED,
			Validata::PARAM_INTEGER,
			Validata::max(52),
			'class=numeric'
		);

		$form->mastersheet_estimate_month(
			Form::TYPE_TEXT,
			Validata::PARAM_REQUIRED,
			Validata::PARAM_INTEGER,
			Validata::max(12),
			'class=numeric'
		);

		$form->fieldset('LPS', array(
			'mastersheet_pos_date',
			'mastersheet_phase_out_date',
			'mastersheet_week_number_first',
			'mastersheet_weeks',
			'mastersheet_estimate_month'
		));
	}
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	if ($buttons['reset_exchange_data']) {
		$form->button(ui::button(array(
			'id' => 'reset_exchange_data',
			'class' => 'dialog',
			'icon' => 'reload',
			'href' => $buttons['reset_exchange_data'],
			'label' => $translate->reset_exchange_data
		)));
	}
	
	if ($buttons['archive']) {
		$form->button(ui::button(array(
			'id' => 'mastersheet_archive',
			'class' => 'dialog',
			'icon' => 'lock',
			'href' => $buttons['archive'],
			'label' => $translate->put_to_archive
		)));
	}
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	if ($buttons['detail-list']) {
		$form->button(ui::button(array(
			'id' => 'btnDetailList',
			'icon' => 'print',
			'label' => $printGroupCaption,
			'href' => '#detailListDialog',
			'class' => 'print-options-modal'
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<?php 
	
	echo $form; 
	
	if ($buttons['delete']) {
		echo ui::dialogbox(array(
			'id' => 'delete_dialog',
			'title' => $translate->delete,
			'content' => $translate->dialog_delete_record,
			'buttons' => array(
				'cancel' => array(
					'icon' => 'cancel',
					'label' => $translate->cancel,
					'class' => 'cancel'
				),
				'apply' => array(
					'icon' => 'apply',
					'label' => $translate->yes,
					'class' => 'apply'
				),
			)
		));
	}

	if ($buttons['archive']) {
		echo ui::dialogbox(array(
			'id' => 'mastersheet_archive_dialog',
			'title' => $translate->put_to_archive,
			'content' => $translate->dialog_mastersheet_put_to_archive(array('mastersheet' => $data['mps_mastersheet_name'])),
			'buttons' => array(
				'cancelArchive' => array(
					'icon' => 'cancel',
					'label' => $translate->cancel,
					'class' => 'cancel'
				),
				'applyArchive' => array(
					'icon' => 'apply',
					'label' => $translate->yes,
					'class' => 'apply'
				),
			)
		));
	}

	if ($buttons['reset_exchange_data']) {
		echo ui::dialogbox(array(
			'id' => 'reset_exchange_data_dialog',
			'title' => $translate->reset_exchange_data,
			'content' => $translate->dialog_reset_exchange_data,
			'buttons' => array(
				'cancelReset' => array(
					'icon' => 'cancel',
					'label' => $translate->cancel,
					'class' => 'cancel'
				),
				'applyReset' => array(
					'icon' => 'apply',
					'label' => $translate->yes,
					'class' => 'apply'
				),
			)
		));
	}
?>
<style type="text/css">
	
	.modalbox-content p span {
		display: inline-block;
		padding-left: 10px;
		font-size: 12px;
	}

	#detailListDialog,
	#splittingDialog {
		width: 500px;
		min-height: 500px;
	}
	
	#splittingDialog .content-caption {
		display: block;
		font-size: 13px;
		padding: 10px 0;
	}
	
	.modalbox-content .error {
		display: block;
		color: red;
		font-weight: 300;
	}
	
	.modalbox-content span.error {
		padding-left: 40px;
	}

</style>
<?php if ($printGroups && $printOptions) : ?>
<!-- Splitting Box: Print dialog form splitting list -->
<div class='modalbox-container'>
	<div id="detailListDialog" class="modalbox modal print-box">
		<div class="modalbox-header" >
			<div class='title'>Select Export Options</div>
		</div>
		<div class="modalbox-content-container" >
			<div class="modalbox-content" >
				<form class="spliting-list" method="post" action="<?php echo $buttons['detail-list'] ?>" >
					<div class="content-caption" >Product Groups</div>
					<div class="modal-fieldset groups planningtypes-placeholder">
						<?php 
							foreach ($printGroups as $group => $name) {
								echo "
									<p>
										<input type=\"checkbox\" name=\"group[$group]\" value=\"1\" /> 
										<span>$name</span>
									</p>
								";
							}
						?>
					</div>
					<br /><br />
					<div class="content-caption" >Options</div>
					<div class="modal-fieldset options" >
						<?php
							foreach ($printOptions as $name => $caption) {
								echo "
									<p>
										<input type=\"checkbox\" name=\"$name\" value=\"1\" /> 
										<span>$caption</span>
									</p>
								";
							}
						?>	
					</div>
				</form>
			</div>
		</div>
		<div class="modalbox-footer" >
			<div class="modalbox-actions" >
				<span class='button cancel'>
					<span class="icon cancel"></span>
					<span class="label"><?php echo $translate->cancel ?></span>
				</span>
				<span class='button apply'>
					<span class="icon print"></span>
					<span class="label"><?php echo $translate->print ?></span>
				</span>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>