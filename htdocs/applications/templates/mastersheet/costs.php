<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();

	if ($datagrid) { 
		
		$list  = "<table class=listing ><thead>";
		$list .= "<tr>";
		$list .= "<td colspan=2 >&nbsp;</td>";
		$list .= "<td class=number >Quantities</td>";
		$list .= "<td class=number >Approved Quantities</td>";
		$list .= "</tr>";
		$list .= "</thead><tbody>";
		
		// datagrid
		foreach ($datagrid as $grop => $row) {
			
			$totalGroupQuantity = 0;
			$totalGroupQuantityApproved = 0;
			$totalSubgroupQuantityEstimate = 0;
			
			$list .= "<tr class=group ><td colspan=4 >{$row[caption]}</td></tr>";

			if ($row['subgroups']) {
				
				foreach ($row['subgroups'] as $subgroup => $subrow) {
					
					$totalSubgroupQuantity = 0;
					$totalSubgroupQuantityApproved = 0;
					
					// order sheet items
					if ($subrow['items']) {
						
						$totalCostQuantity = 0;
						$totalCostQuantityApproved = 0;
						
						foreach ($subrow['items'] as $key => $item) {
							$totalCostQuantity = $totalCostQuantity + $item['cost_quantity'];
							$totalCostQuantityApproved = $totalCostQuantityApproved + $item['cost_quantity_approved'];
						}
						
						$totalSubgroupQuantity = $totalSubgroupQuantity + $totalCostQuantity;
						$totalSubgroupQuantityApproved = $totalSubgroupQuantityApproved + $totalCostQuantityApproved;
					}
					
					// number format
					$totalSubgroupQuantity = number_format($totalSubgroupQuantity, 2, '.', '');
					$totalSubgroupQuantityApproved = number_format($totalSubgroupQuantityApproved, 2, '.', '');
					
					$list .= "<tr>";
					$list .= "<td class=subcategory >{$subrow[caption]}</td>";
					$list .= "<td class=currency >CHF</td>";
					$list .= "<td class=number >$totalSubgroupQuantity</td>";
					$list .= "<td class=number >$totalSubgroupQuantityApproved</td>";
					$list .= "</tr>";
					
					$totalGroupQuantity = $totalGroupQuantity + $totalSubgroupQuantity;
					$totalGroupQuantityApproved = $totalGroupQuantityApproved + $totalSubgroupQuantityApproved;
				}
			}
			elseif ($row['items']) {

				$totalCostQuantity = 0;
				$totalCostQuantityApproved = 0;
				$totalCostQuantityEstimate = 0;
				
				foreach ($row['items'] as $key => $item) {
					$totalCostQuantity = $totalCostQuantity + $item['cost_quantity'];
					$totalCostQuantityApproved = $totalCostQuantityApproved + $item['cost_quantity_approved'];
					$totalCostQuantityEstimate = $totalCostQuantityEstimate + $item['cost_quantity_estimate'];
				}

				// number format
				$totalCostQuantity = number_format($totalCostQuantity, 2, '.', '');
				$totalCostQuantityApproved = number_format($totalCostQuantityApproved, 2, '.', '');
				$totalCostQuantityEstimate = number_format($totalCostQuantityEstimate, 2, '.', '');
				
				$list .= "<tr>";
				$list .= "<td class=subcategory >{$row[caption]}</td>";
				$list .= "<td class=currency >CHF</td>";
				$list .= "<td class=number >$totalCostQuantity</td>";
				$list .= "<td class=number >$totalCostQuantityApproved</td>";
				$list .= "</tr>";

				$totalGroupQuantity = $totalGroupQuantity + $totalCostQuantity;
				$totalGroupQuantityApproved = $totalGroupQuantityApproved + $totalCostQuantityApproved;
				$totalGroupQuantityEstimate = $totalGroupQuantityEstimate + $totalCostQuantityEstimate;
			}
			
			// number format
			$totalGroupQuantity = number_format($totalGroupQuantity, 2, '.', '');
			$totalGroupQuantityApproved = number_format($totalGroupQuantityApproved, 2, '.', '');
			$totalGroupQuantityEstimate = number_format($totalGroupQuantityEstimate, 2, '.', '');
					
			$list .= "<tr class=subtotal >";
			$list .= "<td class=group >$translate->total {$row[caption]}</td>";
			$list .= "<td class=currency >CHF</td>";
			$list .= "<td class=number >$totalGroupQuantity</td>";
			$list .= "<td class=number >$totalGroupQuantityApproved</td>";
			$list .= "</tr>";
			
			$totalSheetQuantity = $totalSheetQuantity + $totalGroupQuantity;
			$totalSheetQuantityApproved = $totalSheetQuantityApproved + $totalGroupQuantityApproved;
			$totalSheetQuantityEstimate = $totalSheetQuantityEstimate + $totalSubgroupQuantityEstimate;
		}
		
		// number format
		$totalSheetQuantity = number_format($totalSheetQuantity, 2, '.', '');
		$totalSheetQuantityApproved = number_format($totalSheetQuantityApproved, 2, '.', '');
		$totalSheetQuantityEstimate = number_format($totalSheetQuantityEstimate, 2, '.', '');
		
		$list .= "<tr class=total >";
		$list .= "<td>Total Master Sheet</td>";
		$list .= "<td class=currency >CHF</td>";
		$list .= "<td class=number >$totalSheetQuantity</td>";
		$list .= "<td class=number >$totalSheetQuantityApproved</td>";
		$list .= "</tr>";


		$list .= "</tbody></table>";
	}
	else  {
		$list = "<div class=emptybox >$translate->empty_costs</div>";
	}
	
?>
<style type="text/css">
	
	.costs { 
		display: block;
		width: 800px; 
	}
	
	.listing thead td {
		color: gray;
	}
	
	.listing td {
		border: 0;
	}
	
	.listing .group td {
		padding-top: 30px;
		font-size: 14px;
		font-weight: 700;
		border-bottom: 1px solid silver;
	}
	
	.listing .subtotal td {
		color: gray;
		border-top: 1px solid #ddd;
		font-size: 12px;
		padding: 5px 10px;
	}
	
	.listing tr:first-child td {
		padding-top: 10px;	
	}
	
	.listing  .subgroup {
		padding-left: 30px;
	}
	
	.listing  .number {
		text-align: right;
		width: 20%;
	}
	
	.listing .currency {
		text-align: right;
	}

	.listing .total td {
		padding-top: 40px;
		font-weight: 700;
		font-size: 18px;
	}
	
</style>
<div class=costs >
	<div class="-box">
		<div class=-content ><?php echo $list; ?></div>
	</div>
	<div class='actions'>
	<?php 
	
		if ($buttons['back']) {
			echo ui::button(array(
				'id' => 'back',
				'icon' => 'back',
				'href' => $buttons['back'],
				'label' => $translate->back
			));
		}
		
		if ($buttons['print']) {
			echo ui::button(array(
				'id' => 'print',
				'icon' => 'print',
				'href' => $buttons['print'],
				'label' => $translate->print
			));
		}
	?>
	</div>
</div>