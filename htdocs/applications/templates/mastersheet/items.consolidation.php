<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$request = request::instance();
$translate = Translate::instance();
?>
<style type="text/css">

	#version_name {
		width: 350px !important;
		border: 1px solid silver;
		padding: 5px;
		color: black;
		font-size: .9em;
	}
	
	.modalbox-content p span {
		display: inline-block;
		padding-left: 10px;
		font-size: 12px;
	}

	#splittingDialog {
		width: 500px;
		min-height: 500px;
	}
	
	#splittingDialog .content-caption {
		display: block;
		font-size: 13px;
		padding: 10px 0;
	}
	
	.modalbox-content .error {
		display: block;
		color: red;
		font-weight: 300;
	}
	
	.modalbox-content span.error {
		padding-left: 40px;
	}

	.selection {
		display: none !important;
	}

	.actions {
		margin: 0;
		padding: 0;
	}

	
</style>
<form id="filters">
	<label class="dropdown dropdown-primary"><?php echo $dropdowns['launchplans'] ?></label>
</form>
<div id="spreadsheet"></div>	
<div class='actions align-left'>
	<?php 
		
		echo ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => $translate->back
		));

		if ($buttons['print']) {
			echo ui::button(array(
				'id' => 'btnprint',
				'icon' => 'print',
				'label' => $translate->print,
				'href' => $buttons['print']
			));
		}
		
		if ($buttons['splitting']) {
			echo ui::button(array(
				'id' => 'btnSplitting',
				'icon' => 'print',
				'label' => 'Detail List',
				'href' => $buttons['splitting']
			));
		}
		
		if ($buttons['version']) {
			echo ui::button(array(
				'id' => 'btnVersion',
				'icon' => 'add',
				'href' => $buttons['version'],
				'label' => $translate->create_version
			));
		}
		
		if ($buttons['consolidate']) {
			echo ui::button(array(
				'id' => 'consolidate',
				'icon' => 'save',
				'label' => $translate->consolidate,
				'href' => $buttons['consolidate']
			));
		}
		
		if ($buttons['unconsolidate']) {
			echo ui::button(array(
				'id' => 'unconsolidate',
				'icon' => 'save',
				'label' => $translate->mastersheet_unconsolidate,
				'href' => $buttons['unconsolidate']
			));
		}
	?>
</div>
<?php 
	
	echo $request->form();
	
	if ($buttons['export']) {
		echo $export_dialog;
	}
	
	if ($buttons['version']) {

		echo ui::dialogbox(array(
			'id' => 'version_dialog',
			'title' => $translate->create_version,
			'content' => "<input type=text class=silver name=version_name id=version_name placeholder='Version Title' />",
			'buttons' => array(
				'versionCancel' => array(
					'icon' => 'cancel',
					'class' => 'cancel',
					'label' => $translate->cancel
				),
				'versionApply' => array(
					'icon' => 'apply',
					'class' => 'disabled',
					'label' => $translate->create
				)
			)
		));
	}
?>
<!-- Splitting Box: Print dialog form splitting list -->
<div class='modalbox-container'>
	<div id="splittingDialog" class="modalbox modal">
		<div class="modalbox-header" >
			<div class='title'>Select Export Options</div>
		</div>
		<div class="modalbox-content-container" >
			<div class="modalbox-content" >
				<form class="spliting-list" method="post" action="<?php echo $buttons['splitting'] ?>" >
					<div class="content-caption" >Product Groups</div>
					<div class="modal-fieldset groups planningtypes-placeholder"></div>
					<div class="content-caption" >Options</div>
					<div class="modal-fieldset options" >
						<p>
							<input type="checkbox" name='estimate_quantity' value='1' /> 
							<span>Estimate Quantity</span>
						</p>
						<p>
							<input type="checkbox" name='desired_quantity' value='1' /> 
							<span>Total Launch Quantity</span>
						</p>
						<p>
							<input type="checkbox" name='desired_quantity_total' value='1' /> 
							<span>Total Launch Cost</span>
						</p>
						<p>
							<input type="checkbox" name='approved_quantity' value='1' /> 
							<span>Total Approved Quantity</span>
						</p>
						<p>
							<input type="checkbox" name='approved_quantity_total' value='1' /> 
							<span>Total Approved Cost</span>
						</p>
					</div>
				</form>
			</div>
		</div>
		<div class="modalbox-footer" >
			<div class="modalbox-actions" >
				<span class='button cancel'>
					<span class="icon cancel"></span>
					<span class="label"><?php echo $translate->cancel ?></span>
				</span>
				<span id='submitSplittingList' class='button'>
					<span class="icon print"></span>
					<span class="label"><?php echo $translate->print ?></span>
				</span>
			</div>
		</div>
	</div>
</div>