<?php 
	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();

	$LPS = in_array($request->application, array('lps')) ? true : false;
?>
<script type="text/javascript">
$(document).ready(function() {

	var app = $('#application').val();
	app = app.length > 3 ? app.substring(3) : app;

	// spiner instance	
	retailnet.loader.init();

	// button: splitting list dialog
	$('#btnSplitting').on('click', function(e) {

		e.preventDefault();

		var dialog = $('#splitting-dialog-'+app); 

		// empty planning types place holder
		$('.planningtypes-placeholder', dialog).empty();

		var groups = $('h5');
		
		groups.each(function(i,el) {

			// insert input box 
			$('.planningtypes-placeholder', dialog).append('\
				<p><input type=checkbox name=group['+$(this).data('id')+'] value=1 />\
				<span>'+$(this).text()+'</span></p>\
			');
		});

		// show modal box
		retailnet.modal.show('#splitting-dialog-'+app, {
			autoSize	: true,
			closeClick	: false,
			closeBtn	: false,
			fitToView   : true
		});

		return false;
	});

	// submit splitting list
	$('.submit-splitting').on('click', function(e) {

		e.preventDefault();
		retailnet.loader.hide();
		retailnet.notification.hide();

		var dialog = $(this).closest('.splitting-dialog');
		var selectedGroups = $('.modal-fieldset.groups input:checked', dialog).length ;
		var selectedOptions = $('.modal-fieldset.options input:checked', dialog).length;
		
		if (selectedGroups && selectedOptions) {
			$('form', dialog).submit();
			$('input:checkbox', dialog).attr('checked', false);
			retailnet.modal.close();
		} else {
			retailnet.notification.error('Please, select at least one planning type and one print option.');
		}

		return false;
	});

	// close modal screen
	$('.cancel').on('click', function(e) {
		e.preventDefault();
		retailnet.modal.close();
		retailnet.notification.hide();
		return false;
	});	

	// button: create version
	$('#delete').on('click', function(e) {
		e.preventDefault();
		retailnet.modal.dialog('#delete_dialog');
		return false;
	});
});
</script>
<style type="text/css">

	.itemslist {
		width: 1200px;
	}

	h6 { 
		display: block; 
		margin-bottom: 5px; 
		font-size: .8em;
		font-weight: 500;
	}
	
	h5 { 
		display: block;
		margin: 40px 0 20px; 
		font-size: 1em;
		font-variant: small-caps;
		border-bottom: 1px solid silver;
		color: gray;
	}

	table { 
		margin-top: 20px; 
		border-style: solid; 
		border-color: silver; 
	}
	
	.totoal-collection {
		display: block;
		width: 100%;
		font-size: .75em;
		text-align: right;
		padding-top: 5px;
	}
	
	.totoal-collection b {
		padding-left: 5px;
	}
	
	.modalbox-content p span {
		display: inline-block;
		padding-left: 10px;
		font-size: 12px;
	}

	.splitting-dialog {
		width: 500px;
		min-height: 540px;
	}
	
	.splitting-dialog .content-caption {
		display: block;
		font-size: 13px;
		padding: 10px 0;
	}
	
	.modalbox-content .error {
		display: block;
		color: red;
		font-weight: 300;
	}
	
	.modalbox-content span.error {
		padding-left: 40px;
	}

	table.default tfoot td {
		padding: 10px !important;
	}

	table.default tfoot tr td:last-child {
		text-align: right;
	}

</style>
<div class="itemslist">
<?php 
	
	if ($datagrid) {
		
		foreach ($datagrid as $group => $row) {

			$totalGroup = 0;

			$list .= "<h5 data-id=$group >{$row[caption]}</h5>";

			if ($row['subgroups']) {

				foreach ($row['subgroups'] as $subgroup => $subrow) {

					$list .= "<h6 data=$key >{$subrow[caption]}</h6>";

					$totalprice = 0;
						
					$table = new Table(array('id' => "$key-$subkey"));
					$table->datagrid = $subrow['items'];
					$table->dataloader($dataloader);

					$table->collection_code(
						Table::ATTRIBUTE_NOWRAP,
						'width=10%'
					);

					$table->reference_code(
						Table::ATTRIBUTE_NOWRAP,
						'width=10%'
					);

					$table->reference_name();

					if (!$LPS) {
						$table->mps_material_hsc(
							Table::ATTRIBUTE_NOWRAP,
							'width=5%'
						);
					}

					$table->setof(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%',
						'caption=Set of'
					);

					$table->price(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%',
						'caption=Price'
					);

					$table->currency_symbol(
						Table::ATTRIBUTE_NOWRAP,
						'width=20px'
					);

					$table->quantity(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						Table::PARAM_GET_FROM_LOADER,
						'width=5%'
					);

					$table->quantity_approved(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%',
						'caption=Approved'
					);
				
					$table->total_cost(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);

					// total collection price
					foreach ($subrow['items'] as $key => $array) {
						$totalprice = $totalprice+$array['total_cost'];
					}

					$totalprice = number_format($totalprice, 4, '.', '');

					$table->footer(array(
						'content' => "Total cost for \"{$subrow[caption]}\" ",
						'attributes' => array('colspan' => 8)
					));

					$table->footer(array('content' => $totalprice));
						
					$list .= $table->render(array('order'=>true));
				}

			}
			elseif ($row['items']) {

				$totalprice = 0;
					
				$table = new Table();
				$table->datagrid = $row['items'];

				$table->collection_code(
					Table::ATTRIBUTE_NOWRAP,
					'width=10%'
				);

				$table->reference_code(
					Table::ATTRIBUTE_NOWRAP,
					'width=10%'
				);

				$table->reference_name();

				$table->price(
					Table::ATTRIBUTE_ALIGN_RIGHT,
					Table::ATTRIBUTE_NOWRAP,
					'width=5%',
					'caption=Price'
				);

				$table->currency_symbol(
					Table::ATTRIBUTE_NOWRAP,
					'width=20px'
				);

				$table->quantity_estimate(
					Table::ATTRIBUTE_ALIGN_RIGHT,
					Table::ATTRIBUTE_NOWRAP,
					Table::PARAM_GET_FROM_LOADER,
					'width=5%',
					'caption=Estimated'
				);

				$table->quantity(
					Table::ATTRIBUTE_ALIGN_RIGHT,
					Table::ATTRIBUTE_NOWRAP,
					Table::PARAM_GET_FROM_LOADER,
					'width=5%'
				);

				$table->quantity_approved(
					Table::ATTRIBUTE_ALIGN_RIGHT,
					Table::ATTRIBUTE_NOWRAP,
					'width=5%',
					'caption=Approved'
				);
			
				$table->total_cost(
					Table::ATTRIBUTE_ALIGN_RIGHT,
					Table::ATTRIBUTE_NOWRAP,
					'width=5%'
				);

				// total collection price
				foreach ($row['items'] as $key => $array) {
					$totalprice = $totalprice+$array['total_cost'];
				}

				$totalprice = number_format($totalprice, 4, '.', '');

				$table->footer(array(
					'content' => "Total cost for \"{$row[caption]}\" ",
					'attributes' => array('colspan' => 8)
				));

				$table->footer(array('content' => $totalprice));
					
				$list .= $table->render(array('order'=>true));
			}
		}
	}
	else {
		$list = html::emptybox($translate->empty_result);
	}
	
	echo $list;

?>
	<div class='actions'>
		<?php 
			
			if ($buttons['back']) {
				echo ui::button(array(
					'id' => 'back',
					'icon' => 'back',
					'href' => $buttons['back'],
					'label' => $translate->back
				));
			}

			if ($buttons['delete']) {
				echo ui::button(array(
					'id' => 'delete',
					'icon' => 'delete',
					'label' => $translate->delete,
					'href' => $buttons['delete']
				));
			}
			
			if ($buttons['print']) {
				echo ui::button(array(
					'id' => 'btnprint',
					'icon' => 'print',
					'label' => $translate->print,
					'href' => $buttons['print']
				));
			}
			
			if ($buttons['splitting']) {
				echo ui::button(array(
					'id' => 'btnSplitting',
					'icon' => 'print',
					'label' => $LPS ? 'Detail List' : $translate->splitting_list,
					'href' => $buttons['splitting']
				));
			}
		?>
	</div>
</div>
<?php 

	echo $request->form();
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'class' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'class' => 'submit',
				'href' => $buttons['delete'],
				'label' => $translate->yes
			),
		)
	));
?>
<!-- lps splitting list dialog -->
<div class='modalbox-container'>
	<div id="splitting-dialog-lps"  class="modalbox modal splitting-dialog">
		<div class="modalbox-header" >
			<div class='title'>Select Export Options</div>
		</div>
		<div class="modalbox-content-container" >
			<div class="modalbox-content" >
				<form class="spliting-list" method="post" action="<?php echo $buttons['splitting'] ?>" >
					<div class="content-caption" >Product Groups</div>
					<div class="modal-fieldset groups planningtypes-placeholder"></div>
					<div class="content-caption" >Options</div>
					<div class="modal-fieldset options" >
						<p>
							<input type="checkbox" name='estimate_quantity' value='1' /> 
							<span>Estimate Quantity</span>
						</p>
						<p>
							<input type="checkbox" name='desired_quantity' value='1' /> 
							<span>Desired Quantity by Customer</span>
						</p>
						<p>
							<input type="checkbox" name='desired_quantity_total' value='1' /> 
							<span>Total Cost of Desired Quantity</span>
						</p>
						<p>
							<input type="checkbox" name='approved_quantity' value='1' /> 
							<span>Approved Quantity</span>
						</p>
						<p>
							<input type="checkbox" name='approved_quantity_total' value='1' /> 
							<span>Total Cost of Approved Quantity</span>
						</p>
					</div>
				</form>
			</div>
		</div>
		<div class="modalbox-footer" >
			<div class="modalbox-actions" >
				<span class='button cancel'>
					<span class="icon cancel"></span>
					<span class="label">Cancel</span>
				</span>
				<span class='button submit-splitting'>
					<span class="icon print"></span>
					<span class="label">Print</span>
				</span>
			</div>
		</div>
	</div>
</div>

<!-- mps splitting dialog -->
<div class='modalbox-container'>
	<div id="splitting-dialog-mps" class="modalbox modal splitting-dialog">
		<div class="modalbox-header" >
			<div class='title'>Select Export Options</div>
		</div>
		<div class="modalbox-content-container" >
			<div class="modalbox-content" >
				<form class="spliting-list" method="post" action="<?php echo $buttons['splitting'] ?>" >
					<div class="content-caption" >Planning Types</div>
					<div class="modal-fieldset groups planningtypes-placeholder"></div>
					<div class="content-caption" >Options</div>
					<div class="modal-fieldset options" >
						<p>
							<input type=checkbox name='proposed_quantity' value='1' /> 
							<span>Proposed Quantity</span>
						</p>
						<p>
							<input type=checkbox name='desired_quantity' value='1' /> 
							<span>Desired Quantity by Customer</span>
						</p>
						<p>
							<input type=checkbox name='desired_quantity_total' value='1' /> 
							<span>Total Cost of Desired Quantity</span>
						</p>
						<p>
							<input type=checkbox name='approved_quantity' value='1' /> 
							<span>Approved Quantity</span>
						</p>
						<p>
							<input type=checkbox name='approved_quantity_total' value='1' /> 
							<span>Total Cost of Approved Quantity</span>
						</p>
						<p>
							<input type=checkbox name='ordered_quantity' value='1' /> 
							<span>Pre Ordered Quantity</span>
						</p>
						<p>
							<input type=checkbox name='ordered_quantity_total' value='1' /> 
							<span>Total Cost of Pre Ordered Quantity</span>
						</p>
					</div>
				</form>
			</div>
		</div>
		<div class="modalbox-footer" >
			<div class="modalbox-actions" >
				<span class='button cancel'>
					<span class="icon cancel"></span>
					<span class="label">Cancel</span>
				</span>
				<span class='button submit-splitting'>
					<span class="icon print"></span>
					<span class="label">Print</span>
				</span>
			</div>
		</div>
	</div>
</div>