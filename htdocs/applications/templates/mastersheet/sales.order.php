<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$request = request::instance();
	$translate = Translate::instance();
	
	$mastersheet = new Mastersheet($request->application);
	$mastersheet->read($id);
?>
<style type="text/css">


	#itemlist {
		width: 1200px;
	}

	#export-modal-box {
		width: 80%;
		max-width: 1600px;
	}

	h6 { 
		display: block; 
		margin-bottom: 5px; 
		font-size: 14px;
		font-weight: 500;
	}
	
	h5 { 
		display: block;
		margin: 40px 0 20px; 
		font-size: 18px;
		font-weight: 500;
		font-variant: small-caps;
		border-bottom: 1px solid silver;
		color: gray;
	}

	.ado-modal-body h5 {
		border: 0;
		margin-bottom: 5px;
		font-weight: 400;
		color: black;
	}

	.ado-modal-body h5:first-child {
		margin-top: 0;
	}

	.ado-modal-body table {  
		border-style: solid; 
		border-color: silver; 
		margin-bottom: 60px;
	}

	.ado-modal-body table:last-child {
		margin-bottom: 0;
	}

	table.listing th {
		padding: 5px 10px;
		font-size: 12px; 
		color: #22547A;
	}
	
	table.listing td {
		border-bottom: 1px solid silver;
	}

	table.listing th.quantity,
	table.listing th[class^="week_quantity"] {
	    text-align: right;
	}

	.errors p {
		color: #ff0000;
		font-size: 12px;
		padding-bottom: 5px;
	}
	/*
	.table-mps tr td:nth-child(n+4) {
		border: 0;
	}

	.ado-modal-input.no-block {
		width: auto;
	}
	*/
	
</style>
<form class='request'>
	<input type=hidden name=application value="<?php echo $request->application ?>" />
	<input type=hidden name=controller value="<?php echo $request->controller ?>" />
	<input type=hidden name=archived value="<?php echo $request->archived ?>" />
	<input type=hidden name=action value="<?php echo $request->action ?>" />
	<input type=hidden name=mastersheet value="<?php echo $id ?>" />
	<?php if ($errors) : ?><input type=hidden name=error value="1" /><?php endif; ?>
	<input type=hidden name=selected_items id=selected_items >
</form>
<?php if ($errors) : ?>
<div class="errors">
<?php 
	foreach ($errors as $error) {
		echo "<p>$error</p>";
	}
?>
</div>
<?php endif; ?>
<div id="itemlist"></div>	
<div class='actions list-actions'>
	<?php 
		
		echo ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => $translate->back
		));

		if ($buttons['print']) {
			echo ui::button(array(
				'id' => 'btnprint',
				'icon' => 'print',
				'label' => $translate->print,
				'href' => $buttons['print']
			));
		}
		
		if ($buttons['export']) {
			echo ui::button(array(
				'id' => 'export',
				'icon' => 'save',
				'label' => 'Export'
			));
		}
	?>
</div>

<!-- export dialog -->
<div id="export-modal-box" class="ado-modal ado-box">
	<div class="ado-modal-header">
		<div class="ado-title">Sales Orders Export</div>
		<div class="ado-subtitle"><?php echo $header ?></div>
	</div>
	<div class="ado-modal-body">
		<form id="sales-orders-export-list" action="<?php echo $buttons['export'] ?>" method="post" >
		
		</form>
	</div>
	<div class="ado-modal-footer" >
		<div class="ado-row ado-actions">
			<a class="button cancel ado-modal-close"  href="#" >
				<span class="icon cancel"></span>
				<span class="label"><?php echo $translate->cancel ?></span>
			</a>
			<a class="button submit-export" href="#" >
				<span class="icon reload"></span>
				<span class="label">Export</span>
			</a>
		</div>
	</div>
</div>