<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<style type="text/css">
	
	.wrapper { 
		width: 1000px; 
	}
	
	.wrapper .table-toolbox { 
		max-width: 960px; 
	}
	
	h6 { 
		display:block; 
		margin-bottom: 5px; 
		font-size: .8em;
		min-height: 20px;
		line-height: 20px;
	}
	
	h5 { 
		display:block;
		margin: 40px 0 30px; 
		font-size: 1.2em;
		font-weight: 500;
		font-variant: small-caps;
		border-bottom: 1px solid silver;
		color: gray;
	}

	table { 
		margin-bottom: 30px; 
	}
	
	#popover {
		min-width: 200px;
	}
	
	#popover ul span {
		display: block;
		margin: 10px 0;
		text-align: left;
	}
	
	.delivery {
		float: right;
		height: 20px;
		line-height: 20px;
		font-size: 12px;
		font-weight: 400;
		color: #444;
		text-align: right;
	}
	
	.delivery input {
		width: 80px;
		height: 16px;
		font-size: 11px;
		color: black;
		border: 1px solid silver;
		padding: 1px;
		outline: none;
		text-align: right;
	}
	
	.delivery input.error {
		border-color: red;
	}

	.input-comment {
		width: 200px;
	}

	.input-comment[disabled=disabled] {
		border-color: transparent !important;
		background-color: transparent; !important;
	}
	
</style>
<?php 

	if ($buttons['add'] || $buttons['print']) {

		if ($buttons['add'] && $request->isField('editabled')) {
			
			$toolbox = ui::button(array(
				'id' => 'add',
				'icon' => 'add',
				'label' => $translate->add_new,
				'data' => '#popover'
			));

			// first chained dropdown
			$dropdown = ui::dropdown($dataloader['group'], array(
				'id' => 'group',
				'name' => 'group',
				'caption' => $translate->all_collections
			));

			$popover = "
				<div id=popover >
					<form class=default >
					<ul>
						<li>
							<span>$dropdown</span>
							<span><select id=subgroup name=subgroup ></select></span>
						</li>
					</ul>
					</form>
				</div>
			";
		}
		
		if ($buttons['print']) {
			$toolbox .= ui::button(array(
				'id' => 'print',
				'icon' => 'print',
				'href' => $buttons['print'],
				'label' => $translate->print
			));
		}
	}
?>
<div class='wrapper'>
	<?php 
	
		if ($toolbox) {
			echo "<div class=table-toolbox >$toolbox</div>";
		}
		
		echo $request->form();
	?>
	<div id="mastersheet_items"></div>
	<div class='actions'><?php 
	
		echo ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => $translate->back
		));
	
		echo $popover;
	?></div>
</div>