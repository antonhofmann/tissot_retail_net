<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$db = Connector::get();

	switch ($_REQUEST['section']) {
		
		case 'project-order-files':

			$sth = $db->prepare("
				SELECT DISTINCT
					order_file_id AS id, 
					order_file_title AS title,
					order_file_path AS file
				FROM order_files
				LEFT JOIN order_file_categories on order_file_category_id = order_file_category 
				LEFT JOIN users on user_id = order_file_owner 
				LEFT JOIN file_types on order_file_type = file_type_id
				WHERE order_file_order = ? AND order_file_category = ?
				ORDER BY order_files.date_created DESC
			");

			$sth->execute(array($_REQUEST['order'], $_REQUEST['category']));
			$result = $sth->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					
					if (!file_exists($_SERVER['DOCUMENT_ROOT'].$row['file'])) continue;

					if (check::image($row['file'])) {
						$_IMAGES[] = $row;
					}
				}
			}

			$_ENTITY = $_IMAGES ? $_REQUEST['order'] : null;

		break;
		
		case 'af_ln-files':

			$sth = $db->prepare("
				SELECT DISTINCT
					ln_basicdata_pix1 AS pix1, ln_basicdata_pix2 AS pix2, ln_basicdata_pix3 AS pix3
				FROM ln_basicdata
				LEFT JOIN projects on project_id = ln_basicdata_project
				WHERE ln_basicdata_version = 0 and project_order = ? 
			");

			$sth->execute(array($_REQUEST['order']));
			$result = $sth->fetchAll();

			if ($result) {
				foreach ($result as $row) {
					if (!file_exists($_SERVER['DOCUMENT_ROOT'].$row['pix1'])) continue;

					if (check::image($row['pix1'])) {
						$_IMAGES[] = array("id"=>1, "title"=>'Photo 1', "file"=>$row['pix1']);
					}

					if (!file_exists($_SERVER['DOCUMENT_ROOT'].$row['pix2'])) continue;

					if (check::image($row['pix2'])) {
						$_IMAGES[] = array("id"=>2, "title"=>'Photo 2', "file"=>$row['pix2']);
					}

					if (!file_exists($_SERVER['DOCUMENT_ROOT'].$row['pix3'])) continue;

					if (check::image($row['pix3'])) {
						$_IMAGES[] = array("id"=>3, "title"=>'Photo 3', "file"=>$row['pix3']);
					}
				}
			}

			$_ENTITY = $_IMAGES ? $_REQUEST['order'] : null;

		break;
	}

?>
<!doctype html>
<!--[if IE 7]>    <html class="ie ie7" lang="en"> <![endif]-->
<!--[if gt IE 7]>    <html class="ie" lang="en"> <![endif]-->
<!--[if !IE]><!--><html lang="en"> <!--<![endif]--> 
<head>
	<title>Image Gallery - Retail Net</title>
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link href="/public/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="/public/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/x-icon" />
	
	<script src="/public/scripts/jquery/1.9.1.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/reset.css" />

	<link href="/public/scripts/fotorama/fotorama.css" rel="stylesheet" type="text/css" />
	<script src="/public/scripts/fotorama/fotorama.js" type="text/javascript"></script>

	<style type="text/css">
		body {
			background: #000;
		}

	</style>

	<script type="text/javascript">
		$(document).ready(function() {

			var ID_ENTITY = $('#entity').val();

			if (!ID_ENTITY && parent) {
				
				parent.retailnet.notification.hide();
				
				parent.retailnet.notification.show("You don't have access to this page", { 
					live: 3000,
					sticky: false, 
					theme: 'error'
				});
				
				parent.retailnet.modal.hide();
			}

			// close modal screen
			$('.modal-close').click(function(e) {
				
				e.preventDefault();
				e.stopImmediatePropagation();
				
				if (parent && parent.retailnet) {
					parent.retailnet.modal.close();
				}
				
				return false;
			})

		})
	</script>
	
</head>
<body class="adomat">
	<input type="hidden" id="entity" name="id" value="<?php echo $_ENTITY ?>">
	<div id="gallery" class="fotorama" 
		data-width="100%"
    	data-height="100%"
    	data-allowfullscreen="native"
		data-arrows="true"
		data-keyboard="true"
		data-fit="cover"
		data-nav="thumbs" >
		<?php

			if ($_IMAGES) {
				foreach ($_IMAGES as $image) {
					echo "<img src=\"{$image[file]}\">";
				}
			}
		?>
	</div>
</body>
</html>