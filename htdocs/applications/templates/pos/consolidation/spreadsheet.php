<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();
	echo $request->form(); 
?>
<div class="actions">
	<form class="filters">
		<?php 
		
			// display buttons
			if ($buttons) {
				echo join(array_values($buttons));
			}
	
			if ($dataloader['periodes']) {
				echo html::select($dataloader['periodes'], array(
					'name' => 'periodes',
					'id' => 'periodes',
					'class' => 'submit selectbox',
					'value' => $period,
					'caption' => false
				));
			}
		
			if ($dataloader['regions']) {
				echo html::select($dataloader['regions'], array(
					'name' => 'regions',
					'id' => 'regions',
					'class' => 'submit selectbox',
					'value' => $region,
					'caption' => $translate->all_regions
				));
			}
			
			if ($dataloader['states']) {
				echo html::select($dataloader['states'], array(
					'name' => 'states',
					'id' => 'states',
					'class' => 'submit selectbox',
					'value' => $state,
					'caption' => 'Data Entry Status'
				));
			}
			
			if ($dataloader['clients']) {
				echo html::select($dataloader['clients'], array(
					'id' => 'clients',
					'name' => 'clients',
					'value' => $data['clients'],
					'class' => 'submit selectbox',
					'caption' => 'All Clients'
				));	
			}
			
			if ($dataloader['addresstypes']) {
				echo html::select($dataloader['addresstypes'], array(
					'id' => 'addresstypes',
					'name' => 'addresstypes',
					'value' => $data['addresstype'],
					'class' => 'submit selectbox',
					'caption' => 'All Client Types'
				));	
			}
	
		?>
	</form>
</div>
<div id="spreadsheet"></div>

<div id="printDialog"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" >Select Export Options</div>
	</div>
	<div class="ado-modal-body">
		<form method="post">
			<div class="content-caption" >Options</div>
			<div class="modal-fieldset options" >
				<p>
					<input type="checkbox" class='option' name='options[posindex]' value='POS' checked /> 
					<span>POS Index</span>
				</p>
				<p>
					<input type="checkbox" class='option' name='options[actual]' value='Actual' checked /> 
					<span>Actual</span>
				</p>
				<p>
					<input type="checkbox" class='option' name='options[delta]' value='Delta' checked /> 
					<span>Delta</span>
				</p>
			</div>
		</form>
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button submit-print" href="/applications/modules/pos/verification/consolidation/print.php" >
				<span class="icon print"></span>
				<span class="label" ><?php echo $translate->print ?></span>
			</a>
		</div>
	</div>
</div>

<?php
	echo ui::dialogbox(array(
		'id' => 'reset_verification_dialog',
		'title' => 'Confirm',
		'content' => "Reset all confirmed data?",
		'buttons' => array(
			'versionCancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'versionApply' => array(
				'icon' => 'apply',
				'class' => 'disabled apply',
				'label' => 'Reset'
			)
		)
	));
?>