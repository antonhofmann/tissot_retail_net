<?php 
	$request = request::instance();
	$translate = Translate::instance();
	$user = User::instance();
	echo $request->form();
?>
<style type="text/css">
	#list { width: 800px;}
	.table-toolbox {display: none;}
</style>
<div id="list"></div>
<div class='actions'><?php echo $buttons ? join($buttons) : null; ?></div>
<script type="text/javascript" src="/public/scripts/tinymce4/js/tinymce/tinymce.min.js"></script>

<!-- mail template modal -->
<div id="reminder-mail-template" class="ado-modal ado-box">
	<div class="ado-modal-header">
		<div class="ado-title"><?php echo $modal['title'] ?></div>
		<div class="ado-subtitle"><?php echo $modal['subtitle'] ?></div>
	</div>
	<div class="ado-modal-body">
		<form action="/applications/modules/pos/verification/consolidation/sendmail.php" method="post" >
			<input type="hidden" name="mail_template_id" id="mail_template_id" value="<?php echo $maildata['mail_template_id'] ?>" />
			<input type="hidden" name="mail_template_view_modal" id="mail_template_view_modal" value="<?php echo $maildata['mail_template_view_modal'] ?>" />
			<div class="ado-row">
				<input type=text name="mail_template_subject" id="mail_template_subject" class="ado-modal-input required" value="<?php echo $maildata['mail_template_subject'] ?>" />
			</div>
			<div class="ado-row">
				<textarea name="mail_template_text" id="mail_template_text" class="ado-modal-input required" ><?php echo $maildata['mail_template_text'] ?></textarea>
			</div>
		</form>
	</div>
	<div class="ado-modal-footer" >
		<div class="ado-row ado-actions">
			<a class='button cancel ado-modal-close'>
				<span class="icon cancel"></span>
				<span class="label"><?php echo $translate->cancel ?></span>
			</a>
			<a class='button test-mail' href="#">
				<span class="icon mail"></span>
				<span class="label">Test Mail</span>
			</a>
			<a class='button preview-mail' href="/applications/modules/pos/verification/consolidation/sendmail.preview.php">
				<span class="icon icon84"></span>
				<span class="label">Preview</span>
			</a>
			<a class='button sendmail'>
				<span class="icon mail"></span>
				<span class="label"><?php echo $translate->sendmail ?></span>
			</a>
		</div>
	</div>
</div>

<!-- preview mail modal -->
<div id="preview-mail-template" class="ado-modal ado-box">
	<div class="ado-modal-header">
		<span class="fa-stack ado-modal-close">
			<i class="fa fa-circle fa-stack-2x"></i>
			<i class="fa fa-times fa-stack-1x fa-inverse"></i>
		</span>
		<div class="ado-row ado-title mail-subject"></div>
		<div class="ado-row" >
			<span class="ado-label ado-width-30">To:</span>
			<span class="ado-label mail-address"></span>
		</div>			
		<div class="ado-row">
			<span class="ado-label ado-width-30">CC:</span>
			<span class="ado-label mail-cc-address"></span>
		</div>
		<div class="ado-notification-right-bottom">
			<span class="ado-label mail-date"></span>
		</div>
	</div>
	<div class="ado-modal-body ado-max-height-500 mail-content"></div>
	<div class="ado-modal-footer">
		<div class="mail-footer"></div>
	</div>
</div>

<!-- test mail modal dialog -->
<div id="test-mail-template"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" >Send Test E-mail an:</div>
	</div>
	<div class="ado-modal-body">
		<input type="text" name="test-mail-recipient" id="test-mail-recipient" class="ado-modal-input required" value="<?php echo $user->email ?>" placeholder="Recipient E-mail" />
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="/applications/modules/pos/verification/consolidation/sendmail.php" >
				<span class="icon mail"></span>
				<span class="label" ><?php echo $translate->send ?></span>
			</a>
		</div>
	</div>
</div>

<!-- reset verification dialog confirm -->
<div id="dialog-reset-verification"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" ><?php echo $translate->reset_verification ?></div>
	</div>
	<div class="ado-modal-body">
		<p><?php echo $translate->dialog_reset_verification; ?></p>
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->no ?></span>
			</a>
			<a class="button apply" href="#" >
				<span class="icon save"></span>
				<span class="label" ><?php echo $translate->yes ?></span>
			</a>
		</div>
	</div>
</div>
