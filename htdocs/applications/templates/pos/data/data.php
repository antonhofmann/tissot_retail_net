<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();
	
	// permission
	$permission_edit = user::permission(Pos_Verification::PERMISSION_EDIT_ALL);
	$permission_edit_limited = user::permission(Pos_Verification::PERMISSION_EDIT_LIMITED);
	
	// editabled form
	$canEdit = (!$data['posverification_confirm_date'] && ($permission_edit || $permission_edit_limited)) ? true : false;
?>
<?php 
	echo $request->form(array(
		'id' => $id
	)); 
?>
<div id="list">
	<div class="table-toolbox">
		<?php 
			if ($dataloader['periodes']) {
				echo ui::filterLabel($translate->period); 
				echo ui::dropdown($dataloader['periodes'], array(
					'name' => 'periodes',
					'id' => 'periodes',
					'class' => 'submit selectbox',
					'value' => $period,
					'caption' => false
				));
			}
		?> 
	</div>
	<?php 

		if ($datagrid) {
			
			foreach ($datagrid as $key => $row) {
				
				echo "<h5>".$row['caption']."</h5>";
				
				$table = new Table();
				$table->datagrid = $row['data'];
				$table->dataloader($dataloader);
				
				$table->mps_distchannel_name(
					Table::ATTRIBUTE_NOWRAP
				);
				
				$table->mps_distchannel_code(
					Table::ATTRIBUTE_NOWRAP,
					'width=120px'
				);

				$table->in_pos_index(
					Table::ATTRIBUTE_NOWRAP, 
					Table::PARAM_GET_FROM_LOADER,
					'width=90px',
					'class=number'
				);

				$table->posverification_data_actual_number(
					Table::ATTRIBUTE_NOWRAP,
					Table::PARAM_GET_FROM_LOADER,
					'width=90px',
					'class=number'
				);

				$table->delta(
					Table::ATTRIBUTE_NOWRAP,
					'width=90px',
					'class=number'
				);
				
				// dataloder
				if ($canEdit) {
					$table->posverification_data_actual_number(
						Table::DATA_TYPE_TEXTBOX
					);
				}
				
				// FOOTER
				$table->footer(array(
					'content' => 'Total '.$row['caption'],
					'attributes' => array(
						'colspan' => 2
					)
				));
				
				$table->footer(array(
					'attributes' => array(
						'class' => 'number total-cell in_pos_index',
						'data-target' => '.in_pos_index'
					)
				));
				
				$table->footer(array(
					'attributes' => array(
						'class' => 'number total-cell posverification_data_actual_number',
						'data-target' => '.posverification_data_actual_number'
					)
				));
				
				$table->footer(array(
					'attributes' => array(
						'class' => 'number total-cell delta',
						'data-target' => '.delta'
					)
				));
				
				echo $table->render();
			}
			
			
			echo "
				<table class=listing>
					<tr>
						<td valign='top' colspan='2' >Total Locations without distribution channel or with inactive distribution channel</td>
						<td valign='top' width='70px' class='number total-whithout-channels' >".$data['pos-whithout-channels']."</td>
						<td valign='top' width='70px' >&nbsp;</td>
						<td valign='top' width='70px' >&nbsp;</td>
					</tr>
					<tr>
						<td valign=top colspan=2 >Grand Total of all POS Locations</td>
						<td id='number_of_total_pos_locations' valign=top class='number total-pos-locations' >".$data['pos-total']."</td>
						<td id='posverification_data_actual_number' valign=top class='number grand-total-cell posverification_data_actual_number' data-target='.posverification_data_actual_number' >&nbsp;</td>
						<td valign=top class='number grand-total-cell delta' data-target='.delta' >&nbsp;</td>
					</tr>
				</table>
			";

			if ($data['submitted']) {
				echo "<p class=submitted>".$data['submitted']."</p>";
			}

			echo '<input type="hidden" id="pos_verification_deleted" value="' . $data['pos_verification_deleted'] . '" />';

			
		}
	?>
	<div class='actions'>
	<?php 
	
		if ($buttons) {
			echo join(array_values($buttons));
		}
		
		if ($buttons['confirm_verification']) {
			echo ui::dialogbox(array(
				'id' => 'confirm_verification_dialog',
				'title' => $translate->confirm_verification,
				'content' => $translate->dialog_confirm_verification,
				'buttons' => array(
					'confirm_verification_dialog_cancel' => array(
						'icon' => 'cancel',
						'class' => 'cancel',
						'label' => $translate->cancel
					),
					'confirm_verification_dialog_apply' => array(
						'icon' => 'apply',
						'class' => 'apply',
						'label' => $translate->yes
					)
				)
			));
		}
		
		if ($buttons['delete_confirmation']) {
			echo ui::dialogbox(array(
				'id' => 'delete_confirmation_dialog',
				'title' => $translate->delete_confirmation,
				'content' => $translate->dialog_delete_confirmation,
				'buttons' => array(
					'delete_confirmation_dialog_cancel' => array(
						'icon' => 'cancel',
						'class' => 'cancel',
						'label' => $translate->cancel
					),
					'delete_confirmation_dialog_apply' => array(
						'icon' => 'apply',
						'class' => 'apply',
						'label' => $translate->yes
					)
				)
			));
		}
	?>
	</div>
</div>

<div id="printDialog"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" >Select Export Options</div>
	</div>
	<div class="ado-modal-body">
		<form method="post">
			<div class="content-caption" >Select Month(s)</div>
			<div class="modal-fieldset groups">
				<?php
					if ($dataloader['periodes']) {
						foreach ($dataloader['periodes'] as $value => $name) {
							$checked = $period==$value ? 'checked=checked' : null;
							echo "<p>
								<input class='month' type='checkbox' name='months[$value]' value='$value' $checked /> 
								<span>$name</span>
							</p>";
						}
					}
				?>
			</div>
			<div class="content-caption" >Options</div>
			<div class="modal-fieldset options" >
				<p>
					<input type="checkbox" class='option' name='options[posindex]' value='POS' checked /> 
					<span>POS Index</span>
				</p>
				<p>
					<input type="checkbox" class='option' name='options[actual]' value='Actual' checked /> 
					<span>Actual</span>
				</p>
				<p>
					<input type="checkbox" class='option' name='options[delta]' value='Delta' checked /> 
					<span>Delta</span>
				</p>
			</div>
		</form>
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button submit-print" href="/applications/modules/ordersheet/sendmail.php" >
				<span class="icon print"></span>
				<span class="label" ><?php echo $translate->print ?></span>
			</a>
		</div>
	</div>
</div>