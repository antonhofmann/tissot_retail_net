<div class="table-container table-xxs">
	<div class="toolbox table-filters">
		<?php echo $actions ? join($actions) : null; ?>
	</div>
	<table id="listSubcategories" class="default" width="100%">
	<tr>
		<th nowrap="nowrap"><strong>Subcategory Name</strong></th>
		<th nowrap="nowrap"><strong>Active</strong></th>
	</tr>
	<?php
		if ($subcategories) {
			foreach ($subcategories as $i => $row) {
				echo "<tr>";
				echo "<td>{$row[item_subcategory_name]}</td>";
				echo "<td width='20' align='center'>{$row[active]}</td>";
				echo "</tr>";
			}
		} else {
			echo "<tr>";
			echo "<td colspan='2' class='-emptybox'>No matching records found</td>";
			echo "</tr>";
		}
	?>
	</table>
	<div class="actions">
		<?php echo $buttons ? join($buttons) : null; ?>
	</div>
</div>