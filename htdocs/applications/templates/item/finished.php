<?php 
	$user = User::instance();
	$translate = Translate::instance();
?>

<style type="text/css">

	#items { 
		width: 800px; 
	}

	#items h5 {
		margin: 25px 0 0;
		padding: 0 0 5px 0;
		color: #404040;
		font-size: 13px;
		cursor: pointer;
		border-bottom: double 3px #ccc;
	}

	#items .toggler { color: #404040; }

	
</style>
<script type="text/javascript">
$(document).ready(function() {

	$('#items h5').on('click', function(e) {
		e.preventDefault();
		$(this).next().slideToggle();
		$('.toggler', $(this)).toggleClass('fa-caret-right').toggleClass('fa-caret-down');
	})

	$('input.part').on('change', function() {
			
		var self = $(this);
		var table = self.closest('table');
		var checked = self.is(':checked') ? 1 : 0;
		var data = $('form.request').serializeArray();

		// add checked value
		data.push({ name: 'section',  value: 'item.part'});
		data.push({ name: 'checked',  value: checked});
		data.push({ name: 'child',  value: self.val() });
		
		retailnet.ajax.json('/applications/helpers/item.ajax.php', data).done(function(xhr) {

			if (xhr.success) {
				// check controllr for checkall
				$('input.category', table).trigger('checkall');
			}
		});
	});	

	$('input.category').on({
		
		checkall: function(e) {

			var table = $(this).closest('table');
			var boxes = $('input.part', table);
			var checkedBoxes = $('input.part:checked', table);

			$(this).attr('checked', boxes.length==checkedBoxes.length ? true : false);
		},
		
		change: function(e) {
			
			var table = $(this).closest('table');
			var boxes = $('input.part', table);
			var checked = $(this).is(':checked') ? true : false;

			boxes.each(function(i,el) {
				$(el).attr('checked', checked).trigger('change');
			});
		}
	}).trigger('checkall')
})
</script>
<div id="items">
<?php 

	if ($datagrid) {

		foreach ($datagrid as $key => $row) {
			
			echo "<h5><i class='toggler fa fa-caret-down'></i> {$row[caption]}</h5>";

			$table = new Table(array('theme' => 'spreadsheet', 'class' => 'spreadsheet'));
			$table->datagrid = $row['items'];
			$table->box('width=20');	
			$table->caption('box', $row['checkall']);
			$table->item_code('width=20%');	
			$table->item_name();
			$table->item_price('width=10%');	
			echo $table->render();
		}
	}			
?>
</div>
<?php echo Request::instance()->form(); ?>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>