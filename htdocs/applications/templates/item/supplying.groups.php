<?php 
	$user = User::instance();
	$translate = Translate::instance();
?>

<style type="text/css">

	#items { 
		width: 500px; 
	}
	
</style>
<script type="text/javascript">
$(document).ready(function() {

	var item = $('#item').val();
	var supplyingGroup = $('#itemSupplyingGroups td input[type="checkbox"]');
	var selectAll = $('#itemSupplyingGroups th input[type="checkbox"]');

	var submitSupplyingGroups = function(data, callback) {

		$.ajax({
			method: "POST",
  			url: "/applications/helpers/item.ajax.php",
  			dataType: "json",
			data: data
		}).done(function(xhr) {

			if (xhr && xhr.message) {
            	retailnet.notification.success(xhr.message);
            }

            if (typeof callback === 'function') {
            	callback(xhr);
            }
		})
	}

	supplyingGroup.on('change', function(e) {
		selectAll.trigger('change', [true]);
		selectAll.trigger('changed');
	})

	selectAll.on('change', function(e, manuallayTriggered) {

		if (!manuallayTriggered) {
			var checked = $(this).is(':checked') ? true : false;
			supplyingGroup.prop('checked', checked)
			selectAll.trigger('changed');
		}

		var values =  [];

		$('#itemSupplyingGroups td input[type="checkbox"]:checked').each(function(i, el) {
			values.push($(el).val());
		})

		submitSupplyingGroups({
			section: 'supplying.groups',
			item: item,
			groups: values
		})
	})

	selectAll.on('changed', function(e) {
		var checkbox = $('#itemSupplyingGroups td input[type="checkbox"]').length;
		var selected = $('#itemSupplyingGroups td input[type="checkbox"]:checked').length;
		$(this).prop('checked', checkbox==selected ? true : false); 
	}).trigger('changed')
})
</script>
<div id="items">
	<?php 
		
		$table = new Table(array('id' => 'itemSupplyingGroups'));
		$table->datagrid = $datagrid;
		$table->dataloader($dataloader);
		
		$table->supplying_group_id('width=20px');
		

		if ($datagrid && !$disabled) {
			$table->caption('supplying_group_id', '<input type=checkbox class=checkall >');
			$table->supplying_group_id(Table::DATA_TYPE_CHECKBOX);
		}
		
		$table->supplying_group_name();
		
		echo $table->render();
	?>
</div>
<?php 

	echo Request::instance()->form();

	echo "<div class='actions'>";
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
	echo "</div>";
?>