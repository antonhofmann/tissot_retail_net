<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$_ID = $_REQUEST['id'];

	if (!$_REQUEST['frame']) {
		die("You don't have access to this page.");
		exit;
	}

	$user = User::instance();
	$db = Connector::get();

	// get item_information
	$item_information = new Iteminformation();
	$item_information->read($_ID);
	$_ENTITY = $item_information->id;

	// get items
	$sth = $db->prepare("
		SELECT DISTINCT
			item_id AS id,
			item_code AS code,
			item_name AS name
		FROM items
		INNER JOIN item_information_items ON item_information_item_item_id = item_id
		WHERE item_information_item_item_information_id = ?
	");

	$sth->execute(array($_ID));
	$_DATAGRID = $sth->fetchAll();
?>
<!doctype html>
<!--[if IE 7]>    <html class="ie ie7" lang="en"> <![endif]-->
<!--[if gt IE 7]>    <html class="ie" lang="en"> <![endif]-->
<!--[if !IE]><!--><html lang="en"> <!--<![endif]--> 
<head>
	<title>item_information Items - Retail Net</title>
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link href="/public/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="/public/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/x-icon" />
	
	<script src="/public/scripts/jquery/1.9.1.js" type="text/javascript"></script>
	<script src="/public/scripts/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	
	<link rel="stylesheet" type="text/css" href="/public/scripts/fancybox/fancybox.css?v=2.1.5" />
	<script type="text/javascript" src="/public/scripts/fancybox/fancybox.js?v=2.1.5"></script>
	
	<script type="text/javascript" src="/public/scripts/jgrowl/jgrowl.js"></script>
	<link rel="stylesheet" type="text/css" href="/public/scripts/jgrowl/jgrowl.css" />
	
	<link href="/public/scripts/adomodal/adomodal.css" rel="stylesheet" type="text/css" />
	<script src="/public/scripts/adomodal/adomodal.js" type="text/javascript"></script> 
	<script type="text/javascript" src="/public/scripts/jquery.actual.min.js"></script>
	<link rel="stylesheet" href="/public/css/spinners.css">
	
	<script type="text/javascript" src="/public/scripts/message.js"></script>
	<script type="text/javascript" src="/public/scripts/retailnet.js"></script>

	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/modal.css" />
	<link rel="stylesheet" href="/public/scripts/bootstrap/css/bootstrap.compiled.css">
	<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
	
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/modal.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/reset.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/layout.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/table.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/form.css" />
	<link rel="stylesheet" type="text/css" href="/public/css/gui.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/gui.css" />

	<style type="text/css">
	
		body,
		body.adomat {
			background: #eeeeee !important;
			min-width: 400px !important;
		}

		.bootstrap {
			background: transparent;
		}
		
		.modalbox-container {
			display: block;
		}

		.modalbox-container {
			display: block;
			height: 640px;
		}

		.section + .section {
			margin-top: 40px;
		}

		.section-title {
			font-size: 14px;
			margin-bottom: 10px;
		}

		.section .table-striped tbody tr:last-child td {
			border-bottom: 1px solid #ddd;
		}

		.section tr th {
			font-weight: 500;
			font-size: 12px;
		}

		.section tbody td,
		.section tfoot td {
			font-size: 12px;
		}

		.modalbox-content-container {
			bottom: 10px;
		}
		
	</style>

	<script type="text/javascript">
		$(document).ready(function() {

			if (!$('#idEntity').val() && parent) {
				
				parent.retailnet.notification.hide();
				
				parent.retailnet.notification.show("You don't have access to this page", { 
					live: 3000,
					sticky: false, 
					theme: 'error'
				});
				
				parent.retailnet.modal.hide();
			}

			retailnet.loader.init();

			// close modal screen
			$('.close', $('#modalbox')).click(function(e) {
				
				e.preventDefault();
				e.stopImmediatePropagation();
				
				parent.retailnet.modal.close();
				
				return false;
			})

		})
	</script>
	
</head>
<body class="adomat">
	<div class="modalbox-container">
		<div id="modalbox" class="adomat modalbox">
			<div class="modalbox-header">
				<div class="title"><?php echo $item_information->name ?></div>
				<span class="fa-stack ado-modal-close close">
					<i class="fa fa-circle fa-stack-2x"></i>
					<i class="fa fa-times fa-stack-1x fa-inverse"></i>
				</span>
			</div>
			<div class="modalbox-content-container">
				<div class="modalbox-content">
					<input type="hidden" id="idEntity" name="id" value="<?php echo $_ENTITY ?>">
					<div class="bootstrap">
						<?php 

							if ($_DATAGRID) {

								echo "<table class=\"table table-condensed table-striped\">";
								echo "<thead>";
								echo "<tr>";
								echo "<th width=\"20%\" nowrap=\"nowrap\">Item Code</th>";
								echo "<th nowrap=\"nowrap\">Item Name</th>";
								echo "</tr>";
								echo "</thead>";
								echo "<tbody>";

								foreach ($_DATAGRID as $row) {
									echo "<tr>";
									echo "<td nowrap=\"nowrap\" valign=\"top\"><a href='/catalog/items/data/{$row[id]}' target='_blank' >{$row[code]}</a></td>";
									echo "<td>{$row[name]}</td>";
									echo "</tr>";
								}

								echo "</tbody>";
								echo "</table>";

							} else {
								echo "<p class=\"emptybox\">No matching records found</p>";
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>