<?php 

	$user = User::instance();
	$translate = Translate::instance();
	$settings = Settings::init();
	
	
	$data['file'] = '
		<span id="showfile" class="button">
			<span class="icon download"></span>
			<span class="label">'.$translate->download_file.'</span>	
		</span>
		<span id="upload" class="button">
			<span class="icon upload"></span>
			<span class="label">'.$translate->upload_file.'</span>	
		</span>
		<input type=hidden name="item_information_file" id="item_information_file" value="'.$data['item_information_file'].'" class="file_path required" />
		<input type="hidden" name="has_upload" id="has_upload" />
	';
	
	$form = new Form(array(
		'id' => 'form',
		'action' => '/applications/modules/iteminformation/submit.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->item_information_id(
		Form::TYPE_HIDDEN
	);

	$form->item_information_title_id(
		Form::TYPE_SELECT
	);

	$form->item_information_name(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		'class=file_title'
	);

	$form->item_information_address_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);


	$form->item_information_version_date(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		Form::TOOLTIP_DATE,
		Validata::PARAM_DATE,
		Validata::CLASS_DATAPICKER
	);


	$form->file(
		Form::TYPE_AJAX,
		FORM::PARAM_SHOW_VALUE,
		Form::PARAM_LABEL,
		'label=File (PDF only)*'
	);
	
	$form->fieldset('item_information', array(
		'item_information_title_id',
		'item_information_name',
		'item_information_address_id',
		'item_information_version_date',
		'file'
	));	

		
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}

	if ($buttons['save']) {
		$form->button(ui::button(array(
			'id' => 'save',
			'icon' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();

	
	
?>
<script type="text/javascript" src="/public/scripts/date.js"></script>
<script type="text/javascript">
	
	$(document).ready(function() {

		$('#item_information_version_date').change(function() {
		
			var self = $(this);
			var val = $(this).val();
			
			// close all notofications
			retailnet.notification.hide();
			self.removeClass('error');
			
			if (val) {
				
				if (Date.parse(val, "dd.mm.YYYY")) {
					$('#item_information_version_date').removeClass('error').next().removeClass('error');
				} else {
					self.addClass('error');
					retailnet.notification.error('Invalid date.');
				}
			}
		});
	});

</script>
<?php 
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete_file,
		'content' => $translate->dialog_delete_file,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
?>