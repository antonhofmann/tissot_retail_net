<?php 

$user = User::instance();
$translate = Translate::instance();

$form = new Form(array(
	'id' => 'collection',
	'action' => '/applications/modules/iteminformation/title/submit.php',
	'method' => 'post',
	'class' => 'validator'
));

$form->redirect(
	Form::TYPE_HIDDEN
);

$form->application(
	Form::TYPE_HIDDEN
);

$form->controller(
	Form::TYPE_HIDDEN
);

$form->item_information_title_id(
	Form::TYPE_HIDDEN
);

$form->item_information_title_title(
	Form::TYPE_TEXTAREA,
	Form::PARAM_LABEL,
	Validata::PARAM_REQUIRED
);

$form->item_information_title_description(
	Form::TYPE_TEXTAREA,
	Form::PARAM_LABEL
);

$form->fieldset('item_information_title_title', array(
	'item_information_title_title',
	'item_information_title_description'
));

if ($disabled) {
	foreach ($disabled as $field => $value) {
		if ($value) $form->param($field, Form::PARAM_DISABLED);
	}
}

if ($hidden) {
	foreach ($hidden as $field => $value) {
		if ($value) $form->param($field, Form::PARAM_HIDDEN);
	}
}

$form->button(ui::button(array(
	'id' => 'back',
	'icon' => 'back',
	'href' => $buttons['back'],
	'label' => $translate->back
)));

if ($buttons['delete']) {
	$form->button(ui::button(array(
		'id' => 'delete',
		'class' => 'dialog',
		'icon' => 'delete',
		'href' => $buttons['delete'],
		'label' => $translate->delete
	)));
}

if ($buttons['save']) {
	$form->button(ui::button(array(
		'icon' => 'save',
		'id' => 'save',
		'label' => $translate->save
	)));
}

$form->dataloader($data);
$form = $form->render();

echo $form; 

echo "<style type='text/css'> textarea {width: 400px; height: 60px;} </style>";

echo ui::dialogbox(array(
	'id' => 'delete_dialog',
	'title' => $translate->delete,
	'content' => $translate->dialog_delete_record,
	'buttons' => array(
		'cancel' => array(
			'icon' => 'cancel',
			'label' => $translate->cancel
		),
		'apply' => array(
			'icon' => 'apply',
			'label' => $translate->yes
		),
	)
));
?>