<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array(
		'id' => 'brand',
		'action' => '/applications/modules/translationresponsible/submit.php',
		'method' => 'post',
		'class' => 'validator'
	));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->translationresponsible_id(
		Form::TYPE_HIDDEN
	);

	$form->translationresponsible_address_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->translationresponsible_country_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	

	$form->translationresponsible_role_id(
		Form::TYPE_SELECT
	);

	$form->translationresponsible_user_id(
		Form::TYPE_SELECT
	);

	$form->translationresponsible_language_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);


	// fieldset
	$form->fieldset('Translation Responsible', array(
		'translationresponsible_address_id',
		'translationresponsible_country_id', 
		'translationresponsible_role_id',
		'translationresponsible_user_id', 
		'translationresponsible_language_id'
	));


	
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));
	
	
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'href' => $buttons['delete'],
			'label' => $translate->delete
		)));
	}
	
	
	if ($buttons['save']) {
		$form->button(ui::button(array(
			'icon' => 'save',
			'id' => 'save',
			'label' => $translate->save
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<?php 
	
	echo $form; 
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			),
		)
	));
	
?>
