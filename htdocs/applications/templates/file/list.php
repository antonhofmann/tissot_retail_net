<script type="text/javascript">
$(document).ready(function() {

	$(".modal").on("click", function(e) {
		
		e.preventDefault();

		var path = $(this).attr('tag');
		var title = $(this).parent().next().find('a').text();
		title = (title) ? title : '';
	
		if (isImage(path)) retailnet.modal.show(path, {title: title});
		else window.open('http://'+location.host+path);
	});
	
});
</script>
<style type="text/css">
	
	.list table, 
	.-actions { 
		width: 700px; 
	}
	
	td span {
		display:block; 
		color:gray; 
		font-size:11px;
	}
	
	td.fileicon {width: 32px;}
	td.date {width: 100px;}
	
	h6 { 
		display:block; 
		margin: 40px 0 5px; 
		font-size: .825em;
	}

	table { 
		margin-bottom:20px; 
		border-style: solid; 
		border-color: silver; 
	}
	
	.file-extension {cursor: pointer;}
	span.order_title {color: black;}
	span.filename {display:block; font-weight:600; color:#0050CC; cursor:pointer; font-size: 11px;}
	span.filename:hover { color: black; }
	
</style>
<div class='list'>
<?php if ($buttons['add']) : ?>
<div class='table-toolbox'>
	<form class=toolbox>
		<?php echo $buttons['add']; ?>
	</form>
</div>
<?php endif; ?>
<?php

	if ($datagrid) {

		$dateCreated = Translate::instance()->date_created;
		
		foreach ($datagrid as $row) {
			$ext = file::extension($row['path']);
			$row['description'] = ($row['description']) ? substr($row['description'], 0, 80).".." : null;
			$files[$row['type']]['caption'] = $row['type_name'];
			$files[$row['type']]['files'][$row['id']]['title'] = $row['title']."<span>{$row[description]}</span>";
			$files[$row['type']]['files'][$row['id']]['date']= "<span>$dateCreated:<br />{$row[date]}</span>";
			$files[$row['type']]['files'][$row['id']]['path'] = $row['path'];
			$files[$row['type']]['files'][$row['id']]['fileicon']  = "<span class='file-extension $ext modal' tag='{$row[path]}'></span>";
		}
	}

	if ($files) {
		foreach ($files as $key => $row) {
			$table = new Table(array('thead' => false));
			$table->datagrid = $row['files'];
			$table->fileicon();
			$table->title("href=$link");
			$table->date();
			echo "<h6>".$row['caption']."</h6>";
			echo $table->render();
		}
	}

	echo Request::instance()->form();
?>
</div>
<div class='actions'>
	<?php echo $buttons['back']; ?>
</div>