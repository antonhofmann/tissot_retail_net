<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	
	$form = new Form(array('class'=>'validator'));
	
	$form->redirect(
		Form::TYPE_HIDDEN
	);
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->ordersheet_id(
		Form::TYPE_HIDDEN
	);
	
	$form->mastersheet_year(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE
	);
	
	$form->mastersheet_name(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE
	);
	
	$form->ordersheet_workflowstate_id(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED		
	);
	
	$form->ordersheet_comment(
		Form::TYPE_TEXTAREA,
		Form::PARAM_LABEL,
		'class=tinymce'		
	);
	
	$form->ordersheet_openingdate(
		Form::TYPE_TEXT
	);
	
	$form->ordersheet_closingdate(
		Form::TYPE_TEXT
	);
	
	$form->fieldset('ordersheet', array(
		'mastersheet_year',
		'mastersheet_name',
		'ordersheet_workflowstate_id',
		'ordersheet_comment',
		'ordersheet_openingdate',
		'ordersheet_closingdate'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	$form->button(ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	)));

	if ($buttons['items']) {
		$form->button(ui::button(array(
			'id' => 'toggleItems',
			'icon' => 'icon120',
			'href' => '#',
			'label' => 'Show Item List'
		)));
	}
	
	if ($buttons['print']) {
		$form->button(ui::button(array(
			'id' => 'export',
			'icon' => 'print',
			'href' => $buttons['print'],
			'label' => $translate->print
		)));
	}
	
	if ($buttons['delete']) {
		$form->button(ui::button(array(
			'id' => 'delete',
			'class' => 'dialog',
			'icon' => 'delete',
			'label' => $translate->delete,
			'href' => $buttons['delete']
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	echo $form->render();

	// version items
	if ($items) {

		foreach ($items as $row) {
			
			$group = $row['group_id'];
			$item = $row['id'];	
			
			$datagrid[$group]['caption'] = $row['group_name'];
			$datagrid[$group]['data'][$item]['collection_code'] = $row['collection_code'];
			$datagrid[$group]['data'][$item]['reference_code'] = $row['reference_code'];
			$datagrid[$group]['data'][$item]['reference_name'] = $row['reference_name'];
			$datagrid[$group]['data'][$item]['reference_description'] = $row['reference_description'];
			$datagrid[$group]['data'][$item]['price'] = $row['price'];
			$datagrid[$group]['data'][$item]['currency'] = $row['currency'];
			$datagrid[$group]['data'][$item]['quantity_estimate'] = $row['quantity_estimate'];
			$datagrid[$group]['data'][$item]['quantity'] = $row['quantity'];
			
			$cal = $row['quantity'] && $row['quantity_estimate'] ? ($row['quantity']/$row['quantity_estimate'])*100 : 0;
			$datagrid[$group]['data'][$item]['calc_estimated'] = number_format($cal, 2, '.', '');
		}
	}

	if ($datagrid) {

		foreach ($datagrid as $group => $row) {
			
			$list .= "<h5>{$row[caption]}</h5>";

			$groupTotalLaunch = 0;
			$groupTotalApproved = 0;
			$totalWeekQuantity = array();
			$totalWeekQuantityApproved = array();

			$table = new Table();
			
			if ($itemWeekQuantities) {
				
				foreach ($row['data'] as $item => $value) {
						
					$totalLaunch = 0;
					$totalApproved = 0;

					for ($i=0; $i < $mastersheet['mastersheet_weeks']; $i++) { 
						
						$week = $i+$mastersheet['mastersheet_week_number_first'];
						$column = "quantity_$week";
						$row['data'][$item][$column] = $itemWeekQuantities[$item][$week]['quantity'];
						$totalLaunch = $totalLaunch+$itemWeekQuantities[$item][$week]['quantity'];
						$totalWeekQuantity[$week] = $totalWeekQuantity[$week]+$itemWeekQuantities[$item][$week]['quantity'];

						if ($showApprovment) {
							$column = "quantity_approved_$week";
							$row['data'][$item][$column] = $itemWeekQuantities[$item][$week]['quantity_approved'];
							$totalApproved = $totalApproved+$itemWeekQuantities[$item][$week]['quantity_approved'];
							$totalWeekQuantityApproved[$week] = $totalWeekQuantityApproved[$week]+$itemWeekQuantities[$item][$week]['quantity_approved'];
						}
					}

					$row['data'][$item]['total_week_quantity'] = $totalLaunch;
					$row['data'][$item]['total_week_quantity_approved'] = $totalApproved;
					
					// total calculations
					$groupTotalLaunch = $groupTotalLaunch+$totalLaunch;
					$groupTotalApproved = $groupTotalApproved+$totalApproved;
					$quantity_estimate = $quantity_estimate+$value['quantity_estimate'];
					$quantity = $quantity+$value['quantity'];
					$calc_estimated = $calc_estimated+$value['calc_estimated'];
				}
			}

			$table->datagrid = $row['data'];
			$table->collection_code('nowrap=nowrap');
			$table->reference_code('nowrap=nowrap');
			$table->reference_name('nowrap=nowrap');
			$table->reference_description('width=200px');
			$table->price('nowrap=nowrap','caption=Estimated Price', 'class=number');
			$table->quantity_estimate('nowrap=nowrap', "caption=Estimated for {$mastersheet[mastersheet_estimate_month]} months", 'class=number');
			$table->quantity('nowrap=nowrap', 'caption=Total Planned', 'class=number');
			$table->calc_estimated('nowrap=nowrap', 'caption=% of Estimated', 'class=number');

			// week columns
			if ($mastersheet['mastersheet_weeks']) {

				// week quantities
				for ($i=0; $i < $mastersheet['mastersheet_weeks']; $i++) { 
					$week = $i+$mastersheet['mastersheet_week_number_first'];
					$column = "quantity_$week";
					$table->$column('nowrap=nowrap', "caption=Week $week", 'class=number');
				}

				// total week quantities
				$table->total_week_quantity('nowrap=nowrap', 'caption=Total Launch',  'class=number');

				if ($showApprovment) {

					// week approved quantities
					for ($i=0; $i < $mastersheet['mastersheet_weeks']; $i++) { 
						$week = $i+$mastersheet['mastersheet_week_number_first'];
						$column = "quantity_approved_$week";
						$table->$column('nowrap=nowrap', "caption=Week $week", 'class=number');
					}

					// total week quantities
					$table->total_week_quantity_approved('nowrap=nowrap', 'caption=Total Approved',  'class=number');
				}

			}

			//$quantity_estimate = number_format($quantity_estimate, 2, '.', '');

			$table->footer(array(
				'content' => "<h6>Total {$row[caption]}</h6>",
				'attributes' => array('colspan' => 5)
			));

			$table->footer(array(
				'content' => $quantity_estimate,
				'attributes' => array('class' => 'number')
			));

			$table->footer(array(
				'content' => $quantity,
				'attributes' => array('class' => 'number')
			));

			$calc_estimated = $quantity && $quantity_estimate ? ($quantity/$quantity_estimate)*100 : 0;
			$table->footer(array(
				'content' => number_format($calc_estimated, 2, '.', '').' %',
				'attributes' => array('class' => 'number')
			));

			// week columns
			if ($mastersheet['mastersheet_weeks']) {

				// week quantities
				for ($i=0; $i < $mastersheet['mastersheet_weeks']; $i++) { 
					
					$week = $i+$mastersheet['mastersheet_week_number_first'];

					$table->footer(array(
						'content' => $totalWeekQuantity[$week],
						'attributes' => array('class' => 'number')
					));
				}

				// group total launch
				$table->footer(array(
					'content' => $groupTotalLaunch,
					'attributes' => array('class' => 'number')
				));

				if ($showApprovment) {

					// week quantities
					for ($i=0; $i < $mastersheet['mastersheet_weeks']; $i++) { 
						
						$week = $i+$mastersheet['mastersheet_week_number_first'];

						$table->footer(array(
							'content' => $totalWeekQuantityApproved[$week],
							'attributes' => array('class' => 'number')
						));
					}

					// group total launch
					$table->footer(array(
						'content' => $groupTotalApproved ,
						'attributes' => array('class' => 'number')
					));
				}
			}

			$list .= $table->render();
		}

	}
?>
<style type="text/css">

	#itemslist {
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;  
		padding: 0 40px 40px 0;
		overflow-x: auto;
		display: none;
	}

	h5 {
		font-size: 18px;
		margin: 40px 0 10px;
	}

	#itemslist table {
		width: 100%;
	}

</style>
<script type="text/javascript">
	$(document).ready(function() {
		
		var buttonCaption = $('.label', $('#toggleItems')).text();

		$('#toggleItems').on('click', function() {
			var text = $('.label', $(this)).text()==buttonCaption ? 'Hide Item List' : 'Show Item List';
			$('.label', $(this)).text(text);
			$('#itemslist').slideToggle('fast');
		});
	});
</script>
<?php 

	echo "<div id='itemslist' >$list</div>";
	
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			)
		)
	));
	
?>