<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

$settings = Settings::init();
$user = User::instance();
$translate = Translate::instance();
$request = request::instance();
echo $request->form();
?>
<style type="text/css">
	
	#reports {
		width: 1000px;
	}

	.table-toggler {
		cursor: pointer;
		font-size: 16px;
	}

	.row-subtable {
		display:none;
	}

	.has-subtable td {
		border-bottom: 1px solid silver;
	}

	.row-subtable td {
		border-top: 1px solid silver;
		border-bottom: 1px solid silver;
		background-color: white !important;
	}

	.row-subtable td.cell-subtable {
		padding: 10px !important;
	}

	.subtable-container {
		display: block;
		background-color: white;
		border: 1px solid silver;
		box-shadow: rgba(100,100,100,0.4) 0 0 10px 3px;
		padding: 10px;
	}

	.subtable-container .order-container {
		display: block;
	}

	.order-container + .order-container {
		margin-top: 20px;
	}

	.order-container .order-header {
		display: block;
		padding: 5px 10px;
		margin: 0 0 10px 0;
		border-bottom: 1px solid silver;
		height: 18px;
		vertical-align: bottom;
	}

	.unnotified .order-header {
		height: 25px;
	}

	.order-header h5 {
		font-size: 14px;
	}

	.unnotified .order-header h5 {
		color: #009909;
		float: left;
	}

	.order-header .order-actions {
		display: block;
		float: right;
	}

	.order-actions button + button { 
		margin-left: 10px;
	}

	table.subtable {
		border: 1px solid silver;
	}

</style>
<div id="reports"></div>
<script type="text/javascript" src="/public/scripts/tinymce4/js/tinymce/tinymce.min.js"></script>

<!-- mail template modal -->
<div id="sendmail-template" class="ado-modal ado-box">
	<div class="ado-modal-header">
		<div class="ado-title"><?php echo $translate->sendmail; ?></div>
		<div class="ado-subtitle"></div>
	</div>
	<div class="ado-modal-body">
		<form action="/applications/modules/ordersheet/sendmail.php" method="post" >
			<input type="hidden" name="mail_template_id" id="mail_template_id" />
			<input type="hidden" name="mail_template_view_modal" id="mail_template_view_modal" />
			<div class="ado-row">
				<input type=text name="mail_template_subject" id="mail_template_subject" class="ado-modal-input required" />
			</div>
			<div class="ado-row">
				<textarea name="mail_template_text" id="mail_template_text" class="ado-modal-input required" ></textarea>
			</div>
		</form>
	</div>
	<div class="ado-modal-footer" >
		<div class="ado-row ado-actions">
			<a class='button cancel ado-modal-close'>
				<span class="icon cancel"></span>
				<span class="label"><?php echo $translate->cancel ?></span>
			</a>
			<a class='button sendmail apply'>
				<span class="icon mail"></span>
				<span class="label"><?php echo $translate->sendmail ?></span>
			</a>
		</div>
	</div>
</div>

<!-- preview mail modal -->
<div id="preview-mail-template" class="ado-modal ado-box">
	<div class="ado-modal-header">
		<span class="fa-stack ado-modal-close">
			<i class="fa fa-circle fa-stack-2x"></i>
			<i class="fa fa-times fa-stack-1x fa-inverse"></i>
		</span>
		<div class="ado-row ado-title mail-subject"></div>
		<div class="ado-row" >
			<span class="ado-label ado-width-30">To:</span>
			<span class="ado-label mail-address"></span>
		</div>			
		<div class="ado-row">
			<span class="ado-label ado-width-30">CC:</span>
			<span class="ado-label mail-cc-address"></span>
		</div>
		<div class="ado-notification-right-bottom">
			<span class="ado-label mail-date"></span>
		</div>
	</div>
	<div class="ado-modal-body ado-max-height-500 mail-content"></div>
	<div class="ado-modal-footer">
		<div class="mail-footer"></div>
	</div>
</div>

<!-- test mail modal dialog -->
<div id="test-mail-template"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" >Send Test E-mail an:</div>
	</div>
	<div class="ado-modal-body">
		<input type="text" name="test-mail-recipient" id="test-mail-recipient" class="ado-modal-input required" value="<?php echo $user->email ?>" placeholder="Recipient E-mail" />
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="/applications/modules/ordersheet/sendmail.php" >
				<span class="icon mail"></span>
				<span class="label" ><?php echo $translate->send ?></span>
			</a>
		</div>
	</div>
</div>