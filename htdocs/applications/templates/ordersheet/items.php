<?php 

	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();
	
	// db application model
	$model = new Model($request->application);

	// order sheet
	$ordersheet = new Ordersheet($application);
	$ordersheet->read($id);
	
	// ordersheet items
	$result = $model->query("
		SELECT 
			mps_ordersheet_item_id,
			mps_ordersheet_item_price,
			mps_ordersheet_item_quantity,
			mps_ordersheet_item_quantity_proposed,
			mps_ordersheet_item_quantity_approved,
			mps_ordersheet_item_quantity_distributed,
			mps_ordersheet_item_quantity_confirmed,
			mps_ordersheet_item_quantity_shipped,
			mps_ordersheet_item_status,
			mps_ordersheet_item_price,
			mps_ordersheet_item_order_date,
			mps_material_collection_code,
			mps_material_collection_category_id,
			mps_material_collection_category_code,
			mps_material_planning_type_id,
			mps_material_planning_type_name,
			mps_material_code,
			mps_material_name,
			mps_material_setof,
			mps_material_hsc,
			currency_symbol,
			(mps_ordersheet_item_price * mps_ordersheet_item_quantity_confirmed) AS total_cost_ordered
		FROM mps_ordersheet_items
	")
	->bind(Ordersheet_Item::DB_BIND_ORDERSHEETS)
	->bind(Ordersheet_Item::DB_BIND_MATERIALS)
	->bind(Material::DB_BIND_PLANNING_TYPES)
	->bind(Material::DB_BIND_COLLECTIONS)
	->bind(Material::DB_BIND_COLLECTION_CATEGORIES)
	->bind(Material::DB_BIND_CURRENCIES)
	->filter('ordersheets', "mps_ordersheet_id = $id")
	->order('mps_material_planning_type_name')
	->order('mps_material_collection_category_code')
	->order('mps_material_code')
	->order('mps_material_name')
	->fetchAll();
	
	if ($result) { 

		// workflow states
		$state_completed = $ordersheet->state()->isCompleted();
		$state_approved = $ordersheet->state()->isApproved();
		$state_revision = $ordersheet->state()->isRevision();
		$STATE_DISTRIBUTED = $ordersheet->state()->isDistributed();
		$state_expired = $ordersheet->state()->isExpired();
		$state_open = $ordersheet->state()->open;
		$icon_revision = ui::icon('warrning');
		
		// user role
		$administrator = $ordersheet->state()->canAdministrate();
		$user_owner = $ordersheet->state()->owner;

		// group statments
		$statment_on_preparation = $ordersheet->state()->onPreparation();
		$statment_on_completing = $ordersheet->state()->onCompleting();
		$statment_on_revision = $ordersheet->state()->onCompleting();
		$statment_on_confirmation = $ordersheet->state()->onConfirmation();
		$statment_on_distribution = $ordersheet->state()->onDistribution();
		
		foreach ($result as $row) {
			
			$item = $row['mps_ordersheet_item_id'];
			$planning = $row['mps_material_planning_type_id'];
			$collection = $row['mps_material_collection_category_id'];
			$isDummy = $dummyPlanningTypes[$planning];
			
			// quantities
			$quantity = $row['mps_ordersheet_item_quantity'];
			$quantity_proposed = $row['mps_ordersheet_item_quantity_proposed'];
			$quantity_approved = $row['mps_ordersheet_item_quantity_approved'];
			$quantity_confirmed = $row['mps_ordersheet_item_quantity_confirmed'];
			$quantity_shipped = $row['mps_ordersheet_item_quantity_shipped'];
			$quantity_distributed = $row['mps_ordersheet_item_quantity_distributed'];
			
			// if order sheet is archived
			// reset all NULL quantitis to Zero
			if ($STATE_DISTRIBUTED) {
				$quantity = ($quantity) ? $quantity : 0;
				$quantity_proposed = ($quantity_proposed) ? $quantity_proposed : 0;
				$quantity_approved = ($quantity_approved) ? $quantity_approved : 0;
			}
			
			// item prise
			$item_price = $row['mps_ordersheet_item_price'];
				
			// item ordered date
			$item_order_date = $row['mps_ordersheet_item_order_date'];
			
			// total prices
			if ($statment_on_preparation) {
				if ($state_approved) $totalprice = $item_price * $quantity_approved;
				elseif ($quantity_approved) $totalprice = $item_price*$quantity_approved;
				elseif ($quantity) $totalprice = $item_price*$quantity;
				elseif ($quantity_proposed) $totalprice = $item_price*$quantity_proposed;	
				else $totalprice = null;
			}
			elseif ($statment_on_confirmation) {				
				$totalprice = ($quantity_confirmed) ? $item_price*$quantity_confirmed : $item_price*$quantity_approved;
			} 
			else {
				if ($quantity_distributed) $totalprice = $item_price*$quantity_distributed;
				elseif($quantity_shipped) $totalprice = $item_price*$quantity_shipped;
				else $totalprice = ($quantity_confirmed) ? $item_price*$quantity_confirmed : $item_price*$quantity_approved;
			}
			
			// total costs 
			$total_cost = number_format($totalprice, $settings->decimal_places, '.', '');
				
			// datagrid
			$datagrid[$planning]['caption'] = $row['mps_material_planning_type_name'];
			$datagrid[$planning]['data'][$collection]['caption'] = $row['mps_material_collection_category_code'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_material_collection_code'] = $row['mps_material_collection_code'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_material_code'] = $row['mps_material_code'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_material_name'] = $row['mps_material_name'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_material_setof'] = $row['mps_material_setof'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_material_hsc'] = $row['mps_material_hsc'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_price'] = $row['mps_ordersheet_item_price'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['currency_symbol'] = strtoupper($row['currency_symbol']);
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_quantity_proposed'] = $quantity_proposed;
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_quantity'] = $quantity;
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_quantity_approved'] = $quantity_approved;
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_quantity_confirmed'] = $quantity_confirmed;
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_quantity_distributed'] = $quantity_distributed;
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_quantity_shipped'] = $quantity_shipped;
			$datagrid[$planning]['data'][$collection]['data'][$item]['mps_ordersheet_item_status'] = $row['mps_ordersheet_item_status'];
			$datagrid[$planning]['data'][$collection]['data'][$item]['total_cost'] = $total_cost;
				
			// proposed quantities
			if ($columns['proposed']['show'] || $columns['proposed']['edit']) {
				if ($columns['proposed']['edit'] && !$isDummy) {
					$dataloader['mps_ordersheet_item_quantity_proposed'][$item] = "<input type=text data=$item class=quantity_proposed name=mps_ordersheet_item_quantity_proposed[$item] value='$quantity_proposed' />";
				} 
				else $dataloader['mps_ordersheet_item_quantity_proposed'][$item] = $quantity_proposed;
			}
			
			// owner quantities
			if ($columns['quantity']['show'] || $columns['quantity']['edit']) {
				
				if ($columns['quantity']['edit'] && !$isDummy) {
					if ($state_revision && $user_owner) {
						$quantitybox = ($row['mps_ordersheet_item_status']) ? true : false;
						$dataloader['mps_ordersheet_item_quantity'][$item] = ($row['mps_ordersheet_item_status']) ? "<input type=text data='$item' class=quantity name=mps_ordersheet_item_quantity[$item] value='$quantity' />" : $quantity;
					} else {
						$dataloader['mps_ordersheet_item_quantity'][$item] = "<input type=text data='$item' class=quantity name=mps_ordersheet_item_quantity[$item] value='$quantity' />";				
					}
				}
				else $dataloader['mps_ordersheet_item_quantity'][$item] = $quantity;
			}
			
			// approved quantities
			if ($columns['approved']['show'] || $columns['approved']['edit']) {
				
				if ($columns['approved']['edit'] && !$isDummy) {
					$dataloader['mps_ordersheet_item_quantity_approved'][$item] = "<input type=text data='$item' class=quantity_approved name=mps_ordersheet_item_quantity_approved[$item] value='$quantity_approved' />";
				}
				else $dataloader['mps_ordersheet_item_quantity_approved'][$item] = $quantity_approved;
			}
			
			// confirmed quantities
			if ($columns['confirmed']['show'] || $columns['confirmed']['edit']) {
				if ($columns['confirmed']['edit'] && !$isDummy) {
					$dataloader['mps_ordersheet_item_quantity_confirmed'][$item] = "<input type=text data='$item' class=quantity_confirmed name=mps_ordersheet_item_quantity_confirmed[$item] value='$quantity_confirmed' />";
				}
				else $dataloader['mps_ordersheet_item_quantity_confirmed'][$item] = $quantity_confirmed;
			}
			
			// confirmed quantities
			if ($columns['shipped']['show'] || $columns['shipped']['edit']) {
				if ($columns['shipped']['edit'] && !$isDummy) {
					$dataloader['mps_ordersheet_item_quantity_shipped'][$item] = "<input type=text data='$item' class=quantity_shipped name=mps_ordersheet_item_quantity_shipped[$item] value='$quantity_shipped' />";
				}
				else $dataloader['mps_ordersheet_item_quantity_shipped'][$item] = $quantity_shipped;
			}
			
			// distributed quantities
			if ($columns['distributed']['show'] || $columns['distributed']['edit']) {
				if ($columns['distributed']['edit'] && !$isDummy) {
					$dataloader['mps_ordersheet_item_quantity_distributed'][$item] = "<input type=text data='$item' class=quantity_distributed name=mps_ordersheet_item_quantity_distributed[$item] value='$quantity_distributed' />";
				}
				else $dataloader['mps_ordersheet_item_quantity_distributed'][$item] = $quantity_distributed;
			}
			
			// dataloder: item in revision
			if ($administrator && !$item_order_date) {
				if ($state_completed) {
					$checked = ($row['mps_ordersheet_item_status']) ? "checked=checked" : null;
					$dataloader['mps_ordersheet_item_status'][$item] = "<input type=checkbox data='$item' class=item_revision $checked name=mps_ordersheet_item_status[$item] value=1 />";
				}
				else { 
					$dataloader['mps_ordersheet_item_status'][$item] = ($row['mps_ordersheet_item_status']) ? $icon_revision : null;
				}
			} 
		}
		
		
		if ($datagrid) { 
				
			foreach ($datagrid as $key => $row) {
				
				$planningType = $row['caption'];
				
				$list .= "<h5>$planningType</h5>";
				
				foreach ($row['data'] as $subkey => $value) {
					
					$collectionCode = $value['caption'];
					$list .= "<h6>$collectionCode</h6>";
						
					$totalprice = 0;
					$tableKey = $key."-".$subkey;
					
					$table = new Table(array(
						'id' => $tableKey
					));
						
					$table->datagrid = $value['data'];
					$table->dataloader($dataloader);

					$table->mps_material_collection_code(
						Table::ATTRIBUTE_NOWRAP,
						'width=10%'
					);
					
					$table->mps_material_code(
						Table::ATTRIBUTE_NOWRAP,
						'width=10%'
					);
					
					$table->mps_material_name();

					$table->mps_material_hsc(
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);
					
					$table->mps_material_setof(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);
					
					$table->mps_ordersheet_item_price(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						'width=5%'
					);
					
					$table->currency_symbol(
						Table::ATTRIBUTE_NOWRAP,
						'width=20px'
					);
					
					// proposed quantites
					if ($columns['proposed']['show']) {
						
						$table->mps_ordersheet_item_quantity_proposed(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
						
						if ($columns['proposed']['edit']) {
							$table->mps_ordersheet_item_quantity_proposed(
								Table::PARAM_GET_FROM_LOADER
							);
						}
					}
					
					// owner quantites
					if ($columns['quantity']['show']) {
						
						$table->mps_ordersheet_item_quantity(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
						
						if ($columns['quantity']['edit']) {
							$table->mps_ordersheet_item_quantity(
								Table::PARAM_GET_FROM_LOADER
							);
						}
					}
				
					// approved quantities
					if($columns['approved']['show']) {
						
						$table->mps_ordersheet_item_quantity_approved(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
						
						if ($columns['approved']['edit']) {
							$table->mps_ordersheet_item_quantity_approved(
								Table::PARAM_GET_FROM_LOADER
							);
						}
					}
					
					// confirmed quantites
					if ($columns['confirmed']['show']) {
						
						$table->mps_ordersheet_item_quantity_confirmed(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
						
						if ($columns['confirmed']['edit']) {
							$table->mps_ordersheet_item_quantity_confirmed(
								Table::PARAM_GET_FROM_LOADER
							);
						}
					}
					
					// shipped quantites
					if ($columns['shipped']['show']) {
						
						$table->mps_ordersheet_item_quantity_shipped(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
						
						if ($columns['confirmed']['edit']) {
							$table->mps_ordersheet_item_quantity_shipped(
								Table::PARAM_GET_FROM_LOADER
							);
						}
					}
					
					// distributed quantites
					if ($columns['distributed']['show']) {
						
						$table->mps_ordersheet_item_quantity_distributed(
							Table::ATTRIBUTE_ALIGN_RIGHT,
							Table::ATTRIBUTE_NOWRAP,
							Table::FORMAT_NUMBER,
							'width=5%'
						);
						
						if ($columns['distributed']['edit']) {
							$table->mps_ordersheet_item_quantity_distributed(
								Table::PARAM_GET_FROM_LOADER
							);
						}
					}
					
					// total price
					$table->total_cost(
						Table::ATTRIBUTE_ALIGN_RIGHT,
						Table::ATTRIBUTE_NOWRAP,
						Table::FORMAT_NUMBER,
						Table::FORMAT_NUMBER,
						'width=5%'
					);
					
					$has_item_in_revision = false;
					
					// total collection price
					foreach ($value['data'] as $key => $array) {
						
						$totalprice = $totalprice+$array['total_cost'];
						
						if (!$has_item_in_revision && $dataloader['mps_ordersheet_item_status'][$key]) {
							$has_item_in_revision = true;
						}
					}
						
					// items in revision
					if ($has_item_in_revision) {
						$table->mps_ordersheet_item_status(
							Table::PARAM_GET_FROM_LOADER,
							Table::ATTRIBUTE_ALIGN_CENTER,
							'width=20px'
						);
					}
					
					$total_cost = number_format($totalprice, $settings->decimal_places, '.', '');

					$table->footer(array(
						'content' => $translate->total_cost." (<em>$collectionCode</em>): <b class='total-items $tableKey'>$total_cost</b>",
						'attributes' => array(
							'colspan' => 15,
							'align' => 'right'
						)
					));
					
					$list .= $table->render();
					//$list .= "<div class='totoal-collection'>".$translate->total_cost." (<em>$collectionCode</em>): <b class='total-items $tableKey'>$total_cost</b></div>";
				}
			}
		}
	}
	else {
		$list = html::emptybox($translate->empty_result);
	}

?>
<style type="text/css">

	#items {
		width: 1300px;
		padding-right: 40px;
	}
	
	h6 { 
		display: block; 
		margin-bottom: 5px; 
		font-size: .8em;
		font-weight: 500;
	}
	
	h5 { 
		display: block;
		margin: 40px 0 20px; 
		font-size: 1em;
		font-variant: small-caps;
		border-bottom: 1px solid silver;
		color: gray;
	}
	
	.totoal-collection {
		display: block;
		width: 100%;
		font-size: .75em;
		text-align: right;
		padding-top: 5px;
		margin-bottom: 20px;
	}
	
	.totoal-collection b {
		padding-left: 5px;
	}
	
	#items input[type=text] {
		width: 50px !important;
	}

	
</style>
<form id="items" class="ordersheet_items default" method="post" ><?php echo $list; ?></form>
<div class="actions"><?php echo $buttons ? join($buttons) : ''; ?></div>
<?php echo Request::instance()->form() ?>

<!-- wysiwyg editor -->
<script type="text/javascript" src="/public/scripts/tinymce4/js/tinymce/tinymce.min.js"></script>

<!-- mail template modal -->
<div id="sendmail-template" class="ado-modal ado-box">
	<div class="ado-modal-header">
		<div class="ado-title"><?php echo $translate->sendmail; ?></div>
		<div class="ado-subtitle"><?php echo $maildata['mail_template_code'] ?></div>
	</div>
	<div class="ado-modal-body">
		<form action="/applications/modules/ordersheet/sendmail.php" method="post" >
			<input type="hidden" name="mail_template_id" id="mail_template_id" value="<?php echo $maildata['mail_template_id'] ?>" />
			<input type="hidden" name="mail_template_view_modal" id="mail_template_view_modal" value="<?php echo $maildata['mail_template_view_modal'] ?>" />
			<div class="ado-row">
				<input type=text name="mail_template_subject" id="mail_template_subject" class="ado-modal-input required" value="<?php echo $maildata['mail_template_subject'] ?>" />
			</div>
			<div class="ado-row">
				<textarea name="mail_template_text" id="mail_template_text" class="ado-modal-input required" ><?php echo $maildata['mail_template_text'] ?></textarea>
			</div>
		</form>
	</div>
	<div class="ado-modal-footer" >
		<div class="ado-row ado-actions">
			<a class='button cancel ado-modal-close'>
				<span class="icon cancel"></span>
				<span class="label"><?php echo $translate->cancel ?></span>
			</a>
			<a class='button test-mail' href="#">
				<span class="icon mail"></span>
				<span class="label">Test Mail</span>
			</a>
			<a class='button preview-mail' href="/applications/modules/ordersheet/sendmail.preview.php">
				<span class="icon icon84"></span>
				<span class="label">Preview</span>
			</a>
			<a class='button sendmail'>
				<span class="icon mail"></span>
				<span class="label"><?php echo $translate->sendmail ?></span>
			</a>
		</div>
	</div>
</div>

<!-- preview mail modal -->
<div id="preview-mail-template" class="ado-modal ado-box">
	<div class="ado-modal-header">
		<span class="fa-stack ado-modal-close">
			<i class="fa fa-circle fa-stack-2x"></i>
			<i class="fa fa-times fa-stack-1x fa-inverse"></i>
		</span>
		<div class="ado-row ado-title mail-subject"></div>
		<div class="ado-row" >
			<span class="ado-label ado-width-30">To:</span>
			<span class="ado-label mail-address"></span>
		</div>			
		<div class="ado-row">
			<span class="ado-label ado-width-30">CC:</span>
			<span class="ado-label mail-cc-address"></span>
		</div>
		<div class="ado-notification-right-bottom">
			<span class="ado-label mail-date"></span>
		</div>
	</div>
	<div class="ado-modal-body ado-max-height-500 mail-content"></div>
	<div class="ado-modal-footer">
		<div class="mail-footer"></div>
	</div>
</div>

<!-- test mail modal dialog -->
<div id="test-mail-template"  class="ado-modal ado-dialog">
	<div class="ado-modal-header">
		<div class="ado-title" >Send Test E-mail an:</div>
	</div>
	<div class="ado-modal-body">
		<input type="text" name="test-mail-recipient" id="test-mail-recipient" class="ado-modal-input required" value="<?php echo $user->email ?>" placeholder="Recipient E-mail" />
	</div>
	<div class="ado-modal-footer">
		<div class="ado-row ado-actions">
			<a class="button ado-modal-close" href="#" >
				<span class="icon cancel"></span>
				<span class="label" ><?php echo $translate->cancel ?></span>
			</a>
			<a class="button apply" href="/applications/modules/ordersheet/sendmail.php" >
				<span class="icon mail"></span>
				<span class="label" ><?php echo $translate->send ?></span>
			</a>
		</div>
	</div>
</div>
