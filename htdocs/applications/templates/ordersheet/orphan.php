<link rel="stylesheet" type="text/css" href="/public/css/switch.css">
<style type="text/css">
	
	.orphans {
		display: inline-block;
		border: 1px solid silver;
		background-color: #fff;
		border-collapse: collapse;
	}
	
	.orphans thead.caption td {
		background-color: #ccc;
		border-bottom: 1px solid silver;
		padding: 10px;
		line-height: 26px;
	}

	.switch {
		float: right;
	}

	.orphans thead.caption td:first-child {
		border-right: 1px solid silver;
	}

	.orphans .listing thead td {
		font-weight: 600;
		font-variant: small-caps;
		font-size: 14px;
		border-bottom: 1px solid silver;
	}

	.ramco-items {
		padding: 10px;
		border-right: 1px solid silver;
	}

	.mps-items {
		padding: 10px;
	}

	.orphans .listing tbody tr:nth-child(odd) td { background-color:#f3f3f3; border-bottom: 1px solid #ccc;  }
	.orphans .listing tbody tr:nth-child(even) td { background-color:#fff; border-bottom: 1px solid #ccc; }

	#ordersheet {
		height: 26px;
		line-height: 26px;
	}
	
	
</style>
<script type="text/javascript">
	$(document).ready(function() {

		var application = $('#application').val();
		var form = $('.form-orphan');

		$('#ordersheet').on('change', function() {

			var self = $(this);
			var val = self.val();

			$('.mps-items tbody').empty();

			retailnet.notification.hide();
			self.removeClass('error');

			if (val) {
				
				retailnet.ajax.json('/applications/modules/ordersheet/ajax.php', {
					section: 'exchange.import.items',
					application: application,
					id: val
				}).done(function(xhr) {

					if (xhr) {
						
						$.each(xhr, function(i, item) {
							var tr = $('<tr />');
							tr.append("<td nowrap=nowrap >" + item.code + "</td>");
							tr.append("<td nowrap=nowrap >" + item.customer + "</td>");
							tr.append("<td nowrap=nowrap >" + item.shipto + "</td>");
							tr.append("<td nowrap=nowrap >" + item.approved + "</td>");
							tr.append("<td nowrap=nowrap >" + item.pon + "</td>");
							$('.mps-items tbody').append(tr);
						});
					}
				});
			}

		});

		$('#search-mode').on('change', function() {
			var checked= $(this).is(':checked');
			$('option:not(.sensitive):not(:first)', $('#ordersheet')).toggle(!checked);
			$('#ordersheet').val('').trigger('change');
		}).trigger('change');

		$('#import').on('click', function(e) {
			
			e.preventDefault();

			var self = $(this);
			var ordersheet = $('#ordersheet').val();
			$('#ordersheet').removeClass('error');

			retailnet.notification.hide();

			if (ordersheet) {
				form.attr('action', $(this).attr('href')).submit();
			}
			else {
				$('#ordersheet').addClass('error').focus();
				retailnet.notification.error("Please select an order sheet.");
			}

			return false;
		});

		$('#ignore').on('click', function(e) {
			e.preventDefault();
			form.attr('action', $(this).attr('href')).submit();
			return false;
		});

		form.submit(function(e) {

			e.preventDefault();

			var self = $(this);

			retailnet.loader.show();

			retailnet.ajax.json(self.attr('action'), self.serialize()).done(function(xhr) {

				retailnet.loader.hide();
				
				if (xhr) {
				
					if (xhr.response && xhr.redirect) {
						window.location=xhr.redirect;
					}	

					if (xhr.message) {
						if (xhr.response) retailnet.notification.success(xhr.message);
						else retailnet.notification.error(xhr.message);
					}
				}
			});

			return false;
		});

	});
</script>
<form class="default form-orphan" method="post">
	<input type="hidden" name="id" id="id" value="<?php echo $id ?>" >
	<input type="hidden" name="pon" id="pon" value="<?php echo $pon ?>" >
	<input type="hidden" name="application" id="application" value="<?php echo $application ?>" >
	<table class="orphans">
		<thead class="caption">
			<tr>
				<td width="50%" align="left">
					Ramco Purchase Order Number: <?php echo $pon ?>
					<label class='switch switch-green' style='width:100px !important' >
						<input type='checkbox' class='switch-input' id="search-mode" checked=checked>
						<span class='switch-label' data-on='Sensitive' data-off='Insensitive'></span>
						<span class='switch-handle'></span>
					</label>
				</td>
				<td align="right">
					<select name="ordersheet" id="ordersheet">
						<option value="">Select Order Sheet</option>
						<?php
							if ($ordersheets) {
								foreach ($ordersheets as $value => $row) {
									echo "<option value='$value' class='{$row['class']}' >{$row['caption']}</option>";
								}
							}
						?> 
					</select>
				</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="ramco-items" valign="top">
					<table class="listing">
						<thead>
							<tr>
								<td nowrap="nowrap">Item</td>
								<td nowrap="nowrap" width="15%">Customer No.</td>
								<td nowrap="nowrap" width="15%">Shipto No.</td>
								<td nowrap="nowrap" width="15%">Sold No.</td>
								<td nowrap="nowrap" width="15%">Confirmed</td>
								<td nowrap="nowrap" width="15%">Shipped</td>
							</tr>
						</thead>
						<tbody>
							<?php 
								if ($exchangeItems) {
									foreach ($exchangeItems as $k => $row) {
										echo "<tr>";
										echo "<td nowrap='nowrap' >{$row[code]}</td>";
										echo "<td nowrap='nowrap' >{$row[customer]}</td>";
										echo "<td nowrap='nowrap' >{$row[shipto]}</td>";
										echo "<td nowrap='nowrap' >{$row[sap]}</td>";
										echo "<td nowrap='nowrap' >{$row[confirmed]}</td>";
										echo "<td nowrap='nowrap' >{$row[shipped]}</td>";
										echo "</tr>";
									}
								}
							?>
						</tbody>
					</table>
				</td>
				<td class="mps-items"  valign="top">
					<table class="listing">
						<thead>
							<tr>
								<td nowrap="nowrap">Item</td>
								<td nowrap="nowrap" width="15%">Customer No.</td>
								<td nowrap="nowrap" width="15%">Shipto No.</td>
								<td nowrap="nowrap" width="15%">Approved</td>
								<td nowrap="nowrap" width="15%">PO Number</td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	<div class="actions">
		<a id="back" href="<?php echo $buttons['back']?>" class="button">
			<span class="icon back"></span>
			<span class="label">Back</span>
		</a>

		<?php if ($buttons['ignore']) : ?>
		<a id="ignore" class="button submit" href="/applications/modules/ordersheet/orphans/ignore.php">
			<span class="icon cancel"></span>
			<span class="label">Kill the orphan</span>
		</a>
		<?php endif; ?>

		<?php if ($buttons['import']) : ?>
		<a id="import" class="button submit" href="/applications/modules/ordersheet/orphans/import.php">
			<span class="icon save"></span>
			<span class="label">Import</span>
		</a>
		<?php endif; ?>
	</div>
</form>