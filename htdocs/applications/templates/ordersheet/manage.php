<?php 

	$settings = Settings::init();
	$user = User::instance();
	$translate = Translate::instance();
	$request = request::instance();
	
	$data['listing'] = '
		<ul class="tabbed">
			<li id="existing_ordersheets" ><a class="existing_ordersheets">Existing Order Sheets</a></li>
			<li id="ordersheet_items" ><a class="ordersheet_items">Items for existing Order Sheets</a></li>
			<li id="new_ordersheets" ><a class="new_ordersheets selected">Customers without an Order Sheet</a></li>
		</ul>
		<div class="tabbed-content">
			<div class="content" id=loader ></div>
		</div>
	';
	
	$form = new Form(array(
		'id' => 'bulkform',
		'method' => 'post'
	));
	
	$form->application(
		Form::TYPE_HIDDEN
	);
	
	$form->controller(
		Form::TYPE_HIDDEN
	);
	
	$form->action(
		Form::TYPE_HIDDEN
	);
	
	$form->selected_companies(
		Form::TYPE_HIDDEN
	);
	
	$form->selected_items(
		Form::TYPE_HIDDEN
	);
	
	$form->selected_tab(
		Form::TYPE_HIDDEN
	);
	
	$form->mps_mastersheet_year(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_mastersheet_id(
		Form::TYPE_SELECT,
		Form::PARAM_AJAX,
		Validata::PARAM_REQUIRED
	);
	
	$form->mps_ordersheet_comment(
		Form::TYPE_WYSIWYG,
		Form::PARAM_LABEL,
		'class=tinymce'		
	);
	
	$form->mps_ordersheet_openingdate(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::CLASS_DATAPICKER,
		Validata::PARAM_DATE
	);
	
	$form->mps_ordersheet_closingdate(
		Form::TYPE_TEXT,
		Form::TOOLTIP_DATE,
		Validata::CLASS_DATAPICKER,
		Validata::PARAM_DATE
	);
	
	$form->mps_ordersheet_workflowstate_id(
		Form::TYPE_SELECT
	);
	
	$form->listing(
		Form::TYPE_AJAX,
		Form::PARAM_SHOW_VALUE	
	);
	
	$form->fieldset('master_sheet', array(
		'mps_mastersheet_year',
		'mps_mastersheet_id',
		'mps_ordersheet_comment',
		'mps_ordersheet_openingdate',
		'mps_ordersheet_closingdate',
		'mps_ordersheet_workflowstate_id'
	));
	
	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	if ($hidden) {
		foreach ($hidden as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_HIDDEN);
		}
	}
	
	if ($buttons['back']) {
		$form->button(ui::button(array(
			'id' => 'back',
			'icon' => 'back',
			'href' => $buttons['back'],
			'label' => $translate->back
		)));
	}
	
	if ($buttons['delete_items']) {
		$form->button(ui::button(array(
			'id' => 'delete_items',
			'class' => 'dialog -items',
			'icon' => 'delete',
			'href' => $buttons['delete_items'],
			'label' => $translate->delete_items
		)));
	}
	
	if ($buttons['items']) {
		$form->button(ui::button(array(
			'id' => 'items',
			'class' => 'submit -items',
			'icon' => 'save',
			'href' => $buttons['items'],
			'label' => $translate->save_items
		)));
	}
	
	if ($buttons['delete_ordersheets']) {
		$form->button(ui::button(array(
			'id' => 'delete_ordersheets',
			'class' => 'dialog -ordersheets',
			'icon' => 'delete',
			'href' => $buttons['delete_ordersheets'],
			'label' => $translate->delete
		)));
	}
	
	if ($buttons['update']) {
		$form->button(ui::button(array(
			'id' => 'update',
			'class' => 'submit -ordersheets',
			'icon' => 'save',
			'href' => $buttons['update'],
			'label' => $translate->update
		)));
	}
	
	if ($buttons['add']) {
		$form->button(ui::button(array(
			'id' => 'add',
			'class' => 'submit -companies',
			'icon' => 'add',
			'href' => $buttons['add'],
			'label' => $translate->create
		)));
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	$form = $form->render();
?>
<style type="text/css">

	.ordersheet_form {
		display: block;
		width: 800px; 
	}
	
	#listing {
		display: block;
		width: 800px; 
		font-size: 1.2em;
	}
	
	h6 { 
		display:block; 
		margin-top: 20px;
		margin-bottom: 5px; 
		font-size: .8em;
	}
	
	h6:first-child {
		margin-top: 0;
	}
	
	h5 { 
		display:block;
		margin: 40px 0 20px; 
		font-size: .9em;
		border-bottom: 1px solid silver;
	}
	
	#listing table td {
		border-width: 0 0 1px 0;
	}
	
	
	#listing .xhr-placeholder {
		padding: 0 !important;
	}
	
	.ordersheet_form textarea { width:400px; height: 100px;}
	
	.mps_ordersheet_openingdate input {width:100px !important;}
	.mps_ordersheet_closingdate input {width:100px !important;}
	.textbox {width: 50px !important;}
	
</style>
<script type="text/javascript" src="/public/scripts/tinymce4/js/tinymce/tinymce.min.js"></script>
<div class=ordersheet_form >
	<?php echo $form; ?>
</div>
<?php 

	echo ui::dialogbox(array(
		'id' => 'delete_ordersheets_dialog',
		'title' => $translate->delete_ordersheets,
		'content' => $translate->dialog_delete_ordersheets,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'class' => 'dialog_submit',
				'label' => $translate->yes
			),
		)
	));

	echo ui::dialogbox(array(
		'id' => 'delete_items_dialog',
		'title' => $translate->delete_items,
		'content' => $translate->dialog_delete_items,
		'buttons' => array(
			'cancelItem' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'applyItem' => array(
				'icon' => 'apply',
				'class' => 'dialog_submit',
				'label' => $translate->yes
			),
		)
	));
?>