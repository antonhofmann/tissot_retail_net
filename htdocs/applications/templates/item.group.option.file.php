<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

	$user = User::instance();
	$translate = Translate::instance();

	$id = $_REQUEST['id'];
	$option = $_REQUEST['option'];
	$permission_edit = user::permission('can_edit_catalog');

	$model = new Model(Connector::DB_CORE);

	$itemGroupOptionFile = new Item_Group_Option_File();
	$itemGroupOptionFile->read($id);
	$data = $itemGroupOptionFile->data;

	$data['item_group_file_group_option'] = $option;
	$data['application'] = 'catalog';

	if (!$permission_edit) {
		foreach ($data as $key => $value) {
			$disabled[$key] = true;
		}
	}

	$result = $model->query("
		SELECT 
			file_purposes.file_purpose_id AS purpose,
			file_purposes.file_purpose_name AS caption
		FROM file_purposes
		ORDER BY file_purpose_name
	")->fetchAll();

	$dataloader['item_group_file_purpose'] = _array::extract($result);

	$data['file'] = '
		<span id="showfile" class="button">
			<span class="icon download"></span>
			<span class="label">Download</span>	
		</span>
		<span id="upload" class="button">
			<span class="icon upload"></span>
			<span class="label">Upload</span>	
		</span>
		<input type=hidden name="item_group_file_path" id="item_group_file_path" value="'.$data['item_group_file_path'].'" class="file_path required" />
		<input type="hidden" name="has_upload" id="has_upload" />
	';
	
	$form = new Form(array(
		'id' => 'form',
		'action' => '/applications/helpers/item.group.option.file.save.php',
		'method' => 'post',
		'class' => 'validator'
	));

	$form->application(
		Form::TYPE_HIDDEN
	);

	$form->controller(
		Form::TYPE_HIDDEN
	);

	$form->action(
		Form::TYPE_HIDDEN
	);

	$form->item_group_file_id(
		Form::TYPE_HIDDEN,
		'class=file_id'
	);	

	$form->item_group_file_group_option(
		Form::TYPE_HIDDEN
	);

	$form->item_group_file_title(
		Form::TYPE_TEXT,
		Validata::PARAM_REQUIRED,
		'class=file_title'
	);	

	$form->item_group_file_description(
		Form::TYPE_TEXTAREA,
		FORM::PARAM_LABEL,
		'class=description'
	);

	$form->item_group_file_purpose(
		Form::TYPE_SELECT,
		Validata::PARAM_REQUIRED
	);

	$form->file(
		Form::TYPE_AJAX,
		FORM::PARAM_SHOW_VALUE
	);

	$form->fieldset('File', array(
		'item_group_file_title',
		'item_group_file_description',
		'item_group_file_purpose',
		'file'
	));

	if ($disabled) {
		foreach ($disabled as $field => $value) {
			if ($value) $form->param($field, Form::PARAM_DISABLED);
		}
	}
	
	$form->dataloader($data);
	$form->dataloader($dataloader);
	
	$form = $form->render();

?>
<!doctype html>
<html lang="en">
<head>
	<title>Item Group Option File - Retail Net</title>
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link href="/public/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="/public/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/x-icon" />
	
	<script src="/public/scripts/jquery/1.7.2.js" type="text/javascript"></script>
	<script src="/public/scripts/jquery/migrate.min.js" type="text/javascript"></script>
	<script src="/public/scripts/jquery/jquery.ui.1.10.4.min.js"></script>
	<link type="text/css" href="/public/scripts/jquery/jquery.ui.1.10.4.min.css" rel="stylesheet" />
	
	<link rel="stylesheet" type="text/css" href="/public/scripts/fancybox/fancybox.css?v=2.1.5" />
	<script type="text/javascript" src="/public/scripts/fancybox/fancybox.js?v=2.1.5"></script>
	
	<script type="text/javascript" src="/public/scripts/jgrowl/jgrowl.js"></script>
	<link rel="stylesheet" type="text/css" href="/public/scripts/jgrowl/jgrowl.css" />
	
	<link href="/public/scripts/adomodal/adomodal.css" rel="stylesheet" type="text/css" />
	<script src="/public/scripts/adomodal/adomodal.js" type="text/javascript"></script> 
	<script type="text/javascript" src="/public/scripts/jquery.actual.min.js"></script>
	<link rel="stylesheet" href="/public/css/spinners.css">
	
	<script src="/public/scripts/master.js" type="text/javascript"></script>
	<script type="text/javascript" src="/public/scripts/message.js"></script>
	<script type="text/javascript" src="/public/scripts/retailnet.js"></script>

	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/modal.css" />
	<link rel="stylesheet" href="/public/scripts/bootstrap/css/bootstrap.compiled.css">
	<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
	
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/modal.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/reset.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/layout.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/table.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/form.css" />
	<link rel="stylesheet" type="text/css" href="/public/css/gui.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/gui.css" />
	
	<script type="text/javascript" src="/public/scripts/ajaxuploader/ajaxupload.js"></script>
	<link rel="stylesheet" type="text/css" href="/public/scripts/validationEngine/css/validationEngine.jquery.css" />
	<script type="text/javascript" src="/public/scripts/validationEngine/js/languages/jquery.validationEngine-en.js"></script>
	<script type="text/javascript" src="/public/scripts/validationEngine/js/jquery.validationEngine.js"></script>
	<link rel="stylesheet" type="text/css" href="/public/scripts/qtip/qtip.css" />
	<script type="text/javascript" src="/public/scripts/qtip/qtip.js"></script>
	<script type="text/javascript" src="/public/js/form.validator.file.js"></script>

	<style type="text/css">
	
		body,
		body.adomat {
			background: #eeeeee !important;
			min-width: 400px !important;
		}
		
		.modalbox-container {
			display: block;
			height: 580px;
		}
		
		textarea.description {
			width: 440px;
			height: 100px;
		}

		#showfile {
			margin: 0 10px 0 0 !important;
		}

		#upload {
			margin: 0 !important;
		}
		
	</style>

	<script type="text/javascript">
		
		$(document).ready(function() {

			// close modal screen
			$('.close', $('#modalbox')).click(function(e) {
				
				e.preventDefault();
				e.stopImmediatePropagation();
				
				parent.retailnet.modal.close();
				
				return false;
			});
		});
	</script>
	
</head>
<body class="adomat modal-iframe">

	<div class="modalbox-container">
		<div id="modalbox" class="adomat modalbox">
			<div class="modalbox-header">
				<div class="title">File Upload</div>
			</div>
			<div class="modalbox-content-container">
				<div class="modalbox-content"><?php echo $form; ?></div>
			</div>
			<div class="modalbox-footer">
				<div class="modalbox-actions">
					
					<a class="button close cancel"> 
						<span class="icon cancel"></span>
						<span class="label">Close</span>
					</a>

					<?php
						if ($permission_edit) {
							
							if ($data['item_group_file_id']) {
								echo ui::button(array(
									'id' => 'delete',
									'href' => "/applications/helpers/item.group.option.file.delete.php?id={$data[item_group_file_id]}",
									'class' => 'dialog',
									'icon' => 'delete',
									'label' => $translate->delete
								));
							}
							
							echo ui::button(array(
								'icon' => 'save',
								'id' => 'save',
								'label' => $translate->save
							));							
						}
					?>
				</div>
			</div>
		</div>
	</div>
<?php
	echo ui::dialogbox(array(
		'id' => 'delete_dialog',
		'title' => $translate->delete,
		'content' => $translate->dialog_delete_record,
		'buttons' => array(
			'cancel' => array(
				'icon' => 'cancel',
				'label' => $translate->cancel
			),
			'apply' => array(
				'icon' => 'apply',
				'label' => $translate->yes
			)
		)
	));
?>
</body>
</html>