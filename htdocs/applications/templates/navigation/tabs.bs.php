<?php 

	$request = request::instance();
	$params = $params && is_array($params) ? '/'.join('/', $params) : null;
	$style = $style ?: 'nav-tabs';

	if (count($tabs) > 1) { 
			
		echo "<ul id='$id' class='nav $style'>";
	
		foreach ($tabs as $id => $row) {
				
			if ($row['tab'] && $row['active']) {
				
				$url = $row['url'];
				$caption = $row['caption'];
				
				$visible = (in_array($url, $request->excluded)) ? false : true;
				$classname = str_replace('/', '-', $url);
				$active = (url::system_request()==$url) ? "active" : null;		
				
				if ($visible) {
					$icon = $data[$url]['icon'];
					$class = $data[$url]['class'];
					$caption = ($data[$url]['caption']) ? $data[$url]['caption'] : $caption;
					echo "<li role='presentation' class='$active'>";
					echo "<a href='/$url$params' class='$class'>$icon<b>$caption</b></a>";
					echo "</li>";
				}
			}
		}
	
		echo "</ul>";
	}

?>