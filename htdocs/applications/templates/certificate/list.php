<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<link href="/public/scripts/dropdown/dropdown.css" rel="stylesheet" type="text/css" />
<script src="/public/scripts/dropdown/dropdown.js" type="text/javascript"  ></script>
<script type="text/javascript" src="/public/scripts/table.loader.js" ></script>
<script type="text/javascript">
$(document).ready(function() {

	$('#items').tableLoader({
		url: '/applications/modules/cerificates/list.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			$('select',self).dropdown();

		}
	});

});
</script>
<style type="text/css">
	
	#items { width: 1200px; }

	.fa-stack {
		color: #3276b1;
	}

	.infobox {
		/*min-width: 30px;*/
	}

	.bootstrap .infobox { padding: 1px 5px 1px 5px; border-radius:50px;}

	.infobox i { font-size: 16px; padding-top: 2px;}

	.bootstrap { background-color: transparent !important;}

	.btn-glyphicon { padding:8px; background:#ffffff; margin-right:4px; }
	.icon-btn { padding: 1px 15px 3px 2px; border-radius:50px;}

</style>
<div id="items"></div>
<?php echo $request->form(); ?>