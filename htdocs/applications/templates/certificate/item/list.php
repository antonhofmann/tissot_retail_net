<?php 
	$request = request::instance();
	$translate = Translate::instance();
?>
<script type="text/javascript">
$(document).ready(function() {

	$("input.item_certificate").on("change", function() {

		var data = $('form.request').serializeArray();

		data.push({
			name:'item', 
			value: $(this).val()
		});
		
		data.push({
			name:'checked', 
			value: $(this).is(':checked') ? 1 : 0
		});

		retailnet.ajax.json('/applications/modules/cerificates/item/submit.php', data).done(function(xhr) {
			if (xhr.message) {
				if (xhr.response) retailnet.notification.success(xhr.message);
				else retailnet.notification.error(xhr.message);
			}
		}); 
	});
});
</script>
<style type="text/css">

	#items { 
		width: 700px; 
	}
	
</style>
<div id="items">
	<?php
		
		if ($items) {
			$table = new Table();
			$table->datagrid = $items;
			$table->checkbox('width=20px');
			$table->item_code();
			$table->item_name();
			echo $table->render();
		}

		echo $request->form();
	?>
</div>
<div class='actions'>
<?php 
	echo ui::button(array(
		'id' => 'back',
		'icon' => 'back',
		'href' => $buttons['back'],
		'label' => $translate->back
	));
?>
</div>