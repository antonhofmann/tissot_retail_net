<?php
/********************************************************************

    traffic_by_ip.php

    Enter parameters for the query of traffic

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-04-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-04-22
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");



/********************************************************************
    prepare all data needed
*********************************************************************/
$sql_ips = "select distinct statistic_user, address_shortcut, ".
           "concat(user_name, ' ', user_firstname) as user_fullname ".
           "from statistics " .
           "left join users on user_id = statistic_user " .
           "left join addresses on address_id = user_address";

$duration = array();
$script = array();

$sql = "SELECT statistic_user, statistic_url, " .
       "max(statistic_duration )  AS slowest " .
       "FROM statistics " .
       "WHERE statistic_user > 0 and statistic_date >  '2005-04-25' " .
       "GROUP  BY statistic_user, statistic_url " .
       "ORDER  BY statistic_user, statistic_url, slowest DESC ";


$max = 0;
$group = "";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    if($group != $row["statistic_user"])
    {
        $max = 0;
        $group = $row["statistic_user"];
    }
    if($max < $row["slowest"])
    {
        
        $duration[$row["statistic_user"]] = $row["slowest"];

        $script[$row["statistic_user"]] = $row["statistic_url"];
        $max = $row["slowest"];
    }
}

// ip list
$list = new ListView($sql_ips, LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("statistics");
$list->set_order("user_fullname");
$list->set_filter("statistic_user > 0 and statistic_user <>''");

$list->add_column("user_fullname", "User", "traffic_by_ip_detail.php", "", "", COLUMN_NO_WRAP);
$list->add_column("address_shortcut", "Company", "", "", "", COLUMN_NO_WRAP);
$list->add_text_column("slowest", "Slowest", COLUMN_NO_WRAP | COLUMN_ALIGN_RIGHT, $duration);
$list->add_text_column("url", "URL", COLUMN_NO_WRAP, $script);


$list->process();


/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("traffic");

require "include/stats_page_actions.php";

$page->header();
$page->title("Traffic by IP since Monday 25th April 2005");

$list->render();

$page->footer();

?>