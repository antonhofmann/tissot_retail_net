<?php
/********************************************************************

    welcome.php

    Main entry page after successful login.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-04-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-04-22
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");


$page = new Page("traffic");

require_once "include/stats_page_actions.php";

$page->header();
echo "<p>Welcome to the Traffic Analysis of " . APPLICATION_NAME . ".</p>";


$page->footer();

?>