<?php
/********************************************************************

    project_view_material_list_supplier.php

    View List of Materials for Supplier's Use Only

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-01-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-04
    Version:        1.1.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_edit_his_list_of_materials_in_projects");

register_param("pid");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get user data
$user_data = get_user(user_id());

// get company's address
$client_address = get_address($project["order_client_address"]);

// get Supplier currency
//$supplier_currency = get_address_currency($user_data["address"]);

// create sql for order items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, order_item_order_revisions, ".
				   "    order_item_po_number, order_item_system_price, order_item_client_price, item_id, " . 
				   "    order_item_supplier_address, ".
				   "    order_item_forwarder_address, " . 
				   "    if(order_item_not_in_budget=1 , 'no', 'yes') as order_item_in_budget, ".
				   "    TRUNCATE(order_item_quantity * order_item_system_price, 2) as total_price, ".
				   "    TRUNCATE(order_item_quantity * order_item_client_price, 2) as total_price_loc, ".
				   "    if(item_code <>'', item_code, item_type_name) as item_shortcut, ".
				   "    order_item_supplier_freetext, order_item_exclude_from_ps, ".
				   "    addresses.address_shortcut as supplier_company, ".
				   "    addresses_1.address_shortcut as forwarder_company, ".
				   "    item_type_id, item_type_name, ".
				   "    item_type_priority, " . 
				   "    concat(project_cost_groupname_name, ' - ', item_category_name) as cat_name, " .
				   "    order_item_type,  ".
				   "    item_stock_property_of_swatch, unit_name, order_item_client_price_freezed, " . 
				   " order_item_supplier_exchange_rate, project_cost_groupname_name " .
				   "from order_items ".
				   "left join items on order_item_item = item_id ".
				   "left join item_categories on item_category_id = item_category " .
				   "left join addresses on order_item_supplier_address = addresses.address_id ".
				   "left join addresses as addresses_1 ".
				   "     on order_item_forwarder_address = addresses_1.address_id ".
				   "left join item_types on order_item_type = item_type_id " .
				   "left join project_cost_groupnames on project_cost_groupname_id = order_item_cost_group " .
				   "left join units on unit_id = order_item_unit_id";

$where_clause = " where (order_item_type = " . ITEM_TYPE_STANDARD . 
                "    or order_item_type = " . ITEM_TYPE_SPECIAL . ") ".
                "    and order_item_order = " . $project["project_order"] .
				"    and order_item_supplier_address = " . $user_data["address"] ;

$values = array();
$res = mysql_query($sql_order_items . $where_clause) or dberror($sql_order_items . $where_clause);
while ($row = mysql_fetch_assoc($res))
{
    $values[$row["order_item_id"]] = $row["order_item_quantity"];
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);

require_once "include/project_head_small.php";

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create List for Catalog Items
*********************************************************************/ 
$list1 = new ListView($sql_order_items, LIST_HAS_HEADER);

$list1->set_title("Catalog Items");
$list1->set_entity("order_item");
$list1->set_filter("order_item_type = " . ITEM_TYPE_STANDARD . " and order_item_order = " . $project["project_order"] . " and order_item_supplier_address = " . $user_data["address"]);
$list1->set_order("item_category_name, item_code");
$list1->set_group("item_category_name");

$list1->add_column("item_shortcut", "Item Code");

$list1->add_column("order_item_text", "Name");

$list1->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$list1->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);

// attention: curreny of suppliers price is defined in table suppliers
$list1->add_column("order_item_supplier_price", "Price", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);


$list1->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("forwarder_company", "Forwarder");

/********************************************************************
    Create List for Special Items
*********************************************************************/ 
$list2 = new ListView($sql_order_items, LIST_HAS_HEADER);

$list2->set_title("Special Items");
$list2->set_entity("order_item");
$list2->set_filter("order_item_type = " . ITEM_TYPE_SPECIAL . " and order_item_order = " . $project["project_order"] . " and order_item_supplier_address = " . $user_data["address"]);
$list2->set_order("order_item_text");

$list2->add_column("item_shortcut", "Item Code");

$list2->add_column("order_item_text", "Name");

$list2->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);

// attention: curreny of suppliers price is defined in table suppliers
$list2->add_column("order_item_supplier_price", "Price", "", "", "", COLUMN_ALIGN_RIGHT);


$list2->add_column("order_item_po_number", "P.O. Number");
$list2->add_column("forwarder_company", "Forwarder");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();

$list2->populate();
$list2->process();

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("View Supplier's List of Materials");
$form->render();
$list1->render();
$list2->render();
$page->footer();

?>