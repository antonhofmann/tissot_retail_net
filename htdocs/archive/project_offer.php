<?php
/********************************************************************

    project_offer.php

    Add a new or Edit an Offer for Local Construction Works

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-09
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/offer_functions.php";
require_once "include/save_functions.php";
check_access("can_edit_local_constrction_work");


if(!param("pid"))
{
	register_param("pid");
	param("pid", id());
}
// read project and order details
$project = get_project(param("pid"));

set_referer("project_offer_position.php");

$currency = get_address_currency($project["order_client_address"]);


// Changes 2008
// reduction to groups, no more editing of single offer positions
// leave the position editing in case it will be used/activated at a leter time

//offer positions
/*
$sql = "select lwofferposition_id, lwofferposition_lwoffergroup, lwofferposition_code, " .
       "lwofferposition_text, lwofferposition_action, lwofferposition_unit, " . 
       "lwofferposition_numofunits, lwofferposition_price, " . 
       "lwofferposition_numofunits_billed, lwofferposition_price_billed, " .
       "lwofferposition_hasoffer, lwofferposition_inbudget, " .
       "concat(lwoffergroup_code,  ' ', lwoffergroup_text) as lwoffergroup " .
       "from lwofferpositions " .       
       "left join lwoffergroups on lwoffergroup_id = lwofferposition_lwoffergroup ";

$list_filter = "lwoffergroup_offer = " . id();


$num_of_units = array();
$price = array();
$totals = array();

$num_of_units_billed = array();
$price_billed = array();
$totals_billed = array();

$hasoffers = array();
$inbudget = array();

$res = mysql_query($sql . " where " . $list_filter) or dberror($sql_order_items . " where " . $list_filter);
while ($row = mysql_fetch_assoc($res))
{
    $num_of_units[$row["lwofferposition_id"]] = $row["lwofferposition_numofunits"];
    $price[$row["lwofferposition_id"]] = $row["lwofferposition_price"];

    $totals[$row["lwofferposition_id"]] = number_format($row["lwofferposition_price"]*$row["lwofferposition_numofunits"],2, ".", "'");

    $num_of_units_billed[$row["lwofferposition_id"]] = $row["lwofferposition_numofunits_billed"];
    $price_billed[$row["lwofferposition_id"]] = $row["lwofferposition_price_billed"];

    $totals_billed[$row["lwofferposition_id"]] = number_format($row["lwofferposition_price_billed"]*$row["lwofferposition_numofunits_billed"],2, ".", "'");

    
    
    if ($project["order_budget_is_locked"] == 1)
    {
        
        if($row["lwofferposition_hasoffer"] == 1)
        {
            $hasoffers[$row["lwofferposition_id"]] = "yes";
        }
        else
        {
            $hasoffers[$row["lwofferposition_id"]] = "no";
        }

        if($row["lwofferposition_inbudget"] == 1)
        {
            $inbudget[$row["lwofferposition_id"]] = "yes";
        }
        else
        {
            $inbudget[$row["lwofferposition_id"]] = "no";
        }

    }
    else
    {
        $hasoffers[$row["lwofferposition_id"]] = $row["lwofferposition_hasoffer"];
        $inbudget[$row["lwofferposition_id"]] = $row["lwofferposition_inbudget"];
    }

}


//build totals
$sql_a = "select lwoffer_id, " . 
       "sum(lwofferposition_numofunits*lwofferposition_price) as total " .
       "from lwoffers " .
       "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " . 
       "left join lwofferpositions on lwofferposition_lwoffergroup = lwoffergroup_id " . 
       "where lwoffer_id = " . id() .
       " GROUP BY lwoffer_id ";

$offered = 0;
$res_a = mysql_query($sql_a) or dberror($sql_a);
if ($row_a = mysql_fetch_assoc($res_a))
{
    $offered = number_format($row_a["total"],2, ".", "'");

}

$sql_a = "select lwoffer_id, " . 
       "sum(lwofferposition_numofunits*lwofferposition_price) as total " .
       "from lwoffers " .
       "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " . 
       "left join lwofferpositions on lwofferposition_lwoffergroup = lwoffergroup_id " . 
       "where lwofferposition_inbudget = 1 and lwoffer_id = " . id() .
       " GROUP BY lwoffer_id ";

$budget = 0;
$res_a = mysql_query($sql_a) or dberror($sql_a);
if ($row_a = mysql_fetch_assoc($res_a))
{
    $budget = number_format($row_a["total"],2, ".", "'");

}

$sql_a = "select lwoffer_id, " . 
       "sum(lwofferposition_numofunits_billed*lwofferposition_price_billed) as total " .
       "from lwoffers " .
       "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " . 
       "left join lwofferpositions on lwofferposition_lwoffergroup = lwoffergroup_id " . 
       "where lwofferposition_price_billed <> 0 " . 
       "  and lwofferposition_price_billed is not NULL " . 
       "  and lwoffer_id = " . id() .
       " GROUP BY lwoffer_id ";

$real = 0;
$res_a = mysql_query($sql_a) or dberror($sql_a);
if ($row_a = mysql_fetch_assoc($res_a))
{
    $real = number_format($row_a["total"],2, ".", "'");

}
*/


//offer groups
$sql = "select lwoffergroup_id, lwoffergroup_offer, lwoffergroup_group, lwoffergroup_hasoffer, lwoffergroup_inbudget, " .
       "lwoffergroup_action, lwoffergroup_code, lwoffergroup_text, lwoffergroup_price, lwoffergroup_price_billed, " .
	   "lwoffergroup_discount " . 
	   "from lwoffergroups ";

$list_filter = "lwoffergroup_offer = " . dbquote(id());

$hasoffers = array();
$inbudget = array();
$price = array();
$price_billed = array();

$res = mysql_query($sql . " where " . $list_filter) or dberror($sql_order_items . " where " . $list_filter);
while ($row = mysql_fetch_assoc($res))
{
    $price[$row["lwoffergroup_id"]] = $row["lwoffergroup_price"];
	$price_billed[$row["lwoffergroup_id"]] = $row["lwoffergroup_price_billed"];

	if ($project["order_budget_is_locked"] == 1)
    {
        
        if($row["lwoffergroup_hasoffer"] == 1)
        {
            $hasoffers[$row["lwoffergroup_id"]] = "yes";
        }
        else
        {
            $hasoffers[$row["lwoffergroup_id"]] = "no";
        }

        if($row["lwoffergroup_inbudget"] == 1)
        {
            $inbudget[$row["lwoffergroup_id"]] = "yes";
        }
        else
        {
            $inbudget[$row["lwoffergroup_id"]] = "no";
        }

    }
    else
    {
        $hasoffers[$row["lwoffergroup_id"]] = $row["lwoffergroup_hasoffer"];
        $inbudget[$row["lwoffergroup_id"]] = $row["lwoffergroup_inbudget"];
    }
}

//build totals
$sql_a = "select lwoffer_id, currency_symbol, lwoffer_exchange_rate, lwoffer_factor, " . 
		 "sum(lwoffergroup_price) as total " .
		 "from lwoffers " .
		 "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " .
		 "left join currencies on currency_id = lwoffer_currency " . 
		 "where lwoffer_id = " . id() .
         " GROUP BY lwoffer_id, currency_symbol,lwoffer_exchange_rate,lwoffer_factor ";

$offered = 0;
$offer_currency_symbol = "";
$offer_currency_exchangerate = 0;
$offer_currency_factor = 1;
$res_a = mysql_query($sql_a) or dberror($sql_a);
if ($row_a = mysql_fetch_assoc($res_a))
{
    $offered = number_format($row_a["total"],2, ".", "'");
	$offer_currency_symbol = $row_a["currency_symbol"];
	$offer_currency_exchangerate = $row_a["lwoffer_exchange_rate"];
	$offer_currency_factor = $row_a["lwoffer_factor"];

}

$sql_a = "select lwoffer_id, " . 
         "sum(lwoffergroup_price) as total " .
         "from lwoffers " .
         "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " . 
         "where lwoffergroup_inbudget = 1 and lwoffer_id = " . id() .
         " GROUP BY lwoffer_id ";

$budget = 0;
$res_a = mysql_query($sql_a) or dberror($sql_a);
if ($row_a = mysql_fetch_assoc($res_a))
{
    $budget = $row_a["total"]*$offer_currency_exchangerate/$offer_currency_factor;
	$budget = number_format($budget,2, ".", "'");
}

$sql_a = "select lwoffer_id, " . 
         "sum(lwoffergroup_price_billed) as total " .
         "from lwoffers " .
         "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " . 
         "where lwoffergroup_price_billed <> 0 " . 
         "  and lwoffergroup_price_billed is not NULL " . 
         "  and lwoffer_id = " . id() .
         " GROUP BY lwoffer_id ";

$real = 0;
$res_a = mysql_query($sql_a) or dberror($sql_a);
if ($row_a = mysql_fetch_assoc($res_a))
{
	$real = $row_a["total"]*$offer_currency_exchangerate / $offer_currency_factor;
	$real = number_format($real,2, ".", "'");

}

/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("lwoffers", "Offer for Local Construction Works");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("lwoffer_order", $project["project_order"]);
$form->add_hidden("lwoffer_owner", user_id());
$form->add_hidden("lwoffer_id", id());
$form->add_hidden("lwoffer_order_item");

if ($project["order_budget_is_locked"] == 1)
{
    $form->error = "Budget is locked, offered quantities and prices can not be changed anymore.";
}

if(!id())
{
    
    $tmp_text1="Please indicate the constructor address.";
    $tmp_text2="\nYou can either select an existing address or enter a new address.";
    $form->add_comment($tmp_text1 . $tmp_text2);
    $form->add_comment("");

    /*
	$sql_p = "select lwoffer_id, concat(lwoffer_company, ' ' , lwoffer_place) as company " .
             "from lwoffers " . 
             "where lwoffer_owner = " . user_id() . 
             " order by lwoffer_company";
    */

	$sql_p = "select lwoffer_id, lwoffer_company as company " .
             "from lwoffers " . 
             "where lwoffer_owner = " . user_id() . 
             " order by lwoffer_company";

    $form->add_list("adrselect", "Preselect", $sql_p, SUBMIT);
}

$form->add_edit("lwoffer_date", "Date*", NOTNULL, "", TYPE_DATE);
//$form->add_edit("lwoffer_number", "Offer Number");
$form->add_edit("lwoffer_company", "Company*", NOTNULL);
//$form->add_edit("lwoffer_company2", "");
//$form->add_edit("lwoffer_address", "Address");
//$form->add_edit("lwoffer_address2", "");
//$form->add_edit("lwoffer_zip", "Zip", 0);
//$form->add_edit("lwoffer_place", "City*", NOTNULL);
//$form->add_list("lwoffer_country", "Country", "select country_id, country_name from countries order by country_name");

//$form->add_edit("lwoffer_contact", "Contact's Name");
//$form->add_edit("lwoffer_phone", "Phone");
//$form->add_edit("lwoffer_mobile_phone", "Mobile Phone");
//$form->add_edit("lwoffer_email", "Email");

$form->add_label("dummy", " ", 0, " ");

$form->add_list("lwoffer_currency", "Currency*",
    "select currency_id, currency_symbol from currencies order by currency_symbol", NOTNULL);

$form->add_label("exchangerate", "Exchange Rate ", 0, $offer_currency_exchangerate);

$form->add_label("offered", "Offered Cost " . $offer_currency_symbol, 0, $offered);
//$form->add_label("budget", "In Client's Budget " . $currency["symbol"], 0, $budget);
//$form->add_label("real", "Real Cost " . $currency["symbol"], 0, $real);
$form->add_label("budget", "In Client's Budget CHF", 0, $budget);
$form->add_label("real", "Real Cost CHF", 0, $real);


//$form->add_checkbox("lwoffer_accepted", "Part of Offer or Offer Accepted", false);

//$form->add_label("dummy1", " ", 0, " ");

$form->add_multiline("lwoffer_remark", "Remarks", 6);


$form->add_button("back", "Back");



if(id())
{
    
    /*
	if ($project["order_budget_is_locked"] == 0)
    {
        $link = "project_offer_xls_empty.php?pid=" . param("pid") . "&lwoid=" . id(); 
        $link = "javascript:popup('". $link . "', 800, 600)";
        $form->add_button("pdf_empty", "Generate Offer Form", $link);
    }

    $link = "project_offer_pdf.php?pid=" . param("pid") . "&lwoid=" . id(); 
    $link = "javascript:popup('". $link . "', 800, 600)";
    $form->add_button("pdf", "Print Offer", $link);
	*/
}



if ($project["order_budget_is_locked"] == 1)
{
}
else
{
	//$form->add_button(FORM_BUTTON_DELETE, "Delete Offer", "", OPTIONAL);
	//$form->add_button("save_form", "Save");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();
$form->process();

/********************************************************************
    Create List Offer postions
*********************************************************************/
/*
$list = new ListView($sql);

$list->set_entity("lwofferpositions");
$list->set_filter($list_filter);
$list->set_group("lwoffergroup");
$list->set_order("lwofferposition_code");


if ($project["order_budget_is_locked"] == 0)
{
    $list->add_column("lwofferposition_code", "Code", "project_offer_position.php?lwoid=" . id()  . "&pid=" . param("pid"));
    $list->add_checkbox_column("lwofferposition_hasoffer", "to be offered", 0, $hasoffers);
    $list->add_checkbox_column("lwofferposition_inbudget", "in budget", 0, $inbudget);
}
else
{
    $list->add_column("lwofferposition_code", "Code");
    $list->add_text_column("lwofferposition_hasoffer", "to be offered", 0, $hasoffers);
    $list->add_text_column("lwofferposition_inbudget", "in budget", 0, $inbudget);
}


$list->add_column("lwofferposition_text", "Description");
$list->add_column("lwofferposition_action", "Action", "", "", "", COLUMN_NO_WRAP);
$list->add_column("lwofferposition_unit", "Unit", "", "", "", COLUMN_NO_WRAP);

if ($project["order_budget_is_locked"] == 1)
{
    $list->add_text_column("lwofferposition_numofunits", "Quantity\noffered", COLUMN_ALIGN_RIGHT, $num_of_units);
    $list->add_text_column("lwofferposition_price", "Price\noffered", COLUMN_ALIGN_RIGHT, $price);
}
else
{
    
    $list->add_edit_column("lwofferposition_numofunits", "Quantity\noffered", "6", 0, $num_of_units);
    $list->add_edit_column("lwofferposition_price", "Price\noffered", "8", 0, $price);
}


$list->add_text_column("total", "Total\noffered", COLUMN_ALIGN_RIGHT, $totals);

$list->add_edit_column("lwofferposition_numofunits_billed", "Quantity\nbilled", "6", 0, $num_of_units_billed);


$list->add_edit_column("lwofferposition_price_billed", "Price\nbilled", "8", 0, $price_billed);
$list->add_text_column("total", "Total\nbilled", COLUMN_ALIGN_RIGHT, $totals_billed);


if(id())
{
    if ($project["order_budget_is_locked"] == 0)
    {
        $list->add_button(LIST_BUTTON_NEW, "New Offer Position", "project_offer_position.php?lwoid=" . id() . "&pid=" . param("pid"));
    }
    $list->add_button("save_list", "Save List");

}

$list->populate();
$list->process();

*/

/********************************************************************
    Create List Offer groups
*********************************************************************/
$list = new ListView($sql, LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("lwoffergroups");
$list->set_filter($list_filter);
$list->set_order("lwoffergroup_code");

if ($project["order_budget_is_locked"] == 0)
{
    $list->add_column("lwoffergroup_code", "Code", "project_offer_group.php?lwoid=" . id()  . "&pid=" . param("pid"));
    //$list->add_checkbox_column("lwoffergroup_hasoffer", "offered", 0, $hasoffers);
    $list->add_checkbox_column("lwoffergroup_inbudget", "in budget", COLUMN_ALIGN_CENTER, $inbudget);
}
else
{
    $list->add_column("lwoffergroup_code", "Code");
    //$list->add_text_column("lwoffergroup_hasoffer", "offered", 0, $hasoffers);
    $list->add_text_column("lwoffergroup_inbudget", "in budget", COLUMN_ALIGN_CENTER, $inbudget);
}


$list->add_column("lwoffergroup_text", "Description");
$list->add_column("lwoffergroup_action", "Action by", "", "", "", COLUMN_NO_WRAP);

if ($project["order_budget_is_locked"] == 1)
{
    $list->add_text_column("lwoffergroup_price", "Price\noffered", COLUMN_ALIGN_RIGHT, $price);
}
else
{
    
    $list->add_edit_column("lwoffergroup_price", "Price\noffered", "8", 0, $price);
}


//$list->add_edit_column("lwoffergroup_price_billed", "Price\nbilled", "8", 0, $price_billed);
$list->add_column("lwoffergroup_discount", "Discount in %", "", "", "", COLUMN_NO_WRAP);

if(id())
{
    if ($project["order_budget_is_locked"] == 0)
    {
        $list->add_button(LIST_BUTTON_NEW, "New Offer Group", "project_offer_group.php?lwoid=" . id() . "&pid=" . param("pid"));
		$list->add_button("save_list", "Save List");
    }
}

$list->populate();
$list->process();




if ($form->button("back"))
{
    $link = "project_offers.php?pid=" . param("pid");
    redirect ($link);
}
elseif ($form->button("save_form"))
{
    //$form->add_validation("{lwoffer_company}", "The company name must not be empty!");
    //$form->add_validation("{lwoffer_place}", "The city must not be empty!");
    //$form->add_validation("{lwoffer_currency}", "The currency must not be empty!");

    if ($form->validate())
    {
        if(id()) // update
        {

            $form->save();
            $form->message("The data has been saved.");
            $offer_id = id();

            //update_offer_positions($list, $form);
			if ($project["order_budget_is_locked"] == 0)
			{
				update_offer_groups($list, $form);
			}
        }
        else // insert new
        {

            $form->save();
            $form->message("The data has been saved.");
            $offer_id = mysql_insert_id($db);
        
            // insert standard offer positions
            $sql = "select * from lwgroups order by lwgroup_group, lwgroup_code";

            $res = mysql_query($sql) or dberror($sql);


            while($row = mysql_fetch_assoc($res))
            {
                $sql_u = "insert into lwoffergroups (" . 
                         "lwoffergroup_offer, " .
                         "lwoffergroup_group, " .
                         "lwoffergroup_code, " .
                         "lwoffergroup_text, " .
                         "user_created, " .
                         "date_created, " .
                         "user_modified, " .
                         "date_modified " .
                         ") values (" .
                         $offer_id . ", " .
                         dbquote($row["lwgroup_group"]) .  ", " .
                         dbquote($row["lwgroup_code"]) .  ", " .
                         dbquote($row["lwgroup_text"]) .  ", " .
                         user_id() . ", '" .
                         date("Y-m-d")  .  "', " .
                         user_id() . ", '" .
                         date("Y-m-d")  .  "')";
                
                $res_u = mysql_query($sql_u) or dberror($sql_u);

                $last_id = mysql_insert_id($db);


                /* Offer Positions
				$sql_i = "select * from lwitems " .
                         "where lwitem_group = " . $row["lwgroup_id"] .
                         " order by lwitem_code";
                
                $res_i = mysql_query($sql_i) or dberror($sql_i);

                while($row_i = mysql_fetch_assoc($res_i))
                {
                    $sql_u = "insert into lwofferpositions (" . 
                             "lwofferposition_lwoffergroup, " .
                             "lwofferposition_code, " .
                             "lwofferposition_text, " .
                             "lwofferposition_action, " .
                             "lwofferposition_unit, " .
                             "user_created, " .
                             "date_created, " .
                             "user_modified, " .
                             "date_modified " .
                             ") values (" .
                             $last_id . ", " .
                             dbquote($row_i["lwitem_code"]) .  ", " .
                             dbquote($row_i["lwitem_text"]) .  ", " .
                             dbquote($row_i["lwitem_action"]) .  ", " .
                             dbquote($row_i["lwitem_unit"]) .  ", " .
                             user_id() . ", '" .
                             date("Y-m-d")  .  "', " .
                             user_id() . ", '" .
                             date("Y-m-d")  .  "')";
                    
                    $res_u = mysql_query($sql_u) or dberror($sql_u);
                }
				*/
            }
            
        }

		//update currency values
		if ($project["order_budget_is_locked"] == 0)
		{
			$currency = get_currency($form->value("lwoffer_currency"));

			$sql = "update lwoffers set " .
				   "lwoffer_exchange_rate = " . $currency["exchange_rate"] . ", " . 
				   "lwoffer_factor = " . $currency["factor"] .
				   " where lwoffer_id = " . id();

			$result = mysql_query($sql) or dberror($sql);
		}

		$link = "project_offer.php?id=" . $offer_id . "&pid=" . param("pid");
		redirect ($link);

    }

	

    

}
elseif ($list->button("save_list"))
{
    //update_offer_positions($list, $form);
	update_offer_groups($list, $form, 7);

	update_exchange_rates($project["project_order"]);

    $link = "project_offer.php?id=" . id() . "&pid=" . param("pid");
    redirect ($link);
}
elseif($form->button("adrselect"))
{
    
	// set new billing address
    $form->value("lwoffer_company", "");
    /*
	$form->value("lwoffer_company2",  "");
    $form->value("lwoffer_address",  "");
    $form->value("lwoffer_address2",  "");
    $form->value("lwoffer_zip",  "");
    $form->value("lwoffer_place",  "");
    $form->value("lwoffer_country",  0);
    $form->value("lwoffer_phone",  "");
    $form->value("lwoffer_mobile_phone",  "");
    $form->value("lwoffer_email",  "");
    $form->value("lwoffer_contact",  "");
	*/
    $form->value("lwoffer_currency",  0);

    if ($form->value("adrselect"))
    {
        $sql = "select * from lwoffers where lwoffer_id = " . $form->value("adrselect");
        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $form->value("lwoffer_company", $row["lwoffer_company"]);
            /*
			$form->value("lwoffer_company2",  $row["lwoffer_company2"]);
            $form->value("lwoffer_address",  $row["lwoffer_address"]);
            $form->value("lwoffer_address2",  $row["lwoffer_address2"]);
            $form->value("lwoffer_zip",  $row["lwoffer_zip"]);
            $form->value("lwoffer_place",  $row["lwoffer_place"]);
            $form->value("lwoffer_country",  $row["lwoffer_country"]);
            $form->value("lwoffer_phone",  $row["lwoffer_phone"]);
            $form->value("lwoffer_mobile_phone",  $row["lwoffer_mobile_phone"]);
            $form->value("lwoffer_email",  $row["lwoffer_email"]);
            $form->value("lwoffer_contact",  $row["lwoffer_contact"]);
			*/
            $form->value("lwoffer_currency",  $row["lwoffer_currency"]);
        }
    }
}


/********************************************************************
    Populate list and process button clicks
*********************************************************************/



$page = new Page("projects");
require "include/project_page_actions.php";

$page->header();
$page->title(id() ? "Edit Offer for Local Construction Works" : "Add Offer for Local Construction Works");
$form->render();

echo "<br>";
$list->render();

$page->footer();
?>