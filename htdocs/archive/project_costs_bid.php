<?php
/********************************************************************

    project_costs_costsheet_bids.php

    View or edit bids for a project

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2012, OMEGA SA, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/project_cost_functions.php";

if (has_access("can_view_project_costs") 
   or has_access("can_view_budget_in_projects")
   or  has_access('can_edit_project_costs_bids')
   )
{ 
}
else {
	redirect("noaccess.php");
}


if(!param("pid"))
{
	$link = "../user/welcome.php";
	redirect($link);
}



/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));
// get company's address
$client_address = get_address($project["order_client_address"]);
$order_currency = get_order_currency($project["project_order"]);

$currency_symbol = '';
if(id() > 0)
{
	$currency = get_bid_currency(id());
	$currency_symbol = $currency['symbol'];
}

/********************************************************************
    check if project cost positions are present and create if not
*********************************************************************/

$sql = "select count(costsheet_id) as num_recs from costsheets " . 
       " where costsheet_version = 0
			and costsheet_project_id = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) //make the user select a cost template
{
	$link = "project_costs_overview.php?pid=" . param("pid");
	redirect($link);
}


/********************************************************************
    check if bid positions are present 
*********************************************************************/
$has_positions = true;

$sql = "select count(costsheet_bid_position_id) as num_recs from costsheet_bid_positions " . 
       " where costsheet_bid_position_costsheet_bid_id = " . id();

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) 
{
	$has_positions = false;
}

/********************************************************************
	prepare data
*********************************************************************/ 
$sql_cost_groups = "select DISTINCT costsheet_pcost_group_id, " . 
		               "concat(pcost_group_code, ' ', pcost_group_name) as costgroup " .
		               "from costsheets " .
					   "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " .
					   "where costsheet_version = 0
						and costsheet_project_id = " . param("pid") . 
					   " order by costgroup";


$sql_bids = "select costsheet_bid_id, costsheet_bid_company " . 
            " from costsheet_bids " . 
			" where costsheet_bid_project_id = " . param("pid") . 
			" order by costsheet_bid_company";

$has_bids = false;

$res = mysql_query($sql_bids) or dberror($sql_bids);
if($row = mysql_fetch_assoc($res))
{
	$has_bids = true;
}


$sql_currencies = "select currency_id, currency_symbol
					from currencies
					order by currency_symbol";


/********************************************************************
	Create Form
*********************************************************************/ 

$form = new Form("costsheet_bids", "costsheet_bids");


$form->add_section("Project");
$form->add_hidden("pid", param('pid'));


require_once "include/project_head_small.php";

$form->add_section("General Information");
$form->add_hidden("costsheet_bid_project_id", param("pid"));
$form->add_list("costsheet_bid_currency", "Currency*", $sql_currencies, NOTNULL);
$form->add_edit("costsheet_bid_company", "Company*", NOTNULL);
$form->add_edit("costsheet_bid_date", "Date*", NOTNULL, "", TYPE_DATE);
$form->add_multiline("costsheet_bid_remark", "Remarks", 4);
$form->add_hidden('costsheet_bid_exchange_rate');
$form->add_hidden('costsheet_bid_factor');




$link = "javascript:popup('/user/project_costs_bid_pdf.php?pid=" . param("pid") . "&bid=" . id() . "', 800, 600);";
$form->add_button("print_bid", "Print Bid in " . $currency_symbol, $link);	

$form->add_button("back", "Back");


$form->populate();
$form->process();


if($form->button("back"))
{
	$link = "project_costs_bids.php?pid=" . param("pid");
	redirect($link);
}


/********************************************************************
	Create list of cost positions
*********************************************************************/ 
$list_names = array();
$group_ids = array();
$group_titles = array();
$sub_group_ids = array();


//get all subgroups
$sql = "select costsheet_bid_position_id, costsheet_bid_position_pcost_group_id, 
       costsheet_bid_position_pcost_subgroup_id,  costsheet_bid_position_costsheet_id, 
	   costsheet_bid_position_code, costsheet_bid_position_text, costsheet_bid_position_comment, 
	   concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup 
	   from costsheet_bid_positions 
	    left join pcost_subgroups on pcost_subgroup_id = costsheet_bid_position_pcost_subgroup_id  
        where costsheet_bid_position_amount > 0 
			and costsheet_bid_position_project_id = " . dbquote(param("pid")) .
		" and costsheet_bid_position_costsheet_bid_id = " . dbquote(param("id"));

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$sub_group_ids[$row['subgroup']] = $row['costsheet_bid_position_pcost_subgroup_id'];
}

if($has_positions == true)
{	
	//get data 
	$bid_totals = get_project_bid_totals(id(), $order_currency);
	$code_data = array();
	$amount_data = array();
	$currency_data = array();
	$text_data = array();
	$comment_data = array();
	$in_budget = array();
	$delete_links = array();
	

	$sql = "select costsheet_bid_position_id, costsheet_bid_position_pcost_group_id, " . 
		   "costsheet_bid_position_code, costsheet_bid_position_text, " . 
		   "costsheet_bid_position_amount, costsheet_bid_position_comment, costsheet_bid_position_is_in_budget " . 
		   "from costsheet_bid_positions " .
		   "where costsheet_bid_position_amount > 0 
			and costsheet_bid_position_costsheet_bid_id =" .id();

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$code_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_code"];
		$text_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_text"];
		$amount_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_amount"];
		$currency_data[$row["costsheet_bid_position_id"]] = $currency_symbol;
		$comment_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_comment"];
		$in_budget[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_is_in_budget"];
		$checkbox_ids[$row["costsheet_bid_position_pcost_group_id"]][$row["costsheet_bid_position_id"]] = "__costsheet_bid_positions_costsheet_bid_position_is_in_budget_" . $row["costsheet_bid_position_id"];
		$delete_ids[$row["costsheet_bid_position_pcost_group_id"]][$row["costsheet_bid_position_id"]] = "__costsheet_bid_positions_select_to_delete_" . $row["costsheet_bid_position_id"];
		
		$list_has_positions[$row["costsheet_bid_position_pcost_group_id"]] = true;

		$delete_links[$row["costsheet_bid_position_id"]] = '<a class="delete_line" data="'.$row['costsheet_bid_position_id'].'" href="javascript:void(0);" title="Remove line"><img src="/pictures/closed.gif" /></i></a>';
	}

	


	//add all cost groups and cost sub groups
	$select_button_names = array();
	$group_ids = array();
	$group_titles = array();
	

	$sql = "select DISTINCT costsheet_bid_position_pcost_group_id, pcost_group_code, pcost_group_name " .
		   "from costsheet_bid_positions " .
		   "left join pcost_groups on pcost_group_id = costsheet_bid_position_pcost_group_id " . 
		   "where costsheet_bid_position_amount > 0 
			and costsheet_bid_position_costsheet_bid_id = " . id() . 
		   " order by pcost_group_code";


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$listname = "list" . $row["pcost_group_code"];
		$select_buttonname = "select_all" . $row["pcost_group_code"];
		$list_names[] = $listname;
		$select_button_names[] = $select_buttonname;
		$group_ids[] = $row["costsheet_bid_position_pcost_group_id"];
		$group_titles[] = $row["pcost_group_name"];

		
		$sql = "select costsheet_bid_position_id, costsheet_bid_position_code, costsheet_bid_position_text, " . 
			   "costsheet_bid_position_amount, costsheet_bid_position_comment, costsheet_bid_position_is_in_budget, " . 
			   "concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup " . 
			   "from costsheet_bid_positions " .
			   "left join pcost_groups on pcost_group_id = costsheet_bid_position_pcost_group_id " .
			   " left join pcost_subgroups on pcost_subgroup_id = costsheet_bid_position_pcost_subgroup_id"; 

		$list_filter = "costsheet_bid_position_amount > 0 
			and costsheet_bid_position_costsheet_bid_id = " . id() . " and costsheet_bid_position_pcost_group_id = " . $row["costsheet_bid_position_pcost_group_id"];
		
		$toggler = '<div class="toggler_pointer" id="l' . $row["costsheet_bid_position_pcost_group_id"] . '_on"><span class="fa fa-minus-square toggler"></span>' .$row["pcost_group_name"] . '</div>';
		
		$$listname = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

		$$listname->set_entity("costsheet_bid_positions");
		$$listname->set_order("LENGTH(costsheet_bid_position_code), COALESCE(costsheet_bid_position_code,'Z')");
		$$listname->set_group("subgroup");
		$$listname->set_filter($list_filter);
		$$listname->set_title($toggler);

		
		

		
		$$listname->add_checkbox_column("costsheet_bid_position_is_in_budget", "", COLUMN_UNDERSTAND_HTML, $in_budget);
		$$listname->add_text_column("costsheet_bid_position_code", "Code", 0, $code_data);
		$$listname->add_text_column("costsheet_bid_position_text", "Text", 0, $text_data);
		$$listname->add_text_column("currency", "", 0, $currency_data);
		$$listname->add_text_column("costsheet_bid_position_amount", "Costs", 0, $amount_data);
		$$listname->add_text_column("costsheet_bid_position_comment", "Comment", 0, $comment_data);
		
		if(array_key_exists($row["costsheet_bid_position_pcost_group_id"], $list_has_positions))
		{
			foreach($bid_totals["subgroup_totals"] as $subgroup=>$bid_sub_group_total)
			{
				
				if(array_key_exists($subgroup, $sub_group_ids))
				{
					$$listname->set_group_footer("costsheet_bid_position_text", $subgroup , "Subgroup Total");
					$$listname->set_group_footer("costsheet_bid_position_amount",  $subgroup , number_format($bid_sub_group_total, 2));

					$$listname->set_group_footer("costsheet_bid_position_text", $subgroup , "Subgroup Total");
					$$listname->set_group_footer("costsheet_bid_position_amount",  $subgroup , number_format($bid_sub_group_total, 2, ".", "'"), 'sgt' . $sub_group_ids[$subgroup]);
				}
			}
					
			if(array_key_exists($subgroup, $sub_group_ids))
			{
				$$listname->set_footer("costsheet_bid_position_code", "Total");
				$$listname->set_footer("costsheet_bid_position_amount", number_format($bid_totals["group_totals"][$row["costsheet_bid_position_pcost_group_id"]], 2, ".", "'"), 'gt' . $row["costsheet_bid_position_pcost_group_id"]);	
			}				
		}

		$$listname->populate();
	}
	
}

$page = new Page("projects");


require "include/project_page_actions.php";

$page->header();
$page->title(id() ? "Project Costs - Edit Bid" : "Project Costs - Add Bid");

require_once("include/costsheet_tabs.php");
$form->render();


foreach($list_names as $key=>$listname)
{
	
	if(array_key_exists("costsheet", $_SESSION) and array_key_exists($listname, $_SESSION["costsheet"]))
	{
		if($_SESSION["costsheet"][$listname] == 0)
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div style="display:none;" id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
		else
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
	}
	else
	{
		echo '<p>&nbsp;</p>';
		$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
		echo $toggler;
		
		echo '<div id="' . $listname . '">';
		$$listname->render();
		echo '</div>';
	}
}


?>
<script language="javascript">
jQuery(document).ready(function($) {
	<?php
	foreach($list_names as $key=>$listname)
	{
	?>
		$('#l<?php echo $group_ids[$key];?>_on').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'none');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'block');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=0",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
		
		$('#l<?php echo $group_ids[$key];?>_off').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'block');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'none');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=1",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
	<?php
	}
	?>
});


</script>

<?php

$page->footer();

?>