<?php
/********************************************************************


    project_edit_comment.php


    edit comments of a project


    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-02-28
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-18
    Version:        1.0.0


    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";


check_access("can_edit_comment_data_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));


// get company's address
$client_address = get_address($project["order_client_address"]);


// bulid sql for comment categoreis
$sql_comment_categories = "select comment_category_id, comment_category_name ".
                          "from comment_categories ".
                          "where comment_category_order_type = 2 ".
                          "order by comment_category_priority ";




// get addresses involved in the project
$companies = get_involved_companies_2($project["project_order"], id());


// determine users that have already got an email announcing the new attachment
$old_recipients = array();
foreach($companies as $key=>$value)
{
    if($value["access"] == 1)
    {
        $old_recipients[] = $value["id"];
    }
}


// checkbox names
$check_box_names = array();


/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("comments", "comment");


$form->add_hidden("pid", param('pid'));
$form->add_hidden("order_id", $project["project_order"]);


require_once "include/project_head_small.php";


$form->add_section("Comment");
$form->add_list("comment_category", "Category*", $sql_comment_categories, NOTNULL);
$form->add_multiline("comment_text", "Comment", 4, NOTNULL);


if (has_access("can_set_comment_accessibility_in_projects"))
{
    $form->add_section("Accessibility");
    $form->add_comment("Please indicate who is allowed to read this comment.");

	
    $num_checkboxes = 1;
    foreach ($companies as $key=>$value_array)
    {
        if(!array_key_exists('role', $value_array)) {
			$value_array["role"] = "";
		}
		$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], $value_array["access"], 0, $value_array["role"]);
        $check_box_names["A" . $num_checkboxes] = $value_array["id"];
        $num_checkboxes++;
    }
	
}

$form->add_section("CC Recipients");
$form->add_modal_selector("ccmails", "Selected Recipients", 8);


$form->add_button(FORM_BUTTON_SAVE, "Save");


$form->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button(FORM_BUTTON_SAVE))
{
    // check if a recipient was selected
    if (has_access("can_set_comment_accessibility_in_projects"))
    {
        $no_recipient = 1;
    }
    else
    {
        $no_recipient = 0;
    }


    foreach ($form->items as $item)
    {
        if ($item["type"] == "checkbox" and $item["value"] == "1")
        {
            $no_recipient = 0;
        }
    }


    if ($form->validate() and $no_recipient == 0)
    {
        update_comment_accessibility_info(id(), $form,  $check_box_names);


        $link = "project_view_comments.php?pid=" . param("pid");
        redirect($link);
    }
    else
    {
        $form->error("Please select a least one person to have access to the comment.");
    }
}



$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Edit Comment");
$form->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#ccmails_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/shared/select_mail_recipients.php'
    });
    return false;
  });
});
</script>

<?php

$page->footer();


?>