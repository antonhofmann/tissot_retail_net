<?php
/********************************************************************

    project_edit_project_budget.php

    Edit Project's Budget Sheet

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-06-15
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_budget_in_projects");

register_param("pid");
set_referer("project_edit_budget_position.php");
set_referer("project_add_budget_position.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get system currency
$system_currency = get_system_currency_fields();

// get orders's currency
$order_currency = get_order_currency($project["project_order"]);


// build line numbers for budget positions
$line_numbers = get_budet_line_numbers($project["project_order"]);

// build group_totals of standard item groups


if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    
    $sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                 "    order_item_po_number, item_id, order_item_system_price, ".
                 "    TRUNCATE(order_item_quantity * order_item_system_price, 2) as total_price, ".
                 "    order_item_client_price, ".
                 "    if(item_code <> '', item_code, item_type_name) as item_shortcut, ".
                 "    category_priority, category_name, ".
                 "    address_shortcut, item_type_id, ".
                 "    item_type_priority, order_item_type, ".
                 "    concat(item_category_sortorder, ' ', item_category_name, ', ', category_name) as cat_name  ".
                 "from order_items ".
                 "left join items on order_item_item = item_id ".
                 "left join categories on order_item_category = category_id ".
                 "left join item_categories on item_category_id = item_category " .
                 "left join addresses on order_item_supplier_address = address_id ".
                 "left join item_types on order_item_type = item_type_id ";


    $list1_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order = " . $project["project_order"] .  " and (order_item_type = " . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ")";

    $list2_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_SPECIAL;

    $list3_filter = "order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_COST_ESTIMATION;

    $list6_filter = "order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST;
    
    $group_totals = get_group_total_of_standard_items($project["project_order"], "order_item_system_price");
    $standard_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_STANDARD);
    $special_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_SPECIAL);
    $cost_estimation_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_COST_ESTIMATION);

    $local_construction_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_LOCALCONSTRUCTIONCOST);
}
else
{
    $sql_order_items = "select order_item_id, order_item_text, order_item_quantity_freezed, ".
                 "    order_item_po_number, item_id, order_item_system_price_freezed, ".
                 "    TRUNCATE(order_item_quantity_freezed * order_item_system_price_freezed, 2) as total_price, ".
                 "    order_item_client_price_freezed, ".
                 "    if(item_code <> '', item_code, item_type_name) as item_shortcut, ".
                 "    category_priority, category_name, ".
                 "    address_shortcut, item_type_id, ".
                 "    item_type_priority, order_item_type, " .
                 "    concat(item_category_sortorder, ' ', item_category_name, ', ', category_name) as cat_name  ".
                 "from order_items ".
                 "left join items on order_item_item = item_id ".
                 "left join categories on order_item_category = category_id ".
                 "left join item_categories on item_category_id = item_category " .
                 "left join addresses on order_item_supplier_address = address_id ".
                 "left join item_types on order_item_type = item_type_id ";


    $list1_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order = " . $project["project_order"] .  " and (order_item_type = " . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ") and order_item_in_freezed_budget = 1";

    $list2_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_SPECIAL . " and order_item_in_freezed_budget = 1";

    $list3_filter = "order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_COST_ESTIMATION . " and order_item_in_freezed_budget = 1";

    $list6_filter = "order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . " and order_item_in_freezed_budget = 1";

    $group_totals = get_group_total_of_standard_items_freezed($project["project_order"], "order_item_system_price_freezed");
    $standard_item_total = get_order_item_type_total_freezed($project["project_order"],  ITEM_TYPE_STANDARD);
    $special_item_total = get_order_item_type_total_freezed($project["project_order"],  ITEM_TYPE_SPECIAL);
    $cost_estimation_item_total = get_order_item_type_total_freezed($project["project_order"],  ITEM_TYPE_COST_ESTIMATION);

    $local_construction_item_total = get_order_item_type_total_freezed($project["project_order"],  ITEM_TYPE_LOCALCONSTRUCTIONCOST);
}


$grand_total_in_system_currency = number_format($standard_item_total["in_system_currency"] + $special_item_total["in_system_currency"],2, ".", "'");


$total_cost = $standard_item_total["in_system_currency"] + $special_item_total["in_system_currency"] + $cost_estimation_item_total["in_system_currency"] + $local_construction_item_total["in_system_currency"];


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_label("project_number", "Project Number", 0, $project["order_number"]);

require_once "include/project_head_small.php";

$form->add_section("Budget Totals");
$form->add_label("grand_total1", "Standard/Special Items in " . $system_currency["symbol"], 0, $grand_total_in_system_currency);


$form->add_label("grand_total6", "Estimation Local Construction Cost in " . $system_currency["symbol"], 0, number_format($local_construction_item_total["in_system_currency"],2, ".", "'"));

$form->add_label("grand_total2", "Estimation of Add. Cost in " . $system_currency["symbol"], 0, number_format($cost_estimation_item_total["in_system_currency"],2));
$form->add_label("grand_total3", "Total Cost " . $system_currency["symbol"], 0, number_format($total_cost,2, ".", "'"));

$form->add_section("Budget State");
$form->add_checkbox("order_budget_is_locked", "Budget is locked", $project["order_budget_is_locked"], 0);

/*
$form->add_button("save", "Update Budget State");
$form->add_button("recalculate", "Recalculate Budget");


if(has_access("can_unfreeze_budget"))
{
    $form->add_button("unfreeze", "Unfreeze Budget");
	$form->add_button("correct_prices", "Correct Prices");
}
*/

/********************************************************************
    Create Standard Item List
*********************************************************************/ 
$list1 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("Catalog Items");
$list1->set_entity("order_item");
$list1->set_filter($list1_filter);
$list1->set_order("item_code");
$list1->set_group("cat_name");

$list1->add_hidden("pid", param("pid"));
$list1->add_hidden("oid",$project["project_order"]);

$link="project_edit_budget_position.php?pid=" . param("pid");

$list1->add_text_column("pos", "No.", 0, $line_numbers[ITEM_TYPE_STANDARD]);

if ($project["order_budget_is_locked"] == 1)
{
    $list1->add_column("item_shortcut", "Item Code");   
}
else
{
    $list1->add_column("item_shortcut", "Item Code", $link);
}


$list1->add_column("order_item_text", "Name");
if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    $list1->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
    $list1->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}
else
{
    $list1->add_column("order_item_quantity_freezed", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
    $list1->add_column("order_item_system_price_freezed", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}
$list1->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);



// set group totals
foreach ($group_totals as $key=>$value)
{
    $list1->set_group_footer("total_price", $key , number_format($value,2, ".", "'"));
}

$list1->set_footer("item_code", "Total");
$list1->set_footer("total_price", number_format($standard_item_total["in_system_currency"],2, ".", "'"));



/********************************************************************
    Create Special Item List
*********************************************************************/ 
$list2 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list2->set_title("Special Items");
$list2->set_entity("order_item");
$list2->set_filter($list2_filter);
$list2->set_order("item_code");

$list2->add_hidden("pid", param("pid"));
$list2->add_hidden("oid",$project["project_order"]);

$link="project_edit_budget_position.php?pid=" . param("pid");

$list2->add_text_column("pos", "No.", 0, $line_numbers[ITEM_TYPE_SPECIAL]);

if ($project["order_budget_is_locked"] == 1)
{
    $list2->add_column("item_shortcut", "Item Code");   
}
else
{
    $list2->add_column("item_shortcut", "Item Code", $link);
}

$list2->add_column("order_item_text", "Name");

if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    $list2->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
    $list2->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}
else
{
    $list2->add_column("order_item_quantity_freezed", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
    $list2->add_column("order_item_system_price_freezed", "Price " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}

$list2->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list2->set_footer("item_code", "Total");
$list2->set_footer("total_price", number_format($special_item_total["in_system_currency"],2, ".", "'"));


/********************************************************************
    Create Local Construction Cost Positions
*********************************************************************/ 
$list6 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list6->set_title("Local Construction Cost Estimation");
$list6->set_entity("order_item");
$list6->set_filter($list6_filter);
$list6->set_order("item_code, order_item_id");

$list6->add_hidden("pid", param("pid"));
$list6->add_hidden("oid",$project["project_order"]);

$link="project_edit_budget_position.php?pid=" . param("pid");

$list6->add_text_column("pos", "No.", 0, $line_numbers[ITEM_TYPE_LOCALCONSTRUCTIONCOST]);

if ($project["order_budget_is_locked"] == 1)
{
    $list6->add_column("item_shortcut", "Item Code");   
}
else
{
    $list6->add_column("item_shortcut", "Item Code", $link);
}

$list6->add_column("order_item_text", "Text");
if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    $list6->add_column("order_item_system_price", "Estimated Cost " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
    $list6->set_footer("item_code", "Total");
    $list6->set_footer("order_item_system_price", number_format($local_construction_item_total["in_system_currency"],2, ".", "'"));
}
else
{
    $list6->add_column("order_item_system_price_freezed", "Estimated Cost " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
    $list6->set_footer("item_code", "Total");
    $list6->set_footer("order_item_system_price_freezed", number_format($local_construction_item_total["in_system_currency"],2, ".", "'"));
}


/********************************************************************
    Create Cost Estimation Positions
*********************************************************************/ 
$list3 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list3->set_title("Cost Estimation");
$list3->set_entity("order_item");
$list3->set_filter($list3_filter);

$list3->add_hidden("pid", param("pid"));
$list3->add_hidden("oid",$project["project_order"]);

$link="project_edit_budget_position.php?pid=" . param("pid");

$list3->add_text_column("pos", "No.", 0, $line_numbers[ITEM_TYPE_COST_ESTIMATION]);

if ($project["order_budget_is_locked"] == 1)
{
    $list3->add_column("item_shortcut", "Item Code");   
}
else
{
    $list3->add_column("item_shortcut", "Item Code", $link);
}

$list3->add_column("order_item_text", "Text");
if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    $list3->add_column("order_item_system_price", "Estimated Cost " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
    $list3->set_footer("item_code", "Total");
    $list3->set_footer("order_item_system_price", number_format($cost_estimation_item_total["in_system_currency"],2, ".", "'"));
}
else
{
    $list3->add_column("order_item_system_price_freezed", "Estimated Cost " . $system_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
    $list3->set_footer("item_code", "Total");
    $list3->set_footer("order_item_system_price_freezed", number_format($cost_estimation_item_total["in_system_currency"],2, ".", "'"));
}




/********************************************************************
    Create Exclusion Positions
*********************************************************************/ 
$list4 = new ListView($sql_order_items, LIST_HAS_HEADER);

$list4->set_title("Exclusions");
$list4->set_entity("order_item");
$list4->set_filter("order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_EXCLUSION);

$list4->add_hidden("pid", param("pid"));
$list4->add_hidden("oid",$project["project_order"]);

$link="project_edit_budget_position.php?pid=" . param("pid");

$list4->add_text_column("pos", "No.", 0, $line_numbers[ITEM_TYPE_EXCLUSION]);

if ($project["order_budget_is_locked"] == 1)
{
    $list4->add_column("item_shortcut", "Item Code");   
}
else
{
    $list4->add_column("item_shortcut", "Item Code", $link);
}

$list4->add_column("order_item_text", "Exclusion");

if ($project["order_budget_is_locked"] == 1)
{
    $list4->add_button("nothing", "");
}
else
{
    $list4->add_button("add_s_exclusion", "Add Standard Exclusions");
    $list4->add_button("add_exclusion", "Add Individual Exclusion");
    $list4->add_button("delete_exclusions", "Delete All Exclusions");
}



/********************************************************************
    Create Notification Positions
*********************************************************************/ 
$list5 = new ListView($sql_order_items, LIST_HAS_HEADER);

$list5->set_title("Notifications");
$list5->set_entity("order_item");
$list5->set_filter("order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_NOTIFICATION);

$list5->add_hidden("pid", param("pid"));
$list5->add_hidden("oid",$project["project_order"]);

$link="project_edit_budget_position.php?pid=" . param("pid");

$list5->add_text_column("pos", "No.", 0, $line_numbers[ITEM_TYPE_NOTIFICATION]);

if ($project["order_budget_is_locked"] == 1)
{
    $list5->add_column("item_shortcut", "Item Code");   
}
else
{
    $list5->add_column("item_shortcut", "Item Code", $link);
}

$list5->add_column("order_item_text", "Notification");

if ($project["order_budget_is_locked"] == 1)
{
    $list5->add_button("nothing", "");
}
else
{
    $list5->add_button("add_s_notification", "Add Standard Notifications");
    $list5->add_button("add_notification", "Add Individual Notification");
    $list5->add_button("delete_notifications", "Delet All Notifications");
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();

$list2->populate();
$list2->process();

$list3->populate();
$list3->process();

$list4->populate();
$list4->process();

$list5->populate();
$list5->process();

$list6->populate();
$list6->process();

if ($form->button("recalculate"))
{
    update_exchange_rates($project["project_order"]);
    $link = "project_edit_project_budget.php?pid=" . param("pid");
    redirect($link);

}
elseif ($form->button("unfreeze"))
{
    unfreeze_project_budget($project["project_order"]);
    $link = "project_edit_project_budget.php?pid=" . param("pid");
    redirect($link);
}

if ($list4->button("add_s_exclusion"))
{
     add_standard_budget_positions(ITEM_TYPE_EXCLUSION, $project["project_order"]);
     redirect ("project_edit_project_budget.php?pid=" . param("pid"));
}
else if ($list5->button("add_s_notification"))
{
     add_standard_budget_positions(ITEM_TYPE_NOTIFICATION, $project["project_order"]);
     redirect ("project_edit_project_budget.php?pid=" . param("pid"));
}
else if ($list4->button("add_exclusion"))
{
    $link = "project_add_budget_position.php?pid=" . param("pid") . "&oid=" . $project["project_order"] . "&type=" . ITEM_TYPE_EXCLUSION; 
    redirect($link);
}
else if ($list5->button("add_notification"))
{
    $link = "project_add_budget_position.php?pid=" . param("pid") . "&oid=" . $project["project_order"] . "&type=" . ITEM_TYPE_NOTIFICATION; 
    redirect($link);
}
else if ($list4->button("delete_exclusions"))
{
     delete_all_budget_positions(ITEM_TYPE_EXCLUSION, $project["project_order"]);
     redirect ("project_edit_project_budget.php?pid=" . param("pid"));
}
else if ($list5->button("delete_notifications"))
{
     delete_all_budget_positions(ITEM_TYPE_NOTIFICATION, $project["project_order"]);
     redirect ("project_edit_project_budget.php?pid=" . param("pid"));
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("save"))
{
    update_budget_state($project["project_order"], $form->value("order_budget_is_locked"));
    $link = "project_edit_project_budget.php?pid=" . param("pid");
    redirect($link);
}
elseif ($form->button("correct_prices"))
{
	$sql_u = "update order_items set " . 
		     "order_item_client_price = order_item_system_price, " . 
		     "order_item_client_price_freezed = order_item_system_price_freezed " . 
		     "where order_item_order = " . $project["project_order"];

	$res = mysql_query($sql_u);

	$sql_u = "update orders set " . 
		     "order_client_currency = 1, " . 
		     "order_client_exchange_rate = 1 " . 
		     "where order_item_order = " . $project["project_order"];

	$res = mysql_query($sql_u);


	$form->message("Prices were corrected.");
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit Budget");

$form->render();

echo "<br>";
$list1->render();
echo "<br>";
$list2->render();
echo "<br>";
// render list1 to 5 only when list of materials is present
if ($grand_total_in_system_currency > 0)
{
    $list6->render();
    echo "<br>";
    $list3->render();
    echo "<br>";
    $list4->render();
    echo "<br>";
    $list5->render();
    echo "<br>";
    
}

$page->footer();

?>