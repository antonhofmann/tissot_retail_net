<?php
/********************************************************************

    project_edit_cost_monitoring_preview_pdf

    Print Cost Monitoring Sheet

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-10-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-07
    Version:        1.1.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";


if(!has_access("can_view_cost_monitoring") and !has_access("has_access_to_cost_monitoring"))
{
	redirect("noaccess.php");
}


/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());

// read project and order details
$project = get_project(param("pid"));


$show_budget_in_loc = true;
$order_currency2 = get_order_currency($project["project_order"]);
if(array_key_exists("chf", $_GET) and $_GET["chf"] == 1)
{
	$order_currency = get_system_currency_fields();
	$show_budget_in_loc = false;
}
else
{
	$order_currency = get_order_currency($project["project_order"]);
}

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];

$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);
$retail_coordinator = get_user($project["project_retail_coordinator"]);
$retail_operator = get_user($project["order_retail_operator"]);


// get company's address
$client_address = get_address($project["order_client_address"]);

/********************************************************************
    Get Cost Monitoring Sheet CER Data
*********************************************************************/ 
$cer_investments = array();
$cer_investments_approved = array();
$cer_investments_kl_additional_approved = array();

$total_from_cer = 0;
$total_from_cer_approved = 0;
$total_from_cer_additional_approved = 0;
$landlord_contribution = 0;

$sql = "select * from cer_investments " . 
       "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
       "where cer_investment_type in(1, 3, 5, 7, 11, 19) and cer_investment_project = " . param("pid")  .
	   " and cer_investment_cer_version = 0 " . 
	   " order by cer_investment_type";


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res)) {
	
	if($show_budget_in_loc == true)
	{
		


		if($row["cer_investment_type"] == 19)
		{
			$landlord_contribution = $project["project_cost_real_landlord_contribution_loc"];
		}
		else
		{
			$cer_investments[] = $row["cer_investment_amount_cer_loc"];
			$total_from_cer = $total_from_cer + $row["cer_investment_amount_cer_loc"];
			
			
			if($project["project_cost_type"] == 1) // corporate
			{
				$cer_investments_approved[] = $row["cer_investment_amount_cer_loc_approved"];
				$cer_investments_kl_additional_approved[] = $row["cer_investment_amount_additional_cer_loc_approved"];
				$total_from_cer_approved = $total_from_cer_approved + $row["cer_investment_amount_cer_loc_approved"];
				$total_from_cer_additional_approved = $total_from_cer_additional_approved + $row["cer_investment_amount_additional_cer_loc_approved"];
			}
			else
			{
				$cer_investments_approved[] = '';
				$cer_investments_kl_additional_approved[] = '';
			}
		}
	}
	else
	{
		if($row["cer_investment_type"] == 19)
		{
			$landlord_contribution = $exchange_rate*$project["project_cost_real_landlord_contribution_loc"]/$factor;
		}
		else
		{
			$exchange_rate = $order_currency2["exchange_rate"];
			$factor = $order_currency2["factor"];

			$cer_investments[] = $exchange_rate*$row["cer_investment_amount_cer_loc"]/$factor;
			$total_from_cer = $total_from_cer + ($exchange_rate*$row["cer_investment_amount_cer_loc"]/$factor);

			if($project["project_cost_type"] == 1) // corporate
			{
				$cer_investments_approved[] = $exchange_rate*$row["cer_investment_amount_cer_loc_approved"]/$factor;
				$cer_investments_kl_additional_approved[] = $exchange_rate*$row["cer_investment_amount_additional_cer_loc_approved"]/$factor;
				$total_from_cer_approved = $total_from_cer_approved + ($exchange_rate*$row["cer_investment_amount_cer_loc_approved"]/$factor);
				$total_from_cer_additional_approved = $total_from_cer_additional_approved + ($exchange_rate*$row["cer_investment_amount_additional_cer_loc_approved"]/$factor);
			}
			else
			{
				$cer_investments_approved[] = '';
				$cer_investments_kl_additional_approved[] = '';
			}
		}
	}
}

for($i=0;$i<5;$i++) {
	if(!array_key_exists($i, $cer_investments)) {
		$cer_investments[$i] = 0;
	}
	if(!array_key_exists($i, $cer_investments_approved)) {
		$cer_investments_approved[$i] = 0;
	}
	if(!array_key_exists($i, $cer_investments_kl_additional_approved)) {
		$cer_investments_kl_additional_approved[$i] = 0;
	}
}


/********************************************************************
    Get Cost Monitoring Sheet Budget Data
*********************************************************************/ 
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$project_cost_gross_sqms = $row["project_cost_grosssqms"];
$project_cost_total_sqms = $row["project_cost_totalsqms"];
$project_cost_sqms = $row["project_cost_sqms"];


/********************************************************************
    project items coming from catalogue orders
*********************************************************************/ 
$oip_group_totals = array();


if($show_budget_in_loc == true)
{
	$sql_oip = "select order_items_in_project_id, order_items_in_project_costgroup_id, " . 
		       "order_items_in_project_order_item_id, order_items_in_project_cms_remark, " . 
			   " IF(address_shortcut <> '', address_shortcut, order_item_supplier_freetext) as address_shortcut, " .
			   "order_item_text,  order_item_id, " .
			   "DATE_FORMAT(order_item_ordered, '%d.%m.%y') as date1, " .
				"DATE_FORMAT(order_item_pickup, '%d.%m.%y') as date2, " .
				"DATE_FORMAT(order_item_arrival, '%d.%m.%y') as date3, " .
				"TRUNCATE(sum(order_item_quantity * order_items_in_project_real_client_price), 2) as investment, ".
			   " order_item_po_number, project_cost_groupname_name, " .
			   "DATE_FORMAT(order_item_reinvoiced, '%d.%m.%y') as date4, " .
				"order_item_reinvoicenbr, " .
				"DATE_FORMAT(order_item_reinvoiced2, '%d.%m.%y') as date42, " .
				"order_item_reinvoicenbr2 " .
			   " from order_items_in_projects " .
			   " left join order_items on order_item_id = order_items_in_project_order_item_id " .
			   " left join addresses on order_item_supplier_address = address_id ".
			   " left join project_cost_groupnames on project_cost_groupname_id =  order_items_in_project_costgroup_id " .
			   " where order_items_in_project_project_order_id = " . $project["project_order"] . 
			   " group by order_items_in_project_id";
}
else
{
	$sql_oip = "select order_items_in_project_id, order_items_in_project_costgroup_id, order_items_in_project_order_item_id, " . 
			   " IF(address_shortcut <> '', address_shortcut, order_item_supplier_freetext) as address_shortcut, " .
			   "order_item_text,  order_item_id, " .
			   "DATE_FORMAT(order_item_ordered, '%d.%m.%y') as date1, " .
				"DATE_FORMAT(order_item_pickup, '%d.%m.%y') as date2, " .
				"DATE_FORMAT(order_item_arrival, '%d.%m.%y') as date3, " .
				"TRUNCATE(sum(order_item_quantity * order_items_in_project_real_price), 2) as investment, ".
			   " order_item_po_number, project_cost_groupname_name, " .
			   "DATE_FORMAT(order_item_reinvoiced, '%d.%m.%y') as date4, " .
				"order_item_reinvoicenbr, " .
				"DATE_FORMAT(order_item_reinvoiced2, '%d.%m.%y') as date42, " .
				"order_item_reinvoicenbr2 " .
			   " from order_items_in_projects " .
			   " left join order_items on order_item_id = order_items_in_project_order_item_id " .
			   " left join addresses on order_item_supplier_address = address_id ".
			   " left join project_cost_groupnames on project_cost_groupname_id =  order_items_in_project_costgroup_id " .
			   " where order_items_in_project_project_order_id = " . $project["project_order"] . 
			   " group by order_items_in_project_id";
}



$grouptotals_oip = 0;
$has_catalogue_orders = false;
$replacementinfos = array();
$res = mysql_query($sql_oip) or dberror($sql_oip);
while($row = mysql_fetch_assoc($res))
{
	$grouptotals_oip = $grouptotals_oip  + $row["investment"];

	if(array_key_exists($row["order_items_in_project_costgroup_id"], $oip_group_totals))
	{
		$oip_group_totals[$row["order_items_in_project_costgroup_id"]] = 		$oip_group_totals[$row["order_items_in_project_costgroup_id"]] + $row["investment"];
	}
	else
	{
		$oip_group_totals[$row["order_items_in_project_costgroup_id"]] = $row["investment"];
	}
	$has_catalogue_orders = true;

	//check if item is a replacement
	$sql_r = "select count(order_item_replacement_id) as num_recs " . 
		     " from order_item_replacements " . 
		     " where order_item_replacement_catalog_order_order_item_id = " . dbquote($row["order_item_id"]);


	$res_r = mysql_query($sql_r) or dberror($sql_r);
	$row_r = mysql_fetch_assoc($res_r);
	if($row_r["num_recs"] > 0)
	{
		$replacementinfos[$row["order_item_id"]] = 'Replacement';
	}

}


/********************************************************************
    Calculate difference in percent
*********************************************************************/ 
$project_cost_real = 0;
$project_cost_budget = 0;

$grouptotals_investment = array();
$grouptotals_investment[2] = 0;
$grouptotals_investment[6] = 0;
$grouptotals_investment[7] = 0;
$grouptotals_investment[8] = 0;
$grouptotals_investment[9] = 0;
$grouptotals_investment[10] = 0;
$grouptotals_investment[11] = 0;

$grouptotals_real = array();
$grouptotals_real[2] = 0;
$grouptotals_real[6] = 0;
$grouptotals_real[7] = 0;
$grouptotals_real[8] = 0;
$grouptotals_real[9] = 0;
$grouptotals_real[10] = 0;
$grouptotals_real[11] = 0;

$grouptotals_difference_in_cost = array();
$grouptotals_difference_in_cost[2] = 0;
$grouptotals_difference_in_cost[6] = 0;
$grouptotals_difference_in_cost[7] = 0;
$grouptotals_difference_in_cost[8] = 0;
$grouptotals_difference_in_cost[9] = 0;
$grouptotals_difference_in_cost[10] = 0;
$grouptotals_difference_in_cost[11] = 0;

$grouptotals_difference_in_cost2 = array();
$grouptotals_difference_in_cost2[2] = 0;
$grouptotals_difference_in_cost2[6] = 0;
$grouptotals_difference_in_cost2[7] = 0;
$grouptotals_difference_in_cost2[8] = 0;
$grouptotals_difference_in_cost2[9] = 0;
$grouptotals_difference_in_cost2[10] = 0;
$grouptotals_difference_in_cost2[11] = 0;

$num_recs2 = 0;
$num_recs6 = 0;
$num_recs7 = 0;
$num_recs8 = 0;
$num_recs9 = 0;
$num_recs10 = 0;
$num_recs11 = 0;

$cms_remarks = array();
$cms_remark_keys = array();
$cms_remark_referenced_line_numbers = array();


if($show_budget_in_loc == true)
{
	$sql = "select order_item_cost_group, order_item_type, ".
		   "order_item_client_price, order_item_real_client_price, order_item_quantity,  ".
		   "order_item_client_price_freezed, order_item_quantity_freezed,  ".
		   "order_item_text, order_item_cms_remark, " .
		   "if(order_items_monitoring_group is not null, order_items_monitoring_group, order_item_text) as monitoring_group " .  
		   "from order_items ".
		   "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
		   "   and order_item_order=" . $project["project_order"] . 
		   "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
		   "   or order_item_type = " . ITEM_TYPE_SERVICES .
		   "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
		   " ) " .
		   " and order_item_cost_group > 0 " . 
		   " order by order_item_cost_group";
}
else
{
	$sql = "select order_item_cost_group, order_item_type, ".
		   "order_item_system_price, order_item_real_system_price, order_item_quantity,  ".
		   "order_item_system_price_freezed, order_item_quantity_freezed,  ".
		   "order_item_text, order_item_cms_remark, " .
		   "if(order_items_monitoring_group is not null, order_items_monitoring_group, order_item_text) as monitoring_group " .  
		   "from order_items ".
		   "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
		   "   and order_item_order=" . $project["project_order"] . 
		   "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
		   "   or order_item_type = " . ITEM_TYPE_SERVICES .
		   "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
		   " ) " .
		   " and order_item_cost_group > 0 " . 
		   " order by order_item_cost_group";
}


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	/*
	if($row["order_item_quantity_freezed"] > 0)
    {
		$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"];
		$project_cost_budget = $project_cost_budget + $row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"];
    }
    else
    {
		$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_freezed"];
		$project_cost_budget = $project_cost_budget + $row["order_item_system_price_freezed"];
    }

	if($row["order_item_quantity"] > 0)
    {
		$grouptotals_real[$row["order_item_cost_group"]] =  $grouptotals_real[$row["order_item_cost_group"]] + ($row["order_item_system_price"] * $row["order_item_quantity"]);
		$project_cost_real = $project_cost_real + $row["order_item_system_price"] * $row["order_item_quantity"];

	}
	else
	{
		$grouptotals_real[$row["order_item_cost_group"]] = $grouptotals_real[$row["order_item_cost_group"]] + $row["order_item_system_price"];
		$project_cost_real = $project_cost_real + $row["order_item_system_price"];
	}
	*/
	
	
	if($show_budget_in_loc == true)
	{
		$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"];
		$project_cost_budget = $project_cost_budget + $row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"];

		//$grouptotals_real[$row["order_item_cost_group"]] =  $grouptotals_real[$row["order_item_cost_group"]] + ($row["order_item_client_price"] * $row["order_item_quantity"]);

		$grouptotals_real[$row["order_item_cost_group"]] =  $grouptotals_real[$row["order_item_cost_group"]] + ($row["order_item_real_client_price"] * $row["order_item_quantity"]);

		
		//$project_cost_real = $project_cost_real + $row["order_item_client_price"] * $row["order_item_quantity"];
		$project_cost_real = $project_cost_real + $row["order_item_real_client_price"] * $row["order_item_quantity"];
	}
	else
	{
		$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"];
		$project_cost_budget = $project_cost_budget + $row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"];

		//$grouptotals_real[$row["order_item_cost_group"]] =  $grouptotals_real[$row["order_item_cost_group"]] + ($row["order_item_system_price"] * $row["order_item_quantity"]);

		$grouptotals_real[$row["order_item_cost_group"]] =  $grouptotals_real[$row["order_item_cost_group"]] + ($row["order_item_real_system_price"] * $row["order_item_quantity"]);

		
		//$project_cost_real = $project_cost_real + $row["order_item_system_price"] * $row["order_item_quantity"];
		$project_cost_real = $project_cost_real + $row["order_item_real_system_price"] * $row["order_item_quantity"];
	}

	
    if(strlen($row["order_item_cms_remark"]) > 0)
	{
		$cms_remarks[] = $row["monitoring_group"] . "@@@@" . $row["order_item_text"] . ": " . $row["order_item_cms_remark"];

		$cms_remark_keys[] =  $row["monitoring_group"];
	}


	if($row["order_item_cost_group"] == 6)  //freight charges
    {
        $num_recs6 = $num_recs6 + 1;
    }
    elseif($row["order_item_cost_group"] == 7)  //local construction cost
    {
        $num_recs7 = $num_recs7 + 1;
    }
	elseif($row["order_item_cost_group"] == 9)  //architectural cost
    {
        $num_recs9 = $num_recs9 + 1;
    }
	elseif($row["order_item_cost_group"] == 10)  //fixturing other cost
    {
        $num_recs10 = $num_recs10 + 1;
    }
	elseif($row["order_item_cost_group"] == 11)  //equipment
    {
        $num_recs11 = $num_recs11 + 1;
    }
	elseif($row["order_item_cost_group"] == 8) // other cost
    {
        $num_recs8 = $num_recs8 + 1;
    }
    elseif($row["order_item_cost_group"] == 2) // HQ Suplied Items
    {
        $num_recs2 = $num_recs2 + 1;
    }


}




foreach($grouptotals_real as $key=>$value)
{
     if(array_key_exists($key, $oip_group_totals))
	 {
		$grouptotals_real[$key] = $grouptotals_real[$key] + $oip_group_totals[$key];
	 }
}

//budget
$project_cost_construction = $grouptotals_investment[7];
$project_cost_fixturing = $grouptotals_investment[10] + $grouptotals_investment[6] + $grouptotals_investment[2];
$project_cost_architectural = $grouptotals_investment[9];
$project_cost_equipment = $grouptotals_investment[11];
$project_cost_other = + $grouptotals_investment[8];
$budget_total = $project_cost_construction + $project_cost_fixturing + $project_cost_architectural + $project_cost_equipment + $project_cost_other;


//calculate difference in percent
$project_cost_real = 0;
foreach($grouptotals_real as $key=>$value)
{
     
	 $project_cost_real = $project_cost_real + $value;
     $grouptotals_difference_in_cost[$key] =  number_format($grouptotals_real[$key] - $grouptotals_investment[$key], 2, ".", "'");
     
     if($grouptotals_investment[$key] > 0)
     {
        $grouptotals_difference_in_cost2[$key] =  $grouptotals_real[$key] - $grouptotals_investment[$key];
        $grouptotals_difference_in_cost2[$key] = -1 * (1- ($value / $grouptotals_investment[$key]));
     }
     else
     {
        $grouptotals_difference_in_cost2[$key] = 1;
        $grouptotals_investment[$key] = 0;
     }
     
     $grouptotals_difference_in_cost2[$key] = number_format(100*$grouptotals_difference_in_cost2[$key], 2, ".", "'")  . "%";
}

if($landlord_contribution > 0)
{
	$project_cost_real = $project_cost_real - $landlord_contribution;
}

$project_cost_diff1 =  $project_cost_real - $project_cost_budget;

if($project_cost_budget > 0)
{
    $project_cost_diff2 = -1 * ( 1 - ($project_cost_real / $project_cost_budget));
}
else
{
    $project_cost_diff2 = 1;
}

if($budget_total > 0)
{
    $project_cost_diff3 = -1 * ( 1 - ($project_cost_real / $budget_total));
}
else
{
    $project_cost_diff3 = 1;
}



/********************************************************************
    List SQls
*********************************************************************/ 
$sql_cost_groups = "select * from project_cost_groupnames";


if($show_budget_in_loc == true)
{
	$sql_base = "select max(order_item_id) as item, " .
				"IF(address_shortcut <> '', address_shortcut, order_item_supplier_freetext) as address_shortcut, " .
				"DATE_FORMAT(order_item_ordered, '%d.%m.%y') as date1, " .
				"DATE_FORMAT(order_item_pickup, '%d.%m.%y') as date2, " .
				"DATE_FORMAT(order_item_arrival, '%d.%m.%y') as date3, " .
				//"IF(order_item_quantity_freezed>0,TRUNCATE(sum(order_item_quantity_freezed * order_item_client_price_freezed), 2), order_item_client_price_freezed) as approved, ". 
				//"TRUNCATE(sum(order_item_quantity * order_item_client_price), 2) as investment, ".
				"TRUNCATE(sum(order_item_quantity_freezed * order_item_client_price_freezed), 2) as approved, ".
				"TRUNCATE(sum(order_item_quantity * order_item_real_client_price), 2) as investment, ".
				"if(order_items_monitoring_group is not null, order_items_monitoring_group, order_item_text) as monitoring_group,  " .
				"order_item_po_number, " .
				"DATE_FORMAT(order_item_reinvoiced, '%d.%m.%y') as date4, " .
				"order_item_reinvoicenbr, " .
				"DATE_FORMAT(order_item_reinvoiced2, '%d.%m.%y') as date42, " .
				"order_item_reinvoicenbr2 " .
				"from order_items ".
				"left join items on order_item_item = item_id ".
				"left join addresses on order_item_supplier_address = address_id ".
				"left join item_types on order_item_type = item_type_id " . 
				"left join project_cost_groupnames on project_cost_groupname_id =  order_item_cost_group ";
}
else
{
	$sql_base = "select max(order_item_id) as item, " .
				"IF(address_shortcut <> '', address_shortcut, order_item_supplier_freetext) as address_shortcut, " .
				"DATE_FORMAT(order_item_ordered, '%d.%m.%y') as date1, " .
				"DATE_FORMAT(order_item_pickup, '%d.%m.%y') as date2, " .
				"DATE_FORMAT(order_item_arrival, '%d.%m.%y') as date3, " .
				//"IF(order_item_quantity_freezed>0,TRUNCATE(sum(order_item_quantity_freezed * order_item_system_price_freezed), 2), order_item_system_price_freezed) as approved, ". 
				//"TRUNCATE(sum(order_item_quantity * order_item_system_price), 2) as investment, ".
				"TRUNCATE(sum(order_item_quantity_freezed * order_item_system_price_freezed), 2) as approved, ".
				"TRUNCATE(sum(order_item_quantity * order_item_real_system_price), 2) as investment, ".
				"if(order_items_monitoring_group is not null, order_items_monitoring_group, order_item_text) as monitoring_group,  " .
				"order_item_po_number, " .
				"DATE_FORMAT(order_item_reinvoiced, '%d.%m.%y') as date4, " .
				"order_item_reinvoicenbr, " .
				"DATE_FORMAT(order_item_reinvoiced2, '%d.%m.%y') as date42, " .
				"order_item_reinvoicenbr2 " .
				"from order_items ".
				"left join items on order_item_item = item_id ".
				"left join addresses on order_item_supplier_address = address_id ".
				"left join item_types on order_item_type = item_type_id " . 
				"left join project_cost_groupnames on project_cost_groupname_id =  order_item_cost_group ";
}

$sql_grouping = " group by address_shortcut, monitoring_group, " .
             "   date1, date2, date3, order_item_po_number, " .
             "   date4, order_item_reinvoicenbr " .
             "order by address_shortcut ";


$sql_list7 = $sql_base . 
             "where order_item_cost_group = 7 " . 
             " and order_item_order = " . $project["project_order"] . 
             $sql_grouping;


$sql_list2 = $sql_base . 
             "where order_item_cost_group = 2 " .
             "   and order_item_order = " . $project["project_order"] . 
             $sql_grouping;

$sql_list6 = $sql_base . 
             "where order_item_cost_group = 6 " .
             "   and order_item_order = " . $project["project_order"] . 
             $sql_grouping;

$sql_list10 = $sql_base . 
             "where order_item_cost_group = 10 " .
             "   and order_item_order = " . $project["project_order"] . 
             $sql_grouping;

$sql_list9 = $sql_base . 
             "where order_item_cost_group = 9 " .
             "   and order_item_order = " . $project["project_order"] . 
             $sql_grouping;

$sql_list11 = $sql_base . 
             "where order_item_cost_group = 11 " .
             "   and order_item_order = " . $project["project_order"] . 
             $sql_grouping;

$sql_list8 = $sql_base . 
             "where order_item_cost_group = 8 " .
             "   and order_item_order = " . $project["project_order"] . 
             $sql_grouping;


$sql_remarks = "select project_cost_remark_id, " .
               "project_cost_remark_remark " .
               "from project_cost_remarks " . 
               "where project_cost_remark_order = " . $project["project_order"];



/********************************************************************
	Calucalate Shares
*********************************************************************/ 

//local construction work
$shares_p7 = array();

$sql = $sql_list7;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_p7[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_p7[$row["item"]] = "0.00%";
	}

}

if($project_cost_real > 0)
{
	if(array_key_exists(7, $oip_group_totals))
	{
		$share_t7 = number_format(round(100*($grouptotals_real[7]-$oip_group_totals[7]) / $project_cost_real,2),2, ".", "'") . "%";
	}
	else
	{
		$share_t7 = number_format(round(100*$grouptotals_real[7] / $project_cost_real,2),2, ".", "'") . "%";
	}
}
else
{
	$share_t7 = "0.00%";
}

//HQ Supplied Items
$shares_p2 = array();
$shares_p2_hq = array();
$shares_p2_hq_fc = array();

$shares_t2_hq = 0;
$shares_t2_hq_fc = 0;


$group_totals_real_hq_fc = $grouptotals_real[2] + $grouptotals_real[6] + $grouptotals_real[10];

if(array_key_exists(2, $oip_group_totals))
{
	$group_totals_real_hq_fc = $group_totals_real_hq_fc - $oip_group_totals[2];
}
if(array_key_exists(6, $oip_group_totals))
{
	$group_totals_real_hq_fc = $group_totals_real_hq_fc - $oip_group_totals[6];
}
if(array_key_exists(10, $oip_group_totals))
{
	$group_totals_real_hq_fc = $group_totals_real_hq_fc - $oip_group_totals[10];
}


$sql = $sql_list2;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_p2[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_p2[$row["item"]] = "0.00%";
	}

	if($grouptotals_real[2] > 0)
	{
		if(array_key_exists(2, $oip_group_totals))
		{
			$tmp = 100*$row["investment"]/($grouptotals_real[2]-$oip_group_totals[2]);
		}
		else
		{
			$tmp = 100*$row["investment"]/$grouptotals_real[2];
		}
        $shares_p2_hq[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
		$shares_t2_hq = $shares_t2_hq + $tmp;
	}
	else
	{
		$shares_p2_hq[$row["item"]] = "0.00%";
	}

	if($group_totals_real_hq_fc > 0)
	{
		$tmp = 100*$row["investment"]/$group_totals_real_hq_fc;
        $shares_p2_hq_fc[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
		$shares_t2_hq_fc = $shares_t2_hq_fc + $tmp;
	}
	else
	{
		$shares_p2_hq_fc[$row["item"]] = "0.00%";
	}
}

if($project_cost_real > 0)
{
	if(array_key_exists(2, $oip_group_totals))
	{
		$share_t2 = number_format(round(100*($grouptotals_real[2]-$oip_group_totals[2]) / $project_cost_real,2),2, ".", "'") . "%";
	}
	else
	{
		$share_t2 = number_format(round(100*$grouptotals_real[2] / $project_cost_real,2),2, ".", "'") . "%";
	}
}
else
{
	$share_t2 = "0.00%";
}

$shares_t2_hq = number_format(round($shares_t2_hq,2),2, ".", "'") . "%";
$shares_t2_hq_fc = number_format(round($shares_t2_hq_fc,2),2, ".", "'") . "%";

//Freight Charges
$shares_p6 = array();

$shares_p6_hq = array();
$shares_p6_hq_fc = array();

$shares_t6_hq = 0;
$shares_t6_hq_fc = 0;

$sql = $sql_list6;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_p6[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_p6[$row["item"]] = "0.00%";
	}
	
	if($grouptotals_real[6] > 0)
	{
		if(array_key_exists(6, $oip_group_totals))
		{
			$v = $grouptotals_real[6]-$oip_group_totals[6];
			$tmp = $v>0 ? 100*$row["investment"]/$v : 0;
		}
		else
		{
			$tmp = 100*$row["investment"]/$grouptotals_real[6];
		}
        $shares_p6_hq[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
		$shares_t6_hq = $shares_t6_hq + $tmp;
	}
	else
	{
		$shares_p6_hq[$row["item"]] = "0.00%";
	}

	if($group_totals_real_hq_fc > 0)
	{
		$tmp = 100*$row["investment"]/$group_totals_real_hq_fc;
        $shares_p6_hq_fc[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
		$shares_t6_hq_fc = $shares_t6_hq_fc + $tmp;
	}
	else
	{
		$shares_p6_hq_fc[$row["item"]] = "0.00%";
	}
	
}

$shares_t6_hq = number_format(round($shares_t6_hq,2),2, ".", "'") . "%";
$shares_t6_hq_fc = number_format(round($shares_t6_hq_fc,2),2, ".", "'") . "%";

if($project_cost_real > 0)
{
	if(array_key_exists(6, $oip_group_totals))
	{
		$share_t6 = number_format(round(100*($grouptotals_real[6]-$oip_group_totals[6]) / $project_cost_real,2),2, ".", "'") . "%";
	}
	else
	{
	
	}
}
else
{
	$share_t6 = "0.00%";
}

//Fixturing Other Cost
$shares_p10 = array();

$shares_p10_hq = array();
$shares_p10_hq_fc = array();

$shares_t10_hq = 0;
$shares_t10_hq_fc = 0;

$sql = $sql_list10;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_p10[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_p10[$row["item"]] = "0.00%";
	}

	if(($grouptotals_real[10]-$oip_group_totals[10]) > 0)
	{
		if(array_key_exists(10, $oip_group_totals))
		{
			$tmp = 100*$row["investment"]/($grouptotals_real[10]-$oip_group_totals[10]);
		}
		else
		{
			$tmp = 100*$row["investment"]/$grouptotals_real[10];
		}
        $shares_p10_hq[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
		$shares_t10_hq = $shares_t10_hq + $tmp;
	}
	else
	{
		$shares_p10_hq[$row["item"]] = "0.00%";
	}

	if($group_totals_real_hq_fc > 0)
	{
		$tmp = 100*$row["investment"]/$group_totals_real_hq_fc;
        $shares_p10_hq_fc[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
		$shares_t10_hq_fc = $shares_t10_hq_fc + $tmp;
	}
	else
	{
		$shares_p10_hq_fc[$row["item"]] = "0.00%";
	}
}

$shares_t10_hq = number_format(round($shares_t10_hq,2),2, ".", "'") . "%";
$shares_t10_hq_fc = number_format(round($shares_t10_hq_fc,2),2, ".", "'") . "%";

if($project_cost_real > 0)
{
	if(array_key_exists(10, $oip_group_totals))
	{
		$share_t10 = number_format(round(100*($grouptotals_real[10]-$oip_group_totals[10]) / $project_cost_real,2),2, ".", "'") . "%";
	}
	else
	{
		$share_t10 = number_format(round(100*$grouptotals_real[10] / $project_cost_real,2),2, ".", "'") . "%";
	}
}
else
{
	$share_t10 = "0.00%";
}

//Architectural Cost
$shares_p9 = array();

$sql = $sql_list9;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_p9[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_p9[$row["item"]] = "0.00%";
	}
}

if($project_cost_real > 0)
{
	if(array_key_exists(9, $oip_group_totals))
	{
		$share_t9 = number_format(round(100*($grouptotals_real[9]-$oip_group_totals[9]) / $project_cost_real,2),2, ".", "'") . "%";
	}
	else
	{
		$share_t9 = number_format(round(100*$grouptotals_real[9] / $project_cost_real,2),2, ".", "'") . "%";
	}
}
else
{
	$share_t9 = "0.00%";
}

//Equipment Cost
$shares_p11 = array();

$sql = $sql_list11;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_p11[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_p11[$row["item"]] = "0.00%";
	}
}

if($project_cost_real > 0)
{
	if(array_key_exists(11, $oip_group_totals))
	{
		$share_t11 = number_format(round(100*($grouptotals_real[11]-$oip_group_totals[11]) / $project_cost_real,2),2, ".", "'") . "%";
	}
	else
	{
		$share_t11 = number_format(round(100*$grouptotals_real[11] / $project_cost_real,2),2, ".", "'") . "%";	
	}
}
else
{
	$share_t11 = "0.00%";
}

//Other Cost
$shares_p8 = array();

$sql = $sql_list8;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_p8[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_p8[$row["item"]] = "0.00%";
	}
}



if($project_cost_real > 0)
{
	if(array_key_exists(8, $oip_group_totals))
	{
		$share_t8 = number_format(round(100*($grouptotals_real[8]-$oip_group_totals[8]) / $project_cost_real,2),2, ".", "'") . "%";
	}
	else
	{
		$share_t8 = number_format(round(100*$grouptotals_real[8] / $project_cost_real,2),2, ".", "'") . "%";
	}
}
else
{
	$share_t8 = "0.00%";
}



//Cots of Catalogue items in projects
$shares_oip = array();

$res = mysql_query($sql_oip) or dberror($sql_oip);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_oip[$row["order_items_in_project_order_item_id"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_oip[$row["order_items_in_project_order_item_id"]] = "0.00%";
	}

}

if($project_cost_real > 0)
{
	$share_oip = number_format(round(100*$grouptotals_oip / $project_cost_real,2),2, ".", "'") . "%";
}
else
{
	$share_oip = "0.00%";
}

/********************************************************************
    prepare pdf Data
*********************************************************************/

$captions1 = array();
$captions1[] = "Project Number:";
$captions1[] = "Product Line / Type / POS Type Subclass:";
$captions1[] = "Project Starting Date / Shop Opening Date:";
$captions1[] = "Status:";
$captions1[] = "Project Leader / Logistics Coordinator:";
//$captions1[] = "Project Legal Type / Gross/Total/Sales Surface in sqms:";
$captions1[] = "Project Legal Type / Total/Sales Surface in sqms:";
$captions1[] = "Budget approved by Client in " . $order_currency["symbol"] . ":";
$captions1[] = "";

$data1 = array();
$data1[] = $project["project_number"];
$data1[] = $project["product_line_name"] . " / " . $project["postype_name"] . " / " . $project["possubclass_name"];
$data1[] = to_system_date($project["order_date"]) . " / " .  to_system_date($project["project_actual_opening_date"]);
$data1[] = $project["order_actual_order_state_code"] . " " . substr($order_state_name, 0, 30) . '...';
$data1[] = $retail_coordinator["firstname"] . " " . $retail_coordinator["name"] . " / " . $retail_operator["firstname"] . " " . $retail_operator["name"];
//$data1[] = $project["project_costtype_text"] . " / " . $project_cost_gross_sqms . " / " . $project_cost_total_sqms . " / " . $project_cost_sqms. " sqms";
$data1[] = $project["project_costtype_text"] . " / " . $project_cost_total_sqms . " / " . $project_cost_sqms. " sqms";
$data1[] = number_format($project_cost_budget, 2, ".", "'");
$data1[] = "";

$captions2 = array();
$captions2[] = "Amounts in " . $order_currency["symbol"];
$captions2[] = "Totals:";
$captions2[] = "- Local Construction:";
$captions2[] = "- Store fixturing / Furniture:";
$captions2[] = "- Architectural Services:";
$captions2[] = "- Equipment:";
$captions2[] = "- Other:";
if($landlord_contribution > 0)
{
	$captions2[] = "- Landlord's Contribution:";
}
$captions2[] = "- per m2 of sales surface:";
    
$data2 = array();
$tmp = array();

if($project["project_cost_type"] == 1) // corporate
{
	$tmp[] = "Budget appr.";
	$tmp[] = "CER";
	$tmp[] = "KL approved";
	$tmp[] = "KL additional";
	$tmp[] = "Real Cost";
	$tmp[] = "Difference";
	$tmp[] = "% KL/Real";
}
else
{
	
	$tmp[] = "";
	$tmp[] = "AF";
	$tmp[] = "Budget appr.";
	$tmp[] = "Real Cost";
	$tmp[] = "Difference";
	$tmp[] = "% AP/Real";
}



$data2[] = $tmp;


/*Difference to budget
$tmp = $project_cost_real - $budget_total;
if($budget_total > 0)
{
	$tmpp = number_format(round(100*$tmp/$budget_total, 2), 2, ".", "'");
}
else
{
	$tmpp = "0.00%";
}
*/



if($project["project_cost_type"] == 1) // corporate
{
	
	//Difference to KL approved CER
	$tmp = $project_cost_real - $total_from_cer_approved - $total_from_cer_additional_approved;
	if($total_from_cer_approved > 0)
	{
		$tmpp = number_format(round(100*$tmp/($total_from_cer_approved + $total_from_cer_additional_approved), 2), 2, ".", "'") . '%';
	}
	else
	{
		$tmpp = "0.00%";
	}
	$data2[] = array(number_format($budget_total, 2, ".", "'"), number_format($total_from_cer,2, ".", "'"), number_format($total_from_cer_approved,2, ".", "'"), number_format($total_from_cer_additional_approved,2, ".", "'"), number_format($project_cost_real, 2, ".", "'"), number_format($tmp, 2, ".", "'"), $tmpp);
}
else
{
	//Difference to Budget Approved
	$tmp = $project_cost_real - $budget_total;
	if($budget_total > 0)
	{
		$tmpp = number_format(round(100*$tmp/$budget_total, 2), 2, ".", "'");
	}
	else
	{
		$tmpp = "0.00%";
	}
	
	$data2[] = array('', number_format($total_from_cer,2, ".", "'"), number_format($budget_total,2, ".", "'"), number_format($project_cost_real, 2, ".", "'"), number_format($tmp, 2, ".", "'"), $tmpp . '%');
}


//if($budget_total > 0)
//{
	$t1 = number_format($grouptotals_real[7], 2, ".", "'");
	$t2 = number_format($grouptotals_real[2] + $grouptotals_real[6] + $grouptotals_real[10], 2, ".", "'");
	$t3 = number_format($grouptotals_real[9], 2, ".", "'");
	$t4 = number_format($grouptotals_real[11], 2, ".", "'");
	$t5 = number_format($grouptotals_real[8], 2, ".", "'");

	
	//Difference to CER KL approved amount
	if($project["project_cost_type"] == 1) // corporate
	{
		$tmp1 = number_format($grouptotals_real[7] - $cer_investments_approved[0] - $cer_investments_kl_additional_approved[0], 2, ".", "'");
		$tmp2 = number_format($grouptotals_real[2] + $grouptotals_real[6] + $grouptotals_real[10] - $cer_investments_approved[1] - $cer_investments_kl_additional_approved[1], 2, ".", "'");
		$tmp3 = number_format($grouptotals_real[9] - $cer_investments_approved[2] - $cer_investments_kl_additional_approved[2], 2, ".", "'");
		$tmp4 = number_format($grouptotals_real[11] - $cer_investments_approved[3] - $cer_investments_kl_additional_approved[3], 2, ".", "'");
		$tmp5 = number_format($grouptotals_real[8] - $cer_investments_approved[4] - $cer_investments_kl_additional_approved[4], 2, ".", "'");
	}
	else // Difference to Budget
	{
		
		$tmp1 = number_format($grouptotals_real[7] - $project_cost_construction, 2, ".", "'");
		$tmp2 = number_format($grouptotals_real[2] + $grouptotals_real[6] + $grouptotals_real[10] - $project_cost_fixturing, 2, ".", "'");
		$tmp3 = number_format($grouptotals_real[9] - $project_cost_architectural, 2, ".", "'");
		$tmp4 = number_format($grouptotals_real[11] - $project_cost_equipment, 2, ".", "'");
		$tmp5 = number_format($grouptotals_real[8] - $project_cost_other, 2, ".", "'");
	}

	
	

	//Difference in Percen to kl approved cer amount
	if($project["project_cost_type"] == 1) // corporate
	{
		if($cer_investments_approved[0] > 0 and $cer_investments_kl_additional_approved[0] > 0)
		{
			$tmpp1 = number_format(round(100*($grouptotals_real[7] - $cer_investments_approved[0] - $cer_investments_kl_additional_approved[0])/($cer_investments_approved[0] +  $cer_investments_kl_additional_approved[0]), 2), 2, ".", "'") . '%';
		}
		elseif($cer_investments_approved[0] > 0)
		{
			$tmpp1 = number_format(round(100*($grouptotals_real[7] - $cer_investments_approved[0])/($cer_investments_approved[0]), 2), 2, ".", "'") . '%';
		}
		else
		{
			$tmpp1 = "0.00%";
		}

		if($cer_investments_approved[1] > 0 and $cer_investments_kl_additional_approved[1] > 0)
		{
			$tmpp2 = number_format(round(100*($grouptotals_real[2] + $grouptotals_real[6] + $grouptotals_real[10] - $cer_investments_approved[1] - $cer_investments_kl_additional_approved[1])/($cer_investments_approved[1] +  $cer_investments_kl_additional_approved[1]), 2), 2, ".", "'") . '%';
		}
		elseif($cer_investments_approved[1] > 0)
		{
			$tmpp2 = number_format(round(100*($grouptotals_real[2] + $grouptotals_real[6] + $grouptotals_real[10] - $cer_investments_approved[1])/($cer_investments_approved[1]), 2), 2, ".", "'") . '%';
		}
		else
		{
			$tmpp2 = "0.00%";
		}

		if($cer_investments_approved[2] > 0 and $cer_investments_kl_additional_approved[2] > 0)
		{
			$tmpp3 = number_format(round(100*($grouptotals_real[9] - $cer_investments_approved[2] - $cer_investments_kl_additional_approved[2])/($cer_investments_approved[2]+ $cer_investments_kl_additional_approved[2]), 2), 2, ".", "'") . '%';
		}
		elseif($cer_investments_approved[2] > 0)
		{
			$tmpp3 = number_format(round(100*($grouptotals_real[9] - $cer_investments_approved[2])/($cer_investments_approved[2]), 2), 2, ".", "'") . '%';
		}
		else
		{
			$tmpp3 = "0.00%";
		}

		if($cer_investments_approved[3] > 0 and $cer_investments_kl_additional_approved[3] > 0)
		{
			$tmpp4 = number_format(round(100*($grouptotals_real[11] - $cer_investments_approved[3] - $cer_investments_kl_additional_approved[3])/($cer_investments_approved[3]+  $cer_investments_kl_additional_approved[3]), 2), 2, ".", "'") . '%';
		}
		elseif($cer_investments_approved[3] > 0)
		{
			$tmpp4 = number_format(round(100*($grouptotals_real[11] - $cer_investments_approved[3])/($cer_investments_approved[3]), 2), 2, ".", "'") . '%';
		}
		else
		{
			$tmpp4 = "0.00%";
		}

		if($cer_investments_approved[4] > 0 and $cer_investments_kl_additional_approved[4] > 0)
		{
			$tmpp5 = number_format(round(100*($grouptotals_real[8] - $cer_investments_approved[4] - $cer_investments_kl_additional_approved[4])/($cer_investments_approved[4]+  $cer_investments_kl_additional_approved[4]), 2), 2, ".", "'") . '%';
		}
		elseif($cer_investments_approved[4] > 0)
		{
			$tmpp5 = number_format(round(100*($grouptotals_real[8] - $cer_investments_approved[4])/($cer_investments_approved[4]), 2), 2, ".", "'") . '%';
		}

		else
		{
			$tmpp5 = "0.00%";
		}
	}
	else //Differenc in percen to budget
	{
		if($project_cost_construction > 0)
		{
			$tmpp1 = number_format(round(100*($grouptotals_real[7] - $project_cost_construction)/$project_cost_construction, 2), 2, ".", "'") . '%';
		}
		else
		{
			$tmpp1 = "0.00%";
		}

		if($project_cost_fixturing > 0)
		{
			$tmpp2 = number_format(round(100*($grouptotals_real[2] + $grouptotals_real[6] + $grouptotals_real[10] - $project_cost_fixturing)/$project_cost_fixturing, 2), 2, ".", "'") . '%';
		}
		else
		{
			$tmpp2 = "0.00%";
		}

		if($project_cost_architectural > 0)
		{
			$tmpp3 = number_format(round(100*($grouptotals_real[9] - $project_cost_architectural)/$project_cost_architectural, 2), 2, ".", "'") . '%';
		}
		else
		{
			$tmpp3 = "0.00%";
		}

		if($project_cost_equipment > 0)
		{
			$tmpp4 = number_format(round(100*($grouptotals_real[11] - $project_cost_equipment)/$project_cost_equipment, 2), 2, ".", "'") . '%';
		}
		else
		{
			$tmpp4 = "0.00%";
		}

		if($project_cost_other > 0)
		{
			$tmpp5 = number_format(round(100*($grouptotals_real[8] - $project_cost_other)/$project_cost_other, 2), 2, ".", "'") . '%';
		}
		else
		{
			$tmpp5 = "0.00%";
		}
	
	}


	if($project["project_cost_type"] == 1) // corporate
	{
	
		$data2[] = array(number_format($project_cost_construction, 2, ".", "'"), number_format($cer_investments[0], 2, ".", "'"), number_format($cer_investments_approved[0], 2, ".", "'"), number_format($cer_investments_kl_additional_approved[0], 2, ".", "'"), $t1, $tmp1, $tmpp1);
		$data2[] = array(number_format($project_cost_fixturing, 2, ".", "'"),number_format($cer_investments[1], 2, ".", "'"), number_format($cer_investments_approved[1], 2, ".", "'"), number_format($cer_investments_kl_additional_approved[1], 2, ".", "'"), $t2 , $tmp2, $tmpp2);
		$data2[] = array(number_format($project_cost_architectural, 2, ".", "'"), number_format($cer_investments[2], 2, ".", "'"), number_format($cer_investments_approved[2], 2, ".", "'"), number_format($cer_investments_kl_additional_approved[2], 2, ".", "'"), $t3, $tmp3, $tmpp3);
		$data2[] = array(number_format($project_cost_equipment, 2, ".", "'"), number_format($cer_investments[3], 2, ".", "'"), number_format($cer_investments_approved[3], 2, ".", "'"), number_format($cer_investments_kl_additional_approved[3], 2, ".", "'"), $t4, $tmp4, $tmpp4);
		$data2[] = array(number_format($project_cost_other, 2, ".", "'"), number_format($cer_investments[4], 2, ".", "'"), number_format($cer_investments_approved[4], 2, ".", "'"), number_format($cer_investments_kl_additional_approved[4], 2, ".", "'"), $t5, $tmp5, $tmpp5);
		

		if($project_cost_sqms > 0)
		{
			$project_cost_sqms1 =  $budget_total / $project_cost_sqms;
			$project_cost_sqms1_cer =  $total_from_cer / $project_cost_sqms;
			$project_cost_sqms1_cer_approved =  $total_from_cer_approved / $project_cost_sqms;
			$project_cost_sqms1_cer_additional_approved =  $total_from_cer_additional_approved / $project_cost_sqms;
			$project_cost_sqms1_real =  $project_cost_real/ $project_cost_sqms;
		}
		else
		{
			$project_cost_sqms1 = 0;
			$project_cost_sqms1_cer = 0;
			$project_cost_sqms1_cer_approved = 0;
			$project_cost_sqms1_real = 0;
		}


		if($project_cost_sqms1_cer_approved > 0)
		{
			$tmpp5 = number_format(round(100*($project_cost_sqms1_real - $project_cost_sqms1_cer_approved)/$project_cost_sqms1_cer_approved, 2), 2, ".", "'");
		}
		else
		{
			$tmpp5 = "0.00%";
		}

		if($landlord_contribution > 0)
		{
			$data2[] = array("",  "",  "", "", number_format(-1*$landlord_contribution, 2, ".", "'"), "", "");
		}
		
		

		$data2[] = array(number_format($project_cost_sqms1, 2),  number_format($project_cost_sqms1_cer, 2, ".", "'"),  number_format($project_cost_sqms1_cer_approved, 2, ".", "'"), number_format($project_cost_sqms1_cer_additional_approved, 2, ".", "'"), number_format($project_cost_sqms1_real, 2, ".", "'"), "", "");
	}
	else
	{
		$data2[] = array('', number_format($cer_investments[0], 2, ".", "'"), number_format($project_cost_construction, 2), $t1, $tmp1, $tmpp1);
		$data2[] = array('',number_format($cer_investments[1], 2, ".", "'"), number_format($project_cost_fixturing, 2), $t2 , $tmp2, $tmpp2);
		$data2[] = array('', number_format($cer_investments[2], 2, ".", "'"), number_format($project_cost_architectural, 2), $t3, $tmp3, $tmpp3);
		$data2[] = array('', number_format($cer_investments[3], 2, ".", "'"), number_format($project_cost_equipment, 2), $t4, $tmp4, $tmpp4);
		$data2[] = array('', number_format($cer_investments[4], 2, ".", "'"), number_format($project_cost_other, 2), $t5, $tmp5, $tmpp5);
		

		if($project_cost_sqms > 0)
		{
			$project_cost_sqms1 =  $budget_total / $project_cost_sqms;
			$project_cost_sqms1_cer =  $total_from_cer / $project_cost_sqms;
			$project_cost_sqms1_cer_approved =  $total_from_cer_approved / $project_cost_sqms;
			$project_cost_sqms1_real =  $project_cost_real/ $project_cost_sqms;
		}
		else
		{
			$project_cost_sqms1 = 0;
			$project_cost_sqms1_cer = 0;
			$project_cost_sqms1_cer_approved = 0;
			$project_cost_sqms1_real = 0;
		}


		if($project_cost_sqms1_cer_approved > 0)
		{
			$tmpp5 = number_format(round(100*($project_cost_sqms1_real - $project_cost_sqms1_cer_approved)/$project_cost_sqms1_cer_approved, 2), 2, ".", "'");
		}
		else
		{
			$tmpp5 = "0.00%";
		}

		if($landlord_contribution > 0)
		{
			$data2[] = array("",  "",  "", "", number_format(-1*$landlord_contribution, 2, ".", "'"), "", "");
		}
		

		$data2[] = array('',  number_format($project_cost_sqms1_cer, 2, ".", "'"),  number_format($project_cost_sqms1, 2, ".", "'"), number_format($project_cost_sqms1_real, 2, ".", "'"), "", "");

	}
/*
}
else
{
    $data2[] = array(" ", " ", " ", " ");
    $data2[] = array(" ", " ", " ", " ");
    $data2[] = array(" ", " ", " ", " ");
    $data2[] = array(" ", " ", " ", " ");
    $data2[] = array(" ", " ", " ", " ");
	$data2[] = array(" ", " ", " ", " ");
	$data2[] = array(" ", " ", " ", " ");
	$data2[] = array(" ", " ", " ", " ");
}
*/


global $page_title;
$page_title = "Cost Monitoring Sheet: Project " . $project["project_number"];

/********************************************************************
    prepare pdf 
*********************************************************************/
require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');


class MYPDF extends TCPDF
{
    //Page header
    function Header()
    {
		global $page_title;
		//Logo
        $this->Image('../pictures/brand_logo.jpg',10,8,33);
        //arialn bold 15
        $this->SetFont('arialn','B',10);
        //Move to the right
        $this->Cell(80);
        //Title
        $this->Cell(0,30, $page_title, 0, 0, 'R');
        //Line break
        $this->Ln(12);

    }

    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //arialn italic 8
        $this->SetFont('arialn','I',8);
        //Page number
        $this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
    }


    function group_header($group, $currency)
    {

        $this->SetFont('arialn','B',10);
        $this->Cell(278 ,4, $group, 1, 0, 'L', 1);
        $this->Ln();
        $this->SetFont('arialn','B',7);
        $this->Cell(6,4, "No.", "LTR", 0, 'L', 1);
        $this->Cell(40,4,"Supplier","LTR", 0, 'L', 1);
        $this->Cell(60,4,"Description","LTR", 0, 'L', 1);
        $this->Cell(16,4,"Approved" ,"LTR", 0, 'R', 1);
        $this->Cell(13,4,"Ordered","LTR", 0, 'L', 1);
        $this->Cell(13,4,"Pick Up","LTR", 0, 'L', 1);
        $this->Cell(13,4,"Arrival","LTR", 0, 'L', 1);
        $this->Cell(23,4,"P.O. Number","LTR", 0, 'L', 1);
        $this->Cell(19,4,"Real Cost " . $currency , "LTR", 0, 'R', 1);
        $this->Cell(15,4,"Reinvoiced","LTR", 0, 'L', 1);
        $this->Cell(24,4,"Invoice Nbr.", "LTR" , 0, 'L', 1);
        $this->Cell(12,4,"% excl.","LTR", 0, 'R', 1);
        $this->Cell(12,4,"% incl.","LTR", 0, 'R', 1);
        $this->Cell(12,4,"% Cost","LTR", 0, 'R31.10.05', 1);
        $this->Ln();

        $this->Cell(6,4, "", "LBR", 0, 'L', 1);
        $this->Cell(40,4,"", "LBR", 0, 'L', 1);
        $this->Cell(60,4,"", "LBR", 0, 'L', 1);
        $this->Cell(16,4,"Budget " . $currency, "LBR", 0, 'R', 1);
        $this->Cell(13,4,"", "LBR", 0, 'L', 1);
        $this->Cell(13,4,"", "LBR", 0, 'L', 1);
        $this->Cell(13,4,"", "LBR", 0, 'L', 1);
        $this->Cell(23,4,"", "LBR", 0, 'L', 1);
        $this->Cell(19,4,"" , "LBR", 0, 'R', 1);
        $this->Cell(15,4,"", "LBR", 0, 'L', 1);
        $this->Cell(24,4,"", "LBR", 0, 'L', 1);
        $this->Cell(12,4,"Freight", "LBR", 0, 'R', 1);
        $this->Cell(12,4,"Freight", "LBR", 0, 'R', 1);
        $this->Cell(12,4,"", "LBR", 0, 'L', 1);
        $this->Ln();
    }

	function simple_group_header($group)
    {

        $this->SetFont('arialn','B',10);
        $this->Cell(278 ,4, $group, 1, 0, 'L', 1);
        $this->Ln();

    }

	function sub_group_header($sub_group, $currency)
    {

		$this->SetFont('arialn','B',8);
        $this->Cell(278 ,4, $sub_group, 1, 0, 'L', 1);
        $this->Ln();
		$this->SetFont('arialn','B',7);
        $this->Cell(6,4, "No.", "LTR", 0, 'L', 1);
        
		 
		if($sub_group == "HQ Supplied Items"
			or $sub_group == "Freight Charges"
			or $sub_group == "Other") {
			$this->Cell(15,4,"Supplier","LTR", 0, 'L', 1);
			$this->Cell(85,4,"Description","LTR", 0, 'L', 1);
		}
		else {
			$this->Cell(40,4,"Supplier","LTR", 0, 'L', 1);
			$this->Cell(60,4,"Description","LTR", 0, 'L', 1);
		}
		$this->Cell(16,4,"Approved" ,"LTR", 0, 'R', 1);
        $this->Cell(13,4,"Ordered","LTR", 0, 'L', 1);
        $this->Cell(13,4,"Pick Up","LTR", 0, 'L', 1);
        $this->Cell(13,4,"Arrival","LTR", 0, 'L', 1);
        $this->Cell(23,4,"P.O. Number","LTR", 0, 'L', 1);
        $this->Cell(19,4,"Real Cost " . $currency , "LTR", 0, 'R', 1);
        $this->Cell(15,4,"Reinvoiced","LTR", 0, 'L', 1);
        $this->Cell(24,4,"Invoice Nbr.", "LTR" , 0, 'L', 1);
        $this->Cell(12,4,"% excl.","LTR", 0, 'R', 1);
        $this->Cell(12,4,"% incl.","LTR", 0, 'R', 1);
        $this->Cell(12,4,"% Cost","LTR", 0, 'R31.10.05', 1);
        $this->Ln();

        $this->Cell(6,4, "", "LBR", 0, 'L', 1);
        
		if($sub_group == "HQ Supplied Items"
			or $sub_group == "Freight Charges"
			or $sub_group == "Other") {
			$this->Cell(15,4,"", "LBR", 0, 'L', 1);
			$this->Cell(85,4,"", "LBR", 0, 'L', 1);
		}
		else {
			$this->Cell(40,4,"", "LBR", 0, 'L', 1);
			$this->Cell(60,4,"", "LBR", 0, 'L', 1);
		}
        $this->Cell(16,4,"Budget " . $currency, "LBR", 0, 'R', 1);
        $this->Cell(13,4,"", "LBR", 0, 'L', 1);
        $this->Cell(13,4,"", "LBR", 0, 'L', 1);
        $this->Cell(13,4,"", "LBR", 0, 'L', 1);
        $this->Cell(23,4,"", "LBR", 0, 'L', 1);
        $this->Cell(19,4,"" , "LBR", 0, 'R', 1);
        $this->Cell(15,4,"", "LBR", 0, 'L', 1);
        $this->Cell(24,4,"", "LBR", 0, 'L', 1);
        $this->Cell(12,4,"Freight", "LBR", 0, 'R', 1);
        $this->Cell(12,4,"Freight", "LBR", 0, 'R', 1);
        $this->Cell(12,4,"", "LBR", 0, 'L', 1);
        $this->Ln();
        
	}

	function simple_sub_group_header($sub_group, $y)
    {
		$this->SetY($y);
		$this->SetFont('arialn','B',8);
        $this->Cell(278 ,4, $sub_group, 1, 0, 'L', 1);
        $this->Ln();
	}

}


//Instanciation of inherited class
$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 22, 10);

//$pdf->setCellHeightRatio(0.8);

$pdf->Open();

$pdf->SetLineWidth(0.1);

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->AddPage();

$pdf->SetFillColor(240, 240, 240); 
$pdf_linebreak_after = "180";


// output project header informations
$pdf->SetFont('arialn','B',10);
$pdf->Cell(278,5, $project["postype_name"] .": " . $shop,1);
$pdf->Cell(13,5," ",0);

$pdf->Ln();


$pdf->SetFont('arialn','',8);

/*
foreach($captions1 as $key=>$value)
{
    $pdf->Cell(68,4,$captions1[$key],1);
    $pdf->Cell(50,4,$data1[$key],1);

    $pdf->Cell(4,4," ",0);

    $pdf->Cell(34,4,$captions2[$key],1);
    
    //$pdf->MultiCell(65,4,$data2[$key],1, 0, "R");
    
	$line = $data2[$key];

    $pdf->Cell(18,4,$line[0],1, 0, "R");
    
	$pdf->Cell(18,4,$line[1],1, 0, "R");
    $pdf->Cell(18,4,$line[2],1, 0, "R");
	
	$pdf->Cell(18,4,$line[3],1, 0, "R");

    $pdf->Cell(17,4,$line[4],1, 0, "R");
    $pdf->Cell(15,4,$line[5],1, 0, "R");

    $pdf->Ln();
}
*/

$num_lines = 7;
if($landlord_contribution > 0)
{
	$num_lines = 8;
}

for($i=0;$i<=$num_lines;$i++)
{
    
	$pdf->Cell(68,4,$captions1[$i],1);
	$pdf->Cell(50,4,$data1[$i],1);

	$pdf->Cell(4,4," ",0);

	$pdf->Cell(34,4,$captions2[$i],1);
    
    
	$line = $data2[$i];

	for($k=0;$k<$num_lines;$k++)
	{
		if(!array_key_exists($k, $line))
		{
			$line[$k] = "";
		}
	}

	
	$pdf->Cell(18,4,$line[0],1, 0, "R");
	
	$pdf->Cell(18,4,$line[1],1, 0, "R");
	$pdf->Cell(18,4,$line[2],1, 0, "R");
	
	$pdf->Cell(18,4,$line[3],1, 0, "R");

	$pdf->Cell(17,4,$line[4],1, 0, "R");
	$pdf->Cell(17,4,$line[5],1, 0, "R");
	$pdf->Cell(16,4,$line[6],1, 0, "R");

	$pdf->Ln();
}



$pdf->Ln();
if($show_budget_in_loc == true)
{
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(90,5, "All amounts for HQ supplied items converted from CHF into LOC are for reference only as the exchange rate may change.", 0);
	$pdf->Ln();
}


// print local construction cost
$nr = 1;

if($num_recs7 > 0)
{

    $y = $pdf->getY();
    if($y >= $pdf_linebreak_after)
    {
        $pdf->AddPage();
        $pdf->group_header("Local Construction", $order_currency["symbol"]);
    }
    else
    {
        $pdf->group_header("Local Construction", $order_currency["symbol"]);
    }
    
    $pdf->SetFont('arialn','',7);
    
    $res = mysql_query($sql_list7);

    while ($row = mysql_fetch_assoc($res))
    {
        
        $y = $pdf->getY();
        if($y >= $pdf_linebreak_after)
        {
            $pdf->AddPage();
            $pdf->group_header("Local Construction", $order_currency["symbol"]);
        }

        if($row["monitoring_group"])
        {
            $nls = ceil(strlen($row["monitoring_group"]) /55);
            $h = 3.5 * $nls;
        }
        else
        {
            $nls = 1;
            $h = 3.5;
        }

        
		$x = $pdf->GetX();
        $y = $pdf->GetY();

        $pdf->SetFont('arialn','',7);

		$cell_height = 3.5;
		
		$cms_remark_referenced_line_numbers[$row["monitoring_group"]] = $nr;

		if(in_array($row["monitoring_group"], $cms_remark_keys))
		{
			$pdf->Cell(6, $cell_height, $nr . "*", 0);
		}
		else
		{
			$pdf->Cell(6, $cell_height, $nr, 0);
		}

        $pdf->Rect($x, $y, 6, $h);
        $pdf->SetXY($x+6,$y);

        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(40, $cell_height, substr($row["address_shortcut"], 0,30), 0);
        $pdf->Rect($x, $y, 40, $h);
        $pdf->SetXY($x+40,$y);
        
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        if($row["monitoring_group"])
        {
			$pdf->MultiCell(60, $cell_height,  $row["monitoring_group"], 1, '', 0, 1, $x, $y, 0, 0, false, false);
            $pdf->SetXY($x+60,$y);
        }
        else
        {
            $pdf->MultiCell(60, 3.5,  "", 1, '', 0, 1, $x, $y, 0, 0, false, false);
            $pdf->Rect($x, $y, 60, $h);
            $pdf->SetXY($x+60,$y);
        }


        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(16,$cell_height, number_format($row["approved"],2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 16, $h);
        $pdf->SetXY($x+16,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date1"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date2"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date3"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        //$pdf->Cell(23,$cell_height, $row["order_item_po_number"], 0);
		$pdf->MultiCell(23, $cell_height,  $row["order_item_po_number"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 3.5, 'T', true);

        $pdf->Rect($x, $y, 23, $h);
        $pdf->SetXY($x+23,$y);

		
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(19,$cell_height, number_format($row["investment"],2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 19, $h);
        
		
		$pdf->SetXY($x+19,$y);
        
        
		$x = $pdf->GetX();
        $y = $pdf->GetY();
		if($row["order_item_reinvoicenbr2"])
		{
			$pdf->MultiCell(15,$cell_height, $row["date4"] . "\r\n" . $row["date42"], 'T', '', 0, 1, $x, $y, 0, 0, false, false, 10, 'T', true);
			$pdf->Rect($x, $y, 15, 2*$h);
			$pdf->SetXY($x+15,$y);
		}
		else
		{
			$pdf->Cell(15,$cell_height, $row["date4"], 0);
			$pdf->Rect($x, $y, 15, $h);
			$pdf->SetXY($x+15,$y);
		}

		$x = $pdf->GetX();
        $y = $pdf->GetY();
		if($row["order_item_reinvoicenbr2"])
		{
			//$pdf->Cell(1,24, "", 0);
			$pdf->SetXY($x,$y);
			$pdf->MultiCell(24, $cell_height,  $row["order_item_reinvoicenbr"] . "\r\n" . $row["order_item_reinvoicenbr2"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 10, 'T', true);
			$pdf->Rect($x, $y, 24, 2*$h);
			$pdf->SetXY($x+24,$y);

		}
		else
		{
			$pdf->Cell(1,$cell_height, "", 0);
			$pdf->SetXY($x,$y);
			$pdf->MultiCell(24, $cell_height,  $row["order_item_reinvoicenbr"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 3.5, 'T', true);
			$pdf->SetXY($x+24,$y);
			$y = $pdf->GetY();
		}

        $x = $pdf->GetX();
        
		$pdf->Cell(12,$cell_height, "", 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height, "", 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height, $shares_p7[$row["item"]], 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);

		if($row["order_item_reinvoicenbr2"])
		{
			$y = $pdf->SetY($pdf->GetY() + $h);
		}

        for($i=1;$i<=$nls;$i++)
        {
            $pdf->Ln();
        }

        $nr = $nr + 1;
    }


	
    $pdf->SetFont('arialn','B',7);
    $pdf->Cell(106,4, "Total Local Construction", 1, 0, 'L', 1);
    $pdf->Cell(16,4, number_format($grouptotals_investment[7],2, ".", "'"), 1, 0, "R", 1);
    $pdf->Cell(13,4, "", 1, 0, 'L', 1);
    $pdf->Cell(13,4, "", 1, 0, 'L', 1);
    $pdf->Cell(13,4, "", 1, 0, 'L', 1);
    $pdf->Cell(23,4, "", 1, 0, 'L', 1);
    if(array_key_exists(7, $oip_group_totals))
	{
		$pdf->Cell(19,4, number_format($grouptotals_real[7] - $oip_group_totals[7],2, ".", "'"), 1, 0, "R", 1);
	}
	else
	{
		$pdf->Cell(19,4, number_format($grouptotals_real[7],2, ".", "'"), 1, 0, "R", 1);
	}
	
    $pdf->Cell(15,4, "", 1, 0, 'L', 1);
    $pdf->Cell(24,4, "", 1, 0, 'L', 1);
    $pdf->Cell(12,4, "", 1,0, "R", 1);
    $pdf->Cell(12,4, "", 1,0, "R", 1);
    $pdf->Cell(12,4,  $share_t7, 1,0, "R", 1);
    $pdf->Ln();

    $pdf->Ln();
}


if($num_recs2 > 0)
{

    $y = $pdf->getY();
    if($y >= $pdf_linebreak_after)
    {
        $pdf->AddPage();
        $pdf->simple_group_header("Store fixturing / Furniture");
		$pdf->sub_group_header("HQ Supplied Items", $order_currency["symbol"]);
    }
    else
    {
        $pdf->simple_group_header("Store fixturing / Furniture");
	    $pdf->sub_group_header("HQ Supplied Items", $order_currency["symbol"]);
    }
    
    $pdf->SetFont('arialn','',7);
    
    $res = mysql_query($sql_list2);

    while ($row = mysql_fetch_assoc($res))
    {
        
        
		if($row["monitoring_group"])
        {
			$nls = ceil(strlen(str_replace("\r\n", " ", $row["monitoring_group"])) / 55);
            $h = 3.5 * $nls;
        }
        else
        {
            $nls = 1;
            $h = 3.5;
        }
		
		
		
		$y = $pdf->getY();
        if($y+$h >= $pdf_linebreak_after)
        {
            $pdf->AddPage();
            $pdf->simple_group_header("Store fixturing / Furniture");
			$pdf->sub_group_header("HQ Supplied Items", $order_currency["symbol"]);
        }
		
        $x = $pdf->GetX();
        $y = $pdf->GetY();

        
		$pdf->SetFont('arialn','',7);


		$cell_height = 3.5;

		$cms_remark_referenced_line_numbers[$row["monitoring_group"]] = $nr;
		
		if(in_array($row["monitoring_group"], $cms_remark_keys))
		{
			$pdf->Cell(6, $cell_height, $nr . "*", 0);
		}
		else
		{
			$pdf->Cell(6, $cell_height, $nr, 0);
		}
        $pdf->Rect($x, $y, 6, $h);
        $pdf->SetXY($x+6,$y);

        
		$x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(15, $cell_height, $row["address_shortcut"], 0);
        $pdf->Rect($x, $y, 15, $h);
        $pdf->SetXY($x+15,$y);
        
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        if($row["monitoring_group"])
        {
			$pdf->MultiCell(85, $h,  str_replace("\r\n", " ", $row["monitoring_group"]), 1, 'L', 0, 1, $x, $y, 0, 0, false, false, 0, 'T', false);
            $pdf->SetXY($x+85,$y);
        }
        else
        {
            $pdf->MultiCell(85, $h,  "", 1, '', 0, 1, $x, $y, 0, 0, false, false);
            $pdf->Rect($x, $y, 85, $h);
            $pdf->SetXY($x+85,$y);
        }


        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(16,$cell_height, number_format($row["approved"],2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 16, $h);
        $pdf->SetXY($x+16,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        
		$pdf->Cell(13,$cell_height, $row["date1"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date2"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date3"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);

        $x = $pdf->GetX();
        $y = $pdf->GetY();
        //$pdf->Cell(23,$cell_height, $row["order_item_po_number"], 0);
		$pdf->MultiCell(23, $cell_height,  $row["order_item_po_number"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 3.5, 'T', true);
        $pdf->Rect($x, $y, 23, $h);
        $pdf->SetXY($x+23,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(19,$cell_height, number_format($row["investment"],2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 19, $h);
        
		
		$x_tmp = $x+19;
		
		$x = $x+58;
		$pdf->SetXY($x,$y);

		$pdf->Cell(12,$cell_height, $shares_p2_hq[$row["item"]], 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height, $shares_p2_hq_fc[$row["item"]], 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height, $shares_p2[$row["item"]], 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);


		//reinvoice data
		$x = $x_tmp;
		$pdf->SetXY($x,$y);
		
		$tmp = "";
		$num_of_lines = 1;
		if($row["order_item_reinvoicenbr2"])
		{
			$tmp = $row["date4"] . "\r\n" . $row["date42"];
			$num_of_lines++;
		}
		else
		{
			$tmp = $row["date4"];
		}

		$pdf->MultiCell(15,$h, $tmp, 1, '', 0, 1, $x, $y, 0, 0, false, false, 0, 'T', true);
		$x = $x+15;

		$tmp = '';
		if($row["order_item_reinvoicenbr2"])
		{
			$tmp =$row["order_item_reinvoicenbr"] . "\r\n" . $row["order_item_reinvoicenbr2"];

		}
		else
		{
			$tmp =$row["order_item_reinvoicenbr"];
		}
		$pdf->MultiCell(24,$h, $tmp, 1, '', 0, 0, $x, $y, 0, 0, false, false, 0, 'T', true);
		

		$pdf->setXY(10, $y+$h);
		//$pdf->Ln();
        $nr = $nr + 1;
    }

    $pdf->SetFont('arialn','B',7);
    $pdf->Cell(106,4, "Total HQ Supplied Items", 1, 0, 'L', 0);
    $pdf->Cell(16,4, number_format($grouptotals_investment[2],2, ".", "'"), 1, 0, "R", 0);
    $pdf->Cell(13,4, "", 1, 0, 'L', 0);
    $pdf->Cell(13,4, "", 1, 0, 'L', 0);
    $pdf->Cell(13,4, "", 1, 0, 'L', 0);
    $pdf->Cell(23,4, "", 1, 0, 'L', 0);
    if(array_key_exists(2, $oip_group_totals))
	{
		$pdf->Cell(19,4, number_format($grouptotals_real[2] - $oip_group_totals[2],2, ".", "'"), 1, 0, "R", 0);
	}
	else
	{
		$pdf->Cell(19,4, number_format($grouptotals_real[2],2, ".", "'"), 1, 0, "R", 0);
	}
    $pdf->Cell(15,4, "", 1, 0, 'L', 0);
    $pdf->Cell(24,4, "", 1, 0, 'L', 0);
    $pdf->Cell(12,4, $shares_t2_hq, 1,0, "R", 0);
    $pdf->Cell(12,4, $shares_t2_hq_fc, 1,0, "R", 0);
    $pdf->Cell(12,4,  $share_t2, 1,0, "R", 0);
    $pdf->Ln();
}




if($num_recs6 > 0)
{

    if($y >= $pdf_linebreak_after)
    {
        $pdf->AddPage();
        $pdf->simple_group_header("Store fixturing / Furniture");
		$pdf->sub_group_header("Freight Charges", $order_currency["symbol"]);
    }
    else
    {
	    $pdf->simple_sub_group_header("Freight Charges", $pdf->getY());
    }
    
    $pdf->SetFont('arialn','',7);
    
    $res = mysql_query($sql_list6);

    while ($row = mysql_fetch_assoc($res))
    {
        
        if($row["monitoring_group"])
        {
			$nls = ceil(strlen(str_replace("\r\n", " ", $row["monitoring_group"])) / 55);
            $h = 3.5 * $nls;
        }
        else
        {
            $nls = 1;
            $h = 3.5;
        }
		
		$y = $pdf->getY();
        if($y + $h >= $pdf_linebreak_after)
        {
            $pdf->AddPage();
            $pdf->simple_group_header("Store fixturing / Furniture");
			$pdf->sub_group_header("Freight Charges", $order_currency["symbol"]);
        }

        
		
        $x = $pdf->GetX();
        $y = $pdf->GetY();

        
		$pdf->SetFont('arialn','',7);

		$cell_height = 3.5;
		
		$cms_remark_referenced_line_numbers[$row["monitoring_group"]] = $nr;

		if(in_array($row["monitoring_group"], $cms_remark_keys))
		{
			$pdf->Cell(6, $cell_height, $nr . "*", 0);
		}
		else
		{
			$pdf->Cell(6, $cell_height, $nr, 0);
		}
        $pdf->Rect($x, $y, 6, $h);
        $pdf->SetXY($x+6,$y);

        
		$x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(15, $cell_height, $row["address_shortcut"], 0);
        $pdf->Rect($x, $y, 15, $h);
        $pdf->SetXY($x+15,$y);
        
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        if($row["monitoring_group"])
        {
			$pdf->MultiCell(85, $h,  str_replace("\r\n", " ", $row["monitoring_group"]), 1, 'L', 0, 1, $x, $y, 0, 0, false, false, 0, 'T', false);
			$pdf->SetXY($x+85,$y);
        }
        else
        {
			$pdf->MultiCell(85, $h,  "", 1, '', 0, 1, $x, $y, 0, 0, false, false);
            $pdf->Rect($x, $y, 85, $h);
            $pdf->SetXY($x+85,$y);
        }


        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(16,$cell_height, number_format($row["approved"],2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 16, $h);
        $pdf->SetXY($x+16,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date1"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date2"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date3"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        //$pdf->Cell(23,$cell_height, $row["order_item_po_number"], 0);
		$pdf->MultiCell(23, $cell_height,  $row["order_item_po_number"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 3.5, 'T', true);
        $pdf->Rect($x, $y, 23, $h);
        $pdf->SetXY($x+23,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(19,$cell_height, number_format($row["investment"],2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 19, $h);
        $pdf->SetXY($x+19,$y);
        
        
		
		
		
		$x_tmp = $x+19;
		
		$x = $x+58;
		$pdf->SetXY($x,$y);

		$pdf->Cell(12,$cell_height, $shares_p6_hq[$row["item"]], 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height, $shares_p6_hq_fc[$row["item"]], 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height, $shares_p6[$row["item"]], 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
		
		//reinvoice data
		$x = $x_tmp;
		$pdf->SetXY($x,$y);
		
		$tmp = "";
		$num_of_lines = 1;
		if($row["order_item_reinvoicenbr2"])
		{
			$tmp = $row["date4"] . "\r\n" . $row["date42"];
			$num_of_lines++;
		}
		else
		{
			$tmp = $row["date4"];
		}

		$pdf->MultiCell(15,$h, $tmp, 1, '', 0, 1, $x, $y, 0, 0, false, false, 0, 'T', true);
		$x = $x+15;

		$tmp = '';
		if($row["order_item_reinvoicenbr2"])
		{
			$tmp =$row["order_item_reinvoicenbr"] . "\r\n" . $row["order_item_reinvoicenbr2"];

		}
		else
		{
			$tmp =$row["order_item_reinvoicenbr"];
		}
		$pdf->MultiCell(24,$h, $tmp, 1, '', 0, 0, $x, $y, 0, 0, false, false, 0, 'T', true);
		

		$pdf->setXY(10, $y+$h);
		//$pdf->Ln();
        $nr = $nr + 1;

        
    }

    $pdf->SetFont('arialn','B',7);
    $pdf->Cell(106,4, "Total Freight Charges", 1, 0, 'L', 0);
    $pdf->Cell(16,4, number_format($grouptotals_investment[6],2, ".", "'"), 1, 0, "R", 0);
    $pdf->Cell(13,4, "", 1, 0, 'L', 0);
    $pdf->Cell(13,4, "", 1, 0, 'L', 0);
    $pdf->Cell(13,4, "", 1, 0, 'L', 0);
    $pdf->Cell(23,4, "", 1, 0, 'L', 0);
    if(array_key_exists(6, $oip_group_totals))
	{
		$pdf->Cell(19,4, number_format($grouptotals_real[6] - $oip_group_totals[6],2, ".", "'"), 1, 0, "R", 0);
	}
	else
	{
		$pdf->Cell(19,4, number_format($grouptotals_real[6],2, ".", "'"), 1, 0, "R", 0);
	}
	
    $pdf->Cell(15,4, "", 1, 0, 'L', 0);
    $pdf->Cell(24,4, "", 1, 0, 'L', 0);
    //$pdf->Cell(12,4, $shares_t6_hq, 1,0, "R", 0);
	$pdf->Cell(12,4, "", 1,0, "R", 0);
    $pdf->Cell(12,4, $shares_t6_hq_fc, 1,0, "R", 0);
    $pdf->Cell(12,4,  $share_t6, 1,0, "R", 0);
    $pdf->Ln();

}

if($num_recs10 > 0)
{

    $y = $pdf->getY();
    if($y >= $pdf_linebreak_after)
    {
        $pdf->AddPage();
        $pdf->simple_group_header("Store fixturing / Furniture");
		$pdf->sub_group_header("Other", $order_currency["symbol"]);
    }
    else
    {
	    $pdf->simple_sub_group_header("Other", $pdf->getY());
    }
    
    $pdf->SetFont('arialn','',7);
    
    $res = mysql_query($sql_list10);

    while ($row = mysql_fetch_assoc($res))
    {
        
        if($row["monitoring_group"])
        {
			$nls = ceil(strlen(str_replace("\r\n", " ", $row["monitoring_group"])) / 55);
            $h = 3.5 * $nls;
        }
        else
        {
            $nls = 1;
            $h = 3.5;
        }
		
		$y = $pdf->getY();
        if($y + $h >= $pdf_linebreak_after)
        {
            $pdf->AddPage();
            $pdf->simple_group_header("Store fixturing / Furniture");
			$pdf->sub_group_header("Other", $order_currency["symbol"]);
        }

        

        $x = $pdf->GetX();
        $y = $pdf->GetY();

        
		$pdf->SetFont('arialn','',7);

		$cell_height = 3.5;
		
		$cms_remark_referenced_line_numbers[$row["monitoring_group"]] = $nr;

		if(in_array($row["monitoring_group"], $cms_remark_keys))
		{
			$pdf->Cell(6, $cell_height, $nr . "*", 0);
		}
		else
		{
			$pdf->Cell(6, $cell_height, $nr, 0);
		}
        $pdf->Rect($x, $y, 6, $h);
        $pdf->SetXY($x+6,$y);

        
		$x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(15, $cell_height, $row["address_shortcut"], 0);
        $pdf->Rect($x, $y, 15, $h);
        $pdf->SetXY($x+15,$y);
        
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        if($row["monitoring_group"])
        {
			$pdf->MultiCell(85, $h,  str_replace("\r\n", " ", $row["monitoring_group"]), 1, 'L', 0, 1, $x, $y, 0, 0, false, false, 0, 'T', false);
            $pdf->SetXY($x+85,$y);
        }
        else
        {
            $pdf->MultiCell(85, $cell_height,  "", 1, '', 0, 1, $x, $y, 0, 0, false, false);
            $pdf->Rect($x, $y, 85, $h);
            $pdf->SetXY($x+85,$y);
        }


        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(16,$cell_height, number_format($row["approved"],2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 16, $h);
        $pdf->SetXY($x+16,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date1"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date2"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date3"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        //$pdf->Cell(23,$cell_height, $row["order_item_po_number"], 0);
		$pdf->MultiCell(23, $cell_height,  $row["order_item_po_number"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 3.5, 'T', true);
        $pdf->Rect($x, $y, 23, $h);
        $pdf->SetXY($x+23,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(19,$cell_height, number_format($row["investment"],2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 19, $h);
        $pdf->SetXY($x+19,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		if($row["order_item_reinvoicenbr2"])
		{
			$pdf->MultiCell(15,$cell_height, $row["date4"] . "\r\n" . $row["date42"], 'T', '', 0, 1, $x, $y, 0, 0, false, false, 10, 'T', true);
			$pdf->Rect($x, $y, 15, $h);
			$pdf->SetXY($x+15,$y);
		}
		else
		{
			$pdf->Cell(15,$cell_height, $row["date4"], 0);
			$pdf->Rect($x, $y, 15, $h);
			$pdf->SetXY($x+15,$y);
		}

		$x = $pdf->GetX();
        $y = $pdf->GetY();
		if($row["order_item_reinvoicenbr2"])
		{
			//$pdf->Cell(1,24, "", 0);
			$pdf->SetXY($x,$y);
			$pdf->MultiCell(24, $cell_height,  $row["order_item_reinvoicenbr"] . "\r\n" . $row["order_item_reinvoicenbr2"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 10, 'T', true);
			$pdf->Rect($x, $y, 24, $h);
			$pdf->SetXY($x+24,$y);

		}
		else
		{
			$pdf->Cell(1,$cell_height, "", 0);
			$pdf->SetXY($x,$y);
			$pdf->MultiCell(24, $cell_height,  $row["order_item_reinvoicenbr"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 3.5, 'T', true);
			$pdf->SetXY($x+24,$y);
			$y = $pdf->GetY();
		}

        $x = $pdf->GetX();


		//$pdf->Cell(12,$cell_height, $shares_p10_hq[$row["item"]], 0, 0, "R");
		$pdf->Cell(12,$cell_height, "", 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height, $shares_p10_hq_fc[$row["item"]], 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height, $shares_p10[$row["item"]], 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);

		$pdf->setXY(10, $y+$h);


        $nr = $nr + 1;
    }

    $pdf->SetFont('arialn','B',7);
    $pdf->Cell(106,4, "Total Other", 1, 0, 'L', 0);
    $pdf->Cell(16,4, number_format($grouptotals_investment[10],2, ".", "'"), 1, 0, "R", 0);
    $pdf->Cell(13,4, "", 1, 0, 'L', 0);
    $pdf->Cell(13,4, "", 1, 0, 'L', 0);
    $pdf->Cell(13,4, "", 1, 0, 'L', 0);
    $pdf->Cell(23,4, "", 1, 0, 'L', 0);
    if(array_key_exists(10, $oip_group_totals))
	{
		$pdf->Cell(19,4, number_format($grouptotals_real[10] - $oip_group_totals[10],2, ".", "'"), 1, 0, "R", 0);
	}
	else
	{
		$pdf->Cell(19,4, number_format($grouptotals_real[10],2, ".", "'"), 1, 0, "R", 0);
	}
    $pdf->Cell(15,4, "", 1, 0, 'L', 0);
    $pdf->Cell(24,4, "", 1, 0, 'L', 0);
    //$pdf->Cell(12,4, $shares_t10_hq, 1,0, "R", 0);
	$pdf->Cell(12,4, "", 1,0, "R", 0);
    $pdf->Cell(12,4, $shares_t10_hq_fc, 1,0, "R", 0);
    $pdf->Cell(12,4,  $share_t10, 1,0, "R", 0);
    $pdf->Ln();


}

//store fixturing totals
$tmp1 = $grouptotals_investment[2] + $grouptotals_investment[6] + $grouptotals_investment[10];
$tmp2 = $grouptotals_real[2] + $grouptotals_real[6] + $grouptotals_real[10];
$tmp3 = $share_t2 + $share_t6 + $share_t10;

if(array_key_exists(2, $oip_group_totals))
{
	$tmp2 = $tmp2 - $oip_group_totals[2];
}
if(array_key_exists(6, $oip_group_totals))
{
	$tmp2 = $tmp2 - $oip_group_totals[6];
}
if(array_key_exists(10, $oip_group_totals))
{
	$tmp2 = $tmp2 - $oip_group_totals[10];
}

$pdf->SetFont('arialn','B',7);
$pdf->Cell(106,4, "Total Store fixturing / Furniture", 1, 0, 'L', 1);
$pdf->Cell(16,4, number_format($tmp1,2, ".", "'"), 1, 0, "R", 1);
$pdf->Cell(13,4, "", 1, 0, 'L', 1);
$pdf->Cell(13,4, "", 1, 0, 'L', 1);
$pdf->Cell(13,4, "", 1, 0, 'L', 1);
$pdf->Cell(23,4, "", 1, 0, 'L', 1);
$pdf->Cell(19,4, number_format($tmp2,2, ".", "'"), 1, 0, "R", 1);
$pdf->Cell(15,4, "", 1, 0, 'L', 1);
$pdf->Cell(24,4, "", 1, 0, 'L', 1);
$pdf->Cell(12,4, "", 1,0, "R", 1);
$pdf->Cell(12,4, "0.00%", 1,0, "R", 1);
$pdf->Cell(12,4,  $tmp3, 1,0, "R", 1);
$pdf->Ln();
$pdf->Ln();


if($num_recs9 > 0)
{

    $y = $pdf->getY();
    if($y >= $pdf_linebreak_after)
    {
        $pdf->AddPage();
        $pdf->group_header("Architectural Services", $order_currency["symbol"]);
    }
    else
    {
        $pdf->group_header("Architectural Services", $order_currency["symbol"]);
    }
    
    $pdf->SetFont('arialn','',7);
    
    $res = mysql_query($sql_list9);

    while ($row = mysql_fetch_assoc($res))
    {
        
        if($row["monitoring_group"])
        {
			$nls = ceil(strlen(str_replace("\r\n", " ", $row["monitoring_group"])) / 55);
            $h = 3.5 * $nls;
        }
        else
        {
            $nls = 1;
            $h = 3.5;
        }
		
		$y = $pdf->getY();
        if($y+$h >= $pdf_linebreak_after)
        {
            $pdf->AddPage();
            $pdf->group_header("Architectural Services", $order_currency["symbol"]);
        }

        
        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $pdf->SetFont('arialn','',7);

		$cell_height = 3.5;
		
		$cms_remark_referenced_line_numbers[$row["monitoring_group"]] = $nr;

		if(in_array($row["monitoring_group"], $cms_remark_keys))
		{
			$pdf->Cell(6, $cell_height, $nr . "*", 0);
		}
		else
		{
			$pdf->Cell(6, $cell_height, $nr, 0);
		}
        $pdf->Rect($x, $y, 6, $h);
        $pdf->SetXY($x+6,$y);

        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(40, $cell_height, $row["address_shortcut"], 0);
        $pdf->Rect($x, $y, 40, $h);
        $pdf->SetXY($x+40,$y);
        
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        if($row["monitoring_group"])
        {
			$pdf->MultiCell(60, $h,  str_replace("\r\n", " ", $row["monitoring_group"]), 1, 'L', 0, 1, $x, $y, 0, 0, false, false, 0, 'T', false);
            $pdf->SetXY($x+60,$y);
        }
        else
        {
            $pdf->MultiCell(60, $cell_height,  "", 1, '', 0, 1, $x, $y, 0, 0, false, false);
            $pdf->Rect($x, $y, 60, $h);
            $pdf->SetXY($x+60,$y);
        }


        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(16,$cell_height, number_format($row["approved"],2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 16, $h);
        $pdf->SetXY($x+16,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date1"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date2"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date3"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        //$pdf->Cell(23,$cell_height, $row["order_item_po_number"], 0);
		$pdf->MultiCell(23, $cell_height,  $row["order_item_po_number"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 3.5, 'T', true);
        $pdf->Rect($x, $y, 23, $h);
        $pdf->SetXY($x+23,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(19,$cell_height, number_format($row["investment"],2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 19, $h);
        $pdf->SetXY($x+19,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		if($row["order_item_reinvoicenbr2"])
		{
			$pdf->MultiCell(15,$cell_height, $row["date4"] . "\r\n" . $row["date42"], 'T', '', 0, 1, $x, $y, 0, 0, false, false, 10, 'T', true);
			$pdf->Rect($x, $y, 15, $h);
			$pdf->SetXY($x+15,$y);
		}
		else
		{
			$pdf->Cell(15,$cell_height, $row["date4"], 0);
			$pdf->Rect($x, $y, 15, $h);
			$pdf->SetXY($x+15,$y);
		}

		$x = $pdf->GetX();
        $y = $pdf->GetY();
		if($row["order_item_reinvoicenbr2"])
		{
			//$pdf->Cell(1,24, "", 0);
			$pdf->SetXY($x,$y);
			$pdf->MultiCell(24, $cell_height,  $row["order_item_reinvoicenbr"] . "\r\n" . $row["order_item_reinvoicenbr2"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 10, 'T', true);
			$pdf->Rect($x, $y, 24, $h);
			$pdf->SetXY($x+24,$y);

		}
		else
		{
			$pdf->Cell(1,$cell_height, "", 0);
			$pdf->SetXY($x,$y);
			$pdf->MultiCell(24, $cell_height,  $row["order_item_reinvoicenbr"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 3.5, 'T', true);
			$pdf->SetXY($x+24,$y);
			$y = $pdf->GetY();
		}

        $x = $pdf->GetX();


		$pdf->Cell(12,$cell_height, "", 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height, "", 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height, $shares_p9[$row["item"]], 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);

		$pdf->setXY(10, $y+$h);


        $nr = $nr + 1;
    }


    $pdf->SetFont('arialn','B',7);
    $pdf->Cell(106,4, "Total Architectural Services", 1, 0, 'L', 1);
    $pdf->Cell(16,4, number_format($grouptotals_investment[9],2, ".", "'"), 1, 0, "R", 1);
    $pdf->Cell(13,4, "", 1, 0, 'L', 1);
    $pdf->Cell(13,4, "", 1, 0, 'L', 1);
    $pdf->Cell(13,4, "", 1, 0, 'L', 1);
    $pdf->Cell(23,4, "", 1, 0, 'L', 1);
	if(array_key_exists(9, $oip_group_totals))
	{
		$pdf->Cell(19,4, number_format($grouptotals_real[9] - $oip_group_totals[9],2, ".", "'"), 1, 0, "R", 1);
	}
	else
	{
		$pdf->Cell(19,4, number_format($grouptotals_real[9],2, ".", "'"), 1, 0, "R", 1);
	}
    $pdf->Cell(15,4, "", 1, 0, 'L', 1);
    $pdf->Cell(24,4, "", 1, 0, 'L', 1);
    $pdf->Cell(12,4, "", 1,0, "R", 1);
    $pdf->Cell(12,4, "", 1,0, "R", 1);
    $pdf->Cell(12,4,  $share_t9, 1,0, "R", 1);
    $pdf->Ln();

    $pdf->Ln();
}


if($num_recs11 > 0)
{

    $y = $pdf->getY();
    if($y >= $pdf_linebreak_after)
    {
        $pdf->AddPage();
        $pdf->group_header("Equipment", $order_currency["symbol"]);
    }
    else
    {
        $pdf->group_header("Equipment", $order_currency["symbol"]);
    }
    
    $pdf->SetFont('arialn','',7);
    
    $res = mysql_query($sql_list11);

    while ($row = mysql_fetch_assoc($res))
    {
        
        if($row["monitoring_group"])
        {
			$nls = ceil(strlen(str_replace("\r\n", " ", $row["monitoring_group"])) / 55);
            $h = 3.5 * $nls;
        }
        else
        {
            $nls = 1;
            $h = 3.5;
        }
		
		$y = $pdf->getY();
        if($y+$h >= $pdf_linebreak_after)
        {
            $pdf->AddPage();
            $pdf->group_header("Equipment", $order_currency["symbol"]);
        }

        
        $x = $pdf->GetX();
        $y = $pdf->GetY();

        
		$pdf->SetFont('arialn','',7);

		$cell_height = 3.5;
		
		$cms_remark_referenced_line_numbers[$row["monitoring_group"]] = $nr;

		if(in_array($row["monitoring_group"], $cms_remark_keys))
		{
			$pdf->Cell(6, $cell_height, $nr . "*", 0);
		}
		else
		{
			$pdf->Cell(6, $cell_height, $nr, 0);
		}
        $pdf->Rect($x, $y, 6, $h);
        $pdf->SetXY($x+6,$y);

        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(40, $cell_height, $row["address_shortcut"], 0);
        $pdf->Rect($x, $y, 40, $h);
        $pdf->SetXY($x+40,$y);
        
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        if($row["monitoring_group"])
        {
			
			$pdf->MultiCell(60, $h,  str_replace("\r\n", " ", $row["monitoring_group"]), 1, 'L', 0, 1, $x, $y, 0, 0, false, false, 0, 'T', false);
            $pdf->SetXY($x+60,$y);
        }
        else
        {
            $pdf->MultiCell(60, $cell_height,  "", 1, '', 0, 1, $x, $y, 0, 0, false, false);
            $pdf->Rect($x, $y, 60, $h);
            $pdf->SetXY($x+60,$y);
        }


        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(16,$cell_height, number_format($row["approved"],2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 16, $h);
        $pdf->SetXY($x+16,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date1"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date2"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date3"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        //$pdf->Cell(23,$cell_height, $row["order_item_po_number"], 0);
		$pdf->MultiCell(23, $cell_height,  $row["order_item_po_number"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 3.5, 'T', true);
        $pdf->Rect($x, $y, 23, $h);
        $pdf->SetXY($x+23,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(19,$cell_height, number_format($row["investment"],2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 19, $h);
        $pdf->SetXY($x+19,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		if($row["order_item_reinvoicenbr2"])
		{
			$pdf->MultiCell(15,$cell_height, $row["date4"] . "\r\n" . $row["date42"], 'T', '', 0, 1, $x, $y, 0, 0, false, false, 10, 'T', true);
			$pdf->Rect($x, $y, 15, $h);
			$pdf->SetXY($x+15,$y);
		}
		else
		{
			$pdf->Cell(15,$cell_height, $row["date4"], 0);
			$pdf->Rect($x, $y, 15, $h);
			$pdf->SetXY($x+15,$y);
		}

		$x = $pdf->GetX();
        $y = $pdf->GetY();
		if($row["order_item_reinvoicenbr2"])
		{
			//$pdf->Cell(1,24, "", 0);
			$pdf->SetXY($x,$y);
			$pdf->MultiCell(24, $cell_height,  $row["order_item_reinvoicenbr"] . "\r\n" . $row["order_item_reinvoicenbr2"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 10, 'T', true);
			$pdf->Rect($x, $y, 24, $h);
			$pdf->SetXY($x+24,$y);

		}
		else
		{
			$pdf->Cell(1,$cell_height, "", 0);
			$pdf->SetXY($x,$y);
			$pdf->MultiCell(24, $cell_height,  $row["order_item_reinvoicenbr"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 3.5, 'T', true);
			$pdf->SetXY($x+24,$y);
			$y = $pdf->GetY();
		}

        $x = $pdf->GetX();
		$pdf->Cell(12,$cell_height, "", 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height, "", 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height, $shares_p11[$row["item"]], 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);

		$pdf->setXY(10, $y+$h);


        $nr = $nr + 1;
    }


    $pdf->SetFont('arialn','B',7);
    $pdf->Cell(106,4, "Total Equipment", 1, 0, 'L', 1);
    $pdf->Cell(16,4, number_format($grouptotals_investment[11],2, ".", "'"), 1, 0, "R", 1);
    $pdf->Cell(13,4, "", 1, 0, 'L', 1);
    $pdf->Cell(13,4, "", 1, 0, 'L', 1);
    $pdf->Cell(13,4, "", 1, 0, 'L', 1);
    $pdf->Cell(23,4, "", 1, 0, 'L', 1);
    if(array_key_exists(11, $oip_group_totals))
	{
		$pdf->Cell(19,4, number_format($grouptotals_real[11]-$oip_group_totals[11],2, ".", "'"), 1, 0, "R", 1);
	}
	else
	{
		$pdf->Cell(19,4, number_format($grouptotals_real[11],2, ".", "'"), 1, 0, "R", 1);
	}
    $pdf->Cell(15,4, "", 1, 0, 'L', 1);
    $pdf->Cell(24,4, "", 1, 0, 'L', 1);
    $pdf->Cell(12,4, "", 1,0, "R", 1);
    $pdf->Cell(12,4, "", 1,0, "R", 1);
    $pdf->Cell(12,4,  $share_t11, 1,0, "R", 1);
    $pdf->Ln();

    $pdf->Ln();
}


if($num_recs8 > 0)
{

    
	$y = $pdf->getY();
    if($y >= $pdf_linebreak_after)
    {
        $pdf->AddPage();
        $pdf->group_header("Other", $order_currency["symbol"]);
    }
    else
    {
        $pdf->group_header("Other", $order_currency["symbol"]);
    }
    
    $pdf->SetFont('arialn','',7);
    
    $res = mysql_query($sql_list8);

    while ($row = mysql_fetch_assoc($res))
    {
        
        if($row["monitoring_group"])
        {
			$nls = ceil(strlen(str_replace("\r\n", " ", $row["monitoring_group"])) / 55);
            $h = 3.5 * $nls;
        }
        else
        {
            $nls = 1;
            $h = 3.5;
        }
		
		$y = $pdf->getY();
        if($y+$h >= $pdf_linebreak_after)
        {
            $pdf->AddPage();
            $pdf->group_header("Other", $order_currency["symbol"]);
        }

        
        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $pdf->SetFont('arialn','',7);

		$cell_height = 3.5;
		
		$cms_remark_referenced_line_numbers[$row["monitoring_group"]] = $nr;

		if(in_array($row["monitoring_group"], $cms_remark_keys))
		{
			$pdf->Cell(6, $cell_height, $nr . "*", 0);
		}
		else
		{
			$pdf->Cell(6, $cell_height, $nr, 0);
		}
        $pdf->Rect($x, $y, 6, $h);
        $pdf->SetXY($x+6,$y);

        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(40, $cell_height, $row["address_shortcut"], 0);
        $pdf->Rect($x, $y, 40, $h);
        $pdf->SetXY($x+40,$y);
        
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        if($row["monitoring_group"])
        {
			$pdf->MultiCell(60, $h,  str_replace("\r\n", " ", $row["monitoring_group"]), 1, 'L', 0, 1, $x, $y, 0, 0, false, false, 0, 'T', false);
            $pdf->SetXY($x+60,$y);
        }
        else
        {
            $pdf->MultiCell(60, $cell_height,  "", 1, '', 0, 1, $x, $y, 0, 0, false, false);
            $pdf->Rect($x, $y, 60, $h);
            $pdf->SetXY($x+60,$y);
        }


        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(16,$cell_height, number_format($row["approved"],2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 16, $h);
        $pdf->SetXY($x+16,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date1"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date2"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date3"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        //$pdf->Cell(23,$cell_height, $row["order_item_po_number"], 0);
		$pdf->MultiCell(23, $cell_height,  $row["order_item_po_number"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 3.5, 'T', true);
        $pdf->Rect($x, $y, 23, $h);
        $pdf->SetXY($x+23,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(19,$cell_height, number_format($row["investment"],2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 19, $h);
        $pdf->SetXY($x+19,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		if($row["order_item_reinvoicenbr2"])
		{
			$pdf->MultiCell(15,$cell_height, $row["date4"] . "\r\n" . $row["date42"], 'T', '', 0, 1, $x, $y, 0, 0, false, false, 10, 'T', true);
			$pdf->Rect($x, $y, 15, $h);
			$pdf->SetXY($x+15,$y);
		}
		else
		{
			$pdf->Cell(15,$cell_height, $row["date4"], 0);
			$pdf->Rect($x, $y, 15, $h);
			$pdf->SetXY($x+15,$y);
		}

		$x = $pdf->GetX();
        $y = $pdf->GetY();
		if($row["order_item_reinvoicenbr2"])
		{
			//$pdf->Cell(1,24, "", 0);
			$pdf->SetXY($x,$y);
			$pdf->MultiCell(24, $cell_height,  $row["order_item_reinvoicenbr"] . "\r\n" . $row["order_item_reinvoicenbr2"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 10, 'T', true);
			$pdf->Rect($x, $y, 24, $h);
			$pdf->SetXY($x+24,$y);

		}
		else
		{
			$pdf->Cell(1,$cell_height, "", 0);
			$pdf->SetXY($x,$y);
			$pdf->MultiCell(24, $cell_height,  $row["order_item_reinvoicenbr"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 3.5, 'T', true);
			$pdf->SetXY($x+24,$y);
			$y = $pdf->GetY();
		}

        $x = $pdf->GetX();
		$pdf->Cell(12,$cell_height, "", 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height, "", 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height, $shares_p8[$row["item"]], 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);

		$pdf->setXY(10, $y+$h);

        $nr = $nr + 1;
    }


    $pdf->SetFont('arialn','B',7);
    $pdf->Cell(106,4, "Total Other", 1, 0, 'L', 1);
    $pdf->Cell(16,4, number_format($grouptotals_investment[8],2, ".", "'"), 1, 0, "R", 1);
    $pdf->Cell(13,4, "", 1, 0, 'L', 1);
    $pdf->Cell(13,4, "", 1, 0, 'L', 1);
    $pdf->Cell(13,4, "", 1, 0, 'L', 1);
    $pdf->Cell(23,4, "", 1, 0, 'L', 1);
	if(array_key_exists(8, $oip_group_totals))
	{	
		$pdf->Cell(19,4, number_format($grouptotals_real[8] - $oip_group_totals[8],2, ".", "'"), 1, 0, "R", 1);
	}
	else
	{
		$pdf->Cell(19,4, number_format($grouptotals_real[8],2, ".", "'"), 1, 0, "R", 1);
	}
    $pdf->Cell(15,4, "", 1, 0, 'L', 1);
    $pdf->Cell(24,4, "", 1, 0, 'L', 1);
    $pdf->Cell(12,4, "", 1,0, "R", 1);
    $pdf->Cell(12,4, "", 1,0, "R", 1);
    $pdf->Cell(12,4,  $share_t8, 1,0, "R", 1);
    $pdf->Ln();

    $pdf->Ln();
}



$oip_cms_remarks = array();
if($has_catalogue_orders == true)
{

    $y = $pdf->getY();
    if($y >= $pdf_linebreak_after)
    {
        $pdf->AddPage();
        $pdf->group_header("Catalogue Orders", $order_currency["symbol"]);
    }
    else
    {
        $pdf->group_header("Catalogue Orders", $order_currency["symbol"]);
    }
    
    $pdf->SetFont('arialn','',7);
    
    $res = mysql_query($sql_oip);


    while ($row = mysql_fetch_assoc($res))
    {
        
        
		if($row["order_item_text"])
        {
			if(array_key_exists($row["order_item_id"], $replacementinfos))
			{
				$nls = ceil(strlen(str_replace("\r\n", " ", "Replacement: "  . $row["order_item_text"])) / 55);
			}
			else
			{
				$nls = ceil(strlen(str_replace("\r\n", " ", $row["order_item_text"])) / 55);
			}

            $h = 3.5 * $nls;
        }
        else
        {
            $nls = 1;
            $h = 3.5;
        }
		
		$y = $pdf->getY();
        if($y+$h >= $pdf_linebreak_after)
        {
            $pdf->AddPage();
            $pdf->group_header("Catalogue Orders", $order_currency["symbol"]);
        }

        
        $x = $pdf->GetX();
        $y = $pdf->GetY();

        $pdf->SetFont('arialn','',7);

		$cell_height = 3.5;
		
		if($row["order_items_in_project_cms_remark"])
		{
			$oip_cms_remarks[$nr] = $row["order_items_in_project_cms_remark"];
		}


		$pdf->Cell(6, $cell_height, $nr, 0);
        $pdf->Rect($x, $y, 6, $h);
        $pdf->SetXY($x+6,$y);

        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(40, $cell_height, $row["address_shortcut"], 0);
        $pdf->Rect($x, $y, 40, $h);
        $pdf->SetXY($x+40,$y);
        
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        
		
		if(array_key_exists($row["order_item_id"], $replacementinfos))
		{
			$pdf->MultiCell(60, $h,  str_replace("\r\n", " ", "Replacement: " . $row["order_item_text"]), 1, 'L', 0, 1, $x, $y, 0, 0, false, false, 0, 'T', false);
		}
		else
		{
			$pdf->MultiCell(60, $h,  str_replace("\r\n", " ", $row["order_item_text"]), 1, 'L', 0, 1, $x, $y, 0, 0, false, false, 0, 'T', false);
		}
        $pdf->SetXY($x+60,$y);
		

        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(16,$cell_height, number_format(0,2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 16, $h);
        $pdf->SetXY($x+16,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date1"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date2"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(13,$cell_height, $row["date3"], 0);
        $pdf->Rect($x, $y, 13, $h);
        $pdf->SetXY($x+13,$y);

        $x = $pdf->GetX();
        $y = $pdf->GetY();
        //$pdf->Cell(23,$cell_height, $row["order_item_po_number"], 0);
		$pdf->MultiCell(23, $cell_height,  $row["order_item_po_number"], 'LT', '', 0, 1, $x, $y, 0, 0, false, false, 3.5, 'T', true);
        $pdf->Rect($x, $y, 23, $h);
        $pdf->SetXY($x+23,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Cell(19,$cell_height, number_format($row["investment"],2, ".", "'"), 0, 0, "R");
        $pdf->Rect($x, $y, 19, $h);
        $pdf->SetXY($x+19,$y);
        
        
		
		$x_tmp = $x+19;
		
		$x = $x+58;
		$pdf->SetXY($x,$y);

		$pdf->Cell(12,$cell_height, "", 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height,"", 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
        $pdf->SetXY($x+12,$y);
        
        $x = $pdf->GetX();
        $y = $pdf->GetY();
		$pdf->Cell(12, $cell_height, $shares_oip[$row["order_items_in_project_order_item_id"]], 0, 0, "R");
        $pdf->Rect($x, $y, 12, $h);
		
		//reinvoice data
		$x = $x_tmp;
		$pdf->SetXY($x,$y);
		
		$tmp = "";
		$num_of_lines = 1;
		if($row["order_item_reinvoicenbr2"])
		{
			$tmp = $row["date4"] . "\r\n" . $row["date42"];
			$num_of_lines++;
		}
		else
		{
			$tmp = $row["date4"];
		}

		$pdf->MultiCell(15,$h, $tmp, 1, '', 0, 1, $x, $y, 0, 0, false, false, 0, 'T', true);
		$x = $x+15;

		$tmp = '';
		if($row["order_item_reinvoicenbr2"])
		{
			$tmp =$row["order_item_reinvoicenbr"] . "\r\n" . $row["order_item_reinvoicenbr2"];

		}
		else
		{
			$tmp =$row["order_item_reinvoicenbr"];
		}
		$pdf->MultiCell(24,$h, $tmp, 1, '', 0, 0, $x, $y, 0, 0, false, false, 0, 'T', true);
		

		$pdf->setXY(10, $y+$h);
		//$pdf->Ln();
        $nr = $nr + 1;
		
		
    }


    $pdf->SetFont('arialn','B',7);
    $pdf->Cell(106,4, "Total Catalogue Orders", 1, 0, 'L', 1);
    $pdf->Cell(16,4, number_format(0,2, ".", "'"), 1, 0, "R", 1);
    $pdf->Cell(13,4, "", 1, 0, 'L', 1);
    $pdf->Cell(13,4, "", 1, 0, 'L', 1);
    $pdf->Cell(13,4, "", 1, 0, 'L', 1);
    $pdf->Cell(23,4, "", 1, 0, 'L', 1);
    $pdf->Cell(19,4, number_format($grouptotals_oip,2, ".", "'"), 1, 0, "R", 1);
    $pdf->Cell(15,4, "", 1, 0, 'L', 1);
    $pdf->Cell(24,4, "", 1, 0, 'L', 1);
    $pdf->Cell(12,4, "", 1,0, "R", 1);
    $pdf->Cell(12,4, "", 1,0, "R", 1);
    $pdf->Cell(12,4,  $share_oip, 1,0, "R", 1);
    $pdf->Ln();

    $pdf->Ln();
}

//print remarks from order_items
$cms_remarks_printed = false;
if(count($cms_remarks) > 0)
{
	$cms_remarks_printed = true;
	$r = 0;

	$cms_remark_referenced_line_numbers[$row["monitoring_group"]] = $nr;
	

	foreach($cms_remarks as $key=>$remark)
	{
		$tmp = explode("@@@@", $remark);
		if(count($tmp) == 2)
		{
			$remark = $cms_remark_referenced_line_numbers[$tmp[0]] . " - " . $tmp[1];
		}
		$cms_remarks[$key] = $remark;
	}

	asort($cms_remarks, SORT_NUMERIC);

	foreach($cms_remarks as $key=>$remark)
	{

		$remark = "ad " . $remark;
		$y = $pdf->getY();
		if($y >= $pdf_linebreak_after)
		{
			$pdf->AddPage();
			$pdf->simple_group_header("Remarks");
			$r = 1;
			
		}

		
		$x = $pdf->GetX();
		$y = $pdf->GetY();

		if($r == 0)
		{
			$pdf->simple_group_header("Remarks");
			$r = 1;
		}

		$x = $pdf->GetX();
		$y = $pdf->GetY();

		$pdf->SetFont('arialn','',7);
		$pdf->Cell(1,0, "", 0,0, "R", 0);
		$pdf->MultiCell(278, 2.5,  $remark, 1, '', 0, 1, $x, $y, 0, 0, true, false);
		
	}
}

if(count($oip_cms_remarks) > 0)
{
	$r = 0;

	foreach($oip_cms_remarks as $key=>$remark)
	{

		$remark = "ad " . $key . " - " . $remark;
		
		
		$y = $pdf->getY();
		if($y >= $pdf_linebreak_after)
		{
			$pdf->AddPage();
			$pdf->simple_group_header("Remarks");
			$r = 1;
			
		}

		
		$x = $pdf->GetX();
		$y = $pdf->GetY();

		if($cms_remarks_printed == false and $r == 0)
		{
			$pdf->simple_group_header("Remarks");
			$r = 1;
		}

		$x = $pdf->GetX();
		$y = $pdf->GetY();

		$pdf->SetFont('arialn','',7);
		$pdf->Cell(1,0, "", 0,0, "R", 0);
		$pdf->MultiCell(278, 2.5,  $remark, 1, '', 0, 1, $x, $y, 0, 0, true, false);
		
	}
}



// print general remarks

$r = 0;
$res = mysql_query($sql_remarks) or dberror($sql_remarks);
while ($row = mysql_fetch_assoc($res))
{

    
	$y = $pdf->getY();
	if($y >= $pdf_linebreak_after)
	{
		$pdf->AddPage();
		$pdf->simple_group_header("General Remarks");
        $r = 1;
		
	}

	
	$x = $pdf->GetX();
	$y = $pdf->GetY();

	if($r == 0)
    {
        $pdf->simple_group_header("General Remarks");
        $r = 1;
    }

	$x = $pdf->GetX();
	$y = $pdf->GetY();

    $pdf->SetFont('arialn','',7);
	$pdf->setCellHeightRatio(0);
	$pdf->MultiCell(278, 2.5,  $row["project_cost_remark_remark"], 1, '', 0, 1, $x, $y, 0, 0, true, false);
}

$file_name = "CMS_" . $project["project_number"] . "_" . date("Y-m-d") . ".pdf";
// write pdf
$pdf->Output($file_name);

?>