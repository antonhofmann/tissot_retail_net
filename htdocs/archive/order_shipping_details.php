<?php
/********************************************************************

    order_shipping_details.php

    List shipping details for each forwarder

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2015-01-20
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-20
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/logistic_functions.php";

check_access("has_access_to_shipping_details_in_orders");

register_param("oid");


/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

// read order and order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);


//update order_item_packaging data from item data
$result = update_packaging_info_for_items($order["order_id"]);

// create sql for order items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, order_item_shipment_code, ".
                   "    if(order_item_item <>'', item_code, item_type_name) as item_code, " .
                    "    concat(if(addresses.address_company is not null, addresses.address_company, '!! no supplier assigned !!'), ' / ', if(addresses1.address_company is not null, addresses1.address_company, '!! no forwarder assigned !!')) as address_group, " .
				   "    if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
				   "    DATE_FORMAT(order_item_ready_for_pickup,'%d.%m.%y') as ready_for_pickup, " .
				   "    DATE_FORMAT(order_item_pickup,'%d.%m.%y') as pickup, " . 
				   "    DATE_FORMAT(order_item_expected_arrival,'%d.%m.%y') as expected_arrival, " . 
				   "    DATE_FORMAT(order_item_arrival,'%d.%m.%y') as arrival " . 
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join item_types on order_item_type = item_type_id ".
                   "left join addresses on addresses.address_id = order_item_supplier_address " . 
				   "left join addresses as addresses1 on addresses1.address_id = order_item_forwarder_address ";

/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "order");

$form->add_hidden("oid", param("oid"));

require_once "include/order_head_small.php";




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create Item List
*********************************************************************/ 
$list = new ListView($sql_order_items, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);

$list->set_title("Items up to Delivery");
$list->set_entity("order_item");
$list->set_group("address_group");


if (has_access("has_access_to_all_traffic_data_in_orders"))
{
    $list->set_filter("(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
		              "   and order_item_quantity > 0 " .
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $order["order_id"]);

}
elseif(in_array(6, $user_roles)) // forwarder
{
    $list->set_filter("(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
		              "   and order_item_quantity > 0 " .
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $order["order_id"] .
                      "   and order_item_forwarder_address = " . $user_data["address"]);
}
elseif(in_array(5, $user_roles)) // supplier
{
    $list->set_filter("(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
		              "   and order_item_quantity > 0 " .
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $order["order_id"] .
                      "   and order_item_supplier_address = " . $user_data["address"]);
}
else
{
	$list->set_filter("(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
		              "   and order_item_quantity > 0 " .
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $order["order_id"] .
                      "   and order_item_supplier_address = 0");
}


$list->set_order("order_item_type, item_code");


$list->add_column("item_code", "Item Code", "", "", "", COLUMN_BREAK);
$list->add_column("order_item_text", "Name", "", "", "", COLUMN_BREAK);
$list->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_BREAK);
$list->add_column("order_item_shipment_code", "Shipment Code", "", "", "", COLUMN_BREAK);
$list->add_column("ready_for_pickup", "Ready 4 Pickup", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
$list->add_column("pickup", "Pickup Date", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
$list->add_column("expected_arrival", "Expected Arrival", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
$list->add_column("arrival", "Arrival", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Shipping Details - Items up to Delivery");


require_once("include/tabs_shipping_details.php");



$form->render();
$list->render();

$page->footer();

?>