<?php
/********************************************************************

    project_shipping_documents_client_info.php

    List shipping details for the client

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2015-03-02
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2015-03-02
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";


check_access("has_access_to_shipping_details_in_projects");

register_param("pid");


/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

$client_data = array();
$sql = "select address_number2, address_vat_id, address_financecontroller_name, address_financecontroller_phone, " .
       " address_financecontroller_email, address_logistics_fedex_dhl, address_logistics_coo, address_logistics_notes, " . 
	   " address_logistics_comments " . 
       " from addresses " . 
	   " where address_id = " . dbquote($client_address["id"]);

$res = mysql_query($sql) or dberror($sql);
$client_data = mysql_fetch_assoc($res);

foreach($client_data as $key=>$value)
{
	$client_data[$key] = str_replace("\r\n", "<br />", $value);
	$client_data[$key] = str_replace("\n", "<br />", $value);
}

/********************************************************************
    build form
*********************************************************************/


$form = new Form("addresses", "address");

$form->add_hidden("pid", param("pid"));

require_once "include/project_head_small.php";


$form->add_section("Logistics");
$form->add_label("address_number2", "Tissot SAP Customer Code Retail Furniture", 0, $client_data["address_number2"]);
$form->add_label("address_vat_id", "VAT ID", 0, $client_data["address_vat_id"]);
$form->add_label("address_financecontroller_name", "Name of finance controller", 0, $client_data["address_financecontroller_name"]);
$form->add_label("address_financecontroller_phone", "Phone of finance controller", 0, $client_data["address_financecontroller_phone"]);
$form->add_label("address_financecontroller_email", "Email of finance controller", 0, $client_data["address_financecontroller_email"]);


$form->add_label("address_logistics_fedex_dhl", "FEDEX/DHL", RENDER_HTML, $client_data["address_logistics_fedex_dhl"]);
$form->add_checkbox("address_logistics_coo", "to ship original COO and invoice to client",$client_data["address_logistics_coo"], 0, "Original COO and Invoice");
$form->add_label("address_logistics_notes", "Notes", RENDER_HTML, $client_data["address_logistics_notes"]);
$form->add_label("address_logistics_comments", "Comments", RENDER_HTML, $client_data["address_logistics_comments"]);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();





/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Shipping Details - Client Info");


require_once("include/tabs_shipping_details.php");



$form->render();
require_once "include/project_footer_logistic_state.php";
$page->footer();

?>