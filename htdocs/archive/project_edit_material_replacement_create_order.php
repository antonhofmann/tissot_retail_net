<?php
/********************************************************************

    project_edit_material_replacement_create_order.php

    Edit Replacements of Materials - Create Catalogue order

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2015-06-30
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2015-06-30
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";


if(!has_access("can_edit_material_replacements"))
{
	redirect("noaccess.php");
}


register_param("pid");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);


//posdata
$pos_data = get_pos_data($project["project_order"]);

// get System currency
$system_currency = get_system_currency_fields();
$exchange_rate_info = get_order_currency($project["project_order"]);


$sql_items = "select order_item_replacement_id, order_item_replacement_item_id, order_item_replacement_item_type, " . 
             " if(order_item_replacement_item_id > 0, item_code, 'Special Item') as item_shortcut, " . 
			 " order_item_replacement_item_text, order_item_replacement_quantity, order_item_replacement_supplier_address, " . 
			 " replacements_payer_name_name, item_category, order_item_cost_group, " .
			 " item_width, item_height, item_length, item_gross_weight, item_unit, item_packaging_type, item_stackable " . 
             " from order_item_replacements " . 
			 " left join items on item_id = order_item_replacement_item_id " . 
			 " left join order_items on order_item_id = order_item_replacement_order_item_id " . 
			 " left join replacements_payer_names on replacements_payer_name_id = order_item_replacement_item_to_be_payed_by";

$list_filter = "(order_item_replacement_catalog_order_id = 0 or order_item_replacement_catalog_order_id is null)" . 
               " and order_item_replacement_order_id = " . dbquote($project["project_order"]);


//check if we have items to be replaced
$has_items_to_be_replaced = false;
$sql = $sql_items  . " where " . $list_filter;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$has_items_to_be_replaced = true;
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);

require_once "include/project_head_small.php";


$form->add_button(FORM_BUTTON_BACK, "Back");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();

/********************************************************************
    Create List for Catalog Items
*********************************************************************/ 
$list = new ListView($sql_items);

$list->set_title("Replacements");
$list->set_entity("order_item_replacements");
$list->set_filter($list_filter);
$list->set_order("item_shortcut");

$list->add_column("item_shortcut", "Item Code", "", "", "", COLUMN_NO_WRAP);
$list->add_column("order_item_replacement_item_text", "Name");
$list->add_column("order_item_replacement_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$list->add_column("replacements_payer_name_name", "Paid by");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$list->populate();
$list->process();

if ($list->button("add_item"))
{
    $link = "project_edit_material_replacement.php?pid=" . param("pid"); 
    redirect($link);
}
elseif($list->button("create_catalog_order"))
{
	$link = "project_edit_material_replacement_create_order.php?pid=" . param("pid"); 
    redirect($link);
}




/********************************************************************
    Create Form Order Details
*********************************************************************/

//prepare data
$project_delivery_address = array();

//standard delivery address
$standard_delivery_address = array();

$sql_delivery_addresses = "select * " . 
						  "from standard_delivery_addresses " . 
						  "where delivery_address_address_id = " . dbquote($client_address['id']) . 
						  " order by delivery_address_company";

$res = mysql_query($sql_delivery_addresses);

if ($row = mysql_fetch_assoc($res))
{
	$standard_delivery_address = $row;

	$sql = "select place_province from places where place_id = " . $standard_delivery_address["delivery_address_place_id"];
	$res = mysql_query($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$standard_delivery_address["delivery_address_province_id"] = $row["place_province"];

	}
}


//get choices of possible delivery addresses
$choices['01'] = "POS location";
$choices['02'] = $client_address['company'];
if(count($standard_delivery_address) > 0)
{
	$choices['03'] = $standard_delivery_address["delivery_address_company"] . " " . $standard_delivery_address["delivery_address_place"];
}

$sql = "select order_address_id, order_address_company, order_address_place from order_addresses " . 
       " where order_address_company <> '' " . 
	   " and order_address_type = 2 and order_address_order = " . dbquote($project["project_order"]) . 
	   " order by order_address_company";

$res = mysql_query($sql);

while ($row = mysql_fetch_assoc($res))
{
	if(!in_array($row["order_address_company"] . " " . $row["order_address_place"], $choices))
	{
		$choices[$row["order_address_id"]] = $row["order_address_company"] . " " . $row["order_address_place"];
	}
}

$sql_countries = "select country_id, country_name ".
                     "from countries ".
                     "order by country_name";


//build form

$form1 = new Form("projects", "project");

$form1->add_hidden("pid", param("pid"));
$form1->add_hidden("oid",$project["project_order"]);


// Delivery Address
$form1->add_section("Ship Items to");
$form1->add_list("delivery_is_address", "Ship to information is identical to", $choices, SUBMIT, param("delivery_is_address"));

$form1->add_edit("delivery_address_company", "Company*", NOTNULL);
$form1->add_edit("delivery_address_company2", "",0);


if(param("delivery_is_address") == '01') // pos address selected
{
	param("delivery_address_country", $pos_data["posaddress_country"]);
	param("delivery_address_province_id", $pos_data["place_province"]);
	param("delivery_address_place_id", $pos_data["posaddress_place_id"]);
	param("delivery_address_place", $pos_data["posaddress_place"]);
	
	$form1->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL | SUBMIT);

	$sql_provinces = "select province_id, province_canton from provinces where province_country = " . $pos_data["posaddress_country"] . " order by province_canton";
	$form1->add_list("delivery_address_province_id", "Province*", $sql_provinces, SUBMIT);

	$sql_places = "select place_id, place_name from places where place_province = " . dbquote($pos_data["place_province"]) . " order by place_name";
	$form1->add_list("delivery_address_place_id", "City*", $sql_places, NOTNULL);
	$form1->add_hidden("delivery_address_place");
}
elseif(param("delivery_is_address") == '02') // client address selected
{
	param("delivery_address_country", $client_address["country"]);
	param("delivery_address_province_id", $client_address["place_province"]);
	param("delivery_address_place_id", $client_address["place_id"]);
	param("delivery_address_place", $client_address["place"]);
	
	$form1->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL | SUBMIT);

	$sql_provinces = "select province_id, province_canton from provinces where province_country = " . $client_address["country"] . " order by province_canton";
	$form1->add_list("delivery_address_province_id", "Province*", $sql_provinces, SUBMIT);

	$sql_places = "select place_id, place_name from places where place_province = " . dbquote($client_address["place_province"]) . " order by place_name";
	$form1->add_list("delivery_address_place_id", "City*", $sql_places, NOTNULL);
	$form1->add_hidden("delivery_address_place");
}
elseif(param("delivery_is_address") == '03') // standard delivery address selected
{
	param("delivery_address_country", $standard_delivery_address["delivery_address_country"]);
	param("delivery_address_province_id", $standard_delivery_address["delivery_address_province_id"]);
	param("delivery_address_place_id", $standard_delivery_address["delivery_address_place_id"]);
	param("delivery_address_place", $standard_delivery_address["delivery_address_place"]);
	
	$form1->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL | SUBMIT);

	$sql_provinces = "select province_id, province_canton from provinces where province_country = " . $standard_delivery_address["delivery_address_country"] . " order by province_canton";
	$form1->add_list("delivery_address_province_id", "Province*", $sql_provinces, SUBMIT);

	$sql_places = "select place_id, place_name from places where place_province = " . dbquote($standard_delivery_address["delivery_address_province_id"]) . " order by place_name";
	$form1->add_list("delivery_address_place_id", "City*", $sql_places, NOTNULL);
	$form1->add_hidden("delivery_address_place");
}
elseif(param("delivery_is_address") > 0) // project's delivery address selected
{
	$sql = "select * from order_addresses " .
	       " left join places on place_id = order_address_place_id " . 
	       "where order_address_id = " . dbquote(param("delivery_is_address"));
	$res = mysql_query($sql);
	$project_delivery_address = mysql_fetch_assoc($res);
	
	param("delivery_address_country", $project_delivery_address["order_address_country"]);
	param("delivery_address_province_id", $project_delivery_address["place_province"]);
	param("delivery_address_place_id", $project_delivery_address["order_address_place_id"]);
	param("delivery_address_place", $project_delivery_address["place_name"]);
	
	$form1->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL | SUBMIT);

	$sql_provinces = "select province_id, province_canton from provinces where province_country = " . $project_delivery_address["order_address_country"] . " order by province_canton";
	$form1->add_list("delivery_address_province_id", "Province*", $sql_provinces, SUBMIT);

	$sql_places = "select place_id, place_name from places where place_province = " . dbquote($project_delivery_address["place_province"]) . " order by place_name";
	$form1->add_list("delivery_address_place_id", "City*", $sql_places, NOTNULL);
	$form1->add_hidden("delivery_address_place");
}
else
{
	$form1->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL | SUBMIT, param("delivery_address_country"));

	if(param("delivery_address_country")) {
		$sql_provinces = "select province_id, province_canton from provinces where province_country = " . param("delivery_address_country") . " order by province_canton";
	}
	else
	{
		$sql_provinces = "select province_id, province_canton from provinces where province_country = 0";
	}
	$form1->add_list("delivery_address_province_id", "Province*", $sql_provinces, SUBMIT, param("delivery_address_province_id"));

	if(param("delivery_address_province_id")) {
		$sql_places = "select place_id, place_name from places where place_province = " . param("delivery_address_province_id") . " order by place_name";
	}
	elseif(param("delivery_address_country")) {
		$sql_places = "select place_id, place_name from places where place_country = " . param("delivery_address_country") . " order by place_name";
	}
	else
	{
		$sql_places = "select place_id, place_name from places where place_country = 0";
	}

	$form1->add_list("delivery_address_place_id", "City*", $sql_places, NOTNULL);
	$form1->add_hidden("delivery_address_place");
}


$form1->add_edit("delivery_address_zip", "ZIP", 0);
$form1->add_hidden("delivery_address_address");
$form1->add_multi_edit("delivery_street", array("order_address_street", "order_address_street_number"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6));


$form1->add_edit("delivery_address_address2", "Additional Address Info",0);


$form1->add_hidden("delivery_address_phone");
$form1->add_multi_edit("delivery_phone_number", array("order_address_phone_country", "order_address_phone_area", "order_address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array('','',''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));



$form1->add_edit("delivery_address_mobile_phone", "Mobile Phone",0);
$form1->add_hidden("delivery_address_mobile_phone");
$form1->add_multi_edit("delivery_mobile_phone_number", array("order_address_mobile_phone_country", "order_address_mobile_phone_area", "order_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array('','',''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


$form1->add_edit("delivery_address_email", "Email",0);
$form1->add_edit("delivery_address_contact", "Contact*" , NOTNULL);

// Transportation Preferences
$form1->add_section("Transportation Preferences");
$form1->add_comment("Please enter the date in the form of dd.mm.yy");
$form1->add_edit("preferred_delivery_date", "Preferred Arrival Date*", NOTNULL,
				"", TYPE_DATE, 20);

$form1->add_list("preferred_transportation_arranged", "Transportation arranged by*",
				"select transportation_type_id, transportation_type_name " .
				"from transportation_types where transportation_type_visible = 1 " .
				" and transportation_type_code = 'arranged' " .
				"order by transportation_type_name", NOTNULL,
				"");

$form1->add_list("preferred_transportation_mode", "Transportation mode*",
				"select transportation_type_id, transportation_type_name " .
				"from transportation_types where transportation_type_visible = 1 " .
				" and transportation_type_code = 'mode' " .
				"order by transportation_type_name", NOTNULL,
				"");


$form1->add_comment("Please indicate if there is a special approval needed for delivery into ".
				   "a pedestrian area."); 
$form1->add_radiolist("pedestrian_mall_approval", "Pedestrian Area Approval Needed",
					 array(0 => "no", 1 => "yes"), 0, 
					 isset($project['order_pedestrian_mall_approval']) ? 
					 $project['order_pedestrian_mall_approval'] : 0);

$form1->add_comment("Please indicate if partial delivery is possible or " .
				   "full delivery is required."); 
$form1->add_radiolist( "full_delivery", "Full Delivery", array(0 => "no", 1 => "yes"), 0);

$form1->add_comment("Please indicate any other circumstances/restrictions " .
				   "concerning delivery and traffic."); 
$form1->add_multiline("delivery_comments", "Delivery Comments", 4,0, $project['order_delivery_comments']);

$form1->add_section("Insurance");
$form1->add_radiolist("order_insurance", array(1=>"Insurance by Tissot/Forwarder", 0=>""), array(1=>"covered",0=>"not covered"), VERTICAL,1);


// Voltage selection(s)

$form1->add_section("Location Info");
$form1->add_list("voltage", "Voltage*",
				"select voltage_id, voltage_name " .
				"from voltages ", NOTNULL,
				isset($project['order_voltage']) ? 
				$project['order_voltage'] : 0);

$form1->add_section("General Comments");
$form1->add_multiline("comments", "Comments", 4,0);


$form1->add_checkbox("PL", $project_manager["name"] . " ". $project_manager["firstname"], 1, 0, "Project Manager");
$form1->add_checkbox("CL", $order_user["name"] . " ". $order_user["firstname"], 1, 0, "Client");

$form1->add_hidden("target_project_id", $project["project_id"]);
$form1->add_hidden("project_leader", $project["project_retail_coordinator"]);
$form1->add_hidden("order_user", $project["order_user"]);
$form1->add_hidden("project_in_archive", 1);
    
$form1->add_button("create_catalogue_order", "Create Catalogue Order");
$form1->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form1->populate();
$form1->process();

if($form1->button("delivery_is_address"))
{	
	if($form1->value("delivery_is_address") == '01') //POS location
	{
		$form1->value("delivery_address_company", "" . $pos_data["posaddress_name"]);
		$form1->value("delivery_address_company2", "" . $pos_data["posaddress_name2"]);
		$form1->value("delivery_address_address", "" . $pos_data["posaddress_address"]);
		$form1->value("delivery_address_address2", "" . $pos_data["posaddress_address2"]);
		$form1->value("delivery_address_zip", "" . $pos_data["posaddress_zip"]);
		$form1->value("delivery_address_phone", "" . $pos_data["posaddress_phone"]);
		$form1->value("delivery_address_mobile_phone", "" . $pos_data["posaddress_mobile_phone"]);
		$form1->value("delivery_address_email", "" . $pos_data["posaddress_email"]);
		$form1->value("delivery_address_contact", "" . $pos_data["posaddress_contact_name"]);

		if(array_key_exists('delivery_street', $form1->items))
		{
			$form1->items['delivery_street']['values'][0] = $pos_data["posaddress_street"];
			$form1->items['delivery_street']['values'][1] = $pos_data["posaddress_street_number"];
		}

		if(array_key_exists('delivery_phone_number', $form1->items))
		{
			$form1->items['delivery_phone_number']['values'][0] = $pos_data["posaddress_phone_country"];
			$form1->items['delivery_phone_number']['values'][1] = $pos_data["posaddress_phone_area"];
			$form1->items['delivery_phone_number']['values'][2] = $pos_data["posaddress_phone_number"];
		}

		if(array_key_exists('delivery_mobile_phone_number', $form1->items))
		{
			$form1->items['delivery_mobile_phone_number']['values'][0] = $pos_data["posaddress_mobile_phone_country"];
			$form1->items['delivery_mobile_phone_number']['values'][1] = $pos_data["posaddress_mobile_phone_area"];
			$form1->items['delivery_mobile_phone_number']['values'][2] = $pos_data["posaddress_mobile_phone_number"];
		}
	}
	elseif($form1->value("delivery_is_address") == '02') // client
	{
		$form1->value("delivery_address_company", "" . $client_address["company"]);
		$form1->value("delivery_address_company2", "" . $client_address["company2"]);
		$form1->value("delivery_address_address", "" . $client_address["address"]);
		$form1->value("delivery_address_address2", "" . $client_address["address2"]);
		$form1->value("delivery_address_zip", "" . $client_address["zip"]);
		$form1->value("delivery_address_phone", "" . $client_address["phone"]);
		$form1->value("delivery_address_mobile_phone", "" . $client_address["mobile_phone"]);
		$form1->value("delivery_address_email", "" . $client_address["email"]);
		$form1->value("delivery_address_contact", "" . $client_address["contact_name"]);

		if(array_key_exists('delivery_street', $form1->items))
		{
			$form1->items['delivery_street']['values'][0] = $client_address["street"];
			$form1->items['delivery_street']['values'][1] = $client_address["streetnumber"];
		}

		if(array_key_exists('delivery_phone_number', $form1->items))
		{
			$form1->items['delivery_phone_number']['values'][0] = $client_address["phone_country"];
			$form1->items['delivery_phone_number']['values'][1] = $client_address["phone_area"];
			$form1->items['delivery_phone_number']['values'][2] = $client_address["phone_number"];
		}

		if(array_key_exists('delivery_mobile_phone_number', $form1->items))
		{
			$form1->items['delivery_mobile_phone_number']['values'][0] = $client_address["mobile_phone_country"];
			$form1->items['delivery_mobile_phone_number']['values'][1] = $client_address["mobile_phone_area"];
			$form1->items['delivery_mobile_phone_number']['values'][2] = $client_address["mobile_phone_number"];
		}
	}
	elseif($form1->value("delivery_is_address") == '03') // standard delivery address
	{
		$form1->value("delivery_address_company", "" . $standard_delivery_address["delivery_address_company"]);
		$form1->value("delivery_address_company2", "" . $standard_delivery_address["delivery_address_company2"]);
		$form1->value("delivery_address_address", "" . $standard_delivery_address["delivery_address_address"]);
		$form1->value("delivery_address_address2", "" . $standard_delivery_address["delivery_address_address2"]);
		$form1->value("delivery_address_zip", "" . $standard_delivery_address["delivery_address_phone"]);
		$form1->value("delivery_address_phone", "" . $standard_delivery_address["delivery_address_phone"]);
		$form1->value("delivery_address_mobile_phone", "" . $standard_delivery_address["delivery_address_mobile_phone"]);
		$form1->value("delivery_address_email", "" . $standard_delivery_address["delivery_address_email"]);
		$form1->value("delivery_address_contact", "" . $standard_delivery_address["delivery_address_contact"]);


		if(array_key_exists('delivery_street', $form1->items))
		{
			$form1->items['delivery_street']['values'][0] = $standard_delivery_address["delivery_address_street"];
			$form1->items['delivery_street']['values'][1] = $standard_delivery_address["delivery_address_streetnumber"];
		}

		if(array_key_exists('delivery_phone_number', $form1->items))
		{
			$form1->items['delivery_phone_number']['values'][0] = $standard_delivery_address["delivery_address_phone_country"];
			$form1->items['delivery_phone_number']['values'][1] = $standard_delivery_address["delivery_address_phone_area"];
			$form1->items['delivery_phone_number']['values'][2] = $standard_delivery_address["delivery_address_phone_number"];
		}

		if(array_key_exists('delivery_mobile_phone_number', $form1->items))
		{
			$form1->items['delivery_mobile_phone_number']['values'][0] = $standard_delivery_address["delivery_address_mobile_phone_country"];
			$form1->items['delivery_mobile_phone_number']['values'][1] = $standard_delivery_address["delivery_address_mobile_phone_area"];
			$form1->items['delivery_mobile_phone_number']['values'][2] = $standard_delivery_address["delivery_address_mobile_phone_number"];
		}
	}
	elseif(count($project_delivery_address) > 0) //project's delivery address
	{
		$form1->value("delivery_address_company", "" . $project_delivery_address["order_address_company"]);
		$form1->value("delivery_address_company2", "" . $project_delivery_address["order_address_company2"]);
		$form1->value("delivery_address_address", "" . $project_delivery_address["order_address_address"]);
		$form1->value("delivery_address_address2", "" . $project_delivery_address["order_address_address2"]);
		$form1->value("delivery_address_zip", "" . $project_delivery_address["order_address_zip"]);
		$form1->value("delivery_address_phone", "" . $project_delivery_address["order_address_phone"]);
		$form1->value("delivery_address_mobile_phone", "" . $project_delivery_address["order_address_mobile_phone"]);
		$form1->value("delivery_address_email", "" . $project_delivery_address["order_address_email"]);
		$form1->value("delivery_address_contact", "" . $project_delivery_address["order_address_contact"]);


		if(array_key_exists('delivery_street', $form1->items))
		{
			$form1->items['delivery_street']['values'][0] = $project_delivery_address["order_address_street"];
			$form1->items['delivery_street']['values'][1] = $project_delivery_address["order_address_street_number"];
		}

		if(array_key_exists('delivery_phone_number', $form1->items))
		{
			$form1->items['delivery_phone_number']['values'][0] = $project_delivery_address["order_address_phone_country"];
			$form1->items['delivery_phone_number']['values'][1] = $project_delivery_address["order_address_phone_area"];
			$form1->items['delivery_phone_number']['values'][2] = $project_delivery_address["order_address_phone_number"];
		}

		if(array_key_exists('delivery_mobile_phone_number', $form1->items))
		{
			$form1->items['delivery_mobile_phone_number']['values'][0] = $project_delivery_address["order_address_mobile_phone_country"];
			$form1->items['delivery_mobile_phone_number']['values'][1] = $project_delivery_address["order_address_mobile_phone_area"];
			$form1->items['delivery_mobile_phone_number']['values'][2] = $project_delivery_address["order_address_mobile_phone_number"];
		}
	}
}
elseif($form1->button("create_catalogue_order"))
{
	$form1->value("delivery_address_phone", $form1->unify_multi_edit_field($form1->items["delivery_phone_number"]));
	$form1->value("delivery_address_mobile_phone", $form1->unify_multi_edit_field($form1->items["delivery_mobile_phone_number"]));
	
	$form1->add_validation("{delivery_address_phone} != '' or {delivery_address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");
	
	if($form1->validate())
	{
		$order_fields = array();
		$order_values = array();

		$delivery_address_fields = array();
		$delivery_address_values = array();

		$order_number = order_generate_order_number(true);

		$order_fields[] = "order_number";
		$order_values[] = dbquote($order_number);

		$order_fields[] = "order_date";
		$order_values[] = "now()";

		$order_fields[] = "order_type";
		$order_values[] = dbquote(2);    // catalog order

		$order_fields[] = "order_client_address";
		$order_values[] = dbquote($client_address["id"]);

		$order_fields[] = "order_user";
		$order_values[] = dbquote(user_id());

		// currencies
		$currency = get_address_currency($client_address["id"]);
		$system_currency = get_system_currency_fields();

		$order_fields[] = "order_client_currency";
		$order_values[] = dbquote($currency["id"]);

		$order_fields[] = "order_system_currency";
		$order_values[] = dbquote($system_currency["id"]);

		$order_fields[] = "order_client_exchange_rate";
		$order_values[] = dbquote($currency["exchange_rate"]);

		$order_fields[] = "order_system_exchange_rate";
		$order_values[] = dbquote($system_currency["exchange_rate"]);

		$order_fields[] = "order_preferred_transportation_arranged";
		$order_values[] = dbquote($form1->value("preferred_transportation_arranged"));

		$order_fields[] = "order_preferred_transportation_mode";
		$order_values[] = dbquote($form1->value("preferred_transportation_mode"));

		$order_fields[] = "order_voltage";
		$order_values[] = dbquote($form1->value("voltage"));

		$order_fields[] = "order_preferred_delivery_date";
		$order_values[] = dbquote(from_system_date($form1->value("preferred_delivery_date")));

		$order_fields[] = "order_full_delivery";
		$order_values[] = dbquote($form1->value("full_delivery"));

		$order_fields[] = "order_pedestrian_mall_approval";
		$order_values[] = dbquote($form1->value("pedestrian_mall_approval"));

		$order_fields[] = "order_delivery_comments";
		$order_values[] = dbquote($form1->value("delivery_comments"));

		$order_fields[] = "order_insurance";
		$order_values[] = dbquote($form1->value("order_insurance"));

		$order_fields[] = "order_special_item_request";
		$order_values[] = dbquote($form1->value("comments"));


		// billing address

		if(count($client_address) > 0 and $client_address["invoice_recipient"] > 0)
		{
			$invoice_address =  get_address($client_address["invoice_recipient"]);
		}
		else
		{
			$invoice_address = $client_address;
		}
		

		$order_fields[] = "order_direct_invoice_address_id";
		$order_values[] = dbquote($invoice_address["id"]);
		
		$order_fields[] = "order_billing_address_company";
		$order_values[] = dbquote($invoice_address["company"]);

		$order_fields[] = "order_billing_address_company2";
		$order_values[] = dbquote($invoice_address["company2"]);

		$order_fields[] = "order_billing_address_address";
		$order_values[] = dbquote($invoice_address["address"]);

		$order_fields[] = "order_billing_address_street";
		$order_values[] = dbquote($invoice_address["street"]);

		$order_fields[] = "order_billing_address_streetnumber";
		$order_values[] = dbquote($invoice_address["streetnumber"]);

		$order_fields[] = "order_billing_address_address2";
		$order_values[] = dbquote($invoice_address["address2"]);

		$order_fields[] = "order_billing_address_zip";
		$order_values[] = dbquote($invoice_address["zip"]);

		$order_fields[] = "order_billing_address_place";
		$order_values[] = dbquote($invoice_address["place"]);

		$order_fields[] = "order_billing_address_place_id";
		$order_values[] = dbquote($invoice_address["place_id"]);

		$order_fields[] = "order_billing_address_country";
		$order_values[] = dbquote($invoice_address["country"]);

		$order_fields[] = "order_billing_address_phone";
		$order_values[] = dbquote($invoice_address["phone"]);

		$order_fields[] = "order_billing_address_phone_country";
		$order_values[] = dbquote($invoice_address["phone_country"]);

		$order_fields[] = "order_billing_address_phone_area";
		$order_values[] = dbquote($invoice_address["phone_area"]);

		$order_fields[] = "order_billing_address_phone_number";
		$order_values[] = dbquote($invoice_address["phone_number"]);

		$order_fields[] = "order_billing_address_mobile_phone";
		$order_values[] = dbquote($invoice_address["mobile_phone"]);

		$order_fields[] = "order_billing_address_mobile_phone_country";
		$order_values[] = dbquote($invoice_address["mobile_phone_country"]);

		$order_fields[] = "order_billing_address_mobile_phone_area";
		$order_values[] = dbquote($invoice_address["mobile_phone_area"]);

		$order_fields[] = "order_billing_address_mobile_phone_number";
		$order_values[] = dbquote($invoice_address["mobile_phone_number"]);

		$order_fields[] = "order_billing_address_email";
		$order_values[] = dbquote($invoice_address["email"]);

		$order_fields[] = "order_billing_address_contact";
		$order_values[] = dbquote($invoice_address["contact"]);


		//POS location
		/*Problem: we do not know the final posaddress id when a
		POS is in the pipeline:
		Question is it necessary to assign the order to a POS?
		*/

		/*
		$order_fields[] = "order_shop_address_company";
		$order_values[] = dbquote($pos_data["posaddress_name"]);

		$order_fields[] = "order_shop_address_company2";
		$order_values[] = dbquote($pos_data["posaddress_name2"]);

		$order_fields[] = "order_shop_address_address";
		$order_values[] = dbquote($pos_data["posaddress_address"]);

		$order_fields[] = "order_shop_address_address2";
		$order_values[] = dbquote($pos_data["posaddress_address2"]);

		$order_fields[] = "order_shop_address_zip";
		$order_values[] = dbquote($pos_data["posaddress_zip"]);

		$order_fields[] = "order_shop_address_place";
		$order_values[] = dbquote($pos_data["posaddress_place"]);

		$order_fields[] = "order_shop_address_country";
		$order_values[] = dbquote($pos_data["posaddress_country"]);

		$order_fields[] = "order_shop_address_phone";
		$order_values[] = dbquote($pos_data["posaddress_phone"]);

		$order_fields[] = "order_shop_address_mobile_phone";
		$order_values[] = dbquote($pos_data["posaddress_mobile_phone"]);

		$order_fields[] = "order_shop_address_email";
		$order_values[] = dbquote($pos_data["posaddress_email"]);
		*/

		$order_fields[] = "date_created";
		$order_values[] = "now()";

		$order_fields[] = "date_modified";
		$order_values[] = "now()";
		
		$order_fields[] = "user_created";
		$order_values[] = dbquote(user_login());

		$order_fields[] = "user_modified";
		$order_values[] = dbquote(user_login());
		

		$sql = "insert into orders (" . join(", ", $order_fields) . ") " .
			   "values (" . join(", ", $order_values) . ")";
		mysql_query($sql) or dberror($sql);

		$order_id = mysql_insert_id();
		
		append_order_state($order_id, "100", 2, 1);


		
		
		$form1->value("delivery_address_address", $form1->unify_multi_edit_field($form1->items["delivery_street"], get_country_street_number_rule($form1->value("delivery_address_country"))));
		
		save_address($form1, 2, $order_id, "", $client_address["id"], $prefix = "delivery_address");


		
		//save all items
		$sql = $sql_items  . " where " . $list_filter;
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$order_item_fields = array();
			$order_item_values = array();
			
			$order_item_fields[] = "order_item_order";
			$order_item_values[] = dbquote($order_id);

			$order_item_fields[] = "order_item_item";
			$order_item_values[] = dbquote($row['order_item_replacement_item_id']);

			$order_item_fields[] = "order_item_category";
			$order_item_values[] = dbquote($row['item_category']);

			$order_item_fields[] = "order_item_type";
			$order_item_values[] = dbquote($row['order_item_replacement_item_type']);

			$order_item_fields[] = "order_item_text";
			$order_item_values[] = dbquote($row['order_item_replacement_item_text']);


			$order_item_fields[] = "order_item_supplier_address";
			$order_item_values[] = dbquote($row["order_item_replacement_supplier_address"]);


						
			// get supplier's address id (first record in case of several)
			if($row["order_item_replacement_item_type"] == 1)
			{
				$sql_s = "select address_id, address_company, supplier_item_price, supplier_item_currency ".
					   "from items ".
					   "left join suppliers on item_id = supplier_item ".
					   "left join addresses on supplier_address = address_id ".
					   "where item_id = " . dbquote($row['order_item_replacement_item_id']);

				$res_s = mysql_query($sql_s) or dberror($sql_s);
				if ($row_s = mysql_fetch_assoc($res_s))
				{
					if ($row_s["address_id"])
					{
					   // price and currency

						if ($row_s["supplier_item_currency"])
						{
							$currency = get_currency($row_s["supplier_item_currency"]);
						}
						else
						{
							$currency = get_address_currency($row_s["address_id"]);
						}

						$order_item_fields[] = "order_item_supplier_currency";
						$order_item_values[] = $currency["id"];

						$order_item_fields[] = "order_item_supplier_exchange_rate";
						$order_item_values[] = $currency["exchange_rate"];

						$order_item_fields[] = "order_item_supplier_price";
						$order_item_values[] = $row_s["supplier_item_price"];

						
					}
				}
			}
			else
			{
				$sql_s = "select address_id, address_company, address_currency ".
					   "from addresses ".
					   "where address_id = " . dbquote($row['order_item_replacement_supplier_address']);

				$res_s = mysql_query($sql_s) or dberror($sql_s);
				if ($row_s = mysql_fetch_assoc($res_s))
				{
					if ($row_s["address_id"])
					{
					   
						$currency = get_address_currency($row_s["address_id"]);

						$order_item_fields[] = "order_item_supplier_currency";
						$order_item_values[] = $currency["id"];

						$order_item_fields[] = "order_item_supplier_exchange_rate";
						$order_item_values[] = $currency["exchange_rate"];
						
					}
				}
			}


			// get orders's currency  & get item data
			
			$currency = get_order_currency($order_id);
			$sql_s = "select * ".
				   "from items ".
				   "where item_id = " . dbquote($row['order_item_replacement_item_id']);;

			$res_s = mysql_query($sql_s) or dberror($sql_s);

			if ($row_s = mysql_fetch_assoc($res_s))
			{
				$order_item_fields[] = "order_item_system_price";
				$order_item_values[] = trim($row_s["item_price"]) == "" ? "null" : dbquote($row_s["item_price"]);
				
				$client_price = $row_s["item_price"] / $currency["exchange_rate"] * $currency["factor"];

				$order_item_fields[] = "order_item_client_price";
				$order_item_values[] = trim($client_price) == "" ? "null" : dbquote($client_price);

				$order_item_fields[] = "order_item_cost_unit_number";
				$order_item_values[] = trim($row_s["category_cost_unit_number"]) == "" ? "null" : dbquote($row_s["category_cost_unit_number"]);
			}

			$order_item_fields[] = "order_item_unit_id";
			$order_item_values[] = dbquote($row["item_unit"]);

			$order_item_fields[] = "order_item_width";
			$order_item_values[] = dbquote($row["item_width"]); 

			$order_item_fields[] = "order_item_length";
			$order_item_values[] = dbquote($row["item_length"]);

			$order_item_fields[] = "order_item_height";
			$order_item_values[] = dbquote($row["item_height"]);

			$order_item_fields[] = "order_item_gross_weight";
			$order_item_values[] = dbquote($row["item_gross_weight"]);

			$order_item_fields[] = "order_item_packaging_type_id";
			$order_item_values[] = dbquote($row["item_packaging_type"]);

			$order_item_fields[] = "order_item_stackable";
			$order_item_values[] = dbquote($row["item_stackable"]);
			
			$order_item_fields[] = "order_item_no_offer_required";
			
			if($row["order_item_replacement_item_type"] == 1)
			{
				$order_item_values[] = dbquote("1");
			}
			else
			{
				$order_item_values[] = dbquote("0");
			}

			if ($row['order_item_replacement_quantity'] > 0)
			{
				$order_item_fields[] = "order_item_quantity";
				$order_item_values[] = dbquote($row['order_item_replacement_quantity']);

				$order_item_fields[] = "date_created";
				$order_item_values[] = "now()";

				$order_item_fields[] = "date_modified";
				$order_item_values[] = "now()";
				
				$order_item_fields[] = "user_created";
				$order_item_values[] = dbquote(user_login());

				$order_item_fields[] = "user_modified";
				$order_item_values[] = dbquote(user_login());

				$sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";

				mysql_query($sql) or dberror($sql);

				$order_item_id = mysql_insert_id();

				save_address($form1, 2, $order_id, $order_item_id, $client_address["id"], $prefix = "delivery_address");

				
				//assign order_item to the project

				$fields = array();
				$values = array();

				$fields[] = "order_items_in_project_order_id";
				$values[] = dbquote($order_id);

				$fields[] = "order_items_in_project_project_order_id";
				$values[] = dbquote($project["project_order"]);

				$fields[] = "order_items_in_project_order_item_id";
				$values[] = dbquote($order_item_id);

				$fields[] = "order_items_in_project_costgroup_id";
				$values[] = dbquote($row["order_item_cost_group"]);


				$fields[] = "order_items_in_project_is_a_replacement_item";
				$values[] = 1;


				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d H:i:s"));


				$sql = "insert into order_items_in_projects (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);

				
				
				//update table order_item_replacements
				$sql = "update order_item_replacements set " .
					   " order_item_replacement_catalog_order_id = " . dbquote($order_id) . ", " . 
					   " order_item_replacement_catalog_order_order_item_id = " . dbquote($order_item_id) . 
					   " where order_item_replacement_id = " . dbquote($row["order_item_replacement_id"]);

				mysql_query($sql) or dberror($sql);
				
				
				//add tracking info
				$result = track_change_in_order_items('New item assigned from catalogue order', 'Quantity', $project["project_order"], $order_item_id, $row["order_item_replacement_order_item_id"],$row["order_item_replacement_item_type"], $row["order_item_replacement_item_text"], 0, $row["order_item_replacement_quantity"]);
			}
		}


		//send email notifivation
		$alert_pl = 0;
		$alert_cl = 0;
		if(array_key_exists("PL", $form1->items) and $form1->value("PL") == 1) {
			$alert_pl = $form1->value("project_leader");
		}

		if(array_key_exists("CL", $form1->items) and $form1->value("CL") == 1) {
			$alert_cl = $form1->value("order_user");
		}

		if($alert_pl > 0 or $alert_cl > 0) {

			$actionmail = new ActionMail(150);
			$actionmail->setParam('target_project_id',$project["project_id"]);
			$actionmail->setParam('oid', $order_id);
			$actionmail->setParam('pl', $alert_pl);
			$actionmail->setParam('cl', $alert_cl);
			$actionmail->setParam('order_link', APPLICATION_URL . '/user/order_task_center.php?oid=' . $order_id);
			
			if($form1->value("project_in_archive") == 1) {
				$actionmail->setParam('project_link', APPLICATION_URL . '/archive/project_view_material_list.php?pid=' . param("target_project_id"));
			}
			else {
				$actionmail->setParam('project_link', APPLICATION_URL . '/user/project_view_material_list.php?pid=' . param("target_project_id"));
			}
			
			if($senmail_activated == true)
			{
				$actionmail->send();
			}
		}

		$link = "/user/order_edit_material_list.php?oid=" . $order_id; 
		redirect($link);
	}
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Replacement of Materials - Create Catalogue Order");
$form->render();
echo "<br>";
$list->render();

if($has_items_to_be_replaced == true)
{
	echo "<br>";
	$form1->render();
}

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>