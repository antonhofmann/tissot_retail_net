<?php
/********************************************************************


    order_edit_attachment.php


    Add comments to a project


    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-27
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-06-18
    Version:        1.0.2


    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
//require_once "include/order_functions.php";


check_access("can_edit_attachment_data_in_orders");




/********************************************************************
    prepare all data needed
*********************************************************************/
$user_roles = get_user_roles(user_id());
$user = get_user(user_id());

// read project and order details
$order = get_order(param("oid"));


// get company's address
$client_address = get_address($order["order_client_address"]);


// buld sql for attachment categories
$sql_attachment_categories = "select order_file_category_id, concat(order_file_category_priority, ' ', order_file_category_name) as group_name ".
                             "from order_file_categories ".
                             "where order_file_category_type = 1 ".
                             "order by order_file_category_priority";


// get addresses involved in the project
$companies = get_involved_companies(param('oid'), id());



// determine users that have already got an email announcing the new attachment
$old_recipients = array();
foreach($companies as $key=>$value)
{
    if($value["access"] == 1)
    {
        $old_recipients[] = $value["user"];
    }
}



//get all involved suppliers
$involved_suppliers = array();

$sql =  "select DISTINCT order_item_supplier_address,  address_company " . 
		"from order_items ".
		"left join addresses on address_id = order_item_supplier_address " . 
		" where (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
				  "   and order_item_quantity > 0 " .
				  "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
				  "   and order_item_order = " . $order["order_id"] . 
				  "   and order_item_supplier_address > 0 " . 
		"order by address_company";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	$involved_suppliers[$row["order_item_supplier_address"]] = $row["address_company"];
}

if(count($involved_suppliers) > 0)
{
	$involved_suppliers['nosupplier'] = "No specific supplier";
}

//check if user is one of the suppliers
$user_is_a_supplier = false;
if(array_key_exists($user["address"], $involved_suppliers))
{
	$user_is_a_supplier = true;
}


//compose invisible div for shipping documents selector
$former_attachment = array();

$sql = "select * " . 
	   "from order_files ". 
	   " where order_file_id = " . dbquote(id());

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
	$former_attachment = $row;
}

$sql_shipping_documents = "select standard_shipping_document_id, standard_shipping_document_name " . 
                          "from address_shipping_documents " . 
						  " left join standard_shipping_documents  on standard_shipping_document_id = address_shipping_document_document_id " . 
						  " where address_shipping_document_address_id = " . dbquote($order["order_client_address"]) . 
						  " order by standard_shipping_document_name";

$shipping_document_categories = array();
$res = mysql_query($sql_shipping_documents) or dberror($sql_shipping_documents);
while ($row = mysql_fetch_assoc($res))
{
	$shipping_document_categories[$row["standard_shipping_document_id"]] = $row["standard_shipping_document_name"];
}

if(count($shipping_document_categories) > 0)
{
	$shipping_document_categories['other'] = "Other";
}

//only allow indicate shipping docs if user is supplier, logistics coordinator or project manager
if($user_is_a_supplier == false
   and !in_array(1, $user_roles)
   and !in_array(2, $user_roles)
   and !in_array(3, $user_roles))
{
	$shipping_document_categories = array();
}



if((count($former_attachment) > 0 and $former_attachment['order_file_category'] == 15)
	or param("order_file_category") == 15)
{
	$shipping_document_category_selector = '<div id="shipping_document_category_selector" style="display:block;">';
}
else
{
	$shipping_document_category_selector = '<div id="shipping_document_category_selector" style="display:none;">';
}




if((count($former_attachment) > 0 and $former_attachment['order_file_category'] == 15)
	or param("order_file_category") == 15)
{
	$shipping_document_category_selector = '<div id="shipping_document_category_selector" style="display:block;">';
}
else
{
	$shipping_document_category_selector = '<div id="shipping_document_category_selector" style="display:none;">';
}


$shipping_document_category_selector .= '<br />Please select the type of the shipping document:<br />';
foreach($shipping_document_categories as $id=>$name)
{
	$checked = "";
	if((count($former_attachment) > 0 and $former_attachment['order_file_standard_shipping_document_id'] == $id) or param("order_file_standard_shipping_document_id") == $id)
	{
		$checked = "checked";
	}
	elseif((count($former_attachment) > 0 and $former_attachment['order_file_standard_shipping_document_id'] == 0) or param("order_file_standard_shipping_document_id") == 'other')
	{
		$checked = "checked";
	}
	$shipping_document_category_selector .= "<input type=\"radio\" name=\"standard_shipping_document_id\" " . $checked . " value=\"" . $id . "\">" . $name . '<br />';
}


if($user_is_a_supplier == false)
{
	$shipping_document_category_selector .= '<br />Please tell us to which supplier this shipping document belongs:<br />';
	foreach($involved_suppliers as $id=>$name)
	{
		$checked = "";
		if((count($former_attachment) > 0 and $former_attachment['order_file_supplier_id'] == $id) or param("order_file_supplier_id") == $id)
		{
			$checked = "checked";
		}
		elseif((count($former_attachment) > 0 and $former_attachment['order_file_supplier_id'] == 0) or param("order_file_supplier_id") == 'nosupplier')
		{
			$checked = "checked";
		}
		$shipping_document_category_selector .= "<input type=\"radio\" name=\"standard_shipping_supplier_id\" " . $checked . " value=\"" . $id . "\">" . $name . '<br />';
	}
}

$shipping_document_category_selector .= '<br /></div>';


//get attachment data
$order_file_standard_shipping_document_id = 0;
$file_category = 0;
$sql = "select order_file_standard_shipping_document_id, order_file_category " . 
		   "from order_files ". 
		   " where order_file_id = " . dbquote(id());

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
	$shipping_document_category_id = $row["order_file_standard_shipping_document_id"];
	$file_category = $row["order_file_category"];
}

// checkbox names
$check_box_names = array();


// get file owner
$owner = get_file_owner(id());






/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("order_files", "file", 640);


$form->add_section("Order");
$form->add_hidden("oid", param('oid'));
$form->add_hidden("order_file_order", param('oid'));
$form->add_hidden("order_file_owner", user_id());


require_once "include/order_head_small.php";


$form->add_section("Attachment");
$form->add_list("order_file_category", "Category*", $sql_attachment_categories, NOTNULL);


if(count($shipping_document_categories) > 0)
{
	$form->add_label("shipping_document_category", "", RENDER_HTML, $shipping_document_category_selector);
}

$form->add_hidden("order_file_standard_shipping_document_id",0);
$form->add_hidden("order_file_supplier_id",0);



$form->add_edit("order_file_title", "Title*", NOTNULL, "", TYPE_CHAR);
$form->add_multiline("order_file_description", "Description*", 4);



$form->add_hidden("supplier1", 0);
$form->add_hidden("supplier2", 0);


if (has_access("can_set_attachment_accessibility_in_orders"))
{
    $form->add_section("Accessibility");
    $form->add_comment("Please indicate who is allowed to view this attachment.");


    $num_checkboxes=1;
    foreach ($companies as $key=>$value_array)
    {
        if(!array_key_exists('role', $value_array)) {
			$value_array["role"] = "";
		}
		$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], $value_array["access"], 0, $value_array["role"]);
        $check_box_names["A" . $num_checkboxes] = $value_array["user"];
        $num_checkboxes++;
    }
}


$order_number = $order["order_number"];


$form->add_section("File");
$form->add_hidden("order_file_type");
$form->add_upload("order_file_path", "File", "/files/orders/$order_number", NOTNULL);


$form->add_section("CC Recipients");
$form->add_modal_selector("ccmails", "Selected Recipients", 8);


$form->add_button("save", "Save");


if(has_access("can_delete_attachment_in_orders") or $owner == user_id())
{
    $form->add_button("delete", "Delete");
}


$form->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("order_file_category"))
{
	$form->value("order_file_standard_shipping_document_id", 0);
}
elseif ($form->button("save"))
{
    
	
	if(count($shipping_document_categories) > 0
		and $form->value("order_file_category") == 15) // shipping document
	{
		if($form->value("order_file_standard_shipping_document_id") == 0
		   or $form->value("order_file_supplier_id") == 0)
		{
			//no validation
		}
		else
		{
			$form->add_validation("{order_file_standard_shipping_document_id} > 0", "You must indicate the type of the shipping document.");
			$form->add_validation("{order_file_supplier_id} > 0", "A shipping document must always be related to a supplier.");
		}
	}

	
	
	//get file_type
	$file_type_is_valid = false;
	$path = $form->value("order_file_path");
	$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

	$sql = "select file_type_id from file_types " . 
		   " where file_type_extension like '%" . $ext ."%'";
	
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value("order_file_type", $row["file_type_id"]);
		$file_type_is_valid = true;
	}


	if($file_type_is_valid == false)
	{
		$form->error("The file type of the file you try to upload has an extension not allowed to be uploaded!");
	}
	elseif($form->validate())
	{

		$form->save();
		
		// check if a recipient was selected
		if (has_access("can_set_attachment_accessibility_in_orders"))
		{
			$no_recipient = 1;
		}
		else
		{
			$no_recipient = 0;
		}


		foreach ($form->items as $item)
		{
			if ($item["type"] == "checkbox" and $item["value"] == "1")
			{
				$no_recipient = 0;
			}
		}


		if ($form->validate()  and $no_recipient == 0)
		{
			update_attachment_accessibility_info(id(), $form,  $check_box_names);

			
			$link = "order_view_attachments.php?oid=" . param("oid");
			redirect($link);
		}
		else
		{
			if($file_category == 999915)
			{
			}
			else
			{
				$form->error("Please select a least one person to have access to the attachment.");
			}
		}
	}
}
elseif ($form->button("delete"))
{
    delete_attachment(id());
    $link = "order_view_attachments.php?oid=" . param("oid");
    redirect($link);
}




$page = new Page("orders");


require "include/order_page_actions.php";


$page->header();
$page->title("Edit Attachment Data");
$form->render();


?>

<script language="Javascript">

	$(document).ready(function(){

		$("#order_file_category").change(function(){
		
			if( $('#shipping_document_category_selector').is(':hidden') ) 
			{
				if($("#order_file_category").val() == 15)
				{
					$("#shipping_document_category_selector").show();
				}
			}
			else
			{
				$("#shipping_document_category_selector").hide();
				$('input[name="standard_shipping_document_id"]').attr('checked', false);
				$("#order_file_standard_shipping_document_id").val(0);

				$('input[name="standard_shipping_supplier_id"]').attr('checked', false);
				$("#order_file_supplier_id").val(0);
			}
		});

		$("input[name='standard_shipping_document_id']").change(function(){
			
			var selectedVal = 0;
			var selected = $("input[type='radio'][name='standard_shipping_document_id']:checked");
			if (selected.length > 0) {
				selectedVal = selected.val();
			}
			
			$("#order_file_standard_shipping_document_id").val(selectedVal);
		});

		
		<?php
		if($user_is_a_supplier == false)
		{
		?>
			$("input[name='standard_shipping_supplier_id']").change(function(){
				
				var selectedVal = 0;
				var selected = $("input[type='radio'][name='standard_shipping_supplier_id']:checked");
				if (selected.length > 0) {
					selectedVal = selected.val();
				}
				$("#order_file_supplier_id").val(selectedVal);
			});
		<?php
		}
		else
		{
		?>
			$("#order_file_supplier_id").val(<?php echo $user["address"];?>);
		<?php
		}
		?>

	});

</script>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#ccmails_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/shared/select_mail_recipients.php'
    });
    return false;
  });
});
</script>

<?php

$page->footer();
?>