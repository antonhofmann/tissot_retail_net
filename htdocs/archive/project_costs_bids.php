<?php
/********************************************************************

    project_costs_bids.php

    View or edit costsheet_bids for a project

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2012, OMEGA SA, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/project_cost_functions.php";

if (has_access("can_view_project_costs") 
   or has_access("can_view_budget_in_projects")
   or has_access('can_edit_project_costs_bids')
   )
{ 
}
else {
	redirect("noaccess.php");
}


if(!param("pid"))
{
	$link = "../user/welcome.php";
	redirect($link);
}



/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));
// get company's address
$client_address = get_address($project["order_client_address"]);
$order_currency = get_order_currency($project["project_order"]);


if($project["order_budget_is_locked"] == 0) {
	$result = update_bid_positions_from_budget(param("pid"));
}


/********************************************************************
    check if project cost positions are present and create if not
*********************************************************************/

$sql = "select count(costsheet_id) as num_recs from costsheets " . 
       " where costsheet_version = 0
			and costsheet_project_id = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) //make the user select a cost template
{
	$link = "project_costs_overview.php?pid=" . param("pid");
	redirect($link);
}


$sql = "select costsheet_bid_id, costsheet_bid_date, costsheet_bid_company, costsheet_bid_project_id, " .
       "DATE_FORMAT(costsheet_bid_date, '%d.%m.%y') as costsheet_biddate, currency_symbol " .
       "from costsheet_bids " . 
	   "left join currencies on currency_id = costsheet_bid_currency ";
$list_filter = "costsheet_bid_project_id = " . param("pid");


$bid_totals = array();
$bid_budget_totals = array();
$sql_b = $sql . " where " . $list_filter;
$res = mysql_query($sql_b) or dberror($sql_b);
while($row = mysql_fetch_assoc($res))
{
	$tmp = get_project_bid_totals($row["costsheet_bid_id"], $order_currency);
	$bid_totals[$row["costsheet_bid_id"]] = number_format($tmp["bid_total"], 2);
	$bid_budget_totals[$row["costsheet_bid_id"]] = number_format($tmp["bid_total_in_budget"], 2);
}


/********************************************************************
	Create Form
*********************************************************************/ 

$form = new Form("projects", "projects");


$form->add_section("Project");
$form->add_hidden("pid", param('pid'));


require_once "include/project_head_small.php";


$form->populate();


/********************************************************************
	Create List of costsheet_bids
*********************************************************************/ 



$list = new ListView($sql);
$list->set_title("Bids");
$list->set_entity("costsheet_bids");
$list->set_order("costsheet_bid_company");
$list->set_filter($list_filter);

$list->add_column("costsheet_bid_company", "Company", "project_costs_bid.php?pid={costsheet_bid_project_id}");
$list->add_column("costsheet_biddate", "Date");
$list->add_column("currency_symbol", "");
$list->add_text_column("totals", "Bid Total", COLUMN_ALIGN_RIGHT, $bid_totals);
$list->add_text_column("budget_totals", "In Budget", COLUMN_ALIGN_RIGHT, $bid_budget_totals);


$link = "project_costs_bid_comparison_pdf.php?pid=" . param("pid");
$link = "javascript:popup('". $link . "', 800, 600)";
$list->add_button("pdf", "Print Bid Comparison", $link);


$link = "javascript:popup('/user/project_costs_bids_pdf.php?pid=" . param("pid") .  "', 800, 600);";
$list->add_button("print_bids", "Print Bids", $link);

$list->populate();
$list->process();


$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Project Costs - Bids");

require_once("include/costsheet_tabs.php");
$form->render();
$list->render();

$page->footer();

?>