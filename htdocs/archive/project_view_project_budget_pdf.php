<?php
/********************************************************************

    project_view_project_budget_pdf.php

    View project budget in a PDF

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-10-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-07
    Version:        1.1.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_budget_in_projects");


/********************************************************************
    prepare pdf
*********************************************************************/

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');

//Instanciation of inherited class
$pdf = new TCPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 10);
$pdf->setPrintHeader(false);

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->Open();

include("project_view_project_budget_pdf_detail.php");

if(	array_key_exists('cerversion', $_GET) and array_key_exists('pid', $_GET))
{
	//check if directory esists
	$file_path = $_SERVER['DOCUMENT_ROOT'] . '/files/ln/' . $project["project_number"];
	if(!file_exists($file_path))
	{
		create_directory($file_path);
	}
	
	if(	array_key_exists('loc', $_GET) and $_GET["loc"] == 1)
	{
		$file_path = $_SERVER['DOCUMENT_ROOT'] . '/files/ln/' . $project["project_number"] . '/cer_budget_' . $_GET['pid'] . '_version_' . $_GET['cerversion'] . '_loc.pdf';
	}
	else
	{
		$file_path = $_SERVER['DOCUMENT_ROOT'] . '/files/ln/' . $project["project_number"] . '/cer_budget_' . $_GET['pid'] . '_version_' . $_GET['cerversion'] . '.pdf';
	}
	$data = $pdf->Output("", "S");
	
	file_put_contents($file_path, $data);
	//echo "success";
}
elseif(isset($write_budget_to_disk) and $write_budget_to_disk == true)
{
	//check if directory esists
	$file_path = $_SERVER['DOCUMENT_ROOT'] . 'files/orders/' . $project["project_number"];
	if(!file_exists($file_path))
	{
		create_directory($file_path);
	}
	
	$file_path = $_SERVER['DOCUMENT_ROOT'] . 'files/orders/' . $project["project_number"] . '/' . $file_name;
	$data = $pdf->Output("", "S");
	file_put_contents($file_path, $data);
}
else
{
	$pdf->Output();
	$pdf->Output($file_name);
}

?>