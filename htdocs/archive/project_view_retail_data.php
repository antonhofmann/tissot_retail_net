<?php
/********************************************************************

    project_view_retail_data.php

    View Retail Assignements.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-08
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-04
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_state_constants.php";

if(!has_access("can_view_client_data_in_projects"))
{
	check_access("can_edit_retail_data");
}

register_param("pid");


/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));
$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);

// get Action parameter
$action_parameter_rto = get_action_parameter(RETAIL_OPERATOR_ASSIGNED, 1);
$action_parameter_rtc = get_action_parameter(RETAIL_COORDINATOR_ASSIGNED, 1);

// get company's address
$client_address = get_address($project["order_client_address"]);

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);

//show project information
require_once "include/project_head_small.php";


$form->add_section("Design Staff");
$line = "concat(user_name, ' ', user_firstname)";
if ($project["project_design_contractor"])
{
    $form->add_lookup("contractor", "Design Contractor", "users", $line, 0, $project["project_design_contractor"]);
}
else
{
    $form->add_label("contractor", "Design Contractor");
}

if ($project["project_design_supervisor"])
{
    $form->add_lookup("design_supervisor", "Design Supervisor", "users", $line, 0, $project["project_design_supervisor"]);
}
else
{
    $form->add_label("design_supervisor", "Design Supervisor");
}


$form->add_section("Confirmation of Delivery by");
$form->add_lookup("delivery_confirmation_by", "Person", "users", $line, 0, $project["order_delivery_confirmation_by"]);

if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
{
	$form->add_section("Project Dates");
}
else
{
	$form->add_section("POS Opening Date");
}

$form->add_label("project_real_opening_date", $project["projectkind_milestone_name_01"], NOTNULL, to_system_date($project["project_real_opening_date"]));

/*
$form->add_section("Miscellanous");
$form->add_checkbox("project_no_planning", "project does not need architectural planning", $project["project_no_planning"], 0, "Planning");
$form->add_checkbox("project_is_local_production", "project is locally realized (local production)", $project["project_is_local_production"], 0, "Local Production");
*/

$form->add_section("Visuals");
$form->add_checkbox("project_uses_icedunes_visuals", "The project uses Visuals", $project["project_uses_icedunes_visuals"], 0, "Visuals");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("View Retail Data");
$form->render();
$page->footer();

?>