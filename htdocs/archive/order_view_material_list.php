<?php
/********************************************************************

    order_view_material_list.php

    View List of Materials

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-03
    Version:        1.2.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_list_of_materials_in_orders");

register_param("oid");

set_referer("order_view_material_list_view_item.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$order = get_order(param('oid'));

// get company's address
$client_address = get_address($order["order_client_address"]);

// get System currency
$system_currency = get_system_currency_fields();

// get orders's currency
$order_currency = get_order_currency($order["order_id"]);



$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
				   "    order_item_po_number, order_item_system_price, order_item_client_price, item_id, ".
				   "    if(order_item_not_in_budget=1 , 'no', 'yes') as order_item_in_budget, ".
				   "    if(item_code <>'', item_code, item_type_name) as item_shortcut, ".
				   "    addresses.address_shortcut as supplier_company, ".
				   "    addresses_1.address_shortcut as forwarder_company, item_category_name, ".
				   "    item_type_id, item_type_name, unit_name, ".
				   "    item_type_priority, item_stock_property_of_swatch " .
				   "from order_items ".
				   "left join items on order_item_item = item_id ".
				   "left join item_categories on item_category_id = item_category ".
				   "left join addresses on order_item_supplier_address = addresses.address_id ".
				   "left join addresses as addresses_1 ".
				   "     on order_item_forwarder_address = addresses_1.address_id ".
				   "left join item_types on order_item_type = item_type_id " .
				   "left join units on unit_id = item_unit";

$where_clause = " where (order_item_type = " . ITEM_TYPE_STANDARD . 
                "    or order_item_type = " . ITEM_TYPE_SPECIAL . ") ".
                "    and order_item_order = " . param('oid');

$values = array();

$images = array();


$res = mysql_query($sql_order_items . $where_clause) or dberror($sql_order_items . $where_clause);
while ($row = mysql_fetch_assoc($res))
{
    $values[$row["order_item_id"]] = $row["order_item_quantity"];

	if($row["item_stock_property_of_swatch"] == 1)
	{
		$property[$row["order_item_id"]] = "/pictures/stockproperty.gif";
	}
	
	$link = "<a class=\"item-files-modal\" data-fancybox-type=\"iframe\" href=\"/applications/templates/item.info.modal.php?id=" . $row["item_id"] . "\"><img border=\"0\" src=\"/pictures/document_view.gif\" /></a>";
		
	$images[$row["order_item_id"]] = $link;
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("orders", "order", 640);

$form->add_section("Order");
$form->add_hidden("oid", param('oid'));
$form->add_hidden("order_file_order", param('oid'));

require_once "include/order_head_small.php";

$form->add_section("Clinte Currency Information");
$form->add_label("currency", "Currency", 0, $order_currency["symbol"]);
$form->add_label("exchange_rate", "Exchange Rate", 0, $order_currency["exchange_rate"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$form->add_comment('<span class="error">All amounts for HQ supplied items converted from CHF into LOC are for reference only as the exchange rate may change.</span>');
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create List for Catalog Items
*********************************************************************/ 
$list1 = new ListView($sql_order_items);

$list1->set_title("Catalog Items");
$list1->set_entity("order_item");
$list1->set_filter("order_item_type = " . ITEM_TYPE_STANDARD . " and order_item_order = " . param('oid'));
$list1->set_order("supplier_company, item_code");
$list1->set_group("item_category_name");

$link="order_view_material_list_view_item.php?oid=" . param('oid');

$list1->add_column("item_shortcut", "Item Code", $link);

$list1->add_text_column("pix", "Pix", COLUMN_UNDERSTAND_HTML, $images);
$list1->add_image_column("property", "", 0, $property);

$list1->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_ALIGN_CENTER);
$list1->add_column("order_item_text", "Name");

$list1->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$list1->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);


$list1->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

if($system_currency["symbol"] != $order_currency["symbol"])
{
	$list1->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}
$list1->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("supplier_company", "Supplier");
$list1->add_column("forwarder_company", "Forwarder");

$entityValue = http_build_query(array(
    'frame' => true,
    'nofilters' => true,
    'entity' => 'order_material_list',
    'entity_values' => array(
        'oid' => param("oid")
    )
));

$list1->add_button(LIST_BUTTON_LINK, "Print Product Info Sheets", array(
    'href' => '/applications/templates/item.booklet.php?'.$entityValue,
    'data-fancybox-type' => 'iframe'
));


/********************************************************************
    Create List for Special Items
*********************************************************************/ 
$list2 = new ListView($sql_order_items);

$list2->set_title("Special Items");
$list2->set_entity("order_item");
$list2->set_filter("order_item_type = " . ITEM_TYPE_SPECIAL . " and order_item_order = " . param('oid'));
$list2->set_order("supplier_company, order_item_text");

$link="order_view_material_list_view_item.php?oid=" . param("oid");

$list2->add_column("item_shortcut", "Item Code", $link);

$list2->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_ALIGN_CENTER);
$list2->add_column("order_item_text", "Name");

$list2->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);


$list2->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

if($system_currency["symbol"] != $order_currency["symbol"])
{
	$list2->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}

$list2->add_column("order_item_po_number", "P.O. Number");
$list2->add_column("supplier_company", "Supplier");
$list2->add_column("forwarder_company", "Forwarder");


/********************************************************************
    Create List for Cost Estimation Positions
*********************************************************************/ 
$list3 = new ListView($sql_order_items);

$list3->set_title("Cost Estimation");
$list3->set_entity("order_item");
$list3->set_filter("order_item_type = " . ITEM_TYPE_COST_ESTIMATION . " and order_item_order = " . param('oid'));
$list3->set_order("item_code");

$list3->add_column("item_shortcut", "Item Code");

$list3->add_column("order_item_text", "Name");


$list3->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
if($system_currency["symbol"] != $order_currency["symbol"])
{
	$list3->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();


$list2->populate();
$list2->process();




$list3->populate();
$list3->process();




/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("View List of Materials");
$form->render();
$list1->render();
$list2->render();
$list3->render();
$page->footer();

?>