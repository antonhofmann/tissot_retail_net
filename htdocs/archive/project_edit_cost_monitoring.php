<?php
/********************************************************************

    project_edit_cost_monitoring.php

    Edit cost monitoring sheet

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-10-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2009-06-04
    Version:        2.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("has_access_to_cost_monitoring");

register_param("pid");

set_referer("project_edit_cost_monitoring_group.php");
set_referer("project_edit_cost_monitoring_reinvoice.php");
set_referer("project_edit_cost_monitoring_preview.php");
set_referer("project_edit_cost_monitoring_remark.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

$show_budget_in_loc = false;
if(array_key_exists("action", $_POST) and $_POST["action"] == 'budget_loc')
{
	$show_budget_in_loc = true;
	$_SESSION["show_budget_in_loc"] = true;
}
elseif(array_key_exists("action", $_POST) and $_POST["action"] == 'budget_chf')
{
	if(array_key_exists("show_budget_in_loc", $_SESSION))
	{
		unset($_SESSION["show_budget_in_loc"]);
	}
	$show_budget_in_loc = false;
}

if(array_key_exists("show_budget_in_loc", $_SESSION) and $_SESSION["show_budget_in_loc"] == true)
{
	$show_budget_in_loc = true;
}

$user = get_user(user_id());
$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));

$order_currency2 = get_order_currency($project["project_order"]);
$system_currency = get_system_currency_fields();

//check if user has the same currency as the project
$user_currency = get_user_currency (user_id());


/*
if($user["country"] ==  $project["order_shop_address_country"]
	and $order_currency2["symbol"] != $system_currency["symbol"])
{
	$show_budget_in_loc = true;
	$_SESSION["show_budget_in_loc"] = true;
}
*/

if(($user["country"] ==  $project["order_shop_address_country"] 
	or $user["address"] ==  $project["order_client_address"])
	and $order_currency2["symbol"] != $system_currency["symbol"])
{
	$show_budget_in_loc = true;
	$_SESSION["show_budget_in_loc"] = true;
}



if($show_budget_in_loc == true)
{
	$order_currency = get_order_currency($project["project_order"]);
}
else
{
	$order_currency = get_system_currency_fields();
}

// get company's address
$client_address = get_address($project["order_client_address"]);



//check if the cost monitoring sheet exists already
$sql = "select count(project_cost_id) as num_recs " .
       "from project_costs " .
       "where project_cost_order = "  . $project["project_order"]; 
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0)
{
    $fields = array();
    $values = array();

    $fields[] = "project_cost_order";
    $values[] = $project["project_order"];
    
    $fields[] = "date_created";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "date_modified";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "user_created";
    $values[] = dbquote(user_login());

    $fields[] = "user_modified";
    $values[] = dbquote(user_login());

    $sql = "insert into project_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

    mysql_query($sql) or dberror($sql);
}

//update order items with predefined cost_groups
$sql = "select order_item_id, item_cost_group, item_costmonitoring_group " . 
       "from order_items " . 
	   "left join items on item_id = order_item_item " .
	   "where order_item_order = " . $project["project_order"] . 
	   "  and order_item_cost_group = 0 and order_item_not_in_budget <> 1 " .
	   "  and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . 
       "  or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
	   "  or order_item_type = " . ITEM_TYPE_SERVICES . ") ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$sql_u = "update order_items Set order_item_cost_group =  " . dbquote($row["item_cost_group"]) . " " . 
		     " where (order_item_cost_group is NULL or order_item_cost_group = 0) " .
		     "  and order_item_id = " . $row["order_item_id"];

	mysql_query($sql_u) or dberror($sql_u);

	/* cost monitoring group was removed by Katrin Eckert on January 26th 2016
	$sql_u = "update order_items Set order_items_costmonitoring_group =  " . dbquote($row["item_costmonitoring_group"]) . " " . 
		     " where (order_items_costmonitoring_group is NULL or order_items_costmonitoring_group = 0) " .
		     "  and order_item_id = " . $row["order_item_id"];

	mysql_query($sql_u) or dberror($sql_u);
	*/
}


//update real cost with system price for HQ supplied items
// price for all other item types has to be entered in the CMS

/*
$sql_u = "update order_items " . 
         "set order_item_real_system_price = order_item_system_price " . 
		 "where (order_item_cost_group = 2 or order_item_type <= " . ITEM_TYPE_SPECIAL . " or order_item_type = " . ITEM_TYPE_SERVICES . " ) and order_item_order = " . $project["project_order"];
*/

//update only standard furniture and services once

$sql_u = "update order_items " . 
		 "set order_item_real_system_price = order_item_system_price " . 
		 "where order_item_quantity > 0 
		  and (order_item_real_system_price = 0 or order_item_real_system_price is null)
		  and (order_item_cost_group = 2 or order_item_type = " . ITEM_TYPE_SERVICES . " ) and order_item_order = " . $project["project_order"];

$result = mysql_query($sql_u) or dberror($sql_u);


//update kl approved investments
/*
$sql = "update cer_investments set cer_investment_amount_cer_loc_approved = cer_investment_amount_cer_loc " . 
	   "where cer_investment_amount_cer_loc_approved = 0 and cer_investment_type in(1, 3, 5, 7, 11) and cer_investment_project = " . param("pid");

$result = mysql_query($sql) or dberror($sql);
*/


//create due date for CMS completion
if($project["project_cost_cms_completion_due_date"] == NULL
 or $project["project_cost_cms_completion_due_date"] == '0000-00-00')
 {
	$pos_opening_date = $project["project_actual_opening_date"];
	if($pos_opening_date != NULL and $pos_opening_date != '0000-00-00')
	{
		$due_date = date('Y-m-d', strtotime("+4 months", strtotime($pos_opening_date)));
		$sql_u = "update project_costs " . 
				 " set project_cost_cms_completion_due_date = " . dbquote($due_date) . 
				 " where project_cost_order = " . dbquote($project["project_order"]);

		$result = mysql_query($sql_u) or dberror($sql_u);
		$project["project_cost_cms_completion_due_date"] = $due_date;
	}
 }


$tracking_info = array();
$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
	   "concat(user_name, ' ', user_firstname) as user_name " . 
	   "from projecttracking " . 
       "left join users on user_id = projecttracking_user_id " . 
       "where projecttracking_project_id = " . param("pid") . 
	   " and projecttracking_field = 'project_cost_cms_completion_due_date' " . 
	   " order by projecttracking_time";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$tracking_info[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
		"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
		"projecttracking_comment"=>$row["projecttracking_comment"],
		"projecttracking_time"=>$row["projecttracking_time"],
		"user_name"=>$row["user_name"]
		);
}


/********************************************************************
    SQLS for lists
*********************************************************************/




if($show_budget_in_loc == true)
{
	$sql_list = "select order_item_id, order_item_text, " .
				"order_item_quantity, order_item_quantity_freezed, order_item_supplier_freetext, order_items_costmonitoring_group, ".
				"order_item_po_number, order_item_client_price,  " . 
				" order_item_real_client_price, order_item_client_price_freezed, " .
				" order_item_client_price_freezed*order_item_quantity_freezed as cost_freezed, " . 
				"item_id, if(item_code is null, 'special item', item_code) as item_shortcut, order_item_cost_group, ".
				"    address_shortcut as supplier_company, ".
				"    item_type_id, item_type_name, ".
				"    item_type_priority, item_stock_property_of_swatch, order_item_cms_remark ".
				"from order_items ".
				"left join items on order_item_item = item_id ".
				"left join addresses on order_item_supplier_address = addresses.address_id ".
				"left join item_types on order_item_type = item_type_id";
}
else
{
	$sql_list = "select order_item_id, order_item_text, " .
				"order_item_quantity, order_item_quantity_freezed, order_item_supplier_freetext, order_items_costmonitoring_group, ".
				"order_item_po_number, order_item_system_price,  " . 
				" order_item_real_system_price, order_item_system_price_freezed, " .
				" order_item_system_price_freezed*order_item_quantity_freezed as cost_freezed, " . 
				"item_id, if(item_code is null, 'special item', item_code) as item_shortcut, order_item_cost_group, ".
				"    address_shortcut as supplier_company, ".
				"    item_type_id, item_type_name, ".
				"    item_type_priority, item_stock_property_of_swatch, order_item_cms_remark ".
				"from order_items ".
				"left join items on order_item_item = item_id ".
				"left join addresses on order_item_supplier_address = addresses.address_id ".
				"left join item_types on order_item_type = item_type_id";
}


//HQ Supplied Items
//$list_sf_filter = "(order_item_cost_group = 2 and (order_item_type <= " . ITEM_TYPE_SPECIAL . " or order_item_type = " . ITEM_TYPE_SERVICES . " )) and order_item_order = " . $project["project_order"];

$list_sf_filter = "order_item_cost_group = 2 and order_item_order = " . $project["project_order"];


//freight charges
$list_fch_filter = "order_item_cost_group = 6  and order_item_order = " . $project["project_order"];

//local construction cost
$list_lc_filter = "order_item_cost_group = 7  and order_item_order = " . $project["project_order"];

//Other Cost
$list_otc_filter = "(order_item_cost_group = 8 or order_item_cost_group = 0) " . 
                "  and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . 
                "  or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
				"  or order_item_type = " . ITEM_TYPE_SERVICES . ") " . 
				"  and order_item_order = " . $project["project_order"];

//architectural construction cost
$list_arch_filter = "order_item_cost_group = 9  and order_item_order = " . $project["project_order"];

//fixturing other cost
$list_fixother_filter = "order_item_cost_group = 10  and order_item_order = " . $project["project_order"];

//Equipment
$list_eq_filter = "order_item_cost_group = 11  and order_item_order = " . $project["project_order"];

$where_clause = " where order_item_not_in_budget <> 1 " .
                "  and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . 
                "  or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
				"  or order_item_type = " . ITEM_TYPE_SERVICES .
                "  ) and order_item_order = " . $project["project_order"];

/*
$where_clause = " where (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . 
                "  or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
				"  or order_item_type = " . ITEM_TYPE_SERVICES .
                "  ) and order_item_order = " . $project["project_order"];
*/



//get remarks for cost monitoring sheet
$sql_remarks = "select project_cost_remark_id, project_cost_remark_remark " .
               "from project_cost_remarks";

/********************************************************************
    Get Item entries fro lists
*********************************************************************/
$quantities = array();
$prices = array();
$quantities_freezed = array();
$prices_freezed = array();
$suppliers = array();
$images = array();
$costgroups = array();
$costtypes = array();
//$costmonitoringgroups = array();
$property = array();
$cms_remarks = array();

$num_recs2 = 0;
$num_recs6 = 0;
$num_recs7 = 0;
$num_recs8 = 0;
$num_recs9 = 0;
$num_recs10 = 0;
$num_recs11 = 0;

$res = mysql_query($sql_list . $where_clause) or dberror($sql_list . $where_clause);
while ($row = mysql_fetch_assoc($res))
{
    $quantities[$row["order_item_id"]] = $row["order_item_quantity"];
    
	if($show_budget_in_loc == true)
	{	
		$prices[$row["order_item_id"]] = $row["order_item_real_client_price"];
		$approved_prices[$row["order_item_id"]] = $row["order_item_real_client_price"];
		$prices_freezed[$row["order_item_id"]] = $row["order_item_client_price_freezed"];

	}
	else
	{
		$prices[$row["order_item_id"]] = $row["order_item_real_system_price"];
		$approved_prices[$row["order_item_id"]] = $row["order_item_real_system_price"];
		$prices_freezed[$row["order_item_id"]] = $row["order_item_system_price_freezed"];
	}

    //$costmonitoringgroups[$row["order_item_id"]] = $row["order_items_costmonitoring_group"];

    $suppliers[$row["order_item_id"]] = $row["order_item_supplier_freetext"];

	$cms_remarks[$row["order_item_id"]] = $row["order_item_cms_remark"];

	
    
    if(!$row["order_item_cost_group"] and ($row["item_type_id"] < ITEM_TYPE_COST_ESTIMATION or $row["item_type_id"] < ITEM_TYPE_SERVICES))
    {
        $costgroups[$row["order_item_id"]] = 2;
    }
    elseif(!$row["order_item_cost_group"] and $row["item_type_id"] == ITEM_TYPE_COST_ESTIMATION)
    {
        $costgroups[$row["order_item_id"]] = 6;
    }
    elseif(!$row["order_item_cost_group"] and $row["item_type_id"] == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
    {
        $costgroups[$row["order_item_id"]] = 7;
    }
    else
    {
        $costgroups[$row["order_item_id"]] = $row["order_item_cost_group"];
    }

    $problem = 0;

    
    if($row["item_type_id"] < ITEM_TYPE_COST_ESTIMATION or $row["item_type_id"] == ITEM_TYPE_SERVICES or $row["item_type_id"] == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
    {
        if($row["order_item_quantity"] != $row["order_item_quantity_freezed"]){$problem = 1;}
    }
    
	if($show_budget_in_loc == true)
	{
		if($row["order_item_real_client_price"] != $row["order_item_client_price_freezed"]){$problem = 1;}
		if($row["order_item_real_client_price"] == 0){$problem = 2;}
	}
	else
	{
		if($row["order_item_real_system_price"] != $row["order_item_system_price_freezed"]){$problem = 1;}
		if($row["order_item_real_system_price"] == 0){$problem = 2;}
	}

    if($problem == 2)
    {
		$images[$row["order_item_id"]] = "/pictures/wf_warning.gif";
	}
	elseif($problem == 1)
    {
        $images[$row["order_item_id"]] = "/pictures/bullet_ball_glass_yellow.gif";
    }
    else
    {
        $images[$row["order_item_id"]] = "/pictures/bullet_ball_glass_green.gif";
    }


    
	if($row["order_item_cost_group"] == 6)  //freight charges
    {
        $num_recs6 = $num_recs6 + 1;
    }
    elseif($row["order_item_cost_group"] == 7)  //local construction cost
    {
        $num_recs7 = $num_recs7 + 1;
    }
	elseif($row["order_item_cost_group"] == 9)  //architectural cost
    {
        $num_recs9 = $num_recs9 + 1;
    }
	elseif($row["order_item_cost_group"] == 10)  //fixturing other cost
    {
        $num_recs10 = $num_recs10 + 1;
    }
	elseif($row["order_item_cost_group"] == 11)  //fixturing other cost
    {
        $num_recs11 = $num_recs11 + 1;
    }
	elseif($row["order_item_cost_group"] == 8) // other cost
    {
        $num_recs8 = $num_recs8 + 1;
    }
    elseif($row["order_item_cost_group"] == 2) // HQ Suplied Items
    {
        $num_recs2 = $num_recs2 + 1;
    }

	if($row["item_stock_property_of_swatch"] == 1)
	{
		$property[$row["order_item_id"]] = "/pictures/stockproperty.gif";
	}

	
	
}

/********************************************************************
    Get Cost Monitoring Sheet CER Data
*********************************************************************/ 
$cer_investments = array();
$cer_investments_approved = array();
$cer_investments_kl_additional_approved = array();

$total_from_cer = 0;
$total_from_cer_approved = 0;
$total_from_cer_additional_approved = 0;
$landlord_contribution = 0;

$sql = "select * from cer_investments " . 
       "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
       "where cer_investment_type in(1, 3, 5, 7, 11, 19) and cer_investment_project = " . param("pid")  .
	   " and cer_investment_cer_version = 0 " . 
	   " order by cer_investment_type";


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res)) {

	if($show_budget_in_loc == true)
	{
		if($row["cer_investment_type"] == 19)
		{
			$landlord_contribution = $project["project_cost_real_landlord_contribution_loc"];
		}
		else
		{
		
			$cer_investments[] = $row["cer_investment_amount_cer_loc"];
			$total_from_cer = $total_from_cer + $row["cer_investment_amount_cer_loc"];
			

			if($project["project_cost_type"] == 1) // corporate
			{
				$cer_investments_approved[] = $row["cer_investment_amount_cer_loc_approved"];
				$cer_investments_kl_additional_approved[] = $row["cer_investment_amount_additional_cer_loc_approved"];
				$total_from_cer_approved = $total_from_cer_approved + $row["cer_investment_amount_cer_loc_approved"];
				$total_from_cer_additional_approved = $total_from_cer_additional_approved + $row["cer_investment_amount_additional_cer_loc_approved"];
			}
			else
			{
				$cer_investments_approved[] = '';
			}
		}
	}
	else
	{
		$exchange_rate = $order_currency2["exchange_rate"];
		$factor = $order_currency2["factor"];
		
		$landlord_contribution = $exchange_rate*$project["project_cost_real_landlord_contribution_loc"]/$factor;
		
		if($row["cer_investment_type"] == 19)
		{
			//nop
		}
		else
		{
			$cer_investments[] = $exchange_rate*$row["cer_investment_amount_cer_loc"]/$factor;
			$total_from_cer = $total_from_cer + ($exchange_rate*$row["cer_investment_amount_cer_loc"]/$factor);

			
			if($project["project_cost_type"] == 1) // corporate
			{
				$cer_investments_approved[] = $exchange_rate*$row["cer_investment_amount_cer_loc_approved"]/$factor;
				$cer_investments_kl_additional_approved[] = $exchange_rate*$row["cer_investment_amount_additional_cer_loc_approved"]/$factor;
				$total_from_cer_approved = $total_from_cer_approved + ($exchange_rate*$row["cer_investment_amount_cer_loc_approved"]/$factor);
				$total_from_cer_additional_approved = $total_from_cer_additional_approved + ($exchange_rate*$row["cer_investment_amount_additional_cer_loc_approved"]/$factor);
			}
			else
			{
				$cer_investments_approved[] = '';
			}
		}
	}

}


for($i=0;$i<5;$i++) {
	if(!array_key_exists($i, $cer_investments)) {
		$cer_investments[$i] = 0;
	}
	if(!array_key_exists($i, $cer_investments_approved)) {
		$cer_investments_approved[$i] = 0;
	}
	if(!array_key_exists($i, $cer_investments_kl_additional_approved)) {
		$cer_investments_kl_additional_approved[$i] = 0;
	}
}


/********************************************************************
    Get Cost Monitoring Sheet Budget Data from project_costs
*********************************************************************/ 

$project_cost_gross_sqms = $project["project_cost_grosssqms"];
$project_cost_total_sqms = $project["project_cost_totalsqms"];
$project_cost_sqms = $project["project_cost_sqms"];

$project_cost_id = $project["project_cost_id"];
$projectctype = $project["project_cost_type"];

$cms_completed = $project["project_cost_cms_completed"];
$cms_completed_by = $project["project_cost_cms_completed_by"];
$cms_completion_date = $project["project_cost_cms_completion_date"];

$cms_completed2 = $project["project_cost_cms_completed2"];
$cms_completed2_by = $project["project_cost_cms_completed2_by"];
$cms_completion2_date = $project["project_cost_cms_completion2_date"];

$cms_approved = $project["project_cost_cms_approved"];
$cms_approved_by = $project["project_cost_cms_approved_by"];
$cms_approved_date = $project["project_cost_cms_approved_date"];

$cms_controlled = $project["project_cost_cms_controlled"];
$cms_controlled_by = $project["project_cost_cms_controlled_by"];
$cms_controlled_date = $project["project_cost_cms_controlled_date"];



//get cms approval person coordinator
$approval_person_name = "";
$sql = "select user_id, concat(user_name, ' ' , user_firstname) as uname " .
       "from users " . 
	   "where user_id = " . dbquote($project["project_cms_approver"]);

$res = mysql_query($sql) or dberror($sql);

if($row = mysql_fetch_assoc($res))
{
	$approval_person_name = $row["uname"];
}

//get cms completion person coordinator
$person_to_complete_cms_name = "";
$sql = "select user_id, concat(user_name, ' ' , user_firstname) as uname " .
       "from users " . 
	   "where user_id = " . dbquote($project["order_retail_operator"]);

$res = mysql_query($sql) or dberror($sql);

if($row = mysql_fetch_assoc($res))
{
	$person_to_complete_cms_name = $row["uname"];
}


//get controllers name
$controller_person_name = "Controller";
$sql = "select user_id, concat(user_name, ' ' , user_firstname) as uname " .
       "from users " .
	   "where user_id> 0 and user_id = " . dbquote($project["project_cost_cms_controlled_by"]);

$res = mysql_query($sql) or dberror($sql);

if($row = mysql_fetch_assoc($res))
{
	$controller_person_name = $row["uname"];
}


/********************************************************************
    project items coming from catalogue orders
*********************************************************************/ 
$has_catalogue_orders = false;

$oip_cms_remarks = array();
if($show_budget_in_loc == true)
{
	$sql_oip = "select order_items_in_project_id, order_items_in_project_order_item_id, " .
			   "order_items_in_project_id, order_item_order, order_items_in_project_cms_remark, " . 
			   " IF(address_shortcut <> '', address_shortcut, order_item_supplier_freetext) as address_shortcut, " .
			   "order_item_text,  order_item_id, " .
			   "DATE_FORMAT(order_item_ordered, '%d.%m.%y') as date1, " .
				"DATE_FORMAT(order_item_pickup, '%d.%m.%y') as date2, " .
				"DATE_FORMAT(order_item_arrival, '%d.%m.%y') as date3, " .
				"order_item_quantity, order_item_client_price, " . 
			   " order_item_po_number, order_item_quantity, project_cost_groupname_name, order_items_in_project_real_client_price " . 
			   " from order_items_in_projects " .
			   " left join order_items on order_item_id = order_items_in_project_order_item_id " .
			   " left join addresses on order_item_supplier_address = address_id ".
			   " left join project_cost_groupnames on project_cost_groupname_id =  order_items_in_project_costgroup_id " .
			   " where order_items_in_project_project_order_id = " . $project["project_order"];
}
else
{
	$sql_oip = "select order_items_in_project_id, order_items_in_project_order_item_id, " .
			   "order_items_in_project_id, order_item_order, order_items_in_project_cms_remark, " . 
			   " IF(address_shortcut <> '', address_shortcut, order_item_supplier_freetext) as address_shortcut, " .
			   "order_item_text,  order_item_id, " .
			   "DATE_FORMAT(order_item_ordered, '%d.%m.%y') as date1, " .
				"DATE_FORMAT(order_item_pickup, '%d.%m.%y') as date2, " .
				"DATE_FORMAT(order_item_arrival, '%d.%m.%y') as date3, " .
				"order_item_quantity, order_item_system_price, " . 
			   " order_item_po_number, order_item_quantity, project_cost_groupname_name, order_items_in_project_real_price " . 
			   " from order_items_in_projects " .
			   " left join order_items on order_item_id = order_items_in_project_order_item_id " .
			   " left join addresses on order_item_supplier_address = address_id ".
			   " left join project_cost_groupnames on project_cost_groupname_id =  order_items_in_project_costgroup_id " .
			   " where order_items_in_project_project_order_id = " . $project["project_order"];
}

$grouptotals_oip = 0;
$order_items_in_project_real_prices = array();
$replacementinfos = array();
$res = mysql_query($sql_oip) or dberror($sql_oip);
while($row = mysql_fetch_assoc($res))
{
	if($show_budget_in_loc == true)
	{
		$grouptotals_oip = $grouptotals_oip  + ($row["order_item_quantity"]*$row["order_items_in_project_real_client_price"]);
		$has_catalogue_orders = true;
		$order_items_in_project_real_prices[$row["order_items_in_project_id"]] = $row["order_items_in_project_real_client_price"];
	}
	else
	{
		
		$grouptotals_oip = $grouptotals_oip  + ($row["order_item_quantity"]*$row["order_items_in_project_real_price"]);
		$has_catalogue_orders = true;
		$order_items_in_project_real_prices[$row["order_items_in_project_id"]] = $row["order_items_in_project_real_price"];
	}

	//check if item is a replacement
	$sql_r = "select count(order_item_replacement_id) as num_recs " . 
		     " from order_item_replacements " . 
		     " where order_item_replacement_catalog_order_order_item_id = " . dbquote($row["order_item_id"]);


	$res_r = mysql_query($sql_r) or dberror($sql_r);
	$row_r = mysql_fetch_assoc($res_r);
	if($row_r["num_recs"] > 0)
	{
		$replacementinfos[$row["order_items_in_project_id"]] = '<span class="error">Replacement</span>';
	}

	$oip_cms_remarks[$row["order_items_in_project_id"]] = $row["order_items_in_project_cms_remark"];

}


/********************************************************************
    Get Group Totals
*********************************************************************/ 
$grouptotals_investment = array();
$grouptotals_investment[2] = 0;
$grouptotals_investment[6] = 0;
$grouptotals_investment[7] = 0;
$grouptotals_investment[8] = 0;
$grouptotals_investment[9] = 0;
$grouptotals_investment[10] = 0;
$grouptotals_investment[11] = 0;
$grouptotals_investment[19] = "";

$grouptotals_hq_investment = array();
$grouptotals_hq_investment[2] = 0;
$grouptotals_hq_investment[6] = 0;

$grouptotals_real = array();
$grouptotals_real[2] = 0;
$grouptotals_real[6] = 0;
$grouptotals_real[7] = 0;
$grouptotals_real[8] = 0;
$grouptotals_real[9] = 0;
$grouptotals_real[10] = 0;
$grouptotals_real[11] = 0;
$grouptotals_real[19] = -1*$landlord_contribution;

$grouptotals_difference_in_cost = array();
$grouptotals_difference_in_cost[2] = 0;
$grouptotals_difference_in_cost[6] = 0;
$grouptotals_difference_in_cost[7] = 0;
$grouptotals_difference_in_cost[8] = 0;
$grouptotals_difference_in_cost[9] = 0;
$grouptotals_difference_in_cost[10] = 0;
$grouptotals_difference_in_cost[11] = 0;
$grouptotals_difference_in_cost[19] = 0;

$grouptotals_difference_in_cost2 = array();
$grouptotals_difference_in_cost2[2] = 0;
$grouptotals_difference_in_cost2[6] = 0;
$grouptotals_difference_in_cost2[7] = 0;
$grouptotals_difference_in_cost2[8] = 0;
$grouptotals_difference_in_cost2[9] = 0;
$grouptotals_difference_in_cost2[10] = 0;
$grouptotals_difference_in_cost2[11] = 0;
$grouptotals_difference_in_cost2[19] = 0;

$project_cost_budget = 0;
$project_cost_budget_hq = 0;

if($show_budget_in_loc == true)
{
		$sql = "select order_item_cost_group, order_item_type, ".
		   "order_item_client_price, order_item_real_client_price, order_item_quantity, ".
		   "order_item_client_price_freezed, order_item_quantity_freezed, ".
		   "order_item_client_price_hq_freezed, order_item_quantity_hq_freezed " . 
		   "from order_items ".
		   "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
		   "   and order_item_order=" . $project["project_order"] . 
		   "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
		   "   or order_item_type = " . ITEM_TYPE_SERVICES .
		   "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
		   " ) " . 
		   " and order_item_cost_group > 0 " . 
		   "order by order_item_cost_group";

}
else
{
	$sql = "select order_item_cost_group, order_item_type, ".
		   "order_item_system_price, order_item_real_system_price, order_item_quantity, ".
		   "order_item_system_price_freezed, order_item_quantity_freezed, ".
		   "order_item_system_price_hq_freezed, order_item_quantity_hq_freezed " . 
		   "from order_items ".
		   "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
		   "   and order_item_order=" . $project["project_order"] . 
		   "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
		   "   or order_item_type = " . ITEM_TYPE_SERVICES .
		   "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
		   " ) " . 
		   " and order_item_cost_group > 0 " . 
		   "order by order_item_cost_group";
}

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	
	if($show_budget_in_loc == true)
	{
		$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"];
		
		$project_cost_budget = $project_cost_budget + $row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"];

		if($row["order_item_cost_group"] == 2 or $row["order_item_cost_group"] == 6)
		{
			$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_client_price_hq_freezed"] * $row["order_item_quantity_hq_freezed"];

			$project_cost_budget_hq = $project_cost_budget_hq + $row["order_item_client_price_hq_freezed"] * $row["order_item_quantity_hq_freezed"];
		}

		$grouptotals_real[$row["order_item_cost_group"]] =  $grouptotals_real[$row["order_item_cost_group"]] + ($row["order_item_real_client_price"] * $row["order_item_quantity"]);
	}
	else
	{
		$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"];
		
		$project_cost_budget = $project_cost_budget + $row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"];
		
		if($row["order_item_cost_group"] == 2 or $row["order_item_cost_group"] == 6)
		{
			$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_hq_freezed"] * $row["order_item_quantity_hq_freezed"];
				
			$project_cost_budget_hq = $project_cost_budget_hq + $row["order_item_system_price_hq_freezed"] * $row["order_item_quantity_hq_freezed"];
		}

		$grouptotals_real[$row["order_item_cost_group"]] =  $grouptotals_real[$row["order_item_cost_group"]] + ($row["order_item_real_system_price"] * $row["order_item_quantity"]);
	}

}


//add catalogue orders
$grouptotals_real[2] = $grouptotals_real[2] + $grouptotals_oip;
$grouptotals_real_oip[2] = number_format($grouptotals_oip, 2, ".", "'");

$grouptotals_investment1 = array();
$grouptotals_hq_investment1 = array();
$grouptotals_real1 = array();
foreach($grouptotals_investment as $key=>$value)
{
	if($value == ''){$value = 0;}
	$grouptotals_investment1[$key] = number_format($value, 2, ".", "'");
	
	if($key == 2 or $key == 6)
	{
		$grouptotals_hq_investment1[$key] = number_format($grouptotals_hq_investment[$key], 2, ".", "'");
	}

	$grouptotals_real1[$key] = number_format($grouptotals_real[$key], 2, ".", "'");
}


//calculate difference in percent
$project_cost_real = 0;
$project_cost_real_hq = 0;
foreach($grouptotals_real as $key=>$value)
{
     $project_cost_real = $project_cost_real + $value;

	 if($key == 2 or $key == 6) // hq supplied items, ferigth charges
	 {
		$project_cost_real_hq = $project_cost_real_hq + $value;
	 }


     $grouptotals_difference_in_cost[$key] =  number_format($grouptotals_real[$key] - $grouptotals_investment[$key], 2, ".", "'");
     
     if($grouptotals_investment[$key] > 0)
     {
        $grouptotals_difference_in_cost2[$key] =  $grouptotals_real[$key] - $grouptotals_investment[$key];
        $grouptotals_difference_in_cost2[$key] = -1 * (1- ($value / $grouptotals_investment[$key]));
     }
     else
     {
        $grouptotals_difference_in_cost2[$key] = 1;
        $grouptotals_investment[$key] = 0;
     }
     
     $grouptotals_difference_in_cost2[$key] = number_format(100*$grouptotals_difference_in_cost2[$key], 2, ".", "'")  . "%";
     
     $grouptotals_real[$key] = number_format($value, 2, ".", "'");
}

foreach($grouptotals_investment as $key=>$value)
{
     $grouptotals_investment[$key] = number_format($value, 2, ".", "'");
}


//hq supplied items
$project_cost_diff1_hq =  $project_cost_real_hq - $project_cost_budget_hq;

if($project_cost_budget_hq == 0 and $project_cost_real_hq == 0)
{
	 $project_cost_diff2_hq = 0;
}
elseif($project_cost_budget_hq > 0)
{
    $project_cost_diff_hq2 = -1 * ( 1 - ($project_cost_real_hq / $project_cost_budget_hq));
}
else
{
    $project_cost_diff2_hq = 0;
}

//all items
$project_cost_diff1 =  $project_cost_real - $project_cost_budget;

if($project_cost_budget == 0 and $project_cost_real == 0)
{
	 $project_cost_diff2 = 0;
}
elseif($project_cost_budget > 0)
{
    $project_cost_diff2 = -1 * ( 1 - ($project_cost_real / $project_cost_budget));
}
else
{
    $project_cost_diff2 = 0;
}

if($total_from_cer_approved > 0 and $project["project_cost_type"] == 1) //corporate
{
	$project_cost_diff3 = -1 * ( 1 - ($project_cost_real / ($total_from_cer_approved + $total_from_cer_additional_approved)));
}
else
{
    $project_cost_diff3 = 0;
}


$grouptotals_real[2] = 0;
$grouptotals_real[6] = 0;
$grouptotals_real[7] = 0;
$grouptotals_real[8] = 0;
$grouptotals_real[9] = 0;
$grouptotals_real[10] = 0;
$grouptotals_real[11] = 0;



/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);

require_once "include/project_head_small.php";

$form->add_label("spacer", "", 0, "");

//$form->add_label("project_cost_gross_sqms", "Gross Surface sqms", 0, $project_cost_gross_sqms);
$form->add_label("project_cost_total_sqms", "Total Surface sqms", 0, $project_cost_total_sqms);
$form->add_label("project_cost_sqms", "Sales Surface sqms", 0, $project_cost_sqms);

$form->add_label("spacer1", "", 0, "");

if($show_budget_in_loc == true)
{
	$form->add_label("cb1l", "Total Budget approved by Client " . $order_currency["symbol"], 0, number_format($project_cost_budget, 2, ".", "'"));
	$form->add_label("cb4l", "Real Cost (Material, Freight, Local Cost) " . $order_currency["symbol"], 0, number_format($project_cost_real, 2, ".", "'"));

	if($project_cost_diff2 > 0)
	{
		$output = number_format(100*$project_cost_diff2, 2, ".", "'") . "%";
		$output = "<font color='#FF0000'><strong>" . $output . "</strong></font>";
		$form->add_label("cb5l", "Real Cost above approved Budget " . $order_currency["symbol"], RENDER_HTML, $output);
	}
	else
	{
		$output = number_format(100*$project_cost_diff2, 2, ".", "'") . "%";
		$form->add_label("cb5l", "Total Real Cost below approved Budget " . $order_currency["symbol"], 0, $output);
	}
}
else
{
	$form->add_label("cb1", "Total Budget approved by Client " . $order_currency["symbol"], 0, number_format($project_cost_budget, 2, ".", "'"));
	$form->add_label("cb4", "Real Cost (Material, Freight, Local Cost) " . $order_currency["symbol"], 0, number_format($project_cost_real, 2, ".", "'"));

	if($project_cost_diff2 > 0)
	{
		$output = number_format(100*$project_cost_diff2, 2, ".", "'") . "%";
		$output = "<font color='#FF0000'><strong>" . $output . "</strong></font>";
		$form->add_label("cb5", "Real Cost above approved Budget " . $order_currency["symbol"], RENDER_HTML, $output);
	}
	else
	{
		$output = number_format(100*$project_cost_diff2, 2, ".", "'") . "%";
		$form->add_label("cb5", "Total Real Cost below approved Budget " . $order_currency["symbol"], 0, $output);
	}
}


if($projectctype == 1) // Corporate
{
    
	if($show_budget_in_loc == true)
	{
		$form->add_label("spacer2", " ", 0, "");
		$form->add_label("title1_loc", "KL approved investment", 0, number_format($total_from_cer_approved + $total_from_cer_additional_approved, 2, ".", "'"));

		$form->add_label("cer1_loc", "- Construction - Buildung " . $order_currency["symbol"], 0 , number_format($cer_investments_approved[0] + $cer_investments_kl_additional_approved[0], 2, ".", "'"));
		$form->add_label("cer2_loc", "- Store fixturing / Furniture " . $order_currency["symbol"], 0 , number_format($cer_investments_approved[1] + $cer_investments_kl_additional_approved[1], 2, ".", "'"));
		$form->add_label("cer3_loc", "- Architectural Services " . $order_currency["symbol"], 0 , number_format($cer_investments_approved[2] + $cer_investments_kl_additional_approved[2], 2, ".", "'"));
		$form->add_label("cer4_loc", "- Equipment " . $order_currency["symbol"], 0 , number_format($cer_investments_approved[3] + $cer_investments_kl_additional_approved[3], 2, ".", "'"));
		$form->add_label("cer5_loc", "- Other " . $order_currency["symbol"], 0 , number_format($cer_investments_approved[4] + $cer_investments_kl_additional_approved[4], 2, ".", "'"));
		
		
		$form->add_label("cb51_loc", "Real Cost in " . $order_currency["symbol"], 0, number_format($project_cost_real, 2, ".", "'"));
		if($project_cost_diff3 >= 0)
		{
			$output = number_format(100*$project_cost_diff3, 2, ".", "'") . "%";
			$output = "<font color='#FF0000'><strong>" . $output . "</strong></font>";
			$form->add_label("cb6_loc", "Real Cost above Investment ", RENDER_HTML, $output);
		}
		else
		{
			$output = number_format(100*$project_cost_diff3, 2, ".", "'") . "%";
			$form->add_label("cb6_loc", "Real Cost below Investment ", 0, $output);
		}
	}
	else
	{
		$form->add_label("spacer2", " ", 0, "");
		$form->add_label("title1", "KL approved investment", 0, number_format($total_from_cer_approved + $total_from_cer_additional_approved, 2, ".", "'"));

		$form->add_label("cer1", "- Construction - Buildung " . $order_currency["symbol"], 0 , number_format($cer_investments_approved[0] + $cer_investments_kl_additional_approved[0], 2, ".", "'"));
		$form->add_label("cer2", "- Store fixturing / Furniture " . $order_currency["symbol"], 0 , number_format($cer_investments_approved[1] + $cer_investments_kl_additional_approved[1], 2, ".", "'"));
		$form->add_label("cer3", "- Architectural Services " . $order_currency["symbol"], 0 , number_format($cer_investments_approved[2] + $cer_investments_kl_additional_approved[2], 2, ".", "'"));
		$form->add_label("cer4", "- Equipment " . $order_currency["symbol"], 0 , number_format($cer_investments_approved[3] + $cer_investments_kl_additional_approved[3], 2, ".", "'"));
		$form->add_label("cer5", "- Other " . $order_currency["symbol"], 0 , number_format($cer_investments_approved[4] + $cer_investments_kl_additional_approved[4], 2, ".", "'"));
		
		
		$form->add_label("cb51", "Real Cost in " . $order_currency["symbol"], 0, number_format($project_cost_real, 2, ".", "'"));
		if($project_cost_diff3 >= 0)
		{
			$output = number_format(100*$project_cost_diff3, 2, ".", "'") . "%";
			$output = "<font color='#FF0000'><strong>" . $output . "</strong></font>";
			$form->add_label("cb6", "Real Cost above approved Investment ", RENDER_HTML, $output);
		}
		else
		{
			$output = number_format(100*$project_cost_diff3, 2, ".", "'") . "%";
			$form->add_label("cb6", "Real Cost below approved Investment ", 0, $output);
		}	
	}
}

$form->add_section("Project Costs: Landlord's Contribution");

if($show_budget_in_loc == true)
{
	$form->add_edit("project_cost_real_landlord_contribution_loc", "Landlord's contribution in " . $order_currency2["symbol"], 0 , $landlord_contribution, TYPE_DECIMAL, 12,2);
}
else
{
	$form->add_label("project_cost_real_landlord_contribution", "Landlord's contribution in CHF", 0 , $landlord_contribution);
}


if(has_access("can_edit_cost_monitoring_due_date"))
{
	$form->add_section("CMS Completion Due Date");
	$form->add_hidden("old_project_cost_cms_completion_due_date", $project["project_cost_cms_completion_due_date"]);
	if($project["project_cost_cms_completion_due_date"] != NULL and $project["project_cost_cms_completion_due_date"] != '0000-00-00')
	{
		$form->add_edit("project_cost_cms_completion_due_date", "Due Date", 0 , to_system_date($project["project_cost_cms_completion_due_date"]), TYPE_DATE, 10, 0, 1, "changehistory");
		$form->add_edit("change_comment", "Reason for changing the due date", 0);
	}
	else
	{
		$form->add_edit("project_cost_cms_completion_due_date", "Due Date for Completion", 0 , to_system_date($project["project_cost_cms_completion_due_date"]), TYPE_DATE, 10);
		$form->add_hidden("change_comment");
	}
}
else
{
	$form->add_hidden("project_cost_cms_completion_due_date", to_system_date($project["project_cost_cms_completion_due_date"]));
	$form->add_hidden("change_comment");
}

/*
$form->add_hidden("old_project_cost_cms_completion_due_date", $project["project_cost_cms_completion_due_date"]);
$form->add_hidden("project_cost_cms_completion_due_date", to_system_date($project["project_cost_cms_completion_due_date"]));
*/


/* solved via steps in task center 17.08.2015
$form->add_section("CMS Logistic Completion");
if(has_access("can_complete_cms") and $cms_completed == 0)
{
	$form->add_comment("Please confirm after you have completed the cost information of your items.");
	$form->add_label("completor1", "Person to complete the cost information", 0, $person_to_complete_cms_name);
	$form->add_checkbox("completionstate", "completed", $cms_completed, 0, "Logistic Completion State");
}
else
{
	$form->add_lookup("completor2", "Person having completed", "users", "concat(user_name, ' ', user_firstname)", 0, $cms_completed_by);
	
	$form->add_label("completiondate", "Date completed", 0, to_system_date($cms_completion_date));
	$form->add_hidden("completionstate", $cms_completed);
	
}
*/

if($cms_completed)
{
	$form->add_section("CMS Logistic Completion");
	$form->add_lookup("completor2", "Person having completed", "users", "concat(user_name, ' ', user_firstname)", 0, $cms_completed_by);
	$form->add_label("completiondate", "Date completed", 0, to_system_date($cms_completion_date));
	$form->add_hidden("completionstate", $cms_completed);
}

if($cms_completed2)
{
	$form->add_section("CMS Client Completion");
	$form->add_lookup("completor22", "Person having completed", "users", "concat(user_name, ' ', user_firstname)", 0, $cms_completed2_by);
	$form->add_label("completiondate2", "Date completed", 0, to_system_date($cms_completion2_date));
	$form->add_hidden("completionstate2", $cms_completed2);
}

/* solved via steps in task center 17.08.2015
$form->add_section("CMS Development Completion");
if(has_access("can_complete_cms") and $cms_approved == 0)
{
	$form->add_comment("Please confirm after you have completed the cost information of your items.");
	$form->add_label("approver1", "Person to complete the cost information", 0, $approval_person_name);
	$form->add_checkbox("approvalstate", "completed", $cms_approved, 0, "Development Completion State");
}
else
{
	$form->add_lookup("approver2", "Person having completed", "users", "concat(user_name, ' ', user_firstname)", 0, $cms_approved_by);
	$form->add_label("approvaldate", "Date completed", 0, to_system_date($cms_approved_date));
	$form->add_hidden("approvalstate", $cms_approved);

}
*/
if($cms_approved)
{
	$form->add_section("CMS Development Completion");
	$form->add_lookup("approver2", "Person having completed", "users", "concat(user_name, ' ', user_firstname)", 0, $cms_approved_by);
	$form->add_label("approvaldate", "Date completed", 0, to_system_date($cms_approved_date));
	$form->add_hidden("approvalstate", $cms_approved);
}

/* solved via steps in task center 17.08.2015
if($project["project_cost_type"] == 1) // legal type = corporate
{
	$form->add_section("CMS Controlling Approval");
	if(has_access("can_approve_cms") and $cms_completed == 1 and $cms_approved == 1 and $cms_controlled == 0)
	{
		$form->add_comment("Please approve the completion of the CMS.");
		$form->add_label("controller1", "Person to approve", 0, $controller_person_name);
		$form->add_checkbox("controlledstate", "approved", $cms_controlled, 0, "Approval State");
	}
	else
	{
		$form->add_lookup("controller2", "Person having approved", "users", "concat(user_name, ' ', user_firstname)", 0, $cms_controlled_by);
		$form->add_label("controlleddate", "Date of approval", 0, to_system_date($cms_controlled_date));
		$form->add_hidden("controlledstate", $cms_controlled);

	}
}
else
{
	$form->add_hidden("controlleddate",  to_system_date($cms_controlled_date));
	$form->add_hidden("controlledstate", $cms_controlled);
}

*/

if($cms_controlled)
{
	if($project["project_cost_type"] == 1) // legal type = corporate
	{
		$form->add_section("CMS Controlling Approval");
		$form->add_lookup("controller2", "Person having approved", "users", "concat(user_name, ' ', user_firstname)", 0, $cms_controlled_by);
		$form->add_label("controlleddate", "Date of approval", 0, to_system_date($cms_controlled_date));
		$form->add_hidden("controlledstate", $cms_controlled);
	}
	else
	{
		$form->add_hidden("controlleddate",  to_system_date($cms_controlled_date));
		$form->add_hidden("controlledstate", $cms_controlled);
	}
}

if(param("s") == 1)
{
    $form->message = "Your changes have been saved.";
}


if(has_access("can_print_cost_monitoring_sheet"))
{
	$link = "project_edit_cost_monitoring_preview_pdf.php?pid=" . param("pid") . "&chf=1"; 
	$link = "javascript:popup('". $link . "', 800, 600)";
	$form->add_button("pdf_chf", "Print PDF in CHF", $link);

	if($system_currency["symbol"] != $order_currency2['symbol'])
	{
		$link = "project_edit_cost_monitoring_preview_pdf.php?pid=" . param("pid"); 
		$link = "javascript:popup('". $link . "', 800, 600)";
		$form->add_button("pdf_loc", "Print PDF in " . $order_currency2["symbol"], $link);
	}
}


$form->add_button("preview", "Preview CMS");


if($user_currency["currency_symbol"] != $order_currency2["symbol"])
{
	if($show_budget_in_loc == true and $order_currency2["symbol"] != 'CHF')
	{
		$form->add_button("budget_chf", "Switch List to CHF");
		
	}
	elseif($order_currency2["symbol"] != 'CHF')
	{
		$form->add_button("budget_loc", "Switch List to " . $order_currency2["symbol"]);
	}
}

if(has_access("can_edit_cost_monitoring_due_date"))
{
	$form->add_button("save_form", "Save Data");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("save_form"))
{
    if($form->value("old_project_cost_cms_completion_due_date") 
		and $form->value("old_project_cost_cms_completion_due_date") != '0000-00-00'
		and !$form->value("project_cost_cms_completion_due_date"))
	{
		$form->value("project_cost_cms_completion_due_date", $form->value("old_project_cost_cms_completion_due_date"));
	}
	
	if($form->value("old_project_cost_cms_completion_due_date") 
		and $form->value("old_project_cost_cms_completion_due_date") != '0000-00-00'
		and $form->value("old_project_cost_cms_completion_due_date") != from_system_date($form->value("project_cost_cms_completion_due_date")))
	{
		$form->add_validation("{change_comment} != ''", "Please indicate the reason for changing the due date!");
	}

	


    if ($form->validate())
    {
				
		$approval_fields = "";

		$approval_fields .= "project_cost_cms_completion_due_date = '" . from_system_date($form->value("project_cost_cms_completion_due_date")) . "', ";


		if(trim($form->value("old_project_cost_cms_completion_due_date")) != trim(from_system_date($form->value("project_cost_cms_completion_due_date"))))
		{
			//project tracking
			$field = "project_cost_cms_completion_due_date";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote(to_system_date($form->value("old_project_cost_cms_completion_due_date"))) . ", " . 
				   dbquote(to_system_date($form->value("project_cost_cms_completion_due_date"))) . ", " . 
				   dbquote($form->value("change_comment")) . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
		}

		
		/* Approval was replaced by steps in the task center 17.8.2015
		if($cms_completed == 0 and 
			$form->value("completionstate") == 1 ) //Logistic CMS was completed
		{
			$approval_fields = "project_cost_cms_completed = 1, ";
			$approval_fields .= "project_cost_cms_completed_by = '" . user_id() . "', ";
			$approval_fields .= "project_cost_cms_completion_date = '" . date("Y.m.d") . "', ";


						
		}
		

		if($project["project_cost_type"] == 1) // legal type corporate
		{
			
			if($cms_approved == 0 and
				$form->value("approvalstate") == 1) //Development CMS was completed
			{
				$approval_fields = "project_cost_cms_approved = 1, ";
				$approval_fields .= "project_cost_cms_approved_by = '" . user_id() . "', ";
				$approval_fields .= "project_cost_cms_approved_date = '" . date("Y.m.d") . "', ";
			}
			


			if($cms_controlled == 0 and
				$form->value("controlledstate") == 1) //CMS was approved by controller
			{
				
				$approval_fields = "project_cost_cms_controlled = 1, ";
				$approval_fields .= "project_cost_cms_controlled_by = '" . user_id() . "', ";
				$approval_fields .= "project_cost_cms_controlled_date = '" . date("Y.m.d") . "', ";

				
				//check if the POS has pictures uploaded
				$pos_pictures_uploaded = false;

				$sql = "select count(order_file_id) as num_recs from order_files " . 
					   " where order_file_category = 11 " . 
					   " and order_file_order = " . dbquote($project["project_order"]);

				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				if ($row["num_recs"] > 0)
				{
					$pos_pictures_uploaded = true;
				}
		
				if($pos_pictures_uploaded == true 
					and $project["project_actual_opening_date"] != NULL 
					and $project["project_actual_opening_date"] != '0000-00-00'
					and ($project["order_archive_date"] == NULL 
					or $project["order_archive_date"] == '0000-00-00'))
				{
					if($project["order_actual_order_state_code"] < '890')
					{
						append_order_state($project["project_order"], '890', 1, 1);
						set_archive_date($project["project_order"]);
					}
				}

				//delete all tasks for this user and this project
				$sql = "delete from tasks where task_order = " . dbquote($project["project_order"]) . 
					   " and task_user = " . dbquote($project["project_cms_approver"]);

				$result = mysql_query($sql) or dberror($sql);
				
			}
		}
		else
		{

			if($cms_approved == 0 and
				$form->value("approvalstate") == 1) //Development CMS was completed
			{
				$approval_fields = "project_cost_cms_approved = 1, ";
				$approval_fields .= "project_cost_cms_approved_by = '" . user_id() . "', ";
				$approval_fields .= "project_cost_cms_approved_date = '" . date("Y.m.d") . "', ";


				//check if the POS has pictures uploaded
				$pos_pictures_uploaded = false;

				$sql = "select count(order_file_id) as num_recs from order_files " . 
					   " where order_file_category = 11 " . 
					   " and order_file_order = " . dbquote($project["project_order"]);

				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				if ($row["num_recs"] > 0)
				{
					$pos_pictures_uploaded = true;
				}
		

				if($pos_pictures_uploaded == true 
					and $project["project_actual_opening_date"] != NULL 
					and $project["project_actual_opening_date"] != '0000-00-00'
					and ($project["order_archive_date"] == NULL 
					or $project["order_archive_date"] == '0000-00-00'))
				{
					if($project["order_actual_order_state_code"] < '890')
					{
						append_order_state($project["project_order"], '890', 1, 1);
						set_archive_date($project["project_order"]);

					}
				}

				//delete all tasks for this user and this project
				$sql = "delete from tasks where task_order = " . dbquote($project["project_order"]) . 
					   " and task_user = " . dbquote($project["project_cms_approver"]);

				$result = mysql_query($sql) or dberror($sql);
				
			}
		}
		*/

        
		if($approval_fields)
		{
			$sql = "update project_costs set " .
				   substr($approval_fields, 0, strlen($approval_fields)- 2) . 
				   " where project_cost_order = " . $project["project_order"];
			$result = mysql_query($sql) or dberror($sql);
		
		}

		
		if($show_budget_in_loc == true)
		{
			$landlord_contribution = $form->value("project_cost_real_landlord_contribution_loc");
			$fields = "project_cost_real_landlord_contribution_loc = " . dbquote($landlord_contribution);;

			$sql = "update project_costs set " .
					   $fields . 
					   " where project_cost_order = " . $project["project_order"];
			$result = mysql_query($sql) or dberror($sql);
		}
		

        
		$link = "project_edit_cost_monitoring.php?pid=" . param("pid") . "&s=1"; 
		redirect($link);
    }
}
elseif ($form->button("preview"))
{
	$link = "project_edit_cost_monitoring_preview.php?pid=" . param("pid"); 
	redirect($link);
}



if($landlord_contribution > 0)
{
	$sql_cost_groups = "select * from project_cost_groupnames";
}
else
{
	$sql_cost_groups = "select * from project_cost_groupnames where project_cost_groupname_active = 1";
}


/********************************************************************
	LIST'S SECTION
*********************************************************************/ 

if($projectctype == 1)  // Corporate
{
    /********************************************************************
        Create List for Cost Types
    *********************************************************************/ 
    $list_cgs = new ListView($sql_cost_groups, LIST_HAS_HEADER | LIST_HAS_FOOTER);
    $list_cgs->set_title("Cost Groups");
    $list_cgs->set_entity("project_cost_groupnames");
    $list_cgs->set_order("project_cost_groupname_sortorder");



    $list_cgs->add_column("project_cost_groupname_name", "Group");

    $list_cgs->add_text_column("investment_hq", "Approved 620", COLUMN_ALIGN_RIGHT, $grouptotals_hq_investment1);
	$list_cgs->add_text_column("investment", "Approved 840", COLUMN_ALIGN_RIGHT, $grouptotals_investment1);

    $list_cgs->add_text_column("real_cost", "Real Cost " . $order_currency["symbol"], COLUMN_ALIGN_RIGHT, $grouptotals_real1);

	if($grouptotals_oip > 0)
	{
		$list_cgs->add_text_column("real_cost", "CO", COLUMN_ALIGN_RIGHT, $grouptotals_real_oip);
	}

    $list_cgs->add_text_column("difference_in_cost", "Difference", COLUMN_ALIGN_RIGHT, $grouptotals_difference_in_cost);

    $list_cgs->add_text_column("difference_in_percent", "Percent", COLUMN_ALIGN_RIGHT, $grouptotals_difference_in_cost2);

    $list_cgs->set_footer("project_cost_groupname_name", "Total");
	$list_cgs->set_footer("investment_hq", number_format($project_cost_budget_hq, 2, ".", "'"));
    $list_cgs->set_footer("investment", number_format($project_cost_budget, 2, ".", "'"));
    $list_cgs->set_footer("real_cost", number_format($project_cost_real, 2, ".", "'"));
    $list_cgs->set_footer("difference_in_cost", number_format($project_cost_diff1, 2, ".", "'"));
    $list_cgs->set_footer("difference_in_percent", number_format(100*$project_cost_diff2, 2, ".", "'") . "%");


	
}

/********************************************************************
    Create List for Local Construction Cost
*********************************************************************/ 
$list_lc = new ListView($sql_list);

$list_lc->set_title("Local Construction");
$list_lc->set_entity("order_item");
$list_lc->set_filter($list_lc_filter);
$list_lc->set_order("order_item_supplier_freetext, order_item_id, item_shortcut");
//$list_lc->set_group("order_item_supplier_freetext", "order_item_supplier_freetext");

$list_lc->add_image_column("pix", "", 0, $images);

$list_lc->add_list_column("order_item_cost_group", "Cost Group", "select project_cost_groupname_id, project_cost_groupname_name from project_cost_groupnames where project_cost_groupname_active = 1 order by project_cost_groupname_sortorder", $flags = 0, $costgroups);

if($show_budget_in_loc == true)
{
	$list_lc->add_column("order_item_client_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_lc->add_edit_column("order_item_real_client_price", "Real Cost " . $order_currency["symbol"], "6", 0, $prices);
}
else
{
	$list_lc->add_column("order_item_system_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_lc->add_edit_column("order_item_real_system_price", "Real Cost " . $order_currency["symbol"], "6", 0, $prices);
}

$list_lc->add_column("order_item_supplier_freetext", "Supplier");

$list_lc->add_image_column("property", "", 0, $property);
$list_lc->add_column("order_item_text", "Name");

$list_lc->add_edit_column("order_item_cms_remark", "Remark", "60", 0, $cms_remarks);


$list_lc->add_button("save_cost", "Save", "");
if(has_access("can_edit_cost_monitoring_reinvoice_data"))
{
	$list_lc->add_button("reinvoice", "Reinvoice Data", "");
	$list_lc->add_button("group", "Group Data", "");
}
$list_lc->add_button("preview", "Preview CMS", "");


$list_lc->populate();
$list_lc->process();

if ($list_lc->button("save_cost"))
{
    project_update_order_item_cost2($list_lc, $order_currency2, $show_budget_in_loc);
    $link = "project_edit_cost_monitoring.php?pid=" . param("pid") . "&s=1"; 
    redirect($link);
}
elseif ($list_lc->button("reinvoice"))
{
    $link = "project_edit_cost_monitoring_reinvoice.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list_lc->button("group"))
{
    $link = "project_edit_cost_monitoring_group.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list_lc->button("preview"))
{
    $link = "project_edit_cost_monitoring_preview.php?pid=" . param("pid"); 
    redirect($link);
}



/********************************************************************
    Create List for HQ supplied Items
*********************************************************************/ 
$list_sf = new ListView($sql_list);

$list_sf->set_title("Fixturing: HQ Supplied Items");
$list_sf->set_entity("order_item");
$list_sf->set_filter($list_sf_filter);
$list_sf->set_order("order_item_cost_group, supplier_company, item_shortcut");
$list_sf->set_group("supplier_company", "supplier_company");


$list_sf->add_image_column("pix", "", 0, $images);

$list_sf->add_list_column("order_item_cost_group", "Cost Group", "select project_cost_groupname_id, project_cost_groupname_name from project_cost_groupnames where project_cost_groupname_active = 1 order by project_cost_groupname_sortorder", $flags = 0, $costgroups);

//$list_sf->add_list_column("order_items_costmonitoring_group", "Benchmark Item Group", "select costmonitoringgroup_id, costmonitoringgroup_text from costmonitoringgroups order by costmonitoringgroup_text", $flags = 0, $costmonitoringgroups);



$list_sf->add_column("order_item_quantity_freezed", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
if($show_budget_in_loc == true)
{
	if($project["order_date"] > "2005-10-31")
	{
		$list_sf->add_column("order_item_client_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		
	}
	else
	{
		$list_sf->add_edit_column("order_item_client_price_freezed", "Approved", "6", 0, $prices_freezed);
	}
}
else
{
	if($project["order_date"] > "2005-10-31")
	{
		$list_sf->add_column("order_item_system_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	}
	else
	{
		$list_sf->add_edit_column("order_item_system_price_freezed", "Approved", "6", 0, $prices_freezed);
	}
}

$list_sf->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);

$list_sf->add_edit_column("order_item_quantity", "Quantity", "4", 0, $quantities);

if($show_budget_in_loc == true)
{
	$list_sf->add_edit_column("order_item_real_client_price", "Real Cost " . $order_currency["symbol"], "6", 0, $prices);
}
else
{
	$list_sf->add_edit_column("order_item_real_system_price", "Real Cost " . $order_currency["symbol"], "6", 0, $prices);
}
$list_sf->add_image_column("property", "", 0, $property);
$list_sf->add_column("order_item_text", "Name", "", "", "");

$list_sf->add_edit_column("order_item_cms_remark", "Remark", "60", 0, $cms_remarks);

$list_sf->add_button("save_items", "Save", "");
if(has_access("can_edit_cost_monitoring_reinvoice_data"))
{
	$list_sf->add_button("reinvoice", "Reinvoice Data", "");
	$list_sf->add_button("group", "Group Data", "");
}

$list_sf->add_button("preview", "Preview CMS", "");

$list_sf->populate();
$list_sf->process();


if ($list_sf->button("save_items"))
{
	project_update_order_item_cost1($list_sf, true, $order_currency2, $show_budget_in_loc);
    $link = "project_edit_cost_monitoring.php?pid=" . param("pid") . "&s=1"; 
    redirect($link);
}
elseif ($list_sf->button("reinvoice"))
{
    $link = "project_edit_cost_monitoring_reinvoice.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list_sf->button("group"))
{
    $link = "project_edit_cost_monitoring_group.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list_sf->button("preview"))
{
    $link = "project_edit_cost_monitoring_preview.php?pid=" . param("pid"); 
    redirect($link);
}



/********************************************************************
    Create List for Freight Charges
*********************************************************************/ 
$list_fch = new ListView($sql_list);

$list_fch->set_title("Fixturing: Freight Charges");
$list_fch->set_entity("order_item");
$list_fch->set_filter($list_fch_filter);
$list_fch->set_order("order_item_supplier_freetext, order_item_id, item_shortcut");
$list_fch->set_group("supplier_company", "supplier_company");

$list_fch->add_image_column("pix", "", 0, $images);

$list_fch->add_list_column("order_item_cost_group", "Cost Group", "select project_cost_groupname_id, project_cost_groupname_name from project_cost_groupnames where project_cost_groupname_active = 1 order by project_cost_groupname_sortorder", $flags = 0, $costgroups);

if($show_budget_in_loc == true)
{
	$list_fch->add_column("order_item_client_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_fch->add_edit_column("order_item_real_client_price", "Real Cost " . $order_currency["symbol"], "6", 0, $prices);
}
else
{
	$list_fch->add_column("order_item_system_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_fch->add_edit_column("order_item_real_system_price", "Real Cost " . $order_currency["symbol"], "6", 0, $prices);
}


//$list_fch->add_edit_column("order_item_supplier_freetext", "Supplier Info", "30", 0, $suppliers);
$list_fch->add_column("order_item_supplier_freetext", "Supplier");
$list_fch->add_image_column("property", "", 0, $property);
$list_fch->add_column("order_item_text", "Name");
$list_fch->add_edit_column("order_item_cms_remark", "Remark", "60", 0, $cms_remarks);


$list_fch->add_button("save_cost", "Save", "");
if(has_access("can_edit_cost_monitoring_reinvoice_data"))
{
	$list_fch->add_button("reinvoice", "Reinvoice Data", "");
	$list_fch->add_button("group", "Group Data", "");
}
$list_fch->add_button("preview", "Preview CMS", "");


$list_fch->populate();
$list_fch->process();

if ($list_fch->button("save_cost"))
{
    project_update_order_item_cost2($list_fch, $order_currency2, $show_budget_in_loc);
    $link = "project_edit_cost_monitoring.php?pid=" . param("pid") . "&s=1"; 
    redirect($link);
}
elseif ($list_fch->button("reinvoice"))
{
    $link = "project_edit_cost_monitoring_reinvoice.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list_fch->button("group"))
{
    $link = "project_edit_cost_monitoring_group.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list_fch->button("preview"))
{
    $link = "project_edit_cost_monitoring_preview.php?pid=" . param("pid"); 
    redirect($link);
}


/********************************************************************
    Create List for Fixturing Other Cost
*********************************************************************/ 
$list_fixother = new ListView($sql_list);

$list_fixother->set_title("Fixturing: Other");
$list_fixother->set_entity("order_item");
$list_fixother->set_filter($list_fixother_filter);
$list_fixother->set_order("order_item_supplier_freetext, order_item_id, item_shortcut");
$list_fixother->set_group("supplier_company", "supplier_company");

$list_fixother->add_image_column("pix", "", 0, $images);

$list_fixother->add_list_column("order_item_cost_group", "Cost Group", "select project_cost_groupname_id, project_cost_groupname_name from project_cost_groupnames where project_cost_groupname_active = 1 order by project_cost_groupname_sortorder", $flags = 0, $costgroups);

if($show_budget_in_loc == true)
{
	$list_fixother->add_column("order_item_client_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_fixother->add_edit_column("order_item_real_client_price", "Real Cost " . $order_currency["symbol"], "6", 0, $prices);
}
else
{
	$list_fixother->add_column("order_item_system_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_fixother->add_edit_column("order_item_real_system_price", "Real Cost " . $order_currency["symbol"], "6", 0, $prices);
}

$list_fixother->add_column("order_item_supplier_freetext", "Supplier");
$list_fixother->add_image_column("property", "", 0, $property);
$list_fixother->add_column("order_item_text", "Name");
$list_fixother->add_edit_column("order_item_cms_remark", "Remark", "60", 0, $cms_remarks);


$list_fixother->add_button("save_cost", "Save", "");
if(has_access("can_edit_cost_monitoring_reinvoice_data"))
{
	$list_fixother->add_button("reinvoice", "Reinvoice Data", "");
	$list_fixother->add_button("group", "Group Data", "");
}
$list_fixother->add_button("preview", "Preview CMS", "");


$list_fixother->populate();
$list_fixother->process();

if ($list_fixother->button("save_cost"))
{
    project_update_order_item_cost2($list_fixother, $order_currency2, $show_budget_in_loc);
    $link = "project_edit_cost_monitoring.php?pid=" . param("pid") . "&s=1"; 
    redirect($link);
}
elseif ($list_fixother->button("reinvoice"))
{
    $link = "project_edit_cost_monitoring_reinvoice.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list_fixother->button("group"))
{
    $link = "project_edit_cost_monitoring_group.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list_fixother->button("preview"))
{
    $link = "project_edit_cost_monitoring_preview.php?pid=" . param("pid"); 
    redirect($link);
}


/********************************************************************
    Create List for Architectural Cost
*********************************************************************/ 
$list_arch = new ListView($sql_list);

$list_arch->set_title("Architectural Services");
$list_arch->set_entity("order_item");
$list_arch->set_filter($list_arch_filter);
$list_arch->set_order("order_item_supplier_freetext, order_item_id, item_shortcut");
$list_arch->set_group("supplier_company", "supplier_company");

$list_arch->add_image_column("pix", "", 0, $images);

$list_arch->add_list_column("order_item_cost_group", "Cost Group", "select project_cost_groupname_id, project_cost_groupname_name from project_cost_groupnames where project_cost_groupname_active = 1 order by project_cost_groupname_sortorder", $flags = 0, $costgroups);

if($show_budget_in_loc == true)
{
	$list_arch->add_column("order_item_client_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_arch->add_edit_column("order_item_real_client_price", "Real Cost " . $order_currency["symbol"], "6", 0, $prices);
}
else
{
	$list_arch->add_column("order_item_system_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_arch->add_edit_column("order_item_real_system_price", "Real Cost " . $order_currency["symbol"], "6", 0, $prices);
}

$list_arch->add_column("order_item_supplier_freetext", "Supplier");

$list_arch->add_image_column("property", "", 0, $property);

$list_arch->add_column("order_item_text", "Name");
$list_arch->add_edit_column("order_item_cms_remark", "Remark", "60", 0, $cms_remarks);

$list_arch->add_button("save_cost", "Save", "");
if(has_access("can_edit_cost_monitoring_reinvoice_data"))
{
	$list_arch->add_button("reinvoice", "Reinvoice Data", "");
	$list_arch->add_button("group", "Group Data", "");
}
$list_arch->add_button("preview", "Preview CMS", "");


$list_arch->populate();
$list_arch->process();

if ($list_arch->button("save_cost"))
{
    project_update_order_item_cost2($list_arch, $order_currency2, $show_budget_in_loc);
    $link = "project_edit_cost_monitoring.php?pid=" . param("pid") . "&s=1"; 
    redirect($link);
}
elseif ($list_arch->button("reinvoice"))
{
    $link = "project_edit_cost_monitoring_reinvoice.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list_arch->button("group"))
{
    $link = "project_edit_cost_monitoring_group.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list_arch->button("preview"))
{
    $link = "project_edit_cost_monitoring_preview.php?pid=" . param("pid"); 
    redirect($link);
}

/********************************************************************
    Create List for Equipment
*********************************************************************/ 
$list_eq = new ListView($sql_list);

$list_eq->set_title("Equipment");
$list_eq->set_entity("order_item");
$list_eq->set_filter($list_eq_filter);
$list_eq->set_order("order_item_supplier_freetext, order_item_id, item_shortcut");
$list_eq->set_group("supplier_company", "supplier_company");

$list_eq->add_image_column("pix", "", 0, $images);

$list_eq->add_list_column("order_item_cost_group", "Cost Group", "select project_cost_groupname_id, project_cost_groupname_name from project_cost_groupnames where project_cost_groupname_active = 1 order by project_cost_groupname_sortorder", $flags = 0, $costgroups);

if($show_budget_in_loc == true)
{
	$list_eq->add_column("order_item_client_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_eq->add_edit_column("order_item_real_client_price", "Real Cost " . $order_currency["symbol"], "6", 0, $prices);
}
else
{
	$list_eq->add_column("order_item_system_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_eq->add_edit_column("order_item_real_system_price", "Real Cost " . $order_currency["symbol"], "6", 0, $prices);
}
$list_eq->add_column("order_item_supplier_freetext", "Supplier");
$list_eq->add_image_column("property", "", 0, $property);

$list_eq->add_column("order_item_text", "Name");
$list_eq->add_edit_column("order_item_cms_remark", "Remark", "60", 0, $cms_remarks);

$list_eq->add_button("save_cost", "Save", "");
if(has_access("can_edit_cost_monitoring_reinvoice_data"))
{
	$list_eq->add_button("reinvoice", "Reinvoice Data", "");
	$list_eq->add_button("group", "Group Data", "");
}
$list_eq->add_button("preview", "Preview CMS", "");


$list_eq->populate();
$list_eq->process();

if ($list_eq->button("save_cost"))
{
    project_update_order_item_cost2($list_eq, $order_currency2, $show_budget_in_loc);
    $link = "project_edit_cost_monitoring.php?pid=" . param("pid") . "&s=1"; 
    redirect($link);
}
elseif ($list_eq->button("reinvoice"))
{
    $link = "project_edit_cost_monitoring_reinvoice.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list_eq->button("group"))
{
    $link = "project_edit_cost_monitoring_group.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list_eq->button("preview"))
{
    $link = "project_edit_cost_monitoring_preview.php?pid=" . param("pid"); 
    redirect($link);
}


/********************************************************************
    Create List for Costestimations
*********************************************************************/ 
$list_otc = new ListView($sql_list);

$list_otc->set_title("Other");
$list_otc->set_entity("order_item");
$list_otc->set_filter($list_otc_filter);
$list_otc->set_order("order_item_supplier_freetext, order_item_id, item_shortcut");
$list_otc->set_group("supplier_company", "supplier_company");

$list_otc->add_image_column("pix", "", 0, $images);

$list_otc->add_list_column("order_item_cost_group", "Cost Group", "select project_cost_groupname_id, project_cost_groupname_name from project_cost_groupnames where project_cost_groupname_active = 1 order by project_cost_groupname_sortorder", $flags = 0, $costgroups);

$list_otc->add_column("order_item_quantity_freezed", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
if($show_budget_in_loc == true)
{
	if($project["order_date"] > "2005-10-31")
	{
		$list_otc->add_column("order_item_client_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	}
	else
	{
		$list_otc->add_edit_column("order_item_client_price_freezed", "Approved", "6", 0, $prices_freezed);
	}
}
else
{
	if($project["order_date"] > "2005-10-31")
	{
		$list_otc->add_column("order_item_system_price_freezed", "Approved " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	}
	else
	{
		$list_otc->add_edit_column("order_item_system_price_freezed", "Approved", "6", 0, $prices_freezed);
	}
}

$list_otc->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);

$list_otc->add_edit_column("order_item_quantity", "Quantity", "4", 0, $quantities);
if($show_budget_in_loc == true)
{
	$list_otc->add_edit_column("order_item_real_client_price", "Real Cost " . $order_currency["symbol"], "6", 0, $prices);
}
else
{
	$list_otc->add_edit_column("order_item_real_system_price", "Real Cost " . $order_currency["symbol"], "6", 0, $prices);
	
}
$list_otc->add_column("order_item_supplier_freetext", "Supplier");
$list_otc->add_column("order_item_text", "Name", "", "", "", COLUMN_NO_WRAP);
$list_otc->add_edit_column("order_item_cms_remark", "Remark", "60", 0, $cms_remarks);


$list_otc->add_button("save_cost8", "Save", "");
if(has_access("can_edit_cost_monitoring_reinvoice_data"))
{
	$list_otc->add_button("reinvoice", "Reinvoice Data", "");
	$list_otc->add_button("group", "Group Data", "");
}
$list_otc->add_button("preview", "Preview CMS", "");


$list_otc->populate();
$list_otc->process();

if ($list_otc->button("save_cost8"))
{
	project_update_order_item_cost1($list_otc, false, $order_currency2, $show_budget_in_loc);
    $link = "project_edit_cost_monitoring.php?pid=" . param("pid") . "&s=1"; 
    redirect($link);
}
elseif ($list_otc->button("reinvoice"))
{
    $link = "project_edit_cost_monitoring_reinvoice.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list_otc->button("group"))
{
    $link = "project_edit_cost_monitoring_group.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list_otc->button("preview"))
{
    $link = "project_edit_cost_monitoring_preview.php?pid=" . param("pid"); 
    redirect($link);
}




/********************************************************************
    Create List for item in projects coming from catalogue orders
*********************************************************************/ 
$link = "/user/order_assign_items_to_a_project.php?oid={order_item_order}";

$list_oip = new ListView($sql_oip, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_oip->set_title("Catalogue Orders");
$list_oip->set_entity("order_items_in_projects");

$list_oip->add_column("address_shortcut", "Supplier", "", "", "", COLUMN_NO_WRAP);
$list_oip->add_column("project_cost_groupname_name", "Cost Group", "", "", "", COLUMN_NO_WRAP);
$list_oip->add_text_column("replacement", "", COLUMN_NO_WRAP | COLUMN_UNDERSTAND_HTML, $replacementinfos);
$list_oip->add_column("order_item_text", "Description", $link, "", "");


$list_oip->add_column("date1", "Ordered");
$list_oip->add_column("date2", "Pickup");
$list_oip->add_column("date3", "Arrival");
$list_oip->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);

$list_oip->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

if($show_budget_in_loc == true)
{
	$list_oip->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list_oip->add_edit_column("order_items_in_project_real_client_price", "Real Price " . $order_currency["symbol"], "6", 0, $order_items_in_project_real_prices);
}
else
{
	$list_oip->add_column("order_item_system_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list_oip->add_edit_column("order_items_in_project_real_price", "Real Price " . $order_currency["symbol"], "6", 0, $order_items_in_project_real_prices);
}

$list_oip->add_edit_column("order_items_in_project_cms_remark", "Remark", "60", 0, $oip_cms_remarks);


$list_oip->add_button("save_oip", "Save", "");
if(has_access("can_edit_cost_monitoring_reinvoice_data"))
{
	$list_oip->add_button("reinvoice", "Reinvoice Data", "");
}

$list_oip->populate();
$list_oip->process();


if ($list_oip->button("save_oip"))
{
	project_update_order_item_cost3($list_oip, $order_currency2, $show_budget_in_loc);
    $link = "project_edit_cost_monitoring.php?pid=" . param("pid") . "&s=1"; 
    redirect($link);
}
elseif ($list_oip->button("reinvoice"))
{
    $link = "project_edit_cost_monitoring_reinvoice.php?pid=" . param("pid"); 
    redirect($link);
}




/********************************************************************
    Create List for remarks
*********************************************************************/ 
//get remarks for cost monitoring sheet
$sql_remarks = "select project_cost_remark_id, 'edit' as edit, project_cost_remark_remark " .
               "from project_cost_remarks";

$list3_filter = "project_cost_remark_order = " . $project["project_order"];


$list3 = new ListView($sql_remarks, LIST_HAS_HEADER | LIST_HAS_FOOTER);
$list3->set_title("General Remarks");
$list3->set_entity("project_cost_remarks");
$list3->set_filter($list3_filter);

$link = "project_edit_cost_monitoring_remark.php?pid=" . param("pid"); 
$list3->add_column("edit", "  ", $link);
$list3->add_column("project_cost_remark_remark", "Remark");

$list3->add_button("new_remark", "Add New General Remark", "project_cost_remark.php");

$list3->process();


if ($list3->button("new_remark"))
{
    $link = "project_edit_cost_monitoring_remark.php?pid=" . param("pid"); 
    redirect($link);
}




/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Cost Monitoring Sheet");
$form->render();



if($projectctype == 1) // Corporate
{
    echo "<br>";
    $list_cgs->render();
}


if(has_access("can_edit_cost_monitoring_lc") and $num_recs7 > 0)
{
    echo "<br>";
    $list_lc->render();
}

if(has_access("can_edit_cost_monitoring_sf") and $num_recs2 > 0)
{
    echo "<br>";
    $list_sf->render();
}

if(has_access("can_edit_cost_monitoring_fch") and $num_recs6 > 0)
{
    echo "<br>";
    $list_fch->render();
}

if(has_access("can_edit_cost_monitoring_fixother") and $num_recs10 > 0)
{
    echo "<br>";
    $list_fixother->render();
}

if(has_access("can_edit_cost_monitoring_arch") and $num_recs9 > 0)
{
    echo "<br>";
    $list_arch->render();
}

if(has_access("can_edit_cost_monitoring_eq") and $num_recs11 > 0)
{
    echo "<br>";
    $list_eq->render();
}

if(has_access("can_edit_cost_monitoring_otc") and $num_recs8 > 0)
{
    echo "<br>";
    $list_otc->render();
}


if(has_access("can_edit_cost_monitoring_oip") and $has_catalogue_orders == true)
{
	echo "<br>";
	$list_oip->render();
}

if(has_access("can_edit_cost_monitoring_remarks"))
{
	echo "<br>";
	$list3->render();
}


?>
<div id="changehistory" style="display:none;">
    <strong>Changes of the due date</strong>
	<table class="table_tracking">
	<tr>
	<td class="label">User</td>
	<td class="label">Time</td>
	<td class="label">Old Value</td>
	<td class="label">New Value</td>
	<td class="label">Comment</td>
	</tr>

	<?php
		foreach($tracking_info as $key=>$values)
		{
			echo '<tr class="tr_tracking"><td class="td_tracking_nobr">' . $values['user_name'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_time'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_oldvalue'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_newvalue'] . '</td>';
			echo '<td class="td_tracking">' . $values['projecttracking_comment'] . '</td></tr>';
		}
	?>
	
	</table>
</div> 


<?php

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>