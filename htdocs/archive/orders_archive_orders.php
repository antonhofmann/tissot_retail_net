<?php
/********************************************************************

    orders_archive_orders.php

    Entry page for the orders section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-11-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-03
    Version:        1.0.5

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/access_filters.php";

check_access("has_access_to_archive");

register_param("y1");
register_param("y2");
register_param("c");
register_param("on");


/********************************************************************
    prepare all data needed
*********************************************************************/
if (!param("y1"))
{
    $y1 = 0;
}
else
{
	$y1 = param("y1");
}
if (!param("y2"))
{
    $y2 = 0;
}
else
{
	$y2 = param("y2");
}

if (!param("c"))
{
    $c = 0;
}
else
{
	$c = param("c");
}

if (!param("on"))
{
    $on = 0;
}
else
{
	$on = param("on");
}


//get regional access
$country_filter = "";
$tmp = array();
$sql = "select * from country_access " .
	   "where country_access_user = " . user_id();


$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{            
	$tmp[] = $row["country_access_country"];
}

if(count($tmp) > 0) {
	$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
}

$country_access_filter = get_users_regional_access_to_countries(2, user_id());
if($country_access_filter)
{
	if($country_filter)
	{
		$country_filter = "(" . $country_filter .  " or " . $country_access_filter . ")";
	}
	else
	{
		$country_filter = "(" . $country_access_filter . ")";
	}
}


// get user_data
$user_data = get_user(user_id());

// create list filter
$condition = get_user_specific_order_list(user_id(), 2);

// create filter
$filter = "";


if($y1 > 0)
{
	$filter =  "   and left(order_date,4) >= " . $y1;
}
if($y2 > 0)
{
	$filter .=   "   and left(order_date,4) <= " . $y2;
}
if($c > 0)
{
	$filter .=   "   and country_id = " . $c;
}
if($on != '')
{
	$filter .=   "   and order_number = " . dbquote($on);
}


$sql = "select distinct order_id, order_number, " . 
       "   order_actual_order_state_code, order_number, left(orders.date_created, 10), " .
       "   concat(user_name,' ',user_firstname), " . 
       "   country_name, ".
	   "   posaddress_name, place_name " . 
       "from orders " .
	   "left join addresses on address_id = order_client_address " . 
       "left join countries on address_country = countries.country_id ".
       "left join users on order_retail_operator = users.user_id " . 
	   "left join posorders on posorder_order = order_id " . 
	   "left join posaddresses on posaddress_id = posorder_posaddress " . 
	   "left join places on place_id = posaddress_place_id";



$list_filter = "";
if (has_access("has_access_to_all_orders"))
{
    $list_filter = "order_archive_date is not null ".
                   "   and order_archive_date <> '0000-00-00' ".
                   $filter .
                   "   and order_type = 2";
}
elseif($country_filter != '')
{
	
	if($filter)
	{
		$filter = $filter . " and " . $country_filter;
	}
	else
	{
		$filter = $country_filter;
	}

	$list_filter = "order_archive_date is not null ".
                   "   and order_archive_date <> '0000-00-00' ".
                   $filter .
                   "   and order_type = 2";

}
elseif(has_access("has_access_to_all_orders_of_his_country"))
{
	if ($condition == "")
    {
		if (has_access("can_view_order_before_budget_approval_in_orders"))
		{   
			if (has_access("can_view_empty_orders"))
			{ 
				$list_filter = "order_archive_date is not null ".
							   "   and order_archive_date <> '0000-00-00' " .
							   " and country_id = " . $user_data["country"] . 
							   $filter .
							   "   and  order_type = 2";
			}
			else
			{
				$list_filter = " order_archive_date is not null ".
							   "   and order_archive_date <> '0000-00-00' ".
							   " and country_id = " . $user_data["country"] . 
							   $filter .
							   "   and  order_type = 2";
			}
		}
		else
		{   
			 if (has_access("can_view_empty_orders"))
			 {
				 $list_filter = "  order_show_in_delivery = 1 ".
								"   and order_archive_date is not null ".
								"   and order_archive_date <> '0000-00-00' ".
								" and country_id = " . $user_data["country"] . 
								$filter .
								"   and  order_type = 2";
			 }
			 else
			 {
				 $list_filter = "  order_show_in_delivery = 1 ".
								"   and order_archive_date is not null ".
								"   and order_archive_date <> '0000-00-00' " .
								" and country_id = " . $user_data["country"] . 
								$filter .
								"   and  order_type = 2";
			 }
		}
	}
	else
	{
		if (has_access("can_view_order_before_budget_approval_in_orders"))
		{   
			if (has_access("can_view_empty_orders"))
			{ 
				$list_filter = " order_archive_date is not null ".
							   "   and order_archive_date <> '0000-00-00' " .
							   "   and (" . $condition . ")" .
							   $filter .
							   "   and  order_type = 2";
			}
			else
			{
				$list_filter = " order_archive_date is not null ".
							   "   and order_archive_date <> '0000-00-00' ".
							   "   and (" . $condition . ")" .
							   $filter .
							   "   and  order_type = 2";
			}
		}
		else
		{   
			 if (has_access("can_view_empty_orders"))
			 {
				 $list_filter = "  order_show_in_delivery = 1 ".
								"   and order_archive_date is not null ".
								"   and order_archive_date <> '0000-00-00' ".
								"   and (" . $condition . ")" .
								$filter .
								"   and  order_type = 2";
			 }
			 else
			 {
				 $list_filter = "  order_show_in_delivery = 1 ".
								"   and order_archive_date is not null ".
								"   and order_archive_date <> '0000-00-00' " .
								"   and (" . $condition . ")" .
								$filter .
								"   and  order_type = 2";
			 }
		}

		
	}
}
else
{
    if ($condition == "")
    {
          
		if (has_access("can_view_empty_orders"))
		{ 
			$list_filter = " order_archive_date is not null ".
						   "   and order_archive_date <> '0000-00-00' ".
						   $filter .
						   "   and  order_type = 2 ".
						   "   and (order_item_supplier_address = " . $user_data["address"] . " " .
						   "   or order_item_forwarder_address = " . $user_data["address"] . " " .
						   "   or order_retail_operator = " . user_id() . " " .
						   "   or order_client_address = " . $user_data["address"] . ")";   
		}
		else
		{
			$list_filter = " order_archive_date is not null ".
						   "   and order_archive_date <> '0000-00-00' ".
						   $filter .
						   "   and  order_type = 2 ".
						   "   and (order_item_supplier_address = " . $user_data["address"] . " " .
						   "   or order_item_forwarder_address = " . $user_data["address"] . " " .
						   "   or order_retail_operator = " . user_id() . " " .
						   "   or order_client_address = " . $user_data["address"] . ")";   
		}
    }
    else
    {
           
		if (has_access("can_view_empty_orders"))
		{ 
		$list_filter = " order_archive_date is not null ".
				   "   and order_archive_date <> '0000-00-00' ".
				   $filter .
				   "   and  order_type = 2 " .
				   "   and (" . $condition . ")";
		}
		else
		{
		$list_filter = " order_archive_date is not null ".
				   "   and order_archive_date <> '0000-00-00' ".
				   $filter .
				   "   and  order_type = 2 " .
				   "   and (" . $condition . ")";
		}
    }
}


if($user_data["user_can_only_see_orders_of_his_address"] == 1)
{
	$list_filter.= " and order_client_address = " . dbquote($user_data["address"]);
}

//count orders

$sql_count = "select count(distinct order_id) as num_recs ".
			   "from orders " .
			   "left join order_items on order_item_order = order_id " .
			   "left join countries on order_shop_address_country = countries.country_id ".
			   "left join users on order_retail_operator = users.user_id ";

$sql_count = $sql_count . " where " . $list_filter;
$res = mysql_query($sql_count);
$row = mysql_fetch_assoc($res);



if($row["num_recs"] == 1) {
	
	
	$sql_count = "select distinct order_id ".
           "from orders " .
           "left join countries on order_shop_address_country = countries.country_id ".
		    "left join order_items on order_item_order = order_id ".
           "left join users on order_retail_operator = users.user_id ";

	$sql_count = $sql_count . " where " . $list_filter;
	$res = mysql_query($sql_count);
	$row = mysql_fetch_assoc($res);


	$link = "order_task_center.php?oid=" . $row["order_id"]. "&y1=" .param("y1") . "&y2=" . param("y2") . "&c=" . param("c") . "&on=" . param("on");
	redirect($link);
}



$poslocations = array();
$poslocations = array();

$sql_tmp = $sql . " where " . $list_filter;
$res = mysql_query($sql_tmp);

while($row = mysql_fetch_assoc($res))
{
	if($row["posaddress_name"])
	{
		$poslocations[$row["order_id"]] = $row["place_name"] . ", " . $row["posaddress_name"];
	}
	elseif(substr($row["order_number"], 0, 1) == 'R') // replacement order
	{
		
		
		
		$sql_r = "select distinct order_id, order_number, " . 
				   "   order_actual_order_state_code, order_number, left(orders.date_created, 10), " .
				   "   concat(user_name,' ',user_firstname), " . 
				   "   country_name, order_shop_address_place, order_shop_address_company, ".
				   "   posaddress_name, place_name " . 
				   "from order_item_replacements " .
				   " left join orders on order_id = order_item_replacement_order_id " .
				   "left join addresses on address_id = order_client_address " . 
				   "left join countries on address_country = countries.country_id ".
				   "left join users on order_retail_operator = users.user_id " . 
				   "left join posorders on posorder_order = order_id " . 
				   "left join posaddresses on posaddress_id = posorder_posaddress " . 
				   "left join places on place_id = posaddress_place_id " . 
				   " where order_item_replacement_catalog_order_id = " . dbquote($row["order_id"]);

		$res_r = mysql_query($sql_r) or dberror($sql_r);

		if ($row_r = mysql_fetch_assoc($res_r))
		{
			$poslocations[$row["order_id"]] = $row_r["order_shop_address_place"] . ", " . $row_r["order_shop_address_company"];
		}
	}
	
}


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("orders in the records");
$list->set_order("orders.date_created desc");
$list->set_filter($list_filter);

$list->add_hidden("y1", param("y1"));
$list->add_hidden("y2", param("y2"));
$list->add_hidden("c", param("c"));
$list->add_hidden("on", param("on"));

$link = "order_task_center.php?oid={order_id}&y1=" .param("y1") . "&y2=" . param("y2") . "&c=" . param("c") . "&on=" . param("on");
$list->add_column("order_number", "Order Number", $link, "", "", COLUMN_NO_WRAP);
$list->add_column("order_actual_order_state_code", "Status");
$list->add_column("left(orders.date_created, 10)", "Submitted", "", "", "", COLUMN_NO_WRAP);
$list->add_column("country_name", "Country");
$list->add_text_column("poslocation", "POS Location", 0, $poslocations);
$list->add_column("concat(user_name,' ',user_firstname)", "Logistics Coordinator");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->process();


$page = new Page("orders");

$page->register_action('orders', 'Orders', "orders_archive.php");
$page->register_action('projects', 'Projects', "projects_archive.php");
$page->register_action('welcome', 'Quit Archive', "../user/welcome.php");

$page->header();
$page->title('Orders: Archive');
$list->render();
$page->footer();

?>