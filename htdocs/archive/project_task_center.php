<?php
/********************************************************************

    project_task_center.php

    List Flow Hitory and Actions

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-11-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-11-23
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

require_once "include/get_functions.php";
//require_once "include/order_functions.php";
check_access("can_use_taskcentre_in_projects");

register_param("pid");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read order details
$project = get_project(param("pid"));
$order_id = $project["project_order"];

$order_state_code = $project["order_actual_order_state_code"];
$last_order_state_code = get_code_of_last_order_state_performed($order_id);
$last_order_state = get_last_order_state_performed($order_id);


//check if project step 423 (former 430) was performed in this project
// in March 2015 we eliminated former step 430, but is must be shown in old projects
$show_former_step_430 = false;
$sql = "select count(actual_order_state_id) as num_recs from actual_order_states " . 
       " where actual_order_state_order = " . dbquote($order_id) . 
	   " and actual_order_state_state = 21";

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	if($row['num_recs'] > 0)
	{
		$show_former_step_430 = true;
	}
}


// get company's address
$client_address = get_address($project["order_client_address"]);


//get user roles
$user_roles = get_user_roles(user_id());
$user = get_user(user_id());
$user_name = $user["firstname"] . " " . $user["name"];


if(in_array(1, $user_roles) or has_access("can_view_all_steps_in_the_task_center")) // Administrator
{
    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' ";

	if($show_former_step_430 == false)
	{
		$filter_hi .= " and order_state_not_used_anymore = 0 ";
	}
}
elseif(in_array(3, $user_roles)) // retrail operator
{
    
    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' ";

	if($show_former_step_430 == false)
	{
		$filter_hi .= " and order_state_not_used_anymore = 0 ";
	}

}
elseif(in_array(2, $user_roles)) // retrail operator
{
   
    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' ";

	if($show_former_step_430 == false)
	{
		$filter_hi .= " and order_state_not_used_anymore = 0 ";
	}

}
elseif(in_array(4, $user_roles)) // client
{
    
    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' " .
                 "and (order_state_performer = 4 " .
                 "or notification_recipient_role = 4) ";

	if($show_former_step_430 == false)
	{
		$filter_hi .= " and order_state_not_used_anymore = 0 ";
	}
}
elseif(in_array(5, $user_roles) or in_array(29, $user_roles)) // supplier, warehouse
{
    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' " .
                 "and (tousers.user_id = " . user_id() .
                 "  or users.user_id = " . user_id() . ")";

	if($show_former_step_430 == false)
	{
		$filter_hi .= " and order_state_not_used_anymore = 0 ";
	}
}
elseif(in_array(6, $user_roles)) // forwarder
{

    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' " .
                 "and (tousers.user_id = " . user_id() .
                 "  or users.user_id = " . user_id() . ")";

	if($show_former_step_430 == false)
	{
		$filter_hi .= " and order_state_not_used_anymore = 0 ";
	}
}
elseif(in_array(7, $user_roles)) // design contractor
{
    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' " .
                 "and (order_state_performer = 7 " .
                 "or notification_recipient_role = 7) ";

	if($show_former_step_430 == false)
	{
		$filter_hi .= " and order_state_not_used_anymore = 0 ";
	}
}
elseif(in_array(8, $user_roles)) // Design Supervisor
{
    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' " .
                 "and (order_state_performer = 8 " .
                 "or notification_recipient_role = 8) ";

	if($show_former_step_430 == false)
	{
		$filter_hi .= " and order_state_not_used_anymore = 0 ";
	}
}
else // all others
{

    //history
    $sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
              "order_state_group_code, order_state_group_name, " .
              "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
              "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
              "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
              "from order_state_groups " .
              "left join order_states on order_state_group = order_state_group_id " . "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
              "left join users on users.user_id = actual_order_state_user " .
              "left join order_mails on order_mail_order = " . $order_id . " " .
              "and order_mail_order_state = order_state_id " .
              "left join users as tousers on tousers.user_id = order_mail_user " .
              "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

    $filter_hi = "order_state_group_order_type = 1 " .
                 "and users.user_name <> '' ";

	if($show_former_step_430 == false)
	{
		$filter_hi .= " and order_state_not_used_anymore = 0 ";
	}

}



/********************************************************************
   get dates of performance ot each step
*********************************************************************/
$os = "";
$dc = 1;
$dates_performed = array();
$performed_by = array();

$sql = "select order_state_code," .
       "actual_order_states.date_created as state_date, " .
       "    concat(user_name, ' ', left(user_firstname,1), '.') as user_fullname " .
       "from actual_order_states " .
       "left join order_states on order_state_id = actual_order_state_state " .
       "left join users on actual_order_state_user = user_id ".
       "where actual_order_state_order = " . $order_id . 
       " order by order_state_code, actual_order_states.date_created ASC";


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    if($os != $row["order_state_code"])
    {
        $os = $row["order_state_code"];
        $dc = 1;
    }

    $dates_performed[$row["order_state_code"]] = "[" . $dc . "]" . to_system_date($row["state_date"]);
    $dc++;
    $performed_by[$row["order_state_code"]] = $row["user_fullname"];
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "task_center", 640);
$form->add_hidden("pid", param("pid"));

if(!$project["project_retail_coordinator"])
{
    //$form->error("No Logistics Coordinator has been assigned!");
}



require_once "include/project_head_small.php";

if(has_access("can_activate_projects"))
{
	$form->add_button("activate", "Activate Project again");
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("activate"))
{
	//project tracking

	$sql = "Insert into projecttracking (" . 
		   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
		   user_id() . ", " . 
		   $project["project_id"] . ", " . 
		   dbquote("order_actual_order_state_code") . ", " . 
		   dbquote('archived') . ", " . 
		   dbquote('ongoing - 100') . ", " . 
		   dbquote('project was reactivated') . ", " . 
		   dbquote(date("Y-m-d:H:i:s")) . ")"; 
		   
	$result = mysql_query($sql) or dberror($sql);
	
	
	// get latest order_state before pur to archive
	$sql = "select order_state_code " . 
		   "from actual_order_states " . 
		   "left join order_states on order_state_id = actual_order_state_state " . 
		   " where actual_order_state_order = " . $project["project_order"] . 
		   " order by actual_order_state_id DESC";

	$last_order_state = "";
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if($row["order_state_code"] != $project["order_actual_order_state_code"])
		{
			$last_order_state = $row["order_state_code"] ;
		}
	}
	
	if($last_order_state == "" or $last_order_state == NULL)
	{
		$sql = "update orders set order_archive_date = NULL  where order_id = " . $project["project_order"];
	}
	else
	{
		$sql = "update orders set order_archive_date = NULL, order_actual_order_state_code=" . dbquote($last_order_state) . " where order_id = " . $project["project_order"];
	}
	$result = mysql_query($sql) or dberror($sql);
	$form->message("The project has been activated.");
}


//actual_step
$icons = array();

if($last_order_state_code < $project["order_actual_order_state_code"])
{
    $icons[$last_order_state_code] = "/pictures/wf_right_red.gif";
}
else
{
    $icons[$project["order_actual_order_state_code"]] = "/pictures/wf_right_green.gif";
}

/********************************************************************
    Create History
*********************************************************************/ 
$history = new ListView($sql_hi, LIST_HAS_HEADER);

$history->set_title("History");
$history->set_entity("order_state");
$history->set_filter($filter_hi);
$history->set_order("order_state_code, actual_order_states.date_created ASC");
$history->set_group("order_state_group_code", "order_state_group_name");

$history->add_column("order_state_code", "Step");
$history->add_column("order_state_action_name", "Action");

$history->add_column("fullname", "Performed by");
$history->add_column("performed", "Date");
$history->add_column("tofullname", "Recipient");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$history->populate();



/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Task Center");
$form->render();

$history->render();

$page->footer();

?>