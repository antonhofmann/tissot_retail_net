<?php
/********************************************************************

    order_shipping_volumes.php

    List shipping details for each forwarder

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2015-01-20
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-20
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";


check_access("has_access_to_shipping_details_in_orders");

register_param("oid");
set_referer("order_shipping_volumes_edit.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

// read order and order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);


// create sql for order items
$sql_order_items = "select order_item_id, order_item_text, order_item_packaging_number, addresses.address_company as supplier_company, ".
                   "    if(order_item_item <>'', item_code, item_type_name) as item_code, " .
                   "    concat(if(order_item_item <>'', item_code, item_type_name), ': ', if(addresses.address_company is not null, addresses.address_company, '!! no supplier assigned !!'), ' / ', if(addresses1.address_company is not null, addresses1.address_company, '!! no forwarder assigned !!')) as group_head, " .
				   " ROUND(order_item_quantity*order_item_packaging_number*order_item_packaging_weight_gross,2) as gross_weight, " .
				   " concat(ROUND(order_item_packaging_length), 'x', ROUND(order_item_packaging_width), 'x', ROUND(order_item_packaging_height)) as lxwxh, " .
				   " ROUND((order_item_quantity*order_item_packaging_number*(order_item_packaging_length*order_item_packaging_width*order_item_packaging_height/1000000)), 2) as cbm, " .
				   " unit_name, packaging_type_name, " .
				   " if(order_item_stackable = 1, 'x', '') as stackable " .
                   "from order_items ".
				   "left join order_item_packaging on order_item_packaging_order_item_id = order_item_id " . 
                   "left join items on order_item_item = item_id ".
                   "left join item_types on order_item_type = item_type_id ".
                   "left join addresses on addresses.address_id = order_item_supplier_address " . 
				   "left join addresses as addresses1 on addresses1.address_id = order_item_forwarder_address " . 
				   "left join units on unit_id = order_item_packaging_unit_id " . 
				   "left join packaging_types on packaging_type_id = order_item_packaging_packaging_id" ;


if (has_access("has_access_to_all_traffic_data_in_orders"))
{
    $list_filter = " (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
		              "   and order_item_quantity > 0 " .
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $order["order_id"];

}
elseif(in_array(6, $user_roles)) // forwarder
{
    $list_filter = " (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
		              "   and order_item_quantity > 0 " .
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $order["order_id"] .
                      "   and order_item_forwarder_address = " . $user_data["address"];
}
elseif(in_array(5, $user_roles)) // supplier
{
    $list_filter = " (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
		              "   and order_item_quantity > 0 " .
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $order["order_id"] .
                      "   and order_item_supplier_address = " . $user_data["address"];
}
else
{
    $list_filter = " (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
		              "   and order_item_quantity > 0 " .
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $order["order_id"] .
                      "   and order_item_forwarder_address = 0";
}


//get list group totals
$total_cbm = array();
$total_weight = array();


$sql_u = $sql_order_items . " where " . $list_filter . " order by group_head";
$res = mysql_query($sql_u) or dberror($sql_u);
while ($row = mysql_fetch_assoc($res))
{
	if(array_key_exists($row["group_head"], $total_cbm))
	{
		$total_cbm[$row["group_head"]] = $total_cbm[$row["group_head"]] + $row["cbm"];
		$total_weight[$row["group_head"]] = $total_weight[$row["group_head"]] + $row["gross_weight"];
	}
	else
	{
		$total_cbm[$row["group_head"]] = $row["cbm"];
		$total_weight[$row["group_head"]] = $row["gross_weight"];
	}

}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "orders");

$form->add_hidden("oid", param("oid"));

require_once "include/order_head_small.php";


$link = "order_shipping_weight_and_volumes_pdf.php?oid=" . param("oid"); 
$link = "javascript:popup('". $link . "', 800, 600)";
$form->add_button("pdf", "Print PDF", $link);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Create Item List
*********************************************************************/ 
$list = new ListView($sql_order_items, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);

$list->set_title("Items up to Delivery");
$list->set_entity("order_item");
$list->set_group("group_head");
$list->set_filter($list_filter);
$list->set_order("order_item_type, supplier_company, item_code");


$list->add_column("order_item_packaging_number", "Quantity", "", "", "", COLUMN_UNDERSTAND_HTML | COLUMN_ALIGN_LEFT | COLUMN_BREAK);
$list->add_column("unit_name", "Unit", "", "", "", COLUMN_BREAK);
$list->add_column("packaging_type_name", "Packaging", "", "", "", COLUMN_BREAK);
$list->add_column("gross_weight", "Gross Weight<br />in kg", "", "", "", COLUMN_UNDERSTAND_HTML | COLUMN_ALIGN_RIGHT | COLUMN_BREAK);
$list->add_column("lxwxh", "LxWxH", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_BREAK);
$list->add_column("cbm", "CBM", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_BREAK);

//$list->add_column("stackable", "Stackable", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_CENTER);

// set group totals

foreach ($total_weight as $key=>$value)
{
	$list->set_group_footer("gross_weight", $key , number_format($value,2, ".", "'"));
}


foreach ($total_cbm as $key=>$value)
{
    $list->set_group_footer("cbm", $key , number_format($value,2, ".", "'"));
}



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Shipping Details - Weight and Volumes");


require_once("include/tabs_shipping_details.php");



$form->render();
$list->render();

$page->footer();

?>