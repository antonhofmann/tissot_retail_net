<?php
/********************************************************************

    project_edit_retail_data.php

    Edit Retail Assignements.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-08
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-16
    Version:        1.0.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_state_constants.php";

check_access("can_edit_retail_data");

register_param("pid");
set_referer("project_confirm_to_client.php");


/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));
$old_project_number = $project["project_number"];
$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);
$old_retail_coordinator = $project["project_retail_coordinator"];
$old_retail_operator = $project["order_retail_operator"];


$tracking_info = array();
$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
	   "concat(user_name, ' ', user_firstname) as user_name " . 
	   "from projecttracking " . 
       "left join users on user_id = projecttracking_user_id " . 
       "where projecttracking_project_id = " . param("pid") . 
	   " and projecttracking_field = 'project_real_opening_date' " . 
	   " order by projecttracking_time";


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$tracking_info[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
		"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
		"projecttracking_comment"=>$row["projecttracking_comment"],
		"projecttracking_time"=>$row["projecttracking_time"],
		"user_name"=>$row["user_name"]
		);
}

$tracking_info2 = array();
$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
	   "concat(user_name, ' ', user_firstname) as user_name " . 
	   "from projecttracking " . 
       "left join users on user_id = projecttracking_user_id " . 
       "where projecttracking_project_id = " . param("pid") . 
	   " and projecttracking_field = 'project_state' " . 
	   " order by projecttracking_time";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$tracking_info2[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
		"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
		"projecttracking_comment"=>$row["projecttracking_comment"],
		"projecttracking_time"=>$row["projecttracking_time"],
		"user_name"=>$row["user_name"]
		);
}

$old_product_line = $project["project_product_line"];
$old_shop_real_opening_date = $project["project_real_opening_date"];
$old_project_state = $project["project_state"];

// get Action parameter
$action_parameter_rto = get_action_parameter(RETAIL_OPERATOR_ASSIGNED, 1);
$action_parameter_rtc = get_action_parameter(RETAIL_COORDINATOR_ASSIGNED, 1);

// get company's address
$client_address = get_address($project["order_client_address"]);

// create sql for the retail_coordinator listbox
$sql_retail_coordinators = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as user_fullname ".
                           "from users ".
                           "left join user_roles on user_id = user_role_user ".
                           "where ((user_role_role = 3 or user_role_role = 80) and user_active = 1) " .
						   " or user_id =  " . dbquote($project["project_retail_coordinator"]) .  
                           "order by user_name, user_firstname";


// create sql for the local retail_coordinator listbox
$sql_local_retail_coordinators = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as user_fullname ".
                           "from users ".
                           "left join user_roles on user_id = user_role_user ".
                           "where (user_address = " . $project["order_client_address"] .
						   " and (user_role_role = 15 or user_role_role = 4 or user_role_role = 16 or user_role_role = 10) " . 
						   " and user_active = 1) " .
						   " or user_id =  " . dbquote($project["project_local_retail_coordinator"]) .
                           "order by user_name, user_firstname";

// create sql for the retail_operator listbox
$sql_retail_operators = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as user_fullname ".
                        "from users ".
                        "left join user_roles on user_id = user_role_user ".
                        "where (user_role_role = 2 and user_active = 1) " .
						" or user_id =  " . dbquote($project["order_retail_operator"]) .
						"order by user_name, user_firstname";

// create sql for the design contractor listbox
$sql_contractors = "select DISTINCT user_id, ".
                   "    concat(address_company, ', ', user_name, ' ', user_firstname) as user_fullname ".
                   "from addresses ".
                   "left join users on user_address = address_id ".
                   "where (address_type = 5 ".
                   "    and address_active = 1 ".
                   "    and user_active = 1) or (user_id =  " . dbquote($project["project_design_contractor"]) . ") " . 
                   "order by address_company, user_name, user_firstname";

// create sql for the design supervisor listbox
$sql_supervisors = "select DISTINCT user_id, ".
                   "    concat(user_name, ' ', user_firstname) as user_fullname ".
                   "from users ".
                   "left join user_roles on user_role_user = user_id ".
                   "where (user_role_role = 8 " .
                   "    and user_active = 1) or (user_id =  " . dbquote($project["project_design_supervisor"]) . ") " .
                   "order by user_name";


// create sql for the client's contact listbox
$sql_address_user = "select DISTINCT user_id, concat(address_company, ', ', user_name, ' ', user_firstname) ".
                    "from users ".
                    "left join addresses on address_id = " . $project["order_client_address"] . " " .
                    "where (user_active = 1 and user_address = ". $project["order_client_address"] . ") ".
					" or (user_id =  " . dbquote($project["order_delivery_confirmation_by"]) . ") " .
                    "order by address_company, user_name";

//get RRMA: HQ Project Leaders
$sql_rrmas = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username " . 
             "from user_roles " .
	         "left join users on user_id = user_role_user " . 
	         "where user_role_role = 19 " .
			 "order by user_name, user_firstname";


// create sql for the cms approvers listbox
$sql_cms_approvers = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as user_fullname ".
					 "from users ".
					 "left join user_roles on user_id = user_role_user ".
					 "where user_role_role in (3, 8, 10) and (user_active = 1 " . 
					 " or user_id =  " . dbquote($project["project_cms_approver"]) . ") " .
					 "order by user_name, user_firstname";



// create sql for the product line listbox
if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
{
	$sql_product_line = "select product_line_id, product_line_name ".
						"from product_lines ".
						"where ( product_line_budget = 1) ".
		                " or product_line_id = " . dbquote($project["project_product_line"]) . 
						" order by product_line_name";
}
else
{
	$sql_product_line = "select DISTINCT product_line_id, product_line_name ".
						"from product_lines ".
		                "left join productline_regions on productline_region_productline = product_line_id " .
						"where (product_line_budget = 1 ".
		                " and productline_region_region = " . $client_address["country_region"] . ") " .
		                " or product_line_id = " . dbquote($project["project_product_line"]) . 
						" order by product_line_name";
}


//create sql for product line subclasses
$num_or_product_line_subclasses = 0;
if(param("product_line"))
{
	$sql_product_line_sub_classes = "select productline_subclass_id, productline_subclass_name " . 
									"from productline_subclass_productlines " .
		                            " left join productline_subclasses on  productline_subclass_id = productline_subclass_productline_class_id " . 
									"where (productline_subclass_active = 1 or productline_subclass_id = " . dbquote($project["project_product_line_subclass"]) . ") ".
		                            "and productline_subclass_productline_line_id = " . dbquote(param("product_line")) . 
		                            " order by productline_subclass_name";

	//count subclasses
	$sql_product_line_sub_classes_count = "select count(productline_subclass_id) as num_recs " . 
									"from productline_subclass_productlines " .
		                            " left join productline_subclasses on  productline_subclass_id = productline_subclass_productline_class_id " .
									"where (productline_subclass_active = 1 or productline_subclass_id = " . dbquote($project["project_product_line_subclass"]) . ") ".
		                            "and productline_subclass_productline_line_id = " . dbquote(param("product_line"));

	$res = mysql_query($sql_product_line_sub_classes_count) or dberror($sql_product_line_sub_classes_count);
    $row = mysql_fetch_assoc($res);
	$num_or_product_line_subclasses = $row["num_recs"];

	
}
elseif($project["project_product_line"] > 0) 
{
	$sql_product_line_sub_classes = "select productline_subclass_id, productline_subclass_name " . 
									"from productline_subclass_productlines " .
								    " left join productline_subclasses on  productline_subclass_id = productline_subclass_productline_class_id " . 
									"where (productline_subclass_active = 1 or productline_subclass_id = " . dbquote($project["project_product_line_subclass"]) . ") ".
		                            "and productline_subclass_productline_line_id = " . dbquote($project["project_product_line"]) . 
		                            " order by productline_subclass_name";

	//count subclasses
	$sql_product_line_sub_classes_count = "select count(productline_subclass_id) as num_recs " . 
									"from productline_subclass_productlines " .
								    " left join productline_subclasses on  productline_subclass_id = productline_subclass_productline_class_id " . 
									"where (productline_subclass_active = 1 or productline_subclass_id = " . dbquote($project["project_product_line_subclass"]) . ") ".
		                            "and productline_subclass_productline_line_id = " . dbquote($project["project_product_line"]);

	$res = mysql_query($sql_product_line_sub_classes_count) or dberror($sql_product_line_sub_classes_count);
    $row = mysql_fetch_assoc($res);
	$num_or_product_line_subclasses = $row["num_recs"];
}

//get addresses from pos index
$sql_posaddresses = "select posaddress_id, concat(posaddress_place, ', ', posaddress_name) as posaddress " .
                    "from posaddresses " . 
					"where posaddress_country = " . $project["order_shop_address_country"] . 
					" order by posaddress_place, posaddress_name";


$sql_project_type_subclasses = "select project_type_subclass_id, project_type_subclass_name " . 
                               " from project_type_subclasses order by project_type_subclass_name";

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);
$form->add_hidden("project_number", $project["project_number"]);

$form->add_hidden("old_retail_coordinator", $old_retail_coordinator);
$form->add_hidden("old_retail_operator", $old_retail_operator);

//show project information

//show project information
if(($project["project_projectkind"] == 1 
     and $project["project_actual_opening_date"] != NULL 
	 and $project["project_actual_opening_date"] != '0000-00-00') 
    or $project["project_projectkind"] == 2 
	or $project["project_projectkind"] == 3) // renovation or takeover/renovation
{
	$renovated_pos_id = get_renovated_pos_info($project["project_order"]);
	if ($renovated_pos_id > 0)
	{
		if (has_access("can_edit_pos_data") or has_access("can_view_pos_data"))
		{ 
			$shop_address = $project["order_shop_address_zip"] . " " .
							$project["order_shop_address_place"] . ", " .
							$project["order_shop_address_country_name"];
			
			$tmp = '<a href="../pos/posindex_pos.php?country=' . $project["order_shop_address_country"] . '&ltf=all&ostate=&province=&id=' . $renovated_pos_id . '" target="_blank">' . $project["order_shop_address_company"] . '</a><br />' . $shop_address;
		}
		$form->add_label("shop_address", "POS Location Address", RENDER_HTML, $tmp);
	}
	else
	{	
		$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];
		$form->add_label("shop_address", "POS Location Address", 0, $shop);
	}
}
else
{
	$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];
	$form->add_label("shop_address", "POS Location Address", 0, $shop);
}

if(($project["project_projectkind"] == 6 
	or $project["project_projectkind"] == 9) 
	and $project["project_relocated_posaddress_id"] > 0) // relocation
{
	$relocated_pos = get_relocated_pos_info($project["project_relocated_posaddress_id"]);

	if (count($relocated_pos) > 0)
	{
		if (has_access("can_edit_pos_data") or has_access("can_view_pos_data"))
		{ 
			$tmp = '<a href="../pos/posindex_pos.php?country=' . $project["order_shop_address_country"] . '&ltf=all&ostate=&province=&id=' . $project["project_relocated_posaddress_id"] . '" target="_blank">' . $relocated_pos["posaddress_name"] . ", " .$relocated_pos["place_name"] . '</a>';
			$form->add_label("relocated_pos", "Relocated POS", RENDER_HTML, $tmp );
		}
		else
		{
			$form->add_label("relocated_pos", "Relocated POS", 0, $relocated_pos["posaddress_name"] . ", " .$relocated_pos["place_name"] );
		}
	}
}


$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];




$form->add_label("client_address", "Client", 0, $client);


$franchisee = $project["order_franchisee_address_company"] . ", " .
        $project["order_franchisee_address_zip"] . " " .
        $project["order_franchisee_address_place"] . ", " .
        $project["order_franchisee_address_country_name"];

if($project["project_cost_type"] != 6 and $project["project_postype"] !=2)
{
	$form->add_label("franchisee_address", "Owner Company", 0, $franchisee);		
}
else
{
	$form->add_label("franchisee_address", "Owner Company", 0, $franchisee);		
}


if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
{
	$form->add_section("Production Type");
	$form->add_list("project_production_type", "Production Type",
		"select production_type_id, production_type_name from production_types " .
		"order by production_type_name", NOTNULL, $project["project_production_type"]);
}
else {
	$form->add_hidden("project_production_type", 1);
}

$form->add_label("type3", "Project Legal Type / Project Type", 0, $project["project_costtype_text"] . " / " . $project["projectkind_name"]);

$form->add_list("project_type_subclass_id", "Project Type Subclass", $sql_project_type_subclasses, 0, $project["project_type_subclass_id"]);

$form->add_checkbox("posaddress_is_flagship", "the POS is a flag ship POS", $project["project_is_flagship"], "", "Flag Ship Option");

$form->add_label("project_postype", "POS Type / Subclass", 0, $project["postype_name"] . " / " . $project["possubclass_name"]);


$form->add_list("product_line", "Product Line*", $sql_product_line, NOTNULL | SUBMIT, $project["project_product_line"]);

if($num_or_product_line_subclasses > 0)
{
	$form->add_list("product_line_subclass", "Product Line Subclass", $sql_product_line_sub_classes,0, $project["project_product_line_subclass"]);
}
else
{
	$form->add_hidden("product_line_subclass",0);
}

$form->add_label("project_number_label", "Project Number  /Treatment State", 0, $project["project_number"] . " / " . $project["project_state_text"]);


if (has_access("can_edit_status_in_projects") and
       ($project["order_actual_order_state_code"] == '120'
	    or $project["order_actual_order_state_code"] == '210')
    )
{
    $sql = "select distinct order_state_code " .
           "from order_states " .
		   " where order_state_code IN (800, " . $project["order_actual_order_state_code"] . ") " .
           "order by order_state_code";
    $form->add_list("status", "Project State", $sql, NOTNULL, $project["order_actual_order_state_code"]);

}
else
{
	$form->add_hidden("status", $project["order_actual_order_state_code"]);
	$form->add_label("status1", "Project Status", 0, $project["order_actual_order_state_code"]  . " " . $order_state_name);
}

//add fields for editing
$form->add_section("Project Management");
$form->add_list("retail_coordinator", "Project Leader*", $sql_retail_coordinators, NOTNULL, $project["project_retail_coordinator"]);

$form->add_list("local_retail_coordinator", "Local Project Leader", $sql_local_retail_coordinators, 0, $project["project_local_retail_coordinator"]);

//$form->add_list("hq_project_manager", "HQ Project Leader", $sql_rrmas, 0, $project["project_hq_project_manager"]);




if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) // take over, lease renewal
{

	$form->add_hidden("retail_operator", 0);
	$form->add_hidden("contractor_user_id", 0);
	$form->add_hidden("supervisor_user_id", 0);
	$form->add_hidden("cms_approver_user_id", 0);
	$form->add_hidden("delivery_confirmation_by", 0);
	
	
	if(count($tracking_info) > 0) {
		$form->add_edit("project_real_opening_date", $project["projectkind_milestone_name_01"], NOTNULL, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
	}
	else
	{
		$form->add_edit("project_real_opening_date", $project["projectkind_milestone_name_01"], NOTNULL, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
	}

	if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] != '0000-00-00')
	{
		$form->add_edit("change_comment", "Reason for changing agreed opening date*", 0);
	}
	else
	{
		$form->add_hidden("change_comment");
	}

	$form->add_hidden("old_shop_real_opening_date", to_system_date($project["project_real_opening_date"]));
	$form->add_hidden("project_no_planning");
	$form->add_hidden("project_is_local_production");
	$form->add_hidden("project_furniture_type_store");
	$form->add_hidden("project_furniture_type_sis");
	

	$form->add_hidden("old_project_state", $project["project_state"]);
	//$form->add_hidden("project_state", $project["project_state"]);

	if (has_access("can_edit_treatment_state"))
	{
		$form->add_section("Treatment State");
		
		
			
		if(count($tracking_info2) > 0) {
			//$sql = "select project_state_id, project_state_text from " .
			//	   " project_states  where project_state_selectable = 1";
		    $sql = "select project_state_id, project_state_text from " .
				   " project_states order by project_state_text";
			$form->add_list("project_state", "Treatment State", $sql, 0, $project["project_state"], 1, "changehistory2");

		}
		else
		{
			//$sql = "select project_state_id, project_state_text from " .
			//	   " project_states  where project_state_selectable = 1";
			$sql = "select project_state_id, project_state_text from " .
				   " project_states   order by project_state_text";
			$form->add_list("project_state", "Treatment State", $sql, 0, $project["project_state"]);
		}

		
		$form->add_edit("change_comment2", "Reason for changing the treatment state", 0);
	}
	$form->add_hidden("project_budget_covered_by");
}
else
{
	//$form->add_list("retail_operator", "Logistics Coordinator", $sql_retail_operators, NOTNULL, $project["order_retail_operator"]);
	$form->add_hidden("retail_operator", 0);
	
	$form->add_section("Design Staff");
	$form->add_list("contractor_user_id", "Design Contractor", $sql_contractors, 0, $project["project_design_contractor"]);
	$form->add_list("supervisor_user_id", "Design Supervisor", $sql_supervisors, 0, $project["project_design_supervisor"]);

	$form->add_section("Controller");
	$form->add_list("cms_approver_user_id", "CMS Approval", $sql_cms_approvers, 0, $project["project_cms_approver"]);

	$form->add_section("Confirmation of Delivery by");
	$form->add_list("delivery_confirmation_by", "Person", $sql_address_user, 0, $project["order_delivery_confirmation_by"]);

	
	if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
	{
		$form->add_section("Project Date");
	}
	elseif($project["project_projectkind"] == 3
		or $project["project_projectkind"] == 9) //Take Over and renovation
	{
		$form->add_section("POS Opening Dates");
		$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));
	}
	else
	{
		$form->add_section("POS Opening Dates");
	}

	$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));

	
	if(count($tracking_info) > 0) {
		$form->add_edit("project_real_opening_date", $project["projectkind_milestone_name_01"], NOTNULL, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
	}
	else
	{
		$form->add_edit("project_real_opening_date", $project["projectkind_milestone_name_01"], NOTNULL, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
	}

	if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] != '0000-00-00')
	{
		$form->add_edit("change_comment", "Reason for changing agreed opening date*", 0);
	}
	else
	{
		$form->add_hidden("change_comment");
	}

	
	$form->add_hidden("old_shop_real_opening_date", to_system_date($project["project_real_opening_date"]));

	if (has_access("can_edit_treatment_state"))
	{
		$form->add_section("Treatment State");
		
		
			
		if(count($tracking_info2) > 0) {
			//$sql = "select project_state_id, project_state_text from " .
			//	   " project_states  where project_state_selectable = 1";
		    $sql = "select project_state_id, project_state_text from " .
				   " project_states  order by project_state_text";


			 
			$form->add_list("project_state", "Treatment State", $sql, 0, $project["project_state"], 1, "changehistory2");

		}
		else
		{
			//$sql = "select project_state_id, project_state_text from " .
			//	   " project_states  where project_state_selectable = 1";
			$sql = "select project_state_id, project_state_text from " .
				   " project_states  order by project_state_text";


			$form->add_list("project_state", "Treatment State", $sql, 0, $project["project_state"]);
		}

		
		$form->add_edit("change_comment2", "Reason for changing the treatment state", 0);
	}
	else
	{
		$form->add_hidden("project_state", $project["project_state"]);
	}

	$form->add_hidden("old_project_state", $project["project_state"]);


	if($project["project_projectkind"] == 8)
	{
		$form->add_section("Budget Coverage");
		$form->add_list("project_budget_covered_by", "Budget covered by", "select budget_covering_unit_id, budget_covering_unit_unitname from budget_covering_units order by budget_covering_unit_unitname", NOTNULL, $project["project_budget_covered_by"]);
	}
	else
	{
		$form->add_hidden("project_budget_covered_by");
	}

	
	
	$form->add_hidden("project_no_planning");
	$form->add_hidden("project_is_local_production");
	$form->add_hidden("project_furniture_type_store");
	$form->add_hidden("project_furniture_type_sis");

	/*
	$form->add_section("Miscellanous");
	$form->add_checkbox("project_no_planning", "project does not need architectural planning", $project["project_no_planning"], 0, "Planning");
	$form->add_checkbox("project_is_local_production", "project is locally realized (local production)", $project["project_is_local_production"], 0, "Local Production");

	if($project["project_postype"] == 1) // Store
	{
		$form->add_hidden("project_furniture_type_store", 0);
		$form->add_checkbox("project_furniture_type_sis", "STORE project to be realized in SIS furniture", $project["project_furniture_type_sis"], 0, "Furniture Type Stores");
		
	}
	elseif($project["project_postype"] == 2) //SIS
	{
		$form->add_checkbox("project_furniture_type_store", "SIS project to be realized in STORE furniture", $project["project_furniture_type_store"], 0, "Furniture Type SIS");
		$form->add_hidden("project_furniture_type_sis", 0);
	}
	else
	{
		$form->add_hidden("project_furniture_type_store", 0);
		$form->add_hidden("project_furniture_type_sis", 0);
	}
	*/

	$form->add_section("Visuals");
	$form->add_checkbox("project_uses_icedunes_visuals", "The project uses Visuals", $project["project_uses_icedunes_visuals"], 0, "Visuals");
}


if($project["project_projectkind"] == 6
	or $project["project_projectkind"] == 9) // Relocation Project
{
	$form->add_section("Relocation Info");
	$form->add_comment("Please indicate the POS to be relocated.");
	$form->add_list("project_relocated_posaddress_id", "POS being relocated", $sql_posaddresses, 0, $project["project_relocated_posaddress_id"]);
}
else
{
	$form->add_hidden("project_relocated_posaddress_id", 0);
}



$form->add_hidden("project_use_ps2004", $project["project_use_ps2004"]);
//$form->add_checkbox("project_use_ps2004", "use project sheet 2004", $project["project_use_ps2004"]);


$form->add_button("save", "Save Data");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("product_line"))
{
	//get standard design contractor
	$sql = 'select standarddesigncontractor_dcon_user ' . 
		   'from standarddesigncontractors ' . 
	       'where standarddesigncontractor_productline = ' . dbquote($form->value("product_line")) . 
		   ' and standarddesigncontractor_postype = ' . dbquote($form->value("project_postype"));

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$form->value("contractor_user_id", $row["standarddesigncontractor_dcon_user"]);
	}

	$form->value("product_line_subclass", 0);
	$form->value("project_uses_icedunes_visuals", 0);

}
elseif ($form->button("save"))
{
	
	if($form->value("old_shop_real_opening_date") and $form->value("old_shop_real_opening_date") != $form->value("project_real_opening_date"))
	{
		$form->add_validation("{change_comment} != ''", "Please indicate the reason for changing the agreed opening date!");
	}


	if($form->value("old_project_state") != $form->value("project_state"))
	{
		$form->add_validation("{change_comment2} != ''", "Please indicate the reason for changing the treatment state!");
	}
	
	if ($form->validate())
    {
        project_update_retail_data($form);

		//update flag ship option
		if($project["posaddress_id"] > 0 and $project["pipeline"] == 1) // project is in pipeline (new, relocation)
		{
			$sql = "update posaddressespipeline set posaddress_is_flagship = " . dbquote($form->value("posaddress_is_flagship")) . 
				   " where posaddress_id = " . $project["posaddress_id"];
			$result = mysql_query($sql) or dberror($sql);
		}
		elseif($project["posaddress_id"] > 0 
			and $project["project_actual_opening_date"] != NULL
			and $project["project_actual_opening_date"] != '0000-00-00'
			)
		{
			$sql = "update posaddresses set posaddress_is_flagship = " . dbquote($form->value("posaddress_is_flagship")) . 
				   " where posaddress_id = " . $project["posaddress_id"];
			$result = mysql_query($sql) or dberror($sql);
		}

		$sql = "update projects set project_is_flagship = " . dbquote($form->value("posaddress_is_flagship")) . 
			   " where project_id = " . dbquote(param("pid"));
		
		$result = mysql_query($sql) or dberror($sql);
		
		if($form->value("old_shop_real_opening_date") != $form->value("project_real_opening_date"))
		{
			
			//project tracking
			$field = "project_real_opening_date";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote(to_system_date($form->value("old_shop_real_opening_date"))) . ", " . 
				   dbquote(to_system_date($form->value("project_real_opening_date"))) . ", " . 
				   dbquote($form->value("change_comment")) . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);

			
		}


		if($form->value("old_project_state") != $form->value("project_state"))
		{
			
			//project tracking
			$field = "project_state";
			$project_state_name_old = get_project_state_name($form->value("old_project_state"));
			$project_state_name_new = get_project_state_name($form->value("project_state"));
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote($project_state_name_old) . ", " . 
				   dbquote($project_state_name_new) . ", " . 
				   dbquote($form->value("change_comment2")) . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
		}


		if($old_retail_coordinator > 0 
			and $form->value('retail_coordinator') > 0
			and  $old_retail_coordinator != $form->value('retail_coordinator')) 
		{

			$field = "retail_coordinator";
			$old_tmp = get_user($old_retail_coordinator);
			$new_tmp = get_user($form->value('retail_coordinator'));
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote($old_tmp["name"] . ' ' . $old_tmp["firstname"]) . ", " . 
				   dbquote($new_tmp["name"] . ' ' . $new_tmp["firstname"]) . ", " . 
				   "'Project Leader changed', " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
		}

		if($old_retail_operator > 0 
			and $form->value('retail_operator') > 0
			and  $old_retail_operator != $form->value('retail_operator')) 
		{

			
			$field = "retail_operator";
			$old_tmp = get_user($old_retail_operator);
			$new_tmp = get_user($form->value('retail_operator'));
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote($old_tmp["name"] . ' ' . $old_tmp["firstname"]) . ", " . 
				   dbquote($new_tmp["name"] . ' ' . $new_tmp["firstname"]) . ", " .  
				   "'Logistics Coordinator changed', " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
		}
		
		if($old_project_number != $form->value('project_number')) {

				
				//update table old project numbers
				$sql = "Insert into oldproject_numbers (".
					   "oldproject_number_project_id, oldproject_number_user_id, " . 
					   "oldproject_number_old_number, oldproject_number_new_number, " . 
					   "user_created, date_created) VALUES (" .
					   $project["project_id"] . ', ' .
					   user_id() . ', ' .
					   dbquote($old_project_number) . ', ' .
					   dbquote($form->value('project_number')) . ', ' .
					   dbquote(user_login()) . ', ' .
					   dbquote(date("Y-m-d h:i:s")) . ')';
				$res = mysql_query($sql) or dberror($sql); 
		}


        $form->message("Your changes have been saved.");
    }   
}


    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit Retail Data");
$form->render();


?>


<div id="changehistory" style="display:none;">
    <strong>Changes of the agreed opening date</strong>
	<table class="table_tracking">
	<tr>
	<td class="label">User</td>
	<td class="label">Time</td>
	<td class="label">Old Value</td>
	<td class="label">New Value</td>
	<td class="label">Comment</td>
	</tr>

	<?php
		foreach($tracking_info as $key=>$values)
		{
			echo '<tr class="tr_tracking"><td class="td_tracking_nobr">' . $values['user_name'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_time'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_oldvalue'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_newvalue'] . '</td>';
			echo '<td class="td_tracking">' . $values['projecttracking_comment'] . '</td></tr>';
		}
	?>
	
	</table>
</div> 


<div id="changehistory2" style="display:none;">
    <strong>Changes of the treatment state</strong>
	<table class="table_tracking">
	<tr>
	<td class="label">User</td>
	<td class="label">Time</td>
	<td class="label">Old Value</td>
	<td class="label">New Value</td>
	<td class="label">Comment</td>
	</tr>

	<?php
		foreach($tracking_info2 as $key=>$values)
		{
			echo '<tr class="tr_tracking"><td class="td_tracking_nobr">' . $values['user_name'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_time'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_oldvalue'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_newvalue'] . '</td>';
			echo '<td class="td_tracking">' . $values['projecttracking_comment'] . '</td></tr>';
		}
	?>
	
	</table>
</div> 

<?php

echo "<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>";

$page->footer();

?>