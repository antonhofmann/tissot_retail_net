<?php
/********************************************************************

    offer_functions.php

    Various utility functions to save information into tables

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-11
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
   update or delete project order items (material list)
*********************************************************************/
function  update_offer_groups($list, $form, $defaul_cost_group = 7)
{
	global $db;

    $prices = $list->values("lwoffergroup_price");
    //$prices_billed = $list->values("lwoffergroup_price_billed");

    //$hasoffers = $list->values("lwoffergroup_hasoffer");
    $inbudget = $list->values("lwoffergroup_inbudget");

    foreach ($list->values("lwoffergroup_price") as $key=>$value)
    {
        // update record
        $offer_position_fields = array();

        
        //if($prices[$key] > 0)
        if($prices[$key])
        {
            $offer_position_fields[] = "lwoffergroup_price = " . $value;
        }
        else
        {
            $offer_position_fields[] = "lwoffergroup_price = NULL";
        }
        
		/*
		if($prices_billed[$key] > 0)
        {
            $offer_position_fields[] = "lwoffergroup_price_billed = " . $prices_billed[$key];
        }
        else
        {
            $offer_position_fields[] = "lwoffergroup_price_billed = NULL";
        }
        */
        if($inbudget[$key] != "yes" and $inbudget[$key] != "no")
        {
            //$offer_position_fields[] = "lwoffergroup_hasoffer = '" . $hasoffers[$key] . "'";
            
			if(array_key_exists("__lwoffergroups_lwoffergroup_inbudget_" . $key, $_POST))
			{
				$offer_position_fields[] = "lwoffergroup_inbudget = '" . $inbudget[$key] . "'";
			}
			else
			{
				$offer_position_fields[] = "lwoffergroup_inbudget = 0";
			}
        }

        $value1 = "current_timestamp";
        $offer_position_fields[] = "date_modified = " . $value1;

        if (isset($_SESSION["user_login"]))
        {
            $value1 = dbquote($_SESSION["user_login"]);
            $offer_position_fields[] = "user_modified = " . $value1;
        }
   
        $sql = "update lwoffergroups set " . join(", ", $offer_position_fields) . " where lwoffergroup_id = " . $key;
        mysql_query($sql) or dberror($sql);

    }

    //update order items of the order by local construction works (order_item_type = 6)
    $offer_id = $form->value("lwoffer_id");


    $order_id = $form->value("lwoffer_order");
    $order_item_id = $form->value("lwoffer_order_item");
    $order_item_type = 6;
    $order_item_quantity = 1;
    
    $sql =   "select lwoffergroup_id, lwoffer_order, " .
             "lwoffergroup_group, lwoffergroup_text, lwoffergroup_code, " .
             "sum(lwoffergroup_price) as total, " .
             "sum(lwoffergroup_price_billed) as total_billed, lwgroup_costgroup " .
             "from lwoffers " .
             "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " .
		     "left join lwgroups on lwgroup_code = lwoffergroup_code " . 
             "where lwoffergroup_inbudget = 1 and lwoffer_id = " . $offer_id .
             " GROUP BY lwoffer_order, lwoffergroup_code, lwgroup_costgroup " . 
		     " order by lwoffergroup_code ";

	$budget = array();
    $billed = array();

    $res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
    {
        $budget[$row["lwoffergroup_code"]] = $row["total"];
        $billed[$row["lwoffergroup_code"]] = $row["total_billed"];


		
		//check if there is already an order item having the same code
		$order_id = $form->value("lwoffer_order");


		$sql_a = "select order_item_id, order_item_localcostgroupcode " .
			     " from order_items " . 
			     "where order_item_localcostgroupcode = " . dbquote($row["lwoffergroup_code"]) . 
			     " and order_item_order = " . $order_id;

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if($row_a = mysql_fetch_assoc($res_a))
		{
			$last_digit = substr($row["lwoffergroup_code"], strlen($row["lwoffergroup_code"])-1, 1);
			
			
			if(!is_numeric($last_digit))
			{
				$last_digit = $last_digit . "1";
				$new_lwoffergroup_code = substr($row["lwoffergroup_code"], 0, strlen($row["lwoffergroup_code"])) . $last_digit;
			}
			else
			{
				$last_digit = 1*$last_digit + 1;
				$new_lwoffergroup_code = substr($row["lwoffergroup_code"], 0, strlen($row["lwoffergroup_code"])-1) . $last_digit;
			}
			
			
			$sql_u = "update lwoffergroups set lwoffergroup_code = " . dbquote($new_lwoffergroup_code) .
				     " where lwoffergroup_id = " . $row["lwoffergroup_id"];

			$result = mysql_query($sql_u) or dberror($sql_u);


			$budget[$new_lwoffergroup_code] = $budget[$row["lwoffergroup_code"]];
			$billed[$new_lwoffergroup_code] = $billed[$row["lwoffergroup_code"]];
		}


		/*
		$sql_a =   "select lwoffergroup_id, lwoffergroup_code " .
					 "from lwoffers " .
					 "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " .
					 "left join lwgroups on lwgroup_code = lwoffergroup_code " . 
					 "where lwoffergroup_code = " . dbquote($row["lwoffergroup_code"]) . 
			         " and lwoffer_order = " . $order_id . 
					 " and lwoffer_id <> " . $offer_id;
		
		$res_a = mysql_query($sql_a) or dberror($sql_a);
		while($row_a = mysql_fetch_assoc($res_a))
		{
			$last_digit = substr($row["lwoffergroup_code"], strlen($row["lwoffergroup_code"])-1, 1);
			
			
			if(!is_numeric($last_digit))
			{
				$last_digit = $last_digit . "1";
				$new_lwoffergroup_code = substr($row["lwoffergroup_code"], 0, strlen($row["lwoffergroup_code"])) . $last_digit;
			}
			else
			{
				$last_digit = 1*$last_digit + 1;
				$new_lwoffergroup_code = substr($row["lwoffergroup_code"], 0, strlen($row["lwoffergroup_code"])-1) . $last_digit;
			}
			
			
			$sql_u = "update lwoffergroups set lwoffergroup_code = " . dbquote($new_lwoffergroup_code) .
				     " where lwoffergroup_id = " . $row["lwoffergroup_id"];

			$result = mysql_query($sql_u) or dberror($sql_u);

		}
		*/
    }




    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {

        $cost_group = $defaul_cost_group;
		if($row["lwgroup_costgroup"] > 0) {
			$cost_group = $row["lwgroup_costgroup"];
		}
		
		if($billed[$row["lwoffergroup_code"]] > 0)
        {
            $order_item_supplier_price = $billed[$row["lwoffergroup_code"]];
        }
        else
        {
            $order_item_supplier_price = $budget[$row["lwoffergroup_code"]];
        }
        
        $currency = get_currency($form->value("lwoffer_currency"));

        
        $order_item_system_price = $order_item_supplier_price * $currency["exchange_rate"] / $currency["factor"];

        $order_item_supplier_currency = $form->value("lwoffer_currency");
        $order_item_supplier_exchange_rate = $currency["exchange_rate"];

        $currency = get_order_currency($order_id);

        
        $order_item_client_price = $order_item_system_price / $currency["exchange_rate"] * $currency["factor"];
        
        
        $num_recs = 0;
        $insert_new = false;
        $sql_a = "select count(order_item_id) as num_recs " .
                 "from order_items as num_recs " .
                 "where order_item_order = " . $order_id . 
                 " and  order_item_localcostgroupcode = '" . $row["lwoffergroup_code"] ."'";

        $res_a = mysql_query($sql_a) or dberror($sql_a);
        if ($row_a = mysql_fetch_assoc($res_a))
        {
            $num_recs = $row_a["num_recs"];

        }

       
        if($num_recs > 0) // update existing record for local works
        {

			if($order_item_supplier_price == 0)
            {
				$sql_u = "Delete from order_items " .
                         "where order_item_order = " . $order_id . 
                         " and  order_item_localcostgroupcode = '" . $row["lwoffergroup_code"] . "'";

                $last_id = NULL;
            }
            else
            {
                $sql_u = "Update order_items SET " . 
                         "order_item_text = '" . $row["lwoffergroup_text"] . "', " .
                         "order_item_supplier_price = '" . $order_item_supplier_price . "', " .
                         "order_item_system_price = '" . $order_item_system_price . "', " .
                         "order_item_client_price = '" . $order_item_client_price . "', " .
                         "order_item_supplier_currency = '" . $order_item_supplier_currency . "', " .
                         "order_item_supplier_exchange_rate = '" . $order_item_supplier_exchange_rate . "', " .
					     "order_item_cost_group = " . dbquote($cost_group) . ", " . 
					     "order_item_supplier_freetext = " . dbquote($form->value("lwoffer_company")) . ", " .
                         "user_modified = " . user_id()  . ", " .
                         "date_modified = '" . date("Y-m-d") . "' " .
                         "where order_item_order = " . $order_id . 
                         " and order_item_localcostgroupcode = '" . $row["lwoffergroup_code"] . "'";
                         
                         $last_id = $order_item_id ;
            }

            $res_u = mysql_query($sql_u) or dberror($sql_u);
        
        }
        else
        {
			$sql_u = "insert into order_items (" . 
                     "order_item_order, " .
                     "order_item_type, " .
                     "order_item_localcostgroupcode, " .
                     "order_item_quantity, " .
                     "order_item_text, " .
                     "order_item_supplier_price, " .
                     "order_item_system_price, " .
                     "order_item_client_price, " .
                     "order_item_supplier_currency, " .
                     "order_item_supplier_exchange_rate, " .
				     "order_item_supplier_freetext, ".
                     "order_item_cost_group, " .
				     "order_item_price_entered_in_loc, " . 
                     "user_created, " .
                     "date_created, " .
                     "user_modified, " .
                     "date_modified " .
                     ") values (" .
                     $order_id . ", " .
                     $order_item_type . ", '" .
                     $row["lwoffergroup_code"] . "', " .
                     $order_item_quantity . ", '" .
                     $row["lwoffergroup_text"] .  "', " .
                     "'" . $order_item_supplier_price . "', " .
                     "'" . $order_item_system_price . "', " .
                     "'" . $order_item_client_price . "', " .
                     $order_item_supplier_currency . ", " .
                     $order_item_supplier_exchange_rate . ", " .
				     dbquote($form->value("lwoffer_company")) . ", " . 
                     dbquote($cost_group)  . ", " .
				     "1, " . 
                     user_id() . ", '" .
                     date("Y-m-d")  .  "', " .
                     user_id() . ", '" .
                     date("Y-m-d")  .  "')";


                    

                $res_u = mysql_query($sql_u) or dberror($sql_u);

                $last_id = mysql_insert_id($db);

        }
        
        //$sql_u = "update lwoffers Set lwoffer_order_item = '" . $last_id . "'" . 
        //         " where lwoffer_id = " . $offer_id;

        //$res_u = mysql_query($sql_u) or dberror($sql_u);
    }

    return true;
}


/********************************************************************
   update or delete project order items (material list)
*********************************************************************/
function  update_offer_positions($list, $form)
{
	global $db;

    $prices = $list->values("lwofferposition_price");
    $units_billed = $list->values("lwofferposition_numofunits_billed");
    $prices_billed = $list->values("lwofferposition_price_billed");

    $hasoffers = $list->values("lwofferposition_hasoffer");
    $inbudget = $list->values("lwofferposition_inbudget");

    //print_r($hasoffers);
    foreach ($list->values("lwofferposition_numofunits") as $key=>$value)
    {
        // update record
        $offer_position_fields = array();

        if($value > 0)
        {
            $offer_position_fields[] = "lwofferposition_numofunits = " . $value;
        }
        else
        {
            $offer_position_fields[] = "lwofferposition_numofunits = NULL";
        }
        if($prices[$key] > 0)
        {
            $offer_position_fields[] = "lwofferposition_price = " . $prices[$key];
        }
        else
        {
            $offer_position_fields[] = "lwofferposition_price = NULL";
        }
        
		/*
		if($prices_billed[$key] > 0)
        {
            $offer_position_fields[] = "lwofferposition_price_billed = " . $prices_billed[$key];
        }
        else
        {
            $offer_position_fields[] = "lwofferposition_price_billed = NULL";
        }
        if($units_billed[$key] > 0)
        {
            $offer_position_fields[] = "lwofferposition_numofunits_billed = " . $units_billed[$key];
        }
        else
        {
            $offer_position_fields[] = "lwofferposition_numofunits_billed = NULL";
        }
		*/
        
        if($inbudget[$key] != "yes" and $inbudget[$key] != "no")
        {
            $offer_position_fields[] = "lwofferposition_hasoffer = '" . $hasoffers[$key] . "'";
            $offer_position_fields[] = "lwofferposition_inbudget = '" . $inbudget[$key] . "'";
        }

        $value1 = "current_timestamp";
        $offer_position_fields[] = "date_modified = " . $value1;

        if (isset($_SESSION["user_login"]))
        {
            $value1 = dbquote($_SESSION["user_login"]);
            $offer_position_fields[] = "user_modified = " . $value1;
        }
   
        $sql = "update lwofferpositions set " . join(", ", $offer_position_fields) . " where lwofferposition_id = " . $key;
        mysql_query($sql) or dberror($sql);

    }


    //update order items of the order by local construction works (order_item_type = 6)
    $offer_id = $form->value("lwoffer_id");


    $order_id = $form->value("lwoffer_order");
    $order_item_id = $form->value("lwoffer_order_item");
    $order_item_type = 6;
    $order_item_quantity = 1;
    $order_item_text = "Local Construction Works " . $form->value("lwoffer_company");
    
    $sql =   "select lwoffer_order, lwofferposition_lwoffergroup, lwgroup_costgroup, " .
             "lwoffergroup_group, lwoffergroup_text, lwoffergroup_code, " .
             "sum(lwofferposition_numofunits*lwofferposition_price) as total, " .
             "sum(lwofferposition_numofunits_billed*lwofferposition_price_billed) as total_billed " .
             "from lwoffers " .
             "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " . 
             "left join lwofferpositions on lwofferposition_lwoffergroup = lwoffergroup_id " .
             "where lwofferposition_inbudget = 1 and lwoffer_id = " . $offer_id .
             " GROUP BY lwoffer_order, lwoffergroup_code ";

    $budget = array();
    $billed = array();

    $res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
    {
        $budget[$row["lwoffergroup_code"]] = $row["total"];
        $billed[$row["lwoffergroup_code"]] = $row["total_billed"];
    }

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {

        if($billed[$row["lwoffergroup_code"]] > 0)
        {
            $order_item_supplier_price = $billed[$row["lwoffergroup_code"]];
        }
        else
        {
            $order_item_supplier_price = $budget[$row["lwoffergroup_code"]];
        }
        
        $currency = get_currency($form->value("lwoffer_currency"));
        
        $order_item_system_price = $order_item_supplier_price * $currency["exchange_rate"] / $currency["factor"];

        $order_item_supplier_currency = $form->value("lwoffer_currency");
        $order_item_supplier_exchange_rate = $currency["exchange_rate"];

        $currency = get_order_currency($order_id);
        
        $order_item_client_price = $order_item_system_price / $currency["exchange_rate"] * $currency["factor"];
        
        
        $num_recs = 0;
        $insert_new = false;
        $sql_a = "select count(order_item_id) as num_recs " .
                 "from order_items as num_recs " .
                 "where order_item_order = " . $order_id . 
                 " and  order_item_localcostgroupcode = '" . $row["lwoffergroup_code"] ."'";

        $res_a = mysql_query($sql_a) or dberror($sql_a);
        if ($row_a = mysql_fetch_assoc($res_a))
        {
            $num_recs = $row_a["num_recs"];

        }

       
        if($num_recs > 0) // update existing record for local works
        {

            if($order_item_supplier_price == 0)
            {
                $sql_u = "Delete from order_items " .
                         "where order_item_order = " . $order_id . 
                         " and  order_item_localcostgroupcode = '" . $row["lwoffergroup_code"] . "'";

                $last_id = NULL;
            }
            else
            {
                $sql_u = "Update order_items SET " . 
                         "order_item_text = '" . $row["lwoffergroup_text"] . "', " .
                         "order_item_supplier_price = '" . $order_item_supplier_price . "', " .
                         "order_item_system_price = '" . $order_item_system_price . "', " .
                         "order_item_client_price = '" . $order_item_client_price . "', " .
                         "order_item_supplier_currency = '" . $order_item_supplier_currency . "', " .
                         "order_item_supplier_exchange_rate = '" . $order_item_supplier_exchange_rate . "', " .
                         "user_modified = " . user_id()  . ", " .
                         "date_modified = '" . date("Y-m-d") . "' " .
                         "where order_item_order = " . $order_id . 
                         " and order_item_localcostgroupcode = '" . $row["lwoffergroup_code"] . "'";
                         
                         $last_id = $order_item_id ;

                //echo $sql_u . "<br><br>";

            }

            $res_u = mysql_query($sql_u) or dberror($sql_u);
        
        }
        else
        {
            $sql_u = "insert into order_items (" . 
                     "order_item_order, " .
                     "order_item_type, " .
                     "order_item_localcostgroupcode, " .
                     "order_item_quantity, " .
                     "order_item_text, " .
                     "order_item_supplier_price, " .
                     "order_item_system_price, " .
                     "order_item_client_price, " .
                     "order_item_supplier_currency, " .
                     "order_item_supplier_exchange_rate, " .
                     "order_item_cost_group, " .
                     "user_created, " .
                     "date_created, " .
                     "user_modified, " .
                     "date_modified " .
                     ") values (" .
                     $order_id . ", " .
                     $order_item_type . ", '" .
                     $row["lwoffergroup_code"] . "', " .
                     $order_item_quantity . ", '" .
                     $row["lwoffergroup_text"] .  "', " .
                     "'" . $order_item_supplier_price . "', " .
                     "'" . $order_item_system_price . "', " .
                     "'" . $order_item_client_price . "', " .
                     $order_item_supplier_currency . ", " .
                     $order_item_supplier_exchange_rate . ", " .
                     $row["lwgroup_costgroup"]  . ", " .
                     user_id() . ", '" .
                     date("Y-m-d")  .  "', " .
                     user_id() . ", '" .
                     date("Y-m-d")  .  "')";
                    
                
                $res_u = mysql_query($sql_u) or dberror($sql_u);

				echo $sql_u . "<br>";


                $last_id = mysql_insert_id($db);
        }

        
        
        //$sql_u = "update lwoffers Set lwoffer_order_item = '" . $last_id . "'" . 
        //         " where lwoffer_id = " . $offer_id;

        //$res_u = mysql_query($sql_u) or dberror($sql_u);
    }
    return true;
}


/********************************************************************
    get offer  data 
*********************************************************************/
function get_offer($id)
{
    $offer = array();

    $sql = "select * ".
           "from lwoffers " .
           "left join currencies on currency_id = lwoffer_currency " . 
           "where lwoffer_id  = " . $id;


    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $offer = $row;
    }


    return $offer;
}

?>