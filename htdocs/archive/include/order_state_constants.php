<?php
/********************************************************************

    order_state_constants.php

    Defines Constants used in the context with order states

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-19
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-03-03
    Version:        1.0.0


    Copyright (c) 2002, Swatch AG, All Rights Reserved.

/********************************************************************
    Constants
    Acces rights to a step are connected with the name of the step
    refer to table permissions
    e.g. can_perform_action_in_orders_110

    also there is a string for each order state in table strings
    having the order_state_code in its name e.g. order_110

    so if you are going to change constants you also have to
    change the names of permissions in the table permissions
    and the names of strings in table strings
*********************************************************************/


define("RETAIL_OPERATOR_ASSIGNED", "110");
define("RETAIL_COORDINATOR_ASSIGNED", "110");
define("RETAIL_OPERATOR_ASSIGNED_P", "120");

define("ORDER_CONFIRMED", "210");
define("LAYOUT_REQUEST_ACCEPTED", "210");
define("SUBMIT_BRIEFING_FOR_LAYOUT_PREVIEW", "220");
define("REQUEST_FOR_LAYOUT_PREVIEW_ACCEPTED", "240");
define("LAYOUT_PREVIEW_ACCEPTED", "280");

define("SUBMIT_REQUEST_FOR_DESIGN_APPROVAL", "300");
define("SUBMIT_LAYOUT_APPROVAL_TO_CLIENT", "330");
define("LAYOUT_ACCEPTED", "350");

define("REQUEST_FOR_MINI_BOOKLET_ACCEPTED", "410");
define("SUBMIT_MINI_BOOKLET_FOR_APPROVAL", "411");
define("MINI_BOOKLET_APPROVED", "413");
define("MINI_BOOKLET_APPROVED_BY_MANAGEMENT", "414");
define("SUBMIT_MINI_BOOKLET_TO_CLIENT", "415");


define("REQUEST_FOR_BOOKLET_ACCEPTED", "422");
define("PROJECT_BOOKLET_APPROVED", "426");

define("PROJECT_BOOKLET_ACCEPTED_BY_CLIENT", "441");

define("SUBMIT_REQUEST_FOR_OFFER", "510");
define("REQUEST_FOR_OFFER_SUBMITTED", "510");
define("REQUEST_FOR_OFFER_REJECTED", "520");
define("REQUEST_FOR_OFFER_ACCEPTED", "530");
define("OFFER_SUBMITTED", "540");
define("OFFER_REJECTED", "550");
define("OFFER_ACCEPTED", "560");

define("ORDERS_REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED", "600");
define("ORDERS_BUDGET_APPROVED", "620");

define("REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED", "600");
define("BUDGET_APPROVED", "620");
define("ORDER_TO_SUPPLIER_SUBMITTED", "700");
define("REJECT_ORDER_BY_SUPPLIER", "710");
define("CONFIRM_ORDER_BY_SUPPLIER", "720");
define("REQUEST_FOR_DELIVERY_SUBMITTED", "730");
define("REQUEST_FOR_DELIVERY_ACCEPTED", "740");
define("PICK_UP_CONFIRMED", "745");
define("DELIVERY_CONFIRMED_FRW", "750");


define("DELIVERY_CONFIRMED", "800");

define("REQUEST_FOR_FULL_BUDGET_APPROVAL_SUBMITTED", "820");
define("FULL_BUDGET_APPROVED", "840");


define("CMS_COMPLETTION_LOGISTICS", "850");
define("CMS_COMPLETTION_CLIENT", "851");
define("CMS_REJECT_LOGISTICS", "864");
define("CMS_REJECT_CLIENT", "865");
define("CMS_COMPLETTION_DEVELOPMENT", "866");
define("CMS_REJECT_BY_CONTROLLER", "867");
define("CMS_APPROVE_BY_CONTROLLER", "868");


define("MOVED_TO_THE_RECORDS", "890");

define("ORDER_CANCELLED", "900");
define("EMAIL_SENT", "910");


?>