<?php
/********************************************************************

    save_functions.php

    Various utility functions to save information into tables

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-08
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-02-23
    Version:        1.2.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
   update cost monitoring information for items
*********************************************************************/
function  project_update_order_item_cost1($list, $nocom = true, $order_currency = array(), $show_budget_in_loc = false)
{
		
	//get cost group names
	$costgroup_names = array();
	$sql = "select project_cost_groupname_id, project_cost_groupname_name " . 
		   " from project_cost_groupnames ";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$costgroup_names[$row["project_cost_groupname_id"]] = $row["project_cost_groupname_name"];
	}

	//get cost monitoring groups
	$costmonitoringgroups = array();
	$sql = "select costmonitoringgroup_id, costmonitoringgroup_text " . 
		   " from costmonitoringgroups ";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$costmonitoringgroups[$row["costmonitoringgroup_id"]] = $row["costmonitoringgroup_text"];
	}


	//get old values of order_items
	$old_values = array();
	
	if($show_budget_in_loc == true)
	{
		foreach ($list->values("order_item_cost_group") as $key=>$value)
		{
			$sql = "select order_item_order, order_item_type, order_item_cost_group, " .
				   "order_item_item, order_item_text, order_item_quantity, order_item_real_client_price, " .
				   " order_item_cms_remark, order_items_costmonitoring_group " . 
				   " from order_items " .
				   " where order_item_id = " . $key;
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$old_values[$key] = $row;
			}
		}
	}
	else
	{
		foreach ($list->values("order_item_cost_group") as $key=>$value)
		{
			$sql = "select order_item_order, order_item_type, order_item_cost_group, " .
				   "order_item_item, order_item_text, order_item_quantity, order_item_real_system_price, " .
				   " order_item_cms_remark, order_items_costmonitoring_group " . 
				   " from order_items " .
				   " where order_item_id = " . $key;
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$old_values[$key] = $row;
			}
		}
	}

	foreach ($list->values("order_item_cost_group") as $key=>$value)
    {
		// update record
        if(!$value) {$value = 0;}
        $order_item_fields = array();

        $order_item_fields[] = "order_item_cost_group = " . $value;

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_cost_group"])
		{
			 $old_costgroup_name = "";
			 $new_costgroup_name = "";
			 if(array_key_exists($old_values[$key]["order_item_cost_group"], $costgroup_names))
			 {
				$old_costgroup_name = $costgroup_names[$old_values[$key]["order_item_cost_group"]];
			 }
			 if(array_key_exists($value, $costgroup_names))
			 {
				$new_costgroup_name = $costgroup_names[$value];
			 }
			 
			 $result = track_change_in_order_items('Change cost group in CMS', 'Cost Group', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_costgroup_name, $new_costgroup_name);
		}
    }


    foreach ($list->values("order_item_quantity") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}
        $order_item_fields = array();

        $order_item_fields[] = "order_item_quantity = " . $value;

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);


		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_quantity"])
		{
			 $result = track_change_in_order_items('Change cost group in CMS', 'Quantity', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_quantity"], $value);
		}

    }

	if($show_budget_in_loc == true)
	{
		foreach ($list->values("order_item_real_client_price") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}
		
			$order_item_fields = array();

			$order_item_fields[] = "order_item_real_client_price = " . $value;
			
			if(count($order_currency) > 0)
			{
				$client_price = $value;
				if($order_currency["exchange_rate"] > 0)
				{
					$system_price = $client_price * $order_currency["exchange_rate"] / $order_currency["factor"];
				}
				else
				{
					$system_price = 0;
				}
				$order_item_fields[] = "order_item_real_system_price = " . $system_price;
			}

			$value1 = "current_timestamp";
			$project_fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = dbquote($_SESSION["user_login"]);
				$project_fields[] = "user_modified = " . $value1;
			}
	   
			$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
			mysql_query($sql) or dberror($sql);

			//add tracking information
			if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_real_client_price"])
			{
				 $result = track_change_in_order_items('Change cost group in CMS', 'Real Cost', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $order_currency["symbol"] . " " . $old_values[$key]["order_item_real_client_price"], $order_currency["symbol"] . " " . $value);
			}
		}
	}
	else
	{
		foreach ($list->values("order_item_real_system_price") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}
		
			$order_item_fields = array();

			$order_item_fields[] = "order_item_real_system_price = " . $value;
			
			if(count($order_currency) > 0)
			{
				$system_price = $value;
				if($order_currency["exchange_rate"] > 0)
				{
					$client_price = $system_price / $order_currency["exchange_rate"] * $order_currency["factor"];
				}
				else
				{
					$client_price = 0;
				}
				$order_item_fields[] = "order_item_real_client_price = " . $client_price;
			}

			$value1 = "current_timestamp";
			$project_fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = dbquote($_SESSION["user_login"]);
				$project_fields[] = "user_modified = " . $value1;
			}
	   
			$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
			mysql_query($sql) or dberror($sql);

			//add tracking information
			if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_real_system_price"])
			{
				 $result = track_change_in_order_items('Change cost group in CMS', 'Real Cost', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], "CHF " . $old_values[$key]["order_item_real_system_price"], "CHF " . $value);
			}
		}
	}

	foreach ($list->values("order_item_cms_remark") as $key=>$value)
    {
        // update record
    
        $order_item_fields = array();

        
		$order_item_fields[] = "order_item_cms_remark = " . dbquote($value);

        $value1 = "current_timestamp";
        $project_fields[] = "date_modified = " . $value1;

        if (isset($_SESSION["user_login"]))
        {
            $value1 = dbquote($_SESSION["user_login"]);
            $project_fields[] = "user_modified = " . $value1;
        }
   
        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);


		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_cms_remark"])
		{
			 $result = track_change_in_order_items('Change cost group in CMS', 'Remark', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_cms_remark"], $value);
		}
    }

    /*
    if($project["order_date"] > "2005-10-31")
    {
        //update budget approved
        if(isset($list["order_item_quantity_freezed"]))
        {
            foreach ($list->values("order_item_quantity_freezed") as $key=>$value)
            {
                // update record
                if(!$value) {$value = 0;}
                $order_item_fields = array();

                $order_item_fields[] = "order_item_quantity_freezed = " . $value;

                $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
                mysql_query($sql) or dberror($sql);
            }

            foreach ($list->values("order_item_system_price_freezed") as $key=>$value)
            {
                // update record
                if(!$value) {$value = 0;}
            
                $order_item_fields = array();

                $order_item_fields[] = "order_item_system_price_freezed = " . $value;

                $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
                mysql_query($sql) or dberror($sql);
            }
        }
    }
	*/
	
	/* cost monitoring group was removed by Katrin Eckert on January 26th 2016
	if($nocom == true)
	{
		foreach ($list->values("order_items_costmonitoring_group") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}
		
			$order_item_fields = array();

			$order_item_fields[] = "order_items_costmonitoring_group = " . $value;

			$value1 = "current_timestamp";
			$project_fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = dbquote($_SESSION["user_login"]);
				$project_fields[] = "user_modified = " . $value1;
			}
	   
			$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
			mysql_query($sql) or dberror($sql);

			//add tracking information
			if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_items_costmonitoring_group"])
			{
				 $old_costmonitoring_group_name = "";
				 $new_costmonitoring__group_name = "";
				 if(array_key_exists($old_values[$key]["order_items_costmonitoring_group"], $costmonitoringgroups))
				 {
					$old_costmonitoring_group_name = $costmonitoringgroups[$old_values[$key]["order_items_costmonitoring_group"]];
				 }
				 if(array_key_exists($value, $costmonitoringgroups))
				 {
					$new_costmonitoring__group_name = $costmonitoringgroups[$value];
				 }
				 
				 $result = track_change_in_order_items('Change cost monitoring group in CMS', 'Benchmark Item Group', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_costmonitoring_group_name, $new_costmonitoring__group_name);
			}
		}
	}
	*/
}


/********************************************************************
   update cost monitoring information for otehr cost
*********************************************************************/
function  project_update_order_item_cost2($list, $order_currency = array(), $show_budget_in_loc = false)
{

	//get cost group names
	$costgroup_names = array();
	$sql = "select project_cost_groupname_id, project_cost_groupname_name " . 
		   " from project_cost_groupnames ";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$costgroup_names[$row["project_cost_groupname_id"]] = $row["project_cost_groupname_name"];
	}

	
	//get old values of order_items
	$old_values = array();
	if($show_budget_in_loc == true)
	{
		foreach ($list->values("order_item_cost_group") as $key=>$value)
		{
			$sql = "select order_item_order, order_item_type, order_item_cost_group, " .
				   "order_item_item, order_item_text, order_item_quantity, order_item_real_client_price, " .
				   " order_item_cms_remark, order_items_costmonitoring_group " . 
				   " from order_items " .
				   " where order_item_id = " . $key;
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$old_values[$key] = $row;
			}
		}
	}
	else
	{
		foreach ($list->values("order_item_cost_group") as $key=>$value)
		{
			$sql = "select order_item_order, order_item_type, order_item_cost_group, " .
				   "order_item_item, order_item_text, order_item_quantity, order_item_real_system_price, " .
				   " order_item_cms_remark, order_items_costmonitoring_group " . 
				   " from order_items " .
				   " where order_item_id = " . $key;
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$old_values[$key] = $row;
			}
		}
	}

	foreach ($list->values("order_item_cost_group") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}
        $order_item_fields = array();

        $order_item_fields[] = "order_item_cost_group = " . $value;

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);


		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_cost_group"])
		{
			 $old_costgroup_name = "";
			 $new_costgroup_name = "";
			 if(array_key_exists($old_values[$key]["order_item_cost_group"], $costgroup_names))
			 {
				$old_costgroup_name = $costgroup_names[$old_values[$key]["order_item_cost_group"]];
			 }
			 if(array_key_exists($value, $costgroup_names))
			 {
				$new_costgroup_name = $costgroup_names[$value];
			 }
			 
			 $result = track_change_in_order_items('Change cost group in CMS', 'Cost Group', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_costgroup_name, $new_costgroup_name);
		}
    }

    /*
	foreach ($list->values("order_item_supplier_freetext") as $key=>$value)
    {
        // update record

        $order_item_fields = array();

        $order_item_fields[] = "order_item_supplier_freetext = " . dbquote($value);

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

    }
	*/

    if($show_budget_in_loc == true)
	{
		foreach ($list->values("order_item_real_client_price") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}

			$order_item_fields = array();

			$order_item_fields[] = "order_item_real_client_price = " . $value;

			if(count($order_currency) > 0)
			{
				$client_price = $value;
				if($order_currency["exchange_rate"] > 0)
				{
					$system_price = $client_price * $order_currency["exchange_rate"] / $order_currency["factor"];
				}
				else
				{
					$system_price = 0;
				}
				$order_item_fields[] = "order_item_real_system_price = " . $system_price;
			}


			$value1 = "current_timestamp";
			$project_fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = dbquote($_SESSION["user_login"]);
				$project_fields[] = "user_modified = " . $value1;
			}
	   
			$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
			mysql_query($sql) or dberror($sql);

			//echo $sql . "<br />";

			//add tracking information
			if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_real_client_price"])
			{
				 $result = track_change_in_order_items('Change cost group in CMS', 'Real Cost', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $order_currency["symbol"] . " " . $old_values[$key]["order_item_real_client_price"], $order_currency["symbol"] . " " . $value);
			}

		}
	}
	else
	{
		foreach ($list->values("order_item_real_system_price") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}

			$order_item_fields = array();


			$order_item_fields[] = "order_item_real_system_price = " . $value;

			if(count($order_currency) > 0)
			{
				$system_price = $value;
				if($order_currency["exchange_rate"] > 0)
				{
					$client_price = $system_price / $order_currency["exchange_rate"] * $order_currency["factor"];
				}
				else
				{
					$client_price = 0;
				}
				$order_item_fields[] = "order_item_real_client_price = " . $client_price;
			}


			$value1 = "current_timestamp";
			$project_fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = dbquote($_SESSION["user_login"]);
				$project_fields[] = "user_modified = " . $value1;
			}
	   
			$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
			mysql_query($sql) or dberror($sql);

			//add tracking information
			if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_real_system_price"])
			{
				 $result = track_change_in_order_items('Change cost group in CMS', 'Real Cost', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], "CHF " . $old_values[$key]["order_item_real_system_price"], "CHF " . $value);
			}

		}
	}

	foreach ($list->values("order_item_cms_remark") as $key=>$value)
    {
        // update record
    
        $order_item_fields = array();

        
		$order_item_fields[] = "order_item_cms_remark = " . dbquote($value);

        $value1 = "current_timestamp";
        $project_fields[] = "date_modified = " . $value1;

        if (isset($_SESSION["user_login"]))
        {
            $value1 = dbquote($_SESSION["user_login"]);
            $project_fields[] = "user_modified = " . $value1;
        }
   
        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_cms_remark"])
		{
			 $result = track_change_in_order_items('Change cost group in CMS', 'Remark', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_cms_remark"], $value);
		}
    }


	/*
	foreach ($list->values("order_item_system_price_freezed") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}

        $order_item_fields = array();

		$order_item_fields[] = "order_item_system_price_freezed = " . $value;
		$order_item_fields[] = "order_item_client_price_freezed = " . $value;

        $value1 = "current_timestamp";
        $project_fields[] = "date_modified = " . $value1;

        if (isset($_SESSION["user_login"]))
        {
            $value1 = dbquote($_SESSION["user_login"]);
            $project_fields[] = "user_modified = " . $value1;
        }
   
        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

    }
	*/

    /*
	if($project["order_date"] > "2005-10-31")
    {
        //budget approved
        foreach ($list->values("order_item_system_price_freezed") as $key=>$value)
        {
            // update record
            if(!$value) {$value = 0;}

            $order_item_fields = array();

            $order_item_fields[] = "order_item_system_price_freezed = " . $value;

            $value1 = "current_timestamp";
            $project_fields[] = "date_modified = " . $value1;

            if (isset($_SESSION["user_login"]))
            {
                $value1 = dbquote($_SESSION["user_login"]);
                $project_fields[] = "user_modified = " . $value1;
            }
       
            $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
            mysql_query($sql) or dberror($sql);
        }
    }
	*/
}



/********************************************************************
   update cost monitoring information for items
*********************************************************************/
function  project_update_order_item_cost3($list, $order_currency = array(), $show_budget_in_loc = false)
{
    
	

	if($show_budget_in_loc == true)
	{
		//get old values of order_items
		$old_values = array();
		foreach ($list->values("order_items_in_project_real_client_price") as $key=>$value)
		{
			$sql = "select order_items_in_project_project_order_id, order_item_type, order_item_cost_group, " .
				   "order_item_item, order_item_text, order_item_quantity, order_items_in_project_real_client_price, " .
				   " order_item_cms_remark, order_items_costmonitoring_group " . 
				   " from order_items " .
				   " left join order_items_in_projects on order_items_in_project_order_item_id = order_item_id " .
				   " where order_items_in_project_id = " . $key;

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$old_values[$key] = $row;
			}
		}
		
		foreach ($list->values("order_items_in_project_real_client_price") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}
			$order_item_fields = array();

			$order_item_fields[] = "order_items_in_project_real_client_price = " . $value;

			if(count($order_currency) > 0)
			{
				$client_price = $value;
				if($order_currency["exchange_rate"] > 0)
				{
					$system_price = $client_price * $order_currency["exchange_rate"] / $order_currency["factor"];
				}
				else
				{
					$system_price = 0;
				}
				$order_item_fields[] = "order_items_in_project_real_price = " . $system_price;
			}

			$sql = "update order_items_in_projects set " . join(", ", $order_item_fields) . " where order_items_in_project_id = " . $key;
			mysql_query($sql) or dberror($sql);

			//add tracking information
			if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_items_in_project_real_client_price"])
			{
				 $result = track_change_in_order_items('Change cost group in CMS', 'Real Cost', $old_values[$key]["order_items_in_project_project_order_id"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $order_currency["symbol"] . " " . $old_values[$key]["order_items_in_project_real_client_price"], $order_currency["symbol"] . " " . $value);
			}
		}
	}
	else
	{
		//get old values of order_items
		$old_values = array();
		foreach ($list->values("order_items_in_project_real_price") as $key=>$value)
		{
			$sql = "select order_items_in_project_project_order_id, order_item_type, order_item_cost_group, " .
				   "order_item_item, order_item_text, order_item_quantity, order_items_in_project_real_price, " .
				   " order_item_cms_remark, order_items_costmonitoring_group " . 
				   " from order_items " .
				   " left join order_items_in_projects on order_items_in_project_order_item_id = order_item_id " .
				   " where order_items_in_project_id = " . $key;
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$old_values[$key] = $row;
			}
		}
		
		foreach ($list->values("order_items_in_project_real_price") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}
			$order_item_fields = array();

			$order_item_fields[] = "order_items_in_project_real_price = " . $value;

			if(count($order_currency) > 0)
			{
				$system_price = $value;
				if($order_currency["exchange_rate"] > 0)
				{
					$client_price = $system_price / $order_currency["exchange_rate"] * $order_currency["factor"];
				}
				else
				{
					$client_price = 0;
				}
				$order_item_fields[] = "order_items_in_project_real_client_price = " . $client_price;
			}

			$sql = "update order_items_in_projects set " . join(", ", $order_item_fields) . " where order_items_in_project_id = " . $key;
			mysql_query($sql) or dberror($sql);

			//echo $sql . "<br />";


			//add tracking information
			if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_items_in_project_real_price"])
			{
				 $result = track_change_in_order_items('Change cost group in CMS', 'Real Cost', $old_values[$key]["order_items_in_project_project_order_id"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], "CHF " . $old_values[$key]["order_items_in_project_real_price"], "CHF " . $value);
			}
		}
	}


	foreach ($list->values("order_items_in_project_cms_remark") as $key=>$value)
	{
		// update record
		$order_item_fields = array();

		$order_item_fields[] = "order_items_in_project_cms_remark = " . dbquote($value);
		
		$sql = "update order_items_in_projects set " . join(", ", $order_item_fields) . " where order_items_in_project_id = " . $key;
		mysql_query($sql) or dberror($sql);

	}
}



/********************************************************************
   update cost monitoring information for items: reinvoice data
*********************************************************************/
function  project_update_order_item_reinvoice_data_0($list)
{
    
    //get old values of order_items
	$old_values = array();
	foreach ($list->values("order_item_reinvoiced") as $key=>$value)
    {
		$sql = "select order_item_order, order_item_type, order_item_item, " .
			   " order_item_text, order_item_reinvoiced, order_item_reinvoicenbr, " .
			   " order_item_reinvoiced2, order_item_reinvoicenbr2 " . 
			   " from order_items " .
			   " where order_item_id = " . $key;
		
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$old_values[$key] = $row;
		}
	}
	
	$pos = array();
    $sups = array();

    $sql = $list->sql;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $pos[] = $row["order_item_po_number"];
        $sups[] = $row["order_item_supplier_address"];

    }
    
    $i = 0;
    foreach ($list->values("order_item_reinvoiced") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();
                if($value)
                {
                    $order_item_fields[] = "order_item_reinvoiced = " . dbquote(from_system_date($value));
				}
				else
			    {
					$order_item_fields[] = "order_item_reinvoiced = NULL";
			    }

                $sql = "update order_items set " . join(", ", $order_item_fields) . 
					" where order_item_po_number = " . dbquote($pos[$i]) . 
					" and order_item_supplier_address = " . dbquote($sups[$i]);
                mysql_query($sql) or dberror($sql);


				//add tracking information
				if(array_key_exists($key, $old_values) and from_system_date($value) != $old_values[$key]["order_item_reinvoiced"])
				{
					 $result = track_change_in_order_items('Change reinvoice date in CMS', 'Reinvoice Date', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoiced"], from_system_date($value));
				}

           }
            $i++;
    }

    $i = 0;
    foreach ($list->values("order_item_reinvoicenbr") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();

                $order_item_fields[] = "order_item_reinvoicenbr = " . dbquote(trim($value));

                $sql = "update order_items set " . join(", ", $order_item_fields) . 
                " where order_item_po_number = " . dbquote($pos[$i]) . 
                " and order_item_supplier_address = " . dbquote($sups[$i]);
                mysql_query($sql) or dberror($sql);


				//add tracking information
				if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_reinvoicenbr"])
				{
					 $result = track_change_in_order_items('Change reinvoice number in CMS', 'Reinvoice Number', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoicenbr"], $value);
				}
           }
            $i++;
    }


	$i = 0;
    foreach ($list->values("order_item_reinvoiced2") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();
                if($value)
                {
                    $order_item_fields[] = "order_item_reinvoiced2 = " . dbquote(from_system_date($value));
				}
				else
			    {
					$order_item_fields[] = "order_item_reinvoiced2 = NULL";
			    }

                $sql = "update order_items set " . join(", ", $order_item_fields) . 
					" where order_item_po_number = " . dbquote($pos[$i]) . 
					" and order_item_supplier_address = " . dbquote($sups[$i]);
			    mysql_query($sql) or dberror($sql);

				//add tracking information
				if(array_key_exists($key, $old_values) and from_system_date($value) != $old_values[$key]["order_item_reinvoiced2"])
				{
					 $result = track_change_in_order_items('Change reinvoice date in CMS', 'Reinvoice Date', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoiced2"], from_system_date($value));
				}

           }
            $i++;
    }

    $i = 0;
    foreach ($list->values("order_item_reinvoicenbr2") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();

                $order_item_fields[] = "order_item_reinvoicenbr2 = " . dbquote(trim($value));

                $sql = "update order_items set " . join(", ", $order_item_fields) . 
                " where order_item_po_number = " . dbquote($pos[$i]) . 
                " and order_item_supplier_address = " . dbquote($sups[$i]);
                mysql_query($sql) or dberror($sql);

				//add tracking information
				if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_reinvoicenbr2"])
				{
					 $result = track_change_in_order_items('Change reinvoice number in CMS', 'Reinvoice Number', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoicenbr2"], $value);
				}
           }
            $i++;
    }
}


/********************************************************************
   update cost monitoring information for items: reinvoice data
*********************************************************************/
function  project_update_order_item_reinvoice_data($list)
{
    
    //get old values of order_items
	$old_values = array();
	foreach ($list->values("order_item_reinvoiced") as $key=>$value)
    {
		$sql = "select order_item_order, order_item_type, order_item_item, " .
			   " order_item_text, order_item_reinvoiced, order_item_reinvoicenbr, " .
			   " order_item_reinvoiced2, order_item_reinvoicenbr2 " . 
			   " from order_items " .
			   " where order_item_id = " . $key;
		
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$old_values[$key] = $row;
		}
	}
	
	foreach ($list->values("order_item_reinvoiced") as $key=>$value)
    {
        // update record
        
            $order_item_fields = array();

            if($value)
			{
				$order_item_fields[] = "order_item_reinvoiced = " . dbquote(from_system_date($value));
			}
			else
			{
				$order_item_fields[] = "order_item_reinvoiced = NULL";
			}

			$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
			mysql_query($sql) or dberror($sql);

			//add tracking information
			if(array_key_exists($key, $old_values) and from_system_date($value) != $old_values[$key]["order_item_reinvoiced"])
			{
				 $result = track_change_in_order_items('Change reinvoice date in CMS', 'Reinvoice Date', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoiced"], from_system_date($value));
			}

    }

    foreach ($list->values("order_item_reinvoicenbr") as $key=>$value)
    {
        // update record
        $order_item_fields = array();

        $order_item_fields[] = "order_item_reinvoicenbr = " . dbquote(trim($value));

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_reinvoicenbr"])
		{
			 $result = track_change_in_order_items('Change reinvoice number in CMS', 'Reinvoice Number', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoicenbr"], $value);
		}
    }

	foreach ($list->values("order_item_reinvoiced2") as $key=>$value)
    {
        // update record
        
            $order_item_fields = array();

            if($value)
			{
				$order_item_fields[] = "order_item_reinvoiced2 = " . dbquote(from_system_date($value));
			}
			else
			{
				$order_item_fields[] = "order_item_reinvoiced2 = NULL";
			}

            $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
            mysql_query($sql) or dberror($sql);

			//add tracking information
			if(array_key_exists($key, $old_values) and from_system_date($value) != $old_values[$key]["order_item_reinvoiced2"])
			{
				 $result = track_change_in_order_items('Change reinvoice date in CMS', 'Reinvoice Date', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoiced2"], from_system_date($value));
			}
    }

    foreach ($list->values("order_item_reinvoicenbr2") as $key=>$value)
    {
        // update record
        $order_item_fields = array();

        $order_item_fields[] = "order_item_reinvoicenbr2 = " . dbquote(trim($value));

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_reinvoicenbr2"])
		{
			 $result = track_change_in_order_items('Change reinvoice number in CMS', 'Reinvoice Number', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoicenbr2"], $value);
		}
    }

}

/********************************************************************
   update cost monitoring information for items: cost group data
*********************************************************************/
function  project_update_order_item_group_data($list, $order_id)
{
	//get old values of order_items
	$old_values = array();
	foreach ($list->values("order_items_monitoring_group") as $key=>$value)
    {
		$sql = "select order_item_order, order_item_type, order_item_item, " .
			   " order_item_text, order_items_monitoring_group " . 
			   " from order_items " .
			   " where order_item_id = " . $key;
		
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$old_values[$key] = $row;
		}
	}
    
	foreach ($list->values("order_items_monitoring_group") as $key=>$value)
    {

        // update record
        $order_item_fields = array();

        if($value)
		{
			$order_item_fields[] = "order_items_monitoring_group = " . dbquote($value);
		}
		else
		{
			$order_item_fields[] = "order_items_monitoring_group = NULL";
		}

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_items_monitoring_group"])
		{
			 $result = track_change_in_order_items('Change grouping in CMS', 'Group Name', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_items_monitoring_group"], $value);
		}
    }

    // update all empty fields with th entry the user made for the sam supplier

    $sql = "select distinct order_item_supplier_address, " .
           "order_items_monitoring_group  " .
           "from order_items " .
           "where order_item_order = " . $order_id .
           "   and order_items_monitoring_group <>'' ";

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $monitoring_group = $row["order_items_monitoring_group"];
        $supplier = $row["order_item_supplier_address"];

        $order_item_fields = array();

        $order_item_fields[] = "order_items_monitoring_group = " . dbquote($monitoring_group);

        $sql = "update order_items set " . join(", ", $order_item_fields) . 
        "  where order_item_order = " . $order_id .
        "   and order_item_supplier_address = " . $supplier . 
        "   and (order_items_monitoring_group  = '' or order_items_monitoring_group is null)";


        mysql_query($sql) or dberror($sql);
    }

}

/********************************************************************
    freeze budget
*********************************************************************/
function freeze_budget($order_id)
{
    
    $budget = 0;

    $sql =  "select order_item_id, order_item_quantity, " .
            "order_item_system_price, order_item_client_price " .
            "from order_items " . 
            " where order_item_not_in_budget <> 1 " .
            "  and order_item_order = " . $order_id;

    $res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
    {
        if($row["order_item_quantity"] > 0)
        {
            $budget = $budget + $row["order_item_quantity"] * $row["order_item_system_price"];
        }
        else
        {
            $budget = $budget + $row["order_item_system_price"];
        }

        $quantity = $row["order_item_quantity"];
        $sprice = $row["order_item_system_price"];
        $cprice = $row["order_item_client_price"];

        if(!$quantity){$quantity = 0;}
        if(!$sprice){$sprice = 0;}
        if(!$cprice){$cprice = 0;}

        $sql_u = "Update order_items set " .
                 "order_item_in_freezed_budget = 1, " .
                 "order_item_quantity_freezed = " . $quantity  . ", " .
                 "order_item_system_price_freezed = " . $sprice . ", " .
                 "order_item_client_price_freezed = " . $cprice . " " .
                 "where order_item_id = " . $row["order_item_id"];
        
        $result = mysql_query($sql_u) or dberror($sql_u);
    }


    //check if the cost monitoring sheet exists already
    $sql = "select count(project_cost_id) as num_recs " .
           "from project_costs " .
           "where project_cost_order = "  . $order_id; 

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);

    if($row["num_recs"] == 0)
    {
        
        $fields = array();
        $values = array();

        $fields[] = "project_cost_order";
        $values[] = $order_id;

        $fields[] = "date_created";
        $values[] = dbquote(date("Y-m-d"));

        $fields[] = "date_modified";
        $values[] = dbquote(date("Y-m-d"));

        $fields[] = "user_created";
        $values[] = dbquote(user_login());

        $fields[] = "user_modified";
        $values[] = dbquote(user_login());
        
        $sql = "insert into project_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

        mysql_query($sql) or dberror($sql);
    }


    $sql = "update project_costs " .
           "set project_cost_budget =  " . $budget . 
           " where project_cost_order = " . $order_id;

    mysql_query($sql) or dberror($sql);


    $sql = "update orders " .
           "set order_budget_freezed_date =  " . dbquote(date("Y-m-d")) . 
           " where order_id = " . $order_id;

    mysql_query($sql) or dberror($sql);


	//update user_tracking
	$text = "User has freezed budget of archived order " . $order_id;
	$sql = 'insert into user_tracking (user_tracking_user, user_tracking_track, date_created, user_created) VALUES (' .
		   user_id() . ', "' . $text . '", "' . date("Y-m-d H:i:s") . '", "' . user_login() . '")';

	mysql_query($sql) or dberror($sql);
}


/********************************************************************
   update project client data
*********************************************************************/
function  project_update_client_data($form,$design_objectives_listbox_names,$design_objectives_checklist_names)
{

    $order_fields = array();

    $delivery_address_fields = array();

    $project_fields = array();

    $project_item_fields = array();
    $project_item_values = array();


	//update table posorders or posorderspipeline
	$sql = "select order_number, project_projectkind from orders " . 
		   "left join projects on project_order = order_id " . 
		   "where order_id = " . $form->value("oid");

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$old_project_number = $row["order_number"];
		$old_projectkind = $row["project_projectkind"];

		$year_of_order = substr($form->value("project_number"),0,2);
		if($year_of_order > 30 and $year_of_order <= 99)
		{
			$year_of_order = "19" . $year_of_order;
		}
		else
		{
			$year_of_order = substr($year_of_order, 0, 1) . '00' . substr($year_of_order, 1, 1);
		}

		if(substr($form->value("project_number"),3,1) == '.')
		{
			$year_of_order = substr($form->value("project_number"),0,1) . '0' . substr($form->value("project_number"),1,2);
		}


		$sql = "update posorders set " .
			   "posorder_ordernumber = " . dbquote($form->value("project_number")) . ", " . 
			   "posorder_year = " . dbquote($year_of_order) . ", " . 
			   "posorder_product_line = " . dbquote($form->value("product_line")) . ", " . 
			   "posorder_postype = " . dbquote($form->value("project_postype")) . ", " . 
			   "posorder_subclass = " . dbquote($form->value("project_pos_subclass")) . ", " . 
			   "posorder_project_kind = " . dbquote($form->value("project_projectkind")) . ", " . 
			   "posorder_legal_type = " . dbquote($form->value("project_cost_type")) . ", " .
			   "posorder_ordernumber = " . dbquote($form->value("project_number")) .
			   " where posorder_ordernumber = " . dbquote($old_project_number);


		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posorderspipeline set " .
			   "posorder_ordernumber = " . dbquote($form->value("project_number")) . ", " . 
			   "posorder_year = " . dbquote($year_of_order) . ", " . 
			   "posorder_product_line = " . dbquote($form->value("product_line")) . ", " . 
			   "posorder_postype = " . dbquote($form->value("project_postype")) . ", " . 
			   "posorder_subclass = " . dbquote($form->value("project_pos_subclass")) . ", " . 
			   "posorder_project_kind = " . dbquote($form->value("project_projectkind")) . ", " . 
			   "posorder_legal_type = " . dbquote($form->value("project_cost_type")) . ", " .
			   "posorder_ordernumber = " . dbquote($form->value("project_number")) . 
			   " where posorder_ordernumber = " . dbquote($old_project_number);

		$result = mysql_query($sql) or dberror($sql);
	}


    // update record in table orders
    $value = trim($form->value("project_number")) == "" ? "null" : dbquote($form->value("project_number"));
    $order_fields[] = "order_number = " . $value;

    $value = $form->value("status");
    $order_fields[] = "order_actual_order_state_code = " . $value;

    $value = $form->value("client_address_id");
    $order_fields[] = "order_client_address = " . $value;

    $value = $form->value("client_address_user_id");
    $order_fields[] = "order_user = " . $value;

    $value = trim($form->value("preferred_transportation_arranged")) == "" ? "null" : dbquote($form->value("preferred_transportation_arranged"));
    $order_fields[] = "order_preferred_transportation_arranged = " . $value;

	$value = trim($form->value("preferred_transportation_mode")) == "" ? "null" : dbquote($form->value("preferred_transportation_mode"));
    $order_fields[] = "order_preferred_transportation_mode = " . $value;

    $value = trim($form->value("voltage")) == "" ? "null" : dbquote($form->value("voltage"));
    $order_fields[] = "order_voltage = " . $value;

    //$value = dbquote(from_system_date($form->value("preferred_delivery_date")), true);
    //$order_fields[] = "order_preferred_delivery_date = " . $value;


    $value = trim($form->value("full_delivery")) == "" ? "null" : dbquote($form->value("full_delivery"));
    $order_fields[] = "order_full_delivery = " . $value;

    //$value = trim($form->value("packaging_retraction")) == "" ? "null" : dbquote($form->value("packaging_retraction"));
    //$order_fields[] = "order_packaging_retraction = " . $value;

    $value = trim($form->value("pedestrian_mall_approval")) == "" ? "null" : dbquote($form->value("pedestrian_mall_approval"));
    $order_fields[] = "order_pedestrian_mall_approval = " . $value;

    $value = trim($form->value("delivery_comments")) == "" ? "null" : dbquote($form->value("delivery_comments"));
    $order_fields[] = "order_delivery_comments = " . $value;

	$value = dbquote($form->value("order_insurance"));
    $order_fields[] = "order_insurance = " . $value;

	$value = "current_timestamp";
    $order_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");

    mysql_query($sql) or dberror($sql);

    
	    
    // update record in table projects

    $value = trim($form->value("project_number")) == "" ? "null" : dbquote($form->value("project_number"));
    $project_fields[] = "project_number = " . $value;

	$value = trim($form->value("project_postype")) == "" ? "0" : dbquote($form->value("project_postype"));
    $project_fields[] = "project_postype = " . $value;


	$value = trim($form->value("project_pos_subclass")) == "" ? "0" : dbquote($form->value("project_pos_subclass"));
    $project_fields[] = "project_pos_subclass = " . $value;


    $value = trim($form->value("project_state")) == "" ? "1" : dbquote($form->value("project_state"));
    $project_fields[] = "project_state = " . $value;

    $value = trim($form->value("project_projectkind")) == "" ? "null" :
    dbquote($form->value("project_projectkind"));
    $project_fields[] = "project_projectkind = " . $value;

    $value = trim($form->value("product_line")) == "" ? "null" : dbquote($form->value("product_line"));
    $project_fields[] = "project_product_line = " . $value;

    $value = dbquote($form->value("location_type"));
    $project_fields[] = "project_location_type = " . $value;

    $value = trim($form->value("location_type_other")) == "" ? "null" : dbquote($form->value("location_type_other"));
    $project_fields[] = "project_location = " . $value;

    $value = trim($form->value("approximate_budget")) == "" ? "null" : dbquote($form->value("approximate_budget"));
    $project_fields[] = "project_approximate_budget = " . $value;

    $value = dbquote(from_system_date($form->value("planned_opening_date")), true);
    $project_fields[] = "project_planned_opening_date = " . $value;

	$value = dbquote(from_system_date($form->value("planned_closing_date")), true);
    $project_fields[] = "project_planned_closing_date = " . $value;

	$value = dbquote(from_system_date($form->value("planned_takeover_date")), true);
    $project_fields[] = "project_planned_takeover_date = " . $value;

    /*
	$value = trim($form->value("watches_displayed")) == "" ? "null" : dbquote($form->value("watches_displayed"));
    $project_fields[] = "project_watches_displayed = " . $value;

    $value = trim($form->value("watches_stored")) == "" ? "null" : dbquote($form->value("watches_stored"));
    $project_fields[] = "project_watches_stored = " . $value;

	$value = trim($form->value("bijoux_displayed")) == "" ? "null" : dbquote($form->value("bijoux_displayed"));
    $project_fields[] = "project_bijoux_displayed = " . $value;

    $value = trim($form->value("bijoux_stored")) == "" ? "null" : dbquote($form->value("bijoux_stored"));
    $project_fields[] = "project_bijoux_stored = " . $value;
	*/

    $value = trim($form->value("comments")) == "" ? "null" : dbquote($form->value("comments"));
    $project_fields[] = "project_comments = " . $value;

	
	$value = trim($form->value("project_relocated_posaddress_id")) == "" ? "0" : dbquote($form->value("project_relocated_posaddress_id"));
    $project_fields[] = "project_relocated_posaddress_id = " . $value;


    $value = "current_timestamp";
    $project_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $project_fields[] = "user_modified = " . $value;
    }

    $sql = "update projects set " . join(", ", $project_fields) . " where project_id = " . $form->value("pid");
    mysql_query($sql) or dberror($sql);

    $_REQUEST["id"] = $form->value("pid");

    // delete all records form project_items belonign to the project
    $sql = "delete from project_items where project_item_project = " . $form->value("pid");
    mysql_query($sql) or dberror($sql);

    // insert records into table project_items
    foreach ($design_objectives_listbox_names as $element)
    {
        $project_item_fields[0] = "project_item_project";
        $project_item_values[0] = $form->value("pid");

        $project_item_fields[1] = "project_item_item";
        $project_item_values[1] = $form->value($element);
        
        $project_item_fields[2] = "date_created";
        $project_item_values[2] = "current_timestamp";

        $project_item_fields[3] = "date_modified";
        $project_item_values[3] = "current_timestamp";
                
        if (isset($_SESSION["user_login"]))
        {
            $project_item_fields[4] = "user_created";
            $project_item_values[4] = dbquote($_SESSION["user_login"]);

            $project_item_fields[5] = "user_modified";
            $project_item_values[5] = dbquote($_SESSION["user_login"]);
        }
        $sql = "insert into project_items (" . join(", ", $project_item_fields) . ") values (" . join(", ", $project_item_values) . ")";
        mysql_query($sql) or dberror($sql);
    }
    

    foreach ($design_objectives_checklist_names as $name)
    {
        $tmp = $form->value($name);
        foreach ($tmp as $value)
        {
        $project_item_fields[0] = "project_item_project";
        $project_item_values[0] =$form->value("pid");

        $project_item_fields[1] = "project_item_item";
        $project_item_values[1] = $value;
        
        $project_item_fields[2] = "date_created";
        $project_item_values[2] = "current_timestamp";

        $project_item_fields[3] = "date_modified";
        $project_item_values[3] = "current_timestamp";
                
        if (isset($_SESSION["user_login"]))
        {
            $project_item_fields[4] = "user_created";
            $project_item_values[4] = dbquote($_SESSION["user_login"]);

            $project_item_fields[5] = "user_modified";
            $project_item_values[5] = dbquote($_SESSION["user_login"]);
        }
        $sql = "insert into project_items (" . join(", ", $project_item_fields) . ") values (" . join(", ", $project_item_values) . ")";
        mysql_query($sql) or dberror($sql);
        }
    }


    // update record in table project_costs
    $value = dbquote($form->value("store_retailarea"));
    $project_cost_fields[] = "project_cost_sqms = " . $value;

	//$value = dbquote($form->value("store_grosssurface"));
    //$project_cost_fields[] = "project_cost_grosssqms = " . $value;

	$value = dbquote($form->value("store_totalsurface"));
    $project_cost_fields[] = "project_cost_totalsqms = " . $value;

	$value = dbquote($form->value("store_backoffice"));
    $project_cost_fields[] = "project_cost_backofficesqms = " . $value;

	/*
	$value = dbquote($form->value("store_floorsurface1"));
    $project_cost_fields[] = "project_cost_floorsurface1 = " . $value;

	$value = dbquote($form->value("store_floorsurface2"));
    $project_cost_fields[] = "project_cost_floorsurface2 = " . $value;

	$value = dbquote($form->value("store_floorsurface3"));
    $project_cost_fields[] = "project_cost_floorsurface3 = " . $value;

	$value = dbquote($form->value("store_numfloors"));
    $project_cost_fields[] = "project_cost_numfloors = " . $value;
	*/
	
	$value = dbquote($form->value("project_cost_type"));
    $project_cost_fields[] = "project_cost_type = " . $value;

    $value = "current_timestamp";
    $project_cost_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $project_cost_fields[] = "user_modified = " . $value;
    }

    $sql = "update project_costs set " . join(", ", $project_cost_fields) . " where project_cost_order = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);


	
	return true;

}

/********************************************************************
    append records to table order_mails
*********************************************************************/
function append_mail($order_id, $recepient_id, $user_id, $text, $order_state_code, $type, $order_revision=0, $order_mail_is_cc = 0, $table = "", $table_key = 0)
{

    $text = str_replace("\n\n", "\n", $text);
	$text = str_replace("\n\n", "\n", $text);

	$sql = "select order_state_id " .
           "from order_states ".
           "left join order_state_groups on order_state_group = order_state_group_id ".
           "where order_state_code = " . dbquote($order_state_code) .
           "    and order_state_group_order_type = " .$type;

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);

    $order_mail_fields = array();
    $order_mail_values = array();
    
    // insert record into table tasks

    $order_mail_fields[] = "order_mail_order";
    $order_mail_values[] = $order_id;

	if ($recepient_id != "")
    {
        $order_mail_fields[] = "order_mail_table";
        $order_mail_values[] = dbquote($table);
    }

	if ($recepient_id != "")
    {
        $order_mail_fields[] = "order_mail_table_key";
        $order_mail_values[] = dbquote($table_key);
    }

    if ($recepient_id != "")
    {
        $order_mail_fields[] = "order_mail_user";
        $order_mail_values[] = $recepient_id;
    }

    $order_mail_fields[] = "order_mail_from_user";
    $order_mail_values[] = $user_id;

    $order_mail_fields[] = "order_mail_text";
    $order_mail_values[] = dbquote($text);

    if ($order_state_code != "")
    {
        $order_mail_fields[] = "order_mail_order_state";
        $order_mail_values[] = $row["order_state_id"];
    }

	$order_mail_fields[] = "order_revisioned";
    $order_mail_values[] = $order_revision;

	$order_mail_fields[] = "order_mail_is_cc";
    $order_mail_values[] = $order_mail_is_cc;

	

    $order_mail_fields[] = "date_created";
    $order_mail_values[] = "current_timestamp";

    $order_mail_fields[] = "date_modified";
    $order_mail_values[] = "current_timestamp";

    if (isset($_SESSION["user_login"]))
    {
        $order_mail_fields[] = "user_created";
        $order_mail_values[] = dbquote($_SESSION["user_login"]);

        $order_mail_fields[] = "user_modified";
        $order_mail_values[] = dbquote($_SESSION["user_login"]);
    }

    $sql = "insert into order_mails (" . join(", ", $order_mail_fields) . ") values (" . join(", ", $order_mail_values) . ")";
        mysql_query($sql) or dberror($sql);

}



/***********************************************************************
   save suppliers currency and pricing data and
   copy item information into all other item records of the same order
   belonging to the same supplier
************************************************************************/
function  project_edit_order_item_save($form)
{
		$order_item_fields = array();

        $value =  trim($form->value("order_item_text")) == "" ? "null" : dbquote($form->value("order_item_text"));
        $order_item_fields[] = "order_item_text = " . $value;

        if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SERVICES or $form->value("order_item_type") == ITEM_TYPE_SPECIAL)
        {

            $value =  trim($form->value("order_item_quantity")) == "" ? "null" : dbquote($form->value("order_item_quantity"));
            $order_item_fields[] = "order_item_quantity = " . $value;
    
            // get suppliers' currency data
            $supplier_currency_id = null;
            $supplier_currency_exchangerate = null;

            
            if ($form->value("order_item_currency"))
            {
                $currency = get_currency($form->value("order_item_currency"));
                $supplier_currency_id = $currency["id"];
                $supplier_currency_exchangerate = $currency["exchange_rate"];
            }
            else
            {
                $currency = get_address_currency($form->value("order_item_supplier_address"));
                $supplier_currency_id = $currency["id"];
                $supplier_currency_exchangerate = $currency["exchange_rate"];
            }

            
            $order_item_fields[] = "order_item_supplier_currency = " . $supplier_currency_id;

            $order_item_fields[] = "order_item_supplier_exchange_rate = " . $supplier_currency_exchangerate;

            if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SERVICES)
            {
                $value =  trim($form->value("order_item_client_price")) == "" ? "null" : dbquote($form->value("order_item_client_price"));
                $order_item_fields[] = "order_item_client_price = " . $value;

                // update supplier's price
                $sql = "select * ".
                       "from suppliers ".
                       "where supplier_address = " . $form->value("order_item_supplier_address") .
                       "    and supplier_item = " . $form->value("order_item_item");

                $res = mysql_query($sql) or dberror($sql);
                if ($row = mysql_fetch_assoc($res))
                {
                    $order_item_fields[] = "order_item_supplier_price = " . $row["supplier_item_price"];
                }
            }
            else if ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
            {
                
				if(isset($form->items["order_item_cost_group"]))
				{
					$value = $form->value("order_item_cost_group");
					$order_item_fields[] = "order_item_cost_group = " . $value;

					/* cost monitoring group was removed by Katrin Eckert on January 26th 2016
					$value = $form->value("order_items_costmonitoring_group");
					$order_item_fields[] = "order_items_costmonitoring_group = " . $value;
					*/
				}

				$value =  trim($form->value("order_item_system_price")) == "" ? "null" : dbquote($form->value("order_item_system_price"));
                $order_item_fields[] = "order_item_system_price = " . $value;

                $order_currency = get_order_currency($form->value("oid"));
                $value = $form->value("order_item_system_price") / $order_currency["exchange_rate"] * $order_currency["factor"];
                $order_item_fields[] = "order_item_client_price = " . $value;

                $value =  trim($form->value("order_item_supplier_price")) == "" ? "null" : dbquote($form->value("order_item_supplier_price"));
                $order_item_fields[] = "order_item_supplier_price = " . $value;


                $value =  trim($form->value("order_item_supplier_item_code")) == "" ? "null" : dbquote($form->value("order_item_supplier_item_code"));
                $order_item_fields[] = "order_item_supplier_item_code = " . $value;

                $value =  trim($form->value("order_item_offer_number")) == "" ? "null" : dbquote($form->value("order_item_offer_number"));
                $order_item_fields[] = "order_item_offer_number = " . $value;

                $value =  trim($form->value("order_item_production_time")) == "" ? "null" : dbquote($form->value("order_item_production_time"));
                $order_item_fields[] = "order_item_production_time = " . $value;

            }

            $value =  trim($form->value("order_item_cost_unit_number")) == "" ? "null" : dbquote($form->value("order_item_cost_unit_number"));
            $order_item_fields[] = "order_item_cost_unit_number = " . $value;

            $value =  trim($form->value("order_item_po_number")) == "" ? "null" : dbquote($form->value("order_item_po_number"));
            $order_item_fields[] = "order_item_po_number = " . $value;

            $value =  trim($form->value("order_item_supplier_address")) == "" ? "null" : dbquote($form->value("order_item_supplier_address"));
            $order_item_fields[] = "order_item_supplier_address = " . $value;


            if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SPECIAL)
			{
				$value =  trim($form->value("order_item_transportation")) == "" ? "null" : dbquote($form->value("order_item_transportation"));
				$order_item_fields[] = "order_item_transportation = " . $value;

				$value =  trim($form->value("order_item_forwarder_address")) == "" ? "null" : dbquote($form->value("order_item_forwarder_address"));
				$order_item_fields[] = "order_item_forwarder_address = " . $value;

				$value =  trim($form->value("order_item_staff_for_discharge")) == "" ? "null" :     dbquote($form->value("order_item_staff_for_discharge"));
				$order_item_fields[] = "order_item_staff_for_discharge = " . $value;

				$value =  trim($form->value("order_item_no_offer_required")) == "" ? "null" : dbquote($form->value("order_item_no_offer_required"));
				$order_item_fields[] = "order_item_no_offer_required = " . $value;

				$value =  trim($form->value("order_item_not_in_budget")) == "" ? "null" : dbquote($form->value("order_item_not_in_budget"));
				$order_item_fields[] = "order_item_not_in_budget = " . $value;
			}

        }
        else if ($form->value("order_item_type") == ITEM_TYPE_COST_ESTIMATION OR $form->value("order_item_type") == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
        {

			if(isset($form->items["order_item_cost_group"]))
			{
				$value = $form->value("order_item_cost_group");
				$order_item_fields[] = "order_item_cost_group = " . $value;

			}
			
			if(isset($form->items["order_item_supplier_freetext"]))
			{
				$value =  trim($form->value("order_item_supplier_freetext")) == "" ? "null" : dbquote($form->value("order_item_supplier_freetext"));
				$order_item_fields[] = "order_item_supplier_freetext = " . $value;
			}

            $value =  trim($form->value("order_item_system_price")) == "" ? "null" : dbquote($form->value("order_item_system_price"));
            $order_item_fields[] = "order_item_system_price = " . $value;

            $order_currency = get_order_currency($form->value("oid"));
            $value = $form->value("order_item_system_price") / $order_currency["exchange_rate"] * $order_currency["factor"];
            $order_item_fields[] = "order_item_client_price = " . $value;

        }
        
        $value = "current_timestamp";
        $order_item_fields[] = "date_modified = " . $value;
    
        if (isset($_SESSION["user_login"]))
        {
            $value = dbquote($_SESSION["user_login"]);
            $order_item_fields[] = "user_modified = " . $value;
        }

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $form->value("order_item_id");
        mysql_query($sql) or dberror($sql);


        if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SPECIAL or $form->value("order_item_type") == ITEM_TYPE_SERVICES)
        {
            // copy values into all aother order_items
            $sql_order_item = "select * ".
                              "from order_items ".
                              "where (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
                              "    and order_item_order = " . param("oid") . " " .
                              "    and order_item_supplier_address = ". $form->value("order_item_supplier_address").
                              "    and (order_item_type = " . ITEM_TYPE_STANDARD . " ".
				              "    or order_item_type = " . ITEM_TYPE_SERVICES . " ".
                              "    or order_item_type = " . ITEM_TYPE_SPECIAL . ")";
    
            $res = mysql_query($sql_order_item) or dberror($sql_order_item);
            while ($row = mysql_fetch_assoc($res))
            {
                // update all empty P.O. Numbers
                $value = trim($form->value("order_item_po_number")) == "" ? "null" : dbquote($form->value("order_item_po_number"));

                if (!$row["order_item_po_number"])
                {
                    $sql = "update order_items set order_item_po_number =" . $value . " ".
                       "where order_item_id = " . $row["order_item_id"];
                    mysql_query($sql) or dberror($sql);
                }


                // update all empty cost unit numbers
                $value = trim($form->value("order_item_cost_unit_number")) == "" ? "null" : dbquote($form->value("order_item_cost_unit_number"));

                if (!$row["order_item_cost_unit_number"])
                {
                    $sql = "update order_items set order_item_cost_unit_number =" . $value . " ".
                       "where order_item_id = " . $row["order_item_id"];
                    mysql_query($sql) or dberror($sql);
                }

                if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SPECIAL)
				{
					// update all empty forwarder addresses
					$value = trim($form->value("order_item_forwarder_address")) == "" ? "null" :     dbquote($form->value("order_item_forwarder_address"));

					if (!$row["order_item_forwarder_address"])
					{
						$sql = "update order_items set order_item_forwarder_address =" . $value . " ".
							   "where order_item_id = " . $row["order_item_id"];
						mysql_query($sql) or dberror($sql);
					}
		
					// update all transportation codes
					$value = trim($form->value("order_item_transportation")) == "" ? "null" : dbquote($form->value("order_item_transportation"));

					if (!$row["order_item_transportation"])
					{
						$sql = "update order_items set order_item_transportation =" . $value . " ".
						   "where order_item_id = " . $row["order_item_id"];
						mysql_query($sql) or dberror($sql);
					}
				}
            }
        }

}

/********************************************************************
    update budget state
*********************************************************************/
function update_budget_state($order_id, $state)
{
    $sql = "update orders set order_budget_is_locked = " . $state . " where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);

	//update user_tracking
	$text = "User has changed budget state of order " . $order_id . " to " . $state;
	$sql = 'insert into user_tracking (user_tracking_user, user_tracking_track, date_created, user_created) VALUES (' .
		   user_id() . ', "' . $text . '", "' . date("Y-m-d H:i:s") . '", "' . user_login() . '")';

	 mysql_query($sql) or dberror($sql);


}


/********************************************************************
    unfreeze project budget
*********************************************************************/
function unfreeze_project_budget($order_id)
{

    $sql = "update orders set order_budget_is_locked = 0 where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);


    $sql = "update order_items set " .
           "order_item_in_freezed_budget = 0, " .
           "order_item_quantity_freezed = '0000-00-00', " .
           "order_item_system_price_freezed = NULL, " .
           "order_item_client_price_freezed = NULL " .
           " where order_item_order = " . $order_id;

    mysql_query($sql) or dberror($sql);


    $sql = "update orders " .
           "set order_budget_freezed_date =  Null " . 
           " where order_id = " . $order_id;

    mysql_query($sql) or dberror($sql);  
	

	//update user_tracking
	$text = "User has freezed budget of order " . $order_id;
	$sql = 'insert into user_tracking (user_tracking_user, user_tracking_track, date_created, user_created) VALUES (' .
		   user_id() . ', "' . $text . '", "' . date("Y-m-d H:i:s") . '", "' . user_login() . '")';

	 mysql_query($sql) or dberror($sql);

}


/********************************************************************
    delete design objective items of a project
*********************************************************************/
function delete_design_objective_items($project_id)
{
    $sql = "delete from project_items where project_item_project = " . $project_id;
    mysql_query($sql) or dberror($sql);

}

/********************************************************************
    update project type
*********************************************************************/
function project_update_project_product_line($project_id, $project_line)
{
    $sql = "update projects set project_product_line = " . $project_line . " where project_id = " .  $project_id;
    mysql_query($sql) or dberror($sql);

}


/********************************************************************
    save attachment infos concerning visibility and roles
*********************************************************************/
function save_attachment_accessibility_info($form, $check_box_names)
{

    // get order_file_id from the inserted record
    $sql = "select order_file_id ".
           "from order_files ".
           "where order_file_order = " . $form->value("order_file_order") . " " .
           "order by date_created DESC ".
           "limit 1";

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $attachment_id = $row["order_file_id"];
    }

    // insert records into order_file_addresses
    //if (has_access("can_set_attachment_accessibility_in_orders") or has_access("can_set_attachment_accessibility_in_projects"))
    //{
    
        foreach ($check_box_names as $key=>$value)
        {
            if ($form->value($key))
            {
                $sql = "select user_address from users where user_id = " . $value;
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$address_id = $row["user_address"];

				$attachment_address_fields = array();
                $attachment_address_values = array();

                $attachment_address_fields[] = "order_file_address_file";
                $attachment_address_values[] = $attachment_id;

                $attachment_address_fields[] = "order_file_address_address";
                $attachment_address_values[] = $address_id;

                $attachment_address_fields[] = "date_created";
                $attachment_address_values[] = "current_timestamp";

                $attachment_address_fields[] = "date_modified";
                $attachment_address_values[] = "current_timestamp";
                
                if (isset($_SESSION["user_login"]))
                {
                    $attachment_address_fields[] = "user_created";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);

                    $attachment_address_fields[] = "user_modified";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);
                }
    
                $sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
                mysql_query($sql) or dberror($sql);
            }
        }
    //}
}


/********************************************************************
    update file accessibility
*********************************************************************/
function update_attachment_accessibility_info($id, $form, $check_box_names)
{
	$sql = "delete from order_file_addresses where order_file_address_file = " . $id;
    mysql_query($sql) or dberror($sql);

	foreach ($check_box_names as $key=>$value)
    {
		if ($form->value($key))
        {
			$sql = "select user_address from users where user_id = " . $value;

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			$address_id = $row["user_address"];

			
			$attachment_address_fields = array();
            $attachment_address_values = array();

            $attachment_address_fields[] = "order_file_address_file";
            $attachment_address_values[] = $id;

            $attachment_address_fields[] = "order_file_address_address";
            $attachment_address_values[] = $address_id;

            $attachment_address_fields[] = "date_created";
            $attachment_address_values[] = "current_timestamp";

            $attachment_address_fields[] = "date_modified";
            $attachment_address_values[] = "current_timestamp";
                
            if (isset($_SESSION["user_login"]))
            {
                $attachment_address_fields[] = "user_created";
                $attachment_address_values[] = dbquote($_SESSION["user_login"]);

                $attachment_address_fields[] = "user_modified";
                $attachment_address_values[] = dbquote($_SESSION["user_login"]);
            }
    
            $sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
                mysql_query($sql) or dberror($sql);
        }

    }
}

/********************************************************************
    delete attachment
*********************************************************************/
function delete_attachment($id)
{
    // delete access rights
    $sql = "delete from order_file_addresses where order_file_address_file = " . $id;
    mysql_query($sql) or dberror($sql);

    // delete file
    $sql = "select order_file_path from order_files where order_file_id = " . $id;

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $file = ".." . $row["order_file_path"];
        if (file_exists($file))
        {
            unlink($file);
        }
    }

    // delete attachment data
    $sql = "delete from order_files where order_file_id = " . $id;
    mysql_query($sql) or dberror($sql);

	// delete order shipment documents
    $sql = "update order_shipping_documents set order_shipping_document_file_order_file_id = NULL where order_shipping_document_file_order_file_id = " . $id;
    mysql_query($sql) or dberror($sql);


}


/********************************************************************
   update project client data
*********************************************************************/
function  order_update_client_data($form)
{

    $order_fields = array();
    $delivery_address_fields = array();

    // update record in table orders
    $value = $form->value("client_address_id");
    $order_fields[] = "order_client_address = " . $value;

    
    $value = $form->value("status");
    $order_fields[] = "order_actual_order_state_code = " . $value;

    $value = $form->value("client_address_user_id");
    $order_fields[] = "order_user = " . $value;

    $value = trim($form->value("billing_address_company")) == "" ? "null" : dbquote($form->value("billing_address_company"));
    $order_fields[] = "order_billing_address_company = " . $value;

     $value = trim($form->value("billing_address_company2")) == "" ? "null" : dbquote($form->value("billing_address_company2"));
    $order_fields[] = "order_billing_address_company2 = " . $value;

    $value = trim($form->value("billing_address_address")) == "" ? "null" : dbquote($form->value("billing_address_address"));
    $order_fields[] = "order_billing_address_address = " . $value;


	if(array_key_exists('bill_to_street', $form->items))
	{
		foreach($form->items['bill_to_street']['fields'] as $key=>$fieldname)
		{
			$value = dbquote($form->items['bill_to_street']['values'][$key]);
			$order_fields[] = $fieldname . " = " . $value;

		}
	}

    $value = trim($form->value("billing_address_address2")) == "" ? "null" : dbquote($form->value("billing_address_address2"));
    $order_fields[] = "order_billing_address_address2 = " . $value;

    $value = trim($form->value("billing_address_zip")) == "" ? "null" : dbquote($form->value("billing_address_zip"));
    $order_fields[] = "order_billing_address_zip = " . $value;

    $value = dbquote($form->value("billing_address_place_id"));
    $order_fields[] = "order_billing_address_place_id = " . $value;

	$value = trim($form->value("billing_address_place")) == "" ? "null" : dbquote($form->value("billing_address_place"));
    $order_fields[] = "order_billing_address_place = " . $value;

    $value = trim($form->value("billing_address_country")) == "" ? "null" : dbquote($form->value("billing_address_country"));
    $order_fields[] = "order_billing_address_country = " . $value;

    $value = trim($form->value("billing_address_phone")) == "" ? "null" : dbquote($form->value("billing_address_phone"));
    $order_fields[] = "order_billing_address_phone = " . $value;

	
	if(array_key_exists('bill_to_phone_number', $form->items))
	{
		foreach($form->items['bill_to_phone_number']['fields'] as $key=>$fieldname)
		{
			$value = dbquote($form->items['bill_to_phone_number']['values'][$key]);
			$order_fields[] = $fieldname . " = " . $value;

		}
	}

    $value = trim($form->value("billing_address_mobile_phone")) == "" ? "null" : dbquote($form->value("billing_address_mobile_phone"));
    $order_fields[] = "order_billing_address_mobile_phone = " . $value;

	if(array_key_exists('bill_to_mobile_phone_number', $form->items))
	{
		foreach($form->items['bill_to_mobile_phone_number']['fields'] as $key=>$fieldname)
		{
			$value = dbquote($form->items['bill_to_mobile_phone_number']['values'][$key]);
			$order_fields[] = $fieldname . " = " . $value;

		}
	}
	
    $value = trim($form->value("billing_address_email")) == "" ? "null" : dbquote($form->value("billing_address_email"));
    $order_fields[] = "order_billing_address_email = " . $value;

    $value = trim($form->value("billing_address_contact")) == "" ? "null" : dbquote($form->value("billing_address_contact"));
    $order_fields[] = "order_billing_address_contact = " . $value;

    $value = trim($form->value("shop_address_company")) == "" ? "null" : dbquote($form->value("shop_address_company"));
    $order_fields[] = "order_shop_address_company = " . $value;

     $value = trim($form->value("shop_address_company2")) == "" ? "null" : dbquote($form->value("shop_address_company2"));
    $order_fields[] = "order_shop_address_company2 = " . $value;

    $value = trim($form->value("shop_address_address")) == "" ? "null" : dbquote($form->value("shop_address_address"));
    $order_fields[] = "order_shop_address_address = " . $value;


	if(array_key_exists('shop_street', $form->items))
	{
		foreach($form->items['shop_street']['fields'] as $key=>$fieldname)
		{
			$value = dbquote($form->items['shop_street']['values'][$key]);
			$order_fields[] = $fieldname . " = " . $value;

		}
	}


    $value = trim($form->value("shop_address_address2")) == "" ? "null" : dbquote($form->value("shop_address_address2"));
    $order_fields[] = "order_shop_address_address2 = " . $value;

    $value = trim($form->value("shop_address_zip")) == "" ? "null" : dbquote($form->value("shop_address_zip"));
    $order_fields[] = "order_shop_address_zip = " . $value;

    $value = trim($form->value("shop_address_place")) == "" ? "null" : dbquote($form->value("shop_address_place"));
    $order_fields[] = "order_shop_address_place = " . $value;

    $value = trim($form->value("shop_address_country")) == "" ? "null" : dbquote($form->value("shop_address_country"));
    $order_fields[] = "order_shop_address_country = " . $value;

	$value = trim($form->value("shop_address_phone")) == "" ? "null" : dbquote($form->value("shop_address_phone"));
    $order_fields[] = "order_shop_address_phone = " . $value;

	if(array_key_exists('shop_phone_number', $form->items))
	{
		foreach($form->items['shop_phone_number']['fields'] as $key=>$fieldname)
		{
			$value = dbquote($form->items['shop_phone_number']['values'][$key]);
			$order_fields[] = $fieldname . " = " . $value;

		}
	}

	$value = trim($form->value("shop_address_mobile_phone")) == "" ? "null" : dbquote($form->value("shop_address_mobile_phone"));
    $order_fields[] = "order_shop_address_mobile_phone = " . $value;

	if(array_key_exists('shop_mobile_phone_number', $form->items))
	{
		foreach($form->items['shop_mobile_phone_number']['fields'] as $key=>$fieldname)
		{
			$value = dbquote($form->items['shop_mobile_phone_number']['values'][$key]);
			$order_fields[] = $fieldname . " = " . $value;

		}
	}

	$value = trim($form->value("shop_address_email")) == "" ? "null" : dbquote($form->value("shop_address_email"));
    $order_fields[] = "order_shop_address_email = " . $value;

    $value = trim($form->value("preferred_transportation_arranged")) == "" ? "null" : dbquote($form->value("preferred_transportation_arranged"));
    $order_fields[] = "order_preferred_transportation_arranged = " . $value;

	$value = trim($form->value("preferred_transportation_mode")) == "" ? "null" : dbquote($form->value("preferred_transportation_mode"));
    $order_fields[] = "order_preferred_transportation_mode = " . $value;

    $value = trim($form->value("voltage")) == "" ? "null" : dbquote($form->value("voltage"));
    $order_fields[] = "order_voltage = " . $value;

    $value = trim($form->value("preferred_delivery_date")) == "" ? "null" : dbquote(from_system_date($form->value("preferred_delivery_date")));
    $order_fields[] = "order_preferred_delivery_date = " . $value;


    $value = trim($form->value("full_delivery")) == "" ? "null" : dbquote($form->value("full_delivery"));
    $order_fields[] = "order_full_delivery = " . $value;

    //$value = trim($form->value("packaging_retraction")) == "" ? "null" : dbquote($form->value("packaging_retraction"));
    //$order_fields[] = "order_packaging_retraction = " . $value;

    $value = trim($form->value("pedestrian_mall_approval")) == "" ? "null" : dbquote($form->value("pedestrian_mall_approval"));
    $order_fields[] = "order_pedestrian_mall_approval = " . $value;

    $value = trim($form->value("delivery_comments")) == "" ? "null" : dbquote($form->value("delivery_comments"));
    $order_fields[] = "order_delivery_comments = " . $value;

	$value = dbquote($form->value("order_insurance"));
    $order_fields[] = "order_insurance = " . $value;


    $value = "current_timestamp";
    $order_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);

//echo $sql;
    
	// update POSaddresses (POS Index)
	if($form->value("posaddress_id") > 0)
	{
		$sql = "select posorder_id " . 
			   "from posorders " . 
			   "where posorder_order = " . $form->value("oid");
		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
		
			$sql = "Update posorders set " . 
				   "posorder_posaddress = " . $form->value("posaddress_id") . 
				   " where posorder_id = " . $row["posorder_id"];

			mysql_query($sql) or dberror($sql);
		}
		else
		{
			$sql = "insert into posorders (" . 
				   "posorder_posaddress, " .
				   "posorder_order, " .
				   "posorder_ordernumber, " .
				   "posorder_type) Values ( " .
				   $form->value("posaddress_id") . ", " .
				   $form->value("oid") . ", " .
				   dbquote($form->value("order_number")) . ", " .
				   2  . ")";

			$result = mysql_query($sql) or dberror($sql);
		}
	
	}


} // order_update_client_data


/********************************************************************
   update project retail data
*********************************************************************/
function  project_update_retail_data($form)
{

    $project_fields = array();
    $order_fields = array();

    // update record in table projects

	$value = dbquote($form->value("project_production_type"));
    $project_fields[] = "project_production_type = " . $value;

	$value = dbquote($form->value("project_number"));
    $project_fields[] = "project_number = " . $value;

	$value = dbquote($form->value("project_type_subclass_id"));
    $project_fields[] = "project_type_subclass_id = " . $value;

	$value = dbquote($form->value("product_line"));
    $project_fields[] = "project_product_line = " . $value;

	$value = dbquote($form->value("product_line_subclass"));
    $project_fields[] = "project_product_line_subclass = " . $value;

    $value = dbquote($form->value("retail_coordinator"));
    $project_fields[] = "project_retail_coordinator = " . $value;

	//$value = dbquote($form->value("hq_project_manager"));
    //$project_fields[] = "project_hq_project_manager = " . $value;

	$value = dbquote($form->value("local_retail_coordinator"));
    $project_fields[] = "project_local_retail_coordinator = " . $value;
    
    
	if($form->value("project_no_planning") == 1) {
		$project_fields[] = "project_design_contractor = 0";
	}
	else
	{
		$value = dbquote($form->value("contractor_user_id"));
		$project_fields[] = "project_design_contractor = " . $value;
	}
    
    
    $value = dbquote($form->value("supervisor_user_id"));
    $project_fields[] = "project_design_supervisor = " . $value;

	$value = dbquote($form->value("cms_approver_user_id"));
    $project_fields[] = "project_cms_approver = " . $value;

    $value = dbquote(from_system_date($form->value("project_real_opening_date")));
    $project_fields[] = "project_real_opening_date = " . $value;

	$value = dbquote($form->value("project_state"));
    $project_fields[] = "project_state = " . $value;

	$value = dbquote($form->value("project_no_planning"));
    $project_fields[] = "project_no_planning = " . $value;

	$value = dbquote($form->value("project_is_local_production"));
    $project_fields[] = "project_is_local_production = " . $value;

	$value = dbquote($form->value("project_furniture_type_sis"));
    $project_fields[] = "project_furniture_type_sis = " . $value;

	$value = dbquote($form->value("project_furniture_type_store"));
    $project_fields[] = "project_furniture_type_store = " . $value;

	$value = dbquote($form->value("project_uses_icedunes_visuals"));
    $project_fields[] = "project_uses_icedunes_visuals = " . $value;

	$value = dbquote($form->value("project_relocated_posaddress_id"));
    $project_fields[] = "project_relocated_posaddress_id = " . $value;

	$value = dbquote($form->value("project_budget_covered_by"));
    $project_fields[] = "project_budget_covered_by = " . $value;


    $value = dbquote($form->value("project_use_ps2004"));
    $project_fields[] = "project_use_ps2004 = " . $value;

    $value = "current_timestamp";
    $project_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $project_fields[] = "user_modified = " . $value;
    }

    $sql = "update projects set " . join(", ", $project_fields) . " where project_id = " . $form->value("pid");
    mysql_query($sql) or dberror($sql);

    $_REQUEST["id"] = $form->value("pid");


    // update record in table orders
    
    $value = dbquote($form->value("project_number"));
    $order_fields[] = "order_number = " . $value;
	
	$value = dbquote($form->value("retail_operator"));
    $order_fields[] = "order_retail_operator = " . $value;

    $value = dbquote($form->value("delivery_confirmation_by"));
    $order_fields[] = "order_delivery_confirmation_by = " . $value;

    $value = "current_timestamp";
    $order_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);


	//update posorders
	$sql = "update posorders set " . 
		   "posorder_ordernumber = " . dbquote($form->value("project_number")) . ", " .
		   "posorder_project_locally_produced = " . dbquote($form->value("project_is_local_production")) . ", " . 
		   "posorder_project_type_subclass_id = " . dbquote($form->value("project_type_subclass_id")) . ", " . 
		   "posorder_furniture_type_store = " . dbquote($form->value("project_furniture_type_store")) . ", " . 
		   "posorder_furniture_type_sis = " . dbquote($form->value("project_furniture_type_sis")) . ", " .
		   "posorder_uses_icedunes_visuals = " . dbquote($form->value("project_uses_icedunes_visuals")) . 
		   " where posorder_order = " . dbquote($form->value("oid"));
	$result = mysql_query($sql) or dberror($sql);
	$sql = "update posorderspipeline set " . 
		   "posorder_ordernumber = " . dbquote($form->value("project_number")) . ", " .
		   "posorder_project_locally_produced = " . dbquote($form->value("project_is_local_production")) . ", " . 
		   "posorder_project_type_subclass_id = " . dbquote($form->value("project_type_subclass_id")) . ", " . 
		   "posorder_furniture_type_store = " . dbquote($form->value("project_furniture_type_store")) . ", " . 
		   "posorder_furniture_type_sis = " . dbquote($form->value("project_furniture_type_sis")) . ", " .
		   "posorder_uses_icedunes_visuals = " . dbquote($form->value("project_uses_icedunes_visuals")) . 
		   " where posorder_order = " . dbquote($form->value("oid"));
	$result = mysql_query($sql) or dberror($sql);

}


/********************************************************************
    save comment
*********************************************************************/
function save_comment($form, $check_box_names)
{
    $comment_fields = array();
    $comment_values = array();

    $comment_fields[] = "comment_order";
    $comment_values[] = $form->value("order_id");

    $comment_fields[] = "comment_user";
    $comment_values[] = user_id();

    $comment_fields[] = "comment_category";
    $comment_values[] = $form->value("comment_category");

    $comment_fields[] = "comment_text";
    $comment_values[] = dbquote($form->value("comment_text"));

    $comment_fields[] = "date_created";
    $comment_values[] = "current_timestamp";

    $comment_fields[] = "date_modified";
    $comment_values[] = "current_timestamp";
                
    if (isset($_SESSION["user_login"]))
    {
        $comment_fields[] = "user_created";
        $comment_values[] = dbquote($_SESSION["user_login"]);

        $comment_fields[] = "user_modified";
        $comment_values[] = dbquote($_SESSION["user_login"]);
    }
    
    $sql = "insert into comments (" . join(", ", $comment_fields) . ") values (" . join(", ", $comment_values) . ")";
    mysql_query($sql) or dberror($sql);

    $comment_id = mysql_insert_id();


    // insert records into comment_addresses

    if (has_access("can_set_comment_accessibility_in_projects") or has_access("can_set_comment_accessibility_in_orders"))
    {

        foreach ($check_box_names as $key=>$value)
        {
            if ($form->value($key))
            {
                $comment_address_fields = array();
                $comment_address_values = array();

                $comment_address_fields[] = "comment_address_comment";
                $comment_address_values[] = $comment_id;

                $comment_address_fields[] = "comment_address_address";
                $comment_address_values[] = $value;

                $comment_address_fields[] = "date_created";
                $comment_address_values[] = "current_timestamp";

                $comment_address_fields[] = "date_modified";
                $comment_address_values[] = "current_timestamp";
                
                if (isset($_SESSION["user_login"]))
                {
                    $comment_address_fields[] = "user_created";
                    $comment_address_values[] = dbquote($_SESSION["user_login"]);

                    $comment_address_fields[] = "user_modified";
                    $comment_address_values[] = dbquote($_SESSION["user_login"]);
                }
    
                $sql = "insert into comment_addresses (" . join(", ", $comment_address_fields) . ") values (" . join(",        ", $comment_address_values) . ")";
                mysql_query($sql) or dberror($sql);
            }
        }
    }
    return $comment_id;
}


/********************************************************************
    update comment accessibility
*********************************************************************/
function update_comment_accessibility_info($id, $form, $check_box_names)
{
    $sql = "delete from comment_addresses where comment_address_comment = " . $id;
    mysql_query($sql) or dberror($sql);


    foreach ($check_box_names as $key=>$value)
    {	
		
        if ($form->value($key))
        {
            $comment_addressfields = array();
            $comment_addressvalues = array();

            $comment_addressfields[] = "comment_address_comment";
            $comment_addressvalues[] = $id;

            $comment_addressfields[] = "comment_address_address";
            $comment_addressvalues[] = $value;

            $comment_addressfields[] = "date_created";
            $comment_addressvalues[] = "current_timestamp";

            $comment_addressfields[] = "date_modified";
            $comment_addressvalues[] = "current_timestamp";
                
            if (isset($_SESSION["user_login"]))
            {
                $comment_addressfields[] = "user_created";
                $comment_addressvalues[] = dbquote($_SESSION["user_login"]);

                $comment_addressfields[] = "user_modified";
                $comment_addressvalues[] = dbquote($_SESSION["user_login"]);
            }
    
            $sql = "insert into comment_addresses (" . join(", ", $comment_addressfields) . ") values (" . join(", ", $comment_addressvalues) . ")";
                mysql_query($sql) or dberror($sql);

        }
    }
}

/********************************************************************
    append record to task list
*********************************************************************/
function append_task($order_id, $user_id, $text, $url, $due_date, $from_user, $order_state_code, $type, $keep_order_state = 0)
{

        $task_fields = array();
        $task_values = array();

        $order_state_id = null;
        $order_state_predecessor = "";
        $order_state_delete_tasks = 0;
        $order_state_manually_deleted = 0;

        // get order_state_id
        $sql = "select order_state_id, order_state_predecessor, ".
               "    order_state_delete_tasks, order_state_manually_deleted " .
               "from order_states ".
               "left join order_state_groups on order_state_group = order_state_group_id ".
               "where order_state_code = " . dbquote($order_state_code) .
               "    and order_state_group_order_type = " .$type;
        
        $res = mysql_query($sql) or dberror($sql);
    
        if ($row = mysql_fetch_assoc($res))
        {
            $order_state_id = $row["order_state_id"];
            $order_state_predecessor = $row["order_state_predecessor"];
            $order_state_delete_tasks = $row["order_state_delete_tasks"];
            $order_state_manually_deleted = $row["order_state_manually_deleted"];
        }

        // insert record into table tasks

        $task_fields[] = "task_order";
        $task_values[] = $order_id;

        $task_fields[] = "task_order_state";
        $task_values[] = $order_state_id;

        $task_fields[] = "task_user";
        $task_values[] = $user_id;

        $task_fields[] = "task_from_user";
        $task_values[] = $from_user;

        $task_fields[] = "task_text";
        $task_values[] = dbquote($text);

        $task_fields[] = "task_url";
        $task_values[] = dbquote($url);

        $task_fields[] = "task_due_date";
        $task_values[] = dbquote($due_date);

        $task_fields[] = "task_delete_task";
        $task_values[] = $order_state_delete_tasks;

        $task_fields[] = "task_manually_deleted";
        $task_values[] = $order_state_manually_deleted;

		$task_fields[] = "task_keep_order_state";
        $task_values[] = $keep_order_state;
		

        $task_fields[] = "date_created";
        $task_values[] = "current_timestamp";

        $task_fields[] = "date_modified";
        $task_values[] = "current_timestamp";

        if (isset($_SESSION["user_login"]))
        {
            $task_fields[] = "user_created";
            $task_values[] = dbquote($_SESSION["user_login"]);

            $task_fields[] = "user_modified";
            $task_values[] = dbquote($_SESSION["user_login"]);
        }

        $sql = "insert into tasks (" . join(", ", $task_fields) . ") values (" . join(", ", $task_values) . ")";
        mysql_query($sql) or dberror($sql);

}


/********************************************************************
    add record to order_item_tracking
*********************************************************************/
function track_change_in_order_items($action = '', $field = '', $order_id = 0, $order_item_id = 0, $item_id = 0, $item_type = 0, $item_text = '', $old_value = '', $new_value = '')
{
	if($order_id > 0)
	{
		//get actual order_state_id
		$order_state_id = 0;

		$sql = "select order_state_id from orders " .
			   " left join order_states on order_state_code = order_actual_order_state_code " . 
			   " left join order_state_groups on order_state_group_id = order_state_group " . 
			   " where order_state_group_order_type = 1 " . 
			   " and order_state_code = order_actual_order_state_code " . 
			  " and order_id = " . dbquote($order_id);
		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$order_state_id = $row["order_state_id"];
		}

		$order_item_tracking_fields = array();
		$order_item_tracking_values = array();

		$order_item_tracking_fields[] = "order_item_tracking_action";
		$order_item_tracking_values[] = dbquote($action);

		$order_item_tracking_fields[] = "order_item_tracking_field_name";
		$order_item_tracking_values[] = dbquote($field);

		$order_item_tracking_fields[] = "order_item_tracking_order_id";
		$order_item_tracking_values[] = $order_id;

		$order_item_tracking_fields[] = "order_item_tracking_order_item_id";
		$order_item_tracking_values[] = dbquote($order_item_id);

		$order_item_tracking_fields[] = "order_item_tracking_order_item_item_id";
		$order_item_tracking_values[] = dbquote($item_id);

		$order_item_tracking_fields[] = "order_item_tracking_item_type";
		$order_item_tracking_values[] = dbquote($item_type);

		$order_item_tracking_fields[] = "order_item_tracking_item_text";
		$order_item_tracking_values[] = dbquote($item_text);

		
		$order_item_tracking_fields[] = "order_item_tracking_old_value";
		$order_item_tracking_values[] = dbquote($old_value);
		
		$order_item_tracking_fields[] = "order_item_tracking_new_value";
		$order_item_tracking_values[] = dbquote($new_value);

		$order_item_tracking_fields[] = "order_item_tracking_order_state_id";
		$order_item_tracking_values[] = dbquote($order_state_id);

		$order_item_tracking_fields[] = "order_item_tracking_user_id";
		$order_item_tracking_values[] = dbquote(user_id());
		
		$order_item_tracking_fields[] = "user_created";
		$order_item_tracking_values[] = dbquote($_SESSION["user_login"]);

		$order_item_tracking_fields[] = "date_created";
		$order_item_tracking_values[] = "current_timestamp";

		
		$sql = "insert into order_item_trackings (" . join(", ", $order_item_tracking_fields) . ") values (" . join(", ", $order_item_tracking_values) . ")";
		mysql_query($sql) or dberror($sql);
	}
	return true;
}


/**************************************************************************************
    append record to the tabel actual_order_states
***************************************************************************************/
function append_order_state($order_id, $order_state_code, $type, $do_update, $keep_project_state = 0)
{
	// get order_state_id
    $sql = "select order_state_id, order_state_change_state, " .
           "order_state_order_state_type, " .
           "   order_state_delete_tasks " .
           "from order_states ".
           "left join order_state_groups on order_state_group = order_state_group_id ".
           "where order_state_code = " . dbquote($order_state_code) .
           "    and order_state_group_order_type = " .$type;

    $res = mysql_query($sql) or dberror($sql);
    
    if ($row = mysql_fetch_assoc($res))
    {
        $order_state_change_state = $row["order_state_change_state"];
        $order_state_delete_tasks = $row["order_state_delete_tasks"];
        $order_state_order_state_type = $row["order_state_order_state_type"];
        
        $order_state_fields = array();
        $order_state_values = array();
    
        // insert record into table orders

        $order_state_fields[] = "actual_order_state_order";
        $order_state_values[] = $order_id;

        $order_state_fields[] = "actual_order_state_state";
        $order_state_values[] = $row["order_state_id"];

        $order_state_fields[] = "actual_order_state_user";
        $order_state_values[] = user_id();

        $order_state_fields[] = "date_created";
        $order_state_values[] = "current_timestamp";

        $order_state_fields[] = "date_modified";
        $order_state_values[] = "current_timestamp";

        if (isset($_SESSION["user_login"]))
        {
            $order_state_fields[] = "user_created";
            $order_state_values[] = dbquote($_SESSION["user_login"]);

            $order_state_fields[] = "user_modified";
            $order_state_values[] = dbquote($_SESSION["user_login"]);
        }


        $sql = "insert into actual_order_states (" . join(", ", $order_state_fields) . ") values (" . join(", ", $order_state_values) . ")";
        mysql_query($sql) or dberror($sql);
    }
}


/***********************************************************************
   create order number
************************************************************************/
function order_generate_order_number($is_replacement = false)
{
    // find latest order in database

    if($is_replacement == true)
	{
		$nr_today = "R-" . date("Y-m-d", time());
	}
	else
	{
		$nr_today = "C-" . date("Y-m-d", time());
	}

    $sql = "select order_number " .
           "from orders " .
           "where order_type = 2 and left(order_number, 12) = '" .$nr_today . "' " .
           "order by order_number desc";

    $res = mysql_query($sql) or dberror($sql);

    if ($res)
    {
        $bla = 2;
        $row = mysql_fetch_assoc($res);
        $tokens = explode("-", $row['order_number']);

        if (isset($tokens[4]))
        {
            $running_number = $tokens[4] + 1;
        }
        else
        {
            $running_number = 1;
        }
    }
    else
    {
        $running_number = 1;
    }

    if($is_replacement == true)
	{
		$number = "R-" . date("Y-m-d", time()) . "-";
	}
	else
	{
		$number = "C-" . date("Y-m-d", time()) . "-";
	}
    $number .= sprintf("%04d", $running_number);
    return $number;
}


/********************************************************************
    save an address record
*********************************************************************/
function save_address($form, $address_type = BILLING_ADDRESS, $order_id = "NULL",
                            $order_item_id = "NULL", $address_parent = "NULL", 
                            $prefix = "billing_address")
{
    
	
	//insert new place into places

	
	if(array_key_exists('delivery_address_place_id', $form->items))
	{
		if($form->items['delivery_address_place_id']['value'] == 999999999) {
			$country_id = $form->items['delivery_address_country']['value'];
			$province_id = $form->items['delivery_address_province_id']['value'];
			$place_name = $form->items['delivery_address_place']['value'];

			$sql = "insert into places (place_country, place_province, place_name, date_created, user_created) Values (" . 
				   $country_id .  "," . $province_id . "," . dbquote($place_name) . "," . dbquote(date('Y-m-d')) . "," . dbquote(user_login()) . ")";

			$result = mysql_query($sql) or dberror($sql);

			$form->items['delivery_address_place_id']['value'] = mysql_insert_id();

		}
	};



	$sql = "show columns ".
           "from order_addresses " . 
           "like 'order_address_%'";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        if (empty($row['Key']))
        {
            if($row['Field'] != "order_address_inactive" 
				and $row['Field'] != "order_address_place_new" 
				and $row['Field'] != "order_address_place_tmp"
				and $row['Field'] != "order_address_fax"
				and $row['Field'] != "order_address_fax_country"
				and $row['Field'] != "order_address_fax_area"
				and $row['Field'] != "order_address_fax_number")
			{
				$table_fields[] = $row['Field'];
			}
        }
    }

    $address_fields[] = "order_address_order";
    $address_values[] = dbquote($order_id);

    $address_fields[] = "order_address_order_item";
    $address_values[] = dbquote($order_item_id);
    
    $address_fields[] = "order_address_type";
    $address_values[] = dbquote($address_type);

    $address_fields[] = "order_address_parent";
    $address_values[] = dbquote($address_parent);

    foreach ($table_fields as $field)
    {
        if (($field != "order_address_id")
           && ($field != "order_address_order")
           && ($field != "order_address_order_item")
           && ($field != "order_address_type")
           && ($field != "order_address_parent"))
        {

			if (($prefix == "shop_address") 
			&& (($field == "order_address_phone")
				|| ($field == "order_address_fax")
				|| ($field == "order_address_email")
				|| ($field == "order_address_contact")
				|| ($field == "order_address_fax")))
            {
                continue;
            }

            $address_fields[] = $field;
			
			if ((($prefix == "delivery_address") 
                || ($prefix == "shop_address"))
                 )
            {
                
				if(!array_key_exists(str_replace("order_address", "delivery_address", $field), $form->items))
				{
					foreach($form->items as $fieldname=>$field_array)
					{
						if($field_array['type'] == 'multi_edit')
						{
							foreach($field_array['fields'] as $key=>$db_field_name)
							{
								if($field == $db_field_name)
								{
									$address_values[] = dbquote(trim($field_array['values'][$key]));
								}
							}
						}
					}
				}
				else
				{
					$address_values[] = trim($form->value(str_replace("order_address", "delivery_address", $field))) == "" ? "null" : dbquote($form->value(str_replace("order_address", "delivery_address", $field)));
				}
            } else {
                
				if(!array_key_exists(str_replace("order_address", $prefix, $field), $form->items))
				{
					foreach($form->items as $fieldname=>$field_array)
					{
						if($field_array['type'] == 'multi_edit')
						{
							foreach($field_array['fields'] as $key=>$db_field_name)
							{
								if($field == $db_field_name)
								{
									$address_values[] = dbquote(trim($field_array['values'][$key]));
								}
							}
						}
					}
				}
				else
				{
					$address_values[] = trim($form->value(str_replace("order_address", $prefix, $field))) == "" ? "null" : dbquote($form->value(str_replace("order_address", $prefix, $field)));
				}
            }
        }
    }

    add_fbi_info($address_fields, $address_values);

    $sql = "insert into order_addresses (" . join(", ", $address_fields) . ") values (" . join(", ", $address_values) . ")";

    mysql_query($sql) or dberror($sql);

    return mysql_insert_id();
}


/********************************************************************
    add fbi_info to the list of fields
*********************************************************************/
function add_fbi_info(&$fields, &$values)
{
     $fields[] = "date_created";
     $values[] = "now()";

     $fields[] = "date_modified";
     $values[] = "now()";

     $fields[] = "user_created";
     $values[] = dbquote(user_login());

     $fields[] = "user_modified";
     $values[] = dbquote(user_login());
}
?>