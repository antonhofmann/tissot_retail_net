<?php
/********************************************************************

    project_view_comments.php

    List of comments made

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-25
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-02-16
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_comments_in_projects");

register_param("pid");
set_referer("project_edit_comment.php");
set_referer("project_add_comment.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get user data
$user_data = get_user(user_id());

//get retail operator
// build sql for comment entries
$sql_comments = "select distinct ".
                "    comment_id, comment_text, comment_visited, " .
                "    comments.date_created, ".
                "    comment_category_name, comment_category_priority, ".
                "    concat(user_name, ' ', user_firstname) as user_fullname ".
                "from comments ".
                "inner join users on user_id = comment_user ".                
                "inner join comment_categories on comment_category_id = comment_category ".
                "inner join comment_addresses on comment_address_comment = comment_id";


// build filter for the list of comments
$list1_filter = "comment_category_order_type = 2 and comment_order = " . $project["project_order"];


if (has_access("has_access_to_all_comments_in_projects"))
{
    
}
else
{
	$list1_filter = $list1_filter . " and (comment_address_address = " . $user_data["address"] . " or comment_user = " . user_id() . ")";
}




// get new comment info pix
$sql_pix = $sql_comments . " where " . $list1_filter;
$images = set_new_comment_pictures($sql_pix, $project["project_order"]);

// determine users that have already got an email announcing the new attachment
$recipients = array();

$user_filter = array();

if($project["project_retail_coordinator"]) {
	$user_filter[] = $project["project_retail_coordinator"];
}
if($project["project_hq_project_manager"]) {
	$user_filter[] = $project["project_hq_project_manager"];
}
if($project["project_local_retail_coordinator"]) {
	$user_filter[] = $project["project_local_retail_coordinator"];
}
if($project["project_design_contractor"]) {
	$user_filter[] = $project["project_design_contractor"];
}
if($project["project_design_supervisor"]) {
	$user_filter[] = $project["project_design_supervisor"];
}
if($project["order_retail_operator"]) {
	$user_filter[] = $project["order_retail_operator"];
}
if($project["order_user"]) {
	$user_filter[] = $project["order_user"];
}

$res = mysql_query($sql_pix) or dberror($sql_pix);
while ($row = mysql_fetch_assoc($res))
{
	$names = array();
	$namescc = array();

	
	$sql_a = "select comment_address_address " . 
		     " from comment_addresses " . 
		     " where comment_address_comment = " . dbquote($row["comment_id"]);

	$res_a = mysql_query($sql_a) or dberror($sql_a);
	while ($row_a = mysql_fetch_assoc($res_a))
	{
		if(count($user_filter) > 0)
		{
			$sql_o = "select DISTINCT order_mail_is_cc, concat(user_name, ' ', user_firstname, ' (', address_company , ')') as username  " . 
				   " from order_mails " . 
				   " inner join users on user_id = order_mail_user  " . 
				   " inner join addresses on address_id = user_address " . 
				   " where order_mail_order =  " . dbquote($project["project_order"]) . 
				   " and (user_address = " .  dbquote($row_a["comment_address_address"]) . 
				   "    or order_mail_user in (" . implode(',', $user_filter) . ")) " . 
				   " and REPLACE(order_mail_text, \"'\", \"\") like '%" . str_replace("'", "", $row["comment_text"]). "%'" .
				   " order by user_name ";
		}
		else
		{
			$sql_o = "select DISTINCT order_mail_is_cc, concat(user_name, ' ', user_firstname, ' (', address_company , ')') as username  " . 
				   " from order_mails " . 
				   " inner join users on user_id = order_mail_user  " . 
				   " inner join addresses on address_id = user_address " . 
				   " where order_mail_order =  " . dbquote($project["project_order"]) . 
				   " and user_address = " .  dbquote($row_a["comment_address_address"]) . 
				   " and REPLACE(order_mail_text, \"'\", \"\") like '%" . str_replace("'", "", $row["comment_text"]). "%'" .
				   " order by user_name ";
		}
		
		$res_o = mysql_query($sql_o) or dberror($sql_o);
		while ($row_o = mysql_fetch_assoc($res_o))
		{
			if($row_o["order_mail_is_cc"] == 1)
			{
				$namescc[ $row_o["username"]] = "cc: " . $row_o["username"] . "<br />";
			}
			else
			{
				$names[$row_o["username"]] = $row_o["username"] . "<br />";
			}
		
		}
	}


	if(count($names) > 0)
	{
		$recipients[$row["comment_id"]] = implode('',$names) .  implode('',$namescc);
	}
}


/*performing too slow
$res = mysql_query($sql_pix) or dberror($sql_pix);
while ($row = mysql_fetch_assoc($res))
{
	$names = "";

	$order_mail_recipients = get_order_mail_recipients($project["project_order"], "comments", $row["comment_id"]);
	
	if(count($order_mail_recipients) > 0)
	{
		foreach($order_mail_recipients as$user_id=>$value)
		{
			$names .= $value . "<br />";
		}
	}
	else
	{
		$companies = get_involved_companies_2($project["project_order"], $row["comment_id"]);


		foreach($companies as $key=>$value)
		{
			if($value["access"] == 1)
			{
				$names .= $value["name"] . "<br />";
			}
		}
	}

	if($names)
	{
		$recipients[$row["comment_id"]] = $names;
	}
}

*/

/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("comments", "comment");


$form->add_hidden("pid", param("pid"));


require_once "include/project_head_small.php";


/********************************************************************
    Create List
*********************************************************************/ 


$list1 = new ListView($sql_comments);


$list1->set_entity("comments");
$list1->set_filter($list1_filter);
$list1->set_order("comments.date_created DESC");
$list1->set_group("comment_category_priority", "comment_category_name");





$link = "project_edit_comment.php?pid=" . param("pid");


if (has_access("can_edit_comment_data_in_projects"))
{
    $list1->add_column("date_created", "Date/Time", $link, "", "", COLUMN_NO_WRAP);
}
else
{
    $list1->add_column("date_created", "Date/Time", "", "", "", COLUMN_NO_WRAP);
}

if(count($images)> 0)
{
    $list1->add_image_column("comment_id", "New", 0, $images);
}

$list1->add_column("user_fullname", "Made by", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("comment_text", "Comment", "", "", "", COLUMN_BREAK);

$list1->add_text_column("info", "Recipients", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP, $recipients);



if (has_access("can_add_comments_in_projects"))
{
    $list1->add_button("add_comment", "Add Comment");
}




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();


if ($list1->button("add_comment"))
{
    $link = "project_add_comment.php?pid=" . param("pid");
    redirect ($link);
}

$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Comments");
$form->render();
$list1->render();
$page->footer();


?>