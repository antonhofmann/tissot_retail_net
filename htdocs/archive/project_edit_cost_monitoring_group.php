<?php
/********************************************************************

    project_edit_cost_monitoring_group.php

    Edit cost monitoring sheet: group criteria

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-10-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-10-24
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_cost_monitoring_reinvoice_data");

register_param("pid");

set_referer("project_edit_cost_monitoring_edit_group.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));


// get System currency
$system_currency = get_system_currency_fields();


// get company's address
$client_address = get_address($project["order_client_address"]);

// get System currency
$system_currency = get_system_currency_fields();



/********************************************************************
    Get Item Data
*********************************************************************/ 
$sql_list = "select order_item_id, order_item_text, " .
                   "order_item_po_number,  order_item_cost_group," .
                   "item_id, if(item_code is null, 'special item', item_code) as item_shortcut, ".
                   "    address_shortcut as supplier_company, order_item_supplier_freetext, ".
                   "    item_type_id, item_type_name, ".
                   "    item_type_priority, project_cost_groupname_name, ".
                   "    order_items_monitoring_group " .
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join addresses on order_item_supplier_address = addresses.address_id ".
                   "left join item_types on order_item_type = item_type_id " . 
                   "left join project_cost_groupnames on project_cost_groupname_id = order_item_cost_group";


$list1_filter = "order_item_type <= " . ITEM_TYPE_SPECIAL . " and order_item_order = " . $project["project_order"];

$where_clause = " where order_item_not_in_budget <> 1 " .
                "  and order_item_type < " . ITEM_TYPE_COST_ESTIMATION . 
                "  and order_item_order = " . $project["project_order"];


$groups = array();

$num_recs1 = 0;

$res = mysql_query($sql_list . $where_clause) or dberror($sql_list . $where_clause);
while ($row = mysql_fetch_assoc($res))
{
    $groups[$row["order_item_id"]] = $row["order_items_monitoring_group"];
    $suppliers[$row["order_item_id"]] = $row["order_item_supplier_freetext"];

    $num_recs1 = $num_recs1 + 1;
}



/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);

require_once "include/project_head_small.php";


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create List for Items
*********************************************************************/ 
$list1 = new ListView($sql_list);

$list1->set_title("Itemlist");
$list1->set_entity("order_item");
$list1->set_filter($list1_filter);
$list1->set_order("supplier_company, item_shortcut");
$list1->set_group("supplier_company", "supplier_company");


$list1->add_column("project_cost_groupname_name", "Cost Group", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("item_shortcut", "Item Code");

$list1->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);

$list1->add_edit_column("order_items_monitoring_group", "Monitoring Sheet Group Name", "40", 0, $groups);

$list1->add_column("order_item_text", "Name");

$list1->add_button("save_data", "Save");
$list1->add_button("cost_monitoring", "Back");
$list1->add_button("preview", "Preview CMS");

$list1->populate();
$list1->process();


if ($list1->button("save_data"))
{
     project_update_order_item_group_data($list1, $project["project_order"]);
     $link = "project_edit_cost_monitoring_group.php?pid=" . param("pid"); 
     redirect($link);
}
elseif ($list1->button("cost_monitoring"))
{
    $link = "project_edit_cost_monitoring.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list1->button("preview"))
{
    $link = "project_edit_cost_monitoring_preview.php?pid=" . param("pid"); 
    redirect($link);
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Cost Monitoring Sheet: Group Data");
$form->render();

if($num_recs1 > 0)
{
    $list1->render();
}


$page->footer();

?>