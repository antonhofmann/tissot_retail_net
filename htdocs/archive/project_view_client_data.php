<?php
/********************************************************************

    project_view_client_data.php

    See the projec details.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-02-18
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require "include/get_functions.php";

check_access("can_view_client_data_in_projects");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));


// get company's address
$client_address = get_address($project["order_client_address"]);

// get order's currency
$currency = get_order_currency($project["project_order"]);

// read design_item_ids from project items
$project_design_objective_item_ids = get_project_design_objective_item_ids(param("pid"));

// read information from order_addresses
$delivery_address = get_order_address(2, $project["project_order"]);

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";


$billing_address_province_name = "";
if($project["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($project["order_billing_address_place_id"]);
}

$delivery_address_province_name = "";
if($delivery_address["place_id"])
{
	$delivery_address_province_name = get_province_name($delivery_address["place_id"]);
}


$franchisee_address = array();
if($project["order_franchisee_address_id"] > 0)
{
	$franchisee_address = get_address($project["order_franchisee_address_id"]);
	
}


/********************************************************************
    Get Cost Monitoring Sheet Budget Data
*********************************************************************/ 
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$project_cost_grosssqms = $row["project_cost_grosssqms"];
$project_cost_totalsqms = $row["project_cost_totalsqms"];
$project_cost_sqms = $row["project_cost_sqms"];
$project_cost_original_sqms = $row["project_cost_original_sqms"];
$project_cost_backofficesqms = $row["project_cost_backofficesqms"];
$project_cost_floorsurface1 = $row["project_cost_floorsurface1"];
$project_cost_floorsurface2 = $row["project_cost_floorsurface2"];
$project_cost_floorsurface3 = $row["project_cost_floorsurface3"];
$project_cost_numfloors = $row["project_cost_numfloors"];

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

require_once "include/project_head_small.php";

if($project["project_cost_type"] == 2) // Franchisee
{
	//Franchisee Address
	$form->add_section("POS Owner Address");
	
	if(count($franchisee_address) > 0)
	{
		$form->add_label("franchisee_address_company", "Company", 0, $franchisee_address["company"]);
		if ($franchisee_address["company2"])
		{
			$form->add_label("franchisee_address_company2", "", 0, $franchisee_address["company2"]);
		}
		$form->add_label("franchisee_address_address", "Street", 0, $franchisee_address["address"]);
		if ($franchisee_address["address2"])
		{
			$form->add_label("franchisee_address_address2", "Additional Address Info", 0, $franchisee_address["address2"]);
		}
		$form->add_label("franchisee_address_place", "City", 0, $franchisee_address["zip"] . " " . $franchisee_address["place_name"]);
		$form->add_label("franchisee_address_country", "Country", 0, $franchisee_address["country_name"]);
		$form->add_label("franchisee_address_phone", "Phone", 0, $franchisee_address["phone"]);
		$form->add_label("franchisee_address_mobile_phone", "Mobile Phone", 0, $franchisee_address["mobile_phone"]);
		$form->add_label("franchisee_address_email", "Email", 0, $franchisee_address["email"]);
	}
	else
	{
		$form->add_label("franchisee_address_company", "Company", 0, $project["order_franchisee_address_company"]);
		if ($project["order_franchisee_address_company2"])
		{
			$form->add_label("franchisee_address_company2", "", 0, $project["order_franchisee_address_company2"]);
		}
		$form->add_label("franchisee_address_address", "Street", 0, $project["order_franchisee_address_address"]);
		if ($project["order_franchisee_address_address2"])
		{
			$form->add_label("franchisee_address_address2", "Additional Address Info", 0, $project["order_franchisee_address_address2"]);
		}
		$form->add_label("franchisee_address_place", "City", 0, $project["order_franchisee_address_zip"] . " " . $project["order_franchisee_address_place"]);
		$form->add_lookup("franchisee_address_country", "", "countries", "country_name", 0, $project["order_franchisee_address_country"]);
		$form->add_label("franchisee_address_phone", "Phone", 0, $project["order_franchisee_address_phone"]);
		$form->add_label("franchisee_address_mobile_phone", "Mobile Phone", 0, $project["order_franchisee_address_mobile_phone"]);
		$form->add_label("franchisee_address_email", "Email", 0, $project["order_franchisee_address_email"]);
	}
}


if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
{
	//bill to
	$form->add_section("Bill to");
	$form->add_label("billing_address_company", "Company", 0, $project["order_billing_address_company"]);


	if ($project["order_billing_address_company2"])
	{
		$form->add_label("billing_address_company2", "", 0, $project["order_billing_address_company2"]);
	}


	$form->add_label("billing_address_address", "Street", 0, $project["order_billing_address_address"]);


	if ($project["order_billing_address_address2"])
	{
		$form->add_label("billing_address_address2", "Additional Address Info", 0, $project["order_billing_address_address2"]);
	}


	$form->add_label("billing_address_place", "City", 0, $project["order_billing_address_zip"] . " " . $project["order_billing_address_place"]);
	$form->add_label("billing_address_province_name", "Province", 0, $billing_address_province_name);
	$form->add_label("billing_address_country_name", "Country", 0, $project["order_billing_address_country_name"]);


	$form->add_label("billing_address_phone", "Phone", 0, $project["order_billing_address_phone"]);
	$form->add_label("billing_address_mobile_phone", "Mobile Phone", 0, $project["order_billing_address_mobile_phone"]);
	$form->add_label("billing_address_email", "Email", 0, $project["order_billing_address_email"]);

	/*
	$form->add_section("Ship to");
	$form->add_label("delivery_address_company", "Company", 0, $delivery_address["company"]);


	if ($delivery_address["company2"])
	{
		$form->add_label("delivery_address_company2", "", 0, $delivery_address["company2"]);
	}


	$form->add_label("delivery_address_address", "Street", 0, $delivery_address["address"]);


	if ($delivery_address["address2"])
	{
		$form->add_label("delivery_address_address2", "Additional Address Info", 0, $delivery_address["address2"]);
	}


	$form->add_label("delivery_address_place", "City", 0, $delivery_address["zip"] . " " . $delivery_address["place"]);
	$form->add_label("delivery_address_province_name", "Province", 0, $delivery_address_province_name);

	$form->add_lookup("delivery_address_country", "Country", "countries", "country_name", 0, $delivery_address["country"]);


	$form->add_label("delivery_address_phone", "Phone", 0, $delivery_address["phone"]);
	$form->add_label("delivery_address_mobile_phone", "Mobile Phone", 0, $delivery_address["mobile_phone"]);
	$form->add_label("delivery_address_email", "Email", 0, $delivery_address["email"]);
	*/
}


$form->add_section("POS Location Address");
$form->add_label("shop_address_company", "Project Name", 0, $project["order_shop_address_company"]);


if ($project["order_shop_address_company2"])
{
    $form->add_label("shop_address_company2", "", 0, $project["order_shop_address_company2"]);
}


$form->add_label("shop_address_address", "Street", 0, $project["order_shop_address_address"]);


if ($project["order_shop_address_address2"])
{
    $form->add_label("shop_address_address2", "Additional Address Info", 0, $project["order_shop_address_address2"]);
}

$form->add_label("shop_address_place", "City", 0, $project["order_shop_address_zip"] . " " . $project["order_shop_address_place"]);
$form->add_lookup("shop_address_country", "", "countries", "country_name", 0, $project["order_shop_address_country"]);
$form->add_label("shop_address_phone", "Phone", 0, $project["order_shop_address_phone"]);
$form->add_label("shop_address_mobile_phone", "Mobile Phone", 0, $project["order_shop_address_mobile_phone"]);
$form->add_label("shop_address_email", "Email", 0, $project["order_shop_address_email"]);

if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
{
	$form->add_section("Surfaces");

	//$form->add_label("store_grosssurface", "Gross Surface in sqms*", 0, $project_cost_grosssqms);
	$form->add_label("store_totalsurface", "Total Surface in sqms*", 0, $project_cost_totalsqms);

	$form->add_label("requested_store_retailarea", "Sales Surface upon project entry in sqms*", 0, $project_cost_original_sqms);

	//$form->add_label("store_retailarea", "Sales Surface according to layout in sqms*", 0, $project_cost_sqms);
	$form->add_label("store_backoffice", "Other Surface in sqms", 0, $project_cost_backofficesqms);
	
	/*
	$form->add_label("store_numfloors", "Number of Floors*", 0, $project_cost_numfloors);
	$form->add_label("store_floorsurface1", "Floor 1: Surface  in sqms", 0, $project_cost_floorsurface1);
	$form->add_label("store_floorsurface2", "Floor 2: Surface  in sqms", 0, $project_cost_floorsurface2);
	$form->add_label("store_floorsurface3", "Floor 3: Surface  in sqms", 0, $project_cost_floorsurface3);
	*/

	$form->add_section("Location Info");
	if ($project["project_location_type"])
	{
		$form->add_lookup("location_type_name", "Location Type", "location_types", "location_type_name", 0, $project["project_location_type"]);
		$form->add_lookup("location_type", "", "projects", "project_location", HIDEEMPTY, param("pid"));
	}
	else
	{
		$form->add_lookup("location_type", "Location Type", "projects", "project_location", HIDEEMPTY, param("pid"));
	}

	if($project['project_tissot_shop_around']) {
		$form->add_label("tissot_shop_around", "POS selling Tissot in 1km around the location", 0, 'yes');
	}
	else {
		$form->add_label("tissot_shop_around", "POS selling Tissot in 1km around the location", 0, 'no');
	}

	//$form->add_lookup("voltage_name", "Voltage", "voltages", "voltage_name", 0, $project["order_voltage"]);


	// read design_item_ids from project items
	$sql = "select project_item_item, design_objective_group_name ".
		   "from project_items ".
		   "left join design_objective_items on project_item_item = design_objective_item_id ".
		   "left join design_objective_groups on design_objective_item_group = design_objective_group_id ".
		   "where project_item_project  = " . param("pid") ." ".
		   "order by design_objective_group_priority, design_objective_item_priority";


	$res = mysql_query($sql) or dberror($sql);


	$i=1;
	$group="";
	while ($row = mysql_fetch_assoc($res))
	{
		if($i == 1)
		{
			$form->add_section("Design Objectives");
		}
		
		if ($group != $row["design_objective_group_name"])
		{
			$form->add_lookup("design_objective_item" . $i, $row["design_objective_group_name"], "design_objective_items", "design_objective_item_name", 0, $row["project_item_item"]);
			$group = $row["design_objective_group_name"];
		}
		else
		{
			$form->add_lookup("design_objective_item" . $i, "", "design_objective_items", "design_objective_item_name", 0, $row["project_item_item"]);
		}
		$i++;
	}



	/*
	$form->add_section("Capacity Request by Client");
	$form->add_label("watches_displayed", "Watches Displayed", 0, $project["project_watches_displayed"]);
	$form->add_label("watches_stored", "Watches Stored", 0, $project["project_watches_stored"]);
	
	$form->add_label("bijoux_displayed", "Watch Straps Displayed", 0, $project["project_bijoux_displayed"]);
	$form->add_label("bijoux_stored", "Watch Straps Stored", 0, $project["project_bijoux_stored"]);
	*/
	
	/*
	$form->add_section("Preferred Arrival Dates of Goods for each Supplier");
	//add preferred arrival dates for each supplier
	$sql_suppliers = "select distinct address_id, address_company, item_category, item_category_name, order_item_preferred_arrival_date " . 
					   " from order_items " . 
					   " left join addresses on address_id = order_item_supplier_address " .
					   " left join items on item_id = order_item_item " . 
					   " left join item_categories on item_category_id = item_category " . 
					   " where order_item_order = " . $project["project_order"] .
					   " and address_id > 0 " . 
					   " order by address_company";

	$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);
	while ($row = mysql_fetch_assoc($res))
	{
		$label = '<strong>' . $row["item_category_name"] . '</strong>  - ' . $row["address_company"];
		$form->add_label("preferred_delivery_date_" . $row["address_id"] . '_' . $row["item_category"], $label, 0, to_system_date($row["order_item_preferred_arrival_date"]), TYPE_DATE, 10);
	}

	$form->add_section("Preferences and Traffic Checklist");
	//$form->add_label("preferred_delivery_date", "Preferred Arrival Date", 0, to_system_date($project["order_preferred_delivery_date"]));
	

	$form->add_lookup("preferred_transportation_arranged", "Transportation arranged by", "transportation_types", "transportation_type_name", 0, $project["order_preferred_transportation_arranged"]);

	$form->add_lookup("preferred_transportation_mode", "Transportation mode", "transportation_types", "transportation_type_name", 0, $project["order_preferred_transportation_mode"]);


	$value="no";
	if ($project["order_packaging_retraction"])
	{
		$value="yes";
	}
	$form->add_label("order_packaging_retraction", "Packaging Retraction Desired", 0, $value);
	

	$value="no";
	if ($project["order_pedestrian_mall_approval"])
	{
		$value="yes";
	}
	$form->add_label("pedestrian_mall_approval", "Pedestrian Area Approval Needed", 0, $value);


	$value="no";
	if ($project["order_full_delivery"])
	{
		$value="yes";
	}
	$form->add_label("full_delivery", "Full Delivery Desired", 0, $value);


	$form->add_label("delivery_comments", "Delivery Comments", 0, $project["order_delivery_comments"]);


	$form->add_section("Insurance");
	if($project["order_insurance"] == 1)
	{
		$form->add_label("order_insurance", "Insurance by Tissot/Forwarder", 0, "covered");
	}
	else
	{
		$form->add_label("order_insurance", "Insurance by Tissot/Forwarder", 0, "not covered");
	}
	*/

	$form->add_section("Other Information");
	$form->add_label("approximate_budget", "Approximate Budget in " . $currency["symbol"], 0, $project["project_approximate_budget"]);
	
	$form->add_label("project_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));

	if($project["project_pos_subclass"] == 27) //temporary POS
	{
		$form->add_label("project_closing_date", "Client's Planned Closing Date", 0, to_system_date($project["project_planned_closing_date"]));
	}

	$form->add_label("project_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
	$form->add_label("project_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

	$form->add_section("General Comments");
	$form->add_label("general_comments", "General Comments", 0, $project["project_comments"]);
}






/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("View Project's Client Data");
$form->render();

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>