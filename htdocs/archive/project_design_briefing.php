<?php
/********************************************************************

    project_design_briefing.php

    Edit/View project's design briefing

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2017-12-14
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2017-12-14
    Version:        1.0.0

    Copyright (c) 2017, Tissot, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";


if(!has_access("can_view_design_briefing") and !has_access("can_edit_design_briefing") ) {
	redirect("noaccess.php");
}
if(!param("pid")) {
	redirect("noaccess.php");
}

register_param("pid");


/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);



if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");

	$sql = "select posarea_area, posareatype_name " . 
		   "from posareas " . 
		   " left join posareatypes on posareatype_id = posarea_area " . 
		   "where posarea_posaddress = " . dbquote($pos_data["posaddress_id"]) . 
		   " order by posarea_area";
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);

	$sql = "select posarea_area, posareatype_name  
		   from posareaspipeline
		   left join posareatypes on posareatype_id = posarea_area 
		   where posarea_posaddress = " . dbquote($pos_data["posaddress_id"]) . 
		   " order by posarea_area";
}

//get posareas
$posareas = '';
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$pos_posareas[]	= $row["posareatype_name"];
}
if(count($pos_posareas) > 0 ){
	$posareas = implode(', ', $pos_posareas);
}

//get floor
$posareas = $posareas . ", " . $project["project_floor"];

/********************************************************************
	build design briefing records if not available
*********************************************************************/
$design_brief_id = '';
$submitted = '';
$rejected = '';
$resubmitted = '';

$sql = "select *
	   from project_design_briefs 
	   where project_design_brief_project_id = " . dbquote(param("pid"));

$res = mysql_query($sql);
if($row = mysql_fetch_assoc($res))
{
	$design_brief_id = $row["project_design_brief_id"];

	$tmp = get_user($row["project_design_brief_submitted_by"]);
	$submitted = to_system_date($row["project_design_brief_submission_date"]) . " " . $tmp['name'] . " ".  $tmp['firstname'];
	$tmp = get_user($row["project_design_brief_rejected_by"]);
	$rejected = to_system_date($row["project_design_brief_rejection_date"]) . " " . $tmp['name'] . " ".  $tmp['firstname'];
	$tmp = get_user($row["project_design_brief_resubmitted_by"]);
	$resubmitted = to_system_date($row["project_design_brief_resubmission_date"]) . " " . $tmp['name'] . " ".  $tmp['firstname'];
}

/********************************************************************
    prepare data
*********************************************************************/
//design briefing documents
$files = array();
$sql = "select distinct order_file_id, order_file_visited, 
		  order_file_title, order_file_description, 
		  order_file_path, file_type_name, 
		  order_files.date_created, 
		  order_file_category_name, order_file_category_priority, 
		  concat(user_name, ' ', user_firstname) as owner_fullname 
	  from order_files  
	  left join order_file_categories on order_file_category_id = order_file_category 
	  left join users on user_id = order_file_owner 
	  left join file_types on order_file_type = file_type_id 
	  where order_file_category = 6 and order_file_order = " . dbquote($project["project_order"]);


$res = mysql_query($sql);
while($row = mysql_fetch_assoc($res))
{
	$files[] = array('title'=>$row["order_file_title"], 'path'=>$row['order_file_path'], 'date'=>$row['date_created']);
}

$order_number = $project['order_number'];


$sql = "select * from project_design_brief_positions
       left join design_brief_groups on design_brief_group_id = project_design_brief_position_group_id
	   left join design_brief_positions on design_brief_position_id = project_design_brief_position_position_id 
	   where project_design_brief_position_content <> ''
	   and project_design_brief_position_content is not null
	   and project_design_brief_position_design_brief_id = " . dbquote($design_brief_id) .
	   " order by design_brief_group_sort, design_brief_position_sort";


/********************************************************************
    build form
*********************************************************************/
$form = new Form("project_design_briefs", "project design briefings");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);
$form->add_hidden("dbid", $design_brief_id);

include('include/project_head_small.php');
if($posareas) {
	$form->add_label("environment", "Environment",0, $posareas);
}

$form->add_section("Submission");
$form->add_label('submitted', 'Submitted', 0, $submitted);
$form->add_label('rejected', 'Rejected', 0, $rejected);
$form->add_label('resubmitted', 'Resubmitted', 0, $resubmitted);

$link = "/user/project_design_briefing_pdf.php?pid=" . param("pid"); 
$link = "javascript:popup('". $link . "', 800, 600)";
$form->add_button("pdf1", "Print Design Briefing", $link);

$link = "/user/project_design_briefing_pdf.php?pid=" . param("pid") . "&files=1"; 
$link = "javascript:popup('". $link . "', 800, 600)";
$form->add_button("pdf2", "Print Design Briefing with File Attachments<br /><br />", $link);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();

/********************************************************************
    Populate forms and process button clicks
*********************************************************************/ 
$formnames = array();
$field_names = array();
$field_types = array();
$field_sets = array();
$sql = "select distinct project_design_brief_position_group_id, design_brief_group_name
	   from project_design_brief_positions
	   left join design_brief_groups on design_brief_group_id = project_design_brief_position_group_id
	   left join design_brief_positions on design_brief_position_id = project_design_brief_position_position_id 
	   where project_design_brief_position_content <> ''
	   and project_design_brief_position_content is not null
	   and project_design_brief_position_design_brief_id = " . dbquote($design_brief_id) .
	   " order by design_brief_group_sort, design_brief_position_sort";

$group = '';
$res = mysql_query($sql);
while($row = mysql_fetch_assoc($res))
{
	$formname = "form" . $row["project_design_brief_position_group_id"];
	
	$group_ids[$row["project_design_brief_position_group_id"]] = $row["project_design_brief_position_group_id"];
	$group_titles[$row["project_design_brief_position_group_id"]] = $row["design_brief_group_name"];

	$formnames[$row["project_design_brief_position_group_id"]] = $formname;

	$$formname = new Form("project_design_briefs", "project design briefings");
	


	//compose form
	$sql2 = "select * from project_design_brief_positions
		   left join design_brief_groups on design_brief_group_id = project_design_brief_position_group_id
		   left join design_brief_positions on design_brief_position_id = project_design_brief_position_position_id 
		   where project_design_brief_position_content <> ''
	   and project_design_brief_position_content is not null
	   and project_design_brief_position_design_brief_id = " . dbquote($design_brief_id) .
		   " and project_design_brief_position_group_id = " . dbquote($row["project_design_brief_position_group_id"]) .
		   " order by design_brief_position_sort";

	$res2 = mysql_query($sql2);
	while($row2 = mysql_fetch_assoc($res2))
	{
		
		$field_names[] = "P" . $row2["project_design_brief_position_id"];
		$field_types[] = $row2["project_design_brief_position_type"];

		$not_null = 0;
		$asterix = '';
		if($row2['design_brief_position_mandatory'] == 1) {
			$not_null = NOTNULL;
			$asterix = '*';
		}

		$text2 = '';
		if($row2["design_brief_position_infotext"]) {
			$text2 = '<br /><span class="text">'. nl2br($row2["design_brief_position_infotext"]) . '</span>';
		}

		switch ($row2['project_design_brief_position_type']) {
			case 1:
				$$formname->add_label('RO' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, 0, $row2["project_design_brief_position_content"]);
				break;
			case 2:
				$$formname->add_label('RO' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, 0, $row2["project_design_brief_position_content"]);
				break;
			case 3:
				$$formname->add_label('RO' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, 0, $row2["project_design_brief_position_content"]);
				break;
			case 4:
				$$formname->add_label('RO' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, 0, $row2["project_design_brief_position_content"]);
				break;
			case 5:
				$$formname->add_label('RO' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, 0, $row2["project_design_brief_position_content"]);
				break;
			case 6:
				$selection = explode("\r\n", $row2['project_design_brief_position_values']);
				$selection = explode("\r\n", $row2['project_design_brief_position_values']);
				if(array_key_exists($row2["project_design_brief_position_content"], $selection)) {
					$$formname->add_label('RO' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, 0, $selection[$row2["project_design_brief_position_content"]]);
				}
				else {
					$$formname->add_label('RO' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, 0, "");
				}
				
				break;
			case 7:
				$selection = explode("\r\n", $row2['project_design_brief_position_values']);
				
				$field_sets['P' . $row2["project_design_brief_position_id"]] = $selection;
				$content = json_decode($row2['project_design_brief_position_content'], true);
				$$formname->add_label('L' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2);
				foreach($selection as $key=>$value) {
					$selected = '';
					if($content[$key] == 1) {
						$selected = 'checked';
					}
					
					$$formname->add_checkbox('RO' . $row2["project_design_brief_position_id"] . '-' . $key, "", $selected, $not_null, $value, true);
				}
				break;
		}
		
	}


	//files for project information
	if ($row["project_design_brief_position_group_id"] == 13
		and has_access("can_add_attachments_in_projects"))
	{
		
		if(count($files) > 0) {
			$$formname->add_comment('<strong>List of uploaded Files</stong>');
			$html_filelist = '<table>';
			foreach($files as $key=>$file) {
				$html_filelist .= '<tr>';
				$html_filelist .= '<td><a href="'.$file['path'].'"><img src="/pictures/document_view.gif" /></a></td>';
				$html_filelist .= '<td><a href="'.$file['path'].'">' .$file['title'] . '</a></td>';
				$html_filelist .= '<td>' . to_system_date($file['date']) . '</td>';
				$html_filelist .= '</tr>';
			}
			$html_filelist .= '</table>';
			$$formname->add_comment($html_filelist);
		
		}
		
	}
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Design Briefing");

$form->render();


foreach($formnames as $key=>$formname) {

	$toggler = '<div class="toggler_pointer" id="f' . $key . '_on"><span class="fa fa-minus-square toggler"></span>' .$group_titles[$key] . '</div>';
	echo $toggler;

	$toggler = '<div class="toggler_pointer toggler_pointer_off" id="f' . $key . '_off"><span class="fa fa-plus-square toggler"></span>' .$group_titles[$key] . '</div>';
	echo $toggler;
	echo '<br />';
	
	$$formname->render($key);

}


require_once "include/project_footer_logistic_state.php";




?>
<script language="javascript">
jQuery(document).ready(function($) {

	<?php
	foreach($formnames as $key=>$formname)
	{
		if(array_key_exists("designbrief", $_SESSION) and array_key_exists($formname, $_SESSION["designbrief"]))
		{
			if($_SESSION["designbrief"][$formname] == 0)
			{
				?>
				$('#tid<?php echo $key;?>').css('display', 'none');
				$('#f<?php echo $key;?>_on').css('display', 'none');
				$('#f<?php echo $key;?>_off').css('display', 'block');
				<?php
			}
		}
	
	?>
		$('#f<?php echo $key;?>_on').click(function()  {
			$('#tid<?php echo $key;?>').css('display', 'none');
			$('#f<?php echo $key;?>_on').css('display', 'none');
			$('#f<?php echo $key;?>_off').css('display', 'block');

			$.ajax({
				type: "POST",
				data: "formname=<?php echo $formname;?>&visibility=0",
				url: "../shared/ajx_design_brief_formstaes.php",
				success: function(msg){
				}
			});
		});
		
		$('#f<?php echo $key;?>_off').click(function()  {
			$('#tid<?php echo $key;?>').css('display', 'block');
			$('#f<?php echo $key;?>_on').css('display', 'block');
			$('#f<?php echo $key;?>_off').css('display', 'none');

			$.ajax({
				type: "POST",
				data: "formname=<?php echo $formname;?>&visibility=1",
				url: "../shared/ajx_design_brief_formstaes.php",
				success: function(msg){
				}
			});
		});
	<?php
	}
	?>
	
});




</script>


	<?php
$page->footer();

?>