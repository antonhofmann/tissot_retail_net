<?php
/********************************************************************

    project_edit_cer_data.php

    Edit Retail Assignements.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2010-8-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2010-8-16
    Version:        1.0.0

    Copyright (c) 2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_state_constants.php";

check_access("can_edit_retail_data");

register_param("pid");
set_referer("project_confirm_to_client.php");


/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));


//check if basic data exists for this project
$result = build_missing_cer_records(param("pid"));


//get investments
$keymoney = get_pos_intangibles(param("pid"), 15);
$goodwill = get_pos_intangibles(param("pid"), 17);
$construction = get_pos_intangibles(param("pid"), 1);
$fixturing = get_pos_intangibles(param("pid"), 3);
$architectural = get_pos_intangibles(param("pid"), 5);
$equipment = get_pos_intangibles(param("pid"), 7);
$deposit = get_pos_intangibles(param("pid"), 9);
$other1 = get_pos_intangibles(param("pid"), 11);
$other2 = get_pos_intangibles(param("pid"), 13);
$dismantling = get_pos_intangibles(param("pid"), 18);
$landlord_contribution = get_pos_intangibles(param("pid"), 19);

//get cer_approval data
$approved_by = "";
$approved_date = "";
$approved3_by = "";
$approved3_date = "";
$cer_number = "";

$sql = "select cer_summary_in01_cernr, cer_summary_in01_approval_by, cer_summary_in01_approval_date, " .
       "cer_summary_in03_approval_by, cer_summary_in03_approval_date " .
       "from cer_summary ". 
	   " where cer_summary_project = " . param("pid");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$approved_by = $row["cer_summary_in01_approval_by"];
	$approved_date = to_system_date($row["cer_summary_in01_approval_date"]);

	$approved3_by = $row["cer_summary_in03_approval_by"];
	$approved3_date = to_system_date($row["cer_summary_in03_approval_date"]);

	$cer_number = $row["cer_summary_in01_cernr"];
}


$currency = get_cer_currency(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);


/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);

require_once "include/project_head_small.php";


//currency
$form->add_section("Currency Information");

if($currency["symbol"]) {
	$form->add_label("currency", "Currency", 0, $currency["symbol"]);
}
else
{
	$form->add_list("currency_id", "Currency",
    "select currency_id, currency_symbol from currencies order by currency_symbol");
}


if($currency["exchange_rate"] > 0) {
	$form->add_label("cer_basicdata_exchangerate", "Exchange Rate", 0, $currency["exchange_rate"]);
}
else
{
	$form->add_edit("cer_basicdata_exchangerate", "Exchange Rate", 0, $currency["exchange_rate"], TYPE_DECIMAL, 10, 6);
}


//Investments
$total = 0;
$form->add_section("Investments in " . $currency["symbol"]);

$form->add_label("construction", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, number_format($construction["cer_investment_amount_cer_loc"], 2, ".", "'"));
$total = $construction["cer_investment_amount_cer_loc"];

$form->add_label("fixturing", $fixturing["posinvestment_type_name"] . " in " . $currency["symbol"], 0, number_format($fixturing["cer_investment_amount_cer_loc"], 2, ".", "'"));
$form->add_label("architectural", $architectural["posinvestment_type_name"] . " in " . $currency["symbol"], 0, number_format($architectural["cer_investment_amount_cer_loc"], 2, ".", "'"));



$form->add_label("equipment", $equipment["posinvestment_type_name"] ." in " . $currency["symbol"], 0, number_format($equipment["cer_investment_amount_cer_loc"], 2, ".", "'"));

$form->add_label("other1", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0, number_format($other1["cer_investment_amount_cer_loc"], 2, ".", "'"));

$total = $total + $fixturing["cer_investment_amount_cer_loc"] + $architectural["cer_investment_amount_cer_loc"] + $equipment["cer_investment_amount_cer_loc"] + $other1["cer_investment_amount_cer_loc"];

$form->add_label("total", "Total Investments in Fixed Assets in " . $currency["symbol"], 0, number_format($total, 2, ".", "'"));

$form->add_label("keymoney", $keymoney["posinvestment_type_name"] ." in " . $currency["symbol"], 0, number_format($keymoney["cer_investment_amount_cer_loc"], 2, ".", "'"));

$form->add_label("deposit", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, number_format($deposit["cer_investment_amount_cer_loc"], 2, ".", "'"));

$form->add_label("landlord_contribution", $landlord_contribution["posinvestment_type_name"] ." in " . $currency["symbol"], 0, number_format($landlord_contribution["cer_investment_amount_cer_loc"], 2, ".", "'"));


$form->add_label("other2", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, number_format($other2["cer_investment_amount_cer_loc"], 2, ".", "'"));


$form->add_label("dismantling", $dismantling["posinvestment_type_name"] ." in " . $currency["symbol"], 0, number_format($dismantling["cer_investment_amount_cer_loc"], 2, ".", "'"));


$total2 = $total + $keymoney["cer_investment_amount_cer_loc"] + $deposit["cer_investment_amount_cer_loc"] + $other2["cer_investment_amount_cer_loc"] + $dismantling["cer_investment_amount_cer_loc"] - $landlord_contribution["cer_investment_amount_cer_loc"];
$form->add_label("total2", "Total amount in " . $currency["symbol"], 0, number_format($total2, 2, ".", "'"));




//KL approved add fields for editing
$total_klapproved = 0;
$form->add_section("KL approved Investments in " . $currency["symbol"]);



$form->add_hidden("h_construction", $construction["cer_investment_amount_cer_loc"]);
$form->add_hidden("h_fixturing", $fixturing["cer_investment_amount_cer_loc"]);
$form->add_hidden("h_architectural", $architectural["cer_investment_amount_cer_loc"]);
$form->add_hidden("h_equipment", $equipment["cer_investment_amount_cer_loc"]);
$form->add_hidden("h_other1", $other1["cer_investment_amount_cer_loc"]);
$form->add_hidden("h_keymoney", $keymoney["cer_investment_amount_cer_loc"]);
$form->add_hidden("h_deposit", $deposit["cer_investment_amount_cer_loc"]);
$form->add_hidden("h_other2", $other2["cer_investment_amount_cer_loc"]);
$form->add_hidden("h_dismantling", $dismantling["cer_investment_amount_cer_loc"]);

$link = '<a id="copy_amounts" href="javascript:void(0);">Copy amounts from investments</a>';
$form->add_comment($link);

$form->add_edit("cer_summary_in01_cernr", "CER Number", 0, $cer_number);

$form->add_edit("cer_summary_in01_approval_date", "Approval Date", 0, $approved_date, TYPE_DATE, 10);
$form->add_edit("cer_summary_in01_approval_by", "Approved by", 0, $approved_by);

$form->add_edit("construction_approved", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $construction["cer_investment_amount_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$total_klapproved = $construction["cer_investment_amount_cer_loc_approved"];

$form->add_edit("fixturing_approved", $fixturing["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $fixturing["cer_investment_amount_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$form->add_edit("architectural_approved", $architectural["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $architectural["cer_investment_amount_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$form->add_edit("equipment_approved", $equipment["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $equipment["cer_investment_amount_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$form->add_edit("other1_approved", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other1["cer_investment_amount_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$form->add_edit("dismantling_approved", $dismantling["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $dismantling["cer_investment_amount_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$total_klapproved = $total_klapproved + $fixturing["cer_investment_amount_cer_loc_approved"] + $architectural["cer_investment_amount_cer_loc_approved"] + $equipment["cer_investment_amount_cer_loc_approved"] + $other1["cer_investment_amount_cer_loc_approved"] + $dismantling["cer_investment_amount_cer_loc_approved"];

$form->add_label("approved_total", "Total KL approved Investments in Fixed Assets in " . $currency["symbol"], 0, number_format($total_klapproved, 2, ".", "'"));

$form->add_edit("keymoney_approved", $keymoney["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $keymoney["cer_investment_amount_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$form->add_edit("deposit_approved", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $deposit["cer_investment_amount_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$form->add_edit("other2_approved", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

//$form->add_label("deposite", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $deposit["cer_investment_amount_cer_loc"]);
//$form->add_label("noncapitalized", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_cer_loc"]);

$total_klapproved2 = $total_klapproved + $keymoney["cer_investment_amount_cer_loc_approved"] + $deposit["cer_investment_amount_cer_loc_approved"] + $other2["cer_investment_amount_cer_loc_approved"];

$form->add_label("approved_total2", "Total Project Costs KL approved amount in " . $currency["symbol"], 0, number_format($total_klapproved2, 2, ".", "'"));



//KL approved additional funding fields for editing
$total_additional = 0;
$form->add_section("KL approved Additional Funding in " . $currency["symbol"]);


$form->add_hidden("h_construction_in03", $construction["cer_investment_amount_additional_cer_loc"]);
$form->add_hidden("h_fixturing_in03", $fixturing["cer_investment_amount_additional_cer_loc"]);
$form->add_hidden("h_architectural_in03", $architectural["cer_investment_amount_additional_cer_loc"]);
$form->add_hidden("h_equipment_in03", $equipment["cer_investment_amount_additional_cer_loc"]);
$form->add_hidden("h_other1_in03", $other1["cer_investment_amount_additional_cer_loc"]);
$form->add_hidden("h_keymoney_in03", $keymoney["cer_investment_amount_additional_cer_loc"]);
$form->add_hidden("h_deposit_in03", $deposit["cer_investment_amount_additional_cer_loc"]);
$form->add_hidden("h_other2_in03", $other2["cer_investment_amount_additional_cer_loc"]);
$form->add_hidden("h_dismantling", $dismantling["cer_investment_amount_additional_cer_loc"]);

$link = '<a id="copy_amounts_in03" href="javascript:void(0);">Copy amounts from IN03</a>';
$form->add_comment($link);

$form->add_edit("cer_summary_in03_approval_date", "Approval Date", 0, $approved3_date, TYPE_DATE, 10);
$form->add_edit("cer_summary_in03_approval_by", "Approved by", 0, $approved3_by);

$form->add_edit("construction_additional", $construction["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $construction["cer_investment_amount_additional_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$total_additional = $construction["cer_investment_amount_additional_cer_loc_approved"];

$form->add_edit("fixturing_additional", $fixturing["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $fixturing["cer_investment_amount_additional_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$form->add_edit("architectural_additional", $architectural["posinvestment_type_name"] . " in " . $currency["symbol"], 0, $architectural["cer_investment_amount_additional_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$form->add_edit("equipment_additional", $equipment["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $equipment["cer_investment_amount_additional_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$form->add_edit("other1_additional", $other1["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other1["cer_investment_amount_additional_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$form->add_edit("dismantling_additional", $dismantling["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $dismantling["cer_investment_amount_additional_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$total_additional = $total_additional + $fixturing["cer_investment_amount_additional_cer_loc_approved"] + $architectural["cer_investment_amount_additional_cer_loc_approved"] + $equipment["cer_investment_amount_additional_cer_loc_approved"] + $other1["cer_investment_amount_additional_cer_loc_approved"] + $dismantling["cer_investment_amount_additional_cer_loc_approved"];

$form->add_label("additional_total", "Total additional Investments in Fixed Assets in " . $currency["symbol"], 0, number_format($total_additional, 2, ".", "'"));

$form->add_edit("keymoney_additional", $keymoney["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $keymoney["cer_investment_amount_additional_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$form->add_edit("deposit_additional", $deposit["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $deposit["cer_investment_amount_additional_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$form->add_edit("other2_additional", $other2["posinvestment_type_name"] ." in " . $currency["symbol"], 0, $other2["cer_investment_amount_additional_cer_loc_approved"], TYPE_DECIMAL, 12, 2);

$total_additional2 = $total_additional + $keymoney["cer_investment_amount_additional_cer_loc_approved"] + $deposit["cer_investment_amount_additional_cer_loc_approved"] + $other2["cer_investment_amount_additional_cer_loc_approved"];

$form->add_label("additional_total2", "Total additional amount in " . $currency["symbol"], 0, number_format($total_additional2, 2, ".", "'"));

$form->add_button("save", "Save Data");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("save"))
{
    if($form->validate())
	{
		
		//cer basic data
		if(array_key_exists('currency_id', $form->items)) {
		
			$fields = array();
			$factor = 1;
			$sql = "select * from currencies where currency_id = " . $form->value("currency_id");
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$factor = $row["currency_factor"];
			}

			$fields[] = "cer_basicdata_currency = " . $form->value("currency_id");
			$fields[] = "cer_basicdata_factor = " . $factor;


			$value = dbquote($form->value("cer_basicdata_exchangerate"));
			$fields[] = "cer_basicdata_exchangerate = " . $value;

			$fields[] = "cer_basicdata_exchangerate2 = " . $value;

			$value = dbquote(date("Y-m-d"));
			$fields[] = "cer_basicdata_exr_date = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_basicdata set " . join(", ", $fields) . 
				   " where cer_basicdata_version = 0 and cer_basicdata_project = " . param("pid");
			mysql_query($sql) or dberror($sql);

		}


		//cer summary
		$fields = array();
		
		
		$value = dbquote($form->value("cer_summary_in01_cernr"));
		$fields[] = "cer_summary_in01_cernr = " . $value;

		$value = dbquote(from_system_date($form->value("cer_summary_in01_approval_date")));
		$fields[] = "cer_summary_in01_approval_date = " . $value;
		$value = dbquote($form->value("cer_summary_in01_approval_by"));
		$fields[] = "cer_summary_in01_approval_by = " . $value;

		$value = dbquote(from_system_date($form->value("cer_summary_in03_approval_date")));
		$fields[] = "cer_summary_in03_approval_date = " . $value;
		$value = dbquote($form->value("cer_summary_in03_approval_by"));
		$fields[] = "cer_summary_in03_approval_by = " . $value;
		
		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_summary set " . join(", ", $fields) . " where cer_summary_project = " . param("pid");
		mysql_query($sql) or dberror($sql);

				
		//save investment type construction
		$fields = array();
    
		$value = dbquote($form->value("construction_approved"));
		$fields[] = "cer_investment_amount_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $construction["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type fixturing
		$fields = array();
    
		$value = dbquote($form->value("fixturing_approved"));
		$fields[] = "cer_investment_amount_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $fixturing["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		//save investment type architectural
		$fields = array();
    
		$value = dbquote($form->value("architectural_approved"));
		$fields[] = "cer_investment_amount_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $architectural["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type equipment
		$fields = array();
    
		$value = dbquote($form->value("equipment_approved"));
		$fields[] = "cer_investment_amount_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $equipment["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		
		//save investment type keymoney
		$fields = array();
    
		$value = dbquote($form->value("keymoney_approved"));
		$fields[] = "cer_investment_amount_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " .$value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}

		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $keymoney["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		//save investment type deposit
		$fields = array();
    
		$value = dbquote($form->value("deposit_approved"));
		$fields[] = "cer_investment_amount_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " .$value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $deposit["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type other costs
		$fields = array();
    
		$value = dbquote($form->value("other1_approved"));
		$fields[] = "cer_investment_amount_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $other1["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		//save investment type other non captalized costs
		$fields = array();
    
		$value = dbquote($form->value("other2_approved"));
		$fields[] = "cer_investment_amount_cer_loc_approved = " . $value;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$project_fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $other2["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		//save investment type dismantling
		$fields = array();
    
		$value = dbquote($form->value("dismantling_approved"));
		$fields[] = "cer_investment_amount_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $dismantling["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		$total_klapproved = $form->value('construction_approved') + $form->value('fixturing_approved')  + $form->value('architectural_approved')  + $form->value('equipment_approved')  + $form->value('other1_approved') + $form->value('dismantling_approved');

		$form->value("approved_total", number_format($total_klapproved, 2, ".", "'"));
		
		$total_klapproved2 = $total_klapproved + $form->value("deposit_approved") + $form->value("other2_approved");

		$form->value("approved_total2", number_format($total_klapproved2, 2, ".", "'"));



		//additional funding
		
		
		//save investment type construction
		$fields = array();
    
		$value = dbquote($form->value("construction_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $construction["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type fixturing
		$fields = array();
    
		$value = dbquote($form->value("fixturing_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $fixturing["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		//save investment type architectural
		$fields = array();
    
		$value = dbquote($form->value("architectural_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $architectural["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type equipment
		$fields = array();
    
		$value = dbquote($form->value("equipment_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $equipment["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		
		
		//save investment type keymoney
		$fields = array();
    
		$value = dbquote($form->value("keymoney_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " .$value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $keymoney["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		
		//save investment type deposit
		$fields = array();
    
		$value = dbquote($form->value("deposit_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " .$value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $deposit["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type other costs
		$fields = array();
    
		$value = dbquote($form->value("other1_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $other1["cer_investment_id"];
		mysql_query($sql) or dberror($sql);

		//save investment type other non captalized costs
		$fields = array();
    
		$value = dbquote($form->value("other2_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc_approved = " . $value;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$project_fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $other2["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		//save investment type dismantling
		$fields = array();
    
		$value = dbquote($form->value("dismantling_additional"));
		$fields[] = "cer_investment_amount_additional_cer_loc_approved = " . $value;

		$value1 = "current_timestamp";
		$fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $dismantling["cer_investment_id"];
		mysql_query($sql) or dberror($sql);


		$total_additional = $form->value('construction_additional') + $form->value('fixturing_additional')  + $form->value('architectural_additional')  + $form->value('equipment_additional')  + $form->value('other1_additional') + $form->value('dismantling_additional');

		$form->value("additional_total", number_format($total_additional, 2, ".", "'"));
		
		$total_additional2 = $total_additional + $form->value("deposit_additional") + $form->value("other2_additional");

		$form->value("additional_total2", number_format($total_additional2, 2, ".", "'"));



		//build missing investment records
		//$result = update_investments_from_the_list_of_materials($project['project_id'], $project['project_order'], 0);
		//$result = update_approved_investments_from_the_list_of_materials($project['project_id'], $project['project_order'], 0);

		$link = "project_edit_cer_data.php?pid=" . param("pid");
		redirect($link);
	}  
}


    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit CER Data");
$form->render();

?>
<script languege="javascript">
$(document).ready(function() {

	$("#copy_amounts").click(function () 
	{ 
		$("#construction_approved").val($("#h_construction").val());
		$("#fixturing_approved").val($("#h_fixturing").val());
		$("#architectural_approved").val($("#h_architectural").val());
		$("#equipment_approved").val($("#h_equipment").val());
		$("#other1_approved").val($("#h_other1").val());
		$("#keymoney_approved").val($("#h_keymoney").val());
		$("#deposit_approved").val($("#h_deposit").val());
		$("#other2_approved").val($("#h_other2").val());


		
	});

	$("#copy_amounts_in03").click(function () 
	{ 
		$("#construction_additional").val($("#h_construction_in03").val());
		$("#fixturing_additional").val($("#h_fixturing_in03").val());
		$("#architectural_additional").val($("#h_architectural_in03").val());
		$("#equipment_additional").val($("#h_equipment_in03").val());
		$("#other1_additional").val($("#h_other1_in03").val());
		$("#keymoney_additional").val($("#h_keymoney_in03").val());
		$("#deposit_additional").val($("#h_deposit_in03").val());
		$("#other2_additional").val($("#h_other2_in03").val());


		
	});
});
</script>

<?php

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>