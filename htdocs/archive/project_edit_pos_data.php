<?php
/********************************************************************

    project_edit_pos_data.php

    Edit POS Data

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-02-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-02-18
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_edit_pos_data");


/********************************************************************
    prepare all data needed
*********************************************************************/
$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));


$tracking_info = array();
$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
	   "concat(user_name, ' ', user_firstname) as user_name " . 
	   "from projecttracking " . 
       "left join users on user_id = projecttracking_user_id " . 
       "where projecttracking_project_id = " . param("pid") . 
	   " and projecttracking_field = 'project_real_opening_date' " . 
	   " order by projecttracking_time";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$tracking_info[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
		"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
		"projecttracking_comment"=>$row["projecttracking_comment"],
		"projecttracking_time"=>$row["projecttracking_time"],
		"user_name"=>$row["user_name"]
		);
}

// get company's address
$client_address = get_address($project["order_client_address"]);


$table = "posaddresses";
$table2 = "posareas";

if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
	$pos_order_sql = "select * from posorders where posorder_order = " . $project["order_id"];
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
	$table = "posaddressespipeline";
	$table2 = "posareaspipeline";

	$pos_order_sql = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
}

//$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
//$pos_order_sql = "select * from posorders where posorder_order = " . $project["order_id"];


//posareas
$posareas = array();
$sql = "select posareatype_id, posareatype_name " . 
	   "from posareatypes " . 
	   " order by posareatype_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$posareas[$row["posareatype_id"]]	= $row["posareatype_name"];
}

$pos_posareas = array();

if(count($pos_data) > 0)
{
	$sql = "select posarea_area " . 
		   "from $table2 " . 
		   "where posarea_posaddress = " . dbquote($pos_data["posaddress_id"]) . 
		   " order by posarea_area";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$pos_posareas[$row["posarea_area"]]	= $row["posarea_area"];
		
	}
}

//neighbourhood
$neighbourhoods = array();
$neighbourhoods_business_types = array();
$neighbourhoods_price_ranges = array();
$res = mysql_query($pos_order_sql) or dberror($pos_order_sql);
if ($row = mysql_fetch_assoc($res))
{
	$neighbourhoods["Shop on Left Side"] = $row["posorder_neighbour_left"];
	$neighbourhoods["Shop on Right Side"] = $row["posorder_neighbour_right"];
	$neighbourhoods["Shop Across Left Side"] = $row["posorder_neighbour_acrleft"];
	$neighbourhoods["Shop Across Right Side"] = $row["posorder_neighbour_acrright"];
	$neighbourhoods["Other Brands in Area"] = $row["posorder_neighbour_brands"];

	$neighbourhoods_business_types["Shop on Left Side"] = $row["posorder_neighbour_left_business_type"];
	$neighbourhoods_business_types["Shop on Right Side"] = $row["posorder_neighbour_right_business_type"];
	$neighbourhoods_business_types["Shop Across Left Side"] = $row["posorder_neighbour_acrleft_business_type"];
	$neighbourhoods_business_types["Shop Across Right Side"] = $row["posorder_neighbour_acrright_business_type"];

	$neighbourhoods_price_ranges["Shop on Left Side"] = $row["posorder_neighbour_left_price_range"];
	$neighbourhoods_price_ranges["Shop on Right Side"] = $row["posorder_neighbour_right_price_range"];
	$neighbourhoods_price_ranges["Shop Across Left Side"] = $row["posorder_neighbour_acrleft_price_range"];
	$neighbourhoods_price_ranges["Shop Across Right Side"] = $row["posorder_neighbour_acrright_price_range"];
}


//get business types and price ranges

$businesstypes = array();
$sql = "select businesstype_id, businesstype_text " . 
	   "from businesstypes " . 
	   " order by businesstype_text";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$businesstypes[$row["businesstype_id"]]	= $row["businesstype_text"];
}
//$businesstypes["---"]	= "---------------------------------";
//$businesstypes[999999]	= "Other not listed above";

$priceranges = array();
$sql = "select pricerange_id, pricerange_text " . 
	   "from priceranges " . 
	   " order by pricerange_text";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$priceranges[$row["pricerange_id"]]	= $row["pricerange_text"];
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);
$form->add_hidden("pipeline", $project["pipeline"]);



require_once "include/project_head_small.php";

$form->add_section("POS Location Address");

if(count($pos_data) > 0)
{
	
	$form->add_hidden("posaddress_id", $pos_data["posaddress_id"]);
	
	if( $project["project_projectkind"] == 8) //popup
	{
		$form->add_edit("popup_name", "PopUp Description*", NOTNULL, $project["project_popup_name"], TYPE_CHAR, 0, 0, 2, "popup");
	}
	else
	{
		$form->add_hidden("popup_name");
	}
	
	$form->add_label("shop_address_company", "Project Name", 0, $pos_data["posaddress_name"]);
	$form->add_label("shop_address_company2", "", 0, $pos_data["posaddress_name2"]);
	$form->add_label("shop_address_address", "Street", 0, $pos_data["posaddress_address"]);
	$form->add_label("shop_address_address2", "Additional Address Info", 0, $pos_data["posaddress_address2"]);
	$form->add_label("shop_address_zip", "ZIP", 0, $pos_data["posaddress_zip"]);
	$form->add_label("shop_address_place", "City", 0 , $pos_data["place_name"]);
	$form->add_label("province", "Province", 0, $pos_data["province_canton"]);

	$form->add_label("country", "Country", 0, $pos_data["country_name"]);
	$form->add_hidden("shop_address_country", $project["order_shop_address_country"]);


	$form->add_section("Google Map Coordinates");
	$form->add_label("GM", "Google Map", RENDER_HTML);
	$form->add_edit("posaddress_google_lat", "Latitude", NOTNULL | DISABLED, $pos_data["posaddress_google_lat"]);
	$form->add_edit("posaddress_google_long", "Longitude", NOTNULL | DISABLED, $pos_data["posaddress_google_long"]);



	$form->add_section("Environment");
	foreach($posareas as $key=>$name)
	{
		if(array_key_exists($key, $pos_posareas))
		{
			$form->add_checkbox("area". $key, $name, 1, 0, "");
		}
		else
		{
			$form->add_checkbox("area". $key, $name, "", 0, "");
		}

	}

	$form->add_comment("Please indicate on which floor the POS will be situated (Ground floor, Street Level, 1st Floor etc.).");

	$form->add_edit("project_floor", "Floor*", NOTNULL,$project["project_floor"], TYPE_CHAR, 30);


	$form->add_section("Neighbourhood");
	$form->add_edit("posorder_neighbour_left", "Shop on Left Side", 0, $neighbourhoods["Shop on Left Side"]);

	$form->add_list("posorder_neighbour_left_business_type", "Business Type Shop on Left Side", $businesstypes, 0, $neighbourhoods_business_types["Shop on Left Side"]);
	if(param("posorder_neighbour_left_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_left_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_left_business_type_new");
	}
	
	$form->add_list("posorder_neighbour_left_price_range", "Price Range Shop on Left Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop on Left Side"]);

	$form->add_edit("posorder_neighbour_right", "Shop on Right Side", 0, $neighbourhoods["Shop on Right Side"]);

	$form->add_list("posorder_neighbour_right_business_type", "Business Type Shop on Right Side", $businesstypes, 0, $neighbourhoods_business_types["Shop on Right Side"]);

	if(param("posorder_neighbour_right_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_right_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_right_business_type_new");
	}

	$form->add_list("posorder_neighbour_right_price_range", "Price Range Shop on Right Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop on Right Side"]);

	$form->add_edit("posorder_neighbour_acrleft", "Shop Across Left Side", 0, $neighbourhoods["Shop Across Left Side"]);
	$form->add_list("posorder_neighbour_acrleft_business_type", "Business Type  Across Left Side", $businesstypes, 0, $neighbourhoods_business_types["Shop Across Left Side"]);

	if(param("posorder_neighbour_acrleft_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_acrleft_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_acrleft_business_type_new");
	}

	$form->add_list("posorder_neighbour_acrleft_price_range", "Price Range Shop  Across Left Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop Across Left Side"]);

	$form->add_edit("posorder_neighbour_acrright", "Shop Across Right Side", 0, $neighbourhoods["Shop Across Right Side"]);

	$form->add_list("posorder_neighbour_acrright_business_type", "Business Type Shop Across Right Side", $businesstypes, 0, $neighbourhoods_business_types["Shop Across Right Side"]);

	if(param("posorder_neighbour_acrright_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_acrright_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_acrright_business_type_new");
	}

	$form->add_list("posorder_neighbour_acrright_price_range", "Price Range Shop Across Right Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop Across Right Side"]);

	$form->add_multiline("posorder_neighbour_brands", "Other Brands in Area", 4, 0, $neighbourhoods["Other Brands in Area"]);
	
	
	if (has_access("can_edit_treatment_state"))
	{
		$form->add_section("Treatment State");
		
		
		if($project["project_actual_opening_date"] and $project["project_actual_opening_date"]<>"0000-00-00")
		{
			$form->add_lookup("project_state", "Treatment State", "project_states", "project_state_text", 0, $project["project_state"]);
		}
		else
		{
			$sql = "select project_state_id, project_state_text from " .
				   " project_states where project_state_selectable = 1  or project_state_id = " . dbquote($project["project_state"]);
			$form->add_list("project_state", "Treatment State", $sql, 0, $project["project_state"]);
		}
		
		
	}
	else
	{
		$form->add_hidden("project_state", $project["project_state"]);
	}

	if(in_array(4, $user_roles) and has_access("can_edit_pos_calendar_data"))
	{
		if(in_array(2, $user_roles) or in_array(3, $user_roles) or in_array(10, $user_roles) or in_array(80, $user_roles))
		{
			
			if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
			{
				$form->add_section("Project Dates");
			}
			else
			{
				$form->add_section("POS Opening Dates");
			}

			

			if($project["project_projectkind"] == 3
				or $project["project_projectkind"] == 9) //Take Over and renovation
			{
				$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));
			}

			$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));

			if(count($tracking_info) > 0) {
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"],  DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
			}
			else
			{
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
			}


			$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);

			$form->add_section("POS Closing Dates");
			if($project["project_projectkind"] == 8) // popup
			{
				$form->add_hidden("shop_closing_date");
				$form->add_edit("popup_closing_date", "PopUp Closing Date", 0, to_system_date($project["project_popup_closingdate"]), TYPE_DATE, 20);
			}
			else
			{
				$form->add_hidden("popup_closing_date");
				$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
			}
		}
		elseif($project["order_actual_order_state_code"] >= '800')
		{
			if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
			{
				$form->add_section("Project Dates");
			}
			else
			{
				$form->add_section("POS Opening Dates");
			}

			if($project["project_projectkind"] == 3
				or $project["project_projectkind"] == 9) //Take Over and renovation
			{
				$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));
			}

			$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));

			
			if(count($tracking_info) > 0) {
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
			
			}
			else
			{
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
			}


			$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);


			$form->add_section("POS Closing Dates");
			if($project["project_projectkind"] == 8) // popup
			{
				$form->add_hidden("shop_closing_date");
				$form->add_edit("popup_closing_date", "PopUp Closing Date", 0, to_system_date($project["project_popup_closingdate"]), TYPE_DATE, 20);
			}
			else
			{
				$form->add_hidden("popup_closing_date");
				$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
			}
		}
		else
		{
			if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
			{
				$form->add_section("Project Dates");
			}
			else
			{
				$form->add_section("POS Opening Dates");
			}

			if($project["project_projectkind"] == 3
				or $project["project_projectkind"] == 9) //Take Over and renovation
			{
				$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));
			}

			$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));


			$form->add_label("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
			$form->add_label("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

			if($project["project_projectkind"] == 8) // popup
			{
				$form->add_hidden("shop_closing_date");
				$form->add_label("popup_closing_date", "PopUp Closing Date", 0, to_system_date($project["project_popup_closingdate"]));
			}
			else
			{
				$form->add_hidden("popup_closing_date");
				$form->add_label("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]));
			}
		}
	}
	elseif (has_access("can_edit_pos_calendar_data"))
	{
		
		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			$form->add_section("Project Dates");
		}
		else
		{
			$form->add_section("POS Opening Dates");
		}

		if($project["project_projectkind"] == 3
			or $project["project_projectkind"] == 9) //Take Over and renovation
		{
			$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));
		}

		$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));


		if(count($tracking_info) > 0) {
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
		
		}
		else
		{
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
		}

		
		$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);


		$form->add_section("POS Closing Dates");
		if($project["project_projectkind"] == 8) // popup
		{
			$form->add_hidden("shop_closing_date");
			$form->add_edit("popup_closing_date", "PopUp Closing Date", 0, to_system_date($project["project_popup_closingdate"]), TYPE_DATE, 20);
		}
		else
		{
			$form->add_hidden("popup_closing_date");
			$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
		}
	}
	else
	{
		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			$form->add_section("Project Dates");
		}
		else
		{
			$form->add_section("POS Opening Dates");
		}

		if($project["project_projectkind"] == 3
			or $project["project_projectkind"] == 9) //Take Over and renovation
		{
			$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));
		}

		$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));


		$form->add_label("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
		$form->add_label("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

		if($project["project_projectkind"] == 8) // popup
		{
			$form->add_hidden("shop_closing_date");
			$form->add_label("popup_closing_date", "PopUp Closing Date", 0, to_system_date($project["project_popup_closingdate"]));
		}
		else
		{
			$form->add_hidden("popup_closing_date");
			$form->add_label("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]));
		}
	}

	$form->add_hidden("old_shop_actual_opening_date", to_system_date($project["project_actual_opening_date"]));
	$form->add_hidden("old_shop_real_opening_date", to_system_date($project["project_real_opening_date"]));
	$form->add_hidden("old_shop_closing_date", to_system_date($project["project_shop_closingdate"]));

	$form->add_button("save", "Save Data");
}
else
{
	$pos_data["posaddress_google_lat"] = 0;
	$pos_data["posaddress_google_long"] = 0;


	$form->add_comment("The project has not been assigned to a POS yet.");


	$form->add_hidden("posaddress_id",0);

	if( $project["project_projectkind"] == 8)
	{
		$form->add_label("popup_name", "PopUp Description", 0, $project["project_popup_name"]);
	}
	else
	{
		$form->add_hidden("popup_name");
	}
	
	$form->add_label("shop_address_company", "Project Name", 0, $project["order_shop_address_company"]);
	$form->add_label("shop_address_company2", "", 0, $project["order_shop_address_company2"]);
	$form->add_label("shop_address_address", "Street", 0, $project["order_shop_address_address"]);
	$form->add_label("shop_address_address2", "Additional Address Info", 0, $project["order_shop_address_address2"]);
	$form->add_label("shop_address_zip", "ZIP", 0, $project["order_shop_address_zip"]);
	$form->add_label("shop_address_place", "City", 0 , $project["order_shop_address_place"]);
	$form->add_label("order_shop_address_country_name", "Country", 0, $project["order_shop_address_country_name"]);
	$form->add_hidden("shop_address_country", $project["order_shop_address_country"]);


	$form->add_section("Google Map Coordinates");
	$form->add_label("GM", "Google Map", RENDER_HTML);
	$form->add_edit("posaddress_google_lat", "Latitude", NOTNULL | DISABLED, $pos_data["posaddress_google_lat"]);
	$form->add_edit("posaddress_google_long", "Longitude", NOTNULL | DISABLED, $pos_data["posaddress_google_long"]);
	
		
	
	if (has_access("can_edit_treatment_state"))
	{
		$form->add_section("Treatment State");
		
		
		if($project["project_actual_opening_date"] and $project["project_actual_opening_date"]<>"0000-00-00")
		{
			$form->add_lookup("project_state", "Treatment State", "project_states", "project_state_text", 0, $project["project_state"]);
		}
		else
		{
			$sql = "select project_state_id, project_state_text from " .
				   " project_states  where project_state_selectable = 1";
			$form->add_list("project_state", "Treatment State", $sql, 0, $project["project_state"]);
		}
		
		
	}
	else
	{
		$form->add_hidden("project_state", $project["project_state"]);
	}

	if(in_array(4, $user_roles) and has_access("can_edit_pos_calendar_data"))
	{
		if(in_array(2, $user_roles) or in_array(3, $user_roles) or in_array(10, $user_roles))
		{
			
			if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
			{
				$form->add_section("Project Dates");
			}
			else
			{
				$form->add_section("POS Opening Dates");
			}

			if($project["project_projectkind"] == 3
				or $project["project_projectkind"] == 9) //Take Over and renovation
			{
				$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));
			}

			$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));


			if(count($tracking_info) > 0) {
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
			}
			else
			{
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
			}


			$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);

			$form->add_section("POS Closing Dates");
			$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
		}
		elseif($project["order_actual_order_state_code"] >= '800')
		{
			if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
			{
				$form->add_section("Project Dates");
			}
			else
			{
				$form->add_section("POS Opening Dates");
			}
			
			
			if($project["project_projectkind"] == 3
				or $project["project_projectkind"] == 9) //Take Over and renovation
			{
				$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));
			}

			$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));


			if(count($tracking_info) > 0) {
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
			
			}
			else
			{
				$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
			}


			$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);


			$form->add_section("POS Closing Dates");
			$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
		}
		else
		{
			if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
			{
				$form->add_section("Project Dates");
			}
			else
			{
				$form->add_section("POS Opening Dates");
			}

			if($project["project_projectkind"] == 3
				or $project["project_projectkind"] == 9) //Take Over and renovation
			{
				$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));
			}

			$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));


			$form->add_label("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
			$form->add_label("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

			$form->add_label("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]));
		}
	}
	elseif (has_access("can_edit_pos_calendar_data"))
	{
		
		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			$form->add_section("Project Dates");
		}
		else
		{
			$form->add_section("POS Opening Dates");
		}

		if($project["project_projectkind"] == 3
			or $project["project_projectkind"] == 9) //Take Over and renovation
			{
				$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));
			}

			$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));


		if(count($tracking_info) > 0) {
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
		
		}
		else
		{
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], DISABLED, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
		}

		
		$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);


		$form->add_section("POS Closing Dates");
		$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
	}
	else
	{
		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			$form->add_section("Project Dates");
		}
		else
		{
			$form->add_section("POS Opening Dates");
		}

		if($project["project_projectkind"] == 3
			or $project["project_projectkind"] == 9) //Take Over and renovation
			{
				$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));
			}

			$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));


		$form->add_label("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
		$form->add_label("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

		$form->add_label("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]));
	}

	$form->add_hidden("old_shop_actual_opening_date", to_system_date($project["project_actual_opening_date"]));
	$form->add_hidden("old_shop_real_opening_date", to_system_date($project["project_real_opening_date"]));
	$form->add_hidden("old_shop_closing_date", to_system_date($project["project_shop_closingdate"]));
	$form->add_hidden("project_floor", $project["project_floor"]);
	$form->add_hidden("popup_closing_date");

	$form->add_button("save", "Save Data");
}



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("save"))
{
    $error = '';
	if($form->value("shop_actual_opening_date"))
	{
		$form->add_validation("from_system_date({shop_actual_opening_date}) <=  " . dbquote(date("Y-m-d", strtotime("+3 days"))), "The actual POS opening date must be a date in the past or can only be at maximum three days in the future!");

	}

	if($form->value("shop_closing_date"))
	{
		$form->add_validation("from_system_date({shop_closing_date}) <=  " . dbquote(date("Y-m-d", strtotime("+2 days"))), "The POS closing date must be a date in the past or can only be at maximum two days in the future!");

	}


	if($form->value("shop_closing_date"))
	{
		$closing_date = from_system_date($form->value("shop_closing_date"));
		$opening_date = from_system_date($form->value("shop_actual_opening_date"));
		if($closing_date < $opening_date)
		{
			$error = 3;
		}
	}




	if($error == 3)
	{
		$form->error("The POS closing date can not be a date in the past of the POS opening date!");
	}
	elseif ($form->validate())
    {
	
		
		//check if POS Closing date is valid (max 2 days in the future)
		$error = 0;
		if($form->value("shop_closing_date"))
		{
			$closing_date = from_system_date($form->value("shop_closing_date"));
			$aftertomorrow = date('Y-m-d', strtotime(date("Y-m-d"). ' + 2 days'));
			if($closing_date > $aftertomorrow)
			{
				$error = 1;
			}
		}

		if($error == 0)
		{
			$project_fields = array();
			// update record in table projects

			if($form->value("shop_closing_date"))
			{
				$value = 5;
				$project_fields[] = "project_state = " . $value;
			}
			elseif($form->value("shop_actual_opening_date"))
			{
				$project_fields[] = "project_state = 4"; //open
			}
			else
			{
				$value = trim($form->value("project_state")) == "" ? "null" : dbquote($form->value("project_state"));
				$project_fields[] = "project_state = " . $value;
			}

			
			$value = dbquote(trim($form->value("project_floor")));
			$project_fields[] = "project_floor = " . $value;
			
			$value = trim($form->value("popup_name")) == "" ? "null" : dbquote($form->value("popup_name"));
			$project_fields[] = "project_popup_name = " . $value;
		
			$value = trim($form->value("shop_real_opening_date")) == "" ? "null" : dbquote(from_system_date($form->value("shop_real_opening_date")));
			$project_fields[] = "project_real_opening_date = " . $value;

			$value = trim($form->value("shop_actual_opening_date")) == "" ? "null" : dbquote(from_system_date($form->value("shop_actual_opening_date")));
			$project_fields[] = "project_actual_opening_date = " . $value;

			$value = trim($form->value("shop_closing_date")) == "" ? "null" : dbquote(from_system_date($form->value("shop_closing_date")));
			$project_fields[] = "project_shop_closingdate = " . $value;

			
			$value = trim($form->value("popup_closing_date")) == "" ? "null" : dbquote(from_system_date($form->value("popup_closing_date")));
			$project_fields[] = "project_popup_closingdate = " . $value;

			$value = "current_timestamp";
			$project_fields[] = "date_modified = " . $value;
			
			if (isset($_SESSION["user_login"]))
			{
				$value = dbquote($_SESSION["user_login"]);
				$project_fields[] = "user_modified = " . $value;
			}


			$sql = "update projects set " . join(", ", $project_fields) . " where project_id = " . $form->value("pid");
			mysql_query($sql) or dberror($sql);


			if($form->value("posaddress_id") > 0)
			{
				//update posorder
				$sql = "select posorder_id " . 
					   "from posorders " . 
					   "where posorder_order = " . $form->value("oid");
				
				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
				
				
					$fields = array();

					$value = dbquote($form->value("posorder_neighbour_left"));
					$fields[] = "posorder_neighbour_left = " . $value;

					$value = dbquote($form->value("posorder_neighbour_left_business_type"));
					$fields[] = "posorder_neighbour_left_business_type = " . $value;

					$value = dbquote($form->value("posorder_neighbour_left_price_range"));
					$fields[] = "posorder_neighbour_left_price_range = " . $value;

					$value = dbquote($form->value("posorder_neighbour_right"));
					$fields[] = "posorder_neighbour_right = " . $value;

					$value = dbquote($form->value("posorder_neighbour_right_business_type"));
					$fields[] = "posorder_neighbour_right_business_type = " . $value;

					$value = dbquote($form->value("posorder_neighbour_right_price_range"));
					$fields[] = "posorder_neighbour_right_price_range = " . $value;

					$value = dbquote($form->value("posorder_neighbour_acrleft"));
					$fields[] = "posorder_neighbour_acrleft = " . $value;

					$value = dbquote($form->value("posorder_neighbour_acrleft_business_type"));
					$fields[] = "posorder_neighbour_acrleft_business_type = " . $value;

					$value = dbquote($form->value("posorder_neighbour_acrleft_price_range"));
					$fields[] = "posorder_neighbour_acrleft_price_range = " . $value;

					$value = dbquote($form->value("posorder_neighbour_acrright"));
					$fields[] = "posorder_neighbour_acrright = " . $value;

					$value = dbquote($form->value("posorder_neighbour_acrright_business_type"));
					$fields[] = "posorder_neighbour_acrright_business_type = " . $value;

					$value = dbquote($form->value("posorder_neighbour_acrright_price_range"));
					$fields[] = "posorder_neighbour_acrright_price_range = " . $value;

					$value = dbquote($form->value("posorder_neighbour_brands"));
					$fields[] = "posorder_neighbour_brands = " . $value;

					$value = dbquote(from_system_date($form->value("shop_actual_opening_date")));
					$fields[] = "posorder_opening_date = " . $value;

					$value = dbquote(from_system_date($form->value("shop_closing_date")));
					$fields[] = "posorder_closing_date = " . $value;

					$value = dbquote(from_system_date($form->value("popup_closing_date")));
					$fields[] = "posorder_popup_closingdate = " . $value;

					$value = dbquote($form->value("popup_name"));
					$fields[] = "posorder_popup_name = " . $value;

					$sql = "update posorders set " . join(", ", $fields) . "  where posorder_id = " . $row["posorder_id"];
					mysql_query($sql) or dberror($sql);
					
					/*
					$sql = "Update posorders set " . 
						   "posorder_opening_date = " . dbquote(from_system_date($form->value("shop_actual_opening_date"))) . ", " .
						   "posorder_closing_date = " . dbquote(from_system_date($form->value("shop_closing_date"))) . 
						   " where posorder_id = " . $row["posorder_id"];

					mysql_query($sql) or dberror($sql);
					*/
				}
				elseif($form->value("shop_actual_opening_date"))
				{ 
					//check if project's order still is in piepeline
					$sql = "select * " . 
						   "from posorderspipeline " . 
						   "where posorder_order = " . $form->value("oid");

					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						
						$pos_id = $row["posorder_posaddress"];
						$pos_id_pipeline = $row["posorder_posaddress"];
						
						//check if posaddress is in pipelien
						if($row["posorder_parent_table"] = 'posaddressespipeline')
						{
							//transfer posaddress
							$posorder_pos_id = $row["posorder_posaddress"];

							$sql1 = "select * from posaddressespipeline where posaddress_id = " . $posorder_pos_id;
							$res1 = mysql_query($sql1) or dberror($sql1);
							if ($row1 = mysql_fetch_assoc($res1))
							{
								//pos address
								$fields = array();
								$values = array();

								$fields[] = "posaddress_client_id";
								$values[] = dbquote($row1["posaddress_client_id"]);

								$fields[] = "posaddress_ownertype";
								$values[] = dbquote($row1["posaddress_ownertype"]);

								$fields[] = "posaddress_franchisor_id";
								$values[] = dbquote($row1["posaddress_franchisor_id"]);

								$fields[] = "posaddress_franchisee_id";
								$values[] = dbquote($row1["posaddress_franchisee_id"]);

								$fields[] = "posaddress_name";
								$values[] = dbquote($row1["posaddress_name"]);

								$fields[] = "posaddress_name2";
								$values[] = dbquote($row1["posaddress_name2"]);

								$fields[] = "posaddress_address";
								$values[] = dbquote($row1["posaddress_address"]);

								$fields[] = "posaddress_street";
								$values[] = dbquote($row1["posaddress_street"]);

								$fields[] = "posaddress_street_number";
								$values[] = dbquote($row1["posaddress_street_number"]);

								$fields[] = "posaddress_address2";
								$values[] = dbquote($row1["posaddress_address2"]);

								$fields[] = "posaddress_zip";
								$values[] = dbquote($row1["posaddress_zip"]);

								$fields[] = "posaddress_place";
								$values[] = dbquote($row1["posaddress_place"]);

								$fields[] = "posaddress_place_id";
								$values[] = dbquote($row1["posaddress_place_id"]);

								$fields[] = "posaddress_country";
								$values[] = dbquote($row1["posaddress_country"]);
								
								$fields[] = "posaddress_phone";
								$values[] = dbquote($row1["posaddress_phone"]);

								$fields[] = "posaddress_phone_country";
								$values[] = dbquote($row1["posaddress_phone_country"]);

								$fields[] = "posaddress_phone_area";
								$values[] = dbquote($row1["posaddress_phone_area"]);

								$fields[] = "posaddress_phone_number";
								$values[] = dbquote($row1["posaddress_phone_number"]);


								$fields[] = "posaddress_mobile_phone";
								$values[] = dbquote($row1["posaddress_mobile_phone"]);

								$fields[] = "posaddress_mobile_phone_country";
								$values[] = dbquote($row1["posaddress_mobile_phone_country"]);

								$fields[] = "posaddress_mobile_phone_area";
								$values[] = dbquote($row1["posaddress_mobile_phone_area"]);

								$fields[] = "posaddress_mobile_phone_number";
								$values[] = dbquote($row1["posaddress_mobile_phone_number"]);

								$fields[] = "posaddress_email";
								$values[] = dbquote($row1["posaddress_email"]);

								$fields[] = "posaddress_google_lat";
								$values[] = dbquote($row1["posaddress_google_lat"]);

								$fields[] = "posaddress_google_long";
								$values[] = dbquote($row1["posaddress_google_long"]);

								$fields[] = "posaddress_google_precision";
								$values[] = dbquote($row1["posaddress_google_precision"]);

								$fields[] = "posaddress_store_postype";
								$values[] = dbquote($row1["posaddress_store_postype"]);

								$fields[] = "posaddress_store_subclass";
								$values[] = dbquote($row1["posaddress_store_subclass"]);

								$fields[] = "posaddress_store_furniture";
								$values[] = dbquote($row1["posaddress_store_furniture"]);

								$fields[] = "posaddress_store_totalsurface";
								$values[] = dbquote($row1["posaddress_store_totalsurface"]);

								$fields[] = "posaddress_store_retailarea";
								$values[] = dbquote($row1["posaddress_store_retailarea"]);

								$fields[] = "posaddress_store_backoffice";
								$values[] = dbquote($row1["posaddress_store_backoffice"]);

								$fields[] = "posaddress_store_numfloors";
								$values[] = dbquote($row1["posaddress_store_numfloors"]);

								$fields[] = "posaddress_store_floorsurface1";
								$values[] = dbquote($row1["posaddress_store_floorsurface1"]);

								$fields[] = "posaddress_store_floorsurface2";
								$values[] = dbquote($row1["posaddress_store_floorsurface2"]);

								$fields[] = "posaddress_store_floorsurface3";
								$values[] = dbquote($row1["posaddress_store_floorsurface3"]);


								$fields[] = "posaddress_fagagreement_type";
								$values[] = dbquote($row1["posaddress_fagagreement_type"]);

								$fields[] = "posaddress_fagrsent";
								$values[] = dbquote($row1["posaddress_fagrsent"]);

								$fields[] = "posaddress_fagrsigned";
								$values[] = dbquote($row1["posaddress_fagrsigned"]);

								$fields[] = "posaddress_fagrstart";
								$values[] = dbquote($row1["posaddress_fagrstart"]);

								$fields[] = "posaddress_fagrend";
								$values[] = dbquote($row1["posaddress_fagrend"]);

								$fields[] = "posaddress_fag_comment";
								$values[] = dbquote($row1["posaddress_fag_comment"]);

								$fields[] = "posaddress_perc_class";
								$values[] = dbquote($row1["posaddress_perc_class"]);

								$fields[] = "posaddress_perc_tourist";
								$values[] = dbquote($row1["posaddress_perc_tourist"]);

								$fields[] = "posaddress_perc_transport";
								$values[] = dbquote($row1["posaddress_perc_transport"]);

								$fields[] = "posaddress_perc_people";
								$values[] = dbquote($row1["posaddress_perc_people"]);

								$fields[] = "posaddress_perc_parking";
								$values[] = dbquote($row1["posaddress_perc_parking"]);

								$fields[] = "posaddress_perc_visibility1";
								$values[] = dbquote($row1["posaddress_perc_visibility1"]);

								$fields[] = "posaddress_perc_visibility2";
								$values[] = dbquote($row1["posaddress_perc_visibility2"]);

								$fields[] = "posaddress_export_to_web";
								$values[] = dbquote($row1["posaddress_export_to_web"]);

								$sql = "insert into posaddresses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
								mysql_query($sql) or dberror($sql);

								$new_posaddress_id = mysql_insert_id();
								$pos_id = mysql_insert_id();

								$sql = "delete from posaddressespipeline where posaddress_id = " . $posorder_pos_id;
								mysql_query($sql) or dberror($sql);


								//transfer posleases
								$sql = "select * from posleasespipeline where poslease_posaddress = " . dbquote($pos_id_pipeline);

								$res = mysql_query($sql) or dberror($sql);
								while ($row = mysql_fetch_assoc($res))
								{
									$fields = array();
									$values = array();


									$fields[] = "poslease_posaddress";
									$values[] = dbquote($pos_id);

									$fields[] = "poslease_order";
									$values[] = dbquote($row["poslease_ordere"]);

									$fields[] = "poslease_lease_type";
									$values[] = dbquote($row["poslease_lease_type"]);
									
									$fields[] = "poslease_anual_rent";
									$values[] = dbquote($row["poslease_anual_rent"]);

									$fields[] = "poslease_salespercent";
									$values[] = dbquote($row["poslease_salespercent"]);

									$fields[] = "poslease_indexclause_in_contract";
									$values[] = dbquote($row["poslease_indexclause_in_contract"]);

									$fields[] = "poslease_isindexed";
									$values[] = dbquote($row["poslease_isindexed"]);

									$fields[] = "poslease_tacit_renewal_duration_years";
									$values[] = dbquote($row["poslease_tacit_renewal_duration_years"]);

									$fields[] = "poslease_tacit_renewal_duration_months";
									$values[] = dbquote($row["poslease_tacit_renewal_duration_months"]);

									$fields[] = "poslease_indexrate";
									$values[] = dbquote($row["poslease_indexrate"]);

									$fields[] = "poslease_average_increase";
									$values[] = dbquote($row["poslease_average_increase"]);

									$fields[] = "poslease_realestate_fee";
									$values[] = dbquote($row["poslease_realestate_fee"]);

									$fields[] = "poslease_annual_charges";
									$values[] = dbquote($row["poslease_annual_charges"]);

									$fields[] = "poslease_other_fees";
									$values[] = dbquote($row["poslease_other_fees"]);
									
									$fields[] = "poslease_startdate";
									$values[] = dbquote($row["poslease_startdate"]);

									$fields[] = "poslease_enddate";
									$values[] = dbquote($row["poslease_enddate"]);

									$fields[] = "poslease_extensionoption";
									$values[] = dbquote($row["poslease_extensionoption"]);

									$fields[] = "poslease_handoverdate";
									$values[] = dbquote($row["poslease_handoverdate"]);

									$fields[] = "poslease_firstrentpayed";
									$values[] = dbquote($row["poslease_firstrentpayed"]);

									$fields[] = "poslease_exitoption";
									$values[] = dbquote($row["poslease_exitoption"]);

									$fields[] = "poslease_negotiator";
									$values[] = dbquote($row["poslease_negotiator"]);

									$fields[] = "poslease_landlord_name";
									$values[] = dbquote($row["poslease_landlord_name"]);

									$fields[] = "poslease_negotiated_conditions";
									$values[] = dbquote($row["poslease_negotiated_conditions"]);

									$fields[] = "poslease_termination_time";
									$values[] = dbquote($row["poslease_termination_time"]);

									$fields[] = "date_created";
									$values[] = dbquote(date("Y-m-d"));

									$fields[] = "date_modified";
									$values[] = dbquote(date("Y-m-d"));

									$fields[] = "user_created";
									$values[] = dbquote(user_login());

									$fields[] = "user_modified";
									$values[] = dbquote(user_login());

									$sql = "insert into posleases (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
									mysql_query($sql) or dberror($sql);
							
								}

								$sql = "delete from posleasespipeline where poslease_posaddress = " . dbquote($pos_id_pipeline);
								mysql_query($sql) or dberror($sql);


								//posareas
								$sql = "select * from posareaspipeline where posarea_posaddress = " . $pos_id_pipeline;

								$res = mysql_query($sql) or dberror($sql);
								while ($row = mysql_fetch_assoc($res))
								{
									$fields = array();
									$values = array();

									$fields[] = "posarea_posaddress";
									$values[] = dbquote($pos_id);

									$fields[] = "posarea_area";
									$values[] = dbquote($row["posarea_area"]);

									$fields[] = "date_created";
									$values[] = dbquote(date("Y-m-d"));

									$fields[] = "date_modified";
									$values[] = dbquote(date("Y-m-d"));

									$fields[] = "user_created";
									$values[] = dbquote(user_login());

									$fields[] = "user_modified";
									$values[] = dbquote(user_login());

									$sql = "insert into posareas (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
									mysql_query($sql) or dberror($sql);
								}

								$sql = "delete from posareaspipeline where posarea_posaddress = " . $pos_id_pipeline;
								mysql_query($sql) or dberror($sql);
							}
						}
						
						//transfer posorder
						$fields = array();
						$values = array();

						$fields[] = "posorder_posaddress";
						$values[] = dbquote($pos_id);

						$fields[] = "posorder_order";
						$values[] = dbquote($row["posorder_order"]);

						$fields[] = "posorder_type";
						$values[] = dbquote($row["posorder_type"]);

						$fields[] = "posorder_ordernumber";
						$values[] = dbquote($row["posorder_ordernumber"]);

						$fields[] = "posorder_year";
						$values[] = dbquote($row["posorder_year"]);

						$fields[] = "posorder_product_line";
						$values[] = dbquote($row["posorder_product_line"]);

						$fields[] = "posorder_product_line_subclass";
						$values[] = dbquote($row["posorder_product_line_subclass"]);

						$fields[] = "posorder_postype";
						$values[] = dbquote($row["posorder_postype"]);

						$fields[] = "posorder_subclass";
						$values[] = dbquote($row["posorder_subclass"]);

						$fields[] = "posorder_project_kind";
						$values[] = dbquote($row["posorder_project_kind"]);

						$fields[] = "posorder_legal_type";
						$values[] = dbquote($row["posorder_legal_type"]);

						$fields[] = "posorder_system_currency";
						$values[] = dbquote($row["posorder_system_currency"]);

						$fields[] = "posorder_budget_approved_sc";
						$values[] = dbquote($row["posorder_budget_approved_sc"]);

						$fields[] = "posorder_real_cost_sc";
						$values[] = dbquote($row["posorder_real_cost_sc"]);

						$fields[] = "posorder_client_currency";
						$values[] = dbquote($row["posorder_client_currency"]);

						$fields[] = "posorder_budget_approved_cc";
						$values[] = dbquote($row["posorder_budget_approved_cc"]);

						$fields[] = "posorder_real_cost_cc";
						$values[] = dbquote($row["posorder_real_cost_cc"]);

						
						$fields[] = "posorder_neighbour_left";
						$values[] = dbquote($row["posorder_neighbour_left"]);

						$fields[] = "posorder_neighbour_left_business_type";
						$values[] = dbquote($row["posorder_neighbour_left_business_type"]);

						$fields[] = "posorder_neighbour_left_price_range";
						$values[] = dbquote($row["posorder_neighbour_left_price_range"]);

						$fields[] = "posorder_neighbour_right";
						$values[] = dbquote($row["posorder_neighbour_right"]);

						$fields[] = "posorder_neighbour_right_business_type";
						$values[] = dbquote($row["posorder_neighbour_right_business_type"]);

						$fields[] = "posorder_neighbour_right_price_range";
						$values[] = dbquote($row["posorder_neighbour_right_price_range"]);


						$fields[] = "posorder_neighbour_acrleft";
						$values[] = dbquote($row["posorder_neighbour_acrleft"]);

						$fields[] = "posorder_neighbour_acrleft_business_type";
						$values[] = dbquote($row["posorder_neighbour_acrleft_business_type"]);

						$fields[] = "posorder_neighbour_acrleft_price_range";
						$values[] = dbquote($row["posorder_neighbour_acrleft_price_range"]);


						$fields[] = "posorder_neighbour_acrright";
						$values[] = dbquote($row["posorder_neighbour_acrright"]);

						$fields[] = "posorder_neighbour_acrright_business_type";
						$values[] = dbquote($row["posorder_neighbour_acrright_business_type"]);

						$fields[] = "posorder_neighbour_acrright_price_range";
						$values[] = dbquote($row["posorder_neighbour_acrright_price_range"]);


						$fields[] = "posorder_neighbour_brands";
						$values[] = dbquote($row["posorder_neighbour_brands"]);

						$fields[] = "posorder_neighbour_comment";
						$values[] = dbquote($row["posorder_neighbour_comment"]);

						$fields[] = "posorder_currency_symbol";
						$values[] = dbquote($row["posorder_currency_symbol"]);

						$fields[] = "posorder_exchangerate";
						$values[] = dbquote($row["posorder_exchangerate"]);

						$fields[] = "posorder_remark";
						$values[] = dbquote($row["posorder_remark"]);

						$fields[] = "posorder_project_locally_produced";
						$values[] = dbquote($row["posorder_project_locally_produced"]);

						$fields[] = "date_created";
						$values[] = dbquote(date("Y-m-d"));

						$fields[] = "date_modified";
						$values[] = dbquote(date("Y-m-d"));

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());
						
						$sql = "insert into posorders (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);


						$sql = "delete from posorderspipeline where posorder_order = " . $form->value("oid");
						mysql_query($sql) or dberror($sql);

					}
				}


				//update posareas
				$sql = "delete from $table2 where posarea_posaddress =" . dbquote($pos_data["posaddress_id"]);
				$result = $res = mysql_query($sql) or dberror($sql);
				foreach($posareas as $key=>$name)
				{
					if($form->value("area" . $key) == 1)
					{
						$fields = array();
						$values = array();

						$fields[] = "posarea_posaddress";
						$values[] = dbquote($pos_data["posaddress_id"]);

						$fields[] = "posarea_area";
						$values[] = dbquote($key);

						$fields[] = "date_created";
						$values[] = dbquote(date("Y-m-d"));

						$fields[] = "date_modified";
						$values[] = dbquote(date("Y-m-d"));

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into $table2 (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
				}


				$result = update_posdata_from_posorders($form->value("posaddress_id"));

				if($form->value("shop_actual_opening_date") and $form->value("posaddress_id") > 0)
				{
					update_store_locator($form->value("posaddress_id"));
				}

				set_owner_company_state($form->value("posaddress_id"), $form->value("shop_closing_date"));




				//update postracking
				if($form->value("shop_actual_opening_date"))
				{
					if($form->value("shop_closing_date") != $form->value("old_shop_closing_date"))
					{
					
						//project tracking
						$field = "posaddress_store_closingdate";
						$sql = "Insert into postracking (" . 
							   "postracking_user_id, postracking_pos_id, postracking_field, postracking_oldvalue, postracking_newvalue, postracking_comment, postracking_time) VALUES (" . 
							   user_id() . ", " . 
							   dbquote($form->value("posaddress_id")) . ", " . 
							   dbquote($field) . ", " . 
							   dbquote(from_system_date($form->value('old_shop_closing_date'))) . ", " . 
							   dbquote(from_system_date($form->value('shop_closing_date'))) . ", " . 
							   dbquote('changed by user') . ", " . 
							   dbquote(date("Y-m-d:H:i:s")) . ")"; 
							   
						$result = mysql_query($sql) or dberror($sql);

						//send mail alerts
						$mail_template_id = 19;
						if($form->value("old_shop_closing_date") == NULL 
							or $form->value("old_shop_closing_date") == '0000-00-00'
						    or $form->value("old_shop_closing_date") == '')
						{
							$mail_template_id = 21;
						}
						
						$actionMail = new ActionMail($mail_template_id);
						
						$actionMail->setParam('id', $pos_data["posaddress_id"]);
						$actionMail->setParam('section', 'posaddress_store_planned_closingdate');
						
						$model = new Model(Connector::DB_CORE);
						$dataloader = $model->query("
							SELECT *
							FROM posaddresses 
							LEFT JOIN countries ON country_id = posaddress_country
							LEFT JOIN posowner_types ON posowner_type_id = posaddress_ownertype
							LEFT JOIN postypes ON postype_id = posaddress_store_postype
							WHERE posaddress_id = " . $pos_data["posaddress_id"] . "
						")->fetch();
						
						// link
						$protocol = Settings::init()->http_protocol.'://';
						$server = $_SERVER['SERVER_NAME'];
						$dataloader['link'] = $protocol.$server."/pos/posindex_pos.php?id=" . $pos_data["posaddress_id"];
						$dataloader['closing_date'] = $form->value("shop_closing_date");
						

						$actionMail->setDataloader($dataloader);
						
						// send mails
						if($senmail_activated == true)
						{
							$actionMail->send();
						}
					}



					//send mail alerts for clsing of a Corporate POS
					if(param("shop_closing_date") 
						and param("old_shop_closing_date") == ''
						and $project["project_cost_type"] == 1)
					{
							$dchannel = 'not available';
							$sql = "select concat(mps_distchannel_name, ' - ', mps_distchannel_code) as dchannel 
								   from mps_distchannels 
								   where mps_distchannel_id = " . dbquote($pos_data['posaddress_distribution_channel']);


							$res = mysql_query($sql) or dberror($sql);
							if ($row = mysql_fetch_assoc($res))
							{
								$dchannel = $row["dchannel"];
							}
							
							$Mail = new ActionMail('info.new.closings.corporate.pos');

							$data = array(
								'country' => $pos_data['country_name'],
								'pos_name' => $pos_data['posaddress_name'],
								'closing_date' => param("shop_closing_date"),
								'eprnr' => $pos_data['posaddress_eprepnr'] ? $pos_data['posaddress_eprepnr'] : "not available",
								'dchannel' => $dchannel,
								'sap_nr' => $pos_data['posaddress_sapnumber'] ? $pos_data['posaddress_sapnumber'] : 'not available',
								'sap_shipto_nr' => $pos_data['posaddress_sap_shipto_number'] ? $pos_data['posaddress_sap_shipto_number'] : 'not available'
							);
							

							$Mail->setDataloader($data);
							
							if($senmail_activated == true)
							{
								$Mail->send(true);

								$recipients = $Mail->getRecipients();
							

								foreach($recipients as $user_id=>$recipient_data)
								{
									append_mail($project["project_order"], $user_id, $Mail->getSender()->id, $recipient_data->getBody(true), "", 1);
								}
							}
					}



				}
			}

							
			$form->message("Your changes have been saved.");
		}

		
		//$link = "project_edit_pos_data.php?pid=" . param("pid"); 
        //redirect($link);

    }
}


//gogle map link
if($form->value("shop_address_country") and $form->value("shop_address_place") and $form->value("shop_address_address"))
{
	if($form->value("posaddress_id") > 0)
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", " ", $form->value("shop_address_place")) . "&a=" . str_replace("'", " ", $form->value("shop_address_address")) . "\", 700,650)'>here</a> to verify the geografical position of the POS.";
	}
	else
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", " ", $form->value("shop_address_place")) . "&a=" . str_replace("'", " ", $form->value("shop_address_address")) . "\", 700,650)'>here</a> to choose the geografical position of the POS.";
	}
}
elseif($form->value("shop_address_country") and $form->value("shop_address_place"))
{
	if($form->value("posaddress_id") > 0)
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", " ", $form->value("shop_address_place")) . "&a=\", 700,650)'>here</a> to verify the geografical position of the POS.";
	}
	else
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", " ", $form->value("shop_address_place")) . "&a=\", 700,650)'>here</a> to choose the geografical position of the POS.";
	}
}
elseif($form->value("shop_address_country"))
{
	if($form->value("posaddress_id") > 0)
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=&a=\", 700,650)'>here</a> to verify the geografical position of the POS.";
	}
	else
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=&a=\", 700,650)'>here</a> to choose the geografical position of the POS.";
	}
}
elseif(!isset($googlemaplink))
{
	$googlemaplink = "";
}


$form->value("GM", $googlemaplink);

    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit POS-Data");
$form->render();

?>
<div id="changehistory" style="display:none;">
    <strong>Changes of the agreed opening date</strong>
	<table class="table_tracking">
	<tr>
	<td class="label">User</td>
	<td class="label">Time</td>
	<td class="label">Old Value</td>
	<td class="label">New Value</td>
	<td class="label">Comment</td>
	</tr>

	<?php
		foreach($tracking_info as $key=>$values)
		{
			echo '<tr class="tr_tracking"><td class="td_tracking_nobr">' . $values['user_name'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_time'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_oldvalue'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_newvalue'] . '</td>';
			echo '<td class="td_tracking">' . $values['projecttracking_comment'] . '</td></tr>';
		}
	?>
	
	</table>
</div> 

<?php
echo "<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>";


$page->footer();

?>