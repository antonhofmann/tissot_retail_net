<?php
/********************************************************************

    order_shipping_documents.php

    List shipping details for each forwarder

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2015-01-20
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-20
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";


check_access("has_access_to_shipping_details_in_orders");

register_param("oid");


/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

// read order and order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);


//check if order_shipping_documents are present

$sql = "select DISTINCT order_item_supplier_address,  ".
		" address_company " . 
		"from order_items ".
		"left join addresses on address_id = order_item_supplier_address ";

$filter = " (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
				  "   and order_item_quantity > 0 " .
				  "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
				  "   and order_item_order = " . $order["order_id"] . 
				  "   and order_item_supplier_address > 0";


$sql = $sql . " where " . $filter . " order by address_company";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	//insert missing records
	$sql_p = "select * from address_shipping_documents " . 
			 " left join standard_shipping_documents on standard_shipping_document_id = address_shipping_document_document_id " . 
			 " where address_shipping_document_address_id = " . dbquote($order["order_client_address"]);

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	while ($row_p = mysql_fetch_assoc($res_p))
	{
		
		//check if dcoument is already there
		$sql_s = "select count(order_shipping_document_id) as num_recs " . 
			   " from order_shipping_documents " . 
			   " where order_shipping_document_order_id = " . dbquote($order["order_id"]) .
			   " and order_shipping_document_supplier_id = " . dbquote($row["order_item_supplier_address"]) . 
			   " and order_shipping_document_standard_document_id = " . dbquote($row_p["address_shipping_document_document_id"]);



		$res_s = mysql_query($sql_s) or dberror($sql_s);
		$row_s = mysql_fetch_assoc($res_s);
		if($row_s["num_recs"] == 0)
		{
		
			$fields = array();
			$values = array();

			$fields[] = "order_shipping_document_order_id";
			$values[] = dbquote($order["order_id"]);

			$fields[] = "order_shipping_document_supplier_id";
			$values[] = dbquote($row["order_item_supplier_address"]);

			$fields[] = "order_shipping_document_standard_document_id";
			$values[] = dbquote($row_p["address_shipping_document_document_id"]);

			$fields[] = "order_shipping_document_name";
			$values[] = dbquote($row_p["standard_shipping_document_name"]);

			$fields[] = "order_shipping_document_description";
			$values[] = dbquote($row_p["standard_shipping_document_description"]);

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "date_created";
			$values[] = dbquote(date("Y-m-d H:i:s"));

			
			$sql_i = "insert into order_shipping_documents (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			$result = mysql_query($sql_i) or dberror($sql_i);
		}
	}
}



//update order shipping documents from attachments
$slq_o = "select * from order_files " . 
         " left join standard_shipping_documents on standard_shipping_document_id = order_file_standard_shipping_document_id " .
         " where order_file_order = " . dbquote($order["order_id"]) .
		 " and order_file_supplier_id > 0 and order_file_standard_shipping_document_id > 0 ";


$res_o = mysql_query($slq_o) or dberror($slq_o);
while ($row_o = mysql_fetch_assoc($res_o))
{
	//check if file already is there
	$sql_f = "select count(order_shipping_document_id) as num_recs " . 
		     " from order_shipping_documents " . 
		     " where order_shipping_document_file_order_file_id = " . dbquote($row_o["order_file_id"]);

	$res_f = mysql_query($sql_f) or dberror($sql_f);
	$row_f = mysql_fetch_assoc($res_f);
	
	if($row_f["num_recs"] == 0) //update new file from order files
	{
		//check if an empty shipping document is there
		$sql_s = "select order_shipping_document_id " . 
			     " from order_shipping_documents " . 
			     " where order_shipping_document_order_id = " . dbquote($row_o["order_file_order"]) . 
			     " and order_shipping_document_standard_document_id = " . dbquote($row_o["order_file_standard_shipping_document_id"]) . 
			     " and order_shipping_document_supplier_id = " . dbquote($row_o["order_file_supplier_id"]) . 
			     " and (order_shipping_document_file_order_file_id is null or order_shipping_document_file_order_file_id = 0)";

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$value = $row_o["order_file_id"];
			$order_fields[] = "order_shipping_document_file_order_file_id = " . $value;

			$value = "current_timestamp";
			$order_fields[] = "date_modified = " . $value;

			$value = dbquote($_SESSION["user_login"]);
			$order_fields[] = "user_modified = " . $value;
			
			$sql_up = "update order_shipping_documents set " . join(", ", $order_fields) . " where order_shipping_document_id = " . $row_s["order_shipping_document_id"];
			mysql_query($sql_up) or dberror($sql_up);
		}
		else
		{
			
			$fields = array();
			$values = array();

			$fields[] = "order_shipping_document_order_id";
			$values[] = dbquote($$order["order_id"]);

			$fields[] = "order_shipping_document_supplier_id";
			$values[] = dbquote($row_o["order_file_supplier_id"]);

			$fields[] = "order_shipping_document_standard_document_id";
			$values[] = dbquote($row_o["order_file_standard_shipping_document_id"]);

			$fields[] = "order_shipping_document_name";
			$values[] = dbquote($row_o["standard_shipping_document_name"]);

			$fields[] = "order_shipping_document_description";
			$values[] = dbquote($row_o["standard_shipping_document_description"]);

			$fields[] = "order_shipping_document_file_order_file_id";
			$values[] = dbquote($row_o["order_file_id"]);

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "date_created";
			$values[] = dbquote(date("Y-m-d H:i:s"));

			
			$sql_i = "insert into order_shipping_documents (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			$result = mysql_query($sql_i) or dberror($sql_i);
			
		}


	}
}


//build sql for the list
$sql = "select address_company, order_shipping_document_name, order_file_title, order_shipping_document_file_order_file_id " . 
       " from order_shipping_documents "  . 
	   " left join addresses on address_id = order_shipping_document_supplier_id " .
	   " left join standard_shipping_documents on standard_shipping_document_id = order_shipping_document_standard_document_id " . 
	   " left join order_files on order_file_id = order_shipping_document_file_order_file_id ";

if(count($user_roles) == 1 and in_array(6, $user_roles)) // forwarder
{
	
	$list_filter = "order_shipping_document_order_id = " . dbquote($order["order_id"]);
	
	$suppliers = array();
	$sql_s = "select DISTINCT order_item_supplier_address,  ".
			 " address_company " . 
			 "from order_items ".
			 "left join addresses on address_id = order_item_supplier_address ";

	$filter = " (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
					  "   and order_item_quantity > 0 " .
					  "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
					  "   and order_item_order = " . $order["order_id"] . 
					  "   and order_item_supplier_address > 0 " . 
		              "   and order_item_forwarder_address = " . dbquote($user_data["address"]);


	$sql_s = $sql_s . " where " . $filter . " order by address_company";
	$res_s = mysql_query($sql_s) or dberror($sql_s);
	
	
	while($row_s = mysql_fetch_assoc($res_s)) 
	{
		$suppliers[] = $row_s["order_item_supplier_address"];
	}

	if(count($suppliers) > 0)
	{
		$list_filter .= " and order_shipping_document_supplier_id in(" . implode(', ', $suppliers) . ")";
	}
	else
	{
		$list_filter .= " and order_shipping_document_supplier_id = 0";
	}
	
}
elseif(count($user_roles) == 1 and in_array(5, $user_roles)) // supplier
{
    $list_filter = "order_shipping_document_order_id = " . dbquote($order["order_id"]);
	$list_filter .= " and order_shipping_document_supplier_id = " . $user_data["address"];
}
elseif (has_access("can_edit_shipping_details_in_orders"))
{
	$list_filter = "order_shipping_document_order_id = " . dbquote($order["order_id"]);
}
else
{
    $list_filter = " (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
		              "   and order_item_quantity > 0 " .
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $order["order_id"] .
                      "   and order_item_supplier_address = 0";
}


/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "order");

$form->add_hidden("oid", param("oid"));

require_once "include/order_head_small.php";




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create Item List
*********************************************************************/ 
$list = new ListView($sql, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);
$list->set_title("Mandatory Shipping Documents");
$list->set_entity("order_shipping_documents");
$list->set_filter($list_filter);
$list->set_group("address_company");
$list->set_order("order_shipping_document_name, order_file_title");

//$list->add_column("address_company", "Supplier", "", "", "", COLUMN_NO_WRAP);

$link = "http://" . $_SERVER["HTTP_HOST"] . "/include/openfile.php?id={order_shipping_document_file_order_file_id}";
$list->add_column("order_shipping_document_name", "Name", "", "", "", COLUMN_NO_WRAP);
$list->add_column("order_file_title", "Documents", $link, "", "", COLUMN_NO_WRAP);


$list->populate();
$list->process();



/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Shipping Details - Mandatory Shipping Documents");


require_once("include/tabs_shipping_details.php");



$form->render();

$list->render();


$page->footer();

?>