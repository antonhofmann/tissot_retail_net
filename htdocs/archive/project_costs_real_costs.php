<?php
/********************************************************************

    project_costs_real_costs.php

    View or edit the real costs of a project

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2012, OMEGA SA, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/project_cost_functions.php";
require_once "include/save_functions.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));
// get company's address
$client_address = get_address($project["order_client_address"]);

$currency_symbol = get_currency_symbol($project["order_client_currency"]);
$order_currency = get_order_currency($project["project_order"]);



/********************************************************************
    check if project cost positions are present and create if not
*********************************************************************/

$sql = "select count(costsheet_id) as num_recs from costsheets " . 
       " where costsheet_version = 0
			and costsheet_project_id = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) //make the user select a cost template
{
	$link = "project_costs_overview.php?pid=" . param("pid");
	redirect($link);
}



$currencies = array();
$dropdown_currencies_html = '<option value="0">';
$sql_currencies = "select currency_id, currency_symbol
					from currencies
					order by currency_symbol";
$res = mysql_query($sql_currencies) or dberror($sql_currencies);
while($row = mysql_fetch_assoc($res)) {
	$currencies[$row['currency_id']] = $row['currency_symbol'];

	if($project["order_client_currency"] == $row['currency_id']) {
		$dropdown_currencies_html .= '<option value="'. $row['currency_id'] .'" selected="selected">'. $row['currency_symbol'] .'</option>';
	}
	else {
		$dropdown_currencies_html .= '<option value="'. $row['currency_id'] .'">'. $row['currency_symbol'] .'</option>';
	}
}

//get cms approval person coordinator
$approval_person_name = "";
$sql = "select user_id, concat(user_name, ' ' , user_firstname) as uname " .
       "from users " . 
	   "where user_id = " . dbquote($project["project_cms_approver"]);

$res = mysql_query($sql) or dberror($sql);

if($row = mysql_fetch_assoc($res))
{
	$approval_person_name = $row["uname"];
}

/********************************************************************
    get budget data from cost sheet
*********************************************************************/

$code_data = array();
$budget_data = array();
$currency_data = array();
$currency_data_chf = array();
$text_data = array();
$comment_data = array();
$amounts_chf = array();
$delete_ids = array();
$delete_links = array();
$list_has_positions = array();
$costsheet_partner_contribution = array();
$costsheet_currencies = array();
$costsheet_currency_symbols = array();

$sql = "select costsheet_id, costsheet_code, costsheet_real_amount, costsheet_currency_id, currency_symbol,
         costsheet_comment2, costsheet_reinvoice_info, costsheet_exchangerate, costsheet_budget_amount, currency_factor, 
		 costsheet_is_in_budget 
         from costsheets 
		 left join currencies on currency_id = costsheet_currency_id
	     where costsheet_version = 0
			and costsheet_project_id = " . dbquote(param("pid"));


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$currency_data[$row["costsheet_id"]] = $row["currency_symbol"];
	$comment_data[$row["costsheet_id"]] = $row["costsheet_comment2"];
	$real_data[$row["costsheet_id"]] = $row["costsheet_real_amount"];
	$code_data[$row["costsheet_id"]] = $row["costsheet_code"];
	$reinvoice_data[$row["costsheet_id"]] = $row["costsheet_reinvoice_info"];

	if($row["costsheet_is_in_budget"] == 0) {
		$delete_ids[$row["costsheet_pcost_group_id"]][$row["costsheet_id"]] = "__costsheets_select_to_delete_" . $row["costsheet_id"];
		$delete_links[$row["costsheet_id"]] = '<a class="delete_line" data="'.$row['costsheet_id'].'" href="javascript:void(0);" title="Remove line"><img src="/pictures/closed.gif" /></i></a>';
	}

	$costsheet_currencies[$row["costsheet_id"]] = $row["costsheet_currency_id"];
	$costsheet_currency_symbols[$row["costsheet_id"]] = $row["currency_symbol"];

	$amounts_chf[$row["costsheet_id"]] = round(($row["costsheet_exchangerate"] * $row["costsheet_real_amount"])/$row["currency_factor"], 2);

	$amounts_chf[$row["costsheet_id"]] = number_format($amounts_chf[$row["costsheet_id"]], 2, '.', "'");
}

//get status data
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$cms_approved = $row["project_cost_cms_approved"];
$cms_approved_by = $row["project_cost_cms_approved_by"];
$cms_approved_date = $row["project_cost_cms_approved_date"];

$cms_completed = $row["project_cost_cms_completed"];
$cms_completed_by = $row["project_cost_cms_completed_by"];
$cms_completion_date = $row["project_cost_cms_completion_date"];

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("comments", "comment");


$form->add_section("Project");
$form->add_hidden("pid", param('pid'));
$form->add_hidden("order_id", $project["project_order"]);


require_once "include/project_head_small.php";


$form->add_section("Approval");

$form->add_comment("The CMS was approved as follows:");
$form->add_label("approver1", "Person to approve", 0, $approval_person_name);
if($cms_approved == 1)
{
	$form->add_label("approvalstate", "Approval State", 0, "approved");
}
else
{
	$form->add_label("approvalstate", "Approval State", 0, "not approved");
}
$form->add_lookup("approver2", "Person having approved", "users", "concat(user_name, ' ', user_firstname)", 0, dbquote($cms_approved_by));
$form->add_label("approvaldate", "Date of approval", 0, to_system_date($cms_approved_date));


$link = "javascript:popup('/user/project_costs_cms_pdf.php?pid=" . param("pid") . "', 800, 600);";
$form->add_button("print_cms", "Print CMS in " . $currency_symbol, $link);

$link = "javascript:popup('/user/project_costs_cms_pdf.php?pid=" . param("pid") . "&chf=1', 800, 600);";
$form->add_button("print_cms2", "Print CMS in CHF", $link);


$form->populate();


/********************************************************************
    Compose Cost Sheet
*********************************************************************/ 
$budget = get_project_budget_totals(param("pid"), $order_currency);

//add all cost groups and cost sub groups

$list_names = array();
$group_ids = array();
$group_titles = array();
$sub_group_ids = array();


///get all subgroups
$sql = "select costsheet_id, costsheet_pcost_group_id, costsheet_pcost_subgroup_id,  
	   costsheet_code, costsheet_text, costsheet_comment, 
	   concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup 
	   from costsheets 
	    left join pcost_subgroups on pcost_subgroup_id = costsheet_pcost_subgroup_id  
        where costsheet_version = 0
		    and (costsheet_budget_amount > 0 or costsheet_real_amount > 0) 
			and costsheet_project_id = " . dbquote(param("pid")) . 
		" and costsheet_is_in_cms = 1";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$sub_group_ids[$row['subgroup']] = $row['costsheet_pcost_subgroup_id'];
}

$sql2 = "select DISTINCT costsheet_pcost_group_id, pcost_group_code, pcost_group_name " .
	   "from costsheets " .
	   "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " .
	   " where costsheet_version = 0
	        and (costsheet_budget_amount > 0 or costsheet_real_amount > 0) 
			and costsheet_project_id = " . param("pid") . " and costsheet_is_in_cms = 1" .
	   " order by pcost_group_code";



$res = mysql_query($sql2) or dberror($sql2);
while ($row = mysql_fetch_assoc($res))
{
	$listname = "list" . $row["pcost_group_code"];
	$list_names[] = $listname;
	$group_ids[] = $row["costsheet_pcost_group_id"];
	$group_titles[] = $row["pcost_group_name"];
	
	$sql = "select costsheet_id, costsheet_code, costsheet_text, costsheet_budget_amount, " . 
		   "costsheet_budget_approved_amount, costsheet_comment2, " .
		   "concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup  " . 
		   "from costsheets " .
		   " left join pcost_subgroups on pcost_subgroup_id = costsheet_pcost_subgroup_id"; 
	
	$list_filter = "costsheet_version = 0
	        and (costsheet_budget_amount > 0 or costsheet_real_amount > 0) 
			and costsheet_project_id = " . param("pid") . " and costsheet_pcost_group_id = " . $row["costsheet_pcost_group_id"] . " and costsheet_is_in_cms = 1";


	$toggler = '<div class="toggler_pointer" id="l' . $row["costsheet_pcost_group_id"] . '_on"><span class="fa fa-minus-square toggler"></span>' .$row["pcost_group_name"] . '</div>';
	
	$$listname = new ListView($sql, LIST_HAS_HEADER );

	$$listname->set_entity("costsheets");
	$$listname->set_order("LENGTH(costsheet_code), COALESCE(costsheet_code,'Z')");
	$$listname->set_group("subgroup");
	$$listname->set_filter($list_filter);
	$$listname->set_title($toggler);

	
	
	$$listname->add_text_column("costsheet_code", "Code", 0, $code_data);

	$$listname->add_column("costsheet_budget_amount", "Budget", "", "", "", COLUMN_ALIGN_RIGHT);
	$$listname->add_column("costsheet_budget_approved_amount", "Approved", "", "", "", COLUMN_ALIGN_RIGHT);
	$$listname->add_text_column("currency", "", 0, $currency_data);
	$$listname->add_text_column("costsheet_real_amount", "Real Costs", 0, $real_data);
	$$listname->add_text_column("chf", "CHF", 0, $amounts_chf);
	$$listname->add_column("costsheet_text", "Text");
	$$listname->add_text_column("costsheet_comment2", "Comment", 0, $comment_data);
	$$listname->add_text_column("costsheet_reinvoice_info", "Reinvoice Info", 0, $reinvoice_data);


	$$listname->populate();

}

$page = new Page("projects");

require "include/project_page_actions.php";


$page->header();
$page->title("Project Costs - Real Costs");

require_once("include/costsheet_tabs.php");
$form->render();

foreach($list_names as $key=>$listname)
{
	
	if(array_key_exists("costsheet", $_SESSION) and array_key_exists($listname, $_SESSION["costsheet"]))
	{
		if($_SESSION["costsheet"][$listname] == 0)
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div style="display:none;" id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
		else
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
	}
	else
	{
		echo '<p>&nbsp;</p>';
		$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
		echo $toggler;
		
		echo '<div id="' . $listname . '">';
		$$listname->render();
		echo '</div>';
	}
}



?>
<script language="javascript">
jQuery(document).ready(function($) {
	<?php
	foreach($list_names as $key=>$listname)
	{
	?>
		$('#l<?php echo $group_ids[$key];?>_on').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'none');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'block');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=0",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
		
		$('#l<?php echo $group_ids[$key];?>_off').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'block');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'none');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=1",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
	<?php
	}
	?>
});


</script>

<?php

$page->footer();


?>