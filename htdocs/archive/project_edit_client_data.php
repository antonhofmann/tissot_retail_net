<?php
/********************************************************************

    project_edit_client_data.php

    Edit project's  data as entered by the client.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-19
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_edit_client_data_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/
$units = array();
$units[1] = "Square Feet (sqf)";
$units[2] = "Square Meters (sqm)";

// Vars
$error = "unspecified";
$design_objectives_listbox_names = array();
$design_objectives_checklist_names = array();

// read project and order details
$project = get_project(param("pid"));


$old_legal_type = $project["project_cost_type"];
$old_project_number = $project["project_number"];
$old_pos_type = $project["project_postype"];
$old_projectkind = $project["project_projectkind"];
$old_client_user = $project["order_user"];

//check if the cost monitoring sheet exists already
$sql = "select count(project_cost_id) as num_recs " .
       "from project_costs " .
       "where project_cost_order = "  . $project["project_order"]; 
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0)
{
    $fields = array();
    $values = array();

    $fields[] = "project_cost_order";
    $values[] = $project["project_order"];
    
    $fields[] = "date_created";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "date_modified";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "user_created";
    $values[] = dbquote(user_login());

    $fields[] = "user_modified";
    $values[] = dbquote(user_login());

    $sql = "insert into project_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

    mysql_query($sql) or dberror($sql);
}

//update order items with predefined cost_groups
$sql = "select order_item_id, order_item_item, item_cost_group, item_costmonitoring_group " . 
       "from order_items " . 
	   "left join items on item_id = order_item_item " .
	   "where order_item_order = " . $project["project_order"] . 
	   "  and order_item_not_in_budget <> 1 " .
	   "  and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . " or order_item_type >= " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . ") ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$sql_u = "update order_items Set order_item_cost_group =  " . dbquote($row["item_cost_group"]) . " " . 
		     " where (order_item_cost_group is NULL or order_item_cost_group = 0) " .
		     "  and order_item_id = " . $row["order_item_id"];

	mysql_query($sql_u) or dberror($sql_u);

	/* cost monitoring group was removed by Katrin Eckert on January 26th 2016
	$sql_u = "update order_items Set order_items_costmonitoring_group =  " . dbquote($row["item_costmonitoring_group"]) . " " . 
		     " where (order_items_costmonitoring_group is NULL or order_items_costmonitoring_group = 0) " .
		     "  and order_item_id = " . $row["order_item_id"];

	mysql_query($sql_u) or dberror($sql_u);
	*/
}


// get users' company address
$address = get_address($project["order_client_address"]);

// get client user data
$user = get_user($project["order_user"]);

//project legal types
if($address["client_type"] == 1) // client is agent, only franchisee as option
{
    $sql_project_cost_types = "select * from project_costtypes where project_costtype_id IN (2,6)";
}
else
{
    $sql_project_cost_types = "select * from project_costtypes where project_costtype_id IN (1,2,6)";    
}


$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);

$project_state_name = get_project_state_name($project["project_state"]);

// get orders's currency
$currency = get_order_currency($project["project_order"]);

// read design_item_ids from project items
$project_design_objective_item_ids = get_project_design_objective_item_ids(param("pid"));


// create sql for the client listbox
$sql_address = "select address_id, address_company ".
               "from addresses ".
               "where address_type = 1 or address_type = 4 ".
               "order by address_company";


// create sql for the client's contact listbox
if (!param("client_address_id"))
{
    $sql_address_user = "select user_id, concat(user_name, ' ', user_firstname) ".
                        "from users ".
                        "where user_id = " . $project["order_user"] . " or (user_active = 1 and user_address = ". $project["order_client_address"] . ") ".
                        "order by user_name";
}
else
{
    $sql_address_user = "select user_id, concat(user_name, ' ', user_firstname) ".
                        "from users ".
                        "where user_id = " . $project["order_user"] . " or (user_active = 1 and user_address = ". param("client_address_id") . ") ".
                        "order by user_name";
}


// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";

// create sql for the location type listbox
$sql_location_types = "select location_type_id, location_type_name ".
                      "from location_types ".
                      "order by location_type_name";

// create sql for the voltage listbox
$sql_voltages = "select voltage_id, voltage_name ".
                "from voltages";

// create sql for the transportation type listbox
$sql_transportation_modes = "select transportation_type_id, transportation_type_name ".
                            "from transportation_types ".
                            "where transportation_type_visible = 1 " .
							" and transportation_type_code = 'mode' " . 
                            "order by transportation_type_name";


$sql_transportation_arranged = "select transportation_type_id, transportation_type_name ".
								"from transportation_types ".
								"where transportation_type_visible = 1 " .
								" and transportation_type_code = 'arranged' " . 
								"order by transportation_type_name";


// create sql for the project type listbox
/*
$sql_pos_types = "select postype_id, postype_name ".
                     "from product_line_pos_types ".
                     "left join postypes on postype_id = product_line_pos_type_pos_type ".
                     "where product_line_pos_type_product_line = " . $project["project_product_line"] . " " .
                     "order by postype_name";
*/

$sql_pos_types = "select postype_id, postype_name ".
                     "from postypes  " .
                     "order by postype_name";


//get addresses from pos index
$sql_posaddresses = "select posaddress_id, concat(posaddress_place, ', ', posaddress_name, ', ', posaddress_address) as posaddress " .
						"from posaddresses " . 
						"where posaddress_country = " . $project["order_shop_address_country"] . 
						" order by posaddress_place, posaddress_name";


//pos subclasses
if(param("project_postype"))
{
	$sql_pos_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = " . param("project_postype") .
		              " and product_line_pos_type_product_line is null " .
	                  " order by possubclass_name";
}
elseif($project["project_postype"])
{
	$sql_pos_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = " . $project["project_postype"] .
		              " and product_line_pos_type_product_line is null " .
	                  " order by possubclass_name";
}
else
{
	$sql_pos_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = 0 " .
		              " and product_line_pos_type_product_line =  0" .
	                  " order by possubclass_name";
}


//project legal types
if(param("project_cost_type"))
{
	if(param("project_cost_type") == 1) // Corporate
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id > 0 ";
	}
	else
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
								 "from projectkinds " . 
								 "where projectkind_id in (1,2,6, 7)";
	}
}
else
{
	
	$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id > 0 ";
}

// create sql for the furniture height listbox
$sql_ceiling_heights = "select ceiling_height_id, ceiling_height_height ".
                "from ceiling_heights";

//get pos data
if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
}


//get province
$billing_address_province_name = "";
if($project["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($project["order_billing_address_place_id"]);
}

/********************************************************************
    Get Cost Monitoring Sheet Budget Data
*********************************************************************/ 
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$project_cost_grosssqms = $row["project_cost_grosssqms"];
$project_cost_totalsqms = $row["project_cost_totalsqms"];
$project_cost_sqms = $row["project_cost_sqms"];
$project_cost_original_sqms = $row["project_cost_original_sqms"];
$project_cost_backofficesqms = $row["project_cost_backofficesqms"];
$project_cost_floorsurface1 = $row["project_cost_floorsurface1"];
$project_cost_floorsurface2 = $row["project_cost_floorsurface2"];
$project_cost_floorsurface3 = $row["project_cost_floorsurface3"];
$project_cost_numfloors = $row["project_cost_numfloors"];

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_section("Project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);



if (has_access("can_edit_product_line"))
{
    
    $form->add_list("project_cost_type", "Project Legal Type", $sql_project_cost_types, 0, $project["project_cost_type"]);
	
	if($project["project_projectkind"] != 8) // not a Popup
	{
		$form->add_list("project_projectkind", "Project Type*", $sql_project_kinds,SUBMIT | NOTNULL, $project["project_projectkind"]);
	}
	else
	{
		$form->add_label("type3", "Project Type", 0, $project["projectkind_name"]);
		$form->add_hidden("project_projectkind", $project["project_projectkind"]);
	}

	$form->add_list("project_postype", "POS Type*", $sql_pos_types, NOTNULL | SUBMIT, $project["project_postype"]);
	$form->add_list("project_pos_subclass", "POS Type Subclass", $sql_pos_subclasses, 0, $project["project_pos_subclass"]);

	
    $form->add_label("product_line_name", "Product Line / Subclass", 0, $project["product_line_name"] . " / " . $project["productline_subclass_name"]);
	$form->add_hidden("product_line",$project["project_product_line"]);
	
	if (has_access("can_edit_project_number"))
    {
        $form->add_edit("project_number", "Project Number*", NOTNULL, $project["order_number"], TYPE_CHAR);
    }
    else
    {
        $form->add_label("project_number", "Project Number*", 0, $project["order_number"]);
    }
	$form->add_label("treatment_state", "Treatment State", 0, $project["project_state_text"]);
	
}
else
{
    $form->add_label("type3", "Project Legal Type / Project Type", 0, $project["project_costtype_text"] . " / " . $project["projectkind_name"]);

	$form->add_label("project_postype", "POS Type / Subclass", 0, $project["postype_name"] . " / " . $project["possubclass_name"]);

	$form->add_label("product_line_name", "Product Line / Subclass", 0, $project["product_line_name"] . " / " . $project["productline_subclass_name"]);
	$form->add_hidden("product_line",$project["project_product_line"]);

	$form->add_label("project_number_label", "Project Number  /Treatment State", 0, $project["project_number"] . " / " . $project["project_state_text"]);
}

if (has_access("can_edit_status_in_projects"))
{
    $sql = "select distinct order_state_code " .
           "from order_states " .
           "order by order_state_code";
    $form->add_list("status", "Project Status", $sql, NOTNULL, $project["order_actual_order_state_code"]);

	//$form->add_label("staff", "Project Leader / Logistics Coordinator", 0, $project["project_manager"] . " / " . $project["operator"]);

	$form->add_label("staff", "Project Leader", 0, $project["project_manager"]);

	$form->add_hidden("project_state", $project["project_state"]);
}
else
{
    $form->add_hidden("project_state", $project["project_state"]);

    $form->add_hidden("status", $project["order_actual_order_state_code"]);

    $form->add_label("status1", "Project Status", 0, $project["order_actual_order_state_code"]  . " " . $order_state_name);

}

$form->add_section("Client");
$form->add_list("client_address_id", "Client*", $sql_address, SUBMIT | NOTNULL, $project["order_client_address"]);
$form->add_list("client_address_user_id", "Contact*", $sql_address_user, NOTNULL, $project["order_user"]);

if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) // take over, lease renewal
{
 // nop
}
else
{
	$form->add_section("Bill to");
	$form->add_label("billing_address_company", "Company*", NOTNULL, $project["order_billing_address_company"]);
	$form->add_label("billing_address_company2", "", 0, $project["order_billing_address_company2"]);
	$form->add_label("billing_address_address", "Street", NOTNULL, $project["order_billing_address_address"]);
	$form->add_label("billing_address_address2", "Additional Address Info", 0, $project["order_billing_address_address2"]);
	$form->add_label("billing_address_zip", "ZIP", 0, $project["order_billing_address_zip"]);
	$form->add_label("billing_address_place", "City*", NOTNULL, $project["order_billing_address_place"]);
	$form->add_label("billing_address_province_name", "Province", 0, $billing_address_province_name);
	$form->add_lookup("billing_address_country", "Country", "countries", "country_name", 0, $project["order_billing_address_country"]);
	$form->add_label("billing_address_phone", "Phone*", NOTNULL, $project["order_billing_address_phone"]);
	$form->add_label("billing_address_mobile_phone", "Mobile Phone", 0, $project["order_billing_address_mobile_phone"]);
	$form->add_label("billing_address_email", "Email", 0, $project["order_billing_address_email"]);
	$form->add_label("billing_address_contact", "Contact*", NOTNULL, $project["order_billing_address_contact"]);

}

$form->add_section("POS Location Address");
if(param("project_projectkind") == 7 and param("project_projectkind") != $project["project_projectkind"])
{
	
	$form->add_comment("Assignment to POS Index.");
	$form->add_list("posaddress_id", "POS Index POS Name", $sql_posaddresses, SUBMIT);
	
}
else
{
	$form->add_hidden("posaddress_id", $pos_data["posaddress_id"]);
}
$form->add_label("shop_address_company", "Project Name*", 0, $project["order_shop_address_company"]);
$form->add_label("shop_address_company2", "", 0, $project["order_shop_address_company2"]);
$form->add_label("shop_address_address", "Street", 0, $project["order_shop_address_address"]);
$form->add_label("shop_address_address2", "Additional Address Info", 0, $project["order_shop_address_address2"]);
$form->add_label("shop_address_zip", "ZIP", 0, $project["order_shop_address_zip"]);
$form->add_label("shop_address_place", "City*", 0, $project["order_shop_address_place"]);
$form->add_lookup("shop_address_country_name", "Country", "countries", "country_name", 0, $project["order_shop_address_country"]);
$form->add_hidden("shop_address_country",$project["order_shop_address_country"]);
$form->add_label("shop_address_phone", "Phone", 0, $project["order_shop_address_phone"]);
$form->add_label("shop_address_mobile_phone", "Mobile Phone", 0, $project["order_shop_address_mobile_phone"]);
$form->add_label("shop_address_email", "Email", 0, $project["order_shop_address_email"]);



if($project["project_projectkind"] == 6
	or $project["project_projectkind"] == 9) // relocated POS
{
	$form->add_section("Relocation Info");
	$form->add_comment("Please indicate the POS to be relocated.");
	$form->add_list("project_relocated_posaddress_id", "POS being relocated", $sql_posaddresses, 0, $project["project_relocated_posaddress_id"]);
}
else
{
	$form->add_hidden("project_relocated_posaddress_id", 0);
}

if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) // take over, lease renewal
{
	$form->add_hidden("store_grosssurface", $project_cost_grosssqms);
	$form->add_hidden("store_totalsurface", $project_cost_totalsqms);
	$form->add_hidden("store_retailarea", $project_cost_sqms);
	$form->add_hidden("store_backoffice", $project_cost_backofficesqms);
	$form->add_hidden("store_numfloors", $project_cost_numfloors);
	$form->add_hidden("store_floorsurface1", $project_cost_floorsurface1);
	$form->add_hidden("store_floorsurface2", $project_cost_floorsurface2);
	$form->add_hidden("store_floorsurface3", $project_cost_floorsurface3);


	$form->add_hidden("location_type", $project["project_location_type"]);
	$form->add_hidden("location_type_other", $project["project_location"]);
	$form->add_hidden("voltage", $project["order_voltage"]);

	$form->add_hidden("watches_displayed",$project["project_watches_displayed"]);
	$form->add_hidden("watches_stored", $project["project_watches_stored"]);
	$form->add_hidden("bijoux_displayed",$project["project_bijoux_displayed"]);
	$form->add_hidden("bijoux_stored", $project["project_bijoux_stored"]);

	//$form->add_hidden("preferred_delivery_date", to_system_date($project["order_preferred_delivery_date"]));
	$form->add_hidden("preferred_transportation_arranged", $project["order_preferred_transportation_arranged"]);
	$form->add_hidden("preferred_transportation_mode", $project["order_preferred_transportation_mode"]);

	$form->add_hidden( "pedestrian_mall_approval", $project["order_pedestrian_mall_approval"]);
	$form->add_hidden( "full_delivery", $project["order_full_delivery"]);
	$form->add_hidden("delivery_comments", $project["order_delivery_comments"]);

	$form->add_hidden("order_insurance", $project["order_insurance"]);

	$form->add_hidden("approximate_budget", $project["project_approximate_budget"]);

	$form->add_hidden("planned_takeover_date");
	$form->add_hidden("planned_opening_date", to_system_Date($project["project_planned_opening_date"]));


	$form->add_hidden("comments", $project["project_comments"]);
	$form->add_hidden("unit_of_measurement", 2);

	$form->add_hidden("planned_closing_date", to_system_Date($project["project_planned_closing_date"]));

}
else
{
	$form->add_section("Surfaces");
	$form->add_list("unit_of_measurement", "Unit of measurement", $units, NOTNULL, 2);
	
	//$form->add_edit("store_grosssurface", "Gross Surface in <span id='um01'>sqms</span>*", NOTNULL, $project_cost_grosssqms, TYPE_DECIMAL, 10,2);
	$form->add_hidden("store_grosssurface", $project_cost_grosssqms);


	$form->add_edit("store_totalsurface", "Total Surface in <span id='um01'>sqms</span>*", NOTNULL, $project_cost_totalsqms, TYPE_DECIMAL, 10,2);

	$form->add_label("requested store_retailarea", "Sales Surface upon project entry in sqms", 0, $project_cost_original_sqms);

	$form->add_edit("store_retailarea", "Sales Surface in <span id='um02'>sqms</span>*", NOTNULL, $project_cost_sqms, TYPE_DECIMAL, 10,2);
	$form->add_edit("store_backoffice", "Other Surface in <span id='um03'>sqms</span>", 0, $project_cost_backofficesqms, TYPE_DECIMAL, 10,2);
	
	/*
	$form->add_edit("store_numfloors", "Number of Floors*", NOTNULL, $project_cost_numfloors, TYPE_DECIMAL, 10,2);
	$form->add_edit("store_floorsurface1", "Floor 1: Surface in <span id='um04'>sqms</span>", 0, $project_cost_floorsurface1, TYPE_DECIMAL, 10,2);
	$form->add_edit("store_floorsurface2", "Floor 2: Surface in <span id='um05'>sqms</span>", 0, $project_cost_floorsurface2, TYPE_DECIMAL, 10,2);
	$form->add_edit("store_floorsurface3", "Floor 3: Surface in <span id='um06'>sqms</span>", 0, $project_cost_floorsurface3, TYPE_DECIMAL, 10,2);
	*/

	$form->add_hidden("store_numfloors", $project_cost_numfloors);
	$form->add_hidden("store_floorsurface1", $project_cost_floorsurface1);
	$form->add_hidden("store_floorsurface2", $project_cost_floorsurface2);
	$form->add_hidden("store_floorsurface3", $project_cost_floorsurface3);


	$form->add_section("Location Info");
	$form->add_list("location_type", "Location Type*", $sql_location_types, 0, $project["project_location_type"]);
	$form->add_edit("location_type_other", "Other Location Type*", 0, $project["project_location"], TYPE_CHAR, 20);
	//$form->add_list("voltage", "Voltage Choice*", $sql_voltages, NOTNULL, $project["order_voltage"]);
	$form->add_hidden("voltage", $project["order_voltage"]);

	if($project["date_created"] < '2010-12-05')
	{
		// count design_objective_groups old version when design objectives were depending on the product line
		$sql_design_objective_group = "select design_objective_group_id, design_objective_group_name, " .
									  "    design_objective_group_multiple " .
									  "from design_objective_groups " .
									  "where design_objective_group_product_line=  " . $project["project_product_line"] . " " .
									  "order by design_objective_group_priority";

		$res = mysql_query($sql_design_objective_group) or dberror($sql_design_objective_group);
		$number_of_design_objective_groups = mysql_num_rows($res);
	}
	else
	{
		// count design_objective_groups new version, design objectives depending on the postype
		$sql_design_objective_group = "select design_objective_group_id, design_objective_group_name, " .
									  "    design_objective_group_multiple " .
									  "from design_objective_groups " .
									  "where design_objective_group_postype =  " . $project["project_postype"] . " " .
									  "order by design_objective_group_priority";

		$res = mysql_query($sql_design_objective_group) or dberror($sql_design_objective_group);
		$number_of_design_objective_groups = mysql_num_rows($res);
	}

	if ($number_of_design_objective_groups > 0)
	{
		$form->add_section("Design Objectives");
		$form->add_comment("Please indicate your basic needs and requirements to assist out design process. ".
		"\n A Project Leader will ensure that the design is consistent with the ".
		"Tissot Retail objectives."); 

		$i = 1;
		
		while ($row = mysql_fetch_assoc($res))
		{
			$sql_design_objective_item = "select design_objective_item_id, design_objective_item_name ".
										 "from design_objective_items ".
										 "where design_objective_item_group=" .    $row["design_objective_group_id"] . " ".
										 "order by design_objective_item_priority";

			if ($row["design_objective_group_multiple"] == 0) 
			{
				$value = "";
				foreach ($project_design_objective_item_ids as $key=>$element)
				{
				  if ($element == $row["design_objective_group_name"])
				  {
					  $value = $key;
				  }
				}

				$form->add_list("design_objective_items" . $i, $row["design_objective_group_name"] . "*", $sql_design_objective_item, NOTNULL, $value);
				$design_objectives_listbox_names[] = "design_objective_items" . $i;
			}
			else 
			{
				$values = array();
				foreach ($project_design_objective_item_ids as $key=>$element)
				{
					if ($element == $row["design_objective_group_name"])
					{
						$values[] = $key;
					}
					next($project_design_objective_item_ids);
				}
				$form->add_checklist("design_objective_items" . $i, $row["design_objective_group_name"] . "*", "project_items", $sql_design_objective_item, NOTNULL, $values);
				$design_objectives_checklist_names[] = "design_objective_items" . $i;
			}
			
			$i++;   
		}
	}

	/*
	$form->add_section("Capacity Request by Client");
	$form->add_edit("watches_displayed", "Watches Displayed", 0, $project["project_watches_displayed"], TYPE_INT, 20);
	$form->add_edit("watches_stored", "Watches Stored", 0, $project["project_watches_stored"], TYPE_INT, 20);
	$form->add_edit("bijoux_displayed", "Watch Straps Displayed", 0, $project["project_bijoux_displayed"], TYPE_INT, 20);
	$form->add_edit("bijoux_stored", "Watch Straps Stored", 0, $project["project_bijoux_stored"], TYPE_INT, 20);
    */

	$form->add_hidden("watches_displayed",$project["project_watches_displayed"]);
	$form->add_hidden("watches_stored", $project["project_watches_stored"]);
	$form->add_hidden("bijoux_displayed",$project["project_bijoux_displayed"]);
	$form->add_hidden("bijoux_stored", $project["project_bijoux_stored"]);

	$form->add_section("Preferences and Traffic Checklist");
	$form->add_comment("Please enter the date in the form of dd.mm.yyyy.");
	//$form->add_edit("preferred_delivery_date", "Preferred Arrival Date*", NOTNULL, to_system_date($project["order_preferred_delivery_date"]), TYPE_DATE, 20);
	
	
	
	$form->add_list("preferred_transportation_arranged", "Transportation arranged by*", $sql_transportation_arranged, NOTNULL, $project["order_preferred_transportation_arranged"]);

	$form->add_list("preferred_transportation_mode", "Transportation mode*", $sql_transportation_modes, NOTNULL, $project["order_preferred_transportation_mode"]);

	//$form->add_radiolist( "packaging_retraction", "Packaging Retraction Desired", array(0 => "no", 1 => "yes"), 0, $project["order_packaging_retraction"]);

	$form->add_comment("Please indicate if there is a special approval needed for delivery into ".
					   "a pedestrian area."); 
	$form->add_radiolist( "pedestrian_mall_approval", "Pedestrian Area Approval Needed",
		array(0 => "no", 1 => "yes"), 0, $project["order_pedestrian_mall_approval"]);
	$form->add_comment("Please indicate if partial delivery is possible or full delivery is required."); 
	$form->add_radiolist( "full_delivery", "Full Delivery",
		array(0 => "no", 1 => "yes"), 0, $project["order_full_delivery"]);
	$form->add_comment("Please indicate any other circumstances/restrictions concerning delivery and traffic."); 
	$form->add_multiline("delivery_comments", "Delivery Comments", 4, 0, $project["order_delivery_comments"]);

	$form->add_section("Insurance");
	$form->add_radiolist("order_insurance", array(1=>"Insurance by Tissot/Forwarder", 0=>""), array(1=>"covered",0=>"not covered"), VERTICAL, $project["order_insurance"]);


	$form->add_section("Other Information");
	$form->add_comment("Please use only figures to indicate the budget ".
					   "and enter the date in the form of dd.mm.yyyy.");
	$form->add_edit("approximate_budget", "Approximate Budget in " . $currency["symbol"] . "*", NOTNULL, $project["project_approximate_budget"], TYPE_DECIMAL, 20, 2);
	
	if($project["project_projectkind"] == 3
		or $project["project_projectkind"] == 9) // takover renovation
	{
		$form->add_edit("planned_takeover_date", "Client's Planned Takeover Date*", NOTNULL, to_system_Date($project["project_planned_takeover_date"]), TYPE_DATE, 20);

	}
	else
	{
		$form->add_hidden("planned_takeover_date");
	}
	$form->add_edit("planned_opening_date", "Client's Preferred Opening Date*", NOTNULL, to_system_Date($project["project_planned_opening_date"]), TYPE_DATE, 20);

	if($project["project_pos_subclass"] == 27 or $project["project_projectkind"] == 8) //temporary POS, PopUp
	{
		$form->add_edit("planned_closing_date", "Client's Planned Closing Date*", NOTNULL, to_system_Date($project["project_planned_closing_date"]), TYPE_DATE, 20);
	}
	else
	{
		$form->add_hidden("planned_closing_date");	
	}


	$form->add_section("General Comments");
	$form->add_multiline("comments", "Comments", 4, 0, $project["project_comments"]);
}

$form->add_button("save", "Save Data");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("product_line"))
{
    delete_design_objective_items(param("pid"));
    project_update_project_product_line(param("pid"), $form->value("product_line"));
    $link = "project_edit_client_data.php?pid=" . param("pid") . "&change_of_product_line=1";
    redirect($link);
}
elseif ($form->button("client_address_id"))
{
}
else if ($form->button("posaddress_id"))
{
	$sql = "select * from posaddresses where posaddress_id = " . $form->value("posaddress_id");
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->value("shop_address_company", $row["posaddress_name"]);
		$form->value("shop_address_address",  $row["posaddress_address"]);
		$form->value("shop_address_address",  $row["posaddress_address"]);
		$form->value("shop_address_address",  $row["posaddress_address"]);
		$form->value("shop_address_address2",  $row["posaddress_address2"]);
		$form->value("shop_address_zip",  $row["posaddress_zip"]);
		$form->value("shop_address_place",  $row["posaddress_place"]);
		$form->value("shop_address_country",  $row["posaddress_country"]);
		$form->value("shop_address_phone",  $row["posaddress_phone"]);
		$form->value("shop_address_mobile_phone",  $row["posaddress_mobile_phone"]);
		$form->value("shop_address_email",  $row["posaddress_email"]);
	}
}
else if ($form->button("save"))
{
// add validation ruels

    
	
	if($old_project_number != $form->value('project_number')) {

					
		//project tracking
		$field = "project_real_opening_date";
		$sql = "Insert into projecttracking (" . 
			   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
			   user_id() . ", " . 
			   $project["project_id"] . ", " . 
			   dbquote("project_number") . ", " . 
			   dbquote($old_project_number ) . ", " . 
			   dbquote($form->value('project_number')) . ", " . 
			   dbquote('changed') . ", " . 
			   dbquote(date("Y-m-d:H:i:s")) . ")"; 
			   
		$result = mysql_query($sql) or dberror($sql);
	}


	if($old_client_user > 0 
		and $old_client_user != $form->value('client_address_user_id')) {
		$field = "order_user";
		$old_tmp = get_user($old_client_user );
		$new_tmp = get_user($form->value('client_address_user_id'));
		$sql = "Insert into projecttracking (" . 
			   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
			   user_id() . ", " . 
			   $project["project_id"] . ", " . 
			   dbquote($field) . ", " . 
			   dbquote($old_tmp["name"] . ' ' . $old_tmp["firstname"]) . ", " . 
			   dbquote($new_tmp["name"] . ' ' . $new_tmp["firstname"]) . ", " .  
			   "'Projectowner changed', " . 
			   dbquote(date("Y-m-d:H:i:s")) . ")"; 
			   
		$result = mysql_query($sql) or dberror($sql);
	}

	if($old_projectkind != $form->value('project_projectkind')) {

			
		//project tracking
		$field = "project_real_opening_date";
		$sql = "Insert into projecttracking (" . 
			   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
			   user_id() . ", " . 
			   $project["project_id"] . ", " . 
			   dbquote("project_projectkind") . ", " . 
			   dbquote($old_projectkind ) . ", " . 
			   dbquote($form->value('project_projectkind')) . ", " . 
			   dbquote('changed') . ", " . 
			   dbquote(date("Y-m-d:H:i:s")) . ")"; 
			   
		$result = mysql_query($sql) or dberror($sql);
	}

	if($old_pos_type != $form->value('project_postype')) {

			
		//project tracking
		$field = "project_real_opening_date";
		$sql = "Insert into projecttracking (" . 
			   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
			   user_id() . ", " . 
			   $project["project_id"] . ", " . 
			   dbquote("project_postype") . ", " . 
			   dbquote($old_pos_type ) . ", " . 
			   dbquote($form->value('project_postype')) . ", " . 
			   dbquote('changed') . ", " . 
			   dbquote(date("Y-m-d:H:i:s")) . ")"; 
			   
		$result = mysql_query($sql) or dberror($sql);
	}

	if($old_legal_type != $form->value('project_cost_type')) {

			
		//project tracking
		$field = "project_real_opening_date";
		$sql = "Insert into projecttracking (" . 
			   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
			   user_id() . ", " . 
			   $project["project_id"] . ", " . 
			   dbquote("project_cost_type") . ", " . 
			   dbquote($old_legal_type ) . ", " . 
			   dbquote($form->value('project_cost_type')) . ", " . 
			   dbquote('changed') . ", " . 
			   dbquote(date("Y-m-d:H:i:s")) . ")"; 
			   
		$result = mysql_query($sql) or dberror($sql);
	}
	
	if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
	{
		if (!$form->value("location_type") and !$form->value("location_type_other"))
		{
			$form->add_validation("{location_type}", "The location type must not be empty.");
		}
	}    
    
	
	if(param("project_projectkind") == 7 
		and param("project_projectkind") != $old_projectkind 
		and !$form->value("posaddress_id"))
	{
		$form->error("You have changed the project type from NEW to Equip Retailer. You must assign an existing Independent Retailer to the project.");
	}
	elseif ($form->validate())
    {
        
		
	   if($form->value("unit_of_measurement") == 1)
	   {
			$form->value("unit_of_measurement", 2);
			$form->value("store_totalsurface", feet_to_sqm($form->value("store_totalsurface")));
			$form->value("store_retailarea", feet_to_sqm($form->value("store_retailarea")));
			$form->value("store_backoffice", feet_to_sqm($form->value("store_backoffice")));
			$form->value("store_floorsurface1", feet_to_sqm($form->value("store_floorsurface1")));
			$form->value("store_floorsurface2", feet_to_sqm($form->value("store_floorsurface2")));
			$form->value("store_floorsurface3", feet_to_sqm($form->value("store_floorsurface3")));
	   }
		
	  
	  project_update_client_data($form,$design_objectives_listbox_names,$design_objectives_checklist_names);
      
		

	  if(param("project_projectkind") == 7 
		and param("project_projectkind") != $old_projectkind 
		and $form->value("posaddress_id"))
	  {
	  
		//create fake project
		//get date of sales agreement in case of an equipment project
		$sales_agreement_date = "";
		$sales_agreement_year = 0;
		$fake_project_exists = false;
		$was_independent_retailer = false;
		$project = get_project($form->value("pid"));

		//check if address is an independent retailer
		$sql_p = 'select address_is_independent_retailer ' . 
				 'from addresses ' .
				 'where address_id = ' . dbquote($project["order_franchisee_address_id"]);
		$res_p = mysql_query($sql_p) or dberror($sql_p);
		
		if($row_p = mysql_fetch_assoc($res_p))
		{
			if($row_p["address_is_independent_retailer"] == 1)
			{
				$was_independent_retailer = true;
			}
		}

		
		//check if fake project already exists
		$sql_p = 'select count(posorder_id) as num_recs ' . 
				 'from posorders ' .
				 'where user_created = "system@retailnet" and posorder_posaddress = ' . dbquote($form->value("posaddress_id"));
		
		$res_p = mysql_query($sql_p) or dberror($sql_p);
		$row_p = mysql_fetch_assoc($res_p);
		if($row_p["num_recs"] > 0)
		{
			$fake_project_exists = true;
		}
		
		$sales_agreement_year = 1970;
		$sales_agreement_date = "1970-01-01";

		if($fake_project_exists == false)
		{
			$fields = array();
			$values = array();


			$fields[] = "posorder_posaddress";
			$values[] = dbquote($form->value("posaddress_id"));

			$fields[] = "posorder_type";
			$values[] = 1;

			$fields[] = "posorder_ordernumber";
			$values[] = dbquote('sales agreement');

			$fields[] = "posorder_year";
			$values[] = dbquote($sales_agreement_year);

			$fields[] = "posorder_product_line";
			$values[] = 0;

			$fields[] = "posorder_product_line_subclass";
			$values[] = 0;

			$fields[] = "posorder_postype";
			$values[] = 4;

			$fields[] = "posorder_subclass";
			$values[] = 0;

			$fields[] = "posorder_project_kind";
			$values[] = 1;

			$fields[] = "posorder_legal_type";
			$values[] = 6;

			$fields[] = "posorder_opening_date";
			$values[] = dbquote($sales_agreement_date);

			$fields[] = "date_created";
			$values[] = dbquote(date("Y-m-d H:i:s"));

			$fields[] = "date_modified";
			$values[] = dbquote(date("Y-m-d H:i:s"));

			$fields[] = "user_created";
			$values[] = dbquote('system@retailnet');

			$fields[] = "user_modified";
			$values[] = dbquote(user_login());

			$sql = "insert into posorders (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);
		}
	  }
	  $form->message("Your changes have been saved.");
    }
}


    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit Request");

$form->render();

?>

<script language="Javascript">

	function round_decimal(num,decimals){
		return Math.round(num*Math.pow(10,decimals))/Math.pow(10,decimals);
	}


	$(document).ready(function(){
		  $("#unit_of_measurement" ).change(function() {
				
				if($("#unit_of_measurement" ).val() == 1)
				{
					$("#um01").html("<strong> sqf</strong>");
					$("#um02").html("<strong> sqf</strong>");
					$("#um03").html("<strong> sqf</strong>");
					$("#um04").html("<strong> sqf</strong>");
					$("#um05").html("<strong> sqf</strong>");
					$("#um06").html("<strong> sqf</strong>");
					
					$("#store_totalsurface").val(round_decimal(10.7639104*$("#store_totalsurface").val(), 2));
					$("#store_retailarea").val(round_decimal(10.7639104*$("#store_retailarea").val(), 2));
					$("#store_backoffice").val(round_decimal(10.7639104*$("#store_backoffice").val(), 2));

					$("#store_floorsurface1").val(round_decimal(10.7639104*$("#store_floorsurface1").val(), 2));
					$("#store_floorsurface2").val(round_decimal(10.7639104*$("#store_floorsurface2").val(), 2));
					$("#store_floorsurface3").val(round_decimal(10.7639104*$("#store_floorsurface3").val(), 2));
				}
				else
			    {
					$("#um01").html("<strong> sqms</strong>");
					$("#um02").html("<strong> sqms</strong>");
					$("#um03").html("<strong> sqms</strong>");
					$("#um04").html("<strong> sqms</strong>");
					$("#um05").html("<strong> sqms</strong>");
					$("#um06").html("<strong> sqms</strong>");

					
					$("#store_totalsurface").val(round_decimal(0.09290304*$("#store_totalsurface").val(), 2));
					$("#store_retailarea").val(round_decimal(0.09290304*$("#store_retailarea").val(), 2));
					$("#store_backoffice").val(round_decimal(0.09290304*$("#store_backoffice").val(), 2));

					$("#store_floorsurface1").val(round_decimal(0.09290304*$("#store_floorsurface1").val(), 2));
					$("#store_floorsurface2").val(round_decimal(0.09290304*$("#store_floorsurface2").val(), 2));
					$("#store_floorsurface3").val(round_decimal(0.09290304*$("#store_floorsurface3").val(), 2));

				}
		  });


	});

</script>


<?php
$page->footer();

?>